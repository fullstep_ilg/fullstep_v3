﻿Imports Fullstep.FSNLibrary
Imports Fullstep.FSNServer
Imports Infragistics.Web.UI
Imports System.Web.Script.Serialization
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports System.IO
Imports Infragistics.Web.UI.GridControls

Public Class DetallePedido
    Inherits FSEPPage

    Private _Empresas As CEmpresas
    Private _Persona As CPersona
    Private _Destinos As CDestinos
    Private Desde As Byte
    Private InstanciaAprob As Long
    Private dsPedidos As DataSet
    Private m_sNoAplica As String
    Private m_idOrdenPedAbierto As Integer
    Private Enum Acciones
        AccionNula = 0
        NoAnularOrden = 1
        AnularOrden = 2
        AnularRecepcion = 3
        AnularLinea = 4
        BloquearFacturacionAlbaran = 5
        DesBloquearFacturacionAlbaran = 6
    End Enum
#Region "Propiedades"
    Private _oInstanciaGrupos As FSNServer.Grupos
    Protected ReadOnly Property oInstanciaGrupos() As FSNServer.Grupos
        Get
            If _oInstanciaGrupos Is Nothing Then
                If Me.IsPostBack Then
                    _oInstanciaGrupos = CType(Cache("oInstanciaGrupos" & FSNUser.Cod), FSNServer.Grupos)
                Else
                    _oInstancia.CargarCamposInstancia(Idioma, FSNUser.CodPersona, , True)
                    _oInstanciaGrupos = _oInstancia.Grupos
                End If
            End If
            Return _oInstanciaGrupos
        End Get
    End Property
    Private _oInstancia As FSNServer.Instancia
    Protected ReadOnly Property oInstancia() As FSNServer.Instancia
        Get
            If _oInstancia Is Nothing Then
                If Me.IsPostBack Then
                    _oInstancia = CType(Cache("oInstancia" & FSNUser.Cod), FSNServer.Instancia)
                Else
                    _oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                    _oInstancia.ID = Request("Instancia")
                    'Carga los datos de la instancia:
                    _oInstancia.Cargar(Idioma)
                    Dim lBloque, lRol As Long
                    If (Request("Bloque") <> Nothing) And (Request("Bloque") <> "") Then
                        lBloque = CLng(Request("Bloque"))
                    Else
                        lBloque = 0
                    End If
                    If (Request("Rol") <> Nothing) And (Request("Rol") <> "") Then
                        lRol = CLng(Request("Rol"))
                    Else
                        lRol = 0
                    End If
                    _oInstancia.CargarCamposInstancia(Idioma, FSNUser.CodPersona, , True, , , , lBloque, lRol)
                    _oInstancia.DevolverEtapaActual(Idioma, , lBloque)
                    _oInstancia.CargarCumplimentacionDesglosePedido()
                    _oInstanciaGrupos = _oInstancia.Grupos

                    If Not _oInstancia Is Nothing Then Me.InsertarEnCache("oInstancia" & FSNUser.Cod, _oInstancia)
                    If Not _oInstanciaGrupos Is Nothing Then Me.InsertarEnCache("oInstanciaGrupos" & FSNUser.Cod, _oInstanciaGrupos)
                End If
            End If
            Return _oInstancia
        End Get
    End Property
    Private _oInstanciaLinea As FSNServer.Instancia
    Protected ReadOnly Property oInstanciaLinea(ByVal idInstancia As Long) As FSNServer.Instancia
        Get
            If Cache("oInstancia" & "_" & idInstancia & FSNUser.Cod) Is Nothing Then
                Dim lBloque As Long = 0
                _oInstanciaLinea = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                _oInstanciaLinea.ID = idInstancia
                'Carga la etapa actual de la instancia:
                If CType(Request.QueryString("estado"), CPedido.Estado) = CPedido.Estado.EmitidoAlProveedor Then
                    ''Para devolver etapa actual si el pedido estÃ¡ emitido
                    _oInstanciaLinea.DevolverEtapaActualEmitido(Idioma)
                Else
                    _oInstanciaLinea.DevolverEtapaActual(Idioma, , lBloque)
                End If
                If Not _oInstanciaLinea Is Nothing Then Me.InsertarEnCache("oInstancia" & "_" & idInstancia & FSNUser.Cod, _oInstanciaLinea)
            End If
            If _oInstanciaLinea Is Nothing Then
                _oInstanciaLinea = Cache("oInstancia" & "_" & idInstancia & FSNUser.Cod)
            End If
            Return _oInstanciaLinea
        End Get
    End Property
    ''' <summary>
    ''' Dataset que contiene el listado de pedidos a mostrar en pantalla
    ''' </summary>
    Private ReadOnly Property DsetPedidos(Optional ByVal MasResultados As Boolean = False) As DataSet
        Get
            Dim idPedido As String = Request.QueryString("ID")
            Dim dsObj As Object = HttpContext.Current.Cache("DsetEmisionPedidos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString() & "_" & idPedido.ToString())
            Return ActualizarDsetpedidos()
        End Get
    End Property
    ''' <summary>
    ''' Devuelve la relación entre:
    ''' campos del grid, del dataset de pedidos y el datatable con los datos de ancho, visibilidad y posicion
    ''' Aquí se establecen los valores de visibilidad y ancho por defecto. Si hay valores guardados, se sobreescriben posteriormente
    ''' </summary>
    ''' <returns>Lista de Campos con la relación</returns>
    ''' <remarks>Llamada desde confAnchosVisibilidadYPosicion. Máx. 0,1 seg.</remarks>
    Private ReadOnly Property relacionarEntregasyConfiguracion() As List(Of Campos)
        Get
            Dim relacionEntregasyConfiguracion As New List(Of Campos)
            'Cada campo consta de la info:
            '   Campo del stored que carga el whdg (ej: "RETRASADO")
            '   Ancho por defecto del campo
            '   Tipo de Campo
            '   ID del campo a usar cuando guardemos la configuración
            If CType(Request("Desde"), Integer) = 3 Then
                relacionEntregasyConfiguracion.Add(New Campos("NUM_LINEA", TipoCampoPedido.Generico, "NUM_LINEA", 35, True, 0))
                relacionEntregasyConfiguracion.Add(New Campos("IDLINEA", TipoCampoPedido.Generico, "IDLINEA", 0, False, 1))
                relacionEntregasyConfiguracion.Add(New Campos("COD_ITEM", TipoCampoPedido.Generico, "COD_ITEM", 100, True, 2))
                relacionEntregasyConfiguracion.Add(New Campos("ART_DEN", TipoCampoPedido.Generico, "ARTDEN", 0, True, 3))
                relacionEntregasyConfiguracion.Add(New Campos("PREC", TipoCampoPedido.Generico, "PREC", 80, True, 4))
                relacionEntregasyConfiguracion.Add(New Campos("CANT", TipoCampoPedido.Generico, "CANT", 60, True, 5))
                relacionEntregasyConfiguracion.Add(New Campos("UNI", TipoCampoPedido.Generico, "UNI", 40, True, 6))
                relacionEntregasyConfiguracion.Add(New Campos("UP_DEF", TipoCampoPedido.Generico, "UP_DEF", 0, False, 7))
                relacionEntregasyConfiguracion.Add(New Campos("COSTES", TipoCampoPedido.Generico, "COSTES", 60, True, 8))
                relacionEntregasyConfiguracion.Add(New Campos("DESCUENTOS", TipoCampoPedido.Generico, "DESCUENTOS", 60, True, 9))
                relacionEntregasyConfiguracion.Add(New Campos("IMPBRUTO", TipoCampoPedido.Generico, "IMPBRUTO", 60, True, 10))
                relacionEntregasyConfiguracion.Add(New Campos("CANTIDADPDTE", TipoCampoPedido.Generico, "CANTIDADPDTE", 60, True, 11))
                relacionEntregasyConfiguracion.Add(New Campos("IMPORTEPDTE", TipoCampoPedido.Generico, "IMPORTEPDTE", 60, True, 12))
                relacionEntregasyConfiguracion.Add(New Campos("EST", TipoCampoPedido.Generico, "EST", 70, True, 13))
                relacionEntregasyConfiguracion.Add(New Campos("INSTANCIA_APROB", TipoCampoPedido.Generico, "INSTANCIA_APROB", 100, True, 14))
                relacionEntregasyConfiguracion.Add(New Campos("FECENTREGA", TipoCampoPedido.Generico, "FECENTREGA", 70, True, 15))
                relacionEntregasyConfiguracion.Add(New Campos("FECENTREGAPROVE", TipoCampoPedido.Generico, "FECENTREGAPROVE", 70, True, 16))
                relacionEntregasyConfiguracion.Add(New Campos("RECEPCIONES", TipoCampoPedido.Generico, "RECEPCIONES", 25, True, 17))
                relacionEntregasyConfiguracion.Add(New Campos("FRA", TipoCampoPedido.Generico, "FRA", 25, True, 18))
                relacionEntregasyConfiguracion.Add(New Campos("PAGO", TipoCampoPedido.Generico, "PAGO", 25, True, 19))
                If InvisibilidadBtBorrarLin Then
                    relacionEntregasyConfiguracion.Add(New Campos("DARBAJA", TipoCampoPedido.Generico, "DARBAJA", 0, False, 20))
                Else
                    relacionEntregasyConfiguracion.Add(New Campos("DARBAJA", TipoCampoPedido.Generico, "DARBAJA", 35, True, 0))
                End If
            Else
                relacionEntregasyConfiguracion.Add(New Campos("NUM_LINEA", TipoCampoPedido.Generico, "NUM_LINEA", 35, True, 0))
                relacionEntregasyConfiguracion.Add(New Campos("IDLINEA", TipoCampoPedido.Generico, "IDLINEA", 0, False, 1))
                relacionEntregasyConfiguracion.Add(New Campos("COD_ITEM", TipoCampoPedido.Generico, "COD_ITEM", 100, True, 2))
                relacionEntregasyConfiguracion.Add(New Campos("ART_DEN", TipoCampoPedido.Generico, "ARTDEN", 0, True, 3))
                relacionEntregasyConfiguracion.Add(New Campos("PREC", TipoCampoPedido.Generico, "PREC", 140, True, 4))
                relacionEntregasyConfiguracion.Add(New Campos("CANT", TipoCampoPedido.Generico, "CANT", 80, True, 5))
                relacionEntregasyConfiguracion.Add(New Campos("UNI", TipoCampoPedido.Generico, "UNI", 50, True, 6))
                relacionEntregasyConfiguracion.Add(New Campos("UP_DEF", TipoCampoPedido.Generico, "UP_DEF", 0, False, 7))
                relacionEntregasyConfiguracion.Add(New Campos("COSTES", TipoCampoPedido.Generico, "COSTES", 100, True, 8))
                relacionEntregasyConfiguracion.Add(New Campos("DESCUENTOS", TipoCampoPedido.Generico, "DESCUENTOS", 100, True, 9))
                relacionEntregasyConfiguracion.Add(New Campos("IMPBRUTO", TipoCampoPedido.Generico, "IMPBRUTO", 100, True, 10))
                relacionEntregasyConfiguracion.Add(New Campos("APROBAR", TipoCampoPedido.Generico, "APROBAR", 25, True, 11))
                relacionEntregasyConfiguracion.Add(New Campos("RECHAZAR", TipoCampoPedido.Generico, "RECHAZAR", 25, True, 12))
                If InvisibilidadBtBorrarLin Then
                    relacionEntregasyConfiguracion.Add(New Campos("DARBAJA", TipoCampoPedido.Generico, "DARBAJA", 0, False, 13))
                Else
                    relacionEntregasyConfiguracion.Add(New Campos("DARBAJA", TipoCampoPedido.Generico, "DARBAJA", 35, True, 0))
                End If
            End If
            Return relacionEntregasyConfiguracion
        End Get
    End Property
    Private ReadOnly Property Destinos() As CDestinos
        Get
            If _Destinos Is Nothing Then
                If HttpContext.Current.Cache("_Destinos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) Is Nothing Then
                    Persona.CargarDestinos(Me.Idioma)
                    _Destinos = Me.Persona.Destinos
                    If _Destinos IsNot Nothing Then Me.InsertarEnCache("_Destinos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString, _Destinos)
                Else
                    _Destinos = HttpContext.Current.Cache("_Destinos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
                End If
            End If

            Return _Destinos
        End Get
    End Property
    Private ReadOnly Property Persona() As CPersona
        Get
            If _Persona Is Nothing Then
                _Persona = FSNServer.Get_Object(GetType(FSNServer.CPersona))
                _Persona.Cod = Me.Usuario.CodPersona
                _Persona.CargarPersona()
            End If
            Return _Persona
        End Get
    End Property
    ''' <summary>
    ''' Direcciones de envío de facturas de todas las empresas.
    ''' Valor que se guarda en caché para su uso por todos los usuarios que lo requieran
    ''' </summary>
    Private ReadOnly Property DireccionesEnvioFacturas() As CEnvFacturaDirecciones
        Get
            Dim CDireccionesEnvioFacturas As CEnvFacturaDirecciones = Me.FSNServer.Get_Object(GetType(CEnvFacturaDirecciones))
            If HttpContext.Current.Cache("CDireccionesEnvioFacturas") Is Nothing Then
                CDireccionesEnvioFacturas.CargarDireccionesDeEnvioDeFactura()
                Me.InsertarEnCache("CDireccionesEnvioFacturas", CDireccionesEnvioFacturas)
            Else
                CDireccionesEnvioFacturas = HttpContext.Current.Cache("CDireccionesEnvioFacturas")
            End If
            Return CDireccionesEnvioFacturas
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que recupera el conjunto de las unidades de Pedido para su posterior uso en la página
    ''' </summary>
    Private ReadOnly Property TodasLasUnidades() As FSNServer.CUnidadesPedido
        Get
            Dim oUnidadesPedido As FSNServer.CUnidadesPedido = Me.FSNServer.Get_Object(GetType(FSNServer.CUnidadesPedido))
            If HttpContext.Current.Cache("TodasLasUnidades_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                oUnidadesPedido.CargarTodasLasUnidades(Me.Usuario.Idioma)
                Me.InsertarEnCache("TodasLasUnidades" & Me.Usuario.Idioma.ToString(), oUnidadesPedido)
            Else
                oUnidadesPedido = HttpContext.Current.Cache("TodasLasUnidades" & Me.Usuario.Idioma.ToString())
            End If
            Return oUnidadesPedido
        End Get
    End Property
    Private ReadOnly Property ProveAutofacturacion(ByVal CodProve As String) As Boolean
        Get
            If ViewState("AutofacProve" & CodProve) IsNot Nothing Then
                Return ViewState("AutofacProve" & CodProve)
            Else
                Dim oProve As CProveedor = FSNServer.Get_Object(GetType(CProveedor))
                oProve.Cod = CodProve
                oProve.getAutofactura()
                ViewState("AutofacProve" & CodProve) = oProve.Autofactura
                Return oProve.Autofactura
            End If
        End Get
    End Property
    Private ReadOnly Property Empresas() As CEmpresas
        Get
            If _Empresas Is Nothing Then
                If HttpContext.Current.Cache("_Empresas" & FSNUser.CodPersona) Is Nothing Then
                    Persona.CargarEmpresas(Me.PargenSM, FSNUser.PedidosOtrasEmp)
                    _Empresas = Me.Persona.Empresas

                    'Asociamos las direcciones de envio a las empresas del usuario
                    If Me.Acceso.gbDirEnvFacObl AndAlso _Empresas IsNot Nothing Then
                        For value As Integer = 0 To _Empresas.Count - 1
                            Dim empresa As CEmpresa
                            empresa = _Empresas(value)
                            If empresa.DireccionesEnvioFactura Is Nothing Then
                                Dim listaDirecciones As List(Of CEnvFacturaDireccion) = Me.DireccionesEnvioFacturas.FindAll(Function(direc As CEnvFacturaDireccion) direc.IdEmpresa = empresa.ID)
                                Dim direcciones As CEnvFacturaDirecciones = Me.FSNServer.Get_Object(GetType(CEnvFacturaDirecciones))
                                If listaDirecciones IsNot Nothing Then
                                    For Each dir As CEnvFacturaDireccion In listaDirecciones
                                        direcciones.Add(dir)
                                    Next
                                    empresa.DireccionesEnvioFactura = direcciones
                                End If
                            End If
                        Next
                    End If
                    Me.InsertarEnCache("_Empresas" & FSNUser.CodPersona, _Empresas)
                Else
                    _Empresas = HttpContext.Current.Cache("_Empresas" & FSNUser.CodPersona)
                End If
            End If

            Return _Empresas
        End Get
    End Property
    ''' <summary>
    ''' El botón Reabrir estará visible cuando el usuario tenga permiso de reabrir y el estado sea cerrado
    ''' Si la orden no es suya sino de su centro de coste: tiene que tener permiso para 
    ''' recepcionar ordenes de sus centros de coste
    ''' </summary>
    Private Shared ReadOnly Property MostrarBotonReabrir(ByVal Estado As Integer, ByVal Aprobador As String, ByVal Receptor As String) As Boolean
        Get
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim bPermisoReabrirOrdenes As Boolean = FSNUser.PermisoReabrirOrden
            Dim bEstadoCerrado As Boolean = (Estado = CPedido.Estado.Cerrado)
            Dim UsuarioAprovisionadorYReceptor As Boolean = (DBNullToStr(Aprobador) = FSNUser.CodPersona) AndAlso (DBNullToStr(Receptor) = FSNUser.CodPersona)
            Dim bPermisoRecepcionarPedidosCCImputables As Boolean = FSNUser.PermisoRecepcionarPedidosCCImputables
            Return bPermisoReabrirOrdenes And bEstadoCerrado AndAlso (UsuarioAprovisionadorYReceptor OrElse bPermisoRecepcionarPedidosCCImputables)
        End Get
    End Property
    ''' <summary>
    ''' El botón Recepcionar estará visible cuando el aprovisionador del pedido sea el usuario.
    ''' </summary>
    Private Shared ReadOnly Property MostrarBotonAnular(ByVal Estado As Integer, ByVal Aprobador As String) As Boolean
        Get
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim UsuarioAprovisionador As Boolean = (Aprobador = FSNUser.CodPersona)
            Dim OrdenNoAnulada As Boolean = Not (Estado = CPedido.Estado.Anulado)
            Return UsuarioAprovisionador And OrdenNoAnulada
        End Get
    End Property
    ''' <summary>
    ''' El botón Cerrar estará visible cuando:
    ''' El usuario tenga permiso de cierre y el estado no sea cerrado
    ''' Si la orden no es suya sino de su centro de coste: tiene que tener permiso para 
    ''' recepcionar ordenes de sus centros de coste
    ''' </summary>
    Private Shared ReadOnly Property MostrarBotonCerrar(ByVal Estado As Integer, ByVal Aprobador As String, ByVal Receptor As String) As Boolean
        Get
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim bPermisoCerrarOrdenes As Boolean = FSNUser.PermisoCerrarOrden
            Dim bEstadoNoCerrado As Boolean = Not (Estado = CPedido.Estado.Cerrado)
            Dim OrdenNoAnulada As Boolean = (Estado <> CPedido.Estado.Anulado)
            Dim UsuarioAprovisionadorYReceptor As Boolean = (Aprobador = FSNUser.CodPersona) AndAlso (Receptor = FSNUser.CodPersona)
            Dim bPermisoRecepcionarPedidosCCImputables As Boolean = FSNUser.PermisoRecepcionarPedidosCCImputables
            Return bPermisoCerrarOrdenes AndAlso bEstadoNoCerrado AndAlso (UsuarioAprovisionadorYReceptor OrElse bPermisoRecepcionarPedidosCCImputables) AndAlso OrdenNoAnulada
        End Get
    End Property
    ''' <summary>
    ''' El botón Recepcionar estará visible cuando :
    '''     El pedido esté recibido parcialmente o aceptado por el proveedor 
    '''     Y al mismo tiempo receptor y aprovisionador coincidan con el usuario o el usuario tenga permiso para recepcionar pedidos 
    '''     Y al mismo tiempo no tenga todas las líneas definidas para ser recepcionadas de forma automática
    ''' LA INFORMACIÓN PARA MOSTRAR ESTE BOTON DEPENDÍA DE LA ORDEN HASTA QUE SE AÑADIÓ EL TEMA DE LAS RECEPCIONES AUTOMÁTICAS
    ''' AHORA, SE TIENEN QUE CUMPLIR LAS CONDICIONES A NIVEL DE ORDEN Y A NIVEL DE LÍNEA INDICADAS ARRIBA
    ''' PERO PARA SABER LA INFORMACIÓN DE LAS LÍNEAS EN EL MOMENTO EN QUE SE USA ESTA PROPIEDAD EN EL ITEMDATABOUND DE LAS ÓRDENES
    ''' HABRÍA QUE HACER UNA NUEVA CONSULTA A BDD PARA CADA ORDEN Y DEVOLVER EL CAMPO IM_RECEPAUTO DE SUS LÍNEAS
    ''' SIN EMBARGO, TAMBIÉN PODEMOS OPTAR POR CREAR UNA PROPIEDAD CON EL MISMO NOMBRE QUE CONTROLE EL VALOR DE LAS LÍNEAS SÓLO CUANDO REALMENTE LAS MOSTRAMOS,
    ''' O SEA, EN EL CARGARLINEAS().
    ''' POR ESO HAY DOS PROPIEDADES CON EL MISMO NOMBRE Y DIFERENTE FIRMA.
    ''' LAS DOS DEBEN EJECUTARSE SUCESIVAMENTE (PERO EN DIFERENTES EVENTOS) PARA OBTENER EL VALOR DEFINITIVO DE VISIBILIDAD DEL BOTÓN RECEPCIONAR
    ''' </summary>
    Private Shared ReadOnly Property MostrarBotonRecepcionar(ByVal Estado As Integer, ByVal Aprobador As String, ByVal Receptor As String, ByVal bAcepProve As Boolean) As Boolean
        Get
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim cEstado As CPedido.Estado = CType(Estado, CPedido.Estado)
            Dim RecibidoParcialmenteOAceptadoPorElProveedor = (cEstado = CPedido.Estado.RecibidoParcialmente OrElse (cEstado = CPedido.Estado.AceptadoPorElProveedor Or (cEstado = CPedido.Estado.EmitidoAlProveedor And Not bAcepProve)) Or cEstado = CPedido.Estado.EnCamino)
            Dim UsuarioAprovisionadorYReceptor As Boolean = (Aprobador = FSNUser.CodPersona) AndAlso (Receptor = FSNUser.CodPersona)
            Dim bPermisoRecepcionarPedidosCCImputables As Boolean = FSNUser.PermisoRecepcionarPedidosCCImputables
            Return RecibidoParcialmenteOAceptadoPorElProveedor _
                AndAlso (UsuarioAprovisionadorYReceptor OrElse bPermisoRecepcionarPedidosCCImputables)
        End Get
    End Property
    ''' <summary>
    ''' El botÃ³n Reemitir estarÃ¡ visible cuando el estado sea denegado parcialmente
    ''' </summary>
    Private Shared ReadOnly Property MostrarBotonReemitir(ByVal Estado As Integer) As Boolean
        Get
            Dim bEstadoDenegadoParcial As Boolean = (Estado = CPedido.Estado.DenegadoParcialmente)
            Return bEstadoDenegadoParcial
        End Get
    End Property


#End Region
    ''' <summary>
    ''' Estructura compuesta por 5 elementos q nos va a permitir almacenar:
    ''' 1. El nombre del campo en el datatable de entregas (DsetPedidos.Tables("ORDENES"))
    ''' 2. Tipo de campo (Campo personalizado, Centro de Coste, Partida Presupuestaria o Genérico para todos los demás)
    ''' 3. ID que usaremos para guardar el campo en la tabla EP_CONF_VISOR_RECEPCIONES
    ''' 4. Si es visible 
    ''' 5. Su anchura.
    ''' 6. Su posición
    ''' </summary>
    Private Structure Campos
        Public CampoEntrega As String
        Public TipoCampoPedido As TipoCampoPedido
        Public CampoEntregaIDGuardado As String
        Public Visible As Boolean
        Public Width As Double
        Public Position As Integer

        ''' <summary>
        ''' Método para crear una nueva instancia de la estructura Campos
        ''' </summary>
        ''' <param name="sCampoEntrega">nombre del campo en el datatable de entregas (DatasetEntregas.Tables("ORDENES"))</param>
        ''' <param name="iTipoCampoPedido">Tipo de campo</param>
        ''' <param name="sCampoEntregaIDGuardado">Nombre del campo con el que se guardarán los datos en la tabla que contiene los datos de configuración</param>
        ''' <remarks>Llamada desde New. Máx. 0,1 seg.</remarks>
        Sub New(ByVal sCampoEntrega As String, ByVal iTipoCampoPedido As TipoCampoPedido, ByVal sCampoEntregaIDGuardado As String)
            Me.CampoEntrega = sCampoEntrega
            Me.TipoCampoPedido = iTipoCampoPedido
            Me.CampoEntregaIDGuardado = sCampoEntregaIDGuardado
        End Sub

        ''' Revisado por: blp. Fecha: 27/08/2012
        ''' <summary>
        ''' Método para crear una nueva instancia de la estructura Campos
        ''' </summary>
        ''' <param name="sCampoEntrega">nombre del campo en el datatable de entregas (DatasetEntregas.Tables("ORDENES"))</param>
        ''' <param name="iTipoCampoPedido">Tipo de campo</param>
        ''' <param name="sCampoEntregaIDGuardado">Nombre del campo con el que se guardarán los datos en la tabla que contiene los datos de configuración</param>
        ''' <param name="dblWidth">ancho del campo</param>
        ''' <remarks>Llamada desde New. Máx. 0,1 seg.</remarks>
        Sub New(ByVal sCampoEntrega As String, ByVal iTipoCampoPedido As TipoCampoPedido, ByVal sCampoEntregaIDGuardado As String, ByVal dblWidth As Double)
            Me.CampoEntrega = sCampoEntrega
            Me.TipoCampoPedido = iTipoCampoPedido
            Me.CampoEntregaIDGuardado = sCampoEntregaIDGuardado
            Me.Width = dblWidth
        End Sub

        ''' Revisado por: blp. Fecha: 27/08/2012
        ''' <summary>
        ''' Método para crear una nueva instancia de la estructura Campos
        ''' </summary>
        ''' <param name="sCampoEntrega">nombre del campo en el datatable de entregas (DatasetEntregas.Tables("ORDENES"))</param>
        ''' <param name="iTipoCampoPedido">Tipo de campo</param>
        ''' <param name="sCampoEntregaIDGuardado">Nombre del campo con el que se guardarán los datos en la tabla que contiene los datos de configuración</param>
        ''' <param name="dblWidth">ancho del campo</param>
        ''' <param name="bVisible">Es visible</param>
        ''' <remarks>Llamada desde New. Máx. 0,1 seg.</remarks>
        Sub New(ByVal sCampoEntrega As String, ByVal iTipoCampoPedido As TipoCampoPedido, ByVal sCampoEntregaIDGuardado As String, ByVal dblWidth As Double, ByVal bVisible As Boolean)
            Me.CampoEntrega = sCampoEntrega
            Me.TipoCampoPedido = iTipoCampoPedido
            Me.CampoEntregaIDGuardado = sCampoEntregaIDGuardado
            Me.Width = dblWidth
            Me.Visible = bVisible
        End Sub

        ''' Revisado por: blp. Fecha: 27/08/2012
        ''' <summary>
        ''' Método para crear una nueva instancia de la estructura Campos
        ''' </summary>
        ''' <param name="sCampoEntrega">nombre del campo en el datatable de entregas (DatasetEntregas.Tables("ORDENES"))</param>
        ''' <param name="iTipoCampoPedido">Tipo de campo</param>
        ''' <param name="sCampoEntregaIDGuardado">Nombre del campo con el que se guardarán los datos en la tabla que contiene los datos de configuración</param>
        ''' <param name="dblWidth">ancho del campo</param>
        ''' <param name="bVisible">Es visible</param>
        ''' <param name="iPosition">posición del campo</param>
        ''' <remarks>Llamada desde New. Máx. 0,1 seg.</remarks>
        Sub New(ByVal sCampoEntrega As String, ByVal iTipoCampoPedido As TipoCampoPedido, ByVal sCampoEntregaIDGuardado As String, ByVal dblWidth As Double, ByVal bVisible As Boolean, ByVal iPosition As Integer)
            Me.CampoEntrega = sCampoEntrega
            Me.TipoCampoPedido = iTipoCampoPedido
            Me.CampoEntregaIDGuardado = sCampoEntregaIDGuardado
            Me.Width = dblWidth
            Me.Visible = bVisible
            Me.Position = iPosition
        End Sub
    End Structure
#Region "TipoCampoPedido"
    <Serializable()>
    Public Enum TipoCampoPedido As Integer
        Generico = 1
        CampoPersonalizado = 2
        CentroCoste = 3
        PartidaPresupuestaria = 4
    End Enum
#End Region
    ''' <summary>
    ''' Carga la pagina
    ''' </summary>
    ''' <param name="sender">pagina</param>
    ''' <param name="e">evento</param>
    ''' <remarks>Llamada desde: Seguimiento.aspx.vb. Máx 0 seg</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
        m_sNoAplica = Me.Textos(272)
        m_idOrdenPedAbierto = 0
        CargarTextos()
        Desde = Request("Desde")
        InstanciaAprob = Request("Instancia")
        HttpContext.Current.Session("idOrdenPedidoAbierto") = ""
        Dim bFSFAActivado As Boolean = Me.Acceso.g_bAccesoFSFA
        'Visibilidad de los campos de proveedores
        If Not FSNServer.TipoAcceso.gbCampo1 Then
            lblCampo1.Style.Add("visibility", "hidden")
            IdCampo1.Style.Add("visibility", "hidden")
        End If

        If Not FSNServer.TipoAcceso.gbCampo2 Then
            lblCampo2.Style.Add("visibility", "hidden")
            IdCampo2.Style.Add("visibility", "hidden")
        End If

        If Not FSNServer.TipoAcceso.gbCampo3 Then
            lblCampo3.Style.Add("visibility", "hidden")
            IdCampo3.Style.Add("visibility", "hidden")
        End If

        If Not FSNServer.TipoAcceso.gbCampo4 Then
            lblCampo4.Style.Add("visibility", "hidden")
            IdCampo4.Style.Add("visibility", "hidden")
        End If

        If Not IsPostBack Then
            CargarScripts()
            If Desde = 1 Then
                ConfigurarCabeceraMenuAprobacionLimitePedido()
                CargarDatosSolicitudDePedido()
            ElseIf Desde = 2 Then
                ConfigurarCabeceraMenuAprobacionLimiteAdjudicacion()
            ElseIf Desde = 3 Then
                ConfigurarCabeceraMenuAprobacionSeguimiento()
            ElseIf Desde = 4 Then
                ConfigurarCabeceraMenuAprobacionLimitePedido()
                CargarDatosSolicitudDePedido()
            End If

            CargarwhdgArticulos(False)

            If Me.Acceso.gbUsarPedidosAbiertos Then
                'Una vez cargados los datos de lineas ya podemos acceder datos como la orden de pedido abierto
                'Este dato está almacenado a nivel de linea, sin embargo de momento todas las lineas de un pedido contraabierto
                'van a proceder de un mismo pedido abierto y además según Tarea 3497 vamos a mostrar el dato en la cabecera.
                'Nos quedamos por tanto con la primera linea del pedido.
                Dim dt As DataTable = HttpContext.Current.Cache("DsetEmisionPedidos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString() & "_" & Request.QueryString("ID")).tables(0)
                If dt.Rows.Count > 0 Then
                    Me.FSNLinkFilaPedidoAbierto.ContextKey = DBNullToStr(dt.Rows(0)("ORDEN_PED_ABIERTO"))
                    HttpContext.Current.Session("idOrdenPedidoAbierto") = DBNullToInteger(dt.Rows(0)("ORDEN_PED_ABIERTO"))
                End If
            End If
        Else
            Select Case Request("__EVENTTARGET")
                Case btnOcultoRecep.ClientID
                    Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
                    lblTituloRecep.Text = Textos(159)
                    lblError.Text = ""

                    litLinCodArticulo.Text = Textos(41)
                    litLinCantidadTotalPedida.Text = Textos(193)
                    litLinPrecUni.Text = Textos(65)

                    'Atributo con la linea de pedido y aprovador que luego usaremos para recargar la modal de recepciones
                    'si se elimina alguna de las recepciones
                    Dim arrAux() As String
                    arrAux = Split(Request("__EVENTARGUMENT"), "#")

                    'Pasamos al panel de recepciones el estado de la orden
                    Dim oRecepciones As CRecepciones = Me.FSNServer.Get_Object(GetType(FSNServer.CRecepciones))
                    oRecepciones = oRecepciones.CargarRecepciones(CType(Request("__EVENTARGUMENT"), Integer), bFSFAActivado, Me.Usuario.Idioma)
                    lblNumPedido.Visible = True
                    lblRefFactura.Visible = True
                    CargarRecepciones(oRecepciones)

                    updpnlPopupRecep.Update()
                    mpeRecepciones.Show()
                    ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
                Case btnOcultoFra.ClientID
                    Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
                    lblTituloFacturas.Text = Textos(146)
                    Dim Linea As String = CType(Split(Request("__EVENTARGUMENT"), "#")(1), Integer)

                    lblTituloFacturasLin.Text = Textos(147) & " " & Right("000" & Linea, 3)
                    PanelTituloFacturasLin.Update()

                    Dim oFacturas As CFacturas = Me.FSNServer.Get_Object(GetType(FSNServer.CFacturas))
                    oFacturas = oFacturas.CargarFacturas(CType(Usuario.Idioma, FSNLibrary.Idioma), CType(Split(Request("__EVENTARGUMENT"), "#")(0), Integer))

                    pnlFacturasLin.Visible = True
                    grdFacturas.DataSource = oFacturas
                    grdFacturas.DataBind()
                    updpnlPopupFacturas.Update()
                    mpeFactura.Show()
                    ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
                Case btnOcultoPagos.ClientID
                    Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
                    lblTituloPagos.Text = Textos(149)

                    Dim oPagos As CPagos = Me.FSNServer.Get_Object(GetType(FSNServer.CPagos))
                    oPagos = oPagos.CargarPagos(CType(Usuario.Idioma, FSNLibrary.Idioma), CType(Request("__EVENTARGUMENT"), Integer))

                    pnlPagosLin.Visible = True
                    grdPagos.DataSource = oPagos
                    grdPagos.DataBind()
                    updpnlPopupPagos.Update()
                    mpePagos.Show()
                    ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
                Case btnOcultoAnularLinea.ClientID
                    Dim oRecepcion As CRecepcion = Me.FSNServer.Get_Object(GetType(FSNServer.CRecepcion))
                    Dim sIdLineaPedYLinRecep() As String = Split(Request("__EVENTARGUMENT"), "###")
                    Dim idLineaPedido As Integer = CType(sIdLineaPedYLinRecep(0), Integer)
                    Dim idLineaRecep As Integer = CType(sIdLineaPedYLinRecep(1), Integer)
                    Dim rdoEliminacion As Integer

                    rdoEliminacion = oRecepcion.EliminarLineaDeRecepcion(idLineaRecep, Me.Usuario.Cod)

                    Dim oRecepciones As CRecepciones = Me.FSNServer.Get_Object(GetType(FSNServer.CRecepciones))
                    oRecepciones = oRecepciones.CargarRecepciones(idLineaPedido, bFSFAActivado, Me.Usuario.Idioma)
                    If Not oRecepciones Is Nothing Then
                        'Queda alguna linea en la recepcion para esa linea de pedido
                        lblNumPedido.Visible = True
                        lblRefFactura.Visible = True
                        CargarRecepciones(oRecepciones)
                        mpeRecepciones.Show()
                        ScriptManager.RegisterStartupScript(Me, Me.GetType, "ActualizarImagenRecepcion", "ActualizarImagenRecepcion(" & idLineaPedido & ",'" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Page.Theme & "/images/recepcion.gif');", True)
                    Else
                        ScriptManager.RegisterStartupScript(Me, Me.GetType, "ActualizarImagenRecepcion", "ActualizarImagenRecepcion(" & idLineaPedido & ",'" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Page.Theme & "/images/recepcion_desactivado.gif');", True)
                    End If
                Case btnOcultoAnularRecepcion.ClientID
                    Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
                    Dim oRecepciones As CRecepciones = Me.FSNServer.Get_Object(GetType(FSNServer.CRecepciones))
                    Dim sAlbaranFechaProve() As String = Split(Request("__EVENTARGUMENT"), "###")
                    Dim sAlbaran As String = sAlbaranFechaProve(0)
                    Dim sFechaAlbaran As Date = CType(FormatDateTime(CType(sAlbaranFechaProve(1), DateTime), DateFormat.ShortDate), DateTime)
                    Dim sCodProve As String = sAlbaranFechaProve(2)
                    Dim idLineaPedido As Integer = CType(sAlbaranFechaProve(3), Integer)
                    Dim rdoEliminacion As String
                    rdoEliminacion = oRecepciones.EliminarRecepcionesAlbaran(sAlbaran, sFechaAlbaran, sCodProve, Me.Acceso.giRecepAprov, Me.Acceso.giRecepDirecto, Me.Usuario.CodPersona, Me.Usuario.Cod, CType(Me.Usuario.Idioma, FSNLibrary.Idioma), Me.Acceso)

                    Dim rdosEliminacion() As String = Split(rdoEliminacion, TextosSeparadores.dosArrobas)
                    Dim todoOK As Boolean = True
                    Dim todoKO As Boolean = True
                    For i As Integer = 0 To rdosEliminacion.Length - 1
                        If rdosEliminacion(i) <> "0" Then
                            todoOK = False
                        Else
                            todoKO = False
                        End If
                    Next
                    If todoOK Then
                        'MOSTRAR MENSAJE DE QUE LA RECEPCIÓN SE HA ANULADO CORRECTAMENTE
                        lblError.Text = ""
                        dlLineasRecepciones.Visible = False
                        lblTituloRecep.Visible = False
                        lblError.Text = "<br /><br />" & sAlbaran & "<br />" & Textos(243) 'Albarán anulado correctamente
                        lblNumPedido.Visible = False
                        lblRefFactura.Visible = False
                        updpnlPopupRecepCabecera.Update()
                    Else
                        'MOSTRAR MENSAJE DE ERROR
                        If todoKO Then
                            lblError.Text = Textos(245) 'No se ha anulado ninguna de las recepciones vinculadas al albarán. Los motivos son:
                        Else
                            lblError.Text = Textos(244) 'No se han podido anular todas las recepciones vinculadas al albarán. Los motivos son:
                        End If
                        For Each rdo As String In rdosEliminacion
                            Select Case rdo
                                Case "1"
                                    lblError.Text += "<br /> - " & Textos(239) 'Se ha producido un error. Por favor, inténtelo de nuevo más tarde y si el problema persiste, contacte con el soporte técnico.
                                Case "0"
                                Case Else
                                    lblError.Text += "<br /> - " & Textos(rdo) 'Al eliminar albarán, la función devuelve una serie de números que coinciden con los índices de los textos de error de Seguimiento.
                            End Select
                        Next
                    End If

                    'Actualizar el updatepanel de recepción para mostrar el resultado
                    updpnlPopupRecep.Update()
                    mpeRecepciones.Show()
                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "ActualizarImagenRecepcion", "ActualizarImagenRecepcion(" & idLineaPedido & ",'" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Page.Theme & "/images/recepcion_desactivado.gif');", True)
                    ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
                Case btnBloquearFacturacionAlbaran.ClientID, btnDesbloquearFacturacionAlbaran.ClientID
                    Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
                    Dim oRecepcion As CRecepcion = FSNServer.Get_Object(GetType(FSNServer.CRecepcion))
                    Dim rdoBloqueoAlbaran As Integer
                    Dim sParametros() As String = Split(Request("__EVENTARGUMENT"), "###")
                    Dim sAlbaran As String = sParametros(0)
                    Dim iLineaPedido As Integer = strToInt(sParametros(1))
                    Dim iIdOrden As Integer = strToInt(sParametros(2))
                    Dim txtConfirm As String = sParametros(3)
                    'BLOQUEO o DESBLOQUEO
                    If Request("__EVENTTARGET") = btnBloquearFacturacionAlbaran.ClientID Then
                        rdoBloqueoAlbaran = oRecepcion.BloquearFacturacionAlbaran(sAlbaran, iIdOrden, txtConfirm)
                    Else
                        rdoBloqueoAlbaran = oRecepcion.DesBloquearFacturacionAlbaran(sAlbaran, iIdOrden)
                    End If

                    If rdoBloqueoAlbaran <> 0 Then
                        'MOSTRAR MENSAJE DE ERROR
                        'Este error se muestra cuando el stored no devuelve 0 (=ok). Eso ocurre cuando no se ha actualizado ningún registro en bdd
                        lblError.Text = Textos(239) 'Se ha producido un error. Por favor, inténtelo de nuevo más tarde y si el problema persiste, contacte con el soporte técnico.
                        updpnlPopupRecep.Update()
                        mpeRecepciones.Show()
                    Else
                        lblError.Text = ""
                        dlLineasRecepciones.Visible = False
                        lblTituloRecep.Visible = False
                        updpnlPopupRecep.Update()
                        updpnlPopupRecepCabecera.Update()

                        'ACTUALIZAR Recepciones
                        Dim oRecepciones As CRecepciones = FSNServer.Get_Object(GetType(FSNServer.CRecepciones))
                        oRecepciones = oRecepciones.CargarRecepciones(iLineaPedido, bFSFAActivado, Usuario.Idioma)
                        CargarRecepciones(oRecepciones)
                        mpeRecepciones.Show()
                    End If
                    ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
                Case btnExportarExcel.ClientID
                    Dim dt As New DataTable
                    dt.Columns.Add("ID", GetType(Integer))
                    dt.Rows.Add(CType(Request("__EVENTARGUMENT"), Integer))
                    InsertarEnCache("dsReportEP" & Usuario.Cod, dt)
                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "Exportar_ViewerSinMenu", "window.open('InformeExcelOrdenes.aspx' ,'_blank','height=300,width=300,menubar=no,scrollbars=no,resizable=no,toolbar=no,addressbar=no');", True)
            End Select
        End If

        Dim ModalProgressClientID As String = CType(Master.Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalClientID", "<script>var ModalProgress = '" & ModalProgressClientID & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bMostrarCodigoProve", "<script>var bMostrarCodigoProve = " & LCase(FSNUser.MostrarCodProve) & " </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bMostrarCodigoArt", "<script>var bMostrarCodigoArt = " & LCase(FSNUser.MostrarCodArt) & " </script>")

        If Request.QueryString("Desde") IsNot Nothing Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Desde",
                "<script>var Desde=" & CType(Request.QueryString("Desde"), Integer) & ";</script>")
        End If
        If Request.QueryString("aprobador") IsNot Nothing Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "aprobador",
                "<script>var aprobador='" & DBNullToStr(Request.QueryString("aprobador")) & "';</script>")
        End If
        If Request.QueryString("receptor") IsNot Nothing Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "receptor",
                "<script>var receptor='" & DBNullToStr(Request.QueryString("receptor")) & "';</script>")
        End If
        If Request.QueryString("vpu") IsNot Nothing Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "vpu",
                "<script>var vpu='" & CBool(Request.QueryString("vpu")).ToString & "';</script>")
        End If
        If Request.QueryString("vpou") IsNot Nothing Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "vpou",
                "<script>var vpou='" & CBool(Request.QueryString("vpou")).ToString & "';</script>")
        End If
        If Request.QueryString("acepProve") IsNot Nothing Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "acepProve",
                "<script>var acepProve='" & DBNullToStr(Request.QueryString("acepProve")) & "';</script>")
        End If
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mpeRecepciones",
            "<script>var mpeRecepciones='" & mpeRecepciones.ClientID & "';</script>")
    End Sub
    ''' Revisado por: blp. Fecha:18/06/2012
    ''' <summary>
    ''' Funcion que carga las recepciones en el popup de Recepciones que se carga al pinchar desde Seguimiento
    ''' </summary>
    ''' <param name="oRecepciones">objeto cRecepciones con las recepciones para esa linea de pedido y ese articulo</param>
    ''' <remarks>Llamada desde: Seguimiento.aspx.vb. MÃ¡x 0,3 seg</remarks>
    Private Sub CargarRecepciones(ByVal oRecepciones As CRecepciones)
        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
        'Cargamos la cabecera
        lblNumPedido.Text = Textos(221).Replace("XXXX", oRecepciones(0).NumPedido.ToString)
        If HayIntegracionRecep AndAlso oRecepciones(0).NumERP IsNot Nothing Then
            lblRefFactura.Visible = True
            Dim oCParametros As FSNServer.CParametros = FSNServer.Get_Object(GetType(FSNServer.CParametros))
            lblRefFactura.Text = oCParametros.CargarLiteralParametros(TiposDeDatos.LiteralesParametros.PedidoERP, Usuario.Idioma) & ": " & oRecepciones(0).NumERP.ToString
        Else
            lblRefFactura.Visible = False
        End If
        litItemCodArticulo.Text = oRecepciones(0).CodArt
        litItemDenArticulo.Text = oRecepciones(0).DenArt
        If oRecepciones(0).TipoRecepcion = 0 Then
            litItemCantidad.Text = FSNLibrary.FormatNumber(oRecepciones(0).CantPedida, Me.Usuario.NumberFormat)
            FSNlnkUniCantPdte.Text = oRecepciones(0).Unidad
            FSNlnkUniCantPdte.ContextKey = oRecepciones(0).DenUnidad
            lblPrecUni.Text = FSNLibrary.FormatNumber(oRecepciones(0).PrecioUnitario, Me.Usuario.NumberFormat)
            lblMonPrecUni.Text = oRecepciones(0).Moneda
            litLinImporteTotalPedido.Visible = False
            lblItemImportePed.Visible = False
            lblMonPrecImportePed.Visible = False
        Else
            litLinImporteTotalPedido.Text = Textos(266)
            lblItemImportePed.Text = FSNLibrary.FormatNumber(oRecepciones(0).ImportePedido, Me.Usuario.NumberFormat)
            lblMonPrecImportePed.Text = oRecepciones(0).Moneda
            litLinCantidadTotalPedida.Visible = False
            litLinPrecUni.Visible = False
            litItemCantidad.Visible = False
            FSNlnkUniCantPdte.Visible = False
            FSNlnkUniCantPdte.Visible = False
            lblPrecUni.Visible = False
            lblMonPrecUni.Visible = False
        End If
        'btnAceptarConfirm.OnClientClick = "$find('" & mpeRecepciones.ClientID & "').hide(); return false"
        'Cargamos las lineas
        dlLineasRecepciones.DataSource = oRecepciones
        dlLineasRecepciones.DataBind()
        dlLineasRecepciones.Visible = True
        updpnlPopupRecep.Update()
        updpnlPopupRecepCabecera.Update()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
    End Sub
#Region "Integracion"
    ''' <summary>
    ''' Integracion de recepciones
    ''' </summary>
    Private ReadOnly Property HayIntegracionRecep() As Boolean
        Get
            If Session("HayIntegracionRecep") Is Nothing Then
                Session("HayIntegracionRecep") = False
                Dim oEP_ValidacionesIntegracion As EP_ValidacionesIntegracion = Me.FSNServer.Get_Object(GetType(EP_ValidacionesIntegracion))
                If oEP_ValidacionesIntegracion.HayIntegracion(TablasIntegracion.Rec_Aprov) OrElse oEP_ValidacionesIntegracion.HayIntegracion(TablasIntegracion.Rec_Directo) Then
                    Session("HayIntegracionRecep") = True
                End If
            End If
            If CBool(Session("HayIntegracionRecep")) Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property
    ''' <summary>
    ''' Integracion de pedidos
    ''' </summary>
    Private ReadOnly Property HayIntegracionPedidos() As Boolean
        Get
            If Session("HayIntegracionPedidos") Is Nothing Then
                Session("HayIntegracionPedidos") = False
                Dim oEP_ValidacionesIntegracion As EP_ValidacionesIntegracion = Me.FSNServer.Get_Object(GetType(EP_ValidacionesIntegracion))
                If oEP_ValidacionesIntegracion.HayIntegracion(TablasIntegracion.PED_Aprov) Then
                    Session("HayIntegracionPedidos") = True
                End If
            End If
            If CBool(Session("hayIntegracionPedidos")) Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property
#End Region
    ''' <summary>
    ''' Actualiza el dataset de pedidos.
    ''' </summary>
    ''' <remarks>Se produce cuando el control de servidor se carga en el objeto Page. Tiempo: depende del nº de datos</remarks>
    Private Function ActualizarDsetpedidos(Optional ByVal MasResultadodos As Boolean = False) As DataSet
        Dim dsLineasPedido As New DataSet
        Dim idPedido As String = Request.QueryString("ID")

        dsLineasPedido = GenerarDsetPedidos(Me.Usuario.CodPersona, Me.Usuario.Idioma)

        Session("FilaQuitada") = False
        InsertarEnCache("DsetEmisionPedidos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString() & "_" & idPedido.ToString(), dsLineasPedido.Copy)
        Return dsLineasPedido
    End Function
    ''' <summary>
    ''' Generamos el conjunto de datos de la página.
    ''' He añadido el FiltrosAnyadidos a los parámetros para cambiar la firma y que no de errores aunque no era necesario
    ''' dado que FiltrosAnyadidos es una propiedad
    ''' </summary>
    ''' <param name="CodPersona">Código de la persona</param>
    ''' <param name="Idioma">idioma en el que devolver el dataset</param>
    ''' True-->Centros coste
    ''' False-->Usuario    ''' 
    ''' <returns>dataset con los pedidos</returns>
    ''' <remarks>Llamada desde GenerarDsetPedidos. Máx. depende del número de datos</remarks>
    Private Function GenerarDsetPedidos(ByVal CodPersona As String, ByVal Idioma As FSNLibrary.Idioma) As DataSet
        Dim idPedido As String = Request.QueryString("ID")
        Dim oOrdenes As COrdenes = FSNServer.Get_Object(GetType(FSNServer.COrdenes))

        Return oOrdenes.DevolverLineasOrden(CodPersona, Idioma.ToString(), idPedido, InstanciaAprob, Acceso.gbBajaLOGPedidos, (InstanciaAprob <> 0))
    End Function
    Private Sub CargarDatosSolicitudDePedido()
        pnlCabeceraAprobacion.Visible = True
        lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & oInstancia.Moneda
        lblCreadoPor.Text = oInstancia.NombrePeticionario
        lblEstado.Text = oInstancia.DenEtapaActual
        lblFecAlta.Text = "(" & FormatDate(oInstancia.FechaAlta, FSNUser.DateFormat) & ")"
        lblTipo.Text = oInstancia.Solicitud.DenTipo
        imgInfCreadoPor.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosPeticionario.AnimationClientID & "', event, '" & FSNPanelDatosPeticionario.DynamicPopulateClientID & "', '" & oInstancia.Peticionario & "'); return false;")
        imgInfTipo.Attributes.Add("onclick", "DetalleTipoSolicitud(" & oInstancia.Solicitud.ID & ")")
        imgInfCreadoPor.ImageUrl = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/info.gif"
        imgInfTipo.ImageUrl = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/info.gif"
        HyperDetalle.Attributes.Add("onclick", "VerFlujo(" & oInstancia.ID & ")")
        hid_IdOrden.Value = Request("idOrden")
        ConfigurarEstadoSolicitud()
    End Sub
    Private Sub ConfigurarEstadoSolicitud()
        Dim ModuloIdiomaAnt As FSNLibrary.TiposDeDatos.ModulosIdiomas = Me.ModuloIdioma
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.DetalleSeguimiento
        Dim sEtapaActual As String
        Select Case oInstancia.Estado
            Case TipoEstadoSolic.Guardada   'Guardada
                sEtapaActual = Textos(6)
            Case TipoEstadoSolic.Pendiente, TipoEstadoSolic.EnCurso  'En curso
                sEtapaActual = Textos(25)
            Case TipoEstadoSolic.SCPendiente, TipoEstadoSolic.SCAprobada, TipoEstadoSolic.SCRechazada, TipoEstadoSolic.SCAnulada, TipoEstadoSolic.SCCerrada
                sEtapaActual = Textos(26)
            Case TipoEstadoSolic.Rechazada  'Rechazada    
                sEtapaActual = Textos(9)
            Case TipoEstadoSolic.Anulada  'Anulada
                sEtapaActual = Textos(10)
            Case TipoEstadoSolic.SCCerrada  'Cerrada  
                sEtapaActual = Textos(11)
            Case Else
                sEtapaActual = oInstancia.CargarEstapaActual(Idioma, True)
                If sEtapaActual = "##" Then
                    sEtapaActual = Textos(36)
                Else
                    sEtapaActual = Textos(25)
                End If
        End Select
        ModuloIdioma = ModuloIdiomaAnt
        lblIDInstanciayEstado.Text = oInstancia.ID & " (" & sEtapaActual & ")"
    End Sub
    ''' <summary>
    ''' Configura la cabecera cuando se llega al detalle de pedido desde aprobacion porque se ha superado el limite de adjudicacion
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ConfigurarCabeceraMenuAprobacionLimiteAdjudicacion()
        With FSNPageHeaderAprobacion
            Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
            .UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/Factura.png"
            .TextoBotonVolver = Textos(287)
            .OnClientClickVolver = "window.location.href='" & ConfigurationManager.AppSettings("rutaFS") & "EP/Aprobacion.aspx" & IIf(Request("PaginaPaginacion") = Nothing, "", "?PaginaPaginacion=" & Request("PaginaPaginacion")) & IIf(Request("TotalRegistros") = Nothing, "", "&TotalRegistros=" & Request("TotalRegistros")) & "'; return false;"

            .VisibleBotonExcel = True
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_RecepcionPedidos
            .TextoBotonExcel = Textos(194)
            .OnClientClickExcel = "ExportarExcel(" & CType(Request.QueryString("Id"), Integer) & "); return false;"
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
            .TituloCabecera = Textos(259) & " " & Request("anyo") & "/" & Request("numped") & "/" & Request("numorden")
        End With
    End Sub
    ''' <summary>
    ''' Configura la cabecera cuando se llega al detalle de pedido desde la pagina de seguimiento, y dependiendo del estado de la orden mostrara unas opciones a realizar u otras
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ConfigurarCabeceraMenuAprobacionSeguimiento()
        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
        With FSNPageHeaderAprobacion
            .UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/Factura.png"
            .OnClientClickVolver = "window.location.href='" & System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "EP/Seguimiento.aspx" & IIf(Request("PaginaPaginacion") = Nothing, "", "?PaginaPaginacion=" & Request("PaginaPaginacion")) & IIf(Request("TotalRegistros") = Nothing, "", "&TotalRegistros=" & Request("TotalRegistros")) & "'; return false;"
            .TextoBotonVolver = Textos(287)
            .VisibleBotonEmitir = True
            .OcultarBotonEmitir = Not MostrarBotonReemitir(CType(Request.QueryString("estado"), Integer))
            .OnClientClickEmitir = "ReemitirOrden(); return false;"
            .TextoBotonEmitir = Textos(58)
            .VisibleBotonAnular = FSNUser.EPPermitirAnularPedidos
            .OcultarBotonAnular = Not MostrarBotonAnular(CType(Request.QueryString("estado"), Integer), DBNullToStr(Request.QueryString("aprobador")))
            .OnClientClickAnular = "AnularOrden(); return false;"
            .TextoBotonAnular = Textos(199)
            .VisibleBotonReabrir = True
            .OcultarBotonReabrir = Not MostrarBotonReabrir(CType(Request.QueryString("estado"), Integer), DBNullToStr(Request.QueryString("aprobador")), DBNullToStr(Request.QueryString("receptor")))
            .OnClientClickReabrir = "ReabrirOrden(); return false;"
            .TextoBotonReabrir = Textos(232)
            .VisibleBotonCierrePositivo = True
            .OcultarBotonCierrePositivo = Not MostrarBotonCerrar(CType(Request.QueryString("estado"), Integer), DBNullToStr(Request.QueryString("aprobador")), DBNullToStr(Request.QueryString("receptor")))
            .OnClientClickCierrePositivo = "CerrarOrden(); return false;"
            .TextoBotonCierrePositivo = Textos(230)
            .VisibleBotonRecepcionar = True
            .OcultarBotonRecepcionar = Not MostrarBotonRecepcionar(CType(Request.QueryString("estado"), Integer), DBNullToStr(Request.QueryString("aprobador")), DBNullToStr(Request.QueryString("receptor")), DBNullToBoolean(Request.QueryString("acepProve")))
            .OnClientClickRecepcionar = "Recepcionar(); return false;"
            .TextoBotonRecepcionar = Textos(61)
            .VisibleBotonExcel = True
            If FSNServer.TipoAcceso.gbBajaLOGPedidos AndAlso FSNUser.EPPermitirBorrarPedidos Then
                'La visibilidad del botÃ³n en funciÃ³n del tipo de pedido y su estado se hace en DetallePedido.js tras la carga de datos del pedido
                .VisibleBotonBorrar = True
                .TextoBotonBorrar = Textos(296)
                .OnClientClickBorrar = "BorrarOrden(); return false;"
            End If
            If FSNServer.TipoAcceso.gbBajaLOGPedidos AndAlso FSNUser.EPPermitirDeshacerBorradoPedidos Then
                'La visibilidad del botÃ³n en funciÃ³n del tipo de pedido y su estado se hace en DetallePedido.js tras la carga de datos del pedido
                .VisibleBotonDeshacerBorrar = True
                .TextoBotonDeshacerBorrar = Textos(302)
                .OnClientClickDeshacerBorrar = "DeshacerBorrarOrden(); return false;"
            End If
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_RecepcionPedidos
            .TextoBotonExcel = Textos(194)
            .OnClientClickExcel = "ExportarExcel(" & CType(Request.QueryString("Id"), Integer) & "); return false;"
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_AprobacionPedidos
            .TituloCabecera = Textos(171) & " " & Request("anyo") & "/" & Request("numped") & "/" & Request("numorden")

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_RecepcionPedidos
            .VisibleBotonEFacturaPDF = True
            .TextoBotonEFacturaPDF = Textos(195)
            .OnClientClickEFacturaPDF = "VerDetalleExp(" & CType(Request.QueryString("id"), Integer) & "," & CType(Request.QueryString("estado"), Integer) & ",'" & DBNullToStr(Request.QueryString("aprobador")) & "','" & DBNullToStr(Request.QueryString("receptor")) & "'); return false;"

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos

        End With

    End Sub
    ''' <summary>
    ''' Configura la cabecera, muestra en el menu las posibles acciones cuando se muestra el detalle de pedido viniendo desde la pantalla de aprobacion de pedidos por limite de pedido superado
    ''' Activamos los botones que pueden aparecer en la pantalla
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load; Tiempo máximo:=0,1seg.</remarks>
    Private Sub ConfigurarCabeceraMenuAprobacionLimitePedido()
        Dim tipoSolicitud As TipoDeSolicitud
        If Request("TipoPedido") = TipoDePedido.ContraPedidoAbierto Then
            tipoSolicitud = TipoDeSolicitud.SolicitudDePedidoContraAbierto
        Else
            tipoSolicitud = TipoDeSolicitud.SolicitudDePedidoCatalogo
        End If

        FSNPageHeaderAprobacion.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/Factura.png"

        FSNPageHeaderAprobacion.VisibleBotonExcel = True
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_RecepcionPedidos
        FSNPageHeaderAprobacion.TextoBotonExcel = Textos(194)
        FSNPageHeaderAprobacion.OnClientClickExcel = "ExportarExcel(" & CType(Request.QueryString("Id"), Integer) & "); return false;"

        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
        FSNPageHeaderAprobacion.VisibleBotonVolver = True
        FSNPageHeaderAprobacion.TextoBotonVolver = Textos(287)

        If Desde = 4 Then
            FSNPageHeaderAprobacion.OnClientClickVolver = "window.location.href='" & ConfigurationManager.AppSettings("rutaFS") & "EP/Aprobacion.aspx'; return false;"
        Else
            FSNPageHeaderAprobacion.OnClientClickVolver = "window.location.href='" & ConfigurationManager.AppSettings("rutaFS") & "EP/Aprobacion.aspx" & IIf(Request("PaginaPaginacion") = Nothing, "", "?PaginaPaginacion=" & Request("PaginaPaginacion")) & IIf(Request("TotalRegistros") = Nothing, "", "&TotalRegistros=" & Request("TotalRegistros")) & "'; return false;"
        End If

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
        FSNPageHeaderAprobacion.TituloCabecera = Textos(259) & " " & Request("anyo") & "/" & Request("numped") & "/" & Request("numorden")
        Dim oPopMenu As Infragistics.Web.UI.NavigationControls.WebDataMenu
        Dim oItem As Infragistics.Web.UI.NavigationControls.DataMenuItem

        If (Desde = 4) AndAlso Not (oInstancia.AprobadorActual = FSNUser.CodPersona) Then ' has usado link.
            'Puede Volver y exportar si NO eres aprobador.
            'Eoc. Es como no haber usado link
            Exit Sub
        End If

        Dim oRol As FSNServer.Rol
        oRol = FSNServer.Get_Object(GetType(FSNServer.Rol))
        If (Request("Bloque") <> Nothing) And (Request("Bloque") <> "") Then
            oRol.Id = oInstancia.RolActual
            oRol.Bloque = oInstancia.Etapa
            'else 
            'En Aprobacion existe la posibilidad de "Ver Pedidos pendientes de validaciÃ³n por aprobador superior".
            'Para cargar la pantalla se saca el rol/bloque del superior. NO el de la persona q esta conectado. 
        End If

        Dim oDSAcciones As DataSet = oRol.CargarAcciones(Idioma)

        oPopMenu = Me.wdmAcciones
        Dim blnAprobar As Boolean = False
        Dim blnRechazar As Boolean = False
        Dim iAccionesRestarOtrasAcciones As Integer
        Dim iOtrasAcciones As Integer
        If oDSAcciones.Tables.Count > 0 Then
            If oDSAcciones.Tables(0).Rows.Count > 1 Then
                'Comprobamos si entre las acciones hay de tipo aprobar y rechazar
                For Each oRow In oDSAcciones.Tables(0).Rows
                    If DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
                        blnAprobar = True
                        iAccionesRestarOtrasAcciones = iAccionesRestarOtrasAcciones + 1
                        If blnRechazar Then Exit For
                    ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
                        blnRechazar = True
                        iAccionesRestarOtrasAcciones = iAccionesRestarOtrasAcciones + 1
                        If blnAprobar Then Exit For
                    End If
                Next

                iOtrasAcciones = oDSAcciones.Tables(0).Rows.Count - iAccionesRestarOtrasAcciones
                Dim cont As Integer = 0
                For Each oRow In oDSAcciones.Tables(0).Rows
                    If iOtrasAcciones > 3 AndAlso DBNullToSomething(oRow.Item("RA_APROBAR")) <> 1 AndAlso DBNullToSomething(oRow.Item("RA_RECHAZAR")) <> 1 Then
                        If IsDBNull(oRow.Item("DEN")) Then
                            oItem = oPopMenu.Items.Add("&nbsp;")
                        Else
                            If oRow.Item("DEN") = "" Then
                                oItem = oPopMenu.Items.Add("&nbsp;")
                            Else
                                oItem = oPopMenu.Items.Add(DBNullToStr(oRow.Item("DEN")))
                            End If
                        End If
                        oItem.NavigateUrl = "javascript:EjecutarAccion(" + oInstancia.ID.ToString + " , " + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(DBNullToSomething(oRow.Item("CUMP_OBL_ROL")) = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("GUARDA")) = 1, "true", "false")) + "'," + CStr(oInstancia.InstanciaBloqueBloq) + ",'" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'," + CStr(tipoSolicitud) + ")"
                    ElseIf DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
                        FSNPageHeaderAprobacion.OnClientClickAprobar = "EjecutarAccion(" + oInstancia.ID.ToString + " , " + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(DBNullToSomething(oRow.Item("CUMP_OBL_ROL")) = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("GUARDA")) = 1, "true", "false")) + "'," + CStr(oInstancia.InstanciaBloqueBloq) + ",'" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'," + CStr(tipoSolicitud) + "); return false;"
                        FSNPageHeaderAprobacion.TextoBotonAprobar = DBNullToStr(oRow.Item("DEN"))
                    ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
                        FSNPageHeaderAprobacion.OnClientClickRechazar = "EjecutarAccion(" + oInstancia.ID.ToString + " , " + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(DBNullToSomething(oRow.Item("CUMP_OBL_ROL")) = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("GUARDA")) = 1, "true", "false")) + "'," + CStr(oInstancia.InstanciaBloqueBloq) + ",'" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'," + CStr(tipoSolicitud) + "); return false;"
                        FSNPageHeaderAprobacion.TextoBotonRechazar = DBNullToStr(oRow.Item("DEN"))
                    Else
                        If cont = 0 Then
                            FSNPageHeaderAprobacion.OnClientClickAccion1 = "EjecutarAccion(" + oInstancia.ID.ToString + " , " + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(DBNullToSomething(oRow.Item("CUMP_OBL_ROL")) = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("GUARDA")) = 1, "true", "false")) + "'," + CStr(oInstancia.InstanciaBloqueBloq) + ",'" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'," + CStr(tipoSolicitud) + "); return false;"
                            FSNPageHeaderAprobacion.TextoBotonAccion1 = DBNullToStr(oRow.Item("DEN"))
                            FSNPageHeaderAprobacion.VisibleBotonAccion1 = True
                            cont = 1
                        ElseIf cont = 1 Then
                            FSNPageHeaderAprobacion.OnClientClickAccion2 = "EjecutarAccion(" + oInstancia.ID.ToString + " , " + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(DBNullToSomething(oRow.Item("CUMP_OBL_ROL")) = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("GUARDA")) = 1, "true", "false")) + "'," + CStr(oInstancia.InstanciaBloqueBloq) + ",'" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'," + CStr(tipoSolicitud) + "); return false;"
                            FSNPageHeaderAprobacion.TextoBotonAccion2 = DBNullToStr(oRow.Item("DEN"))
                            FSNPageHeaderAprobacion.VisibleBotonAccion2 = True
                            cont = 2
                        Else
                            FSNPageHeaderAprobacion.OnClientClickAccion3 = "EjecutarAccion(" + oInstancia.ID.ToString + " , " + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(DBNullToSomething(oRow.Item("CUMP_OBL_ROL")) = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("GUARDA")) = 1, "true", "false")) + "'," + CStr(oInstancia.InstanciaBloqueBloq) + ",'" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'," + CStr(tipoSolicitud) + "); return false;"
                            FSNPageHeaderAprobacion.TextoBotonAccion3 = DBNullToStr(oRow.Item("DEN"))
                            FSNPageHeaderAprobacion.VisibleBotonAccion3 = True
                        End If
                    End If
                Next
                If blnAprobar Then FSNPageHeaderAprobacion.VisibleBotonAprobar = True
                If blnRechazar Then FSNPageHeaderAprobacion.VisibleBotonRechazar = True
                If iOtrasAcciones > 3 Then
                    FSNPageHeaderAprobacion.VisibleBotonAccion1 = True
                    FSNPageHeaderAprobacion.OnClientClickAccion1 = "MostrarMenuAcciones(this); return false;"
                End If
            ElseIf oDSAcciones.Tables(0).Rows.Count > 0 Then
                'En Aprobacion existe la posibilidad de "Ver Pedidos pendientes de validaciÃ³n por aprobador superior" 
                Dim oRow As DataRow = oDSAcciones.Tables(0).Rows(0)
                If DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
                    FSNPageHeaderAprobacion.VisibleBotonAprobar = True
                    FSNPageHeaderAprobacion.TextoBotonAprobar = DBNullToStr(oRow.Item("DEN"))
                    FSNPageHeaderAprobacion.OnClientClickAprobar = "EjecutarAccion(" + oInstancia.ID.ToString + " , " + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(DBNullToSomething(oRow.Item("CUMP_OBL_ROL")) = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("GUARDA")) = 1, "true", "false")) + "'," + CStr(oInstancia.InstanciaBloqueBloq) + ",'" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'," + CStr(tipoSolicitud) + "); return false;"
                ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
                    FSNPageHeaderAprobacion.VisibleBotonRechazar = True
                    FSNPageHeaderAprobacion.TextoBotonRechazar = DBNullToStr(oRow.Item("DEN"))
                    FSNPageHeaderAprobacion.OnClientClickRechazar = "EjecutarAccion(" + oInstancia.ID.ToString + " , " + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(DBNullToSomething(oRow.Item("CUMP_OBL_ROL")) = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("GUARDA")) = 1, "true", "false")) + "'," + CStr(oInstancia.InstanciaBloqueBloq) + ",'" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'," + CStr(tipoSolicitud) + "); return false;"
                Else
                    FSNPageHeaderAprobacion.VisibleBotonAccion1 = True
                    FSNPageHeaderAprobacion.OnClientClickAccion1 = "EjecutarAccion(" + oInstancia.ID.ToString + " , " + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(DBNullToSomething(oRow.Item("CUMP_OBL_ROL")) = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("GUARDA")) = 1, "true", "false")) + "'," + CStr(oInstancia.InstanciaBloqueBloq) + ",'" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'," + CStr(tipoSolicitud) + "); return false;"
                    FSNPageHeaderAprobacion.TextoBotonAccion1 = DBNullToStr(oRow.Item("DEN"))
                End If
            End If
        End If
        '''''''''''''''''''''''''''''''
    End Sub
    Private Sub CargarTextos()
        LblProve.Text = Textos(50)
        lblEstadoOrden.Text = Textos(143) & ":"
        lblImporteBruto.Text = Textos(206)
        lblCostesGenerales.Text = Textos(209)
        lblDescuentosGenerales.Text = Textos(210)
        lblImporteTotal.Text = Textos(211)
        LblNumSap.Text = Textos(266) & ":"
        LblTipoPedido.Text = Textos(129) & ":"
        LblProveErp.Text = Textos(49)
        LblReceptor.Text = Textos(48)
        LblEmpresa.Text = Textos(47)
        LblGestorTitulo.Text = Textos(189) & ":"
        lblDireccionEnvioFactura.Text = Textos(165) & ":"
        lblPedidoAbierto.Text = Textos(293) & ":"
        lblCentro.Text = Textos(302) & ":"
        lblCostes.Text = Textos(207)
        lblDescuentos.Text = Textos(208)
        lblCostesLinea.Text = Textos(207)
        lblDescuentosLinea.Text = Textos(208)
        lblObservaciones.Text = Textos(128) & " :"
        lblArchivosLineas.Text = Textos(114)
        lblArchivos.Text = Textos(225)
        whdgArticulos.GroupingSettings.EmptyGroupAreaText = Textos(202)
        lblDatosGenerales.Text = Textos(226)
        lblResumenLinea.Text = Textos(227)
        lblDatosLinea.Text = Textos(228)
        lblOtrosDatos.Text = Textos(218)
        lblCodigoAtributoTablaLinea.Text = Textos(88)
        lblCodCoste.Text = Textos(88)
        lblCodCosteLinea.Text = Textos(88)
        lblCodDescuento.Text = Textos(88)
        lblCodDescuentoLinea.Text = Textos(88)
        lblDenomAtributoTablaLinea.Text = Textos(89)
        lblDenCoste.Text = Textos(89)
        lblDenCosteLinea.Text = Textos(89)
        lblDenDescuento.Text = Textos(89)
        lblDenDescuentoLinea.Text = Textos(89)
        lblTipoAtributoTablaLinea.Text = Textos(229)
        lblTipoCoste.Text = Textos(229)
        lblTipoCosteLinea.Text = Textos(229)
        lblTipoDescuento.Text = Textos(229)
        lblTipoDescuentoLinea.Text = Textos(229)
        lblIntroAtributoTablaLinea.Text = Textos(230)
        lblIntroCoste.Text = Textos(230)
        lblIntroCosteLinea.Text = Textos(230)
        lblIntroDescuento.Text = Textos(230)
        lblIntroDescuentoLinea.Text = Textos(230)
        lblObligAtributoTablaLinea.Text = Textos(231)
        lblValorAtributoTablaLinea.Text = Textos(237)
        lblValCoste.Text = Textos(237)
        lblValCosteLinea.Text = Textos(237)
        lblValDescuento.Text = Textos(237)
        lblValDescuentoLinea.Text = Textos(237)
        lblImpCoste.Text = Textos(12)
        lblImpCosteLinea.Text = Textos(12)
        lblImpDescuento.Text = Textos(12)
        lblImpDescuentoLinea.Text = Textos(12)
        lblTotCostes.Text = Textos(233)
        lblTotCostesLinea.Text = Textos(233)
        lblTotDescuento.Text = Textos(234)
        lblTotDescuentoLinea.Text = Textos(234)
        lblNumLinea.Text = Textos(232)
        lblNumLineaCoste.Text = Textos(232)
        lblNumLineaDescuento.Text = Textos(232)
        lblNumLineaPlanesEntrega.Text = Textos(232)
        LblDest.Text = Textos(13) & ":"
        LblAlmacen.Text = Textos(136) & ":"
        lblCentroCoste.Text = Textos(56)
        lblCategoria.Text = Textos(147)
        lblArcPed.Text = Textos(120)
        lblObsGen.Text = Textos(5)
        lblBotonBuscarIdentificador.Text = Textos(79)
        lblLitCreadoPor.Text = Textos(250) & ":"
        lblLitFecAlta.Text = Textos(251) & ":"
        lblLitImporte.Text = Textos(252) & ":"
        lblLitEstado.Text = Textos(253) & ":"
        lblLitTipo.Text = Textos(254) & ":"
        HyperDetalle.Text = Textos(255)
        lblCodigoAtributoTablaOrden.Text = Textos(256)
        lblDenomAtributoTablaOrden.Text = Textos(257)
        lblValorAtributoTablaOrden.Text = Textos(258)
        btnConfirmarEnvio.Text = Textos(260)
        btnCancelarEnvio.Text = Textos(261)
        lblEtapas.Text = Textos(262) & ":"
        lblRoles.Text = Textos(263) & ":"
        lblComentarios.Text = Textos(264) & ":"
        btnCancelarRechazoLinea.Text = Textos(34)
        btnAceptarRechazoLinea.Text = Textos(79)
        lblCausaDenegacionLinea.Text = Textos(265) & ":"
        lblRolTablaRoles.Text = Textos(268)
        lblUsuDepUonTablaRoles.Text = Textos(269)
        lblPlanesEntrega.Text = Textos(320)
        lblRecepcionAutomatica.Text = Textos(321)
        lblSolicAcepProve.Text = Textos(334) & ":"

        Me.lblEstadoOrdenIntegracion.Text = Textos(337) & ":"
        Me.btnRelanzar.Text = Textos(338)
        Me.lblAlertaRelanzar.Text = Textos(339)
        Me.btnAceptarRelanzar.Value = Textos(79)
        Me.btnCancelarRelanzar.Value = Textos(34)
        Me.lblRelanzarOK.Text = Textos(340)
        Me.btnRelanzarOk.Value = Textos(79)

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.TraspasoAdjudicaciones
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "Textos") Then
            Dim sVariableJavascriptTextosBoolean As String = "var TextosBoolean = new Array();"
            sVariableJavascriptTextosBoolean &= "TextosBoolean[0]='" & JSText(Textos(44)) & "';"
            sVariableJavascriptTextosBoolean &= "TextosBoolean[1]='" & JSText(Textos(43)) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Textos", sVariableJavascriptTextosBoolean, True)
        End If

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosScript") Then
            Dim sVariableJavascriptTextosJScript As String = "var TextosJScript = new Array();"
            sVariableJavascriptTextosJScript &= "TextosJScript[0]='" & JSText(Textos(204)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[1]='" & JSText(Textos(205)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[2]='" & FSNUser.IdiomaCod & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[3]='" & JSText(Textos(212)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[4]='" & JSText(Textos(207)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[5]='" & JSText(Textos(206)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[6]='" & JSText(Textos(213)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[7]='" & JSText(Textos(79)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[8]='" & JSText(Textos(34)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[9]='" & JSText(Textos(214)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[10]='" & JSText(Textos(215)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[11]='" & JSText(Textos(216)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[12]='" & JSText(Textos(217)) & "';" 'Es obligatorio cabecera...
            sVariableJavascriptTextosJScript &= "TextosJScript[13]='" & JSText(Textos(207)) & "';" 'Costes
            sVariableJavascriptTextosJScript &= "TextosJScript[14]='" & JSText(Textos(208)) & "';" 'Descuentos
            sVariableJavascriptTextosJScript &= "TextosJScript[15]='" & JSText(Textos(218)) & "';" 'Otros datos
            sVariableJavascriptTextosJScript &= "TextosJScript[16]='" & JSText(Textos(219)) & "';" 'Es obligatorio linea...
            sVariableJavascriptTextosJScript &= "TextosJScript[17]='" & JSText(Textos(220)) & "';" 'Debe seleccionar un receptor...
            sVariableJavascriptTextosJScript &= "TextosJScript[18]='" & JSText(Textos(221)) & "';" 'Error 1 Cantidad
            sVariableJavascriptTextosJScript &= "TextosJScript[19]='" & JSText(Textos(222)) & "';" 'Error 2 Cantidad
            sVariableJavascriptTextosJScript &= "TextosJScript[20]='" & JSText(Textos(223)) & "';" 'Error 3 Cantidad
            sVariableJavascriptTextosJScript &= "TextosJScript[21]='" & JSText(Textos(224)) & "';" 'Error 4 Cantidad
            sVariableJavascriptTextosJScript &= "TextosJScript[22]='" & JSText(Textos(86)) & "';" 'Seleccione destino
            sVariableJavascriptTextosJScript &= "TextosJScript[23]='" & JSText(Textos(88)) & "';" 'Codigo
            sVariableJavascriptTextosJScript &= "TextosJScript[24]='" & JSText(Textos(89)) & "';" 'Denominacion
            sVariableJavascriptTextosJScript &= "TextosJScript[25]='" & JSText(Textos(90)) & "';" 'Direccion
            sVariableJavascriptTextosJScript &= "TextosJScript[26]='" & JSText(Textos(91)) & "';" 'CP
            sVariableJavascriptTextosJScript &= "TextosJScript[27]='" & JSText(Textos(92)) & "';" 'Poblacion
            sVariableJavascriptTextosJScript &= "TextosJScript[28]='" & JSText(Textos(93)) & "';" 'Provincia
            sVariableJavascriptTextosJScript &= "TextosJScript[29]='" & JSText(Textos(94)) & "';" 'Pais
            sVariableJavascriptTextosJScript &= "TextosJScript[32]='" & JSText(Textos(246)) & "';" 'Confirmar anulación
            sVariableJavascriptTextosJScript &= "TextosJScript[33]='" & JSText(Textos(249)) & "';" 'Marque la casilla de confirmación si desea anular el pedido.
            sVariableJavascriptTextosJScript &= "TextosJScript[34]='" & JSText(Textos(79)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[35]='" & JSText(Textos(34)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[36]='" & JSText(Textos(271)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[37]='" & JSText(Textos(232)) & "';"
            sVariableJavascriptTextosJScript &= "TextosJScript[38]='" & JSText(Textos(270)) & "';"
            lblTitulo.InnerHtml = Textos(276)       'Proveedor en ERP
            lblCodigo.InnerHtml = Textos(277)       'Codigo:
            lblDescripcion.InnerHtml = Textos(278)  'Descripcion:
            lblDestInfoDir.InnerHtml = Textos(90)
            lblDestInfoCP.InnerHtml = Textos(91)
            lblDestInfoPob.InnerHtml = Textos(92)
            lblDestInfoProv.InnerHtml = Textos(93)
            lblDestInfoPais.InnerHtml = Textos(94)
            'Destinos Teléfono, FAX y E-mail
            lblDestInfoTfno.InnerHtml = Textos(279)       'Teléfono
            lblDestInfoFAX.InnerHtml = Textos(280)       'Proveedor en ERP
            lblDestInfoEmail.InnerHtml = Textos(281)       'Proveedor en ERP
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
            sVariableJavascriptTextosJScript &= "TextosJScript[39]='" & JSText(Textos(31)) & "';" 'Pendiente de aprobar
            sVariableJavascriptTextosJScript &= "TextosJScript[40]='" & JSText(Textos(32)) & "';" 'DenegadoParcialmente
            sVariableJavascriptTextosJScript &= "TextosJScript[41]='" & JSText(Textos(33)) & "';" 'EmitidoAlProveedor
            sVariableJavascriptTextosJScript &= "TextosJScript[42]='" & JSText(Textos(34)) & "';" 'AceptadoPorElProveedor
            sVariableJavascriptTextosJScript &= "TextosJScript[43]='" & JSText(Textos(35)) & "';" 'EnCamino
            sVariableJavascriptTextosJScript &= "TextosJScript[44]='" & JSText(Textos(36)) & "';" 'RecibidoParcialmente
            sVariableJavascriptTextosJScript &= "TextosJScript[45]='" & JSText(Textos(37)) & "';" 'Cerrado
            sVariableJavascriptTextosJScript &= "TextosJScript[46]='" & JSText(Textos(38)) & "';" 'Anulado
            sVariableJavascriptTextosJScript &= "TextosJScript[47]='" & JSText(Textos(39)) & "';" 'Rechazado
            sVariableJavascriptTextosJScript &= "TextosJScript[48]='" & JSText(Textos(40)) & "';" 'Denegado
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
            sVariableJavascriptTextosJScript &= "TextosJScript[49]='" & JSText(Textos(319)) & "';" 'Fecha de entrega
            sVariableJavascriptTextosJScript &= "TextosJScript[50]='" & JSText(Textos(11)) & "';" 'Cantidad
            sVariableJavascriptTextosJScript &= "TextosJScript[51]='" & JSText(Textos(12)) & "';" 'Importe
            sVariableJavascriptTextosJScript &= "TextosJScript[52]='" & JSText(Textos(335)) & "';" 'Sí
            sVariableJavascriptTextosJScript &= "TextosJScript[53]='" & JSText(Textos(336)) & "';" 'No
            Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
            sVariableJavascriptTextosJScript &= "TextosJScript[54]='" & JSText(Textos(301)) & "';" 'Borrada
            sVariableJavascriptTextosJScript &= "TextosJScript[55]='" & JSText(Textos(288)) & "';" 'En Espera Lim Adj
            sVariableJavascriptTextosJScript &= "TextosJScript[56]='" & JSText(Textos(51)) & "';" 'En Espera
            sVariableJavascriptTextosJScript &= "TextosJScript[57]='" & JSText(Textos(50)) & "';" 'Denegada
            sVariableJavascriptTextosJScript &= "TextosJScript[58]='" & JSText(Textos(49)) & "';" 'Cerrado
            sVariableJavascriptTextosJScript &= "TextosJScript[59]='" & JSText(Textos(48)) & "';" 'Recibodo parcialmente
            sVariableJavascriptTextosJScript &= "TextosJScript[60]='" & JSText(Textos(46)) & "';" 'Pend.. de aprobar
            sVariableJavascriptTextosJScript &= "TextosJScript[61]='" & JSText(Textos(100)) & "';" 'Acep.. por el prove..
            sVariableJavascriptTextosJScript &= "TextosJScript[62]='" & JSText(Textos(92)) & "';" 'Emitida
            sVariableJavascriptTextosJScript &= "TextosJScript[63]='" & JSText(Textos(47)) & "';" 'Aprobada
            sVariableJavascriptTextosJScript &= "TextosJScript[64]='" & JSText(Textos(325)) & "';" 'Borrado
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
            sVariableJavascriptTextosJScript &= "TextosJScript[65]='" & JSText(Textos(341)) & "';" 'EstadoOrdenIntegracion=0 Pendiente de tratar
            sVariableJavascriptTextosJScript &= "TextosJScript[66]='" & JSText(Textos(342)) & "';" 'EstadoOrdenIntegracion=1 Enviado
            sVariableJavascriptTextosJScript &= "TextosJScript[67]='" & JSText(Textos(343)) & "';" 'EstadoOrdenIntegracion=3 Error
            sVariableJavascriptTextosJScript &= "TextosJScript[68]='" & JSText(Textos(344)) & "';" 'EstadoOrdenIntegracion=4 Correcto

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosScript", sVariableJavascriptTextosJScript, True)
        End If

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_AprobacionPedidos
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosAlert") Then
            Dim sVariableJavascriptTextosAlert As String = "var TextosAlert = new Array();"
            sVariableJavascriptTextosAlert &= "TextosAlert[0]='" & JSText(Textos(125)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[1]='" & JSText(Textos(131)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[2]='" & JSText(Textos(130)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[3]='" & JSText(Textos(132)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[4]='" & JSText(Textos(140)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[5]='" & JSText(Textos(142)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[6]='" & JSText(Textos(128)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[7]='" & JSText(Textos(172)) & "';"
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
            sVariableJavascriptTextosAlert &= "TextosAlert[10]='" & JSText(Textos(200)) & " " & JSText(Textos(201)) & "';" 'Imposible Anular. 'El pedido tiene recepciones asociadas, debe eliminarlas previamente para poder anular el pedido.
            sVariableJavascriptTextosAlert &= "TextosAlert[11]='" & JSText(Textos(200)) & " " & JSText(Textos(202)) & "';" 'Imposible Anular. 'El pedido tiene facturas o pagos asociados, debe eliminarlos previamente para poder anular el pedido.
            sVariableJavascriptTextosAlert &= "TextosAlert[12]='" & JSText(Textos(203)) & "';" 'El proceso de anulación no se puede deshacer. Si desea continuar, chequee la casilla de confirmación.
            sVariableJavascriptTextosAlert &= "TextosAlert[13]='" & JSText(Textos(205)) & "';" 'Anulada correctamente
            sVariableJavascriptTextosAlert &= "TextosAlert[14]='" & JSText(Textos(206)) & "';" 'Se ha producido un error al anular el pedido. Por favor, inténtelo de nuevo más tarde y si el problema persiste, contacte con el soporte técnico.
            sVariableJavascriptTextosAlert &= "TextosAlert[20]='" & JSText(Textos(220)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[21]='" & JSText(Textos(164)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[22]='" & JSText(Textos(156)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[23]='" & JSText(Textos(236)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[24]='" & JSText(Textos(297)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[25]='" & JSText(Textos(298)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[26]='" & JSText(Textos(299)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[27]='" & JSText(Textos(300)) & "';"

            sVariableJavascriptTextosAlert &= "TextosAlert[28]='" & JSText(Textos(312)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[29]='" & JSText(Textos(313)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[30]='" & JSText(Textos(314)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[31]='" & JSText(Textos(315)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[32]='" & JSText(Textos(316)) & "';"

            sVariableJavascriptTextosAlert &= "TextosAlert[33]='" & JSText(Textos(317)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[34]='" & JSText(Textos(318)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[35]='" & JSText(Textos(319)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[36]='" & JSText(Textos(320)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[37]='" & JSText(Textos(321)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[38]='" & JSText(Textos(322)) & "';"

            sVariableJavascriptTextosAlert &= "TextosAlert[39]='" & JSText(Textos(323)) & "';" 'Línea de baja, no se puede gestionar.

            sVariableJavascriptTextosAlert &= "TextosAlert[40]='" & JSText(Textos(303)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[41]='" & JSText(Textos(304)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[42]='" & JSText(Textos(305)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[43]='" & JSText(Textos(306)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[44]='" & JSText(Textos(307)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[45]='" & JSText(Textos(308)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[46]='" & JSText(Textos(309)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[47]='" & JSText(Textos(310)) & "';"
            sVariableJavascriptTextosAlert &= "TextosAlert[48]='" & JSText(Textos(311)) & "';"

            sVariableJavascriptTextosAlert &= "TextosAlert[49]='" & JSText(Textos(333)) & "';" 'tarea 10413 / No es posible anular el pedido, la eliminación de las recepciones del pedido no se ha integrado correctamente.
            sVariableJavascriptTextosAlert &= "TextosAlert[50]='" & JSText(Textos(334)) & "';" 'tarea 10413 / No es posible borrar el pedido, la eliminación de las recepciones del pedido no se ha integrado correctamente.
            sVariableJavascriptTextosAlert &= "TextosAlert[51]='" & JSText(Textos(335)) & "';" 'tarea 10414 / No es posible borrar el pedido, la eliminación de las recepciones del pedido no se ha integrado correctamente.
            sVariableJavascriptTextosAlert &= "TextosAlert[52]='" & JSText(Textos(336)) & "';" 'No es posible borrar el pedido, el pedido tiene hitos de facturación.
            sVariableJavascriptTextosAlert &= "TextosAlert[53]='" & JSText(Textos(337)) & "';" 'La línea tiene hitos de facturación, no se puede borrar.
            sVariableJavascriptTextosAlert &= "TextosAlert[54]='" & JSText(Textos(338)) & "';" 'No es posible anular el pedido, el pedido tiene hitos de facturación.

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosAlert", sVariableJavascriptTextosAlert, True)
        End If

        lblCampo1.InnerHtml = Me.Acceso.nomCampo1(Me.Idioma) & ":"
        lblCampo2.InnerHtml = Me.Acceso.nomCampo2(Me.Idioma) & ":"
        lblCampo3.InnerHtml = Me.Acceso.nomCampo3(Me.Idioma) & ":"
        lblCampo4.InnerHtml = Me.Acceso.nomCampo4(Me.Idioma) & ":"

        lblTituloFacturasLin.Text = Textos(147)
        lblTituloPagosLin.Text = Textos(148)

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
    End Sub
    ''' <summary>
    ''' Funcion que carga variables javascript
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarScripts()
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "NumberFormatUsu") Then
            With FSNUser
                Dim sVariablesJavascript As String = "var UsuNumberDecimalSeparator = '" & .NumberFormat.NumberDecimalSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberGroupSeparator='" & .NumberFormat.NumberGroupSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberNumDecimals='" & .NumberFormat.NumberDecimalDigits & "';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NumberFormatUsu", sVariablesJavascript, True)
            End With
        End If

        Dim idOrden As Long
        If Not Request.QueryString("Id") Is Nothing Then
            idOrden = Request.QueryString("Id")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sID", "<script>var sID = '" & idOrden & "' </script>")
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "tema") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "tema",
                "<script>var tema = '" & Me.Page.Theme & "';</script>")
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutaPM") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM",
                "<script>var rutaPM = '" & System.Configuration.ConfigurationManager.AppSettings("rutaPM") & "';</script>")
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutaFS") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS",
                "<script>var rutaFS = '" & System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "';</script>")
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutaEP") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaEP",
                "<script>var rutaEP = '" & System.Configuration.ConfigurationManager.AppSettings("rutaEP") & "';</script>")
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "ruta") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ruta",
                "<script>var ruta = '" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "';</script>")
        End If

        Dim hayIntegracionPedidos As Boolean = CBool(Session("HayIntegracionPedidos"))
        Dim sHayIntegracionPedidos As String

        hayIntegracionPedidos = True
        If hayIntegracionPedidos Then
            sHayIntegracionPedidos = "1"
        Else
            sHayIntegracionPedidos = "0"
        End If
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sHayIntegracionPedidos", "<script>var sHayIntegracionPedidos = '" & sHayIntegracionPedidos & "' </script>")
        Dim dCambio As Double = DsetPedidos.Tables(0).Rows(0).Item("CAMBIO")
        Dim sCambio As String = dCambio.ToString.Replace(",", ".")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "dCambio", "<script>var dCambio = " & sCambio & " </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "instanciaAprob",
                "<script>var instanciaAprob='" & DBNullToStr(InstanciaAprob) & "';</script>")
        imgCabeceraConfirmacion.Src = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/FSPMWebPartAltaSolicitudes.gif"
    End Sub
    ''' <summary>
    ''' Hacemos todas las validaciones correspondientes a la dirección de envío de la factura.
    ''' Comprobamos si es obligatorio o no (si debe mostrarse o no) y si hay datos
    ''' </summary>
    ''' <param name="sProveedor">sProveedor</param>
    ''' <param name="IdEmpresa">Id de la empresa de la que se quieren mostrar las direcciones. Si es cero es que no hay empresa</param>
    ''' <remarks>Llamada desde dlCesta.ItemDataBound. Tiempo máx inferior a 0,2 seg</remarks>
    Private Overloads Sub CargarDireccionesEnvioFacturas(ByRef sProveedor As String, ByVal IdEmpresa As Integer)
        If IdEmpresa > 0 Then
            Dim _DireccionesEnvioFactura As CEnvFacturaDirecciones = FSNServer.Get_Object(GetType(CEnvFacturaDirecciones))
            _DireccionesEnvioFactura = Me.Empresas.Item(IdEmpresa.ToString).DireccionesEnvioFactura
        Else
            Dim _DireccionesEnvioFactura As CEnvFacturaDirecciones = Nothing
        End If
    End Sub
    ''' <summary>
    ''' Método mediante el cual cargamos en el control whdgEntregas los datos de entregas
    ''' </summary>
    ''' <param name="paraExportacion">
    '''      False->La carga del grid no es para exportación sino para mostrar en pantalla por lo que se pagina
    '''      True->La carga del grid es para exportación por lo que no se pagina a sólo saldrían en la exportación el número de registros indicado en la paginación
    ''' </param>
    ''' <remarks>Llamada desde Load() y Exportar. Máx 1 seg</remarks>
    Private Sub CargarwhdgArticulos(ByVal paraExportacion As Boolean, Optional ByVal MasResultados As Boolean = False)
        dsPedidos = New DataSet
        dsPedidos = DsetPedidos(MasResultados)

        Dim groupedColumns() As String = Split(Session("COLSGROUPBY"), "#")
        For Each item As String In groupedColumns
            If Not item = "" Then whdgArticulos.GroupingSettings.GroupedColumns.Add(Split(item, " ")(0),
                IIf(Split(item, " ")(1) = "DESC", Infragistics.Web.UI.GridControls.GroupingSortDirection.Descending, Infragistics.Web.UI.GridControls.GroupingSortDirection.Ascending))
        Next

        'Se tiene que hacer antes que el databind
        If Not paraExportacion Then
            whdgArticulos.GridView.Behaviors.Paging.PageSize = ConfigurationManager.AppSettings("registrosPaginacion")
        End If

        'IMP: Si anteriormente hay una o mas filas seleccionadas da un error y no se carga la grid. Por eso hay que limpiar las filas seleccionadas.
        whdgArticulos.DataSource = dsPedidos
        'Los DataKeyFields deben definirse entre el Datasource y el Databind

        whdgArticulos.DataMember = "LINEAS_PEDIDO"
        whdgArticulos.DataKeyFields = "IDLINEA"
        'Añadir campos al webdatagrid
        'Necesitamos que se genere el DSetPedidos para usarlo dentro del CreatewhdgEntregasColumnsYConfiguracion
        'por lo que no llamamos al CreatewhdgEntregasColumnsYConfiguracion hasta tenerlo
        If dsPedidos.Tables.Count > 0 Then
            CreatewhdgEntregasColumnsYConfiguracion(dsPedidos.Tables(0).Rows(0).Item("MON"))
        Else
            CreatewhdgEntregasColumnsYConfiguracion(Usuario.Mon)
        End If
        whdgArticulos.DataBind()

    End Sub
    ''' <summary>
    ''' Crea las columnas fijas del grid en función de los datos guardados en la configuración 
    ''' y añade los campos al panel de configuración y añade las columnas al grid de configuracion que esta en ese mismo panel
    ''' </summary>
    ''' <remarks>Llamada desde CargarwhdgArticulos. Máx. 0,5 seg</remarks>
    Private Sub CreatewhdgEntregasColumnsYConfiguracion(ByVal sMoneda As String)
        EditColumnYConfiguracion("DARBAJA", "", InvisibilidadBtBorrarLin) 'Icono Baja
        EditColumnYConfiguracion("NUM_LINEA", "", False) 'Nº Línea
        EditColumnYConfiguracion("IDLINEA", "", False) 'Nº Línea
        EditColumnYConfiguracion("COD_ITEM", Textos(69), False) 'Artículo
        EditColumnYConfiguracion("ART_DEN", Textos(69), False) 'Artículo
        EditColumnYConfiguracion("PREC", Textos(198) & " (" & sMoneda & ")", False) 'Prec.Uni, Precio por unidad
        EditColumnYConfiguracion("CANT", Textos(11), False) 'Cantidad
        EditColumnYConfiguracion("UNI", Textos(113), False) 'Unidad
        EditColumnYConfiguracion("IMPBRUTO", Textos(206) & " (" & sMoneda & ")", False)  'Imp. Bruto
        EditColumnYConfiguracion("COSTES", Textos(207) & " (" & sMoneda & ")", False)  'Costes
        EditColumnYConfiguracion("DESCUENTOS", Textos(208) & " (" & sMoneda & ")", False)  'Descuentos
        EditColumnYConfiguracion("UP", "", True)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
        EditColumnYConfiguracion("CANTIDADPDTE", Textos(195), Not (CType(Request("Desde"), Integer) = 3))
        EditColumnYConfiguracion("IMPORTEPDTE", Textos(265), Not (CType(Request("Desde"), Integer) = 3))
        EditColumnYConfiguracion("EST", Textos(44), Not (CType(Request("Desde"), Integer) = 3))
        EditColumnYConfiguracion("FECENTREGA", Textos(118), Not (CType(Request("Desde"), Integer) = 3))
        EditColumnYConfiguracion("FECENTREGAPROVE", Textos(119), Not (CType(Request("Desde"), Integer) = 3))
        EditColumnYConfiguracion("RECEPCIONES", Textos(158), Not (CType(Request("Desde"), Integer) = 3))
        EditColumnYConfiguracion("APROBAR", "", (CType(Request("Desde"), Integer) = 3))
        EditColumnYConfiguracion("RECHAZAR", "", (CType(Request("Desde"), Integer) = 3))
        EditColumnYConfiguracion("FRA", Textos(143), (Not (CType(Request("Desde"), Integer) = 3) OrElse Not Acceso.g_bAccesoFSFA))
        EditColumnYConfiguracion("PAGO", Textos(144), (Not (CType(Request("Desde"), Integer) = 3) OrElse Not Acceso.g_bAccesoFSFA))
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_AprobacionPedidos
        EditColumnYConfiguracion("INSTANCIA_APROB", Textos(168), Not (CType(Request("Desde"), Integer) = 3))
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
        'Configurar Anchos y Visibilidad
        If Not IsPostBack Then
            confAnchosVisibilidadYPosicion()
        End If
    End Sub
    ''' <summary>
    ''' Configurar Anchos, Visibilidad y Posición de los campos del webhierarchicaldatagrid de pedidos y configuracion
    ''' </summary>
    ''' <remarks>Llamada desde CreatewhdgEntregasColumns. Máx. 0,1 seg.</remarks>
    Private Sub confAnchosVisibilidadYPosicion()
        'Obtenemos la relación entre los campos de la tabla de configuracion de anchos y visibilidad con los campos de la tabla de entregas
        Dim camposEntregasYConfiguracion As List(Of Campos) = relacionarEntregasyConfiguracion()

        'Coger los datos de ancho y visibilidad de la tabla desde base de datos
        For Each campo As Campos In camposEntregasYConfiguracion
            If whdgArticulos.GridView.Columns("Key_" & campo.CampoEntrega) IsNot Nothing Then
                If campo.Visible AndAlso Not campo.Width = 0 Then whdgArticulos.GridView.Columns("Key_" & campo.CampoEntrega).Width = Unit.Pixel(campo.Width)
                whdgArticulos.GridView.Columns("Key_" & campo.CampoEntrega).Hidden = Not campo.Visible
                whdgArticulos.GridView.Columns("Key_" & campo.CampoEntrega).VisibleIndex = campo.Position

                If campo.Visible AndAlso Not campo.Width = 0 Then whdgArticulos.Columns("Key_" & campo.CampoEntrega).Width = Unit.Pixel(campo.Width)
                whdgArticulos.Columns("Key_" & campo.CampoEntrega).Hidden = Not campo.Visible
                whdgArticulos.Columns("Key_" & campo.CampoEntrega).VisibleIndex = campo.Position
            End If
        Next

        If FSNUser.MostrarCodArt Then
            whdgArticulos.Columns("Key_COD_ITEM").Hidden = False
            whdgArticulos.GridView.Columns("Key_COD_ITEM").Hidden = False
        Else
            whdgArticulos.Columns("Key_COD_ITEM").Hidden = True
            whdgArticulos.GridView.Columns("Key_COD_ITEM").Hidden = True
        End If

        If hayQueOcultarColumnasRecepcionCantidad() Then
            whdgArticulos.Columns("Key_PREC").Hidden = True
            whdgArticulos.GridView.Columns("Key_PREC").Hidden = True
            whdgArticulos.Columns("Key_CANT").Hidden = True
            whdgArticulos.GridView.Columns("Key_CANT").Hidden = True
            whdgArticulos.Columns("Key_CANTIDADPDTE").Hidden = True
            whdgArticulos.GridView.Columns("Key_CANTIDADPDTE").Hidden = True
            whdgArticulos.Columns("Key_UNI").Hidden = True
            whdgArticulos.GridView.Columns("Key_UNI").Hidden = True
        End If
        If hayQueOcultarColumnaSituacion() Then
            whdgArticulos.Columns("Key_INSTANCIA_APROB").Hidden = True
            whdgArticulos.GridView.Columns("Key_INSTANCIA_APROB").Hidden = True
        End If
    End Sub
    ''' <summary>
    ''' Función que añade una columna al control whdgEntregas, vinculada al origen de datos de éste.
    ''' Añade el mismo campo al panel de configuración
    ''' </summary>
    ''' <param name="fieldName">Nombre del campo en el origen de datos</param>
    ''' <param name="headerText">Título de la columna</param>
    ''' <param name="headerTooltip">Tooltip de la columna</param>
    ''' <remarks>Llamada desde CreatewhdgEntregasColumns. Máx. 0,1 seg.</remarks>
    Private Sub EditColumnYConfiguracion(ByVal fieldName As String, ByVal headerText As String, ByVal Hidden As Boolean,
                                         Optional ByVal headerTooltip As String = "", Optional ByVal MostrarEnPanelConfiguracion As Boolean = True)
        'Configurar el grid
        If Not whdgArticulos.GridView.Columns.Item("Key_" & fieldName) Is Nothing Then
            With whdgArticulos.GridView.Columns.Item("Key_" & fieldName)
                .Header.Text = headerText
                .Hidden = Hidden
                If headerTooltip = String.Empty Then headerTooltip = headerText
                .Header.Tooltip = headerTooltip
            End With
            If whdgArticulos.Columns.Item("Key_" & fieldName) IsNot Nothing Then
                whdgArticulos.Columns.Item("Key_" & fieldName).Header.Text = headerText
                If headerTooltip = String.Empty Then headerTooltip = headerText
                whdgArticulos.Columns.Item("Key_" & fieldName).Header.Tooltip = headerTooltip
            End If
        End If
    End Sub
#Region "Eventos Grid Lineas Pedido"
    ''' Revisado por: blp. Fecha: 23/08/2012
    ''' <summary>
    ''' Método que se lanza en el evento InitializeRow (cuando se enlazan los datos del origen de datos con la línea)
    ''' En él vamos a configurar la columna de entrega con retrasos (mostrar la imagen de retraso en las entregas retrasadas)
    ''' </summary>
    ''' <param name="sender">Control que lanza el evento (whdgEntregas)</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. Máx 0,1 seg</remarks>
    Private Sub whdgArticulos_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgArticulos.InitializeRow
        Dim oData As DataRow = e.Row.DataItem.Item.Row
        Dim dCant As Double
        Dim dCantDec As Double
        Dim sCantidadPdte As String = "0"
        Dim txtNoAplica As String = UCase("&lt;" & m_sNoAplica & "&gt;")
        If sender.Columns.FromKey("Key_IDLINEA") IsNot Nothing Then
            Dim sCantidad As String
            Dim oUnidadPedido As FSNServer.CUnidadPedido = Nothing
            If Not DBNullToStr(oData("UP")) = Nothing Then
                oUnidadPedido = TodasLasUnidades.Item(Trim(DBNullToStr(oData("UP"))))
                oUnidadPedido.AsignarFormatoaUnidadPedido(Me.Usuario.NumberFormat)
            End If

            Dim Index As Integer = sender.Columns.FromKey("Key_IDLINEA").Index()
            If (DBNullToStr(oData("TIPORECEPCION").ToString()) = 0) Then
                Dim vprecisionfmt As Integer
                Dim unitFormatTemp As System.Globalization.NumberFormatInfo = oUnidadPedido.UnitFormat.Clone
                Try
                    dCant = Double.Parse(DBNullToStr(oData("CANT").ToString()))
                Catch ex As FormatException
                    Exit Sub
                Catch ex As Exception
                    Exit Sub
                End Try
				If Not oUnidadPedido Is Nothing Then
					If oUnidadPedido.NumeroDeDecimales Is Nothing Then
						'Cuando el número de decimales de la unidad no está definido, teóricamente puede ser cualquiera pero
						'vamos a poner un límite de 99. Si fuese necesario cambiar esto, habrá que revisar su repercusión en jsTextBoxFormat.js
						'pues allí se controla el valor 99
						vprecisionfmt = 99
						oUnidadPedido.UnitFormat.NumberDecimalDigits = 0
						If Not Double.IsNaN(dCant) AndAlso (dCant <> CInt(dCant)) Then
							dCantDec = dCant - CInt(dCant)
							Dim numDecimales As Integer = 0
							If dCantDec.ToString.IndexOf(".") > 0 Then
								numDecimales = dCantDec.ToString.Substring(dCantDec.ToString.IndexOf(".") + 1).Length
							ElseIf dCant.ToString.IndexOf(",") > 0 Then
								numDecimales = dCantDec.ToString.Substring(dCantDec.ToString.IndexOf(",") + 1).Length
							End If
							unitFormatTemp.NumberDecimalDigits = numDecimales
						Else
							unitFormatTemp.NumberDecimalDigits = 0
						End If
					ElseIf oUnidadPedido.NumeroDeDecimales > 0 Then
						vprecisionfmt = oUnidadPedido.NumeroDeDecimales
					ElseIf oUnidadPedido.NumeroDeDecimales = 0 Then
						vprecisionfmt = oUnidadPedido.NumeroDeDecimales
					End If
					ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos

					sCantidad = FormatNumber(dCant, unitFormatTemp)
					If oData("CANT") - oData("CANT_REC") < 0 Then
						sCantidadPdte = FSNLibrary.FormatNumber(0, unitFormatTemp)
					Else
						sCantidadPdte = FSNLibrary.FormatNumber(oData("CANT") - oData("CANT_REC"), unitFormatTemp)
					End If
				Else 'Si no tenemos datos de la unidad, usamos los datos del usuario
					vprecisionfmt = Me.Usuario.NumberFormat.NumberDecimalDigits

					sCantidad = FormatNumber(dCant, Me.Usuario.NumberFormat)
					If oData("CANT") - oData("CANT_REC") < 0 Then
						sCantidadPdte = FSNLibrary.FormatNumber(0, Me.Usuario.NumberFormat)
					Else
						sCantidadPdte = FSNLibrary.FormatNumber(oData("CANT") - oData("CANT_REC"), Me.Usuario.NumberFormat)
					End If
				End If

				If DBNullToInteger(oData("GESTIONABLE")) > 0 AndAlso oInstancia.DetalleEditable = True Then
                    If DBNullToInteger(oData("EST")) = 0 Or DBNullToInteger(oData("EST")) = 20 Or DBNullToInteger(oData("EST")) = 1 Then
                        e.Row.Items.FindItemByKey("Key_CANT").Text = "<input name='Cant" & DBNullToStr(oData("IDLINEA")) & "' id='Cant" & DBNullToStr(oData("IDLINEA")) & "' size='8' value='" & sCantidad & "' onBlur='if(validaFloatCantidad(this,null," & DBNullToStr(oData("IDLINEA")) & ")){cambiarCantidad(" & DBNullToStr(oData("IDLINEA")) & ")}' />  "
                    Else
                        e.Row.Items.FindItemByKey("Key_CANT").Text = "<span name='Cant" & DBNullToStr(oData("IDLINEA")) & "' id='Cant" & DBNullToStr(oData("IDLINEA")) & "'>" & sCantidad & "</>"
                    End If
                Else
                    e.Row.Items.FindItemByKey("Key_CANT").Text = "<span name='Cant" & DBNullToStr(oData("IDLINEA")) & "' id='Cant" & DBNullToStr(oData("IDLINEA")) & "'>" & sCantidad & "</>"
                End If

                e.Row.Items.FindItemByKey("Key_NUM_LINEA").Text = "<label id='" & DBNullToStr(oData("IDLINEA")) & "' >" & Format(DBNullToInteger(oData("NUM_LINEA")), "000").ToString() & "</label>"
                e.Row.Items.FindItemByKey("Key_UNI").Text = "<label id='Uni" & DBNullToStr(oData("IDLINEA")) & "' >" & oUnidadPedido.Codigo & "</label>"

                Dim sPrec As String
                Dim sPrecFormat As String
                Dim dPrec As Double
                Dim dCambio As Double
                Try
                    dPrec = Double.Parse(DBNullToStr(oData("PREC").ToString()))
                    dCambio = Double.Parse(DBNullToStr(oData("CAMBIO").ToString()))
                Catch ex As FormatException
                    Exit Sub
                Catch ex As Exception
                    Exit Sub
                End Try

                e.Row.Items.FindItemByKey("Key_PREC").Text = ""
                dPrec = dPrec * dCambio
                sPrec = dPrec.ToString()
                sPrecFormat = FSNLibrary.FormatNumber(dPrec, Me.Usuario.NumberFormat)

                If FSNUser.ModificarPrecios Then
                    If Not IsDBNull(oData("ANYO")) Then
                        If DBNullToInteger(oData("GESTIONABLE")) > 0 And oInstancia.DetalleEditable = True And (DBNullToInteger(oData("EST")) = 0 Or DBNullToInteger(oData("EST")) = 1) Then
                            e.Row.Items.FindItemByKey("Key_PREC").Text = "<input name='Prec" & DBNullToStr(oData("IDLINEA")) & "' id='Prec" & DBNullToStr(oData("IDLINEA")) & "' value='" & sPrecFormat & "' onBlur='if(validaFloat(this,null," & DBNullToStr(oData("IDLINEA")) & ")){cambiarPrecio(" & DBNullToStr(oData("IDLINEA")) & ")}' />" _
                                & "<label id='PrecT" & DBNullToStr(oData("IDLINEA")) & "' style='display:none;'>" & sPrecFormat & "</label> <label id='Prec" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;'>" & sPrec & "</label>"
                        Else
                            e.Row.Items.FindItemByKey("Key_PREC").Text = "<label id='PrecT" & DBNullToStr(oData("IDLINEA")) & "' >" & sPrecFormat & "</label><input name='Prec" & DBNullToStr(oData("IDLINEA")) & "' id='Prec" & DBNullToStr(oData("IDLINEA")) & "' value='" & sPrecFormat & "' readonly style='visibility:hidden;' /> " _
                                & " <label id='Prec" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;'>" & sPrec.Replace(",", ".") & "</label>"
                        End If
                    Else
                        e.Row.Items.FindItemByKey("Key_PREC").Text = "<label id='PrecT" & DBNullToStr(oData("IDLINEA")) & "' >" & sPrecFormat & "</label><input name='Prec" & DBNullToStr(oData("IDLINEA")) & "' id='Prec" & DBNullToStr(oData("IDLINEA")) & "' value='" & sPrecFormat & "' readonly style='visibility:hidden;' /> " _
                            & " <label id='Prec" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;'>" & sPrec.Replace(",", ".") & "</label>"
                    End If
                Else
                    e.Row.Items.FindItemByKey("Key_PREC").Text = "<label id='PrecT" & DBNullToStr(oData("IDLINEA")) & "' >" & sPrecFormat & "</label><input name='Prec" & DBNullToStr(oData("IDLINEA")) & "' id='Prec" & DBNullToStr(oData("IDLINEA")) & "' value='" & sPrecFormat & "' readonly style='visibility:hidden;' /> " _
                        & " <label id='Prec" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;'>" & sPrec.Replace(",", ".") & "</label>"
                End If

                Dim dPrecActual As Double
                Dim dPrecActualConBajas As Double
                If (lblImpBruto_hdd.Text <> String.Empty) Then
                    dPrecActual = Double.Parse(lblImpBruto_hdd.Text)
                Else
                    dPrecActual = 0
                End If
                Dim dPrecioLinea As Double
                dPrecioLinea = dPrec * dCant

                e.Row.Items.FindItemByKey("Key_IMPBRUTO").Text = "<label id='lblImpBruto" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;' >" & dPrecioLinea.ToString().Replace(",", ".") & "</label>" _
                    & "<label id='lblImpBruto" & DBNullToStr(oData("IDLINEA")) & "'>" & FSNLibrary.FormatNumber(dPrecioLinea, Me.Usuario.NumberFormat) & "</label>"

                If Not DBNullToBoolean(oData("BAJA_LOG")) OrElse DBNullToBoolean(oData("OE_BAJA_LOG")) Then
                    dPrecActual = dPrecActual + dPrecioLinea
                    dPrecActualConBajas = dPrecActual
                Else
                    dPrecActualConBajas = dPrecActual + dPrecioLinea
                End If

                lblImpBruto_hdd.Text = dPrecActual.ToString()
                lblImpBrutoConBajas_hdd.Text = dPrecActualConBajas.ToString()
                lblImpBrutoLinea_hdd.Text = dPrecActual.ToString()
                lblImpBrutoLineaConBajas_hdd.Text = dPrecActualConBajas.ToString()

                lblImpBruto.Text = FSNLibrary.FormatNumber(dPrecActual, Me.Usuario.NumberFormat) & " " & DBNullToStr(oData("MON"))
                e.Row.Items.FindItemByKey("Key_ART_DEN").Text = "<a id='COD_ITEM' >" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "</a>"
                e.Row.Items.FindItemByKey("Key_CANTIDADPDTE").Text = "<span id='lblCantPdte_" & DBNullToStr(oData("IDLINEA")) & "'>" & sCantidadPdte & "</span>"

            Else
                e.Row.Items.FindItemByKey("Key_NUM_LINEA").Text = "<label id='" & DBNullToStr(oData("IDLINEA")) & "' >" & Format(DBNullToInteger(oData("NUM_LINEA")), "000").ToString() & "</label>"
                Dim dImportePedido As Double
                Try
                    dImportePedido = Double.Parse(DBNullToStr(oData("IMPORTE_PEDIDO").ToString()))
                    dImportePedido = dImportePedido * Double.Parse(DBNullToStr(oData("CAMBIO").ToString()))
                Catch ex As Exception

                End Try

                Dim dPrecActual As Double
                Dim dPrecActualConBajas As Double
                If (lblImpBruto_hdd.Text <> String.Empty) Then
                    dPrecActual = Double.Parse(lblImpBruto_hdd.Text)
                Else
                    dPrecActual = 0
                End If
                If Not DBNullToBoolean(oData("BAJA_LOG")) OrElse DBNullToBoolean(oData("OE_BAJA_LOG")) Then
                    dPrecActual = dPrecActual + dImportePedido
                    dPrecActualConBajas = dPrecActual
                Else
                    dPrecActualConBajas = dPrecActual + dImportePedido
                End If

                e.Row.Items.FindItemByKey("Key_PREC").Text = txtNoAplica
                e.Row.Items.FindItemByKey("Key_PREC").CssClass = "apagado"
                e.Row.Items.FindItemByKey("Key_CANT").Text = txtNoAplica
                e.Row.Items.FindItemByKey("Key_CANT").CssClass = "apagado"
                e.Row.Items.FindItemByKey("Key_CANTIDADPDTE").Text = txtNoAplica
                e.Row.Items.FindItemByKey("Key_CANTIDADPDTE").CssClass = "apagado"

                lblImpBruto_hdd.Text = dPrecActual.ToString()
                lblImpBrutoConBajas_hdd.Text = dPrecActualConBajas.ToString()
                lblImpBrutoLinea_hdd.Text = dPrecActual.ToString()
                lblImpBrutoLineaConBajas_hdd.Text = dPrecActualConBajas.ToString()

                If Request("Desde") = 3 Then
                    'Desde seguimiento no se podra modificar el importe de pedido
                    e.Row.Items.FindItemByKey("Key_IMPBRUTO").Text = "<label name='lblImpBruto" & DBNullToStr(oData("IDLINEA")) & "' id='lblImpBruto" & DBNullToStr(oData("IDLINEA")) & "'>" & FSNLibrary.FormatNumber(dImportePedido, Me.Usuario.NumberFormat) & "</>" _
                                & "<label id='lblImpBruto" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;'>" & dImportePedido.ToString().Replace(",", ".") & "</label>"
                Else
                    If oInstancia.DetalleEditable Then
                        e.Row.Items.FindItemByKey("Key_IMPBRUTO").Text = "<input name='lblImpBruto" & DBNullToStr(oData("IDLINEA")) & "' id='lblImpBruto" & DBNullToStr(oData("IDLINEA")) & "' value='" & dImportePedido.ToString() & "' onBlur='if(validaFloat(this,null," & DBNullToStr(oData("IDLINEA")) & ")){cambiarImporteBruto(" & DBNullToStr(oData("IDLINEA")) & ")};' />" _
                                    & "<label id='lblImpBruto" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;'>" & dImportePedido.ToString().Replace(",", ".") & "</label>"
                    Else
                        e.Row.Items.FindItemByKey("Key_IMPBRUTO").Text = "<label name='lblImpBruto" & DBNullToStr(oData("IDLINEA")) & "' id='lblImpBruto" & DBNullToStr(oData("IDLINEA")) & "'>" & FSNLibrary.FormatNumber(dImportePedido, Me.Usuario.NumberFormat) & "</>" _
                                & "<label id='lblImpBruto" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;'>" & dImportePedido.ToString().Replace(",", ".") & "</label>"
                    End If
                End If

                e.Row.Items.FindItemByKey("Key_UNI").Text = "<span id='Uni" & DBNullToStr(oData("IDLINEA")) & "' style='display:none;' >" & oUnidadPedido.Codigo & "</span>"

                e.Row.Items.FindItemByKey("Key_ART_DEN").Text = "<a id='COD_ITEM' >" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "</a>"
            End If

            e.Row.Items.FindItemByKey("Key_COSTES").Text = "<span id='lblCosteTotalLinea_" & DBNullToStr(oData("IDLINEA")) & "' onclick='mostrarLineaCostes(" & DBNullToStr(oData("IDLINEA")) & ", " & DBNullToStr(oData("IDLINEA")) & ");' style='text-decoration:underline;'></span>" _
                & "<label id='lblCosteTotalLinea_" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;' ></label>"

            e.Row.Items.FindItemByKey("Key_DESCUENTOS").Text = "<span id='lblDescuentoTotalLinea_" & DBNullToStr(oData("IDLINEA")) & "' onclick='mostrarLineaDescuentos(" & DBNullToStr(oData("IDLINEA")) & ", " & DBNullToStr(oData("IDLINEA")) & ");' style='text-decoration:underline;'></span>" _
                & "<label id='lblDescuentoTotalLinea_" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;' ></label>"

            e.Row.Items.FindItemByKey("Key_IMPORTEPDTE").Text = "<span id='lblImportePdte_" & DBNullToStr(oData("IDLINEA")) & "'>" & FSNLibrary.FormatNumber(oData("IMPORTE_PENDIENTE") * oData("CAMBIO"), Me.Usuario.NumberFormat) & "</span>"

            Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
            If SQLBinaryToBoolean(oData("BAJA_LOG")) Then
                e.Row.Items.FindItemByKey("Key_EST").Text = "<span id='lblEstado_" & DBNullToStr(oData("IDLINEA")) & "'>" & Textos(301) & "</span>"
            Else
                If Not IsDBNull(oData("EST")) Then
                    If CType(Request.QueryString("estado"), CPedido.Estado) = 21 Then
                        e.Row.Items.FindItemByKey("Key_EST").Text = "<span id='lblEstado_" & DBNullToStr(oData("IDLINEA")) & "'>" & Textos(39) & "</span>"
                    Else
                        Select Case oData("EST")
                            Case 0
                                e.Row.Items.FindItemByKey("Key_EST").Text = "<span id='lblEstado_" & DBNullToStr(oData("IDLINEA")) & "'>" & Textos(46) & "</span>"
                            Case 1
                                If CType(Request.QueryString("estado"), CPedido.Estado) = 3 Then
                                    e.Row.Items.FindItemByKey("Key_EST").Text = "<span id='lblEstado_" & DBNullToStr(oData("IDLINEA")) & "'>" & Textos(100) & "</span>"
                                Else
                                    If CType(Request.QueryString("estado"), CPedido.Estado) >= 2 Then
                                        e.Row.Items.FindItemByKey("Key_EST").Text = "<span id='lblEstado_" & DBNullToStr(oData("IDLINEA")) & "'>" & Textos(92) & "</span>"
                                    Else
                                        e.Row.Items.FindItemByKey("Key_EST").Text = "<span id='lblEstado_" & DBNullToStr(oData("IDLINEA")) & "'>" & Textos(47) & "</span>"
                                    End If
                                End If
                            Case 2
                                e.Row.Items.FindItemByKey("Key_EST").Text = "<span id='lblEstado_" & DBNullToStr(oData("IDLINEA")) & "'>" & Textos(48) & "</span>"
                            Case 3
                                e.Row.Items.FindItemByKey("Key_EST").Text = "<span id='lblEstado_" & DBNullToStr(oData("IDLINEA")) & "'>" & Textos(49) & "</span>"
                            Case 20
                                e.Row.Items.FindItemByKey("Key_EST").Text = "<span id='lblEstado_" & DBNullToStr(oData("IDLINEA")) & "'>" & Textos(50) & "</span>"
                            Case 21
                                e.Row.Items.FindItemByKey("Key_EST").Text = "<span id='lblEstado_" & DBNullToStr(oData("IDLINEA")) & "'>" & Textos(51) & "</span>"
                            Case 23
                                e.Row.Items.FindItemByKey("Key_EST").Text = "<span id='lblEstado_" & DBNullToStr(oData("IDLINEA")) & "'>" & Textos(288) & "</span>"
                        End Select
                    End If
                End If
            End If
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
            If Not IsDBNull(oData("FECENTREGA")) Then _
                e.Row.Items.FindItemByKey("Key_FECENTREGA").Text = FSNLibrary.FormatDate(oData("FECENTREGA"), Me.Usuario.DateFormat) &
                        IIf(CType(oData("ENTREGA_OBL"), Boolean), "<sup>(*)</sup>", "")

            If Not IsDBNull(oData("FECENTREGAPROVE")) Then _
                e.Row.Items.FindItemByKey("Key_FECENTREGAPROVE").Text = FSNLibrary.FormatDate(oData("FECENTREGAPROVE"), Me.Usuario.DateFormat)

            If CType(oData("TIENE_FRAS"), Boolean) Then
                e.Row.Items.FindItemByKey("Key_FRA").Text = "<img id='img_Fra_" & DBNullToStr(oData("IDLINEA")) & "' " &
                    "onclick='Facturas(" & DBNullToInteger(oData("IDLINEA")) & "," & DBNullToInteger(oData("NUM_LINEA")) & ")'" &
                    "src='" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Page.Theme & "/images/facturas.gif' style='text-align:center;cursor:pointer;' />"
            Else
                e.Row.Items.FindItemByKey("Key_FRA").Text = "<img id='img_Fra_" & DBNullToStr(oData("IDLINEA")) & "' " &
                    "src='" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Page.Theme & "/images/facturas_desactivado.gif' style='text-align:center;' />"
            End If
            If CType(oData("TIENE_PAGOS"), Boolean) Then
                e.Row.Items.FindItemByKey("Key_PAGO").Text = "<img id='img_Pago_" & DBNullToStr(oData("IDLINEA")) & "' " &
                    "onclick='Pagos(" & DBNullToInteger(oData("IDLINEA")) & ")'" &
                    "src='" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Page.Theme & "/images/pagos.gif' style='text-align:center;cursor:pointer;' />"
            Else
                e.Row.Items.FindItemByKey("Key_PAGO").Text = "<img id='img_Pago_" & DBNullToStr(oData("IDLINEA")) & "' " &
                    "src='" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Page.Theme & "/images/pagos_desactivado.gif' style='text-align:center;' />"
            End If

            'Apareceran los botones de aprobar y rechazar siempre que las lineas sean de la seguridad del aprobador(gestionable) y si es un flujo 
            'que tenga el campo "Desglose de pedido" editable
            If DBNullToInteger(oData("GESTIONABLE")) > 0 Then
                If DBNullToInteger(oData("EST")) = 20 Then 'Denegada
                    e.Row.Items.FindItemByKey("Key_RECHAZAR").Text = " <img id='img_Rechazar_" & DBNullToStr(oData("IDLINEA")) & "' onclick=""DenegarLineaPedido(" & DBNullToStr(oData("IDLINEA")) & "," & Desde & ");"" src='" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Page.Theme & "/images/rechaz_enable.gif' style='text-align:center;cursor:pointer;' />"
                    If (DBNullToInteger(oData("NIVEL_APROB")) <> 1) Or (DBNullToInteger(oData("NIVEL_APROB")) = 1 AndAlso ((oInstancia.AprobadorActual = FSNUser.CodPersona) Or DBNullToInteger(oData("ES_APROBADOR")) = 1)) Then
                        e.Row.Items.FindItemByKey("Key_APROBAR").Text = " <img id='img_Aprobar_" & DBNullToStr(oData("IDLINEA")) & "' onclick=""AprobarLineaPedido(" & DBNullToStr(oData("IDLINEA")) & "," & Desde & ");"" src='" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Page.Theme & "/images/Aprob_disable.gif' style='text-align:center;cursor:pointer;' />"
                    End If
                ElseIf DBNullToInteger(oData("EST")) = 1 Then 'Aprobada
                    If (DBNullToInteger(oData("NIVEL_APROB")) <> 1) Or (DBNullToInteger(oData("NIVEL_APROB")) = 1 AndAlso ((oInstancia.AprobadorActual = FSNUser.CodPersona) Or DBNullToInteger(oData("ES_APROBADOR")) = 1)) Then
                        e.Row.Items.FindItemByKey("Key_RECHAZAR").Text = " <img id='img_Rechazar_" & DBNullToStr(oData("IDLINEA")) & "' onclick=""DenegarLineaPedido(" & DBNullToStr(oData("IDLINEA")) & "," & Desde & ");"" src='" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Page.Theme & "/images/rechaz_disable.gif' style='text-align:center;cursor:pointer;' />"
                    End If
                    e.Row.Items.FindItemByKey("Key_APROBAR").Text = " <img id='img_Aprobar_" & DBNullToStr(oData("IDLINEA")) & "' onclick=""AprobarLineaPedido(" & DBNullToStr(oData("IDLINEA")) & "," & Desde & ");"" src='" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Page.Theme & "/images/Aprob_enable.gif' style='text-align:center;cursor:pointer;' />"
                ElseIf DBNullToInteger(oData("EST")) = 0 Then
                    If (DBNullToInteger(oData("NIVEL_APROB")) <> 1) Or (DBNullToInteger(oData("NIVEL_APROB")) = 1 AndAlso ((oInstancia.AprobadorActual = FSNUser.CodPersona) Or DBNullToInteger(oData("ES_APROBADOR")) = 1)) Then
                        'Pendiente de aprobar
                        e.Row.Items.FindItemByKey("Key_RECHAZAR").Text = " <img id='img_Rechazar_" & DBNullToStr(oData("IDLINEA")) & "' onclick=""DenegarLineaPedido(" & DBNullToStr(oData("IDLINEA")) & "," & Desde & ");"" src='" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Page.Theme & "/images/rechaz_disable.gif' style='text-align:center;cursor:pointer;' />"
                        e.Row.Items.FindItemByKey("Key_APROBAR").Text = " <img id='img_Aprobar_" & DBNullToStr(oData("IDLINEA")) & "' onclick=""AprobarLineaPedido(" & DBNullToStr(oData("IDLINEA")) & "," & Desde & ");"" src='" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Page.Theme & "/images/Aprob_disable.gif' style='text-align:center;cursor:pointer;' />"
                    End If
                End If
            End If

            'Si la línea tiene recepciones:boton habilitado
            Dim iEstadoLinea As Integer = DBNullToInteger(oData("EST"))
            Dim recepcionHabilitado As Boolean = True
            If Not IsDBNull(iEstadoLinea) Then
                If Not iEstadoLinea = CLinea.Estado.RecibidoEnSuTotalidad AndAlso Not iEstadoLinea = CLinea.Estado.RecibidoParcialmente Then
                    'si no: boton deshabilitado
                    recepcionHabilitado = False
                End If
            Else
                'si no: boton deshabilitado
                recepcionHabilitado = False
            End If
            If recepcionHabilitado Then
                e.Row.Items.FindItemByKey("Key_RECEPCIONES").Text = "<img id='img_Recepciones_" & DBNullToStr(oData("IDLINEA")) & "' onclick='RecepcionesLinea(" & DBNullToInteger(oData("IDLINEA")) & ")' src='" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Page.Theme & "/images/recepcion.gif' style='text-align:center;cursor:pointer;' />"
            Else
                e.Row.Items.FindItemByKey("Key_RECEPCIONES").Text = "<img id='img_Recepciones_" & DBNullToStr(oData("IDLINEA")) & "' src='" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Page.Theme & "/images/recepcion_desactivado.gif' style='text-align:center;' />"
            End If
            Dim idInstancia As Integer = DBNullToInteger(oData("INSTANCIA_APROB"))
            If idInstancia <> 0 Then
                e.Row.Items.FindItemByKey("Key_INSTANCIA_APROB").Text = oInstanciaLinea(idInstancia).DenEtapaActual & " <img onclick='VerFlujo(" & DBNullToInteger(idInstancia) & ")' src='" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Page.Theme & "/images/coment.gif' />"
            End If

            If Not InvisibilidadBtBorrarLin Then
                Dim sBotonera As String = ""
                If FSNUser.EPPermitirBorrarLineasPedido Then
                    sBotonera = " <img id='img_Baja_" & DBNullToStr(oData("IDLINEA")) & "' onclick=""BorrarLineaPedido(" & DBNullToStr(oData("IDLINEA")) & "); return false;"" src='" & ConfigurationManager.AppSettings("ruta") & "/images/HacerBorrado.gif' style='text-align:center;cursor:pointer;"
                Else
                    sBotonera = " <img id='img_Baja_" & DBNullToStr(oData("IDLINEA")) & "' style='"
                End If
                If Not DBNullToBoolean(oData("BAJA_LOG")) Then
                    sBotonera = sBotonera & "' />"
                Else
                    sBotonera = sBotonera & "visibility:hidden;width:0px;' />"
                End If
                If FSNUser.EPPermitirDeshacerBorradoLineasPedido Then
                    sBotonera = sBotonera & " <img id='img_DeshBaja_" & DBNullToStr(oData("IDLINEA")) & "' onclick=""DeshacerBorrarLineaPedido(" & DBNullToStr(oData("IDLINEA")) & "); return false;"" src='" & ConfigurationManager.AppSettings("ruta") & "/images/DeshacerBorrado.gif' style='text-align:center;cursor:pointer;"
                Else
                    sBotonera = sBotonera & " <img id='img_DeshBaja_" & DBNullToStr(oData("IDLINEA")) & "' style='"
                End If
                If Not DBNullToBoolean(oData("BAJA_LOG")) Then
                    sBotonera = sBotonera & "visibility:hidden;width:0px;float:left;' />"
                Else
                    sBotonera = sBotonera & "float:left;' />"
                End If

                e.Row.Items.FindItemByKey("Key_DARBAJA").Text = sBotonera
            End If

            If Not SQLBinaryToBoolean(oData("OE_BAJA_LOG")) AndAlso SQLBinaryToBoolean(oData("BAJA_LOG")) Then
                e.Row.CssClass = "LineaPedidoBajaLogica"
                'Las celdas sin cssClass. Borrar línea y Deshacer Línea, solo, tocan el css del <TR>, no de los <TD>.
            End If
        End If
    End Sub
#End Region
#Region "Web Methods"
    ''' <summary>
    ''' Obtiene los datos de la pestaña de lineas de pedido
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function Cargar_Lineas(ByVal sIden As String, ByVal sInstanciaAprob As String) As Object
        Try
            Dim serializer As New JavaScriptSerializer()
            Dim iId As Integer
            Dim iInstanciaAprob As Long

            Dim oAtributos As CAtributosPedido
            Dim oAtributo As CAtributoPedido
            Dim oLinea As CLineaPedido
            Dim oDetallePedido As New CDetallePedido
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oDetallePedidos As cDetallePedidos
            Dim oLineas As CLineasPedido

            oDetallePedidos = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))

            Int32.TryParse(sIden, iId)
            Int32.TryParse(sInstanciaAprob, iInstanciaAprob)
            Dim PresDen(3) As String
            PresDen(0) = FSNServer.TipoAcceso.gbPres1Denominacion(FSNUser.Idioma.ToString)
            PresDen(1) = FSNServer.TipoAcceso.gbPres2Denominacion(FSNUser.Idioma.ToString)
            PresDen(2) = FSNServer.TipoAcceso.gbPres3Denominacion(FSNUser.Idioma.ToString)
            PresDen(3) = FSNServer.TipoAcceso.gbPres4Denominacion(FSNUser.Idioma.ToString)

            oLineas = oDetallePedidos.Cargar_Lineas(FSNUser.CodPersona, FSNUser.Idioma, iId, iInstanciaAprob, PresDen, FSNServer.TipoAcceso.gbBajaLOGPedidos, (iInstanciaAprob <> 0))

            For Each oLinea In oLineas
                Dim listaCostes As New List(Of CAtributoPedido)
                oAtributos = oDetallePedidos.Cargar_Costes_Linea(oLinea.ID, FSNUser.Idioma)
                For Each oAtributo In oAtributos
                    listaCostes.Add(oAtributo)
                Next
                oLinea.Costes = listaCostes

                Dim listaDescuentos As New List(Of CAtributoPedido)
                oAtributos = oDetallePedidos.Cargar_Descuentos_Linea(oLinea.ID)
                For Each oAtributo In oAtributos
                    listaDescuentos.Add(oAtributo)
                Next
                oLinea.Descuentos = listaDescuentos

                Dim listaOtrosDatos As New List(Of CAtributoPedido)
                oAtributos = oDetallePedidos.Cargar_Atributos_Linea(oLinea.ID)
                For Each oAtributo In oAtributos
                    listaOtrosDatos.Add(oAtributo)
                Next
                oLinea.OtrosDatos = listaOtrosDatos
            Next

            Return serializer.Serialize(oLineas)

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene los archivos adjuntos de la cabecera de la orden
    ''' </summary>
    ''' <param name="sIden">Identificador de la cesta</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function Cargar_Archivos(ByVal sIden As String) As Object
        Try
            Dim serializer As New JavaScriptSerializer()

            Dim iId As Integer
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim cDetallePedido As cDetallePedidos
            cDetallePedido = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))

            Int32.TryParse(sIden, iId)

            Dim oAdjuntos As CAdjuntos
            oAdjuntos = cDetallePedido.Cargar_Adjuntos_Cabecera_Orden(FSNUser.CodPersona, iId)

            Return serializer.Serialize(oAdjuntos)

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene los archivos adjuntos de la linea de la orden
    ''' </summary>
    ''' <param name="sIdenLinea">Identificador de la cesta</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function Cargar_Archivos_Linea(ByVal sIdenLinea As String) As Object
        Try
            Dim serializer As New JavaScriptSerializer()
            Dim iIdLinea As Integer
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim cDetallePedido As cDetallePedidos
            cDetallePedido = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))

            Int32.TryParse(sIdenLinea, iIdLinea)

            Dim oAdjuntos As CAdjuntos
            oAdjuntos = cDetallePedido.Cargar_Adjuntos_Linea_Orden(FSNUser.CodPersona, iIdLinea)

            Return serializer.Serialize(oAdjuntos)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene los datos de la pestaña datos generales
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function Cargar_Datos_Generales(ByVal sIden As String) As Object
        Try
            Dim serializer As New JavaScriptSerializer()

            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim cDetallePedidos As cDetallePedidos
            cDetallePedidos = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))

            Dim listaCostes As New List(Of CAtributoPedido)
            Dim listaDescuentos As New List(Of CAtributoPedido)
            Dim iId As Integer

            Dim cDetallePedido As New CDetallePedido

            Int32.TryParse(sIden, iId)

            Dim oDatosDetallePedido As Object = cDetallePedidos.Cargar_Datos_Generales_Orden(FSNUser.CodPersona, iId, FSNUser.IdiomaCod)
            Dim oDatosGenerales As CDatosGenerales
            oDatosGenerales = CType(oDatosDetallePedido(0), CDatosGenerales)
            cDetallePedido.DatosGenerales = oDatosGenerales
            cDetallePedido.IdPedido = CType(oDatosDetallePedido(1), Integer)
            Return serializer.Serialize(cDetallePedido)

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Cargar Atributos Item
    ''' </summary>
    ''' <param name="sIDOrden">Id de la orden</param>
    ''' <returns>Atributos</returns>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function CargarAtributosOrden(ByVal sIDOrden As String) As Object
        Try

            Dim serializer As New JavaScriptSerializer()

            Dim iIdOrden As Integer
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim cDetallePedidos As cDetallePedidos
            cDetallePedidos = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))


            Int32.TryParse(sIDOrden, iIdOrden)
            Dim oAtributos As CAtributosPedido
            oAtributos = cDetallePedidos.Cargar_Atributos(iIdOrden)


            Return serializer.Serialize(oAtributos)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Cargar Atributos Costes de la orden de entrega
    ''' </summary>
    ''' <param name="sIdOrden">id de la orden de entrega</param>
    ''' <returns>Atributos</returns>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function CargarAtributosCostes(ByVal sIdOrden As String) As Object
        Try

            Dim serializer As New JavaScriptSerializer()

            Dim idOrden As Long
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oDetallePedidos As cDetallePedidos
            oDetallePedidos = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))


            Int32.TryParse(sIdOrden, idOrden)
            Dim oAtributos As CAtributosPedido
            oAtributos = oDetallePedidos.Cargar_Costes(idOrden, FSNUser.Idioma.ToString())

            Return serializer.Serialize(oAtributos)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Cargar Atributos Descuentos de la orden de entrega
    ''' </summary>
    ''' <param name="sIdOrden">id de la orden de entrega</param>
    ''' <returns>Atributos</returns>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function CargarAtributosDescuentos(ByVal sIdOrden As Long) As Object
        Try

            Dim serializer As New JavaScriptSerializer()

            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oDetallePedidos As cDetallePedidos
            oDetallePedidos = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))

            Dim oAtributos As CAtributosPedido
            oAtributos = oDetallePedidos.Cargar_Descuentos(sIdOrden)

            Return serializer.Serialize(oAtributos)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function seleccionaGestorWM(ByVal Partida As String) As String
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            If Partida <> String.Empty And Partida <> "null" Then
                Dim sPRES() As String = Partida.Split("#")
                Dim sPRES0 As String = sPRES(0)
                Dim sGestor As String
                Dim oPartidas As PartidasPRES5 = FSNServer.Get_Object(GetType(PartidasPRES5))
                oPartidas.ObtGestoresPartidasUsu(FSNUser.CodPersona, sPRES0)
                sGestor = oPartidas.Find(Function(oParPres As FSNServer.PartidaPRES5) oParPres.NIV0 = sPRES0).GestorCod '+ " - " + oPartidas.Find(Function(oParPres As FSNServer.PartidaPRES5) oParPres.NIV0 = sPRES0).GestorDen
                If sGestor = "" Then
                    Return String.Empty
                End If
                Return sGestor
            Else
                Return String.Empty
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Cargar Atributos Costes de las lineas
    ''' </summary>
    ''' <param name="iCodLinea">iCodPedido</param>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function CargarAtributosCostesLinea(ByVal iCodLinea As Integer) As Object
        Try

            Dim serializer As New JavaScriptSerializer()


            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oDetallePedidos As cDetallePedidos
            oDetallePedidos = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))

            Dim oAtributos As CAtributosPedido
            oAtributos = oDetallePedidos.Cargar_Costes_Linea(iCodLinea, FSNUser.Idioma)

            Return serializer.Serialize(oAtributos)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Cargar Atributos Descuentos de la linea
    ''' </summary>
    ''' <param name="iCodLinea">iCodLinea</param>
    ''' <returns>Atributos</returns>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function CargarAtributosDescuentosLinea(ByVal iCodLinea As Integer) As Object
        Try

            Dim serializer As New JavaScriptSerializer()

            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oDetallePedidos As cDetallePedidos
            oDetallePedidos = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))

            Dim oAtributos As CAtributosPedido
            oAtributos = oDetallePedidos.Cargar_Descuentos_Linea(iCodLinea)

            Return serializer.Serialize(oAtributos)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Cargar Atributos Linea
    ''' </summary>
    ''' <param name="iCodLinea">CodLinea</param>
    ''' <returns>Atributos</returns>
    ''' <remarks>Llamada desde: EmisionPedido.aspx; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function CargarAtributosLinea(ByVal iCodLinea As Integer) As Object
        Try

            Dim serializer As New JavaScriptSerializer()

            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oDetallePedidos As cDetallePedidos
            oDetallePedidos = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))

            Dim oAtributos As CAtributosPedido
            oAtributos = oDetallePedidos.Cargar_Atributos_Linea(iCodLinea)


            Return serializer.Serialize(oAtributos)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' ComprobarCantidades
    ''' </summary>
    ''' <param name="IDLinea">IDLinea</param>
    ''' <returns>Atributos</returns>
    ''' <remarks>; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ComprobarCantidades(ByVal idLinea As Integer, ByVal sUPed As String, ByVal dCant As Double, ByVal sPedido As String) As Object
        Try
            Dim serializer As New JavaScriptSerializer()

            Dim oDetallePedido As CDetallePedido = serializer.Deserialize(Of CDetallePedido)(sPedido)
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim iValidacionCantidad As Integer = 0
            Dim oDetallePedidos As cDetallePedidos
            oDetallePedidos = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))


            iValidacionCantidad = oDetallePedidos.ComprobarCantidadesLineaOrden(idLinea, sUPed, dCant, oDetallePedido.Lineas)


            Return serializer.Serialize(iValidacionCantidad)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Comprobara el estado de la linea y como quedara la orden de entrega
    ''' </summary>
    ''' <param name="sIdLinea">id de la linea de pedido</param>
    ''' <param name="sIdOrden">id de la orden</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ComprobarAprobacionDenegacionLinea(ByVal Aprobar As Boolean, ByVal sIdLinea As String, ByVal sIdOrden As String,
                            ByVal iIdPedido As Integer, ByVal iTipoPedido As Integer, ByVal iIdEmpresa As Integer, ByVal Motivo As String,
                            ByVal instanciaAprob As String, ByVal sMoneda As String, ByVal iTipo As TipoDePedido) As Integer
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim idLinea, idOrden As Long
            Dim oOrden As COrden = FSNServer.Get_Object(GetType(COrden))
            Int32.TryParse(sIdLinea, idLinea)
            Int32.TryParse(sIdOrden, idOrden)
            oOrden.ID = idOrden

            Dim resp As Integer
            resp = oOrden.ConfirmarAprobacionDenegacion(sIdLinea, FSNUser.CodPersona, Aprobar, Motivo)

            If resp = 10 Or resp = 6 Then '10 El pedido se emitira/ 6: Lineas aprobadas y rechazadas, se comprueba si las aprobadas se pueden emitir para pasar a denegado parcialmente
                'ALERT E IR A VISOR
                Dim oEmisionPedidos As CEmisionPedidos = FSNServer.Get_Object(GetType(CEmisionPedidos))
                Dim dsSolicitudesDePedido As DataSet
                If iTipo = TipoDePedido.Aprovisionamiento Then
                    dsSolicitudesDePedido = oEmisionPedidos.ValidacionesEmisionPedidoCatalogo(iIdPedido, Nothing, iTipoPedido, iIdEmpresa, True)
                    If dsSolicitudesDePedido.Tables.Count > 0 AndAlso dsSolicitudesDePedido.Tables(0).Rows.Count > 0 Then
                        resp = 7
                        GenerarWorkFlowSolicitudPedido(dsSolicitudesDePedido, FSNUser.CodPersona, FSNUser.IdiomaCod, iIdEmpresa, iIdPedido, sMon:=sMoneda)
                    Else
                        'Sumamos el importe comprometido del SM
                        Dim obDetallePedido As cDetallePedidos = FSNServer.Get_Object(GetType(cDetallePedidos))
                        obDetallePedido.ActualizarImputacion(idOrden)
                        'Llamamos al servicio de integración WCF.
                        Dim oIntegracion As Integracion = FSNServer.Get_Object(GetType(Integracion))
                        oIntegracion.LlamarFSIS(TablasIntegracion.PED_Aprov, oOrden.ID)
                    End If
                End If
            End If
            EliminarCachePedidos(oOrden.ID, FSNUser.CodPersona, False, False, True, instanciaAprob)
            Return resp
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene los datos de costes y descuentos del pedido
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function Cargar_Costes_Descuentos(ByVal sIden As String) As Object
        Try
            Dim serializer As New JavaScriptSerializer()

            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oDetallePedidos As cDetallePedidos
            oDetallePedidos = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))


            Dim listaCostes As New List(Of CAtributoPedido)
            Dim listaDescuentos As New List(Of CAtributoPedido)
            Dim iCodPedido As Long

            Dim oAtributos As CAtributosPedido
            Dim oAtributo As CAtributoPedido

            Dim oDetallePedido As New CDetallePedido

            Int32.TryParse(sIden, iCodPedido)

            oAtributos = oDetallePedidos.Cargar_Costes(iCodPedido, FSNUser.Idioma.ToString)
            For Each oAtributo In oAtributos
                listaCostes.Add(oAtributo)
            Next

            oDetallePedido.Costes = listaCostes

            oAtributos = oDetallePedidos.Cargar_Descuentos(iCodPedido)
            For Each oAtributo In oAtributos
                listaDescuentos.Add(oAtributo)
            Next
            oDetallePedido.Descuentos = listaDescuentos

            Return serializer.Serialize(oDetallePedido)

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function DevolverSiguientesEtapas(ByVal idAccion As Long, ByVal tipoSolicitud As TiposDeDatos.TipoDeSolicitud) As Object
        Dim oEtapas As DataSet
        Dim oInstancia As FSNServer.Instancia
        Dim oAccion As FSNServer.Accion
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim serializer As New JavaScriptSerializer()

        oInstancia = CType(HttpContext.Current.Cache("oInstancia" & FSNUser.Cod), FSNServer.Instancia)
        oEtapas = oInstancia.DevolverSiguientesEtapas(idAccion, FSNUser.CodPersona, FSNUser.Idioma, , , , , tipoSolicitud)

        oAccion = FSNServer.Get_Object(GetType(FSNServer.Accion))
        oAccion.Id = idAccion
        oAccion.CargarAccion(FSNUser.IdiomaCod)

        Dim oSiguientesEtapas As New JSonSiguientesEtapas

        oSiguientesEtapas.DenAccion = oAccion.Den
        If oEtapas.Tables.Count > 1 Then
            'Etapas
            For Each oRowEtapa As DataRow In oEtapas.Tables(0).Rows
                Dim oEtapa As New Etapas
                oEtapa.Den = oRowEtapa("DEN")
                oSiguientesEtapas.Etapas.Add(oEtapa)
            Next
            'Roles
            For Each oRowRol As DataRow In oEtapas.Tables(1).Rows
                Dim oRol As New Roles
                If DBNullToStr(oRowRol("NOMBRE")) <> String.Empty Then
                    oRol.Tipo = 1
                    oRol.Nombre = DBNullToStr(oRowRol("NOMBRE"))
                    oRol.Per = DBNullToStr(oRowRol("PER"))
                End If

                oRol.Den = DBNullToStr(oRowRol("DEN"))
                oRol.Rol = DBNullToStr(oRowRol("ROL"))
                oSiguientesEtapas.Roles.Add(oRol)
            Next
        End If

        Return serializer.Serialize(oSiguientesEtapas)
    End Function
    ''' <summary>
    ''' Ejecuta la accion continuando con el workflow de la solicitud de pedido y actualiza las lineas de pedido que se han modificado, con sus costes, descuentos, impuestos....
    ''' </summary>
    ''' <param name="sDetallePedido">string con todo el objeto del detalle de pedido de la clase cDetallePedido</param>
    ''' <param name="sIdOrden">id de la orden de entrega</param>
    ''' <param name="oCentroCoste">objeto con el centro de coste</param>
    ''' <param name="oPartidas">objeto con las partidas</param>
    ''' <param name="sIdAccion">id de la accion a realizar</param>
    ''' <param name="sIdBloqueActual">id del bloque actual de la instancia</param>
    ''' <param name="sObservaciones">Observaciones de la pantalla de confirmacion al confirmar la accion</param>
    ''' <param name="ImportePedido" >Importe del pedido</param>
    ''' <param name="ImporteSolicitud" >Importe de la solicitud si estamos en DetallePedido por haber superado el limite de pedido y se ha creado una solicitud</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function EjecutarAccion(ByVal sDetallePedido As String, ByVal sIdOrden As String, ByVal oCentroCoste As String, ByVal oPartidas As String, ByVal sIdAccion As String, ByVal sIdBloqueActual As String, ByVal sObservaciones As String, ByVal ImportePedido As Double, ByVal ImporteSolicitud As Double, ByVal idInstancia As Long, ByVal tipoSolicitud As TipoDeSolicitud) As Boolean
        Try
            Dim serializer As New JavaScriptSerializer()
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim UON As List(Of UON) = serializer.Deserialize(Of List(Of UON))(oCentroCoste)
            Dim oInstancia As FSNServer.Instancia
            Dim PartidaPres5 As List(Of PartidaPRES5) = serializer.Deserialize(Of List(Of PartidaPRES5))(oPartidas)

            Dim oPer As FSNServer.CPersona
            oPer = FSNServer.Get_Object(GetType(FSNServer.CPersona))
            oPer.Cod = FSNUser.CodPersona
            oPer.CargarDestinos(FSNUser.Idioma)
            Dim idOrden, idAccion, idBloqueActual As Long
            Dim oDetallePedido As CDetallePedido = serializer.Deserialize(Of CDetallePedido)(sDetallePedido)

            Dim oDetallePedidos As cDetallePedidos
            oDetallePedidos = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))

            oInstancia = CType(HttpContext.Current.Cache("oInstancia" & FSNUser.Cod), FSNServer.Instancia)

            'cargamos las partidas presupuestarias y su configuracion
            Dim oPargenSMS As FSNServer.PargensSMs
            oPargenSMS = FSNServer.Get_Object(GetType(FSNServer.PargensSMs))
            oPargenSMS.CargarConfiguracionSM(FSNUser.Cod, FSNUser.Idioma, True)

            Dim iConta As Integer = 0
            For Each oLinea As CLineaPedido In oDetallePedido.Lineas
                Dim Imputaciones As New List(Of Imputacion)
                Dim Imputacion As New Imputacion
                Imputacion.Id = oDetallePedido.DatosGenerales.Id
                Imputacion.LineaId = oLinea.ID
                Imputacion.CentroCoste = UON.Item(iConta)
                Imputacion.Partida = PartidaPres5.Item(iConta)
                Imputaciones.Add(Imputacion)
                oLinea.Pres5_ImportesImputados_EP = Imputaciones
                iConta = iConta + 1
            Next

            Dim _mipargenSMs As FSNServer.PargensSMs
            Dim _mipargenSM As FSNServer.PargenSM
            If FSNServer.TipoAcceso.gbAccesoFSSM Then
                _mipargenSMs = oPargenSMS
                If _mipargenSMs.Count > 0 Then
                    _mipargenSM = _mipargenSMs(0)
                Else
                    _mipargenSM = Nothing
                End If
            Else
                _mipargenSM = Nothing
            End If

            Int32.TryParse(sIdOrden, idOrden)
            Int32.TryParse(sIdAccion, idAccion)
            Int32.TryParse(sIdBloqueActual, idBloqueActual)

            oDetallePedidos.ActualizarPedido(FSNUser.CodPersona, FSNUser.Idioma, oPer.Cod, oDetallePedido.DatosGenerales.Moneda, oDetallePedido, idOrden, _mipargenSM, ImportePedido, ImporteSolicitud, idInstancia)
            If oInstancia.AprobacionCompletaCentroSM(oPer.Cod, idBloqueActual, idAccion) Then
                Dim oEtapas As DataSet
                oEtapas = oInstancia.DevolverSiguientesEtapas(idAccion, FSNUser.CodPersona, FSNUser.Idioma, , , , TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoCatalogo)

                'Recojo el bloque o bloques de destino de la accion
                Dim sBloquesDestino As String = String.Empty
                If oEtapas.Tables.Count > 0 Then
                    If oEtapas.Tables.Count > 1 Then
                        For Each oRow As DataRow In oEtapas.Tables(0).Rows
                            sBloquesDestino = sBloquesDestino & oRow.Item("BLOQUE") & " "
                        Next
                    End If
                End If

                'Ponemos la instancia en proceso
                oInstancia.Actualizar_En_proceso(1)

                Dim lIDTiempoProc As Long
                oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, FSNUser.Cod, idBloqueActual)

                'Creo el XML y llamo al webservice para que lo procese
                Dim sXMLName As String
                Dim dsXML As New DataSet
                dsXML.Tables.Add("SOLICITUD")
                With dsXML.Tables("SOLICITUD").Columns
                    .Add("TIPO_PROCESAMIENTO_XML")
                    .Add("TIPO_DE_SOLICITUD")
                    .Add("COMPLETO")
                    .Add("CODIGOUSUARIO")
                    .Add("INSTANCIA")           '1
                    .Add("SOLICITUD")           '2
                    .Add("USUARIO")             '3
                    .Add("USUARIO_EMAIL")       '4
                    .Add("USUARIO_IDIOMA")      '5
                    .Add("PEDIDO_DIRECTO")      '6
                    .Add("FORMULARIO")          '7
                    .Add("WORKFLOW")            '8
                    .Add("IMPORTE", System.Type.GetType("System.Double")) '9
                    .Add("COMENTALTANOCONF")    '10
                    .Add("ACCION")              '11
                    .Add("IDACCION")            '12
                    .Add("IDACCIONFORM")        '13
                    .Add("GUARDAR")             '14
                    .Add("NOTIFICAR")           '15
                    .Add("TRASLADO_USUARIO")    '16
                    .Add("TRASLADO_PROVEEDOR")  '17
                    .Add("TRASLADO_FECHA")      '18
                    .Add("TRASLADO_COMENTARIO") '19
                    .Add("TRASLADO_PROVEEDOR_CONTACTO") '20
                    .Add("DEVOLUCION_COMENTARIO")       '21
                    .Add("COMENTARIO")          '22
                    .Add("BLOQUE_ORIGEN")       '23
                    .Add("NUEVO_ID_INSTANCIA")  '24
                    .Add("BLOQUES_DESTINO")     '25
                    .Add("ROL_ACTUAL")          '26
                    .Add("PEDIDO")          '31
                    .Add("IDTIEMPOPROC")
                End With
                'Cargo los datos de la solicitud
                oInstancia.Solicitud.Load(FSNUser.Idioma.ToString())

                Dim drSolicitud As DataRow
                drSolicitud = dsXML.Tables("SOLICITUD").NewRow
                With drSolicitud
                    .Item("TIPO_PROCESAMIENTO_XML") = CInt(TipoProcesamientoXML.FSNWeb)
                    .Item("TIPO_DE_SOLICITUD") = CInt(tipoSolicitud)
                    .Item("COMPLETO") = 1
                    .Item("CODIGOUSUARIO") = FSNUser.Cod
                    .Item("INSTANCIA") = oInstancia.ID
                    .Item("SOLICITUD") = ""
                    .Item("USUARIO") = FSNUser.CodPersona
                    .Item("USUARIO_EMAIL") = DBNullToStr(FSNUser.Email)
                    .Item("USUARIO_IDIOMA") = FSNUser.Idioma.ToString()
                    .Item("PEDIDO_DIRECTO") = 0
                    .Item("FORMULARIO") = oInstancia.Solicitud.Formulario.Id
                    .Item("WORKFLOW") = oInstancia.Solicitud.Workflow
                    .Item("IMPORTE") = oInstancia.Importe
                    .Item("IDACCION") = idAccion
                    .Item("IDACCIONFORM") = idAccion
                    .Item("GUARDAR") = 1
                    .Item("NOTIFICAR") = 1
                    .Item("COMENTARIO") = sObservaciones
                    .Item("BLOQUE_ORIGEN") = idBloqueActual
                    .Item("BLOQUES_DESTINO") = sBloquesDestino
                    .Item("ROL_ACTUAL") = oInstancia.RolActual
                    .Item("PEDIDO") = CInt(tipoSolicitud)
                    .Item("IDTIEMPOPROC") = lIDTiempoProc
                End With
                dsXML.Tables("SOLICITUD").Rows.Add(drSolicitud)

                sXMLName = FSNUser.Cod & "#" & oInstancia.ID & "#" & idBloqueActual

                'Cargar XML completo
                oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=TiempoProcesamiento.InicioDisco)
                If ConfigurationManager.AppSettings("SERVIDOR_TRATAMIENTO_INSTANCIAS") = "0" Then
                    'Se hace con un StringWriter porque el método GetXml del dataset no incluye el esquema
                    Dim oSW As New System.IO.StringWriter()
                    dsXML.WriteXml(oSW, XmlWriteMode.WriteSchema)

                    Dim oSrvTrataInst As New FSNWebServiceXML.TratamientoInstancias
                    oSrvTrataInst.TransferirXMLEnEpera(oSW.ToString(), sXMLName)
                Else
                    dsXML.WriteXml(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & "#FSNWEB.xml", XmlWriteMode.WriteSchema)
                    If File.Exists(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml") Then _
                        File.Delete(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml")
                    FileSystem.Rename(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & "#FSNWEB.xml",
                                      ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml")
                End If

                Dim PeticUser As FSNServer.User = FSNServer.Get_Object(GetType(FSNServer.User))
                PeticUser.LoadUserData(oInstancia.Peticionario)

                If PeticUser.ModificarPrecios Then
                    HttpContext.Current.Cache.Remove("DsetArticulos_" & FSNUser.CodPersona)
                End If
            End If
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ComprobarPrecondiciones(ByVal oInstancia As FSNServer.Instancia, ByVal dsPrecondiciones As DataSet) As DataTable
        Dim dtAvisos As DataTable = New DataTable
        dtAvisos.Columns.Add("TIPO", Type.GetType("System.Int32"))
        dtAvisos.Columns.Add("COD", Type.GetType("System.String"))
        dtAvisos.Columns.Add("DEN", Type.GetType("System.String"))

        Dim bCumple As Boolean
        If dsPrecondiciones.Tables.Count > 0 Then
            For Each drPrecondicion As DataRow In dsPrecondiciones.Tables(0).Rows
                bCumple = ComprobarCondiciones(oInstancia, drPrecondicion.Item("FORMULA"), drPrecondicion.GetChildRows("PRECOND_CONDICIONES"))
                If bCumple Then
                    Dim newRow As DataRow = dtAvisos.NewRow
                    newRow.Item("TIPO") = drPrecondicion.Item("TIPO")
                    newRow.Item("COD") = drPrecondicion.Item("COD")
                    newRow.Item("DEN") = drPrecondicion.Item("DEN")
                    dtAvisos.Rows.Add(newRow)
                End If
            Next
        End If
        Return dtAvisos
    End Function
    ''' <summary>
    ''' Procedimiento que conprueba si se cumpln las precondiciones definidas en la acción
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: PmWeb/guardarInstancia/ComprobarPreCondiciones
    ''' Tiempo máximo: 1 seg</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ComprobarCondiciones(ByVal oInstancia As FSNServer.Instancia, ByVal sFormula As String, ByVal drCondiciones As DataRow()) As Boolean
        Dim oValorCampo As Object = Nothing
        Dim oValor As Object = Nothing
        Dim iTipo As TiposDeDatos.TipoGeneral
        Dim oDSFormula As New DataSet
        Dim oDSCond As New DataSet
        Dim oCond As DataRow
        Dim i As Long
        Dim sVariables() As String = Nothing
        Dim dValues() As Double = Nothing
        Dim iEq As New USPExpress.USPExpression

        Try
            i = 0
            For Each oCond In drCondiciones
                ReDim Preserve sVariables(i)
                ReDim Preserve dValues(i)

                sVariables(i) = DBNullToSomething(oCond.Item("COD"))
                dValues(i) = 0 'El valor será 1 o 0 dependiendo de si se cumple o no la condición

                '<EXPRESION IZQUIEDA> <OPERADOR> <EXPRESION DERECHA>
                'Primero evaluamos <EXPRESION IZQUIERDA> 
                Select Case oCond.Item("TIPO_CAMPO")
                    Case 7 'Importe de la solicitud de compra
                        oValorCampo = oInstancia.Importe
                        iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
                End Select

                'Luego <EXPRESION DERECHA>
                Select Case oCond.Item("TIPO_VALOR")
                    Case 7 'Importe de la solicitud de compra
                        oValor = oInstancia.Importe
                        iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
                End Select

                'y por último con el OPERADOR obtenemos el valor
                Select Case iTipo
                    Case 2, 3
                        If oValorCampo Is Nothing Then 'Condición vacía
                            If oValor Is Nothing Then
                                dValues(i) = 1
                            Else
                                dValues(i) = 0
                            End If
                        Else
                            Select Case oCond.Item("OPERADOR")
                                Case ">"
                                    If oValorCampo > oValor Then
                                        dValues(i) = 1
                                    Else
                                        dValues(i) = 0
                                    End If
                                Case "<"
                                    If oValorCampo < oValor Then
                                        dValues(i) = 1
                                    Else
                                        dValues(i) = 0
                                    End If
                                Case ">="
                                    If oValorCampo >= oValor Then
                                        dValues(i) = 1
                                    Else
                                        dValues(i) = 0
                                    End If
                                Case "<="
                                    If oValorCampo <= oValor Then
                                        dValues(i) = 1
                                    Else
                                        dValues(i) = 0
                                    End If
                                Case "="
                                    If oValorCampo = oValor Then
                                        dValues(i) = 1
                                    Else
                                        dValues(i) = 0
                                    End If
                                Case "<>"
                                    If oValorCampo <> oValor Then
                                        dValues(i) = 1
                                    Else
                                        dValues(i) = 0
                                    End If
                            End Select
                        End If
                    Case 4
                        If oValorCampo Is Nothing Then 'Condición vacía
                            If oValor Is Nothing Then
                                dValues(i) = 1
                            Else
                                dValues(i) = 0
                            End If
                        Else
                            If IIf(IsNothing(oValorCampo), -1, oValorCampo) = IIf(IsNothing(oValor), -1, oValor) Then
                                dValues(i) = 1
                            Else
                                dValues(i) = 0
                            End If
                        End If

                    Case Else
                        If oValorCampo Is Nothing Then 'Condición vacía
                            If oValor Is Nothing Then
                                dValues(i) = 1
                            Else
                                dValues(i) = 0
                            End If
                        Else
                            Select Case UCase(oCond.Item("OPERADOR"))
                                Case "="
                                    If UCase(oValorCampo) = UCase(oValor) Then
                                        dValues(i) = 1
                                    Else
                                        dValues(i) = 0
                                    End If

                                Case "LIKE"
                                    If Left(oValor, 1) = "*" Then
                                        If oValorCampo = Nothing Then 'Condición vacía
                                            If oValor = Nothing Then
                                                dValues(i) = 1
                                            Else
                                                dValues(i) = 0
                                            End If
                                        Else
                                            If Right(oValor, 1) = "*" Then
                                                oValor = oValor.ToString.Replace("*", "")
                                                If InStr(oValorCampo.ToString, oValor.ToString) >= 0 Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If
                                            Else
                                                oValor = oValor.ToString.Replace("*", "")
                                                If oValorCampo.ToString.EndsWith(oValor.ToString) Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If
                                            End If
                                        End If
                                    Else
                                        If oValorCampo = Nothing Then 'Condición vacía
                                            If oValor = Nothing Then
                                                dValues(i) = 1
                                            Else
                                                dValues(i) = 0
                                            End If
                                        Else
                                            If Right(oValor, 1) = "*" Then
                                                oValor = oValor.ToString.Replace("*", "")
                                                If oValorCampo.ToString.StartsWith(oValor.ToString) Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If
                                            Else
                                                If UCase(oValorCampo.ToString) = UCase(oValor.ToString) Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If

                                            End If
                                        End If
                                    End If
                                Case "<>"
                                    If Not (oValorCampo Is Nothing) Then
                                        If UCase(oValorCampo.ToString) <> UCase(oValor.ToString) Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Else
                                        dValues(i) = 0
                                    End If
                            End Select
                        End If
                End Select

                i += 1
            Next
            oValor = Nothing
            Try
                iEq.Parse(sFormula, sVariables)
                oValor = iEq.Evaluate(dValues)
            Catch ex As USPExpress.ParseException
            Catch e As Exception
            End Try
        Catch e As Exception
            Dim a As String = e.Message
        End Try


        Return (oValor > 0)
    End Function
    ''' <summary>
    ''' Genera los xml para los flujos de las solicitudes de pedido y llama al Web service
    ''' </summary>
    ''' <param name="dsSolicitudes">Dataset con las solicitudes a generar</param>
    ''' <param name="UsuCod">usuario aprovisionador</param>
    ''' <param name="Idioma">idioma del usuario</param>
    ''' <remarks></remarks>
    Private Shared Sub GenerarWorkFlowSolicitudPedido(ByVal dsSolicitudes As DataSet, ByVal UsuCod As String, ByVal Idioma As String, ByVal idEmp As Long, ByVal idPedido As Long, Optional ByVal idInstanciaAprob As Long = 0, Optional ByVal BloqueOrigen As Long = 0, Optional ByVal BloqueDestino As Long = 0, Optional ByVal Accion As Long = 0, Optional ByVal sMon As String = "")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim oSolicitud As Solicitud
        Dim oInstancia As Instancia
        Dim oEmisionPedidos As CEmisionPedidos
        oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        Dim YaRemoveCache As Boolean = False

        Try
            If dsSolicitudes Is Nothing Then
                'Si dsSolicitudes es nothing quiere decir que el motivo es 2(superar limite de pedido) y que se pasa de etapa en el flujo de aprobacion
                ', en este caso solo tendremos esa solicitud
                dsSolicitudes = New DataSet
                dsSolicitudes.Tables.Add("SOLICITUD")
                oInstancia.ID = idInstanciaAprob
                oInstancia.Load(FSNUser.IdiomaCod, True)
                With dsSolicitudes.Tables("SOLICITUD").Columns
                    .Add("IMPORTE")
                    .Add("SOLICITUD")
                    .Add("BLOQUE_ORIGEN")
                    .Add("BLOQUE_DESTINO")
                    .Add("ACCION")
                End With
                Dim arrDatosSolicitud(4) As Object
                arrDatosSolicitud(0) = oInstancia.Importe
                arrDatosSolicitud(1) = oInstancia.Solicitud.ID
                arrDatosSolicitud(2) = BloqueOrigen
                arrDatosSolicitud(3) = BloqueDestino
                arrDatosSolicitud(4) = Accion
                dsSolicitudes.Tables(0).Rows.Add(arrDatosSolicitud)
            End If

            For Each dr As DataRow In dsSolicitudes.Tables(dsSolicitudes.Tables.Count - 1).Rows
                oEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))
                oSolicitud.ID = dr("SOLICITUD")
                oSolicitud.Load(Idioma)
                oInstancia.Peticionario = UsuCod
                oInstancia.Solicitud = oSolicitud
                If idInstanciaAprob = 0 Then oInstancia.Create_Prev(idEmp, sMon)

                If oInstancia.ID > 0 Then
                    'Ponemos la instancia en proceso
                    oInstancia.Actualizar_En_proceso(1)

                    Dim lIDTiempoProc As Long
                    oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, FSNUser.Cod, BloqueOrigen)

                    If idInstanciaAprob = 0 Then
                        'Solo se actualiza si es aqui donde se crea la instancia y las lineas de pedido no tienen aun ,el id de la instancia(LINEAS_PEDIDO.INSTANCIA_APROB)
                        For Each drCat As DataRow In dsSolicitudes.Tables(0).Rows
                            oEmisionPedidos.ActualizarLineasPedido_InstanciaSolicitudPedido(drCat, idPedido, oInstancia.ID)
                        Next
                    End If
                    If BloqueOrigen = 0 Then
                        Dim ds As DataSet
                        ds = oEmisionPedidos.ObtenerBloque_Accion(oSolicitud.ID)
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                            BloqueOrigen = ds.Tables(0)(0)("BLOQUE_ORIGEN")
                            BloqueDestino = ds.Tables(0)(0)("BLOQUE_DESTINO")
                            Accion = ds.Tables(0)(0)("ACCION")
                        End If
                    End If

                    oInstancia.DevolverEtapaActual(FSNUser.Idioma.ToString, FSNUser.CodPersona, BloqueOrigen)
                    Dim sXMLName As String
                    Dim dsXML As New DataSet
                    dsXML.Tables.Add("SOLICITUD")
                    With dsXML.Tables("SOLICITUD").Columns
                        .Add("TIPO_PROCESAMIENTO_XML")
                        .Add("TIPO_DE_SOLICITUD")
                        .Add("COMPLETO")
                        .Add("CODIGOUSUARIO")
                        .Add("INSTANCIA")           '1
                        .Add("SOLICITUD")           '2
                        .Add("USUARIO")             '3
                        .Add("USUARIO_EMAIL")       '4
                        .Add("USUARIO_IDIOMA")      '5
                        .Add("PEDIDO_DIRECTO")      '6
                        .Add("FORMULARIO")          '7
                        .Add("WORKFLOW")            '8
                        .Add("IMPORTE", System.Type.GetType("System.Double")) '9
                        .Add("COMENTALTANOCONF")    '10
                        .Add("ACCION")              '11
                        .Add("IDACCION")            '12
                        .Add("IDACCIONFORM")        '13
                        .Add("GUARDAR")             '14
                        .Add("NOTIFICAR")           '15
                        .Add("TRASLADO_USUARIO")    '16
                        .Add("TRASLADO_PROVEEDOR")  '17
                        .Add("TRASLADO_FECHA")      '18
                        .Add("TRASLADO_COMENTARIO") '19
                        .Add("TRASLADO_PROVEEDOR_CONTACTO") '20
                        .Add("DEVOLUCION_COMENTARIO")       '21
                        .Add("COMENTARIO")          '22
                        .Add("BLOQUE_ORIGEN")       '23
                        .Add("NUEVO_ID_INSTANCIA")  '24
                        .Add("BLOQUES_DESTINO")     '25
                        .Add("ROL_ACTUAL")          '26
                        .Add("PEDIDO")          '31
                        .Add("IDTIEMPOPROC")
                    End With

                    Dim drSolicitud As DataRow
                    drSolicitud = dsXML.Tables("SOLICITUD").NewRow
                    With drSolicitud
                        .Item("TIPO_PROCESAMIENTO_XML") = CInt(TipoProcesamientoXML.FSNWeb)
                        .Item("TIPO_DE_SOLICITUD") = CInt(oInstancia.Solicitud.TipoSolicit)
                        .Item("COMPLETO") = 1
                        .Item("CODIGOUSUARIO") = FSNUser.Cod
                        .Item("INSTANCIA") = oInstancia.ID
                        .Item("SOLICITUD") = IIf(idInstanciaAprob = 0, oInstancia.Solicitud.ID, "")
                        .Item("USUARIO") = FSNUser.CodPersona
                        .Item("USUARIO_EMAIL") = DBNullToStr(FSNUser.Email)
                        .Item("USUARIO_IDIOMA") = FSNUser.Idioma.ToString()
                        .Item("PEDIDO_DIRECTO") = 0
                        .Item("FORMULARIO") = oSolicitud.Formulario.Id
                        .Item("WORKFLOW") = oSolicitud.Workflow
                        .Item("IMPORTE") = dr("IMPORTE")
                        .Item("IDACCION") = Accion
                        .Item("IDACCIONFORM") = Accion
                        .Item("GUARDAR") = 1
                        .Item("NOTIFICAR") = 1
                        .Item("BLOQUE_ORIGEN") = BloqueOrigen
                        .Item("BLOQUES_DESTINO") = BloqueDestino
                        .Item("ROL_ACTUAL") = oInstancia.RolActual
                        .Item("PEDIDO") = CInt(oInstancia.Solicitud.TipoSolicit)
                        .Item("IDTIEMPOPROC") = lIDTiempoProc
                    End With
                    dsXML.Tables("SOLICITUD").Rows.Add(drSolicitud)

                    sXMLName = FSNUser.Cod & "#" & oInstancia.ID & "#" & BloqueOrigen

                    'Cargar XML completo
                    oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=TiempoProcesamiento.InicioDisco)
                    If ConfigurationManager.AppSettings("SERVIDOR_TRATAMIENTO_INSTANCIAS") = "0" Then
                        'Se hace con un StringWriter porque el método GetXml del dataset no incluye el esquema
                        Dim oSW As New System.IO.StringWriter()
                        dsXML.WriteXml(oSW, XmlWriteMode.WriteSchema)

                        Dim oSrvTrataInst As New FSNWebServiceXML.TratamientoInstancias
                        oSrvTrataInst.TransferirXMLEnEpera(oSW.ToString(), sXMLName)
                    Else
                        dsXML.WriteXml(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & "#FSNWEB.xml", XmlWriteMode.WriteSchema)
                        If File.Exists(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml") Then _
                            File.Delete(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml")
                        FileSystem.Rename(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & "#FSNWEB.xml",
                                          ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml")
                    End If
                End If

                If Not YaRemoveCache Then
                    YaRemoveCache = True

                    Dim PeticUser As FSNServer.User = FSNServer.Get_Object(GetType(FSNServer.User))
                    PeticUser.LoadUserData(oInstancia.Peticionario)

                    If PeticUser.ModificarPrecios Then
                        HttpContext.Current.Cache.Remove("DsetArticulos_" & FSNUser.CodPersona)
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Evento que se lanza al pinchar en el botón btnReabrirOrden y que lanza el proceso de reapertura de la orden
    ''' </summary>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ReabrirOrden(ByVal IdOrden As Integer, ByVal Aprobador As String, ByVal Receptor As String, ByVal acepProve As Boolean) As String
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oOrden As COrden = FSNServer.Get_Object(GetType(FSNServer.COrden))
        oOrden.ID = IdOrden
        'Reabrimos la orden y recuperamos su nuevo estado
        Dim iEstadoNuevo As Integer = oOrden.ReabrirOrden(FSNUser.CodPersona)
        Dim resultado As New Dictionary(Of String, Boolean)
        resultado("Reabrir") = MostrarBotonReabrir(iEstadoNuevo, Aprobador, Receptor)
        resultado("Cerrar") = MostrarBotonCerrar(iEstadoNuevo, Aprobador, Receptor)
        resultado("Anular") = MostrarBotonAnular(iEstadoNuevo, Aprobador)
        resultado("Recepcionar") = MostrarBotonRecepcionar(iEstadoNuevo, Aprobador, Receptor, acepProve)

        Dim oIntegracion As Integracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))
        'Llamamos al servicio de integración WCF.
        oIntegracion.LlamarFSIS(TablasIntegracion.PED_Aprov, oOrden.ID)

        Dim serializer As New JavaScriptSerializer()
        Return serializer.Serialize(resultado)
    End Function
    ''' <summary>
    ''' Evento que se lanza al pinchar en el botón btnCerrarOrden y que lanza el proceso de cambio de estado de la orden a cerrado
    ''' </summary>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function CerrarOrden(ByVal IdOrden As Integer, ByVal Aprobador As String, ByVal Receptor As String, ByVal acepProve As Boolean, ByVal TipoPedidoRec As Integer) As String
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oOrden As COrden = FSNServer.Get_Object(GetType(FSNServer.COrden))
        oOrden.ID = IdOrden

        Dim rdoCerrarOrden As Integer = oOrden.CerrarOrden(FSNUser.CodPersona)
        Dim oIntegracion As Integracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))

        If rdoCerrarOrden = 1 Then 'Todo OK
            'Llamamos al servicio de integración WCF.
            oIntegracion.LlamarFSIS(TablasIntegracion.PED_Aprov, oOrden.ID)
        End If
        Dim resultado As New Dictionary(Of String, Boolean)
        resultado("Reabrir") = MostrarBotonReabrir(CPedido.Estado.Cerrado, Aprobador, Receptor)
        resultado("Cerrar") = MostrarBotonCerrar(CPedido.Estado.Cerrado, Aprobador, Receptor)
        resultado("Anular") = MostrarBotonAnular(CPedido.Estado.Cerrado, Aprobador)
        resultado("Recepcionar") = MostrarBotonRecepcionar(CPedido.Estado.Cerrado, Aprobador, Receptor, acepProve)
        Dim serializer As New JavaScriptSerializer()
        Return serializer.Serialize(resultado)
    End Function

    'ComprobarCerrarOrden
    ''' <summary>Evento que se lanza al pinchar en el boton Cerrar para pedidos de recepcion obligatoria.</summary>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ComprobarCerrarOrden(ByVal IdOrden As Integer) As Boolean  'tarea 10414 - No permitir cerrar el pedido si no tiene todas las recepciones        
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oOrden As COrden = FSNServer.Get_Object(GetType(FSNServer.COrden))
        Return oOrden.ComprobarCerrarOrden(IdOrden)
    End Function


    ''' <summary>Evento que se lanza al pinchar en el botÃ³n Borrar y que lanza la comprobaciÃ³n del borrado del pedido.</summary>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ComprobarBorrarOrden(ByVal sPedido As String) As Object
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")

        Dim oSerializer As New Script.Serialization.JavaScriptSerializer()
        Dim oPedido As CDetallePedido = oSerializer.Deserialize(Of CDetallePedido)(sPedido)

        Dim oOrden As COrden = FSNServer.Get_Object(GetType(FSNServer.COrden))
        Return oOrden.ComprobarBorrarOrden(oPedido, IIf(FSNServer.TipoAcceso.g_bAccesoFSFA, 1, 2), FSNUser.Idioma, FSNServer.TipoAcceso.gbPedidosHitosFacturacion)
    End Function
    ''' <summary>FunciÃ³n que se lanza al pinchar en el botÃ³n Deshacer Borrar y que lanza la comprobaciÃ³n de deshacer el borrado del pedido.</summary>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ComprobarDeshacerBorrarOrden(ByVal IdOrden As Integer, ByVal Estado As Integer, ByVal TipoPedido As Integer) As Object
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oOrden As COrden = FSNServer.Get_Object(GetType(FSNServer.COrden))
        Return oOrden.ComprobarDeshacerBorrarOrden(IdOrden, Estado, TipoPedido)
    End Function
    ''' <summary>
    ''' Evento que se lanza al pinchar en el botón Anular y que lanza el proceso de anulación.
    ''' </summary>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ComprobarAnularOrden(ByVal IdOrden As Integer, ByVal ProveCod As String, ByVal iTipoPedido As Integer) As Integer
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim sCommandName As String = Acciones.AccionNula.ToString
        Dim oOrden As COrden = FSNServer.Get_Object(GetType(FSNServer.COrden))
        oOrden.ID = IdOrden
        oOrden.Prove = ProveCod

        'Comprobar recepciones asociadas
        Dim hayRecepcionesAsociadas As Boolean = oOrden.TieneRecepciones
        Dim hayRecepcionesIntKO As Boolean = oOrden.TieneRecepcionesIntKO
        Dim dsLineasPedido As DataSet = HttpContext.Current.Cache("DsetEmisionPedidos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString() & "_" & IdOrden.ToString())
        'Comprobar facturas y pagos (como basta que haya recepciones para detener el proceso de anulación, seguimos sólo si NO hay anulaciones)
        Dim hayFacturasOPagos As Boolean = False
        If Not hayRecepcionesAsociadas Then
            For Each linea As DataRow In dsLineasPedido.Tables(0).Rows
                If linea.Item("TIENE_FRAS") = 1 OrElse linea.Item("TIENE_PAGOS") = 1 Then
                    hayFacturasOPagos = True
                    Exit For
                End If
            Next
        End If

        Dim resultado As Integer
        If hayRecepcionesAsociadas Then
            'sTextoPanel = Textos(200) & " " & Textos(201) 'Imposible Anular. 'El pedido tiene recepciones asociadas, debe eliminarlas previamente para poder anular el pedido.
            'sCommandName = Acciones.NoAnularOrden.ToString
            resultado = 1
        ElseIf hayFacturasOPagos Then
            'sTextoPanel = Textos(200) & " " & Textos(202) 'Imposible Anular. 'El pedido tiene facturas o pagos asociados, debe eliminarlos previamente para poder anular el pedido.
            'sCommandName = Acciones.NoAnularOrden.ToString
            resultado = 2
        ElseIf hayRecepcionesIntKO Then
            resultado = 3
        ElseIf FSNServer.TipoAcceso.gbPedidosHitosFacturacion AndAlso (iTipoPedido = TipoDePedido.Directo Or iTipoPedido = TipoDePedido.PedidosPM) AndAlso oOrden.ComprobarHitosFacturacion Then
            resultado = 4
        Else
            'sTextoPanel = Textos(203) 'El proceso de anulación no se puede deshacer. Si desea continuar, chequee la casilla de confirmación.
            'sCommandName = Acciones.AnularOrden.ToString
            resultado = 0
        End If

        'En este caso, el panel confirmar afronta 3 posibles casos:
        'Confirmar la anulación, informar de que no se anula porque hay recepciones o informar de que no se anula porque hay fras o pagos
        'Confirmar(sTextoPanel, sCommandName, sCommandArgument)
        Return resultado
    End Function
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function BorrarOrden(ByVal OrdenId As Integer, ByVal dImporteTotalOrden As Double, ByVal dImporteCostes As Double, ByVal dImporteDescuentos As Double) As Boolean
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim idOrdenPedidoAbierto As Integer = HttpContext.Current.Session("idOrdenPedidoAbierto")
        Dim oOrden As COrden = FSNServer.Get_Object(GetType(FSNServer.COrden))

        Dim bRes As Boolean = oOrden.BorrarOrden(OrdenId, FSNUser.Cod, FSNUser.CodPersona, FSNServer.TipoAcceso.g_bAccesoFSFA, dImporteTotalOrden, dImporteCostes, dImporteDescuentos, FSNUser.Idioma, idOrdenPedidoAbierto)
        If bRes Then
            'Llamada a FSIS.
            Dim oIntegracion As FSNServer.Integracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))
            oIntegracion.LlamarFSIS(TablasIntegracion.PED_Aprov, OrdenId)
        End If
        Return bRes
    End Function
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function DeshacerBorrarOrden(ByVal sDatosOrden As String) As Boolean
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim oOrden As COrden = FSNServer.Get_Object(GetType(FSNServer.COrden))
        Dim idOrdenPedidoAbierto As Integer = HttpContext.Current.Session("idOrdenPedidoAbierto")
        Dim oSerializer As New JavaScriptSerializer
        Dim oDatosOrden As Object = oSerializer.Deserialize(Of Object)(sDatosOrden)

        Dim bRes As Boolean = oOrden.DeshacerBorrarOrden(oDatosOrden, FSNServer.TipoAcceso.gbAccesoFSSM, FSNUser.Cod, FSNUser.Idioma, idOrdenPedidoAbierto)
        If bRes Then
            'Llamada a FSIS.
            Dim oIntegracion As FSNServer.Integracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))
            oIntegracion.LlamarFSIS(TablasIntegracion.PED_Aprov, oDatosOrden("IdOrden"))
        End If

        Return bRes
    End Function
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function AnularOrden(ByVal IdOrden As Integer, ByVal ProveCod As String, ByVal Estado As Integer) As Integer
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

        'anular la orden
        Dim oOrden As COrden = FSNServer.Get_Object(GetType(FSNServer.COrden))
        oOrden.ID = IdOrden
        oOrden.Prove = ProveCod
        oOrden.OrdenEst = Estado
        Dim rdoAnulacion As Integer = oOrden.AnularOrden(FSNUser.Idioma, FSNUser.CodPersona, FSNServer.TipoAcceso.gbAccesoFSSM)
        Dim resultado As Integer
        If rdoAnulacion = 1 Then 'Anulada correctamente
            resultado = 0
            Dim oIntegracion As Integracion = FSNServer.Get_Object(GetType(Integracion))
            'Llamamos al servicio de integración WCF.
            oIntegracion.LlamarFSIS(TablasIntegracion.PED_Aprov, oOrden.ID)
        ElseIf rdoAnulacion = 0 Then 'Error
            resultado = 1 'lblConfirm.Text = Textos(206) 'Se ha producido un error al anular el pedido. Por favor, inténtelo de nuevo más tarde y si el problema persiste, contacte con el soporte técnico.
        End If
        Return resultado
    End Function
    ''' Revisado por: blp. Fecha: 15/02/2012
    ''' <summary>
    ''' Evento que se lanza al pinchar en el botón Recepcionar y que redirige a la pantalla de recepciones mostrando únicamente esa recepción.
    ''' </summary>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Sub Recepcionar(ByVal IdOrden As Integer, ByVal ProveCod As String,
                                                ByVal Anyo As Integer, ByVal NumOrden As Integer, ByVal NumPedido As Integer,
                                                ByVal VerPedidosUsuario As Boolean, ByVal VerPedidosOtrosUsuarios As Boolean)
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim OrdenIdYProveedor As String = IdOrden & "###" & ProveCod

        '---Si sólo se ven los pedidos del usuario, pasamos los valores
        If Not FSNUser.PermisoVerPedidosCCImputables Then
            VerPedidosUsuario = True
            VerPedidosOtrosUsuarios = False
        End If
        OrdenIdYProveedor += "###" & Anyo.ToString & "###" & NumPedido.ToString & "###" & NumOrden.ToString &
                            "###" & VerPedidosUsuario & "###" & VerPedidosOtrosUsuarios

        HttpContext.Current.Session("OrdenIdYProveedor") = OrdenIdYProveedor
    End Sub
    ''' <summary>
    ''' Reemitir Orden
    ''' </summary>
    ''' <param name="IdOrden">Orden</param>
    ''' <param name="ProveCod">Proveedor</param>
    ''' <param name="PedidoId">Pedido</param>
    ''' <param name="iTipoPedido">TipoPedido</param>
    ''' <param name="EmpresaId">Empresa</param>
    ''' <remarks>Llamada desde: DEtallePedido.js; Tiempo máximo: 0,2</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Sub ReemitirOrden(ByVal IdOrden As Long, ByVal ProveCod As String, ByVal PedidoId As Long, ByVal iTipoPedido As Integer, ByVal EmpresaId As Long, ByVal iTipo As TipoDePedido, ByVal sMoneda As String)
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oIntegracion As Integracion = FSNServer.Get_Object(GetType(Integracion))

        Dim oOrden As COrden = FSNServer.Get_Object(GetType(FSNServer.COrden))
        oOrden.ID = IdOrden
        oOrden.Prove = ProveCod
        oOrden.PedidoId = PedidoId
        Dim dsImputacion As DataSet
        dsImputacion = oOrden.NoImputacion(FSNUser.CodPersona)

        If dsImputacion.Tables.Count > 0 Then
            If dsImputacion.Tables(0).Rows.Count > 0 Then
                Dim sMens As String = ""
                Dim oRow As DataRow
                For Each oRow In dsImputacion.Tables(0).Rows
                    If sMens <> "" Then sMens = sMens & ", "
                    If Not IsDBNull(oRow("UON4")) Then
                        sMens = sMens & oRow("UON4")
                    ElseIf Not IsDBNull(oRow("UON3")) Then
                        sMens = sMens & oRow("UON3")
                    ElseIf Not IsDBNull(oRow("UON2")) Then
                        sMens = sMens & oRow("UON2")
                    Else
                        sMens = sMens & oRow("UON1")
                    End If
                    sMens = sMens & " " & oRow("DEN")
                Next
                Exit Sub
            End If
        End If
        dsImputacion.Dispose()

        'DEMO BERGE / 2017 / 35: El prototipo no dice gran cosa, se decide q
        '   El Pedido "denegado parcialmente" pasa a "emitido al proveedor". 
        '   Las lineas rechazadas se eliminan
        '   Es una emisión sin flujo
        Dim NotificadoProve As Long
        NotificadoProve = oOrden.Reemitir(FSNUser.CodPersona, False)

        'El estado es 2, YA Q es emisión sin flujo
        If FSNUser.AccesoSM Then
            'Sumamos el importe comprometido del SM
            Dim obDetallePedido As cDetallePedidos = FSNServer.Get_Object(GetType(cDetallePedidos))
            obDetallePedido.ActualizarImputacion(oOrden.ID)
        End If

        Dim cEmisionPedidos As CEmisionPedidos
        cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))
        Dim dr As DataRowCollection = cEmisionPedidos.TrasCrearOrdenEntregaLineaActualizarCatalogo(oOrden.ID, 0, FSNUser.CodPersona, FSNUser.ModificarPrecios)

        If Not dr Is Nothing Then
            For Each fila As DataRow In dr
                ActualizarPrecArticulo(fila.Item("ID"), fila.Item("PRECUC"))
            Next
        End If

        'Llamamos al servicio de integración WCF.
        oIntegracion.LlamarFSIS(TablasIntegracion.PED_Aprov, oOrden.ID)

        oOrden.NotificarOrden(DBNullToStr(FSNUser.Email), NotificadoProve)

    End Sub
#End Region
    ''' Revisado por: blp. Fecha: 10/05/2012
    ''' <summary>
    ''' Procedimiento que maneja el evento CLICK del boton que anula una linea de una recepciÃ³n
    ''' </summary>
    ''' <param name="sender">Objeto que ha lanzado el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde el boton btnAnularLinea de Seguimiento.aspx; Tiempo mÃ¡ximo 1seg</remarks>
    Public Sub btnBloquearFacturacionAlbaran_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        mpeRecepciones.Hide()
        Select Case DirectCast(sender, FSNWebControls.FSNButton).CommandName
            Case Acciones.DesBloquearFacturacionAlbaran.ToString
                'Dejamos que se ejecute a travÃ©s del case Acciones.DesBloquearFacturacionAlbaran.ToString
                'de la funciÃ³n btnAceptarConfirm_Click
                'btnAceptarConfirm.CommandName = Acciones.DesBloquearFacturacionAlbaran.ToString
                'btnAceptarConfirm.CommandArgument = DirectCast(sender, FSNWebControls.FSNButton).CommandArgument
                'btnAceptarConfirm_Click(sender, e)
                Dim sParametros() As String = Split(DirectCast(sender, FSNWebControls.FSNButton).CommandArgument, "###")
                Dim sIdOrden As String = sParametros(2)
                HttpContext.Current.Cache.Remove("DsetAlbaranes_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString() & "_" & sIdOrden)
            Case Else
                Dim sParametros() As String = Split(DirectCast(sender, FSNWebControls.FSNButton).CommandArgument, "###")
                Dim sAlbaran As String = sParametros(0)
                'Ejemplo del texto: AlbarÃ¡n 123456 SALTO DE LÃNEA Puede introducir comentarios al bloqueo del albarÃ¡n para facturaciÃ³n
                Dim sTextoPanelConfirmacion As String = "<b>" & Textos(156) & " " & sAlbaran & "</b><p>" & Textos(236) & ":</p>"
                Dim sAccion As String = DirectCast(sender, FSNWebControls.FSNButton).CommandName

                'Confirmar(sTextoPanelConfirmacion, sAccion, DirectCast(sender, FSNWebControls.FSNButton).CommandArgument)
                Dim sIdOrden As String = sParametros(2)
                HttpContext.Current.Cache.Remove("DsetAlbaranes_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString() & "_" & sIdOrden)
        End Select
    End Sub
    ''' Revisado por: blp. Fecha: 06/03/2012
    ''' <summary>
    ''' Procedimiento que se ejecuta cuando se enlaza a la fuente de datos el Datalist y se ejecuta una vez por cada linea
    ''' </summary>
    ''' <param name="sender">datalist</param>
    ''' <param name="e">argumentos del evento del datalist</param>
    ''' <remarks>Llamada desde el evento ItemDataBound. Máx. 0,5 seg.</remarks>
    Private Sub dlLineasRecepciones_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlLineasRecepciones.ItemDataBound
        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
        Dim fila As CRecepcion = DirectCast(e.Item.DataItem, CRecepcion)
        Dim Recepciones As CRecepciones = DirectCast(DirectCast(sender, DataList).DataSource, FSNServer.CRecepciones)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Static litCabeceraCantidadRecibida As Literal
        Static litCabeceraCantidadRecibida_Edit As Literal
        With e.Item
            Dim btnGuardarID As String
            Dim wneCantidad As String
            Dim idlblPrecUni As String
            Select Case .ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem
                    btnGuardarID = DirectCast(.FindControl("btnGuardarModifLinea"), Fullstep.FSNWebControls.FSNButton).ClientID
                    wneCantidad = DirectCast(.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).ClientID
                    idlblPrecUni = DirectCast(.FindControl("lblPrecUni"), System.Web.UI.WebControls.Label).ClientID
                    With DirectCast(.FindControl("btnAnulRecep"), Fullstep.FSNWebControls.FSNButton)
                        .Text = Textos(224)
                        .OnClientClick = "AnularRecepcion(" & fila.LineaPedido & ",'" & fila.Albaran & "','" & fila.Fecha & "','" & fila.OrdenProveedor & "'); return false;"
                    End With
                    With DirectCast(.FindControl("btnAnularLineaRecep"), Fullstep.FSNWebControls.FSNButton)
                        .Text = Textos(199)
                        .OnClientClick = "AnularLineaRecep(" & fila.LineaPedido & "," & fila.LineaRecep & ");return false;"
                    End With
                    DirectCast(.FindControl("btnGuardarModifLinea"), Fullstep.FSNWebControls.FSNButton).Text = Textos(225)
                    DirectCast(.FindControl("btnGuardarModifLinea"), Fullstep.FSNWebControls.FSNButton).Style.Add("display", "none")
                    DirectCast(.FindControl("btnGuardarModifLinea"), Fullstep.FSNWebControls.FSNButton).OnClientClick = "ModificarLineaRecepcion(" & fila.LineaPedido.ToString & "," & fila.LineaRecep.ToString & ",'" & btnGuardarID & "', '" & wneCantidad & "','" & idlblPrecUni & "', '" & fila.PrecioUnitario & "'); return false;"
                    DirectCast(.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Attributes.Add("btnGuardarID", btnGuardarID)
                    DirectCast(.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Attributes.Add("vdecimalfmt", Me.Usuario.NumberFormat.NumberDecimalSeparator)
                    DirectCast(.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Attributes.Add("vthousanfmt", Me.Usuario.NumberFormat.NumberGroupSeparator)
                    DirectCast(.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Attributes.Add("numdec", Me.Usuario.NumberFormat.NumberDecimalDigits)
                    DirectCast(.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Attributes.Add("lblPrecioUni", idlblPrecUni)
                    DirectCast(.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Attributes.Add("PrecioUni", fila.PrecioUnitario.ToString)
                    DirectCast(.FindControl("litItemFecha"), System.Web.UI.WebControls.Label).Text = FSNLibrary.FormatDate(fila.Fecha, Me.Usuario.DateFormat)
                    'Unicamente se podrán eliminar líneas de la recepción cuando:
                    'Todavía no haya llegado la factura correspondiente a ese albarán
                    'y cuando el usuario cuente con el correspondiente permiso de anulación
                    'y la línea de pedido a la que corresponden las recepciones no sea de Recepción automática
                    'y el pedido no este cerrado.
                    Dim bLineaPedidoTieneRecepAuto As Boolean = LineaPedidoTieneRecepAuto(fila.LineaPedido)
                    If oUsuario.AnularRecepciones AndAlso fila.Factura = 0 AndAlso Not bLineaPedidoTieneRecepAuto AndAlso Not (Recepciones(0).OrdenEstado = TipoEstadoOrdenEntrega.RecibidoYCerrado) Then
                        'El botón de anulación a nivel de línea estará disponible siempre y cuando existan varias líneas recepcionadas dentro del mismo albarán
                        If fila.NumLineasAlbaran > 1 Then
                            DirectCast(.FindControl("btnAnularLineaRecep"), Fullstep.FSNWebControls.FSNButton).Visible = True
                        Else
                            DirectCast(.FindControl("btnAnularLineaRecep"), Fullstep.FSNWebControls.FSNButton).Visible = False
                        End If
                        DirectCast(.FindControl("btnAnulRecep"), Fullstep.FSNWebControls.FSNButton).Visible = True
                        DirectCast(.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Visible = True
                        configurarDecimales(fila.Unidad, DirectCast(.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor))
                        DirectCast(.FindControl("litItemCantidadRecib"), System.Web.UI.WebControls.Label).Visible = False
                        DirectCast(.FindControl("FSNlnkUniCantPdte"), Fullstep.FSNWebControls.FSNLinkTooltip).Visible = False
                        litCabeceraCantidadRecibida.Text = ""
                        litCabeceraCantidadRecibida_Edit.Text = Textos(194)
                    Else
                        DirectCast(.FindControl("litItemCantidadRecib"), System.Web.UI.WebControls.Label).Text = FSNLibrary.FormatNumber(fila.CantRecep, Me.Usuario.NumberFormat)
                        DirectCast(.FindControl("litItemCantidadRecib"), System.Web.UI.WebControls.Label).Visible = True
                        DirectCast(.FindControl("FSNlnkUniCantPdte"), Fullstep.FSNWebControls.FSNLinkTooltip).Visible = True
                        DirectCast(.FindControl("btnAnularLineaRecep"), Fullstep.FSNWebControls.FSNButton).Visible = False
                        DirectCast(.FindControl("btnAnulRecep"), Fullstep.FSNWebControls.FSNButton).Visible = False
                        DirectCast(.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Visible = False
                        litCabeceraCantidadRecibida.Text = Textos(194)
                        litCabeceraCantidadRecibida_Edit.Text = ""
                    End If
                    DirectCast(.FindControl("lblPrecUni"), System.Web.UI.WebControls.Label).Text = FSNLibrary.FormatNumber(fila.Importe, Me.Usuario.NumberFormat)
                    If Acceso.gbCodRecepERP Then
                        DirectCast(.FindControl("litItemNumRecepERP"), System.Web.UI.WebControls.Label).Visible = True
                    Else
                        DirectCast(.FindControl("litItemNumRecepERP"), System.Web.UI.WebControls.Label).Visible = False
                    End If
                    'Bloquear Facturacion
                    Dim oBtnBloquearFacturacionAlbaran As Fullstep.FSNWebControls.FSNButton = TryCast(.FindControl("btnBloquearFacturacionAlbaran"), Fullstep.FSNWebControls.FSNButton)
                    Dim oImgBloquearFacturacionAlbaran As Image = TryCast(.FindControl("imgBloquearFacturacionAlbaran"), Image)
                    Dim oLblBloquearFacturacionAlbaran As Label = TryCast(.FindControl("lblBloquearFacturacionAlbaran"), Label)
                    Dim oLnkBloquearFacturacionAlbaran As Fullstep.FSNWebControls.FSNLinkInfo = TryCast(.FindControl("lnkBloquearFacturacionAlbaran"), Fullstep.FSNWebControls.FSNLinkInfo)
                    If Not Me.Acceso.g_bAccesoFSFA Then
                        If oBtnBloquearFacturacionAlbaran IsNot Nothing Then
                            oBtnBloquearFacturacionAlbaran.Visible = False
                        End If
                        If oLnkBloquearFacturacionAlbaran IsNot Nothing Then
                            oLnkBloquearFacturacionAlbaran.Visible = False
                        End If
                        If oImgBloquearFacturacionAlbaran IsNot Nothing Then
                            oImgBloquearFacturacionAlbaran.Visible = False
                        End If
                        If oLblBloquearFacturacionAlbaran IsNot Nothing Then
                            oLblBloquearFacturacionAlbaran.Visible = False
                        End If
                    Else
                        'SI EL USUARIO PUEDE BLOQUEAR ALBARANES, MOSTRAMOS BOTÓN
                        If Me.FSNUser.BloquearFacturacionAlbaranes Then
                            If Not oBtnBloquearFacturacionAlbaran Is Nothing Then
                                If fila.AlbaranBloqueadoParaFacturacion Then
                                    oBtnBloquearFacturacionAlbaran.Text = Textos(235) 'Desbloquear facturación
                                    oBtnBloquearFacturacionAlbaran.CommandName = Acciones.DesBloquearFacturacionAlbaran.ToString
                                    With oBtnBloquearFacturacionAlbaran
                                        .OnClientClick = "DesbloquearFacturacionAlbaran('" & fila.Albaran & "','" & fila.LineaPedido & "','" & fila.Orden & "'); return false;"
                                    End With
                                Else
                                    oBtnBloquearFacturacionAlbaran.Text = Textos(234) 'Bloquear facturación
                                    oBtnBloquearFacturacionAlbaran.CommandName = Acciones.BloquearFacturacionAlbaran.ToString
                                    With oBtnBloquearFacturacionAlbaran
                                        .OnClientClick = "BloquearFacturacionAlbaran('" & fila.Albaran & "','" & fila.LineaPedido & "','" & fila.Orden & "'); return false;"
                                    End With
                                End If
                            End If
                            If Not oImgBloquearFacturacionAlbaran Is Nothing Then _
                                oImgBloquearFacturacionAlbaran.Visible = False
                            If Not oLblBloquearFacturacionAlbaran Is Nothing Then _
                                oLblBloquearFacturacionAlbaran.Visible = False
                            If Not oLnkBloquearFacturacionAlbaran Is Nothing Then _
                                oLnkBloquearFacturacionAlbaran.Visible = False
                        Else 'SI NO PUEDE BLOQUEAR PERO EL ALBARÁN ESTÁ BLOQUEADO, MOSTRAMOS EL BLOQUEO
                            If Not oBtnBloquearFacturacionAlbaran Is Nothing Then _
                                oBtnBloquearFacturacionAlbaran.Visible = False
                            If fila.AlbaranBloqueadoParaFacturacion Then

                                If Not oImgBloquearFacturacionAlbaran Is Nothing Then
                                    oImgBloquearFacturacionAlbaran.Visible = True
                                    oImgBloquearFacturacionAlbaran.Style.Add("float", "left")
                                    oImgBloquearFacturacionAlbaran.Style.Add("margin-right", "5px")
                                End If
                                If Not oLblBloquearFacturacionAlbaran Is Nothing Then
                                    oLblBloquearFacturacionAlbaran.Visible = True
                                    oLblBloquearFacturacionAlbaran.Text = Textos(238)
                                End If
                                If Not oLnkBloquearFacturacionAlbaran Is Nothing Then
                                    oLnkBloquearFacturacionAlbaran.Visible = True
                                    oLnkBloquearFacturacionAlbaran.ContextKey = DBNullToStr(fila.AlbaranBloqueadoParaFacturacionObservaciones)
                                    oLnkBloquearFacturacionAlbaran.Style.Add("text-decoration", "none")
                                End If
                            Else
                                oLnkBloquearFacturacionAlbaran.Visible = False
                            End If
                        End If
                    End If

                    If IsDBNull(fila.Obs) OrElse String.IsNullOrEmpty(fila.Obs) Then
                        DirectCast(.FindControl("btnFilaObservacionesRecep"), FSNWebControls.FSNImageTooltip).Enabled = False
                        DirectCast(.FindControl("btnFilaObservacionesRecep"), FSNWebControls.FSNImageTooltip).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, "observaciones_desactivado.gif")
                    Else
                        DirectCast(.FindControl("btnFilaObservacionesRecep"), FSNWebControls.FSNImageTooltip).Enabled = True
                    End If
                    If fila.TipoRecepcion = 0 Then
                        DirectCast(.FindControl("lblImporterecep"), System.Web.UI.WebControls.Label).Visible = False
                        DirectCast(.FindControl("lblMonImporterecep"), System.Web.UI.WebControls.Label).Visible = False
                    Else
                        DirectCast(.FindControl("litItemCantidadRecib"), System.Web.UI.WebControls.Label).Visible = False
                        DirectCast(.FindControl("FSNlnkUniCantPdte"), Fullstep.FSNWebControls.FSNLinkTooltip).Visible = False
                        DirectCast(.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Visible = False
                        DirectCast(.FindControl("FSNlnkUniCantPdte1"), Fullstep.FSNWebControls.FSNLinkTooltip).Visible = False
                        DirectCast(.FindControl("lblPrecUni"), System.Web.UI.WebControls.Label).Visible = False
                        DirectCast(.FindControl("lblMonPrecUni"), System.Web.UI.WebControls.Label).Visible = False
                        DirectCast(.FindControl("lblImporterecep"), System.Web.UI.WebControls.Label).Visible = True
                        DirectCast(.FindControl("lblImporterecep"), System.Web.UI.WebControls.Label).Text = FSNLibrary.FormatNumber(fila.ImporteRecep, Me.Usuario.NumberFormat)
                        DirectCast(.FindControl("lblMonImporterecep"), System.Web.UI.WebControls.Label).Visible = True
                    End If
                Case ListItemType.Header
                    litCabeceraCantidadRecibida = DirectCast(.FindControl("litLinCantidadRecibida"), System.Web.UI.WebControls.Literal)
                    litCabeceraCantidadRecibida_Edit = DirectCast(.FindControl("litLinCantidadRecibida_Edit"), System.Web.UI.WebControls.Literal)

                    DirectCast(.FindControl("litLinAlbaran"), System.Web.UI.WebControls.Literal).Text = Textos(156)
                    DirectCast(.FindControl("litLinFecha"), System.Web.UI.WebControls.Literal).Text = Textos(223)
                    If Acceso.gbCodRecepERP Then
                        Dim oCParametros As FSNServer.CParametros = FSNServer.Get_Object(GetType(FSNServer.CParametros))
                        DirectCast(.FindControl("litLinNumRecepERP"), System.Web.UI.WebControls.Literal).Text = oCParametros.CargarLiteralParametros(TiposDeDatos.LiteralesParametros.RecepcionERP, Me.Usuario.Idioma)
                        DirectCast(.FindControl("litLinNumRecepERP"), System.Web.UI.WebControls.Literal).Visible = True
                    Else
                        DirectCast(.FindControl("litLinNumRecepERP"), System.Web.UI.WebControls.Literal).Visible = False
                    End If
                    DirectCast(.FindControl("litLinUsuario"), System.Web.UI.WebControls.Literal).Text = Textos(233) 'Usuario
                    If Recepciones(0).TipoRecepcion = 0 Then
                        DirectCast(.FindControl("litLinCantidadRecibida"), System.Web.UI.WebControls.Literal).Visible = True
                        DirectCast(.FindControl("litLinImporte"), System.Web.UI.WebControls.Literal).Visible = True
                        DirectCast(.FindControl("litLinImporte"), System.Web.UI.WebControls.Literal).Text = Textos(67)
                        DirectCast(.FindControl("litLinImporteRecibido"), System.Web.UI.WebControls.Literal).Visible = False
                    Else
                        DirectCast(.FindControl("litLinImporteRecibido"), System.Web.UI.WebControls.Literal).Visible = True
                        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_RecepcionPedidos
                        DirectCast(.FindControl("litLinImporteRecibido"), System.Web.UI.WebControls.Literal).Text = Textos(269)
                        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
                        DirectCast(.FindControl("litLinCantidadRecibida"), System.Web.UI.WebControls.Literal).Visible = False
                        DirectCast(.FindControl("litLinImporte"), System.Web.UI.WebControls.Literal).Visible = False
                    End If
            End Select
        End With
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
    End Sub
    ''' <summary>
    ''' Buscamos la orden y la línea en el dataset cacheado. Si no se encuentra, la buscamos en los objetos de la página 
    ''' y devolvemos si la línea de pedido tiene o no tiene recepción automática
    ''' </summary>
    Private ReadOnly Property LineaPedidoTieneRecepAuto(ByVal IDLineaPedido As Integer) As Boolean
        Get
            Dim dsDsetPedidos As DataSet = DsetPedidos().Copy
            If dsDsetPedidos IsNot Nothing AndAlso dsDsetPedidos.Tables("LINEAS_PEDIDO") IsNot Nothing Then
                Dim drLineaPedido As DataRow = TryCast(dsDsetPedidos.Tables("LINEAS_PEDIDO").Rows.Find(IDLineaPedido), DataRow)
                dsDsetPedidos = Nothing
                If drLineaPedido IsNot Nothing Then
                    Return CBool(drLineaPedido.Item("IM_RECEPAUTO"))
                End If
            End If
            'Si no hemos encontrado nada, devolvemos true, por precaución
            Return True
        End Get
    End Property
    ''' <summary>
    ''' Método que recupera el número de decimales para una unidad de pedido dada y configura la presentación de los datos en los controles webnumericEdit
    ''' </summary>
    ''' <param name="unidad">Código de la unidad de pedido</param>
    ''' <param name="wNumericEdit">Control webNumericEdit a configurar</param>
    ''' <remarks>Llamada desde: la página, allí donde se configure un control webNumericEdit. Tiempo máximo: inferior a 0,3</remarks>
    Private Sub configurarDecimales(ByVal unidad As Object, ByRef wNumericEdit As Infragistics.Web.UI.EditorControls.WebNumericEditor)
        Dim oUnidadPedido As FSNServer.CUnidadPedido = Nothing
        If Not DBNullToSomething(unidad) = Nothing Then
            unidad = Trim(unidad.ToString)
            oUnidadPedido = TodasLasUnidades.Item(unidad)
            oUnidadPedido.AsignarFormatoaUnidadPedido(Me.Usuario.NumberFormat)
        End If
        If Not oUnidadPedido Is Nothing Then
            Dim ci As System.Globalization.CultureInfo = Threading.Thread.CurrentThread.CurrentCulture.Clone()
            ci.NumberFormat = oUnidadPedido.UnitFormat
            If oUnidadPedido.NumeroDeDecimales Is Nothing Then
                ci.NumberFormat.NumberDecimalDigits = 15
                wNumericEdit.MinDecimalPlaces = 0
                wNumericEdit.DataMode = EditorControls.NumericDataMode.Double
            ElseIf oUnidadPedido.NumeroDeDecimales > 0 Then
                ci.NumberFormat.NumberDecimalDigits = oUnidadPedido.NumeroDeDecimales
                wNumericEdit.MinDecimalPlaces = oUnidadPedido.NumeroDeDecimales
                wNumericEdit.DataMode = EditorControls.NumericDataMode.Double
            ElseIf oUnidadPedido.NumeroDeDecimales = 0 Then
                ci.NumberFormat.NumberDecimalDigits = oUnidadPedido.NumeroDeDecimales
                wNumericEdit.MinDecimalPlaces = oUnidadPedido.NumeroDeDecimales
                wNumericEdit.DataMode = EditorControls.NumericDataMode.Int
            End If
            wNumericEdit.Culture = ci
        End If
    End Sub
    ''' <summary>
    ''' Procedimiento que se lanza al asociar datos a una fila del control grdFacturas. Se ejecuta tantas veces como filas se asocien
    ''' Mediante este mÃ©todo controlamos los datos asociados a cada uno de los elementos del gridview grdFacturas
    ''' </summary>
    ''' <param name="sender">el control grdFacturas</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde el propio objeto datalist
    ''' Tiempo maximo 1 sec</remarks>
    Public Sub grdFacturas_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        With e.Row
            Select Case .RowType
                Case DataControlRowType.Header
                    CType(.FindControl("lblNumFra"), Label).Text = Textos(143)
                    CType(.FindControl("lblFechaFra"), Label).Text = Textos(27)
                    CType(.FindControl("lblEstadoFra"), Label).Text = Textos(44)
                    CType(.FindControl("lblImporteFra"), Label).Text = Textos(267)
                Case DataControlRowType.DataRow
                    Dim fila As CFactura = CType(.DataItem, CFactura)
                    CType(.FindControl("litNumFra"), Literal).Text = fila.Num.ToString
                    CType(.FindControl("litFechaFra"), Literal).Text = FSNLibrary.modUtilidades.FormatDate(fila.Fecha.ToShortDateString, Me.Usuario.DateFormat)
                    CType(.FindControl("litEstadoFra"), Literal).Text = fila.EstadoDen.ToString
                    Dim Importe As String = FSNLibrary.FormatNumber(fila.Importe, Me.Usuario.NumberFormat).ToString
                    CType(.FindControl("litImporteFra"), Literal).Text = Importe & " " & fila.Moneda.ToString
            End Select
        End With
    End Sub
    ''' <summary>
    ''' Procedimiento que se lanza al asociar datos a una fila del control grdPagos. Se ejecuta tantas veces como filas se asocien
    ''' Mediante este mÃ©todo controlamos los datos asociados a cada uno de los elementos del gridview grdPagos
    ''' </summary>
    ''' <param name="sender">el control grdPagos</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde el propio objeto datalist
    ''' Tiempo maximo 1 sec</remarks> 
    Public Sub grdPagos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        With e.Row
            Select Case .RowType
                Case DataControlRowType.Header
                    CType(.FindControl("lblNumPago"), Label).Text = Textos(150)
                    CType(.FindControl("lblFechaPago"), Label).Text = Textos(27)
                    CType(.FindControl("lblNumFra"), Label).Text = Textos(143)
                    CType(.FindControl("lblEstadoPago"), Label).Text = Textos(44)
                Case DataControlRowType.DataRow
                    Dim fila As CPago = CType(.DataItem, CPago)
                    CType(.FindControl("litNumPago"), Literal).Text = fila.NumPago.ToString
                    CType(.FindControl("litFechaPago"), Literal).Text = FSNLibrary.modUtilidades.FormatDate(fila.Fecha.ToShortDateString, Me.Usuario.DateFormat)
                    CType(.FindControl("litNumFra"), Literal).Text = fila.NumFra.ToString
                    CType(.FindControl("litEstadoPago"), Literal).Text = fila.EstadoDen.ToString
            End Select
        End With
    End Sub
    ''' <summary>
    ''' Muestra el panel de de detalle del articulo cuando pinchamos en la columna de codigo de articulo del grid
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnMostrarDetalleArticulo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMostrarDetalleArticulo.Click
        pnlDetalleArticulo.CargarDetalleArticuloEP(DsetArticulos, hid_lineaPedido.Value, hid_cantidad.Value, hid_Precio.Value, False, hid_TipoRecepcion.Value)
    End Sub
    ''' <summary>
    ''' Comprueba si hay que ocultar columnas de recepcion cantidad
    ''' </summary>
    ''' <remarks></remarks>
    Private Function hayQueOcultarColumnasRecepcionCantidad() As Boolean
        Dim iLineasRecepImporte As Integer = 0
        Dim iLineasRecepCantidad As Integer = 0
        For Each row As DataRow In dsPedidos.Tables(0).Rows
            If row("TIPORECEPCION") = 1 Then
                iLineasRecepImporte += 1
            Else
                iLineasRecepCantidad += 1
            End If
        Next
        Return (iLineasRecepCantidad = 0)
    End Function
    ''' <summary>
    ''' Comprueba si hay que ocultar columnas de recepcion cantidad
    ''' </summary>
    Private Function hayQueOcultarColumnaSituacion() As Boolean
        Dim bOcultarSituacion As Boolean = True
        For Each row As DataRow In dsPedidos.Tables(0).Rows
            If DBNullToInteger(row("INSTANCIA_APROB")) <> 0 Then
                bOcultarSituacion = False
                Exit For
            End If
        Next
        Return bOcultarSituacion
    End Function
    <System.Web.Services.WebMethodAttribute(),
    System.Web.Script.Services.ScriptMethodAttribute()>
    Public Shared Function DevolverTexto(ByVal contextKey As String) As String
        Return DBNullToStr(contextKey)
    End Function
    ''' <summary>
    ''' CargarDatosDestinos
    ''' </summary>
    ''' <returns>Destinos</returns>
    ''' <remarks>Llamada desde: EmisionPedidoBis.aspx; Tiempo mÃ¡ximo:0</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function CargarDatosDestinos(ByVal sID As String, ByVal iCodLinea As Integer) As Object
        Try
            Dim serializer As New JavaScriptSerializer()
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oCDestinos As Fullstep.FSNServer.CDestinos = FSNServer.Get_Object(GetType(Fullstep.FSNServer.CDestinos))

            oCDestinos.CargarUnDestino(FSNUser.Idioma, sID, iCodLinea)
            Return serializer.Serialize(oCDestinos)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

#Region "Borrar Linea Pedido"
    Private ReadOnly Property InvisibilidadBtBorrarLin As Boolean
        Get
            Dim OcultoBajaLin As Boolean = True
            Dim EstadoPedido As CPedido.Estado = CType(Request.QueryString("estado"), CPedido.Estado)
            If FSNServer.TipoAcceso.gbBajaLOGPedidos AndAlso (FSNUser.EPPermitirBorrarLineasPedido OrElse FSNUser.EPPermitirDeshacerBorradoLineasPedido) Then
                If EstadoPedido = CPedido.Estado.EmitidoAlProveedor OrElse EstadoPedido = CPedido.Estado.AceptadoPorElProveedor OrElse EstadoPedido = CPedido.Estado.EnCamino Then
                    OcultoBajaLin = Not (dsPedidos.Tables.Count > 0 AndAlso dsPedidos.Tables(0).Rows.Count > 1)
                End If
            End If

            Return OcultoBajaLin
        End Get
    End Property

    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ComprobarBorrarLinea(ByVal idLinea As Long, ByVal IdOrden As Long) As Object
        Try
            Dim serializer As New JavaScriptSerializer()

            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim iValidacion As Integer = 0
            Dim oLineaPedido As CLineaPedido
            oLineaPedido = FSNServer.Get_Object(GetType(FSNServer.CLineaPedido))

            oLineaPedido.ID = idLinea
            iValidacion = oLineaPedido.ComprobarBorrarLinea(IdOrden)

            Return serializer.Serialize(iValidacion)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function BorrarLinea(ByVal IdLinea As Integer, ByVal IdOrden As Long _
    , ByVal ImporteBrutoLineas As Double, ByVal ImporteLinea As Double, ByVal ImporteCoste As Double, ByVal ImporteDescuento As Double _
    , ByVal ImporteLineaBD As Double, ByVal ImporteCosteCab As Double, ByVal ImporteDescuentoCab As Double) As String

        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")

        Dim olineaPedido As CLineaPedido = FSNServer.Get_Object(GetType(FSNServer.CLineaPedido))
        olineaPedido.ID = IdLinea

        Dim iValidacion As Integer = olineaPedido.DarDeBaja(FSNServer.TipoAcceso.gbAccesoFSSM, IdOrden, FSNUser.Cod, ImporteLineaBD, ImporteCosteCab, ImporteDescuentoCab)

        If iValidacion = 1 Then
            Dim oIntegracion As FSNServer.Integracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))
            oIntegracion.LlamarFSIS(TablasIntegracion.PED_Aprov, IdOrden)
        End If

        Dim resultado As New Dictionary(Of String, String)
        resultado("Validacion") = CStr(iValidacion)

        Dim TotalLineas As Double = ImporteBrutoLineas - ImporteLinea - ImporteCoste + ImporteDescuento
        resultado("TotalLineas") = TotalLineas.ToString(System.Globalization.CultureInfo.InvariantCulture)

        Dim oSerializer As New JavaScriptSerializer()
        Return oSerializer.Serialize(resultado)
    End Function

    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ComprobarDeshacerBorrarLinea(ByVal idLinea As Long, ByVal IdOrden As Long, ByVal OrdenEstado As String, ByVal TipoPedido As TipoDePedido) As Object
        Try
            Dim serializer As New JavaScriptSerializer()

            Dim LineasNoDeshacer As String, LineasADeshacer As String

            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim iValidacion As Integer = 0
            Dim oLineaPedido As CLineaPedido
            oLineaPedido = FSNServer.Get_Object(GetType(FSNServer.CLineaPedido))

            oLineaPedido.ID = idLinea
            iValidacion = oLineaPedido.ComprobarDeshacerBorrarLinea(IdOrden, OrdenEstado, TipoPedido, LineasNoDeshacer, LineasADeshacer)

            Dim resultado As New Dictionary(Of String, String)
            resultado("Validacion") = iValidacion
            resultado("LineasNoDeshacer") = LineasNoDeshacer
            resultado("LineasADeshacer") = LineasADeshacer
            Return serializer.Serialize(resultado)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function DeshacerBorrarLinea(ByVal IdLinea As Integer, ByVal IdOrden As Long _
    , ByVal ImporteBrutoLineas As Double, ByVal ImporteLinea As Double, ByVal ImporteCoste As Double, ByVal ImporteDescuento As Double _
    , ByVal ImporteLineaBD As Double, ByVal ImporteCosteCab As Double, ByVal ImporteDescuentoCab As Double) As String

        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")

        Dim olineaPedido As CLineaPedido = FSNServer.Get_Object(GetType(FSNServer.CLineaPedido))
        olineaPedido.ID = IdLinea

        Dim iValidacion As Integer = olineaPedido.DeshacerBaja(FSNServer.TipoAcceso.gbAccesoFSSM, IdOrden, FSNUser.Cod, ImporteLineaBD, ImporteCosteCab, ImporteDescuentoCab)

        If iValidacion = 1 Then
            Dim oIntegracion As FSNServer.Integracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))
            oIntegracion.LlamarFSIS(TablasIntegracion.PED_Aprov, IdOrden)
        End If

        Dim resultado As New Dictionary(Of String, String)
        resultado("Validacion") = CStr(iValidacion)

        Dim TotalLineas As Double = ImporteBrutoLineas + ImporteLinea + ImporteCoste - ImporteDescuento
        resultado("TotalLineas") = TotalLineas.ToString(System.Globalization.CultureInfo.InvariantCulture)

        Dim oSerializer As New JavaScriptSerializer
        Return oSerializer.Serialize(resultado)
    End Function

#End Region
End Class
Public Class JSonSiguientesEtapas
    Private _etapas As New List(Of Etapas)
    Private _roles As New List(Of Roles)
    Private _DenAccion As String
    Public Property Etapas() As List(Of Etapas)
        Get
            Return _etapas
        End Get
        Set(ByVal value As List(Of Etapas))
            _etapas = value
        End Set
    End Property
    Public Property Roles() As List(Of Roles)
        Get
            Return _roles
        End Get
        Set(ByVal value As List(Of Roles))
            _roles = value
        End Set
    End Property
    Public Property DenAccion As String
        Get
            Return _DenAccion
        End Get
        Set(ByVal value As String)
            _DenAccion = value
        End Set
    End Property
    Public Sub New()

    End Sub
End Class
Public Class Etapas
    Private _Den As String
    Public Property Den() As String
        Get
            Return _Den
        End Get
        Set(ByVal value As String)
            _Den = value
        End Set
    End Property
    Public Sub New()

    End Sub
End Class
Public Class Roles
    Private _Nombre As String
    Private _Den As String
    Private _Rol As String
    Private _Per As String
    Private _Tipo As Byte
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property
    Public Property Den() As String
        Get
            Return _Den
        End Get
        Set(ByVal value As String)
            _Den = value
        End Set
    End Property
    Public Property Rol() As String
        Get
            Return _Rol
        End Get
        Set(ByVal value As String)
            _Rol = value
        End Set
    End Property
    Public Property Per() As String
        Get
            Return _Per
        End Get
        Set(ByVal value As String)
            _Per = value
        End Set
    End Property
    Public Property Tipo() As Byte
        Get
            Return _Tipo
        End Get
        Set(ByVal value As Byte)
            _Tipo = value
        End Set
    End Property
    Public Sub New()

    End Sub
End Class