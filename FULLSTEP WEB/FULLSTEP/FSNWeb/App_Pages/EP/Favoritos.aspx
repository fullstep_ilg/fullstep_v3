﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" EnableViewState="true" MasterPageFile="~/App_Master/CabCatalogo.master" CodeBehind="Favoritos.aspx.vb" Inherits="Fullstep.FSNWeb.Favoritos" %>
<%@ MasterType VirtualPath="~/App_Master/CabCatalogo.master"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript" language="javascript">

    var ArrOrdenes = new Array();

    function Orden(ID, collapseID, gridLineasID, cargandoID, botoncollapseID, botonaprobarID, botondenegarID) {
        this.ID = ID;
        this.collapseID = collapseID;
        this.gridLineasID = gridLineasID;
        this.cargandoID = cargandoID;
        this.botoncollapseID = botoncollapseID;
        this.botonaprobarID = botonaprobarID;
        this.botondenegarID = botondenegarID;
        this.collapse = collapse;
        this.gridLineas = gridLineas;
        this.cargando = cargando;
        this.botoncollapse = botoncollapse;
        this.botonaprobar = botonaprobar;
        this.botondenegar = botondenegar;
        //this.fechaActualizacion = fechaActualizacion;
        this.abierto = abierto;
        this.accion = new String();
        this.lineaaccion = new Number();
        this.accionOk = false;
        this.Lineas = new Array();
        this.ejecutaraccion = ejecutaraccion;
        ArrOrdenes[ID] = this;
    }

    function collapse() {
        return $find(this.collapseID);
    }

    function gridLineas() {
        return $get(this.gridLineasID);
    }

    function cargando() {
        return $get(this.cargandoID);
    }

    function botoncollapse() {
        return $get(this.botoncollapseID);
    }

    function botonaprobar() {
        return $get(this.botonaprobarID);
    }

    function botondenegar() {
        return $get(this.botondenegarID);
    }

    function abierto() {
        return !this.collapse().get_Collapsed();
    }

    function ejecutaraccion() {
        this.accionOk = true;
        return false;
    }

    function comprobarlineasmodificadas(ordenid, linea, accion) {
        var orden = ArrOrdenes[ordenid];
        switch (accion) {
            case 'desplegar':
                if (orden.cargando().style.display != 'none') return true;
                if (orden.collapse().get_Collapsed()) {
                    orden.collapse().set_Collapsed(false);
                }
                else {
                    orden.collapse().set_Collapsed(true);
                }
                var abiertos = new Array();
                for (var o in ArrOrdenes) {
                    if (ArrOrdenes[o].abierto()) abiertos.push(o);
                }
                $get(hddcollLineasID).value = abiertos.toString();
                break;
            default:
                if (ArrOrdenes[ordenid].accion == accion && ArrOrdenes[ordenid].accionOk) {
                    ArrOrdenes[ordenid].accion = '';
                    ArrOrdenes[ordenid].accionOk = false;
                    return true;
                }
        }
        ArrOrdenes[ordenid].accion = accion;
        ArrOrdenes[ordenid].lineaaccion = linea;
        ArrOrdenes[ordenid].accionOk = false;
        return false;
    }

    function AsyncLineasModifOk(msg) {
        if (msg != '') {
            var arrmsg = msg.split('#');
            var orden = ArrOrdenes[parseInt(arrmsg[0])];
            if (parseInt(arrmsg[2]) == 1) {
                orden.gridLineas().style.display = 'none';
                orden.cargando().style.display = 'block';
                orden.botoncollapse().click();
            }
        }
    }

</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Ppl" runat="server">
    <input type="hidden" id="collapseLineas" runat="server" value="" />
    <!-- Panel Cancelar Emision Favorito -->
    <fsep:CancelarEmisionFavorito ID="pnlCancelarFav" runat=server EnableViewState=false />
    <!-- Panel Info Proveedor -->
    <fsn:FSNPanelInfo ID="FSNPanelDatosProve" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx" ServiceMethod="Proveedor_DatosContacto" TipoDetalle="1" />
    <!-- Panel Persona -->
    <fsn:FSNPanelInfo ID="FSNPanelDatosPersona" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx" ServiceMethod="Persona_Datos" />
    <!-- Panel Destino -->
    <fsn:FSNPanelInfo ID="FSNPanelDatosDestino" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx" ServiceMethod="Destino_Datos" Height="180px" />
    <!-- Paneles Tooltip -->
    <fsn:FSNPanelTooltip ID="pnlTooltipObservaciones" runat="server" ServiceMethod="Observaciones">
    </fsn:FSNPanelTooltip>
    <fsn:FSNPanelTooltip ID="pnlTooltipObservacionesProv" runat="server" ServiceMethod="ObservacionesProv"></fsn:FSNPanelTooltip>
    <fsn:FSNPanelTooltip ID="pnlTooltipObservacionesLinea" runat="server" ServiceMethod="ObservacionesLinea"></fsn:FSNPanelTooltip>
    <fsn:FSNPanelTooltip ID="pnlTooltipTexto" runat="server" ServiceMethod="DevolverTexto"></fsn:FSNPanelTooltip>
    <fsn:FSNPanelTooltip ID="pnlTooltipProceso" runat="server" ServiceMethod="DevolverTexto"></fsn:FSNPanelTooltip>
    <!-- Panel Detalle Artículo -->
    <fsep:DetArticuloPedido ID="pnlDetalleArticulo" runat="server" />

    <!-- Panel Confirmación -->
    <input id="btnOcultoConfirm" type="button" value="button" runat="server" style="display: none" />
    <asp:Panel ID="pnlConfirm" runat="server" BackColor="White" BorderColor="DimGray"
            BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Center" Style="display: none;
            min-width: 300px; padding: 20px" Width="300px">
        <asp:UpdatePanel ID="upConfirm" runat="server" UpdateMode="Conditional" >
            <ContentTemplate>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpeConfirm" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlConfirm" TargetControlID="btnOcultoConfirm"></ajx:ModalPopupExtender>
	
      <!-- Panel Adjuntos -->
            <input id="btnOcultoAdj" type="button" value="button" runat="server" style="display: none" />
            <asp:Panel ID="pnlAdjuntos" runat="server" BackColor="White" BorderColor="DimGray"
                BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left" Style="display: none;
                min-width: 450px; padding: 2px" Width="450px">
                <fsn:FSNPanelTooltip ID="pnlTooltipComentarioAdj" runat="server" ServiceMethod="ComentarioAdj" Contenedor="pnlAdjuntos">
                </fsn:FSNPanelTooltip>
                <table>
                <tr>
                <td style="width:430px">               
                </td>
                <td align="right" valign="top" align="right">
                    <asp:ImageButton ID="ImgBtnCerrarpnlAdjuntos" runat="server" ImageUrl="~/images/Bt_Cerrar.png" ImageAlign="Right" />
                </td>
                </tr>
                </table>
                <asp:UpdatePanel ID="updpnlPopupAdjuntos" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td align="left" colspan="2">
                              <asp:Panel ID="pnlAdjuntosLin" runat="server" Visible="false">
                              <asp:Label ID="lblTituloAdjuntosLin" runat="server" Text="DFicheros adjuntados a la línea de pedido"
                                    CssClass="Rotulo"></asp:Label><br />
                              <asp:Literal ID="litComentarioAdjuntosLin" runat="server" Text="DComentarios adjuntos" ></asp:Literal>
                              </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Panel ID="pnlAdj" runat="server" CssClass="Rectangulo" Width="370px" 
                                style="padding:8px;">
                                <asp:Label ID="lblTituloAdjuntos" runat="server" Text="DArchivos adjuntos" CssClass="Rotulo"></asp:Label>
                                <asp:GridView ID="grdAdjuntos" runat="server" ShowHeader="False" 
                                    AutoGenerateColumns="False" CellPadding="4" GridLines="None">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False" ItemStyle-Wrap="False">
                                            <ItemTemplate>
                                                <asp:Literal ID="litFechaAdjunto" runat="server"></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="False" ItemStyle-Width="300px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnAdjunto" runat="server" CommandName="Descargar" CssClass="Normal" style="text-decoration:none;"></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle Width="300px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <fsn:FSNImageTooltip ID="imgComentarioAdjunto" runat="server" PanelTooltip="pnlTooltipComentarioAdj"
                                                    SkinID="ObservacionesPeq" ContextKey='<%# Container.DataItem("ID") & " " & Container.DataItem("TIPO") %>'/>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle CssClass="FilaPar2" />
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                </ContentTemplate>
            </asp:UpdatePanel> 
            </asp:Panel>
            <ajx:ModalPopupExtender ID="mpeAdj" runat="server" BackgroundCssClass="modalBackground"
                CancelControlID="ImgBtnCerrarpnlAdjuntos" PopupControlID="pnlAdjuntos" TargetControlID="btnOcultoAdj">
            </ajx:ModalPopupExtender>
    
    <asp:UpdatePanel ID="pnlGrid" runat="server" UpdateMode="Conditional">
    <ContentTemplate>      
    <asp:DataList ID="DlPedFavos" runat="server" ExtractTemplateRows="True" 
            Width="100%" CellPadding="4" CssClass="ListaPedidos">
            <ItemStyle CssClass="FilaImpar" />
            <AlternatingItemStyle CssClass="FilaPar" />
            <HeaderTemplate>
                <asp:Table ID="tblCabecera" runat="server">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ColumnSpan="13">
                        </asp:TableHeaderCell>
                    </asp:TableHeaderRow>
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ColumnSpan="13">
                            <fsn:FSNButton ID="btnVerTodos" runat="server" Text="DVer Todos" Font-Bold=false CssClass="Boton" Alineacion="Right" CommandName="VerTodos"></fsn:FSNButton>
                        </asp:TableHeaderCell>
                    </asp:TableHeaderRow>
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ColumnSpan="13">
                            <fsep:Paginador ID="pagdsCabecera" runat="server" PagSize="10" OnCambioOrden="Paginador_OnCambioOrden" />
                        </asp:TableHeaderCell>
                    </asp:TableHeaderRow>
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell></asp:TableHeaderCell>
                        <asp:TableHeaderCell></asp:TableHeaderCell>
                        <asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabDenominacion" runat="server"></asp:Literal></asp:TableHeaderCell>
                        <asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabEmpresa" runat="server"></asp:Literal></asp:TableHeaderCell>
                        <asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabReceptor" runat="server"></asp:Literal></asp:TableHeaderCell>
                        <asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabProveedor" runat="server"></asp:Literal></asp:TableHeaderCell>
                        <asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabProveedorERP" runat="server"></asp:Literal></asp:TableHeaderCell>
                        <asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabImporte" runat="server"></asp:Literal></asp:TableHeaderCell>
                        <asp:TableHeaderCell><asp:Literal ID="litCabObservaciones" runat="server"></asp:Literal></asp:TableHeaderCell>
                        <asp:TableHeaderCell><asp:Literal ID="litCabArchivos" runat="server"></asp:Literal></asp:TableHeaderCell>
<%--
                        <asp:TableHeaderCell><asp:Literal ID="litCabModificar" runat="server"></asp:Literal></asp:TableHeaderCell>
--%>
                        <asp:TableHeaderCell><asp:Literal ID="litCabEliminar" runat="server"></asp:Literal></asp:TableHeaderCell>
                    </asp:TableHeaderRow>
                </asp:Table>
            </HeaderTemplate>
            <HeaderStyle CssClass="FilaPar" />
            <FooterTemplate>
                <asp:Table ID="tblPie" runat="server">
                    <asp:TableRow><asp:TableCell ColumnSpan="13">
                        <fsep:Paginador ID="pagdsPie" runat="server" PagSize="10" OnCambioOrden="Paginador_OnCambioOrden" />
                    </asp:TableCell></asp:TableRow>
                </asp:Table>
            </FooterTemplate>
            <ItemTemplate>
                <asp:Table ID="tblItem" runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top"><asp:ImageButton ID="imgbtnLineas" runat="server" SkinID="ExpandirFondoTrans" CommandName="DetalleLineas" CommandArgument='<%#Eval("ID")%>'/></asp:TableCell>
                        <asp:TableCell VerticalAlign="Top"><fsn:FSNButton ID="BtnEmitirPedFavo" runat="server" CssClass="Boton" CommandName="EmitirPedidoFavorito" CommandArgument='<%#Eval("ID")%>'></fsn:FSNButton></asp:TableCell>
                        <asp:TableCell VerticalAlign="Top"><fsn:FSNLinkInfo ID="fsnlnkFilaDenPedido" Text='<%# Eval("Den")%>' runat="server" CommandName="ModificarPedidoFavorito" CommandArgument='<%# Eval("ID") %>' CssClass="Rotulo" style="text-decoration:none;"></fsn:FSNLinkInfo></asp:TableCell>
                        <asp:TableCell VerticalAlign="Top"><asp:Literal ID="litFilaEmpresa" runat="server" Text='<%# Eval("DENEMPRESA") %>'></asp:Literal></asp:TableCell>
                        <asp:TableCell VerticalAlign="Top"><fsn:FSNLinkInfo ID="fsnlnkFilaReceptor" runat="server" Text='<%# Eval("NomReceptor") %>' ContextKey='<%# Eval("Receptor") %>' PanelInfo="FSNPanelDatosPersona" CssClass="Rotulo" style="text-decoration:none;"></fsn:FSNLinkInfo></asp:TableCell>
                        <asp:TableCell VerticalAlign="Top"><fsn:FSNLinkInfo ID="fsnlnkFilaProveedor" runat="server" PanelInfo="FSNPanelDatosProve" ContextKey='<%# Eval("PROVECOD") %>' CssClass="Rotulo" style="text-decoration:none;"></fsn:FSNLinkInfo></asp:TableCell>
                        <asp:TableCell VerticalAlign="Top"><asp:Literal ID="litFilaProveedorERP" runat="server" Text='<%# Eval("Cod_PRove_ERP") %>'></asp:Literal></asp:TableCell>
                        <asp:TableCell HorizontalAlign="Right" VerticalAlign="Top"><asp:Literal ID="litFilaImporte" runat="server"></asp:Literal></asp:TableCell>
                        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="30"><fsn:FSNImageTooltip ID="btnFilaObservaciones" runat="server" SkinID="Observaciones" ContextKey='<%# Eval("ID") %>' PanelTooltip="pnlTooltipObservaciones" /></asp:TableCell>
                        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="30"><asp:ImageButton ID="btnFilaArchivos" runat="server" SkinID="Adjuntos" CommandName="Adjuntos" CommandArgument='<%# Eval("ID") %>' /></asp:TableCell>
<%--
                        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="30"><asp:ImageButton ID="btnFilaModificarFavorito" runat="server" SkinID="Modificar" CommandName="ModificarPedidoFavorito" CommandArgument='<%# Eval("ID") %>' /></asp:TableCell>
--%>
                        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="30"><asp:ImageButton ID="btnFilaEliminarFavorito" runat="server" SkinID="Eliminar" CommandName="EliminarPedidoFavorito" CommandArgument='<%# Eval("ID") %>' /></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="13">
                            <asp:Panel ID="pnlLineasPedido" runat="server" style="height: 0px; width: 100%; overflow: hidden;">
                            <asp:UpdatePanel ID="UpGridLineasPedidoFavo" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:GridView ID="grdLineasPedido" runat="server" AutoGenerateColumns="false" GridLines="None" CellPadding="4" Width="100%" OnRowDataBound="grdLineasPedidos_RowDataBound" OnRowCommand="grdLineasPedidos_RowCommand">
                                <Columns>
                                        <asp:TemplateField HeaderText="DArticulo" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkCodArt" runat="server" Font-Underline="false"  Text='<%#Eval("CODART")%>' CssClass="Rotulo" CommandName="MostrarDetalleArt" CommandArgument='<%#Eval("LINEAID")%>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DDenominación" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"  HeaderStyle-Font-Bold="true" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDenArt" runat="server" Font-Underline="false"  Text='<%#Eval("DENART")%>' CssClass="Rotulo" CommandName="MostrarDetalleArt" CommandArgument='<%#Eval("LINEAID")%>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DDest." HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"  HeaderStyle-Font-Bold="true" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <fsn:FSNLinkInfo ID="FSNlnkDest" runat="server" Font-Underline="false" Text='<%# Eval("DESTINO_DEN") %>' CssClass="Rotulo" ContextKey='<%# Eval("DESTINO") %>' PanelInfo="FSNPanelDatosDestino"></fsn:FSNLinkInfo>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="CANTIDAD" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"  HeaderText="DCant." HeaderStyle-Font-Bold="true" ItemStyle-VerticalAlign="Top" />
                                        <asp:TemplateField HeaderText="DUni." HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"  HeaderStyle-Font-Bold="true" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <fsn:FSNLinkTooltip ID="FSNlnkUni" runat="server" Text='<%# Eval("UNIDAD") %>' CssClass="Rotulo" Font-Underline="false" ContextKey='<%# Eval("UNIDEN") %>' PanelTooltip="pnlTooltipTexto"></fsn:FSNLinkTooltip>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"  HeaderText="DP.U." HeaderStyle-Font-Bold="true" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPrecUni" runat="server" Text='<%#Eval("PRECIOUNITARIO")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField  DataField="IMPORTE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" HeaderText="DImporte (EUR)" HeaderStyle-Font-Bold="true"  ItemStyle-VerticalAlign="Top"/>
                                        <asp:TemplateField HeaderText="DCat." HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <fsn:FSNLinkTooltip ID="FSNlnkCat" runat="server" Text='<%# Eval("CATEGORIA") %>' CssClass="Rotulo" Font-Underline="false" ContextKey='<%# Eval("CATEGORIARAMA") %>' PanelTooltip="pnlTooltipTexto"></fsn:FSNLinkTooltip>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DProceso" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <fsn:FSNLinkTooltip ID="FSNlnkProce" runat="server" Text='<%# Eval("ANYOPROCE") & "/" & Eval("GMN1_PROCE") & "/" & Eval("CODPROCE") %>' CssClass="Rotulo" Font-Underline="false" ContextKey='<%# Eval("DenProce")  %>' PanelTooltip="pnlTooltipProceso"></fsn:FSNLinkTooltip>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:BoundField DataField="Proceso" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" HeaderText="DProceso" HeaderStyle-Font-Bold="true" ItemStyle-VerticalAlign="Top" />--%>
                                        <%--<asp:BoundField DataField="PRES5DEN" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" HeaderText="DCC" HeaderStyle-Font-Bold="true" ItemStyle-VerticalAlign="Top" />--%>
                                        <asp:TemplateField HeaderText="DArch." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnAdjLin" runat="server" SkinID="Adjuntos" CommandName="AdjuntosLinea" CommandArgument='<%#Eval("LINEAID")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DObs." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate><fsn:FSNImageTooltip ID="FSNbtnObs" runat="server" SkinID="Observaciones" ContextKey='<%#Eval("ORDEN") & "/" & Eval("LINEAID")%>' PanelTooltip="pnlTooltipObservacionesLinea" /></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DELim." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate><asp:ImageButton ID="ImgBtnElimLineaFavo" runat="server" SkinID="Eliminar" ContextKey='<%# Eval("LINEAID") %>' CommandName="EliminarLinea" CommandArgument='<%# Eval("LINEAID") %>' /></ItemTemplate>
                                        </asp:TemplateField>
                                </Columns>
                                </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>      
                                <asp:Panel ID="pnlCargando" runat="server" Width="100%" Height="100" BackImageUrl="~/images/cargando.gif" style="background-repeat:no-repeat; background-position:center; display:none;">
                                    <asp:Literal ID="litCargando" runat="server" Text="DEl pedido ha sido modificado por otro usuario. Actualizando la información del pedido..."></asp:Literal>
                                </asp:Panel>
                            </asp:Panel>
                            <ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" SkinID="FondoTrans"
                                ImageControlID="imgbtnLineas" TargetControlID="pnlLineasPedido" Collapsed="true">
                            </ajx:CollapsiblePanelExtender>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ItemTemplate>
        </asp:DataList>
    </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>
