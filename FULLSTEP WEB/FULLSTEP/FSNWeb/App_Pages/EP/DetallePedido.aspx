﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="DetallePedido.aspx.vb" Inherits="Fullstep.FSNWeb.DetallePedido" %>
<%@ MasterType VirtualPath="~/App_Master/Menu.master" %>
<asp:Content ContentPlaceHolderID="head" ID="content1" runat="server">
    <style type="text/css">
        #divname
        {
            display: block;
            width: 600px;
            height: 400px;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -200px;
            margin-left: -300px;
        }
        .ButtonClass
        {
            cursor: pointer;
        }
        tr.border td
        {
            border: 1px solid grey;
        }
        #tblPlanesEntrega,#tblPlanesEntrega td,#tblPlanesEntrega th {
			border: 1px solid #aaaaaa;
		}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">   
    <div id="divDetalleFactura" runat="server" clientidmode="Static" style="float: left; width: 100%; height: 100%;">   
        <fsn:FSNPageHeader ID="FSNPageHeaderAprobacion" runat="server" VisibleBotonVolver="true"></fsn:FSNPageHeader>                 
        <div id="divAcciones" style="position:absolute; display:none;">
            <ig:WebDataMenu runat="server" ID="wdmAcciones" EnableScrolling="true" MaxDataBindDepth="1">
                <GroupSettings Orientation="Vertical" EnableAnimation="true"
                    AnimationType="OpacityAnimation" AnimationDuration="500" />               
            </ig:WebDataMenu>            
        </div>
        <asp:Panel ID="pnlCabeceraAprobacion" Visible="false" runat="server" Style="float: left;
            margin-top: 10px" BackColor="#f5f5f5" Font-Names="Arial" Width="99%">
            <table style="height: 15%; width: 100%; padding-bottom: 15px; padding-left: 5px" cellspacing="0" cellpadding="1" border="0">
                <tr>
                    <td style="padding-top: 5px; padding-bottom: 5px;" class="fondoCabecera">
                        <table style="width: 100%; table-layout: fixed; padding-left: 10px" border="0">
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="lblIDInstanciayEstado" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblLitImporte" runat="server" Text="DImporte:" CssClass="captionDarkGraySmall"
                                        Font-Size="12px"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblImporte" runat="server" Text="DXXXX" CssClass="label" Font-Bold="true" ClientIDMode="Static"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblLitCreadoPor" runat="server" Text="DAprovisionador:" CssClass="captionDarkGraySmall"
                                        Font-Size="12px"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCreadoPor" runat="server" Text="DXXXXXXXX" CssClass="label"></asp:Label>
                                    <asp:Image ID="imgInfCreadoPor" ImageUrl="images/info.gif" runat="server" Style="cursor: pointer" />
                                </td>
                                <td>
                                    <asp:Label ID="lblLitEstado" runat="server" Text="DEstado:" CssClass="captionDarkGraySmall"
                                        Font-Size="12px"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblEstado" runat="server" Text="DXXXXXX" CssClass="label"></asp:Label>
                                </td>
                                <td>
                                    <asp:HyperLink ID="HyperDetalle" runat="server" Text="DDetalle de tareas" CssClass="CaptionLink" style="cursor:pointer"></asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblLitFecAlta" runat="server" Text="DFec.Alta:" CssClass="captionDarkGraySmall"
                                        Font-Size="12px"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblFecAlta" runat="server" Text="DXXXXXX" CssClass="label"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblLitTipo" runat="server" Text="DTipo:" CssClass="captionDarkGraySmall"
                                        Font-Size="12px"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblTipo" runat="server" Text="DXXXXXX" CssClass="label"></asp:Label>
                                    <asp:Image ID="imgInfTipo" ImageUrl="~/images/info.gif" runat="server" Style="cursor: pointer" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div id="divError" style="display: none; position: absolute; left: 30px; background-color: white;
            border: solid black; border-width: 1px; width: 40%;">
            <table cellpadding="0" cellspacing="0" border="0" class="CajaError" style="height: auto;
                min-height: 100px; width: 95%;">
                <tr>
                    <td valign="top" align="left">
                        <div id="textErrorCab" style="width: 95%;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top" align="left">
                        <div id="textError" style="width: 95%;">
                        </div>
                    </td>
                </tr>
                <tr style="margin-top: 5px;">
                    <td>
                        <input id="btnAceptarError" type="button" value="button" runat="server" enableviewstate="False"
                            clientidmode="Static" />
                    </td>
                    <td>
                        <input id="btnCancelarError" type="button" value="button" runat="server" enableviewstate="False"
                            clientidmode="Static" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divMensaje" class="popupCN" style="display: none; position: absolute;">
            <div class="CajaError">
                <div id="textMensaje" style="width: 95%;">
                </div>
                <div style="text-align: center; margin-top: 1em;">
                    <div id="btnAceptarMensaje" class="botonRedondeado" style="margin-left: 0.5em;">
                        <asp:Label runat="server" ID="lblBotonBuscarIdentificador"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <div id="divCabeceraImportesPedido" class="Texto12 RectanguloEP" style="padding: 0.2em 0.3em;
            overflow: auto; float: left; width: 99%; margin-top: 15px; margin-bottom: 15px">
            <div id="infoProv" class="FilaSeleccionada">
                <table style="width:100%">
                    <tr class="FilaSeleccionada" style="height: 30px">
                        <td align="left" runat="server" id="tablaCollapseCab">
                            <asp:Label ID="LblProve" runat="server" Text="DProveedor:" CssClass="Rotulo" EnableViewState="True">
                            </asp:Label>
                            <asp:Label runat="server" ID="FSNLinkProve" ClientIDMode="static" CssClass="ButtonClass"></asp:Label>
                        </td>
                        <td align="right">
                            <asp:Label runat="server" ID="lblEstadoOrden" Text="DEstado:" CssClass="Rotulo"></asp:Label>
                            <asp:Label runat="server" ID="EstadoOrden" ClientIDMode="static"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divImportesPedido" style="display: none;">
                <table id="ImportesPedido" style="width: 100%;">
                    <tr>
                        <td align="left" runat="server" id="Td1" style="width: 42%;">
                            <div style="float: left; vertical-align: middle;" class="FilaPar">
                                <asp:Label ID="lblImporteBruto" runat="server" Text="DImporte Bruto:" CssClass="Rotulo"
                                    EnableViewState="True">
                                </asp:Label>
                            </div>
                            <div style="border-bottom-style: dotted; margin-left: 19%; vertical-align: bottom;
                                height: 8px;">
                            </div>
                        </td>
                        <td align="right" style="width: 8%;">
                            <asp:Label runat="server" ID="lblImpBruto" ClientIDMode="static">0</asp:Label>
                        </td>
                        <td>
                            <div style="visibility: hidden">
                                <asp:Label runat="server" ID="lblImpBruto_hdd" ClientIDMode="static">0</asp:Label>
                                <asp:Label runat="server" ID="lblImpBrutoConBajas_hdd" ClientIDMode="static">0</asp:Label>
                                <asp:Label runat="server" ID="lblImpBrutoLinea_hdd" ClientIDMode="static">0</asp:Label>
                                <asp:Label runat="server" ID="lblImpBrutoLineaConBajas_hdd" ClientIDMode="static">0</asp:Label>
                                <asp:Label runat="server" ID="lblImpBrutoSolicitud_hdd" ClientIDMode="static">0</asp:Label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" runat="server" id="Td2" style="width: 42%;">
                            <div style="float: left; vertical-align: middle;" class="FilaPar">
                                <asp:Label ID="lblCostesGenerales" runat="server" Text="DCostes Generales:" CssClass="Rotulo"
                                    EnableViewState="True">
                                </asp:Label>
                            </div>
                            <div style="border-bottom-style: dotted; margin-left: 19%; vertical-align: bottom;
                                height: 8px;">
                            </div>
                        </td>
                        <td align="right" style="width: 8%;">
                            <asp:Label runat="server" ID="lblCosteCabeceraTotal" ClientIDMode="static" CssClass="FilaPar">0</asp:Label>
                        </td>
                        <td>
                            <div style="visibility: hidden">
                                <asp:Label runat="server" ID="lblCosteCabeceraTotal_hdd" ClientIDMode="static">0</asp:Label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" runat="server" id="Td3" style="width: 42%;">
                            <div style="float: left; vertical-align: middle;" class="FilaPar">
                                <asp:Label ID="lblDescuentosGenerales" runat="server" Text="DDescuentos Generales:"
                                    CssClass="Rotulo" EnableViewState="True">
                                </asp:Label>
                            </div>
                            <div style="border-bottom-style: dotted; margin-left: 19%; vertical-align: bottom;
                                height: 8px;">
                            </div>
                        </td>
                        <td align="right" style="width: 8%;">
                            <asp:Label runat="server" ID="lblDescuentosCabeceraTotal" ClientIDMode="static" CssClass="FilaPar">0</asp:Label>
                        </td>
                        <td>
                            <div style="visibility: hidden">
                                <asp:Label runat="server" ID="lblDescuentosCabeceraTotal_hdd" ClientIDMode="static">0</asp:Label></div>
                        </td>
                    </tr>
                </table>
                <div id="campos" style="width: 100%;">
                </div>
            </div>
            <div style="visibility: hidden">
                <asp:Label runat="server" ID="lblImpTotalCuotas_hdd" ClientIDMode="static">0</asp:Label></div>            

            <div style="display:inline-block; width:50%;">            
            <asp:Panel ID="pnlCabeceraImporte"  runat="server" onClick="checkCollapseStatus();" 
                Width="100%">
                <table style="width: 100%;">
                    <tr>
                        <td valign="middle" style="width: 2%;">
                            <img id="imgExpandir" runat="server" src="../../images/expandir_abajo.png" clientidmode="Static" />
                        </td>
                        <td runat="server" id="Td4" style="width: 40%;">
                            <div>
                                <div style="float: left; vertical-align: middle;" class="FilaPar">
                                    <asp:Label ID="lblImporteTotal" runat="server" Text="DIMPORTE TOTAL:" CssClass="Rotulo"
                                        EnableViewState="True">
                                    </asp:Label>
                                </div>
                                <div style="border-bottom-style: dotted; margin-left: 15%; vertical-align: bottom;
                                    height: 8px;">
                                </div>
                            </div>
                        </td>
                        <td align="right" style="width: 8%;">
                            <asp:Label runat="server" ID="lblImporteTotalCabecera" ClientIDMode="static">0</asp:Label>
                            <asp:Label runat="server" ID="lblImporteTotalCabecera_hdd" ClientIDMode="static" style="display:none;">0</asp:Label>
                        </td>                                                                                                             
                    </tr>                      
                </table>
            </asp:Panel>
            </div>
            <div style="float:right;display:inline-block;" >  
              <table>                     
                <tr>
                    <td align="left">                       
                        <asp:Label runat="server" ID="lblEstadoOrdenIntegracion" Text="DEstadoIntegracion:" CssClass="Rotulo"></asp:Label> 
                        <asp:Label runat="server" ID="EstadoOrdenIntegracion" ClientIDMode="static"></asp:Label>
                    </td> 
                    <td align="right">
                        <div  id="divbtnRelanzar"> 
                        <fsn:FSNButton ID="btnRelanzar" runat="server" Alineacion="Right" Text="DRelanzar Integración" OnClientClick="return ConfirmarRelanzar();"></fsn:FSNButton>
                            </div>         
                    </td> 
                </tr>
              </table>

            </div>   

            </div>
		<br />             
        <!--Panel relanzar integracion-->
        <div id="pnlRelanzar" class="popupCN" style="margin:0 auto;position:absolute;z-index:1555;display:none;border:1px;padding:1.5em;">
            <div style="text-align:right;position:absolute;top:0.25em;right:0.25em;">
		            <img ID="ImageButton1" style="vertical-align:top;" src="../../Images/Cerrar.png" 
                        height="26px" width="23px" onclick ="cancelarRelanzar();" />
            </div>
            <div>
                <p style="margin:1em auto;"><asp:Label id='lblAlertaRelanzar' runat="server">D¿Desea relanzar la integración del pedido?</asp:Label></p>
            </div>
            <div style="text-align:center">
                <input type="button" id="btnAceptarRelanzar" runat="server" value="DAceptar" class="botonRedondeado" style="margin-right:2em" onclick="relanzar();"/>
                        <input type="button" id="btnCancelarRelanzar"  runat="server" value="DCancelar" class="botonRedondeado" style="margin-left:2em;" onclick="cancelarRelanzar();" />    
            </div>    
        </div>
        <!--Panel fin relanzar>
        <!-- popup, metido en dentro de update panel para que no haga postback-->
        <div id="pnlRelanzarOk"  class="popupCN" style="margin:0 auto;position:absolute;z-index:1555;display:none;padding:1.5em;">
            <div style="float:left;width: 100%; text-align:right; position:absolute;top:0.25em;right:0.25em;vertical-align:top">
		            <asp:ImageButton ID="ImageButton2" runat="server" SkinID="Cerrar" 
                        style="float:right; vertical-align:top;" ImageUrl="~/Images/Cerrar.png" 
                        Height="26px" Width="23px" OnClientClick="cerrarRelanzarOK();" /> 
            </div>
            <p style="margin:1em auto;clear:both;">
                <asp:Label id='lblRelanzarOK' runat="server">Dintegracion relanzada correctamente</asp:Label>
            </p>
            <div style="text-align:center;">
                <input type="button" id="btnRelanzarOk" runat="server" value="DAceptar" class="botonRedondeado" onclick="cerrarRelanzarOk();"/>
            </div>
        </div>        
        <!--CABECERA PEDIDO -->
        <div id="pesDatosGenerales" title="Datos Generales" style="clear: both; display: inline-block;"
            class="escenarioVistaEP escenarioSeleccionado">
            <asp:Label ID="lblDatosGenerales" runat="server" ClientIDMode="static">
            DDatos Generales</asp:Label></div>
        <div id="pesArObs" title="Archivos/Observaciones" style="clear: both; display: inline-block;"
            class="escenarioVistaEP">
            <asp:Label ID="lblArchivos" runat="server" ClientIDMode="static">
            DArchivos/Observaciones</asp:Label></div>
        <div id="pesCostes" title="Costes" style="clear: both; display: inline-block;" class="escenarioVistaEP">
            <asp:Label ID="lblCostes" runat="server" ClientIDMode="static">
            DCostes</asp:Label></div>
        <div id="pesDescuentos" title="Descuentos" style="clear: both; display: inline-block;"
            class="escenarioVistaEP">
            <asp:Label ID="lblDescuentos" runat="server" ClientIDMode="static">
            DDescuentos</asp:Label></div>
        <div id="pesOtrosDatos" title="Otros Datos" style="clear: both; display: inline-block;"
            class="escenarioVistaEP">
            <asp:Label ID="lblOtrosDatos" runat="server" ClientIDMode="static">
            DOtros Datos</asp:Label>
        </div>
        <br />
        <div id="DatosGenerales" class="Texto12 RectanguloEP" style="padding: 0.2em 0.3em;
            overflow: auto;">
            <br />
            <table cellpadding="1" cellspacing="1" border="0" width="100%">
                <tr>
                    <td style="width: 10%;">
                        <asp:Label ID="LblProveErp" CssClass="Etiqueta" runat="server" ClientIDMode="static">
                        </asp:Label>
                    </td>
                    <td style="width: 90%;" colspan="2">
                        <asp:Label ID="LblProveErpDato" runat="server" ClientIDMode="static">
                        </asp:Label>
                           <div style="display:inline; position:absolute;">
                            <img id="btnInfo" alt="Info" style="display:none; cursor: pointer" src="../../images/info.gif" />
                            <div id="pnlInfoProveERP" class="popupCN" style="position:absolute; display:none; background-color: rgb(232, 232, 232); width:450px; height:170px; ">    
                                <div style="position:relative;">
                                    <img id="AspaCerrar" alt="Cerrar" src="../../images/cerrar.png" style="left: 410px; display:block; position:relative; " onclick="$('#pnlInfoProveERP').hide()"/>
                                    <b><span id="lblTitulo" runat="server">TextosJScript[63]</span></b>
                                    <br />
                                    <b><span id="lblCodigo" runat="server">TextosJScript[64]</span></b> <span id="IdCodigo">IdCodigo</span>
                                    <br />
                                    <b><span id="lblDescripcion" runat='server'>TextosJScript[65]</span></b> <span id="IdDescripcion">IdDescripcion</span>
                                    <br />
                                    <b><span id="lblCampo1" runat="server">DCampo1:</span></b> <span id="IdCampo1" runat="server">IdCampo1</span>
                                    <br />
                                    <b><span id="lblCampo2" runat="server">DCampo2:</span></b> <span id="IdCampo2" runat="server">IdCampo2</span>
                                    <br />
                                    <b><span id="lblCampo3" runat="server">DCampo3:</span></b> <span id="IdCampo3" runat="server">IdCampo3</span>
                                    <br />
                                    <b><span id="lblCampo4" runat="server">DCampo4:</span></b> <span id="IdCampo4" runat="server">IdCampo4</span>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <asp:Label ID="lblSolicAcepProve" runat="server" ClientIDMode="static" CssClass="Etiqueta" Text="DSolicitar aceptación del proveedor"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblSolicAcepProveDato" runat="server" ClientIDMode="static"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 10%;">
                        <asp:Label ID="LblEmpresa" CssClass="Etiqueta" Text="DEmpresa:" runat="server"></asp:Label>
                    </td>
                    <td style="width: 25%;">
                        <asp:Label ID="LblEmpresaDato" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                    <td align="left" style="width: 13%;">
                        <asp:Label ID="LblTipoPedido" runat="server" Text="DTipo de pedido:" CssClass="Etiqueta"
                            EnableViewState="True">
                        </asp:Label>
                    </td>
                    <td style="width: 30%;">
                        <asp:Label ID="LblTipoPedidoDato" runat="server" EnableViewState="True"
                            ClientIDMode="Static">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 10%;">
                        <asp:Label ID="LblReceptor" Text="Receptor:" CssClass="Etiqueta" runat="server"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="LblReceptorDato" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                    <td align="left" style="width: 10%;">
                        <asp:Label ID="LblGestorTitulo" CssClass="Etiqueta" runat="server" Text="DGestor:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LblGestorCab" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="left">
                        <asp:Label ID="LblNumSap" CssClass="Etiqueta" runat="server" Text="DRef. en factura:">
                        </asp:Label>
                    </td>
                    <td valign="middle" align="left">
                        <asp:Label ID="LblNumSapDato" runat="server" ClientIDMode="Static">
                        </asp:Label>
                    </td>
                    <td style="width: 5%;">
                    </td>
                    <td valign="middle" align="left" style="width: 10%;">
                        <asp:Label ID="lblDireccionEnvioFactura" Text="DDir.Envio Factura:" CssClass="Etiqueta"
                            runat="server" ClientIDMode="static"></asp:Label>
                    </td>
                    <td valign="middle" align="left">
                        <asp:Label ID="lblDireccionEnvioFacturaDato" CssClass="Normal" runat="server"
                            ClientIDMode="static"></asp:Label>
                        <asp:Label ID="lblCodDireccion" CssClass="Normal" runat="server" Style="display: none"
                            ClientIDMode="static"></asp:Label>
                    </td>
                </tr>

                <tr id="trPedidoAbierto">
                    <td align="left" style="width: 10%;">
                        <asp:label ID="lblPedidoAbierto" Text="dPedido abierto :" runat="server" CssClass="Etiqueta" ClientIDMode="static"></asp:label>
                    </td>
                    <td style="width: 25%;">
                        <asp:Label ID="lblCodPedidoAbierto" CssClass="Normal" runat="server" ClientIDMode="static"></asp:Label>
                        <fsn:FSNLinkInfo ID="FSNLinkFilaPedidoAbierto" runat="server" ClientIDMode = "Static" Text="" ContextKey="" PanelInfo="FSNPanelDatosPedidoAbierto" CssClass="Rotulo" style="text-decoration:none;">
                            <img id="imgInfoPedAbierto" runat="server" clientidmode="Static" alt="Info" style="cursor: pointer;border:0px;" src="../../images/info.gif" />
                        </fsn:FSNLinkInfo>
                        <fsn:FSNPanelInfo ID="FSNPanelDatosPedidoAbierto" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx"  Contenedor = "" ServiceMethod="Detalle_PedidoAbierto_Datos" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="ArchivosObser" class="Texto12 RectanguloEP" style="display: none; padding: 0.2em 0.3em;
            overflow: auto;">
            <br />
            <fieldset id="fsTextObser" runat="server" class="RectanguloEP" style="margin-top: 10px;
                margin-bottom: 10px; width: 95%;">
                <legend align="left">
                    <asp:Label ID="lblObsGen" CssClass="Etiqueta" runat="server" Text="DObservaciones Generales"
                        ClientIDMode="Static"></asp:Label></legend>
                <textarea id="textObser" rows="3" style="width: 95%;" runat="server" clientidmode="Static" disabled="disabled"></textarea>
            </fieldset>
            <!-- Panel adjuntos de artículo -->
            <fieldset id="fsTableAdjuntos" runat="server" class="RectanguloEP" style="margin-top: 10px;
                margin-bottom: 10px; width: 95%;">
                <legend align="left">
                    <asp:Label ID="lblArcPed" CssClass="Etiqueta" runat="server" Text="DArchivos para el pedido"
                        ClientIDMode="Static"></asp:Label></legend>
                <div id="tablaAdjuntos">
                </div>
            </fieldset>
            <br />
            <div id="DivPanelAdjuntos" class="modalPopup" style="display: none;" style="display: none;
                position: absolute; z-index: 10003; width: 300px; min-height: 70px;">
                <!-- Panel de adjuntos del pedido -->
                <!--OnPreRender="PanelAdjunPed_PreRender">-->
                <table id="PanelAdjunPedTitulo" cellpadding="4" cellspacing="0" border="0">
                    <tr>
                        <td valign="bottom">
                            <asp:Image ID="Image1" runat="server" SkinID="CabAdjuntos" />
                        </td>
                        <td valign="bottom">
                            <asp:Label ID="Label1" runat="server" CssClass="RotuloGrande" EnableViewState="False"
                                Text="...."></asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
                <table id="PanelAdjunPedCuerpo" cellpadding="4" cellspacing="0" border="0">
                    <tr>
                        <td>
                            <input id="files" name="files" type="file" size="55px" width="380px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="comentarioAdjunto" rows="5" cols="40"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table id="PanelAdjunPedConfirmacionCancelacion" cellpadding="10" cellspacing="0"
                                border="0">
                                <tr>
                                    <td>
                                        <div>
                                            <span onclick="subirArchivoCesta()"><a class="Boton" style="color: White; background-color: rgb(171, 0, 32);
                                                font-size: 12px; float: right; text-decoration: none; border-style: none; border-color: inherit;
                                                border-width: 0px; padding: 3px 10px; border-radius: 4px;" onfocus="this.blur();"
                                                onmouseup="this.style.padding='3px 10px 3px 10px';this.style.cursor='hand';"
                                                onmousedown="this.style.padding='4px 10px 2px 10px';this.style.cursor='hand';"
                                                onmouseout="this.style.backgroundColor='#AB0020';this.style.padding='3px 10px 3px 10px';this.style.cursor='pointer';"
                                                onmouseover="this.style.backgroundColor='#780000';this.style.cursor='hand';">DAceptar</a>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <span onclick="ocultarAdjuntar()"><a class="Boton" style="color: White; background-color: rgb(171, 0, 32);
                                                font-size: 12px; float: right; text-decoration: none; border-style: none; border-color: inherit;
                                                border-width: 0px; padding: 3px 10px; border-radius: 4px;" onfocus="this.blur();"
                                                onmouseup="this.style.padding='3px 10px 3px 10px';this.style.cursor='hand';"
                                                onmousedown="this.style.padding='4px 10px 2px 10px';this.style.cursor='hand';"
                                                onmouseout="this.style.backgroundColor='#AB0020';this.style.padding='3px 10px 3px 10px';this.style.cursor='pointer';"
                                                onmouseover="this.style.backgroundColor='#780000';this.style.cursor='hand';">DCancelar</a>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <asp:HiddenField ID="HiddenField1" runat="server" ClientIDMode="Static" Value="test" />
            <asp:TextBox ID="listaAdjuntosJSON" runat="server" ClientIDMode="Static" Style="display: none;"
                Text="[]"></asp:TextBox>
            <div id="listaAdjuntos" style="clear: both; margin-top: 5px; width: 50%;">
            </div>
        </div>
        <div id="OtrosDatos" class="Texto12 RectanguloEP" style="display: none; padding: 0.2em 0.3em;
            overflow: auto;">
            <div id="divAtributosOrden" runat="server" style="position: relative; margin-left: 5px;
                float: left; padding: 6px;">
                <table id="tblAtributosOrden" style="border: 1px solid #E8E8E8; width: 100%; table-layout: fixed;
                    border-collapse: collapse; background-color: #F0F0F0;">
                    <tr style="background-color: #cccccc; height: 20px">
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblIdAtributoTablaOrden" runat="server" Text="DId"></asp:Label>
                        </td>
                        <td style="width: 23%; height: 20px">
                            <asp:Label ID="lblCodigoAtributoTablaOrden" runat="server" Text="DCodigo"></asp:Label>
                        </td>
                        <td style="width: 40%; height: 20px">
                            <asp:Label ID="lblDenomAtributoTablaOrden" runat="server" Text="DDenominacion"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblTipoAtributoTablaOrden" runat="server" Text="DTipo"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblIntroAtributoTablaOrden" runat="server" Text="DIntro"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblObligAtributoTablaOrden" runat="server" Text="DObligatorio"></asp:Label>
                        </td>
                        <td style="width: 37%; height: 22px">
                            <asp:Label ID="lblValorAtributoTablaOrden" runat="server" Text="DValor"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="Costes" class="Texto12 RectanguloEP" style="display: none; padding: 0.2em 0.3em;
            overflow: auto;">
            <div id="divAtributosCoste" runat="server" style="position: relative;">
                <br />
                <table id="tblAtributosCoste" style="border: 1px solid #E8E8E8; width: 100%; background-color: #F0F0F0;
                    border-collapse: collapse;">
                    <tr style="background-color: #cccccc; height: 20px">
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblIdCoste" runat="server" Text="DId"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px">
                            <input type="checkbox" style="visibility: hidden;" />
                        </td>
                        <td style="width: 25%; height: 20px">
                            <asp:Label ID="lblCodCoste" runat="server" Text="DCodigo"></asp:Label>
                        </td>
                        <td style="width: 38%; height: 20px">
                            <asp:Label ID="lblDenCoste" runat="server" Text="DDenominacion"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblTipoCoste" runat="server" Text="DTipo"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblIntroCoste" runat="server" Text="DIntro"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblOblCoste" runat="server" Text="DObligatorio"></asp:Label>
                        </td>
                        <td style="width: 15%; height: 22px">
                            <asp:Label ID="lblValCoste" runat="server" Text="DValor"></asp:Label>
                        </td>
                        <td style="width: 18%; height: 22px">
                            <asp:Label ID="lblImpCoste" runat="server" Text="DImporte"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table id="tblCosteTotales" style="border: 1px solid #E8E8E8; width: 100%; border-collapse: collapse;
                    margin-top: 2px;">
                    <tr class="border" style="background-color: #cccccc; height: 20px">
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblCosteTotal_hdd" runat="server" ClientIDMode="Static" Text="0"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px">
                            <input type="checkbox" style="visibility: hidden;" />
                        </td>
                        <td style="width: 63%; height: 20px">
                            <asp:Label ID="lblTotCostes" runat="server" Text="DTotal Costes Generales"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                        </td>
                        <td style="width: 15%; height: 22px">
                        </td>
                        <td style="width: 18%; height: 22px">
                            <asp:Label ID="lblCosteTotal" runat="server" ClientIDMode="Static" Style="width: 97%;
                                border: none; text-align: right; display: block;"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="Descuentos" class="Texto12 RectanguloEP" style="display: none; padding: 0.2em 0.3em;
            overflow: auto;">
            <div id="divAtributosDescuento" runat="server">
                <br />
                <table id="tblAtributosDescuento" style="border: 1px solid #E8E8E8; width: 100%;
                    border-collapse: collapse; background-color: #F0F0F0;">
                    <tr class="border" style="background-color: #cccccc; height: 20px">
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblIdDescuento" runat="server" Text="DId"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px">
                            <input type="checkbox" style="visibility: hidden;" />
                        </td>
                        <td style="width: 25%; height: 20px">
                            <asp:Label ID="lblCodDescuento" runat="server" Text="DCodigo"></asp:Label>
                        </td>
                        <td style="width: 38%; height: 20px">
                            <asp:Label ID="lblDenDescuento" runat="server" Text="DDenominacion"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblTipoDescuento" runat="server" Text="DTipo"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblIntroDescuento" runat="server" Text="DIntro"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblOblDescuento" runat="server" Text="DObligatorio"></asp:Label>
                        </td>
                        <td style="width: 15%; height: 22px">
                            <asp:Label ID="lblValDescuento" runat="server" Text="DValor"></asp:Label>
                        </td>
                        <td style="width: 18%; height: 22px">
                            <asp:Label ID="lblImpDescuento" runat="server" Text="DImporte"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table id="tblDescuentoTotales" style="border: 1px solid #E8E8E8; width: 100%; border-collapse: collapse;
                    margin-top: 2px;">
                    <tr class="border" style="background-color: #cccccc; height: 20px">
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblDescuentoTotal_hdd" runat="server" ClientIDMode="Static" Text="0"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px;">
                            <input type="checkbox" style="visibility: hidden;" />
                        </td>
                        <td style="width: 63%; height: 20px">
                            <asp:Label ID="lblTotDescuento" runat="server" Text="DTotal Descuentos Generales"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                        </td>
                        <td style="width: 15%; height: 22px;">
                        </td>
                        <td style="width: 18%; height: 22px;">
                            <asp:Label ID="lblDescuentoTotal" runat="server" ClientIDMode="Static" Style="width: 97%;
                                border: none; text-align: right; display: block;"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <!--FIN CABECERA PEDIDO -->
        <br />
        <!--RESUMEN LINEAS PEDIDO -->
        <div id="pesResumenLineas" title="Resumen Lineas Pedido" style="float: left; display: inline-block;
            margin-top: 0.5em;" class="escenarioVista escenarioSeleccionado">
            <asp:Label ID="lblResumenLinea" runat="server" ClientIDMode="static">
            DResumen Lineas Pedido</asp:Label></div>
        <div id="pesDatosLineas" title="Datos de linea" style="display: inline-block; margin-top: 0.5em;"
            class="escenarioVistaEP">
            <asp:Label ID="lblDatosLinea" runat="server" ClientIDMode="static">
            DDatos de linea</asp:Label></div>
        <div id="pesCostesLinea" title="Costes" style="display: inline-block; margin-top: 0.5em;"
            class="escenarioVistaEP">
            <asp:Label ID="lblCostesLinea" runat="server" ClientIDMode="static">
            DCostes</asp:Label></div>
        <div id="pesDescuentosLinea" title="Descuentos" style="display: inline-block; margin-top: 0.5em;"
            class="escenarioVistaEP">
            <asp:Label ID="lblDescuentosLinea" runat="server" ClientIDMode="static"></asp:Label>
        </div>
        <div id="pesPlanesEntrega" style="display: none; margin-top: 0.5em;" class="escenarioVistaEP">
			<asp:Label ID="lblPlanesEntrega" runat="server" ClientIDMode="static"></asp:Label>
		</div>
        <div id="ResumenLineas" class="Texto12 RectanguloEP" style="padding: 0.2em 0.3em;
            overflow: auto;">
            <div style="visibility: hidden;">
                <asp:Label runat="server" ID="lblTotalCostesLinea_hdd" ClientIDMode="static">0</asp:Label>
                <asp:Label runat="server" ID="lblTotalDescuentosLinea_hdd" ClientIDMode="static">0</asp:Label>
            </div>
            <asp:Button runat="server" ID="btnExportarExcel" Style="display: none;" />
            <asp:UpdatePanel ID="pnlGrid" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnExportarExcel" EventName="Click" />
                </Triggers>
                <ContentTemplate>
                    <ig:WebHierarchicalDataGrid ID="whdgArticulos" runat="server" Visible="true" AutoGenerateColumns="false"
                        Width="100%" ShowHeader="true" EnableAjax="false" EnableAjaxViewState="false"
                        EnableDataViewState="false" CssClass="headerNoWrap">
                        <Columns>
                            <ig:TemplateDataField Key="Key_DARBAJA"></ig:TemplateDataField>
                            <ig:BoundDataField Key="Key_NUM_LINEA" DataFieldName="NUM_LINEA"></ig:BoundDataField>
                            <ig:BoundDataField Key="Key_IDLINEA" DataFieldName="IDLINEA" Hidden="true"></ig:BoundDataField>
                            <ig:BoundDataField Key="Key_COD_ITEM" DataFieldName="COD_ITEM" Hidden="true"></ig:BoundDataField>
                            <ig:BoundDataField Key="Key_ART_DEN" DataFieldName="ART_DEN" ></ig:BoundDataField>
                            <ig:BoundDataField Key="Key_PREC" DataFieldName="PREC"></ig:BoundDataField>
                            <ig:BoundDataField Key="Key_CANT" DataFieldName="CANT"></ig:BoundDataField>
                            <ig:BoundDataField Key="Key_UNI" DataFieldName="UP"></ig:BoundDataField>
                            <ig:TemplateDataField Key="Key_COSTES"></ig:TemplateDataField>
                            <ig:TemplateDataField Key="Key_DESCUENTOS"></ig:TemplateDataField>
                            <ig:TemplateDataField Key="Key_IMPBRUTO"></ig:TemplateDataField>
                            <ig:BoundDataField Key="Key_UP" DataFieldName="UP"></ig:BoundDataField>
                            <ig:TemplateDataField Key="Key_CANTIDADPDTE"></ig:TemplateDataField>
                            <ig:TemplateDataField Key="Key_IMPORTEPDTE"></ig:TemplateDataField>
                            <ig:BoundDataField Key="Key_EST" DataFieldName="EST"></ig:BoundDataField>
                            <ig:BoundDataField Key="Key_INSTANCIA_APROB" DataFieldName="INSTANCIA_APROB"></ig:BoundDataField>
                            <ig:BoundDataField Key="Key_FECENTREGA" DataFieldName="FECENTREGA"></ig:BoundDataField>
                            <ig:BoundDataField Key="Key_FECENTREGAPROVE" DataFieldName="FECENTREGAPROVE"></ig:BoundDataField>
                            <ig:TemplateDataField Key="Key_RECEPCIONES"></ig:TemplateDataField>
                            <ig:TemplateDataField Key="Key_APROBAR"></ig:TemplateDataField>
                            <ig:TemplateDataField Key="Key_RECHAZAR"></ig:TemplateDataField>
                            <ig:TemplateDataField Key="Key_FRA"></ig:TemplateDataField>
                            <ig:TemplateDataField Key="Key_PAGO"></ig:TemplateDataField>
                        </Columns>
                        <Behaviors>
                            <ig:Activation Enabled="true" />
                            <ig:RowSelectors Enabled="true" RowSelectorCssClass="RowSelectorCssClass">
                            </ig:RowSelectors>
                            <ig:Selection Enabled="true" CellSelectType="Single" RowSelectType="Multiple" ColumnSelectType="None">
                                <SelectionClientEvents RowSelectionChanged="whdgArticulos_RowSelectionChanged" />
                            </ig:Selection>
                            <ig:Paging Enabled="false">
                            </ig:Paging>
                            <ig:ColumnResizing Enabled="true">
                                <ColumnSettings>
                                    <ig:ColumnResizeSetting ColumnKey="Key_APROBAR" EnableResize="false" />
                                    <ig:ColumnResizeSetting ColumnKey="Key_RECHAZAR" EnableResize="false" />
                                    <ig:ColumnResizeSetting ColumnKey="Key_RECEPCIONES" EnableResize="false" />
                                    <ig:ColumnResizeSetting ColumnKey="Key_FRA" EnableResize="false" />
                                    <ig:ColumnResizeSetting ColumnKey="Key_PAGO" EnableResize="false" />
                                </ColumnSettings>
                            </ig:ColumnResizing>
                        </Behaviors>
                        <ClientEvents Click="whdgArticulos_CellClick" /> 
                    </ig:WebHierarchicalDataGrid>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="DatosLinea" class="Texto12 RectanguloEP" style="padding: 0.2em 0.3em; overflow: auto; margin-bottom: 10px; display: none;">
            <table style="border-collapse: collapse; width: 100%;">
                <tr class="FilaSeleccionada" style="height: 30px;">
                    <td style="width: 2%;">
                        <asp:Label ID="lblNumLinea" runat="server" CssClass="Etiqueta" Text="DLinea"></asp:Label>:
                    </td>
                    <td style="width: 98%;">
                        <asp:Label ID="lblLinea" runat="server" CssClass="Normal" style="margin-left:0.5em;" ClientIDMode="static"></asp:Label>
                    </td>
                </tr>
            </table>
            <hr style="width: 100%;" />
            <table style="width: 100%">
                <tr>
                    <td>
                        <asp:Label ID="LblDest" runat="server" CssClass="Etiqueta" Text="DDestino"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblDestino" runat="server" CssClass="Normal" Width="200px" Text=""
                            ClientIDMode="static"></asp:Label>
                        <asp:Image ID="ImgDestInfo" ImageUrl="~/images/info.gif" runat="server" ClientIDMode="Static" Style="cursor: pointer" />
                        <div id="PnlDestInfo" class="popupCN" style="position:absolute; display:none; background-color: rgb(232, 232, 232); width:450px; height:250px;z-index:1005; ">
                                <div style="position:relative;padding: 15px;">
                                        <img id="ImgDestInfoCerrar" alt="Cerrar" src="../../images/cerrar.png" style="left: 200px; display:block; position:relative;cursor: pointer; " />
                                        <b>  <span id="lblDestInfoTituloCod">lblDestInfoTituloCod</span></b> 
                                        <br />
                                        <b>  <span id="lblDestInfoDir" runat="server"></span></b>: <span id="txtDestInfoDir"></span>
                                        <br />
                                        <b>  <span id="lblDestInfoCP" runat='server'></span></b>: <span id="txtDestInfoCP"></span>
                                        <br />
                                        <b>  <span id="lblDestInfoPob" runat="server"></span></b>: <span id="txtDestInfoPob"></span>
                                        <br />
                                        <b>  <span id="lblDestInfoProv" runat="server"></span></b>: <span id="txtDestInfoProv"></span>
                                        <br />
                                        <b>  <span id="lblDestInfoPais" runat="server"></span></b>: <span id="txtDestInfoPais"></span>
                                        <br />
                                        <b>  <span id="lblDestInfoTfno" runat="server"></span></b>: <span id="txtDestInfoTfno"></span>
                                        <br />
                                        <b>  <span id="lblDestInfoFAX" runat="server"></span></b>: <span id="txtDestInfoFAX"></span>
                                        <br />
                                        <b>  <span id="lblDestInfoEmail" runat="server"></span></b>: <span id="txtDestInfoEmail"></span>
                                    </div>
                            </div>
                    </td>
                    <td colspan="2" style="width:50%;">
                        <asp:Label ID="LblAlmacen" runat="server" CssClass="Etiqueta"></asp:Label>
                        <asp:Label ID="LblAlmacenDato" runat="server" Text="" ClientIDMode="Static" style="margin-left:1em;"></asp:Label>

                    </td>                    
                    <td>&nbsp;</td>
                </tr>
                <tr id="Fila5Tabla1" runat="server" clientidmode="Static">
                    <td>
                        <asp:Label ID="lblCentroCoste" runat="server" CssClass="Etiqueta" Text="DCentro de Coste:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="tbCtroCoste" runat="server" CssClass="Normal" ClientIDMode="Static"></asp:Label>
                        <asp:HiddenField ID="tbCtroCoste_Hidden" runat="server" ClientIDMode="Static" />
                    </td>
                    <td>
                        <asp:Label ID="lblPartidaPresupuestaria" runat="server" CssClass="Etiqueta" ClientIDMode="Static"
                            Text="DContrato"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="tbPPres" runat="server" CssClass="Normal" ClientIDMode="Static"></asp:Label>
                        <asp:HiddenField ID="tbPPres_Hidden" runat="server" ClientIDMode="Static" />
                    </td>
                </tr>
                <tr id="trCategoria">
                    <td>
                        <asp:Label ID="lblCategoria" runat="server" CssClass="Etiqueta" Text="DCategoría:"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:Label ID="lblCategoriaDato" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                    </td>
                </tr>
                <tr id="divPartidas">
                    <!--meter aquí los centros de coste / presupuestos activos-->
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblCentro" runat="server" CssClass="Etiqueta" Text="DCentro:"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:Label ID="lblTituloCentro" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                    </td>
                </tr>
            </table>
            <div id="divAtributosLinea" runat="server" style="position: relative; margin-left: 5px;
                float: left; padding: 6px;">
                <table id="tblAtributosLinea" style="border: 1px solid #E8E8E8; width: 100%; table-layout: fixed;
                    border-collapse: collapse">
                    <tr style="background-color: #cccccc; height: 20px">
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblIdAtributoTablaLinea" runat="server" Text="DId"></asp:Label>
                        </td>
                        <td style="width: 23%; height: 20px">
                            <asp:Label ID="lblCodigoAtributoTablaLinea" runat="server" Text="DCodigo"></asp:Label>
                        </td>
                        <td style="width: 40%; height: 20px">
                            <asp:Label ID="lblDenomAtributoTablaLinea" runat="server" Text="DDenominacion"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblTipoAtributoTablaLinea" runat="server" Text="DTipo"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblIntroAtributoTablaLinea" runat="server" Text="DIntro"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblObligAtributoTablaLinea" runat="server" Text="DObligatorio"></asp:Label>
                        </td>
                        <td style="width: 37%; height: 22px">
                            <asp:Label ID="lblValorAtributoTablaLinea" runat="server" Text="DValor"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <fieldset id="fsObserLineas" runat="server" class="RectanguloEP" style="margin-top: 10px;
                margin-bottom: 10px; width: 95%;">
                <legend align="left">
                    <asp:Label ID="lblObservaciones" CssClass="Etiqueta" runat="server" Text="DObservaciones Linea"
                        ClientIDMode="Static"></asp:Label></legend>
                <textarea id="txtObservacionesLinea" rows="3" style="width: 95%;" runat="server" disabled="disabled"  clientidmode="Static"></textarea>
            </fieldset>
            <!-- Panel adjuntos de artículo -->
            <fieldset id="fsTablaAdjuntosLinea" runat="server" class="RectanguloEP" style="margin-top: 10px;
                margin-bottom: 10px; width: 95%;">
                <legend align="left">
                    <asp:Label ID="lblArchivosLineas" CssClass="Etiqueta" runat="server" Text="DArchivos para las lineas"
                        ClientIDMode="Static"></asp:Label></legend>
                <div id="divArchivosLinea">
                </div>
            </fieldset>
        </div>
        <div id="DatosDestinos" style="display: none; z-index: 1005;" class="popupCN">
            <table cellspacing="0" border="0" width="100%">
                <tr>
                    <td valign="bottom">
                        <asp:Image ID="imgDestinos" runat="server" SkinID="CabDestinos" />
                    </td>
                    <td>
                        <asp:Label ID="LblTxtSelecDest" runat="server" CssClass="RotuloGrande" Text="DSeleccione destino de la lista"
                            EnableViewState="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                        <div id='cboDest' style="float: left; height: 22px; width: 99%;">
                        </div>
                        <table cellpadding="4" cellspacing="2" border="0" width="100%">
                            <tr>
                                <td class="FilaSeleccionada">
                                    <asp:Label ID="LblCodDest" runat="server" CssClass="Normal" EnableViewState="False"
                                        Text="DCodigo"></asp:Label>
                                </td>
                                <td class="FilaPar2">
                                    <asp:Label ID="LblCodDestSrv" runat="server" CssClass="Normal" EnableViewState="False"
                                        ClientIDMode="static"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="FilaSeleccionada">
                                    <asp:Label ID="LblDenDest" runat="server" CssClass="Normal" Text="DDenominación"
                                        EnableViewState="False"></asp:Label>
                                </td>
                                <td class="FilaPar2">
                                    <asp:Label ID="LblDenDestSrv" CssClass="Normal" runat="server" EnableViewState="True"
                                        ClientIDMode="static"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="FilaSeleccionada">
                                    <asp:Label ID="LblDirecDest" runat="server" CssClass="Normal" EnableViewState="False"
                                        Text="DDireccion"></asp:Label>
                                </td>
                                <td class="FilaPar2">
                                    <asp:Label ID="LblDirecDestSrv" runat="server" CssClass="Normal" EnableViewState="False"
                                        ClientIDMode="static"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="FilaSeleccionada">
                                    <asp:Label ID="LblCpDest" runat="server" CssClass="Normal" EnableViewState="False"
                                        Text="DCP"></asp:Label>
                                </td>
                                <td class="FilaPar2">
                                    <asp:Label ID="LblCpDestSrv" runat="server" CssClass="Normal" EnableViewState="False"
                                        ClientIDMode="static"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="FilaSeleccionada">
                                    <asp:Label ID="LblPobDest" runat="server" CssClass="Normal" Text="DPoblación" EnableViewState="False"></asp:Label>
                                </td>
                                <td class="FilaPar2">
                                    <asp:Label ID="LblPobDestSrv" runat="server" CssClass="Normal" EnableViewState="False"
                                        ClientIDMode="static"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="FilaSeleccionada">
                                    <asp:Label ID="LblProvDest" CssClass="Normal" runat="server" Text="DProv." EnableViewState="False"></asp:Label>
                                </td>
                                <td class="FilaPar2">
                                    <asp:Label ID="LblProvDestSrv" runat="server" CssClass="Normal" EnableViewState="False"
                                        ClientIDMode="static"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="FilaSeleccionada">
                                    <asp:Label ID="LblPaisDest" runat="server" CssClass="Normal" Text="DPaís" EnableViewState="False"></asp:Label>
                                </td>
                                <td class="FilaPar2">
                                    <asp:Label ID="LblPaisDestSrv" runat="server" CssClass="Normal" EnableViewState="False"
                                        ClientIDMode="static"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <br />
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td align="right">
                                    <input id="BtnAcepDest" type="button" value="Aceptar" runat="server" clientidmode="Static" />
                                </td>
                                <td style="width: 25px">
                                    &nbsp;
                                </td>
                                <td align="left">
                                    <input id="BtnCancDest" type="button" value="Cancelar" runat="server" clientidmode="Static" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div id="CostesLinea" class="Texto12 RectanguloEP" style="display: none; padding: 0.2em 0.3em; margin-bottom: 10px; overflow: auto;">
            <table style="border-collapse: collapse; width: 100%;">
                <tr class="FilaSeleccionada" style="height: 30px;">
                    <td style="width: 2%;">
                        <asp:Label ID="lblNumLineaCoste" runat="server" CssClass="Etiqueta" Text="DLinea"></asp:Label>:
                    </td>
                    <td style="width: 98%;">
                        <asp:Label ID="lblLineaCoste" runat="server" CssClass="Normal" style="margin-left:0.5em;" ClientIDMode="static"></asp:Label>
                    </td>
                </tr>
            </table>
            <hr style="width: 100%;" />
            <div id="divAtributosCosteLinea" runat="server" style="position: relative; width: 100%;">
                <table id="tblAtributosCosteLinea" style="border: 1px solid #E8E8E8; width: 100%;
                    border-collapse: collapse">
                    <tr class="border" style="background-color: #cccccc; height: 20px">
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblIdCosteLinea" runat="server" Text="DId"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px">
                            <input type="checkbox" style="visibility: hidden;" />
                        </td>
                        <td style="width: 25%; height: 20px">
                            <asp:Label ID="lblCodCosteLinea" CssClass="Etiqueta" runat="server" Text="DCodigo"></asp:Label>
                        </td>
                        <td style="width: 38%; height: 20px">
                            <asp:Label ID="lblDenCosteLinea" CssClass="Etiqueta" runat="server" Text="DDenominacion"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblTipoCosteLinea" CssClass="Etiqueta" runat="server" Text="DTipo"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblIntroCosteLinea" CssClass="Etiqueta" runat="server" Text="DIntro"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblOblCosteLinea" CssClass="Etiqueta" runat="server" Text="DObligatorio"></asp:Label>
                        </td>
                        <td style="width: 15%; height: 22px">
                            <asp:Label ID="lblValCosteLinea" CssClass="Etiqueta" runat="server" Text="DValor"></asp:Label>
                        </td>
                        <td style="width: 18%; height: 22px">
                            <asp:Label ID="lblImpCosteLinea" CssClass="Etiqueta" runat="server" Text="DImporte"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table id="Table2" style="border: 1px solid #E8E8E8; width: 100%; border-collapse: collapse;
                    margin-top: 2px;">
                    <tr class="border" style="background-color: #cccccc; height: 20px">
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblCosteTotalLinea_hdd" runat="server" ClientIDMode="Static" Text="0"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px">
                            <input type="checkbox" style="visibility: hidden;" />
                        </td>
                        <td style="width: 63%; height: 20px">
                            <asp:Label ID="lblTotCostesLinea" runat="server" Text="DTotal Costes Generales"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                        </td>
                        <td style="width: 15%; height: 22px">
                        </td>
                        <td style="width: 18%; height: 22px">
                            <asp:Label ID="lblCosteTotalLinea" runat="server" ClientIDMode="Static" Style="width: 97%;
                                border: none; text-align: right; display: block;"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="DescuentosLinea" class="Texto12 RectanguloEP" style="display: none; padding: 0.2em 0.3em; margin-bottom: 10px; overflow: auto;">
            <table style="border-collapse: collapse; width: 100%;">
                <tr class="FilaSeleccionada" style="height: 30px;">
                    <td style="width: 2%;">
                        <asp:Label ID="lblNumLineaDescuento" runat="server" CssClass="Etiqueta" Text="DLinea"></asp:Label>:
                    </td>
                    <td style="width: 98%;">
                        <asp:Label ID="lblLineaDescuento" runat="server" CssClass="Normal" style="margin-left:0.5em;" ClientIDMode="static"></asp:Label>
                    </td>
                </tr>
            </table>
            <hr width="100%" />
            <div id="divAtributosDescuentoLinea" runat="server">
                <table id="tblAtributosDescuentoLinea" style="border: 1px solid #E8E8E8; width: 100%;
                    border-collapse: collapse">
                    <tr class="border" style="background-color: #cccccc; height: 20px; width: 100%;">
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblIdDescuentoLinea" runat="server" Text="DId"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px">
                            <input type="checkbox" style="visibility: hidden;" />
                        </td>
                        <td style="width: 25%; height: 20px">
                            <asp:Label ID="lblCodDescuentoLinea" runat="server" Text="DCodigo"></asp:Label>
                        </td>
                        <td style="width: 38%; height: 20px">
                            <asp:Label ID="lblDenDescuentoLinea" runat="server" Text="DDenominacion"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblTipoDescuentoLinea" runat="server" Text="DTipo"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblIntroDescuentoLinea" runat="server" Text="DIntro"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblOblDescuentoLinea" runat="server" Text="DObligatorio"></asp:Label>
                        </td>
                        <td style="width: 15%; height: 22px">
                            <asp:Label ID="lblValDescuentoLinea" runat="server" Text="DValor"></asp:Label>
                        </td>
                        <td style="width: 18%; height: 22px">
                            <asp:Label ID="lblImpDescuentoLinea" runat="server" Text="DImporte"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table id="Table1" style="border: 1px solid #E8E8E8; width: 100%; border-collapse: collapse;
                    margin-top: 2px;">
                    <tr class="border" style="background-color: #cccccc; height: 20px">
                        <td style="width: 1%; height: 20px; display: none;">
                            <asp:Label ID="lblDescuentoTotalLinea_hdd" runat="server" ClientIDMode="Static" Text="0"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px;">
                            <input type="checkbox" style="visibility: hidden;" />
                        </td>
                        <td style="width: 63%; height: 20px">
                            <asp:Label ID="lblTotDescuentoLinea" runat="server" Text="DTotal Descuentos Generales"></asp:Label>
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                        </td>
                        <td style="width: 1%; height: 20px; display: none;">
                        </td>
                        <td style="width: 15%; height: 22px;">
                        </td>
                        <td style="width: 18%; height: 22px;">
                            <asp:Label ID="lblDescuentoTotalLinea" runat="server" ClientIDMode="Static" Style="width: 97%;
                                border: none; text-align: right; display: block;"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="PlanesEntrega" class="Texto12 RectanguloEP" style="display: none; padding: 0.2em 0.3em; margin-bottom: 10px; overflow: auto;">
            <table style="border-collapse: collapse; width: 100%;">
                <tr class="FilaSeleccionada" style="height: 30px;">
                    <td style="width: 2%;">
                        <asp:Label ID="lblNumLineaPlanesEntrega" runat="server" CssClass="Etiqueta" Text="DLinea"></asp:Label>:
                    </td>
                    <td style="width: 98%;">
                        <asp:Label ID="lblLineaPlanesEntrega" runat="server" CssClass="Normal" style="margin-left:0.5em;" ClientIDMode="static"></asp:Label>
                    </td>
                </tr>
            </table>
            <hr style="width:100%;" />
			<div>
				<input type="checkbox" id="chkRecepcionAutomatica" disabled />
				<asp:Label runat="server" ID="lblRecepcionAutomatica" ClientIDMode="static"></asp:Label>				
			</div>
			<table id="tblPlanesEntrega" style="border-collapse:collapse; border-spacing:0.1em; padding:0em; margin-top:1em;">
                <thead>
				    <tr class="CabeceraBotones">
					    <th style="width:3em; height:2em;">

					    </th>
					    <th style="width:10em;">
						    <span id="lblCabeceraPlanEntregaFechaEntrega"></span>
					    </th>
					    <th style="width:12em;">
						    <span id="lblCabeceraPlanEntregaCantidadImporteEntrega"></span>
					    </th>
				    </tr>
                </thead>
                <tbody></tbody>
			</table>
		</div>
        <asp:HiddenField ID="hid_CodArticulo" runat="server" />
        <asp:HiddenField ID="hid_lineaPedido" runat="server" />
        <asp:HiddenField ID="hid_cantidad" runat="server" />
        <asp:HiddenField ID="hid_Precio" runat="server" />
        <asp:HiddenField ID="hid_TipoRecepcion" runat="server" />
        <asp:HiddenField ID="hid_IdOrden" runat="server" />

        <!-- Panel de confirmación de la emisión de un pedido -->
        <div id="pnlConfirmacionEmisionPedido" style="display: none; z-index: 1005; width:500px; height:400px; position: absolute;" class="popupCN">
            <div style="float:left; width:100%; margin:10px">
                <asp:Label ID="lblEmisionPedido" runat="server" CssClass="Rotulo" ClientIDMode="Static" Text="DEl pedido se ha emitido correctamente"></asp:Label>
            </div>
            <div style="float:left; width:100%;">
                <table style="border:none; width:100%">
                    <tr>
                        <th><asp:Label ID="lbllitPedidoCabecera" runat="server" Text="DPedido"></asp:Label></th>
                        <th><asp:Label ID="lbllitProveedorCabecera" runat="server" Text="DProveedor"></asp:Label></th>
                        <th><asp:Label ID="lbllitImporteCabecera" runat="server" Text="DImporte"></asp:Label></th>
                        <th><asp:Label ID="lbllitEstadoCabecera" runat="server" Text="DEstado"></asp:Label></th>
                    </tr>
                    <tr>
                        <td><a id="hypPedidoEmitido" class="Rotulo" ></a></td>
                        <td><asp:Label runat="server" ID="FSNLinkProveConfirmPedido" ClientIDMode="static" CssClass="ButtonClass"></asp:Label></td>
                        <td><asp:Label runat="server" ID="lblImporteConfirmPedido"></asp:Label></td>
                        <td><asp:Label runat="server" ID="lblEstadoConfirmPedido"></asp:Label></td>
                    </tr>
                </table>
            </div>
        </div> 
	
        <!-- Panel Info Proveedor -->
        <fsn:FSNPanelInfo ID="FSNPanelDatosProveCab" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx"
            ServiceMethod="Proveedor_DatosContacto" TipoDetalle="1" />
        <!-- Panel Info Peticionario -->
        <fsn:FSNPanelInfo ID="FSNPanelDatosPeticionario" runat="server" ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx"
            ServiceMethod="Obtener_DatosPersona" TipoDetalle="2" />
        <!-- Panel Detalle Artículo -->
        <asp:Button ID="btnMostrarDetalleArticulo" runat="server" EnableViewState="false"
            Style="display: none" />
        <asp:UpdatePanel ID="upDetalleArticulo" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnMostrarDetalleArticulo" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <fsep:DetArticuloPedido ID="pnlDetalleArticulo2" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <!--Panel detalle de artículo-->
        <fsep:DetArticuloCatalogo ID="pnlDetalleArticulo" runat="server" EnableViewState="true" />
        <!-- Panel Info Unidad -->
        <fsn:FSNPanelTooltip ID="FSNPanelDatosUnidad" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx"
            ServiceMethod="Unidad_Datos" EnableViewState="False" />
        <!--PANEL DE MOTIVO DE RECHAZO DE UNA LINEA DE PEDIDO -->
        <asp:Button runat="server" ID="btnHidden" Style="display: none;" />
        <asp:Panel ID="pnlMotivoRechazoLinea" runat="server" Width="450px" Style="background-color: White; display:none;">
            <div style="width: 100%; float: left; line-height: 12%; padding-top: 25px; padding-bottom: 15px;
                padding-left: 25px">
                <asp:Label runat="server" ID="lblCausaDenegacionLinea" Text="DIntroduzca la causa de la denegación si lo desea:"></asp:Label>
            </div>
            <div style="width: 100%; float: left; padding-top: 5px; padding-bottom: 5px; padding-left: 25px">
                <asp:TextBox ID="txtCausaDenegacionLinea" runat="server" Width="400px" Height="100px"
                    ClientIDMode="Static" TextMode="MultiLine"></asp:TextBox>
            </div>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 49%; text-align: right;">
                        <fsn:FSNButton ID="btnAceptarRechazoLinea" runat="server" Text="DAceptar" Alineacion="Right"
                            OnClientClick="btnAceptarRechazoLinea_Click(); return false;"></fsn:FSNButton>
                    </td>
                    <td style="width: 49%; text-align: left;">
                        <fsn:FSNButton ID="btnCancelarRechazoLinea" runat="server" Text="DCancelar" Alineacion="Left"></fsn:FSNButton>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <ajx:ModalPopupExtender ID="mpePanelRechazoLinea" runat="server" PopupControlID="pnlMotivoRechazoLinea"
            ClientIDMode="Static" TargetControlID="btnHidden" CancelControlID="btnCancelarRechazoLinea"
            BackgroundCssClass="modalBackground">
        </ajx:ModalPopupExtender>
    </div>

   <div id="divDevolverSiguientesEtapas" clientidmode="Static" style="display: none;  width:100%; height:100%; float:left" runat="server">
    <asp:UpdatePanel ID="upDevolverSiguientesEtapas" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
    <ContentTemplate>
         <div id="divPageHeader" style="position:relative; z-index:0;">
	        <div id="divPageHeaderBack" class="PageHeaderBackground"></div>
            <div id="divPageHeaderImagen" style="width:32px; height:30px; padding-left:10px; padding-right:5px; position:absolute; bottom:2px;">
	            <img src="" alt="" id="imgCabeceraConfirmacion" style="max-width:32px;max-height:30px;" runat="server" />
	        </div>
            <div id="divPageHeaderTitulo" style="float:left; margin-top:15px; white-space:nowrap; overflow:hidden;">
                <asp:label id="lblCabeceraConfirmacion" runat="server" ClientIDMode="Static" CssClass="LabelTituloCabeceraCustomControl"></asp:label>
	        </div>
        </div>

        <table id="tblGeneral" style="width:97%; float:left" border="0" runat="server">
        <tbody>
        <tr>
	        <td align="left"><asp:label id="lblEtapas" runat="server" CssClass="Etiqueta" Text="DEtapas"></asp:label></td>
        </tr>
        <tr>
	        <td>
                <table id="tblEtapas" style="border: 1px solid #E8E8E8; width: 100%;
                    border-collapse: collapse">
                </table>
            </td>
        </tr>
        <tr>
	        <td style="HEIGHT: 25px" valign="bottom" align="left"><asp:label id="lblRoles" runat="server" CssClass="Etiqueta" Text="DRoles" ClientIDMode="Static"></asp:label></td>
        </tr>
        <tr>
        <td>
            <table id="tblRoles" style="border: 1px solid #E8E8E8; width: 100%;
                border-collapse: collapse">
                <tr cellspacing="1" cellpadding="1" style="margin: 5px; padding: 5px;vertical-align:middle;">
                    <th class="Cabecera" style="text-align:left ; vertical-align:middle"><asp:label runat="server" Text="DRol" ID="lblRolTablaRoles"></asp:label></th>
                    <th class="Cabecera" style="text-align:left ; vertical-align:middle"><asp:label runat="server" Text="DUsuario/Departamento/Unid.Org" ID="lblUsuDepUonTablaRoles"></asp:label></th>
                    <th class="Cabecera" style="text-align:left ; vertical-align:middle; display:none"><span>${Per}</span></th>
                    <th class="Cabecera" style="text-align:left ; vertical-align:middle; display:none"><span>${Tipo}</span></th>
                </tr>
            </table>
        </td>
        </tr>
        <tr><td style="HEIGHT: 25px" valign="bottom" align="left"><asp:label id="lblComentarios" runat="server" Width="100%" CssClass="Etiqueta" Text="DComentarios para los proximos participantes"></asp:label></TD>
        </tr>
        <tr><td><textarea onkeypress="return validarLength(this,500)" id="txtComent" clientidmode="Static" onkeydown="textCounter(this,500)"
		        onkeyup="textCounter(this,500)" style="WIDTH: 100%; HEIGHT: 100px" name="txtComent" onchange="return validarLength(this,500)"
		        runat="server">
		        </textarea>
	        </td>
        </tr>
        <tr><td align="center">
            <table><tr>
            <td><fsn:FSNButton ID="btnConfirmarEnvio" runat="server" Alineacion="Right" Text="Confirmar envío" OnClientClick="return ConfirmarEnvio();"></fsn:FSNButton></td>
            <td><fsn:FSNButton ID="btnCancelarEnvio" runat="server" Alineacion="Left" Text="Cancelar" OnClientClick="return VolverAlDetalle()"></fsn:FSNButton></td>
            </tr></table>
        </td>
        </tr>
        </tbody>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
</div>

    <div id="pnlConfirm" class="popupCN" style="display: none; position: absolute; z-index: 1004; padding: 2em; max-width: 30em;">
        <div>
            <img alt="" title="" id="imgPnlConfirm" style="float: left; margin-right: 0.5em;"
                src="/images/icono_info.gif" />
            <span id="lblConfirm" style="text-align: justify;"></span><span id="lblChkConfirmError"
                class="TextoResaltado" style="visibility: hidden;"></span>
        </div>
        <div style="margin-top: 0.5em;">
            <textarea name="txtConfirm" id="txtConfirm" rows="4" cols="40" style="display: none;
                width: 100%;"></textarea>
        </div>
        <div style="margin-top: 0.5em;">
            <input type="checkbox" id="chkConfirm" style="display: none;" />
            <span id="lblChkConfirm" style="display: none;"></span>
        </div>
        <div id="divBotonesConfirm" style="position: relative; clear: both; float: left;
            width: 100%; margin-top: 15px; text-align: center;">
            <div id="btnAceptar" class="botonRedondeado" style="display: none; margin-right: 5px;">
                <span id="lblAceptar" style="line-height: 23px;"></span>
            </div>
            <div id="btnCancelar" class="botonRedondeado" style="margin-left: 5px;">
                <span id="lblCancelar" style="line-height: 23px;"></span>
            </div>
        </div>
    </div>
    <div id="pnlConfirmRecep" class="popupCN" style="display: none; position: absolute; z-index: 1004; padding: 2em; width: 300px;">
        <div>
            <img alt="" title="" id="imgPnlConfirmRecep" style="float: left; margin-right: 0.5em;" />
            <span id="lblConfirmRecep" style="text-align: justify;"></span>
        </div>
        <div style="margin-top: 0.5em;">
            <textarea name="txtConfirmRecep" id="txtConfirmRecep" rows="4" cols="40" style="display: none; width: 100%;"></textarea>
        </div>
        <div style="margin-top: 0.5em;">
            <input type="checkbox" id="chkConfirmRecep" style="display: none;" />
            <span id="lblChkConfirmRecep" style="display: none;"></span>
        </div>
        <div style="position: relative; clear: both; float: left; width: 100%; margin-top: 15px; text-align: center;">
            <div id="btnAceptarConfirmRecep" class="botonRedondeado" style="margin-right: 5px;">
                <span id="lblAceptarConfirmRecep" style="line-height: 23px;"></span>
            </div>
            <div id="btnCancelarConfirmRecep" class="botonRedondeado" style="margin-left: 5px;">
                <span id="lblCancelarConfirmRecep" style="line-height: 23px;"></span>
            </div>
        </div>
    </div>
    <!-- Panel Recepciones -->
    <asp:Button runat="server" ID="btnOcultoRecep" Style="display: none;" />
    <asp:Button runat="server" ID="btnOcultoAnularLinea" Style="display: none;" />
    <asp:Button runat="server" ID="btnOcultoAnularRecepcion" Style="display: none;" />
    <asp:Button runat="server" ID="btnBloquearFacturacionAlbaran" Style="display: none;" />
    <asp:Button runat="server" ID="btnDesbloquearFacturacionAlbaran" Style="display: none;" />
    <asp:Panel ID="pnlRecepciones" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left" Style="display: none;
        min-width: 450px; padding: 2px">
        <table width="100%">
            <tr>
                <td align="left">
                    <asp:UpdatePanel ID="updpnlPopupRecepCabecera" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblNumPedido" runat="server" Text="DNumero de Pedido" CssClass="Rotulo"
                                Style="margin-right: 60px;" />
                            <asp:Label ID="lblRefFactura" runat="server" Text="DReferencia en factura" CssClass="Rotulo" /><br />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="right" valign="top">
                    <asp:ImageButton ID="ImgBtnCerrarpnlRecep" runat="server" ImageUrl="~/images/Bt_Cerrar.png" />
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="updpnlPopupRecep" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnOcultoRecep" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnOcultoAnularLinea" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnOcultoAnularRecepcion" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnBloquearFacturacionAlbaran" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnDesbloquearFacturacionAlbaran" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            <asp:Panel ID="pnlRecepcion" runat="server" CssClass="Rectangulo" Style="padding: 8px;">
                                <asp:Table ID="tblCabeceraArticulo" runat="server" Style="border-collapse: collapse;
                                    min-width: 430px;" CellPadding="4">
                                    <asp:TableRow BackColor="#DCDCDC" Width="80%">
                                        <asp:TableHeaderCell HorizontalAlign="Left">
                                            <asp:Literal ID="litLinCodArticulo" runat="server"></asp:Literal></asp:TableHeaderCell>
                                        <asp:TableHeaderCell HorizontalAlign="Right">
                                            <asp:Literal ID="litLinCantidadTotalPedida" runat="server"></asp:Literal></asp:TableHeaderCell>
                                        <asp:TableHeaderCell HorizontalAlign="Right">
                                            <asp:Literal ID="litLinPrecUni" runat="server"></asp:Literal></asp:TableHeaderCell>
                                        <asp:TableHeaderCell HorizontalAlign="Right">
                                            <asp:Literal ID="litLinImporteTotalPedido" runat="server"></asp:Literal></asp:TableHeaderCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell VerticalAlign="Middle" Width="40%">
                                            <asp:Label ID="litItemCodArticulo" CssClass="Rotulo10Azul" runat="server" Text=""></asp:Label>
                                            &nbsp;
                                            <asp:Label ID="litItemDenArticulo" CssClass="Rotulo10Azul" runat="server" Text=""></asp:Label></asp:TableCell>
                                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right" Width="20%">
                                            <asp:Label ID="litItemCantidad" runat="server" Text=""></asp:Label>
                                            &nbsp;
                                            <fsn:FSNLinkTooltip ID="FSNlnkUniCantPdte" runat="server" Text="" CssClass="Rotulo10"
                                                Font-Underline="false" ContextKey="" PanelTooltip="pnlTooltipTexto"></fsn:FSNLinkTooltip>
                                        </asp:TableCell>
                                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right" Width="20%">
                                            <asp:Label ID="lblPrecUni" runat="server" Text=""></asp:Label>
                                            &nbsp;
                                            <asp:Label ID="lblMonPrecUni" runat="server" Text="" CssClass="Rotulo10"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right" Width="20%">
                                            <asp:Label ID="lblItemImportePed" runat="server" Text=""></asp:Label>
                                            &nbsp;
                                            <asp:Label ID="lblMonPrecImportePed" runat="server" Text="" CssClass="Rotulo10"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                                <br />
                                <asp:Label ID="lblError" runat="server" Text="" CssClass="Rotulo"></asp:Label>
                                <asp:Label ID="lblTituloRecep" runat="server" Text="DRecepciones" CssClass="Rotulo"></asp:Label>
                                <br />
                                <asp:DataList ID="dlLineasRecepciones" runat="server" ShowHeader="true" ExtractTemplateRows="True"
                                    Width="100%" CellPadding="4" CssClass="ListaPedidos">
                                    <ItemStyle CssClass="FilaImpar" />
                                    <AlternatingItemStyle CssClass="FilaPar" />
                                    <HeaderTemplate>
                                        <asp:Table ID="tblCabeceraLineasArticulo" runat="server">
                                            <asp:TableRow>
                                                <asp:TableHeaderCell HorizontalAlign="Left">
                                                    <asp:Literal ID="litLinFecha" runat="server"></asp:Literal></asp:TableHeaderCell>
                                                <asp:TableHeaderCell HorizontalAlign="Left">
                                                    <asp:Literal ID="litLinAlbaran" runat="server"></asp:Literal></asp:TableHeaderCell>
                                                <asp:TableHeaderCell HorizontalAlign="Left">
                                                    <asp:Literal ID="litLinUsuario" runat="server"></asp:Literal></asp:TableHeaderCell>
                                                <asp:TableHeaderCell HorizontalAlign="Right">
                                                    <asp:Literal ID="litLinCantidadRecibida" runat="server"></asp:Literal></asp:TableHeaderCell>
                                                <asp:TableHeaderCell HorizontalAlign="Right">
                                                    <asp:Literal ID="litLinCantidadRecibida_Edit" runat="server"></asp:Literal></asp:TableHeaderCell>
                                                <asp:TableHeaderCell HorizontalAlign="Right">
                                                    <asp:Literal ID="litLinImporteRecibido" runat="server"></asp:Literal></asp:TableHeaderCell>
                                                <asp:TableHeaderCell HorizontalAlign="Right">
                                                    <asp:Literal ID="litLinImporte" runat="server"></asp:Literal></asp:TableHeaderCell>
                                                <asp:TableHeaderCell HorizontalAlign="Right">
                                                    <asp:Literal ID="litLinObsRecep" runat="server"></asp:Literal></asp:TableHeaderCell>
                                                <asp:TableHeaderCell HorizontalAlign="Left">
                                                    <asp:Literal ID="litLinNumRecepERP" runat="server" Visible="false"></asp:Literal></asp:TableHeaderCell>
                                                <asp:TableHeaderCell HorizontalAlign="Left">
                                                    <asp:Literal ID="litLinAnularLinea" runat="server"></asp:Literal></asp:TableHeaderCell>
                                                <asp:TableHeaderCell HorizontalAlign="Left">
                                                    <asp:Literal ID="litLinAnularRecep" runat="server"></asp:Literal></asp:TableHeaderCell>
                                                <asp:TableHeaderCell HorizontalAlign="Left">
                                                    <asp:Literal ID="litModifLinea" runat="server"></asp:Literal></asp:TableHeaderCell>
                                                <asp:TableHeaderCell HorizontalAlign="Left"></asp:TableHeaderCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Table ID="tblItemLineasArticulo" runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Middle">
                                                    <asp:Label ID="litItemFecha" runat="server" Text=''></asp:Label></asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle">
                                                    <asp:Label ID="litItemAlbaran" runat="server" Text='<%# Eval("Albaran") %>'></asp:Label></asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle">
                                                    <fsn:FSNLinkInfo ID="fsnlnkItemUsuario" runat="server" Text='<%# Eval("RECEPTOR") %>'
                                                        ContextKey='<%# Eval("RECEPTOR") %>' PanelInfo="FSNPanelDatosPersona" PanelInfoZindex="110002"
                                                        PanelInfoUpdatePanel="upFSNPanelDatosPersona" CssClass="Rotulo" Style="text-decoration: none;"></fsn:FSNLinkInfo></asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right">
                                                    <asp:Label ID="litItemCantidadRecib" Visible="false" runat="server" Text='<%# Eval("CantRecep") %>'></asp:Label>
                                                    &nbsp;
                                                    <fsn:FSNLinkTooltip ID="FSNlnkUniCantPdte" Visible="false" runat="server" Text='<%# Eval("Unidad") %>'
                                                        CssClass="Rotulo10" Font-Underline="false" ContextKey='<%# Eval("DenUnidad") %>'
                                                        PanelTooltip="pnlTooltipTexto"></fsn:FSNLinkTooltip>
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right">
                                                    <igpck:WebNumericEditor ID="WNumEdCantidadRecibida" Value='<%# Eval("CantRecep") %>'
                                                        runat="server" Nullable="False" Width="65px" Visible="false" EnableViewState="false">
                                                        <Buttons CustomButtonDisplay="OnRight"></Buttons>
                                                        <ClientEvents ValueChanged="TextChange" />
                                                    </igpck:WebNumericEditor>
                                                    &nbsp;
                                                    <fsn:FSNLinkTooltip ID="FSNlnkUniCantPdte1" Visible="false" runat="server" Text='<%# Eval("Unidad") %>'
                                                        CssClass="Rotulo10" Font-Underline="false" ContextKey='<%# Eval("DenUnidad") %>'
                                                        PanelTooltip="pnlTooltipTexto"></fsn:FSNLinkTooltip>
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right">
                                                    <asp:Label ID="lblImporterecep" runat="server" Text='<%# Eval("Importerecep") %>'></asp:Label>
                                                    &nbsp;
                                                    <asp:Label ID="lblMonImporterecep" runat="server" Text='<%# Eval("Moneda") %>' CssClass="Rotulo10"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right">
                                                    <asp:Label ID="lblPrecUni" runat="server" Text='<%# Eval("Importe") %>'></asp:Label>
                                                    &nbsp;
                                                    <asp:Label ID="lblMonPrecUni" runat="server" Text='<%# Eval("Moneda") %>' CssClass="Rotulo10"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="30">
                                                    <fsn:FSNImageTooltip ID="btnFilaObservacionesRecep" runat="server" SkinID="Observaciones"
                                                        ContextKey='<%# Eval("Obs") %>' PanelTooltip="pnlTooltipTexto" /></asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle">
                                                    <asp:Label ID="litItemNumRecepERP" Visible="false" runat="server" Text='<%# Eval("NumRecepERP") %>'></asp:Label></asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                                    <fsn:FSNButton ID="btnAnularLineaRecep" runat="server" Visible="false"></fsn:FSNButton>
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                                    <fsn:FSNButton ID="btnAnulRecep" runat="server" Visible="false"></fsn:FSNButton>
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                                    <fsn:FSNButton ID="btnGuardarModifLinea" runat="server" Text="DModif" Visible="true"
                                                        CommandName="ModifLinea" CommandArgument='<%#Eval("LineaRecep")%>'></fsn:FSNButton>
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                                    <fsn:FSNButton ID="btnBloquearFacturacionAlbaran" runat="server" Visible="true" CommandArgument='<%#Eval("Albaran") & "###" & Eval("LineaPedido") & "###" & Eval("ORDEN")%>'></fsn:FSNButton>
                                                    <fsn:FSNLinkInfo ID="lnkBloquearFacturacionAlbaran" runat="server" PanelInfo="pnlInfoGenerico"
                                                        ContextKey="" CssClass="Rotulo" PanelInfoTipoDetalle="6" PanelInfoZindex="110002"
                                                        PanelInfoUpdatePanel="upPnlInfoGenerico">
                                                            <asp:Image ID="imgBloquearFacturacionAlbaran" runat="server" ImageUrl="~/Images/stopx40.png" />
                                                            <asp:Label id="lblBloquearFacturacionAlbaran" runat="server" Text="DAlbarán bloqueado para facturación"></asp:Label>
                                                    </fsn:FSNLinkInfo>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ItemTemplate>
                                </asp:DataList>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <fsn:FSNPanelTooltip ID="pnlTooltipRecep" runat="server" ServiceMethod="ComentarioRecep"
            Contenedor="pnlRecepciones">
        </fsn:FSNPanelTooltip>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpeRecepciones" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlRecepciones" TargetControlID="btnOcultoRecep" CancelControlID="ImgBtnCerrarpnlRecep"
        RepositionMode="None">
    </ajx:ModalPopupExtender>
    <!-- Panel Facturas -->
    <asp:Button runat="server" ID="btnOcultoFra" Style="display: none;" />
    <asp:Panel ID="pnlFacturas" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left" Style="display: none;
        min-width: 300px; padding: 2px">
        <table width="100%">
            <tr>
                <td align="left">
                    <asp:UpdatePanel ID="PanelTituloFacturasLin" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnOcultoFra" EventName="Click" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:Panel ID="pnlFacturasLin" runat="server">
                                <asp:Label ID="lblTituloFacturasLin" runat="server" Text="DFacturas vinculadas a la línea de pedido"
                                    CssClass="Rotulo" /><br />
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="right" valign="top">
                    <asp:ImageButton ID="ImgBtnCerrarpnlFacturas" runat="server" ImageUrl="~/images/Bt_Cerrar.png" />
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="updpnlPopupFacturas" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnOcultoFra" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            <asp:Panel ID="pnlFras" runat="server" CssClass="Rectangulo" Style="padding: 8px; min-width:290px">
                                <asp:Label ID="lblTituloFacturas" runat="server" Text="DFacturas" CssClass="Rotulo"></asp:Label>
                                <asp:GridView ID="grdFacturas" runat="server" ShowHeader="True" AutoGenerateColumns="False"
                                    CellPadding="4" GridLines="None" OnRowDataBound="grdFacturas_RowDataBound" Width="100%">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                            ShowHeader="True" ItemStyle-Wrap="False">
                                            <HeaderTemplate>
                                                <asp:Label ID="lblNumFra" runat="server" Text="DFra."></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Literal ID="litNumFra" runat="server" Text=''></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                            ShowHeader="True" ItemStyle-Wrap="False">
                                            <HeaderTemplate>
                                                <asp:Label ID="lblFechaFra" runat="server" Text="DFecha"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Literal ID="litFechaFra" runat="server" Text=''></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                            ShowHeader="True" ItemStyle-Wrap="False">
                                            <HeaderTemplate>
                                                <asp:Label ID="lblEstadoFra" runat="server" Text="DEmpresa"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Literal ID="litEstadoFra" runat="server" Text=''></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                                            ShowHeader="True" ItemStyle-Wrap="False">
                                            <HeaderTemplate>
                                                <asp:Label ID="lblImporteFra" runat="server" Text="DImporte"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Literal ID="litImporteFra" runat="server" Text=''></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle CssClass="FilaPar2" />
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpeFactura" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="ImgBtnCerrarpnlFacturas" PopupControlID="pnlFacturas" TargetControlID="btnOcultoFra">
    </ajx:ModalPopupExtender>
    <!-- Panel Pagos -->
    <asp:Button runat="server" ID="btnOcultoPagos" Style="display: none;" />
    <asp:Panel ID="pnlPagos" runat="server" BackColor="White" BorderColor="DimGray" BorderStyle="Solid"
        BorderWidth="1px" HorizontalAlign="Left" Style="display: none; min-width: 450px;
        padding: 2px" Width="450px">
        <table width="100%">
            <tr>
                <td align="left">
                    <asp:Panel ID="pnlPagosLin" runat="server">
                        <asp:Label ID="lblTituloPagosLin" runat="server" Text="DPagos vinculados a la línea de pedido"
                            CssClass="Rotulo" /><br />
                    </asp:Panel>
                </td>
                <td align="right" valign="top">
                    <asp:ImageButton ID="ImgBtnCerrarpnlPagos" runat="server" ImageUrl="~/images/Bt_Cerrar.png" />
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="updpnlPopupPagos" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnOcultoPagos" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            <asp:Panel ID="pnlPgo" runat="server" CssClass="Rectangulo" Width="370px" Style="padding: 8px;">
                                <asp:Label ID="lblTituloPagos" runat="server" Text="DPagos" CssClass="Rotulo"></asp:Label>
                                <asp:GridView ID="grdPagos" runat="server" ShowHeader="True" AutoGenerateColumns="False"
                                    CellPadding="4" GridLines="None" OnRowDataBound="grdPagos_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                            ShowHeader="True" ItemStyle-Wrap="False">
                                            <HeaderTemplate>
                                                <asp:Label ID="lblNumPago" runat="server" Text="DNúm. Pago"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Literal ID="litNumPago" runat="server" Text=''></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                            ShowHeader="True" ItemStyle-Wrap="False">
                                            <HeaderTemplate>
                                                <asp:Label ID="lblFechaPago" runat="server" Text="DFecha"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Literal ID="litFechaPago" runat="server" Text=''></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                            ShowHeader="True" ItemStyle-Wrap="False">
                                            <HeaderTemplate>
                                                <asp:Label ID="lblNumFra" runat="server" Text="DNúm. Fra."></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Literal ID="litNumFra" runat="server" Text=''></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                            ShowHeader="True" ItemStyle-Wrap="False">
                                            <HeaderTemplate>
                                                <asp:Label ID="lblEstadoPago" runat="server" Text="DLínea Fra."></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Literal ID="litEstadoPago" runat="server" Text=''></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle CssClass="FilaPar2" />
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpePagos" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="ImgBtnCerrarpnlPagos" PopupControlID="pnlPagos" TargetControlID="btnOcultoPagos">
    </ajx:ModalPopupExtender>

    <fsn:FSNPanelTooltip ID="pnlTooltipTexto" runat="server" ServiceMethod="DevolverTexto"></fsn:FSNPanelTooltip>
</asp:Content>
