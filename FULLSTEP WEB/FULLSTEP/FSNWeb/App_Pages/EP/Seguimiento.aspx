<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="Seguimiento.aspx.vb" Inherits="Fullstep.FSNWeb.Seguimiento" Async="True" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/App_Master/Menu.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">   
    <%--Panel Selecci�n centro de coste --%>
    <input id="btnCentrosOculto" type="button" value="button" runat="server" style="display: none" />
    <asp:Panel ID="pnlCentrosOculto" runat="server" Style="display: none">
    </asp:Panel>
    <asp:Panel ID="pnlCentros" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display: none;
        padding: 2px">
        <table width="450">
            <tr>
                <td align="left" style="line-height:30px;">
                    <img src="../../Images/centrocoste.JPG" style="padding-left:10px; vertical-align:middle" />
                    <asp:Label ID="lblCentros" runat="server" CssClass="RotuloGrande" style="padding-left:10px;">
                    </asp:Label>
                </td>
                <td align="right" valign="top">
                    <asp:ImageButton runat="server" ID="ImgBtnCerrarPanelCentros" ImageUrl="~/images/Bt_Cerrar.png" style="vertical-align:top;" OnClientClick="ocultarPanelModal('mpeCentros');" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblFiltroCentros" runat="server" CssClass="Rotulo" />
                    <input class="Normal" type="text" id="FiltroCentros" runat="server" style="margin-bottom:5px;width:320px;" />
                    <div id="listadoCentros" runat="server" style="height: 400px; overflow: auto;border-top:solid 1px #909090; clear: both;">
                    </div>
                </td>       
            </tr>
        </table>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpeCentros" runat="server" PopupControlID="pnlCentros"
        TargetControlID="btnCentrosOculto" CancelControlID="ImgBtnCerrarPanelCentros" RepositionMode="None"
        PopupDragHandleControlID="pnlCentrosOculto" ClientIDMode="Static">
    </ajx:ModalPopupExtender>
    <%--Panel Seleccion Partidas--%>
    <input id="btnPartidasOculto" type="button" value="button" runat="server" style="display: none" />
    <asp:Panel ID="pnlPartidasOculto" runat="server" Style="display: none">
    </asp:Panel>
    <asp:Panel ID="pnlPartidas" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display: none;
        padding: 2px">
        <table width="450">
            <tr>
                <td align="left">
                    <img src="../../Images/centrocoste.JPG" style="padding-left:10px; vertical-align:middle;" />
                    <asp:Label ID="lblPartidaPres0" runat="server" CssClass="RotuloGrande" style="padding-left:10px;">
                    </asp:Label>
                </td>
                <td align="right" valign="top">
                    <asp:ImageButton runat="server" ID="ImgBtnCerrarPanelPartida" ImageUrl="~/images/Bt_Cerrar.png" style="vertical-align:top;" OnClientClick="ocultarPanelModal('mpePartidas');" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="listadoInputsFiltro" runat="server">
                        <asp:Label ID="lblFiltroPartida" runat="server" CssClass="Rotulo" />
                    </div>
                    <div id="listadoPartidas" runat="server"></div>
                </td>       
            </tr>
        </table>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpePartidas" runat="server" PopupControlID="pnlPartidas"
        TargetControlID="btnPartidasOculto" CancelControlID="ImgBtnCerrarPanelPartida" RepositionMode="None"
        PopupDragHandleControlID="pnlPartidasOculto" ClientIDMode="Static">
    </ajx:ModalPopupExtender>
    <input type="hidden" id="collapseLineas" runat="server" value="" />
    <input type="hidden" id="OrdenesCargadas" runat="server" value="" />
    <!-- Panel Partida Presupuestaria (Contrato) -->
    <fsn:FSNPanelInfo ID="FSNPanelDatosPartida" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx" ServiceMethod="PartidaPresupuestaria_Datos" Height="320px" BackColor="White" />
    <!-- Panel Info Proveedor -->
    <fsn:FSNPanelInfo ID="FSNPanelDatosProve" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx" ServiceMethod="Proveedor_DatosContacto" TipoDetalle="1" />
    <!-- Panel Persona -->
    <asp:UpdatePanel ID="upFSNPanelDatosPersona" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
    <ContentTemplate>
        <fsn:FSNPanelInfo ID="FSNPanelDatosPersona" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx" ServiceMethod="Persona_Datos" />
    </ContentTemplate>
    </asp:UpdatePanel>
    <!-- Panel Info Receptor -->
    <fsn:FSNPanelInfo ID="FSNPanelDatosReceptor" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx" ServiceMethod="Persona_Datos" />
    <!-- Paneles Tooltip -->
    <fsn:FSNPanelTooltip ID="pnlTooltipObservaciones" runat="server" ServiceMethod="Observaciones">
    </fsn:FSNPanelTooltip>
    <fsn:FSNPanelTooltip ID="pnlTooltipObservacionesProv" runat="server" ServiceMethod="ObservacionesProv"></fsn:FSNPanelTooltip>
    <fsn:FSNPanelTooltip ID="pnlTooltipObservacionesLinea" runat="server" ServiceMethod="ObservacionesLinea"></fsn:FSNPanelTooltip>
    <fsn:FSNPanelTooltip ID="pnlTooltipTexto" runat="server" ServiceMethod="DevolverTexto"></fsn:FSNPanelTooltip>
    <fsn:FSNPanelTooltip ID="pnlTooltipComentarioLinea" runat="server" ServiceMethod="ComentarioLinea"></fsn:FSNPanelTooltip>
    <!-- Panel Detalle Art�culo -->
    <fsep:DetArticuloPedido ID="pnlDetalleArticulo" runat="server" />
    <!-- Panel Detalle Categoria -->
    <fsep:DetCategoria ID="pnlDetalleCategoria" runat="server" />
    <!-- Panel Info gen�rico -->
    <asp:UpdatePanel ID="upPnlInfoGenerico" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
    <ContentTemplate>
    <fsn:FSNPanelInfo ID="pnlInfoGenerico" runat="server" ServicePath="" ServiceMethod="DevolverTexto" Height="320px" BackColor="White" />
    </ContentTemplate>
    </asp:UpdatePanel>

    <!-- Panel Categor�as -->
    <input id="btnOcultoCat" type="button" value="button" runat="server" style="display: none" />
    <asp:Panel ID="PanelOculto" runat="server" Style="display: none">
    </asp:Panel>
    <asp:Panel ID="PanelArbol" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display: none;
        min-width: 350px; padding: 2px" onmouseout="FSNCerrarPanelesTemp()" onmouseover="FSNNoCerrarPaneles()">
        <table width="350">
            <tr>
                <td align="left">
                    <asp:Image ID="ImgCatalogoArbol" runat="server" ImageUrl="~/images/categorias_small.gif" />
                    &nbsp; &nbsp;
                    <asp:Label ID="LblCatalogoArbol" runat="server" CssClass="Rotulo">
                    </asp:Label>
                </td>
                <td align="right">
                    <asp:ImageButton runat="server" ID="ImgBtnCerrarPanelArbol" ImageUrl="~/images/Bt_Cerrar.png" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <asp:UpdatePanel ID="pnlCategorias" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:TreeView ID="fsnTvwCategorias" NodeIndent="20" CssClass="Normal" 
                                    ShowExpandCollapse="true" ExpandDepth="0" EnableViewState="True"
                                    runat="server" EnableClientScript="true" ForeColor="Black" PopulateNodesFromClient="true">
                                    <Nodes>
                                        <asp:TreeNode PopulateOnDemand="true" Value="_0_" SelectAction="None" Expanded="false"></asp:TreeNode>
                                    </Nodes>
                                </asp:TreeView>
                            </td>
                        </tr>
                    </table>
                    </ContentTemplate>
                </asp:UpdatePanel>  
                </td>       
            </tr>
        </table>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpePanelArbol" runat="server" PopupControlID="PanelArbol"
        TargetControlID="btnOcultoCat" CancelControlID="ImgBtnCerrarPanelArbol" RepositionMode="None"
        PopupDragHandleControlID="PanelOculto">
    </ajx:ModalPopupExtender>
    
    <!-- Panel Adjuntos -->
    <input id="btnOcultoAdj" type="button" value="button" runat="server" style="display: none" />
    <asp:Panel ID="pnlAdjuntos" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left" Style="display: none;
        min-width: 450px; padding: 2px" Width="450px">
        <table width="100%">
            <tr>
                <td align="left">
                    <asp:Panel ID="pnlAdjuntosLin" runat="server" Visible="false">
                        <asp:Label ID="lblTituloAdjuntosLin" runat="server" Text="DFicheros adjuntados a la l�nea de pedido"
                            CssClass="Rotulo" /><br />
                        <asp:Literal ID="litComentarioAdjuntosLin" runat="server" Text="DComentarios adjuntos" />
                    </asp:Panel>
                </td>
                <td align="right" valign="top">
                    <asp:ImageButton ID="ImgBtnCerrarpnlAdjuntos" runat="server" ImageUrl="~/images/Bt_Cerrar.png" />
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="updpnlPopupAdjuntos" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            <asp:Panel ID="pnlAdj" runat="server" CssClass="Rectangulo" Width="370px" 
                                style="padding:8px;">
                                <asp:Label ID="lblTituloAdjuntos" runat="server" Text="DArchivos adjuntos" CssClass="Rotulo"></asp:Label>
                                <asp:GridView ID="grdAdjuntos" runat="server" ShowHeader="False" 
                                    AutoGenerateColumns="False" CellPadding="4" GridLines="None">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False" ItemStyle-Wrap="False">
                                            <ItemTemplate>
                                                <asp:Literal ID="litFechaAdjunto" runat="server"></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="False" ItemStyle-Width="300px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnAdjunto" runat="server" CommandName="Descargar" CssClass="Normal" style="text-decoration:none;"></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle Width="300px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <fsn:FSNImageTooltip ID="imgComentarioAdjunto" runat="server" PanelTooltip="pnlTooltipComentarioAdj"
                                                    SkinID="ObservacionesPeq" ContextKey='<%# Container.DataItem("ID") & " " & Container.DataItem("TIPO") %>'/>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle CssClass="FilaPar2" />
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <fsn:FSNPanelTooltip ID="pnlTooltipComentarioAdj" runat="server" ServiceMethod="ComentarioAdj" Contenedor="pnlAdjuntos">
        </fsn:FSNPanelTooltip>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpeAdj" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="ImgBtnCerrarpnlAdjuntos" PopupControlID="pnlAdjuntos" TargetControlID="btnOcultoAdj">
    </ajx:ModalPopupExtender>
            
    <!-- Panel Albaranes -->
    <input id="btnOcultoAlbaranes" type="button" value="button" runat="server" style="display: none" />
    <asp:Panel ID="pnlAlbaranes" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left" Style="display: none;
        min-width: 850px; padding: 2px; overflow:auto;" Height="400px">
       <asp:UpdatePanel ID="updpnlPopupAlbaranesCab" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <table width="100%">
            <tr>
                <td align="left">
                 <asp:Image ID="ImageRecepAlbaranes" runat="server" ImageUrl="~/images/recepcion_small.jpg" />
                 <asp:Label  ID="lblTituloalbaranes" runat="server" VerticalAlign="Middle" CssClass="RotuloGrande" Height="51px" Text="DDetalle de recepciones del Pedido"></asp:Label>
                </td>
                <td align="right" valign="top">
                    <asp:ImageButton ID="ImgBtnCerrarpnlAlbaranes" runat="server" ImageUrl="~/images/Bt_Cerrar.png" />
                </td>
            </tr>
        </table>
        </ContentTemplate> 
        </asp:UpdatePanel> 
        <asp:UpdatePanel ID="updpnlPopupAlbaranes" runat="server" UpdateMode="Conditional">
         <ContentTemplate>
          <asp:DataList ID="dlAlbaranes" runat="server" Width="100%">
           <ItemTemplate>
            <table width="100%">
               <tr>      
                 <td align="left">
                    <asp:Panel ID="pnlCabAbaran" runat="server">
                        <asp:Table ID="tblCabAlbaran" runat="server">
                           <asp:TableRow>
                            <asp:TableCell>
                                <asp:Label ID="lblAlbaran" runat="server" Text="Albaran" CssClass="RotuloGrande"></asp:Label>
                                &nbsp;
                                <asp:Label ID="litalbaran" runat="server" CssClass="RotuloGrande" Text='<%# Eval("ALBARAN") %>'></asp:Label>
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Middle">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Image ID="ImageBloqueoAlbaran" runat="server"  Visible="false" ImageUrl="~/Images/stopx40.png" />
                                &nbsp;
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Label ID="lblBloqueoAlbaran" runat="server"  Visible="false"></asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>
                    </asp:Panel>
                 </td>
               </tr>
               <tr>
                <td>
                   <asp:Panel ID="pnlDatosAlbaran" runat="server" Width="100%">
                        <asp:Table ID="tblDatosAlabaran" runat="server" Width="100%">
                                <asp:TableRow>    
                                    <asp:TableCell>
                                        <asp:Label ID="litAlbFechaRecep" runat="server" CssClass="Rotulo">DFechaRecepcion</asp:Label>
                                        &nbsp;
                                        <asp:Literal ID="litFechaRecep" runat="server" Text='<%# Eval("FECHA_RECEPCION") %>' >DFechaRecep</asp:Literal>
                                    </asp:TableCell>
                                    <asp:TableCell runat="server" ID="tbcFechaContable" Visible="false">
                                        <asp:Label ID="litAlbFechaContable" runat="server" CssClass="Rotulo"></asp:Label>
                                        &nbsp;
                                        <asp:Literal ID="litFechaContable" runat="server" Text='<%# Eval("FEC_CONTABLE") %>'></asp:Literal>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="litAlbNumRecepErp" runat="server" CssClass="Rotulo" Visible="false">DNumeroRecepcionErp</asp:Label>
                                        &nbsp;
                                        <asp:Literal ID="litNumRecepErp" runat="server" Visible="false" Text='<%# Eval("NUM_ERP") %>' >DNumErp</asp:Literal>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="litAlbReceptor" runat="server" CssClass="Rotulo">DReceptor</asp:Label>
                                        &nbsp;
                                        <asp:Literal ID="litReceptor" runat="server" Text='<%# Eval("RECEPTOR") & " " & Eval("NOM") & " " & Eval("APE")%>'>DReceptor</asp:Literal>
                                    </asp:TableCell>                                           
                                    <asp:TableCell>
                                        <asp:Label ID="litAlbTotalImporte" runat="server" CssClass="Rotulo">DImporteTotalAlbaran</asp:Label>
                                        &nbsp;
                                        <asp:Literal ID="litTotalImporte" runat="server">DTmporte</asp:Literal>
                                        <asp:Label ID="litAlbMonTotalImporte" runat="server"  CssClass="Rotulo10">DMonImportealbaran</asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <br />
                            <asp:DataList ID="dlLineasAlbaran" runat="server" ShowHeader="true" ExtractTemplateRows="True" 
                                Width="100%" CellPadding="4"  CssClass="ListaPedidos" OnItemDataBound="dlLineasAlbaran_ItemDataBound" >
                                <ItemStyle CssClass="FilaImpar" />
                                <AlternatingItemStyle CssClass="FilaPar" />
                            <HeaderTemplate>
                                <asp:Table ID="tblCabeceraLineasAlbaran" runat="server" CellSpacing="10">
                                    <asp:TableRow BackColor="#DCDCDC">    
                                        <asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litLinArticulo" runat="server">DArticulo</asp:Literal></asp:TableHeaderCell>
                                        <asp:TableHeaderCell HorizontalAlign="Right"><asp:Literal ID="litLinCantidadPedida" runat="server">DCantidadPedida</asp:Literal></asp:TableHeaderCell>
                                        <asp:TableHeaderCell HorizontalAlign="Right"><asp:Literal ID="litLinImportePedido" runat="server">DImportePedido</asp:Literal></asp:TableHeaderCell>
                                        <asp:TableHeaderCell HorizontalAlign="Right"><asp:Literal ID="litLinCantidadRecibida" runat="server">DCantidadRecibida</asp:Literal></asp:TableHeaderCell>
                                        <asp:TableHeaderCell HorizontalAlign="Right"><asp:Literal ID="litLinPrecioUnitario" runat="server">DPrecioUnitario</asp:Literal></asp:TableHeaderCell>
                                        <asp:TableHeaderCell HorizontalAlign="Right"><asp:Literal ID="litLinImporterecibido" runat="server">DImporteRecibido</asp:Literal></asp:TableHeaderCell>
                                        <asp:TableHeaderCell HorizontalAlign="Right"><asp:Literal ID="litLinImporte" runat="server">DImporte</asp:Literal></asp:TableHeaderCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                    <asp:Table ID="tblItemLineasAlbaran" runat="server">
                                    <asp:TableRow>
                                            <asp:TableCell VerticalAlign="Middle" >
                                                <asp:label ID="litItemArticulo"  runat="server" Text='<%# IIf(Eval("DESCR_LIBRE") Is DBNull.Value,Eval("ART") & " - "& Eval("DESCR"),Eval("ART_INT") & " - "& Eval("DESCR_LIBRE"))%>'></asp:label>
                                            </asp:TableCell>
                                            <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right">
                                                <asp:label ID="litItemCantidadPedida"  runat="server" Text='<%# Eval("CANT_PED") %>'></asp:label>
                                                    &nbsp;
                                                <fsn:FSNLinkTooltip ID="FSNlnkUniCantPdte" runat="server" Text='<%# Eval("UNIDAD") %>' CssClass="Rotulo10"
                                                Font-Underline="false" ContextKey='<%# Eval("DENUNIDAD") %>' PanelTooltip="pnlTooltipTexto"></fsn:FSNLinkTooltip>
                                            </asp:TableCell>
                                            <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right">
                                                <asp:label ID="lblItemImportePedido"  runat="server" Text='<%# Eval("IMPORTE_PED") %>'></asp:label>
                                                <asp:Label ID="lblMonImportePedido" runat="server" CssClass="Rotulo10"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right"><asp:label ID="LitItemCantidadRecibida"  runat="server" Text='<%# Eval("CANT_REC") %>'></asp:label>
                                                    &nbsp;
                                                <fsn:FSNLinkTooltip ID="FSNLinkUniCantRec" runat="server" Text='<%# Eval("UNIDAD") %>' CssClass="Rotulo10"
                                                Font-Underline="false" ContextKey='<%# Eval("DENUNIDAD") %>' PanelTooltip="pnlTooltipTexto"></fsn:FSNLinkTooltip>
                                            </asp:TableCell>
                                            <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right">
                                                <asp:label ID="LisItemprecioUnitario"  runat="server" Text='<%# Eval("PRECIOUNITARIO") %>'></asp:label>
                                                <asp:Label ID="lblMonPrecUniLinea" runat="server" CssClass="Rotulo10"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right">
                                                <asp:label ID="lblItemImporteRecibido"  runat="server" Text='<%# Eval("IMPORTE_ARTICULO") %>'></asp:label>
                                                <asp:Label ID="lblMonImporteRecibido" runat="server" CssClass="Rotulo10"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right">
                                                <asp:label ID="ListItemImporteLinea"  runat="server" Text='<%# Eval("IMPORTE_ARTICULO") %>'></asp:label>
                                                <asp:Label ID="lblMonImporteLinea" runat="server" CssClass="Rotulo10"></asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>    
                            </ItemTemplate> 
                        </asp:DataList>
                    </asp:Panel> 
                 </td>
               </tr>
            </table>
            <br />
           </ItemTemplate>
         </asp:DataList>
        </ContentTemplate>
        </asp:UpdatePanel>
       </asp:Panel>
    <ajx:ModalPopupExtender ID="mpeAlbaranes" runat="server" BackgroundCssClass="modalBackground" PopupControlID="pnlAlbaranes"
        TargetControlID="btnOcultoAlbaranes" CancelControlID="ImgBtnCerrarpnlAlbaranes" RepositionMode="None">
    </ajx:ModalPopupExtender>    

    <!-- Panel Confirmaci�n Recepciones -->
    <input id="btnOcultoConfirmRecep" type="button" value="button" runat="server" style="display: none" />
    <asp:Panel ID="pnlConfirmRecep" runat="server" BackColor="White" BorderColor="DimGray"
            BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Center" Style="display: none;
            min-width: 300px; padding: 20px" Width="300px">
                <table cellpadding="0" cellspacing="7" border="0">
                    <tr><td colspan="2"><div style="float:left; display:inline; width:60px"><img alt="" title="" id="imgPnlConfirmRecep" runat="server" src="~/images/icono_info.gif" /></div><div style="float:left; display:inline; width:225px"><asp:Label ID="lblConfirmRecep" runat="server" Text="Label"></asp:Label></div></td></tr>
                    <tr><td colspan="2" align="left"><asp:TextBox ID="txtConfirmRecep" runat="server" Visible="false" TextMode="MultiLine" Rows="4" MaxLength="4000" Width="100%"></asp:TextBox></td></tr>
                    <tr><td colspan="2"><asp:CheckBox runat="server" ID="chkConfirmRecep" Visible="false" Text="DConfirmar Anulaci�n" /></td></tr>
                    <tr style="width:300px;">
                        <td runat="server" id="TdAceptConfRecep"  align="right" style="width:150px;"><fsn:FSNButton ID="btnAceptarConfirmRecep" runat="server" Text="DAceptar"></fsn:FSNButton></td>
                        <td align="left" id="TdCancelConfRecep" style="width:150px;"><fsn:FSNButton ID="btnCancelarConfirmRecep" runat="server" Text="DCancelar" Alineacion="Left"></fsn:FSNButton></td>
                    </tr>
                </table>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpeConfirmRecep" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlConfirmRecep" TargetControlID="btnOcultoConfirmRecep"></ajx:ModalPopupExtender>
    <!-- Cabecera -->
    <aspf:WebPartManager ID="WebPartManager1" runat="server">
    </aspf:WebPartManager>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td width="50%">
                <asp:Image ID="Image2" runat="server" Height="55px" ImageUrl="~/images/camion.jpg"
                    Style="margin-bottom: 0px" Width="70px" />
                <asp:Label ID="lblTitulo" runat="server" CssClass="RotuloGrande" Height="21px" Text="DSeguimiento de Pedidos"> 
                </asp:Label>
            </td>
            <td align="right" style="padding-right:20px;padding-top:5px;padding-bottom:5px;" width="20%">
                <fsn:FSEPWebPartBusqArticulos ID="FSEPWebPartBusqArticulos1" runat="server" AuthorizationFilter="EPAprovisionador" Height="65px" style="float:left;" CssClass="Rectangulo" />
            </td><td align="right" style="padding-left:20px;padding-right:20px;padding-top:5px;padding-bottom:5px;" width="30%">
                <fsn:FSEPWebPartCesta ID="FSEPWebPartCesta1" runat="server" Width="300px" AuthorizationFilter="EPAprovisionador" Height="65px" CssClass="Rectangulo" />
            </td>
        </tr>
    </table>

    <!-- Buscador -->
    <asp:UpdatePanel ID="pnlBuscador" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
    <asp:Panel ID="pnlCabeceraRecepcion" runat="server" SkinID="PanelColapsable" onClick="checkCollapseStatus();">
        <table cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td valign="middle" style="width:24px;"><img ID="imgExpandir" runat="server" src="../../images/expandir.gif" clientidmode="Static" /></td>
                <td valign="middle">
                    <asp:Label ID="lblCabecera" runat="server" Text="DCriterios de busqueda" Font-Bold="True" ForeColor="White"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlBusqueda" runat="server" DefaultButton="btnBuscar" style="display:none; overflow: hidden;">
        <asp:Table runat="server" ID="TablaPrimera" Width="95%">
            <asp:TableRow ID="Fila1Tabla1">
                <asp:TableCell Width="100">
                    <asp:Literal ID="litAnio" runat="server" Text="DA�o:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell Width="70">
                    <asp:DropDownList ID="ddlAnio" runat="server" DataTextField="Text" DataValueField="Value">
                    </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell Width="90">
                    <asp:Literal ID="litCesta" runat="server" Text="DN� de cesta:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell Width="92">
                    <asp:TextBox ID="txtCesta" runat="server" Width="90" onchange="validarsolonum(this)"
                        onkeypress="return validarkeynum()" onpaste="validarpastenum()"></asp:TextBox>
                    <ajx:TextBoxWatermarkExtender ID="txtCesta_TextBoxWatermarkExtender" runat="server"
                        Enabled="True" TargetControlID="txtCesta" WatermarkText="DN� de cesta">
                    </ajx:TextBoxWatermarkExtender>
                </asp:TableCell>
                <asp:TableCell Width="90">
                    <asp:Literal ID="litPedido" runat="server" Text="DN� de pedido:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell Width="92" HorizontalAlign="Right">
                    <asp:TextBox ID="txtPedido" runat="server" Width="90" onchange="validarsolonum(this)"
                        onkeypress="return validarkeynum()" onpaste="validarpastenum()"></asp:TextBox>
                    <ajx:TextBoxWatermarkExtender ID="txtPedido_TextBoxWatermarkExtender" runat="server"
                        Enabled="True" TargetControlID="txtPedido" WatermarkText="DN� de pedido">
                    </ajx:TextBoxWatermarkExtender>
                </asp:TableCell>
                <asp:TableCell Width="10">
                    <asp:Image ID="imgOculta1" runat="server" ImageUrl="~/images/trans.gif" Width="10" />
                </asp:TableCell>
                <asp:TableCell Width="120">
                    <asp:Literal ID="litPedidoProve" runat="server" Text="DN� de pedido proveedor:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell Width="107">
                    <asp:TextBox ID="txtPedidoProve" runat="server" Width="101"></asp:TextBox>
                    <ajx:TextBoxWatermarkExtender ID="txtPedidoProve_TextBoxWatermarkExtender" runat="server"
                        Enabled="True" TargetControlID="txtPedidoProve" WatermarkText="DN� de pedido"
                        WatermarkCssClass="WaterMark">
                    </ajx:TextBoxWatermarkExtender>
                </asp:TableCell>
                <asp:TableCell Width="90">
                    <asp:Literal ID="litPedidoERP" runat="server" Text="DN� de pedido ERP:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell Width="200">
                    <asp:TextBox ID="txtPedidoERP" runat="server" Width="101"></asp:TextBox>
                    <ajx:TextBoxWatermarkExtender ID="txtPedidoERP_TextBoxWatermarkExtender" runat="server"
                        Enabled="True" TargetControlID="txtPedidoERP" WatermarkText="DN� de pedido">
                    </ajx:TextBoxWatermarkExtender>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="Fila2Tabla1">
                <asp:TableCell Width="100">
                    <asp:Literal ID="litArticulo" runat="server" Text="DB�squeda por art�culo:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell ColumnSpan="5" HorizontalAlign="Right">
                    <asp:TextBox ID="txtArticulo" runat="server" Width="99%"></asp:TextBox>
                    <ajx:TextBoxWatermarkExtender ID="txtArticulo_TextBoxWatermarkExtender" runat="server"
                        Enabled="True" TargetControlID="txtArticulo" WatermarkText="DC�digo, Descripci�n, ...">
                    </ajx:TextBoxWatermarkExtender>
                    <ajx:AutoCompleteExtender ID="txtArticulo_AutoCompleteExtender" runat="server" CompletionInterval="500"
                        MinimumPrefixLength="3" ServiceMethod="AutoCompletar" TargetControlID="txtArticulo"
                        UseContextKey="True">
                    </ajx:AutoCompleteExtender>
                </asp:TableCell>
                <asp:TableCell Width="10">
                </asp:TableCell>
                <asp:TableCell Width="120">
                    <asp:Literal ID="litDesde" runat="server" Text="DDesde:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell Width="107">
                    <igpck:WebDatePicker runat="server" ID="dteDesde" Width="105" SkinID="Calendario">
                        <Buttons ButtonCssClass=>
                            <CustomButton  />
                        </Buttons>
                    </igpck:WebDatePicker>
                </asp:TableCell>
                <asp:TableCell Width="90">
                    <asp:Literal ID="litHasta" runat="server" Text="DHasta:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell Width="200">
                    <igpck:WebDatePicker runat="server" ID="dteHasta" Width="105" SkinID="Calendario">
                    </igpck:WebDatePicker>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="Fila3Tabla1">
                <asp:TableCell Width="100">
                    <asp:Literal ID="litEmpresa" runat="server" Text="DEmpresa:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell ColumnSpan="5" HorizontalAlign="Right">
                    <asp:DropDownList ID="ddlEmpresa" runat="server" DataTextField="Text" DataValueField="Value"
                        Width="100%">
                    </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell Width="10">
                </asp:TableCell>
                <asp:TableCell Width="120">
                    <asp:Literal ID="litReceptor" runat="server" Text="DReceptor:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell ColumnSpan="3" ID="CeldaReceptor" runat="server" Width="350" HorizontalAlign="Right"
                    Style="margin-right: 3">
                    <asp:DropDownList ID="ddlReceptor" runat="server" DataTextField="Text" DataValueField="Value"
                        Width="100%">
                    </asp:DropDownList>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="Fila4Tabla1">
                <asp:TableCell Width="100">
                    <asp:Literal ID="litProveedor" runat="server" Text="DProveedor:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell ColumnSpan="5" HorizontalAlign="Right">
                    <asp:TextBox ID="txtProveedor" runat="server" ClientIDMode="Static" Style="float: left;
                        width: 90%;" onkeydown="controlarBorrado(event, 'txtProveedor', 'hidProveedor')"></asp:TextBox>
                    <asp:HiddenField ID="hidProveedor" runat="server" ClientIDMode="Static" />
                    <img id="imgProveedor" style="float: right; cursor: pointer;" src="../../Images/abrir_ptos_susp.gif"
                        onclick="window.open('<%=ConfigurationManager.AppSettings("RutaFS") & "_common/BuscadorProveedores.aspx?Notificacion=1&desde=SeguimientoEP&IDCONTROL=" & txtProveedor.ClientID & "&HiddenFieldID=" & hidProveedor.ClientID%>', '_blank', 'width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes');return false;" />
                </asp:TableCell>
                <asp:TableCell Width="10">
                </asp:TableCell>
                <asp:TableCell Width="120">
                    <asp:Literal ID="litProvedorERP" runat="server" Text="DProveedor ERP:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell Width="107" ID="celdaProveedorERP" runat="server">
                    <asp:TextBox ID="txtProveedorERP" runat="server" Width="101"></asp:TextBox>
                    <ajx:TextBoxWatermarkExtender ID="txtProveedorERP_TextBoxWatermarkExtender" runat="server"
                        Enabled="True" TargetControlID="txtProveedorERP" WatermarkText="DProveedor ERP">
                    </ajx:TextBoxWatermarkExtender>
                </asp:TableCell>
                <asp:TableCell Width="90" ID="celdaLitGestor" runat="server">
                    <asp:Literal ID="LitGestor" runat="server" Text="DGestor:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell Width="200" ID="celdaDdlGestor" runat="server">
                    <asp:DropDownList ID="ddlGestor" runat="server" Width="100%">
                    </asp:DropDownList>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="Fila5Tabla1">
                <asp:TableCell Width="100">
                    <asp:Literal ID="litCentroCoste" runat="server" Text="DCentro de Coste:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell ColumnSpan="5" HorizontalAlign="Right">
                    <asp:TextBox ID="tbCtroCoste" runat="server" CssClass="Normal" Style="float: left;
                        width: 90%;" ClientIDMode="Static"></asp:TextBox>
                    <asp:HiddenField ID="tbCtroCoste_Hidden" runat="server" ClientIDMode="Static" />
                    <img src="../../Images/abrir_ptos_susp.gif" id="imgPanelCentros" runat="server" style="float: right;
                        cursor: pointer;" />
                </asp:TableCell>
                <asp:TableCell Width="10">
                </asp:TableCell>
                <asp:TableCell ColumnSpan="4">
                    <asp:PlaceHolder ID="phrPartidasPresBuscador" runat="server" ClientIDMode="Static">
                    </asp:PlaceHolder>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="Fila6Tabla1">
                <asp:TableCell Width="100">
                    <asp:Literal ID="litTipoPedido" runat="server" Text="DTipo de pedido:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell ColumnSpan="5" HorizontalAlign="Right">
                    <asp:DropDownList ID="ddlTipoPedido" runat="server" DataTextField="Text" DataValueField="Value"
                        Width="100%">
                    </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell>
                </asp:TableCell>
                <asp:TableCell ColumnSpan="4">
                    <asp:Literal ID="litNumMesesIni" runat="server" Text="DCargar Pedidos de " />
                    <igpck:WebNumericEditor ID="igNumMeses" runat="server" MinValue="1" Nullable="true" DataMode="Int" 
                         style="display:inline-table; width:4em;">
                        <Buttons SpinButtonsDisplay="OnRight" SpinOnArrowKeys="true" SpinOnReadOnly="true"
                            SpinWrap="true" UpperSpinButton-ImageUrl="../../Images/igte_upperHover.gif" LowerSpinButton-ImageUrl="../../Images/igte_lowerHover.gif"> 
                        </Buttons>
                    </igpck:WebNumericEditor>                    
                    <asp:Literal ID="litNumMesesFin" runat="server" Text="Dultimos meses" />
                </asp:TableCell></asp:TableRow></asp:Table><table border="0" cellpadding="0" cellpadding="0" style="margin:0px;padding:0px;" width="100%">
        <tr><td>
        <fieldset class="Checkboxes">
            <asp:CheckBoxList ID="chklstEstado" runat="server" RepeatDirection="Horizontal" AutoPostBack="False" RepeatColumns="5"></asp:CheckBoxList>
            <table border="0">
            <tbody><tr><td>
                <asp:CheckBox ID="chkBorrado" Visible="false" runat="server" Text="DBorrado" AutoPostBack="false" />
            </td></tr></tbody>
            </table>
        </fieldset>
                        </td></tr><tr><td>
        <fieldset class="Checkboxes">
            <asp:CheckBoxList ID="chklstOtrosFiltros" runat="server" RepeatDirection="Horizontal" AutoPostBack="False" RepeatColumns="5"></asp:CheckBoxList>
        </fieldset>
                        </td></tr><tr><td>
        <fieldset id="fldsetIMPCC" runat="server" class="Checkboxes">
            <asp:CheckBoxList ID="chkSeguimientoPedidos" runat="server" RepeatDirection="Horizontal" AutoPostBack="False" RepeatColumns="2" Width="500px">
            <asp:ListItem Text="dVer solo mis pedidos"></asp:ListItem><asp:ListItem Text="dVer todos los pedidos"></asp:ListItem></asp:CheckBoxList></fieldset> <fieldset id="fldsetPedidosBloqueados" runat="server" class="Checkboxes" visible="false">
            <table border="0"><%--Esta tabla se coloca para que los navegadores construyan este fieldset con la misa altura que el fieldset superior (fldsetIMPCC), que tiene un control Checkboxlist que asp.net renderiza como table--%>
            <tbody><tr><td>
                <asp:CheckBox ID="chkPedidosBloqueados" Visible="false" runat="server" Text="DVer pedidos con albaranes bloqueados" AutoPostBack="false" />
            </td></tr></tbody>
            </table>
        </fieldset>
                        </td></tr></table><table>
        <tr><td>
            <asp:Table ID="buscarFrasPagos" runat="server" HorizontalAlign="Left"  style="padding-right:20px">
                <asp:TableRow>
                    <asp:TableCell Width="100">
                    </asp:TableCell><asp:TableCell>
                        <asp:Literal runat="server" ID="litBusAnioFra" Text="DA�o Fra."></asp:Literal>
                    </asp:TableCell><asp:TableCell>
                        <asp:Literal runat="server" ID="litBusNumFra" Text="DN�m. Fra."></asp:Literal>
                    </asp:TableCell><asp:TableCell>
                        <asp:Literal runat="server" ID="litBusEstadoFra" Text="DEstado Fra."></asp:Literal>
                    </asp:TableCell><asp:TableCell>
                        <asp:Literal runat="server" ID="litBusFraFDesde" Text="DFecha Desde"></asp:Literal>
                    </asp:TableCell><asp:TableCell>
                        <asp:Literal runat="server" ID="litBusFraFHasta" Text="DFecha Hasta"></asp:Literal>
                    </asp:TableCell></asp:TableRow><asp:TableRow>
                    <asp:TableCell Width="100">
                        <asp:Literal runat="server" ID="litBusFras" Text="DFacturas"></asp:Literal>
                    </asp:TableCell><asp:TableCell>
                        <asp:TextBox ID="txtAnioFra" runat="server" Width="120"></asp:TextBox>
                        <ajx:TextBoxWatermarkExtender ID="txtAnioFra_TextBoxWatermarkExtender" runat="server" Enabled="True" TargetControlID="txtAnioFra" WatermarkText="DA�o Fra.">
                        </ajx:TextBoxWatermarkExtender>
                    </asp:TableCell><asp:TableCell>
                        <asp:TextBox ID="txtNumFra" runat="server" Width="120"></asp:TextBox>
                        <ajx:TextBoxWatermarkExtender ID="txtNumFra_TextBoxWatermarkExtender" runat="server" Enabled="True" TargetControlID="txtNumFra" WatermarkText="DNum Fra.">
                        </ajx:TextBoxWatermarkExtender>
                    </asp:TableCell><asp:TableCell>
                        <asp:DropDownList ID="ddlEstadoFra" runat="server" DataTextField="Text" DataValueField="Value" Width="120px"></asp:DropDownList>
                    </asp:TableCell><asp:TableCell>
                        <igpck:WebDatePicker runat="server" ID="dteFraFDesde" Width="120" SkinID="Calendario"></igpck:WebDatePicker>
                    </asp:TableCell><asp:TableCell>
                        <igpck:WebDatePicker runat="server" ID="dteFraFHasta" Width="120" SkinID="Calendario"></igpck:WebDatePicker>
                    </asp:TableCell></asp:TableRow><asp:TableRow>
                    <asp:TableCell Width="100">
                    </asp:TableCell><asp:TableCell>
                    </asp:TableCell><asp:TableCell>
                        <asp:Literal runat="server" ID="litBusNumPago" Text="DN�m. Pago"></asp:Literal>
                    </asp:TableCell><asp:TableCell>
                        <asp:Literal runat="server" ID="litBusEstadoPago" Text="DEstado Pago"></asp:Literal>
                    </asp:TableCell><asp:TableCell>
                        <asp:Literal runat="server" ID="litBusPagFDesde" Text="DFecha Desde"></asp:Literal>
                    </asp:TableCell><asp:TableCell>
                        <asp:Literal runat="server" ID="litBusPagFHasta" Text="DFecha Hasta"></asp:Literal>
                    </asp:TableCell></asp:TableRow><asp:TableRow>
                    <asp:TableCell Width="100">
                        <asp:Literal runat="server" ID="litBusPagos" Text="DPagos"></asp:Literal>
                    </asp:TableCell><asp:TableCell>
                    </asp:TableCell><asp:TableCell>
                        <asp:TextBox ID="txtNumPago" runat="server" Width="120"></asp:TextBox>
                        <ajx:TextBoxWatermarkExtender ID="txtNumPago_TextBoxWatermarkExtender" runat="server" Enabled="True" TargetControlID="txtNumPago" WatermarkText="DNum Pago">
                        </ajx:TextBoxWatermarkExtender>
                    </asp:TableCell><asp:TableCell>
                        <asp:DropDownList ID="ddlEstadoPago" runat="server" DataTextField="Text" DataValueField="Value" Width="120px"></asp:DropDownList>
                    </asp:TableCell><asp:TableCell>
                        <igpck:WebDateTimeEditor runat="server" ID="dtePagoFDesde" Width="120" SkinID="Calendario"></igpck:WebDateTimeEditor>
                    </asp:TableCell><asp:TableCell>
                        <igpck:WebDateTimeEditor runat="server" ID="dtePagoFHasta" Width="120" SkinID="Calendario"></igpck:WebDateTimeEditor>
                    </asp:TableCell></asp:TableRow></asp:Table></td><td ID="tdCategorias" runat="server" clientidmode ="Static" valign="middle" style="padding-top:5px" >
                <asp:UpdatePanel ID="updpnlCategorias" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                    <fsn:FSNLinkInfo ID="BtnCategoria" runat="server" CssClass="Rotulo"
                        style="text-decoration:none" PanelInfo="mpePanelArbol">
                        <asp:Literal ID="LnkBtnCategoria" runat="server"></asp:Literal>
                        &nbsp;<asp:Image ID="imgDespCategoria" runat="server" SkinID="desplegar" /></fsn:FSNLinkInfo> &nbsp; &nbsp; <asp:Literal ID="litCategoria" runat="server" Visible="False"></asp:Literal><asp:LinkButton ID="lnkQuitarCategoria" runat="server" Visible="False" Text="(x)" CssClass="Normal" style="text-decoration:none"></asp:LinkButton></ContentTemplate><Triggers>
                        <asp:AsyncPostBackTrigger ControlID="fsnTvwCategorias" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
            <td id="trPedidosAbiertos" runat="server" clientidmode ="Static">
                <asp:TableCell>
                    <asp:Label ID="litAnyoAbierto" runat="server" Text="DPedido Abierto" CssClass="Rotulo"></asp:Label>
                </asp:TableCell><asp:TableCell>
                    <asp:DropDownList ID="ddlAnyoAbierto" runat="server" DataTextField="Text" DataValueField="Value"
                        Width="100px" />
                </asp:TableCell><asp:TableCell>
                    <asp:TextBox ID="txtCestaAbierto" runat="server" Width="96" onchange="validarsolonum(this)"
                        onkeypress="return validarkeynum()" onpaste="validarpastenum()"></asp:TextBox>
                    <ajx:TextBoxWatermarkExtender ID="txtCestaAbierto_TextBoxWatermarkExtender" runat="server"
                        Enabled="True" TargetControlID="txtCestaAbierto" WatermarkText="DNº de cesta">
                    </ajx:TextBoxWatermarkExtender>
                </asp:TableCell><asp:TableCell>
                    <asp:TextBox ID="txtPedidoPedAbierto" runat="server" Width="100" onchange="validarsolonum(this)"
                        onkeypress="return validarkeynum()" onpaste="validarpastenum()"></asp:TextBox>
                    <ajx:TextBoxWatermarkExtender ID="txtPedidoPedAbierto_TextBoxWatermarkExtender" runat="server"
                        Enabled="True" TargetControlID="txtPedidoPedAbierto" WatermarkText="DNº de pedido"
                        WatermarkCssClass="WaterMark">
                    </ajx:TextBoxWatermarkExtender>
                </asp:TableCell></td></tr></table><table align="center">
        <tr><td>
        <fsn:FSNButton ID="btnBuscar" runat="server" Text="DBuscar" Alineacion="Right" style="float:none;" OnClientClick="Colapsar();"></fsn:FSNButton></td><td>
        <fsn:FSNButton ID="btnLimpiar" runat="server" Text="DLimpiar" Alineacion="Right" style="float:none;"></fsn:FSNButton></td></tr></table></asp:Panel></ContentTemplate></asp:UpdatePanel><asp:UpdatePanel ID="upListaFiltros" UpdateMode="Conditional" runat="server" ChildrenAsTriggers="false">
        <ContentTemplate>
            <div id="divListaFiltros" class="Cabecera_Seleccionada" runat="server" style="margin:5px">
                <asp:Label ID="lblListaFiltros" runat="server"></asp:Label></div></ContentTemplate></asp:UpdatePanel><!-- Lista Pedidos --><asp:UpdatePanel ID="pnlGrid" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
    <ContentTemplate>
    <asp:DataList ID="dlOrdenes" runat="server" ExtractTemplateRows="True" 
        Width="100%" CellPadding="4" CssClass="ListaPedidos">
        <ItemStyle CssClass="FilaImpar" />
        <AlternatingItemStyle CssClass="FilaPar" />
        <HeaderTemplate>
            <asp:Table ID="tblCabecera" runat="server">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell ColumnSpan="16">
                        <fsep:PaginadorBDD ID="pagdsCabecera" runat="server" PagSize="10" OnCambioOrden="Paginador_OnCambioOrden" OnExportar="Paginador_Exportar" onActualizarGrid="Paginador_ActualizarGrid" />
                    </asp:TableHeaderCell></asp:TableHeaderRow><asp:TableHeaderRow>
                    <asp:TableHeaderCell></asp:TableHeaderCell><asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabEmpresa" runat="server"></asp:Literal></asp:TableHeaderCell><asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabPedido" runat="server"></asp:Literal></asp:TableHeaderCell><asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabPedidoERP" runat="server"></asp:Literal></asp:TableHeaderCell><asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabPedidoProve" runat="server"></asp:Literal></asp:TableHeaderCell><asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabProveedor" runat="server"></asp:Literal></asp:TableHeaderCell><asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabProveedorERP" runat="server"></asp:Literal></asp:TableHeaderCell><asp:TableHeaderCell><asp:Literal ID="litCabImporte" runat="server"></asp:Literal></asp:TableHeaderCell><asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabTipoPedido" runat="server"></asp:Literal></asp:TableHeaderCell><asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabEstado" runat="server"></asp:Literal></asp:TableHeaderCell><asp:TableHeaderCell><asp:Literal ID="litCabObservacionesProv" runat="server"></asp:Literal></asp:TableHeaderCell><asp:TableHeaderCell><asp:Literal ID="LitCabRecep" runat="server"></asp:Literal></asp:TableHeaderCell><asp:TableHeaderCell></asp:TableHeaderCell><asp:TableHeaderCell></asp:TableHeaderCell></asp:TableHeaderRow></asp:Table></HeaderTemplate><HeaderStyle CssClass="FilaPar" />
        <FooterTemplate>
            <asp:Table ID="tblPie" runat="server">
                <asp:TableRow><asp:TableCell ColumnSpan="16">
                    <fsep:PaginadorBDD ID="pagdsPie" runat="server" PagSize="10" OnCambioOrden="Paginador_OnCambioOrden" OnImprimir="Paginador_Imprimir" OnExportar="Paginador_Exportar" onActualizarGrid="Paginador_ActualizarGrid" />
                </asp:TableCell></asp:TableRow></asp:Table></FooterTemplate><ItemTemplate>
            <asp:Table ID="tblItem" runat="server">
                <asp:TableRow>                    
                    <asp:TableCell VerticalAlign="Top"><asp:LinkButton ID="lnkVerDetalle" runat="server" CssClass="Link" style="text-decoration:none"></asp:LinkButton></asp:TableCell><asp:TableCell VerticalAlign="Top"><asp:Literal ID="litFilaEmpresa" runat="server" Text='<%# Eval("DENEMPRESA") %>'></asp:Literal></asp:TableCell><asp:TableCell VerticalAlign="Top"><asp:Literal ID="litFilaPedido" runat="server"></asp:Literal><br /><fsn:FSNLinkInfo ID="fsnlnkFilaPET" runat="server" Text='<%# Eval("APROV") %>' ContextKey='<%# Eval("APROV") %>' PanelInfo="FSNPanelDatosPersona" CssClass="Rotulo" style="text-decoration:none;"></fsn:FSNLinkInfo></asp:TableCell>
                    <asp:TableCell VerticalAlign="Top"><asp:Literal ID="litFilaPedidoERP" runat="server"></asp:Literal></asp:TableCell><asp:TableCell VerticalAlign="Top"><asp:Literal ID="litFilaPedidoProve" runat="server" Text='<%# Eval("NUMEXT") %>'></asp:Literal></asp:TableCell><asp:TableCell VerticalAlign="Top">
                        <fsn:FSNLinkInfo ID="fsnlnkFilaProveedor" runat="server" PanelInfo="FSNPanelDatosProve" ContextKey='<%# Eval("PROVECOD") %>' CssClass="Rotulo" style="text-decoration:none;"></fsn:FSNLinkInfo><br />
                        <div style="margin-top:5px">
                        <asp:Image ID="imgBloquearFacturacionAlbaranOrden" runat="server" ImageUrl="~/Images/stopx40.png" Visible="false" />
                        <asp:Label id="lblBloquearFacturacionAlbaranOrden" runat="server" Text="DAlbar�n bloqueado para facturaci�n" Visible="false"></asp:Label>
                        </div>
                    </asp:TableCell><asp:TableCell VerticalAlign="Top"><asp:Literal ID="litFilaProveedorERP" runat="server"></asp:Literal></asp:TableCell><asp:TableCell HorizontalAlign="Right" VerticalAlign="Top" ID="CeldaImporte" runat="server"><asp:table ID="Table1" runat="server"><asp:TableRow><asp:TableCell><asp:label ID="litFilaImporte" runat="server"></asp:label></asp:TableCell></asp:TableRow><asp:TableRow ID="FilaABONO" runat="server" Visible="false"><asp:TableCell><asp:label ID="litABONO" runat="server" Text="DABONO" ForeColor="White"></asp:label></asp:TableCell></asp:TableRow></asp:table></asp:TableCell><asp:TableCell VerticalAlign="Top"><asp:Label ID="lblFilaTipoPedido" runat="server" CssClass="Normal"></asp:Label></asp:TableCell><asp:TableCell VerticalAlign="Top"><asp:Label ID="lblFilaEstado" runat="server" CssClass="Normal"></asp:Label></asp:TableCell><asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="30"><fsn:FSNImageTooltip ID="btnFilaObservacionesProv" runat="server" SkinID="Observaciones" ContextKey='<%# Eval("ID") %>' PanelTooltip="pnlTooltipObservacionesProv" /></asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="30"><asp:ImageButton ID="btnFilarecep" runat="server" SkinID="Recepcion" CommandName="Recepciones" CommandArgument='<%# Eval("ID") %>' /></asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="30"><asp:ImageButton ID="btnFilaExcel" runat="server" SkinID="Excel" CommandName="Exportar" CommandArgument='<%# Eval("ID") %>' /></asp:TableCell>                    
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="30"><asp:ImageButton ID="btnFilaPDF" runat="server" SkinID="Pdf" CommandName="PDF" AlternateText="PDF"  /></asp:TableCell>
                </asp:TableRow>                
            </asp:Table>
        </ItemTemplate>
    </asp:DataList>
    <asp:HiddenField ID="hidIdCantidadRecepcionadaModificada" runat="server" />
    <asp:HiddenField ID="hidIdLblEstadoLinea" runat="server" />
    <asp:HiddenField ID="hidIdLblEstadoCabecera" runat="server" />
    <asp:Literal ID="litNoPedidos" runat="server" Visible="false" ></asp:Literal></ContentTemplate></asp:UpdatePanel><script type="text/javascript" language="javascript">  
        var xmlHttp;
        var mioEntryId;
        document.getElementById("ctl00_ctl00_CPH1_CPH4_chklstOtrosFiltros_1").onchange = function () {
            mostrarOcultarCategorias();
        };

        document.getElementById("ctl00_ctl00_CPH1_CPH4_chklstOtrosFiltros_2").onchange = function () {
            mostrarOcultarCategorias();
        };

        function mostrarOcultarCategorias() {
            if (document.getElementById("ctl00_ctl00_CPH1_CPH4_chklstOtrosFiltros_1").checked || document.getElementById("ctl00_ctl00_CPH1_CPH4_chklstOtrosFiltros_2").checked){
                $("#tdCategorias").show();
            }else{
                $("#tdCategorias").hide();
            }
        }

        document.getElementById("ctl00_ctl00_CPH1_CPH4_chklstOtrosFiltros_3").onchange = function () {
            $("#trPedidosAbiertos").toggle();
        };
        
        //<%'Funci�n que crea el objeto Ajax%>
        function CreateXmlHttp() {

            //<%' Probamos con IE%>
            try {
                //<%' Funcionar� para JavaScript 5.0%>
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (e) {
                try {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (oc) {
                    xmlHttp = null;
                }
            }

            //<%' Si no se trataba de un IE, probamos con esto%>
            if (!xmlHttp && typeof XMLHttpRequest != "undefined") {
                xmlHttp = new XMLHttpRequest();
            }

            return xmlHttp;
        }        

        function BorrarArraysOrdenesCargadasYAbiertas() {
            $get(hddOrdenesCargadasID).value = '';
        }

        /*<%
        ''' <summary>
        ''' Evento que salta al pinchar en un check de seguimiento de pedidos.
        ''' Si deseleccionamos ambos, nos marcar el ultimo que hemos seleccionado, no dejando deseleccionarlos todos
        ''' </summary>
        ''' <param name="indice">Nos indica que check hemos seleccionado</param>       %>
        */
        function ValidacionChecksSeguimiento(indice) {
            var chkBoxList = document.getElementById("<%=chkSeguimientoPedidos.clientID%>");
            if (chkBoxList) {
                var chkBoxCount = chkBoxList.getElementsByTagName("input");
                var cont = 0;
                for (var i = 0; i < chkBoxCount.length; i++) {
                    if (chkBoxCount[i].checked == true)
                        cont++;

                    if (i == indice)
                        objCheck = chkBoxCount[i]
                }
                if (cont == 0)
                    objCheck.checked = true;
            }
        };        
        /*<%-- 
        ''' Revisado por: blp. Fecha: 19/04/2012
        ''' Funci�n que elimina el valor de las fechas cuando se pone un n�mero de meses.
        --%>*/
        function igNumMeses_ValueChange(oEdit, oldValue, oEvent) {
            if (oEdit.text != '') {
                var odteDesde = igedit_getById('<%=dteDesde.clientID %>');
                if (odteDesde) {
                    odteDesde.setValue('');
                    odteDesde = null;
                }
                var odteHasta = igedit_getById('<%=dteHasta.clientID %>');
                if (odteHasta) {
                    odteHasta.setValue('');
                    odteHasta = null;
                }
            }
        };
        //<%--'lblListaFiltros y pnlBusqueda no pueden estar visibles al mismo tiempo --%>
        function checkCollapseStatus() {
            var oControlpnlBusqueda = $get("<%=pnlBusqueda.clientId %>");
            var oControlListaFiltros = $get("<%=divListaFiltros.clientID %>");
            var imgExpandir = $get("imgExpandir");
            if (oControlpnlBusqueda) {
                if (oControlpnlBusqueda.style.display == "none") {
                    oControlpnlBusqueda.style.display = "block";
                    imgExpandir.src = "../../Images/contraer.gif"
                    oControlListaFiltros.style.display = "none";
                } else {
                    oControlpnlBusqueda.style.display = "none";
                    imgExpandir.src = "../../Images/expandir.gif"
                    oControlListaFiltros.style.display = "block";
                }
            }
        }
        function Colapsar() {
            var opnlBusqueda = $get("<%=pnlBusqueda.clientID %>");
            if (opnlBusqueda)
                opnlBusqueda.style.display = "none";
        };
        function VerDetalle(idOrden,estado,aprobador,receptor,anyoPedido,numPedido,numOrden,acepProve) {
            //en el parametro desde indicamos el origen desde donde se accede al detalle del pedido. 1=seguimiento
            var vpu = ($('[id$=chkSeguimientoPedidos] input').first().prop('checked') == undefined ? true : $('[id$=chkSeguimientoPedidos] input').first().prop('checked'));
            var vpou = ($('[id$=chkSeguimientoPedidos] input').last().prop('checked') == undefined ? false : $('[id$=chkSeguimientoPedidos] input').last().prop('checked'));
            var oTotalRegistros = $('[id$=lblResulTot]')
            var TotalRegistros,PaginaActual;
            var oPaginaActual = $('[id$=ddlPage]');
            if (oPaginaActual.length>0) {
                TotalRegistros = oTotalRegistros.first().text();
                PaginaActual = parseInt(oPaginaActual.first().val())+1;
            } else {
                TotalRegistros = -1;
                PaginaActual = 1;
            }
            document.location.href = rutaFS + 'EP/detallePedido.aspx?desde=3&id=' + idOrden + '&estado=' + estado + '&aprobador=' + aprobador + '&receptor=' + receptor + '&vpu=' + vpu + '&vpou=' + vpou + '&anyo=' + anyoPedido + '&numped=' + numPedido + '&numorden=' + numOrden + '&PaginaPaginacion=' + PaginaActual + '&TotalRegistros=' + TotalRegistros + '&AcepProve=' + acepProve;
        };
        function VerDetalleExp(idOrden, estado, aprobador, receptor) {
            //en el parametro desde indicamos el origen desde donde se accede al detalle del pedido. 1=seguimiento
            var vpu = ($('[id$=chkSeguimientoPedidos] input').first().prop('checked') == undefined ? true : $('[id$=chkSeguimientoPedidos] input').first().prop('checked'));
            var vpou = ($('[id$=chkSeguimientoPedidos] input').last().prop('checked') == undefined ? false : $('[id$=chkSeguimientoPedidos] input').last().prop('checked'));
            //document.location.href = rutaFS + 'EP/detallePedidoExp.aspx?desde=3&id=' + idOrden + '&estado=' + estado + '&aprobador=' + aprobador + '&receptor=' + receptor + '&vpu=' + vpu + '&vpou=' + vpou;
            window.open(rutaFS + 'EP/DetallePedidoExp.aspx?desde=3&id=' + idOrden + '&estado=' + estado + '&aprobador=' + aprobador + '&receptor=' + receptor + '&vpu=' + vpu + '&vpou=' + vpou, '_blank', 'width=600,height=275,status=no,resizable=yes,top=200,left=200,menubar=yes')
        };
    </script><!-- Panel Cargando... --></asp:Content>