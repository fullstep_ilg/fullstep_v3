﻿var idRecept = "";
var bCargadoInicio = false;
var gEmisionPedido = null;
var gAtributos = null;
var gCostes = null;
var gImpuestos = null;
var gCostesLinea = null;
var gDescu = null;
var gDescuLinea = null;
var gDatosGenerales = null;
var gArchivos = null;
var oTipo = null;
var gLineas = null;
var Linea = null;
var gDatosDestinos = null;
var gDatosUnidades = null;
var sMoneda = "EUR";
var LineaUnidad = null;
var gbError = false;
var bHayIntegracionPedidos = false;
var bdevolverCodErp = false;
var bCambioEmpresa = false;
var gCentroContrato = null;
var gTipoLinea = null;
var bObliAlmacen = false;
var gCentroContratoComprobar;
var articulos;
var ModoEdicion = false;
var recep;
var popupListaExternaCargado = false;
var iFCAnt = 0;
var EsPedidoAbierto = false;
var ComprobacionesOK, gDatosCentros;
var NUMActual, NUMInicial;
var iFilaSeleccionada, iCodFilaSeleccionada;
$(document).ready(function () {
    if (CamposProveOpcionales.Campo1) $('#divCampoProveOpcional1').show();
    if (CamposProveOpcionales.Campo2) $('#divCampoProveOpcional2').show();
    if (CamposProveOpcionales.Campo3) $('#divCampoProveOpcional3').show();
    if (CamposProveOpcionales.Campo4) $('#divCampoProveOpcional4').show();

    if (btnModificarFavorito.visible) $('#btnModificarFavorito').show();
    else $('#btnModificarFavorito').hide();
    if (imgGuardarFavorito.visible) {
        $('#imgGuardarFavorito').attr('src', rutaTheme + '/images/favorito_add.gif');
        $('#imgGuardarFavorito').show();
    } else $('#imgGuardarFavorito').hide();
    if (btnEmitirPedido.visible) $('[id$=btnEmitirPedido]').show();
    else $('[id$=btnEmitirPedido]').hide();
    if (btnGuardarCesta.visible) $('[id$=btnGuardarCesta]').show();
    else $('[id$=btnGuardarCesta]').hide();
    if (txtNomFavorito.visible) $('#txtNomFavorito').show();

    $.get(ruta + '/App_Pages/EP/html/emisionpedidos.tmpl.htm', function (templates) {
        $('body').append(templates);
        $.get(ruta + '/App_Pages/EP/html/cabecera_emision_pedido.tmpl.htm', function (templates) {
            $('body').append(templates);
            cargarTextosPantalla();
            iFilaSeleccionada = 0;
            iCodFilaSeleccionada = 0;
            cargarDatosGenerales();
            CargarAtributosPedido(true);
            cargarLineas();
            calcularTotalesInicio();
            $.each(gLineas, function () {
                if (this.LineaCatalogo !== 0 && comprobarEmpresa(this.ID)) {
                    OcultarError();
                    quitarAvisoEnLinea();
                    $('[id^=aviso]').css("display", "none");
                };
                if ((bAccesoFSSM) && (bImputaPed)) {
                    var CentroContrato = this.Pres5_ImportesImputados_EP[0];
                    if (this.PluriAnual == false)
                        if (CentroContrato == undefined || CentroContrato.CentroCoste == undefined || CentroContrato.CentroCoste.CentroSM.Codigo == "") comprobarCentroUnico();
                }
            });
            //Mostramos la pantalla y ocultamos la imagen de cargando
            $('#contenidoPantalla').show();
            $('#pantallaCargando').hide();

            //Seleccionamos fila grid
            var grid = $find($('[id$=whdgArticulos]').attr('id'));
            if (grid !== undefined && grid !== null) {
                grid.get_element().focus();
                var gridView = grid.get_gridView();
                var activation = gridView.get_behaviors().get_activation();
                if (gridView.get_rows().get_length() > 0) activation.set_activeCell(gridView.get_rows().get_row(0).get_cell(0));
            };
        });
    });
});
$(document).mouseup(function (e) {
    var container = $('[id^=divInformacionIVA_]');
    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
        container.hide();
});
//#region Funciones de utilidadades
//Funcion que valida si es un numero valido y si lo es lo formatea
function validaFloatCantidadNo0(sender, old, idLinea) {
    var numDecimales = UsuNumberNumDecimals;
    $.each(gEmisionPedido.Lineas, function () {
        if (this.ID == idLinea) {
            if (this.NumDecUP !== null) {
                numDecimales = this.NumDecUP;
            }
        }
    });
    gModificadaDistribucionPanelProv = true
    var numero = sender.value;
    var exp
    if (UsuNumberDecimalSeparator == ',')
        exp = /^([0-9])*[.]?([0-9])*[,]?[0-9]*$/
    else
        exp = /^([0-9])*[,]?([0-9])*[.]?[0-9]*$/

    if (!exp.test(numero)) {
        alert(TextosJScript[0].toString().replace("XXXX", numero));
        sender.value = ''
        gBoolValidaFloat = false
        return false;
    } else {
        numero = strToNum(numero)

        if (numero == 0) {
            alert(TextosJScript[113].toString());
            sender.value = sender.defaultValue
            gBoolValidaFloat = false
            return false;
        }
        numero = formatNumberUnidad(numero, numDecimales)
        sender.value = numero
        sender.defaultValue = numero
        gBoolValidaFloat = true
        return true
    }
}
//Funcion que valida si es un numero valido y si lo es lo formatea
function validaFloat(sender, old) {
    gModificadaDistribucionPanelProv = true
    var numero = sender.value;
    var exp
    if (UsuNumberDecimalSeparator == ',')
        exp = /^([0-9])*[.]?([0-9])*[,]?[0-9]*$/
    else
        exp = /^([0-9])*[,]?([0-9])*[.]?[0-9]*$/

    if (!exp.test(numero)) {
        alert(TextosJScript[0].toString().replace("XXXX", numero));
        sender.value = ''
        gBoolValidaFloat = false
        return false;
    } else {
        numero = strToNum(numero)
        numero = formatNumber(numero)
        sender.value = numero
        gBoolValidaFloat = true
        return true
    }
}
function strToNum(strNum) {
    if (strNum == undefined || strNum == null || strNum === '')
        strNum = '0'

    while (strNum.indexOf(UsuNumberGroupSeparator) >= 0) {
        strNum = strNum.replace(UsuNumberGroupSeparator, "")
    }
    strNum = strNum.replace(UsuNumberDecimalSeparator, ".")
    return parseFloat(strNum)
}
function formatNumberUnidad(num, UsuNumberNumDecimalsUnidad) {
    if (num == undefined || num == null || num === '')
        num = 0
    num = parseFloat(num)
    var result = num.toFixed(UsuNumberNumDecimalsUnidad);
    result = addSeparadorMiles(result.toString())
    if (parseInt(UsuNumberNumDecimalsUnidad) !== 0) {
        var result1 = result.substring(0, result.length - parseInt(UsuNumberNumDecimalsUnidad) - 1)
        var result2 = result.substring(result.length - parseInt(UsuNumberNumDecimalsUnidad), result.length)
        result = result1 + UsuNumberDecimalSeparator + result2
    }
    return result
}
function formatNumber(num) {
    if (num == undefined || num == null || num === '')
        num = 0
    num = parseFloat(num)
    var result = num.toFixed(UsuNumberNumDecimals);
    result = addSeparadorMiles(result.toString())
    if (parseInt(UsuNumberNumDecimals) !== "0") {
        var result1 = result.substring(0, result.length - parseInt(UsuNumberNumDecimals) - 1)
        var result2 = result.substring(result.length - parseInt(UsuNumberNumDecimals), result.length)
        result = result1 + UsuNumberDecimalSeparator + result2
    }
    return result
}
function addSeparadorMiles(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + UsuNumberGroupSeparator + '$2');
    }
    var retorno = x1 + x2;
    return retorno
}
function validarFecha(value) {
    var validator = this;
    var datePat = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    var fechaCompleta = value.match(datePat);
    if (fechaCompleta == null) return false;
    return true;
}
function stopEvent(e) {
    if (!e) var e = window.event;

    //e.cancelBubble is supported by IE -
    // this will kill the bubbling process.
    e.cancelBubble = true;
    e.returnValue = false;

    //e.stopPropagation works only in Firefox.
    if (e.stopPropagation) e.stopPropagation();
    if (e.preventDefault) e.preventDefault();

    return false;
}
function JSText(Entrada) {
    Entrada = replaceAll(Entrada, "'", "\'");
    Entrada = replaceAll(Entrada, '"', "\'");

    return Entrada
}
/*''' <summary>
''' emitirPedido en FF no oculta la ventana de cargando.
''' </summary>      
''' <remarks>Llamada desde: emitirPedido ; Tiempo máximo: 0</remarks>*/
function OcultarCargando() {
    var modalprog = $find(ModalProgress);
    if (modalprog) modalprog.hide();
};
function replaceAll(sOriginal, sBuscar, sSustituir) {
    if (sOriginal == null) return null;

    var re = eval("/" + sBuscar + "/");
    var sTmp = "";
    while (sOriginal.search(re) >= 0) {
        sTmp += sOriginal.substr(0, sOriginal.search(re)) + sSustituir
        sOriginal = sOriginal.substr(sOriginal.search(re) + sBuscar.length, sOriginal.length)
    }
    sTmp += sOriginal;
    return sTmp;
}
function cambiarImgCopiar(boton) {
    $(boton).attr('src', '../../Images/aplicar_plus_activo.gif');
    setTimeout(function () { $(boton).attr('src', '../../Images/aplicar_plus.gif'); }, 1000);
};
//#endregion
//#region Eventos Click
$('#btnAceptarPedido').live('click', function () {
    if (EsPedidoAbierto) document.location.href = ruta + '/App_Pages/EP/PedidoAbierto.aspx';
    else document.location.href = ruta + '/App_Pages/EP/CatalogoWeb.aspx';
});
//Controlamos el div de _ep para que se muestre u oculte
$('#imgExpandir,#imgContraer').live('click', function () {
    $('#divImportesPedido').toggle();
    $('#imgExpandir').toggle();
    $('#imgContraer').toggle();
});
$('[id$=btnAceptarUnidad1]').click(function () {
    OcultarFondoPopUp();
    $('#pnlUnidad').hide();
    $('#mpepnlUnidad_backgroundElement').hide();
    var idLinea = LineaUnidad.ID;
    var sCodUnidad = $('#lblCodUnidadSrv').text();
    if (LineaUnidad.UP !== sCodUnidad) {
        var cantidad = parseFloat(strToNum($.trim($('#Cant' + idLinea).val())));
        var iPrecUc = parseFloat(strToNum($.trim($('#Prec' + idLinea).val())));
        // Dejo los valores de la unidad por defecto para poder trabajar con los factores de conversión
        // porque son respecto de esta unidad, si entro por primero vez le aplico el factor de conversión
        var iUnidadConversion = parseFloat(strToNum($.trim($('#lblFactorConversionSrv').text())));
        if ((iFCAnt == 0) && (iUnidadConversion == 1)) {
            iFCAnt = LineaUnidad.FC;
        } else if (iFCAnt == 0) {
            iFCAnt = 1;
        }
        cantidad = cantidad * iFCAnt;
        iPrecUc = iPrecUc / iFCAnt;
        iFCAnt = iUnidadConversion;

        var precioImpBruto = parseFloat(strToNum($.trim($('#lblImpBrutoLinea_hdd').text())));
        var precioLineaAnt = parseFloat(strToNum($.trim($('#lblImpBrutolin_' + idLinea + '_hdd').text())));
        var iCantMinima = parseFloat(strToNum($.trim($('#lblCantMinimaSrv').text())));
        var numDecimales = strToNum($.trim($('#lblNumDecimales_hdd').text()));

        var nPrecUc;
        var cantCambio;
        if (iUnidadConversion !== 0) {
            nPrecUc = iPrecUc * iUnidadConversion;
            cantCambio = cantidad / iUnidadConversion;
        } else {
            nPrecUc = iPrecUc;
            cantCambio = cantidad;
        }
        $('#Uni' + LineaUnidad.ID).text(JSText(sCodUnidad));
        $('#Prec' + LineaUnidad.ID).val(formatNumber(nPrecUc));
        $('#FC' + LineaUnidad.ID).val(iUnidadConversion);
        $('#PrecT' + LineaUnidad.ID).text(formatNumber(nPrecUc));
        $('#Prec' + LineaUnidad.ID + '_hdd').text(nPrecUc);

        if (numDecimales !== "null") {
            cantCambio = formatNumberUnidad(cantCambio, numDecimales);
        } else {
            cantCambio = formatNumber(cantCambio);
        }
        cantidad = strToNum(cantCambio);
        $('#Cant' + LineaUnidad.ID).val(cantCambio);
        $('#Cant' + LineaUnidad.ID + '_hdd').text(cantidad);

        var precio = cantidad * nPrecUc;
        precioImpBruto = precioImpBruto - precioLineaAnt + precio;

        $.each(gEmisionPedido.Lineas, function () {
            if (this.ID == idLinea) {
                this.FC = iUnidadConversion;
                this.UP = sCodUnidad;
                this.CantPed = cantidad;
                this.PrecUc = iPrecUc;
                if (numDecimales == "null") this.NumDecUP = null;
                else this.NumDecUP = numDecimales;
            }
        });
        $('#lblImpBruto').text(formatNumber(precioImpBruto) + " " + sMoneda);
        $('#lblImpBrutoLinea_hdd').text(precioImpBruto);
        $('#lblImpBruto_hdd').text(precioImpBruto);
        $('#lblImpBrutolin_' + idLinea + '_hdd').text(precio);
        $('#lblImpBrutolin_' + idLinea).text(formatNumber(precio));
        recalcularLineaCosteDescuento();
        $('#pesCostes').click();
        $('#pesDescuento').click();
        $('#pesDatosGenerales').click();

        calculoTotalCabecera();
    }
});
$('[id$=btnCancelarUnidad1]').click(function () {
    OcultarFondoPopUp();
    $('#pnlUnidad').hide();
    $('#mpepnlUnidad_backgroundElement').hide();
});
//#region Pestañas Cabecera
$('#pesDatosGenerales').live('click', function () {
    if ($('#pesOtrosDatos').hasClass("escenarioSeleccionadoEP")) guardarOtrosDatos();
    $('#pesArObs,#pesOtrosDatos,#pesCostes,#pesDescuentos').removeClass("escenarioSeleccionadoEP");
    $('#pesDatosGenerales').addClass("escenarioSeleccionadoEP");

    $('#DatosGenerales').css("display", "block");
    $('#ArchivosObser').hide();
    $('#OtrosDatos').hide();
    $('#Costes').hide();
    $('#Descuentos').hide();
});
$('#pesArObs').live('click', function () {
    if ($('#pesOtrosDatos').hasClass("escenarioSeleccionadoEP")) guardarOtrosDatos();
    $('#pesDatosGenerales,#pesOtrosDatos,#pesCostes,#pesDescuentos').removeClass("escenarioSeleccionadoEP");
    $('#pesArObs').addClass("escenarioSeleccionadoEP");

    $('#DatosGenerales').hide();
    $('#ArchivosObser').css("display", "block");
    $('#OtrosDatos').hide();
    $('#Costes').hide();
    $('#Descuentos').hide();

    if (gEmisionPedido == null && $("#textObser").val() == "") cargarObservaciones();
    cargarArchivos();
});
$('#pesOtrosDatos').live('click', function () {
    $('#pesDatosGenerales').removeClass("escenarioSeleccionadoEP");
    $('#pesArObs').removeClass("escenarioSeleccionadoEP");
    $('#pesOtrosDatos').addClass("escenarioSeleccionadoEP");
    $('#pesCostes').removeClass("escenarioSeleccionadoEP");
    $('#pesDescuentos').removeClass("escenarioSeleccionadoEP");

    $('#DatosGenerales').hide();
    $('#ArchivosObser').hide();
    $('#OtrosDatos').show();
    $('#Costes').hide();
    $('#Descuentos').hide();
    CargarAtributosPedido(true);
});
$('#pesCostes,#pesDescuentos').live('click', function () {
    if ($('#pesOtrosDatos').hasClass("escenarioSeleccionadoEP")) {
        guardarOtrosDatos();
    }
    $('#pesDatosGenerales').removeClass("escenarioSeleccionadoEP");
    $('#pesArObs').removeClass("escenarioSeleccionadoEP");
    $('#pesOtrosDatos').removeClass("escenarioSeleccionadoEP");
    $('#pesCostes').removeClass("escenarioSeleccionadoEP");
    $('#pesDescuentos').removeClass("escenarioSeleccionadoEP");

    $('#DatosGenerales').hide();
    $('#ArchivosObser').hide();
    $('#OtrosDatos').hide();
    if (this.id == 'pesCostes') {
        $('#pesCostes').addClass("escenarioSeleccionadoEP");
        $('#Costes').show();
        $('#Descuentos').hide();
        cargarCostes();
    } else {
        $('#pesDescuentos').addClass("escenarioSeleccionadoEP");
        $('#Costes').hide();
        $('#Descuentos').show();
        if (gDescu == null) CargarAtributosDescuento();
    }
});
//#endregion
//#region Pestañas Detalle
$('#pesResumenLineas').live('click', function () {
    if ($('#pesDatosLineas').hasClass("escenarioSeleccionadoEP")) guardarOtrosDatosLineas();
    if ($('#pesCostesLinea').hasClass("escenarioSeleccionadoEP")) guardarCostesLinea();
    if ($('#pesDescuentosLinea').hasClass("escenarioSeleccionadoEP")) guardarDescuentosLinea();
    $('#pesDatosLineas,#pesCostesLinea,#pesDescuentosLinea,#pesPlanesEntrega').removeClass("escenarioSeleccionadoEP");
    $('#pesResumenLineas').addClass("escenarioSeleccionadoEP");

    $('#ResumenLineas').css("display", "block");
    $('#DatosLinea,#CostesLinea,#DescuentosLinea,#PlanesEntrega').hide();
});
$('#pesDatosLineas').live('click', function () {
    MostrarCargando();
    setTimeout(function () {
        if ($('#pesCostesLinea').hasClass("escenarioSeleccionadoEP")) guardarCostesLinea();
        if ($('#pesDescuentosLinea').hasClass("escenarioSeleccionadoEP")) guardarDescuentosLinea();

        if (iCodFilaSeleccionada == 0) {
            alert(TextosJScript[1]);
            OcultarCargando();
        } else {
            cargarLineaPedido();
            if (Linea !== null) {
                $('#pesDatosLineas').addClass("escenarioSeleccionadoEP");
                $('#pesResumenLineas,#pesCostesLinea,#pesDescuentosLinea,#pesPlanesEntrega').removeClass("escenarioSeleccionadoEP");

                $('#ResumenLineas,#CostesLinea,#DescuentosLinea,#PlanesEntrega').hide();
                $('#DatosLinea').css("display", "block");
                //Almacenes            
                //Demo Berge 2017/16. Tras devoluciÃ³n quedamos en q Tipo Pedido Manda. Si Oblig-> Lo veras y Oblig. Luego NO emitira, pero ver lo ves.                
                var DebeEstar = false;
                if (gEmisionPedido.DatosGenerales.Tipo !== null) {
                    DebeEstar = (gEmisionPedido.DatosGenerales.Tipo.Almacenable == 1)
                }
                if ((Linea.TipoArticulo.TipoAlmacenamiento !== 0) || (DebeEstar)) {
                    var bAlmacenes = comprobarAlmacenes(Linea.Dest, Linea, Linea.CentroAprovisionamiento);
                    if (bAlmacenes == true) {
                        $('[id$=LblAlmacen]').show();
                        $('#cboAlmacen').show();
                        $('#BtnCopiarAlmacen').css("display", "inline-block");
                        if (bObliAlmacen == true) {
                            if ($('[id$=LblAlmacen]').text().indexOf("*") <= 0) $('[id$=LblAlmacen]').text("(*) " + TextosJScript[60] + " :");
                        } else {
                            if ($('[id$=LblAlmacen]').text().indexOf("*") > 0) $('[id$=LblAlmacen]').text(TextosJScript[60] + " :");
                        }
                        if (Linea.IdAlmacen != undefined || Linea.IdAlmacen != 0) { $('#cboAlmacen').fsCombo('selectValue', Linea.IdAlmacen); }
                    } else {
                        $('[id$=LblAlmacen],#cboAlmacen,#BtnCopiarAlmacen').hide();
                        Linea.IdAlmacen = 0;
                        Linea.strAlmacen = "";
                    }
                } else {
                    $('[id$=LblAlmacen],#cboAlmacen,#BtnCopiarAlmacen').hide();
                    Linea.IdAlmacen = 0;
                    Linea.strAlmacen = "";
                }
                cargarAtributosLinea();
                asignarCentroAprovisionamiento(Linea);
            }
        }

        var cacheCentro = {};
        $('#tbCtroCoste').autocomplete({
            minLength: 3,
            delay: 200,
            source: function (request, response) {
                var term = request.term;
                if (term in cacheCentro) {
                    if ((cacheCentro[term] != "null") || (cacheCentro[term] != "")) {
                        response($.map(cacheCentro[term], function (item) {
                            return {
                                label: item.CCValor,
                                value: item.CCHidden
                            }
                        }))
                    } else {
                        vaciarCentroCoste();
                    }

                } else {
                    var empresa = $("#CmbBoxEmpresa").val();
                    var origen = '&H7FFF';
                    var params = JSON.stringify({ origen: origen, verUON: sbVerUON, Empresa: empresa.toString(), textoFiltro: term });
                    $.ajax({
                        type: "POST",
                        url: ruta + '/App_Pages/_Common/App_Services/Consultas.asmx/DevolverCentrosPedidosEPCompl',
                        contentType: "application/json; charset=utf-8",
                        data: params,
                        dataType: "json",
                        success: function (data) {
                            if ((data.d != "null") && (data.d != "")) {
                                cacheCentro[term] = $.parseJSON(data.d);
                                response($.map(cacheCentro[term], function (item) {
                                    return {
                                        label: item.CCValor,
                                        value: item.CCHidden
                                    }
                                }))
                            } else {
                                vaciarCentroCoste();
                            }
                        }
                    });
                }
            },
            focus: function (event, ui) {
                $('#tbCtroCoste').val(ui.item.label);
                return false;
            },
            select: function (event, ui) {
                $('#tbCtroCoste_Hidden').val(ui.item.value);
                $('#tbCtroCoste').val(ui.item.label);
                if ($('#tbCtroCoste').val() == '') {
                    vaciarCentroCoste();
                }
                vaciarPartida();
                vaciarActivo();
                for (var i = 0; i < gLineas.length; i++) {
                    if (gLineas[i].ID == Linea.ID) {
                        ComprobarCentroContratoEscrito();
                        gCentroContrato.LineaId = Linea.ID;
                        CentroContrato = gCentroContrato;
                        Linea.Pres5_ImportesImputados_EP[0] = CentroContrato;
                        gLineas[i].Pres5_ImportesImputados_EP[0] = CentroContrato;
                    };
                }
                return false;
            }
        });

        var cachePartida = {};
        $('#tbPPres').autocomplete({
            minLength: 3,
            delay: 200,
            source: function (request, response) {
                var term = request.term;
                if (term in cachePartida) {
                    if ((cachePartida[term] != "null") && (cachePartida[term] != "")) {
                        response($.map(cachePartida[term], function (item) {
                            return {
                                label: item.PresValor,
                                value: item.PresHidden,
                                desc: item.CtroCod,
                                desc2: item.CtroDen
                            }
                        }))
                    } else {
                        vaciarPartida();
                    }
                } else {
                    var sData;
                    var IdCodCentro = "";
                    var IdCodEmpresa = 0;
                    IdCodCentro = $("#tbCtroCoste_Hidden").val();
                    IdCodEmpresa = $("#CmbBoxEmpresa").val();
                    var origen = '&H7FFF';
                    datos = origen + '@@@' + IdCodCentro + '@@@' + IdCodEmpresa + '@@@' + term;
                    sData = { datos: datos };
                    $.ajax({
                        type: "POST",
                        url: ruta + '/App_Pages/_Common/App_Services/Consultas.asmx/DevolverPartidasEPCompl',
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(sData),
                        dataType: "json",
                        success: function (data) {
                            if ((data.d != "null") && (data.d != "")) {
                                cachePartida[term] = $.parseJSON(data.d);
                                response($.map(cachePartida[term], function (item) {
                                    return {
                                        label: item.PresValor,
                                        value: item.PresHidden,
                                        desc: item.CtroCod,
                                        desc2: item.CtroDen
                                    }
                                }))
                            } else {
                                vaciarPartida();
                            }
                        }
                    });
                }
            },
            focus: function (event, ui) {
                $('#tbPPres').val(ui.item.label);
                return false;
            },
            select: function (event, ui) {
                $('#tbPPres_Hidden').val(ui.item.value);
                $('#tbPPres').val(ui.item.label);
                $('#tbCtroCoste_Hidden').val(ui.item.desc);
                $('#tbCtroCoste').val(ui.item.desc2);
                if ($('#tbPPres').val() !== "") {
                    for (var i = 0; i < gLineas.length; i++) {
                        if (gLineas[i].ID == Linea.ID) {
                            gCentroContrato = gLineas[i].Pres5_ImportesImputados_EP[0];
                            ComprobarContratoEscrito();
                            gCentroContrato.LineaId = parseInt(gLineas[i].ID.toString(), 10);
                            ComprobarCentroContrato(1);
                            CentroContrato = gCentroContrato;
                            Linea.Pres5_ImportesImputados_EP[0] = CentroContrato;
                            gLineas[i].Pres5_ImportesImputados_EP[0] = CentroContrato;
                            if ($('#tbPPres_Hidden').val() !== "") {
                                var partidas = $('#tbPPres_Hidden').val().replace(/@@/g, '#');
                                ComprobarGestorLinea(partidas);
                            }
                        };
                    }
                } else {
                    vaciarPartida();
                }
                return false;
            }
        });
    }, 1);
});
$('#pesCostesLinea').live('click', function () {
    if ($('#pesDatosLineas').hasClass("escenarioSeleccionadoEP")) guardarOtrosDatosLineas();
    if ($('#pesDescuentosLineas').hasClass("escenarioSeleccionadoEP")) guardarDescuentosLinea();
    if (iCodFilaSeleccionada == 0) {
        alert(TextosJScript[1]);
    } else {
        $('#pesDatosLineas,#pesResumenLineas,#pesDescuentosLinea,#pesPlanesEntrega').removeClass("escenarioSeleccionadoEP");
        $('#pesCostesLinea').addClass("escenarioSeleccionadoEP");

        $('#ResumenLineas,#DatosLinea,#DescuentosLinea,#PlanesEntrega').hide();
        $('#CostesLinea').css("display", "block");
        cargarCostesLinea();
    }
});
$('#pesDescuentosLinea').live('click', function () {
    if ($('#pesDatosLineas').hasClass("escenarioSeleccionadoEP")) guardarOtrosDatosLineas();
    if ($('#pesCostesLineas').hasClass("escenarioSeleccionadoEP")) guardarCostesLinea();
    if (iCodFilaSeleccionada == 0) {
        alert(TextosJScript[1]);
    } else {
        $('#pesDatosLineas,#pesResumenLineas,#pesCostesLinea,#pesPlanesEntrega').removeClass("escenarioSeleccionadoEP");
        $('#pesDescuentosLinea').addClass("escenarioSeleccionadoEP");

        $('#ResumenLineas,#DatosLinea,#CostesLinea,#PlanesEntrega').hide();
        $('#DescuentosLinea').css("display", "block");
        cargarDescuentosLinea();
    }
});
$('#pesPlanesEntrega').live('click', function () {
    if ($('#pesPlanesEntrega').hasClass("escenarioSeleccionadoEP")) return;
    if ($('#pesDatosLineas').hasClass("escenarioSeleccionadoEP")) guardarOtrosDatosLineas();
    if ($('#pesCostesLineas').hasClass("escenarioSeleccionadoEP")) guardarCostesLinea();
    if ($('#pesDescuentosLineas').hasClass("escenarioSeleccionadoEP")) guardarDescuentosLinea();
    if (iCodFilaSeleccionada == 0) {
        alert(TextosJScript[1]);
    } else {
        var lineaPlanEntrega = $.grep(gLineas, function (x) { return (x.ID == iCodFilaSeleccionada) })[0];
        $('#pesDatosLineas,#pesResumenLineas,#pesCostesLinea,#pesDescuentosLinea').removeClass("escenarioSeleccionadoEP");
        $('#pesPlanesEntrega').addClass("escenarioSeleccionadoEP");
        switch (gDatosGenerales.TipoRecepcionAutomatica) {
            case 0: //Sin especificar
            case 1: //Automatica 
                $('#chkRecepcionAutomatica').prop('checked', (gDatosGenerales.TipoRecepcionAutomatica == 1 ? true : lineaPlanEntrega.ConRecepcionAutomatica));
                $('#tblPlanesEntrega tbody').empty();
                $('#trPlanEntrega').tmpl(lineaPlanEntrega.PlanEntrega).appendTo('#tblPlanesEntrega tbody');
                $('[name=imgSelectedPlanEntrega]').attr('src', rutaTheme + '/images/row_collapsed.gif');
                $('[name=txtFechaPlanEntrega]').inputmask(UsuMask.replace('MM', 'mm')).datepicker({
                    showOn: 'both',
                    buttonImage: ruta + 'images/colorcalendar.png',
                    buttonImageOnly: true,
                    buttonText: '',
                    dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
                    showAnim: 'slideDown'
                });
                $('[name=txtCantidadImportePlanEntrega]').numeric({ decimal: '' + usuario.DecimalFmt + '', negative: false }).numericFormatted({
                    decimalSeparator: UsuNumberDecimalSeparator,
                    groupSeparator: UsuNumberGroupSeparator,
                    decimalDigits: UsuNumberNumDecimals
                });
                $('#lblCabeceraPlanEntregaCantidadImporteEntrega').text((lineaPlanEntrega.TipoRecepcion == 0 ? TextosJScript[99] + '(' + lineaPlanEntrega.UP + ')' : TextosJScript[100] + '(' + lineaPlanEntrega.Mon + ')'));
                var trPlanEntrega;
                $.each(lineaPlanEntrega.PlanEntrega, function (index) {
                    trPlanEntrega = $('#tblPlanesEntrega tbody tr:nth-child(' + (index + 1) + ')');
                    $(trPlanEntrega).find('[name=txtFechaPlanEntrega]').datepicker('setDate', new Date(this.FechaEntrega));
                    if (lineaPlanEntrega.TipoRecepcion == 0) //Cantidad
                        $(trPlanEntrega).find('[name=txtCantidadImportePlanEntrega]').numericFormatted('val', this.CantidadEntrega);
                    else //Importe
                        $(trPlanEntrega).find('[name=txtCantidadImportePlanEntrega]').numericFormatted('val', this.ImporteEntrega);
                });
                if (gDatosGenerales.TipoRecepcionAutomatica == 1) $('#chkRecepcionAutomatica').attr("disabled", true);
                if ($('#tblPlanesEntrega tbody tr').length == 0 || gLineas.length <= 1) $('#btnCopiarPlanEntrega').hide();
                else $('#btnCopiarPlanEntrega').show();
                break;
            case 2: //Manual
                $('#checkRecepcionAutomatica').hide();
                $('#lblCabeceraPlanEntregaCantidadImporteEntrega').text((lineaPlanEntrega.TipoRecepcion == 0 ? TextosJScript[99] + '(' + lineaPlanEntrega.UP + ')' : TextosJScript[100] + '(' + lineaPlanEntrega.Mon + ')'));
                if ($('#tblPlanesEntrega tbody tr').length == 0 || gLineas.length <= 1) $('#btnCopiarPlanEntrega').hide();
                else $('#btnCopiarPlanEntrega').show();
                break;
        }

        $('#ResumenLineas,#DatosLinea,#CostesLinea,#DescuentosLinea').hide();
        $('#PlanesEntrega').css("display", "block");
    }
});
//#endregion
$('#btnInfo').live('click', function () {
    var empresa = 0;
    if ($("#CmbBoxEmpresa").val() !== null) empresa = $("#CmbBoxEmpresa").val();

    varCodERP = 0;
    if ($("#CmbBoxProvesErp").val() !== null) CodERP = $("#CmbBoxProvesErp").val();

    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarProveedoresERP',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: "{'lEmpresa':'" + empresa + "','sProve':'" + gDatosGenerales.CodProve + "', 'sCodERP':'" + CodERP + "'}",
        async: true
    })).done(function (msg) {
        if ($.parseJSON(msg.d[1]).length !== 0) {
            resultado = $.parseJSON(msg.d[1])[0];

            $("[id$=IdCodigo]").html(resultado.Cod)
            $("[id$=IdDescripcion]").html(resultado.Den)
            $("[id$=IdCampo1]").html(resultado.Campo1)
            $("[id$=IdCampo2]").html(resultado.Campo2)
            $("[id$=IdCampo3]").html(resultado.Campo3)
            $("[id$=IdCampo4]").html(resultado.Campo4)
            $('#pnlInfoProveERP').show();
        }
    });
});
$('#BtnAcepDest').live('click', function () {
    if ($('#cboDest')[0].attributes.length > 1) {
        var idSeleccionado = $('#cboDest').fsCombo('getSelectedValue');
        var denSeleccionado = $('#cboDest').fsCombo('getSelectedText');
        Linea.Dest = idSeleccionado;
        Linea.DestDen = denSeleccionado;
        cargarReceptorInicio();
        var sCentro
        if (bHayIntegracionPedidos && gDatosGenerales.UsarOrgCompras) { sCentro = $('#cboCentroOrgCompra').val() };

        var tipoArticulo;
        $('#lblDestino').text(idSeleccionado + "-" + denSeleccionado);
        if (Linea.TipoArticulo.TipoAlmacenamiento !== 0) {
            var bAlmacenes = comprobarAlmacenes(idSeleccionado, Linea, sCentro);
            if (bAlmacenes == true) {
                $('[id$=LblAlmacen]').show();
                $('#cboAlmacen').show();
                $('#BtnCopiarAlmacen').css("display", "inline-block");
            } else {
                $('[id$=LblAlmacen]').hide();
                $('#BtnCopiarAlmacen').hide();
                $('#cboAlmacen').hide();

                Linea.IdAlmacen = 0;
                Linea.strAlmacen = "";
            }
        } else {
            $('[id$=LblAlmacen]').hide();
            $('#BtnCopiarAlmacen').hide();
            $('#cboAlmacen').hide();
            Linea.IdAlmacen = 0;
            Linea.strAlmacen = "";
        }

        var rowIndex = $.map(gLineas, function (x, index) { if (x.ID == Linea.ID) return index });
        gLineas[rowIndex] = Linea;
        gEmisionPedido.Lineas = gLineas;
    } else { $('#lblDestino').text(""); }

    $('#DatosDestinos').hide();
    $('#popupFondo').hide();
});
$('#CBObli').live('click', function () {
    if ($("#CBObli").is(':checked')) Linea.EntregaObl = 1;
    else Linea.EntregaObl = 0;

    var rowIndex = $.map(gLineas, function (x, index) { if (x.ID == Linea.ID) return index });
    gLineas[rowIndex] = Linea;
});
$('#btnCopiarPorcentajeDesvio').live('click', function () {
    gLineas = gEmisionPedido.Lineas;
    for (var i = 0; i < gLineas.length; i++) {
        gLineas[i].DesvioRecepcion = parseFloat($("#txtDesvioRecepcion").val());
    }
    gEmisionPedido.Lineas = gLineas;
    cambiarImgCopiar('#btnCopiarPorcentajeDesvio');
});
$('#BtnCopiarFechaEnt').live('click', function () {
    gLineas = gEmisionPedido.Lineas;
    for (var i = 0; i < gLineas.length; i++) {
        var fecha = $('[id^=txtFecEntSol]')[0].value;
        if (fecha == "") gLineas[i].FecEntrega = null;
        else gLineas[i].FecEntrega = $('[id^=txtFecEntSol]')[0].value;

        if ($("#CBObli").is(':checked')) gLineas[i].EntregaObl = 1;
        else gLineas[i].EntregaObl = 0;
    }
    gEmisionPedido.Lineas = gLineas;
    cambiarImgCopiar('#BtnCopiarFechaEnt');
});
$('#BtnCancDest').live('click', function () {
    $('#DatosDestinos').hide();
    $('#popupFondo').hide();
});
$('#imEmiPedLnkBtnCambiarDest').live('click', function () {
    var popup = $('#DatosDestinos');
    var sSeleccionado = "";

    if (gDatosDestinos == undefined) CargarDestinos();
    if (gDatosDestinos.length > 0) {
        var items = "{"
        var i = 0;

        $.each(gDatosDestinos, function () {
            if (Linea.Dest !== "") {
                if (this.Cod == Linea.Dest) {
                    sSeleccionado = Linea.Dest;
                    $('#LblCodDestSrv').text(this.Cod);
                    $('#LblDenDestSrv').text(this.Den);
                    $('#LblDirecDestSrv').text(this.Direccion);
                    $('#LblCpDestSrv').text(this.CP);
                    $('#LblPobDestSrv').text(this.Pob);
                    $('#LblProvDestSrv').text(this.Provi);
                    $('#LblPaisDestSrv').text(this.Pai);
                    $('#LblTfnoDestSrv').text(this.Tfno);
                    $('#LblFAXDestSrv').text(this.FAX);
                    $('#LblEmailDestSrv').text(this.Email);
                }
            } else {
                if (i == 0) {
                    sSeleccionado = this.Cod;
                    $('#LblCodDestSrv').text(this.Cod);
                    $('#LblDenDestSrv').text(this.Den);
                    $('#LblDirecDestSrv').text(this.Direccion);
                    $('#LblCpDestSrv').text(this.CP);
                    $('#LblPobDestSrv').text(this.Pob);
                    $('#LblProvDestSrv').text(this.Provi);
                    $('#LblPaisDestSrv').text(this.Pai);
                    $('#LblTfnoDestSrv').text(this.Tfno);
                    $('#LblFAXDestSrv').text(this.FAX);
                    $('#LblEmailDestSrv').text(this.Email);
                }
            }
            items += '"' + i + '": { "value":"' + this.Cod + '", "text": "' + this.Den + '" }, '
            i++;
        });
        items = items.substr(0, items.length - 2) //quitamos la ultima coma que sobra
        items += "}"

        var optionsDestino = { valueField: "value", textField: "text", adjustWidthToSpace: true, dropDownImageURL: 'Flecha.png', itemListWidth: 550, itemListMaxHeight: 100, autoComplete: true, data: $.parseJSON(items) };
        $('#cboDest').fsCombo(optionsDestino);

        $('#cboDest').fsCombo('selectValue', sSeleccionado);
        $('#cboDest').fsCombo('fuerzaDataLoaded');
        $('#fsComboValueDisplay_cboDest').css({ 'width': 550 });
        $('#cboDest').fsCombo('onSelectedValueChange', function () { cambioCombo() });
    };

    $('#popupFondo').css('height', $(document).height());
    $('#popupFondo').show();

    popup.css("width", "600");
    popup.css("height", "380");

    CentrarPopUp($('#DatosDestinos'));
    $('#DatosDestinos').show();
});
$('#imLnkBtnCambiarUnidad').live('click', function () {
    var popup = $('#pnlUnidad');
    var idLinea = this.alt;
    for (var i = 0; i < gLineas.length; i++) {
        if (gLineas[i].ID == idLinea) {
            LineaUnidad = gLineas[i];
            break
        }
    }
    var sLineaUP = JSText(LineaUnidad.UP);
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarPanelUnidades',
        data: "{'idLinea':'" + LineaUnidad.LineaCatalogo + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
    })).done(function (msg) {
        gDatosUnidades = $.parseJSON(msg.d);
        var items = "{"
        var i = 0;
        $.each(gDatosUnidades, function () {
            if (JSText(this.Codigo) == sLineaUP) {
                var sDesUPActual = $('#Uni' + idLinea).text();
                $('#lblCodUnidadSrv').text(JSText(this.Codigo));
                $('#lblCodUnidadSrvDesc').text(JSText(this.Codigo) + " - " + this.Denominacion);
                $('#lblUnidadCompraSrv').text(this.UnidadCompra);
                $('#lblUnidadCompraSrvDesc').text(this.UnidadCompra + " - " + this.DenUnidadCompra);
                $('#lblFactorConversionSrv').text(this.FactorConversion);
                $('#lblFactorConversionSrvDesc').text(JSText(this.Codigo) + " = " + this.FactorConversion + " " + this.UnidadCompra);
                $('#lblCantMinimaSrv').text(this.CantidadMinima);
                $('#lblCantMinimaSrvDesc').text(formatNumber(this.CantidadMinima))
                if (this.NumeroDeDecimales !== null) {
                    $('#lblNumDecimales_hdd').text(this.NumeroDeDecimales);
                } else {
                    $('#lblNumDecimales_hdd').text(UsuNumberNumDecimals);
                }
                $('#lblIdLineaUnidad').text(idLinea);
            }
            items += '"' + i + '": { "value":"' + JSText(this.Codigo) + '", "text": "' + this.Denominacion + '" }, '
            i++;
        });
        items = items.substr(0, items.length - 2) //quitamos la ultima coma que sobra
        items += "}"
        var optionsUnidades = { valueField: "value", textField: "text", adjustWidthToSpace: true, dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, autoComplete: false, data: $.parseJSON(items) };
        $('#cmbUnidades').fsCombo(optionsUnidades);
        $('#cmbUnidades').fsCombo('selectValue', LineaUnidad.UP);
        $('#fsComboValueDisplay_cmbUnidades').css({ 'width': 300 });
        $('#cmbUnidades').fsCombo('onSelectedValueChange', function (event) { cambioComboUnidad(event) });
    });

    $('#popupFondo').css('height', $(document).height());
    $('#popupFondo').show();

    popup.css("width", "400");
    popup.css("height", "300");
    CentrarPopUp($('#pnlUnidad'));
    $('#pnlUnidad').show();
});
$('#BtnCopiarDestinos').live('click', function () {
    gLineas = gEmisionPedido.Lineas;
    for (var i = 0; i < gLineas.length; i++) {
        gLineas[i].Dest = Linea.Dest;
        gLineas[i].DestDen = Linea.DestDen;

        gLineas[i].IdAlmacen = 0;
        gLineas[i].strAlmacen = "";
    }
    gEmisionPedido.Lineas = gLineas;
    cargarReceptorInicio();
    cambiarImgCopiar('#BtnCopiarDestinos');
});
$('#btnCopiarCentroOrgCompra').live('click', function () {
    gLineas = gEmisionPedido.Lineas;
    for (var i = 0; i < gLineas.length; i++) {
        gLineas[i].CentroAprovisionamiento = Linea.CentroAprovisionamiento;
        gLineas[i].CentroAprovisionamiento_Den = Linea.CentroAprovisionamiento_Den;
    }
    gEmisionPedido.Lineas = gLineas;
    cambiarImgCopiar('#btnCopiarCentroOrgCompra');
});
$('[id*=btnCopiarCentroCoste]').live('click', function () {
    gLineas = gEmisionPedido.Lineas;
    var id = $(this).attr('id');
    var iTipo = id.substr(id.length - 1);
    for (var i = 0; i < gLineas.length; i++) {
        switch (parseInt(iTipo)) {
            case 1:
                gLineas[i].Pres1 = Linea.Pres1;
                gLineas[i].Pres1Den = Linea.Pres1Den;
                break;
            case 2:
                gLineas[i].Pres2 = Linea.Pres2;
                gLineas[i].Pres2Den = Linea.Pres2Den;
                break;
            case 3:
                gLineas[i].Pres3 = Linea.Pres3;
                gLineas[i].Pres3Den = Linea.Pres3Den;
                break;
            default:
                gLineas[i].Pres4 = Linea.Pres4;
                gLineas[i].Pres4Den = Linea.Pres4Den;
                break;
        }
    }
    gEmisionPedido.Lineas = gLineas;
    cambiarImgCopiar('[id*=btnCopiarCentroCoste]');
});
$('#BtnCopiarCtroCoste').live('click', function () {
    gLineas = gEmisionPedido.Lineas;
    for (var i = 0; i < gLineas.length; i++) {
        if (gLineas[i].ID !== iCodFilaSeleccionada) {
            var CentroContrato = JSON.parse(JSON.stringify(Linea.Pres5_ImportesImputados_EP[0]));
            if (CentroContrato == undefined) {
                CentroContrato = {};
                CentroContrato.Id = "";
                CentroContrato.LineaId = "";
                CentroContrato.CentroCoste;
                CentroContrato.Partida;
                var CentroCoste = {};
                CentroCoste.UON1 = "";
                CentroCoste.UON2 = "";
                CentroCoste.UON3 = "";
                CentroCoste.UON4 = "";
                CentroCoste.CentroSM = "";
                var CentroSM = {};
                CentroSM.Denominacion = "";
                CentroSM.Codigo = "";
                var Partida = {};
                Partida.Denominacion = "";
                Partida.PresupuestoId = 0;
                Partida.NIV0 = "";
                Partida.NIV1 = "";
                Partida.NIV2 = "";
                Partida.NIV3 = "";
                Partida.NIV4 = "";
                Partida.FechaInicioPresupuesto = "";
                Partida.FechaFinPresupuesto = "";
            } else {
                if (CentroContrato.CentroCoste == undefined) {
                    CentroContrato = {};
                    CentroContrato.Id = "";
                    CentroContrato.LineaId = "";
                    CentroContrato.CentroCoste;
                    CentroContrato.Partida;
                    var CentroCoste = {};
                    CentroCoste.UON1 = "";
                    CentroCoste.UON2 = "";
                    CentroCoste.UON3 = "";
                    CentroCoste.UON4 = "";
                    CentroCoste.CentroSM = "";
                    var CentroSM = {};
                    CentroSM.Denominacion = "";
                    CentroSM.Codigo = "";
                    var Partida = {};
                    Partida.Denominacion = "";

                    Partida.PresupuestoId = 0;
                    Partida.NIV0 = "";
                    Partida.NIV1 = "";
                    Partida.NIV2 = "";
                    Partida.NIV3 = "";
                    Partida.NIV4 = "";
                    Partida.FechaInicioPresupuesto = "";
                    Partida.FechaFinPresupuesto = "";
                } else {
                    var CentroCoste = CentroContrato.CentroCoste;
                    var CentroSM = CentroCoste.CentroSM;
                    var Partida = CentroContrato.Partida;
                }
            }
            if ($('#tbCtroCoste').val() == "") {
                $('#tbCtroCoste_Hidden').val("");
                CentroContrato = {};
                CentroContrato.Id = "";
                CentroContrato.LineaId = "";
                CentroContrato.CentroCoste;
                CentroContrato.Partida;
                var CentroCoste = {};
                CentroCoste.UON1 = "";
                CentroCoste.UON2 = "";
                CentroCoste.UON3 = "";
                CentroCoste.UON4 = "";
                CentroCoste.CentroSM = "";
                var CentroSM = {};
                CentroSM.Denominacion = "";
                CentroSM.Codigo = "";
                var Partida = {};
                Partida.Denominacion = "";

                Partida.PresupuestoId = 0;
                Partida.NIV0 = "";
                Partida.NIV1 = "";
                Partida.NIV2 = "";
                Partida.NIV3 = "";
                Partida.NIV4 = "";
                Partida.FechaInicioPresupuesto = "";
                Partida.FechaFinPresupuesto = "";
                vaciarPartida();
            }

            CentroCoste.CentroSM = CentroSM;
            CentroContrato.CentroCoste = CentroCoste;
            CentroContrato.Partida = Partida;
            CentroContrato.LineaId = parseInt(gLineas[i].ID.toString(), 10);
            CentroContrato.Id = 1;

            gLineas[i].Pres5_ImportesImputados_EP[0] = CentroContrato;
            //Limpio el activo
            gLineas[i].Activo = 0
            gLineas[i].DescripcionActivo = ""
        }
        gEmisionPedido.Lineas = gLineas;
        cambiarImgCopiar('#BtnCopiarCtroCoste');
    }
});
$('#BtnCopiarPartidas').live('click', function () {
    gLineas = gEmisionPedido.Lineas;
    for (var i = 0; i < gLineas.length; i++) {
        var CentroContrato = JSON.parse(JSON.stringify(Linea.Pres5_ImportesImputados_EP[0]));
        if (CentroContrato == undefined) {
            CentroContrato = {};
            CentroContrato = CrearObjCentroPartidaVacio(CentroContrato);
            var PartidaBis = {};
            PartidaBis = CentroContrato.Partida;
        } else {
            if (CentroContrato.CentroCoste == undefined) {
                CentroContrato = {};
                CentroContrato = CrearObjCentroPartidaVacio(CentroContrato);
                var PartidaBis = {};
                PartidaBis = CentroContrato.Partida;
            }
            else {
                var CentroCoste = CentroContrato.CentroCoste;
                var CentroSM = CentroCoste.CentroSM;
                var PartidaBis = {};
                PartidaBis = CrearObjPartidaVacio(PartidaBis);
            }
        }
        if ($('#tbCtroCoste').val() !== "") {
            var gLineasCTO = gEmisionPedido.Lineas;
            for (var j = 0; j < gLineas.length; j++) {
                if (gLineas[j].ID == Linea.ID) {

                    CentroSM.Denominacion = gLineasCTO[j].Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM.Denominacion;
                    CentroSM.Codigo = gLineasCTO[j].Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM.Codigo
                    CentroCoste.UON1 = gLineasCTO[j].Pres5_ImportesImputados_EP[0].CentroCoste.UON1
                    CentroCoste.UON2 = gLineasCTO[j].Pres5_ImportesImputados_EP[0].CentroCoste.UON2
                    CentroCoste.UON3 = gLineasCTO[j].Pres5_ImportesImputados_EP[0].CentroCoste.UON3
                    CentroCoste.UON4 = gLineasCTO[j].Pres5_ImportesImputados_EP[0].CentroCoste.UON4
                    break;
                }
            }
        } else {
            $('#tbCtroCoste_Hidden').val("");
        }
        var PartidaBis;

        if (CentroContrato == undefined) {
            CentroContrato = gLineas[i].Pres5_ImportesImputados_EP;
            PartidaBis = {};
            PartidaBis = CrearObjPartidaVacio(PartidaBis);
        } else {
            if (CentroContrato.Partida == undefined) {
                PartidaBis = {};
                PartidaBis = CrearObjPartidaVacio(PartidaBis);
            } else PartidaBis = CentroContrato.Partida;
        }

        if ($('#tbPPres').val() == "") {
            $('#tbPPres_Hidden').val("");
            PartidaBis = CrearObjPartidaVacio(PartidaBis);
        }

        CentroCoste.CentroSM = CentroSM;
        CentroContrato.CentroCoste = CentroCoste;
        CentroContrato.Partida = PartidaBis;
        CentroContrato.LineaId = parseInt(gLineas[i].ID.toString(), 10);
        CentroContrato.Id = 1;
        gLineas[i].Pres5_ImportesImputados_EP[0] = CentroContrato;

    }
    gEmisionPedido.Lineas = gLineas;
    cambiarImgCopiar('#BtnCopiarPartidas');
});
$('[id^=chkNumAtribCoste_]').live('click', function () {
    var parts = this.id.split("_");
    var id = $.trim(parts[1]);

    if (this.checked) {
        $.each(gCostes, function () {
            if (this.Id == id) {
                var costeID = this.Id;
                this.Seleccionado = true;
                $.each(this.Impuestos, function () {
                    this.Seleccionado = true;
                    $('#trIVA_opc' + costeID + "_" + this.Id).css("display", "");
                });
            }
        });
        gEmisionPedido.Costes = gCostes;
        $('#inputNumAtribCosteOpc_' + id).css("display", "block");
        $('#inputNumAtribCosteOpc_' + id).css("text-align", "right");
        recalcularImportesCosteAnyadir($('#valNumAtribCosteOpc_' + id).text());
    } else {
        $.each(gCostes, function () {
            if (this.Id == id) {
                this.Seleccionado = false;
                var costeID = this.Id;
                $.each(this.Impuestos, function () {
                    this.Seleccionado = false;
                    $('#trIVA_opc' + costeID + "_" + this.Id).hide();
                });
            }
        });
        gEmisionPedido.Costes = gCostes;
        $('#inputNumAtribCosteOpc_' + id).hide();
        recalcularImportesCosteResta($('#valNumAtribCosteOpc_' + id).text());
    }
});
$('[id^=chkCosOblExc_]').live('click', function () {
    var parts = this.id.split("_");
    var idGrupo = $.trim(parts[1]);
    var id = $.trim(parts[2]);
    var restar = 0;
    if (this.checked) {
        var costeID;
        $.each($('[id^=inputNumAtribCosteOblExc_' + idGrupo + ']'), function () {
            var parts = this.id.split("_");
            var idAnt = $.trim(parts[2]);
            if ($('#inputNumAtribCosteOblExc_' + idGrupo + "_" + idAnt).css("display") == "block") {
                restar = $('#valNumAtribCosteOblExc_' + idGrupo + '_' + idAnt).text();
                $('[id^=trIVA_oblExc' + idAnt + ']').hide();
            }
        });

        $.each(gCostes, function () {
            var costeID = this.Id;
            if (this.GrupoCosteDescuento == idGrupo) {
                if (this.Id == id) {
                    this.Seleccionado = true;
                    $.each(this.Impuestos, function () {
                        this.Seleccionado = true;
                        $('[id^=trIVA_oblExc' + costeID + "_" + this.Id + ']').css("display", "");
                    });
                } else {
                    this.Seleccionado = false;
                    $.each(this.Impuestos, function () {
                        this.Seleccionado = false;
                    });
                }
            }
        });
        gEmisionPedido.Costes = gCostes;
        $('[id^=inputNumAtribCosteOblExc_' + idGrupo + ']').hide();
        $('#inputNumAtribCosteOblExc_' + idGrupo + '_' + id).css("display", "block");
        $('#inputNumAtribCosteOblExc_' + idGrupo + '_' + id).css("text-align", "right");
        recalcularImportesCosteAnyadir($('#valNumAtribCosteOblExc_' + idGrupo + '_' + id).text(), restar);
    } else {
        $('#inputNumAtribCosteOblExc_' + idGrupo).hide();
        $('#inputNumAtribCosteOblExc_' + idGrupo + '_' + id).css("display", "block");
    }
});
$('[id^=chkCosOpcExc_]').live('click', function () {
    var parts = this.id.split("_");
    var idGrupo = $.trim(parts[1]);
    var id = $.trim(parts[2]);
    var restar = 0;
    var restarIVA = 0;
    if (this.checked) {
        $.each($('[id^=inputNumAtribCosteOpcExc_' + idGrupo + ']'), function () {
            var parts = this.id.split("_");
            var idAnt = $.trim(parts[2]);
            if ($('#inputNumAtribCosteOpcExc_' + idGrupo + '_' + idAnt).css("display") == "block") {
                restar = $('#valNumAtribCosteOpcExc_' + idGrupo + '_' + idAnt).text();
                $('[id^=trIVA_oblOpc' + idAnt + ']').hide();
            }
        });

        $.each(gCostes, function () {
            var costeID = this.Id;
            if (this.GrupoCosteDescuento == idGrupo) {
                if (this.Id == id) {
                    this.Seleccionado = true;
                    $.each(this.Impuestos, function () {
                        this.Seleccionado = true;
                        $('[id^=trIVA_oblOpc' + costeID + "_" + this.Id + ']').css("display", "");
                    });
                } else {
                    this.Seleccionado = false;
                    $.each(this.Impuestos, function () {
                        this.Seleccionado = false;
                    });
                }
            }
        });
        gEmisionPedido.Costes = gCostes;
        $('[id^=inputNumAtribCosteOpcExc_' + idGrupo + ']').hide();
        $('#inputNumAtribCosteOpcExc_' + idGrupo + '_' + id).css("display", "block");
        $('#inputNumAtribCosteOpcExc_' + idGrupo + '_' + id).css("text-align", "right");
        recalcularImportesCosteAnyadir($('#valNumAtribCosteOpcExc_' + idGrupo + '_' + id).text(), restar);
    } else {
        $('#inputNumAtribCosteOpcExc_' + idGrupo).hide();
        $('#inputNumAtribCosteOpcExc_' + idGrupo + '_' + id).css("display", "block");
    }
});
$('#BtnCopiarAlmacen').live('click', function () {
    var idSeleccionado = $('#cboAlmacen').fsCombo('getSelectedValue');
    var denSeleccionado = $('#cboAlmacen').fsCombo('getSelectedText');

    gLineas = gEmisionPedido.Lineas;
    for (var i = 0; i < gLineas.length; i++) {
        gLineas[i].Dest = Linea.Dest;
        gLineas[i].DestDen = Linea.DestDen;
        if ((gLineas[i].TipoArticulo.TipoAlmacenamiento !== 0) && !(idSeleccionado == '')) {
            gLineas[i].IdAlmacen = idSeleccionado;
            gLineas[i].strAlmacen = denSeleccionado;
        } else {
            gLineas[i].IdAlmacen = 0;
            gLineas[i].strAlmacen = "";
        }
    };
    gEmisionPedido.Lineas = gLineas;

    cargarReceptorInicio();
    cambiarImgCopiar('#BtnCopiarAlmacen');
});
$(document).live('click', function (event) {
    $('[id^=divInformacionIVA_]').hide();
});
$('[id^=lblBase_]').live('click', function (event) {
    var parts = this.id.split("_");
    var id = $.trim(parts[1]);
    $("#divInformacionIVA_" + id).fadeToggle(1000);
    //event.stopPropagation();
    stopEvent(event);
});
$('[id^=chkNumAtribDescu_]').live('click', function () {
    var parts = this.id.split("_");
    var id = $.trim(parts[1]);

    if (this.checked) {
        $.each(gDescu, function () {
            if (this.Id == id) this.Seleccionado = true;
        });
        $('#inputNumAtribDescuOpc_' + id).css("display", "block");
        $('#inputNumAtribDescuOpc_' + id).css("text-align", "right");
        recalcularImportesDescuentoAnyadir($('#valNumAtribDescuOpc_' + id).text());
    } else {
        $.each(gDescu, function () {
            if (this.Id == id) this.Seleccionado = false;
        });
        $('#inputNumAtribDescuOpc_' + id).hide();
        recalcularImportesDescuentoResta($('#valNumAtribDescuOpc_' + id).text());
    }
});
$('[id^=chkDescuOblExc_]').live('click', function () {
    var parts = this.id.split("_");
    var idGrupo = $.trim(parts[1]);
    var id = $.trim(parts[2]);
    var restar = 0;

    if (this.checked) {
        $.each(gDescu, function () {
            if (this.GrupoCosteDescuento == idGrupo) {
                if (this.Id == id) this.Seleccionado = true;
                else this.Seleccionado = false;
            }
        });
        $.each($('[id^=inputNumAtribDescuExc_' + idGrupo + ']'), function () {
            var parts = this.id.split("_");
            var idAnt = $.trim(parts[2]);
            if ($('#inputNumAtribDescuExc_' + idGrupo + '_' + idAnt).css("display") == "block")
                restar = $('#valNumAtribDescuExc_' + idGrupo + '_' + idAnt).text();
        });
        $('[id^=inputNumAtribDescuExc_' + idGrupo + ']').hide();
        $('#inputNumAtribDescuExc_' + idGrupo + '_' + id).css("display", "block");
        $('#inputNumAtribDescuExc_' + idGrupo + '_' + id).css("text-align", "right");
        recalcularImportesDescuentoAnyadir($('#valNumAtribDescuExc_' + idGrupo + '_' + id).text(), restar);
    } else {
        $('#inputNumAtribDescuExc_' + idGrupo).hide();
        $('#inputNumAtribDescuExc_' + idGrupo + '_' + id).css("display", "block");
    }
});
$('[id^=chkDescuOpcExc_]').live('click', function () {
    var parts = this.id.split("_");
    var idGrupo = $.trim(parts[1]);
    var id = $.trim(parts[2]);
    var restar = 0;
    if (this.checked) {
        $.each(gDescu, function () {
            if (this.GrupoCosteDescuento == idGrupo) {
                if (this.Id == id) this.Seleccionado = true;
                else this.Seleccionado = false;
            }
        });
        $.each($('[id^=inputNumAtribDescuOpcExc_' + idGrupo + ']'), function () {
            var parts = this.id.split("_");
            var idAnt = $.trim(parts[2]);
            if ($('#inputNumAtribDescuOpcExc_' + idGrupo + '_' + idAnt).css("display") == "block")
                restar = $('#valNumAtribDescuOpcExc_' + idGrupo + '_' + idAnt).text();
        });
        $('[id^=inputNumAtribDescuOpcExc_' + idGrupo + ']').hide();
        $('#inputNumAtribDescuOpcExc_' + idGrupo + '_' + id).css("display", "block");
        $('#inputNumAtribDescuOpcExc_' + idGrupo + '_' + id).css("text-align", "right");
        recalcularImportesDescuentoAnyadir($('#valNumAtribDescuOpcExc_' + idGrupo + '_' + id).text(), restar);
    } else {
        $('#inputNumAtribDescuOpcExc_' + idGrupo).hide();
        $('#inputNumAtribDescuOpcExc_' + idGrupo + '_' + id).css("display", "block");
    }
});
$('[id^=chkNumAtribCosteLinea_]').live('click', function () {
    var parts = this.id.split("_");
    var id = $.trim(parts[1]);

    if (this.checked) {
        $('#inputNumAtribCosteOpcLinea_' + id).css("display", "block");
        $('#inputNumAtribCosteOpcLinea_' + id).css("text-align", "right");
        recalcularImportesCosteAnyadirLinea($('#valNumAtribCosteOpcLinea_' + id).text());

        $.each(gCostesLinea, function () {
            if (this.Id == id) {
                this.Seleccionado = true;
                var costeID = this.Id;
                $.each(this.Impuestos, function () {
                    this.Seleccionado = true;
                    $('#trIVA_opcLin' + costeID + "_" + this.Id).css("display", "");
                });
            }
        });
    } else {
        $('#inputNumAtribCosteOpcLinea_' + id).hide();
        recalcularImportesCosteRestaLinea($('#valNumAtribCosteOpcLinea_' + id).text());
        $.each(gCostesLinea, function () {
            if (this.Id == id) {
                this.Seleccionado = false;
                var costeID = this.Id;
                $.each(this.Impuestos, function () {
                    this.Seleccionado = false;
                    $('#trIVA_opcLin' + costeID + "_" + this.Id).hide();
                });
            }
        });
    }

    for (var i = 0; i < gLineas.length; i++) {
        if (gLineas[i].ID == Linea.ID) {
            gLineas[i].Costes = gCostesLinea;
            break;
        }
    }
    gEmisionPedido.Lineas = gLineas;
});
$('[id^=chkCosOblExcLinea_]').live('click', function () {
    var parts = this.id.split("_");
    var idGrupo = $.trim(parts[1]);
    var id = $.trim(parts[2]);
    var restar = 0;
    if (this.checked) {
        $.each($('[id^=inputNumAtribCosteOblExcLinea_' + idGrupo + ']'), function () {
            var parts = this.id.split("_");
            var idAnt = $.trim(parts[2]);
            if ($('#inputNumAtribCosteOblExcLinea_' + idGrupo + '_' + idAnt).css("display") == "block") {
                restar = $('#valNumAtribCosteOblExcLinea_' + idGrupo + '_' + idAnt).text();
                $('[id^=trIVA_oblExcLin' + idAnt + ']').hide();
            }
        });

        $.each(gCostesLinea, function () {
            var costeID = this.Id;
            if (this.GrupoCosteDescuento == idGrupo) {
                if (this.Id == id) {
                    this.Seleccionado = true;
                    $.each(this.Impuestos, function () {
                        this.Seleccionado = true;
                        $('[id^=trIVA_oblExcLin' + costeID + "_" + this.Id + ']').css("display", "");
                    });
                } else {
                    this.Seleccionado = false;
                    $.each(this.Impuestos, function () {
                        this.Seleccionado = false;
                    });
                }
            }
        });

        for (var i = 0; i < gLineas.length; i++) {
            if (gLineas[i].ID == Linea.ID) {
                gLineas[i].Costes = gCostesLinea;
                break;
            }
        }
        gEmisionPedido.Lineas = gLineas;

        $('[id^=inputNumAtribCosteOblExcLinea_' + idGrupo + ']').hide();
        $('#inputNumAtribCosteOblExcLinea_' + idGrupo + '_' + id).css("display", "block");
        $('#inputNumAtribCosteOblExcLinea_' + idGrupo + '_' + id).css("text-align", "right");
        recalcularImportesCosteAnyadirLinea($('#valNumAtribCosteOblExcLinea_' + idGrupo + '_' + id).text(), restar);
    } else {
        $('#inputNumAtribCosteOblExcLinea_' + idGrupo).hide();
        $('#inputNumAtribCosteOblExcLinea_' + idGrupo + '_' + id).css("display", "block");
    }
});
$('[id^=chkCosOpcExcLinea_]').live('click', function () {
    var parts = this.id.split("_");
    var idGrupo = $.trim(parts[1]);
    var id = $.trim(parts[2]);
    var restar = 0;
    if (this.checked) {
        $.each($('[id^=inputNumAtribCosteOpcExcLinea_' + idGrupo + ']'), function () {
            var parts = this.id.split("_");
            var idAnt = $.trim(parts[2]);
            if ($('#inputNumAtribCosteOpcExcLinea_' + idGrupo + '_' + idAnt).css("display") == "block") {
                restar = $('#valNumAtribCosteOpcExcLinea_' + idGrupo + '_' + idAnt).text();
                $('[id^=trIVA_oblOpcLin' + idAnt + ']').hide();
            }
        });

        $.each(gCostesLinea, function () {
            var costeID = this.Id;
            if (this.GrupoCosteDescuento == idGrupo) {
                if (this.Id == id) {
                    this.Seleccionado = true;
                    $.each(this.Impuestos, function () {
                        this.Seleccionado = true;
                        $('[id^=trIVA_oblOpcLin' + costeID + "_" + this.Id + ']').css("display", "");
                    });
                } else {
                    this.Seleccionado = false;
                    $.each(this.Impuestos, function () {
                        this.Seleccionado = false;
                    });
                }
            }
        });

        for (var i = 0; i < gLineas.length; i++) {
            if (gLineas[i].ID == Linea.ID) {
                gLineas[i].Costes = gCostesLinea;
                break;
            }
        }
        gEmisionPedido.Lineas = gLineas;

        $('[id^=inputNumAtribCosteOpcExcLinea_' + idGrupo + ']').hide();
        $('#inputNumAtribCosteOpcExcLinea_' + idGrupo + '_' + id).css("display", "block");
        $('#inputNumAtribCosteOpcExcLinea_' + idGrupo + '_' + id).css("text-align", "right");
        recalcularImportesCosteAnyadirLinea($('#valNumAtribCosteOpcExcLinea_' + idGrupo + '_' + id).text(), restar);
    } else {
        $('#inputNumAtribCosteOpcExcLinea_' + idGrupo).hide();
        $('#inputNumAtribCosteOpcExcLinea_' + idGrupo + '_' + id).css("display", "block");
    }
});
$('[id^=chkNumAtribDescuLinea_]').live('click', function () {
    var parts = this.id.split("_");
    var id = $.trim(parts[1]);
    if (this.checked) {
        $('#inputNumAtribDescuOpcLinea_' + id).css("display", "block");
        $('#inputNumAtribDescuOpcLinea_' + id).css("text-align", "right");
        recalcularImportesDescuentoAnyadirLinea($('#valNumAtribDescuOpcLinea_' + id).text());

        $.each(gDescuentosLinea, function () {
            if (this.Id == id) this.Seleccionado = true;
        });

    } else {
        $('#inputNumAtribDescuOpcLinea_' + id).hide();
        recalcularImportesDescuentoRestaLinea($('#valNumAtribDescuOpcLinea_' + id).text());
        $.each(gDescuentosLinea, function () {
            if (this.Id == id) this.Seleccionado = false;
        });
    }
});
$('[id^=chkDescuOblExcLinea_]').live('click', function () {
    var parts = this.id.split("_");
    var idGrupo = $.trim(parts[1]);
    var id = $.trim(parts[2]);
    var restar = 0;
    if (this.checked) {
        $.each($('[id^=inputNumAtribDescuExcLinea_' + idGrupo + ']'), function () {
            var parts = this.id.split("_");
            var idAnt = $.trim(parts[2]);
            if ($('#inputNumAtribDescuExcLinea_' + idGrupo + '_' + idAnt).css("display") == "block") {
                restar = $('#valNumAtribDescuExcLinea_' + idGrupo + '_' + idAnt).text();
            }
        });
        $.each(gDescuentosLinea, function () {
            if (this.GrupoCosteDescuento == idGrupo) {
                if (this.Id == id) this.Seleccionado = true;
                else this.Seleccionado = false;
            }
        });

        $('[id^=inputNumAtribDescuExcLinea_' + idGrupo + ']').hide();
        $('#inputNumAtribDescuExcLinea_' + idGrupo + '_' + id).css("display", "block");
        $('#inputNumAtribDescuExcLinea_' + idGrupo + '_' + id).css("text-align", "right");
        recalcularImportesDescuentoAnyadirLinea($('#valNumAtribDescuExcLinea_' + idGrupo + '_' + id).text(), restar);
    } else {
        $('#inputNumAtribDescuExcLinea_' + idGrupo).hide();
        $('#inputNumAtribDescuOblExcLinea_' + idGrupo + '_' + id).css("display", "block");
    }
});
$('[id^=chkDescuOpcExcLinea_]').live('click', function () {
    var parts = this.id.split("_");
    var idGrupo = $.trim(parts[1]);
    var id = $.trim(parts[2]);
    var restar = 0;
    if (this.checked) {
        $.each($('[id^=inputNumAtribDescuOpcExcLinea_' + idGrupo + ']'), function () {
            var parts = this.id.split("_");
            var idAnt = $.trim(parts[2]);
            if ($('#inputNumAtribDescuOpcExcLinea_' + idGrupo + '_' + idAnt).css("display") == "block")
                restar = $('#valNumAtribDescuOpcExcLinea_' + idGrupo + '_' + idAnt).text();
        });
        $.each(gDescuentosLinea, function () {
            if (this.GrupoCosteDescuento == idGrupo) {
                if (this.Id == id) this.Seleccionado = true;
                else this.Seleccionado = false;
            }
        });

        $('[id^=inputNumAtribDescuOpcExcLinea_' + idGrupo + ']').hide();
        $('#inputNumAtribDescuOpcExcLinea_' + idGrupo + '_' + id).css("display", "block");
        $('#inputNumAtribDescuOpcExcLinea_' + idGrupo + '_' + id).css("text-align", "right");
        recalcularImportesDescuentoAnyadirLinea($('#valNumAtribDescuOpcExcLinea_' + idGrupo + '_' + id).text(), restar);
    } else {
        $('#inputNumAtribDescuOpcExcLinea_' + idGrupo).hide();
        $('#inputNumAtribDescuOpcExcLinea_' + idGrupo + '_' + id).css("display", "block");
    }
});
$('#imgAplicarActivo').live('click', function () {
    gLineas = gEmisionPedido.Lineas;

    for (var i = 0; i < gLineas.length; i++) {
        if (gLineas[i].ID !== Linea.ID) {
            if (gLineas[i].Pres5_ImportesImputados_EP !== null) {
                if (gLineas[i].Pres5_ImportesImputados_EP[0] !== undefined) {
                    var CentroContrato = gLineas[i].Pres5_ImportesImputados_EP[0];
                    if (CentroContrato.CentroCoste !== undefined) {
                        var CentroCoste = CentroContrato.CentroCoste;
                        if (CentroCoste == undefined) {
                            gLineas[i].Activo = $("#tbActivosLinea_Hidden").val();
                            gLineas[i].DescripcionActivo = $("#tbActivosLinea").val();
                        } else {
                            var CentroSM = CentroCoste.CentroSM;
                            var centroActivoLinea = $("#tbActivosCentro_Hidden").val();
                            if (centroActivoLinea !== "") {
                                var parts = centroActivoLinea.split(" - ");
                                if (CentroSM.Codigo == "" || CentroSM.Codigo == $.trim(parts[0])) {
                                    gLineas[i].Activo = $("#tbActivosLinea_Hidden").val();
                                    gLineas[i].DescripcionActivo = $("#tbActivosLinea").val();
                                }
                            }
                        }
                    }
                } else {
                    gLineas[i].Activo = $("#tbActivosLinea_Hidden").val();
                    gLineas[i].DescripcionActivo = $("#tbActivosLinea").val();
                }
            } else {
                gLineas[i].Activo = $("#tbActivosLinea_Hidden").val();
                gLineas[i].DescripcionActivo = $("#tbActivosLinea").val();
            }
        };
    }
    gEmisionPedido.Lineas = gLineas;
});
$('[id$=imgActivo]').live('click', function () {
    var popup = $('#divActivos');
    $('#popupFondo').css('height', $(document).height());
    $('#popupFondo').show();
    popup.css("min-width", "700");
    popup.css("min-height", "540px");
    CentrarPopUp($('#divActivos'));
    var iConta = 0;
    var empresa = 0;
    if ($("#CmbBoxEmpresa").val() !== null) empresa = $("#CmbBoxEmpresa").val();
    $('[id$=SMActivosEP_ddlEmpresa]').val(empresa);
    if ($('[id$=SMActivosEP_ddlEmpresa]').val() == "" || $('[id$=SMActivosEP_ddlEmpresa]').val() == null) {
        $('[id$=SMActivosEP_ddlEmpresa]').append('<option value="' + empresa + '" selected="selected" nuevo="true">' + $("#CmbBoxEmpresa option:selected").text() + '</option>');
        $('[id$=SMActivosEP_ddlEmpresa]').val(empresa);
        $('[id$=SMActivosEP_gridActivos] tbody').html("");
        $('[id$=SMActivosEP_gridActivos] tbody').append("<tr><td><span id='lblEmptyDataText'>" + TextosJScript[57] + "</span></td></tr>");
    }
    var tbCtroCosteHidden = $('#tbCtroCoste_Hidden');
    var tbCtroCoste = $('#tbCtroCoste');
    if (tbCtroCosteHidden.val() !== "") {
        $('[id$=SMActivosEP_txtCentro]').val(tbCtroCoste.val());
    } else {
        $('[id$=SMActivosEP_txtCentro]').val("");
    }

    $('[id$=SMActivosEP_ddlEmpresa]').prop("disabled", true);
    $('#divActivos').show();
    if ($('[id$=SMActivosEP_ddlEmpresa]').val() !== "") {
        $('[id$=SMActivosEP_btnBuscar]')[0].click();
    }
});
$('[id$=SMActivosEP_btnBuscar]').live('click', function (event) {
    if ($('[id$=SMActivosEP_ddlEmpresa] option:selected')[0].attributes.getNamedItem('nuevo') !== null) {
        if ($('[id$=SMActivosEP_ddlEmpresa] option:selected')[0].attributes.getNamedItem('nuevo').value.indexOf("true") >= 0) {
            stopEvent(event);
        }
    }
});
$('[id$=btnGuardarCesta]').live('click', function () {
    MostrarCargando();
    setTimeout(function () { guardarPedido(0) }, 100);

});
$('[id$=GuardarFavorito]').live('click', function () {
    MostrarCargando();
    if (imgGuardarFavorito.accion) setTimeout(function () { guardarPedido(1) }, 100);
    else setTimeout(function () { guardarPedido(3) }, 100);
});
$('[id$=btnModificarFavorito]').live('click', function () {
    MostrarCargando();
    if (btnModificarFavorito.accion) setTimeout(function () { modificarFavorito() }, 100);
    else setTimeout(function () { guardarPedido(2) }, 100);
});
$('[id$=btnEmitirPedido]').live('click', function () {
    MostrarCargando();
    setTimeout(function () { emitirPedido(false) }, 300);
});
$('[id$=PbtnAceptarErrorPartida],[id$=PbtnCancelarErrorPartida]').click(function () {
    $('#textErrorPartida').empty();
    $('#textErrorPartidaCab').empty();
    $('#divErrorPartida').hide();
    OcultarFondoPopUp();
});
$('[id$=PbtnAceptarErrorAviso]').live('click', function () {
    $('#textErrorAviso').empty();
    $('#divErrorAviso').hide();
    MostrarCargando();
    setTimeout("emitirPedido(true)", 10);
});
$('[id$=PbtnCancelarErrorAviso],[id$=ImgBtnCerrarErrorAviso]').live('click', function () {
    $('#textErrorAviso').empty();
    $('#divErrorAviso').hide();
    OcultarFondoPopUp();
});
$('[id$=btnAceptarErrorE],[id$=btnCancelarErrorE]').live('click', function () {
    $('#textError').empty();
    $('#textErrorCab').empty();
    OcultarFondoPopUp();
    OcultarError();
    $('[id$=panelUpdateProgress]').hide();
    $('[id$=backgroundElement]').hide();
});
$('#btnCerrarPopUp').live('click', function () {
    $('#popupFondo').hide();
    $('#popupBuscadorObjetoImputacion').hide();
});
$('[id^=atrib_]').live('click', function () {
    $(this).select();
});
$('#btnAceptarConfirmPedido').live('click', function () {
    if (EsPedidoAbierto) document.location.href = rutaFS + 'EP/PedidoAbierto.aspx';
    else document.location.href = rutaFS + 'EP/CatalogoWeb.aspx';
});
$('[id*=imgCentroCoste]').live('click', function () {
    var id = $(this).attr('id');
    var iTipo = id.substr(id.length - 1);
    var valor;
    switch (parseInt(iTipo)) {
        case 1:
            valor = (Linea.Pres1 == null ? '' : Linea.Pres1);
            break;
        case 2:
            valor = (Linea.Pres2 == null ? '' : Linea.Pres2);
            break;
        case 3:
            valor = (Linea.Pres3 == null ? '' : Linea.Pres3);
            break;
        default:
            valor = (Linea.Pres4 == null ? '' : Linea.Pres4);
            break;
    }
    window.open(rutaFS + '_common/BuscadorPresupuestos.aspx?EsPedidoAbierto=1&iTipo=' + iTipo + '&ID=&Valor=' + Var2Param(valor) + '&IDPres=' + $(this).attr('id') + '&Campo=25841&Instancia=&Solicitud1431=&Cantidad=&Titulo=&OrgCompras=&Articulo=', '_blank', 'width=840,height=660,status=yes,resizable=no,top=75,left=120');
});
$('#cboCentroOrgCompra').live('click', function () {
    //Miramos si esta el combo cargado preguntando si existe el item vacio
    if ($('#cboCentroOrgCompra option[value=""]').length == 0) cargarCentrosAprovisionamiento(gDatosGenerales.OrgCompras);

    if (bHayIntegracionPedidos && gDatosGenerales.UsarOrgCompras) {
        if ((Linea.CentroAprovisionamiento !== null) && (Linea.CentroAprovisionamiento !== '')) {
            //Demo Berge 2017/16. Tras devolución quedamos en q Tipo Pedido Manda. Si Oblig-> Lo veras y Oblig. Luego NO emitira, pero ver lo ves.
            var DebeEstar = false;
            if (gEmisionPedido.DatosGenerales.Tipo !== null) {
                DebeEstar = (gEmisionPedido.DatosGenerales.Tipo.Almacenable == 1)
            }
            if ((Linea.TipoArticulo.TipoAlmacenamiento !== 0) || (DebeEstar)) {
                var idSeleccionado = ''; //Destino no me importa
                var sCentro = $('#cboCentroOrgCompra').val();

                var bAlmacenes = comprobarAlmacenes(idSeleccionado, Linea, sCentro);
                //Demo Berge 2017/16. Tras devolución quedamos en q Tipo Pedido Manda. Si Oblig-> Lo veras y Oblig. Luego NO emitira, pero ver lo ves.
                if ((bAlmacenes == true) || (DebeEstar)) {
                    $('[id$=LblAlmacen]').show();
                    $('#cboAlmacen').show();
                    $('#BtnCopiarAlmacen').css("display", "inline-block");
                } else {
                    $('[id$=LblAlmacen]').hide();
                    $('#BtnCopiarAlmacen').hide();
                    $('#cboAlmacen').hide();
                    Linea.IdAlmacen = 0;
                    Linea.strAlmacen = "";
                }
            } else {
                $('[id$=LblAlmacen]').hide();
                $('#BtnCopiarAlmacen').hide();
                $('#cboAlmacen').hide();
                Linea.IdAlmacen = 0;
                Linea.strAlmacen = "";
            }
        }
    }
});
$('#im').live('click', function () { infoDire(); return false; });
//#endregion
//#region Eventos change
$("#cboCentroOrgCompra").change(function () {
    idCentroOrgCompra = $('#cboCentroOrgCompra').val();
    Linea.CentroAprovisionamiento = idCentroOrgCompra;
});
$('#CmbBoxProvesErp').change(function () {
    if ($('#CmbBoxProvesErp')[0].value == "") $('#btnInfo').hide();
    else $('#btnInfo').show();
});
$("#wddRecept").change(function () {
    idRecept = $('#wddRecept').val();
    $("#wddRecept").val(idRecept).attr("selected", "selected");
});
$("#cbDireccionEnvioFactura").change(function () {
    if ($('#cbDireccionEnvioFactura').val() !== "") $('#lblCodDireccion').text($('#cbDireccionEnvioFactura').val());
    else $('#lblCodDireccion').text("0");
});
$("#CmbBoxTipoPedido").change(function () {
    MostrarCargando();
    setTimeout(function () {
        var bValidado;
        var combo = $("#CmbBoxTipoPedido");
        bValidado = comprobarTipoPedido();
        if (bValidado == true) {
            oTipo = combo.val();
        }

        //Otros Datos
        CargarAtributosCabeceraTipoPedido();
        CargarAtributosLineasTipoPedido();

        if (gAtributos.length == 0) {
            $('[id$=divAtributosItem]').hide();
            $('#pesOtrosDatos').hide();
        }
        else {
            $('#pesOtrosDatos').css("display", "inline-block");
            $('[id$=divAtributosItem]').show();
        }

        $.each(gLineas, function () {
            if (gEmisionPedido.DatosGenerales.Tipo !== null) {
                if (gEmisionPedido.DatosGenerales.Tipo.Concepto == 0) {
                    this.Activo = 0;
                    this.DescripcionActivo = "";
                }
            } else {
                this.Activo = 0;
                this.DescripcionActivo = "";
            }
        });
        bCambioEmpresa = true;
        var iCodFilaSeleccionadaAnterior = iCodFilaSeleccionada;
        if (gLineas.length !== 0) {
            calcularTotalesInicio();
            iCodFilaSeleccionada = iCodFilaSeleccionadaAnterior;
        }

        $('#pesDatosGenerales').click();
        $('#pesResumenLineas').click();

        $.each($.map(gLineas, function (x) { if (x.LineaCatalogo !== 0) return x; }), function () {
            if (comprobarEmpresa(this.ID)) {
                OcultarError();
                quitarAvisoEnLinea();
                $('[id^=aviso]').css("display", "none");
            };
        });
    }, 1);
});
$("#CmbBoxEmpresa").change(function () {
    MostrarCargando();
    var bEmpresa = true;
    var bOrgCompras = true;
    bCambioEmpresa = true;
    ComprobarIntegracion();
    gDatosCentros = undefined;
    $.each($.map(gLineas, function (x) { if (x.LineaCatalogo !== 0) return x; }), function () {
        if (comprobarEmpresa(this.ID)) {
            comprobarCentroUnico();
            OcultarError();
            quitarAvisoEnLinea();
            $('[id^=aviso]').css("display", "none");
        };
    });
});
$('[id$=CmbBoxOrgCompras]').change(function () {
    gDatosGenerales.OrgCompras = $('[id$=CmbBoxOrgCompras]').val();
    if ($('[id$=CmbBoxOrgCompras]').val() != "") {
        for (var j = 0; j < gLineas.length; j++) {
            if (gLineas[j].LineaCatalogo !== 0) {
                bOrgCompras = comprobarOrganizacionCompras(gLineas[j].ID);
                if (bOrgCompras) {
                    OcultarError();
                    quitarAvisoEnLinea();
                    $('[id^=aviso]').hide();
                }
            };
        }
    }

    //Si está visible el combo de organización de compras recargarlo
    if ($('#cboCentroOrgCompra').is(':visible')) { cargarCentrosAprovisionamiento() }
    //Establecer centros de aprovisionamiento    
    for (var j = 0; j < gLineas.length; j++) {
        asignarCentroAprovisionamiento(gLineas[j]);
    }
});
$('[id^=txtFecEntSol]').change(function () {
    if ($('[id^=txtFecEntSol]')[0].value == "") {
        Linea.FecEntrega = null;
    } else {
        Linea.FecEntrega = $('[id^=txtFecEntSol]').datepicker("getDate");
    }
    for (var i = 0; i < gLineas.length; i++) {
        if (gLineas[i].ID == Linea.ID) {
            gLineas[i] = Linea;
            break;
        }
    }
});
$('[id$=tbCtroCoste]').change(function (event) {
    var tbCtroCoste = $('#tbCtroCoste');
    var tbCtroCosteHidden = $('#tbCtroCoste_Hidden');
    if (tbCtroCoste.val() == '') tbCtroCosteHidden.val('');
    for (var i = 0; i < gLineas.length; i++) {
        if (gLineas[i].ID == Linea.ID) {
            ComprobarCentroContratoEscrito();
            gCentroContrato.LineaId = Linea.ID;
            CentroContrato = gCentroContrato;
            Linea.Pres5_ImportesImputados_EP[0] = CentroContrato;
            gLineas[i].Pres5_ImportesImputados_EP[0] = CentroContrato;
        }
    }
    var tbActivoCentroHidden = $('#tbActivosCentro_Hidden');
    tbActivoCentroHidden.val("");
    var tbActivoHidden = $('#tbActivosLinea_Hidden');
    tbActivoHidden.val("0");
    var tbActivo = $('#tbActivosLinea');
    tbActivo.val("");
    stopEvent(event);
});
$('[id$=tbPPres]').change(function (event) {
    if ($('#tbPPres').val() !== "") {
        var sTexto = $('#tbPPres').val();
        for (var i = 0; i < gLineas.length; i++) {
            if (gLineas[i].ID == Linea.ID) {
                gCentroContrato = gLineas[i].Pres5_ImportesImputados_EP[0];
                ComprobarContratoEscrito();
                gCentroContrato.LineaId = Linea.ID;
                ComprobarCentroContrato(1);
                CentroContrato = gCentroContrato;
                Linea.Pres5_ImportesImputados_EP[0] = CentroContrato;
                gLineas[i].Pres5_ImportesImputados_EP[0] = CentroContrato;
            };
        }
    }
    else {
        $('#tbPPres_Hidden').val('');
    }
    stopEvent(event);
});
//#endregion
$('[id$=tbActivosLinea]').blur(function () {
    var oActivo;
    var tbActivo = $('#tbActivosLinea');
    var tbActivoHidden = $('#tbActivosLinea_Hidden');
    var tbCtroCoste = $('#tbCtroCoste');
    var parts = tbCtroCoste.val().split(" - ");
    var sCentro = "";
    if ($.trim(parts[0]) !== "") sCentro = $.trim(parts[0]);

    var empresa = 0;
    if ($("#CmbBoxEmpresa").val() !== null) empresa = $("#CmbBoxEmpresa").val();
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Comprobar_Activo',
        contentType: "application/json; charset=utf-8",
        data: "{'sEmp':'" + empresa + "', 'sActivo':'" + tbActivo.val() + "', 'sCentro':'" + sCentro + "'}",
        dataType: "json",
        async: false
    })).done(function (msg) {
        if (msg.d == 0) {
            tbActivoHidden.val(0);
            tbActivo.val("");
            tbActivo.css('background-color', '');
        } else {
            oActivo = $.parseJSON(msg.d);
            tbActivoHidden.val(oActivo.ID);
            tbActivo.val(oActivo.Codigo + " - " + oActivo.Denominacion);
            tbActivo.css('background-color', '#D8E4BC');
        }
    });
});
//#region Funciones carga pantalla
//Cargamos los textos
function cargarTextosPantalla() {
    $("#LblTxtSelecDest").text(TextosJScript[22]);
    $("#LblCodDest").text(TextosJScript[23]);
    $("#LblDenDest").text(TextosJScript[24]);
    $("#LblDirecDest").text(TextosJScript[25]);
    $("#LblCpDest").text(TextosJScript[26]);
    $("#LblPobDest").text(TextosJScript[27]);
    $("#LblProvDest").text(TextosJScript[28]);
    $("#LblPaisDest").text(TextosJScript[29]);
    $("#txtAceptarAdj").text(TextosJScript[7]);
    $("#txtCancelarAdj").text(TextosJScript[8]);
    $("#txtAceptarAdjLin").text(TextosJScript[7]);
    $("#txtCancelarAdjLin").text(TextosJScript[8]);
    $('#btnAceptarConfirmPedido').text(TextosJScript[73]);
    $('#btnModificarFavorito').text(TextosJScript[74]);
    $('[id$=btnGuardarCesta]').text(TextosJScript[75]);
    $('[id$=btnEmitirPedido]').text(TextosJScript[76]);
    $('#lblTitulo').text(TextosJScript[77]);
    $('#lblCodigo').text(TextosJScript[78]);
    $('#lblDescripcion').text(TextosJScript[79]);
    $('#lblCampo1').text(TextosJScript[80]);
    $('#lblCampo2').text(TextosJScript[81]);
    $('#lblCampo3').text(TextosJScript[82]);
    $('#lblCampo4').text(TextosJScript[83]);

    $('#btnAceptarPedido').text(TextosJScript[85]);

    $('#lblCabeceraPlanEntregaFechaEntrega').text(TextosJScript[98]);
    $('#btnAgregarPlanEntrega').val(TextosJScript[96]);
    $('#btnEliminarPlanEntrega').val(TextosJScript[97]);
}
//Cargamos la pestaña de datos generales
function cargarDatosGenerales() {
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Cargar_Datos_Generales',
        contentType: "application/json; charset=utf-8",
        data: "{'iDesdeFavorito': '" + desdeFavorito + "','sIden':'" + sID + "','iCodFavorito': '" + sIDFavorito + "','iModiFav': '" + bModificarFavorito + "'}",
        dataType: "json",
        async: false
    })).done(function (msg) {
        bHayIntegracionPedidos = false;
        gEmisionPedido = $.parseJSON(msg.d);
        gDatosGenerales = gEmisionPedido.DatosGenerales;
        sMoneda = gDatosGenerales.Moneda;
        EsPedidoAbierto = gDatosGenerales.EsPedidoAbierto;
        var iEmpresa = gDatosGenerales.Emp;
        if (iEmpresa == "") {
            if (iEmpresa == "") {
                if ($("#CmbBoxEmpresa").val() !== null) iEmpresa = $("#CmbBoxEmpresa").val();
                else iEmpresa = 0;
            }
            gDatosGenerales.Emp = iEmpresa;
        }
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Comprobar_Integracion',
            contentType: "application/json; charset=utf-8",
            data: "{'iEmpresa':'" + iEmpresa + "'}",
            dataType: "json",
            async: false
        })).done(function (msg) {
            bHayIntegracionPedidos = $.parseJSON(msg.d);
            if (bHayIntegracionPedidos) {
                $('#CmbBoxProvesErp').css("display", "inline");
                $('#LblProveErp').css("display", "block");
                if (gDatosGenerales.UsarOrgCompras) {
                    $('[id$=CmbBoxOrgCompras]').css("display", "inline");
                    $('#lblOrgCompras').css("display", "block");
                    $('#centroAprovisionamiento').css("display", "inline");

                    cargarOrganizacionesCompras(gDatosGenerales.Emp);
                    asignarOrganizacionCompras(gDatosGenerales);
                } else {
                    $('[id$=CmbBoxOrgCompras]').hide();
                    $('#lblOrgCompras').hide();
                }
            } else {
                $('#CmbBoxProvesErp').hide();
                $('#LblProveErp').hide();
                $('[id$=CmbBoxOrgCompras]').hide();
                $('#lblOrgCompras').hide();
            }
        });
        if (bMostrarCodigoProve == true) $('#FSNLinkProve').text(gDatosGenerales.CodProve + " - " + gDatosGenerales.DenProve);
        else $('#FSNLinkProve').text(gDatosGenerales.DenProve);

        $('#FSNLinkProve').val(gDatosGenerales.CodProve);
        if ($('#CmbBoxTipoPedido option').length == 1) {
            oTipo = $('#CmbBoxTipoPedido').val();
            cargarNuevoTipoPedido();
        } else {
            if (gDatosGenerales.Tipo !== null) {
                $('#CmbBoxTipoPedido').val(gDatosGenerales.Tipo.Id).attr("selected", "selected");
                oTipo = $('#CmbBoxTipoPedido').val();
            } else oTipo = 0;
        }

        $('#TxtBxPedSAP').val(gDatosGenerales.Referencia);
        if ($('#lblCodDireccion').text() == "0") {
            if (gDatosGenerales.DirEnvio !== 0) {
                $('#lblCodDireccion').text(gDatosGenerales.DirEnvio);
                $('#cbDireccionEnvioFactura').val(gDatosGenerales.DirEnvio);
            } else {
                $('#lblCodDireccion').text("0");
            }
        };
        idRecept = gDatosGenerales.Receptor;
        if (idRecept !== "" && idRecept !== 0) bCargadoInicio = true;

        if ($("#CmbBoxEmpresa").val() !== null) {
            $("#CmbBoxEmpresa").val(gDatosGenerales.Emp);
            if (EsPedidoAbierto) {
                $("#CmbBoxEmpresa").hide();
                $("#CmbBoxEmpresa").siblings('span').text($("#CmbBoxEmpresa option[value='" + gDatosGenerales.Emp + "']").text());
            }
        }
        var empresa = 0;
        if ($("#CmbBoxEmpresa").val() !== null) empresa = $("#CmbBoxEmpresa").val();
        if (bHayIntegracionPedidos == true) {
            $('#CmbBoxProvesErp').css("display", "inline");
            $('#LblProveErp').css("display", "block");
            cargarProveerdoresERP(empresa, gDatosGenerales.CodProve, gDatosGenerales.CodErp);
            if (bdevolverCodErp) {
                $('#CmbBoxProvesErp').val(gDatosGenerales.CodErp).attr("selected", "selected");
                if (gDatosGenerales.CodErp == 0 || $('#CmbBoxProvesErp option').length == 0) $('#btnInfo').hide();
                else $('#btnInfo').show();
                if (EsPedidoAbierto) $('#CmbBoxProvesErp :not(option[value="' + gDatosGenerales.CodErp + '"])').remove();
                if (EsPedidoAbierto) {
                    $('#CmbBoxProvesErp').hide();
                    $("#CmbBoxProvesErp").siblings('span').text($("#CmbBoxProvesErp option[value='" + gDatosGenerales.CodErp + "']").text());
                }
            } else {
                $('#CmbBoxProvesErp').hide();
                $('#LblProveErp').hide();
                $('#btnInfo').hide()
            }
        } else {
            $('#CmbBoxProvesErp').hide();
            $('#LblProveErp').hide();
            $('#btnInfo').hide()
        }
        if (gDatosGenerales.Tipo !== null) {
            if (gDatosGenerales.Tipo.Id !== 0) $('#pesOtrosDatos').css("display", "inline-block");
            else $('#pesOtrosDatos').hide();
        } else $('#pesOtrosDatos').hide();
        $("#textObser").val(gDatosGenerales.Obs);
        $("#textObser").val(gDatosGenerales.Obs);

        if (gDatosGenerales.CodProvePortal == '') {
            //Si el proveedor no está dado de alta en portal no se le puede pedir aceptación del pedido
            document.getElementById("chkSolicitAcepProve").checked = false;
            document.getElementById("chkSolicitAcepProve").disabled = true;
        } else {
            //CESTA_CABECERA.ACEP_PROVE está a NULL sielmpre que se entra por primera vez        
            if (gDatosGenerales.AcepProve == null && desdeFavorito == 0) {
                if (!gbEPMarcarAcepProve) {
                    document.getElementById("chkSolicitAcepProve").checked = gDatosGenerales.ProveSolicitAceptacionRecepcion;
                    document.getElementById("chkSolicitAcepProve").disabled = gDatosGenerales.ProveSolicitAceptacionRecepcion;
                }
                else {
                    document.getElementById("chkSolicitAcepProve").checked = true;
                }
            }
            else {
                document.getElementById("chkSolicitAcepProve").checked = gDatosGenerales.AcepProve;
                if (desdeFavorito == 0) document.getElementById("chkSolicitAcepProve").disabled = (!gbEPMarcarAcepProve && gDatosGenerales.ProveSolicitAceptacionRecepcion);
            }
        }

        if (desdeFavorito == 1) $("#txtNomFavorito").val(gDatosGenerales.NomFav);
        else $("#txtNomFavorito").val("");

        if (!gDatosGenerales.GS_Pres1.Activo) $('#pres1OrgCompra').hide();
        else if (!gDatosGenerales.GS_Pres1.Obligatorio) $('#pres1OrgCompra span').text(gDatosGenerales.GS_Pres1.Denominacion);
        else $('#pres1OrgCompra span').text('(*) ' + gDatosGenerales.GS_Pres1.Denominacion);

        if (!gDatosGenerales.GS_Pres2.Activo) $('#pres2OrgCompra').hide();
        else if (!gDatosGenerales.GS_Pres2.Obligatorio) $('#pres2OrgCompra span').text(gDatosGenerales.GS_Pres2.Denominacion);
        else $('#pres2OrgCompra span').text('(*) ' + gDatosGenerales.GS_Pres2.Denominacion);

        if (!gDatosGenerales.GS_Pres3.Activo) $('#pres3OrgCompra').hide();
        else if (!gDatosGenerales.GS_Pres3.Obligatorio) $('#pres3OrgCompra span').text(gDatosGenerales.GS_Pres3.Denominacion);
        else $('#pres3OrgCompra span').text('(*) ' + gDatosGenerales.GS_Pres3.Denominacion);

        if (!gDatosGenerales.GS_Pres4.Activo) $('#pres4OrgCompra').hide();
        else if (!gDatosGenerales.GS_Pres4.Obligatorio) $('#pres4OrgCompra span').text(gDatosGenerales.GS_Pres4.Denominacion);
        else $('#pres4OrgCompra span').text('(*) ' + gDatosGenerales.GS_Pres4.Denominacion);
    });
};
//#endregion
//#region Eventos grid whdgArticulos
/*Revisado por: blp. Fecha: 29/08/2012
<summary>
Método necesario para ordenar las columnas del grid
</summary>
<remarks>Llamada desde: evento ColumnSorting del grid. Máx. 0,1 seg.</remarks>*/
function whdgArticulos_Sorting_ColumnSorting(grid, args) {
    var sorting = grid.get_behaviors().get_sorting();
    var col = args.get_column();
    var sortedCols = sorting.get_sortedColumns();
    for (var x = 0; x < sortedCols.length; ++x) {
        if (col.get_key() == sortedCols[x].get_key()) {
            args.set_clear(false);
            break;
        }
    }
}
function whdgArticulos_CellClick(sender, args) {
    var grid = $find($('[id$=whdgArticulos]').attr('id'));
    var gridView = grid.get_gridView();
    var NUMActual, NUMInicial;
    if (args.get_type() == "cell") {
        if (args.get_item().get_column().get_key() !== "Key_DELETE") {
            var row = args.get_item().get_row();
            if (row.get_index() == -1) { //Si pincha en la fila de filtros salimos de la funcion
                args.set_cancel(true);
                return false;
            };

            iFilaSeleccionada = row.get_cellByColumnKey("Key_NUM_LINEA").get_value();
            iCodFilaSeleccionada = row.get_cellByColumnKey("Key_IDLINEA").get_value();
            gridView.get_behaviors().get_selection().get_selectedRows().add(row);
            var strFila = ("00" + iFilaSeleccionada.toString()).slice(-3);
            $('#lblDatosLineaLinea').text(" - " + TextosJScript[51] + " " + strFila);
            gLineas = gEmisionPedido.Lineas;
            Linea = $.grep(gLineas, function (x) { return (x.ID == iCodFilaSeleccionada) })[0];
            if ((Linea.Descuentos == undefined) || (Linea.Descuentos.length == 0)) {
                $('#pesDescuentosLinea').hide();
            } else {
                $('#pesDescuentosLinea').show();
                $('#lblDescuentosLineaLinea').text(" - " + TextosJScript[51] + " " + strFila);
            }
            if ((Linea.Costes == undefined) || (Linea.Costes.length == 0)) {
                $('#pesCostesLinea').hide();
            } else {
                $('#pesCostesLinea').show();
                $('#lblCostesLineaLinea').text(" - " + TextosJScript[51] + " " + strFila);
            }
            $('#lblPlanesEntregaLinea').text(' - ' + TextosJScript[51] + ' ' + strFila);
            if (Linea.ConPlanEntrega) {
                $('#rbPlanEntrega').prop('checked', true);
                GestionarChecksFechaPlanEntrega(false);
            } else {
                GestionarChecksFechaPlanEntrega(true);
                $('#rbFechaEntrega').prop('checked', true);
                if (Linea.FecEntrega !== null) {
                    $('#txtFecEntSol').datepicker('setDate', Linea.FecEntrega);
                } else $('#txtFecEntSol').val("");
                if (Linea.EntregaObl == 1) $('#CBObli').prop('checked', true);
                else $('#CBObli').prop('checked', false);
            };
            ComprobacionesOK = undefined;

            //Botón subir (En la primera fila deshabilitado) 
            if ((args.get_item().get_column().get_key() == "Key_SUBIRORDEN") && (row.get_index() !== 0)) {
                var btn = $('[id$=btnArticulosPostBack]').attr('id');
                NUMActual = gridView.get_rows().get_row(args.get_item().get_row().get_index()).get_cellByColumnKey("Key_NUM_LINEA").get_value();
                NUMInicial = gridView.get_rows().get_row(args.get_item().get_row().get_index() - 1).get_cellByColumnKey("Key_NUM_LINEA").get_value();

                //Se actualiza gEmisionPedido.Lineas con NUMActual, NUMInicial
                for (var i = 0; i < gEmisionPedido.Lineas.length; i++) {
                    if (gEmisionPedido.Lineas[i].NumLinea == NUMInicial) gEmisionPedido.Lineas[i].NumLinea = 2000;
                    if (gEmisionPedido.Lineas[i].NumLinea == NUMActual) gEmisionPedido.Lineas[i].NumLinea = NUMInicial;
                }

                for (var k = 0; k < gEmisionPedido.Lineas.length; k++) {
                    if (gEmisionPedido.Lineas[k].NumLinea == 2000) {
                        gEmisionPedido.Lineas[k].NumLinea = NUMActual
                        break;
                    }
                }

                __doPostBack(btn, JSON.stringify({ NUMActual: NUMActual, NUMInicial: NUMInicial }));
                ModoEdicion = true;
            }

            //Botón bajar (En la última fila deshabilitado)
            if ((args.get_item().get_column().get_key() == "Key_BAJARORDEN") && (row.get_index() !== gridView.get_rows().get_length() - 1)) {
                var btn = $('[id$=btnArticulosPostBack]').attr('id');
                NUMActual = gridView.get_rows().get_row(args.get_item().get_row().get_index()).get_cellByColumnKey("Key_NUM_LINEA").get_value();
                NUMInicial = gridView.get_rows().get_row(args.get_item().get_row().get_index() + 1).get_cellByColumnKey("Key_NUM_LINEA").get_value();

                //Se actualiza gEmisionPedido.Lineas
                for (var m = 0; m < gEmisionPedido.Lineas.length; m++) {
                    if (gEmisionPedido.Lineas[m].NumLinea == NUMInicial) gEmisionPedido.Lineas[m].NumLinea = 2000;
                    if (gEmisionPedido.Lineas[m].NumLinea == NUMActual) gEmisionPedido.Lineas[m].NumLinea = NUMInicial;
                }

                for (var n = 0; n < gEmisionPedido.Lineas.length; n++) {
                    if (gEmisionPedido.Lineas[n].NumLinea == 2000) {
                        gEmisionPedido.Lineas[n].NumLinea = NUMActual
                        break;
                    }
                }

                __doPostBack(btn, JSON.stringify({ NUMActual: NUMActual, NUMInicial: NUMInicial }));
                ModoEdicion = true;
            }
        }
    }
}
//Función que impide que se puedan seleccionar las líneas cuya recepción está completa o bien que corresponden a recepciones automáticas de planes de entrega
function whdgArticulos_RowSelectionChanged(sender, e) {
    var grid = $find($('[id$=whdgArticulos]').attr('id'));
    var gridView = grid.get_gridView();

    var row = gridView.get_behaviors().get_selection().get_selectedRows(0);
    if (row.getItem(0) != null) {
        iFilaSeleccionada = row.getItem(0).get_cellByColumnKey("Key_NUM_LINEA").get_value();
        iCodFilaSeleccionada = row.getItem(0).get_cellByColumnKey("Key_IDLINEA").get_value();
        gridView.get_behaviors().get_selection().get_selectedRows().add(row.getItem(0));

        var strFila = ("00" + iFilaSeleccionada.toString()).slice(-3);
        $('#lblDatosLineaLinea').text(" - " + TextosJScript[51] + " " + strFila);

        Linea = $.map(gEmisionPedido.Lineas, function (x) { if (x.ID == iCodFilaSeleccionada) return x; })[0];
        if ((Linea.Descuentos == undefined) || (Linea.Descuentos.length == 0)) {
            $('#pesDescuentosLinea').hide();
        } else {
            $('#pesDescuentosLinea').css("display", "inline-block");
            $('#lblDescuentosLineaLinea').text(' - ' + TextosJScript[51] + ' ' + strFila);
        }
        if ((Linea.Costes == undefined) || (Linea.Costes.length == 0)) {
            $('#pesCostesLinea').hide();
        } else {
            $('#pesCostesLinea').css("display", "inline-block");
            $('#lblCostesLineaLinea').text(' - ' + TextosJScript[51] + ' ' + strFila);
        }
        $('#lblPlanesEntregaLinea').text(' - ' + TextosJScript[51] + ' ' + strFila);
        if (Linea.ConPlanEntrega) {
            $('#rbPlanEntrega').prop('checked', true);
            GestionarChecksFechaPlanEntrega(false);
        } else {
            GestionarChecksFechaPlanEntrega(true);
            $('#rbFechaEntrega').prop('checked', true);
            if (Linea.FecEntrega !== null) {
                $('#txtFecEntSol').datepicker('setDate', Linea.FecEntrega);
            } else $('#txtFecEntSol').val("");
            if (Linea.EntregaObl == 1) $('#CBObli').prop('checked', true);
            else $('#CBObli').prop('checked', false);
        };
    }
}
function whdgArticulos_ExitedEditMode(sender, e) {
    var grid = $find($('[id$=whdgArticulos]').attr('id'));
    var gridView = grid.get_gridView();

    //En este punto, se introduce el valor inicial en la celda del grid editada
    gridView.get_rows().get_row(e.getCell().get_row().get_index()).get_cellByColumnKey("Key_NUM_LINEA").set_value(NUMInicial, ("00" + NUMInicial).slice(-3));
};
function whdgArticulos_ExitingEditMode(sender, e) {
    var grid = $find($('[id$=whdgArticulos]').attr('id'));
    var gridView = grid.get_gridView();
    var k
    //Al editar un número, entra dos veces. La segunda vez no interesa que se vuelva a hacer. En CellClick, cada vez que se edite un número, se marca como undefined
    if (ComprobacionesOK == undefined) {
        ComprobacionesOK = true;
        var RecuperarValorInicial = false;
        var hid_HaSidoEditada = document.getElementById("<%=hid_HaSidoEditada.clientID %>");

        NUMInicial = gridView.get_rows().get_row(e.getCell().get_row().get_index()).get_cellByColumnKey("Key_NUM_LINEA").get_value();
        NUMActual = e.get_displayText()
        if (NUMActual != "") {
            if ((NUMActual >= 1) && (NUMActual <= 999)) {
                //Comprobar que el número asignado no está ya asignado a otra línea
                if (gridView != null) {
                    //i : Para que no compruebe el valor de la posición que toque con la fila actual (Que no se compare consigo misma)
                    var i = 0;
                    if ($.map(gLineas, function (x) { if ((x.NumLinea == parseInt(NUMActual) && (i != e.getCell().get_row().get_index()))) return x; i++ }).length > 0) {
                        //alert("El número de línea indicado ya está asignado");
                        alert(TextosJScript[62]);
                        ComprobacionesOK = false;
                        RecuperarValorInicial = true;
                    }
                }
            } else {
                //NUM no se encuentra entre un rango válido [1-999]
                RecuperarValorInicial = true;
                ComprobacionesOK = false;
            }
        } else {
            //NUM Línea vacio
            RecuperarValorInicial = true;
            ComprobacionesOK = false;
        }

        if (ComprobacionesOK) {
            $([id$ = hid_HaSidoEditada]).val("1")
            $.map(gLineas, function (x) { if (x.ID == e.getCell().get_row().get_cellByColumnKey("Key_IDLINEA").get_value()) return x; })[0].NumLinea = NUMActual;
        }
        if (ComprobacionesOK) {
            e.set_cancel(true);
            //Si las comprobaciones han ido bien, se ha editado. Se realiza el postback para su posterior reordeno
            //sino se introduce el valor inicial en la celda del grid editadase introduce el valor inicial en la celda del grid editada
            var btn = $('[id$=btnArticulosPostBack]').attr('id');
            __doPostBack(btn, JSON.stringify({ NUMActual: NUMActual, NUMInicial: NUMInicial }));
            ModoEdicion = true;
        }
    }
};
//#endregion
//#region Gestion Errores

function mostrarError() {
    $('#divError').css('z-index', '200002');
    $('#divError').show();
    MostrarFondoPopUp();
    CentrarPopUp($('#divError'));
}
function mostrarErrorPartida() {
    $('#divErrorPartida').css('z-index', '200002');
    $('#divErrorPartida').show();
    MostrarFondoPopUp();
    CentrarPopUp($('#divErrorPartida'));
}
function mostrarErrorAviso() {
    $('#divErrorAviso').css('z-index', '200002');
    $('#divErrorAviso').show();
    MostrarFondoPopUp();
    CentrarPopUp($('#divErrorAviso'));
}
function mostrarOcultarError(texto, Pluri) {
    $('#textError').empty();
    $('#textErrorCab').empty();
    if (texto !== "") {
        MostrarFondoPopUp();
        var mensaje =
            [{
                valor: texto
            }];

        if (Pluri == true) {
            $('#textError').html($('#MensajeError').tmpl(mensaje)[0].innerText);
        } else {
            $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
        }

        $('#divError').css('z-index', '200002');
        $('#divError').show();
        CentrarPopUp($('#divError'));
    }

    $('[id$=panelUpdateProgress]').hide();
    $('[id$=backgroundElement]').hide();
}
function OcultarError() {
    $('#lblErrorTexto').text('');
    $('#divError').hide();
    $('[id$=panelUpdateProgress]').hide();
    $('[id$=backgroundElement]').hide();
    OcultarFondoPopUp();
}
//#endregion
//Calculamos los totales de Costes y Descuentos de la cabecera
function calcularTotalesInicio() {
    var bHayCostes, bHayDescuentos;
    bHayCostes = false;
    bHayDescuentos = false;
    var comboVal = $("#CmbBoxTipoPedido").val().toString();
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Cargar_Costes_Descuentos',
        contentType: "application/json; charset=utf-8",
        data: "{'iDesdeFavorito': '" + bModificarFavorito + "','sIden':'" + sID + "', 'sIDTipoPedido':'" + comboVal.toString() + "',EsPedidoAbierto:" + EsPedidoAbierto + "}",
        dataType: "json",
        async: false
    })).done(function (msg) {
        var localEmisionPedido = $.parseJSON(msg.d);
        var ImporteFijo = 0;
        var GrupoCosteDescuentoAnt = "";

        gEmisionPedido.Costes = localEmisionPedido.Costes;

        if (gEmisionPedido.Costes.length !== 0) { //Si hay atributos calculamos totales            
            $.each(gEmisionPedido.Costes, function () {
                bHayCostes = true;
                if (this.Operacion == "+") {
                    if (this.Valor == undefined) {
                        var Importe = 0;
                    } else {
                        var Importe = this.Valor;
                    }
                } else {
                    if (this.Valor == undefined) {
                        var Importe = 0;
                    } else {
                        var Importe = parseFloat($('#lblImpBruto_hdd').text());
                        Importe = Importe * (this.Valor / 100);
                    }
                }

                this.Importe = Importe;

                if (this.GrupoCosteDescuento == undefined) {
                    var GrupoCosteDescuento = "";
                } else {
                    var GrupoCosteDescuento = this.GrupoCosteDescuento;
                }
                if (this.Inicio == undefined) {
                    if (GrupoCosteDescuentoAnt !== "" && this.GrupoCosteDescuento !== undefined) {
                        if (this.GrupoCosteDescuento == GrupoCosteDescuentoAnt) {
                            Inicio = "1";
                        } else {
                            Inicio = "0";
                        }
                    } else {
                        Inicio = "0";
                    }
                } else {
                    Inicio = this.Inicio;
                };
                this.Inicio = Inicio;
                if (this.Seleccionado == undefined) {
                    var Seleccionado = false;
                    this.Seleccionado = Seleccionado;
                }
                this.Moneda = sMoneda;
                GrupoCosteDescuentoAnt = GrupoCosteDescuento;
                switch (this.TipoCosteDescuento) {
                    case 0:
                        ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                        break
                    case 1:
                        if (this.Seleccionado == true) {
                            ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                        }
                        if ($('#lblCostes').text().indexOf("*") <= 0) {
                            $('#lblCostes').text($('#lblCostes').text() + " (*)");
                            $('#pesCostes').css('color', '#ab0020');
                        }
                        break;
                    case 2:
                        if (this.Seleccionado == true) {
                            ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                        }
                        break;
                    case 3:
                        if (this.Seleccionado == true) {
                            ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                        }
                        break;
                }
            });
        }
        if (bHayCostes == false) $('#pesCostes').hide();
        else $('#pesCostes').css("display", "inline-block");

        $('#lblCosteCabeceraTotal_hdd').text(ImporteFijo);
        $('#lblCosteCabeceraTotal').text(formatNumber(ImporteFijo) + " " + sMoneda);
        ImporteFijo = 0;
        gEmisionPedido.Descuentos = localEmisionPedido.Descuentos;
        if (gEmisionPedido.Descuentos.length !== 0) { //Si hay atributos calculamos totales            
            $.each(gEmisionPedido.Descuentos, function () {
                bHayDescuentos = true;

                if (this.Operacion == "-") {
                    if (this.Valor == undefined) {
                        var Importe = 0;
                    } else {
                        var Importe = this.Valor;
                    }
                } else {
                    if (this.Valor == undefined) {
                        var Importe = 0;
                    } else {
                        var Importe = parseFloat($('#lblImpBruto_hdd').text());
                        Importe = Importe * (this.Valor / 100);
                    }
                }

                this.Importe = Importe;

                if (this.GrupoCosteDescuento == undefined) {
                    var GrupoCosteDescuento = "";
                } else {
                    var GrupoCosteDescuento = this.GrupoCosteDescuento;
                }
                if (this.Inicio == undefined) {
                    if (GrupoCosteDescuentoAnt !== "" && this.GrupoCosteDescuento !== undefined) {
                        if (this.GrupoCosteDescuento == GrupoCosteDescuentoAnt) {
                            Inicio = "1";
                        } else {
                            Inicio = "0";
                        }
                    } else {
                        Inicio = "0";
                    }
                } else {
                    Inicio = this.Inicio;
                }
                this.Inicio = Inicio;
                if (this.Seleccionado == undefined) {
                    var Seleccionado = false;
                    this.Seleccionado = Seleccionado;
                }
                this.Moneda = sMoneda;
                GrupoCosteDescuentoAnt = GrupoCosteDescuento;
                switch (this.TipoCosteDescuento) {
                    case 0:
                        ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                        break;
                    case 1:
                        if (this.Seleccionado == true) {
                            ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                        }
                        if ($('#lblDescuentos').text().indexOf("*") <= 0) {
                            $('#lblDescuentos').text($('#lblDescuentos').text() + " (*)");
                            $('#pesDescuentos').css('color', '#ab0020');
                        }
                        break;
                    case 2:
                        if (this.Seleccionado == true) {
                            ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                        }
                        break;
                    case 3:
                        if (this.Seleccionado == true) {
                            ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                        }
                        break;
                }
            });
        }
        if (bHayDescuentos == false) $('#pesDescuentos').hide();
        else $('#pesDescuentos').css("display", "inline-block");

        $('#lblDescuentosCabeceraTotal_hdd').text(ImporteFijo);
        $('#lblDescuentosCabeceraTotal').text(formatNumber(ImporteFijo) + " " + sMoneda);
        calculoTotalCabecera();
    });
}
//Funcion que nos muestra la pestaÃ±a de Costes de la linea seleccionada
function mostrarLineaCostes(fila, codLinea) {
    iFilaSeleccionada = fila;
    iCodFilaSeleccionada = codLinea;
    $('#pesCostesLinea').click();
}
//Funcion que nos muestra la pestaÃ±a de Descuentos de la linea seleccionada
function mostrarLineaDescuentos(fila, codLinea) {
    iFilaSeleccionada = fila;
    iCodFilaSeleccionada = codLinea;
    $('#pesDescuentosLinea').click();
}
//Cargamos el grid de los costes de la linea
function cargarCostesGrid(linea) {
    var ImporteFijo = 0;
    var ImporteFijoLinea = 0;
    var GrupoCosteDescuentoAnt = "";

    if (linea.Costes.length !== 0) { //Si hay atributos calculamos totales
        $.each(linea.Costes, function () {
            ImporteFijoLinea = 0;
            var Importe = 0;
            if (this.Operacion == "+") {
                if (this.Valor !== undefined) Importe = this.Valor;
            } else {
                if (this.Valor !== undefined) {
                    Importe = parseFloat($('#lblImpBrutolin_' + linea.ID + '_hdd').text());
                    Importe = Importe * (this.Valor / 100);
                }
            }

            this.Importe = Importe;

            if (this.GrupoCosteDescuento == undefined) var GrupoCosteDescuento = "";
            else var GrupoCosteDescuento = this.GrupoCosteDescuento;

            if (this.Inicio == undefined) {
                if (GrupoCosteDescuentoAnt !== "" && this.GrupoCosteDescuento !== undefined) {
                    if (this.GrupoCosteDescuento == GrupoCosteDescuentoAnt) {
                        Inicio = "1";
                    } else {
                        Inicio = "0";
                    }
                } else {
                    Inicio = "0";
                }
            } else {
                Inicio = this.Inicio;
            }
            this.Inicio = Inicio;
            if (this.Seleccionado == undefined) {
                var Seleccionado = false;
                this.Seleccionado = Seleccionado;
            }
            this.Moneda = sMoneda;
            GrupoCosteDescuentoAnt = GrupoCosteDescuento;
            switch (this.TipoCosteDescuento) {
                case 0:
                    ImporteFijoLinea = parseFloat(ImporteFijoLinea) + parseFloat(this.Importe);
                    ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                    break;
                case 1:
                    if (this.Seleccionado == true) {
                        ImporteFijoLinea = parseFloat(ImporteFijoLinea) + parseFloat(this.Importe);
                        ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                    }
                    if ($('#lblCostesLinea').text().indexOf("*") <= 0) {
                        $('#lblCostesLinea').text($('#lblCostesLinea').text() + " (*)");
                        $('#pesCostesLinea').css('color', '#ab0020');
                    }
                    break;
                case 2:
                    if (this.Seleccionado == true) {
                        ImporteFijoLinea = parseFloat(ImporteFijoLinea) + parseFloat(this.Importe);
                        ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                    }
                    break;
                case 3:
                    if (this.Seleccionado == true) {
                        ImporteFijoLinea = parseFloat(ImporteFijoLinea) + parseFloat(this.Importe);
                        ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                    }
                    break;
            }
        });
    }
    $('#lblCosteTotalLinea_' + linea.ID).text(formatNumber(ImporteFijo) + " " + sMoneda);
    $('#lblCosteTotalLinea_' + linea.ID + '_hdd').text(ImporteFijo);
}
//Cargamos el Grid de descuentos de la linea
function cargarDescuentosGrid(linea) {
    var ImporteFijo = 0;
    var ImporteFijoLinea = 0;
    var GrupoCosteDescuentoAnt = "";

    if (linea.Descuentos.length !== 0) { //Si hay atributos calculamos totales
        $.each(linea.Descuentos, function () {
            ImporteFijoLinea = 0;
            if (this.Operacion == "-") {
                if (this.Valor == undefined) {
                    var Importe = 0;
                } else {
                    var Importe = this.Valor;
                }
            } else {
                if (this.Valor == undefined) {
                    var Importe = 0;
                } else {
                    var Importe = parseFloat($('#lblImpBrutolin_' + linea.ID + '_hdd').text());
                    Importe = Importe * (this.Valor / 100);
                }
            }

            this.Importe = Importe;

            if (this.GrupoCosteDescuento == undefined) var GrupoCosteDescuento = "";
            else var GrupoCosteDescuento = this.GrupoCosteDescuento;

            if (this.Inicio == undefined) {
                if (GrupoCosteDescuentoAnt !== "" && this.GrupoCosteDescuento !== undefined) {
                    if (this.GrupoCosteDescuento == GrupoCosteDescuentoAnt) {
                        Inicio = "1";
                    } else {
                        Inicio = "0";
                    }
                } else {
                    Inicio = "0";
                }
            } else {
                Inicio = this.Inicio;
            }
            this.Inicio = Inicio;
            if (this.Seleccionado == undefined) {
                var Seleccionado = false;
                this.Seleccionado = Seleccionado;
            }
            this.Moneda = sMoneda;
            GrupoCosteDescuentoAnt = GrupoCosteDescuento;
            switch (this.TipoCosteDescuento) {
                case 0:
                    ImporteFijoLinea = parseFloat(ImporteFijoLinea) + parseFloat(this.Importe);
                    ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                    break;
                case 1:
                    if (this.Seleccionado == true) {
                        ImporteFijoLinea = parseFloat(ImporteFijoLinea) + parseFloat(this.Importe);
                        ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                    }
                    if ($('#lblDescuentosLinea').text().indexOf("*") <= 0) {
                        $('#lblDescuentosLinea').text($('#lblDescuentosLinea').text() + " (*)");
                        $('#pesDescuentosLinea').css('color', '#ab0020');
                    }
                    break;
                case 2:
                    if (this.Seleccionado == true) {
                        ImporteFijoLinea = parseFloat(ImporteFijoLinea) + parseFloat(this.Importe);
                        ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                    }
                    break;
                case 3:
                    if (this.Seleccionado == true) {
                        ImporteFijoLinea = parseFloat(ImporteFijoLinea) + parseFloat(this.Importe);
                        ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                    }
                    break;
            }

        });
    }
    $('#lblDescuentoTotalLinea_' + linea.ID).text(formatNumber(ImporteFijo) + " " + sMoneda);
    $('#lblDescuentoTotalLinea_' + linea.ID + '_hdd').text(ImporteFijo);
}
//Cargamos toda la informacion de las lineas
function cargarLineas() {
    var bCoste, bDescuento;
    var ImporteDescuento, ImporteCoste;
    var iLinea;

    iLinea = 0;
    ImporteDescuento = 0;
    ImporteCoste = 0;
    bCoste = false;
    bDescuento = false;

    var comboVal = $("#CmbBoxTipoPedido").val().toString();
    var empresa = 0;
    if (bEmitirFavorito == 1) {
        ModificarFavorito = 2
        CodOrdenEmitir = sIDFavorito
    } else {
        ModificarFavorito = bModificarFavorito
        CodOrdenEmitir = 0
    }
    if ($("#CmbBoxEmpresa").val() !== null) empresa = $("#CmbBoxEmpresa").val();

    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Cargar_Lineas',
        contentType: "application/json; charset=utf-8",
        data: "{'iDesdeFavorito':'" + ModificarFavorito + "','sIden':'" + sID + "', 'sIDTipoPedido':'" + comboVal.toString() + "', 'bHayIntegracion':'" + bHayIntegracionPedidos + "', 'idEmpresa':'" + empresa + "', 'EsPedidoAbierto':" + EsPedidoAbierto + ", 'iCodOrdenEmitir':" + CodOrdenEmitir + "}",
        dataType: "json",
        async: false
    })).done(function (msg) {
        gLineas = $.parseJSON(msg.d);
        if (gLineas.length !== 0) {
            gEmisionPedido.Lineas = gLineas;
            $.each(gLineas, function () {
                var idLinea = this.ID;
                if (this.Costes.length !== 0) {
                    cargarCostesGrid(this);
                    bCoste = true;
                    var ImporteFijo = parseFloat($.trim($('#lblCosteTotalLinea_' + this.ID + '_hdd').text()));
                    this.Costes.Importe = ImporteFijo;
                    ImporteCoste = ImporteCoste + ImporteFijo;
                    $('#lblTotalCostesLinea_hdd').text(ImporteCoste);
                } else {
                    $('#lblCosteTotalLinea_' + this.ID).text(formatNumber(0) + " " + sMoneda);
                    $('#lblCosteTotalLinea_' + this.ID + '_hdd').text("0");
                }
                if (this.Descuentos.length !== 0) {
                    cargarDescuentosGrid(this);
                    bDescuento = true;

                    var ImporteFijo = parseFloat($.trim($('#lblDescuentoTotalLinea_' + this.ID + '_hdd').text()));
                    this.Descuentos.Importe = ImporteFijo;
                    ImporteDescuento = ImporteDescuento + ImporteFijo;
                    $('#lblTotalDescuentosLinea_hdd').text(ImporteDescuento);
                } else {
                    $('#lblDescuentoTotalLinea_' + this.ID).text(formatNumber(0) + " " + sMoneda);
                    $('#lblDescuentoTotalLinea_' + this.ID + '_hdd').text("0");
                }
                //Tengo que cargar los centros de aprovisionamiento de cada linea ya que si solo hay uno hay que seleccionarlo
                //y si no se asigna no se guarda o salta la validacion de centro de aprovisionamiento   
                Linea = this;
                if (gDatosGenerales.UsarOrgCompras) cargarCentrosAprovisionamiento(gDatosGenerales.OrgCompras);

                //Pasar a fecha las fechas de los planes de entrega
                $.each(this.PlanEntrega, function () {
                    this.FechaEntrega = eval("new " + this.FechaEntrega.slice(1, -1));
                });
                //Tengo q cargar el almacen de cada linea ya que si solo hay uno hay que seleccionarlo 
                //y si no se asigna no se guarda o salta la validacion de almacen
                if (this.IdAlmacen == 0) {
                    CargaAlmacenUnico(this.Dest, this, this.CentroAprovisionamiento);
                }
            });
            //Para mostrar el gestor tomamos los datos de la primera linea. Segun tarea 2470 solo puede haber un gestor para el pedido,
            //es decir, todas las partidas imputadas a las lineas del pedido deben de tener mismo gestor, que es unico por partida.
            var lineaPartida = gEmisionPedido.Lineas[0];
            if (lineaPartida.Pres5_ImportesImputados_EP[0] !== undefined)
                if (lineaPartida.Pres5_ImportesImputados_EP[0].Partida.NIV0 !== null)
                    ComprobarGestorLinea(lineaPartida.Pres5_ImportesImputados_EP[0].Partida.NIV0 + '#' + lineaPartida.Pres5_ImportesImputados_EP[0].Partida.NIV1 + '#' + lineaPartida.Pres5_ImportesImputados_EP[0].Partida.NIV2 + '#' + lineaPartida.Pres5_ImportesImputados_EP[0].Partida.NIV3 + '#' + lineaPartida.Pres5_ImportesImputados_EP[0].Partida.NIV4);

            //Cargamos los destinos si alguna de las lineas tiene destino no vacio.
            if ($.map(gLineas, function (x) { if (x.Dest !== '') return x; }).length > 0 && !gDatosDestinos) {
                CargarDestinos();
            }
            cargarReceptorInicio();
            recalcularImportes();
        }

        if (bCoste !== true) $('#pesCostesLinea').hide();
        else $('#pesCostesLinea').css("display", "inline-block");

        if (bDescuento !== true) $('#pesDescuentosLinea').hide();
        else $('#pesDescuentosLinea').css("display", "inline-block");
    });
};
function CargarDestinos() {
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarDatosDestinos',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
    })).done(function (msg) {
        gDatosDestinos = $.parseJSON(msg.d);
    });
}
function cargarLineaPedido() {
    gLineas = gEmisionPedido.Lineas;
    Linea = $.grep(gLineas, function (x) { return (x.ID == iCodFilaSeleccionada) })[0];
    devolverTipoLinea(Linea);
    var tbCtroCoste = $('#tbCtroCoste');
    var tbPPres = $('#tbPPres');
    tbCtroCoste.css('background-color', '');
    tbPPres.css('background-color', '');
    tbCtroCoste.val("");
    tbPPres.val("");

    $('#tbCtroCoste_Hidden').val("");
    $('#tbPPres_Hidden').val("");
    $("#textObserLin").val(Linea.Obs);
    $('#txtDesvioRecepcion').val(Linea.DesvioRecepcion);

    srtLinea = ("00" + iFilaSeleccionada.toString()).slice(-3);
    if ((bMostrarCodigoArt == true) && (Linea.Cod !== '')) cabLinea = srtLinea + " - " + Linea.Cod + " - " + Linea.Den;
    else cabLinea = srtLinea + " - " + Linea.Den;



    $('#lblLinea').text(cabLinea);
    if (Linea.Dest !== "") {
        if (comprobarDestinoLista(Linea.Dest)) {
            $('#lblDestino').text(Linea.Dest + "-" + Linea.DestDen);
        } else {
            $('#lblDestino').text("");
            Linea.Dest = "";
            Linea.DestDen = "";
        }
    } else $('#lblDestino').text("");

    if (bActivos == true && (gTipoLinea.Concepto !== 0 && (gTipoLinea.Concepto !== 2 || gTipoLinea.TipoRecepcion !== 1))) {
        $('#Fila8Tabla1').css("visibility", "");
        if (Linea.Activo !== 0) {
            $("#tbActivosLinea_Hidden").val(Linea.Activo);
            $("#tbActivosLinea").val(Linea.DescripcionActivo);
            $("#tbActivosLinea").css('background-color', '#D8E4BC');
        } else {
            $("#tbActivosLinea_Hidden").val(0);
            $("#tbActivosLinea").val("");
            $("#tbActivosLinea").css('background-color', '');
        }
        if (gTipoLinea.Concepto == 1) {
            $("#lblActivoDesc").text("(*) " + TextosJScript[59]);
        } else {
            $("#lblActivoDesc").text(TextosJScript[59]);
        }
    } else {
        Linea.Activo = 0;
        Linea.DescripcionActivo = "";
        $("#tbActivosLinea_Hidden").val(0);
        $("#tbActivosLinea").val("");
        $('#Fila8Tabla1').css("visibility", "hidden");
        $('#lblActivoDesc').css("visibility", "hidden");
        $('#tbActivosLinea').css("visibility", "hidden");
        $('#imgActivo').css("visibility", "hidden");
        $('#imgAplicarActivo').css("visibility", "hidden");
    }

    $('[id^=txtFecEntSol]').datepicker({
        showOn: 'both',
        buttonImage: ruta + 'images/colorcalendar.png',
        buttonImageOnly: true,
        buttonText: '',
        dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
        showAnim: 'slideDown'
    }, $.datepicker.regional[UsuLanguageTag]);
    if (Linea.FecEntrega !== null) {
        $('#txtFecEntSol').datepicker('setDate', Linea.FecEntrega);
    } else $('#txtFecEntSol').val("");

    if (Linea.EntregaObl == 1) $('#CBObli').prop('checked', true);
    else $('#CBObli').prop('checked', false);
    redrawAdjuntosLin();
    if ((bAccesoFSSM) && (bImputaPed)) {
        if (Linea.Pres5_ImportesImputados_EP !== null && Linea.Pres5_ImportesImputados_EP[0] !== undefined) {
            var CentroContrato = Linea.Pres5_ImportesImputados_EP[0];
            if ((CentroContrato.CentroCoste !== undefined) && (CentroContrato.CentroCoste.CentroSM.Codigo !== "")) {
                var CentroCoste = CentroContrato.CentroCoste;
                if (CentroCoste == undefined) vaciarCentroCoste();
                else {
                    var CentroSM = CentroCoste.CentroSM;
                    if ((CentroCoste.UON1 !== "" && CentroCoste.UON1 !== null) && CentroSM.Denominacion !== "") {
                        var CentroCoste_visible = CentroCoste.UON1;
                        var CentroCoste_hidden = CentroCoste.UON1;
                        if (CentroCoste.UON2 !== "" && CentroCoste.UON2 !== null) {
                            var CentroCoste_visible = CentroCoste.UON2;
                            var CentroCoste_hidden = CentroCoste_hidden + "@@" + CentroCoste.UON2;
                            if (CentroCoste.UON3 !== "" && CentroCoste.UON3 !== null) {
                                var CentroCoste_visible = CentroCoste.UON3;
                                var CentroCoste_hidden = CentroCoste_hidden + "@@" + CentroCoste.UON3;
                                if (CentroCoste.UON4 !== "" && CentroCoste.UON4 !== null) {
                                    var CentroCoste_visible = CentroCoste.UON4;
                                    var CentroCoste_hidden = CentroCoste_hidden + "@@" + CentroCoste.UON4;
                                }
                            }
                        }
                        $('#tbCtroCoste').val(CentroCoste_visible + " - " + CentroSM.Denominacion);
                        $('#tbCtroCoste_Hidden').val(CentroSM.Codigo + "@@" + CentroCoste_hidden);

                        gCentroContrato = CentroContrato;
                        if (gCentroContrato.CentroCoste.CentroSM.PartidasPresupuestarias !== undefined) {
                            gCentroContrato.CentroCoste.CentroSM.PartidasPresupuestarias = null;
                        }
                        ComprobarCentroContrato(1);
                        CentroContrato = gCentroContrato;
                    } else {
                        vaciarCentroCoste();
                    }
                }

                var Contrato = CentroContrato.Partida;
                if (Contrato == undefined) {
                    vaciarPartida();
                } else {
                    if ((Contrato.NIV0 !== "" && Contrato.NIV0 !== null) && (Contrato.Denominacion !== "" && Contrato.Denominacion !== null)) {
                        var Contrato_hidden = Contrato.NIV0;
                        var Contrato_visible = Contrato.NIV0;
                        if (Contrato.NIV1 !== "" && Contrato.NIV1 !== null) {
                            Contrato_hidden = Contrato_hidden + "@@" + Contrato.NIV1;
                            Contrato_visible = Contrato.NIV1;
                            if (Contrato.NIV2 !== "" && Contrato.NIV2 !== null) {
                                Contrato_hidden = Contrato_hidden + "@@" + Contrato.NIV2;
                                Contrato_visible = Contrato.NIV2;
                                if (Contrato.NIV3 !== "" && Contrato.NIV3 !== null) {
                                    Contrato_hidden = Contrato_hidden + "@@" + Contrato.NIV3;
                                    Contrato_visible = Contrato.NIV3;
                                    if (Contrato.NIV4 !== "" && Contrato.NIV4 !== null) {
                                        Contrato_hidden = Contrato_hidden + "@@" + Contrato.NIV4;
                                        Contrato_visible = Contrato.NIV4;
                                    }
                                }
                            }
                        }

                        var strFechaIni = Contrato.FechaInicioPresupuesto;
                        if (Contrato.FechaInicioPresupuesto.toString().toLowerCase().indexOf("date") >= 0) {
                            var fechaIni = eval("new " + Contrato.FechaInicioPresupuesto.slice(1, -1));
                            strFechaIni = fechaIni.format(UsuMask);
                        }
                        var strFechaFin = Contrato.FechaFinPresupuesto;
                        if (Contrato.FechaInicioPresupuesto.toString().toLowerCase().indexOf("date") >= 0) {
                            var fechaFin = eval("new " + Contrato.FechaFinPresupuesto.slice(1, -1));
                            strFechaFin = fechaFin.format(UsuMask);
                        }
                        Contrato_visible = Contrato_visible + " - " + Contrato.Denominacion + " - (" + strFechaIni + " - " + strFechaFin + ")";
                        $('#tbPPres').val(Contrato_visible);
                        $('#tbPPres_Hidden').val(Contrato_hidden);
                    } else {
                        vaciarPartida();
                    }
                }
            } else comprobarCentroUnico();
        } else {
            vaciarCentroCoste();
            vaciarPartida();
            vaciarActivo();
        };
    };
    if (gDatosGenerales.GS_Pres1.Activo) {
        $('#txtCentroCoste1').val(Linea.Pres1Den);
        $('#imgCentroCoste1').data('valor', Linea.Pres1);
    };
    if (gDatosGenerales.GS_Pres2.Activo) {
        $('#txtCentroCoste2').val(Linea.Pres2Den);
        $('#imgCentroCoste2').data('valor', Linea.Pres2);
    };
    if (gDatosGenerales.GS_Pres3.Activo) {
        $('#txtCentroCoste3').val(Linea.Pres3Den);
        $('#imgCentroCoste3').data('valor', Linea.Pres3);
    };
    if (gDatosGenerales.GS_Pres4.Activo) {
        $('#txtCentroCoste4').val(Linea.Pres4Den);
        $('#imgCentroCoste4').data('valor', Linea.Pres4);
    };
    if (gDatosGenerales.UsarOrgCompras) $('#cboCentroOrgCompra option[value="' + Linea.CentroAprovisionamiento + '"]').prop('selected', true);
}
function ComprobarGestorLinea(pres5) {
    var lblGestor = $('#LblGestorCab');
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/seleccionaGestorWM',
        contentType: "application/json; charset=utf-8",
        data: "{'Partida':'" + pres5 + "'}",
        dataType: "json",
        async: false
    })).done(function (msg) {
        $('#LblGestorCab').text(msg.d);
    });
}
function infoProve(sender, e) {
    var posicionX, posicionY;
    posicionX = $('#FSNLinkProve').position().left + ($('#FSNLinkProve').innerWidth() / 4);
    posicionY = $('#FSNLinkProve').position().top + 20;

    FSNMostrarPanel(ProveAnimationClientID, e, ProveDynamicPopulateClienteID, $('#FSNLinkProve').val(), null, null, posicionX, posicionY);
}
function infoDire(sender, e) {
    if ($('#lblCodDireccion').text() !== "0") {
        var posicionX, posicionY;
        posicionX = $('#im').position().left + ($('#im').innerWidth() / 4);
        posicionY = $('#im').position().top + 20;
        FSNMostrarPanel(DireAnimationClientID, e, DireDynamicPopulateClienteID, $('#lblCodDireccion').text(), null, null, posicionX, posicionY);
    }
}
function cargarProveerdoresERP(emp, codprove, coderp) {
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarProveedoresERP',
        data: "{'lEmpresa':'" + emp + "','sProve':'" + codprove + "', 'sCodERP':'" + coderp + "'}",
        contentType: "application/json; utf-8",
        dataType: "json",
        async: false
    })).done(function (msg) {
        bdevolverCodErp = msg.d[0];
        var prove = $.parseJSON(msg.d[1]);
        $("#CmbBoxProvesErp option").remove();
        var optionsProvesErp = $('#CmbBoxProvesErp').prop('options');
        if (prove.length == 0) {
            optionsProvesErp[optionsProvesErp.length] = new Option('', 0);
            $('#btnInfo').hide();
        } else {
            $.each(prove, function () {
                optionsProvesErp[optionsProvesErp.length] = new Option(this.Den, this.Cod, prove.length == 1, prove.length == 1);
            });
            if ($("#CmbBoxProvesErp option:selected").length == 0) $('#btnInfo').hide();
            else $('#btnInfo').show();

            if (EsPedidoAbierto || prove.length == 1) gDatosGenerales.CodErp = prove[0].Cod;
        }
    });
}
function cargarOrganizacionesCompras(emp) {
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarOrganizacionesCompras',
        data: "{'iEmpresa':'" + emp + "'}",
        contentType: "application/json; utf-8",
        dataType: "json",
        async: false
    })).done(function (msg) {
        $('[id$=CmbBoxOrgCompras]').val('');
        var oOrgCompras = $.parseJSON(msg.d);
        var opcionesOrgCompras = $('[id$=CmbBoxOrgCompras]').prop('options');
        opcionesOrgCompras.length = 0;
        if (oOrgCompras.length > 1) { opcionesOrgCompras[opcionesOrgCompras.length] = new Option('', '') };
        $.each(oOrgCompras, function () {
            opcionesOrgCompras[opcionesOrgCompras.length] = new Option(this.Den, this.Cod);
        });
    });
}
function asignarOrganizacionCompras(gDatosGenerales) {
    var sOrgCompras = gDatosGenerales.OrgCompras;
    if (sOrgCompras == "") {
        if ($('[id$=CmbBoxOrgCompras]').val() !== null) sOrgCompras = $('[id$=CmbBoxOrgCompras]').val();
        gDatosGenerales.OrgCompras = sOrgCompras;
    } else $("#CmbBoxOrgCompras").val(sOrgCompras);

    //Establecer centros de aprovisionamiento
    if (gLineas != undefined) {
        for (var j = 0; j < gLineas.length; j++) {
            asignarCentroAprovisionamiento(gLineas[j]);
        }
    }
}
function cargarCentrosAprovisionamiento(OrgCompras) {
    var Art4, Anyo, Gmn1, Proce, Item;
    $('#cboCentroOrgCompra option').remove();
    var optionsCentroAprovisionamiento = $('#cboCentroOrgCompra').prop('options');
    if (OrgCompras != undefined && OrgCompras != "") {
        if (Linea.Cod == undefined) Art4 = '';
        else Art4 = Linea.Cod;
        if (Linea.AnyoProce == undefined) Anyo = 0;
        else Anyo = Linea.AnyoProce;
        if (Linea.GMN1Proce == undefined) Gmn1 = '';
        else Gmn1 = Linea.GMN1Proce;
        if (Linea.Proce == undefined) Proce = 0;
        else Proce = Linea.Proce;
        if (Linea.Item == undefined) Item = 0;
        else Item = Linea.Item;
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Cargar_Centros_Aprovisionamiento',
            contentType: "application/json; charset=utf-8",
            data: "{'OrgCompras': '" + gDatosGenerales.OrgCompras + "','Art4':'" + Art4 + "','Anyo':'" + Anyo + "','Gmn1':'" + Gmn1 + "','Proce':'" + Proce + "','Item':'" + Item + "'}",
            dataType: "json",
            async: false
        })).done(function (msg) {
            var centrosAprovisionamiento = $.parseJSON(msg.d);
            if (centrosAprovisionamiento.length == 1) Linea.CentroAprovisionamiento = centrosAprovisionamiento[0].cod;
            else optionsCentroAprovisionamiento[optionsCentroAprovisionamiento.length] = new Option('', '', false, false);
            $.each(centrosAprovisionamiento, function () {
                optionsCentroAprovisionamiento[optionsCentroAprovisionamiento.length] = new Option(this.cod + " - " + this.den, this.cod, (this.cod == Linea.CentroAprovisionamiento), (this.cod == Linea.CentroAprovisionamiento));
            });
        });
    }
}
function asignarCentroAprovisionamiento(LineaSel) {
    var idSeleccionado = $('#cboCentroOrgCompra').val();
    if (idSeleccionado != undefined && idSeleccionado != "") {
        var denSeleccionado = $("#cboCentroOrgCompra option[value='" + idSeleccionado + "']").text();
        denSeleccionado = denSeleccionado.substr(idSeleccionado.length + 3, denSeleccionado.length - (idSeleccionado.length + 3));

        LineaSel.CentroAprovisionamiento = idSeleccionado;
        LineaSel.CentroAprovisionamiento_Den = denSeleccionado;

        //Cargar almacenes del centro        
        var bAlmacenes = comprobarAlmacenes(null, LineaSel, idSeleccionado);
        //Demo Berge 2017/16. Tras devoluciÃ³n quedamos en q Tipo Pedido Manda. Si Oblig-> Lo veras y Oblig. Luego NO emitira, pero ver lo ves.
        var DebeEstar = false;
        if (gEmisionPedido.DatosGenerales.Tipo !== null) {
            DebeEstar = (gEmisionPedido.DatosGenerales.Tipo.Almacenable == 1)
        }
        if ((bAlmacenes == true) || (DebeEstar)) {
            $('[id$=LblAlmacen]').show();
            $('#cboAlmacen').show();
            $('#BtnCopiarAlmacen').css("display", "inline-block");
        } else {
            $('[id$=LblAlmacen]').hide();
            $('#BtnCopiarAlmacen').hide();
            $('#cboAlmacen').hide();

            LineaSel.IdAlmacen = 0;
            LineaSel.strAlmacen = "";
        }
    } else {
        LineaSel.CentroAprovisionamiento = "";
        LineaSel.CentroAprovisionamiento_Den = "";
        if (bHayIntegracionPedidos && gDatosGenerales.UsarOrgCompras) {
            LineaSel.IdAlmacen = 0;
            if (Linea.TipoArticulo.TipoAlmacenamiento == 0) {
                if ($('#cboAlmacen').is(':visible')) {
                    $('#cboAlmacen').fsCombo('destroy');
                    $('[id$=LblAlmacen]').hide();
                    $('#BtnCopiarAlmacen').hide();
                    $('#cboAlmacen').hide();
                }
            }
        }
    }
}
function cargarObservaciones() {
    $.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Cargar_Datos_Generales',
        contentType: "application/json; charset=utf-8",
        data: "{'iDesdeFavorito': '" + desdeFavorito + "','sIden':'" + sID + "','iCodFavorito': '" + sIDFavorito + "','iModiFav': '" + bModificarFavorito + "'}",
        dataType: "json",
        async: false
    }).done(function (msg) {
        var info = $.parseJSON(msg.d);
        gDatosGenerales = info.DatosGenerales;
        $("#textObser").val(info.Obs);
    });
}
function cargarArchivos() {
    if (gEmisionPedido.Adjuntos == null || gEmisionPedido.Adjuntos.length == 0) {
        $.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Cargar_Archivos',
            contentType: "application/json; charset=utf-8",
            data: "{'iDesdeFavorito':'" + bModificarFavorito + "','sIden':'" + sID + "'}",
            dataType: "json",
            async: false
        }).done(function (msg) {
            $("#tablaAdjuntos").html("");
            gArchivos = $.parseJSON(msg.d);
            gEmisionPedido.Adjuntos = gArchivos;
            $('#filaDocumento').tmpl(gArchivos).appendTo($('#tablaAdjuntos'));
        });
    }
}
function cargarArchivosLinea() {
    $("#tablaAdjuntosLin").html("");
    if (Linea.Adjuntos == null || Linea.Adjuntos.length == 0) {
        $.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Cargar_ArchivosLinea',
            contentType: "application/json; charset=utf-8",
            data: "{'iDesdeFavorito':'" + bModificarFavorito + "','sIden':'" + sID + "', 'sIdLinea':'" + Linea.ID + "'}",
            dataType: "json",
            async: false
        }).done(function (msg) {
            gArchivos = $.parseJSON(msg.d);
            Linea.Adjuntos = gArchivos;
            $('#filaDocumentoLin').tmpl(gArchivos).appendTo($('#tablaAdjuntosLin'));
        });
    } else {
        $("#tablaAdjuntosLin").html("");
        gArchivos = Linea.Adjuntos;
        $('#filaDocumentoLin').tmpl(gArchivos).appendTo($('#tablaAdjuntosLin'));
    }
}
function cargarReceptorInicio() {
    var lineasEnvio = [];
    var conta = 0;
    var bVaciar = true;
    $("#wddRecept").empty();
    $.each(gEmisionPedido.Lineas, function () {
        lineasEnvio.push(this.Dest);
        conta = conta + 1;
    });
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Cargar_Combo_Receptores',
        data: "{'sDestinos':'" + lineasEnvio + "'}",
        contentType: "application/json; utf-8",
        dataType: "json",
        async: false
    })).done(function (msg) {
        recep = $.parseJSON(msg.d);
        if (idRecept !== "" && bCargadoInicio == true) {
            var temp = new Option("", "");
            $("#wddRecept")[0].options[$("#wddRecept")[0].options.length] = temp;
            $.each(recep, function () {
                var temp = new Option(this.NomApe, this.Cod);
                $("#wddRecept")[0].options[$("#wddRecept")[0].options.length] = temp;
                if (this.Cod == idRecept) $("#wddRecept").val(idRecept).attr("selected", "selected");
            });
            bVaciar = false;
        } else {
            var temp = new Option("", "");
            var bExiste = false;
            $("#wddRecept")[0].options[$("#wddRecept")[0].options.length] = temp;
            if (idRecept !== "") {
                $.each(recep, function () {
                    var temp = new Option(this.NomApe, this.Cod);
                    $("#wddRecept")[0].options[$("#wddRecept")[0].options.length] = temp;
                    if (this.Cod == idRecept) {
                        bExiste = true;
                    }
                });
            } else {
                var idReceptorInicio = sCodPersona;
                $.each(recep, function () {
                    var temp = new Option(this.NomApe, this.Cod);
                    $("#wddRecept")[0].options[$("#wddRecept")[0].options.length] = temp;
                    if (this.Cod == idReceptorInicio) {
                        bExiste = true;
                        idRecept = idReceptorInicio;
                    }
                });
            }
            bVaciar = false;
            if (bExiste == true) $("#wddRecept").val(idRecept).attr("selected", "selected");
        }
    });
}
function calculoTotalCabecera() {
    calcularImpuestos();
    recalcularImportes();
}
function recalcularLineaCosteDescuento() {
    var idLinea;
    var bCoste, bDescuento;
    var ImporteDescuento, ImporteCoste;
    var iLinea;
    iLinea = 0;
    ImporteDescuento = 0;
    ImporteCoste = 0;
    bCoste = false;
    bDescuento = false;
    gLineas = gEmisionPedido.Lineas;
    $.each(gLineas, function () {
        idLinea = this.ID;
        $('#lblDescuentoTotalLinea_' + idLinea + '_hdd').text("0");
        $('#lblCosteTotalLinea_' + idLinea + '_hdd').text("0");
        if (this.Costes.length !== 0) {
            cargarCostesGrid(this);
            bCoste = true;
            var ImporteFijo = parseFloat($.trim($('#lblCosteTotalLinea_' + this.ID + '_hdd').text()));
            this.Costes.Importe = ImporteFijo;
            ImporteCoste = ImporteCoste + ImporteFijo;
            $('#lblTotalCostesLinea_hdd').text(ImporteCoste);
        } else {
            $('#lblCosteTotalLinea_' + this.ID).text(formatNumber(0) + " " + sMoneda);
            $('#lblCosteTotalLinea_' + this.ID + '_hdd').text("0");
        }
        if (this.Descuentos.length !== 0) {
            cargarDescuentosGrid(this);
            bDescuento = true;

            var ImporteFijo = parseFloat($.trim($('#lblDescuentoTotalLinea_' + this.ID + '_hdd').text()));
            this.Descuentos.Importe = ImporteFijo;
            ImporteDescuento = ImporteDescuento + ImporteFijo;
            $('#lblTotalDescuentosLinea_hdd').text(ImporteDescuento);
        } else {
            $('#lblDescuentoTotalLinea_' + this.ID).text(formatNumber(0) + " " + sMoneda);
            $('#lblDescuentoTotalLinea_' + this.ID + '_hdd').text("0");
        }
    });
    recalcularImportes();

    if (bCoste !== true) $('#pesCostesLinea').hide();
    else $('#pesCostesLinea').css("display", "inline-block");

    if (bDescuento !== true) $('#pesDescuentosLinea').hide();
    else $('#pesDescuentosLinea').css("display", "inline-block");
}
//funcion que carga los atributos a nivel de Item(Panel edicion Item)
function CargarAtributosPedido(mostrarAtributos) {
    if (gAtributos == null) { //Si no hay atributos cargados los carga y los pinta
        var comboVal = $("#CmbBoxTipoPedido").val().toString();
        var empresa = 0;
        if ($("#CmbBoxEmpresa").val() !== null) empresa = $("#CmbBoxEmpresa").val();
        if (bEmitirFavorito == 1) {
            ModificarFavorito = 2
            CodOrdenEmitir = sIDFavorito
        } else {
            ModificarFavorito = bModificarFavorito
            CodOrdenEmitir = 0
        }
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarAtributosPedido',
            contentType: "application/json; charset=utf-8",
            data: "{'iDesdeFavorito':'" + ModificarFavorito + "','iCodPedido':'" + sID + "','sIDTipoPedido':'" + comboVal.toString() + "', 'bHayIntegracion':'" + bHayIntegracionPedidos + "', 'iEmpresa':'" + empresa + "', 'EsPedidoAbierto':" + EsPedidoAbierto + ", 'iCodOrdenEmitir':" + CodOrdenEmitir + "}",
            dataType: "json",
            async: false
        })).done(function (msg) {
            gAtributos = $.parseJSON(msg.d);
            gEmisionPedido.OtrosDatos = gAtributos;
            if (gAtributos.length == 0) $('#pesOtrosDatos').hide();
            else $('#pesOtrosDatos').css("display", "inline-block");
            if (mostrarAtributos) MostrarAtributos();
        });
    } else {
        if (bCambioEmpresa == true) { //Si ya hay atributos cargados pero ha habido cambio de empresa, se actualizan los atributos y se pintan, sino solo mostrar pestaña
            gAtributos = gEmisionPedido.OtrosDatos;
            bCambioEmpresa = false;
            if (mostrarAtributos) MostrarAtributos();
        } else $('[id$=divAtributosItem]').show();
    }
};
function MostrarAtributos() {
    if (gAtributos.length == 0) { //Si no hay atributos ocultamos el Div de Atributos    
        $('[id$=divAtributosItem]').hide();
        $('#pesDatosGenerales').click();
        $('#pesOtrosDatos').hide();
    } else {
        $('#pesOtrosDatos').css("display", "inline-block");
        $('[id$=divAtributosItem]').show();

        var indAtributo = 0;
        //Se eliminan las filas si las hubiese de la tabla(menos la cabecera)
        var table = $("#tblAtributosItem")
        table.find('tr:not(:first)').remove();

        $.each(gAtributos, function () {
            indAtributo += 1;
            if (this.Intro == 1) {
                if (this.ListaExterna) {
                    var valorListaExterna;
                    var oAtributoListaExterna = this;

                    //Tarea "Atrib Por Defecto"
                    //  Al entrar, si es atrib por defecto. VALOR_TEXT ES NULO y VALOR_COD NO ES NULO
                    var atributosLlamada = '';
                    $.each(gEmisionPedido.OtrosDatos, function () {
                        if (atributosLlamada == '')
                            atributosLlamada = this.Id + '@########@' + this.Valor;
                        else {
                            atributosLlamada = atributosLlamada + "##########" + this.Id + '@########@' + this.Valor;
                        }
                    });
                    var sOrgCompras = '';
                    var sCentro = '';
                    if (bHayIntegracionPedidos && gDatosGenerales.UsarOrgCompras) {
                        sOrgCompras = gDatosGenerales.OrgCompras;
                        if (gEmisionPedido.Lineas.length > 0) sCentro = gEmisionPedido.Lineas[0].CentroAprovisionamiento;
                    };

                    if (this.Valor !== '') {
                        if (this.Valores.length == 1) {
                            if (this.Valores[0].Valor == '') {
                                var Resultado = ''
                                var bOk = false;

                                var param = JSON.stringify({ 'Empresa': gDatosGenerales.Emp, 'IdAtributoListaExterna': this.Id, 'atributos': atributosLlamada, 'codigoBuscado': this.Valor, 'denominacionBuscado': '', 'buscador': true, 'OrgCompras': sOrgCompras, 'Centro': sCentro });
                                $.when($.ajax({
                                    type: "POST",
                                    url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Obtener_Datos_ListaExterna',
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    data: param,
                                    async: false
                                })).done(function (msg) {
                                    if (msg.d.length == 1) {
                                        bOk = true;
                                        Resultado = msg.d[0].id + ' - ' + msg.d[0].name;
                                    }
                                });

                                if (bOk) {
                                    this.Valores[0].Valor = Resultado;
                                } else {
                                    this.Valor = '';
                                    this.Valores.length = 0;
                                }
                            }
                        }
                    }
                    //---------------------------------------------------------------------------
                    //Lo q había antes de tarea "Atrib Por Defecto"

                    if (this.Valores.length > 0) valorListaExterna = { 'id': this.Valor, 'name': this.Valores[0].Valor };
                    $('#tmplListaExterna').tmpl(this).appendTo($('#tblAtributosItem'));
                    if (!popupListaExternaCargado) {
                        popupListaExternaCargado = true;
                        $.get(rutaFS + 'EP/html/buscadorListaExterna.htm', function (buscadorPopUp) {
                            buscadorPopUp = buscadorPopUp.replace(/src="/gi, 'src="' + ruta);
                            $('body').append(buscadorPopUp);
                        });
                    }
                    var listaExternaAtrib = $('#atribListaExterna_' + this.Id, $("#tblAtributosItem"));
                    var IdAtributoListaExterna = this.Id;
                    listaExternaAtrib.tokenInput(function () {
                        guardarOtrosDatos(true);
                        var atributosLlamada = '';
                        $.each(gEmisionPedido.OtrosDatos, function () {
                            if (atributosLlamada == '')
                                atributosLlamada = this.Id + '@########@' + this.Valor;
                            else {
                                atributosLlamada = atributosLlamada + "##########" + this.Id + '@########@' + this.Valor;
                            }
                        });
                        var sOrgCompras = '';
                        var sCentro = '';
                        if (bHayIntegracionPedidos && gDatosGenerales.UsarOrgCompras) {
                            sOrgCompras = gDatosGenerales.OrgCompras;
                            if (gEmisionPedido.Lineas.length > 0) sCentro = gEmisionPedido.Lineas[0].CentroAprovisionamiento;
                        };

                        return rutaFS + 'EP/EmisionPedido.aspx/Obtener_Datos_ListaExterna?Empresa=' + gDatosGenerales.Emp + '&IdAtributoListaExterna=' + IdAtributoListaExterna + '&atributos=' + atributosLlamada +
                            '&codigoBuscado=&denominacionBuscado=&buscador=false&OrgCompras=' + sOrgCompras + '&Centro=' + sCentro
                    }, {
                            queryParam: 'codigoBuscado',
                            hintText: TextosJScript[63],
                            searchingText: TextosJScript[64],
                            noResultsText: TextosJScript[65],
                            minChars: 3,
                            tokenLimit: 1,
                            resultsLimit: 50,
                            preventDuplicates: true,
                            method: "POST",
                            onReady: function () {
                                if (valorListaExterna) setTimeout(function () { listaExternaAtrib.tokenInput('addRange', [{ 'id': valorListaExterna.id, 'name': valorListaExterna.name }]) }, 50);
                                //Ponemos el display de la lista inline-block
                                $("ul[class^=token-input-list]", listaExternaAtrib.parent()).css('display', 'inline-block');
                                $("ul[class^=token-input-list]", listaExternaAtrib.parent()).css('vertical-align', 'middle');
                                $("ul[class^=token-input-list]", listaExternaAtrib.parent()).css('max-height', '10em');
                                $("ul[class^=token-input-list]", listaExternaAtrib.parent()).css('overflow-y', 'auto');
                                $("ul[class^=token-input-list]", listaExternaAtrib.parent()).css('width', '90%');
                                //En las ventanas modales el dropdown se debe mostrar en primer plano
                                $("div[class^=token-input-dropdown]").css("z-index", "9999");
                            },
                            onDelete: function () { oAtributoListaExterna.Valor = ''; oAtributoListaExterna.Valores = []; }
                        });
                } else {
                    //Opcion Lista
                    var iSeleccionado = "";
                    $('#tmplComboEP').tmpl(this).appendTo($('#tblAtributosItem'));
                    var item = this;
                    var items = "{"
                    //vamos componiendo con los valores de la combo el objeto Json
                    if (this.Tipo !== 3) {
                        for (i = 0; i <= item.Valores.length - 1; i++) {
                            items += '"' + i + '": { "value":"' + i.toString() + '", "text": "' + item.Valores[i].Valor.toString().replace(/"/g, "'") + '" }, '
                            if (item.Valor !== undefined) {
                                if (item.Valor.toString() == item.Valores[i].Valor) {
                                    iSeleccionado = i;
                                }
                            }
                        }
                    } else {
                        for (i = 0; i <= item.Valores.length - 1; i++) {
                            var fecha = eval("new " + item.Valores[i].Valor.slice(1, -1));
                            var strFecha = fecha.format(UsuMask);
                            items += '"' + i + '": { "value":"' + strFecha + '", "text": "' + strFecha + '" }, '
                        }
                    }
                    items = items.substr(0, items.length - 2) //quitamos la ultima coma que sobra
                    items += "}"

                    var optionsPermiso = { valueField: "value", textField: "text", adjustWidthToSpace: true, dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: false, data: $.parseJSON(items) };
                    $('#cboAtribEP_' + item.Id).fsCombo(optionsPermiso);
                    if (this.Tipo == 3) {
                        if (item.Valor !== null) {
                            if (item.Valor.toString().toLowerCase().indexOf("date") >= 0) {
                                var fecha = eval("new " + item.Valor.slice(1, -1));
                                var strFecha = fecha.format(UsuMask);
                            } else {
                                var strFecha = item.Valor;
                            }
                            $('#cboAtribEP_' + item.Id).fsCombo('selectValue', strFecha);
                        } else {
                            $('#cboAtribEP_' + item.Id).fsCombo('selectValue', item.Valor);
                        }
                    } else {
                        $('#cboAtribEP_' + item.Id).fsCombo('selectValue', iSeleccionado.toString());
                    }
                };
            } else {
                switch (this.Tipo.toString()) {
                    case "1": //Texto
                        $("#tmplCampoTextoEP").tmpl(this).appendTo("#tblAtributosItem");
                        break
                    case "2": //Numerico

                        $("#tmplCampoNumericoEP").tmpl(this).appendTo("#tblAtributosItem");
                        break
                    case "3": //Fecha
                        $("#tmplCampoFechaEP").tmpl(this).appendTo("#tblAtributosItem");
                        var item = this;
                        var idioma
                        switch (TextosJScript[2]) {
                            case 'SPA':
                                idioma = 'es';
                                break;
                            case 'ENG':
                                idioma = 'en';
                                break;
                            case 'GER':
                                idioma = 'de';
                                break;
                            default:
                                idioma = 'es';
                                break;
                        }
                        $('#inputFechaAtribEP_' + item.Id).datepicker({
                            showOn: 'both',
                            buttonImage: ruta + 'images/colorcalendar.png',
                            buttonImageOnly: true,
                            buttonText: '',
                            dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
                            showAnim: 'slideDown'
                        }, $.datepicker.regional[idioma]);
                        if (item.Valor != null && item.Valor != undefined)
                            if (item.Valor.toString().indexOf('Date') != -1) //Viene de BD en milisegundos
                                $('#inputFechaAtribEP_' + this.Id).datepicker('setDate', eval("new " + item.Valor.slice(1, -1)));
                            else
                                $('#inputFechaAtribEP_' + this.Id).datepicker('setDate', item.Valor);
                        break
                    case "4": //Boolean
                        $("#tmplComboEP").tmpl(this).appendTo("#tblAtributosItem");
                        var item = this;
                        var items = "{"
                        //vamos componiendo con los valores de la combo el objeto Json
                        items += '"' + 0 + '": { "value":"' + 1 + '", "text": "' + TextosBoolean[1] + '" }, ';
                        items += '"' + 1 + '": { "value":"' + 0 + '", "text": "' + TextosBoolean[0] + '" } ';
                        items += "}"

                        var optionsPermiso = { valueField: "value", textField: "text", adjustWidthToSpace: true, dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: false, data: $.parseJSON(items) };
                        $('#cboAtribEP_' + item.Id).fsCombo(optionsPermiso);
                        //Se pone el valor si lo tuviera el atributo
                        $('#cboAtribEP_' + item.Id).fsCombo('selectValue', item.Valor.toString());
                        break;
                };
            };
        });
    };
};
function cargarCostes() {
    CargarAtributosCoste();
    var Importe = $('#lblCosteTotal_hdd').text();
    $('#lblCosteCabeceraTotal_hdd').text(Importe);
    $('#lblCosteCabeceraTotal').text(formatNumber(Importe) + " " + sMoneda);
}
function cargarCostesLinea() {
    gLineas = gEmisionPedido.Lineas;
    $.each(gLineas, function () {
        if (this.ID == iCodFilaSeleccionada) {
            Linea = this;
            srtLinea = ("00" + iFilaSeleccionada.toString()).slice(-3);
            if ((bMostrarCodigoArt == true) && (Linea.Cod !== '')) {
                cabLinea = srtLinea + " - " + this.Cod + " - " + this.Den;
            } else {
                cabLinea = srtLinea + " - " + this.Den;
            }
            $('#lblLineaCoste').text(cabLinea);
            if (Linea.Costes == undefined) {
                CargarAtributosCosteLinea();
            } else {
                gCostesLinea = this.Costes;
                pintarCostesLinea();
            }
        };
    });
}
function cargarDescuentosLinea() {
    gLineas = gEmisionPedido.Lineas;
    $.each(gLineas, function () {
        if (this.ID == iCodFilaSeleccionada) {
            Linea = this;
            srtLinea = ("00" + iFilaSeleccionada.toString()).slice(-3);
            if ((bMostrarCodigoArt == true) && (Linea.Cod !== '')) {
                cabLinea = srtLinea + " - " + this.Cod + " - " + this.Den;
            } else {
                cabLinea = srtLinea + " - " + this.Den;
            }
            $('#lblLineaDescuento').text(cabLinea);
            if (Linea.Descuentos == undefined) {
                CargarAtributosDescuentoLinea();
            } else {
                gDescuentosLinea = this.Descuentos;
                pintarDescuentosLinea();
            }
        };
    });
}
function guardarOtrosDatosLineas(excluirListaExterna) {
    if (excluirListaExterna == undefined) excluirListaExterna = false;
    if (Linea.OtrosDatos !== undefined) {
        if (Linea.OtrosDatos !== null) {
            // Guardo atributos de linea
            for (var i = 0; i < Linea.OtrosDatos.length; i++) {
                if (Linea.OtrosDatos[i].Intro == 1) {
                    if (Linea.OtrosDatos[i].ListaExterna && !excluirListaExterna) {
                        if ($('#atribListaExterna_' + Linea.OtrosDatos[i].Id).length) {
                            if ($('#atribListaExterna_' + Linea.OtrosDatos[i].Id).tokenInput('get').length) {
                                var selectedToken = $('#atribListaExterna_' + Linea.OtrosDatos[i].Id).tokenInput('get')[0];
                                Linea.OtrosDatos[i].Valor = selectedToken.id;
                                Linea.OtrosDatos[i].Valores = [];
                                Linea.OtrosDatos[i].Valores.push({ 'Valor': selectedToken.name }); //Aprovechamos que existe un array de valores para guardar el texto de un atributo tipo lista externa
                            }
                        }
                    } else {
                        if ($('#cboAtribLineaEP_' + Linea.OtrosDatos[i].Id).length) {
                            Linea.OtrosDatos[i].Valor = $('#fsComboValueDisplay_cboAtribLineaEP_' + Linea.OtrosDatos[i].Id).val();
                        }
                    }
                } else {
                    switch (Linea.OtrosDatos[i].Tipo.toString()) {
                        case "1":
                            if ($('#inputTextAtribLineaEP_' + Linea.OtrosDatos[i].Id).length) {
                                Linea.OtrosDatos[i].Valor = $('#inputTextAtribLineaEP_' + Linea.OtrosDatos[i].Id).val()
                            }
                            break;
                        case "2":
                            if ($('#inputNumAtribLineaEP_' + Linea.OtrosDatos[i].Id).length) {
                                Linea.OtrosDatos[i].Valor = $('#inputNumAtribLineaEP_' + Linea.OtrosDatos[i].Id).val()
                            }
                            break;
                        case "3":
                            if ($('#inputFechaAtribLineaEP_' + Linea.OtrosDatos[i].Id).length) {
                                Linea.OtrosDatos[i].Valor = $('#inputFechaAtribLineaEP_' + Linea.OtrosDatos[i].Id).datepicker("getDate");
                            }
                            break;
                        case "4":
                            if ($('#cboAtribLineaEP_' + Linea.OtrosDatos[i].Id).length) {
                                if ($('#fsComboValueDisplay_cboAtribLineaEP_' + Linea.OtrosDatos[i].Id).val() == TextosBoolean[0]) { Linea.OtrosDatos[i].Valor = 0; }
                                else { if ($('#fsComboValueDisplay_cboAtribLineaEP_' + Linea.OtrosDatos[i].Id).val() == TextosBoolean[1]) { Linea.OtrosDatos[i].Valor = 1; } }
                            }
                            break;
                    }
                }
            }

        }
    };
    // Guardo los datos de imputación
    var CentroContrato = Linea.Pres5_ImportesImputados_EP[0];
    gCentroContrato = CentroContrato;
    CentroContrato = gCentroContrato;

    if ($('#tbCtroCoste').val() == "") {
        if (CentroContrato == undefined) {
            CentroContrato = JSON.parse(JSON.stringify(Linea.Pres5_ImportesImputados_EP));
            CentroContrato.Id = 1;
            CentroContrato.LineaId = iCodFilaSeleccionada;
            CentroContrato.CentroCoste;
            CentroContrato.Partida;
            var CentroCoste = {};
            CentroCoste = CrearObjCentroCosteVacio(CentroCoste);
            CentroContrato.CentroCoste = CentroCoste;
        } else {
            CentroContrato = CentroContrato;
        }
        Linea.Pres5_ImportesImputados_EP[0] = CentroContrato;
        vaciarPartida();
        vaciarActivo();
    } else {
        Linea.Pres5_ImportesImputados_EP[0] = CentroContrato;
    }
    var CentroContrato = Linea.Pres5_ImportesImputados_EP[0];
    if ($('#tbPPres').val() == "") {
        if (CentroContrato == undefined) {
            CentroContrato = JSON.parse(JSON.stringify(Linea.Pres5_ImportesImputados_EP));
            var Partida = {};
            Partida = CrearObjPartidaVacio(Partida);
            CentroContrato.Partida = Partida;
        } else {
            var Partida = CentroContrato.Partida;
            CentroContrato.Partida = Partida;
        }

        Linea.Pres5_ImportesImputados_EP[0] = CentroContrato;
    } else {
        Linea.Pres5_ImportesImputados_EP[0] = CentroContrato;
    }
    Linea.DesvioRecepcion = parseFloat($("#txtDesvioRecepcion").val());
    // Guardo observaciones y Activos
    Linea.Obs = $("#textObserLin").val();
    if ($("#tbActivosLinea").val() == "") {
        Linea.Activo = 0;
        Linea.DescripcionActivo = "";
    } else {
        Linea.Activo = $("#tbActivosLinea_Hidden").val();
        Linea.DescripcionActivo = $("#tbActivosLinea").val();
    }
    for (var i = 0; i < gLineas.length; i++) {
        if (gLineas[i].ID == Linea.ID) {
            gLineas[i] = Linea;
            break;
        }
    }
    gEmisionPedido.Lineas = gLineas;
}
function guardarCostesLinea() {
    for (var i = 0; i < gLineas.length; i++) {
        if (gLineas[i].ID == Linea.ID) {
            gLineas[i].Costes = gCostesLinea;
            break;
        }
    }
    gEmisionPedido.Lineas = gLineas;
}
function guardarDescuentosLinea() {
    for (var i = 0; i < gLineas.length; i++) {
        if (gLineas[i].ID == Linea.ID) {
            gLineas[i].Descuentos = gDescuentosLinea;
            break;
        }
    }
    gEmisionPedido.Lineas = gLineas;
}
function cargarNuevoTipoPedido() {
    var oTipoPedido;
    var combo = $("#CmbBoxTipoPedido");
    var CmbBoxEmpresa = $("#CmbBoxEmpresa");
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Cargar_Tipo_Pedido',
        contentType: "application/json; charset=utf-8",
        data: "{'iCodPedido':'" + sID + "', 'sIDTipoPedido':'" + combo.val().toString() + "', 'sEmp':'" + CmbBoxEmpresa.val() + "' }",
        dataType: "json",
        async: false
    })).done(function (msg) {
        oTipoPedido = $.parseJSON(msg.d);
        gEmisionPedido.DatosGenerales.Tipo = oTipoPedido;
    });
}
function comprobarTipoPedido() {
    var bValidado;
    var combo = $("#CmbBoxTipoPedido");
    var CmbBoxEmpresa = $("#CmbBoxEmpresa");
    if (oTipo !== combo.val()) {
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Comprobar_TipoPedido',
            contentType: "application/json; charset=utf-8",
            data: "{'iCodPedido':'" + sID + "', 'sIDTipoPedido':'" + combo.val().toString() + "', 'sEmp':'" + CmbBoxEmpresa.val() + "' }",
            dataType: "json",
            async: false
        })).done(function (msg) {
            bValidado = $.parseJSON(msg.d);
        });
        if (bValidado == true) {
            cargarNuevoTipoPedido();
            bValidado = ComprobarTipoPedidoRecepcionable();

            if (bValidado == true) {
                gbError = false;
                OcultarError();
                quitarAvisoEnLinea();
                gAtributos = null;
                $('[id^=aviso]').hide();
                if (combo.val() !== "0") {
                    $('#pesOtrosDatos').css("display", "inline-block");
                } else {
                    $('#pesOtrosDatos').hide();
                }
                return true;
            } else {
                gbError = true;
                mostrarOcultarError(TextosJScript[9]);
                $('#aviso' + bValidado).css("display", "inline-block");
                ponerAvisoEnLinea(bValidado);
                return false;
            }
        } else {
            gbError = true;
            mostrarOcultarError(TextosJScript[9]);
            $('#aviso' + bValidado).css("display", "inline-block");
            $('#aviso' + bValidado).prop('title', TextosJScript[10]);
            ponerAvisoEnLinea(bValidado);
            return false;
        }
    } else {
        bValidado = ComprobarTipoPedidoRecepcionable();
        if (bValidado !== true) {
            gbError = true;
            mostrarOcultarError(TextosJScript[9]);
            $('#aviso' + bValidado).css("display", "inline-block");
            ponerAvisoEnLinea(bValidado);
            return false;
        }
    };
}
function devolverTipoLinea(lLinea) {
    var oTipoLinea;
    if (gEmisionPedido.DatosGenerales.Tipo !== null) {
        oTipoLinea = gEmisionPedido.DatosGenerales.Tipo;
    } else {
        oTipoLinea = null;
    }
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/recuperarTipoLinea',
        contentType: "application/json; charset=utf-8",
        data: "{'TipoPedidoOrden':'" + JSON.stringify(oTipoLinea) + "', 'sLineaCatalogo':'" + lLinea.LineaCatalogo.toString() + "', 'TipoLinea':'" + JSON.stringify(lLinea.TipoArticulo) + "'}",
        dataType: "json",
        async: false
    })).done(function (msg) {
        gTipoLinea = $.parseJSON(msg.d);
    });
}
function comprobarEmpresa(idLinea) {
    var bValidado;
    var CmbBoxEmpresa = $("#CmbBoxEmpresa");
    if (CmbBoxEmpresa.val() !== "") {
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Comprobar_Empresa',
            contentType: "application/json; charset=utf-8",
            data: "{'idLinea':'" + idLinea + "', 'sEmp':'" + CmbBoxEmpresa.val() + "','EsPedidoAbierto':" + EsPedidoAbierto + ", 'sOrgCompras':" + null + ", 'modFav': " + bModificarFavorito + "}",
            dataType: "json",
            async: false
        })).done(function (msg) {
            bValidado = $.parseJSON(msg.d);
        });
        if (bValidado == true) {
            gbError = false;
            return true;
        } else {
            gbError = true;
            mostrarOcultarError(TextosJScript[61]);
            $('#aviso' + bValidado).css("display", "inline-block");
            $('#aviso' + bValidado).prop('title', TextosJScript[35]);
            ponerAvisoEnLinea(bValidado);
            return false;
        }
    }
}
function ComprobarIntegracion() {
    bHayIntegracionPedidos = false;
    var iEmpresa = $("#CmbBoxEmpresa").val();
    if (iEmpresa == "") iEmpresa = 0;

    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Comprobar_Integracion',
        contentType: "application/json; charset=utf-8",
        data: "{'iEmpresa':'" + iEmpresa + "'}",
        dataType: "json",
        async: true
    })).done(function (msg) {
        var bHayIntegracionPedidosAnt = bHayIntegracionPedidos
        bHayIntegracionPedidos = $.parseJSON(msg.d);

        gLineas = gEmisionPedido.Lineas;
        if (gEmisionPedido.OtrosDatos !== undefined) {
            if (gEmisionPedido.OtrosDatos !== null) {
                var lenOtrosDatos = gEmisionPedido.OtrosDatos.length;
                for (var i = 0; i < lenOtrosDatos; i++) {
                    if (gEmisionPedido.OtrosDatos[i].Integracion == true) {
                        gEmisionPedido.OtrosDatos.splice(i, 1);
                        i = i - 1;
                        lenOtrosDatos = lenOtrosDatos - 1;
                    }
                }
            } else CargarAtributosPedido(true);
        }
        for (var j = 0; j < gLineas.length; j++) {
            var lenOtrosDatos = gLineas[j].OtrosDatos.length;
            for (var i = 0; i < lenOtrosDatos; i++) {
                if (gLineas[j].OtrosDatos[i].Integracion == true) {
                    gLineas[j].OtrosDatos.splice(i, 1);
                    i = i - 1;
                    lenOtrosDatos = lenOtrosDatos - 1;
                }
            }
        }
        gEmisionPedido.Lineas = gLineas;
        var empresa = 0;
        gDatosGenerales.OrgCompras = "";
        if ($("#CmbBoxEmpresa").val() !== null) empresa = $("#CmbBoxEmpresa").val();

        if (bHayIntegracionPedidos) {
            //Comprobar si hay que mostrar la organización de compras
            if (gDatosGenerales.UsarOrgCompras == true) {
                $('[id$=CmbBoxOrgCompras]').css("display", "inline");
                $('#lblOrgCompras').css("display", "block");
                cargarOrganizacionesCompras(empresa);
                asignarOrganizacionCompras(gDatosGenerales);
                if (gDatosGenerales.UsarOrgCompras) cargarCentrosAprovisionamiento(gDatosGenerales.OrgCompras);
            }
            else {
                $('[id$=CmbBoxOrgCompras]').hide();
                $('#lblOrgCompras').hide();
            }

            $('#CmbBoxProvesErp').css("display", "inline");
            $('#LblProveErp').css("display", "block");
            cargarProveerdoresERP(empresa, gDatosGenerales.CodProve, gDatosGenerales.CodErp);
            if (EsPedidoAbierto && bdevolverCodErp) $('#CmbBoxProvesErp :not(option[value="' + gDatosGenerales.CodErp + '"])').remove();
            if (!bdevolverCodErp) {
                $('#CmbBoxProvesErp').hide();
                $('#LblProveErp').hide();
                $('#btnInfo').hide();
            }
            CargarAtributosCabeceraIntegracion();
            CargarAtributosLineasIntegracion();
        } else {
            $('#CmbBoxProvesErp').val(0);
            $('#CmbBoxProvesErp').hide();
            $('#LblProveErp').hide();
            $('#btnInfo').hide()
            //Organización de compras
            $('[id$=CmbBoxOrgCompras]').hide();
            $('#lblOrgCompras').hide();
        }
        for (var j = 0; j < gLineas.length; j++) {
            if (gLineas[j].LineaCatalogo !== 0) {
                if (bHayIntegracionPedidos == true && gDatosGenerales.UsarOrgCompras == true && $('[id$=CmbBoxOrgCompras]').val() != null) {
                    //Si hay integración y se usa organizaciones de compras y hay una elegida se valida esta
                    bOrgCompras = comprobarOrganizacionCompras(gLineas[j].ID);
                    if (bOrgCompras) {
                        OcultarError();
                        quitarAvisoEnLineaDef(gLineas[j].ID);
                        OcultarFondoPopUp();
                        $('[id^=aviso]').hide();
                    }
                }
                else {
                    bEmpresa = comprobarEmpresa(gLineas[j].ID);
                    if (bEmpresa == true) {
                        OcultarError();
                        quitarAvisoEnLineaDef(gLineas[j].ID);
                        OcultarFondoPopUp();
                        $('[id^=aviso]').hide();
                    }
                }
            };
        }

        $('#pesDatosGenerales').click();
        $('#pesResumenLineas').click();
        $('#pesOtrosDatos').css("display", "inline-block");
        $('[id$=divAtributosItem]').show();
        OcultarCargando();
    });
}
function comprobarOrganizacionCompras(idLinea) {
    var bValidado;
    var CmbBoxOrgCompras = $('[id$=CmbBoxOrgCompras]');
    if (CmbBoxOrgCompras.val() !== "") {
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Comprobar_Empresa',
            contentType: "application/json; charset=utf-8",
            data: "{'idLinea':'" + idLinea + "', 'sEmp':'" + $('#CmbBoxEmpresa').val() + "','EsPedidoAbierto':" + EsPedidoAbierto + ", 'sOrgCompras':'" + $('[id$=CmbBoxOrgCompras]').val() + "', 'modFav': " + bModificarFavorito + "}",
            dataType: "json",
            async: false
        })).done(function (msg) {
            bValidado = $.parseJSON(msg.d);
        });
        if (bValidado == true) {
            gbError = false;
            gAtributos = null;
            return true;
        } else {
            gbError = true;
            mostrarOcultarError(TextosJScript[91]);
            $('#aviso' + bValidado).css("display", "inline-block");
            $('#aviso' + bValidado).prop('title', TextosJScript[92]);
            ponerAvisoEnLinea(bValidado);
            return false;
        }
    }
}
function CargarAtributosCabeceraIntegracion() {
    var comboVal = $("#CmbBoxTipoPedido").val().toString();
    var empresa = 0;
    if ($("#CmbBoxEmpresa").val() !== null) { empresa = $("#CmbBoxEmpresa").val(); }
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarAtributosCabeceraIntegracion',
        contentType: "application/json; charset=utf-8",
        data: "{'iCodPedido':'" + sID + "','sIDTipoPedido':'" + comboVal.toString() + "', 'bHayIntegracion':'" + bHayIntegracionPedidos + "', 'iEmpresa':'" + empresa + "'}",
        dataType: "json",
        async: false
    })).done(function (msg) {
        var atributosIntegracion = $.parseJSON(msg.d);
        if (atributosIntegracion.length !== 0) {
            for (var j = 0; j < atributosIntegracion.length; j++) {
                var bInsertaAtrib = true;
                for (var z = 0; z < gEmisionPedido.OtrosDatos.length; z++) {
                    if (gEmisionPedido.OtrosDatos[z].Id == atributosIntegracion[j].Id) {
                        bInsertaAtrib = false;
                        //Ya estaba así que no lo vuelvo a guardar en el objeto pero le digo que es de integración
                        gEmisionPedido.OtrosDatos[z].Integracion = true;
                        break;
                    }
                }
                if (bInsertaAtrib) gEmisionPedido.OtrosDatos.push(atributosIntegracion[j]);
            }
        }
    });
}
function CargarAtributosLineasIntegracion() {
    var comboVal = $("#CmbBoxTipoPedido").val().toString();
    gLineas = gEmisionPedido.Lineas;
    var empresa = 0;
    if ($("#CmbBoxEmpresa").val() !== null) { empresa = $("#CmbBoxEmpresa").val(); }
    for (var j = 0; j < gLineas.length; j++) {
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarAtributosIntegracionLinea',
            contentType: "application/json; charset=utf-8",
            data: "{'iDesdeFavorito':'" + bModificarFavorito + "','sIDTipoPedido':'" + comboVal.toString() + "', 'iCodLinea':'" + gLineas[j].ID.toString() + "', 'bHayIntegracion':'" + bHayIntegracionPedidos + "', 'iEmpresa':'" + empresa + "'}",
            dataType: "json",
            async: false
        })).done(function (msg) {
            var atributosIntegracion = $.parseJSON(msg.d);
            if (atributosIntegracion.length !== 0) {
                for (var k = 0; k < atributosIntegracion.length; k++) {
                    var bInsertaAtrib = true;
                    for (var z = 0; z < gLineas[j].OtrosDatos.length; z++) {
                        if (gLineas[j].OtrosDatos[z].Id == atributosIntegracion[k].Id) {
                            bInsertaAtrib = false;
                            //Ya estaba así que no lo vuelvo a guardar en el objeto pero le digo que es de integración
                            gLineas[j].OtrosDatos[z].Integracion = true;
                            break;
                        }
                    }
                    if (bInsertaAtrib) {
                        gLineas[j].OtrosDatos.push(atributosIntegracion[k]);
                    }
                }
            }
        });
    }
    gEmisionPedido.Lineas = gLineas;
}
function ComprobarTipoPedidoRecepcionable() {
    if (gEmisionPedido.DatosGenerales.Tipo !== null) {
        for (var j = 0; j < gLineas.length; j++) {
            switch (gEmisionPedido.DatosGenerales.Tipo.Almacenable) {
                case 0:
                    if (gLineas[j].TipoArticulo.TipoAlmacenamiento == 1) {
                        $('#aviso' + gLineas[j].ID).prop('title', TextosJScript[43]);
                        ponerAvisoEnLinea(gLineas[j].ID);
                        return gLineas[j].ID
                    }
                    break;
                case 1:
                    if (gLineas[j].TipoArticulo.TipoAlmacenamiento == 0) {
                        $('#aviso' + gLineas[j].ID).prop('title', TextosJScript[42]);
                        ponerAvisoEnLinea(gLineas[j].ID);
                        return gLineas[j].ID
                    }
                    break;
            }
            switch (gEmisionPedido.DatosGenerales.Tipo.Concepto) {
                case 0:
                    if (gLineas[j].TipoArticulo.Concepto == 1) {
                        $('#aviso' + gLineas[j].ID).prop('title', TextosJScript[45]);
                        ponerAvisoEnLinea(gLineas[j].ID);
                        return gLineas[j].ID
                    }
                    break;
                case 1:
                    if (gLineas[j].TipoArticulo.Concepto == 0) {
                        $('#aviso' + gLineas[j].ID).prop('title', TextosJScript[44]);
                        ponerAvisoEnLinea(gLineas[j].ID);
                        return gLineas[j].ID
                    }
                    break;
            }
            switch (gEmisionPedido.DatosGenerales.Tipo.Recepcionable) {
                case 0:
                    if (gLineas[j].TipoArticulo.TipoRecepcion == 1) {
                        $('#aviso' + gLineas[j].ID).prop('title', TextosJScript[47]);
                        ponerAvisoEnLinea(gLineas[j].ID);
                        return gLineas[j].ID
                    }
                    break;
                case 1:
                    if (gLineas[j].TipoArticulo.TipoRecepcion == 0) {
                        $('#aviso' + gLineas[j].ID).prop('title', TextosJScript[46]);
                        ponerAvisoEnLinea(gLineas[j].ID);
                        return gLineas[j].ID
                    }
                    break;
            }
        };
    }
    return true
}
function comprobarDestino(sDest) {
    var bEncontrado = false;
    if (gDatosDestinos.length !== 0) {
        if (sDest !== "") {
            $.each(gDatosDestinos, function () {
                if (this.Cod == sDest) {
                    bEncontrado = true;
                    return false
                }
            });
        }
    }
    return bEncontrado;
}
/*
''' <summary>
''' Mira si el destino de la linea es admisible segun la lista destinos posibles.
''' </summary>
''' <param name="sDest">destino de la linea</param>
''' <param name="gDatosDestinos">lista destinos posibles</param>        
''' <remarks>Llamada desde: cargarLineas ; Tiempo máximo: 0</remarks>*/
function comprobarDestinoLista(sDest) {
    return ($.map(gDatosDestinos, function (x) { if (x.Cod == sDest) return x; }).length == 1);
}
function cambioCombo() {
    var idSeleccionado = $('#cboDest').fsCombo('getSelectedValue');
    $.each(gDatosDestinos, function () {
        if (this.Cod == idSeleccionado) {
            $('#LblCodDestSrv').text(this.Cod);
            $('#LblDenDestSrv').text(this.Den);
            $('#LblDirecDestSrv').text(this.Direccion);
            $('#LblCpDestSrv').text(this.CP);
            $('#LblPobDestSrv').text(this.Pob);
            $('#LblProvDestSrv').text(this.Provi);
            $('#LblPaisDestSrv').text(this.Pai);
            $('#LblTfnoDestSrv').text(this.Tfno);
            $('#LblFAXDestSrv').text(this.FAX);
            $('#LblEmailDestSrv').text(this.Email);
        }
    });
    $('#cboDest').fsCombo('selectValue', idSeleccionado);
}
function cambioComboUnidad(event) {
    var idSeleccionado = $('#cmbUnidades').fsCombo('getSelectedValue');

    if (idSeleccionado == "") { idSeleccionado = "\""; }
    var bEncontrado = false;
    $.each(gDatosUnidades, function () {
        if (this.Codigo == idSeleccionado) {
            var idLinea = $('#lblIdLineaUnidad').text();
            var sDesUPActual = $('#Uni' + idLinea).text();
            $('#lblCodUnidadSrv').text(JSText(this.Codigo));
            $('#lblCodUnidadSrvDesc').text(JSText(this.Codigo) + " - " + this.Denominacion);
            $('#lblUnidadCompraSrv').text(this.UnidadCompra);
            $('#lblUnidadCompraSrvDesc').text(this.UnidadCompra + " - " + this.DenUnidadCompra);
            $('#lblFactorConversionSrv').text(this.FactorConversion);
            $('#lblFactorConversionSrvDesc').text(JSText(this.Codigo) + " = " + this.FactorConversion + " " + this.UnidadCompra);
            $('#lblCantMinimaSrv').text(this.CantidadMinima);
            $('#lblCantMinimaSrvDesc').text(formatNumber(this.CantidadMinima));
            if (this.NumeroDeDecimales !== null) {
                $('#lblNumDecimales_hdd').text(this.NumeroDeDecimales);
            } else {
                $('#lblNumDecimales_hdd').text(UsuNumberNumDecimals);
            }
            bEncontrado = true;
            return false;
        }
    });
    if (bEncontrado !== true) { idSeleccionado = ""; }
    stopEvent(event);
}

function comprobarCentroUnico() {
    var empresa = 0;
    var empresa = $("#CmbBoxEmpresa").val();
    var origen = '&H7FFF';
    var params = JSON.stringify({ origen: origen, verUON: sbVerUON, Empresa: empresa.toString() });
    if (gDatosCentros == undefined) {
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/_Common/App_Services/Consultas.asmx/DevolverCentrosPedidosEP',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        })).done(function (msg) {
            gDatosCentros = $.parseJSON(msg.d);
            GestionarCentroUnico();
        });
    } else GestionarCentroUnico();
};
function GestionarCentroUnico() {
    if ((gDatosCentros != null) && (gDatosCentros.length == 1)) {
        var sCCCod = gDatosCentros[0].CCCOD;
        var sCCDen = gDatosCentros[0].CCUON + " - " + gDatosCentros[0].CCDEN;
        var sCCUon = gDatosCentros[0].CCPM;
        var datos = gDatosCentros[0].CCUON + "@@" + gDatosCentros[0].CCDEN;

        var tbCtroCoste = $('#tbCtroCoste');
        tbCtroCoste.val(sCCDen)
        tbCtroCoste = null;
        var tbCtroCosteHidden = $('#tbCtroCoste_Hidden');
        tbCtroCosteHidden.val(sCCCod + "@@" + sCCUon);
        tbCtroCosteHidden = null;
        vaciarPartida();
        vaciarActivo();
        ComprobarCentroContratoEscrito();
        for (var i = 0; i < gLineas.length; i++) {
            gCentroContrato.LineaId = parseInt(gLineas[i].ID.toString(), 10);
            var CentroContrato = CopiarObjCentroPartida(gCentroContrato);
            gLineas[i].Pres5_ImportesImputados_EP[0] = CentroContrato;
        }
        codCentro = $("#tbCtroCoste_Hidden").val();
        comprobarPartidaUnica(codCentro);
        gEmisionPedido.Lineas = gLineas;
    } else {
        vaciarCentroCoste();
        vaciarPartida();
        vaciarActivo();
    };
};
function CopiarObjCentroPartida(centro) {
    CentroContrato = {};
    CentroContrato.Id = centro.Id;
    CentroContrato.LineaId = centro.LineaId;
    CentroContrato.CentroCoste = CopiarObjCentroCoste(centro.CentroCoste);
    CentroContrato.Partida = CopiarObjPartida(centro.Partida);
    return CentroContrato;
}
function CopiarObjCentroCoste(centroCos) {
    CentroCoste = {};
    CentroCoste.UON1 = centroCos.UON1;
    CentroCoste.UON2 = centroCos.UON2;
    CentroCoste.UON3 = centroCos.UON3;
    CentroCoste.UON4 = centroCos.UON4;
    var CentroSM = {};
    CentroSM.Denominacion = centroCos.CentroSM.Denominacion;
    CentroSM.Codigo = centroCos.CentroSM.Codigo;
    CentroCoste.CentroSM = CentroSM;
    return CentroCoste;
}
function CopiarObjPartida(partida) {
    Partida = {};
    Partida.Denominacion = partida.Denominacion;
    Partida.PresupuestoId = partida.PresupuestoId;
    Partida.NIV0 = partida.NIV0;
    Partida.NIV1 = partida.NIV1;
    Partida.NIV2 = partida.NIV2;
    Partida.NIV3 = partida.NIV3;
    Partida.NIV4 = partida.NIV4;
    Partida.FechaInicioPresupuesto = partida.FechaInicioPresupuesto;
    Partida.FechaFinPresupuesto = partida.FechaFinPresupuesto;
    return Partida;
}
function comprobarPartidaUnica(codCentro) {
    var sData;
    var IdCodCentro = "";
    var IdCodEmpresa = 0;
    IdCodCentro = codCentro;
    IdCodEmpresa = $("#CmbBoxEmpresa").val();
    var origen = '&H7FFF';
    var texto = "";
    datos = origen + '@@@' + IdCodCentro + '@@@' + IdCodEmpresa + '@@@' + texto;
    sData = { datos: datos };
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/_Common/App_Services/Consultas.asmx/DevolverPartidasEPCompl',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(sData),
        dataType: "json",
        async: false
    })).done(function (msg) {
        var gDatosPartidas = $.parseJSON(msg.d);
        if ((gDatosPartidas != null) && (gDatosPartidas.length == 1)) {
            $('#tbPPres_Hidden').val(gDatosPartidas[0].PresHidden);
            $('#tbPPres').val(gDatosPartidas[0].PresValor);
            $('#tbCtroCoste_Hidden').val(gDatosPartidas[0].CtroCod);
            $('#tbCtroCoste').val(gDatosPartidas[0].CtroDen);
            if ($('#tbPPres').val() !== "") {
                ComprobarContratoEscrito();
                ComprobarCentroContrato(1);
                for (var i = 0; i < gLineas.length; i++) {
                    gCentroContrato.LineaId = parseInt(gLineas[i].ID.toString(), 10);
                    var CentroContrato = CopiarObjCentroPartida(gCentroContrato);
                    gLineas[i].Pres5_ImportesImputados_EP[0] = CentroContrato;
                }
            } else {
                vaciarPartida();
            }
        } else {
            vaciarPartida();
        };
    });
}
function comprobarPartidaUnicaLinea() {
    var sData;
    var IdCodCentro = "";
    var IdCodEmpresa = 0;
    IdCodCentro = $("#tbCtroCoste_Hidden").val();
    IdCodEmpresa = $("#CmbBoxEmpresa").val();
    var origen = '&H7FFF';
    var texto = "";
    datos = origen + '@@@' + IdCodCentro + '@@@' + IdCodEmpresa + '@@@' + texto;
    sData = { datos: datos };
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/_Common/App_Services/Consultas.asmx/DevolverPartidasEPCompl',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(sData),
        dataType: "json",
        async: false
    })).done(function (msg) {
        var gDatosPartidas = $.parseJSON(msg.d);
        if ((gDatosPartidas != null) && (gDatosPartidas.length == 1)) {
            $('#tbPPres_Hidden').val(gDatosPartidas[0].PresHidden);
            $('#tbPPres').val(gDatosPartidas[0].PresValor);
            $('#tbCtroCoste_Hidden').val(gDatosPartidas[0].CtroCod);
            $('#tbCtroCoste').val(gDatosPartidas[0].CtroDen);
            if ($('#tbPPres').val() !== "") {
                for (var i = 0; i < gLineas.length; i++) {
                    if (gLineas[i].ID == Linea.ID) {
                        gCentroContrato = gLineas[i].Pres5_ImportesImputados_EP[0];
                        ComprobarContratoEscrito();
                        gCentroContrato.LineaId = Linea.ID;
                        var tbCtroCoste = $('#tbCtroCoste');
                        tbCtroCoste.css('background-color', '#D8E4BC');
                        var tbPPres = $('#tbPPres');
                        tbPPres.css('background-color', '#D8E4BC');
                        CentroContrato = gCentroContrato;
                        Linea.Pres5_ImportesImputados_EP[0] = CentroContrato;
                        gLineas[i].Pres5_ImportesImputados_EP[0] = CentroContrato;
                    };
                }
            } else {
                vaciarPartida();
            }
        } else {
            vaciarPartida();
        }
    });
}
function vaciarCentroCoste() {
    $('#tbCtroCoste').val("");
    $('#tbCtroCoste_Hidden').val("");
    $('#tbCtroCoste').css('background-color', '');
}
function vaciarPartida() {
    $('#tbPPres').val("");
    $('#tbPPres_Hidden').val("");
    $('#tbPPres').css('background-color', '');
}
function vaciarActivo() {
    $('#tbActivosLinea').val("");
    $('#tbActivosLinea_Hidden').val("");
    $('#tbActivosLinea').css('background-color', '');
}
function cargarComboAlmacen(gDatosAlmacenes) {
    var items = "{";
    var i = 0;
    var codigo;
    if (gDatosAlmacenes == null) {
        codigo = 0;
        items += '"' + i + '": { "value":"0", "text": "" }, ';
    } else {
        if (gDatosAlmacenes.length == 0) {
            codigo = 0;
            items += '"' + i + '": { "value":"0", "text": "" }, ';
        } else {
            $.each(gDatosAlmacenes, function () {
                codigo = this.Id;
                items += '"' + i + '": { "value":"' + this.Id + '", "text": "' + this.Denominacion + '" }, '
                i++;
            });
        }
    }
    items = items.substr(0, items.length - 2) //quitamos la ultima coma que sobra
    items += "}";
    var idSelection;
    if ($('#fsComboValueDisplay_cboAlmacen').length !== 0) {
        $('#cboAlmacen').fsCombo('destroy');

    };
    if (Linea.IdAlmacen == undefined) {
        idSelection = codigo;
    } else { idSelection = Linea.IdAlmacen; }
    var optionsAlmacenes = { valueField: "value", textField: "text", adjustWidthToSpace: false, dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, autoComplete: false, data: $.parseJSON(items) };
    $('#cboAlmacen').fsCombo(optionsAlmacenes);
    $('#cboAlmacen').fsCombo('selectValue', idSelection);
    $('#cboAlmacen').fsCombo('onSelectedValueChange', function () { cambioComboAlmacen() });
    if ($('#fsComboValueDisplay_cboAlmacen').length !== 0) {
        $('#fsComboValueDisplay_cboAlmacen').width("80%");

    }
}
function cambioComboAlmacen() {
    var idSeleccionado = $('#cboAlmacen').fsCombo('getSelectedValue');
    var denSeleccionado = $('#cboAlmacen').fsCombo('getSelectedText');

    gLineas = gEmisionPedido.Lineas;

    for (var i = 0; i < gLineas.length; i++) {
        if (gLineas[i].ID == iCodFilaSeleccionada) {
            gLineas[i].IdAlmacen = idSeleccionado;
            gLineas[i].strAlmacen = denSeleccionado;
            break;
        }
    }
    gEmisionPedido.Lineas = gLineas;
}
//funcion que carga los atributos a nivel de Item(Panel edicion Item)
function CargarAtributosCoste() {
    var indAtributo = 0;
    var ImporteFijo = 0;
    var comboVal = $("#CmbBoxTipoPedido").val().toString();
    //si no tenemos los costes cargardos los cargamos
    if (gCostes == null) {
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarAtributosCostes',
            contentType: "application/json; charset=utf-8",
            data: "{'iDesdeFavorito':'" + bModificarFavorito + "','iCodPedido':'" + sID + "','sIDTipoPedido':'" + comboVal.toString() + "',EsPedidoAbierto:" + EsPedidoAbierto + "}",
            dataType: "json",
            async: false
        })).done(function (msg) {
            gCostes = $.parseJSON(msg.d);
            gEmisionPedido.Costes = gCostes;
            if (gCostes.length == 0) { //Si no hay atributos ocultamos el Div de Atributos    
                $('[id$=divAtributosCoste]').hide();
                $('#pesDatosGenerales').click();
            } else {
                $('[id$=divAtributosCoste]').show();
            }

            //Se eliminan las filas si las hubiese de la tabla(menos la cabecera)
            var table = $("#tblAtributosCoste")
            table.find('tr:not(:first)').remove();
            var GrupoCosteDescuentoAnt = "";
            var Inicio = "0";
            $.each(gCostes, function () {
                indAtributo += 1;
                if (this.Operacion == "+") {
                    if (this.Valor == undefined) {
                        var Importe = 0;
                    } else {
                        var Importe = this.Valor;
                    }
                } else {
                    if (this.Valor == undefined) {
                        var Importe = 0;
                    } else {
                        var Importe = parseFloat($('#lblImpBruto_hdd').text());
                        Importe = Importe * (this.Valor / 100);
                    }
                }
                if (this.Seleccionado == undefined) {
                    var Seleccionado = false;
                    this.Seleccionado = Seleccionado;
                }
                this.Importe = Importe;

                if (this.GrupoCosteDescuento == undefined) {
                    var GrupoCosteDescuento = "";
                } else {
                    var GrupoCosteDescuento = this.GrupoCosteDescuento;
                }
                if (this.Inicio == undefined) {
                    if (GrupoCosteDescuentoAnt !== "" && this.GrupoCosteDescuento !== undefined) {
                        if (this.GrupoCosteDescuento == GrupoCosteDescuentoAnt) {
                            Inicio = "1";
                        } else {
                            Inicio = "0";
                        }
                    } else {
                        Inicio = "0";
                    }
                } else {
                    Inicio = this.Inicio;
                }
                this.Inicio = Inicio;

                this.Moneda = sMoneda;
                GrupoCosteDescuentoAnt = GrupoCosteDescuento;

                var filaSel = false;
                if (this.TipoCosteDescuento == 0) filaSel = true;
                else filaSel = this.Seleccionado;

                switch (this.TipoCosteDescuento) {
                    case 0:
                        $("#tmplCampoCosteFijo").tmpl(this).appendTo("#tblAtributosCoste");
                        ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                        pintarIVA(this.Impuestos, filaSel, "fij" + this.Id, this.Importe);
                        break;
                    case 1:
                        if (this.Seleccionado == true) {
                            ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                        }
                        $("#tmplCampoCosteExc").tmpl(this).appendTo("#tblAtributosCoste");
                        pintarIVA(this.Impuestos, filaSel, "oblExc" + this.Id, this.Importe);
                        break;
                    case 2:
                        if (this.Seleccionado == true) ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);

                        $("#tmplCampoCosteOpc").tmpl(this).appendTo("#tblAtributosCoste");
                        pintarIVA(this.Impuestos, filaSel, "opc" + this.Id, this.Importe);
                        break;
                    case 3:
                        if (this.Seleccionado == true) ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);

                        $("#tmplCampoCosteOpcExc").tmpl(this).appendTo("#tblAtributosCoste");
                        pintarIVA(this.Impuestos, filaSel, "oblOpc" + this.Id, this.Importe);
                        break;
                }
            })
        });
        $('#lblCosteTotal_hdd').text(ImporteFijo);
        $('#lblCosteTotal').text(formatNumber(ImporteFijo) + " " + sMoneda);
    }
}
function pintarIVA(Impuestos, filaSel, origen, importeCoste) {
    $.each(Impuestos, function () {
        this.Seleccionado = filaSel;
        this.Importe = importeCoste * (this.Valor / 100);
        this.Moneda = sMoneda;
        this.NomCampo = origen + "_" + this.Id;
        $("#IVA").tmpl(this).appendTo("#tblAtributosCoste");
    });
}
function pintarIVALinea(Impuestos, filaSel, origen, importeCoste) {
    $.each(Impuestos, function () {
        this.Seleccionado = filaSel;
        this.Importe = importeCoste * (this.Valor / 100);
        this.Moneda = sMoneda;
        this.NomCampo = origen + "_" + this.Id;
        $("#IVA").tmpl(this).appendTo("#tblAtributosCosteLinea");
    });
}
function recorrerImpuestosCabeceraPorcentajes() {
    calcularImpuestos();
    recalcularImportesPorcentajes();

    $('#pesCostes').click();
    $('#pesDescuento').click();
    $('#pesDatosGenerales').click();
    recalcularImportes();
}
function calcularImpuestos() {
    $('#lblImpTotalCuotas_hdd').text(0);
    var Impuestos = 0;
    var ImpuestoCuota = 0;
    var ImporteCoste = 0;
    var ImporteCosteLinea = 0;
    if ($('[id^=descCuota_]').length !== 0) {
        $.each($('[id^=descCuota_]'), function () {
            var parts = this.id.split("_");
            var idCouta = $.trim(parts[1]);
            $('[id^=trIVA_' + idCouta + ']').remove();
            $('[id^=divInformacionIVA_' + idCouta + ']').remove();
            $('#descCuota_' + idCouta).hide();
        });
    }

    //Impuestos Cabecera
    var iLinea = 0;
    var idConcepto = 0;
    var totalConcepto = 0;
    $.each(gEmisionPedido.Costes, function () {
        idConcepto = this.Id;
        ImporteCoste = this.Importe;
        //Linea++;
        if (this.Seleccionado == true || this.TipoCosteDescuento == 0) {
            Impuestos = parseFloat($.trim($('#lblImpTotalCuotas_hdd').text()));
            $.each(this.Impuestos, function () {
                var ImpuestoAct = ImporteCoste * (this.Valor / 100);
                if ($('#cuota_' + this.Id + '_hdd').length == 0) {
                    Impuestos = Impuestos + ImpuestoAct;
                    AgregarCampo(this.Id, this.Denominacion + " " + this.Valor + "%", ImpuestoAct, "C", iLinea, ImporteCoste, idConcepto);
                } else {
                    ImpuestoCuota = parseFloat($.trim($('#cuota_' + this.Id + '_hdd').text()));
                    Impuestos = Impuestos - ImpuestoCuota;
                    ImpuestoAct = ImpuestoAct + ImpuestoCuota;
                    Impuestos = Impuestos + ImpuestoAct;
                    $('#descCuota_' + this.Id).show();
                    $('#cuota_' + this.Id + '_hdd').text(ImpuestoAct.toString());
                    $('#cuota_' + this.Id).text(formatNumber(ImpuestoAct.toString()) + " " + sMoneda);
                    totalConcepto = ImpuestoAct * (100 / this.Valor);
                    agregarCampoCuota(this.Id, idConcepto, this.Denominacion + " " + this.Valor + "%", "C", iLinea, ImporteCosteLinea, totalConcepto);
                }
                $('#lblImpTotalCuotas_hdd').text(Impuestos);
            });
        }
    });

    //Impuestos Linea Catalogo
    var ImporteLinea, ImporteBrutoLinea, ImporteCostesLinea, ImporteDescuentosLinea, DescuentosCabeceraGeneral, ImporteTotalLineas;
    var DescuentosLinea, DescuentosCabecera;
    var ImporteBruto, ImporteBrutoCabecera;
    var iNumerosLinea = 0;
    iLinea = 0;
    idConcepto = 0;
    DescuentosCabecera = 0;
    ImporteTotalLineas = 0;
    ImporteBrutoLinea = 0;
    ImporteCostesLinea = 0;
    ImporteDescuentosLinea = 0;
    $.each(gLineas, function () {
        iNumerosLinea++;
        ImporteBrutoLinea = parseFloat($.trim($('#lblImpBrutolin_' + this.ID + '_hdd').text()));
        ImporteCostesLinea = parseFloat($.trim($('#lblCosteTotalLinea_' + this.ID + '_hdd').text()));
        ImporteDescuentosLinea = parseFloat($.trim($('#lblDescuentoTotalLinea_' + this.ID + '_hdd').text()));
        ImporteTotalLineas = ImporteTotalLineas + ImporteBrutoLinea + ImporteCostesLinea - ImporteDescuentosLinea;
    });
    $.each(gLineas, function () {
        iLinea++;
        idConcepto = this.ID;
        ImporteBrutoLinea = 0;
        ImporteCostesLinea = 0;
        ImporteDescuentosLinea = 0;

        ImporteBrutoLinea = parseFloat($.trim($('#lblImpBrutolin_' + this.ID + '_hdd').text()));
        ImporteCostesLinea = parseFloat($.trim($('#lblCosteTotalLinea_' + this.ID + '_hdd').text()));
        ImporteDescuentosLinea = parseFloat($.trim($('#lblDescuentoTotalLinea_' + this.ID + '_hdd').text()));
        ImporteLinea = ImporteBrutoLinea + ImporteCostesLinea - ImporteDescuentosLinea;
        DescuentosCabeceraGeneral = 0;
        $.each(gEmisionPedido.Descuentos, function () {
            if (this.Seleccionado == true || this.TipoCosteDescuento == 0) {
                if (this.Operacion == "-") {
                    DescuentosCabeceraGeneral = DescuentosCabeceraGeneral + (this.Valor / iNumerosLinea); // (ImporteLinea * this.Valor) / ImporteTotalLineas;
                } else {
                    ImporteBruto = ImporteLinea * (this.Valor / 100);
                    DescuentosCabeceraGeneral = DescuentosCabeceraGeneral + ImporteBruto;
                }
            }
        });
        DescuentosLinea = 0;
        ImporteCosteLinea = parseFloat($.trim($('#lblImpBrutolin_' + this.ID + '_hdd').text()));
        Impuestos = parseFloat($.trim($('#lblImpTotalCuotas_hdd').text()));
        $.each(this.Descuentos, function () {
            if (this.Seleccionado == true || this.TipoCosteDescuento == 0) {
                if (this.Operacion == "-") {
                    DescuentosLinea = DescuentosLinea + this.Valor;
                } else {
                    ImporteBruto = ImporteCosteLinea * (this.Valor / 100);
                    DescuentosLinea = DescuentosLinea + ImporteBruto;
                }
            }
        });
        ImporteCosteLinea = ImporteCosteLinea - DescuentosLinea - DescuentosCabeceraGeneral;
        $.each(this.Impuestos, function () {
            var ImpuestoAct = ImporteCosteLinea * (this.Valor / 100);
            if ($('#cuota_' + this.Id + '_hdd').length == 0) {
                Impuestos = Impuestos + ImpuestoAct;
                AgregarCampo(this.Id, this.Denominacion + " " + this.Valor + "%", ImpuestoAct, "L", iLinea, ImporteCosteLinea, idConcepto);
            } else {
                ImpuestoCuota = parseFloat($.trim($('#cuota_' + this.Id + '_hdd').text()));
                Impuestos = Impuestos - ImpuestoCuota;
                ImpuestoAct = ImpuestoAct + ImpuestoCuota;
                Impuestos = Impuestos + ImpuestoAct;
                $('#descCuota_' + this.Id).show();
                $('#cuota_' + this.Id + '_hdd').text(ImpuestoAct.toString());
                $('#cuota_' + this.Id).text(formatNumber(ImpuestoAct.toString()) + " " + sMoneda);
                totalConcepto = ImpuestoAct * (100 / this.Valor);
                agregarCampoCuota(this.Id, idConcepto, this.Denominacion + " " + this.Valor + "%", "L", iLinea, ImporteCosteLinea, totalConcepto);
            }
            $('#lblImpTotalCuotas_hdd').text(Impuestos);
        });
    });

    //Impuestos Costes Lineas Catalogo
    var ImporteBruto;
    var TipoCosteDescuento, Seleccionado;
    idConcepto = 0;
    iLinea = 0;

    $.each(gLineas, function () {
        iLinea++;
        idConcepto = this.ID;

        $.each(this.Costes, function () {
            ImporteCosteLinea = parseFloat($.trim($('#lblImpBrutolin_' + idConcepto + '_hdd').text()));
            if (this.Seleccionado == true || this.TipoCosteDescuento == 0) {
                if (this.Operacion == "+") ImporteCosteLinea = this.Valor;
                else ImporteCosteLinea = ImporteCosteLinea * (this.Valor / 100);

                Impuestos = parseFloat($.trim($('#lblImpTotalCuotas_hdd').text()));
                TipoCosteDescuento = this.TipoCosteDescuento;
                Seleccionado = this.Seleccionado;
                $.each(this.Impuestos, function () {
                    if (Seleccionado == true || TipoCosteDescuento == 0) {
                        var ImpuestoAct = ImporteCosteLinea * (this.Valor / 100);
                        if ($('#cuota_' + this.Id + '_hdd').length == 0) {
                            Impuestos = Impuestos + ImpuestoAct;
                            AgregarCampo(this.Id, this.Denominacion + " " + this.Valor + "%", ImpuestoAct, "LC", iLinea, ImporteCosteLinea, idConcepto);
                        } else {
                            ImpuestoCuota = parseFloat($.trim($('#cuota_' + this.Id + '_hdd').text()));
                            Impuestos = Impuestos - ImpuestoCuota;
                            ImpuestoAct = ImpuestoAct + ImpuestoCuota;
                            Impuestos = Impuestos + ImpuestoAct;
                            $('#descCuota_' + this.Id).show();
                            $('#cuota_' + this.Id + '_hdd').text(ImpuestoAct.toString());
                            $('#cuota_' + this.Id).text(formatNumber(ImpuestoAct.toString()) + " " + sMoneda);
                            totalConcepto = ImpuestoAct * (100 / this.Valor);
                            agregarCampoCuota(this.Id, idConcepto, this.Denominacion + " " + this.Valor + "%", "LC", iLinea, ImporteCosteLinea, totalConcepto);
                        }
                        $('#lblImpTotalCuotas_hdd').text(Impuestos);
                    }
                });
            }
        });
    });
}
function AgregarCampo(id, den, valor, tipo, linea, importeBruto, idConcepto) {
    var datos =
        [{
            id: id,
            den: den,
            valor: valor,
            sMoneda: sMoneda,
            importeBruto: importeBruto,
            baseImponible: TextosJScript[55]
        }];
    var desc;
    if (tipo == "C") {
        desc = TextosJScript[4]; //"Costes"
    } else {
        if (tipo == "LC") desc = TextosJScript[4] + " " + TextosJScript[51]; //"IVA Coste Linea"
        else desc = TextosJScript[52]; //"Importe Bruto Linea"        
    }
    var datosBaseCabecera =
        [{
            id: id,
            importeTotal: importeBruto,
            den: den
        }];
    var datosBase =
        [{
            id: idConcepto,
            den: den,
            desc: desc,
            linea: linea,
            importeBruto: importeBruto,
            sMoneda: sMoneda
        }];
    $('#CabeceraIVA').tmpl(datos).appendTo($('#ImportesPedido'));
    $('#CabeceraInformacionIVA').tmpl(datosBaseCabecera).appendTo($('#descCuota_' + id));
    $('#InformacionIVA').tmpl(datosBase).appendTo($('#baseImponible_' + id));
}
function agregarCampoCuota(id, idConcepto, den, tipo, linea, importeBruto, importeBase) {
    var desc;
    if (tipo == "C") {
        desc = TextosJScript[4]; //"Coste"
    } else {
        if (tipo == "LC") desc = TextosJScript[4] + " " + TextosJScript[51]; //"IVA Coste Linea"
        else desc = TextosJScript[52]; //"Importe Bruto Linea"        
    }
    var datosBase =
        [{
            id: idConcepto,
            den: den,
            desc: desc,
            linea: linea,
            importeBruto: importeBruto,
            sMoneda: sMoneda
        }];
    $('#lblImporteBase_' + id).text(formatNumber(importeBase.toString()) + " " + sMoneda);
    $('#totalcuota_' + id).text(formatNumber(importeBase.toString()) + " " + sMoneda);

    $('#InformacionIVA').tmpl(datosBase).appendTo($('#baseImponible_' + id));
}
function recalcularImportesCosteAnyadir(importe, restar) {
    var parts = importe.split(" ");
    var valor = parseFloat(strToNum($.trim(parts[0])));
    var operacion = $.trim(parts[1]);
    var importeBruto = parseFloat($('#lblImpBruto_hdd').text());
    var importeBrutoResta = 0;
    var costesGenerales = parseFloat($('#lblCosteTotal_hdd').text());

    if (restar == undefined) {
        restar = 0;
    } else {
        if (restar !== 0) {
            var partsResta = restar.split(" ");
            var valorResta = parseFloat(strToNum($.trim(partsResta[0])));
            var operacionResta = $.trim(partsResta[1]);
            if (operacionResta == "%") {
                importeBrutoResta = importeBruto * (valorResta / 100);
                costesGenerales = costesGenerales - importeBrutoResta;
                $('#lblCosteTotal_hdd').text(costesGenerales);
                $('#lblCosteTotal').text(formatNumber(costesGenerales));
            } else {
                costesGenerales = costesGenerales - valorResta;
                $('#lblCosteTotal_hdd').text(costesGenerales);
                $('#lblCosteTotal').text(formatNumber(costesGenerales) + " " + sMoneda);
            }
        }
    }
    if (operacion == "%") {
        importeBruto = importeBruto * (valor / 100);
        costesGenerales = costesGenerales + importeBruto;
        $('#lblCosteTotal_hdd').text(costesGenerales);
        $('#lblCosteTotal').text(formatNumber(costesGenerales) + " " + sMoneda);
    } else {
        costesGenerales = costesGenerales + valor;
        $('#lblCosteTotal_hdd').text(costesGenerales);
        $('#lblCosteTotal').text(formatNumber(costesGenerales) + " " + sMoneda);
    }

    $('#lblCosteCabeceraTotal_hdd').text($('#lblCosteTotal_hdd').text());
    $('#lblCosteCabeceraTotal').text($('#lblCosteTotal').text());
    calculoTotalCabecera();
}
function recalcularImportesCosteResta(importe) {
    var parts = importe.split(" ");
    var valor = parseFloat(strToNum($.trim(parts[0])));
    var operacion = $.trim(parts[1]);
    var importeBruto = parseFloat($('#lblImpBruto_hdd').text());
    var costesGenerales = parseFloat($('#lblCosteTotal_hdd').text());
    if (operacion == "%") {
        importeBruto = importeBruto * (valor / 100);
        costesGenerales = costesGenerales - importeBruto;
        $('#lblCosteTotal_hdd').text(costesGenerales);
        $('#lblCosteTotal').text(formatNumber(costesGenerales) + " " + sMoneda);
    } else {
        costesGenerales = costesGenerales - valor;
        $('#lblCosteTotal_hdd').text(costesGenerales);
        $('#lblCosteTotal').text(formatNumber(costesGenerales) + " " + sMoneda);
    }

    $('#lblCosteCabeceraTotal_hdd').text($('#lblCosteTotal_hdd').text());
    $('#lblCosteCabeceraTotal').text($('#lblCosteTotal').text());
    calculoTotalCabecera();
}
//funcion que carga los atributos a nivel de Item(Panel edicion Item)
function CargarAtributosDescuento() {
    var indAtributo = 0;
    var ImporteFijo = 0;
    var comboVal = $("#CmbBoxTipoPedido").val().toString();

    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarAtributosDescuentos',
        contentType: "application/json; charset=utf-8",
        data: "{'iCodPedido':'" + sID + "','sIDTipoPedido':'" + comboVal.toString() + "',EsPedidoAbierto:" + EsPedidoAbierto + "}",
        dataType: "json",
        async: false
    })).done(function (msg) {
        gDescu = $.parseJSON(msg.d);
        gEmisionPedido.Descuentos = gDescu;
        //gEmisionPedido.OtrosDatos = gAtributos;
        if (gDescu.length == 0) { //Si no hay atributos ocultamos el Div de Atributos    
            $('[id$=divAtributosDescuento]').hide();
            $('#pesDatosGenerales').click();
        } else {
            $('[id$=divAtributosDescuento]').show();
        }

        //Se eliminan las filas si las hubiese de la tabla(menos la cabecera)
        var table = $("#tblAtributosDescuento")
        table.find('tr:not(:first)').remove();
        var GrupoCosteDescuentoAnt = "";
        var Inicio = "0";
        $.each(gDescu, function () {
            indAtributo += 1;
            if (this.Operacion == "-") {
                if (this.Valor == undefined) var Importe = 0;
                else var Importe = this.Valor;
            } else {
                if (this.Valor == undefined) {
                    var Importe = 0;
                } else {
                    var Importe = parseFloat($('#lblImpBruto_hdd').text());
                    Importe = Importe * (this.Valor / 100);
                }
            }

            this.Importe = Importe;

            if (this.GrupoCosteDescuento == undefined) var GrupoCosteDescuento = "";
            else var GrupoCosteDescuento = this.GrupoCosteDescuento;

            if (this.Inicio == undefined) {
                if (GrupoCosteDescuentoAnt !== "" && this.GrupoCosteDescuento !== undefined) {
                    if (this.GrupoCosteDescuento == GrupoCosteDescuentoAnt) Inicio = "1";
                    else Inicio = "0";
                } else {
                    Inicio = "0";
                }
            } else {
                Inicio = this.Inicio;
            }
            this.Inicio = Inicio;
            if (this.Seleccionado == undefined) {
                var Seleccionado = false;
                this.Seleccionado = Seleccionado;
            }
            this.Moneda = sMoneda;
            GrupoCosteDescuentoAnt = GrupoCosteDescuento;
            switch (this.TipoCosteDescuento) {
                case 0:
                    $("#tmplCampoDescuFijo").tmpl(this).appendTo("#tblAtributosDescuento");
                    ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                    break;
                case 1:
                    if (this.Seleccionado == true) ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);

                    $("#tmplCampoDescuExc").tmpl(this).appendTo("#tblAtributosDescuento");
                    break;
                case 2:
                    if (this.Seleccionado == true) ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);

                    $("#tmplCampoDescuOpc").tmpl(this).appendTo("#tblAtributosDescuento");
                    break;
                case 3:
                    if (this.Seleccionado == true) ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);

                    $("#tmplCampoDescuOpcExc").tmpl(this).appendTo("#tblAtributosDescuento");
                    break;
            }

        })
    });
    $('#lblDescuentoTotal_hdd').text(ImporteFijo);
    $('#lblDescuentoTotal').text(formatNumber(ImporteFijo) + " " + sMoneda);
    $('#lblDescuentosCabeceraTotal_hdd').text(ImporteFijo);
    $('#lblDescuentosCabeceraTotal').text(formatNumber(ImporteFijo) + " " + sMoneda);
}
function recalcularImportesDescuentoAnyadir(importe, restar) {
    var parts = importe.split(" ");
    var valor = parseFloat(strToNum($.trim(parts[0])));
    var operacion = $.trim(parts[1]);
    var importeBruto = parseFloat($('#lblImpBruto_hdd').text());
    var importeBrutoResta = 0;
    var descuentosGenerales = parseFloat($('#lblDescuentoTotal_hdd').text());
    if (restar == undefined) {
        restar = 0;
    } else {
        if (restar !== 0) {
            var partsResta = restar.split(" ");
            var valorResta = parseFloat(strToNum($.trim(partsResta[0])));
            var operacionResta = $.trim(partsResta[1]);
            if (operacionResta == "%") {
                importeBrutoResta = importeBruto * (valorResta / 100);
                descuentosGenerales = descuentosGenerales - importeBrutoResta;
                $('#lblDescuentoTotal_hdd').text(descuentosGenerales);
                $('#lblDescuentoTotal').text(formatNumber(descuentosGenerales));
            } else {
                descuentosGenerales = descuentosGenerales - valorResta;
                $('#lblDescuentoTotal_hdd').text(descuentosGenerales);
                $('#lblDescuentoTotal').text(formatNumber(descuentosGenerales) + " " + sMoneda);
            }
        }
    }

    if (operacion == "%") {
        importeBruto = importeBruto * (valor / 100);
        descuentosGenerales = descuentosGenerales + importeBruto;
        $('#lblDescuentoTotal_hdd').text(descuentosGenerales);
        $('#lblDescuentoTotal').text(formatNumber(descuentosGenerales) + " " + sMoneda);
    } else {
        descuentosGenerales = descuentosGenerales + valor;
        $('#lblDescuentoTotal_hdd').text(descuentosGenerales);
        $('#lblDescuentoTotal').text(formatNumber(descuentosGenerales) + " " + sMoneda);
    }

    $('#lblDescuentosCabeceraTotal_hdd').text($('#lblDescuentoTotal_hdd').text());
    $('#lblDescuentosCabeceraTotal').text($('#lblDescuentoTotal').text());
    recalcularImportes();
}
function recalcularImportesDescuentoResta(importe) {
    var parts = importe.split(" ");
    var valor = parseFloat(strToNum($.trim(parts[0])));
    var operacion = $.trim(parts[1]);
    var importeBruto = parseFloat($('#lblImpBruto_hdd').text());
    var descuentosGenerales = parseFloat($('#lblDescuentoTotal_hdd').text());
    if (operacion == "%") {
        importeBruto = importeBruto * (valor / 100);
        descuentosGenerales = descuentosGenerales - importeBruto;
        $('#lblDescuentoTotal_hdd').text(descuentosGenerales);
        $('#lblDescuentoTotal').text(formatNumber(descuentosGenerales) + " " + sMoneda);
    } else {
        descuentosGenerales = descuentosGenerales - valor;
        $('#lblDescuentoTotal_hdd').text(descuentosGenerales);
        $('#lblDescuentoTotal').text(formatNumber(descuentosGenerales) + " " + sMoneda);
    }
    $('#lblDescuentosCabeceraTotal_hdd').text($('#lblDescuentoTotal_hdd').text());
    $('#lblDescuentosCabeceraTotal').text($('#lblDescuentoTotal').text());

    recalcularImportes();
}
//funcion que carga los atributos a nivel de Item(Panel edicion Item)
function CargarAtributosCosteLinea() {
    var comboVal = $("#CmbBoxTipoPedido").val().toString();
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarAtributosCostesLinea',
        contentType: "application/json; charset=utf-8",
        data: "{'iDesdeFavorito':'" + bModificarFavorito + "','iCodLinea':'" + Linea.ID + "','sIDTipoPedido':'" + comboVal.toString() + "', 'EsPedidoAbierto':" + EsPedidoAbierto + "}",
        dataType: "json",
        async: false
    })).done(function (msg) {
        gCostesLinea = $.parseJSON(msg.d);
        Linea.Costes = gCostesLinea;
        pintarCostesLinea();
    });
}
function pintarCostesLinea() {
    var indAtributo = 0;
    var ImporteFijo = 0;
    var ImporteFijoLinea = 0;

    if (gCostesLinea.length == 0) { //Si no hay atributos ocultamos el Div de Atributos    
        $('[id$=divAtributosCosteLinea]').hide();
        $('#pesResumenLineas').click();
    } else {
        $('[id$=divAtributosCosteLinea]').show();
    }

    //Se eliminan las filas si las hubiese de la tabla(menos la cabecera)
    var table = $("#tblAtributosCosteLinea")
    table.find('tr:not(:first)').remove();
    var GrupoCosteDescuentoAnt = "";
    var Inicio = "0";
    $.each(gCostesLinea, function () {
        ImporteFijoLinea = 0;
        indAtributo += 1;
        if (this.Operacion == "+") {
            if (this.Valor == undefined) var Importe = 0;
            else var Importe = this.Valor;
        } else {
            if (this.Valor == undefined) var Importe = 0;
            else {
                var Importe = parseFloat($('#lblImpBrutolin_' + iCodFilaSeleccionada.toString() + '_hdd').text());
                Importe = Importe * (this.Valor / 100);
            }
        }

        this.Importe = Importe;

        if (this.GrupoCosteDescuento == undefined) var GrupoCosteDescuento = "";
        else var GrupoCosteDescuento = this.GrupoCosteDescuento;

        if (this.Inicio == undefined) {
            if (GrupoCosteDescuentoAnt !== "" && this.GrupoCosteDescuento !== undefined) {
                if (this.GrupoCosteDescuento == GrupoCosteDescuentoAnt) Inicio = "1";
                else Inicio = "0";
            } else {
                Inicio = "0";
            }
        } else {
            Inicio = this.Inicio;
        }
        this.Inicio = Inicio;
        if (this.Seleccionado == undefined) {
            var Seleccionado = false;
            this.Seleccionado = Seleccionado;
        }
        this.Moneda = sMoneda;
        GrupoCosteDescuentoAnt = GrupoCosteDescuento;

        var filaSel = false;
        if (this.TipoCosteDescuento == 0) filaSel = true;
        else filaSel = this.Seleccionado;

        switch (this.TipoCosteDescuento) {
            case 0:
                $("#tmplCampoCosteFijoLinea").tmpl(this).appendTo("#tblAtributosCosteLinea");
                ImporteFijoLinea = parseFloat(ImporteFijoLinea) + parseFloat(this.Importe);
                ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                pintarIVALinea(this.Impuestos, filaSel, "fijLin" + this.Id, this.Importe);
                break;
            case 1:
                if (this.Seleccionado == true) {
                    ImporteFijoLinea = parseFloat(ImporteFijoLinea) + parseFloat(this.Importe);
                    ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                }
                $("#tmplCampoCosteExcLinea").tmpl(this).appendTo("#tblAtributosCosteLinea");
                pintarIVALinea(this.Impuestos, filaSel, "oblExcLin" + this.Id, this.Importe);
                break;
            case 2:
                if (this.Seleccionado == true) {
                    ImporteFijoLinea = parseFloat(ImporteFijoLinea) + parseFloat(this.Importe);
                    ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                }
                $("#tmplCampoCosteOpcLinea").tmpl(this).appendTo("#tblAtributosCosteLinea");
                pintarIVALinea(this.Impuestos, filaSel, "opcLin" + this.Id, this.Importe);
                break;
            case 3:
                if (this.Seleccionado == true) {
                    ImporteFijoLinea = parseFloat(ImporteFijoLinea) + parseFloat(this.Importe);
                    ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                }
                $("#tmplCampoCosteOpcExcLinea").tmpl(this).appendTo("#tblAtributosCosteLinea");
                pintarIVALinea(this.Impuestos, filaSel, "oblOpcLin" + this.Id, this.Importe);
                break;
        }
    });

    $('#lblCosteTotalLinea_hdd').text(ImporteFijo);
    $('#lblCosteTotalLinea').text(formatNumber(ImporteFijo) + " " + sMoneda);
}
function recalcularImportesCosteAnyadirLinea(importe, restar) {
    var parts = importe.split(" ");
    var valor = parseFloat(strToNum($.trim(parts[0])));
    var operacion = $.trim(parts[1]);
    var importeBruto = parseFloat($('#lblImpBrutolin_' + iCodFilaSeleccionada.toString() + '_hdd').text());
    var costesGenerales = parseFloat($('#lblCosteTotalLinea_hdd').text());
    var costesLineas = parseFloat($('#lblTotalCostesLinea_hdd').text());
    var impBrutoTotal = parseFloat($.trim($('#lblImpBruto_hdd').text()));
    var impBrutoCal;
    if (restar == undefined) {
        restar = 0;
    } else {
        if (restar !== 0) {
            var partsResta = restar.split(" ");
            var valorResta = parseFloat(strToNum($.trim(partsResta[0])));
            var operacionResta = $.trim(partsResta[1]);
            if (operacionResta == "%") {
                impBrutoCal = importeBruto * (valorResta / 100);
                costesGenerales = costesGenerales - impBrutoCal;
                costesLineas = costesLineas - impBrutoCal;
                impBrutoTotal = impBrutoTotal - impBrutoCal;
            } else {
                costesGenerales = costesGenerales - valorResta;
                costesLineas = costesLineas - valorResta;
                impBrutoTotal = impBrutoTotal - valorResta;
            }
            $('#lblCosteTotalLinea_hdd').text(costesGenerales);
            $('#lblCosteTotalLinea').text(formatNumber(costesGenerales));
            $('#lblTotalCostesLinea_hdd').text(costesLineas);
        }
    }
    if (operacion == "%") {
        importeBruto = importeBruto * (valor / 100);
        costesGenerales = costesGenerales + importeBruto;
        costesLineas = costesLineas + importeBruto;
        $('#lblCosteTotalLinea_hdd').text(costesGenerales);
        $('#lblCosteTotalLinea').text(formatNumber(costesGenerales) + " " + sMoneda);
        $('#lblTotalCostesLinea_hdd').text(costesLineas);
    } else {
        costesGenerales = costesGenerales + valor;
        costesLineas = costesLineas + valor;
        $('#lblCosteTotalLinea_hdd').text(costesGenerales);
        $('#lblCosteTotalLinea').text(formatNumber(costesGenerales) + " " + sMoneda);
        $('#lblTotalCostesLinea_hdd').text(costesLineas);
    }
    $('#lblCosteTotalLinea_' + iCodFilaSeleccionada).text(formatNumber(costesGenerales) + " " + sMoneda);
    $('#lblCosteTotalLinea_' + iCodFilaSeleccionada + '_hdd').text(costesGenerales);

    recorrerImpuestosCabeceraPorcentajes();
    $('#pesCostes').click();
    $('#pesDescuentos').click();
    $('#pesDatosGenerales').click();
}
function recalcularImportesCosteRestaLinea(importe) {
    var parts = importe.split(" ");
    var valor = parseFloat(strToNum($.trim(parts[0])));
    var operacion = $.trim(parts[1]);
    var importeBruto = parseFloat($('#lblImpBrutolin_' + iCodFilaSeleccionada.toString() + '_hdd').text());
    var costesGenerales = parseFloat($('#lblCosteTotalLinea_hdd').text());
    var costesLineas = parseFloat($('#lblTotalCostesLinea_hdd').text());
    var impBrutoTotal = parseFloat($.trim($('#lblImpBruto_hdd').text()));
    if (operacion == "%") {
        importeBruto = importeBruto * (valor / 100);
        costesGenerales = costesGenerales - importeBruto;
        costesLineas = costesLineas - importeBruto;
        impBrutoTotal = impBrutoTotal - importeBruto;
    } else {
        costesGenerales = costesGenerales - valor;
        costesLineas = costesLineas - valor;
        impBrutoTotal = impBrutoTotal - valor;
    }
    $('#lblCosteTotalLinea_hdd').text(costesGenerales);
    $('#lblCosteTotalLinea').text(formatNumber(costesGenerales) + " " + sMoneda);
    $('#lblTotalCostesLinea_hdd').text(costesLineas);

    $('#lblCosteTotalLinea_' + iCodFilaSeleccionada).text(formatNumber(costesGenerales) + " " + sMoneda);
    $('#lblCosteTotalLinea_' + iCodFilaSeleccionada + '_hdd').text(costesGenerales);
    recorrerImpuestosCabeceraPorcentajes();
}
function CargarAtributosDescuentoLinea() {
    var comboVal = $("#CmbBoxTipoPedido").val().toString();
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarAtributosDescuentosLinea',
        contentType: "application/json; charset=utf-8",
        data: "{'iDesdeFavorito':'" + bModificarFavorito + "','iCodLinea':'" + Linea.ID + "','sIDTipoPedido':'" + comboVal.toString() + "'}",
        dataType: "json",
        async: false
    })).done(function (msg) {
        gDescuentosLinea = $.parseJSON(msg.d);
        Linea.Descuentos = gDescuentosLinea;
        pintarDescuentosLinea();
    });
}
function pintarDescuentosLinea() {
    var indAtributo = 0;
    var ImporteFijo = 0;

    if (gDescuentosLinea.length == 0) { //Si no hay atributos ocultamos el Div de Atributos    
        $('[id$=divAtributosDescuentoLinea]').hide();
        $('#pesResumenLineas').click();
    } else {
        $('[id$=divAtributosDescuentoLinea]').show();
    }

    //Se eliminan las filas si las hubiese de la tabla(menos la cabecera)
    var table = $("#tblAtributosDescuentoLinea")
    table.find('tr:not(:first)').remove();
    var GrupoCosteDescuentoAnt = "";
    var Inicio = "0";
    $.each(gDescuentosLinea, function () {
        indAtributo += 1;
        if (this.Operacion == "-") {
            if (this.Valor == undefined) var Importe = 0;
            else var Importe = this.Valor;
        } else {
            if (this.Valor == undefined) {
                var Importe = 0;
            } else {
                var Importe = parseFloat($('#lblImpBrutolin_' + iCodFilaSeleccionada.toString() + '_hdd').text());
                Importe = Importe * (this.Valor / 100);
            }
        }

        this.Importe = Importe;

        if (this.GrupoCosteDescuento == undefined) var GrupoCosteDescuento = "";
        else var GrupoCosteDescuento = this.GrupoCosteDescuento;

        if (this.Inicio == undefined) {
            if (GrupoCosteDescuentoAnt !== "" && this.GrupoCosteDescuento !== undefined) {
                if (this.GrupoCosteDescuento == GrupoCosteDescuentoAnt) Inicio = "1";
                else Inicio = "0";
            } else {
                Inicio = "0";
            }
        } else {
            Inicio = this.Inicio;
        }
        this.Inicio = Inicio;
        if (this.Seleccionado == undefined) {
            var Seleccionado = false;
            this.Seleccionado = Seleccionado;
        }
        this.Moneda = sMoneda;
        GrupoCosteDescuentoAnt = GrupoCosteDescuento;
        switch (this.TipoCosteDescuento) {
            case 0:
                $("#tmplCampoDescuFijoLinea").tmpl(this).appendTo("#tblAtributosDescuentoLinea");
                ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                break;
            case 1:
                if (this.Seleccionado == true) ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);

                $("#tmplCampoDescuExcLinea").tmpl(this).appendTo("#tblAtributosDescuentoLinea");
                break;
            case 2:
                if (this.Seleccionado == true) ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);

                $("#tmplCampoDescuOpcLinea").tmpl(this).appendTo("#tblAtributosDescuentoLinea");
                break;
            case 3:
                if (this.Seleccionado == true) ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);

                $("#tmplCampoDescuOpcExcLinea").tmpl(this).appendTo("#tblAtributosDescuentoLinea");
                break;
        }
    });
    var impBruto = parseFloat($.trim($('#lblImpBruto_hdd').text()));

    $('#lblDescuentoTotalLinea_hdd').text(ImporteFijo);
    $('#lblDescuentoTotalLinea').text(formatNumber(ImporteFijo) + " " + sMoneda);
}
function recalcularImportesDescuentoAnyadirLinea(importe, restar) {
    var parts = importe.split(" ");
    var valor = parseFloat(strToNum($.trim(parts[0])));
    var operacion = $.trim(parts[1]);
    var importeBruto = parseFloat($('#lblImpBrutolin_' + iCodFilaSeleccionada.toString() + '_hdd').text());
    var descuentosGenerales = parseFloat($('#lblDescuentoTotalLinea_hdd').text());
    var descuentosLineas = parseFloat($('#lblTotalDescuentosLinea_hdd').text());
    var impBrutoTotal = parseFloat($.trim($('#lblImpBruto_hdd').text()));
    if (restar == undefined) {
        restar = 0;
    } else {
        if (restar !== 0) {
            var partsResta = restar.split(" ");
            var valorResta = parseFloat(strToNum($.trim(partsResta[0])));
            var operacionResta = $.trim(partsResta[1]);
            if (operacionResta == "%") {
                importeBruto = importeBruto * (valorResta / 100);
                descuentosGenerales = descuentosGenerales - importeBruto;
                descuentosLineas = descuentosLineas - importeBruto;
                impBrutoTotal = impBrutoTotal + importeBruto;
            } else {
                descuentosGenerales = descuentosGenerales - valorResta;
                descuentosLineas = descuentosLineas - valorResta;
                impBrutoTotal = impBrutoTotal + valorResta;
            }
            $('#lblDescuentoTotalLinea_hdd').text(descuentosGenerales);
            $('#lblDescuentoTotalLinea').text(formatNumber(descuentosGenerales) + " " + sMoneda);
            $('#lblTotalDescuentosLinea_hdd').text(descuentosLineas);
        }
    }
    if (operacion == "%") {
        importeBruto = importeBruto * (valor / 100);
        descuentosGenerales = descuentosGenerales + importeBruto;
        descuentosLineas = descuentosLineas + importeBruto;
        $('#lblDescuentoTotalLinea_hdd').text(descuentosGenerales);
        $('#lblDescuentoTotalLinea').text(formatNumber(descuentosGenerales) + " " + sMoneda);
        $('#lblTotalDescuentosLinea_hdd').text(descuentosLineas);
    } else {
        descuentosGenerales = descuentosGenerales + valor;
        descuentosLineas = descuentosLineas + valor;
        $('#lblDescuentoTotalLinea_hdd').text(descuentosGenerales);
        $('#lblDescuentoTotalLinea').text(formatNumber(descuentosGenerales) + " " + sMoneda);
        $('#lblTotalDescuentosLinea_hdd').text(descuentosLineas);
    }

    $('#lblDescuentoTotalLinea_' + iCodFilaSeleccionada).text(formatNumber(descuentosGenerales) + " " + sMoneda);
    $('#lblDescuentoTotalLinea_' + iCodFilaSeleccionada + '_hdd').text(descuentosGenerales);
    recorrerImpuestosCabeceraPorcentajes();
}
function recalcularImportesDescuentoRestaLinea(importe) {
    var parts = importe.split(" ");
    var valor = parseFloat(strToNum($.trim(parts[0])));
    var operacion = $.trim(parts[1]);
    var importeBruto = parseFloat($('#lblImpBrutolin_' + iCodFilaSeleccionada.toString() + '_hdd').text());
    var descuentoGeneral = parseFloat($('#lblDescuentoTotalLinea_hdd').text());
    var descuentosLineas = parseFloat($('#lblTotalDescuentosLinea_hdd').text());
    var impBrutoTotal = parseFloat($.trim($('#lblImpBruto_hdd').text()));

    if (operacion == "%") {
        importeBruto = importeBruto * (valor / 100);
        descuentoGeneral = descuentoGeneral - importeBruto;
        descuentosLineas = descuentosLineas - importeBruto;
        impBrutoTotal = impBrutoTotal + importeBruto;
    } else {
        descuentoGeneral = descuentoGeneral - valor;
        descuentosLineas = descuentosLineas - valor;
        impBrutoTotal = impBrutoTotal + valor;
    }
    $('#lblCosteTotalLinea_hdd').text(descuentoGeneral);
    $('#lblCosteTotalLinea').text(formatNumber(descuentoGeneral) + " " + sMoneda);
    $('#lblTotalDescuentosLinea_hdd').text(descuentosLineas);

    $('#lblDescuentoTotalLinea_' + iCodFilaSeleccionada).text(formatNumber(descuentoGeneral) + " " + sMoneda);
    $('#lblDescuentoTotalLinea_' + iCodFilaSeleccionada + '_hdd').text(descuentoGeneral);
    recorrerImpuestosCabeceraPorcentajes();
}
function recalcularImportes() {
    var impBruto = parseFloat($.trim($('#lblImpBrutoLinea_hdd').text()));
    var coste = parseFloat($.trim($('#lblCosteCabeceraTotal_hdd').text()));
    var descuentos = parseFloat($.trim($('#lblDescuentosCabeceraTotal_hdd').text()));
    var costesLinea = parseFloat($.trim($('#lblTotalCostesLinea_hdd').text()));
    impBruto = impBruto + costesLinea;

    var descuentosLinea = parseFloat($.trim($('#lblTotalDescuentosLinea_hdd').text()));
    impBruto = impBruto - descuentosLinea;

    var cuotasPedido = parseFloat($.trim($('#lblImpTotalCuotas_hdd').text()));
    var valor = impBruto + coste - descuentos + cuotasPedido;
    $('#lblImpBruto_hdd').text(impBruto);
    $('#lblImpBruto').text(formatNumber(impBruto) + " " + sMoneda);
    $('#lblImporteTotalCabecera').text(formatNumber(valor) + " " + sMoneda);
    $('#lblImporteTotalCabecera_hdd').text(valor);
}
function recalcularImportesPorcentajes() {
    var impBruto = parseFloat($.trim($('#lblImpBrutoLinea_hdd').text()));
    var costesLinea = parseFloat($.trim($('#lblTotalCostesLinea_hdd').text()));
    impBruto = impBruto + costesLinea;
    var descuentosLinea = parseFloat($.trim($('#lblTotalDescuentosLinea_hdd').text()));
    impBruto = impBruto - descuentosLinea;

    recalcularPorcentajesCabecera(impBruto);
    var coste = parseFloat($.trim($('#lblCosteCabeceraTotal_hdd').text()));
    var descuentos = parseFloat($.trim($('#lblDescuentosCabeceraTotal_hdd').text()));
    var valor = impBruto + coste - descuentos;

    $('#lblImpBruto').text(formatNumber(impBruto) + " " + sMoneda);
    $('#lblImpBruto_hdd').text(impBruto);
    $('#lblImporteTotalCabecera').text(formatNumber(valor) + " " + sMoneda);
    $('#lblImporteTotalCabecera_hdd').text(valor);
}
function recalcularPorcentajesCabecera(importeBruto) {
    var restar = 0;
    var importeCalculado = 0;

    $('#pesCostes').click();
    $('#pesDescuentos').click();
    $('#pesDatosGenerales').click();

    var costesGenerales = parseFloat($('#lblCosteCabeceraTotal_hdd').text());
    $.each($('[id^=valNumAtribCosteOblExc_]'), function () {
        var parts = this.id.split("_");
        var grup = $.trim(parts[1]);
        var idAnt = $.trim(parts[2]);
        var restar = $('#inputNumAtribCosteOblExc_' + grup + '_' + idAnt).text().split(" ");
        var cantidad = parseFloat(strToNum($.trim(restar[0])));
        var parts = $('#valNumAtribCosteOblExc_' + grup + '_' + idAnt).text().split(" ");
        var valor = parseFloat(strToNum($.trim(parts[0])));
        var operacion = $.trim(parts[1]);
        if (operacion == "%") {
            importeCalculado = importeBruto * (valor / 100);
            $('#inputNumAtribCosteOblExc_' + grup + '_' + idAnt).text(formatNumber(importeCalculado) + " " + sMoneda);
            if ($('#inputNumAtribCosteOblExc_' + grup + '_' + idAnt).css("display") == "block") {
                costesGenerales = costesGenerales - cantidad;
                costesGenerales = costesGenerales + importeCalculado;
            }
        }
    });
    $.each($('[id^=valNumAtribCosteOpcExc_]'), function () {
        var parts = this.id.split("_");
        var grup = $.trim(parts[1]);
        var idAnt = $.trim(parts[2]);
        var restar = $('#inputNumAtribCosteOpcExc_' + grup + '_' + idAnt).text().split(" ");
        var cantidad = parseFloat(strToNum($.trim(restar[0])));
        var parts = $('#valNumAtribCosteOpcExc_' + grup + '_' + idAnt).text().split(" ");
        var valor = parseFloat(strToNum($.trim(parts[0])));
        var operacion = $.trim(parts[1]);
        if (operacion == "%") {
            importeCalculado = importeBruto * (valor / 100);
            $('#inputNumAtribCosteOpcExc_' + grup + '_' + idAnt).text(formatNumber(importeCalculado) + " " + sMoneda);
            if ($('#inputNumAtribCosteOpcExc_' + grup + '_' + idAnt).css("display") == "block") {
                costesGenerales = costesGenerales - cantidad;
                costesGenerales = costesGenerales + importeCalculado;
            }
        }
    });
    $.each($('[id^=valNumAtribCosteOpc_]'), function () {
        var parts = this.id.split("_");
        var grup = $.trim(parts[1]);
        var restar = $('#inputNumAtribCosteOpc_' + grup).text().split(" ");
        var cantidad = parseFloat(strToNum($.trim(restar[0])));
        var parts = $('#valNumAtribCosteOpc_' + grup).text().split(" ");
        var valor = parseFloat(strToNum($.trim(parts[0])));
        var operacion = $.trim(parts[1]);
        if (operacion == "%") {
            importeCalculado = importeBruto * (valor / 100);
            $('#inputNumAtribCosteOpc_' + grup).text(formatNumber(importeCalculado) + " " + sMoneda);
            if ($('#inputNumAtribCosteOpc_' + grup).css("display") == "block") {
                costesGenerales = costesGenerales - cantidad;
                costesGenerales = costesGenerales + importeCalculado;
            }
        }
    });
    $.each($('[id^=valNumAtribCoste_]'), function () {
        var parts = this.id.split("_");
        var grup = $.trim(parts[1]);
        var restar = $('#inputNumAtribCosteF_' + grup).text().split(" ");
        var cantidad = parseFloat(strToNum($.trim(restar[0])));
        var parts = $('#valNumAtribCoste_' + grup).text().split(" ");
        var valor = parseFloat(strToNum($.trim(parts[0])));
        var operacion = $.trim(parts[1]);
        if (operacion == "%") {
            importeCalculado = importeBruto * (valor / 100);
            $('#inputNumAtribCosteF_' + grup).text(formatNumber(importeCalculado) + " " + sMoneda);
            if ($('#inputNumAtribCosteF_' + grup).css("display") == "block") {
                costesGenerales = costesGenerales - cantidad;
                costesGenerales = costesGenerales + importeCalculado;
            }
        }
    });

    $('#lblCosteTotal_hdd').text(costesGenerales);
    $('#lblCosteTotal').text(formatNumber(costesGenerales) + " " + sMoneda);
    $('#lblCosteCabeceraTotal_hdd').text(costesGenerales);
    $('#lblCosteCabeceraTotal').text(formatNumber(costesGenerales) + " " + sMoneda);

    var descuentosGenerales = parseFloat($('#lblDescuentosCabeceraTotal_hdd').text());

    $.each($('[id^=valNumAtribDescuExc_]'), function () {
        var parts = this.id.split("_");
        var grup = $.trim(parts[1]);
        var idAnt = $.trim(parts[2]);
        var restar = $('#inputNumAtribDescuExc_' + grup + '_' + idAnt).text().split(" ");
        var cantidad = parseFloat(strToNum($.trim(restar[0])));
        var parts = $('#valNumAtribDescuExc_' + grup + '_' + idAnt).text().split(" ");
        var valor = parseFloat(strToNum($.trim(parts[0])));
        var operacion = $.trim(parts[1]);
        if (operacion == "%") {
            importeCalculado = importeBruto * (valor / 100);
            $('#inputNumAtribDescuExc_' + grup + '_' + idAnt).text(formatNumber(importeCalculado) + " " + sMoneda);
            if ($('#inputNumAtribDescuExc_' + grup + '_' + idAnt).css("display") == "block") {
                descuentosGenerales = descuentosGenerales - cantidad;
                descuentosGenerales = descuentosGenerales + importeCalculado;
            }
        }
    });
    $.each($('[id^=valNumAtribDescuOpcExc_]'), function () {
        var parts = this.id.split("_");
        var grup = $.trim(parts[1]);
        var idAnt = $.trim(parts[2]);
        var restar = $('#inputNumAtribDescuOpcExc_' + grup + '_' + idAnt).text().split(" ");
        var cantidad = parseFloat(strToNum($.trim(restar[0])));
        var parts = $('#valNumAtribDescuOpcExc_' + grup + '_' + idAnt).text().split(" ");
        var valor = parseFloat(strToNum($.trim(parts[0])));
        var operacion = $.trim(parts[1]);
        if (operacion == "%") {
            importeCalculado = importeBruto * (valor / 100);
            $('#inputNumAtribDescuOpcExc_' + grup + '_' + idAnt).text(formatNumber(importeCalculado) + " " + sMoneda);
            if ($('#inputNumAtribDescuOpcExc_' + grup + '_' + idAnt).css("display") == "block") {
                descuentosGenerales = descuentosGenerales - cantidad;
                descuentosGenerales = descuentosGenerales + importeCalculado;
            }
        }
    });
    $.each($('[id^=valNumAtribDescuOpc_]'), function () {
        var parts = this.id.split("_");
        var grup = $.trim(parts[1]);
        var restar = $('#inputNumAtribDescuOpc_' + grup).text().split(" ");
        var cantidad = parseFloat(strToNum($.trim(restar[0])));
        var parts = $('#valNumAtribDescuOpc_' + grup).text().split(" ");
        var valor = parseFloat(strToNum($.trim(parts[0])));
        var operacion = $.trim(parts[1]);
        if (operacion == "%") {
            importeCalculado = importeBruto * (valor / 100);
            $('#inputNumAtribDescuOpc_' + grup).text(formatNumber(importeCalculado) + " " + sMoneda);
            if ($('#inputNumAtribDescuOpc_' + grup).css("display") == "block") {
                descuentosGenerales = descuentosGenerales - cantidad;
                descuentosGenerales = descuentosGenerales + importeCalculado;
            }
        }
    });
    $.each($('[id^=valNumAtribDescu_]'), function () {
        var parts = this.id.split("_");
        var grup = $.trim(parts[1]);
        var restar = $('#inputNumAtribDescu_' + grup).text().split(" ");
        var cantidad = parseFloat(strToNum($.trim(restar[0])));
        var parts = $('#valNumAtribDescu_' + grup).text().split(" ");
        var valor = parseFloat(strToNum($.trim(parts[0])));
        var operacion = $.trim(parts[1]);
        if (operacion == "%") {
            importeCalculado = importeBruto * (valor / 100);
            $('#inputNumAtribDescu_' + grup).text(formatNumber(importeCalculado) + " " + sMoneda);
            if ($('#inputNumAtribDescu_' + grup).css("display") == "block") {
                descuentosGenerales = descuentosGenerales - cantidad;
                descuentosGenerales = descuentosGenerales + importeCalculado;
            }
        }
    });
    $('#lblDescuentoTotal_hdd').text(descuentosGenerales);
    $('#lblDescuentoTotal').text(formatNumber(descuentosGenerales) + " " + sMoneda);
    $('#lblDescuentosCabeceraTotal_hdd').text(descuentosGenerales);
    $('#lblDescuentosCabeceraTotal').text(formatNumber(descuentosGenerales) + " " + sMoneda);
}
//funcion que carga los atributos a nivel de Item(Panel edicion Item)
function cargarAtributosLinea() {
    if (Linea.OtrosDatos == undefined) {
        var comboVal = $("#CmbBoxTipoPedido").val().toString();
        var empresa = 0;
        if ($("#CmbBoxEmpresa").val() !== null) empresa = $("#CmbBoxEmpresa").val();
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarAtributosLinea',
            contentType: "application/json; charset=utf-8",
            data: "{'iDesdeFavorito':'" + bModificarFavorito + "','sIDTipoPedido':'" + comboVal.toString() + "', 'iCodLinea':'" + iCodFilaSeleccionada.toString() + "', 'bHayIntegracion':'" + bHayIntegracionPedidos + "', 'iEmpresa':'" + empresa + "', 'EsPedidoAbierto':" + EsPedidoAbierto + "}",
            dataType: "json",
            async: true
        })).done(function (msg) {
            Linea.OtrosDatos = $.parseJSON(msg.d);
            MostrarAtributosLinea();
        });
    } else MostrarAtributosLinea();
}
function MostrarAtributosLinea() {
    var indAtributo = 0;
    if (Linea.OtrosDatos.length == 0) $('[id$=divAtributosLinea]').hide(); //Si no hay atributos ocultamos el Div de Atributos    
    else {
        $('[id$=divAtributosLinea]').show();
        //Se eliminan las filas si las hubiese de la tabla(menos la cabecera)
        var table = $("#tblAtributosLinea")
        table.find('tr:not(:first)').remove();
        $.each(Linea.OtrosDatos, function () {
            indAtributo += 1;
            if (this.Intro == 1) {//Opcion Lista
                if (this.ListaExterna) {
                    var valorListaExterna;
                    var oAtributoListaExterna = this;

                    //Tarea "Atrib Por Defecto"
                    //  Al entrar, si es atrib por defecto. VALOR_TEXT ES NULO y VALOR_COD NO ES NULO
                    var atributosLlamada = '';
                    $.each(Linea.OtrosDatos, function () {
                        if (atributosLlamada == '')
                            atributosLlamada = this.Id + '@########@' + this.Valor;
                        else {
                            atributosLlamada = atributosLlamada + "##########" + this.Id + '@########@' + this.Valor;
                        }
                    });
                    var sOrgCompras = '';
                    var sCentro = '';
                    if (bHayIntegracionPedidos && gDatosGenerales.UsarOrgCompras) {
                        sOrgCompras = gDatosGenerales.OrgCompras;
                        sCentro = Linea.CentroAprovisionamiento;
                    };

                    if (this.Valor !== '') {
                        if (this.Valores.length == 1) {
                            if (this.Valores[0].Valor == '') {
                                var Resultado = '';
                                var bOk = false;

                                var param = JSON.stringify({ 'Empresa': gDatosGenerales.Emp, 'IdAtributoListaExterna': this.Id, 'atributos': atributosLlamada, 'codigoBuscado': this.Valor, 'denominacionBuscado': '', 'buscador': true, 'OrgCompras': sOrgCompras, 'Centro': sCentro });
                                $.when($.ajax({
                                    type: "POST",
                                    url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Obtener_Datos_ListaExterna',
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    data: param,
                                    async: false
                                })).done(function (msg) {
                                    if (msg.d.length == 1) {
                                        bOk = true;
                                        Resultado = msg.d[0].id + ' - ' + msg.d[0].name;
                                    }
                                });

                                if (bOk) {
                                    this.Valores[0].Valor = Resultado;
                                } else {
                                    this.Valor = '';
                                    this.Valores.length = 0;
                                }
                            }
                        }
                    }
                    //fin -- Tarea "Atrib Por Defecto"

                    if (this.Valores.length > 0) valorListaExterna = { 'id': this.Valor, 'name': this.Valores[0].Valor };
                    $('#tmplListaExternaLinea').tmpl(this).appendTo($('#tblAtributosLinea'));
                    if (!popupListaExternaCargado) {
                        popupListaExternaCargado = true;
                        $.get(rutaFS + 'EP/html/buscadorListaExterna.htm', function (buscadorPopUp) {
                            buscadorPopUp = buscadorPopUp.replace(/src="/gi, 'src="' + ruta);
                            $('body').append(buscadorPopUp);
                        });
                    }
                    var listaExternaAtrib = $('#atribListaExterna_' + this.Id, $('#tblAtributosLinea'));
                    var IdAtributoListaExterna = this.Id;
                    listaExternaAtrib.tokenInput(function () {
                        guardarOtrosDatosLineas(true);
                        var atributosLlamada = '';
                        $.each(Linea.OtrosDatos, function () {
                            if (atributosLlamada == '')
                                atributosLlamada = this.Id + '@########@' + this.Valor;
                            else {
                                atributosLlamada = atributosLlamada + "##########" + this.Id + '@########@' + this.Valor;
                            }
                        });
                        var sOrgCompras = '';
                        var sCentro = '';
                        if (bHayIntegracionPedidos && gDatosGenerales.UsarOrgCompras) {
                            sOrgCompras = gDatosGenerales.OrgCompras;
                            sCentro = Linea.CentroAprovisionamiento;
                        };
                        return rutaFS + 'EP/EmisionPedido.aspx/Obtener_Datos_ListaExterna?Empresa=' + gDatosGenerales.Emp + '&IdAtributoListaExterna=' + IdAtributoListaExterna + '&atributos=' + atributosLlamada +
                            '&codigoBuscado=&denominacionBuscado=&buscador=false&OrgCompras=' + sOrgCompras + '&Centro=' + sCentro
                    }, {
                            queryParam: 'codigoBuscado',
                            hintText: TextosJScript[63],
                            searchingText: TextosJScript[64],
                            noResultsText: TextosJScript[65],
                            minChars: 3,
                            tokenLimit: 1,
                            resultsLimit: 50,
                            preventDuplicates: true,
                            method: "POST",
                            onReady: function () {
                                if (valorListaExterna) setTimeout(function () { listaExternaAtrib.tokenInput('addRange', [{ 'id': valorListaExterna.id, 'name': valorListaExterna.name }]) }, 50);
                                //Ponemos el display de la lista inline-block
                                $("ul[class^=token-input-list]", listaExternaAtrib.parent()).css('display', 'inline-block');
                                $("ul[class^=token-input-list]", listaExternaAtrib.parent()).css('vertical-align', 'middle');
                                $("ul[class^=token-input-list]", listaExternaAtrib.parent()).css('max-height', '10em');
                                $("ul[class^=token-input-list]", listaExternaAtrib.parent()).css('overflow-y', 'auto');
                                $("ul[class^=token-input-list]", listaExternaAtrib.parent()).css('width', '90%');
                                //En las ventanas modales el dropdown se debe mostrar en primer plano
                                $("div[class^=token-input-dropdown]").css("z-index", "9999");
                            },
                            onDelete: function () { oAtributoListaExterna.Valor = ''; oAtributoListaExterna.Valores = []; }
                        });
                } else {
                    var iSeleccionado = "";
                    $('#tmplComboLineaEP').tmpl(this).appendTo($('#tblAtributosLinea'));
                    var item = this;
                    var items = "{"
                    //vamos componiendo con los valores de la combo el objeto Json
                    if (this.Tipo !== 3) {
                        for (i = 0; i <= item.Valores.length - 1; i++) {
                            //items += '"' + i + '": { "value":"' + item.Valores[i].Valor + '", "text": "' + item.Valores[i].Valor + '" }, '
                            items += '"' + i + '": { "value":"' + i.toString() + '", "text": "' + item.Valores[i].Valor.toString().replace(/"/g, "'") + '" }, '
                            if (item.Valor !== undefined) {
                                if (item.Valor.toString() == item.Valores[i].Valor) {
                                    iSeleccionado = i;
                                }
                            }
                        }
                    } else {
                        for (i = 0; i <= item.Valores.length - 1; i++) {
                            var fecha = eval("new " + item.Valores[i].Valor.slice(1, -1));
                            var strFecha = fecha.format(UsuMask);

                            items += '"' + i + '": { "value":"' + strFecha + '", "text": "' + strFecha + '" }, ';
                        }

                    }
                    items = items.substr(0, items.length - 2) //quitamos la ultima coma que sobra
                    items += "}"
                    var optionsPermiso = { valueField: "value", textField: "text", adjustWidthToSpace: true, dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: false, data: $.parseJSON(items) };
                    $('#cboAtribLineaEP_' + item.Id).fsCombo(optionsPermiso);
                    if (this.Tipo == 3) {
                        if (item.Valor !== null) {
                            if (item.Valor.toString().toLowerCase().indexOf("date") >= 0) {
                                var fecha = eval("new " + item.Valor.slice(1, -1));
                                var strFecha = fecha.format(UsuMask);
                            } else {
                                var strFecha = item.Valor;
                            }
                            $('#cboAtribLineaEP_' + item.Id).fsCombo('selectValue', strFecha);
                        } else {
                            $('#cboAtribLineaEP_' + item.Id).fsCombo('selectValue', item.Valor);
                        }

                    } else {
                        $('#cboAtribLineaEP_' + item.Id).fsCombo('selectValue', iSeleccionado.toString());
                    }
                }
            } else {
                switch (this.Tipo.toString()) {//this.Tipo) {
                    case "1": //Texto
                        $("#tmplCampoTextoLineaEP").tmpl(this).appendTo("#tblAtributosLinea");
                        break;
                    case "2": //Numerico
                        $("#tmplCampoNumericoLineaEP").tmpl(this).appendTo("#tblAtributosLinea");
                        break;
                    case "3": //Fecha
                        $("#tmplCampoFechaLineaEP").tmpl(this).appendTo("#tblAtributosLinea");
                        var item = this;
                        var idioma
                        switch (TextosJScript[2]) {
                            case 'SPA':
                                idioma = 'es';
                                break;
                            case 'ENG':
                                idioma = 'en';
                                break;
                            case 'GER':
                                idioma = 'de';
                                break;
                            default:
                                idioma = 'es';
                                break;
                        }
                        $('#inputFechaAtribLineaEP_' + item.Id).datepicker({
                            showOn: 'both',
                            buttonImage: ruta + 'images/colorcalendar.png',
                            buttonImageOnly: true,
                            buttonText: '',
                            dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
                            showAnim: 'slideDown'
                        }, $.datepicker.regional[idioma]);
                        if (item.Valor != null && item.Valor != undefined)
                            if (item.Valor.toString().indexOf('Date') != -1) //Viene de BD en milisegundos
                                $('#inputFechaAtribLineaEP_' + this.Id).datepicker('setDate', eval("new " + item.Valor.slice(1, -1)));
                            else
                                $('#inputFechaAtribLineaEP_' + this.Id).datepicker('setDate', item.Valor); //eval("new Date('" + item.Valor + "')"));
                        break;
                    case "4": //Boolean
                        $("#tmplComboLineaEP").tmpl(this).appendTo("#tblAtributosLinea");
                        var item = this;
                        var items = "{"
                        //vamos componiendo con los valores de la combo el objeto Json
                        items += '"' + 0 + '": { "value":"' + 1 + '", "text": "' + TextosBoolean[1] + '" }, ';
                        items += '"' + 1 + '": { "value":"' + 0 + '", "text": "' + TextosBoolean[0] + '" } ';
                        items += "}"
                        var optionsPermiso = { valueField: "value", textField: "text", adjustWidthToSpace: true, dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: false, data: $.parseJSON(items) };
                        $('#cboAtribLineaEP_' + item.Id).fsCombo(optionsPermiso);
                        //Se pone el valor si lo tuviera el atributo
                        $('#cboAtribLineaEP_' + item.Id).fsCombo('selectValue', item.Valor.toString());
                        break;
                }
            }
        });
    };
    OcultarCargando();
};
/*Coloca la partida seleccionada en el Textbox de la partida y oculta el panel de búsqueda*/
function seleccionarPartidaEP(sender) {
    var devu = htmlDecode(sender.getAttribute("cod")).split("||");
    var datos = htmlDecode(sender.getAttribute("datos")); //.split("@@");
    var sCodigoPres = $.trim(devu[0]);
    var sPresCod = $.trim(devu[1]);
    var sPresDen = htmlDecode(sender.innerHTML);
    var sCentroDen = htmlDecode(sender.getAttribute("centroDen"));
    var sCentroCod = htmlDecode(sender.getAttribute("centroCod"));

    //CALIDAD
    var sPRES0 = sender.parentElement.id;
    var tbPPres = $('#tbPPres');
    tbPPres.val(sPresDen);
    tbPPres = null;
    var tbPPresHidden = $('#tbPPres_Hidden');
    tbPPresHidden.val(sPresCod);
    tbPPresHidden = null;
    ocultarPanelModal('mpePartidas');
    gLineas = gEmisionPedido.Lineas;
    for (var i = 0; i < gLineas.length; i++) {
        if (gLineas[i].ID == Linea.ID) {
            var CentroContrato = gLineas[i].Pres5_ImportesImputados_EP[0];
            var Partida = {};
            if (CentroContrato == undefined) {
                //CentroContrato = JSON.parse(JSON.stringify(gLineas[i].Pres5_ImportesImputados_EP));
                CentroContrato = {};
                CentroContrato = CrearObjCentroPartidaVacio(CentroContrato);
            } else {
                if (CentroContrato.CentroCoste == undefined) {
                    CentroContrato = {};
                    CentroContrato = CrearObjCentroPartidaVacio(CentroContrato);
                } else {
                    Partida = CrearObjPartidaVacio(Partida);
                    CentroContrato.Partida = Partida;
                }
            }
            var CentroCoste = CentroContrato.CentroCoste;
            var CentroSM = CentroCoste.CentroSM;
            Partida = CentroContrato.Partida;

            var tbCtroCoste = $('#tbCtroCoste');
            if (tbCtroCoste.val() == "") {
                tbCtroCoste.val(sCentroDen);
                var tbCtroCosteHidden = $('#tbCtroCoste_Hidden');
                tbCtroCosteHidden.val(sCentroCod);

                if ($('#tbCtroCoste').val() !== "") {
                    var parts = $('#tbCtroCoste').val().split(" - ");
                    //CentroCoste.UON1 = $.trim(parts[0]);
                    CentroSM.Denominacion = $.trim(parts[1]);
                } else {
                    CentroCoste.UON1 = "";
                    CentroSM.Denominacion = "";
                }

                if ($('#tbCtroCoste_Hidden').val() !== "") {
                    var parts = $('#tbCtroCoste_Hidden').val().split("@@");
                    CentroSM.Codigo = $.trim(parts[0]);
                    CentroCoste.UON1 = $.trim(parts[1]);
                    if (parts.length > 2) {
                        CentroCoste.UON2 = $.trim(parts[2]);
                        if (parts.length > 3) {
                            CentroCoste.UON3 = $.trim(parts[3]);
                            if (parts.length > 4) {
                                CentroCoste.UON4 = $.trim(parts[4]);
                            }
                        }
                    }
                } else {
                    CentroCoste = CrearObjCentroCosteVacio(CentroCoste);
                }
                CentroCoste.CentroSM = CentroSM;
                CentroContrato.CentroCoste = CentroCoste;
            }


            if ($('#tbPPres').val() !== "") {
                var parts = datos.split("@@");
                //Partida.PresupuestoId = $.trim(parts[0]);
                Partida.Denominacion = $.trim(parts[1]).replace(/"/g, "'");
                if ($('#tbPPres_Hidden').val() !== "") {
                    var partsBis = $('#tbPPres_Hidden').val().split("@@");
                    Partida.PresupuestoId = sCodigoPres;
                    //Partida.Codigo = $('#tbPPres_Hidden').val();
                    Partida.NIV0 = $.trim(partsBis[0]);
                    Partida.NIV1 = $.trim(partsBis[1]);
                    if (partsBis.length > 2) {
                        Partida.NIV2 = $.trim(partsBis[2]);
                        if (partsBis.length > 3) {
                            Partida.NIV3 = $.trim(partsBis[3]);
                            if (partsBis.length > 4) {
                                Partida.NIV4 = $.trim(partsBis[4]);
                            }
                        }
                    }

                }
                if (parts.length > 2) {
                    var fechaInicio = $.trim(parts[2]);  //$.trim(parts[2]) + "-" + $.trim(parts[3]) + "-" + $.trim(parts[4]);
                    Partida.FechaInicioPresupuesto = fechaInicio; //.replace("(", "");
                } else {
                    Partida.FechaInicioPresupuesto = "";
                }

                if (parts.length > 3) {
                    var fechaFin = $.trim(parts[3]); //$.trim(parts[5]) + "-" + $.trim(parts[6]) + "-" + $.trim(parts[7]);
                    Partida.FechaFinPresupuesto = fechaFin; //.replace(")", "");
                } else {
                    Partida.FechaFinPresupuesto = "";
                }
            } else {
                Partida = CrearObjPartidaVacio(Partida);
            }
            CentroContrato.Partida = Partida;
            CentroContrato.LineaId = parseInt(gLineas[i].ID.toString(), 10);
            CentroContrato.Id = 1;

            gCentroContrato = CentroContrato;
            ComprobarCentroContrato(1);
            CentroContrato = gCentroContrato;
            gLineas[i].Pres5_ImportesImputados_EP[0] = CentroContrato;
            Linea.Pres5_ImportesImputados_EP[0] = CentroContrato;
            if ($('#tbPPres_Hidden').val() !== "") {
                var partidas = $('#tbPPres_Hidden').val().replace(/@@/g, '#');
                ComprobarGestorLinea(partidas);
            }
            break;
        }
    }
    gEmisionPedido.Lineas = gLineas;
}
function ComprobarCentroContrato(pintar) {
    //DEVOLVER 0 --> GREEN a centro y DEVOLVER VACIAR a partida
    //DEVOLVER 1 --> GREEN a centro y DEVOLVER --> GREEN a partida
    //DEVOLVER 2 --> GREEN a centro y DEVOLVER --> Orange a partida
    //DEVOLVER 3 --> GREEN a centro y DEVOLVER --> Red a partida
    //DEVOLVER 4 --> Red a centro y vaciar partida
    var iRespuesta = true;
    var tbCtroCoste = $('#tbCtroCoste');
    var tbCtroCosteHidden = $('#tbCtroCoste_Hidden');
    var tbPPres = $('#tbPPres');
    var tbPPres_Hidden = $('#tbPPres_Hidden');
    gCentroContratoComprobar = jQuery.extend(true, {}, gCentroContrato);
    if (gCentroContratoComprobar !== undefined) {
        if ($('#tbCtroCoste').val() == "" && pintar == 0) {
            var CentroContrato = {};
            CentroContrato = CrearObjCentroPartidaVacio(CentroContrato);
            gCentroContratoComprobar = CentroContrato;
            gCentroContrato = CentroContrato;
            tbCtroCoste.val('');
            tbPPres.val('');
            tbPPres_Hidden.val('');
            tbCtroCoste.css('background-color', '');
            tbPPres.css('background-color', '');
            iRespuesta = false;
        } else {
            if ($('#tbPPres').val() == "" && pintar == 0) {
                var Partida = {};
                Partida = CrearObjPartidaVacio(Partida);
                gCentroContratoComprobar.Partida = Partida;
                gCentroContrato.Partida = Partida;
                tbPPres.val('');
                tbPPres_Hidden.val('');
                tbPPres.css('background-color', '');
            } else {
                if (gCentroContratoComprobar.Partida !== undefined) {
                    gCentroContratoComprobar.Partida.Denominacion = "";
                    gCentroContratoComprobar.Partida.FechaInicioPresupuesto = "";
                    gCentroContratoComprobar.Partida.FechaFinPresupuesto = "";
                } else {
                    var Partida = {};
                    Partida = CrearObjPartidaVacio(Partida);
                    gCentroContratoComprobar.Partida = Partida;
                    gCentroContrato.Partida = Partida;
                }
            }
            var empresa = 0;
            if ($("#CmbBoxEmpresa").val() !== null) empresa = $("#CmbBoxEmpresa").val();
            var respuesta;
            var tbCtroCosteVal = "";
            if (tbCtroCoste.val().toString() != "") {
                var CtroCosteDen = tbCtroCoste.val().toString().split(" - ");
                var CtroCosteHidden = tbCtroCosteHidden.val().toString();
                var numInicio = CtroCosteHidden.indexOf("@@") + 2;
                tbCtroCosteVal = CtroCosteHidden.substring(numInicio, CtroCosteHidden.length) + ' - ' + CtroCosteDen[1];
            }
            $.when($.ajax({
                type: "POST",
                url: ruta + '/App_Pages/EP/EmisionPedido.aspx/ComprobarCentroCoste',
                contentType: "application/json; charset=utf-8",
                data: "{'emp':'" + empresa + "', 'sImputacion':'" + JSON.stringify(gCentroContratoComprobar) + "', 'txt':'" + tbCtroCosteVal + "', 'emitir':'false', 'CtrlPluriAnual':'false'}",
                dataType: "json",
                async: false
            })).done(function (msg) {
                iRespuesta = $.parseJSON(msg.d);
                switch (iRespuesta) {
                    case 0:
                        if (pintar == 1) {
                            tbCtroCoste.css('background-color', '#D8E4BC');
                            tbPPres.css('background-color', '');
                            tbPPres.val('');
                            tbPPres_Hidden.val('');
                            var Partida = {};
                            Partida = CrearObjPartidaVacio(Partida);
                            gCentroContrato.Partida = Partida;
                            if (gCentroContrato.CentroCoste == null) {
                                var CentroCoste = {};
                                CentroCoste = CrearObjCentroCosteVacio(CentroCoste);
                                CentroCoste.CentroSM.Denominacion = tbCtroCoste.val().toString();;
                                gCentroContrato.CentroCoste = CentroCoste;
                            }
                        }
                        respuesta = iRespuesta;
                        break;
                    case 1:
                        if (pintar == 1) {
                            tbCtroCoste.css('background-color', '#D8E4BC');
                            tbPPres.css('background-color', '#D8E4BC');
                        }
                        respuesta = iRespuesta;
                        break;
                    case 2:
                        if (pintar == 1) {
                            tbCtroCoste.css('background-color', '#D8E4BC');
                            tbPPres.css('background-color', '#FFA500');
                        }
                        respuesta = iRespuesta;
                        break;
                    case 3:
                        tbCtroCoste.css('background-color', '#D8E4BC');
                        tbPPres.css('background-color', '#FF8484');
                        respuesta = iRespuesta;
                        break;
                    case 4:
                        if (pintar == 1) {
                            tbCtroCoste.css('background-color', '#FF8484');
                            tbPPres.val('');
                            tbPPres_Hidden.val('');
                            var Partida = {};
                            Partida = CrearObjPartidaVacio(Partida);
                            gCentroContrato.Partida = Partida;
                        }
                        respuesta = iRespuesta;
                        break;
                    default:
                        if (pintar == 1) {
                            tbCtroCoste.css('background-color', '');
                            tbPPres.css('background-color', '');
                        }
                        respuesta = iRespuesta;
                        break;
                }
            });
            return respuesta;
        }
    }
}
function ComprobarCentroContratoAEmitir() {
    //Return 1 --> Centro OK y Partida OK
    var iRespuesta = true;
    var ccUON = "";
    ccUON = gCentroContrato.CentroCoste.UON1;
    if (gCentroContrato.CentroCoste.UON4 != null && gCentroContrato.CentroCoste.UON4 != "") {
        ccUON = ccUON + "@@" + gCentroContrato.CentroCoste.UON2 + "@@" + gCentroContrato.CentroCoste.UON3 + "@@" + gCentroContrato.CentroCoste.UON4;
    } else {
        if (gCentroContrato.CentroCoste.UON3 != null && gCentroContrato.CentroCoste.UON3 != "") {
            ccUON = ccUON + "@@" + gCentroContrato.CentroCoste.UON2 + "@@" + gCentroContrato.CentroCoste.UON3;
        } else {
            if (gCentroContrato.CentroCoste.UON2 != null && gCentroContrato.CentroCoste.UON2 != "") {
                ccUON = ccUON + "@@" + gCentroContrato.CentroCoste.UON2;
            }
        }
    }
    var ccDEN = gCentroContrato.CentroCoste.CentroSM.Denominacion;
    var tbCtroCosteVal = ccUON + " - " + ccDEN;
    var tbPPresVal = gCentroContrato.Partida.Denominacion;
    if (gCentroContrato.CentroCoste.CentroSM.PartidasPresupuestarias !== undefined) {
        gCentroContrato.CentroCoste.CentroSM.PartidasPresupuestarias = null;
    }
    gCentroContratoComprobar = jQuery.extend(true, {}, gCentroContrato);
    if (gCentroContratoComprobar !== undefined) {
        if (tbCtroCosteVal == " - ") {
            var CentroContrato = {};
            CentroContrato = CrearObjCentroPartidaVacio(CentroContrato);
            gCentroContratoComprobar = CentroContrato;
            gCentroContrato = CentroContrato;
            iRespuesta = false;
        } else {
            if (tbPPresVal == "") {
                var Partida = {};
                Partida = CrearObjPartidaVacio(Partida);
                gCentroContratoComprobar.Partida = Partida;
                gCentroContrato.Partida = Partida;
            } else {
                if (gCentroContratoComprobar.Partida !== undefined) {
                    gCentroContratoComprobar.Partida.Denominacion = "";
                    gCentroContratoComprobar.Partida.FechaInicioPresupuesto = "";
                    gCentroContratoComprobar.Partida.FechaFinPresupuesto = "";
                } else {
                    var Partida = {};
                    Partida = CrearObjPartidaVacio(Partida);
                    gCentroContratoComprobar.Partida = Partida;
                    gCentroContrato.Partida = Partida;
                }
            }
            var empresa = 0;
            if ($("#CmbBoxEmpresa").val() !== null) empresa = $("#CmbBoxEmpresa").val();
            var respuesta;
            $.when($.ajax({
                type: "POST",
                url: ruta + '/App_Pages/EP/EmisionPedido.aspx/ComprobarCentroCoste',
                contentType: "application/json; charset=utf-8",
                data: "{'emp':'" + empresa + "', 'sImputacion':'" + JSON.stringify(gCentroContratoComprobar) + "', 'txt':'" + tbCtroCosteVal + "', 'emitir':'false', 'CtrlPluriAnual':'false'}",
                dataType: "json",
                async: false
            })).done(function (msg) {
                iRespuesta = $.parseJSON(msg.d);
                respuesta = iRespuesta;
            });
            return respuesta;
        }
    }
}

function ComprobarCentroContratoAEmitirPluriAnual(idLinea) {
    //Return 100000 --> OK Partida + año fec entrega
    //Return 100000 --> OK Partida + años planes entrega
    //Return 200000 --> KO Partida. No has dado Fecha Entrega/Plan
    var iRespuesta = true;

    $('#aviso' + idLinea).hide();
    quitarAvisoEnLineaDef(idLinea);

    var ccUON = "";
    ccUON = gCentroContrato.CentroCoste.UON1;
    if (gCentroContrato.CentroCoste.UON4 != null && gCentroContrato.CentroCoste.UON4 != "") {
        ccUON = ccUON + "@@" + gCentroContrato.CentroCoste.UON2 + "@@" + gCentroContrato.CentroCoste.UON3 + "@@" + gCentroContrato.CentroCoste.UON4;
    } else {
        if (gCentroContrato.CentroCoste.UON3 != null && gCentroContrato.CentroCoste.UON3 != "") {
            ccUON = ccUON + "@@" + gCentroContrato.CentroCoste.UON2 + "@@" + gCentroContrato.CentroCoste.UON3;
        } else {
            if (gCentroContrato.CentroCoste.UON2 != null && gCentroContrato.CentroCoste.UON2 != "") {
                ccUON = ccUON + "@@" + gCentroContrato.CentroCoste.UON2;
            }
        }
    }

    var ccDEN = gCentroContrato.CentroCoste.CentroSM.Denominacion;
    var tbCtroCosteVal = ccUON + " - " + ccDEN;
    var tbPPresVal = gCentroContrato.Partida.Denominacion;
    if (gCentroContrato.CentroCoste.CentroSM.PartidasPresupuestarias !== undefined) {
        gCentroContrato.CentroCoste.CentroSM.PartidasPresupuestarias = null;
    }
    gCentroContratoComprobar = jQuery.extend(true, {}, gCentroContrato);
    if (gCentroContratoComprobar !== undefined) {
        if (tbCtroCosteVal == " - ") {
            //No hay q comprobar si no hay datos
            iRespuesta = 100000;
        } else {
            if (tbPPresVal == "") {
                //No hay q comprobar si no hay datos
                iRespuesta = 100000;
            } else {
                if (gCentroContratoComprobar.Partida !== undefined) {
                    gCentroContratoComprobar.Partida.Denominacion = "";
                    gCentroContratoComprobar.Partida.FechaInicioPresupuesto = "";
                    gCentroContratoComprobar.Partida.FechaFinPresupuesto = "";

                    var empresa = 0;
                    if ($("#CmbBoxEmpresa").val() !== null) empresa = $("#CmbBoxEmpresa").val();
                    var respuesta;
                    $.when($.ajax({
                        type: "POST",
                        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/ComprobarCentroCoste',
                        contentType: "application/json; charset=utf-8",
                        data: "{'emp':'" + empresa + "', 'sImputacion':'" + JSON.stringify(gCentroContratoComprobar) + "', 'txt':'" + tbCtroCosteVal + "', 'emitir':'true', 'CtrlPluriAnual':'true'}",
                        dataType: "json",
                        async: false
                    })).done(function (msg) {
                        iRespuesta = $.parseJSON(msg.d);
                        respuesta = iRespuesta;
                    });

                    if (iRespuesta == 200000) {
                        var textoError = TextosJScript[30]
                        mostrarOcultarError(textoError, true);

                        $('#aviso' + idLinea).css("display", "inline-block");
                        $('#aviso' + idLinea).prop('title', textoError);
                        ponerAvisoEnLinea(idLinea);
                    } else {
                        if (iRespuesta !== 100000) {
                            //Llega el primer año q no cumple

                            var textoError = TextosJScript[114].replace("XXXXXX", iRespuesta)

                            if (gCentroContrato.Partida.NIV2 == '') gCentroContrato.Partida.NIV2 = null
                            if (gCentroContrato.Partida.NIV3 == '') gCentroContrato.Partida.NIV3 = null
                            if (gCentroContrato.Partida.NIV4 == '') gCentroContrato.Partida.NIV4 = null

                            if (gCentroContrato.Partida.NIV4 !== null)
                                textoError = textoError.replace("YYYYYY", gCentroContrato.Partida.NIV4);
                            else
                                if (gCentroContrato.Partida.NIV3 !== null)
                                    textoError = textoError.replace("YYYYYY", gCentroContrato.Partida.NIV3);
                                else
                                    if (gCentroContrato.Partida.NIV2 !== null)
                                        textoError = textoError.replace("YYYYYY", gCentroContrato.Partida.NIV2);
                                    else
                                        textoError = textoError.replace("YYYYYY", gCentroContrato.Partida.NIV1);
                            textoError = textoError.replace("ZZZZZZ", gCentroContrato.Partida.Denominacion);
                            mostrarOcultarError(textoError, true);

                            $('#aviso' + idLinea).css("display", "inline-block");
                            textoError = TextosJScript[115].replace("XXXXXX", iRespuesta)
                            $('#aviso' + idLinea).prop('title', textoError);
                            ponerAvisoEnLinea(idLinea);
                        }
                    }
                } else {
                    //No hay q comprobar si no hay datos
                    iRespuesta = 100000;
                }
            }

        }
        return iRespuesta;
    }
}

function ComprobarContratoEscrito() {
    //DEVOLVER 0 --> GREEN a centro y DEVOLVER VACIAR a partida
    //DEVOLVER 1 --> GREEN a centro y DEVOLVER --> GREEN a partida
    //DEVOLVER 2 --> GREEN a centro y DEVOLVER --> Orange a partida
    //DEVOLVER 3 --> GREEN a centro y DEVOLVER --> Red a partida
    //DEVOLVER 4 --> Red a centro y vaciar partida
    var iRespuesta = true;
    var tbCtroCoste = $('#tbCtroCoste');
    var tbCtroCoste_Hidden = $('#tbCtroCoste_Hidden');
    var tbPPres = $('#tbPPres');
    var tbPPres_Hidden = $('#tbPPres_Hidden');
    var DevueltaCentro;
    var Partida = {};
    Partida = CrearObjPartidaVacio(Partida);
    Partida.Denominacion = tbPPres.val();
    if ((gCentroContratoComprobar == undefined) || (gCentroContratoComprobar.Id == "")) {
        var CentroContrato = {};
        CentroContrato.Id = 0;
        CentroContrato.CentroCoste;
        CentroContrato.Partida;
        CentroContrato.Partida = Partida;
        gCentroContratoComprobar = CentroContrato;
    }
    gCentroContratoComprobar.Partida = Partida;
    gCentroContrato.Partida = Partida; // = CentroContrato;
    tbPPres_Hidden.val('');
    tbCtroCoste.css('background-color', '');
    tbPPres.css('background-color', '');
    var tbCtroCosteVal = "";
    if (tbCtroCoste.val().toString() != "") {
        var CtroCosteDen = tbCtroCoste.val().toString().split(" - ");
        var CtroCosteHidden = tbCtroCoste_Hidden.val().toString();
        var numInicio = CtroCosteHidden.indexOf("@@") + 2;
        tbCtroCosteVal = CtroCosteHidden.substring(numInicio, CtroCosteHidden.length) + ' - ' + CtroCosteDen[1];
    }
    var empresa = 0;
    if ($("#CmbBoxEmpresa").val() !== null) { empresa = $("#CmbBoxEmpresa").val(); }
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/BuscarPartidaEnCentro',
        contentType: "application/json; charset=utf-8",
        data: "{'emp':'" + empresa + "', 'sImputacion':'" + JSON.stringify(gCentroContratoComprobar) + "', 'txt':'" + tbCtroCosteVal + "'}",
        dataType: "json",
        async: false
    })).done(function (msg) {
        if (isNaN($.parseJSON(msg.d).toString()) && ($.parseJSON(msg.d).toString() !== true)) {
            DevueltaCentro = $.parseJSON(msg.d);
            var Partida = {};
            Partida.Denominacion = DevueltaCentro.Denominacion;
            Partida.PresupuestoId = DevueltaCentro.PresupuestoId;
            Partida.NIV0 = DevueltaCentro.NIV0;
            Partida.NIV1 = DevueltaCentro.NIV1;
            Partida.NIV2 = DevueltaCentro.NIV2;
            Partida.NIV3 = DevueltaCentro.NIV3;
            Partida.NIV4 = DevueltaCentro.NIV4;
            Partida.FechaInicioPresupuesto = DevueltaCentro.FechaInicioPresupuesto;
            Partida.FechaFinPresupuesto = DevueltaCentro.FechaFinPresupuesto;
            gCentroContrato.Partida = Partida;
            var tbCtroCoste = $('#tbCtroCoste');
            if (tbCtroCoste.val() == "") {
                tbCtroCoste.val(DevueltaCentro.DenCentro);
                var tbCtroCoste_Hidden = $('#tbCtroCoste_Hidden');
                tbCtroCoste_Hidden.val(DevueltaCentro.CodCentro);
            }
            var nivs;
            var nivelUltimo;
            if (DevueltaCentro.NIV4 !== "") {
                nivs = DevueltaCentro.NIV0 + "@@" + DevueltaCentro.NIV1 + "@@" + DevueltaCentro.NIV2 + "@@" + DevueltaCentro.NIV3 + "@@" + DevueltaCentro.NIV4;
                nivelUltimo = DevueltaCentro.NIV1 + " - " + DevueltaCentro.NIV2 + " - " + DevueltaCentro.NIV3 + " - " + DevueltaCentro.NIV4;
            } else {
                if (DevueltaCentro.NIV3 !== "") {
                    nivs = DevueltaCentro.NIV0 + "@@" + DevueltaCentro.NIV1 + "@@" + DevueltaCentro.NIV2 + "@@" + DevueltaCentro.NIV3;
                    nivelUltimo = DevueltaCentro.NIV1 + " - " + DevueltaCentro.NIV2 + " - " + DevueltaCentro.NIV3;
                } else {
                    if (DevueltaCentro.NIV2 !== "") {
                        nivs = DevueltaCentro.NIV0 + "@@" + DevueltaCentro.NIV1 + "@@" + DevueltaCentro.NIV2;
                        nivelUltimo = DevueltaCentro.NIV1 + " - " + DevueltaCentro.NIV2;
                    } else {
                        if (DevueltaCentro.NIV1 !== "") {
                            nivs = DevueltaCentro.NIV0 + "@@" + DevueltaCentro.NIV1;
                            nivelUltimo = DevueltaCentro.NIV1;
                        } else {
                            nivs = DevueltaCentro.NIV0;
                            nivelUltimo = DevueltaCentro.NIV0;
                        }
                    }
                }
            }

            var strFechaIni = DevueltaCentro.FechaInicioPresupuesto;
            if (DevueltaCentro.FechaInicioPresupuesto !== undefined) {
                if (DevueltaCentro.FechaInicioPresupuesto.toString().toLowerCase().indexOf("date") >= 0) {
                    var fechaIni = eval("new " + DevueltaCentro.FechaInicioPresupuesto.slice(1, -1));
                    strFechaIni = fechaIni.format(UsuMask);
                }
            }
            else { strFechaIni = ""; }
            var strFechaFin = DevueltaCentro.FechaFinPresupuesto;
            if (DevueltaCentro.FechaFinPresupuesto !== undefined) {
                if (DevueltaCentro.FechaFinPresupuesto.toString().toLowerCase().indexOf("date") >= 0) {
                    var fechaFin = eval("new " + DevueltaCentro.FechaFinPresupuesto.slice(1, -1));
                    strFechaFin = fechaFin.format(UsuMask);
                }
            } else { strFechaFin = ""; }
            tbPPres.val(nivelUltimo + " - " + DevueltaCentro.Denominacion + " - (" + strFechaIni + " - " + strFechaFin + ")");
            tbPPres_Hidden.val(nivs);
            iRespuesta = 0;
        } else {
            tbPPres.val('');
            tbPPres_Hidden.val('');
        }
    });
    return iRespuesta;
}
function ComprobarCentroContratoEscrito() {
    //DEVOLVER 0 --> GREEN a centro y DEVOLVER VACIAR a partida
    //DEVOLVER 1 --> GREEN a centro y DEVOLVER --> GREEN a partida
    //DEVOLVER 2 --> GREEN a centro y DEVOLVER --> Orange a partida
    //DEVOLVER 3 --> GREEN a centro y DEVOLVER --> Red a partida
    //DEVOLVER 4 --> Red a centro y vaciar partida
    var iRespuesta = true;
    var tbCtroCoste = $('#tbCtroCoste');
    var tbPPres = $('#tbPPres');
    var tbPPres_Hidden = $('#tbPPres_Hidden');
    var tbCtroCoste_Hidden = $('#tbCtroCoste_Hidden');
    var DevueltaCentro;
    var tbCtroCosteVal = "";
    if (tbCtroCoste.val().toString() != "") {
        var CtroCosteDen = tbCtroCoste.val().toString().split(" - ");
        var CtroCosteHidden = tbCtroCoste_Hidden.val().toString();
        var numInicio = CtroCosteHidden.indexOf("@@") + 2;
        tbCtroCosteVal = CtroCosteHidden.substring(numInicio, CtroCosteHidden.length) + ' - ' + CtroCosteDen[1];
    }
    var CentroContrato = {};
    CentroContrato.Id = 0;
    CentroContrato.CentroCoste;
    CentroContrato.Partida;
    var CentroCoste = {};
    CentroCoste = CrearObjCentroCosteVacio(CentroCoste);
    CentroCoste.CentroSM.Denominacion = tbCtroCoste.val().toString();
    CentroContrato.CentroCoste = CentroCoste;
    var Partida = {};
    Partida = CrearObjPartidaVacio(Partida);
    CentroContrato.Partida = Partida;
    gCentroContratoComprobar = CentroContrato;
    gCentroContrato = CentroContrato;
    tbPPres.val('');
    tbPPres_Hidden.val('');
    tbCtroCoste.css('background-color', '');
    tbPPres.css('background-color', '');
    var empresa = 0;
    if ($("#CmbBoxEmpresa").val() !== null) empresa = $("#CmbBoxEmpresa").val();
    if ($('#tbCtroCoste').val() !== "") {
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/ComprobarCentroCosteEscrito',
            contentType: "application/json; charset=utf-8",
            data: "{'emp':'" + empresa + "', 'sImputacion':'" + JSON.stringify(gCentroContratoComprobar) + "', 'txt':'" + tbCtroCosteVal + "'}",
            dataType: "json",
            async: false
        })).done(function (msg) {
            if (isNaN($.parseJSON(msg.d).toString()) && ($.parseJSON(msg.d).toString() !== true)) {
                DevueltaCentro = $.parseJSON(msg.d);

                CentroContrato = {};
                CentroContrato.Id = "";
                CentroContrato.CentroCoste;
                CentroContrato.Partida;
                var CentroCoste = {};
                CentroCoste.UON1 = DevueltaCentro.UONS.UON1;
                CentroCoste.UON2 = DevueltaCentro.UONS.UON2;
                CentroCoste.UON3 = DevueltaCentro.UONS.UON3;
                CentroCoste.UON4 = DevueltaCentro.UONS.UON4;
                CentroCoste.CentroSM = DevueltaCentro.UONS.CentroSM;
                var CentroSM = {};
                CentroSM.Denominacion = DevueltaCentro.Denominacion;
                CentroSM.Codigo = DevueltaCentro.Codigo;
                var Partida = {};
                Partida = CrearObjPartidaVacio(Partida);

                CentroCoste.CentroSM = CentroSM;
                CentroContrato.CentroCoste = CentroCoste;
                CentroContrato.Partida = Partida;
                //CentroContrato.LineaId = parseInt(gLineas[i].ID.toString(), 10);
                CentroContrato.Id = 1;

                var coalUON;
                var descUON;
                if (CentroCoste.UON4 !== "") {
                    coalUON = CentroCoste.UON1 + "@@" + CentroCoste.UON2 + "@@" + CentroCoste.UON3 + "@@" + CentroCoste.UON4;
                    descUON = CentroCoste.UON4;
                } else {
                    if (CentroCoste.UON3 !== "") {
                        coalUON = CentroCoste.UON1 + "@@" + CentroCoste.UON2 + "@@" + CentroCoste.UON3;
                        descUON = CentroCoste.UON3;
                    } else {
                        if (CentroCoste.UON2 !== "") {
                            coalUON = CentroCoste.UON1 + "@@" + CentroCoste.UON2;
                            descUON = CentroCoste.UON2;
                        } else {
                            coalUON = CentroCoste.UON1;
                            descUON = CentroCoste.UON1;
                        }
                    }
                }
                tbCtroCoste.val(descUON + " - " + DevueltaCentro.Denominacion);
                gCentroContrato = CentroContrato;
                tbCtroCoste_Hidden.val(DevueltaCentro.Codigo + "@@" + coalUON);
                tbCtroCoste.css('background-color', '#D8E4BC');
                tbPPres.css('background-color', '');
                tbPPres.val('');
                tbPPres_Hidden.val('');
                iRespuesta = 0;
            } else {
                tbCtroCoste.val('');
                tbCtroCoste_Hidden.val('');
                tbPPres.val('');
                tbPPres_Hidden.val('');
            }
        });
    }
    return iRespuesta;
}
function guardarCentroActivoEP(gridCentros, event) {
    var oGridView = $('[id$=SMActivosEPpnlCentrosCosteuwtCentros_1]')[1];
    if (oGridView !== undefined) {
        for (fila = 0; fila < oGridView.childNodes.length; fila++) {
            if (oGridView.childNodes.item(fila).attributes != null) {
                if (oGridView.childNodes.item(fila).innerHTML.indexOf("ig_f153791a_r9") >= 0) {
                    denCentro = oGridView.childNodes.item(fila).innerText;
                    var tbActivosCentro = $('[id$=SMActivosEP_txtCentro]');
                    tbActivosCentro.val($.trim(denCentro));
                    $('[id$=SMActivosEP_modalCentrosCoste_backgroundElement]').hide();
                    $('[id$=SMActivosEP_modalCentrosCoste_backgroundElement]').css('z-index', 1001);
                    $('[id$=SMActivosEP_pnlCentrosCoste]').hide();

                    stopEvent(event);
                }
            }
        }
    }
    $('[id$=SMActivosEP_modalCentrosCoste_backgroundElement]').hide();
    $('[id$=SMActivosEP_modalCentrosCoste_backgroundElement]').css('z-index', 1001);
    $('[id$=SMActivosEP_pnlCentrosCoste]').hide();
    stopEvent(event);
}
function cerrarDivActivos() {
    $('#divActivos').hide();
    OcultarFondoPopUp();
}
function guardarActivoSeleccionado(gridActivos) {
    var codActivo;
    var denActivo;
    var denCentroActivo = "";
    var oGridView = $('[id$=SMActivosEP_gridActivos]')
    var GridView = oGridView[0].childNodes.item(1);
    for (fila = 0; fila < GridView.childNodes.length; fila++) {
        if (GridView.childNodes.item(fila).attributes != null) {
            if (GridView.childNodes.item(fila).attributes.getNamedItem('Seleccionado') != null) {
                if (GridView.childNodes.item(fila).attributes.getNamedItem('Seleccionado').value == 'True') {
                    codActivo = GridView.childNodes.item(fila).attributes.getNamedItem('ID').value;
                    denActivo = GridView.childNodes.item(fila).attributes.getNamedItem('DEN_ACTIVO').value;
                    denCentroActivo = GridView.childNodes.item(fila).attributes.getNamedItem('ID_DEN_CENTRO').value;
                    codCentroActivo = GridView.childNodes.item(fila).attributes.getNamedItem('CENTRO_SM').value;
                    UonCentroActivo = GridView.childNodes.item(fila).attributes.getNamedItem('UONS').value;
                    var tbActivoCentroHidden = $('#tbActivosCentro_Hidden');
                    tbActivoCentroHidden.val(codCentroActivo.toString());
                    var tbActivoHidden = $('#tbActivosLinea_Hidden');
                    tbActivoHidden.val(codActivo.toString());
                    var tbActivo = $('#tbActivosLinea');
                    tbActivo.val(denActivo.toString());
                    tbActivo.css('background-color', '#D8E4BC');
                    break
                }
            } else {
                var tbActivoCentroHidden = $('#tbActivosCentro_Hidden');
                tbActivoCentroHidden.val("");
                var tbActivoHidden = $('#tbActivosLinea_Hidden');
                tbActivoHidden.val(0);
                var tbActivo = $('#tbActivosLinea');
                tbActivo.val("");
                tbActivo.css('background-color', '');
            }
        } else {
            var tbActivoCentroHidden = $('#tbActivosCentro_Hidden');
            tbActivoCentroHidden.val("");
            var tbActivoHidden = $('#tbActivosLinea_Hidden');
            tbActivoHidden.val(0);
            var tbActivo = $('#tbActivosLinea');
            tbActivo.val("");
            tbActivo.css('background-color', '');
        }
    }

    cerrarDivActivos();
    if (denCentroActivo !== "") {
        var tbCtroCoste = $('#tbCtroCoste');
        var tbCtroCosteHidden = $('#tbCtroCoste_Hidden');
        if (tbCtroCoste.val() !== denCentroActivo) {
            tbCtroCoste.val(denCentroActivo);
            tbCtroCosteHidden.val(UonCentroActivo);
            for (var i = 0; i < gLineas.length; i++) {
                if (gLineas[i].ID == Linea.ID) {
                    ComprobarCentroContratoEscrito();
                    gCentroContrato.LineaId = Linea.ID;
                    CentroContrato = gCentroContrato;
                    Linea.Pres5_ImportesImputados_EP[0] = CentroContrato;
                    gLineas[i].Pres5_ImportesImputados_EP[0] = CentroContrato;
                };
            }
        }
    }
}
/*Coloca el centro seleccionado en el Textbox de los centros y oculta el panel de búsqueda*/
function seleccionarCentroEP(sender) {
    var sCCCod = htmlDecode(sender.getAttribute("cod"));
    var sCCDen = htmlDecode(sender.innerHTML);
    var sCCUon = htmlDecode(sender.getAttribute("coalUon"));
    var datos = htmlDecode(sender.getAttribute("datos"));
    var tbCtroCoste = $('#tbCtroCoste');
    tbCtroCoste.val(sCCDen)
    tbCtroCoste = null;
    var tbCtroCosteHidden = $('#tbCtroCoste_Hidden');
    tbCtroCosteHidden.val(sCCCod + "@@" + sCCUon);
    //tbCtroCosteHidden.val(sCCUon);
    tbCtroCosteHidden = null;
    ocultarPanelModal('mpeCentros');

    gLineas = gEmisionPedido.Lineas;
    for (var i = 0; i < gLineas.length; i++) {
        if (gLineas[i].ID == Linea.ID) {
            var CentroContrato = gLineas[i].Pres5_ImportesImputados_EP[0];
            if (CentroContrato == undefined) {
                CentroContrato = {};
                CentroContrato = CrearObjCentroPartidaVacio(CentroContrato);
            } else {
                if (CentroContrato.CentroCoste == undefined) {
                    CentroContrato = {};
                    CentroContrato = CrearObjCentroPartidaVacio(CentroContrato);
                } else {
                    if (CentroContrato.Partida == undefined) {
                        var Partida = {};
                        Partida = CrearObjPartidaVacio(Partida);
                        CentroContrato.Partida = Partida;
                    } else var Partida = CentroContrato.Partida;
                }
            }
            var CentroCoste = CentroContrato.CentroCoste;
            var CentroSM = CentroCoste.CentroSM;
            vaciarPartida();
            vaciarActivo();

            if ($('#tbCtroCoste').val() !== "") {
                var parts = datos.split("@@");
                CentroSM.Denominacion = $.trim(parts[1]);
            } else {
                CentroCoste.UON1 = "";
                CentroSM.Denominacion = "";
            }

            if ($('#tbCtroCoste_Hidden').val() !== "") {
                var parts = $('#tbCtroCoste_Hidden').val().split("@@");
                CentroSM.Codigo = $.trim(parts[0]);
                CentroCoste.UON1 = $.trim(parts[1]);
                if (parts.length > 2) {
                    CentroCoste.UON2 = $.trim(parts[2]);
                    if (parts.length > 3) {
                        CentroCoste.UON3 = $.trim(parts[3]);
                        if (parts.length > 4) {
                            CentroCoste.UON4 = $.trim(parts[4]);
                        }
                    }
                }
                CentroCoste.CentroSM = CentroSM;
                CentroContrato.CentroCoste = CentroCoste;
            } else {
                CentroCoste = CrearObjCentroCosteVacio(CentroCoste);
                CentroContrato.CentroCoste = CentroCoste;
            }

            CentroContrato.LineaId = parseInt(gLineas[i].ID.toString(), 10);
            CentroContrato.Id = 1;

            gCentroContrato = CentroContrato;
            var tbCtroCoste = $('#tbCtroCoste');
            tbCtroCoste.css('background-color', '#D8E4BC');
            CentroContrato = gCentroContrato;
            Linea.Pres5_ImportesImputados_EP[0] = CentroContrato;
            gLineas[i].Pres5_ImportesImputados_EP[0] = CentroContrato;
            comprobarPartidaUnicaLinea();
            break;
        };
    }
    gEmisionPedido.Lineas = gLineas;
}
function CrearObjCentroPartidaVacio(centro) {
    CentroContrato = centro;
    CentroContrato.Id = 0;
    CentroContrato.LineaId = "";
    CentroContrato.CentroCoste;
    CentroContrato.Partida;
    var CentroCoste = {};
    CentroCoste = CrearObjCentroCosteVacio(CentroCoste);
    CentroContrato.CentroCoste = CentroCoste;
    var Partida = {};
    Partida = CrearObjPartidaVacio(Partida);
    CentroContrato.Partida = Partida;
    return CentroContrato;
}
function CrearObjCentroCosteVacio(centroCos) {
    CentroCoste = centroCos;
    CentroCoste.UON1 = "";
    CentroCoste.UON2 = "";
    CentroCoste.UON3 = "";
    CentroCoste.UON4 = "";
    CentroCoste.CentroSM = "";
    var CentroSM = {};
    CentroSM.Denominacion = "";
    CentroSM.Codigo = "";
    CentroCoste.CentroSM = CentroSM;
    return CentroCoste;
}
function CrearObjPartidaVacio(partida) {
    Partida = partida;
    Partida.Denominacion = "";
    Partida.PresupuestoId = 0;
    Partida.NIV0 = "";
    Partida.NIV1 = "";
    Partida.NIV2 = "";
    Partida.NIV3 = "";
    Partida.NIV4 = "";
    Partida.FechaInicioPresupuesto = "";
    Partida.FechaFinPresupuesto = "";
    return Partida;
}
function guardarPedido(tipoGuardado) {
    var gEmisionPedidoGuardar = null;
    var gPartidas = [];
    var gCentroCoste = [];

    //Cabecera
    ////DatosGenerales
    var empresa = 0;
    var denempresa = "";
    if ($("#CmbBoxEmpresa").val() !== null) {
        empresa = $("#CmbBoxEmpresa").val();
        denempresa = $("#CmbBoxEmpresa option:selected").text();
    }
    gDatosGenerales.Emp = empresa;
    gDatosGenerales.DenEmp = denempresa;
    gDatosGenerales.Receptor = $("#wddRecept").val();
    gDatosGenerales.Referencia = $("#TxtBxPedSAP").val();
    gDatosGenerales.AcepProve = document.getElementById("chkSolicitAcepProve").checked;

    if ($('[id$=CmbBoxOrgCompras]').val() !== null) gDatosGenerales.OrgCompras = $('[id$=CmbBoxOrgCompras]').val();

    if ($("#CmbBoxProvesErp").val() !== null) gDatosGenerales.CodErp = $("#CmbBoxProvesErp").val();
    else gDatosGenerales.CodErp = 0;

    if ($("#CmbBoxTipoPedido").val() !== "0") gDatosGenerales.Tipo.Id = $("#CmbBoxTipoPedido").val();

    gDatosGenerales.Gestor = $("#LblGestorCab").text();
    gDatosGenerales.Obs = $("#textObser").val();

    if ($("#lblCodDireccion").text() !== "") gDatosGenerales.DirEnvio = $("#lblCodDireccion").text();

    gEmisionPedido.DatosGenerales = gDatosGenerales;

    ////Adjuntos ya estan en el objeto
    $.each(gEmisionPedido.Adjuntos, function () {
        if (this.Fecha !== undefined) {
            if (this.Fecha.toString().toLowerCase().indexOf("date") >= 0)
                this.Fecha = eval("new " + this.Fecha.slice(1, -1));
        }
    });
    ////Otros Datos
    if (gEmisionPedido.OtrosDatos !== undefined) guardarOtrosDatos();

    ////Adjuntos ya estan en el objeto
    $.each(gEmisionPedido.Adjuntos, function () {
        if (this.Fecha !== undefined) {
            if (this.Fecha !== null) {
                if (this.Fecha.toString().toLowerCase().indexOf("date") >= 0)
                    this.Fecha = eval("new " + this.Valor.slice(1, -1));
            }
        }
    });
    ////Costes
    if (gCostes !== null) gEmisionPedido.Costes = gCostes;

    ////Descuentos
    if (gDescu !== null) gEmisionPedido.Descuentos = gDescu;

    //Validaciones de los planes de entrega de las lineas
    //cada fila debe tener fecha y cantidad/importe / la suma de cantidad/importe no debe superar a la de la linea / las fechas no pueden ser anteriores a la actual
    var exit = false;
    $.each(gLineas, function () {
        if (this.PlanEntrega.length > 0)
            if (!ValidarPlanEntrega(this.TipoRecepcion, this.PlanEntrega, this.CantPed, this.ImportePedido, ("0000" + this.NumLinea).slice(-3))) { exit = true; return false; };
    });
    if (exit) { OcultarCargando(); return false; };



    //guardar las pestaña linea
    if (iCodFilaSeleccionada !== 0) {
        if ($('#pesDatosLineas').hasClass("escenarioSeleccionadoEP"))
            guardarOtrosDatosLineas();

        if ($('#pesCostesLinea').hasClass("escenarioSeleccionadoEP"))
            guardarCostesLinea();

        if ($('#pesDescuentosLinea').hasClass("escenarioSeleccionadoEP"))
            guardarDescuentosLinea();
    }
    $.each(gLineas, function () {
        ////Adjuntos ya estan en el objeto
        $.each(this.Adjuntos, function () {
            if (this.Fecha !== undefined) {
                if (this.Fecha.toString().toLowerCase().indexOf("date") >= 0)
                    this.Fecha = eval("new " + this.Fecha.slice(1, -1));
            }
        });
        if (this.OtrosDatos !== null) {
            $.each(this.OtrosDatos, function () {
                if (this.Tipo == 3) {
                    if (this.Valor !== null) {
                        if (this.Valor.toString().toLowerCase().indexOf("date") >= 0) {
                            this.Valor = eval("new " + this.Valor.slice(1, -1));
                        } else {
                            if (this.Valor.toString() == "") this.Valor = null;
                        }
                    }
                }
            });
        };
    });
    var lineasBorrar = $("#lineasBorrar").val().toString();
    if (lineasBorrar !== "") {
        var params = JSON.stringify({ iDesdeFavorito: bModificarFavorito, idCabecera: sID, lineas: lineasBorrar });
        $.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/BorrarLineas',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        });
    }
    $("#lineasBorrar").val("");
    gEmisionPedidoGuardar = jQuery.extend(true, {}, gEmisionPedido);

    var i = 0;
    $.each(gEmisionPedidoGuardar.Lineas, function () {
        gCentroCoste = [];
        gPartidas = [];
        i = i + 1;

        if (this.Pres5_ImportesImputados_EP !== undefined) {
            if (this.Pres5_ImportesImputados_EP[0] !== undefined) {
                if (this.Pres5_ImportesImputados_EP[0].CentroCoste !== undefined) {
                    if (this.Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM.PartidasPresupuestarias !== undefined)
                        this.Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM.PartidasPresupuestarias = null;

                    gCentroCoste.push(JSON.parse(JSON.stringify(this.Pres5_ImportesImputados_EP[0].CentroCoste)));
                } else gCentroCoste.push(new Object());

                if (this.Pres5_ImportesImputados_EP[0].Partida !== undefined) {
                    if (this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto !== null) {
                        this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto = null;
                    }
                    if (this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto !== null) {
                        this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto = null;
                    }
                    gPartidas.push(JSON.parse(JSON.stringify(this.Pres5_ImportesImputados_EP[0].Partida)));
                } else gPartidas.push(new Object());
            }
            this.Pres5_ImportesImputados_EP = [];
        } else {
            if (this.Pres5_ImportesImputados_EP[0] !== undefined) {
                if (this.Pres5_ImportesImputados_EP[0].CentroCoste !== undefined) {
                    gCentroCoste.push(JSON.parse(JSON.stringify(this.Pres5_ImportesImputados_EP[0].CentroCoste)));
                    if (this.Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM.PartidasPresupuestarias !== undefined) {
                        this.Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM.PartidasPresupuestarias = null;
                    }
                } else {
                    gCentroCoste.push(new Object());
                }
                if (this.Pres5_ImportesImputados_EP[0].Partida !== undefined) {
                    if (this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto !== null) {
                        this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto = null;
                    }
                    if (this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto !== null) {
                        this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto = null;
                    }
                    gPartidas.push(JSON.parse(JSON.stringify(this.Pres5_ImportesImputados_EP[0].Partida)));
                } else
                    gPartidas.push(new Object());
            }
            this.Pres5_ImportesImputados_EP = [];
        }

        ////Adjuntos ya estan en el objeto
        $.each(this.Adjuntos, function () {
            if (this.Fecha !== undefined) {
                if (this.Fecha.toString().toLowerCase().indexOf("date") >= 0)
                    this.Fecha = eval("new " + this.Fecha.slice(1, -1));
            }
        });
        if (this.OtrosDatos !== null) {
            $.each(this.OtrosDatos, function () {
                if (this.Tipo == 3) {
                    if (this.Valor !== null) {
                        if (this.Valor.toString().toLowerCase().indexOf("date") >= 0) {
                            this.Valor = eval("new " + this.Valor.slice(1, -1));
                        } else {
                            if (this.Valor.toString() == "") {
                                this.Valor = null;
                            }
                        }
                    }
                }
            });
        };
        var params = JSON.stringify({
            iDesdeFavorito: bModificarFavorito,
            oLinea: JSON.stringify(this),
            oCentroCoste: JSON.stringify(gCentroCoste),
            oPartidas: JSON.stringify(gPartidas),
            iCestaCabeceraId: gEmisionPedido.DatosGenerales.Id,
            bHayIntegracion: bHayIntegracionPedidos,
            idEmpresa: gDatosGenerales.Emp
        });
        $.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/GuardarLineasPedido',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        });
    });
    var sVacio = "";
    var params = JSON.stringify({
        iDesdeFavorito: bModificarFavorito,
        oEmisionPedido: JSON.stringify(gEmisionPedidoGuardar),
        bHayIntegracion: bHayIntegracionPedidos,
        idEmpresa: gDatosGenerales.Emp,
        sDescFavorito: sVacio,
        ModoEdicion: ModoEdicion,
        EsPedidoAbierto: EsPedidoAbierto
    });
    $.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/GuardarPedido',
        contentType: "application/json; charset=utf-8",
        data: params,
        dataType: "json",
        async: false
    });
    $.each(gEmisionPedido.Adjuntos, function () {
        this.Operacion = "";
    });
    $.each(gEmisionPedido.Lineas, function () {
        $.each(this.Adjuntos, function () {
            this.Operacion = "";
        });
    });

    //Guardar un pedido favorito nuevo
    var iRespuesta = 0;
    if (tipoGuardado == 1) {
        var sNombreFavorito = $.trim($("#txtNomFavorito").val().toString());
        if (sNombreFavorito !== "") {
            var dImporte = $('#lblImpBruto_hdd').text();
            var params = JSON.stringify({ oEmisionPedido: JSON.stringify(gEmisionPedidoGuardar), sDescFavorito: sNombreFavorito, dImporte: dImporte });
            $.when($.ajax({
                type: "POST",
                url: ruta + '/App_Pages/EP/EmisionPedido.aspx/GuardarNuevoFavorito',
                contentType: "application/json; charset=utf-8",
                data: params,
                dataType: "json",
                async: false
            })).done(function (msg) {
                iRespuesta = msg.d;
            });
            sIDFavorito = iRespuesta.toString();
            desdeFavorito = "1";
            $("#btnModificarFavorito").css("display", "block");
            $("#btnModificarFavorito1").css("display", "block");

            var CantFavoritosStr = "";
            var CantFavoritos = 0;
            var textoFavoritos = $('[id$=TabFavoritos]').text();
            if (textoFavoritos !== "") {
                CantFavoritosStr = textoFavoritos.slice(textoFavoritos.indexOf('(') + 1, textoFavoritos.length);
                CantFavoritos = CantFavoritosStr.slice(0, CantFavoritosStr.length - (CantFavoritosStr.length - CantFavoritosStr.indexOf(')')));
                if ($.isNumeric(CantFavoritos.toString())) {
                    CantFavoritos = parseInt(CantFavoritos) + 1;
                    CantFavoritosStr = textoFavoritos.slice(0, textoFavoritos.length - (textoFavoritos.length - textoFavoritos.indexOf('(')));
                    CantFavoritosStr = CantFavoritosStr + "(" + CantFavoritos.toString() + ")"
                    $('[id$=TabFavoritos]').text(CantFavoritosStr.toString());
                }
            }
            //Mensaje de OK
            mensaje =
                [{
                    valor: " - " + TextosJScript[56]
                }];
            $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
            mostrarError();
        } else {
            //Error falta descripcion
            mensaje =
                [{
                    valor: " - " + TextosJScript[49]
                }];
            $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
            mostrarError();
        }
    }

    if (tipoGuardado == 3) {
        var sNombreFavorito = $.trim($("#txtNomFavorito").val().toString());
        if (sNombreFavorito !== "") {
            var dImporte = $('#lblImpBruto_hdd').text();
            var params = JSON.stringify({ oEmisionPedido: JSON.stringify(gEmisionPedidoGuardar), sDescFavorito: sNombreFavorito, dImporte: dImporte });
            $.when($.ajax({
                type: "POST",
                url: ruta + '/App_Pages/EP/EmisionPedido.aspx/GuardarNuevoFavoritoYBorrar',
                contentType: "application/json; charset=utf-8",
                data: params,
                dataType: "json",
                async: false
            })).done(function (msg) {
                iRespuesta = msg.d;
            });
            sIDFavorito = iRespuesta.toString();

            var CantFavoritosStr = "";
            var CantFavoritos = 0;
            var textoFavoritos = $('[id$=TabFavoritos]').text();
            if (textoFavoritos !== "") {
                CantFavoritosStr = textoFavoritos.slice(textoFavoritos.indexOf('(') + 1, textoFavoritos.length);
                CantFavoritos = CantFavoritosStr.slice(0, CantFavoritosStr.length - (CantFavoritosStr.length - CantFavoritosStr.indexOf(')')));
                if ($.isNumeric(CantFavoritos.toString())) {
                    CantFavoritos = parseInt(CantFavoritos) + 1;
                    CantFavoritosStr = textoFavoritos.slice(0, textoFavoritos.length - (textoFavoritos.length - textoFavoritos.indexOf('(')));
                    CantFavoritosStr = CantFavoritosStr + "(" + CantFavoritos.toString() + ")"
                    $('[id$=TabFavoritos]').text(CantFavoritosStr.toString());
                }
            }
            mensaje =
                [{
                    valor: " - " + TextosJScript[56]
                }];
            $('#MensajeError').tmpl(mensaje).appendTo($('#TextoPedido'));
            mostrarPedidoGenerado();
            OcultarCargando();
        } else {
            //Error falta descripcion
            mensaje =
                [{
                    valor: " - " + TextosJScript[49]
                }];
            $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
            mostrarError();
        }
    };

    //Modificar Pedido Favorito
    if (tipoGuardado == 2) {
        var params = JSON.stringify({ idFav: sIDFavorito });
        $.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/EliminarFavorito',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        });
        //Una vez eliminado el favorito se vuelve a crear otro a partir de la cesta
        var sNombreFavorito = $.trim($("#txtNomFavorito").val().toString());
        var dImporte = $('#lblImpBruto_hdd').text();
        var params = JSON.stringify({ oEmisionPedido: JSON.stringify(gEmisionPedidoGuardar), sDescFavorito: sNombreFavorito, dImporte: dImporte });
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/GuardarNuevoFavorito',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        })).done(function (msg) {
            iRespuesta = msg.d;
        });
        sIDFavorito = iRespuesta.toString();
        desdeFavorito = "1";
    }
    OcultarCargando();
}
function comprobarCantidad(idLinea) {
    var dCantidad = parseFloat(strToNum($.trim($('#Cant' + idLinea).val())));
    var sUPed = $('#Uni' + idLinea).text();
    var gEmisionPedidoGuardar = jQuery.extend(true, {}, gEmisionPedido);
    var i = 0;

    $('#aviso' + idLinea).hide();
    quitarAvisoEnLineaDef(idLinea);
    $.each(gEmisionPedidoGuardar.Lineas, function () {
        gCentroCoste = [];
        gPartidas = [];
        i = i + 1;
        if (this.Pres5_ImportesImputados_EP[0] !== undefined) {
            if (this.Pres5_ImportesImputados_EP[0].CentroCoste !== undefined) {
                if (this.Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM !== undefined) {
                    if (this.Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM.PartidasPresupuestarias !== undefined) {
                        this.Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM.PartidasPresupuestarias = null;
                    }
                }
            }
            this.Pres5_ImportesImputados_EP = [];
        } else {
            this.Pres5_ImportesImputados_EP = [];
        }
        ////Adjuntos ya estan en el objeto
        $.each(this.Adjuntos, function () {
            if (this.Fecha !== undefined) {
                if (this.Fecha.toString().toLowerCase().indexOf("date") >= 0)
                    this.Fecha = eval("new " + this.Fecha.slice(1, -1));
            }
        });
    });
    ////Adjuntos ya estan en el objeto
    $.each(gEmisionPedidoGuardar.Adjuntos, function () {
        if (this.Fecha !== undefined) {
            if (this.Fecha.toString().toLowerCase().indexOf("date") >= 0)
                this.Fecha = eval("new " + this.Fecha.slice(1, -1));
        }
    });
    var params = JSON.stringify({ idLinea: idLinea, sUPed: sUPed, dCant: dCantidad, sPedido: JSON.stringify(gEmisionPedidoGuardar), EsPedidoAbierto: EsPedidoAbierto });
    var iRespuesta = 0;
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/ComprobarCantidades',
        contentType: "application/json; charset=utf-8",
        data: params,
        dataType: "json",
        async: false
    })).done(function (msg) {
        iRespuesta = $.parseJSON(msg.d);
    });

    if (iRespuesta !== 0) {
        //mostrar error cantidad       
        //1 -- Cantidad no supera la minima establecida
        //2 -- Cantidad supera la maxima establecida
        //3 -- Cantidad supera maxima total a pedir por linea de catalogo
        //4 -- Supera la cantidad maxima a pedir por linea de catalogo en unidad de pedido
        switch (iRespuesta) {
            case 1:
                mostrarOcultarError(TextosJScript[18]);

                $('#aviso' + idLinea).css("display", "inline-block");
                $('#aviso' + idLinea).prop('title', TextosJScript[18]);
                ponerAvisoEnLinea(idLinea);
                break;
            case 2:
                mostrarOcultarError(TextosJScript[19]);

                $('#aviso' + idLinea).css("display", "inline-block");
                $('#aviso' + idLinea).prop('title', TextosJScript[19]);
                ponerAvisoEnLinea(idLinea);
                break;
            case 3:
                mostrarOcultarError(TextosJScript[20]);

                $('#aviso' + idLinea).css("display", "inline-block");
                $('#aviso' + idLinea).prop('title', TextosJScript[20]);
                ponerAvisoEnLinea(idLinea);
                break;
            case 4:
                mostrarOcultarError(TextosJScript[21]);

                $('#aviso' + idLinea).css("display", "inline-block");
                $('#aviso' + idLinea).prop('title', TextosJScript[21]);
                ponerAvisoEnLinea(idLinea);
                break;
        }
    }
    return iRespuesta;
}
function cambiarCantidad(idLinea, event) {
    var iCambiarCantidadValido = comprobarCantidad(idLinea);
    var cantidad = parseFloat(strToNum($.trim($('#Cant' + idLinea).val())));
    var FC = parseFloat(strToNum($.trim($('#FC' + idLinea).val())));
    var precioU = parseFloat($.trim($('#Prec' + idLinea + '_hdd').text()));
    var precioImpBruto = parseFloat($.trim($('#lblImpBrutoLinea_hdd').text()));
    var precioLineaAnt = parseFloat($.trim($('#lblImpBrutolin_' + idLinea + '_hdd').text()));
    var precio = cantidad * precioU;

    precioImpBruto = precioImpBruto - precioLineaAnt + precio;
    $.each(gEmisionPedido.Lineas, function () {
        if (this.ID == idLinea)
            this.CantPed = cantidad;
    });
    $('#lblImpBruto').text(formatNumber(precioImpBruto) + " " + sMoneda);
    $('#lblImpBrutoLinea_hdd').text(precioImpBruto);
    $('#lblImpBruto_hdd').text(precioImpBruto);
    $('#lblImpBrutolin_' + idLinea + '_hdd').text(precio);
    $('#lblImpBrutolin_' + idLinea).text(formatNumber(precio));
    recalcularLineaCosteDescuento();
    $('#pesCostes').click();
    $('#pesDescuento').click();
    $('#pesDatosGenerales').click();
    calculoTotalCabecera();
    $.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CambiarCantidadLinea',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ idPedido: sID, idLinea: idLinea, cantidad: cantidad }),
        dataType: "json",
        async: true
    });
    stopEvent(event);
}
function cambiarDescripcion(idLinea) {
    var descripcion = $.trim($('#inpArtDen_' + idLinea).val());

    var lineaDescripcion = $.map(gEmisionPedido.Lineas, function (x) { if (x.ID == idLinea) return x; })[0];
    if (descripcion == '') $('#inpArtDen_' + idLinea).val(lineaDescripcion.Den);
    else {
        lineaDescripcion.Den = descripcion;

        $.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CambiarDescripcionLinea',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ idPedido: sID, idLinea: idLinea, descripcion: descripcion }),
            dataType: "json",
            async: true
        });
    };
    stopEvent(event);
}
function cambiarPrecio(idLinea, event) {
    var cantidad = parseFloat(strToNum($.trim($('#Cant' + idLinea).val())));
    var precioU = parseFloat(strToNum($.trim($('#Prec' + idLinea).val())));
    var FC = parseFloat(strToNum($.trim($('#FC' + idLinea).val())));
    var precioImpBruto = parseFloat($.trim($('#lblImpBrutoLinea_hdd').text()));
    var precioLineaAnt = parseFloat($.trim($('#lblImpBrutolin_' + idLinea + '_hdd').text()));
    var precio = cantidad * precioU;

    $('#Prec' + idLinea + '_hdd').text(precioU);
    $('#PrecT' + idLinea).val(formatNumber(precioU));

    precioImpBruto = precioImpBruto - precioLineaAnt + precio;

    $.each(gEmisionPedido.Lineas, function () {
        if (this.ID == idLinea) {
            this.PrecUc = precioU;
            this.fc = 1;
        }
    });
    $('#lblImpBruto').text(formatNumber(precioImpBruto) + " " + sMoneda);
    $('#lblImpBrutoLinea_hdd').text(precioImpBruto);
    $('#lblImpBruto_hdd').text(precioImpBruto);
    $('#lblImpBrutolin_' + idLinea + '_hdd').text(precio);
    $('#lblImpBrutolin_' + idLinea).text(formatNumber(precio));
    recalcularLineaCosteDescuento();
    $('#pesCostes').click();
    $('#pesDescuento').click();
    $('#pesDatosGenerales').click();
    calculoTotalCabecera();

    stopEvent(event);
}
function cambiarImporteBruto(idLinea, event) {
    var iCambiarImporteBruto = comprobarImporteBruto(idLinea);
    var precioImpBrutoLinea = parseFloat(strToNum($.trim($('#lblImpBrutolin_' + idLinea).val())));
    var precioLineaAnt = parseFloat($.trim($('#lblImpBrutolin_' + idLinea + '_hdd').text()));
    var precioImpBruto = parseFloat($.trim($('#lblImpBrutoLinea_hdd').text()));

    precioImpBruto = precioImpBruto - precioLineaAnt + precioImpBrutoLinea;

    $.each(gEmisionPedido.Lineas, function () {
        if (this.ID == idLinea) this.ImportePedido = precioImpBrutoLinea;
    });
    $('#lblImpBruto').text(formatNumber(precioImpBruto) + " " + sMoneda);
    $('#lblImpBrutoLinea_hdd').text(precioImpBruto);
    $('#lblImpBruto_hdd').text(precioImpBruto);
    $('#lblImpBrutolin_' + idLinea + '_hdd').text(precioImpBrutoLinea);
    $('#lblImpBrutolin_' + idLinea).text(formatNumber(precioImpBrutoLinea));
    recalcularLineaCosteDescuento();
    $('#pesCostes').click();
    $('#pesDescuento').click();
    $('#pesDatosGenerales').click();
    calculoTotalCabecera();

    stopEvent(event);
}
function comprobarImporteBruto(idLinea) {
    var precioImpBrutoLinea = parseFloat(strToNum($.trim($('#lblImpBrutolin_' + idLinea).val())));
    var sUPed = $('#Uni' + idLinea).text();
    var gEmisionPedidoGuardar = jQuery.extend(true, {}, gEmisionPedido);
    var i = 0;
    $('#aviso' + idLinea).hide();
    quitarAvisoEnLineaDef(idLinea);
    gEmisionPedidoGuardar.Adjuntos = [];
    $.each(gEmisionPedidoGuardar.Lineas, function () {
        gCentroCoste = [];
        gPartidas = [];
        i = i + 1;
        if (this.Pres5_ImportesImputados_EP[0] !== undefined)
            this.Pres5_ImportesImputados_EP = [];
        else
            this.Pres5_ImportesImputados_EP = [];

        if (this.OtrosDatos !== null) {
            $.each(this.OtrosDatos, function () {
                if (this.Tipo == 3) {
                    if (this.Valor !== null) {
                        if (this.Valor.toString().toLowerCase().indexOf("date") >= 0) {
                            this.Valor = eval("new " + this.Valor.slice(1, -1));
                        } else {
                            if (this.Valor.toString() == "") {
                                this.Valor = null;
                            }
                        }
                    }
                };
            });
        }
        this.Adjuntos = [];
    });

    var params = JSON.stringify({ idLinea: idLinea, sUPed: sUPed, dImporteBruto: precioImpBrutoLinea, sPedido: JSON.stringify(gEmisionPedidoGuardar) });
    var iRespuesta = 0;
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/ComprobarImporteBruto',
        contentType: "application/json; charset=utf-8",
        data: params,
        dataType: "json",
        async: false
    })).done(function (msg) {
        iRespuesta = $.parseJSON(msg.d);
    });

    if (iRespuesta !== 0) {
        //mostrar error cantidad       
        //1 -- Cantidad no supera la minima establecida
        //2 -- Cantidad supera la maxima extablecida
        //3 -- Cantidad supera maxima total a pedir por linea de catalogo
        //4 -- Supera la cantidad maxima a pedir por linea de catalogo en unidad de pedido
        switch (iRespuesta) {
            case 1:
                mostrarOcultarError(TextosJScript[18]);

                $('#aviso' + idLinea).css("display", "inline-block");
                $('#aviso' + idLinea).prop('title', TextosJScript[18]);
                ponerAvisoEnLinea(idLinea);
                break;
            case 2:
                mostrarOcultarError(TextosJScript[19]);

                $('#aviso' + idLinea).css("display", "inline-block");
                $('#aviso' + idLinea).prop('title', TextosJScript[19]);
                ponerAvisoEnLinea(idLinea);
                break;
            case 3:
                mostrarOcultarError(TextosJScript[20]);

                $('#aviso' + idLinea).css("display", "inline-block");
                $('#aviso' + idLinea).prop('title', TextosJScript[20]);
                ponerAvisoEnLinea(idLinea);
                break;
            case 4:
                mostrarOcultarError(TextosJScript[21]);

                $('#aviso' + idLinea).css("display", "inline-block");
                $('#aviso' + idLinea).prop('title', TextosJScript[21]);
                ponerAvisoEnLinea(idLinea);
                break;
        }
    }
    return iRespuesta;
}
function guardarOtrosDatos(excluirListaExterna) {
    if (excluirListaExterna == undefined) excluirListaExterna = false;
    if (gEmisionPedido.OtrosDatos !== undefined && gEmisionPedido.OtrosDatos !== null) {
        if (gEmisionPedido.OtrosDatos !== 0) {
            for (var i = 0; i < gEmisionPedido.OtrosDatos.length; i++) {
                if (gEmisionPedido.OtrosDatos[i].Intro == 1) {
                    if (gEmisionPedido.OtrosDatos[i].ListaExterna && !excluirListaExterna) {
                        if ($('#atribListaExterna_' + gEmisionPedido.OtrosDatos[i].Id).length) {
                            if ($('#atribListaExterna_' + gEmisionPedido.OtrosDatos[i].Id).tokenInput('get').length) {
                                var selectedToken = $('#atribListaExterna_' + gEmisionPedido.OtrosDatos[i].Id).tokenInput('get')[0];
                                gEmisionPedido.OtrosDatos[i].Valor = selectedToken.id;
                                gEmisionPedido.OtrosDatos[i].Valores = [];
                                gEmisionPedido.OtrosDatos[i].Valores.push({ 'Valor': selectedToken.name }); //Aprovechamos que existe un array de valores para guardar el texto de un atributo tipo lista externa
                            }
                        }
                    } else {
                        if ($('#cboAtribEP_' + gEmisionPedido.OtrosDatos[i].Id).length) {
                            gEmisionPedido.OtrosDatos[i].Valor = $('#fsComboValueDisplay_cboAtribEP_' + gEmisionPedido.OtrosDatos[i].Id).val();
                        }
                    }
                } else {
                    switch (gEmisionPedido.OtrosDatos[i].Tipo.toString()) {
                        case "1":
                            if ($('#inputTextAtribEP_' + gEmisionPedido.OtrosDatos[i].Id).length)
                                gEmisionPedido.OtrosDatos[i].Valor = $('#inputTextAtribEP_' + gEmisionPedido.OtrosDatos[i].Id).val();
                            break;
                        case "2":
                            if ($('#inputNumAtribEP_' + gEmisionPedido.OtrosDatos[i].Id).length)
                                gEmisionPedido.OtrosDatos[i].Valor = $('#inputNumAtribEP_' + gEmisionPedido.OtrosDatos[i].Id).val();
                            break;
                        case "3":
                            if ($('#inputFechaAtribEP_' + gEmisionPedido.OtrosDatos[i].Id).length)
                                gEmisionPedido.OtrosDatos[i].Valor = $('#inputFechaAtribEP_' + gEmisionPedido.OtrosDatos[i].Id).datepicker("getDate");
                            break;
                        case "4":
                            if ($('#cboAtribEP_' + gEmisionPedido.OtrosDatos[i].Id).length) {
                                if ($('#fsComboValueDisplay_cboAtribEP_' + gEmisionPedido.OtrosDatos[i].Id).val() == TextosBoolean[0]) { gEmisionPedido.OtrosDatos[i].Valor = 0; }
                                else { if ($('#fsComboValueDisplay_cboAtribEP_' + gEmisionPedido.OtrosDatos[i].Id).val() == TextosBoolean[1]) { gEmisionPedido.OtrosDatos[i].Valor = 1; } }
                            }
                            break;
                    }
                }
            }
        }
    }
};
function emitirPedido(bPartida) {
    var gPartidas = [];
    var gCentroCoste = [];
    var bOk = true;

    if (ModoEdicion == undefined) ModoEdicion = false;
    if (!bPartida) {
        if (sAvisoEmisionPedido != "") {
            mensaje = [{
                valor: sAvisoEmisionPedido
            }];
            $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorAviso'));
            mostrarErrorAviso();
            return false;
        }
    }
    //Cabecera
    ////DatosGenerales
    var empresa = 0;
    var denempresa = "";
    if ($("#CmbBoxEmpresa").val() !== null) {
        empresa = $("#CmbBoxEmpresa").val();
        denempresa = $("#CmbBoxEmpresa option:selected").text();
    }
    gDatosGenerales.Emp = empresa;
    gDatosGenerales.DenEmp = denempresa;
    gDatosGenerales.Receptor = $("#wddRecept").val();
    gDatosGenerales.Referencia = $("#TxtBxPedSAP").val();

    if ($("#CmbBoxProvesErp").val() !== null) gDatosGenerales.CodErp = $("#CmbBoxProvesErp").val();
    else gDatosGenerales.CodErp = 0;

    gDatosGenerales.AcepProve = document.getElementById("chkSolicitAcepProve").checked;

    if ($("#CmbBoxTipoPedido").val() !== "0") gDatosGenerales.Tipo.Id = $("#CmbBoxTipoPedido").val();

    gDatosGenerales.Gestor = $("#LblGestorCab").text();
    gDatosGenerales.Obs = $("#textObser").val();

    if ($("#lblCodDireccion").text() !== "") gDatosGenerales.DirEnvio = $("#lblCodDireccion").text();

    gEmisionPedido.DatosGenerales = gDatosGenerales;

    ////Adjuntos ya estan en el objeto
    $.each(gEmisionPedido.Adjuntos, function () {
        if (this.Fecha !== undefined) {
            if (this.Fecha.toString().toLowerCase().indexOf("date") >= 0)
                this.Fecha = eval("new " + this.Fecha.slice(1, -1));
        }
    });

    ////Otros Datos
    if (gEmisionPedido.OtrosDatos !== undefined) guardarOtrosDatos();

    ////Adjuntos ya estan en el objeto
    $.each(gEmisionPedido.Adjuntos, function () {
        if (this.Fecha !== undefined) {
            if (this.Fecha !== null) {
                if (this.Fecha.toString().toLowerCase().indexOf("date") >= 0)
                    this.Fecha = eval("new " + this.Valor.slice(1, -1));
            }
        }
    });

    ////Costes
    if (gCostes !== null) gEmisionPedido.Costes = gCostes;

    ////Descuentos
    if (gDescu !== null) gEmisionPedido.Descuentos = gDescu;

    var exit = false;
    //guardar las pestaña linea
    if (iCodFilaSeleccionada !== 0) {
        if ($('#pesDatosLineas').hasClass("escenarioSeleccionadoEP")) guardarOtrosDatosLineas();
        if ($('#pesCostesLinea').hasClass("escenarioSeleccionadoEP")) guardarCostesLinea();
        if ($('#pesDescuentosLinea').hasClass("escenarioSeleccionadoEP")) guardarDescuentosLinea();
    }

    var quienFallaListaExterna = ''

    if (gEmisionPedido.OtrosDatos !== undefined && gEmisionPedido.OtrosDatos.length > 0) {
        var atributosLlamada = '';
        $.each(gEmisionPedido.OtrosDatos, function () {
            if (atributosLlamada == '')
                atributosLlamada = this.Id + '@########@' + this.Valor;
            else {
                atributosLlamada = atributosLlamada + "##########" + this.Id + '@########@' + this.Valor;
            }
        });

        $.each(gEmisionPedido.OtrosDatos, function () {
            if (this.Intro == 1) {
                if (this.ListaExterna) {
                    if (this.Valor !== '') {
                        var sOrgCompras = '';
                        var sCentro = '';
                        if (bHayIntegracionPedidos && gDatosGenerales.UsarOrgCompras) {
                            sOrgCompras = gDatosGenerales.OrgCompras;
                            if (gEmisionPedido.Lineas.length > 0) sCentro = gEmisionPedido.Lineas[0].CentroAprovisionamiento;
                        };

                        var IdAtributoListaExterna = this.Id;

                        var param = JSON.stringify({ 'Empresa': gDatosGenerales.Emp, 'IdAtributoListaExterna': IdAtributoListaExterna, 'atributos': atributosLlamada, 'codigoBuscado': this.Valor, 'denominacionBuscado': '', 'buscador': true, 'OrgCompras': sOrgCompras, 'Centro': sCentro });
                        $.when($.ajax({
                            type: "POST",
                            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Obtener_Datos_ListaExterna',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: param,
                            async: false
                        })).done(function (msg) {
                            if (msg.d.length == 0) {
                                exit = true;
                            }
                        });
                        if (exit) {
                            quienFallaListaExterna = this.Denominacion;
                            return false;
                        }
                    }
                }
            }
        });
    }
    if (exit) {
        mensaje =
            [{
                valor: TextosJScript[116].replace("XXXXXX", quienFallaListaExterna)
            }];
        $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorCab'));
        mostrarError();
        OcultarCargando();
        return false;
    };

    $.each(gLineas, function () {
        if (this.OtrosDatos !== null) {
            var atributosLlamada = '';
            $.each(this.OtrosDatos, function () {
                if (atributosLlamada == '')
                    atributosLlamada = this.Id + '@########@' + this.Valor;
                else {
                    atributosLlamada = atributosLlamada + "##########" + this.Id + '@########@' + this.Valor;
                }
            });

            $.each(this.OtrosDatos, function () {
                if (this.Tipo == 3) {
                    if (this.Valor !== null) {
                        if (this.Valor.toString().toLowerCase().indexOf("date") >= 0) {
                            this.Valor = eval("new " + this.Valor.slice(1, -1));
                        } else {
                            if (this.Valor.toString() == "") {
                                this.Valor = null;
                            }
                        }
                    }
                } else {
                    if (this.Intro == 1) {
                        if (this.ListaExterna) {
                            if (this.Valor !== '') {
                                var IdAtributoListaExterna = this.Id;

                                var sOrgCompras = '';
                                var sCentro = '';
                                if (bHayIntegracionPedidos && gDatosGenerales.UsarOrgCompras) {
                                    sOrgCompras = gDatosGenerales.OrgCompras;
                                    sCentro = $('#cboCentroOrgCompra').val();
                                };
                                var param = JSON.stringify({ 'Empresa': gDatosGenerales.Emp, 'IdAtributoListaExterna': IdAtributoListaExterna, 'atributos': atributosLlamada, 'codigoBuscado': this.Valor, 'denominacionBuscado': '', 'buscador': true, 'OrgCompras': sOrgCompras, 'Centro': sCentro });
                                $.when($.ajax({
                                    type: "POST",
                                    url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Obtener_Datos_ListaExterna',
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    data: param,
                                    async: false
                                })).done(function (msg) {
                                    if (msg.d.length == 0) {
                                        exit = true;
                                    }
                                });
                                if (exit) {
                                    quienFallaListaExterna = this.Denominacion;
                                    return false;
                                }
                            }
                        }
                    }
                }
            });

            if (exit) {
                mensaje =
                    [{
                        valor: TextosJScript[116].replace("XXXXXX", quienFallaListaExterna)
                    }];
                $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
                mostrarError();
                OcultarCargando();
                return false;
            };
        };
        //PlanEntrega
        if (this.ConPlanEntrega)
            if (this.PlanEntrega.length == 0) this.ConPlanEntrega = false;
        if (!ValidarPlanEntrega(this.TipoRecepcion, this.PlanEntrega, this.CantPed, this.ImportePedido, ("0000" + this.NumLinea).slice(-3))) { exit = true; return false; };

        //No cantidad cero en recepción por cantidad             
        if (this.TipoRecepcion == 0) {
            if (this.CantPed == 0) {
                alert(TextosJScript[113].toString());
                exit = true;
                return false;
            }
        }
    });
    if (exit) { OcultarCargando(); return false; };
    if (bOk) {
        var gEmisionPedidoGuardar = jQuery.extend(true, {}, gEmisionPedido);
        var i = 0;
        var lineasBorrar = $("#lineasBorrar").val().toString();
        if (lineasBorrar !== "") {
            var params = JSON.stringify({ iDesdeFavorito: bModificarFavorito, idCabecera: sID, lineas: lineasBorrar });
            $.ajax({
                type: "POST",
                url: ruta + '/App_Pages/EP/EmisionPedido.aspx/BorrarLineas',
                contentType: "application/json; charset=utf-8",
                data: params,
                dataType: "json",
                async: false
            });
        }
        $("#lineasBorrar").val("");
        $.each(gEmisionPedidoGuardar.Lineas, function () {
            gCentroCoste = [];
            gPartidas = [];
            i = i + 1;
            if (this.Pres5_ImportesImputados_EP[0] !== undefined) {
                if (this.Pres5_ImportesImputados_EP[0].CentroCoste !== undefined) {
                    if (this.Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM.PartidasPresupuestarias !== undefined)
                        this.Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM.PartidasPresupuestarias = null;

                    if (this.Pres5_ImportesImputados_EP[0].CentroCoste !== undefined)
                        gCentroCoste.push(JSON.parse(JSON.stringify(this.Pres5_ImportesImputados_EP[0].CentroCoste)));
                    else
                        gCentroCoste.push(new Object());
                }
                if (this.Pres5_ImportesImputados_EP[0].Partida !== undefined) {
                    if (this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto !== null)
                        this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto = null;
                    else
                        this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto = null;

                    if (this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto !== null)
                        this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto = null;
                    else
                        this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto = null;

                    gPartidas.push(JSON.parse(JSON.stringify(this.Pres5_ImportesImputados_EP[0].Partida)));
                } else {
                    gPartidas.push(new Object());
                }
                this.Pres5_ImportesImputados_EP = [];
            } else {
                this.Pres5_ImportesImputados_EP = [];
            }
            ////Adjuntos ya estan en el objeto
            $.each(this.Adjuntos, function () {
                if (this.Fecha !== undefined) {
                    if (this.Fecha.toString().toLowerCase().indexOf("date") >= 0)
                        this.Fecha = eval("new " + this.Fecha.slice(1, -1));
                }
            });
            var params = JSON.stringify({ iDesdeFavorito: bModificarFavorito, oLinea: JSON.stringify(this), oCentroCoste: JSON.stringify(gCentroCoste), oPartidas: JSON.stringify(gPartidas), iCestaCabeceraId: gEmisionPedido.DatosGenerales.Id, bHayIntegracion: bHayIntegracionPedidos, idEmpresa: gDatosGenerales.Emp });
            $.ajax({
                type: "POST",
                url: ruta + '/App_Pages/EP/EmisionPedido.aspx/GuardarLineasPedido',
                contentType: "application/json; charset=utf-8",
                data: params,
                dataType: "json",
                async: false
            });
        });
        if (gEmisionPedidoGuardar.Lineas.length > 0) {
            var sNomVacio = "";
            var params = JSON.stringify({ iDesdeFavorito: bModificarFavorito, oEmisionPedido: JSON.stringify(gEmisionPedidoGuardar), bHayIntegracion: bHayIntegracionPedidos, idEmpresa: gDatosGenerales.Emp, sDescFavorito: sNomVacio, ModoEdicion: ModoEdicion, EsPedidoAbierto: EsPedidoAbierto });
            $.ajax({
                type: "POST",
                url: ruta + '/App_Pages/EP/EmisionPedido.aspx/GuardarPedido',
                contentType: "application/json; charset=utf-8",
                data: params,
                dataType: "json",
                async: false
            });
            $.each(gEmisionPedido.Adjuntos, function () {
                this.Operacion = "";
            });
            $.each(gEmisionPedido.Lineas, function () {
                $.each(this.Adjuntos, function () {
                    this.Operacion = "";
                });
            });
            bOk = recuperarValidarDatosPantalla();
            if (bOk) {
                gEmisionPedidoGuardar = jQuery.extend(true, {}, gEmisionPedido);
                var i = 0;
                gCentroCoste = [];
                gPartidas = [];
                $.each(gEmisionPedidoGuardar.Lineas, function () {
                    i = i + 1;
                    if (this.Pres5_ImportesImputados_EP[0] !== undefined) {
                        if (this.Pres5_ImportesImputados_EP[0].CentroCoste !== undefined) {
                            if (this.Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM.PartidasPresupuestarias !== undefined) {
                                this.Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM.PartidasPresupuestarias = null;
                            }
                            if (this.Pres5_ImportesImputados_EP[0].CentroCoste !== undefined)
                                gCentroCoste.push(JSON.parse(JSON.stringify(this.Pres5_ImportesImputados_EP[0].CentroCoste)));

                            if (this.Pres5_ImportesImputados_EP[0].Partida !== undefined) {
                                if (this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto !== null)
                                    this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto = null;
                                else
                                    this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto = null;

                                if (this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto !== null)
                                    this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto = null;
                                else
                                    this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto = null;

                                gPartidas.push(JSON.parse(JSON.stringify(this.Pres5_ImportesImputados_EP[0].Partida)));
                            } else {
                                gPartidas.push(new Object());
                            }
                            this.Pres5_ImportesImputados_EP = [];
                        } else {
                            gCentroCoste.push(new Object());
                            gPartidas.push(new Object());

                            this.Pres5_ImportesImputados_EP = [];
                        }
                    } else {
                        gCentroCoste.push(new Object());
                        gPartidas.push(new Object());

                        this.Pres5_ImportesImputados_EP = [];
                    }

                    ////Adjuntos ya estan en el objeto
                    $.each(this.Adjuntos, function () {
                        if (this.Fecha !== undefined) {
                            if (this.Fecha.toString().toLowerCase().indexOf("date") >= 0)
                                this.Fecha = eval("new " + this.Fecha.slice(1, -1));
                        }
                    });
                });
                //Comprobar comunicación  
                var params = JSON.stringify({ Empresa: gEmisionPedido.DatosGenerales.Emp });
                var iNotificar = 0;
                $.when($.ajax({
                    type: "POST",
                    url: ruta + '/App_Pages/EP/EmisionPedido.aspx/ComprobarComunicacion',
                    contentType: "application/json; charset=utf-8",
                    data: params,
                    dataType: "json",
                    async: false
                })).done(function (msg) {
                    switch (msg.d) {
                        case 0:
                            //No comunicar
                            iNotificar = 0;
                            break;
                        case 1:
                            //Comunicar
                            iNotificar = 1;
                            break;
                        case 2:
                            //Preguntar al emitir pedido si comunicar al emitir                            
                            if (confirm(TextosJScript[84] + ' ' + TextosJScript[71])) { iNotificar = 1; }
                            break;
                        case 3:
                            //Se comunicarÃ¡ automÃ¡ticamente al integrar
                            iNotificar = 2
                            break;
                        case 4:
                            //Preguntar al emitir el pedido si comunicar al integrar                                                         
                            if (confirm(TextosJScript[84] + ' ' + TextosJScript[72])) { iNotificar = 2; }
                            break;
                    }
                });

                //Errores al emitir
                //1-Error de aprovisionador en favorito
                //2-Error en vigencia de partida pero puede continuar
                //3-Error en vigencia de partida pero no puede continuar
                //4-Existen varios gestores
                //5-Aviso de emision de pedido (el usuario elige si continuar)
                //Texto-Error mapper
                var dImporte = $('#lblImporteTotalCabecera_hdd').text();
                var params = JSON.stringify({ iDesdeFavorito: bModificarFavorito, oEmisionPedido: JSON.stringify(gEmisionPedidoGuardar), oCentroCoste: JSON.stringify(gCentroCoste), oPartidas: JSON.stringify(gPartidas), idFav: sIDFavorito, bValidacionIntegracion: bHayIntegracionPedidos, bContinuarSinAviso: bPartida, dImporte: dImporte, iNotificar: iNotificar, EsPedidoAbierto: EsPedidoAbierto });
                var bError, mensaje;
                var oRespuesta;

                $.when($.ajax({
                    type: "POST",
                    url: ruta + '/App_Pages/EP/EmisionPedido.aspx/EmitirPedido',
                    contentType: "application/json; charset=utf-8",
                    data: params,
                    dataType: "json",
                    async: false
                })).done(function (msg) {
                    oRespuesta = $.parseJSON(msg.d);
                    if (oRespuesta.respuesta == undefined) bError = oRespuesta;
                    else bError = oRespuesta.respuesta;

                    if (bError !== true) {
                        $('#textErrorCab').empty();
                        $('#textError').empty();
                        $('#textErrorPartidaCab').empty();
                        $('#textErrorPartida').empty();
                        $('#textErrorAviso').empty();
                        if (bError == 1) {
                            mensaje =
                                [{
                                    valor: TextosJScript[34]
                                }];
                            $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorCab'));
                            mostrarError();
                        } else {
                            if (bError == 2) {
                                mensaje =
                                    [{
                                        valor: TextosJScript[37]
                                    }];
                                $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorPartidaCab'));
                                var textoError = TextosJScript[38].replace("XXXXXX", sPartidaDen);
                                mensaje =
                                    [{
                                        valor: textoError
                                    }];
                                $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorPartida'));
                                mostrarErrorPartida();
                            } else {
                                if (bError == 3) {
                                    mensaje =
                                        [{
                                            valor: TextosJScript[37]
                                        }];
                                    $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorCab'));
                                    var textoError = TextosJScript[39].replace("XXXXXX", sPartidaDen);
                                    mensaje =
                                        [{
                                            valor: textoError
                                        }];
                                    $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
                                    mostrarError();
                                } else {
                                    if (bError == 4) {
                                        mensaje =
                                            [{
                                                valor: TextosJScript[37]
                                            }];
                                        $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorCab'));
                                        var textoError = TextosJScript[40].replace("XXXXXX", sPartidaDen);
                                        mensaje =
                                            [{
                                                valor: textoError
                                            }];
                                        $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
                                        mostrarError();
                                    } else {
                                        mensaje =
                                            [{
                                                valor: TextosJScript[37]
                                            }];
                                        $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorCab'));
                                        mensaje =
                                            [{
                                                valor: bError
                                            }];
                                        $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
                                        mostrarError();
                                    }
                                }
                            }
                        }
                    } else {
                        //Mostramos el mensaje de confirmacion con el estado del pedido
                        mostrarConfirmacionEmisionPedido(oRespuesta);
                        OcultarCargando();
                    }
                });
            }
        } else {
            //cero filas en el pedido
            mensaje =
                [{
                    valor: TextosJScript[37]
                }];
            $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorCab'));
            mensaje =
                [{
                    valor: TextosJScript[50]
                }];
            $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
            mostrarError();
        }
    }
    OcultarCargando();
}
function recuperarValidarDatosPantalla() {
    //Comprobaciones
    //Receptor
    var bErrorCostesDescuentos = false;
    var mensaje;
    if (gbError !== true) {
        $('#textErrorCab').empty();
        $('#textError').empty();
        $('#textErrorAtributo').empty();
        //Validamos el tipo de pedido
        var bTipoPedido = comprobarTipoPedido();
        if (bTipoPedido == false) {
            gbError = false;
            return false;
        };
        var combo = $("#CmbBoxTipoPedido");
        if (combo.val() == "0") {
            mensaje =
                [{
                    valor: TextosJScript[12]
                }];
            $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorCab'));
            mensaje =
                [{
                    valor: " - " + TextosJScript[48]
                }];
            $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
            mostrarError();
            return false;
        }

        //Comprobar organización de compras
        if (bHayIntegracionPedidos && gDatosGenerales.UsarOrgCompras) {
            var combo = $('[id$=CmbBoxOrgCompras]');
            if (combo.val() == "0" || combo.val() == "") {
                mensaje =
                    [{
                        valor: TextosJScript[12]
                    }];
                $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
                mensaje =
                    [{
                        valor: " - " + TextosJScript[93]
                    }];
                $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
                mostrarError();
                return false;
            }
        }
        if (gDatosGenerales.Tipo.Recepcionable !== 0) {
            if ((gDatosGenerales.Tipo.Recepcionable == 1) || ((gDatosGenerales.Tipo.Recepcionable == 2) && ($("#wddRecept").val() != ""))) {
                //Si es obligatorio u opcional y tiene valor comprobar que el receptor es valido
                var bReceptor = ComprobarReceptor();
                if (bReceptor == false)
                    return false;
            }
        }

        if (bMostrarDireccionEnvioFactura == true) {
            var codDireccion = $("#lblCodDireccion").text();
            if (codDireccion == 0) {
                mensaje =
                    [{
                        valor: TextosJScript[12]
                    }];
                $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorCab'));
                mensaje =
                    [{
                        valor: " - " + TextosJScript[36]
                    }];
                $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
                mostrarError();
                return false;
            }
        }
        //Comprobar obligatoriedad del campo para codificacion personalizada
        if (gbOblCodPedidoObl == true) {
            var numPedSAP = $('[id$=TxtBxPedSAP]').val();
            var lblnumSAP = $('[id$=LblNumSap]').text();
            lblnumSAP = lblnumSAP.substring(4, lblnumSAP.length - 2);
            if (numPedSAP == "") {
                mensaje =
                    [{
                        valor: TextosJScript[12]
                    }];
                $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorCab'));
                mensaje =
                    [{
                        valor: " - " + lblnumSAP
                    }];
                $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
                mostrarError();
                return false;
            }
        }
        ////Otros Datos
        if (gEmisionPedido.OtrosDatos !== undefined && gEmisionPedido.OtrosDatos !== null) {
            if (gEmisionPedido.OtrosDatos.length == 0) {
                gAtributos = null;
                gEmisionPedido.OtrosDatos = null;
                CargarAtributosPedido(false);
            }
        }

        $('#textError').empty();
        ////Costes
        var grupoAnterior = "";
        var grupoAnteriorSel = false;
        var texto = "";
        if (gEmisionPedido.Costes !== null)
            bErrorCostesDescuentos = ComprobarSeleccionado(gEmisionPedido.Costes, 1);

        if (bErrorCostesDescuentos == true) {
            $('#textErrorCab').empty();
            mensaje =
                [{
                    valor: TextosJScript[12]
                }];
            $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorCab'));
            mostrarError();
            return false;
        }

        $('#textError').empty();
        ////Descuentos
        if (gEmisionPedido.Descuentos !== null) {
            bErrorCostesDescuentos = ComprobarSeleccionado(gEmisionPedido.Descuentos, 2);
        }
        if (bErrorCostesDescuentos == true) {
            $('#textErrorCab').empty();
            mensaje =
                [{
                    valor: TextosJScript[12]
                }];
            $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorCab'));
            mostrarError();
            return false;
        }
        if (gEmisionPedido.OtrosDatos !== null) {
            $.each(gEmisionPedido.OtrosDatos, function () {
                if (this.Obligatorio == 1) {
                    if ((this.Valor == null) || ((this.Valor !== null) && (this.Valor.toString() == ""))) {
                        //error!!
                        bErrorCostesDescuentos = true;
                        var texto = "- " + TextosJScript[95] + " : " + this.Codigo + " - " + this.Denominacion;
                        mensaje =
                            [{
                                valor: texto
                            }];
                        $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
                    }
                }
            });
        }

        if (bErrorCostesDescuentos == true) {
            $('#textErrorCab').empty();
            mensaje =
                [{
                    valor: TextosJScript[12]
                }];
            $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorCab'));
            gAtributos = null;
            mostrarError();
            return false;
        }

        //guardar las pestaña linea
        if (iCodFilaSeleccionada !== 0) {
            if ($('#pesDatosLineas').hasClass("escenarioSeleccionadoEP")) guardarOtrosDatosLineas();
            if ($('#pesCostesLinea').hasClass("escenarioSeleccionadoEP")) guardarCostesLinea();
            if ($('#pesDescuentosLinea').hasClass("escenarioSeleccionadoEP")) guardarDescuentosLinea();
        }

        $('#textErrorCab').empty();
        $('#textError').empty();
        mensaje =
            [{
                valor: TextosJScript[16]
            }];
        $('#textErrorCab').empty();
        $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorCab'));
        var bAlmacen = false;
        var bComprobarCantidad = 0;
        var bComprobarImporteBruto = 0;
        var bFechaEntrega = false;
        var bDestino = false;
        var bCentroAprov = false;
        var bEmpresa = true;
        var bPartida = false;
        var bErrorOtrosDatos = false;
        var bErrorDescuentos = false;
        var bActivo = false;
        var bFecEntrega = false;
        var bErrorCantidadImportePedidoAbierto = false;
        var oLineaValidacion;
        var validacionesPedidoAbierto = {};
        var bPres1 = false;
        var bPres2 = false;
        var bPres3 = false;
        var bPres4 = false;
        var bComprobarPluriAnual = 0;
        articulos = new Array();
        $.each(gLineas, function () {
            var bErrorLinea = false;
            devolverTipoLinea(this);
            if (this.LineaCatalogo !== 0) {
                //solo valido el artículo si no lo he validado antes
                //mejorable si en lugar de pasar la linea pasamos el código de artículo directamtne... 
                if (articulos[this.Cod] == null) {
                    articulos[this.Cod] = this.Cod;
                    bEmpresa = comprobarEmpresa(this.ID);
                    if (bEmpresa == false) {
                        gbError = false;
                        return false;
                    }
                }
            }
            oLineaValidacion = this;
            if (EsPedidoAbierto) {
                if (validacionesPedidoAbierto[oLineaValidacion.IdLineaPedidoAbierto] == undefined) {
                    $.when($.ajax({
                        type: "POST",
                        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/Comprobar_Importes_Cantidades_PedidoAbierto',
                        contentType: "application/json; charset=utf-8",
                        data: "{'IdLineaPedidoAbierto':" + oLineaValidacion.IdLineaPedidoAbierto + "}",
                        dataType: "json",
                        async: false
                    })).done(function (msg) {
                        var validacion = $.parseJSON(msg.d);
                        validacionesPedidoAbierto[oLineaValidacion.IdLineaPedidoAbierto] = { CantPed: validacion.CantPed, CantPedAbierto: validacion.CantPedAbierto, ImportePed: validacion.ImportePed, ImportePedAbierto: validacion.ImportePedAbierto };
                    });
                }
                var texto;
                if (oLineaValidacion.TipoRecepcion == 0) {
                    validacionesPedidoAbierto[oLineaValidacion.IdLineaPedidoAbierto].CantPedAbierto += oLineaValidacion.CantPed;
                    if (validacionesPedidoAbierto[oLineaValidacion.IdLineaPedidoAbierto].CantPed < validacionesPedidoAbierto[oLineaValidacion.IdLineaPedidoAbierto].CantPedAbierto) {
                        texto = TextosJScript[87].replace('\n', '<br/>');
                        bErrorCantidadImportePedidoAbierto = true;
                    }
                } else {
                    validacionesPedidoAbierto[oLineaValidacion.IdLineaPedidoAbierto].ImportePedAbierto += oLineaValidacion.ImportePed;
                    if (validacionesPedidoAbierto[oLineaValidacion.IdLineaPedidoAbierto].ImportePed < validacionesPedidoAbierto[oLineaValidacion.IdLineaPedidoAbierto].ImportePedAbierto) {
                        texto = TextosJScript[88];
                        bErrorCantidadImportePedidoAbierto = true;
                    }
                }
                if (bErrorCantidadImportePedidoAbierto) {
                    var valorPendiente;
                    if (oLineaValidacion.TipoRecepcion == 0)
                        valorPendiente = (validacionesPedidoAbierto[oLineaValidacion.IdLineaPedidoAbierto].CantPed - validacionesPedidoAbierto[oLineaValidacion.IdLineaPedidoAbierto].CantPedAbierto + oLineaValidacion.CantPed);
                    else
                        valorPendiente = (validacionesPedidoAbierto[oLineaValidacion.IdLineaPedidoAbierto].ImportePed - validacionesPedidoAbierto[oLineaValidacion.IdLineaPedidoAbierto].ImportePedAbierto + oLineaValidacion.ImportePed);
                    mensaje = [{ valor: texto }];
                    $('#textErrorCab').empty();
                    $('#textError').empty();
                    $('#MensajeError').tmpl([{ valor: TextosJScript[86] }]).appendTo($('#textErrorCab'));
                    $('#textError').html($('#MensajeError').tmpl(mensaje)[0].innerText);
                    $('#aviso' + this.ID).css("display", "inline-block");
                    $('#aviso' + this.ID).prop('title', (oLineaValidacion.TipoRecepcion == 0 ? TextosJScript[89] + ': ' : TextosJScript[90] + ': ') + valorPendiente);
                    ponerAvisoEnLinea(this.ID);
                    bErrorLinea = true;
                    return false;
                }
            }

            if (this.TipoRecepcion == 0) {
                bComprobarCantidad = comprobarCantidad(this.ID);
                if (bComprobarCantidad !== 0) {
                    return false;
                }
            } else {
                bComprobarImporteBruto = comprobarImporteBruto(this.ID);
                if (bComprobarImporteBruto !== 0) {
                    return false;
                }
            }

            //9624
            if (bAccesoFSSM == true) {
                gCentroContrato = this.Pres5_ImportesImputados_EP[0];

                if (gCentroContrato !== undefined) {
                    bComprobarPluriAnual = ComprobarCentroContratoAEmitirPluriAnual(this.ID);
                    if (bComprobarPluriAnual !== 100000) {
                        return false;
                    }
                }
            } else bComprobarPluriAnual = 100000;

            var iNumber;
            if (bAccesoFSSM == true && bSMOblig == true) {
                gCentroContrato = this.Pres5_ImportesImputados_EP[0];
                if (gCentroContrato == undefined) {
                    iNumber = 0;
                } else {
                    iNumber = ComprobarCentroContratoAEmitir();
                }
                this.Pres5_ImportesImputados_EP[0] = gCentroContrato;
                if (iNumber !== true && iNumber !== 1) {
                    var mensaje;
                    var textoError = TextosJScript[41].replace("XXXXXX", sPartidaDen);
                    mensaje =
                        [{
                            valor: " - " + textoError
                        }];
                    if (bPartida == false) {
                        bPartida = true;
                        $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
                    }
                    $('#aviso' + this.ID).css("display", "inline-block");

                    $('#aviso' + this.ID).prop('title', textoError);
                    ponerAvisoEnLinea(this.ID);
                    bErrorLinea = true;
                }
            }
            var numLinea = "0000" + this.NumLinea;
            numLinea = numLinea.substring(numLinea.length - 3, numLinea.length);
            if (this.OtrosDatos !== null) {
                $.each(this.OtrosDatos, function () {
                    if (this.Obligatorio == true) {
                        if (this.Valor.toString() == "") {
                            //error!!                                
                            var texto = "- " + TextosJScript[15] + ' - ' + TextosJScript[51] + " " + numLinea + " : " + this.Codigo + " - " + this.Denominacion;
                            mensaje =
                                [{
                                    valor: texto
                                }];
                            $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
                            if (bErrorOtrosDatos == false) {
                                bErrorOtrosDatos = true;

                            }
                            $('#aviso' + this.ID).css("display", "inline-block");
                            $('#aviso' + this.ID).prop('title', texto);
                            ponerAvisoEnLinea(this.ID);
                            bErrorLinea = true;
                        }
                    }
                });
            }

            grupoAnterior = "";
            grupoAnteriorSel = false;
            if (this.Costes !== null) {
                var bErrorFila = ComprobarSeleccionado(this.Costes, 1);
                if (bErrorFila == true) {
                    if (bErrorCostesDescuentos == false) {
                        bErrorCostesDescuentos = true;
                    }
                    $('#aviso' + this.ID).css("display", "inline-block");
                    $('#aviso' + this.ID).prop('title', TextosJScript[13]);
                    ponerAvisoEnLinea(this.ID);
                    bErrorLinea = true;
                }
            }


            grupoAnterior = "";
            grupoAnteriorSel = false;
            if (this.Descuentos !== null) {
                var bErrorFila = ComprobarSeleccionado(this.Descuentos, 2);
                if (bErrorFila == true) {
                    if (bErrorDescuentos == false) {
                        bErrorDescuentos = true;
                    }
                    $('#aviso' + this.ID).css("display", "inline-block");
                    $('#aviso' + this.ID).prop('title', TextosJScript[14]);
                    ponerAvisoEnLinea(this.ID);
                    bErrorLinea = true;
                }
            }

            //Centro de aprovisionamiento
            if (bHayIntegracionPedidos && gDatosGenerales.UsarOrgCompras) {
                if (this.CentroAprovisionamiento == "") {
                    if (bCentroAprov == false) { bCentroAprov = true; }
                    mensaje =
                        [{
                            valor: "- " + TextosJScript[15] + ' - ' + TextosJScript[51] + " " + numLinea + " : " + TextosJScript[94]
                        }];
                    $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
                    $('#aviso' + this.ID).css("display", "inline-block");
                    $('#aviso' + this.ID).prop('title', TextosJScript[94]);
                    ponerAvisoEnLinea(this.ID);
                    bErrorLinea = true;
                }
            }

            if (this.Dest == 0 || this.Dest == null || this.Dest == '') {
                if (bDestino == false) {
                    bDestino = true;
                }
                mensaje =
                    [{
                        valor: "- " + TextosJScript[15] + ' - ' + TextosJScript[51] + " " + numLinea + " : " + TextosJScript[31]
                    }];
                $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));

                $('#aviso' + this.ID).css("display", "inline-block");
                $('#aviso' + this.ID).prop('title', TextosJScript[31]);
                ponerAvisoEnLinea(this.ID);
                bErrorLinea = true;
            }

            //Fecha de entrega
            if (bOblFecEntrega && (this.FecEntrega == null || this.FecEntrega == "")) {
                if (!bFecEntrega) bFecEntrega = true;
                mensaje = [{ valor: "- " + TextosJScript[15] + ' - ' + TextosJScript[51] + " " + numLinea + " : " + TextosJScript[111] }];
                $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
                $('#aviso' + this.ID).css("display", "inline-block");
                $('#aviso' + this.ID).prop('title', TextosJScript[111]);
                ponerAvisoEnLinea(this.ID);
                bErrorLinea = true;
            }

            if (this.TipoArticulo.TipoAlmacenamiento !== 0) {
                if (this.IdAlmacen == 0) {
                    var bHayAlmacenes = comprobarAlmacenes(this.Dest, this, this.CentroAprovisionamiento);
                    if (bHayAlmacenes == true) {
                        if (bObliAlmacen == true) {
                            if (bAlmacen == false) {
                                bAlmacen = true;
                            }
                            mensaje =
                                [{
                                    valor: "- " + TextosJScript[15] + ' - ' + TextosJScript[51] + " " + numLinea + " : " + TextosJScript[60]
                                }];
                            $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));

                            $('#aviso' + this.ID).css("display", "inline-block");
                            $('#aviso' + this.ID).prop('title', TextosJScript[60]);
                            ponerAvisoEnLinea(this.ID);
                            bErrorLinea = true;
                        }
                    }
                }
            }
            //Presup 1..4: Obligatorio
            if (gDatosGenerales.GS_Pres1.Obligatorio) {
                if ((this.Pres1 == null ? '' : this.Pres1) == '') {
                    if (bPres1 == false) {
                        bPres1 = true;
                    }
                    mensaje =
                        [{
                            valor: "- " + TextosJScript[15] + ' - ' + TextosJScript[51] + " " + numLinea + " : " + gDatosGenerales.GS_Pres1.Denominacion
                        }];
                    $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));

                    $('#aviso' + this.ID).css("display", "inline-block");
                    $('#aviso' + this.ID).prop('title', gDatosGenerales.GS_Pres1.Denominacion);
                    ponerAvisoEnLinea(this.ID);
                    bErrorLinea = true;
                }
            }
            if (gDatosGenerales.GS_Pres1.NivelMinimo > 0) {
                if ((this.Pres1 == null ? '' : this.Pres1) !== '') {
                    var pres = this.Pres1.substring(0, 1);
                    if (parseInt(pres) < gDatosGenerales.GS_Pres1.NivelMinimo) {
                        if (bPres1 == false) {
                            bPres1 = true;
                        }
                        mensaje =
                            [{
                                valor: "- " + TextosJScript[15] + ' - ' + TextosJScript[51] + " " + numLinea + " : " + gDatosGenerales.GS_Pres1.Denominacion + ", " + TextosJScript[112] + " " + gDatosGenerales.GS_Pres1.NivelMinimo
                            }];
                        $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));

                        $('#aviso' + this.ID).css("display", "inline-block");
                        $('#aviso' + this.ID).prop('title', gDatosGenerales.GS_Pres1.Denominacion);
                        ponerAvisoEnLinea(this.ID);
                        bErrorLinea = true;
                    }
                }
            }
            if (gDatosGenerales.GS_Pres2.Obligatorio) {
                if ((this.Pres2 == null ? '' : this.Pres2) == '') {
                    if (bPres2 == false) {
                        bPres2 = true;
                    }
                    mensaje =
                        [{
                            valor: "- " + TextosJScript[15] + ' - ' + TextosJScript[51] + " " + numLinea + " : " + gDatosGenerales.GS_Pres2.Denominacion
                        }];
                    $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));

                    $('#aviso' + this.ID).css("display", "inline-block");
                    $('#aviso' + this.ID).prop('title', gDatosGenerales.GS_Pres2.Denominacion);
                    ponerAvisoEnLinea(this.ID);
                    bErrorLinea = true;
                }
            }
            if (gDatosGenerales.GS_Pres2.NivelMinimo > 0) {
                if ((this.Pres2 == null ? '' : this.Pres2) !== '') {
                    var pres = this.Pres2.substring(0, 1);
                    if (parseInt(pres) < gDatosGenerales.GS_Pres2.NivelMinimo) {
                        if (bPres2 == false) {
                            bPres2 = true;
                        }
                        mensaje =
                            [{
                                valor: "- " + TextosJScript[15] + ' - ' + TextosJScript[51] + " " + numLinea + " : " + gDatosGenerales.GS_Pres2.Denominacion + ", " + TextosJScript[112] + " " + gDatosGenerales.GS_Pres2.NivelMinimo
                            }];
                        $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));

                        $('#aviso' + this.ID).css("display", "inline-block");
                        $('#aviso' + this.ID).prop('title', gDatosGenerales.GS_Pres2.Denominacion);
                        ponerAvisoEnLinea(this.ID);
                        bErrorLinea = true;
                    }
                }
            }
            if (gDatosGenerales.GS_Pres3.Obligatorio) {
                if ((this.Pres3 == null ? '' : this.Pres3) == '') {
                    if (bPres3 == false) {
                        bPres3 = true;
                    }
                    mensaje =
                        [{
                            valor: "- " + TextosJScript[15] + ' - ' + TextosJScript[51] + " " + numLinea + " : " + gDatosGenerales.GS_Pres3.Denominacion
                        }];
                    $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));

                    $('#aviso' + this.ID).css("display", "inline-block");
                    $('#aviso' + this.ID).prop('title', gDatosGenerales.GS_Pres3.Denominacion);
                    ponerAvisoEnLinea(this.ID);
                    bErrorLinea = true;
                }
            }
            if (gDatosGenerales.GS_Pres3.NivelMinimo > 0) {
                if ((this.Pres3 == null ? '' : this.Pres3) !== '') {
                    var pres = this.Pres3.substring(0, 1);
                    if (parseInt(pres) < gDatosGenerales.GS_Pres3.NivelMinimo) {
                        if (bPres3 == false) {
                            bPres3 = true;
                        }
                        mensaje =
                            [{
                                valor: "- " + TextosJScript[15] + ' - ' + TextosJScript[51] + " " + numLinea + " : " + gDatosGenerales.GS_Pres3.Denominacion + ", " + TextosJScript[112] + " " + gDatosGenerales.GS_Pres3.NivelMinimo
                            }];
                        $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));

                        $('#aviso' + this.ID).css("display", "inline-block");
                        $('#aviso' + this.ID).prop('title', gDatosGenerales.GS_Pres3.Denominacion);
                        ponerAvisoEnLinea(this.ID);
                        bErrorLinea = true;
                    }
                }
            }
            if (gDatosGenerales.GS_Pres4.Obligatorio) {
                if ((this.Pres4 == null ? '' : this.Pres4) == '') {
                    if (bPres4 == false) {
                        bPres4 = true;
                    }
                    mensaje =
                        [{
                            valor: "- " + TextosJScript[15] + ' - ' + TextosJScript[51] + " " + numLinea + " : " + gDatosGenerales.GS_Pres4.Denominacion
                        }];
                    $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));

                    $('#aviso' + this.ID).css("display", "inline-block");
                    $('#aviso' + this.ID).prop('title', gDatosGenerales.GS_Pres4.Denominacion);
                    ponerAvisoEnLinea(this.ID);
                    bErrorLinea = true;
                }
            }
            if (gDatosGenerales.GS_Pres4.NivelMinimo > 0) {
                if ((this.Pres4 == null ? '' : this.Pres4) !== '') {
                    var pres = this.Pres4.substring(0, 1);
                    if (parseInt(pres) < gDatosGenerales.GS_Pres4.NivelMinimo) {
                        if (bPres4 == false) {
                            bPres4 = true;
                        }
                        mensaje =
                            [{
                                valor: "- " + TextosJScript[15] + ' - ' + TextosJScript[51] + " " + numLinea + " : " + gDatosGenerales.GS_Pres4.Denominacion + ", " + TextosJScript[112] + " " + gDatosGenerales.GS_Pres4.NivelMinimo
                            }];
                        $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));

                        $('#aviso' + this.ID).css("display", "inline-block");
                        $('#aviso' + this.ID).prop('title', gDatosGenerales.GS_Pres4.Denominacion);
                        ponerAvisoEnLinea(this.ID);
                        bErrorLinea = true;
                    }
                }
            }
            //fin Pres 1..4

            if (bActivos == true && (gTipoLinea.Concepto !== 0 && (gTipoLinea.Concepto !== 2 || gTipoLinea.TipoRecepcion !== 1))) {
                if (gTipoLinea.Concepto == 1) {
                    if (this.Activo == 0) {
                        if (bActivo == false) {
                            bActivo = true;
                        }
                        mensaje =
                            [{
                                valor: "- " + TextosJScript[15] + ' - ' + TextosJScript[51] + " " + numLinea + " : " + TextosJScript[59]
                            }];
                        $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));

                        $('#aviso' + this.ID).css("display", "inline-block");
                        $('#aviso' + this.ID).prop('title', TextosJScript[58]);
                        ponerAvisoEnLinea(this.ID);
                        bErrorLinea = true;
                    }
                }
            }
            if (bErrorLinea == false) quitarAvisoEnLineaDef(this.ID);
        });
        if (bErrorCostesDescuentos || bErrorOtrosDatos || bErrorDescuentos || bErrorCantidadImportePedidoAbierto) {
            mostrarError();
            return false;
        }
        if (!bEmpresa || bComprobarCantidad !== 0 || bComprobarImporteBruto !== 0 || bFechaEntrega || bDestino || bAlmacen || bPartida || bActivo || bCentroAprov || bFecEntrega) {
            mostrarError();
            return false;
        }
        if (bPres1 || bPres2 || bPres3 || bPres4) {
            mostrarError();
            return false;
        }
        if (bComprobarPluriAnual !== 100000) {
            mostrarError();
            return false;
        }
        //Proveedor ERP
        if ((bHayIntegracionPedidos == true) && (bdevolverCodErp == true)) {
            if ($("#CmbBoxProvesErp").val() == 0 || $("#CmbBoxProvesErp").val() == null) {
                $('#textErrorCab').empty();
                mensaje =
                    [{
                        valor: TextosJScript[12]
                    }];
                $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorCab'));
                mensaje =
                    [{
                        valor: " - " + TextosJScript[33]
                    }];
                $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
                mostrarError();
                return false;
            }
        }
        $('[id^=aviso]').hide();
        gbError = false;
        return true;
    } else gbError = false; return false;
}

function CargaAlmacenUnico(idSeleccionado, LineaAlmacen, sCentro) {
    var Centro = "";
    var Destino = "";
    if (bHayIntegracionPedidos && gDatosGenerales.UsarOrgCompras) Centro = sCentro;
    else Destino = idSeleccionado;

    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarAlmacenes',
        contentType: "application/json; charset=utf-8",
        data: "{'idDestino':'" + Destino + "','sCentro':'" + Centro + "'}",
        dataType: "json",
        async: false
    })).done(function (msg) {
        var bExcluirArticulo = false;
        if (gEmisionPedido.DatosGenerales.Tipo !== null) {
            if ((LineaAlmacen.TipoArticulo.TipoAlmacenamiento == 0 && gEmisionPedido.DatosGenerales.Tipo.Almacenable == 1) || (LineaAlmacen.TipoArticulo.TipoAlmacenamiento == 1 && gEmisionPedido.DatosGenerales.Tipo.Almacenable == 0))
                bExcluirArticulo = true;
            else bExcluirArticulo = false;
        } else bExcluirArticulo = false;
        if ((gEmisionPedido.DatosGenerales.Tipo !== null) && (LineaAlmacen.TipoArticulo.TipoAlmacenamiento !== 0))
            visible = ((gEmisionPedido.DatosGenerales.Tipo.Almacenable !== 0) && (bExcluirArticulo == false));
        else visible = true;
        if (visible) {
            var gHayDatosAlmacenes = $.parseJSON(msg.d);
            if (gHayDatosAlmacenes !== null) {
                if (gHayDatosAlmacenes.length == 1) {
                    LineaAlmacen.IdAlmacen = gHayDatosAlmacenes[0].Id;
                }
            }
        }
    });
}

function comprobarAlmacenes(idSeleccionado, LineaAlmacen, sCentro) {
    var bComprobarAlmacenes = false;
    var Centro = "";
    var Destino = "";
    if (bHayIntegracionPedidos && gDatosGenerales.UsarOrgCompras) Centro = sCentro;
    else Destino = idSeleccionado;

    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarAlmacenes',
        contentType: "application/json; charset=utf-8",
        data: "{'idDestino':'" + Destino + "','sCentro':'" + Centro + "'}",
        dataType: "json",
        async: false
    })).done(function (msg) {
        var Almacenamiento = LineaAlmacen.TipoArticulo.TipoAlmacenamiento;
        var bExcluirArticulo = false;
        if (gEmisionPedido.DatosGenerales.Tipo !== null) {
            if ((LineaAlmacen.TipoArticulo.TipoAlmacenamiento == 0 && gEmisionPedido.DatosGenerales.Tipo.Almacenable == 1) || (LineaAlmacen.TipoArticulo.TipoAlmacenamiento == 1 && gEmisionPedido.DatosGenerales.Tipo.Almacenable == 0))
                bExcluirArticulo = true;
            else bExcluirArticulo = false;
        } else bExcluirArticulo = false;
        var visible = true;
        //Demo Berge / 2017 / 16: Si el artículo es de almacen obligatorio/opcional se debe controlar ALMACEN aunque en el tipo de pedido sea Opcional
        if ((gEmisionPedido.DatosGenerales.Tipo !== null) && (LineaAlmacen.TipoArticulo.TipoAlmacenamiento !== 0))
            visible = ((gEmisionPedido.DatosGenerales.Tipo.Almacenable !== 0) && (bExcluirArticulo == false));
        else visible = true;

        if (visible) {
            var gHayDatosAlmacenes = $.parseJSON(msg.d);
            //Demo Berge / 2017 / 16: Si el artículo es de almacen obligatorio/opcional se debe controlar ALMACEN aunque en el tipo de pedido sea Opcional
            if (gHayDatosAlmacenes == null) {
                bComprobarAlmacenes = false;
                bObliAlmacen = false;

                if ((LineaAlmacen.TipoArticulo.TipoAlmacenamiento == 1) || (LineaAlmacen.TipoArticulo.TipoAlmacenamiento == 2)) {
                    bComprobarAlmacenes = true;
                    cargarComboAlmacen(gHayDatosAlmacenes);
                    bObliAlmacen = ((LineaAlmacen.TipoArticulo.TipoAlmacenamiento == 1) || (gEmisionPedido.DatosGenerales.Tipo.Almacenable == 1));
                }
            } else {
                if (gHayDatosAlmacenes.length == 0) {
                    bComprobarAlmacenes = false;
                    bObliAlmacen = false;

                    if ((LineaAlmacen.TipoArticulo.TipoAlmacenamiento == 1) || (LineaAlmacen.TipoArticulo.TipoAlmacenamiento == 2)) {
                        bComprobarAlmacenes = true;
                        cargarComboAlmacen(gHayDatosAlmacenes);
                        bObliAlmacen = ((LineaAlmacen.TipoArticulo.TipoAlmacenamiento == 1) || (gEmisionPedido.DatosGenerales.Tipo.Almacenable == 1));
                    }
                } else {
                    if (gHayDatosAlmacenes.length == 1) {
                        LineaAlmacen.IdAlmacen = gHayDatosAlmacenes[0].Id;
                    }

                    bComprobarAlmacenes = true;
                    cargarComboAlmacen(gHayDatosAlmacenes);

                    if (gEmisionPedido.DatosGenerales.Tipo !== null) {
                        if ((LineaAlmacen.TipoArticulo.TipoAlmacenamiento == 1) || (gEmisionPedido.DatosGenerales.Tipo.Almacenable == 1)) bObliAlmacen = true;
                        else bObliAlmacen = false;
                    } else bObliAlmacen = (LineaAlmacen.TipoArticulo.TipoAlmacenamiento == 1);

                    if (bObliAlmacen == true) {
                        if ($('#LblAlmacen').text().indexOf("*") <= 0) {
                            $('#LblAlmacen').text("(*)" + TextosJScript[60] + " :");
                        }
                    } else {
                        if ($('#LblAlmacen').text().indexOf("*") > 0) {
                            $('#LblAlmacen').text(TextosJScript[60] + " :");
                        }
                    }
                }
            }
        } else {
            bComprobarAlmacenes = false;
            bObliAlmacen = false;
        }
    });
    return bComprobarAlmacenes;
}
function ComprobarSeleccionado(Atributos, tipo) {
    var bErrorCostesDescuentos = false;
    var grupoAnterior = "";
    var grupoAnteriorSel = false;
    var mensaje;

    $.each(Atributos, function () {
        if (this.TipoCosteDescuento == 1 && bErrorCostesDescuentos == false) {
            if (grupoAnterior !== "") {
                if (this.GrupoCosteDescuento !== grupoAnterior) {
                    if (grupoAnteriorSel !== true) {
                        //error!!
                        grupoAnterior = this.GrupoCosteDescuento;
                        if (tipo == 1) {
                            texto = "- " + TextosJScript[13] + ": " + this.Codigo + " - " + this.Denominacion;
                        } else { texto = "- " + TextosJScript[14] + ": " + this.Codigo + " - " + this.Denominacion; }
                        mensaje =
                            [{
                                valor: texto
                            }];
                        $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorAtributo'));
                        bErrorCostesDescuentos = true;
                    } else {
                        $('#textErrorAtributo').empty();
                        grupoAnterior = this.GrupoCosteDescuento;
                        if (this.Seleccionado == true) {
                            grupoAnteriorSel = true;
                        } else {
                            grupoAnteriorSel = false;
                        }
                        if (tipo == 1) {
                            texto = "- " + TextosJScript[13] + ": " + this.Codigo + " - " + this.Denominacion;
                        } else { texto = "- " + TextosJScript[14] + ": " + this.Codigo + " - " + this.Denominacion; }
                        mensaje =
                            [{
                                valor: texto
                            }];
                        $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorAtributo'));
                    }
                } else {
                    grupoAnterior = this.GrupoCosteDescuento;
                    if (this.Seleccionado == true || grupoAnteriorSel == true || this.TipoCosteDescuento !== 1) {
                        grupoAnteriorSel = true;
                    } else {
                        if (tipo == 1) {
                            texto = "- " + TextosJScript[13] + ": " + this.Codigo + " - " + this.Denominacion;
                        } else { texto = "- " + TextosJScript[14] + ": " + this.Codigo + " - " + this.Denominacion; }
                        mensaje =
                            [{
                                valor: texto
                            }];
                        $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorAtributo'));
                    }
                }
            } else {
                $('#textErrorAtributo').empty();
                grupoAnterior = this.GrupoCosteDescuento;
                if (this.Seleccionado == true) {
                    grupoAnteriorSel = true;
                } else {
                    if (tipo == 1) {
                        texto = "- " + TextosJScript[13] + ": " + this.Codigo + " - " + this.Denominacion;
                    } else { texto = "- " + TextosJScript[14] + ": " + this.Codigo + " - " + this.Denominacion; }
                    mensaje =
                        [{
                            valor: texto
                        }];
                    $('#MensajeError').tmpl(mensaje).appendTo($('#textErrorAtributo'));
                }
            }
        } else {
            if (grupoAnterior !== "") {
                //error!!
                if (grupoAnteriorSel !== true)
                    bErrorCostesDescuentos = true;
            }
        }
    });
    if (bErrorCostesDescuentos == false)
        $('#textErrorAtributo').empty();

    return bErrorCostesDescuentos
}
function ComprobarReceptor() {
    var bExiste = true;
    var bHayError = false;
    if ($("#wddRecept").val() == "") {
        mostrarOcultarError(TextosJScript[17]);
        bExiste = false;
    } else {
        var recep = $("#wddRecept").val();
        var params = JSON.stringify({ idOrden: sID, sReceptor: recep });
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/ValidarReceptor',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        })).done(function (msg) {
            var lineas = msg.d;
            if (lineas !== null) {
                var parts = lineas.toString().split("-");
                if (gLineas.length >= parts.length) {
                    var bHayLinea = false;
                    $.each(gLineas, function () {
                        bHayLinea = false;
                        for (var i = 0; i < parts.length; i++) {
                            if (this.ID == parts[i]) {
                                bHayLinea = true;
                                break;
                            }
                        }

                        if (bHayLinea == false) {
                            mostrarOcultarError(TextosJScript[17]);
                            $('#aviso' + this.ID).css("display", "inline-block");
                            $('#aviso' + this.ID).prop('title', TextosJScript[11]);
                            ponerAvisoEnLinea(this.ID);
                            bExiste = false;
                        }
                    });
                } else bExiste = true;
            } else {
                bExiste = false;
                mostrarOcultarError(TextosJScript[17]);
                $('#aviso' + this.ID).css("display", "inline-block");
                $('#aviso' + this.ID).prop('title', TextosJScript[11]);
                ponerAvisoEnLinea(this.ID);
            }
        });
    }
    return bExiste;
}
//Gestion Pedidos Generados
function mostrarPedidoGenerado() {
    $('#divPedido').css('z-index', '200002');
    $('#divPedido').show();
    CentrarPopUp($('#divPedido'));
}
function MostrarFondoPopUp() {
    $('#popupFondo').css('height', $(document).height());
    $('#popupFondo').css('z-index', 9800);
    $('#popupFondo').show();
}
function OcultarFondoPopUp() {
    $('#popupFondo').hide();
    $('#popupFondo').css('z-index', 1001);
}
function ponerAvisoEnLinea(idLinea) {
    $('#PrecT' + idLinea).css("color", "red");
    $('#Uni' + idLinea).css("color", "red");
    $('#lblCosteTotalLinea_' + idLinea).css("color", "red");
    $('#lblDescuentoTotalLinea_' + idLinea).css("color", "red");
    $('#lblImpBrutolin_' + idLinea).css("color", "red");
    $('#num' + idLinea).css("color", "red");
    $('#COD_ITEM_' + idLinea).removeClass("DescArt");
    $('#COD_ITEM_' + idLinea).addClass("DescArtRed");
    $('#adr' + idLinea).css("display", "red");
}
function quitarAvisoEnLineaDef(idLinea) {
    $('#PrecT' + idLinea).css("color", "black");
    $('#Uni' + idLinea).css("color", "black");
    $('#lblCosteTotalLinea_' + idLinea).css("color", "black");
    $('#lblDescuentoTotalLinea_' + idLinea).css("color", "black");
    $('#lblImpBrutolin_' + idLinea).css("color", "black");
    $('#num' + idLinea).css("color", "black");
    $('#COD_ITEM_' + idLinea).removeClass("DescArtRed");
    $('#COD_ITEM_' + idLinea).addClass("DescArt");
    $('#adr').hide();
}
function quitarAvisoEnLinea() {
    $('[id^=PrecT]').css("color", "black");
    $('[id^=Uni]').css("color", "black");
    $('[id^=lblCosteTotalLinea_]').css("color", "black");
    $('[id^=lblDescuentoTotalLinea_]').css("color", "black");
    $('[id^=lblImpBrutolin_]').css("color", "black");
    $('[id^=num]').css("color", "black");
    $('[id^=COD_ITEM_]').removeClass("DescArtRed");
    $('[id^=COD_ITEM_]').addClass("DescArt");
    $('[id^=adr]').hide();
}
//funcion que muestra el panel con el estado del pedido que se acaba de emitir /oRespuesta: objeto con el estado del pedido
function mostrarConfirmacionEmisionPedido(oRespuesta) {
    if (oRespuesta.estado == 0) $("#lblEstadoConfirmPedido").text(TextosJScript[53]);
    else $("#lblEstadoConfirmPedido").text(TextosJScript[54]);

    $("#hypPedidoConfirmPedido").attr("href", "Seguimiento.aspx?orden=" + oRespuesta.anyo + '/' + oRespuesta.ped_num + '/' + oRespuesta.oe_num)
    $("#hypPedidoConfirmPedido").text(oRespuesta.anyo + '/' + oRespuesta.ped_num + '/' + oRespuesta.oe_num)
    $("#lblImporteConfirmPedido").val(oRespuesta.importe)
    $("#lblImporteConfirmPedido").text(formatNumber(oRespuesta.importe) + " " + oRespuesta.moneda)
    $("#lblProveedorConfirmPedido").val(oRespuesta.prove)
    $("#lblProveedorConfirmPedido").text(oRespuesta.prove)
    if (oRespuesta.iniciaFlujo == 0) {
        $("[id$=hid_btnAceptarConfirmPedido]").val(0)
    } else {
        $("[id$=hid_btnAceptarConfirmPedido]").val(1)
    }
    MostrarFondoPopUp();
    $('#pnlConfirmacionPedidoEmitido').show();
    CentrarPopUp($('#pnlConfirmacionPedidoEmitido'));
}
function CopiarDatosLineas(Id) {
    cambiarImgCopiar('#btnCopiarDatos_' + Id);
    gLineas = gEmisionPedido.Lineas;
    var valor, valorDen;
    for (var i = 0; i < Linea.OtrosDatos.length; i++) {
        if (Linea.OtrosDatos[i].Id == Id) {
            if (Linea.OtrosDatos[i].Intro == 1) {
                if (Linea.OtrosDatos[i].ListaExterna) {
                    if ($('#atribListaExterna_' + Linea.OtrosDatos[i].Id).tokenInput('get').length) {
                        var selectedToken = $('#atribListaExterna_' + Linea.OtrosDatos[i].Id).tokenInput('get')[0];
                        valor = selectedToken.id;
                        valorDen = selectedToken.name; //Aprovechamos que existe un array de valores para guardar el texto de un atributo tipo lista externa
                    }
                } else {
                    if ($('#cboAtribLineaEP_' + Linea.OtrosDatos[i].Id).length)
                        valor = $('#fsComboValueDisplay_cboAtribLineaEP_' + Linea.OtrosDatos[i].Id).val();
                }
            } else {
                switch (Linea.OtrosDatos[i].Tipo.toString()) {
                    case "1":
                        if ($('#inputTextAtribLineaEP_' + Linea.OtrosDatos[i].Id).length) {
                            valor = $('#inputTextAtribLineaEP_' + Linea.OtrosDatos[i].Id).val()
                        }
                        break;
                    case "2":
                        if ($('#inputNumAtribLineaEP_' + Linea.OtrosDatos[i].Id).length) {
                            valor = $('#inputNumAtribLineaEP_' + Linea.OtrosDatos[i].Id).val()
                        }
                        break;
                    case "3":
                        if ($('#inputFechaAtribLineaEP_' + Linea.OtrosDatos[i].Id).length) {
                            valor = $('#inputFechaAtribLineaEP_' + Linea.OtrosDatos[i].Id).datepicker("getDate");
                        }
                        break;
                    case "4":
                        if ($('#cboAtribLineaEP_' + Linea.OtrosDatos[i].Id).length) {
                            if ($('#fsComboValueDisplay_cboAtribLineaEP_' + Linea.OtrosDatos[i].Id).val() == TextosBoolean[0]) valor = 0;
                            else
                                if ($('#fsComboValueDisplay_cboAtribLineaEP_' + Linea.OtrosDatos[i].Id).val() == TextosBoolean[1]) valor = 1;
                        }
                        break;
                }
            };
        };
    };
    for (var j = 0; j < gLineas.length; j++) {
        if (gLineas[j].OtrosDatos !== undefined) {
            if (gLineas[j].OtrosDatos.length !== 0) {
                for (var k = 0; k < gLineas[j].OtrosDatos.length; k++) {
                    if (gLineas[j].OtrosDatos[k].Id == Id) {
                        gLineas[j].OtrosDatos[k].Valor = valor;
                        if (gLineas[j].OtrosDatos[k].ListaExterna) {
                            gLineas[j].OtrosDatos[k].Valores = [];
                            gLineas[j].OtrosDatos[k].Valores.push({ 'Valor': valorDen });
                        }
                    }
                }
            }
        };
    };
    gEmisionPedido.Lineas = gLineas;
}
function modificarFavorito() {
    var gEmisionPedidoGuardar = null;
    var gPartidas = [];
    var gCentroCoste = [];

    //Cabecera
    ////DatosGenerales
    var empresa = 0;
    var denempresa = "";
    if ($("#CmbBoxEmpresa").val() !== null) {
        empresa = $("#CmbBoxEmpresa").val();
        denempresa = $("#CmbBoxEmpresa option:selected").text();
    }
    gDatosGenerales.Emp = empresa;
    gDatosGenerales.DenEmp = denempresa;
    gDatosGenerales.Receptor = $("#wddRecept").val();
    gDatosGenerales.Referencia = $("#TxtBxPedSAP").val();
    gDatosGenerales.AcepProve = document.getElementById("chkSolicitAcepProve").checked;

    if ($("#CmbBoxProvesErp").val() !== null) gDatosGenerales.CodErp = $("#CmbBoxProvesErp").val();
    else gDatosGenerales.CodErp = 0;

    if ($("#CmbBoxTipoPedido").val() !== "0")
        gDatosGenerales.Tipo.Id = $("#CmbBoxTipoPedido").val();

    gDatosGenerales.Gestor = $("#LblGestorCab").text();
    gDatosGenerales.Obs = $("#textObser").val();

    if ($("#lblCodDireccion").text() !== "")
        gDatosGenerales.DirEnvio = $("#lblCodDireccion").text();

    gEmisionPedido.DatosGenerales = gDatosGenerales;

    ////Adjuntos ya estan en el objeto
    $.each(gEmisionPedido.Adjuntos, function () {
        if (this.Fecha !== undefined) {
            if (this.Fecha.toString().toLowerCase().indexOf("date") >= 0)
                this.Fecha = eval("new " + this.Fecha.slice(1, -1));
        }
    });
    ////Otros Datos
    if (gEmisionPedido.OtrosDatos !== undefined) guardarOtrosDatos();

    ////Adjuntos ya estan en el objeto
    $.each(gEmisionPedido.Adjuntos, function () {
        if (this.Fecha !== undefined) {
            if (this.Fecha !== null) {
                if (this.Fecha.toString().toLowerCase().indexOf("date") >= 0)
                    this.Fecha = eval("new " + this.Valor.slice(1, -1));
            }
        }
    });
    ////Costes
    if (gCostes !== null) gEmisionPedido.Costes = gCostes;

    ////Descuentos
    if (gDescu !== null) gEmisionPedido.Descuentos = gDescu;

    //guardar las pestaña linea
    if (iCodFilaSeleccionada !== 0) {
        if ($('#pesDatosLineas').hasClass("escenarioSeleccionadoEP")) guardarOtrosDatosLineas();
        if ($('#pesCostesLinea').hasClass("escenarioSeleccionadoEP")) guardarCostesLinea();
        if ($('#pesDescuentosLinea').hasClass("escenarioSeleccionadoEP")) guardarDescuentosLinea();
    }
    $.each(gLineas, function () {
        ////Adjuntos ya estan en el objeto
        $.each(this.Adjuntos, function () {
            if (this.Fecha !== undefined) {
                if (this.Fecha.toString().toLowerCase().indexOf("date") >= 0)
                    this.Fecha = eval("new " + this.Fecha.slice(1, -1));
            }
        });
        if (this.OtrosDatos !== null) {
            $.each(this.OtrosDatos, function () {
                if (this.Tipo == 3) {
                    if (this.Valor !== null) {
                        if (this.Valor.toString().toLowerCase().indexOf("date") >= 0) {
                            this.Valor = eval("new " + this.Valor.slice(1, -1));
                        } else {
                            if (this.Valor.toString() == "") {
                                this.Valor = null;
                            }
                        }
                    }
                }
            });
        };
    });
    var lineasBorrar = $("#lineasBorrar").val().toString();
    if (lineasBorrar !== "") {
        var params = JSON.stringify({ iDesdeFavorito: bModificarFavorito, idCabecera: sID, lineas: lineasBorrar });
        $.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/BorrarLineas',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        });
    }
    $("#lineasBorrar").val("");
    gEmisionPedidoGuardar = jQuery.extend(true, {}, gEmisionPedido);

    var i = 0;
    $.each(gEmisionPedidoGuardar.Lineas, function () {
        gCentroCoste = [];
        gPartidas = [];
        i = i + 1;

        if (this.Pres5_ImportesImputados_EP !== undefined) {
            if (this.Pres5_ImportesImputados_EP[0] !== undefined) {
                if (this.Pres5_ImportesImputados_EP[0].CentroCoste !== undefined) {
                    if (this.Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM.PartidasPresupuestarias !== undefined) {
                        this.Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM.PartidasPresupuestarias = null;
                    }
                    gCentroCoste.push(JSON.parse(JSON.stringify(this.Pres5_ImportesImputados_EP[0].CentroCoste)));
                } else gCentroCoste.push(new Object());

                if (this.Pres5_ImportesImputados_EP[0].Partida !== undefined) {
                    if (this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto !== null) {
                        this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto = null;
                    }
                    if (this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto !== null) {
                        this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto = null;
                    }
                    gPartidas.push(JSON.parse(JSON.stringify(this.Pres5_ImportesImputados_EP[0].Partida)));
                } else
                    gPartidas.push(new Object());
            }
            this.Pres5_ImportesImputados_EP = [];
        } else {
            if (this.Pres5_ImportesImputados_EP[0] !== undefined) {
                if (this.Pres5_ImportesImputados_EP[0].CentroCoste !== undefined) {
                    gCentroCoste.push(JSON.parse(JSON.stringify(this.Pres5_ImportesImputados_EP[0].CentroCoste)));
                    if (this.Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM.PartidasPresupuestarias !== undefined) {
                        this.Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM.PartidasPresupuestarias = null;
                    }
                } else {
                    gCentroCoste.push(new Object());
                }
                if (this.Pres5_ImportesImputados_EP[0].Partida !== undefined) {
                    if (this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto !== null) {
                        this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto = null;
                    }
                    if (this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto !== null) {
                        this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto = null;
                    }
                    gPartidas.push(JSON.parse(JSON.stringify(this.Pres5_ImportesImputados_EP[0].Partida)));
                } else
                    gPartidas.push(new Object());
            }
            this.Pres5_ImportesImputados_EP = [];
        }

        ////Adjuntos ya estan en el objeto
        $.each(this.Adjuntos, function () {
            if (this.Fecha !== undefined) {
                if (this.Fecha.toString().toLowerCase().indexOf("date") >= 0) {
                    this.Fecha = eval("new " + this.Fecha.slice(1, -1));
                }
            }
        });
        if (this.OtrosDatos !== null) {
            $.each(this.OtrosDatos, function () {
                if (this.Tipo == 3) {
                    if (this.Valor !== null) {
                        if (this.Valor.toString().toLowerCase().indexOf("date") >= 0) {
                            this.Valor = eval("new " + this.Valor.slice(1, -1));
                        } else {
                            if (this.Valor.toString() == "")
                                this.Valor = null;
                        }
                    }
                }
            });
        }

        var params = JSON.stringify({ iDesdeFavorito: bModificarFavorito, oLinea: JSON.stringify(this), oCentroCoste: JSON.stringify(gCentroCoste), oPartidas: JSON.stringify(gPartidas), iCestaCabeceraId: gEmisionPedido.DatosGenerales.Id, bHayIntegracion: bHayIntegracionPedidos, idEmpresa: gDatosGenerales.Emp });
        $.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/GuardarLineasPedido',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        });
    });
    var sNombreFavorito = $("#txtNomFavorito").val();
    if (sNombreFavorito !== "") {
        var params = JSON.stringify({ iDesdeFavorito: bModificarFavorito, oEmisionPedido: JSON.stringify(gEmisionPedidoGuardar), bHayIntegracion: bHayIntegracionPedidos, idEmpresa: gDatosGenerales.Emp, sDescFavorito: sNombreFavorito, ModoEdicion: ModoEdicion, EsPedidoAbierto: EsPedidoAbierto });
        $.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/GuardarPedido',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        });
        $.each(gEmisionPedido.Adjuntos, function () {
            this.Operacion = "";
        });
        $.each(gEmisionPedido.Lineas, function () {
            $.each(this.Adjuntos, function () {
                this.Operacion = "";
            });
        });
    } else {
        //Error falta descripcion
        mensaje =
            [{
                valor: " - " + TextosJScript[49]
            }];
        $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
        mostrarError();
    }
    OcultarCargando();
}
function ShowBuscadorObjetoImputacion(id, tipo, denominacion) {
    //tipo 1-cabecera, 2-linea
    $('#popupBuscadorObjetoImputacion span.RotuloGrande').text(TextosJScript[66] + ' ' + denominacion);
    $('#lblCodigoListaExterna').text(TextosJScript[67] + ': ');
    $('#lblDenominacion').text(TextosJScript[70] + ': ');
    $('#btnBuscar').text(TextosJScript[68]);
    $('#btnLimpiar').text(TextosJScript[69]);
    $('#headerCodigo').text(TextosJScript[67]);
    $('#headerDenominacion').text(TextosJScript[70]);
    $('#hidTipo').val(tipo);

    $('#txtCodigo').val('');
    $('#txtDenominacion').val('');
    $('#grResultadosObjetoImputacion').empty();

    $('#popupFondo').css('height', $(document).height());
    $('#popupFondo').show();
    $('#popupBuscadorObjetoImputacion').show();
    CentrarPopUp($('#popupBuscadorObjetoImputacion'));
    idAtributoListaExterna = id;
    atributos = '';
    if (tipo == 1) {
        guardarOtrosDatos(true);
        $.each(gEmisionPedido.OtrosDatos, function () {
            if (atributos == '')
                atributos = this.Id + '@########@' + this.Valor;
            else {
                atributos = atributos + "##########" + this.Id + '@########@' + this.Valor;
            }
        });
    } else {
        guardarOtrosDatosLineas(true);
        $.each(Linea.OtrosDatos, function () {
            if (atributos == '')
                atributos = this.Id + '@########@' + this.Valor;
            else {
                atributos = atributos + "##########" + this.Id + '@########@' + this.Valor;
            }
        });
    }
};
function pres_seleccionado(IDControl, sValor, sTexto, CabeceraDesglose, DesgloseAsociado, OrgCompras) {
    var control = $('#' + IDControl).parent().find('input');
    $(control).val(sTexto);
    var iTipo = IDControl.substr(IDControl.length - 1);
    switch (parseInt(iTipo)) {
        case 1:
            Linea.Pres1 = sValor;
            Linea.Pres1Den = sTexto;
            break;
        case 2:
            Linea.Pres2 = sValor;
            Linea.Pres2Den = sTexto;
            break;
        case 3:
            Linea.Pres3 = sValor;
            Linea.Pres3Den = sTexto;
            break;
        default:
            Linea.Pres4 = sValor;
            Linea.Pres4Den = sTexto;
            break;
    }
}
function DeleteRow(linea) {
    var grid = $find($('[id$=whdgArticulos]').attr('id'));
    var gridView = grid.get_gridView();
    var editada = false;

    if (gridView != null) {
        gLineas = gEmisionPedido.Lineas;
        var index = $.map(gLineas, function (oLinea, idx) { if (oLinea.ID == linea) return idx; })[0];
        var lineasBorrar = $('#lineasBorrar').val().toString();
        if (lineasBorrar !== "") lineasBorrar = lineasBorrar + "," + gLineas[index].ID;
        else lineasBorrar = gLineas[index].ID;
        $("#lineasBorrar").val(lineasBorrar);
        recalcularBorrado(gLineas[index]);
        gLineas.splice(index, 1);
        $('#lblDatosLineaLinea').text("");
        $('#lblCostesLineaLinea').text("");
        $('#lblDescuentosLineaLinea').text("");

        recalcularNumeracion(gLineas, gridView, index);

        //Para hacer el PostBack proveniente del borrado, hace falta pasarle si las líneas han sido editadas o no
        var hid_HaSidoEditada = $([id$ = hid_HaSidoEditada]);
        if (hid_HaSidoEditada.value == undefined) hid_HaSidoEditada.value = 0;
        var ModificadoYaEnBD = false;

        //Si se ha editado anteriormente y se ha guardado en BD, gLineas[i].NumLinModificado = true
        for (var j = 0; j < gLineas.length; j++) {
            if (gLineas[j].NumLinModificado == true) {
                ModificadoYaEnBD = true;
                break;
            }
        }

        if (hid_HaSidoEditada.value != 0 || ModificadoYaEnBD == true) {
            editada = true;
        }

        iFilaSeleccionada = 0;
        iCodFilaSeleccionada = 0;
        if (gEmisionPedido.Lineas.length == 0) $('[id$=]').hide();
        var btn = $('[id$=btnArticuloDeletePostBack]').attr('id');
        __doPostBack(btn, JSON.stringify({ linea: linea, editada: editada }));
    }
}
function recalcularNumeracion(gLineas, gridView, IndexFilaOculta) {
    var hid_HaSidoEditada = $([id$ = hid_HaSidoEditada]);
    var CambiarIndice = false;
    var ModificadoYaEnBD = false;

    //Si se ha editado anteriormente y se ha guardado en BD, gLineas[i].NumLinModificado = true
    for (var j = 0; j < gLineas.length; j++) {
        if (gLineas[j].NumLinModificado == true) {
            ModificadoYaEnBD = true;
            break;
        }
    }
    //Si la numeración no ha sido modificada(ni en el momento de emitir, ni anteriormente habiendo guardado la numeración en algun momento anterior), recalculamos la numeración de la línea
    if (hid_HaSidoEditada.value == undefined) hid_HaSidoEditada.value = 0;
    if (hid_HaSidoEditada.value == 0 && ModificadoYaEnBD == false) {
        switch (true) {
            case gLineas.length < 100: //Numeramos las lineas 10,20,30,40....
                for (var i = 0; i < gLineas.length; i++) {
                    gLineas[i].NumLinea = ((i + 1) * 10);
                }
                break;
            case (gLineas.length >= 100 && gLineas.length < 210): //Numeramos las lineas 1,5,10,15....
                for (var i = 0; i < gLineas.length; i++) {
                    gLineas[i].NumLinea = (i == 0 ? 1 : i * 5);
                }
                break;
            case (gLineas.length >= 210 && gLineas.length < 500): //Numeramos las lineas 1,3,5,7....
                for (var i = 0; i < gLineas.length; i++) {
                    gLineas[i].NumLinea = ((i * 2) + 1);
                }
                break;
            case (gLineas.length >= 500 && gLineas.length < 1000): //Numeramos las lineas 1,2,3,4...
                for (var i = 0; i < gLineas.length; i++) {
                    gLineas[i].NumLinea = (i + 1);
                }
                break;
        }
    }
}
function recalcularBorrado(linea) {
    var impBruto = parseFloat($.trim($('#lblImpBruto_hdd').text()));
    var impLinea = parseFloat($.trim($('#lblImpBrutolin_' + linea.ID + '_hdd').text()));
    var impCostesLinea = parseFloat($.trim($('#lblTotalCostesLinea_hdd').text()));
    var impDescuentosLinea = parseFloat($.trim($('#lblTotalDescuentosLinea_hdd').text()));
    var impCuotasFila = parseFloat($.trim($('#lblTotalDescuentosLinea_hdd').text()));
    var impCostes, impDescuentos;
    var costesLinea = linea.Costes;
    impCostes = 0;
    var precLinea = parseFloat($.trim($('#Prec' + linea.ID + '_hdd').text()));
    $.each(linea.Impuestos, function () {
        if ($('#cuota_' + this.Id).length !== 0) {
            var Impuestos = parseFloat($.trim($('#lblImpTotalCuotas_hdd').text()));
            var ImpuestoCuota = parseFloat($.trim($('#cuota_' + this.Id).text()));

            Impuestos = Impuestos - ImpuestoCuota;

            var ImpuestoAct = this.Valor * precLinea;
            ImpuestoCuota = ImpuestoCuota - ImpuestoAct;
            Impuestos = Impuestos + ImpuestoCuota;

            if (ImpuestoCuota == 0) {
                var id = $(this).parent().get(0).id;
                $('#cuota_' + this.Id).text(0);
                $('#descCuota' + this.Id).remove();
            }
            else {
                $('#cuota_' + this.Id).text(ImpuestoAct.toString());
            }

            $('#lblImpTotalCuotas_hdd').text(Impuestos.toString());
        }
    });
    $.each(costesLinea, function () {
        Importe = 0;
        if (this.Seleccionado !== undefined) {
            if (this.TipoCosteDescuento == 0) {
                if (this.Operacion == "+") {
                    if (this.Valor == undefined) {
                        var Importe = 0;
                    }
                    else {
                        var Importe = this.Valor;
                    }
                }
                else {
                    if (this.Valor == undefined) {
                        var Importe = 0;
                    }
                    else {
                        Importe = impLinea * (this.Valor / 100);
                    }
                }
            }
            else {
                if (this.Seleccionado == true) {
                    if (this.Operacion == "+") {
                        if (this.Valor == undefined) {
                            var Importe = 0;
                        }
                        else {
                            var Importe = this.Valor;
                        }
                    }
                    else {
                        if (this.Valor == undefined) {
                            var Importe = 0;
                        }
                        else {
                            Importe = impLinea * (this.Valor / 100);
                        }
                    }
                }
            }

        }
        impCostes = impCostes + Importe;
    });
    impCostesLinea = impCostesLinea - impCostes;

    impDescuentos = 0;
    var descuentosLinea = linea.Descuentos;
    $.each(descuentosLinea, function () {
        Importe = 0;
        if (this.Seleccionado !== undefined) {
            if (this.TipoCosteDescuento == 0) {
                if (this.Operacion == "-") {
                    if (this.Valor == undefined) {
                        var Importe = 0;
                    }
                    else {
                        var Importe = this.Valor;
                    }
                }
                else {
                    if (this.Valor == undefined) {
                        var Importe = 0;
                    }
                    else {
                        Importe = impLinea * (this.Valor / 100);
                    }
                }
            }
            else {
                if (this.Seleccionado == true) {
                    if (this.Operacion == "-") {
                        if (this.Valor == undefined) {
                            var Importe = 0;
                        }
                        else {
                            var Importe = this.Valor;
                        }
                    }
                    else {
                        if (this.Valor == undefined) {
                            var Importe = 0;
                        }
                        else {
                            Importe = impLinea * (this.Valor / 100);
                        }
                    }
                }
            }

        }
        impDescuentos = impDescuentos + Importe;
    });
    impDescuentosLinea = impDescuentosLinea - impDescuentos;
    impBruto = impBruto - impCostes + impDescuentos - impLinea;
    $('#lblTotalDescuentosLinea_hdd').text(impDescuentosLinea);
    $('#lblTotalCostesLinea_hdd').text(impCostesLinea);
    $('#lblImpBrutolin_' + linea.ID + '_hdd').text(0);
    $('#lblCosteTotalLinea_' + this.ID + '_hdd').text(0);
    $('#lblDescuentoTotalLinea_' + this.ID + '_hdd').text(0);
    recalcularPorcentajesCabeceraPag(impBruto);
    calcularImpuestos();
    $('#pesDatosGenerales').click();

    var coste = parseFloat($.trim($('#lblCosteCabeceraTotal_hdd').text()));
    var descuentos = parseFloat($.trim($('#lblDescuentosCabeceraTotal_hdd').text()));
    var cuotasPedido = parseFloat($.trim($('#lblImpTotalCuotas_hdd').text()));
    var valor = impBruto + coste - descuentos + cuotasPedido;

    $('#lblImpBruto').text(formatNumber(impBruto) + " " + sMoneda);
    $('#lblImpBruto_hdd').text(impBruto);
    $('#lblImpBrutoLinea_hdd').text(impBruto);
    $('#lblImporteTotalCabecera').text(formatNumber(valor) + " " + sMoneda);
    $('#lblImporteTotalCabecera_hdd').text(valor);
}
function recalcularPorcentajesCabeceraPag(importeBruto) {
    var importeCalculado = 0;
    var importeRestar = 0;

    var costesGenerales = parseFloat($('#lblCosteCabeceraTotal_hdd').text());
    gCostes = gEmisionPedido.Costes;
    $.each(gCostes, function () {
        if (this.Seleccionado == undefined) {
            var Seleccionado = false;
            this.Seleccionado = Seleccionado;
        }
        if (this.Operacion == "+") {
            if (this.Valor == undefined) {
                var Importe = 0;
            }
            else {
                var Importe = this.Valor;
            }
        }
        else {
            if (this.Valor == undefined) {
                var Importe = 0;
            }
            else {
                Importe = importeBruto * (this.Valor / 100);
                if (this.Seleccionado == true) {
                    importeRestar = importeRestar + this.Importe;
                    importeCalculado = importeCalculado + Importe;
                }
            }
        }
        this.Importe = Importe;
    });
    costesGenerales = costesGenerales - importeRestar + importeCalculado;
    $('#lblCosteTotal_hdd').text(costesGenerales);
    $('#lblCosteTotal').text(formatNumber(costesGenerales) + " " + sMoneda);
    $('#lblCosteCabeceraTotal_hdd').text(costesGenerales);
    $('#lblCosteCabeceraTotal').text(formatNumber(costesGenerales) + " " + sMoneda);
    gCostes = null;

    importeCalculado = 0;
    importeRestar = 0;
    var descuentosGenerales = parseFloat($('#lblDescuentosCabeceraTotal_hdd').text());
    gDescu = gEmisionPedido.Descuentos;
    $.each(gDescu, function () {
        if (this.Seleccionado == undefined) {
            var Seleccionado = false;
            this.Seleccionado = Seleccionado;
        }
        if (this.Operacion == "-") {
            if (this.Valor == undefined) {
                var Importe = 0;
            }
            else {
                var Importe = this.Valor;
            }
        }
        else {
            if (this.Valor == undefined) {
                var Importe = 0;
            }
            else {

                Importe = importeBruto * (this.Valor / 100);
                if (this.Seleccionado == true) {
                    importeRestar = importeRestar + this.Importe;
                    importeCalculado = importeCalculado + Importe;
                }
            }
        }
        this.Importe = Importe;
    });
    descuentosGenerales = descuentosGenerales - importeRestar + importeCalculado;
    $('#lblDescuentoTotal_hdd').text(descuentosGenerales);
    $('#lblDescuentoTotal').text(formatNumber(descuentosGenerales) + " " + sMoneda);
    $('#lblDescuentosCabeceraTotal_hdd').text(descuentosGenerales);
    $('#lblDescuentosCabeceraTotal').text(formatNumber(descuentosGenerales) + " " + sMoneda);
    gDescu = null;
}
function AbrirDetalleArticulo(IdLinea, Cantidad, Precio, TipoRecepcion, LineaID) {
    var hid_CodArticulo = document.getElementById("<%=hid_CodArticulo.clientID %>");
    var hid_LineaPedido = document.getElementById("<%=hid_lineaPedido.clientID %>");
    var hid_TipoRecepcion = document.getElementById("<%=hid_TipoRecepcion.clientID %>");
    var hid_cantidad = document.getElementById("<%=hid_cantidad.clientID %>");
    var hid_Precio = document.getElementById("<%=hid_Precio.clientID %>");
    if (TipoRecepcion == 1) {
        Cantidad = $('#lblImpBrutolin_' + LineaID.toString()).val();
    } else {
        Precio = $('#Prec' + LineaID.toString()).val();
        Cantidad = $('#Cant' + LineaID.toString()).val();
    }
    var btnOcultoArt = document.getElementById("<%=btnMostrarDetalleArticulo.clientID %>");
    hid_LineaPedido.value = IdLinea;
    hid_cantidad.value = Cantidad;
    hid_Precio.value = Precio;
    hid_TipoRecepcion.value = TipoRecepcion;
    btnOcultoArt.click();
}
function mostrarAdjuntarLin() {
    MostrarFondoPopUp();

    $('#DivPanelAdjuntosLin').css('z-index', 200002);
    CentrarPopUp($('#DivPanelAdjuntosLin'));
    $('#DivPanelAdjuntosLin').show();
    var input = $('#filesLin');
    var clon = input.val('').clone();  // Creamos un clon del elemento original
    input.replaceWith(clon);
    $('#comentarioAdjuntoLin')[0].value = "";
}
function ocultarAdjuntarLin() {
    OcultarFondoPopUp();
    $('#DivPanelAdjuntosLin').hide();
}
function subirArchivoLin() {
    var form_data = new FormData();
    form_data.append("content", $("#filesLin")[0].files[0]);
    $.when($.ajax({
        url: rutaEP + "EPFileTransferHandler.ashx",
        dataType: 'script',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'POST'
    })).done(function (result) {
        var resultado = JSON.parse(result);
        var nuevoAdjunto = {};
        nuevoAdjunto.Id = '-1';
        nuevoAdjunto.Nombre = resultado[0].name;
        nuevoAdjunto.FileName = resultado[0].name;
        nuevoAdjunto.dataSize = resultado[0].filesize;
        nuevoAdjunto.Comentario = $('#comentarioAdjuntoLin')[0].value;
        nuevoAdjunto.url = resultado[0].url;
        nuevoAdjunto.DirectorioGuardar = resultado[0].folder;
        nuevoAdjunto.tipo = resultado[0].tipoadjunto;
        nuevoAdjunto.Cod = '<%=Me.Usuario.Cod %>';
        nuevoAdjunto.Operacion = "I"
        var gLineasArch = gEmisionPedido.Lineas;
        for (var i = 0; i < gLineasArch.length; i++) {
            if (gLineasArch[i].ID == iCodFilaSeleccionada) {
                gLineasArch[i].Adjuntos.push(nuevoAdjunto);
                Linea = gLineasArch[i];
                break;
            }
        }
        gEmisionPedido.Lineas = gLineasArch;

        $('#comentarioAdjuntoLin')[0].innerHTML = "";
        redrawAdjuntosLin();
        ocultarAdjuntarLin();
    });
}
function redrawAdjuntosLin() {
    var gLineasArch = gEmisionPedido.Lineas;
    var gLineaArch;
    for (var i = 0; i < gLineasArch.length; i++) {
        if (gLineasArch[i].ID == iCodFilaSeleccionada) gLineaArch = gLineasArch[i];
    }
    $('#tablaAdjuntosLin').html("");
    $('#filaDocumentoLin').tmpl(gLineaArch.Adjuntos).appendTo($('#tablaAdjuntosLin'));
}
function Descarcargar_ArchivoLin(sIden, sNombre, sTipo, sDataSize, sURL) {
    if (sIden > 0) {
        var link = rutaEP + "EPFileTransferHandler.ashx?f=" + sNombre + "&t=" + sTipo + "&id=" + sIden + "&nombre=" + sNombre + "&ds=" + sDataSize + "&isEP=true&isNew=false";
        window.location = link;
    } else window.location = sURL + "&t=" + sTipo + "&id=" + sIden + "&nombre=" + sNombre + "&ds=" + sDataSize + "&isEP=true&isNew=true";
}
function Eliminar_ArchivoLin(sIden, sNombre, sTipo, sDataSize, sURL) {
    var gLineasArch = gEmisionPedido.Lineas;
    for (var i = 0; i < gLineasArch.length; i++) {
        if (gLineasArch[i].ID == iCodFilaSeleccionada) {
            for (var j = 0; j < gLineasArch[i].Adjuntos.length; j++) {
                if ((gLineasArch[i].Adjuntos[j].Nombre == sNombre) &&
                    (gLineasArch[i].Adjuntos[j].Id == sIden) &&
                    (gLineasArch[i].Adjuntos[j].dataSize == sDataSize))
                    gLineasArch[i].Adjuntos[j].Operacion = "D";
            }
            redrawAdjuntosLin();
            gEmisionPedido.Lineas = gLineasArch;
            break;
        }
    }
}
function mostrarAdjuntar() {
    MostrarFondoPopUp();

    $('#DivPanelAdjuntos').css('z-index', 200002);
    CentrarPopUp($('#DivPanelAdjuntos'));
    $('#DivPanelAdjuntos').show();
    var input = $('#filesCab');
    var clon = input.val('').clone();  // Creamos un clon del elemento original
    input.replaceWith(clon);
    $('#comentarioAdjunto')[0].value = "";
}
function ocultarAdjuntar() {
    OcultarFondoPopUp();
    $('#DivPanelAdjuntos').hide();
}
function subirArchivoCesta() {
    var form_data = new FormData();
    form_data.append("content", $("#filesCab")[0].files[0]);
    $.when($.ajax({
        url: rutaEP + "EPFileTransferHandler.ashx",
        dataType: 'script',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'POST'
    })).done(function (result) {
        var resultado = JSON.parse(result);
        var nuevoAdjunto = {};
        nuevoAdjunto.Id = '-1';
        nuevoAdjunto.Nombre = resultado[0].name;
        nuevoAdjunto.FileName = resultado[0].name;
        nuevoAdjunto.dataSize = resultado[0].filesize;
        nuevoAdjunto.Comentario = $("#comentarioAdjunto")[0].value;
        nuevoAdjunto.url = resultado[0].url;
        nuevoAdjunto.DirectorioGuardar = resultado[0].folder;
        nuevoAdjunto.tipo = resultado[0].tipoadjunto;
        nuevoAdjunto.Cod = '<%=Me.Usuario.Cod %>';
        nuevoAdjunto.Operacion = "I"

        gEmisionPedido.Adjuntos.push(nuevoAdjunto);
        $('#comentarioAdjunto')[0].innerHTML = "";
        redrawAdjuntos();
        ocultarAdjuntar();
    });
}
function redrawAdjuntos() {
    $('#tablaAdjuntos').html("");
    $('#filaDocumento').tmpl(gEmisionPedido.Adjuntos).appendTo($('#tablaAdjuntos'));
}
function Descarcargar_Archivo(sIden, sNombre, sTipo, sDataSize, sURL) {
    if (sIden > 0) {
        var link = rutaEP + "EPFileTransferHandler.ashx?f=" + sNombre + "&t=" + sTipo + "&id=" + sIden + "&nombre=" + sNombre + "&ds=" + sDataSize + "&isEP=true&isNew=false";
        window.location = link;
    } else window.location = sURL + "&t=" + sTipo + "&id=" + sIden + "&nombre=" + sNombre + "&ds=" + sDataSize + "&isEP=true&isNew=true";
}
function Eliminar_Archivo(sIden, sNombre, sTipo, sDataSize, sURL) {
    for (var i = 0; i < gEmisionPedido.Adjuntos.length; i++) {
        if ((gEmisionPedido.Adjuntos[i].Nombre == sNombre) &&
            (gEmisionPedido.Adjuntos[i].Id == sIden) &&
            (gEmisionPedido.Adjuntos[i].dataSize == sDataSize))
            gEmisionPedido.Adjuntos[i].Operacion = "D";
    }
    redrawAdjuntos();
}
function CargarAtributosCabeceraTipoPedido() {
    var comboVal = $("#CmbBoxTipoPedido").val().toString();
    var empresa = 0;
    if ($("#CmbBoxEmpresa").val() !== null) { empresa = $("#CmbBoxEmpresa").val(); }
    for (var i = gEmisionPedido.OtrosDatos.length - 1; i >= 0; i--) {
        if (gEmisionPedido.OtrosDatos[i].TipoPed == true) {
            gEmisionPedido.OtrosDatos.splice(i, 1);
        }
    }
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarAtributosCabeceraTipoPedido',
        contentType: "application/json; charset=utf-8",
        data: "{'iCodPedido':'" + sID + "','sIDTipoPedido':'" + comboVal.toString() + "', 'bHayIntegracion':'" + bHayIntegracionPedidos + "', 'iEmpresa':'" + empresa + "'}",
        dataType: "json",
        async: false
    })).done(function (msg) {
        var atributosTipoPedido = $.parseJSON(msg.d);
        if (atributosTipoPedido.length !== 0) {
            for (var j = 0; j < atributosTipoPedido.length; j++) {
                var bInsertaAtrib = true;
                for (var z = 0; z < gEmisionPedido.OtrosDatos.length; z++) {
                    if (gEmisionPedido.OtrosDatos[z].Id == atributosTipoPedido[j].Id) {
                        bInsertaAtrib = false;
                        //Ya estaba, así que no lo vuelvo a guardar en el objeto pero le digo que es de tipopedido
                        gEmisionPedido.OtrosDatos[z].TipoPed = true;
                        break;
                    }
                }
                if (bInsertaAtrib) gEmisionPedido.OtrosDatos.push(atributosTipoPedido[j]);
            }
            gAtributos = gEmisionPedido.OtrosDatos;
        }
    });
}
function CargarAtributosLineasTipoPedido() {
    var comboVal = $("#CmbBoxTipoPedido").val().toString();
    gLineas = gEmisionPedido.Lineas;
    var empresa = 0;
    if ($("#CmbBoxEmpresa").val() !== null) { empresa = $("#CmbBoxEmpresa").val(); }
    for (var j = 0; j < gLineas.length; j++) {
        for (var i = gLineas[j].OtrosDatos.length - 1; i >= 0; i--) {
            if (gLineas[j].OtrosDatos[i].TipoPed == true) {
                gLineas[j].OtrosDatos.splice(i, 1);
            }
        }
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarAtributosIntegracionLinea',
            contentType: "application/json; charset=utf-8",
            data: "{'iDesdeFavorito':'" + bModificarFavorito + "','sIDTipoPedido':'" + comboVal.toString() + "', 'iCodLinea':'" + gLineas[j].ID.toString() + "', 'bHayIntegracion':'" + bHayIntegracionPedidos + "', 'iEmpresa':'" + empresa + "'}",
            dataType: "json",
            async: false
        })).done(function (msg) {
            var atributosTipoPedido = $.parseJSON(msg.d);
            if (atributosTipoPedido.length !== 0) {
                for (var k = 0; k < atributosTipoPedido.length; k++) {
                    var bInsertaAtrib = true;
                    if (atributosTipoPedido[k].TipoPed) {
                        for (var z = 0; z < gLineas[j].OtrosDatos.length; z++) {
                            if (gLineas[j].OtrosDatos[z].Id == atributosTipoPedido[k].Id) {
                                bInsertaAtrib = false;
                                //Ya estaba así que no lo vuelvo a guardar en el objeto pero le digo que es de TipoPedido
                                gLineas[j].OtrosDatos[z].TipoPed = true;
                                gLineas[j].OtrosDatos[z].Integracion = atributosTipoPedido[k].TipoPed;
                                break;
                            }
                        }
                        if (bInsertaAtrib) {
                            gLineas[j].OtrosDatos.push(atributosTipoPedido[k]);
                        }
                    }

                }
            }
        });
    }
    gEmisionPedido.Lineas = gLineas;
}
//PLANES DE ENTREGA
$('[name=fecPlanEntrega]').live('click', function () {
    Linea.ConPlanEntrega = !($('#rbFechaEntrega').is(':checked'));
    GestionarChecksFechaPlanEntrega($('#rbFechaEntrega').is(':checked'));
});
$('#tblPlanesEntrega tbody tr').live('click', function () {
    $(this).toggleClass('ItemParaGrupoSelected');
    $('img[name=imgSelectedPlanEntrega]', $(this)).toggle();
});
$('#btnAgregarPlanEntrega').live('click', function () {
    $('#trPlanEntrega').tmpl().appendTo('#tblPlanesEntrega tbody');
    $('#tblPlanesEntrega tbody tr:last img[name=imgSelectedPlanEntrega]').attr('src', rutaTheme + '/images/row_collapsed.gif');
    $('#tblPlanesEntrega tbody tr:last [name=txtFechaPlanEntrega]').inputmask(UsuMask.replace('MM', 'mm')).datepicker({
        showOn: 'both',
        buttonImage: ruta + 'images/colorcalendar.png',
        buttonImageOnly: true,
        buttonText: '',
        dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
        showAnim: 'slideDown'
    });
    $('#tblPlanesEntrega tbody tr:last [name=txtCantidadImportePlanEntrega]').numeric({ decimal: '' + usuario.DecimalFmt + '', negative: false }).numericFormatted({
        decimalSeparator: UsuNumberDecimalSeparator,
        groupSeparator: UsuNumberGroupSeparator,
        decimalDigits: UsuNumberNumDecimals
    });
    Linea.PlanEntrega.push({ FechaEntrega: '', CantidadPlanEntrega: '', ImportePlanEntrega: '' });
    if (gLineas.length > 1) $('#btnCopiarPlanEntrega').show();
    else $('#btnCopiarPlanEntrega').hide();
});
$('#btnEliminarPlanEntrega').live('click', function () {
    $('#tblPlanesEntrega tbody tr.ItemParaGrupoSelected').remove();
    Linea.PlanEntrega = [];
    $.each($('#tblPlanesEntrega tbody tr'), function () {
        Linea.PlanEntrega.push({
            IdPlanEntrega: ($(this).attr('name') == '' ? 0 : $(this).attr('name')),
            FechaEntrega: ($(this).find('[name=txtFechaPlanEntrega]').datepicker('getDate') == null ? '' : new Date($(this).find('[name=txtFechaPlanEntrega]').datepicker('getDate'))),
            CantidadEntrega: Obtener_Float_Texto(Linea.TipoRecepcion == 0 ? $(this).find('[name=txtCantidadImportePlanEntrega]').val() : '0'),
            ImporteEntrega: Obtener_Float_Texto(Linea.TipoRecepcion == 1 ? $(this).find('[name=txtCantidadImportePlanEntrega]').val() : '0')
        });
    });
    if ($('#tblPlanesEntrega tbody tr').length == 0) $('#btnCopiarPlanEntrega').hide();
});
$('[name=txtFechaPlanEntrega],[name=txtCantidadImportePlanEntrega]').live('click', function (event) {
    event.stopPropagation();
});
$('[name=txtFechaPlanEntrega],[name=txtCantidadImportePlanEntrega]').live('change', function (event) {
    GenerarPlanEntrega();
});
$('#chkRecepcionAutomatica').live('click', function () {
    Linea.ConRecepcionAutomatica = $(this).prop('checked');
});
function GenerarPlanEntrega() {
    var planEntrega = [];
    var sumCantidadImporte = 0;
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    $.each($('#tblPlanesEntrega tbody tr'), function () {
        planEntrega.push({
            IdPlanEntrega: ($(this).attr('name') == '' ? 0 : $(this).attr('name')),
            FechaEntrega: ($(this).find('[name=txtFechaPlanEntrega]').datepicker('getDate') == null ? '' : new Date($(this).find('[name=txtFechaPlanEntrega]').datepicker('getDate'))),
            CantidadEntrega: Obtener_Float_Texto(Linea.TipoRecepcion == 0 ? $(this).find('[name=txtCantidadImportePlanEntrega]').val() : '0'),
            ImporteEntrega: Obtener_Float_Texto(Linea.TipoRecepcion == 1 ? $(this).find('[name=txtCantidadImportePlanEntrega]').val() : '0')
        });
        if (new Date(planEntrega.FechaEntrega) < today) alert(TextosJScript[101]);
        sumCantidadImporte += Obtener_Float_Texto(($(this).find('[name=txtCantidadImportePlanEntrega]').val() == '' ? '0' : $(this).find('[name=txtCantidadImportePlanEntrega]').val()));
    });
    if (Linea.TipoRecepcion == 0 && Linea.CantPed < sumCantidadImporte) alert(TextosJScript[102]);
    if (Linea.TipoRecepcion == 1 && Linea.ImportePedido < sumCantidadImporte) alert(TextosJScript[103]);
    Linea.PlanEntrega = $.parseJSON(JSON.stringify(planEntrega));
    var rowIndex = $.map(gLineas, function (x, index) { if (x.ID == Linea.ID) return index });
    gLineas[rowIndex].PlanEntrega = $.parseJSON(JSON.stringify(planEntrega));
};
function GestionarChecksFechaPlanEntrega(esFechaEntrega) {
    if (esFechaEntrega) {
        $('#LblFecEntSol').next('div').show();
        $('#pesPlanesEntrega').hide();
    } else {
        $('#LblFecEntSol').next('div').hide();
        $('#pesPlanesEntrega').show();

        var lineaPlanEntrega = $.grep(gLineas, function (x) { return (x.ID == iCodFilaSeleccionada) })[0];
        $('#tblPlanesEntrega tbody').empty();
        if (Linea.PlanEntrega.length > 0) {
            $('#trPlanEntrega').tmpl(lineaPlanEntrega.PlanEntrega).appendTo('#tblPlanesEntrega tbody');
            $('[name=imgSelectedPlanEntrega]').attr('src', rutaTheme + '/images/row_collapsed.gif');
            $('[name=txtFechaPlanEntrega]').inputmask(UsuMask.replace('MM', 'mm')).datepicker({
                showOn: 'both',
                buttonImage: ruta + 'images/colorcalendar.png',
                buttonImageOnly: true,
                buttonText: '',
                dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
                showAnim: 'slideDown'
            });
            $('[name=txtCantidadImportePlanEntrega]').numeric({ decimal: '' + usuario.DecimalFmt + '', negative: false }).numericFormatted({
                decimalSeparator: UsuNumberDecimalSeparator,
                groupSeparator: UsuNumberGroupSeparator,
                decimalDigits: UsuNumberNumDecimals
            });
            $('#lblCabeceraPlanEntregaCantidadImporteEntrega').text((lineaPlanEntrega.TipoRecepcion == 0 ? TextosJScript[99] : TextosJScript[100]) + '(' + lineaPlanEntrega.UP + ')');
            var trPlanEntrega;
            $.each(lineaPlanEntrega.PlanEntrega, function (index) {
                trPlanEntrega = $('#tblPlanesEntrega tbody tr:nth-child(' + (index + 1) + ')');
                $(trPlanEntrega).find('[name=txtFechaPlanEntrega]').datepicker('setDate', new Date(this.FechaEntrega));
                if (lineaPlanEntrega.TipoRecepcion == 0) //Cantidad
                    $(trPlanEntrega).find('[name=txtCantidadImportePlanEntrega]').numericFormatted('val', this.CantidadEntrega);
                else //Importe
                    $(trPlanEntrega).find('[name=txtCantidadImportePlanEntrega]').numericFormatted('val', this.ImporteEntrega);
            });
            if (gDatosGenerales.TipoRecepcionAutomatica == 1) $('#chkRecepcionAutomatica').attr("disabled", true);
            if ($('#tblPlanesEntrega tbody tr').length == 0 || gLineas.length <= 1) $('#btnCopiarPlanEntrega').hide();
            else $('#btnCopiarPlanEntrega').show();
        }
    }
}
function Obtener_Float_Texto(texto) {
    return parseFloat(texto.replace(usuario.ThousanFmt, '').replace(',', '.'));
}
$('#btnCopiarPlanEntrega').live('click', function () {
    GenerarPlanEntrega();
    if (ValidarPlanEntrega(Linea.TipoRecepcion, Linea.PlanEntrega, Linea.CantPed, Linea.ImportePedido, ("000" + Linea.NumLinea).slice(-3))) {
        if (confirm(TextosJScript[104])) {
            var porcentajesPlanEntregaSeleccionado = [];
            var ultimoPorcentaje = 0;
            var porcentaje;
            var lineaPlanEntrega;
            var planEntregaTrasladado;
            $.each(Linea.PlanEntrega, function () {
                porcentaje = (Linea.TipoRecepcion == 0 ? Math.floor((this.CantidadEntrega / Linea.CantPed) * 100) : Math.floor((this.ImporteEntrega / Linea.ImportePedido) * 100));
                ultimoPorcentaje = porcentaje;
                porcentajesPlanEntregaSeleccionado.push(porcentaje);
            });
            var planEntregaTotal = $.map(Linea.PlanEntrega, function (x) { if (Linea.TipoRecepcion == 0) return x.CantidadEntrega; else return x.ImporteEntrega }).reduce(function (x, y) { return x + y });
            var esPlanEntregaTotal = ((Linea.TipoRecepcion == 0 && Linea.CantPed == planEntregaTotal) || (Linea.TipoRecepcion != 0 && Linea.ImportePedido == planEntregaTotal));

            if (esPlanEntregaTotal)//Si el plan de entrega a copiar esta establecido al 100% la ultima linea llevara el resto hasta el 100%
                porcentajesPlanEntregaSeleccionado[porcentajesPlanEntregaSeleccionado.length - 1] += 100 - porcentajesPlanEntregaSeleccionado.reduce(function (x, y) { return x + y });
            var lineasError = [];
            var cantidadImportePlan = 0;
            $.each(gLineas, function () {
                lineaPlanEntrega = this;
                planEntregaTrasladado = [];
                if (lineaPlanEntrega.ID !== Linea.ID) {
                    $.each(porcentajesPlanEntregaSeleccionado, function (index) {
                        if (esPlanEntregaTotal && index == porcentajesPlanEntregaSeleccionado.length - 1)
                            cantidadImportePlan = ((lineaPlanEntrega.TipoRecepcion == 0 ? lineaPlanEntrega.CantPed : lineaPlanEntrega.ImportePedido) - $.map(planEntregaTrasladado, function (x) { if (lineaPlanEntrega.TipoRecepcion == 0) return x.CantidadEntrega; else return x.ImporteEntrega }).reduce(function (x, y) { return x + y })).toString();
                        else
                            cantidadImportePlan = (lineaPlanEntrega.TipoRecepcion == 0 ? Math.floor(lineaPlanEntrega.CantPed * (porcentajesPlanEntregaSeleccionado[index] / 100)).toString() : Math.floor(lineaPlanEntrega.ImportePedido * (porcentajesPlanEntregaSeleccionado[index] / 100)).toString());

                        planEntregaTrasladado.push({
                            IdPlanEntrega: 0,
                            FechaEntrega: Linea.PlanEntrega[index].FechaEntrega,
                            CantidadEntrega: Obtener_Float_Texto(lineaPlanEntrega.TipoRecepcion == 0 ? cantidadImportePlan : '0'),
                            ImporteEntrega: Obtener_Float_Texto(lineaPlanEntrega.TipoRecepcion == 1 ? cantidadImportePlan : '0')
                        });
                        if (cantidadImportePlan == 0 || planEntregaTrasladado[planEntregaTrasladado.length - 1].FechaEntrega < new Date()) {
                            lineasError.push(lineaPlanEntrega.NumLinea);
                            return false;
                        };
                    });
                    if ($.inArray(lineaPlanEntrega.NumLinea, lineasError) == -1) {
                        if ((Linea.TipoRecepcion == 0 && Linea.CantPed == planEntregaTotal) || (Linea.TipoRecepcion != 0 && Linea.ImportePedido == planEntregaTotal)) {
                            if (lineaPlanEntrega.TipoRecepcion == 0)
                                planEntregaTrasladado[planEntregaTrasladado.length - 1].CantidadEntrega += lineaPlanEntrega.CantPed - planEntregaTrasladado.reduce(function (x, y) { return { CantidadEntrega: (x.CantidadEntrega + y.CantidadEntrega) } }).CantidadEntrega;
                            else
                                planEntregaTrasladado[planEntregaTrasladado.length - 1].ImporteEntrega += lineaPlanEntrega.ImportePedido - planEntregaTrasladado.reduce(function (x, y) { return { ImporteEntrega: (x.ImporteEntrega + y.ImporteEntrega) } }).ImporteEntrega;
                        }
                        lineaPlanEntrega.PlanEntrega = $.parseJSON(JSON.stringify(planEntregaTrasladado));
                        lineaPlanEntrega.ConPlanEntrega = true;
                    };
                };
            });
            if (lineasError.length > 0) alert(TextosJScript[105] + ': ' + $.map(lineasError, function (x) { return ("000" + x.toString()).slice(-3) }).join(','));
        }
    };
});
function ValidarPlanEntrega(tipoRecepcion, planEntrega, CantidadTotal, ImporteTotal, numLinea) {
    var valido = true;
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    var sumCantidadImporte = 0;
    $.each(planEntrega, function () {
        if ((this.FechaEntrega !== '' && (tipoRecepcion == 0 && (this.CantidadEntrega == null || this.CantidadEntrega == '')))
            || (this.FechaEntrega == '' && (tipoRecepcion == 0 && (this.CantidadEntrega !== null || this.CantidadEntrega !== '')))) {
            alert(TextosJScript[51] + " " + numLinea + '. ' + TextosJScript[109]);
            valido = false;
            return false;
        };
        if ((this.FechaEntrega !== '' && (tipoRecepcion == 1 && (this.ImporteEntrega == null || this.ImporteEntrega == '')))
            || (this.FechaEntrega == '' && (tipoRecepcion == 1 && (this.ImporteEntrega !== null || this.ImporteEntrega !== '')))) {
            alert(TextosJScript[51] + " " + numLinea + '. ' + TextosJScript[110]);
            valido = false;
            return false;
        };
        if (new Date(this.FechaEntrega) < today) {
            alert(TextosJScript[51] + " " + numLinea + '. ' + TextosJScript[101]);
            valido = false;
            return false;
        };
        sumCantidadImporte += ((tipoRecepcion == 0 ? this.CantidadEntrega : this.ImporteEntrega));
    });
    if (tipoRecepcion == 0 && CantidadTotal < sumCantidadImporte) { alert(TextosJScript[51] + " " + numLinea + '. ' + TextosJScript[102]); valido = false; };
    if (tipoRecepcion == 1 && ImporteTotal < sumCantidadImporte) { alert(TextosJScript[51] + " " + numLinea + '. ' + TextosJScript[103]); valido = false; };

    return valido;
};