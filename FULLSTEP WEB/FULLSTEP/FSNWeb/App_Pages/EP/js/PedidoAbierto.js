﻿var cargado = false;
var _anioSeleccionadoInicio, _anioSeleccionado, _numPedidoSeleccionado, _numOrdenSeleccionado, _idEmpresaSeleccionado, _codArticuloSeleccionado, _fecInicioSeleccionado, _fecFinSeleccionado, _codProveSeleccionado;
var seleccionarPrimerRegistro = true;
var FSNPeticionesCesta = new Array();
$(document).ready(function () {
    MostrarCargando();
    //plugin de datepicker para las cajas desde/hasta de las fechas
    $('#txtBscFecInicioDesde,#txtBscFecInicioHasta,#txtBscFecFinDesde,#txtBscFecFinHasta').datepicker({
        showOn: 'both',
        buttonImage: ruta + 'images/colorcalendar.png',
        buttonImageOnly: true,
        buttonText: '',
        dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
        showAnim: 'slideDown',
        onClose: function (dateText) {
            switch ($(this).attr('id')) {
                case 'txtBscFecInicioDesde':
                case 'txtBscFecInicioHasta':
                    Gestionar_Filtro_FechaInicio();
                    break;
                default:
                    Gestionar_Filtro_FechaFin();
                    break;
            }
            RecargarGrid();
        }
    });
    $.datepicker.setDefaults($.datepicker.regional[usuario.RefCultural]);

    $('#lstBscProveedores').css('width', $('#lstBscProveedores').parent('li').width() - $('#lstBscProveedores').siblings('div').outerWidth() - 30);
    _anioSeleccionado = (typeof (_anioSeleccionadoInicio) == 'undefined' ? $('#cboBscAnioPedidoAbierto').val() : _anioSeleccionadoInicio);
    _numPedidoSeleccionado = 0;
    _numOrdenSeleccionado = 0;
    _idEmpresaSeleccionado = 0;
    _fecInicioDesde = '';
    _fecInicioHasta = '';
    _fecFinDesde = '';
    _fecFinHasta = '';
    _codProveSeleccionado = '';
    $('#faAnio span:last').text((typeof (_anioSeleccionadoInicio) == 'undefined' ? $('#cboBscAnioPedidoAbierto').val() : _anioSeleccionadoInicio));
    delete _anioSeleccionadoInicio;
    Cargar_TextosPantalla();
    Cargar_FiltrosAvanzados_PedidosAbiertos();
});
function OcultarCargando() {
    var modalprog = $find(ModalProgress);
    if (modalprog) modalprog.hide();
};
function Cargar_TextosPantalla() {
    $('#lblBscPedidoAbierto').text(TextosPantalla[0] + ':');
    $('#lblBscEmpresa').text(TextosPantalla[1] + ':');
    $('#lblBscArticulo').text(TextosPantalla[2] + ':');
    $('#lblBscFecInicioDesde').text(TextosPantalla[3] + ' ' + TextosPantalla[5] + ':');
    $('#lblBscFecInicioHasta').text(TextosPantalla[6] + ':');
    $('#lblBscFecFinDesde').text(TextosPantalla[4] + ' ' + TextosPantalla[5] + ':');
    $('#lblBscFecFinHasta').text(TextosPantalla[6] + ':');
    $('#lblBscProveedores').text(TextosPantalla[7] + ':');
    $('#lblBscFiltros').text(TextosPantalla[8] + ':');
    $('#faAnio span:first').text(TextosPantalla[16] + ':');
    $('#faNumPedido span:first').text(TextosPantalla[17] + ':');
    $('#faNumOrden span:first').text(TextosPantalla[18] + ':');
    $('#faEmpresa span:first').text(TextosPantalla[1] + ':');
    $('#faArticulo span:first').text(TextosPantalla[2] + ':');
    $('#faFecInicio span:first').text(TextosPantalla[3]);
    $('#faFecFin span:first').text(TextosPantalla[4]);
    $('#faBorrarFiltros a').text(TextosPantalla[19] + ' (x)');
    $('#lblFiltrosAvanzados').text(TextosPantalla[35]);
    $('#lstBscProveedores').siblings('a').text(TextosPantalla[36]);
};
//funciones que sirven para ordenar arrays de objetos
function ordenAscendenteEnteros(a, b) {
    return a - b;
};
function ordenAscendenteEmpresasDen(a, b) {
    return a.denEmpresa < b.denEmpresa ? -1 : 1;
};
function ordenDescendenteProveedoresNumPedidosAbiertos(a, b) {
    return b.numPedidosAbiertos - a.numPedidosAbiertos;
};
//Que hacer cuando se cambian los valores de las fechas y mostrar lo filtrado en la barra de filtro
function Gestionar_Filtro_FechaInicio() {
    if ($('#txtBscFecInicioDesde').datepicker('getDate') == null && $('#txtBscFecInicioHasta').datepicker('getDate') == null) $('#faFecInicio').hide();
    else {
        $('#faFecInicio').show();
        if ($('#txtBscFecInicioDesde').datepicker('getDate') == null) $('#faFecInicio div:first').children().hide();
        else {
            $('#faFecInicio div:first span').text(TextosPantalla[5] + ' ' + $('#txtBscFecInicioDesde').val());
            $('#faFecInicio div:first').children().show();
        };
        if ($('#txtBscFecInicioHasta').datepicker('getDate') == null) $('#faFecInicio div:last').children().hide();
        else {
            $('#faFecInicio div:last span').text(TextosPantalla[6] + ' ' + $('#txtBscFecInicioHasta').val());
            $('#faFecInicio div:last').children().show();
        };
    };        
};
function Gestionar_Filtro_FechaFin() {
    if ($('#txtBscFecFinDesde').datepicker('getDate') == null && $('#txtBscFecFinHasta').datepicker('getDate') == null) $('#faFecInicio').hide();
    else {
        $('#faFecFin').show();
        if ($('#txtBscFecFinDesde').datepicker('getDate') == null) $('#faFecFin div:first').children().hide();
        else {
            $('#faFecFin div:first span').text(TextosPantalla[5] + ' ' + $('#txtBscFecFinDesde').val());
            $('#faFecFin div:first').children().show();
        };
        if ($('#txtBscFecInicioHasta').datepicker('getDate') == null) $('#faFecFin div:last').children().hide();
        else {
            $('#faFecFin div:last span').text(TextosPantalla[6] + ' ' + $('#txtBscFecFinHasta').val());
            $('#faFecFin div:last').children().show();
        };
    };
};
//Seleccionar la primera linea del grid y mostrar sus lineas
function Cargar_PrimerRegistroGrid() {
    var grid = $find($('[id$=whdgPedidosAbiertos]').attr('id')).get_gridView();
    grid.get_behaviors().get_selection().get_selectedRows().clear();
    if (grid.get_rows().get_length() > 0) {
        grid.get_behaviors().get_selection().get_selectedRows().add(grid.get_rows().get_row(0));
        Cargar_Lineas_PedidoAbierto(grid.get_rows().get_row(0).get_cellByColumnKey("ORDENENTREGA").get_value());
    };
}
//Cargar las lineas del pedido abierto seleccionado
function Cargar_Lineas_PedidoAbierto(ordenEntrega) {
    //Llamamos al page_method para obtener las lineas del pedido abierto
    var params = { Orden: ordenEntrega };
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'EP/PedidoAbierto.aspx/Obtener_Lineas_PedidoAbierto',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(params),
        async: true
    })).done(function (msg) {
        var lineasPedidoAbierto = $.parseJSON(msg.d);
        $('#lstLineasPedidoAbierto').empty();
        $('#tmplLineaPedidoAbierto').tmpl(lineasPedidoAbierto).appendTo($('#lstLineasPedidoAbierto'));
        $('#lstLineasPedidoAbierto input').numeric({ decimal: '' + usuario.DecimalFmt + '', negative: false }).numericFormatted({
            decimalSeparator: UsuNumberDecimalSeparator,
            groupSeparator: UsuNumberGroupSeparator,
            decimalDigits: UsuNumberNumDecimals
        }, $('#lstLineasPedidoAbierto input').val('0' + UsuNumberDecimalSeparator + '00000000'.substring(0, UsuNumberNumDecimals)));
        if (cargado) {
            OcultarCargando();
            cargado = false;
        } else cargado = true;
    });
}
function RecargarGrid() {
    MostrarCargando();
    $('#lstLineasPedidoAbierto').empty();
    seleccionarPrimerRegistro = true;
    var btnFiltrarPedAbierto = $('[id$=btnFiltrarPedAbierto]').attr('id');
    var sParameters = {};
    sParameters.Anio = $('#cboBscAnioPedidoAbierto').val();
    sParameters.NumPedido = ($('#txtBscNumPedidoPedidoAbierto').val() == '' ? 0 : $('#txtBscNumPedidoPedidoAbierto').val());
    sParameters.NumOrden = ($('#txtBscNumOrdenPedidoAbierto').val() == '' ? 0 : $('#txtBscNumOrdenPedidoAbierto').val());
    sParameters.IdEmpresa = $('#cboBscEmpresa').val();
    sParameters.CodArticulo = ($('#txtBscArticulo').tokenInput('get').length == 0 ? '' : $('#txtBscArticulo').tokenInput('get')[0].id);
    sParameters.FecInicioDesde = $('#txtBscFecInicioDesde').datepicker('getDate');
    sParameters.FecInicioHasta = $('#txtBscFecInicioHasta').datepicker('getDate');
    sParameters.FecFinDesde = $('#txtBscFecFinDesde').datepicker('getDate');
    sParameters.FecFinHasta = $('#txtBscFecFinHasta').datepicker('getDate');
    sParameters.CodProve = $('#faProveedor span span').first().text();
    __doPostBack(btnFiltrarPedAbierto, JSON.stringify(sParameters));
};
//#region Filtros avanzados
$('#lnkFiltrosAvanzados').live('click', function () {
    $('#pnlFiltrosAvanzados li').toggle();
    $('[id$=imgExpandFiltrosAvanzados]').toggle();
    $('[id$=imgCollapseFiltrosAvanzados]').toggle();    
});
$('[id$=imgBscExpandProveedores],[id$=imgBscCollapseProveedores]').live('click', function () {
    $('[id$=imgBscExpandProveedores]').toggle();
    $('[id$=imgBscCollapseProveedores]').toggle();
    $('#lstBscProveedores').toggle();
});
$('#cboBscAnioPedidoAbierto').live('change', function () {
    if ($('#cboBscAnioPedidoAbierto').val() == 0) $('#faAnio').hide();
    else {
        $('#faAnio').show();
        $('#faAnio span:last').text($('#cboBscAnioPedidoAbierto').val());
    };
    _anioSeleccionado = $('#cboBscAnioPedidoAbierto').val();
    RecargarGrid();
});
/*
En el evento de teclear para los numeros de pedido y orden, controlamos que no pueda meter algo no numerico.
Ademas, si da a las teclas de borrar, delete y return borramos o buscamos por lo seleccionado.
*/
$('#txtBscNumPedidoPedidoAbierto,txtBscNumOrdenPedidoAbierto').live(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
    var code;
    if (event.keyCode) code = event.keyCode;
    else if (event.which) code = event.which;
    switch (code) {
        case KEY.BACKSPACE:
        case KEY.DEL:
            break;
        case KEY.RETURN:
            $('#cboBscAnioPedidoAbierto').focus();
            break;
        default:
            var anterior = $(this).val();
            var txt = $(this);
            setTimeout(function () {
                if (isNaN(txt.val())) {
                    txt.val(anterior);
                    return false;
                }
            }, 5);
            break;
    }
});
//Mostrar en la barra de filtro las opciones seleccionadas
$('#txtBscNumPedidoPedidoAbierto').live('change', function () {
    if ($('#txtBscNumPedidoPedidoAbierto').val() == 0) $('#faNumPedido').hide();
    else {
        $('#faNumPedido').show();
        $('#faNumPedido span:last').text($('#txtBscNumPedidoPedidoAbierto').val());
    };
    _numPedidoSeleccionado = $('#txtBscNumPedidoPedidoAbierto').val();
    RecargarGrid();
});
$('#txtBscNumOrdenPedidoAbierto').live('change', function () {
    if ($('#txtBscNumOrdenPedidoAbierto').val() == 0) $('#faNumOrden').hide();
    else {
        $('#faNumOrden').show();
        $('#faNumOrden span:last').text($('#txtBscNumOrdenPedidoAbierto').val());
    };
    _numOrdenSeleccionado = $('#txtBscNumOrdenPedidoAbierto').val();
    RecargarGrid();
});
$('#cboBscEmpresa').live('change', function () {
    if ($('#cboBscEmpresa').val() == 0) $('#faEmpresa').hide();
    else {
        $('#faEmpresa').show();
        $('#faEmpresa span:last').text($('#cboBscEmpresa option:selected').text());
    };
    _idEmpresaSeleccionado = $('#cboBscEmpresa').val();
    RecargarGrid();
});
$('#lstBscProveedores li').live('click', function () {
    _codProveSeleccionado = $(this).find('span:first').text();
    $('#tmplItemProveedor').tmpl({ codProve: $(this).find('span:first').text(), denProve: $(this).find('a:nth-of-type(2)').text() }).appendTo($('#faProveedor'));
    RecargarGrid();
});
//Al hacer click en el aspa de la barra de filtro, borramos el filtrado y borramos el valor en la seccion de opciones de filtrado
$('#faAnio a').live('click', function () {
    if (datosFiltrosAvanzados == null) {
        $('#faAnio').hide();
        return false;
    }
    if (datosFiltrosAvanzados.ComboAnios.length == 1) return false;
    _anioSeleccionado = 0;
    $('#faAnio').hide();
    $('#cboBscAnioPedidoAbierto').val(0);
    RecargarGrid();
});
$('#faNumPedido a').live('click', function () {
    _numPedidoSeleccionado = 0;
    $('#faNumPedido').hide();
    RecargarGrid();
});
$('#faNumOrden a').live('click', function () {
    _numOrdenSeleccionado = 0;
    $('#faNumOrden').hide();
    RecargarGrid();
});
$('#faEmpresa a').live('click', function () {
    _idEmpresaSeleccionado = 0;
    $('#faEmpresa').hide();
    $('#cboBscEmpresa').val(0);
    RecargarGrid();
});
$('#faArticulo a').live('click', function () {
    _codArticuloSeleccionado = '';
    $('#faArticulo').empty();
    $('#txtBscArticulo').tokenInput("clear");
    Cargar_Proveedores_FiltrosAvanzados()
    RecargarGrid();
});
$('#faFecInicio div:first a').live('click', function () {
    $('#txtBscFecInicioDesde').datepicker('setDate', null);
    Gestionar_Filtro_FechaInicio();
    RecargarGrid();
});
$('#faFecInicio div:last a').live('click', function () {
    $('#txtBscFecInicioHasta').datepicker('setDate', null);
    Gestionar_Filtro_FechaInicio();
    RecargarGrid();
});
$('#faFecFin div:first a').live('click', function () {
    $('#txtBscFecFinDesde').datepicker('setDate', null);
    Gestionar_Filtro_FechaFin();
    RecargarGrid();
});
$('#faFecFin div:last a').live('click', function () {
    $('#txtBscFecFinHasta').datepicker('setDate', null);
    Gestionar_Filtro_FechaFin();
    RecargarGrid();
});
$('#faProveedor a').live('click', function () {
    _codProveSeleccionado = '';
    $('#faProveedor').empty();
    Cargar_Proveedores_FiltrosAvanzados();
    RecargarGrid();
});
$('#faBorrarFiltros a').live('click', function () {
    if (datosFiltrosAvanzados == null || datosFiltrosAvanzados.ComboAnios.length > 1) {
        _anioSeleccionado = 0;
        $('#faAnio').hide();
    };
    _numPedidoSeleccionado = 0;
    $('#txtBscNumPedidoPedidoAbierto').val('');
    $('#faNumPedido').hide();
    _numOrdenSeleccionado = 0;
    $('#txtBscNumOrdenPedidoAbierto').val('');
    $('#faNumOrden').hide();
    if (datosFiltrosAvanzados == null || datosFiltrosAvanzados.ComboEmpresas.length > 1) {
        _idEmpresaSeleccionado = 0;
        $('#faEmpresa').hide();
    };
    $('#txtBscFecInicioDesde').datepicker('setDate', null);
    $('#faFecInicio').hide();
    $('#txtBscFecInicioHasta').datepicker('setDate', null);
    $('#faFecFin').hide();
    _codProveSeleccionado = '';
    $('#faProveedor').empty();
    Cargar_Proveedores_FiltrosAvanzados();
    if (datosFiltrosAvanzados != null) {
        RecargarGrid();
    }
});
function MostrarTodosProveedores() {
    $('#lstBscProveedores li div').show();
    $('#lstBscProveedores').siblings('a').hide();
};
function Cargar_FiltrosAvanzados_PedidosAbiertos() {
    if (datosFiltrosAvanzados == null) return false;
    $('#cboBscAnioPedidoAbierto option').remove();
    var opcionesAnio = $('#cboBscAnioPedidoAbierto').prop('options');
    if (datosFiltrosAvanzados.ComboAnios.length == 1) {        
        $('#faAnio a').text('/');
        $('#faAnio a').removeClass('clickable');
        $('#faAnio').show();
    } else opcionesAnio[opcionesAnio.length] = new Option('', 0);
    $.each(datosFiltrosAvanzados.ComboAnios, function () {
        opcionesAnio[opcionesAnio.length] = new Option(this.anio, this.anio, (this.anio == _anioSeleccionado || datosFiltrosAvanzados.ComboAnios.length == 1), (this.anio == _anioSeleccionado || datosFiltrosAvanzados.ComboAnios.length == 1));
    });

    $('#cboBscEmpresa option').remove();
    var opcionesEmpresa = $('#cboBscEmpresa').prop('options');
    if (datosFiltrosAvanzados.ComboEmpresas.length == 1) {
        $('#faEmpresa span:last').text(datosFiltrosAvanzados.ComboEmpresas[0].denEmpresa);       
        $('#faEmpresa a').text('/');
        $('#faEmpresa a').removeClass('clickable');
        $('#faEmpresa').show();
    } else opcionesEmpresa[opcionesEmpresa.length] = new Option('', 0);
    $.each(datosFiltrosAvanzados.ComboEmpresas, function () {
        opcionesEmpresa[opcionesEmpresa.length] = new Option(this.denEmpresa, this.idEmpresa, (this.idEmpresa == _idEmpresaSeleccionado || datosFiltrosAvanzados.ComboEmpresas.length == 1), (this.idEmpresa, this.idEmpresa == _idEmpresaSeleccionado || datosFiltrosAvanzados.ComboEmpresas.length == 1));
    });    

    $('#txtBscArticulo').tokenInput(datosFiltrosAvanzados.BuscadorArticulos, {
        searchingText: TextosPantalla[29],
        noResultsText: TextosPantalla[30],
        hintText: TextosPantalla[31],
        preventDuplicates: true,
        tokenLimit: 1,
        minChars: 3,
        resultsLimit: 50,
        onAdd: function (item) {
            $('#faArticulo span:last').text(item.name);
            $('#faArticulo').show();
            _codArticuloSeleccionado = item.id;
            RecargarGrid();
        },
        onDelete: function () {
            $('#faArticulo').hide();
            _codArticuloSeleccionado = '';
            RecargarGrid();
        }
    });
};
function Cargar_Proveedores_FiltrosAvanzados() {
    $('#lstBscProveedores').empty();
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'EP/PedidoAbierto.aspx/Obtener_Proveedores_PedidosAbiertos',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true
    })).done(function (msg) {
        var datosProveedoresFiltrosAvanzados = $.parseJSON(msg.d);
        if (_codProveSeleccionado == '') {
            $('#tmplItemBscProveedor').tmpl(datosProveedoresFiltrosAvanzados.sort(ordenDescendenteProveedoresNumPedidosAbiertos)).appendTo($('#lstBscProveedores'));
        };
        $('#lstBscProveedores li div').slice(0, 12).show();
        if ($('#lstBscProveedores li div').length > 12) $('#lstBscProveedores').siblings('a').show();
        if (cargado) {
            OcultarCargando();
            cargado = false;
        } else cargado = true;
        if ($('#divFiltrosAplicados a.clickable:visible').length > 1) $('#faBorrarFiltros').show();
        else $('#faBorrarFiltros').hide();
    });
}
//#endregion
//#region Paginador
function IndexChanged() {
    var dropdownlist = $('[id$=PagerPageList]')[0];
    var grid = $find($('[id$=whdgPedidosAbiertos]').attr('id')).get_gridView();
    var newValue = dropdownlist.selectedIndex;
    grid.get_behaviors().get_paging().set_pageIndex(newValue);
}
$('[id$=ImgBtnFirst]').live('click', function () {
    var grid = $find($('[id$=whdgPedidosAbiertos]').attr('id')).get_gridView();
    grid.get_behaviors().get_paging().set_pageIndex(0);
});
$('[id$=ImgBtnPrev]').live('click', function () {
    var grid = $find($('[id$=whdgPedidosAbiertos]').attr('id')).get_gridView();
    var dropdownlist = $('[id$=PagerPageList]')[0];
    if (grid.get_behaviors().get_paging().get_pageIndex() > 0) {
        grid.get_behaviors().get_paging().set_pageIndex(grid.get_behaviors().get_paging().get_pageIndex() - 1);
    }
});
$('[id$=ImgBtnNext]').live('click', function () {
    var grid = $find($('[id$=whdgPedidosAbiertos]').attr('id')).get_gridView();
    var dropdownlist = $('[id$=PagerPageList]')[0];
    if (grid.get_behaviors().get_paging().get_pageIndex() < grid.get_behaviors().get_paging().get_pageCount() - 1) {
        grid.get_behaviors().get_paging().set_pageIndex(grid.get_behaviors().get_paging().get_pageIndex() + 1);
    }
});
$('[id$=ImgBtnLast]').live('click', function () {
    var grid = $find($('[id$=whdgPedidosAbiertos]').attr('id')).get_gridView();
    grid.get_behaviors().get_paging().set_pageIndex(grid.get_behaviors().get_paging().get_pageCount() - 1);
});
function whdgPedidosAbiertos_PageIndexChanged() {
    var grid = $find($('[id$=whdgPedidosAbiertos]').attr('id')).get_gridView();
    var dropdownlist = $('[id$=PagerPageList]')[0];

    dropdownlist.options[grid.get_behaviors().get_paging().get_pageIndex()].selected = true;
}
//#endregion
//#region Eventos WebDataGrid
function whdgPedidosAbiertos_RowSelectionChanged(sender, e) {
    var ordenEntrega = e.getSelectedRows().getItem(0).get_cellByColumnKey("ORDENENTREGA").get_value();
    Cargar_Lineas_PedidoAbierto(ordenEntrega);
}
function whdgPedidosAbiertos_Initialize(sender, e) {    
    if (sender.get_rows().get_length() == 0) cargado = true;                
    else if (seleccionarPrimerRegistro) Cargar_PrimerRegistroGrid();
    Cargar_Proveedores_FiltrosAvanzados(); 
    seleccionarPrimerRegistro = false;
};
//#endregion
//#region Eventos spinner
$('ul.spinner>li.up').live('click', function () {
    var cantidad = $(this).parent('ul').siblings('input');
    if (cantidad.val() == '') cantidad.val(0);
    cantidad.numericFormatted('val', parseFloat(parseFloat(cantidad.val()) + 1));
});
$('ul.spinner>li.down').live('click', function () {
    var cantidad = $(this).parent('ul').siblings('input');
    if (cantidad.val() == '') cantidad.val(0);
    if (parseFloat(parseFloat(cantidad.val()) - 1) >= 0) cantidad.numericFormatted('val', parseFloat(parseFloat(cantidad.val()) - 1));
});
//#endregion
//#region LineasPedido
/*
Boton de agregar a cesta
*/
$('#lstLineasPedidoAbierto li span.botonRedondeado').live('click', function () {
    var boton = $(this);
    var grid = $find($('[id$=whdgPedidosAbiertos]').attr('id')).get_gridView();
    var tipopedido, pedido;
    pedido = Obtener_Float_Texto(boton.parents('#lstLineasPedidoAbierto>li').find('input').val());
    if (grid.get_behaviors().get_selection().get_selectedRows().getItem(0).get_cellByColumnKey("TIPO").get_value() == 2) tipopedido = 2;
    else tipopedido = 3;
    if (pedido == 0) {
        if (tipopedido == 2) alert(TextosPantalla[33]);
        else alert(TextosPantalla[32]);
        return false;
    };

    if (LimiteNoSuperado(boton)) {
        var numPeticion = 1;
        if (FSNPeticionesCesta.length > 0) numPeticion = FSNPeticionesCesta[FSNPeticionesCesta.length - 1] + 1;
        FSNWebPartsCesta.cambiarmodo('actualizando');
        FSNPeticionesCesta.push(numPeticion);
        if (grid.get_rows().get_length() > 0) {
            var ordenEntrega = grid.get_behaviors().get_selection().get_selectedRows().getItem(0).get_cellByColumnKey("ORDENENTREGA").get_value();
            MostrarCargando();
            var params = { NumPeticion: numPeticion, Orden_Ped_Abierto: ordenEntrega, IdLineaPedido: parseInt(boton.siblings('span').text()), Importe: (tipopedido == 2 ? pedido : 0), Cantidad: (tipopedido == 3 ? pedido : 0) };
            $.when($.ajax({
                type: "POST",
                url: rutaFS + 'EP/PedidoAbierto.aspx/Agregar_a_Cesta',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(params),
                async: true
            })).done(function (msg) {
                //Actualizamos los datos para mostrar la cantidad que tiene en cesta el usuario
                var datos = $.parseJSON(msg.d);
                FSNWebPartsCesta.actualizarvalores(datos[1], datos[2]);
                FSNPeticionesCesta.splice(FSNPeticionesCesta.indexOf(datos[0]), 1);
                if (FSNPeticionesCesta.length == 0) FSNWebPartsCesta.cambiarmodo('cesta');
                if (datos[1] != '' && datos[2] != '') FSNWebPartsCesta.actualizarvalores(datos[1], datos[2]);
                var index = $('#lstLineasPedidoAbierto>li').index(boton.parents('#lstLineasPedidoAbierto>li'));
                $('#lstLineasPedidoAbierto>li:nth-of-type(' + (index + 1) + ')').remove();
                if (index == 0) $('#tmplLineaPedidoAbierto').tmpl(datos[3]).prependTo($('#lstLineasPedidoAbierto'));
                else $('#lstLineasPedidoAbierto>li:nth-of-type(' + index + ')').after($('#tmplLineaPedidoAbierto').tmpl(datos[3]));
                $('#lstLineasPedidoAbierto>li:nth-of-type(' + (index + 1) + ') input').val('0' + UsuNumberDecimalSeparator + '00000000'.substring(0, UsuNumberNumDecimals));
                $('#lstLineasPedidoAbierto>li:nth-of-type(' + (index + 1) + ') input').numeric({ decimal: '' + usuario.DecimalFmt + '', negative: false }).numericFormatted({
                    decimalSeparator: UsuNumberDecimalSeparator,
                    groupSeparator: UsuNumberGroupSeparator,
                    decimalDigits: UsuNumberNumDecimals
                })
                OcultarCargando();
            });
        };
    } else {
        var textoAlert = '.\n' + TextosPantalla[28] + ':\n' + $(this).parent('li').siblings('li').children('span:first').text() + ' ' + $(this).parent('li').siblings('li').children('span:last').text();
        if (grid.get_behaviors().get_selection().get_selectedRows().getItem(0).get_cellByColumnKey("TIPO").get_value() == 2) alert(TextosPantalla[26] + textoAlert);
        else alert(TextosPantalla[27] + textoAlert);
    }
});
$('#lstLineasPedidoAbierto li input').live('change', function () {
    if ($(this).val() == '') $(this).val('0');
});
$('#lstLineasPedidoAbierto li input').live('blur', function () {
    if ($(this).val() == '') $(this).val('0');
});
$('#lstLineasPedidoAbierto li input').live('click', function () {
    $(this).select();
});
function LimiteNoSuperado(boton) {
    var lineaPedido=boton.parents('#lstLineasPedidoAbierto>li');
    if (Obtener_Float_Texto(lineaPedido.find('div:last ul li:first span:first').text()) - Obtener_Float_Texto(lineaPedido.find('input').val()) < 0) return false;
    else return true;
};
//#endregion
//#region Funciones utiles
function Obtener_Float_Texto(texto) {
    return parseFloat(replaceAll(texto, usuario.ThousanFmt, '').replace(usuario.DecimalFmt, '.'));
}
function replaceAll(sOriginal, sBuscar, sSustituir) {
    if (sOriginal == null) return null;
    if (sBuscar == "") return sOriginal;

    var re = ""
    if (sBuscar == ".") re = "[\.]";
    else re = eval("/" + sBuscar + "/");

    var sTmp = "";
    while (sOriginal.search(re) >= 0) {
        sTmp += sOriginal.substr(0, sOriginal.search(re)) + sSustituir
        sOriginal = sOriginal.substr(sOriginal.search(re) + sBuscar.length, sOriginal.length)
    }
    sTmp += sOriginal;
    return sTmp;
}
//#endregion