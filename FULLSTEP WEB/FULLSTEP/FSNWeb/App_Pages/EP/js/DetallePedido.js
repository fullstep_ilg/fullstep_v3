﻿var idRecept = "";
var gDetallePedido = null;
var gAtributos = null;
var gAtributosLinea = null;
var gCostes = null;
var gImpuestos = null;
var gCostesLinea = null;
var gDescu = null;
var gDescuLinea = null;
var gDatosGenerales = null;
var gArchivos = null;
var gidLineaRechazo = null;
var oTipo = null; 
var gLineas = null;
var Linea = null;
var gDatosDestinos = null;
var gDatosUnidades = null;
var gDesde=null;
var sMoneda = "EUR";
var LineaUnidad = null;
var gbError = false;
var IdPedido, IdEmpresa;
var gIdAccion, gIdBloque, gIdInstancia;
var gIdLog;
var gIdEntidad;
var gErp;
$(document).ready(function () {
    OcultarRelanzar();
    if (gDatosGenerales == null) {
        iFilaSeleccionada = 0;
        iCodFilaSeleccionada = 0;
        cargarDatosGenerales();
        cargarLineas();
        calcularTotalesInicio(false);
        cargarTextosJ();
        ComprobarAtributosOrden();
    };
    $('#lblAceptar').text(TextosJScript[34]);
    $('#lblCancelar').text(TextosJScript[35]);
    $('#lblAceptarConfirmRecep').text(TextosJScript[34]);
    $('#lblCancelarConfirmRecep').text(TextosJScript[35]);
    $('#imgPnlConfirm').attr('src', ruta + 'images/icono_info.gif');
    $('#txtConfirm').live(($.browser.opera ? 'keypress' : 'keydown'), function () {
        return imposeMaxLength(this, 4000);
    });
    $('#txtConfirm').live('blur', function () {
        return limitToMaxLength(this, 4000);
    });

    ConfigurarCabecera();    

    //Seleccionamos fila grid
    var grid = $find($('[id$=whdgArticulos]').attr('id'));
    if (grid !== undefined && grid !== null) {
        grid.get_element().focus();
        var gridView = grid.get_gridView();
        var activation = gridView.get_behaviors().get_activation();
        if (gridView.get_rows().get_length() > 0) activation.set_activeCell(gridView.get_rows().get_row(0).get_cell(0));
    };
});

function ConfigurarCabecera() {
    //Visibilidad botones
    //Si el pedido estÃ¡ Anulado o Cerrado o En recepciÃ³n no se mostrarÃ¡ el botÃ³n de borrado lÃ³gico.  SÃ­ se mostrarÃ¡, como excepciÃ³n, en los pedidos cerrados que son abonos o pedidos no recepcionables. 
    //Estado:EnRecepcion=5, RecibidoYCerrado=6, Anulado=20 -- TipoPedido:No recepciÃ³n=0, obligatoria=1, opcional=2    
    if ($('[id$=lnkBotonBorrar]').css("display") != "none" && (gDatosGenerales.BajaLogica || gDatosGenerales.Estado == 20 || gDatosGenerales.Estado == 5 || (gDatosGenerales.Estado == 6 && !gDatosGenerales.Abono && gDatosGenerales.Tipo.Recepcionable != 0))) $('[id$=lnkBotonBorrar]').css("display", "none");
    //El botÃ³n de deshacer borrado no se mostrarÃ¡ si el pedido no estÃ¡ borrado
    if ($('[id$=lnkBotonDeshacerBorrar]').css("display") != "none" && (!gDatosGenerales.BajaLogica)) $('[id$=lnkBotonDeshacerBorrar]').css("display", "none");
    //Los botones Anular, Cerrar y Reabrir no se muestran si el pedido estÃ¡ borrado
    if ($('[id$=lnkBotonAnular]').css("display") != "none" && (gDatosGenerales.BajaLogica)) $('[id$=lnkBotonAnular]').css("display", "none");
    if ($('[id$=lnkBotonCierrePositivo]').css("display") != "none" && (gDatosGenerales.BajaLogica)) $('[id$=lnkBotonCierrePositivo]').css("display", "none");
    if ($('[id$=lnkBotonCierreNegativo]').css("display") != "none" && (gDatosGenerales.BajaLogica)) $('[id$=lnkBotonCierreNegativo]').css("display", "none");
    if ($('[id$=lnkBotonReabrir]').css("display") != "none" && (gDatosGenerales.BajaLogica)) $('[id$=lnkBotonReabrir]').css("display", "none");
}

function cargarTextosJ() {
    $("#btnAceptarError").val(TextosJScript[7]);
    $("#btnCancelarError").val(TextosJScript[8]);
    $("#PbtnAceptarMensaje").val(TextosJScript[7]);
    $("#PbtnCancelarMensaje").val(TextosJScript[8]);
    $('#lblCabeceraPlanEntregaFechaEntrega').text(TextosJScript[49]);
};
    function cargarCostesGrid(linea) {
        var ImporteFijo = 0;
        var ImporteFijoLinea = 0;
        var GrupoCosteDescuentoAnt = "";

        if (linea.Costes.length !== 0) { //Si hay atributos calculamos totales
            $.each(linea.Costes, function () {
                ImporteFijoLinea = 0;
                if (this.Operacion == "0") {  //+
                    if (this.Valor == undefined) {
                        var Importe = 0;
                    }
                    else {
                        this.Valor = this.Valor * dCambio;
                        var Importe = this.Valor;
                    }
                }
                else {
                    if (this.Valor == undefined) {
                        var Importe = 0;
                    }
                    else {
                        var Importe = parseFloat($('#lblImpBruto' + linea.ID + '_hdd').text().replace(",", "."));
                        Importe = Importe * (this.Valor / 100);
                    }
                }

                this.Importe = Importe;

                if (this.GrupoCosteDescuento == undefined) {
                    var GrupoCosteDescuento = "";
                }
                else {
                    var GrupoCosteDescuento = this.GrupoCosteDescuento;
                }
                if (this.Inicio == undefined) {
                    if (GrupoCosteDescuentoAnt !== "" && this.GrupoCosteDescuento !== undefined) {
                        if (this.GrupoCosteDescuento == GrupoCosteDescuentoAnt) {
                            Inicio = "1";
                        }
                        else {
                            Inicio = "0";
                        }
                    }
                    else {
                        Inicio = "0";
                    }
                }
                else {
                    Inicio = this.Inicio;
                }
                this.Inicio = Inicio;
                if (this.Seleccionado == undefined) {
                    var Seleccionado = false;
                    this.Seleccionado = Seleccionado;
                }
                this.Moneda = sMoneda;
                GrupoCosteDescuentoAnt = GrupoCosteDescuento;
                ImporteFijoLinea = parseFloat(ImporteFijoLinea) + parseFloat(this.Importe);
                ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
            });
        }
        $('#lblCosteTotalLinea_' + linea.ID).text(formatNumber(ImporteFijo) + " " + sMoneda);
        $('#lblCosteTotalLinea_' + linea.ID + '_hdd').text(ImporteFijo);
    }
    function cargarDescuentosGrid(linea) {
        var ImporteFijo = 0;
        var ImporteFijoLinea = 0;
        var GrupoCosteDescuentoAnt = "";

        if (linea.Descuentos.length !== 0) { //Si hay atributos calculamos totales
            $.each(linea.Descuentos, function () {

                ImporteFijoLinea = 0;
                if (this.Operacion == "0") { //-
                    if (this.Valor == undefined) {
                        var Importe = 0;
                    }
                    else {
                        this.Valor = this.Valor * dCambio;
                        var Importe = this.Valor;
                    }
                }
                else {
                    if (this.Valor == undefined) {
                        var Importe = 0;
                    }
                    else {
                        var Importe = parseFloat($('#lblImpBruto' + linea.ID + '_hdd').text().replace(",", "."));
                        Importe = Importe * (this.Valor / 100);
                    }
                }
                this.Importe = Importe;
                if (this.GrupoCosteDescuento == undefined) {
                    var GrupoCosteDescuento = "";
                }
                else {
                    var GrupoCosteDescuento = this.GrupoCosteDescuento;
                }
                if (this.Inicio == undefined) {
                    if (GrupoCosteDescuentoAnt !== "" && this.GrupoCosteDescuento !== undefined) {
                        if (this.GrupoCosteDescuento == GrupoCosteDescuentoAnt) {
                            Inicio = "1";
                        }
                        else {
                            Inicio = "0";
                        }
                    }
                    else {
                        Inicio = "0";
                    }
                }
                else {
                    Inicio = this.Inicio;
                }
                this.Inicio = Inicio;
                if (this.Seleccionado == undefined) {
                    var Seleccionado = false;
                    this.Seleccionado = Seleccionado;
                }
                this.Moneda = sMoneda;
                GrupoCosteDescuentoAnt = GrupoCosteDescuento;
                ImporteFijoLinea = parseFloat(ImporteFijoLinea) + parseFloat(this.Importe);
                ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
            });
        }
        $('#lblDescuentoTotalLinea_' + linea.ID).text(formatNumber(ImporteFijo) + " " + sMoneda);
        $('#lblDescuentoTotalLinea_' + linea.ID + '_hdd').text(ImporteFijo);
    }
    function cargarCostesLinea() {
        gLineas = gDetallePedido.Lineas;
        $.each(gLineas, function () {
            if (this.ID == iCodFilaSeleccionada) {
                Linea = this;
                srtLinea = ("00" + iFilaSeleccionada.toString()).slice(-3);
                if (bMostrarCodigoArt == true) {
                    cabLinea = srtLinea + " - " + this.Cod + " - " + this.Den;
                } else {
                    cabLinea = srtLinea + " - " + this.Den;
                }
                $('#lblLineaCoste').text(cabLinea);
                if (Linea.Costes == undefined) {
                    if ($('#tmplCampoCosteLineaReadOnly').length == 0) {
                        $.when($.get(ruta + '/App_Pages/EP/html/detallepedido.tmpl.htm', function (templates) {
                            $('body').append(templates);
                        })).done(function () {
                            CargarAtributosCosteLinea();
                        });
                    } else { CargarAtributosCosteLinea(); }
                }
                else {

                    gCostesLinea = this.Costes;
                    if ($('#tmplCampoCosteLineaReadOnly').length == 0) {
                        $.when($.get(ruta + '/App_Pages/EP/html/detallepedido.tmpl.htm', function (templates) {
                            $('body').append(templates);
                        })).done(function () {
                            pintarCostesLinea();
                        });
                    } else { pintarCostesLinea(); }

                }
            }
        });
    }
    function cargarDescuentosLinea() {
        gLineas = gDetallePedido.Lineas;
        $.each(gLineas, function () {
            if (this.ID == iCodFilaSeleccionada) {
                Linea = this;
                srtLinea = ("00" + iFilaSeleccionada.toString()).slice(-3);
                if (bMostrarCodigoArt == true) {
                    cabLinea = srtLinea + " - " + this.Cod + " - " + this.Den;
                } else {
                    cabLinea = srtLinea + " - " + this.Den;
                }
                $('#lblLineaDescuento').text(cabLinea);
                if (Linea.Descuentos == undefined) {
                    if ($('#tmplCampoDescuFijoLinea').length == 0) {
                        $.when($.get(ruta + '/App_Pages/EP/html/detallepedido.tmpl.htm', function (templates) {
                            $('body').append(templates);
                        })).done(function () {
                            CargarAtributosDescuentoLinea();
                        });
                    } else { CargarAtributosDescuentoLinea(); }
                }
                else {
                    gDescuentosLinea = this.Descuentos;
                    if ($('#tmplCampoCosteFijoLinea').length == 0) {
                        $.when($.get(ruta + '/App_Pages/EP/html/detallepedido.tmpl.htm', function (templates) {
                            $('body').append(templates);
                        })).done(function () {
                            pintarDescuentosLinea();
                        });
                    } else { pintarDescuentosLinea(); }
                }

            }
        });
    }
    function checkCollapseStatus() {
        var imgExpandir = $get("imgExpandir");
        if ($('#divImportesPedido').css("display") == "none") {
            $('#divImportesPedido').css("display", "block");
            imgExpandir.src = "../../Images/contraer_arriba.png"
        } else {
            $('#divImportesPedido').css("display", "none");
            imgExpandir.src = "../../Images/expandir_abajo.png"
        }
    }
    //funcion para cambiar el precio de una linea de pedido
    //idLinea: id de la linea de pedido
    function cambiarPrecio(idLinea) {
        var EstaDeBaja = false;
        var PrecioBaja;
        $.each(gDetallePedido.Lineas, function () {
            if (this.ID == idLinea) {
                EstaDeBaja = this.EstaDeBaja;
                PrecioBaja = this.PrecUc
            }
        });        
        if (EstaDeBaja) {
            $('#Prec' + idLinea).val(formatNumber(PrecioBaja));
            alert(TextosAlert[39])
            return false;
        } else {
            var cantidad = parseFloat(strToNum($.trim($('#Cant' + idLinea).val())));
            var precioU = parseFloat(strToNum($.trim($('#Prec' + idLinea).val())));
            var precioImpBruto = parseFloat($.trim($('#lblImpBrutoLinea_hdd').text()).replace(",", "."));
            var precioLineaAnt = parseFloat($.trim($('#lblImpBruto' + idLinea + '_hdd').text()).replace(",", "."));
            var precio = cantidad * precioU;

            $('#Prec' + idLinea + '_hdd').text(precioU);
            $('#PrecT' + idLinea).val(precioU);

            precioImpBruto = precioImpBruto - precioLineaAnt + precio;
        
            $.each(gDetallePedido.Lineas, function () {
                if (this.ID == idLinea) {
                    this.PrecUc = precioU;
                    this.Modificada = true
                }
            });

            $('#lblImpBruto' + idLinea + '_hdd').text(precio);
            $('#lblImpBruto' + idLinea).text(formatNumber(precio));
            $('#lblImpBruto').text(formatNumber(precioImpBruto) + " " + sMoneda);
            $('#lblImpBrutoLinea_hdd').text(precioImpBruto);
            $('#lblImpBruto_hdd').text(precioImpBruto);
            $('#lblImpBruto' + idLinea + '_hdd').text(precio);
            $('#lblImpBruto' + idLinea).text(formatNumber(precio));
            recalcularLineaCosteDescuento();
            $('#pesCostes').click();
            $('#pesDescuento').click();
            $('#pesDatosGenerales').click();
            calculoTotalCabecera(false);
        }
    }
    function cambiarImporteBruto(idLinea) {
        var precioImpBrutoLinea = parseFloat($.trim($('#lblImpBruto' + idLinea).val()).replace(",", "."));
        var precioLineaAnt = parseFloat($.trim($('#lblImpBruto' + idLinea + '_hdd').text()).replace(",", "."));
        var precioImpBruto = parseFloat($.trim($('#lblImpBrutoLinea_hdd').text()).replace(",", "."));

        precioImpBruto = precioImpBruto - precioLineaAnt + precioImpBrutoLinea;

        var EstaDeBaja = false;
        $.each(gDetallePedido.Lineas, function () {
            if (this.ID == idLinea) {
                this.ImportePedido = precioImpBrutoLinea;
                EstaDeBaja = this.EstaDeBaja;
            }
        });

        if (EstaDeBaja) {
            $('#lblImpBruto' + idLinea + '_hdd').text(precioImpBrutoLinea);
            $('#lblImpBruto' + idLinea).val(formatNumber(precioImpBrutoLinea));
            alert(TextosAlert[39])
            return false;
        } else {
            $('#lblImpBruto').text(formatNumber(precioImpBruto) + " " + sMoneda);
            $('#lblImpBrutoLinea_hdd').text(precioImpBruto);
            $('#lblImpBruto_hdd').text(precioImpBruto);
            $('#lblImpBruto' + idLinea + '_hdd').text(precioImpBrutoLinea);
            $('#lblImpBruto' + idLinea).val(formatNumber(precioImpBrutoLinea));
            recalcularLineaCosteDescuento();
            $('#pesCostes').click();
            $('#pesDescuento').click();
            $('#pesDatosGenerales').click();
            calculoTotalCabecera(false);
        }
    };
    $('#btnAceptarMensaje').live('click', function () {
        $('#textMensaje').empty();
        $('#divMensaje').hide();
        OcultarFondoPopUp();
        MostrarCargando();
        window.location.href = rutaFS + 'EP/Aprobacion.aspx';
    });

    $('[id^=lblBase_]').live('click', function (event) {
        var parts = this.id.split("_");
        var id = $.trim(parts[1]);
        var valorImp = $.trim(parts[2]);
        $("#divInformacionIVA_" + id + '_' + valorImp).fadeToggle(1000);
        //event.stopPropagation();
        stopEvent(event);
    });

    function stopEvent(e) {

        if (!e) var e = window.event;

        //e.cancelBubble is supported by IE -
        // this will kill the bubbling process.
        e.cancelBubble = true;
        e.returnValue = false;

        //e.stopPropagation works only in Firefox.
        if (e.stopPropagation) e.stopPropagation();
        if (e.preventDefault) e.preventDefault();

        return false;
    }
    $(document).mouseup(function (e) {
        var container = $('[id^=divInformacionIVA_]');
        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.hide();
        }
    });
    function mostrarError() {
        $('#divError').css('z-index', '200002');
        $('#divError').show();
        CentrarPopUp($('#divError'));
    }
    function mostrarOcultarError(texto) {
        $('#textError').empty();
        $('#textErrorCab').empty();
        if (texto !== "") {
            MostrarFondoPopUp();
            var mensaje =
            [{
                valor: texto
            }];
            $('#MensajeError').tmpl(mensaje).appendTo($('#textError'));
            //$("#lblErrorTexto").text(texto);
            $('#divError').css('z-index', '200002');
            $('#divError').show();
            CentrarPopUp($('#divError'));
        }
    }
    function mostrarOcultarMensaje(texto) {
        mensaje = [{ valor: "DTextoCabecera" }];
        $('#MensajeError').tmpl(mensaje).appendTo($('#textMensajeCab'));
        mensaje = [{ valor: texto }];
        $('#MensajeError').tmpl(mensaje).appendTo($('#textMensaje'));
        mostrarMensaje();
    }
    function MostrarFondoPopUp() {
        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').css('z-index', 10002);
        $('#popupFondo').show();
    }
    function OcultarFondoPopUp() {
        $('#popupFondo').hide();
        $('#popupFondo').css('z-index', 1001);
    }
    function mostrarMensaje() {
        $('#divMensaje').css('z-index', '200002');
        $('#divMensaje').show();
        CentrarPopUp($('#divMensaje'));
    }
    function ponerAvisoEnLinea(idLinea) {
        $('#adr' + idLinea).css("display", "block");
    }
    function quitarAvisoEnLinea(idLinea) {
        $('#adr').css("display", "none");
    }
    function mostrarLineaCostes(fila, codLinea) {
        iFilaSeleccionada = fila;
        iCodFilaSeleccionada = codLinea;
        $('#pesCostesLinea').click();
    }
    function mostrarLineaDescuentos(fila, codLinea) {
        iFilaSeleccionada = fila;
        iCodFilaSeleccionada = codLinea;
        $('#pesDescuentosLinea').click();
    }
    function comprobarCantidad(idLinea) {
        var dCantidad = parseFloat(strToNum($.trim($('#Cant' + idLinea).val()).replace(",", ".")));
        var sUPed = $('#Uni' + idLinea).text();

        var gDetallePedidoGuardar = jQuery.extend(true, {}, gDetallePedido);
        var i = 0;

        $('#aviso' + idLinea).css("display", "none");
        $.each(gDetallePedidoGuardar.Lineas, function () {
            gCentroCoste = [];
            gPartidas = [];
            i = i + 1;
            if (this.Pres5_ImportesImputados_EP[0] !== undefined) {
                this.Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM.PartidasPresupuestarias = null;
                this.Pres5_ImportesImputados_EP = [];
            } else {
                this.Pres5_ImportesImputados_EP = [];
            }
        });

        var params = JSON.stringify({ idLinea: idLinea, sUPed: sUPed, dCant: dCantidad, sPedido: JSON.stringify(gDetallePedidoGuardar) });

        var iRespuesta = 0;
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/ComprobarCantidades',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        })).done(function (msg) {
            iRespuesta = $.parseJSON(msg.d);
        });

        if (iRespuesta !== 0) {
            //mostrar error cantidad       
            //1 -- Cantidad no supera la minima establecida
            //2 -- Cantidad supera la maxima extablecida
            //3 -- Cantidad supera maxima total a pedir por linea de catalogo
            //4 -- Supera la cantidad maxima a pedir por linea de catalogo en unidad de pedido
            switch (iRespuesta) {
                case 1:
                    {
                        mostrarOcultarError(TextosJScript[18]);

                        $('#aviso' + idLinea).css("display", "inline-block");
                        $('#aviso' + idLinea).prop('title', TextosJScript[18]);
                        break
                    }
                case 2:
                    {
                        mostrarOcultarError(TextosJScript[19]);

                        $('#aviso' + idLinea).css("display", "inline-block");
                        $('#aviso' + idLinea).prop('title', TextosJScript[19]);
                        break
                    }
                case 3:
                    {
                        mostrarOcultarError(TextosJScript[20]);

                        $('#aviso' + idLinea).css("display", "inline-block");
                        $('#aviso' + idLinea).prop('title', TextosJScript[20]);
                        break
                    }
                case 4:
                    {
                        mostrarOcultarError(TextosJScript[21]);

                        $('#aviso' + idLinea).css("display", "inline-block");
                        $('#aviso' + idLinea).prop('title', TextosJScript[21]);
                        break
                    }
            }
        }
        return iRespuesta;
    }
    //funcion para cambiar la cantidad de la linea de pedido
    //idLinea: id de la linea de pedido
    function cambiarCantidad(idLinea) {
        var iCambiarCantidadValido = comprobarCantidad(idLinea);
        var cantidad = parseFloat(strToNum($.trim($('#Cant' + idLinea).val())));
        var precioU = parseFloat($.trim($('#Prec' + idLinea + '_hdd').text()).replace(",", "."));
        var precioImpBruto = parseFloat($.trim($('#lblImpBrutoLinea_hdd').text()).replace(",", "."));
        var precioLineaAnt = parseFloat($.trim($('#lblImpBruto' + idLinea + '_hdd').text()).replace(",", "."));
        var precio = cantidad * precioU;

        precioImpBruto = precioImpBruto - precioLineaAnt + precio;

        var EstaDeBaja = false;
        $.each(gDetallePedido.Lineas, function () {
            if (this.ID == idLinea) {
                this.CantPed = cantidad;
                this.Modificada = true
                EstaDeBaja = this.EstaDeBaja;
            }
        });

        if (EstaDeBaja) {
            $('#lblImpBruto' + idLinea + '_hdd').text(precio);
            $('#lblImpBruto' + idLinea).text(formatNumber(precio));
            alert(TextosAlert[39])
            return false;
        } else {
            $('#lblImpBruto').text(formatNumber(precioImpBruto) + " " + sMoneda);
            $('#lblImpBrutoLinea_hdd').text(precioImpBruto);
            $('#lblImpBruto_hdd').text(precioImpBruto);
            $('#lblImpBruto' + idLinea + '_hdd').text(precio);
            $('#lblImpBruto' + idLinea).text(formatNumber(precio));
            recalcularLineaCosteDescuento();
            $('#pesCostes').click();
            $('#pesDescuento').click();
            $('#pesDatosGenerales').click();
            calculoTotalCabecera(false);
        }
    }
    //<param name="bConBajas">Indica si hay que incluir las lÃ­neas de baja en el cÃ¡lculo</param>
    function calculoTotalCabecera(bConBajas) {
        if ($('#tmplComboEP').length == 0) {
            $.when($.get(ruta + '/App_Pages/EP/html/cabecera_detalle_pedido.tmpl.htm', function (templates) {
                $('body').append(templates);
            })).done(function () {
                calcularImpuestos(bConBajas);
                recalcularImportes(bConBajas);
            });
        }
        else {
            calcularImpuestos(bConBajas);
            recalcularImportes(bConBajas);
        }
    }
    //<param name="bConBajas">Indica si hay que incluir las lÃ­neas de baja en el cÃ¡lculo</param>
    function calcularTotalesInicio(bConBajas) {
        var bHayCostes, bHayDescuentos;
        bHayCostes = false;
        bHayDescuentos = false;
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/Cargar_Costes_Descuentos',
            contentType: "application/json; charset=utf-8",
            data: "{'sIden':'" + sID + "'}",
            dataType: "json",
            async: false
        })).done(function (msg) {
            var localDetallePedido = $.parseJSON(msg.d);
            var ImporteFijo = 0;
            var GrupoCosteDescuentoAnt = "";

            gDetallePedido.Costes = localDetallePedido.Costes;
            if (gDetallePedido.Costes.length !== 0) { //Si hay atributos calculamos totales            
                $.each(gDetallePedido.Costes, function () {
                    bHayCostes = true;
                    if (this.Operacion == "0") { //+
                        if (this.Valor == undefined) {
                            var Importe = 0;
                        }
                        else {
                            this.Valor = this.Valor * dCambio;
                            var Importe = this.Valor;
                        }
                    }
                    else {
                        if (this.Valor == undefined) {
                            var Importe = 0;
                        }
                        else {                            
                            var Importe = bConBajas ? parseFloat($('#lblImpBrutoConBajas_hdd').text().replace(",", ".")) : parseFloat($('#lblImpBruto_hdd').text().replace(",", "."));
                            Importe = Importe * (this.Valor / 100);
                        }
                    }

                    this.Importe = Importe;

                    if (this.GrupoCosteDescuento == undefined) {
                        var GrupoCosteDescuento = "";
                    }
                    else {
                        var GrupoCosteDescuento = this.GrupoCosteDescuento;
                    }
                    if (this.Inicio == undefined) {
                        if (GrupoCosteDescuentoAnt !== "" && this.GrupoCosteDescuento !== undefined) {
                            if (this.GrupoCosteDescuento == GrupoCosteDescuentoAnt) {
                                Inicio = "1";
                            }
                            else {
                                Inicio = "0";
                            }
                        }
                        else {
                            Inicio = "0";
                        }
                    }
                    else {
                        Inicio = this.Inicio;
                    }
                    this.Inicio = Inicio;
                    if (this.Seleccionado == undefined) {
                        var Seleccionado = false;
                        this.Seleccionado = Seleccionado;
                    }
                    this.Moneda = sMoneda;
                    GrupoCosteDescuentoAnt = GrupoCosteDescuento;
                    ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                });
            }
            if (bHayCostes == false) {
                $('#pesCostes').css("display", "none");
            }
            $('#lblCosteCabeceraTotal_hdd').text(ImporteFijo);
            $('#lblCosteCabeceraTotal').text(formatNumber(ImporteFijo) + " " + sMoneda);
            ImporteFijo = 0;
            gDetallePedido.Descuentos = localDetallePedido.Descuentos;

            if (gDetallePedido.Descuentos.length !== 0) { //Si hay atributos calculamos totales            
                $.each(gDetallePedido.Descuentos, function () {
                    bHayDescuentos = true;

                    if (this.Operacion == "0") { //-
                        if (this.Valor == undefined) {
                            var Importe = 0;
                        }
                        else {
                            this.Valor = this.Valor * dCambio;
                            var Importe = this.Valor;
                        }
                    }
                    else {
                        if (this.Valor == undefined) {
                            var Importe = 0;
                        }
                        else {
                            var Importe = bConBajas ? parseFloat($('#lblImpBrutoConBajas_hdd').text().replace(",", ".")) : parseFloat($('#lblImpBruto_hdd').text().replace(",", "."));                            
                            Importe = Importe * (this.Valor / 100);
                        }
                    }

                    this.Importe = Importe;

                    if (this.GrupoCosteDescuento == undefined) {
                        var GrupoCosteDescuento = "";
                    }
                    else {
                        var GrupoCosteDescuento = this.GrupoCosteDescuento;
                    }
                    if (this.Inicio == undefined) {
                        if (GrupoCosteDescuentoAnt !== "" && this.GrupoCosteDescuento !== undefined) {
                            if (this.GrupoCosteDescuento == GrupoCosteDescuentoAnt) {
                                Inicio = "1";
                            }
                            else {
                                Inicio = "0";
                            }
                        }
                        else {
                            Inicio = "0";
                        }
                    }
                    else {
                        Inicio = this.Inicio;
                    }
                    this.Inicio = Inicio;
                    if (this.Seleccionado == undefined) {
                        var Seleccionado = false;
                        this.Seleccionado = Seleccionado;
                    }
                    this.Moneda = sMoneda;
                    GrupoCosteDescuentoAnt = GrupoCosteDescuento;
                    ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                });
            }
            if (bHayDescuentos == false) {
                $('#pesDescuentos').css("display", "none");
            }
            $('#lblDescuentosCabeceraTotal_hdd').text(ImporteFijo);
            $('#lblDescuentosCabeceraTotal').text(formatNumber(ImporteFijo) + " " + sMoneda);
            calculoTotalCabecera(bConBajas);
            recalcularImportes(bConBajas);
        });
    }
    //function CalcularImporteBrutoLineas() {                       
    //}
    function recorrerImpuestosCabeceraPorcentajes() {
        if ($('#tmplComboEP').length == 0) {

            $.when($.get(ruta + '/App_Pages/EP/html/cabecera_detalle_pedido.tmpl.htm', function (templates) {
                $('body').append(templates);
            })).done(function () {
                calcularImpuestos(false);
                recalcularImportesPorcentajes();

                $('#pesCostes').click();
                $('#pesDescuento').click();
                $('#pesDatosGenerales').click();
                recalcularImportes(false);
            });
        }
        else {
            calcularImpuestos(false);
            recalcularImportesPorcentajes();

            $('#pesCostes').click();
            $('#pesDescuento').click();
            $('#pesDatosGenerales').click();
            recalcularImportes(false);
        }

    }
    function recorrerImpuestosCabecera() {
        if ($('#tmplComboEP').length == 0) {

            $.when($.get(ruta + '/App_Pages/EP/html/cabecera_detalle_pedido.tmpl.htm', function (templates) {
                $('body').append(templates);
            })).done(function () {
                calcularImpuestos(false);
            });
        }
        else {
            calcularImpuestos(false);
        }
    }
    //<param name="bConBajas">Indica si hay que incluir las lÃ­neas de baja en el cÃ¡lculo</param>
    function calcularImpuestos(bConBajas) {
        $('#lblImpTotalCuotas_hdd').text(0);
        var Impuestos = 0;
        var ImpuestoCuota = 0;
        var ImporteCoste = 0;
        var ImporteCosteLinea = 0;
        if ($('[id^=descCuota_]').length !== 0) {
            $.each($('[id^=descCuota_]'), function () {
                var parts = this.id.split("_");
                var idCouta = $.trim(parts[1]);
                var valorImpCouta = $.trim(parts[2]);
                $('[id^=trIVA_' + idCouta + '_' + valorImpCouta + ']').remove();
                $('[id^=divInformacionIVA_' + idCouta + '_' + valorImpCouta + ']').remove();
                EliminarCampo(idCouta, valorImpCouta);
            });
        }
        //Impuestos Cabecera
        var Linea = 0;
        var idConcepto = 0;
        var totalConcepto = 0;
        $.each(gDetallePedido.Costes, function () {
            idConcepto = this.Id;
            ImporteCoste = this.Importe;
            Linea++;

            if (this.Seleccionado == true) {
                Impuestos = parseFloat($.trim($('#lblImpTotalCuotas_hdd').text()).replace(",", "."));
                $.each(this.Impuestos, function () {
                    var ImpuestoAct = ImporteCoste * (this.Valor / 100);
                    if ($('#cuota_' + this.Id + '_' + this.Valor + '_hdd').length == 0) {
                        Impuestos = Impuestos + ImpuestoAct;
                        AgregarCampo(this.Id, this.Denominacion + " " + this.Valor + "%", ImpuestoAct, "C", Linea, ImporteCoste, idConcepto, this.Valor);
                    }
                    else {
                        ImpuestoCuota = parseFloat($.trim($('#cuota_' + this.Id + '_' + this.Valor + '_hdd').text()).replace(",", "."));
                        Impuestos = Impuestos - ImpuestoCuota;
                        ImpuestoAct = ImpuestoAct + ImpuestoCuota;
                        Impuestos = Impuestos + ImpuestoAct;
                        MostrarCampo(this.Id, this.Valor);
                        $('#cuota_' + this.Id + '_' + this.Valor + '_hdd').text(ImpuestoAct.toString());
                        $('#cuota_' + this.Id + '_' + this.Valor).text(formatNumber(ImpuestoAct.toString()) + " " + sMoneda);
                        totalConcepto = ImpuestoAct * (100 / this.Valor);
                        agregarCampoCuota(this.Id, idConcepto, this.Denominacion + " " + this.Valor + "%", "C", Linea, ImporteCosteLinea, totalConcepto, this.Valor);
                    }
                    $('#lblImpTotalCuotas_hdd').text(Impuestos);
                });
            }

        });

        //Impuestos Linea Catalogo
        var ImporteLinea, ImporteBrutoLinea, ImporteCostesLinea, ImporteDescuentosLinea, DescuentosCabeceraGeneral;
        var DescuentosLinea, DescuentosCabecera;
        var ImporteBruto, ImporteBrutoCabecera;
        var iNumerosLinea = 0;
        Linea = 0;
        idConcepto = 0;
        DescuentosCabecera = 0;

        var EstaDeBaja = false;
        $.each(gLineas, function () {
            if (!this.EstaDeBaja || gDetallePedido.DatosGenerales.BajaLogica || bConBajas) {
                iNumerosLinea++;
            }
        });        
        $.each(gLineas, function () {
            Linea++;
            idConcepto = this.ID;
            ImporteBrutoLinea = 0;
            ImporteCostesLinea = 0;
            ImporteDescuentosLinea = 0;
            ImporteBrutoLinea = parseFloat($.trim($('#lblImpBruto' + this.ID + '_hdd').text()).replace(",", "."));
            ImporteCostesLinea = parseFloat($.trim($('#lblCosteTotalLinea_' + this.ID + '_hdd').text()).replace(",", "."));
            ImporteDescuentosLinea = parseFloat($.trim($('#lblDescuentoTotalLinea_' + this.ID + '_hdd').text()).replace(",", "."));
            ImporteLinea = ImporteBrutoLinea + ImporteCostesLinea - ImporteDescuentosLinea;
            DescuentosCabeceraGeneral = 0;
            CostesCabeceraGeneral = 0;
            EstaDeBaja = this.EstaDeBaja;

            $.each(gDetallePedido.Descuentos, function () {
                if (this.Seleccionado == true) {

                    if (this.Operacion == "1") { //%
                        ImporteBruto = ImporteLinea * (this.Valor / 100);
                        DescuentosCabeceraGeneral = DescuentosCabeceraGeneral + ImporteBruto;
                    }
                    else {
                        DescuentosCabeceraGeneral = DescuentosCabeceraGeneral + (this.Valor / iNumerosLinea);
                    }
                }
            });

            $.each(gDetallePedido.Costes, function () {
                if (this.Seleccionado == true) {
                    if (this.Operacion == "1") { //%
                        ImporteBruto = ImporteLinea * (this.Valor / 100);
                        CostesCabeceraGeneral = CostesCabeceraGeneral + ImporteBruto;
                    }
                    else {
                        CostesCabeceraGeneral = CostesCabeceraGeneral + (this.Valor / iNumerosLinea);
                    }
                }
            });

            if (this.Est !== 99) {
                DescuentosLinea = 0;
                ImporteCosteLinea = parseFloat($.trim($('#lblImpBruto' + this.ID + '_hdd').text()).replace(",", "."));
                Impuestos = parseFloat($.trim($('#lblImpTotalCuotas_hdd').text()).replace(",", "."));

                $.each(this.Descuentos, function () {

                    if (this.Seleccionado == true) {

                        if (this.Operacion == "1") { //%
                            ImporteBruto = ImporteCosteLinea * (this.Valor / 100);
                            DescuentosLinea = DescuentosLinea + ImporteBruto;
                        }
                        else {
                            DescuentosLinea = DescuentosLinea + this.Valor;
                        }
                    }
                });
                ImporteCosteLinea = ImporteCosteLinea - DescuentosLinea
                //¿Veo en emision q los descuentos/costes cabecera NO cuentan para la base Imponible? Ejemplo: 2018/41/41 scrum
                //- DescuentosCabeceraGeneral + CostesCabeceraGeneral; 
                $.each(this.Impuestos, function () {
                    var ImpuestoAct = ImporteCosteLinea * (this.Valor / 100);
                    if ((EstaDeBaja) && (!gDetallePedido.DatosGenerales.BajaLogica && !bConBajas)) {
                        ImpuestoAct = 0;
                        ImporteCosteLinea = 0;
                    }
                    if ($('#cuota_' + this.Id + '_' + this.Valor + '_hdd').length == 0) {
                        Impuestos = Impuestos + ImpuestoAct;
                        AgregarCampo(this.Id, this.Denominacion + " " + this.Valor + "%", ImpuestoAct, "L", Linea, ImporteCosteLinea, idConcepto, this.Valor);
                    }
                    else {
                        ImpuestoCuota = parseFloat($.trim($('#cuota_' + this.Id + '_' + this.Valor + '_hdd').text()).replace(",", "."));
                        Impuestos = Impuestos - ImpuestoCuota;
                        ImpuestoAct = ImpuestoAct + ImpuestoCuota;
                        Impuestos = Impuestos + ImpuestoAct;
                        MostrarCampo(this.Id, this.Valor);
                        $('#cuota_' + this.Id + '_' + this.Valor + '_hdd').text(ImpuestoAct.toString());
                        $('#cuota_' + this.Id + '_' + this.Valor).text(formatNumber(ImpuestoAct.toString()) + " " + sMoneda);
                        totalConcepto = ImpuestoAct * (100 / this.Valor);
                        agregarCampoCuota(this.Id, idConcepto, this.Denominacion + " " + this.Valor + "%", "L", Linea, ImporteCosteLinea, totalConcepto, this.Valor);
                    }
                    $('#lblImpTotalCuotas_hdd').text(Impuestos);
                });
            }

        });

        //Impuestos Costes Lineas Catalogo

        var ImporteBruto;
        var TipoCosteDescuento, Seleccionado;
        idConcepto = 0;
        Linea = 0;

        $.each(gLineas, function () {
            Linea++;
            idConcepto = this.ID;
            EstaDeBaja = this.EstaDeBaja;

            if (this.Est !== 99) {
                ImporteCosteLinea = parseFloat($.trim($('#lblImpBruto' + this.ID + '_hdd').text()).replace(",", "."));
                $.each(this.Costes, function () {
                    if (this.Seleccionado == true) {
                        if (this.Operacion == "1") { //%
                            ImporteCosteLinea = ImporteCosteLinea * (this.Valor / 100);
                        }
                        else {
                            ImporteCosteLinea = this.Valor;
                        }
                        Impuestos = parseFloat($.trim($('#lblImpTotalCuotas_hdd').text()).replace(",", "."));
                        TipoCosteDescuento = this.TipoCosteDescuento;
                        Seleccionado = this.Seleccionado;
                        $.each(this.Impuestos, function () {
                            if (Seleccionado == true) {
                                var ImpuestoAct = ImporteCosteLinea * (this.Valor / 100);
                                if ((EstaDeBaja) && (!gDetallePedido.DatosGenerales.BajaLogica && !bConBajas)) {
                                    ImpuestoAct = 0;
                                    ImporteCosteLinea = 0;
                                }
                                if ($('#cuota_' + this.Id + '_' + this.Valor + '_hdd').length == 0) {
                                    Impuestos = Impuestos + ImpuestoAct;
                                    AgregarCampo(this.Id, this.Denominacion + " " + this.Valor + "%", ImpuestoAct, "LC", Linea, ImporteCosteLinea, idConcepto, this.Valor);
                                }
                                else {
                                    ImpuestoCuota = parseFloat($.trim($('#cuota_' + this.Id + '_' + this.Valor + '_hdd').text()).replace(",", "."));
                                    Impuestos = Impuestos - ImpuestoCuota;
                                    ImpuestoAct = ImpuestoAct + ImpuestoCuota;
                                    Impuestos = Impuestos + ImpuestoAct;
                                    MostrarCampo(this.Id, this.Valor);
                                    $('#cuota_' + this.Id + '_' + this.Valor + '_hdd').text(ImpuestoAct.toString());
                                    $('#cuota_' + this.Id + '_' + this.Valor).text(formatNumber(ImpuestoAct.toString()) + " " + sMoneda);
                                    totalConcepto = ImpuestoAct * (100 / this.Valor);
                                    agregarCampoCuota(this.Id, idConcepto, this.Denominacion + " " + this.Valor + "%", "LC", Linea, ImporteCosteLinea, totalConcepto, this.Valor);
                                }
                                $('#lblImpTotalCuotas_hdd').text(Impuestos);
                            }
                        });
                    }
                });
            }
        });

    }
    //<param name="bConBajas">Indica si hay que incluir las lÃ­neas de baja en el cÃ¡lculo</param>
    function recalcularImportes(bConBajas) {
        var impBruto = bConBajas ? parseFloat($.trim($('#lblImpBrutoLineaConBajas_hdd').text()).replace(",", ".")) : parseFloat($.trim($('#lblImpBrutoLinea_hdd').text()).replace(",", "."));
        var coste = parseFloat($.trim($('#lblCosteCabeceraTotal_hdd').text()).replace(",", "."));
        var descuentos = parseFloat($.trim($('#lblDescuentosCabeceraTotal_hdd').text()).replace(",", "."));
        var costesLinea = parseFloat($.trim($('#lblTotalCostesLinea_hdd').text()).replace(",", "."));
        impBruto = impBruto + costesLinea;

        var descuentosLinea = parseFloat($.trim($('#lblTotalDescuentosLinea_hdd').text()).replace(",", "."));
        impBruto = impBruto - descuentosLinea;


        var cuotasPedido = parseFloat($.trim($('#lblImpTotalCuotas_hdd').text()).replace(",", "."));
        var valor = impBruto + coste - descuentos + cuotasPedido;

        if (valor != undefined) {
            //Indicamos el importe de la solicitud(instancia)
            $("#lblImporte").text(formatNumber(valor) + " " + sMoneda)
        }

        $('#lblImpBruto_hdd').text(impBruto);
        $('#lblImpBruto').text(formatNumber(impBruto) + " " + sMoneda);

        $('#lblImporteTotalCabecera').text(formatNumber(valor) + " " + sMoneda);
        $('#lblImporteTotalCabecera_hdd').text(valor);

    }
    function recalcularLineaCosteDescuento() {
        var idLinea;
        var bCoste, bDescuento;
        var ImporteDescuento, ImporteCoste;
        var iLinea;
        iLinea = 0;
        ImporteDescuento = 0;
        ImporteCoste = 0;
        bCoste = false;
        bDescuento = false;
        gLineas = gDetallePedido.Lineas;
        var EstaDeBaja = false;
        $.each(gLineas, function () {
            idLinea = this.ID;
            EstaDeBaja = this.EstaDeBaja;
            $('#lblDescuentoTotalLinea_' + idLinea + '_hdd').text("0");
            $('#lblCosteTotalLinea_' + idLinea + '_hdd').text("0");
            if (this.Costes.length !== 0) {
                cargarCostesGrid(this);
                bCoste = true;

                var ImporteFijo = parseFloat($.trim($('#lblCosteTotalLinea_' + this.ID + '_hdd').text()).replace(",", "."));
                this.Costes.Importe = ImporteFijo;
                ImporteCoste = ImporteCoste + ImporteFijo;
                if (!EstaDeBaja || gDetallePedido.DatosGenerales.BajaLogica) { $('#lblTotalCostesLinea_hdd').text(ImporteCoste); }
            }
            else {
                $('#lblCosteTotalLinea_' + this.ID).text(formatNumber(0) + " " + sMoneda);
                $('#lblCosteTotalLinea_' + this.ID + '_hdd').text("0");
            }
            if (this.Descuentos.length !== 0) {
                cargarDescuentosGrid(this);
                bDescuento = true;

                var ImporteFijo = parseFloat($.trim($('#lblDescuentoTotalLinea_' + this.ID + '_hdd').text()).replace(",", "."));
                this.Descuentos.Importe = ImporteFijo;
                ImporteDescuento = ImporteDescuento + ImporteFijo;
                if (!EstaDeBaja || gDetallePedido.DatosGenerales.BajaLogica) { $('#lblTotalDescuentosLinea_hdd').text(ImporteDescuento); }
            }
            else {
                $('#lblDescuentoTotalLinea_' + this.ID).text(formatNumber(0) + " " + sMoneda);
                $('#lblDescuentoTotalLinea_' + this.ID + '_hdd').text("0");
            }
            if (iLinea == 0) {
                iLinea = iLinea + 1;
                if (this.Pres5_ImportesImputados_EP[0] !== undefined) {
                    ComprobarGestorLinea(this.Pres5_ImportesImputados_EP[0].Partida.NIV0); //this.Pres5_ImportesImputados[0].);
                }
            }
        });
        recalcularImportes(false);

        if (bCoste !== true) {
            $('#pesCostesLinea').css("display", "none");
        }
        if (bDescuento !== true) {
            $('#pesDescuentosLinea').css("display", "none");
        }

    }
    //////CARGA PESTAÑAS CABECERA DE PEDIDO//////
    $('#pesDatosGenerales').live('click', function () {
        $('#pesDatosGenerales').addClass("escenarioSeleccionado");
        $('#pesArObs').removeClass("escenarioSeleccionado");
        $('#pesOtrosDatos').removeClass("escenarioSeleccionado");
        $('#pesCostes').removeClass("escenarioSeleccionado");
        $('#pesDescuentos').removeClass("escenarioSeleccionado");
        $('#DatosGenerales').css("display", "block");
        $('#ArchivosObser').css("display", "none");
        $('#OtrosDatos').hide();
        $('#Costes').hide();
        $('#Descuentos').hide();
    });
    $('#pesArObs').live('click', function () {
        $('#pesDatosGenerales').removeClass("escenarioSeleccionado");
        $('#pesArObs').addClass("escenarioSeleccionado");
        $('#pesOtrosDatos').removeClass("escenarioSeleccionado");
        $('#pesCostes').removeClass("escenarioSeleccionado");
        $('#pesDescuentos').removeClass("escenarioSeleccionado");

        $('#DatosGenerales').css("display", "none");
        $('#ArchivosObser').css("display", "block");
        $('#OtrosDatos').hide();
        $('#Costes').hide();
        $('#Descuentos').hide();

        if (gDetallePedido == null) {
            if ($("#textObser").val() == "") {
                cargarObservaciones();
            }
        } else {
            if ($("#textObser").val() == "") {
                var info = gDetallePedido.DatosGenerales;
                $("#textObser").val(info.Obs);
            };
        };
        cargarArchivos();
    });
    $('#pesOtrosDatos').live('click', function () {
        $('#pesDatosGenerales').removeClass("escenarioSeleccionado");
        $('#pesArObs').removeClass("escenarioSeleccionado");
        $('#pesOtrosDatos').addClass("escenarioSeleccionado");
        $('#pesCostes').removeClass("escenarioSeleccionado");
        $('#pesDescuentos').removeClass("escenarioSeleccionado");

        $('#DatosGenerales').css("display", "none");
        $('#ArchivosObser').css("display", "none");
        $('#OtrosDatos').show();
        $('#Costes').hide();
        $('#Descuentos').hide();
        if (gAtributos == null) {
            if ($('#tmplComboEP').length == 0) {
                $.when($.get(ruta + '/App_Pages/EP/html/detallepedido.tmpl.htm', function (templates) {
                    $('body').append(templates);
                })).done(function () {
                    CargarAtributosOrden();
                });
            } else CargarAtributosOrden();
        }
    });
    $('#pesCostes').live('click', function () {
        $('#pesDatosGenerales').removeClass("escenarioSeleccionado");
        $('#pesArObs').removeClass("escenarioSeleccionado");
        $('#pesOtrosDatos').removeClass("escenarioSeleccionado");
        $('#pesCostes').addClass("escenarioSeleccionado");
        $('#pesDescuentos').removeClass("escenarioSeleccionado");

        $('#DatosGenerales').css("display", "none");
        $('#ArchivosObser').css("display", "none");
        $('#OtrosDatos').hide();
        $('#Costes').show();
        $('#Descuentos').hide();

        cargarCostes();
    });
    $('#pesDescuentos').live('click', function () {
        $('#pesDatosGenerales').removeClass("escenarioSeleccionado");
        $('#pesArObs').removeClass("escenarioSeleccionado");
        $('#pesOtrosDatos').removeClass("escenarioSeleccionado");
        $('#pesDescuentos').addClass("escenarioSeleccionado");
        $('#pesCostes').removeClass("escenarioSeleccionado");

        $('#DatosGenerales').css("display", "none");
        $('#ArchivosObser').css("display", "none");
        $('#OtrosDatos').hide();
        $('#Costes').hide();
        $('#Descuentos').show();

        cargarDescuentos();
    });
    $('#pesResumenLineas').live('click', function () {
        if ($('#pesCostesLinea').hasClass("escenarioSeleccionado")) guardarCostesLinea();
        if ($('#pesDescuentosLinea').hasClass("escenarioSeleccionado")) guardarDescuentosLinea();
        $('#pesResumenLineas').addClass("escenarioSeleccionado");
        $('#pesDatosLineas').removeClass("escenarioSeleccionado");
        $('#pesCostesLinea').removeClass("escenarioSeleccionado");
        $('#pesDescuentosLinea').removeClass("escenarioSeleccionado");
        $('#pesPlanesEntrega').removeClass("escenarioSeleccionado");
        $('#ResumenLineas').css("display", "block");
        $('#DatosLinea,#CostesLinea,#DescuentosLinea,#PlanesEntrega').hide();
    });
    $('#pesDatosLineas').live('click', function () {
        if ($('#pesCostesLinea').hasClass("escenarioSeleccionado")) guardarCostesLinea();
        if ($('#pesDescuentosLinea').hasClass("escenarioSeleccionado")) guardarDescuentosLinea();
        if (iCodFilaSeleccionada == 0) {
            alert(TextosJScript[1]);
        } else {
            $('#pesDatosLineas').addClass("escenarioSeleccionado");
            $('#pesResumenLineas').removeClass("escenarioSeleccionado");
            $('#pesCostesLinea').removeClass("escenarioSeleccionado");
            $('#pesDescuentosLinea').removeClass("escenarioSeleccionado");
            $('#pesPlanesEntrega').removeClass("escenarioSeleccionado");
            $('#ResumenLineas,#CostesLinea,#DescuentosLinea,#PlanesEntrega').hide();
            $('#DatosLinea').show();
            cargarLineaPedido();
            if ($('#tmplComboEP').length == 0) {
                $.when($.get(ruta + '/App_Pages/EP/html/detallepedido.tmpl.htm', function (templates) {
                    $('body').append(templates);
                })).done(function () {
                    //$('#tmplComboEP').tmpl(this).appendTo($('#tblAtributosItem'));
                    cargarAtributosLinea();
                });
            } else {
                if (Linea.OtrosDatos == undefined) {
                    cargarAtributosLinea();
                }
                else {
                    if (Linea.OtrosDatos.length == 0) cargarAtributosLinea();
                    else cargarAtributosLineaSinConsulta();
                }
            }
        }
    });
    $('#pesCostesLinea').live('click', function () {
        if ($('#pesDescuentosLineas').hasClass("escenarioSeleccionado")) guardarDescuentosLinea();
        if (iCodFilaSeleccionada == 0) {
            alert(TextosJScript[1]);
        } else {
            $('#pesDatosLineas').removeClass("escenarioSeleccionado");
            $('#pesResumenLineas').removeClass("escenarioSeleccionado");
            $('#pesCostesLinea').addClass("escenarioSeleccionado");
            $('#pesDescuentosLinea').removeClass("escenarioSeleccionado");
            $('#pesPlanesEntrega').removeClass("escenarioSeleccionado");
            $('#ResumenLineas,#DatosLinea,#DescuentosLinea,#PlanesEntrega').hide();
            $('#CostesLinea').show();
            cargarCostesLinea();
        }
    });
    $('#pesDescuentosLinea').live('click', function () {
        if ($('#pesCostesLineas').hasClass("escenarioSeleccionado")) guardarCostesLinea();
        if (iCodFilaSeleccionada == 0) {
            alert(TextosJScript[1]);
        } else {
            $('#pesDatosLineas').removeClass("escenarioSeleccionado");
            $('#pesResumenLineas').removeClass("escenarioSeleccionado");
            $('#pesCostesLinea').removeClass("escenarioSeleccionado");
            $('#pesDescuentosLinea').addClass("escenarioSeleccionado");
            $('#pesPlanesEntrega').removeClass("escenarioSeleccionado");
            $('#ResumenLineas,#DatosLinea,#CostesLinea,#PlanesEntrega').hide();
            $('#DescuentosLinea').show();
            cargarDescuentosLinea();
        }
    });
    $('#pesPlanesEntrega').live('click', function () {
        if ($('#pesPlanesEntrega').hasClass("escenarioSeleccionado")) return;
        if ($('#pesCostesLineas').hasClass("escenarioSeleccionado")) guardarCostesLinea();
        if ($('#pesDescuentosLineas').hasClass("escenarioSeleccionado")) guardarDescuentosLinea();
        if (iCodFilaSeleccionada == 0) {
            alert(TextosJScript[1]);
        } else {
            var lineaPlanEntrega = $.grep(gLineas, function (x) { return (x.ID == iCodFilaSeleccionada) })[0];
            $('#pesDatosLineas,#pesResumenLineas,#pesCostesLinea,#pesDescuentosLinea').removeClass("escenarioSeleccionado");
            $('#pesPlanesEntrega').addClass("escenarioSeleccionado");
            $('#lblCabeceraPlanEntregaCantidadImporteEntrega').text((lineaPlanEntrega.TipoRecepcion == 0 ? TextosJScript[50] : TextosJScript[51]) + '(' + lineaPlanEntrega.UP + ')');            

            $('#chkRecepcionAutomatica').prop('checked', lineaPlanEntrega.ConRecepcionAutomatica);
            $('#tblPlanesEntrega tbody').empty();
            var templatePlanEntrega = $.map(JSON.parse(JSON.stringify(lineaPlanEntrega.PlanEntrega)), function (x) {
                x.FechaEntrega = $.datepicker.formatDate(UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'), new Date(Date.parse(x.FechaEntrega)));
                if (lineaPlanEntrega.TipoRecepcion == 0) x.CantidadEntrega = formatNumber(x.CantidadEntrega);
                else x.ImporteEntrega = formatNumber(x.ImporteEntrega);
                return x;
            });
            if ($('#trPlanEntregaCantidad').length == 0) {
                $.when($.get(ruta + '/App_Pages/EP/html/detallepedido.tmpl.htm', function (templates) {
                    $('body').append(templates);
                })).done(function () {
                    if (lineaPlanEntrega.TipoRecepcion == 0) $('#trPlanEntregaCantidad').tmpl(templatePlanEntrega).appendTo('#tblPlanesEntrega tbody');
                    else $('#trPlanEntregaImporte').tmpl(templatePlanEntrega).appendTo('#tblPlanesEntrega tbody');
                });
            } else {
                if (lineaPlanEntrega.TipoRecepcion == 0) $('#trPlanEntregaCantidad').tmpl(lineaPlanEntrega.PlanEntrega).appendTo('#tblPlanesEntrega tbody');
                else $('#trPlanEntregaImporte').tmpl(lineaPlanEntrega.PlanEntrega).appendTo('#tblPlanesEntrega tbody');
            }

            $('#ResumenLineas,#DatosLinea,#CostesLinea,#DescuentosLinea').hide();
            $('#PlanesEntrega').show();
            var lSeleccionada = $.grep(gDetallePedido.Lineas, function (x) { return (x.ID == iCodFilaSeleccionada) })[0];
            var srtLinea, cabLinea;
            srtLinea = ("00" + iFilaSeleccionada.toString()).slice(-3);
            if (bMostrarCodigoArt) cabLinea = srtLinea + " - " + lSeleccionada.Cod + " - " + lSeleccionada.Den;
            else cabLinea = srtLinea + " - " + lSeleccionada.Den;
            $('#lblLineaPlanesEntrega').text(cabLinea);
        }
    });
    function cargarCostes() {
        if ($('#tmplCampoCosteFijo').length == 0) {
            $.when($.get(ruta + '/App_Pages/EP/html/detallepedido.tmpl.htm', function (templates) {
                $('body').append(templates);
            })).done(function () {
                CargarAtributosCoste();
                var Importe = $('#lblCosteTotal_hdd').text();
                $('#lblCosteCabeceraTotal_hdd').text(Importe);
                $('#lblCosteCabeceraTotal').text(formatNumber(Importe) + " " + sMoneda);
            });
        } else {
            CargarAtributosCoste();
            var Importe = $('#lblCosteTotal_hdd').text();
            $('#lblCosteCabeceraTotal_hdd').text(Importe);
            $('#lblCosteCabeceraTotal').text(formatNumber(Importe) + " " + sMoneda);
        }
    }
    function pintarIVA(Impuestos, filaSel, origen, importeCoste) {

        $.each(Impuestos, function () {
            this.Seleccionado = filaSel;
            this.Importe = importeCoste * (this.Valor / 100);
            this.Moneda = sMoneda;
            this.NomCampo = origen + "_" + this.Id;
            $("#IVA").tmpl(this).appendTo("#tblAtributosCoste");
        });
    }
    function pintarIVALinea(Impuestos, filaSel, origen, importeCoste) {

        $.each(Impuestos, function () {
            this.Seleccionado = filaSel;
            this.Importe = importeCoste * (this.Valor / 100);
            this.Moneda = sMoneda;
            this.NomCampo = origen + "_" + this.Id;
            $("#IVA").tmpl(this).appendTo("#tblAtributosCosteLinea");
        });
    }
    function cargarDescuentos() {
        if (gDescu == null) {
            if ($('#tmplCampoDescuReadOnly').length == 0) {
                $.when($.get(ruta + '/App_Pages/EP/html/detallepedido.tmpl.htm', function (templates) {
                    $('body').append(templates);
                })).done(function () {
                    CargarAtributosDescuento();
                });
            } else { CargarAtributosDescuento(); }
        }
    };
    function cargarLineaPedido() {
        gLineas = gDetallePedido.Lineas;
        var EstaDeBaja = false;
        $.each(gLineas, function () {
            if (this.ID == iCodFilaSeleccionada) {
                Linea = this;
                var srtLinea, cabLinea;
                srtLinea = ("00" + iFilaSeleccionada.toString()).slice(-3);
                if (bMostrarCodigoArt == true) cabLinea = srtLinea + " - " + this.Cod + " - " + this.Den;
                else cabLinea = srtLinea + " - " + this.Den;
                $('#lblLinea').text(cabLinea);
                $('#lblDestino').text(this.Dest + "-" + this.DestDen);
                $('#LblAlmacenDato').text(this.Almacen);
                $('#lblCategoriaDato').text(this.CatRama);
                $('#txtObservacionesLinea').val(this.Obs);
                $('#txtObservacionesLinea').prop('disabled', true);
                cargarArchivosLinea();
                $('#Fila5Tabla1').show()
                if (this.Pres5_ImportesImputados_EP !== null) {
                    if (this.Pres5_ImportesImputados_EP[0] !== undefined) {
                        var CentroContrato = this.Pres5_ImportesImputados_EP[0];
                        if (CentroContrato.CentroCoste !== undefined) {
                            var CentroCoste = CentroContrato.CentroCoste;
                            if (CentroCoste == undefined) {
                                $('#tbCtroCoste').text("");
                                $('#tbCtroCoste_Hidden').val("");
                                $('#Fila5Tabla1').hide()
                            }
                            else {
                                var CentroSM = CentroCoste.CentroSM;
                                if ((CentroCoste.UON1 !== "" && CentroCoste.UON1 !== null) && CentroSM.Denominacion !== "") {
                                    var CentroCoste_visible = CentroCoste.UON1;
                                    var CentroCoste_hidden = CentroCoste.UON1;
                                    if (CentroCoste.UON2 !== "" && CentroCoste.UON2 !== null) {
                                        var CentroCoste_visible = CentroCoste.UON2;
                                        var CentroCoste_hidden = CentroCoste_hidden + "@@" + CentroCoste.UON2;
                                        if (CentroCoste.UON3 !== "" && CentroCoste.UON3 !== null) {
                                            var CentroCoste_visible = CentroCoste.UON3;
                                            var CentroCoste_hidden = CentroCoste_hidden + "@@" + CentroCoste.UON3;
                                            if (CentroCoste.UON4 !== "" && CentroCoste.UON4 !== null) {
                                                var CentroCoste_visible = CentroCoste.UON4;
                                                var CentroCoste_hidden = CentroCoste_hidden + "@@" + CentroCoste.UON4;
                                            }
                                        }
                                    }
                                    $('#tbCtroCoste').text(CentroCoste_visible + " - " + CentroSM.Denominacion);
                                    $('#tbCtroCoste_Hidden').val(CentroSM.Codigo + "@@" + CentroCoste_hidden);
                                }
                                else {
                                    $('#tbCtroCoste').text("");
                                    $('#tbCtroCoste_Hidden').val("");
                                    $('#Fila5Tabla1').hide()
                                }
                            }

                            var Contrato = CentroContrato.Partida;
                            if (Contrato == undefined) {
                                $('#tbPPres').text("");
                                $('#tbPPres_Hidden').val("");
                                $('#Fila5Tabla1').hide()
                            }
                            else {
                                if ((Contrato.NIV0 !== "" && Contrato.NIV0 !== null) && (Contrato.Denominacion !== "" && Contrato.Denominacion !== null)) {
                                    var Contrato_hidden = Contrato.NIV0;
                                    var Contrato_visible = Contrato.NIV0;
                                    if (Contrato.NIV1 !== "" && Contrato.NIV1 !== null) {
                                        Contrato_hidden = Contrato_hidden + "@@" + Contrato.NIV1;
                                        Contrato_visible = Contrato.NIV1_Den;
                                        if (Contrato.NIV2 !== "" && Contrato.NIV2 !== null) {
                                            Contrato_hidden = Contrato_hidden + "@@" + Contrato.NIV2;
                                            Contrato_visible = Contrato.NIV2_Den;
                                            if (Contrato.NIV3 !== "" && Contrato.NIV3 !== null) {
                                                Contrato_hidden = Contrato_hidden + "@@" + Contrato.NIV3;
                                                Contrato_visible = Contrato.NIV3_Den;
                                                if (Contrato.NIV4 !== "" && Contrato.NIV4 !== null) {
                                                    Contrato_hidden = Contrato_hidden + "@@" + Contrato.NIV4;
                                                    Contrato_visible = Contrato.NIV4_Den;
                                                }
                                            }
                                        }
                                    }
                                    var fechaIni = eval("new " + Contrato.FechaInicioPresupuesto.slice(1, -1));
                                    var strFechaIni = fechaIni.format(UsuMask);
                                    var fechaFin = eval("new " + Contrato.FechaFinPresupuesto.slice(1, -1));
                                    var strFechaFin = fechaFin.format(UsuMask);
                                    Contrato_visible = Contrato_visible + " - " + Contrato.Denominacion + " - (" + strFechaIni + " - " + strFechaFin + ")";
                                    $('#tbPPres').text(Contrato_visible);
                                    $('#tbPPres_Hidden').val(Contrato_hidden);
                                    $('#lblPartidaPresupuestaria').show()
                                    $('#lblPartidaPresupuestaria').text(Contrato.Denominacion)
                                }
                                else {
                                    $('#tbPPres').text("");
                                    $('#tbPPres_Hidden').val("");
                                    $('#Fila5Tabla1').hide()
                                }
                            }
                        }
                        else {
                            $('#tbPPres').text("");
                            $('#tbPPres_Hidden').val("");
                            $('#tbCtroCoste').text("");
                            $('#tbCtroCoste_Hidden').val("");
                            $('#Fila5Tabla1').hide()
                        }
                    }
                    else {
                        $('#tbPPres').text("");
                        $('#tbPPres_Hidden').val("");
                        $('#tbCtroCoste').text("");
                        $('#tbCtroCoste_Hidden').val("");
                        $('#Fila5Tabla1').hide()
                    }
                }
                if (this.CentroAprovisionamiento) {
                    $("#lblTituloCentro").text(this.CentroAprovisionamiento);
                }
                var pres = $.map(this.Presupuestos, function (e) {
                    return { name: e.den, value: e.partidas };
                });
                $('#divPartidas').empty();
                if (pres.length > 0) {
                    $.each(pres, function () {
                        var partida = "<td><span class='Etiqueta'>" + this.name + " : </span></td>";
                        if (this.value) {
                            partida += "<td>";
                            $.each(this.value, function () {
                                partida += this.den + "(" + this.porcen * 100 + " %)" + ","
                            });
                            if (partida.length > 0) partida = partida.substring(0, partida.length - 1);
                            partida += "</td>";
                            $("#divPartidas").append(partida);
                        }
                    });
                }
                return false;
            }
        });
    }
    function guardarCostesLinea() {
        for (var i = 0; i < gLineas.length; i++) {
            if (gLineas[i].ID == Linea.ID) {
                gLineas[i].Costes = gCostesLinea;
                break;
            }
        }
        gDetallePedido.Lineas = gLineas;
    }
    function guardarDescuentosLinea() {
        for (var i = 0; i < gLineas.length; i++) {
            if (gLineas[i].ID == Linea.ID) {
                gLineas[i].Descuentos = gDescuentosLinea;
                break;
            }
        }
        gDetallePedido.Lineas = gLineas;
    }
    //funcion que carga los campos de pedido(atributos) de una linea de pedido sin llamar a un web method
    function cargarAtributosLineaSinConsulta() {
        var indAtributo = 0;

        if (Linea.OtrosDatos.length == 0) { //Si no hay atributos ocultamos el Div de Atributos    
            $('[id$=divAtributosLinea]').hide();
        } else {
            $('[id$=divAtributosLinea]').show();
        }

        //Se eliminan las filas si las hubiese de la tabla(menos la cabecera)
        var table = $("#tblAtributosLinea")
        table.find('tr:not(:first)').remove();

        $.each(Linea.OtrosDatos, function () {
            indAtributo += 1
            switch (this.Tipo.toString()) {
                case "1": //Texto
                    $("#tmplCampoLineaReadOnly").tmpl(this).appendTo("#tblAtributosLinea");
                    break
                case "2": //Numerico
                    $("#tmplCampoLineaReadOnlyNumerico").tmpl(this).appendTo("#tblAtributosLinea");
                    break
                case "3": //Fecha
                    $("#tmplCampoLineaReadOnly").tmpl(this).appendTo("#tblAtributosLinea");
                    var item = this;
                    var dateFormat;
                    var fechaAtrib;
                    dateFormat = UsuMask.replace('MM', 'mm');
                    if (item.Valor != null && item.Valor != undefined) {
                        if (item.Valor.toString().indexOf('Date') != -1) //Viene de BD en milisegundos
                            fechaAtrib = eval("new " + item.Valor.slice(1, -1));
                        else
                            fechaAtrib = eval("new Date('" + item.Valor + "')");
                        $('#spanAtribLineaEP_' + this.Id).text(fechaAtrib.format(dateFormat))
                    } else {
                        $('#spanAtribLineaEP_' + this.Id).text("")
                    }
                    break
                case "4": //Boolean
                    $("#tmplCampoLineaReadOnly").tmpl(this).appendTo("#tblAtributosLinea");
                    var item = this;
                    if (item.Valor == 1)
                        $('#spanAtribLineaEP_' + this.Id).text(TextosBoolean[1])
                    else if (item.Valor == 0)
                        $('#spanAtribLineaEP_' + this.Id).text(TextosBoolean[0])
                    break

            }

        })
    }
    function pintarDescuentosLinea() {
        var indAtributo = 0;
        var ImporteFijo = 0;

        if (gDescuentosLinea.length == 0) { //Si no hay atributos ocultamos el Div de Atributos    
            $('[id$=divAtributosDescuentoLinea]').hide();
            $('#pesResumenLineas').click();
        }
        else {
            $('[id$=divAtributosDescuentoLinea]').show();
        }
        //Se eliminan las filas si las hubiese de la tabla(menos la cabecera)
        var table = $("#tblAtributosDescuentoLinea")
        table.find('tr:not(:first)').remove();
        var GrupoCosteDescuentoAnt = "";
        var Inicio = "0";
        $.each(gDescuentosLinea, function () {
            indAtributo += 1;
            if (this.Operacion == "0") { //-
                if (this.Valor == undefined) {
                    var Importe = 0;
                }
                else {
                    // Ya he aplicado antes el cambio
                    var Importe = this.Valor;
                }
            }
            else {
                if (this.Valor == undefined) {
                    var Importe = 0;
                }
                else {
                    var Importe = parseFloat($('#lblImpBruto' + iCodFilaSeleccionada.toString() + '_hdd').text().replace(",", "."));
                    Importe = Importe * (this.Valor / 100);
                }
            }

            this.Importe = Importe;

            if (this.GrupoCosteDescuento == undefined) {
                var GrupoCosteDescuento = "";
            }
            else {
                var GrupoCosteDescuento = this.GrupoCosteDescuento;
                //this.GrupoCosteDescuento = GrupoCosteDescuento;
            }
            if (this.Inicio == undefined) {
                if (GrupoCosteDescuentoAnt !== "" && this.GrupoCosteDescuento !== undefined) {
                    if (this.GrupoCosteDescuento == GrupoCosteDescuentoAnt) {
                        Inicio = "1";
                    }
                    else {
                        Inicio = "0";
                    }
                }
                else {
                    Inicio = "0";
                }
            }
            else {
                Inicio = this.Inicio;
            }
            this.Inicio = Inicio;
            if (this.Seleccionado == undefined) {
                //            if (this.TipoCosteDescuento == 0) {
                //                var Seleccionado = true;
                //                this.Seleccionado = Seleccionado;
                //            }
                //            else {
                var Seleccionado = false;
                this.Seleccionado = Seleccionado;
                //   }
            }
            this.Moneda = sMoneda;
            GrupoCosteDescuentoAnt = GrupoCosteDescuento;

            $("#tmplCampoDescuLineaReadOnly").tmpl(this).appendTo("#tblAtributosDescuentoLinea");
            ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);

        })
        var impBruto = parseFloat($.trim($('#lblImpBruto_hdd').text()).replace(",", "."));
        //    $('#lblDescuentoTotalLinea_' + linea.ID + '_hdd').text(ImporteFijo);
        //    $('#lblDescuentoTotalLinea_' + linea.ID).text(formatNumber(ImporteFijo) + " " + Moneda);
        $('#lblDescuentoTotalLinea_hdd').text(ImporteFijo);
        $('#lblDescuentoTotalLinea').text(formatNumber(ImporteFijo) + " " + sMoneda);
        /*$('#lblTotalDescuentosLinea_hdd').text(ImporteFijo);
    
            impBruto = impBruto - ImporteFijo;
    
            $('#lblImpBruto').text(impBruto);
            $('#lblImpBruto_hdd').text(impBruto);*/
    };
    function EliminarCampo(id, valorImpCouta) {
        $('#descCuota_' + id + '_' + valorImpCouta).hide();
    }
    function MostrarCampo(id, valorImpCouta) {
        $('#descCuota_' + id + '_' + valorImpCouta).show();
    }
    function AgregarCampo(id, den, valor, tipo, linea, importeBruto, idConcepto, valorImp) {
        var datos =
        [{
            id: id,
            den: den,
            valor: valor,
            sMoneda: sMoneda,
            importeBruto: importeBruto,
            baseImponible: TextosJScript[36],
            valorImp: valorImp
        }];
        var desc;
        if (tipo == "C") {
            desc = TextosJScript[4]; //"Costes"
        }
        else {
            if (tipo == "LC") {
                desc = TextosJScript[4] + " " + TextosJScript[37]; //"IVA Coste Linea"
            }
            else {
                desc = TextosJScript[38]; //"Importe Bruto Linea"
            }
        }
        var datosBaseCabecera =
        [{
            id: id,
            importeTotal: importeBruto,
            den: den,
            valorImp: valorImp
        }];
        var datosBase =
        [{
            id: idConcepto,
            den: den,
            desc: desc,
            linea: linea,
            importeBruto: importeBruto,
            sMoneda: sMoneda,
            valorImp: valorImp
        }];
        $('#CabeceraIVA').tmpl(datos).appendTo($('#ImportesPedido'));
        $('#CabeceraInformacionIVA').tmpl(datosBaseCabecera).appendTo($('#descCuota_' + id + '_' + valorImp));
        $('#InformacionIVA').tmpl(datosBase).appendTo($('#baseImponible_' + id + '_' + valorImp));
    }
    function agregarCampoCuota(id, idConcepto, den, tipo, linea, importeBruto, importeBase, valorImp) {//, iva)
        var desc;
        if (tipo == "C") {
            desc = TextosJScript[4]; //"Costes"
        }
        else {
            if (tipo == "LC") {
                desc = TextosJScript[4] + " " + TextosJScript[37]; //"IVA Coste Linea"
            }
            else {
                desc = TextosJScript[38]; //"Importe Bruto Linea"
            }
        }
        var datosBase = [{
            id: idConcepto,
            den: den,
            desc: desc,
            linea: linea,
            importeBruto: importeBruto,
            sMoneda: sMoneda,
            valorImp: valorImp
        }];
        $('#lblImporteBase_' + id + '_' + valorImp).text(formatNumber(importeBase.toString()) + " " + sMoneda);
        $('#totalcuota_' + id + '_' + valorImp).text(formatNumber(importeBase.toString()) + " " + sMoneda);

        $('#InformacionIVA').tmpl(datosBase).appendTo($('#baseImponible_' + id + '_' + valorImp));
    }
    function btnAceptarRechazoLinea_Click() {
        $('#img_Rechazar_' + gidLineaRechazo).attr("src", ruta + "/App_Themes/" + tema + "/images/rechaz_enable.gif")
        $('#img_Aprobar_' + gidLineaRechazo).attr("src", ruta + "/App_Themes/" + tema + "/images/Aprob_disable.gif")
        gLineas = gDetallePedido.Lineas
        $.each(gLineas, function () {
            if (this.ID == gidLineaRechazo) {
                this.Est = 20
                this.MotivoRechazo = $('#txtCausaDenegacionLinea').val();
                $find('mpePanelRechazoLinea').hide();
                if (gDesde == 2)
                    ComprobarDenegacionLinea(this.ID, this.MotivoRechazo);
            };
        });
    }
    //funcion que cambia el estado de la linea a denegada
    //idLinea: id de la linea de pedido
    //desde: 1 Desde aprobacion de pedido por superar limite de pedido,2 Desde aprobacion de pedido por superar limite de adjudicacion, 3 desde seguimiento
    function DenegarLineaPedido(idLinea, desde) {
        gidLineaRechazo = idLinea
        gDesde = desde
        $.each(gLineas, function () {
            if (this.ID == gidLineaRechazo) {
                this.Est = 20
                if (this.MotivoRechazo != null && this.MotivoRechazo != undefined) {
                    $('#txtCausaDenegacionLinea').val(this.MotivoRechazo);
                    $('#txtCausaDenegacionLinea').html(this.MotivoRechazo);
                } else {
                    $('#txtCausaDenegacionLinea').val('');
                    $('#txtCausaDenegacionLinea').html('');
                }
            };
        });
        $find('mpePanelRechazoLinea').show();
    };
    //funcion que cambia el estado de la linea a aprobada
    //idLinea: id de la linea del pedido
    //desde: 1 Desde aprobacion de pedido por superar limite de pedido,2 Desde aprobacion de pedido por superar limite de adjudicacion, 3 desde seguimiento
    function AprobarLineaPedido(idLinea, desde) {
        $('#img_Rechazar_' + idLinea).attr("src", ruta + "/App_Themes/" + tema + "/images/rechaz_disable.gif")
        $('#img_Aprobar_' + idLinea).attr("src", ruta + "/App_Themes/" + tema + "/images/Aprob_enable.gif")

        gDesde = desde
        gLineas = gDetallePedido.Lineas
        $.each(gLineas, function () {
            if (this.ID == idLinea) {
                this.Est = 1;
                if (desde == 2) ComprobarAprobacionLinea(idLinea);
            };
        });
    };
    ////////////////////////////////////////
    ///LLAMADA A WEB METHODS DE LA PAGINA///
    ////////////////////////////////////////
    //funcion que cuando se apruebe una linea por limite de adjudicacion compruebe el estado de la linea y como quedara la orden
    function ComprobarAprobacionLinea(idLinea) {
        var params = JSON.stringify({ Aprobar: true, sIdLinea: idLinea, sIdOrden: sID, iIdPedido: IdPedido, iTipoPedido: TipoPedido, iIdEmpresa: IdEmpresa, Motivo: '', instanciaAprob: instanciaAprob, sMoneda: sMoneda, iTipo: gDatosGenerales.TipoPedido });
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/ComprobarAprobacionDenegacionLinea',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        })).done(function (msg) {
            var idMensaje = parseInt(msg.d);
            if (idMensaje !== 0) {
                if (idMensaje == 10) mostrarOcultarMensaje(TextosAlert[0]);
                else if (idMensaje != 8) mostrarOcultarMensaje(TextosAlert[idMensaje]);
            };
        });
    }
    //funcion que cuando se deniege una linea por limite de adjudicacion compruebe el estado de la linea y como quedara la orden
    function ComprobarDenegacionLinea(idLinea, motivo) {
        var params = JSON.stringify({ Aprobar: false, sIdLinea: idLinea, sIdOrden: sID, iIdPedido: IdPedido, iTipoPedido: TipoPedido, iIdEmpresa: IdEmpresa, Motivo: motivo, instanciaAprob: instanciaAprob, sMoneda: sMoneda, iTipo: gDatosGenerales.TipoPedido });
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/ComprobarAprobacionDenegacionLinea',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        })).done(function (msg) {
            var idMensaje = parseInt(msg.d);
            if (idMensaje !== 0) {
                if (idMensaje == 10) mostrarOcultarMensaje(TextosAlert[0]);
                else if (idMensaje != 8) mostrarOcultarMensaje(TextosAlert[idMensaje]);
            };
        });
    }
    function cargarLineas() {
        var bCoste, bDescuento;
        var ImporteDescuento, ImporteCoste;
        var iLinea;
        iLinea = 0;
        ImporteDescuento = 0;
        ImporteCoste = 0;
        bCoste = false;
        bDescuento = false;
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/Cargar_Lineas',
            contentType: "application/json; charset=utf-8",
            data: "{'sIden':'" + sID + "','sInstanciaAprob':'" + instanciaAprob + "'}",
            dataType: "json",
            async: false
        })).done(function (msg) {
            gLineas = $.parseJSON(msg.d);
            gDetallePedido.Lineas = gLineas;
            var EstaDeBaja = false;
            $.each(gLineas, function () {
                var xLinea = this;
                var idLinea = this.ID;
                EstaDeBaja = this.EstaDeBaja;
                if (this.Costes.length !== 0) {
                    cargarCostesGrid(this);
                    bCoste = true;

                    var ImporteFijo = parseFloat($.trim($('#lblCosteTotalLinea_' + this.ID + '_hdd').text()).replace(",", "."));
                    this.Costes.Importe = ImporteFijo;
                    ImporteCoste = ImporteCoste + ImporteFijo;
                    if (!EstaDeBaja || gDetallePedido.DatosGenerales.BajaLogica) { $('#lblTotalCostesLinea_hdd').text(ImporteCoste); }

                } else {
                    $('#lblCosteTotalLinea_' + this.ID).text(formatNumber(0) + " " + sMoneda);
                    $('#lblCosteTotalLinea_' + this.ID + '_hdd').text("0");
                }
                if (this.Descuentos.length !== 0) {
                    cargarDescuentosGrid(this);
                    bDescuento = true;

                    var ImporteFijo = parseFloat($.trim($('#lblDescuentoTotalLinea_' + this.ID + '_hdd').text()).replace(",", "."));
                    this.Descuentos.Importe = ImporteFijo;
                    ImporteDescuento = ImporteDescuento + ImporteFijo;
                    if (!EstaDeBaja || gDetallePedido.DatosGenerales.BajaLogica) { $('#lblTotalDescuentosLinea_hdd').text(ImporteDescuento); }
                } else {
                    $('#lblDescuentoTotalLinea_' + this.ID).text(formatNumber(0) + " " + sMoneda);
                    $('#lblDescuentoTotalLinea_' + this.ID + '_hdd').text("0");
                }
                if (iLinea == 0) {
                    iLinea = iLinea + 1;
                    if (this.Pres5_ImportesImputados_EP[0] !== undefined) ComprobarGestorLinea(this.Pres5_ImportesImputados_EP[0].Partida.NIV0);
                };
                $.each(this.PlanEntrega, function () {                                  
                    this.FechaEntrega = eval("new " + this.FechaEntrega.slice(1, -1));
                });
            });
            recalcularImportes(false);
        });
        if (bCoste !== true) $('#pesCostesLinea').css("display", "none");
        if (bDescuento !== true) $('#pesDescuentosLinea').css("display", "none");
    };
    function cargarDatosGenerales() {
        OcultarRelanzar();
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/Cargar_Datos_Generales',
            contentType: "application/json; charset=utf-8",
            data: "{'sIden':'" + sID + "'}",
            dataType: "json",
            async: false
        })).done(function (msg) {

            gDetallePedido = $.parseJSON(msg.d);
            gDatosGenerales = gDetallePedido.DatosGenerales;
            IdPedido = gDetallePedido.IdPedido;
            IdEmpresa = gDatosGenerales.IdEmpresa;
            TipoPedido = gDatosGenerales.Tipo.Id;
            sMoneda = gDatosGenerales.Moneda;
            gIdLog = gDatosGenerales.IdLogGral;
            gIdEntidad = gDatosGenerales.IdEntidad;
            gErp = gDatosGenerales.IdErp;

            if (bMostrarCodigoProve == true) {
                $('#FSNLinkProve').text(gDatosGenerales.CodProve + " - " + gDatosGenerales.DenProve);
            }
            else {
                $('#FSNLinkProve').text(gDatosGenerales.DenProve);
            }
            $('#FSNLinkProve').val(gDatosGenerales.CodProve);
            $('#FSNLinkProve').addClass("LinkButtonInfo");
            var fechaAtrib;
            var fechaEstado;

            if (gDatosGenerales.BajaLogica) {
                if (gDatosGenerales.FechaBajaLogica.toString().indexOf('Date') != -1) //Viene de BD en milisegundos
                    fechaEstado = eval("new " + gDatosGenerales.FechaBajaLogica.slice(1, -1));
                else
                    fechaEstado = eval("new Date('" + gDatosGenerales.FechaBajaLogica + "')");
                fechaEstado = fechaEstado.format(UsuMask + ' hh:mm:ss');
            } else {
                if (gDatosGenerales.FechaEstadoOrden != null && gDatosGenerales.FechaEstadoOrden != undefined) {
                    if (gDatosGenerales.FechaEstadoOrden.toString().indexOf('Date') != -1) //Viene de BD en milisegundos
                        fechaAtrib = eval("new " + gDatosGenerales.FechaEstadoOrden.slice(1, -1));
                    else
                        fechaAtrib = eval("new Date('" + gDatosGenerales.FechaEstadoOrden + "')");
                    fechaEstado = fechaAtrib.format(UsuMask);
                } else {
                    fechaEstado = "";
                }
            }

            var estOrden = "";
            var sClass = "LinkButtonInfo";
            if (gDatosGenerales.BajaLogica) {
                sClass = "LineaPedidoBajaLogica";
                estOrden = TextosJScript[64];
            } else {
                switch (gDatosGenerales.EstadoOrden) {
                    case 1:
                        estOrden = TextosJScript[39];
                        $("#divPageHeaderBotonCierrePositivo").hide();
                        break;
                    case 2:
                        estOrden = TextosJScript[40];
                        break;
                    case 4:
                        estOrden = TextosJScript[41];
                        break;
                    case 8:
                        estOrden = TextosJScript[42];
                        break;
                    case 16:
                        estOrden = TextosJScript[43];
                        break;
                    case 32:
                        estOrden = TextosJScript[44];
                        break;
                    case 64:
                        estOrden = TextosJScript[45];
                        break;
                    case 128:
                        estOrden = TextosJScript[46];
                        break;
                    case 256:
                        estOrden = TextosJScript[47];
                        break;
                    case 512:
                        estOrden = TextosJScript[48];
                        break;
                }
            }
            $('#EstadoOrden').text(estOrden + " (" + fechaEstado + ")");
            $('#EstadoOrden').val(estOrden + " (" + fechaEstado + ")");
            $('#EstadoOrden').removeClass();
            $('#EstadoOrden').addClass(sClass);
            $('#LblTipoPedidoDato').text(gDatosGenerales.Tipo.Denominacion);
            $('#TxtBxPedSAP').val(gDatosGenerales.Referencia);
            if (gDatosGenerales.DirEnvioDen !== "") {
                $('#lblCodDireccion').text(gDatosGenerales.DirEnvio);
                $('#lblDireccionEnvioFacturaDato').text(gDatosGenerales.DirEnvioDen);
            }
            else {
                $('#lblDireccionEnvioFacturaDato').text('');
                $('#lblCodDireccion').text("0");
                $('#lblDireccionEnvioFactura').css("display", "none");
                $('#lblDireccionEnvioFacturaDato').css("display", "none");
                $('#lblCodDireccion').css("display", "none");
            }
            idRecept = gDatosGenerales.Receptor;
            if (sHayIntegracionPedidos == "1") {
                var estOrdenIntegracion = "";
                
                switch (gDatosGenerales.EstadoIntegracionOrden) {
                    
                    case 0:
                        estOrdenIntegracion = TextosJScript[65];
                        break;
                    case 1:
                        estOrdenIntegracion = TextosJScript[66];
                        break;
                    case 3:
                        estOrdenIntegracion = TextosJScript[67];
                        $('#EstadoOrdenIntegracion').css("color", "red");
                        $('#divbtnRelanzar').show();
                        break;
                    case 4:
                        estOrdenIntegracion = TextosJScript[68];;
                        break;
                }
                $('#EstadoOrdenIntegracion').text(estOrdenIntegracion);

                $('#LblProveErpDato').css("display", "inline");
                $('#LblProveErp').css("display", "block");
                $('#LblProveErpDato').text(gDatosGenerales.CodErp);

                $.when($.ajax({
                    type: "POST",
                    url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarProveedoresERP',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{'lEmpresa':'" + gDatosGenerales.IdEmpresa + "','sProve':'" + gDatosGenerales.CodProve + "', 'sCodERP':'" + gDatosGenerales.CodigoErp + "'}",
                    async: true
                })).done(function (msg) {
                    if ($.parseJSON(msg.d[1]).length == 0) {
                        $('#btnInfo').hide();
                    }
                    else {
                        $('#btnInfo').show();
                    }
                });

            }
            else {
                $('#LblProveErpDato').css("display", "none");
                $('#LblProveErp').css("display", "none");
                $('#btnInfo').hide();
            }
            if (gDatosGenerales.Tipo.Id !== 0) {
                $('#pesOtrosDatos').css("display", "inline-block");
            }
            else {
                $('#pesOtrosDatos').css("display", "none");
            }
            $("#lblSolicAcepProveDato").text(gDatosGenerales.OESolicitAceptacionProveedor ? TextosJScript[52] : TextosJScript[53]);
            $("#textObser").val(gDatosGenerales.Obs);
            $("#LblEmpresaDato").text(gDatosGenerales.Emp)
            $("#LblReceptorDato").text(gDatosGenerales.Receptor)
            $("#LblGestorCab").text(gDatosGenerales.Gestor)

            $("#LblNumSapDato").text(gDatosGenerales.Referencia)
            //Tarea 3497 - Pedidos abiertos
            if (gDatosGenerales.TipoPedido == 6) {//Si el pedido es contra pedido abierto (tipo 6) mostramos el código del pedido del que viene y ocultamos los datos de categoría
                $("#lblCodPedidoAbierto").text(gDatosGenerales.CodPedidoAbierto);
                $("#trCategoria").hide();
            } else {
                $("#trPedidoAbierto").hide();
            }
        });
    }
    function cargarArchivos() {
        if (gDetallePedido.Adjuntos == null || gDetallePedido.Adjuntos.length == 0) {
            $.ajax({
                type: "POST",
                url: ruta + '/App_Pages/EP/DetallePedido.aspx/Cargar_Archivos',
                contentType: "application/json; charset=utf-8",
                data: "{'sIden':'" + sID + "'}",
                dataType: "json",
                async: false
            }).done(function (msg) {
                $("#tablaAdjuntos").html("");
                gArchivos = $.parseJSON(msg.d);
                gDetallePedido.Adjuntos = gArchivos;
                if ($('#tmplComboEP').length == 0) {
                    $.when($.get(ruta + '/App_Pages/EP/html/detallepedido.tmpl.htm', function (templates) {
                        $('body').append(templates);
                    })).done(function () {
                        $('#filaDocumento').tmpl(gArchivos).appendTo($('#tablaAdjuntos'));
                    });
                } else { $('#filaDocumento').tmpl(gArchivos).appendTo($('#tablaAdjuntos')); }
            });
        }
    }
    function cargarArchivosLinea() {
        var gArchivosLinea = null;
        $("#divArchivosLinea").html("");
        if (Linea.Adjuntos == null || Linea.Adjuntos.length == 0) {
            $.ajax({ 
                type: "POST",
                url: ruta + '/App_Pages/EP/DetallePedido.aspx/Cargar_Archivos_Linea',
                contentType: "application/json; charset=utf-8",
                data: "{'sIdenLinea':'" + Linea.ID + "'}",
                dataType: "json",
                async: false
            }).done(function (msg) {
                gArchivosLinea = $.parseJSON(msg.d);
                if ($('#tmplComboEP').length == 0) {
                    $.when($.get(ruta + '/App_Pages/EP/html/detallepedido.tmpl.htm', function (templates) {
                        $('body').append(templates);
                    })).done(function () {
                        $('#filaDocumento').tmpl(gArchivosLinea).appendTo($('#divArchivosLinea'));
                    });
                } else { $('#filaDocumento').tmpl(gArchivosLinea).appendTo($('#divArchivosLinea')); }
            });
        }
    }
    function cargarObservaciones() {
        $.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/Cargar_Datos_Generales',
            contentType: "application/json; charset=utf-8",
            data: "{'sIden':'" + sID + "'}",
            dataType: "json",
            async: false
        }).done(function (msg) {
            var info = $.parseJSON(msg.d);
            gDatosGenerales = info.DatosGenerales;
            $("#textObser").val(info.Obs);
        });
    }
    //funcion que carga los campos de pedido a nivel de cabecera
    function CargarAtributosOrden() {
        var indAtributo = 0;

        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/CargarAtributosOrden',
            contentType: "application/json; charset=utf-8",
            data: "{'sIDOrden':'" + sID + "'}",
            dataType: "json",
            async: false
        })).done(function (msg) {
            gAtributos = $.parseJSON(msg.d);
            gDetallePedido.OtrosDatos = gAtributos;
            if (gAtributos.length == 0) { //Si no hay atributos ocultamos el Div de Atributos    
                $('[id$=divAtributosOrden]').hide();
                $('#pesDatosGenerales').click();
            }
            else {
                $('[id$=divAtributosOrden]').show();
            }
        
            //Se eliminan las filas si las hubiese de la tabla(menos la cabecera)
            var table = $("#tblAtributosOrden")
            table.find('tr:not(:first)').remove();

            $.each(gAtributos, function () {
                indAtributo += 1;

                switch (this.Tipo.toString()) {
                    case "1": //Texto
                        $("#tmplCampoReadOnly").tmpl(this).appendTo("#tblAtributosOrden");
                        break
                    case "2": //Numerico
                        $("#tmplCampoReadOnlyNumerico").tmpl(this).appendTo("#tblAtributosOrden");
                        break
                    case "3": //Fecha
                        $("#tmplCampoReadOnly").tmpl(this).appendTo("#tblAtributosOrden");
                        var item = this;
                        var dateFormat;
                        var fechaAtrib;
                        dateFormat = UsuMask.replace('MM', 'mm');
                        if (item.Valor != null && item.Valor != undefined) {
                            if (item.Valor.toString().indexOf('Date') != -1) //Viene de BD en milisegundos
                                fechaAtrib = eval("new " + item.Valor.slice(1, -1));
                            else
                                fechaAtrib = eval("new Date('" + item.Valor + "')");
                        }
                        $('#spanAtribEP_' + this.Id).text(fechaAtrib.format(dateFormat))
                        break
                    case "4": //Boolean
                        $("#tmplCampoReadOnly").tmpl(this).appendTo("#tblAtributosOrden");
                        var item = this;
                        if (item.Valor == 1)
                            $('#spanAtribEP_' + this.Id).text(TextosBoolean[1])
                        else if (item.Valor == 0)
                            $('#spanAtribEP_' + this.Id).text(TextosBoolean[0])
                        break

                }
            });
        });
    }
    //funcion que carga los atributos a nivel de Item(Panel edicion Item)
    function cargarAtributosLinea() {
        var indAtributo = 0;

        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/CargarAtributosLinea',
            contentType: "application/json; charset=utf-8",
            data: "{'iCodLinea':'" + iCodFilaSeleccionada.toString() + "'}",
            dataType: "json",
            async: false
        })).done(function (msg) {
            gAtributosLinea = $.parseJSON(msg.d);
            Linea.OtrosDatos = gAtributosLinea;
            if (gAtributosLinea.length == 0) { //Si no hay atributos ocultamos el Div de Atributos    
                $('[id$=divAtributosLinea]').hide();
            }else {
                $('[id$=divAtributosLinea]').show();
            }

            //Se eliminan las filas si las hubiese de la tabla(menos la cabecera)
            var table = $("#tblAtributosLinea")
            table.find('tr:not(:first)').remove();

            $.each(gAtributosLinea, function () {
                indAtributo += 1

                switch (this.Tipo.toString()) {
                    case "1": //Texto
                        $("#tmplCampoLineaReadOnly").tmpl(this).appendTo("#tblAtributosLinea");
                        break
                    case "2": //Numerico
                        $("#tmplCampoLineaReadOnlyNumerico").tmpl(this).appendTo("#tblAtributosLinea");
                        break
                    case "3": //Fecha
                        $("#tmplCampoLineaReadOnly").tmpl(this).appendTo("#tblAtributosLinea");
                        var item = this;
                        var fechaAtrib;
                        if (item.Valor != null && item.Valor != undefined) {
                            if (item.Valor.toString().indexOf('Date') != -1) //Viene de BD en milisegundos
                                fechaAtrib = eval("new " + item.Valor.slice(1, -1));
                            else
                                fechaAtrib = eval("new Date('" + item.Valor + "')");
                            $('#spanAtribLineaEP_' + this.Id).text(fechaAtrib.format(UsuMask))
                        } else {
                            $('#spanAtribLineaEP_' + this.Id).text("")
                        }

                        break
                    case "4": //Boolean
                        $("#tmplCampoLineaReadOnly").tmpl(this).appendTo("#tblAtributosLinea");
                        var item = this;
                        if (item.Valor == 1)
                            $('#spanAtribLineaEP_' + this.Id).text(TextosBoolean[1])
                        else if (item.Valor == 0)
                            $('#spanAtribLineaEP_' + this.Id).text(TextosBoolean[0])
                        break

                }
            })
        });
    }
    //funcion que carga los costes de cabecera de la orden de entrega
    function CargarAtributosCoste() {
        var indAtributo = 0;
        var ImporteFijo = 0;


        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/CargarAtributosCostes',
            contentType: "application/json; charset=utf-8",
            data: "{'sIdOrden':'" + sID + "'}",
            dataType: "json",
            async: false
        })).done(function (msg) {
            gCostes = $.parseJSON(msg.d);
            gDetallePedido.Costes = gCostes;
            if (gCostes.length == 0) { //Si no hay atributos ocultamos el Div de Atributos    
                $('[id$=divAtributosCoste]').hide();
                $('#pesDatosGenerales').click();
            }
            else {
                $('[id$=divAtributosCoste]').show();
            }

            //Se eliminan las filas si las hubiese de la tabla(menos la cabecera)
            var table = $("#tblAtributosCoste")
            table.find('tr:not(:first)').remove();
            var GrupoCosteDescuentoAnt = "";
            var Inicio = "0";
            $.each(gCostes, function () {
                indAtributo += 1;
                if (this.Operacion == "0") { //+
                    if (this.Valor == undefined) {
                        var Importe = 0;
                    }
                    else {
                        this.Valor = this.Valor * dCambio;
                        var Importe = this.Valor;
                    }
                }
                else {
                    if (this.Valor == undefined) {
                        var Importe = 0;
                    }
                    else {
                        var Importe = parseFloat($('#lblImpBruto_hdd').text().replace(",", "."));
                        Importe = Importe * (this.Valor / 100);
                    }
                }
                if (this.Seleccionado == undefined) {
                    var Seleccionado = false;
                    this.Seleccionado = Seleccionado;
                }
                this.Importe = Importe;

                if (this.GrupoCosteDescuento == undefined) {
                    var GrupoCosteDescuento = "";
                }
                else {
                    var GrupoCosteDescuento = this.GrupoCosteDescuento;
                }
                if (this.Inicio == undefined) {
                    if (GrupoCosteDescuentoAnt !== "" && this.GrupoCosteDescuento !== undefined) {
                        if (this.GrupoCosteDescuento == GrupoCosteDescuentoAnt) {
                            Inicio = "1";
                        }
                        else {
                            Inicio = "0";
                        }
                    }
                    else {
                        Inicio = "0";
                    }
                }
                else {
                    Inicio = this.Inicio;
                }
                this.Inicio = Inicio;

                this.Moneda = sMoneda;
                GrupoCosteDescuentoAnt = GrupoCosteDescuento;

                var filaSel = false;
                //if (this.TipoCosteDescuento == 0) {
                filaSel = true;
                //} else {
                //filaSel = this.Seleccionado;
                //}

                $("#tmplCampoCosteReadOnly").tmpl(this).appendTo("#tblAtributosCoste");
                ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
                pintarIVA(this.Impuestos, filaSel, "fij" + this.Id, this.Importe);
            })
        });
        $('#lblCosteTotal_hdd').text(ImporteFijo);
        $('#lblCosteTotal').text(formatNumber(ImporteFijo) + " " + sMoneda);
    }
    //funcion que carga los descuentos de cabecera de la orden de entrega
    function CargarAtributosDescuento() {
        var indAtributo = 0;
        var ImporteFijo = 0;


        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/CargarAtributosDescuentos',
            contentType: "application/json; charset=utf-8",
            data: "{'sIdOrden':'" + sID + "'}",
            dataType: "json",
            async: false
        })).done(function (msg) {
            gDescu = $.parseJSON(msg.d);
            gDetallePedido.Descuentos = gDescu;
            if (gDescu.length == 0) { //Si no hay atributos ocultamos el Div de Atributos    
                $('[id$=divAtributosDescuento]').hide();
                $('#pesDatosGenerales').click();
            }
            else {
                $('[id$=divAtributosDescuento]').show();
            }

            //Se eliminan las filas si las hubiese de la tabla(menos la cabecera)
            var table = $("#tblAtributosDescuento")
            table.find('tr:not(:first)').remove();
            var GrupoCosteDescuentoAnt = "";
            var Inicio = "0";
            $.each(gDescu, function () {
                indAtributo += 1;
                if (this.Operacion == "0") { //-
                    if (this.Valor == undefined) {
                        var Importe = 0;
                    }
                    else {
                        this.Valor = this.Valor * dCambio;
                        var Importe = this.Valor;
                    }
                }
                else {
                    if (this.Valor == undefined) {
                        var Importe = 0;
                    }
                    else {
                        var Importe = parseFloat($('#lblImpBruto_hdd').text().replace(",", "."));
                        Importe = Importe * (this.Valor / 100);
                    }
                }

                this.Importe = Importe;

                if (this.GrupoCosteDescuento == undefined) {
                    var GrupoCosteDescuento = "";
                }
                else {
                    var GrupoCosteDescuento = this.GrupoCosteDescuento;
                }
                if (this.Inicio == undefined) {
                    if (GrupoCosteDescuentoAnt !== "" && this.GrupoCosteDescuento !== undefined) {
                        if (this.GrupoCosteDescuento == GrupoCosteDescuentoAnt) {
                            Inicio = "1";
                        }
                        else {
                            Inicio = "0";
                        }
                    }
                    else {
                        Inicio = "0";
                    }
                }
                else {
                    Inicio = this.Inicio;
                }
                this.Inicio = Inicio;
                if (this.Seleccionado == undefined) {
                    var Seleccionado = false;
                    this.Seleccionado = Seleccionado;
                }
                this.Moneda = sMoneda;
                GrupoCosteDescuentoAnt = GrupoCosteDescuento;

                $("#tmplCampoDescuReadOnly").tmpl(this).appendTo("#tblAtributosDescuento");
                ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);

            })
        });
        $('#lblDescuentoTotal_hdd').text(ImporteFijo);
        $('#lblDescuentoTotal').text(formatNumber(ImporteFijo) + " " + sMoneda);
        $('#lblDescuentosCabeceraTotal_hdd').text(ImporteFijo);
        $('#lblDescuentosCabeceraTotal').text(formatNumber(ImporteFijo) + " " + sMoneda);
    }
    function ComprobarGestorLinea(pres5) {
        var lblGestor = $('#LblGestorCab');
        if (lblGestor.text() == "") {
            $.when($.ajax({
                type: "POST",
                url: ruta + '/App_Pages/EP/DetallePedido.aspx/seleccionaGestorWM',
                contentType: "application/json; charset=utf-8",
                data: "{'Partida':'" + pres5 + "'}",
                dataType: "json",
                async: false
            })).done(function (msg) {
                var gestor = msg.d;
                if (gestor !== "") {
                    $('#LblGestorCab').text(gestor);
                }
                else {
                    $('#LblGestorCab').text("");
                }
            });
        }
    }
    function CargarAtributosCosteLinea() {
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/CargarAtributosCostesLinea',
            contentType: "application/json; charset=utf-8",
            data: "{'iCodLinea':'" + Linea.ID + "'}",
            dataType: "json",
            async: false
        })).done(function (msg) {
            gCostesLinea = $.parseJSON(msg.d);
            Linea.Costes = gCostesLinea;
            pintarCostesLinea();
        });

    }
    function pintarCostesLinea() {
        var indAtributo = 0;
        var ImporteFijo = 0;
        var ImporteFijoLinea = 0;

        if (gCostesLinea.length == 0) { //Si no hay atributos ocultamos el Div de Atributos    
            $('[id$=divAtributosCosteLinea]').hide();
            $('#pesResumenLineas').click();
        }
        else {
            $('[id$=divAtributosCosteLinea]').show();
        }

        //Se eliminan las filas si las hubiese de la tabla(menos la cabecera)
        var table = $("#tblAtributosCosteLinea")
        table.find('tr:not(:first)').remove();
        var GrupoCosteDescuentoAnt = "";
        var Inicio = "0";
        $.each(gCostesLinea, function () {
            ImporteFijoLinea = 0;
            indAtributo += 1;
            if (this.Operacion == "0") { //+
                if (this.Valor == undefined) {
                    var Importe = 0;
                }
                else {
                    // Ya he aplicado antes el cambio
                    var Importe = this.Valor;
                }
            }
            else {
                if (this.Valor == undefined) {
                    var Importe = 0;
                }
                else {
                    var Importe = parseFloat($('#lblImpBruto' + iCodFilaSeleccionada.toString() + '_hdd').text().replace(",", "."));
                    Importe = Importe * (this.Valor / 100);
                }
            }

            this.Importe = Importe;

            if (this.GrupoCosteDescuento == undefined) {
                var GrupoCosteDescuento = "";
            }
            else {
                var GrupoCosteDescuento = this.GrupoCosteDescuento;
                //this.GrupoCosteDescuento = GrupoCosteDescuento;
            }
            if (this.Inicio == undefined) {
                if (GrupoCosteDescuentoAnt !== "" && this.GrupoCosteDescuento !== undefined) {
                    if (this.GrupoCosteDescuento == GrupoCosteDescuentoAnt) {
                        Inicio = "1";
                    }
                    else {
                        Inicio = "0";
                    }
                }
                else {
                    Inicio = "0";
                }
            }
            else {
                Inicio = this.Inicio;
            }
            this.Inicio = Inicio;
            if (this.Seleccionado == undefined) {
                //            if (this.TipoCosteDescuento == 0) {
                //                var Seleccionado = true;
                //                this.Seleccionado = Seleccionado;
                //            }
                //            else {
                var Seleccionado = false;
                this.Seleccionado = Seleccionado;
                //            }
            }
            this.Moneda = sMoneda;
            GrupoCosteDescuentoAnt = GrupoCosteDescuento;

            var filaSel = true;

            $("#tmplCampoCosteLineaReadOnly").tmpl(this).appendTo("#tblAtributosCosteLinea");
            ImporteFijoLinea = parseFloat(ImporteFijoLinea) + parseFloat(this.Importe);
            ImporteFijo = parseFloat(ImporteFijo) + parseFloat(this.Importe);
            pintarIVALinea(this.Impuestos, filaSel, "fijLin" + this.Id, this.Importe);

        })

        $('#lblCosteTotalLinea_hdd').text(ImporteFijo);
        $('#lblCosteTotalLinea').text(formatNumber(ImporteFijo) + " " + sMoneda);
    }
    function CargarAtributosDescuentoLinea() {
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/CargarAtributosDescuentosLinea',
            contentType: "application/json; charset=utf-8",
            data: "{'iCodLinea':'" + Linea.ID + "'}",
            dataType: "json",
            async: false
        })).done(function (msg) {
            gDescuentosLinea = $.parseJSON(msg.d);
            Linea.Descuentos = gDescuentosLinea;
            pintarDescuentosLinea();
        });

    }
    function EjecutarAccion(idInstancia ,id, bloque, comp_obl, guarda, bloq, rechazo, tipoSolicitud) {
        gIdAccion = id;
        gIdBloque = bloque;
        gIdInstancia=idInstancia
        var params = JSON.stringify({ idAccion: id , tipoSolicitud: tipoSolicitud });
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/DevolverSiguientesEtapas',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        })).done(function (msg) {
            var obj = $.parseJSON(msg.d);
            var oSiguientesEtapas = obj.Etapas;
            var oRoles = obj.Roles;
            $("#lblCabeceraConfirmacion").text(obj.DenAccion)
            var table = $("#tblRoles")
            //mostramos la tabla roles ya que si va a una etapa(como FIN) que no tenga roles se debe ocultar la tabla y el label
            table.show()
            $("#lblRoles").show()
            //Se eliminan las filas si las hubiese de la tabla(menos la cabecera)
            table.find('tr:not(:first)').remove();
            table = $("#tblEtapas")
            table.find('tr').remove();
            if ($('#tmplCampoCosteLineaReadOnly').length == 0) {
                $.when($.get(ruta + '/App_Pages/EP/html/detallepedido.tmpl.htm', function (templates) {
                    $('body').append(templates);
                })).done(function () {
                    $.each(oSiguientesEtapas, function () {
                        $("#tmplSiguientesEtapas").tmpl(this).appendTo("#tblEtapas");
                    })
                    if (oRoles.length != 0) {
                        $.each(oRoles, function () {
                            $("#tmplRoles").tmpl(this).appendTo("#tblRoles");
                        })
                    } else {
                        $("#tblRoles").hide()
                        $("#lblRoles").hide()
                    }
                });
            } else {
                $.each(oSiguientesEtapas, function () {
                    $("#tmplSiguientesEtapas").tmpl(this).appendTo("#tblEtapas");
                })
                if (oRoles.length != 0) {
                    $.each(oRoles, function () {
                        $("#tmplRoles").tmpl(this).appendTo("#tblRoles");
                    })
                } else {
                    $("#tblRoles").hide()
                    $("#lblRoles").hide()
                }
            }
        });
        document.getElementById('divDetalleFactura').style.display = 'none';
        document.getElementById('divDevolverSiguientesEtapas').style.display = 'inline';
    }
    function VolverAlDetalle() {
        document.getElementById('divDetalleFactura').style.display = 'inline';
        document.getElementById('divDevolverSiguientesEtapas').style.display = 'none';
    }
    //Descripcion: Cuando se pulsa un boton para realizar una accion, se llama a esta funcion
    //Parametros: id: id de la accion
    // bloque: id del bloque
    // comp_olb: si hay que comprobar los campos de obligatorios o no
    // guarda: si hay que guardar o version o no
    // rechazo: indica si la accion implica un rechazo, ya sea temporal o definitivo
    function ConfirmarEnvio() {
        var gDetallePedidoGuardar;
        var gCentroCoste, gPartidas;
        var i = 0;
        var observaciones;
        var importeTotal;
        var importeSolicitud;

        importeTotal = $("#lblImporteTotalCabecera").text().replace(sMoneda, '').trim();
        importeTotal = strToNum(importeTotal);
        importeTotal = importeTotal / gDetallePedido.DatosGenerales.Cambio;
        importeSolicitud = $("#lblImporte").text().replace(sMoneda, '').trim();
        importeSolicitud = strToNum(importeSolicitud)
        observaciones=$("#txtComent").text()
        gCentroCoste = [];
        gPartidas = [];
        gDetallePedidoGuardar = jQuery.extend(true, {}, gDetallePedido);
        $.each(gDetallePedidoGuardar.Lineas, function () {
            i = i + 1;
            if (this.Pres5_ImportesImputados_EP[0] !== undefined) {
                this.Pres5_ImportesImputados_EP[0].CentroCoste.CentroSM.PartidasPresupuestarias = null;
                if (this.Pres5_ImportesImputados_EP[0].CentroCoste !== undefined)
                    gCentroCoste.push(JSON.parse(JSON.stringify(this.Pres5_ImportesImputados_EP[0].CentroCoste)));

                if (this.Pres5_ImportesImputados_EP[0].Partida !== undefined) {
                    if (this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto !== null) {
                        if (this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto.toString().toLowerCase().indexOf("date") >= 0) {
                            this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto = eval("new " + this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto.slice(1, -1));
                        }
                        else {
                            if (this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto.toString().toString() == "") {
                                this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto = null;
                            }
                        }
                    }
                    else {
                        this.Pres5_ImportesImputados_EP[0].Partida.FechaInicioPresupuesto = null;
                    }
                    if (this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto !== null) {
                        if (this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto.toString().toLowerCase().indexOf("date") >= 0) {
                            this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto = eval("new " + this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto.slice(1, -1));
                        }
                        else {
                            if (this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto.toString().toString() == "") {
                                this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto = null;
                            }
                        }
                    }
                    else {
                        this.Pres5_ImportesImputados_EP[0].Partida.FechaFinPresupuesto = null;
                    }
                    gPartidas.push(JSON.parse(JSON.stringify(this.Pres5_ImportesImputados_EP[0].Partida)));
                }
                this.Pres5_ImportesImputados_EP = [];
            } else {
                gCentroCoste.push(new Object());
                gPartidas.push(new Object());
                this.Pres5_ImportesImputados_EP = [];
            }
        });
        var tipoSolicitud=10;//Solicitud de pedido de catálogo

        if (gDatosGenerales.TipoPedido==6) {//si el pedido es contraabierto
            tipoSolicitud = 11;//Solicitud de pedido contra abierto
        }

        var params = JSON.stringify({ sDetallePedido: JSON.stringify(gDetallePedidoGuardar), sIdOrden: sID, oCentroCoste: JSON.stringify(gCentroCoste), oPartidas: JSON.stringify(gPartidas), sIdAccion: gIdAccion, sIdBloqueActual: gIdBloque, sObservaciones: observaciones, ImportePedido: importeTotal, ImporteSolicitud: importeSolicitud, idInstancia: gIdInstancia ,tipoSolicitud: tipoSolicitud });
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/EjecutarAccion',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        })).done(function (msg) {
            if (msg.d == true) {
                var newWindow = window.open(rutaFS + "EP/Aprobacion.aspx?CargarTodo=1","_top");
            }
        });
    };
    function ActualizarBotoneraHeader(result) {
        if (result.Reabrir) FSNHeader_MostrarBoton('BotonReabrir');
        else FSNHeader_OcultarBoton('BotonReabrir');
        if (result.Anular) FSNHeader_MostrarBoton('BotonAnular');
        else FSNHeader_OcultarBoton('BotonAnular');
        if (result.Cerrar) FSNHeader_MostrarBoton('BotonCierrePositivo');
        else FSNHeader_OcultarBoton('BotonCierrePositivo');
        if (result.Recepcionar) FSNHeader_MostrarBoton('BotonRecepcionar');
        else FSNHeader_OcultarBoton('BotonRecepcionar');
    };
    function ReabrirOrden() {
        var params = JSON.stringify({ IdOrden: sID, Aprobador: aprobador, Receptor: receptor, acepProve: (acepProve = 1 ? true : false) });
        MostrarCargando();
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/ReabrirOrden',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: true
        })).done(function (msg) {
            setTimeout(OcultarCargando(), 1000);
            var result = $.parseJSON(msg.d);
            ActualizarBotoneraHeader(result);
        });
    };
    function CerrarOrden() {
        
        var bCerrar = true;
        if (gDatosGenerales.Tipo.Recepcionable == 1) {
            //si el tipo de pedido es de Recepcion Obligatoria hay que comprobar que tenga todas las rececpciones para poder cerrarlo
            var params = JSON.stringify({ IdOrden: sID });
            $.when($.ajax({
                type: "POST",
                url: ruta + '/App_Pages/EP/DetallePedido.aspx/ComprobarCerrarOrden',
                contentType: "application/json; charset=utf-8",
                data: params,
                dataType: "json",
                async: false
            })).done(function (msg) {                
                if (msg.d == false) {                    
                    //no se puede cerrar porque el tipo de pedido es de recepción obligatoria pero alguna de las líneas no está recepcionada.
                    $('#lblChkConfirmError').hide();
                    $('#btnCancelar').hide();
                    $('#btnAceptar').show();
                    $('#lblConfirm').text(TextosAlert[51]);                    
                    $('#popupFondo').css('height', $(document).height());
                    $('#popupFondo').show();
                    CentrarPopUp($('#pnlConfirm'));
                    $('#pnlConfirm').show();                    
                    $('#btnAceptar').die('click');
                    $('#btnAceptar').live('click', function () {
                        $('#popupFondo').hide();
                        $('#pnlConfirm').hide();
                    });
                    bCerrar = false;
                };  
            });
        };        
        if (bCerrar==true) {
            var params = JSON.stringify({ IdOrden: sID, Aprobador: aprobador, Receptor: receptor, acepProve: (acepProve = 1 ? true : false), TipoPedidoRec: gDatosGenerales.Tipo.Recepcionable  });
            //Se puede cerrar
            $.when($.ajax({
                type: "POST",
                url: ruta + '/App_Pages/EP/DetallePedido.aspx/CerrarOrden',
                contentType: "application/json; charset=utf-8",
                data: params,
                dataType: "json",
                async: true
            })).done(function (msg) {
                setTimeout(OcultarCargando(), 1000);
                var result = $.parseJSON(msg.d);
                ActualizarBotoneraHeader(result);                       
                 
            });
        }        
    }

    //''' <summary>
    //''' Reemitir Orden
    //''' </summary>
    //''' <remarks>Llamada desde: DetallePedido.aspx.vb ; Tiempo máximo: 0,2</remarks>
    function ReemitirOrden() {
        var params = JSON.stringify({ IdOrden: sID, ProveCod: gDetallePedido.DatosGenerales.CodProve, PedidoId: gDetallePedido.IdPedido, iTipoPedido: TipoPedido, EmpresaId: IdEmpresa, iTipo: gDatosGenerales.TipoPedido, sMoneda: sMoneda });
        MostrarCargando();
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/ReemitirOrden',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: true
        })).done(function () {
            setTimeout(OcultarCargando(), 1000);
            window.location.href = rutaFS + 'EP/Seguimiento.aspx';
        });
    };
    function BorrarOrden() {        
        //A ComprobarBorrarOrden se le pasa una copia del objeto pedido para poder hacer las comprobaciones de integración. No hace falta pasar datos de imputación y estos dan error porque PartidasPres5 da problemas de serialización
        //Por ello hago una copia y elimino estos datos de la copia
        var oCopiaPedido = jQuery.extend(true, {}, gDetallePedido);
        $.each(oCopiaPedido.Lineas, function () {
            this.Pres5_ImportesImputados_EP = null;
        });
        var params = JSON.stringify({ sPedido: JSON.stringify(oCopiaPedido) });
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/ComprobarBorrarOrden',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        })).done(function (msg) {
            $('#chkConfirm').hide();
            $('#txtConfirm').hide();
            $('#lblChkConfirm').hide();
           
            if (isNaN(parseFloat(msg.d))) {
                //Error de integraciÃ³n
                $('#btnCancelar').hide();
                $('#lblConfirm').text(msg.d);

                $('#btnAceptar').die('click');
                $('#btnAceptar').live('click', function () {
                    $('#popupFondo').hide();
                    $('#pnlConfirm').hide();
                });
            } else {                
                $('#lblChkConfirmError').hide();
                switch (parseInt(msg.d)) {
                
                    case 0:
                        //Se puede borrar
                        $('#btnCancelar').show();

                        $('#lblConfirm').text(TextosAlert[24]);

                        $('#btnAceptar').die('click');
                        $('#btnAceptar').live('click', function () {
                            $('#popupFondo').hide();
                            $('#pnlConfirm').hide();

                            calcularTotalesInicio(true);

                            var dImporteTotalOrden = parseFloat($('#lblImpBruto_hdd').text().replace(",", "."));
                            var dImporteCostes = parseFloat($('#lblCosteCabeceraTotal_hdd').text().replace(",", "."));
                            var dImporteDescuentos = parseFloat($('#lblDescuentosCabeceraTotal_hdd').text().replace(",", "."));

                            var params = JSON.stringify({ OrdenId: sID, dImporteTotalOrden: dImporteTotalOrden, dImporteCostes: dImporteCostes, dImporteDescuentos: dImporteDescuentos })
                            $.when($.ajax({
                                type: "POST",
                                url: ruta + '/App_Pages/EP/DetallePedido.aspx/BorrarOrden',
                                contentType: "application/json; charset=utf-8",
                                data: params,
                                dataType: "json",
                                async: false
                            })).done(function (msg) {
                                if (msg.d) {
                                    alert(TextosAlert[25]);
                                    window.location.href = rutaFS + 'EP/Seguimiento.aspx';
                                }
                                else alert(TextosAlert[26]);
                            });
                        });

                        break;
                    case 1:
                        //Caso de pedido cerrado que no es abono ni no recepcionable. No se da el caso porque no se muestra el botón borrar                    
                    case 2:
                        //Caso de pedido con recepciones. No se da el caso porque no se muestra el botón borrar                    
                    case 3:
                        //Caso de pedido cerrado con facturas. No se da el caso porque no se muestra el botón borrar
                        break;
                    case 4:
                        //Caso de pedidos no cerrado con facturas no anuladas
                        $('#btnCancelar').hide();
                        $('#lblConfirm').text(TextosAlert[27]);

                        $('#btnAceptar').die('click');
                        $('#btnAceptar').live('click', function () {
                            $('#popupFondo').hide();
                            $('#pnlConfirm').hide();
                        });
                        break;
                    case 5:
                        //Caso de pedido abierto en estado cerrado. No se da el caso porque no se muestran pedidos abiertos                   
                    case 6:
                        //caso de pedido abierto con algún contraabierto no borrado ni anulado. No se da el caso porque no se muestran pedidos abiertos
                    case 7:
                        
                        $('#btnCancelar').hide();
                        $('#lblConfirm').text(TextosAlert[50]);

                        $('#btnAceptar').die('click');
                        $('#btnAceptar').live('click', function () {
                            $('#popupFondo').hide();
                            $('#pnlConfirm').hide();
                        });
                    case 8:
                        //Caso de pedido directo o negociado con hitos de facturación
                        $('#btnCancelar').hide();
                        $('#lblConfirm').text(TextosAlert[52]);

                        $('#btnAceptar').die('click');
                        $('#btnAceptar').live('click', function () {
                            $('#popupFondo').hide();
                            $('#pnlConfirm').hide();
                        });
                        break;
                }
            }
            
            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').show();
            CentrarPopUp($('#pnlConfirm'));
            $('#pnlConfirm').show();

            $('#btnAceptar').show();
        });
    }
    function DeshacerBorrarOrden() {
        var params = JSON.stringify({ IdOrden: sID, Estado: gDatosGenerales.Estado, TipoPedido: gDatosGenerales.TipoPedido });
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/ComprobarDeshacerBorrarOrden',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        })).done(function (msg) {
            $('#chkConfirm').hide();
            $('#txtConfirm').hide();
            $('#lblChkConfirm').hide();

            switch (parseInt(msg.d.MotivoNoDeshacerBorradoLogicoPedido)) {
                case 0:
                    //Se puede borrar                    
                    $('#lblConfirm').text(TextosAlert[46]);

                    if (gDatosGenerales.TipoPedido == 6) {
                        //Pedido contra pedido abierto
                        var sLineasADeshacer = "";
                        if (msg.d.LineasADeshacer.length > 0) {
                            if (msg.d.LineasADeshacer.slice(-1) == ",") sLineasADeshacer = msg.d.LineasADeshacer.substring(0, msg.d.LineasADeshacer.length - 1)
                        }
                        PrepararDeshacerBorrarOrden(sLineasADeshacer.split(","));
                    } else {
                        var oLineasADeshacer = new Array();
                        $.each(gDetallePedido.Lineas, function () {
                            if (this.EstaDeBaja) {
                                oLineasADeshacer.push(this.ID);
                            }
                        });

                        PrepararDeshacerBorrarOrden(oLineasADeshacer);
                    }

                    break;
                default:
                    $('#lblConfirm').text(MensajeNoDeshacerBorrarOrden(gDatosGenerales.Estado, parseInt(msg.d.MotivoNoDeshacerBorradoLogicoPedido), msg.d.LineasNoDeshacer));

                    if (parseInt(msg.d.MotivoNoDeshacerBorradoLogicoPedido) == 4 || parseInt(msg.d.MotivoNoDeshacerBorradoLogicoPedido) == 5) {
                        PrepararDeshacerBorrarOrden(msg.d.LineasADeshacer);
                    } else {
                        $('#btnCancelar').hide();
                        $('#btnAceptar').show();
                        $('#btnAceptar').die('click');
                        $('#btnAceptar').live('click', function () {
                            $('#popupFondo').hide();
                            $('#pnlConfirm').hide();
                        });
                    }
            }

            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').show();
            CentrarPopUp($('#pnlConfirm'));
            $('#pnlConfirm').show();
        });
    }
    function MensajeNoDeshacerBorrarOrden(Estado, Motivo, LineasNoDeshacer) {
        var sMensaje = "";

        switch (Motivo) {
            case 1:
                //NO SE PUEDE DESHACER BORRADO DE UN PEDIDO si el pedido tiene estado<2 (estados 0 o 1) o estado anulado 
                sMensaje = TextosAlert[40];

                switch (Estado) {
                    case 0:  //PendienteDeAprobacion
                        sMensaje = sMensaje.replace("@EST", TextosJScript[39]);
                        break;
                    case 1:  //DenegadoParcialAprob
                        sMensaje = sMensaje.replace("@EST", TextosJScript[40]);
                        break;
                    case 20:    //Anulado
                        sMensaje = sMensaje.replace("@EST", TextosJScript[46]);
                }
                break;
            case 2:
                //NO SE PUEDE DESHACER BORRADO DE UN PEDIDO tipo ContraPedidoAbierto Si el pedido abierto correspondiente estÃ¡ cerrado (o anulado o no vigente)                    
                sMensaje = TextosAlert[41];
                break;
            case 3:
                //NO SE PUEDE DESHACER BORRADO DE UN PEDIDO tipo ContraPedidoAbierto Si el pedido abierto correspondiente estÃ¡ borrado
                sMensaje = TextosAlert[42];
                break;
            case 4:
                //NO SE PUEDE DESHACER BORRADO DE UN PEDIDO tipo ContraPedidoAbierto Si se superarÃ¡ la cantidad de las lÃ­neas del abierto correspondiente                                            
            case 5:
                //NO SE PUEDE DESHACER BORRADO DE UN PEDIDO tipo ContraPedidoAbierto Si se superarÃ¡ el importe de las lÃ­neas del abierto correspondiente  
                sMensaje = TextosAlert[43] + "\r\n" + LineasNoDeshacer + "\r\n" + TextosAlert[44];
                break;
            case 41:
                //NO SE PUEDE DESHACER BORRADO DE UN PEDIDO tipo ContraPedidoAbierto Si se superarÃ¡ la cantidad de las lÃ­neas del abierto correspondiente. Ninguna fila es posible deshacer                
            case 51:
                //NO SE PUEDE DESHACER BORRADO DE UN PEDIDO tipo ContraPedidoAbierto Si se superarÃ¡ el importe de las lÃ­neas del abierto correspondiente. Ninguna fila es posible deshacer
                sMensaje = TextosAlert[45];
        }

        return sMensaje
    }
    function PrepararDeshacerBorrarOrden(LineasADeshacer) {
        //Se puede borrar
        $('#btnCancelar').show();
        $('#btnAceptar').show();
        $('#btnAceptar').die('click');  //Cuando se asocia un handler con la funciÃ³n live, para desasociarlo hay que usar die, no unbind
        $('#btnAceptar').live('click', function () {
            $('#popupFondo').hide();
            $('#pnlConfirm').hide();

            //Construir objeto con datos de la orden y de las lÃ­neas 
            var oDatosOrden = { IdOrden: gDetallePedido.DatosGenerales.Id, Lineas: new Array() };
            $.each(gLineas, function () {
                var ID = this.ID;
                if (jQuery.grep(LineasADeshacer, function (o) { return o == ID; }).length > 0) oDatosOrden.Lineas.push({
                    Id: this.ID, ImporteBruto: this.ImportePedido,
                    ImporteCostes: this.Costes.Importe ? this.Costes.Importe : 0, ImporteDescuentos: this.Descuentos.Importe ? this.Descuentos.Importe : 0
                });
            });

            var params = JSON.stringify({ sDatosOrden: JSON.stringify(oDatosOrden) })
            $.when($.ajax({
                type: "POST",
                url: ruta + '/App_Pages/EP/DetallePedido.aspx/DeshacerBorrarOrden',
                contentType: "application/json; charset=utf-8",
                data: params,
                dataType: "json",
                async: false
            })).done(function (msg) {
                if (msg.d) {
                    alert(TextosAlert[47]);
                    window.location.reload(true);
                }
                else alert(TextosAlert[48]);
            });
        });
    }
    function AnularOrden() {        
        var bAnular = false;
        var params = JSON.stringify({ IdOrden: sID, ProveCod: gDetallePedido.DatosGenerales.CodProve, Estado: gDatosGenerales.Estado, iTipoPedido: gDatosGenerales.TipoPedido });
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/ComprobarAnularOrden',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        })).done(function (msg) {
            $('#btnAceptar').show();
            $('#chkConfirm').prop('checked', false);
            $('#chkConfirm').hide();
            $('#txtConfirm').hide();
            $('#lblChkConfirm').hide();
            $('#btnCancelar').hide();
                              
            switch (parseInt(msg.d)) {
                case 1:
                    $('#lblConfirm').text(TextosAlert[10]);
                    $('#lblChkConfirmError').hide();                    
                    break;
                case 2:
                    $('#lblConfirm').text(TextosAlert[11]);
                    $('#lblChkConfirmError').hide();                    
                    break;
                case 3:
                    $('#lblConfirm').text(TextosAlert[49]);
                    $('#lblChkConfirmError').hide();                    
                    break;
                case 4:
                    $('#lblConfirm').text(TextosAlert[54]);
                    $('#lblChkConfirmError').hide();
                    break;
                default:
                    $('#lblConfirm').text(TextosAlert[12]);
                    $('#chkConfirm').show();                                        
                    $('#lblChkConfirm').text(TextosJScript[32]);
                    $('#lblChkConfirm').show();                   
                    $('#btnCancelar').show();
                    bAnular = true;
                    break;
            };

            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').show();
            CentrarPopUp($('#pnlConfirm'));
            $('#pnlConfirm').show();            
            if (bAnular == true) {
                $('#btnAceptar').die('click');
                $('#btnAceptar').live('click', function () {
                    $('#popupFondo').hide();
                    $('#pnlConfirm').hide();

                    if ($('#chkConfirm').prop('checked')) {
                        $.when($.ajax({
                            type: "POST",
                            url: ruta + '/App_Pages/EP/DetallePedido.aspx/AnularOrden',
                            contentType: "application/json; charset=utf-8",
                            data: params,
                            dataType: "json",
                            async: false
                        })).done(function (msg) {
                            if (parseInt(msg.d) == 0) {
                                alert(TextosAlert[13]);
                                window.location.href = rutaFS + 'EP/Seguimiento.aspx';
                            }
                            else alert(TextosAlert[14]);
                        });
                    } else {
                        
                        $('#lblChkConfirmError').text(TextosJScript[33]);
                        $('#lblChkConfirmError').visible();
                        $('#lblChkConfirmError').show();
                    };
                });
            }
            else
            {
                $('#btnAceptar').die('click');
                $('#btnAceptar').live('click', function () {
                    $('#popupFondo').hide();
                    $('#pnlConfirm').hide();
                });

            }

        });
    };
    $('#btnCancelar').live('click', function () {
        $('#chkConfirm').prop('checked', false);
        $('#chkConfirm').hide();
        $('#txtConfirm').hide();
        $('#lblChkConfirm').hide();           
        $('#popupFondo').hide();
        $('#pnlConfirm').hide();
    });
    $('#btnInfo').live('click', function () {

        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/EmisionPedido.aspx/CargarProveedoresERP',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: "{'lEmpresa':'" + gDatosGenerales.IdEmpresa + "','sProve':'" + gDatosGenerales.CodProve + "', 'sCodERP':'" + gDatosGenerales.CodigoErp + "'}",
            async: true
        })).done(function (msg) {
            if ($.parseJSON(msg.d[1]).length !== 0) {
                resultado = $.parseJSON(msg.d[1])[0];
                $("[id$=IdCodigo]").html(resultado.Cod)
                $("[id$=IdDescripcion]").html(resultado.Den)

                $("[id$=IdCampo1]").html(resultado.Campo1)
                $("[id$=IdCampo2]").html(resultado.Campo2)
                $("[id$=IdCampo3]").html(resultado.Campo3)
                $("[id$=IdCampo4]").html(resultado.Campo4)

                $('#pnlInfoProveERP').show();
            }
        });
    });
    //Función para mostrar la información del destino - GFA
    $('#ImgDestInfo').live('click', function () {
        var popup = $('#PnlDestInfo');
        var sSeleccionado = "";
        var params = { sID: $.map(gLineas, function (x) { if (x.ID == iCodFilaSeleccionada) return x; })[0].Dest,
            iCodLinea: iCodFilaSeleccionada
        };
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/CargarDatosDestinos',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(params),
            dataType: "json",
            async: false
        })).done(function (msg) {
            gDatosDestinos = $.parseJSON(msg.d);
            if (gDatosDestinos.length !== 0) {
                $('#lblDestInfoTituloCod').text(gDatosDestinos[0].Den);
                $('#txtDestInfoDir').text(gDatosDestinos[0].Direccion);
                $('#txtDestInfoCP').text(gDatosDestinos[0].CP);
                $('#txtDestInfoPob').text(gDatosDestinos[0].Pob);
                $('#txtDestInfoProv').text(gDatosDestinos[0].Provi);
                $('#txtDestInfoPais').text(gDatosDestinos[0].Pai);
                $('#txtDestInfoTfno').text(gDatosDestinos[0].Tfno);
                $('#txtDestInfoFAX').text(gDatosDestinos[0].FAX);
                $('#txtDestInfoEmail').text(gDatosDestinos[0].Email);
            };
            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').show();

            popup.css("width", "260");
            popup.css("height", "222");

            CentrarPopUp($('#PnlDestInfo'));
            $('#PnlDestInfo').show();
        });
    });
    $('#ImgDestInfoCerrar').live('click', function () {
        $('#PnlDestInfo').hide();
        $('#popupFondo').hide();
    });
    function Recepcionar() {
        var params = JSON.stringify({ IdOrden: sID,
            ProveCod: gDetallePedido.DatosGenerales.CodProve,
            Anyo: gDetallePedido.DatosGenerales.Anyo,
            NumOrden: gDetallePedido.DatosGenerales.NumOrden,
            NumPedido: gDetallePedido.DatosGenerales.NumPedido,
            VerPedidosUsuario: vpu,
            VerPedidosOtrosUsuarios: vpou
        });
        MostrarCargando();
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/Recepcionar',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: true
        })).done(function (msg) {
            window.location.href = rutaFS + 'EP/Recepcion.aspx';
        });
    };
    function OcultarCargando() {
        var modalprog = $find(ModalProgress);
        if (modalprog) modalprog.hide();
    };
/* GRID EVENTS */
    function DeshacerBorrarLineaPedido(IdLineaPedido) {       
        var result;
        var iRespuesta = 0;
        var LineasNoDeshacer;
        var LineasADeshacer;
        
        var params = JSON.stringify({ idLinea: IdLineaPedido, IdOrden: sID, OrdenEstado: gDatosGenerales.Estado, TipoPedido: gDatosGenerales.TipoPedido });
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/ComprobarDeshacerBorrarLinea',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        })).done(function (msg) {
            result = $.parseJSON(msg.d);
            iRespuesta = parseInt(result.Validacion);
            LineasNoDeshacer = result.LineasNoDeshacer;
            LineasADeshacer = result.LineasADeshacer;
        });
        
        if (iRespuesta !== 0) {
            $('#btnCancelar').hide();
            $('#btnAceptar').show();
            $('#btnAceptar').die('click');
            $('#btnAceptar').live('click', function () {
                $('#popupFondo').hide();
                $('#pnlConfirm').hide();
            });
            switch (iRespuesta) {
                case 1: 
                    {
                        var estOrden = "";
                        switch (gDatosGenerales.EstadoOrden) {
                            case 1:
                                estOrden = TextosJScript[39];
                                break;
                            case 2:
                                estOrden = TextosJScript[40];
                                break;
                            case 4:
                                estOrden = TextosJScript[41];
                                break;
                            case 8:
                                estOrden = TextosJScript[42];
                                break;
                            case 16:
                                estOrden = TextosJScript[43];
                                break;
                            case 32:
                                estOrden = TextosJScript[44];
                                break;
                            case 64:
                                estOrden = TextosJScript[45];
                                break;
                            case 128:
                                estOrden = TextosJScript[46];
                                break;
                            case 256:
                                estOrden = TextosJScript[47];
                                break;
                            case 512:
                                estOrden = TextosJScript[48];
                                break;
                        }

                        $('#lblConfirm').text(TextosAlert[33] + ' ' + estOrden); //Imposible deshacer el borrado de un pedido en estado
                        break
                    }
                case 2:
                    {
                        $('#lblConfirm').text(TextosAlert[34]); //Imposible deshacer el borrado de un pedido contra abierto cuando el pedido abierto no está abierto
                        break
                    }
                case 3:
                    {
                        $('#lblConfirm').text(TextosAlert[35]); //Imposible deshacer el borrado de un pedido contra abierto cuando el pedido abierto está borrado.
                        break
                    }
                case 41:
                case 51:
                    {
                        $('#lblConfirm').text(TextosAlert[36]); //Imposible deshacer el borrado de un pedido contra abierto cuando ninguna de sus líneas se pueden deshacer.
                        break
                    }
            }
            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').show();
            CentrarPopUp($('#pnlConfirm'));
            $('#pnlConfirm').show();
        }
        else {
           
            $('#lblConfirm').text(TextosAlert[37]); //Se dispone a deshacer el borrado de la línea. ¿Desea continuar?
            $('#chkConfirm').hide();

            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').show();
            CentrarPopUp($('#pnlConfirm'));
            $('#pnlConfirm').show();
            $('#txtConfirm').hide();

            $('#btnCancelar').show();
            $('#btnAceptar').show();
            $('#btnAceptar').die('click');
            $('#btnAceptar').live('click', function () {
                $('#popupFondo').hide();
                $('#pnlConfirm').hide();

                ImporteBrutoLineas = parseFloat($('#lblImpBruto_hdd').text());
                
                var idLinea
                var ImporteLinea, ImporteDescuento, ImporteCoste;

                ImporteLinea = 0
                ImporteDescuento = 0
                ImporteCoste = 0

                var iNumerosLinea = 0;
                $.each(gLineas, function () {
                    if (!this.EstaDeBaja) {
                        iNumerosLinea++;
                    }               
                });
                $.each(gLineas, function () {
                    idLinea = this.ID;                    

                    if (idLinea == IdLineaPedido) {
                        //Vas a Restar la linea
                        
                        //Importe Bruto
                        ImporteLinea =  parseFloat($('#lblImpBruto' + idLinea + '_hdd').text().replace(",", "."));

                        //Coste Linea
                        if (this.Costes.length !== 0) {
                            ImporteCoste = this.Costes.Importe;
                        } else {
                            ImporteCoste = 0
                        }
                        //Descuento Linea
                        if (this.Descuentos.length !== 0) {
                            ImporteDescuento = this.Descuentos.Importe
                        } else {
                            ImporteDescuento = 0
                        }

                        //Nuevo CD en orden entrega
                        DescuentosCabeceraBD = 0;
                        CostesCabeceraBD = 0;
                        //Impuesto Linea
                        DescuentosCabeceraGeneral = 0;
                        CostesCabeceraGeneral = 0;

                        $.each(gDetallePedido.Descuentos, function () {
                            if (this.Seleccionado == true) {
                                MiImporteLinea = ImporteLinea + ImporteCoste - ImporteDescuento
                                if (this.Operacion == "1") { //%
                                    ImporteBruto = MiImporteLinea * (this.Valor / 100);
                                    DescuentosCabeceraGeneral = DescuentosCabeceraGeneral + ImporteBruto;
                                    //
                                    DescuentosCabeceraBD = DescuentosCabeceraBD + ImporteBruto;
                                }
                                else {                                     
                                    DescuentosCabeceraGeneral = DescuentosCabeceraGeneral + (this.Valor / iNumerosLinea);
                                    //Continua igual. Ejemplo: 3€ al pedido
                                }
                            }
                        });
                        $.each(gDetallePedido.Costes, function () {
                            if (this.Seleccionado == true) {
                                MiImporteLinea = ImporteLinea + ImporteCoste - ImporteDescuento
                                if (this.Operacion == "1") { //%
                                    ImporteBruto = MiImporteLinea * (this.Valor / 100);
                                    CostesCabeceraGeneral = CostesCabeceraGeneral + ImporteBruto;
                                    //
                                    CostesCabeceraBD = CostesCabeceraBD + ImporteBruto;
                                }
                                else {
                                    CostesCabeceraGeneral = CostesCabeceraGeneral + (this.Valor / iNumerosLinea);
                                    //Continua igual. Ejemplo: 3€ al pedido
                                }
                            }
                        });
                        if (this.Est !== 99) {
                            DescuentosLinea = 0;
                            ImporteCosteLinea = parseFloat($.trim($('#lblImpBruto' + this.ID + '_hdd').text()).replace(",", "."));
                            Impuestos = parseFloat($.trim($('#lblImpTotalCuotas_hdd').text()).replace(",", "."));
                            ImpuestosLinea =0

                            $.each(this.Descuentos, function () {
                                if (this.Seleccionado == true) {
                                    if (this.Operacion == "1") { //%
                                        ImporteBruto = ImporteCosteLinea * (this.Valor / 100);
                                        DescuentosLinea = DescuentosLinea + ImporteBruto;
                                    }
                                    else {
                                        DescuentosLinea = DescuentosLinea + this.Valor;
                                    }
                                }
                            });
                            ImporteCosteLinea = ImporteCosteLinea - DescuentosLinea - DescuentosCabeceraGeneral + CostesCabeceraGeneral;
                            $.each(this.Impuestos, function () {
                                var ImpuestoAct = ImporteCosteLinea * (this.Valor / 100);
                                ImpuestosLinea = ImpuestosLinea +ImpuestoAct;
                            });
                            ImporteCosteLinea = parseFloat($.trim($('#lblImpBruto' + this.ID + '_hdd').text()).replace(",", "."));
                            $.each(this.Costes, function () {
                                if (this.Seleccionado == true) {
                                    if (this.Operacion == "1") { //%
                                        ImporteCosteLinea = ImporteCosteLinea * (this.Valor / 100);
                                    }
                                    else {
                                        ImporteCosteLinea = this.Valor;
                                    }
                                    $.each(this.Impuestos, function () {
                                        var ImpuestoAct = ImporteCosteLinea * (this.Valor / 100);
                                        ImpuestosLinea = ImpuestosLinea + ImpuestoAct;
                                    });
                                }
                            });
                        }
                    }  //else NO cambia
                });
                
                CostesCabeceraMoneda = CostesCabeceraBD / dCambio
                DescuentosCabeceraMoneda = DescuentosCabeceraBD / dCambio

                ImporteLineaBD = ImporteLinea / dCambio
                ImporteLineaBD = ImporteLineaBD + (ImporteCoste / dCambio)
                ImporteLineaBD = ImporteLineaBD - (ImporteDescuento / dCambio)
                ImporteLineaBD = ImporteLineaBD + (ImpuestosLinea / dCambio)

                var params = JSON.stringify({ IdLinea: IdLineaPedido, IdOrden: sID, ImporteBrutoLineas: ImporteBrutoLineas, ImporteLinea: ImporteLinea, ImporteCoste: ImporteCoste, ImporteDescuento: ImporteDescuento, ImporteLineaBD: ImporteLineaBD, ImporteCosteCab: CostesCabeceraMoneda, ImporteDescuentoCab: DescuentosCabeceraMoneda })
                $.when($.ajax({
                    type: "POST",
                    url: ruta + '/App_Pages/EP/DetallePedido.aspx/DeshacerBorrarLinea',
                    contentType: "application/json; charset=utf-8",
                    data: params,
                    dataType: "json",
                    async: false
                })).done(function (msg) {
                    result = $.parseJSON(msg.d);
                    iRespuesta = parseInt(result.Validacion);
                    TotalLineas = parseFloat(result.TotalLineas);

                    if (iRespuesta == 1) {
                        if (gDatosGenerales.BajaLogica) {
                            //Si el pedido estaba de baja lÃ³gica se recarga la pÃ¡gina para volver a cargar la cabecera y datos generales
                            window.location.reload(true);
                        } else {
                            $.each(gLineas, function () {
                                if (this.ID == IdLineaPedido) {
                                    this.EstaDeBaja = false;
                                }
                            });
                            var grid = $find($('[id$=whdgArticulos]').attr('id'));
                            var gridView = grid.get_gridView();
                            for (var i = 0; i < gridView.get_behaviors().get_selection().get_selectedRows(0).getItem(0).get_cellCount() ; i++) {
                                var cell = gridView.get_behaviors().get_selection().get_selectedRows(0).getItem(0).get_cell(i);
                                if (cell) {
                                    if (cell.get_column().get_key() == "Key_EST") {
                                        switch (cell.get_value()) {
                                            case 0:
                                                cell.get_element().innerText = TextosJScript[60]
                                                break;
                                            case 1:
                                                switch (gDatosGenerales.EstadoOrden) {
                                                    case 0:
                                                    case 1:
                                                        cell.get_element().innerText = TextosJScript[63]
                                                        break;
                                                    case 2:
                                                        cell.get_element().innerText = TextosJScript[62]
                                                        break;
                                                    case 3:
                                                        cell.get_element().innerText = TextosJScript[61]
                                                        break;
                                                    default:
                                                        cell.get_element().innerText = TextosJScript[62]
                                                        break;
                                                }
                                                break;
                                            case 2:
                                                cell.get_element().innerText = TextosJScript[59]
                                                break;
                                            case 3:
                                                cell.get_element().innerText = TextosJScript[58]
                                                break;
                                            case 20:
                                                cell.get_element().innerText = TextosJScript[57]
                                                break;
                                            case 21:
                                                cell.get_element().innerText = TextosJScript[56]
                                                break;
                                            case 23:
                                                cell.get_element().innerText = TextosJScript[55]
                                                break;
                                        }
                                    }
                                }
                            }


                            var cell = gridView.get_behaviors().get_selection().get_selectedRows(0).getItem(0).get_cell(0);
                            cell.get_element().childNodes['1'].style.cssText = "text-align: center; cursor: pointer;"
                            cell.get_element().childNodes['3'].style.cssText = "text-align: center; visibility: hidden; width:0px; float: left; cursor: pointer;"
                            cell.get_element().parentElement.className = cell._element.parentElement.className.toString().replace("LineaPedidoBajaLogica ", "")

                            $('#lblImpBruto_hdd').text(TotalLineas);
                            $('#lblImpBruto').text(formatNumber(TotalLineas) + " " + sMoneda);
                            var ImpCostes = parseFloat($('#lblCosteCabeceraTotal_hdd').text());
                            ImpCostes = ImpCostes + CostesCabeceraBD
                            var ImpDescuentos = parseFloat($('#lblDescuentosCabeceraTotal_hdd').text());
                            ImpDescuentos = ImpDescuentos + DescuentosCabeceraBD
                            var cuotasPedido = parseFloat($('#lblImpTotalCuotas_hdd').text());
                            var TotalCabecera = TotalLineas + ImpCostes - ImpDescuentos + cuotasPedido + ImpuestosLinea

                            $('#lblImporteTotalCabecera_hdd').text(TotalCabecera);
                            $('#lblImporteTotalCabecera').text(formatNumber(TotalCabecera) + " " + sMoneda);

                            $('#lblCosteCabeceraTotal_hdd').text(ImpCostes);
                            $('#lblCosteCabeceraTotal').text(formatNumber(ImpCostes) + " " + sMoneda);

                            $('#lblDescuentosCabeceraTotal_hdd').text(ImpDescuentos);
                            $('#lblDescuentosCabeceraTotal').text(formatNumber(ImpDescuentos) + " " + sMoneda);

                            calcularImpuestos(false)
                        }
                    }
                    else alert(TextosAlert[38]); //Se ha producido un error al deshacer el borrado de línea. Por favor, inténtelo de nuevo más tarde y si el problema persiste, contacte con el soporte técnico.

                });
            });
        }
    }

    function BorrarLineaPedido(IdLineaPedido) {
        var iRespuesta = 0;
        var params = JSON.stringify({ idLinea: IdLineaPedido, IdOrden: sID });
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/ComprobarBorrarLinea',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        })).done(function (msg) {
            iRespuesta = $.parseJSON(msg.d);
        });

        if (iRespuesta !== 0) {
            $('#btnCancelar').hide();
            $('#btnAceptar').show();
            $('#btnAceptar').die('click');
            $('#btnAceptar').live('click', function () {
                $('#popupFondo').hide();
                $('#pnlConfirm').hide();
            });
            switch (iRespuesta) {
                case 1:
                    {
                        $('#lblConfirm').text(TextosAlert[28]); //???De baja
                        break
                    }
                case 2:
                    {
                        $('#lblConfirm').text(TextosAlert[29]); //La línea tiene pedidos emitidos, no se puede borrar.
                        break
                    }
                case 3:
                    {
                        $('#lblConfirm').text(TextosAlert[30]); //No se pueden borrar todas las líneas de un pedido, para ello borre el pedido.
                        break
                    }
                case 4:
                    {
                        $('#lblConfirm').text(TextosAlert[53]); //La línea tiene hitos de facturación, no se puede borrar.
                        break;
                    }
            }
            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').show();
            CentrarPopUp($('#pnlConfirm'));
            $('#pnlConfirm').show();
        }
        else {
            $('#lblConfirm').text(TextosAlert[31]); //Se dispone a borrar la línea. ¿Desea continuar?
            $('#chkConfirm').hide();

            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').show();
            CentrarPopUp($('#pnlConfirm'));
            $('#pnlConfirm').show();
            $('#txtConfirm').hide();

            $('#btnCancelar').show();
            $('#btnAceptar').show();
            $('#btnAceptar').die('click');
            $('#btnAceptar').live('click', function () {
                $('#popupFondo').hide();
                $('#pnlConfirm').hide();

                ImporteBrutoLineas = parseFloat($('#lblImpBruto_hdd').text());
              
                var idLinea
                var ImporteLinea, ImporteDescuento, ImporteCoste;
                ImporteLinea = 0
                ImporteDescuento = 0
                ImporteCoste = 0

                var iNumerosLinea = 0;
                $.each(gLineas, function () {
                    if (!this.EstaDeBaja) {
                        iNumerosLinea++;
                    }
                });
                $.each(gLineas, function () {
                    idLinea = this.ID;               

                    if (idLinea == IdLineaPedido) {
                        //Vas a Restar la linea

                        //Importe Bruto
                        ImporteLinea = parseFloat($('#lblImpBruto' + idLinea + '_hdd').text().replace(",", "."));

                        //Coste Linea
                        if (this.Costes.length !== 0) {
                            ImporteCoste = this.Costes.Importe;
                        } else {
                            ImporteCoste=0
                        }
                        //Descuento Linea
                        if (this.Descuentos.length !== 0) {
                            ImporteDescuento = this.Descuentos.Importe
                        } else {
                            ImporteDescuento=0
                        }
                        //Nuevo CD en orden entrega
                        DescuentosCabeceraBD = 0;
                        CostesCabeceraBD = 0;
                        //Impuesto Linea
                        DescuentosCabeceraGeneral = 0;
                        CostesCabeceraGeneral = 0;
                        
                        $.each(gDetallePedido.Descuentos, function () {
                            if (this.Seleccionado == true) {
                                MiImporteLinea = ImporteLinea + ImporteCoste - ImporteDescuento
                                if (this.Operacion == "1") { //%
                                    ImporteBruto = MiImporteLinea * (this.Valor / 100);
                                    DescuentosCabeceraGeneral = DescuentosCabeceraGeneral + ImporteBruto;
                                    //
                                    DescuentosCabeceraBD = DescuentosCabeceraBD + ImporteBruto;
                                }
                                else {
                                    DescuentosCabeceraGeneral = DescuentosCabeceraGeneral + (this.Valor / iNumerosLinea);
                                    //Continua igual. Ejemplo: 3€ al pedido
                                }
                            }
                        });
                        $.each(gDetallePedido.Costes, function () {
                            if (this.Seleccionado == true) {
                                MiImporteLinea = ImporteLinea + ImporteCoste - ImporteDescuento
                                if (this.Operacion == "1") { //%
                                    ImporteBruto = MiImporteLinea * (this.Valor / 100);
                                    CostesCabeceraGeneral = CostesCabeceraGeneral + ImporteBruto;
                                    //
                                    CostesCabeceraBD = CostesCabeceraBD + ImporteBruto;
                                }
                                else {
                                    CostesCabeceraGeneral = CostesCabeceraGeneral + (this.Valor / iNumerosLinea);
                                    //Continua igual. Ejemplo: 3€ al pedido
                                }
                            }
                        });

                        if (this.Est !== 99) {
                            DescuentosLinea = 0;
                            ImporteCosteLinea = parseFloat($.trim($('#lblImpBruto' + this.ID + '_hdd').text()).replace(",", "."));
                            Impuestos = parseFloat($.trim($('#lblImpTotalCuotas_hdd').text()).replace(",", "."));
                            ImpuestosLinea = 0

                            $.each(this.Descuentos, function () {
                                if (this.Seleccionado == true) {
                                    if (this.Operacion == "1") { //%
                                        ImporteBruto = ImporteCosteLinea * (this.Valor / 100);
                                        DescuentosLinea = DescuentosLinea + ImporteBruto;
                                    }
                                    else {
                                        DescuentosLinea = DescuentosLinea + this.Valor;
                                    }
                                }
                            });
                            ImporteCosteLinea = ImporteCosteLinea - DescuentosLinea - DescuentosCabeceraGeneral + CostesCabeceraGeneral;
                            $.each(this.Impuestos, function () {
                                var ImpuestoAct = ImporteCosteLinea * (this.Valor / 100);
                                ImpuestosLinea = ImpuestosLinea + ImpuestoAct;
                            });
                            ImporteCosteLinea = parseFloat($.trim($('#lblImpBruto' + this.ID + '_hdd').text()).replace(",", "."));
                            $.each(this.Costes, function () {
                                if (this.Seleccionado == true) {
                                    if (this.Operacion == "1") { //%
                                        ImporteCosteLinea = ImporteCosteLinea * (this.Valor / 100);
                                    }
                                    else {
                                        ImporteCosteLinea = this.Valor;
                                    }
                                    $.each(this.Impuestos, function () {
                                        var ImpuestoAct = ImporteCosteLinea * (this.Valor / 100);
                                        ImpuestosLinea = ImpuestosLinea + ImpuestoAct;
                                    });
                                }
                            });
                        }                                             
                    }
                    //else NO cambia
                });

                CostesCabeceraMoneda = CostesCabeceraBD / dCambio
                DescuentosCabeceraMoneda = DescuentosCabeceraBD / dCambio

                ImporteLineaBD = ImporteLinea / dCambio
                ImporteLineaBD = ImporteLineaBD + (ImporteCoste / dCambio)
                ImporteLineaBD = ImporteLineaBD - (ImporteDescuento / dCambio)
                ImporteLineaBD = ImporteLineaBD + (ImpuestosLinea / dCambio)

                var params = JSON.stringify({ IdLinea: IdLineaPedido, IdOrden: sID, ImporteBrutoLineas: ImporteBrutoLineas, ImporteLinea: ImporteLinea, ImporteCoste: ImporteCoste, ImporteDescuento: ImporteDescuento, ImporteLineaBD: ImporteLineaBD, ImporteCosteCab: CostesCabeceraMoneda, ImporteDescuentoCab: DescuentosCabeceraMoneda })
                 $.when($.ajax({
                    type: "POST",
                    url: ruta + '/App_Pages/EP/DetallePedido.aspx/BorrarLinea',
                    contentType: "application/json; charset=utf-8",
                    data: params,
                    dataType: "json",
                    async: false
                 })).done(function (msg) {
                    result = $.parseJSON(msg.d);
                    iRespuesta = parseInt(result.Validacion);
                    TotalLineas = parseFloat(result.TotalLineas);
                    
                    if (iRespuesta == 1) {          
                        $.each(gLineas, function () {
                            if (this.ID == IdLineaPedido) {
                                this.EstaDeBaja = true;
                            }
                        });              
                        var grid = $find($('[id$=whdgArticulos]').attr('id'));
                        var gridView = grid.get_gridView();

                        for (var i = 0; i < gridView.get_behaviors().get_selection().get_selectedRows(0).getItem(0).get_cellCount() ; i++) {
                            var cell = gridView.get_behaviors().get_selection().get_selectedRows(0).getItem(0).get_cell(i);
                            if (cell) {
                                if (cell.get_column().get_key() == "Key_EST") {
                                    cell.get_element().innerText = TextosJScript[54]
                                    break;
                                }
                            }
                        }                        
                        
                        var cell = gridView.get_behaviors().get_selection().get_selectedRows(0).getItem(0).get_cell(0);
                        cell.get_element().childNodes['1'].style.cssText = "text-align: center; visibility: hidden; width:0px; cursor: pointer;"
                        cell.get_element().childNodes['3'].style.cssText = "text-align: center; float: left; cursor: pointer;"
                        cell.get_element().parentElement.className = "LineaPedidoBajaLogica " + cell._element.parentElement.className

                        $('#lblImpBruto_hdd').text(TotalLineas);
                        $('#lblImpBruto').text(formatNumber(TotalLineas) + " " + sMoneda);
                        var ImpCostes = parseFloat($('#lblCosteCabeceraTotal_hdd').text());
                        ImpCostes = ImpCostes - CostesCabeceraBD
                        var ImpDescuentos = parseFloat($('#lblDescuentosCabeceraTotal_hdd').text());
                        ImpDescuentos = ImpDescuentos - DescuentosCabeceraBD
                        var cuotasPedido = parseFloat($('#lblImpTotalCuotas_hdd').text());
                        var TotalCabecera = TotalLineas + ImpCostes - ImpDescuentos + cuotasPedido - ImpuestosLinea

                        $('#lblImporteTotalCabecera_hdd').text(TotalCabecera);
                        $('#lblImporteTotalCabecera').text(formatNumber(TotalCabecera) + " " + sMoneda);

                        $('#lblCosteCabeceraTotal_hdd').text(ImpCostes);
                        $('#lblCosteCabeceraTotal').text(formatNumber(ImpCostes) + " " + sMoneda);

                        $('#lblDescuentosCabeceraTotal_hdd').text(ImpDescuentos);
                        $('#lblDescuentosCabeceraTotal').text(formatNumber(ImpDescuentos) + " " + sMoneda);

                        calcularImpuestos(false)
                    }
                    else alert(TextosAlert[32]); //Se ha producido un error al borrar la línea. Por favor, inténtelo de nuevo más tarde y si el problema persiste, contacte con el soporte técnico.

                });
            });
        }
    };
    function RecepcionesLinea(IdLineaPedido) {
        MostrarCargando();
        var popup = $find(mpeRecepciones);
        popup.add_shown(OcultarCargando);
        var btnOcultoRecep = $('[id$=btnOcultoRecep]').attr('id');
        __doPostBack(btnOcultoRecep, IdLineaPedido);
    };
    function Facturas(IdLineaPedido,NumLinea) {
        var btnOcultoFra = $('[id$=btnOcultoFra]').attr('id');
        __doPostBack(btnOcultoFra, IdLineaPedido + '#' + NumLinea);
    };
    function Pagos(IdLineaPedido) {
        var btnOcultoPagos = $('[id$=btnOcultoPagos]').attr('id');
        __doPostBack(btnOcultoPagos, IdLineaPedido);
    };
    //funcion que se ejecuta cuando se cambia el valor de la cantidad recibida y que habilita el boton de guardar
    function TextChange(oEdit, newText, oEvent) {
        if (oEdit.value > 0) {
            var IdBtnGuardar = oEdit._element.getAttribute("btnGuardarID")
            var btnGuardar = document.getElementById(IdBtnGuardar)
            btnGuardar.style.display = ""
        } else {
            var IdBtnGuardar = oEdit._element.getAttribute("btnGuardarID")
            var btnGuardar = document.getElementById(IdBtnGuardar)
            btnGuardar.style.display = "none"
        };

        var vdecimalfmt = oEdit._element.getAttribute("vdecimalfmt")
        var vthousanfmt = oEdit._element.getAttribute("vthousanfmt")
        var numdec = oEdit._element.getAttribute("numdec")
        var idlblPrecioUni = oEdit._element.getAttribute("lblPrecioUni")
        var PrecioUni = oEdit._element.getAttribute("PrecioUni")
        var lblprecioUni = document.getElementById(idlblPrecioUni)

        //<%'Actualizamos el importe al cambiar la cantidad%>
        PrecioUni = PrecioUni.replace(' ', '');
        PrecioUni = PrecioUni.replace(',', '.');
        while (PrecioUni.indexOf(".") != PrecioUni.lastIndexOf(".")) {
            PrecioUni = PrecioUni.substring(0, PrecioUni.indexOf(".") - 1) + PrecioUni.slice(PrecioUni.indexOf(".") + 1);
        }
        var strImporte = (parseFloat(PrecioUni) * oEdit.getValue()).toString()
        var BigNumber = str2num(strImporte, '.', '')
        if (lblprecioUni.innerText)
            lblprecioUni.innerText = num2str(BigNumber, vdecimalfmt, vthousanfmt, numdec)
        else
            lblprecioUni.textContent = num2str(BigNumber, vdecimalfmt, vthousanfmt, numdec)
    };
    function AnularLineaRecep(IdLineaPedido, IdLineaRecep) {
        $find(mpeRecepciones).hide();
        $('#lblConfirm').text(TextosAlert[20]);
        $('#chkConfirm').hide();

        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        CentrarPopUp($('#pnlConfirm'));
        $('#pnlConfirm').show();
        $('#txtConfirm').hide();

        $('#btnAceptar').show();
        $('#btnAceptar').unbind('click');
        $('#btnAceptar').live('click', function () {
            $('#popupFondo').hide();
            $('#pnlConfirm').hide();
            var btnOcultoAnularLinea = $('[id$=btnOcultoAnularLinea]').attr('id');
            __doPostBack(btnOcultoAnularLinea, IdLineaPedido + "###" + IdLineaRecep);
        });
    };
    function AnularRecepcion(idLineaPedido,albaran, fecha, OrdenProveedor) {
        $find(mpeRecepciones).hide();
        $('#lblConfirm').text(TextosAlert[21].replace('XXXXX',albaran));
        $('#chkConfirm').hide();

        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        CentrarPopUp($('#pnlConfirm'));
        $('#pnlConfirm').show();
        $('#txtConfirm').hide();

        $('#btnAceptar').show();
        $('#btnAceptar').unbind('click');
        $('#btnAceptar').live('click', function () {
            $('#popupFondo').hide();
            $('#pnlConfirm').hide();
            var btnOcultoAnularRecepcion = $('[id$=btnOcultoAnularRecepcion]').attr('id');
            __doPostBack(btnOcultoAnularRecepcion, albaran + '###' + fecha + '###' + OrdenProveedor + '###' + idLineaPedido);
        });
    };
    function BloquearFacturacionAlbaran(albaran, LineaPedido, Orden) {
        $find(mpeRecepciones).hide();
        $('#lblConfirm').html('<b>' + TextosAlert[22] + ' ' + albaran + '</b><p>' + TextosAlert[23] + ':</p>');
        $('#chkConfirm').hide();

        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        CentrarPopUp($('#pnlConfirm'));
        $('#pnlConfirm').show();
        $('#txtConfirm').val('');
        $('#txtConfirm').show();

        $('#btnAceptar').show();
        $('#btnAceptar').unbind('click');
        $('#btnAceptar').live('click', function () {
            $('#popupFondo').hide();
            $('#pnlConfirm').hide();
            var btnBloquearFacturacionAlbaran = $('[id$=btnBloquearFacturacionAlbaran]').attr('id');
            __doPostBack(btnBloquearFacturacionAlbaran, albaran + '###' + LineaPedido + '###' + Orden + '###' + $('#txtConfirm').val());
        });    
    };
    function DesbloquearFacturacionAlbaran(albaran, LineaPedido, Orden) {
        var btnDesbloquearFacturacionAlbaran = $('[id$=btnDesbloquearFacturacionAlbaran]').attr('id');
        __doPostBack(btnDesbloquearFacturacionAlbaran, albaran + '###' + LineaPedido + '###' + Orden);
    };
    function ActualizarImagenRecepcion(idLinea,rutaImagen) {
        $('#img_Recepciones_' + idLinea).attr('src', rutaImagen);
    };
    function CreateXmlHttp() {
        //<%' Probamos con IE%>
        try {
            //<%' Funcionará para JavaScript 5.0%>
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (oc) {
                xmlHttp = null;
            }
        };
        //<%' Si no se trataba de un IE, probamos con esto%>
        if (!xmlHttp && typeof XMLHttpRequest != "undefined") {
            xmlHttp = new XMLHttpRequest();
        };
        return xmlHttp;
    };
    //funcion que se ejecuta al guardar la modificacion de la cantidad recibida de una linea de la recepcion
    function ModificarLineaRecepcion(idLineaPedido,idLineaRecep, idBtnGuardar, idWneCantidad, idLabelPrecioUni, PrecioUni) {
        CreateXmlHttp();
        if (xmlHttp) {
            var wneCantidad = igedit_getById(idWneCantidad);        
            xmlHttp.open("POST", "App_Services/EP_Pedidos.asmx/ModificarLineaRecepcion", false);
            xmlHttp.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
            xmlHttp.send('{"Cantidad": ' + parseFloat(wneCantidad._vs) + ' , "LineaRecep": ' + idLineaRecep + '}');

            var retorno;
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                retorno = xmlHttp.responseText;
                var objJson = eval('(' + retorno + ')');
                        
                $('#lblConfirmRecep').text(objJson.d.TextoPregunta);
                switch (objJson.d.Respuesta) {
                    case 1:
                    case 2:
                        $find(mpeRecepciones).hide();
                        $('#imgPnlConfirmRecep').attr('src', ruta + 'Images/icono_info.gif');                    
                        $('#btnAceptarConfirmRecep').unbind('click');
                        $('#btnAceptarConfirmRecep').live('click', function () {
                            GrabarModificacionLinea(idLineaPedido.toString(),idLineaRecep.toString(), objJson.d.Respuesta.toString(), idBtnGuardar.toString(), idWneCantidad, idLabelPrecioUni, PrecioUni);
                        });
                        $('#btnCancelarConfirmRecep').unbind('click');
                        $('#btnCancelarConfirmRecep').live('click', function () {
                            CancelarModificacionLinea();
                        });
                        $('#popupFondo').css('height', $(document).height());
                        $('#popupFondo').show();
                        CentrarPopUp($('#pnlConfirmRecep'));
                        $('#pnlConfirmRecep').show();
                        break;
                    case 3:
                        $find(mpeRecepciones).hide();
                        $('#imgPnlConfirmRecep').attr('src', ruta + 'Images/alert-large.gif');
                        $('#btnAceptarConfirmRecep').unbind('click');
                        $('#btnAceptarConfirmRecep').live('click', function () {
                            CancelarModificacionLinea();
                        });
                        $('#btnCancelarConfirmRecep').hide();
                        $('#popupFondo').css('height', $(document).height());
                        $('#popupFondo').show();
                        CentrarPopUp($('#pnlConfirmRecep'));
                        $('#pnlConfirmRecep').show();                    
                        break;
                    case 0:
                        GrabarModificacionLinea(idLineaPedido,idLineaRecep, objJson.d.Respuesta, idBtnGuardar, idWneCantidad, idLabelPrecioUni, PrecioUni);
                        break;
                };
            };
        };
    };
    //funcion que al cargar la pagina comprobara si se tiene que mostrar la pestaña "otros Datos" en funcion de si la orden de entrega
    //tiene atributos o no
    function ComprobarAtributosOrden() {
        var gComprobarAtributos;
        $.when($.ajax({
            type: "POST",
            url: ruta + '/App_Pages/EP/DetallePedido.aspx/CargarAtributosOrden',
            contentType: "application/json; charset=utf-8",
            data: "{'sIDOrden':'" + sID + "'}",
            dataType: "json",
            async: false
        })).done(function (msg) {
            gComprobarAtributos = $.parseJSON(msg.d);
            if (gComprobarAtributos.length == 0) { //Si no hay atributos ocultamos la pestaña de "otros datos"
                $('#pesOtrosDatos').css("display", "none");
            }
            else {
                $('#pesOtrosDatos').css("display", "inline-block");
            }
        });
    }
    //funcion que hace la llamada al WS para que guarde la nueva cantidad recepcionada
    function GrabarModificacionLinea(idLineaPedido,idLineaRecep, idCaso, idBtnGuardar, idWneCantidad, idLabelPrecioUni, PrecioUni) {
        CreateXmlHttp();
        if (xmlHttp) {
            var wneCantidad = igedit_getById(idWneCantidad);
            xmlHttp.open("POST", "App_Services/EP_Pedidos.asmx/GrabarModificacionLineaRecepcion", false);
            xmlHttp.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
            xmlHttp.send('{"Cantidad": ' + parseFloat(wneCantidad._vs) + ' , "LineaRecep": ' + idLineaRecep + ' , "Caso": ' + idCaso + '}');

            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                var retorno = xmlHttp.responseText
                var objJson = eval('(' + retorno + ')');
                switch (idCaso) {
                    case 1:
                    case 2:
                    case 3:
                        $find(mpeRecepciones).show();
                        break;
                };
                wneCantidad.setValue(objJson.d.CantidadGrabada);
                var btnGuardar = document.getElementById(idBtnGuardar);
                btnGuardar.style.display = "none"
                //<%'Actualizamos la cantidad pendiente de recibir%>
                $('#lblCantPdte_' + idLineaPedido).text(objJson.d.CantidadPendienteRecibir.toString());

                //<%'Actualizamos el estado de recepción de la linea%>
                $('#lblEstado_' + idLineaPedido).text(objJson.d.EstadoRecepcionLinea.toString());
                //<%'Actualizamos el importe de la linea del panel de recepciones%>
                var vdecimalfmt = wneCantidad._element.getAttribute("vdecimalfmt")
                var vthousanfmt = wneCantidad._element.getAttribute("vthousanfmt")
                var numdec = wneCantidad._element.getAttribute("numdec")
                var lblprecioUni = document.getElementById(idLabelPrecioUni)
                PrecioUni = PrecioUni.replace(' ', '');
                PrecioUni = PrecioUni.replace(',', '.');
                while (PrecioUni.indexOf(".") != PrecioUni.lastIndexOf(".")) {
                    PrecioUni = PrecioUni.substring(0, PrecioUni.indexOf(".") - 1) + PrecioUni.slice(PrecioUni.indexOf(".") + 1);
                }
                var strImporte = (parseFloat(PrecioUni) * objJson.d.CantidadGrabada).toString()
                var BigNumber = str2num(strImporte, '.', '')
                if (lblprecioUni.innerText)
                    lblprecioUni.innerText = num2str(BigNumber, vdecimalfmt, vthousanfmt, numdec)
                else
                    lblprecioUni.textContent = num2str(BigNumber, vdecimalfmt, vthousanfmt, numdec)
            };
        };
    };
    function CancelarModificacionLinea() {
        $find(mpeRecepciones).show();
        $('#popupFondo').hide();
        $('#pnlConfirmRecep').hide();
    };
    function ExportarExcel(idOrden) {
        var btnExportarExcel = $('[id$=btnExportarExcel]').attr('id');
        __doPostBack(btnExportarExcel, idOrden);
    };
    function VerFlujo(instancia) {
        if (ComprobarEnProceso(instancia)) 
            var newWindow = window.open(rutaPM + 'seguimiento/NWcomentariossolic.aspx?Instancia=' + instancia, '_blank', 'width=1000,height=560,status=yes,resizable=no,top=200,left=200');       
    };
    //Muestra el detalle de la persona
    //Parámetro de entrada: Cod de la persona
    function VerDetallePersona(Per){
        var newWindow = window.open(rutaPM +"_common/detallepersona.aspx?CodPersona=" + Per.toString(),"_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");            
    }
    function validarLength(e,l) {
        if (e.value.length>l) return false
        else return true
    }
    function textCounter(e, l) { 
        if (e.value.length > l) e.value = e.value.substring(0, l); 
    } 
    //Muestra el detalle del tipo de solicitud
    function DetalleTipoSolicitud(IdSolicitud) {
        var newWindow = window.open(rutaPM + 'alta/solicitudPMWEB.aspx?Solicitud=' + IdSolicitud, '_blank', 'width=700,height=450,status=no,resizable=no,top=100,left=100');
        return false;
    }
    function ComprobarEnProceso(instancia) {
        var enProceso = false;
        $.when($.ajax({
            type: 'POST',
            url: rutaFS + '_Common/App_services/Consultas.asmx/ComprobarEnProceso',
            data: JSON.stringify({ contextKey: instancia }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: false
        })).done(function (msg) {
            if (msg.d == "1") {
                var newWindow = window.open(rutaPM + 'seguimiento/NWcomentariossolic.aspx?Instancia=' + instancia + '&EnProceso=1', '_blank', 'width=600,height=300,status=yes,resizable=no,top=200,left=200');                  
                enProceso = false;
            } else  enProceso = true;        
        });
        return enProceso;
    };
    //<%'Función que impide que se puedan seleccionar las líneas cuya recepción está completa o bien que corresponden a recepciones automáticas de planes de entrega%>
    function whdgArticulos_RowSelectionChanged(sender, e) {
        var grid = $find($('[id$=whdgArticulos]').attr('id'));
        var gridView = grid.get_gridView();
        var row = gridView.get_behaviors().get_selection().get_selectedRows(0);
        if (row.getItem(0) != null) {
            iFilaSeleccionada = row.getItem(0).get_cellByColumnKey("Key_NUM_LINEA").get_value();
            iCodFilaSeleccionada = row.getItem(0).get_cellByColumnKey("Key_IDLINEA").get_value();
            gridView.get_behaviors().get_selection().get_selectedRows().add(row.getItem(0));
            if (gridView != null) {
                for (i = 1; i <= gridView.get_behaviors().get_selection().get_selectedRows().get_length(); i++) {
                    var CantPdte = gridView.get_behaviors().get_selection().get_selectedRows().getItem(i - 1).get_cellByColumnKey("Key_CANT").get_value();

                }
            }
            var strFila = ("00" + iFilaSeleccionada.toString()).slice (-3); 
            $('#lblDatosLineaLinea').text(" - " + TextosJScript[37] + " " + strFila);
            $('#lblCostesLineaLinea').text(" - " + TextosJScript[37] + " " + strFila);
            $('#lblDescuentosLineaLinea').text(" - " + TextosJScript[37] + " " + strFila);
        
            if ($.map(gDetallePedido.Lineas, function (x) { if (x.ID == iCodFilaSeleccionada) return x; })[0].ConPlanEntrega) $('#pesPlanesEntrega').show();
            else $('#pesPlanesEntrega').hide();        
        }
    }
    function whdgArticulos_CellClick(sender, args) {
        var grid = $find($('[id$=whdgArticulos]').attr('id'));
        var gridView = grid.get_gridView();
        if (args.get_type() == "cell") {
            var row = args.get_item().get_row();
            if (row.get_index() == -1) //Si pincha en la fila de filtros salimos de la funcion
                return false
            iFilaSeleccionada = row.get_cellByColumnKey("Key_NUM_LINEA").get_value();
            iCodFilaSeleccionada = row.get_cellByColumnKey("Key_IDLINEA").get_value();
            gridView.get_behaviors().get_selection().get_selectedRows().add(row);
            var strFila = ("00" + iFilaSeleccionada.toString()).slice (-3);	
            iFilaSeleccionada=strFila;

            $('#lblDatosLineaLinea').text(" - " + TextosJScript[37] + " " + strFila);
            $('#lblCostesLineaLinea').text(" - " + TextosJScript[37] + " " + strFila);
            $('#lblDescuentosLineaLinea').text(" - " + TextosJScript[37] + " " + strFila);
        }
    }
    function Descarcargar_Archivo(sIden, sNombre, sTipo, sDataSize, sURL) {
        if (sIden > 0) {
            var link = rutaEP+"/EPFileTransferHandler.ashx?f=" + sNombre + "&t=" + sTipo + "&id=" + sIden + "&nombre=" + sNombre + "&ds=" + sDataSize + "&isEP=true&isNew=false";
            window.location = link;
        } else  window.location = sURL + "&t=" + sTipo + "&id=" + sIden + "&nombre=" + sNombre + "&ds=" + sDataSize + "&isEP=true&isNew=true";        
    }      
    function VerDetalleExp(idOrden, estado, aprobador, receptor) {
        //en el parametro desde indicamos el origen desde donde se accede al detalle del pedido. 1=seguimiento
        var vpu = ($('[id$=chkSeguimientoPedidos] input').first().prop('checked') == undefined ? true : $('[id$=chkSeguimientoPedidos] input').first().prop('checked'));
        var vpou = ($('[id$=chkSeguimientoPedidos] input').last().prop('checked') == undefined ? false : $('[id$=chkSeguimientoPedidos] input').last().prop('checked'));
        //document.location.href = rutaFS + 'EP/detallePedidoExp.aspx?desde=3&id=' + idOrden + '&estado=' + estado + '&aprobador=' + aprobador + '&receptor=' + receptor + '&vpu=' + vpu + '&vpou=' + vpou;
        window.open(rutaFS + 'EP/DetallePedidoExp.aspx?desde=3&id=' + idOrden + '&estado=' + estado + '&aprobador=' + aprobador + '&receptor=' + receptor + '&vpu=' + vpu + '&vpou=' + vpou, '_blank', 'width=600,height=275,status=no,resizable=yes,top=200,left=200,menubar=yes')
    };
    function MostrarMenuAcciones(btn){
        var top = $(btn).offset().top+20;
        var left=$(btn).offset().left;
        $('#divAcciones').css({top:top+'px'});
        $('#divAcciones').css({left:left+'px'});
        $('#divAcciones').show();
    }
    //Funcion que valida si es un numero valido y si lo es lo formatea
    function validaFloat(sender, old, idLinea) {
        var EstaDeBaja = false;
        $.each(gDetallePedido.Lineas, function () {
            if (this.ID == idLinea) {
                EstaDeBaja = this.EstaDeBaja;
            }
        });

        var numero = sender.value;
        var exp
        if (UsuNumberDecimalSeparator == ',')  exp = /^([0-9])*[.]?([0-9])*[,]?[0-9]*$/
        else exp = /^([0-9])*[,]?([0-9])*[.]?[0-9]*$/ ///^([0-9])*[.]?[0-9]*$/

        if (EstaDeBaja == true) {
            alert(TextosAlert[39])
            sender.value = sender.defaultValue;
            return false;
        } else {
            if (!exp.test(numero)) {
                alert(TextosJScript[0].toString().replace("XXXX", numero));
                sender.value = ''
                return false;
            } else {
                numero = strToNum(numero)
                numero = formatNumber(numero)
                sender.value = numero
                return true
            }
        }
    }
    //Funcion que valida la cantidad de las lineas en relacion a la unidad de pedido de la linea
    function validaFloatCantidad(sender, old, idLinea) {
        var numDecimales = UsuNumberNumDecimals;
        var EstaDeBaja = false;
        $.each(gDetallePedido.Lineas, function () {
            if (this.ID == idLinea) {
                if (this.EstaDeBaja == true) {                    
                    EstaDeBaja = true;
                }        
                if (this.NumDecUP !== null) {
                    numDecimales = this.NumDecUP;
                }
            }
        });
        var numero = sender.value;
        var exp
        if (UsuNumberDecimalSeparator == ',') exp = /^([0-9])*[.]?([0-9])*[,]?[0-9]*$/
        else exp = /^([0-9])*[,]?([0-9])*[.]?[0-9]*$/ ///^([0-9])*[.]?[0-9]*$/
    
        if (EstaDeBaja == true) {
            alert(TextosAlert[39])
            sender.value = sender.defaultValue;
            gBoolValidaFloat = false
            return false;
        } else {
            if (!exp.test(numero)) {
                alert(TextosJScript[0].toString().replace("XXXX", numero));
                sender.value = ''
                gBoolValidaFloat = false
                return false;
            } else {
                numero = strToNum(numero)
                numero = formatNumberUnidad(numero, numDecimales)
                sender.value = numero
                gBoolValidaFloat = true
                return true
            }
        }
    }
    function strToNum(strNum) {
        if (strNum == undefined || strNum == null || strNum === '')
            strNum = '0'

        while (strNum.indexOf(UsuNumberGroupSeparator) >= 0) {
            strNum = strNum.replace(UsuNumberGroupSeparator, "")
        }
        strNum = strNum.replace(UsuNumberDecimalSeparator, ".")
        return parseFloat(strNum)
    }
    function formatNumber(num) {
        if (num == undefined || num == null || num === '')
            num = 0
        num = parseFloat(num)
        var result = num.toFixed(UsuNumberNumDecimals);
        result = addSeparadorMiles(result.toString())
        var result1 = result.substring(0, result.length - parseInt(UsuNumberNumDecimals) - 1)
        var result2 = result.substring(result.length - parseInt(UsuNumberNumDecimals), result.length)
        result = result1 + UsuNumberDecimalSeparator + result2
        return result
    }
    function formatNumberUnidad(num, UsuNumberNumDecimalsUnidad) {
        if (num == undefined || num == null || num === '')
            num = 0
        num = parseFloat(num)
        var result = num.toFixed(UsuNumberNumDecimalsUnidad);
        result = addSeparadorMiles(result.toString())
        if (parseInt(UsuNumberNumDecimalsUnidad) !== 0) {
            var result1 = result.substring(0, result.length - parseInt(UsuNumberNumDecimalsUnidad) - 1)
            var result2 = result.substring(result.length - parseInt(UsuNumberNumDecimalsUnidad), result.length)
            result = result1 + UsuNumberDecimalSeparator + result2
        }
        return result
    }
    function addSeparadorMiles(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + UsuNumberGroupSeparator + '$2');
        }
        var retorno = x1 + x2;
        return retorno
    }

    /*
    '''<summary>
    '''Muestra la ventana de confirmar relanzamiento y configura el id del registro a relanzar
    '''</summary>
    '''<param name="id">identificador del registro de log a relanzar</param>*/
    function ConfirmarRelanzar() {
        CentrarPopUp($("#pnlRelanzar"));
        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        $("#pnlRelanzar").show();
        //var texto = $("[id$=lblAlertaRelanzar]").html();
        //$("[id$=lblAlertaRelanzar]").html(texto);
        return false;
    }

    function cerrarRelanzarOk() {
        $("#pnlRelanzarOk").hide();
        $('#popupFondo').hide();
        return false;
    }

    function cancelarRelanzar() {
        $("#pnlRelanzar").hide();
        $('#popupFondo').hide();
        //var texto = $("[id$=lblAlertaRelanzar]").html();
        //texto = texto.replace(gIdLog, "###");
        //$("[id$=lblAlertaRelanzar]").html(texto);
    }
    /**
    /*Relanza la integración del registro seleccionado
    **/
    function relanzar() {
        var newId;
        //ocultar popup 
        //var texto = $("[id$=lblAlertaRelanzar]").text();
        //$("[id$=lblAlertaRelanzar]").text(texto);
        $("#pnlRelanzar").hide();
        $('#popupFondo').hide();
        //mostrar ventana cargando
        MostrarCargando();        
        params = { idLog: gIdLog };
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'INT/VisorIntegracion.aspx/relanzarProveedor',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(params),
            dataType: "json",
            async: false
        })).done(function (msg) {
            //Realizar llamada asíncrona a FsnLibraryCom            
            params = { idEntidad: gIdEntidad, idLog: msg.d[1], iErp: gErp };
            $.ajax({
                type: "POST",
                url: rutaFS + 'INT/VisorIntegracion.aspx/exportar',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(params),
                dataType: "json",
                async: true
            });
            OcultarCargando();
            OcultarRelanzar();
            $('#EstadoOrdenIntegracion').text(TextosJScript[65]);
            $('#EstadoOrdenIntegracion').css("color", "black");
            //tras el envío mostrar ventana de OK
            CentrarPopUp($("#pnlRelanzarOk"));
            $('#popupFondo').show();
            $("#pnlRelanzarOk").show();
        });
        return false;
    }

    function OcultarCargando() {
        var modalprog = $find(ModalProgress);
        if (modalprog) modalprog.hide();
    }

    function OcultarRelanzar() {
        $('#divbtnRelanzar').hide();
    }