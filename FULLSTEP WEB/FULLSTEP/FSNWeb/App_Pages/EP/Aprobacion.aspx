﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="Aprobacion.aspx.vb" Inherits="Fullstep.FSNWeb.Aprobacion" Async="True" %>
<%@ MasterType VirtualPath="~/App_Master/Menu.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" language="javascript">
    
    var ArrOrdenes = new Array();
    var ArrLineas = new Array();
    var ArrImages = new Array();
    var ArrTextos = new Array();
    ArrImages['aprobado_act'] = new Image();
    ArrImages['aprobado_desact'] = new Image();
    ArrImages['denegado_act'] = new Image();
    ArrImages['denegado_desact'] = new Image();

    function VerFlujo(instancia) {
        if (ComprobarEnProceso(instancia)) {
            var newWindow = window.open(rutaPM + 'seguimiento/NWcomentariossolic.aspx?Instancia=' + instancia, '_blank', 'width=1000,height=560,status=yes,resizable=no,top=200,left=200');
            
        };
    };

    function ComprobarEnProceso(instancia) {
        var enProceso = false;
        $.when($.ajax({
            type: 'POST',
            url: rutaFS + '_Common/App_services/Consultas.asmx/ComprobarEnProceso',
            data: JSON.stringify({ contextKey: instancia }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: false
        })).done(function (msg) {
            if (msg.d == "1") {
                var newWindow = window.open(rutaPM + 'seguimiento/NWcomentariossolic.aspx?Instancia=' + instancia + '&EnProceso=1', '_blank', 'width=600,height=300,status=yes,resizable=no,top=200,left=200');
                
                enProceso = false;
            } else {
                enProceso = true;
            };
        });
        return enProceso;
    };

    function Orden(ID, botonaprobarID, botondenegarID, codOrdenAprobar, codOrdenRechazar, fechaActualizacion) {
        this.ID = ID;
        this.botonaprobarID = botonaprobarID;
        this.botondenegarID = botondenegarID;
        this.collapse = collapse;
        this.botoncollapse = botoncollapse;
        this.botonaprobar = botonaprobar;
        this.botondenegar = botondenegar;
        this.fechaActualizacion = fechaActualizacion;
        this.abierto = abierto;
        this.accion = new String();
        this.lineaaccion = new Number();
        this.accionOk = false;
        this.Lineas = new Array();
        this.ejecutaraccion = ejecutaraccion;
        this.codOrdenAprobar = codOrdenAprobar;
        this.codOrdenRechazar = codOrdenRechazar;
        ArrOrdenes[ID] = this;
    }

    function Linea(OrdenID, ID, botonaprobarID, botondenegarID, estado) {
        this.ID = ID;
        this.OrdenID = OrdenID;
        this.botonaprobarID = botonaprobarID;
        this.botondenegarID = botondenegarID;
        this.botonaprobar = botonaprobar;
        this.botondenegar = botondenegar;
        this.estado = estado; //0:ninguno 1:aprobada 2:denegada
        this.aprobada = lineaaprobada;
        this.denegada = lineadenegada;
        this.Orden = ArrOrdenes[OrdenID];
        this.Orden.Lineas[ID] = this;
        ArrLineas[ID] = this;
    }

    function collapse() {
        return $find(this.collapseID);
    }

    function gridLineas() {
        return $get(this.gridLineasID);
    }

    function cargando() {
        return $get(this.cargandoID);
    }

    function botoncollapse() {
        return $get(this.botoncollapseID);
    }

    function botonaprobar() {
        return $get(this.botonaprobarID);
    }

    function botondenegar() {
        return $get(this.botondenegarID);
    }

    function abierto() {
        return !this.collapse().get_Collapsed();
    }

    function lineaaprobada() {
        return (this.estado == 1);
    }

    function lineadenegada() {
        return (this.estado == 2);
    }

    function ejecutaraccion() {
        this.accionOk = true;
        switch (this.accion) {
            case 'aprobarorden':
                this.botonaprobar().click();
                break;
            case 'denegarorden':
                this.botondenegar().click();
                break;
        };
        return false;
    };

    function comprobarlineasmodificadas(ordenid, linea, accion) {
        var orden = ArrOrdenes[ordenid];
        switch (accion) {
            case 'desplegar':
                if (orden.cargando().style.display != 'none') return true;
                if (orden.collapse().get_Collapsed()) {
                    orden.collapse().set_Collapsed(false);
                    PageMethods.PedidoModificadoClient(ordenid, (linea) ? linea : 0, orden.fechaActualizacion, AsyncLineasModifOk);
                }
                else {
                    orden.collapse().set_Collapsed(true);
                }
                var abiertos = new Array();
                for (var o in ArrOrdenes) {
                    if (ArrOrdenes[o].abierto()) abiertos.push(o);
                }
                $get(hddcollLineasID).value = abiertos.toString();
                break;
            default:
                if (ArrOrdenes[ordenid].accion == accion && ArrOrdenes[ordenid].accionOk) {
                    ArrOrdenes[ordenid].accion = '';
                    ArrOrdenes[ordenid].accionOk = false;
                    $find(modalMje).hide();
                    $find(ModalProgress).show();
                    return true;
                }
                else {
                    PageMethods.PedidoModificadoClient(ordenid, (linea) ? linea : 0, orden.fechaActualizacion, AsyncLineasModifOk);
                }
        }
        ArrOrdenes[ordenid].accion = accion;
        ArrOrdenes[ordenid].lineaaccion = linea;
        ArrOrdenes[ordenid].accionOk = false;
        return false;
    }

    function AsyncLineasModifOk(msg) {
        if (msg != '') {
            var arrmsg = msg.split('#');
            var orden = ArrOrdenes[parseInt(arrmsg[0])];
            switch (orden.accion) {
                case 'aprobarorden':
                    PageMethods.ComprobarAprobacionClient(orden.ID, orden.codOrdenAprobar, AsyncConfirmarAprobacionOk);
                    break;
                case 'denegarorden':
                    PageMethods.ComprobarDenegacionClient(orden.ID, orden.codOrdenRechazar, AsyncConfirmarDenegacionOk);
                    break;
            };
        };
    };

    //''' <summary>
    //''' Comprueba si es Aprobable por el usuario conectado el pedido
    //''' </summary>
    //''' <remarks>Llamada desde: AsyncLineasModifOk ; Tiempo máximo: 0,2</remarks>
    function AsyncConfirmarAprobacionOk(msg) {
        var arrmsg = msg.split('#');
        var orden = ArrOrdenes[parseInt(arrmsg[0])];
        switch (parseInt(arrmsg[1])) {
            case 2: // La orden será emitida al proveedor
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[29] + '<br/>' + ArrTextos[1], true, false);
                break;
            case 3: // La línea ha sido eliminada por otro usuario
                orden.accion = '';
                orden.accionOk = false;
                MsgBox(ArrTextos[3], false, false);
                break;
            case 5: // Hay líneas denegadas por el mismo
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[7] + '<br/>' + ArrTextos[1], true, false);
                break;
            case 6: // Hay líneas denegadas por otro
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[8] + '<br/>' + ArrTextos[1], true, false);
                break;
            case 7: // Hay líneas pendientes de otros aprobadores
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[9] + '<br/>' + ArrTextos[1], true, false);
                break;
            case 8: // El pedido pasará a parcialmente denegado
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[10] + '<br/>' + ArrTextos[11], true, false);
                break;
        }
    }

    function AsyncConfirmarDenegacionOk(msg) {
        var arrmsg = msg.split('#');
        var orden = ArrOrdenes[parseInt(arrmsg[0])];
        switch (parseInt(arrmsg[1])) {
            case 0: // No necesita confirmación
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[17], true, true);
                break;
            case 1: // Las lineas serán traspasadas a un nivel superior de aprobación
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[0] + '<br/><br/>' + ArrTextos[17], true, true);
                break;
            case 2: // La orden queda pendiente de aprobar
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[9] + '<br/><br/>' + ArrTextos[17], true, true);
                break;
            case 3: // La linea ha sido eliminada por otro usuario
                orden.accion = '';
                orden.accionOk = false;
                MsgBox(ArrTextos[3], false, false);
                break;
            case 4: // La orden queda pendiente y no hay nivel superior
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[14] + '<br/><br/>' + ArrTextos[17], true, true);
                break;
            case 5: // La orden queda parcialmente denegada
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[15] + '<br/><br/>' + ArrTextos[17], true, true);
                break;
            case 6: // La orden queda denegada
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[16] + '<br/><br/>' + ArrTextos[17], true, true);
                break;
        }
    }

    function AsyncConfirmarAprobacionLineaOk(msg) {
        var arrmsg = msg.split('#');
        var orden = ArrOrdenes[parseInt(arrmsg[0])];
        var linea = orden.Lineas[parseInt(arrmsg[1])];
        switch (parseInt(arrmsg[2])) {
            case 0:
                switch (parseInt(arrmsg[3])) {
                    case 0: // Sin errores
                        orden.accion = '';
                        orden.accionOk = false;
                        switch (parseInt(arrmsg[4])) {
                            case 1: // Orden emitida al proveedor
                                MsgBox(ArrTextos[12], false, false);
                                $get(BtnAceptarMje).onclick = null; // Hace postback para refrescar la lista de pedidos
                                break;
                            case 2: // Líneas traspasadas a nivel superior de aprobación
                                MsgBox(ArrTextos[18], false, false);
                                $get(BtnAceptarMje).onclick = null; // Hace postback para refrescar la lista de pedidos
                                break;
                            default:
                                linea.botonaprobar().src = ArrImages['aprobado_act'].src;
                                linea.botonaprobar().disabled = true;
                                linea.botondenegar().src = ArrImages['denegado_desact'].src;
                                linea.botondenegar().disabled = false;
                                linea.botondenegar().onclick = function () { return comprobarlineasmodificadas(orden.ID, linea.ID, 'denegarlinea') };
                                orden.fechaActualizacion = parseInt(arrmsg[5]);
                                break;
                        }
                        break;
                    case 300: // El pedido ha sido anulado
                        orden.accion = '';
                        orden.accionOk = false;
                        MsgBox(ArrTextos[19], false, false);
                        break;
                    case 301: // El pedido se encuentra totalmente denegado
                        orden.accion = '';
                        orden.accionOk = false;
                        MsgBox(ArrTextos[3], false, false);
                        break;
                    case 302: // El pedido ha sido anulado
                        orden.accion = '';
                        orden.accionOk = false;
                        MsgBox(ArrTextos[13], false, false);
                        break;
                    case 303: // El aprobador no tiene importe suficiente para aprobar
                        orden.accion = '';
                        orden.accionOk = false;
                        MsgBox(ArrTextos[4] + '<br/>' + ArrTextos[5] & ' ' & arrmsg[5] & '<br/>' & ArrTextos[6] & ' ' & arrmsg[4], false, false);
                        break;
                    case 304:
                        break;
                    default:
                        MsgBox(ArrTextos[20], false, false);
                        break;
                }
                break;
            case 1: // Las líneas serán traspasadas a un nivel superior de aprobación
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[21], true, false);
                break;
            case 2: // La orden será emitida al proveedor
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[22] + '<br/>' + ArrTextos[1], true, false);
                break;
            case 3: // La línea ha sido eliminada por otro usuario
                orden.accion = '';
                orden.accionOk = false;
                MsgBox(ArrTextos[23], false, false);
                break;
            case 4: // El aprobador no tiene límite suficiente para aprobar y no hay nadie superior
                orden.accion = '';
                orden.accionOk = false;
                MsgBox(ArrTextos[24], false, false);
                break;
            case 5: // Hay líneas denegadas por el mismo usuario
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[7] + '<br/>' + ArrTextos[1], true, false);
                break;
            case 6: // Hay líneas denegadas por otro usuario
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[8] + '<br/>' + ArrTextos[1], true, false);
                break;
            case 7: // Hay líneas pendientes de otros aprobadores
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[9] + '<br/>' + ArrTextos[1], true, false);
                break;
            case 8: // El pedido pasará a parcialmente denegado, solicitar confirmación
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[10] + '<br/>' + ArrTextos[11], true, false);
                break;
        }
    }

    function AsyncConfirmarDenegacionLineaOk(msg) {
        var arrmsg = msg.split('#');
        var orden = ArrOrdenes[parseInt(arrmsg[0])];
        var linea = orden.Lineas[parseInt(arrmsg[1])];
        switch (parseInt(arrmsg[2])) {
            case 0: // No necesita confirmación
            case 2:
                orden.accion = 'denegarlineaAsync';
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[17], true, true);
                break;
            case 1: // Las líneas serán traspasadas a un nivel superior de aprobación
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[21] + '<br/><br/>' + ArrTextos[17], true, true);
                break;
            case 3: // La línea ha sido eliminada por otro usuario
                orden.accion = '';
                orden.accionOk = false;
                MsgBox(ArrTextos[25], false, false);
                break;
            case 4: // La orden queda pendiente y no hay nivel superior
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[14] + '<br/><br/>' + ArrTextos[17], true, true);
                break;
            case 5: // La orden queda parcialmente denegada
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[10] + '<br/>' + ArrTextos[11] + '<br/><br/>' + ArrTextos[17], true, true);
                break;
            case 6: // La orden queda denegada
                $get(BtnAceptarMje).orden = orden.ID;
                $get(BtnAceptarMje).onclick = Confirmar;
                MsgBox(ArrTextos[16] + '<br/><br/>' + ArrTextos[17], true, true);
                break;
        }
    }

    function AsyncDenegarLineaOk(msg) {
        var arrmsg = msg.split('#');
        var orden = ArrOrdenes[parseInt(arrmsg[0])];
        var linea = orden.Lineas[parseInt(arrmsg[1])];
        switch (parseInt(arrmsg[2])) {
            case 300: // El pedido ha sido anulado
                orden.accion = '';
                orden.accionOk = false;
                MsgBox(ArrTextos[26], false, false);
                break;
            case 301: // El pedido ha sido eliminado
                orden.accion = '';
                orden.accionOk = false;
                MsgBox(ArrTextos[3], false, false);
                break;
            case 302: // El pedido ha sido emitido por otro usuario
                orden.accion = '';
                orden.accionOk = false;
                MsgBox(ArrTextos[28]);
                break;
            case 303: // El aprobador no tiene importe suficiente para aprobar
                orden.accion = '';
                orden.accionOk = false;
                MsgBox(ArrTextos[14] + '<br/>' + ArrTextos[26] + ': ' + arrmsg[4] + '<br/>' + ArrTextos[5] + ' ' + arrmsg[3] + '<br/>' + ArrTextos[6] + ' ' + arrmsg[2], false, false);
                break;
            default:
                linea.botondenegar().src = ArrImages['denegado_act'].src;
                linea.botondenegar().disabled = true;
                linea.botonaprobar().src = ArrImages['aprobado_desact'].src;
                linea.botonaprobar().disabled = false;
                linea.botonaprobar().onclick = function () { return comprobarlineasmodificadas(orden.ID, linea.ID, 'aprobarlinea') };
                orden.fechaActualizacion = parseInt(arrmsg[3]);
                break;
        }
    }

    function MsgBox(Texto, MostrarCancelar, InputBox) {
        $get(LabelMje).innerHTML = Texto;
        if (InputBox) {
            $get(TextMje).style.display = 'block';
            $get(TextMje).value = '';
            $get(LabelMaxMje).style.display = 'block';
        }
        else {
            $get(TextMje).style.display = 'none';
            $get(LabelMaxMje).style.display = 'none';
        }

        if (MostrarCancelar) {
            $get(BtnCancelarMje).style.display = 'block';
        }
        else {
            $get(BtnCancelarMje).style.display = 'none';
            $get(BtnAceptarMje).onclick = $get(BtnCancelarMje).onclick;
        }
        $get(BtnCancelarMje).style.display = (MostrarCancelar) ? 'block' : 'none';
        $find(modalMje).show();
    }

    function Confirmar() {
        try {
            var ordenid = parseInt(this.orden);
            ArrOrdenes[ordenid].ejecutaraccion();
        }
        catch (e) {
            $find(modalMje).hide();
        }
        return false;
    }

    function NotificarOrden(IdOrden, IdPedido, addressFrom, iNotificar) {
        //Notificar
        var params = JSON.stringify({ IdOrden: IdOrden, IdPedido: IdPedido, addressFrom: addressFrom, iNotificar: iNotificar });        
        $.ajax({
            type: "POST",
            url: rutaFS + 'EP/Aprobacion.aspx/NotificarOrden',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: false
        });
    }
    function togglePedidosContraAbierto(){
        $("#tdPedidosAbiertos").toggle();
    };
    function togglePedidosCatalogo() {
        $("#tdPedidosCatalogo").toggle();
    };


    

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    <input type="hidden" id="collapseLineas" runat="server" value="" />
    <!-- Panel Info Proveedor -->
    <fsn:FSNPanelInfo ID="FSNPanelDatosProve" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx" ServiceMethod="Proveedor_DatosContacto" TipoDetalle="1" />
    <!-- Panel Persona -->
    <fsn:FSNPanelInfo ID="FSNPanelDatosPersona" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx" ServiceMethod="Persona_Datos" />
    <!-- Panel Persona -->
    <fsn:FSNPanelInfo ID="FSNPanelDatosPedidoAbierto" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx" ServiceMethod="Detalle_PedidoAbierto_Datos" />
    <!-- Paneles Tooltip -->
    <fsn:FSNPanelTooltip ID="pnlTooltipObservaciones" runat="server" ServiceMethod="Observaciones">
    </fsn:FSNPanelTooltip>
    <fsn:FSNPanelTooltip ID="pnlTooltipObservacionesLinea" runat="server" ServiceMethod="ObservacionesLinea"></fsn:FSNPanelTooltip>
    <fsn:FSNPanelTooltip ID="pnlTooltipTexto" runat="server" ServiceMethod="DevolverTexto"></fsn:FSNPanelTooltip>
    <!-- Panel Detalle Artículo -->
    <fsep:DetArticuloPedido ID="pnlDetalleArticulo" runat="server" />
    <!-- Panel Categorías -->
    <asp:UpdatePanel ID="updpnlPopupCategorias" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <input id="btnOcultoCat" type="button" value="button" runat="server" style="display: none" />
            <asp:Panel ID="PanelOculto" runat="server" Style="display: none">
            </asp:Panel>
            <asp:Panel ID="PanelArbol" runat="server" BackColor="White" BorderColor="DimGray"
                BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display: none;
                min-width: 350px; padding: 2px" onmouseout="FSNCerrarPanelesTemp()" onmouseover="FSNNoCerrarPaneles()">
                <table width="350">
                    <tr>
                        <td align="left">
                            <asp:Image ID="ImgCatalogoArbol" runat="server" ImageUrl="~/images/categorias_small.gif" />
                            &nbsp; &nbsp;
                            <asp:Label ID="LblCatalogoArbol" runat="server" CssClass="Rotulo">
                            </asp:Label>
                        </td>
                        <td align="right">
                            <asp:ImageButton runat="server" ID="ImgBtnCerrarPanelArbol" ImageUrl="~/images/Bt_Cerrar.png" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:UpdatePanel ID="pnlCategorias" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:TreeView ID="fsnTvwCategorias" NodeIndent="20" CssClass="Normal" 
                                                ShowExpandCollapse="true" ExpandDepth="0" EnableViewState="True"
                                                runat="server" EnableClientScript="true" ForeColor="Black" PopulateNodesFromClient="true">
                                                <Nodes>
                                                    <asp:TreeNode PopulateOnDemand="true" Value="_0_" SelectAction="None" Expanded="false"></asp:TreeNode>
                                                </Nodes>
                                                </asp:TreeView>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>  
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajx:ModalPopupExtender ID="mpePanelArbol" runat="server" PopupControlID="PanelArbol"
                TargetControlID="btnOcultoCat" CancelControlID="ImgBtnCerrarPanelArbol" RepositionMode="None"
                PopupDragHandleControlID="PanelOculto">
            </ajx:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
<!-- Panel Confirmacion Generar Listado -->
<asp:Panel runat="server" BackColor="White" BorderColor="DimGray" ID="pnlConfGenerarInforme"
        BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display: none;
        min-width: 150px; padding: 2px">
        <asp:Table runat="server" ID="TConfirmacionGenLis">
        <asp:TableRow>
        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center">
        <asp:Image ID="imgInfo" runat="server" ImageUrl="~/images/icono_info.gif" ImageAlign="Left" />
        &nbsp;&nbsp;
        <asp:Label ID="LblConfirmGenerarListado" runat="server" CssClass="normal" Text="DSe va a proceder con la generación del informe.¿Continuar?">
        </asp:Label>
        </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
        <asp:TableCell HorizontalAlign="Right" Width="40%">
        <fsn:FSNButton CssClass="Normal" ID="BtnAceptarGenerarInforme" runat="server" Alineacion="Right"
            OnClientClick="window.open('InformeExcelOrdenes.aspx' ,'_blank','height=300,width=300,menubar=no,scrollbars=no,resizable=no,toolbar=no,addressbar=no'); return false;" >
        </fsn:FSNButton>
        </asp:TableCell>
        <asp:TableCell Width="20%">
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Left" Width="40%">
        <fsn:FSNButton CssClass="Normal" ID="BtnCancelarGenerarInforme" runat="server" Alineacion="Left" Text="DCancelar">
        </fsn:FSNButton>
        </asp:TableCell>
        </asp:TableRow>
        </asp:Table>
        </asp:Panel>
        <button id="BtnOcGenerarInforme" runat="server" style="display:none"></button>
 <ajx:ModalPopupExtender ID="mpeConfGenerarInforme" runat="server" PopupControlID="pnlConfGenerarInforme"
 TargetControlID="BtnOcGenerarInforme" CancelControlID="BtnAceptarGenerarInforme">
 </ajx:ModalPopupExtender>
    <!-- Panel Adjuntos -->
    <input id="btnOcultoAdj" type="button" value="button" runat="server" style="display: none" />
    <asp:Panel ID="pnlAdjuntos" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left" Style="display: none;
        min-width: 450px; padding: 2px" Width="450px">
        <table width="100%">
            <tr>
                <td align="left">
                    <table cellpadding="4" cellspacing="0" border="0">
                        <tr>
                            <td valign="bottom">
                                <asp:Image ID="imgCabAdjuntos" runat="server" SkinID="CabAdjuntos" />
                            </td>
                            <td valign="bottom">
                                <span id="lblArchAdjuntos" class="RotuloGrande">DArchivos adjuntados a la línea de pedido</span>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="right" valign="top">
                    <asp:ImageButton ID="ImgBtnCerrarpnlAdjuntos" runat="server" ImageUrl="~/images/Bt_Cerrar.png" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="capaadjuntos">
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpeAdj" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="ImgBtnCerrarpnlAdjuntos" PopupControlID="pnlAdjuntos" TargetControlID="btnOcultoAdj">
    </ajx:ModalPopupExtender>

    <!-- Panel Confirmación -->
    <input id="btnOcultoConfirm" type="button" value="button" runat="server" style="display: none" />
    <asp:Panel ID="pnlConfirm" runat="server" BackColor="White" BorderColor="DimGray"
            BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Center" Style="display: none;
            min-width: 300px; padding: 20px" Width="300px">
        <asp:UpdatePanel ID="upConfirm" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table cellpadding="0" cellspacing="20" border="0">
                    <tr><td colspan="2" align="left"><asp:Label ID="lblConfirm" runat="server" Text="Label"></asp:Label></td></tr>
                    <tr><td colspan="2" align="left"><asp:Label ID="lblMaxLength" runat="server" Text="" style="display:block;"></asp:Label></td></tr>
                    <tr><td colspan="2" align="left"><asp:TextBox ID="txtConfirm" runat="server" TextMode="MultiLine" Rows="4" MaxLength="4000" Width="100%" style="display:none;"></asp:TextBox></td></tr>
                    <tr>
                        <td align="right"><fsn:FSNButton ID="btnAceptarConfirm" runat="server" Text="FSNButton"></fsn:FSNButton></td>
                        <td align="left"><fsn:FSNButton ID="btnCancelarConfirm" runat="server" Text="FSNButton" Alineacion="Left" style="display:block;"></fsn:FSNButton></td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpeConfirm" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlConfirm" TargetControlID="btnOcultoConfirm"></ajx:ModalPopupExtender>

    <!-- Cabecera -->
    <aspf:WebPartManager ID="WebPartManager1" runat="server">
    </aspf:WebPartManager>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/aprobacion.gif"
                    Style="margin-bottom: 0px" />
                <asp:Label ID="lblTitulo" runat="server" CssClass="RotuloGrande" Height="51px" Text="DAprobación de Pedidos"> 
                </asp:Label>
            </td>
            <td align="right">
                <aspf:WebPartZone ID="WebPartZone1" runat="server" EnableTheming="False" PartChromeType="None"
                    LayoutOrientation="Horizontal" PartStyle-Height="75px" Padding="20">
                    <PartStyle Height="75px"></PartStyle>
                    <ZoneTemplate>
                        <fsn:FSEPWebPartBusqArticulos ID="FSEPWebPartBusqArticulos1" runat="server" AuthorizationFilter="EPAprovisionador" />
                        <fsn:FSEPWebPartCesta ID="FSEPWebPartCesta1" runat="server" Width="300px" AuthorizationFilter="EPAprovisionador" />
                    </ZoneTemplate>
                    <PartChromeStyle CssClass="Rectangulo" />
                </aspf:WebPartZone>
            </td>
        </tr>
    </table>

    <!-- Buscador -->
    <asp:UpdatePanel ID="pnlBuscador" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
    <asp:Panel ID="pnlCabeceraAprobacion" runat="server" SkinID="PanelColapsable">
        <table cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td valign="middle"><asp:Image ID="imgExpandir" runat="server" ImageUrl="~/images/contraer.gif" /></td>
                <td valign="middle"><asp:Label ID="lblCabecera" runat="server" Text="DBuscador de Pedidos:" Font-Bold="True" ForeColor="White"></asp:Label></td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlBusqueda" runat="server" DefaultButton="btnBuscar" style="height: 0px; overflow: hidden;">
        <asp:Table ID="tblBusqueda" runat="server" CellPadding="4" CellSpacing="0" BorderWidth="0">
            <asp:TableRow>
                <asp:TableCell>
                    <asp:Literal ID="litAnio" runat="server" Text="DAño:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:DropDownList ID="ddlAnio" runat="server" DataTextField="Text" DataValueField="Value" Width="100px" />
                </asp:TableCell>
                <asp:TableCell Width="80px">
                    <asp:Literal ID="litCesta" runat="server" Text="DNº de cesta:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="txtCesta" runat="server" Width="96" onchange="validarsolonum(this)" onkeypress="return validarkeynum()" onpaste="validarpastenum()"></asp:TextBox>
                    <ajx:TextBoxWatermarkExtender ID="txtCesta_TextBoxWatermarkExtender" 
                        runat="server" Enabled="True" TargetControlID="txtCesta" 
                        WatermarkText="DNº de cesta">
                    </ajx:TextBoxWatermarkExtender>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Literal ID="litPedido" runat="server" Text="DNº de pedido:" ></asp:Literal>
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Right">
                    <asp:TextBox ID="txtPedido" runat="server" Width="100" onchange="validarsolonum(this)" onkeypress="return validarkeynum()" onpaste="validarpastenum()"></asp:TextBox>
                    <ajx:TextBoxWatermarkExtender ID="txtPedido_TextBoxWatermarkExtender" 
                        runat="server" Enabled="True" TargetControlID="txtPedido" 
                        WatermarkText="DNº de pedido">
                    </ajx:TextBoxWatermarkExtender>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Literal ID="litPedidoProve" runat="server" Text="DNº de pedido proveedor:" ></asp:Literal>
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left">
                    <asp:TextBox ID="txtPedidoProve" runat="server" Width="100"></asp:TextBox>
                    <ajx:TextBoxWatermarkExtender ID="txtPedidoProve_TextBoxWatermarkExtender" 
                        runat="server" Enabled="True" TargetControlID="txtPedidoProve" 
                        WatermarkText="DNº de pedido" WatermarkCssClass="WaterMark">
                    </ajx:TextBoxWatermarkExtender>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Literal ID="litPedidoERP" runat="server" Text="DNº de pedido ERP:" ></asp:Literal>
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Right">
                    <asp:TextBox ID="txtPedidoERP" runat="server" Width="100"></asp:TextBox>
                    <ajx:TextBoxWatermarkExtender ID="txtPedidoERP_TextBoxWatermarkExtender" 
                        runat="server" Enabled="True" TargetControlID="txtPedidoERP" 
                        WatermarkText="DNº de pedido">
                    </ajx:TextBoxWatermarkExtender>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <asp:Literal ID="litDesde" runat="server" Text="DDesde:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell ID="celdaDteDesde" runat="server">
                    <igpck:WebDatePicker ID="dteDesde" runat="server" Width="100px" SkinID="Calendario">
                    </igpck:WebDatePicker>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Literal ID="litHasta" runat="server" Text="DHasta:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell ID="celdaDteHasta" runat="server">
                    <igpck:WebDatePicker ID="dteHasta" runat="server" Width="100px" SkinID="Calendario">
                    </igpck:WebDatePicker>
                </asp:TableCell>
                <asp:TableCell ID="celdaLitProvedorERP" runat="server">
                    <asp:Literal ID="litProvedorERP" runat="server" Text="DProveedor ERP:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell ID="celdaTxtProvedorERP" runat="server" HorizontalAlign="Right">
                    <asp:TextBox ID="txtProveedorERP" runat="server" Width="100"></asp:TextBox>
                    <ajx:TextBoxWatermarkExtender ID="txtProveedorERP_TextBoxWatermarkExtender" 
                        runat="server" Enabled="True" TargetControlID="txtProveedorERP" 
                        WatermarkText="DProveedor ERP">
                    </ajx:TextBoxWatermarkExtender>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Literal ID="litReceptor" runat="server" Text="DReceptor:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell ColumnSpan="3" HorizontalAlign="Left">
                    <asp:DropDownList ID="ddlReceptor" runat="server" DataTextField="Text" DataValueField="Value" Width="99%"></asp:DropDownList>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <asp:Literal ID="litEmpresa" runat="server" Text="DEmpresa:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell ColumnSpan="5" HorizontalAlign="Right">
                    <asp:DropDownList ID="ddlEmpresa" runat="server" DataTextField="Text" DataValueField="Value" Width="100%"></asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Literal ID="litProveedor" runat="server" Text="DProveedor:"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell ColumnSpan="3" HorizontalAlign="Left">
                    <asp:DropDownList ID="ddlProveedor" runat="server" DataTextField="Text" DataValueField="Value" Width="99%"></asp:DropDownList>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <asp:Literal ID="litAprovisionador" runat="server" Text="DAprovisionador"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell ColumnSpan="5" HorizontalAlign="Right">
                    <asp:DropDownList ID="ddlAprovisionador" runat="server" DataTextField="Text" DataValueField="Value" Width="100%"></asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Literal ID="litGestor" runat="server" Text="DAprovisionador"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell ColumnSpan="3" HorizontalAlign="Left">
                    <asp:DropDownList ID="ddlGestor" runat="server" DataTextField="Text" DataValueField="Value" Width="99%"></asp:DropDownList>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="10">
                    <asp:CheckBoxList ID="chklstEstado" runat="server" RepeatDirection="Horizontal" Width="40%" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan ="2" >
                    <asp:CheckBox ID="chkPedidosCatalogo"  ClientIDMode="Static" Checked ="true" Text="dPedidos Catálogo" runat="server" Width="40%" Style="display:inline" onchange="togglePedidosCatalogo();" />
                </asp:TableCell>
                <asp:TableCell ID="tdPedidosCatalogo" runat="server" ClientIDMode="Static" >
                    <asp:UpdatePanel ID="updpnlCategorias" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <fsn:FSNLinkInfo ID="BtnCategoria" runat="server" CssClass="Rotulo"
                                style="text-decoration:none" PanelInfo="mpePanelArbol">
                                <asp:Literal ID="LnkBtnCategoria" runat="server"></asp:Literal>
                                &nbsp;<asp:Image ID="imgDespCategoria" runat="server" SkinID="desplegar" /></fsn:FSNLinkInfo> &nbsp; &nbsp;
                            <asp:Literal ID="litCategoria" runat="server" Visible="False"></asp:Literal>
                            <asp:LinkButton ID="lnkQuitarCategoria" runat="server" Visible="False" Text="(x)" CssClass="Normal" style="text-decoration:none"></asp:LinkButton>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="fsnTvwCategorias" />
                        </Triggers>
                    </asp:UpdatePanel>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="trPedidosAbiertos" runat="server">
                <asp:TableCell ColumnSpan ="2" >
                    <asp:CheckBox ID="chkPedidosContraAbierto" ClientIDMode="Static" Checked ="true" Text="dPedidos Contra Abierto" runat="server" Width="40%" Style="display:inline" onchange ="togglePedidosContraAbierto();"/>
                </asp:TableCell>
                <asp:TableCell ID="tdPedidosAbiertos" runat="server" ClientIDMode ="Static"  ColumnSpan="4">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell >
                                <asp:Label ID="litAnyoAbierto" runat="server" Text="DPedido Abierto" CssClass="Rotulo" ></asp:Label>
                            </asp:TableCell>
                            <asp:TableCell >
                                <asp:DropDownList ID="ddlAnyoAbierto" runat="server" DataTextField="Text" DataValueField="Value" Width="100px" />
                            </asp:TableCell>
                            <asp:TableCell >
                                <asp:TextBox ID="txtCestaAbierto" runat="server" Width="96" onchange="validarsolonum(this)" onkeypress="return validarkeynum()" onpaste="validarpastenum()"></asp:TextBox>
                                <ajx:TextBoxWatermarkExtender ID="txtCestaAbierto_TextBoxWatermarkExtender" runat="server" Enabled="True"
                                    TargetControlID="txtCestaAbierto" WatermarkText="DNº de cesta">
                                </ajx:TextBoxWatermarkExtender>
                            </asp:TableCell>
                            <asp:TableCell >
                                <asp:TextBox ID="txtPedidoAbierto" runat="server" Width="100" onchange="validarsolonum(this)" onkeypress="return validarkeynum()" onpaste="validarpastenum()"></asp:TextBox>
                                <ajx:TextBoxWatermarkExtender ID="txtPedidoAbierto_TextBoxWatermarkExtender" 
                                    runat="server" Enabled="True" TargetControlID="txtPedidoAbierto" 
                                    WatermarkText="DNº de pedido" WatermarkCssClass="WaterMark">
                                </ajx:TextBoxWatermarkExtender>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
                
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="10">
                    <fsn:FSNButton ID="btnBuscar" runat="server" Text="DBuscar" Alineacion="Right"></fsn:FSNButton>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:Panel>
    <ajx:CollapsiblePanelExtender ID="cpeBusqueda" runat="server" 
        CollapseControlID="pnlCabeceraAprobacion" CollapsedImage="~/images/expandir.gif" 
        ExpandControlID="pnlCabeceraAprobacion" ExpandedImage="~/images/contraer.gif" 
        ImageControlID="imgExpandir" SuppressPostBack="True" 
        TargetControlID="pnlBusqueda" Collapsed="True">
    </ajx:CollapsiblePanelExtender>
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- Lista Pedidos -->
    <asp:UpdatePanel ID="pnlGrid" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
    <ContentTemplate>
    <div id="divTabla1" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr><td colspan="2"><hr size="1" /></td></tr>
        <tr>
            <td align="left">
                <table cellpadding="4" border="0">
                    <tr>
                        <td>
                            <asp:Label ID="lblViendo" runat="server" Text="DViendo Pedidos" CssClass="Etiqueta"></asp:Label>&nbsp;
                            <asp:Label ID="lblResul" runat="server" Text="0-0" CssClass="Etiqueta"></asp:Label>&nbsp;
                            <asp:Label ID="lblResulDe" runat="server" Text="Dde" CssClass="Etiqueta"></asp:Label>&nbsp;
                            <asp:Label ID="lblResulTot" runat="server" Text="0" CssClass="Etiqueta"></asp:Label>
                        </td>
                        <td>&nbsp;</td><td>&nbsp;</td>
                        <td><asp:ImageButton runat="server" ID="btnFirstPage" /></td>
                        <td><asp:ImageButton runat="server" ID="btnPreviousPage" /></td>
                        <td><asp:Label ID="lblPagina" runat="server" Text="DPágina" CssClass="Etiqueta"></asp:Label></td>
                        <td><asp:DropDownList ID="ddlPage" runat="server" CausesValidation="False" AutoPostBack="True"></asp:DropDownList></td>
                        <td>
                            <asp:Label ID="lblDe" runat="server" Text="Dde" CssClass="Etiqueta"></asp:Label>&nbsp;
                            <asp:Label ID="lblPagTot" runat="server" Text="1" CssClass="Etiqueta"></asp:Label>
                        </td>
                        <td><asp:ImageButton runat="server" ID="btnNextPage" /></td>
                        <td><asp:ImageButton runat="server" ID="btnLastPage" /></td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <table cellpadding="4" border="0">
                    <tr>
                        <td><asp:Label ID="lblOrdenacion" runat="server" Text="DOrdenar por:" CssClass="Etiqueta"></asp:Label></td>
                        <td><asp:DropDownList ID="ddlOrdenacion" runat="server" AutoPostBack="True"></asp:DropDownList></td>
                        <td><asp:ImageButton ID="imgBtnOrdenacion" runat="server" /></td>
                        <td><asp:ImageButton ID="imgbtnExportar" runat="server" /></td>
                        <td><asp:ImageButton ID="imgbtnImprimir" runat="server" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td colspan="2"><hr size="1" /></td></tr>
    </table>
    </div> 
    <asp:DataList ID="dlOrdenes" runat="server" ExtractTemplateRows="True" 
        Width="100%" CellPadding="4" EnableViewState="False">
        <ItemStyle CssClass="FilaImpar" />
        <AlternatingItemStyle CssClass="FilaPar" />
        <HeaderTemplate>
            <asp:Table ID="tblCabecera" runat="server">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell></asp:TableHeaderCell>
                    <asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabEmpresa" runat="server"></asp:Literal></asp:TableHeaderCell>
                    <asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabPedido" runat="server"></asp:Literal></asp:TableHeaderCell>
                    <asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabPedidoERP" runat="server"></asp:Literal></asp:TableHeaderCell>
                    <asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabPedidoProve" runat="server"></asp:Literal></asp:TableHeaderCell>
                    <asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabProveedor" runat="server"></asp:Literal></asp:TableHeaderCell>
                    <asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabProveedorERP" runat="server"></asp:Literal></asp:TableHeaderCell>
                    <asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabAprovisionador" runat="server"></asp:Literal></asp:TableHeaderCell>
                    <asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabReceptor" runat="server"></asp:Literal></asp:TableHeaderCell>
                    <asp:TableHeaderCell ><asp:Literal ID="litCabImporte" runat="server"></asp:Literal></asp:TableHeaderCell>
                    <asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabEstado" runat="server"></asp:Literal></asp:TableHeaderCell>
                    <asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabMotivo" runat="server"></asp:Literal></asp:TableHeaderCell>
                    <asp:TableHeaderCell HorizontalAlign="Left"><asp:Literal ID="litCabSituacionActual" runat="server"></asp:Literal></asp:TableHeaderCell>
                    <asp:TableHeaderCell></asp:TableHeaderCell>
                    <asp:TableHeaderCell></asp:TableHeaderCell>
                    <asp:TableHeaderCell></asp:TableHeaderCell>
                    <asp:TableHeaderCell></asp:TableHeaderCell>
                </asp:TableHeaderRow>
            </asp:Table>
        </HeaderTemplate>
        <HeaderStyle CssClass="FilaPar" />
        <ItemTemplate>
            <asp:Table ID="tblItem" runat="server">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top"><asp:HyperLink ID="hypVerDetalleOrden" ForeColor="Blue" Font-Underline="true" style="cursor:pointer; white-space:nowrap;" runat="server" CommandName="IrDetallePedido" CommandArgument='<%# Eval("ID") %>'></asp:HyperLink></asp:TableCell>
                    <asp:TableCell VerticalAlign="Top"><asp:Literal ID="litFilaEmpresa" runat="server" Text='<%# Eval("DENEMPRESA") %>'></asp:Literal></asp:TableCell>
                    <asp:TableCell VerticalAlign="Top"><asp:Literal ID="litFilaPedido" runat="server"></asp:Literal></asp:TableCell>
                    <asp:TableCell VerticalAlign="Top"><asp:Literal ID="litFilaPedidoERP" runat="server"></asp:Literal></asp:TableCell>
                    <asp:TableCell VerticalAlign="Top"><asp:Literal ID="litFilaPedidoProve" runat="server" Text='<%# Eval("NUMEXT") %>'></asp:Literal></asp:TableCell>
                    <asp:TableCell VerticalAlign="Top"><fsn:FSNLinkInfo ID="fsnlnkFilaProveedor" runat="server" PanelInfo="FSNPanelDatosProve" ContextKey='<%# Eval("PROVECOD") %>' CssClass="Rotulo" style="text-decoration:none;"></fsn:FSNLinkInfo></asp:TableCell>
                    <asp:TableCell VerticalAlign="Top"><asp:Literal ID="litFilaProveedorERP" runat="server"></asp:Literal></asp:TableCell>
                    <asp:TableCell VerticalAlign="Top"><fsn:FSNLinkInfo ID="fsnlnkFilaAprovisionador" runat="server" Text='<%# Eval("APROVISIONADOR") %>' ContextKey='<%# Eval("APROVISIONADOR") %>' PanelInfo="FSNPanelDatosPersona" CssClass="Rotulo" style="text-decoration:none;"></fsn:FSNLinkInfo></asp:TableCell>
                    <asp:TableCell VerticalAlign="Top"><fsn:FSNLinkInfo ID="fsnlinkFilaReceptor" runat="server" Text='<%# Eval("RECEPTOR") %>' ContextKey='<%# Eval("RECEPTOR") %>' PanelInfo="FSNPanelDatosPersona" CssClass="Rotulo" style="text-decoration:none;"></fsn:FSNLinkInfo></asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Top"><asp:Literal ID="litFilaImporte" runat="server"></asp:Literal></asp:TableCell>
                    <asp:TableCell VerticalAlign="Top"><asp:Literal ID="litFilaEstado" runat="server"></asp:Literal></asp:TableCell>
                    <asp:TableCell VerticalAlign="Top">
                        <asp:Literal ID="litFilaMotivo" runat="server"></asp:Literal>
                        <fsn:FSNLinkInfo ID="FSNLinkFilaPedidoAbierto" runat="server" Text='<%# IIF(NOT ISDBNULL(Eval("ANYO_PED_ABIERTO")),textos(180) & " : " & Eval("ANYO_PED_ABIERTO") & "/" & EVAL("CESTA_PED_ABIERTO") & "/" & EVAL("PEDIDO_PED_ABIERTO"),"") %>' ContextKey='<%# Eval("ORDEN_PED_ABIERTO") %>' PanelInfo="FSNPanelDatosPedidoAbierto" CssClass="Rotulo" style="text-decoration:none;"></fsn:FSNLinkInfo>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Top"><asp:HyperLink ID="hypFilaSituacionActual" style="cursor:pointer" runat="server"></asp:HyperLink></asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="30"><asp:ImageButton ID="btnFilaAprobar" runat="server" CommandName="Aprobar" CommandArgument='<%# Eval("COD_ORDEN_APROBAR") & "_" & Eval("MON") & "_" & Eval("TIPO") %>' /></asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="30"><asp:ImageButton ID="btnFilaDenegar" runat="server" CommandName="Denegar" CommandArgument='<%# Eval("COD_ORDEN_RECHAZAR") & "_" & Eval("MON") & "_" & Eval("TIPO")%>' /></asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="30"><asp:ImageButton ID="btnFilaExcel" runat="server" SkinID="Excel" CommandName="Exportar" CommandArgument='<%# Eval("ID") %>' /></asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="30"><asp:ImageButton ID="btnFilaImprimir" runat="server" SkinID="Imprimir" CommandName="Imprimir" CommandArgument='<%# Eval("ID") %>' /></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </ItemTemplate>
    </asp:DataList>
    <div id="divTabla2" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr><td colspan="2"><hr size="1" /></td></tr>
        <tr>
            <td align="left">
                <table cellpadding="4" border="0" >
                    <tr>
                        <td>
                            <asp:Label ID="lblViendo2" runat="server" Text="DViendo Pedidos" CssClass="Etiqueta"></asp:Label>&nbsp;
                            <asp:Label ID="lblResul2" runat="server" Text="0-0" CssClass="Etiqueta"></asp:Label>&nbsp;
                            <asp:Label ID="lblResulDe2" runat="server" Text="Dde" CssClass="Etiqueta"></asp:Label>&nbsp;
                            <asp:Label ID="lblResulTot2" runat="server" Text="0" CssClass="Etiqueta"></asp:Label>
                        </td>
                        <td>&nbsp;</td><td>&nbsp;</td>
                        <td><asp:ImageButton runat="server" ID="btnFirstPage2" /></td>
                        <td><asp:ImageButton runat="server" ID="btnPreviousPage2" /></td>
                        <td><asp:Label ID="lblPagina2" runat="server" Text="DPágina" CssClass="Etiqueta"></asp:Label></td>
                        <td><asp:DropDownList ID="ddlPage2" runat="server" CausesValidation="False" AutoPostBack="True"></asp:DropDownList></td>
                        <td>
                            <asp:Label ID="lblDe2" runat="server" Text="Dde" CssClass="Etiqueta"></asp:Label>&nbsp;
                            <asp:Label ID="lblPagTot2" runat="server" Text="1" CssClass="Etiqueta"></asp:Label>
                        </td>
                        <td><asp:ImageButton runat="server" ID="btnNextPage2" /></td>
                        <td><asp:ImageButton runat="server" ID="btnLastPage2" /></td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <table cellpadding="4" border="0" >
                    <tr>
                        <td><asp:Label ID="lblOrdenacion2" runat="server" Text="DOrdenar por:" CssClass="Etiqueta"></asp:Label></td>
                        <td><asp:DropDownList ID="ddlOrdenacion2" runat="server" AutoPostBack="True"></asp:DropDownList></td>
                        <td><asp:ImageButton ID="imgBtnOrdenacion2" runat="server" /></td>
                        <td><asp:ImageButton ID="imgbtnExportar2" runat="server" /></td>
                        <td><asp:ImageButton ID="imgbtnImprimir2" runat="server" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td colspan="2"><hr size="1" /></td></tr>
    </table>
    </div>
    <asp:Literal ID="litNoPedidos" runat="server" Visible="false"></asp:Literal>
    </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="dlOrdenes" EventName="DataBinding" />
        </Triggers>
    </asp:UpdatePanel>

    <!-- Panel Cargando... -->

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);
        function beginReq(sender, args) {
            try {
                // shows the Popup
                var Emisor;
                var Comparacion = new Array();
                Emisor = args._postBackElement.id;
                Comparacion[0] = /btnBuscar/i;
                Comparacion[1] = /btnFirstPage/i;
                Comparacion[2] = /btnPreviousPage/i;
                Comparacion[3] = /ddlPage/i;
                Comparacion[4] = /btnNextPage/i;
                Comparacion[5] = /btnLastPage/i;
                Comparacion[6] = /ddlOrdenacion/i;
                Comparacion[7] = /imgBtnOrdenacion/i;
                Comparacion[8] = /lnkDenArt/i;
                Comparacion[9] = /btnRegistrar/i;
                Comparacion[10] = /btnCat/i;
                Comparacion[11] = /TvwCategorias/i;
                for (var c in Comparacion) {
                    if (String(Emisor).search(Comparacion[c]) != -1) {
                        $find(ModalProgress).show();
                        break;
                    }
                }
            }
            catch (e) { }
        }

        function endReq(sender, args) {
            //  shows the Popup 
            $find(ModalProgress).hide();
        } 
    </script>

</asp:Content>
