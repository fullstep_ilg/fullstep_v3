﻿Public Partial Class ParametrosEP
    Inherits FSEPPage


    ''' Revisado por: blp. Fecha 24/02/2012
    ''' <summary>
    ''' Evento de carga de la página, cargando los textos y los datos a mostrar
    ''' </summary>
    ''' <param name="sender">La propia página al cargarse</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Cada vez que se carga la página
    ''' Tiempo máximo: 0,3 seg</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            Exit Sub
        End If
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Parametros
        Call RedibujarPantalla()
        Call CargarTextos()
        Call CargarDatosCombos()
    End Sub

    ''' Revisado por: blp. Fecha: 08/03/2012
    ''' <summary>
    ''' Procedimiento que registra un script para redimensionar la pantalla de Parámetros dependiendo de las longitudes de destino del usuario y de los textos de las opciones, para que entre todo en una línea
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: El page_load
    ''' Tiempo máximo: 0 seg</remarks>
    Public Sub RedibujarPantalla()
        Dim oPer As FSNServer.CPersona
        Dim LongitudDestino As Integer = 0
        oPer = FSNServer.Get_Object(GetType(FSNServer.CPersona))
        oPer.Cod = FSNUser.CodPersona
        oPer.CargarDestinos(FSNUser.Idioma)
        If oPer.Destinos Is Nothing OrElse oPer.Destinos.Count = 0 Then

        Else
            Dim ContDest As Integer = 0
            For Each destino As FSNServer.CDestino In oPer.Destinos
                If destino.Den.Length > LongitudDestino Then
                    LongitudDestino = destino.Den.Length
                End If
            Next
        End If
        Dim MasAnchura As Integer = 0
        If lblDestDef.Text.Length > MasAnchura Then
            MasAnchura = lblDestDef.Text.Length
        End If
        If lblMostImgArt.Text.Length > MasAnchura Then
            MasAnchura = lblMostImgArt.Text.Length
        End If
        If lblMostAtrib.Text.Length > MasAnchura Then
            MasAnchura = lblMostAtrib.Text.Length
        End If
        If lblMostCantMin.Text.Length > MasAnchura Then
            MasAnchura = lblMostCantMin.Text.Length
        End If
        If lblOcCodProve.Text.Length > MasAnchura Then
            MasAnchura = lblOcCodProve.Text.Length
        End If
        If lblOcCodArt.Text.Length > MasAnchura Then
            MasAnchura = lblOcCodArt.Text.Length
        End If
        If lblRecepcionesPendientes.Text.Length > MasAnchura Then
            MasAnchura = lblRecepcionesPendientes.Text.Length
        End If
        Dim altura As Integer = 460

        If Acceso.gbAccesoFSSM Then

            If FSNUser.PermisoVerPedidosCCImputables Then
                chkSeguimientoPedidos.Items(0).Selected = FSNUser.SeguimientoVerPedidosUsuario
                chkSeguimientoPedidos.Items(1).Selected = FSNUser.SeguimientoVerPedidosCCImputables

                tblSeguimiento.Visible = True
                altura = altura + 70
                chkSeguimientoPedidos.Items(0).Attributes.Add("onClick", "ValidacionChecks(0, '" & chkSeguimientoPedidos.ClientID & "')")
                chkSeguimientoPedidos.Items(1).Attributes.Add("onClick", "ValidacionChecks(1, '" & chkSeguimientoPedidos.ClientID & "')")
            End If
            If FSNUser.PermisoRecepcionarPedidosCCImputables Then
                chkRecepcionPedidos.Items(0).Selected = FSNUser.RecepcionVerPedidosUsuario
                chkRecepcionPedidos.Items(1).Selected = FSNUser.RecepcionVerPedidosCCImputables

                chkRecepcionPedidos.Visible = True
                altura = altura + 60
                chkRecepcionPedidos.Items(0).Attributes.Add("onClick", "ValidacionChecks(0, '" & chkRecepcionPedidos.ClientID & "')")
                chkRecepcionPedidos.Items(1).Attributes.Add("onClick", "ValidacionChecks(1, '" & chkRecepcionPedidos.ClientID & "')")
            End If

        End If

        Dim Anchura As Integer = 230
        Anchura = Anchura + 5 * (LongitudDestino) + 2 * (MasAnchura)

        If Not Page.ClientScript.IsStartupScriptRegistered(Me.Page.GetType(), "Resize") Then
            Dim sScript As String = "<script>resize('" & Anchura & "','" & altura & "');</script>"
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "Resize", sScript, False)
        End If
    End Sub

    ''' Revisado por: blp. Fecha:24/02/2012
    ''' <summary>
    ''' Procedimiento que carga los textos a los controles
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: El Page_Load
    ''' Tiempo máximo: 0 seg</remarks>
    Public Sub CargarTextos()
        lblDestDef.Text = Textos(15) & ":"
        lblMostImgArt.Text = Textos(16) & ":"
        lblMostAtrib.Text = Textos(17) & ":"
        lblMostCantMin.Text = Textos(18) & ":"
        lblOcCodProve.Text = Textos(19) & ":"
        lblOcCodArt.Text = Textos(20) & ":"
        lblRecepcionesPendientes.Text = Textos(32) & ":"
        btnAceptar.Text = Textos(13)
        btnAceptar.ToolTip = Textos(13)
        lblTitulo.Text = Textos(26)
        lblLegendSeguimiento.Text = Textos(31) & ":" 'Seguimiento de pedidos:
        chkSeguimientoPedidos.Items(0).Text = Textos(29) '"Ver sólo mis pedidos"
        chkSeguimientoPedidos.Items(1).Text = Textos(30) '"Ver pedidos de otros usuarios"
        lblLegendRecepcion.Text = Textos(33) 'Recepción de pedidos
        chkRecepcionPedidos.Items(0).Text = Textos(35) 'Recepcionar mis pedidos
        chkRecepcionPedidos.Items(1).Text = Textos(34) 'Recepcionar pedidos de otros usuarios

    End Sub

    ''' <summary>
    ''' Procedimiento que carga los datos a mostrar en la página según los parámetros de usuario
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: El page_load
    ''' Tiempo máximo: 0,3 seg</remarks>
    Public Sub CargarDatosCombos()
        Dim oPer As FSNServer.CPersona
        oPer = FSNServer.Get_Object(GetType(FSNServer.CPersona))
        oPer.Cod = FSNUser.CodPersona
        oPer.CargarDestinos(FSNUser.Idioma)
        If oPer.Destinos Is Nothing OrElse oPer.Destinos.Count = 0 Then

        Else
            Dim ContDest As Integer = 0
            For Each destino As FSNServer.CDestino In oPer.Destinos
                ddlDestDefPed.Items.Add(destino.Den)
                ddlDestDefPed.Items(ContDest).Value = destino.Cod
                If Not FSNUser.Destino Is Nothing AndAlso Not FSNUser.Destino = "" Then
                    If destino.Cod = FSNUser.Destino Then
                        ddlDestDefPed.Items(ContDest).Selected = True
                    End If
                End If
                ContDest = ContDest + 1
            Next
        End If
        ddlMostImgArt.Items.Add(Textos(27))
        ddlMostImgArt.Items(0).Value = 1
        ddlMostImgArt.Items.Add(Textos(28))
        ddlMostImgArt.Items(1).Value = 0
        If FSNUser.MostrarImagenArt OrElse FSNUser.MostrarImagenArt = 1 Then
            ddlMostImgArt.Items(0).Selected = True
        Else
            ddlMostImgArt.Items(1).Selected = True
        End If
        ddlMostAtrib.Items.Add(Textos(27))
        ddlMostAtrib.Items(0).Value = 1
        ddlMostAtrib.Items.Add(Textos(28))
        ddlMostAtrib.Items(1).Value = 0
        If FSNUser.MostrarAtrib OrElse FSNUser.MostrarAtrib = 1 Then
            ddlMostAtrib.Items(0).Selected = True
        Else
            ddlMostAtrib.Items(1).Selected = True
        End If
        ddlMostCantMin.Items.Add(Textos(27))
        ddlMostCantMin.Items(0).Value = 1
        ddlMostCantMin.Items.Add(Textos(28))
        ddlMostCantMin.Items(1).Value = 0
        If FSNUser.MostrarCantMin OrElse FSNUser.MostrarCantMin = 1 Then
            ddlMostCantMin.Items(0).Selected = True
        Else
            ddlMostCantMin.Items(1).Selected = True
        End If
        ddlOcCodProve.Items.Add(Textos(27))
        ddlOcCodProve.Items(0).Value = 1
        ddlOcCodProve.Items.Add(Textos(28))
        ddlOcCodProve.Items(1).Value = 0
        If FSNUser.MostrarCodProve OrElse FSNUser.MostrarCodProve = 1 Then
            ddlOcCodProve.Items(1).Selected = True
        Else
            ddlOcCodProve.Items(0).Selected = True
        End If
        ddlOcCodArt.Items.Add(Textos(27))
        ddlOcCodArt.Items(0).Value = 1
        ddlOcCodArt.Items.Add(Textos(28))
        ddlOcCodArt.Items(1).Value = 0
        If FSNUser.MostrarCodArt OrElse FSNUser.MostrarCodArt = 1 Then
            ddlOcCodArt.Items(1).Selected = True
        Else
            ddlOcCodArt.Items(0).Selected = True
        End If

        'OPCION Cargar todas las cantidades pendientes de recepcionar
        ddlRecepcionesPendientes.Items.Add(Textos(27)) 'SI
        ddlRecepcionesPendientes.Items(0).Value = 1
        ddlRecepcionesPendientes.Items.Add(Textos(28)) 'NO
        ddlRecepcionesPendientes.Items(1).Value = 0
        If FSNUser.CargarCantPtesRecep Then
            ddlRecepcionesPendientes.Items(0).Selected = True
        Else
            ddlRecepcionesPendientes.Items(1).Selected = True
        End If

    End Sub

    ''' Revisado por: blp. Fecha: 08/03/2012
    ''' <summary>
    ''' Evento que guarda en BBDD los datos cambiados por el usuario en la página
    ''' </summary>
    ''' <param name="sender">El propio botón pulsado</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde:La propia página, cada vez que se clica el botón aceptar
    ''' Tiempo máximo: 0,2 seg</remarks>
    Protected Sub BtnAceptar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptar.Click
        Dim oPer As FSNServer.CPersona
        oPer = FSNServer.Get_Object(GetType(FSNServer.CPersona))
        oPer.Cod = FSNUser.CodPersona
        oPer.CargarDestinos(FSNUser.Idioma)
        If Not oPer.Destinos Is Nothing AndAlso Not oPer.Destinos.Count = 0 Then
            FSNUser.Destino = ddlDestDefPed.SelectedValue
        Else
            FSNUser.Destino = ""
        End If
        FSNUser.MostrarImagenArt = ddlMostImgArt.SelectedValue
        FSNUser.MostrarAtrib = ddlMostAtrib.SelectedValue
        FSNUser.MostrarCantMin = ddlMostCantMin.SelectedValue
        FSNUser.MostrarCodProve = IIf(ddlOcCodProve.SelectedValue = 1, 0, 1)
        FSNUser.MostrarCodArt = IIf(ddlOcCodArt.SelectedValue = 1, 0, 1)
        FSNUser.SeguimientoVerPedidosUsuario = chkSeguimientoPedidos.Items(0).Selected
        FSNUser.SeguimientoVerPedidosCCImputables = chkSeguimientoPedidos.Items(1).Selected
        FSNUser.CargarCantPtesRecep = IIf(ddlRecepcionesPendientes.SelectedValue = 1, True, False)
        If FSNUser.PermisoRecepcionarPedidosCCImputables Then
            FSNUser.RecepcionVerPedidosUsuario = chkRecepcionPedidos.Items(0).Selected
            FSNUser.RecepcionVerPedidosCCImputables = chkRecepcionPedidos.Items(1).Selected
        End If
        'Salvar las opciones de usuario
        FSNUser.GuardarOpcionesUsuario()
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "Cerrar", "<script>window.close()</script>") 'Inc 49 en Firefox no guardaba los cambios
    End Sub

End Class