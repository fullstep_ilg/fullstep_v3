<%@ Page Title="" Language="vb" AutoEventWireup="false" EnableViewState="true" MasterPageFile="~/App_Master/CabCatalogo.master"
    CodeBehind="Cesta.aspx.vb" Inherits="Fullstep.FSNWeb.Cesta" Async="True" %>

<%@ MasterType VirtualPath="~/App_Master/CabCatalogo.master" %>
<asp:Content ContentPlaceHolderID="head" ID="content1" runat="server">
    <style type="text/css">
        .DisBlock
        {
            display: block;
        }
        .Moneda
        {
            display: table-cell;
            vertical-align: middle;
        }
    </style>
    <script type="text/javascript">
        function validarKeyPress(e, l) {
            if (e.value.length >= l)
                return false
            else
                return true
        }

        function validarPaste(elem, longitud) {
            var texto = window.clipboardData.getData('Text');
            pegadoCorrecto = true;

            longTextoPegado = texto.length;
            longTexto = elem.value.length;

            if (longTexto + longTextoPegado > longitud)
                pegadoCorrecto = false;

            if (!pegadoCorrecto) {
                textoAPegar = texto.substring(0, (longitud - longTexto))
                elem.value = elem.value + textoAPegar;
                event.returnValue = false;
            }
        }

        function mostrarguardarcesta(senderId) {
            var info = $('#' + senderId).closest('tr').find('[id$=LblArticulo]');
            if (info.attr('EsPedidoAbierto')=='true') {
                $.when($.ajax({
                    type: "POST",
                    url: rutaFS + 'EP/Cesta.aspx/Obtener_Info_LineaPedidoAbierto',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ idCesta: info.attr('idCesta') }),
                    async: true
                })).done(function (msg) {
                    var infoLineaPedido = $.parseJSON(msg.d);
                    if (infoLineaPedido.tipoRecepcion == 1) actual = parseFloat($('#' + senderId).closest('tr').find('[id$=WNumEdTotLin]').val().replace(UsuNumberThousanFmt, '').replace(UsuNumberDecimalSeparator, '.'));
                    else actual = parseFloat($('#' + senderId).closest('tr').find('[id$=wnumCantidad]').val().replace(UsuNumberThousanFmt, '').replace(UsuNumberDecimalSeparator, '.'));

                    if (parseFloat(infoLineaPedido.resto) >= actual) document.getElementById($('[id$=BtnGuardarCesta]').attr('id')).click();
                    else {
                        if (infoLineaPedido.tipoRecepcion == 1) $('#' + senderId).closest('tr').find('[id$=WNumEdTotLin]').val(infoLineaPedido.anterior);
                        else $('#' + senderId).closest('tr').find('[id$=wnumCantidad]').val(infoLineaPedido.anterior);
                        alert((infoLineaPedido.tipoRecepcion == 1 ? TextosPantalla[0] : TextosPantalla[1]) + '.\n' + TextosPantalla[2] + ':\n' + parseFloat(infoLineaPedido.resto));
                    }
                });
            } else document.getElementById($('[id$=BtnGuardarCesta]').attr('id')).click();
        }  
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Ppl" runat="server">
    <!-- Panel Info Proveedor -->
    <fsn:FSNPanelInfo ID="FSNPanelDatosProveCab" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx"
        ServiceMethod="Proveedor_DatosContacto" TipoDetalle="1" />
    <!-- Panel Info Unidad -->
    <fsn:FSNPanelTooltip ID="FSNPanelDatosUnidad" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx"
        ServiceMethod="Unidad_Datos" EnableViewState="False" />
    <!-- Panel Direccion Envio Factura -->
    <fsn:FSNPanelInfo ID="FSNPanelDireccionEnvioFactura" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx"
        ServiceMethod="DireccionEnvioFactura_Datos" Width="380px" EnableViewState="False"
        cssTitulo="TituloPopUpPanelInfo" cssSubTitulo="SubTituloPopUpPanelInfo" />
    <!--Panel detalle de art�culo-->
    <fsep:DetArticuloCatalogo ID="pnlDetalleArticulo" runat="server" EnableViewState="true" />
    <!-- Panel Confirmaci�n -->
    <input id="btnOcultoConfirm" type="button" value="button" runat="server" style="display: none"
        enableviewstate="False" />
    <asp:Panel ID="pnlConfirm" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Center" Style="display: none;
        min-width: 300px; padding: 20px" Width="300px">
        <asp:UpdatePanel ID="upConfirm" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table cellpadding="0" cellspacing="20" border="0">
                    <tr>
                        <td colspan="2" align="left">
                            <span id="imgConfirm" runat="server" style="float: left; margin-right: 10px;"></span>
                            <asp:Label ID="lblConfirm" runat="server" Text="Label" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                            <asp:Label ID="lblMaxLength" runat="server" Text="" Style="display: block;" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                            <asp:TextBox ID="txtConfirm" runat="server" TextMode="MultiLine" Rows="4" MaxLength="4000"
                                Width="100%" Style="display: none;"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="width: 300px;">
                        <td align="right" style="width: 150px;">
                            <fsn:FSNButton ID="btnAceptarConfirm" runat="server" Text="FSNButton"></fsn:FSNButton>
                        </td>
                        <td align="left" style="width: 150px;">
                            <fsn:FSNButton ID="btnCancelarConfirm" runat="server" Text="FSNButton" Alineacion="Left"></fsn:FSNButton>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpeConfirm" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlConfirm" TargetControlID="btnOcultoConfirm">
    </ajx:ModalPopupExtender>
    <%--Panel confirmaci�n Emitir Pedido Ok o Mal--%>
    <asp:Panel ID="PanelMensajePedidoOkoMal" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1px" Style="display: none; min-width: 150px;
        max-width: 500px; padding: 20px">
        <asp:UpdatePanel ID="UpPanelEmisionPedidoOkoMal" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table cellpadding="10" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>
                            <asp:Image runat="server" ID="ImgEmisionOkOMal" ImageAlign="Left" ImageUrl="../../Images/trans.gif" />
                        </td>
                        <td>
                            <asp:Label runat="server" ID="LblMensajeEmision" CssClass="Rotulo" EnableViewState="False"></asp:Label>
                        </td>
                        <td style="vertical-align: top;">
                            <asp:LinkButton ID="lnkCerrarMensajePedidoOkoMal" runat="server" Style="text-align: left;">
                                <asp:Image ID="imLnkCerrarMensajePedidoOkoMal" runat="server" SkinID="Cerrar" Style="border: 0px" />
                            </asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%" align="center">
                                <tr>
                                    <td>
                                        <center>
                                            <fsn:FSNButton ID="BtnAceptarEmisionOkOMal" runat="server"></fsn:FSNButton>
                                        </center>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <button id="BtnOcEmiPedOkOMal" runat="server" style="display: none" enableviewstate="False">
    </button>
    <ajx:ModalPopupExtender ID="mpeMensajeEmiPedOkoMal" runat="server" TargetControlID="BtnOcEmiPedOkOMal"
        PopupControlID="PanelMensajePedidoOkoMal" BackgroundCssClass="ModalBackGround">
    </ajx:ModalPopupExtender>
    <!-- Panel MsgBox -->
    <input id="btnOcultoMsgBox" type="button" value="button" runat="server" style="display: none"
        enableviewstate="False" />
    <asp:Panel ID="pnlMsgBox" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Center" Style="display: none;
        min-width: 300px; padding: 20px" Width="300px">
        <asp:UpdatePanel ID="upMsgBox" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table cellpadding="0" cellspacing="20" border="0" width="100%">
                    <tr>
                        <td align="left" valign="top" width="20px">
                            <asp:Image ID="imgMsgBox" runat="server" ImageUrl="../../Images/icono_info.gif" />
                        </td>
                        <td align="left" width="95%">
                            <asp:Label ID="lblTitulo" runat="server" CssClass="Rotulo" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                            <asp:Label ID="lblMensaje" runat="server" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                            <asp:TextBox ID="txtMsgBox" runat="server" TextMode="MultiLine" Rows="4" MaxLength="4000"
                                Width="100%" Style="display: none;"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <table cellpadding="10" cellspacing="5" border="0" align="center">
                                <tr>
                                    <td>
                                        <fsn:FSNButton ID="btnAceptarMsgBox" runat="server" OnClientClick="return aceptarMsgBox()"
                                            EnableViewState="False" />
                                    </td>
                                    <td>
                                        <fsn:FSNButton ID="btnCancelarMsgBox" runat="server" OnClientClick="return cerrarMsgBox()"
                                            EnableViewState="False" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpeMsgBox" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlMsgBox" TargetControlID="btnOcultoMsgBox">
    </ajx:ModalPopupExtender>
    <!-- Panel Cambio Unidad -->
    <asp:Panel ID="pnlUnidad" runat="server" Width="400px" Style="padding: 10px; display: none;"
        CssClass="modalPopup">
        <table cellpadding="2" cellspacing="0" border="0" width="100%">
            <tr>
                <td valign="bottom">
                    <asp:Image ID="imgUnidades" runat="server" SkinID="CabUnidades" />
                </td>
                <td>
                    <asp:Label ID="lblCabUnidades" runat="server" CssClass="RotuloGrande" EnableViewState="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <asp:UpdatePanel ID="updModifUnidad" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:DropDownList ID="cmbUnidades" runat="server" AutoPostBack="True" DataTextField="Denominacion"
                                DataValueField="Codigo" Width="100%">
                            </asp:DropDownList>
                            <br />
                            <br />
                            <table cellpadding="4" cellspacing="2" border="0" width="100%">
                                <tr>
                                    <td class="FilaSeleccionada">
                                        <asp:Label ID="lblCodUnidad" runat="server" CssClass="Normal" EnableViewState="False"></asp:Label>
                                    </td>
                                    <td class="FilaPar2">
                                        <asp:Label ID="lblCodUnidadSrv" runat="server" CssClass="Normal" EnableViewState="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FilaSeleccionada">
                                        <asp:Label ID="lblUnidadCompra" runat="server" CssClass="Normal" EnableViewState="False"></asp:Label>
                                    </td>
                                    <td class="FilaPar2">
                                        <asp:Label ID="lblUnidadCompraSrv" runat="server" CssClass="Normal" EnableViewState="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FilaSeleccionada">
                                        <asp:Label ID="lblFactorConversion" runat="server" CssClass="Normal" EnableViewState="False"></asp:Label>
                                    </td>
                                    <td class="FilaPar2">
                                        <asp:Label ID="lblFactorConversionSrv" runat="server" CssClass="Normal" EnableViewState="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FilaSeleccionada">
                                        <asp:Label ID="lblCantMinima" runat="server" CssClass="Normal" EnableViewState="False"></asp:Label>
                                    </td>
                                    <td class="FilaPar2">
                                        <asp:Label ID="lblCantMinimaSrv" runat="server" CssClass="Normal" EnableViewState="False"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <br />
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td align="right">
                                <fsn:FSNButton ID="btnAceptarUnidad" runat="server"></fsn:FSNButton>
                            </td>
                            <td style="width: 25px;">
                                &nbsp;
                            </td>
                            <td align="left">
                                <fsn:FSNButton ID="btnCancelarUnidad" runat="server" EnableViewState="False"></fsn:FSNButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <input id="btnOcultoUnidad" type="button" value="button" runat="server" style="display: none"
        disabled="disabled" enableviewstate="False" />
    <ajx:ModalPopupExtender ID="mpepnlUnidad" runat="server" TargetControlID="btnOcultoUnidad"
        PopupControlID="pnlUnidad" BackgroundCssClass="modalBackground" CancelControlID="btnCancelarUnidad">
    </ajx:ModalPopupExtender>
    <%--Panel observaciones de art�culo--%>
    <asp:Panel ID="PanelObs" runat="server" Style="padding: 5px; display: none;" CssClass="modalPopup">
        <table cellpadding="2" cellspacing="0" border="0">
            <tr>
                <td align="left">
                    <asp:Label ID="LblObsPed" CssClass="Rotulo" runat="server" EnableViewState="False"></asp:Label>
                </td>
                <td align="right">
                    <fsn:FSNLinkInfo ID="lnkBtnCerrarObs" runat="server" Style="text-align: right;" EnableViewState="False">
                        <asp:Image id="imLnkBtnCerrarObs" runat="server" SkinID="Cerrar" style="border:0px"/>
                    </fsn:FSNLinkInfo>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:UpdatePanel ID="uppnlObservaciones" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:TextBox ID="txtObservaciones" runat="server" Columns="45" Rows="4" TextMode="MultiLine" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <table>
                        <tr>
                            <td>
                                <fsn:FSNButton ID="BtnAceptObs" runat="server" OnClientClick="cerrarpanelobs(true); return false"
                                    EnableViewState="False"></fsn:FSNButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <input id="btnOcultoObservaciones" type="button" runat="server" style="display: none"
        enableviewstate="False" />
    <ajx:ModalPopupExtender ID="mpeObsArt" runat="server" PopupControlID="PanelObs" TargetControlID="btnOcultoObservaciones"
        CancelControlID="lnkBtnCerrarObs" BackgroundCssClass="modalBackground">
    </ajx:ModalPopupExtender>
    <%-- Panel de la cabecera--%>
    <asp:Panel ID="PnlCabecera" runat="server" HorizontalAlign="Center" Width="100%"
        CssClass="ColorFondoTab">
        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="height: 50px;">
            <tr>
                <td align="right" width="100%">
                    <asp:UpdatePanel ID="upBotonesCabecera" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="70%" align="right">
                                        <asp:Label ID="LblSinArticulosCesta" runat="server" CssClass="EtiquetaGrande" Text="DNo hay archivos en la cesta de la compra"
                                            Visible="false">
                                        </asp:Label>
                                    </td>
                                    <td width="20%" align="center">
                                        <fsn:FSNButton ID="BtnGuardarCesta" runat="server" Alineacion="Right" Style="visibility: hidden;
                                            white-space: nowrap;" />
                                        <input type="hidden" id="hddGuardarCestaVisible" runat="server" value="false" />
                                    </td>
                                    <td align="justify" style="text-align: center;" width="10%">
                                        <fsn:FSNButton ID="BtnVaciarCesta" runat="server" Style="float: none; white-space: nowrap;
                                            text-align: center;" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:UpdatePanel runat="server" ID="upCollapse" UpdateMode="conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <asp:DataList ID="dlCesta" runat="server" ShowFooter="False" Width="100%" ExtractTemplateRows="True"
                CellPadding="0" CellSpacing="0" HorizontalAlign="Center" AlternatingItemStyle-CssClass="ColorFondoTab">
                <ItemTemplate>
                    <asp:Table ID="tblOrden" runat="server">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left" Width="40%">
                                <asp:UpdatePanel runat="server" ID="upOrden" UpdateMode="conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table width="100%">
                                                        <tr style="height: 25px;">
                                                            <td align="left" runat="server" id="tablaCollapseCab" style="cursor: hand" width="39%">
                                                                <asp:Label ID="LblProve" CssClass="Rotulo" runat="server" Font-Size="14px" EnableViewState="True"></asp:Label>:&nbsp;
                                                                    <fsn:FSNLinkInfo ID="FSNLinkProve" runat="server" CssClass="LinkButtonInfo" Font-Size="14px"
                                                                        ContextKey='<%# DataBinder.Eval(Container, "DataItem.Prove") %>' 
                                                                        CommandArgument='<%# DataBinder.Eval(Container, "DataItem.Moneda") %>'
                                                                        CommandName='<%# DataBinder.Eval(Container, "DataItem.Id") %>' />
                                                                    <asp:Label ID="lblCategoria" runat="server" CssClass="Etiqueta">
                                                                </asp:Label>
                                                                <asp:Label runat="server" ID="lblPedidoDirecto" CssClass="Etiqueta" Visible="false"></asp:Label>
                                                            </td>
                                                            <td style="width: 15%;" align="center">
                                                                <asp:Label ID="lblCantCab" runat="server" CssClass="Etiqueta">
                                                                </asp:Label>
                                                            </td>
                                                            <td style="width: 16%;" align="center">
                                                                <asp:Label ID="lblPUCab" runat="server" CssClass="Etiqueta">
                                                                </asp:Label>
                                                            </td>
                                                            <td style="width: 17%;" align="center">
                                                                <asp:Label ID="lblImpCab" runat="server" CssClass="Etiqueta">
                                                                </asp:Label>
                                                            </td>
                                                            <td style="width: 10%;" align="center">
                                                                <asp:Label ID="lblEmiCab" runat="server" CssClass="Etiqueta">
                                                                </asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" style="border: 0;">
                                                    <asp:Panel ID="PnelDatosLinea" runat="server" BorderWidth="0px" BorderStyle="None">
                                                        <asp:GridView ID="GridLineas" runat="server" AutoGenerateColumns="False" Width="100%"
                                                            OnRowCommand="GridLineas_RowCommand" OnRowDataBound="GridLineas_RowDataBound"
                                                            OnPreRender="GridLineas_PreRender" OnDataBound="GridLineas_DataBound" BorderStyle="None"
                                                            ShowHeader="false" ShowFooter="false">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-BorderWidth="0px">
                                                                    <ItemTemplate>
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr runat="server" id="FilaAvisoNoIncluir" visible="false" style="max-height: 60">
                                                                                <td colspan="12" style="max-height: 60" align="left">
                                                                                    <asp:Image ID="imageNoIncluir" runat="server" ImageUrl="../../Images/alert-large.gif"
                                                                                        ImageAlign="Left" />
                                                                                    <asp:Label ID="lblNoIncluirArticulo" runat="server" CssClass="Rotulo" Text="DEsta l�nea de pedido no se incluira, verifique que el tipo de pedido y el tipo de art�culo concuerdan">
                                                                                    </asp:Label>
                                                                                    <asp:CustomValidator ID="cusvalTipoArticulo" runat="server" ValidationGroup="EmisionPedido"
                                                                                        OnServerValidate="valTipoArticulo_ServerValidate"></asp:CustomValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr runat="server" id="filaAvisoNoIntegrado" visible="false">
                                                                                <td colspan="7">
                                                                                    <img src="../../Images/Icono_Error_Amarillo_40x40.gif" alt="alerta" style="vertical-align: middle;" />
                                                                                    <asp:Label ID="lblAlertaIntegrado" runat="server" Style="color: red; vertical-align: middle;
                                                                                        font-weight: bold;">
                                                                    Esta l�nea de pedido no se incluir�. No se encuentra correctamente integrado en el ERP (SAP Corporativo)  
                                                                                    </asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="12" style="border: 0px;">
                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                        <tr valign="middle" style="height: 40px;">
                                                                                            <td style="width: 5%;">
                                                                                                <asp:ImageButton ID="ImgBtnArt" runat="server" CommandName="AbrirDetalleArt" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ID") %>'
                                                                                                    ImageUrl='<%# "EPThumbnail.ashx?prove=" & DataBinder.Eval(Container, "DataItem.PROVE") & "&art=" & DataBinder.Eval(Container, "DataItem.Cod") & "&Tipo=Cesta" &  "&PonerImagen=True" %>'/>
                                                                                            </td>
                                                                                            <td align="left" style="width: 9%;">
                                                                                                <asp:LinkButton ID="LblNomArt" runat="server" CssClass="Rotulo" Text='<%# DataBinder.Eval(Container, "DataItem.Cod") %>'
                                                                                                    Visible="true" CommandName="AbrirDetalleArt" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ID") %>'>
                                                                                                </asp:LinkButton>
                                                                                            </td>
                                                                                            <td style="width: 25%;">
                                                                                                <asp:LinkButton ID="LblArticulo" runat="server" CssClass="Rotulo" Text='<%# DataBinder.Eval(Container, "DataItem.Den") %>'
                                                                                                    Visible="true" CommandName="AbrirDetalleArt" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ID") %>'>
                                                                                                </asp:LinkButton>
                                                                                            </td>
                                                                                            <td style="width: 15%; text-align: center;">
                                                                                                <div style="text-align: center; display: inline-block; vertical-align: middle; width: auto;">
                                                                                                    <asp:Label ID="lblCantidadNoAplica" runat="server" CssClass="Normal" Visible="false"
                                                                                                        EnableViewState="False"></asp:Label>
                                                                                                    <div style="text-align: center; display: inline-block;">
                                                                                                        <div class="Moneda">
                                                                                                            <fsn:FSNTextBox ID="wnumCantidad" name="wnumCantidad" runat="server" Style="vertical-align: middle;
                                                                                                                float: left; width: 80%; display: table-cell; text-align: right;"></fsn:FSNTextBox><fsn:FSNUpDownArrows
                                                                                                                    ID="wnumCantidadArrows" runat="server">
                                                                                                                </fsn:FSNUpDownArrows>
                                                                                                        </div>
                                                                                                        <asp:Label ID="lblUnidad" runat="server" CssClass="Moneda" Visible="false" EnableViewState="False"></asp:Label>
                                                                                                    </div>
                                                                                                    <asp:HiddenField runat="server" ID="wnumCantidad_DblValue" Value="0" />
                                                                                                    <asp:CompareValidator ID="valCantidadNoCero" runat="server" ControlToValidate="wnumCantidad"
                                                                                                        ValueToCompare="0" ErrorMessage="CompareValidator" Operator="NotEqual" ForeColor=""
                                                                                                        EnableClientScript="False" ValidationGroup="EmisionPedido" Display="None">
                                                                                                    </asp:CompareValidator>
                                                                                                </div>
                                                                                            </td>
                                                                                            <td style="width: 16%; text-align: center;">
                                                                                                <div style="text-align: center; display: inline-block;">
                                                                                                    <asp:Label ID="lblPrecioNoAplica" runat="server" Visible="false" CssClass="Normal"> 
                                                                                                    </asp:Label>
                                                                                                    <fsn:FSNTextBox ID="WNumEdPrecArt" runat="server" Visible="False" Width="45px" name="WNumEdPrecArt"
                                                                                                        Text='<%# CDbl(DataBinder.Eval(Container, "DataItem.PrecUc") * DataBinder.Eval(Container, "DataItem.FC")) %>'
                                                                                                        Style="vertical-align: middle; float: left; text-align: right;"></fsn:FSNTextBox>
                                                                                                    <fsn:FSNUpDownArrows ID="WNumEdPrecArtArrows" runat="server" Visible="false">
                                                                                                    </fsn:FSNUpDownArrows>
                                                                                                    <asp:Label ID="LblPrecArt" runat="server" CssClass="Normal" Text='<%# DataBinder.Eval(Container, "DataItem.FC") * DataBinder.Eval(Container, "DataItem.PrecUc")%>'>
                                                                                                    </asp:Label>
                                                                                                    &nbsp;
                                                                                                    <asp:Label runat="server" ID="LblMonedaArtSrv" CssClass="Normal">
                                                                                                    </asp:Label>
                                                                                                    <%-- &nbsp;/&nbsp;--%>
                                                                                                    <fsn:FSNLinkTooltip ID="lnkUnidad" runat="server" PanelTooltip="FSNPanelDatosUnidad"
                                                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.UP") %>' ContextKey='<%# DataBinder.Eval(Container, "DataItem.UP") %>'
                                                                                                        CssClass="LinkButtonInfo"></fsn:FSNLinkTooltip>
                                                                                                    <fsn:FSNLinkInfo ID="lnkBtnCambiarUnidad" runat="server" CommandName="ModificarUnidad"
                                                                                                        CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ID") %>'>
                                                                                                        <asp:Image id="imLnkBtnCambiarUnidad" runat="server" SkinID="ModificarPeq" style="border:0px"/>
                                                                                                    </fsn:FSNLinkInfo>
                                                                                                    <asp:HiddenField runat="server" ID="WNumEdPrecArt_DblValue" Value="0" />
                                                                                                </div>
                                                                                            </td>
                                                                                            <td style="width: 17%; vertical-align: middle; text-align: center; margin: 0px;">
                                                                                                <div style="text-align: center; vertical-align: middle; display: inline-block;">
                                                                                                    <asp:Label ID="LblTotLin" runat="server"></asp:Label>
                                                                                                    <div style="text-align: center; display: inline-block;">
                                                                                                        <div class="Moneda">
                                                                                                            <fsn:FSNTextBox ID="WNumEdTotLin" name="WNumEdTotLin" runat="server" Style="vertical-align: middle;
                                                                                                                float: left; width: 80%; display: table-cell; text-align: right;"></fsn:FSNTextBox><fsn:FSNUpDownArrows
                                                                                                                    ID="WNumEdTotLinArrows" runat="server">
                                                                                                                </fsn:FSNUpDownArrows>
                                                                                                        </div>
                                                                                                        <asp:Label runat="server" ID="LblMonedaArtSrvTotal" CssClass="Moneda"></asp:Label>
                                                                                                    </div>
                                                                                                    <asp:HiddenField ID="LblTotLin_DblValue" runat="server" Value="0" />
                                                                                                    <asp:HiddenField ID="LblTotLinAnt_DblValue" runat="server" Value="0" />
                                                                                                </div>
                                                                                                </div>
                                                                                            </td>
                                                                                            <td align="center" style="width: 10%; vertical-align: middle;">
                                                                                                <asp:ImageButton ID="BtnEliminarLinea" runat="server" CommandName="EliminarLinea"
                                                                                                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ID") %>'></asp:ImageButton>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin-bottom: 10px;">                                                        
                                                        <tr>
                                                            <td align="right" style="height: 40px;" width="70%">
                                                                <asp:Label ID="lblSubTotalPie" runat="server" CssClass="EtiquetaGrande">
                                                                </asp:Label>
                                                            </td>                                                            
                                                            <td width="20%" align="center">
                                                                <asp:Label ID="LblPrecPorProve" runat="server" CssClass="EtiquetaGrande">
                                                                </asp:Label>
                                                                <asp:HiddenField ID="LblPrecPorProve_DblValue" runat="server" Value="0" />
                                                            </td>
                                                            <td width="10%" align="justify" style="text-align: center;">
                                                                <fsn:FSNButton ID="BtnEmitirLinea" runat="server" CommandName="EmitirLinea" Style="float: none;
                                                                    white-space: nowrap; text-align: center;" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ID") & "@" & DataBinder.Eval(Container, "DataItem.IdOrdenPedidoAbierto") %>'>
                                                                </fsn:FSNButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </ItemTemplate>
            </asp:DataList>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--<hr style="font-size: small; font-weight: bold; line-height: normal" />--%>
        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="height: 50px;" id="tblPie" runat="server">
            <tr>
                <td align="right" width="100%">
                    <asp:UpdatePanel ID="upBotonesPie" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="70%">
                                    </td>
                                    <td width="20%" align="right">
                                        <fsn:FSNButton ID="btnGuardarCestaPie" runat="server" Alineacion="Right" Style="visibility: hidden;
                                            white-space: nowrap;" />
                                    </td>
                                    <td align="justify" style="text-align: center;" width="10%">
                                        <fsn:FSNButton ID="btnVaciarCestaPie" runat="server" Style="float: none; white-space: nowrap;
                                            text-align: center;" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
</asp:Content>
