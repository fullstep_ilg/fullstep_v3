<%@ Page Title="" Language="vb" AutoEventWireup="false" EnableViewState="true" MasterPageFile="~/App_Master/CabCatalogo.master"
	CodeBehind="EmisionPedido.aspx.vb" Inherits="Fullstep.FSNWeb.EmisionPedido"
	Async="True" %>

<%@ MasterType VirtualPath="~/App_Master/CabCatalogo.master" %>
<asp:Content ContentPlaceHolderID="head" ID="content1" runat="server">
	<link rel="stylesheet" href="<%=ResolveClientUrl("~/js/jquery/plugins/styles/jquery-ui.css")%>" type="text/css" />
	<style type="text/css">
		#divname
		{
			display: block;
			width: 600px;
			height: 400px;
			position: absolute;
			top: 50%;
			left: 50%;
			margin-top: -200px;
			margin-left: -300px;
		}
		.ButtonClass
		{
			cursor: pointer;
			text-decoration: underline;
			color: White;
		}
		.DescArtRed
		{
			cursor: pointer;
			text-decoration: underline;
			color: Red;
			font-size: 12px;
			font-weight: bold;
		}
		tr.border td
		{
			border: 1px solid grey;
		}
		.Prueba
		{
			background-color:White;
		}
		.fsComboValueDisplay
		{
			width:10px;   
		}        
		.CajaErrorAviso #textErrorAviso
		{
			font-weight:normal;   
		}
		.divErrorPantalla
		{
			display: none; 
			position: absolute; 
			left: 30px; 
			background-color: white;
			border: solid black; 
			border-width: 1px; 
			width: 40%;
		}
		#tblPlanesEntrega,#tblPlanesEntrega td,#tblPlanesEntrega th {
			border: 1px solid #aaaaaa;
		}
	</style>
	<link rel="stylesheet" href="<%=ResolveClientUrl("~/js/jquery/plugins/styles/token-input.css")%>" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Ppl" runat="server">
	<div id="divError" runat="server" class="divErrorPantalla" clientidmode="Static">
		<div class="CajaError" style="height:auto; min-height:100px; position:relative;">
			<div id="textErrorCab" style="margin-bottom:0.5em;"></div>
			<div id="textError" runat="server" clientidmode="Static" style="line-height:1.5em;"></div>
			<div id="textErrorAtributo" style="width: 95%; margin-bottom:2em;"></div>
			<div style="text-align:center; position:absolute; width:100%; bottom:1em;">
				<fsn:FSNButton ID="btnAceptarErrorE" runat="server" OnClientClick="return false;" Style="white-space: nowrap; float: none;" />
			</div>
		</div>        
	</div>
	<div id="divPedido" class="divErrorPantalla">
		<table cellpadding="0" cellspacing="0" border="0" class="CajaError" style="height: auto;min-height: 100px; width: 95%;">
			<tr>
				<td valign="top" align="left">
					<div id="TextoPedidoCab" style="width: 95%;"></div>
				</td>
			</tr>
			<tr>
				<td valign="top" align="left">
					<div id="TextoPedido" style="width: 95%;"></div>
					<div id="TextoPedidoAtributo" style="width: 95%;"></div>
				</td>
			</tr>
			<tr style="margin-top: 5px;">
				<td colspan="2">
					<center>
						<fsn:FSNButton ID="btnAceptarPedido" runat="server" OnClick="btnAceptarPedido_Click" Style="white-space: nowrap; float: none;" />
					</center>
				</td>
			</tr>
		</table>
	</div>    
	<div id="divErrorPartida" class="divErrorPantalla">
		<table cellpadding="0" cellspacing="0" border="0" class="CajaError" style="height: auto; min-height: 100px; width: 95%;">
			<tr>
				<td valign="top" align="left">
					<div id="textErrorPartidaCab" style="width: 95%;"></div>
				</td>
			</tr>
			<tr>
				<td valign="top" align="left">
					<div id="textErrorPartida" style="width: 95%;"></div>
				</td>
			</tr>
			<tr style="margin-top: 5px;">
				<td>
					<fsn:FSNButton ID="PbtnAceptarErrorPartida" runat="server" Alineacion="Left" Style="white-space: nowrap;"
						OnClientClick="MostrarCargando();setTimeout('emitirPedido(false, ModoEdicion)', 300);return false;" />
				</td>
				<td>
					<fsn:FSNButton ID="PbtnCancelarErrorPartida" runat="server" Alineacion="Right" Style="white-space: nowrap;" 
						OnClientClick="return false;" />
				</td>
			</tr>
		</table>
	</div>
	<div id="divErrorAviso" class="DivErrorAviso" style="display: none;">
		<table cellpadding="0" cellspacing="0" border="0" class="CajaErrorAviso">
			<tr>
				<td colspan="2" align="right" valign="top" align="right">
					<asp:ImageButton ID="ImgBtnCerrarErrorAviso" runat="server" ImageUrl="~/images/Bt_Cerrar.png" ImageAlign="Right" OnClientClick="return false;"/>
				</td>
			</tr>
			<tr>
				<td colspan="2" valign="top" align="left">
					<div id="textErrorAviso" style="margin-bottom:10px;"></div>
				</td>
			</tr>
			<tr>
				<td style="padding-right: 5px;">
					<fsn:FSNButton ID="PbtnAceptarErrorAviso" runat="server" OnClientClick="return false;" 
						Alineacion="Right" Style="white-space: nowrap;" />
				</td>
				<td style="padding-left: 5px;">
					<fsn:FSNButton ID="PbtnCancelarErrorAviso" runat="server" OnClientClick="return false;"
						Alineacion="Left" Style="white-space: nowrap;" />
				</td>
			</tr>
		</table>
	</div>
	<div id="pantallaCargando" style="text-align:center; margin-top:5em;">
		<img id="imgCargando" alt="" src="../../Images/cargando.gif" />
	</div>
	<div id="contenidoPantalla" style="display:none;">
		<div style="position:relative;">
			<div style="text-align:right; margin-right:1em; position:absolute; right:0em; top:-2em;">        
				<span class="botonRedondeado" id="btnModificarFavorito" style="display:none; white-space:nowrap;"></span>
				<input type="text" id="txtNomFavorito" class="Normal" style="display:none;" />
				<img id="imgGuardarFavorito" alt="" style="display:none; vertical-align:middle;"/>
				<span class="botonRedondeado" id="btnGuardarCesta" style="display:none; white-space:nowrap;"></span>
				<span class="botonRedondeado" id="btnEmitirPedido" style="display:none; white-space:nowrap;"></span>
				<asp:Label runat="server" ID="lblImpTotalCuotas_hdd" ClientIDMode="static" style="display:none;">0</asp:Label>
			</div>    
		</div>
		<div class="Texto12 RectanguloEP" style="padding: 0.2em 0.3em; overflow: auto; margin-bottom:1em;">
			<div id="infoProv" class="FilaSeleccionada" style="line-height:2em;">
				<%--ColorFondoTab">--%>
				<asp:Label ID="LblProve" runat="server" CssClass="Rotulo" style="margin:0em 0.5em;"></asp:Label>
				<asp:Label ID="FSNLinkProve" runat="server" ClientIDMode="static" CssClass="ButtonClass"></asp:Label>
			</div>
			<div id="divImportesPedido" style="display: none;">
				<table id="ImportesPedido" style="width: 100%;">
					<tr>
						<td align="left" runat="server" id="Td1" style="width: 42%;">
							<div style="float: left; vertical-align: middle;" class="FilaPar">
								<asp:Label ID="lblImporteBruto" runat="server" Text="DImporte Bruto:" CssClass="Normal MensajeError" EnableViewState="True"></asp:Label>
							</div>
							<div style="border-bottom-style: dotted; margin-left: 19%; vertical-align: bottom; height: 8px;"></div>
						</td>
						<td align="right" style="width: 10%;">
							<asp:Label runat="server" ID="lblImpBruto" ClientIDMode="static">0</asp:Label>
						</td>
						<td>
							<div style="visibility: hidden">
								<asp:Label runat="server" ID="lineasBorrar" ClientIDMode="static"></asp:Label>
								<asp:Label runat="server" ID="lblImpBruto_hdd" ClientIDMode="static">0</asp:Label>
								<asp:Label runat="server" ID="lblImpBrutoLinea_hdd" ClientIDMode="static">0</asp:Label>
							</div>
						</td>
					</tr>
					<tr>
						<td align="left" runat="server" id="Td2" style="width: 42%;">
							<div style="float: left; vertical-align: middle;" class="FilaPar">
								<asp:Label ID="lblCostesGenerales" runat="server" CssClass="Normal MensajeError" EnableViewState="True"></asp:Label>
							</div>
							<div style="border-bottom-style: dotted; margin-left: 19%; vertical-align: bottom; height: 8px;"></div>
						</td>
						<td align="right" style="width: 10%;">
							<asp:Label runat="server" ID="lblCosteCabeceraTotal" ClientIDMode="static" CssClass="FilaPar">0</asp:Label>
						</td>
						<td>
							<div style="visibility: hidden">
								<asp:Label runat="server" ID="lblCosteCabeceraTotal_hdd" ClientIDMode="static">0</asp:Label>
							</div>
						</td>
					</tr>
					<tr>
						<td align="left" runat="server" id="Td3" style="width: 42%;">
							<div style="float: left; vertical-align: middle;" class="FilaPar">
								<asp:Label ID="lblDescuentosGenerales" runat="server" CssClass="Normal MensajeError" EnableViewState="True"></asp:Label>
							</div>
							<div style="border-bottom-style: dotted; margin-left: 19%; vertical-align: bottom; height: 8px;"></div>
						</td>
						<td align="right" style="width: 10%;">
							<asp:Label runat="server" ID="lblDescuentosCabeceraTotal" ClientIDMode="static" CssClass="FilaPar">0</asp:Label>
						</td>
						<td>
							<div style="visibility: hidden;">
								<asp:Label runat="server" ID="lblDescuentosCabeceraTotal_hdd" ClientIDMode="static">0</asp:Label>
							</div>
						</td>
					</tr>
				</table>
				<div id="campos" style="width: 100%;" class="Normal MensajeError"></div>
			</div>
			<asp:Panel ID="pnlCabeceraImporte" runat="server" Width="100%">
				<table style="width: 100%;">
					<tr style="height: 30px;">
						<td valign="middle" style="width: 2%;">
							<img id="imgExpandir" alt="" src="../../images/expandir_abajo.png" style="cursor: pointer;" clientidmode="Static" />
							<img id="imgContraer" alt="" src="../../images/contraer_arriba.png" style="cursor: pointer; display:none;" clientidmode="Static" />
						</td>
						<td runat="server" id="Td4" style="width: 40%;">
							<div>
								<div style="float: left; vertical-align: middle;" class="FilaPar">
									<asp:Label ID="lblImporteTotal" runat="server" Text="DIMPORTE TOTAL:" CssClass="Rotulo" EnableViewState="True"></asp:Label>
								</div>
								<div style="border-bottom-style: dotted; margin-left: 15%; vertical-align: bottom; height: 8px;"></div>
							</div>
						</td>
						<td align="right" style="width: 10%;">
							<asp:Label runat="server" ID="lblImporteTotalCabecera" class="Etiqueta" ClientIDMode="static">0</asp:Label>
						</td>
						<td>
							<div style="visibility: hidden">
								<asp:Label runat="server" ID="lblImporteTotalCabecera_hdd" ClientIDMode="static">0</asp:Label>
							</div>
						</td>
					</tr>
				</table>
			</asp:Panel>
		</div>
		<!--CABECERA PEDIDO -->
		<div id="pesDatosGenerales" title="Datos Generales" style="clear: both; display: inline-block;" class="escenarioVistaEP escenarioSeleccionadoEP">
			<asp:Label ID="lblDatosGenerales" runat="server" ClientIDMode="static"></asp:Label>
		</div>
		<div id="pesArObs" title="Archivos/Observaciones" style="clear: both; display: inline-block;" class="escenarioVistaEP">
			<asp:Label ID="lblArchivos" runat="server" ClientIDMode="static"></asp:Label>
		</div>    
		<div id="pesCostes" title="Costes" style="clear: both; display: none;" class="escenarioVistaEP">
			<asp:Label ID="lblCostes" runat="server" ClientIDMode="static"></asp:Label>
		</div>
		<div id="pesDescuentos" title="Descuentos" style="clear: both; display: none;" class="escenarioVistaEP">
			<asp:Label ID="lblDescuentos" runat="server" ClientIDMode="static"></asp:Label>
		</div>
		<div id="pesOtrosDatos" title="Otros Datos" style="clear: both; display: inline-block;" class="escenarioVistaEP">
			<asp:Label ID="lblOtrosDatos" runat="server" ClientIDMode="static"></asp:Label>
		</div>
		<div id="DatosGenerales" class="Texto12 RectanguloEP" style="overflow: auto; margin-bottom:1em;">
			<br />
			<table cellpadding="1" cellspacing="1" border="0" width="100%" style="margin-bottom: 5px;">
				<tr>
					<td style="width: 10%;">
						<asp:Label ID="LblProveErp" CssClass="Etiqueta" runat="server" ClientIDMode="static"></asp:Label>
					</td>
					<td style="width: 30%;">
						<select id="CmbBoxProvesErp" runat="server" style="width: 80%;" clientidMode="Static"></select>
						<span></span>
						<div style="display:inline; position:absolute;">                            
							<img id="btnInfo" alt="Info" style="display:none; cursor: pointer"  src="../../images/info.gif"/>
							<div id="pnlInfoProveERP" class="popupCN" style="position:absolute; display:none; background-color: rgb(232, 232, 232); min-width:20em;">
								<div style="position:relative; padding:0.5em 2em 0.5em 0.5em;">
									<img id="AspaCerrar" alt="Cerrar" src="../../images/cerrar.png" style="position:absolute; right:0em; top:0em; cursor:pointer;" onclick="$('#pnlInfoProveERP').hide()"/>
									<div style="line-height:1.5em;"><b><span id="lblTitulo"></span></b></div>
									<div style="line-height:1.5em;"><b><span id="lblCodigo"></span></b><span id="IdCodigo"></span></div>
									<div style="line-height:1.5em;"><b><span id="lblDescripcion"></span></b><span id="IdDescripcion"></span></div>                                    
									<div id="divCampoProveOpcional1" style="display:none; line-height:1.5em;"><b><span id="lblCampo1"></span></b><span id="IdCampo1"></span></div>
									<div id="divCampoProveOpcional2" style="display:none; line-height:1.5em;"><b><span id="lblCampo2"></span></b><span id="IdCampo2"></span></div>
									<div id="divCampoProveOpcional3" style="display:none; line-height:1.5em;"><b><span id="lblCampo3"></span></b><span id="IdCampo3"></span></div>
									<div id="divCampoProveOpcional4" style="display:none; line-height:1.5em;"><b><span id="lblCampo4"></span></b><span id="IdCampo4"></span></div>
								</div>
							</div>
						</div>
					</td>
					<td align="left" style="width: 40%;" colspan="2">
                        <input type="checkbox" runat="server" id="chkSolicitAcepProve" ClientIDMode="static" style="padding-left:0px; margin-left:0px;"/>
                        <asp:Label ID="lblSolicitAcepProve" CssClass="Etiqueta" runat="server" Text="DSolicitar aceptaci�n del proveedor"></asp:Label>
					</td>					
				</tr>
				<tr>
					<td align="left" style="width: 10%;">
						<asp:Label ID="LblEmpresa" CssClass="Etiqueta" runat="server"></asp:Label>
					</td>
					<td style="width: 30%;">
						<asp:DropDownList ID="CmbBoxEmpresa" runat="server" Width="80%" DataTextField="Den"
							DataValueField="ID" ClientIDMode="static">
						</asp:DropDownList>
						<span></span>
					</td>
					<td align="left" style="width: 10%;">
						<asp:Label ID="LblTipoPedido" runat="server" Text="DTipo de pedido:" CssClass="Etiqueta"
							EnableViewState="True">
						</asp:Label>
					</td>
					<td style="width: 30%;">
						<asp:DropDownList runat="server" ID="CmbBoxTipoPedido" Width="50%" CssClass="Normal"
							DataValueField="Id" DataTextField="Denominacion" ClientIDMode="Static">
						</asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td align="left" style="width: 10%;">
						<asp:Label ID="lblOrgCompras" CssClass="Etiqueta" runat="server" ClientIDMode="static"></asp:Label>
					</td>
					<td style="width: 30%;">
						<select id="CmbBoxOrgCompras" style="width:80%"></select>
						<span></span>
					</td>
				</tr>
				<tr>
					<td align="left" style="width: 10%;">
						<asp:Label ID="LblReceptor" CssClass="Etiqueta" runat="server"></asp:Label>
					</td>
					<td align="left" style="width: 30%;">
						<select id="wddRecept" name="wddRecept" style="width: 80%;" clientidmode="Static"></select>
					</td>
					<td align="left" style="width: 10%;">
						<div id="divGestorTitulo" runat="server" clientidmode="Static">
							<asp:Label ID="LblGestorTitulo" CssClass="Etiqueta" runat="server" Text="DGestor:"></asp:Label>
						</div>
					</td>
					<td style="width: 30%;">
						<asp:Label ID="LblGestorCab" runat="server" Width="80%" ClientIDMode="Static"></asp:Label>
					</td>
				</tr>
				<tr>
					<td valign="middle" align="left" style="width: 10%;">
						<asp:Label ID="LblNumSap" CssClass="Etiqueta" runat="server" Text="DRef. en factura:">
						</asp:Label>
					</td>
					<td valign="middle" align="left" style="width: 30%;">
						<asp:TextBox ID="TxtBxPedSAP" CssClass="Normal" runat="server" Width="80%" ClientIDMode="Static"></asp:TextBox>
					</td>
					<td valign="middle" align="left" style="width: 10%;">
						<div id="divLblDireccionEnvioFactura" runat="server" clientidmode="Static">
							<asp:Label ID="LblDireccion" CssClass="Etiqueta" runat="server" Text="DDireccion:"></asp:Label>
						</div>
					</td>
					<td valign="middle" align="left" style="width: 50%; display: inline;">
						<div id="divDireccionEnvioFactura" runat="server" clientidmode="Static" style="float: left;">
							<asp:Label ID="lblDireccionEnvioFactura" CssClass="Normal" runat="server" ClientIDMode="static"></asp:Label>
						</div>
						<div id="divComboEnvioFactura" runat="server" clientidmode="Static" style="float: left;">
							<asp:DropDownList ID="cbDireccionEnvioFactura" runat="server" DataTextField="Direccion"
								DataValueField="ID" ClientIDMode="Static">
							</asp:DropDownList>
						</div>
						<div id="imagenDireccion" runat="server" clientidmode="Static" style="float: left;
							width: 5px; margin-left: 5px;">
							<asp:Image ID="im" runat="server" ImageUrl="../../Images/info.gif" Style="border: 0px" ClientIDMode="Static" />
						</div>
						<div style="display: none;">
							<asp:Label ID="lblCodDireccion" runat="server" ClientIDMode="static"></asp:Label>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div id="ArchivosObser" class="Texto12 RectanguloEP" style="display:none; padding:0.2em 0.3em; overflow:auto; margin-bottom:1em;">
			<fieldset id="fsTextObser" runat="server" class="RectanguloEP" style="margin-top: 10px;
				margin-bottom: 10px; width: 95%;">
				<legend align="left">
					<asp:Label ID="lblObsGen" CssClass="Etiqueta" runat="server" Text="DObservaciones Generales"
						ClientIDMode="Static"></asp:Label></legend>
				<textarea id="textObser" rows="3" style="width: 95%;" runat="server" clientidmode="Static"></textarea>
			</fieldset>
			<!-- Panel adjuntos de art�culo -->
			<fieldset id="fsTableAdjuntos" runat="server" class="RectanguloEP" style="margin-top: 10px;
				margin-bottom: 10px; width: 95%;">
				<legend align="left">
					<asp:Label ID="lblArcPed" CssClass="Etiqueta" runat="server" ClientIDMode="Static"></asp:Label></legend>
				<div id="tablaAdjuntos"></div>
				<br />
				<div id="DivPanelAdjuntos" class="modalPopup" style="display: none; position: absolute; z-index: 10003; min-height: 70px;">
					<!-- Panel de adjuntos del pedido -->
					<table id="PanelAdjunPedTitulo" cellpadding="4" cellspacing="0" border="0">
						<tr>
							<td valign="bottom">
								<asp:Image ID="Image1" runat="server" SkinID="CabAdjuntos" />
							</td>
							<td valign="bottom">
								<asp:Label ID="Label1" runat="server" CssClass="RotuloGrande" EnableViewState="False"
									Text="...."></asp:Label>
							</td>
						</tr>
					</table>
					<br />
					<table id="PanelAdjunPedCuerpo" cellpadding="4" cellspacing="0" border="0">
						<tr>
							<td>
								<input id="filesCab" name="filesCab" type="file" size="55px" width="380px" />
							</td>
						</tr>
						<tr>
							<td>
								<textarea id="comentarioAdjunto" rows="5" cols="40"></textarea>
							</td>
						</tr>
						<tr>
							<td align="center">
								<table id="PanelAdjunPedConfirmacionCancelacion" cellpadding="10" cellspacing="0"
									border="0">
									<tr>
										<td>
											<div>
												<span onclick="subirArchivoCesta()"><a class="Boton" id="txtAceptarAdj" style="color: White;
													background-color: rgb(171, 0, 32); font-size: 12px; float: right; text-decoration: none;
													border-style: none; border-color: inherit; border-width: 0px; padding: 3px 10px;
													border-radius: 4px;" onfocus="this.blur();" onmouseup="this.style.padding='3px 10px 3px 10px';this.style.cursor='hand';"
													onmousedown="this.style.padding='4px 10px 2px 10px';this.style.cursor='hand';"
													onmouseout="this.style.backgroundColor='#AB0020';this.style.padding='3px 10px 3px 10px';this.style.cursor='pointer';"
													onmouseover="this.style.backgroundColor='#780000';this.style.cursor='hand';"></a>
												</span>
											</div>
										</td>
										<td>
											<div>
												<span onclick="ocultarAdjuntar()"><a id="txtCancelarAdj" class="Boton" style="color: White;
													background-color: rgb(171, 0, 32); font-size: 12px; float: right; text-decoration: none;
													border-style: none; border-color: inherit; border-width: 0px; padding: 3px 10px;
													border-radius: 4px;" onfocus="this.blur();" onmouseup="this.style.padding='3px 10px 3px 10px';this.style.cursor='hand';"
													onmousedown="this.style.padding='4px 10px 2px 10px';this.style.cursor='hand';"
													onmouseout="this.style.backgroundColor='#AB0020';this.style.padding='3px 10px 3px 10px';this.style.cursor='pointer';"
													onmouseover="this.style.backgroundColor='#780000';this.style.cursor='hand';"></a>
												</span>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<asp:HiddenField ID="HiddenField1" runat="server" ClientIDMode="Static" Value="test" />
				<asp:TextBox ID="listaAdjuntosJSON" runat="server" ClientIDMode="Static" Style="display: none;"
					Text="[]"></asp:TextBox>
				<div id="listaAdjuntos" style="clear: both; margin-top: 5px; width: 50%;">
				</div>
				<div id="NuevoAdjunto" class="OtrasOpciones" style="border: solid 1px Transparent;"
					onclick="mostrarAdjuntar()">
					<img id="imgNuevoAdjunto" class="Image20" alt="Nuevo Adjunto" src="<%=ConfigurationManager.AppSettings("ruta")%>images/adjunto.png"
						style="margin-left: 5px; vertical-align: bottom; cursor: pointer;" />
					<span id="spanNuevoAdjunto" class="Texto12" style="line-height: 25px;">
						<asp:Label ID="lblNuevoAdjunto" runat="server" Text="DAdjuntar archivos"></asp:Label>
					</span>
				</div>
			</fieldset>
		</div>    
		<div id="Costes" class="Texto12 RectanguloEP" style="display:none; padding:0.2em 0.3em; overflow:auto; margin-bottom:1em;">
			<div id="divAtributosCoste" runat="server" style="padding-top:0.5em;">
				<table id="tblAtributosCoste" style="border:1px solid #E8E8E8; width:100%; background-color:#F0F0F0; border-collapse:collapse;">
					<tr class="border" style="background-color:#cccccc; height:20px; border:1px;">
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblIdCoste" runat="server" Text="DId"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px">
							<input type="checkbox" style="visibility: hidden;" />
						</td>
						<td style="width: 25%; height: 20px">
							<asp:Label ID="lblCodCoste" CssClass="Etiqueta" runat="server" Text="DCodigo"></asp:Label>
						</td>
						<td style="width: 38%; height: 20px">
							<asp:Label ID="lblDenCoste" CssClass="Etiqueta" runat="server" Text="DDenominacion"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblTipoCoste" CssClass="Etiqueta" runat="server" Text="DTipo"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblIntroCoste" CssClass="Etiqueta" runat="server" Text="DIntro"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblOblCoste" CssClass="Etiqueta" runat="server" Text="DObligatorio"></asp:Label>
						</td>
						<td style="width: 15%; height: 22px">
							<asp:Label ID="lblValCoste" CssClass="Etiqueta" runat="server" Text="DValor"></asp:Label>
						</td>
						<td style="width: 18%; height: 22px">
							<asp:Label ID="lblImpCoste" CssClass="Etiqueta" runat="server" Text="DImporte"></asp:Label>
						</td>
					</tr>
				</table>
				<table id="tblCosteTotales" style="border:1px solid #E8E8E8; width:100%; border-collapse:collapse; margin-top:2px;">
					<tr class="border" style="background-color: #cccccc; height: 20px">
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblCosteTotal_hdd" runat="server" ClientIDMode="Static" Text="0"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px">
							<input type="checkbox" style="visibility: hidden;" />
						</td>
						<td style="width: 63%; height: 20px">
							<asp:Label ID="lblTotCostes" runat="server" Text="DTotal Costes Generales"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;"></td>
						<td style="width: 1%; height: 20px; display: none;"></td>
						<td style="width: 1%; height: 20px; display: none;"></td>
						<td style="width: 15%; height: 22px"></td>
						<td style="width: 18%; height: 22px">
							<asp:Label ID="lblCosteTotal" runat="server" ClientIDMode="Static" Style="width: 97%; border: none; text-align: right; display: block;"></asp:Label>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div id="Descuentos" class="Texto12 RectanguloEP" style="display: none; padding: 0.2em 0.3em; overflow: auto; margin-bottom:1em;">
			<div id="divAtributosDescuento" runat="server" style="padding-top:0.5em;">
				<table id="tblAtributosDescuento" style="border: 1px solid #E8E8E8; width: 100%;
					border-collapse: collapse; background-color: #F0F0F0;">
					<tr class="border" style="background-color: #cccccc; height: 20px">
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblIdDescuento" runat="server" Text="DId"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px">
							<input type="checkbox" style="visibility: hidden;" />
						</td>
						<td style="width: 25%; height: 20px">
							<asp:Label ID="lblCodDescuento" CssClass="Etiqueta" runat="server" Text="DCodigo"></asp:Label>
						</td>
						<td style="width: 38%; height: 20px">
							<asp:Label ID="lblDenDescuento" CssClass="Etiqueta" runat="server" Text="DDenominacion"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblTipoDescuento" CssClass="Etiqueta" runat="server" Text="DTipo"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblIntroDescuento" CssClass="Etiqueta" runat="server" Text="DIntro"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblOblDescuento" CssClass="Etiqueta" runat="server" Text="DObligatorio"></asp:Label>
						</td>
						<td style="width: 15%; height: 22px">
							<asp:Label ID="lblValDescuento" CssClass="Etiqueta" runat="server" Text="DValor"></asp:Label>
						</td>
						<td style="width: 18%; height: 22px">
							<asp:Label ID="lblImpDescuento" CssClass="Etiqueta" runat="server" Text="DImporte"></asp:Label>
						</td>
					</tr>
				</table>
				<table id="tblDescuentoTotales" style="border:1px solid #E8E8E8; width:100%; border-collapse:collapse; margin-top:2px;">
					<tr class="border" style="background-color: #cccccc; height: 20px">
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblDescuentoTotal_hdd" runat="server" ClientIDMode="Static" Text="0"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px;">
							<input type="checkbox" style="visibility: hidden;" />
						</td>
						<td style="width: 63%; height: 20px">
							<asp:Label ID="lblTotDescuento" runat="server" Text="DTotal Descuentos Generales"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;"></td>
						<td style="width: 1%; height: 20px; display: none;"></td>
						<td style="width: 1%; height: 20px; display: none;"></td>
						<td style="width: 15%; height: 22px;"></td>
						<td style="width: 18%; height: 22px;">
							<asp:Label ID="lblDescuentoTotal" runat="server" ClientIDMode="Static" Style="width: 97%; border: none; text-align: right; display: block;"></asp:Label>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div id="OtrosDatos" class="Texto12" style="display:none; margin-bottom:1em;">
			<div id="divAtributosItem" runat="server" class="RectanguloEP" style="position: relative; float:left; padding:6px;">
				<table id="tblAtributosItem" style="border: 1px solid #E8E8E8; width: 100%; table-layout: fixed; border-collapse:collapse; background-color:#F0F0F0;">
					<tr class="border" style="background-color: #cccccc; height: 22px">
						<td style="width: 1%; height: 22px; display: none;">
							<asp:Label ID="lblIdAtributoTablaItem" runat="server" Text="DId"></asp:Label>
						</td>
						<td style="width: 23%; height: 22px">
							<asp:Label ID="lblCodigoAtributoTablaItem" CssClass="Etiqueta" runat="server" Text="DCodigo"></asp:Label>
						</td>
						<td style="width: 40%; height: 22px">
							<asp:Label ID="lblDenomAtributoTablaItem" CssClass="Etiqueta" runat="server" Text="DDenominacion"></asp:Label>
						</td>
						<td style="width: 1%; height: 22px; display: none;">
							<asp:Label ID="lblTipoAtributoTablaItem" CssClass="Etiqueta" runat="server" Text="DTipo"></asp:Label>
						</td>
						<td style="width: 1%; height: 22px; display: none;">
							<asp:Label ID="lblIntroAtributoTablaItem" CssClass="Etiqueta" runat="server" Text="DIntro"></asp:Label>
						</td>
						<td style="width: 1%; height: 22px; display: none;">
							<asp:Label ID="lblObligAtributoTablaItem" CssClass="Etiqueta" runat="server" Text="DObligatorio"></asp:Label>
						</td>
						<td style="width: 37%; height: 22px">
							<asp:Label ID="lblValorAtributoTablaItem" CssClass="Etiqueta" runat="server" Text="DValor"></asp:Label>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<!--RESUMEN LINEAS PEDIDO -->
		<div id="pesResumenLineas" style="display: inline-block;" class="escenarioVistaEP escenarioSeleccionadoEP">
			<asp:Label ID="lblResumenLinea" runat="server" ClientIDMode="static"></asp:Label>
		</div>
		<div id="pesDatosLineas" style="display: inline-block; margin-top: 0.5em;" class="escenarioVistaEP">
			<asp:Label ID="lblDatosLinea" runat="server" ClientIDMode="static"></asp:Label>
			<asp:Label ID="lblDatosLineaLinea" runat="server" ClientIDMode="static"></asp:Label>
		</div>
		<div id="pesCostesLinea" style="display: none; margin-top: 0.5em;" class="escenarioVistaEP">
			<asp:Label ID="lblCostesLinea" runat="server" ClientIDMode="static"></asp:Label>
			<asp:Label ID="lblCostesLineaLinea" runat="server" ClientIDMode="static"></asp:Label>
			</div>
		<div id="pesDescuentosLinea" style="display: none; margin-top: 0.5em;" class="escenarioVistaEP">
			<asp:Label ID="lblDescuentosLinea" runat="server" ClientIDMode="static"></asp:Label>
			<asp:Label ID="lblDescuentosLineaLinea" runat="server" ClientIDMode="static"></asp:Label>
		</div>
		<div id="pesPlanesEntrega" style="display: none; margin-top: 0.5em;" class="escenarioVistaEP">
			<asp:Label ID="lblPlanesEntrega" runat="server" ClientIDMode="static"></asp:Label>
            <asp:Label ID="lblPlanesEntregaLinea" runat="server" ClientIDMode="static"></asp:Label>
		</div>
		<div id="ResumenLineas" class="Texto12 RectanguloEP" style="padding: 0.2em 0.3em; overflow: auto;">
			<div style="display: none;">
				<asp:Label runat="server" ID="lblTotalCostesLinea_hdd" ClientIDMode="static">0</asp:Label>
				<asp:Label runat="server" ID="lblTotalDescuentosLinea_hdd" ClientIDMode="static">0</asp:Label>
			</div>
			<asp:Button runat="server" ID="btnArticulosPostBack" style="display:none;" />
			<asp:Button runat="server" ID="btnArticuloDeletePostBack" style="display:none;" />
			<asp:UpdatePanel ID="pnlGrid" runat="server" UpdateMode="Conditional">
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="btnArticulosPostBack" EventName="Click" />
					<asp:AsyncPostBackTrigger ControlID="btnArticuloDeletePostBack" EventName="Click" />
				</Triggers>
				<ContentTemplate>
					<ig:WebHierarchicalDataGrid ID="whdgArticulos" runat="server" Visible="true" AutoGenerateColumns="false"
							Width="100%" ShowHeader="true" EnableAjax="true" EnableAjaxViewState="false"
							EnableDataViewState="false" CssClass="headerNoWrap" >
						<Columns>
							<ig:TemplateDataField Key="Key_AVISO" Width="30px"></ig:TemplateDataField>
							<ig:BoundDataField Key="Key_NUM_LINEA" DataFieldName="NUM_LINEA" Width="40px"></ig:BoundDataField>
							<ig:BoundDataField Key="Key_IDLINEA" DataFieldName="IDLINEA"></ig:BoundDataField>
							<ig:BoundDataField Key="Key_ART_DEN" DataFieldName="ART_DEN" Width="50px"></ig:BoundDataField>
							<ig:BoundDataField Key="Key_PREC" DataFieldName="PREC" Width="70px"></ig:BoundDataField>
							<ig:BoundDataField Key="Key_CANT" DataFieldName="CANT" Width="30px"></ig:BoundDataField>
							<ig:BoundDataField Key="Key_UNI" DataFieldName="CANT" Width="80px"></ig:BoundDataField>
							<ig:TemplateDataField Key="Key_COSTES" Width="90px"></ig:TemplateDataField>
							<ig:TemplateDataField Key="Key_DESCUENTOS" Width="120px"></ig:TemplateDataField>
							<ig:TemplateDataField Key="Key_IMPBRUTO" Width="50px"></ig:TemplateDataField>
							<ig:TemplateDataField Key="Key_DELETE" Width="40px"></ig:TemplateDataField>
							<ig:TemplateDataField Key="Key_SUBIRORDEN" Width="30px"></ig:TemplateDataField>
							<ig:TemplateDataField Key="Key_BAJARORDEN" Width="30px"></ig:TemplateDataField>
						</Columns>
						<Behaviors>
							<ig:EditingCore>
								<Behaviors>
								<ig:CellEditing Enabled="true" EditModeActions-EnableF2="true">
								<ColumnSettings>
								<ig:EditingColumnSetting ColumnKey="Key_AVISO" ReadOnly="true" />
								<ig:EditingColumnSetting ColumnKey="Key_IDLINEA" ReadOnly="true" />
								<ig:EditingColumnSetting ColumnKey="Key_ART_DEN" ReadOnly="true" />
								<ig:EditingColumnSetting ColumnKey="Key_PREC" ReadOnly="true" />
								<ig:EditingColumnSetting ColumnKey="Key_CANT" ReadOnly="true" />
								<ig:EditingColumnSetting ColumnKey="Key_UNI" ReadOnly="true" />
								<ig:EditingColumnSetting ColumnKey="Key_COSTES" ReadOnly="true" />
								<ig:EditingColumnSetting ColumnKey="Key_DESCUENTOS" ReadOnly="true" />
								<ig:EditingColumnSetting ColumnKey="Key_IMPBRUTO" ReadOnly="true" />
								<ig:EditingColumnSetting ColumnKey="Key_DELETE" ReadOnly="true" />
								<ig:EditingColumnSetting ColumnKey="Key_SUBIRORDEN" ReadOnly="true" />
								<ig:EditingColumnSetting ColumnKey="Key_BAJARORDEN" ReadOnly="true" />
								</ColumnSettings>
								<CellEditingClientEvents ExitingEditMode="whdgArticulos_ExitingEditMode" ExitedEditMode="whdgArticulos_ExitedEditMode" />
								</ig:CellEditing>
								</Behaviors>
							</ig:EditingCore>
							<ig:Activation Enabled="true" />
							<ig:RowSelectors Enabled="true" RowSelectorCssClass="RowSelectorCssClass"></ig:RowSelectors>
							<ig:Selection Enabled="true" CellSelectType="Single" RowSelectType="Multiple" ColumnSelectType="None">
								<SelectionClientEvents RowSelectionChanged="whdgArticulos_RowSelectionChanged" />
							</ig:Selection>
							<ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
							<ig:Paging Enabled="false"></ig:Paging>
						</Behaviors>
						<ClientEvents Click="whdgArticulos_CellClick" />
					</ig:WebHierarchicalDataGrid>   
				</ContentTemplate>
			</asp:UpdatePanel>
		</div>
		<div id="DatosLinea" class="Texto12 RectanguloEP" style="padding: 0.2em 0.3em; overflow: auto; margin-bottom: 10px; display: none;">        
			<div class="FilaSeleccionada" style="height:2em; line-height:2em; padding-left:0.5em;">
				<asp:Label ID="lblNumLinea" runat="server" CssClass="Etiqueta"></asp:Label>:
				<asp:Label ID="lblLinea" runat="server" CssClass="Normal" ClientIDMode="static" style="margin-left:1em;"></asp:Label>
			</div>
			<hr />
			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td style="width:70%">
						<div style="line-height:2em; display:inline-block;">
							<asp:Label ID="LblDest" runat="server" CssClass="Etiqueta" style="display:inline-block; width:10em;"></asp:Label>
							<asp:Label ID="lblDestino" runat="server" CssClass="Normal" ClientIDMode="static" style="display:inline-block; width:22.5em; margin-right:0.5em;"></asp:Label>
							<asp:Image ID="imEmiPedLnkBtnCambiarDest" runat="server" ImageUrl="../../Images/abrir_ptos_susp.gif" Style="cursor:pointer; vertical-align:middle;" ClientIDMode="static" />
							<asp:Image ID="BtnCopiarDestinos" runat="server" ImageUrl="../../Images/aplicar_plus.gif" Style="cursor:pointer; vertical-align:middle;" ClientIDMode="static" />
						</div>
						<div style="line-height:2em; display:inline-block;">
							<asp:Label ID="LblAlmacen" runat="server" CssClass="Etiqueta" style="display:inline-block; width:10em;"></asp:Label>
							<div style="display:inline-block; width:24.9em; vertical-align:middle;"><div id="cboAlmacen" style="height:22px; width:24.8em;"></div></div>
							<asp:Image ID="BtnCopiarAlmacen" runat="server" ImageUrl="../../Images/aplicar_plus.gif" Style="cursor:pointer; vertical-align:middle;" ClientIDMode="static" />            
						</div>
						<div style="line-height:2em; display:inline-block;">
							<asp:Label ID="lblCentroCoste" runat="server" CssClass="Etiqueta" Text="DCentro de Coste:" style="display:inline-block; width:10em;"></asp:Label>
							<div style="display:inline-block; width:22.5em; margin-right:0.5em;">
								<asp:TextBox ID="tbCtroCoste" runat="server" CssClass="Normal" Style="width:100%;" ClientIDMode="Static"></asp:TextBox>
							</div>
							<asp:HiddenField ID="tbCtroCoste_Hidden" runat="server" ClientIDMode="Static" />
							<img src="../../Images/abrir_ptos_susp.gif" id="imgPanelCentros" runat="server" style="cursor:pointer; vertical-align:middle;" />
							<asp:Image ID="BtnCopiarCtroCoste" runat="server" ImageUrl="../../Images/aplicar_plus.gif" Style="cursor:pointer; vertical-align:middle;" ClientIDMode="static" />
						</div>
						<div style="line-height:2em; display:inline-block;">
							<div style="display:inline-block; width:35.5em; vertical-align:middle;"><asp:PlaceHolder ID="phrPartidasPresBuscador" runat="server" ClientIDMode="Static"></asp:PlaceHolder></div>
							<asp:Image ID="BtnCopiarPartidas" runat="server" ImageUrl="../../Images/aplicar_plus.gif" Style="cursor:pointer; vertical-align:middle; margin-left:-0.3em;" ClientIDMode="static" />
						</div>
						<div style="line-height:2em;">
							<div id="pres1OrgCompra" style="display:inline-block; box-sizing:border-box;">
								<asp:Label ID="lblCentroCoste1" runat="server" CssClass="Etiqueta" Text="DCentro de coste:" style="display:inline-block; width:10em;"></asp:Label>
								<div style="display:inline-block; width:22.5em;">
									<input id="txtCentroCoste1" style="width:100%;" />
								</div>                        
								<img src="../../Images/abrir_ptos_susp.gif" id="imgCentroCoste1" runat="server" style="cursor:pointer; vertical-align:middle; margin-left:0.5em;" />
								<asp:Image ID="btnCopiarCentroCoste1" runat="server" ImageUrl="../../Images/aplicar_plus.gif" Style="cursor:pointer; vertical-align:middle;" ClientIDMode="static" />
							</div>
							<div id="pres2OrgCompra" style="display:inline-block; box-sizing:border-box;">
								<asp:Label ID="lblCentroCoste2" runat="server" CssClass="Etiqueta" Text="DCentro de coste:" style="display:inline-block; width:10em;"></asp:Label>
								<div style="display:inline-block; width:22.5em;">                        
									<input type="text" id="txtCentroCoste2" style="width:100%;" />
								</div>
								<img src="../../Images/abrir_ptos_susp.gif" id="imgCentroCoste2" runat="server" style="cursor:pointer; vertical-align:middle; margin-left:0.5em;" />
								<asp:Image ID="btnCopiarCentroCoste2" runat="server" ImageUrl="../../Images/aplicar_plus.gif" Style="cursor:pointer; vertical-align:middle;" ClientIDMode="static" />
							</div>
						</div>
						<div style="line-height:2em;">
							<div id="pres3OrgCompra" style="display:inline-block; box-sizing:border-box;">
								<asp:Label ID="lblCentroCoste3" runat="server" CssClass="Etiqueta" Text="DCentro de coste:" style="display:inline-block; width:10em;"></asp:Label>
								<div style="display:inline-block; width:22.5em;">
									<input id="txtCentroCoste3" style="width:100%;" />
								</div>
								<img src="../../Images/abrir_ptos_susp.gif" id="imgCentroCoste3" runat="server" style="cursor:pointer; vertical-align:middle; margin-left:0.5em;" />
								<asp:Image ID="btnCopiarCentroCoste3" runat="server" ImageUrl="../../Images/aplicar_plus.gif" Style="cursor:pointer; vertical-align:middle;" ClientIDMode="static" />
							</div>
							<div id="pres4OrgCompra" style="display:inline-block; box-sizing:border-box;">
								<asp:Label ID="lblCentroCoste4" runat="server" CssClass="Etiqueta" Text="DCentro de coste:" style="display:inline-block; width:10em;"></asp:Label>
								<div style="display:inline-block; width:22.5em;">   
									<input type="text" id="txtCentroCoste4" style="width:100%;" />
								</div>
								<img src="../../Images/abrir_ptos_susp.gif" id="imgCentroCoste4" runat="server" style="cursor:pointer; vertical-align:middle; margin-left:0.5em;" />
								<asp:Image ID="btnCopiarCentroCoste4" runat="server" ImageUrl="../../Images/aplicar_plus.gif" Style="cursor:pointer; vertical-align:middle;" ClientIDMode="static" />
							</div>
						</div>
						<div style="line-height:2em;">
							<div id="centroAprovisionamiento" style="display:none; box-sizing:border-box;">
								<asp:Label ID="lblCentroOrgCompra" runat="server" CssClass="Etiqueta" Text="DCentro:" style="display:inline-block; width:10em;"></asp:Label>
								<div style="display:inline-block; width:24.5em;">
									<select id="cboCentroOrgCompra" style="width:20.5em;"></select>
								</div>
								<asp:Image ID="btnCopiarCentroOrgCompra" runat="server" ImageUrl="../../Images/aplicar_plus.gif" Style="cursor:pointer; vertical-align:middle; margin-left:0.5em;" ClientIDMode="static" />
							</div>
							<div style="display:inline-block; box-sizing:border-box;">
								<asp:Label ID="lblActivoDesc" runat="server" CssClass="Etiqueta" ClientIDMode="Static" style="display:inline-block; width:10em;"></asp:Label>
								<div style="display:inline-block; width:22.5em;">
									<asp:TextBox ID="tbActivosLinea" runat="server" CssClass="Normal" Style="width:100%;" ClientIDMode="Static"></asp:TextBox>
								</div>
								<asp:HiddenField ID="tbActivosLinea_Hidden" runat="server" ClientIDMode="Static" />
								<asp:HiddenField ID="tbActivosCentro_Hidden" runat="server" ClientIDMode="Static" />
								<asp:Image ID="imgActivo" runat="server" ImageUrl="../../Images/abrir_ptos_susp.gif" Style="cursor:pointer; vertical-align:middle; margin-left:0.5em;" ClientIDMode="static" />
								<asp:Image ID="imgAplicarActivo" runat="server" ImageUrl="../../Images/aplicar_plus.gif" Style="cursor:pointer; vertical-align:middle;" ClientIDMode="static" />
							</div>
                            <div style="display:inline-block; box-sizing:border-box;">
                                <asp:Label ID="lblDesvioRecepcion" runat="server" CssClass="Etiqueta" Style="width:100%;" ClientIDMode="Static"></asp:Label>
                                <div style="display:inline-block; width:22.5em;">
                                    <asp:TextBox ID="txtDesvioRecepcion" CssClass="Normal" runat="server" Width="100%" ClientIDMode="Static" value='${formatNumber($data.Valor)}' onBlur="validaFloat(this);"></asp:TextBox>
                                </div>
                                <asp:Image ID="btnCopiarPorcentajeDesvio" runat="server" ImageUrl="../../Images/aplicar_plus.gif" Style="cursor:pointer; vertical-align:middle; margin-left:0.5em;" ClientIDMode="static" />
                            </div>
						</div>
					</td>
					<td style="width:25%; vertical-align:top;">                            
						<input type="radio" name="fecPlanEntrega" id="rbFechaEntrega" checked />   
						<asp:Label ID="LblFecEntSol" CssClass="Etiqueta" runat="server" style="display:inline-block; line-height:2em;" ClientIDMode="Static"></asp:Label> 
						<div style="display:inline-block; line-height:2em;"> 
							<div class="Bordear" style="display:inline-block; vertical-align:middle;">                            
								<input id="txtFecEntSol" class="day-picker" type="text" style="cursor:pointer; font-size:12px; border:0px; width:7em;" /> 
							</div>
							<div style="display:inline-block; vertical-align:middle;">
								<input id="CBObli" type="checkbox" />
								<asp:Label ID="LblObli" runat="server" CssClass="Etiqueta" style="display:inline-block;"></asp:Label>
								<asp:Image ID="BtnCopiarFechaEnt" runat="server" ImageUrl="../../Images/aplicar_plus.gif" Style="cursor:pointer; margin-left:10px; vertical-align:middle;" ClientIDMode="static" />
							</div>
						</div>     
						<div style="line-height:2em;">
							<input type="radio" name="fecPlanEntrega" id="rbPlanEntrega" /> 
							<asp:Label runat="server" ID="lblPlanEntrega" CssClass="Etiqueta"></asp:Label>     
						</div>
					</td>
				</tr>

			</table>        
			<div id="divAtributosLinea" runat="server" style="position:relative; margin-left:5px; float:left; padding:6px;">
				<table id="tblAtributosLinea" style="border: 1px solid #E8E8E8; width: 100%; table-layout: fixed;
					border-collapse: collapse">
					<tr class="border" style="background-color: #cccccc; height: 20px">
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblIdAtributoTablaLinea" runat="server"></asp:Label>
						</td>
						<td style="width: 19%; height: 20px">
							<asp:Label ID="lblCodigoAtributoTablaLinea" runat="server"></asp:Label>
						</td>
						<td style="width: 37%; height: 20px">
							<asp:Label ID="lblDenomAtributoTablaLinea" runat="server"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblTipoAtributoTablaLinea" runat="server"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblIntroAtributoTablaLinea" runat="server"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblObligAtributoTablaLinea" runat="server"></asp:Label>
						</td>
						<td style="width: 37%; height: 22px">
							<asp:Label ID="lblValorAtributoTablaLinea" runat="server"></asp:Label>
						</td>
						<td style="width: 3%; height: 22px">
							<asp:Label ID="lblCopiarDatosLinea" runat="server"></asp:Label>
						</td>
					</tr>
				</table>
			</div>
			<fieldset id="fsTextObserLin" runat="server" class="RectanguloEP" style="margin-top: 15px;
				margin-bottom: 10px; width: 98%; position: relative; margin-left: 12px; float: left;">
				<legend align="left">
					<asp:Label ID="lblObsGenLin" CssClass="Etiqueta" runat="server" Text="DObservaciones Linea"
						ClientIDMode="Static"></asp:Label></legend>
				<textarea id="textObserLin" rows="3" style="width: 99%;" runat="server" clientidmode="Static"></textarea>
			</fieldset>
			<!-- Panel adjuntos de art�culo -->
			<fieldset id="fsTableAdjuntosLin" runat="server" class="RectanguloEP" style="margin-top: 10px;
				margin-left: 12px; margin-bottom: 10px; width: 98%;">
				<legend align="left">
					<asp:Label ID="lblArcPedLin" CssClass="Etiqueta" runat="server" Text="DArchivos para la linea"
						ClientIDMode="Static"></asp:Label></legend>
				<div id="tablaAdjuntosLin">
				</div>
				<br />
				<div id="DivPanelAdjuntosLin" class="modalPopup" style="display: none;" style="display: none;
					position: absolute; z-index: 10003; width: 300px; min-height: 70px;">
					<!-- Panel de adjuntos del pedido -->
					<table id="PanelAdjunPedTituloLin" cellpadding="4" cellspacing="0" border="0">
						<tr>
							<td valign="bottom">
								<asp:Image ID="Image1Lin" runat="server" SkinID="CabAdjuntos" Style="cursor: pointer;" />
							</td>
							<td valign="bottom">
								<asp:Label ID="Label1Lin" runat="server" CssClass="RotuloGrande" EnableViewState="False"
									Text="...."></asp:Label>
							</td>
						</tr>
					</table>
					<br />
					<table id="PanelAdjunPedCuerpoLin" cellpadding="4" cellspacing="0" border="0">
						<tr>
							<td>
								<input id="filesLin" name="filesLin" type="file" size="55px" width="380px" />
							</td>
						</tr>
						<tr>
							<td>
								<span id="span1" class="Texto12" style="line-height: 25px;">
									<asp:Label ID="lblObservacionesAdjuntoLin" runat="server" CssClass="Etiqueta" Text="DAdjuntar archivos"></asp:Label>
								</span>
							</td>
						</tr>
						<tr>
							<td>
								<textarea id="comentarioAdjuntoLin" rows="5" cols="40"></textarea>
							</td>
						</tr>
						<tr>
							<td align="center">
								<table id="PanelAdjunPedConfirmacionCancelacionLin" cellpadding="10" cellspacing="0"
									border="0">
									<tr>
										<td>                                        
											<div>
												<span onclick="subirArchivoLin()"><a class="Boton" id="txtAceptarAdjLin" style="color: White;
													background-color: rgb(171, 0, 32); font-size: 12px; float: right; text-decoration: none;
													border-style: none; border-color: inherit; border-width: 0px; padding: 3px 10px;
													border-radius: 4px;" onfocus="this.blur();" onmouseup="this.style.padding='3px 10px 3px 10px';this.style.cursor='hand';"
													onmousedown="this.style.padding='4px 10px 2px 10px';this.style.cursor='hand';"
													onmouseout="this.style.backgroundColor='#AB0020';this.style.padding='3px 10px 3px 10px';this.style.cursor='pointer';"
													onmouseover="this.style.backgroundColor='#780000';this.style.cursor='hand';"></a>
												</span>
											</div>
										</td>
										<td>
											<div>
												<span onclick="ocultarAdjuntarLin()"><a id="txtCancelarAdjLin" class="Boton" style="color: White;
													background-color: rgb(171, 0, 32); font-size: 12px; float: right; text-decoration: none;
													border-style: none; border-color: inherit; border-width: 0px; padding: 3px 10px;
													border-radius: 4px;" onfocus="this.blur();" onmouseup="this.style.padding='3px 10px 3px 10px';this.style.cursor='hand';"
													onmousedown="this.style.padding='4px 10px 2px 10px';this.style.cursor='hand';"
													onmouseout="this.style.backgroundColor='#AB0020';this.style.padding='3px 10px 3px 10px';this.style.cursor='pointer';"
													onmouseover="this.style.backgroundColor='#780000';this.style.cursor='hand';"></a>
												</span>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<asp:HiddenField ID="HiddenField1Lin" runat="server" ClientIDMode="Static" Value="test" />
				<asp:TextBox ID="listaAdjuntosJSONLin" runat="server" ClientIDMode="Static" Style="display: none;"
					Text="[]"></asp:TextBox>
				<div id="listaAdjuntosLin" style="clear: both; margin-top: 5px; width: 50%;">
				</div>
				<div id="NuevoAdjuntoLin" class="OtrasOpciones" style="border: solid 1px Transparent;"
					onclick="mostrarAdjuntarLin()">
					<img id="imgNuevoAdjuntoLin" class="Image20" alt="Nuevo Adjunto" src="<%=ConfigurationManager.AppSettings("ruta")%>images/adjunto.png"
						style="margin-left: 5px; vertical-align: bottom; cursor: pointer;" />
					<span id="spanNuevoAdjuntoLin" class="Texto12" style="line-height: 25px;">
						<asp:Label ID="lblNuevoAdjuntoLin" runat="server" Text="DAdjuntar archivos"></asp:Label>
					</span>
				</div>
			</fieldset>
		</div>
		<div id="DatosDestinos" style="display: none; z-index: 1005;" class="popupCN"  >
			<table cellspacing="0" border="0" width="100%">
				<tr>
					<td valign="bottom">
						<asp:Image ID="imgDestinos" runat="server" SkinID="CabDestinos" />
					</td>
					<td>
						<asp:Label ID="LblTxtSelecDest" runat="server" CssClass="RotuloGrande" Text="DSeleccione destino de la lista"
							EnableViewState="False"></asp:Label>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<br />
						<div id="cboDest" style="float: left; height: 22px; width: 99%;"></div>
						<table cellpadding="4" cellspacing="2" border="0" width="100%">
							<tr>
								<td class="FilaSeleccionada" style="width: 15%;">
									<asp:Label ID="LblCodDest" runat="server" CssClass="Normal" EnableViewState="False" Text="DCodigo"></asp:Label>
								</td>
								<td class="FilaPar2">
									<asp:Label ID="LblCodDestSrv" runat="server" CssClass="Normal" EnableViewState="False" ClientIDMode="static"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="FilaSeleccionada" style="width: 15%;">
									<asp:Label ID="LblDenDest" runat="server" CssClass="Normal" Text="DDenominaci�n" EnableViewState="False"></asp:Label>
								</td>
								<td class="FilaPar2">
									<asp:Label ID="LblDenDestSrv" CssClass="Normal" runat="server" EnableViewState="True" ClientIDMode="static"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="FilaSeleccionada">
									<asp:Label ID="LblDirecDest" runat="server" CssClass="Normal" EnableViewState="False" Text="DDireccion"></asp:Label>
								</td>
								<td class="FilaPar2">
									<asp:Label ID="LblDirecDestSrv" runat="server" CssClass="Normal" EnableViewState="False" ClientIDMode="static"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="FilaSeleccionada">
									<asp:Label ID="LblCpDest" runat="server" CssClass="Normal" EnableViewState="False" Text="DCP"></asp:Label>
								</td>
								<td class="FilaPar2">
									<asp:Label ID="LblCpDestSrv" runat="server" CssClass="Normal" EnableViewState="False" ClientIDMode="static"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="FilaSeleccionada">
									<asp:Label ID="LblPobDest" runat="server" CssClass="Normal" Text="DPoblaci�n" EnableViewState="False"></asp:Label>
								</td>
								<td class="FilaPar2">
									<asp:Label ID="LblPobDestSrv" runat="server" CssClass="Normal" EnableViewState="False" ClientIDMode="static"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="FilaSeleccionada">
									<asp:Label ID="LblProvDest" CssClass="Normal" runat="server" Text="DProv." EnableViewState="False"></asp:Label>
								</td>
								<td class="FilaPar2">
									<asp:Label ID="LblProvDestSrv" runat="server" CssClass="Normal" EnableViewState="False" ClientIDMode="static"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="FilaSeleccionada">
									<asp:Label ID="LblPaisDest" runat="server" CssClass="Normal" Text="DPa�s" EnableViewState="False"></asp:Label>
								</td>
								<td class="FilaPar2">
									<asp:Label ID="LblPaisDestSrv" runat="server" CssClass="Normal" EnableViewState="False"
										ClientIDMode="static"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="FilaSeleccionada">
									<asp:Label ID="LblTfnoDest" runat="server" CssClass="Normal" Text="DTfno" EnableViewState="False"></asp:Label>
								</td>
								<td class="FilaPar2">
									<asp:Label ID="LblTfnoDestSrv" runat="server" CssClass="Normal" EnableViewState="False"
										ClientIDMode="static"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="FilaSeleccionada">
									<asp:Label ID="LblFAXDest" runat="server" CssClass="Normal" Text="DFAX" EnableViewState="False"></asp:Label>
								</td>
								<td class="FilaPar2">
									<asp:Label ID="LblFAXDestSrv" runat="server" CssClass="Normal" EnableViewState="False"
										ClientIDMode="static"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="FilaSeleccionada">
									<asp:Label ID="LblEmailDest" runat="server" CssClass="Normal" Text="DEmail" EnableViewState="False"></asp:Label>
								</td>
								<td class="FilaPar2">
									<asp:Label ID="LblEmailDestSrv" runat="server" CssClass="Normal" EnableViewState="False"
										ClientIDMode="static"></asp:Label>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<br />
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td align="right">
									<input id="BtnAcepDest" type="button" value="Aceptar" runat="server" clientidmode="Static" />
								</td>
								<td style="width: 25px">
									&nbsp;
								</td>
								<td align="left">
									<input id="BtnCancDest" type="button" value="Cancelar" runat="server" clientidmode="Static" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<div id="CostesLinea" class="Texto12 RectanguloEP" style="display: none; padding: 0.2em 0.3em;
			margin-bottom: 10px; overflow: auto;">
			<table style="border-collapse: collapse; width: 100%;">
				<tr class="FilaSeleccionada" style="height: 30px;">
					<td style="width: 2%;">
						<asp:Label ID="lblNumLineaCoste" runat="server" CssClass="Etiqueta" Text="DLinea"></asp:Label>:
					</td>
					<td style="width: 98%;">
						<asp:Label ID="lblLineaCoste" runat="server" CssClass="Normal" Width="100%" Text=""
							ClientIDMode="static"></asp:Label>
					</td>
				</tr>
			</table>
			<hr width="100%" />
			<div id="divAtributosCosteLinea" runat="server" style="position: relative;">
				<table id="tblAtributosCosteLinea" style="border: 1px solid #E8E8E8; width: 100%;
					border-collapse: collapse">
					<tr class="border" style="background-color: #cccccc; height: 20px">
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblIdCosteLinea" runat="server" Text="DId"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px">
							<input type="checkbox" style="visibility: hidden;" />
						</td>
						<td style="width: 25%; height: 20px">
							<asp:Label ID="lblCodCosteLinea" CssClass="Etiqueta" runat="server" Text="DCodigo"></asp:Label>
						</td>
						<td style="width: 38%; height: 20px">
							<asp:Label ID="lblDenCosteLinea" CssClass="Etiqueta" runat="server" Text="DDenominacion"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblTipoCosteLinea" CssClass="Etiqueta" runat="server" Text="DTipo"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblIntroCosteLinea" CssClass="Etiqueta" runat="server" Text="DIntro"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblOblCosteLinea" CssClass="Etiqueta" runat="server" Text="DObligatorio"></asp:Label>
						</td>
						<td style="width: 15%; height: 22px">
							<asp:Label ID="lblValCosteLinea" CssClass="Etiqueta" runat="server" Text="DValor"></asp:Label>
						</td>
						<td style="width: 18%; height: 22px">
							<asp:Label ID="lblImpCosteLinea" CssClass="Etiqueta" runat="server" Text="DImporte"></asp:Label>
						</td>
					</tr>
				</table>
				<table id="Table2" style="border: 1px solid #E8E8E8; width: 100%; border-collapse: collapse;
					margin-top: 2px;">
					<tr class="border" style="background-color: #cccccc; height: 20px">
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblCosteTotalLinea_hdd" runat="server" ClientIDMode="Static" Text="0"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px">
							<input type="checkbox" style="visibility: hidden;" />
						</td>
						<td style="width: 63%; height: 20px">
							<asp:Label ID="lblTotCostesLinea" runat="server" Text="DTotal Costes Generales"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;">
						</td>
						<td style="width: 1%; height: 20px; display: none;">
						</td>
						<td style="width: 1%; height: 20px; display: none;">
						</td>
						<td style="width: 15%; height: 22px">
						</td>
						<td style="width: 18%; height: 22px">
							<asp:Label ID="lblCosteTotalLinea" runat="server" ClientIDMode="Static" Style="width: 97%;
								border: none; text-align: right; display: block;"></asp:Label>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div id="DescuentosLinea" class="Texto12 RectanguloEP" style="display: none; padding: 0.2em 0.3em; margin-bottom: 10px; overflow: auto;">
			<table style="border-collapse: collapse; width: 100%;">
				<tr class="FilaSeleccionada" style="height: 30px;">
					<td style="width: 2%;">
						<asp:Label ID="lblNumLineaDescuento" runat="server" CssClass="Etiqueta" Text="DLinea"></asp:Label>:
					</td>
					<td style="width: 98%;">
						<asp:Label ID="lblLineaDescuento" runat="server" CssClass="Normal" Width="100%" Text=""
							ClientIDMode="static"></asp:Label>
					</td>
				</tr>
			</table>
			<hr width="100%" />
			<div id="divAtributosDescuentoLinea" runat="server">
				<table id="tblAtributosDescuentoLinea" style="border: 1px solid #E8E8E8; width: 100%;
					border-collapse: collapse">
					<tr class="border" style="background-color: #cccccc; height: 20px">
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblIdDescuentoLinea" runat="server" Text="DId"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px">
							<input type="checkbox" style="visibility: hidden;" />
						</td>
						<td style="width: 25%; height: 20px">
							<asp:Label ID="lblCodDescuentoLinea" CssClass="Etiqueta" runat="server" Text="DCodigo"></asp:Label>
						</td>
						<td style="width: 38%; height: 20px">
							<asp:Label ID="lblDenDescuentoLinea" CssClass="Etiqueta" runat="server" Text="DDenominacion"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblTipoDescuentoLinea" CssClass="Etiqueta" runat="server" Text="DTipo"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblIntroDescuentoLinea" CssClass="Etiqueta" runat="server" Text="DIntro"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblOblDescuentoLinea" CssClass="Etiqueta" runat="server" Text="DObligatorio"></asp:Label>
						</td>
						<td style="width: 15%; height: 22px">
							<asp:Label ID="lblValDescuentoLinea" CssClass="Etiqueta" runat="server" Text="DValor"></asp:Label>
						</td>
						<td style="width: 18%; height: 22px">
							<asp:Label ID="lblImpDescuentoLinea" CssClass="Etiqueta" runat="server" Text="DImporte"></asp:Label>
						</td>
					</tr>
				</table>
				<table id="Table1" style="border: 1px solid #E8E8E8; width: 100%; border-collapse: collapse;
					margin-top: 2px;">
					<tr class="border" style="background-color: #cccccc; height: 20px">
						<td style="width: 1%; height: 20px; display: none;">
							<asp:Label ID="lblDescuentoTotalLinea_hdd" runat="server" ClientIDMode="Static" Text="0"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px;">
							<input type="checkbox" style="visibility: hidden;" />
						</td>
						<td style="width: 63%; height: 20px">
							<asp:Label ID="lblTotDescuentoLinea" runat="server" Text="DTotal Descuentos Generales"></asp:Label>
						</td>
						<td style="width: 1%; height: 20px; display: none;">
						</td>
						<td style="width: 1%; height: 20px; display: none;">
						</td>
						<td style="width: 1%; height: 20px; display: none;">
						</td>
						<td style="width: 15%; height: 22px;">
						</td>
						<td style="width: 18%; height: 22px;">
							<asp:Label ID="lblDescuentoTotalLinea" runat="server" ClientIDMode="Static" Style="width: 97%;
								border: none; text-align: right; display: block;"></asp:Label>
						</td>
					</tr>
				</table>
			</div>
		</div>    
		<div id="PlanesEntrega" class="Texto12 RectanguloEP" style="display: none; padding: 0.2em 0.3em; margin-bottom: 10px; overflow: auto;">
			<div>
				<label id="checkRecepcionAutomatica">
					<input type="checkbox" id="chkRecepcionAutomatica" />
					<asp:Label runat="server" ID="lblRecepcionAutomatica" ClientIDMode="static"></asp:Label>
				</label>
			</div>
			<table id="tblPlanesEntrega" style="border-collapse:collapse; border-spacing:0.1em; padding:0em; margin-top:1em;">
                <thead>
				    <tr class="CabeceraBotones">
					    <th style="width:3em; height:2em;">

					    </th>
					    <th style="width:10em;">
						    <span id="lblCabeceraPlanEntregaFechaEntrega"></span>
					    </th>
					    <th style="width:12em;">
						    <span id="lblCabeceraPlanEntregaCantidadImporteEntrega"></span>
					    </th>
				    </tr>
                </thead>
                <tbody></tbody>
			</table>            
			<div style="margin-top:1em;">
				<input type="button" id="btnAgregarPlanEntrega" class="botonRedondeado" />
				<input type="button" id="btnEliminarPlanEntrega" class="botonRedondeado" />
                <asp:Image ID="btnCopiarPlanEntrega" runat="server" ImageUrl="../../Images/aplicar_plus.gif" Style="cursor:pointer; margin-left:1em; vertical-align:middle;" ClientIDMode="static" />
			</div>
		</div>
		<div style="text-align:right; margin:1em;">
			<span class="botonRedondeado" id="1btnGuardarCesta" style="display:none; white-space:nowrap;"></span>
			<span class="botonRedondeado" id="1btnEmitirPedido" style="display:none; white-space:nowrap;"></span>
		</div>   
	</div>
	<!-- Panel Categor�as -->
	<asp:UpdatePanel ID="updpnlPopupCategorias" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<input id="btnOcultoCat" type="button" value="button" runat="server" style="display: none" />
			<asp:Panel ID="PanelOculto" runat="server" Style="display: none">
			</asp:Panel>
			<asp:Panel ID="PanelArbol" runat="server" BackColor="White" BorderColor="DimGray"
				BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display: none;
				min-width: 350px; padding: 2px" onmouseout="FSNCerrarPanelesTemp()" onmouseover="FSNNoCerrarPaneles()">
				<table width="350">
					<tr>
						<td align="left">
							<asp:Image ID="ImgCatalogoArbol" runat="server" ImageUrl="~/images/categorias_small.gif" />
							&nbsp; &nbsp;
							<asp:Label ID="LblCatalogoArbol" runat="server" CssClass="Rotulo">
							</asp:Label>
						</td>
						<td align="right">
							<asp:ImageButton runat="server" ID="ImgBtnCerrarPanelArbol" ImageUrl="~/images/Bt_Cerrar.png" />
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<asp:UpdatePanel ID="pnlCategorias" runat="server" UpdateMode="Conditional">
								<ContentTemplate>
									<table width="100%">
										<tr>
											<td>
												<asp:TreeView ID="fsnTvwCategorias" NodeIndent="20" CssClass="Normal" ShowExpandCollapse="true"
													ExpandDepth="0" EnableViewState="True" runat="server" EnableClientScript="true"
													ForeColor="Black" PopulateNodesFromClient="true">
													<Nodes>
														<asp:TreeNode PopulateOnDemand="true" Value="_0_" SelectAction="None" Expanded="false">
														</asp:TreeNode>
													</Nodes>
												</asp:TreeView>
											</td>
										</tr>
									</table>
								</ContentTemplate>
							</asp:UpdatePanel>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<ajx:ModalPopupExtender ID="mpePanelArbol" runat="server" PopupControlID="PanelArbol"
				TargetControlID="btnOcultoCat" CancelControlID="ImgBtnCerrarPanelArbol" RepositionMode="None"
				PopupDragHandleControlID="PanelOculto">
			</ajx:ModalPopupExtender>
		</ContentTemplate>
	</asp:UpdatePanel>
	<asp:HiddenField ID="hid_HaSidoEditada" runat="server" Value='0'/>
	<asp:HiddenField ID="hid_CodArticulo" runat="server" />
	<asp:HiddenField ID="hid_lineaPedido" runat="server" />
	<asp:HiddenField ID="hid_cantidad" runat="server" />
	<asp:HiddenField ID="hid_Precio" runat="server" />
	<asp:HiddenField ID="hid_TipoRecepcion" runat="server" /> 
	<div id="pnlConfirmacionPedidoEmitido" style="display: none; width: 600px; height: 150px;
		position: absolute; z-index: 11000; background-color: white">
		<div style="float: left; width: 100%; margin: 10px">
			<asp:Label ID="lblEstadoEmisionPedido" runat="server" CssClass="Rotulo" ClientIDMode="Static"
				Text="DEl pedido se ha emitido correctamente"></asp:Label>
		</div>
		<div style="width: 100%; float: left">
			<table style="width: 100%">
				<tr>
					<th style="text-align: left; padding-left: 5px; width: 22%">
						<asp:Label ID="lblPedidoCabeceraConfirmPedido" runat="server" Text="DPedido" Font-Bold="true"></asp:Label>
					</th>
					<th style="text-align: left; width: 22%">
						<asp:Label ID="lblProveedorCabeceraConfirmPedido" runat="server" Font-Bold="true"></asp:Label>
					</th>
					<th style="text-align: left; width: 22%">
						<asp:Label ID="lblImporteCabeceraConfirmPedido" runat="server" Text="DImporte" Font-Bold="true"></asp:Label>
					</th>
					<th style="text-align: left; width: 34%">
						<asp:Label ID="lblEstadoCabeceraConfirmPedido" runat="server" Text="DEstado" Font-Bold="true"></asp:Label>
					</th>
				</tr>
				<tr>
					<td style="padding-left: 5px">
						<a id="hypPedidoConfirmPedido">D2014/93/25</a>
					</td>
					<td>
						<asp:Label ID="lblProveedorConfirmPedido" runat="server" Text="DES1567864" Font-Bold="true"
							ClientIDMode="Static" Style="cursor: pointer; color: Gray;"></asp:Label>
					</td>
					<td style="text-align: right">
						<asp:Label ID="lblImporteConfirmPedido" runat="server" Text="D198,567 EUR" ClientIDMode="Static"></asp:Label>
					</td>
					<td>
						<asp:Label ID="lblEstadoConfirmPedido" runat="server" ClientIDMode="Static" Text="DPendiente de aprobar"></asp:Label>
					</td>
				</tr>
			</table>
			<center>
				<table cellpadding="0" cellspacing="20" border="0" id="tbAceptarOGuardarResumenPedido"
					runat="server">
					<tr>
						<td>
							<span id="btnAceptarConfirmPedido" class="botonRedondeado">DAceptar</span>
						</td>
					</tr>
				</table>
			</center>
		</div>
	</div>    
	<!-- Panel Activos -->
	<div id="divActivos" style="display: none; z-index: 1005;">
		<fsn:SMActivos ID="SMActivosEP" runat="server" BackColor="White" BorderColor="DimGray"
			BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left" Style="min-width: 700px;
			padding: 3px" Width="700px" AllowSelection="true" />
	</div>
	<!-- Panel Direccion Envio Factura -->
	<fsn:FSNPanelInfo ID="FSNPanelDireccionEnvioFactura" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx"
		ServiceMethod="DireccionEnvioFactura_Datos" Width="380px" Height="200px" EnableViewState="False"
		cssTitulo="TituloPopUpPanelInfo" cssSubTitulo="SubTituloPopUpPanelInfo" />
	<!-- Panel Info Proveedor -->
	<fsn:FSNPanelInfo ID="FSNPanelDatosProveCab" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx"
		ServiceMethod="Proveedor_DatosContacto" TipoDetalle="1" />
	<!-- Panel Detalle Art�culo -->
	<asp:Button ID="btnMostrarDetalleArticulo" runat="server" EnableViewState="false"
		Style="display: none" />
	<asp:UpdatePanel ID="upDetalleArticulo" runat="server" UpdateMode="Conditional">
		<Triggers>
			<asp:AsyncPostBackTrigger ControlID="btnMostrarDetalleArticulo" EventName="Click" />
		</Triggers>
		<ContentTemplate>
			<fsep:DetArticuloPedido ID="pnlDetalleArticulo2" runat="server" />
		</ContentTemplate>
	</asp:UpdatePanel>
	<!-- Cargando -->
	<asp:Panel ID="panelUpdateProgression" runat="server" CssClass="updateProgress" Style="display: none"
		ClientIDMode="Static">
		<div style="position: absolute; top: 30%; left: 50%; text-align: center; z-index: 10005;">
			<asp:Image ID="ImgProgress" runat="server" SkinID="ImgProgress" />
			<asp:Label ID="LblProcesando" runat="server" ForeColor="Black" Text=""></asp:Label>
		</div>
	</asp:Panel>
	<ajx:ModalPopupExtender ID="ModalProgression" runat="server" TargetControlID="panelUpdateProgression"
		BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" ClientIDMode="Static" />
	<!--Panel detalle de art�culo-->
	<fsep:DetArticuloCatalogo ID="pnlDetalleArticulo" runat="server" EnableViewState="true" />
	<!-- Panel Info Unidad -->
	<fsn:FSNPanelTooltip ID="FSNPanelDatosUnidad" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx"
		ServiceMethod="Unidad_Datos" EnableViewState="False" />
	<!-- Panel Adjunto-->
	<asp:Panel ID="PanelAdjunPed" runat="server" Style="padding: 10px; display: none;"
		CssClass="modalPopup">
		<table cellpadding="4" cellspacing="0" border="0">
			<tr>
				<td valign="bottom">
					<asp:Image ID="imgCabAdjunPedido" runat="server" SkinID="CabAdjuntos" />
				</td>
				<td valign="bottom">
					<asp:Label ID="LblArchAdjunPed" runat="server" CssClass="RotuloGrande" EnableViewState="False"></asp:Label>
				</td>
			</tr>
		</table>
		<br />
		<table cellpadding="4" cellspacing="0" border="0">
			<tr>
				<td>
					<asp:FileUpload ID="fuAdjunPed" runat="server" size="55px" Width="380px" CssClass="Normal"
						EnableViewState="False" />
				</td>
			</tr>
			<tr>
				<td>
					<asp:TextBox ID="TxtComenAdjunPed" runat="server" TextMode="MultiLine" Rows="5" Columns="40"
						CssClass="Normal" Width="380px" EnableViewState="False"></asp:TextBox>
				</td>
			</tr>
			<tr>
				<td align="center">
					<table cellpadding="10" cellspacing="0" border="0">
						<tr>
							<td>
								<fsn:FSNButton ID="BtnAcepAdjunPed" runat="server" Text="DAceptar"></fsn:FSNButton>
							</td>
							<td>
								<fsn:FSNButton ID="BtnCancelAdjunPed" runat="server" Text="DCancelar" EnableViewState="False"></fsn:FSNButton>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</asp:Panel>
	<%--Panel Selecci�n centro de coste --%>
	<input id="btnCentrosOculto" type="button" value="button" runat="server" style="display: none" />
	<asp:Panel ID="pnlCentrosOculto" runat="server" Style="display: none">
	</asp:Panel>
	<asp:Panel ID="pnlCentros" runat="server" BackColor="White" BorderColor="DimGray"
		BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display: none;
		padding: 2px">
		<table width="450">
			<tr>
				<td align="left" style="line-height: 30px;">
					<img src="../../Images/centrocoste.JPG" style="padding-left: 10px; vertical-align: middle;
						cursor: pointer;" />
					<asp:Label ID="lblCentros" runat="server" CssClass="RotuloGrande" Style="padding-left: 10px;">
					</asp:Label>
				</td>
				<td align="right" valign="top">
					<asp:ImageButton runat="server" ID="ImgBtnCerrarPanelCentros" ImageUrl="~/images/Bt_Cerrar.png"
						Style="vertical-align: top; cursor: pointer;" OnClientClick="ocultarPanelModal('mpeCentros');" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<asp:Label ID="lblFiltroCentros" runat="server" CssClass="Rotulo" />
					<input class="Normal" type="text" id="FiltroCentros" runat="server" style="margin-bottom: 5px;
						width: 320px;" />
					<div id="listadoCentros" runat="server" style="height: 400px; overflow: auto; border-top: solid 1px #909090;
						clear: both;">
					</div>
				</td>
			</tr>
		</table>
	</asp:Panel>
	<ajx:ModalPopupExtender ID="mpeCentros" runat="server" PopupControlID="pnlCentros"
		TargetControlID="btnCentrosOculto" CancelControlID="ImgBtnCerrarPanelCentros"
		RepositionMode="None" PopupDragHandleControlID="pnlCentrosOculto" ClientIDMode="Static">
	</ajx:ModalPopupExtender>
	<!-- Panel Cambio Unidad -->
	<div id="pnlUnidad" style="display: none; z-index: 1005;" class="popupCN">
		<table cellpadding="2" cellspacing="0" border="0" width="350px">
			<tr>
				<td valign="bottom">
					<asp:Image ID="imgUnidades" runat="server" SkinID="CabUnidades" />
				</td>
				<td>
					<asp:Label ID="lblCabUnidades" runat="server" CssClass="RotuloGrande" EnableViewState="False"></asp:Label>
					<div style="display: none;">
						<asp:Label ID="lblIdLineaUnidad" runat="server" ClientIDMode="Static" EnableViewState="False"></asp:Label></div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<br />
					<div id='cmbUnidades' style="float: left; height: 22px; width: 99%;">
					</div>
					<br />
					<br />
					<table cellpadding="2" cellspacing="2" border="0" width="100%">
						<tr>
							<td class="FilaSeleccionada" style="width: 50%;">
								<asp:Label ID="lblCodUnidad" runat="server" CssClass="Normal" ClientIDMode="Static"
									EnableViewState="False"></asp:Label>
							</td>
							<td class="FilaPar2">
								<asp:Label ID="lblCodUnidadSrvDesc" runat="server" CssClass="Normal" ClientIDMode="Static"
									EnableViewState="False"></asp:Label>
								<div style="display: none;">
									<asp:Label ID="lblCodUnidadSrv" runat="server" ClientIDMode="Static" EnableViewState="False"></asp:Label></div>
							</td>
						</tr>
						<tr>
							<td class="FilaSeleccionada" style="width: 50%;">
								<asp:Label ID="lblUnidadCompra" runat="server" CssClass="Normal" ClientIDMode="Static"
									EnableViewState="False"></asp:Label>
							</td>
							<td class="FilaPar2">
								<asp:Label ID="lblUnidadCompraSrvDesc" runat="server" CssClass="Normal" ClientIDMode="Static"
									EnableViewState="False"></asp:Label>
								<div style="display: none;">
									<asp:Label ID="lblUnidadCompraSrv" runat="server" ClientIDMode="Static" EnableViewState="False"></asp:Label></div>
							</td>
						</tr>
						<tr>
							<td class="FilaSeleccionada">
								<asp:Label ID="lblFactorConversion" runat="server" CssClass="Normal" ClientIDMode="Static"
									EnableViewState="False"></asp:Label>
							</td>
							<td class="FilaPar2">
								<asp:Label ID="lblFactorConversionSrvDesc" runat="server" CssClass="Normal" ClientIDMode="Static"
									EnableViewState="False"></asp:Label>
								<div style="display: none;">
									<asp:Label ID="lblFactorConversionSrv" runat="server" ClientIDMode="Static" EnableViewState="False"></asp:Label></div>
							</td>
						</tr>
						<tr>
							<td class="FilaSeleccionada">
								<asp:Label ID="lblCantMinima" runat="server" CssClass="Normal" ClientIDMode="Static"
									EnableViewState="False"></asp:Label>
							</td>
							<td class="FilaPar2">
								<asp:Label ID="lblCantMinimaSrvDesc" runat="server" CssClass="Normal" ClientIDMode="Static"
									EnableViewState="False"></asp:Label>
								<div style="display: none;">
									<asp:Label ID="lblCantMinimaSrv" runat="server" ClientIDMode="Static" EnableViewState="False"></asp:Label></div>
								<asp:HiddenField ID="lblNumDecimales_hdd" runat="server" ClientIDMode="Static" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<br />
					<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td align="right">
								<fsn:FSNButton ID="btnAceptarUnidad1" runat="server" OnClientClick="return false;"
									Alineacion="Left" Style="white-space: nowrap;" />
							</td>
							<td style="width: 25px;">
								&nbsp;
							</td>
							<td align="left">
								<fsn:FSNButton ID="btnCancelarUnidad1" runat="server" OnClientClick="return false;"
									Alineacion="Left" Style="white-space: nowrap;" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<input id="btnOcultoUnidad" type="button" value="button" runat="server" style="display: none"
		disabled="disabled" enableviewstate="False" />
	<%--Panel Seleccion Partidas--%>
	<input id="btnPartidasOculto" type="button" value="button" runat="server" style="display: none" />
	<asp:Panel ID="pnlPartidasOculto" runat="server" Style="display: none">
	</asp:Panel>
	<asp:Panel ID="pnlPartidas" runat="server" BackColor="White" BorderColor="DimGray"
		BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display: none;
		padding: 2px">
		<table width="450">
			<tr>
				<td align="left">
					<img src="../../Images/centrocoste.JPG" style="padding-left: 10px; vertical-align: middle;
						cursor: pointer;" />
					<asp:Label ID="lblPartidaPres0" runat="server" CssClass="RotuloGrande" Style="padding-left: 10px;">
					</asp:Label>
				</td>
				<td align="right" valign="top">
					<asp:ImageButton runat="server" ID="ImgBtnCerrarPanelPartida" ImageUrl="~/images/Bt_Cerrar.png"
						Style="vertical-align: top; cursor: pointer;" OnClientClick="ocultarPanelModal('mpePartidas');" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div id="listadoInputsFiltro" runat="server">
						<asp:Label ID="lblFiltroPartida" runat="server" CssClass="Rotulo" />
					</div>
					<div id="listadoPartidas" runat="server">
					</div>
				</td>
			</tr>
		</table>
	</asp:Panel>
	<ajx:ModalPopupExtender ID="mpePartidas" runat="server" PopupControlID="pnlPartidas"
		TargetControlID="btnPartidasOculto" CancelControlID="ImgBtnCerrarPanelPartida"
		RepositionMode="None" PopupDragHandleControlID="pnlPartidasOculto" ClientIDMode="Static">
	</ajx:ModalPopupExtender>
</asp:Content>
