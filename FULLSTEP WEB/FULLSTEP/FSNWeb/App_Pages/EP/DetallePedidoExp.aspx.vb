﻿Imports Fullstep.FSNServer
Imports Fullstep.FSNLibrary
Public Class DetallePedidoExp
    Inherits FSNPage

    Private _Empresas As CEmpresas
    Private _Persona As CPersona
    Private _Destinos As CDestinos
    Private InstanciaAprob As String
    Private dblCostesCabecera As Double
    Private dblDescuentosCabecera As Double
    Private dblCostesLinea As Double
    Private dblDescuentosLinea As Double
    Private dblImporteTotal As Double
    Private dblTotalIva As Double
    Private dblTotal As Double
    Private dblImporteBruto As Double
    Private Desde As Byte
    Private costesDescuentosLineas() As Double
    Private descuentosLineas() As Double
    Private pedidoMon As String
    Private cambio As Double
    Dim oAtributosDescuentos As CAtributosPedido
    Private Enum Acciones
        AccionNula = 0
        NoAnularOrden = 1
        AnularOrden = 2
        AnularRecepcion = 3
        AnularLinea = 4
        BloquearFacturacionAlbaran = 5
        DesBloquearFacturacionAlbaran = 6
    End Enum
    Private Sub CargarTextos()
        LblProve.Text = Textos(50)

        lblDatosGenerales.Text = Textos(226)

        lblImporteBruto.Text = Textos(206)
        lblCostesGenerales.Text = Textos(209)
        lblDescuentosGenerales.Text = Textos(210)
        lblImporteTotal.Text = Textos(211)
        LblNumSap.Text = Textos(266) & ":"
        LblTipoPedido.Text = Textos(129) & ":"
        LblProveErp.Text = Textos(49)

        LblReceptor.Text = Textos(48)
        LblEmpresa.Text = Textos(47)
        LblGestorTitulo.Text = Textos(189) & ":"
        lblDireccionEnvioFactura.Text = Textos(165) & ":"
        lblCodCoste.Text = Textos(88)
        lblCodDescuento.Text = Textos(88)
        lblDenCoste.Text = Textos(89)
        lblDenDescuento.Text = Textos(89)
        lblValCoste.Text = Textos(237)
        lblValDescuento.Text = Textos(237)
        lblImpCoste.Text = Textos(12)
        lblImpDescuento.Text = Textos(12)
        lblNombre.Text = Textos(20)

        lblArcPed.Text = Textos(120)
        lblObsGen.Text = Textos(5) & ":"
        lblCostesCabecera.Text = Textos(209)
        lblDescuentosCabecera.Text = Textos(210)
        lblCodigoAtributoTablaOrden.Text = Textos(256)
        lblDenomAtributoTablaOrden.Text = Textos(257)
        lblValorAtributoTablaOrden.Text = Textos(258)
        lblOtrosDatos.Text = Textos(218)

        lblNumberFila.Text = String.Empty
        lblDenArtFila.Text = Textos(69)
        lblPrecUnitarioFila.Text = Textos(198)
        lblCantidadFila.Text = Textos(11)
        lblUnidadFila.Text = Textos(113)
        lblCostesFila.Text = Textos(207)
        lblDescuentosFila.Text = Textos(208)
        lblImpBrutoFila.Text = Textos(206)

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_AprobacionPedidos
        lblLineaPedido.Text = Textos(67).ToUpper()

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
        lblInfoPed.Text = Textos(109)
        lblCantPendFila.Text = Textos(195)
        lblImpPendFila.Text = Textos(265)
        lblEstadoFila.Text = Textos(44)
        lblFecSol.Text = Textos(118)
        lblFecEnt.Text = Textos(119)

        lblEstIntERP.Text = Textos(330) & ":"

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
    End Sub
    Private Sub CargarScripts()
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "NumberFormatUsu") Then
            With FSNUser
                Dim sVariablesJavascript As String = "var UsuNumberDecimalSeparator = '" & .NumberFormat.NumberDecimalSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberGroupSeparator='" & .NumberFormat.NumberGroupSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberNumDecimals='" & .NumberFormat.NumberDecimalDigits & "';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NumberFormatUsu", sVariablesJavascript, True)
            End With
        End If

        Dim idOrden As Long
        If Not Request.QueryString("Id") Is Nothing Then
            idOrden = Request.QueryString("Id")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sID", "<script>var sID = '" & idOrden & "' </script>")
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "tema") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "tema", _
                "<script>var tema = '" & Me.Page.Theme & "';</script>")
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutaPM") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", _
                "<script>var rutaPM = '" & System.Configuration.ConfigurationManager.AppSettings("rutaPM") & "';</script>")
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutaFS") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", _
                "<script>var rutaFS = '" & System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "';</script>")
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "ruta") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ruta",
                "<script>var ruta = '" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "';</script>")
        End If

        Dim hayIntegracionPedidos As Boolean = CBool(Session("HayIntegracionPedidos"))
        Dim sHayIntegracionPedidos As String

        hayIntegracionPedidos = True
        If hayIntegracionPedidos Then
            sHayIntegracionPedidos = "1"
        Else
            sHayIntegracionPedidos = "0"
        End If
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sHayIntegracionPedidos", "<script>var sHayIntegracionPedidos = '" & sHayIntegracionPedidos & "' </script>")

        'imgCabeceraConfirmacion.Src = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/FSPMWebPartAltaSolicitudes.gif"
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos

        Dim scriptReference As ScriptReference
        scriptReference = New ScriptReference
        scriptReference.Path = "~/App_Pages/EP/js/DetallePedidoExp.js"
        Me.ClientScript.RegisterClientScriptInclude("DetallePedidoExp", scriptReference.Path)
        'Me.ScriptMgr.Scripts.Add(scriptReference)

        InstanciaAprob = Request("Instancia")

        CargarTextos()
        Desde = Request("Desde")

        If Not IsPostBack Then
            CargarScripts()
            Dim idOrden As Integer
            If Not Request.QueryString("Id") Is Nothing Then
                idOrden = Request.QueryString("Id")
            End If
            dblTotalIva = 0
            dblTotal = 0
            Cargar_Datos_Generales(idOrden)
            Cargar_Adjuntos(idOrden)
            CargarAtributosOrden(idOrden)
            Cargar_Lineas(idOrden)
            Cargar_Costes_Descuentos(idOrden)
            lblImpBruto.Text = FormatNumber(dblImporteTotal, FSNUser.NumberFormat) + " " + pedidoMon
            lblCosteCabeceraTotal.Text = FormatNumber(dblCostesCabecera, FSNUser.NumberFormat) + " " + pedidoMon
            lblDescuentosCabeceraTotal.Text = FormatNumber(dblDescuentosCabecera, FSNUser.NumberFormat) + " " + pedidoMon
            dblTotal = dblImporteTotal + dblCostesCabecera - dblDescuentosCabecera + dblTotalIva
            pintarIVAs()
            lblImporteTotalCabecera.Text = FormatNumber(dblTotal, FSNUser.NumberFormat) + " " + pedidoMon
        End If

        If Request.QueryString("Desde") IsNot Nothing Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Desde", _
                "<script>var Desde=" & CType(Request.QueryString("Desde"), Integer) & ";</script>")
        End If
        If Request.QueryString("aprobador") IsNot Nothing Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "aprobador", _
                "<script>var aprobador='" & DBNullToStr(Request.QueryString("aprobador")) & "';</script>")
        End If
        If Request.QueryString("receptor") IsNot Nothing Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "receptor", _
                "<script>var receptor='" & DBNullToStr(Request.QueryString("receptor")) & "';</script>")
        End If
        If Request.QueryString("vpu") IsNot Nothing Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "vpu", _
                "<script>var vpu='" & CBool(Request.QueryString("vpu")).ToString & "';</script>")
        End If
        If Request.QueryString("vpou") IsNot Nothing Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "vpou", _
                "<script>var vpou='" & CBool(Request.QueryString("vpou")).ToString & "';</script>")
        End If

        'Pongo invisibles las imagenes de detalles para que no salgan en el pdf:
        cmdImprimir.Visible = False
        'Genero el pdf
        generarPDF()
        'End If
    End Sub
    'Pinto IVAs en el detalle de cabecera
    Public Sub pintarIVAs()

        Try
            Dim tRow As New HtmlTableRow()
            Dim tCell As New HtmlTableCell()
            Dim iVas As String()
            controlIVAs.InnerText = controlIVAs.InnerText.ToString().TrimEnd(",")
            If controlIVAs.InnerText.Trim() <> String.Empty Then
                iVas = controlIVAs.InnerText.Trim().Split(",")
                If iVas.Count > 0 Then
                    For Each i As Integer In iVas

                        Dim oLabelIva As TextBox
                        oLabelIva = divIVAs.FindControl("lblIvahdd_" + i.ToString())

                        Dim oLabelIvaDen As TextBox
                        oLabelIvaDen = divIVAs.FindControl("lblIvaDen_" + i.ToString())

                        tRow = New HtmlTableRow()
                        tRow.Attributes.Add("class", "FilaPar")
                        'iva costes cabecera
                        tCell = New HtmlTableCell()
                        tCell.InnerHtml = "<span class='Normal MensajeError'>" + oLabelIvaDen.Text + "</span> <div style='border-width: 2px; border-bottom-style: dotted; margin-left: 19%; vertical-align: bottom;'></div>"
                        tCell.Attributes.Add("background-color", "white")
                        tRow.Cells.Add(tCell)

                        Dim dIva As Double = Double.Parse(oLabelIva.Text.ToString())
                        Dim sIva As String = FormatNumber(dIva, FSNUser.NumberFormat)
                        tCell = New HtmlTableCell()
                        tCell.InnerHtml = "<span class='Normal'>" + sIva + " " + pedidoMon + "</span>"
                        tCell.Attributes.Add("align", "right")

                        tRow.Cells.Add(tCell)
                        ImportesPedido.Rows.Add(tRow)
                    Next
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Pinto los datos generales del pedido
    Public Sub Cargar_Datos_Generales(ByVal sIden As Integer)
        Try
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim cDetallePedidos As cDetallePedidos
            cDetallePedidos = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))

            Dim listaCostes As New List(Of CAtributoPedido)
            Dim listaDescuentos As New List(Of CAtributoPedido)
            Dim cDetallePedido As New CDetallePedido
            Dim oDatosDetallePedido As Object = cDetallePedidos.Cargar_Datos_Generales_Orden(FSNUser.CodPersona, sIden, FSNUser.IdiomaCod)
            Dim oDatosGenerales As CDatosGenerales
            oDatosGenerales = CType(oDatosDetallePedido(0), CDatosGenerales)
            cDetallePedido.DatosGenerales = oDatosGenerales
            cDetallePedido.IdPedido = CType(oDatosDetallePedido(1), Integer)

            FSNLinkProve.Text = oDatosGenerales.CodProve.ToString() + " - " + oDatosGenerales.DenProve

            lblPedido.Text = oDatosGenerales.Anyo.ToString() + "/" + oDatosGenerales.NumPedido.ToString() + "/" + oDatosGenerales.NumOrden.ToString()
            LblTipoPedidoDato.Text = oDatosGenerales.Tipo.Denominacion
            LblNumSapDato.Text = oDatosGenerales.Referencia.ToString()
            pedidoMon = oDatosGenerales.Moneda
            cambio = oDatosGenerales.Cambio
            If oDatosGenerales.DirEnvioDen <> String.Empty Then
                lblDireccionEnvioFacturaDato.Text = oDatosGenerales.DirEnvioDen
            Else
                lblDireccionEnvioFacturaDato.Text = String.Empty
            End If

            Dim hayIntegracionPedidos As Boolean = CBool(Session("HayIntegracionPedidos"))
            If hayIntegracionPedidos Then
                LblProveErpDato.Visible = True
                LblProveErp.Visible = True
                LblProveErpDato.Text = oDatosGenerales.CodErp
                lblEstIntERP.Visible = True
                lblEstIntERPDato.Visible = True
                lblEstIntERPDato.Text = DenominacionEstadoIntegracion(oDatosGenerales.EstadoIntegracionOrden)
            Else
                LblProveErpDato.Visible = False
                LblProveErp.Visible = False
                LblProveErpDato.Text = String.Empty
                lblEstIntERP.Visible = False
                lblEstIntERPDato.Visible = False
            End If

            textObser.Text = oDatosGenerales.Obs
            LblEmpresaDato.Text = oDatosGenerales.Emp

            Dim oPerContacto As CPersona
            oPerContacto = FSNServer.Get_Object(GetType(FSNServer.CPersona))
            oPerContacto.Cod = oDatosGenerales.CodReceptor
            Dim sDatosContacto As String = String.Empty
            If oPerContacto.Cod <> "" Then
                oPerContacto.CargarPersona()
                If oPerContacto.Email <> String.Empty Then
                    sDatosContacto = oPerContacto.Email
                End If
                If oPerContacto.Tfno <> String.Empty Then
                    If sDatosContacto <> String.Empty Then
                        sDatosContacto = sDatosContacto + "/"
                    End If
                    sDatosContacto = sDatosContacto + oPerContacto.Tfno
                End If
                If sDatosContacto <> String.Empty Then
                    sDatosContacto = oDatosGenerales.Receptor + " (" + sDatosContacto + ")"
                Else
                    sDatosContacto = oDatosGenerales.Receptor
                End If
            End If
            LblReceptorDato.Text = sDatosContacto

            oPerContacto = FSNServer.Get_Object(GetType(FSNServer.CPersona))
            oPerContacto.Cod = oDatosGenerales.CodGestor
            Dim sDatosContactoGestor As String = String.Empty
            If oPerContacto.Cod <> "" Then
                oPerContacto.CargarPersona()
                If oPerContacto.Email <> String.Empty Then
                    sDatosContactoGestor = oPerContacto.Email
                End If
                If oPerContacto.Tfno <> String.Empty Then
                    If sDatosContactoGestor <> String.Empty Then
                        sDatosContactoGestor = sDatosContactoGestor + "/"
                    End If
                    sDatosContactoGestor = sDatosContactoGestor + oPerContacto.Tfno
                End If
                If sDatosContactoGestor <> String.Empty Then
                    sDatosContactoGestor = oDatosGenerales.Gestor + " (" + sDatosContactoGestor + ")"
                Else
                    sDatosContactoGestor = oDatosGenerales.Gestor
                End If
            End If

            LblGestorCab.Text = sDatosContactoGestor
            LblNumSapDato.Text = oDatosGenerales.Referencia
            lblPrecUnitarioFila.Text += " (" + pedidoMon + ")"
            lblCostesFila.Text += " (" + pedidoMon + ")"
            lblDescuentosFila.Text += " (" + pedidoMon + ")"
            lblImpBrutoFila.Text += " (" + pedidoMon + ")"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>Recibe el estado en id y devuelve el texto que le corresponde en el idioma del usuario</summary>
    ''' <param name="IdEstadoInt">Id del estado</param>
    ''' <returns>el texto del estado en el idioma del usuario</returns>   
    Private Function DenominacionEstadoIntegracion(ByVal IdEstadoInt As Integer) As String
        Select Case CType(IdEstadoInt, CPedido.Estado)
            Case TiposDeDatos.EstadoIntegracion.ParcialmenteIntegrado
                Return Textos(341)
            Case TiposDeDatos.EstadoIntegracion.EsperaAcuseRecibo
                Return Textos(342)
            Case TiposDeDatos.EstadoIntegracion.ConError
                Return Textos(343)
            Case TiposDeDatos.EstadoIntegracion.TotalmenteIntegrado
                Return Textos(344)
        End Select
    End Function
    'Pinto los documentos adjuntos al pedido
    Public Sub Cargar_Adjuntos(ByVal sIden As String)
        Try
            Dim iId As Integer
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim cDetallePedido As cDetallePedidos
            cDetallePedido = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))

            Int32.TryParse(sIden, iId)

            Dim oAdjuntos As CAdjuntos
            Dim oAdjunto As Adjunto
            oAdjuntos = cDetallePedido.Cargar_Adjuntos_Cabecera_Orden(FSNUser.CodPersona, iId)
            If oAdjuntos.Count = 0 Then
                ArchivosObser.Visible = False
            Else

                For Each oAdjunto In oAdjuntos
                    Dim tRow As New HtmlTableRow()
                    Dim tCell As New HtmlTableCell()
                    tCell.InnerText = "- " + oAdjunto.Nombre
                    If DBNullToStr(oAdjunto.Comentario) <> String.Empty Then
                        tCell.InnerText = tCell.InnerText + " (" + oAdjunto.Comentario + ")"
                    End If
                    tRow.Cells.Add(tCell)
                    tablaAdjuntos.Rows.Add(tRow)
                Next

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Pinto los adjuntos a las lineas de pedido
    Public Function Cargar_Adjuntos_Lineas(ByRef tablaAdjuntosLinea As HtmlTable, ByVal sIden As String) As Boolean
        Try
            Dim iId As Integer
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim cDetallePedido As cDetallePedidos
            cDetallePedido = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))

            Int32.TryParse(sIden, iId)

            Dim oAdjuntos As CAdjuntos
            Dim oAdjunto As Adjunto
            oAdjuntos = cDetallePedido.Cargar_Adjuntos_Linea_Orden(FSNUser.CodPersona, iId)
            If oAdjuntos.Count = 0 Then
                Return False
            Else
                Dim tRow As New HtmlTableRow()
                Dim tCell As New HtmlTableCell()
                tCell.InnerText = Textos(114)
                tCell.Style.Add("font-size", "12px")
                tCell.Style.Add("min-height", "22px")
                tCell.Style.Add("font-weight", "bold")
                tCell.ColSpan = 13
                tRow.Cells.Add(tCell)
                tablaAdjuntosLinea.Rows.Add(tRow)
                For Each oAdjunto In oAdjuntos
                    tRow = New HtmlTableRow()
                    tCell = New HtmlTableCell()
                    tCell.Style.Add("width", "10%")
                    tCell.ColSpan = 2
                    tRow.Cells.Add(tCell)
                    tCell = New HtmlTableCell()
                    tCell.InnerText = "- " + oAdjunto.Nombre
                    If oAdjunto.Comentario <> String.Empty Then
                        tCell.InnerText = tCell.InnerText + " (" + oAdjunto.Comentario + ")"
                    End If
                    tCell.Style.Add("font-size", "12px")
                    tCell.Style.Add("min-height", "22px")
                    tCell.Style.Add("font-weight", "normal")
                    tCell.Style.Add("width", "90%")
                    tCell.ColSpan = 11
                    tRow.Cells.Add(tCell)
                    tRow.Style.Add("width", "95%")
                    tRow.Style.Add("margin", "0px auto")
                    tRow.Style.Add("text-align", "left")
                    tablaAdjuntosLinea.Rows.Add(tRow)
                Next
                Return True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    'Pinto los costes / descuentos del pedido
    Public Sub Cargar_Costes_Descuentos(ByVal sIden As String)
        Try

            dblCostesCabecera = 0
            dblDescuentosCabecera = 0

            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oDetallePedidos As cDetallePedidos
            oDetallePedidos = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))


            Dim iCodPedido As Long

            Dim oAtributos As CAtributosPedido
            Dim oAtributo As CAtributoPedido

            Dim oDetallePedido As New CDetallePedido

            Int32.TryParse(sIden, iCodPedido)

            oAtributos = oDetallePedidos.Cargar_Costes(iCodPedido, FSNUser.Idioma.ToString)
            If oAtributos.Count = 0 Then
                Costes.Visible = False
            End If
            For Each oAtributo In oAtributos
                Dim tRow As New HtmlTableRow()
                Dim tCell As New HtmlTableCell()
                tCell.InnerText = oAtributo.Id
                tCell.Visible = False
                tRow.Cells.Add(tCell)

                tCell = New HtmlTableCell()
                tCell.InnerText = oAtributo.Codigo
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)

                tCell = New HtmlTableCell()
                tCell.InnerText = oAtributo.Denominacion
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)

                Dim dblValor As Double
                tCell = New HtmlTableCell()
                If oAtributo.Operacion = "1" Then
                    tCell.InnerText = FormatNumber(oAtributo.Valor, FSNUser.NumberFormat) + " %"
                Else
                    tCell.InnerText = FormatNumber(oAtributo.ImporteBD * cambio, FSNUser.NumberFormat) + " " + pedidoMon
                End If
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)


                tCell = New HtmlTableCell()
                If oAtributo.Operacion = "1" Then
                    dblValor = dblImporteTotal * (oAtributo.Valor / 100)
                Else
                    dblValor = oAtributo.ImporteBD * cambio
                End If
                dblCostesCabecera = dblCostesCabecera + dblValor
                tCell.InnerText = FormatNumber(dblValor.ToString(), FSNUser.NumberFormat) + " " + pedidoMon
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)

                tblAtributosCoste.Rows.Add(tRow)
                Dim iva As CImpuestos
                For Each iva In oAtributo.Impuestos
                    'iva lineas coste
                    tRow = New HtmlTableRow()

                    tCell = New HtmlTableCell()
                    tCell.InnerText = iva.Id
                    tCell.Visible = False
                    tRow.Cells.Add(tCell)

                    tCell = New HtmlTableCell()
                    tCell.InnerText = String.Empty
                    tCell.Style.Add("border-right", "1px solid black")
                    tRow.Cells.Add(tCell)

                    tCell = New HtmlTableCell()
                    tCell.InnerText = iva.Denominacion
                    tCell.Style.Add("border-right", "1px solid black")
                    tRow.Cells.Add(tCell)

                    tCell = New HtmlTableCell()
                    tCell.InnerText = FormatNumber(iva.Valor.ToString(), FSNUser.NumberFormat) + " %"
                    tCell.Style.Add("border-right", "1px solid black")
                    tRow.Cells.Add(tCell)

                    Dim dIva As Double = oAtributo.ImporteBD * cambio * (iva.Valor / 100)
                    dblTotalIva = dblTotalIva + dIva
                    tCell = New HtmlTableCell()
                    tCell.InnerText = FormatNumber(dIva, FSNUser.NumberFormat) + " " + pedidoMon
                    tCell.Style.Add("border-right", "1px solid black")
                    tRow.Cells.Add(tCell)

                    tblAtributosCoste.Rows.Add(tRow)

                    Dim oLabelIva As TextBox
                    oLabelIva = divIVAs.FindControl("lblIvahdd_" + iva.Id.ToString())
                    If Not IsNothing(oLabelIva) Then
                        Dim dIvaAcumuluado = oLabelIva.Text
                        dIva = dIva + dIvaAcumuluado
                        oLabelIva.Text = dIva.ToString()
                    Else
                        controlIVAs.InnerText = controlIVAs.InnerText + iva.Id.ToString() + ","
                        Dim TextBox1 As System.Web.UI.WebControls.TextBox
                        TextBox1 = New TextBox()
                        TextBox1.ID = "lblIvaDen_" + iva.Id.ToString()
                        TextBox1.Style("Visible") = "true"
                        TextBox1.Text = iva.Denominacion.ToString()
                        divIVAs.Controls.Add(TextBox1)

                        TextBox1 = New TextBox()
                        TextBox1.ID = "lblIvahdd_" + iva.Id.ToString()
                        TextBox1.Style("Visible") = "true"
                        TextBox1.Text = dIva.ToString()
                        divIVAs.Controls.Add(TextBox1)
                    End If

                Next
            Next


            oAtributosDescuentos = oDetallePedidos.Cargar_Descuentos(iCodPedido)
            If oAtributosDescuentos.Count = 0 Then
                Descuentos.Visible = False
            End If
            For Each oAtributo In oAtributosDescuentos
                Dim tRow As New HtmlTableRow()
                Dim tCell As New HtmlTableCell()
                tCell.InnerText = oAtributo.Id
                tCell.Visible = False
                tRow.Cells.Add(tCell)

                tCell = New HtmlTableCell()
                tCell.InnerText = oAtributo.Codigo.ToString()
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)

                tCell = New HtmlTableCell()
                tCell.InnerText = oAtributo.Denominacion.ToString()
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)


                Dim dblValor As Double
                tCell = New HtmlTableCell()
                If oAtributo.Operacion = "1" Then
                    tCell.InnerText = FormatNumber(oAtributo.Valor.ToString(), FSNUser.NumberFormat) + " %"
                Else
                    tCell.InnerText = FormatNumber((oAtributo.Valor * cambio).ToString(), FSNUser.NumberFormat) + " " + pedidoMon
                End If
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)

                tCell = New HtmlTableCell()
                If oAtributo.Operacion = "1" Then
                    dblValor = dblImporteTotal * (oAtributo.Valor / 100)
                Else
                    dblValor = oAtributo.Valor * cambio
                End If
                dblDescuentosCabecera = dblDescuentosCabecera + dblValor

                tCell = New HtmlTableCell()
                tCell.InnerText = FormatNumber(dblValor.ToString(), FSNUser.NumberFormat) + " " + pedidoMon
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)

                tblAtributosDescuento.Rows.Add(tRow)
                Dim iva As CImpuestos
                For Each iva In oAtributo.Impuestos
                    tCell = New HtmlTableCell()
                    tCell.InnerText = iva.Id
                    tRow.Cells.Add(tCell)

                    tCell = New HtmlTableCell()
                    tCell.InnerText = String.Empty
                    tCell.Style.Add("border-right", "1px solid black")
                    tRow.Cells.Add(tCell)

                    tCell = New HtmlTableCell()
                    tCell.InnerText = iva.Denominacion.ToString()
                    tCell.Style.Add("border-right", "1px solid black")
                    tRow.Cells.Add(tCell)

                    tCell = New HtmlTableCell()
                    tCell.InnerText = iva.Valor.ToString() + " %"
                    tCell.Style.Add("border-right", "1px solid black")
                    tRow.Cells.Add(tCell)

                    tCell = New HtmlTableCell()
                    tCell.InnerText = FormatNumber((oAtributo.ImporteBD * (iva.Valor / 100)).ToString(), FSNUser.NumberFormat) + " " + pedidoMon
                    tCell.Style.Add("border-right", "1px solid black")
                    tRow.Cells.Add(tCell)

                    tblAtributosDescuento.Rows.Add(tRow)


                    Dim dIva = oAtributo.ImporteBD * (iva.Valor / 100)

                    Dim oLabelIva As TextBox
                    oLabelIva = divIVAs.FindControl("lblIvahdd_" + iva.Id.ToString())
                    If Not IsNothing(oLabelIva) Then
                        Dim dIvaAcumuluado = oLabelIva.Text
                        dIva = dIva + dIvaAcumuluado
                        oLabelIva.Text = dIva.ToString()
                    Else
                        controlIVAs.InnerText = controlIVAs.InnerText + iva.Id.ToString() + ","
                        Dim TextBox1 As System.Web.UI.WebControls.TextBox
                        TextBox1 = New TextBox()
                        TextBox1.ID = "lblIvaDen_" + iva.Id.ToString()
                        TextBox1.Style("Visible") = "true"
                        TextBox1.Text = iva.Denominacion.ToString()
                        divIVAs.Controls.Add(TextBox1)

                        TextBox1 = New TextBox()
                        TextBox1.ID = "lblIvahdd_" + iva.Id.ToString()
                        TextBox1.Style("Visible") = "true"
                        TextBox1.Text = dIva.ToString()
                        divIVAs.Controls.Add(TextBox1)
                    End If

                Next
            Next


        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Pinto los atributos del pedido
    Public Sub CargarAtributosOrden(ByVal sIDOrden As String)
        Try

            Dim iIdOrden As Integer
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim cDetallePedidos As cDetallePedidos
            cDetallePedidos = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))

            Int32.TryParse(sIDOrden, iIdOrden)
            Dim oAtributos As CAtributosPedido
            oAtributos = cDetallePedidos.Cargar_Atributos(iIdOrden)
            If oAtributos.Count > 0 Then
                For Each oAtributo In oAtributos
                    Dim tRow As New HtmlTableRow()
                    Dim tCell As New HtmlTableCell()
                    tCell.InnerText = oAtributo.Id
                    tCell.Visible = False
                    tRow.Cells.Add(tCell)

                    tCell = New HtmlTableCell()
                    tCell.InnerHtml = oAtributo.Codigo.ToString()
                    tCell.Style.Add("border-right", "1px solid black")
                    tRow.Cells.Add(tCell)

                    tCell = New HtmlTableCell()
                    tCell.InnerHtml = oAtributo.Denominacion.ToString()
                    tCell.Style.Add("border-right", "1px solid black")
                    tRow.Cells.Add(tCell)

                    ModuloIdioma = TiposDeDatos.ModulosIdiomas.TraspasoAdjudicaciones
                    tCell = New HtmlTableCell()
                    If Not oAtributo.Valor Is Nothing Then
                        If oAtributo.Tipo = 1 Then
                            tCell.InnerHtml = oAtributo.Valor.ToString()
                        ElseIf oAtributo.Tipo = 2 Then
                            tCell.InnerHtml = FormatNumber(oAtributo.Valor, FSNUser.NumberFormat)
                        ElseIf oAtributo.Tipo = 3 Then
                            tCell.InnerHtml = FormatDate(oAtributo.Valor, FSNUser.DateFormat)
                        ElseIf oAtributo.Tipo = 4 Then
                            If oAtributo.Valor = "" OrElse oAtributo.Valor = 0 Then
                                tCell.InnerHtml = Textos(44)
                            Else
                                tCell.InnerHtml = Textos(43)
                            End If
                        End If
                    End If
                    ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
                    tCell.Style.Add("border-right", "1px solid black")
                    tRow.Cells.Add(tCell)
                    tblAtributosOrden.Rows.Add(tRow)
                Next
            Else
                OtrosDatos.Visible = False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Calculo el IVA de cada linea de pedido
    Public Sub CalcularIVALineas(ByVal idOrden As Integer, ByVal oLineas As CLineasPedido)
        Try
            Dim oLinea As CLineaPedido
            Dim dblImporteTotalLinea As Double
            Dim iConta As Integer = 0
            Dim oDetallePedidos As cDetallePedidos
            oDetallePedidos = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))
            oAtributosDescuentos = oDetallePedidos.Cargar_Descuentos(idOrden)
            For Each oLinea In oLineas
                Dim dblCostesDescuentosLinea = costesDescuentosLineas(iConta)
                Dim dblDescuentosLinea = descuentosLineas(iConta)
                Dim dblImporteCosteLinea As Double = 0
                Dim dblImporteBrutoLinea As Double = 0
                If oLinea.TipoRecepcion = 0 Then
                    dblImporteBrutoLinea = oLinea.CantPed * oLinea.PrecUc * cambio
                Else
                    dblImporteBrutoLinea = oLinea.ImportePedido * cambio
                End If
                dblImporteTotalLinea = dblImporteBrutoLinea + dblCostesDescuentosLinea
                Dim DescuentosCabeceraGeneral As Double = 0
                For Each oAtributoDescuento In oAtributosDescuentos
                    If (oAtributoDescuento.Operacion = "1") Then
                        Dim ImporteBruto As Double
                        ImporteBruto = dblImporteTotalLinea * (oAtributoDescuento.Valor / 100)
                        DescuentosCabeceraGeneral = DescuentosCabeceraGeneral + ImporteBruto
                    Else
                        DescuentosCabeceraGeneral = DescuentosCabeceraGeneral + (oAtributoDescuento.Valor * cambio) / oLineas.Count 'dblImporteTotal
                    End If
                Next
                dblImporteCosteLinea = dblImporteBrutoLinea - DescuentosCabeceraGeneral - dblDescuentosLinea
                'IVA Lineas
                For Each oImpuesto In oLinea.Impuestos
                    'IVA Lineas
                    Dim dIva = dblImporteCosteLinea * (oImpuesto.Valor / 100)
                    dblTotalIva = dblTotalIva + dIva

                    Dim oLabelIva As TextBox
                    oLabelIva = divIVAs.FindControl("lblIvahdd_" + oImpuesto.Id.ToString())
                    If Not IsNothing(oLabelIva) Then
                        Dim dIvaAcumuluado = oLabelIva.Text
                        dIva = dIva + dIvaAcumuluado
                        oLabelIva.Text = dIva.ToString()
                    Else
                        controlIVAs.InnerText = controlIVAs.InnerText + oImpuesto.Id.ToString() + ","
                        Dim TextBox1 As System.Web.UI.WebControls.TextBox
                        TextBox1 = New TextBox()
                        TextBox1.ID = "lblIvaDen_" + oImpuesto.Id.ToString()
                        TextBox1.Style("Visible") = "true"
                        TextBox1.Text = oImpuesto.Denominacion.ToString()
                        divIVAs.Controls.Add(TextBox1)

                        TextBox1 = New TextBox()
                        TextBox1.ID = "lblIvahdd_" + oImpuesto.Id.ToString()
                        TextBox1.Style("Visible") = "true"
                        TextBox1.Text = dIva.ToString()
                        divIVAs.Controls.Add(TextBox1)
                    End If
                Next
                iConta = iConta + 1
            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Pinto las lineas del pedido y sus datos
    Public Sub Cargar_Lineas(ByVal sIden As String)
        Try
            Dim iId As Integer
            Dim oAtributo As CAtributoPedido
            Dim oLinea As CLineaPedido
            Dim oDetallePedido As New CDetallePedido
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oDetallePedidos As cDetallePedidos
            Dim oLineas As CLineasPedido
            Dim dblImporteTotalLinea As Double
            Dim iInstanciaAprob As Long

            dblImporteTotal = 0

            oDetallePedidos = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))

            Int32.TryParse(sIden, iId)
            Int32.TryParse(InstanciaAprob, iInstanciaAprob)
            Dim PresDen(3) As String
            PresDen(0) = FSNServer.TipoAcceso.gbPres1Denominacion(FSNUser.Idioma.ToString)
            PresDen(1) = FSNServer.TipoAcceso.gbPres2Denominacion(FSNUser.Idioma.ToString)
            PresDen(2) = FSNServer.TipoAcceso.gbPres3Denominacion(FSNUser.Idioma.ToString)
            PresDen(3) = FSNServer.TipoAcceso.gbPres4Denominacion(FSNUser.Idioma.ToString)
            oLineas = oDetallePedidos.Cargar_Lineas(FSNUser.CodPersona, FSNUser.Idioma, iId, iInstanciaAprob, PresDen, FSNServer.TipoAcceso.gbBajaLOGPedidos)
            Dim iLinea As Integer = 0
            Dim tRow As New HtmlTableRow()
            Dim tCell As New HtmlTableCell()

            ReDim costesDescuentosLineas(oLineas.Count - 1)
            ReDim descuentosLineas(oLineas.Count - 1)

            For Each oLinea In oLineas
                iLinea = iLinea + 1
                dblImporteTotalLinea = 0
                dblCostesLinea = 0
                dblDescuentosLinea = 0
                tRow = New HtmlTableRow()

                tCell = New HtmlTableCell()
                tCell.InnerText = Format(oLinea.NumLinea, "000").ToString()
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)

                tCell = New HtmlTableCell()
                tCell.InnerText = IIf(oLinea.Item <> String.Empty, oLinea.Item + " - ", String.Empty) + oLinea.Den
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)

                tCell = New HtmlTableCell()
                If (oLinea.TipoRecepcion.ToString() = "0") Then
                    tCell.InnerText = FormatNumber(oLinea.PrecUc * cambio, FSNUser.NumberFormat)
                Else
                    tCell.InnerText = String.Empty
                End If
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)

                tCell = New HtmlTableCell()
                If (oLinea.TipoRecepcion = 0) Then
                    tCell.InnerText = FormatNumber(oLinea.CantPed, FSNUser.NumberFormat)
                Else
                    tCell.InnerText = FormatNumber(1, FSNUser.NumberFormat)
                End If
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)
                If oLinea.TipoRecepcion = 0 Then
                    dblImporteTotalLinea = oLinea.CantPed * oLinea.PrecUc * cambio
                Else
                    dblImporteTotalLinea = oLinea.ImportePedido * cambio
                End If

                tCell = New HtmlTableCell()
                If (oLinea.TipoRecepcion = 0) Then
                    tCell.InnerText = oLinea.UP
                Else
                    tCell.InnerText = String.Empty
                End If
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)

                'Atributos Lineas
                Dim tCellFilaAtr As New HtmlTableCell()
                Dim tRowFilaAtr As New HtmlTableRow()
                Dim oAtributos As CAtributosPedido
                oAtributos = oDetallePedidos.Cargar_Atributos_Linea(oLinea.ID)
                Dim tblAtributosLinea As New HtmlTable
                If oAtributos.Count > 0 Then
                    generarCabeceraAtrib(tblAtributosLinea)
                    For Each oAtributo In oAtributos
                        tCellFilaAtr.ColSpan = 13
                        generarFilaAtrib(tblAtributosLinea, oAtributo)
                    Next
                End If

                'Costes Lineas
                Dim oCostes As CAtributosPedido
                oCostes = oDetallePedidos.Cargar_Costes_Linea(oLinea.ID, FSNUser.Idioma)
                Dim tblAtributosCostes As New HtmlTable
                Dim dblCosteAtributo As Double
                If oCostes.Count > 0 Then
                    generarCabeceraCosteDescuento("C", tblAtributosCostes)
                    For Each oAtributo In oCostes
                        dblCosteAtributo = oAtributo.ImporteBD * cambio
                        dblCostesLinea = dblCostesLinea + (oAtributo.ImporteBD * cambio)
                        generarFilaCD("C", tblAtributosCostes, oAtributo, dblCosteAtributo)
                    Next
                End If

                tCell = New HtmlTableCell()
                tCell.InnerText = FormatNumber(dblCostesLinea, FSNUser.NumberFormat)
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)

                'Descuentos Lineas
                Dim oDescuentos As CAtributosPedido
                oDescuentos = oDetallePedidos.Cargar_Descuentos_Linea(oLinea.ID)
                Dim tblAtributosDescuentos As New HtmlTable
                If oDescuentos.Count > 0 Then
                    generarCabeceraCosteDescuento("D", tblAtributosDescuentos)
                    For Each oAtributo In oDescuentos
                        dblDescuentosLinea = dblDescuentosLinea + (oAtributo.ImporteBD * cambio)
                        generarFilaCD("D", tblAtributosDescuentos, oAtributo)
                    Next
                End If
                costesDescuentosLineas(iLinea - 1) = dblCostesLinea - dblDescuentosLinea
                descuentosLineas(iLinea - 1) = dblDescuentosLinea

                tCell = New HtmlTableCell()
                tCell.InnerText = FormatNumber(dblDescuentosLinea, FSNUser.NumberFormat)
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)

                tCell = New HtmlTableCell()
                tCell.InnerText = FormatNumber(dblImporteTotalLinea, FSNUser.NumberFormat)
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)

                Dim sCantidadPdte As String
                If oLinea.TipoRecepcion = 0 Then
                    sCantidadPdte = FSNLibrary.FormatNumber(oLinea.CantPed - oLinea.CantRec, FSNUser.NumberFormat)
                Else
                    sCantidadPdte = FSNLibrary.FormatNumber(0, FSNUser.NumberFormat)
                End If
                tCell = New HtmlTableCell()
                tCell.InnerText = FormatNumber(sCantidadPdte, FSNUser.NumberFormat)
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)

                Dim sImportePendiente As String
                sImportePendiente = FormatNumber(oLinea.ImportePendiente * cambio, FSNUser.NumberFormat)

                tCell = New HtmlTableCell()
                tCell.InnerText = sImportePendiente
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)

                ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
                Dim sTextoEstado As String = String.Empty
                If CType(Request.QueryString("estado"), CPedido.Estado) = 21 Then
                    sTextoEstado = Textos(39)
                Else
                    Select Case oLinea.Est
                        Case 0
                            sTextoEstado = Textos(46)
                        Case 1
                            If CType(Request.QueryString("estado"), CPedido.Estado) = 3 Then
                                sTextoEstado = Textos(100)
                            Else
                                If CType(Request.QueryString("estado"), CPedido.Estado) >= 2 Then
                                    sTextoEstado = Textos(92)
                                Else
                                    sTextoEstado = Textos(47)
                                End If
                            End If
                        Case 2
                            sTextoEstado = Textos(48)
                        Case 3
                            sTextoEstado = Textos(49)
                        Case 20
                            sTextoEstado = Textos(50)
                        Case 21
                            sTextoEstado = Textos(51)
                    End Select
                End If
                ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos

                tCell = New HtmlTableCell()
                tCell.InnerText = sTextoEstado
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)

                Dim sFechaEntrega As String = String.Empty
                If Not IsNothing(oLinea.FecEntrega) Then _
                sFechaEntrega = FSNLibrary.FormatDate(oLinea.FecEntrega, Me.Usuario.DateFormat) & _
                        IIf(CType(oLinea.EntregaObl, Boolean), " (*)", "")
                tCell = New HtmlTableCell()
                tCell.InnerText = sFechaEntrega
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)

                Dim sFechaEntregaProve As String = String.Empty
                If Not IsNothing(oLinea.FecEntregaProve) Then _
                    sFechaEntregaProve = FSNLibrary.FormatDate(oLinea.FecEntregaProve, Me.Usuario.DateFormat)
                tCell = New HtmlTableCell()
                tCell.InnerText = sFechaEntregaProve
                tCell.Style.Add("border-right", "1px solid black")
                tRow.Cells.Add(tCell)
                tRow.Style.Add("min-height", "22px")
                tRow.Style.Add("border-top", "1px solid black")
                tRow.Style.Add("border-bottom", "1px solid black")
                tRow.Attributes.Add("class", "FilaSeleccionada")
                tblLineas.Rows.Add(tRow)

                'Cargar Destinos Lineas
                Dim tablaDestinos As New HtmlTable
                generarDatosLineasDestino(oLinea, tablaDestinos)
                tCellFilaAtr.Controls.Add(tablaDestinos)
                tCellFilaAtr.Style.Add("text-align", "center")
                tCellFilaAtr.Style.Add("font-size", "12px")
                tCellFilaAtr.Style.Add("padding-bottom", "10px")
                tRowFilaAtr.Cells.Add(tCellFilaAtr)
                tblLineas.Rows.Add(tRowFilaAtr)

                'Cargar Centro Lineas
                Dim tablaCentro As New HtmlTable
                If FSNUser.AccesoSM Then
                    generarDatosLineasCentro(oLinea, tablaCentro)
                    tCellFilaAtr.Controls.Add(tablaCentro)
                    tCellFilaAtr.Style.Add("text-align", "center")
                    tCellFilaAtr.Style.Add("font-size", "12px")
                    tRowFilaAtr.Cells.Add(tCellFilaAtr)
                    tblLineas.Rows.Add(tRowFilaAtr)
                End If

                'Pintar Observaciones Linea
                Dim tablaObservaciones As New HtmlTable
                generarObservaciones(oLinea.Obs, tablaObservaciones)
                tCellFilaAtr.Controls.Add(tablaObservaciones)
                tCellFilaAtr.Style.Add("text-align", "center")
                tCellFilaAtr.Style.Add("font-size", "12px")
                tRowFilaAtr.Cells.Add(tCellFilaAtr)
                tblLineas.Rows.Add(tRowFilaAtr)

                dblImporteTotal = dblImporteTotal + dblImporteTotalLinea + dblCostesLinea - dblDescuentosLinea
                tCellFilaAtr.ColSpan = 13
                'Pintar Atributos Linea
                If oAtributos.Count > 0 Then
                    Dim hmlTexto As New HtmlTable
                    Dim trTexto As New HtmlTableRow
                    Dim tdTexto As New HtmlTableCell
                    tdTexto.InnerText = Textos(218)
                    trTexto.Cells.Add(tdTexto)
                    hmlTexto.Rows.Add(trTexto)
                    hmlTexto.Style.Add("font-size", "12px")
                    hmlTexto.Style.Add("border", "none")
                    hmlTexto.Style.Add("width", "95%")
                    hmlTexto.Style.Add("font-weight", "bold")
                    tCellFilaAtr.Controls.Add(hmlTexto)
                    tCellFilaAtr.Style.Add("text-align", "center")
                    tRowFilaAtr.Cells.Add(tCellFilaAtr)
                    tblLineas.Rows.Add(tRowFilaAtr)

                    tCellFilaAtr.Controls.Add(tblAtributosLinea)
                    tCellFilaAtr.Style.Add("text-align", "center")
                    tRowFilaAtr.Cells.Add(tCellFilaAtr)
                    tblLineas.Rows.Add(tRowFilaAtr)
                End If

                'Pintar adjuntos Linea
                If Not IsNothing(oLinea.Adjuntos) Then
                    Dim tablaAdjuntosLinea As New HtmlTable
                    If Cargar_Adjuntos_Lineas(tablaAdjuntosLinea, oLinea.ID) Then
                        tablaAdjuntosLinea.Style.Add("font-size", "12px")
                        tablaAdjuntosLinea.Style.Add("border", "none")
                        tablaAdjuntosLinea.Style.Add("width", "95%")
                        tCellFilaAtr.Controls.Add(tablaAdjuntosLinea)
                        tCellFilaAtr.Style.Add("text-align", "center")
                        tRowFilaAtr.Cells.Add(tCellFilaAtr)
                        tblLineas.Rows.Add(tRowFilaAtr)
                    End If
                End If

                'Pintar Planes de entrega
                If oLinea.ConPlanEntrega Then
                    Dim htmlPlanesEntrega As New HtmlTable
                    Dim trPlanesEntrega As New HtmlTableRow
                    Dim tdPlanesEntrega As New HtmlTableCell
                    tdPlanesEntrega.InnerText = Textos(320) + ": "
                    trPlanesEntrega.Cells.Add(tdPlanesEntrega)
                    htmlPlanesEntrega.Style.Add("font-weight", "bold")
                    htmlPlanesEntrega.Style.Add("width", "95%")
                    htmlPlanesEntrega.Style.Add("border", "none")
                    htmlPlanesEntrega.Style.Add("font-size", "12px")
                    htmlPlanesEntrega.Style.Add("margin-top", "1em")
                    htmlPlanesEntrega.Controls.Add(trPlanesEntrega)
                    tCellFilaAtr.Controls.Add(htmlPlanesEntrega)
                    tCellFilaAtr.Style.Add("text-align", "left")
                    tRowFilaAtr.Cells.Add(tCellFilaAtr)
                    tblLineas.Rows.Add(tRowFilaAtr)

                    htmlPlanesEntrega = New HtmlTable
                    trPlanesEntrega = New HtmlTableRow
                    tdPlanesEntrega = New HtmlTableCell

                    Dim htmlPlanEntrega As New HtmlTable
                    Dim trPlanEntrega As New HtmlTableRow
                    Dim tdPlanEntrega As New HtmlTableCell
                    htmlPlanEntrega.Style.Add("font-size", "12px")
                    htmlPlanEntrega.Style.Add("border", "0.1em solid black")
                    htmlPlanEntrega.Style.Add("width", "45%")
                    htmlPlanEntrega.Style.Add("border-collapse", "collapse")
                    tdPlanEntrega.InnerText = Textos(319)
                    tdPlanEntrega.Style.Add("width", "15%")
                    tdPlanEntrega.Style.Add("font-weight", "bold")
                    tdPlanEntrega.Style.Add("border", "0.1em solid black")
                    tdPlanEntrega.Style.Add("text-align", "center")
                    trPlanEntrega.Cells.Add(tdPlanEntrega)
                    tdPlanEntrega = New HtmlTableCell
                    tdPlanEntrega.InnerText = IIf(oLinea.TipoRecepcion = 0, Textos(11), Textos(12))
                    tdPlanEntrega.Style.Add("width", "10%")
                    tdPlanEntrega.Style.Add("font-weight", "bold")
                    tdPlanEntrega.Style.Add("border", "0.1em solid black")
                    tdPlanEntrega.Style.Add("text-align", "center")
                    trPlanEntrega.Cells.Add(tdPlanEntrega)
                    tdPlanEntrega = New HtmlTableCell
                    ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
                    tdPlanEntrega.InnerText = Textos(156)
                    ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
                    tdPlanEntrega.Style.Add("width", "20%")
                    tdPlanEntrega.Style.Add("font-weight", "bold")
                    tdPlanEntrega.Style.Add("border", "0.1em solid black")
                    tdPlanEntrega.Style.Add("text-align", "center")
                    trPlanEntrega.Cells.Add(tdPlanEntrega)
                    htmlPlanEntrega.Controls.Add(trPlanEntrega)
                    For Each iPlanEntrega As PlanEntrega In oLinea.PlanEntrega
                        trPlanEntrega = New HtmlTableRow
                        tdPlanEntrega = New HtmlTableCell
                        tdPlanEntrega.InnerText = iPlanEntrega.FechaEntrega
                        tdPlanEntrega.Style.Add("border-bottom", "0.1em solid black")
                        tdPlanEntrega.Style.Add("text-align", "center")
                        trPlanEntrega.Cells.Add(tdPlanEntrega)
                        tdPlanEntrega = New HtmlTableCell
                        tdPlanEntrega.InnerText = IIf(oLinea.TipoRecepcion = 0, iPlanEntrega.CantidadEntrega, iPlanEntrega.ImporteEntrega)
                        tdPlanEntrega.Style.Add("text-align", "right")
                        tdPlanEntrega.Style.Add("padding-right", "1em")
                        tdPlanEntrega.Style.Add("border-bottom", "0.1em solid black")
                        trPlanEntrega.Cells.Add(tdPlanEntrega)
                        tdPlanEntrega = New HtmlTableCell
                        tdPlanEntrega.InnerText = iPlanEntrega.Albaran
                        tdPlanEntrega.Style.Add("border-bottom", "0.1em solid black")
                        trPlanEntrega.Cells.Add(tdPlanEntrega)
                        htmlPlanEntrega.Controls.Add(trPlanEntrega)
                    Next

                    tdPlanesEntrega.Style.Add("text-align", "left")
                    tdPlanesEntrega.Controls.Add(htmlPlanEntrega)
                    trPlanesEntrega.Cells.Add(tdPlanesEntrega)
                    htmlPlanesEntrega.Style.Add("width", "95%")
                    htmlPlanesEntrega.Style.Add("border", "none")
                    htmlPlanesEntrega.Style.Add("margin", "0 auto")
                    htmlPlanesEntrega.Style.Add("font-size", "12px")
                    htmlPlanesEntrega.Controls.Add(trPlanesEntrega)

                    tCellFilaAtr.Controls.Add(htmlPlanesEntrega)
                    tRowFilaAtr.Cells.Add(tCellFilaAtr)
                    tblLineas.Rows.Add(tRowFilaAtr)
                End If

                'Pintar Costes Linea
                If oCostes.Count > 0 Then
                    Dim hmlTexto As New HtmlTable
                    Dim trTexto As New HtmlTableRow
                    Dim tdTexto As New HtmlTableCell
                    tdTexto.InnerText = Textos(207)
                    trTexto.Cells.Add(tdTexto)
                    hmlTexto.Rows.Add(trTexto)
                    hmlTexto.Style.Add("font-size", "12px")
                    hmlTexto.Style.Add("border", "none")
                    hmlTexto.Style.Add("width", "95%")
                    hmlTexto.Style.Add("font-weight", "bold")
                    tCellFilaAtr.Controls.Add(hmlTexto)
                    tCellFilaAtr.Style.Add("text-align", "center")
                    tRowFilaAtr.Cells.Add(tCellFilaAtr)
                    tblLineas.Rows.Add(tRowFilaAtr)

                    tCellFilaAtr.Controls.Add(tblAtributosCostes)
                    tCellFilaAtr.Style.Add("text-align", "center")
                    tRowFilaAtr.Cells.Add(tCellFilaAtr)
                    tblLineas.Rows.Add(tRowFilaAtr)
                End If

                'Pintar Descuentos Linea
                If oDescuentos.Count > 0 Then
                    Dim hmlTexto As New HtmlTable
                    Dim trTexto As New HtmlTableRow
                    Dim tdTexto As New HtmlTableCell
                    tdTexto.InnerText = Textos(208)
                    trTexto.Cells.Add(tdTexto)
                    hmlTexto.Rows.Add(trTexto)
                    hmlTexto.Style.Add("font-size", "12px")
                    hmlTexto.Style.Add("border", "none")
                    hmlTexto.Style.Add("width", "95%")
                    hmlTexto.Style.Add("font-weight", "bold")
                    tCellFilaAtr.Controls.Add(hmlTexto)
                    tCellFilaAtr.Style.Add("text-align", "center")
                    tRowFilaAtr.Cells.Add(tCellFilaAtr)
                    tblLineas.Rows.Add(tRowFilaAtr)

                    tCellFilaAtr.Controls.Add(tblAtributosDescuentos)
                    tCellFilaAtr.Style.Add("text-align", "center")
                    tRowFilaAtr.Cells.Add(tCellFilaAtr)
                    tblLineas.Rows.Add(tRowFilaAtr)
                End If
            Next
            CalcularIVALineas(iId, oLineas)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Pinto la informacion de centro y partida de las lineas
    Private Sub generarDatosLineasCentro(ByVal oLinea As CLineaPedido, ByRef htmlCentro As HtmlTable)
        Try
            Dim tRowFilaDatos As New HtmlTableRow()
            Dim tCellFilaDatos As New HtmlTableCell()
            tCellFilaDatos.InnerText = Textos(145)
            tCellFilaDatos.Style.Add("font-weight", "bold")
            tCellFilaDatos.Style.Add("width", "15%")
            tRowFilaDatos.Cells.Add(tCellFilaDatos)

            tCellFilaDatos = New HtmlTableCell()
            Dim sCentro As String
            If oLinea.Pres5_ImportesImputados_EP.Count > 0 Then
                If Not IsNothing(oLinea.Pres5_ImportesImputados_EP.Item(0).CentroCoste.UON4) Then
                    sCentro = oLinea.Pres5_ImportesImputados_EP.Item(0).CentroCoste.UON4 + " - " + oLinea.Pres5_ImportesImputados_EP.Item(0).CentroCoste.CentroSM.Denominacion
                ElseIf Not IsNothing(oLinea.Pres5_ImportesImputados_EP.Item(0).CentroCoste.UON3) Then
                    sCentro = oLinea.Pres5_ImportesImputados_EP.Item(0).CentroCoste.UON3 + " - " + oLinea.Pres5_ImportesImputados_EP.Item(0).CentroCoste.CentroSM.Denominacion
                ElseIf Not IsNothing(oLinea.Pres5_ImportesImputados_EP.Item(0).CentroCoste.UON2) Then
                    sCentro = oLinea.Pres5_ImportesImputados_EP.Item(0).CentroCoste.UON2 + " - " + oLinea.Pres5_ImportesImputados_EP.Item(0).CentroCoste.CentroSM.Denominacion
                ElseIf Not IsNothing(oLinea.Pres5_ImportesImputados_EP.Item(0).CentroCoste.UON1) Then
                    sCentro = oLinea.Pres5_ImportesImputados_EP.Item(0).CentroCoste.UON1 + " - " + oLinea.Pres5_ImportesImputados_EP.Item(0).CentroCoste.CentroSM.Denominacion
                Else
                    sCentro = String.Empty
                End If
                tCellFilaDatos.InnerText = sCentro

                tCellFilaDatos.Style.Add("width", "35%")
                tRowFilaDatos.Cells.Add(tCellFilaDatos)

                tCellFilaDatos = New HtmlTableCell()
                tCellFilaDatos.InnerText = oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.Denominacion + ": "
                tCellFilaDatos.Style.Add("font-weight", "bold")
                tCellFilaDatos.Style.Add("width", "15%")
                tRowFilaDatos.Cells.Add(tCellFilaDatos)

                tCellFilaDatos = New HtmlTableCell()
                Dim sPartida As String
                If Not IsNothing(oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.NIV4) Then
                    sPartida = oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.NIV4_Den + " (" + FormatDate(oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.FechaInicioPresupuesto.ToString(), FSNUser.DateFormat) + " - " + FormatDate(oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.FechaFinPresupuesto.ToString(), FSNUser.DateFormat) + ")"
                ElseIf Not IsNothing(oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.NIV3) Then
                    sPartida = oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.NIV3_Den + " (" + FormatDate(oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.FechaInicioPresupuesto.ToString(), FSNUser.DateFormat) + " - " + FormatDate(oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.FechaFinPresupuesto.ToString(), FSNUser.DateFormat) + ")"
                ElseIf Not IsNothing(oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.NIV2) Then
                    sPartida = oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.NIV2_Den + " (" + FormatDate(oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.FechaInicioPresupuesto.ToString(), FSNUser.DateFormat) + " - " + FormatDate(oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.FechaFinPresupuesto.ToString(), FSNUser.DateFormat) + ")"
                ElseIf Not IsNothing(oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.NIV1) Then
                    sPartida = oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.NIV1_Den + " (" + FormatDate(oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.FechaInicioPresupuesto.ToString(), FSNUser.DateFormat) + " - " + FormatDate(oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.FechaFinPresupuesto.ToString(), FSNUser.DateFormat) + ")"
                ElseIf Not IsNothing(oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.NIV0) Then
                    sPartida = oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.NIV0_Den + " (" + FormatDate(oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.FechaInicioPresupuesto.ToString(), FSNUser.DateFormat) + " - " + FormatDate(oLinea.Pres5_ImportesImputados_EP.Item(0).Partida.FechaFinPresupuesto.ToString(), FSNUser.DateFormat) + ")"
                Else
                    sPartida = String.Empty
                End If

                tCellFilaDatos.InnerText = sPartida
                tRowFilaDatos.Cells.Add(tCellFilaDatos)
                tRowFilaDatos.Style.Add("min-height", "22px")
                htmlCentro.Style.Add("font-size", "12px")
                htmlCentro.Style.Add("border", "none")
                htmlCentro.Style.Add("width", "95%")
                htmlCentro.Style.Add("margin", "0px auto")
                htmlCentro.Style.Add("text-align", "left")
                htmlCentro.Rows.Add(tRowFilaDatos)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Pinto las observaciones de la cabecera
    Private Sub generarObservaciones(ByVal sComentario As String, ByRef htmlOBservaciones As HtmlTable)
        Try
            Dim tRowFilaDatos As New HtmlTableRow()
            Dim tCellFilaDatos As New HtmlTableCell()
            tCellFilaDatos.InnerText = Textos(5) & " :"
            tCellFilaDatos.Style.Add("font-weight", "bold")
            tCellFilaDatos.Style.Add("width", "15%")
            tRowFilaDatos.Cells.Add(tCellFilaDatos)

            tCellFilaDatos = New HtmlTableCell()
            tCellFilaDatos.InnerText = sComentario

            tRowFilaDatos.Cells.Add(tCellFilaDatos)
            tRowFilaDatos.Style.Add("min-height", "22px")

            htmlOBservaciones.Style.Add("font-size", "12px")
            htmlOBservaciones.Style.Add("border", "none")
            htmlOBservaciones.Style.Add("width", "95%")
            htmlOBservaciones.Style.Add("margin", "0px auto")
            htmlOBservaciones.Style.Add("text-align", "left")
            htmlOBservaciones.Rows.Add(tRowFilaDatos)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Pinto la informacion de destino y almacen
    Private Sub generarDatosLineasDestino(ByVal oLinea As CLineaPedido, ByRef htmlDestino As HtmlTable)
        Try
            Dim tRowFilaDatos As New HtmlTableRow()
            Dim tCellFilaDatos As New HtmlTableCell()
            tCellFilaDatos.InnerText = Textos(13) + ": "
            tCellFilaDatos.Style.Add("font-weight", "bold")
            tCellFilaDatos.Style.Add("width", "15%")
            tRowFilaDatos.Cells.Add(tCellFilaDatos)

            Dim sDestino As String
            sDestino = oLinea.Dest.ToString() + " " + oLinea.DestDen
            Dim sAlmacen As String = String.Empty
            If oLinea.IdAlmacen <> 0 Then
                sAlmacen = oLinea.IdAlmacen.ToString() + " " + oLinea.Almacen
            End If
            tCellFilaDatos = New HtmlTableCell()
            tCellFilaDatos.InnerText = sDestino

            tCellFilaDatos.Style.Add("width", "35%")
            tRowFilaDatos.Cells.Add(tCellFilaDatos)

            If sAlmacen <> String.Empty Then
                tCellFilaDatos = New HtmlTableCell()
                tCellFilaDatos.InnerText = Textos(136) + ": "
                tCellFilaDatos.Style.Add("font-weight", "bold")
                tCellFilaDatos.Style.Add("width", "15%")
                tRowFilaDatos.Cells.Add(tCellFilaDatos)

                tCellFilaDatos = New HtmlTableCell()
                tCellFilaDatos.InnerText = sAlmacen
                tRowFilaDatos.Cells.Add(tCellFilaDatos)
            Else
                tCellFilaDatos = New HtmlTableCell()
                tCellFilaDatos.Style.Add("width", "50%")
                tRowFilaDatos.Cells.Add(tCellFilaDatos)
            End If
            tRowFilaDatos.Style.Add("min-height", "22px")

            htmlDestino.Style.Add("font-size", "12px")
            htmlDestino.Style.Add("border", "none")
            htmlDestino.Style.Add("width", "95%")
            htmlDestino.Style.Add("margin", "0px auto")
            htmlDestino.Style.Add("text-align", "left")
            htmlDestino.Rows.Add(tRowFilaDatos)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Pinto los atributos de las lineas de pedido
    Private Sub generarFilaAtrib(ByRef tblAtributosLinea As HtmlTable, ByVal oAtributo As CAtributoPedido)
        Try
            Dim tCell As New HtmlTableCell()
            Dim tRowAtr As New HtmlTableRow()
            tRowAtr = New HtmlTableRow()
            tCell = New HtmlTableCell()
            tCell.InnerText = oAtributo.Id
            tCell.Visible = False
            tRowAtr.Cells.Add(tCell)

            tCell = New HtmlTableCell()
            tCell.InnerHtml = oAtributo.Codigo.ToString()
            tCell.Style.Add("border-right", "1px solid black")
            tCell.Style.Add("border-left", "1px solid black")
            tRowAtr.Cells.Add(tCell)

            tCell = New HtmlTableCell()
            tCell.InnerHtml = oAtributo.Denominacion.ToString()
            tCell.Style.Add("border-right", "1px solid black")
            tRowAtr.Cells.Add(tCell)

            tCell = New HtmlTableCell()
            Dim sTexto As String = String.Empty

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.TraspasoAdjudicaciones
            If IsDBNull(oAtributo.Valor) Then
                sTexto = ""
            Else
                If oAtributo.Tipo = 1 Then
                    sTexto = oAtributo.Valor.ToString()
                ElseIf oAtributo.Tipo = 2 Then
                    sTexto = FormatNumber(oAtributo.Valor, FSNUser.NumberFormat)
                ElseIf oAtributo.Tipo = 3 Then
                    sTexto = FormatDate(oAtributo.Valor, FSNUser.DateFormat)
                ElseIf oAtributo.Tipo = 4 Then
                    If oAtributo.Valor.ToString = "" Then
                        sTexto = ""
                    Else
                        If oAtributo.Valor = 0 Then
                            sTexto = Textos(44)
                        Else
                            sTexto = Textos(43)
                        End If
                    End If
                End If
            End If


            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
            tCell.InnerHtml = sTexto
            tCell.Style.Add("border-right", "1px solid black")
            tRowAtr.Cells.Add(tCell)
            tRowAtr.Style.Add("min-height", "22px")
            tRowAtr.Style.Add("font-weight", "normal")
            tblAtributosLinea.Rows.Add(tRowAtr)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Pinto la cabecera de atributos de las lineas de pedido
    Private Sub generarCabeceraAtrib(ByRef tblAtributosLinea As HtmlTable)
        Try
            Dim tCell As New HtmlTableCell()
            Dim tRowAtr As New HtmlTableRow()
            tRowAtr = New HtmlTableRow()
            tCell = New HtmlTableCell()
            tCell.InnerText = String.Empty
            tCell.Visible = False
            tRowAtr.Cells.Add(tCell)

            tCell = New HtmlTableCell()
            tCell.InnerHtml = Textos(256)
            tCell.Style.Add("border", "1px solid black")
            tCell.Style.Add("width", "23%")
            tRowAtr.Cells.Add(tCell)

            tCell = New HtmlTableCell()
            tCell.InnerHtml = Textos(257)
            tCell.Style.Add("border", "1px solid black")
            tCell.Style.Add("width", "40%")
            tRowAtr.Cells.Add(tCell)

            tCell = New HtmlTableCell()
            tCell.InnerHtml = Textos(258)
            tCell.Style.Add("border", "1px solid black")
            tCell.Style.Add("width", "37%")
            tRowAtr.Cells.Add(tCell)
            tRowAtr.Style.Add("min-height", "22px")

            tblAtributosLinea.Style.Add("font-weight", "bold")
            tblAtributosLinea.Style.Add("font-size", "12px")
            tblAtributosLinea.Style.Add("border-collapse", "collapse")
            tblAtributosLinea.Style.Add("border", "1px solid black")
            tblAtributosLinea.Style.Add("width", "95%")
            tblAtributosLinea.Style.Add("margin", "0px auto")
            tblAtributosLinea.Style.Add("text-align", "left")
            tblAtributosLinea.Rows.Add(tRowAtr)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Pinto la cabecera de los costes y descuentos de las lineas de pedido
    Private Sub generarCabeceraCosteDescuento(ByVal sTipoAtrib As String, ByRef tblCDLinea As HtmlTable)
        Try
            Dim tCell As New HtmlTableCell()
            Dim tRowAtr As New HtmlTableRow()
            tRowAtr = New HtmlTableRow()
            tCell = New HtmlTableCell()
            tCell.InnerText = String.Empty
            tCell.Visible = False
            tRowAtr.Cells.Add(tCell)

            tCell = New HtmlTableCell()
            tCell.InnerHtml = Textos(256)
            tCell.Style.Add("border", "1px solid black")
            tCell.Style.Add("width", "25%")
            tRowAtr.Cells.Add(tCell)

            tCell = New HtmlTableCell()
            tCell.InnerHtml = Textos(257)
            tCell.Style.Add("border", "1px solid black")
            tCell.Style.Add("width", "38%")
            tRowAtr.Cells.Add(tCell)

            tCell = New HtmlTableCell()
            tCell.InnerHtml = Textos(258)
            tCell.Style.Add("border", "1px solid black")
            tCell.Style.Add("width", "15%")
            tRowAtr.Cells.Add(tCell)

            tCell = New HtmlTableCell()
            tCell.InnerHtml = Textos(12)
            tCell.Style.Add("border", "1px solid black")
            tCell.Style.Add("width", "18%")
            tRowAtr.Cells.Add(tCell)
            tRowAtr.Style.Add("min-height", "22px")

            tblCDLinea.Style.Add("font-weight", "bold")
            tblCDLinea.Style.Add("font-size", "12px")
            tblCDLinea.Style.Add("border-collapse", "collapse")
            tblCDLinea.Style.Add("border", "1px solid black")
            tblCDLinea.Style.Add("width", "95%")
            tblCDLinea.Style.Add("margin", "0px auto")
            tblCDLinea.Style.Add("text-align", "left")
            tblCDLinea.Rows.Add(tRowAtr)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Pinto los costes / descuentos de las lineas de pedido
    Private Sub generarFilaCD(ByVal tipo As String, ByRef tblCDLinea As HtmlTable, ByVal oAtributo As CAtributoPedido, Optional ByVal dblCosteAtributo As Double = 0)
        Try
            Dim tCell As New HtmlTableCell()
            Dim tRowAtr As New HtmlTableRow()
            tRowAtr = New HtmlTableRow()
            tCell = New HtmlTableCell()
            tCell.InnerText = oAtributo.Id
            tCell.Visible = False
            tRowAtr.Cells.Add(tCell)

            tCell = New HtmlTableCell()
            tCell.InnerHtml = oAtributo.Codigo.ToString()
            tCell.Style.Add("border-right", "1px solid black")
            tRowAtr.Cells.Add(tCell)

            tCell = New HtmlTableCell()
            tCell.InnerHtml = oAtributo.Denominacion.ToString()
            tCell.Style.Add("border-right", "1px solid black")
            tRowAtr.Cells.Add(tCell)

            tCell = New HtmlTableCell()
            If oAtributo.Operacion = "1" Then
                tCell.InnerText = FormatNumber(oAtributo.Valor, FSNUser.NumberFormat) + " %"
            Else
                tCell.InnerText = FormatNumber(oAtributo.ImporteBD * cambio, FSNUser.NumberFormat) + " " + pedidoMon
            End If
            tCell.Style.Add("border-right", "1px solid black")
            tRowAtr.Cells.Add(tCell)

            tCell = New HtmlTableCell()
            tCell.InnerText = FormatNumber(oAtributo.ImporteBD * cambio, FSNUser.NumberFormat) + " " + pedidoMon
            tCell.Style.Add("border-right", "1px solid black")
            tRowAtr.Cells.Add(tCell)
            tRowAtr.Style.Add("min-height", "22px")
            tRowAtr.Style.Add("font-weight", "normal")

            tblCDLinea.Rows.Add(tRowAtr)

            If tipo = "C" Then
                'IVA Costes Lineas
                For Each oImpuesto In oAtributo.Impuestos

                    Dim dIva = dblCosteAtributo * (oImpuesto.Valor / 100)
                    dblTotalIva = dblTotalIva + dIva


                    'iva lineas coste
                    Dim tRow As New HtmlTableRow()

                    tCell = New HtmlTableCell()
                    tCell.InnerText = String.Empty
                    tCell.Style.Add("border-right", "1px solid black")
                    tRow.Cells.Add(tCell)

                    tCell = New HtmlTableCell()
                    tCell.InnerText = oImpuesto.Denominacion
                    tCell.Style.Add("border-right", "1px solid black")
                    tRow.Cells.Add(tCell)

                    tCell = New HtmlTableCell()
                    tCell.InnerText = FormatNumber(oImpuesto.Valor.ToString(), FSNUser.NumberFormat) + " %"
                    tCell.Style.Add("border-right", "1px solid black")
                    tRow.Cells.Add(tCell)

                    tCell = New HtmlTableCell()
                    tCell.InnerText = FormatNumber(dIva, FSNUser.NumberFormat) + " " + pedidoMon
                    tCell.Style.Add("border-right", "1px solid black")
                    tRow.Cells.Add(tCell)
                    tRow.Style.Add("font-weight", "normal")

                    tblCDLinea.Rows.Add(tRow)

                    Dim oLabelIva As TextBox
                    oLabelIva = divIVAs.FindControl("lblIvahdd_" + oImpuesto.Id.ToString())
                    If Not IsNothing(oLabelIva) Then
                        Dim dIvaAcumuluado = oLabelIva.Text
                        dIva = dIva + dIvaAcumuluado
                        oLabelIva.Text = dIva.ToString()
                    Else
                        controlIVAs.InnerText = controlIVAs.InnerText + oImpuesto.Id.ToString() + ","
                        Dim TextBox1 As System.Web.UI.WebControls.TextBox
                        TextBox1 = New TextBox()
                        TextBox1.ID = "lblIvaDen_" + oImpuesto.Id.ToString()
                        TextBox1.Style("Visible") = "true"
                        TextBox1.Text = oImpuesto.Denominacion.ToString()
                        divIVAs.Controls.Add(TextBox1)

                        TextBox1 = New TextBox()
                        TextBox1.ID = "lblIvahdd_" + oImpuesto.Id.ToString()
                        TextBox1.Style("Visible") = "true"
                        TextBox1.Text = dIva.ToString()
                        divIVAs.Controls.Add(TextBox1)
                    End If
                Next
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Generamos el PDF
    Private Sub generarPDF()
        Try
            Dim oStringWriter As New System.IO.StringWriter
            Dim oHtmlTextWriter As New System.Web.UI.HtmlTextWriter(oStringWriter)

            oHtmlTextWriter.WriteLine("<HTML>")
            oHtmlTextWriter.WriteLine("<HEAD>")
            oHtmlTextWriter.WriteLine("<meta http-equiv=""Content-Type"" content=""text/html; charset=windows-1252"">")
            oHtmlTextWriter.WriteLine("<STYLE>@import url( " & ConfigurationManager.AppSettings("ruta") + "App_Themes/" & Me.Theme & "/estilos.css );</STYLE>")
            oHtmlTextWriter.WriteLine("<STYLE>@import url( " & ConfigurationManager.AppSettings("ruta") + "App_Themes/" & Me.Theme & "/cnStyles.css );</STYLE>")
            oHtmlTextWriter.WriteLine("<STYLE>@import url( " & ConfigurationManager.AppSettings("ruta") + "App_Themes/" & Me.Theme & "/Custom.css );</STYLE>")

            oHtmlTextWriter.WriteLine("</HEAD>")
            oHtmlTextWriter.WriteLine("<BODY>")

            Me.RenderControl(oHtmlTextWriter)
            oHtmlTextWriter.WriteLine("</BODY>")
            oHtmlTextWriter.WriteLine("</HTML>")

            '''''' Guardamos el HTML en un archivo
            Dim sPathBase As String
            Dim sPathHTML As String
            Dim sPathPDF As String
            Dim sCarpeta As String = String.Empty
            Dim sBytes As Long

            sPathBase = ConfigurationManager.AppSettings("temp") + "\" + GenerateRandomPath()
            If Not System.IO.Directory.Exists(sPathBase) Then System.IO.Directory.CreateDirectory(sPathBase)

            sPathHTML = sPathBase + "\" + lblPedido.Text.Replace("/", String.Empty) + ".html"
            sPathPDF = sPathBase + "\" + lblPedido.Text.Replace("/", String.Empty) + ".pdf"

            Dim oStreamWriter As New System.IO.StreamWriter(sPathHTML, False, System.Text.Encoding.UTF8)
            oStreamWriter.Write(oStringWriter.ToString)
            oStreamWriter.Close()

            ''''''' Pasamos el HTML a PDF
            ConvertHTMLToPDF(sPathHTML, sPathPDF)

            ' Ponemos el PDF a descargar
            Dim ofile As New System.IO.FileInfo(sPathPDF)
            sBytes = ofile.Length
            Dim arrPath2() As String = sPathPDF.Split("\")
            Response.Redirect("../_common/download.aspx?path=" + arrPath2(UBound(arrPath2) - 1) + "&nombre=" + Server.UrlEncode(arrPath2(UBound(arrPath2))) + "&datasize=" + CStr(sBytes))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class