﻿Imports Fullstep.FSNServer

Partial Public Class Seguimiento
    Inherits FSEPPage
    Private _sEstados(10) As String
    Private _sOtrosFiltros(6) As String
    Private dsLineasPedido As DataSet

    Private Enum Acciones
        AccionNula = 0
        NoAnularOrden = 1
        AnularOrden = 2
        AnularRecepcion = 3
        AnularLinea = 4
        BloquearFacturacionAlbaran = 5
        DesBloquearFacturacionAlbaran = 6
    End Enum
    Private Enum GestionCookiesFiltroBusqueda
        CargarDesdeCookie = 0
        GuardarEnCookie = 1
    End Enum
#Region "Propiedades"
    ''' <summary>
    ''' A través de esta propiedad devolvemos el total de registros. Se devuelve en generardsetPedidos sólo cuando se solicita la página 1. 
    ''' Esto implica suponer que siempre que se cambien las condiciones de filtro se vuelve a la página 1.
    '''		Valor 0: No hay registros.
    '''		Valor -1: Se está cambiando de página sobre un filtrado previo por lo que el valor no se devuelve y la aplicación puede 
    '''		seguir usando el valor que conserve en memoria.
    '''     Valor -2: Se ha producido un error al recuperar los datos.
    ''' </summary>
    Private Property TotalRegistros() As Integer
        Get
            If String.IsNullOrEmpty(ViewState.Item("TotalRegistros")) Then
                If Not IsPostBack Then
                    If Not Request("TotalRegistros") = Nothing AndAlso Request("TotalRegistros") <> String.Empty Then
                        ViewState.Item("TotalRegistros") = CType(Request("TotalRegistros"), Integer)
                        Return Request("TotalRegistros")
                    Else
                        Return 0
                    End If
                Else
                    Return 0
                End If

            Else
                Return ViewState.Item("TotalRegistros")
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState.Item("TotalRegistros") = value
        End Set
    End Property
    ''' <summary>
    ''' Número de registros por página
    ''' </summary>
    Public Property PageSize() As Integer
        Get
            If String.IsNullOrEmpty(ViewState.Item("PageSize")) Then
                Return CInt(System.Configuration.ConfigurationManager.AppSettings("PageSizeEP"))
            Else
                Return ViewState.Item("PageSize")
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState.Item("PageSize") = value
        End Set
    End Property
    Public Property PageNumber() As Integer
        Get
            If Me.ViewState("PageNumber") IsNot Nothing Then
                If IsPostBack Then
                    Return Convert.ToInt32(Me.ViewState("PageNumber"))
                Else
                    If Not Request("PaginaPaginacion") = Nothing AndAlso Request("PaginaPaginacion") <> String.Empty Then
                        'Se ha pulsado el boton volver en DetallePedido.aspx y cargara la pagina que tenia en Seguimiento en el momento de ir a la pagina de Detalle
                        Me.ViewState.Add("PageNumber", CInt(Request("PaginaPaginacion")))
                        Return Convert.ToInt32(Me.ViewState("PageNumber"))
                    Else
                        Return Convert.ToInt32(Me.ViewState("PageNumber"))
                    End If
                End If
            Else
                If IsPostBack Then
                    Me.ViewState.Add("PageNumber", 1)
                    Return 1
                Else
                    If Not Request("PaginaPaginacion") = Nothing AndAlso Request("PaginaPaginacion") <> String.Empty Then
                        'Se ha pulsado el boton volver en DetallePedido.aspx y cargara la pagina que tenia en Seguimiento en el momento de ir a la pagina de Detalle
                        Me.ViewState.Add("PageNumber", CInt(Request("PaginaPaginacion")))
                        Return Convert.ToInt32(Me.ViewState("PageNumber"))
                    Else
                        Me.ViewState.Add("PageNumber", 1)
                        Return 1
                    End If
                End If
            End If
        End Get

        Set(ByVal value As Integer)
            If Me.ViewState("PageNumber") IsNot Nothing Then
                Me.ViewState("PageNumber") = value
            Else
                Me.ViewState.Add("PageNumber", value)
            End If
        End Set
    End Property
#End Region
#Region "Inicio"
    ''' <summary>
    ''' Método que se ejecuta en el evento init de la página
    ''' </summary>
    ''' <param name="sender">la página</param>
    ''' <param name="e">argumentos del evento</param>
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'Añadimos el jsLimiteDecimales.js al script compuesto que controla el scriptmanager para una carga más rápida de los scripts
        Dim scriptRef As New System.Web.UI.ScriptReference("~/js/jsLimiteDecimales.js")
        ScriptMgr.CompositeScript.Scripts.Add(scriptRef)

        scriptRef = New System.Web.UI.ScriptReference("~/js/jsCalendario.js")
        ScriptMgr.CompositeScript.Scripts.Add(scriptRef)

        CargarScriptProveedor()
        'Cargamos los scripts q manejan el textbox txtProveedor y los paneles que usan JQuery
        CargarScriptBuscadoresJQuery()

        ScriptMgr.LoadScriptsBeforeUI = False
    End Sub
    ''' Revisado por: blp. Fecha: 14/11/2011
    ''' <summary>
    ''' Carga los textos de la página e inicializa los controles
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce cuando el control de servidor se carga en el objeto Page.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Sólo hacemos el Load si es la primera carga (no es postback) o no llega por una solicitud de Carga por ajax de fsnTvwCategorias (el treeview)
        If Not Me.IsPostBack OrElse Request.Form("__CALLBACKID") Is Nothing OrElse (Request.Form("__CALLBACKID") IsNot Nothing AndAlso Request.Form("__CALLBACKID").IndexOf("fsnTvwCategorias") < 0) Then
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
            Master.Seleccionar("Catalogo", "Seguimiento")
            Me.ScriptMgr.EnablePageMethods = True

            Dim lOldTimeOut As Long = Server.ScriptTimeout
            Server.ScriptTimeout = 1800

            SeleccionChecksSeguimiento()

            'Los combos de partidas se crean dinámicamente por lo que tienen que crearse siempre que la página se recarga
            Dim dsDropDownInfo As New DataSet
            dsDropDownInfo = getDropDownsInfo(Me.Usuario.CodPersona, Me.Usuario.Idioma)
            If Me.Acceso.gbAccesoFSSM Then
                CargarCombosPartidas(IIf(dsDropDownInfo.Tables.Count > 0, dsDropDownInfo, Nothing))
                imgPanelCentros.Attributes.Add("onclick", "abrirPanelCentros('" & mpeCentros.ClientID & "', '" & FiltroCentros.ClientID & "', event, 'tbCtroCoste', 'tbCtroCoste_Hidden')")
            Else
                litCentroCoste.Visible = False
                tbCtroCoste.Visible = False
                tbCtroCoste_Hidden.Visible = False
                imgPanelCentros.Visible = False
                phrPartidasPresBuscador.Visible = False
            End If

            chkBorrado.Visible = FSNUser.EPPermitirVerPedidosBorradosSeguimiento

            If Not IsPostBack() Then
                Session("filtroEstadoEP") = ""
                Session("filtroEstadoEP_PedidoAbierto") = ""
                Session("HayIntegracionEntradaoEntradaSalida") = False
                Dim tablas As Integer()
                ReDim tablas(1)
                tablas(0) = TablasIntegracion.PED_Aprov
                tablas(1) = TablasIntegracion.PED_directo
                Dim oEP_ValidacionesIntegracion As EP_ValidacionesIntegracion = Me.FSNServer.Get_Object(GetType(EP_ValidacionesIntegracion))
                If oEP_ValidacionesIntegracion.HayIntegracionEntradaoEntradaSalida(tablas) Then
                    Session("HayIntegracionEntradaoEntradaSalida") = True
                End If

                EliminarCachedePedidos()

                CargarTextos()
                Dim Cookie As HttpCookie
                Cookie = Request.Cookies("SEGUIMIENTO_CRIT_ORD")
                If Cookie Is Nothing OrElse String.IsNullOrEmpty(Cookie.Value) Then
                    CampoOrden = "FECHA"
                Else
                    CampoOrden = Cookie.Value
                    Cookie.Expires = DateTime.Now.AddDays(30)
                    Response.AppendCookie(Cookie)
                End If
                Cookie = Request.Cookies("SEGUIMIENTO_CRIT_DIREC")
                If Cookie Is Nothing OrElse String.IsNullOrEmpty(Cookie.Value) Then
                    SentidoOrdenacion = "DESC"
                Else
                    SentidoOrdenacion = Cookie.Value
                    Cookie.Expires = DateTime.Now.AddDays(30)
                    Response.AppendCookie(Cookie)
                End If

                Dim InicializarSoloCombosyFechas As Boolean = False
                InicializarControles(InicializarSoloCombosyFechas)
                gestionFiltrosBusqueda()
            End If

            dteDesde.Buttons.CustomButtonDisplay = Infragistics.Web.UI.EditorControls.ButtonDisplay.OnRight
            dteHasta.Buttons.CustomButtonDisplay = Infragistics.Web.UI.EditorControls.ButtonDisplay.OnRight
            dteFraFDesde.Buttons.CustomButtonDisplay = Infragistics.Web.UI.EditorControls.ButtonDisplay.OnRight
            dteFraFHasta.Buttons.CustomButtonDisplay = Infragistics.Web.UI.EditorControls.ButtonDisplay.OnRight
            dtePagoFDesde.Buttons.CustomButtonDisplay = Infragistics.Web.UI.EditorControls.ButtonDisplay.OnRight
            dtePagoFHasta.Buttons.CustomButtonDisplay = Infragistics.Web.UI.EditorControls.ButtonDisplay.OnRight

            'Cargamos los datos del grid en tres casos:
            '1. En el Load en la carga inicial (esto lo controlamos comprobando que el postback no es asíncrono dado que todos los postback en la página son asíncronos menos el de fsnTvwCategorias_ExpandState, postback que se controla al principio del Page_Load)
            '2. Cuando el postback lo efectua el control btnAceptarConfirmar, dado que puede que necesitemos esos datos para refrescar el grid, caso de tratarse de una anulación
            If Not ScriptMgr.IsInAsyncPostBack Then
                EliminarCachedePedidos()
                'El grid no es necesario actualizarlo salvo cuando el postback venga del btnAceptarConfirmar, que puede implicar cambios como anular una recepción y eso puede implicar cambio en el estado de la orden
                Dim bActualizarGrid As Boolean = (Not Request("PaginaPaginacion") = Nothing)
                If Not ScriptMgr.IsInAsyncPostBack Then
                    PageNumber = 1
                End If
                Dim bActualizarFiltros As Boolean = True
                recargarGrid(PageNumber, True, bActualizarGrid, bActualizarFiltros)
            End If

            ScriptsCliente()
            Server.ScriptTimeout = lOldTimeOut
        End If
    End Sub
    ''' <summary>
    ''' Genera el código de script de cliente
    ''' </summary>
    Private Sub ScriptsCliente()
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "UpdatePanelSetup") Then
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "UpdatePanelSetup", "var ModalProgress = '" & Master.Master.UpdateModal.ClientID & "';", True)
        End If
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "OrdenesCargadasHidden") Then
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "OrdenesCargadasHidden", "var hddOrdenesCargadasID='" & OrdenesCargadas.ClientID & "';", True)
        End If
    End Sub
    ''' Revisado por: blp. Fecha: 14/11/2011
    ''' <summary>
    ''' Carga los textos de la página en función del idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load. Max. 0,3 seg.</remarks>
    Private Sub CargarTextos()
        lblTitulo.Text = Textos(121)
        lblCabecera.Text = Textos(209) 'Criterios de búsqueda (pulse en esta sección para introducir los criterios de búsqueda)
        litAnio.Text = Textos(3)
        litCesta.Text = Textos(4)
        txtCesta_TextBoxWatermarkExtender.WatermarkText = Textos(4)
        litPedido.Text = Textos(5)
        txtPedido_TextBoxWatermarkExtender.WatermarkText = Textos(5)
        litPedidoProve.Text = Textos(73) & ":"
        txtPedidoProve_TextBoxWatermarkExtender.WatermarkText = Textos(5)
        If Acceso.gbUsarPedidosAbiertos Then
            litAnyoAbierto.Text = Textos(290)
            txtCestaAbierto_TextBoxWatermarkExtender.WatermarkText = Textos(4)
            txtPedidoPedAbierto_TextBoxWatermarkExtender.WatermarkText = Textos(5)
        Else
            trPedidosAbiertos.Visible = False
        End If
        If Acceso.gbOblCodPedido Then
            Dim oCParametros As FSNServer.CParametros = FSNServer.Get_Object(GetType(FSNServer.CParametros))
            litPedidoERP.Text = oCParametros.CargarLiteralParametros(TiposDeDatos.LiteralesParametros.PedidoERP, Usuario.Idioma) & ":"
            txtPedidoERP_TextBoxWatermarkExtender.WatermarkText = Textos(25)
        Else
            litPedidoERP.Visible = False
            txtPedidoERP.Visible = False
        End If
        litArticulo.Text = Textos(2)
        txtArticulo_TextBoxWatermarkExtender.WatermarkText = Textos(136)
        litDesde.Text = Textos(178)
        litHasta.Text = Textos(179)
        litEmpresa.Text = Textos(96) & ":"
        litReceptor.Text = Textos(97) & ":"
        litProveedor.Text = Textos(18)

        If Acceso.gbTrasladoAProvERP And HayIntegracionPedidos Then
            litProvedorERP.Text = Textos(98) & ":"
            txtProveedorERP_TextBoxWatermarkExtender.WatermarkText = Textos(98)
        Else
            litProvedorERP.Visible = False
            txtProveedorERP.Visible = False
        End If

        LnkBtnCategoria.Text = Textos(105)
        btnBuscar.Text = Textos(131)
        btnLimpiar.Text = Textos(213) 'Limpiar

        lblTituloAdjuntosLin.Text = Textos(137)
        lblTituloAdjuntos.Text = Textos(138)
        litNoPedidos.Text = "<p class=""EtiquetaGrande""> &nbsp; &nbsp; " & Textos(54) & "</p>"

        litCentroCoste.Text = Textos(142) & ": "
        litBusFras.Text = Textos(146)
        litBusAnioFra.Text = Textos(23)
        txtAnioFra_TextBoxWatermarkExtender.WatermarkText = Textos(23)
        litBusNumFra.Text = Textos(154)
        txtNumFra_TextBoxWatermarkExtender.WatermarkText = Textos(154)
        litBusEstadoFra.Text = Textos(145)
        litBusFraFDesde.Text = Textos(152) & " " & Textos(103)
        litBusFraFHasta.Text = Textos(152) & " " & Textos(104)
        litBusPagos.Text = Textos(149)
        litBusNumPago.Text = Textos(150)
        txtNumPago_TextBoxWatermarkExtender.WatermarkText = Textos(150)
        litBusEstadoPago.Text = Textos(151)
        litBusPagFDesde.Text = Textos(153) & ". " & Textos(103)
        litBusPagFHasta.Text = Textos(153) & ". " & Textos(104)
        litTipoPedido.Text = Textos(175) & ": "
        litNumMesesIni.Text = Textos(214) & " " 'Cargar Pedidos de 
        litNumMesesFin.Text = " " & Textos(215) 'ultimos meses        
        btnAceptarConfirmRecep.Text = Textos(162)
        btnCancelarConfirmRecep.Text = Textos(163)
        chkSeguimientoPedidos.Items(0).Text = Textos(184) '"Ver sólo mis pedidos"
        chkSeguimientoPedidos.Items(1).Text = Textos(185) '"Ver todos los pedidos de mis centros de coste"
        chkPedidosBloqueados.Text = Textos(237) 'Ver pedidos con albaranes bloqueados        
        LitGestor.Text = Textos(241) & ": " 'Gestor: 
        lblCentros.Text = Textos(264) & ": " & Textos(142) 'Selección de: Centro de coste
        lblFiltroCentros.Text = Textos(142) & ": " 'Centro de coste:
        chkBorrado.Text = Textos(294)  'Borrado
    End Sub
    ''' Revisado por: blp. Fecha: 14/11/2011
    ''' <summary>
    ''' Procedimiento que Inicializa los controles en la primera carga de la página
    ''' </summary>
    ''' <param name="InicializarSoloCombosyFechas">Parámetro Booleano que nos indica si, dentro de los controles que inicializa esta función (casi todos los de la página) 
    '''                          queremos inicializar sólo los combos y fechas. 
    '''                          Este parámetro se ha añadido porque cuando se modifica la combinación de checkboxes "Ver mis pedidos" y "Ver pedidos de otros usuarios" (control chkSeguimientoPedidos())
    '''                          hay que reiniciar estos combos para que incluyan valores que anteriormente no incluían o para que dejen de mostrarlos, según el caso.
    '''                          Por ello, en la carga de la página (Page_Load) el parámetro se pasa a False y al efectuar una búsqueda se para a True.
    ''' </param>
    ''' <remarks>Llamado desde Page_Load
    ''' Tiempo máximo:</remarks>
    Private Sub InicializarControles(ByVal InicializarSoloCombosyFechas As Boolean)
        If Not InicializarSoloCombosyFechas Then
            If Not Acceso.gbVerNumeroProveedor Then
                TablaPrimera.Rows(0).Cells(7).Visible = False
                TablaPrimera.Rows(0).Cells(8).Visible = False
            End If
            pnlCabeceraRecepcion.Visible = True
            pnlBusqueda.Visible = True

            ' Checks estados y otros
            chklstEstado.Items.Clear()
            chklstOtrosFiltros.Items.Clear()
            RellenarTextosCheckboxLists()
            chklstEstado.Items.AddRange(Estados.ToArray)
            chklstOtrosFiltros.Items.AddRange(ClasesDePedido.ToArray)
            If Not Me.Acceso.gbPedidoLibre Then
                chklstOtrosFiltros.Items.FindByValue(CPedido.OtrosAtributosPedido.CatalogadoLibres).Attributes.Add("style", "display:none;")
            End If
            inicializarCheckboxLists()
            txtArticulo_AutoCompleteExtender.ContextKey = Me.Usuario.CodPersona
            CargarArbolCategorias()
            'Valor inicial del control.
            igNumMeses.Value = 1
        End If

        If Me.Acceso.g_bAccesoFSFA Then
            fldsetPedidosBloqueados.Visible = True
            chkPedidosBloqueados.Visible = True
            fldsetIMPCC.Style.Add("float", "left")
        End If

        Dim dsDropDownInfo As New DataSet
        dsDropDownInfo = getDropDownsInfo(Me.Usuario.CodPersona, Me.Usuario.Idioma)
        Dim lista As New List(Of ListItem)
        'DropDown Años
        lista = Anyos().ToList
        If lista.Count > 0 Then _
         lista.Insert(0, New ListItem(String.Empty, String.Empty))
        ddlAnio.DataSource = lista
        ddlAnio.DataBind()

        ddlAnyoAbierto.DataSource = lista
        ddlAnyoAbierto.DataBind()
        'DropDown Empresa
        lista = Empresas(IIf(dsDropDownInfo.Tables.Count > 0, dsDropDownInfo, Nothing)).ToList
        If lista.Count > 0 Then _
         lista.Insert(0, New ListItem(String.Empty, String.Empty))
        ddlEmpresa.DataSource = lista
        ddlEmpresa.DataBind()
        'DropDown Receptor
        lista = Receptores(IIf(dsDropDownInfo.Tables.Count > 0, dsDropDownInfo, Nothing)).ToList
        If lista.Count > 0 Then _
         lista.Insert(0, New ListItem(String.Empty, String.Empty))
        ddlReceptor.DataSource = lista
        ddlReceptor.DataBind()
        If Me.Acceso.g_bAccesoFSFA Then
            'DropDown Estado Fras
            lista = EstadosFra().ToList
            If lista.Count > 0 Then _
             lista.Insert(0, New ListItem(String.Empty, String.Empty))
            ddlEstadoFra.DataSource = lista
            ddlEstadoFra.DataBind()
            'DropDown Estado Pagos
            lista = EstadosPago().ToList
            If lista.Count > 0 Then _
             lista.Insert(0, New ListItem(String.Empty, String.Empty))
            ddlEstadoPago.DataSource = lista
            ddlEstadoPago.DataBind()
        Else
            buscarFrasPagos.Visible = False
        End If

        'DropDown Tipo de pedido
        lista = ListaTiposPedidos().ToList
        If lista.Count > 0 Then _
         lista.Insert(0, New ListItem(String.Empty, String.Empty))
        ddlTipoPedido.DataSource = lista
        ddlTipoPedido.DataBind()

        'DropDown Gestor
        'Gestor: Visible sólo si hay SM
        If Not Me.Acceso.gbAccesoFSSM Then
            LitGestor.Visible = False
            ddlGestor.Visible = False
            celdaLitGestor.Visible = False
            celdaDdlGestor.Visible = False
            celdaProveedorERP.ColumnSpan = 3
        Else
            celdaLitGestor.Style.Add("padding-left", "5px")
            Dim dtGestores As DataTable = ListaGestores(IIf(dsDropDownInfo.Tables.Count > 0, dsDropDownInfo, Nothing))
            If dtGestores.Rows.Count > 0 AndAlso dtGestores.Rows(0).Item("GESTORDEN") <> String.Empty Then
                Dim oRow As DataRow = dtGestores.NewRow
                oRow("GESTORCOD") = String.Empty
                oRow("GESTORDEN") = String.Empty
                dtGestores.Rows.InsertAt(oRow, 0)
            End If
            ddlGestor.DataSource = dtGestores
            ddlGestor.DataTextField = dtGestores.Columns(1).ColumnName
            ddlGestor.DataValueField = dtGestores.Columns(0).ColumnName
            ddlGestor.DataBind()
        End If

        SeleccionarFiltrosEnPanelBusqueda()

        tbCtroCoste.Attributes.Add("onKeyDown", "abrirPanelCentros('" & mpeCentros.ClientID & "','" & FiltroCentros.ClientID & "', event, '" & tbCtroCoste.ClientID & "', '" & tbCtroCoste_Hidden.ClientID & "');")
    End Sub
#End Region
#Region "PartidasPresupuestarias"
    ''' Revisado por: blp. Fecha: 18/04/2012
    ''' <summary>
    ''' Función que nos devuelve un Datatable con las configuraciones de cada partida presupuestaria
    ''' </summary>
    ''' <param name="dsInfo">
    ''' Podemos pasarle el dataset desde el que recupera los datos o dejar que los tome del DsetPedidos
    ''' </param>
    ''' <returns>Datatable con las configuraciones de cada partida presupuestaria</returns>
    ''' <remarks>Llamado desde CargarCombosPartidas
    ''' Tiempo máximo: 0 sec</remarks>
    Private Function CargarPartidasPresupuestarias(Optional ByVal dsInfo As DataSet = Nothing) As DataTable
        Dim query = From Datos In dsInfo.Tables("PARTIDASPRES0").AsEnumerable()
                    Where (Not IsDBNull(Datos.Item("PRES5")))
                    Order By Datos.Item("PARTIDA_DEN")
                    Select Datos!PRES5, Datos!NIVEL, Datos!PARTIDA_DEN
                    Distinct

        Dim PartidasPres = New DataTable("PartidasPres")
        PartidasPres.Columns.Add("PRES0", GetType(String))
        PartidasPres.Columns.Add("NIVEL", GetType(Integer))
        PartidasPres.Columns.Add("PARTIDA_DEN", GetType(String))
        For Each partida In query
            PartidasPres.Rows.Add(New Object() {partida.PRES5, partida.NIVEL, partida.PARTIDA_DEN})
        Next
        Return PartidasPres
    End Function
    ''' Revisado por: blp. Fecha:18/04/2012
    ''' <summary>
    ''' Procedimiento que crea tantos combos en el buscador como partidas presupuestarias se usen
    ''' Y rellena cada uno de los combos con las partidas que existan en las líneas de pedido de los pedidos emitidos por el usuario
    ''' <param name="dsInfo">
    ''' Podemos pasarle el dataset desde es que recupera los datos o dejar que los tome del DsetPedidos
    ''' </param>
    ''' </summary>
    ''' <remarks>Llamado desde Page_Load
    ''' Tiempo máximo: 0 sec</remarks>
    Private Sub CargarCombosPartidas(Optional ByVal dsInfo As DataSet = Nothing)
        Dim dtPartidasPres As DataTable

        If dsInfo Is Nothing OrElse dsInfo.Tables.Count = 0 Then
            dsInfo = getDropDownsInfo(FSNUser.CodPersona, FSNUser.Idioma)
        End If

        '1. coger las partidas (pres0) que hay en la caché (o llamada a la función) de las combos
        dtPartidasPres = CargarPartidasPresupuestarias(dsInfo)
        '2. Para cada partida, crear un label con el texto del punto 1, un textbox readonly y un botón para abrir el panel
        If dtPartidasPres.Rows.Count > 0 Then
            Dim TablaAsp As New Table
            Dim FilaAsp As New TableRow
            Dim Celda1Asp As New TableCell
            Dim Celda2Asp As New TableCell
            For Each PartidaPres As DataRow In dtPartidasPres.Rows
                If phrPartidasPresBuscador.FindControl("tbDdlPP") Is Nothing _
                OrElse phrPartidasPresBuscador.FindControl("tbDdlPP").FindControl("trPPres_" & PartidaPres("PRES0")) Is Nothing Then
                    'Inserta una serie de controles para cada una de las partidas dentro del control PlaceHolder->phrPartidasPresBuscador
                    Celda1Asp = New TableCell
                    Celda2Asp = New TableCell
                    FilaAsp = New TableRow
                    Celda1Asp.Style.Add("padding-bottom", "3px")
                    Celda1Asp.Width = Unit.Pixel(140)
                    Celda2Asp.Width = Unit.Pixel(350)
                    Celda1Asp.HorizontalAlign = HorizontalAlign.Left
                    Celda1Asp.Style.Add("text-align", "left")
                    Celda2Asp.HorizontalAlign = HorizontalAlign.Right

                    Dim tbPartida As New TextBox
                    Dim tbPartidaHidden As New HiddenField
                    Dim litPartida As New Literal()
                    Dim imgPartida As New HtmlControls.HtmlImage
                    litPartida.Text = PartidaPres("PARTIDA_DEN")
                    tbPartida.ID = "tbPPres_" & PartidaPres("PRES0")
                    tbPartida.Style.Add("float", "left")
                    tbPartida.Style.Add("width", "90%")
                    tbPartida.ClientIDMode = UI.ClientIDMode.Static
                    tbPartidaHidden.ID = "tbPPres_Hidden_" & PartidaPres("PRES0")
                    tbPartida.Attributes.Add("onKeyDown", "abrirPanelPartidas('" & mpePartidas.ClientID & "', '" & listadoPartidas.ClientID & "', '" & PartidaPres("PRES0") & "', '" & PartidaPres("PARTIDA_DEN") & "', '" & lblPartidaPres0.ClientID & "', '" & lblFiltroPartida.ClientID & "', '" & Textos(264) & "', '" & listadoInputsFiltro.ClientID & "', event, '" & tbPartida.ClientID & "', '" & tbPartidaHidden.ClientID & "');")
                    tbPartidaHidden.ClientIDMode = UI.ClientIDMode.Static
                    imgPartida.ID = "imgPPres_" & PartidaPres("PRES0")
                    imgPartida.Src = "../../Images/abrir_ptos_susp.gif"
                    imgPartida.Style.Add("border", "0px")
                    imgPartida.Attributes.Add("onClick", "javascript:abrirPanelPartidas('" & mpePartidas.ClientID & "', '" & listadoPartidas.ClientID & "', '" & PartidaPres("PRES0") & "', '" & PartidaPres("PARTIDA_DEN") & "', '" & lblPartidaPres0.ClientID & "', '" & lblFiltroPartida.ClientID & "', '" & Textos(264) & "', '" & listadoInputsFiltro.ClientID & "', event, null, null);")
                    imgPartida.Style.Add("cursor", "pointer")
                    imgPartida.Style.Add("float", "right")
                    tbPartida.Width = Unit.Pixel(ddlReceptor.Width.Value - 50)
                    tbPartida.CssClass = "Normal"

                    Celda1Asp.Controls.Add(litPartida)
                    Celda2Asp.Controls.Add(tbPartida)
                    Celda2Asp.Controls.Add(tbPartidaHidden)
                    Celda2Asp.Controls.Add(imgPartida)
                    FilaAsp.ID = "trPPres_" & PartidaPres("PRES0")
                    FilaAsp.Cells.Add(Celda1Asp)
                    FilaAsp.Cells.Add(Celda2Asp)
                    FilaAsp.Width = Fila5Tabla1.Width
                    FilaAsp.Style.Add("padding-bottom", "3px")
                    TablaAsp.Rows.Add(FilaAsp)
                End If
            Next
            If phrPartidasPresBuscador.FindControl("tbDdlPP") Is Nothing Then
                TablaAsp.Width = Fila5Tabla1.Width
                TablaAsp.CellPadding = 2
                TablaAsp.CellSpacing = 0
                TablaAsp.ID = "tbDdlPP"
                TablaAsp.Width = Unit.Percentage("100")
                TablaAsp.ClientIDMode = UI.ClientIDMode.Static
                phrPartidasPresBuscador.Controls.Add(TablaAsp)
            End If
        End If
        '3. Añadimos al scriptmanager de la página el js que lanzará la carga asíncrona de las partidas desde cliente
        Dim oScriptPartidas As HtmlGenericControl
        If dtPartidasPres.Rows.Count > 0 Then
            oScriptPartidas = CargarScriptBusquedaJQuery(TiposDeDatos.ControlesBusquedaJQuery.PartidaPres, listadoPartidas.ClientID, listadoInputsFiltro.ClientID, TiposDeDatos.Aplicaciones.EP)
        End If
        'Cuando se devuelve un script "script" hay que insertar en la página el script q se recibe
        If oScriptPartidas IsNot Nothing AndAlso oScriptPartidas.TagName = "script" Then
            RegistrarScript(oScriptPartidas, "ScriptPartidas")
        End If
        Dim oScriptCentros As HtmlGenericControl
        oScriptCentros = CargarScriptBusquedaJQuery(TiposDeDatos.ControlesBusquedaJQuery.CentroCoste, listadoCentros.ClientID, FiltroCentros.ClientID, TiposDeDatos.Aplicaciones.EP)
        'Cuando se devuelve un script "script" hay que insertar en la página el script q se recibe
        If oScriptCentros IsNot Nothing AndAlso oScriptCentros.TagName = "script" Then
            RegistrarScript(oScriptCentros, "ScriptCentros")
        End If
    End Sub
    Public Structure partidaPedido
        Dim PRES0 As String
        Dim NivelImp As String
    End Structure
#End Region
#Region "Arbol Categorias"
    ''' <summary>
    ''' Propiedad que nos devuelve un dataset con una tabla y todas las categorías
    ''' </summary>
    ''' <remarks>Propiedad que nos devuelve un dataset con una tabla y todas las categorías
    ''' Tiempo máximo: 0 sec</remarks>
    Private ReadOnly Property TodasCategorias() As DataSet
        Get
            Dim ds As DataSet
            If HttpContext.Current.Cache("TodasCategorias") Is Nothing Then
                Dim oCCategorias As FSNServer.CCategorias = FSNServer.Get_Object(GetType(FSNServer.CCategorias))
                ds = oCCategorias.CargarArbolCategorias("")
                Me.InsertarEnCache("TodasCategorias", ds)
            Else
                ds = HttpContext.Current.Cache("TodasCategorias")
            End If
            Dim key() As DataColumn = {ds.Tables(0).Columns("ID")}
            ds.Tables(0).PrimaryKey = key
            Return ds
        End Get
    End Property
    ''' Revisado por: blp. Fecha: 18/04/2012
    ''' <summary>
    ''' Procedimiento en el que cargaremos los valores para el arbol de categorías
    ''' En el resto del datos presentes en los filtros se muestra sólo la info que hay en los pedidos del usuario y/o de los centros de coste a los que puede imputar 
    ''' pero en este caso mostramos todos
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: El evento Load de la página
    ''' Tiempo máximo: 0 sec.</remarks>
    Private Sub CargarArbolCategorias()
        If TodasCategorias.Tables(0) IsNot Nothing AndAlso TodasCategorias.Tables(0).Rows.Count > 0 Then
            fsnTvwCategorias.Nodes(0).Text = Textos(105) 'Categoria

            fsnTvwCategorias.NodeStyle.Height = "20"
        Else
            fsnTvwCategorias.Nodes.Clear()
        End If
    End Sub
    ''' <summary>
    ''' Evento que se lanzará al seleccionar un nodo del árbol de categorías, mostrando la categoría seleccionada en el panel de búsqueda
    ''' </summary>
    ''' <param name="sender">el objecto que llama al evento (el arbol)</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el propio control
    ''' Tiempo máximo: 0 seg.</remarks>
    Private Sub fsntvwCategorias_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fsnTvwCategorias.SelectedNodeChanged
        If fsnTvwCategorias.SelectedNode Is Nothing Then
            litCategoria.Text = ""
            litCategoria.Visible = False
            lnkQuitarCategoria.CommandArgument = ""
            lnkQuitarCategoria.Visible = False
        Else
            If fsnTvwCategorias.SelectedNode.Value <> "_0_" Then
                litCategoria.Text = fsnTvwCategorias.SelectedNode.Text
                litCategoria.Visible = True
                lnkQuitarCategoria.CommandArgument = fsnTvwCategorias.SelectedValue
                lnkQuitarCategoria.Visible = True
            End If
        End If
        mpePanelArbol.Hide()
        updpnlCategorias.Update()
    End Sub
    ''' Revisado por: blp. Fecha: 12/02/2013
    ''' <summary>
    ''' Carga de un nodo del treeview
    ''' </summary>
    ''' <param name="sender">Control que lanza el evento</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Evento TreeNodePopulate. Max. 0,1 seg.</remarks>
    Public Sub fsnTvwCategorias_TreeNodePopulate(sender As Object, e As TreeNodeEventArgs) Handles fsnTvwCategorias.TreeNodePopulate
        Dim query
        Select Case e.Node.Value
            Case "_0_"
                query = From datos In TodasCategorias.Tables(0)
                        Where datos("PADRE") Is System.DBNull.Value
                        Select datos
            Case Else
                query = From datos In TodasCategorias.Tables(0)
                        Where datos("PADRE") IsNot System.DBNull.Value AndAlso datos("PADRE") = e.Node.Value
                        Select datos
        End Select

        If CType(query, System.Data.EnumerableRowCollection(Of System.Data.DataRow)).Any Then
            For Each oDatosNodo As System.Data.DataRow In CType(query, System.Data.EnumerableRowCollection(Of System.Data.DataRow))
                Dim nodo As New TreeNode
                nodo.Text = oDatosNodo.Item("DEN")
                nodo.Value = oDatosNodo.Item("ID")
                nodo.Expanded = False
                nodo.PopulateOnDemand = True
                nodo.SelectAction = TreeNodeSelectAction.Select
                e.Node.ChildNodes.Add(nodo)
            Next
        End If
    End Sub
    ''' Revisado por: blp. Fecha: 09/01/2013
    ''' <summary>
    ''' Evento que se lanzará al quitar la categoría seleccionada pulsando en el aspa y que eliminará el criterio de búsqueda
    ''' </summary>
    ''' <param name="sender">el objecto que llama al evento (el arbol)</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el propio control
    ''' Tiempo máximo: 0 seg.</remarks>
    Private Sub lnkQuitarCategoria_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkQuitarCategoria.Click
        fsnTvwCategorias.SelectedNode.Selected = False
        litCategoria.Text = ""
        litCategoria.Visible = False
        lnkQuitarCategoria.CommandArgument = ""
        lnkQuitarCategoria.Visible = False
        pnlCategorias.Update()
    End Sub
#End Region
#Region "Consultas"
    ''' <summary>
    ''' Propiedad que nos devolverá los albaranes de una orden, bien de la caché, bien de BBDD 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks>Llamada desde todos los sitios donde se use la propiedad
    ''' Tiempo máximo: </remarks>
    Public ReadOnly Property DsetAlbaranes(ByVal IdOrden As Integer) As DataSet
        Get

            If HttpContext.Current.Cache("DsetAlbaranes_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString() & "_" & IdOrden.ToString()) Is Nothing Then

                Return ActualizarDsetAlbaranes(IdOrden)

            Else
                Dim dsAlbaranes As New DataSet
                If Not HttpContext.Current.Cache("DsetAlbaranes_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString() & "_" & IdOrden.ToString()) Is Nothing Then
                    dsAlbaranes = HttpContext.Current.Cache("DsetAlbaranes_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString() & "_" & IdOrden.ToString())
                ElseIf HttpContext.Current.Cache("DsetAlbaranes_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString() & "_" & IdOrden.ToString()) Is Nothing Then

                    Return ActualizarDsetAlbaranes(IdOrden)
                End If

                Return dsAlbaranes
            End If

        End Get
    End Property
    ''' <summary>
    ''' Función que nos devolverá un dataset con TODAS los albaranes de una orden de pedido
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Llamada desde la propiedad DsetAlbaranes
    ''' Tiempo máximo: 2 sec</remarks>
    Private Function ActualizarDsetAlbaranes(ByVal IdOrden As Integer) As DataSet
        Dim dsAlbaranes As New DataSet

        If HttpContext.Current.Cache("DsetAlbaranes_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString() & "_" & IdOrden.ToString()) Is Nothing Then
            Dim oRecepciones As CRecepciones = Me.FSNServer.Get_Object(GetType(FSNServer.CRecepciones))
            dsAlbaranes = oRecepciones.CargarAlbaranesOrden(IdOrden, Me.Usuario.Idioma)
            Me.InsertarEnCache("DsetAlbaranes_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString() & "_" & IdOrden.ToString(), dsAlbaranes)
        Else
            dsAlbaranes = HttpContext.Current.Cache("DsetAlbaranes_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString() & "_" & IdOrden.ToString())
        End If

        Return dsAlbaranes
    End Function
    ''' Revisado por: blp. Fecha: 10/01/2013
    ''' <summary>
    ''' Propiedad que nos devolverá las ordenes de pedido guardadas en caché.
    ''' Si no hay caché, lanza la carga de órdenes con valores por defecto 
    ''' </summary>
    ''' <returns>ordenes de pedidodel usuario</returns>
    ''' <remarks>Llamada desde todos los sitios donde se use la propiedad
    ''' Tiempo máximo: 2 sec</remarks>
    Public ReadOnly Property DsetPedidos() As DataSet
        Get
            Dim dsOrdenes As DataSet
            If HttpContext.Current.Cache("DsetPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                Dim bActualizarFiltros As Boolean = False
                dsOrdenes = ActualizarDsetpedidos(bActualizarFiltros).Copy
            Else
                dsOrdenes = CType(HttpContext.Current.Cache("DsetPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()), DataSet).Copy
            End If
            Return dsOrdenes
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que nos devolverá todas las ordenes de pedido guardadas en caché.
    ''' Si no hay caché, lanza la carga de órdenes con valores por defecto y sin paginación
    ''' </summary>
    ''' <returns>todas las ordenes de pedido del usuario</returns>
    ''' <remarks>Llamada desde todos los sitios donde se use la propiedad
    ''' Tiempo máximo: 2 sec</remarks>
    Public ReadOnly Property DsetTodosPedidos() As DataSet
        Get
            Dim dsOrdenes As DataSet
            If HttpContext.Current.Cache("DsetTodosPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                dsOrdenes = ActualizarDsetTodosPedidos().Copy
            Else
                dsOrdenes = CType(HttpContext.Current.Cache("DsetTodosPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()), DataSet).Copy
            End If
            Return dsOrdenes
        End Get
    End Property

    ''' Revisado por: blp. Fecha: 17/04/2012
    ''' <summary>
    ''' Función que nos devolverá desde bdd un dataset con las ordenes de pedido que el usuario puede ver
    ''' </summary>
    ''' <param name="bActualizarFiltros">Indica si queremos actualizar el listado de filtros. En teoría, si el usuario no pulsa "Buscar", quiere seguir con los mismos filtros aunque haya cambiado los filtros en el buscador, de modo que sólo debería pasarse TRUE cuando se pulse "Buscar"</param>
    ''' <returns>Dataset de pedidos</returns>
    ''' <remarks>Llamada desde la propiedad DsetPedidos y desde btnReemitir_click
    ''' Tiempo máximo: 2 sec</remarks>
    Private Function ActualizarDsetpedidos(ByVal bActualizarFiltros As Boolean) As DataSet
        Dim dsOrdenes As New DataSet
        If bActualizarFiltros Then _
            ActualizarFiltros()
        dsOrdenes = DevolverPedidosConFiltro(CampoOrden, SentidoOrdenacion, FiltrosAnyadidos, PageNumber, PageSize)
        Me.InsertarEnCache("DsetPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString(), dsOrdenes.Copy, CacheItemPriority.BelowNormal)
        Return dsOrdenes
    End Function
    ''' <summary>
    ''' Función que nos devolverá desde bdd un dataset con las ordenes de pedido que el usuario puede ver
    ''' </summary>
    ''' <returns>Dataset de todos los pedidos del usuario</returns>
    ''' <remarks>Tiempo máximo: 2 sec</remarks>
    Private Function ActualizarDsetTodosPedidos() As DataSet
        Dim dsOrdenes As New DataSet
        dsOrdenes = DevolverPedidosConFiltro(CampoOrden, SentidoOrdenacion, FiltrosAnyadidos, 1, PageSize, True)
        Me.InsertarEnCache("DsetTodosPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString(), dsOrdenes.Copy, CacheItemPriority.BelowNormal)
        Return dsOrdenes
    End Function
    ''' <summary>
    ''' Método que elimina los dataset de pedidos de caché
    ''' a fin de permitir que se vuelva a obtener el dataset de pedidos desde la bdd
    ''' </summary>
    ''' <remarks>Llamada desde todos los lugares donde se desee que conecte de nuevo a la bdd para devolver los pedidos
    ''' Tiempo máximo: 2 sec</remarks>
    Private Sub EliminarCachedePedidos()
        If Not HttpContext.Current.Cache("DsetPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
            HttpContext.Current.Cache.Remove("DsetPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
        End If
    End Sub
    ''' <summary>
    ''' Procedimiento que crea una tabla en el dataset de pedidos para los adjuntos de las ordenes de entrega
    ''' </summary>
    ''' <remarks>Llamada desde la función CargarAdjuntosOrden
    ''' Tiempo máximo: 0 sec.</remarks>
    Public Sub CrearTablaAdjuntosOrden()
        If DsetPedidos.Tables("ADJUNTOS_ORDEN") Is Nothing Then
            Dim dt As DataTable = New DataTable("ADJUNTOS_ORDEN")
            dt.Columns.Add("ID", GetType(System.Int32))
            dt.Columns.Add("ORDEN", GetType(System.Int32))
            dt.Columns.Add("NOMBRE", GetType(System.String))
            dt.Columns.Add("COMENTARIO", GetType(System.String))
            dt.Columns.Add("FECHA", GetType(System.DateTime))
            dt.Columns.Add("DATASIZE", GetType(System.Double))
            dt.Columns.Add("TIPO", GetType(TiposDeDatos.Adjunto.Tipo))
            Dim keylp() As DataColumn = {dt.Columns("ID")}
            dt.PrimaryKey = keylp
            AnyadirTablaADsetPedidos(dt, "ORDENES", "ORDEN", "REL_ORDENES_ADJUNTOS")
        End If
    End Sub
    ''' <summary>
    ''' Procedimiento que crea una tabla en el dataset de pedidos para los adjuntos de los pedidos
    ''' </summary>
    ''' <remarks>Llamada desde la función CargarAdjuntosOrden
    ''' Tiempo máximo: 0 sec.</remarks>
    Public Sub CrearTablasAdjuntosLinea()
        If DsetPedidos.Tables("ADJUNTOS_LINEA") Is Nothing Then
            Dim dt As DataTable = New DataTable("ADJUNTOS_LINEA")
            dt.Columns.Add("ID", GetType(System.Int32))
            dt.Columns.Add("LINEA", GetType(System.Int32))
            dt.Columns.Add("NOMBRE", GetType(System.String))
            dt.Columns.Add("COMENTARIO", GetType(System.String))
            dt.Columns.Add("FECHA", GetType(System.DateTime))
            dt.Columns.Add("DATASIZE", GetType(System.Double))
            dt.Columns.Add("TIPO", GetType(TiposDeDatos.Adjunto.Tipo))
            Dim keylp() As DataColumn = {dt.Columns("ID")}
            dt.PrimaryKey = keylp
            AnyadirTablaADsetPedidos(dt, "LINEASPEDIDO", "LINEA", "REL_LINEAS_ADJUNTOS")
        End If
    End Sub
    ''' Revisado por: blp. Fecha: 09/01/2013
    ''' <summary>
    ''' Añade la tabla pasada como parámetro al dataset de pedidos.
    ''' Si el dataset no está en caché, se llama a ActualizarDsetPedidos
    ''' </summary>
    ''' <param name="dtTabla">Tabla a añadir</param>
    ''' <param name="sTablaPadre">Nombre de la tabla en la que comprobar si existe la fila padre con la que está asociada la fila a añadir</param>
    ''' <param name="sCampoID">Campo de la tabla añadida asociado a la tabla principal del DsetPedidos</param>
    ''' <param name="sRelacionPadre">Nombre de la relación entre la tabla añadida y la tabla padre</param>
    ''' <remarks>Llamada desde CrearTablasAdjuntosLinea y CrearTablasAdjuntosOrden. Máx. 0,1 seg.</remarks>
    Private Sub AnyadirTablaADsetPedidos(ByVal dtTabla As DataTable, ByVal sTablaPadre As String, ByVal sCampoID As String, ByVal sRelacionPadre As String)
        Dim keylp() As DataColumn = {dtTabla.Columns("ID")}
        If HttpContext.Current.Cache("DsetPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
            Dim bActualizarFiltros As Boolean = False
            ActualizarDsetpedidos(bActualizarFiltros)
        End If
        CType(HttpContext.Current.Cache("DsetPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()), DataSet).Tables.Add(dtTabla)
        keylp(0) = CType(HttpContext.Current.Cache("DsetPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()), DataSet).Tables(dtTabla.TableName).Columns(sCampoID)
        CType(HttpContext.Current.Cache("DsetPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()), DataSet).Relations.Add(sRelacionPadre, CType(HttpContext.Current.Cache("DsetPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()), DataSet).Tables(sTablaPadre).PrimaryKey, keylp, False)
    End Sub
    ''' Revisado por: blp. Fecha: 09/01/2013
    ''' <summary>
    ''' Crea la fila en la tabla que corresponda
    ''' </summary>
    ''' <param name="sTabla">Nombre de la tabla en la que crear la fila</param>
    ''' <param name="sTablaPadre">Nombre de la tabla en la que comprobar si existe la fila padre con la que está asociada la fila a añadir</param>
    ''' <param name="sCampoID">Campo de la tabla padre en la que buscar si existe el registro</param>
    ''' <param name="id">Id a buscar en el campo sCampoId de la tabla sTablaPadre</param>
    ''' <returns>La fila creada</returns>
    ''' <remarks>Llamada desde CargarAdjuntosOrden y CargarAdjuntosLinea. Maximo 0,1 seg.</remarks>
    Private Function CrearFilaNueva(ByVal sTabla As String, ByVal sTablaPadre As String, ByVal sCampoID As String, ByVal id As Integer) As DataRow
        Dim drFila As DataRow = Nothing
        Dim drFilaPadre As DataRow = CType(HttpContext.Current.Cache("DsetPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()), DataSet).Tables(sTablaPadre).Rows.Find(id)
        If drFilaPadre IsNot Nothing Then
            drFila = CType(HttpContext.Current.Cache("DsetPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()), DataSet).Tables(sTabla).NewRow
        End If
        Return drFila
    End Function
    ''' Revisado por: blp. Fecha: 09/01/2013
    ''' <summary>
    ''' Añade la tabla pasada como parámetro al dataset de pedidos.
    ''' Si el dataset no está en caché, se llama a ActualizarDsetPedidos
    ''' </summary>
    ''' <param name="sTabla">Nombre de la tabla en la que añadir la fila</param>
    ''' <param name="sTablaPadre">Nombre de la tabla en la que comprobar si existe la fila padre con la que está asociada la fila a añadir</param>
    ''' <param name="sCampoID">Nombre del campo con el que comprobar si existe la fila padre con la que está asociada la fila a añadir</param>
    ''' <param name="drFila">Fila a añadir</param>
    ''' <remarks>Llamada desde CrearTablasAdjuntosLinea y CrearTablasAdjuntosOrden. Máx. 0,1 seg.</remarks>
    Private Sub AnyadirFilaADsetPedidos(ByVal sTabla As String, ByVal sTablaPadre As String, ByVal sCampoID As String, ByVal drFila As DataRow)
        If HttpContext.Current.Cache("DsetPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
            Dim bActualizarFiltros As Boolean = False
            ActualizarDsetpedidos(bActualizarFiltros)
        End If
        Dim drFilaPadre As DataRow = CType(HttpContext.Current.Cache("DsetPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()), DataSet).Tables(sTablaPadre).Rows.Find(drFila.Item(sCampoID))
        If drFilaPadre IsNot Nothing Then
            CType(HttpContext.Current.Cache("DsetPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()), DataSet).Tables(sTabla).Rows.Add(drFila)
        End If
    End Sub
    ''' Revisado por: blp. Fecha:30/11/2012
    ''' <summary>
    ''' Función que devolverá un dataset con los pedidos filtrados y paginados por los campos pasados como parámetros
    ''' </summary>
    ''' <param name="Idioma">Idioma del usuario</param>
    ''' <param name="codPer">Codigo del usuario aprovisionador</param>
    ''' <param name="Anyo">Año del pedido</param>
    ''' <param name="NumPed">Número dado al pedido (cesta) emitido en la aplicación</param>
    ''' <param name="NumOrden">Número dado a la orden (pedido) emitida en la aplicación</param>
    ''' <param name="NumPedProve">Nº de pedido dado por el proveedor</param>
    ''' <param name="NumPedidoErp">Número de pedido dado en el ERP del cliente a la orden</param>
    ''' <param name="FecEmisionDesde">Fecha a partir de la cual filtrar la fecha de emisión del pedido</param>
    ''' <param name="FecEmisionHasta">Fecha hasta la cual filtrar la fecha de emisión</param>
    ''' <param name="NumMeses">Número de meses desde el día de hoy hacia atrás de los que hay que devolver los pedidos</param>
    ''' <param name="Empresa">Código de la empresa</param>
    ''' <param name="Receptor">Codigo del receptor</param>
    ''' <param name="Prove">Codigo del proveedor</param>
    ''' <param name="Prove_Erp">Codigo del proveedor en el ERP</param>
    ''' <param name="TipoPedido">Tipo de pedido</param>
    ''' <param name="Estados">Valor sumado de los estados del pedido que se desean ver</param>
    ''' <param name="Ver_Abonos">Valor 1: Ver los pedidos que son Abono. Valor 0: No verlos</param>
    ''' <param name="Ver_Ped_Ep">Valor 1: Ver los pedidos de tipo (origen) EP (TIPO=1) y no libres. Valor 0: No verlos</param>
    ''' <param name="Ver_Ped_Ep_Libres">Valor 1: Ver los pedidos de tipo (origen) EP (TIPO=1) y libres. Valor 0: No verlos</param>
    ''' <param name="Ver_Ped_Neg">Valor 1: Ver los pedidos de tipo (origen) GS (TIPO=0). Valor 0: No verlos</param>
    ''' <param name="Ver_Ped_Erp">Valor 1: Ver los pedidos de tipo (origen) ERP (TIPO=2). Valor 0: No verlos</param>
    ''' <param name="Usu_Ped">Valor 1: Ver los pedidos del usuario. Valor 0: No verlos</param>
    ''' <param name="Cc_Ped">Valor 1: Ver los pedidos que no sean del aprovisionador y que sean de su CC. Valor 0: No verlos</param>
    ''' <param name="Articulo">Texto a buscar como una parte del código del artículo, de su descripción, o de otros campos descriptivos del artículo</param>
    ''' <param name="Categorias">Conjunto de códigos de partidas categorías pasados en un tipo tabla</param>
    ''' <param name="CentroCoste">Código del centro de coste</param>
    ''' <param name="PartidasPres">Conjunto de códigos de partidas presupuestarias pasados en un tipo tabla</param>
    ''' <param name="AnyoFra">Año de la factura</param>
    ''' <param name="NumFra">Número de la factura (búsqueda exacta, no aproximada)</param>
    ''' <param name="EstFra">Estado de la factura</param>
    ''' <param name="FecFraDesde">Fecha a partir de la cual filtrar la fecha de factura</param>
    ''' <param name="FecFraHasta">Fecha hasta la cual filtrar la fecha de factura</param>
    ''' <param name="NumPago">Número del pago (búsqueda exacta, no aproximada)</param>
    ''' <param name="EstPago">Estado del pago</param>
    ''' <param name="FecPagoDesde">Fecha a partir de la cual filtrar la fecha de pago</param>
    ''' <param name="FecPagoHasta">Fecha hasta la cual filtrar la fecha de pago</param>
    ''' <param name="Gestor">gestor de las partidas a las que se han imputado las líneas de cada pedido</param>
    ''' <param name="iPageNumber">Número de página a devolver</param>
    ''' <param name="PageSize">Número de registros a devolver</param>
    ''' <param name="SortExpression">Campo por el que se va a filtrar</param>
    ''' <param name="SortOrder">Orden de los datos: Ascendente (ASC) o descendente (DESC). Por defecto ASC</param>
    ''' <param name="ParaExportacion">Indica si vamos a usar los datos para mostrar en Seguimiento o Exportar. Si es para exportar, no se paginan (se usa distinto Procedimiento almacenado)</param>
    ''' <returns>Devuelve un dataset con los pedidos</returns>
    ''' <remarks>LLamada desde DsetPedidos
    ''' Tiempo maximo 1 sec</remarks>
    Private Function GenerarDsetPedidos(ByVal Idioma As FSNLibrary.Idioma, ByVal codPer As String, ByVal Anyo As Integer, ByVal NumPed As Integer, ByVal NumOrden As Integer, ByVal NumPedProve As String, ByVal NumPedidoErp As String, ByVal FecEmisionDesde As Date, ByVal FecEmisionHasta As Date, ByVal NumMeses As Integer, ByVal Empresa As Integer, ByVal Receptor As String, ByVal Prove As String, ByVal Prove_Erp As String, ByVal TipoPedido As Integer, ByVal iEstados As Integer, ByVal Ver_Abonos As Boolean, ByVal Ver_Ped_Ep As Boolean, ByVal Ver_Ped_Ep_Libres As Boolean, ByVal Ver_Ped_Neg As Boolean, ByVal Ver_Ped_Erp As Boolean, ByVal Usu_Ped As Boolean, ByVal Cc_Ped As Boolean, ByVal Articulo As DataTable, ByVal Categorias As DataTable, ByVal CentroCoste As String, ByVal PartidasPres As DataTable, ByVal AnyoFra As Integer, ByVal NumFra As String, ByVal EstFra As Integer, ByVal FecFraDesde As Date, ByVal FecFraHasta As Date, ByVal NumPago As String, ByVal EstPago As Integer, ByVal FecPagoDesde As Date, ByVal FecPagoHasta As Date, ByVal Gestor As String, ByVal VerPedidosBloqueados As Boolean, ByVal iPageNumber As Integer, ByVal PageSize As Integer, ByVal SortExpression As String, ByVal SortOrder As String, Optional ByVal ParaExportacion As Boolean = False,
                                                Optional Ver_ContraAbiertos As Boolean = True, Optional ByVal iAnyoPedAbierto As Integer = Nothing, Optional ByVal iCestaPedAbierto As Long = Nothing, Optional ByVal iPedidoPedAbierto As Long = Nothing, Optional ByVal Ver_Ped_Express As Boolean = False, Optional ByVal VerPedidosBorrados As Boolean = False) As DataSet
        Dim oOrdenes As COrdenes = Me.FSNServer.Get_Object(GetType(FSNServer.COrdenes))
        Dim iTotalRegistros As Integer = TotalRegistros

        Dim dtEstados As New DataTable("ESTADOS")
        dtEstados.Columns.Add("EST")
        Dim drEstado As DataRow

        If iEstados And CPedido.Estado.PendienteAprobar Then
            drEstado = dtEstados.NewRow
            drEstado("EST") = CInt(TipoEstadoOrdenEntrega.PendienteDeAprobacion)
            dtEstados.Rows.Add(drEstado)
        End If
        If iEstados And CPedido.Estado.DenegadoParcialmente Then
            drEstado = dtEstados.NewRow
            drEstado("EST") = CInt(TipoEstadoOrdenEntrega.DenegadoParcialAprob)
            dtEstados.Rows.Add(drEstado)
        End If
        If iEstados And CPedido.Estado.EmitidoAlProveedor Then
            drEstado = dtEstados.NewRow
            drEstado("EST") = CInt(TipoEstadoOrdenEntrega.EmitidoAlProveedor)
            dtEstados.Rows.Add(drEstado)
        End If
        If iEstados And CPedido.Estado.AceptadoPorElProveedor Then
            drEstado = dtEstados.NewRow
            drEstado("EST") = CInt(TipoEstadoOrdenEntrega.AceptadoPorProveedor)
            dtEstados.Rows.Add(drEstado)
        End If
        If iEstados And CPedido.Estado.EnCamino Then
            drEstado = dtEstados.NewRow
            drEstado("EST") = CInt(TipoEstadoOrdenEntrega.EnCamino)
            dtEstados.Rows.Add(drEstado)
        End If
        If iEstados And CPedido.Estado.RecibidoParcialmente Then
            drEstado = dtEstados.NewRow
            drEstado("EST") = CInt(TipoEstadoOrdenEntrega.EnRecepcion)
            dtEstados.Rows.Add(drEstado)
        End If
        If iEstados And CPedido.Estado.Cerrado Then
            drEstado = dtEstados.NewRow
            drEstado("EST") = CInt(TipoEstadoOrdenEntrega.RecibidoYCerrado)
            dtEstados.Rows.Add(drEstado)
        End If
        If iEstados And CPedido.Estado.Anulado Then
            drEstado = dtEstados.NewRow
            drEstado("EST") = CInt(TipoEstadoOrdenEntrega.Anulado)
            dtEstados.Rows.Add(drEstado)
        End If
        If iEstados And CPedido.Estado.Rechazado Then
            drEstado = dtEstados.NewRow
            drEstado("EST") = CInt(TipoEstadoOrdenEntrega.RechazadoPorProveedor)
            dtEstados.Rows.Add(drEstado)
        End If
        If iEstados And CPedido.Estado.Denegado Then
            drEstado = dtEstados.NewRow
            drEstado("EST") = CInt(TipoEstadoOrdenEntrega.DenegadoTotalAprobador)
            dtEstados.Rows.Add(drEstado)
        End If
        Dim dsOrdenes As DataSet
        dsOrdenes = oOrdenes.BuscarTodasOrdenes(Idioma, codPer, Anyo, NumPed, NumOrden, NumPedProve, NumPedidoErp, FecEmisionDesde, FecEmisionHasta, NumMeses, Empresa, Receptor, Prove, Prove_Erp, TipoPedido, dtEstados, Ver_Abonos, Ver_Ped_Ep, Ver_Ped_Ep_Libres, Ver_Ped_Neg, Ver_Ped_Erp, Usu_Ped, Cc_Ped, Articulo, Categorias, CentroCoste, PartidasPres, AnyoFra, NumFra, EstFra, FecFraDesde, FecFraHasta, NumPago, EstPago, FecPagoDesde, FecPagoHasta, Gestor, VerPedidosBloqueados, iPageNumber, PageSize, SortExpression, SortOrder, iTotalRegistros, ParaExportacion, Ver_ContraAbiertos:=Ver_ContraAbiertos, iAnyoPedAbierto:=iAnyoPedAbierto, iCestaPedAbierto:=iCestaPedAbierto, iPedidoPedAbierto:=iPedidoPedAbierto, Ver_Ped_Express:=Ver_Ped_Express, VerPedidosBorrados:=VerPedidosBorrados)
        'Actualizamos la propiedad TotalRegistros cuando cambie el número de registros
        If iTotalRegistros <> TotalRegistros AndAlso iTotalRegistros >= 0 Then
            TotalRegistros = iTotalRegistros
        End If
        Return dsOrdenes
    End Function
    ''' Revisado por: blp. Fecha: 17/04/2012
    ''' <summary>
    ''' Vamos a obtener la información nacesaria para los dropdown desde el stored pero sin recuperar ni almacenar en cache el resto de información (órdenes, lineas e imputación)
    ''' </summary>
    ''' <param name="CodPersona">Código de la persona</param>
    ''' <param name="Idioma">idioma en el que devolver el dataset</param>
    ''' <remarks>Llamada desde InicializarControles. Máx. 0,5 seg.</remarks>
    Private Function getDropDownsInfo(ByVal CodPersona As String, ByVal Idioma As FSNLibrary.Idioma) As DataSet
        Dim oOrdenes As COrdenes = Me.FSNServer.Get_Object(GetType(FSNServer.COrdenes))
        Dim dsInfoDropDowns As New DataSet
        If HttpContext.Current.Cache("DsetSeguimientoDropDownsInfo_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
            dsInfoDropDowns = oOrdenes.ObtenerDatosCombosSeguimiento(Idioma.ToString(), CodPersona, Usuario.PermisoVerPedidosCCImputables)
            Me.InsertarEnCache("DsetSeguimientoDropDownsInfo_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString(), dsInfoDropDowns)
        Else
            dsInfoDropDowns = HttpContext.Current.Cache("DsetSeguimientoDropDownsInfo_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
        End If
        Return dsInfoDropDowns
    End Function
    ''' Revisado por: blp. Fecha: 17/01/2012
    ''' <summary>
    ''' Función que devuelve un dataset con los pedidos una vez filtrados y ordenados por los criterios seleccionados por el usuario
    ''' </summary>
    ''' <param name="OrdenacionCampo">Id del Campo por el que ordenar</param>
    ''' <param name="OrdenacionSentido">Sentido de la ordenación ASC o DESC</param>
    ''' <param name="Filtros">Lista de Filtros a aplicar en los pedidos</param>
    ''' <param name="iPageNumber">Número de página a devolver</param>
    ''' <param name="PageSize">Número de registros a devolver</param>
    ''' <param name="ParaExportacionAExcel">Indica si los datos recuperados son para exportar. En ese caso, la consulta se hará sobre otro procedimiento y devolverá todos los registros</param>
    ''' <returns>Dataset con los pedidos una vez filtrados y ordenados</returns>
    ''' <remarks>Llamada desde DevolverPedidosConFiltro. Tiempo máx inferior a 1 seg</remarks>
    Friend Function DevolverPedidosConFiltro(ByVal OrdenacionCampo As String, ByVal OrdenacionSentido As String, ByVal Filtros As List(Of Filtro), ByVal iPageNumber As Integer, ByVal PageSize As Integer, Optional ByVal ParaExportacionAExcel As Boolean = False) As DataSet
        Dim Aprovisionador As String = String.Empty
        Dim Anyo As Integer = 0
        Dim NumPed As Integer = 0
        Dim NumOrden As Integer = 0
        Dim NumPedProve As String = String.Empty
        Dim NumPedidoErp As String = String.Empty
        Dim FecEmisionDesde As Date = Nothing
        Dim FecEmisionHasta As Date = Nothing
        Dim NumMeses As Integer = 0
        Dim Empresa As Integer = 0
        Dim Receptor As String = String.Empty
        Dim Prove As String = String.Empty
        Dim Prove_Erp As String = String.Empty
        Dim TipoPedido As Integer = 0
        Dim Estados As Integer = 0
        Dim Ver_Abonos As Boolean = False
        Dim Ver_Ped_Ep As Boolean = True 'EP no libres
        Dim Ver_Ped_Ep_Libres As Boolean = True 'EP LIBRES
        Dim Ver_Ped_Neg As Boolean = True 'GS
        Dim Ver_Ped_Erp As Boolean = True 'ERP
        Dim Usu_Ped As Boolean
        Dim CC_Ped As Boolean
        Dim Articulo As DataTable
        Dim Categorias As DataTable
        Dim CentroCoste As String = String.Empty
        Dim PartidasPres As DataTable
        Dim AnyoFra As Integer = 0
        Dim NumFra As String = String.Empty
        Dim EstFra As Integer = 0
        Dim FecFraDesde As Date = Nothing
        Dim FecFraHasta As Date = Nothing
        Dim NumPago As String = String.Empty
        Dim EstPago As Integer = 0
        Dim FecPagoDesde As Date = Nothing
        Dim FecPagoHasta As Date = Nothing
        Dim Gestor As String = String.Empty
        Dim sPartidasPres As String()
        Dim VerPedidosBloqueados As Boolean
        Dim Ver_ContraAbiertos As Boolean
        Dim Ver_Ped_Express As Boolean
        Dim iAnyoPedAbierto As Integer
        Dim iCestaPedAbierto As Integer
        Dim iPedidoPedAbierto As Integer
        Dim VerPedidosBorrados As Boolean

        For Each ofiltro As Filtro In Filtros
            Dim x As Filtro = ofiltro
            Select Case x.tipo
                Case TipoFiltro.Anio
                    Anyo = x.Valor
                Case TipoFiltro.Categoria
                    Dim oOrdenes As FSNServer.COrdenes = Me.FSNServer.Get_Object(GetType(FSNServer.COrdenes))
                    Dim sCat As String = x.Valor
                    Categorias = oOrdenes.CargarTablaCategorias(sCat)
                    oOrdenes = Nothing
                Case TipoFiltro.Cesta
                    NumPed = x.Valor
                Case TipoFiltro.Empresa
                    Empresa = x.Valor
                Case TipoFiltro.Estado
                    Estados = x.Valor
                Case TipoFiltro.FechaDesde
                    FecEmisionDesde = x.ValorFecha
                Case TipoFiltro.FechaHasta
                    FecEmisionHasta = x.ValorFecha
                Case TipoFiltro.NumPedido
                    NumOrden = x.Valor
                Case TipoFiltro.NumPedidoERP
                    NumPedidoErp = x.Valor
                Case TipoFiltro.NumPedidoProve
                    NumPedProve = x.Valor
                Case TipoFiltro.Proveedor
                    Prove = x.Valor
                Case TipoFiltro.ProveedorERP
                    Prove_Erp = x.Valor
                Case TipoFiltro.Tipo
                    Ver_Ped_Neg = x.Valores(0)
                    Ver_Ped_Ep = x.Valores(1)
                    Ver_Ped_Ep_Libres = x.Valores(2)
                    Ver_Abonos = x.Valores(4)
                    If Session("HayIntegracionEntradaoEntradaSalida") Then
                        Ver_Ped_Erp = x.Valores(3)
                    Else
                        Ver_Ped_Erp = Ver_Ped_Ep And Ver_Ped_Neg
                    End If
                    Ver_ContraAbiertos = x.Valores(5)
                    Ver_Ped_Express = x.Valores(6)
                Case TipoFiltro.Receptor
                    Receptor = x.Valor
                Case TipoFiltro.CentroCoste
                    CentroCoste = x.Valor
                Case TipoFiltro.PartidaPresupuestaria
                    'Este es el único tipo de filtro que puede repetirse, por lo que lo agrupamos en la variable sPartidasPres y lo trataremos al salir del bucle en el que estamos
                    If sPartidasPres Is Nothing Then
                        ReDim Preserve sPartidasPres(0)
                    Else
                        ReDim Preserve sPartidasPres(sPartidasPres.Length)
                    End If
                    sPartidasPres(sPartidasPres.Length - 1) = x.Valor
                Case TipoFiltro.FraAnio
                    If IsNumeric(x.Valor) Then
                        AnyoFra = x.Valor
                    Else
                        AnyoFra = 0
                    End If
                Case TipoFiltro.FraNum
                    NumFra = x.Valor
                Case TipoFiltro.FraEstado
                    EstFra = x.Valor
                Case TipoFiltro.FraFechaDesde
                    FecFraDesde = x.ValorFecha
                Case TipoFiltro.FraFechaHasta
                    FecFraHasta = x.ValorFecha
                Case TipoFiltro.PagoNum
                    NumPago = x.Valor
                Case TipoFiltro.PagoEstado
                    EstPago = x.Valor
                Case TipoFiltro.PagoFechaDesde
                    FecPagoDesde = x.ValorFecha
                Case TipoFiltro.PagoFechaHasta
                    FecPagoHasta = x.ValorFecha
                Case TipoFiltro.TipoPedido
                    TipoPedido = x.Valor
                Case TipoFiltro.NumMeses
                    NumMeses = x.Valor
                Case TipoFiltro.PedidosConAlbaranesBloqueados
                    VerPedidosBloqueados = x.Valor
                Case TipoFiltro.Gestor
                    Gestor = x.Valor
                Case TipoFiltro.Articulo
                    Dim oOrdenes As FSNServer.COrdenes = Me.FSNServer.Get_Object(GetType(FSNServer.COrdenes))
                    Articulo = oOrdenes.CargarTablaStrings(x.Valor, " ")
                    oOrdenes = Nothing
                Case TipoFiltro.Usu_Ped
                    Usu_Ped = CBool(x.Valor)
                Case TipoFiltro.CC_Ped
                    CC_Ped = CBool(x.Valor)
                Case TipoFiltro.AnyoPedAbierto
                    iAnyoPedAbierto = x.Valor
                Case TipoFiltro.CestaPedAbierto
                    iCestaPedAbierto = x.Valor
                Case TipoFiltro.PedidoPedAbierto
                    iPedidoPedAbierto = x.Valor
                Case TipoFiltro.PedidoBorrado
                    VerPedidosBorrados = x.Valor
            End Select
        Next

        If sPartidasPres IsNot Nothing AndAlso sPartidasPres.Length > 0 Then
            Dim oOrdenes As FSNServer.COrdenes = Me.FSNServer.Get_Object(GetType(FSNServer.COrdenes))
            PartidasPres = oOrdenes.CargarTablaPartidas(sPartidasPres, TiposDeDatos.Aplicaciones.EP)
            oOrdenes = Nothing
        End If
        Return GenerarDsetPedidos(Me.Usuario.Idioma.ToString, Me.Usuario.CodPersona, Anyo, NumPed, NumOrden, NumPedProve, NumPedidoErp, FecEmisionDesde, FecEmisionHasta, NumMeses, Empresa, Receptor, Prove, Prove_Erp, TipoPedido, Estados, Ver_Abonos, Ver_Ped_Ep, Ver_Ped_Ep_Libres, Ver_Ped_Neg, Ver_Ped_Erp, Usu_Ped, CC_Ped, Articulo, Categorias, CentroCoste, PartidasPres, AnyoFra, NumFra, EstFra, FecFraDesde, FecFraHasta, NumPago, EstPago, FecPagoDesde, FecPagoHasta, Gestor, VerPedidosBloqueados, iPageNumber, PageSize, OrdenacionCampo, OrdenacionSentido, ParaExportacionAExcel, Ver_ContraAbiertos, iAnyoPedAbierto:=iAnyoPedAbierto, iCestaPedAbierto:=iCestaPedAbierto, iPedidoPedAbierto:=iPedidoPedAbierto, Ver_Ped_Express:=Ver_Ped_Express, VerPedidosBorrados:=VerPedidosBorrados)
    End Function
    ''' Revisado por: blp. Fecha: 11/01/2013
    ''' <summary>
    ''' Devuelve las descripciones de los artículos que coinciden con el texto pasado como parametro y corresponden al usuario
    ''' </summary>
    ''' <param name="prefixText">Texto buscado</param>
    ''' <param name="count">Cantidad de registros a devolver</param>
    ''' <param name="contextKey">código del usuario (definido en función InicializarControles</param>
    ''' <returns>Lista con los artículos</returns>
    ''' <remarks>Llamada desde control txtArticulo_AutoCompleteExtender. Máximo 3 segundos</remarks>
    <System.Web.Services.WebMethodAttribute(),
        System.Web.Script.Services.ScriptMethodAttribute()>
    Public Shared Function AutoCompletar(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As IEnumerable(Of String) 'String()
        'Como se llama a esta función cada vez que se teclea en el autocompletar (a partir del MinimumPrefixLength que es de 3), vamos a guardar en caché la búsqueda con los 3 caracteres.
        'Si se teclean más, se tira de caché, si se accede con otros tres caracteres, se borra la caché y se crea otra
        Dim textoBuscadoBase = prefixText.Substring(0, 3)
        Dim sArticulos As String()
        If HttpContext.Current.Cache("DsetArticulosSeguimiento_" & contextKey & "_codArt" & textoBuscadoBase) Is Nothing Then
            EliminarCacheArticulos(contextKey)
            sArticulos = DevolverArticulos(textoBuscadoBase, contextKey)
        Else
            sArticulos = CType(HttpContext.Current.Cache("DsetArticulosSeguimiento_" & contextKey & "_codArt" & textoBuscadoBase), String())
        End If
        Dim resul As IEnumerable(Of String) = From articulo In sArticulos
                                              Where articulo.ToUpper.Contains(prefixText.ToUpper)
                                              Take (count)
        Return resul
    End Function
    ''' Revisado por: blp. Fecha: 23/01/2013
    ''' <summary>
    ''' Devuelve las descripciones de los artículos que coinciden con el texto pasado como parametro y corresponden al usuario
    ''' </summary>
    ''' <param name="textoFiltroArticulo">Texto buscado</param>
    ''' <param name="codPer">Código de la persona</param>
    ''' <returns>array con las descripciones</returns>
    ''' <remarks>Laamada desde Autocompletar. Max. 1 seg.</remarks>
    Public Shared Function DevolverArticulos(ByVal textoFiltroArticulo As String, ByVal codPer As String) As String()
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim oArticulos As cArticulos = oServer.Get_Object(GetType(cArticulos))
        If HttpContext.Current.Cache("DsetArticulosSeguimiento_" & codPer & "_codArt" & textoFiltroArticulo) Is Nothing Then
            Dim dsResul As DataSet = oArticulos.DevolverDescripcionesArticulos(textoFiltroArticulo, codPer, oUsuario.PermisoVerPedidosCCImputables)
            Dim resul As String()
            If dsResul IsNot Nothing AndAlso dsResul.Tables(0) IsNot Nothing AndAlso dsResul.Tables(0).Rows.Count > 0 Then
                For i = 0 To dsResul.Tables(0).Rows.Count - 1
                    ReDim Preserve resul(i)
                    resul(i) = dsResul.Tables(0).Rows(i).Item("DESCRDEF")
                Next
                HttpContext.Current.Cache.Insert("DsetArticulosSeguimiento_" & codPer & "_codArt" & textoFiltroArticulo, resul, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration,
                                                 New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            Else
                ReDim resul(0)
                resul(0) = String.Empty
            End If
            Return resul
        Else
            Return HttpContext.Current.Cache("DsetArticulosSeguimiento_" & codPer & "_codArt" & textoFiltroArticulo)
        End If
    End Function
    ''' Revisado por: blp. Fecha: 18/04/2012
    ''' <summary>
    ''' Recuperamos el listado de empresas presentes en la tabla de DDLINFO del dataset de pedidos
    ''' </summary>
    ''' <param name="dsInfo">
    ''' Podemos pasarle el dataset desde es que recupera los datos o dejar que los tome del DsetPedidos
    ''' </param>
    ''' <returns>Lista con las empresas de todos los pedidos del usuario y/o los centros de coste a los que puede imputar</returns>
    ''' <remarks>Llamada desde InicializarControles. Máximo 0,1 seg.</remarks>
    Private Function Empresas(Optional ByVal dsInfo As DataSet = Nothing) As List(Of ListItem)
        If dsInfo Is Nothing OrElse dsInfo.Tables.Count = 0 Then
            dsInfo = getDropDownsInfo(FSNUser.CodPersona, FSNUser.Idioma)
        End If
        Dim query As List(Of ListItem) = From Datos In dsInfo.Tables("EMPRESAS")
                                         Order By Datos.Item("DENEMPRESA")
                                         Select New ListItem(Datos.Item("DENEMPRESA"), Datos.Item("EMPRESA")) Distinct.ToList()
        Return query
    End Function
    ''' Revisado por: blp. Fecha: 18/04/2012
    ''' <summary>
    ''' Recuperamos el listado de Receptores presentes en la tabla de DDLINFO del dataset de pedidos
    ''' </summary>
    ''' <param name="dsInfo">
    ''' Podemos pasarle el dataset desde es que recupera los datos o dejar que los tome del DsetPedidos
    ''' </param>
    ''' <returns>Lista con los receptores de todos los pedidos del usuario y/o los centros de coste a los que puede imputar</returns>
    ''' <remarks>Llamada desde InicializarControles. Máximo 0,1 seg.</remarks>
    Private Function Receptores(Optional ByVal dsInfo As DataSet = Nothing) As List(Of ListItem)
        If dsInfo Is Nothing OrElse dsInfo.Tables.Count = 0 Then
            dsInfo = getDropDownsInfo(FSNUser.CodPersona, FSNUser.Idioma)
        End If
        Dim query As List(Of ListItem) = From Datos In dsInfo.Tables("RECEPTORES")
                                         Where Not IsDBNull(Datos.Item("RECEPTOR"))' IsNot System.DBNull.Value  
                                         Order By DBNullToStr(Datos.Item("RECEP_NOM"))
                                         Select New ListItem(DBNullToStr(Datos.Item("RECEP_NOM")) & " " & DBNullToStr(Datos.Item("RECEP_APE")), DBNullToStr(Datos.Item("RECEPTOR"))) Distinct.ToList()

        Return query
    End Function
	''' <summary>
	''' Función que devuelve un objeto List con los Estados de Factura existentes en la aplicación.
	''' </summary>
	''' <returns>Función que devuelve un objeto List con los Estados de Factura existentes en la aplicación.</returns>
	''' <remarks>
	''' Llamada desde: EpWeb.Seguimientoaspx.vb.InicializarControles</remarks>
	Private Function EstadosFra() As List(Of ListItem)
        Dim oFacturas As CFacturas = Me.FSNServer.Get_Object(GetType(FSNServer.CFacturas))
        Dim dsEstadosFactura As DataSet
        dsEstadosFactura = oFacturas.CargarEstadosFactura(CType(Usuario.Idioma, FSNLibrary.Idioma))
        Dim query As List(Of ListItem)
        query = From Datos In dsEstadosFactura.Tables(0)
                Where Not Datos.Item("ID") Is System.DBNull.Value And Not Datos.Item("DEN") Is System.DBNull.Value
                Order By Datos.Item("DEN") Ascending
                Select New ListItem(Datos.Item("DEN").ToString, Datos.Item("ID").ToString) Distinct.ToList()
        Return query
    End Function
    ''' <summary>
    ''' Función que devuelve un objeto List con los Estados de Pago existentes en la aplicación.
    ''' </summary>
    ''' <returns>Función que devuelve un objeto List con los Estados de Pago existentes en la aplicación.</returns>
    ''' <remarks>
    ''' Llamada desde: EpWeb.Seguimientoaspx.vb.InicializarControles</remarks>
    Private Function EstadosPago() As List(Of ListItem)
        Dim oPagos As CPagos = Me.FSNServer.Get_Object(GetType(FSNServer.CPagos))
        Dim dsEstadosPago As DataSet
        dsEstadosPago = oPagos.CargarEstadosPago(CType(Usuario.Idioma, FSNLibrary.Idioma))
        Dim query As List(Of ListItem)
        query = From Datos In dsEstadosPago.Tables(0)
                Where Not Datos.Item("ID") Is System.DBNull.Value And Not Datos.Item("DEN") Is System.DBNull.Value
                Order By Datos.Item("DEN") Ascending
                Select New ListItem(Datos.Item("DEN").ToString, Datos.Item("ID").ToString) Distinct.ToList()
        Return query
    End Function
    ''' Revisado por: blp. Fecha: 18/04/2012
    ''' <summary>
    ''' Genera una lista con todos los tipos de pedidos existentes
    ''' </summary>
    ''' <returns>Una lista de ListItem</returns>
    ''' <remarks>Llamada desde InicializarControles. Máximo 0,1 seg.</remarks>
    Private Function ListaTiposPedidos() As List(Of ListItem)
        If HttpContext.Current.Cache("DsetSeguimientoTiposPedidos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
            Dim oTiposPedido As cTiposPedidos = FSNServer.Get_Object(GetType(cTiposPedidos))
            oTiposPedido.CargarTiposPedido(Usuario.Idioma)
            Dim query As List(Of ListItem) = From TiposPedido As cTipoPedido In oTiposPedido
                                             Select New ListItem(TiposPedido.Denominacion, TiposPedido.Id) Distinct.ToList()
            Me.InsertarEnCache("DsetSeguimientoTiposPedidos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString(), query)
            Return query
        Else
            Return HttpContext.Current.Cache("DsetSeguimientoTiposPedidos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
        End If
    End Function
    ''' Revisado por: blp. Fecha: 18/04/2012
    ''' <summary>
    ''' Carga los años
    ''' </summary>
    ''' <returns>Lista con años </returns>
    ''' <remarks>Llamada desde InicializarControles. Máximo 0,1 seg.</remarks>
    Private Function Anyos() As List(Of ListItem)
        Dim lsAnyos As New List(Of ListItem)
        For i As Integer = Year(Now) To AnyoMin Step -1
            Dim oListItem As New ListItem
            oListItem.Value = i
            oListItem.Text = i
            lsAnyos.Add(oListItem)
        Next
        Return lsAnyos
    End Function
    ''' Revisado por: blp. Fecha: 18/04/2012
    ''' <summary>
    ''' Genera una lista con los gestores existentes en la lista de pedidos
    ''' </summary>
    ''' <param name="dsInfo">
    ''' Podemos pasarle el dataset desde es que recupera los datos o dejar que los tome del DsetPedidos
    ''' </param>
    ''' <returns>Una lista de ListItem</returns>
    ''' <remarks>Llamada desde InicializarControles. Máximo 0,1 seg.</remarks>
    Private Function ListaGestores(Optional ByVal dsInfo As DataSet = Nothing) As DataTable
        If dsInfo Is Nothing OrElse dsInfo.Tables.Count = 0 Then
            dsInfo = getDropDownsInfo(FSNUser.CodPersona, FSNUser.Idioma)
        End If
        Dim query = From Datos In dsInfo.Tables("GESTORES")
                    Select Datos.Item("GESTOR") Distinct

        Dim oPersonas As CPersonas = Me.FSNServer.Get_Object(GetType(FSNServer.CPersonas))
        Dim dtCodGestores As DataTable = oPersonas.CrearTablaGestores()
        If query.Any Then
            For Each gestorcod In query
                dtCodGestores.Rows.Add({gestorcod})
            Next
        End If
        Dim dtGestores As DataTable = oPersonas.CargarGestores(dtCodGestores)
        Return dtGestores
    End Function
#End Region
#Region "Filtros"
    <Serializable()>
    Public Enum TipoFiltro As Integer
        Anio = 0
        Cesta = 1
        NumPedido = 2
        NumPedidoProve = 3
        NumPedidoERP = 4
        Articulo = 5
        FechaDesde = 6
        FechaHasta = 7
        Empresa = 8
        Proveedor = 9
        ProveedorERP = 10
        Estado = 11
        Tipo = 12
        Categoria = 13
        Receptor = 14
        ID = 15
        CentroCoste = 16
        PartidaPresupuestaria = 17
        FraAnio = 18
        FraNum = 19
        FraEstado = 20
        FraFechaDesde = 21
        FraFechaHasta = 22
        PagoNum = 23
        PagoEstado = 24
        PagoFechaDesde = 25
        PagoFechaHasta = 26
        TipoPedido = 27
        Abono = 28
        NumMeses = 29
        PedidosConAlbaranesBloqueados = 30
        Gestor = 31
        Usu_Ped = 32
        CC_Ped = 33
        incluirPedidosContraAbierto = 34
        AnyoPedAbierto = 35
        CestaPedAbierto = 36
        PedidoPedAbierto = 37
        PedidoBorrado = 38
    End Enum
    <Serializable()>
    Public Structure Filtro
        Dim _tipo As TipoFiltro
        Dim _valor As String
        Dim _valores() As Boolean
        Dim _valorfecha As DateTime
        Dim _filterInfo As String
        Dim _listavalor As List(Of String)
        Public Property tipo() As TipoFiltro
            Get
                Return _tipo
            End Get
            Set(ByVal value As TipoFiltro)
                _tipo = value
            End Set
        End Property
        Public Property Valor() As String
            Get
                Return _valor
            End Get
            Set(ByVal value As String)
                _valor = value
            End Set
        End Property
        Public Property Valores() As Boolean()
            Get
                Return _valores
            End Get
            Set(ByVal value As Boolean())
                _valores = value
            End Set
        End Property
        Public Property ValorFecha() As DateTime
            Get
                Return _valorfecha
            End Get
            Set(ByVal value As DateTime)
                _valorfecha = value
            End Set
        End Property
        ''' <summary>
        ''' Propiedad del filtro que nos indica cualquier dato adicional que convenga conocer del filtro.
        ''' En el caso de las partidas presupuestarias, sirve para guardar la partida de nivel 0 / Nivel al que corresponde el filtro
        ''' (dado que puede haber más de un filtro de partida presuepuestaria)
        ''' </summary>
        Public Property Info() As String
            Get
                Return _filterInfo
            End Get
            Set(ByVal value As String)
                _filterInfo = value
            End Set
        End Property

        Public Property ListaValor() As List(Of String)
            Get
                Return _listavalor
            End Get
            Set(ByVal value As List(Of String))
                _listavalor = value
            End Set
        End Property
        Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As String)
            _tipo = Tipo
            _valor = Valor
        End Sub
        Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As DateTime)
            _tipo = Tipo
            _valorfecha = Valor
        End Sub
        Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As Boolean())
            _tipo = Tipo
            _valores = Valor
        End Sub
        Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As Boolean)
            _tipo = Tipo
            _valor = Valor
        End Sub
        Public Sub New(ByVal Tipo As TipoFiltro, ByVal ListaValor As List(Of String))
            _tipo = Tipo
            _listavalor = ListaValor
        End Sub
        ''' <summary>
        ''' Crear un filtro nuevo
        ''' </summary>
        ''' <param name="Tipo">Tipo de filtro</param>
        ''' <param name="Valor">Valor tipo string del filtro creado</param>
        ''' <param name="Info">Info adicional que se desee conservar del filtro</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As String, ByVal Info As String)
            _tipo = Tipo
            _valor = Valor
            _filterInfo = Info
        End Sub
    End Structure
    Public Property FiltrosAnyadidos() As List(Of Filtro)
        Get
            If ViewState("FiltrosAnyadidos") Is Nothing Then
                Return New List(Of Filtro)
            Else
                Return CType(ViewState("FiltrosAnyadidos"), List(Of Filtro))
            End If
        End Get
        Set(ByVal value As List(Of Filtro))
            ViewState("FiltrosAnyadidos") = value
        End Set
    End Property
    ''' Revisado por: blp. Fecha:16/11/2011
    ''' <summary>
    ''' Procedimiento que añade los filtros que se hayan seleccionado en el panel buscador a la propiedad FiltrosAnyadidos
    ''' de la página Seguimiento.aspx.vb
    ''' </summary>
    ''' <remarks>LLamada desde el propio objeto: btnBuscar_Click, dlOrdenes_itemCommand, Page_Load
    ''' Tiempo maximo 1 sec</remarks>
    Private Sub ActualizarFiltros()
        Dim lis As New List(Of Filtro)
        If pnlBusqueda.Visible Then
            'Año
            If Not String.IsNullOrEmpty(ddlAnio.SelectedValue) Then _
                lis.Add(New Filtro(TipoFiltro.Anio, ddlAnio.SelectedValue))
            'Cesta
            If Not String.IsNullOrEmpty(txtCesta.Text) AndAlso IsNumeric(txtCesta.Text) Then _
                lis.Add(New Filtro(TipoFiltro.Cesta, txtCesta.Text))
            'Nº pedido
            If Not String.IsNullOrEmpty(txtPedido.Text) Then
                Dim itxtPedido As Integer
                Try
                    itxtPedido = CType(txtPedido.Text, Integer)
                Catch ex As Exception
                    itxtPedido = -1
                End Try
                lis.Add(New Filtro(TipoFiltro.NumPedido, CType(itxtPedido, String)))
            End If
            'Año
            If Not String.IsNullOrEmpty(ddlAnyoAbierto.SelectedValue) Then _
                lis.Add(New Filtro(TipoFiltro.AnyoPedAbierto, ddlAnyoAbierto.SelectedValue))
            'Cesta
            If Not String.IsNullOrEmpty(txtCestaAbierto.Text) AndAlso IsNumeric(Me.txtCestaAbierto.Text) Then _
                lis.Add(New Filtro(TipoFiltro.CestaPedAbierto, txtCestaAbierto.Text))
            'Nº pedido
            If Not String.IsNullOrEmpty(txtPedidoPedAbierto.Text) Then
                Dim itxtPedido As Integer
                Try
                    itxtPedido = CType(txtPedidoPedAbierto.Text, Integer)
                Catch ex As Exception
                    itxtPedido = -1
                End Try
                lis.Add(New Filtro(TipoFiltro.PedidoPedAbierto, CType(itxtPedido, String)))
            End If
            'Nº pedido prove
            If Not String.IsNullOrEmpty(txtPedidoProve.Text) Then _
                lis.Add(New Filtro(TipoFiltro.NumPedidoProve, txtPedidoProve.Text))
            'Nº pedido ERP (SAP)
            If Not String.IsNullOrEmpty(txtPedidoERP.Text) Then _
                lis.Add(New Filtro(TipoFiltro.NumPedidoERP, txtPedidoERP.Text))
            'Artículo
            If Not String.IsNullOrEmpty(txtArticulo.Text) Then _
                lis.Add(New Filtro(TipoFiltro.Articulo, txtArticulo.Text))
            'Fecha desde
            If Not dteDesde.Value Is Nothing Then _
                lis.Add(New Filtro(TipoFiltro.FechaDesde, CType(dteDesde.Value, DateTime)))
            'fecha hasta
            If Not dteHasta.Value Is Nothing Then _
                lis.Add(New Filtro(TipoFiltro.FechaHasta, CType(dteHasta.Value, DateTime)))
            'empresa
            If Not String.IsNullOrEmpty(ddlEmpresa.SelectedValue) Then _
                lis.Add(New Filtro(TipoFiltro.Empresa, ddlEmpresa.SelectedValue))
            'Receptor
            If Not String.IsNullOrEmpty(ddlReceptor.SelectedValue) Then _
                lis.Add(New Filtro(TipoFiltro.Receptor, ddlReceptor.SelectedValue))
            'Proveedor
            If Not String.IsNullOrEmpty(txtProveedor.Text) AndAlso Not String.IsNullOrEmpty(hidProveedor.Value) Then _
                lis.Add(New Filtro(TipoFiltro.Proveedor, hidProveedor.Value))
            'ProveedorERP
            If Not String.IsNullOrEmpty(txtProveedorERP.Text) Then _
                lis.Add(New Filtro(TipoFiltro.ProveedorERP, txtProveedorERP.Text))
            'Centro Coste
            If Not String.IsNullOrEmpty(tbCtroCoste.Text) Then
                If Not String.IsNullOrEmpty(tbCtroCoste_Hidden.Value) Then
                    lis.Add(New Filtro(TipoFiltro.CentroCoste, tbCtroCoste_Hidden.Value, tbCtroCoste.Text))
                End If
            End If
            'Partidas presupuestarias
            For Each tabla As Control In phrPartidasPresBuscador.Controls
                If TypeOf tabla Is Table Then
                    For Each fila As Control In CType(tabla, Table).Rows
                        For Each celda As Control In CType(fila, TableRow).Cells
                            For Each tb As Control In CType(celda, TableCell).Controls
                                If TypeOf tb Is TextBox Then
                                    Dim tbPartida As TextBox = CType(tb, System.Web.UI.WebControls.TextBox)
                                    If Not String.IsNullOrEmpty(tbPartida.Text) Then
                                        Dim sPartidaHiddenID As String = tbPartida.ID.Replace("tbPPres_", "tbPPres_Hidden_")
                                        Dim tbPartidaHidden As HiddenField = CType(CType(celda, TableCell).FindControl(sPartidaHiddenID), System.Web.UI.WebControls.HiddenField)
                                        Dim sPRES0 As String = (tb.ID).Replace("tbPPres_", "")
                                        Dim sPartidaDen As String = tbPartida.Text.ToString
                                        If tbPartidaHidden IsNot Nothing AndAlso Not String.IsNullOrEmpty(tbPartidaHidden.Value) Then
                                            lis.Add(New Filtro(
                                                            TipoFiltro.PartidaPresupuestaria,
                                                            tbPartidaHidden.Value,
                                                            sPRES0 & "@@@" & sPartidaDen
                                                    ))
                                        End If
                                    End If
                                ElseIf TypeOf tb Is System.Web.UI.WebControls.Label Then
                                    For Each tb2 As Control In CType(tb, Label).Controls
                                        If TypeOf tb2 Is System.Web.UI.WebControls.TextBox Then
                                            Dim tbPartida As TextBox = CType(tb2, System.Web.UI.WebControls.TextBox)
                                            If Not String.IsNullOrEmpty(tbPartida.Text) Then
                                                Dim sPartidaHiddenID As String = tbPartida.ID.Replace("tbPPres_", "tbPPres_Hidden_")
                                                Dim tbPartidaHidden As HiddenField = CType(CType(tb, Label).FindControl(sPartidaHiddenID), System.Web.UI.WebControls.HiddenField)
                                                Dim sPRES0 As String = (tb2.ID).Replace("tbPPres_", "")
                                                Dim sPartidaDen As String = tbPartida.Text.ToString
                                                If tbPartidaHidden IsNot Nothing AndAlso Not String.IsNullOrEmpty(tbPartidaHidden.Value) Then
                                                    lis.Add(New Filtro(
                                                                    TipoFiltro.PartidaPresupuestaria,
                                                                    tbPartidaHidden.Value,
                                                                    sPRES0 & "@@@" & sPartidaDen
                                                            ))
                                                End If
                                            End If
                                        End If
                                    Next
                                End If
                            Next
                        Next
                    Next
                End If
            Next

            'Tipo de pedido
            If Not String.IsNullOrEmpty(ddlTipoPedido.SelectedValue) Then _
                lis.Add(New Filtro(TipoFiltro.TipoPedido, ddlTipoPedido.SelectedValue))
            'Numero de meses
            If Not igNumMeses.Value Is Nothing AndAlso igNumMeses.Value >= 1 Then _
                lis.Add(New Filtro(TipoFiltro.NumMeses, igNumMeses.Value.ToString))
            'Estados
            Dim iEst As CPedido.Estado
            Dim iMostrarPedidos(6) As Boolean
            Dim iMostrarPedidosNEG As Boolean = True 'Pedidos Negociados -> GS
            Dim iMostrarPedidosEP As Boolean = True
            Dim iMostrarPedidosEPLIBRES As Boolean = True
            Dim iMostrarPedidosERP As Boolean = True
            Dim iMostrarAbonos As Boolean = False
            Dim iMostrarContraAbiertos As Boolean = False
            Dim iMostrarPedidosExpress As Boolean = False

            For Each l As ListItem In chklstEstado.Items
                If l.Selected Then
                    iEst = iEst Or CType(l.Value, CPedido.Estado)
                End If
            Next
			If iEst = CPedido.Estado.SinEstado And chkBorrado.Checked = False Then
				For Each l As ListItem In chklstEstado.Items
					l.Selected = True
				Next
				iEst = MaxEstadosValue
			End If
			lis.Add(New Filtro(TipoFiltro.Estado, CType(iEst, Integer).ToString()))

			Session("filtroEstadoEP") = iEst
			'Clases de pedido
			For Each l As ListItem In chklstOtrosFiltros.Items
				If l.Selected AndAlso l.Value = CPedido.OtrosAtributosPedido.Abono Then
					iMostrarAbonos = True
				ElseIf l.Selected = False AndAlso l.Value = CPedido.OtrosAtributosPedido.CatalogadoNegociado Then
					iMostrarPedidosEP = False
				ElseIf l.Selected = False AndAlso l.Value = CPedido.OtrosAtributosPedido.CatalogadoLibres Then
					iMostrarPedidosEPLIBRES = False
				ElseIf l.Selected = False AndAlso l.Value = CPedido.OtrosAtributosPedido.Negociado Then
					iMostrarPedidosNEG = False
				ElseIf (l.Selected = False AndAlso l.Value = CPedido.OtrosAtributosPedido.Directos) _
					OrElse Not Session("HayIntegracionEntradaoEntradaSalida") Then
					iMostrarPedidosERP = False
				End If
				If l.Value = CPedido.OtrosAtributosPedido.ContraAbiertos Then
					iMostrarContraAbiertos = l.Selected
				End If
				If l.Value = CPedido.OtrosAtributosPedido.Express Then
					iMostrarPedidosExpress = l.Selected
				End If
			Next
			'Gestionar la visibilidad de filtros de categorías y de pedidos abiertos
			If Acceso.gbUsarPedidosAbiertos Then
				If Not chklstOtrosFiltros.Items.FindByValue(CPedido.OtrosAtributosPedido.ContraAbiertos).Selected Then
					trPedidosAbiertos.Attributes.Add("style", "display:none")
				End If
			End If
			If Not (chklstOtrosFiltros.Items.FindByValue(CPedido.OtrosAtributosPedido.CatalogadoNegociado).Selected _
					Or chklstOtrosFiltros.Items.FindByValue(CPedido.OtrosAtributosPedido.CatalogadoLibres).Selected) Then
				tdCategorias.Attributes.Add("style", "display:none")
			End If

			iMostrarPedidos(0) = iMostrarPedidosNEG
			iMostrarPedidos(1) = iMostrarPedidosEP
			iMostrarPedidos(2) = iMostrarPedidosEPLIBRES
			iMostrarPedidos(3) = iMostrarPedidosERP
			iMostrarPedidos(4) = iMostrarAbonos
			iMostrarPedidos(5) = iMostrarContraAbiertos
			iMostrarPedidos(6) = iMostrarPedidosExpress
			lis.Add(New Filtro(TipoFiltro.Tipo, iMostrarPedidos))
			'Facturas
			If Not String.IsNullOrEmpty(txtAnioFra.Text) Then _
				lis.Add(New Filtro(TipoFiltro.FraAnio, txtAnioFra.Text))
			If Not String.IsNullOrEmpty(txtNumFra.Text) Then _
				lis.Add(New Filtro(TipoFiltro.FraNum, txtNumFra.Text))
			If Not String.IsNullOrEmpty(ddlEstadoFra.SelectedValue) Then _
				lis.Add(New Filtro(TipoFiltro.FraEstado, ddlEstadoFra.SelectedValue))
			If Not dteFraFDesde.Value Is Nothing Then _
				lis.Add(New Filtro(TipoFiltro.FraFechaDesde, CType(dteFraFDesde.Value, DateTime)))
			If Not dteFraFHasta.Value Is Nothing Then _
				lis.Add(New Filtro(TipoFiltro.FraFechaHasta, CType(dteFraFHasta.Value, DateTime)))
			'Pagos
			If Not String.IsNullOrEmpty(txtNumPago.Text) Then _
				lis.Add(New Filtro(TipoFiltro.PagoNum, txtNumPago.Text))
			If Not String.IsNullOrEmpty(ddlEstadoPago.SelectedValue) Then _
				lis.Add(New Filtro(TipoFiltro.PagoEstado, ddlEstadoPago.SelectedValue))
			If Not dtePagoFDesde.Value Is Nothing Then _
				lis.Add(New Filtro(TipoFiltro.PagoFechaDesde, CType(dtePagoFDesde.Value, DateTime)))
			If Not dtePagoFHasta.Value Is Nothing Then _
				lis.Add(New Filtro(TipoFiltro.PagoFechaHasta, CType(dtePagoFHasta.Value, DateTime)))
			'Categorías
			If Not String.IsNullOrEmpty(fsnTvwCategorias.SelectedValue) Then _
					lis.Add(New Filtro(TipoFiltro.Categoria, fsnTvwCategorias.SelectedValue, fsnTvwCategorias.SelectedNode.ValuePath))
			'Pedidos con albaranes bloquados
			If chkPedidosBloqueados.Checked Then
				lis.Add(New Filtro(TipoFiltro.PedidosConAlbaranesBloqueados, True))
			End If
			'Pedidos borrados
			If chkBorrado.Checked Then
				lis.Add(New Filtro(TipoFiltro.PedidoBorrado, True))
			End If
			'Gestor
			If Not String.IsNullOrEmpty(ddlGestor.SelectedValue) Then _
				lis.Add(New Filtro(TipoFiltro.Gestor, ddlGestor.SelectedValue))
			Dim bVerPedidosUsuario As Boolean
			Dim bVerPedidosCCoGestor As Boolean
			If Acceso.gbAccesoFSSM AndAlso FSNUser.PermisoVerPedidosCCImputables Then
				bVerPedidosUsuario = chkSeguimientoPedidos.Items(0).Selected
				bVerPedidosCCoGestor = chkSeguimientoPedidos.Items(1).Selected
			Else
				bVerPedidosUsuario = True
				bVerPedidosCCoGestor = False
			End If
			'Usu_Ped
			lis.Add(New Filtro(TipoFiltro.Usu_Ped, bVerPedidosUsuario))
			'CC_Ped
			lis.Add(New Filtro(TipoFiltro.CC_Ped, bVerPedidosCCoGestor))
		End If
		FiltrosAnyadidos = lis
	End Sub
	''' Revisado por: blp. Fecha: 14/11/2011
	''' <summary>
	''' Actualiza la grid con los criterios de busqueda seleccionados en los filtros
	''' </summary>
	''' <param name="sender">del evento. el propio objeto</param>
	''' <param name="e">del evento</param>        
	''' <remarks>Tiempo máximo:1,2seg.</remarks>
	Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
		HttpContext.Current.Cache.Remove("DsetTodosPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())

		ActualizarFiltros()
		GuardarEnCookieFiltrosBusqueda()
		EliminarCachedePedidos()
		PageNumber = 1
		Dim bActualizarFiltros As Boolean = True
		recargarGrid(PageNumber, True, True, bActualizarFiltros)

		RellenarLabelFiltros()
	End Sub
	Private Sub chklstEstado_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chklstEstado.SelectedIndexChanged
		Dim sIndex As String = Request.Form("__EVENTTARGET").Substring(Request.Form("__EVENTTARGET").LastIndexOf("$") + 1)
		If IsNumeric(sIndex) Then
			Dim l As ListItem = CType(sender, CheckBoxList).Items(CType(sIndex, Integer))
			For Each i As ListItem In CType(sender, CheckBoxList).Items
				If l.Selected Then
					If (CType(i.Value, CPedido.Estado) And CType(l.Value, CPedido.Estado)) = CType(i.Value, CPedido.Estado) Then _
						i.Selected = True
				Else
					If (CType(i.Value, CPedido.Estado) And CType(l.Value, CPedido.Estado)) = CType(l.Value, CPedido.Estado) Then _
						i.Selected = False
				End If
			Next
		End If
	End Sub
#End Region
#Region "Ordenación"
	Private Property CampoOrden() As String
		Get
			If String.IsNullOrEmpty(ViewState.Item("CampoOrden")) Then
				Return "FECHA"
			Else
				Return ViewState.Item("CampoOrden")
			End If
		End Get
		Set(ByVal value As String)
			ViewState.Item("CampoOrden") = value
		End Set
	End Property
	Private Property SentidoOrdenacion() As String
		Get
			If String.IsNullOrEmpty(ViewState.Item("SentidoOrdenacion")) Then
				Return "DESC"
			Else
				Return ViewState.Item("SentidoOrdenacion")
			End If
		End Get
		Set(ByVal value As String)
			ViewState.Item("SentidoOrdenacion") = value
		End Set
	End Property
#End Region
#Region "Paginador"
	''' Revisado por:blp. Fecha: 04/12/2012
	''' <summary>
	''' Función que captura el evento del paginador de cambio de orden
	''' </summary>
	''' <param name="Campo">Campo por el que se ordena</param>
	''' <param name="Sentido">Sentido en el que se ordena</param>
	''' <remarks>Llamada desde el evento. Máximo 0,5 seg.</remarks>
	Public Sub Paginador_OnCambioOrden(ByVal Campo As String, ByVal Sentido As String)
		Dim bActualizar As Boolean = False
		If Campo <> CampoOrden Then
			CampoOrden = Campo
			Dim cookie As HttpCookie = Request.Cookies("SEGUIMIENTO_CRIT_ORD")
			If cookie Is Nothing Then
				cookie = New HttpCookie("SEGUIMIENTO_CRIT_ORD", CampoOrden)
			Else
				cookie.Value = CampoOrden
			End If
			cookie.Expires = DateTime.Now.AddDays(30)
			Response.AppendCookie(cookie)
			bActualizar = True
		End If
		If Sentido <> SentidoOrdenacion Then
			SentidoOrdenacion = Sentido
			Dim cookie As HttpCookie = Request.Cookies("SEGUIMIENTO_CRIT_DIREC")
			If cookie Is Nothing Then
				cookie = New HttpCookie("SEGUIMIENTO_CRIT_DIREC", SentidoOrdenacion)
			Else
				cookie.Value = SentidoOrdenacion
			End If
			cookie.Expires = DateTime.Now.AddDays(30)
			Response.AppendCookie(cookie)
			bActualizar = True
		End If
		If bActualizar Then
			HttpContext.Current.Cache.Remove("DsetTodosPedidosSeguimiento_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())

			EliminarCachedePedidos()
			Dim PageNumber As Integer = 1
			Dim bActualizarFiltros As Boolean = False
			recargarGrid(PageNumber, True, True, bActualizarFiltros)
		End If
	End Sub
	''' Revisado por:blp. Fecha: 04/12/2012
	''' <summary>
	''' Función que captura el evento del paginador de exportar
	''' </summary>
	''' <remarks>Llamada desde el evento. Máximo 0,5 seg.</remarks>
	Public Sub Paginador_Exportar()

		Me.prepararOrdenesExportacion(ObtenerDataSetReport())
		ScriptManager.RegisterStartupScript(Me, Me.GetType, "Exportar_ViewerSinMenu", "window.open('InformeExcelOrdenes.aspx?excel=1' ,'_blank','height=300,width=300,menubar=no,scrollbars=no,resizable=no,toolbar=no,addressbar=no');", True)

	End Sub
	''' Revisado por:blp. Fecha: 04/12/2012
	''' <summary>
	''' Función que captura el evento del paginador de Actualizar el grid de órdenes
	''' </summary>
	''' <param name="iPageNumber">Número de página en la que nos encontramos</param>
	''' <remarks>Llamada desde el evento. Máximo 0,5 seg.</remarks>
	Public Sub Paginador_ActualizarGrid(ByVal iPageNumber As Integer)
		EliminarCachedePedidos()
		PageNumber = iPageNumber
		Dim bActualizarFiltros As Boolean = False
		recargarGrid(iPageNumber, True, True, bActualizarFiltros)
	End Sub
#End Region
#Region "DataList Pedidos"
	''' Revisado por:blp. Fecha: 04/12/2012
	''' <summary>
	''' Operaciones para la paginación y la visibilidad en el evento DataBinding
	''' </summary>
	''' <param name="sender">control que lanza el evento</param>
	''' <param name="e">argumentos del evento</param>
	''' <remarks>Llamada desde el evento. Máximo 0,3 seg.</remarks>
	Private Sub dlOrdenes_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlOrdenes.DataBinding
		dlOrdenes.Visible = TotalRegistros > 0
		litNoPedidos.Visible = Not dlOrdenes.Visible
	End Sub
	''' Revisado por:blp. Fecha: 04/12/2012
	''' <summary>
	''' Evento que se lanza al pinchar sobre los controles tipo command del datalist de ordenes
	''' </summary>
	''' <param name="source">los objetos command</param>
	''' <param name="e">argumentos de evento</param>
	''' <remarks>LLamada desde el propio objeto
	''' Tiempo maximo 1 sec</remarks>
	Private Sub dlOrdenes_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlOrdenes.ItemCommand
		Select Case e.CommandName
			Case "Adjuntos"
				Dim dr As DataRow() = CargarAdjuntosOrden(e.CommandArgument)
				pnlAdjuntosLin.Visible = False
				grdAdjuntos.DataSource = dr
				grdAdjuntos.DataBind()
				updpnlPopupAdjuntos.Update()
				mpeAdj.Show()
			Case "Exportar"
				If e.CommandArgument.ToString = "" Then
					Me.prepararOrdenesExportacion(ObtenerDataSetReport)
				Else
					Me.prepararOrdenesExportacion(e.CommandArgument.ToString)
				End If
				ScriptManager.RegisterStartupScript(Me, Me.GetType, "Exportar_ViewerSinMenu", "window.open('InformeExcelOrdenes.aspx?excel=1' ,'_blank','height=300,width=300,menubar=no,scrollbars=no,resizable=no,toolbar=no,addressbar=no');", True)
			Case "Recepciones"
				lblTituloalbaranes.Text = Textos(251)
				ImgBtnCerrarpnlAlbaranes.OnClientClick = "$find('" & mpeAlbaranes.ClientID & "').hide(); return false"
				Dim oRecepciones As CRecepciones = Me.FSNServer.Get_Object(GetType(FSNServer.CRecepciones))
				'Ahora el datasource del dlALbaranes se obtiene del get de la propiedad DsetAlbaranes
				dlAlbaranes.Attributes.Add("IdOrden", e.CommandArgument)
				dlAlbaranes.DataSource = DsetAlbaranes(e.CommandArgument).Tables("ALBARANES")
				Dim NumPedido As String = DsetAlbaranes(e.CommandArgument).Tables("ALBARANES").Rows(0).Item("ANYO") & "/" &
				DsetAlbaranes(e.CommandArgument).Tables("ALBARANES").Rows(0).Item("NUMPEDIDO") & "/" & DsetAlbaranes(e.CommandArgument).Tables("ALBARANES").Rows(0).Item("NUMORDEN")
				lblTituloalbaranes.Text += " " & NumPedido
				dlAlbaranes.DataBind()
				updpnlPopupAlbaranesCab.Update()
				updpnlPopupAlbaranes.Update()
				mpeAlbaranes.Show()
		End Select
	End Sub
	''' Revisado por: blp. Fecha:08/03/2012
	''' <summary>
	''' Método que se lanza al cargarse los items en el datalist de ordenes
	''' </summary>
	''' <param name="sender">el datalist</param>
	''' <param name="e">Argumentos del evento</param>
	''' <remarks>Llamada desde el propio objeto datalist
	''' Tiempo maximo 1 sec</remarks>
	Private Sub dlOrdenes_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlOrdenes.ItemDataBound
		With e.Item
			Dim fila As DataRowView = CType(.DataItem, DataRowView)
			Select Case .ItemType
				Case ListItemType.Header
					Dim pagds As PaginadorBDD = CType(.FindControl("pagdsCabecera"), PaginadorBDD)
					Dim li As New ListItemCollection
					li.Add(New ListItem(Me.Textos(140), "FECHA"))
					li.Add(New ListItem(Me.Textos(96), "EMPRESA"))
					li.Add(New ListItem(Me.Textos(26), "PROVEEDOR"))
					li.Add(New ListItem(Me.Textos(67), "IMPORTE"))
					li.Add(New ListItem(Me.Textos(187), "PETICIONARIO"))
					If Acceso.gbOblCodPedido Then
						Dim oCParametros As FSNServer.CParametros = FSNServer.Get_Object(GetType(FSNServer.CParametros))
						li.Add(New ListItem(oCParametros.CargarLiteralParametros(31, Usuario.Idioma), "REF_FACTURA"))
						CType(.FindControl("litCabPedidoERP"), Literal).Text = oCParametros.CargarLiteralParametros(31, Usuario.Idioma)
					Else
						CType(.FindControl("tblCabecera"), Table).Rows(1).Cells(3).Visible = False
					End If
					If Acceso.gbTrasladoAProvERP Then
						li.Add(New ListItem(Textos(98), "PROVE_ERP"))
					End If
					li.Add(New ListItem(Me.Textos(175), "TIPO_PEDIDO"))
					li.Add(New ListItem(Me.Textos(44), "ESTADO"))

					pagds.ListOrden = li
					pagds.CampoOrden = CampoOrden
					pagds.SentidoOrdenacion = SentidoOrdenacion
					pagds.InitPaginador(TotalRegistros, PageSize, PageNumber)

					CType(.FindControl("litCabEmpresa"), Literal).Text = Textos(96)
					CType(.FindControl("litCabPedido"), Literal).Text = Textos(186)
					If Acceso.gbVerNumeroProveedor Then
						CType(.FindControl("litCabPedidoProve"), Literal).Text = Textos(73)
					Else
						CType(.FindControl("tblCabecera"), Table).Rows(1).Cells(4).Visible = False
					End If
					CType(.FindControl("litCabProveedor"), Literal).Text = Textos(26)

					If Acceso.gbTrasladoAProvERP And HayIntegracionPedidos Then
						CType(.FindControl("litCabProveedorERP"), Literal).Text = Textos(98)
					Else
						CType(.FindControl("tblCabecera"), Table).Rows(1).Cells(6).Visible = False
					End If
					CType(.FindControl("litCabImporte"), Literal).Text = Textos(67)
					CType(.FindControl("litCabTipoPedido"), Literal).Text = Textos(175)
					CType(.FindControl("litCabEstado"), Literal).Text = Textos(44)
					CType(.FindControl("litCabObservacionesProv"), Literal).Text = Textos(111)
					CType(.FindControl("LitCabRecep"), Literal).Text = Textos(158)
				Case ListItemType.Footer
					Dim pagds As PaginadorBDD = CType(.FindControl("pagdsPie"), PaginadorBDD)
					Dim li As New ListItemCollection
					li.Add(New ListItem(Me.Textos(140), "FECHA"))
					li.Add(New ListItem(Me.Textos(96), "EMPRESA"))
					li.Add(New ListItem(Me.Textos(26), "PROVEEDOR"))
					li.Add(New ListItem(Me.Textos(67), "IMPORTE"))
					li.Add(New ListItem(Me.Textos(187), "PETICIONARIO"))
					If Acceso.gbOblCodPedido Then
						Dim oCParametros As FSNServer.CParametros = FSNServer.Get_Object(GetType(FSNServer.CParametros))
						li.Add(New ListItem(oCParametros.CargarLiteralParametros(31, Usuario.Idioma), "REF_FACTURA"))
					End If
					If Acceso.gbTrasladoAProvERP Then
						li.Add(New ListItem(Textos(98), "PROVE_ERP"))
					End If
					li.Add(New ListItem(Me.Textos(175), "TIPO_PEDIDO"))
					li.Add(New ListItem(Me.Textos(44), "ESTADO"))

					pagds.ListOrden = li
					pagds.CampoOrden = CampoOrden
					pagds.SentidoOrdenacion = SentidoOrdenacion
					pagds.InitPaginador(TotalRegistros, PageSize, PageNumber)

				Case ListItemType.Item, ListItemType.AlternatingItem
					CType(.FindControl("lnkVerDetalle"), LinkButton).Text = Textos(268)
					CType(.FindControl("lnkVerDetalle"), LinkButton).OnClientClick = "VerDetalle(" & CType(fila.Item("ID"), Integer) &
								"," & CType(fila.Item("EST"), Integer) &
								",'" & fila.Item("APROV") &
								"','" & fila.Item("RECEPTOR") &
								"','" & fila.Item("ANYO") &
								"','" & fila.Item("NUMPEDIDO") &
								"','" & fila.Item("NUMORDEN") &
								"','" & fila.Item("ACEP_PROVE") &
								"'); return false;"
					CType(.FindControl("btnFilaPDF"), ImageButton).OnClientClick = "VerDetalleExp(" & CType(fila.Item("ID"), Integer) &
								"," & CType(fila.Item("EST"), Integer) &
								",'" & fila.Item("APROV") &
								"','" & fila.Item("RECEPTOR") &
								"'); return false;"
					Dim tipoPedido As String
					Select Case fila.Item("TIPO")
						Case TiposDeDatos.TiposPedido.GS
							tipoPedido = "GS "
						Case TiposDeDatos.TiposPedido.ERP
							tipoPedido = "ERP "
						Case Else
							tipoPedido = ""
					End Select
					CType(.FindControl("litFilaPedido"), Literal).Text = tipoPedido &
						fila.Item("ANYO") & "/" & fila.Item("NUMPEDIDO") & "/" & fila.Item("NUMORDEN") &
						"<br/>" & FSNLibrary.FormatDate(fila.Item("FECHA"), Me.Usuario.DateFormat)
					If Acceso.gbOblCodPedido Then
						CType(.FindControl("litFilaPedidoERP"), Literal).Text = DBNullToStr(fila.Item("REFERENCIA"))
					Else
						CType(.FindControl("tblItem"), Table).Rows(0).Cells(3).Visible = False
					End If
					If Not Acceso.gbVerNumeroProveedor Then
						CType(.FindControl("tblItem"), Table).Rows(0).Cells(4).Visible = False
					End If
					CType(.FindControl("fsnlnkFilaProveedor"), FSNWebControls.FSNLinkInfo).Text =
						IIf(Me.FSNUser.MostrarCodProve, fila.Item("PROVECOD") & " - ", "") & fila.Item("PROVEDEN")

					If Acceso.gbTrasladoAProvERP And HayIntegracionPedidos Then
						CType(.FindControl("litFilaProveedorERP"), Literal).Text = DBNullToStr(fila.Item("COD_PROVE_ERP"))
					Else
						CType(.FindControl("tblItem"), Table).Rows(0).Cells(6).Visible = False
					End If
					CType(.FindControl("litFilaImporte"), Label).Text = FSNLibrary.FormatNumber(fila.Item("IMPORTE") * fila.Item("CAMBIO"), Me.Usuario.NumberFormat) & " " & fila.Item("MON")
					CType(.FindControl("lblFilaTipoPedido"), Label).Text = fila.Item("TIPOPEDIDO_DEN")
					Dim sEstado As String = String.Empty
					If fila.Item("BAJA_LOG") = 0 Then
						Select Case CType(fila.Item("EST"), CPedido.Estado)
							Case CPedido.Estado.PendienteAprobar
								sEstado = Textos(31)
							Case CPedido.Estado.DenegadoParcialmente
								sEstado = Textos(32)
							Case CPedido.Estado.EmitidoAlProveedor
								sEstado = Textos(33)
							Case CPedido.Estado.AceptadoPorElProveedor
								sEstado = Textos(34)
							Case CPedido.Estado.EnCamino
								sEstado = Textos(35)
							Case CPedido.Estado.RecibidoParcialmente
								sEstado = Textos(36)
							Case CPedido.Estado.Cerrado
								sEstado = Textos(37)
							Case CPedido.Estado.Anulado
								sEstado = Textos(324)
							Case CPedido.Estado.Rechazado
								sEstado = Textos(39)
							Case CPedido.Estado.Denegado
								sEstado = Textos(40)
						End Select

						Select Case CType(fila.Item("EST"), CPedido.Estado)
							Case CPedido.Estado.DenegadoParcialmente, CPedido.Estado.Anulado, CPedido.Estado.Rechazado, CPedido.Estado.Denegado
								CType(.FindControl("lblFilaEstado"), Label).ForeColor = Drawing.Color.Red
						End Select
						If IsDBNull(fila.Item("FECEST")) OrElse String.IsNullOrEmpty(fila.Item("FECEST")) Then
							CType(.FindControl("lblFilaEstado"), Label).Text = sEstado
						Else
							CType(.FindControl("lblFilaEstado"), Label).Text = sEstado & "<br/>(" & FSNLibrary.FormatDate(fila.Item("FECEST"), Usuario.DateFormat) & ")"
						End If
					Else
						'Es una baja lógica, un pedido borrado
						sEstado = Textos(325)
						If IsDBNull(fila.Item("FEC_BAJA_LOG")) OrElse String.IsNullOrEmpty(fila.Item("FECEST")) Then
							CType(.FindControl("lblFilaEstado"), Label).Text = sEstado
						Else
							CType(.FindControl("lblFilaEstado"), Label).Text = sEstado & "<br/>(" & FSNLibrary.FormatDate(fila.Item("FEC_BAJA_LOG"), Usuario.DateFormat) & ")"
							CType(.FindControl("lblFilaEstado"), Label).CssClass = "LineaPedidoBajaLogica"
						End If
					End If

					If IsDBNull(fila.Item("COMENTPROVE")) OrElse String.IsNullOrEmpty(fila.Item("COMENTPROVE")) Then
						CType(.FindControl("btnFilaObservacionesProv"), ImageButton).Enabled = False
						CType(.FindControl("btnFilaObservacionesProv"), ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, "observaciones_desactivado.gif")
					End If

					If Not IsDBNull(fila.Item("TIENE_RECEP")) And (fila.Item("TIENE_RECEP") = 0) Then
						CType(.FindControl("btnFilarecep"), ImageButton).Enabled = False
						CType(.FindControl("btnFilarecep"), ImageButton).Visible = True
						CType(.FindControl("btnFilarecep"), ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, "recepcion_desactivado.gif")
					End If

					'Tratamiento de abonos
					If fila.Item("ABONO") = 1 Then
						CType(.FindControl("CeldaImporte"), TableCell).BackColor = Drawing.Color.Red
						CType(.FindControl("FilaABONO"), TableRow).Visible = True
						CType(.FindControl("litFilaImporte"), Label).ForeColor = Drawing.Color.White
						CType(.FindControl("litABONO"), Label).Text = Textos(176)
					End If

					If Me.Acceso.g_bAccesoFSFA Then
						If fila.Item("BLOQUEOFACTURACION") > 0 Then
							CType(.FindControl("imgBloquearFacturacionAlbaranOrden"), Image).Visible = True
							CType(.FindControl("imgBloquearFacturacionAlbaranOrden"), Image).Style.Add("float", "left")
							CType(.FindControl("imgBloquearFacturacionAlbaranOrden"), Image).Style.Add("margin-right", "5px")
							CType(.FindControl("lblBloquearFacturacionAlbaranOrden"), Label).Visible = True
							CType(.FindControl("lblBloquearFacturacionAlbaranOrden"), Label).Text = Textos(238)
						End If
					End If
			End Select
		End With
	End Sub
	''' <summary>
	''' Comprueba si las líneas de una orden de entrega han sido modificadas después de ser recuperadas
	''' </summary>
	''' <param name="OrdenID">ID de la orden de entrega</param>
	''' <returns>True si alguna línea ha sido modificada, si no False</returns>
	Public Shared Function PedidoModificado(ByVal OrdenID As Integer, ByVal FechaActualizacion As Long) As Boolean
		Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
		Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
		Dim oOrden As COrden = oServer.Get_Object(GetType(COrden))
		oOrden.ID = OrdenID
		Return oOrden.Modificada(DateTime.FromBinary(FechaActualizacion))
	End Function
	''' <summary>
	''' Comprueba si las líneas de una orden de entrega han sido modificadas después de ser recuperadas
	''' </summary>
	''' <param name="OrdenID">ID de la orden de entrega</param>
	''' <param name="FechaActualizacion">Fecha de actualización de los datos actualmente en caché</param>
	''' <returns>Un string con el ID de la orden y un 1 ó 0 indicando si la orden ha sido modificada(1) o no(0)</returns>
	''' <remarks>Método web preparado para ser llamado desde scripts cliente.</remarks>
	<System.Web.Services.WebMethodAttribute(),
	System.Web.Script.Services.ScriptMethodAttribute()>
	Public Shared Function PedidoModificadoClient(ByVal OrdenID As Integer, ByVal LineaId As Integer, ByVal FechaActualizacion As Long) As String
		If PedidoModificado(OrdenID, FechaActualizacion) Then Return OrdenID & "#" & LineaId & "#1" Else Return OrdenID & "#" & LineaId & "#0"
	End Function
#End Region
#Region "Tooltips"
	''' Revisado por:blp. Fecha: 18/04/2012
	''' <summary>
	''' Devuelve las observaciones de una orden
	''' </summary>
	''' <param name="contextKey">Id de la orden</param>
	''' <returns>String con las observaciones</returns>
	''' <remarks>Llamadas desde Seguimiento.aspx. Máx. 0,1 seg.</remarks>
	<System.Web.Services.WebMethodAttribute(),
		System.Web.Script.Services.ScriptMethodAttribute()>
	Public Shared Function Observaciones(ByVal contextKey As String) As String
		Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
		If oUsuario Is Nothing Then Return Nothing
		Dim dsOrdenes As DataSet = GetCacheDataset()
		If dsOrdenes IsNot Nothing AndAlso dsOrdenes.Tables("ORDENES").Rows.Find(contextKey) IsNot Nothing Then
			Return dsOrdenes.Tables("ORDENES").Rows.Find(contextKey).Item("OBS")
		Else
			Return GetObservacionesOrden(contextKey)
		End If
	End Function
	''' Revisado por: blp. Fecha: 18/04/2012
	''' <summary>
	''' Devuelve las observaciones de una orden
	''' </summary>
	''' <param name="idOrden">Id de la orden</param>
	''' <returns>String Observaciones</returns>
	''' <remarks>Llamada desde Observaciones(). Máx 0,1 seg.</remarks>
	Private Shared Function GetObservacionesOrden(ByVal idOrden As Integer) As String
		Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
		Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
		Dim oOrden As COrden = oServer.Get_Object(GetType(FSNServer.COrden))
		oOrden.ID = idOrden
		Dim sObservaciones As String = oOrden.GetObservaciones()
		Return sObservaciones
	End Function
	''' Revisado por:blp. Fecha: 18/04/2012
	''' <summary>
	''' Devuelve los comentarios de proveedor de una orden
	''' </summary>
	''' <param name="contextKey">Id de la orden</param>
	''' <returns>String con los comentarios de proveedor</returns>
	''' <remarks>Llamadas desde Recepcion.aspx. Máx. 0,1 seg.</remarks>
	<System.Web.Services.WebMethodAttribute(),
	System.Web.Script.Services.ScriptMethodAttribute()>
	Public Shared Function ObservacionesProv(ByVal contextKey As String) As String
		Dim dsOrdenes As DataSet = GetCacheDataset()
		Return GetComentarioProveOrden(contextKey)
	End Function
	''' Revisado por: blp. Fecha: 18/04/2012
	''' <summary>
	''' Devuelve el comentario del proveedor de la orden
	''' </summary>
	''' <param name="idOrden">Id de la orden</param>
	''' <returns>String Comentario del proveedor</returns>
	''' <remarks>Llamada desde ObservacionesProv(). Máx 0,1 seg.</remarks>
	Private Shared Function GetComentarioProveOrden(ByVal idOrden As Integer) As String
		Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
		Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
		Dim oOrden As COrden = oServer.Get_Object(GetType(FSNServer.COrden))
		oOrden.ID = idOrden
		Dim sObservaciones As String = oOrden.GetComentarioProve()
		Return sObservaciones
	End Function
	''' Revisado por:blp. Fecha: 18/04/2012
	''' <summary>
	''' Devuelve las observaciones de una línea
	''' </summary>
	''' <param name="contextKey">Id de la línea</param>
	''' <returns>String con las observaciones</returns>
	''' <remarks>Llamadas desde Recepcion.aspx. Máx. 0,1 seg.</remarks>
	<System.Web.Services.WebMethodAttribute(),
	System.Web.Script.Services.ScriptMethodAttribute()>
	Public Shared Function ObservacionesLinea(ByVal contextKey As String) As String
		Dim dsOrdenes As DataSet = GetCacheDataset()
		If dsOrdenes Is Nothing OrElse dsOrdenes.Tables("LINEASPEDIDO") Is Nothing OrElse dsOrdenes.Tables("LINEASPEDIDO").Rows.Find(contextKey) Is Nothing Then
			Return GetObservacionesLinea(contextKey)
		Else
			Return dsOrdenes.Tables("LINEASPEDIDO").Rows.Find(contextKey).Item("OBS")
		End If
	End Function
	''' Revisado por: blp. Fecha: 18/04/2012
	''' <summary>
	''' Devuelve el comentario del proveedor de la Línea
	''' </summary>
	''' <param name="idLinea">Id de la línea</param>
	''' <returns>String Comentario del proveedor</returns>
	''' <remarks>Llamada desde ObservacionesProv(). Máx 0,1 seg.</remarks>
	Private Shared Function GetObservacionesLinea(ByVal idLinea As Integer) As String
		Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
		Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
		Dim oLinea As CLinea = oServer.Get_Object(GetType(FSNServer.CLinea))
		oLinea.ID = idLinea
		Dim sObservaciones As String = oLinea.GetObservaciones()
		Return sObservaciones
	End Function
	''' Revisado por: blp. Fecha: 03/12/2012
	''' <summary>
	''' Devuelve el comentario del adjunto a la línea o a la orden seleccionado
	''' </summary>
	''' <param name="contextKey">par de datos con el tipo de elemento: línea u orden y su id</param>
	''' <returns>Comentario del adjunto</returns>
	''' <remarks>Llamada desde los elementos de comentario de los adjuntos. Tiempo inferior a 1 seg </remarks>
	<System.Web.Services.WebMethodAttribute(),
	System.Web.Script.Services.ScriptMethodAttribute()>
	Public Shared Function ComentarioAdj(ByVal contextKey As String) As String
		Dim dsOrdenes As DataSet = GetCacheDataset()
		Dim keys() As String = Split(contextKey)
		Dim sTabla As String = String.Empty
		If CType(keys(1), TiposDeDatos.Adjunto.Tipo) = TiposDeDatos.Adjunto.Tipo.Orden_Entrega Then
			sTabla = "ADJUNTOS_ORDEN"
		ElseIf CType(keys(1), TiposDeDatos.Adjunto.Tipo) = TiposDeDatos.Adjunto.Tipo.Linea_Pedido Then
			sTabla = "ADJUNTOS_LINEA"
		End If
		If sTabla <> String.Empty Then
			If dsOrdenes Is Nothing OrElse dsOrdenes.Tables(sTabla) Is Nothing OrElse dsOrdenes.Tables(sTabla).Rows.Find(keys(0)) Is Nothing Then
				Return GetComentariosAdjunto(keys(0), CType(keys(1), TiposDeDatos.Adjunto.Tipo))
			Else
				Return dsOrdenes.Tables(sTabla).Rows.Find(keys(0)).Item("COMENTARIO")
			End If
		Else
			Return String.Empty
		End If
	End Function
	''' Revisado por: blp. Fecha: 18/04/2012
	''' <summary>
	''' Devuelve el comentario del adjunto cuyo id se pasa como parámetro
	''' </summary>
	''' <param name="idAdj">Id del adjunto</param>
	''' <param name="tipoAdjunto">Tipo de adjunto: En este caso puede ser un adjunto de orden o de línea</param>
	''' <returns>String con el Comentario</returns>
	''' <remarks>Llamada desde ComentarioAdj(). Máx 0,1 seg.</remarks>
	Private Shared Function GetComentariosAdjunto(ByVal idAdj As Integer, ByVal tipoAdjunto As TiposDeDatos.Adjunto.Tipo) As String
		Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
		Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
		Dim oAdjunto As Adjunto = oServer.Get_Object(GetType(FSNServer.Adjunto))
		oAdjunto.Id = idAdj
		Dim sObservaciones As String = oAdjunto.GetComentarios(tipoAdjunto)
		Return sObservaciones
	End Function
	''' Revisado por: blp. Fecha: 18/04/2012
	''' <summary>
	''' Devuelve el comentario del adjunto cuyo id se pasa como parámetro
	''' </summary>
	''' <param name="contextkey">contextkey</param>
	''' <returns>String con el Comentario</returns>
	''' <remarks>Llamada desde Seguimiento. Máx 0,1 seg.</remarks>
	<System.Web.Services.WebMethodAttribute(),
	System.Web.Script.Services.ScriptMethodAttribute()>
	Public Shared Function ComentarioLinea(ByVal contextKey As String) As String
		Dim dsOrdenes As DataSet = GetCacheDataset()
		If dsOrdenes Is Nothing OrElse dsOrdenes.Tables("LINEASPEDIDO") Is Nothing OrElse dsOrdenes.Tables("LINEASPEDIDO").Rows.Find(contextKey) Is Nothing Then
			Return String.Empty
		Else
			Return dsOrdenes.Tables("LINEASPEDIDO").Rows.Find(contextKey).Item("COMENT")
		End If
	End Function
	''' Revisado por: blp. Fecha:18/04/2012
	''' <summary>
	''' Recuperamos la cache de pedidos
	''' </summary>
	''' <returns>Dataset de recepciones en caché si existe. Si no, Nothing</returns>
	''' <remarks>Llamada desde Seguimiento.aspx.vb. Máx 0,5 seg.</remarks>
	Private Shared Function GetCacheDataset() As DataSet
		Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
		If oUsuario Is Nothing Then Return Nothing
		Dim dsOrdenes As DataSet = Nothing
		If HttpContext.Current.Cache("DsetPedidosSeguimiento_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString()) IsNot Nothing Then
			dsOrdenes = HttpContext.Current.Cache("DsetPedidosSeguimiento_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString())
		End If
		Return dsOrdenes
	End Function
	<System.Web.Services.WebMethodAttribute(),
	System.Web.Script.Services.ScriptMethodAttribute()>
	Public Shared Function DevolverTexto(ByVal contextKey As String) As String
		Return DBNullToStr(contextKey)
	End Function
#End Region
#Region "Adjuntos"
	''' Revisado por: blp. Fecha: 09/01/2013
	''' <summary>
	''' Carga los adjuntos de la orden. Si no están cargados en caché, los recupera de bdd
	''' </summary>
	''' <param name="Id">Id de la orden en la que cargar el adjunto</param>
	''' <returns>datarow con los adjuntos</returns>
	''' <remarks>llamada desde Seguimiento.aspx.vb. Máx. 0,5 seg.</remarks>
	Private Function CargarAdjuntosOrden(ByVal Id As Integer) As DataRow()
		CrearTablaAdjuntosOrden()
		Dim dr() As DataRow
		dr = DsetPedidos.Tables("ORDENES").Rows.Find(Id).GetChildRows("REL_ORDENES_ADJUNTOS")
		If dr.Length = 0 Then
			Dim oOrden As COrden = Me.FSNServer.Get_Object(GetType(FSNServer.COrden))
			oOrden.ID = Id
			Dim oAdjs As CAdjuntos = oOrden.CargarAdjuntos(False)
			For Each oAdj As Adjunto In oAdjs
				Dim fila As DataRow = CrearFilaNueva("ADJUNTOS_ORDEN", "ORDENES", "ID", Id)
				fila.Item("ID") = oAdj.Id
				fila.Item("ORDEN") = Id
				fila.Item("NOMBRE") = oAdj.Nombre
				fila.Item("COMENTARIO") = oAdj.Comentario
				fila.Item("FECHA") = oAdj.Fecha
				fila.Item("DATASIZE") = oAdj.dataSize
				fila.Item("TIPO") = TiposDeDatos.Adjunto.Tipo.Orden_Entrega
				AnyadirFilaADsetPedidos("ADJUNTOS_ORDEN", "ORDENES", "ORDEN", fila)
			Next
			dr = DsetPedidos.Tables("ORDENES").Rows.Find(Id).GetChildRows("REL_ORDENES_ADJUNTOS")
		End If
		Return dr
	End Function
	''' Revisado por: blp. Fecha: 09/01/2013
	''' <summary>
	''' Carga los adjuntos de la línea. Si no están cargados en caché, los recupera de bdd
	''' </summary>
	''' <param name="Id">Id de la línea en la que cargar el adjunto</param>
	''' <returns>datarow con los adjuntos</returns>
	''' <remarks>llamada desde Seguimiento.aspx.vb. Máx. 0,5 seg.</remarks>
	Private Function CargarAdjuntosLinea(ByVal Id As Integer) As DataRow()
		CrearTablasAdjuntosLinea()
		Dim dr() As DataRow
		dr = DsetPedidos.Tables("LINEASPEDIDO").Rows.Find(Id).GetChildRows("REL_LINEAS_ADJUNTOS")
		If dr.Length = 0 Then
			Dim oLinea As CLinea = Me.FSNServer.Get_Object(GetType(FSNServer.CLinea))
			oLinea.ID = Id
			Dim oAdjs As CAdjuntos = oLinea.CargarAdjuntos(False)
			For Each oAdj As Adjunto In oAdjs
				Dim fila As DataRow = CrearFilaNueva("ADJUNTOS_LINEA", "LINEASPEDIDO", "ID", Id)
				fila.Item("ID") = oAdj.Id
				fila.Item("LINEA") = Id
				fila.Item("NOMBRE") = oAdj.Nombre
				fila.Item("COMENTARIO") = oAdj.Comentario
				fila.Item("FECHA") = oAdj.Fecha
				fila.Item("DATASIZE") = oAdj.dataSize
				fila.Item("TIPO") = TiposDeDatos.Adjunto.Tipo.Linea_Pedido
				AnyadirFilaADsetPedidos("ADJUNTOS_LINEA", "LINEASPEDIDO", "LINEA", fila)
			Next
			dr = DsetPedidos.Tables("LINEASPEDIDO").Rows.Find(Id).GetChildRows("REL_LINEAS_ADJUNTOS")
		End If
		Return dr
	End Function
	Private Sub grdAdjuntos_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdAdjuntos.RowCommand
		If e.CommandName = "Descargar" Then
			Dim sArgs() As String = Split(e.CommandArgument)
			Select Case CType(sArgs(1), TiposDeDatos.Adjunto.Tipo)
				Case TiposDeDatos.Adjunto.Tipo.Orden_Entrega
					Dim dr As DataRow = Array.Find(CargarAdjuntosOrden(sArgs(2)), Function(fila As DataRow) fila.Item("ID") = sArgs(0))
					Dim AdjunDescargar As FSNServer.Adjunto = Me.FSNServer.Get_Object(GetType(FSNServer.Adjunto))
					AdjunDescargar.Id = sArgs(0)
					AdjunDescargar.Nombre = dr.Item("NOMBRE")
					AdjunDescargar.dataSize = dr.Item("DATASIZE")
					Dim sNombre As String = AdjunDescargar.Nombre
					AdjunDescargar.EscribirADisco(ConfigurationManager.AppSettings("temp") & "\", sNombre, sArgs(1))
					Response.AddHeader("content-disposition", "attachment; filename=""" & AdjunDescargar.Nombre & """")
					Response.ContentType = "application/octet-stream"
					Response.WriteFile(ConfigurationManager.AppSettings("temp") & "\" & sNombre)
					Response.End()
				Case TiposDeDatos.Adjunto.Tipo.Linea_Pedido
					Dim dr As DataRow = Array.Find(CargarAdjuntosLinea(sArgs(2)), Function(fila As DataRow) fila.Item("ID") = sArgs(0))
					Dim AdjunDescargar As FSNServer.Adjunto = Me.FSNServer.Get_Object(GetType(FSNServer.Adjunto))
					AdjunDescargar.Id = sArgs(0)
					AdjunDescargar.Nombre = dr.Item("NOMBRE")
					AdjunDescargar.dataSize = dr.Item("DATASIZE")
					Dim sNombre As String = AdjunDescargar.Nombre
					AdjunDescargar.EscribirADisco(ConfigurationManager.AppSettings("temp") & "\", sNombre, sArgs(1))
					Response.AddHeader("content-disposition", "attachment; filename=""" & AdjunDescargar.Nombre & """")
					Response.ContentType = "application/octet-stream"
					Response.WriteFile(ConfigurationManager.AppSettings("temp") & "\" & sNombre)
					Response.End()
			End Select
		End If
	End Sub
	Private Sub grdAdjuntos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdAdjuntos.RowDataBound
		If e.Row.RowType = DataControlRowType.DataRow Then
			Dim fila As DataRow = CType(e.Row.DataItem, DataRow)
			CType(e.Row.FindControl("litFechaAdjunto"), Literal).Text = FSNLibrary.FormatDate(fila.Item("FECHA"), Me.Usuario.DateFormat)
			CType(e.Row.FindControl("lnkbtnAdjunto"), LinkButton).Text =
				fila.Item("NOMBRE") & " (" & FSNLibrary.FormatNumber(fila.Item("DATASIZE") / 1024, Me.Usuario.NumberFormat) & " kb.)"
			Select Case CType(fila.Item("TIPO"), TiposDeDatos.Adjunto.Tipo)
				Case TiposDeDatos.Adjunto.Tipo.Orden_Entrega
					CType(e.Row.FindControl("lnkbtnAdjunto"), LinkButton).CommandArgument = fila.Item("ID") & " " & fila.Item("TIPO") & " " & fila.Item("ORDEN")
				Case TiposDeDatos.Adjunto.Tipo.Linea_Pedido
					CType(e.Row.FindControl("lnkbtnAdjunto"), LinkButton).CommandArgument = fila.Item("ID") & " " & fila.Item("TIPO") & " " & fila.Item("LINEA")
			End Select
			Me.ScriptMgr.RegisterPostBackControl(e.Row.FindControl("lnkbtnAdjunto"))
			If IsDBNull(fila.Item("COMENTARIO")) OrElse String.IsNullOrEmpty(fila.Item("COMENTARIO")) Then
				CType(e.Row.FindControl("imgComentarioAdjunto"), FSNWebControls.FSNImageTooltip).Visible = False
			End If
		End If
	End Sub
#End Region
	''' Revisado por: blp. Fecha: 20/07/2012
	''' <summary>
	''' Función en la que devolveremos un report cargado con los datos solicitados
	''' Por el momento, y tal y como se ha definido en el prototipo 2358, devolvemos:
	''' Si se elige una única orden: esa orden.
	''' Si se elige exportar todas las órdenes: se cargan todas las mostradas en pantalla (no todas las del usuario)
	''' </summary>
	''' <param name="sOrdenId">Id de la Orden de la que se desean recuperar las líneas</param>
	''' <param name="dsOrdenes">dataset con las órdenes de las que se quieren recuperar las líneas. La tabla con las órdenes debe llevar por nombre, obligatoriamente, "ORDENES"</param>
	''' <returns>El report ya instanciado y con datos</returns>
	''' <remarks>Llamada desde los botones de imprimir y exportar de Seguimiento.aspx
	''' Tiempo máximo 1 sec.</remarks>
	Friend Function ObtenerDataSetReport(Optional ByVal sOrdenId As String = "", Optional ByVal dsOrdenes As DataSet = Nothing) As DataSet
		Dim dtPed As DataTable = Nothing

		Dim dsLineas As DataSet
		Dim hayDatosImputacion As Boolean = False
		If sOrdenId <> "" Then
			Dim sOrdenList As New Generic.List(Of Object)
			sOrdenList.Add(sOrdenId)
			Dim oOrdenes As COrdenes = FSNServer.Get_Object(GetType(FSNServer.COrdenes))
			Dim dtOrtdenesID As DataTable = oOrdenes.CargarOrdenesId(sOrdenList)
			dsLineas = oOrdenes.DevolverLineas(Me.Usuario.Idioma, dtOrtdenesID, Acceso.gbAccesoFSSM)
		Else
			'Si no nos han pasado dataset como parámetro, cogemos el de la página
			If dsOrdenes Is Nothing Then
				If DsetTodosPedidos() IsNot Nothing AndAlso DsetTodosPedidos.Tables("ORDENES") IsNot Nothing Then
					dtPed = DsetTodosPedidos.Tables("ORDENES").Copy
				End If
			Else
				dtPed = dsOrdenes.Tables("ORDENES").Copy
			End If
			Dim ordenesID = From datos In dtPed
							Select datos.Item("ID") Distinct.ToList
			If ordenesID.Any Then
				Dim oOrdenes As COrdenes = FSNServer.Get_Object(GetType(FSNServer.COrdenes))
				Dim dtOrtdenesID As DataTable = oOrdenes.CargarOrdenesId(ordenesID)
				dsLineas = oOrdenes.DevolverLineas(Me.Usuario.Idioma, dtOrtdenesID, Acceso.gbAccesoFSSM)
			Else
				dsLineas = New DataSet
			End If
		End If

		Dim ds As DataSet = New DataSet
		If dsLineas.Tables.Count > 0 Then
			Dim query = From Datos In dtPed
						Join Lineas In dsLineas.Tables(0) On Lineas.Item("ORDEN") Equals Datos.Item("ID")
						Select Lineas

			ds.Tables.Add(dtPed)
			ds.Tables.Add(query.CopyToDataTable())
			ds.Tables(0).TableName = "ORDENES"
			ds.Tables(1).TableName = "LINEASPEDIDO"
			Dim key() As DataColumn = {ds.Tables("ORDENES").Columns("ID")}
			ds.Tables("ORDENES").PrimaryKey = key
			Dim keylp() As DataColumn = {ds.Tables("LINEASPEDIDO").Columns("ID")}
			ds.Tables("LINEASPEDIDO").PrimaryKey = keylp
			keylp(0) = ds.Tables("LINEASPEDIDO").Columns("ORDEN")
			ds.Relations.Add("REL_ORDENES_LINEASPEDIDO", key, keylp, False)

			If Me.Acceso.gbAccesoFSSM Then
				query = From Datos In ds.Tables("LINEASPEDIDO")
						Join Lineas In dsLineas.Tables(1) On Lineas.Item("LINEA") Equals Datos.Item("LINEAID")
						Select Lineas
				If query.Any Then
					ds.Tables.Add(query.CopyToDataTable())
					ds.Tables(2).TableName = "LINEASPEDIMPUTACION"
					Dim keylpi() As DataColumn = {ds.Tables("LINEASPEDIMPUTACION").Columns("LINEA")}
					ds.Relations.Add("REL_LINEASPEDIDO_LINEASPEDIMPUTACION", keylp, keylpi, False)
				End If
			End If
		End If
		Return ds
	End Function
	''' <summary>
	''' Determina si se muestra el control en función de la propiedad AuthorizeWebPart y los permisos del usuario
	''' </summary>
	''' <param name="sender">Origen del evento.</param>
	''' <param name="e">WebPartAuthorizationEventArgs que contiene los datos del evento.</param>
	''' <remarks>Se produce cuando se llama al método IsAuthorized para determinar si se puede agregar un control WebPart o un control de servidor a una página.</remarks>
	Private Sub WebPartManager1_AuthorizeWebPart(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.WebParts.WebPartAuthorizationEventArgs) Handles WebPartManager1.AuthorizeWebPart
		e.IsAuthorized = AutorizacionWebparts(e.AuthorizationFilter)
	End Sub
#Region "Unidades"
	''' <summary>
	''' Propiedad que recupera el conjunto de las unidades de Pedido para su posterior uso en la página
	''' </summary>
	Private ReadOnly Property TodasLasUnidades() As CUnidadesPedido
		Get
			Dim oUnidadesPedido As CUnidadesPedido = Me.FSNServer.Get_Object(GetType(FSNServer.CUnidadesPedido))
			If HttpContext.Current.Cache("TodasLasUnidades" & Me.Usuario.Idioma.ToString()) Is Nothing Then
				oUnidadesPedido.CargarTodasLasUnidades(Me.Usuario.Idioma)
				Me.InsertarEnCache("TodasLasUnidades" & Me.Usuario.Idioma.ToString(), oUnidadesPedido)
			Else
				oUnidadesPedido = HttpContext.Current.Cache("TodasLasUnidades" & Me.Usuario.Idioma.ToString())
			End If
			Return oUnidadesPedido
		End Get
	End Property
#End Region
	''' <summary>
	''' Selecciona los checks de seguimiento con las opciones del usuario y los almacena en variables de sesion para cuando accede a BBDD para obtener el dataset de pedidos
	''' </summary>
	''' <remarks>Llamada desde:page_load; Tiempo máximo:0,2seg.</remarks>
	Private Sub SeleccionChecksSeguimiento()
		'Cargar los checks de seguimiento de pedidos por CC
		fldsetIMPCC.Visible = False
		If Acceso.gbAccesoFSSM Then
			If FSNUser.PermisoVerPedidosCCImputables Then
				fldsetIMPCC.Visible = True
				If Not IsPostBack Then
					chkSeguimientoPedidos.Items(0).Selected = FSNUser.SeguimientoVerPedidosUsuario
					chkSeguimientoPedidos.Items(1).Selected = FSNUser.SeguimientoVerPedidosCCImputables
				End If
				chkSeguimientoPedidos.Items(0).Attributes.Add("onClick", "ValidacionChecksSeguimiento(0)")
				chkSeguimientoPedidos.Items(1).Attributes.Add("onClick", "ValidacionChecksSeguimiento(1)")
			End If
		Else
			chkSeguimientoPedidos.Items(0).Selected = True
			chkSeguimientoPedidos.Items(1).Selected = False
		End If
	End Sub
	''' Revisado por: blp. Fecha:20/04/2012
	''' <summary>
	''' Mira si hemos querido filtrar por una orden. Si es así, filtra por ella. Si no, filtra por cookies
	''' </summary>
	''' <remarks>Llamada desde page_load. Max 0,1 seg.</remarks>
	Private Sub gestionFiltrosBusqueda()
		If Not Page.IsPostBack And Not String.IsNullOrEmpty(Request("orden")) Then
			Dim lis As New List(Of Filtro)
			Dim sArgumentos() As String = Split(Request("orden"), "/")
			lis.Add(New Filtro(TipoFiltro.Anio, sArgumentos(0)))
			lis.Add(New Filtro(TipoFiltro.Cesta, sArgumentos(1)))
			lis.Add(New Filtro(TipoFiltro.NumPedido, sArgumentos(2)))
			FiltrosAnyadidos = lis
			igNumMeses.Value = DBNull.Value
			If ddlAnio.Items.FindByValue(sArgumentos(0)) Is Nothing Then
				Dim li As New ListItem
				li.Value = sArgumentos(0)
				li.Text = sArgumentos(0)
				ddlAnio.Items.Add(li)
			End If
			ddlAnio.SelectedValue() = sArgumentos(0)
			txtCesta.Text = sArgumentos(1)
			txtPedido.Text = sArgumentos(2)
			'Añadir los filtros al label de filtros
			RellenarLabelFiltros()
		Else
			gestionCookieFiltrosBusqueda(GestionCookiesFiltroBusqueda.CargarDesdeCookie)
		End If
	End Sub
	''' <summary>
	''' Vamos a
	''' 1. Recuperar los valores de busqueda guardados en una cookie
	''' 2. Añadir los filtros a la propiedad FiltrosAnyadidos, que gestiona los filtros seleccionados en la búsqueda
	''' 3. Añadir los filtros al label de filtros de busqueda
	''' 4. Añadir los filtros a cada uno de los controles de filtrado
	''' 
	''' o bien
	''' 
	''' 1. Guardar en la cookie los filtros de búsqueda que el usuario ha utilizado
	''' </summary>
	''' <param name="tipodeGestion">Gestión a realizar con los filtros en cookies: Cargar o Guardar</param>
	''' <remarks>Llamada desde el load y el unload de la página</remarks>
	Private Sub gestionCookieFiltrosBusqueda(ByVal tipodeGestion As Integer)
		If tipodeGestion = GestionCookiesFiltroBusqueda.CargarDesdeCookie Then
			'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
			'1. Recuperar cookie y transformar a formato de Filtros
			'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
			Dim _FiltrosAnyadidos As List(Of Filtro) = RecuperarDesdeCookieFiltrosBusqueda()

			'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
			'2. Pasar filtros a la variable de Filtros de busqueda
			'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
			'Si no había cookie
			If _FiltrosAnyadidos.Count > 0 Then
				FiltrosAnyadidos = _FiltrosAnyadidos
			Else
				ActualizarFiltros()
			End If

			'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
			'3.Añadir los filtros a cada uno de los controles de filtrado
			'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
			SeleccionarFiltrosEnPanelBusqueda()

			'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
			'4.Añadir los filtros al label de filtros
			'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
			RellenarLabelFiltros()

		ElseIf tipodeGestion = GestionCookiesFiltroBusqueda.GuardarEnCookie Then
			GuardarEnCookieFiltrosBusqueda()
		End If
	End Sub
	''' <summary>
	''' Método que guarda en cookies los filtros guardados en ese momento
	''' </summary>
	''' <remarks>Llamada desde gestionCookieFiltrosBusqueda. Tiempo máximo inferior 0,1 seg</remarks>
	Private Sub GuardarEnCookieFiltrosBusqueda()
		If FiltrosAnyadidos Is Nothing OrElse FiltrosAnyadidos.Count = 0 Then
			ActualizarFiltros()
		End If
		Dim cookie As HttpCookie = New HttpCookie("Filtros" & Usuario.Cod)
		For Each oFiltro As Filtro In FiltrosAnyadidos
			Select Case oFiltro.tipo
				Case TipoFiltro.Tipo
					'Idéntico a como se hace en DevolverPedidosConFiltro()
					'PedidosGS
					cookie.Item(oFiltro.tipo & "_" & CPedido.OtrosAtributosPedido.Negociado) = oFiltro.Valores(0)
					'PedidosEP no libres
					cookie.Item(oFiltro.tipo & "_" & CPedido.OtrosAtributosPedido.CatalogadoNegociado) = oFiltro.Valores(1)
					'PedidosEP LIBRES
					cookie.Item(oFiltro.tipo & "_" & CPedido.OtrosAtributosPedido.CatalogadoLibres) = oFiltro.Valores(2)
					'Abonos
					cookie.Item(oFiltro.tipo & "_" & CPedido.OtrosAtributosPedido.Abono) = oFiltro.Valores(4)
					'PedidosERP
					If Session("HayIntegracionEntradaoEntradaSalida") Then
						cookie.Item(oFiltro.tipo & "_" & CPedido.OtrosAtributosPedido.Directos) = oFiltro.Valores(3)
					Else
						cookie.Item(oFiltro.tipo & "_" & CPedido.OtrosAtributosPedido.Directos) = oFiltro.Valores(0) And oFiltro.Valores(1)
					End If
					If Acceso.gbUsarPedidosAbiertos Then
						cookie.Item(oFiltro.tipo & "_" & CPedido.OtrosAtributosPedido.ContraAbiertos) = oFiltro.Valores(5)
					End If
					cookie.Item(oFiltro.tipo & "_" & CPedido.OtrosAtributosPedido.Express) = oFiltro.Valores(6)
				Case TipoFiltro.PartidaPresupuestaria
					'Dado que puede haber más de un filtro de partida pres, los diferenciamos en la cookie mediante el info, que lleva la partida 0 y el nivel de la partida presupuestaria
					cookie.Item(oFiltro.tipo & "_" & oFiltro.Info) = oFiltro.Valor
				Case TipoFiltro.FechaDesde, TipoFiltro.FechaHasta, TipoFiltro.FraFechaDesde, TipoFiltro.FraFechaHasta, TipoFiltro.PagoFechaDesde, TipoFiltro.PagoFechaHasta
					cookie.Item(oFiltro.tipo) = oFiltro.ValorFecha
				Case TipoFiltro.Categoria, TipoFiltro.CentroCoste
					'Guardamos en la cookie el valor de la categoría y tb su valuePath
					cookie.Item(oFiltro.tipo) = oFiltro.Valor & TextosSeparadores.dosArrobas & oFiltro.Info
				Case Else
					cookie.Item(oFiltro.tipo) = oFiltro.Valor
			End Select
		Next
		'En la cookie siempre se debe guardar el valor de número de meses
		'(dado que por defecto, si no hay valor seleccionado de meses, se debe cargar solo el ultimo mes mas el resto de filtros)
		If cookie.Item(TipoFiltro.NumMeses) Is Nothing Then
			cookie.Item(TipoFiltro.NumMeses) = 1
		End If
		cookie.Expires = DateTime.Now.AddDays(30)
		Response.AppendCookie(cookie)
	End Sub
	''' Revisado por: blp. Fecha: 17/07/2012
	''' <summary>
	''' Método que recupera desde la cookie de filtros de búsqueda, los filtros guardados en la última búsqueda del usuario
	''' </summary>
	''' <remarks>Llamada desde gestionCookieFiltrosBusqueda. Tiempo máximo inferior 0,1 seg</remarks>
	Private Function RecuperarDesdeCookieFiltrosBusqueda() As List(Of Filtro)
		Dim _FiltrosAnyadidos As New List(Of Filtro)
		If Not Request.Cookies("Filtros" & Usuario.Cod) Is Nothing Then
			Dim MiCookie As HttpCookie = Request.Cookies("Filtros" & Usuario.Cod)
			If MiCookie IsNot Nothing Then
				Dim oFiltro As Filtro
				Dim oFiltroPedidos As New Filtro
				Dim iMostrarPedidos(5) As Boolean
				Dim _FiltrosCookie As System.Collections.Specialized.NameValueCollection = MiCookie.Values
				If _FiltrosCookie.Count > 0 Then
					For i = 0 To _FiltrosCookie.Count - 1
						'Filtro Tipo de origen del pedido
						If _FiltrosCookie.GetKey(i).StartsWith(TipoFiltro.Tipo & "_") Then
							'PedidosGS
							If _FiltrosCookie.GetKey(i) = TipoFiltro.Tipo & "_" & CPedido.OtrosAtributosPedido.Negociado Then
								oFiltroPedidos.tipo = TipoFiltro.Tipo
								iMostrarPedidos(0) = _FiltrosCookie(i).ToString
								'PedidosEP No libres
							ElseIf _FiltrosCookie.GetKey(i) = TipoFiltro.Tipo & "_" & CPedido.OtrosAtributosPedido.CatalogadoNegociado Then
								oFiltroPedidos.tipo = TipoFiltro.Tipo
								iMostrarPedidos(1) = _FiltrosCookie(i).ToString
								'PedidosEP LIBRES
							ElseIf _FiltrosCookie.GetKey(i) = TipoFiltro.Tipo & "_" & CPedido.OtrosAtributosPedido.CatalogadoLibres Then
								If Me.Acceso.gbPedidoLibre Then
									oFiltroPedidos.tipo = TipoFiltro.Tipo
									iMostrarPedidos(2) = _FiltrosCookie(i).ToString
								Else
									oFiltroPedidos.tipo = TipoFiltro.Tipo
									iMostrarPedidos(2) = False
								End If
								'PedidosERP
							ElseIf _FiltrosCookie.GetKey(i) = TipoFiltro.Tipo & "_" & CPedido.OtrosAtributosPedido.Directos Then
								oFiltroPedidos.tipo = TipoFiltro.Tipo
								iMostrarPedidos(3) = _FiltrosCookie(i).ToString
								'Abonos
							ElseIf _FiltrosCookie.GetKey(i) = TipoFiltro.Tipo & "_" & CPedido.OtrosAtributosPedido.Abono Then
								oFiltroPedidos.tipo = TipoFiltro.Tipo
								iMostrarPedidos(4) = _FiltrosCookie(i).ToString

							ElseIf _FiltrosCookie.GetKey(i) = TipoFiltro.Tipo & "_" & CPedido.OtrosAtributosPedido.ContraAbiertos Then
								oFiltroPedidos.tipo = TipoFiltro.Tipo
								iMostrarPedidos(5) = _FiltrosCookie(i).ToString
							End If
						ElseIf Left(_FiltrosCookie.GetKey(i), 2) = TipoFiltro.PartidaPresupuestaria Then
							'Partidas presupuestarias
							oFiltro = New Filtro
							oFiltro.tipo = TipoFiltro.PartidaPresupuestaria
							oFiltro.Info = _FiltrosCookie.GetKey(i).Substring(3)
							oFiltro.Valor = _FiltrosCookie(i).ToString
							_FiltrosAnyadidos.Add(oFiltro)
						ElseIf _FiltrosCookie.GetKey(i) = TipoFiltro.Categoria Then
							Dim sValores() As String = Split(_FiltrosCookie(i).ToString, TextosSeparadores.dosArrobas)
							oFiltro = New Filtro
							oFiltro.tipo = TipoFiltro.Categoria
							oFiltro.Valor = sValores(0)
							oFiltro.Info = sValores(1)
							_FiltrosAnyadidos.Add(oFiltro)
						ElseIf _FiltrosCookie.GetKey(i) = TipoFiltro.PedidosConAlbaranesBloqueados Then
							If Me.Acceso.g_bAccesoFSFA Then
								oFiltro = New Filtro
								oFiltro.tipo = _FiltrosCookie.GetKey(i)
								oFiltro.Valor = _FiltrosCookie(i).ToString
								_FiltrosAnyadidos.Add(oFiltro)
							End If
						ElseIf _FiltrosCookie.GetKey(i) = TipoFiltro.FechaDesde OrElse _FiltrosCookie.GetKey(i) = TipoFiltro.FechaHasta _
							OrElse _FiltrosCookie.GetKey(i) = TipoFiltro.FraFechaDesde OrElse _FiltrosCookie.GetKey(i) = TipoFiltro.FraFechaHasta _
							OrElse _FiltrosCookie.GetKey(i) = TipoFiltro.PagoFechaDesde OrElse _FiltrosCookie.GetKey(i) = TipoFiltro.PagoFechaHasta Then
							oFiltro = New Filtro
							oFiltro.tipo = _FiltrosCookie.GetKey(i)
							oFiltro.ValorFecha = _FiltrosCookie(i).ToString
							_FiltrosAnyadidos.Add(oFiltro)
						ElseIf _FiltrosCookie.GetKey(i) = TipoFiltro.CentroCoste Then
							Dim sValores() As String = Split(_FiltrosCookie(i).ToString, TextosSeparadores.dosArrobas)
							oFiltro = New Filtro
							oFiltro.tipo = TipoFiltro.CentroCoste
							oFiltro.Valor = sValores(0)
							oFiltro.Info = sValores(1)
							_FiltrosAnyadidos.Add(oFiltro)
						Else
							oFiltro = New Filtro
							oFiltro.tipo = _FiltrosCookie.GetKey(i)
							oFiltro.Valor = _FiltrosCookie(i).ToString
							_FiltrosAnyadidos.Add(oFiltro)
						End If
					Next
					oFiltroPedidos.Valores = iMostrarPedidos
					_FiltrosAnyadidos.Add(oFiltroPedidos)
				End If
				'Si hay filtros pero ninguno de ellos es el de número de meses (único filtro de fecha que se guarda en cookie)
				'nos aseguramos de que se añada a los filtros, dado que una vez recogidos de la cookie, no se pasa por los paneles
			End If
		End If
		Return _FiltrosAnyadidos
	End Function
	''' <summary>
	''' Método que limpia los criterios de búsqueda del panel de búsqueda.
	''' </summary>
	''' <param name="sender">Control btnLimpiar</param>
	''' <param name="e">parámetros del evento</param>
	''' <remarks>llamada desde el control, al hacer click. Tiempo máximo inferios a 0,1 seg.</remarks>
	Private Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
		'Recorremos los controles del panel y los vamos limpiando según el tipo
		VaciarControlesRecursivo(pnlBusqueda)
		'Quitamos filtro de categoría
		If Not fsnTvwCategorias.SelectedNode Is Nothing Then
			Dim ev As New System.EventArgs
			lnkQuitarCategoria_Click(fsnTvwCategorias, ev)
		End If
		'Inicializamos filtro checkboxlists
		Session("filtroEstadoEP") = ""
		Session("filtroEstadoEP_PedidoAbierto") = ""
		Dim bDesmarcarAbono As Boolean = True
		inicializarCheckboxLists(bDesmarcarAbono)
		''nos aseguramos que sigue sin verse el lblListaFiltros
		'divListaFiltros.Style("display") = "none"
		pnlBusqueda.Style("display") = "block"
		'Actualizar
		pnlBuscador.Update()
		updpnlCategorias.Update()
		'upListaFiltros.Update()
	End Sub
	''' <summary>
	''' Método para inicializar la selección de los checkboxList del panel buscador
	''' </summary>
	''' <remarks>Llamada desde InicializarCOntroles y btnLimpiar_Click. Tiempo inferior a 0,1 seg</remarks>
	Private Sub inicializarCheckboxLists(Optional ByVal desmarcarAbono As Boolean = False)
		If Not String.IsNullOrEmpty(Session("filtroEstadoEP")) Or Not String.IsNullOrEmpty(Request("filtro")) Then
			Dim iEst As Integer

			If Not String.IsNullOrEmpty(Request("filtro")) Then
				iEst = Request("filtro")
			ElseIf Not String.IsNullOrEmpty(Session("filtroEstadoEP")) Then
				iEst = Session("filtroEstadoEP")
			End If
		Else
			For Each l As ListItem In chklstEstado.Items
				l.Selected = True
			Next
		End If

		For Each l As ListItem In chklstOtrosFiltros.Items
			If l.Value <> CPedido.OtrosAtributosPedido.Abono Then _
			 l.Selected = True
			If desmarcarAbono AndAlso l.Value = CPedido.OtrosAtributosPedido.Abono Then _
				l.Selected = False
			If Not Me.Acceso.gbPedidoLibre AndAlso l.Value = CPedido.OtrosAtributosPedido.CatalogadoLibres Then
				l.Selected = False
			End If
		Next
	End Sub
	''' Revisado por: blp. Fecha: 14/05/2012
	''' <summary>
	''' Método que recorre de manera recursiva el control recibido y vacía las selecciones de todos los controles
	''' </summary>
	''' <param name="oControlBase">Control a recorrer</param>
	''' <remarks>Llamada desde btnLimpiar_Click</remarks>
	Private Sub VaciarControlesRecursivo(ByVal oControlBase As Object)
		For Each oControl In oControlBase.Controls
			Select Case oControl.GetType().ToString
				Case GetType(DropDownList).ToString
					If CType(oControl, DropDownList).SelectedItem IsNot Nothing Then _
						CType(oControl, DropDownList).SelectedItem.Selected = False
				Case GetType(TextBox).ToString
					CType(oControl, TextBox).Text = String.Empty
				Case GetType(Infragistics.Web.UI.EditorControls.WebDatePicker).ToString
					CType(oControl, Infragistics.Web.UI.EditorControls.WebDatePicker).Value = Nothing
				Case GetType(Infragistics.Web.UI.EditorControls.WebDateTimeEditor).ToString
					CType(oControl, Infragistics.Web.UI.EditorControls.WebDateTimeEditor).Value = Nothing
					CType(oControl, Infragistics.Web.UI.EditorControls.WebDateTimeEditor).Text = String.Empty
				Case GetType(Infragistics.Web.UI.EditorControls.WebNumericEditor).ToString
					CType(oControl, Infragistics.Web.UI.EditorControls.WebNumericEditor).Value = String.Empty
				Case GetType(CheckBoxList).ToString
					'Los hacemos aparte, llamando a una función específica en btnLimpiar_Click
				Case GetType(CheckBox).ToString
					CType(oControl, CheckBox).Checked = False
				Case Else
					If oControl.Controls IsNot Nothing AndAlso oControl.Controls.Count > 0 Then
						VaciarControlesRecursivo(oControl)
					End If
			End Select
		Next
	End Sub
	''' <summary>
	''' Propiedad que devuelve una lista de listitems con todos los estados de pedido que hay
	''' </summary>
	Private ReadOnly Property Estados() As List(Of ListItem)
		Get
			Dim _listaDeEstados As New List(Of ListItem)
			RellenarTextosCheckboxListsEstados()
			_listaDeEstados.Add(New ListItem(_sEstados(0), CPedido.Estado.PendienteAprobar))
			_listaDeEstados.Add(New ListItem(_sEstados(1), CPedido.Estado.EmitidoAlProveedor))
			_listaDeEstados.Add(New ListItem(_sEstados(2), CPedido.Estado.AceptadoPorElProveedor))
			_listaDeEstados.Add(New ListItem(_sEstados(3), CPedido.Estado.EnCamino))
			_listaDeEstados.Add(New ListItem(_sEstados(4), CPedido.Estado.RecibidoParcialmente))
			_listaDeEstados.Add(New ListItem(_sEstados(5), CPedido.Estado.DenegadoParcialmente))
			_listaDeEstados.Add(New ListItem(_sEstados(6), CPedido.Estado.Denegado))
			_listaDeEstados.Add(New ListItem(_sEstados(7), CPedido.Estado.Cerrado))
			_listaDeEstados.Add(New ListItem(_sEstados(8), CPedido.Estado.Rechazado))
			_listaDeEstados.Add(New ListItem(_sEstados(9), CPedido.Estado.Anulado))
			Return _listaDeEstados
		End Get
	End Property
	''' <summary>
	''' Valor de la suma de todos los estados existentes
	''' </summary>
	Private ReadOnly Property MaxEstadosValue() As CPedido.Estado
		Get
			Dim _iEst As CPedido.Estado
			For Each oEstado As ListItem In Estados
				_iEst = _iEst Or CType(oEstado.Value, CPedido.Estado)
			Next
			Return _iEst
		End Get
	End Property
	''' <summary>
	''' Propiedad que devuelve una lista de listitems con las clases de pedido que hay
	''' </summary>
	Private ReadOnly Property ClasesDePedido() As List(Of ListItem)
		Get
			Dim _listaDeClasesDePedido As New List(Of ListItem)
			RellenarTextosCheckboxListsClasesDePedido()
			_listaDeClasesDePedido.Add(New ListItem(_sOtrosFiltros(0), CPedido.OtrosAtributosPedido.Abono))
			_listaDeClasesDePedido.Add(New ListItem(_sOtrosFiltros(1), CPedido.OtrosAtributosPedido.CatalogadoNegociado)) 'EP no libres
			_listaDeClasesDePedido.Add(New ListItem(_sOtrosFiltros(2), CPedido.OtrosAtributosPedido.CatalogadoLibres)) 'EP LIBRES
			If Acceso.gbUsarPedidosAbiertos Then
				_listaDeClasesDePedido.Add(New ListItem(_sOtrosFiltros(5), CPedido.OtrosAtributosPedido.ContraAbiertos)) ' Pedidos ContraAbiertos
			End If
			_listaDeClasesDePedido.Add(New ListItem(_sOtrosFiltros(3), CPedido.OtrosAtributosPedido.Negociado)) 'GS
			If Session("HayIntegracionEntradaoEntradaSalida") = True Then
				_listaDeClasesDePedido.Add(New ListItem(_sOtrosFiltros(4), CPedido.OtrosAtributosPedido.Directos)) 'ERP
			End If
			_listaDeClasesDePedido.Add(New ListItem(_sOtrosFiltros(6), CPedido.OtrosAtributosPedido.Express)) 'Express
			Return _listaDeClasesDePedido
		End Get
	End Property
	''' <summary>
	''' Rellenamos los textos de los Checkbox OtrosFiltros y Estados
	''' </summary>
	''' <remarks>Llamada desde InicializarControles. Máx. 0,1 seg.</remarks>
	Private Sub RellenarTextosCheckboxLists()
		RellenarTextosCheckboxListsEstados()
		RellenarTextosCheckboxListsClasesDePedido()
	End Sub
	''' <summary>
	''' Rellenamos los textos de los Checkbox OtrosFiltros y Estados
	''' </summary>
	''' <remarks>Llamada desde RellenarTextosCheckBoxList y Estados(). Máx. 0,1 seg.</remarks>
	Private Sub RellenarTextosCheckboxListsEstados()
		'Comprobamos el primer valor. Si es Nothing, rellenamos todos
		If _sEstados(0) = Nothing Then
			_sEstados(0) = Textos(10)
			_sEstados(1) = Textos(11)
			_sEstados(2) = Textos(12)
			_sEstados(3) = Textos(13)
			_sEstados(4) = Textos(14)
			_sEstados(5) = Textos(70)
			_sEstados(6) = Textos(16)
			_sEstados(7) = Textos(15)
			_sEstados(8) = Textos(17)
			_sEstados(9) = Textos(38)
		End If
	End Sub
	''' <summary>
	''' Rellenamos los textos de los Checkbox OtrosFiltros y Estados
	''' </summary>
	''' <remarks>Llamada desde RellenarTextosCheckboxLists y ClasesdePedido(). Máx. 0,1 seg.</remarks>
	Private Sub RellenarTextosCheckboxListsClasesDePedido()
		If _sOtrosFiltros(0) = Nothing Then
			_sOtrosFiltros(0) = Textos(172) 'Ver abonos
			_sOtrosFiltros(1) = Textos(173) 'Pedidos de catalogo negociados
			_sOtrosFiltros(2) = Textos(263) 'Pedidos de catalogo libres
			_sOtrosFiltros(3) = Textos(174) 'Pedidos negociados
			If Session("HayIntegracionEntradaoEntradaSalida") = True Then
				_sOtrosFiltros(4) = Textos(189) 'Pedidos directos
			End If
			If Me.Acceso.gbUsarPedidosAbiertos Then
				_sOtrosFiltros(5) = Textos(289) 'Pedidos Contra Abierto
			End If
			_sOtrosFiltros(6) = Textos(292) 'Pedidos express
		End If
	End Sub
	''' Revisado por: blp. Fecha: 18/04/2012
	''' <summary>
	''' Método que recorre todos los filtros existentes y prepara el texto que aparecerá 
	''' en pantalla con los filtros seleccionados cuando el panel de búsqueda está oculto.
	''' No se comprueba que los filtros están en las opciones de filtro (ni que hay permisos para ver fras y pagos) porque 
	''' en la llamada desde gestionCookieFiltrosBusqueda ya se ha comprobado en SeleccionarFiltrosEnPanelBusqueda (método que se llama anteriormente)
	''' y en la llamada desde btnBuscar_Click sería muy extraño que un dato seleccionado en el panel desapareciese entre el momento que se selecciona y que se pulsa al botón de buscar
	''' </summary>
	''' <remarks>Llamada desde gestionCookieFiltrosBusqueda y btnBuscar_Click. Máx 0,1 seg.</remarks>
	Private Sub RellenarLabelFiltros()
		Dim sListaFiltros As String = String.Empty
		Dim sListaFiltrosEstado As String = String.Empty
		Dim sListaFiltrosClasesDePedido As String = String.Empty
		Dim sListaFiltrosAlbaranesBloquados As String = String.Empty
		Dim sListaFiltrosPartidasPres As String = String.Empty
		Dim sListaFiltrosFacturas As String = String.Empty
		Dim sListaFiltrosPagos As String = String.Empty
		Dim sListaFiltroContraAbiertos As String = String.Empty
		Dim sAnyoAbierto As String = "_"
		Dim sCestaAbierto As String = "_"
		Dim sPedAbierto As String = "_"
		Dim lista As List(Of ListItem)
		For Each oFiltro As Filtro In FiltrosAnyadidos
			Dim sValor As String = oFiltro.Valor
			Select Case oFiltro.tipo
				Case TipoFiltro.Anio
					sListaFiltros += "<b>" & Textos(23) & ":</b> " & sValor & "; "
				Case TipoFiltro.Cesta
					sListaFiltros += "<b>" & Textos(24) & ":</b> " & sValor & "; "
				Case TipoFiltro.NumPedido
					sListaFiltros += "<b>" & Textos(25) & ":</b> " & sValor & "; "
				Case TipoFiltro.NumPedidoProve
					sListaFiltros += "<b>" & Textos(73) & ":</b> " & sValor & "; "
				Case TipoFiltro.NumPedidoERP
					Dim oCParametros As FSNServer.CParametros = FSNServer.Get_Object(GetType(FSNServer.CParametros))
					sListaFiltros += "<b>" & oCParametros.CargarLiteralParametros(TiposDeDatos.LiteralesParametros.PedidoERP, Usuario.Idioma) & ":</b> " & sValor & "; "
				Case TipoFiltro.Articulo
					sListaFiltros += "<b>" & Textos(41) & ":</b> " & sValor & "; "
				Case TipoFiltro.FechaDesde
					If oFiltro.ValorFecha <> Date.MinValue Then
						sListaFiltros += "<b>" & Textos(178) & "</b> " ' Desde Fecha:
						sListaFiltros += oFiltro.ValorFecha & "; "
					End If
				Case TipoFiltro.FechaHasta
					If oFiltro.ValorFecha <> Date.MinValue Then
						sListaFiltros += "<b>" & Textos(179) & "</b> " ' HAsta Fecha:
						sListaFiltros += oFiltro.ValorFecha & "; "
					End If
				Case TipoFiltro.Empresa
					sListaFiltros += "<b>" & Textos(96) & ":</b> " & ddlEmpresa.SelectedItem.Text & "; "
				Case TipoFiltro.Receptor
					sListaFiltros += "<b>" & Textos(97) & ":</b> " & ddlReceptor.SelectedItem.Text & "; "
				Case TipoFiltro.Proveedor
					sListaFiltros += "<b>" & Textos(26) & ":</b> " & txtProveedor.Text & "; "
				Case TipoFiltro.ProveedorERP
					sListaFiltros += "<b>" & Textos(98) & ":</b> " & sValor & "; "
				Case TipoFiltro.CentroCoste
					sListaFiltros += "<b>" & Textos(142) & ":</b> " & tbCtroCoste.Text & "; "
				Case TipoFiltro.PartidaPresupuestaria
					Dim arSeparadores As String()
					ReDim Preserve arSeparadores(0)
					arSeparadores(0) = "@@@"
					Dim arPartidaPres As String() = oFiltro.Info.Split(arSeparadores, StringSplitOptions.None)
					Dim sPartidaPres0 As String = "tbPPres_" & arPartidaPres(0)
					Dim sPartidaPresDen = arPartidaPres(1)
					If phrPartidasPresBuscador IsNot Nothing _
					AndAlso phrPartidasPresBuscador.FindControl("tbDdlPP") IsNot Nothing _
					AndAlso phrPartidasPresBuscador.FindControl("tbDdlPP").FindControl(sPartidaPres0) IsNot Nothing Then
						Dim tbPartidaPres As TextBox = TryCast(phrPartidasPresBuscador.FindControl("tbDdlPP").FindControl(sPartidaPres0), TextBox)
						Dim sPartidaHiddenID As String = sPartidaPres0.Replace("tbPPres_", "tbPPres_Hidden_")
						Dim tbPartidaPresHidden As HiddenField = TryCast(phrPartidasPresBuscador.FindControl("tbDdlPP").FindControl(sPartidaHiddenID), HiddenField)
						'En teoría este if es innecesario porque todas las comprobaciones se hacen en la función
						'SeleccionarFiltrosEnPanelBusqueda(). Por si acaso, lo dejamos.
						If tbPartidaPresHidden.Value = sValor OrElse tbPartidaPres.Text = sValor Then
							Dim sPres0Den As String = tbPartidaPres.Text
							If sListaFiltrosPartidasPres = String.Empty Then
								sListaFiltrosPartidasPres += "<b>" & Textos(212) & ":</b> " & sPartidaPresDen
							Else
								sListaFiltrosPartidasPres += ", " & sPartidaPresDen
							End If
						End If
					End If
				Case TipoFiltro.TipoPedido 'Esto es TIPO DE PEDIDO
					sListaFiltros += "<b>" & Textos(175) & ":</b> " & ddlTipoPedido.SelectedItem.Text & "; "
				Case TipoFiltro.NumMeses
					If sValor = 1 Then
						sListaFiltros += "<b>" & Textos(210) & "</b>; " 'Pedidos del último mes
					ElseIf sValor > 1 Then
						sListaFiltros += "<b>" & Textos(216) & " " & sValor & " " & Textos(217) & "</b>; " 'Pedidos de los ultimos X meses
					End If
				Case TipoFiltro.Estado
					lista = Estados()
					'Si el filtro es igual a todos los estados (o sea, si todos están seleccionados), no se muestra en el label
					If sValor <> MaxEstadosValue Then
						For Each oEstado As ListItem In lista
							If (oEstado.Value And sValor) = oEstado.Value Then
								If sListaFiltrosEstado = String.Empty Then
									sListaFiltrosEstado += "<b>" & Textos(44) & ":</b> " & oEstado.Text
								Else
									sListaFiltrosEstado += ", " & oEstado.Text
								End If
							End If
						Next
					End If
				Case TipoFiltro.Tipo 'Esto es CLASE DE PEDIDO
					lista = ClasesDePedido()
					Dim ponComa As Boolean = False
					Dim mostrarTODOS As Boolean = True
					For i As Integer = 0 To oFiltro.Valores.Length - 1
						If Not oFiltro.Valores(i) Then
							'Cuando comprobemos el check de pedido libre, este solo cuenta si estan activados los pedidos libres en la aplicacion
							If i = 2 AndAlso Me.Acceso.gbPedidoLibre Then
								mostrarTODOS = False
								Exit For
							End If
						End If
					Next
					'El label con las clases de pedido sólo se muestra si no están todos los checks seleccionados
					If Not mostrarTODOS Then
						For i As Integer = 0 To oFiltro.Valores.Length - 1
							If oFiltro.Valores(i) Then
								If sListaFiltrosClasesDePedido = String.Empty Then sListaFiltrosClasesDePedido = "<b>" & Textos(211) & ":</b> "
								Select Case i
									Case 0 'GS
										sListaFiltrosClasesDePedido += IIf(ponComa, ", ", String.Empty) & _sOtrosFiltros(3)
										ponComa = True
									Case 1 'EP no libres
										sListaFiltrosClasesDePedido += IIf(ponComa, ", ", String.Empty) & _sOtrosFiltros(1)
										ponComa = True
									Case 2 'EP LIBRES
										sListaFiltrosClasesDePedido += IIf(ponComa, ", ", String.Empty) & _sOtrosFiltros(2)
										ponComa = True
									Case 3 'ERP
										If Session("HayIntegracionEntradaoEntradaSalida") = True Then
											sListaFiltrosClasesDePedido += IIf(ponComa, ", ", String.Empty) & _sOtrosFiltros(4)
											ponComa = True
										End If
									Case 4 'ABONOS
										sListaFiltrosClasesDePedido += IIf(ponComa, ", ", String.Empty) & _sOtrosFiltros(0) 'Abonos
										ponComa = True
									Case 5 'Pedidos contra abierto
										sListaFiltrosClasesDePedido += IIf(ponComa, ", ", String.Empty) & _sOtrosFiltros(5)
									Case 6 'Pedidos express
										sListaFiltrosClasesDePedido += IIf(ponComa, ", ", String.Empty) & _sOtrosFiltros(6)
								End Select
							End If
						Next
						If sListaFiltrosClasesDePedido.Length > 0 Then sListaFiltrosClasesDePedido += "; "
					End If
				Case TipoFiltro.PedidosConAlbaranesBloqueados
					sListaFiltrosAlbaranesBloquados = Textos(240) & "; "
				Case TipoFiltro.FraAnio
					If sListaFiltrosFacturas.Length = 0 Then sListaFiltrosFacturas = "<b>" & Textos(146) & ":</b> " 'Facturas
					sListaFiltrosFacturas += Textos(23) & ": " & sValor & "; "
				Case TipoFiltro.FraNum
					If sListaFiltrosFacturas.Length = 0 Then sListaFiltrosFacturas = "<b>" & Textos(146) & ":</b> " 'Facturas
					sListaFiltrosFacturas += Textos(219) & ": " & sValor & "; "
				Case TipoFiltro.FraEstado
					If sListaFiltrosFacturas.Length = 0 Then sListaFiltrosFacturas = "<b>" & Textos(146) & ":</b> " 'Facturas
					sListaFiltrosFacturas += Textos(145) & ": " & ddlEstadoFra.SelectedItem.Text & "; "
				Case TipoFiltro.FraFechaDesde
					If oFiltro.ValorFecha <> Date.MinValue Then
						If sListaFiltrosFacturas.Length = 0 Then sListaFiltrosFacturas = "<b>" & Textos(146) & ":</b> " 'Facturas
						sListaFiltrosFacturas += Textos(103) & " " & oFiltro.ValorFecha & "; " 'Desde:
					End If
				Case TipoFiltro.FraFechaHasta
					If oFiltro.ValorFecha <> Date.MinValue Then
						If sListaFiltrosFacturas.Length = 0 Then sListaFiltrosFacturas = "<b>" & Textos(146) & ":</b> " 'Facturas
						sListaFiltrosFacturas += Textos(104) & " " & oFiltro.ValorFecha & "; " 'Hasta:
					End If
				Case TipoFiltro.PagoNum
					If sListaFiltrosPagos.Length = 0 Then sListaFiltrosPagos = "<b>" & Textos(149) & ":</b> " 'Pagos
					sListaFiltrosPagos += Textos(219) & ": " & sValor & "; "
				Case TipoFiltro.PagoEstado
					If sListaFiltrosPagos.Length = 0 Then sListaFiltrosPagos = "<b>" & Textos(149) & ":</b> " 'Pagos
					sListaFiltrosPagos += Textos(151) & ": " & ddlEstadoPago.SelectedItem.Text & "; "
				Case TipoFiltro.PagoFechaDesde
					If oFiltro.ValorFecha <> Date.MinValue Then
						If sListaFiltrosPagos.Length = 0 Then sListaFiltrosPagos = "<b>" & Textos(149) & ":</b> " 'Pagos
						sListaFiltrosPagos += Textos(103) & " " & oFiltro.ValorFecha & "; " 'Desde:
					End If
				Case TipoFiltro.PagoFechaHasta
					If oFiltro.ValorFecha <> Date.MinValue Then
						If sListaFiltrosPagos.Length = 0 Then sListaFiltrosPagos = "<b>" & Textos(149) & ":</b> " 'Pagos
						sListaFiltrosPagos += Textos(104) & " " & oFiltro.ValorFecha & "; " 'Hasta:
					End If
				Case TipoFiltro.Categoria
					'Este if no debería ser necesario porque RellenarLabelFiltros() se usa después de 
					'SeleccionarFiltrosEnPanelBusqueda.
					If fsnTvwCategorias IsNot Nothing AndAlso fsnTvwCategorias.SelectedNode IsNot Nothing Then
						sListaFiltros += "<b>" & Textos(105) & ":</b> " & fsnTvwCategorias.SelectedNode.Text & "; "
					End If
				Case TipoFiltro.Gestor
					sListaFiltros += "<b>" & Textos(241) & ":</b> " & ddlGestor.SelectedItem.Text & "; "
					sListaFiltros += "<b>" & Textos(23) & ":</b> " & sValor & "; "
				Case TipoFiltro.AnyoPedAbierto
					sAnyoAbierto = sValor
				Case TipoFiltro.CestaPedAbierto
					sCestaAbierto = sValor
				Case TipoFiltro.PedidoPedAbierto
					sPedAbierto = sValor
				Case TipoFiltro.PedidoBorrado
					sListaFiltros += "<b>" & Textos(295) & "; "
			End Select
		Next
		If sAnyoAbierto <> "_" Or sCestaAbierto <> "_" Or sPedAbierto <> "_" Then
			sListaFiltroContraAbiertos = "<b>" & Textos(290) & " : </b>" & sAnyoAbierto & "/" & sCestaAbierto & "/" & sPedAbierto & ";"
		End If
		If sListaFiltrosEstado.Length > 0 Then sListaFiltrosEstado += "; "
		If sListaFiltrosPartidasPres.Length > 0 Then sListaFiltrosPartidasPres += "; "
		sListaFiltros += sListaFiltrosPartidasPres &
						sListaFiltrosEstado &
						sListaFiltrosClasesDePedido &
						sListaFiltrosAlbaranesBloquados &
						sListaFiltrosFacturas &
						sListaFiltrosPagos &
						sListaFiltroContraAbiertos
		If sListaFiltros.Length > 0 Then
			lblListaFiltros.Text = sListaFiltros
			divListaFiltros.Style("display") = "block"
		Else
			lblListaFiltros.Text = String.Empty
			divListaFiltros.Style("display") = "none"
		End If
		upListaFiltros.Update()
	End Sub
	''' Revisado por: blp. Fecha: 19/04/2012
	''' <summary>
	''' Método que va a recorrer los filtros existentes para seleccionar en cada elemento del panel de búsqueda 
	''' el valor de filtrado que le corresponda.
	''' Dado que los filtros se guardan en cookies y puede pasar tiempo antes de volver a usar la búsqueda,
	''' para cada caso se comprobará que efectivamente el filtro aún existe (por ejemplo, si hay una empresa en el filtro,
	''' que ésta aún está presente en el listado de empresas de los pedidos en seguimiento).
	''' En caso de que no esté, se elimina el filtro de la lista.
	''' Se comprueba que los filtros están en las opciones de filtro y que hay permisos para ver fras y pagos
	''' </summary>
	''' <remarks>Llamada desde GestionCookiesFiltroBusqueda e InicializarControles. Máx. 0,1 seg</remarks>
	Private Sub SeleccionarFiltrosEnPanelBusqueda()
		Dim _FiltrosAnyadidos As New List(Of Filtro)
		For Each oFiltro In FiltrosAnyadidos
			_FiltrosAnyadidos.Add(oFiltro)
		Next
		If FiltrosAnyadidos.Count > 0 Then
			For Each oFiltro As Filtro In FiltrosAnyadidos
				Select Case oFiltro.tipo
					Case TipoFiltro.Anio
						If oFiltro.Valor.ToString IsNot System.DBNull.Value Then
							If ddlAnio.Items.FindByValue(oFiltro.Valor) IsNot Nothing Then
								ddlAnio.SelectedValue = oFiltro.Valor
							Else
								_FiltrosAnyadidos.Remove(oFiltro)
							End If
						End If
					Case TipoFiltro.Cesta
						If oFiltro.Valor.ToString IsNot System.DBNull.Value Then
							txtCesta.Text = oFiltro.Valor.ToString
						End If
					Case TipoFiltro.NumPedido
						If oFiltro.Valor.ToString IsNot System.DBNull.Value Then
							txtPedido.Text = oFiltro.Valor.ToString
						End If
					Case TipoFiltro.FechaDesde
						If oFiltro.ValorFecha.ToString IsNot System.DBNull.Value AndAlso oFiltro.ValorFecha.ToString <> Date.MinValue Then
							dteDesde.Value = oFiltro.ValorFecha
						End If
					Case TipoFiltro.FechaHasta
						If oFiltro.ValorFecha.ToString IsNot System.DBNull.Value AndAlso oFiltro.ValorFecha.ToString <> Date.MinValue Then
							dteHasta.Value = oFiltro.ValorFecha
						End If
					Case TipoFiltro.FraFechaDesde
						If oFiltro.ValorFecha.ToString IsNot System.DBNull.Value AndAlso oFiltro.ValorFecha.ToString <> Date.MinValue Then
							dteFraFDesde.Value = oFiltro.ValorFecha
						End If
					Case TipoFiltro.FraFechaHasta
						If oFiltro.ValorFecha.ToString IsNot System.DBNull.Value AndAlso oFiltro.ValorFecha.ToString <> Date.MinValue Then
							dteFraFHasta.Value = oFiltro.ValorFecha
						End If
					Case TipoFiltro.PagoFechaDesde
						If oFiltro.ValorFecha.ToString IsNot System.DBNull.Value AndAlso oFiltro.ValorFecha.ToString <> Date.MinValue Then
							dtePagoFDesde.Value = oFiltro.ValorFecha
						End If
					Case TipoFiltro.PagoFechaHasta
						If oFiltro.ValorFecha.ToString IsNot System.DBNull.Value AndAlso oFiltro.ValorFecha.ToString <> Date.MinValue Then
							dtePagoFHasta.Value = oFiltro.ValorFecha
						End If
					Case TipoFiltro.Empresa
						If oFiltro.Valor.ToString IsNot System.DBNull.Value Then
							If ddlEmpresa.Items.FindByValue(oFiltro.Valor) IsNot Nothing Then
								ddlEmpresa.SelectedValue = oFiltro.Valor
							Else
								_FiltrosAnyadidos.Remove(oFiltro)
							End If
						End If
					Case TipoFiltro.Receptor
						If oFiltro.Valor.ToString IsNot System.DBNull.Value Then
							If ddlReceptor.Items.FindByValue(oFiltro.Valor) IsNot Nothing Then
								ddlReceptor.SelectedValue = oFiltro.Valor
							Else
								_FiltrosAnyadidos.Remove(oFiltro)
							End If
						End If
					Case TipoFiltro.Proveedor
						If oFiltro.Valor.ToString IsNot System.DBNull.Value Then
							Dim oProveedores As CProveedores = FSNServer.Get_Object(GetType(FSNServer.CProveedores))
							Dim dtCodProve As DataTable = oProveedores.CrearTablaProveCods()
							Dim drCodProve As DataRow = dtCodProve.NewRow
							drCodProve("COD") = oFiltro.Valor
							dtCodProve.Rows.InsertAt(drCodProve, 0)
							Dim dtProve As DataTable = oProveedores.DevolverDatosProveedores(Usuario.Idioma, dtCodProve)
							Dim sProveDen As String = String.Empty
							If dtProve IsNot Nothing AndAlso dtProve.Rows.Count > 0 Then
								sProveDen = dtProve.Rows(0).Item("DEN")
							End If
							If Not String.IsNullOrEmpty(sProveDen) Then
								txtProveedor.Text = sProveDen
								hidProveedor.Value = oFiltro.Valor
							Else
								_FiltrosAnyadidos.Remove(oFiltro)
							End If
						End If
					Case TipoFiltro.CentroCoste
						If oFiltro.Valor.ToString IsNot System.DBNull.Value Then
							tbCtroCoste.Text = oFiltro.Info
							tbCtroCoste_Hidden.Value = oFiltro.Valor
						End If
					Case TipoFiltro.FraEstado
						If Me.Acceso.g_bAccesoFSFA AndAlso
							oFiltro.Valor.ToString IsNot System.DBNull.Value AndAlso
							ddlEstadoFra.Items.FindByValue(oFiltro.Valor) IsNot Nothing Then

							ddlEstadoFra.SelectedValue = oFiltro.Valor
						Else
							_FiltrosAnyadidos.Remove(oFiltro)
						End If

					Case TipoFiltro.PagoEstado
						If Me.Acceso.g_bAccesoFSFA AndAlso
							oFiltro.Valor.ToString IsNot System.DBNull.Value AndAlso
							ddlEstadoPago.Items.FindByValue(oFiltro.Valor) IsNot Nothing Then

							ddlEstadoPago.SelectedValue = oFiltro.Valor
						Else
							_FiltrosAnyadidos.Remove(oFiltro)
						End If
					Case TipoFiltro.TipoPedido
						If oFiltro.Valor.ToString IsNot System.DBNull.Value AndAlso
							ddlTipoPedido.Items.FindByValue(oFiltro.Valor) IsNot Nothing Then
							ddlTipoPedido.SelectedValue = oFiltro.Valor
						Else
							_FiltrosAnyadidos.Remove(oFiltro)
						End If
					Case TipoFiltro.ProveedorERP
						If Not String.IsNullOrEmpty(oFiltro.Valor.ToString) Then
							txtProveedorERP.Text = oFiltro.Valor
						Else
							_FiltrosAnyadidos.Remove(oFiltro)
						End If
					Case TipoFiltro.PartidaPresupuestaria
						If oFiltro.Info.Contains("@@@") Then
							Dim arrInfo() = oFiltro.Info.Split("@@@")
							Dim sPartidaPresIDParcial As String = arrInfo(0)
							Dim sPartidaDen As String = arrInfo(1)
							Dim tbPartidaPresID As String = "tbPPres_" & oFiltro.Info
							If phrPartidasPresBuscador IsNot Nothing _
							AndAlso phrPartidasPresBuscador.FindControl("tbDdlPP") IsNot Nothing _
							AndAlso phrPartidasPresBuscador.FindControl("tbDdlPP").FindControl(tbPartidaPresID) IsNot Nothing Then
								Dim tbPartidaPres As TextBox = TryCast(phrPartidasPresBuscador.FindControl("tbDdlPP").FindControl(tbPartidaPresID), TextBox)
								Dim sPartidaHiddenID As String = tbPartidaPresID.Replace("tbPPres_", "tbPPres_Hidden_")
								Dim tbPartidaPresHidden As HiddenField = TryCast(phrPartidasPresBuscador.FindControl("tbDdlPP").FindControl(sPartidaHiddenID), HiddenField)
								If Not String.IsNullOrEmpty(oFiltro.Valor.ToString) AndAlso
									tbPartidaPres IsNot Nothing Then
									tbPartidaPres.Text = sPartidaDen
									tbPartidaPresHidden.Value = oFiltro.Valor
								Else
									_FiltrosAnyadidos.Remove(oFiltro)
								End If
							End If
						Else
							_FiltrosAnyadidos.Remove(oFiltro)
						End If
					Case TipoFiltro.Estado
						If chklstEstado IsNot Nothing AndAlso
							Not String.IsNullOrEmpty(oFiltro.Valor.ToString) Then

							For Each l As ListItem In chklstEstado.Items
								If (CType(oFiltro.Valor, CPedido.Estado) And CType(l.Value, CPedido.Estado)) = CType(l.Value, CPedido.Estado) Then
									l.Selected = True
								Else
									l.Selected = False
								End If
							Next
						Else
							_FiltrosAnyadidos.Remove(oFiltro)
						End If
					Case TipoFiltro.Tipo
						'PedidosGS
						If chklstOtrosFiltros IsNot Nothing AndAlso
							Not oFiltro.Valores Is Nothing Then
							For Each l As ListItem In chklstOtrosFiltros.Items
								'Abonos
								If l.Value = CPedido.OtrosAtributosPedido.Abono Then
									l.Selected = oFiltro.Valores(4)
									'EP no libres
								ElseIf l.Value = CPedido.OtrosAtributosPedido.CatalogadoNegociado Then
									l.Selected = oFiltro.Valores(1)
									'EP LIBRES
								ElseIf l.Value = CPedido.OtrosAtributosPedido.CatalogadoLibres Then
									l.Selected = oFiltro.Valores(2)
									'GS
								ElseIf l.Value = CPedido.OtrosAtributosPedido.Negociado Then
									l.Selected = oFiltro.Valores(0)
									'ERP
								ElseIf l.Value = CPedido.OtrosAtributosPedido.Directos AndAlso
								Session("HayIntegracionEntradaoEntradaSalida") = True Then
									l.Selected = oFiltro.Valores(3)
								ElseIf l.Value = CPedido.OtrosAtributosPedido.ContraAbiertos AndAlso Acceso.gbUsarPedidosAbiertos Then
									l.Selected = oFiltro.Valores(5)
								End If
							Next
						Else
							_FiltrosAnyadidos.Remove(oFiltro)
						End If
					Case TipoFiltro.Categoria
						If fsnTvwCategorias IsNot Nothing AndAlso
							Not String.IsNullOrEmpty(oFiltro.Valor.ToString) Then
							If fsnTvwCategorias.FindNode(oFiltro.Info) IsNot Nothing Then
								fsnTvwCategorias.FindNode(oFiltro.Info).Selected = True
								Dim e As New System.EventArgs
								fsntvwCategorias_SelectedNodeChanged(fsnTvwCategorias, e)
							Else
								_FiltrosAnyadidos.Remove(oFiltro)
							End If
						End If
					Case TipoFiltro.NumMeses
						If igNumMeses IsNot Nothing AndAlso
							Not String.IsNullOrEmpty(oFiltro.Valor.ToString) AndAlso
							CInt(oFiltro.Valor) >= 1 Then
							igNumMeses.Value = CInt(oFiltro.Valor)
						Else
							_FiltrosAnyadidos.Remove(oFiltro)
						End If
					Case TipoFiltro.PedidosConAlbaranesBloqueados
						chkPedidosBloqueados.Checked = CBool(oFiltro.Valor)
					Case TipoFiltro.Gestor
						If ddlGestor.Items.FindByValue(oFiltro.Valor) IsNot Nothing Then
							ddlGestor.SelectedValue = oFiltro.Valor
						Else
							_FiltrosAnyadidos.Remove(oFiltro)
						End If
					Case TipoFiltro.Articulo
						txtArticulo.Text = oFiltro.Valor
					Case TipoFiltro.Usu_Ped
						chkSeguimientoPedidos.Items(0).Selected = Convert.ToBoolean(oFiltro.Valor)
					Case TipoFiltro.CC_Ped
						chkSeguimientoPedidos.Items(1).Selected = Convert.ToBoolean(oFiltro.Valor)
					Case TipoFiltro.NumPedidoProve
						txtPedidoProve.Text = oFiltro.Valor
					Case TipoFiltro.FraAnio
						txtAnioFra.Text = oFiltro.Valor
					Case TipoFiltro.FraNum
						txtNumFra.Text = oFiltro.Valor
					Case TipoFiltro.PagoNum
						txtNumPago.Text = oFiltro.Valor
					Case TipoFiltro.FraEstado
						ddlEstadoFra.Text = oFiltro.Valor
					Case TipoFiltro.PagoEstado
						ddlEstadoPago.Text = oFiltro.Valor
					Case TipoFiltro.AnyoPedAbierto
						ddlAnyoAbierto.Text = oFiltro.Valor
					Case TipoFiltro.CestaPedAbierto
						txtCestaAbierto.Text = oFiltro.Valor
					Case TipoFiltro.PedidoPedAbierto
						txtPedidoPedAbierto.Text = oFiltro.Valor
					Case TipoFiltro.PedidoBorrado
						chkBorrado.Checked = CBool(oFiltro.Valor)
				End Select
			Next
			FiltrosAnyadidos.Clear()
			FiltrosAnyadidos = _FiltrosAnyadidos
			pnlBuscador.Update()
		End If
	End Sub
#Region "Integracion"
	''' <summary>
	''' Integracion de pedidos
	''' </summary>
	Private ReadOnly Property HayIntegracionPedidos() As Boolean
		Get
			If Session("HayIntegracionPedidos") Is Nothing Then
				Session("HayIntegracionPedidos") = False
				Dim oEP_ValidacionesIntegracion As EP_ValidacionesIntegracion = Me.FSNServer.Get_Object(GetType(EP_ValidacionesIntegracion))
				If oEP_ValidacionesIntegracion.HayIntegracion(TablasIntegracion.PED_Aprov) Then
					Session("HayIntegracionPedidos") = True
				End If
			End If
			If CBool(Session("hayIntegracionPedidos")) Then
				Return True
			Else
				Return False
			End If
		End Get
	End Property
#End Region
#Region "Datalist dlAlbaranes"
	Private Sub dlAlbaranes_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlAlbaranes.ItemDataBound
		With e.Item
			Dim fila As DataRowView = CType(.DataItem, DataRowView)
			Select Case .ItemType
				Case ListItemType.Item, ListItemType.AlternatingItem
					'Comprobar si el campo estado el albaran no es vacio , si es 1 el albaran esta bloqueado para facturacion 
					If Not IsDBNull(fila.Item("ALB_BLOQUEADO")) Then
						If fila.Item("ALB_BLOQUEADO") = 1 Then
							CType(.FindControl("ImageBloqueoAlbaran"), Image).Visible = True
							CType(.FindControl("lblBloqueoAlbaran"), Label).Visible = True
						End If
					End If

					CType(.FindControl("lblAlbaran"), Label).Text = Textos(252) '"Albaran XXXX"
					CType(.FindControl("lblBloqueoAlbaran"), Label).Text = Textos(262) '"Albaran bloqueado para facturacion"
					CType(.FindControl("litAlbFechaRecep"), Label).Text = Textos(253) '"Fecha de recepciÃ³n :"
					If FSNServer.TipoAcceso.gbMostrarFechaContable Then
						CType(.FindControl("tbcFechaContable"), TableCell).Visible = True
						ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_RecepcionPedidos
						CType(.FindControl("litAlbFechaContable"), Label).Text = Textos(279) & ":" '"Fecha CONTABLE :"
						If IsDate(fila.Item("FEC_CONTABLE")) Then CType(.FindControl("litFechaContable"), Literal).Text = FSNLibrary.FormatDate(fila.Item("FEC_CONTABLE"), Me.Usuario.DateFormat)
						ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
                    End If
                    CType(.FindControl("litAlbTotalImporte"), Label).Text = Textos(256) '"Importe :"
                    CType(.FindControl("litAlbMonTotalImporte"), Label).Text = fila.Item("MON").ToString()
                    CType(.FindControl("litAlbReceptor"), Label).Text = Textos(255) '"Receptor :"
                    CType(.FindControl("litFechaRecep"), Literal).Text = FSNLibrary.FormatDate(fila.Item("FECHA_RECEPCION"), Me.Usuario.DateFormat)
                    Dim oCParametros As FSNServer.CParametros = FSNServer.Get_Object(GetType(FSNServer.CParametros))
                    If Acceso.gbCodRecepERP Then
                        CType(.FindControl("litAlbNumRecepErp"), Label).Visible = True
                        CType(.FindControl("litNumRecepErp"), Literal).Visible = True
                        CType(.FindControl("litAlbNumRecepErp"), Label).Text = oCParametros.CargarLiteralParametros(TiposDeDatos.LiteralesParametros.RecepcionERP, Me.Usuario.Idioma) '"NÂº recepciÃ³n ERP :"
                        If IsDBNull(fila.Item("NUM_ERP")) Then
                            CType(.FindControl("litNumRecepErp"), Literal).Text = "0"
                        End If
                    End If
                    If IsDBNull(fila.Item("RECEPTOR")) Then
                        CType(.FindControl("litAlbReceptor"), Label).Visible = False
                        CType(.FindControl("litReceptor"), Literal).Visible = False
                    End If

                    Dim idOrden As Integer = CType(dlAlbaranes.Attributes.Item("IdOrden"), Integer)
                    CType(.FindControl("dlLineasAlbaran"), DataList).Attributes.Add("MON", fila.Item("MON"))
                    CType(.FindControl("dlLineasAlbaran"), DataList).Attributes.Add("IMPORTETOTALALBARAN", 0)
                    CType(.FindControl("dlLineasAlbaran"), DataList).DataSource = DsetAlbaranes(idOrden).Tables("ALBARANES").Rows.Find(fila.Item("ALBARAN")).GetChildRows("REL_ALBARANES_LINEASALBARAN").CopyToDataTable()
                    CType(.FindControl("dlLineasAlbaran"), DataList).DataBind()
                    Dim FilasImportes() As DataRow = DsetAlbaranes(idOrden).Tables("ALBARANES").Rows.Find(fila.Item("ALBARAN")).GetChildRows("REL_ALBARANES_LINEASALBARAN")
                    Dim ImporteTotalAlbaran As Double = 0
                    For Each row In FilasImportes
                        ImporteTotalAlbaran = ImporteTotalAlbaran + CType(row.Item("IMPORTE_ARTICULO"), Double)
                    Next
                    CType(.FindControl("litTotalImporte"), Literal).Text = FSNLibrary.FormatNumber(ImporteTotalAlbaran.ToString(), Me.Usuario.NumberFormat)
            End Select

        End With
    End Sub
    Public Sub dlLineasAlbaran_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        With e.Item
            Dim fila As DataRowView = CType(.DataItem, DataRowView)
            Dim tipoRecep As Integer = DirectCast(sender, DataList).DataSource.Rows(0).Item("TIPORECEPCION")
            Select Case .ItemType
                Case ListItemType.Header
                    CType(.FindControl("litLinArticulo"), Literal).Text = Textos(257) '"Articulo"
                    CType(.FindControl("litLinCantidadPedida"), Literal).Text = Textos(258) '"Cantidad pedida"
                    CType(.FindControl("litLinCantidadRecibida"), Literal).Text = Textos(259) '"Cantidad recibida"
                    CType(.FindControl("litLinPrecioUnitario"), Literal).Text = Textos(260) '"Precio unitario"
                    CType(.FindControl("litLinImporte"), Literal).Text = Textos(261) '"Importe"
                    CType(.FindControl("litLinImportePedido"), Literal).Text = Textos(266) '"Importe Pedido"
                    Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_RecepcionPedidos
                    CType(.FindControl("litLinImporteRecibido"), Literal).Text = Textos(269) '"Importe Recibido"
                    Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
                    If tipoRecep = 0 Then
                        CType(.FindControl("litLinArticulo"), Literal).Visible = True
                        CType(.FindControl("litLinCantidadPedida"), Literal).Visible = True
                        CType(.FindControl("litLinCantidadRecibida"), Literal).Visible = True
                        CType(.FindControl("litLinPrecioUnitario"), Literal).Visible = True
                        CType(.FindControl("litLinImporte"), Literal).Visible = True
                        CType(.FindControl("litLinImportePedido"), Literal).Visible = False
                        CType(.FindControl("litLinImporteRecibido"), Literal).Visible = False
                    Else
                        CType(.FindControl("litLinArticulo"), Literal).Visible = False
                        CType(.FindControl("litLinCantidadPedida"), Literal).Visible = False
                        CType(.FindControl("litLinCantidadRecibida"), Literal).Visible = False
                        CType(.FindControl("litLinPrecioUnitario"), Literal).Visible = False
                        CType(.FindControl("litLinImporte"), Literal).Visible = False
                        CType(.FindControl("litLinImportePedido"), Literal).Visible = True
                        CType(.FindControl("litLinImporteRecibido"), Literal).Visible = True
                    End If

                Case ListItemType.Item, ListItemType.AlternatingItem

                    Dim oUnidadPedidoLinea As CUnidadPedido = Nothing
                    If Not DBNullToSomething(fila.Item("UNIDAD")) = Nothing Then
                        oUnidadPedidoLinea = TodasLasUnidades.Item(Trim(fila.Item("UNIDAD")))
                        oUnidadPedidoLinea.AsignarFormatoaUnidadPedido(Me.Usuario.NumberFormat)
                    End If
                    If Not oUnidadPedidoLinea Is Nothing Then
                        If oUnidadPedidoLinea.NumeroDeDecimales Is Nothing Then
                            Dim decimales As Integer = NumeroDeDecimales(DBNullToStr(fila.Item("CANT_PED")))
                            oUnidadPedidoLinea.UnitFormat.NumberDecimalDigits = decimales
                        End If
                        If fila.Item("TIPORECEPCION") = 0 Then
                            Dim sCantidad As String = ""
                            If Not IsDBNull(fila.Item("CANT_PED")) Then
                                sCantidad = FSNLibrary.FormatNumber(fila.Item("CANT_PED"), oUnidadPedidoLinea.UnitFormat)
                            End If
                            CType(.FindControl("litItemCantidadPedida"), Label).Text = sCantidad
                            sCantidad = ""
                            If Not IsDBNull(fila.Item("CANT_REC")) Then
                                sCantidad = FSNLibrary.FormatNumber(fila.Item("CANT_REC"), oUnidadPedidoLinea.UnitFormat)
                            End If
                            CType(.FindControl("LitItemCantidadRecibida"), Label).Text = sCantidad
                        End If
                    End If

                    If fila.Item("TIPORECEPCION") = 0 Then
                        CType(.FindControl("lblItemImportePedido"), System.Web.UI.WebControls.Label).Visible = False
                        CType(.FindControl("lblMonImportePedido"), System.Web.UI.WebControls.Label).Visible = False
                        CType(.FindControl("lblItemImporteRecibido"), System.Web.UI.WebControls.Label).Visible = False
                        CType(.FindControl("lblMonImporteRecibido"), System.Web.UI.WebControls.Label).Visible = False
                        CType(.FindControl("litItemCantidadPedida"), System.Web.UI.WebControls.Label).Visible = True
                        CType(.FindControl("FSNlnkUniCantPdte"), Fullstep.FSNWebControls.FSNLinkTooltip).Visible = True
                        CType(.FindControl("LitItemCantidadRecibida"), System.Web.UI.WebControls.Label).Visible = True
                        CType(.FindControl("FSNLinkUniCantRec"), Fullstep.FSNWebControls.FSNLinkTooltip).Visible = True
                        CType(.FindControl("ListItemImporteLinea"), System.Web.UI.WebControls.Label).Visible = True
                        CType(.FindControl("lblMonImporteLinea"), System.Web.UI.WebControls.Label).Visible = True
                        CType(.FindControl("LisItemprecioUnitario"), System.Web.UI.WebControls.Label).Visible = True
                        CType(.FindControl("lblMonPrecUniLinea"), System.Web.UI.WebControls.Label).Visible = True
                        CType(.FindControl("LisItemprecioUnitario"), Label).Text = FSNLibrary.FormatNumber(fila.Item("PRECIOUNITARIO"), Me.Usuario.NumberFormat)
                        CType(.FindControl("ListItemImporteLinea"), Label).Text = FSNLibrary.FormatNumber(fila.Item("IMPORTE_ARTICULO"), Me.Usuario.NumberFormat)
                        CType(.FindControl("lblMonPrecUniLinea"), Label).Text = CType(sender, DataList).Attributes("MON").ToString
                        CType(.FindControl("lblMonImporteLinea"), Label).Text = CType(sender, DataList).Attributes("MON").ToString
                    Else
                        CType(.FindControl("litItemCantidadPedida"), System.Web.UI.WebControls.Label).Visible = False
                        CType(.FindControl("FSNlnkUniCantPdte"), Fullstep.FSNWebControls.FSNLinkTooltip).Visible = False
                        CType(.FindControl("LitItemCantidadRecibida"), System.Web.UI.WebControls.Label).Visible = False
                        CType(.FindControl("FSNLinkUniCantRec"), Fullstep.FSNWebControls.FSNLinkTooltip).Visible = False
                        CType(.FindControl("ListItemImporteLinea"), System.Web.UI.WebControls.Label).Visible = False
                        CType(.FindControl("lblMonImporteLinea"), System.Web.UI.WebControls.Label).Visible = False
                        CType(.FindControl("LisItemprecioUnitario"), System.Web.UI.WebControls.Label).Visible = False
                        CType(.FindControl("lblMonPrecUniLinea"), System.Web.UI.WebControls.Label).Visible = False
                        CType(.FindControl("lblItemImporteRecibido"), System.Web.UI.WebControls.Label).Visible = True
                        CType(.FindControl("lblItemImporteRecibido"), System.Web.UI.WebControls.Label).Text = FSNLibrary.FormatNumber(fila.Item("IMPORTE_ARTICULO"), Me.Usuario.NumberFormat)
                        CType(.FindControl("lblMonImporteRecibido"), System.Web.UI.WebControls.Label).Visible = True
                        CType(.FindControl("lblMonImporteRecibido"), Label).Text = CType(sender, DataList).Attributes("MON").ToString
                        CType(.FindControl("lblItemImportePedido"), System.Web.UI.WebControls.Label).Visible = True
                        CType(.FindControl("lblItemImportePedido"), System.Web.UI.WebControls.Label).Text = FSNLibrary.FormatNumber(fila.Item("IMPORTE_PED"), Me.Usuario.NumberFormat)
                        CType(.FindControl("lblMonImportePedido"), System.Web.UI.WebControls.Label).Visible = True
                        CType(.FindControl("lblMonImportePedido"), Label).Text = CType(sender, DataList).Attributes("MON").ToString
                    End If

            End Select

        End With
    End Sub
#End Region
    ''' Revisado por: blp. Fecha: 17/10/2012
    ''' <summary>
    ''' Método que recarga el grid de órdenes
    ''' ATENCIÓN: En esta función NO SE BORRA/RECARGA LA CACHÉ DE ÓRDENES, sólo se recarga el grid.
    ''' Para borrar la caché es necesario usar EliminarCacheDePedidos o EliminarCachePedidos
    ''' </summary>
    ''' <param name="PageNumber">Número de página a devolver. El valor mínimo es 1</param>
    ''' <param name="bindSource">Indica si hacemos Databind del control dlOrdenes (el grid de órdenes) o no</param>
    ''' <param name="updateGrid">Indica si hacemos Update del control pnlGrid (el updatepanel que contiene el grid de órdenes)</param>
    ''' <param name="bActualizarFiltros">Indica si queremos actualizar el listado de filtros. En teoría, si el usuario no pulsa "Buscar", quiere seguir con los mismos filtros aunque haya cambiado los filtros en el buscador, de modo que sólo debería pasarse TRUE cuando se pulse "Buscar"</param>
    ''' <remarks>Llamada desde Seguimiento.aspx.vb. Máx: Depende de si recurre al stored de órdenes</remarks>
    Private Sub recargarGrid(ByVal PageNumber As Nullable(Of Integer), ByVal bindSource As Boolean, ByVal updateGrid As Boolean, ByVal bActualizarFiltros As Boolean)
        dlOrdenes.DataSource = ActualizarDsetpedidos(bActualizarFiltros)
        If bindSource Then dlOrdenes.DataBind()
        If updateGrid Then
            pnlGrid.Update()
            'Cuando se actualice el grid vamos a borrar la lista que tenemos guardada en javascript con las órdenes que ya se han cargado y se han guardado en caché.
            ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "BorrarArraysOrdenesCargadasYAbiertas", "BorrarArraysOrdenesCargadasYAbiertas();", True)
        End If

    End Sub
    ''' Revisado por: blp. Fecha: 30/08/2013
    ''' <summary>
    ''' registramos el script que hace que txtProveedor sea de solo lectura
    ''' </summary>
    ''' <remarks>Llamada desde Seguimiento.aspx.vb. Maximo 0,1 seg.</remarks>
    Public Sub CargarScriptProveedor()
        'Cargamos el script en la cabecera.master, para asegurarnos de que
        'se carga una vez se ha cargado el JQuery, que actualmente se carga en la cabecera
        Dim sScript As New StringBuilder
        sScript.Append("$(window).ready(function(){")
        sScript.Append(vbCrLf)
        sScript.Append("    setReadonlyProveedor();")
        sScript.Append(vbCrLf)
        sScript.Append("});")
        sScript.Append(vbCrLf)

        Dim oScript As New HtmlGenericControl("script")
        oScript.InnerHtml = sScript.ToString
        Dim MenuMaster As Page = Me.Master.Page
        Dim CabeceraMaster As Page = MenuMaster.Master.Page
        CabeceraMaster.Controls.Add(oScript)
    End Sub
    ''' <summary>
    ''' Prepara la caché con la ordenes a exportar 
    ''' </summary>
    ''' <param name="ds">dataset del qeu se toman las ordenes a mostrar</param>
    ''' <remarks></remarks>
    Private Sub prepararOrdenesExportacion(ByVal ds As DataSet)
        'jim:tarea 3365 -- A la página descargas le vamos a pasar un datatable con la orden seleccionada (si se ha seleccionado alguna) o en caso
        ' contrario de TODAS las ordenes que aparecen según los criterios de búsqueda y este dtable es el que vamos a guardar en caché para ser
        ' leido desde Descargas.aspx
        Dim dt As New DataTable
        dt.Columns.Add("ID", GetType(Integer))
        For Each dr As DataRow In ds.Tables(0).Rows
            dt.Rows.Add(dr("ID"))
        Next
        Me.InsertarEnCache("dsReportEP" & Me.Usuario.Cod, dt)
        Me.InsertarEnCache("VerOrdenesdeBaja" & Me.Usuario.Cod, IIf(chkBorrado.Checked, 1, 0))
    End Sub
    ''' <summary>
    ''' Prepara el datatable con la ordenes a exportar 
    ''' </summary>
    ''' <param name="idOrden">id. de la orden a exportar</param>
    ''' <remarks></remarks>
    Private Sub prepararOrdenesExportacion(ByVal idOrden As String)
        Dim dt As New DataTable
        dt.Columns.Add("ID", GetType(Integer))
        dt.Rows.Add(idOrden)
        Me.InsertarEnCache("dsReportEP" & Me.Usuario.Cod, dt)
        Me.InsertarEnCache("VerOrdenesdeBaja" & Me.Usuario.Cod, IIf(chkBorrado.Checked, 1, 0))
    End Sub
End Class