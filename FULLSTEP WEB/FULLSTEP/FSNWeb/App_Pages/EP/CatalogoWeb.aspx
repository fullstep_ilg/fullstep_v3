<%@ Page Title="" Language="vb" AutoEventWireup="false" EnableViewState="true" MasterPageFile="~/App_Master/CabCatalogo.master" CodeBehind="CatalogoWeb.aspx.vb" Inherits="Fullstep.FSNWeb.CatalogoWeb" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/App_Master/CabCatalogo.master"  %>
<asp:Content ID="content1" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">
    function quitarDescripcion(control, args) {
        var texto = document.getElementById("<%=tbCodProveBusq.ClientID%>").value;
        texto = texto.substr(texto, texto.indexOf(" - "));
        if (texto != "") {
            document.getElementById("<%=tbCodProveBusq.ClientID%>").value = texto;
        }
    }

    function quitarCodigo(control, args) {
        var texto = document.getElementById("<%=tbDenProveBusq.ClientID%>").value;
        texto = texto.slice(texto.indexOf(" - ") + 3);
        if (texto != "") {
            document.getElementById("<%=tbDenProveBusq.ClientID%>").value = texto;
        }
    }
     
    var FSNPeticionesCesta = new Array()

    //<%--''' Revisado por: blp. Fecha: 15/11/2011
    //''' Funci�n que a�ade el art�culo a la cesta
    //''' lineaID: Id del control l�nea (el art�culo) que queremos guardar en la cesta
    //''' numPrecioID: Id del control que contiene el precio a guardar en la cesta
    //''' numCantidadID: Id del control que contiene la cantidad a guardar en la cesta
    //''' msgCantidadMayorQueCero: Mensaje a mostrar al usuario en caso de que la cantidad sea igual o inferior a cero
    //''' Llamada desde: l�neas del cat�logo. M�x: 0,1 seg--%>
    function FSNAnyadirACesta(lineaID, numPrecioID, numCantidadID, msgCantidadMayorQueCero, txtArtDemID, tipoRecepcion ) {        
        var peticion = 1;
        if (FSNPeticionesCesta.length > 0) peticion = FSNPeticionesCesta[FSNPeticionesCesta.length - 1] + 1;
        
        if (tipoRecepcion == 0) {
            var precio = ($get(numPrecioID) != null) ? $get(numPrecioID).innerHTML : 0;
            var cantidad = ($get(numCantidadID) != null) ? $get(numCantidadID).innerHTML : 0;
            precio = precio.replace(' ', '');
            precio = precio.replace(',', '.');
            while (precio.indexOf(".") != precio.lastIndexOf(".")) {
                precio = precio.substring(0, precio.indexOf(".") - 1) + precio.slice(precio.indexOf(".") + 1);
            }
            cantidad = cantidad.replace(' ', '');
            cantidad = cantidad.replace(',', '.');
            while (cantidad.indexOf(".") != cantidad.lastIndexOf(".")) {
                cantidad = cantidad.substring(0, cantidad.indexOf(".") - 1) + cantidad.slice(cantidad.indexOf(".") + 1);
            }
        }
        else {
            var precio = ($get(numPrecioID) != null) ? $get(numPrecioID).innerHTML : 0;
            var cantidad = 1;
            precio = precio.replace(' ', '');
            precio = precio.replace(',', '.');
            while (precio.indexOf(".") != precio.lastIndexOf(".")) {
                precio = precio.substring(0, precio.indexOf(".") - 1) + precio.slice(precio.indexOf(".") + 1);
            }
        
        }
        if (cantidad > 0) {
            var txtArtDen = ($get(txtArtDemID) != null) ? $get(txtArtDemID).value : null;
            PageMethods.AnyadirACesta(peticion, lineaID, precio, cantidad, txtArtDen, AsyncCestaOk);
            FSNPeticionesCesta.push(peticion);
            FSNWebPartsCesta.cambiarmodo('actualizando');
        }
        else {
            alert(msgCantidadMayorQueCero);
        }
    }    //<%--''' Revisado por: blp. Fecha: 15/11/2011
    //''' Funci�n que a�ade el art�culo a la cesta
    //''' lineaID: Id del control l�nea (el art�culo) que queremos guardar en la cesta
    //''' numPrecioID: Id del control que contiene el precio a guardar en la cesta
    //''' numCantidadID: Id del control que contiene la cantidad a guardar en la cesta
    //''' msgCantidadMayorQueCero: Mensaje a mostrar al usuario en caso de que la cantidad sea igual o inferior a cero
    //''' Llamada desde: l�neas del cat�logo. M�x: 0,1 seg--%>
    function FSNAnyadirACestaFav(lineaID, numPrecioID, numCantidadID, msgCantidadMayorQueCero, txtArtDemID, tipoRecepcion) {
        var peticion = 1;
        if (FSNPeticionesCesta.length > 0) peticion = FSNPeticionesCesta[FSNPeticionesCesta.length - 1] + 1;

        if (tipoRecepcion == 0) {
            var precio = ($get(numPrecioID) != null) ? $get(numPrecioID).innerHTML : 0;
            var cantidad = ($get(numCantidadID) != null) ? $get(numCantidadID).innerHTML : 0;
            precio = precio.replace(' ', '');
            precio = precio.replace(',', '.');
            while (precio.indexOf(".") != precio.lastIndexOf(".")) {
                precio = precio.substring(0, precio.indexOf(".") - 1) + precio.slice(precio.indexOf(".") + 1);
            }
            cantidad = cantidad.replace(' ', '');
            cantidad = cantidad.replace(',', '.');
            while (cantidad.indexOf(".") != cantidad.lastIndexOf(".")) {
                cantidad = cantidad.substring(0, cantidad.indexOf(".") - 1) + cantidad.slice(cantidad.indexOf(".") + 1);
            }
        }
        else {
            var precio = ($get(numPrecioID) != null) ? $get(numPrecioID).innerHTML : 0;
            var cantidad = 1;
            precio = precio.replace(' ', '');
            precio = precio.replace(',', '.');
            while (precio.indexOf(".") != precio.lastIndexOf(".")) {
                precio = precio.substring(0, precio.indexOf(".") - 1) + precio.slice(precio.indexOf(".") + 1);
            }

        }
        if (cantidad > 0) {
            var txtArtDen = ($get(txtArtDemID) != null) ? $get(txtArtDemID).value : null;
            PageMethods.AnyadirACestaFav(peticion, lineaID, precio, cantidad, txtArtDen, AsyncCestaOk);
            FSNPeticionesCesta.push(peticion);
            FSNWebPartsCesta.cambiarmodo('actualizando');
        }
        else {
            alert(msgCantidadMayorQueCero);
        }
    }

    function AsyncCestaOk(msg) {
        var datos = msg.split('##');
        for (var i = 0; i < FSNPeticionesCesta.length; i++) {
            if (FSNPeticionesCesta[i] == datos[0]) FSNPeticionesCesta.splice(i, 1);
            if (FSNPeticionesCesta.length == 0) FSNWebPartsCesta.cambiarmodo('cesta');
        }
        if (datos.length == 3) {
            var datos = msg.split('##');
            if (datos[1] != '' && datos[2] != '') FSNWebPartsCesta.actualizarvalores(datos[1], datos[2]);
        }
        else {
            if (msg != '') alert(datos[1]);
        }
    }

    /*<%--
    Revisado por: blp. Fecha: 07/06/2012
    A�ade el art�culo seleccionado a la comparativa.
    Llama a un m�todo web que devuelve el id de la imagen pulsada para a�adir a la comparativa el art�culo y el n�mero de registros a�adidos para comparar
    Si la llamada al m�todo web es exitosa, se llama a la funci�n AsyncComparativaOk.
    Llamada desde evento onClick en cada imagen para a�adir a comparativa de cada art�culo (ImgComparativa).
    --%>*/
    function FSNAnyadirAComparativa(lineaID, ctrlimagen) {
        PageMethods.AnyadirAComparativa(ctrlimagen.id, lineaID, AsyncComparativaOk);
    }

    /*<%--
    Revisado por: blp. Fecha: 07/06/2012
    Llamada desde FSNAnyadirAComparativa.
    Con la informaci�n que ha proporcionado el m�todo web llamado en FSNAnyadirAComparativa:
    Modifica la imagen pulsada para a�adir a la comparativa el art�culo y la deshabilita.
    Modifica la pesta�a de comparativa para mostrar el total de art�culos a�adidos hasta el momento
    --%>*/
    function AsyncComparativaOk(msg) {
        var datos = msg.split('##');
        if (datos.length == 2) {
            var ctrlimg = document.getElementById(datos[0]);
            if (ctrlimg != null) {
                ctrlimg.src = ctrlimg.src.replace('comparar.gif', 'comparar_desactivado.gif');
                ctrlimg.disabled = true;
                ctrlimg.title = null;
                ctrlimg.onclick = null;
                //var tabcomp = $get(FSNTabComparativaID);
                var tabcomp = document.getElementById(FSNTabComparativaID);
                var iniTextoDis = 0;
                var iniTextoEn = 0;
                /*<%--IE9 est� dando un problema raro con los caracteres especiales (\n \r) por lo que vamos a asegurarnos de que 
                no los hay al calcular d�nde poner el n�mero de art�culos comparados en la pesta�a de la comparativa--%>*/
                if (navigator.appVersion.indexOf('MSIE') > -1) {
                    var sAppVersion = navigator.appVersion.slice(navigator.appVersion.indexOf('MSIE') + 4);
                    var iVersion = sAppVersion.substring(0, sAppVersion.indexOf(';'));
                    if (!isNaN(iVersion) && iVersion >= 9) {
                        iniTextoEn = tabcomp.innerHTML.indexOf(tabcomp.innerText.replace(/[\n\r]/gi, ''));
                        iniTextoDis = iniTextoEn + tabcomp.innerText.replace(/[\n\r]/gi, '').length;
                    }
                    else {
                        iniTextoEn = tabcomp.innerHTML.indexOf(tabcomp.innerText);
                        iniTextoDis = iniTextoEn + tabcomp.innerText.length;
                    }
                }
                else {
                    iniTextoEn = tabcomp.innerHTML.indexOf(tabcomp.innerText);
                    iniTextoDis = iniTextoEn + tabcomp.innerText.length;
                }

                if (tabcomp.getAttribute("disabled") == "disabled" || tabcomp.getAttribute("disabled") == true) {
                    try {
                        tabcomp.setAttribute("disabled", "");
                        tabcomp.disabled = "";
                        tabcomp.removeAttribute("disabled")
                    }
                    catch (e) {
                    }
                    var sOnClick = tabcomp.getAttribute("onclick").toString();
                    sOnClick = sOnClick.replace("return false;", "");
                    tabcomp.setAttribute("onclick", sOnClick);
                    tabcomp.setAttribute("onclick", sOnClick);
                    tabcomp.setAttribute("onmouseover", "FSNHyperlink_Tab_over(this, 'WhiteSmoke', 'Black')");
                    tabcomp.setAttribute("onmouseout", "FSNHyperlink_Tab_out(this, '#AAAAAA', 'White')");
                    tabcomp.style.color = "white";
                    tabcomp.href = 'Comparativa.aspx';
                    tabcomp.innerHTML = tabcomp.innerHTML.substring(0, iniTextoDis) + '(' + datos[1] + ')' + tabcomp.innerHTML.substr(iniTextoDis);

                    FSNHyperlink_Tab_out(tabcomp, '#AAAAAA', 'White')
                }
                else {
                    iniTextoEn = tabcomp.innerHTML.indexOf('(', iniTextoEn);
                    var finTexto = tabcomp.innerHTML.indexOf(')', iniTextoEn);
                    tabcomp.innerHTML = tabcomp.innerHTML.substring(0, iniTextoEn + 1) + datos[1] + tabcomp.innerHTML.substr(finTexto);
                }
            }
        }
        else {
            alert(msg);
        }
    }

    function SetZIndex(control, args) {
        // Set auto complete extender control's z-index to a high value
        // so it will appear on top of, not under, the ModalPopUp extended panel.
        control._completionListElement.style.zIndex = 99999999;
    }

    function WebDataGrid_CellSelectionChanged(sender, e) {
        var rutaPM = '<%=ConfigurationManager.AppSettings("rutaPM")%>frames.aspx?pagina='
        var rutaFS = '<%=ConfigurationManager.AppSettings("rutaFS") %>PM/Tareas/'

        var cell = e.getSelectedCells().getItem(0)

        var column = sender.get_columns().get_columnFromKey("ID");

        var IdSolicitud = e.getSelectedCells().getItem(0).get_row().get_cellByColumn(column).get_value();

        var column = sender.get_columns().get_columnFromKey("DEN");

        var Solicitud = e.getSelectedCells().getItem(0).get_row().get_cellByColumn(column).get_value();

        var column = sender.get_columns().get_columnFromKey("FAVORITOS");

        var nFavoritos = e.getSelectedCells().getItem(0).get_row().get_cellByColumn(column).get_value();

        var column = sender.get_columns().get_columnFromKey("ID_FAVORITA");

        var Id_Favorita = e.getSelectedCells().getItem(0).get_row().get_cellByColumn(column).get_value();

        var column = sender.get_columns().get_columnFromKey("ADJUN");

        var nAdjun = e.getSelectedCells().getItem(0).get_row().get_cellByColumn(column).get_value();

        //elimino la seleccion sobre la celda pulsada para si vuelve a pulsar salte el evento, si la celda sigue 
        //seleccionada no saltaria el evento al hacer click
        e.getSelectedCells().remove(e.getSelectedCells().getItem(0))

        if (cell.get_column()._key == "IMGADJUN") {
            if (nAdjun > 0) {
                var newWindow = window.open('<%=ConfigurationManager.AppSettings("rutaPM")%>alta/solicitudPMWEB.aspx?Solicitud=' + IdSolicitud, "_blank", "width=700,height=450,status=no,resizable=no,top=100,left=100");
                
            }
        }
        else {
            if (cell.get_column()._key == "IMGFAV") {
                if (nFavoritos > 1) {
                    newWindow = window.open(rutaFS + 'SolicitudesFavoritas.aspx?Solicitud=' + IdSolicitud + '&DenSolicitud=' + Solicitud + '&nFavoritos=' + nFavoritos, '_blank', 'width=750,height=300,status=no,resizable=no,top=100,left=100')
                    
                } else {
                    if (nFavoritos == 1) {
                        window.open(rutaPM + escape('alta/NWAlta.aspx?Solicitud=' + IdSolicitud + '&Favorito=1&IdFavorito=' + Id_Favorita), '_self')
                    }
                    else {
                        window.open(rutaPM + 'alta/NWAlta.aspx?Solicitud=' + IdSolicitud, '_self')
                    }
                }
            }
            else {
                window.open(rutaPM + 'alta/NWAlta.aspx?Solicitud=' + IdSolicitud, '_self')
            }
        }
    }
</script>
<style>
   .autoCompleteList  
         {  
             background-color: Blue ;  
             border: solid 2px red;  
             margin: 0px;  
             z-index: 900009;  
         }  
         .autoCompleteListItem  
         {    
             color: Black ;  
              font-size:x-small;
         }  
         .autoCompleteSelectedListItem  
         {  
              color: Red ;  
              font-size:x-small;
              text-decoration:blink;
         }  
        .popupControl2
        {
	        background-color:#AAD4FF;
	        position:absolute;
	        visibility:hidden;
	        border-style:solid;
	        border-color: Black;
	        border-width: 2px;
	
        }
        tbody.igg_FullstepItem>tr>td
        {
            border-right:none;
	        border-left:none;
	        cursor: pointer;
        }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Ppl" runat="server"> 
    <!-- Panel Info Unidad -->
    <fsn:FSNPanelTooltip ID="FSNPanelDatosUnidad" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx"
        ServiceMethod="Unidad_Datos" EnableViewState="False" />
    <!-- Panel Info Proveedor -->
    <fsn:FSNPanelInfo ID="FSNPanelDatosProve" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx" ServiceMethod="Proveedor_DatosContacto" TipoDetalle="1" />
    
    <input id="btnOculto" type="button" value="button" runat="server" style="display: none" />
    <asp:Panel ID="PanelOculto" runat="server" Style="display:none"></asp:Panel>
    
    <fsep:DetArticuloCatalogo ID="pnlDetalleArticulo" runat="server" />
    
    <!-- Panel Categor�as -->
    <asp:Panel ID="PanelArbol" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display: none;
        min-width: 350px; padding: 2px" onmouseout="FSNCerrarPanelesTemp()" onmouseover="FSNNoCerrarPaneles()">
        <table width="350">
            <tr>
                <td align="left">
                    <asp:Image ID="ImgCatalogoArbol" runat="server" ImageUrl="~/images/categorias_small.gif" />
                    &nbsp; &nbsp;
                    <asp:Label ID="LblCatalogoArbol" runat="server" CssClass="Rotulo">
                    </asp:Label>
                </td>
                <td align="right">
                    <asp:ImageButton runat="server" ID="ImgBtnCerrarPanelArbol" ImageUrl="~/images/Bt_Cerrar.png" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:UpdatePanel ID="pnlCategorias" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:TreeView ID="fsnTvwCategorias" NodeIndent="20" CssClass="Normal" 
                                            ShowExpandCollapse="true" ExpandDepth="0" EnableViewState="True"
                                            runat="server" EnableClientScript="true" ForeColor="Black" PopulateNodesFromClient="true">
                                            <Nodes>
                                                <asp:TreeNode PopulateOnDemand="true" Value="_0_" SelectAction="None" Expanded="false"></asp:TreeNode>
                                            </Nodes>
                                        </asp:TreeView>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>  
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpePanelArbol" runat="server" PopupControlID="PanelArbol"
        TargetControlID="btnOculto" CancelControlID="ImgBtnCerrarPanelArbol" 
        RepositionMode="None" PopupDragHandleControlID="PanelOculto">
    </ajx:ModalPopupExtender>
    
    <!-- Panel Destinos -->
    <asp:Panel ID="PanelDestFiltro" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display: none; min-width: 250px ; padding: 2px" onmouseout="FSNCerrarPanelesTemp()" onmouseover="FSNNoCerrarPaneles()">
        <table width="250">
            <tr>
                <td align="left">
                    <asp:Image ID="ImgDestFil" runat="server" SkinID="CabDestinos" />
                    &nbsp; &nbsp;
                    <asp:Label ID="LblLisDestFil" Text="Lista de Destinos" runat="server" CssClass="Rotulo">
                    </asp:Label>
                </td>
                <td align="right">
                    <asp:ImageButton runat="server" ID="ImgBtnCerrarDestFil" ImageUrl="~/images/Bt_Cerrar.png" />
                </td>
            </tr>
            <tr>
                <td colspan="2" nowrap="nowrap">
                    <asp:DataList ID="lstDestPopup" runat="server">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDestPopup" runat="server"
                                CommandArgument='<%# Eval("Value") %>' CssClass="Normal"
                                Text='<%# Eval("Text") %>' style="text-decoration:none" CommandName='<%# Eval("Text") %>' OnClientClick="FSNCerrarPaneles(); return true"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:DataList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpePanelDestFil" runat="server" PopupControlID="PanelDestFiltro"
        TargetControlID="btnOculto" CancelControlID="ImgBtnCerrarDestFil" 
        PopupDragHandleControlID="PanelOculto" RepositionMode="None">
    </ajx:ModalPopupExtender>
    <%--Panel para la Seleccion de favorito--%>
    <asp:Panel ID="panSelecFavoritos" style="display:none"  runat="server" BorderStyle="Solid"
        BorderWidth="1px" HorizontalAlign="Left" Width="450px" BackColor="White">
        <!-- Panel Info Contacto -->
        <fsn:FSNPanelInfo ID="FSNPanelDatosContacto" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx" ServiceMethod="Proveedor_DatosContacto" Contenedor="panSelecFavoritos" />
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td width="40"><asp:Image ID="imgCabSelFavorito" runat="server" SkinID="CabAddFavorito" /></td>
                <td valign="bottom"><asp:Label ID="LblAnyadirFav" runat="server" CssClass="RotuloGrande" Text="DA�adir a favoritos" /></td>
                <td align="right" valign="top"><asp:ImageButton ID="imgBtnCerrarSelFavorito" runat="server" SkinID="Cerrar" /></td>
            </tr>
        </table>
        <br />
        <asp:UpdatePanel ID="upSelecFavoritos" runat="server"  ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
        <table width="100%" cellspacing="4">
        <tr>
            <td colspan="2" align="left">
                <table id="TFavoritos" runat="server" style="border: 1px solid #000000; padding: 2px" width="100%">
                <tr>
                     <td id="Td1" align="left" valign="middle" runat="server">
                        <asp:Label ID="LblArticulo" runat="server" CssClass="Etiqueta" 
                            Text="DArt�culo:">
                        </asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblNomArticulo" runat="server" CssClass="Normal">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblSelFavProveedor" runat="server" CssClass="Etiqueta" 
                            Text="DProveedor:">
                        </asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="LblNomProve" runat="server" Text="Label" CssClass="Normal">
                        </asp:Label>
                        <fsn:FSNLinkInfo ID="fsnlnkContactoProve" runat="server" PanelInfo="FSNPanelDatosContacto" CssClass="Rotulo" style="text-decoration:none;"></fsn:FSNLinkInfo>
                        <asp:Label ID="COD" runat="server"
                            Text="Label" style="display:none">
                        </asp:Label>
                    </td>
               </tr>
              </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
            </td>
        </tr>
        <tr>
            <td align="left" ID="RowAnyadirFavorito0" runat="server">
                <asp:RadioButtonList ID="RBFavoritos" runat="server" CssClass="Normal">
                    <asp:ListItem Enabled="False">DA�adir linea a pedido Favorito</asp:ListItem>
                    <asp:ListItem Selected="True">DCrear un nuevo Pedido favorito</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td ID="RowAnyadirFavorito" align="left" runat="server" valign="top">
                <asp:DropDownList ID="ListaFavoritos" runat="server" CssClass="Normal">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
            </td>
        </tr>
        <tr>
            <td style="width:50%">
                <fsn:FSNButton ID="BtnAceptarFav" runat="server" Text="DAceptar"  Alineacion="Right"></fsn:FSNButton>
            </td>
            <td style="width:50%">
                <fsn:FSNButton ID="BtnCancelarFav" runat="server" Text="DCancelar" Alineacion="Left"></fsn:FSNButton>
            </td>
        </tr>
        </table>
        </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    
    <asp:Button ID="Button3" runat="server" Text="Button" style="display:none" />
    <ajx:ModalPopupExtender runat="server" ID="mpepanSelecFavoritos" PopupControlID="panSelecFavoritos"
    TargetControlID="Button3" CancelControlID="imgBtnCerrarSelFavorito" BackgroundCssClass="modalBackground">
    </ajx:ModalPopupExtender>
    <%--FIN Panel para la Seleccion de nuevo favorito--%>
        
    <%--Panel de Confirmaci�n de Favorito A�adido--%>
    <asp:Panel ID="panConfirmacionFavoritoAnyadido" runat="server" BackColor="#FAFAFA"
    style="display:none;width:300px;height:150px"  BorderColor="DimGray" BorderStyle="Solid" BorderWidth="1">
        <asp:UpdatePanel ID="upConfirmacionFavoritoAnyadido2" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
        <table width="280px" style="margin-left:10px;margin-right:10px;">
        <tr>
            <td  colspan="2">
                <br />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:Label ID="LblMensajeFavoritoAnyadido" runat="server" CssClass="Etiqueta"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
            </td>
        </tr>
        <tr>
            <td style="width:140px;" align="left">
                <fsn:FSNButton ID="BtnIrAFavorito" runat="server" CssClass="Boton"></fsn:FSNButton>
            </td>
            <td  style="width:140px" align="right">
                <fsn:FSNButton ID="BtnSeguirEnCatalogo" runat="server" CssClass="Boton"></fsn:FSNButton>
            </td>
        </tr>        
        </table>  
        </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <asp:Button ID="Button13" runat="server" Text="Button" style="display:none" />
    <asp:Button ID="Button14" runat="server" Text="Button" style="display:none" />
    <asp:Panel ID="PanelConfFavoAny" runat="server" style="display:none"></asp:Panel>
    <ajx:ModalPopupExtender ID="mpepanConfirmacionFavoritoAnyadido" runat="server" PopupControlID="panConfirmacionFavoritoAnyadido"
    TargetControlID="Button13" RepositionMode="None" PopupDragHandleControlID="PanelConfFavoAny" BackgroundCssClass="modalBackground" DropShadow="false"
    CancelControlID="Button14">
    </ajx:ModalPopupExtender>
    <%--FIN Panel de Confirmaci�n de Favorito A�adido--%>
    
    <%--Panel de busqueda de proveedores --%>
    <asp:Panel ID="PanelBusquedaProves" style="display:none"  runat="server" BorderStyle="Solid"
    BorderWidth="1px" Height="250px" HorizontalAlign="Center" Width="424px" BackColor="White">
        <table width="100%">
            <tr>
                <td align="left">
                    <asp:Image ID="ImgProveBusq" runat="server" ImageUrl="../../images/proveedores_buscar.gif" />
                </td>
                <td colspan="2" align="left">
                    <asp:Label ID="LblTitProvesBusq" runat="server" CssClass="RotuloGrande"></asp:Label>
                </td>
                <td align="right">
                    <asp:ImageButton ID="ImgBtnCerrarBusqProves" ImageAlign="Right" runat="server" SkinID="Cerrar" />
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="UpBusqProvesFiltros" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Table runat="server" ID="TablaFiltrosBusquedaPRove" style="display:block; padding:2px">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" style="width:100px">
                            <asp:Label ID="lblCodProveBusq" runat="server" CssClass="Normal" >
                            </asp:Label>
                        </asp:TableCell>
                        <asp:TableCell Width="20px">
                        </asp:TableCell>
                        <asp:TableCell  style="width:300px" HorizontalAlign="left">
                            <asp:TextBox ID="tbCodProveBusq" runat="server" CssClass="Normal" Width="280px">
                            </asp:TextBox>
                            <ajx:AutoCompleteExtender ID="aceTbCodProveBusq" runat="server" TargetControlID="tbCodProveBusq" CompletionInterval="500" MinimumPrefixLength="3"
                             ServiceMethod="Proveedores_AutocompletarCodigo" CompletionListItemCssClass="autoCompleteListItem" CompletionListHighlightedItemCssClass="autoCompleteSelectedListItem" OnClientShown="SetZIndex" UseContextKey="true" OnClientItemSelected="quitarDescripcion">
                             </ajx:AutoCompleteExtender >
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" style="width:100px">
                            <asp:Label ID="LblDenProveBusq" runat="server" CssClass="Normal">
                            </asp:Label>
                        </asp:TableCell>
                        <asp:TableCell Width="20px">
                        </asp:TableCell>
                        <asp:TableCell  style="width:300px" HorizontalAlign="left">
                            <asp:TextBox ID="tbDenProveBusq" runat="server" CssClass="Normal" Width="280px">
                            </asp:TextBox>
                             <ajx:AutoCompleteExtender ID="aceTbDenProveBusq" runat="server" TargetControlID="tbDenProveBusq" CompletionInterval="500" MinimumPrefixLength="3"
                             ServiceMethod="Proveedores_AutocompletarDenominacion" CompletionListItemCssClass="autoCompleteListItem" CompletionListHighlightedItemCssClass="autoCompleteSelectedListItem" OnClientShown="SetZIndex" UseContextKey="true" OnClientItemSelected="quitarCodigo">
                             </ajx:AutoCompleteExtender>
                        </asp:TableCell>
                    </asp:tableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" style="width:100px">
                            <asp:Label ID="LblCifProveBusq" runat="server" CssClass="Normal">
                            </asp:Label>
                        </asp:TableCell>
                        <asp:TableCell Width="20px">
                        </asp:TableCell>
                        <asp:TableCell  style="width:300px" HorizontalAlign="left">
                            <asp:TextBox ID="tbCIFProveBusq" runat="server" CssClass="Normal" Width="280px">
                            </asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="left" Width="100px">
                            <asp:Label ID="lblCpProveBusq" runat="server" CssClass="Normal">
                            </asp:Label>
                        </asp:TableCell>
                        <asp:TableCell Width="20px">
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="left" Width="300px">
                            <asp:TextBox ID="tbCpProveBusq" runat="server" CssClass="Normal" Width="280px">
                            </asp:TextBox>
                            <ajx:FilteredTextBoxExtender FilterMode="ValidChars" FilterType="Numbers" FilterInterval="50" TargetControlID="tbCpProveBusq"
                            runat="server" ID="ftbeTbCpProveBusq"></ajx:FilteredTextBoxExtender>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="left" Width="100px">
                            <asp:Label ID="LblPaiProveBusq" runat="server" CssClass="Normal">
                            </asp:Label>
                        </asp:TableCell>
                        <asp:TableCell Width="20px">
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="left" Width="300px">
                            <asp:Label ID="LblPaiProveBusqSelec" runat="server" CssClass="Normal" Visible="false"></asp:Label>
                            <asp:DropDownList ID="CmbBoxPaiProveBusq" runat="server" CssClass="Normal" AutoPostBack="true">
                        </asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="left" Width="100px">
                            <asp:Label ID="LblProviProveBusq" runat="server" CssClass="Normal">
                            </asp:Label>
                        </asp:TableCell>
                        <asp:TableCell Width="20px">
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="left" Width="300px">
                            <asp:Label ID="LblProviProveBusqSelec" runat="server" CssClass="Normal" Visible="false"></asp:Label>
                            <asp:DropDownList ID="CmbBoxProviProveBusq" runat="server" CssClass="Normal" AutoPostBack="true">
                            </asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="left" Width="100px">
                            <asp:Label ID="lblPobProveBusq" runat="server" CssClass="Normal">
                            </asp:Label>
                        </asp:TableCell>
                        <asp:TableCell Width="20px">
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="left" Width="300px">
                            <asp:Label ID="lblPobProveBusqSelec" runat="server" CssClass="Normal" Visible="false">
                            </asp:Label>
                            <asp:DropDownList ID="CmboBoxPobProveBusq" runat="server" CssClass="Normal">
                            </asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:table>
                <table>
                    <tr>
                        <td  align="right">
                            <asp:Image ID="ImgOculta" runat="server" Height="10px" Width="190px" ImageUrl="~/images/trans.gif" />
                        </td>
                        <td align="Left">
                            <fsn:FSNButton ID="BtnBuscarProve" runat="server" CssClass="Rotulo" Alineacion="Left">
                            </fsn:FSNButton>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <button id="BtnOcAbrirPanelBusqProve" runat="server" style="display:none" >
    </button>
    <ajx:ModalPopupExtender ID="mpopupProveedores" runat="server" TargetControlID="BtnOcAbrirPanelBusqProve"
    PopupControlID="PanelBusquedaProves" DropShadow="false" CancelControlID="ImgBtnCerrarBusqProves"
    RepositionMode="RepositionOnWindowResizeAndScroll" PopupDragHandleControlID="PanelOculto"></ajx:ModalPopupExtender>
    
    <%--FIN Panel Busqueda de proveedores--%>
    
    <asp:Panel ID="PanelPrincipal" runat="server" Width="100%" HorizontalAlign="Center">
        <asp:Panel runat="server" ID="PanelCatalogo" CssClass="ColorFondoTab" style="padding:4px;" Width="97%" HorizontalAlign="Left">
            <asp:UpdatePanel ID="updFiltros" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="PanelFiltrosAvanzados" Style="margin-top: 4px"
                        Width="100%" HorizontalAlign="Right">
                        <a id="LnkSolicitudes" href="#Solicitudes" class="Enlace10"><asp:Label id="lblLnkSolicitudes" runat="server" Visible="False" Style="padding-right:20px;">DSi lo desea puede emitir una Solicitud de compras</asp:Label></a>
                        <asp:LinkButton ID="LnkBtnFiltAv" runat="server" onfocus="this.blur()" style="text-decoration:none">
                            <asp:Image ID="ImgBtnFiltAv" runat="server" SkinID="ExpandirFondoTrans" />
                            &nbsp; <asp:Label ID="LblFiltAv" runat="server" CssClass="EtiquetaPeque" Text="DFiltros avanzados">
                            </asp:Label>
                        </asp:LinkButton>
                        
                        <ajx:CollapsiblePanelExtender ID="cpe2" runat="server" CollapseControlID="LnkBtnFiltAv"
                            Collapsed="false" SuppressPostBack="true" ExpandControlID="LnkBtnFiltAv"
                            ImageControlID="ImgBtnFiltAv" TargetControlID="PanelFiltrosAv" SkinID="FondoTrans">
                        </ajx:CollapsiblePanelExtender>
                    </asp:Panel>
                    <asp:Panel ID="PanelFiltrosAv" runat="server" Width="100%" HorizontalAlign="Left">
                        <table cellpadding="2">
                            <tr>
                                <td valign="top" nowrap="nowrap">
                                    <fsn:FSNLinkInfo ID="BtnCategoria" runat="server" CssClass="Rotulo"
                                        style="text-decoration:none" PanelInfo="mpePanelArbol">
                                        <asp:Image ID="ImgBtnLupaCat" runat="server" SkinID="LupaPeq" />
                                        <asp:Literal ID="LnkBtnCategoria" runat="server"></asp:Literal>
                                    </fsn:FSNLinkInfo>
                                    &nbsp;<asp:Image ID="imgDespCategoria" runat="server" SkinID="desplegar" />                                    
                                </td>
                                <td align="left" valign="top">
                                    <asp:Panel ID="pnlFiltroCat" runat="server" Width="100%" HorizontalAlign="Left">
                                        <asp:DataList ID="lstCategorias" runat="server" RepeatDirection="Horizontal" EnableViewState="false"
                                            RepeatLayout="Flow">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkFiltroCategoria" runat="server" 
                                                    CommandArgument='<%# Eval("Value") %>' Text='<%# Eval("Text") %>' CssClass="Normal" Font-Underline="False"></asp:LinkButton>
                                            </ItemTemplate>
                                            <SeparatorTemplate>
                                                &nbsp; 
                                            </SeparatorTemplate>
                                        </asp:DataList>
                                    </asp:Panel>
                                    <ajx:CollapsiblePanelExtender ID="cpePnlFiltroCat" runat="server" CollapseControlID="imgDespCategoria"
                                        Collapsed="false" SuppressPostBack="true" ExpandControlID="imgDespCategoria"
                                        ImageControlID="imgDespCategoria" TargetControlID="pnlFiltroCat" SkinID="FondoTrans" >
                                    </ajx:CollapsiblePanelExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" nowrap="nowrap">
                                    <fsn:FSNLinkInfo ID="BtnProves" runat="server" OnClick="BtnProves_Click" CssClass="Rotulo" style="text-decoration:none">
                                        <asp:Image ID="ImgBtnLupaProv" runat="server" SkinID="LupaPeq" />
                                        <asp:Literal ID="LnkBtnProves" runat="server"></asp:Literal>
                                    </fsn:FSNLinkInfo>
                                    &nbsp;<asp:Image ID="imgBtnProves" runat="server" SkinID="desplegar" />
                                </td>
                                <td align="left" valign="top">
                                    <asp:Panel ID="pnlFiltroProv" runat="server" Width="100%" HorizontalAlign="Left">
                                    </asp:Panel>
                                    <ajx:CollapsiblePanelExtender ID="cpePnlFiltroProv" runat="server" CollapseControlID="imgBtnProves"
                                        Collapsed="false" SuppressPostBack="true" ExpandControlID="imgBtnProves"
                                        ImageControlID="imgBtnProves" TargetControlID="pnlFiltroProv" SkinID="FondoTrans" >
                                    </ajx:CollapsiblePanelExtender>
                                </td>
                            </tr>
                            <tr id="FilaFiltroProve" runat="server" visible="false" align="left">    
                            <td colspan="2">
                           <asp:DataList ID="DlFiltrosProve" runat="server" RepeatDirection="Horizontal" 
                                        RepeatLayout="Flow">
                                        <HeaderTemplate>
                                        <asp:Label ID="lblFiltroDl" runat="server">
                                        </asp:Label>
                                        &nbsp;&nbsp;
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFiltro" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                                            <asp:LinkButton ID="lnkFiltro" runat="server" 
                                                CommandArgument='<%# Eval("Codigo") %>' Text='<%# Eval("Denominacion") & " (x)" %>' CssClass="Normal" Font-Underline="false"
                                                CommandName='<%# Eval("Denominacion") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                        <SeparatorTemplate>
                                            &nbsp; | &nbsp; 
                                        </SeparatorTemplate>
                                        <FooterTemplate>
                                            &nbsp; &nbsp; |
                                            <asp:LinkButton ID="lnkQuitarTodo" runat="server" CommandName="QuitarTodo" 
                                                CssClass="Normal">DQuitar todo</asp:LinkButton>
                                        </FooterTemplate>
                            </asp:DataList>
                            </td>
                        </tr>
                            <tr>
                                <td valign="top" nowrap="nowrap">
                                    <fsn:FSNLinkInfo ID="BtnDests" runat="server" CssClass="Rotulo"
                                        style="text-decoration:none;" PanelInfo="mpePanelDestFil">
                                    <asp:Image ID="ImgBtnLupaDests" runat="server" SkinID="LupaPeq" />
                                    <asp:Literal ID="LnkBtnDests" runat="server"></asp:Literal>
                                    </fsn:FSNLinkInfo>
                                    &nbsp;<asp:Image ID="ImgBtnDests" runat="server" SkinID="desplegar" />
                                </td>
                                <td align="left" valign="top">
                                    <asp:Panel ID="pnlFiltroDests" runat="server" Width="100%" HorizontalAlign="Left">
                                        <asp:DataList ID="lstDestinos" runat="server" RepeatDirection="Horizontal" EnableViewState="false"
                                            RepeatLayout="Flow">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkFiltroDestino" runat="server" 
                                                    CommandArgument='<%# Eval("Value") %>' Text='<%# Eval("Text") %>' CssClass="Normal" Font-Underline="False"></asp:LinkButton>
                                            </ItemTemplate>
                                            <SeparatorTemplate>
                                                &nbsp; 
                                            </SeparatorTemplate>
                                        </asp:DataList>
                                    </asp:Panel>
                                    <ajx:CollapsiblePanelExtender ID="cpePnlFiltroDests" runat="server" CollapseControlID="ImgBtnDests"
                                        Collapsed="false" SuppressPostBack="true" ExpandControlID="ImgBtnDests"
                                        ImageControlID="ImgBtnDests" TargetControlID="pnlFiltroDests" SkinID="FondoTrans" >
                                    </ajx:CollapsiblePanelExtender>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <hr align="center" width="100%" />
                    <table id="tFiltro" runat="server" border="0">
                        <tr>
                            <td valign="top">
                                <asp:Label ID="LblFiltro" runat="server" CssClass="Rotulo" Text="Filtro">
                                </asp:Label>
                            </td>
                            <td valign="top">
                                    <asp:DataList ID="lstFiltros" runat="server" RepeatDirection="Horizontal" 
                                        RepeatLayout="Flow">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFiltro" runat="server" Text='<%# Eval(HttpContext.Current.Server.HtmlEncode("Nombre")) %>'></asp:Label>
                                            <asp:LinkButton ID="lnkFiltro" runat="server" 
                                                CommandArgument='<%# Eval(HttpContext.Current.Server.HtmlEncode("Codigo")) %>' Text='(x)' CssClass="Normal" Font-Underline="false"
                                                CommandName='<%# Eval("Tipo") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                        <SeparatorTemplate>
                                            &nbsp; 
                                        </SeparatorTemplate>
                                        <FooterTemplate>
                                            &nbsp; &nbsp; |
                                            <asp:LinkButton ID="lnkQuitarTodo" runat="server" CommandName="QuitarTodo" 
                                                CssClass="Normal">DQuitar todo</asp:LinkButton>
                                        </FooterTemplate>
                                    </asp:DataList>
                            </td>
                        </tr>
                    </table>
                    <hr align="center" width="100%" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="UpdatePanelGrid" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
                <ContentTemplate>
                    <asp:GridView ID="GridArticulos" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False"
                        GridLines="None" HorizontalAlign="Center" ShowFooter="False" Width="100%" 
                        CellPadding="5" ShowHeader="False" EnableViewState="false">
                        <PagerSettings Position="TopAndBottom" />
                        <RowStyle BackColor="#F7F7F7" />
                        <Columns>
                            <asp:TemplateField FooterStyle-Wrap="False" HeaderStyle-Wrap="False"
                                ShowHeader="False" ItemStyle-VerticalAlign="Top" ItemStyle-Width="50px">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImgFavoritos" runat="server" 
                                        SkinId="AddFavoritos" CommandName="Favorito" 
                                        OnClick='<%# "guardarFAV(""" & DataBinder.Eval(Container, "DataItem.Id") & "###" & DataBinder.Eval(Container, "DataItem.Mon") & """, this.id);" %>'
                                        CommandArgument='<%#DataBinder.Eval(Container, "DataItem.Id") & "###" & DataBinder.Eval(Container, "DataItem.Mon") %>' />
                                    <asp:ImageButton ID="ImgComparativa" runat="server" SkinId="Comparar" OnClientClick='<%#"FSNAnyadirAComparativa(" & DataBinder.Eval(Container, "DataItem.Id") & ",this); return false" %>' />
                                </ItemTemplate>
                                <FooterStyle Wrap="False" />
                                <HeaderStyle Wrap="False" />
                                <ItemStyle VerticalAlign="Top" Width="45px" />
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                <table><tr><td style="border-style:none; border-width:0px; width:139px; height:94px; padding:2px; text-align: center; vertical-align: middle;">
                                    <asp:ImageButton ID="ImgBtnArt" runat="server" CommandName="AbrirDetalleArt" OnClick='<%# "guardarINFO(""" & DataBinder.Eval(Container, "DataItem.Id") & """);" %>'
                                        ImageUrl='<%# "EPThumbnail.ashx?prove=" & DataBinder.Eval(Container, "DataItem.PROVE") & "&art=" & DataBinder.Eval(Container, "DataItem.COD_ITEM") & "&Tipo=Thumbnail" &  "&PonerImagen=True" %>' />
                                </td></tr></table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td style="padding-top: 8px;">
                                                <asp:LinkButton ID="LnkCodArtOc" runat="server" CssClass="RotuloGrande" Text='<%# DataBinder.Eval(Container, "DataItem.COD_ITEM") & IIf(IsDBNull(DataBinder.Eval(Container, "DataItem.COD_ITEM")),""," - ") %>'
                                                    Visible="false" CommandName="AbrirDetalleArt" OnClick='<%# "guardarINFO(""" & DataBinder.Eval(Container, "DataItem.Id") & """);" %>'></asp:LinkButton>
                                                <asp:LinkButton ID="LnkNomArt" runat="server" CssClass="RotuloGrande" Text='<%# DataBinder.Eval(Container, "DataItem.ART_DEN") %>'
                                                    Visible="false" CommandName="AbrirDetalleArt" OnClick='<%# "guardarINFO(""" & DataBinder.Eval(Container, "DataItem.Id") & """);" %>'></asp:LinkButton>
                                                <asp:TextBox ID="TxtNomArt" runat="server" CssClass="RotuloGrande" Text='<%# DataBinder.Eval(Container, "DataItem.ART_DEN") %>'
                                                    Visible="false" Width="400px" CommandName="AbrirDetalleArt" OnClick='<%# "guardarINFO(""" & DataBinder.Eval(Container, "DataItem.Id") & """);" %>'></asp:TextBox>
                                                <asp:Label ID="lblId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Id") %>'
                                                    Visible="False">
                                                </asp:Label>
                                                <asp:Label ID="COD" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Prove") %>'
                                                    Visible="False">
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 8px;">
                                                <asp:Label ID="LblDescArt" runat="server" CssClass="Normal" Text='<%# DataBinder.Eval(Container, "DataItem.ESP") %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 8px;">
                                                <asp:Label ID="LblCaracteristicasArt" runat="server" CssClass="Normal" Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 8px;">
                                                <fsn:FSNLinkInfo ID="FSNLinkProve" runat="server" CssClass="LinkButtonInfo"/>&nbsp;
                                                <asp:Label ID="Label15" runat="server" CssClass="LinkButtonInfo" Text=" | ">
                                                </asp:Label>
                                                &nbsp;<asp:DataList ID="lstCategoriasArt" runat="server" RepeatDirection="Horizontal"
                                                    RepeatLayout="Flow" CssClass="LinkButtonInfo" OnItemCommand="lstCategoriasCat_ItemCommand">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LnkCategoriaArt" runat="server" CommandArgument='<%# Eval("Value") %>'
                                                            Text='<%# Eval("Text") %>' CssClass="LinkButtonInfo"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <SeparatorTemplate>
                                                        :
                                                    </SeparatorTemplate>
                                                </asp:DataList>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Bottom" ItemStyle-Wrap="False">
                                <ItemTemplate>
                                    <table style="min-width:260px;" width="100%" align="right">
                                        <tr>
                                            <td align="right" width="110px">
                                                <asp:Label ID="LblPrecio" runat="server" CssClass="Normal">
                                                </asp:Label>
                                            </td>
                                            <td align="right" colspan="2" width="190px">
                                                <asp:Label ID="LblUnd" runat="server" CssClass="Normal">
                                                </asp:Label>
                                                <asp:Label ID="LblCantMinPedOc" runat="server" CssClass="Normal" 
                                                    Visible="False">
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" width="110px">
                                                <asp:Label ID="LblPrecArt" runat="server" CssClass="EtiquetaGrande">
                                                </asp:Label>
                                                <fsn:FSNUpDownArrows ID="WNumEdPrecioArtArrows" runat="server" style="float:right;">
                                                </fsn:FSNUpDownArrows><fsn:FSNTextBox ID="WNumEdPrecioArt" runat="server" Width="60px" style="vertical-align:middle;float:right;" ></fsn:FSNTextBox>
                                                <asp:Label runat="server" ID="WNumEdPrecioArt_DblValue" Text="0" style="display:none;"></asp:Label>
                                            </td><td align="right" width="70px">   
                                                <asp:Label ID="LblMonUni" runat="server" CssClass="Normal">
                                                </asp:Label><fsn:FSNLinkTooltip ID="lnkUnidad" runat="server" PanelTooltip="FSNPanelDatosUnidad"
                                                    Text='<%# DataBinder.Eval(Container, "DataItem.UP_DEF") %>' ContextKey='<%# DataBinder.Eval(Container, "DataItem.UP_DEF") & "_@_" & DataBinder.Eval(Container, "DataItem.ID") %>' 
                                                    CssClass="LinkButtonInfo"></fsn:FSNLinkTooltip>
                                                <asp:Label id="spanX" runat="server" class="Normal"> x </asp:Label>
                                            </td><td align="right" width="135px">
                                                <fsn:FSNUpDownArrows ID="WNumEdCantidadArrows" runat="server" style="float:right;"></fsn:FSNUpDownArrows>
                                                <fsn:FSNTextBox ID="WNumEdCantidad" runat="server" Width="80px" style="vertical-align:middle;float:right; margin:0px"></fsn:FSNTextBox>
                                                <div style="display:none"><asp:Label runat="server" ID="WNumEdCantidad_DblValue" Text="0" ></asp:Label></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" valign="bottom" style="height:35px;" colspan="3">
                                                <fsn:FSNHyperlink ID="LnkAnyadirACesta" runat="server" Alineacion="Right"></fsn:FSNHyperlink>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle Wrap="False" />
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Bottom" Wrap="False" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerTemplate>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td align="left">
                                        <table cellpadding="4" border="0">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LblOrd" runat="server" CssClass="Etiqueta" Text="Ordenar por:" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ListaOrdenacionArt" runat="server" CssClass="Etiqueta"
                                                        OnSelectedIndexChanged="ListaOrdenacionArt_SelectedIndexChanged"
                                                        OnTextChanged="ListaOrdenacionArt_SelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="LnkBtnOrdenacion" runat="server" style="text-align:left;" OnClick="LnkBtnOrdenacion_Click">
                                                        <asp:Image id="imLnkBtnOrdenacion" runat=server style="border:0px"/>
                                                    </asp:LinkButton>
                                                </td>
                                                <td>&nbsp;</td><td>&nbsp;</td>
                                                <td>
                                                    <asp:LinkButton ID="LnkBtnFirst" runat="server" style="text-align:left;" CommandArgument="first" CommandName="page">
                                                        <asp:Image id="imLnkBtnFirst" runat=server style="border:0px"/>
                                                    </asp:LinkButton>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="LnkBtnPrev" runat="server" style="text-align:left;" CommandArgument="prev" CommandName="page">
                                                        <asp:Image id="imLnkBtnPrev" runat=server style="border:0px"/>
                                                    </asp:LinkButton>
                                                </td>
                                                <td>
                                                    <asp:Label ID="LblPagina" runat="server" Text="P�gina" Width="42px" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="CmbBoxNumPags" runat="server" AutoPostBack="True"
                                                        OnSelectedIndexChanged="CmbBoxNumPags_SelectedIndexChanged" EnableViewState="false"
                                                        Width="48px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="LblDe" runat="server" CssClass="Etiqueta" Text="De">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="LblToArt" runat="server" CssClass="Etiqueta" Text="X">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="LnkBtnNext" runat="server" style="text-align:left;" CommandArgument="next" CommandName="page">
                                                        <asp:Image id="imLnkBtnNext" runat=server style="border:0px"/>
                                                    </asp:LinkButton>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="LnkBtnLast" runat="server" style="text-align:left;" CommandArgument="last" CommandName="page">
                                                        <asp:Image id="imLnkBtnLast" runat=server style="border:0px"/>
                                                    </asp:LinkButton>
                                                 </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="right">
                                        <asp:Panel ID="panFiltroArticulos" runat="server" DefaultButton="BtnFiltrarPaginador">
                                            <table cellpadding="4">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxBusqArt" runat="server" CssClass="Etiqueta" Width="218px" EnableViewState="False">
                                                        </asp:TextBox>
                                                        <ajx:TextBoxWatermarkExtender ID="TextBoxBusqArt_TextBoxWatermarkExtender" runat="server"
                                                            TargetControlID="TextBoxBusqArt" WatermarkText="Nombre, Descripci�n,...">
                                                        </ajx:TextBoxWatermarkExtender>
                                                        <ajx:AutoCompleteExtender ID="TextBoxBusqArt_AutoCompleteExtender" runat="server"
                                                            CompletionInterval="500" MinimumPrefixLength="3" ServiceMethod="AutoCompletar"
                                                            TargetControlID="TextBoxBusqArt" UseContextKey="True">
                                                        </ajx:AutoCompleteExtender>
                                                    </td>
                                                    <td>
                                                        <fsn:FSNButton ID="BtnFiltrarPaginador" runat="server" CommandName="BuscarArticulos" Alineacion="Left">
                                                        </fsn:FSNButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                            <hr width="100%" />
                        </PagerTemplate>
                        <AlternatingRowStyle BackColor="#EFEFEF" />
                        <EmptyDataTemplate>
                        </EmptyDataTemplate>
                    </asp:GridView>                    
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="fsnTvwCategorias" EventName="SelectedNodeChanged" />
                    <asp:AsyncPostBackTrigger ControlID="lstDestPopup" EventName="ItemCommand" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>        
    </asp:Panel>

    <!-- BEGIN Solicitud de compra -->
    <asp:UpdatePanel runat="server" ID="updLblSinArticulos" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="fsnTvwCategorias" EventName="SelectedNodeChanged" />
            <asp:AsyncPostBackTrigger ControlID="lstDestPopup" EventName="ItemCommand" />
        </Triggers>
        <ContentTemplate>
            <a name="Solicitudes"></a>
            <p>
                <asp:Label id="lblSolicitudes" runat="server" CssClass="Rotulo" Visible="False">DSolicitudes de compra</asp:Label>
                <asp:Label id="lblEmitir" runat="server" CssClass="Normal" ForeColor="Black" Visible="False">DSi lo desea puede emitir una solicitud de comnpra, en vez de realizar un pedido:</asp:Label>
            </p>
	        <table id="tblSolicitudes" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
		        <tr>
			        <td class="Cabecera Texto12"><asp:label id="lblCabeceraAltaSolicitudes" runat="server">Tipos de solicitudes disponibles</asp:label></td>
		        </tr>
		        <tr>
			        <td>
				        <ig:WebDataGrid id="uwgSolicitudes_WDG"  runat="server" Width="100%" AutoGenerateColumns="false" ShowHeader="false">
					        <Behaviors>
					        <ig:Selection RowSelectType="Single" CellClickAction="Cell"  CellSelectType="Single" Enabled="true"  >
					            <SelectionClientEvents CellSelectionChanged="WebDataGrid_CellSelectionChanged" />
					        </ig:Selection>
					        </Behaviors>
					        <Columns>
					            <ig:TemplateDataField Key="IMGFAV" Width="4%"></ig:TemplateDataField>
					            <ig:BoundDataField DataFieldName="ID" Key="ID" Hidden="true"></ig:BoundDataField>
					            <ig:BoundDataField DataFieldName="TIPO" Key="TIPO" Hidden="true"></ig:BoundDataField>
					            <ig:BoundDataField DataFieldName="COD" Key="COD" Hidden="true"></ig:BoundDataField>
					            <ig:BoundDataField DataFieldName="DEN" Key="DEN" Width="91%"></ig:BoundDataField>
					            <ig:BoundDataField DataFieldName="ADJUN" Key="ADJUN" Hidden="true" ></ig:BoundDataField>
					            <ig:BoundDataField DataFieldName="FAVORITOS" Key="FAVORITOS" Hidden="true"></ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="ID_FAVORITA" Key="ID_FAVORITA" Hidden="true"></ig:BoundDataField>
					            <ig:TemplateDataField Key="IMGADJUN" Width="5%"></ig:TemplateDataField>
					        </Columns>
				        </ig:WebDataGrid>
			        </td>
		        </tr>
		        <tr>
			        <td>&nbsp;
			        </td>
		        </tr>
	        </table>
	        <p><asp:label id="lblFooter" runat="server" CssClass="Rotulo10">Si la solicitud que usted necesita no se encuentra disponible en la lista consulte con el administrador del sistema.</asp:label></p>
            <p><asp:label id="lblAplicarFiltro" runat="server" CssClass="Normal">DPara obtener art�culos efect�e una b�squeda por Art�culo o aplique al menos un Filtro por Categor�a, Proveedor y/o Destino.</asp:label></p>  
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- END   Solicitud de compra -->    

    <input type="hidden" runat="server" id="IDPostBack" />
    <script type="text/javascript" language="javascript">
        function guardarINFO(INFO) {
            document.getElementById('<%=IDPostBack.ClientID %>').value = INFO;
        }
        function guardarFAV(INFO, idImagenFav) {
            var IDcantidad = idImagenFav.replace('ImgFavoritos', 'WNumEdCantidad_DblValue');
            var IDNomArt = idImagenFav.replace('ImgFavoritos', 'TxtNomArt');
            var cantidad = $get(IDcantidad).textContent;
            var oNomArt = $get(IDNomArt)
            var nomArt = '';
            if(oNomArt)
                nomArt = oNomArt.value;
            else {
                var IDNomArt = idImagenFav.replace('ImgFavoritos', 'LnkNomArt');
                oNomArt = $get(IDNomArt)
                nomArt = oNomArt.innerHTML;
            }
            INFO = INFO + '###' + cantidad + '###' + nomArt;
            guardarINFO(INFO);
        }
        function JSText(Entrada) {
            Entrada = replaceAll(Entrada, "'", "\'");
            Entrada = replaceAll(Entrada, '"', "\'");

            return Entrada
        }
    </script>
    
    </asp:Content>
