﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ParametrosEP.aspx.vb" Inherits="Fullstep.FSNWeb.ParametrosEP" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    <style>
    .RotuloGrande
{
    font-size: 16px;
    font-weight: bold;
}
</style>
<script type="text/javascript" language="javascript">
    /*<%-- 
    ''' Revisado por: blp. Fecha: 24/02/2012
    ''' <summary>
    ''' Evento que salta al pinchar en un check de seguimiento de pedidos.
    ''' Si deseleccionamos ambos, nos marcar el ultimo que hemos seleccionado, no dejando deseleccionarlos todos
    ''' </summary>
    ''' <param name="indice">Nos indica que check hemos seleccionado</param>       
    ''' <param name="idCheckBoxList">CheckboxList que hay que modificar</param>
    --%>*/
    function ValidacionChecks(indice, idCheckBoxList) {
        var chkBoxList = document.getElementById(idCheckBoxList);
        if (chkBoxList) {
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            var cont = 0;
            for (var i = 0; i < chkBoxCount.length; i++) {
                if (chkBoxCount[i].checked == true)
                    cont++;

                if (i == indice)
                    objCheck = chkBoxCount[i]
            }
            if (cont == 0)
                objCheck.checked = true;
        }
    }
    /*  Descripcion:Posicionar y ridemesionar con las medidas la pagina
        Parametros entrada:
            anchura:
            altura:
        Llamada desde:Form_load (vb)    
        Tiempo ejecucion:0seg.    */
    function resize(anchura, altura) {
        window.moveTo(200, 150);
        if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
            var ieversion = new Number(RegExp.$1)            
            if (ieversion == 7) {
                //Darle mayor altura ya que el explorer no quita la barra de menus...
                altura = parseFloat(altura) + 130
            }            
        }
        window.resizeTo(anchura, altura)
    }


</script>
</head>
<body style="background-color: white">
    <form id="form1" runat="server" defaultbutton="BtnAceptar">
    <div>
    <table border="0" cellpadding="4" runat="server" id="TParametros" width="100%">
    <tr width="100%"><td valign="top" colspan="2" ><asp:Image ID="imgParametros" runat="server" 
            SkinID="Parametros" /> &nbsp; <asp:label id="lblTitulo" runat="server" CssClass="RotuloGrande"></asp:label></td>
	</tr>
	<tr width="100%">
	<td>
	<table width="100%">
	<tr width="100%">
	<td align="left">
	<asp:label id="lblDestDef" runat="server" CssClass="Listado" Text="DDestino por defecto para pedidos:">
	</asp:label>
	</td>
	<td align="right">
	<asp:DropDownList runat="server" ID="ddlDestDefPed" style="text-align:right">
					</asp:DropDownList>
	</td>
	</tr>
	</table>
	</td>		
	</tr>
    <tr width="100%">
    <td>
    <table width="100%">
    <tr width="100%">
    <td align="left">
    <asp:label id="lblMostImgArt" runat="server" CssClass="Listado" Text="DMostrar imagen de artículo en resultado de la busqueda:"></asp:label>
    </td>
    <td align="right">
    <asp:DropDownList runat="server" ID="ddlMostImgArt" Width="50px" style="text-align:right"></asp:DropDownList>
    </td>
    </tr>
    </table>
    </td>				
	</tr>
	<tr width="100%">
	<td>
	<table width="100%">
    <tr width="100%">
    <td align="left">
    <asp:label id="lblMostAtrib" runat="server" CssClass="Listado" Text="DMostrar imagen de artículo en resultado de la busqueda:"></asp:label>
    </td>
    <td align="right">
    <asp:DropDownList runat="server" ID="ddlMostAtrib" Width="50px" style="text-align:right"></asp:DropDownList>
    </td>
    </tr>
    </table>
    </td>				
	</tr>
	<tr width="100%">
	<td>
    <table width="100%">
    <tr width="100%">
    <td align="left">
    <asp:label id="lblMostCantMin" runat="server" CssClass="Listado" Text="DMostrar cantidad mínima de pedido en resultado de busqueda:"></asp:label>
    </td>
    <td align="right">
    <asp:DropDownList runat="server" ID="ddlMostCantMin" Width="50px" style="text-align:right"></asp:DropDownList>
    </td>
    </tr>
    </table>			
    </td>
	</tr>
	<tr width="100%">
	<td>
	<table width="100%">
    <tr width="100%">
    <td align="left">
    <asp:label id="lblOcCodProve" runat="server" CssClass="Listado" Text="DOcultar código de proveedor:"></asp:label>
    </td>
    <td align="right">
    <asp:DropDownList runat="server" ID="ddlOcCodProve" Width="50px" style="text-align:right"></asp:DropDownList>
    </td>
    </tr>
    </table>
    </td>				
	</tr>
	<tr width="100%">
	<td>
	<table width="100%">
    <tr width="100%">
    <td align="left">
    <asp:label id="lblOcCodArt" runat="server" CssClass="Listado" Text="DOcultar código de artículo:"></asp:label>
    </td>
    <td align="right">
    <asp:DropDownList runat="server" ID="ddlOcCodArt" Width="50px" style="text-align:right"></asp:DropDownList>
    </td>
    </tr>
    </table>
    </td>				
	</tr>
<%--Incidencia 15916: Comentamos el código. Es posible que sea reutilizado en el futuro--%>
<%--	
	<tr runat="server" id="FBarraControl" width="100%">
	<td>
    <table width="100%">
    <tr width="100%">
    <td align="left">
    <asp:label id="lblMostBarraControl" runat="server" CssClass="Listado" Text="DMostrar barra de control de gasto en la emisión:"></asp:label>
    </td>
    <td align="right">
    <asp:DropDownList runat="server" ID="ddlMostBarraControl" Width="50px" style="text-align:right"></asp:DropDownList>
    </td>
    </tr>
    </table>
    </td>			
	</tr>
--%>
	<tr runat="server" id="tblSeguimiento" style="width:100%" visible="false">
	    <td>    	
            <fieldset id="fldSetSeguimiento" runat="server">
                <legend style="vertical-align:top">
                    <asp:Label ID="lblLegendSeguimiento" runat="server" Text="dSeguimiento de pedidos">
                    </asp:Label>
                </legend>
                
                <asp:CheckBoxList ID="chkSeguimientoPedidos" runat="server" RepeatDirection="Vertical">
                    <asp:ListItem Value="1" Text="Ver sólo mis pedidos" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="2" Text="Ver todos los pedidos de mis centros de coste" Selected="False"></asp:ListItem>
                    
                </asp:CheckBoxList>
                    
            
            </fieldset>
        
        </td>			
	</tr>
	
	<tr>
	<td>
        <fieldset>
            <legend style="vertical-align:top">
                <asp:Label ID="lblLegendRecepcion" runat="server" Text="dRecepciÃ³n de pedidos">
                </asp:Label>
            </legend>
                
            <asp:label id="lblRecepcionesPendientes" runat="server" CssClass="Listado" Text="DCargar todas las cantidades pendientes de recepcionar:"></asp:label>


            <asp:DropDownList runat="server" ID="ddlRecepcionesPendientes" Width="50px" style="text-align:right"></asp:DropDownList>
            <br />
            <asp:CheckBoxList ID="chkRecepcionPedidos" runat="server" RepeatDirection="Vertical" Visible="false">
                <asp:ListItem Value="1" Text="DRecepcionar mis pedidos" Selected="True"></asp:ListItem>
                <asp:ListItem Value="2" Text="DRecepcionar pedidos de otros usuarios" Selected="False"></asp:ListItem>
            </asp:CheckBoxList>
        </fieldset>
    </td>			
	</tr>
	
	
	
	
    <tr style="width:100%">
    <td align="center" valign="bottom">
    <table><tr><td><fsn:FSNButton ID="btnAceptar" runat="server" Text="DAceptar" Alineacion="Left"/></td></tr></table>
    </td>
    </tr>
    </table>
    </div>
    </form>
</body>
</html>
