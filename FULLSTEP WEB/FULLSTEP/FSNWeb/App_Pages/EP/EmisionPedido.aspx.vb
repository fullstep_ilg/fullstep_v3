﻿Imports Fullstep.FSNLibrary
Imports Fullstep.FSNServer
Imports Infragistics.Web.UI
Imports System.Web.Script.Serialization
Imports System.IO

Partial Public Class EmisionPedido
	Inherits FSEPPage

	Private _Empresas As CEmpresas
	Private _Persona As CPersona
	Private _Destinos As CDestinos
	Private _PaginadorTop As PaginadorWebHierarchical
	Private _Cesta As COrdenes
	Dim idLinea As String
	Dim bTodosImporte As Boolean
	Private _PartidaPresupuestariaDEN As String = String.Empty
	Private ModoEdicion As Boolean
	Dim IdDesdeFav As Integer

#Region "TipoCampoPedido"
	<Serializable()>
	Public Enum TipoCampoPedido As Integer
		Generico = 1
		CampoPersonalizado = 2
		CentroCoste = 3
		PartidaPresupuestaria = 4
	End Enum
#End Region
#Region "Read only properties"
	Private ReadOnly Property Persona() As CPersona
		Get
			If _Persona Is Nothing Then
				_Persona = FSNServer.Get_Object(GetType(FSNServer.CPersona))
				_Persona.Cod = Me.Usuario.CodPersona
				_Persona.CargarPersona()
			End If
			Return _Persona
		End Get
	End Property
	Private Shared ReadOnly Property LineasPedido(ByVal idPedido As Integer, ByVal iDesdeFavorito As Integer, ByVal idEmpresa As Integer) As CLineasPedido
		Get
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			If HttpContext.Current.Cache("CLineasPedido_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString() & "_" & idPedido.ToString()) Is Nothing Then
				GenerarDsetPedidos(FSNUser.CodPersona, FSNUser.Idioma, idPedido, iDesdeFavorito, idEmpresa)
			End If
			Return CType(HttpContext.Current.Cache("CLineasPedido_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString() & "_" & idPedido.ToString()), CLineasPedido)
		End Get
	End Property
	''' <summary>
	''' Propiedad que actualmente sólo devuelve la primera partida presupuestaria existente, independientemente de cuántas haya creadas,
	''' dado que EmisionPedidobis.aspx.vb sól oestá preparada para mostrar una partida presupuestaria.
	''' </summary>
	Private _mipargenSMs As FSNServer.PargensSMs
	Private ReadOnly Property _PargenSM() As FSNServer.PargenSM
		Get
			If _mipargenSMs Is Nothing Then
				If Acceso.gbAccesoFSSM Then
					_mipargenSMs = Me.PargenSM
					If _mipargenSMs.Count > 0 Then
						Return _mipargenSMs(0)
					Else
						Return Nothing
					End If
				Else
					Return Nothing
				End If
			Else
				Return _mipargenSMs(0)
			End If
		End Get
	End Property
	Private ReadOnly Property Empresas() As CEmpresas
		Get
			If _Empresas Is Nothing Then
				If HttpContext.Current.Cache("_Empresas" & FSNUser.CodPersona & IIf(Request.QueryString("abierto") Is Nothing, "", DBNullToStr(NothingToDBNull(Request.QueryString("abierto"))))) Is Nothing Then
					If Request.QueryString("abierto") Is Nothing Then
						Persona.CargarEmpresas(Me.PargenSM, FSNUser.PedidosOtrasEmp)
						_Empresas = Me.Persona.Empresas
					Else
						'Si es pedido contra pedido abierto cargamos unicamente la empresa del pedido abierto
						Dim cEmpresas As FSNServer.CEmpresas = FSNServer.Get_Object(GetType(FSNServer.CEmpresas))
						cEmpresas.CargarEmpresaPedidoAbierto(CType(Request.QueryString("abierto"), Integer))
						_Empresas = cEmpresas
					End If

					'Asociamos las direcciones de envio a las empresas del usuario
					If Me.Acceso.gbDirEnvFacObl AndAlso _Empresas IsNot Nothing Then
						Dim empresa As CEmpresa
						For value As Integer = 0 To _Empresas.Count - 1
							empresa = _Empresas(value)
							If empresa.DireccionesEnvioFactura Is Nothing Then
								Dim listaDirecciones As List(Of CEnvFacturaDireccion) = Me.DireccionesEnvioFacturas.FindAll(Function(direc As CEnvFacturaDireccion) direc.IdEmpresa = empresa.ID)
								Dim direcciones As CEnvFacturaDirecciones = Me.FSNServer.Get_Object(GetType(CEnvFacturaDirecciones))
								If listaDirecciones IsNot Nothing Then
									For Each dir As CEnvFacturaDireccion In listaDirecciones
										direcciones.Add(dir)
									Next
									empresa.DireccionesEnvioFactura = direcciones
								End If
							End If
						Next
					End If
					Me.InsertarEnCache("_Empresas" & FSNUser.CodPersona & IIf(Request.QueryString("abierto") Is Nothing, "", DBNullToStr(NothingToDBNull(Request.QueryString("abierto")))), _Empresas)
				Else
					_Empresas = HttpContext.Current.Cache("_Empresas" & FSNUser.CodPersona & IIf(Request.QueryString("abierto") Is Nothing, "", DBNullToStr(NothingToDBNull(Request.QueryString("abierto")))))
				End If
			End If

			Return _Empresas
		End Get
	End Property
	''' <summary>
	''' Direcciones de envío de facturas de todas las empresas.
	''' Valor que se guarda en caché para su uso por todos los usuarios que lo requieran
	''' </summary>
	Private ReadOnly Property DireccionesEnvioFacturas() As CEnvFacturaDirecciones
		Get
			Dim CDireccionesEnvioFacturas As CEnvFacturaDirecciones = Me.FSNServer.Get_Object(GetType(CEnvFacturaDirecciones))
			If HttpContext.Current.Cache("CDireccionesEnvioFacturas") Is Nothing Then
				CDireccionesEnvioFacturas.CargarDireccionesDeEnvioDeFactura()
				Me.InsertarEnCache("CDireccionesEnvioFacturas", CDireccionesEnvioFacturas)
			Else
				CDireccionesEnvioFacturas = HttpContext.Current.Cache("CDireccionesEnvioFacturas")
			End If
			Return CDireccionesEnvioFacturas
		End Get
	End Property
	''' <summary>
	''' Propiedad que recupera el conjunto de las unidades de Pedido para su posterior uso en la página
	''' </summary>
	Private ReadOnly Property TodasLasUnidades() As FSNServer.CUnidadesPedido
		Get
			Dim oUnidadesPedido As FSNServer.CUnidadesPedido = Me.FSNServer.Get_Object(GetType(FSNServer.CUnidadesPedido))
			If HttpContext.Current.Cache("TodasLasUnidades_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
				oUnidadesPedido.CargarTodasLasUnidades(Me.Usuario.Idioma)
				Me.InsertarEnCache("TodasLasUnidades" & Me.Usuario.Idioma.ToString(), oUnidadesPedido)
			Else
				oUnidadesPedido = HttpContext.Current.Cache("TodasLasUnidades" & Me.Usuario.Idioma.ToString())
			End If
			Return oUnidadesPedido
		End Get
	End Property
	''' <summary>
	''' Propiedad que, si hemos llegado a la página con un favorito preseleccionado e informado por querystring con la variable favid
	''' (actualmente el único caso es desde el webpart de pedidos favoritos)
	''' nos devuelve su id, si no devuelve cero
	''' </summary>
	Private ReadOnly Property FavId() As Integer
		Get
			If (Request.QueryString("favid") <> "") Then
				Return CType(Request.QueryString("favid"), Integer)
			Else
				Return 0
			End If
		End Get
	End Property
	Private ReadOnly Property DesdeFavorito() As Boolean
		Get
			Return Request.QueryString("DesdeFavorito") = "True"
		End Get
	End Property
	''' <summary>
	''' Dataset que contiene el listado de pedidos a mostrar en pantalla
	''' </summary>
	Private ReadOnly Property DsetPedidos(ByVal idPedido As Integer, Optional ByVal bModificarFavorito As Boolean = False) As DataSet
		Get
			Dim ddl As DropDownList = CType(CmbBoxEmpresa, DropDownList)

			Dim dsLineasPedido As New DataSet
			dsLineasPedido = GenerarDsetPedidos(Me.Usuario.CodPersona, Me.Usuario.Idioma, idPedido, bModificarFavorito, ddl.SelectedValue)

			Dim pKeys(0) As DataColumn
			pKeys(0) = dsLineasPedido.Tables("CESTA").Columns("IDLINEA")
			dsLineasPedido.Tables("CESTA").PrimaryKey = pKeys

			Session("FilaQuitada") = False
			Me.InsertarEnCache("DsetEmisionPedidos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString() & "_" & idPedido.ToString(), dsLineasPedido.Copy)

			Return dsLineasPedido
		End Get
	End Property
	''' <summary>
	''' Generamos el conjunto de datos de la página.
	''' He añadido el FiltrosAnyadidos a los parámetros para cambiar la firma y que no de errores aunque no era necesario
	''' dado que FiltrosAnyadidos es una propiedad
	''' </summary>
	''' <param name="CodPersona">Código de la persona</param>
	''' <param name="Idioma">idioma en el que devolver el dataset</param>
	''' True-->Centros coste
	''' False-->Usuario
	''' </param>
	''' <returns>dataset con los pedidos</returns>
	''' <remarks>Llamada desde GenerarDsetPedidos. Máx. depende del número de datos</remarks>
	Private Shared Function GenerarDsetPedidos(ByVal CodPersona As String, ByVal Idioma As FSNLibrary.Idioma, ByVal idPedido As String, ByVal bModificarFavorito As Boolean, ByVal idEmpresa As Integer) As Object
		Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
		Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
		Dim oEmisionPedidos As CEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))
		Dim infoLineas As Object = oEmisionPedidos.Cargar_Lineas(CodPersona, Idioma.ToString(), idPedido, bModificarFavorito, idEmpresa)
		HttpContext.Current.Cache.Insert("CLineasPedido_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString() & "_" & idPedido.ToString(), CType(infoLineas(1), CLineasPedido), Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration,
										 New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
		Return CType(infoLineas(0), DataSet)
	End Function
	''' <summary>
	''' Devuelve la relación entre:
	''' campos del grid, del dataset de pedidos y el datatable con los datos de ancho, visibilidad y posicion
	''' Aquí se establecen los valores de visibilidad y ancho por defecto. Si hay valores guardados, se sobreescriben posteriormente
	''' </summary>
	''' <returns>Lista de Campos con la relación</returns>
	''' <remarks>Llamada desde confAnchosVisibilidadYPosicion. Máx. 0,1 seg.</remarks>
	Private ReadOnly Property relacionarEntregasyConfiguracion() As List(Of Campos)
		Get
			Dim relacionEntregasyConfiguracion As New List(Of Campos)
			'Cada campo consta de la info:
			'   Campo del stored que carga el whdg (ej: "RETRASADO")
			'   Ancho por defecto del campo
			'   Tipo de Campo
			'   ID del campo a usar cuando guardemos la configuración
			relacionEntregasyConfiguracion.Add(New Campos("AVISO", TipoCampoPedido.Generico, "", 3, True, 0))
			relacionEntregasyConfiguracion.Add(New Campos("NUM_LINEA", TipoCampoPedido.Generico, "NUM_LINEA", 3, True, 1))
			relacionEntregasyConfiguracion.Add(New Campos("IDLINEA", TipoCampoPedido.Generico, "IDLINEA", 0, False, 2))
			If bTodosImporte Then
				relacionEntregasyConfiguracion.Add(New Campos("ART_DEN", TipoCampoPedido.Generico, "ARTDEN", 50, True, 3))
				relacionEntregasyConfiguracion.Add(New Campos("PREC", TipoCampoPedido.Generico, "PREC", 0, False, 4))
				relacionEntregasyConfiguracion.Add(New Campos("CANT", TipoCampoPedido.Generico, "CANT", 0, False, 5))
				relacionEntregasyConfiguracion.Add(New Campos("UNI", TipoCampoPedido.Generico, "UNI", 0, False, 6))
				relacionEntregasyConfiguracion.Add(New Campos("COSTES", TipoCampoPedido.Generico, "COSTES", 15, True, 7))
				relacionEntregasyConfiguracion.Add(New Campos("DESCUENTOS", TipoCampoPedido.Generico, "DESCUENTOS", 15, True, 8))
				relacionEntregasyConfiguracion.Add(New Campos("IMPBRUTO", TipoCampoPedido.Generico, "IMPBRUTO", 14, True, 9))
			Else
				relacionEntregasyConfiguracion.Add(New Campos("ART_DEN", TipoCampoPedido.Generico, "ARTDEN", 33, True, 3))
				relacionEntregasyConfiguracion.Add(New Campos("PREC", TipoCampoPedido.Generico, "PREC", 14, True, 4))
				relacionEntregasyConfiguracion.Add(New Campos("CANT", TipoCampoPedido.Generico, "CANT", 14, True, 5))
				relacionEntregasyConfiguracion.Add(New Campos("UNI", TipoCampoPedido.Generico, "UNI", 5, True, 6))
				relacionEntregasyConfiguracion.Add(New Campos("COSTES", TipoCampoPedido.Generico, "COSTES", 10, True, 7))
				relacionEntregasyConfiguracion.Add(New Campos("DESCUENTOS", TipoCampoPedido.Generico, "DESCUENTOS", 10, True, 8))
				relacionEntregasyConfiguracion.Add(New Campos("IMPBRUTO", TipoCampoPedido.Generico, "IMPBRUTO", 14, True, 9))
			End If
			relacionEntregasyConfiguracion.Add(New Campos("DELETE", TipoCampoPedido.Generico, "DELETE", 3, True, 10))
			Return relacionEntregasyConfiguracion
		End Get
	End Property
	'Devuelve el valor de autofacturacion del proveedor pasado como parámetro
	Private ReadOnly Property ProveAutofacturacion(ByVal CodProve As String) As Boolean
		Get
			If ViewState("AutofacProve" & CodProve) IsNot Nothing Then
				Return ViewState("AutofacProve" & CodProve)
			Else
				Dim oProve As CProveedor = FSNServer.Get_Object(GetType(CProveedor))
				oProve.Cod = CodProve
				oProve.getAutofactura()
				ViewState("AutofacProve" & CodProve) = oProve.Autofactura
				Return oProve.Autofactura
			End If
		End Get
	End Property
	''' <summary>
	''' Listado de los Gestores para cada una de las partidas a las que el usuario podrá imputar
	''' </summary>
	''' <param name="PRES0">Árbol de Partida Prespuestaria del que queremos recuperar los gestores</param>
	Private ReadOnly Property GestoresPartidasUsu(ByVal PRES0 As String) As PartidasPRES5
		Get
			If HttpContext.Current.Cache("GestoresPartidasUsu_" & Me.Usuario.Cod & "_" & PRES0) Is Nothing Then
				Dim oPartidas As PartidasPRES5 = FSNServer.Get_Object(GetType(PartidasPRES5))
				oPartidas.ObtGestoresPartidasUsu(Me.Usuario.Cod, PRES0)
				Me.InsertarEnCache("GestoresPartidasUsu_" & Me.Usuario.Cod & "_" & PRES0, oPartidas)
			End If
			Return CType(HttpContext.Current.Cache("GestoresPartidasUsu_" & Me.Usuario.Cod & "_" & PRES0), PartidasPRES5)
		End Get
	End Property
#End Region
#Region "PartidasPresupuestarias"
	''' <summary>
	''' Función que nos devuelve un Datatable con las configuraciones de cada partida presupuestaria
	''' </summary>
	''' <param name="dsInfo">
	''' Podemos pasarle el dataset desde el que recupera los datos o dejar que los tome del DsetPedidos
	''' </param>
	''' <returns>Datatable con las configuraciones de cada partida presupuestaria</returns>
	''' <remarks>Llamado desde CargarCombosPartidas
	''' Tiempo máximo: 0 sec</remarks>
	Private Function CargarPartidasPresupuestarias(Optional ByVal dsInfo As DataSet = Nothing) As DataTable

		Dim query = From Datos In dsInfo.Tables("PARTIDASPRES0").AsEnumerable()
					Where (Not IsDBNull(Datos.Item("PRES5")))
					Order By Datos.Item("PARTIDA_DEN")
					Select Datos!PRES5, Datos!NIVEL, Datos!PARTIDA_DEN
					Distinct

		Dim PartidasPres = New DataTable("PartidasPres")
		PartidasPres.Columns.Add("PRES0", GetType(String))
		PartidasPres.Columns.Add("NIVEL", GetType(Integer))
		PartidasPres.Columns.Add("PARTIDA_DEN", GetType(String))
		For Each partida In query
			PartidasPres.Rows.Add(New Object() {partida.PRES5, partida.NIVEL, partida.PARTIDA_DEN})
		Next
		Return PartidasPres
	End Function
	''' <summary>
	''' Procedimiento que crea tantos combos en el buscador como partidas presupuestarias se usen
	''' Y rellena cada uno de los combos con las partidas que existan en las líneas de pedido de los pedidos emitidos por el usuario
	''' <param name="dsInfo">
	''' Podemos pasarle el dataset desde es que recupera los datos o dejar que los tome del DsetPedidos
	''' </param>
	''' </summary>
	''' <remarks>Llamado desde Page_Load
	''' Tiempo máximo: 0 sec</remarks>
	Private Sub CargarCombosPartidas(Optional ByVal dsInfo As DataSet = Nothing)
		Dim dtPartidasPres As DataTable

		If dsInfo Is Nothing OrElse dsInfo.Tables.Count = 0 Then
			dsInfo = getDropDownsInfo(FSNUser.CodPersona, FSNUser.Idioma)
		End If

		'1. coger las partidas (pres0) que hay en la caché (o llamada a la función) de las combos
		dtPartidasPres = CargarPartidasPresupuestarias(dsInfo)
		'2. Para cada partida, crear un label con el texto del punto 1, un textbox readonly y un botón para abrir el panel
		If dtPartidasPres.Rows.Count > 0 Then
			Dim TablaAsp As New Table
			Dim FilaAsp As New TableRow
			Dim Celda1Asp As New TableCell
			Dim Celda2Asp As New TableCell
			For Each PartidaPres As DataRow In dtPartidasPres.Rows
				If phrPartidasPresBuscador.FindControl("tbDdlPP") Is Nothing _
				OrElse phrPartidasPresBuscador.FindControl("tbDdlPP").FindControl("trPPres") Is Nothing Then '_" & PartidaPres("PRES0")
					'Inserta una serie de controles para cada una de las partidas dentro del control PlaceHolder->phrPartidasPresBuscador
					Celda1Asp = New TableCell
					Celda2Asp = New TableCell
					FilaAsp = New TableRow

					Celda1Asp.Style.Add("width", "10em")

					Celda1Asp.CssClass = "Etiqueta"
					Celda1Asp.Style.Add("text-align", "left")

					Dim tbPartida As New TextBox
					Dim tbPartidaHidden As New HiddenField
					Dim litPartida As New Literal()
					Dim imgPartida As New HtmlControls.HtmlImage
					If Me.Acceso.gbAccesoFSSM AndAlso Not _PargenSM Is Nothing Then
						litPartida.Text = IIf(_PargenSM.ImputacionPedido = Fullstep.FSNServer.PargenSM.EnumTipoImputacion.Obligatorio, "(*) ", String.Empty) & PartidaPres("PARTIDA_DEN") + " :"
					Else
						litPartida.Text = PartidaPres("PARTIDA_DEN") + " :"
					End If
					tbPartida.ID = "tbPPres"
					tbPartida.Style.Add("width", "20.3em")
					tbPartida.Style.Add("margin-left", "0.2em")
					tbPartida.ClientIDMode = UI.ClientIDMode.Static
					tbPartidaHidden.ID = "tbPPres_Hidden"
					tbPartidaHidden.ClientIDMode = UI.ClientIDMode.Static
					tbPartida.CssClass = "Normal"

					Celda1Asp.Controls.Add(litPartida)

					Celda2Asp.Controls.Add(tbPartida)
					Celda2Asp.Controls.Add(tbPartidaHidden)

					imgPartida.ID = "imgPPres_" & PartidaPres("PRES0")
					imgPartida.Src = "../../Images/abrir_ptos_susp.gif"
					imgPartida.Attributes.Add("onClick", "javascript:abrirPanelPartidasEP('" & mpePartidas.ClientID & "', '" & listadoPartidas.ClientID & "', '" & PartidaPres("PRES0") & "', '" & PartidaPres("PARTIDA_DEN") & "', '" & lblPartidaPres0.ClientID & "', '" & lblFiltroPartida.ClientID & "', '" & JSText(Textos(203)) & "', '" & listadoInputsFiltro.ClientID & "', event, null, null,'" & TiposDeDatos.ControlesBusquedaJQuery.PartidaPres & "', '" & listadoPartidas.ClientID & "', '" & listadoInputsFiltro.ClientID & "', '" & tbCtroCoste_Hidden.ClientID & "', '" & TiposDeDatos.Aplicaciones.EP & "', '" & CmbBoxEmpresa.ClientID & "',0,'');")
					imgPartida.Style.Add("border", "0px")
					imgPartida.Style.Add("cursor", "pointer")
					imgPartida.Style.Add("vertical-align", "middle")
					imgPartida.Style.Add("margin-left", "0.5em")
					Celda2Asp.Controls.Add(imgPartida)

					FilaAsp.ID = "trPPres"
					FilaAsp.Cells.Add(Celda1Asp)
					FilaAsp.Cells.Add(Celda2Asp)
					FilaAsp.Style.Add("line-height", "1em")
					TablaAsp.Rows.Add(FilaAsp)
					Exit For
				End If
			Next
			If phrPartidasPresBuscador.FindControl("tbDdlPP") Is Nothing Then
				TablaAsp.CellPadding = 0
				TablaAsp.CellSpacing = 0
				TablaAsp.ID = "tbDdlPP"
				TablaAsp.Width = Unit.Percentage("100")
				TablaAsp.ClientIDMode = UI.ClientIDMode.Static
				phrPartidasPresBuscador.Controls.Add(TablaAsp)
			End If
		End If
		'3. Añadimos al scriptmanager de la página el js que lanzará la carga asíncrona de las partidas desde cliente
		Dim oScriptCentros As HtmlGenericControl
		oScriptCentros = CargarScriptBusquedaJQueryEMP(TiposDeDatos.ControlesBusquedaJQuery.CentroCoste, listadoCentros.ClientID, FiltroCentros.ClientID, TiposDeDatos.Aplicaciones.EP, False)
		'Cuando se devuelve un script "script" hay que insertar en la página el script q se recibe
		If oScriptCentros IsNot Nothing AndAlso oScriptCentros.TagName = "script" Then
			RegistrarScript(oScriptCentros, "ScriptCentros")
		End If
	End Sub
	Public Structure partidaPedido
		Dim PRES0 As String
		Dim NivelImp As String
	End Structure
#End Region
#Region "Private Classes"
	Private Class JsonConfirmacionPedido
		Private _respuesta As Boolean
		Private _ped_num As Long
		Private _oe_num As Long
		Private _anyo As Long
		Private _prove As String
		Private _moneda As String
		Private _estado As Short
		Private _importe As Double
		Public Property respuesta As Boolean
			Get
				Return _respuesta
			End Get
			Set(ByVal value As Boolean)
				_respuesta = value
			End Set
		End Property
		Public Property anyo As Long
			Get
				Return _anyo
			End Get
			Set(ByVal value As Long)
				_anyo = value
			End Set
		End Property
		Public Property ped_num As Long
			Get
				Return _ped_num
			End Get
			Set(ByVal value As Long)
				_ped_num = value
			End Set
		End Property
		Public Property oe_num As Long
			Get
				Return _oe_num
			End Get
			Set(ByVal value As Long)
				_oe_num = value
			End Set
		End Property
		Public Property estado As Short
			Get
				Return _estado
			End Get
			Set(ByVal value As Short)
				_estado = value
			End Set
		End Property
		Public Property prove As String
			Get
				Return _prove
			End Get
			Set(ByVal value As String)
				_prove = value
			End Set
		End Property
		Public Property moneda As String
			Get
				Return _moneda
			End Get
			Set(ByVal value As String)
				_moneda = value
			End Set
		End Property
		Public Property importe As Double
			Get
				Return _importe
			End Get
			Set(ByVal value As Double)
				_importe = value
			End Set
		End Property
	End Class
	Private Class itemListaExterna
		Private _id As String
		Public Property id() As String
			Get
				Return _id
			End Get
			Set(ByVal value As String)
				_id = value
			End Set
		End Property
		Private _name As String
		Public Property name() As String
			Get
				Return _name
			End Get
			Set(ByVal value As String)
				_name = value
			End Set
		End Property
	End Class
#End Region
	''' <summary>
	''' Estructura compuesta por 5 elementos q nos va a permitir almacenar:
	''' 1. El nombre del campo en el datatable de entregas (DsetPedidos.Tables("ORDENES"))
	''' 2. Tipo de campo (Campo personalizado, Centro de Coste, Partida Presupuestaria o Genérico para todos los demás)
	''' 3. ID que usaremos para guardar el campo en la tabla EP_CONF_VISOR_RECEPCIONES
	''' 4. Si es visible 
	''' 5. Su anchura.
	''' 6. Su posición
	''' </summary>
	Private Structure Campos
		Public CampoEntrega As String
		Public TipoCampoPedido As TipoCampoPedido
		Public CampoEntregaIDGuardado As String
		Public Visible As Boolean
		Public Width As Double
		Public Position As Integer
		''' <summary>
		''' Método para crear una nueva instancia de la estructura Campos
		''' </summary>
		''' <param name="sCampoEntrega">nombre del campo en el datatable de entregas (DatasetEntregas.Tables("ORDENES"))</param>
		''' <param name="iTipoCampoPedido">Tipo de campo</param>
		''' <param name="sCampoEntregaIDGuardado">Nombre del campo con el que se guardarán los datos en la tabla que contiene los datos de configuración</param>
		''' <remarks>Llamada desde New. Máx. 0,1 seg.</remarks>
		Sub New(ByVal sCampoEntrega As String, ByVal iTipoCampoPedido As TipoCampoPedido, ByVal sCampoEntregaIDGuardado As String)
			Me.CampoEntrega = sCampoEntrega
			Me.TipoCampoPedido = iTipoCampoPedido
			Me.CampoEntregaIDGuardado = sCampoEntregaIDGuardado
		End Sub
		''' <summary>
		''' Método para crear una nueva instancia de la estructura Campos
		''' </summary>
		''' <param name="sCampoEntrega">nombre del campo en el datatable de entregas (DatasetEntregas.Tables("ORDENES"))</param>
		''' <param name="iTipoCampoPedido">Tipo de campo</param>
		''' <param name="sCampoEntregaIDGuardado">Nombre del campo con el que se guardarán los datos en la tabla que contiene los datos de configuración</param>
		''' <param name="dblWidth">ancho del campo</param>
		''' <remarks>Llamada desde New. Máx. 0,1 seg.</remarks>
		Sub New(ByVal sCampoEntrega As String, ByVal iTipoCampoPedido As TipoCampoPedido, ByVal sCampoEntregaIDGuardado As String, ByVal dblWidth As Double)
			Me.CampoEntrega = sCampoEntrega
			Me.TipoCampoPedido = iTipoCampoPedido
			Me.CampoEntregaIDGuardado = sCampoEntregaIDGuardado
			Me.Width = dblWidth
		End Sub
		''' <summary>
		''' Método para crear una nueva instancia de la estructura Campos
		''' </summary>
		''' <param name="sCampoEntrega">nombre del campo en el datatable de entregas (DatasetEntregas.Tables("ORDENES"))</param>
		''' <param name="iTipoCampoPedido">Tipo de campo</param>
		''' <param name="sCampoEntregaIDGuardado">Nombre del campo con el que se guardarán los datos en la tabla que contiene los datos de configuración</param>
		''' <param name="dblWidth">ancho del campo</param>
		''' <param name="bVisible">Es visible</param>
		''' <remarks>Llamada desde New. Máx. 0,1 seg.</remarks>
		Sub New(ByVal sCampoEntrega As String, ByVal iTipoCampoPedido As TipoCampoPedido, ByVal sCampoEntregaIDGuardado As String, ByVal dblWidth As Double, ByVal bVisible As Boolean)
			Me.CampoEntrega = sCampoEntrega
			Me.TipoCampoPedido = iTipoCampoPedido
			Me.CampoEntregaIDGuardado = sCampoEntregaIDGuardado
			Me.Width = dblWidth
			Me.Visible = bVisible
		End Sub
		''' <summary>
		''' Método para crear una nueva instancia de la estructura Campos
		''' </summary>
		''' <param name="sCampoEntrega">nombre del campo en el datatable de entregas (DatasetEntregas.Tables("ORDENES"))</param>
		''' <param name="iTipoCampoPedido">Tipo de campo</param>
		''' <param name="sCampoEntregaIDGuardado">Nombre del campo con el que se guardarán los datos en la tabla que contiene los datos de configuración</param>
		''' <param name="dblWidth">ancho del campo</param>
		''' <param name="bVisible">Es visible</param>
		''' <param name="iPosition">posición del campo</param>
		''' <remarks>Llamada desde New. Máx. 0,1 seg.</remarks>
		Sub New(ByVal sCampoEntrega As String, ByVal iTipoCampoPedido As TipoCampoPedido, ByVal sCampoEntregaIDGuardado As String, ByVal dblWidth As Double, ByVal bVisible As Boolean, ByVal iPosition As Integer)
			Me.CampoEntrega = sCampoEntrega
			Me.TipoCampoPedido = iTipoCampoPedido
			Me.CampoEntregaIDGuardado = sCampoEntregaIDGuardado
			Me.Width = dblWidth
			Me.Visible = bVisible
			Me.Position = iPosition
		End Sub
	End Structure
	''' <summary>
	''' Método que se ejecuta en el evento init de la página
	''' </summary>
	''' <param name="sender">la página</param>
	''' <param name="e">argumentos del evento</param>
	Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
		'Añadimos el jsLimiteDecimales.js al script compuesto que controla el scriptmanager para una carga más rápida de los scripts
		Dim scriptRef As New System.Web.UI.ScriptReference("~/js/jsLimiteDecimales.js")
		ScriptMgr.CompositeScript.Scripts.Add(scriptRef)

		scriptRef = New System.Web.UI.ScriptReference("~/js/jsCalendario.js")
		ScriptMgr.CompositeScript.Scripts.Add(scriptRef)

		scriptRef = New System.Web.UI.ScriptReference("~/js/jsTextboxFormat.js")
		ScriptMgr.CompositeScript.Scripts.Add(scriptRef)

		'Cargamos los scripts q manejan el textbox txtProveedor y los paneles que usan JQuery
		CargarScriptBuscadoresJQuery()

		ScriptMgr.LoadScriptsBeforeUI = False
	End Sub
	''' Revisado por: sra. Fecha: 23/01/2012
	''' <summary>
	''' Comprueba que el objeto que ha provocado el postback asíncrono es uno de los siguientes. Si es así, devuelve true para que salga de la función y no siga la ejecución
	''' </summary>
	''' <returns>True/False</returns>
	''' <remarks>Llamada desde: Page_Load(); Tiempo máx: 0,01 sg</remarks>
	Function CheckAsyncPostBackElementToExit()
		If (ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$CmbBoxDest") > 0 _
						OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$lblNomArt") > 0 _
						OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$SMActivosEP_btnBuscar") > 0 _
						OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$SMPartidasPresupuestarias_btnBuscar") > 0 _
						OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$_imgbtnNext") > 0 _
						OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$_imgbtnLast") > 0 _
						OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$_imgbtnFirst") > 0 _
						OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$_imgbtnPrev") > 0 _
						OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$_ddlOrdenarPor") > 0 _
						OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$cbDireccionEnvioFactura") > 0) Then
			Return True
		Else
			Return False
		End If
	End Function
	''' <summary>
	''' Evento de carga de la página, cargando las variables necesarias entre una llamada y otra y registrando controles como sincronos o asincronos segun conveniencia
	''' </summary>
	''' <param name="sender">La propia página</param>
	''' <param name="e">El evento de carga de la página</param>
	''' <remarks>
	''' Llamada desde:Cada vez que se hacen un postback de la pagina,sea sincrono o asincrono
	''' Tiempo máximo:Aproximadamente 1,5 seg</remarks>
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Dim gParametrosGenerales As TiposDeDatos.ParametrosGenerales
		ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
		CargarTextos()
		ScriptMgr.EnablePageMethods = True

		If ScriptMgr.IsInAsyncPostBack AndAlso CheckAsyncPostBackElementToExit() Then Exit Sub

		If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutaEP") Then
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaEP", "<script>var rutaEP = '" & ConfigurationManager.AppSettings("rutaEP") & "';</script>")
		End If
		If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "NumberFormatUsu") Then
			With FSNUser
				Dim sVariablesJavascript As String = "var UsuNumberDecimalSeparator = '" & .NumberFormat.NumberDecimalSeparator & "';"
				sVariablesJavascript = sVariablesJavascript & "var UsuNumberGroupSeparator='" & .NumberFormat.NumberGroupSeparator & "';"
				sVariablesJavascript = sVariablesJavascript & "var UsuNumberNumDecimals='" & .NumberFormat.NumberDecimalDigits & "';"
				Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NumberFormatUsu", sVariablesJavascript, True)
			End With
		End If
		'Visibilidad de los campos de proveedores
		Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "CamposProveOpcionales",
					"<script>var CamposProveOpcionales = {'Campo1':" & FSNServer.TipoAcceso.gbCampo1.ToString.ToLower &
														",'Campo2':" & FSNServer.TipoAcceso.gbCampo2.ToString.ToLower &
														",'Campo3':" & FSNServer.TipoAcceso.gbCampo3.ToString.ToLower &
														",'Campo4':" & FSNServer.TipoAcceso.gbCampo4.ToString.ToLower & "}</script>")
		Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaTheme", "<script>var rutaTheme = '" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "'</script>")
		Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "avisoEmitirPedido", "<script>var sAvisoEmisionPedido = '" & FSNServer.TipoAcceso.gEPAvisoEmitirPedido & "'</script>")
		Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bOblFecEntrega", "<script>var bOblFecEntrega=" & FSNServer.TipoAcceso.gbOblPedidoFecEntrega.ToString.ToLower & "</script>")

		'Los combos de partidas se crean dinámicamente por lo que tienen que crearse siempre que la página se recarga
		Dim dsDropDownInfo As New DataSet
		dsDropDownInfo = getDropDownsInfo(Me.Usuario.CodPersona, Me.Usuario.Idioma)
		If Acceso.gbAccesoFSSM Then
			If Acceso.gbAccesoFSSM And Not _PargenSM Is Nothing AndAlso _PargenSM.ImputacionPedido <> Fullstep.FSNServer.PargenSM.EnumTipoImputacion.NoSeImputa Then
				CargarCombosPartidas(IIf(dsDropDownInfo.Tables.Count > 0, dsDropDownInfo, Nothing))
			End If
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sbVerUON", "<script>var sbVerUON = '" & False & "' </script>")
			imgPanelCentros.Attributes.Add("onclick", "abrirPanelCentrosEP('" & mpeCentros.ClientID & "', '" & FiltroCentros.ClientID & "', event, 'tbCtroCoste', 'tbCtroCoste_Hidden',  '" & CmbBoxEmpresa.ClientID & "', '" & TiposDeDatos.ControlesBusquedaJQuery.CentroCoste & "', '" & listadoCentros.ClientID & "', '" & FiltroCentros.ClientID & "', '" & TiposDeDatos.Aplicaciones.EP & "', '" & False & "', '" & lblCentros.ClientID & "', '" & lblFiltroCentros.ClientID & "', '" & JSText(Textos(145).Substring(0, Len(Textos(145)) - 1)) & "','" & JSText(Textos(203)) & "')")

			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bActivos", "<script>var bActivos = true </script>")

			SMActivosEP.UsuarioPM = Usuario.CodPersona
			SMActivosEP.OnCancelarClientClick = "cerrarDivActivos();return false;"
			SMActivosEP.EnableViewState = True

			SMActivosEP.Visible = True
			LblGestorTitulo.Visible = True
			Dim sGestor As String
			If Acceso.gbAccesoFSSM And Not _PargenSM Is Nothing Then
				If _PargenSM.ImputacionPedido = Fullstep.FSNServer.PargenSM.EnumTipoImputacion.NoSeImputa Then
					LblGestorTitulo.Visible = False
					LblGestorCab.Visible = False
					lblCentroCoste.Visible = False
					tbCtroCoste.Visible = False
					tbCtroCoste_Hidden.Visible = False
					imgPanelCentros.Visible = False
					phrPartidasPresBuscador.Visible = False
					BtnCopiarCtroCoste.Visible = False
					BtnCopiarPartidas.Visible = False
				Else
					sGestor = seleccionaGestor(_PargenSM.Pres5)
					If sGestor <> String.Empty Then
						LblGestorCab.Text = sGestor
					Else
						LblGestorCab.Text = String.Empty
					End If
					BtnCopiarCtroCoste.Visible = True
					BtnCopiarPartidas.Visible = True
				End If
			End If
		Else
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bActivos", "<script>var bActivos = false </script>")
			lblCentroCoste.Visible = False
			tbCtroCoste.Visible = False
			tbCtroCoste_Hidden.Visible = False
			imgPanelCentros.Visible = False
			phrPartidasPresBuscador.Visible = False
			LblGestorCab.Text = String.Empty
			LblGestorTitulo.Visible = False
			BtnCopiarCtroCoste.Visible = False
			BtnCopiarPartidas.Visible = False
		End If

		If Not IsPostBack Then
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			'Imagen y texto de la cesta
			Master.ImgCabIzq = "~/images/camion.jpg"
			Master.TituloCabIzq = Textos(241)

			Master.CabTabCesta.Selected = True

			CargarScripts()
			Dim ddlTipos As DropDownList = CType(CmbBoxTipoPedido, DropDownList)
			ddlTipos.DataSource = TiposPedidos
			ddlTipos.DataBind()
			If ddlTipos.Items.Count > 1 Then _
				ddlTipos.Items.Insert(0, New ListItem(String.Empty, 0))

			lblProveedorConfirmPedido.Attributes.Add("onclick", "infoProve();")
			FSNLinkProve.Attributes.Add("onclick", "infoProve();")

			Dim btnModificarFavoritoVisible, btnModificarFavoritoAccion, imgGuardarFavoritoVisible, imgGuardarFavoritoAccion, btnEmitirPedidoVisible, btnGuardarCestaVisible, txtNomFavoritoVisible As Boolean
			txtNomFavoritoVisible = True
			If DesdeFavorito Then
				Dim bModificarFavorito As Boolean = False
				If Not Request.QueryString("ModificarFavorito") Is Nothing Then
					bModificarFavorito = Request("ModificarFavorito")
				End If

				If bModificarFavorito Then
					btnModificarFavoritoVisible = True
					btnModificarFavoritoAccion = True
					imgGuardarFavoritoVisible = False
					imgGuardarFavoritoAccion = False
					btnEmitirPedidoVisible = False
					btnGuardarCestaVisible = False

					If Not Request.QueryString("ID") Is Nothing Then
						idLinea = Request.QueryString("ID")
						Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sID", "<script>var sID = '" & idLinea & "' </script>")
						Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bModificarFavorito", "<script>var bModificarFavorito = '1' </script>")
						Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sIDFavorito", "<script>var sIDFavorito = '" & idLinea & "' </script>")
						Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "desdeFavorito", "<script>var desdeFavorito = '1' </script>")
						Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bEmitirFavorito", "<script>var bEmitirFavorito = '0' </script>")
					End If
				Else
					If Request("ID") Is Nothing Then
						idLinea = 0
					Else
						idLinea = CInt(Request("ID"))
					End If
					Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sIDFavorito", "<script>var sIDFavorito = '" & idLinea & "' </script>")
					Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "desdeFavorito", "<script>var desdeFavorito = '1' </script>")
					Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bModificarFavorito", "<script>var bModificarFavorito = '0' </script>")
					Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bEmitirFavorito", "<script>var bEmitirFavorito = '1' </script>")

					btnModificarFavoritoVisible = True
					btnModificarFavoritoAccion = False
					imgGuardarFavoritoVisible = True
					imgGuardarFavoritoAccion = True
					btnEmitirPedidoVisible = True
					btnGuardarCestaVisible = True
				End If
			Else
				If (Not Request("NuevoFavorito") Is Nothing) Then
					Dim bNuevoFavorito As Boolean = Request("NuevoFavorito")
					If bNuevoFavorito Then
						btnModificarFavoritoVisible = False
						btnModificarFavoritoAccion = False
						imgGuardarFavoritoVisible = True
						imgGuardarFavoritoAccion = False
						btnEmitirPedidoVisible = False
						btnGuardarCestaVisible = False

						If Not Request.QueryString("ID") Is Nothing Then
							idLinea = Request.QueryString("ID")
							Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sID", "<script>var sID = '" & idLinea & "' </script>")
							Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "desdeFavorito", "<script>var desdeFavorito = '0' </script>")
							Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sIDFavorito", "<script>var sIDFavorito = '0' </script>")
							Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bModificarFavorito", "<script>var bModificarFavorito = '0' </script>")
							Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bEmitirFavorito", "<script>var bEmitirFavorito = '0' </script>")
						End If
					Else
						If Request("ID") Is Nothing Then
							idLinea = 0
						Else
							idLinea = CInt(Request("ID"))
						End If
						Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sIDFavorito", "<script>var sIDFavorito = '" & idLinea & "' </script>")
						Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "desdeFavorito", "<script>var desdeFavorito = '1' </script>")
						Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bModificarFavorito", "<script>var bModificarFavorito = '0' </script>")
						Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bEmitirFavorito", "<script>var bEmitirFavorito = '0' </script>")

						btnModificarFavoritoVisible = True
						btnModificarFavoritoAccion = False
						imgGuardarFavoritoVisible = True
						imgGuardarFavoritoAccion = True
						btnEmitirPedidoVisible = True
						btnGuardarCestaVisible = True
					End If
				Else
					If Not Request.QueryString("ID") Is Nothing Then
						idLinea = Request.QueryString("ID")
						Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sID", "<script>var sID = '" & idLinea & "' </script>")
						Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "desdeFavorito", "<script>var desdeFavorito = '0' </script>")
						Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sIDFavorito", "<script>var sIDFavorito = '0' </script>")
						Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bModificarFavorito", "<script>var bModificarFavorito = '0' </script>")
						Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bEmitirFavorito", "<script>var bEmitirFavorito = '0' </script>")
					End If

					btnModificarFavoritoVisible = False
					btnModificarFavoritoAccion = False
					imgGuardarFavoritoVisible = (Request.QueryString("abierto") Is Nothing)
					imgGuardarFavoritoAccion = (Request.QueryString("abierto") Is Nothing)
					btnEmitirPedidoVisible = True
					btnGuardarCestaVisible = True
					txtNomFavoritoVisible = (Request.QueryString("abierto") Is Nothing)
				End If
			End If
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bMostrarDireccionEnvioFactura", "<script>var bMostrarDireccionEnvioFactura = false </script>")
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bMostrarCodigoProve", "<script>var bMostrarCodigoProve = " & LCase(FSNUser.MostrarCodProve) & " </script>")
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bMostrarCodigoArt", "<script>var bMostrarCodigoArt = " & LCase(FSNUser.MostrarCodArt) & " </script>")
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "gbOblCodPedidoObl", "<script>var gbOblCodPedidoObl = " & LCase(Me.Acceso.gbOblCodPedidoObl) & " </script>")
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "gbEPMarcarAcepProve", "<script>var gbEPMarcarAcepProve = " & LCase(Me.Acceso.gbEPMarcarAcepProve) & " </script>")
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sCodPersona", "<script>var sCodPersona = '" & FSNUser.CodPersona & "' </script>")

			Dim ProveAnimationClientID As String = FSNPanelDatosProveCab.AnimationClientID
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ProveAnimationClientID", "<script>var ProveAnimationClientID = '" & ProveAnimationClientID & "' </script>")

			Dim DireAnimationClientID As String = FSNPanelDireccionEnvioFactura.AnimationClientID
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "DireAnimationClientID", "<script>var DireAnimationClientID = '" & DireAnimationClientID & "' </script>")

			Dim ProveDynamicPopulateClienteID As String = FSNPanelDatosProveCab.DynamicPopulateClientID
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ProveDynamicPopulateClienteID", "<script>var ProveDynamicPopulateClienteID = '" & ProveDynamicPopulateClienteID & "' </script>")

			Dim DireDynamicPopulateClienteID As String = FSNPanelDireccionEnvioFactura.DynamicPopulateClientID
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "DireDynamicPopulateClienteID", "<script>var DireDynamicPopulateClienteID = '" & DireDynamicPopulateClienteID & "' </script>")

			If Me.Acceso.gbAccesoFSSM AndAlso Not _PargenSM Is Nothing Then
				Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bAccesoFSSM", "<script>var bAccesoFSSM = true  </script>")
				If _PargenSM.ImputacionPedido = Fullstep.FSNServer.PargenSM.EnumTipoImputacion.Obligatorio Then
					Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bSMOblig", "<script>var bSMOblig = true </script>")
				Else
					Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bSMOblig", "<script>var bSMOblig = false </script>")
				End If
				If _PargenSM.ImputacionPedido = Fullstep.FSNServer.PargenSM.EnumTipoImputacion.NoSeImputa Then
					Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bNoImputaPed", "<script>var bImputaPed = false </script>")
				Else
					Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bNoImputaPed", "<script>var bImputaPed = true </script>")
				End If
			Else
				Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bAccesoFSSM", "<script>var bAccesoFSSM = false </script>")
				Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bSMOblig", "<script>var bSMOblig = false </script>")
			End If

			ddlTipos = CType(CmbBoxEmpresa, DropDownList)
			ddlTipos.DataSource = Me.Empresas
			ddlTipos.DataBind()

			'9624
			If Not Me.DesdeFavorito Then ddlTipos.SelectedValue = Cargar_Empresa_Primer_PluriAnual(Request.QueryString("ID"), ddlTipos.SelectedValue)

			If Acceso.gbOblCodPedido And Not Acceso.gbOblCodPedidoBloq Then
				Dim lbl As Label = CType(LblNumSap, Label)
				lbl.Text = Me.Acceso.nomPedERP(Me.Idioma) & " :"
				If Me.Acceso.gbOblCodPedidoObl Then
					lbl.Text = "(*) " & lbl.Text
				End If
			Else
				LblNumSap.Visible = False
				TxtBxPedSAP.Visible = False
			End If

			'Si tiene permiso de cambiar el %...
			If FSNUser.EPPermitirModificarPorcentajeDesvio Then
				lblDesvioRecepcion.Visible = True
				txtDesvioRecepcion.Visible = True
				btnCopiarPorcentajeDesvio.Visible = True
			Else
				lblDesvioRecepcion.Visible = False
				txtDesvioRecepcion.Attributes.Add("style", "display:none;")
				btnCopiarPorcentajeDesvio.Attributes.Add("style", "display:none;")
			End If

			CargarwhdgArticulos()
			If Me.Empresas.Count = 0 Then
				textError.InnerHtml = "<p>" & Textos(51) & "</p>"
				divError.Style.Add("z-index", "200002")
				divError.Style.Remove("display")
				divError.Style.Add("display", "block")

				divError.Style.Add("position", "absolute")
				divError.Style.Add("margin", "0 auto")
				divError.Style.Add("left", "30%")

				btnModificarFavoritoVisible = False
				btnEmitirPedidoVisible = False
				btnGuardarCestaVisible = False
				imgGuardarFavoritoVisible = False
				txtNomFavoritoVisible = False
			End If
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "btnModificarFavorito",
					"<script>var btnModificarFavorito = {'visible':" & btnModificarFavoritoVisible.ToString.ToLower & ",'accion':" & btnModificarFavoritoAccion.ToString.ToLower & "}</script>")
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "imgGuardarFavorito",
			"<script>var imgGuardarFavorito = {'visible':" & imgGuardarFavoritoVisible.ToString.ToLower & ",'accion':" & imgGuardarFavoritoAccion.ToString.ToLower & "}</script>")
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "btnEmitirPedido",
				"<script>var btnEmitirPedido = {'visible':" & btnEmitirPedidoVisible.ToString.ToLower & "}</script>")
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "btnGuardarCesta",
				"<script>var btnGuardarCesta = {'visible':" & btnGuardarCestaVisible.ToString.ToLower & "}</script>")
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "txtNomFavorito",
				"<script>var txtNomFavorito = {'visible':" & txtNomFavoritoVisible.ToString.ToLower & "}</script>")
		Else
			Dim idPedido As String
			If Me.DesdeFavorito = True Then
				If Request.QueryString("ModificarFavorito") Is Nothing Then
					idPedido = HttpContext.Current.Cache("IdDesdeFav_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
				Else
					idPedido = Request.QueryString("ID")
				End If
			Else
				idPedido = Request.QueryString("ID")
			End If
			If idPedido Is Nothing Then Exit Sub

			Dim ds As DataSet = HttpContext.Current.Cache("DsetEmisionPedidos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString() & "_" & idPedido.ToString())
			Dim dsNew As New DataSet

			whdgArticulos.Rows.Clear()
			whdgArticulos.GridView.ClearDataSource()
			'CargarwhdgArticulos(False)
			Select Case Request("__EVENTTARGET")
				Case btnArticulosPostBack.ClientID
					'Aqui va a entrar en el momento que se edite el nº de una línea o se haya dado al botón subir/bajar
					ModoEdicion = True
					Dim serializer As New JavaScriptSerializer
					Dim oArgumentos As Object = serializer.Deserialize(Of Object)(Request("__EVENTARGUMENT"))
					Dim drOld, drNew As DataRow

					drOld = ds.Tables(0).Select("NUM_LINEA='" & oArgumentos("NUMInicial") & "'").FirstOrDefault
					drNew = ds.Tables(0).Select("NUM_LINEA='" & oArgumentos("NUMActual") & "'").FirstOrDefault

					drOld("NUM_LINEA") = oArgumentos("NUMActual")

					'Este caso es si se ha dado a los botones subir/bajar
					If (drNew IsNot Nothing) Then
						drNew("NUM_LINEA") = oArgumentos("NUMInicial")
					End If

					'Se ordena
					Dim dv As DataView = ds.Tables(0).DefaultView
					dv.Sort = "NUM_LINEA ASC"

					Dim dtOrdenado As DataTable = dv.ToTable
					Dim pKeys(0) As DataColumn
					pKeys(0) = dtOrdenado.Columns("IDLINEA")
					dtOrdenado.PrimaryKey = pKeys
					dsNew.Tables.Add(dtOrdenado.Copy)
					Me.InsertarEnCache("DsetEmisionPedidos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString() & "_" & idPedido.ToString(), dsNew)
					whdgArticulos.DataSource = dsNew
				Case btnArticuloDeletePostBack.ClientID
					Dim serializer As New JavaScriptSerializer
					Dim oArgumentos As Object = serializer.Deserialize(Of Object)(Request("__EVENTARGUMENT"))
					ds.Tables(0).Rows.Remove(ds.Tables(0).Select("IDLINEA=" & CType(oArgumentos("linea"), Integer))(0))

					'Se ordena
					Dim dv As DataView = ds.Tables(0).DefaultView
					dv.Sort = "NUM_LINEA ASC"

					Dim dtOrdenado As DataTable = dv.ToTable
					Dim pKeys(0) As DataColumn
					pKeys(0) = dtOrdenado.Columns("IDLINEA")
					dtOrdenado.PrimaryKey = pKeys
					dsNew.Tables.Add(dtOrdenado.Copy)

					If oArgumentos("editada") Then
						ModoEdicion = True
					End If

					'Al eliminar, el nÃºmero de lÃ­nea se reordenarÃ¡ solo si no ha sido editada antes. Si ha sido editada, se deja la numeraciÃ³n como este
					If Not oArgumentos("editada") Then
						For lRow As Integer = 0 To dsNew.Tables(0).Rows.Count - 1
							Select Case dsNew.Tables(0).Rows.Count
								Case 1 To 99
									dsNew.Tables(0).Rows(lRow)("NUM_LINEA") = (lRow + 1) * 10
								Case 100 To 200
									dsNew.Tables(0).Rows(lRow)("NUM_LINEA") = IIf(lRow = 0, 1, lRow * 5)
								Case 201 To 499
									dsNew.Tables(0).Rows(lRow)("NUM_LINEA") = (lRow * 2) + 1
								Case 500 To 999
									dsNew.Tables(0).Rows(lRow)("NUM_LINEA") = lRow + 1
								Case Else
									If lRow > 998 Then
										dsNew.Tables(0).Rows(lRow)("NUM_LINEA") = lRow + 1
									Else
										dsNew.Tables(0).Rows(lRow)("NUM_LINEA") = 1000
									End If
							End Select
						Next
					End If
					Me.InsertarEnCache("DsetEmisionPedidos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString() & "_" & idPedido.ToString(), dsNew)
					whdgArticulos.DataSource = dsNew
				Case Else
					ModoEdicion = False
					whdgArticulos.DataSource = ds
			End Select

			whdgArticulos.GridView.DataSource = whdgArticulos.DataSource
			whdgArticulos.DataMember = "CESTA"
			whdgArticulos.DataKeyFields = "IDLINEA"
			whdgArticulos.DataBind()
		End If
		pnlGrid.Update()
	End Sub
	Private Sub CargarTextos()
		LblProve.Text = Textos(50)

		btnAceptarErrorE.Text = Textos(79)
		btnAceptarPedido.Text = Textos(79)
		PbtnAceptarErrorPartida.Text = Textos(79)
		btnAceptarUnidad1.Text = Textos(79)
		PbtnAceptarErrorAviso.Text = Textos(79)
		PbtnCancelarErrorPartida.Text = Textos(34)
		btnCancelarUnidad1.Text = Textos(34)
		PbtnCancelarErrorAviso.Text = Textos(34)

		lblCopiarDatosLinea.Text = Textos(267)

		lblImporteBruto.Text = Textos(206)
		lblCostesGenerales.Text = Textos(209)
		lblDescuentosGenerales.Text = Textos(210)
		lblImporteTotal.Text = Textos(211)

		lblDesvioRecepcion.Text = Textos(347) & " :"

		LblTipoPedido.Text = "(*) " & Textos(129) & ": "
		LblProveErp.Text = "(*) " & Textos(49)

		LblReceptor.Text = Textos(48)
		LblEmpresa.Text = "(*) " & Textos(47)
		lblOrgCompras.Text = "(*) " & Textos(313) & ": "
		lblCentroOrgCompra.Text = "(*) " & Textos(316) & ": "
		LblGestorTitulo.Text = Textos(189) & ": "
		LblDireccion.Text = Textos(165)

		lblCostes.Text = Textos(207)
		lblDescuentos.Text = Textos(208)

		lblCostesLinea.Text = Textos(207)
		lblDescuentosLinea.Text = Textos(208)

		lblCabUnidades.Text = Textos(121)
		lblCodUnidad.Text = Textos(122)
		lblUnidadCompra.Text = Textos(123)
		lblFactorConversion.Text = Textos(124)
		lblCantMinima.Text = Textos(125)

		lblArchivos.Text = Textos(225)

		whdgArticulos.GroupingSettings.EmptyGroupAreaText = Textos(202)

		lblDatosGenerales.Text = Textos(226)
		lblResumenLinea.Text = Textos(227)
		lblDatosLinea.Text = Textos(228)
		lblOtrosDatos.Text = Textos(218)

		LblTxtSelecDest.Text = Textos(87)

		lblCodigoAtributoTablaItem.Text = Textos(88)
		lblCodigoAtributoTablaLinea.Text = Textos(88)
		lblCodCoste.Text = Textos(88)
		lblCodCosteLinea.Text = Textos(88)
		lblCodDescuento.Text = Textos(88)
		lblCodDescuentoLinea.Text = Textos(88)

		lblDenomAtributoTablaItem.Text = Textos(89)
		lblDenomAtributoTablaLinea.Text = Textos(89)
		lblDenCoste.Text = Textos(89)
		lblDenCosteLinea.Text = Textos(89)
		lblDenDescuento.Text = Textos(89)
		lblDenDescuentoLinea.Text = Textos(89)

		lblTipoAtributoTablaItem.Text = Textos(229)
		lblTipoAtributoTablaLinea.Text = Textos(229)
		lblTipoCoste.Text = Textos(229)
		lblTipoCosteLinea.Text = Textos(229)
		lblTipoDescuento.Text = Textos(229)
		lblTipoDescuentoLinea.Text = Textos(229)

		lblIntroAtributoTablaItem.Text = Textos(230)
		lblIntroAtributoTablaLinea.Text = Textos(230)
		lblIntroCoste.Text = Textos(230)
		lblIntroCosteLinea.Text = Textos(230)
		lblIntroDescuento.Text = Textos(230)
		lblIntroDescuentoLinea.Text = Textos(230)

		lblObligAtributoTablaItem.Text = Textos(231)
		lblObligAtributoTablaLinea.Text = Textos(231)
		LblObli.Text = Textos(231)

		lblValorAtributoTablaItem.Text = Textos(237)
		lblValorAtributoTablaLinea.Text = Textos(237)
		lblValCoste.Text = Textos(237)
		lblValCosteLinea.Text = Textos(237)
		lblValDescuento.Text = Textos(237)
		lblValDescuentoLinea.Text = Textos(237)

		lblImpCoste.Text = Textos(12)
		lblImpCosteLinea.Text = Textos(12)
		lblImpDescuento.Text = Textos(12)
		lblImpDescuentoLinea.Text = Textos(12)

		lblTotCostes.Text = Textos(233)
		lblTotCostesLinea.Text = Textos(233)
		lblTotDescuento.Text = Textos(234)
		lblTotDescuentoLinea.Text = Textos(234)

		lblNumLinea.Text = Textos(232)
		lblNumLineaCoste.Text = Textos(232)
		lblNumLineaDescuento.Text = Textos(232)

		LblDest.Text = "(*) " & Textos(13) & " :"
		LblAlmacen.Text = Textos(136) & " :"
		LblFecEntSol.Text = IIf(FSNServer.TipoAcceso.gbOblPedidoFecEntrega, "(*) ", "") & Textos(17)
		lblEstadoEmisionPedido.Text = Textos(196)
		lblProveedorCabeceraConfirmPedido.Text = Textos(142)
		lblPedidoCabeceraConfirmPedido.Text = Textos(2)
		lblImporteCabeceraConfirmPedido.Text = Textos(12)
		lblEstadoCabeceraConfirmPedido.Text = Textos(253)
		lblPlanEntrega.Text = Textos(320)
		lblPlanesEntrega.Text = Textos(320)
		lblRecepcionAutomatica.Text = Textos(321)
		lblSolicitAcepProve.Text = Textos(332)

		'Obtenemos la denominacion de la partida presupuestaria
		If Acceso.gbAccesoFSSM AndAlso Not _PargenSM Is Nothing Then
			If _PargenSM.ImputacionPedido = Fullstep.FSNServer.PargenSM.EnumTipoImputacion.NoSeImputa Then
				lblCentroCoste.Text = String.Empty
				tbCtroCoste.Style.Add("visibility", "hidden")
				BtnCopiarPartidas.Style.Add("visibility", "hidden")
				BtnCopiarPartidas.Style.Add("visibility", "hidden")
				phrPartidasPresBuscador.Visible = False
				_PartidaPresupuestariaDEN = ""
				imgPanelCentros.Style.Add("visibility", "hidden")
				BtnCopiarCtroCoste.Style.Add("visibility", "hidden")
			Else
				lblCentroCoste.Text = IIf(_PargenSM.ImputacionPedido = Fullstep.FSNServer.PargenSM.EnumTipoImputacion.Obligatorio, "(*) ", String.Empty) & Textos(145)
				Dim PartPres As FSNServer.PartidasPRES5 = CType(FSNServer.Get_Object(GetType(FSNServer.PartidasPRES5)), FSNServer.PartidasPRES5)
				If Not PartPres Is Nothing Then _PartidaPresupuestariaDEN = PartPres.DenominacionPartidaPRES5(Usuario.Idioma, _PargenSM.Pres5, "", "", "", "", True)
				PartPres = Nothing
			End If
		Else
			lblCentroCoste.Text = String.Empty
			tbCtroCoste.Style.Add("visibility", "hidden")
			BtnCopiarPartidas.Style.Add("visibility", "hidden")
			tbActivosLinea.Style.Add("visibility", "hidden")
			BtnCopiarPartidas.Style.Add("visibility", "hidden")
			phrPartidasPresBuscador.Visible = False
			_PartidaPresupuestariaDEN = ""
			lblActivoDesc.Style.Add("visibility", "hidden")
			imgPanelCentros.Style.Add("visibility", "hidden")
			BtnCopiarCtroCoste.Style.Add("visibility", "hidden")
			imgActivo.Style.Add("visibility", "hidden")
			imgAplicarActivo.Style.Add("visibility", "hidden")
		End If
		Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sPartidaDen", "<script>var sPartidaDen = '" & _PartidaPresupuestariaDEN & "' </script>")
		lblObsGen.Text = Textos(5) & " :"
		lblObsGenLin.Text = Textos(128) & " :"
		lblArcPed.Text = Textos(120) & " :"
		lblArcPedLin.Text = Textos(114)

		lblNuevoAdjunto.Text = Textos(6)
		lblNuevoAdjuntoLin.Text = Textos(6)
		lblObservacionesAdjuntoLin.Text = Textos(116) & " :"

		LblCodDest.Text = Textos(88)
		LblDenDest.Text = Textos(89)
		LblDirecDest.Text = Textos(90)
		LblCpDest.Text = Textos(91)
		LblPobDest.Text = Textos(92)
		LblProvDest.Text = Textos(93)
		LblPaisDest.Text = Textos(94)
		ModuloIdioma = TiposDeDatos.ModulosIdiomas.TraspasoAdjudicaciones
		If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "Textos") Then
			Dim sVariableJavascriptTextosBoolean As String = "var TextosBoolean = new Array();"
			sVariableJavascriptTextosBoolean &= "TextosBoolean[0]='" & JSText(Textos(44)) & "';"
			sVariableJavascriptTextosBoolean &= "TextosBoolean[1]='" & JSText(Textos(43)) & "';"
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Textos", sVariableJavascriptTextosBoolean, True)
		End If
		ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
		If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosScript") Then
			Dim sVariableJavascriptTextosJScript As String = "var TextosJScript = new Array();"
			sVariableJavascriptTextosJScript &= "TextosJScript[0]='" & JSText(Textos(204)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[1]='" & JSText(Textos(205)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[2]='" & FSNUser.IdiomaCod & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[3]='" & JSText(Textos(212)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[4]='" & JSText(Textos(207)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[5]='" & JSText(Textos(206)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[6]='" & JSText(Textos(213)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[7]='" & JSText(Textos(79)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[8]='" & JSText(Textos(34)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[9]='" & JSText(Textos(214)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[10]='" & JSText(Textos(215)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[11]='" & JSText(Textos(216)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[12]='" & JSText(Textos(217)) & ":';" 'Es obligatorio cabecera...
			sVariableJavascriptTextosJScript &= "TextosJScript[13]='" & JSText(Textos(207)) & "';" 'Costes
			sVariableJavascriptTextosJScript &= "TextosJScript[14]='" & JSText(Textos(208)) & "';" 'Descuentos
			sVariableJavascriptTextosJScript &= "TextosJScript[15]='" & JSText(Textos(228)) & "';" 'Datos Linea
			sVariableJavascriptTextosJScript &= "TextosJScript[16]='" & JSText(Textos(219)) & ":';" 'Es obligatorio linea...
			sVariableJavascriptTextosJScript &= "TextosJScript[17]='" & JSText(Textos(220)) & "';" 'Debe seleccionar un receptor...
			sVariableJavascriptTextosJScript &= "TextosJScript[18]='" & JSText(Textos(221)) & "';" 'Error 1 Cantidad
			sVariableJavascriptTextosJScript &= "TextosJScript[19]='" & JSText(Textos(222)) & "';" 'Error 2 Cantidad
			sVariableJavascriptTextosJScript &= "TextosJScript[20]='" & JSText(Textos(223)) & "';" 'Error 3 Cantidad
			sVariableJavascriptTextosJScript &= "TextosJScript[21]='" & JSText(Textos(224)) & "';" 'Error 4 Cantidad
			sVariableJavascriptTextosJScript &= "TextosJScript[22]='" & JSText(Textos(86)) & "';" 'Seleccione destino
			sVariableJavascriptTextosJScript &= "TextosJScript[23]='" & JSText(Textos(88)) & "';" 'Codigo
			sVariableJavascriptTextosJScript &= "TextosJScript[24]='" & JSText(Textos(89)) & "';" 'Denominacion
			sVariableJavascriptTextosJScript &= "TextosJScript[25]='" & JSText(Textos(90)) & "';" 'Direccion
			sVariableJavascriptTextosJScript &= "TextosJScript[26]='" & JSText(Textos(91)) & "';" 'CP
			sVariableJavascriptTextosJScript &= "TextosJScript[27]='" & JSText(Textos(92)) & "';" 'Poblacion
			sVariableJavascriptTextosJScript &= "TextosJScript[28]='" & JSText(Textos(93)) & "';" 'Provincia
			sVariableJavascriptTextosJScript &= "TextosJScript[29]='" & JSText(Textos(94)) & "';" 'Pais
			sVariableJavascriptTextosJScript &= "TextosJScript[30]='" & JSText(Textos(238)) & "';" 'Error Fecha entrega linea
			sVariableJavascriptTextosJScript &= "TextosJScript[31]='" & JSText(Textos(13)) & "';" 'Destino
			sVariableJavascriptTextosJScript &= "TextosJScript[32]='" & JSText(Textos(240)) & "';" 'Error Almacen
			sVariableJavascriptTextosJScript &= "TextosJScript[33]='" & JSText(Textos(158)) & "';" 'Proveedor ERP obligatorio
			sVariableJavascriptTextosJScript &= "TextosJScript[34]='" & JSText(Textos(235)) & "';" 'No es posible emitir el pedido...
			sVariableJavascriptTextosJScript &= "TextosJScript[35]='" & JSText(Textos(236)) & "';" 'LA empresa seleccionado no coincide con las OUNs del art...
			sVariableJavascriptTextosJScript &= "TextosJScript[36]='" & JSText(Textos(167)) & "';" 'Obligatorio seleccionar direccion factura
			sVariableJavascriptTextosJScript &= "TextosJScript[37]='" & JSText(Textos(66)) & "';" 'No se puede emitir el pedido
			sVariableJavascriptTextosJScript &= "TextosJScript[38]='" & JSText(Textos(185)) & "';" 'Caducados pero puede continuar
			sVariableJavascriptTextosJScript &= "TextosJScript[39]='" & JSText(Textos(186)) & "';" 'Caducados y no puede continuar
			sVariableJavascriptTextosJScript &= "TextosJScript[40]='" & JSText(Textos(188)) & "';" 'Error Gestor
			sVariableJavascriptTextosJScript &= "TextosJScript[41]='" & JSText(Textos(187)) & "';" 'Error Partida
			sVariableJavascriptTextosJScript &= "TextosJScript[42]='" & JSText(Textos(151)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[43]='" & JSText(Textos(148)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[44]='" & JSText(Textos(153)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[45]='" & JSText(Textos(150)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[46]='" & JSText(Textos(152)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[47]='" & JSText(Textos(149)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[48]='" & JSText(Textos(156)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[49]='" & JSText(Textos(37)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[50]='" & JSText(Textos(117)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[51]='" & JSText(Textos(232)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[52]='" & JSText(Textos(270)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[53]='" & JSText(Textos(140)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[54]='" & JSText(Textos(141)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[55]='" & JSText(Textos(271)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[56]='" & JSText(Textos(84)) & "';"
			ModuloIdioma = TiposDeDatos.ModulosIdiomas.Activos
			sVariableJavascriptTextosJScript &= "TextosJScript[57]='" & JSText(Textos(12)) & "';"
			ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
			sVariableJavascriptTextosJScript &= "TextosJScript[58]='" & JSText(Textos(154)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[59]='" & JSText(Textos(132)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[60]='" & JSText(Textos(136)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[61]='" & JSText(Textos(274)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[62]='" & JSText(Textos(275)) & "';"
			'Textos tokenInput
			sVariableJavascriptTextosJScript &= "TextosJScript[63]='" & JSText(Textos(286)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[64]='" & JSText(Textos(284)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[65]='" & JSText(Textos(285)) & "';"
			'Buscador Lista externa
			sVariableJavascriptTextosJScript &= "TextosJScript[66]='" & JSText(Textos(287)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[67]='" & JSText(Textos(288)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[68]='" & JSText(Textos(289)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[69]='" & JSText(Textos(290)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[70]='" & JSText(Textos(291)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[71]='" & JSText(Textos(282)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[72]='" & JSText(Textos(283)) & "';"
			'botonesCabecera
			sVariableJavascriptTextosJScript &= "TextosJScript[73]='" & JSText(Textos(79)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[74]='" & JSText(Textos(38)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[75]='" & JSText(Textos(33)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[76]='" & JSText(Textos(171)) & "';"
			'InfoProve campos opcionales
			sVariableJavascriptTextosJScript &= "TextosJScript[77]='" & JSText(Textos(276)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[78]='" & JSText(Textos(277)) & " ';"
			sVariableJavascriptTextosJScript &= "TextosJScript[79]='" & JSText(Textos(278)) & " ';"
			sVariableJavascriptTextosJScript &= "TextosJScript[80]='" & JSText(Acceso.nomCampo1(Me.Idioma).ToString & ": ") & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[81]='" & JSText(Acceso.nomCampo2(Me.Idioma).ToString & ": ") & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[82]='" & JSText(Acceso.nomCampo3(Me.Idioma).ToString & ": ") & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[83]='" & JSText(Acceso.nomCampo4(Me.Idioma).ToString & ": ") & "';"
			'Texto comunicaciÃ³n
			sVariableJavascriptTextosJScript &= "TextosJScript[84]='" & JSText(Textos(292)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[85]='" & JSText(Textos(79)) & "';"
			'Alertas validacion cantidad/importe pedido abierto
			sVariableJavascriptTextosJScript &= "TextosJScript[86]='" & JSText(Textos(297)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[87]='" & JSText(Textos(298)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[88]='" & JSText(Textos(299)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[89]='" & JSText(Textos(300)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[90]='" & JSText(Textos(301)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[91]='" & JSText(Textos(314)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[92]='" & JSText(Textos(315)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[93]='" & JSText(Textos(317)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[94]='" & JSText(Textos(318)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[95]='" & JSText(Textos(218)) & "';"
			'Otros
			sVariableJavascriptTextosJScript &= "TextosJScript[96]='" & JSText(Textos(8)) & "';" 'Añadir
			sVariableJavascriptTextosJScript &= "TextosJScript[97]='" & JSText(Textos(82)) & "';" 'Eliminar
			sVariableJavascriptTextosJScript &= "TextosJScript[98]='" & JSText(Textos(319)) & "';" 'Fecha de entrega
			sVariableJavascriptTextosJScript &= "TextosJScript[99]='" & JSText(Textos(11)) & "';" 'Cantidad
			sVariableJavascriptTextosJScript &= "TextosJScript[100]='" & JSText(Textos(12)) & "';" 'Importe
			sVariableJavascriptTextosJScript &= "TextosJScript[101]='" & JSText(Textos(322)) & "';" 'La fecha de entrega no puede ser anterior a la fecha de emisión del pedido
			sVariableJavascriptTextosJScript &= "TextosJScript[102]='" & JSText(Textos(323)) & "';" 'La suma de la cantidad de todas las entregas no puede superar la cantidad de la línea de pedido
			sVariableJavascriptTextosJScript &= "TextosJScript[103]='" & JSText(Textos(324)) & "';" 'La suma del importe de todas las entregas no puede superar el importe de la línea de pedido
			sVariableJavascriptTextosJScript &= "TextosJScript[104]='" & JSText(Textos(325)) & "';" 'Los planes de entrega de todas las líneas de pedido serán sustituidos por el que acaba de configurar. ¿Desea continuar?
			sVariableJavascriptTextosJScript &= "TextosJScript[105]='" & JSText(Textos(326)) & "';" 'El plan de entregas no se ha podido aplicar a las línea de pedido
			sVariableJavascriptTextosJScript &= "TextosJScript[106]='" & JSText(Textos(327)) & "';" 'El plan de entregas no puede contener fechas de entrega anteriores a la de emisión del pedido
			sVariableJavascriptTextosJScript &= "TextosJScript[107]='" & JSText(Textos(328)) & "';" 'La suma de las cantidades del plan de entregas no puede superar la cantidad total de la línea
			sVariableJavascriptTextosJScript &= "TextosJScript[108]='" & JSText(Textos(329)) & "';" 'La suma de las importes del plan de entregas no puede superar la importe total de la línea
			sVariableJavascriptTextosJScript &= "TextosJScript[109]='" & JSText(Textos(330)) & "';" 'Es necesario indicar fecha de entrega y cantidad en cada plan de entrega
			sVariableJavascriptTextosJScript &= "TextosJScript[110]='" & JSText(Textos(331)) & "';" 'Es necesario indicar fecha de entrega e importe en cada plan de entrega
			sVariableJavascriptTextosJScript &= "TextosJScript[111]='" & JSText(Textos(17)) & "';" 'Fecha entrega solicitada
			sVariableJavascriptTextosJScript &= "TextosJScript[112]='" & JSText(Textos(333)) & "';" 'Nivel mÃ­nimo:
			sVariableJavascriptTextosJScript &= "TextosJScript[113]='" & JSText(Textos(161)) & "';"
			'9624
			sVariableJavascriptTextosJScript &= "TextosJScript[114]='" & JSText(Textos(345)) & "';"
			sVariableJavascriptTextosJScript &= "TextosJScript[115]='" & JSText(Textos(346)) & "';"
			'Tarea Atrib Defecto
			sVariableJavascriptTextosJScript &= "TextosJScript[116]='" & JSText(Textos(348)) & "';"

			'Destinos TelÃ©fono, FAX y E-mail
			LblTfnoDest.Text = Textos(279)       'TelÃ©fono
			LblFAXDest.Text = Textos(280)       'Proveedor en ERP
			LblEmailDest.Text = Textos(281)       'Proveedor en ERP

			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosScript", sVariableJavascriptTextosJScript, True)
		End If
	End Sub
	''' <summary>
	''' Funcion que carga variables javascript
	''' </summary>
	''' <remarks></remarks>
	Private Sub CargarScripts()
		If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "NumberFormatUsu") Then
			With FSNUser
				Dim sVariablesJavascript As String = "var UsuNumberDecimalSeparator = '" & .NumberFormat.NumberDecimalSeparator & "';"
				sVariablesJavascript = sVariablesJavascript & "var UsuNumberGroupSeparator='" & .NumberFormat.NumberGroupSeparator & "';"
				sVariablesJavascript = sVariablesJavascript & "var UsuNumberNumDecimals='" & .NumberFormat.NumberDecimalDigits & "';"
				Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NumberFormatUsu", sVariablesJavascript, True)
			End With
		End If

	End Sub
	''' <summary>
	''' Vamos a obtener la información nacesaria para los dropdown desde el stored pero sin recuperar ni almacenar en cache el resto de información (órdenes, lineas e imputación)
	''' </summary>
	''' <param name="CodPersona">Código de la persona</param>
	''' <param name="Idioma">idioma en el que devolver el dataset</param>
	''' <remarks>Llamada desde InicializarControles. Máx. 0,5 seg.</remarks>
	Private Function getDropDownsInfo(ByVal CodPersona As String, ByVal Idioma As FSNLibrary.Idioma) As DataSet
		Dim oOrdenes As COrdenes = Me.FSNServer.Get_Object(GetType(FSNServer.COrdenes))
		Dim dsInfoDropDowns As New DataSet
		If HttpContext.Current.Cache("DsetSeguimientoDropDownsInfo_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
			dsInfoDropDowns = oOrdenes.ObtenerDatosCombosSeguimiento(Idioma.ToString(), CodPersona, Usuario.PermisoVerPedidosCCImputables)
			Me.InsertarEnCache("DsetSeguimientoDropDownsInfo_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString(), dsInfoDropDowns)
		Else
			dsInfoDropDowns = HttpContext.Current.Cache("DsetSeguimientoDropDownsInfo_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
		End If
		Return dsInfoDropDowns
	End Function
	''' <summary>
	''' Generamos el conjunto de datos de la página.
	''' He añadido el FiltrosAnyadidos a los parámetros para cambiar la firma y que no de errores aunque no era necesario
	''' dado que FiltrosAnyadidos es una propiedad
	''' </summary>
	''' <param name="CodPersona">Código de la persona</param>
	''' <param name="Idioma">idioma en el que devolver el dataset</param>
	''' True-->Centros coste
	''' False-->Usuario
	''' </param>
	''' <returns>dataset con los pedidos</returns>
	''' <remarks>Llamada desde GenerarDsetPedidos. Máx. depende del número de datos</remarks>
	Private Function CrearCestaDesdeFavorito() As Integer
		Dim idPedido As Integer = Request.QueryString("ID")
		Dim oOrdenes As COrdenFavoritos = FSNServer.Get_Object(GetType(FSNServer.COrdenFavoritos))

		Return oOrdenes.CrearCestaDesdeFavorito(idPedido)
	End Function
	''' <summary>
	''' Método mediante el cual cargamos en el control whdgEntregas los datos de entregas
	''' </summary>
	''' <remarks>Llamada desde Load() y Exportar. Máx 1 seg</remarks>
	Private Function CargarwhdgArticulos() As CLineasPedido
		Dim dsPedidos As New DataSet
		bTodosImporte = True
		Dim iIdPedido As Integer = 0
		If Me.DesdeFavorito Then
			Dim bModificarFavorito As Boolean = False
			If Not Request.QueryString("ModificarFavorito") Is Nothing Then
				bModificarFavorito = Request("ModificarFavorito")
			End If
			If Not bModificarFavorito Then
				iIdPedido = CrearCestaDesdeFavorito()
				Me.InsertarEnCache("IdDesdeFav_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString(), iIdPedido)
				Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sID", "<script>var sID = '" & iIdPedido.ToString() & "' </script>")
			Else
				iIdPedido = Request.QueryString("ID")
			End If
			dsPedidos = DsetPedidos(iIdPedido.ToString(), bModificarFavorito)
		Else
			iIdPedido = Request.QueryString("ID")
			dsPedidos = DsetPedidos(iIdPedido)
		End If

		'Oculto las columnas de cantidad, Precio Unitario y Unidad si todos los artículos son de recepción por importe
		If dsPedidos.Tables.Count > 0 Then bTodosImporte = Not dsPedidos.Tables(0).Rows.OfType(Of DataRow).Any(Function(x) x("TIPORECEPCION") = 0)

		Dim groupedColumns() As String = Split(Session("COLSGROUPBY"), "#")
		For Each item As String In groupedColumns
			If Not item = "" Then whdgArticulos.GroupingSettings.GroupedColumns.Add(Split(item, " ")(0),
				IIf(Split(item, " ")(1) = "DESC", Infragistics.Web.UI.GridControls.GroupingSortDirection.Descending, Infragistics.Web.UI.GridControls.GroupingSortDirection.Ascending))
		Next
		'Se tiene que hacer antes que el databind
		whdgArticulos.GridView.Behaviors.Paging.PageSize = ConfigurationManager.AppSettings("registrosPaginacion")

		'IMP: Si anteriormente hay una o mas filas seleccionadas da un error y no se carga la grid. Por eso hay que limpiar las filas seleccionadas.
		'whdgArticulos.GridView.Behaviors.Selection.SelectedRows.Clear()
		whdgArticulos.DataSource = dsPedidos
		'Los DataKeyFields deben definirse entre el Datasource y el Databind

		whdgArticulos.DataMember = "CESTA"
		whdgArticulos.DataKeyFields = "IDLINEA"
		'Añadir campos al webdatagrid
		'Necesitamos que se genere el DSetPedidos para usarlo dentro del CreatewhdgEntregasColumnsYConfiguracion
		'por lo que no llamamos al CreatewhdgEntregasColumnsYConfiguracion hasta tenerlo
		If dsPedidos.Tables.Count > 0 Then
			CreatewhdgEntregasColumnsYConfiguracion(dsPedidos.Tables(0).Rows(0).Item("MON"))
		Else
			CreatewhdgEntregasColumnsYConfiguracion(Usuario.Mon)
		End If
		whdgArticulos.DataBind()
	End Function
	''' <summary>
	''' Crea las columnas fijas del grid en función de los datos guardados en la configuración 
	''' y añade los campos al panel de configuración y añade las columnas al grid de configuracion que esta en ese mismo panel
	''' </summary>
	''' <remarks>Llamada desde CargarwhdgArticulos. Máx. 0,5 seg</remarks>
	Private Sub CreatewhdgEntregasColumnsYConfiguracion(ByVal sMoneda As String)

		Me.EditColumnYConfiguracion("NUM_LINEA", "") 'Nº Línea
		Me.EditColumnYConfiguracion("IDLINEA", "") 'Nº Línea
		Me.EditColumnYConfiguracion("ART_DEN", Textos(69)) 'Artículo
		Me.EditColumnYConfiguracion("PREC", Textos(198) & " (" & sMoneda & ")") 'Prec.Uni, Precio por unidad
		Me.EditColumnYConfiguracion("CANT", Textos(11)) 'Cantidad
		Me.EditColumnYConfiguracion("UNI", Textos(113)) 'Unidad
		Me.EditColumnYConfiguracion("IMPBRUTO", Textos(206) & " (" & sMoneda & ")")  'Imp. Bruto
		Me.EditColumnYConfiguracion("COSTES", Textos(207) & " (" & sMoneda & ")")  'Costes
		Me.EditColumnYConfiguracion("DESCUENTOS", Textos(208) & " (" & sMoneda & ")")  'Descuentos

		'Configurar Anchos y Visibilidad
		If Not IsPostBack Then
			confAnchosVisibilidadYPosicion()
		End If

	End Sub
	''' <summary>
	''' Función que añade una columna al control whdgEntregas, vinculada al origen de datos de éste.
	''' Añade el mismo campo al panel de configuración
	''' </summary>
	''' <param name="fieldName">Nombre del campo en el origen de datos</param>
	''' <param name="headerText">Título de la columna</param>
	''' <param name="headerTooltip">Tooltip de la columna</param>
	''' <remarks>Llamada desde CreatewhdgEntregasColumns. Máx. 0,1 seg.</remarks>
	Private Sub EditColumnYConfiguracion(ByVal fieldName As String, ByVal headerText As String, Optional ByVal headerTooltip As String = "", Optional ByVal MostrarEnPanelConfiguracion As Boolean = True)
		'Configurar el grid
		If Not whdgArticulos.GridView.Columns.Item("Key_" & fieldName) Is Nothing Then
			With whdgArticulos.GridView.Columns.Item("Key_" & fieldName)
				.Header.Text = headerText
				If headerTooltip = String.Empty Then headerTooltip = headerText
				.Header.Tooltip = headerTooltip
			End With
			If whdgArticulos.Columns.Item("Key_" & fieldName) IsNot Nothing Then
				whdgArticulos.Columns.Item("Key_" & fieldName).Header.Text = headerText
				If headerTooltip = String.Empty Then headerTooltip = headerText
				whdgArticulos.Columns.Item("Key_" & fieldName).Header.Tooltip = headerTooltip
			End If
		End If
	End Sub
	''' <summary>
	''' Configurar Anchos, Visibilidad y Posición de los campos del webhierarchicaldatagrid de pedidos y configuracion
	''' </summary>
	''' <remarks>Llamada desde CreatewhdgEntregasColumns. Máx. 0,1 seg.</remarks>
	Private Sub confAnchosVisibilidadYPosicion()
		'Obtenemos la relación entre los campos de la tabla de configuracion de anchos y visibilidad con los campos de la tabla de entregas
		Dim camposEntregasYConfiguracion As List(Of Campos) = relacionarEntregasyConfiguracion()
		'Coger los datos de ancho y visibilidad de la tabla desde base de datos

		For Each campo As Campos In camposEntregasYConfiguracion
			If whdgArticulos.GridView.Columns("Key_" & campo.CampoEntrega) IsNot Nothing Then
				whdgArticulos.GridView.Columns("Key_" & campo.CampoEntrega).Width = Unit.Percentage(campo.Width)
				whdgArticulos.GridView.Columns("Key_" & campo.CampoEntrega).Hidden = Not campo.Visible
				whdgArticulos.GridView.Columns("Key_" & campo.CampoEntrega).VisibleIndex = campo.Position

				whdgArticulos.Columns("Key_" & campo.CampoEntrega).Width = Unit.Percentage(campo.Width)
				whdgArticulos.Columns("Key_" & campo.CampoEntrega).Hidden = Not campo.Visible
				whdgArticulos.Columns("Key_" & campo.CampoEntrega).VisibleIndex = campo.Position
			End If
		Next

	End Sub
	''' <summary>
	''' Método que recupera el número de decimales para una unidad de pedido dada y configura la presentación de los datos en los controles webnumericEdit
	''' </summary>
	''' <param name="unidad">Código de la unidad de pedido</param>
	''' <param name="oFSNTextBox">Control a configurar</param>
	''' <param name="valorMinimo">Valor minimo del control</param>
	''' <param name="valorMaximo">Valor máximo del control</param>
	''' <remarks>Llamada desde: la página, allí donde se configure un control webNumericEdit. Tiempo máximo: inferior a 0,3</remarks>
	Private Sub configurarDecimales(ByVal unidad As Object, ByRef oFSNTextBox As FSNWebControls.FSNTextBox, ByRef valorMinimo As Nullable(Of Double), ByRef valorMaximo As Nullable(Of Double))
		Dim oUnidadPedido As FSNServer.CUnidadPedido = Nothing
		If Not DBNullToSomething(unidad) = Nothing Then
			unidad = Trim(unidad.ToString)
			oUnidadPedido = TodasLasUnidades.Item(unidad)
			oUnidadPedido.AsignarFormatoaUnidadPedido(Me.Usuario.NumberFormat)
			oFSNTextBox.Attributes.Add("UNIDAD", unidad)
		Else
			oFSNTextBox.Attributes.Add("UNIDAD", "")
		End If

		Dim vprecisionfmt As Integer
		Dim minimo As String = IIf(valorMinimo Is Nothing, "null", valorMinimo)
		Dim maximo As String = IIf(valorMaximo Is Nothing, "null", valorMaximo)
		Dim vdecimalfmt As String = Me.Usuario.NumberFormat.NumberDecimalSeparator
		Dim vthousanfmt As String = Me.Usuario.NumberFormat.NumberGroupSeparator

		Dim oFSNTextBox_DblValue As Label = oFSNTextBox.NamingContainer.FindControl(oFSNTextBox.ID & "_DblValue")
		Dim oFSNTextBoxArrows As FSNWebControls.FSNUpDownArrows = oFSNTextBox.NamingContainer.FindControl(oFSNTextBox.ID & "Arrows")

		If Not oUnidadPedido Is Nothing Then
			Dim unitFormatTemp As System.Globalization.NumberFormatInfo = oUnidadPedido.UnitFormat.Clone
			If oUnidadPedido.NumeroDeDecimales Is Nothing Then
				'Cuando el número de decimales de la unidad no está definido, teóricamente puede ser cualquiera pero
				'vamos a poner un límite de 99. Si fuese necesario cambiar esto, habrá que revisar su repercusión en jsTextBoxFormat.js
				'pues allí se controla el valor 99
				vprecisionfmt = 99
				oUnidadPedido.UnitFormat.NumberDecimalDigits = 0
				Dim num As Double = strToDbl(oFSNTextBox_DblValue.Text)
				If Not Double.IsNaN(num) AndAlso (num <> CInt(num)) Then
					num = num - CInt(num)
					Dim numDecimales As Integer = 0
					If num.ToString.IndexOf(".") > 0 Then
						numDecimales = num.ToString.Substring(num.ToString.IndexOf(".") + 1).Length
					ElseIf num.ToString.IndexOf(",") > 0 Then
						numDecimales = num.ToString.Substring(num.ToString.IndexOf(",") + 1).Length
					End If
					unitFormatTemp.NumberDecimalDigits = numDecimales
				Else
					unitFormatTemp.NumberDecimalDigits = 0
				End If
			ElseIf oUnidadPedido.NumeroDeDecimales > 0 Then
				vprecisionfmt = oUnidadPedido.NumeroDeDecimales
			ElseIf oUnidadPedido.NumeroDeDecimales = 0 Then
				vprecisionfmt = oUnidadPedido.NumeroDeDecimales
			End If
			ModuloIdioma = FSNLibrary.TiposDeDatos.ModulosIdiomas.EP_CatalogoWeb
			oFSNTextBox.Attributes.Add("avisoDecimales", Textos(158) & " " & oUnidadPedido.Codigo & ":")

			oFSNTextBox.Text = FormatNumber(strToDbl(oFSNTextBox_DblValue.Text), unitFormatTemp)

		Else 'Si no tenemos datos de la unidad, usamos los datos del usuario
			vprecisionfmt = Me.Usuario.NumberFormat.NumberDecimalDigits

			oFSNTextBox.Text = FormatNumber(strToDbl(oFSNTextBox_DblValue.Text), Me.Usuario.NumberFormat)
		End If

		'Todas las funciones javascript a las que se llama están en el fichero js/jsTextboxFormat.js
		oFSNTextBox.Attributes.Add("onpaste", "return validarpastenum2(" & oFSNTextBox.UniqueID &
																			",event" &
																			",'" & vdecimalfmt &
																			"','" & vthousanfmt &
																			"'," & vprecisionfmt & ");")
		oFSNTextBox.Attributes.Add("onkeypress", "return limiteDecimalesTextBox(" & oFSNTextBox.UniqueID &
																			",event" &
																			",'" & vdecimalfmt &
																			"','" & vthousanfmt &
																			"'," & vprecisionfmt & ", false);")
		ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
		Dim errMsg As String = Textos(41) & " " & Textos(42) & " """ & vdecimalfmt & """ " & Textos(43) & " """ & vthousanfmt & """."

		Dim errLimites As String = Textos(183) & " " &
									IIf(minimo <> "null", "\n" & Textos(181) & " ' + num2str(" & minimo & ",'" & vdecimalfmt & "','" & vthousanfmt & "'," & vprecisionfmt & ") + '", "") &
									IIf(maximo <> "null", "\n" & Textos(182) & " ' + num2str(" & maximo & ",'" & vdecimalfmt & "','" & vthousanfmt & "'," & vprecisionfmt & ") + '", "")

		ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_CatalogoWeb

		'Valores a pasar a las funciones javascript que no son necesarios en este caso:
		Dim sTipoControl As String = String.Empty
		Dim sLblTotLinID As String = String.Empty
		Dim sLblPrecPorProveID As String = String.Empty
		Dim sPrecioOCantidadID As String = String.Empty
		Dim sLblPrecArtID As String = String.Empty
		Dim sMoneda As String = String.Empty

		oFSNTextBox.Attributes.Add("onchange", "addThousandSeparator(" & oFSNTextBox.ClientID & ",'" &
																vdecimalfmt & "','" &
																vthousanfmt & "'," &
																vprecisionfmt & "," &
																"event); validarNumero(" &
																oFSNTextBox.ClientID & ",'" &
																vdecimalfmt & "','" &
																vthousanfmt & "'," &
																vprecisionfmt & "," &
																minimo & "," &
																maximo & ",'" &
																errMsg & "','" &
																errLimites & "','" &
																sTipoControl & "','" &
																sLblTotLinID & "','" &
																sLblPrecPorProveID & "','" &
																sPrecioOCantidadID & "','" &
																sLblPrecArtID & "','" &
																sMoneda & "'," &
																Me.Usuario.NumberFormat.NumberDecimalDigits & ");")

		oFSNTextBox.Attributes.Add("minimo", IIf(valorMinimo Is Nothing, "", valorMinimo))
		oFSNTextBox.Attributes.Add("maximo", IIf(valorMaximo Is Nothing, "", valorMaximo))

		Dim sOperation As String
		Dim oTd_Up As HtmlControls.HtmlGenericControl = CType(oFSNTextBoxArrows.FindControl(oFSNTextBoxArrows.ID & "_td_up"), HtmlControls.HtmlGenericControl)
		Dim oTd_Down As HtmlControls.HtmlGenericControl = CType(oFSNTextBoxArrows.FindControl(oFSNTextBoxArrows.ID & "_td_down"), HtmlControls.HtmlGenericControl)
		sOperation = "add"
		oTd_Up.Attributes.Add("onmousedown", "lowlight(this, event); " &
		   "beginAddOrSubstrackOne('" & oFSNTextBox.ClientID & "', " &
				  minimo & ", " &
				  maximo & ", '" &
				  vdecimalfmt & "', '" &
				  vthousanfmt & "', " &
				  vprecisionfmt & ", '" &
				  sOperation &
				  "', event,'" &
				  errMsg & "','" &
				  errLimites & "','" &
				  sTipoControl & "','" &
				  sLblTotLinID & "','" &
				  sLblPrecPorProveID & "','" &
				  sPrecioOCantidadID & "','" &
				  sLblPrecArtID & "','" &
				  sMoneda & "'," &
				  Me.Usuario.NumberFormat.NumberDecimalDigits & ");")
		sOperation = "subtract"
		oTd_Down.Attributes.Add("onmousedown", "lowlight(this, event); " &
		   "beginAddOrSubstrackOne('" & oFSNTextBox.ClientID & "', " &
				  minimo & ", " &
				  maximo & ", '" &
				  vdecimalfmt & "', '" &
				  vthousanfmt & "', " &
				  vprecisionfmt & ", '" &
				  sOperation &
				  "', event,'" &
				  errMsg & "','" &
				  errLimites & "','" &
				  sTipoControl & "','" &
				  sLblTotLinID & "','" &
				  sLblPrecPorProveID & "','" &
				  sPrecioOCantidadID & "','" &
				  sLblPrecArtID & "','" &
				  sMoneda & "'," &
				  Me.Usuario.NumberFormat.NumberDecimalDigits & ");")
		oTd_Up.Attributes.Add("onmouseover", "highlight(this);")
		oTd_Down.Attributes.Add("onmouseover", "highlight(this);")
		oTd_Up.Attributes.Add("onmouseout", "endAddOrSubstrackOne(); unhighlight(this);")
		oTd_Down.Attributes.Add("onmouseout", "endAddOrSubstrackOne(); unhighlight(this);")
		oTd_Up.Attributes.Add("onmouseup", "unlowlight(this); endAddOrSubstrackOne();")
		oTd_Down.Attributes.Add("onmouseup", "unlowlight(this); endAddOrSubstrackOne();")
	End Sub
	''' <summary>
	''' Hacemos todas las validaciones correspondientes a la dirección de envío de la factura.
	''' Comprobamos si es obligatorio o no (si debe mostrarse o no) y si hay datos
	''' </summary>
	''' <param name="Proveedor">Proveedor</param>
	''' <param name="direcEnvioFactura">Direcciones de envío de factua de la empresa deseada. Si es nothing significa que no hay empresa o se ha producido un error al cargar los datos de empresa. Si está vacío es que no hay direcciones para esa empresa</param>
	''' <remarks>Llamada desde dlCesta.ItemDataBound. Tiempo máx inferior a 0,2 seg</remarks>
	Private Overloads Sub CargarDireccionesEnvioFacturas(ByRef Proveedor As String, ByVal direcEnvioFactura As CEnvFacturaDirecciones)
		Dim bProveAutofactura As Boolean = ProveAutofacturacion(Proveedor)
		'Visibilidad de dirección facturas: 
		'Si es obligatorio rellenarla y al tiempo se cumple bien que no está activado el módulo de Facturación o bien que esté activado pero el proveedor no tenga activada la autofacturación
		Dim bMostrarDireccionEnvioFactura As Boolean = Me.Acceso.gbDirEnvFacObl AndAlso (Not Me.Acceso.g_bAccesoFSFA OrElse (Me.Acceso.g_bAccesoFSFA AndAlso Not bProveAutofactura))

		Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "bMostrarDireccionEnvioFactura", "<script>bMostrarDireccionEnvioFactura = " + bMostrarDireccionEnvioFactura.ToString().ToLower + " </script>")
		divDireccionEnvioFactura.Visible = bMostrarDireccionEnvioFactura
		divLblDireccionEnvioFactura.Visible = bMostrarDireccionEnvioFactura
		divComboEnvioFactura.Visible = bMostrarDireccionEnvioFactura
		imagenDireccion.Visible = bMostrarDireccionEnvioFactura
		If bMostrarDireccionEnvioFactura Then
			Dim ddlDirecciones As DropDownList = CType(cbDireccionEnvioFactura, DropDownList)
			Dim lblDirec As Label = CType(LblDireccion, Label)
			Dim lblDenDirec As Label = CType(lblDireccionEnvioFactura, Label)
			Dim lblCodDirec As Label = CType(lblCodDireccion, Label)
			Dim imaInfo As Image = CType(im, Image)

			lblDirec.Text = "(*) " & Textos(165) & ": "
			If direcEnvioFactura IsNot Nothing AndAlso direcEnvioFactura.Any Then
				ddlDirecciones.DataSource = direcEnvioFactura
				ddlDirecciones.DataBind()
				Select Case direcEnvioFactura.Count
					Case 1
						divDireccionEnvioFactura.Style.Add("display", "block")
						divComboEnvioFactura.Style.Add("display", "none")
						ddlDirecciones.Items(0).Selected = True
						lblDenDirec.Text = ddlDirecciones.SelectedItem.Text
						lblCodDirec.Text = ddlDirecciones.SelectedItem.Value
					Case Else
						ddlDirecciones.Items.Insert(0, New ListItem(String.Empty, String.Empty))
						divComboEnvioFactura.Style.Add("display", "block")
						divDireccionEnvioFactura.Style.Add("display", "none")
						ddlDirecciones.Visible = True
						lblCodDirec.Text = "0"
						ddlDirecciones.Items(0).Selected = True
						lblDenDirec.Visible = False
				End Select
				imaInfo.Visible = True
			Else
				ddlDirecciones.Items.Clear()
				ddlDirecciones.Visible = False
				lblDenDirec.Visible = False
				divDireccionEnvioFactura.Visible = False
				divLblDireccionEnvioFactura.Visible = False
				lblCodDirec.Text = "0"
				imaInfo.Visible = False
			End If
		End If

	End Sub
	''' <summary>
	''' Hacemos todas las validaciones correspondientes a la dirección de envío de la factura.
	''' Comprobamos si es obligatorio o no (si debe mostrarse o no) y si hay datos
	''' </summary>
	''' <param name="sProveedor">sProveedor</param>
	''' <param name="IdEmpresa">Id de la empresa de la que se quieren mostrar las direcciones. Si es cero es que no hay empresa</param>
	''' <remarks>Llamada desde dlCesta.ItemDataBound. Tiempo máx inferior a 0,2 seg</remarks>
	Private Overloads Sub CargarDireccionesEnvioFacturas(ByRef sProveedor As String, ByVal IdEmpresa As Integer)
		If IdEmpresa > 0 Then
			Dim _DireccionesEnvioFactura As CEnvFacturaDirecciones = FSNServer.Get_Object(GetType(CEnvFacturaDirecciones))
			_DireccionesEnvioFactura = Me.Empresas.Item(IdEmpresa.ToString).DireccionesEnvioFactura
			CargarDireccionesEnvioFacturas(sProveedor, _DireccionesEnvioFactura)
		Else
			Dim _DireccionesEnvioFactura As CEnvFacturaDirecciones = Nothing
			CargarDireccionesEnvioFacturas(sProveedor, _DireccionesEnvioFactura)
		End If
	End Sub
	''' <summary>
	''' Muestra el panel de de detalle del articulo cuando pinchamos en la columna de codigo de articulo del grid
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub btnMostrarDetalleArticulo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMostrarDetalleArticulo.Click
		pnlDetalleArticulo.CargarDetalleArticuloEP(DsetArticulos, hid_lineaPedido.Value, hid_cantidad.Value, hid_Precio.Value, False, hid_TipoRecepcion.Value)
	End Sub
	''' <summary>
	''' Se vuelve a catalogoweb.aspx
	''' </summary>
	''' <param name="sender">El objeto que lanza el evento(btnAceptarPedido)</param>
	''' <param name="e">Los argumentos del evento</param>
	''' <remarks>
	''' Llamada desde: AutomÃ¡tica, cuando se pincha el botÃ³n
	''' Tiempo mÃ¡ximo: 0 seg</remarks>
	Protected Sub btnAceptarPedido_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarPedido.Click
		HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaEP") & "CatalogoWeb.aspx")
	End Sub
	''' <summary>
	''' Para un string de partidas separado por # pasado como parámetro, determina el gestor que tiene asociado
	''' </summary>
	''' <param name="Partida">String de partidas separadas por #</param>
	''' <returns>String con el código del gestor</returns>
	''' <remarks>Llamada desde EmisionPedido.aspx.vb. Max. 0,1 seg.</remarks>
	Private Function seleccionaGestor(ByVal Partida As String) As String
		If Partida <> String.Empty Then
			Dim sPRES() As String = Partida.Split("#")
			Dim sPRES0 As String = sPRES(0)
			Dim sGestor As String = String.Empty
			If GestoresPartidasUsu(sPRES0).Count <> 0 Then
				sGestor = GestoresPartidasUsu(sPRES0).Find(Function(oParPres As FSNServer.PartidaPRES5) oParPres.NIV0 = sPRES0).GestorCod + " - " + GestoresPartidasUsu(sPRES0).Find(Function(oParPres As FSNServer.PartidaPRES5) oParPres.NIV0 = sPRES0).GestorDen
			End If
			If sGestor = " - " Or sGestor = String.Empty Then
				Return String.Empty
			End If
			Return sGestor
		Else
			Return String.Empty
		End If

	End Function
#Region "Eventos grid"
	''' <summary>
	''' Método que se lanza en el evento InitializeRow (cuando se enlazan los datos del origen de datos con la línea)
	''' En él vamos a configurar la columna de entrega con retrasos (mostrar la imagen de retraso en las entregas retrasadas)
	''' </summary>
	''' <param name="sender">Control que lanza el evento (whdgEntregas)</param>
	''' <param name="e">Argumentos del evento</param>
	''' <remarks>Llamada desde el evento. Máx 0,1 seg</remarks>
	Private Sub whdgArticulos_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgArticulos.InitializeRow
		Dim oData As DataRow = e.Row.DataItem.Item.Row
		Dim dCant As Double

		If sender.Columns.FromKey("Key_IDLINEA") IsNot Nothing Then
			Dim sCantidad As String
			Dim oUnidadPedido As FSNServer.CUnidadPedido = Nothing
			If Not DBNullToStr(oData("UP")) = Nothing Then
				oUnidadPedido = TodasLasUnidades.Item(Trim(DBNullToStr(oData("UP"))))
				oUnidadPedido.AsignarFormatoaUnidadPedido(Me.Usuario.NumberFormat)
			End If

			Dim Index As Integer = sender.Columns.FromKey("Key_IDLINEA").Index()

			If (DBNullToStr(oData("TIPORECEPCION").ToString()) = 0) Then
				Dim vdecimalfmt As String = Me.Usuario.NumberFormat.NumberDecimalSeparator
				Dim vthousanfmt As String = Me.Usuario.NumberFormat.NumberGroupSeparator

				Dim vprecisionfmt As Integer
				Dim unitFormatTemp As System.Globalization.NumberFormatInfo = oUnidadPedido.UnitFormat.Clone
				Try
					dCant = Double.Parse(DBNullToStr(oData("CANT").ToString()))
				Catch ex As FormatException
					Exit Sub
				Catch ex As Exception
					Exit Sub
				End Try
				If Not oUnidadPedido Is Nothing Then
					If oUnidadPedido.NumeroDeDecimales Is Nothing Then
						'Cuando el número de decimales de la unidad no está definido, teóricamente puede ser cualquiera pero
						'vamos a poner un límite de 99. Si fuese necesario cambiar esto, habrá que revisar su repercusión en jsTextBoxFormat.js
						'pues allí se controla el valor 99
						vprecisionfmt = 99
						oUnidadPedido.UnitFormat.NumberDecimalDigits = 0
						If Not Double.IsNaN(dCant) AndAlso (dCant <> Fix(dCant)) Then
							'dCant = dCant - CInt(dCant)
							Dim numDecimales As Integer = 0
							If dCant.ToString.IndexOf(".") > 0 Then
								numDecimales = dCant.ToString.Substring(dCant.ToString.IndexOf(".") + 1).Length
							ElseIf dCant.ToString.IndexOf(",") > 0 Then
								numDecimales = dCant.ToString.Substring(dCant.ToString.IndexOf(",") + 1).Length
							End If
							unitFormatTemp.NumberDecimalDigits = numDecimales
						Else
							unitFormatTemp.NumberDecimalDigits = 0
						End If
					ElseIf oUnidadPedido.NumeroDeDecimales > 0 Then
						vprecisionfmt = oUnidadPedido.NumeroDeDecimales
					ElseIf oUnidadPedido.NumeroDeDecimales = 0 Then
						vprecisionfmt = oUnidadPedido.NumeroDeDecimales
					End If
					ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos

					sCantidad = FormatNumber(dCant, unitFormatTemp)
				Else 'Si no tenemos datos de la unidad, usamos los datos del usuario
					vprecisionfmt = Me.Usuario.NumberFormat.NumberDecimalDigits

					sCantidad = FormatNumber(dCant, Me.Usuario.NumberFormat)
				End If
				dCant = Double.Parse(sCantidad)

				e.Row.Items.FindItemByKey("Key_NUM_LINEA").Text = IIf(DBNullToInteger(oData("NUM_LINEA")) = 1000, "", "<label id='num" & DBNullToStr(oData("IDLINEA")) & "' >" & Format(DBNullToInteger(oData("NUM_LINEA")), "000").ToString() & "</label>")

				e.Row.Items.FindItemByKey("Key_CANT").Text = "<input name='Cant" & DBNullToStr(oData("IDLINEA")) & "'  style='width: 95%;'  id='Cant" & DBNullToStr(oData("IDLINEA")) & "' size='8' value='" & sCantidad & "' " _
						& " onBlur='if(validaFloatCantidadNo0(this,null," & DBNullToStr(oData("IDLINEA")) & ")){cambiarCantidad(" & DBNullToStr(oData("IDLINEA")) & ", event)}'  />  "

				Dim oUnis As CUnidadesPedido = FSNServer.Get_Object(GetType(FSNServer.CUnidadesPedido))
				Dim dsUnidades As DataSet
				If (DBNullToStr(oData("COD_ITEM")) <> String.Empty) Then
					dsUnidades = oUnis.DevolverUnidadesPedido(FSNUser.Idioma, DBNullToInteger(oData("LINEA")))
				Else
					dsUnidades = oUnis.CargarTodasLasUnidades(FSNUser.Idioma)
				End If
				If (DBNullToStr(oData("COD_ITEM")) <> String.Empty) Then
					If dsUnidades.Tables(0).Rows.Count > 1 Then
						e.Row.Items.FindItemByKey("Key_UNI").Text = "<label id='Uni" & DBNullToStr(oData("IDLINEA")) & "' >" & oUnidadPedido.Codigo & "</label>" _
						& "<img ID='imLnkBtnCambiarUnidad' style='border: 0px currentColor; border-image: none;cursor: pointer;' alt='" & DBNullToStr(oData("IDLINEA")) & "' src='../../App_Themes/" & Page.Theme & "/images/rellenar_12x12.gif'>"
					Else
						e.Row.Items.FindItemByKey("Key_UNI").Text = "<label id='Uni" & DBNullToStr(oData("IDLINEA")) & "' >" & oUnidadPedido.Codigo & "</label>"
					End If
				Else
					e.Row.Items.FindItemByKey("Key_UNI").Text = "<label id='Uni" & DBNullToStr(oData("IDLINEA")) & "' >" & oUnidadPedido.Codigo & "</label>"
				End If

				Dim sPrec As String
				Dim dPrecLimpio As Double
				Dim sPrecFormat As String
				Dim dPrec As Double
				Dim dFC As Double
				Try
					dPrec = Double.Parse(DBNullToStr(oData("PREC").ToString()))
					dFC = Double.Parse(DBNullToStr(oData("FC").ToString()))
				Catch ex As FormatException
					Exit Sub
				Catch ex As Exception
					Exit Sub
				End Try
				dPrecLimpio = dPrec
				e.Row.Items.FindItemByKey("Key_PREC").Text = ""

				If dFC <> 0 Then
					dPrec = dPrec * dFC
				End If
				sPrec = dPrec.ToString()
				sPrecFormat = FSNLibrary.FormatNumber(dPrec, Me.Usuario.NumberFormat)
				dPrec = Double.Parse(sPrecFormat)

				If FSNUser.ModificarPrecios Then
					If (CType(oData("MODIF_PREC"), Boolean)) Then
						e.Row.Items.FindItemByKey("Key_PREC").Text = "<input name='Prec" & DBNullToStr(oData("IDLINEA")) & "' style='width: 95%;' id='Prec" & DBNullToStr(oData("IDLINEA")) & "' value='" & sPrecFormat & "' onBlur='if(validaFloat(this,null)){cambiarPrecio(" & DBNullToStr(oData("IDLINEA")) & ", event)}' />" _
							& "<label id='PrecT" & DBNullToStr(oData("IDLINEA")) & "' style='display:none;'>" & sPrecFormat & "</label> <label id='Prec" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;'>" & dPrec.ToString().Replace(",", ".") & "</label>"
					Else
						e.Row.Items.FindItemByKey("Key_PREC").Text = "<label id='PrecT" & DBNullToStr(oData("IDLINEA")) & "' >" & sPrecFormat & "</label><input name='Prec" & DBNullToStr(oData("IDLINEA")) & "'  id='Prec" & DBNullToStr(oData("IDLINEA")) & "' value='" & sPrecFormat & "' readonly style='visibility:hidden;' /> " _
							& " <label id='Prec" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;'>" & dPrec.ToString().Replace(",", ".") & "</label>"
					End If
				Else
					e.Row.Items.FindItemByKey("Key_PREC").Text = "<label id='PrecT" & DBNullToStr(oData("IDLINEA")) & "' >" & sPrecFormat & "</label><input name='Prec" & DBNullToStr(oData("IDLINEA")) & "' id='Prec" & DBNullToStr(oData("IDLINEA")) & "' value='" & sPrecFormat & "' readonly style='visibility:hidden;' /> " _
						& " <label id='Prec" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;'>" & dPrec.ToString().Replace(",", ".") & "</label>"
				End If

				Dim dPrecActual As Double
				If (lblImpBruto_hdd.Text <> String.Empty) Then
					dPrecActual = Double.Parse(lblImpBruto_hdd.Text.ToString(), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.NumberFormatInfo.InvariantInfo)
				Else
					dPrecActual = 0
				End If
				Dim dPrecioLinea As Double
				If dFC <> 0 Then
					dPrecioLinea = dPrecLimpio * dCant * dFC
				Else
					dPrecioLinea = dPrecLimpio * dCant
				End If

				e.Row.Items.FindItemByKey("Key_IMPBRUTO").Text = "<label id='lblImpBrutolin_" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;' >" & dPrecioLinea.ToString().Replace(",", ".") & "</label>" _
					& "<label id='lblImpBrutolin_" & DBNullToStr(oData("IDLINEA")) & "'>" & FSNLibrary.FormatNumber(dPrecioLinea, Me.Usuario.NumberFormat) & "</label>"

				dPrecActual = dPrecActual + dPrecioLinea
				lblImpBruto_hdd.Text = dPrecActual.ToString().Replace(",", ".")
				lblImpBrutoLinea_hdd.Text = dPrecActual.ToString().Replace(",", ".")

				lblImpBruto.Text = FSNLibrary.FormatNumber(dPrecActual, Me.Usuario.NumberFormat) & " " & DBNullToStr(oData("MON"))
				Dim imagenInfo As String

				If Not DesdeFavorito AndAlso CType(oData("LINEA_PED_ABIERTO"), Integer) = 0 Then
					imagenInfo = "<img id='descArticulo" & DBNullToStr(oData("IDLINEA")) & "' src='" & ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/info.gif' style='cursor:pointer;' onclick='AbrirDetalleArticulo(" & DBNullToStr(oData("LINEA")) & ",  """ & dCant.ToString() & """, """ & dPrec.ToString() & """, " & DBNullToStr(oData("TIPORECEPCION").ToString()) & "," & DBNullToStr(oData("IDLINEA")) & ");return false;' /> "
				Else
					imagenInfo = ""
				End If

				If FSNUser.MostrarCodArt Then
					If FSNUser.ModificarPrecios AndAlso DBNullToInteger(oData("GENERICO")) = 1 Then
						If DBNullToStr(oData("COD_ITEM")) <> String.Empty Then
							e.Row.Items.FindItemByKey("Key_ART_DEN").Text = imagenInfo _
						& "<label id='COD_ITEM_" & DBNullToStr(oData("IDLINEA")) & "'>" & DBNullToStr(oData("COD_ITEM")) & "</label>" _
								& " - <input id='inpArtDen_" & DBNullToStr(oData("IDLINEA")) & "' style='width: 80%;' value='" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "' onBlur='cambiarDescripcion(" & DBNullToStr(oData("IDLINEA")) & ");'/>"
						Else
							e.Row.Items.FindItemByKey("Key_ART_DEN").Text = "<label id='COD_ITEM_" & DBNullToStr(oData("IDLINEA")) & "' >" & DBNullToStr(oData("COD_ITEM")) & "</label>" _
								& " <input id='inpArtDen_" & DBNullToStr(oData("IDLINEA")) & "' style='width: 80%;'  value='" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "' onBlur='cambiarDescripcion(" & DBNullToStr(oData("IDLINEA")) & ");'/>"
						End If

					Else
						If DBNullToStr(oData("COD_ITEM")) <> String.Empty Then
							e.Row.Items.FindItemByKey("Key_ART_DEN").Text = imagenInfo _
						& "<label id='COD_ITEM_" & DBNullToStr(oData("IDLINEA")) & "' >" & DBNullToStr(oData("COD_ITEM")) & " - " & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "</label>"
						Else
							e.Row.Items.FindItemByKey("Key_ART_DEN").Text = "<label id='COD_ITEM_" & DBNullToStr(oData("IDLINEA")) & "'  >" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "</label>"
						End If

					End If
					e.Row.Items.FindItemByKey("Key_ART_DEN").Tooltip = IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "")
					'problema de favoritos
					If Me.DesdeFavorito Then
						If FSNUser.ModificarPrecios AndAlso DBNullToInteger(oData("GENERICO")) = 1 Then
							'tengo que saber que es generico y con oLinea.Generico no me vale Ahora si me valo porque cargo el generico en EmisionPedido.aspx.vb
							e.Row.Items.FindItemByKey("Key_ART_DEN").Text = imagenInfo _
						& "<label id='COD_ITEM_" & DBNullToStr(oData("IDLINEA")) & "' >" & DBNullToStr(oData("COD_ITEM")) & "</label>" _
							& " - <input id='inpArtDen_" & DBNullToStr(oData("IDLINEA")) & "' style='width: 80%;'  value='" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "' onBlur='cambiarDescripcion(" & DBNullToStr(oData("IDLINEA")) & ");'/>"
						End If
					End If
					'fin problema de favoritos
				Else
					If FSNUser.ModificarPrecios AndAlso DBNullToInteger(oData("GENERICO")) = 1 Then
						If DBNullToStr(oData("COD_ITEM")) <> String.Empty Then
							e.Row.Items.FindItemByKey("Key_ART_DEN").Text = imagenInfo _
						& "<label id='COD_ITEM_" & DBNullToStr(oData("IDLINEA")) & "' >" & DBNullToStr(oData("COD_ITEM")) & " </label> <input id='inpArtDen_" & DBNullToStr(oData("IDLINEA")) & "' style='width: 80%;'  value='" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "' onBlur='cambiarDescripcion(" & DBNullToStr(oData("IDLINEA")) & ");'/>"
						Else
							e.Row.Items.FindItemByKey("Key_ART_DEN").Text = imagenInfo _
						& " <input id='inpArtDen_" & DBNullToStr(oData("IDLINEA")) & "' style='width: 80%;'  value='" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "' onBlur='cambiarDescripcion(" & DBNullToStr(oData("IDLINEA")) & ");'/>"
						End If

					Else
						If DBNullToStr(oData("COD_ITEM")) <> String.Empty Then
							e.Row.Items.FindItemByKey("Key_ART_DEN").Text = imagenInfo _
						& "<label id='COD_ITEM_" & DBNullToStr(oData("IDLINEA")) & "' >" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "</label>"
						Else
							e.Row.Items.FindItemByKey("Key_ART_DEN").Text = imagenInfo _
						& "<label id='COD_ITEM_" & DBNullToStr(oData("IDLINEA")) & "' >" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "</label>"
						End If
					End If
				End If
			Else
				e.Row.Items.FindItemByKey("Key_NUM_LINEA").Text = IIf(DBNullToInteger(oData("NUM_LINEA")) = 1000, "", "<label id='" & DBNullToStr(oData("IDLINEA")) & "' >" & Format(DBNullToInteger(oData("NUM_LINEA")), "000").ToString() & "</label>")
				Dim dImportePedido As Double
				Try
					dImportePedido = Double.Parse(DBNullToStr(oData("IMPORTE_PED").ToString()))
				Catch ex As FormatException
					Exit Sub
				Catch ex As Exception
					Exit Sub
				End Try

				Dim dPrecActual As Double
				If (lblImpBruto_hdd.Text <> String.Empty) Then
					dPrecActual = Double.Parse(lblImpBruto_hdd.Text.ToString(), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.NumberFormatInfo.InvariantInfo)
				Else
					dPrecActual = 0
				End If
				dPrecActual = dPrecActual + dImportePedido

				Dim sImportePedido As String
				sImportePedido = FormatNumber(dImportePedido, Me.Usuario.NumberFormat)

				e.Row.Items.FindItemByKey("Key_PREC").Text = ""
				e.Row.Items.FindItemByKey("Key_CANT").Text = "<input name='Cant" & DBNullToStr(oData("IDLINEA")) & "'   id='Cant" & DBNullToStr(oData("IDLINEA")) & "' style='width: 95%;display:none;' size='8' value='1' />  "

				lblImpBruto_hdd.Text = dPrecActual.ToString().Replace(",", ".")
				lblImpBrutoLinea_hdd.Text = dPrecActual.ToString().Replace(",", ".")

				e.Row.Items.FindItemByKey("Key_IMPBRUTO").Text = "<input name='lblImpBrutolin_" & DBNullToStr(oData("IDLINEA")) & "' style='width: 95%;'  id='lblImpBrutolin_" & DBNullToStr(oData("IDLINEA")) & "' value='" & sImportePedido & "' onBlur='if(validaFloat(this,null)){cambiarImporteBruto(" & DBNullToStr(oData("IDLINEA")) & ", event)}'  />" _
							& "<label id='lblImpBrutolin_" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;'>" & dImportePedido.ToString().Replace(",", ".") & "</label>"

				e.Row.Items.FindItemByKey("Key_UNI").Text = "<label id='Uni" & DBNullToStr(oData("IDLINEA")) & "' style='display:none;' >" & oUnidadPedido.Codigo & "</label>"

				Dim imagenInfo As String
				If Not DesdeFavorito AndAlso CType(oData("LINEA_PED_ABIERTO"), Integer) = 0 Then
					imagenInfo = "<img id='descArticulo" & DBNullToStr(oData("IDLINEA")) & "' src='" & ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/info.gif' style='cursor:pointer;' onclick='AbrirDetalleArticulo(" & DBNullToStr(oData("LINEA")) & ",  """ & sImportePedido & """, """ & sImportePedido & """, " & DBNullToStr(oData("TIPORECEPCION").ToString()) & "," & DBNullToStr(oData("IDLINEA")) & ");return false;' /> "
				Else
					imagenInfo = ""
				End If

				e.Row.Items.FindItemByKey("Key_ART_DEN").Text = imagenInfo _
						& " <label id='COD_ITEM_" & DBNullToStr(oData("IDLINEA")) & "' >" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "</label>"

				If FSNUser.MostrarCodArt Then
					If FSNUser.ModificarPrecios AndAlso DBNullToInteger(oData("GENERICO")) = 1 Then
						If DBNullToInteger(oData("LINEA")) > 0 Then
							e.Row.Items.FindItemByKey("Key_ART_DEN").Text = imagenInfo _
						& "<label id='COD_ITEM_" & DBNullToStr(oData("IDLINEA")) & "' >" & DBNullToStr(oData("COD_ITEM")) & "</label>" _
								& " - <input id='inpArtDen_" & DBNullToStr(oData("IDLINEA")) & "' style='width: 80%;' value='" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "' onBlur='cambiarDescripcion(" & DBNullToStr(oData("IDLINEA")) & ");'/>"
						Else
							e.Row.Items.FindItemByKey("Key_ART_DEN").Text = imagenInfo _
						& "<label id='COD_ITEM_" & DBNullToStr(oData("IDLINEA")) & "'  >" & DBNullToStr(oData("COD_ITEM")) & "</label>" _
								& " <input id='inpArtDen_" & DBNullToStr(oData("IDLINEA")) & "' style='width: 80%;' value='" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "' onBlur='cambiarDescripcion(" & DBNullToStr(oData("IDLINEA")) & ");'/>"
						End If

					Else
						If DBNullToInteger(oData("LINEA")) > 0 AndAlso DBNullToStr(oData("COD_ITEM")) <> String.Empty Then
							e.Row.Items.FindItemByKey("Key_ART_DEN").Text = imagenInfo _
						& "<label id='COD_ITEM_" & DBNullToStr(oData("IDLINEA")) & "' >" & DBNullToStr(oData("COD_ITEM")) & " - " & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "</label>"
						Else
							e.Row.Items.FindItemByKey("Key_ART_DEN").Text = imagenInfo _
						& "<label id='COD_ITEM_" & DBNullToStr(oData("IDLINEA")) & "'  >" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "</label>"
						End If

					End If
					'problema de favoritos
					If Me.DesdeFavorito Then
						If FSNUser.ModificarPrecios AndAlso DBNullToInteger(oData("GENERICO")) = 1 Then
							'tengo que saber que es generico y con oLinea.Generico no me vale Ahora si me valo porque cargo el generico en EmisionPedido.aspx.vb
							e.Row.Items.FindItemByKey("Key_ART_DEN").Text = imagenInfo _
						& "<label id='COD_ITEM_" & DBNullToStr(oData("IDLINEA")) & "' >" & DBNullToStr(oData("COD_ITEM")) & "</label>" _
							& " - <input id='inpArtDen_" & DBNullToStr(oData("IDLINEA")) & "'  style='width: 80%;' value='" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "' onBlur='cambiarDescripcion(" & DBNullToStr(oData("IDLINEA")) & ");'/>"
						End If
					End If
					'fin problema de favoritos
				Else
					If FSNUser.ModificarPrecios AndAlso DBNullToInteger(oData("GENERICO")) = 1 Then
						If DBNullToInteger(oData("LINEA")) > 0 AndAlso DBNullToStr(oData("COD_ITEM")) <> String.Empty Then
							e.Row.Items.FindItemByKey("Key_ART_DEN").Text = imagenInfo _
						& " - <input id='inpArtDen_" & DBNullToStr(oData("IDLINEA")) & "'  style='width: 80%;' value='" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "' onBlur='cambiarDescripcion(" & DBNullToStr(oData("IDLINEA")) & ");'/>"
						Else
							e.Row.Items.FindItemByKey("Key_ART_DEN").Text = imagenInfo _
						& " <input id='inpArtDen_" & DBNullToStr(oData("IDLINEA")) & "'  style='width: 80%;' value='" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "' onBlur='cambiarDescripcion(" & DBNullToStr(oData("IDLINEA")) & ");'/>"
						End If

					Else
						If DBNullToInteger(oData("LINEA")) > 0 Then
							e.Row.Items.FindItemByKey("Key_ART_DEN").Text = imagenInfo _
						& "<label id='COD_ITEM_" & DBNullToStr(oData("IDLINEA")) & "' >" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "</label>"
						Else
							e.Row.Items.FindItemByKey("Key_ART_DEN").Text = imagenInfo _
						& "<label id='COD_ITEM_" & DBNullToStr(oData("IDLINEA")) & "'  >" & IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "") & "</label>"
						End If

					End If
				End If
				e.Row.Items.FindItemByKey("Key_ART_DEN").Tooltip = IIf(DBNullToStr(oData("ART_DEN")) <> "", DBNullToStr(oData("ART_DEN")), "")
			End If
			If Not IsDBNull(oData("LINEA")) Then
				e.Row.Items.FindItemByKey("Key_COSTES").Text = "<label id='lblCosteTotalLinea_" & DBNullToStr(oData("IDLINEA")) & "' onclick='mostrarLineaCostes(" & DBNullToStr(oData("LINEA")) & ", " & DBNullToStr(oData("IDLINEA")) & ");' style='text-decoration:underline;'></label>" _
				& "<label id='lblCosteTotalLinea_" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;' ></label>"
				e.Row.Items.FindItemByKey("Key_DESCUENTOS").Text = "<label id='lblDescuentoTotalLinea_" & DBNullToStr(oData("IDLINEA")) & "' onclick='mostrarLineaDescuentos(" & DBNullToStr(oData("LINEA")) & ", " & DBNullToStr(oData("IDLINEA")) & ");' style='text-decoration:underline;'></label>" _
				& "<label id='lblDescuentoTotalLinea_" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;' ></label><label id='FC" & DBNullToStr(oData("IDLINEA")) & "' style='display:none;' > " & DBNullToStr(oData("FC")) & "</label>"
			Else
				e.Row.Items.FindItemByKey("Key_COSTES").Text = "<label id='lblCosteTotalLinea_" & DBNullToStr(oData("IDLINEA")) & "></label>" _
				& "<label id='lblCosteTotalLinea_" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;' ></label>"
				e.Row.Items.FindItemByKey("Key_DESCUENTOS").Text = "<label id='lblDescuentoTotalLinea_" & DBNullToStr(oData("IDLINEA")) & "></label>" _
				& "<label id='lblDescuentoTotalLinea_" & DBNullToStr(oData("IDLINEA")) & "_hdd' style='display:none;' ></label><label id='FC" & DBNullToStr(oData("IDLINEA")) & "' style='display:none;' > " & DBNullToStr(oData("FC")) & "</label>"
			End If

			e.Row.Items.FindItemByKey("Key_DELETE").Text = "<img id='borrar" & DBNullToStr(oData("IDLINEA")) & "' src='" & ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/rechaz_disable.gif' style='cursor:pointer;' onclick='DeleteRow(" & DBNullToStr(oData("IDLINEA")) & ");' />" '
			e.Row.Items.FindItemByKey("Key_AVISO").Text = "<img id='aviso" & DBNullToStr(oData("IDLINEA")) & "' height='20px' style='display:none;' style='cursor:pointer;'  src='" & ConfigurationManager.AppSettings("RUTA") & "Images/alert-large.gif'  />" '

			'Botones Subir/Bajar
			'En la primera fila, el botón ascendente estará deshabilitado
			If e.Row.Index = 0 Then
				e.Row.Items.FindItemByKey("Key_SUBIRORDEN").Text = "<img id='subir" & DBNullToStr(oData("IDLINEA")) & "' src='" & ConfigurationManager.AppSettings("RUTA") & "Images/subir_desactivado.gif' style='cursor:pointer;'/>" '
			Else
				e.Row.Items.FindItemByKey("Key_SUBIRORDEN").Text = "<img id='subir" & DBNullToStr(oData("IDLINEA")) & "' src='" & ConfigurationManager.AppSettings("RUTA") & "Images/ascendente.gif' style='cursor:pointer;'/>" '
			End If

			'En la última fila, el botón descendente estará deshabilitado
			If e.Row.Index = (whdgArticulos.Rows.Count - 1) Then
				e.Row.Items.FindItemByKey("Key_BAJARORDEN").Text = "<img id='bajar" & DBNullToStr(oData("IDLINEA")) & "' src='" & ConfigurationManager.AppSettings("RUTA") & "Images/bajar_desactivado.gif' style='cursor:pointer;'/>" '
			Else
				e.Row.Items.FindItemByKey("Key_BAJARORDEN").Text = "<img id='bajar" & DBNullToStr(oData("IDLINEA")) & "' src='" & ConfigurationManager.AppSettings("RUTA") & "Images/descendente.gif' style='cursor:pointer;'/>" '
			End If

			Dim ddl As DropDownList
			ddl = CType(CmbBoxEmpresa, DropDownList)
			Dim sProveedor As String = DBNullToStr(oData("PROVE"))
			If Me.Empresas IsNot Nothing AndAlso Me.Empresas.Any Then
				Select Case Me.Empresas.Count
					Case -1, 0
						CargarDireccionesEnvioFacturas(sProveedor, False)
					Case 1
						CargarDireccionesEnvioFacturas(sProveedor, Me.Empresas(0).DireccionesEnvioFactura)
					Case Else
						CargarDireccionesEnvioFacturas(sProveedor, Me.Empresas.Item(ddl.SelectedValue).DireccionesEnvioFactura)
				End Select
			Else
				ddl.Style.Add("visibility", "hidden")
				CargarDireccionesEnvioFacturas(sProveedor, False)
			End If
		End If
	End Sub
	''' <summary>
	''' Tras la carga del grid se "sincroniza" con el Custom Pager
	''' </summary>
	''' <param name="sender">control</param>
	''' <param name="e">evento de sistema</param>  
	''' <remarks>Llamada desde: sistema; Tiempo máximo:0 </remarks>
	Private Sub whdgEntregas_GroupedColumnsChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GroupedColumnsChangedEventArgs) Handles whdgArticulos.GroupedColumnsChanged
		Dim groupedColumnList As String = String.Empty
		For Each column As Infragistics.Web.UI.GridControls.GroupedColumn In e.GroupedColumns
			groupedColumnList = IIf(groupedColumnList Is String.Empty, String.Empty, "#")
			groupedColumnList &= column.ColumnKey & IIf(column.SortDirection = Infragistics.Web.UI.GridControls.GroupingSortDirection.Ascending, " ASC", " DESC")
		Next
		Session("COLSGROUPBY") = groupedColumnList

		Dim cookie As HttpCookie
		cookie = Request.Cookies("COLSGROUPBY")
		If cookie Is Nothing Then
			cookie = New HttpCookie("COLSGROUPBY")
		End If
		cookie.Value = groupedColumnList
		cookie.Expires = Date.MaxValue
		Response.AppendCookie(cookie)

	End Sub
#End Region
#Region "Page Methods"
	''' <summary>
	''' Obtiene los datos de la pestaña datos generales
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Cargar_Datos_Generales(ByVal iDesdeFavorito As Integer, ByVal sIden As String, ByVal iCodFavorito As Integer, ByVal iModiFav As Integer) As Object
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			Dim listaCostes As New List(Of CAtributoPedido)
			Dim listaDescuentos As New List(Of CAtributoPedido)
			Dim iId As Integer
			Dim cEmisionPedido As New CEmisionPedido

			Int32.TryParse(sIden, iId)
			cEmisionPedido.DatosGenerales = cEmisionPedidos.Cargar_Datos_Generales(FSNUser.CodPersona, iId, iDesdeFavorito, iCodFavorito, FSNUser.Idioma, iModiFav)

			Return serializer.Serialize(cEmisionPedido)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Obtiene los datos de costes y descuentos del pedido
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Cargar_Costes_Descuentos(ByVal iDesdeFavorito As Integer, ByVal sIden As String, ByVal sIDTipoPedido As String, ByVal EsPedidoAbierto As Boolean) As Object
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			Dim listaCostes As New List(Of CAtributoPedido)
			Dim listaDescuentos As New List(Of CAtributoPedido)
			Dim iCodPedido, iIDTipoPedido As Integer

			Dim oAtributos As CAtributosPedido
			Dim oAtributo As CAtributoPedido

			Dim oEmisionPedido As New CEmisionPedido

			Int32.TryParse(sIden, iCodPedido)
			Int32.TryParse(sIDTipoPedido, iIDTipoPedido)

			If iDesdeFavorito = 0 Then
				oAtributos = cEmisionPedidos.Cargar_Costes(iDesdeFavorito, iCodPedido, iIDTipoPedido, FSNUser.Idioma.ToString, EsPedidoAbierto)
				For Each oAtributo In oAtributos
					listaCostes.Add(oAtributo)
				Next

				oEmisionPedido.Costes = listaCostes

				oAtributos = cEmisionPedidos.Cargar_Descuentos(iCodPedido, iIDTipoPedido, EsPedidoAbierto)
				For Each oAtributo In oAtributos
					listaDescuentos.Add(oAtributo)
				Next
				oEmisionPedido.Descuentos = listaDescuentos
			End If
			Return serializer.Serialize(oEmisionPedido)

		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Obtiene los datos de la pestaña de lineas de pedido
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Cargar_Lineas(ByVal iDesdeFavorito As Integer, ByVal sIden As String, ByVal sIDTipoPedido As String, ByVal bHayIntegracion As Boolean, ByVal idEmpresa As Integer,
										 ByVal EsPedidoAbierto As Boolean, ByVal iCodOrdenEmitir As Long) As Object
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim iId As Integer
			Dim oPer As FSNServer.CPersona
			Dim oAtributos As CAtributosPedido
			Dim oAtributo As CAtributoPedido
			Dim oLinea As CLineaPedido
			Dim iIdTipoPedido As Integer
			Dim cEmisionPedido As New CEmisionPedido
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEmisionPedidos As CEmisionPedidos
			Dim oLineas As CLineasPedido

			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			Integer.TryParse(sIden, iId)
			oPer = FSNServer.Get_Object(GetType(FSNServer.CPersona))
			oPer.Cod = FSNUser.CodPersona
			oPer.CargarDestinos(FSNUser.Idioma, SoloDestinoEmision:=True)

			oLineas = LineasPedido(iId, iDesdeFavorito, idEmpresa)

			Integer.TryParse(sIDTipoPedido, iIdTipoPedido)
			For Each oLinea In oLineas
				If Not String.IsNullOrEmpty(oLinea.Dest) Then
					Dim oDest As CDestino = oPer.Destinos.Find(Function(des As CDestino) des.Cod = oLinea.Dest)
					If oDest IsNot Nothing Then
						oLinea.DestDen = oDest.Den
					Else
						oLinea.Dest = FSNUser.Destino
						Dim oDestino As CDestino = oPer.Destinos.Find(Function(des As CDestino) des.Cod = oLinea.Dest)
						If oDestino IsNot Nothing Then
							oLinea.DestDen = oDestino.Den
						End If
					End If
				End If
				Dim listaCostes As New List(Of CAtributoPedido)
				If oLinea.LineaCatalogo <> 0 Then
					oAtributos = cEmisionPedidos.Cargar_Costes_Linea(iDesdeFavorito, oLinea.ID, iIdTipoPedido, FSNUser.Idioma, EsPedidoAbierto)
					For Each oAtributo In oAtributos
						listaCostes.Add(oAtributo)
					Next
				End If
				oLinea.Costes = listaCostes

				Dim listaDescuentos As New List(Of CAtributoPedido)
				If oLinea.LineaCatalogo <> 0 Then
					oAtributos = cEmisionPedidos.Cargar_Descuentos_Linea(iDesdeFavorito, oLinea.ID, iIdTipoPedido, EsPedidoAbierto)
					For Each oAtributo In oAtributos
						listaDescuentos.Add(oAtributo)
					Next
				End If
				oLinea.Descuentos = listaDescuentos

				Dim listaOtrosDatos As New List(Of CAtributoPedido)
				If oLinea.LineaCatalogo <> 0 Then
					oAtributos = cEmisionPedidos.Cargar_Atributos_Linea(iDesdeFavorito, oLinea.ID, iIdTipoPedido, bHayIntegracion, idEmpresa, EsPedidoAbierto, iCodOrdenEmitir, FSNUser.Cod)
					For Each oAtributo In oAtributos
						listaOtrosDatos.Add(oAtributo)
					Next
				End If
				oLinea.OtrosDatos = listaOtrosDatos
			Next

			Return serializer.Serialize(oLineas)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Cargar combo receptores
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Cargar_Combo_Receptores(ByVal sDestinos As String) As Object
		Try
			Dim serializer As New JavaScriptSerializer()

			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim sComDest = sDestinos.Replace(",", "")
			If sComDest = String.Empty Then
				sDestinos = String.Empty
			End If
			If sDestinos = String.Empty Then
				If Not String.IsNullOrEmpty(CType(FSNUser, FSNServer.User).Destino) Then
					sDestinos = (CType(FSNUser, FSNServer.User).Destino).ToString()
				End If
			End If

			Dim oPer As FSNServer.CPersona

			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

			oPer = FSNServer.Get_Object(GetType(FSNServer.CPersona))
			oPer.Cod = FSNUser.CodPersona
			oPer.CargarDestinos(FSNUser.Idioma, SoloDestinoEmision:=True)

			Dim listaDestinos() As String
			If sDestinos <> "" Then
				listaDestinos = Split(sDestinos, ",")
			ElseIf Not String.IsNullOrEmpty(CType(FSNUser, FSNServer.User).Destino) Then
				listaDestinos = Split(CType(FSNUser, FSNServer.User).Destino, ",")
			ElseIf oPer.Destinos.Count > 0 Then
				For Each oDestino As CDestino In oPer.Destinos
					sDestinos = oDestino.Cod + ","
				Next
				listaDestinos = Split(sDestinos, ",")
			End If

			'Para la carga de receptores hay que tener en cuenta que estos pertenezcan a las unidades organizativas de los destinos

			Dim receptores As CPersonas
			If (Not IsNothing(listaDestinos)) Then
				Dim oHd As String
				oHd = String.Join(",", listaDestinos.Distinct().ToArray())
				receptores = DevolverReceptores(String.Join(",", listaDestinos.Distinct().ToArray()))
			Else
				receptores = DevolverReceptores("")
			End If
			Return (serializer.Serialize(receptores))

		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Carga los receptores de una lista de destinos
	''' </summary>
	''' <param name="sListaDestinos">contiene la cadena con los destinos</param>
	''' <returns>Devuelve un objeto tipo CPersonas</returns>
	''' <remarks>LLamada desde: BtnAcepDest_Click y dlCesta_ItemDataBound ; Tiempo mÃ¡x: 0,1 sg</remarks>
	Private Shared Function DevolverReceptores(ByVal sListaDestinos As String) As CPersonas
		Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
		Dim oReceptores As CPersonas = FSNServer.Get_Object(GetType(FSNServer.CPersonas))
		Dim oUsuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
		oReceptores.CargarReceptores(sListaDestinos)
		Return oReceptores
	End Function
	''' <summary>
	''' Devuelve los datos de los proveedores ERP para la empresa dada, o el proveedor dado, o sólo uno si se incluye el código
	''' </summary>
	''' <remarks>
	''' Llamada desde: EpWeb, página EmisionPedido, método DataList1_ItemDataBound
	''' Tiempo máximo: 50 milisegundos</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function CargarProveedoresERP(ByVal lEmpresa As String, ByVal sProve As String, ByVal sCodERP As String) As Object
		Try
			Dim iEmpresa As Integer
			Dim serializer As New JavaScriptSerializer()
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim devolverCodErp As Boolean
			Dim cProv As CProveERPs
			cProv = FSNServer.Get_Object(GetType(FSNServer.CProveERPs))
			Int32.TryParse(lEmpresa, iEmpresa)
			devolverCodErp = cProv.CargarCodProveERP(iEmpresa)
			If devolverCodErp = False Then

			Else
				Dim cProveErp As CEmisionPedidos
				cProveErp = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

				Dim rs As New DataSet
				cProv = cProveErp.CargarProveedoresERP(FSNUser.CodPersona, iEmpresa, sProve, sCodERP)
				rs.Clear()
			End If
			Return {devolverCodErp, (serializer.Serialize(cProv))}
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>Devuelve las organizaciones de compras para la empresa dada</summary>
	''' <param name="iEmpresa">Id de la empresa</param>
	''' <remarks>Llamada desde: EmisionPedido.js</remarks>    
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function CargarOrganizacionesCompras(ByVal iEmpresa As Integer) As Object
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

			Dim oOrgCompras As COrganizacionesCompras = FSNServer.Get_Object(GetType(FSNServer.COrganizacionesCompras))
			oOrgCompras.CargarOrganizacionesCompras(iEmpresa)

			Return {(serializer.Serialize(oOrgCompras))}
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Obtiene los archivos adjuntos
	''' </summary>
	''' <param name="sIden">Identificador de la cesta</param>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Cargar_Archivos(ByVal iDesdeFavorito As Integer, ByVal sIden As String) As Object
		Try
			Dim serializer As New JavaScriptSerializer()

			Dim iId As Integer
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			Int32.TryParse(sIden, iId)

			Dim oAjuntos As CAdjuntos
			oAjuntos = cEmisionPedidos.Cargar_Archivos(iDesdeFavorito, FSNUser.CodPersona, iId)

			Return serializer.Serialize(oAjuntos)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Obtiene los archivos adjuntos de la linea
	''' </summary>
	''' <param name="sIden">Identificador de la cesta</param>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Cargar_ArchivosLinea(ByVal iDesdeFavorito As Integer, ByVal sIden As String, ByVal sIdLinea As String) As Object
		Try
			Dim serializer As New JavaScriptSerializer()

			Dim iId As Integer
			Dim iIdLinea As Integer
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			Int32.TryParse(sIden, iId)
			Int32.TryParse(sIdLinea, iIdLinea)


			Dim oAjuntos As CAdjuntos
			oAjuntos = cEmisionPedidos.Cargar_ArchivosLinea(iDesdeFavorito, FSNUser.CodPersona, iId, iIdLinea)

			Return serializer.Serialize(oAjuntos)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Cargar Atributos Item
	''' </summary>
	''' <param name="iCodPedido">i Cod Pedido</param>
	''' <param name="sIDTipoPedido">Id Pedido</param>
	''' <returns>Atributos</returns>
	''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function CargarAtributosPedido(ByVal iDesdeFavorito As Integer, ByVal iCodPedido As Integer, ByVal sIDTipoPedido As String, ByVal bHayIntegracion As Boolean,
												 ByVal iEmpresa As Integer, ByVal EsPedidoAbierto As Boolean, ByVal iCodOrdenEmitir As Long) As Object
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim iIdTipoPedido As Integer
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))


			Int32.TryParse(sIDTipoPedido, iIdTipoPedido)
			Dim oAtributos As CAtributosPedido
			oAtributos = cEmisionPedidos.Cargar_Atributos(iDesdeFavorito, iCodPedido, iIdTipoPedido, bHayIntegracion, iEmpresa, EsPedidoAbierto, iCodOrdenEmitir, FSNUser.Cod)


			Return serializer.Serialize(oAtributos)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Cargar CargarAtributosCabeceraIntegracion Item
	''' </summary>
	''' <param name="iCodPedido">i Cod Pedido</param>
	''' <param name="sIDTipoPedido">Id Pedido</param>
	''' <returns>Atributos</returns>
	''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function CargarAtributosCabeceraIntegracion(ByVal iCodPedido As Integer, ByVal sIDTipoPedido As String, ByVal bHayIntegracion As Boolean, ByVal iEmpresa As Integer) As Object
		Try

			Dim serializer As New JavaScriptSerializer()

			Dim iIdTipoPedido As Integer
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))


			Int32.TryParse(sIDTipoPedido, iIdTipoPedido)
			Dim oAtributos As CAtributosPedido
			oAtributos = cEmisionPedidos.Cargar_Atributos_Integracion(iCodPedido, iIdTipoPedido, bHayIntegracion, iEmpresa, FSNUser.Cod)

			Return serializer.Serialize(oAtributos)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Cargar_Tipo_Pedido
	''' </summary>
	''' <param name="iCodPedido">i Cod Pedido</param>
	''' <param name="sIDTipoPedido">Id Pedido</param>
	''' <param name="sEmp">Id Empresa</param>
	''' <returns>Atributos</returns>
	''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Cargar_Tipo_Pedido(ByVal iCodPedido As Integer, ByVal sIDTipoPedido As String, ByVal sEmp As String) As Object
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim iIdTipoPedido, iEmp As Integer
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cTiposPedidos As cTiposPedidos
			cTiposPedidos = FSNServer.Get_Object(GetType(FSNServer.cTiposPedidos))

			Int32.TryParse(sEmp, iEmp)
			Int32.TryParse(sIDTipoPedido, iIdTipoPedido)
			cTiposPedidos.CargarTiposPedido(FSNUser.Idioma)

			Dim oTipoPedido As cTipoPedido
			oTipoPedido = FSNServer.Get_Object(GetType(FSNServer.cTipoPedido))
			oTipoPedido = cTiposPedidos.ItemId(iIdTipoPedido)

			Return serializer.Serialize(oTipoPedido)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Comprobar_TipoPedido
	''' </summary>
	''' <param name="iCodPedido">i Cod Pedido</param>
	''' <param name="sIDTipoPedido">Id Pedido</param>
	''' <param name="sEmp">Id Empresa</param>
	''' <returns>Atributos</returns>
	''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Comprobar_TipoPedido(ByVal iCodPedido As Integer, ByVal sIDTipoPedido As String, ByVal sEmp As String) As Object
		Try

			Dim serializer As New JavaScriptSerializer()

			Dim iIdTipoPedido, iEmp As Integer
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))


			Int32.TryParse(sEmp, iEmp)
			Int32.TryParse(sIDTipoPedido, iIdTipoPedido)
			Dim oTipoPedido As Object
			oTipoPedido = cEmisionPedidos.Comprobar_TipoPedido(iCodPedido, iIdTipoPedido, iEmp)

			Return serializer.Serialize(oTipoPedido)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Comprobar_TipoEmpresa
	''' </summary>
	''' <param name="iCodPedido">i Cod Pedido</param>
	''' <param name="sEmp">Id Empresa</param>
	''' <returns>Atributos</returns>
	''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Comprobar_Empresa(ByVal idLinea As Integer, ByVal sEmp As String, ByVal EsPedidoAbierto As Boolean, ByVal sOrgCompras As String, ByVal modFav As Short) As Object
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim iEmp As Integer
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			Int32.TryParse(sEmp, iEmp)
			Dim oTipoPedido As Object
			oTipoPedido = cEmisionPedidos.Comprobar_Empresa(idLinea, iEmp, EsPedidoAbierto, sOrgCompras, modFav)

			Return serializer.Serialize(oTipoPedido)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Comprobar_Activo
	''' </summary>
	''' <param name="iCodPedido">idLinea</param>
	''' <param name="sEmp">Id Empresa</param>
	''' <param name="iActivo">iActivo</param>
	''' <returns>Atributos</returns>
	''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Comprobar_Activo(ByVal sEmp As String, ByVal sActivo As String, ByVal sCentro As String) As Object
		Try

			Dim serializer As New JavaScriptSerializer()

			Dim iEmp As Integer
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

			Int32.TryParse(sEmp, iEmp)
			Dim oAct As FSNServer.Activos = FSNServer.Get_Object(GetType(FSNServer.Activos))
			oAct.ObtenerActivos(FSNUser.Idioma.ToString(), iEmp, sCentro, True)

			Dim act As FSNServer.Activo = oAct.Find(Function(el As FSNServer.Activo) el.Codigo.ToLower() = sActivo.ToLower())
			If act IsNot Nothing Then
				Return serializer.Serialize(act)
			Else
				If Not String.IsNullOrEmpty(sActivo.ToString()) Then
					act = oAct.Find(Function(el As FSNServer.Activo) _
									  String.Concat(el.Codigo, " - ", el.Denominacion).ToLower().Contains(sActivo.ToLower()))
					If act Is Nothing Then
						Return "0"
					Else
						Return serializer.Serialize(act)
					End If
				End If
			End If

			Return "0"
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Cargar Atributos Costes
	''' </summary>
	''' <param name="iCodPedido">i Cod Pedido</param>
	''' <param name="sIDTipoPedido">Id Pedido</param>
	''' <returns>Atributos</returns>
	''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function CargarAtributosCostes(ByVal iDesdeFavorito As Integer, ByVal iCodPedido As Integer, ByVal sIDTipoPedido As String, ByVal EsPedidoAbierto As Boolean) As Object
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim iIdTipoPedido As Integer
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))


			Int32.TryParse(sIDTipoPedido, iIdTipoPedido)
			Dim oAtributos As CAtributosPedido
			oAtributos = cEmisionPedidos.Cargar_Costes(iDesdeFavorito, iCodPedido, iIdTipoPedido, FSNUser.Idioma.ToString(), EsPedidoAbierto)

			Return serializer.Serialize(oAtributos)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Cargar Atributos Costes de las lineas
	''' </summary>
	''' <param name="iCodLinea">iCodPedido</param>
	''' <param name="sIDTipoPedido">Id Pedido</param>
	''' <returns>Atributos</returns>
	''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function CargarAtributosCostesLinea(ByVal iDesdeFavorito As Integer, ByVal iCodLinea As Integer, ByVal sIDTipoPedido As String, ByVal EsPedidoAbierto As Boolean) As Object
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim iIdTipoPedido As Integer
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			Int32.TryParse(sIDTipoPedido, iIdTipoPedido)
			Dim oAtributos As CAtributosPedido
			oAtributos = cEmisionPedidos.Cargar_Costes_Linea(iDesdeFavorito, iCodLinea, iIdTipoPedido, FSNUser.Idioma, EsPedidoAbierto)

			Return serializer.Serialize(oAtributos)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Cargar Atributos Descuentos
	''' </summary>
	''' <param name="iCodPedido">i Cod Pedido</param>
	''' <param name="sIDTipoPedido">Id Pedido</param>
	''' <returns>Atributos</returns>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function CargarAtributosDescuentos(ByVal iCodPedido As Integer, ByVal sIDTipoPedido As String, ByVal EsPedidoAbierto As Boolean) As Object
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim iIdTipoPedido As Integer
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))


			Int32.TryParse(sIDTipoPedido, iIdTipoPedido)
			Dim oAtributos As CAtributosPedido
			oAtributos = cEmisionPedidos.Cargar_Descuentos(iCodPedido, iIdTipoPedido, EsPedidoAbierto)

			Return serializer.Serialize(oAtributos)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Cargar Atributos Descuentos de la linea
	''' </summary>
	''' <param name="iCodLinea">iCodLinea</param>
	''' <param name="sIDTipoPedido">Id Pedido</param>
	''' <returns>Atributos</returns>
	''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function CargarAtributosDescuentosLinea(ByVal iDesdeFavorito As Integer, ByVal iCodLinea As Integer, ByVal sIDTipoPedido As String, ByVal EsPedidoAbierto As Boolean) As Object
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim iIdTipoPedido As Integer
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			Int32.TryParse(sIDTipoPedido, iIdTipoPedido)
			Dim oAtributos As CAtributosPedido
			oAtributos = cEmisionPedidos.Cargar_Descuentos_Linea(iDesdeFavorito, iCodLinea, iIdTipoPedido, EsPedidoAbierto)

			Return serializer.Serialize(oAtributos)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Cargar Atributos Linea
	''' </summary>
	''' <param name="sIDTipoPedido">Id Pedido</param>
	''' <param name="iCodLinea">CodLinea</param>
	''' <returns>Atributos</returns>
	''' <remarks>Llamada desde: EmisionPedidoBis.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function CargarAtributosLinea(ByVal iDesdeFavorito As Integer, ByVal sIDTipoPedido As Integer, ByVal iCodLinea As Integer, ByVal bHayIntegracion As Boolean,
												ByVal iEmpresa As Integer, ByVal EsPedidoAbierto As Boolean) As Object
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			Dim oAtributos As CAtributosPedido
			oAtributos = cEmisionPedidos.Cargar_Atributos_Lineas(iDesdeFavorito, sIDTipoPedido, iCodLinea, bHayIntegracion, iEmpresa, EsPedidoAbierto, 0, FSNUser.Cod)

			Return serializer.Serialize(oAtributos)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Cargar Atributos Linea Integracion
	''' </summary>
	''' <param name="sIDTipoPedido">Id Pedido</param>
	''' <param name="iCodLinea">CodLinea</param>
	''' <returns>Atributos</returns>
	''' <remarks>Llamada desde: EmisionPedidoBis.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function CargarAtributosIntegracionLinea(ByVal iDesdeFavorito As Integer, ByVal sIDTipoPedido As Integer, ByVal iCodLinea As Integer, ByVal bHayIntegracion As Boolean, ByVal iEmpresa As Integer) As Object
		Try

			Dim serializer As New JavaScriptSerializer()

			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			Dim oAtributos As CAtributosPedido
			oAtributos = cEmisionPedidos.Cargar_Atributos_Integracion_Lineas(iDesdeFavorito, sIDTipoPedido, iCodLinea, bHayIntegracion, iEmpresa, FSNUser.Cod)

			Return serializer.Serialize(oAtributos)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' CargarDatosDestinos
	''' </summary>
	''' <returns>Destinos</returns>
	''' <remarks>Llamada desde: EmisionPedidoBis.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function CargarDatosDestinos() As Object
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim oPer As FSNServer.CPersona
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

			oPer = FSNServer.Get_Object(GetType(FSNServer.CPersona))
			oPer.Cod = FSNUser.CodPersona
			oPer.CargarDestinos(FSNUser.Idioma, SoloDestinoEmision:=True)

			Return serializer.Serialize(oPer.Destinos)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' recuperarTipoLinea
	''' </summary>
	''' <param name="TipoPedidoOrden">TipoPedidoOrden</param>
	''' <param name="oLinea">oLinea</param>
	''' <returns>Destinos</returns>
	''' <remarks>Llamada desde: EmisionPedidoBis.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function recuperarTipoLinea(ByVal TipoPedidoOrden As String, ByVal sLineaCatalogo As String, ByVal TipoLinea As String) As Object
		Dim serializer As New JavaScriptSerializer()
		Dim oTipoLinea As CArticulo.strTipoArticulo


		Dim cTipoPedidoOrden As cTipoPedido = serializer.Deserialize(Of cTipoPedido)(TipoPedidoOrden)
		Dim oLineaTipo As CArticulo.strTipoArticulo = serializer.Deserialize(Of CArticulo.strTipoArticulo)(TipoLinea)
		Dim iLineaCatalogo As Integer
		Int32.TryParse(sLineaCatalogo, iLineaCatalogo)
		If iLineaCatalogo = 0 Then 'Si es pedido libre, no tiene info de artículo
			If cTipoPedidoOrden Is Nothing Then
				oTipoLinea.TipoAlmacenamiento = CArticulo.EnAlmacenTipo.Opcional
				oTipoLinea.TipoRecepcion = CArticulo.EnRecTipo.Opcional
				oTipoLinea.Concepto = CArticulo.EnInvTipo.Ambos
			Else
				oTipoLinea.TipoAlmacenamiento = cTipoPedidoOrden.Almacenable
				oTipoLinea.TipoRecepcion = cTipoPedidoOrden.Recepcionable
				oTipoLinea.Concepto = cTipoPedidoOrden.Concepto
			End If
		ElseIf cTipoPedidoOrden Is Nothing Then 'Si no es pedido libre y no hay tipo de pedido
			oTipoLinea = oLineaTipo
		Else 'Si no es pedido libre y hay tipo de pedido
			oTipoLinea.TipoAlmacenamiento = CombinarTipos(oLineaTipo.TipoAlmacenamiento, cTipoPedidoOrden.Almacenable)
			oTipoLinea.TipoRecepcion = CombinarTipos(oLineaTipo.TipoRecepcion, cTipoPedidoOrden.Recepcionable)
			oTipoLinea.Concepto = CombinarTipos(oLineaTipo.Concepto, cTipoPedidoOrden.Concepto)
		End If
		Return serializer.Serialize(oTipoLinea)
	End Function
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function CombinarTipos(ByVal TipoLinea As Integer, ByVal TipoOrden As Integer) As Integer
		Select Case TipoLinea
			Case 0, 1
				Return TipoLinea
			Case 2
				Select Case TipoOrden
					Case 0, 1, 2
						Return TipoOrden
					Case Else
						Throw New ApplicationException("Tipo erróneo")
				End Select
			Case Else
				Throw New ApplicationException("Tipo erróneo")
		End Select
	End Function
	''' <summary>Cargar Almacenes</summary>
	''' <param name="idDestino">idDestino</param>
	''' <param name="sCentro">Centro de aprovisionamiento</param>
	''' <returns>Atributos</returns>
	''' <remarks>Llamada desde: EmisionPedidoBis.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function CargarAlmacenes(ByVal idDestino As String, ByVal sCentro As String) As Object
		Try
			Dim serializer As New JavaScriptSerializer()

			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

			Dim oAlmacenes As cAlmacenes = FSNServer.Get_Object(GetType(FSNServer.cAlmacenes))

			If Not String.IsNullOrEmpty(idDestino) Then
				'Obtener los almacenes correspondientes al destino                
				Dim oPer As FSNServer.CPersona = FSNServer.Get_Object(GetType(FSNServer.CPersona))
				oPer.Cod = FSNUser.CodPersona
				oPer.CargarDestinos(FSNUser.Idioma, SoloDestinoEmision:=True)
				If (Not IsNothing(oPer.Destinos)) Then
					If Not idDestino = String.Empty And (oPer.Destinos.Count <> 0) Then
						If (Not IsNothing(oPer.Destinos.Item(idDestino))) Then
							oAlmacenes = oPer.Destinos.Item(idDestino).Almacenes
						End If
					End If
				End If
			ElseIf Not String.IsNullOrEmpty(sCentro) Then
				'Obtener los almacenes del centro de aprovisionamiento
				Dim oCentro As FSNServer.Centro = FSNServer.Get_Object(GetType(FSNServer.Centro))
				oCentro.CargarAlmacenesCentroAprovisionamiento(sCentro)
				oAlmacenes = oCentro.Almacenes
			End If

			Return serializer.Serialize(oAlmacenes)
		Catch ex As Exception
			Throw ex
		End Try

	End Function
	''' <summary>
	''' Guardar Pedido
	''' </summary>
	''' <param name="oEmisionPedido">Objeto Pedido</param>
	''' <returns>Atributos</returns>
	''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function GuardarPedido(ByVal iDesdeFavorito As Integer, ByVal oEmisionPedido As String, ByVal bHayIntegracion As Boolean, ByVal idEmpresa As Integer,
										 ByVal sDescFavorito As String, ByVal ModoEdicion As Boolean, ByVal EsPedidoAbierto As Boolean) As Boolean
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")

			Dim oPer As FSNServer.CPersona
			oPer = FSNServer.Get_Object(GetType(FSNServer.CPersona))
			oPer.Cod = FSNUser.CodPersona
			oPer.CargarDestinos(FSNUser.Idioma, SoloDestinoEmision:=True)

			Dim cEmisionPedido As CEmisionPedido = serializer.Deserialize(Of CEmisionPedido)(oEmisionPedido)
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			'ModoEdicion = True en el .aspx cuando se edita un nÃºmero de lÃ­nea o se ha subido/bajado
			'ModoEdicion = True tambien si la cesta que se carga fue guardada habiendo sido editada
			For Each oLinea As CLineaPedido In cEmisionPedido.Lineas
				If oLinea.NumLinModificado = True Then
					ModoEdicion = True
					Exit For
				End If
			Next

			cEmisionPedidos.ActualizarPedido(iDesdeFavorito, FSNUser.Cod, FSNUser.Idioma, oPer.Cod, cEmisionPedido.DatosGenerales.Moneda, cEmisionPedido, sDescFavorito, , , ModoEdicion, EsPedidoAbierto)

			Return True
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Guardar Nuevo Favorito
	''' </summary>
	''' <param name="oEmisionPedido">Objeto Pedido</param>
	''' <param name="sDescFavorito">Nombre favorito</param>
	''' <returns>Atributos</returns>
	''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function GuardarNuevoFavorito(ByVal oEmisionPedido As String, ByVal sDescFavorito As String, ByVal dImporte As Decimal) As Integer
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim iNuevoFavorito As Integer
			Dim cEmisionPedido As CEmisionPedido = serializer.Deserialize(Of CEmisionPedido)(oEmisionPedido)
			Dim cEmisionPedidos As CEmisionPedidos

			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))
			iNuevoFavorito = cEmisionPedidos.GuardarNuevoFavorito(cEmisionPedido, sDescFavorito, dImporte)
			Return serializer.Serialize(iNuevoFavorito)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Guardar Nuevo Favorito y Borrar el pedido
	''' </summary>
	''' <param name="oEmisionPedido">Objeto Pedido</param>
	''' <param name="sDescFavorito">Nombre favorito</param>
	''' <returns>Atributos</returns>
	''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function GuardarNuevoFavoritoYBorrar(ByVal oEmisionPedido As String, ByVal sDescFavorito As String, ByVal dImporte As Decimal) As Integer
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim iNuevoFavorito As Integer
			Dim cEmisionPedido As CEmisionPedido = serializer.Deserialize(Of CEmisionPedido)(oEmisionPedido)
			Dim cEmisionPedidos As CEmisionPedidos

			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))
			iNuevoFavorito = cEmisionPedidos.GuardarNuevoFavorito(cEmisionPedido, sDescFavorito, dImporte)

			Dim cLinea As New CLineaPedido
			For Each cLinea In cEmisionPedido.Lineas
				cEmisionPedidos.BorrarLinea(0, cLinea.ID, cEmisionPedido.DatosGenerales.Id, FSNUser.Cod)
			Next

			Return serializer.Serialize(iNuevoFavorito)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' ComprobarCentroCoste
	''' </summary>
	''' <param name="emp">emp</param>
	''' <param name="sImputacion">oImputacion</param>
	''' <param name="txt">texto a buscar</param>
	''' <param name="CtrlPluriAnual">Al emitir se lanza siempre ComprobarCentroContratoAEmitirPluriAnual. La logica cambia si resulta q esa linea no es plurianual->falso negativo</param>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function ComprobarCentroCoste(ByVal emp As String, ByVal sImputacion As String, ByVal txt As String, ByVal emitir As Boolean, ByVal CtrlPluriAnual As Boolean) As String
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim Imputacion As Imputacion = serializer.Deserialize(Of Imputacion)(sImputacion)
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim AnyoMal As Integer
			Dim oPargenSMS As FSNServer.PargensSMs
			oPargenSMS = FSNServer.Get_Object(GetType(FSNServer.PargensSMs))
			oPargenSMS.CargarConfiguracionSM(FSNUser.Cod, FSNUser.Idioma, True)

			Dim _mipargenSMs As FSNServer.PargensSMs
			Dim _mipargenSM As FSNServer.PargenSM
			If FSNServer.TipoAcceso.gbAccesoFSSM Then
				_mipargenSMs = oPargenSMS
				If _mipargenSMs.Count > 0 Then
					_mipargenSM = Nothing '_mipargenSMs(0)
					For Each pargen As PargenSM In _mipargenSMs
						If pargen.Pres5 = Imputacion.Partida.NIV0 Then
							_mipargenSM = pargen
						End If
					Next

				Else
					_mipargenSM = Nothing
				End If
			Else
				_mipargenSM = Nothing
			End If

			If (emitir AndAlso CtrlPluriAnual AndAlso (_mipargenSM Is Nothing OrElse (Not _mipargenSM.Plurianual))) Then
				Return serializer.Serialize(100000) '100.000 implica todo ok
			End If

			Dim ArbolImputacion As FSNServer.UONs = FSNServer.Get_Object(GetType(FSNServer.UONs))
			ArbolImputacion.ArbolPartidasImputacion(FSNUser.Cod, FSNUser.Idioma, String.Empty, FSNUser.PedidosOtrasEmp, emp)

			If _mipargenSM Is Nothing AndAlso (Not CtrlPluriAnual) Then
				'Antes plurianual siempre hacia esto del ...SMs(0)
				'->siempre entraba por "FSNServer.TipoAcceso.gbAccesoFSSM AndAlso Not _mipargenSM Is Nothing AndAlso _mipargenSM.ImputacionPedido <> Fullstep.FSNServer.PargenSM.EnumTipoImputacion."
				'->->los colores del centro y partida
				_mipargenSM = _mipargenSMs(0)
			End If

			Dim empresa As String = emp
			If FSNServer.TipoAcceso.gbAccesoFSSM AndAlso Not _mipargenSM Is Nothing AndAlso _mipargenSM.ImputacionPedido <> Fullstep.FSNServer.PargenSM.EnumTipoImputacion.NoSeImputa Then
				Dim arbol = ArbolImputacion.FiltrarPorArbolPresupuestario(_mipargenSM.Pres5, (_mipargenSM.Vigencia = TipoVigencia.Bloquear))
				Dim query = From Datos In arbol.DevolverCentrosdeCoste()
							Where If(Datos.UONS.UON4 <> String.Empty, Datos.UONS.UON4.ToLower, If(Datos.UONS.UON3 <> String.Empty, Datos.UONS.UON3.ToLower, If(Datos.UONS.UON2 <> String.Empty, Datos.UONS.UON2.ToLower, If(Datos.UONS.UON1 <> String.Empty, Datos.UONS.UON1.ToLower, String.Empty)))) = txt.ToLower()
							Select Datos Take 1

				If query.Count = 0 Then
					query = From Datos In arbol.DevolverCentrosdeCoste()
							Where String.Concat(If(Datos.UONS.UON4 <> String.Empty, String.Concat(Datos.UONS.UON1.ToLower, "@@", Datos.UONS.UON2.ToLower, "@@", Datos.UONS.UON3.ToLower, "@@", Datos.UONS.UON4.ToLower),
								If(Datos.UONS.UON3 <> String.Empty, String.Concat(Datos.UONS.UON1.ToLower, "@@", Datos.UONS.UON2.ToLower, "@@", Datos.UONS.UON3.ToLower),
								If(Datos.UONS.UON2 <> String.Empty, String.Concat(Datos.UONS.UON1.ToLower, "@@", Datos.UONS.UON2.ToLower), Datos.UONS.UON1.ToLower))), " - ", Datos.Denominacion).ToLower().Contains(txt.ToLower())
							Select Datos Take 1
				End If

				If query.Count = 1 Then
					Dim c As FSNServer.Centro_SM = query(0)
					'DEVOLVER --> GREEN a centro
					If Not String.IsNullOrEmpty(Imputacion.Partida.PresupuestoId) Then
						If arbol.FindInTree(Function(el As FSNServer.UON) _
												el.CentroSM IsNot Nothing AndAlso el.CentroSM.Codigo = c.Codigo AndAlso el.CentroSM.PartidasPresupuestarias IsNot Nothing _
												AndAlso el.CentroSM.PartidasPresupuestarias.Item(Imputacion.Partida.ToString()) IsNot Nothing) Is Nothing Then
							'DEVOLVER --> GREEN a centro y DEVOLVER VACIAR a partida
							If (Not CtrlPluriAnual) Then Return serializer.Serialize(0)
						End If
					End If

					'Sacamos la lista de partidas del centro para dos cosas:
					Dim lis As Generic.List(Of FSNServer.UON) = arbol.ListaUONs().FindAll(Function(el As FSNServer.UON) _
													el.CentroSM IsNot Nothing AndAlso el.CentroSM.Codigo = c.Codigo AndAlso el.CentroSM.PartidasPresupuestarias IsNot Nothing _
													AndAlso el.CentroSM.PartidasPresupuestarias.Count > 0)
					'1. Si no hay partida, buscamos las partidas del centro
					If Not String.IsNullOrEmpty(Imputacion.Partida.PresupuestoId) Then
						If lis.Count = 1 AndAlso lis.Item(0).CentroSM.PartidasPresupuestarias.Count = 1 Then
							Dim p As FSNServer.PartidaPRES5 = lis.Item(0).CentroSM.PartidasPresupuestarias(0)

							If emitir AndAlso _mipargenSM.Plurianual Then AnyoMal = p.TienePresupuestoVigentePluriAnual(Imputacion.LineaId)

							If emitir AndAlso _mipargenSM.Plurianual AndAlso (AnyoMal = 200000) Then
								'200.000 implica KO y dame año
								Return serializer.Serialize(200000)
							ElseIf emitir AndAlso _mipargenSM.Plurianual AndAlso (AnyoMal = 100000) Then
								'100.000 implica todo ok
								'DEVOLVER --> GREEN a centro y DEVOLVER --> GREEN a partida
								Return serializer.Serialize(100000)
							ElseIf (Not emitir OrElse Not _mipargenSM.Plurianual) AndAlso p.TienePresupuestoVigente Then
								'DEVOLVER --> GREEN a centro y DEVOLVER --> GREEN a partida
								Return serializer.Serialize(1)
							Else
								If emitir AndAlso _mipargenSM.Plurianual Then
									Return serializer.Serialize(AnyoMal)
								Else
									Select Case _mipargenSM.Vigencia
										Case TipoVigencia.NoAplica
											'DEVOLVER --> GREEN a centro y DEVOLVER --> GREEN a partida
											Return serializer.Serialize(1)
										Case TipoVigencia.Avisar
											'DEVOLVER --> GREEN a centro y DEVOLVER --> Orange a partida
											Return serializer.Serialize(2)
										Case TipoVigencia.Bloquear
											'DEVOLVER --> GREEN a centro y DEVOLVER --> Red a partida
											Return serializer.Serialize(3)
									End Select
								End If
							End If
						Else
							For Each part As FSNServer.PartidaPRES5 In lis.Item(0).CentroSM.PartidasPresupuestarias
								If part.ToString = Imputacion.Partida.ToString() Then

									If emitir AndAlso _mipargenSM.Plurianual Then AnyoMal = part.TienePresupuestoVigentePluriAnual(Imputacion.LineaId)

									If emitir AndAlso _mipargenSM.Plurianual AndAlso (AnyoMal = 200000) Then
										'200.000 implica KO y dame año
										Return serializer.Serialize(200000)
									ElseIf emitir AndAlso _mipargenSM.Plurianual AndAlso (AnyoMal = 100000) Then
										'100.000 implica todo ok
										'DEVOLVER --> GREEN a centro y DEVOLVER --> GREEN a partida
										Return serializer.Serialize(100000)
									ElseIf (Not emitir OrElse Not _mipargenSM.Plurianual) AndAlso part.TienePresupuestoVigente Then
										'DEVOLVER --> GREEN a centro y DEVOLVER --> GREEN a partida
										Return serializer.Serialize(1)
									Else
										If emitir AndAlso _mipargenSM.Plurianual Then
											Return serializer.Serialize(AnyoMal)
										Else
											Select Case _mipargenSM.Vigencia
												Case TipoVigencia.NoAplica
													'DEVOLVER --> GREEN a centro y DEVOLVER --> GREEN a partida
													Return serializer.Serialize(1)
												Case TipoVigencia.Avisar
													'DEVOLVER --> GREEN a centro y DEVOLVER --> Orange a partida
													Return serializer.Serialize(2)
												Case TipoVigencia.Bloquear
													'DEVOLVER --> GREEN a centro y DEVOLVER --> Red a partida
													Return serializer.Serialize(3)
											End Select
										End If
									End If
								End If
							Next
						End If
					End If
				ElseIf emitir AndAlso CtrlPluriAnual Then
					Return serializer.Serialize(100000) '100.000 implica todo ok. Q el emitir, vaya como antes. El año NO esta mal. 
					'Ver function recuperarValidarDatosPantalla "if (bAccesoFSSM == true && bSMOblig == true) {" q mensajes puedes recibir. 
				Else
					'DEVOLVER --> Red a centro y vaciar partida
					Return serializer.Serialize(4)
				End If
			End If

			If emitir AndAlso CtrlPluriAnual Then
				Return serializer.Serialize(100000) '100.000 implica todo ok
			Else
				Return serializer.Serialize(True)
			End If
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' ComprobarCentroCosteEscrito
	''' </summary>
	''' <param name="emp">emp</param>
	''' <param name="sImputacion">oImputacion</param>
	''' <param name="txt">texto a buscar</param>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function ComprobarCentroCosteEscrito(ByVal emp As String, ByVal sImputacion As String, ByVal txt As String) As String
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim Imputacion As Imputacion = serializer.Deserialize(Of Imputacion)(sImputacion)
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim oPargenSMS As FSNServer.PargensSMs
			oPargenSMS = FSNServer.Get_Object(GetType(FSNServer.PargensSMs))
			oPargenSMS.CargarConfiguracionSM(FSNUser.Cod, FSNUser.Idioma, True)

			Dim _mipargenSMs As FSNServer.PargensSMs
			Dim _mipargenSM As FSNServer.PargenSM
			If FSNServer.TipoAcceso.gbAccesoFSSM Then
				_mipargenSMs = oPargenSMS
				If _mipargenSMs.Count > 0 Then
					_mipargenSM = _mipargenSMs(0)
				Else
					_mipargenSM = Nothing
				End If
			Else
				_mipargenSM = Nothing
			End If

			Dim ArbolImputacion As FSNServer.UONs = FSNServer.Get_Object(GetType(FSNServer.UONs))
			ArbolImputacion.ArbolPartidasImputacion(FSNUser.Cod, FSNUser.Idioma, String.Empty, FSNUser.PedidosOtrasEmp, emp)

			Dim empresa As String = emp
			If FSNServer.TipoAcceso.gbAccesoFSSM AndAlso Not _mipargenSM Is Nothing Then
				Dim arbol = ArbolImputacion.FiltrarPorArbolPresupuestario(_mipargenSM.Pres5, (_mipargenSM.Vigencia = TipoVigencia.Bloquear))
				Dim query = From Datos In arbol.DevolverCentrosdeCoste()
							Where If(Datos.UONS.UON4 <> String.Empty, Datos.UONS.UON4.ToLower, If(Datos.UONS.UON3 <> String.Empty, Datos.UONS.UON3.ToLower, If(Datos.UONS.UON2 <> String.Empty, Datos.UONS.UON2.ToLower, If(Datos.UONS.UON1 <> String.Empty, Datos.UONS.UON1.ToLower, String.Empty)))) = txt.ToLower()
							Select Datos Take 1

				If query.Count = 0 Then
					query = From Datos In arbol.DevolverCentrosdeCoste()
							Where String.Concat(If(Datos.UONS.UON4 <> String.Empty, String.Concat(Datos.UONS.UON1.ToLower, "@@", Datos.UONS.UON2.ToLower, "@@", Datos.UONS.UON3.ToLower, "@@", Datos.UONS.UON4.ToLower),
								If(Datos.UONS.UON3 <> String.Empty, String.Concat(Datos.UONS.UON1.ToLower, "@@", Datos.UONS.UON2.ToLower, "@@", Datos.UONS.UON3.ToLower),
								If(Datos.UONS.UON2 <> String.Empty, String.Concat(Datos.UONS.UON1.ToLower, "@@", Datos.UONS.UON2.ToLower), Datos.UONS.UON1.ToLower))), " - ", Datos.Denominacion).ToLower().Contains(txt.ToLower())
							Select Datos Take 1
				End If

				If query.Count = 1 Then
					Dim c As FSNServer.Centro_SM = query(0)
					'DEVOLVER --> GREEN a centro
					''Return 1
					If Not String.IsNullOrEmpty(Imputacion.Partida.PresupuestoId) Then
						If arbol.FindInTree(Function(el As FSNServer.UON) _
												el.CentroSM IsNot Nothing AndAlso el.CentroSM.Codigo = c.Codigo AndAlso el.CentroSM.PartidasPresupuestarias IsNot Nothing _
												AndAlso el.CentroSM.PartidasPresupuestarias.Item(Imputacion.Partida.ToString()) IsNot Nothing) Is Nothing Then
							'DEVOLVER --> GREEN a centro y DEVOLVER VACIAR a partida
							Return serializer.Serialize(c)
						End If
					End If

					'Sacamos la lista de partidas del centro para dos cosas:
					Dim lis As Generic.List(Of FSNServer.UON) = arbol.ListaUONs().FindAll(Function(el As FSNServer.UON) _
													el.CentroSM IsNot Nothing AndAlso el.CentroSM.Codigo = c.Codigo AndAlso el.CentroSM.PartidasPresupuestarias IsNot Nothing _
													AndAlso el.CentroSM.PartidasPresupuestarias.Count > 0)
					'1. Si no hay partida, buscamos las partidas del centro
					If Not String.IsNullOrEmpty(Imputacion.Partida.PresupuestoId) Then
						If lis.Count = 1 AndAlso lis.Item(0).CentroSM.PartidasPresupuestarias.Count = 1 Then
							Dim p As FSNServer.PartidaPRES5 = lis.Item(0).CentroSM.PartidasPresupuestarias(0)

							If p.TienePresupuestoVigente Then
								'DEVOLVER --> GREEN a centro y DEVOLVER --> GREEN a partida
								Return serializer.Serialize(1)
							Else
								Select Case _mipargenSM.Vigencia
									Case TipoVigencia.NoAplica
										'DEVOLVER --> GREEN a centro y DEVOLVER --> GREEN a partida
										Return serializer.Serialize(1)
									Case TipoVigencia.Avisar
										'DEVOLVER --> GREEN a centro y DEVOLVER --> Orange a partida
										Return serializer.Serialize(2)
									Case TipoVigencia.Bloquear
										'DEVOLVER --> GREEN a centro y DEVOLVER --> Red a partida
										Return serializer.Serialize(3)
								End Select
							End If
						Else
							For Each part As FSNServer.PartidaPRES5 In lis.Item(0).CentroSM.PartidasPresupuestarias
								If part.ToString = Imputacion.Partida.ToString() Then
									If part.TienePresupuestoVigente Then
										'DEVOLVER --> GREEN a centro y DEVOLVER --> GREEN a partida
										Return serializer.Serialize(1)
									Else
										Select Case _mipargenSM.Vigencia
											Case TipoVigencia.NoAplica
												'DEVOLVER --> GREEN a centro y DEVOLVER --> GREEN a partida
												Return serializer.Serialize(1)
											Case TipoVigencia.Avisar
												'DEVOLVER --> GREEN a centro y DEVOLVER --> Orange a partida
												Return serializer.Serialize(2)
											Case TipoVigencia.Bloquear
												'DEVOLVER --> GREEN a centro y DEVOLVER --> Red a partida
												Return serializer.Serialize(3)
										End Select
									End If
								End If
							Next
						End If
					End If
				Else
					'DEVOLVER --> Red a centro y vaciar partida
					Return serializer.Serialize(4)
				End If

				Return serializer.Serialize(True)
			End If

			Return serializer.Serialize(True)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' BuscarPartidaEnCentro
	''' </summary>
	''' <param name="emp">emp</param>
	''' <param name="sImputacion">oImputacion</param>
	''' <param name="txt">texto a buscar</param>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function BuscarPartidaEnCentro(ByVal emp As String, ByVal sImputacion As String, ByVal txt As String) As String
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim Imputacion As Imputacion = serializer.Deserialize(Of Imputacion)(sImputacion)
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim oPargenSMS As FSNServer.PargensSMs
			oPargenSMS = FSNServer.Get_Object(GetType(FSNServer.PargensSMs))
			oPargenSMS.CargarConfiguracionSM(FSNUser.Cod, FSNUser.Idioma, True)

			Dim _mipargenSMs As FSNServer.PargensSMs
			Dim _mipargenSM As FSNServer.PargenSM
			If FSNServer.TipoAcceso.gbAccesoFSSM Then
				_mipargenSMs = oPargenSMS
				If _mipargenSMs.Count > 0 Then
					_mipargenSM = _mipargenSMs(0)
				Else
					_mipargenSM = Nothing
				End If
			Else
				_mipargenSM = Nothing
			End If

			Dim ArbolImputacion As FSNServer.UONs = FSNServer.Get_Object(GetType(FSNServer.UONs))
			ArbolImputacion.ArbolPartidasImputacion(FSNUser.Cod, FSNUser.Idioma, String.Empty, FSNUser.PedidosOtrasEmp, emp)

			Dim empresa As String = emp
			If FSNServer.TipoAcceso.gbAccesoFSSM AndAlso Not _mipargenSM Is Nothing Then
				Dim arbol = ArbolImputacion.FiltrarPorArbolPresupuestario(_mipargenSM.Pres5, (_mipargenSM.Vigencia = TipoVigencia.Bloquear))
				Dim query = From Datos In arbol.DevolverCentrosdeCoste()
							Where If(Datos.UONS.UON4 <> String.Empty, Datos.UONS.UON4.ToLower, If(Datos.UONS.UON3 <> String.Empty, Datos.UONS.UON3.ToLower, If(Datos.UONS.UON2 <> String.Empty, Datos.UONS.UON2.ToLower, If(Datos.UONS.UON1 <> String.Empty, Datos.UONS.UON1.ToLower, String.Empty)))) = txt.ToLower()
							Select Datos Take 1

				If query.Count = 0 Then
					query = From Datos In arbol.DevolverCentrosdeCoste()
							Where String.Concat(If(Datos.UONS.UON4 <> String.Empty, String.Concat(Datos.UONS.UON1.ToLower, "@@", Datos.UONS.UON2.ToLower, "@@", Datos.UONS.UON3.ToLower, "@@", Datos.UONS.UON4.ToLower),
								If(Datos.UONS.UON3 <> String.Empty, String.Concat(Datos.UONS.UON1.ToLower, "@@", Datos.UONS.UON2.ToLower, "@@", Datos.UONS.UON3.ToLower),
								If(Datos.UONS.UON2 <> String.Empty, String.Concat(Datos.UONS.UON1.ToLower, "@@", Datos.UONS.UON2.ToLower), Datos.UONS.UON1.ToLower))), " - ", Datos.Denominacion).ToLower().Contains(txt.ToLower())
							Select Datos Take 1
				End If

				If query.Count = 1 Then
					Dim c As FSNServer.Centro_SM = query(0)
					'Sacamos la lista de partidas del centro para dos cosas:
					Dim lis As Generic.List(Of FSNServer.UON) = arbol.ListaUONs().FindAll(Function(el As FSNServer.UON) _
													el.CentroSM IsNot Nothing AndAlso el.CentroSM.Codigo = c.Codigo AndAlso el.CentroSM.PartidasPresupuestarias IsNot Nothing _
													AndAlso el.CentroSM.PartidasPresupuestarias.Count > 0)
					'1. Si no hay partida, buscamos las partidas del centro
					If Not String.IsNullOrEmpty(Imputacion.Partida.PresupuestoId) Then
						If lis.Count = 1 AndAlso lis.Item(0).CentroSM.PartidasPresupuestarias.Count = 1 Then
							Dim p As FSNServer.PartidaPRES5 = lis.Item(0).CentroSM.PartidasPresupuestarias(0)

							If p.TienePresupuestoVigente Then 'DEVOLVER --> GREEN a centro y DEVOLVER --> GREEN a partida
								Return serializer.Serialize(p)
							Else
								Select Case _mipargenSM.Vigencia
									Case TipoVigencia.NoAplica 'DEVOLVER --> GREEN a centro y DEVOLVER --> GREEN a partida
										Return serializer.Serialize(p)
									Case TipoVigencia.Avisar 'DEVOLVER --> GREEN a centro y DEVOLVER --> Orange a partida
										Return serializer.Serialize(p)
									Case TipoVigencia.Bloquear 'DEVOLVER --> GREEN a centro y DEVOLVER --> Red a partida
										Return serializer.Serialize(p)
								End Select
							End If
						Else
							Dim sImputacionPartida As String = String.Empty
							If Imputacion.Partida.Denominacion.ToLower().ToString() <> String.Empty Then
								'Compruebo contra toda la partida, no solo el ultimo nivel
								sImputacionPartida = Split(Imputacion.Partida.Denominacion.ToLower().ToString(), "- (")(0)
								Dim sDenom As String = ""
								sDenom = sImputacionPartida.Split("-").Last
								sImputacionPartida = Split(sImputacionPartida, " -" & sDenom)(0)
							End If

							For Each part As FSNServer.PartidaPRES5 In lis.Item(0).CentroSM.PartidasPresupuestarias
								Dim spartNiv4 As String = ""
								Dim spartNiv3 As String = ""
								Dim spartNiv2 As String = ""
								Dim spartNiv1 As String = ""
								Dim spartNiv0 As String = ""
								If (part.NIV4 <> String.Empty) Then
									spartNiv4 = part.NIV4.ToLower
								End If
								If (part.NIV3 <> String.Empty) Then
									spartNiv3 = part.NIV3.ToLower
								End If
								If (part.NIV2 <> String.Empty) Then
									spartNiv2 = part.NIV2.ToLower
								End If
								If (part.NIV1 <> String.Empty) Then
									spartNiv1 = part.NIV1.ToLower
								End If
								If (part.NIV0 <> String.Empty) Then
									spartNiv0 = part.NIV0.ToLower
								End If

								If String.Concat(spartNiv0, " - ", spartNiv1, " - ", spartNiv2, " - ", spartNiv3, " - ", spartNiv4, " - ", part.Denominacion).ToLower().Contains(sImputacionPartida) Then
									part.CodCentro = c.Codigo
									part.DenCentro = If(c.UONS.UON4 <> String.Empty, c.UONS.UON4, If(c.UONS.UON3 <> String.Empty, c.UONS.UON3, If(c.UONS.UON2 <> String.Empty, c.UONS.UON2, If(c.UONS.UON1 <> String.Empty, c.UONS.UON1, String.Empty)))) & " - " & c.Denominacion
									If part.TienePresupuestoVigente Then
										'DEVOLVER --> GREEN a centro y DEVOLVER --> GREEN a partida
										Return serializer.Serialize(part)
									Else
										Select Case _mipargenSM.Vigencia
											Case TipoVigencia.NoAplica 'DEVOLVER --> GREEN a centro y DEVOLVER --> GREEN a partida
												Return serializer.Serialize(part)
											Case TipoVigencia.Avisar 'DEVOLVER --> GREEN a centro y DEVOLVER --> Orange a partida
												Return serializer.Serialize(part)
											Case TipoVigencia.Bloquear 'DEVOLVER --> GREEN a centro y DEVOLVER --> Red a partida
												Return serializer.Serialize(part)
										End Select
									End If
								End If
							Next
						End If
					End If
				Else 'DEVOLVER --> Red a centro y vaciar partida
					Return serializer.Serialize(4)
				End If

				Return serializer.Serialize(True)
			End If

			Return serializer.Serialize(True)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Emitir Pedido
	''' </summary>
	''' <param name="oEmisionPedido">Objeto Pedido</param>
	''' <param name="oCentroCoste">Objeto Centro Coste</param>
	''' <param name="oPartidas">Objeto Partidas</param>
	''' <returns>Atributos</returns>
	''' <remarks>Llamada desde: EmisionPedidoBis.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function EmitirPedido(ByVal iDesdeFavorito As Integer, ByVal oEmisionPedido As String, ByVal oCentroCoste As String, ByVal oPartidas As String, ByVal idFav As Integer,
										ByVal bValidacionIntegracion As Boolean, ByVal bContinuarSinAviso As Boolean, ByVal dImporte As Decimal,
										ByVal iNotificar As OrdenEntregaNotificacion, ByVal EsPedidoAbierto As Boolean) As Object
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim idPedido As Integer = 0

			Dim oPer As FSNServer.CPersona
			oPer = FSNServer.Get_Object(GetType(FSNServer.CPersona))
			oPer.Cod = FSNUser.CodPersona
			oPer.CargarDestinos(FSNUser.Idioma, SoloDestinoEmision:=True)

			Dim cEmisionPedido As CEmisionPedido = serializer.Deserialize(Of CEmisionPedido)(oEmisionPedido)
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			If iDesdeFavorito = 1 Then
				Dim oItemsNotIssuable As DataTable = cEmisionPedidos.ComprobarAprovisonador(oPer.Cod, idFav)
				If oItemsNotIssuable IsNot Nothing AndAlso oItemsNotIssuable.Rows.Count > 0 Then
					Return serializer.Serialize(1)
				End If
			End If

			Dim UON As List(Of UON) = serializer.Deserialize(Of List(Of UON))(oCentroCoste)
			Dim PartidaPres5 As List(Of PartidaPRES5) = serializer.Deserialize(Of List(Of PartidaPRES5))(oPartidas)
			Dim iConta As Integer = 0
			For Each oLinea As CLineaPedido In cEmisionPedido.Lineas
				Dim Imputaciones As New List(Of Imputacion)
				Dim Imputacion As New Imputacion
				Imputacion.Id = cEmisionPedido.DatosGenerales.Id
				Imputacion.LineaId = oLinea.ID
				Imputacion.CentroCoste = UON.Item(iConta)
				Imputacion.Partida = PartidaPres5.Item(iConta)
				Imputaciones.Add(Imputacion)
				oLinea.Pres5_ImportesImputados_EP = Imputaciones
				iConta = iConta + 1
			Next

			Dim oPargenSMS As FSNServer.PargensSMs
			oPargenSMS = FSNServer.Get_Object(GetType(FSNServer.PargensSMs))
			oPargenSMS.CargarConfiguracionSM(FSNUser.Cod, FSNUser.Idioma, True)

			Dim _mipargenSMs As FSNServer.PargensSMs
			Dim _mipargenSM As FSNServer.PargenSM

			If FSNServer.TipoAcceso.gbAccesoFSSM Then
				_mipargenSMs = oPargenSMS
				If _mipargenSMs.Count > 0 Then
					_mipargenSM = _mipargenSMs(0)
				Else
					_mipargenSM = Nothing
				End If
			Else
				_mipargenSM = Nothing
			End If

			Dim Importe As Double = 0
			Dim arrItemPrecUni() As Double
			Dim arrItemCant() As Double
			Dim arrCat1Lin() As Integer
			Dim iIndiceItemPrecUni As Integer
			Dim arrCentroSM() As String
			Dim iIndiceCentroSM As Integer
			Dim arrPartida() As String
			Dim iIndicePartida As Integer
			Dim arrItemFecEntrega() As Date
			Dim arrArticulos() As String
			Dim arrCodProveErp() As String
			Dim iIndiceAtributos As Integer
			Dim sOrgCompras As String
			'arrays atributos linea
			Dim arrAtribLinea() As Integer
			Dim arrAtribLineaId() As Integer
			Dim arrAtribLineaValor() As String
			'arrays atributos cabecera
			Dim arrAtribOrdenId() As Integer
			Dim arrAtribOrdenValor() As String
			'arrays de presupuestos GS
			Dim arrPres1() As String
			Dim arrPres2() As String
			Dim arrPres3() As String
			Dim arrPres4() As String
			'array de centros de aprovisionamiento
			Dim arrCentroAprov() As String

			ReDim arrCentroAprov(0)
			ReDim arrPres1(0)
			ReDim arrPres2(0)
			ReDim arrPres3(0)
			ReDim arrPres4(0)
			ReDim arrCodProveErp(0)
			ReDim arrItemPrecUni(0)
			ReDim arrCentroSM(0)
			ReDim arrPartida(0)
			ReDim arrItemFecEntrega(0)

			iIndiceItemPrecUni = -1
			iIndiceCentroSM = -1
			iIndicePartida = -1
			iIndiceAtributos = -1

			sOrgCompras = cEmisionPedido.DatosGenerales.OrgCompras

			Dim sMiPartida As String = String.Empty

			For Each oLinea As CLineaPedido In cEmisionPedido.Lineas
				If oLinea.Est <> 99 Then
					If oLinea.LineaCatalogo > 0 And ((cEmisionPedido.DatosGenerales.Tipo.Almacenable = cTipoPedido.EnAlmacenTipo.NoAlmacenable AndAlso oLinea.TipoArticulo.TipoAlmacenamiento = CArticulo.EnAlmacenTipo.Obligatorio) OrElse (cEmisionPedido.DatosGenerales.Tipo.Almacenable = cTipoPedido.EnAlmacenTipo.Obligatorio AndAlso oLinea.TipoArticulo.TipoAlmacenamiento = CArticulo.EnAlmacenTipo.NoAlmacenable) OrElse (cEmisionPedido.DatosGenerales.Tipo.Concepto = cTipoPedido.EnInvTipo.Gasto AndAlso oLinea.TipoArticulo.Concepto = CArticulo.EnInvTipo.Inversion) OrElse (cEmisionPedido.DatosGenerales.Tipo.Concepto = cTipoPedido.EnInvTipo.Inversion AndAlso oLinea.TipoArticulo.Concepto = CArticulo.EnInvTipo.Gasto) OrElse (cEmisionPedido.DatosGenerales.Tipo.Recepcionable = cTipoPedido.EnRecTipo.NoRecepcionable AndAlso oLinea.TipoArticulo.TipoRecepcion = CArticulo.EnRecTipo.Obligatorio) OrElse (cEmisionPedido.DatosGenerales.Tipo.Recepcionable = cTipoPedido.EnRecTipo.Obligatorio AndAlso oLinea.TipoArticulo.TipoRecepcion = CArticulo.EnRecTipo.NoRecepcionable)) Then
						'Si ocurre esto quiere decir que esta línea no puede ser incluida por que el tipo de pedido donde está incluida no lo permite
						'No sumamos el importe de la línea a la orden
					Else
						Importe = Importe + CDbl(oLinea.PrecUc * oLinea.CantPed * oLinea.FC)

						sMiPartida = ""
						If oLinea.Pres5_ImportesImputados_EP(0).Partida IsNot Nothing _
						AndAlso oLinea.Pres5_ImportesImputados_EP(0).Partida.PresupuestoId > 0 Then
							sMiPartida = oLinea.Pres5_ImportesImputados_EP(0).Partida.ToString()
						End If

						_mipargenSM = Nothing

						If _mipargenSMs IsNot Nothing AndAlso sMiPartida <> "" Then
							For Each oParSm As PargenSM In _mipargenSMs
								If oParSm.Pres5 = Split(sMiPartida, "#")(0) Then
									_mipargenSM = oParSm
								End If
							Next
						End If

						If FSNServer.TipoAcceso.gbAccesoFSSM And _mipargenSM IsNot Nothing AndAlso _mipargenSM.ImputacionPedido <> Fullstep.FSNServer.PargenSM.EnumTipoImputacion.NoSeImputa Then
							If oLinea.Pres5_ImportesImputados_EP(0).CentroCoste IsNot Nothing _
							AndAlso oLinea.Pres5_ImportesImputados_EP(0).CentroCoste.CentroSM IsNot Nothing _
							AndAlso Not String.IsNullOrEmpty(oLinea.Pres5_ImportesImputados_EP(0).CentroCoste.CentroSM.Codigo) Then

								'Metemos el centro SM del item en su array de validaciones
								ReDim Preserve arrCentroSM(iIndiceCentroSM + 1)
								arrCentroSM(iIndiceCentroSM + 1) = oLinea.Pres5_ImportesImputados_EP(0).CentroCoste.CentroSM.Codigo
								iIndiceCentroSM = iIndiceCentroSM + 1
								If oLinea.Pres5_ImportesImputados_EP(0).Partida IsNot Nothing _
									AndAlso oLinea.Pres5_ImportesImputados_EP(0).Partida.PresupuestoId > 0 Then
									'Metemos la partida presup. del item en su array de validaciones
									ReDim Preserve arrPartida(iIndicePartida + 1)
									arrPartida(iIndicePartida + 1) = oLinea.Pres5_ImportesImputados_EP(0).Partida.ToString()
									iIndicePartida = iIndicePartida + 1
								End If
							End If
						End If
						oLinea.Den = oLinea.DescripcionVisible 'cambio para emitir favoritos y poder cambiar la denominacion
					End If

					'Metemos la fecha de entrega del item en su array de validaciones
					ReDim Preserve arrItemFecEntrega(iIndiceItemPrecUni + 1)
					arrItemFecEntrega(iIndiceItemPrecUni + 1) = oLinea.FecEntrega
					ReDim Preserve arrArticulos(iIndiceItemPrecUni + 1) 'Array de los articulos
					arrArticulos(iIndiceItemPrecUni + 1) = oLinea.Cod
					''ARRAYS ATRIBUTOS DE LAS LINEAS
					For Each oLineaAtrib As CAtributoPedido In oLinea.OtrosDatos
						ReDim Preserve arrAtribLinea(iIndiceAtributos + 1) 'Array de los id de las lineas
						arrAtribLinea(iIndiceAtributos + 1) = oLinea.ID
						ReDim Preserve arrAtribLineaId(iIndiceAtributos + 1) 'Array de los id de los atributos de las lineas
						arrAtribLineaId(iIndiceAtributos + 1) = oLineaAtrib.Id
						ReDim Preserve arrAtribLineaValor(iIndiceAtributos + 1) 'Array de los valores de los atributos de las lineas
						arrAtribLineaValor(iIndiceAtributos + 1) = oLineaAtrib.Valor
						iIndiceAtributos = iIndiceAtributos + 1
					Next
					'Metemos el precio unitario del item en su array de validaciones
					ReDim Preserve arrItemPrecUni(iIndiceItemPrecUni + 1)
					If oLinea.TipoRecepcion = 0 Then
						arrItemPrecUni(iIndiceItemPrecUni + 1) = oLinea.PrecUc
					Else
						arrItemPrecUni(iIndiceItemPrecUni + 1) = oLinea.ImportePedido
					End If
					ReDim Preserve arrItemCant(iIndiceItemPrecUni + 1)
					arrItemCant(iIndiceItemPrecUni + 1) = oLinea.CantPed
					ReDim Preserve arrCat1Lin(iIndiceItemPrecUni + 1)
					arrCat1Lin(iIndiceItemPrecUni + 1) = oLinea.Cat1

					'Centros de aprovisionamiento y prosupuestos GS
					ReDim Preserve arrCentroAprov(iIndiceItemPrecUni + 1)
					arrCentroAprov(iIndiceItemPrecUni + 1) = oLinea.CentroAprovisionamiento
					ReDim Preserve arrPres1(iIndiceItemPrecUni + 1)
					arrPres1(iIndiceItemPrecUni + 1) = oLinea.Pres1
					ReDim Preserve arrPres2(iIndiceItemPrecUni + 1)
					arrPres2(iIndiceItemPrecUni + 1) = oLinea.Pres2
					ReDim Preserve arrPres3(iIndiceItemPrecUni + 1)
					arrPres3(iIndiceItemPrecUni + 1) = oLinea.Pres3
					ReDim Preserve arrPres4(iIndiceItemPrecUni + 1)
					arrPres4(iIndiceItemPrecUni + 1) = oLinea.Pres4

					iIndiceItemPrecUni = iIndiceItemPrecUni + 1
				End If
			Next

			iIndiceAtributos = -1
			''ARRAYS ATRIBUTOS DE LA CABECERA
			For Each oCabeceraAtrib As CAtributoPedido In cEmisionPedido.OtrosDatos
				ReDim Preserve arrAtribOrdenId(iIndiceAtributos + 1) 'Array de los id de los atributos de las lineas
				arrAtribOrdenId(iIndiceAtributos + 1) = oCabeceraAtrib.Id
				ReDim Preserve arrAtribOrdenValor(iIndiceAtributos + 1) 'Array de los valores de los atributos de las lineas
				arrAtribOrdenValor(iIndiceAtributos + 1) = oCabeceraAtrib.Valor
				iIndiceAtributos = iIndiceAtributos + 1
			Next

			If FSNServer.TipoAcceso.gbAccesoFSSM AndAlso Not _mipargenSM Is Nothing Then
				If _mipargenSM.Vigencia <> TipoVigencia.NoAplica Then
					Dim bHayPresupNoVigentes As Boolean = False
					For Each oLinea As CLineaPedido In cEmisionPedido.Lineas
						If oLinea.Est <> 99 Then
							If (oLinea.Pres5_ImportesImputados_EP(0).Partida.FechaInicioPresupuesto < Now() Or Not oLinea.Pres5_ImportesImputados_EP(0).Partida.FechaInicioPresupuesto.HasValue) _
								And (oLinea.Pres5_ImportesImputados_EP(0).Partida.FechaFinPresupuesto > Now() Or Not oLinea.Pres5_ImportesImputados_EP(0).Partida.FechaFinPresupuesto.HasValue) Then
							Else
								bHayPresupNoVigentes = True
								Exit For
							End If
						End If
					Next
					If bHayPresupNoVigentes Then
						If _mipargenSM.Vigencia = TipoVigencia.Avisar Then
							'le dejo continuar si esta mal, pero le aviso
							Return serializer.Serialize(2)
						Else
							'no le dejo continuar
							Return serializer.Serialize(3)
						End If
					End If
				End If
			End If

			Dim bHayPartida As Boolean = False
			Dim sPartida As String = String.Empty
			Dim sGestorCod() As String = Nothing
			Dim i As Integer = 0
			For Each oLinea As CLineaPedido In cEmisionPedido.Lineas
				sPartida = oLinea.Pres5_ImportesImputados_EP(0).Partida.ToString()
				If sPartida <> String.Empty Then
					Dim sGestorLinea As String = seleccionaGestorWM(sPartida)
					If sGestorLinea <> String.Empty Then
						If sGestorCod Is Nothing OrElse Array.IndexOf(sGestorCod, seleccionaGestorWM(sPartida)) = -1 Then
							ReDim Preserve sGestorCod(i)
							sGestorCod(i) = seleccionaGestorWM(sPartida)
							i += 1
						End If
					End If
					bHayPartida = True
					'Si hay partida actualizamos comprometido/solicitado
					If bHayPartida Then
						If FSNServer.TipoAcceso.gbAccesoFSSM Then
							If _mipargenSMs.Count > 0 Then
								For Each oParSm As PargenSM In _mipargenSMs
									If oParSm.Pres5 = Split(sPartida, "#")(0) Then
										_mipargenSM = oParSm
									End If
								Next
							Else
								_mipargenSM = Nothing
							End If
						Else
							_mipargenSM = Nothing
						End If

						If Not _mipargenSM Is Nothing Then
							Dim oPRES5 As FSNServer.Partida
							oPRES5 = FSNServer.Get_Object(GetType(FSNServer.Partida))
							If _mipargenSM.Plurianual Then
								If oLinea.PlanEntrega.Count > 0 Then
									For Each iPlanEntrega As PlanEntrega In oLinea.PlanEntrega
										oPRES5.ActualizarImportesPartida(Split(sPartida, "#")(0), Split(sPartida, "#")(1),
													If(Split(sPartida, "#").Length > 2, Split(sPartida, "#")(2), ""),
													If(Split(sPartida, "#").Length > 3, Split(sPartida, "#")(3), ""),
													If(Split(sPartida, "#").Length > 4, Split(sPartida, "#")(4), ""),
													False, oLinea.Solicit, cEmisionPedido.DatosGenerales.Cambio, 0, FSNUser.Idioma.ToString, False,
													iPlanEntrega.CantidadEntrega,
													(iPlanEntrega.ImporteEntrega / cEmisionPedido.DatosGenerales.Cambio),
													oLinea.Campo_Solicit, oLinea.Linea_Solicit,,
													Year(iPlanEntrega.FechaEntrega), oLinea.ID)
									Next
								Else
									oPRES5.ActualizarImportesPartida(Split(sPartida, "#")(0), Split(sPartida, "#")(1),
													   If(Split(sPartida, "#").Length > 2, Split(sPartida, "#")(2), ""),
													   If(Split(sPartida, "#").Length > 3, Split(sPartida, "#")(3), ""),
													   If(Split(sPartida, "#").Length > 4, Split(sPartida, "#")(4), ""),
													   False, oLinea.Solicit, cEmisionPedido.DatosGenerales.Cambio, 0, FSNUser.Idioma.ToString, False,
													   oLinea.CantPed, (oLinea.ImportePedido / cEmisionPedido.DatosGenerales.Cambio), oLinea.Campo_Solicit,
													   oLinea.Linea_Solicit,, Year(oLinea.FecEntrega), oLinea.ID)
								End If
							Else
								oPRES5.ActualizarImportesPartida(Split(sPartida, "#")(0), Split(sPartida, "#")(1),
														 If(Split(sPartida, "#").Length > 2, Split(sPartida, "#")(2), ""),
														 If(Split(sPartida, "#").Length > 3, Split(sPartida, "#")(3), ""),
														 If(Split(sPartida, "#").Length > 4, Split(sPartida, "#")(4), ""),
														 False, oLinea.Solicit, cEmisionPedido.DatosGenerales.Cambio, 0, FSNUser.Idioma.ToString, False,
														 oLinea.CantPed, (oLinea.ImportePedido / cEmisionPedido.DatosGenerales.Cambio), oLinea.Campo_Solicit,
														 oLinea.Linea_Solicit)
							End If
						End If
					End If
				End If
			Next
			If bHayPartida Then
				'En el array sGestorCod tenemos los gestores que hay en las partidas seleccionadas en las líneas
				'1. Miramos si el gestor seleccionado en la orden está en la lista: 
				'       Si no está: Cogemos el primero que haya en el array y lo ponemos como gestor de la orden
				'2. Si hay más de un gestor en el array, actualizamos el error
				'   Si sólo hay uno, borramos el error que pudise haber
				If sGestorCod IsNot Nothing AndAlso sGestorCod.Length > 0 Then
					If sGestorCod.Length > 1 Then
						'Actualizar error
						Return serializer.Serialize(4)
					End If
				End If
			End If

			arrCodProveErp(0) = cEmisionPedido.DatosGenerales.CodErp

			If bValidacionIntegracion Then
				Dim MapperStr As String
				Dim iNumError As Short
				Dim strError As String = ""
				Dim oEP_ValidacionesIntegracion As EP_ValidacionesIntegracion = FSNServer.Get_Object(GetType(EP_ValidacionesIntegracion))
				If Not oEP_ValidacionesIntegracion.HayIntegracionSalidaWCF(TablasIntegracion.PED_Aprov, cEmisionPedido.DatosGenerales.Emp) Then
					If Not oEP_ValidacionesIntegracion.EvalMapper(iNumError, strError, cEmisionPedido.DatosGenerales.Emp, cEmisionPedido.DatosGenerales.Tipo.Codigo, arrCentroSM,
																  arrPartida, arrItemPrecUni, arrItemFecEntrega, arrItemCant, arrCat1Lin, cEmisionPedido.DatosGenerales.Moneda,
																  cEmisionPedido.DatosGenerales.Cambio, arrArticulos, arrAtribLinea, arrAtribLineaId, arrAtribLineaValor, arrAtribOrdenId,
																  arrAtribOrdenValor, arrCodProveErp, oPer.Cod, sOrgCompras, arrCentroAprov, arrPres1, arrPres2, arrPres3, arrPres4, CShort(cEmisionPedido.DatosGenerales.TipoPedido)) Then
						Dim oErrorMapper As New Exception(strError)
						oErrorMapper.Source = CStr(iNumError)
						iNumError = CInt(oErrorMapper.Source)
						If iNumError = -100 Then
							MapperStr = oErrorMapper.Message
						Else
							Dim oTextosIntegracion As TextosIntegracion = FSNServer.Get_Object(GetType(TextosIntegracion))
							MapperStr = oTextosIntegracion.MensajeIntegracionError(MapperModuloMensaje.PedidoDirecto _
									, iNumError, FSNUser.Idioma, oErrorMapper.Message)
						End If

						Return serializer.Serialize(MapperStr)
					End If
				Else
					Dim Est As Integer = TipoEstadoOrdenEntrega.EmitidoAlProveedor
					If Not oEP_ValidacionesIntegracion.ValidacionesWCF_EP(strError, cEmisionPedido, FSNUser.Idioma, Est) Then
						MapperStr = strError
						Return serializer.Serialize(MapperStr)
						Exit Function
					End If
				End If

			End If
			Dim dsSolicitudesDePedido As DataSet
			Dim ped_num As Long
			Dim oe_num As Long
			Dim idOrden As Long
			Dim EstadoOrden As Short
			Dim oRespuesta As New JsonConfirmacionPedido

			cEmisionPedidos.EmitirPedido(FSNUser.CodPersona, FSNUser.Idioma, oPer.Cod, cEmisionPedido.DatosGenerales.Moneda, cEmisionPedido, oPargenSMS, FSNServer.TipoAcceso, _mipargenSM, idPedido, dsSolicitudesDePedido, dImporte,
											ped_num, oe_num, EstadoOrden, FSNUser.Email, iNotificar, cEmisionPedido.DatosGenerales.CodErp, idOrden, EsPedidoAbierto)
			oRespuesta.moneda = cEmisionPedido.DatosGenerales.Moneda
			oRespuesta.prove = cEmisionPedido.DatosGenerales.CodProve
			oRespuesta.ped_num = ped_num
			oRespuesta.oe_num = oe_num
			oRespuesta.anyo = Now.Year
			oRespuesta.respuesta = True
			oRespuesta.estado = EstadoOrden
			oRespuesta.importe = CDbl(dImporte)
			GenerarWorkFlowSolicitudPedido(dsSolicitudesDePedido, FSNUser.CodPersona, FSNUser.Idioma, cEmisionPedido.DatosGenerales.Emp, idPedido, cEmisionPedido.DatosGenerales.Moneda, EsPedidoAbierto)
			If EstadoOrden = 2 Then
				If FSNServer.TipoAcceso.gbAccesoFSSM AndAlso Not _mipargenSM Is Nothing Then
					'Sumamos el importe comprometido del SM
					Dim obDetallePedido As cDetallePedidos = FSNServer.Get_Object(GetType(cDetallePedidos))
					obDetallePedido.ActualizarImputacion(idOrden)
				End If

				Dim dr As DataRowCollection = cEmisionPedidos.TrasCrearOrdenEntregaLineaActualizarCatalogo(0, idPedido, FSNUser.CodPersona, FSNUser.ModificarPrecios)

				If Not dr Is Nothing Then
					For Each fila As DataRow In dr
						ActualizarPrecArticulo(fila.Item("ID"), fila.Item("PRECUC"))
					Next
				End If

			End If

			Dim oIntegracion As FSNServer.Integracion
			'Llamada a FSIS.
			oIntegracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))
			oIntegracion.LlamarFSIS(TablasIntegracion.PED_Aprov, idOrden)

			Return serializer.Serialize(oRespuesta)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>Comprueba si hay que realizar la comunicación del pedido en función de la configuración</summary>    
	''' <param name="oEmisionPedido">Objeto Pedido</param>
	''' <param name="Acceso">Parámetros generales</param>
	''' <returns>0: No comunicar, 1: Comunicar sin preguntar, 2: Preguntar si comunicar el emitir, 3: Preguntar si comunicar al integrar</returns>
	''' <remarks>Llamada desde: cmdEmitir_Click</remarks>
	<System.Web.Services.WebMethod(True), System.Web.Script.Services.ScriptMethod()>
	Shared Function ComprobarComunicacion(ByVal Empresa As Integer) As Short
		Dim iComunicar As Short

		Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

		Dim serializer As New JavaScriptSerializer()
		Dim iEmpresa As Integer = serializer.Deserialize(Of Integer)(Empresa)

		Select Case FSNServer.TipoAcceso.giPedEmail
			Case ComunicacionPedido.NoActivo
				iComunicar = 0
			Case ComunicacionPedido.Activo
				Select Case FSNServer.TipoAcceso.giPedEmailModo
					Case TipoComunicacionPedido.ComAuto
						iComunicar = 1
					Case TipoComunicacionPedido.ComManual
						iComunicar = 0
					Case TipoComunicacionPedido.ComPreguntar
						iComunicar = 2
				End Select
			Case ComunicacionPedido.ConPedidoIntegrado
				'Si el ERP de la empresa del pedido no tiene integración de pedidos de salida se actúa como en el caso de sin integración

				'Para mirar el estado de integración de los pedidos en el erp me vale cualquier pedido porque todos tienen la misma empresa                
				Dim bHayIntPed As Boolean = Comprobar_Integracion(iEmpresa)

				If bHayIntPed Then
					Select Case FSNServer.TipoAcceso.giPedEmailModo
						Case TipoComunicacionPedido.ComAuto
							iComunicar = 3
						Case TipoComunicacionPedido.ComManual
							iComunicar = 0
						Case TipoComunicacionPedido.ComPreguntar
							iComunicar = 4
					End Select
				Else
					Select Case FSNServer.TipoAcceso.giPedEmailModo
						Case TipoComunicacionPedido.ComAuto
							iComunicar = 1
						Case TipoComunicacionPedido.ComManual
							iComunicar = 0
						Case TipoComunicacionPedido.ComPreguntar
							iComunicar = 2
					End Select
				End If
		End Select

		Return serializer.Serialize(iComunicar)
	End Function
	''' Genera los xml para los flujos de las solicitudes de pedido y llama al Web service
	''' </summary>
	''' <param name="dsSolicitudes">Dataset con las solicitudes a generar</param>
	''' <param name="UsuCod">usuario aprovisionador</param>
	''' <param name="Idioma">idioma del usuario</param>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub GenerarWorkFlowSolicitudPedido(ByVal dsSolicitudes As DataSet, ByVal CodPer As String, ByVal Idioma As String, ByVal idEmp As Long, ByVal idPedido As Long, ByVal sMon As String, ByVal EsPedidoAbierto As Boolean)
		Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
		Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
		Dim oSolicitud As Solicitud
		Dim oInstancia As Instancia
		Dim oEmisionPedidos As CEmisionPedidos
		Dim YaRemoveCache As Boolean = False

		Try
			oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
			oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))

			For Each dr As DataRow In dsSolicitudes.Tables(1).Rows
				Dim BloqueOrigen As Long
				Dim BloqueDestino As Long
				Dim Accion As Long
				oEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))
				oSolicitud.ID = dr("SOLICITUD")
				oSolicitud.Load(Idioma)
				oInstancia.Peticionario = CodPer
				oInstancia.Solicitud = oSolicitud

				oInstancia.Create_Prev(idEmp, sMon)
				If oInstancia.ID > 0 Then
					'Ponemos la instancia en proceso
					oInstancia.Actualizar_En_proceso(1)

					Dim lIDTiempoProc As Long
					oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, FSNUser.Cod, BloqueOrigen)

					'Solo se actualiza si es aqui donde se crea la instancia y las lineas de pedido no tienen aun ,el id de la instancia(LINEAS_PEDIDO.INSTANCIA_APROB)    
					For Each drCat As DataRow In dsSolicitudes.Tables(0).Rows
						If oInstancia.Solicitud.ID = drCat("SOLICITUD") Then
							oEmisionPedidos.ActualizarLineasPedido_InstanciaSolicitudPedido(drCat, idPedido, oInstancia.ID, EsPedidoAbierto)
						End If
					Next

					Dim ds As DataSet
					ds = oEmisionPedidos.ObtenerBloque_Accion(oSolicitud.ID)
					If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
						BloqueOrigen = ds.Tables(0)(0)("BLOQUE_ORIGEN")
						BloqueDestino = ds.Tables(0)(0)("BLOQUE_DESTINO")
						Accion = ds.Tables(0)(0)("ACCION")
					End If

					oInstancia.DevolverEtapaActual(FSNUser.Idioma.ToString, FSNUser.CodPersona, BloqueOrigen)

					Dim sXMLName As String
					Dim dsXML As New DataSet
					dsXML.Tables.Add("SOLICITUD")
					With dsXML.Tables("SOLICITUD").Columns
						.Add("TIPO_PROCESAMIENTO_XML")
						.Add("TIPO_DE_SOLICITUD")
						.Add("COMPLETO")
						.Add("CODIGOUSUARIO")
						.Add("INSTANCIA")           '1
						.Add("SOLICITUD")           '2
						.Add("USUARIO")             '3
						.Add("USUARIO_EMAIL")       '4
						.Add("USUARIO_IDIOMA")      '5
						.Add("PEDIDO_DIRECTO")      '6
						.Add("FORMULARIO")          '7
						.Add("WORKFLOW")            '8
						.Add("IMPORTE", System.Type.GetType("System.Double")) '9
						.Add("COMENTALTANOCONF")    '10
						.Add("ACCION")              '11
						.Add("IDACCION")            '12
						.Add("IDACCIONFORM")        '13
						.Add("GUARDAR")             '14
						.Add("NOTIFICAR")           '15
						.Add("TRASLADO_USUARIO")    '16
						.Add("TRASLADO_PROVEEDOR")  '17
						.Add("TRASLADO_FECHA")      '18
						.Add("TRASLADO_COMENTARIO") '19
						.Add("TRASLADO_PROVEEDOR_CONTACTO") '20
						.Add("DEVOLUCION_COMENTARIO")       '21
						.Add("COMENTARIO")          '22
						.Add("BLOQUE_ORIGEN")       '23
						.Add("NUEVO_ID_INSTANCIA")  '24
						.Add("BLOQUES_DESTINO")     '25
						.Add("ROL_ACTUAL")          '26
						.Add("PEDIDO")          '31
						.Add("IDTIEMPOPROC")
					End With
					Dim drSolicitud As DataRow
					drSolicitud = dsXML.Tables("SOLICITUD").NewRow
					With drSolicitud
						.Item("TIPO_PROCESAMIENTO_XML") = CInt(TipoProcesamientoXML.FSNWeb)
						.Item("TIPO_DE_SOLICITUD") = IIf(EsPedidoAbierto, CInt(TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoContraAbierto), CInt(TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoCatalogo)) 'Solicitud de pedido
						.Item("COMPLETO") = 1
						.Item("CODIGOUSUARIO") = FSNUser.Cod
						.Item("INSTANCIA") = oInstancia.ID
						.Item("SOLICITUD") = oInstancia.Solicitud.ID 'Si es la creacion de la instancia, ponemos la solicitud para que haga el instancia.create
						.Item("USUARIO") = FSNUser.CodPersona
						.Item("USUARIO_EMAIL") = DBNullToStr(FSNUser.Email)
						.Item("USUARIO_IDIOMA") = FSNUser.Idioma.ToString()
						.Item("PEDIDO_DIRECTO") = 0
						.Item("FORMULARIO") = oSolicitud.Formulario.Id
						.Item("WORKFLOW") = oSolicitud.Workflow
						.Item("IMPORTE") = dr("IMPORTE")
						.Item("IDACCION") = Accion
						.Item("IDACCIONFORM") = Accion
						.Item("GUARDAR") = 1
						.Item("NOTIFICAR") = 1
						.Item("BLOQUE_ORIGEN") = BloqueOrigen
						.Item("BLOQUES_DESTINO") = BloqueDestino
						.Item("ROL_ACTUAL") = oInstancia.RolActual
						.Item("PEDIDO") = IIf(EsPedidoAbierto, CInt(TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoContraAbierto), CInt(TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoCatalogo)) 'Solicitud de pedido
						.Item("IDTIEMPOPROC") = lIDTiempoProc
					End With
					dsXML.Tables("SOLICITUD").Rows.Add(drSolicitud)

					sXMLName = FSNUser.Cod & "#" & oInstancia.ID & "#" & BloqueOrigen

					'Cargar XML completo
					oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=TiempoProcesamiento.InicioDisco)
					If ConfigurationManager.AppSettings("SERVIDOR_TRATAMIENTO_INSTANCIAS") = "0" Then
						'Se hace con un StringWriter porque el método GetXml del dataset no incluye el esquema
						Dim oSW As New System.IO.StringWriter()
						dsXML.WriteXml(oSW, XmlWriteMode.WriteSchema)

						Dim oSrvTrataInst As New FSNWebServiceXML.TratamientoInstancias
						oSrvTrataInst.TransferirXMLEnEpera(oSW.ToString(), sXMLName)
					Else
						dsXML.WriteXml(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & "#FSNWEB.xml", XmlWriteMode.WriteSchema)
						If File.Exists(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml") Then _
							File.Delete(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml")
						FileSystem.Rename(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & "#FSNWEB.xml",
										  ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml")
					End If

					If Not YaRemoveCache Then
						YaRemoveCache = True

						Dim PeticUser As FSNServer.User = FSNServer.Get_Object(GetType(FSNServer.User))
						PeticUser.LoadUserData(oInstancia.Peticionario)

						If PeticUser.ModificarPrecios Then
							HttpContext.Current.Cache.Remove("DsetArticulos_" & FSNUser.CodPersona)
						End If
					End If

				End If
			Next
		Catch ex As Exception
			Throw ex
		End Try
	End Sub
	''' <summary>
	''' Guardar Pedido
	''' </summary>
	''' <param name="oLinea">Objeto Linea</param>
	''' <param name="oCentroCoste">Objeto Centro Coste</param>
	''' <param name="oPartidas">Objeto Partidas</param>
	''' <param name="iCestaCabeceraId">iCestaCabeceraId</param>
	''' <returns>Atributos</returns>
	''' <remarks>Llamada desde: TraspasoAdjudicaciones.aspx; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub GuardarLineasPedido(ByVal iDesdeFavorito As Integer, ByVal oLinea As String, ByVal oCentroCoste As String, ByVal oPartidas As String, ByVal iCestaCabeceraId As Integer, ByVal bHayIntegracion As Boolean, ByVal idEmpresa As Integer)

		Try
			Dim serializer As New JavaScriptSerializer()
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")

			Dim oPer As FSNServer.CPersona
			oPer = FSNServer.Get_Object(GetType(FSNServer.CPersona))
			oPer.Cod = FSNUser.CodPersona
			oPer.CargarDestinos(FSNUser.Idioma)

			Dim linea As CLineaPedido = serializer.Deserialize(Of CLineaPedido)(oLinea)
			Dim UON As List(Of UON) = serializer.Deserialize(Of List(Of UON))(oCentroCoste)
			Dim PartidaPres5 As List(Of PartidaPRES5) = serializer.Deserialize(Of List(Of PartidaPRES5))(oPartidas)
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			cEmisionPedidos.ActualizarLineaPedido(iDesdeFavorito, FSNUser.Cod, FSNUser.Idioma, oPer.Cod, linea, UON, PartidaPres5, iCestaCabeceraId, bHayIntegracion, idEmpresa)

		Catch ex As Exception
			Throw ex
		End Try
	End Sub
	''' <summary>
	''' Borrar lineas
	''' </summary>
	''' <param name="idCabecera">idCabecera</param>
	''' <param name="lineas">lineas a borrar</param>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub BorrarLineas(ByVal iDesdeFavorito As Integer, ByVal idCabecera As Integer, ByVal lineas As String)
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")

			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			Dim sLineas() As String = Split(lineas, ",")
			Dim sLinea As String
			For Each sLinea In sLineas
				cEmisionPedidos.BorrarLinea(iDesdeFavorito, Int32.Parse(sLinea), idCabecera, FSNUser.Cod)
			Next
		Catch ex As Exception
			Throw ex
		End Try
	End Sub
	''' <summary>
	''' Selecciona el gestor
	''' </summary>
	''' <param name="Partida"></param>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function seleccionaGestorWM(ByVal Partida As String) As String
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			If Partida <> String.Empty And Partida <> "null" Then
				Dim sPRES() As String = Partida.Split("#")
				Dim sPRES0 As String = String.Empty
				Dim sPRES1 As String = String.Empty
				Dim sPRES2 As String = String.Empty
				Dim sPRES3 As String = String.Empty
				Dim sPRES4 As String = String.Empty
				If (sPRES(0) <> "null") Then sPRES0 = sPRES(0)
				If (sPRES(1) <> "null") Then sPRES1 = sPRES(1)
				If sPRES.Length > 2 Then If (sPRES(2) <> "null") Then sPRES2 = sPRES(2)
				If sPRES.Length > 3 Then If (sPRES(3) <> "null") Then sPRES3 = sPRES(3)
				If sPRES.Length > 4 Then If (sPRES(4) <> "null") Then sPRES4 = sPRES(4)
				Dim sGestor As String = String.Empty
				If sPRES0 <> String.Empty And sPRES1 <> String.Empty Then
					Dim oPartidas As PartidasPRES5 = FSNServer.Get_Object(GetType(PartidasPRES5))
					oPartidas.ObtGestoresPartidasUsu(FSNUser.CodPersona, sPRES0)
					If oPartidas.Count <> 0 Then
						sGestor = oPartidas.Find(Function(oParPres As FSNServer.PartidaPRES5) oParPres.NIV0 = sPRES0 AndAlso oParPres.NIV1 = sPRES1 AndAlso oParPres.NIV2 = sPRES2 AndAlso oParPres.NIV3 = sPRES3 AndAlso oParPres.NIV4 = sPRES4).GestorCod + " - " + oPartidas.Find(Function(oParPres As FSNServer.PartidaPRES5) oParPres.NIV0 = sPRES0 AndAlso oParPres.NIV1 = sPRES1 AndAlso oParPres.NIV2 = sPRES2 AndAlso oParPres.NIV3 = sPRES3 AndAlso oParPres.NIV4 = sPRES4).GestorDen '(Function(oParPres As FSNServer.PartidaPRES5) oParPres.NIV0 = sPRES0).GestorCod '+ " - " + oPartidas.Find(Function(oParPres As FSNServer.PartidaPRES5) oParPres.NIV0 = sPRES0).GestorDen
					End If

				End If
				If sGestor = " - " Or sGestor = String.Empty Then
					Return String.Empty
				End If
				Return sGestor
			Else
				Return String.Empty
			End If
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Carga el panel de las unidades para la linea
	''' </summary>
	''' <param name="idLinea"></param>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function CargarPanelUnidades(ByVal idLinea As Integer) As String ', ByVal sUp As String) As String
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim oUnis As CUnidadesPedido = FSNServer.Get_Object(GetType(FSNServer.CUnidadesPedido))

			oUnis.DevolverUnidadesPedido(FSNUser.Idioma, idLinea)

			Return serializer.Serialize(oUnis)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Valida el receptor seleccionado
	''' </summary>
	''' <param name="idOrden"></param>
	''' <param name="sReceptor"></param>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function ValidarReceptor(ByVal idOrden As Integer, ByVal sReceptor As String) As String
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim sFilas As String
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim oUnis As CUnidadesPedido = FSNServer.Get_Object(GetType(FSNServer.CUnidadesPedido))

			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			sFilas = cEmisionPedidos.ValidarReceptor(idOrden, sReceptor)

			Return sFilas
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' ComprobarCantidades
	''' </summary>
	''' <param name="IDLinea">IDLinea</param>
	''' <returns>Atributos</returns>
	''' <remarks>; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function ComprobarCantidades(ByVal idLinea As Integer, ByVal sUPed As String, ByVal dCant As Double, ByVal sPedido As String) As Object
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim cEmisionPedido As CEmisionPedido = serializer.Deserialize(Of CEmisionPedido)(sPedido)
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim iValidacionCantidad As Integer = 0
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			iValidacionCantidad = cEmisionPedidos.ComprobarCantidades(idLinea, sUPed, dCant, cEmisionPedido.Lineas)

			Return serializer.Serialize(iValidacionCantidad)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Comprobar Importe Bruto
	''' </summary>
	''' <param name="IDLinea">IDLinea</param>
	''' <returns>Atributos</returns>
	''' <remarks>; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function ComprobarImporteBruto(ByVal idLinea As Integer, ByVal sUPed As String, ByVal dImporteBruto As Double, ByVal sPedido As String) As Object
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim cEmisionPedido As CEmisionPedido = serializer.Deserialize(Of CEmisionPedido)(sPedido)
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim iValidacionImporteBruto As Integer = 0
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			iValidacionImporteBruto = cEmisionPedidos.ComprobarImporteBruto(idLinea, sUPed, dImporteBruto, cEmisionPedido.Lineas)

			Return serializer.Serialize(iValidacionImporteBruto)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Comprobar_Integracion
	''' </summary>
	''' <param name="iEmpresa">iEmpresa</param>
	''' <returns>Atributos</returns>
	''' <remarks>; Tiempo mÃ¡ximo:0</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Comprobar_Integracion(ByVal iEmpresa As Integer) As Object
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim bIntegracion As Boolean = False
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))
			If iEmpresa <> 0 Then
				bIntegracion = cEmisionPedidos.Comprobar_Integracion(iEmpresa)
			End If
			Return serializer.Serialize(bIntegracion)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>Guardamos en caché el id del Activo seleccionado</summary>
	''' <param name="idActivo">Id del activo seleccionado</param>
	''' <remarks>Llamada desde el javascript de EPWeb\EmisionPedido.aspx contenido en el archivo SMWebControls\SMControls.js. 
	''' Tiempo Máx: 0,1 seg. 
	''' Se hace uso de la variable de session creada en: SMWebControls\SMActivos\_btnAceptar_Click</remarks>
	<System.Web.Services.WebMethodAttribute(),
		System.Web.Script.Services.ScriptMethodAttribute()>
	Public Shared Sub guardarActivoSeleccionado(ByVal idActivo As String)
		HttpContext.Current.Session("idActivo") = idActivo
	End Sub
	''' <summary>
	''' Elimina el pedido favorito
	''' </summary>
	''' <param name="idFav">idFav</param>
	''' <param name="codUsu">codUsu</param>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub EliminarFavorito(ByVal idFav As Integer)
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim oOrdenFavorito As FSNServer.COrdenFavoritos = FSNServer.Get_Object(GetType(FSNServer.COrdenFavoritos))
			oOrdenFavorito.ID = idFav
			oOrdenFavorito.Usuario = FSNUser.Cod
			oOrdenFavorito.EliminarFavorito()
		Catch ex As Exception
			Throw ex
		End Try
	End Sub
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub CambiarCantidadLinea(ByVal idPedido As Integer, ByVal idLinea As Integer, ByVal cantidad As Double)
		Try
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim ds As DataSet = HttpContext.Current.Cache("DsetEmisionPedidos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString() & "_" & idPedido.ToString())

			ds.Tables(0).Select("IDLINEA=" & idLinea)(0)("CANT") = cantidad
			HttpContext.Current.Cache.Insert("DsetEmisionPedidos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString() & "_" & idPedido.ToString(), ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration,
											 New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
		Catch ex As Exception
			Throw ex
		End Try
	End Sub
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Sub CambiarDescripcionLinea(ByVal idPedido As Integer, ByVal idLinea As Integer, ByVal descripcion As String)
		Try
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim ds As DataSet = HttpContext.Current.Cache("DsetEmisionPedidos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString() & "_" & idPedido.ToString())

			ds.Tables(0).Select("IDLINEA=" & idLinea)(0)("ART_DEN") = descripcion

			HttpContext.Current.Cache.Insert("DsetEmisionPedidos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString() & "_" & idPedido.ToString(), ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration,
											 New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
		Catch ex As Exception
			Throw ex
		End Try
	End Sub
	''' <summary>Devuelve los centros de aprovisionamiento</summary>
	''' <param name="OrgCompras">Org. compras</param>   
	''' <param name="Art4">Cod. artículo</param> 
	''' <param name="Anyo">Anyo</param>
	''' <param name="Gmn1">Gmn1</param>
	''' <param name="Proce">Cod. proceso</param>
	''' <param name="Item">Id. ítem</param>    
	''' <remarks>Llamada desde: EmisionPedido.js</remarks>
	<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Cargar_Centros_Aprovisionamiento(ByVal OrgCompras As String, ByVal Art4 As String, ByVal Anyo As Integer, ByVal Gmn1 As String, ByVal Proce As Integer, ByVal Item As Integer) As String
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim oCentros As Centros
			oCentros = FSNServer.Get_Object(GetType(FSNServer.Centros))

			Dim sUsu As String = If(FSNServer.TipoAcceso.gbUsar_OrgCompras And FSNUser.EPRestringirSeleccionCentroAprovUsuario, FSNUser.Cod, Nothing)

			Dim oCentrosAprov As IEnumerable(Of Object)
			If Anyo <> 0 And Not String.IsNullOrEmpty(Gmn1) And Proce <> 0 And Item <> 0 Then
				If Not String.IsNullOrEmpty(Art4) Then
					'Si la línea proviene de adjudicación se cargan los centros correspondientes a la distribución del ítem
					oCentrosAprov = oCentros.DevolverCentrosPorItem(Anyo, Gmn1, Proce, Item, OrgCompras, sUsu)
				Else
					'Si la línea proviene de una adjudicación pero se trata de un artículo no codificado se cargan los centros correspondientes a la distribución del ítem
					oCentrosAprov = oCentros.DevolverCentrosPorItemNoCodificado(Anyo, Gmn1, Proce, Item, , OrgCompras, sUsu)
				End If

				If (oCentrosAprov Is Nothing OrElse oCentrosAprov.Count = 0) And Not String.IsNullOrEmpty(Art4) Then
					'Se cargan los centros correspondientes a la distribución del artículo
					oCentrosAprov = oCentros.DevolverCentrosPorArticulo(Art4, True, Anyo, Gmn1, Proce, Item, , OrgCompras,, sUsu)
				End If
			ElseIf Not String.IsNullOrEmpty(Art4) And (Anyo = 0 Or String.IsNullOrEmpty(Gmn1) Or Proce = 0 Or Item = 0) Then
				'Si la línea proviene de artículo se cargan los centros correspondientes a la distribución del artículo
				oCentrosAprov = oCentros.DevolverCentrosPorArticulo(Art4, False, , , , , , OrgCompras,, sUsu)
			ElseIf String.IsNullOrEmpty(Art4) And (Anyo = 0 Or String.IsNullOrEmpty(Gmn1) Or Proce = 0 Or Item = 0) Then
				'Si se trata de un pedido libre (no hay artículo en la línea ni proceso) se cargan los centros de la organización de compras
				oCentrosAprov = oCentros.DevolverCentrosOrgCompras(FSNUser.Cod, (FSNServer.TipoAcceso.gbUsar_OrgCompras And FSNUser.EPRestringirSeleccionCentroAprovUsuario), False, OrgCompras)
			End If

			Return serializer.Serialize(oCentrosAprov)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Antes de emitir el pedido comprobamos si las cantidades de la linea no superan el limite permitido en el pedido abierto.
	''' Tenemos en cuenta lo ya pedido contra el pedido abierto como lo que vamos acumulando en el pedido en curso, es decir,
	''' si una linea de 100 unidades ya tiene pedidas 4 y en la cesta actual hay dos lineas, una de 5 y otra de 5, esto no seria correcto.
	''' </summary>
	''' <param name="IdLineaPedidoAbierto"></param>
	''' <returns></returns>
	''' <remarks></remarks>
	<System.Web.Services.WebMethod(True),
		System.Web.Script.Services.ScriptMethod()>
	Public Shared Function Comprobar_Importes_Cantidades_PedidoAbierto(ByVal IdLineaPedidoAbierto As Integer) As String
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEmision As FSNServer.CPedidoAbierto
			cEmision = FSNServer.Get_Object(GetType(FSNServer.CPedidoAbierto))

			Return serializer.Serialize(cEmision.Comprobar_Importes_Cantidades_PedidoAbierto(IdLineaPedidoAbierto))
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	<System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function Obtener_Datos_ListaExterna(ByVal Empresa As Integer, ByVal IdAtributoListaExterna As Integer, ByVal atributos As Object, ByVal codigoBuscado As String, ByVal denominacionBuscado As String,
                                                      ByVal buscador As Boolean, ByVal OrgCompras As String, ByVal Centro As String, ByVal sCodProveedor As String) As IList
        Try
            Dim serializer As New JavaScriptSerializer()
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oEP_ValidacionesIntegracion As EP_ValidacionesIntegracion = FSNServer.Get_Object(GetType(EP_ValidacionesIntegracion))

            Dim sEmisor As String

            ' Se crea la lista con el diccionario de articulos
            Dim lAtributosAux(,) As String
            If Not (atributos = "") Then
                Dim lAtributos As String() = Split(atributos, "##########")

                ReDim lAtributosAux(UBound(lAtributos), 1)

                Dim lValor As String()
                For i As Integer = 0 To UBound(lAtributos)
                    lValor = Split(lAtributos(i), "@########@")

                    lAtributosAux(i, 0) = lValor(0)
                    lAtributosAux(i, 1) = lValor(1)
                Next
            End If
            'FIN Se crea la lista con el diccionario de atributos

            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            sEmisor = FSNUser.CodPersona

            Dim lResultados(,) As String
            lResultados = oEP_ValidacionesIntegracion.EvalListaExterna(Empresa, IdAtributoListaExterna, lAtributosAux, sEmisor, codigoBuscado, denominacionBuscado, OrgCompras,, Centro, sCodProveedor)

            Dim listaExterna As New List(Of itemListaExterna)
            Dim iListaExterna As itemListaExterna

            If Not lResultados Is Nothing Then
                For i = 0 To UBound(lResultados, 1)
                    iListaExterna = New itemListaExterna

                    iListaExterna.id = lResultados(i, 0)
                    iListaExterna.name = IIf(buscador, "", iListaExterna.id & " - ") & lResultados(i, 1)

                    listaExterna.Add(iListaExterna)
                Next
            End If

            Return listaExterna
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' FunciÃ³n que devuelve los atributos del pedido relacionados con el tipo de pedido
    ''' </summary>
    ''' <param name="iCodPedido">i Cod Pedido</param>
    ''' <param name="sIDTipoPedido">Id Pedido</param>
    ''' <param name="bHayIntegracion">Si hay integraciÃ³n</param>
    ''' <param name="iEmpresa">Id Empresa</param> 
    ''' <returns>Atributos</returns>
    <System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
	Public Shared Function CargarAtributosCabeceraTipoPedido(ByVal iCodPedido As Integer, ByVal sIDTipoPedido As String, ByVal bHayIntegracion As Boolean, ByVal iEmpresa As Integer) As Object
		Try
			Dim serializer As New JavaScriptSerializer()
			Dim iIdTipoPedido As Integer
			Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			Int32.TryParse(sIDTipoPedido, iIdTipoPedido)
			Dim oAtributos As CAtributosPedido
			oAtributos = cEmisionPedidos.Cargar_Atributos_TipoPedido(iCodPedido, iIdTipoPedido, bHayIntegracion, iEmpresa, FSNUser.Cod)

			Return serializer.Serialize(oAtributos)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
	''' <summary>
	''' Obtiene la empresa para la primera linea de la cesta con partida desde el pm.
	''' </summary>
	''' <param name="sId">Cesta cabecera</param>
	''' <returns></returns>
	Private Function Cargar_Empresa_Primer_PluriAnual(ByVal sID As String, ByVal Defecto As String) As String
		Try
			Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
			Dim cEmisionPedidos As CEmisionPedidos
			cEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))

			Dim Res As String = cEmisionPedidos.Cargar_Empresa_Primer_PluriAnual(sID)
			Return IIf(Res = "", Defecto, Res)
		Catch ex As Exception
			Throw ex
		End Try
	End Function
#End Region
End Class