﻿Imports Fullstep.FSNServer

Partial Public Class Adjuntos
    Inherits FSEPPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim iAdjunto As Integer
        Dim Tipo As TiposDeDatos.Adjunto.Tipo
        Dim iLinea As Integer
        Dim Adjunto() As Byte = Nothing
        If Not String.IsNullOrEmpty(Request("adjunto")) Then iAdjunto = Request("adjunto")
        If Not String.IsNullOrEmpty(Request("tipo")) Then Tipo = CType(Request("tipo"), TiposDeDatos.Adjunto.Tipo)
        If Not String.IsNullOrEmpty(Request("linea")) Then iLinea = Request("linea")
        Dim AdjunDescargar As Adjunto = Me.FSNServer.Get_Object(GetType(Adjunto))
        Select Case Tipo
            Case TiposDeDatos.Adjunto.Tipo.Linea_Pedido ' Adjunto línea aprobación
                Dim ds As DataSet = HttpContext.Current.Cache("DsetPedidosAprobacion_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
                If ds IsNot Nothing Then
                    Dim fila As DataRow = ds.Tables("ADJUNTOS_LINEA").Rows.Find(iAdjunto)
                    If fila IsNot Nothing Then
                        AdjunDescargar.Id = iAdjunto
                        AdjunDescargar.Nombre = fila.Item("NOMBRE")
                        AdjunDescargar.dataSize = fila.Item("DATASIZE")
                        Adjunto = AdjunDescargar.LeerAdjunto(AdjunDescargar.Id, TiposDeDatos.Adjunto.Tipo.Linea_Pedido,,, AdjunDescargar.IdEsp)
                    End If
                End If
            Case TiposDeDatos.Adjunto.Tipo.Orden_Entrega ' Adjunto orden aprobación
                Dim ds As DataSet = HttpContext.Current.Cache("DsetPedidosAprobacion_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
                If ds IsNot Nothing Then
                    Dim fila As DataRow = ds.Tables("ADJUNTOS_ORDEN").Rows.Find(iAdjunto)
                    If fila IsNot Nothing Then
                        AdjunDescargar.Id = iAdjunto
                        AdjunDescargar.Nombre = fila.Item("NOMBRE")
                        AdjunDescargar.dataSize = fila.Item("DATASIZE")
                        Adjunto = AdjunDescargar.LeerAdjunto(AdjunDescargar.Id, TiposDeDatos.Adjunto.Tipo.Orden_Entrega,,, AdjunDescargar.IdEsp)
                    End If
                End If
            Case TiposDeDatos.Adjunto.Tipo.EspecificacionLineaCatalogo ' Especificación de una línea de catálogo
                Dim ds As DataSet = HttpContext.Current.Cache("DsetArticulos_" & Me.Usuario.CodPersona)
                If ds IsNot Nothing Then
                    Dim fila As DataRow = ds.Tables(0).Rows.Find(iLinea)
                    If fila IsNot Nothing Then
                        Dim oArt As CArticulo = Me.FSNServer.Get_Object(GetType(CArticulo))
                        oArt.ID = iLinea
                        oArt.CargarEspecificaciones()
                        Dim oEsp As CEspecificacion = oArt.Especificaciones.Item(iAdjunto.ToString())
                        If oEsp IsNot Nothing Then
                            AdjunDescargar.Id = oEsp.Id
                            AdjunDescargar.IdEsp = oEsp.IdCatLin
                            AdjunDescargar.Nombre = oEsp.Nombre
                            AdjunDescargar.dataSize = oEsp.DataSize
                            Adjunto = AdjunDescargar.LeerAdjunto(AdjunDescargar.Id, TiposDeDatos.Adjunto.Tipo.EspecificacionLineaCatalogo,,, AdjunDescargar.IdEsp)
                        End If
                    End If
                End If
        End Select
        Response.AddHeader("content-disposition", "attachment; filename=""" & AdjunDescargar.Nombre & """")
        Response.ContentType = "application/octet-stream"
        Response.BinaryWrite(Adjunto)
        Response.End()
    End Sub

End Class