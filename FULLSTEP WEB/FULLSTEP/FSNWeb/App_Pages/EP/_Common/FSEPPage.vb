﻿Imports Fullstep.FSNLibrary
Imports Fullstep.FSNServer
Public Class FSEPPage
    Inherits FSNPage

#Region "EP"
    Public Shared ReadOnly Property DsetArticulos() As DataSet
        Get
            Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
            Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
            If HttpContext.Current.Cache("DsetArticulos_" & oUsuario.CodPersona) Is Nothing Then
                Dim oArticulos As FSNServer.cArticulos = oServer.Get_Object(GetType(FSNServer.cArticulos))
                Dim dsArticulos As DataSet = oArticulos.DevolverArticulosCatalogo(0, "", "", System.DBNull.Value, Nothing, Nothing, Nothing, Nothing, Nothing, System.DBNull.Value, oUsuario.CodPersona, 1, "", String.Empty, String.Empty, "")
                HttpContext.Current.Cache.Insert("DsetArticulos_" & oUsuario.CodPersona, dsArticulos, Nothing, Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))

                Return dsArticulos
            Else
                Return HttpContext.Current.Cache("DsetArticulos_" & oUsuario.CodPersona)
            End If
        End Get
    End Property
    ''' <summary>
    ''' Año de la orden más antigua emitida. Valor compartido por todos los usuarios
    ''' </summary>
    Public Shared ReadOnly Property AnyoMin() As Integer
        Get
            Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
            If HttpContext.Current.Cache("AnyoMin") Is Nothing Then
                Dim oOrdenes As FSNServer.COrdenes = oServer.Get_Object(GetType(FSNServer.COrdenes))
                Dim Anyo As Nullable(Of Integer) = oOrdenes.ObtenerMinAnyo()
                If Anyo Is Nothing Then _
                    Anyo = Date.Now.Year - 12
                HttpContext.Current.Cache.Insert("AnyoMin", Anyo, Nothing, Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                Return Anyo
            Else
                Return HttpContext.Current.Cache("AnyoMin")
            End If
        End Get
    End Property
    ''' <summary>
    ''' Actualiza el precio del artículo en los objetos de caché
    ''' </summary>
    ''' <param name="LinCat">ID de la línea del catálogo</param>
    ''' <param name="Precio">Nuevo precio</param>
    Public Shared Sub ActualizarPrecArticulo(ByVal LinCat As Integer, ByVal Precio As Double)
        For Each x As DictionaryEntry In HttpContext.Current.Cache
            If x.Key.ToString().StartsWith("DsetArticulos_") Then
                Dim ds As DataSet = CType(x.Value, DataSet)
                Dim dr As DataRow = ds.Tables(0).Rows.Find(LinCat)
                If dr IsNot Nothing Then
                    dr.Item("PRECIO_UC") = Precio
                    ds.AcceptChanges()
                    HttpContext.Current.Cache(x.Key) = ds
                End If
            End If
        Next
    End Sub
    ''' <summary>
    ''' Elimina los objetos de cache que contienen información sobre una orden de entrega
    ''' </summary>
    ''' <param name="Orden">ID de la orden de entrega</param>
    Public Shared Sub EliminarCachePedidos(ByVal Orden As Integer, Optional ByVal instanciaAprob As Long = 0)
        EliminarCachePedidos(Orden, String.Empty, True, True, True, instanciaAprob)
    End Sub
    ''' Revisado por: blp. Fecha: 08/03/2012
    ''' <summary>
    ''' Elimina los objetos de cache que contienen información sobre las órdenes de entrega que cumplan los criterios establecidos
    ''' </summary>
    ''' <param name="Orden">ID de la orden que debe estar presente para eliminar el objeto, si no se proporciona se eliminan todos los objetos</param>
    ''' <param name="Persona">Código de persona para el que se eliminarán los objetos, si no se proporciona se eliminan los de todos los usuarios</param>
    ''' <param name="Seguimiento">Boolean que indica si se eliminan los objetos de pedidos de seguimiento</param>
    ''' <param name="Recepcion">Boolean que indica si se eliminan los objetos de pedidos de recepción</param>
    ''' <param name="Aprobacion">Boolean que indica si se eliminan los objetos de pedidos de aprobación</param>
    ''' <remarks>Llamada desde Aprobacion.aspx.vb, Seguimiento.aspx.vb, EmisionPedido.aspx.vb. Max. 0,1 seg.</remarks>
    Public Shared Sub EliminarCachePedidos(ByVal Orden As Integer, ByVal Persona As String, ByVal Seguimiento As Boolean, ByVal Recepcion As Boolean, ByVal Aprobacion As Boolean, Optional ByVal instanciaAprob As Long = 0)
        Dim arKeys As New List(Of String)
        If Seguimiento Then
            arKeys.Add("DsetPedidosSeguimiento_" & IIf(String.IsNullOrEmpty(Persona), String.Empty, Persona & "_"))
        End If
        If Recepcion Then
            arKeys.Add("DsetPedidosRecepcion_" & IIf(String.IsNullOrEmpty(Persona), String.Empty, Persona & "_"))
        End If
        If Aprobacion Then _
            arKeys.Add("DsetPedidosAprobacion_" & IIf(String.IsNullOrEmpty(Persona), String.Empty, Persona & "_"))
        For Each x As DictionaryEntry In HttpContext.Current.Cache
            Dim bElim As Boolean = False
            For Each s As String In arKeys
                If x.Key.ToString().StartsWith(s) Then
                    bElim = True
                    Exit For
                End If
            Next
            If bElim Then
                If Orden > 0 Then
                    Dim ds As DataSet = CType(x.Value, DataSet)
                    Dim dr As DataRow
                    'Actualmente, con los cambios en Recepcion, el dataset de esta página se sigue guardando en caché
                    'pero su tabla ORDENES ya no tiene primary key, por lo que no se puede buscar directamente por el ID de la orden:
                    If x.Key.ToString.StartsWith("DsetPedidosRecepcion_" & IIf(String.IsNullOrEmpty(Persona), String.Empty, Persona & "_")) Then
                        'No tiene primary key. El campo por el que buscar es ORDENID
                        Dim rowsQuery = ds.Tables("ORDENES").Select("ORDENID = " & Orden).Take(1)
                        Dim dt As DataTable
                        If rowsQuery.Any Then
                            dt = rowsQuery.CopyToDataTable
                            dr = dt.Rows(0)
                        End If
                    Else
                        'Aprobación y Seguimiento siguen teniendo la primary key ID
                        Try
                            dr = ds.Tables("ORDENES").Rows.Find(Orden)
                        Catch ex As Exception
                            Dim key() As Object = {Orden, instanciaAprob}
                            dr = ds.Tables("ORDENES").Rows.Find(key)
                        End Try

                    End If
                    If dr IsNot Nothing Then HttpContext.Current.Cache.Remove(x.Key)
                Else
                    HttpContext.Current.Cache.Remove(x.Key)
                End If
            End If
        Next
    End Sub
    ''' Revisado por: blp. Fecha: 23/01/2013
    ''' <summary>
    ''' Elimina los objetos de cache que contienen información sobre los artículos que puede ver un usuario determinado
    ''' </summary>
    ''' <param name="Persona">Código de persona para el que se eliminarán los objetos, si no se proporciona se eliminan los de todos los usuarios</param>
    ''' <remarks>Llamada desde Seguimiento.aspx.vb, Max. 0,1 seg.</remarks>
    Public Shared Sub EliminarCacheArticulos(ByVal Persona As String)
        Dim sKey As String = "DsetArticulosSeguimiento_" & IIf(String.IsNullOrEmpty(Persona), "_NoBorrar_", Persona & "_")
        For Each x As DictionaryEntry In HttpContext.Current.Cache
            Dim bElim As Boolean = False
            If x.Key.ToString().StartsWith(sKey) Then
                HttpContext.Current.Cache.Remove(x.Key)
            End If
        Next
    End Sub
    Public ReadOnly Property TodosDestinos() As List(Of ListItem)
        Get
            If HttpContext.Current.Cache("TodosDestinos" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                Dim oDest As FSNServer.CDestinos = Me.FSNServer.Get_Object(GetType(FSNServer.CDestinos))
                Dim ds As DataSet = oDest.CargarTodosLosDestinos(Me.Usuario.Idioma, "")
                Dim Lista As New List(Of ListItem)
                For Each x As DataRow In ds.Tables(0).Rows
                    Lista.Add(New ListItem(x.Item("DEN"), x.Item("COD")))
                Next
                Me.InsertarEnCache("TodosDestinos" & Me.Usuario.Idioma.ToString(), Lista)
                Return Lista
            Else
                Return HttpContext.Current.Cache("TodosDestinos" & Me.Usuario.Idioma.ToString())
            End If
        End Get
    End Property
    Public ReadOnly Property TiposPedidos() As FSNServer.cTiposPedidos
        Get
            Return TiposPedidos(False)
        End Get
    End Property
    Public ReadOnly Property TiposPedidos(ByVal IncluirBajasLogicas As Boolean) As FSNServer.cTiposPedidos
        Get
            Dim oTiposPedidos As FSNServer.cTiposPedidos
            Dim oTiposPedidosActivos As FSNServer.cTiposPedidos
            oTiposPedidos = FSNServer.Get_Object(GetType(FSNServer.cTiposPedidos))
            If HttpContext.Current.Cache("TiposPedidos" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                oTiposPedidos.CargarTiposPedido(Me.Usuario.Idioma)
                Me.InsertarEnCache("TiposPedidos" & Me.Usuario.Idioma.ToString(), oTiposPedidos)
            Else
                oTiposPedidos = HttpContext.Current.Cache("TiposPedidos" & Me.Usuario.Idioma.ToString())
            End If
            If Not IncluirBajasLogicas Then
                oTiposPedidosActivos = FSNServer.Get_Object(GetType(FSNServer.cTiposPedidos))
                oTiposPedidosActivos.AddRange(oTiposPedidos)
                oTiposPedidosActivos.RemoveAll(Function(el As FSNServer.cTipoPedido) el.BajaLogica = True)
                Return oTiposPedidosActivos
            Else
                Return oTiposPedidos
            End If
        End Get
    End Property
    Public Function AutorizacionWebparts(ByVal Filtro As String) As Boolean
        Select Case Filtro
            Case "PM"
                Return FSNUser.AccesoPM _
                    And Me.Acceso.gsAccesoFSPM <> TiposDeDatos.TipoAccesoFSPM.SinAcceso
            Case "QA"
                Return FSNUser.AccesoQA _
                    And Me.Acceso.gsAccesoFSQA <> TiposDeDatos.TipoAccesoFSQA.SinAcceso
            Case "QAPuntuacion"
                Return FSNUser.AccesoQA _
                    And Me.Acceso.gsAccesoFSQA <> TiposDeDatos.TipoAccesoFSQA.SinAcceso _
                    And FSNUser.QAPuntuacionProveedores
            Case "QAMateriales"
                Return FSNUser.AccesoQA _
                    And Me.Acceso.gsAccesoFSQA <> TiposDeDatos.TipoAccesoFSQA.SinAcceso _
                    And FSNUser.QAMantenimientoMat
            Case "QACertificados"
                Return FSNUser.AccesoQA _
                    And Me.Acceso.gsAccesoFSQA <> TiposDeDatos.TipoAccesoFSQA.SinAcceso _
                    And Me.Acceso.gbAccesoQACertificados
            Case "QANoConformidades"
                Return FSNUser.AccesoQA _
                    And Me.Acceso.gsAccesoFSQA <> TiposDeDatos.TipoAccesoFSQA.SinAcceso _
                    And Me.Acceso.gbAccesoQANoConformidad
            Case "QAUNQA"
                Return FSNUser.AccesoQA _
                    And Me.Acceso.gsAccesoFSQA <> TiposDeDatos.TipoAccesoFSQA.SinAcceso _
                    And FSNUser.QAMantenimientoUNQA
            Case "EP"
                Return Me.Acceso.gbAccesoFSEP And FSNUser.AccesoEP
            Case "EPAprovisionador"
                Return Me.Acceso.gbAccesoFSEP And FSNUser.AccesoEP _
                    And (FSNUser.EPTipo = TipoAccesoFSEP.SoloAprovisionador Or FSNUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador)
            Case "EPAprovisionadorPedidoAbierto"
                Return Me.Acceso.gbAccesoFSEP And FSNUser.AccesoEP _
                    And (FSNUser.EPTipo = TipoAccesoFSEP.SoloAprovisionador _
                         OrElse FSNUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador _
                         OrElse FSNUser.EPPermitirEmitirPedidosDesdePedidoAbierto)
            Case "EPAprobador"
                Return Me.Acceso.gbAccesoFSEP And FSNUser.AccesoEP _
                    And (FSNUser.EPTipo = TipoAccesoFSEP.SoloAprobador Or FSNUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador)
            Case "SM"
                Return Me.Acceso.gbAccesoFSSM And FSNUser.AccesoSM
            Case "Oculto"
                Return False
        End Select
    End Function
    ''' <summary>
    ''' Método que inserta al final del formulario de la página (con el método RegisterStartupScript) un script que 
    ''' se ejecutará al cargar la página. El script consiste en una llamada a la función addEvent() presente en js/jsLimitedecimales.js
    ''' que a su vez inserta un capturador del evento Paste de aquellos controles para los que se ejecute este método
    ''' </summary>
    ''' <param name="clientID">ID en el navegador cliente del control para el que queremos ejecutar la función addEvent</param>
    ''' <remarks>Llamada desde: todas las páginas en las que se desee capturar el evento paste en algún control
    ''' Tiempo máximo: inferior a 0,1 seg
    ''' </remarks>
    Public Sub insertarScriptPaste(ByVal clientID As String)
        Dim csname1 As String = "script" & clientID
        Dim cstype As Type = Me.GetType()
        Dim cs As ClientScriptManager = Page.ClientScript

        Dim csScript As String = "if(document.getElementById('" & clientID & "_t') != null){"
        csScript += "addEvent(document.getElementById('" & clientID & "_t'), 'paste', function(event) { eventoPaste = true; });"
        csScript += "} else { if(document.getElementById('igpck" & clientID & "') != null){"
        csScript += "addEvent(document.getElementById('igpck" & clientID & "'), 'paste', function(event) { eventoPaste = true; });"
        csScript += "}}"

        ScriptManager.RegisterStartupScript(Me.Page, cstype, csname1, csScript, True)
    End Sub
#Region "Gestion FAVORITOS"
    ''' <summary>
    ''' Creamos un dataset con la información del pedido favorito que se quiere enviar a EmisionPedido para emitirlo o modificarlo
    ''' </summary>
    ''' <param name="DsetPedidos">Datos de favoritos desde los cuales recuperamos los qeu nos interesan</param>
    ''' <param name="idOrden">Id de la orden que nos interesa recuperar</param>
    ''' <returns>dataset con 5 tablas, Orden Favorita, Lineas de la orden, imputaciones de la linea, adjuntos de la orden y adjuntos de las lineas</returns>
    ''' <remarks>llamada desde Favoritos.aspx.vb y EmisionPEdido.aspx.vb</remarks>
    Public Function CrearDatasetFavoritoParaEmision(ByVal DsetPedidos As DataSet, ByVal idOrden As Integer) As DataSet
        CrearTablasAdjuntosLinea(DsetPedidos)
        CrearTablaAdjuntosOrden(DsetPedidos)
        Dim DsetOrdenFavorito As New DataSet
        DsetOrdenFavorito = DsetPedidos.Clone()

        For Each fila As DataRow In DsetPedidos.Tables("ORDENESFAVORITOS").Rows
            If fila.Item("ID") = idOrden Then
                DsetOrdenFavorito.Tables("ORDENESFAVORITOS").ImportRow(fila)
                Exit For
            End If
        Next

        For Each fila As DataRow In DsetPedidos.Tables("LINEASPEDIDO").Rows
            If fila.Item("ORDEN") = idOrden Then
                DsetOrdenFavorito.Tables("LINEASPEDIDO").ImportRow(fila)

                'adjuntos de la linea
                CargarAdjuntosLinea(fila.Item("LINEAID"), DsetPedidos)

                Dim filasAdjuntos() As DataRow = fila.GetChildRows("REL_LINEAS_ADJUNTOS")
                If filasAdjuntos.Any Then
                    For i As Integer = 0 To filasAdjuntos.Count - 1
                        DsetOrdenFavorito.Tables("ADJUNTOS_LINEA").ImportRow(filasAdjuntos(i))
                    Next
                End If

                'Imputaciones de la linea
                Dim filasImputacion() As DataRow = fila.GetChildRows("REL_FAVORITO_IMPUTACIONES")
                If filasImputacion.Any Then
                    For i As Integer = 0 To filasImputacion.Count - 1
                        DsetOrdenFavorito.Tables("IMPUTACIONES").ImportRow(filasImputacion(i))
                    Next
                End If
            End If
        Next

        'ADJUNTOS ORDEN
        'Nos aseguramos que los adjuntos están en el dsetpedidos
        CargarAdjuntosOrden(idOrden, DsetPedidos)

        Dim filasAdjuntosOrden() As DataRow = DsetPedidos.Tables("ORDENESFAVORITOS").Rows.Find(idOrden).GetChildRows("REL_ORDENES_ADJUNTOS")
        If filasAdjuntosOrden.Any Then
            For i As Integer = 0 To filasAdjuntosOrden.Count - 1
                DsetOrdenFavorito.Tables("ADJUNTOS_ORDEN").ImportRow(filasAdjuntosOrden(i))
            Next
        End If

        Return DsetOrdenFavorito
    End Function
    ''' <summary>
    ''' Procedimiento que crea la tabla de adjuntos de las líneas de pedido
    ''' </summary>
    ''' <param name="DsetPedidos">Dataset en el que crear la tabla</param>
    ''' <remarks>
    ''' Llamada desde: CrearAdjuntosLinea y CrearDatasetFavoritoParaEmision
    ''' Tiempo máximo: 0 seg</remarks>
    Public Sub CrearTablasAdjuntosLinea(ByRef DsetPedidos As DataSet)
        If DsetPedidos.Tables("ADJUNTOS_LINEA") Is Nothing Then
            Dim dt As DataTable = New DataTable("ADJUNTOS_LINEA")
            dt.Columns.Add("ID", GetType(System.Int32))
            dt.Columns.Add("LINEA", GetType(System.Int32))
            dt.Columns.Add("NOMBRE", GetType(System.String))
            dt.Columns.Add("COMENTARIO", GetType(System.String))
            dt.Columns.Add("FECHA", GetType(System.DateTime))
            dt.Columns.Add("DATASIZE", GetType(System.Double))
            dt.Columns.Add("TIPO", GetType(TiposDeDatos.Adjunto.Tipo))
            Dim keylp() As DataColumn = {dt.Columns("ID")}
            dt.PrimaryKey = keylp
            DsetPedidos.Tables.Add(dt)
            keylp(0) = DsetPedidos.Tables("ADJUNTOS_LINEA").Columns("LINEA")
            DsetPedidos.Relations.Add("REL_LINEAS_ADJUNTOS", DsetPedidos.Tables("LINEASPEDIDO").PrimaryKey, keylp, False)
        End If
    End Sub
    ''' <summary>
    ''' Procedimiento que crea la tabla de adjuntos de la orden de favoritos, para mostrarlos
    ''' </summary>
    ''' <param name="DsetPedidos">Dataset en el que crear la tabla</param>
    ''' <remarks>
    ''' Llamada desde:CargarAdjuntosOrden y CrearDatasetFavoritoParaEmision
    ''' tiempo Máximo: 0 seg</remarks>
    Public Sub CrearTablaAdjuntosOrden(ByRef DsetPedidos As DataSet)
        If DsetPedidos.Tables("ADJUNTOS_ORDEN") Is Nothing Then
            Dim dt As DataTable = New DataTable("ADJUNTOS_ORDEN")
            dt.Columns.Add("ID", GetType(System.Int32))
            dt.Columns.Add("ORDEN", GetType(System.Int32))
            dt.Columns.Add("NOMBRE", GetType(System.String))
            dt.Columns.Add("COMENTARIO", GetType(System.String))
            dt.Columns.Add("FECHA", GetType(System.DateTime))
            dt.Columns.Add("DATASIZE", GetType(System.Double))
            dt.Columns.Add("TIPO", GetType(TiposDeDatos.Adjunto.Tipo))
            Dim keylp() As DataColumn = {dt.Columns("ID")}
            dt.PrimaryKey = keylp
            DsetPedidos.Tables.Add(dt)
            keylp(0) = DsetPedidos.Tables("ADJUNTOS_ORDEN").Columns("ORDEN")
            DsetPedidos.Relations.Add("REL_ORDENES_ADJUNTOS", DsetPedidos.Tables("ORDENESFAVORITOS").PrimaryKey, keylp, False)
        End If
    End Sub
    ''' <summary>
    ''' Función que carga los adjuntos de las líneas de pedido
    ''' </summary>
    ''' <param name="Id">Identificador de la línea de la que cargar los archivos adjuntos</param>
    ''' <param name="DsetPedidos">Dataset con las lineas de las que se quieren recuperar los datos de adjuntos (y al que asímismo se añaden)</param>
    ''' <returns>Un número entero indicando 0 si ha ido bien y 1 en caso contrario</returns>
    ''' <remarks>
    ''' Llamada desde:grdLineasPedidos_RowCommand
    ''' Tiempo máximo: 0,2 seg </remarks>
    Public Function CargarAdjuntosLinea(ByVal Id As Integer, ByRef DsetPedidos As DataSet) As DataRow()
        CrearTablasAdjuntosLinea(DsetPedidos)
        Dim dr() As DataRow
        dr = DsetPedidos.Tables("LINEASPEDIDO").Rows.Find(Id).GetChildRows("REL_LINEAS_ADJUNTOS")
        If dr.Length = 0 Then
            Dim oLineaFavoritos As FSNServer.CLineaFavoritos = Me.FSNServer.Get_Object(GetType(FSNServer.CLineaFavoritos))
            oLineaFavoritos.ID = Id
            oLineaFavoritos.Usuario = FSNUser.Cod
            Dim oAdjs As FSNServer.CAdjuntos = oLineaFavoritos.CargarAdjuntos(False)
            For Each oAdj As FSNServer.Adjunto In oAdjs
                oAdj.IdEsp = Id
                Dim fila As DataRow = DsetPedidos.Tables("ADJUNTOS_LINEA").NewRow()
                fila.Item("ID") = oAdj.Id
                fila.Item("LINEA") = Id
                fila.Item("NOMBRE") = oAdj.Nombre
                fila.Item("COMENTARIO") = oAdj.Comentario
                fila.Item("FECHA") = oAdj.Fecha
                fila.Item("DATASIZE") = oAdj.dataSize
                fila.Item("TIPO") = TiposDeDatos.Adjunto.Tipo.Favorito_Linea_Pedido
                DsetPedidos.Tables("ADJUNTOS_LINEA").Rows.Add(fila)
            Next
            dr = DsetPedidos.Tables("LINEASPEDIDO").Rows.Find(Id).GetChildRows("REL_LINEAS_ADJUNTOS")
        End If
        Return dr
    End Function
    ''' <summary>
    ''' Carga los adjuntos del pedido favorito
    ''' </summary>
    ''' <param name="Id"></param>
    ''' <param name="DsetPedidos">Dataset con la orden favorito de la que se quiere recuperar los datos de adjuntos (y al que asímismo se añaden)</param>
    ''' <returns>devuelve un datarow con los adjuntos</returns>
    ''' <remarks>Llamada desde 
    ''' Tiempo maximo 1 sec</remarks>
    Public Function CargarAdjuntosOrden(ByVal Id As Integer, ByRef DsetPedidos As DataSet) As DataRow()
        CrearTablaAdjuntosOrden(DsetPedidos)
        Dim dr() As DataRow
        dr = DsetPedidos.Tables("ORDENESFAVORITOS").Rows.Find(Id).GetChildRows("REL_ORDENES_ADJUNTOS")
        If dr.Length = 0 Then
            Dim oOrdenFav As FSNServer.COrdenFavoritos = Me.FSNServer.Get_Object(GetType(FSNServer.COrdenFavoritos))
            oOrdenFav.ID = Id
            oOrdenFav.Usuario = FSNUser.Cod
            Dim oAdjs As FSNServer.CAdjuntos = oOrdenFav.CargarAdjuntos(False)
            For Each oAdj As FSNServer.Adjunto In oAdjs
                Dim fila As DataRow = DsetPedidos.Tables("ADJUNTOS_ORDEN").NewRow()
                fila.Item("ID") = oAdj.Id
                fila.Item("ORDEN") = Id
                fila.Item("NOMBRE") = oAdj.Nombre
                fila.Item("COMENTARIO") = oAdj.Comentario
                fila.Item("FECHA") = oAdj.Fecha
                fila.Item("DATASIZE") = oAdj.dataSize
                fila.Item("TIPO") = TiposDeDatos.Adjunto.Tipo.Favorito_Orden_Entrega
                DsetPedidos.Tables("ADJUNTOS_ORDEN").Rows.Add(fila)
            Next
            dr = DsetPedidos.Tables("ORDENESFAVORITOS").Rows.Find(Id).GetChildRows("REL_ORDENES_ADJUNTOS")
        End If
        Return dr
    End Function
#End Region
#End Region
#Region " SM "
    Public ReadOnly Property PargenSM() As FSNServer.PargensSMs
        Get
            If Me.Acceso.gbAccesoFSSM Then
                If HttpContext.Current.Cache("PargenSM" & Me.Usuario.Cod & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                    Dim oPargenSMS As FSNServer.PargensSMs
                    oPargenSMS = FSNServer.Get_Object(GetType(FSNServer.PargensSMs))
                    oPargenSMS.CargarConfiguracionSM(FSNUser.Cod, FSNUser.Idioma, True)
                    Me.InsertarEnCache("PargenSM" & Me.Usuario.Cod & "_" & Me.Usuario.Idioma.ToString(), oPargenSMS)
                    Return oPargenSMS
                Else
                    Return HttpContext.Current.Cache("PargenSM" & Me.Usuario.Cod & "_" & Me.Usuario.Idioma.ToString())
                End If
            Else
                Return Nothing
            End If
        End Get
    End Property
    Public ReadOnly Property TodosActivos() As FSNServer.Activos
        Get
            If HttpContext.Current.Cache("TodosActivos" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                Dim oAct As Fullstep.FSNServer.Activos = Me.FSNServer.Get_Object(GetType(Fullstep.FSNServer.Activos))
                oAct.ObtenerActivos(Me.Usuario.Idioma.ToString(), Nothing, String.Empty, True)
                Me.InsertarEnCache("TodosActivos" & Me.Usuario.Idioma.ToString(), oAct)
                Return oAct
            Else
                Return HttpContext.Current.Cache("TodosActivos" & Me.Usuario.Idioma.ToString())
            End If
        End Get
    End Property
    Public ReadOnly Property CentrosDeCoste(ByVal ConUon As Boolean) As DataSet
        Get
            Dim DsCentrosDeCoste As New DataSet
            Dim DsCentrosCosteSinUON As New DataSet
            If ConUon Then
                If HttpContext.Current.Cache("DsCentrosDeCoste_" & FSNUser.CodPersona) Is Nothing Then
                    Dim CentrosCoste As Fullstep.FSNServer.Centros_SM
                    CentrosCoste = FSNServer.Get_Object(GetType(Fullstep.FSNServer.Centros_SM))
                    '¡¡TENGO QUE TENER EN CUENTA EL NIVEL Y EL ARBOL!!
                    DsCentrosDeCoste = CentrosCoste.LoadData(FSNUser.Cod, FSNUser.Idioma, True)
                    Me.InsertarEnCache("DsCentrosDeCoste_" & FSNUser.CodPersona, DsCentrosDeCoste)
                Else
                    DsCentrosDeCoste = HttpContext.Current.Cache("DsCentrosDeCoste_" & FSNUser.CodPersona)
                End If
                Return DsCentrosDeCoste
            Else
                If HttpContext.Current.Cache("DsCentrosDeCosteSinUon_" & FSNUser.CodPersona) Is Nothing Then
                    Dim CentrosCoste As Fullstep.FSNServer.Centros_SM
                    CentrosCoste = FSNServer.Get_Object(GetType(Fullstep.FSNServer.Centros_SM))
                    '¡¡TENGO QUE TENER EN CUENTA EL NIVEL Y EL ARBOL!!
                    DsCentrosCosteSinUON = CentrosCoste.LoadData(FSNUser.Cod, FSNUser.Idioma, False)
                    Me.InsertarEnCache("DsCentrosDeCosteSinUon_" & FSNUser.CodPersona, DsCentrosCosteSinUON)
                Else
                    DsCentrosCosteSinUON = HttpContext.Current.Cache("DsCentrosDeCosteSinUon_" & FSNUser.CodPersona)
                End If
                Return DsCentrosCosteSinUON
            End If
        End Get
    End Property
#End Region
    ''' <summary>
    ''' Copiar propiedades de una instancia de clase de EP a otra
    ''' </summary>
    ''' <param name="oOriginalObject">instancia desde la que copiar</param>
    ''' <returns>instancia copia</returns>
    ''' <remarks>Llamada desde: Emisionpedido Max:0,1</remarks>
    Public Function getProperties(ByVal oOriginalObject As Object) As Object
        If Not oOriginalObject Is Nothing Then
            'Usamos Reflexión para copiar las propiedades. La alternativa sería copiar las propiedades una por una
            Dim type As Type = oOriginalObject.GetType()
            Dim oDestinyObject As Object = FSNServer.Get_Object(type)
            'Dim oDestinyObject As [Object] = type.InvokeMember("", System.Reflection.BindingFlags.CreateInstance, Nothing, oOriginalObject, Nothing)
            Dim properties As Reflection.PropertyInfo() = type.GetProperties()
            For Each propertyInfo As Reflection.PropertyInfo In properties
                If propertyInfo.CanWrite Then 'AndAlso propertyInfo.Name <> "Item" 
                    'If propertyInfo.PropertyType.Namespace.IndexOf("EPServer") > 0 Then
                    '    If propertyInfo.GetValue(oOriginalObject, Nothing) IsNot Nothing Then
                    '        getProperties(propertyInfo.GetValue(oOriginalObject, Nothing))
                    '    Else
                    '        propertyInfo.SetValue(oDestinyObject, propertyInfo.GetValue(oOriginalObject, Nothing), Nothing)
                    '    End If
                    'Else
                    propertyInfo.SetValue(oDestinyObject, propertyInfo.GetValue(oOriginalObject, Nothing), Nothing)
                    'End If
                End If
            Next
            Return oDestinyObject
        Else
            Return oOriginalObject
        End If
    End Function
    ''' Revisado por: blp. Fecha: 29/08/2013
    ''' <summary>
    ''' Registra el script con las funciones para usar con paneles de búsqueda construidos con jQuery
    ''' </summary>
    ''' <remarks>LLamada desde el Page_Load; Tiempo máximo: 0,5 sg</remarks>
    Public Sub CargarScriptBuscadoresJQuery()
        Dim oScript As HtmlGenericControl
        oScript = CargarBuscadoresJQuery(TiposDeDatos.Aplicaciones.EP)
        'Cuando todo se hace en CargarScriptPeticionarios, se devuelve un script "EMPTY" y no hay nada más que hacer. Si no, hay que insertar en la página el script q se recibe
        If oScript IsNot Nothing AndAlso oScript.TagName = "script" Then
            'pag.ScriptPeticionarios = oScript
            RegistrarScript(oScript, "ScriptBuscadoresJQuery")
        End If
    End Sub
End Class
