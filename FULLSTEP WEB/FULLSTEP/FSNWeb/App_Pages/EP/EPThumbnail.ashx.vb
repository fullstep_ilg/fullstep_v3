﻿Imports System.Web
Imports System.Web.Services
Imports System.Drawing

Public Class EPThumbnail
    Implements System.Web.IHttpHandler, IRequiresSessionState, IReadOnlySessionState

    ''' <summary>
    ''' Carga la imagen del catalogo. El articulo por proveedor tiene imagen.
    ''' </summary>
    ''' <param name="context">Contexto en el q se ejecuta. Para tyener acceso a Request</param>
    ''' <remarks>Llamada desde: Cesta.aspx      CatalogoWeb.aspx      DetArticuloPedido.ascx      DetArticuloCatalogo.ascx ; tiempo máximo:0</remarks>
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Dim Prove, Art, Tipo As String
        Dim img As Byte() = Nothing
        Dim PonerImagen As Boolean = False
        Prove = context.Request("Prove")
        Art = context.Request("Art")
        Tipo = context.Request("Tipo")
        PonerImagen = context.Request("PonerImagen")

        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oCArticulos As FSNServer.cArticulos = FSNServer.Get_Object(GetType(FSNServer.cArticulos))

        Select Case Tipo
            Case "Imagen"
                img = oCArticulos.DevolverImagenArt(Art, Prove)
            Case "Thumbnail"
                img = oCArticulos.DevolverThumbnailArt(Art, Prove)
                If img Is Nothing OrElse img.Length = 0 Then
                    img = oCArticulos.DevolverImagenArt(Art, Prove)
                End If
            Case "Cesta"
                img = oCArticulos.DevolverThumbnailArt(Art, Prove)
                If img Is Nothing OrElse img.Length = 0 Then
                    img = oCArticulos.DevolverImagenArt(Art, Prove)
                End If
        End Select

        If img IsNot Nothing AndAlso img.Length > 0 Then
            Dim oImg = New Drawing.Bitmap(New IO.MemoryStream(img))
            If Tipo = "Thumbnail" Then
                If oImg.Width > 135 Or oImg.Height > 90 Then
                    If oImg.Width / 135 > oImg.Height / 90 Then
                        oImg = oImg.GetThumbnailImage(135, CType(oImg.Height * 135 / oImg.Width, Integer), Nothing, New IntPtr())
                    Else
                        oImg = oImg.GetThumbnailImage(CType(oImg.Width * 90 / oImg.Height, Integer), 90, Nothing, New IntPtr())
                    End If
                End If
            ElseIf Tipo = "Cesta" Then
                If oImg.Width > 60 Or oImg.Height > 40 Then
                    If oImg.Width / 60 > oImg.Height / 40 Then
                        oImg = oImg.GetThumbnailImage(60, CType(oImg.Height * 60 / oImg.Width, Integer), Nothing, New IntPtr())
                    Else
                        oImg = oImg.GetThumbnailImage(CType(oImg.Width * 40 / oImg.Height, Integer), 40, Nothing, New IntPtr())
                    End If
                End If
            Else
                If oImg.Width > 346 Or oImg.Height > 476 Then
                    If oImg.Width / 346 > oImg.Height / 476 Then
                        oImg = oImg.GetThumbnailImage(346, CType(oImg.Height * 346 / oImg.Width, Integer), Nothing, New IntPtr())
                    Else
                        oImg = oImg.GetThumbnailImage(CType(oImg.Width * 476 / oImg.Height, Integer), 476, Nothing, New IntPtr())
                    End If
                End If
            End If
            Dim mstr = New IO.MemoryStream()
            oImg.Save(mstr, Imaging.ImageFormat.Jpeg)
            context.Response.BinaryWrite(mstr.ToArray())
        Else
            If PonerImagen Then
                If Tipo = "Cesta" Then
                    context.Response.WriteFile("~/images/noimagecesta.gif")
                Else
                    context.Response.WriteFile("~/images/noimageCatalogo.gif")
                End If
            End If
        End If

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class