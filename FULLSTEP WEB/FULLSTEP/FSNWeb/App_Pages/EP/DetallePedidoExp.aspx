﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DetallePedidoExp.aspx.vb"
    Inherits="Fullstep.FSNWeb.DetallePedidoExp" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <style type="text/css">
        .DescArt
        {
            cursor: pointer;
            text-decoration: underline;
            color: Grey;
            font-size: 12px;
            font-weight: bold;
        }
        .bordeTabla
        {
            border-right: 1px solid black;
        }
        .ButtonClass
        {
            color: White;
            font-size: 12px;
            font: bold;
        }
    </style>
</head>
<script type="text/javascript"><!--

    function imprimir() {
        window.open("DetallePedidoExp.aspx?PDF=1", "fraPMDetallePedidoHidden")
    }
		
--></script>
<body>
    <form id="Form1" method="post" runat="server">
    <div id="divCabeceraImportesPedido" class="Texto12 RectanguloEP" style="overflow: auto;
        width: 99%; margin-top: 15px;">
        <div id="infoPed" class="FilaSeleccionada">
            <table>
                <tr class="FilaSeleccionada" style="height: 30px">
                    <td align="left" runat="server" id="tablaCollapseCab">
                        <asp:Label ID="lblInfoPed" runat="server" Text="DProveedor:" CssClass="Rotulo" EnableViewState="True">
                        </asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label runat="server" ID="lblPedido" ClientIDMode="static" CssClass="ButtonClass"></asp:Label>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divImportesPedido">
            <table id="ImportesPedido" style="width: 100%;" runat="server">
                <tr>
                    <td align="left" runat="server" id="Td1" style="width: 42%;">
                        <div style="float: left; vertical-align: middle;" class="FilaPar">
                            <input language="javascript" class="boton" id="cmdImprimir" onclick="return imprimir();"
                                type="button" value="PDF" name="cmdImprimir" runat="server">
                            <asp:Label ID="lblImporteBruto" runat="server" Text="DImporte Bruto:" CssClass="Normal MensajeError"
                                EnableViewState="True">
                            </asp:Label>
                        </div>
                        <div style="border-bottom-style: dotted; border-width: 2px; margin-left: 19%; vertical-align: bottom;
                            height: 8px;">
                        </div>
                    </td>
                    <td align="right" style="width: 8%;">
                        <asp:Label runat="server" ID="lblImpBruto" CssClass="Normal" ClientIDMode="static">0</asp:Label>
                    </td>
                    <td>
                        <div style="visibility: hidden">
                            <asp:Label runat="server" ID="lblImpBruto_hdd" ClientIDMode="static">0</asp:Label>
                            <asp:Label runat="server" ID="lblImpBrutoLinea_hdd" ClientIDMode="static">0</asp:Label>
                            <asp:Label runat="server" ID="lblImpBrutoSolicitud_hdd" ClientIDMode="static">0</asp:Label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="left" runat="server" id="Td2" style="width: 42%;">
                        <div style="float: left; vertical-align: middle;" class="FilaPar">
                            <asp:Label ID="lblCostesGenerales" runat="server" Text="DCostes Generales:" CssClass="Normal MensajeError"
                                EnableViewState="True">
                            </asp:Label>
                        </div>
                        <div style="border-bottom-style: dotted; border-width: 2px; margin-left: 19%; vertical-align: bottom;
                            height: 8px;">
                        </div>
                    </td>
                    <td align="right" style="width: 8%;">
                        <asp:Label runat="server" ID="lblCosteCabeceraTotal" ClientIDMode="static" CssClass="Normal">0</asp:Label>
                    </td>
                    <td>
                        <div style="visibility: hidden">
                            <asp:Label runat="server" ID="lblCosteCabeceraTotal_hdd" ClientIDMode="static">0</asp:Label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="left" runat="server" id="Td3" style="width: 42%;">
                        <div style="float: left; vertical-align: middle;" class="FilaPar">
                            <asp:Label ID="lblDescuentosGenerales" runat="server" Text="DDescuentos Generales:"
                                CssClass="Normal MensajeError" EnableViewState="True">
                            </asp:Label>
                        </div>
                        <div style="border-bottom-style: dotted; border-width: 2px; margin-left: 19%; vertical-align: bottom;
                            height: 8px;">
                        </div>
                    </td>
                    <td align="right" style="width: 8%;">
                        <asp:Label runat="server" ID="lblDescuentosCabeceraTotal" ClientIDMode="static" CssClass="Normal">0</asp:Label>
                    </td>
                    <td>
                        <div style="visibility: hidden">
                            <asp:Label runat="server" ID="lblDescuentosCabeceraTotal_hdd" ClientIDMode="static">0</asp:Label></div>
                    </td>
                </tr>
            </table>
            <div id="campos" style="width: 100%;">
            </div>
        </div>
        <div style="visibility: hidden">
            <asp:Label runat="server" ID="lblImpTotalCuotas_hdd" ClientIDMode="static">0</asp:Label></div>
        <asp:Panel ID="pnlCabeceraImporte" runat="server" Width="100%">
            <table style="width: 100%;">
                <tr>
                    <td runat="server" id="Td4" style="width: 42%;">
                        <div>
                            <div style="float: left; vertical-align: middle;" class="FilaPar">
                                <asp:Label ID="lblImporteTotal" runat="server" Text="DIMPORTE TOTAL:" CssClass="Rotulo"
                                    EnableViewState="True">
                                </asp:Label>
                            </div>
                            <div style="border-bottom-style: dotted; border-width: 2px; margin-left: 19%; vertical-align: bottom;
                                height: 8px;">
                            </div>
                        </div>
                    </td>
                    <td align="right" style="width: 8%;">
                        <asp:Label runat="server" ID="lblImporteTotalCabecera" class="Etiqueta" ClientIDMode="static">0</asp:Label>
                    </td>
                    <td>
                        <div style="visibility: hidden">
                            <asp:Label runat="server" ID="lblImporteTotalCabecera_hdd" ClientIDMode="static">0</asp:Label></div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br />
    </div>
    <!--CABECERA PEDIDO -->
    <div id="DatosGenerales" class="Texto12 RectanguloEP" runat="server" style="overflow: auto;
        font-size: 12px; margin-top: 15px;">
        <br />
        <table cellpadding="1" cellspacing="1" border="0" width="98%" style="margin-left: 10px;">
            <tr>
                <td align="left" style="width: 10%;" colspan="5">
                    <asp:Label ID="lblDatosGenerales" CssClass="Rotulo" Text="DDatosGenerales:" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 15%;">
                    <asp:Label ID="LblProve" CssClass="Etiqueta" Text="DProveedor:" runat="server"></asp:Label>
                </td>
                <td style="width: 30%;">
                    <asp:Label ID="FSNLinkProve" CssClass="Normal" Text="XXXXXX" runat="server" ClientIDMode="Static"></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
                <td style="width: 15%;">
                    <asp:Label ID="LblProveErp" CssClass="Etiqueta" runat="server" ClientIDMode="static">
                    </asp:Label>
                </td>
                <td style="width: 29%;" colspan="4">
                    <asp:Label ID="LblProveErpDato" CssClass="Normal" runat="server" ClientIDMode="static">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 15%;">
                    <asp:Label ID="LblEmpresa" CssClass="Etiqueta" Text="DEmpresa:" runat="server"></asp:Label>
                </td>
                <td style="width: 30%;">
                    <asp:Label ID="LblEmpresaDato" CssClass="Normal" Text="XXXXXX" runat="server" ClientIDMode="Static"></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
                <td align="left" style="width: 15%;">
                    <asp:Label ID="LblTipoPedido" runat="server" Text="DTipo de pedido:" CssClass="Etiqueta"
                        EnableViewState="True">
                    </asp:Label>
                </td>
                <td style="width: 32%;">
                    <asp:Label ID="LblTipoPedidoDato" CssClass="Normal" runat="server" Text="XXXXXX"
                        EnableViewState="True" ClientIDMode="Static">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 15%;">
                    <asp:Label ID="LblReceptor" Text="Receptor:" CssClass="Etiqueta" runat="server"></asp:Label>
                </td>
                <td align="left">
                    <asp:Label ID="LblReceptorDato" CssClass="Normal" Text="XXXXXXXX" runat="server"
                        ClientIDMode="Static"></asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
                <td align="left" style="width: 15%;">
                    <asp:Label ID="LblGestorTitulo" CssClass="Etiqueta" runat="server" Text="DGestor:"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="LblGestorCab" CssClass="Normal" Text="XXXXXXX" runat="server" ClientIDMode="Static"></asp:Label>
                </td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td valign="middle" align="left">
                    <asp:Label ID="LblNumSap" CssClass="Etiqueta" runat="server" Text="DRef. en factura:">
                    </asp:Label>
                </td>
                <td valign="middle" align="left">
                    <asp:Label ID="LblNumSapDato" CssClass="Normal" runat="server" Text="XXXXXXXX" ClientIDMode="Static">
                    </asp:Label>
                </td>
                <td style="width: 5%;">
                </td>
                <td valign="middle" align="left" style="width: 15%;">
                    <asp:Label ID="lblDireccionEnvioFactura" Text="DDir.Envio Factura:" CssClass="Etiqueta"
                        runat="server" ClientIDMode="static"></asp:Label>
                </td>
                <td valign="middle" align="left">
                    <asp:Label ID="lblDireccionEnvioFacturaDato" Text="XXXXXX" CssClass="Normal" runat="server"
                        ClientIDMode="static"></asp:Label>
                    <asp:Label ID="lblCodDireccion" Text="XXXXXX" CssClass="Normal" runat="server" Style="display: none"
                        ClientIDMode="static"></asp:Label>
                </td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td valign="middle" align="left">
                    <asp:Label ID="lblEstIntERP" CssClass="Etiqueta" runat="server" Text="DEst. int. ERP:">
                    </asp:Label>
                </td>
                <td valign="middle" align="left">
                    <asp:Label ID="lblEstIntERPDato" CssClass="Normal" runat="server" Text="XXXXXXXX" ClientIDMode="Static">
                    </asp:Label>
                </td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td align="left" style="width: 15%;">
                    <asp:Label ID="lblObsGen" CssClass="Etiqueta" Text="DObservaciones Generales:" runat="server"></asp:Label>
                </td>
                <td style="width: 70%;" colspan="4">
                    <asp:Label ID="textObser" Text="XXXXXX" CssClass="Normal" runat="server" ClientIDMode="Static"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div id="ArchivosObser" class="Texto12 RectanguloEP" runat="server" style="overflow: auto;
        margin-top: 15px;">
        <br />
        <!-- Panel adjuntos de artículo -->
        <fieldset id="fsTableAdjuntos" runat="server" class="RectanguloEP" style="margin-top: 10px;
            margin-bottom: 10px; width: auto;">
            <legend align="left">
                <asp:Label ID="lblArcPed" CssClass="Rotulo" runat="server" Text="DArchivos para el pedido"
                    ClientIDMode="Static"></asp:Label></legend>
            <table id="tablaAdjuntos" runat="server" style="width: 100%; font-size: 12px; border-collapse: collapse;">
                <tr style="height: 20px; font-weight: bold;">
                    <td style="height: 20px;">
                        <asp:Label ID="lblNombre" runat="server" Text="DNombre"></asp:Label>
                    </td>
                </tr>
            </table>
        </fieldset>
        <br />
    </div>
    <div id="Costes" class="Texto12 RectanguloEP" runat="server" style="overflow: auto;
        margin-top: 15px;">
        <div id="divAtributosCoste" runat="server" style="position: relative;">
            <br />
            <asp:Label ID="lblCostesCabecera" CssClass="Rotulo" Text="DCostesCabecera:" Style="margin-left: 10px;"
                runat="server"></asp:Label>
            <table id="tblAtributosCoste" runat="server" style="border: 1px solid black; margin: auto;
                width: 98%; font-size: 12px; border-collapse: collapse;">
                <tr style="border: 1px solid black; height: 20px; font-weight: bold;">
                    <td style="width: 1%; height: 20px; display: none;">
                        <asp:Label ID="lblIdCoste" runat="server" Text="DId"></asp:Label>
                    </td>
                    <td style="width: 25%; border: 1px solid black; height: 20px">
                        <asp:Label ID="lblCodCoste" runat="server" Text="DCodigo"></asp:Label>
                    </td>
                    <td style="width: 38%; border: 1px solid black; height: 20px">
                        <asp:Label ID="lblDenCoste" runat="server" Text="DDenominacion"></asp:Label>
                    </td>
                    <td style="width: 15%; border: 1px solid black; height: 22px">
                        <asp:Label ID="lblValCoste" runat="server" Text="DValor"></asp:Label>
                    </td>
                    <td style="width: 18%; border: 1px solid black; height: 22px">
                        <asp:Label ID="lblImpCoste" runat="server" Text="DImporte"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <br />
    </div>
    <div id="Descuentos" class="Texto12 RectanguloEP" runat="server" style="overflow: auto;
        margin-top: 15px;">
        <div id="divAtributosDescuento" runat="server">
            <br />
            <asp:Label ID="lblDescuentosCabecera" CssClass="Rotulo" Text="DDescuentosCabecera:"
                Style="margin-left: 10px;" runat="server"></asp:Label>
            <table id="tblAtributosDescuento" runat="server" style="border: 1px solid black;
                margin: auto; width: 98%; border-width: 1; border-color: Black; border-collapse: collapse;
                font-size: 12px;">
                <tr style="border: 1px solid black; height: 20px; font-weight: bold;">
                    <td style="width: 1%; height: 20px; display: none;">
                        <asp:Label ID="lblIdDescuento" runat="server" Text="DId"></asp:Label>
                    </td>
                    <td style="width: 25%; border: 1px solid black; height: 20px">
                        <asp:Label ID="lblCodDescuento" runat="server" Text="DCodigo"></asp:Label>
                    </td>
                    <td style="width: 38%; border: 1px solid black; height: 20px">
                        <asp:Label ID="lblDenDescuento" runat="server" Text="DDenominacion"></asp:Label>
                    </td>
                    <td style="width: 15%; border: 1px solid black; height: 22px">
                        <asp:Label ID="lblValDescuento" runat="server" Text="DValor"></asp:Label>
                    </td>
                    <td style="width: 18%; border: 1px solid black; height: 22px">
                        <asp:Label ID="lblImpDescuento" runat="server" Text="DImporte"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
        </div>
    </div>
    <div id="OtrosDatos" class="Texto12 RectanguloEP" runat="server" style="overflow: auto;
        margin-top: 15px;">
        <br />
        <asp:Label ID="lblOtrosDatos" CssClass="Rotulo" Text="DOtrosDatos:" runat="server"
            Style="margin-left: 10px;"></asp:Label>
        <div id="divAtributosOrden" runat="server" style="position: relative;">
            <table id="tblAtributosOrden" runat="server" style="border: 1px solid black; margin: auto;
                width: 98%; font-size: 12px; table-layout: fixed; border-collapse: collapse;">
                <tr style="border: 1px solid black; height: 20px; font-weight: bold;">
                    <td style="width: 1%; height: 20px; display: none;">
                        <asp:Label ID="lblIdAtributoTablaOrden" runat="server" Text="DId"></asp:Label>
                    </td>
                    <td style="width: 23%; border: 1px solid black; height: 20px">
                        <asp:Label ID="lblCodigoAtributoTablaOrden" runat="server" Text="DCodigo"></asp:Label>
                    </td>
                    <td style="width: 40%; border: 1px solid black; height: 20px">
                        <asp:Label ID="lblDenomAtributoTablaOrden" runat="server" Text="DDenominacion"></asp:Label>
                    </td>
                    <td style="width: 37%; border: 1px solid black; height: 22px">
                        <asp:Label ID="lblValorAtributoTablaOrden" runat="server" Text="DValor"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <br />
    </div>
    <div id="divLineasPedido" class="FilaSeleccionada Texto12" style="border-width: 1px;
        border-style: solid; width: 100%; margin-top: 15px;">
        <table>
            <tr class="FilaSeleccionada" style="height: 30px">
                <td align="left" runat="server" id="Td5">
                    <asp:Label ID="lblLineaPedido" runat="server" Text="DLINEASDEPEDIDO:" CssClass="ButtonClass"
                        EnableViewState="True">
                    </asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div id="divLineas" class="Texto12 RectanguloEP" runat="server" style="overflow: auto; width: 100%;">
        <br />
        <table id="tblLineas" runat="server" style="border: 1px solid black; width: 100%;
            font-size: 12px; table-layout: fixed; border-collapse: collapse;">
            <tr style="border: 1px solid black; min-height: 20px; font-weight: bold;">
                <td style="width: 3%; border: 1px solid black; height: 20px">
                    <asp:Label ID="lblNumberFila" runat="server" Text="DNumber"></asp:Label>
                </td>
                <td style="width: 20%; border: 1px solid black; height: 20px">
                    <asp:Label ID="lblDenArtFila" runat="server" Text="DItem"></asp:Label>
                </td>
                <td style="width: 9%; border: 1px solid black; height: 20px">
                    <asp:Label ID="lblPrecUnitarioFila" runat="server" Text="DPrecioUnitario"></asp:Label>
                </td>
                <td style="width: 9%; border: 1px solid black; height: 22px">
                    <asp:Label ID="lblCantidadFila" runat="server" Text="DCantidad"></asp:Label>
                </td>
                <td style="width: 5%; border: 1px solid black; height: 22px">
                    <asp:Label ID="lblUnidadFila" runat="server" Text="DUnidad"></asp:Label>
                </td>
                <td style="width: 7%; border: 1px solid black; height: 22px">
                    <asp:Label ID="lblCostesFila" runat="server" Text="DCostes"></asp:Label>
                </td>
                <td style="width: 7%; border: 1px solid black; height: 22px">
                    <asp:Label ID="lblDescuentosFila" runat="server" Text="DDescuentos"></asp:Label>
                </td>
                <td style="width: 6%; border: 1px solid black; height: 22px">
                    <asp:Label ID="lblImpBrutoFila" runat="server" Text="DImp.Bruto"></asp:Label>
                </td>
                <td style="width: 5%; border: 1px solid black; height: 22px">
                    <asp:Label ID="lblCantPendFila" runat="server" Text="DCant.Pend"></asp:Label>
                </td>
                <td style="width: 5%; border: 1px solid black; height: 22px">
                    <asp:Label ID="lblImpPendFila" runat="server" Text="DImp.Pend"></asp:Label>
                </td>
                <td style="width: 10%; border: 1px solid black; height: 22px">
                    <asp:Label ID="lblEstadoFila" runat="server" Text="DEstado"></asp:Label>
                </td>
                <td style="width: 7%; border: 1px solid black; height: 22px">
                    <asp:Label ID="lblFecSol" runat="server" Text="DFecSol"></asp:Label>
                </td>
                <td style="width: 7%; border: 1px solid black; height: 22px">
                    <asp:Label ID="lblFecEnt" runat="server" Text="DFecEnt"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
    </div>
    <div id="divIVAs" runat="server" style="display: none;">
        <label id="controlIVAs" runat="server">
        </label>
    </div>
    </form>
</body>
</html>
