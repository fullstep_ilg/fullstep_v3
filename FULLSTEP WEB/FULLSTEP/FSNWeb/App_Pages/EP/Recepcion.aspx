<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master"
    CodeBehind="Recepcion.aspx.vb" Inherits="Fullstep.FSNWeb.Recepcion" Async="True"
    EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/App_Master/Menu.Master" %>
<%@ Register TagPrefix="pag" Src="App_UserControls/PaginadorWebHierarchical.ascx"
    TagName="Paginador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <style type="text/css">
        ul
        {
            margin-top: 0px;
        }
        .RowSelectorCssClass
        {
            width: 15px;
            max-width: 15px;
            padding-left: 8px;
        }
        Div.igg_FullstepPager
        {
            float: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#imgAlbaranBloqueado').attr('src', rutaTheme + '/');

            $('[id$=imgBtnCerrarDetalleAlbaran]').live('click', function () {
                var modal = $find('<%=mpeDetalleAlbaran.ClientID%>');
                modal.hide();
            });
            $('[id$=imgBtnCerrarRegistroRecepcion]').live('click', function () {
                var modal = $find('<%=mpeRegistroRecepcion.ClientID%>');
                modal.hide();
            });
            $('[id$=imgObservaciones]').live('click', function () {
                MostrarFondoPopUp();
                $('#litDetalleAlbaranComentarios').text($('[id$=lblDetalleAlbaranComentarios]').text());
                CentrarPopUp($('#pnlDetalleAlbaranComentarios'));
                $('#pnlDetalleAlbaranComentarios').css('z-index', 200002);
                $('#pnlDetalleAlbaranComentarios').show();
                return false;
            });
            $('[id$=imgDetalleAlbaranComentarios]').live('click', function () {
                OcultarFondoPopUp();
                $('#pnlDetalleAlbaranComentarios').hide();
            });
            $('[id$=btnCancelarConfirm]').live('click', function () {
                OcultarFondoPopUp();
                $('#pnlConfirm').hide();
                return false;
            });
            $('[id$=btnCancelarRegistrarRecepcion]').live('click', function () {
                $find('<%=mpeRegistroRecepcion.ClientID%>').show();
                $find('<%=mpeConfirm.ClientID%>').hide();
                return false;
            });
            //si no se hace esto, los paneles info de pedido y destino aparecen por detr�s del panel de recepci�n.
            $('[id$=FSNlnkRecepcionPedido]').live('click', function () {
                $('[id$=FSNPanelDatosPedido_pnlInfo]').css('z-index', 220002);
            });
            $('[id$=FSNlnkDest]').live('click', function () {
                $('[id$=FSNPanelDatosDestino_pnlInfo]').css('z-index', 220002);
            });
            //si no se hace esto, los paneles tooltip de unidades aparecen por detr�s del panel de recepci�n.
            $('[id$=FSNlnkUniCantPdte]').live('click', function () {
                $('[id$=pnlTooltipTexto_pntlTooltip]').css('z-index', 200002);
            });
            $('[id$=FSNlnkUniCantRec]').live('click', function () {
                $('[id$=pnlTooltipTexto_pntlTooltip]').css('z-index', 200002);
            });
            $('[id$=FSNlnkUniCant]').live('click', function () {
                $('[id$=pnlTooltipTexto_pntlTooltip]').css('z-index', 200002);
            });
            //lo mismo con el detalle del art�culo
            $('[id$=lnkCodArt]').live('click', function () {
                $('[id$=pnlDetalleArticulo_pnlDetArticulo]').css('z-index', 200002);
            });
            $('[id$=lnkDenArt]').live('click', function () {
                $('[id$=pnlDetalleArticulo_pnlDetArticulo]').css('z-index', 200002);
            });

            $.getScript(ruta + 'js/jsTextboxFormat.js');
        });
        function MostrarFondoPopUp() {
            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').css('z-index', 10002);
            $('#popupFondo').show();
        }
        function OcultarFondoPopUp() {
            $('#popupFondo').hide();
            $('#popupFondo').css('z-index', 1001);
        }
        //<%'Funci�n que impide que se puedan seleccionar las l�neas cuya recepci�n est� completa o bien que corresponden a recepciones autom�ticas de planes de entrega%>
        function whdgEntregas_RowSelectionChanged(sender, e) {
            var grid = $find("<%= whdgEntregas.ClientID %>");
            var gridView = grid.get_gridView();

            if (gridView != null) {
                for (i = 1; i <= gridView.get_behaviors().get_selection().get_selectedRows().get_length(); i++) {

                    var ImportePendiente = gridView.get_behaviors().get_selection().get_selectedRows().getItem(i - 1).get_cellByColumnKey("Key_IMPORTELINEAPENDIENTETOTAL").get_value()
                    var CantPdte = gridView.get_behaviors().get_selection().get_selectedRows().getItem(i - 1).get_cellByColumnKey("Key_CANTLINEAPENDIENTETOTAL").get_value()
                    var RecepAuto = gridView.get_behaviors().get_selection().get_selectedRows().getItem(i - 1).get_cellByColumnKey("Key_IM_RECEPAUTO").get_value()
                    var PorcenDesvio = gridView.get_behaviors().get_selection().get_selectedRows().getItem(i - 1).get_cellByColumnKey("Key_PORCEN_DESVIO").get_value()
                    var estadoOrden = gridView.get_behaviors().get_selection().get_selectedRows().getItem(i - 1).get_cellByColumnKey("Key_ORDENEST").get_value()
                    if (PorcenDesvio > 0)
                        CantPdte = CantPdte + ((CantPdte * 100) / PorcenDesvio)

                    if ((estadoOrden == 6 || ImportePendiente == 0 ) || RecepAuto == 1) {
                        gridView.get_behaviors().get_selection().get_selectedRows().remove(gridView.get_behaviors().get_selection().get_selectedRows().getItem(i - 1));
                        i = i - 1;
                    }
                }
            }
        }

        //funcion que muestra el boton de guardar configuracion cuando se redimensiona alguna columna
        function whdgEntregas_ColResized(sender, e) {
            $('[id$=ibGuardarConfiguracion]')[0].style.display = 'inline'

        }
        //funcion que muestra el boton de guardar configuracion cuando se mueve alguna columna
        function whdgEntregas_ColumnMoving(sender, e) {
            $('[id$=ibGuardarConfiguracion]')[0].style.display = 'inline';
        }

        function whdgEntregas_CellClick(sender, e, event) {
            e.set_cancel(true);
            
            switch (e.getNewSelectedCells().getCell(0).get_column().get_key()) {
                case "Key_ALBARAN":
                    if (e.getNewSelectedCells().get_length() == 1 && e.getNewSelectedCells().getCell(0).get_value() != '') {
                        oPar = new Object();
                        oPar.RowIndex = e.getNewSelectedCells().getCell(0).get_row().get_index();
                        oPar.Albaran = e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("Key_ALBARAN").get_value()
                        oPar.FechaRecep = e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("Key_FECHARECEP").get_value()
                        oPar.ProveCod = e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("Key_PROVECOD").get_value()
                        __doPostBack($('[id$=btnDetalleAlbaran]').attr('id'), JSON.stringify(oPar));
                    }
                    break;
                case "Key_ANYOPEDIDOORDEN":
                case "Key_NUM_PED_ERP":
                    var CantPdte = e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("Key_CANTLINEAPENDIENTETOTAL").get_value()
                    var RecepAuto = e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("Key_IM_RECEPAUTO").get_value()
                    var PorcenDesvio = e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("Key_PORCEN_DESVIO").get_value()
                    var estadoOrden = e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("Key_ORDENEST").get_value()
                    if (PorcenDesvio > 0)
                        CantPdte = CantPdte + ((CantPdte * 100) / PorcenDesvio)

                    if ((estadoOrden != 6 && CantPdte != "" && CantPdte > 0) && RecepAuto != 1) {
                        if (e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("Key_IM_RECEPAUTO").get_value() != 1) {
                            oPar = new Object();
                            oPar.LineasPedido = 0
                            oPar.MostrarOpcionCargarTodas = false;
                            oPar.OrdenId = e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("Key_ORDENID").get_value()
                            __doPostBack($('[id$=btnRegistrarRecepcion]').attr('id'), JSON.stringify(oPar));
                        }
                    }
                    break;
                case "Key_NUMLINEA":
                    var CantPdte = e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("Key_CANTLINEAPENDIENTETOTAL").get_value()
                    var RecepAuto = e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("Key_IM_RECEPAUTO").get_value()
                    var PorcenDesvio = e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("Key_PORCEN_DESVIO").get_value()
                    if (PorcenDesvio > 0)
                        CantPdte = CantPdte + ((CantPdte * 100) / PorcenDesvio)
                    if (!((CantPdte == "" || CantPdte <= 0) || RecepAuto == 1)) {
                        oPar = new Object();
                        oPar.LineasPedido = e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("Key_LINEAPEDID").get_value()
                        oPar.MostrarOpcionCargarTodas = false;
                        oPar.OrdenId = 0;
                        __doPostBack($('[id$=btnRegistrarRecepcion]').attr('id'), JSON.stringify(oPar));
                    }
                    break;
                case "Key_PROVEDEN":
                    FSNMostrarPanel('<%=FSNPanelDatosProve.AnimationClientID%>', event, '<%=FSNPanelDatosProve.DynamicPopulateClientID%>', e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("Key_PROVECOD").get_value());
                    break;
                case "Key_APROVISIONADOR":
                    FSNMostrarPanel('<%=FSNPanelDatosPersona.AnimationClientID%>', event, '<%=FSNPanelDatosPersona.DynamicPopulateClientID%>', e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("Key_APROVISIONADORCOD").get_value());
                    break;
                case "Key_DESTDEN":
                    FSNMostrarPanel('<%=FSNPanelDatosDestino.AnimationClientID%>', event, '<%=FSNPanelDatosDestino.DynamicPopulateClientID%>', e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("Key_DESTCOD").get_value() + '#' + e.getNewSelectedCells().getCell(0).get_row().get_cellByColumnKey("Key_LINEAPEDID").get_value());
                    break;
            }
        }
        //<%'Muestra el panel para efectuar la recepci�n%>
        function RegistrarRecepcion() {
            var grid = $find($('[id$=whdgEntregas]').attr('id'));
            var lineasPedido = '';
            for (i = 0; i < grid.get_gridView().get_behaviors().get_selection().get_selectedRows().get_length(); i++) {
                lineasPedido += (lineasPedido == '' ? '' : ',') + grid.get_gridView().get_behaviors().get_selection().get_selectedRows().getItem(i).get_cellByColumnKey('Key_LINEAPEDID').get_value();
            }
            if (lineasPedido != '') {
                oPar = new Object();
                oPar.LineasPedido = lineasPedido;
                oPar.MostrarOpcionCargarTodas = true;
                oPar.OrdenId = 0;
                __doPostBack($('[id$=btnRegistrarRecepcion]').attr('id'), JSON.stringify(oPar));
            }
        }
        //<%'funcion que se ejecuta cuando se cambia el valor de la cantidad recibida y que habilita el boton de guardar%>
        function TextChange(oEdit, newText, oEvent) {
            var btnGuardar = $('#' + oEdit._element.getAttribute("btnGuardarID"));
            if (oEdit.value > 0) {
                btnGuardar.show();
            } else {
                btnGuardar.hide();
            }

            var vdecimalfmt = oEdit._element.getAttribute("vdecimalfmt");
            var vthousanfmt = oEdit._element.getAttribute("vthousanfmt");
            var numdec = oEdit._element.getAttribute("numdec");
            var PrecioUni = oEdit._element.getAttribute("PrecioUni");
            var lblImporte = $('#' + oEdit._element.getAttribute('lblImporte'));

            //Actualizamos el importe al cambiar la cantidad
            PrecioUni = PrecioUni.replace(' ', '');
            PrecioUni = PrecioUni.replace(',', '.');
            while (PrecioUni.indexOf(".") != PrecioUni.lastIndexOf(".")) {
                PrecioUni = PrecioUni.substring(0, PrecioUni.indexOf(".") - 1) + PrecioUni.slice(PrecioUni.indexOf(".") + 1);
            }
            var strImporte = (parseFloat(PrecioUni) * oEdit.getValue()).toString()
            var BigNumber = str2num(strImporte, '.', '')
            lblImporte.text(num2str(BigNumber, vdecimalfmt, vthousanfmt, numdec));
        }
        function ModificarLineaRecepcion(idLineaRecep, idtxtCantidadRecibida, idbtnGuardar, idlblPrecioUni, PrecioUni) {
            var CantidadRecibida = igedit_getById(idtxtCantidadRecibida)._vs;
            params = { Cantidad: CantidadRecibida, LineaRecep: idLineaRecep }
            $.when($.ajax({
                type: "POST",
                url: rutaFS + 'EP/App_Services/EP_Pedidos.asmx/ModificarLineaRecepcion',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(params),
                dataType: "json",
                async: true
            })).done(function (msg) {
                var respuesta = msg.d;
                $('[id$=lblConfirm]').text(respuesta.TextoPregunta);
                $('[id$=txtObservacionesBloqueoFacturacion]').hide();
                switch (respuesta.Respuesta) {
                    case 1:
                    case 2:
                        $('[id$=imgPnlConfirmRecep]').attr('src', ruta + 'Images/icono_info.gif');
                        MostrarFondoPopUp();
                        CentrarPopUp($('#pnlConfirm'));
                        //<%'Parece que a�adir un modalPopUpExtender al control pnlConfirm altera caracter�sticas como el z-index. No queda m�s remedio que sobreescribirlo%>
                        $('#pnlConfirm').css('z-index', '200002');
                        $('#pnlConfirm').show();
                        $('[id$=btnCancelarConfirm]').show();
                        $('[id$=btnAceptarConfirmServer]').hide();
                        $('[id$=btnCancelarRegistrarRecepcion]').hide();
                        $('[id$=btnAceptarConfirm]').unbind('click');
                        $('[id$=btnAceptarConfirm]').click(function () {
                            GrabarModificacionLinea(idLineaRecep, respuesta.Respuesta, idbtnGuardar, idtxtCantidadRecibida, idlblPrecioUni, PrecioUni);
                            OcultarFondoPopUp();
                            $('#pnlConfirm').hide();
                            return false;
                        });
                        $('[id$=btnAceptarConfirm]').show();
                        return false;
                        break;
                    case 3:
                        $('[id$=imgPnlConfirmRecep]').attr('src', ruta + 'Images/alert-large.gif');
                        MostrarFondoPopUp();
                        CentrarPopUp($('#pnlConfirm'));
                        //<%'Parece que a�adir un modalPopUpExtender al control pnlConfirm altera caracter�sticas como el z-index. No queda m�s remedio que sobreescribirlo%>
                        $('#pnlConfirm').css('z-index', '200002');
                        $('#pnlConfirm').show();
                        $('[id$=btnCancelarConfirm]').hide();
                        $('[id$=btnAceptarConfirmServer]').hide();
                        $('[id$=btnCancelarRegistrarRecepcion]').hide();
                        $('[id$=btnAceptarConfirm]').unbind('click');
                        $('[id$=btnAceptarConfirm]').click(function () {
                            OcultarFondoPopUp();
                            $('#pnlConfirm').hide();
                            return false;
                        });
                        $('[id$=btnAceptarConfirm]').show();
                        break;
                    case 0:
                        GrabarModificacionLinea(idLineaRecep, respuesta.Respuesta, idbtnGuardar, idtxtCantidadRecibida, idlblPrecioUni, PrecioUni)
                        break;
                }
            });
        }
        //funcion que hace la llamada al WS para que guarde la nueva cantidad recepcionada
        function GrabarModificacionLinea(idLineaRecep, idCaso, idbtnGuardar, idtxtCantidadRecibida, idlblPrecioUni, PrecioUni) {
            var CantidadRecibida = igedit_getById(idtxtCantidadRecibida);
            params = { Cantidad: CantidadRecibida._vs, LineaRecep: idLineaRecep, Caso: idCaso };
            $.when($.ajax({
                type: "POST",
                url: rutaFS + 'EP/App_Services/EP_Pedidos.asmx/GrabarModificacionLineaRecepcion',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(params),
                dataType: "json",
                async: true
            })).done(function (msg) {
                var respuesta = msg.d;
                CantidadRecibida.setValue(respuesta.CantidadGrabada);
                $('#' + idbtnGuardar).hide();

                //Actualizamos el importe de la linea del panel de recepciones
                var vdecimalfmt = CantidadRecibida._element.getAttribute("vdecimalfmt");
                var vthousanfmt = CantidadRecibida._element.getAttribute("vthousanfmt");
                var numdec = CantidadRecibida._element.getAttribute("numdec");

                PrecioUni = PrecioUni.replace(' ', '');
                PrecioUni = PrecioUni.replace(',', '.');
                while (PrecioUni.indexOf(".") != PrecioUni.lastIndexOf(".")) {
                    PrecioUni = PrecioUni.substring(0, PrecioUni.indexOf(".") - 1) + PrecioUni.slice(PrecioUni.indexOf(".") + 1);
                }
                var strImporte = (parseFloat(PrecioUni) * respuesta.CantidadGrabada).toString();
                var BigNumber = str2num(strImporte, '.', '');
                $('#' + idlblPrecioUni).text(num2str(BigNumber, vdecimalfmt, vthousanfmt, numdec));
                __doPostBack($('[id$=btnRecargaGrid]').attr('id'), 'RecargarGrid');
            });
        }
        function ConfirmarAnularAlbaran(albaran, fechaAlbaran, proveAlbaran) {
            $('[id$=imgPnlConfirmRecep]').attr('src', ruta + 'Images/icono_info.gif');
            MostrarFondoPopUp();
            $('[id$=lblConfirm]').text(TextosBotonConfirmar[1].replace('XXXXX', albaran));
            $('[id$=txtObservacionesBloqueoFacturacion]').hide();
            CentrarPopUp($('#pnlConfirm'));
            //<%'Parece que a�adir un modalPopUpExtender al control pnlConfirm altera caracter�sticas como el z-index. No queda m�s remedio que sobreescribirlo%>
            $('#pnlConfirm').css('z-index', '200002');
            $('#pnlConfirm').show();
            $('[id$=CommandName]').val('AnularAlbaran');
            $('[id$=btnCancelarConfirm]').show();
            $('[id$=btnAceptarConfirmServer]').hide();
            $('[id$=btnCancelarRegistrarRecepcion]').hide();
            $('[id$=btnAceptarConfirm]').unbind('click');
            $('[id$=btnAceptarConfirm]').click(function () {
                params = { Albaran: albaran, FechaAlbaran: fechaAlbaran, Prove: proveAlbaran };
                $.when($.ajax({
                    type: "POST",
                    url: rutaFS + 'EP/Recepcion.aspx/AnularRecepcion',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(params),
                    dataType: "json",
                    async: true
                })).done(function (msg) {
                    $('[id$=btnCancelarConfirm]').hide();
                    var e = String(msg.d);
                    var arrayRdos = e.split("@@");
                    var todoOK = true;
                    var todoKO = true;
                    for (var i = 0; i <= arrayRdos.length - 1; i++) {
                        if (arrayRdos[i] != '0') {
                            todoOK = false;
                        }
                        else {
                            todoKO = false;
                        }
                    }
                    if (todoOK == true) {
                        var modal = $find('<%=mpeDetalleAlbaran.ClientID%>');
                        modal.hide();
                        $('[id$=lblConfirm]').text(TextosBotonConfirmar[2]);
                        $('[id$=txtObservacionesBloqueoFacturacion]').hide();
                        $('[id$=btnAceptarConfirm]').unbind('click');
                        $('[id$=btnAceptarConfirm]').click(function () {
                            $('#pnlConfirm').hide();
                            OcultarFondoPopUp();
                            //RecargarGrid                            
                            __doPostBack($('[id$=btnRecargaGrid]').attr('id'), 'RecargarGrid');
                            return false;
                        });
                    } else {
                        $('[id$=imgPnlConfirmRecep]').attr('src', ruta + 'Images/alert-large.gif');
                        $('[id$=btnAceptarConfirm]').unbind('click');
                        $('[id$=btnAceptarConfirm]').click(function () {
                            OcultarFondoPopUp();
                            $('#pnlConfirm').hide();
                            return false;
                        });
                        $('[id$=lblConfirm]').text('');
                        $('[id$=txtObservacionesBloqueoFacturacion]').hide();
                        var msgRdo = new String('');
                        switch (todoKO) {
                            case true:
                                msgRdo = TextosError[6] + '\n\n'
                            case false:
                                msgRdo = TextosError[5] + '\n\n';
                        }
                        for (var i = 0; i <= arrayRdos.length - 1; i++) {
                            switch (arrayRdos[i]) {
                                case '166':
                                    msgRdo += TextosError[0] + '\n';
                                case '167':
                                    msgRdo += TextosError[1] + '\n';
                                case '168':
                                    msgRdo += TextosError[2] + '\n';
                                case '169':
                                    msgRdo += TextosError[3] + '\n';
                                case '1':
                                    msgRdo += TextosError[4] + '\n';
                                case '0':
                                    //Si entra aqu� implica que ha habido alguna anulaci�n correcta pero otras no se han podido efectuar
                                    //Por lo que asociamos cerrar el panel a un refresco de la p�gina para mostrar los cambios
                                    $('[id$=btnAceptarConfirm]').unbind('click');
                                    $('[id$=btnAceptarConfirm]').click(function () {
                                        OcultarFondoPopUp();
                                        $('#pnlConfirm').hide();
                                        //RecargarGrid                            
                                        __doPostBack($('[id$=btnRecargaGrid]').attr('id'), 'RecargarGridyPanelRecep');
                                        return false;
                                    });
                            }
                        }
                        $('[id$=lblConfirm]').text(msgRdo);
                    }
                });
                return false;
            });
            $('[id$=btnAceptarConfirm]').show();
        }
        function AnularLineaRecep(idLineaRecep) {
            $('[id$=imgPnlConfirmRecep]').attr('src', ruta + 'Images/icono_info.gif');
            MostrarFondoPopUp();
            $('[id$=lblConfirm]').text(TextosBotonConfirmar[0]);
            $('[id$=txtObservacionesBloqueoFacturacion]').hide();
            CentrarPopUp($('#pnlConfirm'));
            //<%'Parece que a�adir un modalPopUpExtender al control pnlConfirm altera caracter�sticas como el z-index. No queda m�s remedio que sobreescribirlo%>
            $('#pnlConfirm').css('z-index', '200002');
            $('#pnlConfirm').show();
            $('[id$=CommandName]').val('AnularLinea');
            $('[id$=CommandArgument]').val(idLineaRecep);
            $('[id$=btnCancelarConfirm]').show();
            $('[id$=btnAceptarConfirmServer]').hide();
            $('[id$=btnCancelarRegistrarRecepcion]').hide();
            $('[id$=btnAceptarConfirm]').unbind('click');
            $('[id$=btnAceptarConfirm]').click(function () {
                params = { idLineaRecep: idLineaRecep };
                $.when($.ajax({
                    type: "POST",
                    url: rutaFS + 'EP/Recepcion.aspx/AnularLineaRecepcion',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(params),
                    dataType: "json",
                    async: true
                })).done(function (msg) {
                    $('[id$=btnCancelarConfirm]').hide();
                    $('[id$=lblConfirm]').text(TextosBotonConfirmar[4]);
                    $('[id$=txtObservacionesBloqueoFacturacion]').hide();
                    $('[id$=btnAceptarConfirm]').unbind('click');
                    OcultarFondoPopUp();
                    $('#pnlConfirm').hide();
                    //RecargarGrid                            
                    __doPostBack($('[id$=btnRecargaGrid]').attr('id'), 'RecargarGridyPanelRecep');
                    return false;
                });
                OcultarFondoPopUp();
                $('#pnlConfirm').hide();
                return false;
            });
            $('[id$=btnAceptarConfirm]').show();
        }
        function BloquearFacturacion(bloquear, albaran, ordenId) {
            if (bloquear) {
                $('[id$=imgPnlConfirmRecep]').attr('src', ruta + 'Images/icono_info.gif');
                MostrarFondoPopUp();
                $('[id$=lblConfirm]').html(TextosBotonConfirmar[3].replace('XXXXX', albaran));
                $('[id$=txtObservacionesBloqueoFacturacion]').val('');
                $('[id$=txtObservacionesBloqueoFacturacion]').show();
                CentrarPopUp($('#pnlConfirm'));
                //<%'Parece que a�adir un modalPopUpExtender al control pnlConfirm altera caracter�sticas como el z-index. No queda m�s remedio que sobreescribirlo%>
                $('#pnlConfirm').css('z-index', '200002');
                $('#pnlConfirm').show();
                $('[id$=btnCancelarConfirm]').show();
                $('[id$=btnAceptarConfirmServer]').hide();
                $('[id$=btnCancelarRegistrarRecepcion]').hide();
                $('[id$=btnAceptarConfirm]').unbind('click');
                $('[id$=btnAceptarConfirm]').click(function () {
                    params = { Bloquear: bloquear, Albaran: albaran, OrdenId: ordenId, Observaciones: $('[id$=txtObservacionesBloqueoFacturacion]').val() };
                    $.when($.ajax({
                        type: "POST",
                        url: rutaFS + 'EP/Recepcion.aspx/BloquearFacturacion',
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(params),
                        dataType: "json",
                        async: true
                    })).done(function (msg) {
                        var e = parseInt(msg.d);
                        if (e == 0) {
                            OcultarFondoPopUp();
                            $('#pnlConfirm').hide();
                            var modal = $find('<%=mpeDetalleAlbaran.ClientID%>');
                            modal.hide();
                            __doPostBack($('[id$=btnRecargaGrid]').attr('id'), 'RecargarGrid');
                        } else {
                            $('[id$=btnCancelarConfirm]').hide();
                            $('[id$=imgPnlConfirmRecep]').attr('src', ruta + 'Images/alert-large.gif');
                            $('[id$=btnAceptarConfirm]').unbind('click');
                            $('[id$=btnAceptarConfirm]').click(function () {
                                OcultarFondoPopUp();
                                $('#pnlConfirm').hide();
                                return false;
                            });
                            $('[id$=lblConfirm]').text(TextosError[4]);
                            $('[id$=txtObservacionesBloqueoFacturacion]').hide();
                        }
                    });
                    return false;
                });
                $('[id$=btnAceptarConfirm]').show();
            } else {
                params = { Bloquear: bloquear, Albaran: albaran, OrdenId: ordenId, Observaciones: '' };
                $.when($.ajax({
                    type: "POST",
                    url: rutaFS + 'EP/Recepcion.aspx/BloquearFacturacion',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(params),
                    dataType: "json",
                    async: true
                })).done(function (msg) {
                    var e = parseInt(msg.d);
                    if (e == 0) {
                        $('[id$=btnDesbloquearFacturacion]').hide();
                        $('[id$=btnBloquearFacturacion]').show();
                        $('[id$=pnlAlbaranBloqueado]').hide();
                        __doPostBack($('[id$=btnRecargaGrid]').attr('id'), 'RecargarGrid');
                    } else {
                        $('[id$=btnCancelarConfirm]').hide();
                        $('[id$=btnAceptarConfirmServer]').hide();
                        $('[id$=btnCancelarRegistrarRecepcion]').hide();
                        $('[id$=imgPnlConfirmRecep]').attr('src', ruta + 'Images/alert-large.gif');
                        $('[id$=btnAceptarConfirm]').unbind('click');
                        $('[id$=btnAceptarConfirm]').click(function () {
                            OcultarFondoPopUp();
                            $('#pnlConfirm').hide();
                            return false;
                        });
                        $('[id$=btnAceptarConfirm]').show();
                        $('[id$=lblConfirm]').text(TextosError[4]);
                        $('[id$=txtObservacionesBloqueoFacturacion]').hide();
                    }
                });
            }
        }
        //Funci�n que oculta el panel de mensaje
        function CerrarMensaje() {
            $find('<%=mpeMensaje.ClientID %>').hide();
        }
        function pageLoad() {
            var popup = $find('<%=mpeMensaje.ClientID %>');
            popup.add_shown(SetzIndexModalPopUp);
        }
        //''' <summary>
        //''' Initialize whdgEntregasExportacion
        //''' </summary>
        //''' <remarks>Llamada desde: whdgEntregasExportacion ClientEvents ; Tiempo m�ximo: 0,2</remarks>
        function whdgEntregasExportacion_Initialize(sender,e){
            ig_controls.<%= whdgEntregasExportacion.ClientID %>._callbackManager.setTimeout(60000);
            ig_controls.<%= whdgEntregasExportacion.ClientID %>.get_gridView()._callbackManager.setTimeout(60000);
        }
    </script>
    <!-- Panel Activos -->
    <input id="btnOcultoActivos" runat="server" type="button" style="display: none;" />
    <fsn:SMActivos ID="SMActivos" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left" Width="800px" Style="display: none;
        padding: 3px" AllowSelection="True" />
    <ajx:ModalPopupExtender ID="mpeActivos" runat="server" BackgroundCssClass="modalBackground"
        TargetControlID="btnOcultoActivos" PopupControlID="SMActivos" />
    <%-- Panel Configuracion Grid --%>
    <asp:UpdatePanel ID="upConfiguracionGrid" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <input id="btnOcultoConfig" type="button" value="button" runat="server" style="display: none" />
            <asp:Panel ID="pnlConfig" runat="server" BackColor="White" BorderColor="DimGray"
                BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Center" Style="display: none;
                padding: 5px" Width="1050px">
                <div class="PanelCabecera" style="height:30px; text-align:left; padding:0.3em 0em;">
                    <asp:Image ID="imgPanelConfiguracion" runat="server" style="display:inline-block; vertical-align:middle; margin-left:1em;" />
                    <asp:Label ID="lblPanelConfiguracion" runat="server" CssClass="TextoClaro" style="display:inline-block; vertical-align:sub;"></asp:Label>
                </div>
                <fieldset style="padding:5px; margin-top:1em;">
                    <legend style="vertical-align: top">
                        <asp:Label ID="lblConfig" runat="server" Text="DMostrar los siguientes Campos" class="etiquetaBold"></asp:Label>
                    </legend>
                    <table id="tblPnlConfig" runat="server" cellpadding="0" cellspacing="2" border="0" style="text-align:left; width:100%;"></table>
                </fieldset>
				<div style="display:inline-block; width:49%; padding-right:0.5em; margin-top:0.5em;">
					<fsn:FSNButton ID="btnAceptarConfiguracion" runat="server" Alineacion="Right"
                        OnClientClick="LimpiarVariablesConfiguracion();cerrarPanelConfiguracion();">
                    </fsn:FSNButton>
				</div>
				<div style="display:inline-block; width:49%; padding-left:0.5em; margin-top:0.5em;">
					<fsn:FSNButton ID="btnCancelarConfiguracion" runat="server" Text="DCancelar" Alineacion="Left"
                        OnClientClick="cerrarPanelConfiguracion(); return false;">
                    </fsn:FSNButton>
				</div>                
            </asp:Panel>
            <ajx:ModalPopupExtender ID="mpeConfig" runat="server" BackgroundCssClass="modalBackground"
                PopupControlID="pnlConfig" TargetControlID="btnOcultoConfig">
            </ajx:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- Panel Info Proveedor -->
    <fsn:FSNPanelInfo ID="FSNPanelDatosProve" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx"
        ServiceMethod="Proveedor_DatosContacto" TipoDetalle="1" />
    <!-- Panel Info Pedido Fullstep -->
    <fsn:FSNPanelInfo ID="FSNPanelDatosPedido" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx"
        ServiceMethod="Pedido_Datos" TipoDetalle="7" Width="0" />
    <!-- Panel Persona -->
    <fsn:FSNPanelInfo ID="FSNPanelDatosPersona" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx"
        ServiceMethod="Persona_Datos" />
    <!-- Panel Destino -->
    <fsn:FSNPanelInfo ID="FSNPanelDatosDestino" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx"
        ServiceMethod="Destino_Datos" Height="180px" />
    <!-- Panel Activo -->
    <fsn:FSNPanelInfo ID="FSNPanelDatosActivo" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx"
        ServiceMethod="Activo_Datos" Height="280px" BackColor="White" />
    <!-- Panel Partida Presupuestaria (Contrato) -->
    <fsn:FSNPanelInfo ID="FSNPanelDatosPartida" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx"
        ServiceMethod="PartidaPresupuestaria_Datos" Height="320px" BackColor="White" />
    <!-- Paneles Tooltip -->
    <fsn:FSNPanelTooltip ID="pnlTooltipObservaciones" runat="server" ServiceMethod="Observaciones">
    </fsn:FSNPanelTooltip>
    <fsn:FSNPanelTooltip ID="pnlTooltipObservacionesProv" runat="server" ServiceMethod="ObservacionesProv">
    </fsn:FSNPanelTooltip>
    <fsn:FSNPanelTooltip ID="pnlTooltipObservacionesLinea" runat="server" ServiceMethod="ObservacionesLinea">
    </fsn:FSNPanelTooltip>
    <fsn:FSNPanelTooltip ID="pnlTooltipTexto" runat="server" ServiceMethod="DevolverTexto">
    </fsn:FSNPanelTooltip>
    <!-- Panel Categor�as -->
    <asp:UpdatePanel ID="updpnlPopupCategorias" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <input id="btnOcultoCat" type="button" value="button" runat="server" style="display: none" />
            <asp:Panel ID="PanelOculto" runat="server" Style="display: none"></asp:Panel>
            <asp:Panel ID="PanelArbol" runat="server" BackColor="White" BorderColor="DimGray"
                BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display:none; min-width:350px; padding:2px" 
				onmouseout="FSNCerrarPanelesTemp()" onmouseover="FSNNoCerrarPaneles()">
                <table width="350">
                    <tr>
                        <td align="left">
                            <asp:Image ID="ImgCatalogoArbol" runat="server" ImageUrl="~/images/categorias_small.gif" />
                            &nbsp; &nbsp;
                            <asp:Label ID="LblCatalogoArbol" runat="server" CssClass="Rotulo">
                            </asp:Label>
                        </td>
                        <td align="right">
                            <asp:ImageButton runat="server" ID="ImgBtnCerrarPanelArbol" ImageUrl="~/images/Bt_Cerrar.png" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:UpdatePanel ID="pnlCategorias" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:TreeView ID="fsnTvwCategorias" NodeIndent="20" CssClass="Normal" ShowExpandCollapse="true"
                                                    ExpandDepth="0" EnableViewState="True" runat="server" EnableClientScript="true"
                                                    ForeColor="Black" PopulateNodesFromClient="true">
                                                    <Nodes>
                                                        <asp:TreeNode PopulateOnDemand="true" Value="_0_" SelectAction="None" Expanded="false">
                                                        </asp:TreeNode>
                                                    </Nodes>
                                                </asp:TreeView>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajx:ModalPopupExtender ID="mpePanelArbol" runat="server" PopupControlID="PanelArbol"
                TargetControlID="btnOcultoCat" CancelControlID="ImgBtnCerrarPanelArbol" RepositionMode="None"
                PopupDragHandleControlID="PanelOculto">
            </ajx:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hid_CodArticulo" runat="server" />
    <asp:HiddenField ID="hid_lineaPedido" runat="server" />
    <asp:HiddenField ID="hid_OrdenEntrega" runat="server" />
    <asp:HiddenField ID="hid_adjuntosRecepcion" runat="server" />
    <!-- Panel Adjuntos -->
    <asp:Button ID="btnMostrarPopUpAdjuntos" runat="server" EnableViewState="false" Style="display: none" />
    <asp:UpdatePanel ID="updpnlPopupAdjuntos" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnMostrarPopUpAdjuntos" EventName="Click" />
            <asp:PostBackTrigger ControlID="repAdjuntosOrden" />
            <asp:PostBackTrigger ControlID="repAdjuntosLineaPedido" />
            <asp:PostBackTrigger ControlID="repAdjuntosRecepcion" />
        </Triggers>
        <ContentTemplate>
            <input id="btnOcultoAdj" type="button" value="button" runat="server" style="display: none" />
            <asp:Panel ID="pnlAdjuntos" runat="server" BackColor="White" BorderColor="DimGray"
                BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left" Style="display: none;
                min-width: 450px; padding: 2px" Width="450px">
                <div style='width: 100%; z-index: 10000; position: absolute;'>
                    <div style='width: 100%; height: 35px;' class='CapaTituloPanelInfo'>
                        &nbsp;</div>
                    <div style='width: 100%; height: 25px;' class='CapaSubTituloPanelInfo'>
                        &nbsp;</div>
                </div>
                <table cellspacing="0" cellpadding="2" width="100%" border="0" style="z-index: 10002;
                    position: relative;">
                    <tr style="height: 35px;">
                        <td width="1%" style="background-color: Transparent;" rowspan="2">
                            <asp:Image ID="imgAdjunto" runat="server" />
                        </td>
                        <td align="left">
                            <asp:Label ID="lblTituloAdjuntos" runat="server" CssClass="TituloPopUpPanelInfo"></asp:Label>
                        </td>
                        <td id="cellImagenCerrar" align="right" valign="top" style="width: 1%">
                            <asp:Image ID="ImgBtnCerrarpnlAdjuntos" runat="server" Style="cursor: pointer" />
                        </td>
                    </tr>
                    <tr style="height: 25px;">
                        <td align="left" valign="top">
                            <asp:Label ID="lblSubtitulo" runat="server" CssClass="SubTituloPopUpPanelInfo"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="background-color: White">
                            <asp:Repeater ID="repAdjuntosOrden" runat="server">
                                <HeaderTemplate>
                                    <table id="tblAdjuntosPedido" width="100%">
                                        <tr>
                                            <td colspan="3">
                                                <asp:Label ID="lblAdjuntosPedido" CssClass="Rotulo" runat="server" Text="DAdjuntos del Pedido"></asp:Label>
                                            </td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 50%">
                                            <asp:LinkButton ID="linkAdjuntoPedido" runat="server" CommandName="DescargarAdjunto"></asp:LinkButton>
                                            <asp:HiddenField ID="hid_idAdjuntoPedido" runat="server" />
                                            <asp:HiddenField ID="hid_NombreAdjuntoPedido" runat="server" />
                                            <asp:HiddenField ID="hid_DatasizeAdjuntoPedido" runat="server" />
                                        </td>
                                        <td style="width: 35%">
                                            <asp:Label ID="lblFechaAdjPedido" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%">
                                            <asp:Image ID="imgComentarioAdjuntoPedido" runat="server" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:Repeater ID="repAdjuntosLineaPedido" runat="server">
                                <HeaderTemplate>
                                    <table id="tblAdjuntosLineaPedido" width="100%">
                                        <tr>
                                            <td colspan="3">
                                                <asp:Label ID="lblAdjuntosLineaPedido" CssClass="Rotulo" runat="server" Text="DAdjuntos de la linea"></asp:Label>
                                            </td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 50%">
                                            <asp:LinkButton ID="linkAdjuntoLineaPedido" runat="server" CommandName="DescargarAdjunto"></asp:LinkButton>
                                            <asp:HiddenField ID="hid_idAdjuntoLineaPedido" runat="server" />
                                            <asp:HiddenField ID="hid_NombreAdjuntoLineaPedido" runat="server" />
                                            <asp:HiddenField ID="hid_DatasizeAdjuntoLineaPedido" runat="server" />
                                        </td>
                                        <td style="width: 35%">
                                            <asp:Label ID="lblFechaAdjLineaPedido" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%">
                                            <asp:Image ID="imgComentarioAdjuntoLineaPedido" runat="server" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="background-color: White">
                            <asp:Repeater ID="repAdjuntosRecepcion" runat="server">
                                <HeaderTemplate>
                                    <table id="tblAdjuntosPedido" width="100%">
                                        <tr>
                                            <td colspan="3">
                                                <asp:Label ID="lblAdjuntosRecepcion" CssClass="Rotulo" runat="server" Text="DAdjuntos de la Recepcion"></asp:Label>
                                            </td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 50%">
                                            <asp:LinkButton ID="linkAdjuntoRecepcion" runat="server" CommandName="DescargarAdjunto"></asp:LinkButton>
                                            <asp:HiddenField ID="hid_idAdjuntoRecepcion" runat="server" />
                                            <asp:HiddenField ID="hid_NombreAdjuntoRecepcion" runat="server" />
                                            <asp:HiddenField ID="hid_DatasizeAdjuntoRecepcion" runat="server" />
                                            <%--<asp:HiddenField ID="hid_DataAdjuntoRecepcion" runat="server" />--%>
                                        </td>
                                        <td style="width: 35%">
                                            <asp:Label ID="lblFechaAdjRecepcion" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%">
                                            <asp:Image ID="imgComentarioAdjuntoRecepcion" runat="server" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajx:ModalPopupExtender ID="mpeAdj" runat="server" BackgroundCssClass="modalBackground"
                CancelControlID="ImgBtnCerrarpnlAdjuntos" PopupControlID="pnlAdjuntos" TargetControlID="btnOcultoAdj">
            </ajx:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- Panel Detalle Art�culo -->
    <asp:Button ID="btnMostrarDetalleArticulo" runat="server" EnableViewState="false"
        Style="display: none" />
    <asp:UpdatePanel ID="upDetalleArticulo" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnMostrarDetalleArticulo" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <fsep:DetArticuloPedido ID="pnlDetalleArticulo" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- Panel Mensaje -->
    <input id="btnOcultoMensaje" runat="server" type="button" style="display: none;" />
    <asp:Panel ID="pnlMensaje" runat="server" BackColor="#FAFAFA" Width="300px" Height="180px"
        BorderColor="DimGray" BorderWidth="1" BorderStyle="Solid" Style="display: none;">
        <asp:UpdatePanel ID="upMensaje" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table cellpadding="10">
                    <tr>
                        <td style="width:60">
                            <asp:Image runat="server" ID="ImgAlerta" ImageAlign="Left" ImageUrl="~/images/Icono_Error_Amarillo_40x40.gif" />
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblTituloMensaje" CssClass="Rotulo"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label runat="server" ID="lblMensaje"></asp:Label>
                        </td>
                    </tr>
                </table>
                <center>
                    <br />
                    <table>
                        <tr>
                            <td>
                                <fsn:FSNButton ID="BtnAceptarMensaje" runat="server" Alineacion="Right" OnClientClick="CerrarMensaje(); return false;"></fsn:FSNButton>
                            </td>
                        </tr>
                    </table>
                </center>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <button id="BtnOcEmiPed" runat="server" style="display:none"></button>
    <ajx:ModalPopupExtender ID="mpeMensaje" runat="server" TargetControlID="btnOcultoMensaje"
        PopupControlID="pnlMensaje" DropShadow="false" CancelControlID="BtnAceptarMensaje">
    </ajx:ModalPopupExtender>
    <!-- Fin Panel Mensaje -->
    <%--Panel Selecci�n centro de coste --%>
    <input id="btnCentrosOculto" type="button" value="button" runat="server" style="display: none" />
    <asp:Panel ID="pnlCentrosOculto" runat="server" Style="display:none"></asp:Panel>
    <asp:Panel ID="pnlCentros" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display: none;
        padding: 2px">
        <table width="450">
            <tr>
                <td align="left" style="line-height: 30px;">
                    <img src="../../Images/centrocoste.JPG" style="padding-left: 10px; vertical-align: middle" />
                    <asp:Label ID="lblCentros" runat="server" CssClass="RotuloGrande" Style="padding-left: 10px;">
                    </asp:Label>
                </td>
                <td align="right" valign="top">
                    <asp:ImageButton runat="server" ID="ImgBtnCerrarPanelCentros" ImageUrl="~/images/Bt_Cerrar.png"
                        Style="vertical-align: top;" OnClientClick="ocultarPanelModal('mpeCentros');" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblFiltroCentros" runat="server" CssClass="Rotulo" />
                    <input class="Normal" type="text" id="FiltroCentros" runat="server" style="margin-bottom: 5px;
                        width: 320px;" />
                    <div id="listadoCentros" runat="server" style="height: 400px; overflow: auto; border-top: solid 1px #909090;
                        clear: both;">
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpeCentros" runat="server" PopupControlID="pnlCentros"
        TargetControlID="btnCentrosOculto" CancelControlID="ImgBtnCerrarPanelCentros"
        RepositionMode="None" PopupDragHandleControlID="pnlCentrosOculto" ClientIDMode="Static">
    </ajx:ModalPopupExtender>
    <%--Fin Panel Selecci�n centro de coste --%>
    <%--Panel Seleccion Partidas--%>
    <input id="btnPartidasOculto" type="button" value="button" runat="server" style="display: none" />
    <asp:Panel ID="pnlPartidasOculto" runat="server" Style="display: none">
    </asp:Panel>
    <asp:Panel ID="pnlPartidas" runat="server" BackColor="White" BorderColor="DimGray"
        BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Left" Style="display: none;
        padding: 2px">
        <table width="450">
            <tr>
                <td align="left">
                    <img src="../../Images/centrocoste.JPG" style="padding-left: 10px; vertical-align: middle;" />
                    <asp:Label ID="lblPartidaPres0" runat="server" CssClass="RotuloGrande" Style="padding-left: 10px;">
                    </asp:Label>
                </td>
                <td align="right" valign="top">
                    <asp:ImageButton runat="server" ID="ImgBtnCerrarPanelPartida" ImageUrl="~/images/Bt_Cerrar.png"
                        Style="vertical-align: top;" OnClientClick="ocultarPanelModal('mpePartidas');" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="listadoInputsFiltro" runat="server">
                        <asp:Label ID="lblFiltroPartida" runat="server" CssClass="Rotulo" />
                    </div>
                    <div id="listadoPartidas" runat="server">
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpePartidas" runat="server" PopupControlID="pnlPartidas"
        TargetControlID="btnPartidasOculto" CancelControlID="ImgBtnCerrarPanelPartida"
        RepositionMode="None" PopupDragHandleControlID="pnlPartidasOculto" ClientIDMode="Static">
    </ajx:ModalPopupExtender>
    <%--Fin Panel Seleccion Partidas--%>
    <!-- Cabecera -->
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <aspf:WebPartManager ID="WebPartManager1" runat="server">
            </aspf:WebPartManager>
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td>
                        <asp:Image ID="Image2" runat="server" Height="55px" ImageUrl="~/images/recepcion_small.jpg"
                            Style="margin-bottom: 0px" Width="70px" />
                        <asp:Label ID="lblTitulo" runat="server" CssClass="RotuloGrande" Height="51px" Text="DRecepci�n de Pedidos"> 
                        </asp:Label>
                    </td>
                    <td align="right">
                        <aspf:WebPartZone ID="WebPartZone1" runat="server" EnableTheming="False" PartChromeType="None"
                            LayoutOrientation="Horizontal" PartStyle-Height="75px" Padding="20">
                            <PartStyle Height="75px"></PartStyle>
                            <ZoneTemplate>
                                <fsn:FSEPWebPartBusqArticulos ID="FSEPWebPartBusqArticulos1" runat="server" AuthorizationFilter="EPAprovisionador" />
                                <fsn:FSEPWebPartCesta ID="FSEPWebPartCesta1" runat="server" Width="300px" AuthorizationFilter="EPAprovisionador" />
                            </ZoneTemplate>
                            <PartChromeStyle CssClass="Rectangulo" />
                        </aspf:WebPartZone>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- Buscador -->
    <asp:Panel ID="pnlCabeceraRecepcion" runat="server" SkinID="PanelColapsable" onClick="checkCollapseStatus();">
        <table cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td valign="middle">
                    <asp:Image ID="imgExpandir" runat="server" ImageUrl="~/images/contraer.gif" />
                </td>
                <td valign="middle">
                    <asp:Label ID="lblCabecera" runat="server" Text="DBuscador de Pedidos:" Font-Bold="True"
                        ForeColor="White"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:UpdatePanel ID="pnlBuscador" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <asp:Panel ID="pnlBusqueda" runat="server" DefaultButton="btnBuscar" Style="height: 0px;
                overflow: hidden;">
                <table id="tblBusqueda" runat="server" cellpadding="3" cellspacing="0" border="0">
                    <tr>
                        <td>
                            <asp:Literal ID="litAnio" runat="server" Text="DA�o:"></asp:Literal>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlAnio" runat="server" DataTextField="Text" DataValueField="Value">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Literal ID="litCesta" runat="server" Text="DN� de cesta:"></asp:Literal>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCesta" runat="server" Width="74" onchange="validarsolonum(this)"
                                onkeypress="return validarkeynum(event)" onpaste="validarpastenum()"></asp:TextBox>
                            <ajx:TextBoxWatermarkExtender ID="txtCesta_TextBoxWatermarkExtender" runat="server"
                                Enabled="True" TargetControlID="txtCesta" WatermarkText="DN� de cesta">
                            </ajx:TextBoxWatermarkExtender>
                        </td>
                        <td>
                            <asp:Literal ID="litPedido" runat="server" Text="DN� de pedido:"></asp:Literal>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPedido" runat="server" Width="74" onchange="validarsolonum(this)"
                                onkeypress="return validarkeynum(event)" onpaste="validarpastenum()"></asp:TextBox>
                            <ajx:TextBoxWatermarkExtender ID="txtPedido_TextBoxWatermarkExtender" runat="server"
                                Enabled="True" TargetControlID="txtPedido" WatermarkText="DN� de pedido">
                            </ajx:TextBoxWatermarkExtender>
                        </td>
                        <td width="5px" runat="server" id="celdaSpacePedidoERP">
                        </td>
                        <td runat="server" id="celdaLitPedidoERP">
                            <asp:Literal ID="litPedidoERP" runat="server" Text="DN� de pedido ERP:"></asp:Literal>
                        </td>
                        <td runat="server" id="celdaPedidoERP">
                            <asp:TextBox ID="txtPedidoERP" runat="server" Width="207"></asp:TextBox>
                            <ajx:TextBoxWatermarkExtender ID="txtPedidoERP_TextBoxWatermarkExtender" runat="server"
                                Enabled="True" TargetControlID="txtPedidoERP" WatermarkText="DN� de pedido">
                            </ajx:TextBoxWatermarkExtender>
                        </td>
                        <td width="5px">
                        </td>
                        <td>
                            <asp:Literal ID="litAlbaran" runat="server" Text="DN� Albar�n:"></asp:Literal>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAlbaran" runat="server" Width="207px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="litProveedor" runat="server" Text="DProveedor:"></asp:Literal>
                        </td>
                        <td colspan="5">
                            <asp:TextBox ID="txtProveedor" runat="server" Width="90%" ClientIDMode="Static"></asp:TextBox>
                            <asp:HiddenField ID="hidProveedor" runat="server" />
                            <asp:ImageButton ID="imgProveedor" ImageUrl="~/Images/abrir_ptos_susp.gif" runat="server"
                                ImageAlign="Right" />
                            <ajx:TextBoxWatermarkExtender ID="txtProveedor_TWE" runat="server" Enabled="True"
                                TargetControlID="txtProveedor" BehaviorID="txtProveedor_TWE1" WatermarkText="DC�digo, Nombre">
                            </ajx:TextBoxWatermarkExtender>
                            <ajx:AutoCompleteExtender ID="txtProveedor_ACE" runat="server" CompletionInterval="500"
                                MinimumPrefixLength="3" ServiceMethod="AutoCompletar_Proveedor" TargetControlID="txtProveedor"
                                UseContextKey="True" OnClientItemSelected="ProveedorSeleccionado">
                            </ajx:AutoCompleteExtender>
                        </td>
                        <td width="5px">
                        </td>
                        <td runat="server" id="celdaLitProveedorERP">
                            <asp:Literal ID="litProvedorERP" runat="server" Text="DProveedor ERP:"></asp:Literal>
                        </td>
                        <td runat="server" id="celdaProveedorERP">
                            <asp:TextBox ID="txtProveedorERP" runat="server" Width="207"></asp:TextBox>
                            <ajx:TextBoxWatermarkExtender ID="txtProveedorERP_TextBoxWatermarkExtender" runat="server"
                                Enabled="True" TargetControlID="txtProveedorERP" WatermarkText="DProveedor ERP">
                            </ajx:TextBoxWatermarkExtender>
                        </td>
                        <td width="15px" runat="server" id="celdaSpaceProveedorERP">
                        </td>
                        <td>
                            <asp:Literal ID="litFecEmision" runat="server" Text="DFecha Emisi�n:"></asp:Literal>
                        </td>
                        <td>
                            <div style="float: left; display: inline">
                                <igpck:WebDatePicker ID="dteDesdeEmision" runat="server" NullDateLabel="" Width="100px" SkinID="Calendario"></igpck:WebDatePicker>
                            </div>
                            <div style="float: left; display: inline; padding-left: 3px; padding-right: 3px">
                                -</div>
                            <div style="float: left; display: inline">
                                <igpck:WebDatePicker ID="dteHastaEmision" runat="server" NullDateLabel="" Width="100px" SkinID="Calendario"></igpck:WebDatePicker>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="litArticulo" runat="server" Text="DB�squeda por art�culo:"></asp:Literal>
                        </td>
                        <td colspan="5">
                            <asp:TextBox ID="txtArticulo" runat="server" Width="99%"></asp:TextBox>
                            <asp:HiddenField ID="hidArticulo" runat="server" />
                            <ajx:TextBoxWatermarkExtender ID="txtArticulo_TextBoxWatermarkExtender" runat="server"
                                Enabled="True" TargetControlID="txtArticulo" WatermarkText="DC�digo, Descripci�n, ...">
                            </ajx:TextBoxWatermarkExtender>
                            <ajx:AutoCompleteExtender ID="txtArticulo_AutoCompleteExtender" runat="server" CompletionInterval="500"
                                MinimumPrefixLength="3" ServiceMethod="AutoCompletar_Articulo" TargetControlID="txtArticulo"
                                UseContextKey="True" OnClientItemSelected="ArticuloSeleccionado">
                            </ajx:AutoCompleteExtender>
                        </td>
                        <td width="5px">
                        </td>
                        <td>
                            <asp:Literal ID="litFecRecepcion" runat="server" Text="DFecha recepci�n:"></asp:Literal>
                        </td>
                        <td>
                            <div style="float: left; display: inline">
                                <igpck:WebDatePicker ID="dteDesdeRecepcion" runat="server" NullDateLabel="" Width="100px" SkinID="Calendario"></igpck:WebDatePicker>
                            </div>
                            <div style="float: left; display: inline; padding-left: 3px; padding-right: 3px">
                                -</div>
                            <div style="float: left; display: inline">
                                <igpck:WebDatePicker ID="dteHastaRecepcion" runat="server" NullDateLabel="" Width="100px" SkinID="Calendario"></igpck:WebDatePicker>
                            </div>
                        </td>
                        <td width="5px">
                        </td>
                        <td>
                            <asp:Literal ID="litPedidoProve" runat="server" Text="DN� de pedido proveedor:"></asp:Literal>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPedidoProve" runat="server" Width="207"></asp:TextBox>
                            <ajx:TextBoxWatermarkExtender ID="txtPedidoProve_TextBoxWatermarkExtender" runat="server"
                                Enabled="True" TargetControlID="txtPedidoProve" WatermarkText="DN� de pedido"
                                WatermarkCssClass="WaterMark">
                            </ajx:TextBoxWatermarkExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="litEmpresa" runat="server" Text="DEmpresa:"></asp:Literal>
                        </td>
                        <td colspan="5">
                            <asp:DropDownList ID="ddlEmpresa" runat="server" DataTextField="Text" DataValueField="Value"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td width="5px">
                        </td>
                        <td>
                            <asp:Literal ID="litFecEntregaSolicit" runat="server" Text="DFec. entrega solicitada:"></asp:Literal>
                        </td>
                        <td>
                            <div style="float: left; display: inline">
                                <igpck:WebDatePicker ID="dteDesdeEntregaSolicit" runat="server" NullDateLabel="" Width="100px" SkinID="Calendario"></igpck:WebDatePicker>
                            </div>
                            <div style="float: left; display: inline; padding-left: 3px; padding-right: 3px">
                                -</div>
                            <div style="float: left; display: inline">
                                <igpck:WebDatePicker ID="dteHastaEntregaSolicit" runat="server" NullDateLabel="" Width="100px" SkinID="Calendario"></igpck:WebDatePicker>
                            </div>
                        </td>
                        <td width="5px">
                        </td>
                        <td>
                            <asp:Literal ID="litGestor" runat="server" Text="DGestor:"></asp:Literal>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlGestor" runat="server" DataTextField="Text" DataValueField="Value"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="litTipoPedido" runat="server" Text="DTipo de pedido:"></asp:Literal>
                        </td>
                        <td colspan="5">
                            <asp:DropDownList ID="ddlTipoPedido" runat="server" DataTextField="Text" DataValueField="Value"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td width="5px">
                        </td>
                        <td>
                            <asp:Literal ID="litFecEntregaIndic" runat="server" Text="DFec. entrega indicada:"></asp:Literal>
                        </td>
                        <td>
                            <div style="float: left; display: inline">
                                <igpck:WebDatePicker ID="dteDesdeEntregaIndic" runat="server" NullDateLabel="" Width="100px" SkinID="Calendario"></igpck:WebDatePicker>
                            </div>
                            <div style="float: left; display: inline; padding-left: 3px; padding-right: 3px">
                                -</div>
                            <div style="float: left; display: inline">
                                <igpck:WebDatePicker ID="dteHastaEntregaIndic" runat="server" NullDateLabel="" Width="100px" SkinID="Calendario"></igpck:WebDatePicker>
                            </div>
                        </td>
                        <td width="5px">
                        </td>
                        <td colspan="2">
                            <asp:UpdatePanel ID="updpnlCategorias" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <fsn:FSNLinkInfo ID="BtnCategoria" runat="server" CssClass="Rotulo" Style="text-decoration: none"
                                        PanelInfo="mpePanelArbol">
                        <asp:Literal ID="LnkBtnCategoria" runat="server"></asp:Literal>
                        &nbsp;<asp:Image ID="imgDespCategoria" runat="server" SkinID="desplegar" /></fsn:FSNLinkInfo>
                                    &nbsp; &nbsp;
                                    <asp:Literal ID="litCategoria" runat="server" Visible="False"></asp:Literal>
                                    <asp:LinkButton ID="lnkQuitarCategoria" runat="server" Visible="False" Text="(x)"
                                        CssClass="Normal" Style="text-decoration: none"></asp:LinkButton>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="fsnTvwCategorias" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr id="trCentrosPartidas" runat="server" visible="false" style="width: 100%">
                        <td>
                            <asp:Literal ID="litCentroCoste" runat="server" Text="DCentro de Coste:"></asp:Literal>
                        </td>
                        <td colspan="5">
                            <asp:TextBox ID="tbCtroCoste" runat="server" CssClass="Normal" Style="float: left;
                                width: 90%;" ClientIDMode="Static" codCentro=""></asp:TextBox>
                            <asp:HiddenField ID="tbCtroCoste_Hidden" runat="server" ClientIDMode="Static" />
                            <img src="../../Images/abrir_ptos_susp.gif" style="border: 0px; padding-left: 3px;
                                cursor: pointer;" onclick="javascript:abrirPanelCentros('<%=mpeCentros.ClientID%>', '<%=FiltroCentros.ClientID %>', 'event', 'tbCtroCoste','tbCtroCoste_Hidden')" />
                        </td>
                        <td width="5px">
                        </td>
                        <td>
                            <asp:Literal ID="litReceptor" runat="server" Text="DReceptor:"></asp:Literal>
                        </td>
                        <td colspan="4">
                            <asp:DropDownList ID="ddlReceptor" runat="server" DataTextField="Text" DataValueField="Value"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <asp:PlaceHolder ID="phrPartidasPresBuscador" runat="server" ClientIDMode="Static">
                            </asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
                <fieldset class="Checkboxes">
                    <table>
                        <tr>
                            <td>
                                <asp:CheckBoxList ID="chklstEstado" runat="server" RepeatDirection="Horizontal">
                                </asp:CheckBoxList>
                            </td>
                            <td id="tdSoloLineasPendientes">
                                <asp:CheckBox ID="chkSoloLineasPendientes" runat="server" Text="DMostrar s�lo las l�neas pendientes" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset class="Checkboxes">
                    <asp:CheckBoxList ID="chklstTipo" runat="server" RepeatDirection="Horizontal">
                    </asp:CheckBoxList>
                </fieldset>
                <fieldset id="fldsetRecepPedidosCC" runat="server" class="Checkboxes">
                    <asp:CheckBoxList ID="chkRecepcionPedidos" runat="server" RepeatDirection="Horizontal"
                        AutoPostBack="False">
                        <asp:ListItem Text="dRecepcionar mis pedidos"></asp:ListItem>
                        <asp:ListItem Text="dRecepcionar pedidos de otros usuarios"></asp:ListItem>
                    </asp:CheckBoxList>
                </fieldset>
                <table align="center">
                    <tr>
                        <td>
                            <fsn:FSNButton ID="btnBuscar" runat="server" Text="DBuscar" Alineacion="Right" OnClientClick="Collapse();"></fsn:FSNButton>
                        </td>
                        <td>
                            <fsn:FSNButton ID="btnLimpiar" runat="server" Text="DLimpiar" Alineacion="Right"
                                Style="float: none;"></fsn:FSNButton>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:UpdatePanel ID="upListaFiltros" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div id="divListaFiltros" class="Cabecera_Seleccionada" runat="server" style="margin: 5px">
                        <asp:Label ID="lblListaFiltros" runat="server"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <ajx:CollapsiblePanelExtender ID="cpeBusqueda" runat="server" CollapseControlID="pnlCabeceraRecepcion"
                CollapsedImage="~/images/expandir.gif" ExpandControlID="pnlCabeceraRecepcion"
                ExpandedImage="~/images/contraer.gif" ImageControlID="imgExpandir" SuppressPostBack="True"
                TargetControlID="pnlBusqueda" Collapsed="True">
            </ajx:CollapsiblePanelExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- Lista Pedidos -->
    <asp:Button runat="server" ID="btnRecargaGrid" Style="display: none;" />
    <asp:UpdatePanel ID="pnlGrid" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnRecargaGrid" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <table border="0" width="99%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:Panel ID="panSinPaginacion" runat="server" Visible="True" Width="100%">
                            <div style="float: right">
                                <table border="0" align="right">
                                    <tr>
                                        <td align="right" width="700px">
                                            <div style="height: 22px; float: right; line-height: 22px;">
                                                <div class="botonDerechaCabecera" style="display: inline;">
                                                    <asp:LinkButton ID="ibGuardarConfiguracion" runat="server" Style="display: none;
                                                        background-repeat: no-repeat; height: 20px; text-decoration: none; color: Black;">
                                                        <span id="litGuardarConfiguracion" runat="server" style="margin-left: 26px;">DGuardarConfiguracion</span>
                                                    </asp:LinkButton>
                                                </div>
                                                <div id="pnlRegistrarRecepcion" onclick="RegistrarRecepcion(); return false;" class="botonDerechaCabecera"
                                                    style="display: inline;">
                                                    <div id="ibRegistrarRecepcion" runat="server" style="background-repeat: no-repeat;
                                                        height: 20px; display: inline;">
                                                        <span id="litRegistrarRecepcion" runat="server" style="margin-left: 26px;">DRegistrarRecepcion</span>
                                                    </div>
                                                </div>
                                                <div onclick="abrirPanelConfiguracion(); return false;" class="botonDerechaCabecera"
                                                    style="display: inline;">
                                                    <div id="ibConfigurar" runat="server" style="background-repeat: no-repeat; height: 20px;
                                                        display: inline;">
                                                        <span id="litConfigurar" runat="server" style="margin-left: 24px;"></span>
                                                    </div>
                                                </div>
                                                <div onclick="MostrarExcel();return false;" class="botonDerechaCabecera" style="display: inline;">
                                                    <div id="ibExcel" runat="server" style="background-repeat: no-repeat; height: 20px;
                                                        display: inline;">
                                                        <span id="litExcel" runat="server" style="margin-left: 20px;"></span>
                                                    </div>
                                                </div>
                                                <div onclick="MostrarPdf();return false;" class="botonDerechaCabecera" style="display: inline;">
                                                    <div id="ibPDF" runat="server" style="background-repeat: no-repeat; height: 20px;
                                                        display: inline;">
                                                        <span id="litPDF" runat="server" style="margin-left: 20px;"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <ig:WebHierarchicalDataGrid runat="server" ID="whdgEntregasExportacion" 
					            Visible="false" AutoGenerateBands="false"
					            AutoGenerateColumns="false" Width="100%" EnableAjax="true" 
					            EnableAjaxViewState="false" EnableDataViewState="false">
                                <ClientEvents Initialize="whdgEntregasExportacion_Initialize"/>
                                <GroupingSettings EnableColumnGrouping="True" />
					            <Behaviors>
						            <ig:Filtering Enabled="true"></ig:Filtering>
						            <ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting>
						            <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
					            </Behaviors>
				            </ig:WebHierarchicalDataGrid>
                            <ig:WebHierarchicalDataGrid ID="whdgEntregas" runat="server" Visible="true" AutoGenerateColumns="false"
                                Width="100%" ShowHeader="true" EnableAjax="false" EnableAjaxViewState="false"
                                EnableDataViewState="false" CssClass="headerNoWrap">
                                <Columns>                                    
                                </Columns>
                                <Behaviors>
                                    <ig:Activation Enabled="true" />
                                    <ig:RowSelectors Enabled="true" RowSelectorCssClass="RowSelectorCssClass">
                                    </ig:RowSelectors>
                                    <ig:Selection Enabled="true" CellSelectType="Single" RowSelectType="Multiple" ColumnSelectType="None">
                                        <SelectionClientEvents CellSelectionChanging="whdgEntregas_CellClick" RowSelectionChanged="whdgEntregas_RowSelectionChanged" />
                                    </ig:Selection>
                                    <ig:ColumnResizing Enabled="true" ColumnResizingClientEvents-ColumnResized="whdgEntregas_ColResized">
                                    </ig:ColumnResizing>
                                    <ig:ColumnMoving Enabled="true" ColumnMovingClientEvents-HeaderDragEnd="whdgEntregas_ColumnMoving">
                                    </ig:ColumnMoving>
                                    <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true" AnimationEnabled="false">
                                    </ig:Filtering>
                                    <ig:Paging Enabled="true" PagerAppearance="Top">
                                        <PagerTemplate>
                                            <div id="divContenedor" runat="server" style="display: inline; float: left; width: 40%;
                                                height: 100%">
                                                <pag:Paginador ID="wdgPaginador" runat="server" />
                                            </div>
                                        </PagerTemplate>
                                    </ig:Paging>
                                    <ig:VirtualScrolling Enabled="false">
                                    </ig:VirtualScrolling>
                                    <ig:Sorting Enabled="true" SortingMode="Multi">
                                        <SortingClientEvents ColumnSorting="whdgEntregas_Sorting_ColumnSorting" />
                                    </ig:Sorting>
                                </Behaviors>
                                <GroupingSettings EnableColumnGrouping="True" GroupAreaVisibility="Visible" />
                            </ig:WebHierarchicalDataGrid>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <ig:WebDocumentExporter ID="WebDocumentExporter1" runat="server">
    </ig:WebDocumentExporter>
    <ig:WebExcelExporter ID="WebExcelExporter1" runat="server">
    </ig:WebExcelExporter>
    <ajx:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress" Style="display: none">
        <div style="position: relative; top: 30%; text-align: center;">
            <asp:Image ID="ImgProgress" runat="server" SkinID="ImgProgress" />
            <asp:Label ID="LblProcesando" runat="server" ForeColor="Black"></asp:Label>
        </div>
    </asp:Panel>
    <!-- PopUp Detalle Albar�n -->
    <ajx:ModalPopupExtender ID="mpeDetalleAlbaran" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlDetalleAlbaran" TargetControlID="btnDetalleAlbaran">
    </ajx:ModalPopupExtender>
    <asp:Button runat="server" ID="btnDetalleAlbaran" Style="display: none;" />
    <asp:Panel runat="server" ID="pnlDetalleAlbaran" CssClass="modalPopup" Style="display:none; position:absolute; z-index:1002; width:90%; padding:10px; max-height:600px;">
        <asp:UpdatePanel runat="server" ID="upDetalleAlbaran" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnDetalleAlbaran" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <asp:HiddenField ID="hidAlb" runat="server" />
                <asp:HiddenField ID="hidAlbFechaRecep" runat="server" />
                <asp:HiddenField ID="hidAlbProve" runat="server" />
                <div style="clear: both; float: left; width: 100%; position: relative;">
                    <div style="position: absolute; width: 30px; height: 30px; top: 2px; right: 10px;
                        cursor: pointer;">
                        <asp:Image ID="imgBtnCerrarDetalleAlbaran" runat="server" SkinID="Cerrar" ToolTip="Cerrar Detalle" />
                    </div>
                    <div style="clear: both; float: left;">
                        <asp:Image runat="server" ID="imgDetalleAlbaran" ImageUrl="~/images/recepcion_small.jpg" />
                        <asp:Label runat="server" ID="lblDetalleAlbaran" CssClass="RotuloGrande" Text="DDetalle del albar�n"></asp:Label>
                        <asp:Label runat="server" ID="lblAlbaran" CssClass="RotuloGrande" Style="margin-left: 3px;">DXXX0100000000</asp:Label>
                    </div>
                    <asp:Panel runat="server" ID="pnlAlbaranBloqueado" Style="clear: both; float: left;
                        margin-top: 5px;">
                        <asp:Image runat="server" ID="imgAlbaranBloqueado" Style="vertical-align: middle;" />
                        <asp:Label runat="server" ID="lblAlbaranBloqueado" CssClass="Etiqueta" Text="DAlbar�n bloqueado para facturaci�n"></asp:Label>
                    </asp:Panel>
                    <div style="float: right; margin-right: 50px; display: none;">
                        <fieldset class="Checkboxes">
                            <div style="clear: both; float: left;">
                                <asp:Label runat="server" ID="lblPuntuacionRecepcion" Text="DPoco Satisfactoria: 3"></asp:Label>
                            </div>
                            <div style="clear: both; float: left;">
                                <ajx:Rating runat="server" ID="rPuntuacionRecepcion" ReadOnly="true" StarCssClass="ratingStar"
                                    WaitingStarCssClass="savedRatingStar" FilledStarCssClass="filledRatingStar" EmptyStarCssClass="emptyRatingStar"
                                    MaxRating="10" CurrentRating="3">
                                </ajx:Rating>
                            </div>
                        </fieldset>
                    </div>
                    <div style="clear: both; float: left; margin-top: 10px; line-height: 25px;">
                        <asp:Label runat="server" ID="lblDetalleAlbaranFecRecepcion" CssClass="Rotulo" Text="DFecha de recepci�n:"></asp:Label>
                        <asp:Label runat="server" ID="lblDetalleAlbaranFecRecepcionValue" Text="D18/04/2012"
                            Style="margin-left: 5px;"></asp:Label>
                        <asp:Image runat="server" ID="imgObservaciones" Style="vertical-align: middle; margin-left: 5px;
                            cursor: pointer;" />
                        <asp:Label runat="server" ID="lblDetalleAlbaranComentarios" Style="display: none;"></asp:Label>
                        <asp:Label runat="server" ID="lblDetalleAlbaranNumRecepcionERP" CssClass="Rotulo"
                            Text="DN� recepci�n ERP:" Style="margin-left: 50px;"></asp:Label>
                        <asp:Label runat="server" ID="lblDetalleAlbaranNumRecepcionERPValue" Text="DS132345666"
                            Style="margin-left: 5px;"></asp:Label>
                        <asp:Label runat="server" ID="lblProblemasRecepcion" CssClass="datoRojo" Text="D�Problemas en la recepci�n!"
                            Style="margin-left: 20px;"></asp:Label>
                    </div>
                    <div style="clear: both; float: left; width: 100%; margin-top: 5px;">
                        <fieldset class="Checkboxes">
                            <legend>
                                <asp:Label runat="server" ID="lblDetalleAlbaranDatosReceptor" SkinID="EtiquetaNegrita"
                                    Text="DDatos del receptor"></asp:Label>
                            </legend>
                            <div style="float: left; margin-left: 10px;">
                                <div style="clear: both; float: left; line-height: 25px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranCodigoReceptor" CssClass="Rotulo"
                                        Text="DC�digo"></asp:Label>
                                </div>
                                <div style="clear: both; float: left; line-height: 25px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranEmailReceptor" CssClass="Rotulo" Text="DEmail"></asp:Label>
                                </div>
                            </div>
                            <div style="float: left; margin-left: 3px;">
                                <div style="clear: both; float: left; line-height: 25px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranCodigoReceptorValue" Text="D27YI"></asp:Label>
                                </div>
                                <div style="clear: both; float: left; line-height: 25px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranEmailReceptorValue" Text="Daum@fullstep.com"></asp:Label>
                                </div>
                            </div>
                            <div style="float: left; margin-left: 30px;">
                                <div style="clear: both; float: left; line-height: 25px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranNombreReceptor" CssClass="Rotulo"
                                        Text="DNombre"></asp:Label>
                                </div>
                                <div style="clear: both; float: left; line-height: 25px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranTelefonoReceptor" CssClass="Rotulo"
                                        Text="DTel�fono"></asp:Label>
                                </div>
                            </div>
                            <div style="float: left; margin-left: 3px;">
                                <div style="clear: both; float: left; line-height: 25px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranNombreReceptorValue" Text="DAlberto Mu�oz P�rez"></asp:Label>
                                </div>
                                <div style="clear: both; float: left; line-height: 25px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranTelefonoReceptorValue" Text="D913795689"></asp:Label>
                                </div>
                            </div>
                            <div style="float: left; margin-left: 30px;">
                                <div style="clear: both; float: left; line-height: 25px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranCargoReceptor" CssClass="Rotulo" Text="DCargo"></asp:Label>
                                </div>
                                <div style="clear: both; float: left; line-height: 25px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranFaxReceptor" CssClass="Rotulo" Text="DFax"></asp:Label>
                                </div>
                            </div>
                            <div style="float: left; margin-left: 3px;">
                                <div style="clear: both; float: left; line-height: 25px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranCargoReceptorValue" Text="DJefe COEX"></asp:Label>
                                </div>
                                <div style="clear: both; float: left; line-height: 25px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranFaxReceptorValue" Text="D913795689"></asp:Label>
                                </div>
                            </div>
                            <div style="float: left; margin-left: 30px;">
                                <div style="clear: both; float: left; line-height: 25px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranDepartamentoReceptor" CssClass="Rotulo"
                                        Text="DDepartamento"></asp:Label>
                                </div>
                                <div style="clear: both; float: left; line-height: 25px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranOrganizacionReceptor" CssClass="Rotulo"
                                        Text="DOrganizaci�n"></asp:Label>
                                </div>
                            </div>
                            <div style="float: left; margin-left: 3px; margin-right: 10px;">
                                <div style="clear: both; float: left; line-height: 25px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranDepartamentoReceptorValue" Text="DGestores"></asp:Label>
                                </div>
                                <div style="clear: both; float: left; line-height: 25px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranOrganizacionReceptorValue" Text="DConservaci�n Madrid I"></asp:Label>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div style="clear: both; float: left; width: 100%; margin-top: 5px;">
                        <fieldset class="Checkboxes">
                            <legend>
                                <asp:Label runat="server" ID="lblDetalleAlbaranDetalleLineas" SkinID="EtiquetaNegrita"
                                    Text="DDetalle de las l�neas"></asp:Label>
                            </legend>
                            <div style="float: left; width: 100%;" class="CabeceraLineaRecepcion">
                                <div style="float: left; width: 70px; text-align: center;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranPedido" Text="DPedido"></asp:Label>
                                </div>
                                <div style="float: left; width: 50px; text-align: center;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranNumLinea" Text="DLinea"></asp:Label>
                                </div>
                                <asp:Panel runat="server" ID="divCabeceraBotones" Style="float: right; height: 1px;
                                    width: 0px; margin-right: 5px;">
                                </asp:Panel>
                                <div style="float: right; width: 130px; text-align: right; margin-right: 5px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranDetalleLineasSalidaAlmacen" Text="DSalida autom. almacen"></asp:Label>
                                </div>
                                <div style="float: right; width: 90px; text-align: center; margin-right: 2px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranDetalleLineasImporte" Text="DImporte"></asp:Label>
                                </div>
                                <div style="float: right; width: 90px; text-align: center; margin-right: 2px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranDetalleLineasImporteRecibido" Text="DImporteRecibido"></asp:Label>
                                </div>
                                <div style="float: right; width: 90px; text-align: center; margin-right: 2px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranDetalleLineasImportePedido" Text="DImportePedido"></asp:Label>
                                </div>
                                <div style="float: right; width: 80px; text-align: right; margin-right: 5px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranDetalleLineasCantidadRecibida" Text="DCant.recibida"></asp:Label>
                                </div>
                                <div style="float: right; width: 90px; text-align: center; margin-right: 2px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranDetalleLineasPrecioUnitario" Text="DP.U."></asp:Label>
                                </div>
                                <div style="float: right; width: 80px; text-align: right; margin-right: 5px;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranDetalleLineasCantidadPedida" Text="DCant.pedida"></asp:Label>
                                </div>                                
                                <div style="float: left;">
                                    <asp:Label runat="server" ID="lblDetalleAlbaranDetalleLineasArticulo" Text="DArticulo"></asp:Label>
                                </div>                                
                            </div>
                            <div class="texto10" style="clear: both; float: left; width: 100%; border: solid 1px transparent;
                                max-height: 370px; overflow: auto;">
                                <asp:DataList runat="server" ID="dlDetalleAlbaranLineasRecepcion" RepeatDirection="Vertical"
                                    Style="width: 100%;">
                                    <ItemStyle CssClass="FilaImpar" />
                                    <AlternatingItemStyle CssClass="FilaPar" />
                                    <ItemTemplate>
                                        <div style="float: left; width: 70px; text-align: center;">
                                            <asp:Literal runat="server" ID="lblPedido" Text='<%#Eval("PEDIDO") %>'></asp:Literal>
                                        </div>
                                        <div style="float: left; width: 50px; text-align: center;">
                                            <asp:Literal runat="server" ID="lblNumLinea" Text='<%#Eval("NUM") %>'></asp:Literal>
                                        </div>
                                        <asp:Panel runat="server" ID="divBotones" Style="float: right; height: 1px; margin: 0px 5px;">
                                            <fsn:FSNButton ID="btnAnularLineaRecepcion" runat="server" Text="DAnular" Alineacion="Left"
                                                Style="margin-left: 5px;"></fsn:FSNButton>
                                            <fsn:FSNButton ID="btnGuardar" runat="server" Text="DGuardar" Alineacion="Right"
                                                Style="display: none; margin-right: 5px;"></fsn:FSNButton>
                                        </asp:Panel>
                                        <div style="float: right; width: 130px; text-align: center; margin-right: 5px;">
                                            <input ID="chkDetalleAlbaranLineasRecepcionSalidaAlmacen" type="checkbox" runat="server" onclick="return false;" checked='<%#Eval("SALIDA_ALMACEN")%>' />
                                        </div>
                                        <div style="float: right; width: 90px; text-align: right; margin-right: 2px;">
                                            <asp:Label runat="server" ID="lblImporte"></asp:Label>
                                            <asp:Label runat="server" ID="lblImporteMoneda" Text='<%#Eval("MON") %>' CssClass="Rotulo10"
                                                Style="margin-left: 2px;"></asp:Label>
                                        </div>
                                        <div style="float: right; width: 90px; text-align: right; margin-right: 5px;">
                                            <asp:Label runat="server" ID="lblImporteRecibido"></asp:Label>
                                            <asp:Label runat="server" ID="lblImporteRecibidoMoneda" Text='<%#Eval("MON") %>' CssClass="Rotulo10"
                                                Style="margin-left: 2px;"></asp:Label>
                                        </div>
                                        <div style="float: right; width: 90px; text-align: right; margin-right: 5px;">
                                            <asp:Label runat="server" ID="lblImportePedido"></asp:Label>
                                            <asp:Label runat="server" ID="lblImportePedidoMoneda" Text='<%#Eval("MON") %>' CssClass="Rotulo10"
                                                Style="margin-left: 2px;"></asp:Label>
                                        </div>
                                        <div style="float: right; width: 80px; text-align: right; margin-right: 5px;">
                                            <asp:Literal runat="server" ID="lblCantidadRecibida"></asp:Literal>                                            
                                            <igpck:WebNumericEditor ID="WNumEdCantidadRecibida" Value='<%# Eval("CANT") %>' runat="server"
                                                Nullable="False" Width="65px" Visible="false" EnableViewState="false">
                                                <Buttons SpinButtonsDisplay="OnRight"></Buttons>
                                                <ClientEvents TextChanged="TextChange" />
                                            </igpck:WebNumericEditor>
                                        </div>
                                        <div style="float: right; width: 90px; text-align: right; margin-right: 2px;">
                                            <asp:Literal runat="server" ID="lblPrecioUnitario"></asp:Literal>
                                            <asp:Label runat="server" ID="lblPrecioUnitarioMoneda" Text='<%#Eval("MON") %>' CssClass="Rotulo10"
                                                Style="margin-left: 2px;"></asp:Label>
                                        </div>
                                        <div style="float: right; width: 80px; text-align: right; margin-right: 5px;">
                                            <asp:Literal runat="server" ID="lblCantidadPedida"></asp:Literal>
                                        </div>                                        
                                        <div>
                                            <asp:Label runat="server" ID="lblArticulo"></asp:Label>
                                        </div >                                                                                    
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                            <div style="clear: both; float: left; width: 100%; margin-top: 10px;">
                                <fsn:FSNButton ID="btnAnularAlbaran" runat="server" Text="DAnular Albar�n" Alineacion="Left"></fsn:FSNButton>
                                <fsn:FSNButton ID="btnBloquearFacturacion" runat="server" Text="DBloquear facturaci�n"
                                    Alineacion="Left" Style="margin-left: 10px;"></fsn:FSNButton>
                                <fsn:FSNButton ID="btnDesbloquearFacturacion" runat="server" Text="DDesBloquear facturaci�n"
                                    Alineacion="Left" Style="margin-left: 10px;"></fsn:FSNButton>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <div id="pnlDetalleAlbaranComentarios" class="modalPopup" style="display: none; position: absolute;
        z-index: 10003; width: 300px; min-height: 70px;">
        <div style="clear: both; float: left; width: 100%; position: relative;">
            <div style="position: absolute; width: 30px; height: 30px; top: 2px; right: 10px;
                cursor: pointer;">
                <asp:Image ID="imgDetalleAlbaranComentarios" runat="server" SkinID="Cerrar" ToolTip="Cerrar Detalle" />
            </div>
            <div style="clear: both; float: left; margin-right: 40px; padding: 10px 5px 5px 10px;">
                <span id="litDetalleAlbaranComentarios"></span>
            </div>
        </div>
    </div>
    <!-- PopUp Confirmaci�n Anular Linea Recepci�n -->
    <input id="btnOcultoConfirm" type="button" value="button" runat="server" style="display: none" />
    <div id="pnlConfirm" class="modalPopup" style="display: none; position: absolute;
        z-index: 10003; width: 300px; padding: 20px">
        <asp:UpdatePanel ID="upConfirm" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="clear: both; float: left; width: 100%;">
                    <div style="float: left; width: 100%;">
                        <div style="float: left;">
                            <img alt="" title="" id="imgPnlConfirm" runat="server" style="float: left; vertical-align: middle;"
                                src="~/images/icono_info.gif" />
                        </div>
                        <div style="margin-left: 50px;">
                            <asp:Label runat="server" ID="lblConfirm"></asp:Label>
                        </div>
                    </div>
                    <div style="clear: both; float: left; width: 100%; margin-top: 5px;">
                        <asp:TextBox ID="txtObservacionesBloqueoFacturacion" runat="server" TextMode="MultiLine"
                            Rows="4" MaxLength="4000" Width="100%" Font-Size="12px"></asp:TextBox>
                    </div>
                    <div style="clear: both; float: left; width: 100%; margin-top: 15px; text-align: center;">
                        <div style="display: inline-block;">
                            <fsn:FSNButton ID="btnAceptarConfirm" runat="server" Alineacion="Left" Style="margin-right: 5px;"></fsn:FSNButton>
                            <fsn:FSNButton ID="btnAceptarConfirmServer" runat="server" Alineacion="Left" Style="margin-right: 5px;
                                display: none;"></fsn:FSNButton>
                            <fsn:FSNButton ID="btnCancelarConfirm" runat="server" Alineacion="Left" Style="margin-left: 5px;"></fsn:FSNButton>
                            <fsn:FSNButton ID="btnCancelarRegistrarRecepcion" runat="server" Alineacion="Left"
                                Style="margin-left: 5px; display: none;"></fsn:FSNButton>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <ajx:ModalPopupExtender ID="mpeConfirm" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlConfirm" TargetControlID="btnOcultoConfirm" CancelControlID="btnCancelarConfirm">
    </ajx:ModalPopupExtender>
    <!-- PopUp Registro Recepci�n -->
    <ajx:ModalPopupExtender ID="mpeRegistroRecepcion" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlRegistroRecepcion" TargetControlID="btnRegistrarRecepcion">
    </ajx:ModalPopupExtender>
    <asp:Button runat="server" ID="btnRegistrarRecepcion" Style="display: none;" />
    <asp:Panel runat="server" ID="pnlRegistroRecepcion" class="modalPopup" Style="display:none; position: absolute; z-index:1002; padding:0.5em;">
        <asp:UpdatePanel runat="server" ID="upRegistroRecepcion" UpdateMode="Conditional" ChildrenAsTriggers="false">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnRegistrarRecepcion" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="lnkLineasPendientesRecepcionar" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <asp:Image ID="imgBtnCerrarRegistroRecepcion" runat="server" SkinID="Cerrar" 
					ToolTip="Cerrar Detalle" style="position:absolute; width:30px; height:30px; top:2px; right:10px; cursor:pointer;" />
                <asp:Image runat="server" ID="imgRegistroRecepcion" ImageUrl="~/images/recepcion_small.jpg"
                    Style="height:20px; vertical-align:bottom; margin-top:1em; display:inline-block;" />
                <asp:Label runat="server" ID="lblRegistroRecepcion" CssClass="RotuloGrande" style="display:inline-block;"></asp:Label>
                <asp:LinkButton runat="server" ID="lnkLineasPendientesRecepcionar" style="margin-right:50px;"></asp:LinkButton>
                <div style="margin-top:10px;" class="CabeceraLineaRecepcion">
                    <div runat="server" id="pnlRegistroRecepcionCantidadRecepcionar" style="white-space: nowrap; text-align: right"></div>
                    <div runat="server" id="pnlLblRegistroRecepcionAlmacen" style="margin-right: 5px; white-space: nowrap"></div>
                </div>
                <div id="pnlLineasPedido" style="max-height:35vh; min-height:40px; overflow:auto;">
                    <asp:DataList runat="server" ID="dlLineasPedido" RepeatDirection="Vertical" Style="width:100%; max-height: 200px; font-size:11px;">
                        <HeaderTemplate>
							<div class="CabeceraLineaRecepcion">
								<asp:Label runat="server" ID="lblRegistroRecepcionPedido" style="display:inline-block; text-align:center; width:6em; white-space:nowrap;"></asp:Label>
								<asp:Label runat="server" ID="lblRegistroRecepcionArticulo" style="display:inline-block; width:20em; white-space:nowrap;"></asp:Label>
								<asp:Label runat="server" ID="lblRegistroRecepcionDestino" style="display:inline-block; text-align:center; width:4em; white-space:nowrap;" ></asp:Label>
								<asp:Label runat="server" ID="lblRegistroRecepcionPrecioUnitario" style="display:inline-block; width:8em; white-space:nowrap; text-align:center;"></asp:Label>
								<asp:Label runat="server" ID="lblCantidadPedida" style="display:inline-block; width:8em; white-space:nowrap; text-align:right;"></asp:Label>
								<asp:Label runat="server" ID="lblImportePedido" style="display:inline-block; width:8em; white-space:nowrap; text-align:right;"></asp:Label>
								<asp:Label runat="server" ID="lblCantidadRecibida" style="display:inline-block; width:8em; white-space:nowrap; text-align:right;"></asp:Label>
								<asp:Label runat="server" ID="lblImporteRecibido" style="display:inline-block; width:8em; white-space:nowrap; text-align:right;"></asp:Label>
								<div runat="server" id="pnlRegistroRecepcionCantidadRecepcionar2" style="display:inline-block; width:12em; white-space:nowrap; text-align: right">
                                    <asp:Label runat="server" ID="lblCantidadRecepcionar" style="display:inline-block; white-space:nowrap;"></asp:Label>
                                </div>
								<div runat="server" id="pnlRegistroRecepcionImporteRecepcionar" style="display:inline-block; width:12em; white-space:nowrap; text-align: right">
                                    <asp:Label runat="server" ID="lblImporteRecepcionar" style="display:inline-block; width:125px; white-space:nowrap;"></asp:Label>
                                </div>
								<asp:Label runat="server" ID="lblRegistroRecepcionFecEntregaSolicitada" style="display:inline-block; width:8em; white-space:nowrap; text-align:center;"></asp:Label>
								<div runat="server" id="pnlLblRegistroRecepcionAlmacen2" style="display:inline-block; margin-right:5px; white-space:nowrap; width:7em; text-align:center;">
                                    <asp:Label runat="server" ID="lblRegistroRecepcionAlmacen"></asp:Label>
                                </div>
								<asp:Panel id="tdLblSalidaAlmacen" runat="server" style="display:inline-block; width:7em; text-align:right; width:7em; text-align:center;">
                                    <div runat="server" id="pnlLblSalidaAlmacen" style="display:inline-block; margin-right:5px; white-space:nowrap; text-align:center;">
                                        <asp:Label runat="server" ID="lblSalidaAlmacen"></asp:Label>
                                    </div>
                                </asp:Panel>
							</div>                                
                        </HeaderTemplate>
                        <ItemTemplate>							
                            <div>
								<fsn:FSNLinkInfo ID="FSNlnkRecepcionPedido" runat="server" Font-Underline="false"
									Text='<%# Eval("PEDIDO") %>' CssClass="Rotulo10" 
									ContextKey='<%# Eval("APROVISIONADOR") & "#" & Eval("PEDIDO") & "#" & Eval("EST") & "#" & Eval("TIPO") & "#" & Eval("NUM_PED_ERP") & "#" & Eval("IMPORTE") & "#" & Eval("ORDEN") %>'
									PanelInfo="FSNPanelDatosPedido">
								</fsn:FSNLinkInfo>
							</div>
							<div style="display:inline-block; width:6em; text-align:center;"></div>
							<div style="display:inline-block; vertical-align:top; width:20em;">
								<asp:LinkButton ID="lnkCodArt" runat="server" Font-Underline="false" Text='<%#Eval("CODART")%>'
                                    CssClass="Rotulo10" CommandName="MostrarDetalleArt" CommandArgument='<%#Eval("LINEAID")%>' />
                                <asp:LinkButton ID="lnkDenArt" runat="server" Font-Underline="false" Text='<%#Eval("DENART")%>'
                                    CssClass="Rotulo10" CommandName="MostrarDetalleArt" CommandArgument='<%#Eval("LINEAID")%>'
                                    Style="margin-left: 1px;" />
							</div>
							<div style="display:inline-block; width:4em; text-align:center;">
								<fsn:FSNLinkInfo ID="FSNlnkDest" runat="server" Font-Underline="false" Text='<%# Eval("DESTINO") %>'
                                    CssClass="Rotulo10" ContextKey='<%# Eval("DESTINO") & "#" & Eval("LINEAID") %>' PanelInfo="FSNPanelDatosDestino">
                                </fsn:FSNLinkInfo>
							</div>
							<div style="display:inline-block; width:8em; text-align:right;">
								<asp:Literal ID="lblPrecUni" runat="server" Text='<%#Eval("PRECIOUNITARIO") %>'></asp:Literal>
                                <asp:Label ID="lblMonPrecUni" runat="server" Text='<%#Eval("MON") %>' CssClass="Rotulo10"
                                    Style="margin-left: 2px;"></asp:Label>
							</div>
							<div style="display:inline-block; width:8em; text-align:right;">
								<asp:Literal ID="litCantidadPedida" runat="server" Text='<%# Eval("CANTIDAD") %>'></asp:Literal>
                                <fsn:FSNLinkTooltip ID="FSNlnkUniCant" runat="server" Text='<%# Eval("UNIDAD") %>'
                                    CssClass="Rotulo10" Font-Underline="false" ContextKey='<%# Eval("UNIDEN") %>'
                                    PanelTooltip="pnlTooltipTexto" Style="margin-left: 2px;">
                                </fsn:FSNLinkTooltip>
							</div>
							<div style="display:inline-block; width:8em; text-align:right;">
								<asp:Literal ID="litImportePedido" runat="server" Text='<%#Eval("IMPORTE") %>'></asp:Literal>
                                <asp:Label ID="lblMonImporte" runat="server" Text='<%#Eval("MON") %>' CssClass="Rotulo10"
                                    Style="margin-left: 2px;"></asp:Label>
							</div>
							<div style="display:inline-block; width:8em; text-align:right;">
								<asp:Literal ID="litCantRecibida" runat="server" Text='<%# Eval("CANT_REC") %>'></asp:Literal>
                                <fsn:FSNLinkTooltip ID="FSNlnkUniCantRec" runat="server" Text='<%# Eval("UNIDAD") %>'
                                    CssClass="Rotulo10" Font-Underline="false" ContextKey='<%# Eval("UNIDEN") %>'
                                    PanelTooltip="pnlTooltipTexto" Style="margin-left: 2px;">
                                </fsn:FSNLinkTooltip>
							</div>
							<div style="display:inline-block; width:8em; text-align:right;">
								<asp:Literal ID="litImporteRecibido" runat="server" Text='<%# Eval("IMPORTE_RECIBIDO") %>'></asp:Literal>
                                <asp:Label ID="lblMonImporteRecibido" runat="server" Text='<%#Eval("MON") %>' CssClass="Rotulo10"
                                    Style="margin-left: 2px;"></asp:Label>
							</div>
							<div style="display:inline-block; width:12em; text-align:right;">
								<igpck:WebNumericEditor ID="txtCantidad" runat="server" MinValue="0" DataMode="Double"
                                    Width="55" CssClass="texto10" Style="border:1px solid gray; display:inline-block; height:inherit; vertical-align:middle;">
                                    <Buttons SpinButtonsDisplay="OnRight"></Buttons>
                                </igpck:WebNumericEditor>
                                <asp:RangeValidator ID="valCantidad" runat="server" ErrorMessage="RangeValidator"
                                    ControlToValidate="txtCantidad" SetFocusOnError="True" Type="Double" Display="None">
                                </asp:RangeValidator>
                                <asp:Label ID="lblUnidRecep" runat="server" Text='<%#Eval("UNIDAD") %>' CssClass="Rotulo10"
                                    Style="margin-left:2px; display:inline-block; vertical-align:middle"></asp:Label>
                                <asp:Literal ID="litCantidadPendiente" runat="server"></asp:Literal>
							</div>
							<div style="display:inline-block; width:12em; text-align:right;">
								<igpck:WebNumericEditor ID="txtImporte" runat="server" MinValue="0" DataMode="Double"
                                    Width="55" CssClass="texto10" Style="border: 1px solid gray; display:inline-block; height:inherit; vertical-align:middle;">
                                    <Buttons SpinButtonsDisplay="OnRight"></Buttons>
                                </igpck:WebNumericEditor>
                                <asp:RangeValidator ID="valImporte" runat="server" ErrorMessage="RangeValidator"
                                    ControlToValidate="txtImporte" SetFocusOnError="True" Type="Double" Display="None">
                                </asp:RangeValidator>
                                <asp:Label ID="lblMontxtImporte" runat="server" Text='<%#Eval("MON") %>' CssClass="Rotulo10"
                                    Style="margin-left: 2px; display: inline-block; vertical-align: middle"></asp:Label>
                                <asp:Literal ID="litImportePendiente" runat="server"></asp:Literal>
							</div>
							<div style="display:inline-block; width:8em; text-align:right;">
								<asp:Label ID="lblFecRequerida" runat="server" style="margin-right:1em;"></asp:Label>
							</div>
							<div runat="server" id="pnlAlmacen" style="display:inline-block; width:7em; min-height:20px; margin-right:5px; text-align:right;">
                                <asp:DropDownList ID="lstAlmacen" runat="server" DataTextField="ALMACENDEN" DataValueField="ALMACEN"
                                    AutoPostBack="true" Style="font-size: 10px; width: 75px;">
                                </asp:DropDownList>
                                <asp:Label runat="server" ID="lblAlmacen" CssClass="texto10" Visible="false"></asp:Label>
                            </div>
							<asp:Panel id="tdSalidaAlmacen" runat="server" style="display:inline-block; width:7em; text-align:right;">
                                <div runat="server" id="pnlSalidaAlmacen" style="min-height:20px; margin-right:5px; text-align:right; text-align:center;">
                                    <input ID="chkSalidaAlmacen" type="checkbox" runat="server" DataTextField="SALIDA_ALMACEN" DataValueField="SALIDA_ALMACEN"/>                                                    
                                </div>
                            </asp:Panel>                                                                   
                            <asp:Panel ID="pnlActivo" runat="server" Visible="False">
                                <asp:Label ID="lblActivo" runat="server" CssClass="texto10" style="display:inline-block; margin-right:0.5em;"></asp:Label>     
                                <asp:TextBox ID="tbActivosLinea" runat="server" Width="200px" OnTextChanged="tbActivos_TextChanged"
                                    AutoPostBack="true" CssClass="texto10"></asp:TextBox>
                                <fsn:FSNImageInfo ImageUrl="~\images\info.gif" ID="FSNLnkActivo" runat="server" CssClass="Rotulo"
                                    ContextKey="" PanelInfo="FSNPanelDatosActivo"></fsn:FSNImageInfo>
                                <fsn:FSNLinkInfo ID="FSNLnkInfoActivo" runat="server" PanelInfo="FSNPanelDatosActivo"
                                    ContextKey="" CssClass="Rotulo10" Style="text-decoration: none;" Visible="false">
                                </fsn:FSNLinkInfo>
                                <asp:ImageButton ID="ImgBtnBuscarActivoLinea" runat="server" ImageUrl="~/images/abrir_ptos_susp.gif"
                                    CommandName="AbrirPanelBuscarActivo" CommandArgument='<%#Eval("LINEAID")%>' style="display:inline-block; vertical-align:middle;" />
                                <asp:ImageButton ID="ImgBtnAplicarActivoLineas" runat="server" ImageUrl="~/images/aplicar_abajo.gif"
                                    CommandName="AplicarActivoALineas" Visible="false" style="display:inline-block; vertical-align:middle;" />
                            </asp:Panel>                                    
							<asp:Panel runat="server" ID="pnlAtributosRecepcion" style="margin-top:0.3em;"></asp:Panel>
							<!--Ocultamos los campos de cantidad e importe pendientes de recibir-->
                            <asp:HiddenField ID="litImportePdteRecep" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="litPdteRecep" runat="server" Value='<%# If(Eval("CANTIDAD") - Eval("CANT_REC") < 0, 0, Eval("CANTIDAD") - Eval("CANT_REC")) %>'></asp:HiddenField>
                            <asp:HiddenField runat="server" ID="hidTipoRecepcion" Value='<%# Eval("TIPORECEPCION") %>' />
							<asp:HiddenField runat="server" ID="hidIdOrden" Value='<%# Eval("ORDEN") %>' />
                            <asp:HiddenField runat="server" ID="hidPorcenDesvio" Value='<%# Eval("PORCEN_DESVIO") %>' />
                        </ItemTemplate>	
						<ItemStyle CssClass="DatalistItemStyle" />
                    </asp:DataList>
                </div>
                <asp:Panel ID="pnlRecepcion" runat="server" CssClass="texto10" Style="margin-top:1.5em; padding-top:0.5em; border-top:dashed 0.1em black;">
                    <asp:ValidationSummary ID="ValSummaryPedido" runat="server" DisplayMode="List" ValidationGroup="Recepcion" CssClass="CajaError" ForeColor="" Width="400" />
                    <asp:Label ID="litRegistroRecepcionFechaRecepcion" runat="server" style="display:inline-block; vertical-align:middle;"></asp:Label>
                    <div style="display:inline-block; vertical-align:middle;">
						<igpck:WebDatePicker ID="dtFecRecepcion" runat="server" AllowNull="False" Font-Size="X-Small" 
							SkinID="Calendario" style="margin-left:0.5em;">
						</igpck:WebDatePicker>
						<asp:RequiredFieldValidator ID="reqvalFecRecepcion" runat="server" ErrorMessage="RequiredFieldValidator"
							ValidationGroup="Recepcion" ControlToValidate="dtFecRecepcion" Display="None"
							EnableClientScript="true" SetFocusOnError="True">
						</asp:RequiredFieldValidator>
					</div>
                    <asp:Label ID="lblFecRecepcion" runat="server" CssClass="texto10" style="display:inline-block; vertical-align:middle;"></asp:Label>                    
                    <asp:Panel runat="server" ID="pnlFechaContable" Visible="false" style="display:inline-block; margin-left: 3px;">
                        <asp:Label ID="litRegistroRecepcionFecContable" runat="server" style="display:inline-block; vertical-align:middle;"></asp:Label>
                        <div style="display:inline-block; vertical-align:middle;">
							<igpck:WebDatePicker ID="dtFecContable" runat="server" AllowNull="False" Font-Size="X-Small" SkinID="Calendario">
							</igpck:WebDatePicker>
						</div>
                        <asp:Label ID="lblFecContable" runat="server" CssClass="texto10"></asp:Label>                     
                    </asp:Panel>
                    <asp:Label ID="litRegistroRecepcionAlbaran" runat="server" style="display:inline-block; vertical-align:middle;"></asp:Label>                    
                    <asp:TextBox ID="txtRecepAlbaran" runat="server" Font-Size="Small" style="margin-left:0.5em;"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqvalAlbaran" runat="server" ErrorMessage="RequiredFieldValidator"
                        ValidationGroup="Recepcion" ControlToValidate="txtRecepAlbaran" Display="None"
                        EnableClientScript="True" SetFocusOnError="True">
                    </asp:RequiredFieldValidator>
					<div style="display:inline-block; vertical-align:middle;">
						<fsn:FSNButton ID="btnRegistrar" runat="server" ValidationGroup="Recepcion"></fsn:FSNButton>
					</div>
					<div style="display:inline-block; vertical-align:middle;">
						<fsn:FSNButton ID="btnRegistrarCerrar" runat="server" ValidationGroup="Recepcion" 
							CommandName="RegistraryCerrar" CommandArgument='<%# Eval("ID") %>'>
						</fsn:FSNButton>
					</div>
					<div style="display:inline-block; vertical-align:middle;">
						<fsn:FSNButton ID="btnCerrarOrdenes" CausesValidation="false" runat="server" ValidationGroup="Recepcion" 
							CommandName="CerrarOrden" CommandArgument='<%# Eval("ID") %>'>
						</fsn:FSNButton>
					</div>
                    <div id="DivPanelAdjuntos">
                        <!-- Panel de adjuntos del pedido -->
                        <asp:Panel ID="PanelAdjunPed" runat="server" Style="padding: 10px; display: none;" CssClass="modalPopup">
                            <asp:Image ID="imgCabAdjunPedido" runat="server" SkinID="CabAdjuntos" style="display:inline-block; vertical-align:middle;" />
							<asp:Label ID="LblArchAdjunPed" runat="server" CssClass="RotuloGrande" EnableViewState="False" style="display:inline-block; vertical-align:middle;" ></asp:Label>
							<input id="files" name="files" type="file" style="display:block; margin-top:1em;" size="55" />
							<textarea id="comentarioAdjunto" rows="5" cols="40" style="display:block; margin-top:1em;"></textarea>
							<span onclick="subirArchivo()" style="display:inline-block; width:49%; margin-top:1em;">
								<fsn:FSNButton ID="BtnAcepAdjunPed" runat="server" Alineacion="Right" AgregarOnClick="false" EnableViewState="False" ></fsn:FSNButton>
							</span>
							<span style="display:inline-block; width:49%; margin-top:1em;">
								<fsn:FSNButton ID="BtnCancelAdjunPed" runat="server" Alineacion="Left" EnableViewState="False"></fsn:FSNButton>							
							</span>
                        </asp:Panel>
                        <ajx:ModalPopupExtender ID="mpePnelAdjun" TargetControlID="btnOculto" PopupControlID="PanelAdjunPed"
                            BackgroundCssClass="modalBackground" runat="server" CancelControlID="BtnCancelAdjunPed"
                            BehaviorID="mpePnelAdjunBehavior">
                        </ajx:ModalPopupExtender>
                        <!-- deber�a mostrarse -->
                        <div id="adjuntarNuevo" style="margin-top:1em;">
							<input id="btnOculto" type="button" value="button" runat="server" style="display:none" />
							<asp:HiddenField runat="server" ClientIDMode="Static" Value="test" />
							<asp:TextBox ID="listaAdjuntosJSON" runat="server" ClientIDMode="Static" style="display: none;" Text="[]" ></asp:TextBox>
							<asp:Label ID="lblTituloListaAdjuntos" runat="server" CssClass="Negrita" style="margin-top:1em;"></asp:Label>
							<div id="listaAdjuntos" style="margin-top:0.5em; width:50%;"></div>
							<div id="NuevoAdjunto" class="OtrasOpciones" style="border:solid 1px Transparent;" onclick="mostrarAdjuntar()">
								<img id="imgNuevoAdjunto" class="Image20" alt="Nuevo Adjunto" src="<%=ConfigurationManager.AppSettings("ruta")%>images/adjunto.png"
									style="display:inline-block; vertical-align:middle;" />
								<asp:Label ID="lblNuevoAdjunto" runat="server" class="Texto12" style="display:inline-block; vertical-align:middle;"></asp:Label>
							</div>
						</div>
                    </div>
                    <div style="display:inline-block; width:50%; margin-top:1em;">
                        <asp:Literal ID="litObsRecep" runat="server"></asp:Literal>
                        <asp:TextBox ID="txtObsRecep" runat="server" Rows="5" TextMode="MultiLine" style="font-size: 12px; width:98%"></asp:TextBox>
                        <asp:CustomValidator ID="valObsRecep" runat="server" ErrorMessage="" EnableClientScript="True"
                            ValidationGroup="Recepcion" Display="None" OnServerValidate="ValidarRecepcion">
                        </asp:CustomValidator>
                    </div>
                    <div style="display:inline-block; width:49%; margin-top:1em; padding:1em 0em 0.3em 0em; vertical-align:top;">
                        <asp:CheckBox ID="chkProblemaRecep" runat="server" style="display:block;" />
                        <asp:CheckBox ID="chkRecepIncorrecta" runat="server" style="display:block;" />
                        <asp:CheckBox ID="chkNotificar" runat="server" style="display:block;" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlReabrir" runat="server" Visible="false" Style="margin: 5px">
                    <fsn:FSNButton ID="btnReabrirOrden" CausesValidation="false" runat="server" Text="DReabrir el pedido"
                        ValidationGroup="Recepcion" CommandName="ReabrirOrden" CommandArgument='<%# Eval("ID") %>'
                        Alineacion="Right"></fsn:FSNButton>
                </asp:Panel>  
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <!-- Panel Cargando... -->
    <script type="text/javascript">
        var ModalProgress = '<%=ModalProgress.ClientID%>';

        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);
        function beginReq(sender, args) {
            // shows the Popup
            var Emisor;
            var Comparacion = new Array();
            Emisor = args._postBackElement.id;
            Comparacion[0] = /btnBuscar/i;
            Comparacion[1] = /btnFirstPage/i;
            Comparacion[2] = /btnPreviousPage/i;
            Comparacion[3] = /ddlPage/i;
            Comparacion[4] = /btnNextPage/i;
            Comparacion[5] = /btnLastPage/i;
            Comparacion[6] = /ddlOrdenacion/i;
            Comparacion[7] = /imgBtnOrdenacion/i;
            Comparacion[8] = /lnkDenArt/i;
            Comparacion[9] = /btnRegistrar/i;
            Comparacion[10] = /btnCat/i;
            Comparacion[11] = /TvwCategorias/i;
            Comparacion[12] = /btnRecargaGrid/i;
            Comparacion[13] = /whdgEntregas/i;
            Comparacion[14] = /ibGuardarConfiguracion/i;
            for (var c in Comparacion) {
                if (String(Emisor).search(Comparacion[c]) != -1) {
                    var modalProg = $find(ModalProgress)
                    if (modalProg) {
                        modalProg.show();
                        var popupCargando = $get($find(ModalProgress)._PopupControlID);
                        if (popupCargando)
                            popupCargando.style.zIndex = 99999999;
                        popupCargando = null;
                    }
                    modalProg = null;
                    break;
                }
            }
        }

        function endReq(sender, args) {
            //  shows the Popup 
            $find(ModalProgress).hide();
        } 
        var ColHechasVisibles = new Array()
        var ColHechasInvisibles = new Array()

        /*<%--
        ''' Revisado por:blp. Fecha: 24/02/2012
        ''' <summary>
        ''' Evento que salta al pinchar en un check de recepci�n de pedidos.
        ''' Si deseleccionamos ambos, nos marcar el ultimo que hemos seleccionado, no dejando deseleccionarlos todos
        ''' </summary>
        ''' <param name="indice">Nos indica que check hemos seleccionado</param>       
        --%>*/
        function ValidacionChecks(indice) {
            var chkBoxList = document.getElementById("<%=chkRecepcionPedidos.clientID%>");
            if (chkBoxList) {
                var chkBoxCount = chkBoxList.getElementsByTagName("input");
                var cont = 0;
                for (var i = 0; i < chkBoxCount.length; i++) {
                    if (chkBoxCount[i].checked == true)
                        cont++;

                    if (i == indice)
                        objCheck = chkBoxCount[i]
                }
                if (cont == 0)
                    objCheck.checked = true;
            }
        }

        /*<%--Revisado por: blp. Fecha: 29/08/2012
        <summary>
        Provoca el postback para hacer la esportaci�n a excel
        </summary>
        <remarks>Llamada desde: Icono de exportar a Excel</remarks>
        --%>*/
        function MostrarExcel() {
            __doPostBack("", "Excel");
        }

        /*<%--Revisado por: blp. Fecha: 29/08/2012
        <summary>
        Provoca el postback para mostrar mas resultados en la grid de recepciones
        </summary>
        <remarks>Llamada desde: Icono de Mostrar mas resultados</remarks>
        --%>*/
        function MostrarMasResultados() {
            __doPostBack("", "MasResultados");
        }

        /*<%--Revisado por: blp. Fecha: 29/08/2012
        <summary>
        Provoca el postback para hacer la exportaci�n a Pdf
        </summary>
        <remarks>Llamada desde: Icono de exportar a Pdf</remarks>
        --%>*/
        function MostrarPdf() {
            __doPostBack("", "Pdf");
        }

        /*<%--Revisado por: blp. Fecha: 29/08/2012
        <summary>
        Abre el panel de configuraci�n de los campos a mostrar en el grid
        </summary> 
        <remarks>Llamada desde: evento click del control btnConfigurar</remarks>
        --%>*/
        function abrirPanelConfiguracion() {
            var mpeConfig = $find("<%=mpeConfig.clientID%>")
            mpeConfig.show()
            mpeConfig = null;
        }

        /*<%--Revisado por: blp. Fecha: 29/08/2012
        <summary>
        Cierra el panel de configuraci�n de los campos a mostrar en el grid
        y deshace los cambios que no se hayan confirmado con el bot�n aceptar
        </summary>
        <remarks>Llamada desde: evento click del control btnCancelar</remarks>
        --%>*/
        function cerrarPanelConfiguracion() {
            var grid = $find("<%=whdgEntregas.clientID %>")
            for (i = 0; i <= ColHechasVisibles.length - 1; i++) {
                grid.get_gridView().get_columns().get_columnFromKey("Key_" + ColHechasVisibles[i]).set_hidden(true)
                $('[id$=chk' + ColHechasVisibles[i] + ']')[0].checked = false
            }

            for (i = 0; i <= ColHechasInvisibles.length - 1; i++) {
                grid.get_gridView().get_columns().get_columnFromKey("Key_" + ColHechasInvisibles[i]).set_hidden(false)
                $('[id$=chk' + ColHechasInvisibles[i] + ']')[0].checked = true
            }

            //<%'Una vez cambiadas las visibilidades al estado original, las quitamos del array%>
            LimpiarVariablesConfiguracion();

            var mpeConfig = $find("<%=mpeConfig.clientID%>")
            mpeConfig.hide()
            mpeConfig = null;
        }

        //<%'Revisado por: blp. Fecha: 29/08/2012%>
        //<%'<summary>%>
        //<%'lblBusqueda y cpeBusqueda no pueden estar visibles al mismo tiempo %>
        //<%'Mostramos el panel resumen de los filtros cuando se oculta el panel de b�squeda y lo ocultamos cuando este �ltimo se muestra%>
        //<%'</summary>%>
        //<%'<remarks>Llamada desde el evento onClick de cliente del control pnlCabeceraRecepcion. Max. 0,1 seg.</remarks>%>
        function checkCollapseStatus() {
            var oControlcpeBusqueda = $find("<%=cpeBusqueda.clientID %>")
            var oControllblBusqueda = $get("<%=divListaFiltros.clientID %>")
            if (oControlcpeBusqueda) {
                if (oControlcpeBusqueda.get_Collapsed() == true) {
                    oControllblBusqueda.style.display = "none"
                }
                else {
                    oControllblBusqueda.style.display = "block"
                }
            }
            oControlcpeBusqueda = null;
            oControllblBusqueda = null;
        }

        function Collapse() {
            var cpe1 = $find("<%=cpeBusqueda.clientID %>");
            if (cpe1)
                cpe1.set_Collapsed();
        }

        /*<%--Revisado por: blp. Fecha: 29/08/2012
        <summary>
        M�todo necesario para ordenar las columnas del grid
        </summary>
        <remarks>Llamada desde: evento ColumnSorting del grid. M�x. 0,1 seg.</remarks>
        --%>*/
        function whdgEntregas_Sorting_ColumnSorting(grid, args) {
            var sorting = grid.get_behaviors().get_sorting();
            var col = args.get_column();
            var sortedCols = sorting.get_sortedColumns();
            for (var x = 0; x < sortedCols.length; ++x) {
                if (col.get_key() == sortedCols[x].get_key()) {
                    args.set_clear(false);
                    break;
                }
            }
        }

        //Cambiar el Z-index de los autocomplete
        function SetZIndex(control, args) {

            // Set auto complete extender control's z-index to a high value

            // so it will appear on top of, not under, the ModalPopUp extended panel.

            control._completionListElement.style.zIndex = 99999999;

        }
        //Cambiar el Z-index de los modalpopupExtender
        function SetzIndexModalPopUp(control, args) {

            // Set auto complete extender control's z-index to a high value

            // so it will appear on top of, not under, the ModalPopUp extended panel.

            control._popupElement.style.zIndex = 99999999;

        }

        //Recoge el ID del proveedor seleccionado con el autocompletar
        function ProveedorSeleccionado(sender, e) {
            oHidProveedor = document.getElementById('<%= hidProveedor.clientID %>');
            if (oHidProveedor)
                oHidProveedor.value = e._value
        }

        function ArticuloSeleccionado(sender, e) {
            oHidArticulo = document.getElementById("<%= hidArticulo.clientID %>");
            if (oHidArticulo) {
                oHidArticulo.value = e._value
            }
        }

        //funcion que al checkear un check del panel de configuracion muestra u oculta una columna del grid de configuracion
        function CheckConfiguracion(sender) {

            var grid = $find("<%=whdgEntregas.clientID %>")
            var name = sender.getAttribute("colName")

            if (sender.checked) {
                grid.get_gridView().get_columns().get_columnFromKey("Key_" + name).set_hidden(false)
                //<%'Puede que la columna ya est� en el array de invisibles, lo cual significa que se ha ocultado y luego mostrado. En ese caso, dado que el estado final de la columna es el original, la quitamos del array de invisibles y no la a�adimos a visibles (o sea, ignoramos esa columna porque al final se queda como al principio)%>
                //<%'Si previamente estaba en invisibles, la quitamos%>
                var enInvisibles = false;
                for (var z = 0; z <= ColHechasInvisibles.length - 1; z++) {
                    if (ColHechasInvisibles[z] == name) {
                        ColHechasInvisibles.splice(z, 1);
                        enInvisibles = true;
                        break;
                    }
                }
                //<%'A�adimos a visibles la columna, cuando no haya estado previamente en invisibles%>
                if (enInvisibles == false) {
                    var iVisible = ColHechasVisibles.length;
                    ColHechasVisibles[iVisible] = name;
                }
            } else {
                grid.get_gridView().get_columns().get_columnFromKey("Key_" + name).set_hidden(true)
                //<%'Puede que la columna ya est� en el array de visibles, lo cual significa que se ha mostrado y luego ocultado. En ese caso, dado que el estado final de la columna es el original, la quitamos del array de visibles y no la a�adimos a invisibles (o sea, ignoramos esa columna porque al final se queda como al principio)%>
                //<%'Si previamente estaban en visibles, las quitamos%>
                var enVisibles = false;
                for (var z = 0; z <= ColHechasVisibles.length - 1; z++) {
                    if (ColHechasVisibles[z] == name) {
                        ColHechasVisibles.splice(z, 1);
                        enVisibles = true;
                        break;
                    }
                }
                //<%'A�adimos a invisibles la columna, cuando no haya estado previamente en visibles%>
                if (enVisibles == false) {
                    var iInvisible = ColHechasInvisibles.length;
                    ColHechasInvisibles[iInvisible] = name
                }
            }
        }

        //Funcion que abre el modal popup de adjuntos
        //IdOrden: identificador de la orden de entrega
        //IdLinea: identificador de la linea de pedido
        //adjuntosRecepcion: bool (true si se trata de ajuntos de recepcion, false en otro caso).
        function AbrirAdjuntos(IdOrden, IdLinea, adjuntosRecepcion) {
            var hid_LineaPedido = document.getElementById("<%=hid_lineaPedido.clientID %>")
            var hid_OrdenEntrega = document.getElementById("<%=hid_OrdenEntrega.clientID %>")
            var hid_adjuntosRecepcion = document.getElementById("<%=hid_adjuntosRecepcion.clientID %>")
            var btnOcultoAdj = document.getElementById("<%=btnMostrarPopUpAdjuntos.clientID %>")
            hid_LineaPedido.value = IdLinea //4736
            hid_OrdenEntrega.value = IdOrden
            if (adjuntosRecepcion) {
                hid_adjuntosRecepcion.value = true;
            } else {
                hid_adjuntosRecepcion.value = false;
            }
			btnOcultoAdj.click();
        }

        //Funcion que abre el modal popup de detalle de articulo
        //IdLinea: identificador de la linea de pedido
        //4736
        function AbrirDetalleArticulo(IdLinea) {
            var hid_CodArticulo = document.getElementById("<%=hid_CodArticulo.clientID %>")
            var hid_LineaPedido = document.getElementById("<%=hid_lineaPedido.clientID %>")
            var btnOcultoArt = document.getElementById("<%=btnMostrarDetalleArticulo.clientID %>")
            hid_LineaPedido.value = IdLinea
            btnOcultoArt.click();
        }

        //<%'Borramos las variables de columnas visibles e invisibles porque el usuario ha aceptado los cambios%>
        function LimpiarVariablesConfiguracion() {
            ColHechasInvisibles.splice(0, ColHechasInvisibles.length);
            ColHechasVisibles.splice(0, ColHechasVisibles.length);
        }

        function MostrarOcultarCheck() {
            if ($('[id$=tdSoloLineasPendientes]')[0].style.display == '') {
                $('[id$=chkSoloLineasPendientes]')[0].checked = false
                $('[id$=tdSoloLineasPendientes]')[0].style.display = 'none'
            } else {
                $('[id$=tdSoloLineasPendientes]')[0].style.display = ''
            }
        }
		//<%'Incidencia 30817: Error al recuperar el estilo que comparten el conjunto de elementos que componen una columna de la grid de recepciones.
		'Para ocultar o mostrar con javascript una columna de la grid, infragistics ha hecho que todos los elementos de cada columna compartan una misma clase css.
		'De ese modo, mediante una serie de funciones, recupera el nombre de esa clase css y modifica el estilo display de esa clase css para que todos los elementos 
		'que la comparten (la columna) se muestren o se oculten al mismo tiempo. 
		'El nombre de la clase se busca en la colecci�n document.styleSheets. En esa colecci�n se guardan un listado de las clases css usadas en la p�gina (tanto internas como de enlaces externos).
		'El los postbacks, la funci�n que agrega a esa colecci�n las clases es: Infragistics.Web.UI.ControlMain.prototype.__appendStyles
		'Dentro de esta funci�n, para agregar las clases css en IE, se usa el m�todo addRule, que tiene 3 par�metros, uno de ellos opcional. 
		'El primero y el segundo, obligatorios, son: selector y declaration (http://www.javascriptkit.com/domref/stylesheet.shtml)
		'selector es el nombre de la clase css que se agrega (ej: .ig2d281703) y declaration es su contenido (ej: display:none;).
		'El problema es que el campo declaration es obligatorio, no puede ir vac�o. De hecho, para a�adir una clase css vac�a bastar�a con el par�metro declaration llevara un punto y coma ";"
		'Como no se ha hecho as�, el m�todo lanza un error, que los programadores de Infragistics capturan e ignoran mediante un catch(err)
		'El resultado es que esa clase deja de estar en la colecci�n, por lo que, cuando se intenta ocultar una 
		'columna cuya clase css ya no est� en document.styleSheets, se devuelve un error "'null' is null or not an object", 
		'dado que la funci�n Infragistics.Web.UI.GridColumn.prototype.set_hidden no controla si el estilo existe porque 
		'necesariamente deber�a estar presente en la colecci�n (ya que est� presente en la p�gina).
		'Para mantener la funcionalidad de ocultar la columna, he sobreescrito la funci�n __appendStyles de infragistics, 
		'de modo que cuando una clase css vaya vac�s, el par�metro declaration sea ";" en vez de "".
		'As�, todas las clases css se guardan correctamente en la colecci�n document.styleSheets.
		'No he comprobado si este error se debe a la migraci�n en abril de la versi�n 11.2.20112.2025 a la 11.2.20112.2225.
		'Por otro lado, en caso de ser corregida en alguna versi�n posterior a la 2225, habr�a que borrar este parche.
		%>
		$(document).ready(function () {
			if (Infragistics.Web.UI) {
				Infragistics.Web.UI.ControlMain.prototype.__appendStyles = function (cssClasses)
					{                       
						var igStyles = this.__findIgStyleSheet();
						/* OK Bug 29027 11/5/2010
						The browsers are not reacting not reactiong well if a class with the same name is appended
						it does not use the last class always.  And things get messed up, so
						we are going to remove the old version of the css class before we append the new one.*/
						this._removeExistingStyleRules(igStyles, cssClasses);
						if ($util.IsIE) {
							/* DAY 10/05/12 123034- Performance issue with _AppendCssRule and _RemoveCssRule javascript methods from version 10.2 to version 11.1 */
							if (igStyles.addRule) {
								var rules = cssClasses.split("}");
								for (var i = 0; i < rules.length - 1; i++) {
									// A.Y. 4.2.2011 Bug 64859
									// Due to some unknown reason invalid cssClasses({width: ;}) are returned from the server and
									// some browsers throw an error when adding styles like those.
									var parts = rules[i].split('{');
									if(parts[1]=="")
										parts[1]=";";
									try
									{
												igStyles.addRule(parts[0], parts[1]);
									}
									catch (err)
									{
									}
								}
							} else
										igStyles.cssText += cssClasses;
						} else {
							var rules = cssClasses.split("}");
							for (var i = 0; i < rules.length - 1; i++) {
										// A.Y. 4.2.2011 Bug 64859
										// Due to some unknown reason invalid cssClasses({width: ;}) are returned from the server and
										// some browsers throw an error when adding styles like those.
										try
										{
													igStyles.insertRule(rules[i] + "}", igStyles.cssRules.length);
										}
										catch (err)
										{
										}
							}
						}
						/* OK 9/6/2012 119846 - The scrollbar doesn't work after a column is hidden on client and an update panel on the page fires a postback.*/
						for (var igControlID in ig_controls) {
							var igControl = ig_controls[igControlID];
							if (igControl && igControl.__stylesHaveBeenAppended)
										igControl.__stylesHaveBeenAppended();
						}
				}
			}
		});

        function mostrarAdjuntar() {
            $("#files")[0].value = '';
            $("#comentarioAdjunto")[0].value = '';
            var btnOculto = document.getElementById("<%=btnOculto.clientID %>")
            btnOculto.click();
		}
		function subirArchivo() {
            var form_data = new FormData();
            form_data.append("content", $( '#files' )[0].files[0]);
            $.ajax({
                    url: "<%=ConfigurationManager.AppSettings("RutaEp") %>EPFileTransferHandler.ashx",
                    dataType: 'script',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (result) { procesarResultadoAdjunto(result);},
                    error: function (err) { alert('error: '+err.statusText); }, 
                    data: form_data,                         // Setting the data attribute of ajax with file_data
                    type: 'POST'
            });
        }   

        function procesarResultadoAdjunto(result) {
            var resultado = JSON.parse(result);
            var contenido = JSON.parse($('#listaAdjuntosJSON')[0].value);
            var nuevoAdjunto={};
            nuevoAdjunto.Nombre = resultado[0].name;
            nuevoAdjunto.FileName = resultado[0].name;
            nuevoAdjunto.dataSize = resultado[0].filesize;
            nuevoAdjunto.Comentario = $("#comentarioAdjunto")[0].value;
            nuevoAdjunto.url = resultado[0].url + "&t=14&ds="+resultado[0].filesize+"&isNew=True&Nombre="+resultado[0].name;
            nuevoAdjunto.DirectorioGuardar = resultado[0].folder;
			nuevoAdjunto.tipo = 14;
			nuevoAdjunto.Cod = '<%=Me.Usuario.Cod %>';
            nuevoAdjunto.Operacion = "I"
            
            contenido.push(nuevoAdjunto);
            $('#listaAdjuntosJSON')[0].value = JSON.stringify(contenido);
            $("#comentarioAdjunto")[0].innerHTML = "";
                                                    
            $find('mpePnelAdjunBehavior').hide();
            redrawAdjuntos();
        }

        function redrawAdjuntos() {
            var contenido = JSON.parse($('#listaAdjuntosJSON')[0].value);
            if (contenido.length == 0) {
                $('#listaAdjuntos')[0].innerHTML='';
            } else {
                var innerHTML = '<table width="100%" style="border: 1px solid silver;padding: 3px;">';
                var i;
                for (i = 0; i < contenido.length; i++) {
                    innerHTML += '<tr><td><div class="fondoAdjunto"><table width="100%"><tr>';
                    innerHTML += '<td><a alt="'+contenido[i].Nombre+'" href="'+contenido[i].url+'" target="_blank">'+contenido[i].Nombre+'</a>';
                    if (contenido[i].Comentario.length > 0)
                    {
                        innerHTML += ' - '+contenido[i].Comentario;
                    }
                    innerHTML += '</td>';
                    innerHTML += '<td  width="34px">';
                    innerHTML += '<button data-type="POST" onclick="eliminarAdjunto('+"'"+contenido[i].url+"'"+')" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only" role="button" title=""><span class="ui-button-icon-primary ui-icon ui-icon-trash"></span><span class="ui-button-text"></span></button>';
                    innerHTML += '</td></tr></table></div></td></tr>';
                }
                innerHTML += '</table>';
                $('#listaAdjuntos')[0].innerHTML = innerHTML
            }
        }

        function eliminarAdjunto(url) {
            var contenido = JSON.parse($('#listaAdjuntosJSON')[0].value);
            var innerHTML = '<table>';
            var i;
            for (i = 0; i < contenido.length; i++) {
                if (contenido[i].url = url)
                {
                    contenido.splice(i, 1);
                    break;
                }
            }
            $('#listaAdjuntosJSON')[0].value = JSON.stringify(contenido);                                     
            redrawAdjuntos();
        }
    </script>
</asp:Content>
