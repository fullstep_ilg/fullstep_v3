﻿Imports Fullstep.FSNServer

Partial Public Class Favoritos
    Inherits FSEPPage
    Private oOrdenFavoritos As FSNServer.COrdenFavoritos
    Private miIdOrden As Integer
    Private miLineaActual As Integer
    Private mbVerTodos As Boolean
    Private oEmpresas As FSNServer.CEmpresas
    Private _NoBorrarEstadoCollapses As Boolean = False

#Region " Cargas con la Página"

    ''' <summary>
    ''' Proceso que se ejecuta en el evento Init.
    ''' </summary>
    ''' <param name="sender">la pagina</param>
    ''' <param name="e">ARgumentos de evento</param>
    ''' <remarks>LLamada desde la pagina
    ''' Tiempo maximo 0,5 sec</remarks>
    Private Sub Favoritos_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        crearControlesPnlConfirm()
    End Sub

    ''' <summary>
    ''' Carga de la pagina
    ''' </summary>
    ''' <param name="sender">la pagina, al cargarse</param>
    ''' <param name="e">ARgumentos de evento</param>
    ''' <remarks>LLamada desde la pagina, al cargarse
    ''' Tiempo maximo 2 sec</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Favoritos
        Master.ImgCabIzq = "~/images/Favoritos.gif"
        Master.TituloCabIzq = Textos(0)
        Master.CabTabFavoritos.Selected = True

        mbVerTodos = False
        If Request("favid") Is Nothing Then
            miIdOrden = 0
        Else
            miIdOrden = CInt(Request("favid"))
            If Not IsPostBack() Then
                mbVerTodos = True
            End If
        End If

        Me.ScriptMgr.EnablePageMethods = True

        If Not IsPostBack() Then

            If Not HttpContext.Current.Cache("DsetPedidosFavoritos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                HttpContext.Current.Cache.Remove("DsetPedidosFavoritos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
            End If
            CargarTextos()

        End If

        ActualizarDsetPedidos()

        Dim pds As New PagedDataSource
        pds.AllowPaging = True
        pds.DataSource = DevolverPedidosFavoritos(CampoOrden, SentidoOrdenacion, DsetPedidos).DefaultView
        If ViewState("FavoritosPageSize") Is Nothing Then
            pds.PageSize = 10
        Else
            pds.PageSize = CType(ViewState("FavoritosPageSize"), Integer)
        End If
        If ViewState("FavoritosCurrentPage") Is Nothing Then
            pds.CurrentPageIndex = 0
        Else
            pds.CurrentPageIndex = CType(ViewState("FavoritosCurrentPage"), Integer)
        End If
        DlPedFavos.DataSource = pds
        If Not IsPostBack() Then DlPedFavos.DataBind()

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "CollapseHidden") Then
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "CollapseHidden", "var hddcollLineasID='" & collapseLineas.ClientID & "';", True)
        End If

    End Sub

    ''' <summary>
    ''' Procedimiento que actualiza el DataSet de pedidos favoritos
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: Page_Load, DLPedFavoritos_ItemCOmmand,BtnAceptarAnyadido_Click
    ''' Tiempo máximo: 0 seg </remarks>
    Private Sub ActualizarDsetPedidos()
        Dim Cookie As HttpCookie
        Cookie = Request.Cookies("FAVORITOS_CRIT_ORD")
        If Cookie Is Nothing OrElse String.IsNullOrEmpty(Cookie.Value) Then
            CampoOrden = "DENOMINACION"
        Else
            CampoOrden = Cookie.Value
            Cookie.Expires = DateTime.Now.AddDays(30)
            Response.AppendCookie(Cookie)
        End If
        Cookie = Request.Cookies("FAVORITOS_CRIT_DIREC")
        If Cookie Is Nothing OrElse String.IsNullOrEmpty(Cookie.Value) Then
            SentidoOrdenacion = "DESC"
        Else
            SentidoOrdenacion = Cookie.Value
            Cookie.Expires = DateTime.Now.AddDays(30)
            Response.AppendCookie(Cookie)
        End If
        If DsetPedidos.Tables("ORDENESFAVORITOS") Is Nothing Then
            DsetPedidos.Tables(0).TableName = "ORDENESFAVORITOS"
        End If
        If DsetPedidos.Tables("ORDENESFAVORITOS").PrimaryKey Is Nothing OrElse DsetPedidos.Tables("ORDENESFAVORITOS").PrimaryKey.Count = 0 Then
            Dim key() As DataColumn = {DsetPedidos.Tables("ORDENESFAVORITOS").Columns("ID")}
            DsetPedidos.Tables("ORDENESFAVORITOS").PrimaryKey = key
        End If
    End Sub

    ''' <summary>
    ''' ACtualiza el datalist de pedidos, como su propio nombre indica
    ''' </summary>
    ''' <remarks>Llamada desde los puntos de esta pagina que requieran que se refresque
    ''' Tiempo maximo 1 sec</remarks>
    Private Sub ActualizarDatalistPedidos()

        Dim pds As New PagedDataSource
        pds.AllowPaging = True
        pds.DataSource = DevolverPedidosFavoritos(CampoOrden, SentidoOrdenacion, DsetPedidos).DefaultView
        If ViewState("FavoritosPageSize") Is Nothing Then
            pds.PageSize = 10
        Else
            pds.PageSize = CType(ViewState("FavoritosPageSize"), Integer)
        End If
        If ViewState("FavoritosCurrentPage") Is Nothing Then
            pds.CurrentPageIndex = 0
        Else
            pds.CurrentPageIndex = CType(ViewState("FavoritosCurrentPage"), Integer)
        End If
        DlPedFavos.DataSource = pds
        DlPedFavos.DataBind()
        pnlGrid.Update()
    End Sub

    ''' <summary>
    ''' Carga los textos de la página en función del idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load</remarks>
    Private Sub CargarTextos()
        lblTituloAdjuntosLin.Text = Textos(33)
        lblTituloAdjuntos.Text = Textos(34)
    End Sub

#End Region

#Region "DataList Pedidos"

    ''' <summary>
    ''' Evento que se genera cuando los datos del datalist son cargados, guardando la página actual y el tamaño por página de líneas
    ''' </summary>
    ''' <param name="sender">El propio DataList al hacer DataBind()</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automática, cada vez que se hace DataBind()
    ''' Tiempo máximo: 0 seg</remarks>
    Private Sub dlPedFavos_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles DlPedFavos.DataBinding
        Dim pds As PagedDataSource = CType(CType(sender, DataList).DataSource, PagedDataSource)
        ViewState("FavoritosPageSize") = pds.PageSize
        ViewState("FavoritosCurrentPage") = pds.CurrentPageIndex
        DlPedFavos.Visible = pds.DataSourceCount > 0
        If Not _NoBorrarEstadoCollapses Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "BorrarEstadoCollapses", "PeticionesLineas=new Array(); ArrOrdenes=new Array();", True)
            collapseLineas.Value = String.Empty
        End If
    End Sub

    ''' <summary>
    ''' Realiza los eventos de los botones del datalist de favoritos
    ''' </summary>
    ''' <param name="source">el datalist</param>
    ''' <param name="e">argumentos de evento</param>
    ''' <remarks>Llamada desde el propio control
    ''' Tiemp maximo 1 sec</remarks>
    Private Sub dlOrdenes_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles DlPedFavos.ItemCommand
        Select Case e.CommandName
            Case "Adjuntos"
                Dim dr As DataRow() = CargarAdjuntosOrden(e.CommandArgument, DsetPedidos)
                pnlAdjuntosLin.Visible = False
                grdAdjuntos.DataSource = dr
                grdAdjuntos.DataBind()
                updpnlPopupAdjuntos.Update()
                mpeAdj.Show()
            Case "DetalleLineas"

                _NoBorrarEstadoCollapses = True
                Dim pds As PagedDataSource = CType(source, DataList).DataSource
                ActualizarDsetPedidos()
                pds.DataSource = DevolverPedidosFavoritos(CampoOrden, SentidoOrdenacion, DsetPedidos).DefaultView
                DlPedFavos.DataSource = pds
                DlPedFavos.DataBind()
                pnlGrid.Update()

            Case "EliminarPedidoFavorito"

			ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Favoritos
                Dim sTexto As String = Textos(23) 'Se procederá a eliminar el pedido favorito.¿Desea continuar?
                Confirmar(sTexto, "EliminarFavorito", "NoEliminarFavorito", e.CommandArgument, "")
            Case "EmitirPedidoFavorito", "ModificarPedidoFavorito"
                Dim oItemsNotIssuable As DataTable = userHasItemsCategories(e.CommandArgument)
                If oItemsNotIssuable IsNot Nothing AndAlso oItemsNotIssuable.Rows.Count > 0 Then
                    Dim messagePanel As Integer
                    Dim numLineas As Integer = 0
                    Try
                        numLineas = DsetPedidos.Tables("ORDENESFAVORITOS").Rows.Find(e.CommandArgument).GetChildRows("REL_ORDENES_LINEASPEDIDO").Count
                    Catch ex As Exception
                    End Try
                    If oItemsNotIssuable.Rows.Count = numLineas Then
                        If e.CommandName = "EmitirPedidoFavorito" Then
                            messagePanel = 2 'No es posible emitir el pedido. Ninguno de los artículos del favorito está disponible en el catálogo o bien han sido trasladados a una categoría de la cual no es aprovisionador.
                        Else
                            messagePanel = 4 'No es posible modificar el pedido. Ninguno de los artículos del favorito está disponible en el catálogo o bien han sido trasladados a una categoría de la cual no es aprovisionador.
                        End If
                    Else
                        If e.CommandName = "EmitirPedidoFavorito" Then
                            messagePanel = 1 'No es posible emitir el pedido. Los siguientes artículos ya nos están disponibles en el catálogo o han sido trasladados a una categoría de la cual no es aprovisionador. Es necesario borrarlos del pedido si desea continuar con su emisión.
                        Else
                            messagePanel = 3 'No es posible modificar el pedido. Los siguientes artículos ya nos están disponibles en el catálogo o han sido trasladados a una categoría de la cual no es aprovisionador. Es necesario borrarlos del pedido si desea continuar con su emisión.
                        End If
                    End If
                    pnlCancelarFav.CargarPanel(oItemsNotIssuable, messagePanel)
                Else
                    Dim DsetOrdenFavorito As DataSet = CrearDatasetFavoritoParaEmision(DsetPedidos, e.CommandArgument)

                    Me.InsertarEnCache("DsetFavoritoEmitir_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString, DsetOrdenFavorito)
                    If e.CommandName = "ModificarPedidoFavorito" Then
                        Response.Redirect("EmisionPedido.aspx?DesdeFavorito=True&favNuevo=False&ModificarFavorito=True&ID=" & e.CommandArgument)
                    Else
                        Response.Redirect("EmisionPedido.aspx?DesdeFavorito=True&favNuevo=False&ID=" & e.CommandArgument)
                    End If
                End If
            Case "VerTodos"
                mbVerTodos = False
                miIdOrden = 0
                If Not HttpContext.Current.Cache("DsetPedidosFavoritos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                    HttpContext.Current.Cache.Remove("DsetPedidosFavoritos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
                End If

                ActualizarDsetPedidos()
                ActualizarDatalistPedidos()

                e.Item.FindControl("btnVerTodos").Visible = False
        End Select
    End Sub

    ''' <summary>
    ''' Evento que se genera en el momento de la carga de datos del DataList Ordenes, cargando textos o atributos de los controles
    ''' </summary>
    ''' <param name="sender">El DataList que ha generado el evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automática, cada vez que se Cargan los datos del DataList de ordenes
    ''' Tiempo máximo: 0'5 seg</remarks>
    Private Sub dlOrdenes_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DlPedFavos.ItemDataBound
        With e.Item
            Dim fila As DataRowView = CType(.DataItem, DataRowView)
            Select Case .ItemType
                Case ListItemType.Header
                    Dim pagds As Paginador = CType(.FindControl("pagdsCabecera"), Paginador)
                    Dim li As New ListItemCollection
                    li.Add(New ListItem(Textos(4), "DENOMINACION"))
                    li.Add(New ListItem(Textos(5), "PROVEEDOR"))
                    li.Add(New ListItem(Textos(6), "IMPORTE"))
                    pagds.ListOrden = li
                    pagds.CampoOrden = CampoOrden
                    pagds.SentidoOrdenacion = SentidoOrdenacion
                    pagds.FindControl("imgbtnExportar").Visible = False
                    pagds.FindControl("imgbtnImprimir").Visible = False
                    pagds.InitPaginador()
                    CType(.FindControl("litCabEmpresa"), Literal).Text = Textos(29)
                    CType(.FindControl("litCabDenominacion"), Literal).Text = Textos(4)
                    CType(.FindControl("litCabProveedor"), Literal).Text = Textos(5)
                    If Acceso.gbTrasladoAProvERP Then
                        CType(.FindControl("litCabProveedorERP"), Literal).Text = Textos(31)
                    Else
                        CType(.FindControl("tblCabecera"), Table).Rows(3).Cells(7).Visible = False
                    End If
                    CType(.FindControl("litCabReceptor"), Literal).Text = Textos(30)
                    CType(.FindControl("litCabImporte"), Literal).Text = Textos(6)
                    CType(.FindControl("litCabObservaciones"), Literal).Text = Textos(7)
                    CType(.FindControl("litCabArchivos"), Literal).Text = Textos(8)
                    'CType(.FindControl("litCabModificar"), Literal).Text = Textos(9)
                    CType(.FindControl("litCabEliminar"), Literal).Text = Textos(22)
                    CType(.FindControl("btnVerTodos"), FSNWebControls.FSNButton).Text = Textos(37)
                    CType(.FindControl("btnVerTodos"), FSNWebControls.FSNButton).Visible = mbVerTodos
                Case ListItemType.Footer
                    Dim pagds As Paginador = CType(.FindControl("pagdsPie"), Paginador)
                    Dim li As New ListItemCollection
                    li.Add(New ListItem(Textos(4), "DENOMINACION"))
                    li.Add(New ListItem(Textos(5), "PROVEEDOR"))
                    li.Add(New ListItem(Textos(6), "IMPORTE"))
                    pagds.ListOrden = li
                    pagds.CampoOrden = CampoOrden
                    pagds.SentidoOrdenacion = SentidoOrdenacion
                    pagds.FindControl("imgbtnExportar").Visible = False
                    pagds.FindControl("imgbtnImprimir").Visible = False
                    pagds.InitPaginador()
                Case ListItemType.Item, ListItemType.AlternatingItem
                    CType(.FindControl("imgbtnLineas"), ImageButton).OnClientClick = "return comprobarlineasmodificadas(" & fila.Item("ID") & ",null,'desplegar')"
                    If Not OrdenTieneLineas(fila.Item("ID")) Then
                        CType(.FindControl("CollapsiblePanelExtender1"), AjaxControlToolkit.CollapsiblePanelExtender).Enabled = False
                        CType(.FindControl("imgbtnLineas"), ImageButton).Visible = False
                        CType(.FindControl("BtnEmitirPedFavo"), FSNWebControls.FSNButton).Visible = False
                        CType(.FindControl("litFilaImporte"), Literal).Visible = False
                    End If
                    CType(.FindControl("fsnlnkFilaProveedor"), FSNWebControls.FSNLinkInfo).Text = _
                        IIf(Me.FSNUser.MostrarCodProve, fila.Item("PROVECOD") & " - ", "") & fila.Item("PROVEDEN")
                    If Acceso.gbTrasladoAProvERP Then
                        CType(.FindControl("litFilaProveedorERP"), Literal).Text = DBNullToStr(fila.Item("COD_PROVE_ERP"))
                    Else
                        CType(.FindControl("tblItem"), Table).Rows(0).Cells(7).Visible = False
                    End If
                    If Not IsDBNull(fila.Item("IMPORTE")) Then
                        CType(.FindControl("litFilaImporte"), Literal).Text = FSNLibrary.FormatNumber(fila.Item("IMPORTE") * fila.Item("EQUIV"), Me.Usuario.NumberFormat) & " " & fila.Item("MON")
                    Else
                        CType(.FindControl("litFilaImporte"), Literal).Text = "0"
                    End If
                    If IsDBNull(fila.Item("OBS")) Then
                        CType(.FindControl("btnFilaObservaciones"), ImageButton).Enabled = False
                        CType(.FindControl("btnFilaObservaciones"), ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, "observaciones_desactivado.gif")
                    End If
                    If IsDBNull(fila.Item("ADJUNTOS")) OrElse fila.Item("ADJUNTOS") = 0 Then
                        CType(.FindControl("btnFilaArchivos"), ImageButton).Enabled = False
                        CType(.FindControl("btnFilaArchivos"), ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, "adjunto_desactivado.gif")
                    End If
                    CType(.FindControl("BtnEmitirPedFavo"), FSNWebControls.FSNButton).Text = Textos(21)
                    CType(.FindControl("btnFilaEliminarFavorito"), ImageButton).ToolTip = Textos(10)

                    Dim lineas() As DataRow = DsetPedidos.Tables("ORDENESFAVORITOS").Rows.Find(fila.Item("ID")).GetChildRows("REL_ORDENES_LINEASPEDIDO")
                    If lineas.Length > 0 Then _
                    ScriptManager.RegisterStartupScript(Me.Page, sender.GetType(), "ScriptOrden" & fila.Item("ID"), _
                            "new Orden(" & fila.Item("ID") & _
                                ",'" & .FindControl("CollapsiblePanelExtender1").ClientID & "'" & _
                                ",'" & .FindControl("grdLineasPedido").ClientID & "'" & _
                                ",'" & .FindControl("pnlCargando").ClientID & "'" & _
                                ",'" & .FindControl("imgbtnLineas").ClientID & "'" & _
                                ",null" & _
                                ",null" & _
                                ");", True)

                    Dim grdLineas As GridView = CType(.FindControl("grdLineasPedido"), GridView)
                    grdLineas.Attributes.Add("ORDEN", fila.Item("ID"))
                    grdLineas.Columns(0).HeaderText = Textos(12)
                    grdLineas.Columns(1).HeaderText = Textos(4)
                    grdLineas.Columns(2).HeaderText = Textos(13)
                    grdLineas.Columns(3).HeaderText = Textos(14)
                    grdLineas.Columns(4).HeaderText = Textos(15)
                    grdLineas.Columns(5).HeaderText = Textos(16)
                    grdLineas.Columns(6).HeaderText = Textos(6) & " (" & fila.Item("MON") & ")"
                    grdLineas.Columns(7).HeaderText = Textos(17)
                    grdLineas.Columns(8).HeaderText = Textos(26)
                    grdLineas.Columns(9).HeaderText = Textos(8)
                    grdLineas.Columns(10).HeaderText = Textos(7)
                    grdLineas.Columns(11).HeaderText = Textos(22)
                    If FSNUser.MostrarCodArt Then
                        grdLineas.Columns(0).Visible = True
                    Else
                        grdLineas.Columns(0).Visible = False
                    End If
                    If lineas.Length > 0 Then _
                        grdLineas.DataSource = lineas.CopyToDataTable()
                    grdLineas.DataBind()
                    If collapseLineas.Value.Split(",").Contains(fila.Item("ID")) Then
                        CType(.FindControl("CollapsiblePanelExtender1"), AjaxControlToolkit.CollapsiblePanelExtender).Collapsed = False
                    End If
            End Select
        End With
    End Sub

    ''' <summary>
    ''' Evento que se genera cada vez que el GridLineas carga sus datos
    ''' </summary>
    ''' <param name="sender">El propio Grid que ha generado el evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automático, cada vez que se recargan los datos del GridLineas
    ''' Tiempo máximo: 0 seg</remarks>
    Public Sub grdLineasPedidos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        With e.Row
            If .RowType = DataControlRowType.DataRow Then
                Dim fila As DataRowView = CType(.DataItem, DataRowView)
                Dim cambio As Double = DBNullToDbl(DsetPedidos.Tables("ORDENESFAVORITOS").Rows.Find(fila.Item("ORDEN")).Item("EQUIV"))
                Dim oUnidadPedido As CUnidadPedido = Nothing
                If Not DBNullToSomething(fila.Item("UNIDAD")) = Nothing Then
                    oUnidadPedido = TodasLasUnidades.Item(Trim(fila.Item("UNIDAD")))
                    oUnidadPedido.AsignarFormatoaUnidadPedido(Me.Usuario.NumberFormat)
                End If
                If oUnidadPedido.NumeroDeDecimales Is Nothing Then
                    Dim decimales As Integer = NumeroDeDecimales(DBNullToStr(fila.Item("CANTIDAD")))
                    oUnidadPedido.UnitFormat.NumberDecimalDigits = decimales
                End If

                If FSNUser.MostrarCodArt Then
                    CType(.FindControl("lnkCodArt"), LinkButton).Visible = True
                Else
                    CType(.FindControl("lnkCodArt"), LinkButton).Visible = False
                End If
                If fila.Item("TIPORECEPCION") = 0 Then
                    .Cells(3).Text = FSNLibrary.FormatNumber(fila.Item("CANTIDAD"), oUnidadPedido.UnitFormat)
                    CType(.FindControl("lblPrecUni"), Label).Text = FSNLibrary.FormatNumber(fila.Item("PRECIOUNITARIO") * fila.Item("FC") * If(cambio <> 0, cambio, 1), Me.Usuario.NumberFormat)
                Else
                    CType(.FindControl("lblPrecUni"), Label).Text = ""
                    .Cells(3).Text = 1
                End If
                .Cells(6).Text = FSNLibrary.FormatNumber(fila.Item("IMPORTE") * If(cambio <> 0, cambio, 1), Me.Usuario.NumberFormat)
                If IsDBNull(fila.Item("CODPROCE")) Then .FindControl("FSNlnkProce").Visible = False
                If IsDBNull(fila.Item("ADJUNTOS")) OrElse fila.Item("ADJUNTOS") = 0 Then
                    CType(.FindControl("imgbtnAdjLin"), ImageButton).Enabled = False
                    CType(.FindControl("imgbtnAdjLin"), ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, "adjunto_desactivado.gif")
                End If
                If IsDBNull(fila.Item("OBS")) OrElse String.IsNullOrEmpty(fila.Item("OBS")) Then
                    CType(.FindControl("FSNbtnObs"), ImageButton).Enabled = False
                    CType(.FindControl("FSNbtnObs"), ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, "observaciones_desactivado.gif")
                End If

            End If
        End With
    End Sub

    ''' <summary>
    ''' Lanza los eventos de los botones de la grid de Lineas de pedido favorito
    ''' </summary>
    ''' <param name="sender">los botones de la grid</param>
    ''' <param name="e">Arguemtnos de evento</param>
    ''' <remarks>Llamada desde los botones de la grid
    ''' Tiempo maximo 1 sec</remarks>
    Public Sub grdLineasPedidos_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Select Case e.CommandName
            Case "AdjuntosLinea"
                Dim dr As DataRow() = CargarAdjuntosLinea(e.CommandArgument, DsetPedidos)
                pnlAdjuntosLin.Visible = True
                Dim obs As Object = DsetPedidos.Tables("LINEASPEDIDO").Rows.Find(e.CommandArgument).Item("OBSADJUN")
                If IsDBNull(obs) OrElse String.IsNullOrEmpty(obs) Then
                    litComentarioAdjuntosLin.Visible = False
                Else
                    litComentarioAdjuntosLin.Text = CType(obs, String)
                End If
                grdAdjuntos.DataSource = dr
                grdAdjuntos.DataBind()
                updpnlPopupAdjuntos.Update()
                mpeAdj.Show()
            Case "MostrarDetalleArt"
                pnlDetalleArticulo.TipoPedido = "Favorito"
                pnlDetalleArticulo.CargarDetalleArticulo(DsetPedidos, e.CommandArgument, True)
            Case "EliminarLinea"
                Dim IdOrden As Integer
                For Each fila As DataRow In DsetPedidos.Tables("LINEASPEDIDO").Rows
                    If fila.Item("LINEAID") = e.CommandArgument Then
                        IdOrden = fila.Item("ORDEN")
                        Exit For
                    End If
                Next

                If Not HttpContext.Current.Cache("DsetPedidosFavoritos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                    HttpContext.Current.Cache.Remove("DsetPedidosFavoritos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
                End If
                Dim oLineaFavorito As FSNServer.CLineaFavoritos = Me.FSNServer.Get_Object(GetType(FSNServer.CLineaFavoritos))
                oLineaFavorito.Usuario = FSNUser.Cod
                oLineaFavorito.ID = e.CommandArgument
                oLineaFavorito.EliminarFavorito(IdOrden)
                ActualizarDatalistPedidos()
                pnlGrid.Update()
        End Select
    End Sub

#End Region

#Region "Adjuntos"

    ''' <summary>
    ''' Evento que se genera cada vez que un control dentro del Grid Adjuntos es activado por el usuario, realizando las acciones pertinentes dependiendo del control
    ''' </summary>
    ''' <param name="sender">El propio control  que ha realizado el evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: La propia página, cada vez que se utiliza un control dentro del Grid de adjuntos
    ''' Tiempo máximo: alrededor de 0,5 seg</remarks>
    Private Sub grdAdjuntos_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdAdjuntos.RowCommand
        If e.CommandName = "Descargar" Then
            Dim sArgs() As String = Split(e.CommandArgument)
            Select Case CType(sArgs(1), TiposDeDatos.Adjunto.Tipo)
                Case TiposDeDatos.Adjunto.Tipo.Favorito_Orden_Entrega
                    Dim dr As DataRow = Array.Find(CargarAdjuntosOrden(sArgs(2), DsetPedidos), Function(fila As DataRow) fila.Item("ID") = sArgs(0))
                    Dim AdjunDescargar As Fullstep.FSNServer.Adjunto = Me.FSNServer.Get_Object(GetType(Fullstep.FSNServer.Adjunto))
                    AdjunDescargar.Id = sArgs(0)
                    AdjunDescargar.Nombre = dr.Item("NOMBRE")
                    AdjunDescargar.dataSize = dr.Item("DATASIZE")
                    AdjunDescargar.FileName = AdjunDescargar.Nombre
                    AdjunDescargar.DirectorioGuardar = ConfigurationManager.AppSettings("temp") & "\downloads"
                    If Not IO.Directory.Exists(AdjunDescargar.DirectorioGuardar) Then _
                        IO.Directory.CreateDirectory(AdjunDescargar.DirectorioGuardar)
                    If IO.File.Exists(AdjunDescargar.DirectorioGuardar & "\" & AdjunDescargar.FileName) Then
                        Dim i As Integer = 1
                        While IO.File.Exists(AdjunDescargar.DirectorioGuardar & "\" & AdjunDescargar.FileName & "_" & i.ToString())
                            i = i + 1
                        End While
                        AdjunDescargar.FileName = AdjunDescargar.FileName & "_" & i.ToString()
                    End If
                    Dim sNombre As String = AdjunDescargar.FileName
                    AdjunDescargar.EscribirADisco(AdjunDescargar.DirectorioGuardar & "\", sNombre, sArgs(1))
                    Response.AddHeader("content-disposition", "attachment; filename=""" & AdjunDescargar.FileName & """")
                    Response.ContentType = "application/octet-stream"
                    Response.WriteFile(AdjunDescargar.DirectorioGuardar & "\" & sNombre)
                    Response.End()
                Case TiposDeDatos.Adjunto.Tipo.Favorito_Linea_Pedido
                    Dim dr As DataRow = Array.Find(CargarAdjuntosLinea(sArgs(2), DsetPedidos), Function(fila As DataRow) fila.Item("ID") = sArgs(0))
                    Dim AdjunDescargar As Fullstep.FSNServer.Adjunto = Me.FSNServer.Get_Object(GetType(Fullstep.FSNServer.Adjunto))
                    AdjunDescargar.Id = sArgs(0)
                    AdjunDescargar.Nombre = dr.Item("NOMBRE")
                    AdjunDescargar.dataSize = dr.Item("DATASIZE")
                    AdjunDescargar.DirectorioGuardar = ConfigurationManager.AppSettings("temp") & "\downloads"
                    If Not IO.Directory.Exists(AdjunDescargar.DirectorioGuardar) Then _
                        IO.Directory.CreateDirectory(AdjunDescargar.DirectorioGuardar)
                    AdjunDescargar.FileName = AdjunDescargar.Nombre
                    If IO.File.Exists(AdjunDescargar.DirectorioGuardar & "\" & AdjunDescargar.FileName) Then
                        Dim i As Integer = 1
                        Do While IO.File.Exists(AdjunDescargar.DirectorioGuardar & "\" & AdjunDescargar.FileName & "_" & i.ToString())
                            i = i + 1
                        Loop
                        AdjunDescargar.FileName = AdjunDescargar.FileName & "_" & i.ToString()
                    End If
                    Dim sNombre As String = AdjunDescargar.FileName
                    AdjunDescargar.EscribirADisco(AdjunDescargar.DirectorioGuardar & "\", sNombre, sArgs(1))
                    Response.AddHeader("content-disposition", "attachment; filename=""" & AdjunDescargar.Nombre & """")
                    Response.ContentType = "application/octet-stream"
                    Response.WriteFile(AdjunDescargar.DirectorioGuardar & "\" & sNombre)
                    Response.End()
            End Select
        End If
    End Sub

    ''' <summary>
    ''' Evento que se genera cada vez que se cargan los datos del Grid de Adjuntos
    ''' </summary>
    ''' <param name="sender">El propio Grid que ha generado el evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: La propia página, cada vez que se recargan los datos del Grid
    ''' Tiempo máximo: 0 seg</remarks>
    Private Sub grdAdjuntos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdAdjuntos.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim fila As DataRow = CType(e.Row.DataItem, DataRow)
            CType(e.Row.FindControl("litFechaAdjunto"), Literal).Text = FSNLibrary.FormatDate(fila.Item("FECHA"), Me.Usuario.DateFormat)
            CType(e.Row.FindControl("lnkbtnAdjunto"), LinkButton).Text = _
                fila.Item("NOMBRE") & " (" & FSNLibrary.FormatNumber(fila.Item("DATASIZE") / 1024, Me.Usuario.NumberFormat) & " kb.)"
            Select Case CType(fila.Item("TIPO"), TiposDeDatos.Adjunto.Tipo)
                Case TiposDeDatos.Adjunto.Tipo.Favorito_Orden_Entrega
                    CType(e.Row.FindControl("lnkbtnAdjunto"), LinkButton).CommandArgument = fila.Item("ID") & " " & fila.Item("TIPO") & " " & fila.Item("ORDEN")
                Case TiposDeDatos.Adjunto.Tipo.Favorito_Linea_Pedido
                    CType(e.Row.FindControl("lnkbtnAdjunto"), LinkButton).CommandArgument = fila.Item("ID") & " " & fila.Item("TIPO") & " " & fila.Item("LINEA")
            End Select
            Me.ScriptMgr.RegisterPostBackControl(e.Row.FindControl("lnkbtnAdjunto"))
            If IsDBNull(fila.Item("COMENTARIO")) OrElse String.IsNullOrEmpty(fila.Item("COMENTARIO")) Then
                CType(e.Row.FindControl("imgComentarioAdjunto"), FSNWebControls.FSNImageTooltip).Visible = False
            End If
        End If
    End Sub

#End Region

#Region "Ordenación"

    Private Property CampoOrden() As String
        Get
            If String.IsNullOrEmpty(ViewState.Item("FavoritosCampoOrden")) Then
                Return "DENOMINACION"
            Else
                Return ViewState.Item("FavoritosCampoOrden")
            End If
        End Get
        Set(ByVal value As String)
            ViewState.Item("FavoritosCampoOrden") = value
        End Set
    End Property

    Private Property SentidoOrdenacion() As String
        Get
            If String.IsNullOrEmpty(ViewState.Item("FavoritosSentidoOrdenacion")) Then
                Return "DESC"
            Else
                Return ViewState.Item("FavoritosSentidoOrdenacion")
            End If
        End Get
        Set(ByVal value As String)
            ViewState.Item("FavoritosSentidoOrdenacion") = value
        End Set
    End Property

    ''' <summary>
    ''' Cambia de orden los resultados del datalist de pedidos favoritos
    ''' </summary>
    ''' <param name="Campo">campo de ordenacion</param>
    ''' <param name="Sentido">Sentido asc o desc</param>
    ''' <remarks>llamda desde la pagina
    ''' Tiempo maximo 0 sec</remarks>
    Public Sub Paginador_OnCambioOrden(ByVal Campo As String, ByVal Sentido As String)
        Dim bActualizar As Boolean = False
        If Campo <> CampoOrden Then
            CampoOrden = Campo
            Dim cookie As HttpCookie = Request.Cookies("FAVORITOS_CRIT_ORD")
            If cookie Is Nothing Then
                cookie = New HttpCookie("FAVORITOS_CRIT_ORD", CampoOrden)
            Else
                cookie.Value = CampoOrden
            End If
            cookie.Expires = DateTime.Now.AddDays(30)
            Response.AppendCookie(cookie)
            bActualizar = True
        End If
        If Sentido <> SentidoOrdenacion Then
            SentidoOrdenacion = Sentido
            Dim cookie As HttpCookie = Request.Cookies("FAVORITOS_CRIT_DIREC")
            If cookie Is Nothing Then
                cookie = New HttpCookie("FAVORITOS_CRIT_DIREC", SentidoOrdenacion)
            Else
                cookie.Value = SentidoOrdenacion
            End If
            cookie.Expires = DateTime.Now.AddDays(30)
            Response.AppendCookie(cookie)
            bActualizar = True
        End If
        If bActualizar Then
            ViewState("FavoritosCurrentPage") = Nothing
            ActualizarDatalistPedidos()
        End If
    End Sub

#End Region

#Region "Tooltips"

    ''' Revisado por: blp. Fecha:15/02/2012
    ''' <summary>
    ''' Recupera las obervaciones del pedido favorito
    ''' </summary>
    ''' <param name="contextKey">el id del pedido favorito</param>
    ''' <returns>el texto con las observaciones</returns>
    ''' <remarks>Llamada desde la pagina actual
    ''' Tiempo maximo 0 sec</remarks>
    <System.Web.Services.WebMethodAttribute(), _
        System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function Observaciones(ByVal contextKey As String) As String
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        If oUsuario Is Nothing Then Return Nothing
        Dim dsOrdenes As DataSet
        If HttpContext.Current.Cache("DsetPedidosFavoritos_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString()) Is Nothing Then
            dsOrdenes = GenerarDsetPedidos(oUsuario.Cod, oUsuario.Idioma, CInt(contextKey))
        Else
            dsOrdenes = HttpContext.Current.Cache("DsetPedidosFavoritos_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString())
        End If
        Return dsOrdenes.Tables("ORDENESFAVORITOS").Rows.Find(contextKey).Item("OBS")
    End Function

    ''' Revisado por: blp. Fecha:15/02/2012
    ''' <summary>
    ''' Recupera las observaciones del proveedor
    ''' </summary>
    ''' <param name="contextKey">el id del proveedore</param>
    ''' <returns>el texto con las observaciones</returns>
    ''' <remarks>Llamada desde la pagina actual
    ''' Tiempo maximo 0 sec</remarks>
    <System.Web.Services.WebMethodAttribute(), _
    System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function ObservacionesProv(ByVal contextKey As String) As String
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        If oUsuario Is Nothing Then Return Nothing
        Dim dsOrdenes As DataSet
        If HttpContext.Current.Cache("DsetPedidosFavoritos_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString()) Is Nothing Then
            dsOrdenes = GenerarDsetPedidos(oUsuario.Cod, oUsuario.Idioma, CInt(contextKey))
        Else
            dsOrdenes = HttpContext.Current.Cache("DsetPedidosFavoritos_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString())
        End If
        Return dsOrdenes.Tables("ORDENESFAVORITOS").Rows.Find(contextKey).Item("COMENTPROVE")
    End Function

    ''' Revisado por: blp. Fecha:15/02/2012
    ''' <summary>
    ''' Recupera las obervaciones de la linea de pedido favorito
    ''' </summary>
    ''' <param name="contextKey">el id de la linea de pedido favorito</param>
    ''' <returns>el texto con las observaciones</returns>
    ''' <remarks>Llamada desde la pagina actual
    ''' Tiempo maximo 0 sec</remarks>
    <System.Web.Services.WebMethodAttribute(), _
    System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function ObservacionesLinea(ByVal contextKey As String) As String
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        If oUsuario Is Nothing Then Return Nothing
        Dim dsOrdenes As DataSet
        Dim iIds() = contextKey.Split("/")
        If iIds.Length = 2 Then
            Dim idPedido = iIds(0)
            Dim idLinea = iIds(1)
            If HttpContext.Current.Cache("DsetPedidosFavoritos_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString()) Is Nothing Then
                dsOrdenes = GenerarDsetPedidos(oUsuario.Cod, oUsuario.Idioma, CInt(contextKey))
            Else
                dsOrdenes = HttpContext.Current.Cache("DsetPedidosFavoritos_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString())
            End If
            Return dsOrdenes.Tables("LINEASPEDIDO").Rows.Find(idLinea).Item("OBS")
        End If
        Return String.Empty
    End Function

    ''' <summary>
    ''' Función que devuelve el comentario asociado a un adjunto
    ''' </summary>
    ''' <param name="contextKey">Identificador del adjunto</param>
    ''' <returns>Un string que se corresponde con el comentario del archivo adjunto</returns>
    ''' <remarks>
    ''' Llamada desde: La propia página, cada vez que se pasa el ratón por la imagen de observaciones de adjunto
    ''' Tiempo máximo: 0 seg</remarks>
    <System.Web.Services.WebMethodAttribute(), _
    System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function ComentarioAdj(ByVal contextKey As String) As String
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        If oUsuario Is Nothing Then Return Nothing
        If HttpContext.Current.Cache("DsetPedidosFavoritos_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString()) Is Nothing Then _
            Return Nothing
        Dim dsOrdenes As DataSet = _
            HttpContext.Current.Cache("DsetPedidosFavoritos_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString())
        Dim keys() As String = Split(contextKey)
        Dim sComent As String = String.Empty
        Select Case CType(keys(1), TiposDeDatos.Adjunto.Tipo)
            Case TiposDeDatos.Adjunto.Tipo.Favorito_Orden_Entrega
                Return dsOrdenes.Tables("ADJUNTOS_ORDEN").Rows.Find(keys(0)).Item("COMENTARIO")
            Case TiposDeDatos.Adjunto.Tipo.Favorito_Linea_Pedido
                Return dsOrdenes.Tables("ADJUNTOS_LINEA").Rows.Find(keys(0)).Item("COMENTARIO")
        End Select
        Return sComent
    End Function

    ''' <summary>
    ''' Función que devuelve el texto que se le pasa
    ''' </summary>
    ''' <param name="contextKey">Texto a devolver</param>
    ''' <returns>El mismo texto que se le pasa a la función</returns>
    ''' <remarks>
    ''' Llamada desde: La propia página
    ''' Tiempo máximo: 0 seg</remarks>
    <System.Web.Services.WebMethodAttribute(), _
    System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function DevolverTexto(ByVal contextKey As String) As String
        Return contextKey
    End Function

#End Region

#Region " Consultas "

    ''' <summary>
    ''' Función que devuelve un valor booleano indicando si una Orden de Favoritos tiene líneas
    ''' </summary>
    ''' <param name="IdOrden">Identificador de la orden a comprobar</param>
    ''' <returns>True si la orden tiene alguna línea, false en caso contrario</returns>
    ''' <remarks>
    ''' Llamada desde: DlOrdenes_ItemDataBound
    ''' Tiempo máximo: 0 seg</remarks>
    Private Function OrdenTieneLineas(ByVal IdOrden As Integer) As Boolean
        For Each fila As DataRow In DsetPedidos.Tables(1).Rows
            If fila.Item("Orden") = IdOrden Then
                Return True
            End If
        Next
        Return False
    End Function

    ''' Revisado por: blp. Fecha:15/02/2012
    ''' <summary>
    ''' Propiedad que nos recupera un dataset con los pedidos favoritos
    ''' </summary>
    ''' <value></value>
    ''' <returns>el dataset con los pedidos</returns>
    ''' <remarks>LLamada desde la pagina
    ''' Timepo maximo 1 sec</remarks>
    Public ReadOnly Property DsetPedidos() As DataSet
        Get
            If HttpContext.Current.Cache("DsetPedidosFavoritos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                Dim dsOrdenes As DataSet = GenerarDsetPedidos(Me.Usuario.Cod, Me.Usuario.Idioma, miIdOrden)
                If dsOrdenes Is Nothing Then
                    Response.Redirect("CatalogoWeb.aspx")
                    Response.End()
                End If
                Me.InsertarEnCache("DsetPedidosFavoritos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString(), dsOrdenes)
                Return dsOrdenes
            Else
                Return HttpContext.Current.Cache("DsetPedidosFavoritos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
            End If
        End Get
    End Property

    ''' Revisado por: blp. Fecha: 15/02/2012
    ''' <summary>
    ''' Función que genera el DataSet con el pedido que se quiere modificar para utilizarlo en la página
    ''' </summary>
    ''' <param name="CodPersona">Código de la persona del usuario que esta utilizando la aplicación</param>
    ''' <param name="Idioma">Idioma de la aplicación</param>
    ''' <param name="IdOrden">Identificador de la orden a filtrar</param>
    ''' <returns>Un DataSet con la orden de favoritos filtrada y sus líneas correspondientes</returns>
    ''' <remarks>
    ''' Llamada desde: La función GenerarDsetPedidos
    ''' Tiempo máximo:0,5 seg aprox.</remarks>
    Private Shared Function GenerarDsetPedidos(ByVal CodPersona As String, ByVal Idioma As String, ByVal IdOrden As Integer) As DataSet
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oOrdenesFavoritos As FSNServer.COrdenesFavoritos = oServer.Get_Object(GetType(FSNServer.COrdenesFavoritos))
        Dim sMoneda As String = String.Empty
        Return oOrdenesFavoritos.BuscarTodasOrdenes(CodPersona, IdOrden, "", sMoneda, Idioma, True)
    End Function

    ''' <summary>
    ''' Función que devuelve los pedidos favoritos asociados a un proveedor
    ''' </summary>
    ''' <param name="OrdenacionCampo">Campo de la ordenación de los pedidos favoritos del proveedor</param>
    ''' <param name="OrdenacionSentido">Sentido de la ordenación(ASC o DESC)</param>
    ''' <param name="Pedidos">Pedidos favoritos a devolver</param>
    ''' <returns>Un DataTable con todos los pedidos favoritos del proveedor </returns>
    ''' <remarks>
    ''' Llamada desde:Page_Load y ActualizarDataListPedidos 
    ''' Tiempo máximo: 0 seg</remarks>
    Public Function DevolverPedidosFavoritos(ByVal OrdenacionCampo As String, ByVal OrdenacionSentido As String, ByVal Pedidos As DataSet) As DataTable
        If Pedidos.Tables.Count > 0 AndAlso Not Pedidos.Tables(0).TableName = "ORDENESFAVORITOS" Then
            Pedidos.Tables(0).TableName = "ORDENESFAVORITOS"
        End If
        Dim dtPed As DataTable = Pedidos.Tables("ORDENESFAVORITOS").Copy()
        Dim query As IEnumerable(Of DataRow)
        If dtPed.Rows.Count > 0 Then
            Select Case OrdenacionCampo
                Case "DENOMINACION"
                    dtPed.Columns.Add("ORDENACION", Type.GetType("System.String"), "DEN")
                Case "PROVEEDOR"
                    dtPed.Columns.Add("ORDENACION", Type.GetType("System.String"), "PROVEDEN")
                Case "IMPORTE"
                    dtPed.Columns.Add("ORDENACION", Type.GetType("System.Double"), "IMPORTE * EQUIV")
            End Select
            If OrdenacionSentido = "ASC" Then
                query = From Datos In dtPed.AsEnumerable() _
                        Order By DBNullToSomething(Datos.Item("ORDENACION")) Ascending _
                        Select Datos
            Else
                query = From Datos In dtPed.AsEnumerable() _
                      Order By DBNullToSomething(Datos.Item("ORDENACION")) Descending _
                      Select Datos
            End If
            dtPed = CType(query, EnumerableRowCollection(Of DataRow)).CopyToDataTable()
        End If
        Return dtPed
    End Function
#End Region


#Region "Unidades"
    ''' <summary>
    ''' Propiedad que recupera el conjunto de las unidades de Pedido para su posterior uso en la página
    ''' </summary>
    Private ReadOnly Property TodasLasUnidades() As CUnidadesPedido
        Get
            Dim oUnidadesPedido As CUnidadesPedido = Me.FSNServer.Get_Object(GetType(FSNServer.CUnidadesPedido))
            If HttpContext.Current.Cache("TodasLasUnidades" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                oUnidadesPedido.CargarTodasLasUnidades(Me.Usuario.Idioma)
                Me.InsertarEnCache("TodasLasUnidades" & Me.Usuario.Idioma.ToString(), oUnidadesPedido)
            Else
                oUnidadesPedido = HttpContext.Current.Cache("TodasLasUnidades" & Me.Usuario.Idioma.ToString())
            End If
            Return oUnidadesPedido
        End Get
    End Property


#End Region

#Region "Direcciones Envio Facturas"

    ''' <summary>
    ''' Direcciones de envío de facturas de todas las empresas.
    ''' Valor que se guarda en caché para su uso por todos los usuarios que lo requieran
    ''' </summary>
    Private ReadOnly Property DireccionesEnvioFacturas() As CEnvFacturaDirecciones
        Get
            Dim CDireccionesEnvioFacturas As CEnvFacturaDirecciones = Me.FSNServer.Get_Object(GetType(CEnvFacturaDirecciones))
            If HttpContext.Current.Cache("CDireccionesEnvioFacturas") Is Nothing Then
                CDireccionesEnvioFacturas.CargarDireccionesDeEnvioDeFactura()
                Me.InsertarEnCache("CDireccionesEnvioFacturas", CDireccionesEnvioFacturas)
            Else
                CDireccionesEnvioFacturas = HttpContext.Current.Cache("CDireccionesEnvioFacturas")
            End If
            Return CDireccionesEnvioFacturas
        End Get
    End Property

#End Region

#Region "Empresas"
    ''' <summary>
    ''' Empresas del usuario
    ''' </summary>
    Private ReadOnly Property Empresas() As CEmpresas
        Get
            If oEmpresas Is Nothing Then
                If HttpContext.Current.Cache("_Empresas" & FSNUser.CodPersona) Is Nothing Then
                    oEmpresas = Me.FSNServer.Get_Object(GetType(FSNServer.CEmpresas))
                    Dim oPersona As FSNServer.CPersona
                    oPersona = FSNServer.Get_Object(GetType(FSNServer.CPersona))
                    oPersona.Cod = Usuario.CodPersona
                    oEmpresas = oPersona.CargarEmpresas(Me.PargenSM, FSNUser.PedidosOtrasEmp)
                    If Acceso.gbDirEnvFacObl AndAlso oEmpresas IsNot Nothing Then
                        For Each empresa As CEmpresa In oEmpresas
                            If empresa.DireccionesEnvioFactura Is Nothing Then
                                Dim listaDirecciones As List(Of CEnvFacturaDireccion) = Me.DireccionesEnvioFacturas.FindAll(Function(direc As CEnvFacturaDireccion) direc.IdEmpresa = empresa.ID)
                                Dim direcciones As CEnvFacturaDirecciones = Me.FSNServer.Get_Object(GetType(CEnvFacturaDirecciones))
                                If listaDirecciones IsNot Nothing Then
                                    For Each dir As CEnvFacturaDireccion In listaDirecciones
                                        direcciones.Add(dir)
                                    Next
                                    empresa.DireccionesEnvioFactura = direcciones
                                End If
                            End If
                        Next
                    End If
                    Me.InsertarEnCache("_Empresas" & FSNUser.CodPersona, oEmpresas)
                Else
                    oEmpresas = HttpContext.Current.Cache("_Empresas" & FSNUser.CodPersona)
                End If
            End If
            Return oEmpresas
        End Get
    End Property

#End Region

#Region "Panel confirmación"

    ''' <summary>
    ''' Método que crea los controles que componen el panel de confirmación
    ''' </summary>
    ''' <remarks>Llamada desde Confirmar. Tiempo Máximo inferior a 0,1 seg</remarks>
    Private Sub crearControlesPnlConfirm()
        Dim tabla As New HtmlControls.HtmlTable
        Dim imgConfirm As New HtmlControls.HtmlImage
        Dim lblConfirm As New Label
        Dim lblMaxLength As New Label
        Dim txtConfirm As New TextBox
        Dim btnAceptarConfirm As New Global.Fullstep.FSNWebControls.FSNButton
        Dim btnCancelarConfirm As New Global.Fullstep.FSNWebControls.FSNButton

        tabla.CellPadding = 0
        tabla.CellSpacing = 20
        tabla.Border = 0
        tabla.ID = "tblConfirm"

        Dim fila As New HtmlControls.HtmlTableRow()
        Dim celda As New HtmlControls.HtmlTableCell()
        celda.ColSpan = 2
        celda.Align = "left"
        'imgConfirm.ID = upConfirm.ID & "_imgConfirm"
        imgConfirm.ID = "imgConfirm"
        imgConfirm.Style.Item("float") = "Left"
        imgConfirm.Style.Item("margin-right") = "10px"
        imgConfirm.Src = "~/images/Icono_Error_Amarillo_40x40.gif"
        lblConfirm.ID = "lblConfirm"
        celda.Controls.Add(imgConfirm)
        celda.Controls.Add(lblConfirm)
        fila.Controls.Add(celda)
        tabla.Controls.Add(fila)

        fila = New HtmlControls.HtmlTableRow()
        celda = New HtmlControls.HtmlTableCell()
        celda.ColSpan = 2
        celda.Align = "left"
        lblMaxLength.ID = "lblMaxLength"
        'lblMaxLength.Style.Item("display") = "block"
        lblMaxLength.Style.Item("display") = "none"
        celda.Controls.Add(lblMaxLength)
        fila.Controls.Add(celda)
        tabla.Controls.Add(fila)

        fila = New HtmlControls.HtmlTableRow()
        celda = New HtmlControls.HtmlTableCell()
        celda.ColSpan = 2
        celda.Align = "left"
        txtConfirm.ID = "txtConfirm"
        txtConfirm.TextMode = TextBoxMode.MultiLine
        txtConfirm.Rows = 4
        txtConfirm.MaxLength = 4000
        txtConfirm.Width = Unit.Percentage(100)
        txtConfirm.Style.Item("display") = "none"
        celda.Controls.Add(lblMaxLength)
        fila.Controls.Add(celda)
        tabla.Controls.Add(fila)

        fila = New HtmlControls.HtmlTableRow()
        celda = New HtmlControls.HtmlTableCell()
        fila.Style.Item("width") = "300px"
        celda.Align = "right"
        celda.Style.Item("width") = "150px"
        btnAceptarConfirm.ID = "btnAceptarConfirm"
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Favoritos
        btnAceptarConfirm.Text = Textos(41)
        AddHandler btnAceptarConfirm.Click, AddressOf Me.btnAceptarConfirm_Click
        celda.Controls.Add(btnAceptarConfirm)
        fila.Controls.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.Align = "left"
        celda.Style.Item("width") = "150px"
        btnCancelarConfirm.ID = "btnCancelarConfirm"
        btnCancelarConfirm.Alineacion = FSNWebControls.FSNTipos.Alineacion.Left
        btnCancelarConfirm.Style.Item("display") = "block"
        btnCancelarConfirm.Text = Textos(40)
        AddHandler btnCancelarConfirm.Click, AddressOf Me.btnCancelarConfirm_Click
        celda.Controls.Add(btnCancelarConfirm)
        fila.Controls.Add(celda)
        tabla.Controls.Add(fila)
        upConfirm.ContentTemplateContainer.Controls.Add(tabla)
        upConfirm.Update()
        'mpeConfirm.Show()

        btnCancelarConfirm.OnClientClick = "$find('" & mpeConfirm.ClientID & "').hide();"
        btnAceptarConfirm.OnClientClick = "$find('" & mpeConfirm.ClientID & "').hide();"
    End Sub

    ''' <summary>
    ''' Método que lanza el panel de confirmación
    ''' </summary>
    ''' <param name="Texto">Texto a mostrar en el panel</param>
    ''' <param name="AccionAceptar">Acción que debe efectuarse caso de aceptar</param>
    ''' <param name="AccionCancelar">Ácción que debe efectuarse caso de cancelar</param>
    ''' <param name="ArgumentAceptar">Argumento que se pasa al aceptar</param>
    ''' <param name="ArgumentCancelar">Argumento que se pasa al cancelar</param>
    ''' <remarks>Llamada desde </remarks>
    Private Sub Confirmar(ByVal Texto As String, ByVal AccionAceptar As String, ByVal AccionCancelar As String, ByVal ArgumentAceptar As String, ByVal ArgumentCancelar As String)
        'If upConfirm.FindControl("tblConfirm") Is Nothing Then
        '    crearControlesPnlConfirm()
        'End If

        CType(upConfirm.FindControl("lblConfirm"), Label).Text = Texto
        CType(upConfirm.FindControl("btnAceptarConfirm"), FSNWebControls.FSNButton).CommandName = AccionAceptar
        CType(upConfirm.FindControl("btnAceptarConfirm"), FSNWebControls.FSNButton).CommandArgument = ArgumentAceptar
        CType(upConfirm.FindControl("btnCancelarConfirm"), FSNWebControls.FSNButton).CommandName = AccionCancelar
        CType(upConfirm.FindControl("btnCancelarConfirm"), FSNWebControls.FSNButton).CommandArgument = ArgumentCancelar
        upConfirm.Update()
        mpeConfirm.Show()

        'imgConfirm.InnerHtml = "<img src='images/Icono_Error_Amarillo_40x40.gif'>"
        'lblConfirm.Text = Texto
        'txtConfirm.Style.Item("display") = "none"
        'lblMaxLength.Style.Item("display") = "none"
        'btnAceptarConfirm.CommandName = AccionAceptar
        'btnAceptarConfirm.CommandArgument = ArgumentAceptar
        'btnCancelarConfirm.Style.Item("display") = "block"
        'btnCancelarConfirm.CommandName = AccionCancelar
        'btnCancelarConfirm.CommandArgument = ArgumentCancelar
        'upConfirm.Update()
        'mpeConfirm.Show()
    End Sub

    ''' <summary>
    ''' Método que se ejecuta cuando se pulsa el botón aceptar (btnAceptarConfirm) del panel de confirmacion (pnlConfirm)
    ''' </summary>
    ''' <param name="sender">El botón</param>
    ''' <param name="e">argumentos del evento click</param>
    ''' <remarks>Llamada desde el propio botón</remarks>
    Private Sub btnAceptarConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case CType(sender, FSNWebControls.FSNButton).CommandName
            Case "EliminarFavorito"
                If Not HttpContext.Current.Cache("DsetPedidosFavoritos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                    HttpContext.Current.Cache.Remove("DsetPedidosFavoritos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
                End If
                Dim oOrdenFavorito As FSNServer.COrdenFavoritos = Me.FSNServer.Get_Object(GetType(FSNServer.COrdenFavoritos))
                oOrdenFavorito.ID = CType(sender, FSNWebControls.FSNButton).CommandArgument
                oOrdenFavorito.Usuario = Me.Usuario.Cod
                oOrdenFavorito.EliminarFavorito()
                ActualizarDatalistPedidos()
                pnlGrid.Update()
                Dim CantFavoritosStr As String
                Dim CantFavoritos As Integer
                CantFavoritosStr = Right(Master.CabTabFavoritos.Text, Master.CabTabFavoritos.Text.Length - InStr(Master.CabTabFavoritos.Text, "("))
                CantFavoritosStr = Left(CantFavoritosStr, InStr(CantFavoritosStr, ")") - 1)
                CantFavoritos = CInt(CantFavoritosStr)
                CantFavoritosStr = Left(Master.CabTabFavoritos.Text, InStr(Master.CabTabFavoritos.Text, "(") - 1)
                Master.CabTabFavoritos.Text = CantFavoritosStr & "(" & CantFavoritos - 1 & ")"
                Master.CabUpdPestanyasCatalogo.Update()
        End Select
    End Sub

    ''' <summary>
    ''' Método que se ejecuta cuando se pulsa el botón Cancelar (btnCancelarConfirm) del panel de confirmacion (pnlConfirm)
    ''' </summary>
    ''' <param name="sender">El botón</param>
    ''' <param name="e">argumentos del evento click</param>
    ''' <remarks>Llamada desde el propio botón</remarks>
    Private Sub btnCancelarConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case CType(sender, FSNWebControls.FSNButton).CommandName
            Case "NoEliminarFavorito"
                'No hacemos nada
        End Select
    End Sub

#End Region

    ''' <summary>
    ''' Función que compara las categorias de los artículos en el favorito a emitir, las categorías de esos artículos en el catálogo y si el usuario puede emitir en esas categorías. 
    ''' Si el usuario puede emitir todas las categorías de los artículos (o sea, si puede emitir los artículos), devuelve un datatable VACIO.
    ''' Si no, devuelve un datatable con los codigos y descripciónes de los artículos para cuyas categorías el usuario no puede emitir (o sea, no puede emitir esos artículos)
    ''' </summary>
    ''' <returns>
    ''' Si el usuario puede emitir todas las categorías de los artículos (o sea, si puede emitir los artículos), devuelve un datatable VACIO.
    ''' Si no, devuelve un datatable con los codigos y descripciónes de los artículos para cuyas categorías el usuario no puede emitir (o sea, no puede emitir esos artículos)
    ''' </returns>
    ''' <remarks>. Tiempo máx inferior a 1seg.</remarks>
    Private Function userHasItemsCategories(ByVal IDFav As Integer) As DataTable
        Dim oOrden As COrden = FSNServer.Get_Object(GetType(FSNServer.COrden))
        oOrden.ID = IDFav
        Dim oUserItemsNotIssuable As DataTable = oOrden.UserCanIssueItemsCategories(Usuario.Cod)
        Return oUserItemsNotIssuable
    End Function
End Class