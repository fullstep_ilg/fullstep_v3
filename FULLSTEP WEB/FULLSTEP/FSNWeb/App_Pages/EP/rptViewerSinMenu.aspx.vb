﻿Partial Public Class rptViewerSinMenu
    Inherits FSEPPage

    Private Report As CrystalDecisions.CrystalReports.Engine.ReportDocument
    ''' revisado por: blp. Fecha: 26/12/2012
    ''' <summary>
    ''' Evento que se lanza al cargar la página
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumentos del evento lanzado</param>
    ''' <remarks>Llamada desde: Carga inicial de la página
    ''' Tiempo máximo: 0 sec.</remarks>
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        If Not IsPostBack Then
            Cache.Remove("ReportEP" & Me.Usuario.Cod)
            Report = New CrystalDecisions.CrystalReports.Engine.ReportDocument
            If Right(ConfigurationManager.AppSettings("rptFilesPath"), 1) = "\" Then
                Report.Load(ConfigurationManager.AppSettings("rptFilesPath") & "Seguimiento.rpt")
            Else
                Report.Load(ConfigurationManager.AppSettings("rptFilesPath") & "\" & "Seguimiento.rpt")
            End If

            Dim ds As DataSet
            ds = HttpContext.Current.Cache("dsReportImpEP" & Me.Usuario.Cod)
            
            'Si recibimos sólo dos tables es que no hay tabla de imputacion pero el report al que le estamos pasando el 
            'dataset necesita 3 tablas. La tercera la construimos y pasamos vacía
            If ds.Tables.Count < 3 Then
                Dim dtTablaVaciaImputacion As New DataTable
                dtTablaVaciaImputacion.TableName = "LINEASPEDIMPUTACION"
                Dim colVacia As DataColumn = New DataColumn("NIVEL")
                colVacia.DataType = System.Type.GetType("System.Int32")
                dtTablaVaciaImputacion.Columns.Add(colVacia)
                colVacia = New DataColumn("LINEA")
                colVacia.DataType = System.Type.GetType("System.Int32")
                dtTablaVaciaImputacion.Columns.Add(colVacia)
                colVacia = New DataColumn("PRES0")
                colVacia.DataType = System.Type.GetType("System.Int32")
                dtTablaVaciaImputacion.Columns.Add(colVacia)
                colVacia = New DataColumn("PRES1")
                colVacia.DataType = System.Type.GetType("System.Int32")
                dtTablaVaciaImputacion.Columns.Add(colVacia)
                colVacia = New DataColumn("PRES2")
                colVacia.DataType = System.Type.GetType("System.Int32")
                dtTablaVaciaImputacion.Columns.Add(colVacia)
                colVacia = New DataColumn("PRES3")
                colVacia.DataType = System.Type.GetType("System.Int32")
                dtTablaVaciaImputacion.Columns.Add(colVacia)
                colVacia = New DataColumn("PRES4")
                colVacia.DataType = System.Type.GetType("System.Int32")
                dtTablaVaciaImputacion.Columns.Add(colVacia)
                colVacia = New DataColumn("PARTIDA_DEN")
                colVacia.DataType = System.Type.GetType("System.String")
                dtTablaVaciaImputacion.Columns.Add(colVacia)
                colVacia = New DataColumn("NIVEL_DEN")
                colVacia.DataType = System.Type.GetType("System.String")
                dtTablaVaciaImputacion.Columns.Add(colVacia)
                colVacia = New DataColumn("CCCOD")
                colVacia.DataType = System.Type.GetType("System.Int32")
                dtTablaVaciaImputacion.Columns.Add(colVacia)
                colVacia = New DataColumn("CCDEN")
                colVacia.DataType = System.Type.GetType("System.String")
                dtTablaVaciaImputacion.Columns.Add(colVacia)

                ds.Tables.Add(dtTablaVaciaImputacion)
            End If



            'Si el dataset recibido no tiene en la tabla ordenes el campo DIRECCIONENVIOFRA, lo añadimos VACIO
            Dim bMostrarDireccion As Boolean = False 'Este booleano lo pasamos a PonerTextosReport()
            ' El valor de bMostrarDireccion será false cuando no hay permisos para mostrar la direccion o 
            ' cuando la solicitud del informe venga de una página que no proporciona el campo Direccion Envio Factura.
            If Acceso.gbDirEnvFacObl Then
                If ds.Tables.Count > 0 AndAlso ds.Tables("ORDENES").Rows.Count > 0 And ds.Tables("ORDENES").Columns.Item("DIRECCIONENVIOFRA") Is Nothing Then
                    ds.Tables("ORDENES").Columns.Add("DIRECCIONENVIOFRA", System.Type.GetType("System.String"))
                    bMostrarDireccion = False
                Else
                    bMostrarDireccion = True
                End If
            Else
                'Si Me.Acceso.gbDirEnvFacObl ES FALSE en la tabla ordenes el campo DIRECCIONENVIOFRA, lo quitamos
                If ds.Tables.Count > 0 AndAlso ds.Tables("ORDENES").Rows.Count > 0 Then
                    If ds.Tables("ORDENES").Columns.Item("DIRECCIONENVIOFRA") IsNot Nothing Then
                        ds.Tables("ORDENES").Columns.Remove("DIRECCIONENVIOFRA")
                    End If
                    'y lo volvemos a poner VACIO
                    ds.Tables("ORDENES").Columns.Add("DIRECCIONENVIOFRA", System.Type.GetType("System.String"))
                    bMostrarDireccion = False
                End If
            End If

            PonerTextosReport(bMostrarDireccion)

            PonerEstadoOrdenEnReport()

            Report.SetDataSource(ds)

            Me.InsertarEnCache("ReportEP" & Me.Usuario.Cod, Report)

            HttpContext.Current.Cache.Remove("dsReportImpEP" & Me.Usuario.Cod)
        Else
            Report = Cache("ReportEP" & Me.Usuario.Cod)
        End If

        CrystalReportViewer1.ReportSource = Report
    End Sub

    ''' <summary>
    ''' Pone los textos al report
    ''' </summary>
    ''' <param name="bMostrarDireccion">Nos indica si debemos poner texto al campo de Dirección Envío Factura.
    ''' </param>
    ''' <remarks>Llamada desde la carga inical de la pagina
    ''' Tiempo maximo 0 sec</remarks>
    Private Sub PonerTextosReport(ByVal bMostrarDireccion As Boolean)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento

        Dim sNumeroERP As String = ""
        If Acceso.gbOblCodPedido = True Then
            Dim oCParametros As FSNServer.CParametros = FSNServer.Get_Object(GetType(FSNServer.CParametros))
            sNumeroERP = oCParametros.CargarLiteralParametros(TiposDeDatos.LiteralesParametros.PedidoERP, FSNUser.Idioma)
        End If

        Report.DataDefinition.FormulaFields("txtEmpresa").Text = "'" & Textos(96) & "'"
        Report.DataDefinition.FormulaFields("txtProveedor").Text = "'" & Textos(26) & "'"
        Report.DataDefinition.FormulaFields("txtNPedido").Text = "'" & Textos(25) & "'"
        If Acceso.gbVerNumeroProveedor Then
            Report.DataDefinition.FormulaFields("txtNPedidoProveedor").Text = "'" & Textos(73) & "'"
        Else
            Report.DataDefinition.FormulaFields("txtNPedidoProveedor").Text = ""
        End If
        Report.DataDefinition.FormulaFields("txtNumeroERP").Text = "'" & sNumeroERP & "'"
        Report.DataDefinition.FormulaFields("txtFEmision").Text = "'" & Textos(27) & "'"
        Report.DataDefinition.FormulaFields("txtImporte").Text = "'" & Textos(67) & "'"
        Report.DataDefinition.FormulaFields("txtEstadoActual").Text = "'" & Textos(28) & "'"
        Report.DataDefinition.FormulaFields("txtArchivosAdjuntos").Text = "'" & Textos(117) & "'"
        Report.DataDefinition.FormulaFields("txtObservaciones").Text = "'" & Textos(90) & "'"
        Report.DataDefinition.FormulaFields("txtAprovisionador").Text = "'" & Textos(123) & "'"
        Report.DataDefinition.FormulaFields("txtTfno").Text = "'" & Textos(124) & "'"
        Report.DataDefinition.FormulaFields("txtEmail").Text = "'" & Textos(125) & "'"
        Report.DataDefinition.FormulaFields("txtReceptor").Text = "'" & Textos(97) & "'"
        Report.DataDefinition.FormulaFields("DENABONO").Text = "'" & Textos(176) & "'"
        With Report.DataDefinition
            .FormulaFields("txtCodArtComp").Text = "'" & Textos(126) & "'"
            .FormulaFields("txtCodArtProv").Text = "'" & Textos(127) & "'"
            .FormulaFields("txtDenominacion").Text = "'" & Textos(42) & "'"
            .FormulaFields("txtDestino").Text = "'" & Textos(64) & "'"
            .FormulaFields("txtUP").Text = "'" & Textos(63) & "'"
            .FormulaFields("txtFEntrega").Text = "'" & Textos(128) & "'"
            .FormulaFields("txtObl").Text = "'" & Textos(129) & "'"
            .FormulaFields("txtCantidad").Text = "'" & Textos(66) & "'"
            .FormulaFields("txtPU").Text = "'" & Textos(65) & "'"
            .FormulaFields("txtImporteMon").Text = "'" & Textos(67) & "'"
            .FormulaFields("txtCantidadRecibida").Text = "'" & Textos(130) & "'"
            .FormulaFields("txtProceso").Text = "'" & Textos(91) & "'"
            .FormulaFields("txtObservaciones").Text = "'" & Textos(90) & "'"
            .FormulaFields("txtArchivosAdjuntos").Text = "'" & Textos(117) & "'"
            If bMostrarDireccion Then
                .FormulaFields("txtDireccionEnvioFra").Text = "'" & Textos(183) & "'"
            End If
        End With

    End Sub


    ''' <summary>
    ''' Método para pasar al report los textos de estado de la orden
    ''' </summary>
    ''' <remarks>Llamada desde Page_Init. tiempo maximo inferior 1 seg</remarks>
    Private Sub PonerEstadoOrdenEnReport()
        Dim sEstados As String = ""

        'CPedido.Estado.SinEstado -> 0
        sEstados = "'" & " "
        'CPedido.Estado.PendienteAprobar -> 1
        sEstados = sEstados & "@" & Textos(31)
        'CPedido.Estado.DenegadoParcialmente -> 2
        sEstados = sEstados & "@" & Textos(32)
        'CPedido.Estado.EmitidoAlProveedor -> 4
        sEstados = sEstados & "@" & Textos(33)
        'CPedido.Estado.AceptadoPorElProveedor -> 8
        sEstados = sEstados & "@" & Textos(34)
        'CPedido.Estado.EnCamino -> 16
        sEstados = sEstados & "@" & Textos(35)
        'CPedido.Estado.RecibidoParcialmente -> 32
        sEstados = sEstados & "@" & Textos(36)
        'CPedido.Estado.RecibidoEnSuTotalidad -> 64
        sEstados = sEstados & "@" & Textos(37)
        'CPedido.Estado.Anulado -> 128
        sEstados = sEstados & "@" & Textos(38)
        'CPedido.Estado.Rechazado -> 256
        sEstados = sEstados & "@" & Textos(39)
        'CPedido.Estado.Denegado -> 512
        sEstados = sEstados & "@" & Textos(40)
        ' Else
        sEstados = sEstados & "@" & " " & "'"

        Report.DataDefinition.FormulaFields("txtESTADOS").Text = sEstados

    End Sub
End Class