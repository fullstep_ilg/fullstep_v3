<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/CabCatalogo.master" CodeBehind="PedidoLibre.aspx.vb" Inherits="Fullstep.FSNWeb.PedidoLibre" MaintainScrollPositionOnPostback="true" %>
<%@ MasterType VirtualPath="~/App_Master/CabCatalogo.master"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Ppl" runat="server">
        <input id="btnOcultoLin" type="button" value="button" runat="server" style="display: none" />
        <asp:Panel ID="pnlLin" runat="server" BackColor="White" BorderColor="DimGray"
            BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left" Style="display: none;
            min-width: 450px; padding: 10px" DefaultButton="btnAnyadir">
            <table width="700px">
                <tr>
                    <td align="right" style="height: 20px;" valign="top">
                        <asp:ImageButton ID="ImgBtnCerrarpnlLin" runat="server" ImageAlign="Right" ImageUrl="~/images/Bt_Cerrar.png" CausesValidation="False" />
                    </td>
                </tr>
            </table>
            <asp:UpdatePanel ID="updpnlPopupLineas" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Panel Info Proveedor -->
                <fsn:FSNPanelInfo ID="FSNPanelDatosProve" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx" ServiceMethod="Proveedor_DatosContacto" Contenedor="pnlLin" TipoDetalle="1" />                  
                <table width="700px">
                    <tr>
                        <td>   
                            <asp:Label ID="lblCategoria" runat="server" Text="DCategor�a" CssClass="Rotulo"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblProve" runat="server" Text="DCodProveedor - DenProveedor" CssClass="Rotulo"></asp:Label>&nbsp;
                            <asp:Label ID="lblContacto" runat="server" Text="DContacto:" CssClass="Rotulo"></asp:Label>&nbsp;
                            <fsn:FSNLinkInfo ID="fsnlnkContacto" runat="server" PanelInfo="FSNPanelDatosProve" CssClass="Rotulo" style="text-decoration:none;" CausesValidation="false" Font-Underline="True"></fsn:FSNLinkInfo>
                        </td>
                    </tr>
                   <tr><td><br /></td></tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>               
                            <table width="75%">
                                <tr>
                                    <td colspan="5">
                                        <asp:Label ID="lblDescripcion" runat="server" Text="DDescripci�n:" CssClass="Etiqueta"></asp:Label>
                                    </td>
                                </tr>                        
                                <tr><td colspan="5">
                                    <asp:TextBox ID="txtDescripcion" runat="server" 
                                        Text="" Width="100%" ></asp:TextBox>        
                                    <ajx:TextBoxWatermarkExtender ID="txtDescripcion_TextBoxWatermarkExtender" runat="server"  
                                        TargetControlID="txtDescripcion">
                                    </ajx:TextBoxWatermarkExtender>                                    
                                    </td>                            
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <asp:RequiredFieldValidator ID="reqValDescr" runat="server" 
                                        ErrorMessage="Descripci�n obligatoria" 
                                        ControlToValidate="txtDescripcion" 
                                        Display="Static" EnableClientScript="False"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                <td style="width: 20%">
                                <asp:Label ID="lblCantidad" runat="server" Text="DCantidad" CssClass="Etiqueta"></asp:Label>
                                    :</td>
                                    <td style="width: 35%">
                                        <asp:Label ID="lblUnidad" runat="server" Text="DUnidad" CssClass="Etiqueta"></asp:Label>
                                        :</td>
                                    <td style="width: 30%" colspan = "2">
                                        <asp:Label ID="lblPrecio" runat="server" Text="Dprecio por unidad:" CssClass="Etiqueta"></asp:Label>
                                        :</td>
                                    <td style="width: 15%">
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td>
                                        <igpck:WebNumericEditor ID="WNumEdCantidad" runat="server" Nullable="false" Width="80px" NullText=""></igpck:WebNumericEditor>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="lstUnidades" runat="server" DataTextField="CODYDEN" DataValueField="COD" Width="200px" AutoPostBack="true"></asp:DropDownList>
                                    </td>
                                    <td>
                                        <igpck:WebNumericEditor ID="WNumEdPrecio" runat="server" Nullable="True" MinValue="0"></igpck:WebNumericEditor>
                                    </td>
                                    <td >
                                        <asp:Label ID="lblMon" runat="server" Text="DMON"></asp:Label>
                                    </td>
                                    <td>
                                        <fsn:FSNButton ID="btnAnyadir" runat="server" Text="A�adir"></fsn:FSNButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblLineasPedido" runat="server" Text="DLineas de pedido" CssClass="Rotulo"></asp:Label>
                            <hr style="color:#AB0020;height:1px;" />
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                            <asp:GridView ID="grdLineas" runat="server" 
                                CellPadding="1" CellSpacing="1" GridLines="None" 
                                ShowHeader="False" Width="100%" AutoGenerateColumns="False">
                                <AlternatingRowStyle BorderStyle="None" BackColor="#F8F8F8" />
                                <Columns>
                                <asp:BoundField DataField="Descr" ItemStyle-Width="60%" />
                                <asp:BoundField DataField="Cant" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="Unidad" ItemStyle-Width="10%" />
                                <asp:BoundField DataField="PrecioUni" Visible="false" />
                                <asp:BoundField DataField="Precio" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="Mon" ItemStyle-Width="5%" />
                                <asp:TemplateField ItemStyle-Width="10%">
                                <ItemTemplate>
                                <fsn:FSNButton ID="btnEliminar" runat="server" Text="DEliminar" CommandName="EliminarFila" CausesValidation="False"></fsn:FSNButton>
                                </ItemTemplate>
                                </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                   </tr>
                   <tr><td><br /></td></tr>
                   <tr>
                    <td>
                        <fsn:FSNButton ID="btnAnyadirACesta" runat="server" Text="DA�adir a Cesta" 
                            CausesValidation="False"></fsn:FSNButton>
                    </td>
                   </tr>
                </table>
            </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <ajx:ModalPopupExtender ID="mpeLin" runat="server" BackgroundCssClass="modalBackground"
            CancelControlID="ImgBtnCerrarpnlLin" PopupControlID="pnlLin" TargetControlID="btnOcultoLin">
        </ajx:ModalPopupExtender>


        <asp:UpdatePanel ID="updpnlProveedores" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="panProveedores" runat="server" CssClass="ColorFondoTab">
                <asp:Label ID="lblProveedor" runat="server" Text="Label" CssClass="EtiquetaGrande" Font-Size="Medium"></asp:Label>
                <br />
                <asp:DataList ID="dlCategorias" runat="server" ShowFooter="False" ShowHeader="False" Width = "100%">
                    <ItemTemplate>
                        <br />
                        <hr style="height:1px;" />
                        <asp:Label ID="lblCat" runat="server" 
                            Width="100%" Text='<%# Eval("COD") & " - " & Eval("DEN") %>' 
                            CssClass="Rotulo" Font-Size="14px"></asp:Label>
                        <hr style="height:1px;" />    
                        <asp:DataList ID="dlProveedores" runat="server" CellPadding="5" CellSpacing="5" Width="100%"
                            datasource='<%# Container.DataItem.Row.GetChildRows("myrelation") %>' 
                            RepeatColumns="3" RepeatDirection="Horizontal" ShowFooter="False" 
                            ShowHeader="False" OnItemCommand="dlProveedores_OnItemCommand" >
                            <ItemTemplate>  
                                 <img alt="" src="../../images/flecha.gif" runat=server style="width: 10px; height: 10px" />
                                 <asp:LinkButton ID="lnkProve" runat="server" CommandName="MostrarPanelLineas"  
                                     Text='<%# Container.DataItem("PROVECOD") & " - " & Container.DataItem("PROVEDEN") %>' 
                                     CommandArgument='<%# Container.DataItem("COD") & "###" & Container.DataItem("DEN")& "###" & Container.DataItem("PROVECOD") & "###" & Container.DataItem("PROVEDEN") & "###" & Container.DataItem("CODMON") & "###" & Container.DataItem("ID_CAT1") & "###" & Container.DataItem("ID_CAT2") & "###" & Container.DataItem("ID_CAT3") & "###" & Container.DataItem("ID_CAT4") & "###" & Container.DataItem("ID_CAT5") %>' CssClass="Etiqueta" CausesValidation="false"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:DataList>
                    </ItemTemplate>
                </asp:DataList>
                <br />
                &nbsp;
            </asp:Panel>
        </ContentTemplate>
        </asp:UpdatePanel>

</asp:Content>
