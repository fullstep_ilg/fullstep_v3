<%@ Page Title="" Language="vb" AutoEventWireup="false" EnableViewState="true" MaintainScrollPositionOnPostback="false"
    MasterPageFile="~/App_Master/CabCatalogo.master" CodeBehind="Comparativa.aspx.vb"
    Inherits="Fullstep.FSNWeb.Comparativa" %>

<%@ MasterType VirtualPath="~/App_Master/CabCatalogo.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Ppl" runat="server" EnableViewState="true">
    <fsep:DetArticuloCatalogo ID="pnlDetalleArticulo" runat="server" />
    <!-- Panel Tooltip -->
    <fsn:FSNPanelTooltip ID="pnlTooltipTexto" runat="server" ServiceMethod="DevolverTexto">
    </fsn:FSNPanelTooltip>
    <asp:Panel ID="PanelCabecera" runat="server" Width="100%">
        <table width="100%">
            <tr>
                <td style="width: 10%">
                    <fsn:FSNButton ID="BtnVaciarComparativa" runat="server" Alineacion="Left" CssClass="Boton"
                        Text="Vaciar Comparativa">
                    </fsn:FSNButton>
                </td>
                <td style="width: 20%">
                    <fsn:FSNButton ID="BtnRecargarComparativa" runat="server" Alineacion="Left" CssClass="Boton"
                        Text="DRecargar Comparativa">
                    </fsn:FSNButton>
                </td>
                <td style="width: 50%">
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:UpdatePanel ID="upTabla" runat="server" UpdateMode="Conditional" EnableViewState="true">
        <ContentTemplate>
            <table runat="server" id="tCabComparativa" enableviewstate="true" style="background-color: White"
                width="100%">
                <tr id="fCabComparativa" runat="server" class="Etiqueta" enableviewstate="true" style="background-color: #FAFAFA">
                    <td style="margin-left: 8px; margin-right: 8px; margin-top: 3px; margin-bottom: 3px">
                        <br />
                        <br />
                        <asp:Label ID="LblElimCab" runat="server">
                        </asp:Label>
                    </td>
                    <td style="margin-left: 8px; margin-right: 8px; margin-top: 3px; margin-bottom: 3px;
                        max-width: 30px" align="center" runat="server" id="CabeceraUnidad" colspan="2">
                        <br />
                        <br />
                        <asp:Label ID="lblCantMinCab" runat="server">
                        </asp:Label>
                    </td>
                    <td style="margin-left: 8px; margin-right: 8px; margin-top: 3px; margin-bottom: 3px"
                        align="center">
                        <br />
                        <br />
                        <asp:Label ID="LblCestaCab" runat="server">
                        </asp:Label>
                    </td>
                    <td style="margin-left: 8px; margin-right: 8px; margin-top: 3px; margin-bottom: 3px"
                        align="center">
                        <br />
                        <br />
                        <asp:Label ID="LblPrecCab" runat="server">
                        </asp:Label>
                    </td>
                    <td style="margin-left: 8px; margin-right: 8px; margin-top: 3px; margin-bottom: 3px"
                        align="center">
                        <br />
                        <br />
                        <asp:Label ID="LblMonCab" runat="server">
                        </asp:Label>
                    </td>
                    <td style="margin-left: 8px; margin-right: 8px; margin-top: 3px; margin-bottom: 3px"
                        align="center">
                        <br />
                        <br />
                        <asp:Label ID="LblNomCab" runat="server">
                        </asp:Label>
                    </td>
                    <td runat="server" id="CelCabAtrib1" visible="false" style="margin-left: 8px; margin-right: 8px;
                        margin-top: 3px; margin-bottom: 3px" align="right">
                        <asp:ImageButton ID="ImgBtnElimCol1" runat="server" ImageAlign="Top" Style="display: block;
                            vertical-align: top" SkinID="EliminarColumna" />
                        <br />
                        <asp:Label ID="LblCabAtrib1" runat="server" Style="text-align: center; vertical-align: bottom;
                            display: block">
                        </asp:Label>
                    </td>
                    <td runat="server" id="CelCabAtrib2" visible="false" style="margin-left: 8px; margin-right: 8px;
                        margin-top: 3px; margin-bottom: 3px" align="right">
                        <asp:ImageButton ID="ImgBtnElimCol2" runat="server" ImageAlign="Top" Style="display: block;
                            vertical-align: top" SkinID="EliminarColumna" />
                        <br />
                        <asp:Label ID="LblCabAtrib2" runat="server" Style="text-align: center; vertical-align: bottom;
                            display: block">
                        </asp:Label>
                    </td>
                    <td runat="server" id="CelCabAtrib3" visible="false" style="margin-left: 8px; margin-right: 8px;
                        margin-top: 3px; margin-bottom: 3px" align="right">
                        <asp:ImageButton ID="ImgBtnElimCol3" runat="server" ImageAlign="Top" Style="display: block;
                            vertical-align: top" SkinID="EliminarColumna" />
                        <br />
                        <asp:Label ID="LblCabAtrib3" runat="server" Style="text-align: center; vertical-align: bottom;
                            display: block">
                        </asp:Label>
                    </td>
                    <td runat="server" id="CelCabAtrib4" visible="false" style="margin-left: 8px; margin-right: 8px;
                        margin-top: 3px; margin-bottom: 3px" align="right">
                        <asp:ImageButton ID="ImgBtnElimCol4" runat="server" ImageAlign="Top" Style="vertical-align: top;
                            display: block" SkinID="EliminarColumna" />
                        <br />
                        <asp:Label ID="LblCabAtrib4" runat="server" Style="text-align: center; vertical-align: bottom;
                            display: block">
                        </asp:Label>
                    </td>
                    <td runat="server" id="CelCabAtrib5" visible="false" style="margin-left: 8px; margin-right: 8px;
                        margin-top: 3px; margin-bottom: 3px" align="right">
                        <asp:ImageButton ID="ImgBtnElimCol5" runat="server" ImageAlign="Top" Style="vertical-align: top;
                            display: block" SkinID="EliminarColumna" />
                        <br />
                        <asp:Label ID="LblCabAtrib5" runat="server" Style="text-align: center; vertical-align: bottom;
                            display: block">
                        </asp:Label>
                    </td>
                    <td runat="server" id="CelCabAtrib6" visible="false" style="margin-left: 8px; margin-right: 8px;
                        margin-top: 3px; margin-bottom: 3px" align="right">
                        <asp:ImageButton ID="ImgBtnElimCol6" runat="server" ImageAlign="Top" Style="vertical-align: top;
                            display: block" SkinID="EliminarColumna" />
                        <br />
                        <asp:Label ID="LblCabAtrib6" runat="server" Style="text-align: center; display: block;
                            vertical-align: bottom">
                        </asp:Label>
                    </td>
                    <td runat="server" id="CelCabAtrib7" visible="false" style="margin-left: 8px; margin-right: 8px;
                        margin-top: 3px; margin-bottom: 3px" align="right">
                        <asp:ImageButton ID="ImgBtnElimCol7" runat="server" ImageAlign="Top" Style="vertical-align: top;
                            display: block" SkinID="EliminarColumna" />
                        <br />
                        <asp:Label ID="LblCabAtrib7" runat="server" Style="text-align: center; display: block;
                            vertical-align: bottom">
                        </asp:Label>
                    </td>
                    <td runat="server" id="CelCabAtrib8" visible="false" style="margin-left: 8px; margin-right: 8px;
                        margin-top: 3px; margin-bottom: 3px" align="right">
                        <asp:ImageButton ID="ImgBtnElimCol8" runat="server" ImageAlign="Top" Style="vertical-align: top;
                            display: block" SkinID="EliminarColumna" />
                        <br />
                        <asp:Label ID="LblCabAtrib8" runat="server" Style="text-align: center; display: block;
                            vertical-align: bottom">
                        </asp:Label>
                    </td>
                    <td runat="server" id="CelCabAtrib9" visible="false" style="margin-left: 8px; margin-right: 8px;
                        margin-top: 3px; margin-bottom: 3px" align="right">
                        <asp:ImageButton ID="ImgBtnElimCol9" runat="server" ImageAlign="Top" Style="vertical-align: top;
                            display: block" SkinID="EliminarColumna" />
                        <br />
                        <asp:Label ID="LblCabAtrib9" runat="server" Style="text-align: center; display: block;
                            vertical-align: bottom">
                        </asp:Label>
                    </td>
                    <td runat="server" id="CelCabAtrib10" visible="false" style="margin-left: 8px; margin-right: 8px;
                        margin-top: 3px; margin-bottom: 3px" align="right">
                        <asp:ImageButton ID="ImgBtnElimCol10" runat="server" ImageAlign="Top" Style="vertical-align: top;
                            display: block" SkinID="EliminarColumna" />
                        <br />
                        <asp:Label ID="LblCabAtrib10" runat="server" Style="text-align: center; display: block;
                            vertical-align: bottom">
                        </asp:Label>
                    </td>
                    <td runat="server" id="CelCabAtrib11" visible="false" style="margin-left: 8px; margin-right: 8px;
                        margin-top: 3px; margin-bottom: 3px" align="right">
                        <asp:ImageButton ID="ImgBtnElimCol11" runat="server" ImageAlign="Top" Style="vertical-align: top;
                            display: block" SkinID="EliminarColumna" />
                        <br />
                        <asp:Label ID="LblCabAtrib11" runat="server" Style="text-align: center; display: block;
                            vertical-align: bottom">
                        </asp:Label>
                    </td>
                    <td runat="server" id="CelCabAtrib12" visible="false" style="margin-left: 8px; margin-right: 8px;
                        margin-top: 3px; margin-bottom: 3px" align="right">
                        <asp:ImageButton ID="ImgBtnElimcol12" runat="server" ImageAlign="Top" Style="vertical-align: top;
                            display: block" SkinID="EliminarColumna" />
                        <br />
                        <asp:Label ID="LblCabAtrib12" runat="server" Style="text-align: center; display: block;
                            vertical-align: bottom">
                        </asp:Label>
                    </td>
                    <td runat="server" id="CelCabAtrib13" visible="false" style="margin-left: 8px; margin-right: 8px;
                        margin-top: 3px; margin-bottom: 3px" align="right">
                        <asp:ImageButton ID="ImgBtnElimCol13" runat="server" ImageAlign="Top" Style="vertical-align: top;
                            display: block" SkinID="EliminarColumna" />
                        <br />
                        <asp:Label ID="LblCabAtrib13" runat="server" Style="text-align: center; vertical-align: top">
                        </asp:Label>
                    </td>
                    <td runat="server" id="CelCabAtrib14" visible="false" style="margin-left: 8px; margin-right: 8px;
                        margin-top: 3px; margin-bottom: 3px" align="right">
                        <asp:ImageButton ID="ImgBtnElimCol14" runat="server" ImageAlign="Top" Style="vertical-align: top;
                            display: block" SkinID="EliminarColumna" />
                        <br />
                        <asp:Label ID="LblCabAtrib14" runat="server" Style="text-align: center; display: block;
                            vertical-align: bottom">
                        </asp:Label>
                    </td>
                    <td runat="server" id="CelCabAtrib15" visible="false" style="margin-left: 8px; margin-right: 8px;
                        margin-top: 3px; margin-bottom: 3px" align="right">
                        <asp:ImageButton ID="ImgBtnElimCol15" runat="server" ImageAlign="Top" Style="vertical-align: top;
                            display: block" SkinID="EliminarColumna" />
                        <br />
                        <asp:Label ID="LblCabAtrib15" runat="server" Style="text-align: center; display: block;
                            vertical-align: bottom">
                        </asp:Label>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
