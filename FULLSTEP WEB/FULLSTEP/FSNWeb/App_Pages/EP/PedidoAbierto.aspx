﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/CabCatalogo.master" CodeBehind="PedidoAbierto.aspx.vb" Inherits="Fullstep.FSNWeb.PedidoAbierto" %>
<%@ MasterType VirtualPath="~/App_Master/CabCatalogo.master"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        #pnlFiltrosAvanzados>li { margin:0.5em 0em; }        
        #lstLineasPedidoAbierto>li:nth-child(even){ background-color:#efefef; }
        #lstLineasPedidoAbierto>li:nth-child(odd){ background-color:#f7f7f7; }
        ul.token-input-list{ width:100%!important; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Ppl" runat="server">
    <link rel="stylesheet" href="<%=ResolveClientUrl("~/js/jquery/plugins/styles/token-input.css")%>" type="text/css" />
    <link rel="stylesheet" href="<%=ResolveClientUrl("~/js/jquery/plugins/styles/token-input-facebook.css")%>" type="text/css" />
    <link rel="stylesheet" href="<%=ResolveClientUrl("~/js/jquery/plugins/styles/style.min.css")%>" type="text/css" />
    <div class="ColorFondoTab" style="position:relative; padding:0.5em;">    
        <asp:UpdatePanel ID="updFiltros" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
            <ContentTemplate>
                <div id="lnkFiltrosAvanzados" style="position:absolute; top:0.5em; right:1em; cursor:pointer;">
                    <asp:Image ID="imgExpandFiltrosAvanzados" runat="server" SkinID="ExpandirFondoTrans" style="display:none;" />
                    <asp:Image ID="imgCollapseFiltrosAvanzados" runat="server" SkinID="ContraerFondoTrans" />
                    <span id="lblFiltrosAvanzados" class="EtiquetaPeque">DFiltros avanzados</span>      
                </div>
                <ul id="pnlFiltrosAvanzados" style="margin:0.5em; padding:0em; list-style:none; min-height:1em;">
                    <li>
                        <span id="lblBscPedidoAbierto" class="Rotulo" style="display:inline-block; width:11em; margin-right:0.1em;"></span>
                        <select id="cboBscAnioPedidoAbierto" style="width:5em;"></select>
                        <input type="text" id="txtBscNumPedidoPedidoAbierto" style="width:7em;"></select>
                        <input type="text" id="txtBscNumOrdenPedidoAbierto" style="width:7em;"></select>
                        <span id="lblBscEmpresa" class="Rotulo" style="display:inline-block; width:9em; margin-left:2.5em;"></span>
                        <select id="cboBscEmpresa" style="width:20.5em;"></select>
                    </li>                    
                    <li>
                        <span id="lblBscFecInicioDesde" class="Rotulo" style="display:inline-block; width:11em; white-space:nowrap;"></span>
                        <label class="CajaTexto" style="padding:0.3em 0em; background-color:#fff;">
                            <input type="text" id="txtBscFecInicioDesde" style="width:6em; border:solid 0px Transparent;"/>
                        </label>
                        <span id="lblBscFecInicioHasta" class="Rotulo" style="display:inline-block; margin-left:2em;"></span>
                        <label class="CajaTexto" style="padding:0.3em 0em; background-color:#fff;">
                            <input type="text" id="txtBscFecInicioHasta" style="width:6em; border:solid 0px Transparent;"/>
                        </label>
                        <span id="lblBscFecFinDesde" class="Rotulo" style="display:inline-block; margin-left:2.7em; width:9em;"></span>
                        <label class="CajaTexto" style="padding:0.3em 0em; background-color:#fff;">
                            <input type="text" id="txtBscFecFinDesde" style="width:6em; border:solid 0px Transparent;"/>
                        </label>
                        <span id="lblBscFecFinHasta" class="Rotulo" style="display:inline-block; margin-left:2.5em;"></span>
                        <label class="CajaTexto" style="padding:0.3em 0em; background-color:#fff;">
                            <input type="text" id="txtBscFecFinHasta" style="width:6em; border:solid 0px Transparent;"/>
                        </label>
                    </li>
                    <li>
                        <span id="lblBscArticulo" class="Rotulo" style="display:inline-block; width:11em;"></span>
                        <div style="display:inline-block; width:57em; vertical-align:middle;">
                            <input type="text" id="txtBscArticulo" style="width:100%;" />
                        </div>                        
                    </li>
                    <li style="margin-top:1em;">
                        <div style="display:inline-block; vertical-align:top;">
                            <span id="lblBscProveedores" class="Rotulo" style="display:inline-block; width:11em;"></span>
                            <asp:Image ID="imgBscExpandProveedores" runat="server" SkinID="ExpandirFondoTrans" style="display:none; cursor:pointer;"/>
                            <asp:Image ID="imgBscCollapseProveedores" runat="server" SkinID="ContraerFondoTrans" style="cursor:pointer;" /> 
                        </div> 
                        <ul id="lstBscProveedores" style="display:inline-block; margin:0em; padding:0em;"></ul>
                        <a onclick="MostrarTodosProveedores()" class="Rotulo clickable" style="display:none; margin-left:11em; text-decoration:underline;"></a>                        
                    </li>
                </ul>    
                <div id="divFiltrosAplicados" style="border-top:solid 0.1em #000; border-bottom:solid 0.1em #000; padding:0.5em 0em;">
                    <span id="lblBscFiltros" class="Rotulo" style="margin:0.5em;"></span>
                    <span id="faAnio"><span></span><span style="margin-left:0.5em;"></span><a class="clickable" style="text-decoration:none; margin-left:0.5em;">(x)</a></span>
                    <span id="faNumPedido" style="display:none;"><span></span><span style="margin-left:0.5em;"></span><a class="clickable" style="text-decoration:none; margin-left:0.5em;">(x)</a></span>
                    <span id="faNumOrden" style="display:none;"><span></span><span style="margin-left:0.5em;"></span><a class="clickable" style="text-decoration:none; margin-left:0.5em;">(x)</a></span>
                    <span id="faEmpresa" style="display:none;"><span style="margin-left:0.5em;"></span><a class="clickable" style="text-decoration:none; margin-left:0.5em;">(x)</a></span>
                    <span id="faArticulo" style="display:none;"><span style="margin-left:0.5em;"></span><a class="clickable" style="text-decoration:none; margin-left:0.5em;">(x)</a></span>
                    <span id="faFecInicio" style="display:none;">
                        <span></span>
                        <div style="display:inline-block;"><span style="display:none;"></span><a class="clickable" style="text-decoration:none; margin-left:0.5em;">(x)</a></div>
                        <div style="display:inline-block;"><span style="display:none;"></span><a class="clickable" style="text-decoration:none; margin-left:0.5em;">(x)</a></div>
                    </span>
                    <span id="faFecFin" style="display:none;">
                        <span></span>
                        <div style="display:inline-block;"><span style="display:none;"></span><a class="clickable" style="text-decoration:none; margin-left:0.5em;">(x)</a></div>
                        <div style="display:inline-block;"><span style="display:none;"></span><a class="clickable" style="text-decoration:none; margin-left:0.5em;">(x)</a></div>
                    </span>
                    <span id="faProveedor"></span>
                    <span id="faBorrarFiltros"><a class="Normal clickable" style="margin-left:0.5em; text-decoration:underline;"></a></span>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Button runat="server" ID="btnFiltrarPedAbierto" style="display:none;" />
        <asp:UpdatePanel ID="upGrid" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnFiltrarPedAbierto" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <ig:WebHierarchicalDataGrid id="whdgPedidosAbiertos" runat="server" Width="100%" 
                    DataKeyFields="ORDENENTREGA"
                    AutoGenerateBands="false"
                    AutoGenerateColumns="false" 
                    EnableAjax="false" 
                    EnableAjaxViewState="false" 
                    EnableDataViewState="true">
                    <ClientEvents Initialize="whdgPedidosAbiertos_Initialize" />
                    <Behaviors>
                        <ig:Selection RowSelectType="Single" Enabled="True" CellClickAction="Row" SelectionClientEvents-RowSelectionChanged="whdgPedidosAbiertos_RowSelectionChanged"></ig:Selection> 
                        <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true" AnimationEnabled="true"></ig:Filtering> 
                        <ig:RowSelectors Enabled="true" RowNumbering="false"></ig:RowSelectors>
                        <ig:Sorting Enabled="true"></ig:Sorting>
                        <ig:Paging Enabled="true" PagerAppearance="Top">
                            <PagingClientEvents PageIndexChanged="whdgPedidosAbiertos_PageIndexChanged" />
                            <PagerTemplate>
							    <div style="position:relative; text-align:left; margin:0.1em 0em;">
                                    <asp:Label runat="server" ID="lblSeleccionPedidoAbierto" CssClass="Rotulo" style="margin:0.5em; display:inline-block;"></asp:Label> 
							        <div style="position:absolute; padding:3px 0px 2px 0px; top:0em; right:1em;">								        
                                        <asp:Image ID="ImgBtnFirst" runat="server" style="margin-right:2px; vertical-align:middle;" />
                                        <asp:Image ID="ImgBtnPrev" runat="server" style="vertical-align:middle;"/>
                                        <asp:Label ID="lblPage" runat="server" Text="dPage" Style="margin:0em 1em;"></asp:Label>
                                        <asp:DropDownList ID="PagerPageList" runat="server" onchange="return IndexChanged()" Style="margin:0em 1em;"></asp:DropDownList>
                                        <asp:Label ID="lblOF" runat="server" Text="dOf" Style="margin:0em 1em;"></asp:Label>
                                        <asp:Label ID="lblCount" runat="server"></asp:Label>                                      
                                        <asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-right:2px; vertical-align:middle;" />
                                        <asp:Image ID="ImgBtnLast" runat="server" style="vertical-align:middle;" />
							        </div>   
                                </div>  
                            </PagerTemplate>
                        </ig:Paging>
                        <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                    </Behaviors>
                    <Columns>
                        <ig:BoundDataField Key="ORDENENTREGA" DataFieldName="ORDENENTREGA" Hidden="true"></ig:BoundDataField>
                        <ig:BoundDataField Key="PEDIDOABIERTO" DataFieldName="PEDIDOABIERTO" CssClass="itemSeleccionable SinSalto" Width="120px"></ig:BoundDataField>
                        <ig:BoundDataField Key="EMPRESA" DataFieldName="EMPRESA" Header-Text="DEmpresa" CssClass="itemSeleccionable SinSalto"></ig:BoundDataField>
                        <ig:BoundDataField Key="CODPROVE" DataFieldName="CODPROVE" Header-Text="DCódigo proveedor" CssClass="itemSeleccionable SinSalto" Width="150px"></ig:BoundDataField>
                        <ig:BoundDataField Key="DENPROVE" DataFieldName="DENPROVE" Header-Text="DDenominación proveedor" CssClass="itemSeleccionable SinSalto"></ig:BoundDataField>
                        <ig:BoundDataField Key="NUMPEDERP" DataFieldName="NUMPEDERP" Header-Text="DNº ped. ERP" CssClass="itemSeleccionable SinSalto" Width="150px"></ig:BoundDataField>
                        <ig:BoundDataField Key="FECINI" DataFieldName="FECINI" Header-Text="DFec. inicio" CssClass="itemSeleccionable SinSalto" Width="90px"></ig:BoundDataField>
                        <ig:BoundDataField Key="FECFIN" DataFieldName="FECFIN" Header-Text="DFec. fin" CssClass="itemSeleccionable SinSalto" Width="90px"></ig:BoundDataField>
                        <ig:BoundDataField Key="TIPO" DataFieldName="TIPO" Header-Text="DTipo" CssClass="itemSeleccionable SinSalto" Width="100px"></ig:BoundDataField>
                    </Columns>
                </ig:WebHierarchicalDataGrid>
            </ContentTemplate>
        </asp:UpdatePanel>
        <ul id="lstLineasPedidoAbierto" style="list-style:none;"></ul>            
    </div>
    <script id="tmplItemBscProveedor" type="text/x-jquery-tmpl">
        <li style='display:inline-block; cursor:pointer; width:33%; white-space:nowrap;'>
            <div style='display:none;'>
                <span style='display:none;'>${codProve}</span>
                <a style='display:inline-block; width:8em; overflow:hidden; text-overflow:ellipsis; white-space:nowrap;'>${nifProve}</a>
                <a style='display:inline-block; width:18em; overflow:hidden; text-overflow:ellipsis; white-space:nowrap;'>${denProve}</a>
                <a style='display:inline-block; width:2em; overflow:hidden; text-overflow:ellipsis; white-space:nowrap;'>(${numPedidosAbiertos})</a>
            </div>
        </li>
    </script>
    <script id="tmplItemProveedor" type="text/x-jquery-tmpl">
        <span>
            <span style='display:none'>${codProve}</span>
            <span>${denProve}</span>
            <a style="text-decoration:none; cursor:pointer;">(x)</a>
        </span>
    </script>
    <script id="tmplLineaPedidoAbierto" type="text/x-jquery-tmpl">
        <li style='padding:0.2em 0em;'>
            <div>
                <span class='EtiquetaGrande'>${numLineaPedidoAbierto}</span>
                <span class='EtiquetaGrande' style='margin:0em 0.1em;'>-</span>                
                {{if codArticulo ==''}}
                    <span class='RotuloGrande' style='text-decoration:underline;'>${denArticulo}</span>
                {{else}}
                    <span class='RotuloGrande' style='text-decoration:underline;'>${codArticulo} - ${denArticulo}</span>
                {{/if}}
            </div>
            <div style='display:inline-block; width:70%; padding:0.2em 0em; vertical-align:top;'>
                <ul style='list-style:none; padding:0em; margin:0em;'>
                    <li style='padding:0.2em 0em;'>
                        <span class='LinkButtonInfo' style='display:inline-block; width:10em;'>{{if tipoPedidoAbierto==2}}${TextosPantalla[20]}{{else}}${TextosPantalla[22]}{{/if}}:</span>
                        <span style='display:inline-block; width:9em; text-align:right;'>${abierto} {{if tipoPedidoAbierto==2}}${moneda}{{else}}${unidad}{{/if}}</span>
                        {{if tipoPedidoAbierto==3}}
                        <span class='LinkButtonInfo' style='margin-left:5em;'>${TextosPantalla[24]}:</span>
                        <span>${precioUnidad} ${moneda}/${unidad}</span>
                        {{/if}}
                    </li>
                    <li style='padding:0.2em 0em;'>
                        <span class='LinkButtonInfo' style='display:inline-block; width:10em;'>{{if tipoPedidoAbierto==2}}${TextosPantalla[21]}{{else}}${TextosPantalla[23]}{{/if}}:</span>
                        <span style='display:inline-block; width:9em; text-align:right;'>${pedido} {{if tipoPedidoAbierto==2}}${moneda}{{else}}${unidad}{{/if}}</span>
                        <span class='LinkButtonInfo TextoResaltado' style='margin-left:5em;{{if parseFloat(enCesta.replace(",","."))==0}}display:none;{{/if}}'>${TextosPantalla[34]}:</span>
                        <span class='TextoResaltado' style='{{if parseFloat(enCesta.replace(",","."))==0}}display:none;{{/if}}'>${enCesta} {{if tipoPedidoAbierto==2}}${moneda}{{else}}${unidad}{{/if}}</span>
                    </li>
                </ul>
            </div>  
            <div style='display:inline-block; text-align:right; width:25%;'>
                <ul style='list-style:none; padding:0em; margin:0em;'>
                    <li style='padding:0.2em 0em;'>
                        <span class='EtiquetaGrande'>${resto}</span>
                        <span style='display:inline-block; width:2.5em; text-align:left; margin-left:0.3em; margin-right:3em;'>{{if tipoPedidoAbierto==2}}${moneda}{{else}}${unidad}{{/if}}</span>
                        <input type='text' style='width:5em; text-align:right;'/>
                        <ul class='spinner'>
                            <li class='up'></li>
                            <li class='down'></li>
                        </ul>
                        <span style='display:inline-block; width:2.5em; text-align:left; margin-left:0.3em;'>{{if tipoPedidoAbierto==2}}${moneda}{{else}}${unidad}{{/if}}</span>
                    </li>
                    <li style='padding:1em 0em;'>
                        <span style='display:none;'>${id}</span>
                        <span class='botonRedondeado'>${TextosPantalla[25]}</span>                        
                    </li>
                </ul>
            </div>          
        </li>
    </script>
</asp:Content>
