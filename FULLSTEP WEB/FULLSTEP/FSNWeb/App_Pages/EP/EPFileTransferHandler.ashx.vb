﻿Imports System.Web
Imports System.Web.Services

Imports System.Collections.Generic
Imports System.Configuration
Imports System.IO
Imports System.Linq
Imports System.Web.Script.Serialization
Imports Fullstep.FSNServer

Public Class EPFileTransferHandler
    Implements System.Web.IHttpHandler, System.Web.SessionState.IReadOnlySessionState

    Private ReadOnly js As New JavaScriptSerializer()
    Public ReadOnly Property TempStorage As String
        Get
            Return ConfigurationManager.AppSettings("temp") & "\"
        End Get
    End Property
    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    Public Sub ProcessRequest(context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.AddHeader("Pragma", "no-cache")
        context.Response.AddHeader("Cache-Control", "private, no-cache")

        HandleMethod(context)
    End Sub
    ' Handle request based on method
    Private Sub HandleMethod(ByVal context As HttpContext)
        Select Case context.Request.HttpMethod
            Case "HEAD", "GET"
                If GivenFilename(context) Then
                    DeliverFile(context)
                Else
                    ListCurrentFiles(context)
                End If
            Case "POST", "PUT"
                Try
                    If context.Request("type") = "del" Then
                        DeleteFile(context)
                    Else
                        UploadFile(context)
                    End If
                Catch ex As Exception

                End Try
            Case "OPTIONS"
                ReturnOptions(context)
                Exit Select
            Case Else
                context.Response.ClearHeaders()
                context.Response.StatusCode = 405
        End Select
    End Sub
    Private Sub ReturnOptions(ByVal context As HttpContext)
        context.Response.AddHeader("Allow", "DELETE,GET,HEAD,POST,PUT,OPTIONS")
        context.Response.StatusCode = 200
    End Sub
    ' Delete file from the server
    Private Sub DeleteFile(ByVal context As HttpContext)
        Dim filePath = TempStorage & context.Request("f")
        If File.Exists(filePath) Then
            File.Delete(filePath)
        End If
    End Sub
    ' Upload file to the server
    Private Sub UploadFile(ByVal context As HttpContext)
        Dim statuses = New List(Of EPFilesStatus)()
        Dim headers = context.Request.Headers

        If String.IsNullOrEmpty(headers("X-File-Name")) Then
            UploadWholeFile(context, statuses)
        Else
            UploadPartialFile(headers("X-File-Name"), context, statuses)
        End If

        WriteJsonIframeSafe(context, statuses)
    End Sub
    ' Upload partial file
    Private Sub UploadPartialFile(ByVal fileName As String, ByVal context As HttpContext, ByVal statuses As List(Of EPFilesStatus))
        If context.Request.Files.Count <> 1 Then
            Throw New HttpRequestValidationException("Attempt to upload chunked file containing more than one fragment per request")
        End If
        Dim inputStream = context.Request.Files(0).InputStream
        Dim fullName = TempStorage & Path.GetFileName(fileName)

        Using fs = New FileStream(fullName, FileMode.Append, FileAccess.Write)
            Dim buffer = New Byte(1023) {}

            Dim l = inputStream.Read(buffer, 0, 1024)
            While l > 0
                fs.Write(buffer, 0, l)
                l = inputStream.Read(buffer, 0, 1024)
            End While
            fs.Flush()
            fs.Close()
        End Using
        statuses.Add(New EPFilesStatus(New FileInfo(fullName)))
    End Sub
    ' Upload entire file
    Private Sub UploadWholeFile(ByVal context As HttpContext, ByVal statuses As List(Of EPFilesStatus))
        For i As Integer = 0 To context.Request.Files.Count - 1
            Dim file = context.Request.Files(i)
            Dim RandomDirectory As String = modUtilidades.GenerateRandomPath()

            While IO.Directory.Exists(TempStorage & "\" & RandomDirectory)
                RandomDirectory = modUtilidades.GenerateRandomPath()
            End While

            IO.Directory.CreateDirectory(TempStorage & "\" & RandomDirectory)

            file.SaveAs(TempStorage & RandomDirectory & "\" & Path.GetFileName(file.FileName))

            Dim fullName As String = Path.GetFileName(file.FileName)
            statuses.Add(New EPFilesStatus(RandomDirectory, fullName, file.ContentType, file.ContentLength))
        Next
    End Sub
    Private Sub WriteJsonIframeSafe(ByVal context As HttpContext, ByVal statuses As List(Of EPFilesStatus))
        context.Response.AddHeader("Vary", "Accept")
        Try
            If context.Request("HTTP_ACCEPT").Contains("application/json") Then
                context.Response.ContentType = "application/json"
            Else
                context.Response.ContentType = "text/plain"
            End If
        Catch
            context.Response.ContentType = "text/plain"
        End Try

        Dim jsonObj = js.Serialize(statuses.ToArray())
        context.Response.Write(jsonObj)
    End Sub
    Private Function GivenFilename(ByVal context As HttpContext) As Boolean
        Return Not String.IsNullOrEmpty(context.Request("f"))
    End Function
    ''' <summary>
    ''' Procedimiento que envía un archivo en forma de buffer al explorador
    ''' </summary>
    ''' <param name="context">HttpContext</param>
    ''' <remarks>Llamada desde: HandleMethod ; Tiempo máximo: 0,2</remarks>
    Private Sub DeliverFile(ByVal context As HttpContext)
        Dim filename As String = context.Request("f")
        Dim filePath As String
        Dim DGuid As String = context.Request("f")
        Dim byteBuffer() As Byte = Nothing
        Dim isEP As Boolean = False
        Dim isNew As Boolean
        If context.Request("isNew") <> Nothing Then
            isNew = CBool(context.Request("isNew"))
        End If
        If context.Request("isEP") <> Nothing Then
            isEP = CBool(context.Request("isEP"))
        End If
        If isNew Then
            If context.Request("t") = 14 Then
                Try
                    Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
                    Dim AdjunDescargar As FSNServer.Adjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
                    AdjunDescargar.Id = context.Request("id")
                    AdjunDescargar.Nombre = context.Request("f")
                    AdjunDescargar.dataSize = context.Request("ds")
                    'AdjunDescargar.EscribirADisco(TempStorage, AdjunDescargar.Nombre, CInt(Int(context.Request("t"))))
                    filePath = TempStorage & AdjunDescargar.Nombre
                    Dim input As System.IO.FileStream = New System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                    Dim buffer(input.Length - 1) As Byte
                    input.Read(buffer, 0, buffer.Length)
                    input.Close()
                    byteBuffer = buffer
                Catch ex As Exception

                End Try
            Else
                Try
                    Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
                    Dim AdjunDescargar As FSNServer.Adjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
                    AdjunDescargar.Id = context.Request("id")
                    AdjunDescargar.Nombre = context.Request("f")
                    AdjunDescargar.dataSize = context.Request("ds")
                    Dim sNombre As String = AdjunDescargar.Nombre
                    'Si  ya esta en disco no se escribe.
                    If AdjunDescargar.Id <> "-1" Then
                        AdjunDescargar.EscribirADisco(TempStorage, sNombre, CInt(Int(context.Request("t"))))
                    End If
                    filePath = TempStorage & sNombre
                    Dim input As System.IO.FileStream = New System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                    Dim buffer(input.Length - 1) As Byte
                    input.Read(buffer, 0, buffer.Length)
                    input.Close()
                    byteBuffer = buffer

                Catch ex As Exception

                End Try
            End If
        Else
            If isEP Then
                Try
                    Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
                    Dim AdjunDescargar As FSNServer.Adjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
                    AdjunDescargar.Id = context.Request("id")
                    AdjunDescargar.Nombre = context.Request("Nombre")
                    AdjunDescargar.dataSize = context.Request("ds")
                    Dim sNombre As String = context.Request("f")
                    AdjunDescargar.EscribirADisco(TempStorage, sNombre, CInt(Int(context.Request("t"))))
                    filePath = TempStorage & sNombre
                    Dim input As System.IO.FileStream = New System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                    Dim buffer(input.Length - 1) As Byte
                    input.Read(buffer, 0, buffer.Length)
                    input.Close()
                    byteBuffer = buffer
                Catch ex As Exception

                End Try
            Else
                Select Case context.Request("t")
                    Case 0, 1, 2
                        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
                        Dim ocnMensajes As FSNServer.cnMensajes
                        ocnMensajes = FSNServer.Get_Object(GetType(FSNServer.cnMensajes))
                        filename = ocnMensajes.Obtener_Nombre_Adjunto(DGuid)
                        byteBuffer = ocnMensajes.CN_Mensajes_GetAdjuntoMensaje(DGuid)
                    Case Else
                        Try
                            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
                            Dim AdjunDescargar As FSNServer.Adjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
                            AdjunDescargar.Id = context.Request("id")
                            AdjunDescargar.Nombre = context.Request("Nombre")
                            AdjunDescargar.dataSize = context.Request("ds")
                            Dim sNombre As String = context.Request("f")
                            AdjunDescargar.EscribirADisco(TempStorage, sNombre, CInt(Int(context.Request("t"))))
                            'filename = DGuid
                            filePath = TempStorage & sNombre
                            Dim input As System.IO.FileStream = New System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                            Dim buffer(input.Length - 1) As Byte
                            input.Read(buffer, 0, buffer.Length)
                            input.Close()
                            byteBuffer = buffer

                            isEP = CBool(context.Request("isEP"))
                        Catch ex As Exception

                        End Try
                End Select
            End If
        End If

        If context.Request("t") = 0 Then
            Call context.Response.AddHeader("Content-Type", "application/octet-stream")
            Call context.Response.AddHeader("Content-Disposition", "attachment;filename=" & HttpUtility.UrlEncode(context.Request("f")))
            Call context.Response.BinaryWrite(byteBuffer)
            Call context.Response.End()
        ElseIf context.Request("t") = 14 Then
            Call context.Response.AddHeader("Content-Type", "application/octet-stream")
            Call context.Response.AddHeader("Content-Disposition", "attachment;filename=" & HttpUtility.UrlEncode(context.Request("Nombre")))
            Call context.Response.BinaryWrite(byteBuffer)
            Call context.Response.End()
        ElseIf isEP Then
            Call context.Response.AddHeader("Content-Type", "application/octet-stream")
            Call context.Response.AddHeader("Content-Disposition", "attachment;filename=" & HttpUtility.UrlEncode(context.Request("Nombre")))
            Call context.Response.BinaryWrite(byteBuffer)
            Call context.Response.End()
        Else
            context.Response.ContentType = context.Request("c")
            context.Response.BinaryWrite(byteBuffer)
        End If
    End Sub
    Private Sub ListCurrentFiles(ByVal context As HttpContext)
        Dim files = New DirectoryInfo(TempStorage).GetFiles("*", SearchOption.TopDirectoryOnly).Where(Function(f) Not f.Attributes.HasFlag(FileAttributes.Hidden)).[Select](Function(f) New EPFilesStatus(f)).ToArray()

        Dim jsonObj As String = js.Serialize(files)
        context.Response.AddHeader("Content-Disposition", "inline, filename=""files.json""")
        context.Response.Write(jsonObj)
        context.Response.ContentType = "application/json"
    End Sub
End Class