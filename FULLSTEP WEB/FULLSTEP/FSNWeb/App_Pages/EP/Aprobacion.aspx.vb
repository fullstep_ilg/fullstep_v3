﻿Imports Fullstep.FSNServer
Imports System.IO

Partial Public Class Aprobacion
    Inherits FSEPPage

    Private _sEstados(2) As String
    Private _NoBorrarEstadoCollapses As Boolean = False
    Private _PostBack As PostBackHandler

    Public Class PostBackHandler
        Inherits Control
        Implements IPostBackEventHandler

        ''' <summary>
        ''' Gestiona los eventos de PostBack lanzados desde cliente
        ''' </summary>
        ''' <param name="eventArgument">String compuesto por el nombre del comando a ejecutar y una cadena de argumentos separado por el caracter #</param>
        Public Sub RaisePostBackEvent(ByVal eventArgument As String) Implements System.Web.UI.IPostBackEventHandler.RaisePostBackEvent
            Dim CommandName As String = eventArgument.Split("#")(0)
            Dim CommandArgument As String = eventArgument.Split("#")(1)
            Dim pag As Aprobacion = Me.Page
            Select Case CommandName
                Case "Aprobar"
                    pag.Aprobar(CommandArgument)
                Case "Denegar"
                    pag.Denegar(CommandArgument)
                Case "DetalleLineas"
                    pag.DetalleLineas()
                Case "Exportar"
                    pag.ExportarOrden(CommandArgument)
                Case "Imprimir"
                    pag.ImprimirOrden(CommandArgument)
                Case "MostrarDetalleArt"
                    pag.MostrarDetalleArticulo(CommandArgument)
                Case "IrDetallePedido"
                    Dim idOrden As String = CommandArgument.Split("_")(0)
                    Dim idInstancia As String = CommandArgument.Split("_")(1)
                    Dim Bloque As String = CommandArgument.Split("_")(2)
                    Dim Rol As String = CommandArgument.Split("_")(3)
                    Dim Motivo As Integer = CType(CommandArgument.Split("_")(4), Integer)
                    Dim Pedido As Integer = CType(CommandArgument.Split("_")(5), Integer)
                    Dim TipoPedido As Integer = CType(CommandArgument.Split("_")(6), Integer)
                    Dim Empresa As Integer = CType(CommandArgument.Split("_")(7), Integer)
                    Dim Anyo As Short = CType(CommandArgument.Split("_")(8), Integer)
                    Dim NumPed As Long = CType(CommandArgument.Split("_")(9), Integer)
                    Dim NumOrden As Long = CType(CommandArgument.Split("_")(10), Integer)

                    Dim PaginaPaginacion As Short
                    If pag.ddlPage.Visible Then
                        PaginaPaginacion = pag.ddlPage.SelectedValue
                    Else
                        PaginaPaginacion = 0
                    End If

                    HttpContext.Current.Response.Redirect("DetallePedido.aspx?Desde=" & IIf(Motivo = 1, 2, 1) & _
                                        "&Id=" & idOrden & "&Instancia=" & idInstancia & "&Bloque=" & Bloque & "&Rol=" & Rol & _
                                        "&IdPedido=" & Pedido & "&TipoPedido=" & TipoPedido & "&IdEmpresa=" & Empresa & "&Anyo=" & Anyo & "&numped=" & NumPed & "&numorden=" & NumOrden & "&PaginaPaginacion=" & PaginaPaginacion)
            End Select
        End Sub
    End Class
    ''' Revisado por: blp. Fecha: 20/12/2011
    ''' <summary>
    ''' Inicializa textos, controles y paginación
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce cuando el control de servidor se carga en el objeto Page. Tiempo: depende del número de datos</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'SÃ³lo hacemos el Load si es la primera carga (no es postback) o no llega por una solicitud de Carga por ajax de fsnTvwCategorias (el treeview)
        If Not Me.IsPostBack OrElse Request.Form("__CALLBACKID") Is Nothing OrElse (Request.Form("__CALLBACKID") IsNot Nothing AndAlso Request.Form("__CALLBACKID").IndexOf("fsnTvwCategorias") < 0) Then
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_AprobacionPedidos
            Master.Seleccionar("Catalogo", "Aprobacion")
            Me.ScriptMgr.EnablePageMethods = True
            _PostBack = New PostBackHandler()
            _PostBack.ID = "PostBackHandler"
            Me.Controls.Add(_PostBack)
            If Not IsPostBack() Then
                CargarTextos()
            End If

            'InicializarControles debe ir después de cargarTextos pues añade controles a la página que usan textos q se pasan en CargarTExtos
            'Además, se tiene que recargar siempre por las categorías
            InicializarControles()

            If Request.QueryString("CargarTodo") = 1 Then
                ActualizarDsetpedidos()
            End If

            If Not IsPostBack() Then
                'La línea siguiente iba inicialmente en CargarTextos pero dado que usa un valor desde bdd y que eso generaría la carga de datos del dataset antes de crear todos los controles que necesita la carga (se crean en InicializarControles), lo colocamos aquí
                lblMaxLength.Text = Textos(155) & ": " & ComentarioLineaMaxLength

                'Cookies
                Dim Cookie As HttpCookie
                Cookie = Request.Cookies("APROBACION_CRIT_ORD")
                If Cookie Is Nothing OrElse String.IsNullOrEmpty(Cookie.Value) Then
                    CampoOrden = "FECHA"
                Else
                    CampoOrden = Cookie.Value
                    Cookie.Expires = DateTime.Now.AddDays(30)
                    Response.AppendCookie(Cookie)
                End If
                Cookie = Request.Cookies("APROBACION_CRIT_DIREC")
                If Cookie Is Nothing OrElse String.IsNullOrEmpty(Cookie.Value) Then
                    SentidoOrdenacion = "DESC"
                Else
                    SentidoOrdenacion = Cookie.Value
                    Cookie.Expires = DateTime.Now.AddDays(30)
                    Response.AppendCookie(Cookie)
                End If
                ActualizarFiltros()
                btnCancelarConfirm.OnClientClick = "$find('" & mpeConfirm.ClientID & "').hide(); return false;"
            End If

            dteDesde.NullText = ""
            dteHasta.NullText = ""

            Dim pds As New PagedDataSource
            pds.AllowPaging = True
            pds.DataSource = DevolverPedidosConFiltro(CampoOrden, SentidoOrdenacion).DefaultView
            If ViewState("PageSize") Is Nothing Then
                pds.PageSize = 10
            Else
                pds.PageSize = CType(ViewState("PageSize"), Integer)
            End If
            If ViewState("CurrentPage") Is Nothing Then
                pds.CurrentPageIndex = 0
                If Not Request("PaginaPaginacion") = Nothing Then
                    pds.CurrentPageIndex = CInt(Request("PaginaPaginacion"))
                End If
            Else
                pds.CurrentPageIndex = CType(ViewState("CurrentPage"), Integer)
            End If
            dlOrdenes.DataSource = pds
            If Not IsPostBack() Then dlOrdenes.DataBind()
            ScriptsCliente()
            ScriptMgr.RegisterAsyncPostBackControl(BtnCancelarGenerarInforme)
            ScriptMgr.RegisterAsyncPostBackControl(_PostBack)
        End If
    End Sub
    ''' <summary>
    ''' Genera el código de script de cliente
    ''' </summary>
    Private Sub ScriptsCliente()
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "UpdatePanelSetup") Then
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "UpdatePanelSetup", "var ModalProgress = '" & Master.Master.UpdateModal.ClientID & "';", True)
        End If
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "CollapseHidden") Then
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "CollapseHidden", "var hddcollLineasID='" & collapseLineas.ClientID & "';", True)
        End If
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "ArrayImagenes") Then
            Dim sScript As String = "ArrImages['aprobado_act'].src='" & Me.ResolveUrl(String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, "aprobado.gif")) & "';" & _
                "ArrImages['aprobado_desact'].src='" & Me.ResolveUrl(String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, "aprobar.gif")) & "';" & _
                "ArrImages['denegado_act'].src='" & Me.ResolveUrl(String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, "denegado.gif")) & "';" & _
                "ArrImages['denegado_desact'].src='" & Me.ResolveUrl(String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, "denegar.gif")) & "';"
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "ArrayImagenes", sScript, True)
        End If
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "ArrayTextos") Then
            Dim sScript As String = String.Empty
            For i As Integer = 0 To 3
                sScript = sScript & "ArrTextos[" & i & "]='" & JSText(Textos(111 + i)) & "';"
            Next
            For i As Integer = 4 To 12
                sScript = sScript & "ArrTextos[" & i & "]='" & JSText(Textos(113 + i)) & "';"
            Next
            For i As Integer = 13 To 14
                sScript = sScript & "ArrTextos[" & i & "]='" & JSText(Textos(119 + i)) & "';"
            Next
            For i As Integer = 15 To 17
                sScript = sScript & "ArrTextos[" & i & "]='" & JSText(Textos(122 + i)) & "';"
            Next
            For i As Integer = 18 To 25
                sScript = sScript & "ArrTextos[" & i & "]='" & JSText(Textos(125 + i)) & "';"
            Next
            sScript = sScript & "ArrTextos[26]='" & JSText(Textos(65)) & "';"
            sScript = sScript & "ArrTextos[27]='" & JSText(Textos(130)) & "';"
            sScript = sScript & "ArrTextos[28]='" & JSText(Textos(141)) & "';"
            sScript = sScript & "ArrTextos[29]='" & JSText(Textos(175)) & "';"
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "ArrayTextos", sScript, True)
        End If
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "VariablesMsgBox") Then
            Dim sScript As String = "var modalMje='" & mpeConfirm.ClientID & "';" & _
                "var LabelMje='" & lblConfirm.ClientID & "';" & _
                "var TextMje='" & txtConfirm.ClientID & "';" & _
                "var LabelMaxMje='" & lblMaxLength.ClientID & "';" & _
                "var BtnAceptarMje='" & btnAceptarConfirm.ClientID & "';" & _
                "var BtnCancelarMje='" & btnCancelarConfirm.ClientID & "';"
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "VariablesMsgBox", sScript, True)
        End If
        Me.ScriptMgr.Services.Add(New ServiceReference("~/App_Pages/EP/App_Services/EPConsultas.asmx"))
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "ScriptAdjuntos") Then
            Dim sScript As String = "function mostraradjuntos(orden, linea) {" &
                    "ConsultasProxy.Adjuntos(orden, linea, 1);" &
                    "var lblcab = document.getElementById('lblArchAdjuntos');" &
                    "if (linea > 0){" &
                        "lblcab.innerHTML = '" & JSText(Textos(153)) & "'" &
                    "}" &
                    "else {" &
                        "lblcab.innerHTML = '" & JSText(Textos(154)) & "'" &
                    "}" &
                "}" &
                "function ConsultasOk(result) {" &
                    "var RsltElem = document.getElementById('capaadjuntos');" &
                    "RsltElem.innerHTML = result;" &
                    "$find('" & mpeAdj.ClientID & "').show();" &
                "}" &
                "function ConsultasKo(error, userContext, methodName) {" &
                    "if (error !== null) {" &
                        "var RsltElem = document.getElementById('capaadjuntos');" &
                        "RsltElem.innerHTML = 'An error occurred: ' + error.get_message();" &
                        "$find('" & mpeAdj.ClientID & "').show();" &
                    "}" &
                "}"
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "ScriptAdjuntos", sScript, True)
        End If
        If Not Me.Page.ClientScript.IsStartupScriptRegistered(Me.Page.GetType(), "ScriptAdjuntosIni") Then
            Dim sScript As String = "var ConsultasProxy;" & _
                "ConsultasProxy = new Fullstep.FSNWeb.EPConsultas;" & _
                "ConsultasProxy.set_defaultSucceededCallback(ConsultasOk);" & _
                "ConsultasProxy.set_defaultFailedCallback(ConsultasKo);"
            Me.Page.ClientScript.RegisterStartupScript(Me.Page.GetType(), "ScriptAdjuntosIni", sScript, True)
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutaPM") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM",
                "<script>var rutaPM = '" & System.Configuration.ConfigurationManager.AppSettings("rutaPM") & "';</script>")
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutaFS") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", _
                "<script>var rutaFS = '" & System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "';</script>")
        End If
    End Sub
    ''' Revisado por: blp. Fecha: 21/12/2011
    ''' <summary>
    ''' Carga los textos de la página en función del idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load. Máx. 0,1 seg</remarks>
    Private Sub CargarTextos()
        BtnAceptarGenerarInforme.Text = Textos(115)
        BtnCancelarGenerarInforme.Text = Textos(116)
        LblConfirmGenerarListado.Text = Textos(152)
        lblTitulo.Text = Textos(69)
        lblCabecera.Text = Textos(99)
        litAnio.Text = Textos(45)

        litCesta.Text = Textos(17) & ":"
        txtCesta_TextBoxWatermarkExtender.WatermarkText = Textos(17)

        litPedido.Text = Textos(18) & ":"
        txtPedido_TextBoxWatermarkExtender.WatermarkText = Textos(18)

        chkPedidosCatalogo.Text = Textos(177)

        litPedidoProve.Text = Textos(57)
        txtPedidoProve_TextBoxWatermarkExtender.WatermarkText = Textos(18)

        If Acceso.gbUsarPedidosAbiertos Then
            litAnyoAbierto.Text = Textos(45) & ":"
            txtCestaAbierto_TextBoxWatermarkExtender.WatermarkText = Textos(17)
            txtPedidoAbierto_TextBoxWatermarkExtender.WatermarkText = Textos(18)
            chkPedidosContraAbierto.Text = Textos(178)
            litAnyoAbierto.Text = Textos(179)
        Else
            trPedidosAbiertos.Visible = False
            Me.chkPedidosContraAbierto.Visible = False
        End If

        If Acceso.gbOblCodPedido Then
            Dim oCParametros As FSNServer.CParametros = FSNServer.Get_Object(GetType(FSNServer.CParametros))
            litPedidoERP.Text = oCParametros.CargarLiteralParametros(31, Usuario.Idioma) & ":"
            txtPedidoERP_TextBoxWatermarkExtender.WatermarkText = Textos(18)
        Else
            litPedidoERP.Visible = False
            txtPedidoERP.Visible = False
        End If


        litDesde.Text = Textos(100)
        litHasta.Text = Textos(101)
        litReceptor.Text = Textos(95) & ":"
        litEmpresa.Text = Textos(91) & ":"
        litProveedor.Text = Textos(23) & ":"
        If Acceso.gbTrasladoAProvERP Then
            litProvedorERP.Text = Textos(93) & ":"
            txtProveedorERP_TextBoxWatermarkExtender.WatermarkText = Textos(93)
        Else
            litProvedorERP.Visible = False
            txtProveedorERP.Visible = False
            celdaLitProvedorERP.Visible = False
            celdaTxtProvedorERP.Visible = False
            celdaDteDesde.ColumnSpan = 2
            celdaDteHasta.ColumnSpan = 2
        End If
        litAprovisionador.Text = Textos(31) & ":"

        'Gestor: Visible sólo si hay SM
        If Me.Acceso.gbAccesoFSSM Then
            litGestor.Text = Textos(166) & ": " 'Gestor: 
        End If

        _sEstados(0) = Textos(54)
        _sEstados(1) = Textos(55)
        _sEstados(2) = Textos(157) '"Incluir pedidos pendientes de aprobar en nivel superior"


        LblCatalogoArbol.Text = Textos(102)
        LnkBtnCategoria.Text = Textos(102)
        btnBuscar.Text = Textos(60)
        btnAceptarConfirm.Text = Textos(115)
        btnCancelarConfirm.Text = Textos(116)
        litNoPedidos.Text = "<p class=""EtiquetaGrande""> &nbsp; &nbsp; " & Textos(43) & "</p>"

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
        lblViendo.Text = Textos(106)
        lblResulDe.Text = Textos(75)
        lblPagina.Text = Textos(74)
        lblDe.Text = Textos(75)
        lblOrdenacion.Text = Textos(107)

        lblViendo2.Text = Textos(106)
        lblResulDe2.Text = Textos(75)
        lblPagina2.Text = Textos(74)
        lblDe2.Text = Textos(75)
        lblOrdenacion2.Text = Textos(107)

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_AprobacionPedidos

        Dim li As New ListItemCollection
        li.Add(New ListItem(Me.Textos(88), "FECHA"))
        li.Add(New ListItem(Me.Textos(91), "EMPRESA"))
        li.Add(New ListItem(Me.Textos(23), "PROVEEDOR"))
        li.Add(New ListItem(Me.Textos(10), "IMPORTE"))
        ListOrden = li

    End Sub
    ''' Revisado por: blp. Fecha: 20/12/2011
    ''' <summary>
    ''' Inicializa los controles en la primera carga de la página
    ''' </summary>
    ''' <remarks>Llamada desde Page_Load. Máx. 0,2 seg.</remarks>
    Private Sub InicializarControles()
        If Not Me.Acceso.gbVerNumeroProveedor Then
            'Ocultamos litPedidoProve y txtPedidoProve
            tblBusqueda.Rows(0).Cells(6).Visible = False
            tblBusqueda.Rows(0).Cells(7).Visible = False
        End If

        If Not Me.IsPostBack Then
            dteDesde.Value = DateTime.Now.Subtract(System.TimeSpan.FromDays(30)).Date
            dteHasta.Value = DateTime.Now.Date

            ' Checks estados
            chklstEstado.Items.Clear()
            chklstEstado.Items.Add(New ListItem(_sEstados(0), 1))
            chklstEstado.Items.Add(New ListItem(_sEstados(1), 2))
            chklstEstado.Items.Add(New ListItem(_sEstados(2), 3))

            If DsetPedidos.Tables("DDLINFO").Rows.Count > 0 Then
                Dim lista As List(Of ListItem)
                ' DropDown Años
                lista = Anyos().ToList
                If lista.Count > 0 Then _
                    lista.Insert(0, New ListItem(String.Empty, String.Empty))
                ddlAnio.DataSource = lista
                ddlAnio.DataBind()
                ddlAnyoAbierto.DataSource() = lista
                ddlAnyoAbierto.DataBind()
                ' DropDown Empresa
                lista = Empresas().ToList
                If lista.Count > 0 Then _
                    lista.Insert(0, New ListItem(String.Empty, String.Empty))
                For Each l As ListItem In lista
                    If l.Text.Length > 50 Then l.Text = Left(l.Text, 48) & "..."
                Next
                ddlEmpresa.DataSource = lista
                ddlEmpresa.DataBind()
                ' DropDown Proveedor
                lista = Proveedores().ToList
                For Each l As ListItem In lista
                    If l.Text.Length > 50 Then l.Text = Left(l.Text, 48) & "..."
                Next
                If lista.Count > 0 Then _
                    lista.Insert(0, New ListItem(String.Empty, String.Empty))
                ddlProveedor.DataSource = lista
                ddlProveedor.DataBind()
                ' DropDown Aprovisionador
                lista = Aprovisionadores().ToList
                For Each l As ListItem In lista
                    If l.Text.Length > 50 Then l.Text = Left(l.Text, 48) & "..."
                Next
                If lista.Count > 0 Then _
                    lista.Insert(0, New ListItem(String.Empty, String.Empty))
                ddlAprovisionador.DataSource = lista
                ddlAprovisionador.DataBind()
                ' DropDown Receptor
                lista = Receptores().ToList
                For Each l As ListItem In lista
                    If l.Text.Length > 50 Then l.Text = Left(l.Text, 48) & "..."
                Next
                If lista.Count > 0 Then _
                    lista.Insert(0, New ListItem(String.Empty, String.Empty))
                ddlReceptor.DataSource = lista
                ddlReceptor.DataBind()
                'DropDown Gestores
                'Gestor: Visible sólo si hay SM
                If Not Me.Acceso.gbAccesoFSSM Then
                    litGestor.Visible = False
                    ddlGestor.Visible = False
                Else
                    Dim dtGestores As DataTable = Gestores()
                    If dtGestores.Rows.Count > 0 AndAlso dtGestores.Rows(0).Item("GESTORDEN") <> String.Empty Then
                        Dim oRow As DataRow = dtGestores.NewRow
                        oRow("GESTORCOD") = String.Empty
                        oRow("GESTORDEN") = String.Empty
                        dtGestores.Rows.InsertAt(oRow, 0)
                    End If
                    ddlGestor.DataSource = dtGestores
                    ddlGestor.DataTextField = dtGestores.Columns(1).ColumnName
                    ddlGestor.DataValueField = dtGestores.Columns(0).ColumnName
                    ddlGestor.DataBind()
                End If
            End If

            'Categorías
            CargarArbolCategorias()
        End If
    End Sub
#Region "Arbol Categorias"

    Private ReadOnly Property TodasCategorias() As DataSet
        Get
            Dim ds As DataSet
            If HttpContext.Current.Cache("TodasCategorias") Is Nothing Then
                Dim oCCategorias As FSNServer.CCategorias = FSNServer.Get_Object(GetType(FSNServer.CCategorias))
                ds = oCCategorias.CargarArbolCategorias("")
                Me.InsertarEnCache("TodasCategorias", ds)
            Else
                ds = HttpContext.Current.Cache("TodasCategorias")
            End If
            Dim key() As DataColumn = {ds.Tables(0).Columns("ID")}
            ds.Tables(0).PrimaryKey = key
            Return ds
        End Get
    End Property

    ''' Revisado por: blp. Fecha: 22/12/2011
    ''' <summary>
    ''' Procedimiento en el que cargaremos los valores para el arbol de categorías
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: El evento Load de la página
    ''' Tiempo máximo: 0 sec.</remarks>
    Private Sub CargarArbolCategorias()
        If TodasCategorias.Tables(0) IsNot Nothing AndAlso TodasCategorias.Tables(0).Rows.Count > 0 Then
            fsnTvwCategorias.Nodes(0).Text = Textos(102) 'Categorias
            fsnTvwCategorias.NodeStyle.Height = "20"
        Else
            fsnTvwCategorias.Nodes.Clear()
        End If
    End Sub

    ''' <summary>
    ''' Evento que se lanzará al seleccionar un nodo del árbol de categorías, mostrando la categoría seleccionada en el panel de bÃºsqueda
    ''' </summary>
    ''' <param name="sender">el objecto que llama al evento (el arbol)</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el propio control
    ''' Tiempo máximo: 0 seg.</remarks>
    Private Sub fsntvwCategorias_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fsnTvwCategorias.SelectedNodeChanged
        If fsnTvwCategorias.SelectedNode.Value <> "_0_" Then
            If fsnTvwCategorias.SelectedNode Is Nothing Then
                litCategoria.Text = ""
                litCategoria.Visible = False
                lnkQuitarCategoria.CommandArgument = ""
                lnkQuitarCategoria.Visible = False
            Else
                litCategoria.Text = fsnTvwCategorias.SelectedNode.Text
                litCategoria.Visible = True
                lnkQuitarCategoria.CommandArgument = fsnTvwCategorias.SelectedValue
                lnkQuitarCategoria.Visible = True
            End If
            mpePanelArbol.Hide()
            updpnlCategorias.Update()
        End If
    End Sub

    Public Sub fsnTvwCategorias_TreeNodePopulate(ByVal sender As Object, ByVal e As TreeNodeEventArgs) Handles fsnTvwCategorias.TreeNodePopulate
        Dim query
        Select Case e.Node.Value
            Case "_0_"
                query = From datos In TodasCategorias.Tables(0) _
                            Where datos("PADRE") Is System.DBNull.Value _
                            Select datos

            Case Else
                query = From datos In TodasCategorias.Tables(0) _
                            Where datos("PADRE") IsNot System.DBNull.Value AndAlso datos("PADRE") = e.Node.Value _
                            Select datos

        End Select

        If CType(query, System.Data.EnumerableRowCollection(Of System.Data.DataRow)).Any Then
            For Each oDatosNodo As System.Data.DataRow In CType(query, System.Data.EnumerableRowCollection(Of System.Data.DataRow))
                Dim nodo As New TreeNode
                nodo.Text = oDatosNodo.Item("DEN")
                nodo.Value = oDatosNodo.Item("ID")
                nodo.Expanded = False
                nodo.PopulateOnDemand = True
                nodo.SelectAction = TreeNodeSelectAction.Select
                e.Node.ChildNodes.Add(nodo)
            Next
        End If
    End Sub

    ''' <summary>
    ''' Evento que se lanzará al quitar la categoría seleccionada pulsando en el aspa y que eliminará el criterio de búsqueda
    ''' </summary>
    ''' <param name="sender">el objecto que llama al evento (el arbol)</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el propio control
    ''' Tiempo máximo: 0 seg.</remarks>
    Private Sub lnkQuitarCategoria_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkQuitarCategoria.Click
        fsnTvwCategorias.SelectedNode.Selected = False
        litCategoria.Text = ""
        litCategoria.Visible = False
        lnkQuitarCategoria.CommandArgument = ""
        lnkQuitarCategoria.Visible = False
        pnlCategorias.Update()
    End Sub

#End Region
#Region "Consultas"
    Public ReadOnly Property DsetPedidos() As DataSet
        Get
            If HttpContext.Current.Cache("DsetPedidosAprobacion_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                Return ActualizarDsetpedidos()
            Else
                Return HttpContext.Current.Cache("DsetPedidosAprobacion_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
            End If
        End Get
    End Property
    Public ReadOnly Property ComentarioLineaMaxLength() As String
        Get
            If Not DsetPedidos.Tables("COMENTMAXLENGTH").Rows.Find("LINEAS_EST").Item(1) Is Nothing Then
                Return DsetPedidos.Tables("COMENTMAXLENGTH").Rows.Find("LINEAS_EST").Item(1).ToString
            Else
                Return ""
            End If
        End Get
    End Property
    ''' Revisado por: blp. Fecha: 20/12/2011
    ''' <summary>
    ''' Actualiza el dataset de pedidos.
    ''' </summary>
    ''' <remarks>Se produce cuando el control de servidor se carga en el objeto Page. Tiempo: depende del número de datos</remarks>
    Private Function ActualizarDsetpedidos() As DataSet
        'Antes de actualizar los pedidos, actualizamos los filtros para asegurarnos de que los filtros de fecha y de estados (que se usan al generar el dataset) son los seleccionados por el usuario
        ActualizarFiltros()
        Dim dsOrdenes As DataSet = GenerarDsetPedidos(Me.Usuario.CodPersona, Me.FSNServer, Me.Usuario.Idioma, FiltrosAnyadidos)
        Me.InsertarEnCache("DsetPedidosAprobacion_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString(), dsOrdenes)
        Return dsOrdenes
    End Function
    Public Sub CrearTablaAdjuntosOrden()
        If DsetPedidos.Tables("ADJUNTOS_ORDEN") Is Nothing Then
            Dim dt As DataTable = New DataTable("ADJUNTOS_ORDEN")
            dt.Columns.Add("ID", GetType(System.Int32))
            dt.Columns.Add("ORDEN", GetType(System.Int32))
            dt.Columns.Add("NOMBRE", GetType(System.String))
            dt.Columns.Add("COMENTARIO", GetType(System.String))
            dt.Columns.Add("FECHA", GetType(System.DateTime))
            dt.Columns.Add("DATASIZE", GetType(System.Double))
            dt.Columns.Add("TIPO", GetType(TiposDeDatos.Adjunto.Tipo))
            Dim keylp() As DataColumn = {dt.Columns("ID")}
            dt.PrimaryKey = keylp
            DsetPedidos.Tables.Add(dt)
            keylp(0) = DsetPedidos.Tables("ADJUNTOS_ORDEN").Columns("ORDEN")
            DsetPedidos.Relations.Add("REL_ORDENES_ADJUNTOS", DsetPedidos.Tables("ORDENES").PrimaryKey, keylp, False)
        End If
    End Sub
    Public Sub CrearTablasAdjuntosLinea()
        If DsetPedidos.Tables("ADJUNTOS_LINEA") Is Nothing Then
            Dim dt As DataTable = New DataTable("ADJUNTOS_LINEA")
            dt.Columns.Add("ID", GetType(System.Int32))
            dt.Columns.Add("LINEA", GetType(System.Int32))
            dt.Columns.Add("NOMBRE", GetType(System.String))
            dt.Columns.Add("COMENTARIO", GetType(System.String))
            dt.Columns.Add("FECHA", GetType(System.DateTime))
            dt.Columns.Add("DATASIZE", GetType(System.Double))
            dt.Columns.Add("TIPO", GetType(TiposDeDatos.Adjunto.Tipo))
            Dim keylp() As DataColumn = {dt.Columns("ID")}
            dt.PrimaryKey = keylp
            DsetPedidos.Tables.Add(dt)
            keylp(0) = DsetPedidos.Tables("ADJUNTOS_LINEA").Columns("LINEA")
            DsetPedidos.Relations.Add("REL_LINEAS_ADJUNTOS", DsetPedidos.Tables("LINEASPEDIDO").PrimaryKey, keylp, False)
        End If
    End Sub
    ''' Revisado por: blp. Fecha: 19/12/2011
    ''' <summary>
    ''' Generamos el conjunto de datos de la página para los servicios web (para poder mostrar los comentarios de orden o línea)
    ''' </summary>
    ''' <returns>dataset con los pedidos</returns>
    ''' <remarks>Llamada desde GenerarDsetPedidos. Máx. depende del número de datos</remarks>
    Private Shared Function GenerarDsetPedidos() As DataSet
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim ods As DataSet = GenerarDsetPedidos(oUsuario.CodPersona, oServer, oUsuario.Idioma)
        If Not ods Is Nothing Then _
            HttpContext.Current.Cache.Insert("DsetPedidosAprobacion_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString(), ods, Nothing, Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        Return ods
    End Function
    ''' Revisado por: blp. Fecha: 19/12/2011
    ''' <summary>
    ''' Generamos el conjunto de datos de la página para los servicios web (para poder mostrar los comentarios de orden o línea)
    ''' </summary>
    ''' <param name="CodPersona">Código de la persona</param>
    ''' <param name="oServer">Sessión FS_EP_Server</param>
    ''' <param name="Idioma">idioma en el que devolver el dataset</param>
    ''' <returns>dataset con los pedidos</returns>
    ''' <remarks>Llamada desde GenerarDsetPedidos. Máx. depende del número de datos</remarks>
    Private Shared Function GenerarDsetPedidos(ByVal CodPersona As String, ByVal oServer As FSNServer.Root, ByVal Idioma As FSNLibrary.Idioma) As DataSet
        Dim oOrdenes As COrdenes = oServer.Get_Object(GetType(FSNServer.COrdenes))
        Dim bIncluirAprobados As Boolean = False, bIncluirDenegados As Boolean = False, bIncluirAprobNivelSup As Boolean = False

        Dim dFecDesde As Date = Date.MinValue, dFecHasta As Date = Date.MinValue
        If HttpContext.Current.Session("Estado") IsNot Nothing Then
            Dim sAux() As String
            sAux = Split(HttpContext.Current.Session("Estado"), "_")
            bIncluirAprobados = CBool(sAux(0))
            bIncluirDenegados = CBool(sAux(1))
            bIncluirAprobNivelSup = CBool(sAux(2))
        End If
        If HttpContext.Current.Session("FecDesde") IsNot Nothing Then _
            dFecDesde = CDate(HttpContext.Current.Session("FecDesde"))
        If HttpContext.Current.Session("FecHasta") IsNot Nothing Then _
            dFecHasta = CDate(HttpContext.Current.Session("FecHasta"))

        Return oOrdenes.BuscarOrdenesAprobador(CodPersona, Idioma.ToString(), dFecDesde, dFecHasta, bIncluirAprobados, bIncluirDenegados, bIncluirAprobNivelSup)
    End Function
    ''' Revisado por: blp. Fecha: 19/12/2011
    ''' <summary>
    ''' Esta función es idéntica a la inmediatamente superior. He añadido el FiltrosAnyadidos a los parámetros para cambiar la firma y que no de errores aunque no era necesario
    ''' dado que FiltrosAnyadidos es una propiedad
    ''' Generamos el conjunto de datos de la página para los servicios web (para poder mostrar los comentarios de orden o línea)
    ''' </summary>
    ''' <param name="CodPersona">Código de la persona</param>
    ''' <param name="oServer">Sessión FS_EP_Server</param>
    ''' <param name="Idioma">idioma en el que devolver el dataset</param>
    ''' <returns>dataset con los pedidos</returns>
    ''' <remarks>Llamada desde GenerarDsetPedidos. Máx. depende del número de datos</remarks>
    Private Function GenerarDsetPedidos(ByVal CodPersona As String, ByVal oServer As FSNServer.Root, ByVal Idioma As FSNLibrary.Idioma, ByVal FiltrosAnyadidos As List(Of Filtro)) As DataSet
        'Hay tres filtros q implican volver a consultar en bdd: Estado, fecha desde y fecha hasta. Los recuperamos y pasamos a variables
        Dim bIncluirAprobados As Boolean = False, bIncluirDenegados As Boolean = False, bIncluirAprobNivelSup As Boolean = False, bIncluirPedidosCatalogo As Boolean, bIncluirPedidosContraAbiertos As Boolean
        Dim dFecDesde As Date = Date.MinValue, dFecHasta As Date = Date.MinValue
        Dim iAnyoPedAbierto As Integer
        Dim iCestaPedAbierto, iPedidoPedAbierto As Long
        For Each ofiltro As Filtro In FiltrosAnyadidos
            Dim x As Filtro = ofiltro
            Select Case x.tipo
                Case TipoFiltro.FechaDesde
                    dFecDesde = x.ValorFecha
                Case TipoFiltro.FechaHasta
                    dFecHasta = x.ValorFecha
                Case TipoFiltro.Estado
                    Dim sAux() As String
                    sAux = Split(x.Valor, "_")
                    bIncluirAprobados = sAux(0)
                    bIncluirDenegados = sAux(1)
                    bIncluirAprobNivelSup = sAux(2)
                Case TipoFiltro.IncluirPedidosCatalogo
                    bIncluirPedidosCatalogo = True
                Case TipoFiltro.IncluirPedidosContraAbierto
                    bIncluirPedidosContraAbiertos = True
                Case TipoFiltro.AnyoAbierto
                    iAnyoPedAbierto = x.Valor
                Case TipoFiltro.CestaAbierto
                    iCestaPedAbierto = x.Valor
                Case TipoFiltro.NumPedidoAbierto
                    iPedidoPedAbierto = x.Valor
            End Select
        Next
        Dim oOrdenes As COrdenes = oServer.Get_Object(GetType(FSNServer.COrdenes))
        Return oOrdenes.BuscarOrdenesAprobador(CodPersona, Idioma.ToString(), dFecDesde, dFecHasta, bIncluirAprobados, bIncluirDenegados, bIncluirAprobNivelSup, bIncluirPedidosCatalogo, bIncluirPedidosContraAbiertos, iAnyoPedAbierto, iCestaPedAbierto, iPedidoPedAbierto)
    End Function
    ''' Revisado por: blp. Fecha:21/12/2011
    ''' <summary>
    ''' Nos devuelve el dataset con los filtros de seleccion aplicados
    ''' </summary>
    ''' <param name="OrdenacionCampo">Campo de ordenacion</param>
    ''' <param name="OrdenacionSentido">Sentido ordenacion</param>        
    ''' <param name="Pedidos">Dataset con los datos</param>  
    ''' <param name="Filtros">Filtros que se han introducido para la busqueda</param>  
    ''' <returns>Nos devuelve el dataset con los filtros de seleccion aplicados</returns>
    ''' <remarks>Llamada desde:DevolverPedidosConFiltro; Tiempo máximo:1seg.</remarks>
    Public Function DevolverPedidosConFiltro(ByVal OrdenacionCampo As String, ByVal OrdenacionSentido As String, ByVal Pedidos As DataSet, ByVal Filtros As List(Of Filtro)) As DataTable
        Dim dtPed As DataTable = Pedidos.Tables("ORDENES").Copy()
        Dim query As IEnumerable(Of DataRow)
        'Ahora hay una serie de filtros que se usarán con el dataset en caché y otros que implicarán buscar de nuevo en base de datos (Estado, Fecha Desde y Fecha hasta)
        'Aquí dejamos los que corresponden a caché
        For Each ofiltro As Filtro In Filtros
            Dim x As Filtro = ofiltro
            Select Case x.tipo
                Case TipoFiltro.Anio
                    query = From Datos In dtPed.AsEnumerable()
                            Where Datos.Item("ANYO") = x.Valor
                            Select Datos
                Case TipoFiltro.Aprovisionador
                    query = From Datos In dtPed.AsEnumerable()
                            Where Datos.Item("APROVISIONADOR") = x.Valor
                            Select Datos
                Case TipoFiltro.Categoria
                    Dim cats As String() = Split(x.Valor, "_")
                    Select Case UBound(cats)
                        Case 0
                            query = From Datos In dtPed
                                    Join Lineas In DsetPedidos.Tables("LINEASPEDIDO") On Lineas.Item("ORDEN") Equals Datos.Item("ID")
                                    Where DBNullToSomething(Lineas.Item("CAT1")) = cats(0)
                                    Select Datos Distinct
                        Case 1
                            query = From Datos In dtPed
                                    Join Lineas In DsetPedidos.Tables("LINEASPEDIDO") On Lineas.Item("ORDEN") Equals Datos.Item("ID")
                                    Where DBNullToSomething(Lineas.Item("CAT1")) = cats(0) _
                                    And DBNullToSomething(Lineas.Item("CAT2")) = cats(1)
                                    Select Datos Distinct
                        Case 2
                            query = From Datos In dtPed
                                    Join Lineas In DsetPedidos.Tables("LINEASPEDIDO") On Lineas.Item("ORDEN") Equals Datos.Item("ID")
                                    Where DBNullToSomething(Lineas.Item("CAT1")) = cats(0) _
                                      And DBNullToSomething(Lineas.Item("CAT2")) = cats(1) _
                                      And DBNullToSomething(Lineas.Item("CAT3")) = cats(2)
                                    Select Datos Distinct
                        Case 3
                            query = From Datos In dtPed
                                    Join Lineas In DsetPedidos.Tables("LINEASPEDIDO") On Lineas.Item("ORDEN") Equals Datos.Item("ID")
                                    Where DBNullToSomething(Lineas.Item("CAT1")) = cats(0) _
                                      And DBNullToSomething(Lineas.Item("CAT2")) = cats(1) _
                                      And DBNullToSomething(Lineas.Item("CAT3")) = cats(2) _
                                      And DBNullToSomething(Lineas.Item("CAT4")) = cats(3)
                                    Select Datos Distinct
                        Case Else
                            query = From Datos In dtPed
                                    Join Lineas In DsetPedidos.Tables("LINEASPEDIDO") On Lineas.Item("ORDEN") Equals Datos.Item("ID")
                                    Where DBNullToSomething(Lineas.Item("CAT1")) = cats(0) _
                                      And DBNullToSomething(Lineas.Item("CAT2")) = cats(1) _
                                      And DBNullToSomething(Lineas.Item("CAT3")) = cats(2) _
                                      And DBNullToSomething(Lineas.Item("CAT4")) = cats(3) _
                                      And DBNullToSomething(Lineas.Item("CAT5")) = cats(4)
                                    Select Datos Distinct
                    End Select
                Case TipoFiltro.Cesta
                    query = From Datos In dtPed.AsEnumerable()
                            Where DBNullToSomething(Datos.Item("NUMPEDIDO")) = CType(x.Valor, Integer)
                            Select Datos
                Case TipoFiltro.Empresa
                    query = From Datos In dtPed.AsEnumerable()
                            Where DBNullToSomething(Datos.Item("EMPRESA")) = CType(x.Valor, Integer)
                            Select Datos
                Case TipoFiltro.NumPedido
                    query = From Datos In dtPed.AsEnumerable()
                            Where DBNullToSomething(Datos.Item("NUMORDEN")) = CType(x.Valor, Integer)
                            Select Datos
                Case TipoFiltro.NumPedidoERP
                    query = From Datos In dtPed.AsEnumerable()
                            Where DBNullToStr(Datos.Item("REFERENCIA")).ToString().ToUpper() = x.Valor.ToUpper()
                            Select Datos
                Case TipoFiltro.NumPedidoProve
                    query = From Datos In dtPed.AsEnumerable()
                            Where DBNullToStr(Datos.Item("NUMEXT")).ToString().ToUpper() = x.Valor.ToUpper()
                            Select Datos
                Case TipoFiltro.Proveedor
                    query = From Datos In dtPed.AsEnumerable()
                            Where Datos.Item("PROVECOD") = x.Valor
                            Select Datos
                Case TipoFiltro.ProveedorERP
                    query = From Datos In dtPed.AsEnumerable()
                            Where DBNullToStr(Datos.Item("COD_PROVE_ERP")).ToString().ToUpper() = x.Valor.ToUpper()
                            Select Datos
                Case TipoFiltro.Receptor
                    query = From Datos In dtPed.AsEnumerable()
                            Where DBNullToStr(Datos.Item("RECEPTOR")) = x.Valor
                            Select Datos
                Case TipoFiltro.Gestor
                    query = From Datos In dtPed
                            Join Lineas In DsetPedidos.Tables("LINEASPEDIDO") On Lineas.Item("ORDEN") Equals Datos.Item("ID")
                            Join Gestores In DsetPedidos.Tables("GESTORES") On Gestores.Item("LINEA") Equals Lineas.Item("LINEAID")
                            Where DBNullToSomething(Gestores.Item("GESTOR")) = x.Valor
                            Select Datos Distinct
                Case Else
                    query = From Datos In dtPed.AsEnumerable()
                            Select Datos
            End Select
            If query.Count() > 0 Then
                dtPed = query.CopyToDataTable()
            Else
                dtPed.Rows.Clear()
                Exit For
            End If
        Next
        If dtPed.Rows.Count > 0 Then
            Select Case OrdenacionCampo
                Case "EMPRESA"
                    dtPed.Columns.Add("ORDENACION", Type.GetType("System.String"), "DENEMPRESA")
                Case "PROVEEDOR"
                    dtPed.Columns.Add("ORDENACION", Type.GetType("System.String"), "PROVEDEN")
                Case "IMPORTE"
                    dtPed.Columns.Add("ORDENACION", Type.GetType("System.Double"), "IMPORTE * CAMBIO")
                Case Else
                    dtPed.Columns.Add("ORDENACION", Type.GetType("System.DateTime"), "FECHA")
            End Select
            If OrdenacionSentido = "ASC" Then
                query = From Datos In dtPed.AsEnumerable()
                        Order By DBNullToSomething(Datos.Item("ORDENACION")) Ascending
                        Select Datos
            Else
                query = From Datos In dtPed.AsEnumerable()
                        Order By DBNullToSomething(Datos.Item("ORDENACION")) Descending
                        Select Datos
            End If
            dtPed = CType(query, EnumerableRowCollection(Of DataRow)).CopyToDataTable()
        End If
        Return dtPed
    End Function
    Public Function DevolverPedidosConFiltro(ByVal OrdenacionCampo As String, ByVal OrdenacionSentido As String) As DataTable
        'DEMO BERGE / 2017 / 35: no debería usarse la cache en aprobación o al menos que cuando le den al boton Buscar siempre vaya a BD aunque la busqueda sea la misma. 
        'NO se quita la cache. Se usa "ActualizarDsetpedidos" q no tira de cache
        Return DevolverPedidosConFiltro(OrdenacionCampo, OrdenacionSentido, ActualizarDsetpedidos, FiltrosAnyadidos)
    End Function
    ''' Revisado por: blp. Fecha: 21/12/2011
    ''' <summary>
    ''' Devuelve el listado de Empresas de los pedidos aprobados o por aprobar del usuario
    ''' </summary>
    ''' <returns>Listado de Empresas</returns>
    ''' <remarks>Llamada desde InicializarControles. Máx. 0,2 seg.</remarks>
    Public Function Empresas() As List(Of ListItem)
        If HttpContext.Current.Cache("DsetAprobacionEmpresas_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
            Dim query As List(Of ListItem) = From Datos In DsetPedidos.Tables("DDLINFO")
                                             Order By Datos.Item("DENEMPRESA")
                                             Select New ListItem(Datos.Item("DENEMPRESA"), Datos.Item("EMPRESA")) Distinct.ToList()
            Me.InsertarEnCache("DsetAprobacionEmpresas_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString(), query)
            Return query
        Else
            Return HttpContext.Current.Cache("DsetAprobacionEmpresas_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
        End If
    End Function
    ''' Revisado por: blp. Fecha: 21/12/2011
    ''' <summary>
    ''' Devuelve el listado de Proveedores de los pedidos aprobados o por aprobar del usuario
    ''' </summary>
    ''' <returns>Listado de Proveedores</returns>
    ''' <remarks>Llamada desde InicializarControles. Máx. 0,2 seg.</remarks>
    Public Function Proveedores() As List(Of ListItem)
        If HttpContext.Current.Cache("DsetAprobacionProve_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
            Dim query As List(Of ListItem)
            If Me.FSNUser.MostrarCodProve Then
                query = From Datos In DsetPedidos.Tables("DDLINFO")
                        Order By Datos.Item("PROVECOD") & " - " & Datos.Item("PROVEDEN")
                        Select New ListItem(Datos.Item("PROVECOD") & " - " & Datos.Item("PROVEDEN"), Datos.Item("PROVECOD")) Distinct.ToList()
            Else
                query = From Datos In DsetPedidos.Tables("DDLINFO")
                        Order By Datos.Item("PROVEDEN") Ascending
                        Select New ListItem(Datos.Item("PROVEDEN"), Datos.Item("PROVECOD")) Distinct.ToList()
            End If
            Me.InsertarEnCache("DsetAprobacionProve_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString(), query)
            Return query
        Else
            Return HttpContext.Current.Cache("DsetAprobacionProve_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
        End If
    End Function
    ''' Revisado por: blp. Fecha: 21/12/2011
    ''' <summary>
    ''' Devuelve el listado de Aprovisionadores de los pedidos aprobados o por aprobar del usuario
    ''' </summary>
    ''' <returns>Listado de Aprovisionadores</returns>
    ''' <remarks>Llamada desde InicializarControles. Máx. 0,2 seg.</remarks>
    Public Function Aprovisionadores() As List(Of ListItem)
        If HttpContext.Current.Cache("DsetAprobacionAprov_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
            Dim query As List(Of ListItem) = From Datos In DsetPedidos.Tables("DDLINFO")
                                             Order By Datos.Item("APROV_APE"), Datos.Item("APROV_NOM") Ascending
                                             Select New ListItem(Datos.Item("APROV_APE") & ", " & Datos.Item("APROV_NOM"), Datos.Item("APROVISIONADOR")) Distinct.ToList()
            Me.InsertarEnCache("DsetAprobacionAprov_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString(), query)
            Return query
        Else
            Return HttpContext.Current.Cache("DsetAprobacionAprov_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
        End If
    End Function
    ''' Revisado por: blp. Fecha: 21/12/2011
    ''' <summary>
    ''' Devuelve el listado de Receptores de los pedidos aprobados o por aprobar del usuario
    ''' </summary>
    ''' <returns>Listado de Receptores</returns>
    ''' <remarks>Llamada desde InicializarControles. Máx. 0,2 seg.</remarks>
    Public Function Receptores() As List(Of ListItem)
        If HttpContext.Current.Cache("DsetAprobacionRecep_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
            Dim query As List(Of ListItem) = From Datos In DsetPedidos.Tables("DDLINFO")
                                             Where Datos.Item("RECEPTOR") IsNot DBNull.Value
                                             Order By Datos.Item("RECEP_APE"), Datos.Item("RECEP_NOM") Ascending
                                             Select New ListItem(Datos.Item("RECEP_APE") & ", " & Datos.Item("RECEP_NOM"), Datos.Item("RECEPTOR")) Distinct.ToList()
            Me.InsertarEnCache("DsetAprobacionRecep_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString(), query)
            Return query
        Else
            Return HttpContext.Current.Cache("DsetAprobacionRecep_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
        End If
    End Function
    ''' Revisado por: blp. Fecha: 21/12/2011
    ''' <summary>
    ''' Devuelve el listado de Años de los pedidos aprobados o por aprobar del usuario
    ''' </summary>
    ''' <returns>Listado de Años</returns>
    ''' <remarks>Llamada desde InicializarControles. Máx. 0,2 seg.</remarks>
    Public Function Anyos() As List(Of ListItem)
        If HttpContext.Current.Cache("DsetAprobacionAnyos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
            Dim query As List(Of ListItem) = From Datos In DsetPedidos.Tables("DDLINFO")
                                             Order By Datos.Item("ANYO") Descending
                                             Select New ListItem(Datos.Item("ANYO"), Datos.Item("ANYO")) Distinct.ToList()
            Me.InsertarEnCache("DsetAprobacionAnyos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString(), query)
            Return query
        Else
            Return HttpContext.Current.Cache("DsetAprobacionAnyos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
        End If
    End Function
    ''' Revisado por: blp. Fecha: 21/12/2011
    ''' <summary>
    ''' Devuelve el listado de Años de los pedidos aprobados o por aprobar del usuario
    ''' </summary>
    ''' <returns>Listado de Años</returns>
    ''' <remarks>Llamada desde InicializarControles. Máx. 0,2 seg.</remarks>
    Public Function AnyosAbiertos() As List(Of ListItem)
        If HttpContext.Current.Cache("DsetAprobacionAnyos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
            Dim query As List(Of ListItem) = From Datos In DsetPedidos.Tables("DDLINFO")
                                             Order By Datos.Item("ANYO") Descending
                                             Select New ListItem(Datos.Item("ANYO_PED_ABIERTO"), Datos.Item("ANYO_PED_ABIERTO")) Distinct.ToList()
            Me.InsertarEnCache("DsetAprobacionAnyos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString(), query)
            Return query
        Else
            Return HttpContext.Current.Cache("DsetAprobacionAnyos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
        End If
    End Function
    ''' Revisado por: blp. Fecha: 11/07/2011
    ''' <summary>
    ''' Devuelve el listado de Gestores de los pedidos
    ''' </summary>
    ''' <returns>Listado de Gestores</returns>
    ''' <remarks>Llamada desde InicializarControles. Máx. 0,2 seg.</remarks>
    Public Function Gestores() As DataTable
        If HttpContext.Current.Cache("DsetAprobacionGestores_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
            Dim query = From Datos In DsetPedidos.Tables("DDLINFO")
                        Select Datos.Item("GESTOR") Distinct

            Dim oPersonas As CPersonas = Me.FSNServer.Get_Object(GetType(FSNServer.CPersonas))
            Dim dtCodGestores As DataTable = oPersonas.CrearTablaGestores()
            If query.Any Then
                For Each gestorcod In query
                    dtCodGestores.Rows.Add({gestorcod})
                Next
            End If
            Dim dtGestores As DataTable = oPersonas.CargarGestores(dtCodGestores)
            Me.InsertarEnCache("DsetAprobacionGestores_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString(), dtGestores)
            Return dtGestores
        Else
            Return HttpContext.Current.Cache("DsetAprobacionGestores_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
        End If
    End Function
#End Region
#Region "Filtros"

    <Serializable()> _
    Public Enum TipoFiltro As Integer
        Anio = 0
        Cesta = 1
        NumPedido = 2
        NumPedidoProve = 3
        NumPedidoERP = 4
        Empresa = 5
        Proveedor = 6
        ProveedorERP = 7
        Aprovisionador = 8
        Receptor = 9
        FechaDesde = 10
        FechaHasta = 11
        Categoria = 12
        Estado = 13
        Gestor = 14
        AnyoAbierto = 15
        CestaAbierto = 16
        NumPedidoAbierto = 17
        IncluirPedidosCatalogo = 18
        IncluirPedidosContraAbierto = 19
    End Enum

    <Serializable()> _
    Public Structure Filtro
        Dim _tipo As TipoFiltro
        Dim _valor As String
        Dim _valorfecha As DateTime

        Public Property tipo() As TipoFiltro
            Get
                Return _tipo
            End Get
            Set(ByVal value As TipoFiltro)
                _tipo = value
            End Set
        End Property

        Public Property Valor() As String
            Get
                Return _valor
            End Get
            Set(ByVal value As String)
                _valor = value
            End Set
        End Property

        Public Property ValorFecha() As DateTime
            Get
                Return _valorfecha
            End Get
            Set(ByVal value As DateTime)
                _valorfecha = value
            End Set
        End Property

        Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As String)
            _tipo = Tipo
            _valor = Valor
        End Sub

        Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As DateTime)
            _tipo = Tipo
            _valorfecha = Valor
        End Sub
    End Structure

    Public Property FiltrosAnyadidos() As List(Of Filtro)
        Get
            If ViewState("FiltrosAnyadidos") Is Nothing Then
                Return New List(Of Filtro)
            Else
                Return CType(ViewState("FiltrosAnyadidos"), List(Of Filtro))
            End If
        End Get
        Set(ByVal value As List(Of Filtro))
            ViewState("FiltrosAnyadidos") = value
        End Set
    End Property

    ''' Revisado por: blp. Fecha: 20/12/2011
    ''' <summary>
    ''' Procedimiento que añade los filtros que se hayan seleccionado en el panel buscador a la propiedad FiltrosAnyadidos
    ''' </summary>
    ''' <remarks>LLamada desde el propio objeto: btnBuscar_Click, Page_Load
    ''' Tiempo maximo 1 sec</remarks>
    Private Sub ActualizarFiltros()
        Dim lis As New List(Of Filtro)
        If pnlBusqueda.Visible Then
            If Not String.IsNullOrEmpty(ddlAnio.SelectedValue) Then _
                lis.Add(New Filtro(TipoFiltro.Anio, ddlAnio.SelectedValue))
            If Not String.IsNullOrEmpty(ddlAnyoAbierto.SelectedValue) Then _
                lis.Add(New Filtro(TipoFiltro.AnyoAbierto, ddlAnyoAbierto.SelectedValue))

            If Not String.IsNullOrEmpty(txtCesta.Text) AndAlso IsNumeric(txtCesta.Text) Then _
                lis.Add(New Filtro(TipoFiltro.Cesta, txtCesta.Text))
            If Not String.IsNullOrEmpty(txtCestaAbierto.Text) AndAlso IsNumeric(txtCestaAbierto.Text) Then _
                lis.Add(New Filtro(TipoFiltro.CestaAbierto, txtCestaAbierto.Text))

            If Not String.IsNullOrEmpty(txtPedido.Text) Then
                Dim itxtPedido As Integer
                Try
                    itxtPedido = CType(txtPedido.Text, Integer)
                Catch ex As Exception
                    itxtPedido = -1
                End Try
                lis.Add(New Filtro(TipoFiltro.NumPedido, CType(itxtPedido, String)))
            End If

            If Not String.IsNullOrEmpty(txtPedidoAbierto.Text) Then
                Dim itxtPedido As Integer
                Try
                    itxtPedido = CType(txtPedidoAbierto.Text, Integer)
                Catch ex As Exception
                    itxtPedido = -1
                End Try
                lis.Add(New Filtro(TipoFiltro.NumPedidoAbierto, CType(itxtPedido, String)))
            End If

            If Not String.IsNullOrEmpty(txtPedidoProve.Text) Then _
                lis.Add(New Filtro(TipoFiltro.NumPedidoProve, txtPedidoProve.Text))
            If Not String.IsNullOrEmpty(txtPedidoERP.Text) Then _
                lis.Add(New Filtro(TipoFiltro.NumPedidoERP, txtPedidoERP.Text))
            If Not String.IsNullOrEmpty(ddlEmpresa.SelectedValue) Then _
                lis.Add(New Filtro(TipoFiltro.Empresa, ddlEmpresa.SelectedValue))
            If Not String.IsNullOrEmpty(ddlProveedor.SelectedValue) Then _
                lis.Add(New Filtro(TipoFiltro.Proveedor, ddlProveedor.SelectedValue))
            If Not String.IsNullOrEmpty(txtProveedorERP.Text) And txtProveedorERP.Visible Then _
                lis.Add(New Filtro(TipoFiltro.ProveedorERP, txtProveedorERP.Text))
            If Not String.IsNullOrEmpty(ddlAprovisionador.SelectedValue) Then _
                lis.Add(New Filtro(TipoFiltro.Aprovisionador, ddlAprovisionador.SelectedValue))
            If Not String.IsNullOrEmpty(ddlReceptor.SelectedValue) Then _
                lis.Add(New Filtro(TipoFiltro.Receptor, ddlReceptor.SelectedValue))
            If Not dteDesde.Value Is Nothing Then
                lis.Add(New Filtro(TipoFiltro.FechaDesde, CType(dteDesde.Value, Date)))
            End If
            'Comparamos el nuevo valor del filtro con el anterior (si existe)
            'para saber si hay que buscar de nuevo los resultados (dado que la caché no busca todos los resultados sino que filtra por fecha y estado)
            comparaFiltros(TipoFiltro.FechaDesde, CType(dteDesde.Value, Date))
            If Not dteHasta.Value Is Nothing Then
                lis.Add(New Filtro(TipoFiltro.FechaHasta, CType(dteHasta.Value, Date)))
            End If
            'Comparamos el nuevo valor del filtro con el anterior (si existe)
            'para saber si hay que buscar de nuevo los resultados (dado que la caché no busca todos los resultados sino que filtra por fecha y estado)
            comparaFiltros(TipoFiltro.FechaHasta, CType(dteHasta.Value, Date))
            If Not String.IsNullOrEmpty(fsnTvwCategorias.SelectedValue) Then _
                lis.Add(New Filtro(TipoFiltro.Categoria, fsnTvwCategorias.SelectedValue))
            Dim sEst As String = ""
            sEst = sEst & chklstEstado.Items(0).Selected
            sEst = sEst & "_"
            sEst = sEst & chklstEstado.Items(1).Selected
            sEst = sEst & "_"
            sEst = sEst & chklstEstado.Items(2).Selected
            lis.Add(New Filtro(TipoFiltro.Estado, sEst))
            'Comparamos el nuevo valor del filtro con el anterior (si existe)
            'para saber si hay que buscar de nuevo los resultados (dado que la caché no busca todos los resultados sino que filtra por fecha y estado)
            comparaFiltros(TipoFiltro.Estado, sEst)

            'Incluir pedidos abiertos, Catalogo
            If chkPedidosContraAbierto.Checked Or (Not chkPedidosContraAbierto.Checked And Not chkPedidosCatalogo.Checked) Then
                lis.Add(New Filtro(TipoFiltro.IncluirPedidosContraAbierto, "1"))
            End If
            'Comparamos el nuevo valor del filtro con el anterior (si existe)
            'para saber si hay que buscar de nuevo los resultados (dado que la caché no busca todos los resultados sino que filtra por fecha y estado)
            comparaFiltros(TipoFiltro.IncluirPedidosContraAbierto, chkPedidosContraAbierto.Checked)

            If chkPedidosCatalogo.Checked Or (Not chkPedidosContraAbierto.Checked And Not chkPedidosCatalogo.Checked) Then
                lis.Add(New Filtro(TipoFiltro.IncluirPedidosCatalogo, "1"))
            End If
            'Comparamos el nuevo valor del filtro con el anterior (si existe)
            'para saber si hay que buscar de nuevo los resultados (dado que la caché no busca todos los resultados sino que filtra por fecha y estado)
            comparaFiltros(TipoFiltro.IncluirPedidosCatalogo, chkPedidosCatalogo.Checked)

            'Gestionar la visibilidad de filtros de categorías y de pedidos abiertos
            If Acceso.gbUsarPedidosAbiertos Then
                If Not chkPedidosContraAbierto.Checked Then
                    tdPedidosAbiertos.Attributes.Add("style", "display:none")
                End If
            End If
            If Not chkPedidosCatalogo.Checked Then
                tdPedidosCatalogo.Attributes.Add("style", "display:none")
            End If

            'Gestor
            If Not String.IsNullOrEmpty(ddlGestor.SelectedValue) Then _
                lis.Add(New Filtro(TipoFiltro.Gestor, ddlGestor.SelectedValue))
        End If
        FiltrosAnyadidos = lis
    End Sub

    ''' Revisado por: blp. Fecha: 20/12/2011
    ''' <summary>
    ''' Comparamos el nuevo valor del filtro con el anterior (si existe). Sólo se hace para el estado y las fechas q son los valores que se usan al devolver resultados desde bdd.
    ''' Si difiere o no existía previamente, borramos la caché de aprobaciones dado que hay que buscar de nuevo los resultados
    ''' También guardamos en session la info por si se da el caso de que se recupere el dataset desde el método Shared de GenerarDsetPedidos
    ''' </summary>
    ''' <param name="tipoFiltro">Tipo de filtro a comparar</param>
    ''' <param name="valorFiltro">nuevo valor del filtro</param>
    ''' <remarks>Llamada desde ActualizarFiltros. Máx. 0,1 seg.</remarks>
    Private Sub comparaFiltros(ByVal tipoFiltro As TipoFiltro, ByVal valorFiltro As Object)
        Dim guardarEnSession As Boolean = False
        'Antes de que los FiltrosAnyadidos se carguen por primera vez, están vacíos.
        'En ese caso no comparamos con FiltrosAnyadidos dado que es la carga inicial
        'sino con los valores de sesión. Si no hay valores de sesión, se borra la caché
        If FiltrosAnyadidos.Count > 0 Then
            If FiltrosAnyadidos.Exists(Function(ofiltro As Filtro) ofiltro.tipo = tipoFiltro) Then
                Dim valorGuardado As Object
                Select Case tipoFiltro
                    Case Aprobacion.TipoFiltro.FechaDesde, Aprobacion.TipoFiltro.FechaHasta
                        valorGuardado = FiltrosAnyadidos.Find(Function(ofiltro As Filtro) ofiltro.tipo = tipoFiltro).ValorFecha
                        'valorGuardado = CDate(valorGuardado).ToShortDateString
                        'valorFiltro = CDate(valorFiltro).ToShortDateString
                    Case Aprobacion.TipoFiltro.Estado, Aprobacion.TipoFiltro.IncluirPedidosCatalogo, Aprobacion.TipoFiltro.IncluirPedidosContraAbierto, Aprobacion.TipoFiltro.AnyoAbierto, Aprobacion.TipoFiltro.Anio, Aprobacion.TipoFiltro.CestaAbierto, Aprobacion.TipoFiltro.NumPedidoAbierto
                        valorGuardado = FiltrosAnyadidos.Find(Function(ofiltro As Filtro) ofiltro.tipo = tipoFiltro).Valor
                End Select
                If valorGuardado <> valorFiltro Then
                    EliminarCachePedidos(0, Me.Usuario.CodPersona, False, False, True)
                    guardarEnSession = True
                End If
            ElseIf valorFiltro IsNot Nothing Then
                EliminarCachePedidos(0, Me.Usuario.CodPersona, False, False, True)
                guardarEnSession = True
            Else
                guardarEnSession = True
            End If
        Else
            Dim valorSesion As Object = Nothing
            Select Case tipoFiltro
                Case Aprobacion.TipoFiltro.FechaDesde
                    If HttpContext.Current.Session("FecDesde") IsNot Nothing Then
                        valorSesion = HttpContext.Current.Session("FecDesde")
                    Else
                        guardarEnSession = True
                    End If
                Case Aprobacion.TipoFiltro.FechaHasta
                    If HttpContext.Current.Session("FecHasta") IsNot Nothing Then
                        valorSesion = HttpContext.Current.Session("FecHasta")
                    Else
                        guardarEnSession = True
                    End If
                Case Aprobacion.TipoFiltro.Estado
                    If HttpContext.Current.Session("Estado") IsNot Nothing Then
                        valorSesion = HttpContext.Current.Session("Estado")
                    Else
                        guardarEnSession = True
                    End If
                Case Aprobacion.TipoFiltro.IncluirPedidosCatalogo
                    If HttpContext.Current.Session("IncluirPedidosCatalogo") IsNot Nothing Then
                        valorSesion = HttpContext.Current.Session("IncluirPedidosCatalogo")
                    Else
                        guardarEnSession = True
                    End If
                Case Aprobacion.TipoFiltro.IncluirPedidosContraAbierto
                    If HttpContext.Current.Session("IncluirPedidosContraAbierto") IsNot Nothing Then
                        valorSesion = HttpContext.Current.Session("IncluirPedidosContraAbierto")
                    Else
                        guardarEnSession = True
                    End If
                Case Aprobacion.TipoFiltro.AnyoAbierto
                    If HttpContext.Current.Session("AnyoAbierto") IsNot Nothing Then
                        valorSesion = HttpContext.Current.Session("AnyoAbierto")
                    Else
                        guardarEnSession = True
                    End If
                Case Aprobacion.TipoFiltro.CestaAbierto
                    If HttpContext.Current.Session("CestaAbierto") IsNot Nothing Then
                        valorSesion = HttpContext.Current.Session("CestaAbierto")
                    Else
                        guardarEnSession = True
                    End If
                Case Aprobacion.TipoFiltro.NumPedidoAbierto
                    If HttpContext.Current.Session("NumPedidoAbierto") IsNot Nothing Then
                        valorSesion = HttpContext.Current.Session("NumPedidoAbierto")
                    Else
                        guardarEnSession = True
                    End If
            End Select

            If valorSesion IsNot Nothing _
            AndAlso _
            valorSesion <> valorFiltro Then
                EliminarCachePedidos(0, Me.Usuario.CodPersona, False, False, True)
                guardarEnSession = True
                Select Case tipoFiltro
                    Case Aprobacion.TipoFiltro.FechaDesde
                        dteDesde.Value = CType(valorFiltro, Date)
                    Case Aprobacion.TipoFiltro.FechaHasta
                        dteHasta.Value = CType(valorFiltro, Date)
                    Case Aprobacion.TipoFiltro.Estado
                        Dim sAux() As String
                        sAux = Split(valorFiltro, "_")
                        chklstEstado.Items(0).Selected = CBool(sAux(0))
                        chklstEstado.Items(1).Selected = CBool(sAux(1))
                        chklstEstado.Items(2).Selected = CBool(sAux(2))
                    Case Aprobacion.TipoFiltro.IncluirPedidosCatalogo
                        Me.chkPedidosCatalogo.Checked = valorFiltro
                    Case Aprobacion.TipoFiltro.IncluirPedidosContraAbierto
                        Me.chkPedidosContraAbierto.Checked = valorFiltro
                    Case Aprobacion.TipoFiltro.AnyoAbierto
                        Me.ddlAnyoAbierto.SelectedValue = valorFiltro
                    Case Aprobacion.TipoFiltro.CestaAbierto
                        Me.txtCestaAbierto.Text = valorFiltro
                    Case Aprobacion.TipoFiltro.NumPedidoAbierto
                        Me.txtPedidoAbierto.Text = valorFiltro
                End Select
            End If
        End If
        If guardarEnSession Then
            Select Case tipoFiltro
                Case Aprobacion.TipoFiltro.FechaDesde
                    HttpContext.Current.Session.Add("FecDesde", CType(valorFiltro, Date))
                Case Aprobacion.TipoFiltro.FechaHasta
                    HttpContext.Current.Session.Add("FecHasta", CType(valorFiltro, Date))
                Case Aprobacion.TipoFiltro.Estado
                    HttpContext.Current.Session.Add("Estado", valorFiltro)
                Case Aprobacion.TipoFiltro.IncluirPedidosCatalogo
                    HttpContext.Current.Session.Add("IncluirPedidosCatalogo", valorFiltro)
                Case Aprobacion.TipoFiltro.IncluirPedidosContraAbierto
                    HttpContext.Current.Session.Add("IncluirPedidosContraAbierto", valorFiltro)
                Case Aprobacion.TipoFiltro.AnyoAbierto
                    HttpContext.Current.Session.Add("AnyoAbierto", valorFiltro)
                Case Aprobacion.TipoFiltro.CestaAbierto
                    HttpContext.Current.Session.Add("CestaAbierto", valorFiltro)
                Case Aprobacion.TipoFiltro.NumPedidoAbierto
                    HttpContext.Current.Session.Add("NumPedidoAbierto", valorFiltro)
            End Select
        End If
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Dim pds As New PagedDataSource
        pds.AllowPaging = True
        pds.DataSource = DevolverPedidosConFiltro(CampoOrden, SentidoOrdenacion).DefaultView
        If ViewState("PageSize") Is Nothing Then
            pds.PageSize = 10
        Else
            pds.PageSize = CType(ViewState("PageSize"), Integer)
        End If
        pds.CurrentPageIndex = 0
        dlOrdenes.DataSource = pds
        dlOrdenes.DataBind()
        cpeBusqueda.Collapsed = True
        cpeBusqueda.ClientState = "true"
        pnlBuscador.Update()
        pnlGrid.Update()
    End Sub

#End Region
#Region "DataList Pedidos"
    ''' <summary>
    ''' Visualiza o no las tablas  de navegacion. actualiza la paginacion
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo mÃ¡ximo:0,4seg</remarks>
    Private Sub dlOrdenes_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlOrdenes.DataBinding
        Dim pds As PagedDataSource = CType(CType(sender, DataList).DataSource, PagedDataSource)
        ViewState("PageSize") = pds.PageSize
        ViewState("CurrentPage") = pds.CurrentPageIndex
        dlOrdenes.Visible = pds.DataSourceCount > 0
        litNoPedidos.Visible = Not dlOrdenes.Visible
        divTabla1.Visible = dlOrdenes.Visible
        divTabla2.Visible = dlOrdenes.Visible
        If Not _NoBorrarEstadoCollapses Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "BorrarEstadoCollapses", "PeticionesLineas=new Array(); ArrOrdenes=new Array();", True)
            collapseLineas.Value = String.Empty
        End If
    End Sub
    ''' <summary>
    ''' Aprueba una orden de entrega
    ''' </summary>
    ''' <param name="Orden">ID de la orden de entrega</param>
    Friend Sub Aprobar(ByVal Orden As String)
        Dim oOrden As COrden = Me.FSNServer.Get_Object(GetType(COrden))
        Dim keys() As String = Split(Orden, "_")
        Dim motivo As Byte
        Dim idOrden As Long
        Dim idPedido As Long
        Dim TipoPedido As Short
        Dim idEmp As Long
        Dim idInstanciaAprob As String
        Dim BloqueOrigenAprobar As String
        Dim BloqueDestinoAprobar As String
        Dim AccionAprobar As String
        Dim tipoSolicitud As TiposDeDatos.TipoDeSolicitud
        Dim tipoDePedido As TipoDePedido
        motivo = keys(0)
        idOrden = keys(1)
        idPedido = keys(2)
        TipoPedido = keys(3)
        idEmp = keys(4)
        idInstanciaAprob = keys(5)
        BloqueOrigenAprobar = keys(6)
        BloqueDestinoAprobar = keys(7)
        AccionAprobar = keys(8)
        oOrden.Moneda = keys(9)
        tipoDePedido = keys(10)

        If tipoDePedido = tipoDePedido.ContraPedidoAbierto Then
            tipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoContraAbierto
        Else
            tipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoCatalogo
        End If

        oOrden.ID = idOrden
        oOrden.PedidoId = idPedido
        Dim LinModif As DataRowCollection = Nothing
        Dim oError As New CTESError
        If motivo = 1 Then
            'Si el motivo de la aprobacion es 1, es que es por superar la cantidad de la adjudicacion            
            oError = oOrden.AprobarPedido(Me.Usuario.CodPersona, Me.Usuario.Idioma, (Me.Acceso.giPedEmail = ComunicacionPedido.Activo And Me.Acceso.giPedEmailModo = TipoComunicacionPedido.ComAuto), LinModif)
            EliminarCachePedidos(oOrden.ID, idInstanciaAprob)
            mpeConfirm.Hide()
            Select Case oError.NumError
                Case 0

                Case 300    'El pedido ha sido anulado
                    Aviso(Textos(130))
                Case 301    'El pedido ha sido eliminado
                    Aviso(Textos(131))
                Case 302 'El pedido ya ha sido emitido por otro usuario.
                    Aviso(Textos(132))
                Case 303 'El aprobador no tiene importe suficiente para aprobar
                    Aviso(Textos(133) & " " & Textos(65) & ":" & oError.Arg3 & " " & Textos(134) & oError.Arg2 & " " & Textos(135) & oError.Arg1)
                Case 304 'Esta parcialx denegado
                    Aviso(Textos(142))
                    oOrden.NotificarDenegacionParcialOrden(FSNUser.Email)
                Case 305 'Esta  denegado
                    Aviso(Textos(142))
                    oOrden.NotificarDenegacionTotalOrden(FSNUser.Email)
                Case Else
                    Aviso(Textos(136))
            End Select
        ElseIf motivo = 2 Then
            'Realizamos las notificaciones 
            Dim iCom As Short = EmisionPedido.ComprobarComunicacion(idEmp)
            Select Case iCom
                Case 2
                    'Preguntar al emitir pedido si comunicar al emitir
                    ConfirmarNotificar(Textos(176) & " " & Textos(173), idOrden, idPedido, iCom, FSNUser.Email, False)
                Case 4
                    'Preguntar al emitir el pedido si comunicar al integrar 
                    ConfirmarNotificar(Textos(176) & " " & Textos(174), idOrden, idPedido, iCom, FSNUser.Email, False)
            End Select

            'Si el motivo es 2 es que se supero el limite de pedido y se inicio el flujo de aprobacion, al aprobar se pasara a la siguiente etapa del flujo
            GenerarWorkFlowSolicitudPedido(Nothing, Me.Usuario.CodPersona, Me.Usuario.IdiomaCod, idEmp, idPedido, idInstanciaAprob, BloqueOrigenAprobar, BloqueDestinoAprobar, AccionAprobar, oOrden.Moneda, tipoSolicitud)
        End If
        If (motivo = 1 AndAlso oError.NumError = 0) Then
            'Si tenia motivo 1, se ha aprobado el limite de adjudicacion, ahora tocara comprobar si supera el limite de pedido o 
            Dim oEmisionPedidos As CEmisionPedidos = Me.FSNServer.Get_Object(GetType(CEmisionPedidos))
            Dim dsSolicitudesDePedido As DataSet
            dsSolicitudesDePedido = oEmisionPedidos.ValidacionesEmisionPedidoCatalogo(idPedido, Nothing, TipoPedido, idEmp, True)
            If dsSolicitudesDePedido.Tables.Count > 0 AndAlso dsSolicitudesDePedido.Tables(0).Rows.Count > 0 Then
                Aviso(Textos(170))
                GenerarWorkFlowSolicitudPedido(dsSolicitudesDePedido, Me.Usuario.CodPersona, Me.Usuario.IdiomaCod, idEmp, idPedido, , , , , oOrden.Moneda, tipoSolicitud)
            Else
                'La orden esta emitida al proveedor
                'Sumamos el importe comprometido del SM
                Dim obDetallePedido As cDetallePedidos = FSNServer.Get_Object(GetType(cDetallePedidos))
                obDetallePedido.ActualizarImputacion(idOrden)

                'Llamamos al servicio de integración WCF.
                Dim oIntegracion As Integracion = FSNServer.Get_Object(GetType(Integracion))
                oIntegracion.LlamarFSIS(TablasIntegracion.PED_Aprov, oOrden.ID)
                'Realizamos las notificaciones 
                Dim iCom As Short = EmisionPedido.ComprobarComunicacion(idEmp)
                Select Case iCom
                    Case 0
                        'No comunicar                        
                        oOrden.NotificarOrden(FSNUser.Email, OrdenEntregaNotificacion.NoNotificado)
                    Case 1
                        'Comunicar                        
                        oOrden.NotificarOrden(FSNUser.Email, OrdenEntregaNotificacion.Notificado)
                    Case 2
                        'Preguntar al emitir pedido si comunicar al emitir
                        ConfirmarNotificar(Textos(176) & " " & Textos(173), idOrden, idPedido, iCom, FSNUser.Email, True)   'iNotificar = 1                                                 
                    Case 3
                        'Se comunicarÃ¡ automÃ¡ticamente al integrar                        
                        oOrden.NotificarOrden(FSNUser.Email, OrdenEntregaNotificacion.PendienteIntegracion)
                    Case 4
                        'Preguntar al emitir el pedido si comunicar al integrar 
                        ConfirmarNotificar(Textos(176) & " " & Textos(174), idOrden, idPedido, iCom, FSNUser.Email, True)   'iNotificar = 2                         
                End Select
            End If
        End If

        _NoBorrarEstadoCollapses = True
        ActualizarDsetpedidos()
        Dim pds As PagedDataSource = dlOrdenes.DataSource
        pds.DataSource = DevolverPedidosConFiltro(CampoOrden, SentidoOrdenacion).DefaultView
        dlOrdenes.DataSource = pds
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "BorrarArrOrdenes", "ArrOrdenes=new Array();", True)
        dlOrdenes.DataBind()
        pnlGrid.Update()
    End Sub
    ''' <summary>Comprueba si hay que realizar la comunicaciÃ³n del pedido en funciÃ³n de la configuraciÃ³n</summary>    
    ''' <param name="IdOrden">Id orden</param>
    ''' <param name="IdPedido">Id pedido</param>    
    ''' <param name="addressFrom">Dir e-mail</param>  
    ''' <param name="iNotificar">Estado en que quedarÃ¡ la notificaciÃ³n de la orden</param>    
    <System.Web.Services.WebMethod(True), System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub NotificarOrden(ByVal IdOrden As Integer, ByVal IdPedido As Integer, ByVal addressFrom As String, ByVal iNotificar As Short)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oOrden As COrden = FSNServer.Get_Object(GetType(COrden))
        oOrden.ID = IdOrden
        oOrden.PedidoId = IdPedido

        If iNotificar = OrdenEntregaNotificacion.Notificado Or iNotificar = OrdenEntregaNotificacion.PendienteIntegracion Then
            'Hay que realizar la notificaciÃ³n ya            
            oOrden.NotificarOrden(addressFrom, iNotificar)
        Else
            'La notificaciÃ³n se realizarÃ¡ en la etapa correspondiente del workflow            
            'Guardar en la orden la respuesta
            oOrden.TratarComunicacion(iNotificar)
        End If
    End Sub
    ''' <summary>
    ''' Genera los xml para los flujos de las solicitudes de pedido y llama al Web service
    ''' </summary>
    ''' <param name="dsSolicitudes">Dataset con las solicitudes a generar</param>
    ''' <param name="UsuCod">usuario aprovisionador</param>
    ''' <param name="Idioma">idioma del usuario</param>
    ''' <remarks></remarks>
    Private Sub GenerarWorkFlowSolicitudPedido(ByVal dsSolicitudes As DataSet, ByVal UsuCod As String, ByVal Idioma As String, ByVal idEmp As Long, ByVal idPedido As Long, Optional ByVal idInstanciaAprob As Long = 0, Optional ByVal BloqueOrigen As Long = 0, Optional ByVal BloqueDestino As String = "", Optional ByVal Accion As Long = 0, Optional ByVal smon As String = "", Optional ByVal tipoSolicitud As TiposDeDatos.TipoDeSolicitud = -1)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim oSolicitud As Solicitud
        Dim oInstancia As Instancia
        Dim oEmisionPedidos As CEmisionPedidos
        oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        Dim YaRemoveCache As Boolean = False
        Try
            If dsSolicitudes Is Nothing Then
                'Si dsSolicitudes es nothing quiere decir que el motivo es 2(superar limite de pedido) y que se pasa de etapa en el flujo de aprobacion
                ', en este caso solo tendremos esa solicitud
                dsSolicitudes = New DataSet
                dsSolicitudes.Tables.Add("SOLICITUD")
                oInstancia.ID = idInstanciaAprob
                oInstancia.Load(Me.Usuario.IdiomaCod, True)

                oInstancia.DevolverEtapaActual(FSNUser.Idioma, FSNUser.CodPersona, BloqueOrigen)
                If Not oInstancia.AprobacionCompletaCentroSM(FSNUser.CodPersona, BloqueOrigen, Accion) Then
                    Exit Sub
                End If
                Dim oEtapas As DataSet
                oEtapas = oInstancia.DevolverSiguientesEtapas(Accion, FSNUser.CodPersona, FSNUser.Idioma, , , , , tipoSolicitud)

                'Recojo el bloque o bloques de destino de la accion
                If oEtapas.Tables.Count > 0 Then
                    If oEtapas.Tables.Count > 1 Then
                        For Each oRow As DataRow In oEtapas.Tables(0).Rows
                            BloqueDestino = BloqueDestino & oRow.Item("BLOQUE") & " "
                        Next
                    End If
                End If

                With dsSolicitudes.Tables("SOLICITUD").Columns
                    .Add("IMPORTE")
                    .Add("SOLICITUD")
                    .Add("BLOQUE_ORIGEN")
                    .Add("BLOQUE_DESTINO")
                    .Add("ACCION")
                End With
                Dim arrDatosSolicitud(4) As Object
                arrDatosSolicitud(0) = oInstancia.Importe
                arrDatosSolicitud(1) = oInstancia.Solicitud.ID
                arrDatosSolicitud(2) = BloqueOrigen
                arrDatosSolicitud(3) = BloqueDestino
                arrDatosSolicitud(4) = Accion
                dsSolicitudes.Tables(0).Rows.Add(arrDatosSolicitud)
            End If

            For Each dr As DataRow In dsSolicitudes.Tables(dsSolicitudes.Tables.Count - 1).Rows
                oEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))
                oSolicitud.ID = dr("SOLICITUD")
                oSolicitud.Load(Idioma)
                oInstancia.Peticionario = UsuCod
                oInstancia.Solicitud = oSolicitud
                If idInstanciaAprob = 0 Then oInstancia.Create_Prev(idEmp, smon)

                If oInstancia.ID > 0 Then
                    'Ponemos la instancia en proceso
                    oInstancia.Actualizar_En_proceso(1)

                    Dim lIDTiempoProc As Long
                    oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, FSNUser.Cod, BloqueOrigen)

                    If idInstanciaAprob = 0 Then
                        'Solo se actualiza si es aqui donde se crea la instancia y las lineas de pedido no tienen aun el id de la instancia(LINEAS_PEDIDO.INSTANCIA_APROB)
                        For Each drCat As DataRow In dsSolicitudes.Tables(0).Rows
                            oEmisionPedidos.ActualizarLineasPedido_InstanciaSolicitudPedido(drCat, idPedido, oInstancia.ID)
                        Next
                    End If
                    If BloqueOrigen = 0 Then
                        Dim ds As DataSet
                        ds = oEmisionPedidos.ObtenerBloque_Accion(oSolicitud.ID)
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                            BloqueOrigen = ds.Tables(0)(0)("BLOQUE_ORIGEN")
                            BloqueDestino = ds.Tables(0)(0)("BLOQUE_DESTINO")
                            Accion = ds.Tables(0)(0)("ACCION")
                        End If
                    End If

                    Dim sXMLName As String
                    Dim dsXML As New DataSet
                    dsXML.Tables.Add("SOLICITUD")
                    With dsXML.Tables("SOLICITUD").Columns
                        .Add("TIPO_PROCESAMIENTO_XML")
                        .Add("TIPO_DE_SOLICITUD")
                        .Add("COMPLETO")
                        .Add("CODIGOUSUARIO")
                        .Add("INSTANCIA")           '1
                        .Add("SOLICITUD")           '2
                        .Add("USUARIO")             '3
                        .Add("USUARIO_EMAIL")       '4
                        .Add("USUARIO_IDIOMA")      '5
                        .Add("PEDIDO_DIRECTO")      '6
                        .Add("FORMULARIO")          '7
                        .Add("WORKFLOW")            '8
                        .Add("IMPORTE", System.Type.GetType("System.Double")) '9
                        .Add("COMENTALTANOCONF")    '10
                        .Add("ACCION")              '11
                        .Add("IDACCION")            '12
                        .Add("IDACCIONFORM")        '13
                        .Add("GUARDAR")             '14
                        .Add("NOTIFICAR")           '15
                        .Add("TRASLADO_USUARIO")    '16
                        .Add("TRASLADO_PROVEEDOR")  '17
                        .Add("TRASLADO_FECHA")      '18
                        .Add("TRASLADO_COMENTARIO") '19
                        .Add("TRASLADO_PROVEEDOR_CONTACTO") '20
                        .Add("DEVOLUCION_COMENTARIO")       '21
                        .Add("COMENTARIO")          '22
                        .Add("BLOQUE_ORIGEN")       '23
                        .Add("NUEVO_ID_INSTANCIA")  '24
                        .Add("BLOQUES_DESTINO")     '25
                        .Add("ROL_ACTUAL")          '26
                        .Add("PEDIDO")          '31
                        .Add("IDTIEMPOPROC")
                    End With

                    Dim drSolicitud As DataRow
                    drSolicitud = dsXML.Tables("SOLICITUD").NewRow
                    With drSolicitud
                        .Item("TIPO_PROCESAMIENTO_XML") = CInt(TipoProcesamientoXML.FSNWeb)
                        .Item("TIPO_DE_SOLICITUD") = CInt(tipoSolicitud)
                        .Item("COMPLETO") = 1
                        .Item("CODIGOUSUARIO") = FSNUser.Cod
                        .Item("INSTANCIA") = oInstancia.ID
                        .Item("SOLICITUD") = IIf(idInstanciaAprob = 0, oInstancia.Solicitud.ID, "") 'Si es la creacion de la instancia, ponemos la solicitud para que haga el instancia.create
                        .Item("USUARIO") = FSNUser.CodPersona
                        .Item("USUARIO_EMAIL") = DBNullToStr(FSNUser.Email)
                        .Item("USUARIO_IDIOMA") = FSNUser.Idioma.ToString()
                        .Item("PEDIDO_DIRECTO") = 0
                        .Item("FORMULARIO") = oSolicitud.Formulario.Id
                        .Item("WORKFLOW") = oSolicitud.Workflow
                        .Item("IMPORTE") = dr("IMPORTE")
                        .Item("IDACCION") = Accion
                        .Item("IDACCIONFORM") = Accion
                        .Item("GUARDAR") = 1
                        .Item("NOTIFICAR") = 1
                        .Item("BLOQUE_ORIGEN") = BloqueOrigen
                        .Item("BLOQUES_DESTINO") = BloqueDestino
                        .Item("ROL_ACTUAL") = oInstancia.RolActual
                        .Item("PEDIDO") = CInt(tipoSolicitud)
                        .Item("IDTIEMPOPROC") = lIDTiempoProc
                    End With
                    dsXML.Tables("SOLICITUD").Rows.Add(drSolicitud)

                    sXMLName = FSNUser.Cod & "#" & oInstancia.ID & "#" & BloqueOrigen

                    'Cargar XML completo
                    oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=TiempoProcesamiento.InicioDisco)
                    If ConfigurationManager.AppSettings("SERVIDOR_TRATAMIENTO_INSTANCIAS") = "0" Then
                        'Se hace con un StringWriter porque el método GetXml del dataset no incluye el esquema
                        Dim oSW As New System.IO.StringWriter()
                        dsXML.WriteXml(oSW, XmlWriteMode.WriteSchema)

                        Dim oSrvTrataInst As New FSNWebServiceXML.TratamientoInstancias
                        oSrvTrataInst.TransferirXMLEnEpera(oSW.ToString(), sXMLName)
                    Else
                        dsXML.WriteXml(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & "#FSNWEB.xml", XmlWriteMode.WriteSchema)
                        If File.Exists(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml") Then _
                            File.Delete(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml")
                        FileSystem.Rename(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & "#FSNWEB.xml",
                                          ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml")
                    End If

                    If Not YaRemoveCache Then
                        YaRemoveCache = True

                        Dim PeticUser As FSNServer.User = FSNServer.Get_Object(GetType(FSNServer.User))
                        PeticUser.LoadUserData(oInstancia.Peticionario)

                        If PeticUser.ModificarPrecios Then
                            HttpContext.Current.Cache.Remove("DsetArticulos_" & FSNUser.CodPersona)
                        End If
                    End If

                End If
                BloqueOrigen = 0 'Si hay mas de una solicitud, lo pongo a 0 para que busque el bloqueorigen, bloquedestino y accion de cada solicitud
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Deniega una orden de entrega
    ''' </summary>
    ''' <param name="Orden">Datos de la orden</param>
    Friend Sub Denegar(ByVal Orden As String)
        Dim oOrden As COrden = Me.FSNServer.Get_Object(GetType(COrden))

        Dim keys() As String = Split(Orden, "_")
        Dim motivo As Byte
        Dim idOrden As Long
        Dim idPedido As Long
        Dim TipoPedido As Short
        Dim idEmp As Long
        Dim idInstanciaAprob As String
        Dim BloqueOrigenAprobar As String
        Dim BloqueDestinoAprobar As String
        Dim AccionRechazar As String
        Dim tipoSolicitud As TiposDeDatos.TipoDeSolicitud
        Dim tipoDePedido As TipoDePedido
        motivo = keys(0)
        idOrden = keys(1)
        idPedido = keys(2)
        TipoPedido = keys(3)
        idEmp = keys(4)
        idInstanciaAprob = keys(5)
        BloqueOrigenAprobar = keys(6)
        BloqueDestinoAprobar = keys(7)
        AccionRechazar = keys(8)
        oOrden.Moneda = keys(9)
        tipoDePedido = keys(10)

        If tipoDePedido = tipoDePedido.ContraPedidoAbierto Then
            tipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoContraAbierto
        Else
            tipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoCatalogo
        End If

		Dim oError As CTESError
		oOrden.ID = idOrden

		If motivo = 1 Then
			'Si el motivo de la aprobacion es 1, es que es por superar la cantidad de la adjudicacion
			oError = oOrden.DenegarPedido(Me.Usuario.CodPersona, Me.Usuario.Idioma, txtConfirm.Text)
			EliminarCachePedidos(oOrden.ID, idInstanciaAprob)
			mpeConfirm.Hide()
			Select Case oError.NumError
				Case 300 ' El pedido ha sido anulado
					Aviso(Textos(130))
				Case 301 ' El pedido ha sido eliminado
					Aviso(Textos(114))
				Case 302
					Aviso(Textos(141))
				Case 303 'El aprobador no tiene importe suficiente para aprobar
					Aviso(Textos(133) & "<br/>" & Textos(65) & ": " & oError.Arg3 & "<br/>" &
						  Textos(118) & " " & oError.Arg2 & "<br/>" &
						  Textos(119) & " " & oError.Arg1)
				Case Else
					Aviso(Textos(138))
			End Select
		ElseIf motivo = 2 Then
			'Si el motivo es 2 es que se supero el limite de pedido y se inicio el flujo de aprobacion, al aprobar se pasara a la siguiente etapa del flujo
			'Si la accion de aprobar no es null, existe flujo, por tanto generamos xml
			If Not strToInt(AccionRechazar.ToString) = 0 Then
				GenerarWorkFlowSolicitudPedido(Nothing, Me.Usuario.CodPersona, Me.Usuario.IdiomaCod, idEmp, idPedido, idInstanciaAprob, BloqueOrigenAprobar, BloqueDestinoAprobar, AccionRechazar, oOrden.Moneda, tipoSolicitud)
			Else
				oError = oOrden.DenegarPedido(Me.Usuario.CodPersona, Me.Usuario.Idioma, txtConfirm.Text, 1)
				mpeConfirm.Hide()
				Select Case oError.NumError
					Case 300 ' El pedido ha sido anulado
						Aviso(Textos(130))
					Case 301 ' El pedido ha sido eliminado
						Aviso(Textos(114))
					Case 302
						Aviso(Textos(141))
				End Select
			End If
		End If

		_NoBorrarEstadoCollapses = True
        ActualizarDsetpedidos()
        Dim pds As PagedDataSource = dlOrdenes.DataSource
        pds.DataSource = DevolverPedidosConFiltro(CampoOrden, SentidoOrdenacion).DefaultView
        dlOrdenes.DataSource = pds
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "BorrarArrOrdenes", "ArrOrdenes=new Array();", True)
        dlOrdenes.DataBind()
        pnlGrid.Update()
    End Sub
    ''' <summary>
    ''' Actualiza la información del Datalist en caso de que existieran modificaciones
    ''' </summary>
    Friend Sub DetalleLineas()
        _NoBorrarEstadoCollapses = True
        Dim pds As PagedDataSource = dlOrdenes.DataSource
        ActualizarDsetpedidos()
        pds.DataSource = DevolverPedidosConFiltro(CampoOrden, SentidoOrdenacion).DefaultView
        dlOrdenes.DataSource = pds
        dlOrdenes.DataBind()
        pnlGrid.Update()
    End Sub
    ''' <summary>
    ''' Exporta a Excel el contenido de una orden de entrega
    ''' </summary>
    ''' <param name="OrdenId">ID de la orden</param>
    Friend Sub ExportarOrden(ByVal OrdenId As Integer)
        'jim:tarea 3365 -- A la página descargas le vamos a pasar un datatable con la orden seleccionada (si se ha seleccionado alguna) o en caso
        ' contrario de TODAS las ordenes que aparecen segÃºn los criterios de bÃºsqueda y este dtable es el que vamos a guardar en cachÃ© para ser
        ' leido desde Descargas.aspx
        Dim dt As New DataTable
        dt.Columns.Add("ID", GetType(Integer))
        If OrdenId.ToString = "" Then
            Dim ds As DataSet
            ds = ObtenerDataSetReport(OrdenId)
            For Each dr As DataRow In ds.Tables(0).Rows
                dt.Rows.Add(dr("ID"))
            Next
        Else
            dt.Rows.Add(OrdenId)
        End If        
        Me.InsertarEnCache("dsReportEP" & Me.Usuario.Cod, dt)
        mpeConfGenerarInforme.Show()

    End Sub
    ''' <summary>
    ''' Imprime el contenido de una orden de entrega
    ''' </summary>
    ''' <param name="OrdenId">ID de la orden</param>
    Friend Sub ImprimirOrden(ByVal OrdenId As Integer)
        Me.InsertarEnCache("dsReportImpEP" & Me.Usuario.Cod, ObtenerDataSetReport(OrdenId.ToString))
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "ImprimirPed_ViewerSinMenu", "window.open('rptViewerSinMenu.aspx');", True)
    End Sub
    ''' Revisado por: blp. Fecha: 08/03/2012
    ''' <summary>
    ''' Inicializa los controles de las filas del DataList dlOrdenes
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Objeto DataListItemEventArgs que contiene datos del evento. </param>
    ''' <remarks>Se produce después del enlace de datos de un elemento al control DataList. Máx. 0,1 seg.</remarks>
    Private Sub dlOrdenes_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlOrdenes.ItemDataBound
        With e.Item
            Dim fila As DataRowView = CType(.DataItem, DataRowView)
            Select Case .ItemType
                Case ListItemType.Header
                    InitPaginador()
                    CType(.FindControl("litCabEmpresa"), Literal).Text = Textos(91)
                    CType(.FindControl("litCabPedido"), Literal).Text = Textos(103) & "<br/>" & Textos(25)
                    If Acceso.gbOblCodPedido Then
                        Dim oCParametros As FSNServer.CParametros = FSNServer.Get_Object(GetType(FSNServer.CParametros))
                        CType(.FindControl("litCabPedidoERP"), Literal).Text = oCParametros.CargarLiteralParametros(31, Usuario.Idioma)
                    Else
                        CType(.FindControl("tblCabecera"), Table).Rows(0).Cells(3).Visible = False
                    End If
                    If Acceso.gbVerNumeroProveedor Then
                        CType(.FindControl("litCabPedidoProve"), Literal).Text = Textos(104)
                    Else
                        CType(.FindControl("tblCabecera"), Table).Rows(0).Cells(4).Visible = False
                    End If
                    CType(.FindControl("litCabProveedor"), Literal).Text = Textos(23)
                    If Acceso.gbTrasladoAProvERP Then
                        CType(.FindControl("litCabProveedorERP"), Literal).Text = Textos(93)
                    Else
                        CType(.FindControl("tblCabecera"), Table).Rows(0).Cells(6).Visible = False
                    End If
                    CType(.FindControl("litCabAprovisionador"), Literal).Text = Textos(105)
                    CType(.FindControl("litCabReceptor"), Literal).Text = Textos(95)
                    CType(.FindControl("litCabImporte"), Literal).Text = Textos(10)
                    CType(.FindControl("litCabSituacionActual"), Literal).Text = Textos(168)
                    CType(.FindControl("litCabEstado"), Literal).Text = Textos(27)
                    CType(.FindControl("litCabMotivo"), Literal).Text = Textos(82)
                Case ListItemType.Item, ListItemType.AlternatingItem
                    CType(.FindControl("hypVerDetalleOrden"), HyperLink).Text = Textos(169)
                    CType(.FindControl("litFilaPedido"), Literal).Text = IIf(fila.Item("TIPO") = 0, "GS ", "") & _
                        fila.Item("ANYO") & "/" & fila.Item("NUMPEDIDO") & "/" & fila.Item("NUMORDEN") & _
                        "<br/>" & FSNLibrary.FormatDate(fila.Item("FECHA"), Me.Usuario.DateFormat)
                    If Acceso.gbOblCodPedido Then
                        CType(.FindControl("litFilaPedidoERP"), Literal).Text = DBNullToStr(fila.Item("REFERENCIA"))
                    Else
                        CType(.FindControl("tblItem"), Table).Rows(0).Cells(3).Visible = False
                    End If
                    If Not Acceso.gbVerNumeroProveedor Then
                        CType(.FindControl("tblItem"), Table).Rows(0).Cells(3).Visible = False
                    End If
                    CType(.FindControl("fsnlnkFilaProveedor"), FSNWebControls.FSNLinkInfo).Text = _
                        IIf(Me.FSNUser.MostrarCodProve, fila.Item("PROVECOD") & " - ", "") & fila.Item("PROVEDEN")
                    If Acceso.gbTrasladoAProvERP Then
                        CType(.FindControl("litFilaProveedorERP"), Literal).Text = DBNullToStr(fila.Item("COD_PROVE_ERP"))
                    Else
                        CType(.FindControl("tblItem"), Table).Rows(0).Cells(6).Visible = False
                    End If
                    CType(.FindControl("litFilaImporte"), Literal).Text = FSNLibrary.FormatNumber(fila.Item("IMPORTE") * fila.Item("CAMBIO"), Me.Usuario.NumberFormat) & " " & fila.Item("MON")
                    Dim sEstado As String = String.Empty
                    Select Case CType(fila.Item("EST"), CPedido.Estado)
                        Case CPedido.Estado.PendienteAprobar
                            sEstado = Textos(33)
                        Case CPedido.Estado.DenegadoParcialmente
                            sEstado = Textos(34)
                        Case CPedido.Estado.EmitidoAlProveedor
                            sEstado = Textos(35)
                        Case CPedido.Estado.AceptadoPorElProveedor
                            sEstado = Textos(36)
                        Case CPedido.Estado.EnCamino
                            sEstado = Textos(37)
                        Case CPedido.Estado.RecibidoParcialmente
                            sEstado = Textos(38)
                        Case CPedido.Estado.Cerrado
                            sEstado = Textos(39)
                        Case CPedido.Estado.Anulado
                            sEstado = Textos(40)
                        Case CPedido.Estado.Rechazado
                            sEstado = Textos(41)
                        Case CPedido.Estado.Denegado
                            sEstado = Textos(42)
                    End Select
                    CType(.FindControl("litFilaEstado"), Literal).Text = sEstado & "<br/>(" & FSNLibrary.FormatDate(DBNullToSomething(fila.Item("FECEST")), Usuario.DateFormat) & ")"

                    If IsDBNull(fila.Item("ANYO_PED_ABIERTO")) Then
                        CType(.FindControl("litFilaMotivo"), Literal).Text = IIf(fila.Item("MOTIVO") = 1, Textos(84), Textos(85))
                    End If
                    CType(.FindControl("hypFilaSituacionActual"), HyperLink).Text = DBNullToStr(fila.Item("SITUACION_ACTUAL"))
                    CType(.FindControl("hypFilaSituacionActual"), HyperLink).Attributes.Add("onclick", "VerFlujo(" & fila.Item("INSTANCIA_APROB") & ")")
					CType(.FindControl("hypVerDetalleOrden"), HyperLink).Attributes.Add("onclick", "MostrarCargando();" &
							ClientScript.GetPostBackEventReference(_PostBack, "IrDetallePedido#" & fila.Item("ID") &
									"_" & DBNullToStr(fila.Item("INSTANCIA_APROB")) &
									"_" & DBNullToStr(fila.Item("BLOQUE_ORIGEN")) &
									"_" & DBNullToStr(fila.Item("ROL")) &
									"_" & DBNullToStr(fila.Item("MOTIVO")) &
									"_" & DBNullToStr(fila.Item("PEDIDO")) &
									"_" & DBNullToStr(fila.Item("TIPO")) &
									"_" & DBNullToStr(fila.Item("EMPRESA")) &
									"_" & DBNullToStr(fila.Item("ANYO")) &
									"_" & DBNullToStr(fila.Item("NUMPEDIDO")) &
									"_" & DBNullToStr(fila.Item("NUMORDEN"))) &
									"; return false;")
					Dim btnAprobar As ImageButton = CType(.FindControl("btnFilaAprobar"), ImageButton)
                    Dim btnDenegar As ImageButton = CType(.FindControl("btnFilaDenegar"), ImageButton)
                    Select Case CType(fila.Item("EST"), CPedido.Estado)
                        Case CPedido.Estado.PendienteAprobar
                            btnAprobar.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, "aprobar.gif")
                            btnAprobar.ToolTip = Textos(1)
                            btnDenegar.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, "denegar.gif")
                            btnDenegar.ToolTip = Textos(2)
                            Select Case fila.Item("MOTIVO")
                                Case 1 'Por exceder la cantidad adjudicada
                                    btnAprobar.Enabled = True
                                    btnDenegar.Enabled = True
                                    btnAprobar.Visible = True
                                    btnDenegar.Visible = True
                                Case 2 'Por exceder el importe de pedido
                                    If DBNullToInteger(fila.Item("APROBAR")) > 0 Then 'Si la instancia de aprobacion tiene en la etapa en la que se encuentra una accion indicada para aprobar
                                        btnAprobar.Enabled = True
                                        btnAprobar.Visible = True
                                    Else
                                        btnAprobar.Visible = False
                                    End If
                                    If DBNullToInteger(fila.Item("RECHAZAR")) > 0 Then 'Si la instancia de aprobacion tiene en la etapa en la que se encuentra una accion indicada para rechazar
                                        btnDenegar.Enabled = True
                                        btnDenegar.Visible = True
                                    Else
                                        btnDenegar.Visible = False
                                    End If
                            End Select
                        Case CPedido.Estado.Denegado, CPedido.Estado.DenegadoParcialmente
                            btnAprobar.Visible = False
                            btnDenegar.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, "denegado.gif")
                            btnDenegar.Enabled = False
                        Case CPedido.Estado.AceptadoPorElProveedor, CPedido.Estado.EmitidoAlProveedor, CPedido.Estado.EnCamino, CPedido.Estado.Cerrado, CPedido.Estado.RecibidoParcialmente, CPedido.Estado.Rechazado
                            btnAprobar.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, "aprobado.gif")
                            btnAprobar.Enabled = False
                            btnDenegar.Visible = False
                        Case Else
                            btnAprobar.Visible = False
                            btnDenegar.Visible = False
                    End Select
                    btnAprobar.OnClientClick = "if (comprobarlineasmodificadas(" & fila.Item("ID") & ", null, 'aprobarorden')) " & _
                        ClientScript.GetPostBackEventReference(_PostBack, btnAprobar.CommandName & "#" & btnAprobar.CommandArgument) & "; return false;"
                    btnDenegar.OnClientClick = "if (comprobarlineasmodificadas(" & fila.Item("ID") & ", null, 'denegarorden'))" & _
                        ClientScript.GetPostBackEventReference(_PostBack, btnDenegar.CommandName & "#" & btnDenegar.CommandArgument) & "; return false;"
                    Dim btnExportar As ImageButton = .FindControl("btnFilaExcel")
                    btnExportar.OnClientClick = _
                        ClientScript.GetPostBackEventReference(_PostBack, btnExportar.CommandName & "#" & btnExportar.CommandArgument) & "; return false;"
                    Dim btnImprimir As ImageButton = .FindControl("btnFilaImprimir")
                    btnImprimir.OnClientClick = _
                        ClientScript.GetPostBackEventReference(_PostBack, btnImprimir.CommandName & "#" & btnImprimir.CommandArgument) & "; return false;"
                    Dim key() As Object = {fila.Item("ID"), fila.Item("INSTANCIA_APROB")}

                    Dim lineas() As DataRow = DsetPedidos.Tables("ORDENES").Rows.Find(key).GetChildRows("REL_ORDENES_LINEASPEDIDO")
                    Dim FechaActOrden As DateTime = lineas.Max(Function(linea As DataRow) CType(linea.Item("FECACT"), DateTime))
                    ScriptManager.RegisterStartupScript(Me.Page, sender.GetType(), "ScriptOrden" & fila.Item("ID"), _
                            "new Orden(" & fila.Item("ID") & _
                                ",'" & btnAprobar.ClientID & "'" & _
                                ",'" & btnDenegar.ClientID & "'" & _
                                ",'" & fila.Item("COD_ORDEN_APROBAR") & "'" & _
                                ",'" & fila.Item("COD_ORDEN_RECHAZAR") & "'" & _
                                "," & FechaActOrden.ToBinary() & ");", True)
            End Select
        End With
    End Sub
    ''' <summary>
    ''' Muestra el popup de detalle del artículo
    ''' </summary>
    ''' <param name="IdLinea">ID de la línea de pedido</param>
    Friend Sub MostrarDetalleArticulo(ByVal IdLinea As Integer)
        pnlDetalleArticulo.CargarDetalleArticulo(DsetPedidos, IdLinea)
    End Sub
    ''' <summary>
    ''' Comprueba si las líneas de una orden de entrega han sido modificadas después de ser recuperadas
    ''' </summary>
    ''' <param name="OrdenID">ID de la orden de entrega</param>
    ''' <returns>True si alguna línea ha sido modificada, si no False</returns>
    Public Shared Function PedidoModificado(ByVal OrdenID As Integer, ByVal FechaActualizacion As Long) As Boolean
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim oOrden As COrden = oServer.Get_Object(GetType(COrden))
        oOrden.ID = OrdenID
        Return oOrden.Modificada(DateTime.FromBinary(FechaActualizacion))
    End Function
    ''' <summary>
    ''' Comprueba si las líneas de una orden de entrega han sido modificadas después de ser recuperadas
    ''' </summary>
    ''' <param name="OrdenID">ID de la orden de entrega</param>
    ''' <param name="FechaActualizacion">Fecha de actualización de los datos actualmente en caché</param>
    ''' <returns>Un string con el ID de la orden y un 1 ó 0 indicando si la orden ha sido modificada(1) o no(0)</returns>
    ''' <remarks>Método web preparado para ser llamado desde scripts cliente.</remarks>
    <System.Web.Services.WebMethodAttribute(), _
    System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function PedidoModificadoClient(ByVal OrdenID As Integer, ByVal LineaId As Integer, ByVal FechaActualizacion As Long) As String
        If PedidoModificado(OrdenID, FechaActualizacion) Then Return OrdenID & "#" & LineaId & "#1" Else Return OrdenID & "#" & LineaId & "#0"
    End Function
    ''' <summary>
    ''' Comprueba la posibilidad de aprobar la orden de entrega
    ''' </summary>
    ''' <param name="OrdenId">ID de la orden de entrega</param>
    ''' <returns>
    ''' Un objeto CTESError con el resultado de la comprobación
    ''' oTESError.NumError = 0:  No hace falta traspasar
    ''' oTESError.NumError = 1: Habrá que traspasar las lineas de la categoría
    ''' oTESError.NumError = 2: Habrá que emitir la orden de entrega
    ''' oTESError.NumError = 3: La linea ha sido eliminada por otro usuario
    ''' oTESError.NumError = 4: No hay límite superior pero no se puede aprobar
    ''' oTESError.NumError = 5: Hay lineas denegadas por el mismo
    ''' oTESError.NumError = 6: Hay lineas denegadas por otro
    ''' oTESError.NumError = 7: Hay lineas pendientes de otros aprobadores
    ''' oTESError.NumError = 8: El pedido pasará a parcialmente denegado, solicitar confirmación
    ''' </returns>
    Public Shared Function ComprobarAprobacion(ByVal OrdenId As Integer, ByVal motivo As Byte, ByVal instanciaAprob As Long) As CTESError
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim oOrden As COrden = oServer.Get_Object(GetType(COrden))
        oOrden.ID = OrdenId
        Return oOrden.ConfirmarAprobacionPedido(oUsuario.CodPersona, motivo, instanciaAprob)
    End Function
    ''' <summary>
    ''' Comprueba la posibilidad de aprobar la orden de entrega
    ''' </summary>
    ''' <param name="OrdenId">ID de la orden de entrega</param>
    ''' <returns>Un String con el ID de la orden, el código devuelto por la comprobación y los posibles textos variables a mostrar en los mensajes de información</returns>
    ''' <remarks>Función preparada para ser llamada desde scripts de cliente</remarks>
    <System.Web.Services.WebMethodAttribute(), _
    System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function ComprobarAprobacionClient(ByVal OrdenId As Integer, ByVal CodOrdenAprobar As String) As String

        ' oError.NumError = 0:  No hace falta traspasar
        ' oError.NumError = 1: Habrá que traspasar las lineas de la categoría
        ' oError.NumError = 2: Habrá que emitir la orden de entrega
        ' oError.NumError = 3: La linea ha sido eliminada por otro usuario
        ' oError.NumError = 4: No hay límite superior pero no se puede aprobar
        ' oError.NumError = 5: Hay lineas denegadas por el mismo
        ' oError.NumError = 6: Hay lineas denegadas por otro
        ' oError.NumError = 7: Hay lineas pendientes de otros aprobadores
        ' oError.NumError = 8: El pedido pasará a parcialmente denegado, solicitar confirmación
        Dim keys() As String = Split(CodOrdenAprobar, "_")
        Dim motivo As Byte
        Dim idOrden As Long
        Dim idPedido As Long
        Dim TipoPedido As Short
        Dim idEmp As Long
        Dim idInstanciaAprob As String
        Dim BloqueOrigenAprobar As String
        Dim BloqueDestinoAprobar As String
        Dim AccionAprobar As String

        motivo = keys(0)
        idOrden = keys(1)
        idPedido = keys(2)
        TipoPedido = keys(3)
        idEmp = keys(4)
        idInstanciaAprob = keys(5)
        BloqueOrigenAprobar = keys(6)
        BloqueDestinoAprobar = keys(7)
        AccionAprobar = keys(8)

        Dim oError As CTESError = ComprobarAprobacion(OrdenId, motivo, idInstanciaAprob)
        Return OrdenId & "#" & oError.NumError
    End Function
    ''' <summary>
    ''' Comprueba la posibilidad de denegar la orden de entrega
    ''' </summary>
    ''' <param name="OrdenId">ID de la orden de entrega</param>
    ''' <returns>
    ''' Un objeto CTESError con el resultado de la comprobación
    ''' oTESError.NumError = 0:  Nada, no se devuelve 0
    ''' oTESError.NumError = 1: Sube a un nivel superior
    ''' oTESError.NumError = 2: Queda pendiente de aprobar
    ''' oTESError.NumError = 3: El pedido ha sido eliminada por otro usuario
    ''' oTESError.NumError = 4: Queda pendiente y no hay nivel superior
    ''' oTESError.NumError = 5: Queda parcialx denegado, lineas aprobadas por otros
    ''' oTESError.NumError = 6: Pedido denegado del todo
    ''' </returns>
    Public Shared Function ComprobarDenegacion(ByVal OrdenId As Integer, ByVal motivo As Byte, ByVal instanciaAprob As Long) As Integer
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim oOrden As COrden = oServer.Get_Object(GetType(COrden))
        oOrden.ID = OrdenId
        Return oOrden.ConfirmarDenegacionPedido(oUsuario.CodPersona, motivo, instanciaAprob)
    End Function
    ''' <summary>
    ''' Comprueba la posibilidad de denegar la orden de entrega
    ''' </summary>
    ''' <param name="OrdenId">ID de la orden de entrega</param>
    ''' <returns>Un String con el ID de la orden y el código devuelto por la comprobación</returns>
    ''' <remarks>Función preparada para ser llamada desde scripts de cliente</remarks>
    <System.Web.Services.WebMethodAttribute(), _
    System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function ComprobarDenegacionClient(ByVal OrdenId As Integer, ByVal CodOrdenDenegar As String) As String
        Dim keys() As String = Split(CodOrdenDenegar, "_")
        Dim motivo As Byte
        Dim idOrden As Long
        Dim idPedido As Long
        Dim TipoPedido As Short
        Dim idEmp As Long
        Dim idInstanciaAprob As String
        Dim BloqueOrigenAprobar As String
        Dim BloqueDestinoAprobar As String
        Dim AccionRechazar As String

        motivo = keys(0)
        idOrden = keys(1)
        idPedido = keys(2)
        TipoPedido = keys(3)
        idEmp = keys(4)
        idInstanciaAprob = keys(5)
        BloqueOrigenAprobar = keys(6)
        BloqueDestinoAprobar = keys(7)
        AccionRechazar = keys(8)

        Dim resul As Integer = ComprobarDenegacion(OrdenId, motivo, idInstanciaAprob)
        ' 0:  Nada, no se devuelve 0
        ' 1: Sube a un nivel superior
        ' 2: Queda pendiente de aprobar
        ' 3: El pedido ha sido eliminada por otro usuario
        ' 4: Queda pendiente y no hay nivel superior
        ' 5: Queda parcialx denegado, lineas aprobadas por otros
        ' 6: Pedido denegado del todo
        Return OrdenId & "#" & resul.ToString()
    End Function
#End Region
#Region "Paginador"
    ''' <summary>
    ''' Procedimiento en el que configuramos los controles relacionados con la paginación en función del contenido 
    ''' del objeto pds (pageddatasource). 
    ''' </summary>
    ''' <remarks>Llamada desde los controles que refrescan los resultados del accordion de pedidos
    ''' Tiempo máximo: 0 sec.</remarks>
    Public Sub InitPaginador()
        If dlOrdenes.DataSource IsNot Nothing Then

            If dlOrdenes.DataSource.PageCount > 1 Then

                'Actualizamos las combos de paginación
                Dim i As Integer
                ddlPage.Items.Clear()
                ddlPage2.Items.Clear()
                For i = 0 To dlOrdenes.DataSource.PageCount - 1
                    ddlPage.Items.Insert(i, New ListItem(i + 1, i))
                    ddlPage2.Items.Insert(i, New ListItem(i + 1, i))
                Next
                If dlOrdenes.DataSource.CurrentPageIndex >= ddlPage.Items.Count Then dlOrdenes.DataSource.CurrentPageIndex = ddlPage.Items.Count - 1
                ddlPage.Items(dlOrdenes.DataSource.CurrentPageIndex).Selected = True
                ddlPage2.Items(dlOrdenes.DataSource.CurrentPageIndex).Selected = True

                If (dlOrdenes.DataSource.CurrentPageIndex * dlOrdenes.DataSource.PageSize + dlOrdenes.DataSource.PageSize) > dlOrdenes.DataSource.DataSourceCount Then
                    lblResul.Text = CStr(dlOrdenes.DataSource.CurrentPageIndex * dlOrdenes.DataSource.PageSize + 1) & "-" & CStr(dlOrdenes.DataSource.DataSourceCount)
                    lblResul2.Text = CStr(dlOrdenes.DataSource.CurrentPageIndex * dlOrdenes.DataSource.PageSize + 1) & "-" & CStr(dlOrdenes.DataSource.DataSourceCount)
                    lblResulTot.Text = dlOrdenes.DataSource.DataSourceCount
                    lblResulTot2.Text = dlOrdenes.DataSource.DataSourceCount
                Else
                    lblResul.Text = CStr(dlOrdenes.DataSource.CurrentPageIndex * dlOrdenes.DataSource.PageSize + 1) & "-" & CStr(dlOrdenes.DataSource.CurrentPageIndex * dlOrdenes.DataSource.PageSize + dlOrdenes.DataSource.PageSize)
                    lblResul2.Text = CStr(dlOrdenes.DataSource.CurrentPageIndex * dlOrdenes.DataSource.PageSize + 1) & "-" & CStr(dlOrdenes.DataSource.CurrentPageIndex * dlOrdenes.DataSource.PageSize + dlOrdenes.DataSource.PageSize)
                    lblResulTot.Text = dlOrdenes.DataSource.DataSourceCount
                    lblResulTot2.Text = dlOrdenes.DataSource.DataSourceCount
                End If

                lblPagTot.Text = dlOrdenes.DataSource.PageCount
                lblPagTot2.Text = dlOrdenes.DataSource.PageCount

                lblViendo.Visible = True
                lblResul.Visible = True
                lblResulDe.Visible = True
                lblResulTot.Visible = True
                btnFirstPage.Visible = True
                btnPreviousPage.Visible = True
                lblPagina.Visible = True
                ddlPage.Visible = True
                lblDe.Visible = True
                lblPagTot.Visible = True
                btnNextPage.Visible = True
                btnLastPage.Visible = True

                lblViendo2.Visible = True
                lblResul2.Visible = True
                lblResulDe2.Visible = True
                lblResulTot2.Visible = True
                btnFirstPage2.Visible = True
                btnPreviousPage2.Visible = True
                lblPagina2.Visible = True
                ddlPage2.Visible = True
                lblDe2.Visible = True
                lblPagTot2.Visible = True
                btnNextPage2.Visible = True
                btnLastPage2.Visible = True

                If dlOrdenes.DataSource.IsFirstPage Then
                    btnFirstPage.Enabled = False
                    btnFirstPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero_desactivado.gif")
                    btnPreviousPage.Enabled = False
                    btnPreviousPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior_desactivado.gif")

                    btnFirstPage2.Enabled = False
                    btnFirstPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero_desactivado.gif")
                    btnPreviousPage2.Enabled = False
                    btnPreviousPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior_desactivado.gif")
                Else
                    btnFirstPage.Enabled = True
                    btnFirstPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero.gif")
                    btnPreviousPage.Enabled = True
                    btnPreviousPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior.gif")

                    btnFirstPage2.Enabled = True
                    btnFirstPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero.gif")
                    btnPreviousPage2.Enabled = True
                    btnPreviousPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior.gif")
                End If

                If dlOrdenes.DataSource.IsLastPage Then
                    btnNextPage.Enabled = False
                    btnNextPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente_desactivado.gif")
                    btnLastPage.Enabled = False
                    btnLastPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo_desactivado.gif")

                    btnNextPage2.Enabled = False
                    btnNextPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente_desactivado.gif")
                    btnLastPage2.Enabled = False
                    btnLastPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo_desactivado.gif")
                Else
                    btnNextPage.Enabled = True
                    btnNextPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente.gif")
                    btnLastPage.Enabled = True
                    btnLastPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo.gif")

                    btnNextPage2.Enabled = True
                    btnNextPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente.gif")
                    btnLastPage2.Enabled = True
                    btnLastPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo.gif")
                End If
            Else
                lblViendo.Visible = False
                lblResul.Visible = False
                lblResulDe.Visible = False
                lblResulTot.Visible = False
                btnFirstPage.Visible = False
                btnPreviousPage.Visible = False
                lblPagina.Visible = False
                ddlPage.Visible = False
                lblDe.Visible = False
                lblPagTot.Visible = False
                btnNextPage.Visible = False
                btnLastPage.Visible = False

                lblViendo2.Visible = False
                lblResul2.Visible = False
                lblResulDe2.Visible = False
                lblResulTot2.Visible = False
                btnFirstPage2.Visible = False
                btnPreviousPage2.Visible = False
                lblPagina2.Visible = False
                ddlPage2.Visible = False
                lblDe2.Visible = False
                lblPagTot2.Visible = False
                btnNextPage2.Visible = False
                btnLastPage2.Visible = False
            End If

            If dlOrdenes.DataSource.DataSourceCount = 0 Then
                imgbtnExportar.Enabled = False
                imgbtnExportar.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "excel_desactivado.gif")
                imgbtnImprimir.Enabled = False
                imgbtnImprimir.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "imprimir_desactivado.gif")
                lblOrdenacion.Visible = False
                ddlOrdenacion.Visible = False
                imgBtnOrdenacion.Visible = False

                imgbtnExportar2.Enabled = False
                imgbtnExportar2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "excel_desactivado.gif")
                imgbtnImprimir2.Enabled = False
                imgbtnImprimir2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "imprimir_desactivado.gif")
                lblOrdenacion2.Visible = False
                ddlOrdenacion2.Visible = False
                imgBtnOrdenacion2.Visible = False
            Else
                imgbtnExportar.Enabled = True
                imgbtnExportar.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "excel.gif")
                imgbtnImprimir.Enabled = True
                imgbtnImprimir.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "imprimir.gif")
                imgBtnOrdenacion.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, IIf(SentidoOrdenacion = "ASC", "ascendente.gif", "descendente.gif"))

                imgbtnExportar2.Enabled = True
                imgbtnExportar2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "excel.gif")
                imgbtnImprimir2.Enabled = True
                imgbtnImprimir2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "imprimir.gif")
                imgBtnOrdenacion2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, IIf(SentidoOrdenacion = "ASC", "ascendente.gif", "descendente.gif"))
            End If

        End If
    End Sub

    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de primera página
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de primera página
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnFirstPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFirstPage.Click, btnFirstPage2.Click
        If dlOrdenes.DataSource IsNot Nothing Then
            dlOrdenes.DataSource.CurrentPageIndex = 0
            InitPaginador()
            ActualizarControl()
        End If
    End Sub

    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de página anterior
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de página anterior
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnPreviousPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPreviousPage.Click, btnPreviousPage2.Click
        If dlOrdenes.DataSource IsNot Nothing Then
            dlOrdenes.DataSource.CurrentPageIndex = ddlPage.SelectedIndex - 1
            InitPaginador()
            ActualizarControl()
        End If
    End Sub

    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de página siguiente
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de página siguiente
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnNextPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNextPage.Click, btnNextPage2.Click
        If dlOrdenes.DataSource IsNot Nothing Then
            dlOrdenes.DataSource.CurrentPageIndex = ddlPage.SelectedIndex + 1
            InitPaginador()
            ActualizarControl()
        End If
    End Sub

    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de última página
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de última página
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnLastPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLastPage.Click, btnLastPage2.Click
        If dlOrdenes.DataSource IsNot Nothing Then
            dlOrdenes.DataSource.CurrentPageIndex = dlOrdenes.DataSource.PageCount - 1
            InitPaginador()
            ActualizarControl()
        End If
    End Sub

    ''' <summary>
    ''' Evento que se lanza al seleccionar alguna página de la combo de paginación
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: La combo de paginación
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub ddlPage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPage.SelectedIndexChanged, ddlPage2.SelectedIndexChanged
        If dlOrdenes.DataSource IsNot Nothing Then
            dlOrdenes.DataSource.CurrentPageIndex = CType(sender, DropDownList).SelectedIndex
            InitPaginador()
            ActualizarControl()
        End If
    End Sub

    Private Sub imgBtnOrdenacion_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnOrdenacion.Click, imgBtnOrdenacion2.Click
        'SentidoOrdenacion = IIf(SentidoOrdenacion = "ASC", "DESC", "ASC")
        Paginador_OnCambioOrden(CampoOrden, IIf(SentidoOrdenacion = "ASC", "DESC", "ASC"))
    End Sub

    Private Sub imgbtnExportar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnExportar.Click, imgbtnExportar2.Click
        Paginador_Exportar()
    End Sub

    Private Sub imgbtnImprimir_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnImprimir.Click, imgbtnImprimir2.Click
        Paginador_Imprimir()
    End Sub

    Private Sub ddlOrdenacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOrdenacion.SelectedIndexChanged, ddlOrdenacion2.SelectedIndexChanged
        If CampoOrden <> ddlOrdenacion.SelectedValue Then
            Paginador_OnCambioOrden(ddlOrdenacion.SelectedValue, SentidoOrdenacion)
        End If
    End Sub

    Public Sub ActualizarControl()
        dlOrdenes.DataBind()
    End Sub

#End Region
#Region "Ordenación"

    Public Property ListOrden() As ListItemCollection
        Get
            Return ddlOrdenacion.Items
        End Get
        Set(ByVal value As ListItemCollection)
            ddlOrdenacion.Items.Clear()
            ddlOrdenacion2.Items.Clear()
            For Each l As ListItem In value
                ddlOrdenacion.Items.Add(l)
                ddlOrdenacion2.Items.Add(l)
            Next
        End Set
    End Property

    Private Property CampoOrden() As String
        Get
            If String.IsNullOrEmpty(ViewState.Item("CampoOrden")) Then
                Return "FECHA"
            Else
                Return ViewState.Item("CampoOrden")
            End If
        End Get
        Set(ByVal value As String)
            ddlOrdenacion.SelectedValue = value
            ddlOrdenacion2.SelectedValue = value
            ViewState.Item("CampoOrden") = value
        End Set
    End Property

    Private Property SentidoOrdenacion() As String
        Get
            If String.IsNullOrEmpty(ViewState.Item("SentidoOrdenacion")) Then
                Return "DESC"
            Else
                Return ViewState.Item("SentidoOrdenacion")
            End If
        End Get
        Set(ByVal value As String)
            ViewState.Item("SentidoOrdenacion") = value
        End Set
    End Property

    Public Sub Paginador_OnCambioOrden(ByVal Campo As String, ByVal Sentido As String)
        Dim bActualizar As Boolean = False
        If Campo <> CampoOrden Then
            CampoOrden = Campo
            Dim cookie As HttpCookie = Request.Cookies("APROBACION_CRIT_ORD")
            If cookie Is Nothing Then
                cookie = New HttpCookie("APROBACION_CRIT_ORD", CampoOrden)
            Else
                cookie.Value = CampoOrden
            End If
            cookie.Expires = DateTime.Now.AddDays(30)
            Response.AppendCookie(cookie)
            bActualizar = True
        End If
        If Sentido <> SentidoOrdenacion Then
            SentidoOrdenacion = Sentido
            Dim cookie As HttpCookie = Request.Cookies("APROBACION_CRIT_DIREC")
            If cookie Is Nothing Then
                cookie = New HttpCookie("APROBACION_CRIT_DIREC", SentidoOrdenacion)
            Else
                cookie.Value = SentidoOrdenacion
            End If
            cookie.Expires = DateTime.Now.AddDays(30)
            Response.AppendCookie(cookie)
            bActualizar = True
        End If
        If bActualizar Then
            Dim pds As New PagedDataSource
            pds.AllowPaging = True
            pds.DataSource = DevolverPedidosConFiltro(CampoOrden, SentidoOrdenacion).DefaultView
            If ViewState("PageSize") Is Nothing Then
                pds.PageSize = 10
            Else
                pds.PageSize = CType(ViewState("PageSize"), Integer)
            End If
            pds.CurrentPageIndex = 0
            dlOrdenes.DataSource = pds
            dlOrdenes.DataBind()
            pnlGrid.Update()
        End If
    End Sub

#End Region
#Region "Tooltips"

    <System.Web.Services.WebMethodAttribute(), _
        System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function Observaciones(ByVal contextKey As String) As String
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        If oUsuario Is Nothing Then Return Nothing
        Dim dsOrdenes As DataSet
        If HttpContext.Current.Cache("DsetPedidosAprobacion_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString()) Is Nothing Then
            dsOrdenes = GenerarDsetPedidos()
        Else
            dsOrdenes = HttpContext.Current.Cache("DsetPedidosAprobacion_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString())
        End If
        Return dsOrdenes.Tables("ORDENES").Rows.Find(contextKey).Item("OBS")
    End Function

    <System.Web.Services.WebMethodAttribute(), _
    System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function ObservacionesLinea(ByVal contextKey As String) As String
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        If oUsuario Is Nothing Then Return Nothing
        Dim dsOrdenes As DataSet
        If HttpContext.Current.Cache("DsetPedidosAprobacion_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString()) Is Nothing Then
            dsOrdenes = GenerarDsetPedidos()
        Else
            dsOrdenes = HttpContext.Current.Cache("DsetPedidosAprobacion_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString())
        End If
        Return dsOrdenes.Tables("LINEASPEDIDO").Rows.Find(contextKey).Item("OBS")
    End Function

    <System.Web.Services.WebMethodAttribute(), _
    System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function ComentarioAdj(ByVal contextKey As String) As String
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        If oUsuario Is Nothing Then Return Nothing
        If HttpContext.Current.Cache("DsetPedidosAprobacion_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString()) Is Nothing Then _
            Return Nothing
        Dim dsOrdenes As DataSet = _
            HttpContext.Current.Cache("DsetPedidosAprobacion_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString())
        Dim keys() As String = Split(contextKey)
        Dim sComent As String = String.Empty
        Select Case CType(keys(1), TiposDeDatos.Adjunto.Tipo)
            Case TiposDeDatos.Adjunto.Tipo.Favorito_Orden_Entrega
                Return dsOrdenes.Tables("ADJUNTOS_ORDEN").Rows.Find(keys(0)).Item("COMENTARIO")
            Case TiposDeDatos.Adjunto.Tipo.Linea_Pedido
                Return dsOrdenes.Tables("ADJUNTOS_LINEA").Rows.Find(keys(0)).Item("COMENTARIO")
        End Select
        Return sComent
    End Function

    <System.Web.Services.WebMethodAttribute(), _
    System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function DevolverTexto(ByVal contextKey As String) As String
        Return contextKey
    End Function

#End Region
#Region "Panel confirmación"

    ''' <summary>Comprueba si hay que realizar la comunicaciÃ³n del pedido en funciÃ³n de la configuraciÃ³n</summary>    
    ''' <param name="Texto">Texto a mostrar</param>
    ''' <param name="IdOrden">Id orden</param>    
    ''' <param name="IdPedido">Id pedido</param>  
    ''' <param name="iCom"> 0:No comunicar, 1:Comunicar,2: Preguntar al emitir pedido si comunicar al emitir, 3:Se comunicarÃ¡ automÃ¡ticamente al integrar, 4:Preguntar al emitir el pedido si comunicar al integrar</param>
    ''' <param name="addressFrom">Dir. email</param>  
    ''' <param name="bNotificar">Indica si habrÃ¡ que hacer la com unicaciÃ³n si se responde positivamente</param>  
    ''' <remarks>Llamada desde: Aprobar</remarks>
    Private Sub ConfirmarNotificar(ByVal Texto As String, ByVal IdOrden As Integer, ByVal IdPedido As Integer, ByVal iCom As Short, ByVal addressFrom As String, ByVal bNotificar As Boolean)
        lblConfirm.Text = Texto
        txtConfirm.Style.Item("display") = "none"
        lblMaxLength.Style.Item("display") = "none"

        Dim iNotificarAcept As OrdenEntregaNotificacion
        Dim iNotificarCancel As OrdenEntregaNotificacion
        If iCom = 2 Then
            If bNotificar Then
                iNotificarAcept = OrdenEntregaNotificacion.Notificado
            Else
                iNotificarAcept = OrdenEntregaNotificacion.RespuestaComunicarEmitir
                iNotificarCancel = OrdenEntregaNotificacion.RespuestaNoComunicar
            End If
        ElseIf iCom = 4 Then
            If bNotificar Then
                iNotificarAcept = OrdenEntregaNotificacion.PendienteIntegracion
            Else
                iNotificarAcept = OrdenEntregaNotificacion.RespuestaComunicarIntegrar
                iNotificarCancel = OrdenEntregaNotificacion.RespuestaNoComunicar
            End If
        End If
        btnAceptarConfirm.OnClientClick = "NotificarOrden(" & IdOrden & "," & IdPedido & ",'" & addressFrom & "'," & iNotificarAcept & ");$find('" & mpeConfirm.ClientID & "').hide(); return false;"
        If bNotificar Then
            btnCancelarConfirm.OnClientClick = "$find('" & mpeConfirm.ClientID & "').hide(); return false;"
        Else
            btnCancelarConfirm.OnClientClick = "NotificarOrden(" & IdOrden & "," & IdPedido & ",'" & addressFrom & "'," & iNotificarCancel & ");$find('" & mpeConfirm.ClientID & "').hide(); return false;"
        End If
        btnCancelarConfirm.Style.Item("display") = "block"
        mpeConfirm.Show()
        upConfirm.Update()
    End Sub

    Private Sub Aviso(ByVal Texto As String)
        lblConfirm.Text = Texto
        txtConfirm.Style.Item("display") = "none"
        lblMaxLength.Style.Item("display") = "none"
        btnAceptarConfirm.CommandName = ""
        btnAceptarConfirm.OnClientClick = String.Empty
        btnAceptarConfirm.OnClientClick = btnCancelarConfirm.OnClientClick
        btnCancelarConfirm.Style.Item("display") = "none"
        mpeConfirm.Show()
        upConfirm.Update()
    End Sub

    Private Sub Consulta(ByVal Texto As String, ByVal Accion As String, ByVal Argument As String, ByVal CommentMaxLength As String)
        lblConfirm.Text = Texto
        txtConfirm.Text = String.Empty
        txtConfirm.Style.Item("display") = "block"
        lblMaxLength.Style.Item("display") = "block"
        lblMaxLength.Text = Textos(155) & ": " & CommentMaxLength
        btnAceptarConfirm.CommandName = Accion
        btnAceptarConfirm.CommandArgument = Argument
        btnAceptarConfirm.OnClientClick = String.Empty
        btnCancelarConfirm.Style.Item("display") = "block"
        mpeConfirm.Show()
        upConfirm.Update()

    End Sub

    Private Sub btnAceptarConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarConfirm.Click
        _NoBorrarEstadoCollapses = True
        ActualizarDsetpedidos()
        Dim pds As PagedDataSource = dlOrdenes.DataSource
        pds.DataSource = DevolverPedidosConFiltro(CampoOrden, SentidoOrdenacion).DefaultView
        dlOrdenes.DataSource = pds
        dlOrdenes.DataBind()
        pnlGrid.Update()
    End Sub

#End Region
#Region "Informes"

    ''' Revisado por: blp. Fecha: 16/08/2012
    ''' <summary>
    ''' Función en la que devolveremos un report cargado con los datos solicitados
    ''' </summary>
    ''' <returns>El report ya instanciado y con datos</returns>
    ''' <remarks>Llamada desde los botones de imprimir y exportar de Seguimiento.aspx
    ''' Tiempo máximo 1 sec.</remarks>
    Private Function ObtenerDataSetReport(Optional ByVal sOrdenId As String = "") As DataSet

        Dim dtPed As DataTable
        dtPed = DevolverPedidosConFiltro(CampoOrden, SentidoOrdenacion)
        Dim dsLineas As DataSet
        If sOrdenId <> "" Then
            Dim oOrden As COrden = FSNServer.Get_Object(GetType(FSNServer.COrden))
            oOrden.ID = sOrdenId
            dsLineas = oOrden.DevolverLineas(Me.Idioma)
        Else
            Dim ordenesID = From datos In dtPed _
                            Select datos.Item("ID") Distinct.ToList
            If ordenesID.Any Then
                Dim oOrdenes As COrdenes = FSNServer.Get_Object(GetType(FSNServer.COrdenes))
                Dim dtOrtdenesID As DataTable = oOrdenes.CargarOrdenesId(ordenesID)
                dsLineas = oOrdenes.DevolverLineas(Me.Usuario.Idioma, dtOrtdenesID, Acceso.gbAccesoFSSM)
            Else
                dsLineas = New DataSet
            End If
        End If

        Dim query = From Datos In dtPed _
                          Join Lineas In dsLineas.Tables(0) On Lineas.Item("ORDEN") Equals Datos.Item("ID") _
                          Select Lineas

        Dim ds As DataSet = New DataSet
        ds.Tables.Add(dtPed)
        ds.Tables.Add(query.CopyToDataTable())

        ds.Tables(0).TableName = "ORDENES"
        ds.Tables(1).TableName = "LINEASPEDIDO"
        Dim key() As DataColumn = {ds.Tables("ORDENES").Columns("ID")}
        ds.Tables("ORDENES").PrimaryKey = key
        Dim keylp() As DataColumn = {ds.Tables("LINEASPEDIDO").Columns("LINEAID")}
        ds.Tables("LINEASPEDIDO").PrimaryKey = keylp
        keylp(0) = ds.Tables("LINEASPEDIDO").Columns("ORDEN")
        ds.Relations.Add("REL_ORDENES_LINEASPEDIDO", key, keylp, False)

        Return ds
    End Function

    Public Sub Paginador_Imprimir()
        Me.InsertarEnCache("dsReportImpEP" & Me.Usuario.Cod, ObtenerDataSetReport())
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Imprimir_ViewerSinMenu", "window.open('rptViewerSinMenu.aspx');", True)
    End Sub

    ''' <summary>
    ''' Procedimienro que muestra el panel de confirmacion para generar el informe
    ''' </summary>
    ''' <remarks>
    ''' LLamada desdE: Automática, cuando se pincha en el paginador para generar el informe
    ''' Tiempo máximo: 1 seg</remarks>
    Public Sub Paginador_Exportar()
        prepararOrdenesExportacion(ObtenerDataSetReport())
        mpeConfGenerarInforme.Show()
        'ScriptManager.RegisterStartupScript(Me, Me.GetType, "Exportar_ViewerSinMenu", "window.open('Descarga.aspx' ,'_blank','height=300,width=300,menubar=no,scrollbars=no,resizable=no,toolbar=no,addressbar=no');", True)
    End Sub

    Private Sub prepararOrdenesExportacion(ByVal ds As DataSet)
        'jim:tarea 3365 -- A la página descargas le vamos a pasar un datatable con la orden seleccionada (si se ha seleccionado alguna) o en caso
        ' contrario de TODAS las ordenes que aparecen según los criterios de búsqueda y este dtable es el que vamos a guardar en caché para ser
        ' leido desde Descargas.aspx
        Dim dt As New DataTable
        dt.Columns.Add("ID", GetType(Integer))
        For Each dr As DataRow In ds.Tables(0).Rows
            dt.Rows.Add(dr("ID"))
        Next
        Me.InsertarEnCache("dsReportEP" & Me.Usuario.Cod, dt)
    End Sub

    ''' <summary>
    ''' Evento que se genera cuando se pincha el botón de cancelar en la confirmación de generar el informe
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento, en este caso el botón de cancelar</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automática, cuando se clica el botón de cancelar
    ''' tiempo máximo: 0 seg</remarks>
    Protected Sub btnCancelarGenInforme_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnCancelarGenerarInforme.Click
        mpeConfGenerarInforme.Hide()
    End Sub

#End Region
#Region "Unidades"
    ''' <summary>
    ''' Propiedad que recupera el conjunto de las unidades de Pedido para su posterior uso en la página
    ''' </summary>
    Private ReadOnly Property TodasLasUnidades() As CUnidadesPedido
        Get
            Dim oUnidadesPedido As CUnidadesPedido = Me.FSNServer.Get_Object(GetType(FSNServer.CUnidadesPedido))
            If HttpContext.Current.Cache("TodasLasUnidades" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                oUnidadesPedido.CargarTodasLasUnidades(Me.Usuario.Idioma)
                Me.InsertarEnCache("TodasLasUnidades" & Me.Usuario.Idioma.ToString(), oUnidadesPedido)
            Else
                oUnidadesPedido = HttpContext.Current.Cache("TodasLasUnidades" & Me.Usuario.Idioma.ToString())
            End If
            Return oUnidadesPedido
        End Get
    End Property
#End Region
    ''' <summary>
    ''' Determina si se muestra el control en función de la propiedad AuthorizeWebPart y los permisos del usuario
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">WebPartAuthorizationEventArgs que contiene los datos del evento.</param>
    ''' <remarks>Se produce cuando se llama al método IsAuthorized para determinar si se puede agregar un control WebPart o un control de servidor a una página.</remarks>
    Private Sub WebPartManager1_AuthorizeWebPart(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.WebParts.WebPartAuthorizationEventArgs) Handles WebPartManager1.AuthorizeWebPart
        e.IsAuthorized = AutorizacionWebparts(e.AuthorizationFilter)
    End Sub
End Class