﻿Imports Fullstep.FSNServer
Imports Fullstep.FSNLibrary
''' <summary>
''' Pagina que hereda de Descarga.aspx para reutilizar parte de sus métodos, de configuración de columnas
''' </summary>
''' <remarks></remarks>
Public Class InformeExcelOrdenes
    Inherits FSNPage

    Protected dtOrdenesId As New DataTable
    Private iVerBajasOrdenes As Integer = 0
    Private Sub InformeExcelOrdenes_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not HttpContext.Current.Cache("dsReportEP" & Usuario.Cod) Is Nothing Then
            'Tomamos de la caché el listado con las ordenes a mostrar en nuestro excel
            dtOrdenesId = CType(HttpContext.Current.Cache("dsReportEP" & Usuario.Cod), DataTable)
            iVerBajasOrdenes = CType(HttpContext.Current.Cache("VerOrdenesdeBaja" & Usuario.Cod), Integer)
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
            GenerarExcel(Response)
            HttpContext.Current.Cache.Remove("dsReportEP" & Usuario.Cod)
            HttpContext.Current.Cache.Remove("VerOrdenesdeBaja" & Usuario.Cod)
        End If
    End Sub
    '''Revisado por:jim. Fecha: 11/08/14
    ''' <summary>
    ''' Genera un excel con varias pestañas con la información de la orden u ordenes solicitadas y sus detalles correspondientes
    ''' en diferentes pestañas
    ''' </summary>
    ''' <param name="response">objeto HttpResponse</param>
    ''' <remarks>Llamada desde Load. Máx.: depende del número de registros.</remarks>
    Public Sub GenerarExcel(ByVal response As HttpResponse)
        'obtener las ordenes a mostrar
        Dim oOrdenes As COrdenes
        Dim dsOrdenes As New DataSet
        Dim orden As Integer

        oOrdenes = FSNServer.Get_Object(GetType(FSNServer.COrdenes))

        'Cargamos el dataset con las ordenes contenidads en dtOrdenesId y sus detalles
        dsOrdenes = oOrdenes.BuscarDetalleOrdenes(Usuario.Idioma, dtOrdenesId, Acceso.gbAccesoFSSM, iVerBajasOrdenes)

        'rellenar atributos de orden y linea
        orden = 0
        For Each dr As DataRow In dsOrdenes.Tables(0).Rows
            If dr("ORDENID") <> orden Then
                rellenarAtributos(dr("ORDENID"), dsOrdenes.Tables(0))
                orden = dr("ORDENID")
            End If
            rellenarAtributosLinea(dr("LINEAID"), dsOrdenes.Tables(0))
        Next

        'Rellenar columnas relativas a SM
        If Acceso.gbAccesoFSSM Then
            rellenarDatosSM(dsOrdenes)
            'borramos el datatable de sm ya que no queremos que se muestre en el excel
            dsOrdenes.Tables.RemoveAt(7)
        End If

        formatearPlanEntrega(dsOrdenes)

        traducirDataset(dsOrdenes)

        'eliminar del dataset aquellas tablas sin datos
        Dim lst As New List(Of String)
        For Each dt As DataTable In dsOrdenes.Tables
            If dt.Rows.Count = 0 Then
                lst.Add(dt.TableName)
            End If
        Next
        For Each tablename As String In lst
            dsOrdenes.Tables.Remove(tablename)
        Next

        Dim sExcelName = ReemplazaTexto(Textos(0) & "_" & Me.FSNUser.Nombre & "_" & Now().ToString & ".xlsx")
        Dim sXlsRuta As String = ConfigurationManager.AppSettings("temp") & "\" & sExcelName
        CreateExcelFile.CreateExcelDocument(dsOrdenes, sXlsRuta)

        Dim input As System.IO.FileStream = New System.IO.FileStream(sXlsRuta, System.IO.FileMode.Open, System.IO.FileAccess.Read)
        Dim byteBuffer() As Byte
        ReDim byteBuffer(input.Length - 1)
        input.Read(byteBuffer, 0, byteBuffer.Length)
        input.Close()

        Me.Response.ClearContent()
        Me.Response.ClearHeaders()
        Me.Response.Clear()
        Call Me.Response.AddHeader("Content-Type", "application/octet-stream")
        Call Me.Response.AddHeader("Content-Disposition", "attachment;filename=""" & sExcelName & """")
        Call Me.Response.AddHeader("Content-Length", byteBuffer.Length.ToString())
        Call Me.Response.BinaryWrite(byteBuffer)
        IO.File.Delete(sXlsRuta)
        Call Me.Response.End()
    End Sub
    Private Sub rellenarAtributos(ByVal idOrden As String, dTable As DataTable)
        Dim oAtributos As CAtributosPedido = FSNServer.Get_Object(GetType(FSNServer.CAtributosPedido))

        For Each oAtributo As CAtributoPedido In oAtributos.CargarAtributosOrden(idOrden)
            If Not dTable.Columns.Contains("CP_" & oAtributo.Codigo & "_" & oAtributo.Denominacion) Then
                Dim index As Integer = dTable.Columns("LINEAID").Ordinal
                'introducimos la columna inmediatamente antes de lineaid
                dTable.Columns.Add("CP_" & oAtributo.Codigo & "_" & oAtributo.Denominacion).SetOrdinal(index)
            End If
            For Each dr As DataRow In dTable.Select("ORDENID=" & idOrden)
                dr("CP_" & oAtributo.Codigo & "_" & oAtributo.Denominacion) = oAtributo.Valor
            Next
        Next
    End Sub
    Private Sub rellenarAtributosLinea(ByVal idLinea As String, dTable As DataTable)

        Dim oAtributos As CAtributosPedido = FSNServer.Get_Object(GetType(FSNServer.CAtributosPedido))

        For Each oAtributo As CAtributoPedido In oAtributos.CargarAtributosLineaOrden(idLinea)
            If Not dTable.Columns.Contains("LP_" & oAtributo.Codigo & "_" & oAtributo.Denominacion) Then
                dTable.Columns.Add("LP_" & oAtributo.Codigo & "_" & oAtributo.Denominacion)
            End If
            Dim dr As DataRow = dTable.Select("LINEAID=" & idLinea)(0)
            dr("LP_" & oAtributo.Codigo & "_" & oAtributo.Denominacion) = oAtributo.Valor
        Next
    End Sub
    Private Sub traducirDataset(dsOrdenes As DataSet)
        'TRADUCION DE TABLAS
        dsOrdenes.Tables(0).TableName = Textos(269) 'Datos cabecera/lineas
        dsOrdenes.Tables(1).TableName = Textos(270) 'Costes descuentos cabecera
        dsOrdenes.Tables(2).TableName = Textos(277) 'costes descuentos linea
        dsOrdenes.Tables(3).TableName = Textos(271) 'impuestos costes cabecera
        dsOrdenes.Tables(4).TableName = Textos(272) 'impuestos lineas
        dsOrdenes.Tables(5).TableName = Textos(273) 'impuestos costes lineas
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
        dsOrdenes.Tables(6).TableName = Textos(320) 'plan de entrega
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento

        'tabla 0 
        darFormatoATabla(dsOrdenes.Tables(0))
        agregarEditarColumnas(dsOrdenes.Tables(0))
        QuitarColumnas(dsOrdenes.Tables(0))

        With dsOrdenes.Tables(0)
            .Columns("OECOSTES").ColumnName = Textos(283)
            .Columns("OEDESCUENTOS").ColumnName = Textos(284)
            .Columns("LPCOSTES").ColumnName = Textos(285)
            .Columns("LPDESCUENTOS").ColumnName = Textos(286)
        End With
        'tabla 1
        codificarOperacion(dsOrdenes.Tables(1))
        With dsOrdenes.Tables(1) 'COSTES DTO CABECERA
            .Columns("ANYO").ColumnName = Textos(23)
            .Columns("CESTA").ColumnName = Textos(24)
            .Columns("PEDIDO").ColumnName = Textos(25)
            .Columns("CODCOSTE").ColumnName = Textos(280)
            .Columns("DENCOSTE").ColumnName = Textos(281)
            .Columns("OPERACION").ColumnName = Textos(274)
            .Columns("VALOR").ColumnName = Textos(275)
            .Columns("IMPORTE").ColumnName = Textos(276)
            .Columns("MONEDA").ColumnName = Textos(278)
            .Columns.Remove("TIPO")
        End With

        codificarOperacion(dsOrdenes.Tables(2))
        With dsOrdenes.Tables(2) 'Costes descuentos LINEA
            .Columns("ANYO").ColumnName = Textos(23)
            .Columns("CESTA").ColumnName = Textos(24)
            .Columns("PEDIDO").ColumnName = Textos(25)
            .Columns("NUM").ColumnName = Textos(242)
            .Columns("CODART").ColumnName = Textos(282)
            .Columns("CODCOSTE").ColumnName = Textos(280)
            .Columns("DENCOSTE").ColumnName = Textos(281)
            .Columns("OPERACION").ColumnName = Textos(274)
            .Columns("VALOR").ColumnName = Textos(275)
            .Columns("IMPORTE").ColumnName = Textos(276)
            .Columns("MONEDA").ColumnName = Textos(278)
            .Columns.Remove("TIPO")
        End With

        With dsOrdenes.Tables(3) 'IMPUESTO Costes CABECERA
            .Columns("ANYO").ColumnName = Textos(23)
            .Columns("CESTA").ColumnName = Textos(24)
            .Columns("PEDIDO").ColumnName = Textos(25)
            .Columns("CODCOSTE").ColumnName = Textos(280)
            .Columns("DENCOSTE").ColumnName = Textos(281)
            .Columns("IMPUESTO").ColumnName = Textos(279)
            .Columns("VALOR").ColumnName = Textos(275)
        End With

        With dsOrdenes.Tables(4) 'Impeustso lineas
            .Columns("ANYO").ColumnName = Textos(23)
            .Columns("CESTA").ColumnName = Textos(24)
            .Columns("PEDIDO").ColumnName = Textos(25)
            .Columns("NUM").ColumnName = Textos(242)
            .Columns("CODART").ColumnName = Textos(282)
            .Columns("VALOR").ColumnName = Textos(275)
            .Columns("IMPUESTO").ColumnName = Textos(279)
        End With

        codificarOperacion(dsOrdenes.Tables(5))
        With dsOrdenes.Tables(5) 'impuestos lineas coste
            .Columns("ANYO").ColumnName = Textos(23)
            .Columns("CESTA").ColumnName = Textos(24)
            .Columns("PEDIDO").ColumnName = Textos(25)
            .Columns("NUM").ColumnName = Textos(242)
            .Columns("CODART").ColumnName = Textos(282)
            .Columns("CODCOSTE").ColumnName = Textos(280)
            .Columns("DENCOSTE").ColumnName = Textos(281)
            .Columns("VALOR").ColumnName = Textos(275)
            .Columns("IMPUESTO").ColumnName = Textos(279)
            .Columns("OPERACION").ColumnName = Textos(274)
            .Columns.Remove("TIPO")
        End With

        With dsOrdenes.Tables(6) 'plan de entrega
            .Columns("NUMPEDIDOFS").ColumnName = Textos(186)
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_RecepcionPedidos
            .Columns("NUM").ColumnName = Textos(189)
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
            .Columns("CODART").ColumnName = Textos(41)
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
            .Columns("FECHA").ColumnName = Textos(319)
            If .Columns.Contains("IMPORTE") Then .Columns("IMPORTE").ColumnName = Textos(12)
            If .Columns.Contains("CANTIDAD") Then .Columns("CANTIDAD").ColumnName = Textos(11)
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
            .Columns("ALBARAN").ColumnName = Textos(156)
        End With
    End Sub
    Private Sub codificarOperacion(dt As DataTable)
        For Each dr As DataRow In dt.Rows
            dr("OPERACION") = getSimbolo(dr("TIPO"), dr("OPERACION"))
        Next
    End Sub
    Private Function getSimbolo(tipo As Integer, op As Integer) As String
        Dim simbolo As String
        If tipo = 0 Then
            simbolo = "+"
        Else
            simbolo = "-"
        End If
        If op = 1 Then
            simbolo &= "%"
        End If
        Return simbolo
    End Function
    ''' <summary>
    ''' Reemplazamos en el datatable los estados (que son valores numéricos), por su descripción
    ''' Ponemos formato a las fechas y a los importes
    ''' </summary>
    ''' <param name="dt">datatable de origen</param>
    ''' <returns>datatable con los estados cambiados</returns>
    ''' <remarks>Llamadas desde ExportarExcel. Máx 0,1 seg.</remarks>
    Protected Function darFormatoATabla(ByVal dt As DataTable) As DataTable
        Dim oCol As New DataColumn
        oCol.ColumnName = "sEST"
        oCol.DataType = GetType(String)
        dt.Columns.Add(oCol)

        Dim hayIntegracionPedidos As Boolean = CBool(Session("HayIntegracionPedidos"))
        If hayIntegracionPedidos Then
            oCol = New DataColumn
            oCol.ColumnName = "sEST_INT"
            oCol.DataType = GetType(String)
            dt.Columns.Add(oCol)
        End If

        For Each oRow As DataRow In dt.Rows
            'reemplazar campo EST por descripción del estado
            If oRow("BAJA_LOG") = 0 Then
                Dim estado As Integer = oRow("EST")
                oRow("sEST") = reemplazarEstado(estado)
            Else
                oRow("sEST") = Textos(325)
            End If

            If hayIntegracionPedidos AndAlso Not oRow.IsNull("EST_INT") Then oRow("sEST_INT") = ReemplazarEstadoIntegracion(oRow("EST_INT"), oRow("PED_ERP"))

            If oRow("IMPORTEORDEN") IsNot Nothing AndAlso Not IsDBNull(oRow("IMPORTEORDEN")) Then _
                oRow("IMPORTEORDEN") = FSNLibrary.FormatNumber(oRow("IMPORTEORDEN"), Me.Usuario.NumberFormat)
            If oRow("IMPORTELINEA") IsNot Nothing AndAlso Not IsDBNull(oRow("IMPORTELINEA")) Then _
            oRow("IMPORTELINEA") = FSNLibrary.FormatNumber(oRow("IMPORTELINEA"), Me.Usuario.NumberFormat)
            oRow("FECHA") = FSNLibrary.FormatDate(oRow("FECHA"), Me.Usuario.DateFormat)
            If Not IsDBNull(oRow("FECENTREGA")) Then _
                oRow("FECENTREGA") = FSNLibrary.FormatDate(oRow("FECENTREGA"), Me.Usuario.DateFormat)
            If Not IsDBNull(oRow("FECENTREGAPROVE")) Then _
                oRow("FECENTREGAPROVE") = FSNLibrary.FormatDate(oRow("FECENTREGAPROVE"), Me.Usuario.DateFormat)
        Next
        Dim iposicion As Integer = dt.Columns("EST").Ordinal
        dt.Columns.Remove("EST")
        dt.Columns("sEST").SetOrdinal(iposicion)
        dt.Columns("sEST").ColumnName = "EST"

        iposicion = dt.Columns("EST_INT").Ordinal
        dt.Columns.Remove("EST_INT")
        If hayIntegracionPedidos Then
            dt.Columns("sEST_INT").SetOrdinal(iposicion)
            dt.Columns("sEST_INT").ColumnName = "EST_INT"
        End If

        'Si el dataset recibido no tiene en la tabla ordenes el campo DIRECCIONENVIOFRA, lo añadimos VACIO
        ' El valor de bMostrarDireccion será false cuando no hay permisos para mostrar la direccion o 
        ' cuando la solicitud del informe venga de una página que no proporciona el campo Direccion Envio Factura.
        If Me.Acceso.gbDirEnvFacObl Then
            If dt.Rows.Count > 0 And dt.Columns.Item("DIRECCIONENVIOFRA") Is Nothing Then
                dt.Columns.Add("DIRECCIONENVIOFRA", System.Type.GetType("System.String"))
            End If
        Else
            'Si Me.Acceso.gbDirEnvFacObl ES FALSE en la tabla ordenes el campo DIRECCIONENVIOFRA, lo quitamos
            If dt.Rows.Count > 0 Then
                If dt.Columns.Item("DIRECCIONENVIOFRA") IsNot Nothing Then
                    Dim pos = dt.Columns("DIRECCIONENVIOFRA").Ordinal
                    dt.Columns.Remove("DIRECCIONENVIOFRA")
                    'lo ponemos vacío en la posición que se encontraba
                    dt.Columns.Add("DIRECCIONENVIOFRA", System.Type.GetType("System.String")).SetOrdinal(pos)
                End If
            End If
        End If
        Return dt
    End Function
    ''' <summary>
    ''' Quitamos del datatable a exportar las columnas que no queremos que se vean
    ''' </summary>
    ''' <param name="dt">datatable de origen</param>
    ''' <returns>datatable sin las columnas</returns>
    ''' <remarks>Llamadas desde ExportarExcel. Máx 0,1 seg.</remarks>
    Protected Function QuitarColumnas(ByVal dt As DataTable) As DataTable
        dt.PrimaryKey = Nothing
        If dt.Columns("ORDENID") IsNot Nothing Then
            dt.Columns.Remove("ORDENID")
        End If
        If dt.Columns("LINEAID") IsNot Nothing Then
            dt.Columns.Remove("LINEAID")
        End If
        If dt.Columns("CCDEN") IsNot Nothing Then
            dt.Columns.Remove("CCDEN")
        End If
        If dt.Columns("NIVEL_DEN") IsNot Nothing Then
            dt.Columns.Remove("NIVEL_DEN")
        End If
        If Not Acceso.gbOblCodPedido Then
            If dt.Columns("NUMPEDIDOERP") IsNot Nothing Then
                dt.Columns.Remove("NUMPEDIDOERP")
            End If
        End If
        If dt.Columns("PED_ERP") IsNot Nothing Then
            dt.Columns.Remove("PED_ERP")
        End If
        Return dt
    End Function
    ''' <summary>
    ''' Contiene las columnas y textos que usaremos
    ''' Cada vez que se modifiquen las columnas a mostrar, hay que añadirlas o quitarlas de aquí
    ''' </summary>
    Protected ReadOnly Property ColumnasTextos As List(Of String())
        Get
            Dim oColumnas As New List(Of String())
            oColumnas.Add({"DENEMPRESA", Textos("96")}) 'Empresa (denominación)
            oColumnas.Add({"NUMPEDIDOFS", Textos("186")}) 'Número de pedido Fullstep
            Dim oCParametros As FSNServer.CParametros = FSNServer.Get_Object(GetType(FSNServer.CParametros))
            If Acceso.gbOblCodPedido Then
                oColumnas.Add({"NUMPEDIDOERP", oCParametros.CargarLiteralParametros(TiposDeDatos.LiteralesParametros.PedidoERP, FSNUser.Idioma).ToString}) 'Número de pedido ERP
            End If
            oColumnas.Add({"FECHA", Textos("140")}) 'Fecha alta
            oColumnas.Add({"APROV", Textos("123")}) 'Código aprovisionador
            oColumnas.Add({"PROVECOD", Textos("26")}) 'Código del proveedor
            oColumnas.Add({"PROVEDEN", Textos("293")}) 'Denominación proveedor
            oColumnas.Add({"COD_PROVE_ERP", Textos("98")}) 'Código del proveedor en el ERP
            oColumnas.Add({"IMPORTEORDEN", Textos("67")}) 'Importe
            oColumnas.Add({"TIPOPEDIDO_DEN", Textos("175")}) 'Tipo de pedido
            oColumnas.Add({"EST", Textos("44")}) 'Estado
            If CBool(Session("HayIntegracionPedidos")) Then oColumnas.Add({"EST_INT", Textos("330")}) 'Estado integración
            oColumnas.Add({"DIRECCIONENVIOFRA", Textos("182")}) 'Dirección de envío de factura
            oColumnas.Add({"RECEPTOR", Textos("97")}) 'Código del receptor
            oColumnas.Add({"OBS", Textos("90")}) 'Observaciones de cabecera
            oColumnas.Add({"NUM", Textos("242")}) 'Núm.
            oColumnas.Add({"ART_INT", Textos("41")}) 'Código del artículo
            oColumnas.Add({"ART_DEN", Textos("42")}) 'Denominación del artículo
            oColumnas.Add({"CANTIDAD", Textos("193")}) 'Cantidad pedida
            oColumnas.Add({"PRECIOUNITARIO", Textos("247")}) 'Precio unitario
            oColumnas.Add({"UNIDAD", Textos("63")}) 'Unidad de medida
            oColumnas.Add({"IMPORTELINEA", Textos("246")}) 'Importe línea
            oColumnas.Add({"MON", Textos("248")}) 'Código de la moneda
            oColumnas.Add({"FECENTREGA", Textos("249")}) 'Fecha entrega solicitada
            oColumnas.Add({"FECENTREGAPROVE", Textos("250")}) 'Fecha entrega prevista
            oColumnas.Add({"CCUON", Textos("142")}) 'Código y denominación del centro de coste
            oColumnas.Add({"OBSLINEA", Textos("291")})
            Return oColumnas
        End Get
    End Property
    ''' Revisado por: blp. Fecha: 20/12/2012
    ''' <summary>
    ''' Recibe el estado en id y devuelve el texto que le corresponde en el idioma del usuario
    ''' </summary>
    ''' <param name="IdEstado">Id del estado</param>
    ''' <returns>el texto del estado en el idioma del usuario</returns>
    ''' <remarks>Llamada desde ExportaraExcel. Maximo 0,1 seg.</remarks>
    Private Function reemplazarEstado(ByVal IdEstado As Integer) As String
        Select Case CType(IdEstado, CPedido.Estado)
            Case CPedido.Estado.PendienteAprobar
                Return Textos(31)
            Case CPedido.Estado.DenegadoParcialmente
                Return Textos(32)
            Case CPedido.Estado.EmitidoAlProveedor
                Return Textos(33)
            Case CPedido.Estado.AceptadoPorElProveedor
                Return Textos(34)
            Case CPedido.Estado.EnCamino
                Return Textos(35)
            Case CPedido.Estado.RecibidoParcialmente
                Return Textos(36)
            Case CPedido.Estado.Cerrado
                Return Textos(37)
            Case CPedido.Estado.Anulado
                Return Textos(38)
            Case CPedido.Estado.Rechazado
                Return Textos(39)
            Case CPedido.Estado.Denegado
                Return Textos(40)
            Case Else
                Return String.Empty
        End Select
    End Function
    ''' <summary>Recibe el estado en id y devuelve el texto que le corresponde en el idioma del usuario</summary>
    ''' <param name="IdEstadoInt">Id del estado</param>
    ''' <returns>el texto del estado en el idioma del usuario</returns>   
    Private Function ReemplazarEstadoIntegracion(ByVal IdEstadoInt As Integer, ByVal bPedERP As Boolean) As String
        Dim sEstado As String = String.Empty
        Select Case CType(IdEstadoInt, CPedido.Estado)
            Case TiposDeDatos.EstadoIntegracion.ParcialmenteIntegrado
                sEstado = Textos(326)
            Case TiposDeDatos.EstadoIntegracion.EsperaAcuseRecibo
                sEstado = Textos(327)
            Case TiposDeDatos.EstadoIntegracion.ConError
                sEstado = Textos(328)
            Case TiposDeDatos.EstadoIntegracion.TotalmenteIntegrado
                sEstado = Textos(329)
        End Select

        If Not bPedERP And IdEstadoInt <> TiposDeDatos.EstadoIntegracion.TotalmenteIntegrado Then
            sEstado &= " (" & Textos(331) & ")"
        Else
            sEstado &= " (" & Textos(332) & ")"
        End If

        Return sEstado
    End Function
    ''' Revisado por: blp. Fecha: 21/12/2012
    ''' <summary>
    ''' Editamos los nombres de las columnas de la tabla. Si no existen las columnas, se crean.
    ''' </summary>
    ''' <param name="dt">tabla a modificar. SE RECIBE COMO REF</param>
    ''' <remarks>Llamada desde ExportaraExcel. Máximo 0,1 seg.</remarks>
    Protected Sub agregarEditarColumnas(ByRef dt As DataTable)
        If dt.Columns.Count = 0 Then 'Añadir las columnas
            For Each sColumna As String() In ColumnasTextos
                Dim oColumna As New DataColumn
                oColumna.ColumnName = sColumna(1)
                dt.Columns.Add(oColumna)
            Next
        Else
            'cambiar el nombre
            For Each sColumna As String() In ColumnasTextos
                Dim oColumna As DataColumn = dt.Columns(sColumna(0))
                If oColumna IsNot Nothing Then
                    oColumna.ColumnName = sColumna(1)
                End If
            Next
        End If

        If Not Me.Acceso.gbAccesoFSSM AndAlso dt.Columns(Textos("142")) IsNot Nothing Then
            'QUITAR LOS DATOS DE CENTROS (LOS DE PARTIDAS YA LOS HEMOS QUITADO CON ANTERIORIDAD)
            dt.Columns.Remove(Textos("142")) 'Centros
        End If
    End Sub
    ''' <summary>
    ''' Rellena las columnas relativas al control presupuestario
    ''' </summary>
    ''' <param name="ds"></param>
    ''' <remarks></remarks>
    Private Sub rellenarDatosSM(ds As DataSet)
        Dim dtOrdenes As DataTable = ds.Tables(0)
        Dim dtPartidas As DataTable = ds.Tables(7)
        dtOrdenes.Columns.Add("CCUON", GetType(String))
        For Each dr As DataRow In dtPartidas.Rows
            Dim idlinea As Integer = dr("LINEA")
            Dim drLinea As DataRow() = dtOrdenes.Select("LINEAID=" & idlinea)

            For Each row As DataRow In drLinea
                row("CCUON") = dr("CCUON")
                If Not dtOrdenes.Columns.Contains(dr("PARTIDA_DEN")) Then
                    dtOrdenes.Columns.Add(dr("PARTIDA_DEN"))
                End If
                row(dr("PARTIDA_DEN")) = getPartida(dr)
            Next
        Next
    End Sub
    Private Function getPartida(dr As DataRow) As String
        If Not dr.IsNull("PRES4") Then
            Return dr("PRES4")
        Else
            If Not dr.IsNull("PRES3") Then
                Return dr("PRES3")
            Else
                If Not dr.IsNull("PRES2") Then
                    Return dr("PRES2")
                Else
                    If Not dr.IsNull("PRES1") Then
                        Return dr("PRES1")
                    Else
                        Return ""
                    End If
                End If
            End If
        End If
    End Function
    Private Sub formatearPlanEntrega(ByRef ds As DataSet)
        For Each planEntrega As DataRow In ds.Tables(6).Rows
            planEntrega("NUM") = Format(CInt(planEntrega("NUM")), "000").ToString()
        Next
        If Not ds.Tables(6).Rows.OfType(Of DataRow).Where(Function(x) DBNullToDbl(x("CANTIDAD")) > 0).Any Then
            ds.Tables(6).Columns.Remove("CANTIDAD")
        End If
        If Not ds.Tables(6).Rows.OfType(Of DataRow).Where(Function(x) DBNullToDbl(x("IMPORTE")) > 0).Any Then
            ds.Tables(6).Columns.Remove("IMPORTE")
        End If
    End Sub
End Class