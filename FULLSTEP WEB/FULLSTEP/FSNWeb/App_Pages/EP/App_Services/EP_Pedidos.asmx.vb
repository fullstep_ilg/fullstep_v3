﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Services
Imports Fullstep.FSNServer
Imports Fullstep.FSNLibrary.TiposDeDatos

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class EP_Pedidos
    Inherits System.Web.Services.WebService

    Dim FSNServer As Fullstep.FSNServer.Root

    ''' <summary>
    ''' Es llamado desde un link del mail de aprobacion de un pedido cuando se aprueba
    ''' </summary>
    ''' <remarks></remarks>
    <WebMethod()> _
    Public Sub AprobarPedidoEP()
        Dim sIdioma As String = System.Web.HttpContext.Current.Request("Idioma")
        Dim sCodUsuAprobador As String = System.Web.HttpContext.Current.Request("CodAprob")
        Dim iOrdenId As Integer = System.Web.HttpContext.Current.Request("Orden")
        Dim iCodRnd As Long = System.Web.HttpContext.Current.Request("CodRnd")
        Dim iCodSeguridad As Long = System.Web.HttpContext.Current.Request("CodSeguridad")
        Dim iSeguridad_Nivel As Byte = System.Web.HttpContext.Current.Request("Seguridad_Nivel")
        Dim sPedido As String = String.Empty
        Try

            Dim Textos As DataSet
            Dim sTexto As String = String.Empty
            Dim dFechaMail As Date

            FSNServer = New Fullstep.FSNServer.Root
            FSNServer.Login(sCodUsuAprobador, True)

            Dim oOrden As FSNServer.COrden = FSNServer.Get_Object(GetType(FSNServer.COrden))

            oOrden.ID = iOrdenId
            'Se comprueba que el numero aleatorio pertenece a esa orden para poder aprobar el pedido
            sPedido = oOrden.ValidarNumRnd(iOrdenId, iCodRnd)
            oOrden.GrabarAccionEmail("A", iOrdenId, sIdioma)
            Dim FSNDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
            FSNDict.LoadData(TiposDeDatos.ModulosIdiomas.EP_AprobacionPedidos, sIdioma)
            Textos = FSNDict.Data
            If ConfigurationManager.AppSettings("Caducidad_Email") <> "" Then
                dFechaMail = oOrden.ObtenerFecha(iOrdenId)
                If dFechaMail.AddHours(CInt(ConfigurationManager.AppSettings("Caducidad_Email"))) < Now.Date Then
                    sTexto = Textos.Tables(0).Rows(163).Item(1)
                    HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "PedidoAprobadoMail.aspx?CodAprob=" & sCodUsuAprobador & "&Texto=" & HttpContext.Current.Server.UrlEncode(sTexto))
                End If
            End If
            If sPedido <> "" Then
                Dim LinModif As DataRowCollection = Nothing
                Dim oError As Fullstep.FSNServer.CTESError = oOrden.AprobarPedido(sCodUsuAprobador, sIdioma, False, LinModif)
                Select Case oError.NumError
                    Case 0 ' Sin errores
                        sTexto = Textos.Tables(0).Rows(159).Item(1).ToString.Replace("XXXX", sPedido)
                    Case 300    'El pedido ha sido anulado
                        sTexto = Textos.Tables(0).Rows(130).Item(1)
                    Case 301    'El pedido ha sido eliminado
                        sTexto = Textos.Tables(0).Rows(131).Item(1)
                    Case 302 'El pedido ya ha sido emitido por otro usuario.
                        sTexto = Textos.Tables(0).Rows(132).Item(1)
                    Case 303 'El aprobador no tiene importe suficiente para aprobar
                        sTexto = Textos.Tables(0).Rows(133).Item(1) & " " & Textos.Tables(0).Rows(65).Item(1) & ":" & oError.Arg3 & " " & Textos.Tables(0).Rows(134).Item(1) & oError.Arg2 & " " & Textos.Tables(0).Rows(135).Item(1) & oError.Arg1
                    Case 304 'Esta parcialx denegado
                    Case 305 'Esta  denegado
                        sTexto = Textos.Tables(0).Rows(142).Item(1)
                    Case Else
                        sTexto = Textos.Tables(0).Rows(136).Item(1)
                End Select
                HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "PedidoAprobadoMail.aspx?CodAprob=" & sCodUsuAprobador & "&Texto=" & HttpContext.Current.Server.UrlEncode(sTexto))
            Else
                'No se encontro el pedido a aprobar
                sTexto = Textos.Tables(0).Rows(161).Item(1)
                HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "PedidoAprobadoMail.aspx?CodAprob=" & sCodUsuAprobador & "&Texto=" & HttpContext.Current.Server.UrlEncode(sTexto))
            End If
        Catch ex As System.Threading.ThreadAbortException
            'Al hacer el server.Transfer se provoca esta excepcion que se controla.

        Catch ex As Exception
            Dim oOrden As FSNServer.COrden = FSNServer.Get_Object(GetType(FSNServer.COrden))
            oOrden.NotificarErrorAprobacion(iOrdenId, sIdioma, sCodUsuAprobador, iCodSeguridad, iSeguridad_Nivel)
        End Try
    End Sub
    ''' <summary>
    ''' Es llamado desde un link del mail de aprobacion de un pedido cuando se deniega
    ''' </summary>
    ''' <remarks></remarks>
    <WebMethod()> _
    Public Sub DenegarPedidoEP()
        Dim sIdioma As String = System.Web.HttpContext.Current.Request("Idioma")
        Dim sCodUsuAprobador As String = System.Web.HttpContext.Current.Request("CodAprob")
        Dim iOrdenId As Integer = System.Web.HttpContext.Current.Request("Orden")
        Dim iCodRnd As Long = System.Web.HttpContext.Current.Request("CodRnd")
        Dim iCodSeguridad As Long = System.Web.HttpContext.Current.Request("CodSeguridad")
        Dim SeguridadNivel As Byte = System.Web.HttpContext.Current.Request("Seguridad_Nivel")
        Dim Textos As DataSet
        Dim sTexto As String = String.Empty
        Dim sPedido As String = String.Empty
        Try

            Dim dFechaMail As Date

            FSNServer = New Fullstep.FSNServer.Root

            FSNServer.Login(sCodUsuAprobador, True)

            Dim oOrden As FSNServer.COrden = FSNServer.Get_Object(GetType(FSNServer.COrden))
            oOrden.ID = iOrdenId

            Dim FSNDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
            FSNDict.LoadData(TiposDeDatos.ModulosIdiomas.EP_AprobacionPedidos, sIdioma)
            Textos = FSNDict.Data
            'Se comprueba que el numero aleatorio pertenece a esa orden para poder aprobar el pedido
            sPedido = oOrden.ValidarNumRnd(iOrdenId, iCodRnd)
            oOrden.GrabarAccionEmail("R", iOrdenId, sIdioma)

            If ConfigurationManager.AppSettings("Caducidad_Email") <> "" Then
                dFechaMail = oOrden.ObtenerFecha(iOrdenId)
                If dFechaMail.AddHours(CInt(ConfigurationManager.AppSettings("Caducidad_Email"))) < Now.Date Then
                    sTexto = Textos.Tables(0).Rows(164).Item(1)
                    HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "PedidoAprobadoMail.aspx?CodAprob=" & sCodUsuAprobador & "&Texto=" & HttpContext.Current.Server.UrlEncode(sTexto))
                End If
            End If
            If sPedido <> "" Then

                Dim oError As Fullstep.FSNServer.CTESError = oOrden.DenegarPedido(sCodUsuAprobador, sIdioma, "")

                Select Case oError.NumError
                    Case 0 ' Sin errores
                        sTexto = Textos.Tables(0).Rows(160).Item(1).ToString.Replace("XXXX", sPedido)
                    Case 300 ' El pedido ha sido anulado
                        sTexto = Textos.Tables(0).Rows(130).Item(1)
                    Case 301 ' El pedido ha sido eliminado
                        sTexto = Textos.Tables(0).Rows(114).Item(1)
                    Case 302
                        sTexto = Textos.Tables(0).Rows(141).Item(1)
                    Case 303 'El aprobador no tiene importe suficiente para aprobar
                        sTexto = Textos.Tables(0).Rows(133).Item(1) & vbCrLf & Textos.Tables(0).Rows(65).Item(1) & ": " & oError.Arg3 & vbCrLf & _
                                   Textos.Tables(0).Rows(118).Item(1) & " " & oError.Arg2 & vbCrLf & _
                                  Textos.Tables(0).Rows(119).Item(1) & " " & oError.Arg1
                    Case Else
                        sTexto = Textos.Tables(0).Rows(136).Item(1)
                End Select
                HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "PedidoAprobadoMail.aspx?CodAprob=" & sCodUsuAprobador & "&Texto=" & HttpContext.Current.Server.UrlEncode(sTexto))
            Else
                'No se encontro el pedido a rechazar
                sTexto = Textos.Tables(0).Rows(162).Item(1)
                HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "PedidoAprobadoMail.aspx?CodAprob=" & sCodUsuAprobador & "&Texto=" & HttpContext.Current.Server.UrlEncode(sTexto))
            End If
        Catch ex As System.Threading.ThreadAbortException
            'Al hacer el server.Transfer se provoca esta excepcion que se controla.
        Catch ex As Exception

            Dim oOrden As FSNServer.COrden = FSNServer.Get_Object(GetType(FSNServer.COrden))
            oOrden.NotificarErrorRechazo(iOrdenId, sIdioma, sCodUsuAprobador, iCodSeguridad, SeguridadNivel)

        End Try
    End Sub

    <WebMethod(EnableSession:=True)> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function ModificarLineaRecepcion(ByVal Cantidad As Double, ByVal LineaRecep As Long) As JSonResponse
        Dim sIdioma As String = System.Configuration.ConfigurationManager.AppSettings("idioma")
        Dim Textos As DataSet
        Dim objJson As New JSonResponse
        Dim FSNServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oRecepcion As FSNServer.CRecepcion = FSNServer.Get_Object(GetType(FSNServer.CRecepcion))
        Dim respuesta As Byte

        respuesta = oRecepcion.Recepcion_ValidarModificacionLineaRecepcion(LineaRecep, Cantidad)
        Dim FSNDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        FSNDict.LoadData(TiposDeDatos.ModulosIdiomas.EP_Seguimiento, sIdioma)
        Textos = FSNDict.Data
        Select Case respuesta
            Case 1
                objJson.Respuesta = 1
                objJson.TextoPregunta = Textos.Tables(0).Rows(226).Item(1)
            Case 2
                objJson.Respuesta = 2
                objJson.TextoPregunta = Textos.Tables(0).Rows(227).Item(1)
            Case 3
                objJson.Respuesta = 3
                objJson.TextoPregunta = Textos.Tables(0).Rows(228).Item(1)

        End Select

        objJson.TextoAceptar = Textos.Tables(0).Rows(162).Item(1)
        objJson.TextoCancelar = Textos.Tables(0).Rows(163).Item(1)
        objJson.TextoCerrar = Textos.Tables(0).Rows(162).Item(1)


        Return objJson
    End Function

    <WebMethod(EnableSession:=True)> _
   <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GrabarModificacionLineaRecepcion(ByVal Cantidad As Double, ByVal LineaRecep As Long, ByVal Caso As Long) As JSonResponse
        Dim objJson As New JSonResponse
        Dim oReturn(3) As Double
        Dim Textos As DataSet
        Dim sIdioma As String = System.Configuration.ConfigurationManager.AppSettings("idioma")
        Dim FSNServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim oRecepcion As FSNServer.CRecepcion = FSNServer.Get_Object(GetType(FSNServer.CRecepcion))
        Dim oIntegracion As Integracion = FSNServer.Get_Object(GetType(Integracion))

        Dim FSNDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        FSNDict.LoadData(TiposDeDatos.ModulosIdiomas.EP_Seguimiento, sIdioma)
        Textos = FSNDict.Data

        oReturn = oRecepcion.Recepcion_GrabarModificacionLineaRecepcion(LineaRecep, Cantidad, Caso, oUsuario.CodPersona, oUsuario.Cod)
        'Llamadda a FSIS.
        oIntegracion.LlamarFSIS(TablasIntegracion.Rec_Aprov, 0, , , , LineaRecep)

        objJson.CantidadGrabada = oReturn(0)
        objJson.CantidadPendienteRecibir = oReturn(1)
        Select Case oReturn(2)
            Case 0
                'Linea totalmente recibida
                objJson.EstadoRecepcionLinea = Textos.Tables(0).Rows(49).Item(1)
            Case 1
                'No se ha recibido nada, linea Aprobada
                objJson.EstadoRecepcionLinea = Textos.Tables(0).Rows(47).Item(1)
            Case 2
                'Linea parcialmente recibida
                objJson.EstadoRecepcionLinea = Textos.Tables(0).Rows(48).Item(1)
        End Select
        Select Case oReturn(3)
            Case 0
                'Orden totalmente recibida
                objJson.EstadoRecepcionPedido = Textos.Tables(0).Rows(49).Item(1) & " " & FSNLibrary.FormatDate(Now, oUsuario.DateFormat)
            Case 1
                'Orden parcialmente recibida
                objJson.EstadoRecepcionPedido = Textos.Tables(0).Rows(48).Item(1) & " " & FSNLibrary.FormatDate(Now, oUsuario.DateFormat)
        End Select
        Return objJson
    End Function
End Class

Public Class JSonResponse
    Private _respuesta As Byte
    Private _TextoAceptar As String
    Private _TextoCancelar As String
    Private _TextoCerrar As String
    Private _TextoPregunta As String
    Private _EstadoRecepcionLinea As String
    Private _EstadoRecepcionPedido As String
    Private _CantidadGrabada As Double
    Private _CantidadPendienteRecibir As Double

    Public Property Respuesta() As Byte
        Get
            Return _respuesta
        End Get
        Set(ByVal value As Byte)
            _respuesta = value
        End Set
    End Property
    Public Property TextoAceptar() As String
        Get
            Return _TextoAceptar
        End Get
        Set(ByVal value As String)
            _TextoAceptar = value
        End Set
    End Property
    Public Property EstadoRecepcionLinea() As String
        Get
            Return _EstadoRecepcionLinea
        End Get
        Set(ByVal value As String)
            _EstadoRecepcionLinea = value
        End Set
    End Property
    Public Property EstadoRecepcionPedido() As String
        Get
            Return _EstadoRecepcionPedido
        End Get
        Set(ByVal value As String)
            _EstadoRecepcionPedido = value
        End Set
    End Property
    Public Property TextoCancelar() As String
        Get
            Return _TextoCancelar
        End Get
        Set(ByVal value As String)
            _TextoCancelar = value
        End Set
    End Property
    Public Property TextoCerrar() As String
        Get
            Return _TextoCerrar
        End Get
        Set(ByVal value As String)
            _TextoCerrar = value
        End Set
    End Property
    Public Property TextoPregunta() As String
        Get
            Return _TextoPregunta
        End Get
        Set(ByVal value As String)
            _TextoPregunta = value
        End Set
    End Property
    Public Property CantidadGrabada() As Double
        Get
            Return _CantidadGrabada
        End Get
        Set(ByVal value As Double)
            _CantidadGrabada = value
        End Set
    End Property
    Public Property CantidadPendienteRecibir() As Double
        Get
            Return _CantidadPendienteRecibir
        End Get
        Set(ByVal value As Double)
            _CantidadPendienteRecibir = value
        End Set
    End Property

    Public Sub New()

    End Sub
End Class