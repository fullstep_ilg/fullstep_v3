﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports Fullstep.FSNServer
Imports Fullstep.FSNLibrary

<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<System.Web.Script.Services.ScriptService()> _
<ToolboxItem(False)> _
Public Class EPConsultas
    Inherits System.Web.Services.WebService

    Private _FSEPUser As FSNServer.User
    Private _FSEPServer As FSNServer.Root

#Region "Autocompletar"
    ''' Revisado por: blp. Fecha:20/06/2012
    ''' <summary>
    ''' Función utilizada por el servicio web para acceder al DataSet con los artículos 
    ''' a los que tiene acceso el usuario y realizar la consulta con el texto que ha incluido 
    ''' para devolver las sugerencias.
    ''' </summary>
    ''' <param name="prefixText">El texto que ha introducido</param>
    ''' <param name="count">El número de líneas que se mostrarán</param>
    ''' <returns>Un array de strings que almacenará el texto a sugerir</returns>
    ''' <remarks>
    ''' Llamada desde: Cada vez que se introduce texto en cualquiera de los TextBox destinados a buscar artículos
    ''' Tiempo máximo: 0'2 sec. aproximadamente, pero depende de la longitud del texto.
    ''' </remarks>
    <System.Web.Services.WebMethod(True)> _
    Public Function GetAppNosEP(ByVal prefixText As String, ByVal count As Integer) As String()
        If Session("FSN_Server") Is Nothing OrElse Session("FSN_User") Is Nothing Then
            Return Nothing
        Else
            _FSEPServer = Session("FSN_Server")
            _FSEPUser = Session("FSN_User")
            If HttpContext.Current.Cache("DsetArticulos_" & _FSEPUser.CodPersona) Is Nothing Then
                Dim Articulos As FSNServer.cArticulos = _FSEPServer.Get_Object(GetType(FSNServer.cArticulos))
                Dim dsArticulos As DataSet = Articulos.DevolverArticulosCatalogo(0, "", "", System.DBNull.Value, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, _FSEPUser.CodPersona, 1, "", String.Empty, String.Empty, "")
                HttpContext.Current.Cache.Insert("DsetArticulos_" & _FSEPUser.CodPersona, dsArticulos, Nothing, Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            End If
            Dim ds As DataSet = CType(HttpContext.Current.Cache("DsetArticulos_" & _FSEPUser.CodPersona), DataSet)
            Return FiltrarArticulos(prefixText, count, ds.Tables(0))
        End If
    End Function
    ''' <summary>
    ''' Función utilizada por el servicio web para acceder al DataSet con los artículos 
    ''' a los que tiene acceso el usuario y realizar la consulta con el texto que ha incluido 
    ''' para devolver las sugerencias.
    ''' </summary>
    ''' <param name="prefixText">El texto que ha introducido</param>
    ''' <param name="count">El número de líneas que se mostrarán</param>
    ''' <param name="contextKey">Id de la sesión</param>
    ''' <returns>Un array de strings que almacenará el texto a sugerir</returns>
    ''' <remarks>
    ''' Llamada desde: Cada vez que se introduce texto en cualquiera de los TextBox destinados a buscar artículos
    ''' Tiempo máximo: 0'2 sec. aproximadamente, pero depende de la longitud del texto.
    ''' </remarks>
    <System.Web.Services.WebMethod(True)> _
    Public Function GetAppNos(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        If Session("FSN_Server") Is Nothing OrElse Session("FSN_User") Is Nothing Then
            Return Nothing
        Else
            _FSEPServer = Session("FSN_Server")
            _FSEPUser = Session("FSN_User")
            If HttpContext.Current.Cache("DsetArticulos_" & _FSEPUser.CodPersona) Is Nothing Then
                Dim Articulos As FSNServer.cArticulos = _FSEPServer.Get_Object(GetType(FSNServer.cArticulos))
                Dim dsArticulos As DataSet = Articulos.DevolverArticulosCatalogo(0, "", "", System.DBNull.Value, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, _FSEPUser.CodPersona, 1, "", String.Empty, String.Empty, "")
                HttpContext.Current.Cache.Insert("DsetArticulos_" & _FSEPUser.CodPersona, dsArticulos, Nothing, Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            End If
            Dim ds As DataSet = CType(HttpContext.Current.Cache("DsetArticulos_" & _FSEPUser.CodPersona), DataSet)
            Return FiltrarArticulos(prefixText, count, ds.Tables(0))
        End If
    End Function
    ''' <summary>
    ''' Filtra el contenido de tablaArticulos en función del texto prefixText
    ''' </summary>
    ''' <param name="prefixText">Texto a buscar</param>
    ''' <param name="count">Nº máximo de elementos a devolver</param>
    ''' <param name="tablaArticulos">DataTable con el conjunto de artículos del catálogo sobre el que realizar la búsqueda.</param>
    ''' <returns>Array de cadenas con los elementos coincidentes.</returns>
    ''' <remarks>Llamada desde GetAppNos y GetAppNosEP</remarks>
    Private Function FiltrarArticulos(ByVal prefixText As String, ByVal count As Integer, ByVal tablaArticulos As DataTable) As String()
        Dim arrayPalabras As String() = Split(prefixText)
        Dim sbusqueda = From Datos In tablaArticulos _
                        Let w = UCase(DBNullToStr(Datos.Item("COD_ITEM")) & " " & DBNullToStr(Datos.Item("COD_EXT")) & " " & DBNullToStr(Datos.Item("ART_DEN")) & " " & DBNullToStr(Datos.Item("ESP"))) _
                        Where arrayPalabras.All(Function(palabra) w Like "*" & strToSQLLIKE(palabra, True) & "*") _
                        Select Datos.Item("ART_DEN") Distinct.Take(count).ToArray()
        Dim resul() As String
        ReDim resul(UBound(sbusqueda))
        For i As Integer = 0 To UBound(sbusqueda)
            resul(i) = sbusqueda(i).ToString()
        Next
        Return resul
    End Function
#End Region
#Region "Proveedor"
    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle del proveedor dinamicamente
    ''' </summary>
    ''' <param name="contextKey">Código del proveedor a rellenar</param>
    ''' <returns>Un String con todos los datos del proveedor ordenados y en el idioma del usuario</returns>
    ''' <remarks>Tiempo máximo:0,3 seg</remarks>
    <System.Web.Services.WebMethod(True)> _
    Public Function Proveedor_DatosContacto(ByVal contextKey As System.String) As System.String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim FSNServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim dsDatosContacto As DataSet = oServer.Get_Object(GetType(FSNServer.CContactos)).CargarDatosContacto(contextKey)
        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.EP_DatosContacto, oUsuario.Idioma)
        Dim sr As New StringBuilder()
        With dsDatosContacto.Tables(0)
            If .Rows.Count > 0 Then
                sr.Append("<span class=""Rotulo"">")
                If oUsuario.MostrarCodProve Then sr.Append(contextKey & " - ")
                sr.Append(.Rows(0).Item("PROVEDEN") & "</span><br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(0).Item(1) & "</b><br/><br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(1).Item(1) & ": </b>" & .Rows(0).Item("CONTACTO") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(2).Item(1) & ": </b>" & .Rows(0).Item("DEP") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(3).Item(1) & ": </b>" & .Rows(0).Item("CAR") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(4).Item(1) & ": </b>" & .Rows(0).Item("TFNO") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(5).Item(1) & ": </b>" & .Rows(0).Item("TFNO2") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(6).Item(1) & ": </b>" & .Rows(0).Item("TFNO_MOVIL") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(7).Item(1) & ": </b>" & .Rows(0).Item("FAX") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(8).Item(1) & ": </b>" & _
                          "<a class=""Normal"" href=""mailto:" & .Rows(0).Item("EMAIL") & """>" & .Rows(0).Item("EMAIL") & "</a><br/>")
            Else
                sr.Append(oDict.Data.Tables(0).Rows(10).Item(1))
            End If
        End With
        Return sr.ToString()
    End Function
#End Region
#Region "Persona"
    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle de una persona
    ''' </summary>
    ''' <param name="contextKey">Código de persona</param>
    ''' <returns>Un String con los datos de la persona en formato html</returns>
    <System.Web.Services.WebMethod(True)> _
    Public Function Persona_Datos(ByVal contextKey As System.String) As System.String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim FSNServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oPer As FSNServer.CPersona = oServer.Get_Object(GetType(FSNServer.CPersona))
        oPer.Cod = contextKey
        oPer.CargarPersona()
        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.EP_DetalleAprovisionador, oUsuario.Idioma)
        Dim sr As New StringBuilder()
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(1).Item(1) & ":</b><br/>" & oPer.Apellidos & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(2).Item(1) & ":</b><br/>" & oPer.Nombre & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(3).Item(1) & ":</b><br/>" & oPer.Cargo & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(4).Item(1) & ": </b>" & oPer.Tfno & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(5).Item(1) & ": </b>" & oPer.Fax & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(6).Item(1) & ": </b>" & _
                  "<a class=""Normal"" href=""mailto:" & oPer.Email & """>" & oPer.Email & "</a><br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(7).Item(1) & ":</b><br/>" & oPer.DepDen & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(8).Item(1) & ":</b><br/>")
        If Not oPer.UON3DEN Is Nothing Then
            sr.Append(oPer.UON3DEN)
        ElseIf Not oPer.UON2DEN Is Nothing Then
            sr.Append(oPer.UON2DEN)
        Else
            sr.Append(oPer.UON1DEN)
        End If
        Return sr.ToString()
    End Function
#End Region
#Region "Pedido"
    ''' <summary>
    ''' Funcion que retorna el string que se mostrara en el panel de Pedido Fullstep
    ''' </summary>
    ''' <returns>string HTML que se mostrara en el panel</returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True)> _
    Public Function Pedido_Datos(ByVal contextKey As System.String) As String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim FSNServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim arrContextKey As String() = Split(contextKey, "#")
        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))

        oDict.LoadData(TiposDeDatos.ModulosIdiomas.EP_RecepcionPedidos, oUsuario.Idioma)
        Dim sr As New StringBuilder()
		sr.Append("<b>" & oDict.Data.Tables(0).Rows(182).Item(1) & ": </b>" & arrContextKey(0) & "<br/>")
		sr.Append("<b>" & oDict.Data.Tables(0).Rows(32).Item(1) & ": </b>" & arrContextKey(1) & "<br/>")
		Select Case arrContextKey(2)
            Case TipoEstadoOrdenEntrega.EmitidoAlProveedor
				sr.Append("<b>" & oDict.Data.Tables(0).Rows(154).Item(1) & ": </b>" & oDict.Data.Tables(0).Rows(119).Item(1) & "<br/>")
			Case TipoEstadoOrdenEntrega.AceptadoPorProveedor
				sr.Append("<b>" & oDict.Data.Tables(0).Rows(154).Item(1) & ": </b>" & oDict.Data.Tables(0).Rows(24).Item(1) & "<br/>")
			Case TipoEstadoOrdenEntrega.EnCamino
				sr.Append("<b>" & oDict.Data.Tables(0).Rows(154).Item(1) & ": </b>" & oDict.Data.Tables(0).Rows(25).Item(1) & "<br/>")
			Case TipoEstadoOrdenEntrega.EnRecepcion
				sr.Append("<b>" & oDict.Data.Tables(0).Rows(154).Item(1) & ": </b>" & oDict.Data.Tables(0).Rows(26).Item(1) & "<br/>")
			Case TipoEstadoOrdenEntrega.RecibidoYCerrado
				sr.Append("<b>" & oDict.Data.Tables(0).Rows(154).Item(1) & ": </b>" & oDict.Data.Tables(0).Rows(27).Item(1) & "<br/>")
		End Select

        Select Case arrContextKey(3)
            Case CPedido.TipoOrigenPedido.CatalogadoNegociado 'EP no libre
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(117).Item(1) & ": </b>" & oDict.Data.Tables(0).Rows(128).Item(1) & "<br/>")
            Case CPedido.TipoOrigenPedido.Negociado 'GS
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(117).Item(1) & ": </b>" & oDict.Data.Tables(0).Rows(129).Item(1) & "<br/>")
            Case CPedido.TipoOrigenPedido.Directos 'ERP
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(117).Item(1) & ": </b>" & oDict.Data.Tables(0).Rows(131).Item(1) & "<br/>")
            Case CPedido.TipoOrigenPedido.CatalogadoLibres 'EP LIBRE
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(117).Item(1) & ": </b>" & oDict.Data.Tables(0).Rows(255).Item(1) & "<br/>")
        End Select
        Dim oCParametros As FSNServer.CParametros = FSNServer.Get_Object(GetType(FSNServer.CParametros))
        sr.Append("<b>" & oCParametros.CargarLiteralParametros(31, oUsuario.Idioma) & ": </b>" & arrContextKey(4) & "<br/>")
		sr.Append("<b>" & oDict.Data.Tables(0).Rows(48).Item(1) & ": </b>" & FormatNumber(DBNullToDbl(arrContextKey(5)), oUsuario.NumberFormat) & " " & oUsuario.Mon & "<br/>")

		Dim oOrdenes As COrdenes = FSNServer.Get_Object(GetType(FSNServer.COrdenes))
        Dim oOrdenAtributos As DataSet = oOrdenes.CargarAtributosMarcadosMostrarseRecepción(arrContextKey(6))

        If oOrdenAtributos.Tables(0).Rows.Count > 0 Then sr.Append("<br/>")

        For Each Row As DataRow In oOrdenAtributos.Tables(0).Rows
            If Not IsDBNull(Row("VALOR_TEXT")) Then
                sr.Append("<b>" & Row("DEN") & ": </b>" & Row("VALOR_TEXT") & "<br/>")
            ElseIf Not IsDBNull(Row("VALOR_NUM")) Then
                sr.Append("<b>" & Row("DEN") & ": </b>" & FormatNumber(Row("VALOR_NUM"), oUsuario.NumberFormat) & "<br/>")
            ElseIf Not IsDBNull(Row("VALOR_FEC")) Then
                sr.Append("<b>" & Row("DEN") & ": </b>" & FormatDate(Row("VALOR_FEC"), oUsuario.DateFormat) & "<br/>")
            ElseIf Not IsDBNull(Row("VALOR_BOOL")) Then
                If Row("VALOR_BOOL") = 1 Then
                    sr.Append("<b>" & Row("DEN") & ": </b>" & oDict.Data.Tables(0).Rows(73).Item(1) & "<br/>")
                Else
                    sr.Append("<b>" & Row("DEN") & ": </b>" & oDict.Data.Tables(0).Rows(74).Item(1) & "<br/>")
                End If
            Else
                sr.Append("<b>" & Row("DEN") & ": </b><br/>")
            End If
        Next

        Return sr.ToString()
    End Function
    <System.Web.Services.WebMethod(True)> _
    Public Function Detalle_PedidoAbierto_Datos(ByVal contextKey As System.String) As String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim FSNServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))

        Dim cDetallePedidos As cDetallePedidos
        cDetallePedidos = FSNServer.Get_Object(GetType(FSNServer.cDetallePedidos))
        Dim oDatosDetallePedido As Object = cDetallePedidos.Cargar_Datos_Generales_Orden(oUsuario.CodPersona, contextKey, oUsuario.IdiomaCod)
        Dim oOrden As CDatosGenerales = oDatosDetallePedido(0)

        oDict.LoadData(TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos, oUsuario.Idioma)

        Dim sr As New StringBuilder()
        Dim estado As String = String.Empty

        Select Case oOrden.EstadoOrden
            Case TipoEstadoPedidoAbierto.NoVigente
                estado = oDict.Data.Tables(0).Rows(309).Item(1) 'No vigente
            Case TipoEstadoPedidoAbierto.Abierto
                estado = oDict.Data.Tables(0).Rows(310).Item(1) 'Abierto
            Case TipoEstadoPedidoAbierto.Cerrado
                estado = oDict.Data.Tables(0).Rows(311).Item(1) 'Cerrado
            Case TipoEstadoPedidoAbierto.Anulado
                estado = oDict.Data.Tables(0).Rows(311).Item(1) 'Anulado
        End Select

        sr.Append("<b>" & oDict.Data.Tables(0).Rows(303).Item(1) & " : </b>" & oOrden.CodPedido & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(304).Item(1) & " : </b>" & oOrden.Aprovisionador & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(305).Item(1) & " : </b>" & FSNLibrary.FormatDate(oOrden.Fecha, oUsuario.DateFormat) & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(306).Item(1) & " : </b>" & estado & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(307).Item(1) & " : </b>" & FSNLibrary.FormatDate(oOrden.FechaIniPedAbierto, oUsuario.DateFormat) & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(308).Item(1) & " : </b>" & FSNLibrary.FormatDate(oOrden.FechaFinPedAbierto, oUsuario.DateFormat) & "<br/>")

        Return sr.ToString()
    End Function
#End Region
#Region "Destino"
    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle del destino dinamicamente
    ''' </summary>
    ''' <param name="contextKey">Código del destino a rellenar</param>
    ''' <returns>Un String con todos los datos del destinos ordenados y en el idioma del usuario</returns>
    ''' <remarks>Tiempo máximo:0,3 seg</remarks>
    <System.Web.Services.WebMethod(True)> _
    Public Function Destino_Datos(ByVal contextKey As System.String) As System.String
        Dim FSNServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim sDestino As String
        Dim iCodLinea As Integer
        Dim sParam() As String = Split(contextKey, "#")
        sDestino = sParam(0)
        If sParam.Count > 1 Then
            iCodLinea = CLng(sParam(1))
        End If

        Dim oCDestinos As Fullstep.FSNServer.CDestinos = FSNServer.Get_Object(GetType(Fullstep.FSNServer.CDestinos))
        Dim dsDatosDestino As DataSet = oCDestinos.CargarDatosDestino(oUsuario.Idioma, sDestino, iCodLinea)
        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.EP_Destinos, oUsuario.Idioma)
        Dim sr As New StringBuilder()
        sr.Append("<table cellpadding=""4"" cellspacing=""0"" border=""0""><tr><td>")
        With dsDatosDestino.Tables(0)
            If .Rows.Count > 0 Then
                sr.Append("<b>" & .Rows(0).Item("COD").ToString() & " " & .Rows(0).Item("DEN").ToString() & "</b><br/>")
                If Not IsDBNull(.Rows(0).Item("DIR")) Then
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(3).Item(1) & ":</b><br/>" & .Rows(0).Item("DIR").ToString() & "<br/>")
                End If
                If Not IsDBNull(.Rows(0).Item("CP")) Then
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(4).Item(1) & ":</b>" & .Rows(0).Item("CP").ToString() & "<br/>")
                End If
                If Not IsDBNull(.Rows(0).Item("POB")) Then
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(5).Item(1) & ":</b>" & .Rows(0).Item("POB").ToString() & "<br/>")
                End If
                If Not IsDBNull(.Rows(0).Item("PROVI")) Then
                    sr.Append("<b" & oDict.Data.Tables(0).Rows(6).Item(1) & ":</b>" & .Rows(0).Item("PROVI").ToString() & "<br/>")
                End If
                If Not IsDBNull(.Rows(0).Item("PAI")) Then
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(7).Item(1) & ":</b>" & .Rows(0).Item("PAI").ToString() & "<br/>")
                End If
                If Not IsDBNull(.Rows(0).Item("TFNO")) Then
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(17).Item(1) & ":</b>" & .Rows(0).Item("TFNO").ToString() & "<br/>")
                End If
                If Not IsDBNull(.Rows(0).Item("FAX")) Then
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(18).Item(1) & ":</b>" & .Rows(0).Item("FAX").ToString() & "<br/>")
                End If
                If Not IsDBNull(.Rows(0).Item("EMAIL")) Then
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(19).Item(1) & ":</b>" & .Rows(0).Item("EMAIL").ToString() & "<br/>")
                End If
            End If
        End With
        sr.Append("</td></tr></table>")
        Return sr.ToString()
    End Function
#End Region
#Region "Unidad"
    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle de la unidad dinamicamente
    ''' </summary>
    ''' <param name="contextKey">Código de la unidad o código de la unidad seguido de "_@_" y el id de la línea de catálogo</param>
    ''' <returns>Un String con todos los datos de la unidad ordenados y en el idioma del usuario</returns>
    <System.Web.Services.WebMethod(True)> _
    Public Function Unidad_Datos(ByVal contextKey As System.String) As System.String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim FSNServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUnidad As FSNServer.CUnidadPedido = oServer.Get_Object(GetType(FSNServer.CUnidadPedido))
        If contextKey.Contains("_@_") Then
            oUnidad.Codigo = Left(contextKey, contextKey.IndexOf("_@_"))
            Dim sId As String = Right(contextKey, Len(contextKey) - Len(oUnidad.Codigo) - 3)
            oUnidad.CargarDatos(oUsuario.Idioma, CInt(sId))
        Else
            oUnidad.Codigo = contextKey
            oUnidad.CargarDatos(oUsuario.Idioma)
        End If
        oUnidad.AsignarFormatoaUnidadPedido(oUsuario.NumberFormat)
        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.EP_UnidadesPedido, oUsuario.Idioma)
        Dim sr As New StringBuilder()
        sr.Append("<table cellpadding=""4"" cellspacing=""0"" border=""0""><tr><td>")
        If oUnidad IsNot Nothing Then
            With oUnidad
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(12).Item(1) & ":</b> " & .Codigo & " - " & .Denominacion & "<br/>")
                If contextKey.Contains("_@_") Then
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(13).Item(1) & ":</b> " & IIf(String.IsNullOrEmpty(.UnidadCompra), String.Empty, .UnidadCompra & " - " & .DenUnidadCompra) & "<br/>")
                    Dim sFactorConversion As String
                    If .FactorConversion > 0 Then
                        sFactorConversion = FSNLibrary.FormatNumber(.FactorConversion, oUsuario.NumberFormat)
                    Else
                        sFactorConversion = String.Empty
                    End If
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(7).Item(1) & ":</b> " & IIf(String.IsNullOrEmpty(.UnidadCompra), String.Empty, sFactorConversion & " " & .UnidadCompra) & "<br/>")
                    Dim sCantMin As String
                    If .FactorConversion > 0 Then
                        If oUnidad.NumeroDeDecimales Is Nothing Then
                            Dim decimales As Integer = NumeroDeDecimales(DBNullToStr(.CantidadMinima))
                            oUnidad.UnitFormat.NumberDecimalDigits = decimales
                        End If
                        sCantMin = FSNLibrary.FormatNumber(.CantidadMinima, oUnidad.UnitFormat)
                    Else
                        sCantMin = String.Empty
                    End If
                    sr.Append("<b>" & oDict.Data.Tables(0).Rows(8).Item(1) & ":</b> " & sCantMin & "<br/>")
                End If
            End With
        End If
        sr.Append("</td></tr></table>")
        Return sr.ToString()
    End Function
#End Region
#Region "Activo"
    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle del Activo dinamicamente
    ''' </summary>
    ''' <param name="contextKey">Código del activo a rellenar</param>
    ''' <returns>Un String con todos los datos del activo ordenados y en el idioma del usuario</returns>
    ''' <remarks>Tiempo máximo:0,3 seg</remarks>
    <System.Web.Services.WebMethod(True)> _
    Public Function Activo_Datos(ByVal contextKey As System.String) As System.String
        If HttpContext.Current.Session("FSN_Server") Is Nothing OrElse HttpContext.Current.Session("FSN_User") Is Nothing Then
            Return Nothing
        Else
            Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
            Dim FSNServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)

            Dim oActivos As FSNServer.Activos = FSNServer.Get_Object(GetType(FSNServer.Activos))
            Dim bHayIntegracionSalida As Boolean = oActivos.HayIntegracionSentidoSalida()

            Dim oAct As FSNServer.Activo = FSNServer.Get_Object(GetType(FSNServer.Activo))
            Dim dsActivo As DataSet = oAct.DetalleActivo(oUsuario.Idioma.ToString(), contextKey)

            Dim sr As New StringBuilder()
            sr.Append("<table cellpadding=""4"" cellspacing=""0"" border=""0""><tr><td>")
            If Not dsActivo Is Nothing AndAlso dsActivo.Tables.Count > 0 AndAlso dsActivo.Tables(0).Rows.Count > 0 Then
                Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
                oDict.LoadData(TiposDeDatos.ModulosIdiomas.EP_Activos, oUsuario.Idioma)
                sr.Append("<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images\dolar.jpg' alt='" & oDict.Data.Tables(0).Rows(0).Item(1) & "'></td>")
                sr.Append("<td><span style='font-size:1.5em;color:#9D0000;'><b>" & oDict.Data.Tables(0).Rows(0).Item(1) & "</b></span></td></tr>")
                With dsActivo.Tables(0).Rows(0)
                    If Not IsDBNull(.Item("COD")) Then
                        sr.Append("<tr><td><span style='color:#9D0000;'><b>" & oDict.Data.Tables(0).Rows(1).Item(1) & ":</b></span></td><td>" & .Item("COD").ToString() & "</td></tr>")
                    End If
                    If Not IsDBNull(.Item("DEN")) Then
                        sr.Append("<tr><td><span style='color:#9D0000;'><b>" & oDict.Data.Tables(0).Rows(2).Item(1) & ":</b></span></td><td>" & .Item("DEN").ToString() & "</td></tr>")
                    End If
                    If Not IsDBNull(.Item("DEN_CENTRO")) Then
                        sr.Append("<tr><td><span style='color:#9D0000;'><b>" & oDict.Data.Tables(0).Rows(3).Item(1) & ":</b></span></td><td>" & .Item("DEN_CENTRO").ToString() & "</td></tr>")
                    End If
                    If Not IsDBNull(.Item("DEN_EMP")) Then
                        sr.Append("<tr><td><span style='color:#9D0000;'><b>" & oDict.Data.Tables(0).Rows(4).Item(1) & ":</b></span></td><td>" & .Item("DEN_EMP").ToString() & "</td></tr>")
                    End If
                    If bHayIntegracionSalida Then
                        If Not IsDBNull(.Item("COD_ERP")) Then
                            sr.Append("<tr><td><span style='color:#9D0000;'><b>" & oDict.Data.Tables(0).Rows(5).Item(1) & ":</b></span></td><td>" & .Item("COD_ERP").ToString() & "</td></tr><tr><td>")
                        End If
                    End If
                End With
            End If
            sr.Append("</td></tr></table>")
            Return sr.ToString()
        End If
    End Function
#End Region
#Region "Partida Presupuestaria"
    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle de una partida presupuestaria dinamicamente
    ''' </summary>
    ''' <param name="contextKey">Código de la partida a rellenar</param>
    ''' <returns>Un String con todos los datos de la partida ordenados y en el idioma del usuario</returns>
    ''' <remarks>Tiempo máximo:0,3 seg</remarks>
    <System.Web.Services.WebMethod(True)> _
    Public Function PartidaPresupuestaria_Datos(ByVal contextKey As System.String) As System.String

        If HttpContext.Current.Session("FSN_User") Is Nothing _
        OrElse HttpContext.Current.Session("FSN_Server") Is Nothing _
              OrElse String.IsNullOrEmpty(contextKey) Then
            Return Nothing
        Else
            Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
            Dim FSNServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)


            Dim sCentroSM As String = ""
            Dim sPRES0 As String = ""
            Dim sPRES1 As String = ""
            Dim sPRES2 As String = ""
            Dim sPRES3 As String = ""
            Dim sPRES4 As String = ""
            Dim arrCentroPartidas As String() = Split(contextKey, TextosSeparadores.dosArrobas)
            If arrCentroPartidas.Count > 1 Then
                sCentroSM = arrCentroPartidas(0)
                Dim arrPartidas As String() = Split(arrCentroPartidas(1), TextosSeparadores.espacioGuionEspacio)
                If arrPartidas.Count > 4 Then
                    sPRES0 = arrPartidas(0)
                    sPRES1 = arrPartidas(1)
                    sPRES2 = arrPartidas(2)
                    sPRES3 = arrPartidas(3)
                    sPRES4 = arrPartidas(4)
                End If
            End If

            Dim oPartidasPres5 As FSNServer.PartidasPRES5 = FSNServer.Get_Object(GetType(FSNServer.PartidasPRES5))
            Dim dtDatosPartida As DataTable = oPartidasPres5.Datos_Partida(oUsuario.Idioma, sPRES0, sPRES1, sPRES2, sPRES3, sPRES4, sCentroSM)

            Dim sr As New StringBuilder()
            sr.Append("<table cellpadding=""4"" cellspacing=""0"" border=""0""><tr><td>")
            If Not dtDatosPartida Is Nothing AndAlso dtDatosPartida.Rows.Count > 0 Then
                Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
                oDict.LoadData(TiposDeDatos.ModulosIdiomas.Partidas, oUsuario.Idioma)
                With dtDatosPartida.Rows(0)
                    'IMAGEN Y DENOMINACION DEL ARBOL PERSUPUESTARIO
                    sr.Append("<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images\candado.jpg' alt='" & DBNullToStr(.Item("PRES0_DEN")) & "'></td>")
                    sr.Append("<td><span style='font-size:1.5em;color:#9D0000;'><b>" & DBNullToStr(.Item("PRES0_DEN")) & "</b></span></td></tr>")

                    'CÓDIGO PARTIDA
                    sr.Append("<tr><td><span style='color:#9D0000;'><b>" & oDict.Data.Tables(0).Rows(9).Item(1) & "</b></span></td><td>")
                    Select Case .Item("IMP_NIVEL")
                        Case 1
                            sr.Append(.Item("PRES1").ToString())
                        Case 2
                            sr.Append(.Item("PRES2").ToString())
                        Case 3
                            sr.Append(.Item("PRES3").ToString())
                        Case 4
                            sr.Append(.Item("PRES4").ToString())
                        Case Else
                            sr.Append("")
                    End Select
                    sr.Append("</td></tr>")

                    'DENOMINACION PARTIDA
                    If Not IsDBNull(.Item("PRES_DEN")) Then
                        sr.Append("<tr><td><span style='color:#9D0000;'><b>" & oDict.Data.Tables(0).Rows(10).Item(1) & "</b></span></td><td>" & .Item("PRES_DEN").ToString() & "</td></tr>")
                    End If

                    'CENTRO COSTE (CÓDIGO + DENOMINACIÓN)
                    If Not IsDBNull(.Item("CENTRO_SM")) OrElse Not IsDBNull(.Item("CENTRO_SM_DEN")) Then
                        sr.Append("<tr><td><span style='color:#9D0000;'><b>" & oDict.Data.Tables(0).Rows(1).Item(1) & "</b></span></td><td>")
                        sr.Append(DBNullToStr(.Item("CENTRO_SM")))
                        If Not IsDBNull(.Item("CENTRO_SM")) AndAlso Not IsDBNull(.Item("CENTRO_SM_DEN")) Then sr.Append(TextosSeparadores.espacioGuionEspacio)
                        sr.Append(DBNullToStr(.Item("CENTRO_SM_DEN")))
                        sr.Append("</td></tr>")
                    End If

                    'FECHA INI
                    If Not IsDBNull(.Item("FECINI")) Then
                        sr.Append("<tr><td><span style='color:#9D0000;'><b>" & oDict.Data.Tables(0).Rows(11).Item(1) & "</b></span></td><td>" & FormatDate(.Item("FECINI"), oUsuario.DateFormat).ToString & "</td></tr>")
                    End If

                    'FECHA FIN
                    If Not IsDBNull(.Item("FECFIN")) Then
                        sr.Append("<tr><td><span style='color:#9D0000;'><b>" & oDict.Data.Tables(0).Rows(12).Item(1) & "</b></span></td><td>" & FormatDate(.Item("FECFIN"), oUsuario.DateFormat).ToString & "</td></tr>")
                    End If
                End With
            End If
            sr.Append("</td></tr></table>")
            Return sr.ToString()
        End If
    End Function
#End Region
#Region "TipoPedido"
    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle del tipo de pedido dinamicamente
    ''' </summary>
    ''' <param name="contextKey">Código del Tipo de pedido a rellenar</param>
    ''' <returns>Un String con todos los datos del pedido ordenados y en el idioma del usuario</returns>
    ''' <remarks>Tiempo máximo:0,3 seg</remarks>
    <System.Web.Services.WebMethod(True)> _
    Public Function TipoPedido_Datos(ByVal contextKey As System.String) As System.String
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim FSNServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oCTipoPedido As FSNServer.cTipoPedido = FSNServer.Get_Object(GetType(FSNServer.cTipoPedido))
        Dim dsDatosTipoPedido As DataSet = oCTipoPedido.CargarDatosTipoPedido(oUsuario.Idioma, contextKey)
        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        'oDict.LoadData(TiposDeDatos.ModulosIdiomas.EP_TipoPedido, oUsuario.Idioma)
        oDict.LoadData(32855, oUsuario.Idioma)
        Dim sr As New StringBuilder()
        sr.Append("<table cellpadding=""4"" cellspacing=""0"" border=""0""><tr><td>")
        With dsDatosTipoPedido.Tables(0)
            If .Rows.Count > 0 Then
                sr.Append("<b>" & .Rows(0).Item("COD").ToString() & " " & .Rows(0).Item("DEN").ToString() & "</b><br/>")
                If Not IsDBNull(.Rows(0).Item("CONCEPTO")) Then
                    Select Case .Rows(0).Item("CONCEPTO")
                        Case 0
                            sr.Append("<b>" & oDict.Data.Tables(0).Rows(2).Item(1) & ":</b><br/>" & oDict.Data.Tables(0).Rows(9).Item(1) & "<br/>")
                        Case 1
                            sr.Append("<b>" & oDict.Data.Tables(0).Rows(2).Item(1) & ":</b><br/>" & oDict.Data.Tables(0).Rows(10).Item(1) & "<br/>")
                        Case 2
                            sr.Append("<b>" & oDict.Data.Tables(0).Rows(2).Item(1) & ":</b><br/>" & oDict.Data.Tables(0).Rows(11).Item(1) & "<br/>")
                    End Select
                End If
                If Not IsDBNull(.Rows(0).Item("RECEPCIONAR")) Then
                    Select Case .Rows(0).Item("RECEPCIONAR")
                        Case 0
                            sr.Append("<b>" & oDict.Data.Tables(0).Rows(1).Item(1) & ":</b><br/>" & oDict.Data.Tables(0).Rows(6).Item(1) & "<br/>")
                        Case 1
                            sr.Append("<b>" & oDict.Data.Tables(0).Rows(1).Item(1) & ":</b><br/>" & oDict.Data.Tables(0).Rows(7).Item(1) & "<br/>")
                        Case 2
                            sr.Append("<b>" & oDict.Data.Tables(0).Rows(1).Item(1) & ":</b><br/>" & oDict.Data.Tables(0).Rows(8).Item(1) & "<br/>")
                    End Select
                End If
                If Not IsDBNull(.Rows(0).Item("ALMACENAR")) Then
                    Select Case .Rows(0).Item("ALMACENAR")
                        Case 0
                            sr.Append("<b>" & oDict.Data.Tables(0).Rows(0).Item(1) & ":</b><br/>" & oDict.Data.Tables(0).Rows(3).Item(1) & "<br/>")
                        Case 1
                            sr.Append("<b>" & oDict.Data.Tables(0).Rows(0).Item(1) & ":</b><br/>" & oDict.Data.Tables(0).Rows(4).Item(1) & "<br/>")
                        Case 2
                            sr.Append("<b>" & oDict.Data.Tables(0).Rows(0).Item(1) & ":</b><br/>" & oDict.Data.Tables(0).Rows(5).Item(1) & "<br/>")
                    End Select
                End If
            End If
        End With
        sr.Append("</td></tr></table>")
        Return sr.ToString()
    End Function
#End Region
#Region "Adjuntos"
    ''' <summary>
    ''' Genera el HTML para mostrar los adjuntos de una orden o línea
    ''' </summary>
    ''' <param name="Orden">ID de la orden</param>
    ''' <param name="Linea">ID de la línea</param>
    ''' <param name="Origen">Desde dónde se llama:
    ''' 1 - Aprobación de pedidos
    ''' </param>
    ''' <returns>El código HTML generado</returns>
    <System.Web.Services.WebMethod(True)> _
    Public Function Adjuntos(ByVal Orden As Integer, ByVal Linea As Integer, ByVal Origen As Short) As String
        Dim oServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oUsuario As FSNServer.User = HttpContext.Current.Session("FSN_USER")
        Dim FSNServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim ds As DataSet
        Select Case Origen
            Case 1 ' Aprobación
                ds = HttpContext.Current.Cache("DsetPedidosAprobacion_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString())
            Case Else
                ds = Nothing
        End Select
        If ds Is Nothing Then Return String.Empty
        Dim drLin As DataRow = Nothing
        Dim dr() As DataRow
        If Linea > 0 Then
            If ds.Tables("ADJUNTOS_LINEA") Is Nothing Then
                Dim dt As DataTable = New DataTable("ADJUNTOS_LINEA")
                dt.Columns.Add("ID", GetType(Int32))
                dt.Columns.Add("LINEA", GetType(Int32))
                dt.Columns.Add("NOMBRE", GetType(String))
                dt.Columns.Add("COMENTARIO", GetType(String))
                dt.Columns.Add("FECHA", GetType(DateTime))
                dt.Columns.Add("DATASIZE", GetType(Double))
                dt.Columns.Add("TIPO", GetType(TiposDeDatos.Adjunto.Tipo))
                Dim keylp() As DataColumn = {dt.Columns("ID")}
                dt.PrimaryKey = keylp
                ds.Tables.Add(dt)
                keylp(0) = ds.Tables("ADJUNTOS_LINEA").Columns("LINEA")
                ds.Relations.Add("REL_LINEAS_ADJUNTOS", ds.Tables("LINEASPEDIDO").PrimaryKey, keylp, False)
            End If
            drLin = ds.Tables("LINEASPEDIDO").Rows.Find(Linea)
            dr = drLin.GetChildRows("REL_LINEAS_ADJUNTOS")
            If dr.Length = 0 Then
                Dim oLinea As Fullstep.FSNServer.CLinea = oServer.Get_Object(GetType(CLinea))
                oLinea.ID = Linea
                oLinea.CargarAdjuntos(True)
                For Each oAdj As Adjunto In oLinea.Adjuntos
                    Dim fila As DataRow = ds.Tables("ADJUNTOS_LINEA").NewRow()
                    fila.Item("ID") = oAdj.Id
                    fila.Item("LINEA") = Linea
                    fila.Item("NOMBRE") = oAdj.Nombre
                    fila.Item("COMENTARIO") = oAdj.Comentario
                    fila.Item("FECHA") = oAdj.Fecha
                    fila.Item("DATASIZE") = oAdj.dataSize
                    fila.Item("TIPO") = TiposDeDatos.Adjunto.Tipo.Linea_Pedido
                    ds.Tables("ADJUNTOS_LINEA").Rows.Add(fila)
                Next
                dr = ds.Tables("LINEASPEDIDO").Rows.Find(Linea).GetChildRows("REL_LINEAS_ADJUNTOS")
            End If
        Else
            If ds.Tables("ADJUNTOS_ORDEN") Is Nothing Then
                Dim dt As DataTable = New DataTable("ADJUNTOS_ORDEN")
                dt.Columns.Add("ID", GetType(Int32))
                dt.Columns.Add("ORDEN", GetType(Int32))
                dt.Columns.Add("NOMBRE", GetType(String))
                dt.Columns.Add("COMENTARIO", GetType(String))
                dt.Columns.Add("FECHA", GetType(DateTime))
                dt.Columns.Add("DATASIZE", GetType(Double))
                dt.Columns.Add("TIPO", GetType(TiposDeDatos.Adjunto.Tipo))
                Dim keylp() As DataColumn = {dt.Columns("ID")}
                dt.PrimaryKey = keylp
                ds.Tables.Add(dt)
                keylp(0) = ds.Tables("ADJUNTOS_ORDEN").Columns("ORDEN")
                ds.Relations.Add("REL_ORDENES_ADJUNTOS", ds.Tables("ORDENES").PrimaryKey, keylp, False)
            End If
            Dim drOrden As DataRow = ds.Tables("ORDENES").Rows.Find(Orden)
            dr = drOrden.GetChildRows("REL_ORDENES_ADJUNTOS")
            If dr.Length = 0 Then
                Dim oOrden As COrden = oServer.Get_Object(GetType(COrden))
                oOrden.ID = Orden
                oOrden.CargarAdjuntos(True)
                For Each oAdj As Adjunto In oOrden.Adjuntos
                    Dim fila As DataRow = ds.Tables("ADJUNTOS_ORDEN").NewRow()
                    fila.Item("ID") = oAdj.Id
                    fila.Item("ORDEN") = Orden
                    fila.Item("NOMBRE") = oAdj.Nombre
                    fila.Item("COMENTARIO") = oAdj.Comentario
                    fila.Item("FECHA") = oAdj.Fecha
                    fila.Item("DATASIZE") = oAdj.dataSize
                    fila.Item("TIPO") = TiposDeDatos.Adjunto.Tipo.Orden_Entrega
                    ds.Tables("ADJUNTOS_ORDEN").Rows.Add(fila)
                Next
                dr = ds.Tables("ORDENES").Rows.Find(Orden).GetChildRows("REL_ORDENES_ADJUNTOS")
            End If
        End If
        Dim oWriter As IO.TextWriter = New IO.StringWriter()
        Dim HTMLWriter As New HtmlTextWriter(oWriter)
        HTMLWriter.WriteBreak()
        If drLin IsNot Nothing AndAlso Not String.IsNullOrEmpty(DBNullToStr(drLin.Item("OBSADJUN"))) Then
            HTMLWriter.WriteEncodedText(drLin.Item("OBSADJUN"))
            HTMLWriter.WriteBreak()
        End If
        If dr.Count > 0 Then
            HTMLWriter.WriteBreak()
            HTMLWriter.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "4")
            HTMLWriter.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0")
            HTMLWriter.AddAttribute(HtmlTextWriterAttribute.Width, "100%")
            HTMLWriter.AddAttribute(HtmlTextWriterAttribute.Class, "Rectangulo")
            HTMLWriter.RenderBeginTag(HtmlTextWriterTag.Table)
            For Each fila As DataRow In dr
                If Array.IndexOf(dr, fila) Mod 2 = 1 Then _
                    HTMLWriter.AddAttribute(HtmlTextWriterAttribute.Class, "FilaPar2")
                HTMLWriter.RenderBeginTag(HtmlTextWriterTag.Tr)
                HTMLWriter.AddAttribute(HtmlTextWriterAttribute.Wrap, "false")
                HTMLWriter.AddAttribute(HtmlTextWriterAttribute.Valign, "top")
                HTMLWriter.RenderBeginTag(HtmlTextWriterTag.Td)
                HTMLWriter.Write(FSNLibrary.FormatDate(fila.Item("FECHA"), oUsuario.DateFormat))
                HTMLWriter.RenderEndTag()
                HTMLWriter.AddAttribute(HtmlTextWriterAttribute.Valign, "top")
                HTMLWriter.RenderBeginTag(HtmlTextWriterTag.Td)
                HTMLWriter.AddAttribute(HtmlTextWriterAttribute.Class, "Normal")
                HTMLWriter.AddStyleAttribute(HtmlTextWriterStyle.TextDecoration, "none")
                HTMLWriter.AddAttribute(HtmlTextWriterAttribute.Href, ConfigurationManager.AppSettings("rutaEP") & "/_common/Adjuntos.aspx?adjunto=" & fila.Item("ID") & "&tipo=" & IIf(Linea > 0, TiposDeDatos.Adjunto.Tipo.Linea_Pedido, TiposDeDatos.Adjunto.Tipo.Orden_Entrega))
                HTMLWriter.RenderBeginTag(HtmlTextWriterTag.A)
                HTMLWriter.WriteEncodedText(fila.Item("NOMBRE") & " (" & _
                                            FSNLibrary.FormatNumber(fila.Item("DATASIZE") / 1024, oUsuario.NumberFormat) & _
                                            " kb.)")
                HTMLWriter.RenderEndTag()
                HTMLWriter.RenderEndTag()
                If Not String.IsNullOrEmpty(DBNullToStr(fila.Item("COMENTARIO"))) Then
                    HTMLWriter.RenderEndTag()
                    If Array.IndexOf(dr, fila) Mod 2 = 1 Then _
                        HTMLWriter.AddAttribute(HtmlTextWriterAttribute.Class, "FilaPar2")
                    HTMLWriter.RenderBeginTag(HtmlTextWriterTag.Tr)
                    HTMLWriter.AddAttribute(HtmlTextWriterAttribute.Colspan, "2")
                    HTMLWriter.RenderBeginTag(HtmlTextWriterTag.Td)
                    HTMLWriter.RenderBeginTag(HtmlTextWriterTag.Em)
                    HTMLWriter.WriteEncodedText(fila.Item("COMENTARIO"))
                    HTMLWriter.RenderEndTag()
                    HTMLWriter.RenderEndTag()
                End If
                HTMLWriter.RenderEndTag()
            Next
            HTMLWriter.RenderEndTag()
        End If
        Return CType(oWriter, IO.StringWriter).ToString()
    End Function
#End Region
#Region "DireccionEnvioFactura"
    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle de una dirección de entrega de facturas
    ''' </summary>
    ''' <param name="contextKey">Id de la dirección de entrega</param>
    ''' <returns>Un String con todos los datos de la dirección de entrega de factura ordenados y en el idioma del usuario</returns>
    ''' <remarks>Tiempo máximo:0,3 seg</remarks>
    <System.Web.Services.WebMethod(True)> _
    Public Function DireccionEnvioFactura_Datos(ByVal contextKey As System.String) As System.String
        Dim EnvFacturasDirecciones As CEnvFacturaDirecciones = HttpContext.Current.Cache("CDireccionesEnvioFacturas")
        If EnvFacturasDirecciones Is Nothing Then
            Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
            Dim oEPServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
            EnvFacturasDirecciones = oEPServer.Get_Object(GetType(FSNServer.CEnvFacturaDirecciones))
            EnvFacturasDirecciones.CargarDireccionesDeEnvioDeFactura()
            HttpContext.Current.Cache.Insert("CDireccionesEnvioFacturas", EnvFacturasDirecciones, Nothing, Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        End If
        If contextKey = "" Then
            contextKey = "0"
        End If
        Dim direcEnvFra As CEnvFacturaDireccion = EnvFacturasDirecciones.Item(contextKey)
        If direcEnvFra IsNot Nothing Then
            Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
            Dim FSNServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
            Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
            oDict.LoadData(TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos, oUsuario.Idioma)
            Dim sr As New StringBuilder()
            With direcEnvFra
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(90).Item(1) & ": </b>" & .Direccion & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(166).Item(1) & ": </b>" & .CodigoPostal & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(92).Item(1) & ": </b>" & .Poblacion & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(93).Item(1) & ": </b>" & .Provincia & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(94).Item(1) & ": </b>" & .Pais & "<br/>")
            End With
            Return sr.ToString()
        Else
            Return String.Empty
        End If
    End Function
#End Region
#Region "Caché de Articulos y Filtros de Categorias, Proveedores y Destinos"
    ''' Revisado por: blp. Fecha: 20/06/2012
    ''' 20/06/2012: ACTUALMENTE ESTA FUNCIÓN NO SE USA. SU PROPÓSITO ERA SER LANZADA EN LA PÁGINA DE INICIO DE LA APLICACION
    ''' DE FORMA COMPLETAMENTE ASÍNCRONA PARA TENER LOS DATOS DE EP PRECARGADOS EN CACHE.
    ''' DE MOMENTO SE CONSERVA LA FUNCIÓN POR SI SE RETOMA SU USO PERO SI DEFINITIVAMENTE SE CAMBIA EP A PAGINACIÓN EN SERVIDOR, CARECERÍA DE SENTIDO CONSERVARLA
    ''' <summary>
    ''' Método por el que vamos a precargar en caché el dataset de artículos y los de filtros de categorías, proveedores y destinos.
    ''' De este modo, si el usuario entra en los 5 minutos inmediatamente siguientes a EP, tendrá ya disponibles esos datos en caché y
    ''' ahorraremos tiempo de conexión a base de datos y tiempo de procesado de datos por LINQ
    ''' Es un método que sólo se ejecutará cuando no haya ningún valor previo en caché y que precarga todo, sin filtros,
    ''' dado que si no hay datos en caché de artículos, raro sería que lo hubiese de sus filtros.
    ''' </summary>
    ''' <remarks>Llamada asíncrona desde FSNWeb\Inicio.aspx.vb</remarks>
    <System.Web.Services.WebMethod(True)> _
    Public Sub PreCargaEnCacheDeArticulosyFiltros(ByVal sessionID As System.String)
        If Not Session("FSN_Server") Is Nothing AndAlso Not Session("FSN_User") Is Nothing Then
            _FSEPServer = Session("FSN_Server")
            _FSEPUser = Session("FSN_User")
            If _FSEPUser.AccesoEP Then
                'DsetArticulos
                Dim dsArticulos As DataSet
                If HttpContext.Current.Cache("DsetArticulos_" & _FSEPUser.CodPersona) Is Nothing Then
                    Dim Articulos As FSNServer.cArticulos = _FSEPServer.Get_Object(GetType(FSNServer.cArticulos))
                    dsArticulos = Articulos.DevolverArticulosCatalogo(0, "", "", System.DBNull.Value, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, _FSEPUser.CodPersona, 1, "", String.Empty, String.Empty, "")
                    If dsArticulos IsNot Nothing Then _
                        HttpContext.Current.Cache.Insert("DsetArticulos_" & _FSEPUser.CodPersona, dsArticulos, Nothing, Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                Else
                    dsArticulos = HttpContext.Current.Cache("DsetArticulos_" & _FSEPUser.CodPersona)
                End If

                'TodasCategorias
                Dim TodasCategorias As DataSet
                If HttpContext.Current.Cache("TodasCategorias") Is Nothing Then
                    Dim oCCategorias As FSNServer.CCategorias = _FSEPServer.Get_Object(GetType(FSNServer.CCategorias))
                    TodasCategorias = oCCategorias.CargarArbolCategorias("")
                    If TodasCategorias IsNot Nothing Then _
                        HttpContext.Current.Cache.Insert("TodasCategorias", TodasCategorias, Nothing, Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                Else
                    TodasCategorias = HttpContext.Current.Cache("TodasCategorias")
                End If
                Dim key() As DataColumn = {TodasCategorias.Tables(0).Columns("ID")}
                TodasCategorias.Tables(0).PrimaryKey = key

                'TodosDestinos
                Dim TodosDestinos As New List(Of ListItem)
                If HttpContext.Current.Cache("TodosDestinos" & _FSEPUser.Idioma.ToString()) Is Nothing Then
                    Dim oDest As FSNServer.CDestinos = _FSEPServer.Get_Object(GetType(FSNServer.CDestinos))
                    Dim ds As DataSet = oDest.CargarTodosLosDestinos(_FSEPUser.Idioma, "")
                    For Each x As DataRow In ds.Tables(0).Rows
                        TodosDestinos.Add(New ListItem(x.Item("DEN"), x.Item("COD")))
                    Next
                    If TodosDestinos IsNot Nothing Then _
                        HttpContext.Current.Cache.Insert("TodosDestinos" & _FSEPUser.Idioma.ToString(), TodosDestinos, Nothing, Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                Else
                    TodosDestinos = HttpContext.Current.Cache("TodosDestinos" & _FSEPUser.Idioma.ToString())
                End If

                'TodosProves
                Dim TodosProves As New DataSet
                If HttpContext.Current.Cache("DsetProves_" & _FSEPUser.CodPersona & "_" & _FSEPUser.Idioma.ToString) Is Nothing Then
                    Dim oProves As FSNServer.CProveedores
                    oProves = _FSEPServer.Get_Object(GetType(FSNServer.CProveedores))
                    TodosProves = oProves.DevolverProveedoresFiltrados("", "", "", "", "", "", "", _FSEPUser.CodPersona, _FSEPUser.Idioma)
                    If TodosProves IsNot Nothing Then _
                        HttpContext.Current.Cache.Insert("DsetProves_" & _FSEPUser.CodPersona & "_" & _FSEPUser.Idioma.ToString, TodosProves, Nothing, Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                Else
                    TodosProves = HttpContext.Current.Cache("DsetProves_" & _FSEPUser.CodPersona & "_" & _FSEPUser.Idioma.ToString)
                End If

                Dim cEPWebCatalogoWeb As New FSNWeb.CatalogoWeb

                'CategoriasFiltradas
                If HttpContext.Current.Cache("CategoriasFiltradas_" & _FSEPUser.CodPersona & "_" & _FSEPUser.Idioma.ToString) Is Nothing _
                AndAlso Not dsArticulos Is Nothing _
                AndAlso Not TodasCategorias Is Nothing Then
                    Dim Lista As New List(Of ListItem)
                    Lista = cEPWebCatalogoWeb.devolverListaCategorias(dsArticulos.Tables(0))
                    If Lista IsNot Nothing Then _
                        HttpContext.Current.Cache.Insert("CategoriasFiltradas_" & _FSEPUser.CodPersona & "_" & _FSEPUser.Idioma.ToString, Lista, Nothing, Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                End If

                'DestinosFiltrados_
                If HttpContext.Current.Cache("DestinosFiltrados_" & _FSEPUser.CodPersona & "_" & _FSEPUser.Idioma.ToString) Is Nothing _
                AndAlso Not dsArticulos Is Nothing _
                AndAlso Not TodosDestinos Is Nothing Then
                    Dim Lista As New List(Of ListItem)
                    Lista = cEPWebCatalogoWeb.devolverListaDestinos(dsArticulos.Tables(0), TodosDestinos)
                    If Lista IsNot Nothing Then _
                        HttpContext.Current.Cache.Insert("DestinosFiltrados_" & _FSEPUser.CodPersona & "_" & _FSEPUser.Idioma.ToString, Lista, Nothing, Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                End If

                'ProveedoresFiltrados_
                If HttpContext.Current.Cache("ProveedoresFiltrados_" & _FSEPUser.CodPersona & "_" & _FSEPUser.Idioma.ToString) Is Nothing _
                AndAlso Not dsArticulos Is Nothing Then
                    Dim dtProveedoresFiltrados As DataTable
                    dtProveedoresFiltrados = cEPWebCatalogoWeb.devolverListaProveedores(dsArticulos.Tables(0))
                    If dtProveedoresFiltrados IsNot Nothing Then _
                        HttpContext.Current.Cache.Insert("ProveedoresFiltrados_" & _FSEPUser.CodPersona & "_" & _FSEPUser.Idioma.ToString, dtProveedoresFiltrados, Nothing, Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                End If

                'DatosProveedoresFiltrados_
                If HttpContext.Current.Cache("DatosProveedoresFiltrados_" & _FSEPUser.CodPersona & "_" & _FSEPUser.Idioma.ToString) Is Nothing _
                AndAlso Not dsArticulos Is Nothing Then

                    Dim DataSetDevolver As New DataSet

                    Dim DataSetArticulos As New DataTable

                    'CampoOrden
                    Dim CampoOrden As String
                    If _FSEPUser.MostrarCodArt Then
                        CampoOrden = "COD_ITEM"
                    Else
                        CampoOrden = "ART_DEN"
                    End If

                    'SentidoOrdenacion
                    Dim SentidoOrdenacion As String
                    SentidoOrdenacion = "ASC"

                    'FiltrosAnyadidos
                    Dim FiltrosAnyadidos As List(Of FSNWeb.CatalogoWeb.Filtro)
                    If HttpContext.Current.Cache("FiltrosAnyadidos_" & _FSEPUser.CodPersona & "_" & _FSEPUser.Idioma.ToString) IsNot Nothing Then
                        FiltrosAnyadidos = HttpContext.Current.Cache("FiltrosAnyadidos_" & _FSEPUser.CodPersona & "_" & _FSEPUser.Idioma.ToString)
                    Else
                        FiltrosAnyadidos = New List(Of FSNWeb.CatalogoWeb.Filtro)
                    End If

                    DataSetArticulos = dsArticulos.Tables(0)
                    Session("FiltrosAnyadidos_UsadosEnProveedores") = FiltrosAnyadidos
                    DataSetDevolver = cEPWebCatalogoWeb.devolverDatosProveedoresFiltrados(DataSetArticulos, TodosProves)
                    If Not DataSetDevolver Is Nothing Then
                        HttpContext.Current.Cache.Insert("DatosProveedoresFiltrados_" & _FSEPUser.CodPersona & "_" & _FSEPUser.Idioma.ToString, DataSetDevolver, Nothing, Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                    End If
                End If
            End If
        End If
    End Sub
#End Region
End Class