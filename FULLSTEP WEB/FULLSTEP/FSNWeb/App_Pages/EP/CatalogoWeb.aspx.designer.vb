﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class CatalogoWeb

    '''<summary>
    '''FSNPanelDatosUnidad control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FSNPanelDatosUnidad As Global.Fullstep.FSNWebControls.FSNPanelTooltip

    '''<summary>
    '''FSNPanelDatosProve control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FSNPanelDatosProve As Global.Fullstep.FSNWebControls.FSNPanelInfo

    '''<summary>
    '''btnOculto control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnOculto As Global.System.Web.UI.HtmlControls.HtmlInputButton

    '''<summary>
    '''PanelOculto control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelOculto As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''pnlDetalleArticulo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlDetalleArticulo As Global.Fullstep.FSNWeb.DetArticuloCatalogo

    '''<summary>
    '''PanelArbol control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelArbol As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ImgCatalogoArbol control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgCatalogoArbol As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''LblCatalogoArbol control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblCatalogoArbol As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ImgBtnCerrarPanelArbol control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgBtnCerrarPanelArbol As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''pnlCategorias control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlCategorias As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''fsnTvwCategorias control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTvwCategorias As Global.System.Web.UI.WebControls.TreeView

    '''<summary>
    '''mpePanelArbol control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mpePanelArbol As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''PanelDestFiltro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelDestFiltro As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ImgDestFil control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgDestFil As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''LblLisDestFil control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblLisDestFil As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ImgBtnCerrarDestFil control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgBtnCerrarDestFil As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''lstDestPopup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lstDestPopup As Global.System.Web.UI.WebControls.DataList

    '''<summary>
    '''mpePanelDestFil control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mpePanelDestFil As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''panSelecFavoritos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panSelecFavoritos As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''FSNPanelDatosContacto control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FSNPanelDatosContacto As Global.Fullstep.FSNWebControls.FSNPanelInfo

    '''<summary>
    '''imgCabSelFavorito control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgCabSelFavorito As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''LblAnyadirFav control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblAnyadirFav As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''imgBtnCerrarSelFavorito control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgBtnCerrarSelFavorito As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''upSelecFavoritos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upSelecFavoritos As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''TFavoritos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TFavoritos As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''Td1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Td1 As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''LblArticulo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblArticulo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblNomArticulo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNomArticulo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblSelFavProveedor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSelFavProveedor As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''LblNomProve control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblNomProve As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''fsnlnkContactoProve control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnlnkContactoProve As Global.Fullstep.FSNWebControls.FSNLinkInfo

    '''<summary>
    '''COD control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents COD As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''RowAnyadirFavorito0 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RowAnyadirFavorito0 As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''RBFavoritos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RBFavoritos As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''RowAnyadirFavorito control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RowAnyadirFavorito As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''ListaFavoritos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ListaFavoritos As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''BtnAceptarFav control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnAceptarFav As Global.Fullstep.FSNWebControls.FSNButton

    '''<summary>
    '''BtnCancelarFav control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnCancelarFav As Global.Fullstep.FSNWebControls.FSNButton

    '''<summary>
    '''Button3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Button3 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''mpepanSelecFavoritos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mpepanSelecFavoritos As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''panConfirmacionFavoritoAnyadido control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panConfirmacionFavoritoAnyadido As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''upConfirmacionFavoritoAnyadido2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upConfirmacionFavoritoAnyadido2 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''LblMensajeFavoritoAnyadido control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblMensajeFavoritoAnyadido As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''BtnIrAFavorito control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnIrAFavorito As Global.Fullstep.FSNWebControls.FSNButton

    '''<summary>
    '''BtnSeguirEnCatalogo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnSeguirEnCatalogo As Global.Fullstep.FSNWebControls.FSNButton

    '''<summary>
    '''Button13 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Button13 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Button14 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Button14 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''PanelConfFavoAny control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelConfFavoAny As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''mpepanConfirmacionFavoritoAnyadido control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mpepanConfirmacionFavoritoAnyadido As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''PanelBusquedaProves control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelBusquedaProves As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ImgProveBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgProveBusq As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''LblTitProvesBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblTitProvesBusq As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ImgBtnCerrarBusqProves control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgBtnCerrarBusqProves As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''UpBusqProvesFiltros control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpBusqProvesFiltros As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''TablaFiltrosBusquedaPRove control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TablaFiltrosBusquedaPRove As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''lblCodProveBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCodProveBusq As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''tbCodProveBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tbCodProveBusq As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''aceTbCodProveBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents aceTbCodProveBusq As Global.AjaxControlToolkit.AutoCompleteExtender

    '''<summary>
    '''LblDenProveBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblDenProveBusq As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''tbDenProveBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tbDenProveBusq As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''aceTbDenProveBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents aceTbDenProveBusq As Global.AjaxControlToolkit.AutoCompleteExtender

    '''<summary>
    '''LblCifProveBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblCifProveBusq As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''tbCIFProveBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tbCIFProveBusq As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblCpProveBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCpProveBusq As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''tbCpProveBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tbCpProveBusq As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ftbeTbCpProveBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ftbeTbCpProveBusq As Global.AjaxControlToolkit.FilteredTextBoxExtender

    '''<summary>
    '''LblPaiProveBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblPaiProveBusq As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''LblPaiProveBusqSelec control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblPaiProveBusqSelec As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''CmbBoxPaiProveBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CmbBoxPaiProveBusq As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''LblProviProveBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblProviProveBusq As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''LblProviProveBusqSelec control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblProviProveBusqSelec As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''CmbBoxProviProveBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CmbBoxProviProveBusq As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblPobProveBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPobProveBusq As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPobProveBusqSelec control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPobProveBusqSelec As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''CmboBoxPobProveBusq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CmboBoxPobProveBusq As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ImgOculta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgOculta As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''BtnBuscarProve control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnBuscarProve As Global.Fullstep.FSNWebControls.FSNButton

    '''<summary>
    '''BtnOcAbrirPanelBusqProve control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnOcAbrirPanelBusqProve As Global.System.Web.UI.HtmlControls.HtmlButton

    '''<summary>
    '''mpopupProveedores control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mpopupProveedores As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''PanelPrincipal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelPrincipal As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''PanelCatalogo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelCatalogo As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''updFiltros control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updFiltros As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''PanelFiltrosAvanzados control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelFiltrosAvanzados As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblLnkSolicitudes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLnkSolicitudes As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''LnkBtnFiltAv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LnkBtnFiltAv As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''ImgBtnFiltAv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgBtnFiltAv As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''LblFiltAv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblFiltAv As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cpe2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cpe2 As Global.AjaxControlToolkit.CollapsiblePanelExtender

    '''<summary>
    '''PanelFiltrosAv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelFiltrosAv As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''BtnCategoria control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnCategoria As Global.Fullstep.FSNWebControls.FSNLinkInfo

    '''<summary>
    '''ImgBtnLupaCat control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgBtnLupaCat As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''LnkBtnCategoria control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LnkBtnCategoria As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''imgDespCategoria control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgDespCategoria As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''pnlFiltroCat control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlFiltroCat As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lstCategorias control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lstCategorias As Global.System.Web.UI.WebControls.DataList

    '''<summary>
    '''cpePnlFiltroCat control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cpePnlFiltroCat As Global.AjaxControlToolkit.CollapsiblePanelExtender

    '''<summary>
    '''BtnProves control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnProves As Global.Fullstep.FSNWebControls.FSNLinkInfo

    '''<summary>
    '''ImgBtnLupaProv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgBtnLupaProv As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''LnkBtnProves control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LnkBtnProves As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''imgBtnProves control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgBtnProves As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''pnlFiltroProv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlFiltroProv As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''cpePnlFiltroProv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cpePnlFiltroProv As Global.AjaxControlToolkit.CollapsiblePanelExtender

    '''<summary>
    '''FilaFiltroProve control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FilaFiltroProve As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''DlFiltrosProve control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DlFiltrosProve As Global.System.Web.UI.WebControls.DataList

    '''<summary>
    '''BtnDests control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnDests As Global.Fullstep.FSNWebControls.FSNLinkInfo

    '''<summary>
    '''ImgBtnLupaDests control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgBtnLupaDests As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''LnkBtnDests control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LnkBtnDests As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''ImgBtnDests control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgBtnDests As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''pnlFiltroDests control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlFiltroDests As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lstDestinos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lstDestinos As Global.System.Web.UI.WebControls.DataList

    '''<summary>
    '''cpePnlFiltroDests control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cpePnlFiltroDests As Global.AjaxControlToolkit.CollapsiblePanelExtender

    '''<summary>
    '''tFiltro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tFiltro As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''LblFiltro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblFiltro As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lstFiltros control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lstFiltros As Global.System.Web.UI.WebControls.DataList

    '''<summary>
    '''UpdatePanelGrid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanelGrid As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''GridArticulos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GridArticulos As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''updLblSinArticulos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updLblSinArticulos As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''lblSolicitudes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSolicitudes As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblEmitir control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEmitir As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''tblSolicitudes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblSolicitudes As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''lblCabeceraAltaSolicitudes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCabeceraAltaSolicitudes As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''uwgSolicitudes_WDG control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents uwgSolicitudes_WDG As Global.Infragistics.Web.UI.GridControls.WebDataGrid

    '''<summary>
    '''lblFooter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFooter As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAplicarFiltro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAplicarFiltro As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''IDPostBack control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents IDPostBack As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Master property.
    '''</summary>
    '''<remarks>
    '''Auto-generated property.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As Fullstep.FSNWeb.CabCatalogo
        Get
            Return CType(MyBase.Master, Fullstep.FSNWeb.CabCatalogo)
        End Get
    End Property
End Class
