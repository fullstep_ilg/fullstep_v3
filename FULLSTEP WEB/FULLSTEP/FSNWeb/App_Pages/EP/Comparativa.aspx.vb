﻿Public Partial Class Comparativa
    Inherits FSEPPage
    Private DsCarga As DataSet
    Private ArticulosComparar As String()
    Private ContadorAtributos As Integer = 0
    Private contFilas As Integer
    Private ColumnasEliminadas As String()
    Private AtributosCargados As Boolean = False
    Private FilasAnyadidas As Boolean = False
    Dim FilaCabecera As HtmlTableRow
    Private AtribsNumericos As AtribNumericos()

    <Serializable()> _
    Private Structure AtribNumericos
        Public DenAtrib As String
        Public EsNumerico As Boolean
        Public EstaEliminado As Boolean
    End Structure

    ''' <summary>
    ''' Evento de Carga de la página, llamando a otros procedimientos para hacernos con los datos necesarios
    ''' </summary>
    ''' <param name="sender">el emisor del evento, esto es, la propia página</param>
    ''' <param name="e">El evento de Carga</param>
    ''' <remarks>
    ''' Llamada desde: Siempre que se recarga la página
    ''' Tiempo máximo:Depende la cantidad de artículos, pero aproximadamente 0,25 sg por cada uno.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = FSNLibrary.TiposDeDatos.ModulosIdiomas.EP_CatalogoWeb
        ScriptMgr.RegisterPostBackControl(BtnVaciarComparativa)
        Call CargarVariables()
        Call CargarTextos()
        If Not Session("ArticulosComparar") Is Nothing Then
            ArticulosComparar = Session("ArticulosComparar")
        End If
        If ViewState("ArticulosCompararIni") Is Nothing Then
            Dim ArrayAux As String() = {}
            Dim Cont As Integer = 0
            For Each Elem As String In ArticulosComparar
                ReDim Preserve ArrayAux(Cont)
                ArrayAux(Cont) = Elem
                Cont = Cont + 1
            Next
            ViewState("ArticulosCompararIni") = ArrayAux
        End If
        If ArticulosComparar Is Nothing OrElse ArticulosComparar.Count = 0 Then
            Master.CabTabComparativa.Text = Textos(89)
        Else
            Master.CabTabComparativa.Text = Textos(89) & "(" & ArticulosComparar.Count & ")"
        End If
        Master.CabUpdPestanyasCatalogo.Update()
        Master.CabTabComparativa.Selected = True
        If DsCarga Is Nothing Then
            Dim catn1Ini As FSNServer.CCategoriasN1 = Me.FSNServer.Get_Object(GetType(FSNServer.CCategoriasN1))
            Dim catn2Ini As FSNServer.CCategoriasN2 = Me.FSNServer.Get_Object(GetType(FSNServer.CCategoriasN2))
            Dim catn3Ini As FSNServer.CCategoriasN3 = Me.FSNServer.Get_Object(GetType(FSNServer.CCategoriasN3))
            Dim catn4Ini As FSNServer.CCategoriasN4 = Me.FSNServer.Get_Object(GetType(FSNServer.CCategoriasN4))
            Dim catn5Ini As FSNServer.CCategoriasN5 = Me.FSNServer.Get_Object(GetType(FSNServer.CCategoriasN5))
            Dim oArticulos As FSNServer.cArticulos = Me.FSNServer.Get_Object(GetType(FSNServer.cArticulos))
            catn1Ini.CargarTodasLasCategorias(FSNUser.Cod, FSNUser.Idioma)
            DsCarga = oArticulos.FiltrarArticulosPorId(0, "", "", "", catn1Ini, catn2Ini, catn3Ini, catn4Ini, catn5Ini, "", FSNUser.CodPersona, 1, "", String.Empty, String.Empty, "", ArticulosComparar)
        End If
        Call DisenyarGridArticulos()
        upTabla.Update()
        ViewState("DsCarga") = DsCarga
        If ViewState("DsCargaInicial") Is Nothing Then
            Dim DsCargaInicial As New DataSet
            DsCargaInicial = DsCarga.Copy
            ViewState("DsCargaInicial") = DsCargaInicial
        End If
        For i As Integer = 1 To tCabComparativa.Rows.Count - 1
            ' tCabComparativa.Rows(i).Cells(1).Width = "50"
            CType(tCabComparativa.FindControl("wNumEdCant" & i), Infragistics.Web.UI.EditorControls.WebNumericEditor).Width = "60"
        Next
    End Sub

    ''' <summary>
    ''' Carga las variables que tenemos almacenadas en ViewState o Session
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde:Page_load
    ''' Tiempo máximo:0 seg</remarks>
    Public Sub CargarVariables()
        If Not ViewState("contadorAtriburos") Is Nothing Then
            ContadorAtributos = ViewState("ContadorAtributos")
        End If
        If Not ViewState("contFilas") Is Nothing Then
            contFilas = ViewState("contFilas")
        End If
        If Not ViewState("DsCarga") Is Nothing Then
            DsCarga = ViewState("DsCarga")
        End If
        If Not ViewState("ColumnasEliminadas") Is Nothing Then
            ColumnasEliminadas = ViewState("ColumnasEliminadas")
        End If
        If Session("FilaCabecera") Is Nothing Then
            Session("FilaCabecera") = fCabComparativa
            FilaCabecera = fCabComparativa
        Else
            FilaCabecera = Session("FilaCabecera")
        End If
        If Not ViewState("AtribsNumericos") Is Nothing Then
            AtribsNumericos = ViewState("AtribsNumericos")
        End If
        ScriptMgr.RegisterAsyncPostBackControl(BtnRecargarComparativa)
    End Sub

    ''' <summary>
    ''' Crea la tabla donde van a ir los artículos de la comparativa, y la rellena con los datos de los articulos
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde:El page_load
    ''' Tiempo máximo:Unos 0,2 sg por cada artículo de la comparativa</remarks>
    Public Sub DisenyarGridArticulos()
        Dim IdsArt As Integer() = {}
        Dim ContadorArts As Integer = 0
        Dim ContadorFilas As Integer = 1
        If Not ViewState(AtributosCargados) Is Nothing Then
            AtributosCargados = ViewState("AtributosCargados")
        End If
        If Not AtributosCargados Then
            If FSNUser.MostrarAtrib Then
                Dim DsAtrib As New DataSet
                Dim CodArt As String
                Dim CodProve As String
                Dim contador As Integer = 1
                Dim IdArt As Integer
                Dim CountAtribTexto As Integer = 1
                If Not DsCarga.Tables.Count = 0 AndAlso Not DsCarga.Tables(0).Rows.Count = 0 Then
                    For Each fila As DataRow In DsCarga.Tables(0).Rows
                        CountAtribTexto = 1
                        DsAtrib = New DataSet
                        CodArt = fila.Item("Cod_Item")
                        CodProve = fila.Item("prove")
                        IdArt = fila.Item("ID")
                        Dim oCArticulos As FSNServer.cArticulos = Me.FSNServer.Get_Object(GetType(FSNServer.cArticulos))
                        DsAtrib = oCArticulos.DevolverAtributosArt(CodArt, CodProve)
                        If DsAtrib.Tables(0).Rows.Count > 0 Then
                            For Each FilaAtribs As DataRow In DsAtrib.Tables(0).Rows
                                If ColumnasEliminadas Is Nothing OrElse Not ColumnasEliminadas.Contains(FilaAtribs.Item("den")) Then
                                    If Not DsCarga.Tables(0).Columns.Contains(FilaAtribs.Item("den")) Then
                                        If Not EstaAtributo(FilaAtribs.Item("Den")) Then
                                            If AtribsNumericos Is Nothing OrElse AtribsNumericos.Count = 0 Then
                                                ReDim Preserve AtribsNumericos(0)
                                                AtribsNumericos(0).DenAtrib = FilaAtribs.Item("den")
                                                If Not FilaAtribs.Item("Valor_Num") Is Nothing AndAlso Not IsDBNull(FilaAtribs.Item("Valor_Num")) AndAlso (Not ColumnasEliminadas Is Nothing AndAlso Not ColumnasEliminadas.Contains(FilaAtribs.Item("den"))) Then
                                                    AtribsNumericos(0).EsNumerico = True
                                                    AtribsNumericos(0).EstaEliminado = False
                                                ElseIf FilaAtribs.Item("Valor_Num") Is Nothing OrElse IsDBNull(FilaAtribs.Item("Valor_Num")) Then
                                                    AtribsNumericos(0).EsNumerico = False
                                                    AtribsNumericos(0).EstaEliminado = False
                                                ElseIf Not ColumnasEliminadas Is Nothing AndAlso ColumnasEliminadas.Contains(FilaAtribs.Item("Den")) Then
                                                    AtribsNumericos(0).EstaEliminado = True
                                                End If
                                            Else
                                                ReDim Preserve AtribsNumericos(AtribsNumericos.Count)
                                                AtribsNumericos(AtribsNumericos.Count - 1).DenAtrib = FilaAtribs.Item("den")
                                                If Not FilaAtribs.Item("Valor_Num") Is Nothing AndAlso Not IsDBNull(FilaAtribs.Item("Valor_Num")) AndAlso (Not ColumnasEliminadas Is Nothing AndAlso Not ColumnasEliminadas.Contains(FilaAtribs.Item("den"))) Then
                                                    AtribsNumericos(AtribsNumericos.Count - 1).EsNumerico = True
                                                    AtribsNumericos(AtribsNumericos.Count - 1).EstaEliminado = False
                                                ElseIf FilaAtribs.Item("Valor_Num") Is Nothing OrElse IsDBNull(FilaAtribs.Item("Valor_Num")) Then
                                                    AtribsNumericos(AtribsNumericos.Count - 1).EsNumerico = False
                                                    AtribsNumericos(AtribsNumericos.Count - 1).EstaEliminado = False
                                                ElseIf Not ColumnasEliminadas Is Nothing AndAlso ColumnasEliminadas.Contains(FilaAtribs.Item("Den")) Then
                                                    AtribsNumericos(AtribsNumericos.Count - 1).EstaEliminado = True
                                                End If
                                            End If
                                            ViewState("AtribsNumericos") = AtribsNumericos
                                        End If
                                        DsCarga.Tables(0).Columns.Add(FilaAtribs.Item("den"))
                                        If Not FilaAtribs.Item("Valor_Num") Is Nothing AndAlso Not IsDBNull(FilaAtribs.Item("Valor_Num")) Then
                                            ReDim Preserve AtribsNumericos(ContadorAtributos)

                                        Else

                                        End If
                                        fila.Item(FilaAtribs.Item("den")) = FilaAtribs.Item("Valor")
                                        ContadorAtributos = ContadorAtributos + 1
                                        CType(fCabComparativa.FindControl("CelCabAtrib" & ContadorAtributos), HtmlTableCell).Visible = True
                                        CType(fCabComparativa.FindControl("LblCabAtrib" & ContadorAtributos), Label).Text = FilaAtribs.Item("Den")
                                    End If
                                End If
                            Next
                            fila.AcceptChanges()
                            fila.SetModified()
                        End If
                        contador = contador + 1
                    Next
                End If

            End If
            AtributosCargados = True
        End If

        ContadorArts = 0
        If Not ArticulosComparar Is Nothing AndAlso Not ArticulosComparar.Count = 0 Then
            For i As Integer = 0 To ArticulosComparar.Count - 1
                ReDim Preserve IdsArt(i)
                IdsArt(i) = CInt(ArticulosComparar(i))
                ContadorArts = ContadorArts + 1
            Next
            Call AnyadirFilas(ContadorArts, IdsArt)
        End If
        DibujarCeldasVacias()
        ViewState("DsCarga") = DsCarga
        ViewState("AtributosCargados") = AtributosCargados
    End Sub

    ''' <summary>
    ''' Función que comprueba si un atributo ya esta en el objeto
    ''' </summary>
    ''' <param name="DenAtrib">Denominación del atributo que queremos mirar</param>
    ''' <returns>Un valor booleano indicando si está o no el atributo</returns>
    ''' <remarks>
    ''' Llamada desde: DisenyarGridArticulos
    ''' Tiempo máximo: 0 seg</remarks>
    Public Function EstaAtributo(ByVal DenAtrib As String) As Boolean
        If AtribsNumericos Is Nothing OrElse AtribsNumericos.Count = 0 Then
            Return False
        Else
            For Each atributo As AtribNumericos In AtribsNumericos
                If atributo.DenAtrib = DenAtrib Then Return True
            Next
        End If
        Return False
    End Function

    ''' <summary>
    ''' Función que comprueba si un atributo es numérico, mirando el objeto
    ''' </summary>
    ''' <param name="DenAtrib">Denominación del atributo a mirar</param>
    ''' <returns>Un valor booleano indicando si es numérico o no el atributo a mirar</returns>
    ''' <remarks>
    ''' Llamada desde: EliminarColumnas y ELiminarFilas
    ''' Tiempo máximo: 0 seg</remarks>
    Public Function EsNumerico(ByVal DenAtrib As String) As Boolean
        If Not AtribsNumericos Is Nothing AndAlso Not AtribsNumericos.Count = 0 Then
            For Each atributo As AtribNumericos In AtribsNumericos
                If DenAtrib = atributo.DenAtrib AndAlso atributo.EsNumerico AndAlso Not atributo.EstaEliminado Then
                    Return True
                End If
            Next
        End If
        Return False
    End Function

    ''' <summary>
    ''' Añade un artículo a la cesta de la compra, con su precio y la cantidad introducida por el usuario.
    ''' </summary>
    ''' <param name="sender">el botón que lanza el evento</param>
    ''' <param name="e">El evento de clicado</param>
    ''' <remarks>
    ''' Llamada desde:La página, cada vez que se clica el boton de "Añadir" de la columna cesta
    ''' Tiempo máximo:0,3 seg aprox.</remarks>
    Protected Sub AnyadirCestaClick(ByVal sender As Object, ByVal e As EventArgs) 'Handles btnAnyadirCesta1.Click ', btnAnyadirCesta2.Click, btnAnyadirCesta3.Click, btnAnyadirCesta3.Click, btnAnyadirCesta4.Click, btnAnyadirCesta5.Click, btnAnyadirCesta6.Click, btnAnyadirCesta7.Click
        '*******************************************************************************************************
        '*** Descripción: Procedimiento que se utiliza para añadir un artículo a la cesta
        '*** Parámetros de entrada: La fila donde se encuentra el artículo clicado.
        '*** Llamada desde: En el evento "Row Command" del GridView.
        '*** Tiempo máximo:1 sec. aproximadamente
        '*******************************************************************************************************
        'Dim timeactual As Date = Date.Now
        Dim Numfila As Integer
        Dim CantArt As Integer
        Dim PrecioArticuloElegido As Double
        Dim ArticuloAñadir As FSNServer.CArticulo = Me.FSNServer.Get_Object(GetType(FSNServer.CArticulo))
        Dim varId As Integer
        Numfila = CInt(Right(CType(sender, FSNWebControls.FSNButton).ID, 1))
        If CType(CType(tCabComparativa.FindControl("FilaDatos" & Numfila), HtmlTableRow).FindControl("LblPrecArt" & Numfila), Label).Visible Then
            PrecioArticuloElegido = CDbl(Numero(CType(CType(tCabComparativa.FindControl("FilaDatos" & Numfila), HtmlTableRow).FindControl("LblPrecArt" & Numfila), Label).Text, FSNUser.NumberFormat.NumberDecimalSeparator))
        Else
            PrecioArticuloElegido = CDbl(Numero(CType(CType(tCabComparativa.FindControl("FilaDatos" & Numfila), HtmlTableRow).FindControl("tbPrecArt" & Numfila), TextBox).Text, FSNUser.NumberFormat.NumberDecimalSeparator))
        End If
        CantArt = CInt(CType(CType(tCabComparativa.FindControl("FilaDatos" & Numfila), HtmlTableRow).FindControl("wNumEdCant" & Numfila), Infragistics.Web.UI.EditorControls.WebNumericEditor).Text)
        varId = CInt(CType(CType(tCabComparativa.FindControl("FilaDatos" & Numfila), HtmlTableRow).FindControl("LblIdOculto" & Numfila), Label).Text)
        'Hago lo mismo para buscar el label
        'Lo añado a la base de datos dentro de la cesta, para eso busco su id
        Dim oArticulos As FSNServer.cArticulos
        oArticulos = FSNServer.Get_Object(GetType(FSNServer.cArticulos))
        ArticuloAñadir = Me.FSNServer.Get_Object(GetType(FSNServer.CArticulo))
        ArticuloAñadir = oArticulos.CargarArticulo(varId)
        ArticuloAñadir.Cantidad = CantArt
        If IsNumeric(ArticuloAñadir.FC) AndAlso ArticuloAñadir.FC <> 0 Then
            ArticuloAñadir.PrecioUC = PrecioArticuloElegido / ArticuloAñadir.FC
        Else
            ArticuloAñadir.PrecioUC = PrecioArticuloElegido
        End If
        ArticuloAñadir.AnyadirACesta(FSNUser.CodPersona)
        Master.CabControlCesta.Actualizar()
        'Dim timefinal As Date = Date.Now
        'Dim timetotal As Double = (timefinal.Ticks - timeactual.Ticks) / 10000000
    End Sub

    ''' <summary>
    ''' Carga el panel con el detalle del artículo clicado.
    ''' </summary>
    ''' <param name="sender">el LinkButton clicado para ver el detalle</param>
    ''' <param name="e">El evento de clicado</param>
    ''' <remarks>
    ''' Llamada desde: La página cada vez que se clica un LinkButton para ver el detalle del artículo
    ''' Tiempo máximo:0,4 seg</remarks>
    Public Sub CargarDetalleArticulo(ByVal sender As Object, ByVal e As EventArgs) 'Handles LblNom1.Click ', LblNom2.Click, LblNom3.Click, LblNom4.Click, LblNom5.Click, LblNom6.Click, LblNom7.Click
        pnlDetalleArticulo.CargarDetalleArticulo(DsetArticulos, CType(sender, LinkButton).CommandArgument)
    End Sub

    ''' <summary>
    ''' Procedimiento que se utiliza para añadir un artículo a la cesta desde la ventana de detalle
    ''' </summary>
    ''' <param name="IdArt">Id del artículo a añadir</param>
    ''' <param name="Cantidad">Cantidad a añadir a la cesta</param>
    ''' <param name="Precio">Precio del artículo, el parámetro únicamente se tiene en cuenta si el usuario puede modificar el precio</param>
    ''' <remarks></remarks>
    Private Sub pnlDetalleArticulo_AnyadirACesta(ByVal IdArt As Integer, ByVal Cantidad As Double, ByVal Precio As Double, ByVal Key As String) Handles pnlDetalleArticulo.AnyadirACesta
        With DsetArticulos.Tables(0).Rows.Find(IdArt)
            Dim PrecioCesta As Double = .Item("PRECIO_UC")
            If .Item("MODIF_PREC") = 1 And Me.FSNUser.ModificarPrecios Then
                PrecioCesta = Precio / .Item("FC_DEF")
            End If
            If Cantidad > 0 Then
                Dim oCArticulo As FSNServer.CArticulo = Me.FSNServer.Get_Object(GetType(FSNServer.CArticulo))
                oCArticulo.AnyadirACesta(Me.FSNUser.CodPersona, .Item("FC_DEF"), .Item("PROVE"), IdArt, _
                     DBNullToSomething(.Item("CAT1")), DBNullToSomething(.Item("CAT2")), DBNullToSomething(.Item("CAT3")), DBNullToSomething(.Item("CAT4")), DBNullToSomething(.Item("CAT5")), _
                     .Item("ART_DEN"), Cantidad, .Item("UP_DEF"), PrecioCesta, 0, .Item("MON"))
                Master.CabControlCesta.Actualizar()
            End If
        End With
    End Sub

    ''' <summary>
    ''' Añade una fila nueva a los artículos de la comparativa
    ''' </summary>
    ''' <param name="NumFilas">La cantidad de filas a añadir</param>
    ''' <param name="Ids">Un array con los Ids de los artículos a añadir</param>
    ''' <remarks>
    ''' Llamada desde:El método diseñargridArticulos
    ''' Tiempo máximo:unos 0,25 seg por cada articulo</remarks>
    Public Sub AnyadirFilas(ByVal NumFilas As Integer, ByVal Ids As Integer())
        Dim LabelPrecio As New Label
        Dim Tbprecio As New TextBox
        Dim LnkBtnNombre As New LinkButton
        Dim LabelMoneda As New Label
        Dim wNumEdCantidad As New Infragistics.Web.UI.EditorControls.WebNumericEditor
        Dim FSNlnkUnidad As New FSNWebControls.FSNLinkTooltip
        Dim LabelIdOculto As New Label
        Dim BtnAnyadirCesta As New FSNWebControls.FSNButton
        Dim FilaDatos As New HtmlTableRow
        Dim CeldaNormal As New HtmlTableCell
        'Dim ftbeCantidad As New AjaxControlToolkit.FilteredTextBoxExtender
        Dim IdArt As Integer
        Dim ModificarPrecio As Boolean = False
        Dim ArticuloCargar As FSNServer.CArticulo = Me.FSNServer.Get_Object(GetType(FSNServer.CArticulo))
        Dim Articulos As FSNServer.cArticulos
        Dim ImgBtnElimFila As New ImageButton
        Dim TablaBoton As New HtmlTable
        Dim FilaTablaboton As New HtmlTableRow
        Dim CeldaTablaBoton As New HtmlTableCell
        Articulos = FSNServer.Get_Object(GetType(FSNServer.cArticulos))
        For i As Integer = 1 To NumFilas
            FilaDatos = New HtmlTableRow
            CeldaNormal = New HtmlTableCell
            IdArt = Ids(i - 1)
            If Not IdArt = Nothing AndAlso Not IdArt = 0 Then
                ArticuloCargar = DevolverArticulo(IdArt)
                ImgBtnElimFila = New ImageButton
                ImgBtnElimFila.ID = "ImgBtnElimFila" & (i).ToString
                ImgBtnElimFila.ImageAlign = ImageAlign.Left
                ImgBtnElimFila.ImageUrl = "../../images/elimadjunto.gif"
                AddHandler ImgBtnElimFila.Click, AddressOf EliminarFila
                ImgBtnElimFila.ToolTip = Textos(129)
                CeldaNormal.Controls.Add(ImgBtnElimFila)
                FilaDatos.Cells.Add(CeldaNormal)
                CeldaNormal = New HtmlTableCell

                wNumEdCantidad = New Infragistics.Web.UI.EditorControls.WebNumericEditor
                wNumEdCantidad.ID = "wNumEdCant" & (i)
                wNumEdCantidad.Style("display") = ""
                wNumEdCantidad.Buttons.SpinButtonsDisplay = Infragistics.Web.UI.EditorControls.ButtonDisplay.OnRight

                configurarDecimales(ArticuloCargar.UP, wNumEdCantidad)
                'Insertamos un script que controla el evento paste en el control
                Dim sClientIDdeContent2 = upTabla.Parent.ClientID
                Dim sClientID As String = sClientIDdeContent2 & "_" & wNumEdCantidad.ClientID
                Me.insertarScriptPaste(sClientID)

                Dim oUnidadPedido As FSNServer.CUnidadPedido = Nothing
                If Not DBNullToSomething(ArticuloCargar.UP) = Nothing Then
                    oUnidadPedido = TodasLasUnidades.Item(Trim(ArticuloCargar.UP))
                End If

                Dim cantidad As Integer
                If Not IsDBNull(ArticuloCargar.CantMinimaPedido) Then
                    If ArticuloCargar.CantMinimaPedido > 1 Then
                        cantidad = ArticuloCargar.CantMinimaPedido
                    Else
                        cantidad = 1
                    End If
                Else
                    cantidad = 1
                End If
                wNumEdCantidad.Text = cantidad.ToString
                wNumEdCantidad.Value = cantidad
                'wNumEdCantidad.Style.Add("text-align", "right")
                CeldaNormal.Controls.AddAt(0, wNumEdCantidad)
                LabelIdOculto = New Label
                LabelIdOculto.Text = IdArt
                LabelIdOculto.ID = "LblIdOculto" & (i)
                LabelIdOculto.Style.Add("Display", "none")
                CeldaNormal.Controls.AddAt(1, LabelIdOculto)
                'ftbeCantidad = New AjaxControlToolkit.FilteredTextBoxExtender
                'ftbeCantidad.ID = "ftbeCant" & (i)
                'ftbeCantidad.FilterMode = AjaxControlToolkit.FilterModes.ValidChars
                'ftbeCantidad.FilterType = AjaxControlToolkit.FilterTypes.Numbers
                'ftbeCantidad.TargetControlID = wNumEdCantidad.UniqueID
                'CeldaNormal.Controls.AddAt(2, ftbeCantidad)
                CeldaNormal.Align = "Right"
                CeldaNormal.Style.Add("Max-Width", "30px")
                FilaDatos.Cells.Add(CeldaNormal)
                TablaBoton = New HtmlTable

                CeldaNormal = New HtmlTableCell

                FSNlnkUnidad = New FSNWebControls.FSNLinkTooltip
                FSNlnkUnidad.Text = ArticuloCargar.UP
                FSNlnkUnidad.ContextKey = oUnidadPedido.Denominacion
                FSNlnkUnidad.ID = "FSNlnkUni" & (i)
                FSNlnkUnidad.CssClass = "Rotulo"
                FSNlnkUnidad.Font.Underline = False
                FSNlnkUnidad.PanelTooltip = "pnlTooltipTexto"

                CeldaNormal.Controls.AddAt(0, FSNlnkUnidad)

                CeldaNormal.Align = "Left"
                CeldaNormal.Style.Add("Max-Width", "30px")
                FilaDatos.Cells.Add(CeldaNormal)

                FilaTablaboton = New HtmlTableRow
                CeldaNormal = New HtmlTableCell
                BtnAnyadirCesta = New FSNWebControls.FSNButton
                BtnAnyadirCesta.ID = "btnAnyadirCesta" & (i)
                BtnAnyadirCesta.CssClass = "BotonCatalogo"
                BtnAnyadirCesta.Alineacion = FSNWebControls.FSNTipos.Alineacion.Left
                AddHandler BtnAnyadirCesta.Click, AddressOf AnyadirCestaClick
                BtnAnyadirCesta.ToolTip = Textos(87)
                BtnAnyadirCesta.Text = Textos(131)
                CeldaTablaBoton.Style.Add("Width", "10%")
                FilaTablaboton.Cells.Add(CeldaTablaBoton)
                CeldaTablaBoton = New HtmlTableCell
                CeldaTablaBoton.Style.Add("Width", "80%")
                CeldaTablaBoton.Controls.Add(BtnAnyadirCesta)
                FilaTablaboton.Cells.Add(CeldaTablaBoton)
                CeldaTablaBoton = New HtmlTableCell
                CeldaTablaBoton.Style.Add("Width", "10%")
                FilaTablaboton.Cells.Add(CeldaTablaBoton)
                TablaBoton.Rows.Add(FilaTablaboton)
                CeldaNormal.Controls.Add(TablaBoton)
                CeldaNormal.Align = "Center"
                FilaDatos.Cells.Add(CeldaNormal)
                CeldaNormal = New HtmlTableCell
                If ArticuloCargar.ModificarPrec = False Then
                    ModificarPrecio = False
                ElseIf FSNUser.ModificarPrecios AndAlso ArticuloCargar.ModificarPrec = False Then
                    ModificarPrecio = True
                Else
                    ModificarPrecio = False
                End If

                If ModificarPrecio Then
                    Tbprecio = New TextBox
                    Tbprecio.Text = modUtilidades.FormatNumber(CDbl(ArticuloCargar.PrecioUC * ArticuloCargar.FC), FSNUser.NumberFormat)
                    Tbprecio.ID = "tbPrecArt" & (i)
                    Tbprecio.CssClass = "Normal"
                    Tbprecio.Width = "30px"
                    CeldaNormal.Controls.Add(Tbprecio)
                Else
                    LabelPrecio = New Label
                    LabelPrecio.Text = modUtilidades.FormatNumber(CDbl(ArticuloCargar.PrecioUC * ArticuloCargar.FC), FSNUser.NumberFormat)
                    LabelPrecio.ID = "LblPrecArt" & (i)
                    LabelPrecio.CssClass = "Normal"
                    CeldaNormal.Controls.Add(LabelPrecio)
                End If
                CeldaNormal.Align = "Right"
                FilaDatos.Cells.Add(CeldaNormal)
                CeldaNormal = New HtmlTableCell
                LabelMoneda = New Label
                LabelMoneda.Text = ArticuloCargar.Mon
                LabelMoneda.CssClass = "Normal"
                CeldaNormal.Controls.Add(LabelMoneda)
                FilaDatos.Cells.Add(CeldaNormal)
                CeldaNormal = New HtmlTableCell
                LnkBtnNombre = New LinkButton
                If FSNUser.MostrarCodArt Then
                    LnkBtnNombre.Text = ArticuloCargar.Cod & " - " & ArticuloCargar.Den
                Else
                    LnkBtnNombre.Text = ArticuloCargar.Den
                End If
                LnkBtnNombre.Font.Underline = False
                LnkBtnNombre.CssClass = "Rotulo"
                LnkBtnNombre.ID = "LblNom" & (i)
                AddHandler LnkBtnNombre.Click, AddressOf CargarDetalleArticulo
                LnkBtnNombre.ToolTip = Textos(128)
                LnkBtnNombre.CommandArgument = IdArt
                CeldaNormal.Controls.Add(LnkBtnNombre)
                FilaDatos.Cells.Add(CeldaNormal)
                If FSNUser.MostrarAtrib Then
                    'Aqui Meto las celdas vacias
                    Dim newCelda As New HtmlTableCell
                    Dim NewLbldenAtrib As New Label
                    For k As Integer = 1 To 15
                        Dim strContAtrib As String
                        If k > 10 Then
                            strContAtrib = k.ToString
                        Else
                            strContAtrib = "0" & k.ToString
                        End If
                        newCelda = New HtmlTableCell
                        NewLbldenAtrib = New Label
                        NewLbldenAtrib.CssClass = "Normal"
                        NewLbldenAtrib.ID = "LblAtribVal" & (i) & strContAtrib
                        newCelda.Controls.Add(NewLbldenAtrib)
                        newCelda.ID = "AtribValCelda" & (i) & strContAtrib
                        newCelda.Visible = False
                        FilaDatos.Cells.Add(newCelda)
                    Next
                    Dim DsAtrib As New DataSet
                    Dim AtribsArt As FSNServer.CAtributos = Me.FSNServer.Get_Object(GetType(FSNServer.CAtributos))
                    Dim oCArticulos As FSNServer.cArticulos = Me.FSNServer.Get_Object(GetType(FSNServer.cArticulos))
                    DsAtrib = oCArticulos.DevolverAtributosArt(ArticuloCargar.Cod, ArticuloCargar.Prove)
                    If Not DsAtrib.Tables(0).Rows.Count = 0 Then
                        For Each fila As DataRow In DsAtrib.Tables(0).Rows
                            If ColumnasEliminadas Is Nothing OrElse Not ColumnasEliminadas.Contains(fila.Item("Den")) Then
                                If Not DsCarga.Tables(0).Columns.Contains(fila.Item("den")) Then
                                    DsCarga.Tables(0).Columns.Add(fila.Item("den"))
                                    Select Case ContadorAtributos
                                        Case 1
                                            CelCabAtrib1.Visible = True
                                            CeldaNormal.ID = "AtribValCelda" & (i) & "01"
                                            LblCabAtrib1.Text = fila.Item("den")
                                        Case 2
                                            CelCabAtrib2.Visible = True
                                            CeldaNormal.ID = "AtribValCelda" & (i) & "02"
                                            LblCabAtrib2.Text = fila.Item("den")
                                        Case 3
                                            CelCabAtrib3.Visible = True
                                            CeldaNormal.ID = "AtribValCelda" & (i) & "03"
                                            LblCabAtrib3.Text = fila.Item("den")
                                        Case 4
                                            CelCabAtrib4.Visible = True
                                            CeldaNormal.ID = "AtribValCelda" & (i) & "04"
                                            LblCabAtrib4.Text = fila.Item("den")
                                        Case 5
                                            CelCabAtrib5.Visible = True
                                            CeldaNormal.ID = "AtribValCelda" & (i) & "05"
                                            LblCabAtrib5.Text = fila.Item("den")
                                        Case 6
                                            CelCabAtrib6.Visible = True
                                            CeldaNormal.ID = "AtribValCelda" & (i) & "06"
                                            LblCabAtrib6.Text = fila.Item("den")
                                        Case 7
                                            CelCabAtrib7.Visible = True
                                            CeldaNormal.ID = "AtribValCelda" & (i) & "07"
                                            LblCabAtrib7.Text = fila.Item("den")
                                        Case 8
                                            CelCabAtrib8.Visible = True
                                            CeldaNormal.ID = "AtribValCelda" & (i) & "08"
                                            LblCabAtrib8.Text = fila.Item("den")
                                        Case 9
                                            CelCabAtrib9.Visible = True
                                            CeldaNormal.ID = "AtribValCelda" & (i) & "09"
                                            LblCabAtrib9.Text = fila.Item("den")
                                        Case 10
                                            CeldaNormal.ID = "AtribValCelda" & (i) & "10"
                                            CelCabAtrib10.Visible = True
                                            LblCabAtrib10.Text = fila.Item("den")
                                        Case 11
                                            CeldaNormal.ID = "AtribValCelda" & (i) & "11"
                                            CelCabAtrib11.Visible = True
                                            LblCabAtrib11.Text = fila.Item("den")
                                        Case 12
                                            CelCabAtrib12.Visible = True
                                            LblCabAtrib12.Text = fila.Item("den")
                                            CeldaNormal.ID = "AtribValCelda" & (i) & "12"
                                        Case 13
                                            CelCabAtrib13.Visible = True
                                            LblCabAtrib13.Text = fila.Item("den")
                                            CeldaNormal.ID = "AtribValCelda" & (i) & "13"
                                        Case 14
                                            CelCabAtrib14.Visible = True
                                            LblCabAtrib14.Text = fila.Item("den")
                                            CeldaNormal.ID = "AtribValCelda" & (i) & "14"
                                        Case 15
                                            CelCabAtrib15.Visible = True
                                            CeldaNormal.ID = "AtribValCelda" & (i) & "15"
                                            LblCabAtrib15.Text = fila.Item("den")
                                    End Select
                                    Dim strcontAtrib As String
                                    If ContadorAtributos >= 9 Then
                                        strcontAtrib = (ContadorAtributos + 1).ToString
                                    Else
                                        strcontAtrib = "0" & (ContadorAtributos + 1).ToString
                                    End If
                                    CType(FilaDatos.Cells(6 + ContadorAtributos).Controls(0), Label).Text = fila.Item("valor")
                                    If EsNumerico(DBNullToStr(fila.Item("Den"))) Then
                                        CType(FilaDatos.Cells(6 + ContadorAtributos).Controls(0), Label).Style.Add("text-align", "right")
                                        FilaDatos.Cells(6 + ContadorAtributos).Align = "Right"
                                    End If
                                    FilaDatos.Cells.Add(CeldaNormal)
                                    ContadorAtributos = ContadorAtributos + 1
                                Else
                                    Select Case fila.Item("den")
                                        Case LblCabAtrib1.Text
                                            If EsNumerico(DBNullToStr(fila.Item("Den"))) Then
                                                CType(FilaDatos.Cells(6).Controls(0), LinkButton).Style.Add("text-align", "right")
                                                FilaDatos.Cells(6).Align = "Right"
                                            End If
                                            CType(FilaDatos.Cells(6), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(6).Controls(0), LinkButton).Text = fila.Item("valor")
                                        Case LblCabAtrib2.Text
                                            If EsNumerico(DBNullToStr(fila.Item("Den"))) Then
                                                CType(FilaDatos.Cells(7).Controls(0), Label).Style.Add("text-align", "right")
                                                FilaDatos.Cells(7).Align = "Right"
                                            End If
                                            CType(FilaDatos.Cells(6), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(7), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(7).Controls(0), Label).Text = fila.Item("valor")
                                        Case LblCabAtrib3.Text
                                            If EsNumerico(DBNullToStr(fila.Item("Den"))) Then
                                                CType(FilaDatos.Cells(8).Controls(0), Label).Style.Add("text-align", "right")
                                                FilaDatos.Cells(8).Align = "Right"
                                            End If
                                            CType(FilaDatos.Cells(6), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(7), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(8), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(8).Controls(0), Label).Text = fila.Item("valor")
                                        Case LblCabAtrib4.Text
                                            If EsNumerico(DBNullToStr(fila.Item("Den"))) Then
                                                CType(FilaDatos.Cells(9).Controls(0), Label).Style.Add("text-align", "right")
                                                FilaDatos.Cells(9).Align = "Right"
                                            End If
                                            CType(FilaDatos.Cells(6), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(7), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(8), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(9), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(9).Controls(0), Label).Text = fila.Item("valor")
                                        Case LblCabAtrib5.Text
                                            If EsNumerico(DBNullToStr(fila.Item("Den"))) Then
                                                CType(FilaDatos.Cells(10).Controls(0), Label).Style.Add("text-align", "right")
                                                FilaDatos.Cells(10).Align = "Right"
                                            End If
                                            CType(FilaDatos.Cells(6), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(7), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(8), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(9), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(10), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(10).Controls(0), Label).Text = fila.Item("valor")
                                        Case LblCabAtrib6.Text
                                            If EsNumerico(DBNullToStr(fila.Item("Den"))) Then
                                                CType(FilaDatos.Cells(11).Controls(0), Label).Style.Add("text-align", "right")
                                                FilaDatos.Cells(11).Align = "Right"
                                            End If
                                            CType(FilaDatos.Cells(6), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(7), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(8), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(9), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(10), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(11), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(11).Controls(0), Label).Text = fila.Item("valor")
                                        Case LblCabAtrib7.Text
                                            If EsNumerico(DBNullToStr(fila.Item("Den"))) Then
                                                CType(FilaDatos.Cells(12).Controls(0), Label).Style.Add("text-align", "right")
                                                FilaDatos.Cells(12).Align = "Right"
                                            End If
                                            CType(FilaDatos.Cells(6), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(7), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(8), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(9), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(10), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(11), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(12), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(12).Controls(0), Label).Text = fila.Item("valor")
                                        Case LblCabAtrib8.Text
                                            If EsNumerico(DBNullToStr(fila.Item("Den"))) Then
                                                CType(FilaDatos.Cells(13).Controls(0), Label).Style.Add("text-align", "right")
                                                FilaDatos.Cells(13).Align = "Right"
                                            End If
                                            CType(FilaDatos.Cells(6), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(7), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(8), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(9), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(10), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(11), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(12), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(13), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(13).Controls(0), Label).Text = fila.Item("valor")
                                        Case LblCabAtrib9.Text
                                            If EsNumerico(DBNullToStr(fila.Item("Den"))) Then
                                                CType(FilaDatos.Cells(14).Controls(0), Label).Style.Add("text-align", "right")
                                                FilaDatos.Cells(14).Align = "Right"
                                            End If
                                            CType(FilaDatos.Cells(6), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(7), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(8), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(9), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(10), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(11), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(12), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(13), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(14), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(14).Controls(0), Label).Text = fila.Item("valor")
                                        Case LblCabAtrib10.Text
                                            If EsNumerico(DBNullToStr(fila.Item("Den"))) Then
                                                CType(FilaDatos.Cells(15).Controls(0), Label).Style.Add("text-align", "right")
                                                FilaDatos.Cells(15).Align = "Right"
                                            End If
                                            CType(FilaDatos.Cells(6), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(7), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(8), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(9), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(10), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(11), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(12), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(13), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(14), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(15), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(15).Controls(0), Label).Text = fila.Item("valor")
                                        Case LblCabAtrib11.Text
                                            If EsNumerico(DBNullToStr(fila.Item("Den"))) Then
                                                CType(FilaDatos.Cells(16).Controls(0), Label).Style.Add("text-align", "right")
                                                FilaDatos.Cells(16).Align = "Right"
                                            End If
                                            CType(FilaDatos.Cells(6), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(7), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(8), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(9), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(10), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(11), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(12), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(13), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(14), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(15), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(16), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(16).Controls(0), Label).Text = fila.Item("valor")
                                        Case LblCabAtrib12.Text
                                            If EsNumerico(DBNullToStr(fila.Item("Den"))) Then
                                                CType(FilaDatos.Cells(17).Controls(0), Label).Style.Add("text-align", "right")
                                                FilaDatos.Cells(17).Align = "Right"
                                            End If
                                            CType(FilaDatos.Cells(6), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(7), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(8), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(9), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(10), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(11), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(12), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(13), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(14), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(15), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(16), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(17), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(17).Controls(0), Label).Text = fila.Item("valor")
                                        Case LblCabAtrib13.Text
                                            If EsNumerico(DBNullToStr(fila.Item("Den"))) Then
                                                CType(FilaDatos.Cells(18).Controls(0), Label).Style.Add("text-align", "right")
                                                FilaDatos.Cells(18).Align = "Right"
                                            End If
                                            CType(FilaDatos.Cells(6), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(7), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(8), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(9), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(10), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(11), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(12), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(13), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(14), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(15), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(16), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(17), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(18), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(18).Controls(0), Label).Text = fila.Item("valor")
                                        Case LblCabAtrib14.Text
                                            If EsNumerico(DBNullToStr(fila.Item("Den"))) Then
                                                CType(FilaDatos.Cells(19).Controls(0), Label).Style.Add("text-align", "right")
                                                FilaDatos.Cells(19).Align = "Right"
                                            End If
                                            CType(FilaDatos.Cells(6), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(7), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(8), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(9), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(10), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(11), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(12), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(13), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(14), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(15), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(16), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(17), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(18), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(19), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(19).Controls(0), Label).Text = fila.Item("valor")
                                        Case LblCabAtrib15.Text
                                            If EsNumerico(DBNullToStr(fila.Item("Den"))) Then
                                                CType(FilaDatos.Cells(20).Controls(0), Label).Style.Add("text-align", "right")
                                                FilaDatos.Cells(20).Align = "Right"
                                            End If
                                            CType(FilaDatos.Cells(6), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(7), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(8), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(9), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(10), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(11), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(12), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(13), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(14), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(15), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(16), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(17), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(18), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(19), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(20), HtmlTableCell).Visible = True
                                            CType(FilaDatos.Cells(20).Controls(0), Label).Text = fila.Item("valor")
                                    End Select
                                End If
                            End If
                        Next
                    End If
                End If
                If i Mod 2 = 0 Then
                    FilaDatos.BgColor = "#FAFAFA"
                Else
                    FilaDatos.BgColor = "#F1F1F1"
                End If
                FilaDatos.ID = "FilaDatos" & (i)
                FilaDatos.EnableViewState = True
                'FilaDatos.Style.Add("Padding-top", "5")
                'FilaDatos.Style.Add("Padding-bottom", "5")
                'FilaDatos.Style.Add("Padding-left", "10")
                'FilaDatos.Style.Add("Padding-right", "10")
                FilaDatos.Style.Add("margin-top", "5")
                FilaDatos.Style.Add("margin-bottom", "5")
                FilaDatos.Style.Add("margin-left", "8")
                FilaDatos.Style.Add("margin-right", "8")
                tCabComparativa.Rows.Add(FilaDatos)
            End If
        Next
        ViewState("DsCarga") = DsCarga
        upTabla.Update()
    End Sub

    ''' <summary>
    ''' Función que devuelve un objeto de la clase cArticulo contenido en el DataSet que cargamos inicialmente
    ''' </summary>
    ''' <param name="Id">Identificador del artículo a cargar</param>
    ''' <returns>Un objeto de la clase cArticulo con sus datos ya rellenados</returns>
    ''' <remarks>
    ''' Llamada desde: El método DisenyarGridArticulos
    ''' Tiempo máximo: 0 seg</remarks>
    Public Function DevolverArticulo(ByVal Id As Integer) As FSNServer.CArticulo
        Dim oArtDevolver As FSNServer.CArticulo = Me.FSNServer.Get_Object(GetType(FSNServer.CArticulo))
        If Not DsCarga.Tables.Count = 0 AndAlso Not DsCarga.Tables(0).Rows.Count = 0 Then
            With oArtDevolver
                For Each fila As DataRow In DsCarga.Tables(0).Rows
                    If fila.Item("ID") = Id Then
                        .Cat1 = DBNullToDec(fila.Item("Cat1"))
                        .Cat2 = DBNullToDec(fila.Item("Cat2"))
                        .Cat3 = DBNullToDec(fila.Item("Cat3"))
                        .Cat4 = DBNullToDec(fila.Item("Cat4"))
                        .Cat5 = DBNullToDec(fila.Item("Cat5"))
                        .CantAdj = DBNullToDec(fila.Item("Cant_adj"))
                        .CantMinimaPedido = DBNullToDec(fila.Item("Cant_Min_Def"))
                        .Cod = DBNullToStr(fila.Item("Cod_Item"))
                        .CodExt = DBNullToStr(fila.Item("Cod_Ext"))
                        .Den = DBNullToStr(fila.Item("Art_Den"))
                        .Dest = DBNullToStr(fila.Item("Dest"))
                        .Cambio = DBNullToStr(fila.Item("EQUIV"))
                        .Esp = DBNullToStr(fila.Item("Esp"))
                        .FC = DBNullToStr(fila.Item("FC_def"))
                        .ID = Id
                        .ModificarPrec = DBNullToStr(fila.Item("Modif_Prec"))
                        .Mon = DBNullToStr(fila.Item("Mon"))
                        .PrecioUC = DBNullToStr(fila.Item("Precio_Uc"))
                        .Prove = DBNullToStr(fila.Item("PRove"))
                        .proveden = DBNullToStr(fila.Item("Proveden"))
                        .UP = DBNullToStr(fila.Item("Up_def"))
                    End If
                Next
            End With
        Else : Return Nothing
        End If
        Return oArtDevolver
    End Function

    ''' <summary>
    ''' Procedimiento que se encarga de eliminar las columnas que el usuario pincha para eliminar.Volviendo esas columnas no visibles
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde:El método EliminarColumna y desde el Page_load
    ''' Tiempo máximo: 0 seg</remarks>
    Public Sub ComprobarCeldasVacias()
        If CelCabAtrib1.Visible = False Then
            For Each fila As HtmlTableRow In tCabComparativa.Rows
                If fila.Cells.Count > 6 Then
                    fila.Cells(6).Visible = False
                    CType(Page.FindControl(fila.Cells(6).UniqueID), HtmlTableCell).Visible = False
                End If
            Next
        End If
        If CelCabAtrib2.Visible = False Then
            For Each fila As HtmlTableRow In tCabComparativa.Rows
                If fila.Cells.Count > 7 Then
                    fila.Cells(7).Visible = False
                    CType(Page.FindControl(fila.Cells(7).UniqueID), HtmlTableCell).Visible = False
                End If
            Next
        End If
        If CelCabAtrib3.Visible = False Then
            For Each fila As HtmlTableRow In tCabComparativa.Rows
                If fila.Cells.Count > 8 Then
                    fila.Cells(8).Visible = False
                    CType(Page.FindControl(fila.Cells(8).UniqueID), HtmlTableCell).Visible = False
                End If
            Next
        End If
        If CelCabAtrib4.Visible = False Then
            For Each fila As HtmlTableRow In tCabComparativa.Rows
                If fila.Cells.Count > 9 Then
                    fila.Cells(9).Visible = False
                    CType(Page.FindControl(fila.Cells(9).UniqueID), HtmlTableCell).Visible = False
                End If
            Next
        End If
        If CelCabAtrib5.Visible = False Then
            For Each fila As HtmlTableRow In tCabComparativa.Rows
                If fila.Cells.Count > 10 Then
                    fila.Cells(10).Visible = False
                    CType(Page.FindControl(fila.Cells(10).UniqueID), HtmlTableCell).Visible = False
                End If
            Next
        End If
        If CelCabAtrib6.Visible = False Then
            For Each fila As HtmlTableRow In tCabComparativa.Rows
                If fila.Cells.Count > 11 Then
                    fila.Cells(11).Visible = False
                    CType(Page.FindControl(fila.Cells(11).UniqueID), HtmlTableCell).Visible = False
                End If
            Next
        End If
        If CelCabAtrib7.Visible = False Then
            For Each fila As HtmlTableRow In tCabComparativa.Rows
                If fila.Cells.Count > 12 Then
                    fila.Cells(12).Visible = False
                    CType(Page.FindControl(fila.Cells(12).UniqueID), HtmlTableCell).Visible = False
                End If
            Next
        End If
        If CelCabAtrib8.Visible = False Then
            For Each fila As HtmlTableRow In tCabComparativa.Rows
                If fila.Cells.Count > 13 Then
                    fila.Cells(13).Visible = False
                    CType(Page.FindControl(fila.Cells(13).UniqueID), HtmlTableCell).Visible = False
                End If
            Next
        End If
        If CelCabAtrib9.Visible = False Then
            For Each fila As HtmlTableRow In tCabComparativa.Rows
                If fila.Cells.Count > 14 Then
                    fila.Cells(14).Visible = False
                    CType(Page.FindControl(fila.Cells(14).UniqueID), HtmlTableCell).Visible = False
                End If
            Next
        End If
        If CelCabAtrib10.Visible = False Then
            For Each fila As HtmlTableRow In tCabComparativa.Rows
                If fila.Cells.Count > 15 Then
                    fila.Cells(15).Visible = False
                    CType(Page.FindControl(fila.Cells(15).UniqueID), HtmlTableCell).Visible = False
                End If
            Next
        End If
        If CelCabAtrib11.Visible = False Then
            For Each fila As HtmlTableRow In tCabComparativa.Rows
                If fila.Cells.Count > 16 Then
                    fila.Cells(16).Visible = False
                    CType(Page.FindControl(fila.Cells(16).UniqueID), HtmlTableCell).Visible = False
                End If
            Next
        End If
        If CelCabAtrib12.Visible = False Then
            For Each fila As HtmlTableRow In tCabComparativa.Rows
                If fila.Cells.Count > 17 Then
                    fila.Cells(17).Visible = False
                    CType(Page.FindControl(fila.Cells(17).UniqueID), HtmlTableCell).Visible = False
                End If
            Next
        End If
        If CelCabAtrib13.Visible = False Then
            For Each fila As HtmlTableRow In tCabComparativa.Rows
                If fila.Cells.Count > 18 Then
                    fila.Cells(18).Visible = False
                    CType(Page.FindControl(fila.Cells(18).UniqueID), HtmlTableCell).Visible = False
                End If
            Next
        End If
        If CelCabAtrib14.Visible = False Then
            For Each fila As HtmlTableRow In tCabComparativa.Rows
                If fila.Cells.Count > 19 Then
                    fila.Cells(19).Visible = False
                    CType(Page.FindControl(fila.Cells(19).UniqueID), HtmlTableCell).Visible = False
                End If
            Next
        End If
        If CelCabAtrib15.Visible = False Then
            For Each fila As HtmlTableRow In tCabComparativa.Rows
                If fila.Cells.Count > 20 Then
                    fila.Cells(20).Visible = False
                    CType(Page.FindControl(fila.Cells(20).UniqueID), HtmlTableCell).Visible = False
                End If
            Next
        End If
    End Sub

    ''' <summary>
    ''' Elimina una columna de los atributos del artículo, permanentemente
    ''' </summary>
    ''' <param name="sender">El boton en forma de "X" que esta en la cabecera de la columna</param>
    ''' <param name="e">el evento de clicado del boton</param>
    ''' <remarks>
    ''' Llamada desde:La `página cada vez que se clica uno de los botones
    ''' Tiempo máximo:0 seg</remarks>
    Public Sub EliminarColumna(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles ImgBtnElimCol1.Click, ImgBtnElimCol2.Click, ImgBtnElimCol3.Click, ImgBtnElimCol4.Click, ImgBtnElimCol5.Click, ImgBtnElimCol6.Click, ImgBtnElimCol7.Click, ImgBtnElimCol8.Click, ImgBtnElimCol9.Click, ImgBtnElimCol10.Click, ImgBtnElimCol11.Click, ImgBtnElimcol12.Click, ImgBtnElimCol13.Click, ImgBtnElimCol14.Click, ImgBtnElimCol15.Click
        Dim NumCol As Integer
        Dim NombreCol As String = String.Empty
        NumCol = CInt(Right(CType(sender, ImageButton).ID, 1))
        Select Case NumCol
            Case 1
                CelCabAtrib1.Visible = False
                NombreCol = LblCabAtrib1.Text
            Case 2
                CelCabAtrib2.Visible = False
                NombreCol = LblCabAtrib2.Text
            Case 3
                CelCabAtrib3.Visible = False
                NombreCol = LblCabAtrib3.Text
            Case 4
                CelCabAtrib4.Visible = False
                NombreCol = LblCabAtrib4.Text
            Case 5
                CelCabAtrib5.Visible = False
                NombreCol = LblCabAtrib5.Text
            Case 6
                CelCabAtrib6.Visible = False
                NombreCol = LblCabAtrib6.Text
            Case 7
                CelCabAtrib7.Visible = False
                NombreCol = LblCabAtrib7.Text
            Case 8
                CelCabAtrib8.Visible = False
                NombreCol = LblCabAtrib8.Text
            Case 9
                CelCabAtrib9.Visible = False
                NombreCol = LblCabAtrib9.Text
            Case 10
                CelCabAtrib10.Visible = False
                NombreCol = LblCabAtrib10.Text
            Case 11
                CelCabAtrib11.Visible = False
                NombreCol = LblCabAtrib11.Text
            Case 12
                CelCabAtrib12.Visible = False
                NombreCol = LblCabAtrib12.Text
            Case 13
                CelCabAtrib13.Visible = False
                NombreCol = LblCabAtrib13.Text
            Case 14
                CelCabAtrib14.Visible = False
                NombreCol = LblCabAtrib14.Text
            Case 15
                CelCabAtrib15.Visible = False
                NombreCol = LblCabAtrib15.Text
        End Select
        If ColumnasEliminadas Is Nothing Then
            ReDim Preserve ColumnasEliminadas(0)
            ColumnasEliminadas(0) = NombreCol
        Else
            ReDim Preserve ColumnasEliminadas(ColumnasEliminadas.Count)
            ColumnasEliminadas(ColumnasEliminadas.Count - 1) = NombreCol
        End If
        If Not AtribsNumericos Is Nothing AndAlso Not AtribsNumericos.Count = 0 Then
            For Each atributo As AtribNumericos In AtribsNumericos
                If atributo.DenAtrib = NombreCol Then
                    atributo.EstaEliminado = True
                    Exit For
                End If
            Next
        End If
        ViewState("AtribsNumericos") = AtribsNumericos
        For Each colum As DataColumn In DsCarga.Tables(0).Columns
            If colum.ColumnName = NombreCol Then
                DsCarga.Tables(0).Columns.Remove(colum)
                Exit For
            End If
        Next
        DsCarga.Tables(0).AcceptChanges()
        Session("FilaCabecera") = fCabComparativa
        tCabComparativa.Rows.Clear()
        FilaCabecera = Session("FilaCabecera")
        tCabComparativa.Rows.Add(FilaCabecera)
        Call DisenyarGridArticulos()
        Call ComprobarCeldasVacias()
        ViewState("DsCarga") = DsCarga
        ViewState("ColumnasEliminadas") = ColumnasEliminadas
        upTabla.Update()
    End Sub

    ''' <summary>
    ''' Elimina un artículo(una fila) de la comparativa actual.
    ''' </summary>
    ''' <param name="sender">El boton que se encuentra a la izquierda de cada fila con una "x" negra</param>
    ''' <param name="e">El evento de clicado de la imagen</param>
    ''' <remarks>
    ''' Llamada desde:La propia página, cada vez que se clica una imagen
    ''' Tiempo máximo:0 seg</remarks>
    Public Sub EliminarFila(ByVal sender As Object, ByVal e As ImageClickEventArgs) 'Handles ImgBtnElimFila1.Click ', ImgBtnElimFila2.Click, ImgBtnElimFila3.Click, ImgBtnElimFila4.Click, ImgBtnElimFila5.Click, ImgBtnElimFila6.Click, ImgBtnElimFila7.Click
        Dim NumFila As Integer
        Dim IdArt As Integer
        NumFila = CInt(Right(CType(sender, ImageButton).ID, 1))
        IdArt = CInt(CType(CType(tCabComparativa.FindControl("FilaDatos" & NumFila), HtmlTableRow).FindControl("LblIdOculto" & NumFila), Label).Text)
        Dim ArrayAux As String() = {}
        For cont As Integer = 0 To ArticulosComparar.Count - 1
            If ArticulosComparar(cont) = IdArt.ToString Then
                Array.Clear(ArticulosComparar, cont, 1)
            Else
                If ArrayAux Is Nothing Then
                    ReDim Preserve ArrayAux(0)
                    ArrayAux(0) = ArticulosComparar(cont)
                Else
                    ReDim Preserve ArrayAux(ArrayAux.Count)
                    ArrayAux(ArrayAux.Count - 1) = ArticulosComparar(cont)
                End If
            End If
        Next
        ReDim ArticulosComparar(0)
        ArticulosComparar = ArrayAux
        Call EliminarAtributosVacios(IdArt)
        For Each fila As DataRow In DsCarga.Tables(0).Rows
            If fila.Item("id") = IdArt Then
                fila.Delete()
                fila.AcceptChanges()
                Exit For
            End If
        Next
        DsCarga.AcceptChanges()
        'tCabComparativa.Rows.Clear()
        'FilaCabecera = Session("FilaCabecera")
        'tCabComparativa.Rows.Add(FilaCabecera)
        'Nuevo
        Session("FilaCabecera") = fCabComparativa
        tCabComparativa.Rows.Clear()
        FilaCabecera = Session("filaCabecera")
        tCabComparativa.Rows.Add(FilaCabecera)
        AtributosCargados = False
        'Hasta aqui
        Call DisenyarGridArticulos()
        Call ComprobarCeldasVacias()
        upTabla.Update()
        ViewState("DsCarga") = DsCarga
        ViewState("ColumnasEliminadas") = ColumnasEliminadas
        Session("ArticulosComparar") = ArticulosComparar
        If ArticulosComparar Is Nothing OrElse ArticulosComparar.Count = 0 Then
            Master.CabTabComparativa.Text = Textos(89)
        Else
            Master.CabTabComparativa.Text = Textos(89) & "(" & ArticulosComparar.Count & ")"
        End If
        Master.CabUpdPestanyasCatalogo.Update()
    End Sub

    ''' <summary>
    ''' Evento que se ejecuta al pulsar el Botón de Vaciar Comparativa, eliminando todos los artículos de la misma
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento(El boton)</param>
    ''' <param name="e">Los parametros del evento click</param>
    ''' <remarks>
    ''' Llamada desde:La propia página, cada vez que se pulsa el botón de vaciar comparativa
    ''' Tiempo máximo: 0 seg</remarks>
    Protected Sub BtnVaciarComparativa_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnVaciarComparativa.Click
        Dim NewDs As New DataSet
        ArticulosComparar = Nothing
        Session("ArticulosComparar") = ArticulosComparar
        DsCarga = NewDs
        ViewState("DsCarga") = NewDs
        Call DisenyarGridArticulos()
        For Each fila As HtmlTableRow In tCabComparativa.Rows
            fila.Visible = False
        Next
        Master.CabTabComparativa.Text = Textos(89)
        Master.CabUpdPestanyasCatalogo.Update()
        Response.Redirect("CatalogoWeb.aspx")
    End Sub

    ''' <summary>
    ''' Procedimiento que dibuja las celdas de los articulos de la comparativa que no tienen valor en los atributos del resto de articulos que si lo tienen
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: DisenyarGridArticulos
    ''' Tiempo máximo: 0 seg</remarks>
    Public Sub DibujarCeldasVacias()
        For Each fila As HtmlTableRow In tCabComparativa.Rows
            If fila.Visible = True Then
                For Each celda As HtmlTableCell In fila.Cells
                    For Each cont As Control In celda.Controls
                        If TypeOf cont Is Label Then
                            If Not CType(cont, Label).ID Is Nothing AndAlso CType(cont, Label).ID.Contains("Atrib") Then
                                If cont.Visible = False Then
                                    Dim NumCol As String
                                    Dim NumColInt As Integer
                                    NumCol = Right(CType(cont, Label).ID, 2)
                                    If Left(NumCol, 1) <> "1" Then
                                        NumColInt = CInt(Right(NumCol, 1))
                                    Else
                                        NumColInt = CInt(NumCol)
                                    End If
                                    If celda.Visible = False AndAlso CabeceraVisible(NumColInt) Then
                                        celda.Visible = True
                                    End If
                                End If
                            End If
                        End If
                    Next
                Next
            End If
        Next
    End Sub

    ''' <summary>
    ''' Función que comprueba si la cabecera de una columna de la tabla es visible o no.
    ''' </summary>
    ''' <param name="NumCol">Numero de la columna a comprobar</param>
    ''' <returns>Un valor booleano que sera verdadero si es visible y falso en caso contrario</returns>
    ''' <remarks>
    ''' Llamada desde: DibujarCeldasVacias
    ''' Tiempo máximo:0 seg</remarks>
    Public Function CabeceraVisible(ByVal NumCol As Integer) As Boolean
        For Each cont As Control In tCabComparativa.Rows(0).Cells(5 + NumCol).Controls
            If TypeOf cont Is Label Then
                If CType(cont, Label).ID.Contains("Atrib") Then
                    If CType(cont, Label).Visible = True AndAlso CType(cont, Label).Text <> "" Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            End If
        Next
    End Function

    ''' <summary>
    ''' Evento que se ejecuta cuando se clica el botón de Recargar comparativa, recargando los artículos iniciales que el usuario hubiese mandado a comparar
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento, en este caso el botón de "recargar comparativa"</param>
    ''' <param name="e">Los parámetros del evento</param>
    ''' <remarks>
    ''' Llamada desde: La propia página, cada vez que se clica el botón de recargar comparativa
    ''' Tiempo máximo: 0,5 seg</remarks>
    Protected Sub BtnRecargarComparativa_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnRecargarComparativa.Click
        AtributosCargados = False
        Dim DsCargaInicial As New DataSet
        DsCargaInicial = ViewState("DsCargaInicial")
        DsCarga = New DataSet
        DsCarga = DsCargaInicial.Copy
        If Not ColumnasEliminadas Is Nothing AndAlso Not ColumnasEliminadas.Count = 0 Then
            Array.Clear(ColumnasEliminadas, 0, ColumnasEliminadas.Count)
        End If
        Dim ContadorAtributos As Integer = 0
        ReDim ArticulosComparar(0)
        ArticulosComparar = ViewState("ArticulosCompararIni")
        ContadorAtributos = 0
        AtribsNumericos = Nothing
        ViewState("AtribsNumericos") = AtribsNumericos
        ViewState("contadorAtriburos") = ContadorAtributos
        ViewState("DsCarga") = DsCarga
        ViewState("ColumnasEliminadas") = ColumnasEliminadas
        Session("ArticulosComparar") = ArticulosComparar
        Response.Redirect("Comparativa.aspx")
        'Dim FilaCabecera As HtmlTableRow
        'FilaCabecera = Session("FilaCabecera")
        'tCabComparativa.Rows.Clear()
        'tCabComparativa.Rows.Add(FilaCabecera)
        'Call DisenyarGridArticulos()
        'upTabla.Update()   
    End Sub

    ''' <summary>
    ''' Función que elimina las columnas con atributos vacíos, la llamo cuando eliminan un artículo con atributos unicos.
    ''' </summary>
    ''' <param name="IdArticulo">Identificador del artículo eliminado</param>
    ''' <returns>Un valor booleano que indica si habia o no atributos unicos</returns>
    ''' <remarks>
    ''' Llamada desde: EL procedimiento eliminar columna
    ''' Tiempo máximo: 0seg</remarks>
    Public Function EliminarAtributosVacios(ByVal IdArticulo As Integer) As Boolean
        Dim CodArt As String
        Dim CodProve As String
        Dim DsAtribs As New DataSet
        Dim DenAtrib As String
        Dim HayValores As Boolean = True
        For Each fila As DataRow In DsCarga.Tables(0).Rows
            If fila.Item("id") = IdArticulo Then
                CodArt = fila.Item("Cod_item")
                CodProve = fila.Item("Prove")
                Dim oCArticulos As FSNServer.cArticulos = Me.FSNServer.Get_Object(GetType(FSNServer.cArticulos))
                DsAtribs = oCArticulos.DevolverAtributosArt(CodArt, CodProve)
                If Not DsAtribs.Tables(0) Is Nothing AndAlso Not DsAtribs.Tables(0).Rows.Count = 0 Then
                    For Each filaAtribs As DataRow In DsAtribs.Tables(0).Rows
                        DenAtrib = filaAtribs.Item("Den")
                        HayValores = False
                        Dim OtroDsAtrib As New DataSet
                        Dim OtroCodArt As String
                        Dim otroCodProve As String
                        For Each OtraFila As DataRow In DsCarga.Tables(0).Rows
                            If HayValores Then Exit For
                            If OtraFila.Item("Id") <> IdArticulo Then
                                OtroCodArt = OtraFila.Item("Cod_item")
                                otroCodProve = OtraFila.Item("Prove")
                                OtroDsAtrib = oCArticulos.DevolverAtributosArt(OtroCodArt, otroCodProve)
                                If OtroDsAtrib.Tables(0).Rows.Count > 0 Then
                                    For Each OtraFilaAtribs As DataRow In OtroDsAtrib.Tables(0).Rows
                                        If OtraFilaAtribs.Item("den") = DenAtrib Then
                                            HayValores = True
                                            Exit For
                                        End If
                                    Next
                                End If
                            End If
                        Next
                        If Not HayValores Then
                            If ColumnasEliminadas Is Nothing Then
                                ReDim Preserve ColumnasEliminadas(0)
                                ColumnasEliminadas(0) = DenAtrib
                            Else
                                ReDim Preserve ColumnasEliminadas(ColumnasEliminadas.Count)
                                ColumnasEliminadas(ColumnasEliminadas.Count - 1) = DenAtrib
                            End If
                            If Not AtribsNumericos Is Nothing AndAlso Not AtribsNumericos.Count = 0 Then
                                For Each atributo As AtribNumericos In AtribsNumericos
                                    If atributo.DenAtrib = DenAtrib Then
                                        atributo.EstaEliminado = True
                                        Exit For
                                    End If
                                Next
                            End If
                            ViewState("AtribsNumericos") = AtribsNumericos
                            For Each colum As DataColumn In DsCarga.Tables(0).Columns
                                If colum.ColumnName = DenAtrib Then
                                    DsCarga.Tables(0).Columns.Remove(colum)
                                    Exit For
                                End If
                            Next
                            DsCarga.Tables(0).AcceptChanges()
                            Select Case DenAtrib
                                Case LblCabAtrib1.Text
                                    CelCabAtrib1.Visible = False
                                Case LblCabAtrib2.Text
                                    CelCabAtrib2.Visible = False
                                Case LblCabAtrib3.Text
                                    CelCabAtrib3.Visible = False
                                Case LblCabAtrib4.Text
                                    CelCabAtrib4.Visible = False
                                Case LblCabAtrib5.Text
                                    CelCabAtrib5.Visible = False
                                Case LblCabAtrib6.Text
                                    CelCabAtrib6.Visible = False
                                Case LblCabAtrib7.Text
                                    CelCabAtrib7.Visible = False
                                Case LblCabAtrib8.Text
                                    CelCabAtrib8.Visible = False
                                Case LblCabAtrib9.Text
                                    CelCabAtrib9.Visible = False
                                Case LblCabAtrib10.Text
                                    CelCabAtrib10.Visible = False
                                Case LblCabAtrib11.Text
                                    CelCabAtrib11.Visible = False
                                Case LblCabAtrib12.Text
                                    CelCabAtrib12.Visible = False
                                Case LblCabAtrib13.Text
                                    CelCabAtrib13.Visible = False
                                Case LblCabAtrib14.Text
                                    CelCabAtrib14.Visible = False
                                Case LblCabAtrib15.Text
                                    CelCabAtrib15.Visible = False
                            End Select
                        End If
                    Next
                End If
                Exit For
            End If
        Next

    End Function

#Region " Inicialización de controles con Idiomas "
    Protected Sub RangeValidator2_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, RangeValidator).ErrorMessage = Textos(84)
    End Sub
    Protected Sub LblUnd_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, Label).Text = Textos(83)
    End Sub
    '********************************************************
    Public Sub CargarTextos()
        BtnRecargarComparativa.Text = Textos(141)
        BtnRecargarComparativa.ToolTip = Textos(141)
        BtnVaciarComparativa.Text = Textos(139)
        BtnVaciarComparativa.ToolTip = Textos(139)
        lblCantMinCab.Text = Textos(83)
        LblCestaCab.Text = Textos(93)
        LblPrecCab.Text = Textos(108)
        LblMonCab.Text = Textos(9).Replace(":", "")
        LblNomCab.Text = Textos(55)
        ImgBtnElimCol1.ToolTip = Textos(130)
        ImgBtnElimCol2.ToolTip = Textos(130)
        ImgBtnElimCol3.ToolTip = Textos(130)
        ImgBtnElimCol4.ToolTip = Textos(130)
        ImgBtnElimCol5.ToolTip = Textos(130)
        ImgBtnElimCol6.ToolTip = Textos(130)
        ImgBtnElimCol7.ToolTip = Textos(130)
        ImgBtnElimCol8.ToolTip = Textos(130)
        ImgBtnElimCol9.ToolTip = Textos(130)
        ImgBtnElimCol10.ToolTip = Textos(130)
        ImgBtnElimCol11.ToolTip = Textos(130)
        ImgBtnElimcol12.ToolTip = Textos(130)
        ImgBtnElimCol13.ToolTip = Textos(130)
        ImgBtnElimCol14.ToolTip = Textos(130)
        ImgBtnElimCol15.ToolTip = Textos(130)

    End Sub
#End Region

#Region "Unidades"
    ''' <summary>
    ''' Propiedad que recupera el conjunto de las unidades de Pedido para su posterior uso en la pÃ¡gina
    ''' </summary>
    Private ReadOnly Property TodasLasUnidades() As FSNServer.CUnidadesPedido
        Get
            Dim oUnidadesPedido As FSNServer.CUnidadesPedido = Me.FSNServer.Get_Object(GetType(FSNServer.CUnidadesPedido))
            If HttpContext.Current.Cache("TodasLasUnidades" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                oUnidadesPedido.CargarTodasLasUnidades(Me.Usuario.Idioma)
                Me.InsertarEnCache("TodasLasUnidades" & Me.Usuario.Idioma.ToString(), oUnidadesPedido)
            Else
                oUnidadesPedido = HttpContext.Current.Cache("TodasLasUnidades" & Me.Usuario.Idioma.ToString())
            End If
            Return oUnidadesPedido
        End Get
    End Property

    ''' <summary>
    ''' Método que recupera el número de decimales para una unidad de pedido dada y configura la presentación de los datos en los controles webnumericEdit
    ''' </summary>
    ''' <param name="unidad">Código de la unidad de pedido</param>
    ''' <param name="wNumericEdit">Control webNumericEdit a configurar</param>
    ''' <remarks>Llamada desde: la página, allí donde se configure un control webNumericEdit. Tiempo máximo: inferior a 0,3</remarks>
    Private Sub configurarDecimales(ByVal unidad As Object, ByRef wNumericEdit As Infragistics.Web.UI.EditorControls.WebNumericEditor)
        Dim oUnidadPedido As FSNServer.CUnidadPedido = Nothing
        If Not DBNullToSomething(unidad) = Nothing Then
            unidad = Trim(unidad.ToString)
            oUnidadPedido = TodasLasUnidades.Item(Trim(unidad))
            oUnidadPedido.AsignarFormatoaUnidadPedido(Me.Usuario.NumberFormat)
        End If
        If Not oUnidadPedido Is Nothing Then
            Dim ci As System.Globalization.CultureInfo = Threading.Thread.CurrentThread.CurrentCulture.Clone()
            ci.NumberFormat = oUnidadPedido.UnitFormat
            ci.NumberFormat.NumberDecimalDigits = 15
            wNumericEdit.Culture = ci
            If oUnidadPedido.NumeroDeDecimales Is Nothing Then
                wNumericEdit.MinDecimalPlaces = 0
                wNumericEdit.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Double
            ElseIf oUnidadPedido.NumeroDeDecimales > 0 Then
                wNumericEdit.MinDecimalPlaces = oUnidadPedido.NumeroDeDecimales
                wNumericEdit.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Double
            ElseIf oUnidadPedido.NumeroDeDecimales = 0 Then
                wNumericEdit.MinDecimalPlaces = oUnidadPedido.NumeroDeDecimales
                wNumericEdit.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Int
            End If
            wNumericEdit.ClientEvents.TextChanged = "limiteDecimalesTextChanged"
            wNumericEdit.Attributes.Add("numeroDecimales", ci.NumberFormat.NumberDecimalDigits)
            wNumericEdit.Attributes.Add("separadorDecimales", ci.NumberFormat.NumberDecimalSeparator)
            wNumericEdit.Attributes.Add("avisoDecimales", Textos(158) & " " & oUnidadPedido.Codigo & ":")
            wNumericEdit.Buttons.SpinOnArrowKeys = True
        End If
    End Sub
#End Region

#Region "WebMethods"
    <System.Web.Services.WebMethodAttribute(), _
    System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function DevolverTexto(ByVal contextKey As String) As String
        Return contextKey
    End Function
#End Region


End Class