﻿Imports Fullstep.FSNServer
Imports Infragistics.Web.UI.GridControls
Imports System.Web.Script.Serialization

Partial Public Class Recepcion
    Inherits FSEPPage

    Private _sEstados(3) As String
    Private _sTipos(5) As String
    Private _PaginadorTop As PaginadorWebHierarchical
    Private _PaginadoEntregas As Boolean
    Private _AdjuntosRecepcion As CAdjuntos
    Private dsLineasRecep As DataSet
    Private contDesviosCero As Integer
    Private contLineas As Integer

    ''' Revisado por:blp. Fecha: 21/08/2012
    ''' <summary>
    ''' PreCargar la pagina. 
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>        
    ''' <remarks>Llamada desde:sistema; Tiempo máximo:0</remarks>
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not Me.IsPostBack Then
            'Vaciamos la caché porque es una carga nueva. De otro modo, sería necesario controlar los filtros para saber si la carga ha cambiado
            EliminarCachePedidos(0, Me.Usuario.CodPersona, False, True, False)

			InicializarControlesExportacionYConfiguracion()
		End If

        Me._PaginadorTop = TryCast(Me.whdgEntregas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("wdgPaginador"), PaginadorWebHierarchical)
        AddHandler Me._PaginadorTop.PageChanged, AddressOf _Paginador_PageChanged
    End Sub

    ''' Revisado por: blp. Fecha:09/03/2012
    ''' <summary>
    ''' Pone imágenes a los controles de exportación
    ''' </summary>
    ''' <remarks>Llamada desde Load. Máx. 0,01 seg.</remarks>
    Private Sub InicializarControlesExportacionYConfiguracion()
        ibExcel.Style("background-image") = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/Excel.png"
        ibPDF.Style("background-image") = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/PDF.png"
        ibConfigurar.Style("background-image") = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/configurar.png"
        ibGuardarConfiguracion.Style("background-image") = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/emitir.gif"
        imgPanelConfiguracion.ImageUrl = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/configurar_big.png"
        ibRegistrarRecepcion.Style("background-image") = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/recepcion_very_small.png"
    End Sub

    ''' Revisado por: blp. Fecha: 07/12/2011
    ''' <summary>
    ''' Configura la grid para su posterior exportación. Exporta la Grid a una hoja excel o a Pdf
    ''' </summary>
    ''' <param name="iTipoExportacion"> Tipo Exportacion (Excel = 1 // Pdf = 2)</param>
    ''' <remarks>Llamada desde:Page_load; Tiempo ejecucion:0,3seg.</remarks>
    Private Sub Exportar(ByVal iTipoExportacion As Byte)
        CargarGridExportacion()

        If iTipoExportacion = 1 Then 'Excel
            WebExcelExporter1.DownloadName = ReemplazaTexto(Date.Now.ToShortDateString & "_" & Textos(0) & ".xls")
            WebExcelExporter1.DataExportMode = Infragistics.Web.UI.GridControls.DataExportMode.AllDataInDataSource
            WebExcelExporter1.EnableStylesExport = False
			WebExcelExporter1.Export(whdgEntregasExportacion.GridView)
		Else 'PDF
            With WebDocumentExporter1
                .TargetPaperOrientation = Infragistics.Documents.Reports.Report.PageOrientation.Landscape
                .DownloadName = ReemplazaTexto(Date.Now.ToShortDateString & "_" & Textos(0))
                .Format = Infragistics.Documents.Reports.Report.FileFormat.PDF
                .EnableStylesExport = True
                .DataExportMode = Infragistics.Web.UI.GridControls.DataExportMode.AllDataInDataSource
                .CustomFont = New System.Drawing.Font("Arial, Verdana", 4, System.Drawing.GraphicsUnit.Pixel)
                .Margins = Infragistics.Documents.Reports.Report.PageMargins.GetPageMargins("Narrow")
                .TargetPaperSize = Infragistics.Documents.Reports.Report.PageSizes.GetPageSize("A4")
                .Export(whdgEntregasExportacion)
            End With
        End If
    End Sub

    ''' Revisado por: blp. Fecha: 07/03/2012
    ''' <summary>
    ''' Carga los textos de la página e inicializa los controles
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce cuando el control de servidor se carga en el objeto Page.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_RecepcionPedidos
        Master.Seleccionar("Catalogo", "Recepcion")

        RegistrarControles()

        If FSNServer.TipoAcceso.gbMostrarFechaContable Then pnlFechaContable.Visible = True
        'Casos en los que no queremos que se cargue el Load de la página
        If ScriptMgr.IsInAsyncPostBack AndAlso CheckAsyncPostBackElementToExit() Then
            Exit Sub
        End If

		If Request.Form("__CALLBACKID") Is Nothing OrElse (Request.Form("__CALLBACKID") IsNot Nothing AndAlso Request.Form("__CALLBACKID").IndexOf("fsnTvwCategorias") < 0) Then
			SeleccionChecksRecepcion(False)
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaImagenes", "<script>var rutaTheme = '" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "' </script>")
			WriteScripts()

			'Crear los combos de partidas presupuestarias y llenarlos de contenido
			If Me.Acceso.gbAccesoFSSM Then
				CargarCombosPartidas()
			End If

			If Not IsPostBack() Then
				imgObservaciones.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/coment.gif"
				imgAlbaranBloqueado.ImageUrl = ConfigurationManager.AppSettings("ruta") & "images/stopx40.png"

				CargarTextos()
				Dim Cookie As HttpCookie
				Cookie = Request.Cookies("RECEPCION_CRIT_ORD")
				If Cookie Is Nothing OrElse String.IsNullOrEmpty(Cookie.Value) Then
					CampoOrden = "FECHA"
				Else
					If ((Not MostrarProveedorERP) And Cookie.Value = "PROVEEDOR ERP") _
						OrElse
					((Not MostrarNumeroPedidoERP) And Cookie.Value = "REF. EN FACTURA") Then
						CampoOrden = "FECHA"
						Cookie.Value = CampoOrden
					Else
						CampoOrden = Cookie.Value
					End If
					Cookie.Expires = DateTime.Now.AddDays(30)
					Response.AppendCookie(Cookie)
				End If
				Cookie = Request.Cookies("RECEPCION_CRIT_DIREC")
				If Cookie Is Nothing OrElse String.IsNullOrEmpty(Cookie.Value) Then
					SentidoOrdenacion = "DESC"
				Else
					SentidoOrdenacion = Cookie.Value
					Cookie.Expires = DateTime.Now.AddDays(30)
					Response.AppendCookie(Cookie)
				End If
				Cookie = Request.Cookies("COLSGROUPBY")
				If Cookie Is Nothing OrElse String.IsNullOrEmpty(Cookie.Value) Then
					Session("COLSGROUPBY") = Nothing
				Else
					Session("COLSGROUPBY") = Cookie.Value
				End If

				Dim bSoloCombosyFechas As Boolean = False
				InicializarControles(bSoloCombosyFechas)

				ActualizarFiltros()
				RellenarLabelFiltros()
				CargarwhdgEntregas()

				If Me.Acceso.gbAccesoFSSM Then
					SMActivos.OnCancelarClientClick = "$find('" & mpeActivos.ClientID & "').hide(); return false"
					SMActivos.OnAceptarClientClick = "$find('" & mpeActivos.ClientID & "').hide();"
				End If
			Else
				Select Case Request("__EVENTARGUMENT")
					Case "Excel"
						Exportar(1)
					Case "Pdf"
						Exportar(2)
					Case "RecargarGrid"
						EliminarCachePedidos(0) ' Si queremos recargar todo el dataset le pasamos 0
						ActualizarDsetpedidos()
						CargarwhdgEntregas()
						pnlGrid.Update()
					Case "RecargarGridyPanelRecep"
						EliminarCachePedidos(0) ' Si queremos recargar todo el dataset le pasamos 0
						ActualizarDsetpedidos()
						CargarwhdgEntregas()
						pnlGrid.Update()
						Dim Albaran As String = hidAlb.Value
						Dim FechaAlbaran As Date = hidAlbFechaRecep.Value
						Dim Prove As String = hidAlbProve.Value
						CargarDetalleAlbaran(Albaran, FechaAlbaran, Prove)
					Case "GuardarConfiguracion"
						CargarwhdgEntregas()
						GuardarConfiguracion(New Object, New System.EventArgs)
					Case Else
						Select Case Request("__EVENTTARGET")
							Case btnDetalleAlbaran.ClientID
								Dim oParametros As Object
								Dim serializer As New Script.Serialization.JavaScriptSerializer
								oParametros = serializer.Deserialize(Of Dictionary(Of String, String))(Request("__EVENTARGUMENT"))
								Dim Albaran As String = oParametros("Albaran")
								Dim FechaAlbaran As Date = oParametros("FechaRecep")
								Dim Prove As String = oParametros("ProveCod")
								btnAceptarConfirm.CommandArgument = oParametros("RowIndex")
								CargarDetalleAlbaran(Albaran, FechaAlbaran, Prove)
							Case btnRegistrarRecepcion.ClientID
								Dim oParametros As Object
								Dim serializer As New Script.Serialization.JavaScriptSerializer
								oParametros = serializer.Deserialize(Of Dictionary(Of String, String))(Request("__EVENTARGUMENT"))
								RegistrarRecepcion(oParametros("LineasPedido"), CLng(oParametros("OrdenId")), oParametros("MostrarOpcionCargarTodas"))
							Case Else
								If Not (ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$btnMostrarPopUpAdjuntos") > 0 OrElse
									ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$linkAdjuntoPedido") > 0 _
									OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$btnMostrarDetalleArticulo") > 0 _
									OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$btnBuscar") > 0) Then

									CargarwhdgEntregas()
								End If
						End Select
				End Select
			End If

			If Not (ScriptMgr.IsInAsyncPostBack AndAlso ScriptMgr.AsyncPostBackSourceElementID = "whdgEntregas" AndAlso whdgEntregas.GridView.Behaviors.Filtering.Filtered) Then
				'El InicializarPaginador se lanza al filtrar los datos. Si lo lanzamos aquí tb deja de funcionar el recuento de resultados que se hace dentro de la función.
				'Por eso excluimos el caso concreto en que se filtra
				InicializarPaginador()
			End If
		End If
		CargarScriptMarginCabeceraPanelRecepcion()
    End Sub

    ''' <summary>
    ''' Procedimiento que se ejecuta cuando se checkea o descheckea en el panel de configuracion del grid de recepciones
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ClickCheckPanelConfiguracion(ByVal IdCheck As String)
        Dim objCheckConfiguracion As CheckBox = Page.FindControl(IdCheck)
        Dim objCheck As CheckBox = DirectCast(tblPnlConfig.FindControl(IdCheck), CheckBox)
    End Sub

    ''' Revisado por: blp. Fecha: 07/03/2012
    ''' <summary>
    ''' Carga los textos de la página en función del idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load. Máx. 0,1 seg.</remarks>
    Private Sub CargarTextos()
        LblProcesando.Text = Textos(265)
        lblTitulo.Text = Textos(91)
        lblCabecera.Text = Textos(92)
        litAnio.Text = Textos(3)
        litCesta.Text = Textos(4) & ":"
        txtCesta_TextBoxWatermarkExtender.WatermarkText = Textos(4)
        litPedido.Text = Textos(5) & ":"
        txtPedido_TextBoxWatermarkExtender.WatermarkText = Textos(5)
        litPedidoProve.Text = Textos(51) & ":"
        txtPedidoProve_TextBoxWatermarkExtender.WatermarkText = Textos(5)
        btnLimpiar.Text = Textos(170)
        lblPanelConfiguracion.Text = Textos(245)
        lblConfig.Text = Textos(246)

        If MostrarNumeroPedidoERP Then
            Dim oCParametros As FSNServer.CParametros = FSNServer.Get_Object(GetType(FSNServer.CParametros))
            litPedidoERP.Text = oCParametros.CargarLiteralParametros(TiposDeDatos.LiteralesParametros.PedidoERP, Usuario.Idioma) & ":"
            txtPedidoERP_TextBoxWatermarkExtender.WatermarkText = Textos(5)
        Else
            celdaLitPedidoERP.Visible = False
            celdaPedidoERP.Visible = False
            celdaSpacePedidoERP.Visible = False
        End If
        litTipoPedido.Text = Textos(117) & ":"
        txtArticulo_TextBoxWatermarkExtender.WatermarkText = Textos(93)
        litEmpresa.Text = Textos(84) & ":"
        If MostrarProveedorERP Then
            litProvedorERP.Text = Textos(85) & ":"
            txtProveedorERP_TextBoxWatermarkExtender.WatermarkText = Textos(85)
        Else
            celdaLitProveedorERP.Visible = False
            celdaProveedorERP.Visible = False
            celdaSpaceProveedorERP.Visible = False
        End If
        LnkBtnCategoria.Text = Textos(90)
        btnBuscar.Text = Textos(9)
        BtnAceptarMensaje.Text = Textos(113)
        lblConfirm.Text = Textos(123)
        btnAceptarConfirm.Text = Textos(113)
        btnAceptarConfirmServer.Text = Textos(113)
        btnCancelarConfirm.Text = Textos(124)
        btnCancelarRegistrarRecepcion.Text = Textos(124)
        chkRecepcionPedidos.Items(0).Text = Textos(146) 'Recepcionar mis pedidos
        chkRecepcionPedidos.Items(1).Text = Textos(145) 'Recepcionar pedidos de otros usuarios
        litAlbaran.Text = Textos(152) & ":" 'Nº Albarán
        litProveedor.Text = Textos(10) & ":" 'Proveedor
        txtProveedor_TWE.WatermarkText = Textos(199) & ", " & Textos(210) 'Código, Nombre
        litFecEmision.Text = Textos(87) & ":" 'Fecha de pedido
        litArticulo.Text = Textos(148) & ":" 'Artículo
        litFecRecepcion.Text = Textos(149) & ":" 'Fecha de recepción
        litFecEntregaSolicit.Text = Textos(150) & ":" 'Fec. entrega solicitada
        litFecEntregaIndic.Text = Textos(151) & ":" 'Fec. entrega indicada
        litCentroCoste.Text = Textos(116) & ":" ' Centro de coste
        litGestor.Text = Textos(153) & ":" 'Gestor
        litReceptor.Text = Textos(258) & ":"
        litConfigurar.InnerHtml = Textos(196) 'Configurar
        litExcel.InnerHtml = Textos(194) 'Excel
        litPDF.InnerHtml = Textos(195) 'PDF
        btnAceptarConfiguracion.Text = Textos(113) 'Aceptar
        btnCancelarConfiguracion.Text = Textos(124) 'Cancelar

        lblDetalleAlbaran.Text = Textos(208)
        lblAlbaranBloqueado.Text = Textos(256)

        lblDetalleAlbaranFecRecepcion.Text = String.Format("{0}:", Textos(97))

        lblDetalleAlbaranDatosReceptor.Text = Textos(209)

        lblDetalleAlbaranCodigoReceptor.Text = String.Format("{0}:", Textos(199))
        lblDetalleAlbaranNombreReceptor.Text = String.Format("{0}:", Textos(210))
        lblDetalleAlbaranCargoReceptor.Text = String.Format("{0}:", Textos(211))
        lblDetalleAlbaranDepartamentoReceptor.Text = String.Format("{0}:", Textos(212))
        lblDetalleAlbaranEmailReceptor.Text = String.Format("{0}:", Textos(213))
        lblDetalleAlbaranTelefonoReceptor.Text = String.Format("{0}:", Textos(214))
        lblDetalleAlbaranFaxReceptor.Text = String.Format("{0}:", Textos(215))
        lblDetalleAlbaranOrganizacionReceptor.Text = String.Format("{0}:", Textos(216))

        lblDetalleAlbaranPedido.Text = Textos(32)
        lblDetalleAlbaranNumLinea.Text = Textos(232)
        lblDetalleAlbaranDetalleLineas.Text = Textos(217)
        lblDetalleAlbaranDetalleLineasSalidaAlmacen.Text = Textos(278)

        lblDetalleAlbaranDetalleLineasArticulo.Text = Textos(37)
        lblDetalleAlbaranDetalleLineasCantidadPedida.Text = Textos(218)
        lblDetalleAlbaranDetalleLineasPrecioUnitario.Text = Textos(41)
        lblDetalleAlbaranDetalleLineasCantidadRecibida.Text = Textos(219)
        lblDetalleAlbaranDetalleLineasImporte.Text = Textos(48)
        lblDetalleAlbaranDetalleLineasImportePedido.Text = Textos(266)
        lblDetalleAlbaranDetalleLineasImporteRecibido.Text = Textos(269)

        lblProblemasRecepcion.Text = Textos(220)
        lnkLineasPendientesRecepcionar.Text = Textos(233)
        litRegistroRecepcionFechaRecepcion.Text = Textos(97) & ":"
        litRegistroRecepcionAlbaran.Text = Textos(98) & ":"

        chkRecepIncorrecta.Text = Textos(234)
        btnBloquearFacturacion.Text = Textos(228)
        btnDesbloquearFacturacion.Text = Textos(229)
        btnAnularAlbaran.Text = Textos(242) 'Anular Albarán
		litRegistroRecepcionFecContable.Text = Textos(279) & ":"

		If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosBotonConfirmar") Then
            Dim sVariableJavascriptTextos As String = "var TextosBotonConfirmar = new Array();"
            sVariableJavascriptTextos &= "TextosBotonConfirmar[0]='" & JSText(Textos(221)) & "';" 'Se eliminará la línea de la recepción. ¿Desea continuar?
            sVariableJavascriptTextos &= "TextosBotonConfirmar[1]='" & JSText(Textos(222)) & "';" 'Se eliminaran todas las recepciones incluidas en el albarán XXXXX. ¿Desea continuar?
            sVariableJavascriptTextos &= "TextosBotonConfirmar[2]='" & JSText(Textos(249)) & "';" 'Albarán anulado correctamente
            sVariableJavascriptTextos &= "TextosBotonConfirmar[3]='<b>" & Textos(98) & " XXXXX</b><p>" & Textos(230) & ":</p>';" 'Albarán XXXXX Puede introducir comentarios al bloqueo del albarán para facturación:
            sVariableJavascriptTextos &= "TextosBotonConfirmar[4]='<b>" & JSText(Textos(250)) & "';" 'Anulado correctamente
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosBotonConfirmar", sVariableJavascriptTextos, True)
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosError") Then
            Dim sVariableJavascriptTextos As String = "var TextosError = new Array();"
            sVariableJavascriptTextos &= "TextosError[0]='" & JSText(Textos(223)) & "';" 'No existe la orden de entrega correspondiente.
            sVariableJavascriptTextos &= "TextosError[1]='" & JSText(Textos(224)) & "';" 'Se han modificado los datos en otra sesión.
            sVariableJavascriptTextos &= "TextosError[2]='" & JSText(Textos(225)) & "';" 'Existen facturas vinculadas a la recepción. Anule las facturas antes de anular la recepción.
            sVariableJavascriptTextos &= "TextosError[3]='" & JSText(Textos(226)) & "';" 'No se han encontrado recepciones asociadas a la orden de entrega.
            sVariableJavascriptTextos &= "TextosError[4]='" & JSText(Textos(231)) & "';" 'Se ha producido un error. Por favor, inténtelo de nuevo más tarde y si el problema persiste, contacte con el soporte técnico.
            sVariableJavascriptTextos &= "TextosError[5]='" & JSText(Textos(251)) & "';" 'No se han podido anular todas las recepciones vinculadas al albarán. Los motivos son:
            sVariableJavascriptTextos &= "TextosError[6]='" & JSText(Textos(252)) & "';" 'No se ha anulado ninguna de las recepciones vinculadas al albarán. Los motivos son:
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosError", sVariableJavascriptTextos, True)
        End If

        litFecRecepcion.Text = Textos(97) & ":"
        reqvalFecRecepcion.ErrorMessage = Textos(105)
        litAlbaran.Text = Textos(98) & ":"
        reqvalAlbaran.ErrorMessage = Textos(106)
        litRegistrarRecepcion.InnerText = Textos(102)
        lblRegistroRecepcion.Text = Textos(102)
        chkProblemaRecep.Text = Textos(100)
        litObsRecep.Text = Textos(76) & ":"
        chkNotificar.Text = Textos(101)
        btnRegistrar.Text = Textos(102)
        'Si hay permisos para cerrar, se muestra el botón cerrar
        If FSNUser.PermisoCerrarOrden Then
            btnRegistrarCerrar.Text = Textos(103)
            btnCerrarOrdenes.Text = Textos(144) 'Cerrar el pedido
        Else
            btnRegistrarCerrar.Visible = False
            btnCerrarOrdenes.Visible = False
        End If
        whdgEntregas.GroupingSettings.EmptyGroupAreaText = Textos(253)
        litGuardarConfiguracion.InnerText = Textos(254)

        lblCentros.Text = Textos(259) & ": " & Textos(116) 'Selección de: Centro de coste
        lblFiltroCentros.Text = Textos(116) & ": " 'Centro de coste:

        chkSoloLineasPendientes.Text = Textos(260)

        lblTituloListaAdjuntos.Text = Textos(271)   'MOD(29) - ID(272) - 'Archivos adjuntos','Attached files','Angehängte Dateien','Archivos adjuntos'
        lblNuevoAdjunto.Text = Textos(272)          'MOD(29) - ID(273) - 'Adjuntar archivos','Attach files','Dateien anhängen','Adjuntar archivos'
        LblArchAdjunPed.Text = Textos(273)          'MOD(29) - ID(274) - 'Añadir archivo adjunto','Add attached file','Angehängte Datei hinzufügen','Añadir archivo adjunto'
        BtnAcepAdjunPed.Text = Textos(113)
        BtnCancelAdjunPed.Text = Textos(124)

    End Sub

    ''' <summary>
    ''' Tras mover columna del grid se "sincroniza" con el botÃ³n "Guardar Config"
    ''' </summary>
    ''' <param name="sender">control</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo mÃ¡ximo:0 </remarks>
    Private Sub whdgEntregas_ColumnMoved(sender As Object, e As Infragistics.Web.UI.GridControls.ColumnMovingEventArgs) Handles whdgEntregas.ColumnMoved
        ibGuardarConfiguracion.Style("display") = "inline"
        pnlGrid.Update()
    End Sub

    ''' Revisado por: blp. Fecha:22/08/2012
    ''' <summary>
    ''' Tras la carga del grid se "sincroniza" con el Custom Pager
    ''' </summary>
    ''' <param name="sender">control</param>
    ''' <param name="e">evento de sistema</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo máximo:0 </remarks>
    Private Sub whdgEntregas_GroupedColumnsChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GroupedColumnsChangedEventArgs) Handles whdgEntregas.GroupedColumnsChanged
        Dim groupedColumnList As String = String.Empty
        For Each column As Infragistics.Web.UI.GridControls.GroupedColumn In e.GroupedColumns
            groupedColumnList = IIf(groupedColumnList Is String.Empty, String.Empty, "#")
            groupedColumnList &= column.ColumnKey & IIf(column.SortDirection = Infragistics.Web.UI.GridControls.GroupingSortDirection.Ascending, " ASC", " DESC")
        Next
        Session("COLSGROUPBY") = groupedColumnList

        Dim cookie As HttpCookie
        cookie = Request.Cookies("COLSGROUPBY")
        If cookie Is Nothing Then
            cookie = New HttpCookie("COLSGROUPBY")
        End If
        cookie.Value = groupedColumnList
        cookie.Expires = Date.MaxValue
        Response.AppendCookie(cookie)

        InicializarPaginador()
        pnlGrid.Update()
    End Sub

    Private Sub WriteScripts()
        'Pongo el display "inline" a los webdatachooser porque el Desde y el hasta estan en la misma celda y la tabla que crea
        'al general el control es un control de bloque y pone un webdatechooser encima de otro en vez de alineados el Desde y el hasta
        txtProveedor.Attributes.Add("CODIGO", "")
        imgProveedor.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("RutaFS") & "_common/BuscadorProveedores.aspx?Notificacion=1&desde=RecepcionesEP&IDCONTROL=" & txtProveedor.ClientID & "&HiddenFieldID=" & hidProveedor.ClientID & "&TextBoxWatermarkID=" & txtProveedor_TWE.ClientID & "', '_blank', 'width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes');return false;"

        'Funcion que sirve para eliminar el proveedor seleccionado
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarProveedor") Then
            Dim sScript As String = "function eliminarProveedor(event) {" & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    "campo = document.getElementById('" & txtProveedor.ClientID & "')" & vbCrLf & _
                    "if (campo) { " & vbCrLf & _
                        " campoHidden = document.getElementById('" & hidProveedor.ClientID & "')" & vbCrLf & _
                        " if (campoHidden) { campoHidden.value = ''; } " & vbCrLf & _
                    "} " & vbCrLf & _
                " } " & vbCrLf & _
            " } " & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarProveedor", sScript, True)
        End If

        txtProveedor.Attributes.Add("onkeydown", "javascript:return eliminarProveedor(event)")


        'Funcion que sirve para eliminar el articulo seleccionado
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarArticulo") Then
            Dim sScript As String = "function eliminarArticulo(event) {" & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    " campo = document.getElementById('" & txtArticulo.ClientID & "')" & vbCrLf & _
                    " if (campo) { " & vbCrLf & _
                        " campoHidden = document.getElementById('" & hidArticulo.ClientID & "')" & vbCrLf & _
                        " if (campoHidden) { campoHidden.value = ''; } " & vbCrLf & _
                    "} " & vbCrLf & _
                " } " & vbCrLf & _
            " }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarArticulo", sScript, True)
        End If

        txtArticulo.Attributes.Add("onkeydown", "javascript:return eliminarArticulo(event)")



    End Sub

    ''' Revisado por: blp. Fecha: 16/01/2012
    ''' <summary>
    ''' Inicializa los controles en la primera carga de la página
    ''' </summary>
    ''' <param name="InicializarSoloCombosyFechas">Parámetro Booleano que nos indica si, dentro de los controles que inicializa esta función (casi todos los de la página) 
    '''                          queremos inicializar sólo los combos y fechas. 
    '''                          Este parámetro se ha añadido porque cuando se modifica la combinación de checkboxes "Recepcionar mis pedidos" y "Recepcionar pedidos de otros usuarios" (control chkRecepcionPedidos())
    '''                          hay que reiniciar estos combos para que incluyan valores que anteriormente no incluían o para que dejen de mostrarlos, según el caso.
    '''                          Por ello, en la carga de la página (Page_Load) el parámetro se pasa a False y al efectuar una búsqueda se para a True.
    ''' </param>
    ''' <remarks>Llamada desde Page_Load. Máx. 0,2 seg.</remarks>
    Private Sub InicializarControles(ByVal InicializarSoloCombosyFechas As Boolean)
        If Not InicializarSoloCombosyFechas Then
            If Not Me.Acceso.gbVerNumeroProveedor Then
                litPedidoProve.Visible = False
                txtPedidoProve.Visible = False
            End If

            inicializarCheckboxLists()

            txtProveedor_ACE.ContextKey = Me.Usuario.CodPersona
            txtArticulo_AutoCompleteExtender.ContextKey = Me.Usuario.CodPersona

            CargarArbolCategorias()
        End If

        Dim dsDropDownInfo As New DataSet
        dsDropDownInfo = getDropDownsInfo(Me.Usuario.CodPersona, Me.Usuario.Idioma)

        'Antes de comprobar los argumentos que llegan de seguimiento para que se seleccione el año del pedido.
        CargarAnyos()

        If Not Me.IsPostBack Then
            'Session("OrdenIdYProveedor") es una variable que se manda desde Seguimiento al pulsar el botón "Recepcionar" de una orden
            'envía la información de la orden para que se filtre por ella en Recepción
            If Session("OrdenIdYProveedor") IsNot Nothing Then
                Dim sArgumentos() As String = Split(Session("OrdenIdYProveedor"), "###")
                'sArgumentos(0)-->ID
                'sArgumentos(1)-->CÓD. PROVEEDOR
                'sArgumentos(2)-->ANYO
                'sArgumentos(3)-->NUMPEDIDO
                'sArgumentos(4)-->NUMORDEN
                'sArgumentos(5)-->VER PEDIDOS DEL USUARIO
                'sArgumentos(6)-->VER PEDIDOS DE OTROS USUARIOS


                Dim oProveedores As CProveedores = FSNServer.Get_Object(GetType(FSNServer.CProveedores))
                Dim dtCodProve As DataTable = oProveedores.CrearTablaProveCods()
                Dim drCodProve As DataRow = dtCodProve.NewRow
                drCodProve("COD") = sArgumentos(1)
                dtCodProve.Rows.InsertAt(drCodProve, 0)
                Dim dtProve As DataTable = oProveedores.DevolverDatosProveedores(Usuario.Idioma, dtCodProve)
                Dim sProveDen As String = String.Empty
                If dtProve IsNot Nothing AndAlso dtProve.Rows.Count > 0 Then
                    sProveDen = dtProve.Rows(0).Item("DEN")
                End If
                If Not String.IsNullOrEmpty(sProveDen) Then
                    txtProveedor.Text = sArgumentos(1) & " - " & sProveDen
                    hidProveedor.Value = sArgumentos(1)
                End If

                If sArgumentos.Length > 2 Then
                    If ddlAnio.Items.FindByValue(sArgumentos(2)) IsNot Nothing Then
                        ddlAnio.SelectedValue() = sArgumentos(2)
                    End If
                    txtCesta.Text = sArgumentos(3)
                    txtPedido.Text = sArgumentos(4)
                End If

                'Tendremos en cuenta la selección de los checks de pedidos del usuario y pedidos de otros usuarios
                'solo cuando el usuario pueda recepcionar los de otros usuario, si no, los únicos que se pueden recepcionar son los suyos
                If FSNUser.PermisoRecepcionarPedidosCCImputables Then
                    chkRecepcionPedidos.Items(0).Selected = CBool(sArgumentos(5))
                    chkRecepcionPedidos.Items(1).Selected = CBool(sArgumentos(6))
                Else
                    chkRecepcionPedidos.Items(0).Selected = True
                    chkRecepcionPedidos.Items(1).Selected = False
                End If

            Else
                'En la carga por defecto de la página, se filtra por los pedidos del último mes
                'Ponemos el límite de mes sólo si no venimos de seguimiento con una orden ya seleccionada, dado que igual esa orden es anterior
                dteDesdeEmision.Value = DateTime.Now.AddMonths(-1).Date
                dteHastaEmision.Value = DateTime.Now.Date
            End If
        End If

        Dim lista As List(Of ListItem)
        ' DropDown Tipo de pedido
        lista = ListaTiposPedidos().ToList
        If lista.Count > 0 Then _
            lista.Insert(0, New ListItem(String.Empty, String.Empty))
        ddlTipoPedido.DataSource = lista
        ddlTipoPedido.DataBind()
        ' DropDown Empresa
        lista = Empresas(dsDropDownInfo).ToList
        If lista.Count > 0 Then _
            lista.Insert(0, New ListItem(String.Empty, String.Empty))
        ddlEmpresa.DataSource = lista
        ddlEmpresa.DataBind()
        ' DropDown Receptor
        lista = Receptores(dsDropDownInfo).ToList
        If lista.Count > 0 Then _
         lista.Insert(0, New ListItem(String.Empty, String.Empty))
        ddlReceptor.DataSource = lista
        ddlReceptor.DataBind()
        'DropDown Gestor
        'Gestor: Visible sólo si hay SM
        If Not Me.Acceso.gbAccesoFSSM Then
            litGestor.Visible = False
            ddlGestor.Visible = False
        Else
            trCentrosPartidas.Visible = True
            Dim dtGestores As DataTable = ListaGestores(dsDropDownInfo)
            If dtGestores IsNot Nothing Then
                If dtGestores.Rows.Count > 0 AndAlso dtGestores.Rows(0).Item("GESTORDEN") <> String.Empty Then
                    Dim oRow As DataRow = dtGestores.NewRow
                    oRow("GESTORCOD") = String.Empty
                    oRow("GESTORDEN") = String.Empty
                    dtGestores.Rows.InsertAt(oRow, 0)
                End If
                ddlGestor.DataSource = dtGestores
                ddlGestor.DataTextField = dtGestores.Columns(1).ColumnName
                ddlGestor.DataValueField = dtGestores.Columns(0).ColumnName
                ddlGestor.DataBind()
            End If
        End If

        tbCtroCoste.Attributes.Add("onKeyDown", "abrirPanelCentros('" & mpeCentros.ClientID & "','" & FiltroCentros.ClientID & "', event, '" & tbCtroCoste.ClientID & "', '" & tbCtroCoste_Hidden.ClientID & "');")
    End Sub
#Region "Arbol Categorias"

    Private ReadOnly Property TodasCategorias() As DataSet
        Get
            Dim ds As DataSet
            If HttpContext.Current.Cache("TodasCategoriasRecep_" & FSNUser.Cod) Is Nothing Then
                Dim oCCategorias As FSNServer.CCategorias = FSNServer.Get_Object(GetType(FSNServer.CCategorias))
                ds = oCCategorias.CargarArbolCategorias("", 1)
                Me.InsertarEnCache("TodasCategoriasRecep_" & FSNUser.Cod, ds)
            Else
                ds = HttpContext.Current.Cache("TodasCategoriasRecep_" & FSNUser.Cod)
            End If
            Dim key() As DataColumn = {ds.Tables(0).Columns("ID")}
            ds.Tables(0).PrimaryKey = key
            Return ds
        End Get
    End Property

    ''' Revisado por: blp. Fecha: 16/01/2012
    ''' <summary>
    ''' Procedimiento en el que cargaremos los valores para el arbol de categorías
    ''' En el resto del datos presentes en los filtros se muestra sólo la info que realmente 
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: El evento Load de la página
    ''' Tiempo máximo: 0 sec.</remarks>
    Private Sub CargarArbolCategorias()
        If TodasCategorias.Tables(0) IsNot Nothing AndAlso TodasCategorias.Tables(0).Rows.Count > 0 Then
            fsnTvwCategorias.Nodes(0).Text = Textos(90) 'Categorias
            fsnTvwCategorias.NodeStyle.Height = "20"
        Else
            fsnTvwCategorias.Nodes.Clear()
        End If
    End Sub

    ''' <summary>
    ''' Evento que se lanzará al seleccionar un nodo del árbol de categorías, mostrando la categoría seleccionada en el panel de bÃƒÂºsqueda
    ''' </summary>
    ''' <param name="sender">el objecto que llama al evento (el arbol)</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el propio control
    ''' Tiempo máximo: 0 seg.</remarks>
    Private Sub fsntvwCategorias_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fsnTvwCategorias.SelectedNodeChanged
        If fsnTvwCategorias.SelectedNode.Value <> "_0_" Then
            If fsnTvwCategorias.SelectedNode Is Nothing Then
                litCategoria.Text = ""
                litCategoria.Visible = False
                lnkQuitarCategoria.CommandArgument = ""
                lnkQuitarCategoria.Visible = False
            Else
                litCategoria.Text = fsnTvwCategorias.SelectedNode.Text
                litCategoria.Visible = True
                lnkQuitarCategoria.CommandArgument = fsnTvwCategorias.SelectedValue
                lnkQuitarCategoria.Visible = True
            End If
            mpePanelArbol.Hide()
            updpnlCategorias.Update()
        End If
    End Sub

    Public Sub fsnTvwCategorias_TreeNodePopulate(ByVal sender As Object, ByVal e As TreeNodeEventArgs) Handles fsnTvwCategorias.TreeNodePopulate
        Dim query
        Select Case e.Node.Value
            Case "_0_"
                query = From datos In TodasCategorias.Tables(0) _
                            Where datos("PADRE") Is System.DBNull.Value _
                            Select datos

            Case Else
                query = From datos In TodasCategorias.Tables(0) _
                            Where datos("PADRE") IsNot System.DBNull.Value AndAlso datos("PADRE") = e.Node.Value _
                            Select datos

        End Select

        If CType(query, System.Data.EnumerableRowCollection(Of System.Data.DataRow)).Any Then
            For Each oDatosNodo As System.Data.DataRow In CType(query, System.Data.EnumerableRowCollection(Of System.Data.DataRow))
                Dim nodo As New TreeNode
                nodo.Text = oDatosNodo.Item("DEN")
                nodo.Value = oDatosNodo.Item("ID")
                nodo.Expanded = False
                nodo.PopulateOnDemand = True
                nodo.SelectAction = TreeNodeSelectAction.Select
                e.Node.ChildNodes.Add(nodo)
            Next
        End If
    End Sub

    ''' <summary>
    ''' Evento que se lanzará al pulsar la (X) de una categoría en los filtros de categorías
    ''' y que quita la categoría de ahí
    ''' </summary>
    ''' <param name="sender">el objecto que llama al evento</param>
    ''' <param name="e">argumentos del evento Click</param>
    ''' <remarks>Llamada desde el propio control
    ''' Tiempo máximo: 0 seg.</remarks>
    Private Sub lnkQuitarCategoria_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkQuitarCategoria.Click
        fsnTvwCategorias.SelectedNode.Selected = False
        litCategoria.Text = ""
        litCategoria.Visible = False
        lnkQuitarCategoria.CommandArgument = ""
        lnkQuitarCategoria.Visible = False
        pnlCategorias.Update()
    End Sub

#End Region

#Region "Consultas"

    ''' <summary>
    ''' Dataset que contiene el listado de pedidos a mostrar en pantalla
    ''' </summary>
    Private ReadOnly Property DsetPedidos() As DataSet
        Get
            Dim dsObj As Object = HttpContext.Current.Cache("DsetPedidosRecepcion_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
            If dsObj Is Nothing Then
                Return ActualizarDsetpedidos()
            Else
                Dim dsPedidos As New DataSet

                dsPedidos = CType(dsObj, DataSet).Copy

                Return dsPedidos
            End If
        End Get
    End Property

    ''' <summary>
    ''' Dataset que contiene un registro de recepcion para mostrar en el panel de configuracion 
    ''' </summary>
    Private ReadOnly Property DsetConfiguracion() As DataSet
        Get
            If HttpContext.Current.Cache("DsetPedidosRecepcion_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                Return Nothing
            Else
                Dim dsPedidos As New DataSet

                dsPedidos = CType(HttpContext.Current.Cache("DsetPedidosRecepcion_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()), DataSet).Copy
                Dim dsConfiguracion As New DataSet
                Dim dtConfiguracion As DataTable = dsPedidos.Tables("ORDENES").Clone
                If dsPedidos.Tables("ORDENES").Rows.Count > 0 Then
                    dtConfiguracion.ImportRow(dsPedidos.Tables("ORDENES").Rows(0))
                End If
                dsConfiguracion.Tables.Add(dtConfiguracion)
                Return dsConfiguracion
            End If
        End Get
    End Property

    ''' Revisado por: blp. Fecha: 16/01/2012
    ''' <summary>
    ''' Actualiza el dataset de pedidos.
    ''' </summary>
    ''' <remarks>Se produce cuando el control de servidor se carga en el objeto Page. Tiempo: depende del nº de datos</remarks>
    Private Function ActualizarDsetpedidos() As DataSet
        'Antes de actualizar los pedidos, actualizamos los filtros para asegurarnos de que los filtros de fecha y de estados (que se usan al generar el dataset) son los seleccionados por el usuario
        ActualizarFiltros()
        Dim dsOrdenes As New DataSet
        If HttpContext.Current.Cache("DsetPedidosRecepcion_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
            dsOrdenes = GenerarDsetPedidos(Me.Usuario.CodPersona, Me.Usuario.Idioma, FiltrosAnyadidos)
            If Not _PaginadoEntregas Then
                PaginacionEntregas(dsOrdenes.Tables("ORDENES"))
            End If
            InsertarEnCache("DsetPedidosRecepcion_" & Me.Usuario.CodPersona & "_" & Usuario.Idioma.ToString(), dsOrdenes.Copy)
        Else
            dsOrdenes = CType(HttpContext.Current.Cache("DsetPedidosRecepcion_" & Usuario.CodPersona & "_" & Usuario.Idioma.ToString()), DataSet).Copy
        End If
        Return dsOrdenes
    End Function

    ''' Revisado por: blp. Fecha: 15/10/2012
    ''' <summary>
    ''' Al recuperar las recepciones cogemos (en el mismo stored) un registro más de los que están configurados en el web .config para mostrar en 
    ''' pantalla (key "NumRegistrosVisorRecep")
    ''' Lo hacemos a fin de saber si hay más registros aparte de los mostrados y, si los hay, mostrar un botón que permita cargarlos.
    ''' Guardamos los ids de la ultima linea, para mostrar los siguientes resultados a partir de ese id
    ''' </summary>
    ''' <param name="dtEntregas">Datatable con las recepciones recuperadas de base de datos</param>
    ''' <remarks>Llamada desde Recepcion.aspx.vb. Máx. 1 seg.</remarks>
    Private Sub PaginacionEntregas(ByVal dtEntregas As DataTable)
        _PaginadoEntregas = True
        DirectCast(whdgEntregas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("divContenedor"), HtmlGenericControl).Style("width") = "100%"
    End Sub
    ''' <summary>
    ''' Procedimiento que en funcion de la variable de session muestra u oculta el Div de Mostrar mas resultados
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ActualizarMostrarMasResultados()
        If Session("MostrarMasResultados") = True Then
            DirectCast(whdgEntregas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("divContenedor"), HtmlGenericControl).Style("width") = "40%"
            DirectCast(whdgEntregas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("linkMostrarMasResultados"), LinkButton).Visible = True
        Else
            DirectCast(whdgEntregas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("linkMostrarMasResultados"), LinkButton).Visible = False
            DirectCast(whdgEntregas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("divContenedor"), HtmlGenericControl).Style("width") = "100%"
        End If
    End Sub

    ''' Revisado por: blp. Fecha: 15/10/2012
    ''' <summary>
    ''' Funcion que devuelve un datatable con los estados de ordenes que estan seleccionados en pantalla
    ''' </summary>
    ''' <returns>datatable con los estados de ordenes seleccionados en pantalla</returns>
    ''' <remarks>Llamada desde Recepcion.aspx.vb. Maximo 0,1 seg.</remarks>
    Private Function CrearTablaEstadosOrdenes() As DataTable
        Dim dtEstados As New DataTable("ESTADOS")
        dtEstados.Columns.Add("EST")
        Dim drEstado As DataRow
        For Each lst As ListItem In chklstEstado.Items
            If lst.Selected Then
                If (lst.Value = EstadoSinRecepcion) Then
                    'Si se cambia los estados que componen la propiedad EstadoSinRecepcion, hay que cambiar esto:
                    drEstado = dtEstados.NewRow
                    drEstado("EST") = CInt(TipoEstadoOrdenEntrega.EmitidoAlProveedor)
                    dtEstados.Rows.Add(drEstado)
                    drEstado = dtEstados.NewRow
                    drEstado("EST") = CInt(TipoEstadoOrdenEntrega.AceptadoPorProveedor)
                    dtEstados.Rows.Add(drEstado)
                    drEstado = dtEstados.NewRow
                    drEstado("EST") = CInt(TipoEstadoOrdenEntrega.EnCamino)
                    dtEstados.Rows.Add(drEstado)
                Else
                    drEstado = dtEstados.NewRow
                    drEstado("EST") = lst.Value
                    dtEstados.Rows.Add(drEstado)
                End If
            End If
        Next
        Return dtEstados
    End Function

    ''' <summary>
    ''' Devuelve TRUE si todos los estados del control chklstEstado están seleccionados
    ''' </summary>
    Private ReadOnly Property TodosEstadosSeleccionados() As Boolean
        Get
            For Each lst As ListItem In chklstEstado.Items
                If Not lst.Selected Then
                    Return False
                End If
            Next
            Return True
        End Get
    End Property

    ''' <summary>
    ''' Funcion que devuelve un datatable con los tipos de origen que estan seleccionados en pantalla
    ''' </summary>
    ''' <returns>datatable con los tipos de origen seleccionados en pantalla</returns>
    ''' <remarks></remarks>
    Private Function CrearTablaTipoOrigen() As DataTable
        Dim dtTipos As New DataTable("TIPOS_ORIGEN")
        dtTipos.Columns.Add("TIPO") 'en este campo vamos a guardar el tipo de origen del pedido tal y como se guardan en bdd (gs=0, ep=1, erp=2)
        dtTipos.Columns.Add("LIBRE") 'en este campo, que va a ser relevante sólo si el tipo es EP, definimos si el pedido tiene puede contener artículos libres o no
        Dim drTipo As DataRow
        For Each lst As ListItem In chklstTipo.Items
            If lst.Selected Then
                drTipo = dtTipos.NewRow
                If lst.Value = CPedido.TipoOrigenPedido.CatalogadoLibres Then
                    drTipo("TIPO") = 1 'El valor en bdd del tipo catalogado libre y negociado es el mismo: 1
                    drTipo("LIBRE") = True
                Else
                    drTipo("TIPO") = lst.Value
                    drTipo("LIBRE") = False
                End If
                'Cuando no hay pedidos libres, los pedidos libres no se filtran (porque no existen, en teoría).
                'En la práctica, los vamos a filtrar del mismo modo que los negociados para optimizar la consulta sql del procedimiento almacenado que se ejecuta
                If Not Me.Acceso.gbPedidoLibre AndAlso lst.Value = CPedido.TipoOrigenPedido.CatalogadoLibres Then
                    If chklstTipo.Items.FindByValue(CPedido.TipoOrigenPedido.CatalogadoNegociado).Selected Then
                        dtTipos.Rows.Add(drTipo)
                    End If
                Else
                    dtTipos.Rows.Add(drTipo)
                End If
            End If
        Next
        Return dtTipos
    End Function

    ''' <summary>
    ''' Función que crea la estructura de la tabla de partidas que usaremos en los filtros para obtener las entregas
    ''' </summary>
    ''' <returns>DataTable vacío</returns>
    ''' <remarks>Llamada desde entregasPedidos.aspx.vb->ObtenerDatasetEntregas y desde Entregas.vb->Loaddata. Máx. 0,1 seg.</remarks>
    Private Function CrearTablaPartidas() As DataTable
        Dim dtPartidas As DataTable = New DataTable("PARTIDASPRES")
        dtPartidas.Columns.Add("PRES0", GetType(System.String))
        dtPartidas.Columns.Add("PRES1", GetType(System.String))
        dtPartidas.Columns.Add("PRES2", GetType(System.String))
        dtPartidas.Columns.Add("PRES3", GetType(System.String))
        dtPartidas.Columns.Add("PRES4", GetType(System.String))
        Return dtPartidas
    End Function

    ''' <summary>
    ''' Función que crea la estructura de la tabla de categorias que usaremos en los filtros para obtener las entregas
    ''' </summary>
    ''' <returns>DataTable vacío</returns>
    ''' <remarks>Llamada desde entregasPedidos.aspx.vb->ObtenerDatasetEntregas y desde Entregas.vb->Loaddata. Máx. 0,1 seg.</remarks>
    Private Function CrearTablaCategorias() As DataTable
        Dim dtPartidas As DataTable = New DataTable("CATEGORIAS")
        dtPartidas.Columns.Add("CAT1", GetType(System.String))
        dtPartidas.Columns.Add("CAT2", GetType(System.String))
        dtPartidas.Columns.Add("CAT3", GetType(System.String))
        dtPartidas.Columns.Add("CAT4", GetType(System.String))
        dtPartidas.Columns.Add("CAT5", GetType(System.String))
        Return dtPartidas
    End Function

    ''' Revisado por: blp. Fecha: 16/01/2012
    ''' <summary>
    ''' Generamos el conjunto de datos de la página.
    ''' He añadido el FiltrosAnyadidos a los parámetros para cambiar la firma y que no de errores aunque no era necesario
    ''' dado que FiltrosAnyadidos es una propiedad
    ''' </summary>
    ''' <param name="CodPersona">Código de la persona</param>
    ''' <param name="Idioma">idioma en el que devolver el dataset</param>
    ''' <param name="FiltrosAnyadidos">Filtros de la búsqueda</param>
    ''' <param name="bCargarPedidosImpCC">Cargar las órdenes del usuario o los de su centro de coste
    ''' True-->Centros coste
    ''' False-->Usuario
    ''' </param>
    ''' <returns>dataset con los pedidos</returns>
    ''' <remarks>Llamada desde GenerarDsetPedidos. Máx. depende del número de datos</remarks>
    Private Function GenerarDsetPedidos(ByVal CodPersona As String, ByVal Idioma As FSNLibrary.Idioma, ByVal FiltrosAnyadidos As List(Of Filtro)) As DataSet
        Dim dFecRecepDesde As Date
        Dim dFecRecepHasta As Date
        Dim dFecEmisionDesde As Date
        Dim dFecEmisionHasta As Date
        Dim dFecEntregaSolicitadaDesde As Date
        Dim dFecEntregaSolicitadaHasta As Date
        Dim sNumAlbaran As String = String.Empty
        Dim sCodProve As String = String.Empty
        Dim iAnio As Integer
        Dim iNumCesta As Integer
        Dim iNumPedido As Integer
        Dim sNumPedERP As String = String.Empty
        Dim sGestor As String = String.Empty
        Dim sReceptor As String = String.Empty
        Dim sArticulo As String = String.Empty
        Dim sCodArticulo As String = String.Empty
        Dim iEmpresa As Integer
        Dim dFecEntregaIndicadaDesde As Date
        Dim dFecEntregaIndicadaHasta As Date
        Dim SCentro As String = String.Empty
        Dim sNumPedProve As String = String.Empty
        Dim sProveERP As String = String.Empty
        Dim iTipoPedido As Integer
        Dim sCategoria As String = String.Empty

        Dim oOrdenes As COrdenes = FSNServer.Get_Object(GetType(FSNServer.COrdenes))
        Dim bObtenerSoloDropDownInfoFALSE = False

        Dim dtEstados As DataTable = CrearTablaEstadosOrdenes()
        Dim dtTiposOrigen As DataTable = CrearTablaTipoOrigen()
        'Partidas presupuestarias
        Dim dtPartidas As DataTable = CrearTablaPartidas()
        Dim bSoloPendientes As Boolean

        dFecRecepDesde = If(dteDesdeRecepcion.Value Is DBNull.Value, Nothing, dteDesdeRecepcion.Value)
        dFecRecepHasta = If(dteHastaRecepcion.Value Is DBNull.Value, Nothing, dteHastaRecepcion.Value)

        sNumAlbaran = txtAlbaran.Text

        If txtProveedor.Text <> String.Empty AndAlso txtProveedor.Text.IndexOf("-") <> -1 Then
            sCodProve = hidProveedor.Value
        End If
        If hidArticulo.Value <> String.Empty Then
            sCodArticulo = hidArticulo.Value
        End If
        If txtArticulo.Text <> String.Empty Then
            sArticulo = txtArticulo.Text
        End If
        If (ddlAnio.SelectedValue IsNot Nothing AndAlso ddlAnio.SelectedValue <> String.Empty) Then
            iAnio = ddlAnio.SelectedValue
        Else
            iAnio = Nothing
        End If
        If (txtCesta.Text IsNot Nothing AndAlso txtCesta.Text <> String.Empty) Then
            iNumCesta = txtCesta.Text 'Num en tabla PEDIDO
        Else
            iNumCesta = Nothing
        End If
        If (txtPedido.Text IsNot Nothing AndAlso txtPedido.Text <> String.Empty) Then
            iNumPedido = txtPedido.Text 'Num en tabla ORDEN
        Else
            iNumPedido = Nothing
        End If
        If MostrarProveedorERP Then _
            sProveERP = txtProveedorERP.Text

        If MostrarNumeroPedidoERP Then _
            sNumPedERP = txtPedidoERP.Text

        If (ddlEmpresa.SelectedValue IsNot Nothing AndAlso ddlEmpresa.SelectedValue <> String.Empty) Then
            iEmpresa = ddlEmpresa.SelectedValue
        Else
            iEmpresa = Nothing
        End If

        If (ddlReceptor.SelectedValue IsNot Nothing AndAlso ddlReceptor.SelectedValue <> String.Empty) Then
            sReceptor = ddlReceptor.SelectedValue
        Else
            sReceptor = Nothing
        End If

        If (ddlGestor.SelectedValue IsNot Nothing AndAlso ddlGestor.SelectedValue <> String.Empty) Then
            sGestor = ddlGestor.SelectedValue
        Else
            sGestor = Nothing
        End If

        If (ddlTipoPedido.SelectedValue IsNot Nothing AndAlso ddlTipoPedido.SelectedValue <> String.Empty) Then
            iTipoPedido = ddlTipoPedido.SelectedValue
        Else
            iTipoPedido = Nothing
        End If

        dFecEmisionDesde = IIf(dteDesdeEmision.Value Is DBNull.Value, Nothing, dteDesdeEmision.Value)
        dFecEmisionHasta = IIf(dteHastaEmision.Value Is DBNull.Value, Nothing, dteHastaEmision.Value)

        dFecEntregaSolicitadaDesde = IIf(dteDesdeEntregaSolicit.Value Is DBNull.Value, Nothing, dteDesdeEntregaSolicit.Value)
        dFecEntregaSolicitadaHasta = IIf(dteHastaEntregaSolicit.Value Is DBNull.Value, Nothing, dteHastaEntregaSolicit.Value)

        dFecEntregaIndicadaDesde = IIf(dteDesdeEntregaIndic.Value Is DBNull.Value, Nothing, dteDesdeEntregaIndic.Value)
        dFecEntregaIndicadaHasta = IIf(dteHastaEntregaIndic.Value Is DBNull.Value, Nothing, dteHastaEntregaIndic.Value)

        Dim dtCategorias As DataTable
        If fsnTvwCategorias IsNot Nothing AndAlso fsnTvwCategorias.SelectedNode IsNot Nothing Then
            sCategoria = fsnTvwCategorias.SelectedNode.Value
            dtCategorias = CrearTablaCategorias()

            Dim arrCategorias() As String
            arrCategorias = Split(sCategoria, "_")
            Dim drCategoria As DataRow
            drCategoria = dtCategorias.NewRow
            Dim x As Byte = 0
            For i = 0 To arrCategorias.Length - 1
                drCategoria("CAT" & i + 1) = arrCategorias(i)
                x = i
            Next
            For i = x + 1 To 4
                drCategoria("CAT" & i + 1) = ""
            Next
            dtCategorias.Rows.Add(drCategoria)
        End If

        sNumPedProve = txtPedidoProve.Text

        If Me.Acceso.gbAccesoFSSM Then
            If (tbCtroCoste_Hidden.Value IsNot Nothing) Then
                SCentro = tbCtroCoste_Hidden.Value
            End If

            'Partidas presupuestarias
            For Each tabla As Control In phrPartidasPresBuscador.Controls
                If TypeOf tabla Is Table Then
                    For Each fila As Control In CType(tabla, Table).Rows
                        For Each celda As Control In CType(fila, TableRow).Cells
                            For Each tb As Control In CType(celda, TableCell).Controls
                                If TypeOf tb Is TextBox Then
                                    Dim tbPartida As TextBox = CType(tb, System.Web.UI.WebControls.TextBox)
                                    If Not String.IsNullOrEmpty(tbPartida.Text) Then
                                        Dim sPartidaHiddenID As String = tbPartida.ID.Replace("tbPPres_", "tbPPres_Hidden_")
                                        Dim tbPartidaHidden As HiddenField = CType(CType(celda, TableCell).FindControl(sPartidaHiddenID), System.Web.UI.WebControls.HiddenField)
                                        Dim sPRES0 As String = (tb.ID).Replace("tbPPres_", "")
                                        Dim sPartidaDen As String = tbPartida.Text.ToString
                                        If tbPartidaHidden IsNot Nothing AndAlso Not String.IsNullOrEmpty(tbPartidaHidden.Value) Then
                                            Dim arrayPartidas As String() = Split(tbPartidaHidden.Value.ToString, TextosSeparadores.dosArrobas)
                                            Dim arrayPartidasTipado As String() = {"", "", "", "", ""}
                                            For x As Integer = 0 To arrayPartidas.Length - 1
                                                arrayPartidasTipado(x) = arrayPartidas(x)
                                            Next
                                            dtPartidas.Rows.Add(arrayPartidasTipado)
                                        End If
                                    End If
                                ElseIf TypeOf tb Is System.Web.UI.WebControls.Label Then
                                    For Each tb2 As Control In CType(tb, Label).Controls
                                        If TypeOf tb2 Is System.Web.UI.WebControls.TextBox Then
                                            Dim tbPartida As TextBox = CType(tb2, System.Web.UI.WebControls.TextBox)
                                            If Not String.IsNullOrEmpty(tbPartida.Text) Then
                                                Dim sPartidaHiddenID As String = tbPartida.ID.Replace("tbPPres_", "tbPPres_Hidden_")
                                                Dim tbPartidaHidden As HiddenField = CType(CType(tb, Label).FindControl(sPartidaHiddenID), System.Web.UI.WebControls.HiddenField)
                                                Dim sPRES0 As String = (tb2.ID).Replace("tbPPres_", "")
                                                Dim sPartidaDen As String = tbPartida.Text.ToString
                                                If tbPartidaHidden IsNot Nothing AndAlso Not String.IsNullOrEmpty(tbPartidaHidden.Value) Then
                                                    Dim arrayPartidas As String() = Split(tbPartidaHidden.Value.ToString, TextosSeparadores.dosArrobas)
                                                    Dim arrayPartidasTipado As String() = {"", "", "", "", ""}
                                                    For x As Integer = 0 To arrayPartidas.Length - 1
                                                        arrayPartidasTipado(x) = arrayPartidas(x)
                                                    Next
                                                    dtPartidas.Rows.Add(arrayPartidasTipado)
                                                End If
                                            End If
                                        End If
                                    Next
                                End If
                            Next
                        Next
                    Next
                End If
            Next
        End If

        'Ordenes usuario y CC. Valores por defecto
        Dim bCargarPedidosUsu As Boolean = True
        Dim bCargarPedidosImpCC As Boolean = False
        'Valores si el usuario tiene PermisoRecepcionarPedidosCCImputables 
        If Me.Acceso.gbAccesoFSSM AndAlso FSNUser.PermisoRecepcionarPedidosCCImputables Then
            'Pedidos del usuario
            bCargarPedidosUsu = chkRecepcionPedidos.Items(0).Selected
            'Pedidos del centro de coste/gestor
            bCargarPedidosImpCC = chkRecepcionPedidos.Items(1).Selected
        Else
            bCargarPedidosUsu = True
            bCargarPedidosImpCC = False
        End If
        bSoloPendientes = chkSoloLineasPendientes.Checked

        'Pedidos abiertos
        If Acceso.gbUsarPedidosAbiertos Then
            Dim bIncluirContraAbiertos As Boolean = chklstTipo.Items.FindByValue(CPedido.TipoOrigenPedido.ContraAbierto).Selected
        End If
        Return oOrdenes.BuscarTodasOrdenesRecepcion(Idioma.ToString(), CodPersona, iAnio, iNumCesta, iNumPedido, sNumPedERP, dFecRecepDesde, dFecRecepHasta, _
             sNumAlbaran, sNumPedProve, sArticulo, sCodArticulo, sCodProve, sProveERP, iTipoPedido, dtCategorias, dFecEntregaSolicitadaDesde, dFecEntregaSolicitadaHasta _
              , iEmpresa, dFecEntregaIndicadaDesde, dFecEntregaIndicadaHasta, dFecEmisionDesde, dFecEmisionHasta, SCentro, dtPartidas, dtTiposOrigen, dtEstados, sGestor, sReceptor, _
            bCargarPedidosUsu, bCargarPedidosImpCC, Acceso.gbAccesoFSSM, bSoloPendientes)
    End Function

    ''' Revisado por: blp. Fecha: 16/01/2012
    ''' <summary>
    ''' Nos devuelve el dataset con los filtros de seleccion aplicados
    ''' </summary>
    ''' <param name="OrdenacionCampo">Campo de ordenacion</param>
    ''' <param name="OrdenacionSentido">Sentido ordenacion</param>        
    ''' <param name="Pedidos">Dataset con los datos</param>  
    ''' <param name="Filtros">Filtros que se han introducido para la busqueda</param>  
    ''' <returns>Nos devuelve el dataset con los filtros de seleccion aplicados</returns>
    ''' <remarks>Llamada desde:DevolverPedidosConFiltro; Tiempo max:1seg.</remarks>
    Private Function DevolverPedidosConFiltro(ByVal OrdenacionCampo As String, ByVal OrdenacionSentido As String, ByVal Pedidos As DataSet, ByVal Filtros As List(Of Filtro)) As DataTable
        Dim dtPed As DataTable = Pedidos.Tables("ORDENES").Copy()
        Dim query As IEnumerable(Of DataRow)
        'Ahora hay una serie de filtros (las fechas) que se usarán con el dataset en caché y otros que implicarán buscar de nuevo en base de datos (Estado, Fecha Desde y Fecha hasta)
        'Aquí dejamos los que corresponden a caché
        For Each ofiltro As Filtro In Filtros
            Dim x As Filtro = ofiltro
            Select Case x.tipo
                Case TipoFiltro.Anio
                    query = From Datos In dtPed.AsEnumerable() _
                            Where Datos.Item("ANYO") = x.Valor _
                            Select Datos
                Case TipoFiltro.Categoria
                    Dim cats As String() = Split(x.Valor, "_")
                    Select Case UBound(cats)
                        Case 0
                            query = From Datos In dtPed _
                                    Join Lineas In DsetPedidos.Tables("LINEASPEDIDO") On Lineas.Item("ORDEN") Equals Datos.Item("ID") _
                                  Where DBNullToSomething(Lineas.Item("CAT1")) = cats(0) _
                                  Select Datos Distinct
                        Case 1
                            query = From Datos In dtPed _
                                    Join Lineas In DsetPedidos.Tables("LINEASPEDIDO") On Lineas.Item("ORDEN") Equals Datos.Item("ID") _
                                  Where DBNullToSomething(Lineas.Item("CAT1")) = cats(0) _
                                    And DBNullToSomething(Lineas.Item("CAT2")) = cats(1) _
                                  Select Datos Distinct
                        Case 2
                            query = From Datos In dtPed _
                                    Join Lineas In DsetPedidos.Tables("LINEASPEDIDO") On Lineas.Item("ORDEN") Equals Datos.Item("ID") _
                                  Where DBNullToSomething(Lineas.Item("CAT1")) = cats(0) _
                                      And DBNullToSomething(Lineas.Item("CAT2")) = cats(1) _
                                      And DBNullToSomething(Lineas.Item("CAT3")) = cats(2) _
                                  Select Datos Distinct
                        Case 3
                            query = From Datos In dtPed _
                                    Join Lineas In DsetPedidos.Tables("LINEASPEDIDO") On Lineas.Item("ORDEN") Equals Datos.Item("ID") _
                                  Where DBNullToSomething(Lineas.Item("CAT1")) = cats(0) _
                                      And DBNullToSomething(Lineas.Item("CAT2")) = cats(1) _
                                      And DBNullToSomething(Lineas.Item("CAT3")) = cats(2) _
                                      And DBNullToSomething(Lineas.Item("CAT4")) = cats(3) _
                                  Select Datos Distinct
                        Case Else
                            query = From Datos In dtPed _
                                    Join Lineas In DsetPedidos.Tables("LINEASPEDIDO") On Lineas.Item("ORDEN") Equals Datos.Item("ID") _
                                  Where DBNullToSomething(Lineas.Item("CAT1")) = cats(0) _
                                      And DBNullToSomething(Lineas.Item("CAT2")) = cats(1) _
                                      And DBNullToSomething(Lineas.Item("CAT3")) = cats(2) _
                                      And DBNullToSomething(Lineas.Item("CAT4")) = cats(3) _
                                      And DBNullToSomething(Lineas.Item("CAT5")) = cats(4) _
                                  Select Datos Distinct
                    End Select
                Case TipoFiltro.Cesta
                    query = From Datos In dtPed.AsEnumerable() _
                                Where DBNullToSomething(Datos.Item("NUMPEDIDO")) = CType(x.Valor, Integer) _
                                Select Datos
                Case TipoFiltro.Empresa
                    query = From Datos In dtPed.AsEnumerable() _
                          Where DBNullToSomething(Datos.Item("EMPRESA")) = CType(x.Valor, Integer) _
                          Select Datos
                Case TipoFiltro.Estado
                    query = From Datos In dtPed.AsEnumerable() _
                            Where (Datos.Item("EST") And CType(x.Valor, CPedido.Estado)) = Datos.Item("EST") _
                            Select Datos
                Case TipoFiltro.FechaPedidoDesde, TipoFiltro.FechaPedidoHasta
                    query = From Datos In dtPed.AsEnumerable() _
                          Select Datos
                Case TipoFiltro.NumPedido
                    query = From Datos In dtPed.AsEnumerable() _
                            Where DBNullToSomething(Datos.Item("NUMORDEN")) = CType(x.Valor, Integer) _
                            Select Datos
                Case TipoFiltro.NumPedidoERP
                    query = From Datos In dtPed.AsEnumerable() _
                          Where DBNullToStr(Datos.Item("REFERENCIA")).ToString().ToUpper() = x.Valor.ToUpper() _
                          Select Datos
                Case TipoFiltro.NumPedidoProve
                    query = From Datos In dtPed.AsEnumerable() _
                          Where DBNullToStr(Datos.Item("NUMEXT")).ToString().ToUpper() = x.Valor.ToUpper() _
                          Select Datos
                Case TipoFiltro.Proveedor
                    query = From Datos In dtPed.AsEnumerable() _
                          Where Datos.Item("PROVECOD") = x.Valor _
                          Select Datos
                Case TipoFiltro.ProveedorERP
                    query = From Datos In dtPed.AsEnumerable() _
                          Where DBNullToStr(Datos.Item("COD_PROVE_ERP")).ToString().ToUpper() = x.Valor.ToUpper() _
                          Select Datos
                Case TipoFiltro.Tipo
                    Dim iMostrarPedidosGS As Boolean = x.Valores(0)
                    Dim iMostrarPedidosEP As Boolean = x.Valores(1)
                    Dim iMostrarPedidosEPLIBRES As Boolean = x.Valores(2)
                    Dim iMostrarPedidosERP As Boolean = x.Valores(3)

                    query = From Datos In dtPed.AsEnumerable() _
                              Select Datos

                    If Not iMostrarPedidosGS Then
                        query = From Datos In query _
                                Where Datos.Item("TIPO") <> 0 _
                                Select Datos
                    End If
                    If Not iMostrarPedidosEP Then
                        query = From Datos In query _
                                Join Lineas In Pedidos.Tables("LINEASPEDIDO") On Lineas.Item("ORDEN") Equals Datos.Item("ID") _
                                Where Datos.Item("TIPO") <> 1 OrElse (Datos.Item("TIPO") = 1 AndAlso Lineas.Item("ART_INT") Is DBNull.Value) _
                                Select Datos Distinct
                    End If
                    If Not iMostrarPedidosEPLIBRES Then
                        query = From Datos In query _
                                Join Lineas In Pedidos.Tables("LINEASPEDIDO") On Lineas.Item("ORDEN") Equals Datos.Item("ID") _
                                Where Datos.Item("TIPO") <> 1 OrElse (Datos.Item("TIPO") = 1 AndAlso Lineas.Item("ART_INT") IsNot DBNull.Value) _
                                Select Datos Distinct
                    End If
                    If Not iMostrarPedidosERP Then
                        query = From Datos In query _
                                Where Datos.Item("TIPO") <> 2 _
                                Select Datos
                    End If

                Case TipoFiltro.TipoPedido
                    query = From Datos In dtPed.AsEnumerable() _
                          Where Datos.Item("TIPOPEDIDO") = x.Valor _
                          Select Datos
                Case TipoFiltro.OrdenID
                    query = From Datos In dtPed.AsEnumerable() _
                          Where Datos.Item("ID") = x.Valor _
                          Select Datos
                Case Else
                    Dim arrayPalabras As String() = Split(x.Valor)
                    query = From Datos In dtPed.AsEnumerable() _
                          Join Lineas In DsetPedidos.Tables("LINEASPEDIDO") On Lineas.Item("ORDEN") Equals Datos.Item("ID") _
                          Let w = UCase(DBNullToSomething(Lineas.Item("ART_INT")) & " " & DBNullToSomething(Lineas.Item("ART")) & " " & DBNullToSomething(Lineas.Item("DESCR")) & " " & DBNullToSomething(Lineas.Item("ARTDEN")) & " " & DBNullToSomething(Lineas.Item("COD_EXT"))) _
                          Where arrayPalabras.All(Function(palabra) w Like "*" & strToSQLLIKE(palabra, True) & "*") _
                          Select Datos
            End Select
            If query.Count() > 0 Then
                dtPed = query.CopyToDataTable()
            Else
                dtPed.Rows.Clear()
                Exit For
            End If
        Next
        If dtPed.Rows.Count > 0 Then
            Select Case OrdenacionCampo
                Case "EMPRESA"
                    dtPed.Columns.Add("ORDENACION", Type.GetType("System.String"), "DENEMPRESA")
                Case "PROVEEDOR"
                    dtPed.Columns.Add("ORDENACION", Type.GetType("System.String"), "PROVEDEN")
                Case "IMPORTE"
                    dtPed.Columns.Add("ORDENACION", Type.GetType("System.Double"), "IMPORTE * CAMBIO")
                Case "FECHA DE EMISION"
                    dtPed.Columns.Add("ORDENACION", Type.GetType("System.DateTime"), "FECHA")
                Case "APROVISIONADOR"
                    dtPed.Columns.Add("ORDENACION", Type.GetType("System.String"), "APROV")
                Case "REF. EN FACTURA"
                    dtPed.Columns.Add("ORDENACION", Type.GetType("System.String"), "REFERENCIA")
                Case "PROVEEDOR ERP"
                    dtPed.Columns.Add("ORDENACION", Type.GetType("System.String"), "COD_PROVE_ERP")
                Case "TIPO DE PEDIDO"
                    dtPed.Columns.Add("ORDENACION", Type.GetType("System.String"), "TIPOPEDIDO_DEN")
                Case "ESTADO"
                    dtPed.Columns.Add("ORDENACION", Type.GetType("System.Double"), "EST")
                Case Else
                    dtPed.Columns.Add("ORDENACION", Type.GetType("System.DateTime"), "FECHA")
            End Select
            If OrdenacionSentido = "ASC" Then
                query = From Datos In dtPed.AsEnumerable() _
                        Order By DBNullToSomething(Datos.Item("ORDENACION")) Ascending _
                        Select Datos
            Else
                query = From Datos In dtPed.AsEnumerable() _
                      Order By DBNullToSomething(Datos.Item("ORDENACION")) Descending _
                      Select Datos
            End If
            dtPed = CType(query, EnumerableRowCollection(Of DataRow)).CopyToDataTable()
        End If
        Return dtPed
    End Function

    Private Function DevolverPedidosConFiltro(ByVal OrdenacionCampo As String, ByVal OrdenacionSentido As String) As DataTable
        Return DevolverPedidosConFiltro(OrdenacionCampo, OrdenacionSentido, DsetPedidos, FiltrosAnyadidos)
    End Function

#Region "Shared"
    ''' Revisado por: blp. Fecha: 27/02/2012
    ''' <summary>
    ''' Muestra los artículos que coincidan en la caché de pedidos con lo tecleado en el control
    ''' </summary>
    ''' <param name="prefixText">texto buscado</param>
    ''' <param name="count">cantidad de resultado a devolver</param>
    ''' <param name="contextKey">identificativo del objeto de cache en el que buscar</param>
    ''' <returns>Array con la lista de artículos que coincida con la búsqueda</returns>
    ''' <remarks>Llamada desde Recepcion.aspx. Máx. 0,5 seg.</remarks>
    <System.Web.Services.WebMethodAttribute(), _
        System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function AutoCompletar_Articulo(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As IEnumerable(Of String)
        Dim textoBuscadoBase = prefixText.Substring(0, 3)
        Dim sArticulos As String()
        If HttpContext.Current.Cache("DsetArticulosRecepcion_" & contextKey & "_codArt" & textoBuscadoBase) Is Nothing Then
            EliminarCacheArticulos(contextKey)
            sArticulos = DevolverArticulos(textoBuscadoBase, contextKey)
        Else
            sArticulos = CType(HttpContext.Current.Cache("DsetArticulosSeguimiento_" & contextKey & "_codArt" & textoBuscadoBase), String())
        End If
        Dim resul As IEnumerable(Of String) = From articulo In sArticulos _
                                              Where articulo.ToUpper.Contains(prefixText.ToUpper) _
                                              Take (count)
        Return resul
    End Function

    ''' Revisado por: blp. Fecha: 23/01/2013
    ''' <summary>
    ''' Devuelve las descripciones de los artículos que coinciden con el texto pasado como parametro y corresponden al usuario
    ''' </summary>
    ''' <param name="textoFiltroArticulo">Texto buscado</param>
    ''' <param name="codPer">Código de la persona</param>
    ''' <returns>array con las descripciones</returns>
    ''' <remarks>Laamada desde Autocompletar. Max. 1 seg.</remarks>
    Public Shared Function DevolverArticulos(ByVal textoFiltroArticulo As String, ByVal codPer As String) As String()
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim oArticulos As cArticulos = oServer.Get_Object(GetType(cArticulos))
        If HttpContext.Current.Cache("DsetArticulosRecepcion_" & codPer & "_codArt" & textoFiltroArticulo) Is Nothing Then
            Dim dsResul As DataSet = oArticulos.DevolverDescripcionesArticulos(textoFiltroArticulo, codPer, oUsuario.PermisoVerPedidosCCImputables)
            Dim resul As String()
            If dsResul IsNot Nothing AndAlso dsResul.Tables(0) IsNot Nothing AndAlso dsResul.Tables(0).Rows.Count > 0 Then
                For i = 0 To dsResul.Tables(0).Rows.Count - 1
                    ReDim Preserve resul(i)
                    resul(i) = dsResul.Tables(0).Rows(i).Item("DESCRDEF")
                Next
                HttpContext.Current.Cache.Insert("DsetArticulosRecepcion_" & codPer & "_codArt" & textoFiltroArticulo, resul, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration,
                                                 New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            Else
                ReDim resul(0)
                resul(0) = String.Empty
            End If
            Return resul
        Else
            Return HttpContext.Current.Cache("DsetArticulosRecepcion_" & codPer & "_codArt" & textoFiltroArticulo)
        End If
    End Function

    ''' Revisado por: blp. Fecha: 20/08/2012
    ''' <summary>
    ''' Muestra los proveedores que coincidan en la caché de pedidos con lo tecleado en el control
    ''' </summary>
    ''' <param name="prefixText">texto buscado</param>
    ''' <param name="count">cantidad de resultado a devolver</param>
    ''' <param name="contextKey">identificativo del objeto de cache en el que buscar</param>
    ''' <returns>Array con la lista de proveedores que coincida con la búqueda</returns>
    ''' <remarks>Llamada desde Recepcion.aspx. Máx. 0,5 seg.</remarks>
    <System.Web.Services.WebMethodAttribute(), _
        System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function AutoCompletar_Proveedor(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Dim textoBuscadoBase = prefixText.Substring(0, 3)
        Dim sProveedores As String()
        If HttpContext.Current.Cache("DsetProveedoresRecepcion_" & contextKey & "_" & textoBuscadoBase) Is Nothing Then
            sProveedores = DevolverProveedores(textoBuscadoBase, contextKey)
        Else
            'Return CType(HttpContext.Current.Cache("DsetProveedoresRecepcion_" & contextKey & "_" & textoBuscadoBase), String()).Take(count)
            sProveedores = HttpContext.Current.Cache("DsetProveedoresRecepcion_" & contextKey & "_" & textoBuscadoBase)
        End If
        Dim sResultado As String() = Nothing
        Dim l As String = 0
        For i = 0 To sProveedores.Length - 1
            'cuando no ha encontrado nada la longitud es 1.
            If sProveedores(i) <> Nothing AndAlso sProveedores(i).Split(",")(0).Split(":")(1).ToUpper.Contains(UCase(prefixText)) Then
                ReDim Preserve sResultado(l)
                sResultado(l) = sProveedores(i)
                If l >= count - 1 Then Exit For
                l = l + 1
            End If
        Next
        Return sResultado
    End Function

    ''' Revisado por: sra. Fecha: 27/02/2013
    ''' <summary>
    ''' Devuelve las descripciones de los artículos que coinciden con el texto pasado como parametro y corresponden al usuario
    ''' </summary>
    ''' <param name="textoFiltroProveedor">Texto buscado</param>
    ''' <param name="codPer">Código de la persona</param>
    ''' <returns>array con las descripciones</returns>
    ''' <remarks>Laamada desde Autocompletar. Max. 1 seg.</remarks>
    Public Shared Function DevolverProveedores(ByVal textoFiltroProveedor As String, ByVal codPer As String) As String()
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim oProveedores As CProveedores = oServer.Get_Object(GetType(CProveedores))
        If HttpContext.Current.Cache("DsetProveedoresRecepcion_" & codPer & "_" & textoFiltroProveedor) Is Nothing Then
            Dim dsResul As DataSet = oProveedores.DevolverProveedoresCoincidentes(textoFiltroProveedor, codPer, oUsuario.PermisoVerPedidosCCImputables)
            Dim resul As String()
            If dsResul IsNot Nothing AndAlso dsResul.Tables(0) IsNot Nothing AndAlso dsResul.Tables(0).Rows.Count > 0 Then
                For i = 0 To dsResul.Tables(0).Rows.Count - 1
                    ReDim Preserve resul(i)
                    resul(i) = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dsResul.Tables(0).Rows(i).Item("PROVECOD") & " - " & dsResul.Tables(0).Rows(i).Item("PROVEDEN"), dsResul.Tables(0).Rows(i).Item("PROVECOD"))
                Next
                HttpContext.Current.Cache.Insert("DsetProveedoresRecepcion_" & codPer & "_" & textoFiltroProveedor, resul, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration,
                                                 New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            Else
                ReDim resul(0)
                resul(0) = String.Empty
            End If
            Return resul
        Else
            Return HttpContext.Current.Cache("DsetProveedoresRecepcion_" & codPer & "_" & textoFiltroProveedor)
        End If
    End Function

#Region "Tooltips"

    ''' Revisado por:blp. Fecha: 28/02/2012
    ''' <summary>
    ''' Devuelve las observaciones de una orden
    ''' </summary>
    ''' <param name="contextKey">Id de la orden</param>
    ''' <returns>String con las observaciones</returns>
    ''' <remarks>Llamadas desde Recepcion.aspx. Máx. 0,1 seg.</remarks>
    <System.Web.Services.WebMethodAttribute(), _
        System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function Observaciones(ByVal contextKey As String) As String
        Dim dsOrdenes As DataSet = GetCacheDataset()
        If dsOrdenes Is Nothing Then
            Return GetObservacionesOrden(contextKey)
        Else
            Return dsOrdenes.Tables("ORDENES").Rows.Find(contextKey).Item("OBS")
        End If
    End Function


    ''' Revisado por: blp. Fecha: 28/02/2012
    ''' <summary>
    ''' Devuelve las observaciones de una orden
    ''' </summary>
    ''' <param name="idOrden">Id de la orden</param>
    ''' <returns>String Observaciones</returns>
    ''' <remarks>Llamada desde Observaciones(). Máx 0,1 seg.</remarks>
    Private Shared Function GetObservacionesOrden(ByVal idOrden As Integer) As String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim oOrden As COrden = oServer.Get_Object(GetType(FSNServer.COrden))
        oOrden.ID = idOrden
        Dim sObservaciones As String = oOrden.GetObservaciones()
        Return sObservaciones
    End Function

    ''' Revisado por:blp. Fecha: 28/02/2012
    ''' <summary>
    ''' Devuelve los comentarios de proveedor de una orden
    ''' </summary>
    ''' <param name="contextKey">Id de la orden</param>
    ''' <returns>String con los comentarios de proveedor</returns>
    ''' <remarks>Llamadas desde Recepcion.aspx. Máx. 0,1 seg.</remarks>
    <System.Web.Services.WebMethodAttribute(), _
    System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function ObservacionesProv(ByVal contextKey As String) As String
        Dim dsOrdenes As DataSet = GetCacheDataset()
        If dsOrdenes Is Nothing Then
            Return GetComentarioProveOrden(contextKey)
        Else
            Return dsOrdenes.Tables("ORDENES").Rows.Find(contextKey).Item("COMENTPROVE")
        End If
    End Function

    ''' Revisado por: blp. Fecha: 28/02/2012
    ''' <summary>
    ''' Devuelve el comentario del proveedor de la orden
    ''' </summary>
    ''' <param name="idOrden">Id de la orden</param>
    ''' <returns>String Comentario del proveedor</returns>
    ''' <remarks>Llamada desde ObservacionesProv(). Máx 0,1 seg.</remarks>
    Private Shared Function GetComentarioProveOrden(ByVal idOrden As Integer) As String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim oOrden As COrden = oServer.Get_Object(GetType(FSNServer.COrden))
        oOrden.ID = idOrden
        Dim sObservaciones As String = oOrden.GetComentarioProve()
        Return sObservaciones
    End Function

    ''' Revisado por:blp. Fecha: 28/02/2012
    ''' <summary>
    ''' Devuelve las observaciones de una línea
    ''' </summary>
    ''' <param name="contextKey">Id de la línea</param>
    ''' <returns>String con las observaciones</returns>
    ''' <remarks>Llamadas desde Recepcion.aspx. Máx. 0,1 seg.</remarks>
    <System.Web.Services.WebMethodAttribute(), _
    System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function ObservacionesLinea(ByVal contextKey As String) As String
        Dim dsOrdenes As DataSet = GetCacheDataset()
        If dsOrdenes Is Nothing Then
            Return GetObservacionesLinea(contextKey)
        Else
            Return dsOrdenes.Tables("LINEASPEDIDO").Rows.Find(contextKey).Item("OBS")
        End If
    End Function

    ''' Revisado por: blp. Fecha: 28/02/2012
    ''' <summary>
    ''' Devuelve el comentario del proveedor de la línea
    ''' </summary>
    ''' <param name="idLinea">Id de la línea</param>
    ''' <returns>String Comentario del proveedor</returns>
    ''' <remarks>Llamada desde ObservacionesProv(). Máx 0,1 seg.</remarks>
    Private Shared Function GetObservacionesLinea(ByVal idLinea As Integer) As String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim oLinea As CLinea = oServer.Get_Object(GetType(FSNServer.CLinea))
        oLinea.ID = idLinea
        Dim sObservaciones As String = oLinea.GetObservaciones()
        Return sObservaciones
    End Function


    ''' Revisado por:blp. Fecha: 28/02/2012
    ''' <summary>
    ''' Devuelve los comentarios de una adjunto de una orden o de una línea
    ''' </summary>
    ''' <param name="contextKey">Identificador de si es orden o línea e Id de la línea/orden</param>
    ''' <returns>String con los comentarios</returns>
    ''' <remarks>Llamadas desde Recepcion.aspx. Máx. 0,1 seg.</remarks>
    <System.Web.Services.WebMethodAttribute(), _
    System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function ComentarioAdj(ByVal contextKey As String) As String
        Dim dsOrdenes As DataSet = GetCacheDataset()
        Dim keys() As String = Split(contextKey)
        If dsOrdenes Is Nothing Then
            Return GetComentariosAdjunto(keys(0), CType(keys(1), TiposDeDatos.Adjunto.Tipo))
        Else
            Select Case CType(keys(1), TiposDeDatos.Adjunto.Tipo)
                Case TiposDeDatos.Adjunto.Tipo.Orden_Entrega
                    Return dsOrdenes.Tables("ADJUNTOS_ORDEN").Rows.Find(keys(0)).Item("COMENTARIO")
                Case TiposDeDatos.Adjunto.Tipo.Linea_Pedido
                    Return dsOrdenes.Tables("ADJUNTOS_LINEA").Rows.Find(keys(0)).Item("COMENTARIO")
            End Select
            Dim sComent As String = String.Empty
            Return sComent
        End If
    End Function

    ''' Revisado por: blp. Fecha: 28/02/2012
    ''' <summary>
    ''' Devuelve el comentario del adjunto cuyo id se pasa como parámetro
    ''' </summary>
    ''' <param name="idAdj">Id del adjunto</param>
    ''' <param name="tipoAdjunto">Tipo de adjunto: En este caso puede ser un adjunto de orden o de línea</param>
    ''' <returns>String con el Comentario</returns>
    ''' <remarks>Llamada desde ComentarioAdj(). Máx 0,1 seg.</remarks>
    Private Shared Function GetComentariosAdjunto(ByVal idAdj As Integer, ByVal tipoAdjunto As TiposDeDatos.Adjunto.Tipo) As String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim oAdjunto As Adjunto = oServer.Get_Object(GetType(FSNServer.Adjunto))
        oAdjunto.Id = idAdj
        Dim sObservaciones As String = oAdjunto.GetComentarios(tipoAdjunto)
        Return sObservaciones
    End Function


    ''' Revisado por: blp. Fecha:28/02/2012
    ''' <summary>
    ''' Recuperamos la cache de pedidos
    ''' </summary>
    ''' <returns>Dataset de recepciones en caché si existe. Si no, Nothing</returns>
    ''' <remarks>Llamada desde Recepcion.aspx.vb. Máx 0,5 seg.</remarks>
    Private Shared Function GetCacheDataset() As DataSet
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        If oUsuario Is Nothing Then Return Nothing
        Dim dsOrdenes As New DataSet
        If HttpContext.Current.Cache("DsetPedidosRecepcion_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString()) IsNot Nothing Then
            dsOrdenes = CType(HttpContext.Current.Cache("DsetPedidosRecepcion_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString()), DataSet).Copy
        End If
        Return dsOrdenes
    End Function

    <System.Web.Services.WebMethodAttribute(), _
    System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function DevolverTexto(ByVal contextKey As String) As String
        Return contextKey
    End Function

#End Region

#End Region

    ''' Revisado por: blp. Fecha: 18/04/2012
    ''' <summary>
    ''' Genera una lista con todos los tipos de pedidos existentes
    ''' </summary>
    ''' <returns>Una lista de ListItem</returns>
    ''' <remarks>Llamada desde InicializarControles. Máximo 0,1 seg.</remarks>
    Public Function ListaTiposPedidos() As List(Of ListItem)
        If HttpContext.Current.Cache("DsetSeguimientoTiposPedidos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
            Dim oTiposPedido As cTiposPedidos = FSNServer.Get_Object(GetType(cTiposPedidos))
            oTiposPedido.CargarTiposPedido(Usuario.Idioma)
            Dim query As List(Of ListItem) = From TiposPedido As cTipoPedido In oTiposPedido
                                             Select New ListItem(TiposPedido.Denominacion, TiposPedido.Id) Distinct.ToList()
            Me.InsertarEnCache("DsetSeguimientoTiposPedidos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString(), query)
            Return query
        Else
            Return HttpContext.Current.Cache("DsetSeguimientoTiposPedidos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
        End If
    End Function

    ''' Revisado por: blp. Fecha: 16/01/2012
    ''' <summary>
    ''' Devuelve el listado de Empresas de los pedidos recepcionados y por recepcionar
    ''' </summary>
    ''' <param name="dsInfo">
    ''' Pasamos el dataset desde el que recupera los datos
    ''' </param>
    ''' <returns>Listado de Empresas</returns>
    ''' <remarks>Llamada desde InicializarControles. Máx. 0,2 seg.</remarks>
    Private Function Empresas(ByVal dsInfo As DataSet) As List(Of ListItem)
        Dim query As List(Of ListItem) = From Datos In dsInfo.Tables("EMPRESAS") _
                Order By Datos.Item("DENEMPRESA") _
                  Select New ListItem(Datos.Item("DENEMPRESA"), Datos.Item("EMPRESA")) Distinct.ToList()
        Return query

    End Function

    ''' Revisado por: blp. Fecha: 18/04/2012
    ''' <summary>
    ''' Devuelve el listado de Receptores de los pedidos recepcionados y por recepcionar
    ''' </summary>
    ''' <param name="dsInfo">
    ''' Pasamos el dataset desde el que recupera los datos
    ''' </param>
    ''' <returns>Lista con los receptores de todos los pedidos del usuario y/o los centros de coste a los que puede imputar</returns>
    ''' <remarks>Llamada desde InicializarControles. Máximo 0,1 seg.</remarks>
    Public Function Receptores(ByVal dsInfo As DataSet) As List(Of ListItem)
        Dim query As List(Of ListItem) = From Datos In dsInfo.Tables("RECEPTORES") _
                Where Datos.Item("RECEPTOR") IsNot System.DBNull.Value _
                Order By Datos.Item("RECEP_NOM") _
                  Select New ListItem(Datos.Item("RECEP_NOM") & " " & Datos.Item("RECEP_APE"), DBNullToStr(Datos.Item("RECEPTOR"))) Distinct.ToList()
        Return query
    End Function
    ''' Revisado por: blp. Fecha: 24/05/2013
    ''' <summary>
    ''' Carga los años
    ''' </summary>
    ''' <remarks>Llamada desde InicializarControles. Máximo 0,1 seg.</remarks>
    Private Sub CargarAnyos()
        ddlAnio.Items.Clear()
        ddlAnio.Items.Add(New ListItem(String.Empty, String.Empty))
        For i As Integer = Year(Now) To AnyoMin Step -1
            ddlAnio.Items.Add(New ListItem(i.ToString, i))
        Next
    End Sub

    ''' Revisado por: blp. Fecha: 18/04/2012
    ''' <summary>
    ''' Genera una lista con los gestores existentes en la lista de pedidos
    ''' </summary>
    ''' <param name="dsInfo">
    ''' Podemos pasarle el dataset desde es que recupera los datos o dejar que los tome del DsetPedidos
    ''' </param>
    ''' <returns>Una lista de ListItem</returns>
    ''' <remarks>Llamada desde InicializarControles. Máximo 0,1 seg.</remarks>
    Private Function ListaGestores(ByVal dsInfo As DataSet) As DataTable
        Dim query = From Datos In dsInfo.Tables("GESTORES") _
                                       Select Datos.Item("GESTOR") Distinct

        Dim oPersonas As CPersonas = Me.FSNServer.Get_Object(GetType(FSNServer.CPersonas))
        Dim dtCodGestores As DataTable = oPersonas.CrearTablaGestores()
        If query.Any Then
            For Each gestorcod In query
                dtCodGestores.Rows.Add({gestorcod})
            Next
        End If
        Dim dtGestores As DataTable = oPersonas.CargarGestores(dtCodGestores)
        Return dtGestores
    End Function

    ''' Revisado por: blp. Fecha: 22/08/2012
    ''' <summary>
    ''' Función que devuelve un objeto List con los Centros de coste presentes en las entregas.
    ''' </summary>
    ''' <param name="dsInfo">
    ''' Podemos pasarle el dataset desde es que recupera los datos o dejar que los tome del DsetPedidos
    ''' </param>
    ''' <returns>Un dataset con los Centros de coste presentes en las entregas.</returns>
    ''' <remarks>
    ''' Llamada desde: entregasPedidos.vb.InicializarControles. Máx 0,2 seg</remarks>
    Private Function CentrosCoste(ByVal dsInfo As DataSet) As List(Of ListItem)
        Dim query As List(Of ListItem)
        query = From Datos In dsInfo.Tables("ORDENES") _
                Where Not Datos.Item("CCDEN") Is System.DBNull.Value And Not Datos.Item("CCCOD") Is System.DBNull.Value _
                Order By Datos.Item("CCDEN") Ascending _
                Select New ListItem(Datos.Item("CCDEN").ToString, Datos.Item("CCCOD").ToString) Distinct.ToList()

        Return query

    End Function

    ''' <summary>
    ''' Comprobamos si tenemos datos en caché de años, proveedores, tipos de pedido o Empresas.
    ''' Devuelve TRUE si hay datos
    ''' Devuelve FALSE si no hay
    ''' </summary>
    Private ReadOnly Property getDropDownCacheStatusOK() As Boolean
        Get
            If HttpContext.Current.Cache("DsetRecepcionAnyos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing _
                OrElse HttpContext.Current.Cache("DsetRecepcionProve_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing _
                OrElse HttpContext.Current.Cache("DsetRecepcionEmpresas_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing _
                OrElse HttpContext.Current.Cache("DsetRecepcionTiposPedidos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing _
                OrElse HttpContext.Current.Cache("DsetRecepcionGestores_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing _
                OrElse HttpContext.Current.Cache("DsetRecepcionCentroCoste_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                Return False
            Else
                Return True
            End If
        End Get
    End Property

#End Region

#Region "Filtros"

    <Serializable()> _
    Public Enum TipoFiltro As Integer
        Anio = 0
        Cesta = 1
        NumPedido = 2
        NumPedidoProve = 3
        NumPedidoERP = 4
        Articulo = 5
        FechaPedidoDesde = 6
        FechaPedidoHasta = 7
        Empresa = 8
        Proveedor = 9
        ProveedorERP = 10
        Estado = 11
        Tipo = 12
        Categoria = 13
        TipoPedido = 14
        OrdenID = 15
        Gestor = 16
        PedidosCentroCoste = 17
        PedidosUsuario = 18
        FechaRecepcionDesde = 19
        FechaRecepcionHasta = 20
        FechaEntregaSolicitadaDesde = 21
        FechaEntregaSolicitadaHasta = 22
        FechaEntregaIndicadaDesde = 23
        FechaEntregaIndicadaHasta = 24
        Albaran = 25
        CentroCoste = 26
        PartidaPresupuestaria = 27
        Receptor = 28
    End Enum

    <Serializable()> _
    Public Structure Filtro
        Dim _tipo As TipoFiltro
        Dim _valor As String
        Dim _valores() As Boolean
        Dim _valorfecha As DateTime
        Dim sfilterInfo As String

        Public Property tipo() As TipoFiltro
            Get
                Return _tipo
            End Get
            Set(ByVal value As TipoFiltro)
                _tipo = value
            End Set
        End Property

        Public Property Valor() As String
            Get
                Return _valor
            End Get
            Set(ByVal value As String)
                _valor = value
            End Set
        End Property

        Public Property Valores() As Boolean()
            Get
                Return _valores
            End Get
            Set(ByVal value As Boolean())
                _valores = value
            End Set
        End Property

        Public Property ValorFecha() As DateTime
            Get
                Return _valorfecha
            End Get
            Set(ByVal value As DateTime)
                _valorfecha = value
            End Set
        End Property

        ''' <summary>
        ''' Propiedad del filtro que nos indica cualquier dato adicional que convenga conocer.
        ''' En el caso de las partidas presupuestarias, sirve para guardar la partida de nivel 0 / Nivel al que corresponde el filtro
        ''' (dado que puede haber más de un filtro de partida presupuestaria)
        ''' </summary>
        Public Property Info() As String
            Get
                Return sfilterInfo
            End Get
            Set(ByVal value As String)
                sfilterInfo = value
            End Set
        End Property

        ''' Revisado por: blp. Fecha: 22/08/2012
        ''' <summary>
        ''' Método que crea una nueva instancia de la estructura
        ''' </summary>
        ''' <param name="Tipo">Tipo de Filtro</param>
        ''' <param name="Valor">Valor string del filtro</param>
        ''' <remarks>Llamada desde la creación de la instancia. Máx. 0,1 seg.</remarks>
        Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As String)
            _tipo = Tipo
            _valor = Valor
        End Sub

        ''' Revisado por: blp. Fecha: 22/08/2012
        ''' <summary>
        ''' Método que crea una nueva instancia de la estructura
        ''' </summary>
        ''' <param name="Tipo">Tipo de Filtro</param>
        ''' <param name="Valor">Valor fecha del filtro</param>
        ''' <remarks>Llamada desde la creación de la instancia. Máx. 0,1 seg.</remarks>
        Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As DateTime)
            _tipo = Tipo
            _valorfecha = Valor
        End Sub

        ''' Revisado por: blp. Fecha: 22/08/2012
        ''' <summary>
        ''' Método que crea una nueva instancia de la estructura
        ''' </summary>
        ''' <param name="Tipo">Tipo de Filtro</param>
        ''' <param name="Valor">Valor boolean del filtro</param>
        ''' <remarks>Llamada desde la creación de la instancia. Máx. 0,1 seg.</remarks>
        Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As Boolean())
            _tipo = Tipo
            _valores = Valor
        End Sub

        ''' Revisado por: blp. Fecha: 22/08/2012
        ''' <summary>
        ''' Crear un filtro nuevo
        ''' </summary>
        ''' <param name="Tipo">Tipo de filtro</param>
        ''' <param name="Valor">Valor tipo string del filtro creado</param>
        ''' <param name="Info">Info adicional que se desee conservar del filtro</param>
        ''' <remarks>Llamada desde la creación de la instancia. Máx. 0,1 seg.</remarks>
        Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As String, ByVal Info As String)
            _tipo = Tipo
            _valor = Valor
            sfilterInfo = Info
        End Sub

    End Structure

    Public Property FiltrosAnyadidos() As List(Of Filtro)
        Get
            If ViewState("FiltrosAnyadidos") Is Nothing Then
                Return New List(Of Filtro)
            Else
                Return CType(ViewState("FiltrosAnyadidos"), List(Of Filtro))
            End If
        End Get
        Set(ByVal value As List(Of Filtro))
            ViewState("FiltrosAnyadidos") = value
        End Set
    End Property

    ''' Revisado por: blp. Fecha: 16/01/2012
    ''' <summary>
    ''' Procedimiento que añade los filtros que se hayan seleccionado en el panel buscador a la propiedad FiltrosAnyadidos
    ''' de la página Seguimiento.aspx.vb
    ''' </summary>
    ''' <remarks>LLamada desde el propio objeto: btnBuscar_Click, Page_Load
    ''' Tiempo maximo 1 sec</remarks>
    Private Sub ActualizarFiltros()
        Dim lis As New List(Of Filtro)
        If Not String.IsNullOrEmpty(ddlAnio.SelectedValue) Then _
            lis.Add(New Filtro(TipoFiltro.Anio, ddlAnio.SelectedValue))
        If Not String.IsNullOrEmpty(txtCesta.Text) AndAlso IsNumeric(txtCesta.Text) Then _
            lis.Add(New Filtro(TipoFiltro.Cesta, txtCesta.Text))
        If Not String.IsNullOrEmpty(txtPedido.Text) Then
            Dim itxtPedido As Integer
            Try
                itxtPedido = CType(txtPedido.Text, Integer)
            Catch ex As Exception
                itxtPedido = -1
            End Try
            lis.Add(New Filtro(TipoFiltro.NumPedido, CType(itxtPedido, String)))
        End If
        If Not String.IsNullOrEmpty(txtPedidoProve.Text) Then _
            lis.Add(New Filtro(TipoFiltro.NumPedidoProve, txtPedidoProve.Text))
        If Not String.IsNullOrEmpty(txtPedidoERP.Text) Then _
            lis.Add(New Filtro(TipoFiltro.NumPedidoERP, txtPedidoERP.Text))
        If Not String.IsNullOrEmpty(ddlTipoPedido.SelectedValue) Then _
            lis.Add(New Filtro(TipoFiltro.TipoPedido, ddlTipoPedido.SelectedValue))
        If Not String.IsNullOrEmpty(txtArticulo.Text) Then _
            lis.Add(New Filtro(TipoFiltro.Articulo, txtArticulo.Text))
        lis.Add(New Filtro(TipoFiltro.FechaPedidoDesde, CType(dteDesdeEmision.Value, Date)))
        ''Comparamos el nuevo valor del filtro con el anterior (si existe)
        ''para saber si hay que buscar de nuevo los resultados (dado que la caché no busca todos los resultados sino que filtra por fecha)
        'comparaFiltros(TipoFiltro.FechaPedidoDesde, dteDesdeEmision.Value)
        lis.Add(New Filtro(TipoFiltro.FechaPedidoHasta, CType(dteHastaEmision.Value, Date)))
        ''Comparamos el nuevo valor del filtro con el anterior (si existe)
        ''para saber si hay que buscar de nuevo los resultados (dado que la caché no busca todos los resultados sino que filtra por fecha)
        'comparaFiltros(TipoFiltro.FechaHasta, dteHastaEmision.Value)
        If Not String.IsNullOrEmpty(ddlEmpresa.SelectedValue) Then _
            lis.Add(New Filtro(TipoFiltro.Empresa, ddlEmpresa.SelectedValue))
        If Not String.IsNullOrEmpty(ddlReceptor.SelectedValue) Then _
            lis.Add(New Filtro(TipoFiltro.Receptor, ddlReceptor.SelectedValue))
        If Not String.IsNullOrEmpty(txtProveedor.Text) AndAlso Not String.IsNullOrEmpty(hidProveedor.Value) Then _
            lis.Add(New Filtro(TipoFiltro.Proveedor, hidProveedor.Value))
        If Not String.IsNullOrEmpty(txtProveedorERP.Text) And txtProveedorERP.Visible Then _
            lis.Add(New Filtro(TipoFiltro.ProveedorERP, txtProveedorERP.Text))
        Dim iEst As CPedido.Estado
        For Each l As ListItem In chklstEstado.Items
            If l.Selected Then iEst = iEst Or CType(l.Value, CPedido.Estado)
        Next
        If iEst = CPedido.Estado.SinEstado Then
            For Each l As ListItem In chklstEstado.Items
                l.Selected = True
            Next
            iEst = 120
        End If
        lis.Add(New Filtro(TipoFiltro.Estado, CType(iEst, Integer).ToString()))

        Dim iMostrarPedidos(5) As Boolean
        Dim iMostrarPedidosGS As Boolean = True
        Dim iMostrarPedidosEP As Boolean = True
        Dim iMostrarPedidosEPLIBRES As Boolean = True
        Dim iMostrarPedidosERP As Boolean = True
        Dim iMostrarPedidosContraAbierto As Boolean = True
        Dim iMostrarPedidosExpress As Boolean = True
        For Each l As ListItem In chklstTipo.Items
            If l.Selected = False AndAlso l.Value = CPedido.TipoOrigenPedido.CatalogadoNegociado Then
                iMostrarPedidosEP = False
            ElseIf l.Selected = False AndAlso l.Value = CPedido.TipoOrigenPedido.CatalogadoLibres Then
                If Me.Acceso.gbPedidoLibre Then
                    iMostrarPedidosEPLIBRES = False
                Else
                    If chklstTipo.Items.FindByValue(CPedido.TipoOrigenPedido.CatalogadoNegociado).Selected Then
                        iMostrarPedidosEPLIBRES = True
                    Else
                        iMostrarPedidosEPLIBRES = False
                    End If
                End If
            ElseIf l.Selected = False AndAlso l.Value = CPedido.TipoOrigenPedido.Negociado Then
                iMostrarPedidosGS = False
            ElseIf l.Selected = False AndAlso l.Value = CPedido.TipoOrigenPedido.Directos Then
                iMostrarPedidosERP = False
            ElseIf l.Selected = False AndAlso l.Value = CPedido.TipoOrigenPedido.Express Then
                iMostrarPedidosExpress = False
            End If
            If l.Value = CPedido.TipoOrigenPedido.ContraAbierto Then
                iMostrarPedidosContraAbierto = l.Selected
            End If
        Next
        iMostrarPedidos(0) = iMostrarPedidosGS
        iMostrarPedidos(1) = iMostrarPedidosEP
        iMostrarPedidos(2) = iMostrarPedidosEPLIBRES
        iMostrarPedidos(3) = iMostrarPedidosERP
        iMostrarPedidos(4) = iMostrarPedidosContraAbierto
        iMostrarPedidos(5) = iMostrarPedidosExpress
        lis.Add(New Filtro(TipoFiltro.Tipo, iMostrarPedidos))

        If Not String.IsNullOrEmpty(fsnTvwCategorias.SelectedValue) Then _
                lis.Add(New Filtro(TipoFiltro.Categoria, fsnTvwCategorias.SelectedValue))

        'Gestor
        If Not String.IsNullOrEmpty(ddlGestor.SelectedValue) Then _
            lis.Add(New Filtro(TipoFiltro.Gestor, ddlGestor.SelectedValue))
        'Fecha Recepción
        lis.Add(New Filtro(TipoFiltro.FechaRecepcionDesde, CType(dteDesdeRecepcion.Value, Date)))
        lis.Add(New Filtro(TipoFiltro.FechaRecepcionHasta, CType(dteHastaRecepcion.Value, Date)))
        'Fecha entrega solicitada
        lis.Add(New Filtro(TipoFiltro.FechaEntregaSolicitadaDesde, CType(dteDesdeEntregaSolicit.Value, Date)))
        lis.Add(New Filtro(TipoFiltro.FechaEntregaSolicitadaHasta, CType(dteHastaEntregaSolicit.Value, Date)))
        'Fecha entrega indicada
        lis.Add(New Filtro(TipoFiltro.FechaEntregaIndicadaDesde, CType(dteDesdeEntregaIndic.Value, Date)))
        lis.Add(New Filtro(TipoFiltro.FechaEntregaIndicadaHasta, CType(dteHastaEntregaIndic.Value, Date)))
        'Albaran
        If Not String.IsNullOrEmpty(txtAlbaran.Text) And txtAlbaran.Visible Then _
            lis.Add(New Filtro(TipoFiltro.Albaran, txtAlbaran.Text))

        If Me.Acceso.gbAccesoFSSM AndAlso FSNUser.PermisoRecepcionarPedidosCCImputables Then
            'Pedidos del usuario
            lis.Add(New Filtro(TipoFiltro.PedidosUsuario, chkRecepcionPedidos.Items(0).Selected))
            'Pedidos del centro de coste/gestor
            lis.Add(New Filtro(TipoFiltro.PedidosCentroCoste, chkRecepcionPedidos.Items(1).Selected))
        Else
            'Pedidos del usuario
            lis.Add(New Filtro(TipoFiltro.PedidosUsuario, True))
        End If
        If Me.Acceso.gbAccesoFSSM Then
            'Centro Coste
            If Not String.IsNullOrEmpty(tbCtroCoste.Text) Then
                If Not String.IsNullOrEmpty(tbCtroCoste_Hidden.Value) Then
                    lis.Add(New Filtro(TipoFiltro.CentroCoste, tbCtroCoste_Hidden.Value, tbCtroCoste.Text))
                End If
            End If

            'Partidas presupuestarias
            For Each tabla As Control In phrPartidasPresBuscador.Controls
                If TypeOf tabla Is Table Then
                    For Each fila As Control In CType(tabla, Table).Rows
                        For Each celda As Control In CType(fila, TableRow).Cells
                            For Each tb As Control In CType(celda, TableCell).Controls
                                If TypeOf tb Is TextBox Then
                                    Dim tbPartida As TextBox = CType(tb, System.Web.UI.WebControls.TextBox)
                                    If Not String.IsNullOrEmpty(tbPartida.Text) Then
                                        Dim sPartidaHiddenID As String = tbPartida.ID.Replace("tbPPres_", "tbPPres_Hidden_")
                                        Dim tbPartidaHidden As HiddenField = CType(CType(celda, TableCell).FindControl(sPartidaHiddenID), System.Web.UI.WebControls.HiddenField)
                                        Dim sPRES0 As String = (tb.ID).Replace("tbPPres_", "")
                                        Dim sPartidaDen As String = tbPartida.Text.ToString
                                        If tbPartidaHidden IsNot Nothing AndAlso Not String.IsNullOrEmpty(tbPartidaHidden.Value) Then
                                            lis.Add(New Filtro( _
                                                            TipoFiltro.PartidaPresupuestaria, _
                                                            tbPartidaHidden.Value, _
                                                            sPRES0 & "@@@" & sPartidaDen _
                                                    ))
                                        End If
                                    End If
                                ElseIf TypeOf tb Is System.Web.UI.WebControls.Literal Then
                                    For Each tb2 As Control In CType(tb, Literal).Controls
                                        If TypeOf tb2 Is System.Web.UI.WebControls.TextBox Then
                                            Dim tbPartida As TextBox = CType(tb2, System.Web.UI.WebControls.TextBox)
                                            If Not String.IsNullOrEmpty(tbPartida.Text) Then
                                                Dim sPartidaHiddenID As String = tbPartida.ID.Replace("tbPPres_", "tbPPres_Hidden_")
                                                Dim tbPartidaHidden As HiddenField = CType(CType(tb, Label).FindControl(sPartidaHiddenID), System.Web.UI.WebControls.HiddenField)
                                                Dim sPRES0 As String = (tb2.ID).Replace("tbPPres_", "")
                                                Dim sPartidaDen As String = tbPartida.Text.ToString
                                                If tbPartidaHidden IsNot Nothing AndAlso Not String.IsNullOrEmpty(tbPartidaHidden.Value) Then
                                                    lis.Add(New Filtro( _
                                                                    TipoFiltro.PartidaPresupuestaria, _
                                                                    tbPartidaHidden.Value, _
                                                                    sPRES0 & "@@@" & sPartidaDen _
                                                            ))
                                                End If
                                            End If
                                        End If
                                    Next
                                End If
                            Next
                        Next
                    Next
                End If
            Next
        End If

        FiltrosAnyadidos = lis
    End Sub

    ''' Revisado por:blp. Fecha: 22/08/2012
    ''' <summary>
    ''' Buscar los resultados coincidentes con los filtros aplicados
    ''' </summary>
    ''' <param name="sender">btnBuscar</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde el evento Click. Máximo 1 seg.</remarks>
    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click

        EliminarCachePedidos(0, Me.Usuario.CodPersona, False, True, False)

        ActualizarFiltros()
        Me._PaginadorTop.PageNumber = 1
        _PaginadoEntregas = False
        CargarwhdgEntregas()
        InicializarPaginador()

        RellenarLabelFiltros()
        pnlGrid.Update()
    End Sub

#End Region

#Region "Ordenación"

    Private Property CampoOrden() As String
        Get
            If String.IsNullOrEmpty(ViewState.Item("CampoOrden")) Then
                Return "FECHA"
            Else
                Return ViewState.Item("CampoOrden")
            End If
        End Get
        Set(ByVal value As String)
            ViewState.Item("CampoOrden") = value
        End Set
    End Property

    Private Property SentidoOrdenacion() As String
        Get
            If String.IsNullOrEmpty(ViewState.Item("SentidoOrdenacion")) Then
                Return "DESC"
            Else
                Return ViewState.Item("SentidoOrdenacion")
            End If
        End Get
        Set(ByVal value As String)
            ViewState.Item("SentidoOrdenacion") = value
        End Set
    End Property

    Public Sub Paginador_OnCambioOrden(ByVal Campo As String, ByVal Sentido As String)
        Dim bActualizar As Boolean = False
        If Campo <> CampoOrden Then
            CampoOrden = Campo
            Dim cookie As HttpCookie = Request.Cookies("RECEPCION_CRIT_ORD")
            If cookie Is Nothing Then
                cookie = New HttpCookie("RECEPCION_CRIT_ORD", CampoOrden)
            Else
                cookie.Value = CampoOrden
            End If
            cookie.Expires = DateTime.Now.AddDays(30)
            Response.AppendCookie(cookie)
            bActualizar = True
        End If
        If Sentido <> SentidoOrdenacion Then
            SentidoOrdenacion = Sentido
            Dim cookie As HttpCookie = Request.Cookies("RECEPCION_CRIT_DIREC")
            If cookie Is Nothing Then
                cookie = New HttpCookie("RECEPCION_CRIT_DIREC", SentidoOrdenacion)
            Else
                cookie.Value = SentidoOrdenacion
            End If
            cookie.Expires = DateTime.Now.AddDays(30)
            Response.AppendCookie(cookie)
            bActualizar = True
        End If
        If bActualizar Then
            Dim pds As New PagedDataSource
            pds.AllowPaging = True
            pds.DataSource = DevolverPedidosConFiltro(CampoOrden, SentidoOrdenacion).DefaultView
            If ViewState("PageSize") Is Nothing Then
                pds.PageSize = 10
            Else
                pds.PageSize = CType(ViewState("PageSize"), Integer)
            End If
            pds.CurrentPageIndex = 0

            pnlGrid.Update()
        End If
    End Sub

#End Region

#Region "Registrar Recepción"


    ''' Revisado por: blp. Fecha: 17/02/2012
    ''' <summary>
    ''' Método que reabre la orden
    ''' </summary>
    ''' <param name="Orden">ID de la orden a reabrir</param>
    ''' <remarks>Llamada desde Recepcion.aspx.vb. Tiempo máx inferior 1 seg</remarks>
    Private Sub ReabrirOrden(ByVal Orden As Integer)
        Dim oOrden As COrden = FSNServer.Get_Object(GetType(FSNServer.COrden))
        oOrden.ID = Orden
        'Reabrimos la orden y recuperamos su nuevo estado
        Dim iEstadoNuevo As Integer = oOrden.ReabrirOrden(Me.Usuario.CodPersona)
        EliminarCachePedidos(oOrden.ID)
        ActualizarDsetpedidos()

        pnlGrid.Update()
    End Sub

#End Region

    ''' <summary>
    ''' Determina si se muestra el control en función de la propiedad AuthorizeWebPart y los permisos del usuario
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">WebPartAuthorizationEventArgs que contiene los datos del evento.</param>
    ''' <remarks>Se produce cuando se llama al método IsAuthorized para determinar si se puede agregar un control WebPart o un control de servidor a una página.</remarks>
    Private Sub WebPartManager1_AuthorizeWebPart(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.WebParts.WebPartAuthorizationEventArgs) Handles WebPartManager1.AuthorizeWebPart
        e.IsAuthorized = AutorizacionWebparts(e.AuthorizationFilter)
    End Sub


#Region "Confirmar Cantidad Recepcionada"

    ''' <summary>
    ''' Comprueba al recepcionar si, para cada línea del pedido, la cantidad indicada como recepcionada es mayor que la cantidad máxima
    ''' solicitada. Si hay alguna que lo es, devuelve valor true, si no, false
    ''' </summary>
    ''' <param name="Datos">Datos de las lineas de pedido</param>
    ''' <returns>Booleano que indica si hay algúna cantidad recepcionada superior a la cantidad solicitada </returns>
    ''' <remarks></remarks>
    Private Function comprobarRecepcionMayorMaximo(ByVal Datos As DataListItem) As Boolean
        Dim hayRecepcionMayorMaximo As Boolean = False
        If Not Datos.FindControl("dlLineasPedido") Is Nothing Then
            With Datos.FindControl("dlLineasPedido")
                For Each linea As DataListItem In .Controls
                    'la primera linea del datalist es la cabecera
                    If Not CType(linea.FindControl("hidTipoRecepcion"), HiddenField) Is Nothing Then
                        Dim CantidadSolicitada As Double = 0
                        Dim CantidadRecepcionada As Double = 0
                        Dim numDecimales As Integer = 0
                        Dim wNumericEdit As Infragistics.Web.UI.EditorControls.WebNumericEditor = CType(linea.FindControl("txtCantidad"), Infragistics.Web.UI.EditorControls.WebNumericEditor)
                        'Vamos a recuperar el NumberFormatinfo del usuario y el número de decimales de la unidad (o bien el formato de la unidad de la línea, que debería ser lo mismo)
                        'para configurar correctamente el valor que recuperamos del control litPdteRecep
                        If wNumericEdit IsNot Nothing AndAlso wNumericEdit.Attributes("numeroDecimales") IsNot Nothing Then
                            numDecimales = wNumericEdit.Attributes("numeroDecimales")
                        Else
                            If CType(linea.FindControl("FSNlnkUniCant"), FSNWebControls.FSNLinkTooltip) IsNot Nothing Then
                                Dim unidad As String = CType(linea.FindControl("FSNlnkUniCant"), FSNWebControls.FSNLinkTooltip).Text
                                numDecimales = TodasLasUnidades.Item(Trim(unidad)).NumeroDeDecimales
                            End If
                        End If
                        Dim formatoNumerico As System.Globalization.NumberFormatInfo
                        formatoNumerico = Me.Usuario.NumberFormat.Clone
                        formatoNumerico.NumberDecimalDigits = numDecimales

						If Not linea.FindControl("litPdteRecep") Is Nothing _
								AndAlso Not String.IsNullOrEmpty(CType(linea.FindControl("litPdteRecep"), HiddenField).Value) Then
							CantidadSolicitada = Math.Abs(Double.Parse(CType(linea.FindControl("litPdteRecep"), HiddenField).Value, formatoNumerico))
						End If
						If Not linea.FindControl("txtCantidad") Is Nothing _
                            AndAlso Not String.IsNullOrEmpty(CType(linea.FindControl("txtCantidad"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Value) Then
                            CantidadRecepcionada = Math.Abs(CType(linea.FindControl("txtCantidad"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Value)
                        End If

                        If CantidadRecepcionada > CantidadSolicitada Then
                            hayRecepcionMayorMaximo = True
							CType(linea.FindControl("txtCantidad"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Value = Double.Parse(CType(linea.FindControl("litPdteRecep"), HiddenField).Value, formatoNumerico)
						End If
                    End If
                Next
            End With
        End If
        Return hayRecepcionMayorMaximo
    End Function



#End Region

    ''' <summary>
    ''' Método que se lanza justo antes de generar el control en pantalla
    ''' </summary>
    ''' <param name="sender">control que lo lanza</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Evento que se lanza en el momento anterior a dibujar el control en pantalla. Tiempo máximo inferior a 0,1 seg</remarks>
    Protected Sub dlLineasPedido_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlLineasPedido.PreRender
        Dim dlLineasPedido As DataList = CType(sender, DataList)
        For Each Linea As DataListItem In dlLineasPedido.Items
            Dim wnumCant As Infragistics.Web.UI.EditorControls.WebNumericEditor = CType(Linea.FindControl("txtCantidad"), Infragistics.Web.UI.EditorControls.WebNumericEditor)
            'Insertamos un script que controla el evento paste en el control
            Me.insertarScriptPaste(wnumCant.ClientID)
        Next
    End Sub

    ''' <summary>
    ''' El proveedor ERP es un campo de las ordenes que sólo será visible si hay Integracion de pedidos y el campo TRASLADO_APROV_ERP de la tabla pargen_interno (configuraciones de la aplicación) es 1 (true)
    ''' </summary>
    Private ReadOnly Property MostrarProveedorERP() As Boolean
        Get
            If Acceso.gbTrasladoAProvERP And HayIntegracionPedidosEntradaOEntradaSalida Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    ''' <summary>
    ''' El Numero de pedido de ERP (o Ref. en factura) es un campo de las ordenes que sólo será visible si hay Integracion de pedidos y el campo OBLCODPEDIDO de la tabla pargen_interno (configuraciones de la aplicación) es 1 (true)
    ''' </summary>
    Private ReadOnly Property MostrarNumeroPedidoERP() As Boolean
        Get
            If Acceso.gbOblCodPedido And HayIntegracionPedidosEntradaOEntradaSalida Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    ''' Revisado por: blp. Fecha: 24/02/2012
    ''' <summary>
    ''' Selecciona los checks de Recepcion con las opciones del usuario y los almacena en variables de session para cuando accede a BBDD para obtener el dataset de pedidos
    ''' </summary>
    ''' <param name="Limpiar">
    ''' True: hay que volver a dejar los checks como en la carga inicial de la página
    ''' False: los checks dependerán de si hay postback o no.
    ''' </param>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0,2seg.</remarks>
    Private Sub SeleccionChecksRecepcion(ByVal Limpiar As Boolean)
        'Cargar los checks de seguimiento de pedidos por CC
        fldsetRecepPedidosCC.Visible = False
        If Me.Acceso.gbAccesoFSSM Then
            If FSNUser.PermisoRecepcionarPedidosCCImputables Then
                fldsetRecepPedidosCC.Visible = True
                If Limpiar Or (Not IsPostBack) Then
                    chkRecepcionPedidos.Items(0).Selected = FSNUser.RecepcionVerPedidosUsuario
                    chkRecepcionPedidos.Items(1).Selected = FSNUser.RecepcionVerPedidosCCImputables
                End If
                chkRecepcionPedidos.Items(0).Attributes.Add("onClick", "ValidacionChecks(0)")
                chkRecepcionPedidos.Items(1).Attributes.Add("onClick", "ValidacionChecks(1)")
            End If
        Else
            chkRecepcionPedidos.Items(0).Selected = True
            chkRecepcionPedidos.Items(1).Selected = False
        End If
    End Sub

    ''' Revisado por: blp. Fecha: 20/08/2012
    ''' <summary>
    ''' Método que limpia los criterios de búsqueda del panel de búsqueda.
    ''' </summary>
    ''' <param name="sender">Control btnLimpiar</param>
    ''' <param name="e">parámetros del evento</param>
    ''' <remarks>llamada desde el control, al hacer click. Tiempo máximo inferios a 0,1 seg.</remarks>
    Private Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        'Recorremos los controles del panel y los vamos limpiando según el tipo
        VaciarControlesRecursivo(pnlBusqueda)
        'Quitamos filtro de categoría
        If Not fsnTvwCategorias.SelectedNode Is Nothing Then
            Dim ev As New System.EventArgs
            lnkQuitarCategoria_Click(fsnTvwCategorias, ev)
        End If
        'Inicializamos filtro checkboxlists
        inicializarCheckboxLists()
        'nos aseguramos que sigue sin verse el lblBusqueda
        divListaFiltros.Style("display") = "none"
        'Actualizar
        pnlBuscador.Update()
        updpnlCategorias.Update()
    End Sub

    ''' Revisado por: blp. Fecha: 20/08/2012
    ''' <summary>
    ''' Método para inicializar la selección de los checkboxList del panel buscador
    ''' </summary>
    ''' <remarks>Llamada desde InicializarCOntroles y btnLimpiar_Click. Tiempo inferior a 0,1 seg</remarks>
    Private Sub inicializarCheckboxLists()
        ' Checks estados
        chklstEstado.Items.Clear()
        chklstEstado.Items.AddRange(Estados.ToArray)
        For Each l As ListItem In chklstEstado.Items
            If l.Value <> TipoEstadoOrdenEntrega.RecibidoYCerrado Then l.Selected = True
            If l.Value = TipoEstadoOrdenEntrega.EnRecepcion Then
                l.Attributes.Add("onclick", "MostrarOcultarCheck()")
            End If
        Next
        'Check para mostrar las solo pendientes
        chkSoloLineasPendientes.Checked = True
        ' Checks tipos de pedido
        chklstTipo.Items.Clear()
        chklstTipo.Items.AddRange(ClasesDePedido.ToArray)
        If Not Me.Acceso.gbPedidoLibre Then
            chklstTipo.Items.FindByValue(CPedido.TipoOrigenPedido.CatalogadoLibres).Attributes.Add("style", "display:none;")
        End If
        For Each l As ListItem In chklstTipo.Items
            l.Selected = True
        Next

        SeleccionChecksRecepcion(True)
    End Sub

    ''' Revisado por: blp. Fecha: 20/08/2012
    ''' <summary>
    ''' Método que recorre de manera recursiva el control recibido y vacía las selecciones de todos los controles
    ''' </summary>
    ''' <param name="oControlBase">Control a recorrer</param>
    ''' <remarks>Llamada desde btnLimpiar_Click</remarks>
    Private Sub VaciarControlesRecursivo(ByVal oControlBase As Object)
        For Each oControl In oControlBase.Controls
            Select Case oControl.GetType().ToString
                Case GetType(DropDownList).ToString
                    If CType(oControl, DropDownList).SelectedItem IsNot Nothing Then _
                        CType(oControl, DropDownList).SelectedItem.Selected = False
                Case GetType(TextBox).ToString
                    CType(oControl, TextBox).Text = String.Empty
                Case GetType(Infragistics.Web.UI.EditorControls.WebDatePicker).ToString
                    CType(oControl, Infragistics.Web.UI.EditorControls.WebDatePicker).Value = Nothing
                Case GetType(Infragistics.Web.UI.EditorControls.WebNumericEditor).ToString
                    CType(oControl, Infragistics.Web.UI.EditorControls.WebNumericEditor).Value = 1
                Case GetType(CheckBoxList).ToString
                    'Los hacemos aparte, llamando a una función específica en btnLimpiar_Click
                Case GetType(CheckBox).ToString
                    CType(oControl, CheckBox).Checked = False
                Case Else
                    If oControl.Controls IsNot Nothing AndAlso oControl.Controls.Count > 0 Then
                        VaciarControlesRecursivo(oControl)
                    End If
            End Select
        Next
    End Sub

    ''' <summary>
    ''' Propiedad que devuelve una lista de listitems con todos los estados de pedido que hay
    ''' </summary>
    Private ReadOnly Property Estados() As List(Of ListItem)
        Get
            Dim _listaDeEstados As New List(Of ListItem)
            RellenarTextosCheckboxListsEstados()
            _listaDeEstados.Add(New ListItem(_sEstados(1), EstadoSinRecepcion)) 'CPedido.Estado.EmitidoAlProveedor
            _listaDeEstados.Add(New ListItem(_sEstados(3), TipoEstadoOrdenEntrega.RecibidoYCerrado)) 'CPedido.Estado.EnCamino
            _listaDeEstados.Add(New ListItem(_sEstados(2), TipoEstadoOrdenEntrega.EnRecepcion)) 'CPedido.Estado.AceptadoPorElProveedor
            Return _listaDeEstados
        End Get
    End Property

    ''' <summary>
    ''' Unión de los tres estados de un pedido previos a la primera recepción
    ''' </summary>
    Private ReadOnly Property EstadoSinRecepcion As Integer
        Get
            Return TipoEstadoOrdenEntrega.EmitidoAlProveedor Or TipoEstadoOrdenEntrega.AceptadoPorProveedor Or TipoEstadoOrdenEntrega.EnCamino
        End Get
    End Property

    ''' <summary>
    ''' Propiedad que devuelve una lista de listitems con las clases de pedido que hay
    ''' </summary>
    Private ReadOnly Property ClasesDePedido() As List(Of ListItem)
        Get
            Dim _listaDeClasesDePedido As New List(Of ListItem)
            RellenarTextosCheckboxListsClasesDePedido()
            _listaDeClasesDePedido.Add(New ListItem(_sTipos(0), CPedido.TipoOrigenPedido.CatalogadoNegociado)) 'EP no libre
            _listaDeClasesDePedido.Add(New ListItem(_sTipos(1), CPedido.TipoOrigenPedido.CatalogadoLibres)) 'EP LIBRE
            If Acceso.gbUsarPedidosAbiertos Then
                _listaDeClasesDePedido.Add(New ListItem(_sTipos(4), CPedido.TipoOrigenPedido.ContraAbierto))
            End If
            _listaDeClasesDePedido.Add(New ListItem(_sTipos(2), CPedido.TipoOrigenPedido.Negociado)) 'GS
            If HayIntegracionPedidosEntradaOEntradaSalida Then
                _listaDeClasesDePedido.Add(New ListItem(_sTipos(3), CPedido.TipoOrigenPedido.Directos)) 'ERP
            End If
            _listaDeClasesDePedido.Add(New ListItem(_sTipos(5), CPedido.TipoOrigenPedido.Express)) 'Express
            Return _listaDeClasesDePedido
        End Get
    End Property

    ''' Revisado por: blp. Fecha: 22/08/2012
    ''' <summary>
    ''' Rellenamos los textos de los Checkbox OtrosFiltros y Estados
    ''' </summary>
    ''' <remarks>Llamada desde RellenarTextosCheckboxLists y ClasesdePedido(). Máx. 0,1 seg.</remarks>
    Private Sub RellenarTextosCheckboxListsClasesDePedido()
        If _sTipos(0) = Nothing Then
            _sTipos(0) = Textos(128) 'Pedidos de material catalogado
            _sTipos(1) = Textos(255) 'Pedidos de catálogo libres
            _sTipos(2) = Textos(129) 'Pedidos negociados
            If HayIntegracionPedidosEntradaOEntradaSalida Then
                _sTipos(3) = Textos(131) 'Pedidos directos
            End If
            _sTipos(4) = Textos(274)
            _sTipos(5) = Textos(275) 'Pedidos express
        End If
    End Sub

    ''' Revisado por:blp. Fecha: 22/08/2012
    ''' <summary>
    ''' Rellenamos los textos de los Checkbox OtrosFiltros y Estados
    ''' </summary>
    ''' <remarks>Llamada desde RellenarTextosCheckBoxList y Estados(). Máx. 0,1 seg.</remarks>
    Private Sub RellenarTextosCheckboxListsEstados()
        'Comprobamos el primer valor. Si es Nothing, rellenamos todos
        If _sEstados(0) = Nothing Then
            _sEstados(0) = Textos(15)
            _sEstados(1) = Textos(16)
            _sEstados(2) = Textos(17)
            _sEstados(3) = Textos(18)
        End If
    End Sub

    ''' Revisado por: blp. Fecha: 20/08/2012
    ''' <summary>
    ''' Método que recorre todos los filtros existentes y prepara el texto que aparecerá 
    ''' en pantalla con los filtros seleccionados cuando el panel de búsqueda está oculto.
    ''' No se comprueba que los filtros están en las opciones de filtro (ni que hay permisos para ver fras y pagos) porque 
    ''' en la llamada desde gestionCookieFiltrosBusqueda ya se ha comprobado en SeleccionarFiltrosEnPanelBusqueda (método que se llama anteriormente)
    ''' y en la llamada desde btnBuscar_Click sería muy extraño que un dato seleccionado en el panel desapareciese entre el momento que se selecciona y que se pulsa al botón de buscar
    ''' </summary>
    ''' <remarks>Llamada desde gestionCookieFiltrosBusqueda y btnBuscar_Click. Máx 0,1 seg.</remarks>
    Private Sub RellenarLabelFiltros()
        Dim sListaFiltros As String = String.Empty
        Dim sListaFiltrosEstado As String = String.Empty
        Dim sListaFiltrosClasesDePedido As String = String.Empty
        Dim sListaFiltrosPartidasPres As String = String.Empty
        Dim lista As List(Of ListItem)

        For Each oFiltro As Filtro In FiltrosAnyadidos
            Dim sValor As String = oFiltro.Valor
            Select Case oFiltro.tipo
                Case TipoFiltro.Anio
                    sListaFiltros += "<b>" & Textos(3) & "</b> " & sValor & "; "
                Case TipoFiltro.Cesta
                    sListaFiltros += "<b>" & Textos(4) & ":</b> " & sValor & "; "
                Case TipoFiltro.NumPedido
                    sListaFiltros += "<b>" & Textos(5) & ":</b> " & sValor & "; "
                Case TipoFiltro.NumPedidoProve
                    sListaFiltros += "<b>" & Textos(51) & ":</b> " & sValor & "; "
                Case TipoFiltro.Albaran
                    sListaFiltros += "<b>" & Textos(152) & ":</b> " & sValor & "; "
                Case TipoFiltro.NumPedidoERP
                    Dim oCParametros As FSNServer.CParametros = FSNServer.Get_Object(GetType(FSNServer.CParametros))
                    sListaFiltros += "<b>" & oCParametros.CargarLiteralParametros(TiposDeDatos.LiteralesParametros.PedidoERP, Usuario.Idioma) & ":</b> " & sValor & "; "
                Case TipoFiltro.Articulo
                    sListaFiltros += "<b>" & Textos(148) & ":</b> " & sValor & "; " 'Artículo:
                Case TipoFiltro.FechaPedidoDesde
                    If oFiltro.ValorFecha <> Date.MinValue Then
                        sListaFiltros += "<b>" & Textos(87) & "</b> " 'Fecha del pedido.
                        sListaFiltros += Textos(158) & ": " 'Desde:
                        sListaFiltros += oFiltro.ValorFecha & "; "
                    End If
                Case TipoFiltro.FechaPedidoHasta
                    If oFiltro.ValorFecha <> Date.MinValue Then
                        If sListaFiltros.IndexOf(Textos(87)) < 0 Then _
                            sListaFiltros += "<b>" & Textos(87) & "</b> " 'Fecha del pedido.
                        sListaFiltros += Textos(159) & ": " 'Hasta:
                        sListaFiltros += oFiltro.ValorFecha & "; "
                    End If
                Case TipoFiltro.FechaRecepcionDesde
                    If oFiltro.ValorFecha <> Date.MinValue Then
                        sListaFiltros += "<b>" & Textos(149) & ".</b> " 'Fecha de recepción.
                        sListaFiltros += Textos(158) & ": " 'Desde:
                        sListaFiltros += oFiltro.ValorFecha & "; "
                    End If
                Case TipoFiltro.FechaRecepcionHasta
                    If oFiltro.ValorFecha <> Date.MinValue Then
                        If sListaFiltros.IndexOf(Textos(149)) < 0 Then _
                            sListaFiltros += "<b>" & Textos(149) & ".</b> " 'Fecha de recepción.
                        sListaFiltros += Textos(159) & ": " 'Hasta:
                        sListaFiltros += oFiltro.ValorFecha & "; "
                    End If
                Case TipoFiltro.FechaEntregaSolicitadaDesde
                    If oFiltro.ValorFecha <> Date.MinValue Then
                        sListaFiltros += "<b>" & Textos(150) & ".</b> " 'Fecha entrega solicita.
                        sListaFiltros += Textos(158) & ": " 'Desde:
                        sListaFiltros += oFiltro.ValorFecha & "; "
                    End If
                Case TipoFiltro.FechaEntregaSolicitadaHasta
                    If oFiltro.ValorFecha <> Date.MinValue Then
                        If sListaFiltros.IndexOf(Textos(150)) < 0 Then _
                            sListaFiltros += "<b>" & Textos(150) & "</b> " 'Fecha entrega solicita.
                        sListaFiltros += Textos(159) & ": " 'Hasta:
                        sListaFiltros += oFiltro.ValorFecha & "; "
                    End If
                Case TipoFiltro.FechaEntregaIndicadaDesde
                    If oFiltro.ValorFecha <> Date.MinValue Then
                        sListaFiltros += "<b>" & Textos(151) & ".</b> " 'Fecha entrega indicada.
                        sListaFiltros += Textos(158) & ": " 'Desde:
                        sListaFiltros += oFiltro.ValorFecha & "; "
                    End If
                Case TipoFiltro.FechaPedidoHasta
                    If oFiltro.ValorFecha <> Date.MinValue Then
                        If sListaFiltros.IndexOf(Textos(151)) < 0 Then _
                            sListaFiltros += "<b>" & Textos(151) & ".</b> " 'Fecha entrega indicada.
                        sListaFiltros += Textos(159) & ": " 'Hasta:
                        sListaFiltros += oFiltro.ValorFecha & "; "
                    End If
                Case TipoFiltro.Empresa
                    sListaFiltros += "<b>" & Textos(84) & ":</b> " & ddlEmpresa.SelectedItem.Text & "; "
                Case TipoFiltro.Receptor
                    sListaFiltros += "<b>" & Textos(258) & ":</b> " & ddlReceptor.SelectedItem.Text & "; "
                Case TipoFiltro.Proveedor
                    sListaFiltros += "<b>" & Textos(10) & ":</b> " & txtProveedor.Text & "; "
                Case TipoFiltro.ProveedorERP
                    sListaFiltros += "<b>" & Textos(85) & ":</b> " & sValor & "; "
                Case TipoFiltro.CentroCoste
                    sListaFiltros += "<b>" & Textos(116) & ":</b> " & tbCtroCoste.Text & "; "
                Case TipoFiltro.PartidaPresupuestaria
                    Dim arSeparadores As String()
                    ReDim Preserve arSeparadores(0)
                    arSeparadores(0) = "@@@"
                    Dim arPartidaPres As String() = oFiltro.Info.Split(arSeparadores, StringSplitOptions.None)
                    Dim sPartidaPres0 As String = "tbPPres_" & arPartidaPres(0)
                    Dim sPartidaPresDen = arPartidaPres(1)
                    If phrPartidasPresBuscador IsNot Nothing _
                    AndAlso phrPartidasPresBuscador.FindControl("tbDdlPP") IsNot Nothing _
                    AndAlso phrPartidasPresBuscador.FindControl("tbDdlPP").FindControl(sPartidaPres0) IsNot Nothing Then
                        Dim tbPartidaPres As TextBox = TryCast(phrPartidasPresBuscador.FindControl("tbDdlPP").FindControl(sPartidaPres0), TextBox)
                        Dim sPartidaHiddenID As String = sPartidaPres0.Replace("tbPPres_", "tbPPres_Hidden_")
                        Dim tbPartidaPresHidden As HiddenField = TryCast(phrPartidasPresBuscador.FindControl("tbDdlPP").FindControl(sPartidaHiddenID), HiddenField)
                        'En teoría este if es innecesario porque todas las comprobaciones se hacen en la función
                        'SeleccionarFiltrosEnPanelBusqueda(). Por si acaso, lo dejamos.
                        If tbPartidaPresHidden.Value = sValor OrElse tbPartidaPres.Text = sValor Then
                            Dim sPres0Den As String = tbPartidaPres.Text
                            If sListaFiltrosPartidasPres = String.Empty Then
                                sListaFiltrosPartidasPres += "<b>" & Textos(212) & ":</b> " & sPartidaPresDen
                            Else
                                sListaFiltrosPartidasPres += ", " & sPartidaPresDen
                            End If
                        End If
                    End If
                Case TipoFiltro.TipoPedido 'Esto es TIPO DE PEDIDO
                    sListaFiltros += "<b>" & Textos(117) & ":</b> " & ddlTipoPedido.SelectedItem.Text & "; "
                Case TipoFiltro.Estado
                    'Si el filtro es igual a todos los estados (o sea, si todos están seleccionados), no se muestra en el label
                    If Not TodosEstadosSeleccionados Then
                        lista = Estados()
                        For Each oEstado As ListItem In chklstEstado.Items
                            Dim _oEstado As ListItem = oEstado
                            If oEstado.Selected Then
                                If sListaFiltrosEstado = String.Empty Then
                                    sListaFiltrosEstado += "<b>" & Textos(191) & ":</b> " & lista.Find(Function(estado As ListItem) estado.Value = _oEstado.Value).Text
                                Else
                                    sListaFiltrosEstado += ", " & lista.Find(Function(estado As ListItem) estado.Value = _oEstado.Value).Text
                                End If
                            End If
                        Next
                    End If
                Case TipoFiltro.Tipo 'Esto es CLASE DE PEDIDO
                    Dim ponComa As Boolean = False
                    Dim mostrarTODOS As Boolean = True
                    For Each mostrarUno As Boolean In oFiltro.Valores
                        If Not mostrarUno Then
                            mostrarTODOS = False
                            Exit For
                        End If
                    Next
                    'El label con las clases de pedido sólo se muestra si no están todos los checks seleccionados
                    If Not mostrarTODOS Then
                        lista = ClasesDePedido()
                        For i As Integer = 0 To oFiltro.Valores.Length - 1
                            If oFiltro.Valores(i) Then
                                If sListaFiltrosClasesDePedido = String.Empty Then sListaFiltrosClasesDePedido = "<b>" & Textos(155) & ":</b> "
                                Select Case i
                                    Case 0 'GS
                                        sListaFiltrosClasesDePedido += IIf(ponComa, ", ", String.Empty) & _sTipos(2)
                                        ponComa = True
                                    Case 1 'EP
                                        sListaFiltrosClasesDePedido += IIf(ponComa, ", ", String.Empty) & _sTipos(0)
                                        ponComa = True
                                    Case 2 'EP LIBRES
                                        sListaFiltrosClasesDePedido += IIf(ponComa, ", ", String.Empty) & _sTipos(1)
                                        ponComa = True
                                    Case 3 'ERP
                                        If HayIntegracionPedidosEntradaOEntradaSalida Then
                                            sListaFiltrosClasesDePedido += IIf(ponComa, ", ", String.Empty) & _sTipos(3)
                                            ponComa = True
                                        End If
                                    Case 4 'Pedidos contraabiertos
                                        If Acceso.gbUsarPedidosAbiertos Then
                                            sListaFiltrosClasesDePedido += IIf(ponComa, ", ", String.Empty) & _sTipos(4)
                                            ponComa = True
                                        End If
                                    Case 5 'Pedidos Express
                                        sListaFiltrosClasesDePedido += IIf(ponComa, ",", String.Empty) & _sTipos(5)
                                End Select
                            End If
                        Next
                        If sListaFiltrosClasesDePedido.Length > 0 Then sListaFiltrosClasesDePedido += "; "
                    End If
                Case TipoFiltro.Categoria
                    'Este if no debería ser necesario porque RellenarLabelFiltros() se usa después de 
                    'SeleccionarFiltrosEnPanelBusqueda.
                    If fsnTvwCategorias IsNot Nothing AndAlso fsnTvwCategorias.SelectedNode IsNot Nothing Then
                        sListaFiltros += "<b>" & Textos(136) & ":</b> " & fsnTvwCategorias.SelectedNode.Text & "; "
                    End If
                Case TipoFiltro.Gestor
                    sListaFiltros += "<b>" & Textos(241) & ":</b> " & ddlGestor.SelectedItem.Text & "; "
                Case TipoFiltro.PedidosUsuario
                    sListaFiltros += "<b>" & Textos(156) & "</b>" & "; " 'Mis pedidos
                Case TipoFiltro.PedidosCentroCoste
                    sListaFiltros += "<b>" & Textos(157) & "</b>" & "; " 'Pedidos de otros usuarios
            End Select
        Next
        If sListaFiltrosEstado.Length > 0 Then sListaFiltrosEstado += "; "
        If sListaFiltrosPartidasPres.Length > 0 Then sListaFiltrosPartidasPres += "; "
        sListaFiltros += sListaFiltrosPartidasPres & _
                        sListaFiltrosEstado & _
                        sListaFiltrosClasesDePedido

        If sListaFiltros.Length > 0 Then
            lblListaFiltros.Text = sListaFiltros
            divListaFiltros.Style("display") = "block"
        Else
            lblListaFiltros.Text = String.Empty
            divListaFiltros.Style("display") = "none"
        End If
        upListaFiltros.Update()
    End Sub


#Region "PartidasPresupuestarias"

    ''' Revisado por: blp. Fecha: 21/08/2012
    ''' <summary>
    ''' Función que nos devuelve un Datatable con las configuraciones de cada partida presupuestaria
    ''' </summary>
    ''' <param name="dsInfo">
    ''' Podemos pasarle el dataset desde el que recupera los datos o dejar que los tome del DsetPedidos
    ''' </param>
    ''' <returns>Datatable con las configuraciones de cada partida presupuestaria</returns>
    ''' <remarks>Llamado desde CargarCombosPartidas
    ''' Tiempo máximo: 0 sec</remarks>
    Private Function CargarPartidasPresupuestarias(ByVal dsInfo As DataSet) As DataTable
        Dim query = From Datos In dsInfo.Tables("PARTIDASPRES0").AsEnumerable() _
        Where (Not IsDBNull(Datos.Item("PRES5"))) _
            Order By Datos.Item("PARTIDA_DEN") _
            Select Datos!PRES5, Datos!NIVEL, Datos!PARTIDA_DEN _
            Distinct

        Dim PartidasPres = New DataTable("PartidasPres")
        PartidasPres.Columns.Add("PRES0", GetType(String))
        PartidasPres.Columns.Add("NIVEL", GetType(Integer))
        PartidasPres.Columns.Add("PARTIDA_DEN", GetType(String))
        For Each partida In query
            PartidasPres.Rows.Add(New Object() {partida.PRES5, partida.NIVEL, partida.PARTIDA_DEN})
        Next
        Return PartidasPres
    End Function

    ''' Revisado por: blp. Fecha:21/08/2012
    ''' <summary>
    ''' Procedimiento que crea tantos combos en el buscador como partidas presupuestarias se usen
    ''' Y rellena cada uno de los combos con las partidas que existan en las líneas de pedido de los pedidos emitidos por el usuario
    ''' </summary>
    ''' <remarks>Llamado desde Page_Load
    ''' Tiempo máximo: 0 sec</remarks>
    Private Sub CargarCombosPartidas()
        Dim dtPartidasPres As DataTable
        Dim dsInfo As DataSet = getDropDownsInfo(FSNUser.CodPersona, FSNUser.Idioma)

        '1. coger las partidas
        dtPartidasPres = CargarPartidasPresupuestarias(dsInfo)
        '2. Añadimos al scriptmanager de la página el js que lanzará la carga asíncrona de las partidas desde cliente
        CargarScriptBuscadoresJQuery()
        'CargarScriptPartidasCentros(dtPartidasPres, listadoPartidas.ClientID, listadoInputsFiltro.ClientID, listadoCentros.ClientID, FiltroCentros.ClientID, Nothing, TiposDeDatos.Aplicaciones.EP)
        Dim oScriptPartidas As HtmlGenericControl
        If dtPartidasPres.Rows.Count > 0 Then
            oScriptPartidas = CargarScriptBusquedaJQuery(TiposDeDatos.ControlesBusquedaJQuery.PartidaPres, listadoPartidas.ClientID, listadoInputsFiltro.ClientID, TiposDeDatos.Aplicaciones.EP)
        End If
        'Cuando se devuelve un script "script" hay que insertar en la página el script q se recibe
        If oScriptPartidas IsNot Nothing AndAlso oScriptPartidas.TagName = "script" Then
            RegistrarScript(oScriptPartidas, "ScriptPartidas")
        End If
        Dim oScriptCentros As HtmlGenericControl
        oScriptCentros = CargarScriptBusquedaJQuery(TiposDeDatos.ControlesBusquedaJQuery.CentroCoste, listadoCentros.ClientID, FiltroCentros.ClientID, TiposDeDatos.Aplicaciones.EP)
        'Cuando se devuelve un script "script" hay que insertar en la página el script q se recibe
        If oScriptCentros IsNot Nothing AndAlso oScriptCentros.TagName = "script" Then
            RegistrarScript(oScriptCentros, "ScriptCentros")
        End If

        '3. Para cada partida, crear un label con el texto del punto 1, un textbox readonly y un botón para abrir el panel
        If dtPartidasPres.Rows.Count > 0 Then
            Dim TablaAsp As New Table
            Dim FilaAsp As New TableRow
            Dim Celda1Asp As New TableCell
            Dim Celda2Asp As New TableCell
            For Each PartidaPres As DataRow In dtPartidasPres.Rows
                If phrPartidasPresBuscador.FindControl("tbDdlPP") Is Nothing _
                OrElse phrPartidasPresBuscador.FindControl("tbDdlPP").FindControl("trPPres_" & PartidaPres("PRES0")) Is Nothing Then
                    'Inserta una serie de controles para cada una de las partidas dentro del control PlaceHolder->phrPartidasPresBuscador
                    Celda1Asp = New TableCell
                    Celda2Asp = New TableCell
                    FilaAsp = New TableRow
                    Celda1Asp.Style.Add("padding-bottom", "6px")
                    Celda1Asp.Width = Unit.Pixel(95)
                    Celda2Asp.Width = Unit.Pixel(375)
                    Celda1Asp.HorizontalAlign = HorizontalAlign.Left
                    Celda1Asp.Style.Add("text-align", "left")
                    Celda2Asp.HorizontalAlign = HorizontalAlign.Right

                    Dim tbPartida As New TextBox
                    Dim tbPartidaHidden As New HiddenField
                    Dim litPartida As New Literal()
                    Dim aPartida As New HtmlControls.HtmlAnchor
                    Dim sSpan = New HtmlGenericControl("span")
                    sSpan.Style.Add("display", "block")
                    Dim imgPartida As New Image
                    litPartida.Text = PartidaPres("PARTIDA_DEN") & ":"
                    tbPartida.ID = "tbPPres_" & PartidaPres("PRES0")
                    tbPartida.Style.Add("float", "left")
                    tbPartida.Style.Add("width", "90%")
                    tbPartida.ClientIDMode = UI.ClientIDMode.Static
                    tbPartidaHidden.ID = "tbPPres_Hidden_" & PartidaPres("PRES0")
                    tbPartida.Attributes.Add("onKeyDown", "abrirPanelPartidas('" & mpePartidas.ClientID & "', '" & listadoPartidas.ClientID & "', '" & PartidaPres("PRES0") & "', '" & PartidaPres("PARTIDA_DEN") & "', '" & lblPartidaPres0.ClientID & "', '" & lblFiltroPartida.ClientID & "', '" & JSText(Textos(259)) & "', '" & listadoInputsFiltro.ClientID & "', event, '" & tbPartida.ClientID & "', '" & tbPartidaHidden.ClientID & "');")
                    tbPartidaHidden.ClientIDMode = UI.ClientIDMode.Static
                    imgPartida.ID = "imgPPres_" & PartidaPres("PRES0")
                    imgPartida.ImageUrl = "../../Images/abrir_ptos_susp.gif"
                    imgPartida.Style.Add("border", "0px")
                    imgPartida.Style.Add("padding-left", "3px")
                    sSpan.Controls.Add(imgPartida)
                    aPartida.HRef = "#"
                    aPartida.Attributes("onclick") = "javascript:abrirPanelPartidas('" & mpePartidas.ClientID & "', '" & listadoPartidas.ClientID & "', '" & PartidaPres("PRES0") & "', '" & PartidaPres("PARTIDA_DEN") & "', '" & lblPartidaPres0.ClientID & "', '" & lblFiltroPartida.ClientID & "', '" & JSText(Textos(259)) & "', '" & listadoInputsFiltro.ClientID & "', 'event', '" & tbPartida.ClientID & "', '" & tbPartidaHidden.ClientID & "');"
                    aPartida.Style.Add("border", "0px")
                    aPartida.Style.Add("display", "block")
                    aPartida.Style.Add("float", "left")
                    aPartida.Controls.Add(sSpan)
                    tbPartida.CssClass = "Normal"

                    Celda1Asp.Controls.Add(litPartida)
                    Celda2Asp.Controls.Add(tbPartida)
                    Celda2Asp.Controls.Add(tbPartidaHidden)
                    Celda2Asp.Controls.Add(aPartida)
                    FilaAsp.ID = "trPPres_" & PartidaPres("PRES0")
                    FilaAsp.Cells.Add(Celda1Asp)
                    FilaAsp.Cells.Add(Celda2Asp)
                    'FilaAsp.Width = Fila5Tabla1.Width
                    FilaAsp.Style.Add("padding-bottom", "6px")
                    TablaAsp.Rows.Add(FilaAsp)
                End If
            Next
            If phrPartidasPresBuscador.FindControl("tbDdlPP") Is Nothing Then
                'TablaAsp.Width = Fila5Tabla1.Width
                TablaAsp.CellPadding = 0
                TablaAsp.CellSpacing = 0
                TablaAsp.ID = "tbDdlPP"
                phrPartidasPresBuscador.Controls.Add(TablaAsp)
            End If
        End If
    End Sub

    ''' Revisado por: blp. Fecha: 21/08/2012
    ''' <summary>
    ''' Función que devuelve un objeto List que contiene los nodos de un determinado nivel de la partida presupuestaria, presentes en los pedidos
    ''' </summary>
    ''' <param name="Pres0">String que indica a la función la partida presupuestaria de la que se quieren recuperar los nodos presentes en las líneas de pedido de las órdenes</param>
    ''' <param name="Nivel">String que indica el nivel del árbol de partida presupuestaria en el que se van a imputar las líneas de pedido</param>
    ''' <param name="dsInfo">Dataset desde el que se recuperan los datos de los nodos</param>
    ''' <returns>Objeto List que contiene los nodos de la partida presupuestaria seleccionada que están presentes en las líneas de pedido de las órdenes</returns>
    ''' <remarks>Llamado desde CargarCombosPartidas
    ''' Tiempo máximo: 0 sec</remarks>
    Public Function PartidasPresup(ByVal Pres0 As String, ByVal Nivel As String, ByVal dsInfo As DataSet) As List(Of ListItem)
        Dim query As List(Of ListItem)
        Select Case Nivel
            Case "1"
                query = From Datos In dsInfo.Tables("ORDENES") _
                    Where Datos.Item("Pres0") IsNot DBNull.Value AndAlso Datos.Item("Pres0") = Pres0 _
                    Order By Datos.Item("PRES0").ToString, Datos.Item("PRES1").ToString, Datos.Item("PRES3").ToString, Datos.Item("PRES4").ToString _
                    Select New ListItem(Datos.Item("PRES0").ToString & "-" & Datos.Item("PRES1").ToString & " " & Datos.Item("NIVEL_DEN_" & Pres0).ToString, Datos.Item("PRES0").ToString & "@@" & Datos.Item("PRES1").ToString) _
                    Distinct.ToList()
            Case "2"
                query = From Datos In dsInfo.Tables("ORDENES") _
                    Where Datos.Item("Pres0") IsNot DBNull.Value AndAlso Datos.Item("Pres0") = Pres0 _
                    Order By Datos.Item("PRES0").ToString, Datos.Item("PRES1").ToString, Datos.Item("PRES3").ToString, Datos.Item("PRES4").ToString _
                    Select New ListItem(Datos.Item("PRES0").ToString & "-" & Datos.Item("PRES1").ToString & "-" & Datos.Item("PRES2").ToString & " " & Datos.Item("NIVEL_DEN_" & Pres0).ToString, Datos.Item("PRES0").ToString & "@@" & Datos.Item("PRES1").ToString & "-" & Datos.Item("PRES2").ToString) _
                    Distinct.ToList()
            Case "3"
                query = From Datos In dsInfo.Tables("ORDENES") _
                    Where Datos.Item("Pres0") IsNot DBNull.Value AndAlso Datos.Item("Pres0") = Pres0 _
                    Order By Datos.Item("PRES0").ToString, Datos.Item("PRES1").ToString, Datos.Item("PRES3").ToString, Datos.Item("PRES4").ToString _
                    Select New ListItem(Datos.Item("PRES0").ToString & "-" & Datos.Item("PRES1").ToString & "-" & Datos.Item("PRES2").ToString & "-" & Datos.Item("PRES3").ToString & " " & Datos.Item("NIVEL_DEN_" & Pres0).ToString, Datos.Item("PRES0").ToString & "@@" & Datos.Item("PRES1").ToString & "-" & Datos.Item("PRES2").ToString & "-" & Datos.Item("PRES3").ToString) _
                    Distinct.ToList()
            Case Else '4
                query = From Datos In dsInfo.Tables("ORDENES") _
                    Where Datos.Item("Pres0") IsNot DBNull.Value AndAlso Datos.Item("Pres0") = Pres0 _
                    Order By Datos.Item("PRES0").ToString, Datos.Item("PRES1").ToString, Datos.Item("PRES3").ToString, Datos.Item("PRES4").ToString _
                    Select New ListItem(Datos.Item("PRES0").ToString & "-" & Datos.Item("PRES1").ToString & "-" & Datos.Item("PRES2").ToString & "-" & Datos.Item("PRES3").ToString & "-" & Datos.Item("PRES4").ToString & " " & Datos.Item("NIVEL_DEN_" & Pres0).ToString, Datos.Item("PRES0").ToString & "@@" & Datos.Item("PRES1").ToString & "-" & Datos.Item("PRES2").ToString & "-" & Datos.Item("PRES3").ToString & "-" & Datos.Item("PRES4").ToString) _
                    Distinct.ToList()
        End Select
        Return query
    End Function

#End Region

    ''' Revisado por: blp. Fecha: 22/08/2012
    ''' <summary>
    ''' Método mediante el cual cargamos en el control whdgEntregas los datos de entregas
    ''' </summary> 
    ''' <remarks>
    ''' False->La carga del grid no es para exportación sino para mostrar en pantalla por lo que se pagina
    '''      True->La carga del grid es para exportación por lo que no se pagina a sólo saldrían en la exportación el número de registros indicado en la paginación
    ''' Llamada desde Load() y Exportar. Máx 1 seg
    ''' </remarks>
    Private Sub CargarwhdgEntregas()
        Dim dsPedidos As New DataSet
        dsPedidos = DsetPedidos()

        Dim groupedColumns() As String = Split(Session("COLSGROUPBY"), "#")
        For Each item As String In groupedColumns
            If Not item = "" Then whdgEntregas.GroupingSettings.GroupedColumns.Add(Split(item, " ")(0), _
                IIf(Split(item, " ")(1) = "DESC", Infragistics.Web.UI.GridControls.GroupingSortDirection.Descending, Infragistics.Web.UI.GridControls.GroupingSortDirection.Ascending))
        Next

        'Se tiene que hacer antes que el databind
        whdgEntregas.GridView.Behaviors.Paging.Enabled = True
        whdgEntregas.GridView.Behaviors.Paging.PageSize = ConfigurationManager.AppSettings("registrosPaginacion")
        whdgEntregas.GridView.Behaviors.Paging.PageIndex = Me._PaginadorTop.PageNumber - 1


        'IMP: Si anteriormente hay una o mas filas seleccionadas da un error y no se carga la grid. Por eso hay que limpiar las filas seleccionadas.
        whdgEntregas.GridView.Behaviors.Selection.SelectedRows.Clear()

        If Not IsPostBack Then
            whdgEntregas.Rows.Clear()
            whdgEntregas.Columns.Clear()
            whdgEntregas.GridView.Columns.Clear()

            whdgEntregas.DataSource = dsPedidos

            ObtenerColumnasGrid(whdgEntregas.UniqueID)

            'Necesitamos que se genere el DSetPedidos para usarlo dentro del CreatewhdgEntregasColumnsYConfiguracion
            'por lo que no llamamos al CreatewhdgEntregasColumnsYConfiguracion hasta tenerlo
            CreatewhdgEntregasColumnsYConfiguracion()

            'Crea columnas Exportacion
            ObtenerColumnasGrid(whdgEntregasExportacion.UniqueID)
        Else

            whdgEntregas.DataSource = dsPedidos
        End If

        'Los DataKeyFields deben definirse entre el Datasource y el Databind
        whdgEntregas.DataMember = "ORDENES"
        whdgEntregas.DataKeyFields = "LINEAPEDID, LINEARECEPID, PLAN_ENTREGA"
        whdgEntregas.DataBind()


    End Sub

    ''' <summary>
    ''' Inicializa las columnas del grid con los campos generales
    ''' </summary>
    ''' <param name="gridId">grid</param>
    ''' <remarks>Llamada desde=Page_Load uwgCertificados_InitializeLayout// ; Tiempo mÃ¡ximo:0seg.</remarks>
    Private Sub ObtenerColumnasGrid(ByVal gridId As String)
        Dim grid As WebHierarchicalDataGrid = CType(Me.FindControl(gridId), WebHierarchicalDataGrid)

        AddColumnYConfiguracion(gridId, "PLAN_ENTREGA", Textos(239), 0, False)
        AddColumnYConfiguracion(gridId, "FECHARECEP", Textos(160), 1, False, , "{0:d}", , Textos(149))
        AddColumnYConfiguracion(gridId, "ANYOPEDIDOORDEN", Textos(161), 2, False, "Rotulo itemSeleccionable")
        If MostrarNumeroPedidoERP Then 'Literal del num pedido ERP
            Dim oCParametros As FSNServer.CParametros = FSNServer.Get_Object(GetType(FSNServer.CParametros))
            AddColumnYConfiguracion(gridId, "NUM_PED_ERP", oCParametros.CargarLiteralParametros(Fullstep.FSNLibrary.TiposDeDatos.LiteralesParametros.PedidoERP, Me.Usuario.Idioma) _
                                       , 3, False, "Rotulo itemSeleccionable")
        Else
            AddColumnYConfiguracion(gridId, "NUM_PED_ERP", "NUM_PED_ERP", 3, True,,,,, False)
        End If
        AddColumnYConfiguracion(gridId, "ARTDEN", Textos(148), 4, False, "Rotulo itemSeleccionable")
        AddColumnYConfiguracion(gridId, "NUMLINEA", Textos(189), 5, False, "Rotulo itemSeleccionable")
        AddColumnYConfiguracion(gridId, "DENEMPRESA", Textos(84), 6, False)
        AddColumnYConfiguracion(gridId, "ALBARAN", Textos(152), 7, False, "Rotulo itemSeleccionable")
        AddColumnYConfiguracion(gridId, "PREC_UP", Textos(178), 8, False, , , , Textos(179))
        AddColumnYConfiguracion(gridId, "CANTLINEAPEDIDATOTAL", Textos(174), 9, False, , , , Textos(167))
        AddColumnYConfiguracion(gridId, "IMPORTELINEATOTAL", Textos(48), 10, False, , , , Textos(171))
        AddColumnYConfiguracion(gridId, "CANTLINEARECIBIDAPARCIAL", Textos(175), 11, False, , , , Textos(168))
        AddColumnYConfiguracion(gridId, "CANTLINEAPENDIENTETOTAL", Textos(176), 12, False, , , , Textos(169))
        AddColumnYConfiguracion(gridId, "IMPORTE", Textos(266), 13, False, , , , Textos(266)) 'Importe pedido
        AddColumnYConfiguracion(gridId, "CANT_PLANIF", Textos(240), 14, False)
        AddColumnYConfiguracion(gridId, "UNICOD", Textos(177), 15, False, "Rotulo itemSeleccionable")
        AddColumnYConfiguracion(gridId, "FECENTREGASOLICITADA", Textos(163), 16, False, , , , Textos(150))
        AddColumnYConfiguracion(gridId, "FECENTREGAINDICADAPROVE", Textos(164), 17, False, , , , Textos(151))
        AddColumnYConfiguracion(gridId, "APROVISIONADOR", Textos(182), 18, False, "Rotulo itemSeleccionable")
        AddColumnYConfiguracion(gridId, "PROVEDEN", Textos(10), 19, False, "Rotulo itemSeleccionable")
        AddColumnYConfiguracion(gridId, "RECEPTOR", Textos(258), 20, False, , , , , , "DENRECEPTOR")
        AddColumnYConfiguracion(gridId, "DESTDEN", Textos(132), 21, False, "Rotulo itemSeleccionable")
        Dim VisibleIndex As Integer = 23
        'CENTROS DE COSTE Y PARTIDAS
        If Me.Acceso.gbAccesoFSSM Then
            AddColumnYConfiguracion(gridId, "CCDEN", Textos(116), 22, False, , , True)
        Else
            AddColumnYConfiguracion(gridId, "CCDEN", Textos(116), 22, True, , , True, , False)
        End If
        If Me.Acceso.gbAccesoFSSM Then
            Dim oPRES0Cods = PRES0Cods(TipoOrigenPartidaPres.ConfiguracionSM)
            If oPRES0Cods.Any Then
                For Each columnName As String In oPRES0Cods
                    Dim sPartida As String = columnName
                    Dim queryPartida = From nomPartida In DsetPedidos.Tables("PARTIDAS_DEN").Rows _
                                   Select nomPartida(sPartida) Take 1
                    If queryPartida(0).ToString <> String.Empty Then sPartida = queryPartida(0).ToString

                    Me.AddColumnYConfiguracion(gridId, "NIVEL_DEN_" & columnName, sPartida, VisibleIndex, False)
                    VisibleIndex = VisibleIndex + 1
                Next
            End If
        End If

        AddColumnYConfiguracion(gridId, "APROVISIONADORCOD", Textos(182), VisibleIndex, False, , , , , False)
        AddColumnYConfiguracion(gridId, "PROVECOD", Textos(10), SiguiVisibleIndex(VisibleIndex), False, , , , , False)
        AddColumnYConfiguracion(gridId, "ORDENEST", Textos(190), SiguiVisibleIndex(VisibleIndex), False, , , , Textos(191))
        AddColumnYConfiguracion(gridId, "TIPOPEDIDO", Textos(117), SiguiVisibleIndex(VisibleIndex), False)
        If HayIntegracionRecepcionEntradaOEntradaSalida Then
            Dim oCParametros As FSNServer.CParametros = FSNServer.Get_Object(GetType(FSNServer.CParametros))
            AddColumnYConfiguracion(gridId, "NUM_ERP_RECEP", oCParametros.CargarLiteralParametros(Fullstep.FSNLibrary.TiposDeDatos.LiteralesParametros.RecepcionERP, Me.Usuario.Idioma), SiguiVisibleIndex(VisibleIndex), False)
        Else
            AddColumnYConfiguracion(gridId, "NUM_ERP_RECEP", "NUM_ERP_RECEP", SiguiVisibleIndex(VisibleIndex), False)
        End If
        AddColumnYConfiguracion(gridId, "NUMPEDPROVE", Textos(51), SiguiVisibleIndex(VisibleIndex), False)
        AddColumnYConfiguracion(gridId, "FECHAEMISION", Textos(162), SiguiVisibleIndex(VisibleIndex), False, , , , Textos(87))
        AddColumnYConfiguracion(gridId, "IMPORTELINEARECIBIDOPARCIAL", Textos(165), SiguiVisibleIndex(VisibleIndex), False, , , , Textos(172))
        AddColumnYConfiguracion(gridId, "IMPORTELINEAPENDIENTETOTAL", Textos(166), SiguiVisibleIndex(VisibleIndex), False, , , , Textos(173))
        AddColumnYConfiguracion(gridId, "COD_PROVE_ERP", Textos(85), SiguiVisibleIndex(VisibleIndex), False)
        AddColumnYConfiguracion(gridId, "PEDIDOOBS", Textos(183), SiguiVisibleIndex(VisibleIndex), False, , , , Textos(184))
        AddColumnYConfiguracion(gridId, "ORDENOBS", Textos(185), SiguiVisibleIndex(VisibleIndex), False, , , , Textos(186))
        AddColumnYConfiguracion(gridId, "LINEA_PEDIDO_OBS", Textos(187), SiguiVisibleIndex(VisibleIndex), False, , , , Textos(188))
        AddColumnYConfiguracion(gridId, "TIENEADJ", Textos(271), SiguiVisibleIndex(VisibleIndex), False)
        AddColumnYConfiguracion(gridId, "CATEGORIA", Textos(136), SiguiVisibleIndex(VisibleIndex), False, "itemSeleccionable", , , , , "CAT1")
        AddColumnYConfiguracion(gridId, "IM_RECEPAUTO", "", SiguiVisibleIndex(VisibleIndex), True, , , , , False)
        AddColumnYConfiguracion(gridId, "LINEAPEDID", "", SiguiVisibleIndex(VisibleIndex), True, , , , , False)
        AddColumnYConfiguracion(gridId, "FEC_ACEPT", Textos(261), SiguiVisibleIndex(VisibleIndex), False)
        AddColumnYConfiguracion(gridId, "OBS_PROV", Textos(262), SiguiVisibleIndex(VisibleIndex), False)
        AddColumnYConfiguracion(gridId, "TIPO", Textos(263), SiguiVisibleIndex(VisibleIndex), False)
        AddColumnYConfiguracion(gridId, "ORDENID", "ORDENID", SiguiVisibleIndex(VisibleIndex), True,,,,, False)
        AddColumnYConfiguracion(gridId, "DESTCOD", Textos(132), SiguiVisibleIndex(VisibleIndex), False, , , , , False)
        AddColumnYConfiguracion(gridId, "PORCEN_DESVIO", Textos(277), SiguiVisibleIndex(VisibleIndex), False,,,, Textos(276))

        AddColumnYConfiguracion(gridId, "PED_RECEP_ADJUNTOS", Textos(96), SiguiVisibleIndex(VisibleIndex), False, "Rotulo itemSeleccionable")
        AddColumnYConfiguracion(gridId, "TIPORECEPCION", "TIPORECEPCION", SiguiVisibleIndex(VisibleIndex), True)
        If FSNServer.TipoAcceso.gbMostrarFechaContable Then
            AddColumnYConfiguracion(gridId, "FEC_CONTABLE", Textos(279), SiguiVisibleIndex(VisibleIndex), False)
        End If

        setNumberFormat(grid.GridView.Columns.FromKey("Key_CANTLINEAPEDIDATOTAL"))
        setNumberFormat(grid.GridView.Columns.FromKey("Key_CANTLINEARECIBIDAPARCIAL"))
        setNumberFormat(grid.GridView.Columns.FromKey("Key_CANTLINEAPENDIENTETOTAL"))
        setNumberFormat(grid.GridView.Columns.FromKey("Key_CANT_PLANIF"))
        setNumberFormat(grid.GridView.Columns.FromKey("Key_PREC_UP"))
        setNumberFormat(grid.GridView.Columns.FromKey("Key_IMPORTELINEATOTAL"))
        setNumberFormat(grid.GridView.Columns.FromKey("Key_IMPORTELINEARECIBIDOPARCIAL"))
        setNumberFormat(grid.GridView.Columns.FromKey("Key_IMPORTELINEAPENDIENTETOTAL"))

        setDateFormat(grid.GridView.Columns.FromKey("Key_FECHAEMISION"))
        setDateFormat(grid.GridView.Columns.FromKey("Key_FECENTREGASOLICITADA"))
        setDateFormat(grid.GridView.Columns.FromKey("Key_FECENTREGAINDICADAPROVE"))
    End Sub

#Region "Entregas Exportacion"
    ''' <summary>
    ''' Cargar Grid Exportacion
    ''' </summary>
    ''' <remarks>Llamada desde: Exportar; Tiempo mÃ¡ximo: 0 sg.</remarks>
    Private Sub CargarGridExportacion()
        Dim ds As DataSet = CType(HttpContext.Current.Cache("DsetPedidosRecepcion_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()), DataSet).Copy

        With whdgEntregasExportacion
            .Rows.Clear()
            .GridView.ClearDataSource()
            .DataSource = ds
            .GridView.DataSource = .DataSource
            confAnchosVisibilidadYPosicion(whdgEntregasExportacion.UniqueID)
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_RecepcionPedidos
            .DataMember = "ORDENES"
            .DataKeyFields = "LINEAPEDID, LINEARECEPID, PLAN_ENTREGA"
            .DataBind()
            pnlGrid.Update()
        End With
    End Sub

    ''' <summary>
    ''' configurar las columnas de entrega
    ''' </summary>
    ''' <param name="sender">Control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. MÃ¡x 0,1 seg</remarks>
    Private Sub whdgEntregasExportacion_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgEntregasExportacion.InitializeRow
        Dim oData As DataRow = e.Row.DataItem.Item.Row
        Dim txtNoAplica As String = "&lt;" & Me.Textos(268) & "&gt;"
        If whdgEntregasExportacion.GridView.Columns.FromKey("Key_ALBARAN") IsNot Nothing Then
            Dim Index As Integer = whdgEntregasExportacion.GridView.Columns.FromKey("Key_ALBARAN").Index()
            If Me.Acceso.g_bAccesoFSFA AndAlso DBNullToInteger(oData("IM_BLOQUEO")) > 0 Then
                Dim UrlImage As String = "../../Images/stopx40.png"
                Dim sObservaciones As String = DBNullToStr(oData("IM_BLOQUEO_OBS"))
                Dim sHTMLImgBloqueo As String = "<img id='img_IM_BLOQUEO' class='img_IM_BLOQUEO' name='img_IM_BLOQUEO' src='" & UrlImage & "' style='text-align:center;margin-left:5px;"
                If Not String.IsNullOrEmpty(sObservaciones) Then
                    sHTMLImgBloqueo += "cursor:pointer;"
                End If
                sHTMLImgBloqueo += "' observaciones='" & sObservaciones & "' />"
                e.Row.Items.Item(Index).Text += sHTMLImgBloqueo
            End If
        End If

        e.Row.Items.FindItemByKey("Key_TIPOPEDIDO").Text = DBNullToStr(oData("TIPOPEDIDO_DEN"))
        e.Row.Items.FindItemByKey("Key_PROVEDEN").Text = If(FSNUser.MostrarCodProve, DBNullToStr(oData("PROVECOD")) & " - ", "") & DBNullToStr(oData("PROVEDEN"))
        e.Row.Items.FindItemByKey("Key_ARTDEN").Text = "<a id='IM_CODART' onclick='AbrirDetalleArticulo(" & DBNullToStr(oData("LINEAPEDID")) & ")'>" & IIf(DBNullToStr(oData("ARTDEN")) <> "", DBNullToStr(oData("ARTDEN")), "") & "</a>"
        e.Row.Items.FindItemByKey("Key_ARTDEN").Tooltip = IIf(DBNullToStr(oData("ARTDEN")) <> "", DBNullToStr(oData("ARTDEN")), "")
        If DBNullToStr(oData("CAT1")) <> "" Then
            Dim listCategorias As List(Of String) = DevolverRamaCategorias(DBNullToStr(oData("CAT1")), DBNullToStr(oData("CAT2")), DBNullToStr(oData("CAT3")), DBNullToStr(oData("CAT4")), DBNullToStr(oData("CAT5")))
            e.Row.Items.FindItemByKey("Key_CATEGORIA").Text = listCategorias.Item(0)
            e.Row.Items.FindItemByKey("Key_CATEGORIA").Tooltip = listCategorias.Item(1)
        End If
        e.Row.Items.FindItemByKey("Key_UNICOD").Tooltip = DBNullToStr(oData("UNIDEN"))
        If DBNullToStr(oData("TIENEADJ")) = "1" Then
            e.Row.Items.FindItemByKey("Key_TIENEADJ").Text = " <img id='img_IM_ADJUNTO' onclick='AbrirAdjuntos(" & DBNullToStr(oData("ORDENID")) & "," & DBNullToStr(oData("LINEAPEDID")) & ", false)'  src='" & ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/adjuntos_cab_small.gif' style='text-align:center;cursor:pointer;' />"
        Else
            e.Row.Items.FindItemByKey("Key_TIENEADJ").Text = ""
        End If
        If DBNullToStr(oData("PED_RECEP_ADJUNTOS")).Length > 0 Then
            e.Row.Items.FindItemByKey("Key_PED_RECEP_ADJUNTOS").Text = " <span onclick='AbrirAdjuntos(" & DBNullToStr(oData("IDPED_RECEP")) & "," & DBNullToStr(oData("IDPED_RECEP")) & ", true)'  style='text-align:center;cursor:pointer;'>" & DBNullToStr(oData("PED_RECEP_ADJUNTOS")) & "</span>"
        Else
            e.Row.Items.FindItemByKey("Key_PED_RECEP_ADJUNTOS").Text = ""
        End If

        If DBNullToInteger(oData("PLAN_ENTREGA")) <> 0 Then
            'Si tiene plan de entrega se mirara si es manual o automatico
            If DBNullToStr(oData("IM_RECEPAUTO")) = "1" Then
                e.Row.Items.FindItemByKey("Key_PLAN_ENTREGA").Text = " <img id='img_IM_RECEPAUTO'  src='" & System.Configuration.ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/plan_auto.png' style='text-align:center;' />"
            Else
                e.Row.Items.FindItemByKey("Key_PLAN_ENTREGA").Text = " <img id='img_IM_RECEPAUTO'  src='" & System.Configuration.ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/plan_manual.png' style='text-align:center;' />"
            End If
        Else
            e.Row.Items.FindItemByKey("Key_PLAN_ENTREGA").Text = String.Empty
        End If
        If whdgEntregasExportacion.GridView.Columns.FromKey("Key_ORDENEST") IsNot Nothing Then
            Dim Index As Integer = whdgEntregasExportacion.GridView.Columns.FromKey("Key_ORDENEST").Index()
            Dim sEstado As String = String.Empty
            Select Case CType(DBNullToStr(oData("ORDENEST")), TipoEstadoOrdenEntrega)
                Case TipoEstadoOrdenEntrega.EmitidoAlProveedor
                    sEstado = Textos(119) 'Emitido al proveedor
                Case TipoEstadoOrdenEntrega.AceptadoPorProveedor
                    sEstado = Textos(24) 'Pedido Aceptado por el proveedor
                Case TipoEstadoOrdenEntrega.EnCamino
                    sEstado = Textos(25) 'En camino
                Case TipoEstadoOrdenEntrega.EnRecepcion
                    sEstado = Textos(26) 'Recibido parcialmente
                Case TipoEstadoOrdenEntrega.RecibidoYCerrado
                    sEstado = Textos(27) 'Cerrado
            End Select
            e.Row.Items.Item(Index).Text = sEstado
        End If
        If Me.Acceso.gbAccesoFSSM Then
            e.Row.Items.FindItemByKey("Key_CCDEN").Text = DBNullToStr(oData("CCDEN"))
        End If
        Dim Tipo_Index As Integer = whdgEntregasExportacion.GridView.Columns.FromKey("Key_TIPO").Index()
        Dim sTipo As String = String.Empty
        Select Case DBNullToInteger(oData("TIPO"))
            Case 0
                sTipo = Textos(129) 'Pedidos negociados
            Case 2
                sTipo = Textos(131) 'Pedidos directos
            Case 1
                If DBNullToStr(oData("ARTCOD")) = "" Then
                    sTipo = Textos(255) 'Pedidos de catálogo libres
                Else
                    sTipo = Textos(128) 'Pedidos de catálogo negociados
                End If
        End Select
        e.Row.Items.Item(Tipo_Index).Text = sTipo

        If oData("TIPORECEPCION") = 1 Then
            e.Row.Items.FindItemByKey("Key_CANTLINEARECIBIDAPARCIAL").Text = txtNoAplica
            e.Row.Items.FindItemByKey("Key_CANTLINEARECIBIDAPARCIAL").CssClass = "apagado"
            e.Row.Items.FindItemByKey("Key_CANTLINEAPEDIDATOTAL").Text = txtNoAplica
            e.Row.Items.FindItemByKey("Key_CANTLINEAPEDIDATOTAL").CssClass = "apagado"
            e.Row.Items.FindItemByKey("Key_CANTLINEAPENDIENTETOTAL").Text = txtNoAplica
            e.Row.Items.FindItemByKey("Key_CANTLINEAPENDIENTETOTAL").CssClass = "apagado"
            e.Row.Items.FindItemByKey("Key_PREC_UP").Text = txtNoAplica
            e.Row.Items.FindItemByKey("Key_PREC_UP").CssClass = "apagado"
            e.Row.Items.FindItemByKey("Key_UNICOD").Text = oData("MONCOD")
        End If
    End Sub
#End Region

#Region "Paginador"
    ''' Revisado por: blp. Fecha: 15/12/2011
    ''' <summary>
    ''' Inicializamos el paginador
    ''' </summary>
    ''' <remarks>Llamada desde load y botones de búsqueda. Máx. 0,1 seg.</remarks>
    Private Sub InicializarPaginador()
        Dim numRegistros As Long = 0
        If whdgEntregas.GridView.Behaviors.Filtering.Filtered Then
            whdgEntregas.GridView.Behaviors.Paging.Enabled = False
            numRegistros = whdgEntregas.GridView.Rows.Count
            whdgEntregas.GridView.Behaviors.Paging.Enabled = True
        Else
            If DsetPedidos IsNot Nothing AndAlso DsetPedidos.Tables("ORDENES") IsNot Nothing AndAlso DsetPedidos.Tables("ORDENES").Rows.Count > 0 Then _
                numRegistros = DsetPedidos.Tables("ORDENES").Rows.Count
        End If
        Me._PaginadorTop.SetContext(numRegistros, System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion"))
    End Sub

    ''' Revisado por: blp. Fecha: 30/01/2012
    ''' <summary>
    ''' Método que actualiza el contador de páginas del paginador
    ''' </summary>
    ''' <param name="sender">Control que lanza el evento PageChanged</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde evento PageChanged. Máximo: 0,1 seg.</remarks>
    Private Sub _Paginador_PageChanged(ByVal sender As Object, ByVal e As PageSettingsChangedEventArgs)
        If (e.TotalNumberOfPages - 1) < e.PageIndex Then
            Me.whdgEntregas.GridView.Behaviors.Paging.PageIndex = 0
        Else
            Me.whdgEntregas.GridView.Behaviors.Paging.PageIndex = e.PageIndex
        End If

        pnlGrid.Update()
    End Sub
#End Region

    ''' Revisado por: blp. Fecha: 28/08/2012
    ''' <summary>
    ''' En el evento click del control btnAceptarConfiguracion e ibGuardarConfiguracion
    ''' Guardamos la configuración de los campos a mostrar en el webdatagrid: tanto visibilidad (con la info del panel), como ancho de las columnas
    ''' </summary>
    ''' <param name="sender">Control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. Máx. 0,5 seg.</remarks>
    Private Sub GuardarConfiguracion(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarConfiguracion.Click, ibGuardarConfiguracion.Click
        Dim oRecepciones As CRecepciones = FSNServer.Get_Object(GetType(CRecepciones))
        'Obtenemos la relación entre los campos de la tabla de configuracion de anchos y visibilidad con los campos de la tabla de entregas
        Dim camposEntregasYConfiguracionIni As List(Of Campos) = relacionarEntregasyConfiguracion()
        Dim camposEntregasYConfiguracionFinal As DataTable = oRecepciones.CrearTablaEP_CONF_VISOR_RECEPCIONES()
        For Each oCampos As Campos In camposEntregasYConfiguracionIni
            Dim camposEntregasYConfiguracionFinal_row As DataRow = camposEntregasYConfiguracionFinal.NewRow
            camposEntregasYConfiguracionFinal_row.Item("TIPOCAMPO") = oCampos.TipoCampoRecepciones
            camposEntregasYConfiguracionFinal_row.Item("ID") = oCampos.CampoEntregaIDGuardado

            'VISIBLE
            camposEntregasYConfiguracionFinal_row.Item("VISIBLE") = Not whdgEntregas.GridView.Columns.FromKey("Key_" & oCampos.CampoEntrega).Hidden

            ''ANCHO Y POSICION
            If whdgEntregas.GridView.Columns.Item("Key_" & oCampos.CampoEntrega) IsNot Nothing Then
                camposEntregasYConfiguracionFinal_row.Item("ANCHO") = whdgEntregas.GridView.Columns.Item("Key_" & oCampos.CampoEntrega).Width.Value
                camposEntregasYConfiguracionFinal_row.Item("POSICION") = whdgEntregas.GridView.Columns("Key_" & oCampos.CampoEntrega).VisibleIndex
            End If

            camposEntregasYConfiguracionFinal.Rows.Add(camposEntregasYConfiguracionFinal_row)
        Next
        'Guardar
        oRecepciones.Mod_Conf_Visor_Recepciones(Me.Usuario.Cod, camposEntregasYConfiguracionFinal)
        'Configurar anchos y visibilidad de nuevo
        confAnchosVisibilidadYPosicion(Me.whdgEntregas.UniqueID)
        ibGuardarConfiguracion.Style("display") = "none"
        pnlGrid.Update()
    End Sub

    ''' Revisado por: blp. Fecha: 28/08/2012
    ''' <summary>
    ''' Cargamos los valores de configuración del grid de datos en el panel de Configuración
    ''' </summary>
    ''' <remarks>Llamada desde CreatewhdgEntregasColumnsYConfiguracion. Máx. 0,2 seg.</remarks>
    Private Sub CargarInfoPanelConfiguracion()
        Dim oRecepciones As CRecepciones = FSNServer.Get_Object(GetType(FSNServer.CRecepciones))
        Dim dtConfiguracionRecepciones As DataTable = oRecepciones.Obt_Conf_Visor_Recepciones(Me.Usuario.Cod)
        'Obtenemos la relación entre los campos de la tabla de configuracion de anchos y visibilidad con los campos de la tabla de Recepciones
        Dim camposRecepcionesYConfiguracion As List(Of Campos) = relacionarEntregasyConfiguracion()
        For Each oCampos As Campos In camposRecepcionesYConfiguracion
            If tblPnlConfig.FindControl("chk" & oCampos.CampoEntrega) IsNot Nothing Then
                Dim chk As CheckBox = TryCast(tblPnlConfig.FindControl("chk" & oCampos.CampoEntrega), CheckBox)
                If chk IsNot Nothing Then
                    If dtConfiguracionRecepciones.Rows.Count > 0 AndAlso dtConfiguracionRecepciones.Rows.Find({oCampos.CampoEntregaIDGuardado, oCampos.TipoCampoRecepciones}) IsNot Nothing Then
                        chk.Checked = CBool(dtConfiguracionRecepciones.Rows.Find({oCampos.CampoEntregaIDGuardado, oCampos.TipoCampoRecepciones}).Item("VISIBLE"))
                    Else
                        chk.Checked = oCampos.Visible
                    End If
                End If
            End If
        Next
    End Sub

#Region "Validación Intergracion"
    Private ReadOnly Property HayIntegracionPedidosEntradaOEntradaSalida() As Boolean
        Get
            If HttpContext.Current.Session("HayIntegracionPedidosEntradaOEntradaSalida") Is Nothing Then
                Dim tablas As Integer()
                ReDim tablas(1)
                tablas(0) = TablasIntegracion.PED_Aprov
                tablas(1) = TablasIntegracion.PED_directo
                HttpContext.Current.Session("HayIntegracionPedidosEntradaOEntradaSalida") = HayIntegracionEntradaoEntradaSalida(tablas)
            End If
            Return HttpContext.Current.Session("HayIntegracionPedidosEntradaOEntradaSalida")
        End Get
    End Property

    Private ReadOnly Property HayIntegracionRecepcionEntradaOEntradaSalida() As Boolean
        Get
            If HttpContext.Current.Session("HayIntegracionRecepcionEntradaOEntradaSalida") Is Nothing Then
                Dim tablas As Integer()
                ReDim tablas(1)
                tablas(0) = TablasIntegracion.Rec_Aprov
                tablas(1) = TablasIntegracion.Rec_Directo
                HttpContext.Current.Session("HayIntegracionRecepcionEntradaOEntradaSalida") = HayIntegracionEntradaoEntradaSalida(tablas)
            End If
            Return HttpContext.Current.Session("HayIntegracionRecepcionEntradaOEntradaSalida")
        End Get
    End Property

    ''' Revisado por: blp. Fecha: 23/08/2012
    ''' <summary>
    ''' Función que nos dice si hay integracion entrada salida en alguna de las tablas de un array
    ''' </summary>
    ''' <param name="tablas">Array de Id's de tabla para los que se quiere averiguar si hay integración</param>
    ''' <returns>Booleano</returns>
    ''' <remarks>Llamada desde seguimiento.aspx.vb y Recepcion.aspx.vb
    ''' Tiempo máximo 0,1 sec</remarks>
    Public Function HayIntegracionEntradaoEntradaSalida(ByVal tablas As Integer()) As Boolean
        Dim hayIntegracion As Boolean = False
        Dim oEP_ValidacionesIntegracion As EP_ValidacionesIntegracion = Me.FSNServer.Get_Object(GetType(EP_ValidacionesIntegracion))
        hayIntegracion = oEP_ValidacionesIntegracion.HayIntegracionEntradaoEntradaSalida(tablas)
        Return hayIntegracion
    End Function

#End Region


#Region "CultureInfo"
    ''' Revisado por blp. Fecha: 21/08/2012
    ''' <summary>
    ''' Dar el formato del usuario a las columnas numéricas del grid
    ''' </summary>
    ''' <param name="field">Columna seleccionada</param>
    ''' <remarks>Llamada desde setCultureInfoUsuario. 0,1 seg.</remarks>
    Private Sub setNumberFormat(ByVal field As Infragistics.Web.UI.GridControls.BoundDataField)
        field.DataFormatString = "{0:n" & FSNUser.NumberFormat.NumberDecimalDigits & "}"
        field.CssClass = "itemRightAligment"
    End Sub

    ''' Revisado por blp. Fecha: 21/08/2012
    ''' <summary>
    ''' Dar el formato del usuario a las columnas de fecha del grid
    ''' </summary>
    ''' <param name="field">columna seleccionada</param>
    ''' <remarks>Llamada desde setCultureInfoUsuario. 0,1 seg.</remarks>
    Private Sub setDateFormat(ByVal field As Infragistics.Web.UI.GridControls.BoundDataField)
        field.DataFormatString = "{0:" & FSNUser.DateFormat.ShortDatePattern.ToString & "}"
    End Sub

#End Region

#Region "ESTRUCTURA"
    ''' Revisado por: blp. Fecha: 22/08/2012
    ''' <summary>
    ''' Crea las columnas fijas del grid en función de los datos guardados en la configuración (tabla FSP_CONF_VISOR_RECEPCIONES)
    ''' y añade los campos al panel de configuración y añade las columnas al grid de configuracion que esta en ese mismo panel
    ''' </summary>
    ''' <remarks>Llamada desde CargarwhdgEntregas. Máx. 0,5 seg</remarks>
    Private Sub CreatewhdgEntregasColumnsYConfiguracion()
        'Configurar Anchos y Visibilidad
        If Not IsPostBack Then
            confAnchosVisibilidadYPosicion(whdgEntregas.UniqueID)
        End If
        'Cargar desde base de datos la info del panel de configuración
        CargarInfoPanelConfiguracion()
    End Sub

    ''' Revisado por: blp. Fecha: 22/08/2012
    ''' <summary>
    ''' Función que añade una columna al control whdgEntregas, vinculada al origen de datos de éste.
    ''' </summary>
    ''' <param name="gridId">webhierarchicaldatagrid</param> 
    ''' <param name="fieldName">Nombre del campo en el origen de datos</param>
    ''' <param name="headerText">Título de la columna</param>
    ''' <param name="headerTooltip">Tooltip de la columna</param>
    ''' <param name="VisibleIndex">VisibleIndex de la columna</param> 
    ''' <remarks>Llamada desde CreatewhdgEntregasColumns. Máx. 0,1 seg.</remarks>
    Private Sub AddColumnYConfiguracion(ByVal gridId As String, ByVal fieldName As String, ByVal headerText As String, ByVal VisibleIndex As Integer _
                , Optional ByVal Hidden As Boolean = False, Optional ByVal CssClass As String = "", Optional ByVal DataFormatString As String = "" _
                , Optional ByVal UnboundField As Boolean = False, Optional ByVal headerTooltip As String = "", Optional ByVal MostrarEnPanelConfiguracion As Boolean = True _
                , Optional ByVal DataFieldName As String = "")
        Dim grid As WebHierarchicalDataGrid = CType(Me.FindControl(gridId), WebHierarchicalDataGrid)

        Dim field As Object
        If Not UnboundField Then
            field = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            If DataFieldName = String.Empty Then DataFieldName = fieldName
            field.DataFieldName = DataFieldName
        Else
            field = New Infragistics.Web.UI.GridControls.UnboundField
        End If

        field.Key = "Key_" + fieldName

        field.Header.Text = headerText

        If headerTooltip = String.Empty Then headerTooltip = headerText
        field.Header.Tooltip = headerTooltip
        field.Hidden = Hidden
        If Not CssClass = "" Then field.CssClass = CssClass
        If Not DataFormatString = "" Then field.DataFormatString = DataFormatString

        grid.GridView.Columns.Insert(VisibleIndex, field)
        grid.GridView.Columns(VisibleIndex).VisibleIndex = VisibleIndex

        anyadirTextoAColumnaNoFiltrada("Key_" & fieldName)
        'AÃ±adir al panel de configuraciÃ³n
        If MostrarEnPanelConfiguracion AndAlso (Not Hidden) Then anyadirAPanelConfiguracion(fieldName, headerText)
    End Sub


    ''' Revisado por: blp. Fecha: 23/08/2012
    ''' <summary>
    ''' Añade el alt "Filtro no aplicado" a las columnas no filtradas del whdgEntregas
    ''' </summary>
    ''' <param name="fieldKey">Key de la columna a la que queremos añadir el texto</param>
    ''' <remarks>Llamada desde EditColumnYConfiguracion y AddColumnYConfiguracion. Máx. 0,1 seg.</remarks>
    Private Sub anyadirTextoAColumnaNoFiltrada(ByVal fieldKey As String)
        Dim fieldSetting As Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldSetting.ColumnKey = fieldKey
        fieldSetting.BeforeFilterAppliedAltText = Textos(75) 'Filtro no aplicado
        Me.whdgEntregas.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        Me.whdgEntregas.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
    End Sub


    ''' Revisado por: blp. Fecha: 23/08/2012
    ''' <summary>
    ''' Configurar Anchos, Visibilidad y Posición de los campos del webhierarchicaldatagrid de recepciones y configuracion
    ''' </summary>
    ''' <param name="gridId">webhierarchicaldatagrid</param> 
    ''' <remarks>Llamada desde CreatewhdgEntregasColumns. Máx. 0,1 seg.</remarks>
    Private Sub confAnchosVisibilidadYPosicion(ByVal gridId As String)
        Dim grid As WebHierarchicalDataGrid = CType(Me.FindControl(gridId), WebHierarchicalDataGrid)

        Dim bEsExportacion As Boolean = (gridId = Me.whdgEntregasExportacion.UniqueID)

        'Obtenemos la relación entre los campos de la tabla de configuracion de anchos y visibilidad con los campos de la tabla de entregas
        Dim camposEntregasYConfiguracion As List(Of Campos) = relacionarEntregasyConfiguracion()
        'Coger los datos de ancho y visibilidad de la tabla desde base de datos
        Dim oRecepciones As CRecepciones = FSNServer.Get_Object(GetType(FSNServer.CRecepciones))
        Dim dtConfiguracionEntregas As DataTable = oRecepciones.Obt_Conf_Visor_Recepciones(Me.Usuario.Cod)
        If dtConfiguracionEntregas IsNot Nothing AndAlso dtConfiguracionEntregas.Rows.Count > 0 Then
            For Each oConfiguracionCampo As DataRow In dtConfiguracionEntregas.Rows
                Dim campo As Campos = camposEntregasYConfiguracion.Find(Function(x As Campos) x.CampoEntregaIDGuardado = oConfiguracionCampo.Item("ID") AndAlso x.TipoCampoRecepciones = oConfiguracionCampo.Item("TIPOCAMPO"))
                If campo.CampoEntrega IsNot Nothing Then

                    'VISIBLE
                    Dim visible As Boolean = oConfiguracionCampo.Item("VISIBLE")
                    If campo.CampoEntrega = "NUM_PED_ERP" Then visible = (visible And MostrarNumeroPedidoERP)
                    grid.GridView.Columns("Key_" & campo.CampoEntrega).Hidden = Not visible

                    If Not bEsExportacion Then
                        'ANCHO
                        Dim ancho As Integer = oConfiguracionCampo.Item("ANCHO")
                        If ancho > 0 Then
                            If campo.CampoEntrega IsNot Nothing Then
                                grid.GridView.Columns("Key_" & campo.CampoEntrega).Width = Unit.Pixel(ancho)
                            End If
                        Else
                            If campo.CampoEntrega IsNot Nothing Then
                                grid.GridView.Columns("Key_" & campo.CampoEntrega).Width = Unit.Pixel(campo.Width)
                            End If
                        End If
                    End If

                    'POSICIÓN
                    Dim posicion As Integer = oConfiguracionCampo.Item("POSICION")
                    If posicion >= 0 Then
                        If campo.CampoEntrega IsNot Nothing Then
                            If posicion >= grid.GridView.Columns.Count Then
                                grid.GridView.Columns("Key_" & campo.CampoEntrega).VisibleIndex = grid.GridView.Columns.Count - 1
                            Else
                                grid.GridView.Columns("Key_" & campo.CampoEntrega).VisibleIndex = posicion
                            End If
                        End If
                    End If
                End If
            Next
        Else
            For Each campo As Campos In camposEntregasYConfiguracion
                If grid.GridView.Columns("Key_" & campo.CampoEntrega) IsNot Nothing Then
                    grid.GridView.Columns("Key_" & campo.CampoEntrega).Width = Unit.Pixel(campo.Width)
                    grid.GridView.Columns("Key_" & campo.CampoEntrega).Hidden = Not campo.Visible
                    grid.GridView.Columns("Key_" & campo.CampoEntrega).VisibleIndex = campo.Position
                End If
            Next

        End If
    End Sub


    ''' Revisado por: blp. Fecha: 30/01/2012
    ''' <summary>
    ''' Añade al panel de configuración un checkbox con el campo pasado por parámetros
    ''' </summary>
    ''' <param name="fieldName">Código del campo</param>
    ''' <param name="fieldDen">Denominación del campo</param>
    ''' <remarks>Llamada desde EditColumnYConfiguracion y AddColumnYConfiguracion. Máx. 0,1 seg.</remarks>
    Private Sub anyadirAPanelConfiguracion(ByVal fieldName As String, ByVal fieldDen As String)
        'Lo añadimos siempre que no esté ya presente
        If tblPnlConfig.FindControl("chk" & fieldName) Is Nothing Then
            Dim numFilas As Double = tblPnlConfig.Rows.Count
            Dim tbcCelda As New HtmlTableCell
            Dim chkCampo As New CheckBox
            chkCampo.ID = "chk" & fieldName
            chkCampo.Text = fieldDen
            chkCampo.InputAttributes.Add("colName", fieldName)
            chkCampo.Attributes.Add("onclick", "CheckConfiguracion(this)")
            chkCampo.TextAlign = TextAlign.Right
            tbcCelda.Controls.Add(chkCampo)

            Dim iMaxFilas As Integer = 14
            'Para explicar el 14:
            'Al rellenar el panel de configuración con los campos de la grid hay que tener en cuenta que, 
            'aunque haya más de 4 partidas presupuestarias PRES0, sólo se va a guardar la configuración de las 4 primeras
            'por lo que el máximo de campos configurables posible será 27 pero, en el caso improbable de que haya más, tb los mostraremos

            Dim iNumCeldas As Double = 0
            Dim iFilaDeNuevaCelda As Integer = 0
            For Each ofila As HtmlTableRow In tblPnlConfig.Rows
                For Each celda As HtmlTableCell In ofila.Cells
                    iNumCeldas += 1
                Next
            Next
            iFilaDeNuevaCelda = (iNumCeldas Mod iMaxFilas) 'La nueva fila debería ser esto + 1 pero como tblPnlConfig.Rows(index) es de base cero, no hace falta sumar uno
            Dim iColSpan As Integer = CInt(iNumCeldas / iMaxFilas)
            If (iNumCeldas / iMaxFilas) Mod CInt(iNumCeldas / iMaxFilas) > 0 Then
                iColSpan = CInt(iNumCeldas / iMaxFilas) + 1
            End If

            If iColSpan > 3 Then
                'El ancho del panel es 480px y está pensado para tres columnas.
                'Para cada columna nueva, añadimos 160px pero antes comprobamos si hay al menos 120 px por columna, para no añadir ancho innecesariamente
                Dim iAnchoMaximo As Integer = 1050
                For i = 4 To iColSpan
                    If ((pnlConfig.Width.Value / i) < 160) AndAlso (pnlConfig.Width.Value < iAnchoMaximo) Then
                        pnlConfig.Width = Unit.Pixel(pnlConfig.Width.Value + 160)
                    End If
                Next
            End If

            If numFilas < iMaxFilas Then
                Dim tbrfila As New HtmlTableRow
                tbrfila.Cells.Add(tbcCelda)
                tblPnlConfig.Rows.Add(tbrfila)
            Else
                Dim fila As HtmlTableRow = tblPnlConfig.Rows(iFilaDeNuevaCelda)
                fila.Cells.Add(tbcCelda)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Funcion que pasandole las categorias devuelva la rama de denominaciones de esas categorias
    ''' </summary>
    ''' <param name="sCat1">Codigo Categoria nivel 1</param>
    ''' <param name="sCat2">Codigo Categoria nivel 2</param>
    ''' <param name="sCat3">Codigo Categoria nivel 3</param>
    ''' <param name="sCat4">Codigo Categoria nivel 4</param>
    ''' <param name="sCat5">Codigo Categoria nivel 5</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function DevolverRamaCategorias(ByVal sCat1 As String, ByVal sCat2 As String, ByVal sCat3 As String, ByVal sCat4 As String, ByVal sCat5 As String) As List(Of String)
        Dim dsCategorias As DataSet = TodasCategorias
        Dim codCat1 As String
        Dim codCat2 As String
        Dim codCat3 As String
        Dim codCat4 As String
        Dim codCat5 As String
        Dim DenCat1 As String
        Dim DenCat2 As String
        Dim DenCat3 As String
        Dim DenCat4 As String
        Dim DenCat5 As String
        Dim returnList As New List(Of String)
        Dim sDenRamaCategorias As String
        Dim sCodCategoria As String
        If dsCategorias.Tables(0).Rows.Count > 0 Then

            Dim drCat1() As DataRow = dsCategorias.Tables(0).Select("ID='" & sCat1 & "'")
            If drCat1.Length > 0 Then
                codCat1 = DBNullToStr(drCat1(0)("COD"))
                DenCat1 = DBNullToStr(drCat1(0)("DEN"))
                sDenRamaCategorias = "(" & codCat1 & ") " & DenCat1
                sCodCategoria = codCat1
            End If

            Dim drCat2() As DataRow = dsCategorias.Tables(0).Select("ID='" & sCat1 & "_" & sCat2 & "'")
            If drCat1.Length > 0 AndAlso drCat2.Length > 0 Then
                codCat2 = DBNullToStr(drCat2(0)("COD"))
                DenCat2 = DBNullToStr(drCat2(0)("DEN"))
                sDenRamaCategorias += "-(" & codCat2 & ") " & DenCat2
                sCodCategoria = codCat2
            End If

            Dim drCat3() As DataRow = dsCategorias.Tables(0).Select("ID='" & sCat1 & "_" & sCat2 & "_" & sCat3 & "'")
            If drCat1.Length > 0 AndAlso drCat2.Length > 0 AndAlso drCat3.Length > 0 Then
                codCat3 = DBNullToStr(drCat3(0)("COD"))
                DenCat3 = DBNullToStr(drCat3(0)("DEN"))
                sDenRamaCategorias += "-(" & codCat3 & ") " & DenCat3
                sCodCategoria = codCat3
            End If

            Dim drCat4() As DataRow = dsCategorias.Tables(0).Select("ID='" & sCat1 & "_" & sCat2 & "_" & sCat3 & "_" & sCat4 & "'")
            If drCat1.Length > 0 AndAlso drCat2.Length > 0 AndAlso drCat3.Length > 0 AndAlso drCat4.Length > 0 Then
                codCat4 = DBNullToStr(drCat4(0)("COD"))
                DenCat4 = DBNullToStr(drCat4(0)("DEN"))
                sDenRamaCategorias += "-(" & codCat4 & ") " & DenCat4
                sCodCategoria = codCat4
            End If

            Dim drCat5() As DataRow = dsCategorias.Tables(0).Select("ID='" & sCat1 & "_" & sCat2 & "_" & sCat3 & "_" & sCat4 & "_" & sCat5 & "'")
            If drCat1.Length > 0 AndAlso drCat2.Length > 0 AndAlso drCat3.Length > 0 AndAlso drCat4.Length > 0 AndAlso drCat5.Length > 0 Then
                codCat4 = DBNullToStr(drCat4(0)("COD"))
                codCat5 = DBNullToStr(drCat5(0)("COD"))
                DenCat5 = DBNullToStr(drCat5(0)("DEN"))
                sDenRamaCategorias += "-(" & codCat5 & ") " & DenCat5
                sCodCategoria = codCat5
            End If

            returnList.Add(sCodCategoria)
            returnList.Add(sDenRamaCategorias)

            Return returnList
        End If
    End Function

    ''' Revisado por: blp. Fecha: 22/12/2011
    ''' <summary>
    ''' Devuelve la relación entre:
    ''' campos del grid, del dataset de entregas y el datatable con los datos de ancho, visibilidad y posicion
    ''' Aquí se establecen los valores de visibilidad y ancho por defecto. Si hay valores guardados, se sobreescriben posteriormente
    ''' </summary>
    ''' <returns>Lista de Campos con la relación</returns>
    ''' <remarks>Llamada desde confAnchosVisibilidadYPosicion. Máx. 0,1 seg.</remarks>
    Private ReadOnly Property relacionarEntregasyConfiguracion() As List(Of Campos)
        Get
            Dim relacionEntregasyConfiguracion As New List(Of Campos)
            'Cada campo consta de la info:
            '   Campo del stored que carga el whdg (ej: "RETRASADO")
            '   Ancho por defecto del campo
            '   Tipo de Campo
            '   ID del campo a usar cuando guardemos la configuración
            relacionEntregasyConfiguracion.Add(New Campos("PLAN_ENTREGA", TipoCampoRecepciones.Generico, "PLAN_ENTREGA", 25, True, 0))
            relacionEntregasyConfiguracion.Add(New Campos("FECHARECEP", TipoCampoRecepciones.Generico, "FECHARECEP", 60, True, 1))
            relacionEntregasyConfiguracion.Add(New Campos("ANYOPEDIDOORDEN", TipoCampoRecepciones.Generico, "ANYOPEDIDOORDEN", 80, True, 2))
            relacionEntregasyConfiguracion.Add(New Campos("NUM_PED_ERP", TipoCampoRecepciones.Generico, "NUM_PED_ERP", 70, MostrarNumeroPedidoERP, 3))
            relacionEntregasyConfiguracion.Add(New Campos("ARTDEN", TipoCampoRecepciones.Generico, "ARTDEN", 80, True, 4))
            relacionEntregasyConfiguracion.Add(New Campos("NUMLINEA", TipoCampoRecepciones.Generico, "NUMLINEA", 20, True, 5))
            relacionEntregasyConfiguracion.Add(New Campos("DENEMPRESA", TipoCampoRecepciones.Generico, "DENEMPRESA", 60, True, 6))
            relacionEntregasyConfiguracion.Add(New Campos("ALBARAN", TipoCampoRecepciones.Generico, "ALBARAN", 50, True, 7))
            relacionEntregasyConfiguracion.Add(New Campos("PREC_UP", TipoCampoRecepciones.Generico, "PREC_UP", 50, True, 8))
            relacionEntregasyConfiguracion.Add(New Campos("CANTLINEAPEDIDATOTAL", TipoCampoRecepciones.Generico, "CANTLINEAPEDIDATOTAL", 50, True, 9))
            relacionEntregasyConfiguracion.Add(New Campos("CANTLINEARECIBIDAPARCIAL", TipoCampoRecepciones.Generico, "CANTLINEARECIBIDAPARCIAL", 50, True, 10))
            relacionEntregasyConfiguracion.Add(New Campos("CANTLINEAPENDIENTETOTAL", TipoCampoRecepciones.Generico, "CANTLINEAPENDIENTETOTAL", 50, True, 11))
            relacionEntregasyConfiguracion.Add(New Campos("CANT_PLANIF", TipoCampoRecepciones.Generico, "CANT_PLANIF", 50, True, 12))

            relacionEntregasyConfiguracion.Add(New Campos("IMPORTELINEATOTAL", TipoCampoRecepciones.Generico, "IMPORTELINEATOTAL", 50, True, 13))
            relacionEntregasyConfiguracion.Add(New Campos("UNICOD", TipoCampoRecepciones.Generico, "UNICOD", 40, True, 14))
            relacionEntregasyConfiguracion.Add(New Campos("FECENTREGASOLICITADA", TipoCampoRecepciones.Generico, "FECENTREGASOLICITADA", 60, True, 15))
            relacionEntregasyConfiguracion.Add(New Campos("FECENTREGAINDICADAPROVE", TipoCampoRecepciones.Generico, "FECENTREGAINDICADAPROVE", 60, True, 16))
            relacionEntregasyConfiguracion.Add(New Campos("APROVISIONADOR", TipoCampoRecepciones.Generico, "APROVISIONADOR", 40, True, 17))
            relacionEntregasyConfiguracion.Add(New Campos("APROVISIONADORCOD", TipoCampoRecepciones.Generico, "APROVISIONADORCOD", 40, True, -1))
            relacionEntregasyConfiguracion.Add(New Campos("PROVECOD", TipoCampoRecepciones.Generico, "PROVECOD", 50, True, -1))
            relacionEntregasyConfiguracion.Add(New Campos("PROVEDEN", TipoCampoRecepciones.Generico, "PROVEDEN", 70, True, 18))
            relacionEntregasyConfiguracion.Add(New Campos("RECEPTOR", TipoCampoRecepciones.Generico, "DENRECEPTOR", 40, True, 19))
            relacionEntregasyConfiguracion.Add(New Campos("DESTDEN", TipoCampoRecepciones.Generico, "DESTDEN", 80, True, 20))

            'CENTROS DE COSTE Y PARTIDAS
            If Acceso.gbAccesoFSSM Then
                relacionEntregasyConfiguracion.Add(New Campos("CCDEN", TipoCampoRecepciones.CentroCoste, "CCDEN", 80, True, 21))
                Dim queryColumnNames = PRES0Cods(TipoOrigenPartidaPres.ListadoEntregas)
                If queryColumnNames.Any Then
                    For Each columnName As String In queryColumnNames
                        relacionEntregasyConfiguracion.Add(New Campos("NIVEL_DEN_" & columnName, TipoCampoRecepciones.PartidaPresupuestaria, columnName, 100, True, 22))
                    Next
                End If
            End If

            relacionEntregasyConfiguracion.Add(New Campos("ORDENEST", TipoCampoRecepciones.Generico, "ORDENEST", 100, True, -1))
            relacionEntregasyConfiguracion.Add(New Campos("TIPOPEDIDO", TipoCampoRecepciones.Generico, "TIPOPEDIDO", 40, True, -1))

            relacionEntregasyConfiguracion.Add(New Campos("IMPORTE", TipoCampoRecepciones.Generico, "IMPORTE", 50, True, -1))
            relacionEntregasyConfiguracion.Add(New Campos("NUM_ERP_RECEP", TipoCampoRecepciones.Generico, "NUM_ERP_RECEP", 70, True, -1))
            relacionEntregasyConfiguracion.Add(New Campos("NUMPEDPROVE", TipoCampoRecepciones.Generico, "NUMPEDPROVE", 60, True, -1))
            relacionEntregasyConfiguracion.Add(New Campos("FECHAEMISION", TipoCampoRecepciones.Generico, "FECHAEMISION", 60, True, -1))

            relacionEntregasyConfiguracion.Add(New Campos("IMPORTELINEARECIBIDOPARCIAL", TipoCampoRecepciones.Generico, "IMPORTELINEARECIBIDOPARCIAL", 50, True, -1))
            relacionEntregasyConfiguracion.Add(New Campos("IMPORTELINEAPENDIENTETOTAL", TipoCampoRecepciones.Generico, "IMPORTELINEAPENDIENTETOTAL", 50, True, -1))
            relacionEntregasyConfiguracion.Add(New Campos("COD_PROVE_ERP", TipoCampoRecepciones.Generico, "COD_PROVE_ERP", 40, True, -1))

            relacionEntregasyConfiguracion.Add(New Campos("PEDIDOOBS", TipoCampoRecepciones.Generico, "PEDIDOOBS", 40, True, -1))
            relacionEntregasyConfiguracion.Add(New Campos("ORDENOBS", TipoCampoRecepciones.Generico, "ORDENOBS", 40, True, -1))
            relacionEntregasyConfiguracion.Add(New Campos("LINEA_PEDIDO_OBS", TipoCampoRecepciones.Generico, "LINEA_PEDIDO_OBS", 40, True, -1))
            relacionEntregasyConfiguracion.Add(New Campos("TIENEADJ", TipoCampoRecepciones.Generico, "TIENEADJ", 40, True, -1))
            relacionEntregasyConfiguracion.Add(New Campos("CATEGORIA", TipoCampoRecepciones.Generico, "CATEGORIA", 40, True, -1))
            relacionEntregasyConfiguracion.Add(New Campos("IM_RECEPAUTO", TipoCampoRecepciones.CentroCoste, "IM_RECEPAUTO", 20, False, -1))
            relacionEntregasyConfiguracion.Add(New Campos("LINEAPEDID", TipoCampoRecepciones.Generico, "LINEAPEDID", 25, False, -1))

            relacionEntregasyConfiguracion.Add(New Campos("FEC_ACEPT", TipoCampoRecepciones.Generico, "FEC_ACEPT", 25, True, -1))
            relacionEntregasyConfiguracion.Add(New Campos("OBS_PROV", TipoCampoRecepciones.Generico, "OBS_PROV", 40, True, -1))
            relacionEntregasyConfiguracion.Add(New Campos("TIPO", TipoCampoRecepciones.Generico, "TIPO", 40, True, -1))

            relacionEntregasyConfiguracion.Add(New Campos("DESTCOD", TipoCampoRecepciones.Generico, "DESTCOD", 40, True, -1))
            relacionEntregasyConfiguracion.Add(New Campos("PORCEN_DESVIO", TipoCampoRecepciones.Generico, "PORCEN_DESVIO", 40, True, -1))

            relacionEntregasyConfiguracion.Add(New Campos("PED_RECEP_ADJUNTOS", TipoCampoRecepciones.Generico, "PED_RECEP_ADJUNTOS", 40, False, -1))
            relacionEntregasyConfiguracion.Add(New Campos("TIPORECEPCION", TipoCampoRecepciones.Generico, "TIPORECEPCION", 40, False, -1))
            If FSNServer.TipoAcceso.gbMostrarFechaContable Then
                relacionEntregasyConfiguracion.Add(New Campos("FEC_CONTABLE", TipoCampoRecepciones.Generico, "FEC_CONTABLE", 60, True, 23))
            End If

            Return relacionEntregasyConfiguracion
        End Get
    End Property

    ''' <summary>
    ''' Estructura compuesta por 5 elementos q nos va a permitir almacenar:
    ''' 1. El nombre del campo en el datatable de entregas (DsetPedidos.Tables("ORDENES"))
    ''' 2. Tipo de campo (Campo personalizado, Centro de Coste, Partida Presupuestaria o Genérico para todos los demás)
    ''' 3. ID que usaremos para guardar el campo en la tabla EP_CONF_VISOR_RECEPCIONES
    ''' 4. Si es visible 
    ''' 5. Su anchura.
    ''' 6. Su posición
    ''' </summary>
    Private Structure Campos
        Public CampoEntrega As String
        Public TipoCampoRecepciones As TipoCampoRecepciones
        Public CampoEntregaIDGuardado As String
        Public Visible As Boolean
        Public Width As Double
        Public Position As Integer

        ''' Revisado por: blp. Fecha: 27/08/2012
        ''' <summary>
        ''' Método para crear una nueva instancia de la estructura Campos
        ''' </summary>
        ''' <param name="sCampoEntrega">nombre del campo en el datatable de entregas (DatasetEntregas.Tables("ORDENES"))</param>
        ''' <param name="iTipoCampoRecepciones">Tipo de campo</param>
        ''' <param name="sCampoEntregaIDGuardado">Nombre del campo con el que se guardarán los datos en la tabla que contiene los datos de configuración</param>
        ''' <remarks>Llamada desde New. Máx. 0,1 seg.</remarks>
        Sub New(ByVal sCampoEntrega As String, ByVal iTipoCampoRecepciones As TipoCampoRecepciones, ByVal sCampoEntregaIDGuardado As String)
            Me.CampoEntrega = sCampoEntrega
            Me.TipoCampoRecepciones = iTipoCampoRecepciones
            Me.CampoEntregaIDGuardado = sCampoEntregaIDGuardado
        End Sub

        ''' Revisado por: blp. Fecha: 27/08/2012
        ''' <summary>
        ''' Método para crear una nueva instancia de la estructura Campos
        ''' </summary>
        ''' <param name="sCampoEntrega">nombre del campo en el datatable de entregas (DatasetEntregas.Tables("ORDENES"))</param>
        ''' <param name="iTipoCampoRecepciones">Tipo de campo</param>
        ''' <param name="sCampoEntregaIDGuardado">Nombre del campo con el que se guardarán los datos en la tabla que contiene los datos de configuración</param>
        ''' <param name="dblWidth">ancho del campo</param>
        ''' <remarks>Llamada desde New. Máx. 0,1 seg.</remarks>
        Sub New(ByVal sCampoEntrega As String, ByVal iTipoCampoRecepciones As TipoCampoRecepciones, ByVal sCampoEntregaIDGuardado As String, ByVal dblWidth As Double)
            Me.CampoEntrega = sCampoEntrega
            Me.TipoCampoRecepciones = iTipoCampoRecepciones
            Me.CampoEntregaIDGuardado = sCampoEntregaIDGuardado
            Me.Width = dblWidth
        End Sub

        ''' Revisado por: blp. Fecha: 27/08/2012
        ''' <summary>
        ''' Método para crear una nueva instancia de la estructura Campos
        ''' </summary>
        ''' <param name="sCampoEntrega">nombre del campo en el datatable de entregas (DatasetEntregas.Tables("ORDENES"))</param>
        ''' <param name="iTipoCampoRecepciones">Tipo de campo</param>
        ''' <param name="sCampoEntregaIDGuardado">Nombre del campo con el que se guardarán los datos en la tabla que contiene los datos de configuración</param>
        ''' <param name="dblWidth">ancho del campo</param>
        ''' <param name="bVisible">Es visible</param>
        ''' <remarks>Llamada desde New. Máx. 0,1 seg.</remarks>
        Sub New(ByVal sCampoEntrega As String, ByVal iTipoCampoRecepciones As TipoCampoRecepciones, ByVal sCampoEntregaIDGuardado As String, ByVal dblWidth As Double, ByVal bVisible As Boolean)
            CampoEntrega = sCampoEntrega
            TipoCampoRecepciones = iTipoCampoRecepciones
            CampoEntregaIDGuardado = sCampoEntregaIDGuardado
            Width = dblWidth
            Visible = bVisible
        End Sub

        ''' Revisado por: blp. Fecha: 27/08/2012
        ''' <summary>
        ''' Método para crear una nueva instancia de la estructura Campos
        ''' </summary>
        ''' <param name="sCampoEntrega">nombre del campo en el datatable de entregas (DatasetEntregas.Tables("ORDENES"))</param>
        ''' <param name="iTipoCampoRecepciones">Tipo de campo</param>
        ''' <param name="sCampoEntregaIDGuardado">Nombre del campo con el que se guardarán los datos en la tabla que contiene los datos de configuración</param>
        ''' <param name="dblWidth">ancho del campo</param>
        ''' <param name="bVisible">Es visible</param>
        ''' <param name="iPosition">posición del campo</param>
        ''' <remarks>Llamada desde New. Máx. 0,1 seg.</remarks>
        Sub New(ByVal sCampoEntrega As String, ByVal iTipoCampoRecepciones As TipoCampoRecepciones, ByVal sCampoEntregaIDGuardado As String, ByVal dblWidth As Double, ByVal bVisible As Boolean, ByVal iPosition As Integer)
            CampoEntrega = sCampoEntrega
            TipoCampoRecepciones = iTipoCampoRecepciones
            CampoEntregaIDGuardado = sCampoEntregaIDGuardado
            Width = dblWidth
            Visible = bVisible
            Position = iPosition
        End Sub
    End Structure

    ''' Revisado por: blp. Fecha: 23/08/2012
    ''' <summary>
    ''' Devuelve un listado con los nombre de las partidas presupuestarias definidas
    ''' </summary>
    ''' <param name="origenPartida">
    ''' Origen de datos desde el que vamos a recuperar las partidas. 
    '''    ConfiguracionSM: Devolvemos las partidas presentes en la configuración SM (columnas de DatasetEntregas.Tables("PARTIDAS_DEN"))
    '''    ListadoEntregas: Devolvemos las partidas presentes en el listado de Entregas (lo recuperamos de DatasetEntregas.Tables("ORDENES"))
    ''' </param>
    Private ReadOnly Property PRES0Cods(ByVal origenPartida As TipoOrigenPartidaPres) As IEnumerable(Of String)
        Get
            If origenPartida = TipoOrigenPartidaPres.ConfiguracionSM Then
                Dim query = From columnas As DataColumn In DsetPedidos.Tables("PARTIDAS_DEN").Columns _
                       Select columnas.ColumnName Distinct
                Return query
            Else
                Dim query = From columnas As DataColumn In DsetPedidos.Tables("ORDENES").Columns _
                                       Where columnas.ColumnName.StartsWith("NIVEL_DEN_") _
                                       Select columnas.ColumnName.ToString.Replace("NIVEL_DEN_", "") Distinct
                Return query
            End If
        End Get
    End Property

    ''' <summary>
    ''' Tipos de origen desde los que se pueden recuperar partidas
    ''' </summary>
    Public Enum TipoOrigenPartidaPres
        ListadoEntregas = 0
        ConfiguracionSM = 1
    End Enum

    ''' Revisado por: blp. Fecha: 23/08/2012
    ''' <summary>
    ''' Método que se lanza en el evento InitializeRow (cuando se enlazan los datos del origen de datos con la línea)
    ''' En él vamos a configurar la columna de entrega con retrasos (mostrar la imagen de retraso en las entregas retrasadas)
    ''' </summary>
    ''' <param name="sender">Control que lanza el evento (whdgEntregas)</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. Máx 0,1 seg</remarks>
    Private Sub whdgEntregas_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgEntregas.InitializeRow
        Dim oData As DataRow = e.Row.DataItem.Item.Row
        Dim txtNoAplica As String = "&lt;" & Me.Textos(268) & "&gt;"
        If whdgEntregas.GridView.Columns.FromKey("Key_ALBARAN") IsNot Nothing Then
            Dim Index As Integer = whdgEntregas.GridView.Columns.FromKey("Key_ALBARAN").Index()
            If Me.Acceso.g_bAccesoFSFA AndAlso DBNullToInteger(oData("IM_BLOQUEO")) > 0 Then
                Dim UrlImage As String = "../../Images/stopx40.png"
                Dim sObservaciones As String = DBNullToStr(oData("IM_BLOQUEO_OBS"))
                Dim sHTMLImgBloqueo As String = "<img id='img_IM_BLOQUEO' class='img_IM_BLOQUEO' name='img_IM_BLOQUEO' src='" & UrlImage & "' style='text-align:center;margin-left:5px;"
                If Not String.IsNullOrEmpty(sObservaciones) Then
                    sHTMLImgBloqueo += "cursor:pointer;"
                End If
                sHTMLImgBloqueo += "' observaciones='" & sObservaciones & "' />"
                e.Row.Items.Item(Index).Text += sHTMLImgBloqueo
                If Not String.IsNullOrEmpty(sObservaciones) Then
                    Dim sScript As String
                    sScript = "$(document).ready(function(){" & vbCrLf
                    sScript += "    $('#pnl_BLOQUEO_OBSERVACIONES').remove();" & vbCrLf
                    sScript += "    $('body').prepend('<div id=""pnl_BLOQUEO_OBSERVACIONES"" class=""pnl_BLOQUEO_OBSERVACIONES"" & vbCrLf style=""display:none;""></div>');" & vbCrLf
                    sScript += "    $('.img_IM_BLOQUEO').live('mouseover',function(event){" & vbCrLf
                    sScript += "        Ocultar_pnl_BLOQUEO_OBSERVACIONES();" & vbCrLf
                    sScript += "        $('#pnl_BLOQUEO_OBSERVACIONES').css('position', 'absolute');" & vbCrLf
                    sScript += "        $('#pnl_BLOQUEO_OBSERVACIONES').css('top', $(this).position().top + 25);" & vbCrLf
                    sScript += "        $('#pnl_BLOQUEO_OBSERVACIONES').css('left', $(this).position().left);" & vbCrLf
                    sScript += "        $('#pnl_BLOQUEO_OBSERVACIONES').text($(this).attr('observaciones'));" & vbCrLf
                    sScript += "        if($(this).attr('observaciones')!=''){" & vbCrLf
                    sScript += "        	$('#pnl_BLOQUEO_OBSERVACIONES').show('fast');" & vbCrLf
                    sScript += "        }" & vbCrLf
                    sScript += "        $('#pnl_BLOQUEO_OBSERVACIONES').focus();" & vbCrLf
                    sScript += "    });" & vbCrLf
                    sScript += "    $('.img_IM_BLOQUEO').live('mouseout',function(event){" & vbCrLf
                    sScript += "        Ocultar_pnl_BLOQUEO_OBSERVACIONES();" & vbCrLf
                    sScript += "    });" & vbCrLf
                    sScript += "});" & vbCrLf
                    sScript += "function Ocultar_pnl_BLOQUEO_OBSERVACIONES() {" & vbCrLf
                    sScript += "    $('#pnl_BLOQUEO_OBSERVACIONES').hide();" & vbCrLf
                    sScript += "}" & vbCrLf
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "JQUERY_BLOQUEO_OBSERVACIONES", sScript, True)
                End If
            End If
        End If

        e.Row.Items.FindItemByKey("Key_TIPOPEDIDO").Text = DBNullToStr(oData("TIPOPEDIDO_DEN"))
        e.Row.Items.FindItemByKey("Key_PROVEDEN").Text = If(FSNUser.MostrarCodProve, DBNullToStr(oData("PROVECOD")) & " - ", "") & DBNullToStr(oData("PROVEDEN"))
        e.Row.Items.FindItemByKey("Key_ARTDEN").Text = "<a id='IM_CODART' onclick='AbrirDetalleArticulo(" & DBNullToStr(oData("LINEAPEDID")) & ")'>" & IIf(DBNullToStr(oData("ARTDEN")) <> "", DBNullToStr(oData("ARTDEN")), "") & "</a>"
        e.Row.Items.FindItemByKey("Key_ARTDEN").Tooltip = IIf(DBNullToStr(oData("ARTDEN")) <> "", DBNullToStr(oData("ARTDEN")), "")
        If DBNullToStr(oData("CAT1")) <> "" Then
            Dim listCategorias As List(Of String) = DevolverRamaCategorias(DBNullToStr(oData("CAT1")), DBNullToStr(oData("CAT2")), DBNullToStr(oData("CAT3")), DBNullToStr(oData("CAT4")), DBNullToStr(oData("CAT5")))
            e.Row.Items.FindItemByKey("Key_CATEGORIA").Text = listCategorias.Item(0)
            e.Row.Items.FindItemByKey("Key_CATEGORIA").Tooltip = listCategorias.Item(1)
        End If
        e.Row.Items.FindItemByKey("Key_UNICOD").Tooltip = DBNullToStr(oData("UNIDEN"))
        If DBNullToStr(oData("TIENEADJ")) = "1" Then
            e.Row.Items.FindItemByKey("Key_TIENEADJ").Text = " <img id='img_IM_ADJUNTO' onclick='AbrirAdjuntos(" & DBNullToStr(oData("ORDENID")) & "," & DBNullToStr(oData("LINEAPEDID")) & ", false)'  src='" & ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/adjuntos_cab_small.gif' style='text-align:center;cursor:pointer;' />"
        Else
            e.Row.Items.FindItemByKey("Key_TIENEADJ").Text = ""
        End If
        If DBNullToStr(oData("PED_RECEP_ADJUNTOS")).Length > 0 Then
            e.Row.Items.FindItemByKey("Key_PED_RECEP_ADJUNTOS").Text = " <span onclick='AbrirAdjuntos(" & DBNullToStr(oData("IDPED_RECEP")) & "," & DBNullToStr(oData("IDPED_RECEP")) & ", true)'  style='text-align:center;cursor:pointer;'>" & DBNullToStr(oData("PED_RECEP_ADJUNTOS")) & "</span>"
        Else
            e.Row.Items.FindItemByKey("Key_PED_RECEP_ADJUNTOS").Text = ""
        End If

        If DBNullToInteger(oData("PLAN_ENTREGA")) <> 0 Then
            'Si tiene plan de entrega se mirara si es manual o automatico
            If DBNullToStr(oData("IM_RECEPAUTO")) = "1" Then
                e.Row.Items.FindItemByKey("Key_PLAN_ENTREGA").Text = " <img id='img_IM_RECEPAUTO'  src='" & System.Configuration.ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/plan_auto.png' style='text-align:center;' />"
            Else
                e.Row.Items.FindItemByKey("Key_PLAN_ENTREGA").Text = " <img id='img_IM_RECEPAUTO'  src='" & System.Configuration.ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/plan_manual.png' style='text-align:center;' />"
            End If
        Else
            e.Row.Items.FindItemByKey("Key_PLAN_ENTREGA").Text = String.Empty
        End If
        If whdgEntregas.GridView.Columns.FromKey("Key_ORDENEST") IsNot Nothing Then
            Dim Index As Integer = whdgEntregas.GridView.Columns.FromKey("Key_ORDENEST").Index()
            Dim sEstado As String = String.Empty
            Select Case CType(DBNullToStr(oData("ORDENEST")), TipoEstadoOrdenEntrega)
                Case TipoEstadoOrdenEntrega.EmitidoAlProveedor
                    sEstado = Textos(119) 'Emitido al proveedor
                Case TipoEstadoOrdenEntrega.AceptadoPorProveedor
                    sEstado = Textos(24) 'Pedido Aceptado por el proveedor
                Case TipoEstadoOrdenEntrega.EnCamino
                    sEstado = Textos(25) 'En camino
                Case TipoEstadoOrdenEntrega.EnRecepcion
                    sEstado = Textos(26) 'Recibido parcialmente
                Case TipoEstadoOrdenEntrega.RecibidoYCerrado
                    sEstado = Textos(27) 'Cerrado
            End Select
            e.Row.Items.Item(Index).Text = sEstado
        End If
        If Me.Acceso.gbAccesoFSSM Then
            e.Row.Items.FindItemByKey("Key_CCDEN").Text = DBNullToStr(oData("CCDEN"))
        End If
        Dim Tipo_Index As Integer = whdgEntregas.GridView.Columns.FromKey("Key_TIPO").Index()
        Dim sTipo As String = String.Empty
        Select Case DBNullToInteger(oData("TIPO"))
            Case 0
                sTipo = Textos(129) 'Pedidos negociados
            Case 2
                sTipo = Textos(131) 'Pedidos directos
            Case 1
                If DBNullToStr(oData("ARTCOD")) = "" Then
                    sTipo = Textos(255) 'Pedidos de catálogo libres
                Else
                    sTipo = Textos(128) 'Pedidos de catálogo negociados
                End If
        End Select
        e.Row.Items.Item(Tipo_Index).Text = sTipo

        If oData("TIPORECEPCION") = 1 Then
            e.Row.Items.FindItemByKey("Key_CANTLINEARECIBIDAPARCIAL").Text = txtNoAplica
            e.Row.Items.FindItemByKey("Key_CANTLINEARECIBIDAPARCIAL").CssClass = "apagado"
            e.Row.Items.FindItemByKey("Key_CANTLINEAPEDIDATOTAL").Text = txtNoAplica
            e.Row.Items.FindItemByKey("Key_CANTLINEAPEDIDATOTAL").CssClass = "apagado"
            e.Row.Items.FindItemByKey("Key_CANTLINEAPENDIENTETOTAL").Text = txtNoAplica
            e.Row.Items.FindItemByKey("Key_CANTLINEAPENDIENTETOTAL").CssClass = "apagado"
            e.Row.Items.FindItemByKey("Key_PREC_UP").Text = txtNoAplica
            e.Row.Items.FindItemByKey("Key_PREC_UP").CssClass = "apagado"
            e.Row.Items.FindItemByKey("Key_UNICOD").Text = oData("MONCOD")
        Else

        End If
    End Sub

#End Region

#Region "TipoCampoRecepciones"

    <Serializable()> _
    Public Enum TipoCampoRecepciones As Integer
        Generico = 1
        CampoPersonalizado = 2
        CentroCoste = 3
        PartidaPresupuestaria = 4
    End Enum
#End Region


#Region "Unidades"
    ''' <summary>
    ''' Propiedad que recupera el conjunto de las unidades de Pedido para su posterior uso en la página
    ''' </summary>
    Private ReadOnly Property TodasLasUnidades() As CUnidadesPedido
        Get
            Dim oUnidadesPedido As CUnidadesPedido = Me.FSNServer.Get_Object(GetType(FSNServer.CUnidadesPedido))
            If HttpContext.Current.Cache("TodasLasUnidades" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                oUnidadesPedido.CargarTodasLasUnidades(Me.Usuario.Idioma)
                Me.InsertarEnCache("TodasLasUnidades" & Me.Usuario.Idioma.ToString(), oUnidadesPedido)
            Else
                oUnidadesPedido = HttpContext.Current.Cache("TodasLasUnidades" & Me.Usuario.Idioma.ToString())
            End If
            Return oUnidadesPedido
        End Get
    End Property
    ''' <summary>
    ''' Método que recupera el número de decimales para una unidad de pedido dada y configura la presentación de los datos en los controles webnumericEdit
    ''' </summary>
    ''' <param name="unidad">Código de la unidad de pedido</param>
    ''' <param name="wNumericEdit">Control webNumericEdit a configurar</param>
    ''' <remarks>Llamada desde: la página, allí donde se configure un control webNumericEdit. Tiempo máximo: inferior a 0,3</remarks>
    Private Sub configurarDecimales(ByVal unidad As Object, ByRef wNumericEdit As Infragistics.Web.UI.EditorControls.WebNumericEditor)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_RecepcionPedidos
        Dim oUnidadPedido As FSNServer.CUnidadPedido = Nothing
        If Not DBNullToSomething(unidad) = Nothing Then
            unidad = Trim(unidad.ToString)
            oUnidadPedido = TodasLasUnidades.Item(unidad)
            oUnidadPedido.AsignarFormatoaUnidadPedido(Me.Usuario.NumberFormat)
        End If
        If Not oUnidadPedido Is Nothing Then
            Dim ci As System.Globalization.CultureInfo = Threading.Thread.CurrentThread.CurrentCulture.Clone()
            ci.NumberFormat = oUnidadPedido.UnitFormat
            If oUnidadPedido.NumeroDeDecimales Is Nothing Then
                ci.NumberFormat.NumberDecimalDigits = 15
                wNumericEdit.MinDecimalPlaces = 0
                wNumericEdit.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Double
            ElseIf oUnidadPedido.NumeroDeDecimales > 0 Then
                wNumericEdit.MinDecimalPlaces = oUnidadPedido.NumeroDeDecimales
                wNumericEdit.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Double
            ElseIf oUnidadPedido.NumeroDeDecimales = 0 Then
                wNumericEdit.MinDecimalPlaces = oUnidadPedido.NumeroDeDecimales
                wNumericEdit.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Int
            End If
            wNumericEdit.Culture = ci
            wNumericEdit.ClientEvents.TextChanged = "limiteDecimalesTextChanged"
            wNumericEdit.Attributes.Add("numeroDecimales", wNumericEdit.Culture.NumberFormat.NumberDecimalDigits)
            wNumericEdit.Attributes.Add("separadorDecimales", wNumericEdit.Culture.NumberFormat.NumberDecimalSeparator)
            wNumericEdit.Attributes.Add("avisoDecimales", Textos(24) & " " & oUnidadPedido.Codigo & ":")
            wNumericEdit.Buttons.SpinOnArrowKeys = True
        End If
    End Sub
#End Region
#Region "Activos"
    ''' <summary>
    ''' Evento que se genera cuando se cambia el valor del texto del Combo de activos, validando El activo con el centro de coste seleccionado.
    ''' </summary>
    ''' <param name="sender">El propio textbox en el que se ha cambiado el texto</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automática desde la propia página
    ''' Tiempo máximo: 0,2 seg</remarks>
    Protected Sub tbActivos_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim txt As TextBox = CType(sender, TextBox)
        Dim dlActivo = TryCast(txt.NamingContainer, DataListItem)
        Dim fsnLnkActivo As FSNWebControls.FSNImageInfo
        If dlActivo IsNot Nothing Then _
            fsnLnkActivo = TryCast(dlActivo.FindControl("FSNLnkActivo"), FSNWebControls.FSNImageInfo)
        txt.Attributes("CODIGO") = String.Empty
        txt.BackColor = Nothing
        If Not String.IsNullOrEmpty(txt.Text) Then
            Dim dlItem As DataListItem = CType(txt.NamingContainer, DataListItem)
            Dim emp As Integer = CType(dlItem.Attributes("EMPRESA"), Integer)
            Dim CentroCoste As String = String.Empty
            CentroCoste = CType(sender, TextBox).Attributes("CENTROSM")

            Dim query = From act In TodosActivos _
                      Where act.Empresa = emp And act.Codigo.ToLower() = txt.Text.ToLower() _
                      And IIf(String.IsNullOrEmpty(CentroCoste), True, act.Centro_SM.ToString.ToUpper = CentroCoste.ToUpper) _
                      Select act Take 1

            If query.Count = 0 Then
                query = From act In TodosActivos _
                      Where act.Empresa = emp And String.Concat(act.Codigo, " - ", act.Denominacion).ToLower().Contains(txt.Text.ToLower()) _
                      And IIf(String.IsNullOrEmpty(CentroCoste), True, act.Centro_SM.ToString.ToUpper = CentroCoste.ToUpper) _
                      Select act Take 1
            End If
            If query.Count = 1 Then
                Dim a As FSNServer.Activo = query(0)
                txt.Text = a.Codigo & " - " & a.Denominacion
                txt.Attributes("CODIGO") = a.ID
                txt.BackColor = Drawing.Color.Green

                If fsnLnkActivo IsNot Nothing Then
                    fsnLnkActivo.ContextKey = a.Codigo
                    fsnLnkActivo.Visible = True
                End If
            Else
                txt.BackColor = Drawing.Color.Red
                If fsnLnkActivo IsNot Nothing Then _
                    fsnLnkActivo.Visible = False
            End If
            upRegistroRecepcion.Update()
        Else
            If fsnLnkActivo IsNot Nothing Then _
                fsnLnkActivo.Visible = False
        End If
    End Sub

    ''' Revisado por: blp. 20/05/2013
    ''' <summary>
    ''' Rellena el campo activo con el valor seleccionado en el buscador
    ''' </summary>
    ''' <param name="sender">control que lanzó el evento</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. 0,1s</remarks>
    Private Sub SMActivos_Aceptar(ByVal sender As Object, ByVal e As System.EventArgs) Handles SMActivos.Aceptar
        mpeActivos.Hide()
        If SMActivos.Activo IsNot Nothing AndAlso Not String.IsNullOrEmpty(SMActivos.Activo.Codigo) Then
            For Each li As DataListItem In dlLineasPedido.Items
                Dim img As ImageButton = TryCast(li.FindControl("ImgBtnBuscarActivoLinea"), ImageButton)
                If img IsNot Nothing AndAlso img.CommandArgument = SMActivos.Attributes("LINEAID") Then
                    Dim txt As TextBox = CType(li.FindControl("tbActivosLinea"), TextBox)
                    txt.Text = SMActivos.Activo.Codigo & " - " & SMActivos.Activo.Denominacion
                    txt.Attributes("CODIGO") = SMActivos.Activo.ID
                    txt.BackColor = Drawing.Color.Green
                    Dim fsnLnkActivo As FSNWebControls.FSNImageInfo = TryCast(li.FindControl("FSNLnkActivo"), FSNWebControls.FSNImageInfo)
                    If fsnLnkActivo IsNot Nothing Then
                        fsnLnkActivo.ContextKey = SMActivos.Activo.Codigo
                        fsnLnkActivo.Visible = True
                    End If
                    upRegistroRecepcion.Update()
                    Exit Sub
                End If
            Next
        End If
    End Sub
#End Region
#Region "Detalle Albarán"

    '''Revisado por: blp. Fecha: 19/10/2012
    ''' <summary>
    ''' Carga el detalle de un albarán. Son tres los criterios que determinan que las recepciones corresponden a un mismo albarán: El número de albarán, la fecha de la recepción y el código del proveedor 
    ''' </summary>
    ''' <param name="Albaran">Número de albarán</param>
    ''' <param name="FechaAlbaran">Fecha del albarán</param>
    ''' <param name="Prove">Código del proveedor</param>
    ''' <remarks>Llamada desde Recepcion.aspx.vb. Máximo 0,5 seg.</remarks>
    Private Sub CargarDetalleAlbaran(ByVal Albaran As String, ByVal FechaAlbaran As DateTime, ByVal Prove As String)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_RecepcionPedidos
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oOrdenes As FSNServer.COrdenes = FSNServer.Get_Object(GetType(FSNServer.COrdenes))
        Dim dsDetalleAlbaran As DataSet = oOrdenes.Obtener_DetalleAlbaran(Albaran, FechaAlbaran, Prove, FSNUser.Idioma)
        Dim drAlbaran As DataRow = dsDetalleAlbaran.Tables(0).Rows(0)
        'Guardamos los valores de albaran, prove y fecha en campos ocultos por si los necesitamos más adelante para refrescar los datos del panel
        hidAlb.Value = Albaran
        hidAlbFechaRecep.Value = FechaAlbaran
        hidAlbProve.Value = Prove

        lblAlbaran.Text = drAlbaran("ALBARAN")

        If drAlbaran("CORRECTO") = 1 Then
            lblProblemasRecepcion.Visible = False
        Else
            lblProblemasRecepcion.Visible = True
        End If

        If IsDBNull(drAlbaran("COMENT")) OrElse drAlbaran("COMENT") = String.Empty Then
            imgObservaciones.Visible = False
        Else
            imgObservaciones.Visible = True
            lblDetalleAlbaranComentarios.Text = drAlbaran("COMENT")
        End If

        lblDetalleAlbaranFecRecepcionValue.Text = FormatDate(drAlbaran("FECHA"), FSNUser.DateFormat)
        If HayIntegracionRecepcionEntradaOEntradaSalida Then
            lblDetalleAlbaranNumRecepcionERP.Text = drAlbaran("RECEPCIONERP").ToString
            lblDetalleAlbaranNumRecepcionERPValue.Text = drAlbaran("NUM_ERP").ToString
        Else
            lblDetalleAlbaranNumRecepcionERP.Visible = False
            lblDetalleAlbaranNumRecepcionERPValue.Visible = False
        End If
        lblDetalleAlbaranCodigoReceptorValue.Text = drAlbaran("COD").ToString
        lblDetalleAlbaranNombreReceptorValue.Text = drAlbaran("NOMBRE").ToString
        lblDetalleAlbaranCargoReceptorValue.Text = drAlbaran("CARGO").ToString
        lblDetalleAlbaranDepartamentoReceptorValue.Text = drAlbaran("DEPARTAMENTO").ToString
        lblDetalleAlbaranEmailReceptorValue.Text = drAlbaran("EMAIL").ToString
        lblDetalleAlbaranTelefonoReceptorValue.Text = drAlbaran("TFNO").ToString
        lblDetalleAlbaranFaxReceptorValue.Text = drAlbaran("FAX").ToString
        lblDetalleAlbaranOrganizacionReceptorValue.Text = drAlbaran("ORGANIZACION").ToString

        Dim AlbaranParaScript As String = modUtilidades.EncodeJsString(Albaran)
        btnAnularAlbaran.OnClientClick = "ConfirmarAnularAlbaran(" & AlbaranParaScript & ",'" & FechaAlbaran & "','" & Prove & "'); return false;"

        pnlAlbaranBloqueado.Style.Add("display", If(drAlbaran("IM_BLOQUEO"), "", "none"))
        imgAlbaranBloqueado.ToolTip = drAlbaran("IM_BLOQUEO_OBS").ToString

        If Not Me.Acceso.g_bAccesoFSFA Then
            btnBloquearFacturacion.Visible = False
            btnDesbloquearFacturacion.Visible = False
        Else
            'SI EL USUARIO PUEDE BLOQUEAR ALBARANES, MOSTRAMOS BOTÓN
            If Me.FSNUser.BloquearFacturacionAlbaranes Then
                btnDesbloquearFacturacion.OnClientClick = "BloquearFacturacion(false," & AlbaranParaScript & ",'" & drAlbaran("ORDEN_ID") & "'); return false;"
                btnBloquearFacturacion.OnClientClick = "BloquearFacturacion(true," & AlbaranParaScript & ",'" & drAlbaran("ORDEN_ID") & "'); return false;"

                If drAlbaran("IM_BLOQUEO") Then
                    btnBloquearFacturacion.Style.Add("display", "none")
                    btnDesbloquearFacturacion.Style.Add("display", "")
                Else
                    btnBloquearFacturacion.Style.Add("display", "")
                    btnDesbloquearFacturacion.Style.Add("display", "none")
                End If
            Else
                btnBloquearFacturacion.Visible = False
                btnDesbloquearFacturacion.Visible = False
            End If
        End If

        dlDetalleAlbaranLineasRecepcion.DataSource = dsDetalleAlbaran.Tables(1)
        'Miro si son todos de por importe para no sacar las columnas que no tienen datos
        Dim oRow As DataRow
        Dim numImporte As Integer = 0
        For Each oRow In dsDetalleAlbaran.Tables(1).Rows
            If oRow.Item("TIPORECEPCION") = 1 Then
                numImporte += 1
            End If
        Next
        Select Case numImporte
            Case 0 'Todas las lineas son de cantidad
                lblDetalleAlbaranDetalleLineasImportePedido.Visible = False
                lblDetalleAlbaranDetalleLineasImporteRecibido.Visible = False
                lblDetalleAlbaranDetalleLineasCantidadPedida.Visible = True
                lblDetalleAlbaranDetalleLineasCantidadRecibida.Visible = True
                lblDetalleAlbaranDetalleLineasPrecioUnitario.Visible = True
                lblDetalleAlbaranDetalleLineasImporte.Visible = True
            Case dsDetalleAlbaran.Tables(1).Rows.Count 'Todas las lineas son de importe
                lblDetalleAlbaranDetalleLineasCantidadPedida.Visible = False
                lblDetalleAlbaranDetalleLineasCantidadRecibida.Visible = False
                lblDetalleAlbaranDetalleLineasPrecioUnitario.Visible = False
                lblDetalleAlbaranDetalleLineasImporte.Visible = False
                lblDetalleAlbaranDetalleLineasImportePedido.Visible = True
                lblDetalleAlbaranDetalleLineasImporteRecibido.Visible = True
        End Select
        dlDetalleAlbaranLineasRecepcion.DataBind()

        upDetalleAlbaran.Update()
        mpeDetalleAlbaran.Show()
    End Sub

    ''' Revisado por: blp. Fecha: 19/10/2012
    ''' <summary>
    ''' Función que se lanza en el evento de carga del datalist con los detalles del albarán y sirve para llenar de contenido el mismo
    ''' </summary>
    ''' <param name="sender">Control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Evento de carga de cada línea del datalist. Máximo 0,3 seg.</remarks>
    Private Sub dlDetalleAlbaranLineasRecepcion_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlDetalleAlbaranLineasRecepcion.ItemDataBound
        Dim fila As DataRowView = CType(e.Item.DataItem, DataRowView)

        lblDetalleAlbaranDetalleLineasSalidaAlmacen.Visible = Acceso.gbMostrarRecepSalidaAlmacen

        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
            DirectCast(e.Item.FindControl("chkDetalleAlbaranLineasRecepcionSalidaAlmacen"), HtmlInputCheckBox).Visible = Acceso.gbMostrarRecepSalidaAlmacen

            DirectCast(e.Item.FindControl("lblArticulo"), Label).Text = fila.Item("DENART").ToString

            If FSNUser.AnularRecepciones And IsDBNull(fila.Item("FACTURA")) Then
                divCabeceraBotones.Attributes.CssStyle.Remove("display")
                DirectCast(e.Item.FindControl("divBotones"), Panel).Attributes.CssStyle.Remove("display")
                With DirectCast(e.Item.FindControl("btnGuardar"), FSNWebControls.FSNButton)
                    .Text = Textos(243) 'Guardar
                    .Visible = True
                    .OnClientClick = "ModificarLineaRecepcion(" & fila.Item("LINEARECEPID") & ",'" &
                        DirectCast(e.Item.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).ClientID & "','" &
                        .ClientID & "','" & DirectCast(e.Item.FindControl("lblImporte"), System.Web.UI.WebControls.Label).ClientID & "','" & fila.Item("PREC_UP").ToString & "'); return false;"
                End With
                If DirectCast(e.Item.DataItem, System.Data.DataRowView).DataView.Count > 1 Then
                    With DirectCast(e.Item.FindControl("btnAnularLineaRecepcion"), FSNWebControls.FSNButton)
                        .Text = Textos(244) 'Anular
                        .Visible = True
                        .OnClientClick = "AnularLineaRecep(" & fila.Item("LINEARECEPID") & "); return false;"
                    End With
                    divCabeceraBotones.Attributes.CssStyle.Item("width") = "165px"
                    DirectCast(e.Item.FindControl("divBotones"), Panel).Attributes.CssStyle.Item("width") = "165px"
                Else
                    DirectCast(e.Item.FindControl("btnAnularLineaRecepcion"), FSNWebControls.FSNButton).Visible = False
                    divCabeceraBotones.Attributes.CssStyle.Item("width") = "80px"
                    DirectCast(e.Item.FindControl("divBotones"), Panel).Attributes.CssStyle.Item("width") = "80px"
                End If
                DirectCast(e.Item.FindControl("lblCantidadRecibida"), Literal).Visible = False
                DirectCast(e.Item.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Visible = True
                DirectCast(e.Item.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Attributes.Add("btnGuardarID", DirectCast(e.Item.FindControl("btnGuardar"), FSNWebControls.FSNButton).ClientID)
                DirectCast(e.Item.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Attributes.Add("lblImporte", DirectCast(e.Item.FindControl("lblImporte"), System.Web.UI.WebControls.Label).ClientID)
                DirectCast(e.Item.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Attributes.Add("vdecimalfmt", Me.Usuario.NumberFormat.NumberDecimalSeparator)
                DirectCast(e.Item.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Attributes.Add("vthousanfmt", Me.Usuario.NumberFormat.NumberGroupSeparator)
                DirectCast(e.Item.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Attributes.Add("numdec", Me.Usuario.NumberFormat.NumberDecimalDigits)
                DirectCast(e.Item.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Attributes.Add("PrecioUni", fila.Item("PREC_UP").ToString)
                btnAnularAlbaran.Visible = True
            Else
                divCabeceraBotones.Attributes.CssStyle.Add("display", "none")
                DirectCast(e.Item.FindControl("divBotones"), Panel).Attributes.CssStyle.Add("display", "none")
                DirectCast(e.Item.FindControl("btnGuardar"), FSNWebControls.FSNButton).Visible = False
                DirectCast(e.Item.FindControl("btnAnularLineaRecepcion"), FSNWebControls.FSNButton).Visible = False
                DirectCast(e.Item.FindControl("lblCantidadRecibida"), Literal).Visible = True
                DirectCast(e.Item.FindControl("WNumEdCantidadRecibida"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Visible = False
                btnAnularAlbaran.Visible = False
            End If
            If fila.Item("TIPORECEPCION") = 0 Then
                DirectCast(e.Item.FindControl("lblImporte"), Label).Text = FSNLibrary.FormatNumber(fila.Item("IMPORTE"), Me.Usuario.NumberFormat)
                DirectCast(e.Item.FindControl("lblCantidadRecibida"), Literal).Text = FSNLibrary.FormatNumber(fila.Item("CANT"), Me.Usuario.NumberFormat)
                DirectCast(e.Item.FindControl("lblPrecioUnitario"), Literal).Text = FSNLibrary.FormatNumber(fila.Item("PREC_UP"), Me.Usuario.NumberFormat)
                DirectCast(e.Item.FindControl("lblCantidadPedida"), Literal).Text = FSNLibrary.FormatNumber(fila.Item("CANT_PED"), Me.Usuario.NumberFormat)
                DirectCast(e.Item.FindControl("lblImportePedido"), Label).Visible = False
                DirectCast(e.Item.FindControl("lblImportePedidoMoneda"), Label).Visible = False
                DirectCast(e.Item.FindControl("lblImporteRecibido"), Label).Visible = False
                DirectCast(e.Item.FindControl("lblImporteRecibidoMoneda"), Label).Visible = False
            Else
                DirectCast(e.Item.FindControl("lblImporteRecibido"), Label).Text = FSNLibrary.FormatNumber(fila.Item("IMPORTE"), Me.Usuario.NumberFormat)
                DirectCast(e.Item.FindControl("lblImportePedido"), Label).Text = FSNLibrary.FormatNumber(fila.Item("IMPORTE_PED"), Me.Usuario.NumberFormat)
                DirectCast(e.Item.FindControl("lblCantidadPedida"), Literal).Visible = False
                DirectCast(e.Item.FindControl("lblCantidadRecibida"), Literal).Visible = False
                DirectCast(e.Item.FindControl("lblPrecioUnitario"), Literal).Visible = False
                DirectCast(e.Item.FindControl("lblPrecioUnitarioMoneda"), Label).Visible = False
                DirectCast(e.Item.FindControl("lblImporte"), Label).Visible = False
                DirectCast(e.Item.FindControl("lblImporteMoneda"), Label).Visible = False
            End If
        End If
    End Sub
#End Region
#Region "Registrar Recepción"

    ''' Revisado por: blp. Fecha: 20/11/2012
    ''' <summary>
    ''' Carga las líneas de pedido a recepcionar en el panel de recepción y lo muestra
    ''' </summary>
    ''' <param name="LineasPedido">Líneas de pedido recepcionadas</param>
    ''' <remarks>Llamada desde Recepcion.aspx.vb. Máx. 0,3 seg.</remarks>
    Private Sub RegistrarRecepcion(ByVal LineasPedido As String, ByVal OrdenId As Long, Optional ByVal bOpcionCargarTodas As Boolean = True)
        Dim oOrden As COrdenes = FSNServer.Get_Object(GetType(FSNServer.COrdenes))
        _AdjuntosRecepcion = FSNServer.Get_Object(GetType(FSNServer.CAdjuntos))

        If OrdenId = 0 Then
            dsLineasRecep = oOrden.Ordenes_Obtener_Lineas_Recepcion(LineasPedido, Me.Idioma, Me.Acceso.gbAccesoFSSM)
        Else
            dsLineasRecep = oOrden.Ordenes_Obtener_Lineas_PendientesRecepcionar(OrdenId, Me.Idioma, Me.Acceso.gbAccesoFSSM)
        End If

        'Si no hay líneas de pedido pendientes de recepcionar (recep automáticas,...) no hacemos nada
        If dsLineasRecep.Tables(0).Rows.Count > 0 Then
            dlLineasPedido.DataSource = dsLineasRecep
            dlLineasPedido.DataBind()
            Dim ci As System.Globalization.CultureInfo = Threading.Thread.CurrentThread.CurrentCulture.Clone()
            If Acceso.gbFechaRecepBloqueada Then
                Dim sFecha As String = Now().Date.ToString("d", ci)
                lblFecRecepcion.Text = sFecha
                dtFecRecepcion.Visible = False
            Else
                dtFecRecepcion.Value = Now()
                lblFecRecepcion.Visible = False
                'If Me.Acceso.gbControlFechaRecepcion Then 
                'El cambio de la página de recepciones de la 6 a la 7 permite ahora recepcionar a la vez líneas de diferentes órdenes.
                'El parámetro gbControlFechaRecepcion, que indica que la fecha de recepción no puede ser menor que la de la orden, 
                'no puede aplicarse dado que:
                'Hay una única fecha de recepción 
                'Las líneas pueden ser de varias órdenes.
                'Cada orden puede tener una fecha de emisión diferente.
                'Una posibilidad es coger la fecha más alta de emisión y aplicarla como tope para todas las líneas
                'Otra puede ser poner la fecha de recepción en cada línea.
                'de momento, se deja de usar gbControlFechaRecepcion hasta tomar una decisión.
            End If

            If bOpcionCargarTodas Then
                Dim Ordenes As String = String.Empty
                For Each item As DataListItem In dlLineasPedido.Items
                    Ordenes &= If(Ordenes Is String.Empty, "", ",") & CType(item.FindControl("hidIdOrden"), HiddenField).Value
                Next

                dsLineasRecep = oOrden.Ordenes_Obtener_Lineas_PendientesRecepcionar(Ordenes, Me.Idioma, Me.Acceso.gbAccesoFSSM)
                lnkLineasPendientesRecepcionar.Visible = (dsLineasRecep.Tables(0).Rows.Count > dlLineasPedido.Items.Count)
            Else
                lnkLineasPendientesRecepcionar.Visible = bOpcionCargarTodas
            End If

            txtAlbaran.Text = ""
            listaAdjuntosJSON.Text = "[]"

            'Visibilidad del almacén
            If Not AlmacenVisible(dsLineasRecep) Then
                upRegistroRecepcion.FindControl("pnlLblRegistroRecepcionAlmacen").Visible = False
            End If

            If FSNServer.TipoAcceso.gbMostrarFechaContable Then
                dtFecContable.Value = Now.Date
            Else
                dtFecContable.Value = Date.MinValue
            End If
            upRegistroRecepcion.Update()

                LblArchAdjunPed.Text = Textos(273)          'MOD(29) - ID(274) - 'Añadir archivo adjunto','Add attached file','Angehängte Datei hinzufügen','Añadir archivo adjunto'
                BtnAcepAdjunPed.Text = Textos(113)
                BtnCancelAdjunPed.Text = Textos(124)
                mpeRegistroRecepcion.Show()
            End If
    End Sub
    Private Sub lnkLineasPendientesRecepcionar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLineasPendientesRecepcionar.Click
        Dim oOrden As COrdenes = FSNServer.Get_Object(GetType(FSNServer.COrdenes))
        Dim Ordenes As String = String.Empty
        For Each item As DataListItem In dlLineasPedido.Items
            Ordenes &= If(Ordenes Is String.Empty, "", ",") & CType(item.FindControl("hidIdOrden"), HiddenField).Value
        Next

        dsLineasRecep = oOrden.Ordenes_Obtener_Lineas_PendientesRecepcionar(Ordenes, Me.Idioma, Me.Acceso.gbAccesoFSSM)
        dlLineasPedido.DataSource = dsLineasRecep
        dlLineasPedido.DataBind()

        'Si ya le hemos pulsado no tiene que aparecer
        lnkLineasPendientesRecepcionar.Visible = False

        'Visibilidad del almacén
        If Not AlmacenVisible(dsLineasRecep) Then
            upRegistroRecepcion.FindControl("pnlLblRegistroRecepcionAlmacen").Visible = False
        End If

        upRegistroRecepcion.Update()
    End Sub

    Private Sub dlLineasPedido_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlLineasPedido.ItemCommand
        Select Case e.CommandName
            Case "MostrarDetalleArt"
                pnlDetalleArticulo.CargarDetalleArticulo(DsetPedidos.Tables("ORDENES"), e.CommandArgument)
            Case "AbrirPanelBuscarActivo"
                Dim emp As CEmpresa = FSNServer.Get_Object(GetType(CEmpresa))
                emp.ID = e.Item.Attributes("EMPRESA")
                SMActivos.Empresa = emp
                Dim oLinea As CLinea = FSNServer.Get_Object(GetType(CLinea))
                oLinea.ID = e.CommandArgument
                Dim dsCentro As DataSet = oLinea.DevolverCentro(Me.Usuario.Idioma)
                If dsCentro IsNot Nothing AndAlso dsCentro.Tables(0) IsNot Nothing AndAlso dsCentro.Tables(0).Rows.Count > 0 AndAlso dsCentro.Tables(0).Rows(0).Item("CCCOD") IsNot Nothing Then
                    Dim csm As FSNServer.Centro_SM = FSNServer.Get_Object(GetType(FSNServer.Centro_SM))
                    csm.Codigo = dsCentro.Tables(0).Rows(0).Item("CCCOD")
                    csm.Denominacion = dsCentro.Tables(0).Rows(0).Item("CCDEN")
                    SMActivos.CentroSM = csm
                End If
                SMActivos.CargarDatos()
                If SMActivos.Attributes("LINEAID") Is Nothing Then
                    SMActivos.Attributes.Add("LINEAID", e.CommandArgument)
                Else
                    SMActivos.Attributes("LINEAID") = e.CommandArgument
                End If
                mpeActivos.Show()
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "SMActivosZIndex", "$get('" & SMActivos.ClientID & "').style.zIndex=100002;", True)
            Case "AplicarActivoALineas"
                Dim dlLineas As DataList = CType(e.CommandSource, Control).NamingContainer.NamingContainer
                Dim txtact As TextBox = CType(e.CommandSource, Control).NamingContainer.FindControl("tbActivosLinea")
                Dim empresa As String = e.Item.Attributes("EMPRESA")
                Dim bActualizar As Boolean = False
                If Not String.IsNullOrEmpty(txtact.Attributes("CODIGO")) Then
                    For Each fila As DataListItem In dlLineas.Items
                        Dim tact As TextBox = fila.FindControl("tbActivosLinea")
                        Dim EmpresaFila As String = fila.Attributes("EMPRESA")
                        'AQUI NO CONTROLAMOS EL MODO DE IMPUTACION, PORQUE AÚN NO ESTÁ HECHO LO DE CABECERA
                        'xxxxxxxx
                        If tact.Visible Then 'Si el textbox de activo no está visible es que la orden de ese pedido no es de inversión y por tanto no tiene activo, por lo que no puede modificarse.
                            If String.IsNullOrEmpty(tact.Attributes("CENTROSM")) Then
                                Dim query = From activos In TodosActivos _
                                            Where activos.Empresa = EmpresaFila AndAlso activos.Codigo = tact.Attributes("CODIGO") _
                                            Select activos
                                If query.Any Then
                                    tact.Text = txtact.Text
                                    tact.BackColor = txtact.BackColor
                                    If tact.Attributes("CODIGO") Is Nothing Then
                                        tact.Attributes.Add("CODIGO", txtact.Attributes("CODIGO"))
                                    Else
                                        tact.Attributes("CODIGO") = txtact.Attributes("CODIGO")
                                    End If
                                    bActualizar = True
                                End If
                            Else
                                If TodosActivos.Find(Function(el As Fullstep.FSNServer.Activo) _
                                                         el.Centro_SM = tact.Attributes("CENTROSM") And el.Empresa = EmpresaFila _
                                                         And el.ID = txtact.Attributes("CODIGO")) IsNot Nothing Then
                                    tact.Text = txtact.Text
                                    tact.BackColor = txtact.BackColor
                                    If tact.Attributes("CODIGO") Is Nothing Then
                                        tact.Attributes.Add("CODIGO", txtact.Attributes("CODIGO"))
                                    Else
                                        tact.Attributes("CODIGO") = txtact.Attributes("CODIGO")
                                    End If
                                    bActualizar = True
                                End If
                            End If
                        End If
                        'xxxxxxxxx
                    Next
                    If bActualizar Then
                        upRegistroRecepcion.Update()
                    End If
                End If
        End Select
    End Sub

    ''' Revisado por: blp. Fecha: 19/10/2012
    ''' <summary>
    ''' Función que se lanza en el evento de carga del datalist con las líneas del pedido y sirve para llenar de contenido el mismo
    ''' </summary>
    ''' <param name="sender">Control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Evento de carga de cada línea del datalist. Máximo 0,3 seg.</remarks>
    Private Sub dlLineasPedido_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlLineasPedido.ItemDataBound
        Dim fila As DataRowView = CType(e.Item.DataItem, DataRowView)

        If e.Item.ItemType = ListItemType.Header Then
			CType(e.Item.FindControl("tdLblSalidaAlmacen"), Panel).Visible = Acceso.gbMostrarRecepSalidaAlmacen

			DirectCast(e.Item.FindControl("lblRegistroRecepcionPedido"), Label).Text = Textos(32)
            DirectCast(e.Item.FindControl("lblRegistroRecepcionArticulo"), Label).Text = Textos(37)
            DirectCast(e.Item.FindControl("lblRegistroRecepcionPrecioUnitario"), Label).Text = Textos(41)
            DirectCast(e.Item.FindControl("lblCantidadRecibida"), Label).Text = Textos(219)
            DirectCast(e.Item.FindControl("lblImporteRecibido"), Label).Text = Textos(269)
            DirectCast(e.Item.FindControl("lblImportePedido"), Label).Text = Textos(266)
            DirectCast(e.Item.FindControl("lblRegistroRecepcionDestino"), Label).Text = Textos(132)
            DirectCast(e.Item.FindControl("lblRegistroRecepcionAlmacen"), Label).Text = Textos(110)
            DirectCast(e.Item.FindControl("lblRegistroRecepcionFecEntregaSolicitada"), Label).Text = Textos(44)
            DirectCast(e.Item.FindControl("lblCantidadRecepcionar"), Label).Text = Textos(137)
            DirectCast(e.Item.FindControl("lblImporteRecepcionar"), Label).Text = Textos(267)
            DirectCast(e.Item.FindControl("lblCantidadPedida"), Label).Text = Textos(133)
            If Acceso.gbMostrarRecepSalidaAlmacen Then DirectCast(e.Item.FindControl("lblSalidaAlmacen"), Label).Text = Textos(278)
        End If
        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
			CType(e.Item.FindControl("tdSalidaAlmacen"), Panel).Visible = Acceso.gbMostrarRecepSalidaAlmacen

			e.Item.FindControl("lnkCodArt").Visible = Me.FSNUser.MostrarCodArt
            Dim oTipoPedido As cTipoPedido = TiposPedidos(True).ItemId(fila.Item("TIPOPEDIDO"))

            If fila.Item("PEDIDO_ALMACENABLE") = 0 Then
                'Cuando todas las líneas sean de pedidos no almacenables, ocultamos todo lo relativo al Alamcén
                If Not AlmacenVisible(dlLineasPedido.DataSource) Then
                    e.Item.FindControl("pnlAlmacen").Visible = False
                Else 'Si no, ocultamos sólo el dropdown
                    e.Item.FindControl("lstAlmacen").Visible = False
                End If
            Else
                DirectCast(e.Item.FindControl("lstAlmacen"), DropDownList).Attributes.Add("LINID", fila.Item("LINEAID"))
                DirectCast(e.Item.FindControl("lstAlmacen"), DropDownList).Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
                'En el dropdown de Almacén aparecerá una opción vacía en dos casos:
                '1. Cuando tanto el artículo como el tipo de orden tenga almacenamiento opcional
                '2. Cuando no se seleccionó ningún valor al emitir aunque el almacenamiento sea obligatorio (puede ocurrir que al emitir fuese opcional y que la condición de almacenable del tipo de orden o del artículo haya cambiado después)
                'Lo ideal sería guardar en la orden emitida su propiedad de almacenable dado que una vez se emite, es independiente del valor almacenable del tipo de pedido con que se emitió
                If fila.Item("PEDIDO_ALMACENABLE") = 2 OrElse IsDBNull(fila.Item("ALMACEN")) Then
                    Dim oListItem As New ListItem("", 0)
                    oListItem.Attributes.Add("class", "texto10")
                    DirectCast(e.Item.FindControl("lstAlmacen"), DropDownList).Items.Add(oListItem)
                End If
                Dim oDest As CDestino = FSNServer.Get_Object(GetType(CDestino))
                oDest.Cod = fila.Item("DESTINO")
                oDest.DevolverAlmacenesDestino(fila.Item("DESTINO"), Usuario.Idioma.ToString(), True)
                For Each oalm As cAlmacen In oDest.Almacenes
                    Dim sAlmacenDen As String
                    If oalm.Denominacion.Length > 20 Then
                        sAlmacenDen = oalm.Denominacion.Substring(0, 20) & "..."
                    Else
                        sAlmacenDen = oalm.Denominacion
                    End If
                    Dim oListItem As New ListItem(sAlmacenDen, oalm.Id)
                    oListItem.Attributes.Add("class", "texto10")
                    oListItem.Attributes.Add("title", oalm.Denominacion)
                    DirectCast(e.Item.FindControl("lstAlmacen"), DropDownList).Items.Add(oListItem)
                Next
                If Not IsDBNull(fila.Item("ALMACEN")) Then CType(e.Item.FindControl("lstAlmacen"), DropDownList).SelectedValue = fila.Item("ALMACEN")
            End If

            'Mostrar Almacen como texto si el pedido es de inversion y no recepcionable
            If oTipoPedido.Concepto = cTipoPedido.EnInvTipo.Inversion _
            AndAlso oTipoPedido.Recepcionable = cTipoPedido.EnRecTipo.NoRecepcionable Then
                DirectCast(e.Item.FindControl("lblAlmacen"), Label).Visible = True
                DirectCast(e.Item.FindControl("lstAlmacen"), DropDownList).Visible = False
                If Not DirectCast(e.Item.FindControl("lstAlmacen"), DropDownList).SelectedItem Is Nothing Then
                    DirectCast(e.Item.FindControl("lblAlmacen"), Label).Text = CType(e.Item.FindControl("lstAlmacen"), DropDownList).SelectedItem.Text
                    DirectCast(e.Item.FindControl("lblAlmacen"), Label).ToolTip = CType(e.Item.FindControl("lstAlmacen"), DropDownList).SelectedItem.Attributes("title")
                End If
            End If

            Dim oUnidadPedidoLinea As CUnidadPedido = Nothing
            If Not DBNullToSomething(fila.Item("UNIDAD")) = Nothing Then
                oUnidadPedidoLinea = TodasLasUnidades.Item(Trim(fila.Item("UNIDAD")))
                oUnidadPedidoLinea.AsignarFormatoaUnidadPedido(Me.Usuario.NumberFormat)
            End If
            'Pongo los importes en la moneda del pedido
            Dim dImporte, dImporteRec, dImportePend, dPrecioU As Double
            dImporte = DBNullToDbl(fila.Item("IMPORTE")) * fila.Item("CAMBIO")
            dImporteRec = DBNullToDbl(fila.Item("IMPORTE_RECIBIDO")) * fila.Item("CAMBIO")
            dImportePend = DBNullToDbl(fila.Item("IMPORTE_PENDIENTE")) * fila.Item("CAMBIO")
            dPrecioU = DBNullToDbl(fila.Item("PRECIOUNITARIO")) * fila.Item("CAMBIO")
            'Incidencia 30456. Los campos CANT_REC Y CANT_PED están declarados en bdd como FLOAT (con las implicaciones de imprecisión que eso conlleva: http://msdn.microsoft.com/es-es/library/ms187912(v=sql.105).aspx).
            'Para poder comparar ambos valores, necesitamos redondearlos a fin de minimizar la posibilidad de que aparezcan resultados inesperados y erróneos.
            Dim CANT_REC As Decimal = 0
            Dim IMP_REC As Decimal = 0
            Dim CANT_PED As Decimal = 0
            Dim IMP_PED As Decimal = 0

            If fila.Item("TIPORECEPCION") = 0 Then
                If DBNullToSomething(fila.Item("CANT_REC")) IsNot Nothing Then
                    CANT_REC = Math.Round(fila.Item("CANT_REC"), 15)
                Else
                    CANT_REC = 0
                End If
                If DBNullToSomething(fila.Item("CANTIDAD")) IsNot Nothing Then
                    CANT_PED = Math.Round(fila.Item("CANTIDAD"), 15)
                Else
                    CANT_PED = 0
                End If

            Else
                If DBNullToSomething(fila.Item("IMPORTE_RECIBIDO")) IsNot Nothing Then
                    IMP_REC = Math.Round(dImporteRec, 15)
                Else
                    IMP_REC = 0
                End If
                If DBNullToSomething(fila.Item("IMPORTE")) IsNot Nothing Then
                    IMP_PED = Math.Round(dImporte, 15)
                Else
                    IMP_PED = 0
                End If
            End If
            If Not oUnidadPedidoLinea Is Nothing Then
                Dim txtNoAplica As String = "&lt;" & Me.Textos(268) & "&gt;"

                If fila.Item("TIPORECEPCION") = 0 Then
                    DirectCast(e.Item.FindControl("litCantidadPedida"), Literal).Text = FSNLibrary.FormatNumber(CANT_PED, Me.Usuario.NumberFormat)

                    If oUnidadPedidoLinea.NumeroDeDecimales Is Nothing Then
                        Dim decimales As Integer = NumeroDeDecimales(DBNullToStr(CANT_REC))
                        oUnidadPedidoLinea.UnitFormat.NumberDecimalDigits = decimales
                    End If
                    DirectCast(e.Item.FindControl("litCantRecibida"), Literal).Text = FSNLibrary.FormatNumber(CANT_REC, Me.Usuario.NumberFormat)

                    If oUnidadPedidoLinea.NumeroDeDecimales Is Nothing Then
                        Dim decimales As Integer = NumeroDeDecimales(DBNullToStr(CANT_PED - CANT_REC))
                        oUnidadPedidoLinea.UnitFormat.NumberDecimalDigits = decimales
                    End If

                    DirectCast(e.Item.FindControl("lblPrecUni"), Literal).Text = FSNLibrary.FormatNumber(dPrecioU, Me.Usuario.NumberFormat)
                    DirectCast(e.Item.FindControl("litImportePedido"), Literal).Text = FSNLibrary.FormatNumber(dImporte, Me.Usuario.NumberFormat)

                    DirectCast(e.Item.FindControl("litImporteRecibido"), Literal).Text = FSNLibrary.FormatNumber(dImporteRec, Usuario.NumberFormat)
					DirectCast(e.Item.FindControl("txtImporte"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Value = If(dImporteRec > dImporte, 0, dImporte - dImporteRec)
					DirectCast(e.Item.FindControl("txtImporte"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Enabled = False
					DirectCast(e.Item.FindControl("txtCantidad"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Value = If(CANT_REC > CANT_PED, 0, CANT_PED - CANT_REC)

					'Ocultar textbox de importe y mostrar no aplica
					'DirectCast(e.Item.FindControl("txtImporte"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Visible = False
					'DirectCast(e.Item.FindControl("litImportePendiente"), Literal).Visible = True
					'DirectCast(e.Item.FindControl("litImportePendiente"), Literal).Text = txtNoAplica
					'DirectCast(e.Item.FindControl("lblMontxtImporte"), Label).Visible = False
				Else
                    DirectCast(e.Item.FindControl("litCantidadPedida"), Literal).Text = txtNoAplica
                    DirectCast(e.Item.FindControl("FSNlnkUniCant"), Fullstep.FSNWebControls.FSNLinkTooltip).Visible = False
                    DirectCast(e.Item.FindControl("litCantRecibida"), Literal).Text = txtNoAplica
                    DirectCast(e.Item.FindControl("FSNlnkUniCantRec"), Fullstep.FSNWebControls.FSNLinkTooltip).Visible = False
                    DirectCast(e.Item.FindControl("lblPrecUni"), Literal).Text = txtNoAplica
                    DirectCast(e.Item.FindControl("lblMonPrecUni"), Label).Visible = False
                    DirectCast(e.Item.FindControl("lblUnidRecep"), Label).Visible = False
                    DirectCast(e.Item.FindControl("litImportePedido"), Literal).Text = FSNLibrary.FormatNumber(dImporte, Me.Usuario.NumberFormat)
                    DirectCast(e.Item.FindControl("txtImporte"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Value = dImporte - dImporteRec
                    DirectCast(e.Item.FindControl("litCantidadPendiente"), Literal).Visible = False
                    DirectCast(e.Item.FindControl("litImporteRecibido"), Literal).Text = FSNLibrary.FormatNumber(dImporteRec, Usuario.NumberFormat)
					DirectCast(e.Item.FindControl("litImportePdteRecep"), HiddenField).Value = FSNLibrary.FormatNumber(dImportePend, Usuario.NumberFormat)
					'Ocultar textbox de cantidad y mostrar no aplica
					DirectCast(e.Item.FindControl("txtCantidad"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Visible = False
                    DirectCast(e.Item.FindControl("litCantidadPendiente"), Literal).Visible = True
                    DirectCast(e.Item.FindControl("litCantidadPendiente"), Literal).Text = txtNoAplica
                End If

            End If

            If Not IsDBNull(fila.Item("FECENTREGA")) Then
                DirectCast(e.Item.FindControl("lblFecRequerida"), Label).Text = FSNLibrary.FormatDate(fila.Item("FECENTREGA"), Me.Usuario.DateFormat)
                If fila.Item("ENTREGA_OBL") = 1 Then
                    DirectCast(e.Item.FindControl("lblFecRequerida"), Label).Text = CType(e.Item.FindControl("lblFecRequerida"), Label).Text & "<sup>(*)</sup>"
                End If
            Else
                DirectCast(e.Item.FindControl("lblFecRequerida"), Label).Text = "&nbsp;"
            End If
            DirectCast(e.Item.FindControl("lblMonPrecUni"), Label).Text = fila.Item("MON").ToString
            DirectCast(e.Item.FindControl("lblMonImporte"), Label).Text = fila.Item("MON").ToString

			If fila.Item("PEDIDOABIERTO") = 0 Then
				If fila.Item("TIPORECEPCION") = 0 Then
					Dim wnumCant As Infragistics.Web.UI.EditorControls.WebNumericEditor =
					DirectCast(e.Item.FindControl("txtCantidad"), Infragistics.Web.UI.EditorControls.WebNumericEditor)
					oUnidadPedidoLinea = Nothing

					configurarDecimales(fila.Item("UNIDAD"), wnumCant)

					Dim MaxValue As Double
					Dim MinValue As Double
                    If CANT_PED - CANT_REC > 0 Then
                        MinValue = 0
                        MaxValue = CANT_PED - CANT_REC
                        wnumCant.Nullable = True
                    ElseIf CANT_PED - CANT_REC < 0 Then
                        MaxValue = 0
                        MinValue = CANT_PED - CANT_REC
                        wnumCant.Nullable = True
                    Else
                        wnumCant.Visible = False
					End If
					Dim val As RangeValidator = CType(e.Item.FindControl("valCantidad"), RangeValidator)
					val.Visible = wnumCant.Visible
					val.MinimumValue = MinValue
					val.MaximumValue = MaxValue

					wnumCant.MinValue = MinValue

					If FSNUser.CargarCantPtesRecep Then
						wnumCant.Value = CANT_PED - CANT_REC
					End If

					val.ValidationGroup = fila.Item("ORDEN")
					DirectCast(e.Item.FindControl("valImporte"), RangeValidator).Enabled = False
				Else
					Dim wnumImporte As Infragistics.Web.UI.EditorControls.WebNumericEditor =
					DirectCast(e.Item.FindControl("txtImporte"), Infragistics.Web.UI.EditorControls.WebNumericEditor)
					oUnidadPedidoLinea = Nothing

					Dim MaxValue As Double
					Dim MinValue As Double
					If IMP_PED - IMP_REC > 0 Then
						MinValue = 0
						MaxValue = CANT_PED - CANT_REC
						wnumImporte.Nullable = True
					ElseIf IMP_PED - IMP_REC < 0 Then
						MaxValue = 0
						MinValue = IMP_PED - IMP_REC
						wnumImporte.Nullable = True
					Else
						wnumImporte.Visible = False
					End If
					Dim val As RangeValidator = CType(e.Item.FindControl("valImporte"), RangeValidator)
					val.Visible = wnumImporte.Visible
					val.MinimumValue = MinValue
					val.MaximumValue = MaxValue

					wnumImporte.MinValue = MinValue

					If FSNUser.CargarCantPtesRecep Then
						wnumImporte.Value = IMP_PED - IMP_REC
					End If

					val.ValidationGroup = fila.Item("ORDEN")
					DirectCast(e.Item.FindControl("valCantidad"), RangeValidator).Enabled = False
				End If
			Else
				DirectCast(e.Item.FindControl("valCantidad"), RangeValidator).Enabled = False
                DirectCast(e.Item.FindControl("valImporte"), RangeValidator).Enabled = False
            End If

            If Me.Acceso.gbAccesoFSSM Then
                Dim tact As TextBox = CType(e.Item.FindControl("tbActivosLinea"), TextBox)
                If oTipoPedido.Concepto <> cTipoPedido.EnInvTipo.Gasto And fila.Item("CONCEPTO") <> cTipoPedido.EnInvTipo.Gasto Then
                    e.Item.FindControl("pnlActivo").Visible = True
                    DirectCast(e.Item.FindControl("LblActivo"), Label).Text = Textos(115)

                    Dim fsnLnkActivo As FSNWebControls.FSNImageInfo = CType(e.Item.FindControl("FSNLnkActivo"), FSNWebControls.FSNImageInfo)
                    tact.Attributes.Add("CODIGO", String.Empty)
                    If oTipoPedido.Concepto = cTipoPedido.EnInvTipo.Inversion Or fila.Item("CONCEPTO") = cTipoPedido.EnInvTipo.Inversion Then
                        tact.Attributes.Add("OBLIGATORIO", "1")
                    Else
                        tact.Attributes.Add("OBLIGATORIO", "0")
                    End If
                    If fila.Item("ACTIVO") IsNot DBNull.Value Then
                        Dim mact As FSNServer.Activo = TodosActivos.Item(fila.Item("ACTIVO"))
                        If mact IsNot Nothing Then
                            fsnLnkActivo.ContextKey = mact.Codigo
                            fsnLnkActivo.Visible = True
                            tact.Text = mact.Codigo
                            tact.Attributes("CODIGO") = mact.ID
                            tact.BackColor = Drawing.Color.Green
                        Else
                            fsnLnkActivo.Visible = False
                        End If
                    Else
                        fsnLnkActivo.Visible = False
                    End If
                Else
                    e.Item.FindControl("pnlActivo").Visible = False
                End If

                If e.Item.ItemIndex > 0 Then
                    e.Item.FindControl("ImgBtnAplicarActivoLineas").Visible = False
                ElseIf e.Item.ItemIndex = 0 Then
                    Dim datosOrigen As DataSet = Nothing
                    Try
                        datosOrigen = CType(CType(sender, DataList).DataSource, DataSet)
                    Catch ex As Exception

                    End Try

                    If Not datosOrigen Is Nothing AndAlso datosOrigen.Tables.Count > 0 AndAlso datosOrigen.Tables(0).Rows.Count > 0 Then
                        e.Item.FindControl("ImgBtnAplicarActivoLineas").Visible = datosOrigen.Tables(0).Rows.Count > 1
                    End If
                End If
                Dim sCentro As String = DBNullToStr(fila.Item("CENTROSM"))
                tact.Attributes.Add("CENTROSM", sCentro)
                Dim sEmpresa As String = DBNullToStr(fila.Item("EMPRESA"))
                e.Item.Attributes.Add("EMPRESA", sEmpresa)
            End If

			'Recuperamos el atributo de la fila que nos dirá si es un pedido recibido totalmente
			'para así ocultar o no los campos editables.
			If Not IsDBNull(fila.Item("EST")) AndAlso fila.Item("EST") = CPedido.Estado.Cerrado Then
				'Almacen no editable
				DirectCast(e.Item.FindControl("lblAlmacen"), Label).Visible = True
				DirectCast(e.Item.FindControl("lstAlmacen"), DropDownList).Visible = False
				If Not DirectCast(e.Item.FindControl("lstAlmacen"), DropDownList).SelectedItem Is Nothing Then
					DirectCast(e.Item.FindControl("lblAlmacen"), Label).Text = CType(e.Item.FindControl("lstAlmacen"), DropDownList).SelectedItem.Text
					DirectCast(e.Item.FindControl("lblAlmacen"), Label).ToolTip = CType(e.Item.FindControl("lstAlmacen"), DropDownList).SelectedItem.Attributes("title")
				End If

				'Cantidad que se recibe oculta
				DirectCast(e.Item.FindControl("txtCantidad"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Attributes.Add("visibility", "hidden")
				'Activo en solo lectura (si es que el activo se muestra)
				If Me.Acceso.gbAccesoFSSM _
				AndAlso oTipoPedido.Concepto <> cTipoPedido.EnInvTipo.Gasto _
				AndAlso fila.Item("CONCEPTO") <> cTipoPedido.EnInvTipo.Gasto Then
					DirectCast(e.Item.FindControl("tbActivosLinea"), TextBox).Visible = False
					DirectCast(e.Item.FindControl("FSNLnkActivo"), FSNWebControls.FSNImageInfo).Visible = False
					DirectCast(e.Item.FindControl("ImgBtnBuscarActivoLinea"), ImageButton).Visible = False
					DirectCast(e.Item.FindControl("ImgBtnAplicarActivoLineas"), ImageButton).Visible = False

					Dim oFSNLnkInfoActivo As FSNWebControls.FSNLinkInfo = CType(e.Item.FindControl("FSNLnkInfoActivo"), FSNWebControls.FSNLinkInfo)
					Dim tbActivo As TextBox = CType(e.Item.FindControl("tbActivosLinea"), TextBox)
					oFSNLnkInfoActivo.Attributes.Add("CODIGO", String.Empty)
					oFSNLnkInfoActivo.ContextKey = DirectCast(e.Item.FindControl("FSNLnkActivo"), FSNWebControls.FSNImageInfo).ContextKey
					oFSNLnkInfoActivo.Text = tbActivo.Text
					oFSNLnkInfoActivo.Attributes("CODIGO") = tbActivo.Attributes("CODIGO")

					DirectCast(e.Item.FindControl("FSNLnkInfoActivo"), FSNWebControls.FSNLinkInfo).Visible = True
					DirectCast(e.Item.FindControl("FSNLnkInfoActivo"), FSNWebControls.FSNLinkInfo).Text = CType(e.Item.FindControl("tbActivosLinea"), TextBox).Text
				End If
			End If
			For Each drAtributo As DataRow In dsLineasRecep.Tables("DATOS_ATRIBUTOS_RECEPCION").Rows.OfType(Of DataRow).Where(Function(x) x("LINEA") = fila.Item("LINEAID")).ToList
				DirectCast(e.Item.FindControl("pnlAtributosRecepcion"), Panel).Controls.Add(New Panel() With {.ID = "pnlAtributos_" & fila.Item("LINEAID") & "_" & drAtributo("ATRIB")})
				With DirectCast(e.Item.FindControl("pnlAtributosRecepcion"), Panel).FindControl("pnlAtributos_" & fila.Item("LINEAID") & "_" & drAtributo("ATRIB"))
					Select Case drAtributo("TIPO")
						Case TiposDeDatos.TipoGeneral.TipoString
							.Controls.Add(New Label With {.Text = drAtributo("DEN") & ": ", .CssClass = "Negrita"})
							.Controls.Add(New Label With {.Text = drAtributo("VALOR_TEXT").ToString()})
						Case TiposDeDatos.TipoGeneral.TipoNumerico
							.Controls.Add(New Label With {.Text = drAtributo("DEN") & ": ", .CssClass = "Negrita"})
							.Controls.Add(New Label With {.Text = If(String.IsNullOrEmpty(drAtributo("VALOR_NUM").ToString), "",
										  FSNLibrary.FormatNumber(drAtributo("VALOR_NUM").ToString, FSNUser.NumberFormat))})
						Case TiposDeDatos.TipoGeneral.TipoFecha
							.Controls.Add(New Label With {.Text = drAtributo("DEN") & ": ", .CssClass = "Negrita"})
							.Controls.Add(New Label With {.Text = If(String.IsNullOrEmpty(drAtributo("VALOR_FEC").ToString), "",
										  FSNLibrary.FormatDate(drAtributo("VALOR_FEC").ToString, FSNUser.DateFormat))})
						Case Else
							.Controls.Add(New Label With {.Text = drAtributo("DEN") & ": ", .CssClass = "Negrita"})
							.Controls.Add(New Label With {.Text = If(String.IsNullOrEmpty(drAtributo("VALOR_BOOL").ToString), "",
										  If(CBool(drAtributo("VALOR_BOOL")), Textos(73), Textos(74)))})
					End Select
				End With
			Next
		End If
    End Sub
    ''' <summary>
    ''' Método que valida la recepción. Si no es válida, devuelve un mensaje personalizado.
    ''' </summary>
    ''' <param name="source">Control de validacion de la página aspx. En este caso el control CustomValidator de id "valObsRecep"</param>
    ''' <param name="args">argumentos de la validación</param>
    ''' <remarks>Llamada desde: el evento de validación del control CustomValidator. Tiempo inferior a 1 seg</remarks>
    Public Sub ValidarRecepcion(ByVal source As Object, ByVal args As ServerValidateEventArgs)
        CType(source, CustomValidator).ErrorMessage = ""
        With CType(source, CustomValidator).NamingContainer
            Dim chkProblemas As CheckBox = CType(.FindControl("chkProblemaRecep"), CheckBox)
            Dim txtObs As TextBox = CType(.FindControl("txtObsRecep"), TextBox)
            Dim chkMail As CheckBox = CType(.FindControl("chkNotificar"), CheckBox)
            If chkProblemas.Checked And String.IsNullOrEmpty(Trim(txtObs.Text)) Then
                If Not String.IsNullOrEmpty(CType(source, CustomValidator).ErrorMessage) Then _
                    CType(source, CustomValidator).ErrorMessage += "<br/>"
                CType(source, CustomValidator).ErrorMessage += Textos(108)
                args.IsValid = False
            ElseIf chkMail.Checked And String.IsNullOrEmpty(Trim(txtObs.Text)) Then
                If Not String.IsNullOrEmpty(CType(source, CustomValidator).ErrorMessage) Then _
                    CType(source, CustomValidator).ErrorMessage += "<br/>"
                CType(source, CustomValidator).ErrorMessage += Textos(109)
                args.IsValid = False
            End If
            Dim dlLineas As DataList = CType(CType(source, CustomValidator).NamingContainer.FindControl("dlLineasPedido"), DataList)
            For Each di As DataListItem In dlLineas.Items
                Dim txt As TextBox = CType(di.FindControl("tbActivosLinea"), TextBox)
                If txt.Attributes("OBLIGATORIO") IsNot Nothing AndAlso txt.Attributes("OBLIGATORIO") = "1" Then
                    If String.IsNullOrEmpty(txt.Attributes("CODIGO")) Then
                        If Not String.IsNullOrEmpty(CType(source, CustomValidator).ErrorMessage) Then _
                            CType(source, CustomValidator).ErrorMessage += "<br/>"
                        CType(source, CustomValidator).ErrorMessage += Textos(114)
                        args.IsValid = False
                        Exit For
                    End If
                End If
            Next
            Dim HayRecepciones As Boolean = False
            For Each di As DataListItem In dlLineas.Items
                'Usamos la visibilidad del control txtCantidad para saber si la línea ya ha sido totalmente recepcionada o no.
                'Si es visible es que aún no se ha recepcionado del todo y hay que validar su contenido
                If CType(di.FindControl("hidTipoRecepcion"), HiddenField).Value = 0 Then
                    Dim wNumEdCantidad As Infragistics.Web.UI.EditorControls.WebNumericEditor = CType(di.FindControl("txtCantidad"), Infragistics.Web.UI.EditorControls.WebNumericEditor)

                    If wNumEdCantidad.Visible Then
                        If Me.Acceso.gbControlCantidadRecepcion _
                        AndAlso wNumEdCantidad.ValueDouble = wNumEdCantidad.ValueInt _
                        AndAlso wNumEdCantidad.ValueInt = 0 Then
                            If Not String.IsNullOrEmpty(CType(source, CustomValidator).ErrorMessage) Then _
                                    CType(source, CustomValidator).ErrorMessage += "<br/>"
                            CType(source, CustomValidator).ErrorMessage += Textos(125)
                            args.IsValid = False
                            Exit For
                        End If
                        If Not Double.IsNaN(wNumEdCantidad.ValueDouble) Then HayRecepciones = True
                    End If
                Else
                    Dim wNumEdImporte As Infragistics.Web.UI.EditorControls.WebNumericEditor = CType(di.FindControl("txtImporte"), Infragistics.Web.UI.EditorControls.WebNumericEditor)
                    If wNumEdImporte.ValueDouble = 0 Then
                        If Not String.IsNullOrEmpty(CType(source, CustomValidator).ErrorMessage) Then _
                                    CType(source, CustomValidator).ErrorMessage += "<br/>"
                        CType(source, CustomValidator).ErrorMessage += Textos(125)
                        args.IsValid = False
                        Exit For
                    End If
                    If Not Double.IsNaN(wNumEdImporte.ValueDouble) Then HayRecepciones = True
                End If
            Next
            If Not HayRecepciones And args.IsValid Then
                args.IsValid = False
                If Not String.IsNullOrEmpty(CType(source, CustomValidator).ErrorMessage) Then _
                    CType(source, CustomValidator).ErrorMessage += "<br/>"
                CType(source, CustomValidator).ErrorMessage += Textos(127)
            End If
            Dim orden As COrden = FSNServer.Get_Object(GetType(COrden))
            Dim sPedido As String
            For Each dlItem As DataListItem In dlLineas.Items
                orden.ID = DBNullToInteger(DirectCast(dlItem.FindControl("hidIdOrden"), HiddenField).Value)
                sPedido = DBNullToStr(DirectCast(dlItem.FindControl("FSNlnkRecepcionPedido"), FSNWebControls.FSNLinkInfo).Text)
                If Not orden.ValidarIntegracion() Then
                    If Not String.IsNullOrEmpty(CType(source, CustomValidator).ErrorMessage) Then _
                        DirectCast(source, CustomValidator).ErrorMessage += "<br/>"
                    DirectCast(source, CustomValidator).ErrorMessage += sPedido & "- " & Textos(122)
                    args.IsValid = False
                    Exit For
                End If
            Next

        End With
    End Sub

    ''' Revisado por: blp. Fecha: 19/10/2012
    ''' <summary>
    ''' Función que se lanza en el evento Click del control btnRegistrar y lanza el proceso de registro en bdd de la recepción
    ''' </summary>
    ''' <param name="sender">Control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Evento click del control. Máximo 0,3 seg.</remarks>
    Private Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
        If Page.IsValid() Then
            Dim hayRecepcionMayorQueMaximo As Boolean = False
            Dim serializer As New JavaScriptSerializer()

            lblConfirm.Text = ""
            contDesviosCero = 0
            contLineas = 0

            Dim hayRecepcionImporteMayorQueMaximo As Boolean = Me.comprobarRecepcionImporteMayorMaximo(dlLineasPedido)
            hayRecepcionMayorQueMaximo = comprobarRecepcionMayorMaximo(dlLineasPedido)

            If hayRecepcionMayorQueMaximo = False AndAlso Not hayRecepcionImporteMayorQueMaximo Then
                Recepcionar(txtRecepAlbaran.Text,
                            Not CType(chkProblemaRecep, CheckBox).Checked, txtObsRecep.Text,
                            dlLineasPedido, chkRecepIncorrecta.Checked, chkNotificar.Checked, False,
                            New JavaScriptSerializer().Deserialize(Of List(Of Object))(listaAdjuntosJSON.Text)
                            )
            End If
            btnAceptarConfirmServer.CommandName = "Registrar"
            btnAceptarConfirm.Style.Add("display", "none")
            btnCancelarConfirm.Style.Add("display", "none")
            btnAceptarConfirmServer.Style.Add("display", "none")
            If contLineas = contDesviosCero Then
                lblConfirm.Text = Textos(123)   'Ha introducido una cantidad recepcionada superior a la pendiente. La aplicación dará el pedido como recepcionado completamente por la cantidad máxima. ¿Desea continuar?
                btnAceptarConfirmServer.Style("display") = "block"
            End If
            btnCancelarRegistrarRecepcion.Style("display") = "block"
            txtObservacionesBloqueoFacturacion.Style("display") = "none"
            upConfirm.Update()
            mpeConfirm.Show()
            mpeRegistroRecepcion.Hide()
        End If
        upRegistroRecepcion.Update()
    End Sub

    ''' Revisado por: blp. Fecha: 20/11/2012
    ''' <summary>
    ''' Efectúa la recepción de la(s) línea(s) y el cierre de la(s) orden(es)
    ''' Si los valores a recepcionar superan el máximo, se muestra un panel de aviso.
    ''' </summary>
    ''' <param name="sender">Control que lanza el evento</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde evento click del control. Máx. 0,3 seg.</remarks>
    Private Sub btnRegistrarCerrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrarCerrar.Click
        If Page.IsValid() Then
            Dim hayRecepcionMayorQueMaximo As Boolean = False
            Dim hayRecepcionImporteMayorQueMaximo As Boolean = False

            lblConfirm.Text = ""
            contDesviosCero = 0
            contLineas = 0

            hayRecepcionMayorQueMaximo = comprobarRecepcionMayorMaximo(dlLineasPedido)
            hayRecepcionImporteMayorQueMaximo = comprobarRecepcionImporteMayorMaximo(dlLineasPedido)

            If Not hayRecepcionMayorQueMaximo And Not hayRecepcionImporteMayorQueMaximo Then
                If chkProblemaRecep.Checked Then
                    With valObsRecep
                        .ErrorMessage = Textos(107)
                        .IsValid = False
                    End With
                Else
                    Recepcionar(txtRecepAlbaran.Text,
                            Not CType(chkProblemaRecep, CheckBox).Checked, txtObsRecep.Text,
                            dlLineasPedido, chkRecepIncorrecta.Checked, chkNotificar.Checked, True,
                            New JavaScriptSerializer().Deserialize(Of List(Of Object))(listaAdjuntosJSON.Text)
                            )
                End If
            End If
            btnAceptarConfirmServer.CommandName = "Registrar"
            btnAceptarConfirm.Style.Add("display", "none")
            btnCancelarConfirm.Style.Add("display", "none")
            btnAceptarConfirmServer.Style.Add("display", "none")
            If contLineas = contDesviosCero Then
                lblConfirm.Text = Textos(123)   'Ha introducido una cantidad recepcionada superior a la pendiente. La aplicación dará el pedido como recepcionado completamente por la cantidad máxima. ¿Desea continuar?
                btnAceptarConfirmServer.Style("display") = "block"
            End If
            btnCancelarRegistrarRecepcion.Style("display") = "block"
            txtObservacionesBloqueoFacturacion.Style("display") = "none"
            upConfirm.Update()
            mpeConfirm.Show()
            mpeRegistroRecepcion.Hide()
        End If
        upRegistroRecepcion.Update()
    End Sub

	''' <summary>
	''' Cerrar Ordenes
	''' </summary>
	''' <param name="sender">Objeto que ha lanzado el evento</param>
	''' <param name="e">Argumentos del evento</param>
	''' <remarks>Llamada desde: Sistema; Tiempo mÃ¡ximo:0,1</remarks>
	Private Sub btnCerrarOrdenes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCerrarOrdenes.Click
		Dim ordenes As New List(Of Integer)
		Dim iTempOrden As Integer

		With dlLineasPedido
			For Each lin As DataListItem In .Items
				iTempOrden = CType(lin.FindControl("hidIdOrden"), HiddenField).Value
				If Not ordenes.Contains(iTempOrden) Then 'Si estÃƒÆ’Ã‚Â¡ repetido la orden, no merece la pena introducirla porque se cerrarÃƒÆ’Ã‚Â­a mÃƒÆ’Ã‚Â¡s de 1 vez.
					ordenes.Add(iTempOrden)
				End If
			Next
		End With
		Dim oOrden As COrden = FSNServer.Get_Object(GetType(FSNServer.COrden))
		oOrden.Ordenes = ordenes
		oOrden.CerrarOrdenes(Me.Usuario.CodPersona)

        Dim oIntegracion As Integracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))
        'Llamamos al servicio de integración WCF por cada orden.
        For Each IdOrden In ordenes
            oIntegracion.LlamarFSIS(TablasIntegracion.PED_Aprov, IdOrden)
        Next

        mpeRegistroRecepcion.Hide()
        EliminarCachePedidos(0) ' Si queremos recargar todo el dataset le pasamos 0
        ActualizarDsetpedidos()
        CargarwhdgEntregas()
        InicializarPaginador()
        pnlGrid.Update()
    End Sub

    ''' Revisado por: blp. Fecha: 19/10/2012
    ''' <summary>
    ''' Comprueba al recepcionar si, para cada línea del pedido, la cantidad indicada como recepcionada es mayor que la cantidad máxima
    ''' solicitada. Si hay alguna que lo es, devuelve valor true, si no, false
    ''' </summary>
    ''' <param name="LineasPedido">las lineas de pedido</param>
    ''' <returns>Booleano que indica si hay algúna cantidad recepcionada superior a la cantidad solicitada </returns>
    ''' <remarks></remarks>
    Private Function comprobarRecepcionMayorMaximo(ByVal LineasPedido As DataList) As Boolean
        Dim hayRecepcionMayorMaximo As Boolean = False
        If Not LineasPedido Is Nothing Then
            With LineasPedido
                For Each linea As DataListItem In .Controls
                    'la primera linea del datalist es la cabecera
                    If Not CType(linea.FindControl("hidTipoRecepcion"), HiddenField) Is Nothing Then
                        Dim CantidadSolicitada As Double = 0
                        Dim CantidadRecepcionada As Double = 0
                        Dim numDecimales As Integer = 0
                        Dim tipoRecepcion As Integer = CType(linea.FindControl("hidTipoRecepcion"), HiddenField).Value
                        Dim dPorcenDesvio As Double = strToDbl(CType(linea.FindControl("hidPorcenDesvio"), HiddenField).Value)
                        Dim sCodArt As String = CType(linea.FindControl("lnkCodArt"), LinkButton).Text
                        Dim sUni As String = CType(linea.FindControl("FSNlnkUniCant"), FSNWebControls.FSNLinkTooltip).Text

                        If tipoRecepcion = 0 Then
                            Dim wNumericEdit As Infragistics.Web.UI.EditorControls.WebNumericEditor = CType(linea.FindControl("txtCantidad"), Infragistics.Web.UI.EditorControls.WebNumericEditor)
                            'Vamos a recuperar el NumberFormatinfo del usuario y el número de decimales de la unidad (o bien el formato de la unidad de la línea, que debería ser lo mismo)
                            'para configurar correctamente el valor que recuperamos del control litPdteRecep
                            If wNumericEdit IsNot Nothing AndAlso wNumericEdit.Attributes("numeroDecimales") IsNot Nothing Then
                                numDecimales = wNumericEdit.Attributes("numeroDecimales")
                            Else
                                If CType(linea.FindControl("FSNlnkUniCant"), FSNWebControls.FSNLinkTooltip) IsNot Nothing Then
                                    Dim unidad As String = CType(linea.FindControl("FSNlnkUniCant"), FSNWebControls.FSNLinkTooltip).Text
                                    numDecimales = TodasLasUnidades.Item(Trim(unidad)).NumeroDeDecimales
                                End If
                            End If
                            Dim formatoNumerico As System.Globalization.NumberFormatInfo
                            formatoNumerico = Me.Usuario.NumberFormat.Clone
                            formatoNumerico.NumberDecimalDigits = numDecimales

                            If Not linea.FindControl("litPdteRecep") Is Nothing _
                                    AndAlso Not String.IsNullOrEmpty(CType(linea.FindControl("litPdteRecep"), HiddenField).Value) Then
                                CantidadSolicitada = (Math.Abs(Double.Parse(CType(linea.FindControl("litPdteRecep"), HiddenField).Value, formatoNumerico)) + (Math.Abs(Double.Parse(CType(linea.FindControl("litPdteRecep"), HiddenField).Value, formatoNumerico)) * dPorcenDesvio / 100))
                            End If
                            If Not linea.FindControl("txtCantidad") Is Nothing _
                                AndAlso Not String.IsNullOrEmpty(CType(linea.FindControl("txtCantidad"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Value) Then
                                CantidadRecepcionada = Math.Abs(CType(linea.FindControl("txtCantidad"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Value)
                            End If

                            If CantidadRecepcionada > CantidadSolicitada Then
                                contLineas = contLineas + 1
                                hayRecepcionMayorMaximo = True
                                CType(linea.FindControl("txtCantidad"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Value = (Math.Abs(Double.Parse(CType(linea.FindControl("litPdteRecep"), HiddenField).Value _
                                                , formatoNumerico) + (Double.Parse(CType(linea.FindControl("litPdteRecep"), HiddenField).Value, formatoNumerico) * dPorcenDesvio / 100)))
                                If dPorcenDesvio = 0 Then
                                    contDesviosCero = contDesviosCero + 1
                                    lblConfirm.Text = lblConfirm.Text & sCodArt & ": " & Textos(283) & "<P>"
                                Else
                                    lblConfirm.Text = lblConfirm.Text & sCodArt & ": " & Textos(280) & " " & dPorcenDesvio & "%. " & Textos(282) & " " & CantidadSolicitada & " " & sUni & ".<P>"
                                End If

                            End If
                        End If
                    End If
                Next
            End With
        End If
        Return hayRecepcionMayorMaximo
    End Function

    Private Function comprobarRecepcionImporteMayorMaximo(ByVal LineasPedido As DataList) As Boolean
        Dim hayRecepcionMayorMaximo As Boolean = False
        If Not LineasPedido Is Nothing Then
            With LineasPedido
                For Each linea As DataListItem In .Controls
                    'la primera linea del datalist es la cabecera
                    If Not CType(linea.FindControl("hidTipoRecepcion"), HiddenField) Is Nothing Then
                        Dim tipoRecepcion As Integer = CType(linea.FindControl("hidTipoRecepcion"), HiddenField).Value
                        Dim Importepedido As Double = 0
                        Dim importerecepcionado As Double = 0
                        Dim importeRecibido As Double = 0
                        Dim importePendiente As Double = 0
						Dim numDecimales As Integer = 0
                        Dim dPorcenDesvio As Double = strToDbl(CType(linea.FindControl("hidPorcenDesvio"), HiddenField).Value)
                        Dim sCodArt As String = CType(linea.FindControl("lnkCodArt"), LinkButton).Text
                        If tipoRecepcion = 1 Then
                            importerecepcionado = CType(linea.FindControl("txtImporte"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Value
                            Importepedido = Double.Parse(CType(linea.FindControl("litImportePedido"), Literal).Text) + (Double.Parse(CType(linea.FindControl("litImportePedido"), Literal).Text) * dPorcenDesvio / 100)
                            importePendiente = Double.Parse(CType(linea.FindControl("litImportePdteRecep"), HiddenField).Value) + (Double.Parse(CType(linea.FindControl("litImportePdteRecep"), HiddenField).Value) * dPorcenDesvio / 100)
                            importeRecibido = importerecepcionado - importePendiente
                            If Importepedido < importerecepcionado + importeRecibido Then
                                hayRecepcionMayorMaximo = True
                                CType(linea.FindControl("txtImporte"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Value = importePendiente
                                lblConfirm.Text = lblConfirm.Text & sCodArt & ": " & Textos(280) & " " & dPorcenDesvio & "%. " & Textos(281) & " " & Importepedido & Chr(13) & Chr(10)
                            End If
                        End If
                    End If
                Next
            End With
        End If
        Return hayRecepcionMayorMaximo
    End Function

    ''' Revisado por: blp. Fecha: 19/10/2012
    ''' <summary>
    ''' Método que efectúa la recepción.
    ''' </summary>
    ''' <param name="Albaran">Nº albarán</param>
    ''' <param name="ProblemaRecepcion">True->Ha habido problema en la recepción</param>
    ''' <param name="ObservacionesRecepcion">Observaciones del usuario acerca de la recepción</param>
    ''' <param name="LineasRecepcion">Líneas de la recepción</param>
    ''' <param name="RecepcionIncorrecta">True->La recepción es incorrecta</param>
    ''' <param name="Notificar">True->Se notifica la recepción a quien correponda</param>
    ''' <param name="Cerrar">Informa si se debe cerrar la orden o no</param>
    ''' <remarks>Llamada desde Recepcion.aspx.vb. Tiempo máx inferior 1 seg</remarks>
    Public Sub Recepcionar(ByVal Albaran As String,
                           ByVal ProblemaRecepcion As Boolean, ByVal ObservacionesRecepcion As String,
                           ByVal LineasRecepcion As DataList, ByVal RecepcionIncorrecta As Boolean,
                           ByVal Notificar As Boolean, ByVal Cerrar As Boolean, ByVal FicherosAdjuntos As List(Of Object))
        Dim oRecep As CRecepcion = FSNServer.Get_Object(GetType(FSNServer.CRecepcion))
        Dim ci As System.Globalization.CultureInfo = Threading.Thread.CurrentThread.CurrentCulture.Clone()
        'Fecha de recepción del albarán y la fecha de sistema (la fecha en que se graba en el sistema la recepción)
        'Si la fecha de recepción está bloqueada, la fecha de albarán es la fecha de sistema (Now())
        If Acceso.gbFechaRecepBloqueada Then
            oRecep.Fecha = DateTime.Parse(lblFecRecepcion.Text, ci)
            oRecep.FechaSistema = oRecep.Fecha
        Else
            oRecep.Fecha = dtFecRecepcion.Value
            oRecep.FechaSistema = Now
        End If
        oRecep.Albaran = Albaran
        oRecep.Correcto = ProblemaRecepcion
        oRecep.Obs = ObservacionesRecepcion
        oRecep.FechaContable = dtFecContable.Value
        Dim oLineas As CLineas = FSNServer.Get_Object(GetType(FSNServer.CLineas))
        Dim ordenes As New List(Of Integer)
        With LineasRecepcion
            For Each lin As DataListItem In .Items
                Dim tipoRec As Integer
                Dim bSalidaAlmacen As Boolean
                tipoRec = CType(lin.FindControl("hidTipoRecepcion"), HiddenField).Value
                ordenes.Add(CType(lin.FindControl("hidIdOrden"), HiddenField).Value)
                If Acceso.gbMostrarRecepSalidaAlmacen Then bSalidaAlmacen = CType(lin.FindControl("chkSalidaAlmacen"), HtmlInputCheckBox).Checked

                'Recepcion por cantidad
                If tipoRec = 0 Then
                    Dim cantidad As Object
                    If CType(lin.FindControl("txtCantidad"), Infragistics.Web.UI.EditorControls.WebNumericEditor).ValueInt <> CType(lin.FindControl("txtCantidad"), Infragistics.Web.UI.EditorControls.WebNumericEditor).ValueDouble Then
                        cantidad = CType(lin.FindControl("txtCantidad"), Infragistics.Web.UI.EditorControls.WebNumericEditor).ValueDouble
                    Else
                        cantidad = CType(lin.FindControl("txtCantidad"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Value
                    End If
                    If String.IsNullOrEmpty(CType(lin.FindControl("tbActivosLinea"), TextBox).Attributes("CODIGO")) Then
						oLineas.Add(CType(lin.FindControl("lnkDenArt"), LinkButton).CommandArgument, cantidad, bSalidaAlmacen:=bSalidaAlmacen)
					Else
                        oLineas.Add(CType(lin.FindControl("lnkDenArt"), LinkButton).CommandArgument, cantidad, CType(CType(lin.FindControl("tbActivosLinea"), TextBox).Attributes("CODIGO"), Integer), bSalidaAlmacen:=bSalidaAlmacen)
                    End If
                Else
                    'Recepcion por importe
                    Dim importe As Double
                    Dim importePed As Double
                    importe = CType(lin.FindControl("txtImporte"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Value
                    importePed = CDbl(CType(lin.FindControl("litImportePedido"), Literal).Text)
                    oLineas.Add(CType(lin.FindControl("lnkDenArt"), LinkButton).CommandArgument, 0, 0, tipoRec, importe, importePed, bSalidaAlmacen:=bSalidaAlmacen)
                End If
            Next
        End With
        oRecep.Ordenes = ordenes
        oRecep.Lineas = oLineas

        Dim arrIdLineas() As Integer
        ReDim arrIdLineas(0)
        Dim arrIdOrdenes() As Integer
        ReDim arrIdOrdenes(0)
        Dim arrCantLineas() As Double
        ReDim arrCantLineas(0)
        Dim indiceLineas As Integer
        indiceLineas = -1

        For Each lin In oRecep.Lineas
            ReDim Preserve arrIdLineas(indiceLineas + 1)
            ReDim Preserve arrIdOrdenes(indiceLineas + 1)
            ReDim Preserve arrCantLineas(indiceLineas + 1)
            arrIdLineas(indiceLineas + 1) = lin.ID
            arrIdOrdenes(indiceLineas + 1) = oRecep.Ordenes(indiceLineas + 1)
            arrCantLineas(indiceLineas + 1) = lin.CantRec
            indiceLineas = indiceLineas + 1
        Next

        Dim oEP_ValidacionesIntegracion As EP_ValidacionesIntegracion = Me.FSNServer.Get_Object(GetType(EP_ValidacionesIntegracion))
        If oEP_ValidacionesIntegracion.HayIntegracion(TablasIntegracion.Rec_Aprov) Then
            Dim MapperStr As String
            Dim iNumError As Short
            Dim strError As String = ""
            If Not oEP_ValidacionesIntegracion.HayIntegracionSalidaWCF(TablasIntegracion.Rec_Aprov) Then
                If Not oEP_ValidacionesIntegracion.EvalMapperRecepciones(iNumError, strError, arrIdOrdenes, oRecep.Albaran, oRecep.Obs, arrIdLineas, arrCantLineas) Then
                    Dim oErrorMapper As New Exception(strError)
                    oErrorMapper.Source = CStr(iNumError)
                    iNumError = CInt(oErrorMapper.Source)
                    If iNumError = -100 Then
                        MapperStr = oErrorMapper.Message
                    Else
                        Dim oTextosIntegracion As TextosIntegracion = Me.FSNServer.Get_Object(GetType(TextosIntegracion))
                        MapperStr = oTextosIntegracion.MensajeIntegracionError(MapperModuloMensaje.Recepciones _
                                , iNumError, Me.Usuario.Idioma, oErrorMapper.Message)
                    End If

                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "MapperAlert", "alert('" & MapperStr & "');", True)
                    Exit Sub
                End If
            Else
                If Not oEP_ValidacionesIntegracion.ValidacionesWCF_REC(strError, oRecep, FSNUser.Idioma) Then
                    MapperStr = strError
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "MapperAlert", "alert('" & MapperStr & "');", True)
                    Exit Sub
                End If
            End If
        End If

        Try
            oRecep.RegistrarRecepcion(RecepcionIncorrecta, Me.Usuario.CodPersona, Notificar, Cerrar, Me.Acceso)

            'Dim serializer As New JavaScriptSerializer()
            'Dim FicherosAdjuntos = serializer.Deserialize(Of List(Of Object))(listaAdjuntosJSON.Text)
            For i As Integer = 0 To (FicherosAdjuntos.Count - 1)
                Dim oAdjun As Adjunto = Me.FSNServer.Get_Object(GetType(FSNServer.Adjunto))
                oAdjun.Nombre = FicherosAdjuntos(i)("Nombre")
                oAdjun.FileName = FicherosAdjuntos(i)("FileName")
                oAdjun.dataSize = FicherosAdjuntos(i)("dataSize")
                oAdjun.Comentario = FicherosAdjuntos(i)("Comentario")
                oAdjun.DirectorioGuardar = FicherosAdjuntos(i)("DirectorioGuardar")
                oAdjun.tipo = FicherosAdjuntos(i)("tipo")
                oAdjun.Operacion = FicherosAdjuntos(i)("Operacion")
                oAdjun.Cod = Me.Usuario.Cod
                oAdjun.tipo = TiposDeDatos.Adjunto.Tipo.RecepcionPedido
                For j As Integer = 0 To (oRecep.IDs.Count - 1)
                    oAdjun.IdEsp = oRecep.IDs.GetValue(j)
                    oAdjun.GrabarAdjunto(oAdjun.FileName, oAdjun.DirectorioGuardar, TiposDeDatos.Adjunto.Tipo.RecepcionPedido, oAdjun.IdEsp, oAdjun.Comentario, oAdjun.dataSize)
                Next
            Next

            mpeRegistroRecepcion.Hide()
            'Vaciamos el campo albarán para asegurarnos que al volver a mostrar el panel no venga el valor anterior
            txtRecepAlbaran.Text = String.Empty
            'Al recepcionar, recargamos el grid de recepciones
            EliminarCachePedidos(0) ' Si queremos recargar todo el dataset le pasamos 0
            ActualizarDsetpedidos()
            CargarwhdgEntregas()
            InicializarPaginador()
            pnlGrid.Update()
        Catch ex As FSNException
            ImgAlerta.ImageUrl = "~/images/Icono_Error_Amarillo_40x40.gif"
            lblMensaje.Visible = True
            pnlMensaje.Height = Unit.Pixel(180)
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "pnlMensajeHeight", "$get('" & pnlMensaje.ClientID & "').style.height = '180px'", True)
            If ex.Number = FSNException.TipoError.CodigoAlbaranDuplicado Then
                lblTituloMensaje.Text = Textos(111) 'Nº de albarán duplicado
                lblMensaje.Text = Textos(112) 'Ya existen recepciones asignadas a este nº de albarán.
                upMensaje.Update()
                mpeMensaje.Show()
                Exit Sub
            ElseIf ex.Number = FSNException.TipoError.TodasLasLineasHanSidoRecepcionadas Then
                lblTituloMensaje.Text = Textos(138) 'Lineas ya recepcionadas
                lblMensaje.Text = Textos(139) 'Todas las líneas que se intentan recepcionar han sido completamente recepcionadas con anterioridad.
                upMensaje.Update()
                mpeMensaje.Show()
                Exit Sub
            Else
                Throw ex
            End If
        Catch ex As Exception
            Throw ex
        End Try
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "pnlMensajeHeight", "$get('" & pnlMensaje.ClientID & "').style.height = '120px'", True)
        ImgAlerta.ImageUrl = "~/images/baja.gif"
        lblTituloMensaje.Text = Textos(264) 'Recepcion registrada correctamente
        lblMensaje.Visible = False
        upMensaje.Update()
        mpeMensaje.Show()

        'NOTIFICACIONES
        If chkNotificar.Checked Then
            If oRecep.Correcto Then
                oRecep.NotificarRecepcionCorrecta(Me.Usuario.Email)
            Else
                oRecep.NotificarRecepcionIncorrecta(Me.Usuario.Email)
            End If
        End If

        oRecep.NotificarRecepcionAprovisionador(Me.Usuario.Email, DsetPedidos.Tables("ORDENES"), Me.Usuario.CodPersona)
    End Sub
#End Region
#Region "Panel Confirmación"
    ''' Revisado por: blp. Fecha: 02/12/2011
    ''' <summary>
    ''' Procedimiento que maneja el evento CLICK del botón btnAceptarConfirm
    ''' </summary>
    ''' <param name="sender">Objeto que ha lanzado el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde el botón btnAceptarConfirmde Seguimiento.aspx
    ''' Tiempo máximo 1 seg.</remarks>
    Private Sub btnAceptarConfirmServer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarConfirmServer.Click
        Select Case btnAceptarConfirmServer.CommandName
            Case "Registrar"
                Recepcionar(txtRecepAlbaran.Text, _
                Not CType(chkProblemaRecep, CheckBox).Checked, txtObsRecep.Text, _
                dlLineasPedido, chkRecepIncorrecta.Checked, chkNotificar.Checked, False,
                            New JavaScriptSerializer().Deserialize(Of List(Of Object))(listaAdjuntosJSON.Text)
                            )
            Case "RegistraryCerrar"
                If chkProblemaRecep.Checked Then
                    With valObsRecep
                        .ErrorMessage = Textos(107)
                        .IsValid = False
                    End With
                Else
                    Recepcionar(txtRecepAlbaran.Text, _
                            Not CType(chkProblemaRecep, CheckBox).Checked, txtObsRecep.Text, _
                            dlLineasPedido, chkRecepIncorrecta.Checked, chkNotificar.Checked, True,
                            New JavaScriptSerializer().Deserialize(Of List(Of Object))(listaAdjuntosJSON.Text)
                            )

                End If
        End Select
        mpeConfirm.Hide()
    End Sub
#End Region
#Region "Page Methods"

    ''' Revisado por:blp. Fecha: 09/10/2012
    ''' <summary>
    ''' Anular recepción, o sea, anular un albarán completo. 
    ''' Se considera un único albarán a aquel en el que concuerdan el número de albarán, el proveedor y la fecha de recepción
    ''' </summary>
    ''' <param name="Albaran">Número de albarán</param>
    ''' <param name="FechaAlbaran">Fecha de recepción del albarán</param>
    ''' <param name="Prove">Código del proveedor</param>
    ''' <remarks>Llamada desde Recepcion.aspx. Máx. 1 seg.</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function AnularRecepcion(ByVal Albaran As String, ByVal FechaAlbaran As String, ByVal Prove As String) As String
        Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oRecepciones As CRecepciones = FSNServer.Get_Object(GetType(FSNServer.CRecepciones))
        Dim Fecha As DateTime = CType(FormatDateTime(CType(FechaAlbaran, DateTime), DateFormat.ShortDate), DateTime)

        Return oRecepciones.EliminarRecepcionesAlbaran(Albaran, Fecha, Prove, FSNServer.TipoAcceso.giRecepAprov, FSNServer.TipoAcceso.giRecepDirecto, User.CodPersona, User.Cod, CType(User.Idioma, FSNLibrary.Idioma), FSNServer.TipoAcceso)
    End Function

    ''' Revisado por:blp. Fecha: 09/10/2012
    ''' <summary>
    ''' Anular línea recepción
    ''' </summary>
    ''' <param name="idLineaRecep">id de la línea a eliminar</param>
    ''' <remarks>Llamada desde Recepcion.aspx. Máx. 1 seg.</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub AnularLineaRecepcion(ByVal idLineaRecep As Integer)
        Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oRecepcion As CRecepcion = FSNServer.Get_Object(GetType(FSNServer.CRecepcion))
        Dim oIntegracion As Integracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))

        oRecepcion.EliminarLineaDeRecepcion(idLineaRecep, User.Cod)
        'Llamamos al servicio de integraciÃ³n WCF.
        oIntegracion.LlamarFSIS(TablasIntegracion.Rec_Aprov, 0, , , , idLineaRecep)
    End Sub

    ''' Revisado por: blp. Fecha: 19/10/2012
    ''' <summary>
    ''' Bloquea o desbloquea la facturación de las órdenes vinculadas a un albarán
    ''' </summary>
    ''' <param name="Bloquear">True->Bloquea, False->Desbloquea</param>
    ''' <param name="Albaran">Nº albarán</param>
    ''' <param name="FechaAlbaran">Fecha</param>
    ''' <param name="Prove">Código del proveedor</param>
    ''' <param name="Observaciones">Observaciones de bloqueo/desbloqueo</param>
    ''' <returns>Resultado del proceso</returns>
    ''' <remarks>Llamada desde REcepcion.aspx.vb. Máximo 0,3 seg.</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function BloquearFacturacion(ByVal Bloquear As Boolean, ByVal Albaran As String, ByVal OrdenId As Long, _
                                               ByVal Observaciones As String) As String
        Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oRecepcion As CRecepcion = FSNServer.Get_Object(GetType(FSNServer.CRecepcion))

        Dim rdoBloqueoAlbaran As Integer

        'BLOQUEO o DESBLOQUEO
        If Bloquear Then
            rdoBloqueoAlbaran = oRecepcion.BloquearFacturacionAlbaran(Albaran, OrdenId, Observaciones)
        Else
            rdoBloqueoAlbaran = oRecepcion.DesBloquearFacturacionAlbaran(Albaran, OrdenId)
        End If
        Return rdoBloqueoAlbaran

    End Function
#End Region
#Region "Enum"
    Private Enum Acciones
        AccionNula = 0
        NoAnularOrden = 1
        AnularOrden = 2
        AnularRecepcion = 3
        AnularLinea = 4
        BloquearFacturacionAlbaran = 5
        DesBloquearFacturacionAlbaran = 6
    End Enum
#End Region

    ''' <summary>
    ''' Muestra el panel de adjuntos cuando pinchamos en la columna de adjuntos del grid en el caso de que hubiera
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnMostrarPopUpAdjuntos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMostrarPopUpAdjuntos.Click
        Dim oAdjuntos As CAdjuntos = FSNServer.Get_Object(GetType(FSNServer.CAdjuntos))
        Dim dsAdjuntos As DataSet
        If (hid_adjuntosRecepcion.Value) Then
            dsAdjuntos = oAdjuntos.DevolverAdjuntosRecepcionPedido(hid_OrdenEntrega.Value)
            repAdjuntosLineaPedido.DataSource = Nothing
            repAdjuntosLineaPedido.DataBind()
            repAdjuntosOrden.DataSource = Nothing
            repAdjuntosOrden.DataBind()
            If dsAdjuntos.Tables("ADJUNTOS_RECEPCION_PEDIDO").Rows.Count > 0 Then
                repAdjuntosRecepcion.DataSource = dsAdjuntos.Tables("ADJUNTOS_RECEPCION_PEDIDO")
                repAdjuntosRecepcion.DataBind()
            End If
        Else
            dsAdjuntos = oAdjuntos.DevolverAdjuntosLineaYOrden(hid_OrdenEntrega.Value, hid_lineaPedido.Value)
            If dsAdjuntos.Tables("ADJUNTOS_LINEA").Rows.Count > 0 Then
                repAdjuntosLineaPedido.DataSource = dsAdjuntos.Tables("ADJUNTOS_LINEA")
                repAdjuntosLineaPedido.DataBind()
            End If
            If dsAdjuntos.Tables("ADJUNTOS_ORDEN").Rows.Count > 0 Then
                repAdjuntosOrden.DataSource = dsAdjuntos.Tables("ADJUNTOS_ORDEN")
                repAdjuntosOrden.DataBind()
            End If
            repAdjuntosRecepcion.DataSource = Nothing
            repAdjuntosRecepcion.DataBind()
        End If
        lblTituloAdjuntos.Text = Textos(257)

        updpnlPopupAdjuntos.Update()
        mpeAdj.Show()
    End Sub


    ''' <summary>
    ''' Procedimiento que se ejecuta al hacer click en el link del adjunto del repeater de adjuntos de lineas de pedido
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub repAdjuntosLineaPedido_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles repAdjuntosLineaPedido.ItemCommand
        Select Case e.CommandName
            Case "DescargarAdjunto"
                Dim AdjunDescargar As FSNServer.Adjunto = Me.FSNServer.Get_Object(GetType(FSNServer.Adjunto))
                AdjunDescargar.Id = CLng(DirectCast(e.Item.FindControl("hid_idAdjuntoLineaPedido"), HiddenField).Value)
                AdjunDescargar.Nombre = DirectCast(e.Item.FindControl("hid_NombreAdjuntoLineaPedido"), HiddenField).Value
                AdjunDescargar.dataSize = DirectCast(e.Item.FindControl("hid_DatasizeAdjuntoLineaPedido"), HiddenField).Value
                Dim sNombre As String = AdjunDescargar.Nombre
                AdjunDescargar.EscribirADisco(ConfigurationManager.AppSettings("temp") & "\", sNombre, TiposDeDatos.Adjunto.Tipo.Linea_Pedido)
                Response.AddHeader("content-disposition", "attachment; filename=""" & AdjunDescargar.Nombre & """")
                Response.ContentType = "application/octet-stream"
                Response.WriteFile(ConfigurationManager.AppSettings("temp") & "\" & sNombre)
                Response.End()
        End Select
    End Sub

    ''' <summary>
    ''' Procedimiento que se ejecuta al hacer click en el link del adjunto del repeater de adjuntos de recepcion
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub repAdjuntosRecepcion_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles repAdjuntosRecepcion.ItemCommand
        Select Case e.CommandName
            Case "DescargarAdjunto"
                Dim AdjunDescargar As FSNServer.Adjunto = Me.FSNServer.Get_Object(GetType(FSNServer.Adjunto))
                AdjunDescargar.Id = CLng(DirectCast(e.Item.FindControl("hid_idAdjuntoRecepcion"), HiddenField).Value)
                AdjunDescargar.Nombre = DirectCast(e.Item.FindControl("hid_NombreAdjuntoRecepcion"), HiddenField).Value
                AdjunDescargar.dataSize = DirectCast(e.Item.FindControl("hid_DatasizeAdjuntoRecepcion"), HiddenField).Value
                Dim sNombre As String = AdjunDescargar.Nombre
                AdjunDescargar.EscribirADisco(ConfigurationManager.AppSettings("temp") & "\", sNombre, TiposDeDatos.Adjunto.Tipo.RecepcionPedido)
                Response.AddHeader("content-disposition", "attachment; filename=""" & AdjunDescargar.Nombre & """")
                Response.AddHeader("Content-Length", AdjunDescargar.dataSize)
                Response.AddHeader("cache-control", "private")
                Response.AddHeader("Expires", "0")
                Response.AddHeader("Pragma", "cache")
                Response.AddHeader("Accept-Ranges", "none")
                Response.ContentType = "application/octet-stream"
                Response.WriteFile(ConfigurationManager.AppSettings("temp") & "\" & sNombre)
                Response.End()
        End Select
    End Sub
    ''' <summary>
    ''' Procedimiento que se ejecuta al hacer click en el link del adjunto del repeater de adjuntos de pedido(orden de entrega)
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub repAdjuntosOrden_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles repAdjuntosOrden.ItemCommand
        Select Case e.CommandName
            Case "DescargarAdjunto"
                Dim AdjunDescargar As FSNServer.Adjunto = Me.FSNServer.Get_Object(GetType(FSNServer.Adjunto))
                AdjunDescargar.Id = DirectCast(e.Item.FindControl("hid_idAdjuntoPedido"), HiddenField).Value
                AdjunDescargar.Nombre = DirectCast(e.Item.FindControl("hid_NombreAdjuntoPedido"), HiddenField).Value
                AdjunDescargar.dataSize = DirectCast(e.Item.FindControl("hid_DatasizeAdjuntoPedido"), HiddenField).Value
                Dim sNombre As String = AdjunDescargar.Nombre
                AdjunDescargar.EscribirADisco(ConfigurationManager.AppSettings("temp") & "\", sNombre, TiposDeDatos.Adjunto.Tipo.Orden_Entrega)
                Response.AddHeader("content-disposition", "attachment; filename=""" & AdjunDescargar.Nombre & """")
                Response.ContentType = "application/octet-stream"
                Response.WriteFile(ConfigurationManager.AppSettings("temp") & "\" & sNombre)
                Response.End()
        End Select
    End Sub

    ''' <summary>
    ''' Procedimiento que se ejecuta por cada item del datasource de adjuntos de recepcion
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub repAdjuntosRecepcion_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repAdjuntosRecepcion.ItemDataBound
        If e.Item.ItemType = ListItemType.Header Then
            imgAdjunto.ImageUrl = ConfigurationManager.AppSettings("RUTA") & "Images/AdjuntarArchivo.png"
            ImgBtnCerrarpnlAdjuntos.ImageUrl = ConfigurationManager.AppSettings("RUTA") & "Images/Bt_Cerrar.png"
            DirectCast(e.Item.FindControl("lblAdjuntosRecepcion"), Label).Text = Textos(96) 'Archivos adjuntos RECEPCION
        End If
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim link As LinkButton
            Dim lblFecha As Label
            link = DirectCast(e.Item.FindControl("linkAdjuntoRecepcion"), LinkButton)
            link.Text = e.Item.DataItem("NOM")
            lblFecha = DirectCast(e.Item.FindControl("lblFechaAdjRecepcion"), Label)
            lblFecha.Text = FormatDate(e.Item.DataItem("FECACT"), Me.Usuario.DateFormat)
            DirectCast(e.Item.FindControl("hid_idAdjuntoRecepcion"), HiddenField).Value = e.Item.DataItem("ID")
            DirectCast(e.Item.FindControl("hid_NombreAdjuntoRecepcion"), HiddenField).Value = e.Item.DataItem("NOM")
            DirectCast(e.Item.FindControl("hid_DatasizeAdjuntoRecepcion"), HiddenField).Value = e.Item.DataItem("DATASIZE")
            'DirectCast(e.Item.FindControl("hid_DataAdjuntoRecepcion"), HiddenField).Value = e.Item.DataItem("DATA")
            If DBNullToStr(e.Item.DataItem("COM")) = String.Empty Then
                DirectCast(e.Item.FindControl("imgComentarioAdjuntoRecepcion"), Image).Visible = False
            Else
                DirectCast(e.Item.FindControl("imgComentarioAdjuntoRecepcion"), Image).ToolTip = DBNullToStr(e.Item.DataItem("COM"))
                DirectCast(e.Item.FindControl("imgComentarioAdjuntoRecepcion"), Image).ImageUrl = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/coment.gif"
            End If
        End If
    End Sub

    ''' <summary>
    ''' Procedimiento que se ejecuta por cada item del datasource de adjuntos de lineas de pedido
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub repAdjuntosLineaPedido_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repAdjuntosLineaPedido.ItemDataBound
        If e.Item.ItemType = ListItemType.Header Then
            imgAdjunto.ImageUrl = ConfigurationManager.AppSettings("RUTA") & "images/AdjuntarArchivo.png"
            ImgBtnCerrarpnlAdjuntos.ImageUrl = ConfigurationManager.AppSettings("RUTA") & "images/Bt_Cerrar.png"
            DirectCast(e.Item.FindControl("lblAdjuntosLineaPedido"), Label).Text = Textos(238)
        End If
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim link As LinkButton
            Dim lblFecha As Label
            link = DirectCast(e.Item.FindControl("linkAdjuntoLineaPedido"), LinkButton)
            link.Text = e.Item.DataItem("NOM")
            lblFecha = DirectCast(e.Item.FindControl("lblFechaAdjLineaPedido"), Label)
            lblFecha.Text = FormatDate(e.Item.DataItem("FECACT"), Me.Usuario.DateFormat)
            DirectCast(e.Item.FindControl("hid_idAdjuntoLineaPedido"), HiddenField).Value = e.Item.DataItem("ID")
            DirectCast(e.Item.FindControl("hid_NombreAdjuntoLineaPedido"), HiddenField).Value = e.Item.DataItem("NOM")
            DirectCast(e.Item.FindControl("hid_DatasizeAdjuntoLineaPedido"), HiddenField).Value = e.Item.DataItem("DATASIZE")
            If DBNullToStr(e.Item.DataItem("COM")) = String.Empty Then
                DirectCast(e.Item.FindControl("imgComentarioAdjuntoLineaPedido"), Image).Visible = False
            Else
                DirectCast(e.Item.FindControl("imgComentarioAdjuntoLineaPedido"), Image).ToolTip = DBNullToStr(e.Item.DataItem("COM"))
                DirectCast(e.Item.FindControl("imgComentarioAdjuntoLineaPedido"), Image).ImageUrl = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/coment.gif"
            End If
        End If
    End Sub
    ''' <summary>
    ''' Procedimiento que se ejecuta por cada item del datasource de adjuntos de pedido (orden de entrega)
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub repAdjuntosOrden_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repAdjuntosOrden.ItemDataBound
        If e.Item.ItemType = ListItemType.Header Then
            imgAdjunto.ImageUrl = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/AdjuntarArchivo.png"
            ImgBtnCerrarpnlAdjuntos.ImageUrl = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/Bt_Cerrar.png"
            DirectCast(e.Item.FindControl("lblAdjuntosPedido"), Label).Text = Textos(237)
        End If
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim link As LinkButton
            Dim lblFecha As Label
            link = DirectCast(e.Item.FindControl("linkAdjuntoPedido"), LinkButton)
            link.Text = e.Item.DataItem("NOM")
            lblFecha = DirectCast(e.Item.FindControl("lblFechaAdjPedido"), Label)
            lblFecha.Text = FormatDate(e.Item.DataItem("FECACT"), Me.Usuario.DateFormat)
            DirectCast(e.Item.FindControl("hid_idAdjuntoPedido"), HiddenField).Value = e.Item.DataItem("ID")
            DirectCast(e.Item.FindControl("hid_NombreAdjuntoPedido"), HiddenField).Value = e.Item.DataItem("NOM")
            DirectCast(e.Item.FindControl("hid_DatasizeAdjuntoPedido"), HiddenField).Value = e.Item.DataItem("DATASIZE")
            If DBNullToStr(e.Item.DataItem("COM")) = String.Empty Then
                DirectCast(e.Item.FindControl("imgComentarioAdjuntoPedido"), Image).Visible = False
            Else
                DirectCast(e.Item.FindControl("imgComentarioAdjuntoPedido"), Image).ToolTip = DBNullToStr(e.Item.DataItem("COM"))
                DirectCast(e.Item.FindControl("imgComentarioAdjuntoPedido"), Image).ImageUrl = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/coment.gif"
            End If
        End If
    End Sub
    ''' <summary>
    ''' Muestra el panel de de detalle del articulo cuando pinchamos en la columna de codigo de articulo del grid
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnMostrarDetalleArticulo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMostrarDetalleArticulo.Click
        pnlDetalleArticulo.CargarDetalleArticulo(DsetPedidos.Tables("ORDENES"), hid_lineaPedido.Value)
    End Sub

    Private Sub WebDocumentExporter1_GridRecordItemExporting(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.DocumentGridRecordItemExportingEventArgs) Handles WebDocumentExporter1.GridRecordItemExporting
        Select Case e.GridCell.Column.Key
            Case "Key_ORDENEST"
                Dim sEstado As String = String.Empty
                Select Case CType(e.GridCell.Value.ToString, TipoEstadoOrdenEntrega)
                    Case TipoEstadoOrdenEntrega.EmitidoAlProveedor
                        sEstado = Textos(119) 'Emitido al proveedor
                    Case TipoEstadoOrdenEntrega.AceptadoPorProveedor
                        sEstado = Textos(24) 'Pedido Aceptado por el proveedor
                    Case TipoEstadoOrdenEntrega.EnCamino
                        sEstado = Textos(25) 'En camino
                    Case TipoEstadoOrdenEntrega.EnRecepcion
                        sEstado = Textos(26) 'Recibido parcialmente
                    Case TipoEstadoOrdenEntrega.RecibidoYCerrado
                        sEstado = Textos(27) 'Cerrado
                End Select
                e.ExportValue = sEstado
            Case "Key_TIPO"
                Dim sTipo As String = String.Empty
                Select Case e.GridCell.Value.ToString
                    Case 0
                        sTipo = Textos(129) 'Pedidos negociados
                    Case 2
                        sTipo = Textos(131) 'Pedidos directos
                    Case 1
                        If e.GridCell.Row.DataItem.ITEM("ARTCOD") = "" Then
                            sTipo = Textos(255) 'Pedidos de catálogo libres
                        Else
                            sTipo = Textos(128) 'Pedidos de catálogo negociados
                        End If
                End Select
                e.ExportValue = sTipo
            Case "Key_PLAN_ENTREGA", "Key_TIENEADJ"
                e.ExportValue = If(DBNullToSomething(e.GridCell.Value) <> Nothing, Textos(73), Textos(74))
            Case "Key_ANYOPEDIDOORDEN", "Key_DESTCOD", "Key_APROVISIONADOR", "Key_PROVEDEN"
                e.ExportValue = IIf(Not IsDBNull(e.GridCell.Value), e.GridCell.Value.ToString & " ", "")
            Case "Key_ARTDEN"
                e.ExportValue = IIf(DBNullToStr(e.GridCell.Row.DataItem.ITEM("ARTDEN")) <> "", DBNullToStr(e.GridCell.Row.DataItem.ITEM("ARTDEN")), "")
            Case "Key_TIPOPEDIDO"
                e.ExportValue = DBNullToStr(e.GridCell.Row.DataItem.ITEM("TIPOPEDIDO_DEN"))
            Case Else
                Select Case e.GridCell.Column.Type
                    Case System.Type.GetType("System.Boolean")
                        e.ExportValue = IIf(e.GridCell.Value, Textos(73), Textos(74))
                    Case System.Type.GetType("System.DateTime")
                        If IsDBNull(e.ExportValue) Then
                            e.ExportValue = ""
                        Else
                            e.ExportValue = FormatDate(e.ExportValue, FSNUser.DateFormat)
                        End If
                    Case Else
                End Select
        End Select
    End Sub

    Private Sub WebExcelExporter1_GridRecordItemExported(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GridRecordItemExportedEventArgs) Handles WebExcelExporter1.GridRecordItemExported
        e.WorksheetCell.CellFormat.Font.Bold = Infragistics.Documents.Excel.ExcelDefaultableBoolean.False
        e.WorksheetCell.CellFormat.Font.Color = Drawing.Color.Black

        Select Case e.GridCell.Column.Key
            Case "Key_ORDENEST"
                Dim sEstado As String = String.Empty
                Select Case CType(e.GridCell.Value.ToString, TipoEstadoOrdenEntrega)
                    Case TipoEstadoOrdenEntrega.EmitidoAlProveedor
                        sEstado = Textos(119) 'Emitido al proveedor
                    Case TipoEstadoOrdenEntrega.AceptadoPorProveedor
                        sEstado = Textos(24) 'Pedido Aceptado por el proveedor
                    Case TipoEstadoOrdenEntrega.EnCamino
                        sEstado = Textos(25) 'En camino
                    Case TipoEstadoOrdenEntrega.EnRecepcion
                        sEstado = Textos(26) 'Recibido parcialmente
                    Case TipoEstadoOrdenEntrega.RecibidoYCerrado
                        sEstado = Textos(27) 'Cerrado
                End Select
                e.WorksheetCell.Value = sEstado
            Case "Key_TIPO"
                Dim sTipo As String = String.Empty
                Select Case e.GridCell.Value.ToString
                    Case 0
                        sTipo = Textos(129) 'Pedidos negociados
                    Case 2
                        sTipo = Textos(131) 'Pedidos directos
                    Case 1
                        If e.GridCell.Row.DataItem.ITEM("ARTCOD") = "" Then
                            sTipo = Textos(255) 'Pedidos de catálogo libres
                        Else
                            sTipo = Textos(128) 'Pedidos de catálogo negociados
                        End If
                End Select
                e.WorksheetCell.Value = sTipo
            Case "Key_PLAN_ENTREGA", "Key_TIENEADJ"
                e.WorksheetCell.Value = If(DBNullToSomething(e.GridCell.Value) <> Nothing, Textos(73), Textos(74))
            Case "Key_ARTDEN"
                e.WorksheetCell.Value = IIf(DBNullToStr(e.GridCell.Row.DataItem.ITEM("ARTDEN")) <> "", DBNullToStr(e.GridCell.Row.DataItem.ITEM("ARTDEN")), "")
            Case "Key_TIPOPEDIDO"
                e.WorksheetCell.Value = DBNullToStr(e.GridCell.Row.DataItem.ITEM("TIPOPEDIDO_DEN"))
            Case "Key_CCDEN"
                If Me.Acceso.gbAccesoFSSM Then
                    e.WorksheetCell.Value = DBNullToStr(e.GridCell.Row.DataItem.ITEM("CCDEN"))
                End If
            Case Else
                Select Case e.GridCell.Column.Type
                    Case System.Type.GetType("System.Boolean")
                        e.WorksheetCell.Value = If(e.GridCell.Value, Textos(73), Textos(74))
                    Case System.Type.GetType("System.DateTime")
                        e.WorksheetCell.CellFormat.FormatString = FSNUser.DateFormat.ShortDatePattern
                End Select
        End Select
    End Sub

    ''' Revisado por: blp. Fecha: 17/04/2012
    ''' <summary>
    ''' Vamos a obtener la información nacesaria para los dropdown desde el stored pero sin recuperar ni almacenar en cache el resto de información (órdenes, lineas e imputación)
    ''' </summary>
    ''' <param name="CodPersona">Código de la persona</param>
    ''' <param name="Idioma">idioma en el que devolver el dataset</param>
    ''' <remarks>Llamada desde InicializarControles. Máx. 0,5 seg.</remarks>
    Private Function getDropDownsInfo(ByVal CodPersona As String, ByVal Idioma As FSNLibrary.Idioma) As DataSet
        Dim oOrdenes As COrdenes = Me.FSNServer.Get_Object(GetType(FSNServer.COrdenes))
        Dim dsInfoDropDowns As New DataSet
        If HttpContext.Current.Cache("DsetSeguimientoDropDownsInfo_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
            dsInfoDropDowns = oOrdenes.ObtenerDatosCombosSeguimiento(Idioma.ToString(), CodPersona, Usuario.PermisoVerPedidosCCImputables)
            Me.InsertarEnCache("DsetSeguimientoDropDownsInfo_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString(), dsInfoDropDowns)
        Else
            dsInfoDropDowns = HttpContext.Current.Cache("DsetSeguimientoDropDownsInfo_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString())
        End If
        Return dsInfoDropDowns
    End Function

    ''' <summary>
    ''' Determinamos la visibilidad del almacén
    ''' </summary>
    ''' <param name="dsLineasPedido">Lista de líneas a mostrar</param>
    ''' <returns>True-> Visible, Falta-> No visible</returns>
    ''' <remarks>Llamada desde REcepcion.aspx.vb. Maximo 0,2 seg.</remarks>
    Private Function AlmacenVisible(ByVal dsLineasPedido As DataSet) As Boolean
        Dim hayPedidosRecepcionables = From lineas In dsLineasPedido.Tables(0) _
                                       Where lineas("PEDIDO_ALMACENABLE") <> 0 _
                                       Select lineas("ORDEN") Distinct.ToList
        If hayPedidosRecepcionables.Any Then
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>Guardamos en caché el id del Activo seleccionado</summary>
    ''' <param name="idActivo">Id del activo seleccionado</param>
    ''' <remarks>Llamada desde el javascript de EPWeb\EmisionPedido.aspx contenido en el archivo SMWebControls\SMControls.js. 
    ''' Tiempo Máx: 0,1 seg. 
    ''' Se hace uso de la variable de session creada en: SMWebControls\SMActivos\_btnAceptar_Click</remarks>
    <System.Web.Services.WebMethodAttribute(), _
        System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Sub guardarActivoSeleccionado(ByVal idActivo As String)
        HttpContext.Current.Session("idActivo") = idActivo
    End Sub

    ''' Revisado por: blp. Fecha: 20/05/2013
    ''' <summary>
    ''' Comprueba que el objeto que ha provocado el postback asíncrono es uno de los siguientes. Si es así, devuelve true para que salga de la función y no siga la ejecución
    ''' </summary>
    ''' <returns>True/False</returns>
    ''' <remarks>Llamada desde: Page_Load(); Tiempo máx: 0,01 sg</remarks>
    Function CheckAsyncPostBackElementToExit()
        If (ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$SMActivos_btnBuscar") > 0 _
                        OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$SMActivos_btnAceptar") > 0 _
                        OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$SMPartidasPresupuestarias_btnBuscar") > 0 _
                        OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$_imgbtnNext") > 0 _
                        OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$_imgbtnLast") > 0 _
                        OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$_imgbtnFirst") > 0 _
                        OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$_imgbtnPrev") > 0 _
                        OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$_ddlOrdenarPor") > 0) Then
            Return True
        Else
            Return False
        End If
    End Function

    ''' Revisado por: blp. 20/05/2013
    ''' <summary>
    '''Procedimiento que registra controles utilizados en la página como sincronos o asincronos depende de las necesidades. Tambien registra eventos de cliente
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde:Page_Load
    ''' Tiempo máximo: 0 seg</remarks>
    Public Sub RegistrarControles()
        ScriptMgr.RegisterAsyncPostBackControl(SMActivos)
        'ScriptMgr.RegisterPostBackControl(BtnAcepAdjunPed)
    End Sub
    ''' <summary>
    ''' Registramos un script que va a modificar el margin de la cabecera del panel de líneas a recepcionar para que, cuando salga un scroll vertical en el panel que contiene las líneas, no se descuadren líneas y cabeceras
    ''' Incidencia 32941
    ''' </summary>
    ''' <remarks>Llamada desde Load. Maximo 0,1seg</remarks>
    Private Sub CargarScriptMarginCabeceraPanelRecepcion()
        'Registramos un script que va a modificar el ancho del control cuando dl
        Dim sScript As String

        sScript = "$(document).ready(function () {"
        sScript += vbCrLf
        sScript += "    $('[id$=pnlRegistrarRecepcion]').live('click', function () {"
        sScript += vbCrLf
        'Asociamos la función al fin de postback para que se ejecute cuando el panel con las líneas ya se ha construido y podemos saber si tiene o no scroll vertical
        sScript += "        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(marginCabeceraPanelRecepcion);"
        sScript += vbCrLf
        sScript += "        function marginCabeceraPanelRecepcion(sender, args){"
        sScript += vbCrLf
        sScript += "            var opnlLineasPedido = $get('pnlLineasPedido');"
        sScript += vbCrLf
        If pnlLblRegistroRecepcionAlmacen.Visible Then
            sScript += "            var oControl = $get('" & pnlLblRegistroRecepcionAlmacen.ClientID & "');"
        Else
            sScript += "           var oControl = $get('" & pnlRegistroRecepcionCantidadRecepcionar.ClientID & "');"
        End If
        sScript += vbCrLf
        sScript += "           if(opnlLineasPedido){"
        sScript += vbCrLf
        sScript += "                if(oControl){"
        sScript += vbCrLf
        sScript += "                    if(opnlLineasPedido.scrollHeight > opnlLineasPedido.clientHeight){"
        sScript += vbCrLf
        sScript += "                        oControl.style.margin='0px 25px 0px 0px';"
        sScript += vbCrLf
        sScript += "                    }else{"
        sScript += vbCrLf
        sScript += "                        oControl.style.margin='0px 5px 0px 0px';"
        sScript += vbCrLf
        sScript += "                    }"
        sScript += vbCrLf
        sScript += "               }"
        sScript += vbCrLf
        sScript += "            }"
        sScript += vbCrLf
        'Quitamos la asociación con el fin de postback para que se ejecute solo cuando se muestra el panel, no en cada postback
        sScript += "            Sys.WebForms.PageRequestManager.getInstance().remove_endRequest(marginCabeceraPanelRecepcion);"
        sScript += vbCrLf
        sScript += "        }"
        sScript += vbCrLf
        sScript += "    });"
        sScript += vbCrLf
        sScript += "});"
        sScript += vbCrLf
        Dim oScript As New HtmlGenericControl("script")
        oScript.InnerHtml = sScript.ToString
        Me.Controls.Add(oScript)
    End Sub

    ''' <summary>
    ''' Procedimiento que se lanza cuando se acepta un archivo adjunto de pedido, añadiendolo en la base de datos, en el objeto de adjuntosPedido y tambien en la página
    ''' </summary>
    ''' <param name="sender">El propio botón pulsado</param>
    ''' <param name="e">El evento click del botón</param>
    ''' <remarks>
    ''' Llamada desde:La propia página, cuando se pulsa el botón aceptar de los adjuntos del pedido
    ''' Tiempo máximo:0,3 seg</remarks>
    Protected Sub BtnAcepAdjunPed_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnAcepAdjunPed.Click
    End Sub

    ''' <summary>
    ''' Suma uno al VisibleIndex
    ''' </summary>
    ''' <param name="VisibleIndex">Visible Index de la columna q esta siendo creada</param>
    ''' <returns>Suma uno al VisibleIndex</returns>
    ''' <remarks>>Llamada desde Load. Maximo 0,1seg</remarks>
    Private Function SiguiVisibleIndex(ByRef VisibleIndex As Integer) As Integer
        VisibleIndex = VisibleIndex + 1
        Return VisibleIndex
    End Function

End Class