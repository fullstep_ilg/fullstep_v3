﻿Imports Fullstep.FSNLibrary
Imports Fullstep.FSNServer
Imports Infragistics.Web.UI
Imports System.Web.Script.Serialization

Partial Public Class Cesta
    Inherits FSEPPage

#Region " Variables privadas utilizadas en la página "
    Private _Empresas As CEmpresas
    Private _Persona As CPersona
    Private _Destinos As CDestinos
    Private _Cesta As COrdenes
    Private _CestaTemporal As COrdenes
    Private _ProveedoresPedidoLibre As DataTable
    Private _script As StringBuilder = New StringBuilder()
    Private dtLineas As DataTable
    Private _PartidaPresupuestariaDEN As String = String.Empty
    Private _LineasAEliminarDeFavorito As New List(Of Long)
#End Region
    Private ReadOnly Property Persona() As CPersona
        Get
            If _Persona Is Nothing Then
                _Persona = FSNServer.Get_Object(GetType(FSNServer.CPersona))
                _Persona.Cod = Me.Usuario.CodPersona
                _Persona.CargarPersona()
            End If
            Return _Persona
        End Get
    End Property
    Private ReadOnly Property Empresas() As CEmpresas
        Get
            If _Empresas Is Nothing Then
                If HttpContext.Current.Cache("_Empresas" & FSNUser.CodPersona) Is Nothing Then
                    Persona.CargarEmpresas(Me.PargenSM, FSNUser.PedidosOtrasEmp)
                    _Empresas = Me.Persona.Empresas

                    Me.InsertarEnCache("_Empresas" & FSNUser.CodPersona, _Empresas)
                Else
                    _Empresas = HttpContext.Current.Cache("_Empresas" & FSNUser.CodPersona)
                End If
            End If

            Return _Empresas
        End Get
    End Property
    'Propiedad que almacena es una lista los id de las lineas que se han eliminado de un pedido favorito, 
    'se utilizaran cuando se quiera guardar ese pedido favorito para eliminarlas en ese momento
    Private Property LineasAEliminarDeFavorito() As List(Of Long)
        Get
            If HttpContext.Current.Cache("_LineasAEliminarDeFavorito_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) Is Nothing Then
                Me.InsertarEnCache("_LineasAEliminarDeFavorito_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString, _LineasAEliminarDeFavorito)
                Return _LineasAEliminarDeFavorito
            Else
                _LineasAEliminarDeFavorito = HttpContext.Current.Cache("_LineasAEliminarDeFavorito_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
                Return _LineasAEliminarDeFavorito
            End If
        End Get
        Set(ByVal value As List(Of Long))
            If HttpContext.Current.Cache("_LineasAEliminarDeFavorito_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) Is Nothing Then
                Me.InsertarEnCache("_LineasAEliminarDeFavorito_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString, value)
            Else
                HttpContext.Current.Cache("_LineasAEliminarDeFavorito_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) = value
            End If
        End Set
    End Property
    Private ReadOnly Property Destinos() As CDestinos
        Get
            If _Destinos Is Nothing Then
                If HttpContext.Current.Cache("_Destinos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) Is Nothing Then
                    Persona.CargarDestinos(Me.Idioma)
                    _Destinos = Me.Persona.Destinos
                    If _Destinos IsNot Nothing Then Me.InsertarEnCache("_Destinos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString, _Destinos)
                Else
                    _Destinos = HttpContext.Current.Cache("_Destinos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
                End If
            End If

            Return _Destinos
        End Get
    End Property
    Private ReadOnly Property ActivosEmpresa(ByVal idEmpresa As Integer) As FSNServer.Activos
        Get
            If HttpContext.Current.Cache("ActivosEmpresa_" & idEmpresa.ToString & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                Dim oAct As FSNServer.Activos = Me.FSNServer.Get_Object(GetType(FSNServer.Activos))
                oAct.ObtenerActivos(Me.Usuario.Idioma.ToString(), idEmpresa, String.Empty, True)
                Me.InsertarEnCache("ActivosEmpresa_" & idEmpresa.ToString & "_" & Me.Usuario.Idioma.ToString(), oAct)
                Return oAct
            Else
                Return HttpContext.Current.Cache("ActivosEmpresa_" & idEmpresa.ToString & "_" & Me.Usuario.Idioma.ToString())
            End If
        End Get
    End Property
    Private ReadOnly Property ActivosCentroCoste(ByVal CentroCoste As String) As FSNServer.Activos
        Get
            If HttpContext.Current.Cache("ActivosCentroCoste_" & CentroCoste & "_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                Dim oAct As FSNServer.Activos = Me.FSNServer.Get_Object(GetType(FSNServer.Activos))
                oAct.ObtenerActivos(Me.Usuario.Idioma.ToString(), Nothing, CentroCoste, True)
                Me.InsertarEnCache("ActivosCentroCoste_" & CentroCoste & "_" & Me.Usuario.Idioma.ToString(), oAct)
                Return oAct
            Else
                Return HttpContext.Current.Cache("ActivosCentroCoste_" & CentroCoste & "_" & Me.Usuario.Idioma.ToString())
            End If
        End Get
    End Property
    'Devuelve el valor de autofacturacion del proveedor pasado como parámetro
    Private ReadOnly Property ProveAutofacturacion(ByVal CodProve As String) As Boolean
        Get
            If ViewState("AutofacProve" & CodProve) IsNot Nothing Then
                Return ViewState("AutofacProve" & CodProve)
            Else
                Dim oProve As CProveedor = FSNServer.Get_Object(GetType(CProveedor))
                oProve.Cod = CodProve
                oProve.getAutofactura()
                ViewState("AutofacProve" & CodProve) = oProve.Autofactura
                Return oProve.Autofactura
            End If
        End Get
    End Property
    Public ReadOnly Property Cesta() As COrdenes
        Get
            '1. Cogemos los valores de bdd de la cesta o desde la variable de sesión del favorito
            Recuperar_Cesta()

            '2. Se recogen los cambios de datos habidos en los controles
            For Each oOrden As COrden In _Cesta
                Dim dlOrden As DataListItem = dlCesta_FindItem(oOrden.Prove, oOrden.Moneda, oOrden.Categoria, oOrden.Nivel, oOrden.ID)
                If Not dlOrden Is Nothing Then
                    oOrden.Empresa = 0

                    Dim bProveAutofactura As Boolean = ProveAutofacturacion(oOrden.Prove)

                    oOrden.Obs = ""
                    For Each oLinea As CLinea In oOrden.Lineas
                        Dim grLinea As GridViewRow = GridLineas_FindItem(dlOrden.FindControl("GridLineas"), oLinea.ID)
                        If grLinea IsNot Nothing Then
                            oLinea.Obs = ""
                            oLinea.DescripcionVisible = CType(grLinea.FindControl("LblArticulo"), LinkButton).Text
                            oLinea.FC = CType(grLinea.FindControl("lnkUnidad"), FSNWebControls.FSNLinkTooltip).Attributes("fc")
                            If FSNUser.ModificarPrecios AndAlso CType(oLinea.ModificarPrecio, Boolean) Then
                                If oLinea.FC <> 0 Then
                                    oLinea.PrecUc = CDbl(CType(grLinea.FindControl("WNumEdPrecArt_DblValue"), HiddenField).Value) / oLinea.FC
                                Else
                                    oLinea.PrecUc = CType(grLinea.FindControl("WNumEdPrecArt_DblValue"), HiddenField).Value
                                End If
                            End If

                            oLinea.Dest = grLinea.Attributes("DESTINO")
                            oLinea.DestDen = grLinea.Attributes("DESTINO_DEN")

                            oLinea.Almacen = "0"
                            oLinea.IdAlmacen = 0
                            oLinea.FecEntrega = Date.MinValue

                            If oLinea.TipoRecepcion = 0 Or (oLinea.EsLineaPedidoAbierto AndAlso oLinea.TipoRecepcion = 3) Then
                                oLinea.CantPed = CType(grLinea.FindControl("wnumCantidad_DblValue"), HiddenField).Value
                                oLinea.UP = CType(grLinea.FindControl("lnkUnidad"), FSNWebControls.FSNLinkTooltip).Text
                            Else
                                oLinea.ImportePedido = CType(grLinea.FindControl("LblTotLin_DblValue"), HiddenField).Value.ToString()
                            End If
                        End If
                    Next
                End If
            Next
            Return _Cesta
        End Get
    End Property
    ''' Revisado por: blp. Fecha: 19/12/2011
    ''' <summary>
    ''' Método mediante la cual recuperamos desde la bdd los datos de la cesta o desde una variable de sesión los datos del favorito a mostrar en pantalla
    ''' que será usado en la aplicación cuando queramos referirnos a los datos de bdd y los adjuntos (de bdd y pantalla) (la propiedad Cesta en cambio, combina los datos de bdd y todos los datos de pantalla)
    ''' </summary>
    ''' <remarks>Llamada desde EmisiopnPedido.aspx.vb. Máx. 1 seg.</remarks>
    Private Sub Recuperar_Cesta()
        'If _Cesta Is Nothing Then
        Dim oOrdenes As COrdenes = Me.FSNServer.Get_Object(GetType(COrdenes))

        If HttpContext.Current.Cache("_CestaBdd_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) Is Nothing Then
            oOrdenes.CargarCesta(Me.Usuario.CodPersona, Me.Usuario.Idioma, Me.Usuario.Cod)
            For Each orden As COrden In oOrdenes
                If orden.TipoPedido IsNot Nothing Then _
                    orden.TipoPedido = Me.TiposPedidos.ItemId(orden.TipoPedido.Id)
            Next
            Me.InsertarEnCache("_CestaBdd_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString, oOrdenes)
        Else
            oOrdenes = HttpContext.Current.Cache("_CestaBdd_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
        End If

        If ViewState("Cesta") Is Nothing Then
            ViewState("Cesta") = oOrdenes
        Else
            Dim oView As COrdenes = ViewState("Cesta")
            For Each oOrden As COrden In oOrdenes
                Dim oOrdenView As COrden = oView.Item(oOrden.Prove, oOrden.Moneda, oOrden.Categoria, oOrden.Nivel)
                If oOrdenView IsNot Nothing Then
                    If oOrdenView.Adjuntos IsNot Nothing Then _
                        oOrden.Adjuntos = oOrdenView.Adjuntos
                    For Each oLinea As CLinea In oOrden.Lineas
                        If oOrdenView.Lineas IsNot Nothing Then
                            Dim oLineaView As CLinea = oOrdenView.Lineas.ItemId(oLinea.ID)
                            If oLineaView IsNot Nothing Then
                                oLinea.Adjuntos = oLineaView.Adjuntos
                                oLinea.ObsAdjun = oLineaView.ObsAdjun
                            End If
                        End If
                    Next
                End If
            Next
            ViewState("Cesta") = oOrdenes
        End If

        _Cesta = oOrdenes

    End Sub
    ''' <summary>
    ''' Elimina de caché el contenido de la Cesta que corresponde a los datos presentes en bdd
    ''' </summary>
    ''' <remarks>Llamada desde EmisionPedido. Tiempo max inferior 0,1 seg</remarks>
    Private Shared Sub EliminarCestaBdd()
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        If HttpContext.Current.Cache("_CestaBdd_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString) IsNot Nothing Then
            HttpContext.Current.Cache.Remove("_CestaBdd_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString)
        End If
    End Sub
    Private ReadOnly Property DesdeFavorito() As Boolean
        Get
            Return Request.QueryString("DesdeFavorito") = "True"
        End Get
    End Property
    ''' <summary>
    ''' Indica si el favorito es nuevo, es decir, no guardado en la lista de favoritos
    ''' </summary>
    Private ReadOnly Property favNuevo() As Boolean
        Get
            Return Request.QueryString("favNuevo") = "True"
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que, si hemos llegado a la página con un favorito preseleccionado e informado por querystring con la variable favid
    ''' (actualmente el único caso es desde el webpart de pedidos favoritos)
    ''' nos devuelve su id, si no devuelve cero
    ''' </summary>
    Private ReadOnly Property FavId() As Integer
        Get
            If (Request.QueryString("favid") <> "") Then
                Return CType(Request.QueryString("favid"), Integer)
            Else
                Return 0
            End If
        End Get
    End Property
    ''' Revisado por: blp. Fecha: 31/08/2012
    ''' <summary>
    ''' Vamos a comprobar si hay variable de sesion de pedido favorito.
    ''' Si no la hay, la creamos con el id del favorito
    ''' Si la hay, comprobamos si es el mismo favorito, si no lo es, la reemplazamos
    ''' </summary>
    ''' <param name="idFavorito">Id del pedido favorito del que queremos comprobar si existe variable de sesión con sus datos 
    ''' y si no la hay o no corresponde a este favorito crearla o reemplazarla respectivamente
    ''' </param>
    ''' <remarks>Llamada desde el load de la página</remarks>
    Private Sub Crear_Actualizar_DataSet_Favorito(ByVal idFavorito As Integer)
        Dim DsetFavoritoEmitir As DataSet = TryCast(HttpContext.Current.Cache("DsetFavoritoEmitir_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString), DataSet)
        If DsetFavoritoEmitir IsNot Nothing AndAlso CType(DsetFavoritoEmitir, DataSet).Tables("ORDENESFAVORITOS") IsNot Nothing Then
            If CType(DsetFavoritoEmitir, DataSet).Tables("ORDENESFAVORITOS").Rows(0).Item("ID") <> idFavorito Then
                DsetFavoritoEmitir = Nothing
                HttpContext.Current.Cache.Remove("DsetFavoritoEmitir_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
            End If
        Else
            HttpContext.Current.Cache.Remove("DsetFavoritoEmitir_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
            DsetFavoritoEmitir = Nothing
        End If

        If DsetFavoritoEmitir Is Nothing Then
            Dim oOrdenesFavoritos As FSNServer.COrdenesFavoritos = FSNServer.Get_Object(GetType(FSNServer.COrdenesFavoritos))
            Dim dsFavoritoTemp As DataSet = oOrdenesFavoritos.BuscarTodasOrdenes(Me.Usuario.Cod, idFavorito, "", "", Idioma, True)
            DsetFavoritoEmitir = CrearDatasetFavoritoParaEmision(dsFavoritoTemp, idFavorito)
            Me.InsertarEnCache("DsetFavoritoEmitir_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString, DsetFavoritoEmitir)
        End If
    End Sub
    Private ReadOnly Property ArbolImputacion(ByVal IdEmpresa As Integer) As FSNServer.UONs
        Get
            If HttpContext.Current.Cache("ArbolImputacion_" & IdEmpresa & "_" & Me.Usuario.Cod & "_" & Me.Idioma.ToString()) Is Nothing Then
                Dim uons As FSNServer.UONs = FSNServer.Get_Object(GetType(FSNServer.UONs))
                uons.ArbolPartidasImputacion(Me.Usuario.Cod, Me.Idioma, String.Empty, Me.Usuario.PedidosOtrasEmp, IdEmpresa)
                Me.InsertarEnCache("ArbolImputacion_" & IdEmpresa & "_" & Me.Usuario.Cod & "_" & Me.Idioma.ToString(), uons)
                Return uons
            Else
                Return HttpContext.Current.Cache("ArbolImputacion_" & IdEmpresa & "_" & Me.Usuario.Cod & "_" & Me.Idioma.ToString())
            End If
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que actualmente sólo devuelve la primera partida presupuestaria existente, independientemente de cuántas haya creadas,
    ''' dado que EmisionPedido.aspx.vb sól oestá preparada para mostrar una partida presupuestaria.
    ''' </summary>
    Private _mipargenSMs As FSNServer.PargensSMs
    Private ReadOnly Property _PargenSM() As FSNServer.PargenSM
        Get
            If _mipargenSMs Is Nothing Then
                If Acceso.gbAccesoFSSM Then
                    _mipargenSMs = Me.PargenSM
                    If _mipargenSMs.Count > 0 Then
                        Return _mipargenSMs(0)
                    Else
                        Return Nothing
                    End If
                Else
                    Return Nothing
                End If
            Else
                Return _mipargenSMs(0)
            End If
        End Get
    End Property
    Private ReadOnly Property ProveedoresPedidoLibre() As DataTable
        Get
            If HttpContext.Current.Cache("CategoriasProveedoresPedidoLibre_" & Me.Usuario.Cod) Is Nothing Then
                Dim oProves As CProveedores = FSNServer.Get_Object(GetType(CProveedores))
                Dim ds As DataSet = oProves.DevolverProveedoresPedLibre(Me.Usuario.CodPersona, 0, 0, 0, 0, 0, 0)
                Me.InsertarEnCache("CategoriasProveedoresPedidoLibre_" & Me.Usuario.Cod, ds)
            End If
            Return CType(HttpContext.Current.Cache("CategoriasProveedoresPedidoLibre_" & Me.Usuario.Cod), DataSet) _
                .Tables(1)
        End Get
    End Property
#Region "Cargas con la página "
    ''' <summary>
    ''' Método que se ejecuta en el evento init de la página
    ''' </summary>
    ''' <param name="sender">la página</param>
    ''' <param name="e">argumentos del evento</param>
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'Añadimos en el init de la página un manejador de evento para el evento que recibimos desde el webpart de busqueda
        AddHandler CType(Me.Master.FSEPWebPartBusqArticulos1, EPWebControls.FSEPWebPartBusqArticulos).manejadorDeEvento, AddressOf btnBuscarArt_Click_DesdeWebPartBusquedaDeArticulos

        'Añadimos el jsLimiteDecimales.js al script compuesto que controla el scriptmanager para una carga más rápida de los scripts
        Dim scriptRef As New System.Web.UI.ScriptReference("~/js/jsLimiteDecimales.js")
        ScriptMgr.CompositeScript.Scripts.Add(scriptRef)

        scriptRef = New System.Web.UI.ScriptReference("~/js/jsTextboxFormat.js")
        ScriptMgr.CompositeScript.Scripts.Add(scriptRef)

        ScriptMgr.LoadScriptsBeforeUI = False

    End Sub
    ''' Revisado por: sra. Fecha: 23/01/2012
    ''' <summary>
    ''' Comprueba que el objeto que ha provocado el postback asíncrono es uno de los siguientes. Si es así, devuelve true para que salga de la función y no siga la ejecución
    ''' </summary>
    ''' <returns>True/False</returns>
    ''' <remarks>Llamada desde: Page_Load(); Tiempo máx: 0,01 sg</remarks>
    Function CheckAsyncPostBackElementToExit()
        If (ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$lblNomArt") > 0 _
                        OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$_imgbtnNext") > 0 _
                        OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$_imgbtnLast") > 0 _
                        OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$_imgbtnFirst") > 0 _
                        OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$_imgbtnPrev") > 0 _
                        OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$_ddlOrdenarPor") > 0) Then
            Return True
        Else
            Return False
        End If
    End Function
    ''' Revisado por: blp. Fecha: 14/11/2011
    ''' <summary>
    ''' Evento de carga de la página, cargando las variables necesarias entre una llamada y otra y registrando controles como sincronos o asincronos segun conveniencia
    ''' </summary>
    ''' <param name="sender">La propia página</param>
    ''' <param name="e">El evento de carga de la página</param>
    ''' <remarks>
    ''' Llamada desde:Cada vez que se hacen un postback de la pagina,sea sincrono o asincrono
    ''' Tiempo máximo:Aproximadamente 1,5 seg</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
        Call CargarTextos()
        Call RegistrarControles()
        Me.ScriptMgr.EnablePageMethods = True

        If ScriptMgr.IsInAsyncPostBack AndAlso CheckAsyncPostBackElementToExit() Then Exit Sub

        'Obtenemos la denominacion de la partida presupuestaria
        If Me.Acceso.gbAccesoFSSM And Not _PargenSM Is Nothing Then
            Dim PartPres As FSNServer.PartidasPRES5 = CType(FSNServer.Get_Object(GetType(FSNServer.PartidasPRES5)), FSNServer.PartidasPRES5)
            If Not PartPres Is Nothing Then _PartidaPresupuestariaDEN = PartPres.DenominacionPartidaPRES5(Usuario.Idioma, _PargenSM.Pres5, "", "", "", "", True)
            PartPres = Nothing
        End If

        If Not IsPostBack Then
            'Si no es postback, eliminamos la cestabdd
            EliminarCestaBdd()

            'Imagen y texto de la cesta
            Master.ImgCabIzq = "~/images/Cesta.gif"
            Master.TituloCabIzq = Textos(95)

            Master.CabTabCesta.Selected = True

            dlCesta.DataSource = Me.Cesta
            dlCesta.DataBind()
            For Each orden As COrden In Me._Cesta
                ActualizarImportesPedido(orden.Prove, orden.Moneda, orden.Categoria, orden.Nivel, False, orden.ID)
            Next

            'Compruebo que el usuario tenga Articulos para mostrar algo
            If Me._Cesta.Count = 0 Then
                BtnVaciarCesta.Visible = False
                btnVaciarCestaPie.Visible = False
                'LineaCabecera.Visible = False
                LblSinArticulosCesta.Visible = True
            End If

            If Not IsNothing(tblPie) Then
                If Me._Cesta.Count > 0 Then
                    If Me._Cesta.Count Mod 2 = 0 Then
                        tblPie.Attributes.Remove("class")
                    Else
                        tblPie.Attributes.Add("class", "ColorFondoTab")
                    End If
                Else
                    tblPie.Attributes.Add("class", "ColorFondoTab")
                End If
            End If
        End If

        If ScriptMgr.IsInAsyncPostBack Then
            For Each orden As COrden In Me.Cesta
                ActualizarImportesPedido(orden.Prove, orden.Moneda, orden.Categoria, orden.Nivel, False, orden.ID)
            Next
        End If

        If Not IsPostBack Then
            BtnAceptarEmisionOkOMal.OnClientClick = "$find('" & mpeMensajeEmiPedOkoMal.ClientID & "').hide(); return false"
            lnkCerrarMensajePedidoOkoMal.OnClientClick = "$find('" & mpeMensajeEmiPedOkoMal.ClientID & "').hide(); return false"
            btnCancelarConfirm.OnClientClick = "$find('" & mpeConfirm.ClientID & "').hide();"
            btnAceptarConfirm.OnClientClick = "$find('" & mpeConfirm.ClientID & "').hide();"
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "NumberFormatUsu") Then
            With FSNUser
                Dim sVariablesJavascript As String = "var UsuNumberDecimalSeparator = '" & .NumberFormat.NumberDecimalSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberThousanFmt='" & .ThousanFmt & "';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NumberFormatUsu", sVariablesJavascript, True)
            End With
        End If

        ScriptsCliente()
    End Sub
    ''' Revisado por: sra. Fecha: 23/01/2012
    ''' <summary>
    '''Procedimiento que registra controles utilizados en la página como sincronos o asincronos depende de las necesidades. Tambien registra eventos de cliente
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde:Page_Load
    ''' Tiempo máximo: 0 seg</remarks>
    Public Sub RegistrarControles()
        ScriptMgr.RegisterAsyncPostBackControl(btnAceptarUnidad)

    End Sub
    ''' Revisado por: blp. Fecha: 16/07/2012
    ''' <summary>
    ''' Escribe los scripts de cliente de la página
    ''' </summary>
    ''' <remarks>Llamada desde EmisionPedido.aspx.vb. Max. 0,1 seg.</remarks>
    Private Sub ScriptsCliente()
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "AsyncPostBacksMessage") Then
            Dim sScript As String = "function AsyncOk(msg){if (msg!='') alert(msg)}" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "AsyncPostBacksMessage", sScript, True)
        End If
        Dim sScriptErrorGestor As String = "var hayErrorGestor = 'False';" & vbCrLf
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkErrorGestor", sScriptErrorGestor, True)
        'If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "ocultarPanelError") Then
        '    Dim sScript As String = "function ocultarPanelError(){" & vbCrLf & _
        '            "var panelError=document.getElementById('" & pnlError.ClientID & "');" & vbCrLf & _
        '            "if (panelError && hayErrorGestor=='False'){" & vbCrLf & _
        '                "panelError.style.visibility='hidden';" & vbCrLf & _
        '                "panelError.innerText='';" & vbCrLf & _
        '            "}" & vbCrLf & _
        '        "}" & vbCrLf
        '    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "ocultarPanelError", sScript, True)
        'End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "BotonGuardarCesta") Then
            Dim sScript As String = "function ocultarguardarcesta(){" & _
                    "document.getElementById('" & upBotonesCabecera.FindControl("BtnGuardarCesta").ClientID & "').style.visibility='hidden';" & vbCrLf & _
                    "document.getElementById('" & upBotonesPie.FindControl("btnGuardarCestaPie").ClientID & "').style.visibility='hidden';" & vbCrLf & _
                    "document.getElementById('" & upBotonesCabecera.FindControl("hddGuardarCestaVisible").ClientID & "').value='false';" & vbCrLf & _
                "}" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "BotonGuardarCesta", sScript, True)
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "ScriptMsgBox") Then
            Dim sScript As String = "var MsgBoxSender;" & vbCrLf & _
                               "function MsgBox(Titulo, Texto, sender) {" & vbCrLf & _
                "   if (MsgBoxSender == sender) {" & vbCrLf & _
                "       return true;" & vbCrLf & _
                "   }" & vbCrLf & _
                "   else {" & vbCrLf & _
                "       $get('" & lblTitulo.ClientID & "').innerText = Titulo;" & vbCrLf & _
                "       $get('" & lblMensaje.ClientID & "').innerText = Texto;" & vbCrLf & _
                "       $find('" & mpeMsgBox.ClientID & "').show();" & vbCrLf & _
                "      MsgBoxSender = sender;" & vbCrLf & _
                "       return false;" & vbCrLf & _
                "   }" & vbCrLf & _
                "}" & vbCrLf & _
                "function cerrarMsgBox() {" & vbCrLf & _
                "   MsgBoxSender = null;" & vbCrLf & _
                "   $find('" & mpeMsgBox.ClientID & "').hide();" & vbCrLf & _
                "   return false;" & vbCrLf & _
                "}" & vbCrLf & _
                "function aceptarMsgBox() {" & vbCrLf & _
                "   if (MsgBoxSender != null){" & vbCrLf & _
                "       if (document.createEvent){" & vbCrLf & _
                "           //Create event" & vbCrLf & _
                "           var click_ev = document.createEvent(""MouseEvents"");" & vbCrLf & _
                "           //Initialize event" & vbCrLf & _
                "           click_ev.initEvent(""click"", true /* bubble */, true /* cancelable */);" & vbCrLf & _
                "           //Trigger event" & vbCrLf & _
                "           MsgBoxSender.dispatchEvent(click_ev);" & vbCrLf & _
                "       }" & vbCrLf & _
                "       else{" & vbCrLf & _
                "           MsgBoxSender.click();" & vbCrLf & _
                "       }" & vbCrLf & _
                "   }" & vbCrLf & _
                "   $find('" & mpeMsgBox.ClientID & "').hide();" & vbCrLf & _
                "   return false;" & vbCrLf & _
                "}" & vbCrLf
            'El script inmediatamente superior se inserta para dar funcionalidad al boton de Eliminar Articulo pero no funciona en Firefox. Pdte
            '"function aceptarMsgBox() {" & _
            '    "if (MsgBoxSender != null) clickLink(MsgBoxSender);" & _
            '    "$find('" & mpeMsgBox.ClientID & "').hide();" & _
            '    "return false;" & _
            '"}" & _
            '"function clickLink(linkobj) {" & _
            '   "if (linkobj.getAttribute('onclick') == null) {" & _
            '       "if (linkobj.getAttribute('href')) document.location = linkobj.getAttribute('href');" & _
            '   "}" & _
            '   "else linkobj.onclick();" & _
            '"}"

            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "ScriptMsgBox", sScript, True)
        End If
    End Sub
    ''' Revisado por: blp. Fecha: 17/07/2012
    ''' <summary>
    ''' Guarda el estado actual de la cesta en base de datos
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce cuando se hace clic en el control FSNButton BtnGuardarCesta o BtnGuardarCestaPie. Max 0,3 seg.</remarks>
    Private Sub BtnGuardarCesta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnGuardarCesta.Click, btnGuardarCestaPie.Click
        Dim GuardaryEditarPagina As Boolean = True
        'Si estamos guardando justo antes de emitir, no editamos la página, sólo guardamos
        If e IsNot Nothing AndAlso TryCast(e, CommandEventArgs) IsNot Nothing AndAlso TryCast(e, CommandEventArgs).CommandName = "GuardamosAntesDeEmitir" Then
            GuardaryEditarPagina = False
        End If

        'GUARDAMOS LAS MODIFICACIONES EN LA CESTA
        For Each oOrden As COrden In Me._Cesta
            'CABECERAS
            oOrden.ActualizarCesta(Me.Usuario.CodPersona)
            'LINEAS
            For Each oLinea As CLinea In oOrden.Lineas
                oLinea.ActualizarCestaLinea()
                'ADJUNTOS DE LA LÍNEA
                If oLinea.Adjuntos IsNot Nothing Then
                    For Each oAdj As Adjunto In oLinea.Adjuntos
                        Select Case oAdj.Operacion
                            Case "D"
                                oAdj.EliminarAdjunto(TiposDeDatos.Adjunto.Tipo.Cesta)
                            Case "I"
                                oAdj.IdEsp = oLinea.ID
                                oAdj.Cod = Me.Usuario.Cod
                                If oAdj.Id Is Nothing OrElse oAdj.Id = 0 AndAlso oAdj.tipo <> TiposDeDatos.Adjunto.Tipo.EspecificacionLineaCatalogo Then
                                    oAdj.GrabarAdjunto(oAdj.FileName, oAdj.DirectorioGuardar, TiposDeDatos.Adjunto.Tipo.Cesta, oAdj.IdEsp, oAdj.Comentario, oAdj.dataSize)
                                Else
                                    'Para copiar un adjunto de Especificación de línea de catálogo, creamos otro adjunto, dado que necesitaremos datos de ambos
                                    If oAdj.tipo = TiposDeDatos.Adjunto.Tipo.EspecificacionLineaCatalogo Then
                                        Dim oAdjCopia As Adjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
                                        oAdjCopia.IdEsp = oLinea.LineaCatalogo 'Este es el motivo de crear una copia, no se puede pasar oLinea.LineaCatalogo como el idEsp de la linea original porque necesitamos pasar tb el oLinea.ID
                                        oAdjCopia.Cod = oAdj.Cod
                                        oAdjCopia.dataSize = oAdj.dataSize
                                        oAdjCopia.Id = oAdj.Id
                                        oAdjCopia.tipo = oAdj.tipo
                                        oAdj.CopiarAdjunto(oAdjCopia, TiposDeDatos.Adjunto.Tipo.Cesta, oAdj.IdEsp, oAdjCopia.IdEsp)
                                    Else
                                        oAdj.CopiarAdjunto(oAdj, TiposDeDatos.Adjunto.Tipo.Cesta, oAdj.IdEsp)
                                    End If
                                End If
                                oAdj.Operacion = ""
                            Case "U"
                                oAdj.CambiarComentario(TiposDeDatos.Adjunto.Tipo.Cesta)
                        End Select
                    Next
                    oLinea.Adjuntos.RemoveAll(Function(el As FSNServer.Adjunto) el.Operacion = "D")
                End If
            Next

        Next
        EliminarCestaBdd()
        If GuardaryEditarPagina Then
            'OcultarBotonGuardarCesta()
            Master.CabControlCesta.Actualizar()
            upBotonesCabecera.Update()
            upBotonesPie.Update()
        End If
    End Sub
    ''' Revisado por: blp. Fecha: 16/07/2012
    ''' <summary>
    ''' El botón de guardar cesta se muestra por javascript siempre que se modifica algún dato en la página.
    ''' Desde junio de 2010, cuando se muestra el botón de guardar cesta, se oculta el panel de error.
    ''' Sin embargo, la tarea 2470 pide que se muestre el error de gestor (si corresponde) al modificar la partida de una línea.
    ''' Al modificar la línea, se muestra el botón de guardar, o sea, se oculta el panel, 
    ''' para evitarlo añadimos un script que controla si hay que mostrar errores de gestor.
    ''' Cuando los haya, no ocultaremos el panel.
    ''' En un mismo postback, sólo se puede registrar una vez un javascript a través del scriptmanager, dado que los consecutivos los ignora.
    ''' Es decir, si lanzo esta funcion checkErrorGestor dos veces en el mismo postback, no sobreescribe el javascript sino que sólo añade el primero,
    ''' por ello, vamos a variar el nombre de este script en función del parámetro pasado y a duplicar la asignación de valor en el script
    ''' </summary>
    ''' <param name="hayErrorGestor">Boolean</param>
    ''' <remarks>Llamadade desde EmisionPedido.aspx.vb. Max 0,1 seg.</remarks>
    Private Sub checkErrorGestor(ByVal hayErrorGestor As Boolean)
        Dim sScript As String = String.Empty
        If hayErrorGestor Then
            sScript = "hayErrorGestor = '" & hayErrorGestor.ToString & "';" & vbCrLf
        Else
            sScript = "hayErrorGestor = 'False';" & vbCrLf
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "checkErrorGestor" & hayErrorGestor.ToString, sScript, True)
    End Sub
    ''' Revisado por: sra. Fecha: 23/01/2012
    ''' <summary>
    ''' Carga los receptores de una lista de destinos
    ''' </summary>
    ''' <param name="sListaDestinos">contiene la cadena con los destinos</param>
    ''' <returns>Devuelve un objeto tipo CPersonas</returns>
    ''' <remarks>LLamada desde: BtnAcepDest_Click y dlCesta_ItemDataBound ; Tiempo máx: 0,1 sg</remarks>
    ''' 
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Shared Function Receptores(ByVal sListaDestinos As String) As String
        Dim sReceptores As String = String.Empty
        Dim oReceptores As CPersonas = DevolverReceptores(sListaDestinos)
        For Each oRecep As CPersona In oReceptores
            sReceptores &= "{""Valor"":""" & HttpUtility.JavaScriptStringEncode(oRecep.Cod)
            sReceptores &= """,""Texto"":""" & HttpUtility.JavaScriptStringEncode(oRecep.NomApe)
            sReceptores &= """},"
        Next
        If sReceptores.Length > 0 Then sReceptores = sReceptores.Substring(0, sReceptores.Length - 1)

        Return sReceptores
    End Function
    ''' Revisado por: sra. Fecha: 23/01/2012
    ''' <summary>
    ''' Carga los receptores de una lista de destinos
    ''' </summary>
    ''' <param name="sListaDestinos">contiene la cadena con los destinos</param>
    ''' <returns>Devuelve un objeto tipo CPersonas</returns>
    ''' <remarks>LLamada desde: BtnAcepDest_Click y dlCesta_ItemDataBound ; Tiempo mÃ¡x: 0,1 sg</remarks>
    Private Shared Function DevolverReceptores(ByVal sListaDestinos As String) As CPersonas
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oReceptores As CPersonas = FSNServer.Get_Object(GetType(FSNServer.CPersonas))
        Dim oUsuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        oReceptores.CargarReceptores(sListaDestinos)
        Return oReceptores
    End Function
#End Region
#Region "Operaciones DataList"
    ''' Revisado por: blp. Fecha: 01/12/2011
    ''' <summary>
    ''' Procedimiento que se encarga de Cargar los datos necesarios de cada ItemList del DataList, así como también carga los DataSource de cada GridLineas
    ''' </summary>
    ''' <param name="sender">El propio DataList</param>
    ''' <param name="e">el evento ItemDataBound</param>
    ''' <remarks>
    ''' Llamada desde:La propia página, cuando se hace un postback
    ''' Tiempo máximo:Depende de los datos, pero 1,5 seg aprox</remarks>
    Protected Sub dlCesta_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlCesta.ItemDataBound
        With e.Item
            Dim oOrden As COrden = CType(.DataItem, COrden)
            .Attributes.Add("PROVEEDOR", oOrden.Prove)
            .Attributes.Add("MONEDA", oOrden.Moneda)
            .Attributes.Add("CAT", oOrden.Categoria)
            .Attributes.Add("NIVEL", oOrden.Nivel)
            .Attributes.Add("CESTACABECERAID", oOrden.ID)

            CType(.FindControl("lblSubTotalPie"), Label).Text = Textos(105)
            'CType(.FindControl("lblArticulosPie"), Label).Text = Textos(200)
            CType(.FindControl("LblProve"), Label).Text = Textos(201)
            CType(.FindControl("BtnEmitirLinea"), FSNWebControls.FSNButton).Text = Textos(171)
            CType(.FindControl("BtnEmitirLinea"), FSNWebControls.FSNButton).ToolTip = Textos(171)
            CType(.FindControl("lblCantCab"), Label).Text = Textos(11)
            CType(.FindControl("lblPUCab"), Label).Text = Textos(198)
            CType(.FindControl("lblImpCab"), Label).Text = Textos(1)
            CType(.FindControl("lblEmiCab"), Label).Text = Textos(9)

            CType(.FindControl("FSNLinkProve"), FSNWebControls.FSNLinkInfo).PanelInfo = FSNPanelDatosProveCab.ID

            CType(.FindControl("lblPedidoDirecto"), Label).Text = "(" & Textos(293) & ": " & oOrden.Anyo_PedidoAbierto & "/" & oOrden.NumPedido_PedidoAbierto & "/" & oOrden.NumOrden_PedidoAbierto & ")"
            If Not oOrden.IdOrdenPedidoAbierto = 0 Then CType(.FindControl("lblPedidoDirecto"), Label).Visible = True
            Dim scad As String = String.Empty
            If FSNUser.MostrarCodProve AndAlso Not String.IsNullOrEmpty(oOrden.Prove) Then _
                scad = oOrden.Prove & " - "
            scad = scad & oOrden.ProveDen
            CType(.FindControl("FSNLinkProve"), FSNWebControls.FSNLinkInfo).Text = scad
            If Not IsNothing(oOrden.DenCategoria) Then
                CType(.FindControl("lblCategoria"), Label).Text = "(" + Textos(147) + " " + oOrden.DenCategoria + ")"
            End If

            'Para la carga de receptores hay que tener en cuenta que estos pertenezcan a las unidades organizativas de los destinos
            Dim listaDestinos As List(Of String) = ObtenerDestinosOrden(oOrden)

            Dim receptores As CPersonas = DevolverReceptores(String.Join(",", listaDestinos.Distinct().ToArray()))

            Dim sRecep As String
            If Me.DesdeFavorito Then
                sRecep = oOrden.Receptor
            Else
                If String.IsNullOrEmpty(oOrden.Receptor) Then
                    sRecep = Me.Usuario.CodPersona
                Else
                    sRecep = oOrden.Receptor
                End If
            End If

            CType(.FindControl("GridLineas"), GridView).DataSource = oOrden.Lineas

            _CestaTemporal = Me._Cesta
            CType(.FindControl("GridLineas"), GridView).DataBind()
            _CestaTemporal = Nothing
        End With
    End Sub
    ''' <summary>Obtiene los destinos de una orden</summary>
    ''' <param name="oOrden">Orden</param>
    ''' <returns>Lista de destinos</returns>    
    ''' <remarks>Llamada desde: dlCesta.ItemBound</remarks>
    ''' <revision>LTG 07/06/2013</revision>
    ''' 
    Private Function ObtenerDestinosOrden(ByVal oOrden As COrden) As List(Of String)
        Dim listaDestinos As New List(Of String)
        For Each linea As CLinea In oOrden.Lineas
            If Not String.IsNullOrEmpty(linea.Dest) Then
                listaDestinos.Add(linea.Dest)
            ElseIf Not String.IsNullOrEmpty(CType(Me.Usuario, FSNServer.User).Destino) Then
                listaDestinos.Add(CType(Me.Usuario, FSNServer.User).Destino)
            ElseIf Destinos.Count > 0 Then
                listaDestinos.Add(Destinos(0).Cod)
            End If
        Next
        Return listaDestinos
    End Function
    ''' Revisado por: blp. Fecha: 01/12/2011
    ''' <summary>
    ''' Procedimiento que se lanza cada vez que un control contenido en el DataList ejecuta un comando, normalmente hacer click
    ''' </summary>
    ''' <param name="source">El propio DataList</param>
    ''' <param name="e">El objeto que ha realizado el comando</param>
    ''' <remarks>
    ''' Llamada desde: Cualquier elemento del DataList al ser pulsado
    ''' Tiempo máximo: Depende cuál sea el elemento, pero entre 0 seg, y 1 seg</remarks>
    Protected Sub dlCesta_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlCesta.ItemCommand
        Select Case e.CommandName
            Case "EmitirLinea"
                Dim parametro() As String = Split(e.CommandArgument, "@")

                HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaEP") & "EmisionPedido.aspx?ID=" & parametro(0) & IIf(parametro(1) = 0, "", "&abierto=" & parametro(1)))

        End Select
    End Sub
    ''' Revisado por: sra. Fecha: 23/01/2012
    ''' <summary>
    ''' Localiza en el DataList dlCesta un pedido determindado
    ''' </summary>
    ''' <param name="Proveedor">Código de proveedor</param>
    ''' <param name="Moneda">Código de moneda</param>
    ''' <returns>El DataListItem correspondiente</returns>
    Private Function dlCesta_FindItem(ByVal Proveedor As String, ByVal Moneda As String, ByVal Cat As Integer, ByVal Nivel As Integer, ByVal idOrden As Integer) As DataListItem
        Dim orden As DataListItem = Nothing
        Try
            orden = From Pedidos In dlCesta.Items _
                                Where CType(Pedidos, DataListItem).Attributes("PROVEEDOR") = Proveedor _
                                    And CType(Pedidos, DataListItem).Attributes("MONEDA") = Moneda _
                                    And CType(Pedidos, DataListItem).Attributes("CAT") = Cat _
                                    And CType(Pedidos, DataListItem).Attributes("NIVEL") = Nivel _
                                    And CType(Pedidos, DataListItem).Attributes("CESTACABECERAID") = idOrden _
                                    Select Pedidos Distinct.First()
        Catch ex As InvalidOperationException
            ' Si no encuentra ninguna devuelve Nothing
        End Try
        Return orden
        '
    End Function
    ''' <summary>
    ''' Localiza en el DataList dlCesta un pedido determindado
    ''' </summary>
    ''' <returns>El DataListItem correspondiente</returns>
    Private Function dlCesta_FindItem_CC(ByVal CestaCabeceraID As Integer) As DataListItem
        Dim orden As DataListItem = Nothing
        Try
            orden = From Pedidos In dlCesta.Items _
                                Where CType(Pedidos, DataListItem).Attributes("CESTACABECERAID") = CestaCabeceraID _
                                    Select Pedidos Distinct.First()
        Catch ex As InvalidOperationException
            ' Si no encuentra ninguna devuelve Nothing
        End Try
        Return orden
    End Function
#End Region
#Region "Operaciones Grid "
    ''' Revisado por: blp. Fecha: 01/12/2011
    ''' <summary>
    ''' Procedimiento que se realiza cada vez que se carga cada GridLineas, para cargar los datos necesarios de cada item dependiendo de los parámetros de usuario, permisos,etc..
    ''' </summary>
    ''' <param name="sender">El propio GridLineas a tratar</param>
    ''' <param name="e">El propio evento RowDataBound</param>
    ''' <remarks>
    ''' Llamada desde:La propia página, cada vez que se hace PostBack
    ''' Tiempo máximo:0,4 seg</remarks>
    Protected Sub GridLineas_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        'El todo el rowDataBound, vamos a usar la vble _CestaTemporal, a la que hemos pasado el valor de Me.CEsta,
        'para evitar el coste de rendimiento que implica CESTA
        If e.Row.RowType = DataControlRowType.DataRow Then
            With e.Row
                Dim dlOrden As DataListItem = CType(sender, Control).NamingContainer
                CargarTextosGridLineas(e.Row)
                Dim oLinea As CLinea = CType(.DataItem, CLinea)
                Dim oOrden As COrden = _CestaTemporal.Find( _
                    Function(elorden As COrden) elorden.Lineas.ItemId(oLinea.ID) IsNot Nothing)
                .Attributes.Add("LINEA", oLinea.ID)

                If oLinea.LineaCatalogo > 0 Then
                    CType(.FindControl("LblNomArt"), LinkButton).ToolTip = Textos(102)
                    CType(.FindControl("LblArticulo"), LinkButton).ToolTip = Textos(102)
                Else
                    CType(.FindControl("LblNomArt"), LinkButton).Enabled = False
                    CType(.FindControl("LblArticulo"), LinkButton).Enabled = False
                End If

                CType(.FindControl("LblArticulo"), LinkButton).Attributes.Add("IdCesta", oLinea.ID)
                CType(.FindControl("LblArticulo"), LinkButton).Attributes.Add("EsPedidoAbierto", oLinea.EsLineaPedidoAbierto.ToString.ToLower)

                If oLinea.EsLineaPedidoAbierto Then
                    CType(.FindControl("ImgBtnArt"), ImageButton).Enabled = False
                    CType(.FindControl("LblNomArt"), LinkButton).Enabled = False
                    CType(.FindControl("LblArticulo"), LinkButton).Enabled = False
                    CType(.FindControl("lnkUnidad"), FSNWebControls.FSNLinkTooltip).Enabled = False
                End If

                CType(.FindControl("valCantidadNoCero"), CompareValidator).ErrorMessage = Textos(161)

                Dim wNumCant As FSNWebControls.FSNTextBox = CType(.FindControl("wnumCantidad"), FSNWebControls.FSNTextBox)
                Dim wNumCant_DblValue As HiddenField = CType(.FindControl("wnumCantidad_DblValue"), HiddenField)

                Dim lnkUnidad As FSNWebControls.FSNLinkTooltip = .FindControl("lnkUnidad")
                Dim wNumPrec As FSNWebControls.FSNTextBox = CType(.FindControl("WNumEdPrecArt"), FSNWebControls.FSNTextBox)
                Dim wNumPrecArrows As FSNWebControls.FSNUpDownArrows = CType(.FindControl("WNumEdPrecArtArrows"), FSNWebControls.FSNUpDownArrows)
                Dim wNumPrec_DblValue As HiddenField = CType(.FindControl("WNumEdPrecArt_DblValue"), HiddenField)
                Dim wnumCantidadArrows As FSNWebControls.FSNUpDownArrows = CType(.FindControl("wnumCantidadArrows"), FSNWebControls.FSNUpDownArrows)

                Dim WNumEdTotLin As FSNWebControls.FSNTextBox = CType(.FindControl("WNumEdTotLin"), FSNWebControls.FSNTextBox)
                Dim WNumEdTotLinArrows As FSNWebControls.FSNUpDownArrows = CType(.FindControl("WNumEdTotLinArrows"), FSNWebControls.FSNUpDownArrows)

                Dim lblCantidadNoAplica As Label = CType(.FindControl("lblCantidadNoAplica"), Label)
                Dim lblPrecioNoAplica As Label = CType(.FindControl("lblPrecioNoAplica"), Label)
                Dim lblUnidad As Label = CType(.FindControl("lblUnidad"), Label)

                If oLinea.TipoRecepcion = 0 Or (oLinea.EsLineaPedidoAbierto AndAlso oLinea.TipoRecepcion = 3) Then
                    Dim minimo As Nullable(Of Double) = IIf(IsDBNull(oLinea.CantMinimaPedido), 0, CDbl(oLinea.CantMinimaPedido))
                    wNumCant_DblValue.Value = IIf(oLinea.CantPed < IIf(IsDBNull(oLinea.CantMinimaPedido), 0, oLinea.CantMinimaPedido), oLinea.CantMinimaPedido, oLinea.CantPed)

                    Dim maximo As Nullable(Of Double) = IIf(IsDBNull(oLinea.CantMaximaPedido), 0, CDbl(oLinea.CantMaximaPedido))
                    maximo = IIf(maximo = 0, Nothing, maximo)
                    configurarDecimales(oLinea.UP, wNumCant, minimo, maximo)
                    wNumCant.Attributes.Add("IDCesta", oLinea.ID)

                    If lnkUnidad.Attributes("fc") Is Nothing Then
                        lnkUnidad.Attributes.Add("fc", oLinea.FC)
                    End If

                    If oLinea.LineaCatalogo > 0 Then
                        lnkUnidad.ContextKey = oLinea.UP & "_@_" & oLinea.LineaCatalogo
                        lnkUnidad.Attributes.Add("CantMinima", oLinea.CantMinimaPedido.ToString())
                        .FindControl("lnkbtnCambiarUnidad").Visible = oLinea.ModificarUnidad
                    Else
                        .FindControl("lnkbtnCambiarUnidad").Visible = False
                    End If

                    wNumPrec_DblValue.Value = CDbl(oLinea.PrecUc * oLinea.FC)

                    configurarDecimales(Nothing, wNumPrec, Nothing, Nothing)

                    If Me.FSNUser.ModificarPrecios AndAlso CType(oLinea.ModificarPrecio, Boolean) Then
                        wNumPrec.Visible = True
                        wNumPrecArrows.Visible = True
                    End If
                    wNumPrec.Attributes.Add("IDCesta", oLinea.ID)

                    CType(.FindControl("LblPrecArt"), Label).Text = wNumPrec.Text
                    .FindControl("LblPrecArt").Visible = Not wNumPrec.Visible
                    CType(.FindControl("LblMonedaArtSrv"), Label).Text = oOrden.Moneda & " / "

                    CType(.FindControl("LblTotLin"), Label).Text = FSNLibrary.FormatNumber(wNumPrec_DblValue.Value * wNumCant_DblValue.Value, Me.Usuario.NumberFormat) & " " & oOrden.Moneda
                    CType(.FindControl("LblTotLin_DblValue"), HiddenField).Value = wNumPrec_DblValue.Value * wNumCant_DblValue.Value

                    WNumEdTotLin.Visible = False
                    WNumEdTotLinArrows.Visible = False
                    CType(.FindControl("LblMonedaArtSrvTotal"), Label).Visible = False

                    Dim oUnidadPedido As FSNServer.CUnidadPedido = Nothing

                    oUnidadPedido = TodasLasUnidades.Item(Trim("UND"))

                    lblUnidad.Visible = True
                    If Not IsNothing(oUnidadPedido) Then
                        lblUnidad.Text = oUnidadPedido.Codigo
                    Else
                        lblUnidad.Text = "UND"
                    End If
                Else
                    wNumCant_DblValue.Value = 1 'IIf(oLinea.CantPed < IIf(IsDBNull(oLinea.CantMinimaPedido), 0, oLinea.CantMinimaPedido), oLinea.CantMinimaPedido, oLinea.CantPed)

                    wNumCant.Attributes.Add("IDCesta", oLinea.ID)
                    wNumCant.Visible = False

                    .FindControl("lnkbtnCambiarUnidad").Visible = False
                    lnkUnidad.Visible = False
                    If lnkUnidad.Attributes("fc") Is Nothing Then
                        lnkUnidad.Attributes.Add("fc", "1")
                    End If
                    wNumPrec_DblValue.Value = 1 'CDbl(oLinea.ImportePedido)
                    wNumPrec.Visible = False
                    wNumPrecArrows.Visible = False
                    wNumPrec.Attributes.Add("IDCesta", oLinea.ID)
                    wnumCantidadArrows.Visible = False

                    lblCantidadNoAplica.Visible = True
                    lblCantidadNoAplica.Text = "(" + Textos(272) + ")"
                    lblPrecioNoAplica.Visible = True
                    lblPrecioNoAplica.Text = "(" + Textos(272) + ")"

                    CType(.FindControl("LblPrecArt"), Label).Text = 1
                    .FindControl("LblPrecArt").Visible = False

                    CType(.FindControl("LblMonedaArtSrv"), Label).Visible = False

                    CType(.FindControl("LblTotLin"), Label).Visible = False

                    CType(.FindControl("LblTotLin_DblValue"), HiddenField).Value = CDbl(oLinea.ImportePedido)
                    CType(.FindControl("LblTotLinAnt_DblValue"), HiddenField).Value = CDbl(oLinea.ImportePedido)

                    Dim minimo As Nullable(Of Double) = IIf(IsDBNull(oLinea.CantMinimaPedido), 0, oLinea.CantMinimaPedido)
                    Dim maximo As Nullable(Of Double) = IIf(IsDBNull(oLinea.CantMaximaPedido), 0, oLinea.CantMaximaPedido)
                    maximo = IIf(maximo = 0, Nothing, maximo)
                    configurarDecimales(Nothing, WNumEdTotLin, minimo, maximo)
                    WNumEdTotLin.Visible = True
                    WNumEdTotLinArrows.Visible = True
                    CType(.FindControl("LblMonedaArtSrvTotal"), Label).Visible = True
                    CType(.FindControl("LblMonedaArtSrvTotal"), Label).Text = oOrden.Moneda
                End If
                CType(.FindControl("BtnEliminarLinea"), Image).ImageUrl = "~/images/rechaz_disable_Cesta.gif"

                If oLinea.LineaCatalogo = 0 OrElse oLinea.TipoArticulo.TipoAlmacenamiento <> CArticulo.EnAlmacenTipo.NoAlmacenable Then
                    If String.IsNullOrEmpty(oLinea.Dest) Then
                        If Not String.IsNullOrEmpty(CType(Me.Usuario, FSNServer.User).Destino) Then
                            Dim sDestino As String = CType(Me.Usuario, FSNServer.User).Destino
                            oLinea.Dest = sDestino
                            If Not String.IsNullOrEmpty(sDestino) Then
                                oLinea.DestDen = Me.Destinos.Item(sDestino).Den
                            End If
                        ElseIf Destinos.Count > 0 Then
                            oLinea.Dest = Destinos(0).Cod
                            oLinea.DestDen = Destinos(0).Den
                        End If
                    End If
                End If

                If Not String.IsNullOrEmpty(oLinea.Dest) Then
                    .Attributes.Add("DESTINO", oLinea.Dest)
                    .Attributes.Add("DESTINO_DEN", oLinea.DestDen)
                End If

                Dim btn As ImageButton = .FindControl("BtnEliminarLinea")
                'btn. = Textos(103)
                btn.ToolTip = Textos(103)
                btn.OnClientClick = "return MsgBox('" & JSText(Textos(103)) & "', '" & JSText(Textos(160)) & "', this)"

                CType(.FindControl("cusvalTipoArticulo"), CustomValidator).ErrorMessage = Textos(157)
            End With
        End If
    End Sub
    ''' Revisado por: blp. Fecha: 01/12/2011
    ''' <summary>
    ''' Procedimiento perteneciente al GridLineas que capta los comandos de los elementos de sus filas, y realiza diversas acciones dependiendo de cual sea el emisor.
    ''' </summary>
    ''' <param name="sender">El propio GridView</param>
    ''' <param name="e">El elemento que ha lanzado un comando</param>
    ''' <remarks>
    ''' Llamada desde:Cualquiera de los GridView de la pagina, cada vez que un control ejecute un comando(normalmente pincharlo)
    ''' Tiempo maximo: Variable ya que se ejecutaran funciones muy distintas dependiendo de quien mande el comando.
    ''' Varía entre 0 seg, y 1,5 seg
    ''' </remarks>
    Protected Sub GridLineas_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Select Case e.CommandName
            Case "AbrirDetalleArt"
                Dim oOrden As COrden = Me._Cesta.Find(Function(elemento As COrden) elemento.Lineas.ItemId(e.CommandArgument) IsNot Nothing)
                Dim oLinea As CLinea = oOrden.Lineas.ItemId(e.CommandArgument)
                Dim dlorden As DataListItem = dlCesta_FindItem_CC(oOrden.ID) '(oOrden.Prove, oOrden.Moneda, oOrden.Categoria, oOrden.Nivel)
                Dim grlinea As GridViewRow = GridLineas_FindItem(dlorden.FindControl("GridLineas"), oLinea.ID)
                Dim cantidad As Double = CType(grlinea.FindControl("wnumCantidad_DblValue"), HiddenField).Value


                Dim lnkUnidad As FSNWebControls.FSNLinkTooltip = grlinea.FindControl("lnkUnidad")
                Dim fc As Double = CDbl(lnkUnidad.Attributes("fc"))
                Dim cantMinima As Double = CDbl(lnkUnidad.Attributes("CantMinima"))
                Dim cantMaxima As Double = CDbl(lnkUnidad.Attributes("CantMaxima"))
                Dim unidad As String = lnkUnidad.Text
                'Añado la nueva denominacion porque si no me muestra la denominacion antigua
                Dim lblNomArt As Global.System.Web.UI.WebControls.LinkButton = grlinea.FindControl("LblNomArt")
                Dim LblArticulo As Global.System.Web.UI.WebControls.LinkButton = grlinea.FindControl("LblArticulo")


                Dim LblTotLin As Double = CType(grlinea.FindControl("LblTotLin_DblValue"), HiddenField).Value

                Dim sNuevadenominacion As String = DBNullToStr(lblNomArt.Text) + " - " + DBNullToStr(LblArticulo.Text)
                If oLinea.TipoRecepcion = 0 Then
                    pnlDetalleArticulo.CargarDetalleArticulo(DsetArticulos, oLinea.LineaCatalogo, cantidad, oLinea.PrecUc * fc, unidad, cantMinima, True, e.CommandArgument, sNuevadenominacion, cantMaxima, oLinea.TipoRecepcion)
                Else
                    pnlDetalleArticulo.CargarDetalleArticulo(DsetArticulos, oLinea.LineaCatalogo, LblTotLin, oLinea.PrecUc * fc, "", oLinea.CantMinimaPedido, True, e.CommandArgument, sNuevadenominacion, oLinea.CantMaximaPedido, oLinea.TipoRecepcion)
                End If
            Case "EliminarLinea"
                Dim oOrden As COrden = Me._Cesta.Find(Function(elemento As COrden) elemento.Lineas.ItemId(e.CommandArgument) IsNot Nothing)
                Dim dlorden As DataListItem = dlCesta_FindItem(oOrden.Prove, oOrden.Moneda, oOrden.Categoria, oOrden.Nivel, oOrden.ID)
                Dim ordenesEnCesta As Nullable(Of Integer) = Nothing
                'ordenesEnCesta se crea para asegurarnos de que, cuando el pedido no es favorito y se ha eliminado el último artículo de una orden,
                'se actualice la pantalla con los cambios, dado que actualmente el updatepanel de la orden (upOrden) cuya línea ha sido eliminada, se actualiza en la función configurarLineas, de modo que 
                'cuando se elimina la última linea, no se actualiza porque ya no quedan lineas (ni orden) que actualizar
                If DesdeFavorito Then
                    If Not favNuevo Then
                        Recuperar_Cesta()
                        Dim idFavorito As Integer = _Cesta(0).ID
                        Dim IdLinea As String = e.CommandArgument
                        LineasAEliminarDeFavorito.Add(IdLinea)
                        EliminarCestaBdd()

                        Dim DsetFavoritoEmitir As DataSet = TryCast(HttpContext.Current.Cache("DsetFavoritoEmitir_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString), DataSet)
                        If Not DsetFavoritoEmitir Is Nothing Then
                            With CType(DsetFavoritoEmitir, DataSet).Tables("LINEASPEDIDO").Rows
                                Dim dr As DataRow = .Find(e.CommandArgument)
                                If dr IsNot Nothing Then .Remove(dr)
                            End With
                        End If
                    End If
                    upBotonesCabecera.Update()
                    upBotonesPie.Update()
                Else
                    ordenesEnCesta = _Cesta.Count

                    _Cesta.EliminarDeCesta(e.CommandArgument, oOrden.ID, Usuario.CodPersona)
                    EliminarCestaBdd()
                    Recuperar_Cesta()
                End If
                If Me._Cesta.Count = 0 OrElse DesdeFavorito And _Cesta(0).Lineas.Count = 0 Then
                    Response.Redirect("CatalogoWeb.aspx")
                    Response.End()
                Else
                    If ordenesEnCesta IsNot Nothing AndAlso ordenesEnCesta > _Cesta.Count Then
                        upCollapse.Update()
                    End If
                    dlCesta.DataSource = Cesta
                    dlCesta.DataBind()
                    Master.CabControlCesta.Actualizar()
                    For Each orden As COrden In Cesta
                        ActualizarImportesPedido(orden.Prove, orden.Moneda, orden.Categoria, orden.Nivel, False, orden.ID)
                    Next
                End If
            Case "ModificarUnidad"
                Dim oUnis As CUnidadesPedido = Me.FSNServer.Get_Object(GetType(FSNServer.CUnidadesPedido))
                Dim oOrden As COrden = Me._Cesta.Find( _
                    Function(elorden As COrden) elorden.Lineas.ItemId(e.CommandArgument) IsNot Nothing)
                Dim oLinea As CLinea = oOrden.Lineas.ItemId(e.CommandArgument)
                If oLinea.LineaCatalogo = 0 Then
                    oUnis.CargarTodasLasUnidades(Me.Idioma)
                Else
                    oUnis.DevolverUnidadesPedido(Me.Idioma, oLinea.LineaCatalogo)
                End If
                cmbUnidades.DataSource = oUnis
                cmbUnidades.DataBind()
                Dim Unidad As CUnidadPedido = oUnis.Item(oLinea.UP)
                If Unidad IsNot Nothing Then
                    cmbUnidades.SelectedValue = Unidad.Codigo
                    lblCodUnidadSrv.Text = Unidad.Codigo & " - " & Unidad.Denominacion
                    If oLinea.LineaCatalogo = 0 Then
                        lblUnidadCompraSrv.Text = String.Empty
                        lblFactorConversionSrv.Text = String.Empty
                    Else
                        If Not String.IsNullOrEmpty(Unidad.UnidadCompra) Then
                            Dim sFactorConversion As String
                            If Unidad.FactorConversion = CInt(Unidad.FactorConversion) Then
                                sFactorConversion = CInt(Unidad.FactorConversion).ToString()
                            Else
                                sFactorConversion = FSNLibrary.FormatNumber(Unidad.FactorConversion, Me.Usuario.NumberFormat)
                            End If
                            lblUnidadCompraSrv.Text = Unidad.UnidadCompra & " - " & Unidad.DenUnidadCompra
                            lblFactorConversionSrv.Text = Unidad.Codigo & " = " & sFactorConversion & " " & Unidad.UnidadCompra
                        End If
                    End If

                    Unidad.AsignarFormatoaUnidadPedido(Me.Usuario.NumberFormat)
                    Dim sCantidad As String = Unidad.CantidadMinima.ToString
                    If Not Unidad.UnitFormat Is Nothing Then
                        If Unidad.NumeroDeDecimales Is Nothing Then
                            Dim decimales As Integer = NumeroDeDecimales(DBNullToStr(Unidad.CantidadMinima))
                            Unidad.UnitFormat.NumberDecimalDigits = decimales
                        End If
                        sCantidad = FSNLibrary.modUtilidades.FormatNumber(Unidad.CantidadMinima, Unidad.UnitFormat)
                    End If
                    lblCantMinimaSrv.Text = sCantidad

                    btnAceptarUnidad.CommandArgument = e.CommandArgument
                    updModifUnidad.Update()
                    mpepnlUnidad.Show()
                End If
        End Select
    End Sub
    ''' Revisado por: sra. Fecha: 23/01/2012
    ''' <summary>
    ''' Evento que se lanza al pulsar el botón aceptar del detalle de artículo
    ''' </summary>
    ''' <param name="IdArt">Id de la línea de catálogo</param>
    ''' <param name="Cantidad">Cantidad devuelta por el panel de detalle de artículo</param>
    ''' <param name="Precio">Precio devuelto por el panel de detalle de artículo</param>
    Private Sub pnlDetalleArticulo_AnyadirACesta(ByVal IdArt As Integer, ByVal Cantidad As Double, ByVal Precio As Double, ByVal Key As String) Handles pnlDetalleArticulo.AnyadirACesta
        Dim oOrden As COrden = Me._Cesta.Find( _
            Function(elorden As COrden) elorden.Lineas.ItemId(Key) IsNot Nothing)
        Dim orden As DataListItem = dlCesta_FindItem(oOrden.Prove, oOrden.Moneda, oOrden.Categoria, oOrden.Nivel, oOrden.ID)
        Dim filagrid As GridViewRow = GridLineas_FindItem(orden.FindControl("GridLineas"), Key)
        Dim txtcant_DblValue As HiddenField = filagrid.FindControl("wnumCantidad_DblValue")
        Dim txtprec_DblValue As HiddenField = filagrid.FindControl("WNumEdPrecArt_DblValue")
        Dim CantMinima As Double = CDbl(CType(filagrid.FindControl("lnkUnidad"), WebControl).Attributes("CantMinima"))
        Dim bActualizar As Boolean = False

        If txtcant_DblValue.Value <> Cantidad Then
            If DBNullToDbl(CantMinima) < Cantidad Then
                txtcant_DblValue.Value = Cantidad
            ElseIf DBNullToDbl(CantMinima) = 0 Then
                txtcant_DblValue.Value = 1
            Else
                txtcant_DblValue.Value = CantMinima
            End If
            bActualizar = True
        End If

        If txtprec_DblValue.Value <> Precio And Precio > 0 Then
            txtprec_DblValue.Value = Precio
            bActualizar = True
        End If

        If bActualizar Then
            MostrarBotonGuardarCesta()
            ActualizarImportesPedido(oOrden.Prove, oOrden.Moneda, oOrden.Categoria, oOrden.Nivel, True, orden.ID)
        End If
    End Sub
    ''' Revisado por: sra. Fecha: 23/01/2012
    ''' <summary>
    ''' Evento que se lanza al pulsar el botón aceptar del detalle de artículo
    ''' </summary>
    ''' <param name="IdArt">Id de la línea de catálogo</param>
    ''' <param name="Cantidad">Cantidad devuelta por el panel de detalle de artículo</param>
    ''' <param name="Precio">Precio devuelto por el panel de detalle de artículo</param>
    Private Sub pnlDetalleArticulo_AnyadirACestaEP(ByVal IdArt As Integer, ByVal Cantidad As Double, ByVal Precio As Double, ByVal Key As String) Handles pnlDetalleArticulo.AnyadirACestaEP
        Dim oOrden As COrden = Me._Cesta.Find( _
            Function(elorden As COrden) elorden.Lineas.ItemId(Key) IsNot Nothing)
        Dim orden As DataListItem = dlCesta_FindItem(oOrden.Prove, oOrden.Moneda, oOrden.Categoria, oOrden.Nivel, oOrden.ID)
        Dim filagrid As GridViewRow = GridLineas_FindItem(orden.FindControl("GridLineas"), Key)
        Dim txtprec_DblValue As HiddenField = filagrid.FindControl("WNumEdPrecArt_DblValue")

        Dim bActualizar As Boolean = False

        Dim ctlTotalLinea As FSNWebControls.FSNTextBox = filagrid.FindControl("WNumEdTotLin")
        Dim LblTotLin_DblValue As HiddenField = filagrid.FindControl("LblTotLin_DblValue")
        Dim LblTotLinAnt_DblValue As HiddenField = filagrid.FindControl("LblTotLinAnt_DblValue")
        If ctlTotalLinea.Visible = False Then
            Dim txtcant_DblValue As HiddenField = filagrid.FindControl("wnumCantidad_DblValue")

            Dim CantMinima As Double = CDbl(CType(filagrid.FindControl("lnkUnidad"), WebControl).Attributes("CantMinima"))
            Dim CantMaxima As Double = CDbl(CType(filagrid.FindControl("lnkUnidad"), WebControl).Attributes("CantMaxima"))
            If txtcant_DblValue.Value <> Cantidad Then
                If DBNullToDbl(CantMinima) < Cantidad Then
                    txtcant_DblValue.Value = Cantidad
                ElseIf DBNullToDbl(CantMinima) = 0 Then
                    txtcant_DblValue.Value = 1
                Else
                    txtcant_DblValue.Value = CantMinima
                End If
                If DBNullToDbl(CantMaxima) <> 0 Then
                    If DBNullToDbl(CantMaxima) > Cantidad Then
                        txtcant_DblValue.Value = Cantidad
                    Else
                        txtcant_DblValue.Value = CantMaxima
                    End If
                End If
                bActualizar = True
            End If

            If txtprec_DblValue.Value <> Precio And Precio > 0 Then
                txtprec_DblValue.Value = Precio
                bActualizar = True
            End If

            If bActualizar Then
                MostrarBotonGuardarCesta()
                ActualizarImportesPedido(oOrden.Prove, oOrden.Moneda, oOrden.Categoria, oOrden.Nivel, True, oOrden.ID)
            End If
        Else

            Dim CantidadMinima As Double = CDbl(CType(filagrid.FindControl("WNumEdTotLin"), WebControl).Attributes("minima"))
            Dim CantidadMaxima As Double
            If CType(filagrid.FindControl("WNumEdTotLin"), WebControl).Attributes("maximo") <> String.Empty Then
                CantidadMaxima = CDbl(CType(filagrid.FindControl("WNumEdTotLin"), WebControl).Attributes("maximo"))
            Else
                CantidadMaxima = 0
            End If
            If ctlTotalLinea.Text <> Cantidad Then
                If CantidadMinima < Cantidad Then
                    ctlTotalLinea.Text = Cantidad
                    LblTotLin_DblValue.Value = Cantidad
                    LblTotLinAnt_DblValue.Value = Cantidad
                Else
                    ctlTotalLinea.Text = CantidadMinima
                    LblTotLin_DblValue.Value = CantidadMinima
                    LblTotLinAnt_DblValue.Value = CantidadMinima
                End If
                If CantidadMaxima <> 0 Then
                    If CantidadMaxima > Cantidad Then
                        ctlTotalLinea.Text = Cantidad
                        LblTotLin_DblValue.Value = Cantidad
                        LblTotLinAnt_DblValue.Value = Cantidad
                    Else
                        ctlTotalLinea.Text = CantidadMaxima
                        LblTotLin_DblValue.Value = CantidadMaxima
                        LblTotLinAnt_DblValue.Value = CantidadMaxima
                    End If
                End If
                MostrarBotonGuardarCesta()
                ActualizarImportesPedido(oOrden.Prove, oOrden.Moneda, oOrden.Categoria, oOrden.Nivel, True, oOrden.ID)
            End If
        End If
    End Sub
    ''' Revisado por: sra (12/01/2012)
    ''' <summary>
    ''' Actualiza las etiquetas de unidades e importes de una orden
    ''' </summary>
    ''' <param name="Proveedor">Código de proveedor</param>
    ''' <param name="Moneda">Código de moneda</param>
    ''' <param name="UPUpdate">Nos indica si hay que actualizar el panel de la orden o no</param>
    Private Sub ActualizarImportesPedido(ByVal Proveedor As String, ByVal Moneda As String, ByVal Cat As Integer, ByVal Nivel As Integer, ByVal UPUpdate As Boolean, ByVal ordenId As Integer)
        Dim dlorden As DataListItem = dlCesta_FindItem(Proveedor, Moneda, Cat, Nivel, ordenId)
        Dim totalPedido As Double = 0

        For Each fila As GridViewRow In CType(dlorden.FindControl("GridLineas"), GridView).Rows
            If fila.Visible Then
                If CType(fila.FindControl("LblTotLin"), Label).Visible = True Then
                    Dim txtcant_DblValue As HiddenField = fila.FindControl("wnumCantidad_DblValue")
                    Dim txtprec_DblValue As HiddenField = fila.FindControl("WNumEdPrecArt_DblValue")

                    totalPedido = totalPedido + txtcant_DblValue.Value * txtprec_DblValue.Value

                    CType(fila.FindControl("LblTotLin"), Label).Text = _
                        FormatNumber(txtcant_DblValue.Value * txtprec_DblValue.Value, Me.Usuario.NumberFormat) & " " & Moneda
                    'Campo con el importe total por línea que usaremos para tener el dato en javascript
                    CType(fila.FindControl("WNumEdTotLin"), FSNWebControls.FSNTextBox).Text = FormatNumber(txtcant_DblValue.Value * txtprec_DblValue.Value, Me.Usuario.NumberFormat)
                    CType(fila.FindControl("LblTotLin_DblValue"), HiddenField).Value = txtcant_DblValue.Value * txtprec_DblValue.Value
                Else
                    Dim txtimp_DblValue As HiddenField = fila.FindControl("LblTotLin_DblValue")
                    CType(fila.FindControl("WNumEdTotLin"), FSNWebControls.FSNTextBox).Text = FormatNumber(txtimp_DblValue.Value, Me.Usuario.NumberFormat)
                    totalPedido = totalPedido + txtimp_DblValue.Value
                End If

            End If
        Next

        'Importe de la orden con formato
        CType(dlorden.FindControl("LblPrecPorProve"), Label).Text = FormatNumber(totalPedido, Me.Usuario.NumberFormat) & " " & Moneda
        'Importe de la orden en valor completo. Este campo es necesario porque vamos a trabajar con él desde javascript.
        CType(dlorden.FindControl("LblPrecPorProve_DblValue"), HiddenField).Value = totalPedido

        If UPUpdate Then
            'REFRESCAR LOS UPDATE PANEL
            Dim upOrden As UpdatePanel = TryCast(dlorden.FindControl("upOrden"), UpdatePanel)
            If upOrden IsNot Nothing Then
                upOrden.Update()
            End If
        End If
    End Sub
    ''' <summary>
    ''' Localiza en un GridView la fila correspondiente a un ID de línea determinado
    ''' </summary>
    ''' <param name="grid">Control GridView en el que buscar</param>
    ''' <param name="Linea">Id de la línea</param>
    ''' <returns>Un objeto GridViewRow con la fila correspondiente</returns>
    Private Function GridLineas_FindItem(ByVal grid As GridView, ByVal Linea As Integer) As GridViewRow
        Dim filagrid As GridViewRow = Nothing
        Try
            filagrid = From Datos In grid.Rows _
                Where CType(Datos, GridViewRow).Attributes("LINEA") = Linea _
                Select Datos Distinct.First()
        Catch ex As InvalidOperationException
            ' Si no la encuentra devuelve Nothing
        End Try
        Return filagrid
    End Function
    ''' Revisado por: sra (12/01/2012)
    ''' <summary>
    ''' Lanza el procedimiento que configura las líneas del grid según el tipo de pedido seleccionado
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">EventArgs que contiene datos de eventos.</param>
    ''' <remarks>Se produce después de que el control de servidor se enlaza a un origen de datos.</remarks>
    Protected Sub GridLineas_DataBound(ByVal sender As Object, ByVal e As System.EventArgs)
        'Esta funcion se ejecuta dos veces, desde el databind del dlCesta (desconozco el motivo) y desde el databind del GRidLineas
        'Para evitar que en el primer caso llamemos innecesariamente a la funcion ConfigurarLineas cuando en realidad no hay aún líneas en 
        'el GRidLineas (no hemos pasado aún por el GRidLineas_RowDatabound) controlamos si hay lineas:
        If CType(sender, GridView).Rows.Count > 0 Then
            '_CestaTemporal es una variable que contiene la cesta tal y como estaba al iniciar el proceso de Databind del control GRidLineas
            'Y que vamos a usar para ahorrar el coste de tiempo que implica CESTA
            If Not _CestaTemporal Is Nothing Then
                ConfigurarLineas(CType(sender, WebControl).NamingContainer, _CestaTemporal)
            Else
                ConfigurarLineas(CType(sender, WebControl).NamingContainer, Me.Cesta)
            End If
        End If
    End Sub
#End Region
#Region "Unidades de Pedido"
    ''' <summary>
    ''' Cambia los datos mostrados en el panel de unidades mostrando los que correspondan a la unidad seleccionada.
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Objeto System.EventArgs que contiene los datos de eventos. </param>
    ''' <remarks>Se produce cuando la selección del control de lista cambia entre cada envío al servidor.</remarks>
    Private Sub cmbUnidades_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbUnidades.SelectedIndexChanged
        Dim Unidades As CUnidadesPedido = Me.FSNServer.Get_Object(GetType(FSNServer.CUnidadesPedido))
        Dim oOrden As COrden = Me._Cesta.Find( _
            Function(elorden As COrden) elorden.Lineas.ItemId(btnAceptarUnidad.CommandArgument) IsNot Nothing)
        Dim oLinea As CLinea = oOrden.Lineas.ItemId(btnAceptarUnidad.CommandArgument)
        If oLinea.LineaCatalogo = 0 Then
            Unidades.CargarTodasLasUnidades(Me.Idioma)
        Else
            Unidades.DevolverUnidadesPedido(Me.Idioma, oLinea.LineaCatalogo)
        End If
        Dim Unidad As CUnidadPedido = Unidades.Item(CType(sender, DropDownList).SelectedValue)
        lblCodUnidadSrv.Text = Unidad.Codigo & " - " & Unidad.Denominacion
        If oLinea.LineaCatalogo > 0 Then
            Dim sFactorConversion As String
            If Unidad.FactorConversion = CInt(Unidad.FactorConversion) Then
                sFactorConversion = CInt(Unidad.FactorConversion).ToString()
            Else
                sFactorConversion = FSNLibrary.FormatNumber(Unidad.FactorConversion, Me.Usuario.NumberFormat)
            End If
            lblUnidadCompraSrv.Text = Unidad.UnidadCompra & " - " & Unidad.DenUnidadCompra
            lblFactorConversionSrv.Text = Unidad.Codigo & " = " & sFactorConversion & " " & Unidad.UnidadCompra

            Unidad.AsignarFormatoaUnidadPedido(Me.Usuario.NumberFormat)
            Dim sCantidad As String = Unidad.CantidadMinima.ToString
            If Not Unidad.UnitFormat Is Nothing Then
                If Unidad.NumeroDeDecimales Is Nothing Then
                    Dim decimales As Integer = NumeroDeDecimales(DBNullToStr(Unidad.CantidadMinima))
                    Unidad.UnitFormat.NumberDecimalDigits = decimales
                End If
                sCantidad = FSNLibrary.modUtilidades.FormatNumber(Unidad.CantidadMinima, Unidad.UnitFormat)
            End If
            lblCantMinimaSrv.Text = sCantidad
        End If
        updModifUnidad.Update()
    End Sub
    ''' <summary>
    ''' Acepta los cambios realizados en el panel de unidades.
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce cuando se hace clic en el control FSNButton btnAceptarUnidad.</remarks>
    Private Sub btnAceptarUnidad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarUnidad.Click
        Dim iFCAnt As Integer
        Dim oOrden As COrden = Me._Cesta.Find( _
            Function(elorden As COrden) elorden.Lineas.ItemId(btnAceptarUnidad.CommandArgument) IsNot Nothing)
        Dim oLinea As CLinea = oOrden.Lineas.ItemId(btnAceptarUnidad.CommandArgument)
        Dim dlorden As DataListItem = dlCesta_FindItem(oOrden.Prove, oOrden.Moneda, oOrden.Categoria, oOrden.Nivel, oOrden.ID)
        Dim grlinea As GridViewRow = GridLineas_FindItem(dlorden.FindControl("GridLineas"), btnAceptarUnidad.CommandArgument)
        Dim lnkUnidad As FSNWebControls.FSNLinkTooltip = grlinea.FindControl("lnkUnidad")
        If lnkUnidad.Text <> cmbUnidades.SelectedValue Then
            Dim Unidades As CUnidadesPedido = Me.FSNServer.Get_Object(GetType(FSNServer.CUnidadesPedido))
            If oLinea.LineaCatalogo > 0 Then
                Unidades.DevolverUnidadesPedido(Me.Idioma, oLinea.LineaCatalogo)
            Else
                Unidades.CargarTodasLasUnidades(Me.Idioma)
            End If
            Dim Unidad As CUnidadPedido = Unidades.Item(cmbUnidades.SelectedValue)
            lnkUnidad.Text = Unidad.Codigo
            If oLinea.LineaCatalogo > 0 Then
                lnkUnidad.ContextKey = Unidad.Codigo & "_@_" & oLinea.LineaCatalogo
                Dim wnumCantidad As FSNWebControls.FSNTextBox = grlinea.FindControl("wnumCantidad")
                Dim wnumCantidad_DblValue As HiddenField = grlinea.FindControl("wnumCantidad_DblValue")
                Dim wnumPrecio As FSNWebControls.FSNTextBox = grlinea.FindControl("WNumEdPrecArt")
                Dim wnumPrecio_DblValue As HiddenField = grlinea.FindControl("WNumEdPrecArt_DblValue")

                Dim Cantidad As Double = wnumCantidad_DblValue.Value
                If Cantidad < Unidad.CantidadMinima Then
                    wnumCantidad_DblValue.Value = Unidad.CantidadMinima
                    Cantidad = Unidad.CantidadMinima
                End If
                '' Dejo los valores de la unidad por defecto para poder trabajar con los factores de conversión
                '' porque son respecto de esta unidad, si entro por primero vez le aplico el factor de conversión
                If HttpContext.Current.Cache("FCAnt_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) Is Nothing Then
                    iFCAnt = 0
                Else
                    iFCAnt = HttpContext.Current.Cache("FCAnt_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
                End If
                If (iFCAnt = 0) And (Unidad.FactorConversion = 1) Then
                    iFCAnt = oLinea.FC
                ElseIf iFCAnt = 0 Then
                    iFCAnt = 1
                End If
                wnumCantidad_DblValue.Value = wnumCantidad_DblValue.Value * iFCAnt
                wnumPrecio_DblValue.Value = wnumPrecio_DblValue.Value / iFCAnt
                iFCAnt = Unidad.FactorConversion
                HttpContext.Current.Cache("FCAnt_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) = iFCAnt
                '' Una ver con los valores de la unidad por defecto se pueden aplicar los cambios
                wnumCantidad_DblValue.Value = wnumCantidad_DblValue.Value / Unidad.FactorConversion
                Dim CantMinima As Double = IIf(IsDBNull(Unidad.CantidadMinima), 0, Unidad.CantidadMinima)
                'Para que este configurar funcione es necesario que luego haya un refresco del updatepanel
                'ahora mismo ese refresco se produce en la llamada a ActualizarImportesPedido pero podría cambiar (porque ahora es a toda la orden y podría cambiar a los controles). Cuidadín
                configurarDecimales(Nothing, wnumCantidad, CantMinima, Nothing)

                If lnkUnidad.Attributes("fc") Is Nothing Then
                    lnkUnidad.Attributes.Add("fc", Unidad.FactorConversion.ToString())
                Else
                    lnkUnidad.Attributes("fc") = Unidad.FactorConversion.ToString()
                End If
                lnkUnidad.Attributes("CantMinima") = Unidad.CantidadMinima.ToString()

                wnumPrecio_DblValue.Value = wnumPrecio_DblValue.Value * Unidad.FactorConversion

                configurarDecimales(Nothing, wnumPrecio, Nothing, Nothing)

                CType(grlinea.FindControl("LblPrecArt"), Label).Text = wnumPrecio.Text

                'ActualizarImportesPedido ya hace refresco de los updatepanel
                ActualizarImportesPedido(oOrden.Prove, oOrden.Moneda, oOrden.Categoria, oOrden.Nivel, True, oOrden.ID)
                oLinea.FC = Unidad.FactorConversion
            Else
                lnkUnidad.ContextKey = Unidad.Codigo
                If Me.DesdeFavorito Then
                    If Not favNuevo Then
                        Dim dsfav As DataSet = TryCast(HttpContext.Current.Cache("DsetFavoritoEmitir_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString), DataSet)
                        If Not dsfav Is Nothing Then
                            Dim dr As DataRow = dsfav.Tables("LINEASPEDIDO").Rows.Find(oLinea.ID)
                            dr.Item("UNIDAD") = oLinea.UP
                            HttpContext.Current.Cache("DsetFavoritoEmitir_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) = dsfav
                        End If
                    Else
                        If Not HttpContext.Current.Cache("favNuevo" & Me.Usuario.Cod) Is Nothing Then
                            Dim oArt As CArticulo = CType(CType(HttpContext.Current.Cache("favNuevo" & Me.Usuario.Cod), Object())(0), CArticulo)
                            oArt.UP = oLinea.UP
                            HttpContext.Current.Cache("favNuevo" & Me.Usuario.Cod)(0) = oArt
                        End If
                    End If
                End If

                'REFRESCAR LOS UPDATE PANEL
                Dim upOrden As UpdatePanel = dlorden.FindControl("upOrden")
                upOrden.Update()
            End If
        End If
        MostrarBotonGuardarCesta()
        mpepnlUnidad.Hide()
    End Sub
#End Region
#Region "Tipo de pedido"
    ''' <summary>
    ''' Combina el tipo de artículo con el tipo de pedido para obtener el que se debe aplicar a la línea
    ''' </summary>
    ''' <param name="TipoLinea">Tipo del artículo</param>
    ''' <param name="TipoOrden">Tipo del pedido</param>
    ''' <returns>El tipo a aplicar a la línea</returns>
    Private Function CombinarTipos(ByVal TipoLinea As Integer, ByVal TipoOrden As Integer) As Integer
        Select Case TipoLinea
            Case 0, 1
                Return TipoLinea
            Case 2
                Select Case TipoOrden
                    Case 0, 1, 2
                        Return TipoOrden
                    Case Else
                        Throw New ApplicationException("Tipo erróneo")
                End Select
            Case Else
                Throw New ApplicationException("Tipo erróneo")
        End Select
    End Function
    ''' <summary>
    ''' Comprueba que se ha introducido un centro de coste y una partida presupuestaria válidos
    ''' </summary>
    ''' <param name="source">Origen del evento</param>
    ''' <param name="args">ServerValidateEventArgs que contiene los datos del evento.</param>
    ''' <remarks>Se produce cuando la validación se realiza en el servidor.
    ''' </remarks>
    Public Sub valTipoArticulo_ServerValidate(ByVal source As Object, ByVal args As ServerValidateEventArgs)
        args.IsValid = False
    End Sub
    ''' <summary>
    ''' Configura las líneas del GridView en función del tipo de pedido seleccionado
    ''' </summary>
    ''' <param name="dlOrden">DataListItem de la orden cuyo GridView se va a configurar
    ''' </param>
    ''' <param name="CestaTemporal">Valores de la cesta en el momento inmediatamente anterior al databind del control GRidLineas</param>
    ''' <remarks>Llamada desde GridLineas_DataBound</remarks>
    Private Sub ConfigurarLineas(ByVal dlOrden As DataListItem, ByVal CestaTemporal As COrdenes)

        Dim prove As String = CType(dlOrden.FindControl("FSNLinkProve"), FSNWebControls.FSNLinkInfo).ContextKey
        Dim mon As String = CType(dlOrden.FindControl("FSNLinkProve"), FSNWebControls.FSNLinkInfo).CommandArgument
        Dim id As String = CType(dlOrden.FindControl("FSNLinkProve"), FSNWebControls.FSNLinkInfo).CommandName
        Dim TipoPedidoOrden As cTipoPedido = Nothing

        'Ahora me recorro cada línea de pedido dentro del GridView que contiene el datalist
        Dim GridLineas As GridView = CType(dlOrden.FindControl("GridLineas"), GridView)

        For Each lineaGrid As GridViewRow In GridLineas.Rows
            'Vamos a usar CestaTemporal en vez de Cesta porque CestaTemporal contiene los datos de la cesta
            'y así no recorremos de nuevo todos los controles para recuparar sus datos, que es lo que hace Cesta
            Dim oLinea = CestaTemporal.Item(id).Lineas.ItemId(CType(lineaGrid.FindControl("LblNomArt"), LinkButton).CommandArgument)

            Dim oOrden As COrden = Nothing
            oOrden = CestaTemporal.Find(Function(elorden As COrden) elorden.Lineas.ItemId(oLinea.ID) IsNot Nothing)

            'Recuperamos los datos de tipo de línea
            Dim oTipoLinea As CArticulo.strTipoArticulo = recuperarTipoLinea(TipoPedidoOrden, oLinea)
            'Configurar el control filaAviso
            ConfigurarAvisoNoIncluirLinea(lineaGrid, TipoPedidoOrden, oLinea, oTipoLinea)


        Next

        'REFRESCAR LOS UPDATE PANEL
        Dim upOrden As UpdatePanel = TryCast(dlOrden.FindControl("upOrden"), UpdatePanel)
        If upOrden IsNot Nothing Then
            upOrden.Update()
        End If
    End Sub
    ''' <summary>
    ''' Función que devuelve los datos de tipo de línea para una línea de GRidview concreta 
    ''' a partir de la información presente en la cesta y el tipo de pedido (cruza los datos del tipo de artículo y del tipo de pedido para determinar el tipo de línea)
    ''' </summary>
    ''' <param name="TipoPedidoOrden">Tipo de pedido de la orden a la que pertenece la línea</param>
    ''' <param name="oLinea">Línea de la cesta con que se corresponde la lineaGrid</param>
    ''' <returns>Una estructura strTipoArticulo con los datos de tipo de línea</returns>
    ''' <remarks></remarks>
    Private Function recuperarTipoLinea(ByVal TipoPedidoOrden As cTipoPedido, ByVal oLinea As CLinea) As CArticulo.strTipoArticulo
        Dim oTipoLinea As CArticulo.strTipoArticulo
        If oLinea.LineaCatalogo = 0 Then 'Si es pedido libre, no tiene info de artículo
            If TipoPedidoOrden Is Nothing Then
                oTipoLinea.TipoAlmacenamiento = CArticulo.EnAlmacenTipo.Opcional
                oTipoLinea.TipoRecepcion = CArticulo.EnRecTipo.Opcional
                oTipoLinea.Concepto = CArticulo.EnInvTipo.Ambos
            Else
                oTipoLinea.TipoAlmacenamiento = TipoPedidoOrden.Almacenable
                oTipoLinea.TipoRecepcion = TipoPedidoOrden.Recepcionable
                oTipoLinea.Concepto = TipoPedidoOrden.Concepto
            End If
        ElseIf TipoPedidoOrden Is Nothing Then 'Si no es pedido libre y no hay tipo de pedido
            oTipoLinea = oLinea.TipoArticulo
        Else 'Si no es pedido libre y hay tipo de pedido
            oTipoLinea.TipoAlmacenamiento = CombinarTipos(oLinea.TipoArticulo.TipoAlmacenamiento, TipoPedidoOrden.Almacenable)
            oTipoLinea.TipoRecepcion = CombinarTipos(oLinea.TipoArticulo.TipoRecepcion, TipoPedidoOrden.Recepcionable)
            oTipoLinea.Concepto = CombinarTipos(oLinea.TipoArticulo.Concepto, TipoPedidoOrden.Concepto)
        End If
        Return oTipoLinea
    End Function
    ''' <summary>
    ''' Configura la Fila Aviso de una sola línea del GridView en función del tipo de pedido seleccionado
    ''' </summary>
    ''' <param name="lineaGrid">Línea del GridView que se va a configurar</param>
    ''' <param name="TipoPedidoOrden">Tipo de pedido de la orden a la que pertenece la línea</param>
    ''' <param name="oLinea">Línea de la cesta con que se corresponde la lineaGrid</param>
    ''' <param name="oTipoLinea">Datos del tipo de línea</param>
    ''' <remarks>Llamado desde ConfigurarLineas</remarks>
    Private Sub ConfigurarAvisoNoIncluirLinea(ByRef lineaGrid As GridViewRow, ByVal TipoPedidoOrden As cTipoPedido, ByVal oLinea As CLinea, ByVal oTipoLinea As CArticulo.strTipoArticulo)

        Dim TextoAviso As String = String.Empty

        If oLinea.LineaCatalogo <> 0 AndAlso TipoPedidoOrden IsNot Nothing Then 'Si no es pedido libre y hay tipo de pedido
            If oLinea.TipoArticulo.TipoAlmacenamiento = CArticulo.EnAlmacenTipo.NoAlmacenable And TipoPedidoOrden.Almacenable = cTipoPedido.EnAlmacenTipo.Obligatorio Then _
                TextoAviso = TextoAviso & Textos(151)
            If oLinea.TipoArticulo.TipoAlmacenamiento = CArticulo.EnAlmacenTipo.Obligatorio And TipoPedidoOrden.Almacenable = cTipoPedido.EnAlmacenTipo.NoAlmacenable Then _
                TextoAviso = TextoAviso & Textos(148)
            If oLinea.TipoArticulo.Concepto = CArticulo.EnInvTipo.Gasto And TipoPedidoOrden.Concepto = cTipoPedido.EnInvTipo.Inversion Then _
                TextoAviso = TextoAviso & Textos(153)
            If oLinea.TipoArticulo.Concepto = CArticulo.EnInvTipo.Inversion And TipoPedidoOrden.Concepto = cTipoPedido.EnInvTipo.Gasto Then _
                TextoAviso = TextoAviso & Textos(150)
            If oLinea.TipoArticulo.TipoRecepcion = CArticulo.EnRecTipo.NoRecepcionable And TipoPedidoOrden.Recepcionable = cTipoPedido.EnRecTipo.Obligatorio Then _
                TextoAviso = TextoAviso & Textos(152)
            If oLinea.TipoArticulo.TipoRecepcion = CArticulo.EnRecTipo.Obligatorio And TipoPedidoOrden.Recepcionable = cTipoPedido.EnRecTipo.NoRecepcionable Then _
                TextoAviso = TextoAviso & Textos(149)
        End If
        With lineaGrid
            'FilaAvisoNoIncluir
            If String.IsNullOrEmpty(TextoAviso) Then
                .FindControl("FilaAvisoNoIncluir").Visible = False
            Else
                .FindControl("FilaAvisoNoIncluir").Visible = True
                TextoAviso = Textos(135) & ". " & TextoAviso
                CType(.FindControl("lblNoIncluirArticulo"), Label).Text = TextoAviso
            End If
        End With

    End Sub
#End Region
#Region "Cesta de la compra "
    ''' <summary>
    ''' Vacía la cesta y redirige al catálogo
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">EventArgs con los datos del evento</param>
    Protected Sub BtnVaciarCesta_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnVaciarCesta.Click, btnVaciarCestaPie.Click
        Me._Cesta.VaciarCesta(Me.Persona.Cod)
        Try
            For Each oOrden As COrden In Me._Cesta
                If oOrden.Adjuntos IsNot Nothing Then
                    Do While oOrden.Adjuntos.Count > 0
                        IO.File.Delete(oOrden.Adjuntos.Item(0).DirectorioGuardar & "/" & oOrden.Adjuntos.Item(0).FileName)
                        oOrden.Adjuntos.RemoveAt(0)
                    Loop
                End If
                For Each oLinea As CLinea In oOrden.Lineas
                    If oLinea.Adjuntos IsNot Nothing Then
                        For Each oAdj As Adjunto In oLinea.Adjuntos
                            If oAdj.tipo <> TiposDeDatos.Adjunto.Tipo.EspecificacionLineaCatalogo Then
                                IO.File.Delete(oAdj.DirectorioGuardar & "/" & oAdj.FileName)
                                oLinea.Adjuntos.Remove(oAdj)
                            End If
                        Next
                    End If
                Next
            Next
        Catch ex As Exception
            ' Si no se eliminan correctamente los eliminará el Garbage Collector
        End Try
        Me.Cesta.Clear()
        Response.Redirect("CatalogoWeb.aspx")
    End Sub
    ''' <summary>
    ''' Muestra los botones de "Guardar Cesta"
    ''' </summary>
    Private Sub MostrarBotonGuardarCesta()
        Dim sScript As String = "mostrarguardarcesta();"
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "mostrarguardarcesta", sScript, True)
    End Sub
    ''' <summary>
    ''' Oculta los botones de "Guardar Cesta"
    ''' </summary>
    Private Sub OcultarBotonGuardarCesta()
        Dim sScript As String = "ocultarguardarcesta();"
        ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "ocultarguardarcesta", sScript, True)
    End Sub
#End Region
#Region "Inicializacion de controles con Idiomas "
    Public Sub CargarTextos()
        'Poner maás textos(y luego quitar este comentario)
        LblSinArticulosCesta.Text = "<p>&nbsp;</p><p>&nbsp;</p>" & Textos(117) & "<p>&nbsp;</p>"
        BtnAceptarEmisionOkOMal.Text = Textos(78)
        lblCabUnidades.Text = Textos(121)
        lblCodUnidad.Text = Textos(122)
        lblUnidadCompra.Text = Textos(123)
        lblFactorConversion.Text = Textos(124)
        lblCantMinima.Text = Textos(125)
        btnAceptarUnidad.Text = Textos(79)
        btnCancelarUnidad.Text = Textos(34)
        BtnVaciarCesta.Text = Textos(108)
        BtnVaciarCesta.ToolTip = Textos(108)
        BtnVaciarCesta.OnClientClick = "if (confirm('" & JSText(Textos(109)) & "')) {TempCargando(); return true} else return false"
        btnVaciarCestaPie.Text = Textos(108)
        btnVaciarCestaPie.ToolTip = Textos(108)
        btnVaciarCestaPie.OnClientClick = "if (confirm('" & JSText(Textos(109)) & "')) {TempCargando(); return true} else return false"

        BtnGuardarCesta.Text = Textos(115) 'Guardar Cesta
        btnGuardarCestaPie.Text = Textos(115) 'Guardar Cesta

        btnAceptarConfirm.Text = Textos(79)
        btnCancelarConfirm.Text = Textos(34)
        btnAceptarMsgBox.Text = Textos(79)
        btnCancelarMsgBox.Text = Textos(34)

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
            Dim sVariableJavascriptTextosPantalla As String = "var TextosPantalla = new Array();"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[0]='" & JSText(Textos(294)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[1]='" & JSText(Textos(295)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[2]='" & JSText(Textos(296)) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextosPantalla, True)
        End If

    End Sub
    ''' <summary>
    ''' Método para cargar los textos de controles del control GridLineas
    ''' </summary>
    ''' <param name="eRow">Fila del GridLineas para la que se desea cargar textos</param>
    ''' <remarks>
    ''' Se llama desde gridLineas_RowDataBound
    ''' </remarks>
    Private Sub CargarTextosGridLineas(ByVal eRow As GridViewRow)
        With eRow
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
        End With
    End Sub
#End Region
#Region "Unidades"
    ''' <summary>
    ''' Propiedad que recupera el conjunto de las unidades de Pedido para su posterior uso en la página
    ''' </summary>
    Private ReadOnly Property TodasLasUnidades() As FSNServer.CUnidadesPedido
        Get
            Dim oUnidadesPedido As FSNServer.CUnidadesPedido = Me.FSNServer.Get_Object(GetType(FSNServer.CUnidadesPedido))
            If HttpContext.Current.Cache("TodasLasUnidades_" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                oUnidadesPedido.CargarTodasLasUnidades(Me.Usuario.Idioma)
                Me.InsertarEnCache("TodasLasUnidades" & Me.Usuario.Idioma.ToString(), oUnidadesPedido)
            Else
                oUnidadesPedido = HttpContext.Current.Cache("TodasLasUnidades" & Me.Usuario.Idioma.ToString())
            End If
            Return oUnidadesPedido
        End Get
    End Property
    ''' <summary>
    ''' Método que recupera el número de decimales para una unidad de pedido dada y configura la presentación de los datos en los controles FSNTExtBox
    ''' </summary>
    ''' <param name="unidad">Código de la unidad de pedido</param>
    ''' <param name="oFSNTextBox">Control FSNTextBox a configurar</param>
    ''' <remarks>Llamada desde: la página, allí donde se configure un control webNumericEdit. Tiempo máximo: inferior a 0,3</remarks>
    Private Sub configurarDecimales(ByVal unidad As Object, ByRef oFSNTextBox As FSNWebControls.FSNTextBox, ByRef valorMinimo As Nullable(Of Double), ByRef valorMaximo As Nullable(Of Double))
        Dim oUnidadPedido As FSNServer.CUnidadPedido = Nothing
        If Not DBNullToSomething(unidad) = Nothing Then
            unidad = Trim(unidad.ToString)
            oUnidadPedido = TodasLasUnidades.Item(unidad)
            oUnidadPedido.AsignarFormatoaUnidadPedido(Me.Usuario.NumberFormat)
        End If

        Dim vprecisionfmt As Integer
        Dim minimo As String = IIf(valorMinimo Is Nothing, "null", valorMinimo)
        Dim maximo As String = IIf(valorMaximo Is Nothing, "null", valorMaximo)
        If minimo <> "null" Then
            minimo = minimo.ToString().Replace(",", ".")
        End If
        If maximo <> "null" Then
            maximo = maximo.ToString().Replace(",", ".")
        End If
        Dim vdecimalfmt As String = Me.Usuario.NumberFormat.NumberDecimalSeparator
        Dim vthousanfmt As String = Me.Usuario.NumberFormat.NumberGroupSeparator
        Dim oFSNTextBox_DblValue As HiddenField
        If oFSNTextBox.ID = "WNumEdTotLin" Then
            oFSNTextBox_DblValue = oFSNTextBox.NamingContainer.FindControl("LblTotLin_DblValue")
        Else
            oFSNTextBox_DblValue = oFSNTextBox.NamingContainer.FindControl(oFSNTextBox.ID & "_DblValue")
        End If
        'Dim oFSNTextBox_DblValue As HiddenField = oFSNTextBox.NamingContainer.FindControl(oFSNTextBox.ID & "_DblValue")
        Dim oFSNTextBoxArrows As FSNWebControls.FSNUpDownArrows = oFSNTextBox.NamingContainer.FindControl(oFSNTextBox.ID & "Arrows")

        If Not oUnidadPedido Is Nothing Then
            Dim unitFormatTemp As System.Globalization.NumberFormatInfo = oUnidadPedido.UnitFormat.Clone
            If oUnidadPedido.NumeroDeDecimales Is Nothing Then
                'Cuando el número de decimales de la unidad no está definido, teóricamente puede ser cualquiera pero
                'vamos a poner un límite de 99. Si fuese necesario cambiar esto, habrá que revisar su repercusión en jsTextBoxFormat.js
                'pues allí se controla el valor 99
                vprecisionfmt = 99
                oUnidadPedido.UnitFormat.NumberDecimalDigits = 0
                Dim num As Double = strToDbl(oFSNTextBox_DblValue.Value)
                If Not Double.IsNaN(num) AndAlso (num <> CInt(num)) Then
                    num = num - CInt(num)
                    Dim numDecimales As Integer = 0
                    If num.ToString.IndexOf(".") > 0 Then
                        numDecimales = num.ToString.Substring(num.ToString.IndexOf(".") + 1).Length
                    ElseIf num.ToString.IndexOf(",") > 0 Then
                        numDecimales = num.ToString.Substring(num.ToString.IndexOf(",") + 1).Length
                    End If
                    unitFormatTemp.NumberDecimalDigits = numDecimales
                Else
                    unitFormatTemp.NumberDecimalDigits = Me.Usuario.NumberFormat.NumberDecimalDigits
                End If
            ElseIf oUnidadPedido.NumeroDeDecimales > 0 Then
                vprecisionfmt = oUnidadPedido.NumeroDeDecimales
            ElseIf oUnidadPedido.NumeroDeDecimales = 0 Then
                vprecisionfmt = oUnidadPedido.NumeroDeDecimales
            End If
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
            oFSNTextBox.Attributes.Add("avisoDecimales", Textos(159) & " " & oUnidadPedido.Codigo & ":")

            oFSNTextBox.Text = FormatNumber(oFSNTextBox_DblValue.Value, unitFormatTemp)

        Else 'Si no tenemos datos de la unidad, usamos los datos del usuario
            vprecisionfmt = Me.Usuario.NumberFormat.NumberDecimalDigits

            oFSNTextBox.Text = FormatNumber(oFSNTextBox_DblValue.Value, Me.Usuario.NumberFormat)
        End If

        'Todas las funciones javascript a las que se llama están en el fichero js/jsTextboxFormat.js
        oFSNTextBox.Attributes.Add("onpaste", "return validarpastenum2(" & oFSNTextBox.ClientID & _
                                                                            ",event" & _
                                                                            ",'" & vdecimalfmt & _
                                                                            "','" & vthousanfmt & _
                                                                            "'," & vprecisionfmt & ");")
        oFSNTextBox.Attributes.Add("onkeypress", "return limiteDecimalesTextBox(" & oFSNTextBox.ClientID & _
                                                                            ",event" & _
                                                                            ",'" & vdecimalfmt & _
                                                                            "','" & vthousanfmt & _
                                                                            "'," & vprecisionfmt & ", false);")
        Dim errMsg As String = Textos(41) & " " & Textos(42) & " """ & vdecimalfmt & """ " & Textos(43) & " """ & vthousanfmt & """."

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
        Dim errLimites As String = Textos(183) & " " & _
                                    IIf(minimo <> "null", "\n" & Textos(181) & " ' + num2str(num2str4DBlValue(" & minimo & "),'" & vdecimalfmt & "','" & vthousanfmt & "'," & vprecisionfmt & ") + '", "") & _
                                    IIf(maximo <> "null", "\n" & Textos(182) & " ' + num2str(num2str4DBlValue(" & maximo & "),'" & vdecimalfmt & "','" & vthousanfmt & "'," & vprecisionfmt & ") + '", "")


        'Cuando se cambia el precio por unidad o la cantidad de una línea de pedido,
        'hay que cambiar el precio total de la línea y el de la orden
        'Esta función se aplica a más elementos que los controles de precio y cantidad de las líneas por lo que 
        'sólo pasaremos al javascript info de los controles cuando sean específicamente los controles de la línea.
        Dim sTipoControl As String = String.Empty
        Dim sLblTotLinID As String = String.Empty
        Dim sLblPrecPorProveID As String = String.Empty
        Dim sPrecioOCantidadID As String = String.Empty
        Dim sLblPrecArtID As String = String.Empty
        Dim sMoneda As String = String.Empty

        If oFSNTextBox.ID = "wnumCantidad" OrElse oFSNTextBox.ID = "WNumEdPrecArt" Then
            Dim grfila As GridViewRow = oFSNTextBox.NamingContainer
            Dim dlorden As DataListItem = grfila.NamingContainer.NamingContainer
            If oFSNTextBox.ID = "wnumCantidad" Then
                sTipoControl = "Cantidad"
                sPrecioOCantidadID = grfila.FindControl("WNumEdPrecArt_DblValue").ClientID
                sLblPrecArtID = grfila.FindControl("LblPrecArt").ClientID
            Else
                sTipoControl = "Precio"
                sPrecioOCantidadID = grfila.FindControl("wnumCantidad_DblValue").ClientID
            End If
            sLblTotLinID = grfila.FindControl("LblTotLin").ClientID
            sLblPrecPorProveID = dlorden.FindControl("LblPrecPorProve").ClientID
            sMoneda = dlorden.Attributes("MONEDA")

            oFSNTextBox.Attributes.Add("minimo", IIf(valorMinimo Is Nothing, "", valorMinimo))
            oFSNTextBox.Attributes.Add("maximo", IIf(valorMaximo Is Nothing, "", valorMaximo))

            Dim sOperation As String
            Dim oTd_Up As HtmlControls.HtmlGenericControl = CType(oFSNTextBoxArrows.FindControl(oFSNTextBoxArrows.ID & "_td_up"), HtmlControls.HtmlGenericControl)
            Dim oTd_Down As HtmlControls.HtmlGenericControl = CType(oFSNTextBoxArrows.FindControl(oFSNTextBoxArrows.ID & "_td_down"), HtmlControls.HtmlGenericControl)
            sOperation = "add"
            oTd_Up.Attributes.Add("onmousedown", "lowlight(this, event); " & _
               "beginAddOrSubstrackOne('" & oFSNTextBox.ClientID & "', " & _
                      minimo & ", " & _
                      maximo & ", '" & _
                      vdecimalfmt & "', '" & _
                      vthousanfmt & "', " & _
                      vprecisionfmt & ", '" & _
                      sOperation & _
                      "', event,'" & _
                      errMsg & "','" & _
                      errLimites & "','" & _
                      sTipoControl & "','" & _
                      sLblTotLinID & "','" & _
                      sLblPrecPorProveID & "','" & _
                      sPrecioOCantidadID & "','" & _
                      sLblPrecArtID & "','" & _
                      sMoneda & "'," & _
                      Me.Usuario.NumberFormat.NumberDecimalDigits & ");")
            sOperation = "subtract"
            oTd_Down.Attributes.Add("onmousedown", "lowlight(this, event); " & _
               "beginAddOrSubstrackOne('" & oFSNTextBox.ClientID & "', " & _
                      minimo & ", " & _
                      maximo & ", '" & _
                      vdecimalfmt & "', '" & _
                      vthousanfmt & "', " & _
                      vprecisionfmt & ", '" & _
                      sOperation & _
                      "', event,'" & _
                      errMsg & "','" & _
                      errLimites & "','" & _
                      sTipoControl & "','" & _
                      sLblTotLinID & "','" & _
                      sLblPrecPorProveID & "','" & _
                      sPrecioOCantidadID & "','" & _
                      sLblPrecArtID & "','" & _
                      sMoneda & "'," & _
                      Me.Usuario.NumberFormat.NumberDecimalDigits & ");")
            oTd_Up.Attributes.Add("onmouseover", "highlight(this);")
            oTd_Down.Attributes.Add("onmouseover", "highlight(this);")
            oTd_Up.Attributes.Add("onmouseout", "endAddOrSubstrackOne(); unhighlight(this);")
            oTd_Down.Attributes.Add("onmouseout", "endAddOrSubstrackOne(); unhighlight(this);")
            oTd_Up.Attributes.Add("onmouseup", "unlowlight(this); endAddOrSubstrackOne();")
            oTd_Down.Attributes.Add("onmouseup", "unlowlight(this); endAddOrSubstrackOne();")

            oFSNTextBox.Attributes.Add("onchange", "addThousandSeparator(" & oFSNTextBox.ClientID & ",'" & _
                                                                    vdecimalfmt & "','" & _
                                                                    vthousanfmt & "'," & _
                                                                    vprecisionfmt & "," & _
                                                                    "event); validarNumero(" & _
                                                                    oFSNTextBox.ClientID & ",'" & _
                                                                    vdecimalfmt & "','" & _
                                                                    vthousanfmt & "'," & _
                                                                    vprecisionfmt & "," & _
                                                                    minimo & "," & _
                                                                    maximo & ",'" & _
                                                                    errMsg & "','" & _
                                                                    errLimites & "','" & _
                                                                    sTipoControl & "','" & _
                                                                    sLblTotLinID & "','" & _
                                                                    sLblPrecPorProveID & "','" & _
                                                                    sPrecioOCantidadID & "','" & _
                                                                    sLblPrecArtID & "','" & _
                                                                    sMoneda & "'," & _
                                                                    Me.Usuario.NumberFormat.NumberDecimalDigits & ");")
        Else
            If oFSNTextBox.ID = "WNumEdTotLin" Then
                Dim grfila As GridViewRow = oFSNTextBox.NamingContainer
                Dim dlorden As DataListItem = grfila.NamingContainer.NamingContainer

                Dim sPrecioCantidadLineaID As String = String.Empty
                sPrecioCantidadLineaID = grfila.FindControl("LblTotLinAnt_DblValue").ClientID
                'Dim sPrecioCantidadLinea As Decimal = hPrecioCantidadLinea.Value
                sTipoControl = "Importe"
                sPrecioOCantidadID = grfila.FindControl("LblTotLin_DblValue").ClientID
                sLblPrecArtID = grfila.FindControl("LblTotLin").ClientID

                sLblTotLinID = grfila.FindControl("WNumEdTotLin").ClientID
                sLblPrecPorProveID = dlorden.FindControl("LblPrecPorProve").ClientID
                sMoneda = dlorden.Attributes("MONEDA")

                oFSNTextBox.Attributes.Add("minimo", IIf(valorMinimo Is Nothing, "", valorMinimo))
                oFSNTextBox.Attributes.Add("maximo", IIf(valorMaximo Is Nothing, "", valorMaximo))

                Dim sOperation As String
                Dim oTd_Up As HtmlControls.HtmlGenericControl = CType(oFSNTextBoxArrows.FindControl(oFSNTextBoxArrows.ID & "_td_up"), HtmlControls.HtmlGenericControl)
                Dim oTd_Down As HtmlControls.HtmlGenericControl = CType(oFSNTextBoxArrows.FindControl(oFSNTextBoxArrows.ID & "_td_down"), HtmlControls.HtmlGenericControl)
                sOperation = "add"
                oTd_Up.Attributes.Add("onmousedown", "lowlight(this, event); " & _
                   "beginAddOrSubstrackOneImporte('" & oFSNTextBox.ClientID & "', " & _
                          minimo & ", " & _
                          maximo & ", '" & _
                          vdecimalfmt & "', '" & _
                          vthousanfmt & "', " & _
                          vprecisionfmt & ", '" & _
                          sOperation & _
                          "', event,'" & _
                          errMsg & "','" & _
                          errLimites & "','" & _
                          sTipoControl & "','" & _
                          sLblTotLinID & "','" & _
                          sLblPrecPorProveID & "','" & _
                          sPrecioOCantidadID & "','" & _
                          sLblPrecArtID & "','" & _
                          sMoneda & "'," & _
                          Me.Usuario.NumberFormat.NumberDecimalDigits & ");")
                sOperation = "subtract"
                oTd_Down.Attributes.Add("onmousedown", "lowlight(this, event); " & _
                   "beginAddOrSubstrackOneImporte('" & oFSNTextBox.ClientID & "', " & _
                          minimo & ", " & _
                          maximo & ", '" & _
                          vdecimalfmt & "', '" & _
                          vthousanfmt & "', " & _
                          vprecisionfmt & ", '" & _
                          sOperation & _
                          "', event,'" & _
                          errMsg & "','" & _
                          errLimites & "','" & _
                          sTipoControl & "','" & _
                          sLblTotLinID & "','" & _
                          sLblPrecPorProveID & "','" & _
                          sPrecioOCantidadID & "','" & _
                          sLblPrecArtID & "','" & _
                          sMoneda & "'," & _
                          Me.Usuario.NumberFormat.NumberDecimalDigits & ");")
                oTd_Up.Attributes.Add("onmouseover", "highlight(this);")
                oTd_Down.Attributes.Add("onmouseover", "highlight(this);")
                oTd_Up.Attributes.Add("onmouseout", "endAddOrSubstrackOne(); unhighlight(this);")
                oTd_Down.Attributes.Add("onmouseout", "endAddOrSubstrackOne(); unhighlight(this);")
                oTd_Up.Attributes.Add("onmouseup", "unlowlight(this); endAddOrSubstrackOne();")
                oTd_Down.Attributes.Add("onmouseup", "unlowlight(this); endAddOrSubstrackOne();")

                oFSNTextBox.Attributes.Add("onchange", "addThousandSeparator(" & oFSNTextBox.ClientID & ",'" & _
                                                                        vdecimalfmt & "','" & _
                                                                        vthousanfmt & "'," & _
                                                                        vprecisionfmt & "," & _
                                                                        "event); validarNumeroImporte(" & _
                                                                        oFSNTextBox.ClientID & ",'" & _
                                                                        vdecimalfmt & "','" & _
                                                                        vthousanfmt & "'," & _
                                                                        vprecisionfmt & "," & _
                                                                        minimo & "," & _
                                                                        maximo & ",'" & _
                                                                        errMsg & "','" & _
                                                                        errLimites & "','" & _
                                                                        sTipoControl & "','" & _
                                                                        sLblTotLinID & "','" & _
                                                                        sLblPrecPorProveID & "','" & _
                                                                        sPrecioOCantidadID & "','" & _
                                                                        sLblPrecArtID & "','" & _
                                                                        sMoneda & "'," & _
                                                                        Me.Usuario.NumberFormat.NumberDecimalDigits & ", '" & sPrecioCantidadLineaID & "');")
            End If
        End If
    End Sub
#End Region
#Region "Pre Render"
    ''' <summary>
    ''' Método que se lanza justo antes de generar el control en pantalla
    ''' </summary>
    ''' <param name="sender">control que lo lanza</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Evento que se lanza en el momento anterior a dibujar el control en pantalla. Tiempo máximo inferior a 0,1 seg</remarks>
    Protected Sub GridLineas_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim gridlineas = CType(sender, GridView)
        For Each Linea As GridViewRow In gridlineas.Rows
            With Linea
                Dim wnumCant As FSNWebControls.FSNTextBox = CType(.FindControl("wnumCantidad"), TextBox)
                Dim wNumEdPrec As FSNWebControls.FSNTextBox = CType(.FindControl("WNumEdPrecArt"), TextBox)


                If ScriptMgr.IsInAsyncPostBack Then
                    'Damos formato al control wnumCantidad y wNumEdPrec en cada prerender del grid para que no se desconfigure en los postback asíncronos
                    Dim lnkUnidad As FSNWebControls.FSNLinkTooltip = CType(.FindControl("lnkUnidad"), FSNWebControls.FSNLinkTooltip)
                    Dim minimo As Nullable(Of Double)
                    Dim maximo As Nullable(Of Double)

                    If String.IsNullOrEmpty(wnumCant.Attributes("minimo")) Then
                        minimo = Nothing
                    Else
                        minimo = CDbl(wnumCant.Attributes("minimo"))
                    End If
                    If String.IsNullOrEmpty(wnumCant.Attributes("maximo")) Then
                        maximo = Nothing
                    Else
                        maximo = CDbl(wnumCant.Attributes("maximo"))
                    End If

                    configurarDecimales(lnkUnidad.Text, wnumCant, minimo, maximo)
                    configurarDecimales(Nothing, wNumEdPrec, Nothing, Nothing)
                End If

                'Insertamos un script que controla el evento paste en el control
                Me.insertarScriptPaste(wnumCant.ClientID)

                'Recortar textos para que quepan en pantalla
                If Not FSNUser.MostrarCodArt Then
                    CType(.FindControl("LblNomArt"), LinkButton).Visible = False
                End If
                CType(.FindControl("LblNomArt"), LinkButton).ToolTip = CType(.FindControl("LblNomArt"), LinkButton).Text
                CType(.FindControl("LblArticulo"), LinkButton).ToolTip = CType(.FindControl("LblArticulo"), LinkButton).Text
                CType(.FindControl("LblNomArt"), LinkButton).Text = AcortarTexto(CType(.FindControl("LblNomArt"), LinkButton).Text, 50)
                CType(.FindControl("LblArticulo"), LinkButton).Text = AcortarTexto(CType(.FindControl("LblArticulo"), LinkButton).Text, 50)
                If CType(.FindControl("wnumCantidadArrows"), HtmlControls.HtmlContainerControl).Visible = True Then
                    Dim wnumCantArrows As HtmlControls.HtmlContainerControl = CType(.FindControl("wnumCantidadArrows"), HtmlControls.HtmlContainerControl)
                    Dim wNumEdPrecArrows As HtmlControls.HtmlContainerControl = CType(.FindControl("WNumEdPrecArtArrows"), HtmlControls.HtmlContainerControl)
                    Dim wnumCantArrows_Td_Up As HtmlControls.HtmlGenericControl = CType(wnumCantArrows.FindControl(wnumCantArrows.ID & "_td_up"), HtmlControls.HtmlGenericControl)
                    Dim wnumCantArrows_Td_Down As HtmlControls.HtmlGenericControl = CType(wnumCantArrows.FindControl(wnumCantArrows.ID & "_td_down"), HtmlControls.HtmlGenericControl)
                    Dim wNumEdPrecArrows_Td_Up As HtmlControls.HtmlGenericControl = CType(wNumEdPrecArrows.FindControl(wNumEdPrecArrows.ID & "_td_up"), HtmlControls.HtmlGenericControl)
                    Dim wNumEdPrecArrows_Td_Down As HtmlControls.HtmlGenericControl = CType(wNumEdPrecArrows.FindControl(wNumEdPrecArrows.ID & "_td_down"), HtmlControls.HtmlGenericControl)


                    If wnumCantArrows_Td_Up.Attributes("onmousedown").ToString.IndexOf("mostrarguardarcesta()") = -1 Then
                        wnumCantArrows_Td_Up.Attributes.Add("onmousedown", wnumCantArrows_Td_Up.Attributes("onmousedown").ToString & "mostrarguardarcesta('" & wnumCantArrows_Td_Up.ClientID & "')")
                    End If
                    If wnumCantArrows_Td_Down.Attributes("onmousedown").ToString.IndexOf("mostrarguardarcesta()") = -1 Then
                        wnumCantArrows_Td_Down.Attributes.Add("onmousedown", wnumCantArrows_Td_Down.Attributes("onmousedown").ToString & "mostrarguardarcesta('" & wnumCantArrows_Td_Down.ClientID & "')")
                    End If
                    If wNumEdPrecArrows_Td_Up.Attributes("onmousedown").ToString.IndexOf("mostrarguardarcesta()") = -1 Then
                        wNumEdPrecArrows_Td_Up.Attributes.Add("onmousedown", wNumEdPrecArrows_Td_Up.Attributes("onmousedown").ToString & "mostrarguardarcesta()")
                    End If
                    If wNumEdPrecArrows_Td_Down.Attributes("onmousedown").ToString.IndexOf("mostrarguardarcesta()") = -1 Then
                        wNumEdPrecArrows_Td_Down.Attributes.Add("onmousedown", wNumEdPrecArrows_Td_Down.Attributes("onmousedown").ToString & "mostrarguardarcesta()")
                    End If
                Else
                    Dim WNumEdTotLin As FSNWebControls.FSNTextBox = CType(.FindControl("WNumEdTotLin"), FSNWebControls.FSNTextBox)
                    If WNumEdTotLin.Attributes("onchange") IsNot Nothing Then
                        WNumEdTotLin.Attributes.Add("onchange", WNumEdTotLin.Attributes("onchange").ToString & "mostrarguardarcesta('" & WNumEdTotLin.ClientID & "');")
                    Else
                        WNumEdTotLin.Attributes.Add("onchange", "mostrarguardarcesta()")
                    End If

                    Dim WNumEdTotLinArrows As HtmlControls.HtmlContainerControl = CType(.FindControl("WNumEdTotLinArrows"), HtmlControls.HtmlContainerControl)
                    Dim wNumEdTotLinArrows_Td_Up As HtmlControls.HtmlGenericControl = CType(WNumEdTotLinArrows.FindControl(WNumEdTotLinArrows.ID & "_td_up"), HtmlControls.HtmlGenericControl)
                    Dim wNumEdTotLinArrows_Td_Down As HtmlControls.HtmlGenericControl = CType(WNumEdTotLinArrows.FindControl(WNumEdTotLinArrows.ID & "_td_down"), HtmlControls.HtmlGenericControl)

                    If wNumEdTotLinArrows_Td_Up.Attributes("onmousedown").ToString.IndexOf("mostrarguardarcesta()") = -1 Then
                        wNumEdTotLinArrows_Td_Up.Attributes.Add("onmousedown", wNumEdTotLinArrows_Td_Up.Attributes("onmousedown").ToString & "mostrarguardarcesta('" & wNumEdTotLinArrows_Td_Up.ClientID & "')")
                    End If
                    If wNumEdTotLinArrows_Td_Down.Attributes("onmousedown").ToString.IndexOf("mostrarguardarcesta()") = -1 Then
                        wNumEdTotLinArrows_Td_Down.Attributes.Add("onmousedown", wNumEdTotLinArrows_Td_Down.Attributes("onmousedown").ToString & "mostrarguardarcesta('" & wNumEdTotLinArrows_Td_Down.ClientID & "')")
                    End If

                End If

                If wnumCant.Attributes("onchange") IsNot Nothing Then
                    wnumCant.Attributes.Add("onchange", wnumCant.Attributes("onchange").ToString & "mostrarguardarcesta('" & wnumCant.ClientID & "');")
                Else
                    wnumCant.Attributes.Add("onchange", "mostrarguardarcesta('" & wnumCant.ClientID & "')")
                End If
                If wNumEdPrec.Attributes("onchange") IsNot Nothing Then
                    wNumEdPrec.Attributes.Add("onchange", wNumEdPrec.Attributes("onchange").ToString & "mostrarguardarcesta();")
                Else
                    wNumEdPrec.Attributes.Add("onchange", "mostrarguardarcesta()")
                End If

            End With
        Next
    End Sub
    ''' <summary>
    ''' Método que se ejecuta antes del render del datalist dlCEsta para insertar el script construido previamiente y que
    ''' va a permitir que el control calendario wCal esté disponile para todos los controles wDateTimeEdit
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub dlCesta_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlCesta.PreRender
        'Vamos a introducir un script en el javascript del cliente siempre que se recargue la cesta.
        'El javascript se construye en el GridLineas_DataBound->ConfigurarCalendarioLineas


        Dim script As StringBuilder = New StringBuilder()
        script.Append("<script type='text/javascript'> ")
        script.Append("function pageLoad(sender,args) {")
        script.Append(_script.ToString)

        If Session("ancho") <> "" Then
            script.Append("PonerAncho('" + Session("ancho") + "','1')")
            Session("ancho") = Nothing
        End If

        script.Append("}</script>")
        Me.ClientScript.RegisterClientScriptBlock(Me.GetType(), "pageload", script.ToString())
    End Sub
#End Region
#Region "Panel confirmación"
    Private Sub Confirmar(ByVal Texto As String, ByVal AccionAceptar As String, ByVal AccionCancelar As String, ByVal ArgumentAceptar As String, ByVal ArgumentCancelar As String)
        imgConfirm.InnerHtml = "<img src='images/Icono_Error_Amarillo_40x40.gif'>"
        lblConfirm.Text = Texto
        txtConfirm.Style.Item("display") = "none"
        lblMaxLength.Style.Item("display") = "none"
        btnAceptarConfirm.CommandName = AccionAceptar
        btnAceptarConfirm.CommandArgument = ArgumentAceptar
        btnCancelarConfirm.Style.Item("display") = "block"
        btnCancelarConfirm.CommandName = AccionCancelar
        btnCancelarConfirm.CommandArgument = ArgumentCancelar
        mpeConfirm.Show()
        upConfirm.Update()
    End Sub
    ''' Revisado por: blp. Fecha: 17/07/2012
    ''' <summary>
    ''' Método que se ejecuta cuando se pulsa el botón aceptar (btnAceptarConfirm) del panel de confirmacion (pnlConfirm)
    ''' </summary>
    ''' <param name="sender">El botón</param>
    ''' <param name="e">argumentos del evento click</param>
    ''' <remarks>Llamada desde el propio botón. Max. 0,3 seg.</remarks>
    Private Sub btnAceptarConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarConfirm.Click
        Select Case CType(sender, FSNWebControls.FSNButton).CommandName

            Case "NoGuardarFavoritoDespuesDeEmitir"
                EliminarCestaBdd()
                Session("IDsLinea") = Nothing
                'Redirigir al catalogo
                Response.Redirect("CatalogoWeb.aspx")
        End Select
    End Sub
    ''' <summary>
    ''' Método que se ejecuta cuando se pulsa el botón Cancelar (btnCancelarConfirm) del panel de confirmacion (pnlConfirm)
    ''' </summary>
    ''' <param name="sender">El botón</param>
    ''' <param name="e">argumentos del evento click</param>
    ''' <remarks>Llamada desde el propio botón</remarks>
    Private Sub btnCancelarConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelarConfirm.Click
        Select Case CType(sender, FSNWebControls.FSNButton).CommandName
            Case "NoGuardarFavoritoYBuscar"
                HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaEP") & "CatalogoWeb.aspx?TextoBusquedaCatalogo=" & CType(sender, FSNWebControls.FSNButton).CommandArgument)
            Case "NoGuardarFavoritoEIrACatalogo"
                HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaEP") & "CatalogoWeb.aspx?FavId=" & CType(sender, FSNWebControls.FSNButton).CommandArgument)
            Case "NoGuardarFavoritoAntesDeEmitir"
                'Emitir()
        End Select
    End Sub
    ''' <summary>
    ''' Si el usuario está usando el webpart FSEPWebPartBusqArticulos1 (clase FSEPWebPartBusqArticulos) de la página Master CabCatalogo
    ''' Antes de llevarle al resultado de la búsqueda, comprobamos si tiene sin guardar los cambios de la página
    ''' Si no ha guardado le preguntamos si quiere guardar.
    ''' Aceptar-> Guardamos y mandamos a la busqueda
    ''' Cancelar-> No guardamos y mandamos a la búsqueda
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que contiene datos de evento.</param>
    ''' <remarks>Se produce cuando se hace clic en el control btnBuscarArt (botón Buscar del webpart de búsqueda de articulos)</remarks>
    Protected Overloads Sub btnBuscarArt_Click_DesdeWebPartBusquedaDeArticulos(ByVal sender As Object, ByVal e As CommandEventArgs)
        'Dim guardarVisible As Boolean = (BtnGuardarCesta.Visible AndAlso BtnGuardarCesta.Style("visibility") <> "hidden" AndAlso BtnGuardarCesta.Style("display") <> "none")

        HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaEP") & "CatalogoWeb.aspx?TextoBusquedaCatalogo=" & e.CommandArgument)

    End Sub
#End Region
#Region "Page methods"
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function Obtener_Info_LineaPedidoAbierto(ByVal idCesta As Integer) As String
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")

            Dim cCesta As FSNServer.CPedidoAbierto
            cCesta = FSNServer.Get_Object(GetType(FSNServer.CPedidoAbierto))

            Dim serializer As New JavaScriptSerializer
            Return serializer.Serialize(cCesta.Obtener_Info_LineaPedidoAbierto(FSNUser.CodPersona, idCesta))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
End Class