﻿Imports AjaxControlToolkit
Imports Fullstep.FSNLibrary.modUtilidades
Imports Fullstep.FSNServer

Partial Public Class CatalogoWeb
    Inherits FSEPPage

    Private ArticulosComparar As String()

    'PARA EL TRATAMIENTO DE FAVORITOS
    Private oOrdenFav As FSNServer.COrdenFavoritos
    Dim msTextoEliminar As String
    Private oArt As FSNServer.CArticulo
    Private miLineaActual As Integer
    Private sMsgCantidadMayorQueCero As String = ""
    Private oEmpresas As FSNServer.CEmpresas
    ''' <summary>
    ''' Propiedad que devuelve todos los proveedores que se encuentran en el Catálogo
    ''' </summary> 
    ''' <value>Un DataSet con los datos de todos los proveedores del catálogo</value>
    ''' <returns>Un DataSet con los datos de todos los proveedores del catálogo</returns>
    ''' <remarks>Llamada desde: Diferentes métodos de la página
    ''' Tiempo máximo: entre 0 seg y 0,4 seg</remarks>
    Public ReadOnly Property TodosProves() As DataSet
        Get
            Dim DsetProves As New DataSet
            If Cache("DsetProves_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) Is Nothing Then
                Dim oProves As FSNServer.CProveedores
                oProves = FSNServer.Get_Object(GetType(FSNServer.CProveedores))
                DsetProves = oProves.DevolverProveedoresFiltrados("", "", "", "", "", "", "", FSNUser.CodPersona, FSNUser.Idioma)
                If Not DsetProves Is Nothing Then Me.InsertarEnCache("DsetProves_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString, DsetProves)
                Return DsetProves
            Else
                Return Cache("DsetProves_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
            End If
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que devuelve los proveedores del catálogo con los filtros actuales
    ''' </summary>
    ''' <value>Un DataSet con los datos de los proveedores</value>
    ''' <returns>Un DataSet con los datos de los proveedores</returns>
    ''' <remarks>
    ''' Llamada desde: BtnBusqProves_Click y DevolverProveedoresFiltrados
    ''' Tiempo máximo: Entre 0 seg y 0,5 seg</remarks>
    Public ReadOnly Property _DsetProveedores() As DataSet
        Get
            If Not FiltrosAnyadidos.SequenceEqual(FiltrosAnyadidos_UsadosEnProveedores) Then
                borrarCacheDatosProveedores()
            End If
            If HttpContext.Current.Cache("DatosProveedoresFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) Is Nothing Then
                Dim DataSetDevolver As New DataSet

                Dim DataSetArticulosFiltrados As New DataView 'DataTable
                DataSetArticulosFiltrados = DevolverArticulosConFiltro(String.Empty, String.Empty, DsetArticulos.Tables(0), FiltrosAnyadidos)
                FiltrosAnyadidos_UsadosEnProveedores.Clear()
                FiltrosAnyadidos_UsadosEnProveedores.AddRange(FiltrosAnyadidos)

                DataSetDevolver = devolverDatosProveedoresFiltrados(DataSetArticulosFiltrados, TodosProves)

                If Not DataSetDevolver Is Nothing Then Me.InsertarEnCache("DatosProveedoresFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString, DataSetDevolver)

                Return DataSetDevolver
            Else
                Return HttpContext.Current.Cache("DatosProveedoresFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
            End If
        End Get
    End Property
    ''' <summary>
    ''' Función que construye y devuelve el listado de Proveedores del catálogo con los filtros actuales
    ''' </summary>
    ''' <param name="DataSetArticulosFiltrados">Tabla de artículos desde la que se recuperan los datos de Proveedores</param>
    ''' <param name="TodosProves">Proveedores del usuario</param>
    ''' <returns>Una lista de categorias</returns>
    ''' <remarks>Llamada desde CatalogoWeb.aspx.vb->_DsetProveedores y desde Consultas.asmx.vb->PreCargaEnCacheDeArticulosyFiltros. Tiempo máx inferior 1 seg</remarks>
    Public Function devolverDatosProveedoresFiltrados(ByVal DataSetArticulosFiltrados As DataView, ByVal TodosProves As DataSet) As DataSet
        Dim DataSetDevolver As New DataSet
        Dim CodProves As String()
        Dim ContProves As Integer = 0
        ReDim Preserve CodProves(0)
        If Not DataSetArticulosFiltrados Is Nothing AndAlso DataSetArticulosFiltrados.Count > 0 Then

            Dim dtProveedoresArticulos As New DataTable
            dtProveedoresArticulos.Columns.Add("codProve", GetType(String))
            dtProveedoresArticulos.Columns.Add("Cont", GetType(Integer))

            If DataSetArticulosFiltrados.RowFilter = String.Empty Then
                Dim ProveedoresArticulos = From articulos In DataSetArticulosFiltrados.Table
                                           Group By codigoProve = articulos("Prove") Into codigosProveedores = Group
                                           Select dtProveedoresArticulos.LoadDataRow(New Object() {codigoProve, codigosProveedores.Count()}, False)
                ProveedoresArticulos.Count()
            Else
                Dim ProveedoresArticulos = From articulos In DataSetArticulosFiltrados.Table.Select(DataSetArticulosFiltrados.RowFilter)
                                           Group By codigoProve = articulos("Prove") Into codigosProveedores = Group
                                           Select dtProveedoresArticulos.LoadDataRow(New Object() {codigoProve, codigosProveedores.Count()}, False)
                ProveedoresArticulos.Count()
            End If

            If dtProveedoresArticulos.Rows.Count > 0 Then
                Dim dtDatosProveedores As New DataTable
                dtDatosProveedores.Columns.Add("COD", GetType(String))
                dtDatosProveedores.Columns.Add("DEN", GetType(String))
                dtDatosProveedores.Columns.Add("PAI", GetType(String))
                dtDatosProveedores.Columns.Add("PROVI", GetType(String))
                dtDatosProveedores.Columns.Add("POB", GetType(String))
                dtDatosProveedores.Columns.Add("CP", GetType(String))
                dtDatosProveedores.Columns.Add("DIR", GetType(String))
                dtDatosProveedores.Columns.Add("NIF", GetType(String))
                dtDatosProveedores.Columns.Add("MON", GetType(String))
                dtDatosProveedores.Columns.Add("PROVIDEN", GetType(String))
                dtDatosProveedores.Columns.Add("PAIDEN", GetType(String))
                dtDatosProveedores.Columns.Add("Cont", GetType(Integer))

                Dim DatosProveedores As IEnumerable(Of DataRow) = From ProveFilt In (From proveTodos In TodosProves.Tables(0).AsEnumerable
                                                                                     Join proveArt In dtProveedoresArticulos.AsEnumerable On proveArt.Item("codProve") Equals proveTodos.Item("cod")
                                                                                     Select New With {.COD = proveTodos.Field(Of String)("COD"), .DEN = proveTodos.Field(Of String)("DEN"),
                                                                                     .PAI = proveTodos.Field(Of String)("PAI"), .PROVI = proveTodos.Field(Of String)("PROVI"),
                                                                                     .POB = proveTodos.Field(Of String)("POB"), .CP = proveTodos.Field(Of String)("CP"),
                                                                                     .DIR = proveTodos.Field(Of String)("DIR"), .NIF = proveTodos.Field(Of String)("NIF"),
                                                                                     .MON = proveTodos.Field(Of String)("MON"), .PROVIDEN = proveTodos.Field(Of String)("PROVIDEN"),
                                                                                     .PAIDEN = proveTodos.Field(Of String)("PAIDEN"), .Cont = proveArt.Field(Of Integer)("Cont")}) Distinct
                                                                  Select dtDatosProveedores.LoadDataRow(New Object() {ProveFilt.COD, ProveFilt.DEN, ProveFilt.PAI, ProveFilt.PROVI, ProveFilt.POB,
                                                                  ProveFilt.CP, ProveFilt.DIR, ProveFilt.NIF, ProveFilt.MON, ProveFilt.PROVIDEN,
                                                                  ProveFilt.PAIDEN, ProveFilt.Cont}, False)

                If DatosProveedores.Count > 0 Then
                    DataSetDevolver.Tables.Add(dtDatosProveedores)
                End If
            End If
        End If
        Return DataSetDevolver
    End Function
    ''' <summary>
    ''' Función que construye y devuelve el listado de Proveedores del catálogo con los filtros actuales
    ''' </summary>
    ''' <param name="DataSetArticulosFiltrados">Tabla de artículos desde la que se recuperan los datos de Proveedores</param>
    ''' <param name="TodosProves">Proveedores del usuario</param>
    ''' <returns>Una lista de categorias</returns>
    ''' <remarks>Llamada desde CatalogoWeb.aspx.vb->_DsetProveedores y desde Consultas.asmx.vb->PreCargaEnCacheDeArticulosyFiltros. Tiempo máx inferior 1 seg</remarks>
    Public Function devolverDatosProveedoresFiltrados(ByVal DataSetArticulosFiltrados As DataTable, ByVal TodosProves As DataSet) As DataSet
        Dim DataSetDevolver As New DataSet
        Dim CodProves As String()
        Dim ContProves As Integer = 0
        ReDim Preserve CodProves(0)
        If Not DataSetArticulosFiltrados Is Nothing AndAlso DataSetArticulosFiltrados.Rows.Count > 0 Then

            Dim dtProveedoresArticulos As New DataTable
            dtProveedoresArticulos.Columns.Add("codProve", GetType(String))
            dtProveedoresArticulos.Columns.Add("Cont", GetType(Integer))

            Dim ProveedoresArticulos = From articulos In DataSetArticulosFiltrados
                                       Group By codigoProve = articulos.Field(Of String)("Prove") Into codigosProveedores = Group
                                       Select dtProveedoresArticulos.LoadDataRow(New Object() {codigoProve, codigosProveedores.Count()}, False)

            If ProveedoresArticulos.Count > 0 Then
                Dim dtDatosProveedores As New DataTable
                dtDatosProveedores.Columns.Add("COD", GetType(String))
                dtDatosProveedores.Columns.Add("DEN", GetType(String))
                dtDatosProveedores.Columns.Add("PAI", GetType(String))
                dtDatosProveedores.Columns.Add("PROVI", GetType(String))
                dtDatosProveedores.Columns.Add("POB", GetType(String))
                dtDatosProveedores.Columns.Add("CP", GetType(String))
                dtDatosProveedores.Columns.Add("DIR", GetType(String))
                dtDatosProveedores.Columns.Add("NIF", GetType(String))
                dtDatosProveedores.Columns.Add("MON", GetType(String))
                dtDatosProveedores.Columns.Add("PROVIDEN", GetType(String))
                dtDatosProveedores.Columns.Add("PAIDEN", GetType(String))
                dtDatosProveedores.Columns.Add("Cont", GetType(Integer))

                Dim DatosProveedores As IEnumerable(Of DataRow) = From ProveFilt In (From proveTodos In TodosProves.Tables(0).AsEnumerable
                                                                                     Join proveArt In dtProveedoresArticulos.AsEnumerable On proveArt.Item("codProve") Equals proveTodos.Item("cod")
                                                                                     Select New With {.COD = proveTodos.Field(Of String)("COD"), .DEN = proveTodos.Field(Of String)("DEN"),
                                                                                     .PAI = proveTodos.Field(Of String)("PAI"), .PROVI = proveTodos.Field(Of String)("PROVI"),
                                                                                     .POB = proveTodos.Field(Of String)("POB"), .CP = proveTodos.Field(Of String)("CP"),
                                                                                     .DIR = proveTodos.Field(Of String)("DIR"), .NIF = proveTodos.Field(Of String)("NIF"),
                                                                                     .MON = proveTodos.Field(Of String)("MON"), .PROVIDEN = proveTodos.Field(Of String)("PROVIDEN"),
                                                                                     .PAIDEN = proveTodos.Field(Of String)("PAIDEN"), .Cont = proveArt.Field(Of Integer)("Cont")}) Distinct
                                                                  Select dtDatosProveedores.LoadDataRow(New Object() {ProveFilt.COD, ProveFilt.DEN, ProveFilt.PAI, ProveFilt.PROVI, ProveFilt.POB,
                                                                  ProveFilt.CP, ProveFilt.DIR, ProveFilt.NIF, ProveFilt.MON, ProveFilt.PROVIDEN,
                                                                  ProveFilt.PAIDEN, ProveFilt.Cont}, False)

                If DatosProveedores.Count > 0 Then
                    DataSetDevolver.Tables.Add(dtDatosProveedores)
                End If
            End If
        End If
        Return DataSetDevolver
    End Function
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'Añadimos el jsLimiteDecimales.js al script compuesto que controla el scriptmanager para una carga más rápida de los scripts
        Dim scriptRef As New System.Web.UI.ScriptReference("~/js/jsLimiteDecimales.js")
        ScriptMgr.CompositeScript.Scripts.Add(scriptRef)

        scriptRef = New System.Web.UI.ScriptReference("~/js/jsTextboxFormat.js")
        ScriptMgr.CompositeScript.Scripts.Add(scriptRef)

        ScriptMgr.LoadScriptsBeforeUI = False
    End Sub
    ''' <summary>
    ''' Procedimiento de Carga de la pagina, se cargan todas las variables globales que luego se van a utilizar en esta parte de la aplicación, tales como el dataset que contendra todos los articulos a los que el usuario tiene acceso, el arbol de categorias, los textos de idiomas, y los valores de ordenacion, y visibilidad de filtros que el usuario hubiese hecho uso en alguna sesion anterior si es el caso.
    ''' </summary>
    ''' <param name="sender">la pagina</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde la pagina
    ''' Tiempo maximo 3,4 sec sec</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim TextoBusqueda As String = Nothing
        'Antes de cargar nada comprobamos si tiene permiso de ver los articulos (catalogo). Puede llegar a esta pantalla mediante el webpart de busqueda de articulos
        'teniendo permiso de emitir pedidos contra pedidos abiertos. En caso de no tener permiso de ver articulos del catalogo redirigimos directamente a la pagina
        'de pedido abierto filtrando la busqueda a los articulos con la denominacion introducida
        If Not FSNUser.EPTipo = TipoAccesoFSEP.SoloAprovisionador AndAlso Not FSNUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador AndAlso FSNUser.EPPermitirEmitirPedidosDesdePedidoAbierto Then 'no tiene permiso de catalogo pero si de pedido abierto
            HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaEP") & "PedidoAbierto.aspx?Art=" & Request.QueryString("TextoBusquedaCatalogo"), True)
        End If

        'Aqui cargo el módulo de idioma
        ModuloIdioma = FSNLibrary.TiposDeDatos.ModulosIdiomas.EP_CatalogoWeb

        CargarVariables()

        'Aqui marco como seleccionada la Cabecera de artículos
        Master.CabTabArticulos.Selected = True

        ScriptMgr.EnablePageMethods = True

        ' Revisar: variable Shared
        If Not Page.IsPostBack Then
            GridArticulos.Columns(1).Visible = (Me.FSNUser.MostrarImagenArt)

            Dim Cookie As HttpCookie
            Cookie = Request.Cookies("CRIT_ORD")
            If Cookie Is Nothing OrElse String.IsNullOrEmpty(Cookie.Value) Then
                If Me.FSNUser.MostrarCodArt Then
                    CampoOrden = "COD_ITEM"
                Else
                    CampoOrden = "ART_DEN"
                End If
            Else
                CampoOrden = Cookie.Value
                Cookie.Expires = DateTime.Now.AddDays(30)
                Response.AppendCookie(Cookie)
            End If
            Cookie = Request.Cookies("CRIT_DIREC")
            If Cookie Is Nothing OrElse String.IsNullOrEmpty(Cookie.Value) Then
                SentidoOrdenacion = "ASC"
            Else
                SentidoOrdenacion = Cookie.Value
                Cookie.Expires = DateTime.Now.AddDays(30)
                Response.AppendCookie(Cookie)
            End If

            If Request.QueryString("TextoBusquedaCatalogo") IsNot Nothing Then TextoBusqueda = Request.QueryString("TextoBusquedaCatalogo")
            'Compruebo si hay algun proveedor filtrado
            'BLP
            'Primero comprobamos si hay algo en FiltrosAnyadidos 
            'Si no hay nada, miramos en la cookie
            'porque si no, igual estamos añadiendo morralla a esa propiedad
            If FiltrosAnyadidos Is Nothing OrElse FiltrosAnyadidos.Where(Function(x) x.ProveBusqueda = True).Count = 0 Then
                If Not Request.Cookies("FiltrosProveedor") Is Nothing Then
                    Dim MiCookie As HttpCookie = Request.Cookies("FiltrosProveedor")
                    Dim oFiltro As Filtro

                    If Not MiCookie.Item("Cod") Is Nothing AndAlso Not String.IsNullOrEmpty(MiCookie.Item("Cod")) Then
                        oFiltro = New Filtro
                        oFiltro.ProveBusqueda = True
                        oFiltro.tipo = TipoFiltro.Proveedor
                        oFiltro.CodBusquedaProve = MiCookie.Item("Cod")
                        oFiltro.Codigo = "cod"
                        ' Falta texto-->oFiltro.Nombre=Textos()
                        oFiltro.Nombre = "Codigo:"
                        oFiltro.Denominacion = oFiltro.CodBusquedaProve
                        FiltrosAnyadidos.Add(oFiltro)
                    End If
                    If Not MiCookie.Item("Den") Is Nothing AndAlso Not String.IsNullOrEmpty(MiCookie.Item("Den")) Then
                        oFiltro = New Filtro
                        oFiltro.ProveBusqueda = True
                        oFiltro.tipo = TipoFiltro.Proveedor
                        oFiltro.CodBusquedaProve = MiCookie.Item("Den")
                        oFiltro.Codigo = "den"
                        ' Falta texto-->oFiltro.Nombre=Textos()
                        oFiltro.Nombre = "Denominación:"
                        oFiltro.Denominacion = oFiltro.CodBusquedaProve
                        FiltrosAnyadidos.Add(oFiltro)
                    End If
                    If Not MiCookie.Item("Pai") Is Nothing AndAlso Not String.IsNullOrEmpty(MiCookie.Item("Pai")) Then
                        oFiltro = New Filtro
                        oFiltro.ProveBusqueda = True
                        oFiltro.tipo = TipoFiltro.Proveedor
                        oFiltro.CodBusquedaProve = MiCookie.Item("Pai")
                        oFiltro.Codigo = "pai"
                        ' Falta texto-->oFiltro.Nombre=Textos()
                        oFiltro.Nombre = "País:"
                        For Each fila As DataRow In TodosProves.Tables(0).Rows
                            If DBNullToStr(fila.Item("Pai")) = oFiltro.CodBusquedaProve Then
                                oFiltro.Denominacion = DBNullToStr(fila.Item("PaiDen"))
                                Exit For
                            End If
                        Next
                        FiltrosAnyadidos.Add(oFiltro)
                    End If
                    If Not MiCookie.Item("Provi") Is Nothing AndAlso Not String.IsNullOrEmpty(MiCookie.Item("Provi")) Then
                        oFiltro = New Filtro
                        oFiltro.ProveBusqueda = True
                        oFiltro.tipo = TipoFiltro.Proveedor
                        oFiltro.CodBusquedaProve = MiCookie.Item("Provi")
                        oFiltro.Codigo = "provi"
                        ' Falta texto-->oFiltro.Nombre=Textos()
                        oFiltro.Nombre = "Povincia:"
                        For Each fila As DataRow In TodosProves.Tables(0).Rows
                            If DBNullToStr(fila.Item("Provi")) = oFiltro.CodBusquedaProve Then
                                oFiltro.Denominacion = DBNullToStr(fila.Item("ProviDen"))
                                Exit For
                            End If
                        Next
                        FiltrosAnyadidos.Add(oFiltro)
                    End If
                    If Not MiCookie.Item("Pob") Is Nothing AndAlso Not String.IsNullOrEmpty(MiCookie.Item("Pob")) Then
                        oFiltro = New Filtro
                        oFiltro.ProveBusqueda = True
                        oFiltro.tipo = TipoFiltro.Proveedor
                        oFiltro.CodBusquedaProve = MiCookie.Item("Pob")
                        oFiltro.Codigo = "pob"
                        ' Falta texto-->oFiltro.Nombre=Textos()
                        oFiltro.Nombre = "Población:"
                        oFiltro.Denominacion = oFiltro.CodBusquedaProve
                        FiltrosAnyadidos.Add(oFiltro)
                    End If
                    If Not MiCookie.Item("Cif") Is Nothing AndAlso Not String.IsNullOrEmpty(MiCookie.Item("Cif")) Then
                        oFiltro = New Filtro
                        oFiltro.ProveBusqueda = True
                        oFiltro.tipo = TipoFiltro.Proveedor
                        oFiltro.CodBusquedaProve = MiCookie.Item("Cif")
                        oFiltro.Codigo = "cif"
                        ' Falta texto-->oFiltro.Nombre=Textos()
                        oFiltro.Nombre = "CIF:"
                        oFiltro.Denominacion = oFiltro.CodBusquedaProve
                        FiltrosAnyadidos.Add(oFiltro)
                    End If
                    If Not MiCookie.Item("Cp") Is Nothing AndAlso Not String.IsNullOrEmpty(MiCookie.Item("Cp")) Then
                        oFiltro = New Filtro
                        oFiltro.ProveBusqueda = True
                        oFiltro.tipo = TipoFiltro.Proveedor
                        oFiltro.CodBusquedaProve = MiCookie.Item("Cp")
                        oFiltro.Codigo = "cp"
                        ' Falta texto-->oFiltro.Nombre=Textos()
                        oFiltro.Nombre = "C.P:"
                        oFiltro.Denominacion = oFiltro.CodBusquedaProve
                        FiltrosAnyadidos.Add(oFiltro)
                    End If
                    FilaFiltroProve.Visible = True
                    GridArticulos.PageIndex = 0
                    'GridArticulos.DataBind()
                    updFiltros.Update()
                    UpdatePanelGrid.Update()

                    'En el caso de que hay filtro de proveedor, el listado de proveedores guardado en la precarga en caché (en consultas.asmx.vb)
                    'no es válido, por lo que borramos el contenido en caché, para que vuelva a generarse
                    If Not HttpContext.Current.Cache("ProveedoresFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) Is Nothing Then
                        HttpContext.Current.Cache.Remove("ProveedoresFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
                    End If
                End If
            End If

            'Aqui compruebo si ha habido búsqueda
            If TextoBusqueda IsNot Nothing Then BusquedaCabecera(TextoBusqueda)

            BtnCancelarFav.OnClientClick = "$find('" & mpepanSelecFavoritos.ClientID & "').hide(); return false"
            BtnAceptarFav.OnClientClick = "$find('" & mpepanSelecFavoritos.ClientID & "').hide();"
            BtnSeguirEnCatalogo.OnClientClick = "$find('" & mpepanConfirmacionFavoritoAnyadido.ClientID & "').hide(); return false"
            BtnIrAFavorito.OnClientClick = "$find('" & mpepanConfirmacionFavoritoAnyadido.ClientID & "').hide();"

            CargarTextos()
        Else
            CargarVariablesFavoritos()
        End If
        'Rellenamos el GridArticulos
        Dim bActualizarListados As Boolean = True
        'Carga del listado de artículos (GridArticulos)
        '   Definimos cuándo se tiene que efectuar la carga (que por motivos de rendimiento debe efectuarse sólo cuando sea imprescindible):
        '       -Cuando el postback no es asíncrono, o sea con cada postback normal
        '       -Cuando, siendo asíncrono el postback, se cumpla que lo ha efectuado cualquier control que implica un cambio en el listado de artículos, o sea
        '       cualquier control que efectúe un filtro, que cambie de página, ordene, etc
        If Not IsPostBack AndAlso Not ScriptMgr.IsInAsyncPostBack Then
            Dim bCargarGridArticulos As Boolean
            Dim bCargarSolicitudes As Boolean
            If TextoBusqueda Is Nothing Then
                'Caso de primera carga
                bCargarGridArticulos = IIf(FiltrosAnyadidos.Count = 0, Not Acceso.gbEPDesactivarCargaArticulos, True)
                bCargarSolicitudes = False
            Else
                'Caso de carga despuÃ©s de pulsar el botÃ³n Buscar de la bÃºsqueda de artÃ­culos
                bCargarGridArticulos = (Not String.IsNullOrEmpty(TextoBusqueda))
                bCargarSolicitudes = Acceso.gbEPActivarCargaSolPM
            End If
            'Si se trata de la primera carga de la pÃ¡gina se carga el grid de artÃ­culos sÃ³lo si durante la sesiÃ³n el usuario ha aplicado filtros o si el param. gbEPDesactivarCargaArticulos estÃ¡ a False
            RellenarGridArticulos(bActualizarListados, bCargarGridArticulos, bCargarSolicitudes)
        ElseIf Not ScriptMgr.IsInAsyncPostBack _
        OrElse (ScriptMgr.IsInAsyncPostBack _
                AndAlso (ScriptMgr.AsyncPostBackSourceElementID = String.Empty _
                        OrElse (ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$GridArticulos") > 0 _
                        AndAlso ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$ImgBtnArt") < 0 _
                        AndAlso ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$LnkCodArtOc") < 0 _
                        AndAlso ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$LnkNomArt") < 0 _
                        AndAlso ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$ImgFavoritos") < 0) _
                        OrElse (ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$lstCategorias") > 0) _
                        OrElse (ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$lstProv") > 0) _
                        OrElse (ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$DlFiltrosProve") > 0) _
                        OrElse (ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$lstDestinos") > 0) _
                        OrElse (ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$lstFiltros") > 0) _
                        OrElse (ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$fsnTvwCategorias") > 0) _
                        OrElse (ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$lstDestPopup") > 0))) Then
            RellenarGridArticulos(bActualizarListados, , Acceso.gbEPActivarCargaSolPM)
        ElseIf ScriptMgr.IsInAsyncPostBack AndAlso
                        (ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$ImgBtnArt") > 0 _
                        OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$LnkCodArtOc") > 0 _
                        OrElse ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$LnkNomArt") > 0) Then
            'MOSTRAR PANEL ARTICULOS
            'Capturamos aquí mismo el momento en que se lanza el evento para abrir el panel de artículos.
            'El motivo es que, como no recargamos el gridarticulos, no se lanzaría el evento RowCommand del GridArticulos en el que se lanza la misma orden que aquí:cargar el panel de detalle de articulo y mostrarlo
            Dim oSender As Page = TryCast(sender, Page)
            If oSender IsNot Nothing Then
                Dim idArticulo As Integer = strToInt(oSender.Request(IDPostBack.UniqueID))
                If idArticulo <> 0 Then
                    pnlDetalleArticulo.CargarDetalleArticulo(DsetArticulos, idArticulo.ToString)
                End If
            End If
        ElseIf ScriptMgr.IsInAsyncPostBack AndAlso ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$ImgFavoritos") > 0 Then
            'AÑADIR A FAVORITO
            Dim oSender As Page = TryCast(sender, Page)
            If oSender IsNot Nothing Then
                Dim sINFO As String = oSender.Request(IDPostBack.UniqueID)
                Dim sArINFO() As String = Split(sINFO, "###")
                If sArINFO.Length > 1 Then
                    Dim idArticulo As Integer = strToInt(sArINFO(0))
                    Dim sMON As String = sArINFO(1)
                    Dim Cantidad As Double = strToDbl(sArINFO(2))
                    Dim sDen As String = sArINFO(3)
                    CargarFavoritos(idArticulo, Cantidad, sMON, sDen)
                End If
            End If
        ElseIf ScriptMgr.IsInAsyncPostBack AndAlso ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$lnkProvList") > 0 Then
            'FILTRAR POR PROVEEDOR
            borrarCacheCategorias()
            borrarCacheProveedores()
            borrarCacheDestinos()
            Dim oFiltro As New Filtro
            oFiltro.tipo = TipoFiltro.Proveedor

            Dim oSender As Page = TryCast(sender, Page)
            If oSender IsNot Nothing Then
                Dim sINFO As String = oSender.Request(IDPostBack.UniqueID)
                Dim sArINFO() As String = Split(sINFO, "###")
                If sArINFO.Length > 1 Then
                    Dim sCodigo As String = sArINFO(0)
                    Dim sNombre As String = sArINFO(1)
                    oFiltro.Nombre = sNombre
                    oFiltro.Codigo = sCodigo
                    If Not FiltrosAnyadidos.Contains(oFiltro) Then
                        FiltrosAnyadidos.Add(oFiltro)
                        GridArticulos.PageIndex = 0
                        'GridArticulos.DataBind() 'No hacemos databind porque se va a hacer inmediatamente después en el RellenarGridArticulos
                        bActualizarListados = True
                        RellenarGridArticulos(bActualizarListados, , Acceso.gbEPActivarCargaSolPM)
                        UpdatePanelGrid.Update()
                        lstFiltros.DataSource = FiltrosAnyadidos.Where(Function(filtro) filtro.ProveBusqueda = False)
                        lstFiltros.DataBind()
                        updFiltros.Update()
                    End If
                    FilaFiltroProve.Visible = False
                End If
            End If
        ElseIf ScriptMgr.IsInAsyncPostBack AndAlso
                        ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$lnkVerTodos") > 0 Then
            If HttpContext.Current.Cache("ProveedoresFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) Is Nothing Then
                Dim dvArticulosConFiltro As DataView = DevolverArticulosConFiltro(CampoOrden, SentidoOrdenacion)
                rellenarListados(dvArticulosConFiltro)
            Else
                Dim dvArticulosConFiltroVacio As New DataView
                Dim dtListaProveedores As DataTable = ListaProveedores(dvArticulosConFiltroVacio)
                rellenarListados(dvArticulosConFiltroVacio)
            End If
        End If
    End Sub
    ''' <summary>
    ''' Procedimiento que carga los textos de los controles de la página
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: El page_load
    ''' Tiempo máximo: 0 seg</remarks>
    Private Sub CargarTextos()
        LblTitProvesBusq.Text = Textos(143)
        ImgBtnCerrarBusqProves.ToolTip = Textos(132)
        lblCodProveBusq.Text = Textos(75)
        LblDenProveBusq.Text = Textos(144)
        LblCifProveBusq.Text = Textos(145)
        lblCpProveBusq.Text = Textos(146)
        LblPaiProveBusq.Text = Textos(147)
        LblProviProveBusq.Text = Textos(148)
        lblPobProveBusq.Text = Textos(149)
        BtnBuscarProve.Text = Textos(6)
        BtnBuscarProve.ToolTip = Textos(6)
        LblFiltAv.Text = Textos(103)
        LnkBtnFiltAv.ToolTip = Textos(120)
        LblLisDestFil.Text = Textos(124)
        LblCatalogoArbol.Text = Textos(122)
        LblFiltro.Text = Textos(105)
        LnkBtnCategoria.Text = Textos(97)
        LnkBtnProves.Text = Textos(99)
        LnkBtnDests.Text = Textos(100)
        cpe2.CollapsedText = Textos(118)
        cpe2.ExpandedText = Textos(119)
        LblAnyadirFav.Text = Textos(95)
        lblSelFavProveedor.Text = Textos(13) & ":"
        sMsgCantidadMayorQueCero = Textos(157)

        lblCabeceraAltaSolicitudes.Text = Textos(167)
        lblFooter.Text = Textos(168)
        lblAplicarFiltro.Text = Textos(173)
        lblSolicitudes.Text = Textos(174)
        lblEmitir.Text = Textos(175) & ":"
        lblLnkSolicitudes.Text = Textos(176)
    End Sub
    ''' Revisado por: blp. Fecha: 08/11/2012
    ''' <summary>
    ''' Procedimiento que carga las variables globales del ViewState, o cache en su caso, o de parámetros del usuario, para poder ser utilizado en la función o funciones que se necesite
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde:Page_Load
    ''' Tiempo máximo:0 seg</remarks>
    Private Sub CargarVariables()
        If Not Session("ArticulosComparar") Is Nothing Then
            ArticulosComparar = Session("ArticulosComparar")
        End If

        aceTbCodProveBusq.ContextKey = FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString
        'Añadimos una función de javascript al evento de mostrar el panel de autocomplete para corregir un bug del control AjaxControlToolkit.AutoCompleteExtender, 
        'por el que en FF y Chrome se añade un margin al control por el tamaño de letra definido para el control contenedor, en este caso el body, de modo que el desplegable no se encuentra pegado al textbox del que depende.
        'Esta función se asigna al evento OnClientShowing del control en EPWebControls\FSEPWebPartBusqArticulos.vb
        aceTbCodProveBusq.OnClientShowing = "quitarMarginTop_ProveBusq"
        aceTbDenProveBusq.ContextKey = FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString
        aceTbDenProveBusq.OnClientShowing = "quitarMarginTop_ProveBusq"

        'Añadimos la propiedad a todos los autocompleteextender de la página, incluido el del filtro de artículos que hay en el paginador
        Dim sScript As String = "function quitarMarginTop_ProveBusq() { " & vbCrLf
        sScript += "$get('" & aceTbCodProveBusq.ClientID & "_completionListElem').style.marginTop = '0px';" & vbCrLf
        sScript += "$get('" & aceTbDenProveBusq.ClientID & "_completionListElem').style.marginTop = '0px';" & vbCrLf
        sScript += "};"
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered("quitarMarginTop_ProveBusq") Then _
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "quitarMarginTop_ProveBusq", sScript, True)

        If Not ArticulosComparar Is Nothing Then
            If ArticulosComparar.Count > 0 Then
                Master.CabTabComparativa.Text = Textos(89) & "(" & ArticulosComparar.Count & ")"
                Master.CabTabComparativa.Enabled = True
            End If
        Else
            Master.CabTabComparativa.Enabled = False
            Master.CabTabComparativa.Text = Textos(89)

        End If
        Master.CabUpdPestanyasCatalogo.Update()

        If Not HttpContext.Current.Cache("FiltrosAnyadidos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) Is Nothing Then
            FiltrosAnyadidos = HttpContext.Current.Cache("FiltrosAnyadidos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
        Else
            FiltrosAnyadidos = New List(Of Filtro)
        End If

        CargarArbolCategorias()

        CargarPopupDestinos()

    End Sub
#Region "Consultas"
    ''' <summary>
    ''' Función que devuelve los artículos filtrados, con los filtros ya realizados
    ''' </summary>
    ''' <param name="OrdenacionCampo">El campo por el que se ordena</param>
    ''' <param name="OrdenacionSentido">ASC o DESC indicando si es ascendente o descendente</param>
    ''' <param name="tablaArticulos">La tabla que se debe filtrar</param>
    ''' <param name="Filtros">Los filtros que se deben aplicar</param>
    ''' <param name="Proveedor">Si se trata de busqueda de proveedores o no</param>
    ''' <returns>Un DataSet con los datos de los artículos aplicados el filtro</returns>
    ''' <remarks>
    ''' Llamada desde: ObjectDataSource_Selecting
    ''' Tiempo máximo: Entre 0 seg y 1 seg</remarks>
    Public Shared Function DevolverArticulosConFiltro(ByVal OrdenacionCampo As String, ByVal OrdenacionSentido As String, ByVal tablaArticulos As DataTable, ByVal Filtros As List(Of Filtro), ByVal Proveedor As Boolean) As DataTable
        Dim dtArt As DataTable = tablaArticulos.Copy()
        Dim DtIntermedia As New DataTable
        Dim ColumnaImportar As DataColumn
        Dim oConsulta As String = ""
        Dim CantProves As Integer = 0
        Dim query As EnumerableRowCollection(Of DataRow)
        For Each columna As DataColumn In dtArt.Columns
            ColumnaImportar = New DataColumn(columna.ColumnName)
            DtIntermedia.Columns.Add(ColumnaImportar)
        Next
        For Each oFiltroProve As Filtro In Filtros
            Dim x As Filtro = oFiltroProve
            For Each fila As DataRow In dtArt.Rows
                If fila.Item("Prove") = x.Codigo Then
                    DtIntermedia.ImportRow(fila)
                End If
            Next
        Next
        If OrdenacionCampo <> String.Empty AndAlso OrdenacionSentido <> String.Empty Then
            If Not DtIntermedia Is Nothing AndAlso DtIntermedia.Rows.Count > 1 Then
                If OrdenacionCampo.IndexOf("*") >= 0 Then
                    DtIntermedia.Columns.Add("ORDENACION", Type.GetType("System.Double"), OrdenacionCampo)
                Else
                    DtIntermedia.Columns.Add("ORDENACION", Type.GetType("System.String"), OrdenacionCampo)
                End If
                If OrdenacionSentido = "ASC" Then
                    query = From Datos In DtIntermedia.AsEnumerable()
                            Order By DBNullToSomething(Datos.Item("ORDENACION")) Ascending
                            Select Datos
                Else
                    query = From Datos In DtIntermedia.AsEnumerable()
                            Order By DBNullToSomething(Datos.Item("ORDENACION")) Descending
                            Select Datos
                End If
                DtIntermedia = query.CopyToDataTable()
            End If
        End If
        Return DtIntermedia
    End Function
    ''' Revisado por: blp. Fecha: 31/01/2012
    ''' <summary>
    ''' Función que devuelve los artículos filtrados, con los filtros ya realizados
    ''' </summary>
    ''' <param name="OrdenacionCampo">El campo por el que se ordena</param>
    ''' <param name="OrdenacionSentido">ASC o DESC indicando si es ascendente o descendente</param>
    ''' <param name="tablaArticulos">La tabla que se debe filtrar</param>
    ''' <param name="Filtros">Los filtros que se deben aplicar</param>
    ''' <returns>Un DataSet con los datos de los artículos aplicados el filtro</returns>
    ''' <remarks>
    ''' Llamada desde: ObjectDataSource_Selecting y DevolverArticulosConFiltro
    ''' Tiempo máximo: Entre 0 seg y 1 seg</remarks>
    Public Shared Function DevolverArticulosConFiltro(ByVal OrdenacionCampo As String, ByVal OrdenacionSentido As String, ByVal tablaArticulos As DataTable, ByVal Filtros As List(Of Filtro)) As DataView 'As DataTable
        Try
            If OrdenacionCampo <> String.Empty AndAlso OrdenacionSentido <> String.Empty Then
                Dim insertarOrdenacion As Boolean = False
                If tablaArticulos.Columns("ORDENACION") IsNot Nothing Then
                    If tablaArticulos.Columns("ORDENACION").Expression <> OrdenacionCampo Then
                        tablaArticulos.Columns.Remove("ORDENACION")
                        insertarOrdenacion = True
                    End If
                Else
                    insertarOrdenacion = True
                End If
                If insertarOrdenacion Then
                    If OrdenacionCampo.IndexOf("*") >= 0 Then
                        tablaArticulos.Columns.Add("ORDENACION", Type.GetType("System.Double"), OrdenacionCampo)
                    Else
                        tablaArticulos.Columns.Add("ORDENACION", Type.GetType("System.String"), OrdenacionCampo)
                    End If
                End If
            End If
            Dim queryStr As String = String.Empty
            Dim addAND As Boolean = False
            Dim sAND As String = " AND "

            Dim dvTablaArticulos As New DataView
            If Not tablaArticulos Is Nothing Then
                dvTablaArticulos.Table = tablaArticulos
                dvTablaArticulos.RowStateFilter = DataViewRowState.CurrentRows
            End If

            If Not tablaArticulos Is Nothing AndAlso tablaArticulos.Rows.Count > 1 Then
                For Each ofiltro As Filtro In Filtros
                    Dim x As Filtro = ofiltro
                    Select Case x.tipo
                        Case TipoFiltro.Categoria
                            Dim Cats As String() = Split(x.Codigo, "_")
                            Select Case UBound(Cats)
                                Case 0
                                    If Not queryStr.Contains("CAT1 = '" & Cats(0) & "'") Then
                                        queryStr = queryStr & IIf(addAND, sAND, String.Empty) & "CAT1 = '" & Cats(0) & "'"
                                        addAND = True
                                    End If
                                Case 1
                                    If Not queryStr.Contains("CAT1 = '" & Cats(0) & "'") Then
                                        queryStr = queryStr & IIf(addAND, sAND, String.Empty) & "CAT1 = '" & Cats(0) & "'"
                                        addAND = True
                                    End If
                                    If Not queryStr.Contains("CAT2 = '" & Cats(1) & "'") Then
                                        queryStr = queryStr & sAND & "CAT2 = '" & Cats(1) & "'"
                                        addAND = True
                                    End If
                                Case 2
                                    If Not queryStr.Contains("CAT1 = '" & Cats(0) & "'") Then
                                        queryStr = queryStr & IIf(addAND, sAND, String.Empty) & "CAT1 = '" & Cats(0) & "'"
                                        addAND = True
                                    End If
                                    If Not queryStr.Contains("CAT2 = '" & Cats(1) & "'") Then
                                        queryStr = queryStr & sAND & "CAT2 = '" & Cats(1) & "'"
                                        addAND = True
                                    End If
                                    If Not queryStr.Contains("CAT3 = '" & Cats(2) & "'") Then
                                        queryStr = queryStr & sAND & "CAT3 = '" & Cats(2) & "'"
                                        addAND = True
                                    End If
                                Case 3
                                    If Not queryStr.Contains("CAT1 = '" & Cats(0) & "'") Then
                                        queryStr = queryStr & IIf(addAND, sAND, String.Empty) & "CAT1 = '" & Cats(0) & "'"
                                        addAND = True
                                    End If
                                    If Not queryStr.Contains("CAT2 = '" & Cats(1) & "'") Then
                                        queryStr = queryStr & sAND & "CAT2 = '" & Cats(1) & "'"
                                        addAND = True
                                    End If
                                    If Not queryStr.Contains("CAT3 = '" & Cats(2) & "'") Then
                                        queryStr = queryStr & sAND & "CAT3 = '" & Cats(2) & "'"
                                        addAND = True
                                    End If
                                    If Not queryStr.Contains("CAT4 = '" & Cats(3) & "'") Then
                                        queryStr = queryStr & sAND & "CAT4 = '" & Cats(3) & "'"
                                        addAND = True
                                    End If
                                Case Else
                                    If Not queryStr.Contains("CAT1 = '" & Cats(0) & "'") Then
                                        queryStr = queryStr & IIf(addAND, sAND, String.Empty) & "CAT1 = '" & Cats(0) & "'"
                                        addAND = True
                                    End If
                                    If Not queryStr.Contains("CAT2 = '" & Cats(1) & "'") Then
                                        queryStr = queryStr & sAND & "CAT2 = '" & Cats(1) & "'"
                                        addAND = True
                                    End If
                                    If Not queryStr.Contains("CAT3 = '" & Cats(2) & "'") Then
                                        queryStr = queryStr & sAND & "CAT3 = '" & Cats(2) & "'"
                                        addAND = True
                                    End If
                                    If Not queryStr.Contains("CAT4 = '" & Cats(3) & "'") Then
                                        queryStr = queryStr & sAND & "CAT4 = '" & Cats(3) & "'"
                                        addAND = True
                                    End If
                                    If Not queryStr.Contains("CAT5 = '" & Cats(4) & "'") Then
                                        queryStr = queryStr & sAND & "CAT5 = '" & Cats(4) & "'"
                                        addAND = True
                                    End If
                            End Select
                        Case TipoFiltro.Proveedor
                            If x.ProveBusqueda Then
                                Select Case x.Codigo.ToLower
                                    Case "cod"
                                        queryStr = queryStr & IIf(addAND, sAND, String.Empty) & "PROVE LIKE '%" & x.CodBusquedaProve & "%'"
                                        addAND = True
                                    Case "den"
                                        queryStr = queryStr & IIf(addAND, sAND, String.Empty) & "PROVEDEN LIKE '%" & x.CodBusquedaProve & "%'"
                                        addAND = True
                                    Case "cif"
                                        queryStr = queryStr & IIf(addAND, sAND, String.Empty) & "CifProve LIKE '%" & x.CodBusquedaProve & "%'"
                                        addAND = True
                                    Case "pai"
                                        queryStr = queryStr & IIf(addAND, sAND, String.Empty) & "PaiProve = '" & x.CodBusquedaProve & "'"
                                        addAND = True
                                    Case "provi"
                                        queryStr = queryStr & IIf(addAND, sAND, String.Empty) & "ProviProve = '" & x.CodBusquedaProve & "'"
                                        addAND = True
                                    Case "pob"
                                        queryStr = queryStr & IIf(addAND, sAND, String.Empty) & "PobProve = '" & x.CodBusquedaProve & "'"
                                        addAND = True
                                    Case "cp"
                                        queryStr = queryStr & IIf(addAND, sAND, String.Empty) & "CPProve = '" & x.CodBusquedaProve & "'"
                                        addAND = True
                                End Select
                            Else
                                queryStr = queryStr & IIf(addAND, sAND, String.Empty) & "PROVE = '" & x.Codigo & "'"
                                addAND = True
                            End If
                        Case TipoFiltro.Destino
                            queryStr = queryStr & IIf(addAND, sAND, String.Empty) & "DEST = '" & x.Codigo & "'"
                            addAND = True
                        Case Else
                            'Si llega 126*4 busca like '%126*4%'. Con el cambio busca like '%126%' and like '%4%'
                            Dim Aux As String = Replace(Replace(x.Codigo, "*", " "), "%", " ")
                            Dim arrayPalabras As String() = Split(Aux)
                            For Each palabra As String In arrayPalabras
                                queryStr = queryStr & IIf(addAND, sAND, String.Empty) & "(COD_ITEM LIKE '%" & palabra & "%' OR COD_EXT LIKE '%" & palabra & "' OR ART_DEN LIKE '%" & palabra & "%' or ESP LIKE '%" & palabra & "%')"
                                addAND = True
                            Next
                    End Select
                Next
                Dim orden As String = Nothing
                If OrdenacionCampo <> String.Empty AndAlso OrdenacionSentido <> String.Empty Then
                    'Al no usar linq para ordenar (como hasta ahora) sino el sort de un dataview, lo mejor sería no crear la columna ORDENACION (tiempo ganado) pero uno de los campos de la ordenación es calculado (PRECIO = PRECIO por UNIDAD * FC), por lo que no queda más remedio que usar un campo ordenación
                    orden = "ORDENACION " & OrdenacionSentido
                End If
                dvTablaArticulos.RowFilter = queryStr
                dvTablaArticulos.Sort = orden
            End If
            Return dvTablaArticulos
        Catch ex As Exception
            If tablaArticulos IsNot Nothing Then
                Return tablaArticulos.DefaultView
            Else
                Dim dvTablaArticulos As New DataView
                Return dvTablaArticulos
            End If
        End Try
    End Function
    ''' <summary>
    ''' Función que devuelve los artículos filtrados, con los filtros ya realizados
    ''' </summary>
    ''' <param name="OrdenacionCampo">El campo por el que se ordena</param>
    ''' <param name="OrdenacionSentido">ASC o DESC indicando si es ascendente o descendente</param>
    ''' <returns>Un DataSet con los datos de los artículos aplicados el filtro</returns>
    ''' <remarks>
    ''' Llamada desde: ObjectDataSource_Selecting
    ''' Tiempo máximo: Entre 0 seg y 1 seg</remarks>
    Public Function DevolverArticulosConFiltro(ByVal OrdenacionCampo As String, ByVal OrdenacionSentido As String) As DataView 'As DataTable
        Return DevolverArticulosConFiltro(OrdenacionCampo, OrdenacionSentido, DsetArticulos.Tables(0), FiltrosAnyadidos)
    End Function
    ''' <summary>
    ''' Función que devuelve la lista de categorías a poner en los filtros una vez se han filtrado los artículos
    ''' </summary>
    ''' <param name="TablaFiltrada">La tabla de donde se sacarán las categorías filtradas</param>
    ''' <returns>La lista de categorías que se escribirán en los filtros</returns>
    ''' <remarks>
    ''' Llamada desde: GridArticulos_DataBinding 'odsArticulos_Select
    ''' Tiempo máximo: 0 seg</remarks>
    Private Function ListaCategorias(ByVal TablaFiltrada As DataView) As List(Of ListItem)
        If HttpContext.Current.Cache("CategoriasFiltradas_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) Is Nothing Then
            Dim Lista As New List(Of ListItem)
            Lista = devolverListaCategorias(TablaFiltrada)
            Me.InsertarEnCache("CategoriasFiltradas_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString, Lista)
            If Lista.Count = 0 Then
                Return Nothing
            Else
                'guardarCacheFiltrosAnyadidos(FiltrosAnyadidos)
                Return Lista
            End If
        Else
            Return HttpContext.Current.Cache("CategoriasFiltradas_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
        End If
    End Function
    ''' <summary>
    ''' Función que construye y devuelve el listado de Categorias
    ''' </summary>
    ''' <param name="TablaFiltrada">Tabla de artículos desde la que se recuperan los datos de Categorias</param>
    ''' <returns>Una lista de categorias</returns>
    ''' <remarks>Llamada desde CatalogoWeb.aspx.vb->ListaCategorias y desde Consultas.asmx.vb->PreCargaEnCacheDeArticulosyFiltros. Tiempo máx inferior 0,1 seg</remarks>
    Public Function devolverListaCategorias(ByVal TablaFiltrada As DataView) As List(Of ListItem)
        If TablaFiltrada Is Nothing OrElse TablaFiltrada.Count < 2 Then
            Dim ListaVacia As New List(Of ListItem)
            Return ListaVacia
        End If

        Dim dtTablaFiltrada As New DataTable
        dtTablaFiltrada.Columns.Add("CAT1", GetType(String))
        dtTablaFiltrada.Columns.Add("CAT2", GetType(String))
        dtTablaFiltrada.Columns.Add("CAT3", GetType(String))
        dtTablaFiltrada.Columns.Add("CAT4", GetType(String))
        dtTablaFiltrada.Columns.Add("CAT5", GetType(String))
        dtTablaFiltrada.Columns.Add("Total", GetType(Double))

        If TablaFiltrada.RowFilter = String.Empty Then
            Dim querydtTablaFiltrada = From Datos In TablaFiltrada.Table
                                       Group Datos By CAT1 = Datos.Item("CAT1"), CAT2 = Datos.Item("CAT2"), CAT3 = Datos.Item("CAT3"), CAT4 = Datos.Item("CAT4"), CAT5 = Datos.Item("CAT5") Into g = Group
                                       Select dtTablaFiltrada.LoadDataRow(New Object() {CAT1, CAT2, CAT3, CAT4, CAT5, g.Count()}, False)
            querydtTablaFiltrada.Count()
        Else
            Dim querydtTablaFiltrada = From Datos In TablaFiltrada.Table.Select(TablaFiltrada.RowFilter)
                                       Group Datos By CAT1 = Datos.Item("CAT1"), CAT2 = Datos.Item("CAT2"), CAT3 = Datos.Item("CAT3"), CAT4 = Datos.Item("CAT4"), CAT5 = Datos.Item("CAT5") Into g = Group
                                       Select dtTablaFiltrada.LoadDataRow(New Object() {CAT1, CAT2, CAT3, CAT4, CAT5, g.Count()}, False)
            querydtTablaFiltrada.Count()
        End If

        If dtTablaFiltrada.Rows.Count > 0 Then
            Dim query = From Datos In dtTablaFiltrada
                        Join Texto In TodasCategorias.Tables(0) On Texto.Item("ID") Equals Datos("CAT1").ToString()
                        Group Datos By cat = Texto.Item("ID"), den = Texto.Item("DEN") Into g = Group
                        Select New With {.Cat = cat, .Den = den, .Cantidad = g.Count()}
                        Take (2)
            Dim queryRdo
            If query.Count < 2 Then
                query = From Datos In dtTablaFiltrada
                        Join Texto In TodasCategorias.Tables(0) On Texto.Item("ID") Equals Datos("CAT1").ToString() & "_" & Datos("CAT2").ToString()
                        Group Datos By cat = Texto.Item("ID"), den = Texto.Item("DEN") Into g = Group
                        Select New With {.Cat = cat, .Den = den, .Cantidad = g.Count()}
                        Take (2)
                If query.Count < 2 Then
                    query = From Datos In dtTablaFiltrada
                            Join Texto In TodasCategorias.Tables(0) On Texto.Item("ID") Equals Datos("CAT1").ToString() & "_" & Datos("CAT2").ToString() & "_" & Datos("CAT3").ToString()
                            Group Datos By cat = Texto.Item("ID"), den = Texto.Item("DEN") Into g = Group
                            Select New With {.Cat = cat, .Den = den, .Cantidad = g.Count()}
                            Take (2)
                    If query.Count < 2 Then
                        query = From Datos In dtTablaFiltrada
                                Join Texto In TodasCategorias.Tables(0) On Texto.Item("ID") Equals Datos("CAT1").ToString() & "_" & Datos("CAT2").ToString() & "_" & Datos("CAT3").ToString() & "_" & Datos("CAT4").ToString
                                Group Datos By cat = Texto.Item("ID"), den = Texto.Item("DEN") Into g = Group
                                Select New With {.Cat = cat, .Den = den, .Cantidad = g.Count()}
                                Take (2)

                        If query.Count < 2 Then
                            query = From Datos In dtTablaFiltrada
                                    Join Texto In TodasCategorias.Tables(0) On Texto.Item("ID") Equals Datos("CAT1").ToString() & "_" & Datos("CAT2").ToString() & "_" & Datos("CAT3").ToString() & "_" & Datos("CAT4").ToString & "_" & Datos("CAT5").ToString
                                    Group Datos By cat = Texto.Item("ID"), den = Texto.Item("DEN") Into g = Group
                                    Select New With {.Cat = cat, .Den = den, .Cantidad = g.Count()}
                                    Take (2)
                            If query.Count < 2 Then
                                Dim ListaVacia As New List(Of ListItem)
                                Return ListaVacia
                            Else
                                queryRdo = From Datos In dtTablaFiltrada
                                           Join Texto In TodasCategorias.Tables(0) On Texto.Item("ID") Equals Datos("CAT1").ToString() & "_" & Datos("CAT2").ToString() & "_" & Datos("CAT3").ToString() & "_" & Datos("CAT4").ToString & "_" & Datos("CAT5").ToString
                                           Group Datos By cat = Texto.Item("ID"), den = Texto.Item("DEN") Into g = Sum(CDbl(Datos("Total")))
                                           Order By g Descending
                                           Select New With {.Cat = cat, .Den = den, .Cantidad = g}
                            End If
                        Else
                            queryRdo = From Datos In dtTablaFiltrada
                                       Join Texto In TodasCategorias.Tables(0) On Texto.Item("ID") Equals Datos("CAT1").ToString() & "_" & Datos("CAT2").ToString() & "_" & Datos("CAT3").ToString() & "_" & Datos("CAT4").ToString
                                       Group Datos By cat = Texto.Item("ID"), den = Texto.Item("DEN") Into g = Sum(CDbl(Datos("Total")))
                                       Order By g Descending
                                       Select New With {.Cat = cat, .Den = den, .Cantidad = g}
                        End If
                    Else
                        queryRdo = From Datos In dtTablaFiltrada
                                   Join Texto In TodasCategorias.Tables(0) On Texto.Item("ID") Equals Datos("CAT1").ToString() & "_" & Datos("CAT2").ToString() & "_" & Datos("CAT3").ToString()
                                   Group Datos By cat = Texto.Item("ID"), den = Texto.Item("DEN") Into g = Sum(CDbl(Datos("Total")))
                                   Order By g Descending
                                   Select New With {.Cat = cat, .Den = den, .Cantidad = g}
                    End If
                Else
                    queryRdo = From Datos In dtTablaFiltrada
                               Join Texto In TodasCategorias.Tables(0) On Texto.Item("ID") Equals Datos("CAT1").ToString() & "_" & Datos("CAT2").ToString()
                               Group Datos By cat = Texto.Item("ID"), den = Texto.Item("DEN") Into g = Sum(CDbl(Datos("Total")))
                               Order By g Descending
                               Select New With {.Cat = cat, .Den = den, .Cantidad = g}
                End If
            Else
                queryRdo = From Datos In dtTablaFiltrada
                           Join Texto In TodasCategorias.Tables(0) On Texto.Item("ID") Equals Datos("CAT1").ToString
                           Group Datos By cat = Texto.Item("ID"), den = Texto.Item("DEN") Into g = Sum(CDbl(Datos("Total")))
                           Order By g Descending
                           Select New With {.Cat = cat, .Den = den, .Cantidad = g}
            End If

            Dim Lista As New List(Of ListItem)
            For Each x In queryRdo
                Lista.Add(New ListItem(x.Den & " (" & x.Cantidad & ")", x.Cat))
            Next
            Return Lista
        Else
            Dim ListaVacia As New List(Of ListItem)
            Return ListaVacia
        End If
    End Function
    ''' <summary>
    ''' Función que construye y devuelve el listado de Categorias
    ''' </summary>
    ''' <param name="TablaFiltrada">Tabla de artículos desde la que se recuperan los datos de Categorias</param>
    ''' <returns>Una lista de categorias</returns>
    ''' <remarks>Llamada desde CatalogoWeb.aspx.vb->ListaCategorias y desde Consultas.asmx.vb->PreCargaEnCacheDeArticulosyFiltros. Tiempo máx inferior 0,1 seg</remarks>
    Public Function devolverListaCategorias(ByVal TablaFiltrada As DataTable) As List(Of ListItem)
        If TablaFiltrada Is Nothing OrElse TablaFiltrada.Rows.Count < 2 Then
            Dim ListaVacia As New List(Of ListItem)
            Return ListaVacia
        End If
        Dim query = From Datos In TablaFiltrada
                    Join Texto In TodasCategorias.Tables(0) On Texto.Item("ID") Equals Datos.Item("CAT1").ToString()
                    Group Datos By cat = Texto.Item("ID"), den = Texto.Item("DEN") Into g = Group
                    Order By g.Count() Descending
                    Select New With {.Cat = cat, .Den = den, .Cantidad = g.Count()}
        If query.Count < 2 Then
            query = From Datos In TablaFiltrada
                    Join Texto In TodasCategorias.Tables(0) On Texto.Item("ID") Equals Datos.Item("CAT1").ToString() & "_" & Datos.Item("CAT2").ToString()
                    Group Datos By cat = Texto.Item("ID"), den = Texto.Item("DEN") Into g = Group
                    Order By g.Count() Descending
                    Select New With {.Cat = cat, .Den = den, .Cantidad = g.Count()}
            If query.Count < 2 Then
                query = From Datos In TablaFiltrada
                        Join Texto In TodasCategorias.Tables(0) On Texto.Item("ID") Equals Datos.Item("CAT1").ToString() & "_" & Datos.Item("CAT2").ToString() & "_" & Datos.Item("CAT3").ToString()
                        Group Datos By cat = Texto.Item("ID"), den = Texto.Item("DEN") Into g = Group
                        Order By g.Count() Descending
                        Select New With {.Cat = cat, .Den = den, .Cantidad = g.Count()}
                If query.Count < 2 Then
                    query = From Datos In TablaFiltrada
                            Join Texto In TodasCategorias.Tables(0) On Texto.Item("ID") Equals Datos.Item("CAT1").ToString() & "_" & Datos.Item("CAT2").ToString() & "_" & Datos.Item("CAT3").ToString() & "_" & Datos.Item("CAT4")
                            Group Datos By cat = Texto.Item("ID"), den = Texto.Item("DEN") Into g = Group
                            Order By g.Count() Descending
                            Select New With {.Cat = cat, .Den = den, .Cantidad = g.Count()}
                    If query.Count < 2 Then
                        query = From Datos In TablaFiltrada
                                Join Texto In TodasCategorias.Tables(0) On Texto.Item("ID") Equals Datos.Item("CAT1").ToString() & "_" & Datos.Item("CAT2").ToString() & "_" & Datos.Item("CAT3").ToString() & "_" & Datos.Item("CAT4") & "_" & Datos.Item("CAT5")
                                Group Datos By cat = Texto.Item("ID"), den = Texto.Item("DEN") Into g = Group
                                Order By g.Count() Descending
                                Select New With {.Cat = cat, .Den = den, .Cantidad = g.Count()}
                        If query.Count < 2 Then
                            Dim ListaVacia As New List(Of ListItem)
                            Return ListaVacia
                        End If
                    End If
                End If
            End If
        End If
        Dim Lista As New List(Of ListItem)
        For Each x In query
            Lista.Add(New ListItem(x.Den & " (" & x.Cantidad & ")", x.Cat))
        Next
        Return Lista
    End Function
    ''' <summary>
    ''' Función que devuelve la lista de proveedores a poner en los filtros una vez se han filtrado los artículos
    ''' </summary>
    ''' <param name="TablaFiltrada">La tabla de donde se sacarán los proveedores filtrados</param>
    ''' <returns>La lista de proveedores que se escribirán en los filtros</returns>
    ''' <remarks>
    ''' Llamada desde: GridArticulos_DataBinding 'odsArticulos_Select
    ''' Tiempo máximo: 0 seg</remarks>
    Private Function ListaProveedores(ByVal TablaFiltrada As DataView) As DataTable
        If HttpContext.Current.Cache("ProveedoresFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) Is Nothing Then
            Dim dtProveedoresFiltrados As DataTable
            dtProveedoresFiltrados = devolverListaProveedores(TablaFiltrada)
            Me.InsertarEnCache("ProveedoresFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString, dtProveedoresFiltrados)
            If dtProveedoresFiltrados.Rows.Count = 0 Then
                Return Nothing
            Else
                Return dtProveedoresFiltrados
            End If
        Else
            Return HttpContext.Current.Cache("ProveedoresFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
        End If
    End Function
    ''' <summary>
    ''' Función que construye y devuelve el listado de Proveedores
    ''' </summary>
    ''' <param name="TablaFiltrada">Tabla de artículos desde la que se recuperan los datos de Proveedores</param>
    ''' <returns>Una lista de categorias</returns>
    ''' <remarks>Llamada desde CatalogoWeb.aspx.vb->ListaProveedores y desde Consultas.asmx.vb->PreCargaEnCacheDeArticulosyFiltros. Tiempo máx inferior 0,1 seg</remarks>
    Public Function devolverListaProveedores(ByVal TablaFiltrada As DataView) As DataTable
        If TablaFiltrada Is Nothing OrElse TablaFiltrada.Count < 2 Then
            Dim dtListaProveedoresVacio As New DataTable
            Return dtListaProveedoresVacio
        End If
        Dim dtListaProveedores As New DataTable
        dtListaProveedores.Columns.Add("PROVE", GetType(String))
        dtListaProveedores.Columns.Add("DEN", GetType(String))
        dtListaProveedores.Columns.Add("Cantidad", GetType(Integer))

        If TablaFiltrada.RowFilter = String.Empty Then
            Dim query = From Datos In TablaFiltrada.Table
                        Group Datos By prove = Datos.Item("PROVE"), den = Datos.Item("PROVEDEN") Into g = Group
                        Order By den Ascending
                        Select dtListaProveedores.LoadDataRow(New Object() {prove, den, g.Count()}, False)
            query.Count()
        Else
            Dim query = From Datos In TablaFiltrada.Table.Select(TablaFiltrada.RowFilter)
                        Group Datos By prove = Datos.Item("PROVE"), den = Datos.Item("PROVEDEN") Into g = Group
                        Order By den Ascending
                        Select dtListaProveedores.LoadDataRow(New Object() {prove, den, g.Count()}, False)
            query.Count()
        End If

        If dtListaProveedores.Rows.Count < 1 Then
            Dim dtListaProveedoresVacio As New DataTable
            Return dtListaProveedoresVacio
        Else
            Return dtListaProveedores
        End If
    End Function
    ''' <summary>
    ''' Función que construye y devuelve el listado de Proveedores
    ''' </summary>
    ''' <param name="TablaFiltrada">Tabla de artículos desde la que se recuperan los datos de Proveedores</param>
    ''' <returns>Una lista de categorias</returns>
    ''' <remarks>Llamada desde CatalogoWeb.aspx.vb->ListaProveedores y desde Consultas.asmx.vb->PreCargaEnCacheDeArticulosyFiltros. Tiempo máx inferior 0,1 seg</remarks>
    Public Function devolverListaProveedores(ByVal TablaFiltrada As DataTable) As DataTable
        If TablaFiltrada Is Nothing OrElse TablaFiltrada.Rows.Count < 2 Then
            Dim dtListaProveedoresVacio As New DataTable
            Return dtListaProveedoresVacio
        End If

        Dim dtListaProveedores As New DataTable
        dtListaProveedores.Columns.Add("PROVE", GetType(String))
        dtListaProveedores.Columns.Add("DEN", GetType(String))
        dtListaProveedores.Columns.Add("Cantidad", GetType(Integer))

        Dim query = From Datos In TablaFiltrada
                    Group Datos By prove = Datos.Item("PROVE"), den = Datos.Item("PROVEDEN") Into g = Group
                    Order By den Ascending
                    Select dtListaProveedores.LoadDataRow(New Object() {prove, den, g.Count()}, False)
        query.Count()

        If dtListaProveedores.Rows.Count < 2 Then
            Dim dtListaProveedoresVacio As New DataTable
            Return dtListaProveedoresVacio
        Else
            Return dtListaProveedores
        End If
    End Function
    ''' <summary>
    ''' Función que devuelve la lista de destinos a poner en los filtros una vez se han filtrado los artículos
    ''' </summary>
    ''' <param name="TablaFiltrada">La tabla de donde se sacarán los destino filtrados</param>
    ''' <returns>La lista de destinos que se escribirán en los filtros</returns>
    ''' <remarks>
    ''' Llamada desde: GridArticulos_DataBinding 'odsArticulos_Select
    ''' Tiempo máximo: 0 seg</remarks>
    Private Function ListaDestinos(ByVal TablaFiltrada As DataView) As List(Of ListItem)
        If HttpContext.Current.Cache("DestinosFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) Is Nothing Then
            Dim Lista As List(Of ListItem)
            Lista = devolverListaDestinos(TablaFiltrada, TodosDestinos)
            Me.InsertarEnCache("DestinosFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString, Lista)
            If Lista.Count = 0 Then
                Return Nothing
            Else
                Return Lista
            End If
        Else
            Return HttpContext.Current.Cache("DestinosFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
        End If
    End Function
    ''' <summary>
    ''' Función que construye y devuelve el listado de Destinos
    ''' </summary>
    ''' <param name="TablaFiltrada">Tabla de artículos desde la que se recuperan los datos de Proveedores</param>
    ''' <param name="TodosDestinos">Lista de todos los destinos del usuario que recupere la lista de destinos</param>
    ''' <returns>Una lista de categorias</returns>
    ''' <remarks>Llamada desde CatalogoWeb.aspx.vb->ListaDEstinos y desde Consultas.asmx.vb->PreCargaEnCacheDeArticulosyFiltros. Tiempo máx inferior 0,1 seg</remarks>
    Public Function devolverListaDestinos(ByVal TablaFiltrada As DataView, ByVal TodosDestinos As List(Of ListItem)) As List(Of ListItem)
        If TablaFiltrada Is Nothing OrElse TablaFiltrada.Count < 2 Then
            Dim ListaVacia As New List(Of ListItem)
            Return ListaVacia
        End If

        Dim dtquery As New DataTable
        dtquery.Columns.Add("Dest", GetType(String))
        dtquery.Columns.Add("Cantidad", GetType(Double))

        If TablaFiltrada.RowFilter = String.Empty Then
            Dim query = From Datos In TablaFiltrada.Table
                        Group By dest = Datos.Item("DEST") Into g = Group
                        Select dtquery.LoadDataRow(New Object() {dest, g.Count()}, False)
            query.Count()
        Else
            Dim query = From Datos In TablaFiltrada.Table.Select(TablaFiltrada.RowFilter)
                        Group By dest = Datos.Item("DEST") Into g = Group
                        Select dtquery.LoadDataRow(New Object() {dest, g.Count()}, False)
            query.Count()
        End If

        Dim query2 = From Datos In dtquery
                     Join Texto In TodosDestinos On Texto.Value Equals Datos("Dest")
                     Order By Datos("Cantidad") Descending
                     Select New With {.Dest = Texto.Value, .Den = Texto.Text, .Cantidad = Datos("Cantidad")}
        If query2.Count < 2 Then
            Dim ListaVacia As New List(Of ListItem)
            Return ListaVacia
        Else
            Dim Lista As New List(Of ListItem)
            For Each x In query2
                Lista.Add(New ListItem(x.Den & " (" & x.Cantidad & ")", x.Dest))
            Next
            Return Lista
        End If
    End Function
    ''' <summary>
    ''' Función que construye y devuelve el listado de Destinos
    ''' </summary>
    ''' <param name="TablaFiltrada">Tabla de artículos desde la que se recuperan los datos de Proveedores</param>
    ''' <param name="TodosDestinos">Lista de todos los destinos del usuario que recupere la lista de destinos</param>
    ''' <returns>Una lista de categorias</returns>
    ''' <remarks>Llamada desde CatalogoWeb.aspx.vb->ListaDEstinos y desde Consultas.asmx.vb->PreCargaEnCacheDeArticulosyFiltros. Tiempo máx inferior 0,1 seg</remarks>
    Public Function devolverListaDestinos(ByVal TablaFiltrada As DataTable, ByVal TodosDestinos As List(Of ListItem)) As List(Of ListItem)
        If TablaFiltrada Is Nothing OrElse TablaFiltrada.Rows.Count < 2 Then
            Dim ListaVacia As New List(Of ListItem)
            Return ListaVacia
        End If
        Dim query = From Datos In TablaFiltrada
                    Join Texto In TodosDestinos On Texto.Value Equals Datos.Item("DEST")
                    Group Datos By dest = Texto.Value, den = Texto.Text Into g = Group
                    Order By g.Count() Descending
                    Select New With {.Dest = dest, .Den = den, .Cantidad = g.Count()}
        If query.Count < 2 Then
            Dim ListaVacia As New List(Of ListItem)
            Return ListaVacia
        Else
            Dim Lista As New List(Of ListItem)
            For Each x In query
                Lista.Add(New ListItem(x.Den & " (" & x.Cantidad & ")", x.Dest))
            Next
            Return Lista
        End If
    End Function
    ''' <summary>
    ''' Función que autocompleta el textbox de busqueda de artículos con las coincidencias de código y denominación del mismo
    ''' </summary>
    ''' <param name="prefixText">El texto escrito por el usuario</param>
    ''' <param name="count">La cantidad de carácteres escritos</param>
    ''' <param name="contextKey">La clave para la busqueda en el DataSet, en este caso es el código de usuario</param>
    ''' <returns>Una lista con los textos coincidentes</returns>
    ''' <remarks>
    ''' Llamada desde: Automática, cuando se escriben 3 o más carácteres en el textbox
    ''' Tiempo máximo: 0,2 seg</remarks>
    <System.Web.Services.WebMethodAttribute(),
        System.Web.Script.Services.ScriptMethodAttribute()>
    Public Shared Function AutoCompletar(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        If HttpContext.Current.Cache("DsetArticulos_" & contextKey) Is Nothing Then Return Nothing
        Dim tablaArticulos As DataTable = CType(HttpContext.Current.Cache("DsetArticulos_" & contextKey), DataSet).Tables(0)
        Dim Filtros As List(Of Filtro)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        If HttpContext.Current.Cache("FiltrosAnyadidos_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString) IsNot Nothing Then
            Filtros = HttpContext.Current.Cache("FiltrosAnyadidos_" & oUsuario.CodPersona & "_" & oUsuario.Idioma.ToString)
        Else
            Filtros = New List(Of Filtro)
        End If

        Dim arrayPalabras As String() = Split(prefixText)
        Dim sbusqueda = From Datos In tablaArticulos
                        Let w = UCase(DBNullToStr(Datos.Item("COD_ITEM")) & " " & DBNullToStr(Datos.Item("COD_EXT")) & " " & DBNullToStr(Datos.Item("ART_DEN")) & " " & DBNullToStr(Datos.Item("ESP")))
                        Where arrayPalabras.All(Function(palabra) w Like "*" & strToSQLLIKE(palabra, True) & "*")
                        Select Datos.Item("ART_DEN") Distinct.Take(count).ToArray()
        Dim resul As String()
        ReDim resul(UBound(sbusqueda))
        For i As Integer = 0 To UBound(sbusqueda)
            resul(i) = sbusqueda(i).ToString()
        Next
        Return resul
    End Function
    ''' <summary>
    ''' Función que genera el DataSet con el pedido que se quiere modificar para utilizarlo en la página
    ''' </summary>
    ''' <param name="CodPersona">Código de la persona del usuario que esta utilizando la aplicación</param>
    ''' <param name="Idioma">Idioma de la aplicación</param>
    ''' <param name="Moneda">Moneda de la aplicación</param>
    ''' <param name="IdOrden">Identificador de la orden a filtrar</param>
    ''' <returns>Un DataSet con la orden de favoritos filtrada y sus líneas correspondientes</returns>
    ''' <remarks>
    ''' Llamada desde: La función GenerarDsetPedidos
    ''' Tiempo máximo:0,5 seg aprox.</remarks>
    Private Shared Function GenerarDsetPedido(ByVal CodPersona As String, ByVal Idioma As String, ByVal Moneda As String, ByVal IdOrden As Integer) As DataSet
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oOrdenesFavoritos As FSNServer.COrdenesFavoritos = oServer.Get_Object(GetType(FSNServer.COrdenesFavoritos))
        Return oOrdenesFavoritos.BuscarTodasOrdenes(CodPersona, IdOrden, "", Moneda, Idioma, True)
    End Function
#End Region
#Region "Ordenación"
    ''' <summary>
    ''' Propiedad que devuelve el valor del campo de ordenación del Grid de artículos
    ''' </summary>
    ''' <value>El valor del campo de ordenación</value>
    ''' <returns>Un string que podrá ser Prove, Den, Prec o Cod para ordenar los artículos</returns>
    ''' <remarks>
    ''' Llamada desde: Page_Load 
    ''' Tiempo máximo: 0 seg</remarks>
    Private Property CampoOrden() As String
        Get
            If String.IsNullOrEmpty(ViewState.Item("CampoOrden")) Then
                If Me.FSNUser.MostrarCodArt Then Return "COD_ITEM" Else Return "ART_DEN"
            Else
                Return ViewState.Item("CampoOrden")
            End If
        End Get
        Set(ByVal value As String)
            ViewState.Item("CampoOrden") = value
        End Set
    End Property
    ''' <summary>
    ''' Propiedad que devuelve el valor del sentido de ordenación del Grid de artículos
    ''' </summary>
    ''' <value>El valor del sentido de ordenación</value>
    ''' <returns>Un string que podrá ser ASC o DESC para ordenar los artículos por sentido Ascendente o Descendente</returns>
    ''' <remarks>
    ''' Llamada desde: Page_Load 
    ''' Tiempo máximo: 0 seg</remarks>
    Private Property SentidoOrdenacion() As String
        Get
            If String.IsNullOrEmpty(ViewState.Item("SentidoOrdenacion")) Then
                Return "ASC"
            Else
                Return ViewState.Item("SentidoOrdenacion")
            End If
        End Get
        Set(ByVal value As String)
            ViewState.Item("SentidoOrdenacion") = value
        End Set
    End Property
    ''' <summary>
    ''' Evento que se genera cuando se cambia la lista de ordenación de artículos, reordenandolos por el campo elegido y guardando el valor
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento, en este caso la lista de ordenación de articulos</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automática, cada vez que se cambia el campo de ordenación
    ''' Tiempo máximo: 1 seg</remarks>
    Protected Sub ListaOrdenacionArt_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        '*******************************************************************************************************
        '*** Descripción: Procedimiento que se utiliza para cambiar el criterio de ordenación de articulos eligiendo uno de los items de la lista desplegable, añadiendo esa condición a las cookies para futuras sesiones.
        '*** Parámetros de entrada: El Item elegido de la lista, y el evento de haber seleccionado un índice distinto al que estaba
        '*** Llamada desde: La propia interfaz, al seleccionar un item distinto de la lista
        '*** Tiempo máximo: 0'5 sec.  aproximadamente
        '*******************************************************************************************************
        'Dim timeactual As Integer = Date.Now.Millisecond
        If CType(sender, System.Web.UI.WebControls.DropDownList).SelectedValue <> CampoOrden Then
            CampoOrden = CType(sender, System.Web.UI.WebControls.DropDownList).SelectedValue
            Dim cookie As HttpCookie = Request.Cookies("CRIT_ORD")
            If cookie Is Nothing Then
                cookie = New HttpCookie("CRIT_ORD", CampoOrden)
            Else
                cookie.Value = CampoOrden
            End If
            cookie.Expires = DateTime.Now.AddDays(30)
            Response.AppendCookie(cookie)
            GridArticulos.PageIndex = 0
            'GridArticulos.DataBind() 'No hacemos databind porque se va a hacer inmediatamente después en el RellenarGridArticulos
            Dim bActualizarListados As Boolean = False
            RellenarGridArticulos(bActualizarListados, , Acceso.gbEPActivarCargaSolPM)
            UpdatePanelGrid.Update()
        End If
        'Dim timefinal As Integer = Date.Now.Millisecond
        'Dim timetotal As Integer = timefinal - timeactual
    End Sub
    ''' <summary>
    ''' Evento que se genera cuando se cambia el sentido de la ordenación, aplicandolo al GridView y guardando el valor
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento, en este caso el LinkButton</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automática, cuando se pincha la imagen de la flecha para cambiar el sentido de la ordenación
    ''' Tiempo máximo: 1 seg</remarks>
    Protected Sub LnkBtnOrdenacion_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        '*** Descripción: Procedimiento que se ejecuta al clicar sobre la imagen con la flecha que indica la ordenación ascendente o descendente de los artículos mostrados en el GridView
        '*** Parámetros de entrada: El objeto imagen y el evento de clicado
        '*** Llamada desde: Cada vez que se cliquea la imagen de sentido de la ordenación.
        '*** Tiempo máximo: 0'5 sec. aproximadamente
        '*******************************************************************************************************
        'Dim timeactual As Date = Date.Now
        Dim btnOrd As LinkButton = CType(sender, LinkButton)
        If SentidoOrdenacion = "ASC" Then
            SentidoOrdenacion = "DESC"
        Else
            SentidoOrdenacion = "ASC"
        End If
        Dim cookie As HttpCookie = Request.Cookies("CRIT_DIREC")
        If cookie Is Nothing Then
            cookie = New HttpCookie("CRIT_DIREC", SentidoOrdenacion)
        Else
            cookie.Value = SentidoOrdenacion
        End If
        cookie.Expires = DateTime.Now.AddDays(30)
        Response.AppendCookie(cookie)
        GridArticulos.PageIndex = 0
        'GridArticulos.DataBind() 'No hacemos databind porque se va a hacer inmediatamente después en el RellenarGridArticulos
        Dim bActualizarListados As Boolean = False
        RellenarGridArticulos(bActualizarListados, , Acceso.gbEPActivarCargaSolPM)
        UpdatePanelGrid.Update()
        'Dim timefinal As Date = Date.Now
        'Dim timetotal As Double = (timefinal.Ticks - timeactual.Ticks) / 10000000
    End Sub
#End Region
#Region "Filtros"
    <Serializable()>
    Public Enum TipoFiltro As Integer
        Categoria = 0
        Texto = 1
        Proveedor = 2
        Destino = 3
    End Enum
    <Serializable()>
    Public Structure Filtro
        Dim _tipo As TipoFiltro
        Dim _nombre As String
        Dim _codigo As String
        Dim _Denominacion As String
        Dim _ProveBusqueda As Boolean
        Dim _CodBusquedaProve As String

        Public Property tipo() As TipoFiltro
            Get
                Return _tipo
            End Get
            Set(ByVal value As TipoFiltro)
                _tipo = value
            End Set
        End Property
        Public Property Nombre() As String
            Get
                Return _nombre
            End Get
            Set(ByVal value As String)
                _nombre = value
            End Set
        End Property
        Public Property Codigo() As String
            Get
                Return _codigo
            End Get
            Set(ByVal value As String)
                _codigo = value
            End Set
        End Property
        Public Property ProveBusqueda() As Boolean
            Get
                Return _ProveBusqueda
            End Get
            Set(ByVal value As Boolean)
                _ProveBusqueda = value
            End Set
        End Property
        Public Property Denominacion() As String
            Get
                Return _Denominacion
            End Get
            Set(ByVal value As String)
                _Denominacion = value
            End Set
        End Property
        Public Property CodBusquedaProve() As String
            Get
                Return _CodBusquedaProve
            End Get
            Set(ByVal value As String)
                _CodBusquedaProve = value
            End Set
        End Property
    End Structure
    ''' <summary>
    ''' Propiedad que devuelve los filtros que se han añadido hasta el momento
    ''' </summary>
    ''' <value>La lista de los filtros</value>
    ''' <returns>La lista de los filtros que ha ido añadiendo el usuario con sus busquedas y selecciones</returns>
    ''' <remarks>
    ''' Llamada desde: Page_Load, BtnProve_CLick, BtnBusqProve_Click, DlFiltros_ItemCommand
    ''' Tiempo máximo: 0 seg</remarks>
    Public Property FiltrosAnyadidos() As List(Of Filtro)
        Get
            If HttpContext.Current.Cache("FiltrosAnyadidos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) IsNot Nothing Then
                Return HttpContext.Current.Cache("FiltrosAnyadidos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
            Else
                Return New List(Of Filtro)
            End If
        End Get
        Set(ByVal value As List(Of Filtro))
            If value IsNot Nothing Then Me.InsertarEnCache("FiltrosAnyadidos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString, value)
        End Set
    End Property
    ''' <summary>
    ''' Propiedad que devuelve los filtros que se han usado por última vez para obtener el _DsetProveedores
    ''' </summary>
    ''' <value>La lista de los filtros</value>
    ''' <returns>La lista de los filtros que se usaron la última vez que se obtuvo el _DsetProveedores</returns>
    ''' <remarks>
    ''' Llamada desde: _DsetProveedores
    ''' Tiempo máximo: 0 seg</remarks>
    Public Property FiltrosAnyadidos_UsadosEnProveedores() As List(Of Filtro)
        Get
            If Session("FiltrosAnyadidos_UsadosEnProveedores") Is Nothing Then _
                Session("FiltrosAnyadidos_UsadosEnProveedores") = New List(Of Filtro)
            Return Session("FiltrosAnyadidos_UsadosEnProveedores")
        End Get
        Set(ByVal value As List(Of Filtro))
            Session("FiltrosAnyadidos_UsadosEnProveedores") = value
        End Set
    End Property
    ''' <summary>
    ''' Procedimiento utilizado para la busqueda de artículos mediante la escritura en la caja de texto destinado a ello.
    ''' </summary>
    ''' <param name="Fila">La fila del Grid</param>
    ''' <param name="sender">El objeto Button</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: El propio interfaz al hacer click sobre el boton buscar.
    ''' Tiempo máximo: 0'85 sec. aproximadamente </remarks>
    Private Sub BuscarArticulos(ByVal Fila As GridViewRow, ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        borrarCacheCategorias()
        borrarCacheProveedores()
        borrarCacheDestinos()

        Dim TextoCompleto As String = CType(Fila.FindControl("TextBoxBusqArt"), TextBox).Text
        If TextoCompleto <> "" Then
            Dim oFiltro As New Filtro()
            oFiltro.Nombre = TextoCompleto
            oFiltro.Codigo = TextoCompleto
            oFiltro.tipo = TipoFiltro.Texto
            If Not FiltrosAnyadidos.Contains(oFiltro) Then
                FiltrosAnyadidos.Add(oFiltro)
                GridArticulos.PageIndex = 0
                'GridArticulos.DataBind() 'No hacemos databind porque se va a hacer inmediatamente después en el RellenarGridArticulos
                Dim bActualizarListados As Boolean = True
                RellenarGridArticulos(bActualizarListados, , Acceso.gbEPActivarCargaSolPM)
                UpdatePanelGrid.Update()
            End If
        End If
    End Sub
    ''' <summary>
    '''  Procedimiento utilizado para la busqueda de artículos mediante la escritura en la caja de texto destinado a ello.
    ''' </summary>
    ''' <param name="TextoBusqueda">El texto de busqueda</param>
    ''' <remarks>
    ''' Llamada desde: Llamada desde: El propio interfaz al hacer click sobre el boton buscar.
    ''' Tiempo máximo: 0'85 sec. aproximadamente</remarks>
    Private Sub BusquedaCabecera(ByVal TextoBusqueda As String)
        borrarCacheCategorias()
        borrarCacheProveedores()
        borrarCacheDestinos()

        FiltrosAnyadidos.Clear()
        If Not String.IsNullOrEmpty(TextoBusqueda) Then
            Dim oFiltro As New Filtro()
            oFiltro.ProveBusqueda = False
            oFiltro.Nombre = TextoBusqueda
            oFiltro.Codigo = TextoBusqueda
            oFiltro.tipo = TipoFiltro.Texto
            FiltrosAnyadidos.Add(oFiltro)
        End If
    End Sub
    ''' <summary>
    ''' Evento que se genera cuando se pincha algun dato de la lista de categorias
    ''' </summary>
    ''' <param name="source">La propia lista de categorias</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: La propia interfaz, cuando se pincha algun elemento de la lista de categorias
    ''' Tiempo máximo: 0,5 seg</remarks>
    Private Sub lstCategorias_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles lstCategorias.ItemCommand
        borrarCacheCategorias()
        borrarCacheProveedores()
        borrarCacheDestinos()

        FiltrarCategoria(e.CommandArgument, TodasCategorias.Tables(0).Rows.Find(e.CommandArgument).Item("DEN"), False)
    End Sub
    ''' <summary>
    ''' Evento que se genera cuando se pincha algun elemento de las categorias de los artículos que se encuentran en el GridView
    ''' </summary>
    ''' <param name="source">El objeto DataList</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automático, cada vez que se clica en un objeto de la lista de categorias
    ''' Tiempo máximo: 0,5 seg</remarks>
    Public Sub lstCategoriasCat_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs)
        borrarCacheCategorias()
        borrarCacheProveedores()
        borrarCacheDestinos()

        FiltrarCategoria(e.CommandArgument, TodasCategorias.Tables(0).Rows.Find(e.CommandArgument).Item("DEN"), True)
    End Sub
    ''' <summary>
    ''' Procedimiento que filtra los artículos del GridView por la categoría elegida
    ''' </summary>
    ''' <param name="Codigo">Código de la categoría</param>
    ''' <param name="Nombre">Nombre de la categoría</param>
    ''' <param name="Vaciar">Si hay que vaciar el resto de filtros(que no sean de proveedor)</param>
    ''' <remarks>
    ''' Llamada desde: LstCategorias_ItemCommand, lstCategoriasCat_ItemCommand,tvwCategorias_SelectedNodeChanged
    ''' Tiempo máximo: 0,5 seg</remarks>
    Private Sub FiltrarCategoria(ByVal Codigo As String, ByVal Nombre As String, ByVal Vaciar As Boolean)
        borrarCacheCategorias()
        borrarCacheProveedores()
        borrarCacheDestinos()

        If Vaciar Then
            Dim FiltrosValidos As New List(Of Filtro)
            FiltrosValidos = FiltrosAnyadidos.Where(Function(x) x.ProveBusqueda = True).ToList
            FiltrosAnyadidos.Clear()
            FiltrosAnyadidos = FiltrosValidos
        End If
        Dim oFiltro As New Filtro
        oFiltro.tipo = TipoFiltro.Categoria
        oFiltro.Nombre = Nombre
        oFiltro.Codigo = Codigo
        If Not FiltrosAnyadidos.Contains(oFiltro) Then
            FiltrosAnyadidos.Add(oFiltro)
            GridArticulos.PageIndex = 0
            'GridArticulos.DataBind() 'No hacemos databind porque se va a hacer inmediatamente después en el RellenarGridArticulos
            Dim bActualizarListados As Boolean = True
            RellenarGridArticulos(bActualizarListados, , Acceso.gbEPActivarCargaSolPM)
            UpdatePanelGrid.Update()
        End If
        updFiltros.Update()
    End Sub
    ''' <summary>
    ''' Evento que se genera cuando se Selecciona un proveedor de la lista de destinos que se encuentran en el panel de filtros
    ''' </summary>
    ''' <param name="source">El propio DataList que genera el evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automática, siempre que se seleccione un elemento de la lista de destinos
    ''' Tiempo máximo: 0,5 seg</remarks>
    Private Sub lstDestinos_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles lstDestinos.ItemCommand
        'Cambian los filtros, cambiamos los filtros guardados en caché
        borrarCacheCategorias()
        borrarCacheProveedores()
        borrarCacheDestinos()

        Dim oFiltro As New Filtro
        oFiltro.tipo = TipoFiltro.Destino
        oFiltro.Nombre = TodosDestinos.Single(Function(x) x.Value = e.CommandArgument).Text
        oFiltro.Codigo = e.CommandArgument
        If Not FiltrosAnyadidos.Contains(oFiltro) Then
            FiltrosAnyadidos.Add(oFiltro)
            GridArticulos.PageIndex = 0
            'GridArticulos.DataBind() 'No hacemos databind porque se va a hacer inmediatamente después en el RellenarGridArticulos
            Dim bActualizarListados As Boolean = True
            RellenarGridArticulos(bActualizarListados, , Acceso.gbEPActivarCargaSolPM)
            UpdatePanelGrid.Update()
        End If
        updFiltros.Update()
    End Sub
    ''' <summary>
    ''' Evento que se genera cuando se Selecciona un proveedor de la lista de destinos que se encuentran en el panel de filtros
    ''' </summary>
    ''' <param name="source">El propio DataList que genera el evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automática, siempre que se seleccione un elemento de la lista de destinos(del popup)
    ''' Tiempo máximo: 0,5 seg</remarks>
    Private Sub lstDestPopup_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles lstDestPopup.ItemCommand
        borrarCacheCategorias()
        borrarCacheProveedores()
        borrarCacheDestinos()

        Dim FiltrosValidos As New List(Of Filtro)
        FiltrosValidos = FiltrosAnyadidos.Where(Function(x) x.ProveBusqueda = True).ToList
        FiltrosAnyadidos.Clear()
        FiltrosAnyadidos = FiltrosValidos
        Dim oFiltro As New Filtro()
        oFiltro.tipo = TipoFiltro.Destino
        oFiltro.Nombre = e.CommandName
        oFiltro.Codigo = e.CommandArgument
        oFiltro.ProveBusqueda = False
        FiltrosAnyadidos.Add(oFiltro)
        mpePanelDestFil.Hide()
        GridArticulos.PageIndex = 0
        'GridArticulos.DataBind() 'No hacemos databind porque se va a hacer inmediatamente después en el RellenarGridArticulos
        Dim bActualizarListados As Boolean = True
        RellenarGridArticulos(bActualizarListados, , Acceso.gbEPActivarCargaSolPM)
        UpdatePanelGrid.Update()
        updFiltros.Update()
    End Sub
    ''' <summary>
    ''' Evento que se genera cuando se clica en uno de los elementos del datalist de los filtros
    ''' </summary>
    ''' <param name="source">El propio DataList que ha generado el evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automático, siempre que se clique uno de los elementos del DataList de los filtros(no el de proveedores)
    ''' Tiempo máximo: 0,5 seg</remarks>
    Private Sub lstFiltros_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles lstFiltros.ItemCommand
        borrarCacheCategorias()
        borrarCacheDestinos()
        borrarCacheProveedores()

        If e.CommandName = "QuitarTodo" Then
            FiltrosAnyadidos.RemoveAll(Function(x) x.ProveBusqueda = False)
        Else
            FiltrosAnyadidos.RemoveAll(Function(x) x.tipo.ToString() = e.CommandName And x.Codigo = e.CommandArgument)
        End If
        updFiltros.Update()
        GridArticulos.PageIndex = 0
        'GridArticulos.DataBind() 'No hacemos databind porque se va a hacer inmediatamente después en el RellenarGridArticulos
        Dim bActualizarListados As Boolean = True
        RellenarGridArticulos(bActualizarListados, Not Acceso.gbEPDesactivarCargaArticulos, Acceso.gbEPActivarCargaSolPM)
        UpdatePanelGrid.Update()
    End Sub
    ''' <summary>
    ''' Evento que se genera cuando se estan rellenando los elementos del DataList de filtros, para darles el valor de textos, etc..
    ''' </summary>
    ''' <param name="sender">El propio DataList de los filtros</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: La propia página, cada vez que se hace un postback y se rellenan los datos del DataList de filtros
    ''' Tiempo máximo: 0,2 seg</remarks>
    Private Sub lstFiltros_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles lstFiltros.ItemDataBound
        If e.Item.ItemType = ListItemType.Footer Then
            If CType(sender, DataList).Items.Count > 1 Then
                ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_CatalogoWeb
                CType(e.Item.FindControl("lnkQuitarTodo"), LinkButton).Text = Textos(127)
            Else
                e.Item.Visible = False
            End If
        End If
    End Sub
#End Region
#Region "Paginación"
    ''' <summary>
    ''' Procedimiento que se utiliza para cambiar el índice de la pagina en el GridView cuando el usuario selecciona un número
    ''' </summary>
    ''' <param name="sender">El objeto dropDownList que contiene los numeros de paginas</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: La interfaz al seleccionar un índice de página distinto al que estaba.
    ''' Tiempo máximo: 0,5 sec.</remarks>
    Protected Sub CmbBoxNumPags_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        'Sólo vamos a permitir que este evento se active cuando el control que lo ha lanzado 
        'sea el CmbBoxNumPags. Si viene de otro control, como el lstProv, no, porque podría impedir que se cumpla el objetivo de que al filtrar vuelva a la página 1 del paginador
        If Request.Form("__EVENTTARGET").ToString.IndexOf("$CmbBoxNumPags") > 0 Then
            GridArticulos_PageIndexChanging(sender, New GridViewPageEventArgs(DirectCast(sender, DropDownList).SelectedIndex))
        Else
            CType(sender, DropDownList).SelectedIndex = GridArticulos.PageIndex
        End If
    End Sub
    ''' Revisado por: blp. Fecha: 08/11/2012
    ''' <summary>
    ''' Procedimiento que Carga el paginador del GridView, con los textos, imagenes y valores por defecto
    ''' </summary>
    ''' <param name="Paginador">El paginador a iniciar</param>
    ''' <remarks>
    ''' Llamada desde: GridArticulos_RowDataBound
    ''' Tiempo máximo: 0,3 seg</remarks>
    Private Sub InicializarPaginador(ByVal Paginador As TableRow)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_CatalogoWeb
        CType(Paginador.FindControl("LblOrd"), Label).Text = Textos(85)
        Dim ddOrden As DropDownList = CType(Paginador.FindControl("ListaOrdenacionArt"), DropDownList)
        If ddOrden.Items.Count = 0 Then
            ddOrden.Items.Add(Textos(108))
            ddOrden.Items(0).Value = "PRECIO_UC * FC_DEF"
            ddOrden.Items.Add(Textos(109))
            ddOrden.Items(1).Value = "ART_DEN"
            ddOrden.Items.Add(Textos(110))
            ddOrden.Items(2).Value = "PROVEDEN"
            If Me.FSNUser.MostrarCodArt Then
                ddOrden.Items.Add(Textos(111))
                ddOrden.Items(3).Value = "COD_ITEM"
            End If
        End If
        ddOrden.SelectedValue = CampoOrden
        CType(Paginador.FindControl("LnkBtnOrdenacion"), LinkButton).ToolTip = IIf(SentidoOrdenacion = "ASC", Textos(106), Textos(107))
        CType(Paginador.FindControl("ImLnkBtnOrdenacion"), Image).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, IIf(SentidoOrdenacion = "ASC", "ascendente.gif", "descendente.gif"))
        CType(Paginador.FindControl("LnkBtnFirst"), LinkButton).Enabled = GridArticulos.PageIndex > 0
        CType(Paginador.FindControl("LnkBtnFirst"), LinkButton).ToolTip = Textos(116)
        CType(Paginador.FindControl("ImLnkBtnFirst"), Image).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, IIf(GridArticulos.PageIndex > 0, "primero.gif", "primero_desactivado.gif"))
        CType(Paginador.FindControl("LnkBtnPrev"), LinkButton).Enabled = GridArticulos.PageIndex > 0
        CType(Paginador.FindControl("LnkBtnPrev"), LinkButton).ToolTip = Textos(115)
        CType(Paginador.FindControl("ImLnkBtnPrev"), Image).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, IIf(GridArticulos.PageIndex > 0, "anterior.gif", "anterior_desactivado.gif"))
        CType(Paginador.FindControl("LblPagina"), Label).Text = Textos(78)
        With CType(Paginador.FindControl("CmbBoxNumPags"), DropDownList)
            For i As Integer = 1 To GridArticulos.PageCount
                .Items.Add(i)
            Next
            .SelectedIndex = GridArticulos.PageIndex
        End With
        CType(Paginador.FindControl("LblDe"), Label).Text = Textos(79)
        CType(Paginador.FindControl("LblToArt"), Label).Text = GridArticulos.PageCount
        CType(Paginador.FindControl("LnkBtnNext"), LinkButton).Enabled = GridArticulos.PageIndex < GridArticulos.PageCount - 1
        CType(Paginador.FindControl("LnkBtnNext"), LinkButton).ToolTip = Textos(114)
        CType(Paginador.FindControl("ImLnkBtnNext"), Image).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, IIf(GridArticulos.PageIndex < GridArticulos.PageCount - 1, "siguiente.gif", "siguiente_desactivado.gif"))
        CType(Paginador.FindControl("LnkBtnLast"), LinkButton).Enabled = GridArticulos.PageIndex < GridArticulos.PageCount - 1
        CType(Paginador.FindControl("LnkBtnLast"), LinkButton).ToolTip = Textos(117)
        CType(Paginador.FindControl("ImLnkBtnLast"), Image).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Theme, IIf(GridArticulos.PageIndex < GridArticulos.PageCount - 1, "ultimo.gif", "ultimo_desactivado.gif"))
        CType(Paginador.FindControl("TextBoxBusqArt_TextBoxWatermarkExtender"), TextBoxWatermarkExtender).WatermarkText = Textos(121)
        Dim oAutocompleteArticulos As AutoCompleteExtender = CType(Paginador.FindControl("TextBoxBusqArt_AutoCompleteExtender"), AutoCompleteExtender)
        If oAutocompleteArticulos IsNot Nothing Then
            oAutocompleteArticulos.ContextKey = Me.Usuario.CodPersona
            'Añadimos una función de javascript al evento de mostrar el panel de autocomplete para corregir un bug del control AjaxControlToolkit.AutoCompleteExtender, 
            'por el que en FF y Chrome se añade un margin al control por el tamaño de letra definido para el control contenedor, en este caso el body, de modo que el desplegable no se encuentra pegado al textbox del que depende.
            'Esta función se asigna al evento OnClientShowing del control en EPWebControls\FSEPWebPartBusqArticulos.vb
            oAutocompleteArticulos.OnClientShowing = "quitarMarginTop_" & oAutocompleteArticulos.ClientID
            'creamos y Registramos el script de la función
            Dim sScript As String = "function quitarMarginTop_" & oAutocompleteArticulos.ClientID & "() { " & vbCrLf
            sScript += "$get('" & oAutocompleteArticulos.ClientID & "_completionListElem').style.marginTop = '0px';" & vbCrLf
            sScript += "};"
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "quitarMarginTop_" & oAutocompleteArticulos.ClientID, sScript, True)
        End If
        CType(Paginador.FindControl("BtnFiltrarPaginador"), FSNWebControls.FSNButton).Text = Textos(86)
    End Sub
#End Region
#Region "Arbol Categorias"
    ''' <summary>
    ''' Propiedad que devuelve todas las categorias del catálogo de artículos
    ''' </summary>
    ''' <value>El valor de las categorías</value>
    ''' <returns>Un DataSet con los datos de todas las categorías del catálogo de artículos</returns>
    ''' <remarks>
    ''' Llamada desde: CargarArbolCategorias, Page_Load
    ''' Tiempo máximo: 0,5 seg</remarks>
    Private ReadOnly Property TodasCategorias() As DataSet
        Get
            Dim ds As DataSet
            If HttpContext.Current.Cache("TodasCategoriasEmi_" & FSNUser.Cod) Is Nothing Then
                Dim oCCategorias As FSNServer.CCategorias = FSNServer.Get_Object(GetType(FSNServer.CCategorias))
                ds = oCCategorias.CargarArbolCategorias(FSNUser.CodPersona)
                Me.InsertarEnCache("TodasCategoriasEmi_" & FSNUser.Cod, ds)
            Else
                ds = HttpContext.Current.Cache("TodasCategoriasEmi_" & FSNUser.Cod)
            End If
            Dim key() As DataColumn = {ds.Tables(0).Columns("ID")}
            ds.Tables(0).PrimaryKey = key
            Return ds
        End Get
    End Property
    ''' <summary>
    ''' Procedimiento en el que cargaremos los valores para el arbol de categorías
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: El evento Load de la página
    ''' Tiempo máximo: 0 sec.</remarks>
    Private Sub CargarArbolCategorias()
        If TodasCategorias.Tables(0) IsNot Nothing AndAlso TodasCategorias.Tables(0).Rows.Count > 0 Then
            fsnTvwCategorias.Nodes(0).Text = Textos(97) 'Categorias
            fsnTvwCategorias.NodeStyle.Height = "20"
        Else
            fsnTvwCategorias.Nodes.Clear()
        End If
    End Sub
    ''' <summary>
    ''' Evento que se lanzarÃ¡ al seleccionar un nodo del Ã¡rbol de categorÃ­as, mostrando la categorÃ­a seleccionada en el panel de bÃƒÆ’Ã‚Âºsqueda
    ''' </summary>
    ''' <param name="sender">el objecto que llama al evento (el arbol)</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el propio control
    ''' Tiempo mÃ¡ximo: 0 seg.</remarks>
    Private Sub fsntvwCategorias_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fsnTvwCategorias.SelectedNodeChanged
        If fsnTvwCategorias.SelectedNode.Value <> "_0_" Then
            FiltrarCategoria(fsnTvwCategorias.SelectedNode.Value, fsnTvwCategorias.SelectedNode.Text, True)
            mpePanelArbol.Hide()
        End If
    End Sub
    Public Sub fsnTvwCategorias_TreeNodePopulate(ByVal sender As Object, ByVal e As TreeNodeEventArgs) Handles fsnTvwCategorias.TreeNodePopulate
        Dim query
        Select Case e.Node.Value
            Case "_0_"
                query = From datos In TodasCategorias.Tables(0)
                        Where datos("PADRE") Is System.DBNull.Value
                        Select datos

            Case Else
                query = From datos In TodasCategorias.Tables(0)
                        Where datos("PADRE") IsNot System.DBNull.Value AndAlso datos("PADRE") = e.Node.Value
                        Select datos

        End Select

        If CType(query, System.Data.EnumerableRowCollection(Of System.Data.DataRow)).Any Then
            For Each oDatosNodo As System.Data.DataRow In CType(query, System.Data.EnumerableRowCollection(Of System.Data.DataRow))
                Dim nodo As New TreeNode
                nodo.Text = oDatosNodo.Item("DEN")
                nodo.Value = oDatosNodo.Item("ID")
                nodo.Expanded = False
                nodo.PopulateOnDemand = True
                nodo.SelectAction = TreeNodeSelectAction.Select
                e.Node.ChildNodes.Add(nodo)
            Next
        End If
    End Sub
    ''' <summary>
    ''' Evento que se lanzarÃ¡ al pulsar la (X) de una categorÃ­a en los filtros de categorÃ­as
    ''' y que quita la categorÃ­a de ahÃ­
    ''' </summary>
    ''' <param name="sender">el objecto que llama al evento</param>
    ''' <param name="e">argumentos del evento Click</param>
    ''' <remarks>Llamada desde el propio control
    ''' Tiempo mÃ¡ximo: 0 seg.</remarks>
    Private Sub lnkQuitarCategoria_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles lnkQuitarCategoria.Click
        fsnTvwCategorias.SelectedNode.Selected = False
        pnlCategorias.Update()
    End Sub
#End Region
#Region "Popup Proveedores"
    ''' <summary>
    ''' Evento que se genera cuando se clica el botón de buscar proveedores por los filtros que haya introducido el usuario, devolviendo los artículos que lo cumplan y filtrando el resto de elementos
    ''' </summary>
    ''' <param name="sender">El propio botón clicado</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automático, siempre que se clique el botón para buscar los proveedores
    ''' Tiempo máximo: 0,5 seg</remarks>
    Protected Sub BtnBusqProves_click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnBuscarProve.Click
        Dim oProvedoores As FSNServer.CProveedores
        Dim cookie As HttpCookie
        oProvedoores = FSNServer.Get_Object(GetType(FSNServer.CProveedores))
        Dim CodigoProve As String = IIf(String.IsNullOrEmpty(tbCodProveBusq.Text), "", Split(tbCodProveBusq.Text, " - ", , CompareMethod.Text)(0))
        Dim DenProve As String
        If tbDenProveBusq.Text.Contains(" - ") Then
            DenProve = IIf(String.IsNullOrEmpty(tbDenProveBusq.Text), "", Mid(tbDenProveBusq.Text, InStr(tbDenProveBusq.Text, " - ", CompareMethod.Text) + 3))
        Else
            DenProve = IIf(String.IsNullOrEmpty(tbDenProveBusq.Text), "", tbDenProveBusq.Text)
        End If
        Dim CIFProve As String = IIf(String.IsNullOrEmpty(tbCIFProveBusq.Text), "", tbCIFProveBusq.Text)
        Dim PaiProve As String = IIf(String.IsNullOrEmpty(CmbBoxPaiProveBusq.SelectedValue), "", CmbBoxPaiProveBusq.SelectedValue)
        Dim ProviProve As String = IIf(String.IsNullOrEmpty(CmbBoxProviProveBusq.SelectedValue), "", CmbBoxProviProveBusq.SelectedValue)
        Dim CPProve As String = IIf(String.IsNullOrEmpty(tbCpProveBusq.Text), "", tbCpProveBusq.Text)
        Dim PobProve As String = IIf(String.IsNullOrEmpty(CmboBoxPobProveBusq.SelectedValue), "", CmboBoxPobProveBusq.SelectedValue)
        Dim oNuevoFiltro As New Filtro
        Dim EstaFiltro As Boolean = False
        cookie = New HttpCookie("FiltrosProveedor")

        'incidencia 14243 ------
        FiltrosAnyadidos.Clear()
        '-----------------------

        borrarCacheCategorias()
        borrarCacheProveedores()
        borrarCacheDestinos()

        If Not String.IsNullOrEmpty(CodigoProve) Then 'AndAlso Not EstaFiltroCodigoProve Then
            oNuevoFiltro = New Filtro
            oNuevoFiltro.tipo = TipoFiltro.Proveedor
            oNuevoFiltro.Nombre = Textos(75)
            oNuevoFiltro.Codigo = "cod"
            oNuevoFiltro.Denominacion = tbCodProveBusq.Text
            oNuevoFiltro.CodBusquedaProve = CodigoProve
            oNuevoFiltro.ProveBusqueda = True
            FiltrosAnyadidos.Add(oNuevoFiltro)
            cookie.Item("Cod") = CodigoProve
        End If

        If Not String.IsNullOrEmpty(DenProve) Then 'AndAlso Not EstaFiltroDenProve Then
            oNuevoFiltro = New Filtro
            oNuevoFiltro.Nombre = Textos(144)
            oNuevoFiltro.tipo = TipoFiltro.Proveedor
            oNuevoFiltro.Codigo = "den"
            oNuevoFiltro.Denominacion = tbDenProveBusq.Text
            oNuevoFiltro.CodBusquedaProve = DenProve
            oNuevoFiltro.ProveBusqueda = True
            FiltrosAnyadidos.Add(oNuevoFiltro)
            cookie.Item("Den") = DenProve
        End If

        If Not String.IsNullOrEmpty(CIFProve) Then 'AndAlso Not EstaFiltroCIFProve Then
            oNuevoFiltro = New Filtro
            oNuevoFiltro.tipo = TipoFiltro.Proveedor
            oNuevoFiltro.Nombre = Textos(145)
            oNuevoFiltro.Codigo = "cif"
            oNuevoFiltro.Denominacion = tbCIFProveBusq.Text
            oNuevoFiltro.CodBusquedaProve = CIFProve
            oNuevoFiltro.ProveBusqueda = True
            FiltrosAnyadidos.Add(oNuevoFiltro)
            cookie.Item("Cif") = CIFProve
        End If

        If Not String.IsNullOrEmpty(PaiProve) Then 'AndAlso Not EstaFiltroPaiProve Then
            oNuevoFiltro = New Filtro
            oNuevoFiltro.tipo = TipoFiltro.Proveedor
            oNuevoFiltro.Nombre = Textos(147)
            oNuevoFiltro.Codigo = "pai"
            oNuevoFiltro.Denominacion = Left(CmbBoxPaiProveBusq.SelectedItem.Text, InStr(CmbBoxPaiProveBusq.SelectedItem.Text, "(", CompareMethod.Text) - 1)
            oNuevoFiltro.CodBusquedaProve = PaiProve
            oNuevoFiltro.ProveBusqueda = True
            FiltrosAnyadidos.Add(oNuevoFiltro)
            cookie.Item("Pai") = PaiProve
        End If

        If Not String.IsNullOrEmpty(ProviProve) Then 'AndAlso Not EstaFiltroProviProve Then
            oNuevoFiltro = New Filtro
            oNuevoFiltro.tipo = TipoFiltro.Proveedor
            oNuevoFiltro.Nombre = Textos(148)
            oNuevoFiltro.Codigo = "provi"
            oNuevoFiltro.Denominacion = Left(CmbBoxProviProveBusq.SelectedItem.Text, InStr(CmbBoxProviProveBusq.SelectedItem.Text, "(", CompareMethod.Text) - 1)
            oNuevoFiltro.CodBusquedaProve = ProviProve
            oNuevoFiltro.ProveBusqueda = True
            FiltrosAnyadidos.Add(oNuevoFiltro)
            cookie.Item("Provi") = ProviProve
        End If

        If Not String.IsNullOrEmpty(PobProve) Then 'AndAlso Not EstaFiltroPobProve Then
            oNuevoFiltro = New Filtro
            oNuevoFiltro.tipo = TipoFiltro.Proveedor
            oNuevoFiltro.Nombre = Textos(149)
            oNuevoFiltro.Codigo = "pob"
            oNuevoFiltro.Denominacion = Left(CmboBoxPobProveBusq.SelectedItem.Text, InStr(CmboBoxPobProveBusq.SelectedItem.Text, "(", CompareMethod.Text) - 1)
            oNuevoFiltro.CodBusquedaProve = PobProve
            oNuevoFiltro.ProveBusqueda = True
            FiltrosAnyadidos.Add(oNuevoFiltro)
            cookie.Item("Pob") = PobProve
        End If

        If Not String.IsNullOrEmpty(CPProve) Then 'AndAlso Not EstaFiltroCPProve Then
            oNuevoFiltro = New Filtro
            oNuevoFiltro.tipo = TipoFiltro.Proveedor
            oNuevoFiltro.Nombre = Textos(146)
            oNuevoFiltro.Codigo = "cp"
            oNuevoFiltro.Denominacion = tbCpProveBusq.Text
            oNuevoFiltro.CodBusquedaProve = CPProve
            oNuevoFiltro.ProveBusqueda = True
            FiltrosAnyadidos.Add(oNuevoFiltro)
            cookie.Item("Cp") = CPProve
        End If

        cookie.Expires = DateTime.Now.AddDays(30)
        Response.AppendCookie(cookie)
        Dim oProveedoresFiltrados As New List(Of Filtro)
        If Not _DsetProveedores Is Nothing AndAlso Not _DsetProveedores.Tables.Count = 0 Then
            Dim dtListaProveedores As New DataTable
            dtListaProveedores.Columns.Add("PROVE", GetType(String))
            dtListaProveedores.Columns.Add("DEN", GetType(String))
            dtListaProveedores.Columns.Add("Cantidad", GetType(Integer))

            Dim query = From Datos In _DsetProveedores.Tables(0)
                        Where (IIf(String.IsNullOrEmpty(CodigoProve), True, DBNullToStr(Datos.Item("Cod")).ToUpper.Contains(CodigoProve.ToUpper))) _
                        And (IIf(String.IsNullOrEmpty(DenProve), True, DBNullToStr(Datos.Item("Den")).ToUpper.Contains(DenProve.ToUpper))) _
                        And (IIf(String.IsNullOrEmpty(PaiProve), True, DBNullToStr(Datos.Item("Pai")) = PaiProve)) _
                        And (IIf(String.IsNullOrEmpty(ProviProve), True, DBNullToStr(Datos.Item("Provi")) = ProviProve)) _
                        And (IIf(String.IsNullOrEmpty(CPProve), True, DBNullToStr(Datos.Item("CP")).ToUpper = CPProve.ToUpper)) _
                        And (IIf(String.IsNullOrEmpty(PobProve), True, DBNullToStr(Datos.Item("Pob")) = PobProve)) _
                        And (IIf(String.IsNullOrEmpty(CIFProve), True, DBNullToStr(Datos.Item("NIF")).ToUpper.Contains(CIFProve.ToUpper)))
                        Group Datos By cantidad = Datos.Item("Cont"), prove = Datos.Item("COD"), den = Datos.Item("DEN") Into g = Group
                        Order By den Ascending
                        Select dtListaProveedores.LoadDataRow(New Object() {prove, den, cantidad}, False)

            query.Count() 'Con esta línea llenamos el datatable dtListaProveedores

            Me.InsertarEnCache("ProveedoresFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString, dtListaProveedores)
        Else
            Dim dtListaProveedoresVacio As New DataTable
            Me.InsertarEnCache("ProveedoresFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString, dtListaProveedoresVacio)
        End If
        FilaFiltroProve.Visible = True
        GridArticulos.PageIndex = 0
        'GridArticulos.DataBind() 'No hacemos databind porque se va a hacer inmediatamente después en el RellenarGridArticulos
        Dim bActualizarListados As Boolean = True
        RellenarGridArticulos(bActualizarListados, , Acceso.gbEPActivarCargaSolPM)
        updFiltros.Update()
        UpdatePanelGrid.Update()
        mpopupProveedores.Hide()
    End Sub
    ''' <summary>
    ''' Evento que se genera cuando un usuario escribe 3 o más caracteres en el textBox de código de proveedores, autocompletando si hay algúna coincidencia
    ''' </summary>
    ''' <param name="prefixText">El texto que el usuario haya escrito</param>
    ''' <param name="count">La cantidad de carácteres escritos</param>
    ''' <param name="contextKey">La clave para la busqueda, en este caso, el código de usuario e idioma</param>
    ''' <returns>Una lista de texto con las coincidencias de proveedores</returns>
    ''' <remarks>
    ''' Llamada desde: Automática, cada vez que se escriben 3 o más caracteres en el textbox de código de proveedor en el popup de búsqueda
    ''' Tiempo máximo: 0,1 seg</remarks>
    <System.Web.Services.WebMethodAttribute(),
     System.Web.Script.Services.ScriptMethodAttribute()>
    Public Shared Function Proveedores_AutocompletarCodigo(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        If Not HttpContext.Current.Cache("DatosProveedoresFiltrados_" & contextKey) Is Nothing Then
            Dim TablaProves As DataTable = CType(HttpContext.Current.Cache("DatosProveedoresFiltrados_" & contextKey), DataSet).Tables(0)
            Dim arrayPalabras As String() = Split(prefixText)
            Dim sbusqueda = From Datos In TablaProves
                            Let w = UCase(DBNullToStr(Datos.Item("Cod")))
                            Where arrayPalabras.All(Function(palabra) w Like "*" & strToSQLLIKE(palabra, True) & "*")
                            Select COD = Datos.Item("Cod"), dEN = Datos.Item("Den") Distinct.Take(count).ToArray()
            Dim resul As String()
            ReDim resul(UBound(sbusqueda))
            For i As Integer = 0 To UBound(sbusqueda)
                resul(i) = sbusqueda(i).COD & " - " & sbusqueda(i).dEN ' & "(" & sbusqueda(i).CUENTA & ")"
                'En el javascript de CatalogoWeb.aspx hay una función (quitarDescripcion) que usa el texto " - " para quitar la descripción.
                'Si Aquí se cambia el modo de construir el string, es necesario tenerlo en cuenta en esa funcion.
            Next
            Return resul
        Else
            Return Nothing
        End If
    End Function
    ''' <summary>
    ''' Evento que se genera cuando un usuario escribe 3 o más caracteres en el textBox de denominación de proveedores, autocompletando si hay algúna coincidencia
    ''' </summary>
    ''' <param name="prefixText">El texto que el usuario haya escrito</param>
    ''' <param name="count">La cantidad de carácteres escritos</param>
    ''' <param name="contextKey">La clave para la busqueda, en este caso, el código de usuario e idioma</param>
    ''' <returns>Una lista de texto con las coincidencias de proveedores</returns>
    ''' <remarks>
    ''' Llamada desde: Automática, cada vez que se escriben 3 o más caracteres en el textbox de código de proveedor en el popup de búsqueda
    ''' Tiempo máximo: 0,1 seg</remarks>
    <System.Web.Services.WebMethodAttribute(),
     System.Web.Script.Services.ScriptMethodAttribute()>
    Public Shared Function Proveedores_AutocompletarDenominacion(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        If Not HttpContext.Current.Cache("DatosProveedoresFiltrados_" & contextKey) Is Nothing Then
            Dim TablaProves As DataTable = CType(HttpContext.Current.Cache("DatosProveedoresFiltrados_" & contextKey), DataSet).Tables(0)
            Dim arrayPalabras As String() = Split(prefixText)
            Dim sbusqueda = From Datos In TablaProves
                            Let w = UCase(DBNullToStr(Datos.Item("Den")))
                            Where arrayPalabras.All(Function(palabra) w Like "*" & strToSQLLIKE(palabra, True) & "*")
                            Select COD = Datos.Item("Cod"), dEN = Datos.Item("DEN") Distinct.Take(count).ToArray()
            Dim resul As String()
            ReDim resul(UBound(sbusqueda))
            For i As Integer = 0 To UBound(sbusqueda)
                resul(i) = sbusqueda(i).COD & " - " & sbusqueda(i).dEN ' & "(" & sbusqueda(i).CUENTA & ")"
                'En el javascript de CatalogoWeb.aspx hay una función (quitarCodigo) que usa el texto " - " para quitar el código
                'Si Aquí se cambia el modo de construir el string, es necesario tenerlo en cuenta en esa funcion.
            Next
            Return resul
        Else
            Return Nothing
        End If
    End Function
    ''' <summary>
    ''' Evento que se genera cuando se clica el botón de proveedores, abriendo el popup de busqueda con los posibles filtros para los actuales artículos del catálogo
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento, en este caso el LinkButton</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automático, cada vez que se clica el LinkButton 
    ''' Tiempo máximo: 0,3 seg</remarks>
    Protected Sub BtnProves_Click(ByVal sender As Object, ByVal e As EventArgs)

        'Llamamos al listado de proveedores con sus datos extendidos (pais, etc) para que si no está en caché, se cargue
        'porque el panel de proveedores lo necesita.
        If _DsetProveedores.Tables IsNot Nothing Then
            'No hacemos nada, sólo se trata de llamar a _DsetProveedores
        End If

        Dim sCodigoPais As String = ""
        tbCodProveBusq.Text = ""
        tbDenProveBusq.Text = ""
        tbCIFProveBusq.Text = ""
        tbCpProveBusq.Text = ""

        'Incidencia 14243. Hacemos una copia de FiltrosAnyadidos y luego lo vaciamos para que así 
        'salgan todos los datos de pais en el panel de Proveedores.
        'al final volvemos a darle su valor
        'Si se filtra por proveedor -> No importa volver a ponerlo porque lo borrará con lo filtrado
        'Si no se filtra por proveedor-> Hemos dejado el FiltrosAnyadidos tal y como estaba
        'Dim CopiaFiltrosAnyadidos As New List(Of Filtro)
        'For Each elementoFiltro As Filtro In FiltrosAnyadidos
        '    If elementoFiltro.ProveBusqueda = "pai" Then
        '        CopiaFiltrosAnyadidos.Add(elementoFiltro)
        '        FiltrosAnyadidos.Remove(elementoFiltro)
        '    End If
        'Next
        'FiltrosAnyadidos.Clear()

        RellenarCmbBoxPais()

        If FiltrosAnyadidos.Where(Function(x) x.ProveBusqueda = True).Count > 0 Then
            For Each oFiltro As Filtro In FiltrosAnyadidos
                If oFiltro.ProveBusqueda Then
                    Select Case oFiltro.Codigo.ToLower
                        Case "cod"
                            tbCodProveBusq.Text = oFiltro.CodBusquedaProve
                        Case "den"
                            tbDenProveBusq.Text = oFiltro.CodBusquedaProve
                        Case "cif"
                            tbCIFProveBusq.Text = oFiltro.CodBusquedaProve
                        Case "pai"
                            If CmbBoxPaiProveBusq.Items.Count > 1 Then
                                CmbBoxPaiProveBusq.SelectedItem.Selected = False
                                For Each ItemLista As ListItem In CmbBoxPaiProveBusq.Items
                                    If ItemLista.Value = oFiltro.CodBusquedaProve Then
                                        ItemLista.Selected = True
                                        'En este punto hemos comprobado que hay hay un pai ss filtrado
                                        'por lo que FORZAMOS la CARGA DE PROVINCIAS Y MOSTRAmos EL COMBO
                                        RellenarCmbBoxProvincia(oFiltro.CodBusquedaProve)
                                        sCodigoPais = oFiltro.CodBusquedaProve
                                        Exit For
                                    End If
                                Next
                                CmbBoxPaiProveBusq.Visible = False
                                LblPaiProveBusqSelec.Visible = True
                                LblPaiProveBusqSelec.Text = CmbBoxPaiProveBusq.SelectedItem.Text
                            End If

                        Case "provi"
                            If CmbBoxProviProveBusq.Items.Count > 1 Then
                                CmbBoxProviProveBusq.SelectedItem.Selected = False
                                For Each ItemLista As ListItem In CmbBoxProviProveBusq.Items
                                    If ItemLista.Value = oFiltro.CodBusquedaProve Then
                                        ItemLista.Selected = True
                                        'CARGAMOS LAS POBLACIONES Y MOSTRAMOS EL COMBO
                                        If Not String.IsNullOrEmpty(sCodigoPais) Then
                                            RellenarCmbBoxPoblacion(sCodigoPais, oFiltro.CodBusquedaProve)
                                        End If
                                        Exit For
                                    End If
                                Next
                                CmbBoxProviProveBusq.Visible = False
                                LblProviProveBusqSelec.Visible = True
                                LblProviProveBusqSelec.Text = CmbBoxProviProveBusq.SelectedItem.Text
                            End If

                        Case "pob"
                            If CmboBoxPobProveBusq.Items.Count > 1 Then
                                CmboBoxPobProveBusq.SelectedItem.Selected = False
                                For Each ItemLista As ListItem In CmboBoxPobProveBusq.Items
                                    If ItemLista.Value = oFiltro.CodBusquedaProve Then
                                        ItemLista.Selected = True
                                        Exit For
                                    End If
                                Next
                                CmboBoxPobProveBusq.Visible = False
                                lblPobProveBusqSelec.Visible = True
                                lblPobProveBusqSelec.Text = CmboBoxPobProveBusq.SelectedItem.Text
                            End If

                        Case "cp"
                            tbCpProveBusq.Text = oFiltro.CodBusquedaProve
                    End Select
                End If
            Next

        End If

        ''Incidencia 14243. Volvemos a rellenar FiltrosAnyadidos
        'For Each elementoFiltro In CopiaFiltrosAnyadidos
        '    FiltrosAnyadidos.Add(elementoFiltro)
        'Next
        'CopiaFiltrosAnyadidos.Clear()

        UpBusqProvesFiltros.Update()
        mpopupProveedores.Show()
    End Sub
    ''' <summary>
    ''' Evento que se genera cada vez que se cambia el País en el ComboBox de paises de la busqueda de proveedores, filtrando las provincias de ese país
    ''' </summary>
    ''' <param name="sender">El propio objeto que lanza el evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automática, cada vez que se selecciona un item del combobox de paises
    ''' Tiempo máximo: 0 seg</remarks>
    Protected Sub CmbBoxPaiProveBusq_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbBoxPaiProveBusq.SelectedIndexChanged

        Dim CodPais As String = CType(sender, DropDownList).SelectedValue

        RellenarCmbBoxProvincia(CodPais)

        UpBusqProvesFiltros.Update()
    End Sub
    ''' <summary>
    ''' Evento que se genera cada vez que se cambia la provincia en el ComboBox de paises de la busqueda de proveedores, filtrando las poblaciones de esa provincia
    ''' </summary>
    ''' <param name="sender">El propio objeto que lanza el evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automática, cada vez que se selecciona un item del combobox de provincias
    ''' Tiempo máximo: 0 seg</remarks>
    Protected Sub CmbBoxProviProveBusq_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbBoxProviProveBusq.SelectedIndexChanged

        Dim PaisElegido As String = CmbBoxPaiProveBusq.SelectedItem.Value
        Dim ProviElegida As String = CType(sender, DropDownList).SelectedValue

        RellenarCmbBoxPoblacion(PaisElegido, ProviElegida)

        UpBusqProvesFiltros.Update()
    End Sub
    ''' <summary>
    ''' Evento que se genera cuando se clica un elemento del DataList de filtros de proveedor, realizando las acciones pertinentes dependiendo del elemento
    ''' </summary>
    ''' <param name="source">El objeto que lanza el evento, en este caso el DataList</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automática, siempre que se clique un elemento del DataList de los filtros de proveedor
    ''' Tiempo máximo: 0,3 seg</remarks>
    Private Sub lstFiltrosProve_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles DlFiltrosProve.ItemCommand
        borrarCacheCategorias()
        borrarCacheProveedores()
        borrarCacheDestinos()

        If e.CommandName = "QuitarTodo" Then
            FiltrosAnyadidos.RemoveAll(Function(x As Filtro) x.ProveBusqueda = True)
            If Not Request.Cookies("FiltrosProveedor") Is Nothing Then
                Dim MiCookie As HttpCookie = Request.Cookies("FiltrosProveedor")
                MiCookie.Value = String.Empty
                Response.AppendCookie(MiCookie)
            End If
        Else
            FiltrosAnyadidos.RemoveAll(Function(x As Filtro) x.ProveBusqueda = True AndAlso x.Codigo = e.CommandArgument AndAlso x.Denominacion.ToString() = e.CommandName)
            If Not Request.Cookies("FiltrosProveedor") Is Nothing Then
                Dim MiCookie As HttpCookie = Request.Cookies("FiltrosProveedor")
                MiCookie(e.CommandArgument.ToString) = Nothing
                Response.AppendCookie(MiCookie)
            End If
        End If
        GridArticulos.PageIndex = 0
        'GridArticulos.DataBind() 'No hacemos databind porque se va a hacer inmediatamente después en el RellenarGridArticulos
        Dim bActualizarListados As Boolean = True
        RellenarGridArticulos(bActualizarListados, , Acceso.gbEPActivarCargaSolPM)
        UpdatePanelGrid.Update()
        updFiltros.Update()
    End Sub
    ''' <summary>
    ''' Evento que se genera para generar los datos de la lista de filtros de proveedor, cargando sus textos y/o propiedades
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento, en este caso El DataList de filtros de proveedor</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automática, cada vez que se hace un postBack o se cargan los controles
    ''' Tiempo máximo: 0 seg</remarks>
    Private Sub lstFiltrosProve_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DlFiltrosProve.ItemDataBound
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_CatalogoWeb
        If e.Item.ItemType = ListItemType.Footer Then
            If CType(sender, DataList).Items.Count > 1 Then
                e.Item.Visible = True
                FilaFiltroProve.Visible = True
                CType(e.Item.FindControl("lnkQuitarTodo"), LinkButton).Text = Textos(127)
            ElseIf CType(sender, DataList).Items.Count = 0 Then
                e.Item.Visible = False
                FilaFiltroProve.Visible = False
            ElseIf CType(sender, DataList).Items.Count = 1 Then
                e.Item.Visible = False
                FilaFiltroProve.Visible = True
            End If
        ElseIf e.Item.ItemType = ListItemType.Header Then
            If FiltrosAnyadidos.Where(Function(x) x.ProveBusqueda = True).Count > 0 Then
                e.Item.Visible = True
                FilaFiltroProve.Visible = True
                CType(e.Item.FindControl("lblFiltroDl"), Label).Text = Textos(105) & ":"
            Else
                e.Item.Visible = False
                FilaFiltroProve.Visible = False
            End If
        End If
        updFiltros.Update()
    End Sub
#End Region
#Region "Popup Destinos"
    ''' <summary>
    ''' Procedimiento que carga el DataList de destinos que se encuentra en el popup, con los datos de todos los destinos posibles
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: El page_load
    ''' Tiempo máximo: 0,2 seg</remarks>
    Private Sub CargarPopupDestinos()
        If HttpContext.Current.Cache("DestinosTodos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) Is Nothing Then
            Dim query = From Datos In DsetArticulos.Tables(0)
                        Select DESTINO = Datos.Item("DEST").ToString() Distinct

            If query.Any Then
                Dim query2 = From Destinos In TodosDestinos
                             Join Datos In query On Destinos.Value Equals Datos.ToString()
                             Order By Destinos.Text
                             Select New ListItem(Destinos.Text, Destinos.Value) Distinct

                'Insertamos las categorias en caché con las mismas condiciones que el dsetarticulos, único elemento del que depende.                
                Me.InsertarEnCache("DestinosTodos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString, query2.ToList())

                lstDestPopup.DataSource = query2.ToList()
                lstDestPopup.DataBind()
            End If
        Else
            lstDestPopup.DataSource = HttpContext.Current.Cache("DestinosTodos_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
            lstDestPopup.DataBind()
        End If
    End Sub
#End Region
#Region "Grid Articulos"
    ''' <summary>
    ''' Función que rellena el GridArticulos y todas las listas de filtros en función de los artículos mostrados
    ''' </summary>
    ''' <param name="actualizarListados">
    ''' Campo que nos indica si debemos actualizar los listados de los filtros de Proveedor, Categoría y Destino.
    ''' Los listados están cacheados, de modo que no debería ser necesario controlar la recarga con una variable.
    ''' La usamos para evitar excesivas llamadas a la función DevolverArticulosConFiltro, que en entornos con gran número de artículos puede ser bastante lenta
    ''' </param>
    ''' <param name="bCargarGridArticulos">Indica si hay que cargar el grid de artÃ­culos</param>
    ''' <param name="bCargarSolicitudes">Indica si hay que cargar el grid de solicitudes</param>
    ''' <remarks>Llamada desde Page_Load.</remarks>
    Private Sub RellenarGridArticulos(ByVal actualizarListados As Boolean, Optional ByVal bCargarGridArticulos As Boolean = True, Optional ByVal bCargarSolicitudes As Boolean = True)
        Dim dvArticulosConFiltro As DataView = DevolverArticulosConFiltro(CampoOrden, SentidoOrdenacion)
        Dim Solicitudes As FSNServer.Solicitudes

        'Limpiar grid
        GridArticulos.DataSource = Nothing
        GridArticulos.DataBind()
        If bCargarGridArticulos Then
            GridArticulos.DataSource = dvArticulosConFiltro
            GridArticulos.DataBind()
        Else
            cpePnlFiltroCat.Collapsed = True
            cpePnlFiltroProv.Collapsed = True
            cpePnlFiltroDests.Collapsed = True
        End If

        If actualizarListados Then
            rellenarListados(dvArticulosConFiltro)
        End If

        lblSolicitudes.Visible = False
        lblEmitir.Visible = False
        Me.tblSolicitudes.Visible = False
        uwgSolicitudes_WDG.Visible = False
        Me.lblFooter.Visible = False
        lblAplicarFiltro.Visible = (lstFiltros.Items.Count = 0 AndAlso Not bCargarGridArticulos)
        lblLnkSolicitudes.Visible = False

        If (GridArticulos.Rows.Count = 0) Then
            Dim dtArticulosPedidoAbierto As New DataTable
            If FSNUser.EPPermitirEmitirPedidosDesdePedidoAbierto AndAlso Not Request.QueryString("TextoBusquedaCatalogo") Is Nothing AndAlso Not Request.QueryString("TextoBusquedaCatalogo") = "" Then
                'No hay articulos de catalogo. Miraremos si hay algun pedido abierto con algun articulo de los buscados
                'Si hay redirigimos a pagina de pedido abierto, sino, seguiremos como hasta ahora mostrando, si tiene permiso PM
                'un lista de solicitudes...
                Dim cPedidoAbierto As FSNServer.CPedidoAbierto
                cPedidoAbierto = FSNServer.Get_Object(GetType(FSNServer.CPedidoAbierto))
                dtArticulosPedidoAbierto = cPedidoAbierto.Obtener_PedidosAbiertos_ArticuloBuscado(FSNUser.Cod, FSNUser.EPRestringirEmisionPedidosAbiertosEmpresaUsu,
                                                                                                  FSNUser.EPRestringirEmisionPedidosAbiertosEmpresasUONsPerfil, Request.QueryString("TextoBusquedaCatalogo"))
            End If

            If bCargarGridArticulos Then
                If Not FSNUser.EPPermitirEmitirPedidosDesdePedidoAbierto OrElse dtArticulosPedidoAbierto.Rows.Count = 0 Then
                    If (Acceso.gsAccesoFSPM <> FSNLibrary.TipoAccesoFSPM.SinAcceso And FSNUser.AccesoPM) Then
                        Solicitudes = FSNServer.Get_Object(GetType(FSNServer.Solicitudes))
                        Solicitudes.LoadData(FSNUser.Cod, Idioma, iTipoSolicitud:=TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras, bNuevoWorkfl:=True, sCodPer:=FSNUser.CodPersona)

                        If (Solicitudes.Data.Tables.Count > 0 AndAlso Solicitudes.Data.Tables(0).Rows.Count > 0) Or bCargarSolicitudes Then
                            uwgSolicitudes_WDG.DataSource = Solicitudes.Data
                            uwgSolicitudes_WDG.DataBind()
                            lblSolicitudes.Visible = True
                            lblEmitir.Visible = True
                            Me.tblSolicitudes.Visible = True
                            uwgSolicitudes_WDG.Visible = True
                            Me.lblFooter.Visible = True
                        Else
                            Me.tblSolicitudes.Visible = False
                            uwgSolicitudes_WDG.Visible = False

                            Me.lblFooter.Visible = False
                        End If
                    Else
                        lblSolicitudes.Visible = False
                        lblEmitir.Visible = False
                        Me.tblSolicitudes.Visible = False
                        uwgSolicitudes_WDG.Visible = False
                        Me.lblFooter.Visible = False
                    End If

                    lblLnkSolicitudes.Visible = tblSolicitudes.Visible
                Else
                    'redirigir
                    HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaEP") & "PedidoAbierto.aspx?denart=" & Request.QueryString("TextoBusquedaCatalogo"), True)
                End If
            End If
        Else
            If bCargarSolicitudes Then
                If (Acceso.gsAccesoFSPM <> FSNLibrary.TipoAccesoFSPM.SinAcceso And FSNUser.AccesoPM) Then
                    Solicitudes = FSNServer.Get_Object(GetType(FSNServer.Solicitudes))
                    Solicitudes.LoadData(FSNUser.Cod, Idioma, iTipoSolicitud:=TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras, bNuevoWorkfl:=True, sCodPer:=FSNUser.CodPersona)

                    If Solicitudes.Data.Tables.Count > 0 Or bCargarSolicitudes Then
                        uwgSolicitudes_WDG.DataSource = Solicitudes.Data
                        uwgSolicitudes_WDG.DataBind()
                        lblSolicitudes.Visible = True
                        lblEmitir.Visible = True
                        Me.tblSolicitudes.Visible = True
                        uwgSolicitudes_WDG.Visible = True
                        Me.lblFooter.Visible = True
                    Else
                        Me.tblSolicitudes.Visible = False
                        uwgSolicitudes_WDG.Visible = False
                        Me.lblFooter.Visible = False
                    End If
                Else
                    lblSolicitudes.Visible = False
                    lblEmitir.Visible = False
                    Me.tblSolicitudes.Visible = False
                    uwgSolicitudes_WDG.Visible = False
                    Me.lblFooter.Visible = False
                End If

                lblLnkSolicitudes.Visible = tblSolicitudes.Visible
            Else
                Me.tblSolicitudes.Visible = False
                uwgSolicitudes_WDG.Visible = False
            End If
        End If
        updLblSinArticulos.Update()
    End Sub
    ''' <summary>
    ''' Método mediante el cual rellenamos / actualizamos los listados de los filtros categorias, proveedores, destinos y filtros de proveedores
    ''' </summary>
    ''' <param name="dvArticulosConFiltro">dataview de los artículos filtrados</param>
    ''' <remarks>Llamada desde RellenarGridArticulos y Load. Máx: 0,5s</remarks>
    Private Sub rellenarListados(ByVal dvArticulosConFiltro As DataView)
        lstCategorias.DataSource = ListaCategorias(dvArticulosConFiltro)
        lstCategorias.DataBind()
        lstDestinos.DataSource = ListaDestinos(dvArticulosConFiltro)
        lstDestinos.DataBind()
        'Filtros generales
        lstFiltros.DataSource = FiltrosAnyadidos.Where(Function(filtro) filtro.ProveBusqueda = False)
        lstFiltros.DataBind()
        'Filtros de Proveedor
        If FiltrosAnyadidos.Where(Function(filtro) filtro.ProveBusqueda = True).Count > 0 Then
            FilaFiltroProve.Visible = True
            DlFiltrosProve.DataSource = FiltrosAnyadidos.Where(Function(filtro) filtro.ProveBusqueda = True)
            DlFiltrosProve.DataBind()
        Else
            FilaFiltroProve.Visible = False
        End If
        'Proveedores
        Dim dtListaProveedores As DataTable = ListaProveedores(dvArticulosConFiltro)
        'Cuando hay filtro por proveedor, se tienen que ver todos los proveedores, aunque haya uno.
        'Cuando no lo hay, sólo si hay más de uno
        If DlFiltrosProve.Visible _
            OrElse
           dtListaProveedores IsNot Nothing AndAlso dtListaProveedores.Rows.Count > 1 Then

            CrearyAgregarListadoProv(dtListaProveedores)
        Else
            Dim dtListaProveedoresVacio As New DataTable
            CrearyAgregarListadoProv(dtListaProveedoresVacio)
        End If

    End Sub
    ''' <summary>
    ''' Manejador de evento que se invoca automáticamente cuando se el botón para ordenar en el paginador del gridView GRidArticulos
    ''' </summary>
    ''' <param name="sender">GRidArticulos</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Desde el paginador</remarks>
    Private Sub GridArticulos_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GridArticulos.Sorting
        CampoOrden = e.SortExpression
        GridArticulos.DataBind()
    End Sub
    ''' <summary>
    ''' Manejador de evento que se invoca automáticamente cuando se pulsa cualquier botón en el paginador del gridView GRidArticulos
    ''' </summary>
    ''' <param name="sender">GridArticulos</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde los botones del paginador</remarks>
    Private Sub GridArticulos_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridArticulos.PageIndexChanging
        GridArticulos.PageIndex = e.NewPageIndex
        GridArticulos.DataBind()
        UpdatePanelGrid.Update()
    End Sub
    ''' Revisado por: blp. Fecha:23/01/2012
    ''' <summary>
    ''' Procedimiento que se encarga de mostrar los datos del grid según los parámetros de usuario
    ''' </summary>
    ''' <param name="sender">El propio objeto que genera el evento, en ese caso el GridView</param>
    ''' <param name="e">El evento de que se esta rellenando la fila</param>
    ''' <remarks>
    ''' Llamada desde:La propia página, cada vez que se carga el GridView
    ''' Tiempo máximo:Depende de los parámetros de usuario, pero aproximadamente 0,5 sg máximo.</remarks>
    Protected Sub GridArticulos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridArticulos.RowDataBound
        With e.Row
            If .RowType = DataControlRowType.DataRow Then
                Dim CodProve As String = String.Empty
                CodProve = CType(.DataItem, DataRowView).Item("PROVE")
                CType(.FindControl("ImgFavoritos"), ImageButton).ToolTip = Textos(95)
                Dim bEstaEnComparativa As Boolean = False
                If Not ArticulosComparar Is Nothing AndAlso ArticulosComparar.Count > 0 Then
                    If ArticulosComparar.Contains(CType(.DataItem, DataRowView).Item("ID")) Then
                        bEstaEnComparativa = True
                    End If
                End If
                With CType(.FindControl("ImgComparativa"), ImageButton)
                    If bEstaEnComparativa Then
                        .ImageUrl = VirtualPathUtility.GetDirectory(.ImageUrl) & "/comparar_desactivado.gif"
                        .ToolTip = ""
                    Else
                        .ImageUrl = VirtualPathUtility.GetDirectory(.ImageUrl) & "/comparar.gif"
                        .ToolTip = Textos(94)
                    End If
                    .Enabled = Not bEstaEnComparativa
                End With
                CType(.FindControl("LnkCodArtOc"), LinkButton).Visible = (FSNUser.MostrarCodArt)
                If FSNUser.ModificarPrecios AndAlso DBNullToInteger(e.Row.DataItem.Item("GENERICO")) = 1 Then
                    CType(.FindControl("TxtNomArt"), TextBox).Visible = True
                Else
                    CType(.FindControl("LnkNomArt"), LinkButton).Visible = True ' (CType(.DataItem, DataRowView).Item("ART_DEN"))

                End If
                If FSNUser.MostrarCantMin And Not IsDBNull(CType(.DataItem, DataRowView).Item("CANT_MIN_DEF")) AndAlso CType(.DataItem, DataRowView).Item("CANT_MIN_DEF") > 0 Then
                    CType(.FindControl("LblCantMinPedOc"), Label).Visible = True
                    CType(.FindControl("LblCantMinPedOc"), Label).Text = "(" & Textos(137) & " " & CType(.DataItem, DataRowView).Item("CANT_MIN_DEF")
                    If (Not IsDBNull(CType(.DataItem, DataRowView).Item("CANT_MAX_DEF")) AndAlso CType(.DataItem, DataRowView).Item("CANT_MAX_DEF") > 0) Then
                        CType(.FindControl("LblCantMinPedOc"), Label).Text = CType(.FindControl("LblCantMinPedOc"), Label).Text & " / " & Textos(170) & " " & CType(.DataItem, DataRowView).Item("CANT_MAX_DEF") & ")"
                    Else
                        CType(.FindControl("LblCantMinPedOc"), Label).Text = CType(.FindControl("LblCantMinPedOc"), Label).Text & ")"
                    End If
                End If
                CType(.FindControl("FSNLinkProve"), FSNWebControls.FSNLinkInfo).PanelInfo = FSNPanelDatosProve.ID
                CType(.FindControl("FSNLinkProve"), FSNWebControls.FSNLinkInfo).ContextKey = CType(.DataItem, DataRowView).Item("PROVE")
                Dim scad As String = String.Empty
                If FSNUser.MostrarCodProve AndAlso Not IsDBNull(CType(.DataItem, DataRowView).Item("PROVE")) Then _
                    scad = CType(.DataItem, DataRowView).Item("PROVE") & " - "
                scad = scad & CType(.DataItem, DataRowView).Item("PROVEDEN")
                CType(.FindControl("FSNLinkProve"), FSNWebControls.FSNLinkInfo).Text = scad
                Dim olstCats As New List(Of ListItem)
                Dim sCod As String = CType(.DataItem, DataRowView).Item("CAT1")
                If IsNumeric(sCod) Then
                    Dim dr As DataRow = TodasCategorias.Tables(0).Rows.Find(CInt(sCod))
                    If Not dr Is Nothing Then
                        olstCats.Add(New ListItem("(" & dr("COD") & ") " & dr("DEN"), sCod))
                    End If
                End If
                Dim i As Short = 2
                Do While i < 6 AndAlso Not IsDBNull(CType(.DataItem, DataRowView).Item("CAT" & i.ToString()))
                    sCod = sCod & "_" & CType(.DataItem, DataRowView).Item("CAT" & i.ToString())
                    Dim dr As DataRow = TodasCategorias.Tables(0).Rows.Find(sCod)
                    If Not dr Is Nothing Then
                        olstCats.Add(New ListItem("(" & dr("COD") & ") " & dr("DEN"), sCod))
                    End If
                    i += 1
                Loop
                CType(.FindControl("lstCategoriasArt"), DataList).DataSource = olstCats
                CType(.FindControl("lstCategoriasArt"), DataList).DataBind()
                CType(.FindControl("ImgBtnArt"), ImageButton).ToolTip = Textos(96)
                CType(.FindControl("LblDescArt"), Label).Visible = Not IsDBNull(CType(.DataItem, DataRowView).Item("ESP")) AndAlso Not String.IsNullOrEmpty(CType(.DataItem, DataRowView).Item("ESP"))
                If Me.FSNUser.MostrarAtrib Then
                    If Not IsDBNull(CType(.DataItem, DataRowView).Item("COD_ITEM")) And Not IsDBNull(CType(.DataItem, DataRowView).Item("PROVE")) Then
                        Dim oCArticulos As FSNServer.cArticulos = Me.FSNServer.Get_Object(GetType(FSNServer.cArticulos))
                        Dim dsAtrib As DataSet = oCArticulos.DevolverAtributosArt(DBNullToSomething(CType(.DataItem, DataRowView).Item("COD_ITEM")), CType(.DataItem, DataRowView).Item("PROVE"))
                        Dim atribs As DataRow() = dsAtrib.Tables(0).Select
                        CType(.FindControl("LblCaracteristicasArt"), Label).Visible = atribs.Length > 0
                        If atribs.Length > 0 Then
                            Dim strBldr As New StringBuilder("<b>" & Textos(135) & ":</b>")
                            For Each x As DataRow In atribs
                                strBldr.Append(" " & x.Item("DEN") & ": ")
                                If IsDBNull(x.Item("VALOR")) Then
                                    strBldr.Append("&nbsp; &nbsp; ")
                                Else
                                    Select Case x.Item("TIPO")
                                        Case 1
                                            strBldr.Append(x.Item("VALOR"))
                                        Case 2
                                            strBldr.Append(FSNLibrary.FormatNumber(x.Item("VALOR"), Me.Usuario.NumberFormat))
                                        Case 3
                                            strBldr.Append(FSNLibrary.FormatDate(x.Item("VALOR"), Usuario.DateFormat))
                                        Case 4
                                            If x.Item("VALOR") Then strBldr.Append(Textos(69)) Else strBldr.Append(Textos(70))
                                    End Select
                                End If
                            Next
                            CType(.FindControl("LblCaracteristicasArt"), Label).Text = strBldr.ToString()
                        End If
                    End If
                Else
                    CType(.FindControl("LblCaracteristicasArt"), Label).Visible = False
                End If


                Dim wNumEdPrec As FSNWebControls.FSNTextBox = CType(.FindControl("WNumEdPrecioArt"), FSNWebControls.FSNTextBox)
                Dim wNumEdPrec_DblValue As Label = CType(.FindControl("WNumEdPrecioArt_DblValue"), Label)
                Dim wNumEdPrecArrows As FSNWebControls.FSNUpDownArrows = CType(.FindControl("WNumEdPrecioArtArrows"), FSNWebControls.FSNUpDownArrows)
                Dim wNumEdCant As FSNWebControls.FSNTextBox = CType(.FindControl("WNumEdCantidad"), FSNWebControls.FSNTextBox)
                Dim wNumEdCant_DblValue As Label = CType(.FindControl("WNumEdCantidad_DblValue"), Label)
                Dim txt As TextBox = CType(.FindControl("txtNomArt"), TextBox)

                Dim lblMonUni As Label = CType(.FindControl("LblMonUni"), Label)

                Dim lblPrecio As Label = CType(.FindControl("LblPrecio"), Label)
                Dim spanX As Label = CType(.FindControl("spanX"), Label)
                Dim lnkUnidad As FSNWebControls.FSNLinkTooltip = CType(.FindControl("lnkUnidad"), FSNWebControls.FSNLinkTooltip)

                lblPrecio.Text = Textos(108)


                If (CType(.DataItem, DataRowView).Item("TIPORECEPCION") = 1) Then
                    CType(.FindControl("LblUnd"), Label).Text = Textos(77)
                    wNumEdPrec.Visible = False
                    wNumEdPrecArrows.Visible = False
                    lblPrecio.Visible = False
                    Dim MinValue As Nullable(Of Double) = DBNullToDbl(CType(.DataItem, DataRowView).Item("CANT_MIN_DEF"))
                    Dim MaxValue As Nullable(Of Double) = DBNullToDbl(CType(.DataItem, DataRowView).Item("CANT_MAX_DEF"))

                    wNumEdPrec_DblValue.Text = IIf(MinValue = 0, DBNullToDbl(CType(.DataItem, DataRowView).Item("PRECIO_UC")), MinValue).ToString
                    wNumEdPrec.DblValue = IIf(MinValue = 0, FSNLibrary.FormatNumber(DBNullToDbl(CType(.DataItem, DataRowView).Item("PRECIO_UC")), Me.Usuario.NumberFormat), FSNLibrary.FormatNumber(MinValue, Me.Usuario.NumberFormat)).ToString
                    wNumEdCant_DblValue.Text = IIf(MinValue = 0, DBNullToDbl(CType(.DataItem, DataRowView).Item("PRECIO_UC")), MinValue).ToString
                    wNumEdCant.Text = IIf(MinValue = 0, FSNLibrary.FormatNumber(DBNullToDbl(CType(.DataItem, DataRowView).Item("PRECIO_UC")), Me.Usuario.NumberFormat), FSNLibrary.FormatNumber(MinValue, Me.Usuario.NumberFormat)).ToString

                    MaxValue = IIf(MaxValue = 0, Nothing, MaxValue)
                    configurarDecimales(Trim(CType(.DataItem, DataRowView).Item("UP_DEF")), wNumEdCant, MinValue, MaxValue)

                    CType(.FindControl("LblPrecArt"), Label).Visible = True
                    CType(.FindControl("LblPrecArt"), Label).Text = FSNLibrary.FormatNumber(strToDbl(wNumEdPrec_DblValue.Text), Me.Usuario.NumberFormat)

                    lblMonUni.Text = DBNullToStr(CType(.DataItem, DataRowView).Item("MON"))
                    spanX.Visible = False
                    lnkUnidad.Visible = False
                    Dim lnk As FSNWebControls.FSNHyperlink = CType(.FindControl("LnkAnyadirACesta"), FSNWebControls.FSNHyperlink)
                    lnk.Text = Textos(87)
                    lnk.NavigateUrl = "javascript:FSNAnyadirACesta(" & CType(.DataItem, DataRowView).Item("ID").ToString() & ",'" & wNumEdCant_DblValue.ClientID & "','1','" & sMsgCantidadMayorQueCero & "', '" & txt.ClientID & "', " & CType(.DataItem, DataRowView).Item("TIPORECEPCION").ToString() & ")"
                Else
                    wNumEdPrec.Visible = (CType(.DataItem, DataRowView).Item("MODIF_PREC") = 1 And Me.FSNUser.ModificarPrecios)
                    wNumEdPrecArrows.Visible = wNumEdPrec.Visible
                    lblPrecio.Visible = wNumEdPrec.Visible
                    CType(.FindControl("LblUnd"), Label).Text = Textos(83)
                    CType(.FindControl("LblPrecArt"), Label).Visible = Not wNumEdPrec.Visible

                    wNumEdPrec_DblValue.Text = (CType(.DataItem, DataRowView).Item("PRECIO_UC") * CType(.DataItem, DataRowView).Item("FC_DEF")).ToString
                    Dim minimo As Integer = 0
                    configurarDecimales(Nothing, wNumEdPrec, minimo, Nothing)

                    wNumEdPrec.DblValue = FSNLibrary.FormatNumber(strToDbl(wNumEdPrec_DblValue.Text), Me.Usuario.NumberFormat)

                    CType(.FindControl("LblPrecArt"), Label).Text = FSNLibrary.FormatNumber(strToDbl(wNumEdPrec_DblValue.Text), Me.Usuario.NumberFormat)


                    Dim MinValue As Nullable(Of Double) = DBNullToDbl(CType(.DataItem, DataRowView).Item("CANT_MIN_DEF"))
                    Dim MaxValue As Nullable(Of Double) = DBNullToDbl(CType(.DataItem, DataRowView).Item("CANT_MAX_DEF"))
                    MaxValue = IIf(MaxValue = 0, Nothing, MaxValue)
                    wNumEdCant_DblValue.Text = IIf(MinValue = 0, 1, MinValue).ToString
                    configurarDecimales(Trim(CType(.DataItem, DataRowView).Item("UP_DEF")), wNumEdCant, MinValue, MaxValue)

                    lblMonUni.Text = DBNullToStr(CType(.DataItem, DataRowView).Item("MON")) & "/"
                    spanX.Visible = True
                    lnkUnidad.Visible = True
                    Dim lnk As FSNWebControls.FSNHyperlink = CType(.FindControl("LnkAnyadirACesta"), FSNWebControls.FSNHyperlink)
                    lnk.Text = Textos(87)
                    lnk.NavigateUrl = "javascript:FSNAnyadirACesta(" & CType(.DataItem, DataRowView).Item("ID").ToString() & ",'" & wNumEdPrec_DblValue.ClientID & "','" & wNumEdCant_DblValue.ClientID & "','" & sMsgCantidadMayorQueCero & "', '" & txt.ClientID & "', " & CType(.DataItem, DataRowView).Item("TIPORECEPCION").ToString() & ")"
                End If

            ElseIf e.Row.RowType = DataControlRowType.Pager Then
                InicializarPaginador(e.Row)
            End If
        End With
    End Sub
    ''' Revisado por: blp. Fecha:23/01/2012
    ''' <summary>
    ''' Procedimiento que se ejecuta al hacer click sobre cualquier control emplazado dentro del GridView con los artículos, para realizar las acciones que sean necesarias sobre la fila concreta que se ha clicado.
    ''' </summary>
    ''' <param name="sender">El objeto GridView</param>
    ''' <param name="e">argumentos de evento</param>
    ''' <remarks>Llamada desde la grid
    ''' Tiempo maximo 1,7 sec. maximo, no obstante el tiempo es variable, depende completamente de el evento al que se le llame, y eso depende directamente del control que se pinche.</remarks>
    Protected Sub GridArticulos_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridArticulos.RowCommand
        Dim lafila As GridViewRow = CType(e.CommandSource, Control).NamingContainer
        Select Case e.CommandName
            Case "AnyadirAComparativa"
                AnyadirAComparativa(lafila, e.CommandArgument, e.CommandSource)
            Case "BuscarArticulos"
                BuscarArticulos(lafila, sender, e)
            Case "AbrirDetalleArt"
                If CType(lafila.FindControl("lblCantidadNoAplica"), Label).Visible Then
                    pnlDetalleArticulo.CargarDetalleArticuloEP(DsetArticulos, CInt(CType(lafila.FindControl("lblId"), Label).Text), CDbl(CType(lafila.FindControl("wnumCantidad_DblValue"), Label).Text), CDbl(CType(lafila.FindControl("WNumEdPrecArt_DblValue"), Label).Text), True, 0)
                Else
                    pnlDetalleArticulo.CargarDetalleArticuloEP(DsetArticulos, CInt(CType(lafila.FindControl("lblId"), Label).Text), CDbl(CType(lafila.FindControl("wnumCantidad_DblValue"), Label).Text), CDbl(CType(lafila.FindControl("WNumEdPrecArt_DblValue"), Label).Text), True, 1)
                End If

            Case "Favorito"
                Dim Cantidad As Double = strToDbl(CType(lafila.FindControl("WNumEdCantidad_DblValue"), Label).Text)
                Dim arrArgs() As String = Split(e.CommandArgument, "###")
                CargarFavoritos(arrArgs(0), Cantidad, arrArgs(1), arrArgs(2))
        End Select
    End Sub
#End Region
#Region "Cesta"
    ''' <summary>
    ''' Añade un artículo a la cesta
    ''' </summary>
    ''' <param name="Peticion">ID de la petición javascript</param>
    ''' <param name="ID">ID de la línea seleccionada</param>
    ''' <param name="Importe">Importe introducido, únicamente se tendrá en cuenta si el usuario puede modificar el precio</param>
    ''' <param name="Cantidad">Cantidad a añadir</param>
    ''' <returns>Si se añade el artículo, los datos a mostrar en el WebPartCesta con el formato "Petición##Importe##NúmeroArtículos"
    ''' Si no se añade por un valor menor o igual a cero en la cantidad, una cadena vacía
    ''' Si se produce un error se devuelve el mensaje de error</returns>
    ''' <remarks>Se llama desde el código javascript de la página de forma asíncrona</remarks>
    <System.Web.Services.WebMethod(),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function AnyadirACesta(ByVal Peticion As Integer, ByVal ID As Integer, ByVal Importe As Double, ByVal Cantidad As Double, ByVal ArtDen As String) As String
        Try
            Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
            Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
            With DsetArticulos.Tables(0).Rows.Find(ID)
                Dim Precio As Double = .Item("PRECIO_UC")

                Dim sauxARTDEN As String
                If (ArtDen <> Nothing) Then
                    sauxARTDEN = ArtDen
                Else
                    sauxARTDEN = .Item("ART_DEN")
                End If
                If .Item("TIPORECEPCION") = 0 Then

                    If .Item("MODIF_PREC") = 1 And oUsuario.ModificarPrecios Then
                        Precio = Importe
                        Precio = Precio / .Item("FC_DEF")
                    End If
                Else
                    Precio = Importe
                End If
                If Cantidad > 0 Then
                    Dim oCArticulo As FSNServer.CArticulo = oServer.Get_Object(GetType(FSNServer.CArticulo))
                    oCArticulo.AnyadirACesta(oUsuario.CodPersona, .Item("FC_DEF"), .Item("PROVE"), ID,
                         DBNullToSomething(.Item("CAT1")), DBNullToSomething(.Item("CAT2")), DBNullToSomething(.Item("CAT3")), DBNullToSomething(.Item("CAT4")), DBNullToSomething(.Item("CAT5")),
                         sauxARTDEN, Cantidad, .Item("UP_DEF"), Precio, 0, .Item("MON"), .Item("TIPORECEPCION"))
                    'TxtNomArt.text()
                    Dim oUser As FSNServer.User = oServer.Get_Object(GetType(FSNServer.User))
                    Dim dr As DataRow = oUser.InfoCesta(oUsuario.Cod)
                    Dim dImp As Double
                    If dr.Item("IMPORTE") > 0 Then
                        dImp = CDbl(dr.Item("IMPORTE") * dr.Item("EQUIV"))
                    Else
                        dImp = 0
                    End If
                    Dim sDatosCesta As String = Peticion.ToString()
                    sDatosCesta = sDatosCesta & "##" & FSNLibrary.FormatNumber(dImp, oUsuario.NumberFormat) & " " & dr.Item("MON").ToString()
                    sDatosCesta = sDatosCesta & "##" & dr.Item("NUMARTICULOS").ToString()
                    Return sDatosCesta
                Else
                    Return Peticion.ToString("####")
                End If
            End With
        Catch ex As Exception
            Return Peticion.ToString() & "##" & ex.Message
        End Try
    End Function
    ''' <summary>
    ''' Añade un artículo a la cestaFav
    ''' </summary>
    ''' <param name="Peticion">ID de la petición javascript</param>
    ''' <param name="ID">ID de la línea seleccionada</param>
    ''' <param name="Importe">Importe introducido, únicamente se tendrá en cuenta si el usuario puede modificar el precio</param>
    ''' <param name="Cantidad">Cantidad a añadir</param>
    ''' <returns>Si se añade el artículo, los datos a mostrar en el WebPartCesta con el formato "Petición##Importe##NúmeroArtículos"
    ''' Si no se añade por un valor menor o igual a cero en la cantidad, una cadena vacía
    ''' Si se produce un error se devuelve el mensaje de error</returns>
    ''' <remarks>Se llama desde el código javascript de la página de forma asíncrona</remarks>
    <System.Web.Services.WebMethod(),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function AnyadirACestaFav(ByVal Peticion As Integer, ByVal ID As Integer, ByVal Importe As Double, ByVal Cantidad As Double, ByVal ArtDen As String) As String
        Try
            Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
            Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
            With DsetArticulos.Tables(0).Rows.Find(ID)
                Dim Precio As Double = .Item("PRECIO_UC")

                Dim sauxARTDEN As String
                If (ArtDen <> Nothing) Then
                    sauxARTDEN = ArtDen
                Else
                    sauxARTDEN = .Item("ART_DEN")
                End If
                If .Item("TIPORECEPCION") = 0 Then

                    If .Item("MODIF_PREC") = 1 And oUsuario.ModificarPrecios Then
                        Precio = Importe
                        Precio = Precio / .Item("FC_DEF")
                    End If
                Else
                    Precio = Importe
                End If
                If Cantidad > 0 Then
                    Dim oCArticulo As FSNServer.CArticulo = oServer.Get_Object(GetType(FSNServer.CArticulo))
                    oCArticulo.AnyadirACestaFav(oUsuario.CodPersona, .Item("FC_DEF"), .Item("PROVE"), ID,
                         DBNullToSomething(.Item("CAT1")), DBNullToSomething(.Item("CAT2")), DBNullToSomething(.Item("CAT3")), DBNullToSomething(.Item("CAT4")), DBNullToSomething(.Item("CAT5")),
                         sauxARTDEN, Cantidad, .Item("UP_DEF"), Precio, 0, .Item("MON"), .Item("TIPORECEPCION"))
                    'TxtNomArt.text()
                    Dim oUser As FSNServer.User = oServer.Get_Object(GetType(FSNServer.User))
                    Dim dr As DataRow = oUser.InfoCesta(oUsuario.Cod)
                    Dim dImp As Double
                    If dr.Item("IMPORTE") > 0 Then
                        dImp = CDbl(dr.Item("IMPORTE") * dr.Item("EQUIV"))
                    Else
                        dImp = 0
                    End If
                    Dim sDatosCesta As String = Peticion.ToString()
                    sDatosCesta = sDatosCesta & "##" & FSNLibrary.FormatNumber(dImp, oUsuario.NumberFormat) & " " & dr.Item("MON").ToString()
                    sDatosCesta = sDatosCesta & "##" & dr.Item("NUMARTICULOS").ToString()
                    Return sDatosCesta
                Else
                    Return Peticion.ToString("####")
                End If
            End With
        Catch ex As Exception
            Return Peticion.ToString() & "##" & ex.Message
        End Try
    End Function
    ''' <summary>
    ''' Procedimiento que se utiliza para añadir un artículo a la cesta desde la ventana de detalle
    ''' </summary>
    ''' <param name="IdArt">Id del artículo a añadir</param>
    ''' <param name="Cantidad">Cantidad a añadir a la cesta</param>
    ''' <param name="Precio">Precio del artículo, el parámetro únicamente se tiene en cuenta si el usuario puede modificar el precio</param>
    ''' <remarks></remarks>
    Private Sub pnlDetalleArticulo_AnyadirACesta(ByVal IdArt As Integer, ByVal Cantidad As Double, ByVal Precio As Double, ByVal Key As String) Handles pnlDetalleArticulo.AnyadirACesta
        With DsetArticulos.Tables(0).Rows.Find(IdArt)
            Dim PrecioCesta As Double = .Item("PRECIO_UC")
            If (.Item("TIPORECEPCION") = 0) Then
                If .Item("MODIF_PREC") = 1 And Me.FSNUser.ModificarPrecios Then
                    PrecioCesta = Precio / .Item("FC_DEF")
                End If
            Else
                PrecioCesta = Cantidad
                Cantidad = 1
            End If
            If Cantidad > 0 Then
                Dim oCArticulo As FSNServer.CArticulo = Me.FSNServer.Get_Object(GetType(FSNServer.CArticulo))
                oCArticulo.AnyadirACesta(Me.FSNUser.CodPersona, .Item("FC_DEF"), .Item("PROVE"), IdArt,
                     DBNullToSomething(.Item("CAT1")), DBNullToSomething(.Item("CAT2")), DBNullToSomething(.Item("CAT3")), DBNullToSomething(.Item("CAT4")), DBNullToSomething(.Item("CAT5")),
                     .Item("ART_DEN"), Cantidad, .Item("UP_DEF"), PrecioCesta, 0, .Item("MON"), .Item("TIPORECEPCION"))
                Master.CabControlCesta.Actualizar()
            End If
        End With
    End Sub
#End Region
#Region "Comparativa"
    ''' <summary>
    ''' Añade un artículo a la comparativa
    ''' </summary>
    ''' <param name="ImagenID">ID de cliente del control imagen</param>
    ''' <param name="ID">Id de la línea de catálogo</param>
    ''' <returns>Un string con el ID de cliente del control y el número de artículos de la comparativa, sino el mensaje de error</returns>
    ''' <remarks>Se le llama de forma asíncrona desde el código javascript de la página</remarks>
    <System.Web.Services.WebMethod(),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function AnyadirAComparativa(ByVal ImagenID As String, ByVal ID As Integer) As String
        Try
            Dim ArticulosComparar() As String = HttpContext.Current.Session("ArticulosComparar")
            If Not ArticulosComparar Is Nothing Then
                If Not ArticulosComparar.Contains(ID) Then
                    ReDim Preserve ArticulosComparar(ArticulosComparar.Count)
                    ArticulosComparar(ArticulosComparar.Count - 1) = ID
                End If
            Else
                ReDim Preserve ArticulosComparar(0)
                ArticulosComparar(0) = ID
            End If
            HttpContext.Current.Session("ArticulosComparar") = ArticulosComparar
            Return ImagenID & "##" & ArticulosComparar.Count
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
    ''' <summary>
    ''' Procedimiento que se utiliza para añadir un artículo a la comparativa de artículos, si no ha sido añadido anteriormente, tambien se añade uno al total de artículos añadidos, si cumple la condición anterior, y habilita la pestaña del menu para poder seleccionarla, si el total de artículos en la comparativa es mayor que uno.
    ''' </summary>
    ''' <param name="fila">La fila donde se encuentra el artículo clicado para añadir a la comparativa</param>
    ''' <param name="ID">Identificador del artículo</param>
    ''' <param name="Boton">El botón clicado</param>
    ''' <remarks>
    ''' Llamada desde: En el evento "Row Command" del GridView.
    ''' Tiempo máximo:0 seg</remarks>
    Protected Sub AnyadirAComparativa(ByVal fila As GridViewRow, ByVal ID As Integer, ByVal Boton As ImageButton)
        If Not ArticulosComparar Is Nothing Then
            If Not ArticulosComparar.Contains(ID) Then
                ReDim Preserve ArticulosComparar(ArticulosComparar.Count)
                ArticulosComparar(ArticulosComparar.Count - 1) = ID
            End If
        Else
            ReDim Preserve ArticulosComparar(0)
            ArticulosComparar(0) = ID
        End If


        Session("ArticulosComparar") = ArticulosComparar
        Boton.ImageUrl = VirtualPathUtility.GetDirectory(Boton.ImageUrl) & "/comparar_desactivado.gif"
        Boton.Enabled = False
        Boton.ToolTip = ""
        Master.CabTabComparativa.Text = Textos(89) & "(" & ArticulosComparar.Count & ")"
        Master.CabTabComparativa.Enabled = True
        Master.CabUpdPestanyasCatalogo.Update()
    End Sub
#End Region
#Region "Paneles de Favoritos"
    ''' <summary>
    ''' Procedimiento que se lanzará desde el botón de Añadir a favorito de la grid del catalogo
    ''' </summary>
    ''' <param name="IdArt">El id del articulo </param>
    ''' <param name="NumArt">la cantidad seleccionada de articulos</param>
    ''' <param name="Mon">la moneda del artículo</param>
    ''' <remarks>Llamada desde el botón de Añadir a favorito de la grid de artículos
    ''' Tiempo 0 sec.</remarks>
    Private Sub CargarFavoritos(ByVal IdArt As String, ByVal NumArt As Double, ByVal Mon As String, ByVal Den As String)
        VaciarVariablesFavoritos()
        CargarDatosArticulo(IdArt, NumArt, Mon, Den)
        Dim oOrdenesFavoritos As FSNServer.COrdenesFavoritos
        Dim dsetOrdenes As New DataSet
        oOrdenesFavoritos = Me.FSNServer.Get_Object(GetType(FSNServer.COrdenesFavoritos))
        dsetOrdenes = oOrdenesFavoritos.BuscarTodasOrdenes(FSNUser.Cod, Nothing, oArt.Prove, Mon)

        CargarPanelSelecFavoritos(dsetOrdenes)

    End Sub
    ''' <summary>
    ''' Carga los datos del artículo a añadir al pedido favorito, del propio pedido y sus adjuntos
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde:PageLoad
    ''' Tiempo máximo: 0,5 seg </remarks>
    Public Sub CargarDatosArticulo(ByVal IdArt As String, ByVal NumArt As Double, ByVal Mon As String, ByVal Den As String)
        Dim oOrdenesFav As FSNServer.COrdenesFavoritos = Me.FSNServer.Get_Object(GetType(FSNServer.COrdenesFavoritos))
        'Articulo
        If oArt Is Nothing Then
            oArt = Me.FSNServer.Get_Object(GetType(FSNServer.CArticulo))
            oArt.ID = IdArt
            oArt.CargarDatosArticuloFavorito(FSNUser.CodPersona, FSNUser.Idioma)
            Dim oUnidadPedido As CUnidadPedido = Nothing
            Dim sNumArt As String = CType(NumArt, String)
            If Not DBNullToSomething(oArt.UP) = Nothing Then
                oUnidadPedido = TodasLasUnidades.Item(Trim(oArt.UP))
                oUnidadPedido.AsignarFormatoaUnidadPedido(Me.Usuario.NumberFormat)
                If oUnidadPedido.NumeroDeDecimales Is Nothing Then
                    Dim decimales As Integer = NumeroDeDecimales(DBNullToStr(NumArt))
                    oUnidadPedido.UnitFormat.NumberDecimalDigits = decimales
                End If
                sNumArt = FSNLibrary.FormatNumber(NumArt, oUnidadPedido.UnitFormat)
            End If

            fsnlnkContactoProve.ContextKey = oArt.Prove

            oArt.Mon = Mon
            oArt.Den = Den
        End If

        ViewState("oArt") = oArt
        ViewState("NumArt") = NumArt
    End Sub
#Region "Nuevo Favorito"
    ''' <summary>
    ''' Carga las variables que luego utilizo durante la interacción con la página
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde:Page Load
    ''' Tiempo máximo: 0 seg
    ''' </remarks>
    Public Sub CargarVariablesFavoritos()
        If Not ViewState("oOrdenFav") Is Nothing Then
            oOrdenFav = ViewState("oOrdenFav")
        End If
        If Not ViewState("oArt") Is Nothing Then
            oArt = ViewState("oArt")
        End If
        miLineaActual = ViewState("LineaActual")
    End Sub
    ''' <summary>
    ''' Carga las variables que luego utilizo durante la interacción con la página
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde:Page Load
    ''' Tiempo máximo: 0 seg
    ''' </remarks>
    Public Sub VaciarVariablesFavoritos()
        ViewState("oOrdenFav") = Nothing
        ViewState("oAdjuntosArt") = Nothing
        ViewState("oAdjuntosPedido") = Nothing
        ViewState("oArt") = Nothing
        miLineaActual = 0
        oArt = Nothing
        oOrdenFav = Nothing

    End Sub
    ''' <summary>
    ''' Da formato al texto de un adjunto, dejando el nombre del adjunto y su extensión
    ''' </summary>
    ''' <param name="TextoCompleto">El nombre entero del adjunto con su ruta</param>
    ''' <returns>El nombre del adjunto con su extensión</returns>
    ''' <remarks>
    ''' Llamda Desde:BtnAceptarAdjun y BtnAceptarAdjun0
    ''' Tiempo máximo:0 seg</remarks>
    Public Function DarFormato(ByVal TextoCompleto As String) As String
        Dim arrayResultados() As String
        Dim resultado As String
        arrayResultados = Split(TextoCompleto, "/", -1, CompareMethod.Text)
        resultado = arrayResultados(arrayResultados.Length - 1)
        Return resultado
    End Function
#End Region
#Region "Seleccion de Favoritos"
    Private ReadOnly Property FavoritoPreseleccionado() As Integer
        Get
            If Not String.IsNullOrEmpty(Request.QueryString("FavId")) Then
                Return Request.QueryString("FavId")
            Else
                Return 0
            End If
        End Get
    End Property
    ''' <summary>
    ''' Carga el panel de seleccion de favoritos
    ''' </summary>
    ''' <param name="dsetOrdenes">el dataset que usaremos para cargar la combo con la lista de pedidos favoritos</param>
    ''' <remarks>Llamada desde CargarFavoritos
    ''' Tiempo maximo 0 sec</remarks>
    Private Sub CargarPanelSelecFavoritos(ByVal dsetOrdenes As DataSet)

        'Textos
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_SelecFavoritos
        LblAnyadirFav.Text = Textos(0)
        LblArticulo.Text = Textos(1)
        lblNomArticulo.Text = oArt.Den
        fsnlnkContactoProve.Text = Textos(3)
        BtnAceptarFav.Text = Textos(5)
        BtnCancelarFav.Text = Textos(6)

        LblNomProve.Text = oArt.proveden
        COD.Text = oArt.Prove

        If dsetOrdenes.Tables(0).Rows.Count > 0 Then
            RBFavoritos.Items(0).Text = Textos(8)
            RBFavoritos.Items(1).Text = Textos(7)
            RBFavoritos.Items(0).Selected = True
            RBFavoritos.Items(0).Enabled = True
            RBFavoritos.Items(1).Selected = False
            ListaFavoritos.ClearSelection()
            ListaFavoritos.Items.Clear()
            Dim indicefila As Integer = 0
            For Each fila As DataRow In dsetOrdenes.Tables(0).Rows
                ListaFavoritos.Items.Add(fila.Item(0)) 'ID
                ListaFavoritos.Items(indicefila).Text = fila.Item(2) 'DEN
                ListaFavoritos.Items(indicefila).Value = fila.Item(0) 'ID
                indicefila = indicefila + 1
            Next
            If FavoritoPreseleccionado <> 0 AndAlso ListaFavoritos.Items.FindByValue(FavoritoPreseleccionado.ToString) IsNot Nothing Then
                ListaFavoritos.Items.FindByValue(FavoritoPreseleccionado.ToString).Selected = True
            Else
                ListaFavoritos.Items(0).Selected = True
            End If

            RowAnyadirFavorito0.ColSpan = "1"
            RowAnyadirFavorito0.Align = "left"

            'Visibilidad
            ListaFavoritos.Visible = True
            RBFavoritos.Visible = True
            RowAnyadirFavorito.Visible = True
            RowAnyadirFavorito.Style("display") = "block"
            If RowAnyadirFavorito0.FindControl("mensaje") IsNot Nothing Then
                RowAnyadirFavorito0.FindControl("mensaje").Visible = False
            End If
        Else
            'Chequeamos el segundo elemento de RBFavoritos para que al aceptar nos envía a EmisionPedido para poder editar el nuevo favorito
            RBFavoritos.Items(1).Selected = True
            Dim oLiteral As New Literal
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_CatalogoWeb
            oLiteral.ID = "mensaje"
            oLiteral.Text = Textos(159) 'No existe ningún pedido favorito para este proveedor. ¿Desea crear uno nuevo?

            'Visibilidad
            oLiteral.Visible = True
            RBFavoritos.Visible = False
            ListaFavoritos.Visible = False
            RowAnyadirFavorito.Visible = False

            RowAnyadirFavorito0.Controls.Add(oLiteral)
            RowAnyadirFavorito.Style("display") = "none"
            RowAnyadirFavorito0.ColSpan = "2"
            RowAnyadirFavorito0.Align = "center"
        End If

        upSelecFavoritos.Update()
        mpepanSelecFavoritos.Show()
    End Sub
    ''' <summary>
    ''' Acepta la opción que haya seleccionado el usuario para añadir un nuevo pedido favorito, o a uno existente, abriendo la página correspondiente a su elección
    ''' </summary>
    ''' <param name="sender">El propio botón de aceptar</param>
    ''' <param name="e">El evento click del botón</param>
    ''' <remarks>
    ''' Llamada desde: La página, al clicar el botón aceptar
    ''' Tiempo máximo: 0,2 seg</remarks>
    Protected Sub BtnAceptarFav_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnAceptarFav.Click
        If RBFavoritos.Items(1).Selected Then
            'CargarPanelNuevoFavorito()
            Dim oFavNuevo(3) As Object
            oFavNuevo(0) = oArt 'CArticulo
            oFavNuevo(1) = ViewState("NumArt") 'Cantidad (Double)

            Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
            Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
            Dim Precio As Double = oArt.PrecioUC

            If oArt.iTipoRecepcion = 0 Then
                If oArt.ModificarPrec = 1 And Me.Usuario.ModificarPrecios Then
                    Precio = oArt.PrecioUC
                    Precio = Precio / oArt.FC
                End If
            Else
                Precio = oArt.PrecioUC
            End If
            If ViewState("NumArt") > 0 Then
                Dim oCArticulo As FSNServer.CArticulo = oServer.Get_Object(GetType(FSNServer.CArticulo))
                Dim idPedido As Integer
                idPedido = oCArticulo.AnyadirACestaFav(Me.Usuario.CodPersona, oArt.FC, oArt.Prove, oArt.ID,
                     DBNullToInteger(oArt.Cat1), DBNullToInteger(oArt.Cat2), DBNullToInteger(oArt.Cat3), DBNullToInteger(oArt.Cat4), DBNullToInteger(oArt.Cat5),
                     oArt.Den, DBNullToStr(ViewState("NumArt")), oArt.UP, Precio, oArt.Tipo, DBNullToStr(oArt.Mon), oArt.iTipoRecepcion)

                HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaEP") & "EmisionPedido.aspx?ID=" & idPedido.ToString() & "&NuevoFavorito=True")
            Else

            End If
        Else
            'AÑADIR ARTICULO A FAVORITO
            If oOrdenFav Is Nothing Then
                oOrdenFav = Me.FSNServer.Get_Object(GetType(FSNServer.COrdenFavoritos))
            End If
            If oOrdenFav.Lineas Is Nothing Then
                oOrdenFav.Lineas = Me.FSNServer.Get_Object(GetType(FSNServer.CLineasFavoritos))
            End If

            Dim oCampo1 As FSNServer.CCampo = Me.FSNServer.Get_Object(GetType(FSNServer.CCampo))
            Dim oCampo2 As FSNServer.CCampo = Me.FSNServer.Get_Object(GetType(FSNServer.CCampo))
            If Not oArt.Campo1 Is Nothing Then
                oCampo1.ID = oArt.Campo1.ID
                oCampo1.Idioma = FSNUser.Idioma
                oCampo1.Den = oArt.Campo1.Den
                oCampo1.Num = oArt.Campo1.Num
                oCampo1.Obligatorio = oArt.Campo1.Obligatorio
            End If
            If Not oArt.Campo2 Is Nothing Then
                oCampo2.ID = oArt.Campo2.ID
                oCampo2.Idioma = FSNUser.Idioma
                oCampo2.Den = oArt.Campo2.Den
                oCampo2.Num = oArt.Campo2.Num
                oCampo2.Obligatorio = oArt.Campo2.Obligatorio
            End If
            'Actualmente CantAdjudicada no es un valor relevante para los favoritos por lo que lo pasamos a cero
            Dim iCantAdj = 0
            'El Id de la orden es la única información de ella que necesitamos para añadir el artículo
            oOrdenFav.ID = ListaFavoritos.SelectedValue
            Dim oAdjuntosArt As FSNServer.CAdjuntos = Me.FSNServer.Get_Object(GetType(FSNServer.CAdjuntos))
            oOrdenFav.Lineas.Add(0, FSNUser.Cod, oArt.Cod, oArt.GMN1, oArt.GMN2, oArt.GMN3, oArt.GMN4, DBNullToStr(oArt.Prove), DBNullToStr(oArt.Den), oArt.CodExt, DBNullToStr(oArt.Dest), String.Empty, oArt.UP, String.Empty, oArt.PrecioUC, ViewState("NumArt"), iCantAdj, CStr(oArt.FC), CDbl(ViewState("NumArt")) * oArt.PrecioUC * oArt.FC, Nothing, Nothing, oArt.Cat1, oArt.Cat1, oArt.Cat2, oArt.Cat2, oArt.Cat3, oArt.Cat3, oArt.Cat4, oArt.Cat4, oArt.Cat5, oArt.Cat5, String.Empty, oArt.Categoria, oArt.RamaCategoria, String.Empty, Nothing, Nothing, DBNullToSomething(oArt.Item), oArt.ID, oAdjuntosArt, oCampo1, oCampo2, String.Empty, String.Empty, oArt.CantMinimaPedido, oArt.ModificarPrec, oArt.Generico, "")
            Dim Resul As Integer()
            Resul = oOrdenFav.AnyadirLineas()

            ViewState("oOrdenFav") = oOrdenFav

            ConfigurarMensajeFavoritoAnyadido(Resul(0))
            upConfirmacionFavoritoAnyadido2.Update()
            mpepanConfirmacionFavoritoAnyadido.Show()

        End If

    End Sub
#End Region
#Region "Anyadir Favorito"
    ''' <summary>
    ''' carga los textos del panel de Añadir a favorito
    ''' </summary>
    ''' <param name="resultado">Valor que nos indica si el añadido de la linea a la orden ha sido existoso o no, en función de lo cual mostraremos un mensaje u otro.
    ''' 0->linea añadida correctamente
    ''' 1->linea no añadida
    ''' </param>
    ''' <remarks>Llamada desde CargarPanelAnyadirFavorito
    ''' tiempo maximo 0 sec</remarks>
    Private Sub ConfigurarMensajeFavoritoAnyadido(ByVal resultado As Integer)

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_CatalogoWeb
        If resultado = 0 Then
            LblMensajeFavoritoAnyadido.Text = Textos(160)
        Else
            LblMensajeFavoritoAnyadido.Text = Textos(163)
        End If

        BtnIrAFavorito.Text = Textos(161)
        BtnIrAFavorito.ToolTip = Textos(161)
        BtnSeguirEnCatalogo.Text = Textos(162)
        BtnSeguirEnCatalogo.ToolTip = Textos(162)

        BtnIrAFavorito.CommandArgument = ListaFavoritos.SelectedValue
    End Sub
    ''' <summary>
    ''' Redirigimos la pantalla a EmisionPedido para mostrar el favorito
    ''' </summary>
    ''' <param name="sender">El botón BtnIrAFavorito del panel de favorito añadido</param>
    ''' <param name="e">El evento click del boton</param>
    ''' <remarks>
    ''' Llamada desde: El botón BtnIrAFavorito del panel de favorito añadido
    ''' Tiempo máximo:0,3 seg</remarks>
    Private Sub BtnIrAFavorito_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnIrAFavorito.Click
        'Dado que hemos añadido un artículo a un pedido favorito y que si ya existe guardado en caché ese pedido, no nos mostraría el articulo, borramos la sesión del pedido favorito.
        Dim DsetFavoritoEmitir As DataSet = TryCast(HttpContext.Current.Cache("DsetFavoritoEmitir_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString), DataSet)
        If DsetFavoritoEmitir IsNot Nothing AndAlso CType(DsetFavoritoEmitir, DataSet).Tables("ORDENESFAVORITOS") IsNot Nothing _
        AndAlso CType(DsetFavoritoEmitir, DataSet).Tables("ORDENESFAVORITOS").Rows.Count > 0 AndAlso CType(DsetFavoritoEmitir, DataSet).Tables("ORDENESFAVORITOS").Rows(0).Item("ID") = CType(sender, FSNWebControls.FSNButton).CommandArgument Then
            HttpContext.Current.Cache.Remove("DsetFavoritoEmitir_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
        End If
        Response.Redirect(ConfigurationManager.AppSettings("rutaEP") & "EmisionPedido.aspx?DesdeFavorito=True&favNuevo=False&ID=" & CType(sender, FSNWebControls.FSNButton).CommandArgument)
        'Response.Redirect(ConfigurationManager.AppSettings("rutaEP") & "Favoritos.aspx")
    End Sub
#End Region
#End Region
#Region "Tooltips"
    ''' <summary>
    ''' Evento que se genera cuando se pasa el ratón por encima de la imagen de comentario de adjunto, devolviendo el valor del mismo en forma de tooltip
    ''' </summary>
    ''' <param name="contextKey">El identificador del archivo adjunto</param>
    ''' <returns>Un texto con el valor del comentario del archivo adjunto</returns>
    ''' <remarks>
    ''' Llamada desde: Automática, siempre que se pase el ratón por encima de la imagen con los comentarios de los archivos adjuntos
    ''' Tiempo máximo: 0 seg</remarks>
    <System.Web.Services.WebMethodAttribute(),
    System.Web.Script.Services.ScriptMethodAttribute()>
    Public Shared Function ComentarioAdj(ByVal contextKey As String) As String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim context As String() = contextKey.Split("#")
        Dim oLin As FSNServer.CLineaFavoritos = oServer.Get_Object(GetType(FSNServer.CLineaFavoritos))
        oLin.ID = context(0)
        oLin.Usuario = oUsuario.Cod
        oLin.CargarAdjuntos(True)
        Return oLin.Adjuntos.Item(context(1)).Comentario
    End Function
    ''' <summary>
    ''' Evento que devuelve un texto pasado como tooltip
    ''' </summary>
    ''' <param name="contextKey">El texto que se quiere devolver</param>
    ''' <returns>El mismo texto que se le pasa como parámetro</returns>
    ''' <remarks>
    ''' Llamada desde: Automática, cada vez que se pasa el ratón por encima del texto que se quiere devolver
    ''' Tiempo máximo: 0 seg</remarks>
    <System.Web.Services.WebMethodAttribute(),
    System.Web.Script.Services.ScriptMethodAttribute()>
    Public Shared Function DevolverTexto(ByVal contextKey As String) As String
        Return contextKey
    End Function
    ''' <summary>
    ''' Evento que se genera cuando se pasa el ratón por encima de la imagen de comentario de especificaciones, devolviendo el valor del mismo en forma de tooltip
    ''' </summary>
    ''' <param name="contextKey">[ID de la línea de catálogo]#[ID del archivo de especificaciones]</param>
    ''' <returns>El comentario del archivo de especificaciones</returns>
    <System.Web.Services.WebMethodAttribute(),
    System.Web.Script.Services.ScriptMethodAttribute()>
    Public Shared Function ComentarioEspecificacion(ByVal contextKey As String) As String
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        Dim context As String() = contextKey.Split("#")
        Dim oArt As CArticulo = oServer.Get_Object(GetType(CArticulo))
        oArt.ID = context(0)
        oArt.CargarEspecificaciones()
        Return oArt.Especificaciones.Item(context(1)).Comentario
    End Function
#End Region
#Region "ComboBoxes"
    ''' <summary>
    ''' Procedimiento que rellena el combo de Poblaciones
    ''' </summary>
    ''' <param name="sCodPais">Código del país del que queremos recuperar las poblaciones</param>
    ''' <param name="sCodProv">Código de la Provincia de la que queremos recuperar las poblaciones</param>
    ''' <remarks>Llamada desde RellenarCmbBoxProvincia. Tiempo máximo inferior 1 seg</remarks>
    Private Sub RellenarCmbBoxPoblacion(ByVal sCodPais As String, ByVal sCodProv As String)

        Dim tablaProveedores As DataTable

        If Not _DsetProveedores Is Nothing AndAlso _DsetProveedores.Tables.Count > 0 AndAlso _DsetProveedores.Tables(0).Rows.Count > 0 Then

            tablaProveedores = _DsetProveedores.Tables(0)

            Dim NoBuscar As Boolean = False
            Dim sbusquedaPob = From Datos In tablaProveedores
                               Where IIf(String.IsNullOrEmpty(sCodPais), NoBuscar, DBNullToStr(Datos.Item("Pai")) = sCodPais) _
                                   AndAlso IIf(String.IsNullOrEmpty(sCodProv), NoBuscar, DBNullToStr(Datos.Item("Provi")) = sCodProv) _
                                   AndAlso Not String.IsNullOrEmpty(DBNullToStr(Datos.Item("POB")))
                               Select prove = Datos.Item("Cod").ToString, CODPAI = Datos.Item("PAI").ToString, CODPROVI = Datos.Item("PROVI").ToString, Pob = Datos.Item("Pob").ToString
                               Group By CODPAI, CODPROVI, Pob, prove
                               Into Any()
                               Order By Pob Ascending
                               Distinct.Take(Integer.MaxValue).ToArray

            If sbusquedaPob.Any Then
                Dim Texto As String = ""
                Dim Cuenta As Integer = 0
                Dim ListItem As New System.Web.UI.WebControls.ListItem
                ListItem.Text = ""
                ListItem.Value = ""
                CmboBoxPobProveBusq.Items.Clear()
                CmboBoxPobProveBusq.Items.Add(ListItem)
                For i As Integer = 0 To UBound(sbusquedaPob)
                    'If Not String.IsNullOrEmpty(DBNullToStr(sbusquedaPob(i).Pob)) Then
                    Cuenta = 0
                    ListItem = New System.Web.UI.WebControls.ListItem
                    ListItem.Text = DBNullToStr(sbusquedaPob(i).Pob)
                    'ListItem.Value = sbusquedaPob(i).CODPROVI & "###" & sbusquedaPob(i).Pob
                    ListItem.Value = sbusquedaPob(i).Pob
                    If Not String.IsNullOrEmpty(sbusquedaPob(i).Pob) AndAlso CmbBoxProviProveBusq.Items.FindByValue(sbusquedaPob(i).CODPROVI & "###" & sbusquedaPob(i).Pob) Is Nothing Then
                        Texto = sbusquedaPob(i).Pob
                        For j As Integer = 0 To UBound(sbusquedaPob)
                            If DBNullToStr(sbusquedaPob(j).Pob) = DBNullToStr(sbusquedaPob(i).Pob) Then
                                Cuenta = Cuenta + 1
                            End If
                        Next
                        ListItem.Text = ListItem.Text & "(" & Cuenta.ToString & ")"
                        CmboBoxPobProveBusq.Items.Add(ListItem)
                    End If
                    'End If
                Next

                Dim PoblacionEncontrada As Boolean = False
                Dim PobElegida As String = CmboBoxPobProveBusq.SelectedValue
                If String.IsNullOrEmpty(PobElegida) Then
                    CmboBoxPobProveBusq.Items(0).Selected = True
                Else
                    For Each item As ListItem In CmboBoxPobProveBusq.Items
                        If item.Value = PobElegida Then
                            item.Selected = True
                            PoblacionEncontrada = True
                            Exit For
                        End If
                    Next
                    If Not PoblacionEncontrada Then
                        CmboBoxPobProveBusq.Items(0).Selected = True
                    End If
                End If

                Select Case CmboBoxPobProveBusq.Items.Count
                    Case 0, 1
                        Dim mensajeSeleccionar As Boolean = False
                        If String.IsNullOrEmpty(sCodPais) Then
                            mensajeSeleccionar = True
                        End If
                        Dim bOcultarPaises As Boolean = False
                        Dim bOcultarProvincias As Boolean = False
                        Dim bOcultarPoblaciones As Boolean = True
                        OcultarCombos(bOcultarPaises, bOcultarProvincias, bOcultarPoblaciones, mensajeSeleccionar)
                    Case Else
                        Dim bMostrarPaises As Boolean = False
                        Dim bMostrarProvincias As Boolean = False
                        Dim bMostrarPoblaciones As Boolean = True
                        MostrarCombos(bMostrarPaises, bMostrarProvincias, bMostrarPoblaciones)
                End Select

            Else
                Dim mensajeSeleccionar As Boolean = False
                If String.IsNullOrEmpty(sCodPais) Or String.IsNullOrEmpty(sCodProv) Then
                    mensajeSeleccionar = True
                End If
                Dim bOcultarPaises As Boolean = False
                Dim bOcultarProvincias As Boolean = False
                Dim bOcultarPoblaciones As Boolean = True
                OcultarCombos(bOcultarPaises, bOcultarProvincias, bOcultarPoblaciones, mensajeSeleccionar)
            End If
        Else
            Dim mensajeSeleccionar As Boolean = False
            If String.IsNullOrEmpty(sCodPais) Or String.IsNullOrEmpty(sCodProv) Then
                mensajeSeleccionar = True
            End If
            Dim bOcultarPaises As Boolean = False
            Dim bOcultarProvincias As Boolean = False
            Dim bOcultarPoblaciones As Boolean = True
            OcultarCombos(bOcultarPaises, bOcultarProvincias, bOcultarPoblaciones, mensajeSeleccionar)
        End If
    End Sub
    ''' <summary>
    ''' Procedimiento que rellena el combo de Provincias
    ''' Si sólo hay una provincia, llama al método que rellena poblaciones.
    ''' </summary>
    ''' <param name="sCodPais">Código del país del que queremos recuperar las provincias</param>
    ''' <remarks>Llamada desde RellenarCmbBoxPais, CmbBoxProviProveBusq_SelectedIndexChanged. Tiempo máximo inferior 1 seg</remarks>
    Private Sub RellenarCmbBoxProvincia(ByVal sCodPais As String)

        Dim tablaProveedores As DataTable

        If Not _DsetProveedores Is Nothing AndAlso _DsetProveedores.Tables.Count > 0 AndAlso _DsetProveedores.Tables(0).Rows.Count > 0 Then

            tablaProveedores = _DsetProveedores.Tables(0)

            Dim NoBuscar As Boolean = False
            Dim sbusquedaProvi = From Datos In tablaProveedores
                                 Where IIf(String.IsNullOrEmpty(sCodPais), NoBuscar, DBNullToStr(Datos.Item("Pai")) = sCodPais) _
                                    AndAlso Not String.IsNullOrEmpty(DBNullToStr(Datos.Item("PROVI")))
                                 Select prove = Datos.Item("Cod").ToString, CODPAI = Datos.Item("PAI").ToString, CODPROVI = Datos.Item("PROVI").ToString, DENPROVI = Datos.Item("PROVIDEN").ToString
                                 Order By DBNullToStr(DENPROVI) Ascending
                                 Group By CODPAI, CODPROVI, DENPROVI, prove
                                 Into Any()
                                 Distinct.Take(Integer.MaxValue).ToArray

            If sbusquedaProvi.Any Then
                Dim Texto As String = ""
                Dim Cuenta As Integer = 0
                Dim ListItem As New System.Web.UI.WebControls.ListItem
                ListItem.Text = ""
                ListItem.Value = ""
                CmbBoxProviProveBusq.Items.Clear()
                CmbBoxProviProveBusq.Items.Add(ListItem)
                For i As Integer = 0 To UBound(sbusquedaProvi)
                    'If Not String.IsNullOrEmpty(DBNullToStr(sbusquedaProvi(i).CODPROVI)) Then
                    Cuenta = 0
                    ListItem = New System.Web.UI.WebControls.ListItem
                    ListItem.Text = DBNullToStr(sbusquedaProvi(i).DENPROVI)
                    ListItem.Value = sbusquedaProvi(i).CODPROVI
                    If Not String.IsNullOrEmpty(sbusquedaProvi(i).CODPROVI) AndAlso CmbBoxProviProveBusq.Items.FindByValue(sbusquedaProvi(i).CODPROVI) Is Nothing Then
                        Texto = sbusquedaProvi(i).CODPROVI & " - " & sbusquedaProvi(i).DENPROVI
                        For j As Integer = 0 To UBound(sbusquedaProvi)
                            If DBNullToStr(sbusquedaProvi(j).CODPROVI) = DBNullToStr(sbusquedaProvi(i).CODPROVI) AndAlso DBNullToStr(sbusquedaProvi(j).DENPROVI) = DBNullToStr(sbusquedaProvi(i).DENPROVI) Then
                                Cuenta = Cuenta + 1
                            End If
                        Next
                        ListItem.Text = ListItem.Text & "(" & Cuenta.ToString & ")"
                        CmbBoxProviProveBusq.Items.Add(ListItem)
                    End If
                    'End If
                Next

                Dim ProviElegida As String = CmbBoxProviProveBusq.SelectedValue
                Dim ProviEncontrada As Boolean = False
                If Not String.IsNullOrEmpty(ProviElegida) Then
                    For Each item As ListItem In CmbBoxProviProveBusq.Items
                        If item.Value = ProviElegida Then
                            item.Selected = True
                            ProviEncontrada = True
                            Exit For
                        End If
                    Next
                    If Not ProviEncontrada Then
                        CmbBoxProviProveBusq.Items(0).Selected = True
                    End If
                Else
                    CmbBoxProviProveBusq.Items(0).Selected = True
                End If

                Select Case CmbBoxProviProveBusq.Items.Count
                    Case 0, 1
                        Dim bOcultarPaises As Boolean = False
                        Dim bOcultarProvincias As Boolean = True
                        Dim bOcultarPoblaciones As Boolean = True
                        OcultarCombos(bOcultarPaises, bOcultarProvincias, bOcultarPoblaciones)

                    Case Else
                        lblPobProveBusqSelec.Text = Textos(150)
                        lblPobProveBusqSelec.Visible = True
                        CmboBoxPobProveBusq.Visible = False
                        If Not CmboBoxPobProveBusq.SelectedItem Is Nothing Then
                            CmboBoxPobProveBusq.SelectedItem.Selected = False
                        End If

                        Dim bMostrarPaises As Boolean = False
                        Dim bMostrarProvincias As Boolean = True
                        Dim bMostrarPoblaciones As Boolean = False
                        MostrarCombos(bMostrarPaises, bMostrarProvincias, bMostrarPoblaciones)

                End Select

            Else
                Dim mensajeSeleccionar As Boolean = False
                If String.IsNullOrEmpty(sCodPais) Then
                    mensajeSeleccionar = True
                End If
                Dim bOcultarPaises As Boolean = False
                Dim bOcultarProvincias As Boolean = True
                Dim bOcultarPoblaciones As Boolean = True
                OcultarCombos(bOcultarPaises, bOcultarProvincias, bOcultarPoblaciones, mensajeSeleccionar)

                'NO MIRAMOS SI HAY POBLACIONES SIN PROVINCIA
                'Dim sCodProv As String = String.Empty
                'RellenarCmbBoxPoblacion(sCodPais, sCodProv)
            End If

        Else
            Dim mensajeSeleccionar As Boolean = False
            If String.IsNullOrEmpty(sCodPais) Then
                mensajeSeleccionar = True
            End If
            Dim bOcultarPaises As Boolean = False
            Dim bOcultarProvincias As Boolean = True
            Dim bOcultarPoblaciones As Boolean = True
            OcultarCombos(bOcultarPaises, bOcultarProvincias, bOcultarPoblaciones, mensajeSeleccionar)
        End If
    End Sub
    ''' <summary>
    ''' Procedimiento que rellena el combo de país
    ''' Si sólo hay un país, llama al método para rellenar provincias.
    ''' </summary>
    ''' <remarks>Llamada desde btnProves_click, CmbBoxPaiProveBusq_SelectedIndexChanged. Tiempo máximo inferior 1 seg</remarks>
    Private Sub RellenarCmbBoxPais()

        If Not _DsetProveedores Is Nothing AndAlso _DsetProveedores.Tables.Count > 0 AndAlso _DsetProveedores.Tables(0).Rows.Count > 0 Then
            Dim tablaProveedores As DataTable
            Dim Texto As String = ""
            Dim Cuenta As Integer = 0
            Dim ListItem As New System.Web.UI.WebControls.ListItem
            ListItem.Text = ""
            ListItem.Value = ""
            CmbBoxPaiProveBusq.Items.Clear()

            tablaProveedores = _DsetProveedores.Tables(0)

            Dim sbusquedaPai = From Datos In tablaProveedores
                               Where Not String.IsNullOrEmpty(DBNullToStr(Datos.Item("PAI")))
                               Select prove = Datos.Item("Cod").ToString, CODPAI = Datos.Item("PAI").ToString, DENPAI = Datos.Item("PAIDEN").ToString
                               Order By DBNullToStr(DENPAI) Ascending
                               Group By CODPAI, DENPAI, prove
                               Into Count()
                               Distinct.Take(Integer.MaxValue).ToArray

            If sbusquedaPai.Any Then
                Dim resul As String()
                ReDim resul(UBound(sbusquedaPai))
                CmbBoxPaiProveBusq.Items.Add(ListItem)
                For i As Integer = 0 To UBound(sbusquedaPai)
                    'If Not String.IsNullOrEmpty(DBNullToStr(sbusquedaPai(i).CODPAI)) Then
                    Cuenta = 0
                    ListItem = New System.Web.UI.WebControls.ListItem
                    ListItem.Text = DBNullToStr(sbusquedaPai(i).DENPAI)
                    ListItem.Value = sbusquedaPai(i).CODPAI
                    If Not String.IsNullOrEmpty(sbusquedaPai(i).CODPAI) AndAlso CmbBoxPaiProveBusq.Items.FindByValue(sbusquedaPai(i).CODPAI) Is Nothing Then
                        Texto = sbusquedaPai(i).DENPAI
                        For j As Integer = 0 To UBound(sbusquedaPai)
                            If DBNullToStr(sbusquedaPai(j).CODPAI) = DBNullToStr(sbusquedaPai(i).CODPAI) AndAlso DBNullToStr(sbusquedaPai(j).DENPAI) = DBNullToStr(sbusquedaPai(i).DENPAI) Then
                                Cuenta = Cuenta + 1
                            End If
                        Next
                        ListItem.Text = ListItem.Text & "(" & Cuenta.ToString & ")"
                        CmbBoxPaiProveBusq.Items.Add(ListItem)
                    End If
                    'End If
                Next

                Select Case CmbBoxPaiProveBusq.Items.Count

                    Case 0, 1
                        Dim bOcultarPaises As Boolean = True
                        Dim bOcultarProvincias As Boolean = True
                        Dim bOcultarPoblaciones As Boolean = True
                        OcultarCombos(bOcultarPaises, bOcultarProvincias, bOcultarPoblaciones)

                        'NO Miramos SI HAY PROVINCIAS SIN PAÍS
                        'RellenarCmbBoxProvincia(String.Empty)

                    Case Else
                        Dim bMostrarPaises As Boolean = True
                        Dim bMostrarProvincias As Boolean = False
                        Dim bMostrarPoblaciones As Boolean = False
                        MostrarCombos(bMostrarPaises, bMostrarProvincias, bMostrarPoblaciones)

                        LblProviProveBusqSelec.Text = Textos(151)
                        LblProviProveBusqSelec.Visible = True
                        CmbBoxProviProveBusq.Visible = False
                        If Not CmbBoxProviProveBusq.SelectedItem Is Nothing Then
                            CmbBoxProviProveBusq.SelectedItem.Selected = False
                        End If

                        lblPobProveBusqSelec.Text = Textos(150)
                        lblPobProveBusqSelec.Visible = True
                        CmboBoxPobProveBusq.Visible = False
                        If Not CmboBoxPobProveBusq.SelectedItem Is Nothing Then
                            CmboBoxPobProveBusq.SelectedItem.Selected = False
                        End If
                End Select

            Else
                Dim bOcultarPaises As Boolean = True
                Dim bOcultarProvincias As Boolean = True
                Dim bOcultarPoblaciones As Boolean = True
                OcultarCombos(bOcultarPaises, bOcultarProvincias, bOcultarPoblaciones)
            End If
        Else 'No hay datos de proveedores por lo que ponemos que no hay datos de pais, prov y poblacion
            Dim bOcultarPaises As Boolean = True
            Dim bOcultarProvincias As Boolean = True
            Dim bOcultarPoblaciones As Boolean = True
            OcultarCombos(bOcultarPaises, bOcultarProvincias, bOcultarPoblaciones)
        End If
    End Sub
    ''' <summary>
    ''' Método que según el valor de los parámetros de entrada
    ''' reemplaza el combo correspondiente por un mensaje que informa de que no hay datos
    ''' </summary>
    ''' <param name="OcultarPaises">True->No hay datos de país, mostramos mensaje</param>
    ''' <param name="OcultarProvincias">True->No hay datos de provincias, mostramos mensaje</param>
    ''' <param name="OcultarPoblaciones">True->No hay datos de población, mostramos mensaje</param>
    ''' <remarks>Llamado desde RellenarCmbBoxPais, RellenarCmbBoxProvincia, RellenarCmbBoxPoblacion. Tiempo Max. inf 1 seg</remarks>
    Private Sub OcultarCombos(ByVal OcultarPaises As Boolean, ByVal OcultarProvincias As Boolean, ByVal OcultarPoblaciones As Boolean, Optional ByVal mensajeSeleccionar As Boolean = False)
        If OcultarPaises Then
            LblPaiProveBusqSelec.Visible = True
            LblPaiProveBusqSelec.Text = Textos(154)
            CmbBoxPaiProveBusq.Visible = False
            If Not CmbBoxPaiProveBusq.SelectedItem Is Nothing Then
                CmbBoxPaiProveBusq.SelectedItem.Selected = False
            End If
        End If
        If OcultarProvincias Then
            LblProviProveBusqSelec.Visible = True
            If mensajeSeleccionar Then
                LblProviProveBusqSelec.Text = Textos(151)
            Else
                LblProviProveBusqSelec.Text = Textos(155)
            End If
            CmbBoxProviProveBusq.Visible = False
            If Not CmbBoxProviProveBusq.SelectedItem Is Nothing Then
                CmbBoxProviProveBusq.SelectedItem.Selected = False
            End If
        End If
        If OcultarPoblaciones Then
            lblPobProveBusqSelec.Visible = True
            If mensajeSeleccionar Then
                lblPobProveBusqSelec.Text = Textos(150)
            Else
                lblPobProveBusqSelec.Text = Textos(156)
            End If
            CmboBoxPobProveBusq.Visible = False
            If Not CmboBoxPobProveBusq.SelectedItem Is Nothing Then
                CmboBoxPobProveBusq.SelectedItem.Selected = False
            End If
        End If
    End Sub
    ''' <summary>
    ''' Método que si recibe un valor true para la varaible de pais, provincia o poblacion, muestra el combo correspondiente.
    ''' En caso de recibir un valor false, no hace nada.
    ''' lo muestra
    ''' </summary>
    ''' <param name="MostrarPaises">True->hay datos de país, mostramos combo</param>
    ''' <param name="MostrarProvincias">True->hay datos de provincias, mostramos combo</param>
    ''' <param name="MostrarPoblaciones">True->hay datos de población, mostramos combo</param>
    ''' <remarks>Llamado desde RellenarCmbBoxPais, RellenarCmbBoxProvincia, RellenarCmbBoxPoblacion. Tiempo Max. inf 1 seg</remarks>
    Private Sub MostrarCombos(ByVal MostrarPaises As Boolean, ByVal MostrarProvincias As Boolean, ByVal MostrarPoblaciones As Boolean)
        If MostrarPaises Then
            LblPaiProveBusqSelec.Text = String.Empty
            LblPaiProveBusqSelec.Visible = False
            CmbBoxPaiProveBusq.Visible = True
        End If
        If MostrarProvincias Then
            LblProviProveBusqSelec.Text = String.Empty
            LblProviProveBusqSelec.Visible = False
            CmbBoxProviProveBusq.Visible = True
        End If
        If MostrarPoblaciones Then
            lblPobProveBusqSelec.Text = String.Empty
            lblPobProveBusqSelec.Visible = False
            CmboBoxPobProveBusq.Visible = True
        End If
    End Sub
#End Region
#Region "Unidades"
    ''' <summary>
    ''' Propiedad que recupera el conjunto de las unidades de Pedido para su posterior uso en la página
    ''' </summary>
    Private ReadOnly Property TodasLasUnidades() As CUnidadesPedido
        Get
            Dim oUnidadesPedido As CUnidadesPedido = Me.FSNServer.Get_Object(GetType(FSNServer.CUnidadesPedido))
            If HttpContext.Current.Cache("TodasLasUnidades" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                oUnidadesPedido.CargarTodasLasUnidades(Me.Usuario.Idioma)
                Me.InsertarEnCache("TodasLasUnidades" & Me.Usuario.Idioma.ToString(), oUnidadesPedido)
            Else
                oUnidadesPedido = HttpContext.Current.Cache("TodasLasUnidades" & Me.Usuario.Idioma.ToString())
            End If
            Return oUnidadesPedido
        End Get
    End Property
    ''' Revisado por: blp. Fecha:23/01/2012
    ''' <summary>
    ''' Método que recupera el número de decimales para una unidad de pedido dada y configura la presentación de los datos en los controles webnumericEdit
    ''' </summary>
    ''' <param name="unidad">Código de la unidad de pedido</param>
    ''' <param name="oFSNTextBox">Control a configurar</param>
    ''' <param name="valorMinimo">Valor minimo del control</param>
    ''' <param name="valorMaximo">Valor máximo del control</param>
    ''' <remarks>Llamada desde: la página, allí donde se configure un control webNumericEdit. Tiempo máximo: inferior a 0,3</remarks>
    Private Sub configurarDecimales(ByVal unidad As Object, ByRef oFSNTextBox As FSNWebControls.FSNTextBox, ByRef valorMinimo As Nullable(Of Double), ByRef valorMaximo As Nullable(Of Double))
        Dim oUnidadPedido As FSNServer.CUnidadPedido = Nothing
		If Not DBNullToSomething(unidad) = Nothing Then
			oUnidadPedido = FSNServer.Get_Object(GetType(FSNServer.CUnidadPedido))
			unidad = Trim(unidad.ToString)
			oUnidadPedido = TodasLasUnidades.Item(unidad)
			oUnidadPedido.AsignarFormatoaUnidadPedido(Me.Usuario.NumberFormat)
			oFSNTextBox.Attributes.Add("UNIDAD", unidad)
		Else
			oFSNTextBox.Attributes.Add("UNIDAD", "")
        End If

        Dim vprecisionfmt As Integer
        Dim minimo As String = IIf(valorMinimo Is Nothing, "null", valorMinimo)
        Dim maximo As String = IIf(valorMaximo Is Nothing, "null", valorMaximo)
        Dim vdecimalfmt As String = Me.Usuario.NumberFormat.NumberDecimalSeparator
        Dim vthousanfmt As String = Me.Usuario.NumberFormat.NumberGroupSeparator

        Dim oFSNTextBox_DblValue As Label = oFSNTextBox.NamingContainer.FindControl(oFSNTextBox.ID & "_DblValue")
        Dim oFSNTextBoxArrows As FSNWebControls.FSNUpDownArrows = oFSNTextBox.NamingContainer.FindControl(oFSNTextBox.ID & "Arrows")

        If Not oUnidadPedido Is Nothing Then
            Dim unitFormatTemp As System.Globalization.NumberFormatInfo = oUnidadPedido.UnitFormat.Clone
            If oUnidadPedido.NumeroDeDecimales Is Nothing Then
                'Cuando el número de decimales de la unidad no está definido, teóricamente puede ser cualquiera pero
                'vamos a poner un límite de 99. Si fuese necesario cambiar esto, habrá que revisar su repercusión en jsTextBoxFormat.js
                'pues allí se controla el valor 99
                vprecisionfmt = 99
                oUnidadPedido.UnitFormat.NumberDecimalDigits = 0
                Dim num As Double = strToDbl(oFSNTextBox_DblValue.Text)
                If Not Double.IsNaN(num) AndAlso (num <> CInt(num)) Then
                    num = num - CInt(num)
                    Dim numDecimales As Integer = 0
                    If num.ToString.IndexOf(".") > 0 Then
                        numDecimales = num.ToString.Substring(num.ToString.IndexOf(".") + 1).Length
                    ElseIf num.ToString.IndexOf(",") > 0 Then
                        numDecimales = num.ToString.Substring(num.ToString.IndexOf(",") + 1).Length
                    End If
                    unitFormatTemp.NumberDecimalDigits = numDecimales
                Else
                    unitFormatTemp.NumberDecimalDigits = 0
                End If
            ElseIf oUnidadPedido.NumeroDeDecimales > 0 Then
                vprecisionfmt = oUnidadPedido.NumeroDeDecimales
            ElseIf oUnidadPedido.NumeroDeDecimales = 0 Then
                vprecisionfmt = oUnidadPedido.NumeroDeDecimales
            End If
            ModuloIdioma = FSNLibrary.TiposDeDatos.ModulosIdiomas.EP_CatalogoWeb
            oFSNTextBox.Attributes.Add("avisoDecimales", Textos(158) & " " & oUnidadPedido.Codigo & ":")

            oFSNTextBox.Text = FormatNumber(strToDbl(oFSNTextBox_DblValue.Text), unitFormatTemp)

        Else 'Si no tenemos datos de la unidad, usamos los datos del usuario
            vprecisionfmt = Me.Usuario.NumberFormat.NumberDecimalDigits

            oFSNTextBox.Text = FormatNumber(strToDbl(oFSNTextBox_DblValue.Text), Me.Usuario.NumberFormat)
        End If

        'Todas las funciones javascript a las que se llama están en el fichero js/jsTextboxFormat.js
        oFSNTextBox.Attributes.Add("onpaste", "return validarpastenum2(" & oFSNTextBox.ClientID & _
                                                                            ",event" & _
                                                                            ",'" & vdecimalfmt & _
                                                                            "','" & vthousanfmt & _
                                                                            "'," & vprecisionfmt & ");")
        oFSNTextBox.Attributes.Add("onkeypress", "return limiteDecimalesTextBox(" & oFSNTextBox.ClientID & _
                                                                            ",event" & _
                                                                            ",'" & vdecimalfmt & _
                                                                            "','" & vthousanfmt & _
                                                                            "'," & vprecisionfmt & ", false);")
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_EmisionPedidos
        Dim errMsg As String = Textos(41) & " " & Textos(42) & " """ & vdecimalfmt & """ " & Textos(43) & " """ & vthousanfmt & """."

        Dim errLimites As String = Textos(183) & " " & _
                                    IIf(minimo <> "null", "\n" & Textos(181) & " ' + num2str(" & minimo & ",'" & vdecimalfmt & "','" & vthousanfmt & "'," & vprecisionfmt & ") + '", "") & _
                                    IIf(maximo <> "null", "\n" & Textos(182) & " ' + num2str(" & maximo & ",'" & vdecimalfmt & "','" & vthousanfmt & "'," & vprecisionfmt & ") + '", "")

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.EP_CatalogoWeb

        'Valores a pasar a las funciones javascript que no son necesarios en este caso:
        Dim sTipoControl As String = String.Empty
        Dim sLblTotLinID As String = String.Empty
        Dim sLblPrecPorProveID As String = String.Empty
        Dim sPrecioOCantidadID As String = String.Empty
        Dim sLblPrecArtID As String = String.Empty
        Dim sMoneda As String = String.Empty

		oFSNTextBox.Attributes.Add("onchange", "addThousandSeparator(" & oFSNTextBox.ClientID & ",'" &
                                                                vdecimalfmt & "','" &
                                                                vthousanfmt & "'," &
                                                                vprecisionfmt & "," &
                                                                "event); validarNumero(" &
                                                                oFSNTextBox.ClientID & ",'" &
                                                                vdecimalfmt & "','" &
                                                                vthousanfmt & "'," &
                                                                vprecisionfmt & ",JSText('" &
                                                                minimo & "'),JSText('" &
                                                                maximo & "'),'" &
                                                                errMsg & "','" &
                                                                errLimites & "','" &
                                                                sTipoControl & "','" &
                                                                sLblTotLinID & "','" &
                                                                sLblPrecPorProveID & "','" &
                                                                sPrecioOCantidadID & "','" &
                                                                sLblPrecArtID & "','" &
                                                                sMoneda & "'," &
                                                                Me.Usuario.NumberFormat.NumberDecimalDigits & ");")

        oFSNTextBox.Attributes.Add("minimo", IIf(valorMinimo Is Nothing, "", valorMinimo))
        oFSNTextBox.Attributes.Add("maximo", IIf(valorMaximo Is Nothing, "", valorMaximo))

        Dim sOperation As String
        Dim oTd_Up As HtmlControls.HtmlGenericControl = CType(oFSNTextBoxArrows.FindControl(oFSNTextBoxArrows.ID & "_td_up"), HtmlControls.HtmlGenericControl)
        Dim oTd_Down As HtmlControls.HtmlGenericControl = CType(oFSNTextBoxArrows.FindControl(oFSNTextBoxArrows.ID & "_td_down"), HtmlControls.HtmlGenericControl)
        sOperation = "add"
        oTd_Up.Attributes.Add("onmousedown", "lowlight(this, event); " & _
           "beginAddOrSubstrackOne('" & oFSNTextBox.ClientID & "', " & _
                  minimo & ", " & _
                  maximo & ", '" & _
                  vdecimalfmt & "', '" & _
                  vthousanfmt & "', " & _
                  vprecisionfmt & ", '" & _
                  sOperation & _
                  "', event,'" & _
                  errMsg & "','" & _
                  errLimites & "','" & _
                  sTipoControl & "','" & _
                  sLblTotLinID & "','" & _
                  sLblPrecPorProveID & "','" & _
                  sPrecioOCantidadID & "','" & _
                  sLblPrecArtID & "','" & _
                  sMoneda & "'," & _
                  Me.Usuario.NumberFormat.NumberDecimalDigits & ");")
        sOperation = "subtract"
        oTd_Down.Attributes.Add("onmousedown", "lowlight(this, event); " & _
           "beginAddOrSubstrackOne('" & oFSNTextBox.ClientID & "', " & _
                  minimo & ", " & _
                  maximo & ", '" & _
                  vdecimalfmt & "', '" & _
                  vthousanfmt & "', " & _
                  vprecisionfmt & ", '" & _
                  sOperation & _
                  "', event,'" & _
                  errMsg & "','" & _
                  errLimites & "','" & _
                  sTipoControl & "','" & _
                  sLblTotLinID & "','" & _
                  sLblPrecPorProveID & "','" & _
                  sPrecioOCantidadID & "','" & _
                  sLblPrecArtID & "','" & _
                  sMoneda & "'," & _
                  Me.Usuario.NumberFormat.NumberDecimalDigits & ");")
        oTd_Up.Attributes.Add("onmouseover", "highlight(this);")
        oTd_Down.Attributes.Add("onmouseover", "highlight(this);")
        oTd_Up.Attributes.Add("onmouseout", "endAddOrSubstrackOne(); unhighlight(this);")
        oTd_Down.Attributes.Add("onmouseout", "endAddOrSubstrackOne(); unhighlight(this);")
        oTd_Up.Attributes.Add("onmouseup", "unlowlight(this); endAddOrSubstrackOne();")
        oTd_Down.Attributes.Add("onmouseup", "unlowlight(this); endAddOrSubstrackOne();")
    End Sub
#End Region
#Region "Pre Render"
    ''' <summary>
    ''' Método que se lanza justo antes de generar el control en pantalla
    ''' </summary>
    ''' <param name="sender">control que lo lanza</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Evento que se lanza en el momento anterior a dibujar el control en pantalla. Tiempo máximo inferior a 0,1 seg</remarks>
    Private Sub GridArticulos_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridArticulos.PreRender
        Dim GridArticulos As GridView = CType(sender, GridView)
        For Each GridLinea As GridViewRow In GridArticulos.Rows
            Dim wNumEdCant As FSNWebControls.FSNTextBox = CType(GridLinea.FindControl("WNumEdCantidad"), FSNWebControls.FSNTextBox)
            Dim wNumEdPrec As FSNWebControls.FSNTextBox = CType(GridLinea.FindControl("WNumEdPrecioArt"), FSNWebControls.FSNTextBox)
            Dim minimo As Nullable(Of Double)
            Dim maximo As Nullable(Of Double)

            If ScriptMgr.IsInAsyncPostBack Then
                If String.IsNullOrEmpty(wNumEdCant.Attributes("minimo")) Then
                    minimo = Nothing
                Else
                    minimo = CDbl(wNumEdCant.Attributes("minimo"))
                End If
                If String.IsNullOrEmpty(wNumEdCant.Attributes("maximo")) Then
                    maximo = Nothing
                Else
                    maximo = CDbl(wNumEdCant.Attributes("maximo"))
                End If
                configurarDecimales(wNumEdCant.Attributes("UNIDAD"), wNumEdCant, minimo, maximo)
                configurarDecimales(Nothing, wNumEdPrec, 0, Nothing) 'Precio tiene como mínimo cero porque así estaba configurado el control antes de estos cambios
            End If
            'Insertamos un script que controla el evento paste en el control
            Me.insertarScriptPaste(wNumEdCant.ClientID)

        Next
    End Sub
#End Region
#Region "Cache"
    ''' <summary>
    ''' Método que elimina de la caché las Categorias Filtradas que se listan en el filtrador de categorías
    ''' </summary>
    ''' <remarks>Siempre que se desee eliminar la caché de categorías filtradas</remarks>
    Private Sub borrarCacheCategorias()
        If HttpContext.Current.Cache("CategoriasFiltradas_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) IsNot Nothing Then
            HttpContext.Current.Cache.Remove("CategoriasFiltradas_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
        End If
    End Sub
    ''' <summary>
    ''' Método que elimina de la caché las Proveedores Filtrados que se listan en el filtrador de Proveedores
    ''' </summary>
    ''' <remarks>Siempre que se desee eliminar la caché de Proveedores Filtrados</remarks>
    Private Sub borrarCacheProveedores()
        If HttpContext.Current.Cache("ProveedoresFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) IsNot Nothing Then
            HttpContext.Current.Cache.Remove("ProveedoresFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
        End If
    End Sub
    ''' <summary>
    ''' Método que elimina de la caché los Datos de Proveedores Filtrados que se listan en el filtrador de Destinos
    ''' </summary>
    ''' <remarks>Siempre que se desee eliminar la caché de Datos de Proveedores Filtrados</remarks>
    Private Sub borrarCacheDatosProveedores()
        If HttpContext.Current.Cache("DatosProveedoresFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) IsNot Nothing Then
            HttpContext.Current.Cache.Remove("DatosProveedoresFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
        End If
    End Sub
    ''' <summary>
    ''' Método que elimina de la caché los Destinos Filtrados que se listan en el filtrador de destinos
    ''' </summary>
    ''' <remarks>Siempre que se desee eliminar la caché de Destinos Filtrados</remarks>
    Private Sub borrarCacheDestinos()
        If HttpContext.Current.Cache("DestinosFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString) IsNot Nothing Then
            HttpContext.Current.Cache.Remove("DestinosFiltrados_" & FSNUser.CodPersona & "_" & FSNUser.Idioma.ToString)
        End If
    End Sub
#End Region
    ''' Revisado por: blp. Fecha: 31/01/2012
    ''' <summary>
    ''' Método con el que vamos a contruir el listado de proveedores y lo vamos a insertar en el panel de proveedores pnlFiltroProv
    ''' </summary>
    ''' <param name="dtListaProveedores">Tabla de la que recuperamos los datos</param>
    ''' <remarks>Llamada desde todo aquel lugar en que se necesite llenar la lista de proveedores. Tiempo máximo inferior a 0,3 seg</remarks>
    Private Sub CrearyAgregarListadoProv(ByVal dtListaProveedores As DataTable)
        'Vaciamos el panel porque vamos a rellenarlo siempre
        pnlFiltroProv.Controls.Clear()

        If dtListaProveedores IsNot Nothing AndAlso dtListaProveedores.Rows.Count > 0 Then
            'Vamos a generar una serie de divs (paneles) con los datos de los proveedores.
            'Ese contenido lo vamos a meter en un table y luego ese table en el panel de proveedores
            'Parece absurdo usar un table pero si se introducen directamente los divs, hay algo en el _loadScriptsInternal del ajax de ms que hace
            'que se oculte el contenido del panel pnlFiltroProv al pulsar "Ver todos". Con la tabla en cambio, no pasa
            Dim oTable As New HtmlControls.HtmlTable
            Dim oTr As New HtmlControls.HtmlTableRow
            Dim oTd As New HtmlControls.HtmlTableCell
            oTd.ID = pnlFiltroProv.ID & "_TD"
            Dim div1Width As Integer = 90
            Dim div3Width As Integer = 50

            If (ScriptMgr.AsyncPostBackSourceElementID.IndexOf("$lnkVerTodos") > 0) Then
                verTodos = True
            End If

            Dim contador As Integer = 1
            Dim iTotalProveedores As Integer = dtListaProveedores.Rows.Count
            If Usuario.MostrarCodProve Then
                pnlFiltroProv.Attributes.Add("codProveVisible", "true")
            Else
                pnlFiltroProv.Attributes.Add("codProveVisible", "false")

            End If
            For Each prove As DataRow In dtListaProveedores.Rows
                '1
                Dim oDiv As New Panel
                oDiv.Style.Add("margin-rigth", "2px")
                oDiv.Style.Add("float", "left")
                If contador <> 1 AndAlso ((contador - 1) Mod 3) = 0 Then oDiv.Style.Add("clear", "left")
                oDiv.Style.Add("padding-rigth", "5")
                ''Alternar fondo para distinguir bien los proveedores
                'If (contador Mod 2) <> 0 Then
                '    oDiv.Style.Add("background-color", "#E5E5E5")
                'End If
                oDiv.CssClass = "ellipsis"
                Dim lnkProv As New LinkButton
                lnkProv.ID = "lnkProvListA" & contador
                If Usuario.MostrarCodProve Then
                    oDiv.Width = div1Width
                    Dim sCodigo As String = prove.Item("PROVE")
                    lnkProv.Text = sCodigo
                    lnkProv.CssClass = "Normal"
                    lnkProv.Font.Underline = False
                    lnkProv.OnClientClick = "guardarINFO(""" & prove.Item("PROVE") & "###" & HttpUtility.JavaScriptStringEncode(prove.Item("DEN")) & """);"
                    oDiv.Controls.Add(lnkProv)
                End If
                oTd.Controls.Add(oDiv)

                '2
                oDiv = New Panel
                oDiv.Style.Add("margin-rigth", "2px")
                oDiv.Style.Add("float", "left")
                oDiv.Style.Add("padding-rigth", "5")
                ''Alternar fondo para distinguir bien los proveedores
                'If (contador Mod 2) <> 0 Then
                '    oDiv.Style.Add("background-color", "#E5E5E5")
                'End If
                oDiv.CssClass = "ellipsis"
                oDiv.Attributes.Add("name", "divProveDen")
                lnkProv = New LinkButton
                lnkProv.ID = "lnkProvListB" & contador
                Dim sProve As String = prove.Item("DEN")
                lnkProv.Text = HttpUtility.JavaScriptStringEncode(sProve)

                lnkProv.CssClass = "Normal ellipsis"
                lnkProv.Font.Underline = False
                lnkProv.OnClientClick = "guardarINFO(""" & prove.Item("PROVE") & "###" & HttpUtility.JavaScriptStringEncode(prove.Item("DEN")) & """);"
                oDiv.Controls.Add(lnkProv)
                pnlFiltroProv.Controls.Add(oDiv)
                oTd.Controls.Add(oDiv)

                '3
                oDiv = New Panel
                oDiv.Style.Add("margin-rigth", "2px")
                oDiv.Style.Add("float", "left")
                oDiv.Style.Add("padding-rigth", "5")
                ''Alternar fondo para distinguir bien los proveedores
                'If (contador Mod 2) <> 0 Then
                '    oDiv.Style.Add("background-color", "#E5E5E5")
                'End If
                oDiv.CssClass = "ellipsis"
                oDiv.Width = div3Width
                lnkProv = New LinkButton
                lnkProv.ID = "lnkProvListC" & contador
                lnkProv.Text = "(" & prove.Item("Cantidad") & ")"
                lnkProv.CssClass = "Normal"
                lnkProv.Font.Underline = False
                lnkProv.OnClientClick = "guardarINFO(""" & prove.Item("PROVE") & "###" & HttpUtility.JavaScriptStringEncode(prove.Item("DEN")) & """);"
                oDiv.Controls.Add(lnkProv)
                pnlFiltroProv.Controls.Add(oDiv)
                oTd.Controls.Add(oDiv)

                contador += 1
                If Not verTodos Then
                    If contador > 18 Then Exit For
                End If
            Next
            If Not verTodos AndAlso dtListaProveedores.Rows.Count > 18 Then
                Dim oDiv As New Panel
                oDiv.HorizontalAlign = HorizontalAlign.Left
                oDiv.Style.Add("clear", "left")
                Dim lnkProv As New LinkButton
                lnkProv.ID = "lnkVerTodos"
                lnkProv.Text = Textos(164)
                lnkProv.CssClass = "Rotulo"
                lnkProv.Font.Underline = True
                oDiv.Controls.Add(lnkProv)
                oTd.Controls.Add(oDiv)
            End If
            oTr.Cells.Add(oTd)
            oTable.Rows.Add(oTr)
            pnlFiltroProv.Controls.Add(oTable)
        End If
        updFiltros.Update()
    End Sub
    Private Property verTodos() As Boolean
        Get
            Return ViewState("verTodos")
        End Get
        Set(ByVal value As Boolean)
            ViewState("verTodos") = value
        End Set
    End Property
    ''' Revisado por: blp. Fecha: 21/01/2013
    ''' <summary>
    ''' registramos los scripts que modificarán el ancho de los divs que hay dentro del control pnlFiltroProv y que llevan la información de los proveedores
    ''' a fin de que ocupen todo el ancho de la pantalla
    ''' </summary>
    ''' <param name="sender">control que lanza el evento prerender</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. Maximo 0,1 seg.</remarks>
    Private Sub pnlFiltroProv_PreRender(sender As Object, e As System.EventArgs) Handles pnlFiltroProv.PreRender
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered("crearVariableJSpnlFiltroProv") Then _
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "crearVariableJSpnlFiltroProv", "var pnlFiltroProveID ='#" & pnlFiltroProv.ClientID & "'; var pnlFiltroProve_TDID ='#" & pnlFiltroProv.ClientID & "_TD'; var PanelFiltrosAvID ='#" & PanelFiltrosAv.ClientID & "'; var BtnProvesID ='#" & BtnProves.ClientID & "'; ", True)

        'jquery se carga en Cabecera.Master, lo cual hace que en la primera carga, no esté disponible si usamos el scriptmanager o el me.page.clientScript para registrar CatalogoWeb.js
        'Por eso, cuando cargue por vez primera, usaremos ScriptMgr y scriptReference (de modo que se lance automáticaMENTE la parte de CatalogoWeb.js que no se encuentra dentro de ninguna función)
        'y en el resto de casos, usamos ScriptManager.RegisterClientScriptBlock para lanzar la función que hay en ScriptManager.RegisterClientScriptBlock y que hace exactamente lo mismo.
        If Not IsPostBack Then
            Dim scriptReference As New ScriptReference
            scriptReference.Path = ConfigurationManager.AppSettings("ruta") & "App_Pages/EP/js/CatalogoWeb.js"
            ScriptMgr.Scripts.Add(scriptReference)
        Else
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "lanzarConfiguracionAnchospnlFiltroProv", "ConfiguracionAnchospnlFiltroProv();", True)
        End If
    End Sub
    Private Sub uwgSolicitudes_WDG_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles uwgSolicitudes_WDG.InitializeRow
        Dim Index As Integer
        If CType(e.Row.DataItem, System.Data.DataRowView).Row("ADJUN") > 0 Then
            Index = sender.Columns.FromKey("IMGADJUN").Index
            e.Row.Items.Item(Index).Text = "<img src='" & ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/coment.gif" & "' style='text-align:center;'/>"
        End If

        If CType(e.Row.DataItem, System.Data.DataRowView).Row("FAVORITOS") > 0 Then
            Index = sender.Columns.FromKey("IMGFAV").Index
            e.Row.Items.Item(Index).Text = "<img src='" & ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/favoritos_si.gif" & "' style='text-align:center;'/>"
        End If
    End Sub
End Class