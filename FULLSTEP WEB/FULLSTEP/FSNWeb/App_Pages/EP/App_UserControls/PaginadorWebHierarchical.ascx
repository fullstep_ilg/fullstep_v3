﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PaginadorWebHierarchical.ascx.vb" Inherits="Fullstep.FSNWeb.PaginadorWebHierarchical" %>
<asp:UpdatePanel ID="upPaginador" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
<ContentTemplate>
<table width="350px" cellpadding="2" border="0" cellspacing="0">
    <tr>
        <td>&nbsp;</td><td>&nbsp;</td>
        <td>
        <asp:LinkButton runat="server" ID="btnFirstPage" CommandArgument="first" OnClick="changePage" style="text-align:right;">
            <asp:Image id="imFirstPage" runat=server style="border:0px"/>
        </asp:LinkButton>
        </td>
        <td>
        <asp:LinkButton runat="server" ID="btnPreviousPage" CommandArgument="prev" OnClick="changePage" style="text-align:right;">
            <asp:Image id="imPreviousPage" runat=server style="border:0px"/>
        </asp:LinkButton>
        </td>
        <td><asp:Label cssClass="common" ID="lblPagina" runat="server" Text="DPágina"></asp:Label></td>
        <td><asp:DropDownList ID="ddlPage" runat="server" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="changePage"></asp:DropDownList></td>
        <td align="left">
            <asp:Label cssClass="common" ID="lblPagTot" runat="server" Text="D1"></asp:Label>
            <asp:Label cssClass="common" ID="lblTotal" runat="server" Text="D(Total )"></asp:Label>
        </td>
        <td><asp:LinkButton runat="server" ID="btnNextPage" CommandArgument="next" OnClick="changePage" style="text-align:right;">
            <asp:Image id="imNextPage" runat=server style="border:0px"/>
        </asp:LinkButton>
        </td>
        <td><asp:LinkButton runat="server" ID="btnLastPage" CommandArgument="last" OnClick="changePage" style="text-align:right;">
            <asp:Image id="imLastPage" runat=server style="border:0px"/>
        </asp:LinkButton>
        </td>
    </tr>
</table>
</ContentTemplate>
</asp:UpdatePanel>