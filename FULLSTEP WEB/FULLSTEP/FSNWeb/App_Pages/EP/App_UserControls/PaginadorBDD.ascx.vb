﻿Imports Fullstep.FSNServer

Partial Public Class PaginadorBDD
    Inherits System.Web.UI.UserControl

    Const _ModuloIdioma As TiposDeDatos.ModulosIdiomas = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
    Private _Textos As DataSet
    Private _SentidoOrdenacion As String
    Private _CampoOrden As String

    Public Event CambioOrden(ByVal CampoOrden As String, ByVal SentidoOrdenacion As String)
    Public Event Exportar()
    Public Event ActualizarGrid(ByVal PageNumber As Integer)

    Public Property SentidoOrdenacion() As String
        Get
            If String.IsNullOrEmpty(_SentidoOrdenacion) Then
                If String.IsNullOrEmpty(ViewState("SentidoOrdenacion")) Then
                    Return "DESC"
                Else
                    Return ViewState("SentidoOrdenacion")
                End If
            Else
                Return _SentidoOrdenacion
            End If
        End Get
        Set(ByVal value As String)
            If _SentidoOrdenacion <> value Then
                _SentidoOrdenacion = value
                ViewState("SentidoOrdenacion") = _SentidoOrdenacion
            End If
        End Set
    End Property

    Public Property ListOrden() As ListItemCollection
        Get
            Return ddlOrdenacion.Items
        End Get
        Set(ByVal value As ListItemCollection)
            ddlOrdenacion.Items.Clear()
            For Each l As ListItem In value
                ddlOrdenacion.Items.Add(l)
            Next
        End Set
    End Property

    Public Property CampoOrden() As String
        Get
            If String.IsNullOrEmpty(_CampoOrden) Then
                If String.IsNullOrEmpty(ViewState("CampoOrden")) Then
                    Return "FECHA"
                Else
                    Return ViewState("CampoOrden")
                End If
            Else
                Return _CampoOrden
            End If
        End Get
        Set(ByVal value As String)
            If _CampoOrden <> value Then
                _CampoOrden = value
                ddlOrdenacion.SelectedValue = _CampoOrden
                ViewState("CampoOrden") = _CampoOrden
            End If
        End Set
    End Property

    Public Property TotalRegistros() As Integer
        Get
            If String.IsNullOrEmpty(ViewState.Item("TotalRegistros")) Then
                Return 0
            Else
                Return ViewState.Item("TotalRegistros")
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState.Item("TotalRegistros") = value
        End Set
    End Property

    Public Property PageSize() As Integer
        Get
            If String.IsNullOrEmpty(ViewState.Item("PageSize")) Then
                Return 0
            Else
                Return ViewState.Item("PageSize")
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState.Item("PageSize") = value
        End Set
    End Property

    Public ReadOnly Property TotalPaginas() As Integer
        Get
            If TotalRegistros > 0 AndAlso PageSize > 0 Then
                Dim total As Double = TotalRegistros / PageSize
                If total - Math.Round(total, System.MidpointRounding.AwayFromZero) > 0 Then total += 1
                Return total
            Else
                Return 0
            End If
        End Get
    End Property

    ''' <summary>
    ''' Número de página en el que se encuentra el control
    ''' </summary>
    Public Property PageNumber() As Integer
        Get
            If Me.ViewState("PageNumber") IsNot Nothing Then
                Return Convert.ToInt32(Me.ViewState("PageNumber"))
            Else
                Me.ViewState.Add("PageNumber", 1)
                Return 1
            End If
        End Get

        Set(ByVal value As Integer)
            If Me.ViewState("PageNumber") IsNot Nothing Then
                Me.ViewState("PageNumber") = value
            Else
                Me.ViewState.Add("PageNumber", value)
            End If
        End Set
    End Property

    Protected ReadOnly Property PageIndex() As Integer
        Get
            If Me.PageNumber > 1 Then
                Return Me.PageNumber - 1
            Else
                Return 0
            End If
        End Get
    End Property

    Private ReadOnly Property Textos(ByVal iTexto As Integer) As String
        Get
            Dim pag As IFSNPage = CType(Me.Page, IFSNPage)
            If _Textos Is Nothing Then
                _Textos = CType(HttpContext.Current.Cache("Textos_" & pag.Idioma & "_" & _ModuloIdioma), DataSet)
            End If
            If _Textos Is Nothing Then
                Dim FSNDict As FSNServer.Dictionary = pag.FSNServer.Get_Object(GetType(FSNServer.Dictionary))
                FSNDict.LoadData(_ModuloIdioma, pag.Idioma)
                _Textos = FSNDict.Data
                HttpContext.Current.Cache.Insert("Textos_" & pag.Idioma & "_" & _ModuloIdioma, _Textos, Nothing, Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            End If
            Return _Textos.Tables(0).Rows(iTexto).Item(1)
        End Get
    End Property

    ''' Revisado por: blp. Fecha: 04/12/2012
    ''' <summary>
    ''' Procedimiento en el que configuramos los controles relacionados con la paginación en función del totalRegistros
    ''' </summary>
    ''' <param name="iTotalRegistros">total registros</param>
    ''' <param name="iPageSize">número de registros por página</param>
    ''' <param name="iPageNumber">Número de página</param>
    ''' <remarks>Llamada desde los controles que refrescan los resultados del accordion de pedidos
    ''' Tiempo máximo: 0 sec.</remarks>
    Public Sub InitPaginador(ByVal iTotalRegistros As Integer, ByVal iPageSize As Integer, ByVal iPageNumber As Integer)
        TotalRegistros = iTotalRegistros
        PageSize = iPageSize
        PageNumber = iPageNumber
        If TotalPaginas > 1 Then
            'Actualizamos las combos de paginación
            Dim i As Integer
            ddlPage.Items.Clear()
            For i = 0 To TotalPaginas - 1
                ddlPage.Items.Insert(i, New ListItem(i + 1, i))
            Next
            If PageIndex >= ddlPage.Items.Count Then PageNumber = ddlPage.Items.Count
            ddlPage.Items(PageIndex).Selected = True

            If (PageIndex * PageSize + PageSize) > TotalRegistros Then
                lblResul.Text = CStr(PageIndex * PageSize + 1) & "-" & CStr(TotalRegistros)
                lblResulTot.Text = TotalRegistros
            Else
                lblResul.Text = CStr(PageIndex * PageSize + 1) & "-" & CStr(PageIndex * PageSize + PageSize)
                lblResulTot.Text = TotalRegistros
            End If

            lblPagTot.Text = TotalPaginas

            If PageNumber = 1 Then
                btnFirstPage.Enabled = False
                btnFirstPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero_desactivado.gif")
                btnPreviousPage.Enabled = False
                btnPreviousPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior_desactivado.gif")
            Else
                btnFirstPage.Enabled = True
                btnFirstPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero.gif")
                btnPreviousPage.Enabled = True
                btnPreviousPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior.gif")
            End If

            If PageNumber >= TotalPaginas Then
                btnNextPage.Enabled = False
                btnNextPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente_desactivado.gif")
                btnLastPage.Enabled = False
                btnLastPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo_desactivado.gif")
            Else
                btnNextPage.Enabled = True
                btnNextPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente.gif")
                btnLastPage.Enabled = True
                btnLastPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo.gif")
            End If
        Else
            lblViendo.Visible = False
            lblResul.Visible = False
            lblResulDe.Visible = False
            lblResulTot.Visible = False
            btnFirstPage.Visible = False
            btnPreviousPage.Visible = False
            lblPagina.Visible = False
            ddlPage.Visible = False
            lblDe.Visible = False
            lblPagTot.Visible = False
            btnNextPage.Visible = False
            btnLastPage.Visible = False
        End If

        If TotalRegistros = 0 Then
            imgbtnExportar.Enabled = False
            imgbtnExportar.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "excel_desactivado.gif")
            lblOrdenacion.Visible = False
            ddlOrdenacion.Visible = False
            imgBtnOrdenacion.Visible = False
        Else
            imgbtnExportar.Enabled = True
            imgbtnExportar.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "excel.gif")
            imgBtnOrdenacion.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, IIf(SentidoOrdenacion = "ASC", "ascendente.gif", "descendente.gif"))
        End If
    End Sub

    ''' Revisado por: blp. Fecha: 04/12/2012
    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de primera página
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de primera página
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnFirstPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFirstPage.Click
        If TotalRegistros > 0 Then
            PageNumber = 1
            InitPaginador(TotalRegistros, PageSize, PageNumber)
            RaiseEvent ActualizarGrid(PageNumber)
        End If
    End Sub

    ''' Revisado por: blp. Fecha: 04/12/2012
    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de página anterior
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de página anterior
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnPreviousPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPreviousPage.Click
        If TotalRegistros > 0 Then
            PageNumber = ddlPage.SelectedIndex 'Esto, para el pageNumber, es como restarle uno
            InitPaginador(TotalRegistros, PageSize, PageNumber)
            RaiseEvent ActualizarGrid(PageNumber)
        End If
    End Sub

    ''' Revisado por: blp. Fecha: 04/12/2012
    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de página siguiente
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de página siguiente
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnNextPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNextPage.Click
        If TotalRegistros > 0 Then
            PageNumber = ddlPage.SelectedIndex + 2 'Hay diferencia entre el PageIndex y el PageNumber. Como el index es uno menos que el number, cuando sumamos al number un index, hacer que suba uno implica sumarle 2
            InitPaginador(TotalRegistros, PageSize, PageNumber)
            RaiseEvent ActualizarGrid(PageNumber)
        End If
    End Sub

    ''' Revisado por: blp. Fecha: 04/12/2012
    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de última página
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de última página
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnLastPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLastPage.Click
        If TotalRegistros > 0 Then
            PageNumber = TotalPaginas
            InitPaginador(TotalRegistros, PageSize, PageNumber)
            RaiseEvent ActualizarGrid(PageNumber)
        End If
    End Sub

    ''' Revisado por: blp. Fecha: 04/12/2012
    ''' <summary>
    ''' Evento que se lanza al seleccionar alguna página de la combo de paginación
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: La combo de paginación
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub ddlPage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPage.SelectedIndexChanged
        If TotalRegistros > 0 Then
            PageNumber = CType(sender, DropDownList).SelectedIndex + 1
            InitPaginador(TotalRegistros, PageSize, PageNumber)
            RaiseEvent ActualizarGrid(PageNumber)
        End If
    End Sub

    ''' Revisado por: blp. Fecha: 04/12/2012
    ''' <summary>
    ''' Evento que se lanza en el load el paginador
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: evento Load
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblViendo.Text = Textos(106)
        lblResulDe.Text = Textos(75)
        lblPagina.Text = Textos(74)
        lblDe.Text = Textos(75)
        lblOrdenacion.Text = Textos(107)
    End Sub

    ''' Revisado por: blp. Fecha: 04/12/2012
    ''' <summary>
    ''' Evento que se lanza en el click del control de ordenación
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: evento Load
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub imgBtnOrdenacion_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnOrdenacion.Click
        SentidoOrdenacion = IIf(SentidoOrdenacion = "ASC", "DESC", "ASC")
        PageNumber = 1
        InitPaginador(TotalRegistros, PageSize, PageNumber)
        RaiseEvent CambioOrden(CampoOrden, SentidoOrdenacion)
    End Sub

    ''' Revisado por: blp. Fecha: 04/12/2012
    ''' <summary>
    ''' Evento que se lanza en el click del control de Exportación
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: evento Load
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub imgbtnExportar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnExportar.Click
        RaiseEvent Exportar()
    End Sub

    ''' Revisado por: blp. Fecha: 04/12/2012
    ''' <summary>
    ''' Evento que se lanza en el cambio de selección del combo de ordenación
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: evento Load
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub ddlOrdenacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOrdenacion.SelectedIndexChanged
        If CampoOrden <> ddlOrdenacion.SelectedValue Then
            CampoOrden = ddlOrdenacion.SelectedValue
            PageNumber = 1
            InitPaginador(TotalRegistros, PageSize, PageNumber)
            RaiseEvent CambioOrden(CampoOrden, SentidoOrdenacion)
        End If
    End Sub

End Class