﻿Imports Fullstep.FSNLibrary
Imports Fullstep.FSNServer
Partial Public Class DetArticuloCatalogo
    Inherits System.Web.UI.UserControl

    Const _ModuloIdioma As TiposDeDatos.ModulosIdiomas = TiposDeDatos.ModulosIdiomas.EP_CatalogoWeb
    Private _Textos As DataSet

    Private ReadOnly Property Textos(ByVal iTexto As Integer) As String
        Get
            If _Textos Is Nothing Then
                Dim pag As IFSNPage = CType(Me.Page, IFSNPage)
                Dim FSNDict As FSNServer.Dictionary = pag.FSNServer.Get_Object(GetType(FSNServer.Dictionary))
                FSNDict.LoadData(_ModuloIdioma, pag.Idioma)
                _Textos = FSNDict.Data
            End If
            Return _Textos.Tables(0).Rows(iTexto).Item(1)
        End Get
    End Property

    Public ReadOnly Property Modificar() As Boolean
        Get
            Return (BtnAnyadirCestaDet.CommandName = "Modificar")
        End Get
    End Property

    Public ReadOnly Property Key() As Integer
        Get
            Return Me.ViewState("Key")
        End Get
    End Property

    Public Event AnyadirACesta(ByVal IdArt As Integer, ByVal Cantidad As Double, ByVal Precio As Double, ByVal Key As String)
    Public Event AnyadirACestaEP(ByVal IdArt As Integer, ByVal Cantidad As Double, ByVal Precio As Double, ByVal Key As String)

    ''' <summary>
    ''' Procedimiento que se utiliza para Habilitar el control ModalPopup del panel donde tenemos los datos del artículo y hacerlo visible, cuando se clica el LinkButton con el detalle del articulo
    ''' </summary>
    ''' <param name="dsArticulos">Dataset que contiene los artículos del catálogo</param>
    ''' <param name="IdArt">Id del artículo a mostrar</param>
    Public Sub CargarDetalleArticulo(ByVal dsArticulos As DataSet, ByVal IdArt As Integer)
        CargarDetalleArticulo(dsArticulos, IdArt, Nothing, Nothing, String.Empty, 0, False, Nothing, Nothing, 0)
    End Sub

    ''' <summary>
    ''' Procedimiento que se utiliza para Habilitar el control ModalPopup del panel donde tenemos los datos del artículo y hacerlo visible, cuando se clica el LinkButton con el detalle del articulo
    ''' </summary>
    ''' <param name="dsArticulos">Dataset que contiene los artículos del catálogo</param>
    ''' <param name="IdArt">Id del artículo a mostrar</param>
    ''' <param name="Key">Valor de tipo integer para la propiedad Key del Control</param>
    Public Sub CargarDetalleArticulo(ByVal dsArticulos As DataSet, ByVal IdArt As Integer, ByVal Key As Integer)
        CargarDetalleArticulo(dsArticulos, IdArt, Nothing, Nothing, String.Empty, 0, False, Key, Nothing, 0)
    End Sub

    ''' <summary>
    ''' Procedimiento que se utiliza para Habilitar el control ModalPopup del panel donde tenemos los datos del artículo y hacerlo visible, cuando se clica el LinkButton con el detalle del articulo
    ''' </summary>
    ''' <param name="dsArticulos">Dataset que contiene los artículos del catálogo</param>
    ''' <param name="IdArt">Id del artículo a mostrar</param>
    ''' <param name="Cantidad">Cantidad existente en la cesta, sólo en caso de que se vaya a modificar un artículo añadido con anterioridad</param>
    ''' <param name="Precio">Precio existente en la cesta, sólo en el caso de que se vaya a modificar un artículo añadido con anterioridad</param>
    ''' <param name="Modificar">True: Modifica datos de un artículo añadido a la cesta / False: Añade el artículo a la cesta</param>
    Public Sub CargarDetalleArticulo(ByVal dsArticulos As DataSet, ByVal IdArt As Integer, ByVal Cantidad As Double, ByVal Precio As Double, ByVal Modificar As Boolean)
        CargarDetalleArticulo(dsArticulos, IdArt, Cantidad, Precio, String.Empty, 0, Modificar, Nothing, Nothing, 0)
    End Sub

    ''' <summary>
    ''' Procedimiento que se utiliza para Habilitar el control ModalPopup del panel donde tenemos los datos del artículo y hacerlo visible, cuando se clica el LinkButton con el detalle del articulo
    ''' </summary>
    ''' <param name="dsArticulos">Dataset que contiene los artículos del catálogo</param>
    ''' <param name="IdArt">Id del artículo a mostrar</param>
    ''' <param name="Cantidad">Cantidad existente en la cesta, sólo en caso de que se vaya a modificar un artículo añadido con anterioridad</param>
    ''' <param name="Precio">Precio existente en la cesta, sólo en el caso de que se vaya a modificar un artículo añadido con anterioridad</param>
    ''' <param name="Modificar">True: Modifica datos de un artículo añadido a la cesta / False: Añade el artículo a la cesta</param>
    Public Sub CargarDetalleArticuloEP(ByVal dsArticulos As DataSet, ByVal IdArt As Integer, ByVal Cantidad As Double, ByVal Precio As Double, ByVal Modificar As Boolean, ByVal TipoRecepcion As Integer)
        CargarDetalleArticulo(dsArticulos, IdArt, Cantidad, Precio, String.Empty, 0, Modificar, Nothing, Nothing, 0, TipoRecepcion, True)
    End Sub

    ''' <summary>
    ''' Procedimiento que se utiliza para Habilitar el control ModalPopup del panel donde tenemos los datos del artículo y hacerlo visible, cuando se clica el LinkButton con el detalle del articulo
    ''' </summary>
    ''' <param name="dsArticulos">Dataset que contiene los artículos del catálogo</param>
    ''' <param name="IdArt">Id del artículo a mostrar</param>
    ''' <param name="Cantidad">Cantidad existente en la cesta, sólo en caso de que se vaya a modificar un artículo añadido con anterioridad</param>
    ''' <param name="Precio">Precio existente en la cesta, sólo en el caso de que se vaya a modificar un artículo añadido con anterioridad</param>
    ''' <param name="Unidad">Código de la unidad de pedido. En caso de estar vacío se tomará la unidad de compra.</param>
    ''' <param name="CantidadMinima">Cantidad mínima de la unidad de pedido. Si no hay unidad de pedido se utilizará la de la unidad de compra.</param>
    ''' <param name="Modificar">True: Modifica datos de un artículo añadido a la cesta / False: Añade el artículo a la cesta</param>
    ''' <param name="Key">Valor de tipo integer para la propiedad Key del Control</param>
    Public Sub CargarDetalleArticulo(ByVal dsArticulos As DataSet, ByVal IdArt As Integer, ByVal Cantidad As Double, ByVal Precio As Double, ByVal Unidad As String, ByVal CantidadMinima As Double, ByVal Modificar As Boolean, ByVal Key As Integer, Optional ByVal sNuevaDenominacion As String = Nothing, Optional ByVal CantidadMaxima As Double = 0, Optional ByVal iTipoRecepcion As String = Nothing, Optional ByVal bEP As Boolean = False)
        Dim pag As FSNPage = CType(Me.Page, FSNPage)
        Me.ViewState("Key") = Key
        With dsArticulos.Tables(0).Rows.Find(IdArt)
            ImgDetArt.ImageUrl = "../EPThumbnail.ashx?Prove=" & .Item("PROVE") & "&Art=" & .Item("COD_ITEM") & "&Tipo=Imagen" & "&PonerImagen=True"
            Dim sCadena As String = String.Empty
            If sNuevaDenominacion IsNot Nothing Then
                sCadena = sNuevaDenominacion 'esto se añade para mostra la nueva Denominacion
            Else
                If pag.FSNUser.MostrarCodArt And Not IsDBNull(.Item("COD_ITEM")) AndAlso Not String.IsNullOrEmpty(.Item("COD_ITEM")) Then
                    sCadena = .Item("COD_ITEM") & " - "
                End If
                sCadena = sCadena & .Item("ART_DEN")
                If Not IsDBNull(.Item("COD_EXT")) AndAlso Not String.IsNullOrEmpty(.Item("COD_EXT")) Then
                    sCadena = sCadena & " (" & Me.Textos(136) & ": " & .Item("COD_EXT") & ")"
                End If
            End If

            LblDetNomArt.Text = sCadena
            pnlAdjuntos.Visible = False
            LblAdjuntos.Text = Me.Textos(126)
            If IsDBNull(.Item("ESP")) OrElse String.IsNullOrEmpty(.Item("ESP")) Then
                litAdjuntos.Visible = False
            Else
                pnlAdjuntos.Visible = True
                litAdjuntos.Visible = True
                litAdjuntos.Text = "<p>" & .Item("ESP") & "</p>"
            End If
            Dim oArt As CArticulo = CType(Me.Page, FSNPage).FSNServer.Get_Object(GetType(CArticulo))
            oArt.ID = IdArt
            oArt.CargarEspecificaciones()
            If oArt.Especificaciones.Count > 0 Then
                grdAdjuntos.Attributes.Add("ARTICULO", IdArt)
                grdAdjuntos.DataSource = oArt.Especificaciones
                grdAdjuntos.DataBind()
                pnlAdjuntos.Visible = True
                grdAdjuntos.Visible = True
            Else
                grdAdjuntos.Visible = False
            End If
            If pag.FSNUser.MostrarCodProve And Not IsDBNull(.Item("PROVE")) AndAlso Not String.IsNullOrEmpty(.Item("PROVE")) Then
                sCadena = .Item("PROVE") & " - "
            Else
                sCadena = String.Empty
            End If
            sCadena = sCadena & .Item("PROVEDEN")
            FSNLinkProveDet.Text = sCadena
            FSNLinkProveDet.ContextKey = .Item("PROVE")
            If (iTipoRecepcion = Nothing) Then
                If (.Item("TIPORECEPCION") = 0) Then
                    WNumEdPrecArtDet.Visible = (pag.FSNUser.ModificarPrecios And .Item("MODIF_PREC") = 1)
                    LblPrecArt0.Visible = Not WNumEdPrecArtDet.Visible
                    If Modificar Then
                        WNumEdPrecArtDet.ValueDouble = Precio
                    Else
                        WNumEdPrecArtDet.ValueDouble = CDbl(.Item("PRECIO_UC")) * CDbl(.Item("FC_DEF"))
                    End If
                    LblPrecArt0.Text = FSNLibrary.FormatNumber(WNumEdPrecArtDet.ValueDouble, pag.Usuario.NumberFormat)
                    LblUniDet.Text = Me.Textos(83)
                    If String.IsNullOrEmpty(Unidad) Then
                        LblMonUniDet.Text = .Item("MON") & "/" & .Item("UP_DEF")
                    Else
                        LblMonUniDet.Text = .Item("MON") & "/" & Unidad
                    End If

                    configurarDecimales(.Item("UP_DEF"), WNumEdCantidadDet)

                    'Insertamos un script que controla el evento paste en el control
                    CType(Me.Page, FSEPPage).insertarScriptPaste(WNumEdCantidadDet.ClientID)

                    If String.IsNullOrEmpty(Unidad) Then
                        Dim MaxValue As Nullable(Of Double) = DBNullToDbl(.Item("CANT_MAX_DEF"))
                        WNumEdCantidadDet.MinValue = IIf(IsDBNull(.Item("CANT_MIN_DEF")), 0, .Item("CANT_MIN_DEF"))
                        'WNumEdCantidadDet.MaxValue = IIf(IsDBNull(.Item("CANT_MAX_DEF")), Nothing, .Item("CANT_MAX_DEF"))
                        If MaxValue <> 0 Then
                            WNumEdCantidadDet.MaxValue = IIf(MaxValue = 0, Nothing, MaxValue)
                        End If
                    Else
                        WNumEdCantidadDet.MinValue = CantidadMinima
                        If CantidadMaxima <> 0 Then
                            WNumEdCantidadDet.MaxValue = CantidadMaxima
                        End If
                    End If
                    If Modificar Then
                        WNumEdCantidadDet.ValueDouble = Cantidad
                    Else
                        WNumEdCantidadDet.ValueDouble = IIf(Cantidad = 0, IIf(WNumEdCantidadDet.MinValue = 0, 1, WNumEdCantidadDet.MinValue), Cantidad)
                    End If
                    WNumEdCantidadDet.Visible = True
                    WNumEdCantidadDet.Style("display") = ""
                Else
                    LblUniDet.Text = Textos(77)
                    WNumEdPrecArtDet.Visible = False
                    LblPrecArt0.Visible = True
                    LblMonUniDet.Text = .Item("MON")


                    'Insertamos un script que controla el evento paste en el control
                    CType(Me.Page, FSEPPage).insertarScriptPaste(WNumEdCantidadDet.ClientID)

                    Dim MinValue As Nullable(Of Double) = DBNullToDbl(.Item("CANT_MIN_DEF"))
                    Dim MaxValue As Nullable(Of Double) = DBNullToDbl(.Item("CANT_MAX_DEF"))
                    Dim dMostrar As Double = 0
                    If String.IsNullOrEmpty(Unidad) Then

                        WNumEdPrecArtDet.ValueDouble = Cantidad 'IIf(MinValue = 0, DBNullToDbl(.Item("PRECIO_UC")), MinValue).ToString 'IIf(MinValue = 0, DBNullToDbl(.Item("PRECIO_UC")), MinValue).ToString
                        WNumEdCantidadDet.ValueDouble = Cantidad 'IIf(MinValue = 0, DBNullToDbl(.Item("PRECIO_UC")), MinValue).ToString 'IIf(MinValue = 0, DBNullToDbl(.Item("PRECIO_UC")), MinValue).ToString
                        WNumEdCantidadDet.MinValue = IIf(MinValue = 0, DBNullToDbl(.Item("PRECIO_UC")), MinValue).ToString 'IIf(IsDBNull(.Item("CANT_MIN_DEF")), 0, .Item("CANT_MIN_DEF"))
                        dMostrar = IIf(Cantidad = 0, IIf(MinValue = 0, DBNullToDbl(.Item("PRECIO_UC")), MinValue), Cantidad)
                        If MaxValue <> 0 Then
                            WNumEdCantidadDet.MaxValue = MaxValue
                        End If

                        'WNumEdCantidadDet.MaxValue = IIf(MaxValue = 0, DBNullToDbl(.Item("PRECIO_UC")), MaxValue).ToString 'IIf(IsDBNull(.Item("CANT_MAX_DEF")), 0, .Item("CANT_MAX_DEF"))
                    Else
                        WNumEdPrecArtDet.ValueDouble = CantidadMinima
                        WNumEdCantidadDet.ValueDouble = CantidadMinima
                        WNumEdCantidadDet.MinValue = CantidadMinima
                        If CantidadMaxima <> 0 Then
                            WNumEdCantidadDet.MaxValue = CantidadMaxima
                        End If
                    End If



                    WNumEdCantidadDet.Visible = True
                    WNumEdCantidadDet.Style("display") = ""

                    LblPrecArt0.Text = FSNLibrary.FormatNumber(dMostrar, pag.Usuario.NumberFormat)

                    configurarDecimales(.Item("UP_DEF"), WNumEdCantidadDet)
                End If
            Else
                If (iTipoRecepcion = 0) Then
                    WNumEdPrecArtDet.Visible = (pag.FSNUser.ModificarPrecios And .Item("MODIF_PREC") = 1)
                    LblPrecArt0.Visible = Not WNumEdPrecArtDet.Visible
                    If Modificar Then
                        WNumEdPrecArtDet.ValueDouble = Precio
                    Else
                        WNumEdPrecArtDet.ValueDouble = CDbl(.Item("PRECIO_UC")) * CDbl(.Item("FC_DEF"))
                    End If
                    LblPrecArt0.Text = FSNLibrary.FormatNumber(WNumEdPrecArtDet.ValueDouble, pag.Usuario.NumberFormat)
                    LblUniDet.Text = Me.Textos(83)
                    If String.IsNullOrEmpty(Unidad) Then
                        LblMonUniDet.Text = .Item("MON") & "/" & .Item("UP_DEF")
                    Else
                        LblMonUniDet.Text = .Item("MON") & "/" & Unidad
                    End If

                    configurarDecimales(.Item("UP_DEF"), WNumEdCantidadDet)

                    'Insertamos un script que controla el evento paste en el control
                    CType(Me.Page, FSEPPage).insertarScriptPaste(WNumEdCantidadDet.ClientID)

                    If String.IsNullOrEmpty(Unidad) Then
                        Dim MaxValue As Nullable(Of Double) = DBNullToDbl(.Item("CANT_MAX_DEF"))
                        WNumEdCantidadDet.MinValue = IIf(IsDBNull(.Item("CANT_MIN_DEF")), 0, .Item("CANT_MIN_DEF"))
                        'WNumEdCantidadDet.MaxValue = IIf(IsDBNull(.Item("CANT_MAX_DEF")), Nothing, .Item("CANT_MAX_DEF"))
                        If MaxValue <> 0 Then
                            WNumEdCantidadDet.MaxValue = IIf(MaxValue = 0, Nothing, MaxValue)
                        End If
                    Else
                        WNumEdCantidadDet.MinValue = CantidadMinima
                        If CantidadMaxima <> 0 Then
                            WNumEdCantidadDet.MaxValue = CantidadMaxima
                        End If
                    End If
                    If Modificar Then
                        WNumEdCantidadDet.ValueDouble = Cantidad
                    Else
                        WNumEdCantidadDet.ValueDouble = IIf(Cantidad = 0, WNumEdCantidadDet.MinValue, Cantidad)
                    End If
                    WNumEdCantidadDet.Visible = True
                    WNumEdCantidadDet.Style("display") = ""
                Else
                    LblUniDet.Text = Textos(77)
                    WNumEdPrecArtDet.Visible = False
                    LblPrecArt0.Visible = True
                    LblMonUniDet.Text = .Item("MON")


                    'Insertamos un script que controla el evento paste en el control
                    CType(Me.Page, FSEPPage).insertarScriptPaste(WNumEdCantidadDet.ClientID)

                    Dim MinValue As Nullable(Of Double) = DBNullToDbl(.Item("CANT_MIN_DEF"))
                    Dim MaxValue As Nullable(Of Double) = DBNullToDbl(.Item("CANT_MAX_DEF"))
                    If String.IsNullOrEmpty(Unidad) Then

                        WNumEdPrecArtDet.ValueDouble = Cantidad 'IIf(MinValue = 0, DBNullToDbl(.Item("PRECIO_UC")), MinValue).ToString 'IIf(MinValue = 0, DBNullToDbl(.Item("PRECIO_UC")), MinValue).ToString
                        WNumEdCantidadDet.ValueDouble = Cantidad 'IIf(MinValue = 0, DBNullToDbl(.Item("PRECIO_UC")), MinValue).ToString 'IIf(MinValue = 0, DBNullToDbl(.Item("PRECIO_UC")), MinValue).ToString
                        WNumEdCantidadDet.MinValue = IIf(MinValue = 0, DBNullToDbl(.Item("PRECIO_UC")), MinValue).ToString 'IIf(IsDBNull(.Item("CANT_MIN_DEF")), 0, .Item("CANT_MIN_DEF"))
                        If MaxValue <> 0 Then
                            WNumEdCantidadDet.MaxValue = MaxValue
                        End If
                    Else
                        WNumEdPrecArtDet.ValueDouble = CantidadMinima
                        WNumEdCantidadDet.ValueDouble = CantidadMinima
                        WNumEdCantidadDet.MinValue = CantidadMinima
                        If CantidadMaxima <> 0 Then
                            WNumEdCantidadDet.MaxValue = CantidadMaxima
                        End If
                    End If

                    WNumEdCantidadDet.Visible = True
                    WNumEdCantidadDet.Style("display") = ""

                    LblPrecArt0.Text = FSNLibrary.FormatNumber(WNumEdPrecArtDet.ValueDouble, pag.Usuario.NumberFormat)

                    configurarDecimales(.Item("UP_DEF"), WNumEdCantidadDet)
                End If
            End If
            If bEP Then
                WNumEdCantidadDet.ReadOnly = True
                WNumEdPrecArtDet.ReadOnly = True
                BtnAnyadirCestaDet.Visible = False
            End If

            If Not IsDBNull(.Item("COD_ITEM")) And Not IsDBNull(.Item("PROVE")) Then
                Dim oCArticulos As FSNServer.cArticulos = pag.FSNServer.Get_Object(GetType(FSNServer.cArticulos))
                Dim dsAtrib As DataSet = oCArticulos.DevolverAtributosArt(DBNullToSomething(.Item("COD_ITEM")), .Item("PROVE"))
                Dim atribs As DataRow() = dsAtrib.Tables(0).Select
                pnlCaracteristicas.Visible = atribs.Length > 0
                LblCaracteristicas.Text = Me.Textos(135)
                Dim mLista As New List(Of ListItem)
                For Each d As DataRow In atribs
                    If IsDBNull(d.Item("VALOR")) Then
                        mLista.Add(New ListItem(d.Item("DEN") & ":", String.Empty))
                    Else

                        Select Case d.Item("TIPO")
                            Case 1
                                mLista.Add(New ListItem(d.Item("DEN") & ":", d.Item("VALOR")))
                            Case 2
                                mLista.Add(New ListItem(d.Item("DEN") & ":", FSNLibrary.FormatNumber(d.Item("VALOR"), pag.Usuario.NumberFormat)))
                            Case 3
                                mLista.Add(New ListItem(d.Item("DEN") & ":", FSNLibrary.FormatDate(d.Item("VALOR"), pag.Usuario.DateFormat)))
                            Case 4
                                mLista.Add(New ListItem(d.Item("DEN") & ":", IIf(d.Item("VALOR"), Textos(69), Textos(70))))
                        End Select
                    End If
                Next
                lstAtributos.DataSource = mLista
                lstAtributos.DataBind()


                Dim dsAtribEspecificacion As DataSet = oCArticulos.DevolverAtributosEspecificacionArt(DBNullToSomething(.Item("COD_ITEM")), .Item("PROVE"))
                Dim atribsEspecificacion As DataRow() = dsAtribEspecificacion.Tables(0).Select
                pnlAtributosEspecificacion.Visible = atribsEspecificacion.Length > 0
                LblAtributosEspecificacion.Text = Me.Textos(171)
                Dim mListaAtributosEspecificacion As New List(Of ListItem)
                For Each d As DataRow In atribsEspecificacion
                    If IsDBNull(d.Item("VALOR")) Then
                        mListaAtributosEspecificacion.Add(New ListItem(d.Item("DEN") & ":", String.Empty))
                    Else

                        Select Case d.Item("TIPO")
                            Case 1
                                mListaAtributosEspecificacion.Add(New ListItem(d.Item("DEN") & ":", d.Item("VALOR")))
                            Case 2
                                mListaAtributosEspecificacion.Add(New ListItem(d.Item("DEN") & ":", FSNLibrary.FormatNumber(d.Item("VALOR"), pag.Usuario.NumberFormat)))
                            Case 3
                                mListaAtributosEspecificacion.Add(New ListItem(d.Item("DEN") & ":", FSNLibrary.FormatDate(d.Item("VALOR"), pag.Usuario.DateFormat)))
                            Case 4
                                mListaAtributosEspecificacion.Add(New ListItem(d.Item("DEN") & ":", IIf(d.Item("VALOR"), Textos(69), Textos(70))))
                        End Select
                    End If
                Next
                lstAtributosEspecificacion.DataSource = mListaAtributosEspecificacion
                lstAtributosEspecificacion.DataBind()

            End If

            Dim Altura As Integer = hddAlturaPantalla.Value
            If pnlCaracteristicas.Visible And grdAdjuntos.Visible Then
                pnlCaracteristicas.Style.Item("max-height") = CInt((Altura - 550) / 2) & "px"
                pnlAdjuntos.Style.Item("max-height") = CInt((Altura - 550) / 2) & "px"
            ElseIf pnlCaracteristicas.Visible Then
                pnlCaracteristicas.Style.Item("max-height") = Altura - 550 & "px"
            ElseIf grdAdjuntos.Visible Then
                pnlAdjuntos.Style.Item("max-height") = Altura - 550 & "px"
            End If

            BtnAnyadirCestaDet.CommandArgument = IdArt
            If Modificar Then
                BtnAnyadirCestaDet.Text = Me.Textos(140)
                BtnAnyadirCestaDet.ToolTip = Me.Textos(140)
                BtnAnyadirCestaDet.CommandName = "Modificar"
            Else
                If bEP Then
                    BtnAnyadirCestaDet.Text = Me.Textos(140)
                    BtnAnyadirCestaDet.ToolTip = Me.Textos(140)
                    BtnAnyadirCestaDet.CommandName = "Aceptar"
                Else
                    BtnAnyadirCestaDet.Text = Me.Textos(87)
                    BtnAnyadirCestaDet.ToolTip = Me.Textos(87)
                    BtnAnyadirCestaDet.CommandName = "Añadir"
                End If
            End If
        End With
        UpdatePanelModal.Update()
        MPEImgBtnArt.Show()
    End Sub

    ''' <summary>
    ''' Procedimiento que se utiliza para añadir un artículo a la cesta desde la ventana de detalle
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce cuando se hace clic en el control FSNButton BtnAnyadirCestaDet.</remarks>
    Protected Sub BtnAnyadirCestaDet_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnAnyadirCestaDet.Click
        If CType(sender, FSNWebControls.FSNButton).CommandName <> "Aceptar" Then
            Dim Cantidad As Double = WNumEdCantidadDet.ValueDouble
            If CType(sender, FSNWebControls.FSNButton).CommandName = "Modificar" Then
                If Cantidad > 0 Then
                    RaiseEvent AnyadirACestaEP(CType(sender, FSNWebControls.FSNButton).CommandArgument, Cantidad, WNumEdPrecArtDet.ValueDouble, Me.ViewState("Key"))
                    MPEImgBtnArt.Hide()
                End If
            Else
                If Cantidad > 0 Then
                    RaiseEvent AnyadirACesta(CType(sender, FSNWebControls.FSNButton).CommandArgument, Cantidad, WNumEdPrecArtDet.ValueDouble, Me.ViewState("Key"))
                    MPEImgBtnArt.Hide()
                End If
            End If
            'Dim Cantidad As Double = WNumEdCantidadDet.ValueDouble
            'If Cantidad > 0 Then
            '    RaiseEvent AnyadirACestaEP(CType(sender, FSNWebControls.FSNButton).CommandArgument, Cantidad, WNumEdPrecArtDet.ValueDouble, Me.ViewState("Key"))
            '    MPEImgBtnArt.Hide()
            'End If
        Else
            MPEImgBtnArt.Hide()
        End If
    End Sub

    ''' <summary>
    ''' Inicializa los controles con los datos de cada adjunto
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">Objeto GridViewRowEventArgs que contiene los datos del evento.</param>
    ''' <remarks>Se produce cuando una fila de datos se enlaza a los datos del control GridView.</remarks>
    Private Sub grdAdjuntos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdAdjuntos.RowDataBound
        With e.Row
            If .RowType = DataControlRowType.DataRow Then
                Dim pag As FSNPage = Me.Page
                Dim oEsp As CEspecificacion = .DataItem
                Dim idArt As Integer = CType(sender, WebControl).Attributes("ARTICULO")
                CType(.FindControl("litFechaAdjunto"), Literal).Text = FormatDate(oEsp.Fecha, pag.Usuario.DateFormat)
                CType(.FindControl("lnkAdjunto"), HyperLink).Text = oEsp.Nombre
                CType(.FindControl("lnkAdjunto"), HyperLink).NavigateUrl = ConfigurationManager.AppSettings("rutaEP") & "/_common/Adjuntos.aspx?adjunto=" & oEsp.Id & "&tipo=" & TiposDeDatos.Adjunto.Tipo.EspecificacionLineaCatalogo & "&linea=" & idArt
                If String.IsNullOrEmpty(oEsp.Comentario) Then
                    .FindControl("imgComentarioAdjunto").Visible = False
                Else
                    CType(.FindControl("imgComentarioAdjunto"), FSNWebControls.FSNImageTooltip).ContextKey = _
                        idArt & "#" & oEsp.Id
                End If
            End If
        End With
    End Sub

    ''' <summary>
    ''' Genera el script que especifica la altura máxima del control
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">EventArgs que contiene datos de eventos.</param>
    ''' <remarks>Se produce cuando el control de usuario se carga en el objeto Page.</remarks>
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.Page.ClientScript.IsStartupScriptRegistered(Me.GetType(), Me.ClientID & "_Init") Then
            Me.Page.ClientScript.RegisterStartupScript(Me.GetType, Me.ClientID & "_Init", _
                    "$get('" & hddAlturaPantalla.ClientID & "').value = window.screen.availHeight;", _
                    True)
        End If
    End Sub

#Region "Unidades"
    ''' <summary>
    ''' Propiedad que recupera el conjunto de las unidades de Pedido para su posterior uso en la página
    ''' </summary>
    Private ReadOnly Property TodasLasUnidades() As CUnidadesPedido
        Get
            Dim pag As FSNPage = CType(Me.Page, FSNPage)
            Dim oUnidadesPedido As CUnidadesPedido = pag.FSNServer.Get_Object(GetType(FSNServer.CUnidadesPedido))
            If HttpContext.Current.Cache("TodasLasUnidades" & pag.Usuario.Idioma.ToString()) Is Nothing Then
                oUnidadesPedido.CargarTodasLasUnidades(pag.Usuario.Idioma)
                pag.InsertarEnCache("TodasLasUnidades" & pag.Usuario.Idioma.ToString(), oUnidadesPedido)
            Else
                oUnidadesPedido = HttpContext.Current.Cache("TodasLasUnidades" & pag.Usuario.Idioma.ToString())
            End If
            Return oUnidadesPedido
        End Get
    End Property

    ''' <summary>
    ''' Método que recupera el número de decimales para una unidad de pedido dada y configura la presentación de los datos en los controles webnumericEdit
    ''' </summary>
    ''' <param name="unidad">Código de la unidad de pedido</param>
    ''' <param name="wNumericEdit">Control webNumericEdit a configurar</param>
    ''' <remarks>Llamada desde: la página, allí donde se configure un control webNumericEdit. Tiempo máximo: inferior a 0,3</remarks>
    Private Sub configurarDecimales(ByVal unidad As Object, ByRef wNumericEdit As Infragistics.Web.UI.EditorControls.WebNumericEditor)
        Dim pag As FSNPage = CType(Me.Page, FSNPage)
        Dim oUnidadPedido As FSNServer.CUnidadPedido = Nothing
        If Not DBNullToSomething(unidad) = Nothing Then
            unidad = Trim(unidad.ToString)
            oUnidadPedido = TodasLasUnidades.Item(unidad)
            oUnidadPedido.AsignarFormatoaUnidadPedido(pag.Usuario.NumberFormat)
        End If
        Dim ci As System.Globalization.CultureInfo = Threading.Thread.CurrentThread.CurrentCulture.Clone()
        ci.NumberFormat.NumberDecimalDigits = 15
        If Not oUnidadPedido Is Nothing Then
            wNumericEdit.Culture = ci
            If oUnidadPedido.NumeroDeDecimales Is Nothing Then
                wNumericEdit.MinDecimalPlaces = 0
                wNumericEdit.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Double
            ElseIf oUnidadPedido.NumeroDeDecimales > 0 Then
                wNumericEdit.MinDecimalPlaces = oUnidadPedido.NumeroDeDecimales
                wNumericEdit.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Double
            ElseIf oUnidadPedido.NumeroDeDecimales = 0 Then
                wNumericEdit.MinDecimalPlaces = oUnidadPedido.NumeroDeDecimales
                wNumericEdit.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Int
            End If
            wNumericEdit.ClientEvents.TextChanged = "limiteDecimalesTextChanged"
            wNumericEdit.Attributes.Add("numeroDecimales", ci.NumberFormat.NumberDecimalDigits)
            wNumericEdit.Attributes.Add("separadorDecimales", ci.NumberFormat.NumberDecimalSeparator)
            wNumericEdit.Attributes.Add("avisoDecimales", Textos(158) & " " & oUnidadPedido.Codigo & ":")
        End If
    End Sub

#End Region

End Class