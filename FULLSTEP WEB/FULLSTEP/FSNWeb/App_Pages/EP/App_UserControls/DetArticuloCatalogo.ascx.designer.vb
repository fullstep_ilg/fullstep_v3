﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class DetArticuloCatalogo

    '''<summary>
    '''btnOcultoDet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnOcultoDet As Global.System.Web.UI.HtmlControls.HtmlInputButton

    '''<summary>
    '''hddAlturaPantalla control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hddAlturaPantalla As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''pnlDetArticulo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlDetArticulo As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ImgBtnDetCerrar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgBtnDetCerrar As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''UpdatePanelModal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanelModal As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''FSNPanelDatosProveDet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FSNPanelDatosProveDet As Global.Fullstep.FSNWebControls.FSNPanelInfo

    '''<summary>
    '''pnlTooltipComAdj control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlTooltipComAdj As Global.Fullstep.FSNWebControls.FSNPanelTooltip

    '''<summary>
    '''ImgDetArt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgDetArt As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''LblDetNomArt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblDetNomArt As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''FSNLinkProveDet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FSNLinkProveDet As Global.Fullstep.FSNWebControls.FSNLinkInfo

    '''<summary>
    '''pnlAdjuntos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlAdjuntos As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''LblAdjuntos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblAdjuntos As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''litAdjuntos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litAdjuntos As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''grdAdjuntos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdAdjuntos As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''pnlCaracteristicas control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlCaracteristicas As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''LblCaracteristicas control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblCaracteristicas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lstAtributos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lstAtributos As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''pnlAtributosEspecificacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlAtributosEspecificacion As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''LblAtributosEspecificacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblAtributosEspecificacion As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lstAtributosEspecificacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lstAtributosEspecificacion As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''LblUniDet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblUniDet As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''LblPrecArt0 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblPrecArt0 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''WNumEdPrecArtDet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents WNumEdPrecArtDet As Global.Infragistics.Web.UI.EditorControls.WebNumericEditor

    '''<summary>
    '''LblMonUniDet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblMonUniDet As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''WNumEdCantidadDet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents WNumEdCantidadDet As Global.Infragistics.Web.UI.EditorControls.WebNumericEditor

    '''<summary>
    '''BtnAnyadirCestaDet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnAnyadirCestaDet As Global.Fullstep.FSNWebControls.FSNButton

    '''<summary>
    '''MPEImgBtnArt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents MPEImgBtnArt As Global.AjaxControlToolkit.ModalPopupExtender
End Class
