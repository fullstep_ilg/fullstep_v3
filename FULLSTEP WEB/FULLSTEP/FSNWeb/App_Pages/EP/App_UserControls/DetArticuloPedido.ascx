﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DetArticuloPedido.ascx.vb" Inherits="Fullstep.FSNWeb.DetArticuloPedido" %>
    <input id="btnOcultoDet" type="button" value="button" runat="server" style="display: none" />
    <input id="hddAlturaPantalla" type="hidden" runat="server" />
    <asp:Panel ID="pnlDetArticulo" runat="server" Style="padding: 10px; display:none;"
        CssClass="DetallArticulo">
        <table width="815">
            <tr>
                <td align="right" style="height: 20px;" valign="top">
                    <asp:Label ID="LblOcTipoArticulo" runat="server" style="display: none"></asp:Label>
                    <asp:ImageButton ID="ImgBtnDetCerrar" runat="server" ImageUrl="~/images/Bt_Cerrar.png"
                        ToolTip="Cerrar Detalle" />
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="UpdatePanelModal" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
            <ContentTemplate>
                <!-- Panel Info Proveedor -->
                <fsn:FSNPanelInfo ID="FSNPanelDatosProveDet" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx" ServiceMethod="Proveedor_DatosContacto" Contenedor="pnlDetArticulo" TipoDetalle="1">
                </fsn:FSNPanelInfo>
                <!-- Panel Tooltip Comentario Adjunto -->
                <fsn:FSNPanelTooltip ID="pnlTooltipComAdj" runat="server" ServiceMethod="ComentarioAdj" Contenedor="pnlDetArticulo">
                </fsn:FSNPanelTooltip>

                <table style="height: 480px">
                    <tr>
                        <td width="350" align="left" valign="top" rowspan="2" style="border-style:solid; border-width:1px; border-color:#666666; padding:2px; text-align: center; vertical-align: middle;">
                            <asp:UpdatePanel ID="UpImagen" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>
                                    <asp:Image ID="ImgDetArt" runat="server" ImageUrl="~/images/trans.gif" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td rowspan="2" width="20">&nbsp;</td>
                        <td valign="top" width="445">
                            <table style="width: 100%;">
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:Label ID="LblDetNomArt" runat="server" Text="Label" CssClass="Rotulo"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:Label ID="LblDescArtDet" runat="server" Text="Label" CssClass="Normal">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr align="left" valign="top">
                                    <td>
                                        <fsn:FSNLinkInfo ID="FSNLinkProveDet" runat="server" PanelInfo="FSNPanelDatosProveDet"
                                            CssClass="LinkButtonInfo"></fsn:FSNLinkInfo>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <!-- Especificaciones -->
                                
                                 <tr>
                                    <td align="left" valign="top">
                                        <asp:Panel ID="pnlEspecificaciones" runat="server" CssClass="Rectangulo" Style="padding: 4px;" ScrollBars="Auto" Visible="false">
                                            <asp:Label ID="lblEspecificaciones" runat="server" CssClass="Rotulo"></asp:Label>
                                            <asp:Literal ID="LitEspecificaciones" runat="server"></asp:Literal>
                                        </asp:Panel>
                                    </td>
                                </tr>    
                                
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:Panel ID="pnlCaracteristicas" runat="server" CssClass="Rectangulo" Style="padding: 4px;" ScrollBars="Auto">
                                            <asp:Label ID="LblCaracteristicas" runat="server" CssClass="Rotulo"></asp:Label>
                                            <asp:GridView ID="lstAtributos" runat="server" AutoGenerateColumns="False" CellPadding="6"
                                                GridLines="None" ShowHeader="False" Width="100%" RowStyle-CssClass="Normal">
                                                <Columns>
                                                    <asp:BoundField DataField="Text" />
                                                    <asp:BoundField DataField="Value" />
                                                </Columns>
                                                <AlternatingRowStyle BackColor="#F1F1F1" />
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:Panel ID="pnlAdjuntos" runat="server" CssClass="Rectangulo" Style="padding: 4px;" ScrollBars="Auto">
                                            <asp:Label ID="LblAdjuntos" runat="server" CssClass="Rotulo"></asp:Label>
                                            <asp:Literal ID="litAdjuntos" runat="server"></asp:Literal>
                                            <asp:GridView ID="grdAdjuntos" runat="server" ShowHeader="False" AutoGenerateColumns="False"
                                                CellPadding="4" GridLines="None">
                                                <Columns>
                                                    <asp:TemplateField ShowHeader="False" ItemStyle-Wrap="False">
                                                        <ItemTemplate>
                                                            <asp:Literal ID="litFechaAdjunto" runat="server"></asp:Literal>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ShowHeader="False" ItemStyle-Width="300px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkbtnAdjunto" runat="server" CommandName="Descargar" CssClass="Normal"
                                                                Style="text-decoration: none;"></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="300px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ShowHeader="False">
                                                        <ItemTemplate>
                                                            <fsn:FSNImageTooltip ID="imgComentarioAdjunto" runat="server" PanelTooltip="pnlTooltipComAdj"
                                                                SkinID="ObservacionesPeq" ContextKey='<%# Container.DataItem("ID") & " " & Container.DataItem("TIPO") %>' />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="FilaPar2" />
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" width="445" align="right">
                            <asp:Label ID="LblPrecArt0" runat="server" CssClass="PrecioArticulo"
                                Text='<%# DataBinder.Eval(Container, "DataItem.PRECIO_UC") * DataBinder.Eval(Container, "DataItem.FC_DEF") %>'>
                            </asp:Label>
                            &nbsp;
                            <asp:Label ID="LblMonUniDet" runat="server" CssClass="Normal" Text='<%# DataBinder.Eval(Container, "DataItem.MON") & "/" & DataBinder.Eval(Container, "DataItem.UP_DEF") & " x" %>'>
                            </asp:Label>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="MPEImgBtnArt" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="ImgBtnDetCerrar" Enabled="true"
    PopupControlID="pnlDetArticulo" RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="btnOcultoDet">
    </ajx:ModalPopupExtender>
