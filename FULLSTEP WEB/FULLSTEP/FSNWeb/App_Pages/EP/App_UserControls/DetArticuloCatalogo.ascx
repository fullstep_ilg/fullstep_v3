﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DetArticuloCatalogo.ascx.vb" Inherits="Fullstep.FSNWeb.DetArticuloCatalogo" %>

    <%-- 
        ESTE CONTROL DE USUARIO REQUIERE EL ARCHIVO jsLimiteDecimales.js SIN EMBARGO, PARA PODER INCLUIR DICHO 
        JAVASCRIPT EN EL SCRIPT COMPUESTO QUE GESTIONA EL SCRIPTMANAGER DE LA PÁGINA, VAMOS A COLOCARLO EN CADA PÁGINA
        DESDE LA QUE EL CONTROL ES LLAMADO. ES DECIR, SI AÑADES ESTE CONTROL A ALGUNA PÁGINA, ASEGÚRATE DE QUE ESA PÁGINA
        INCLUYE EL ARCHIVO jsLimiteDecimales.js TAL Y COMO LO HACE EmisionPEdido.aspx.vb EN SU EVENTO INIT
    --%>

    <input id="btnOcultoDet" type="button" value="button" runat="server" style="display: none" enableviewstate="false" />
    <input id="hddAlturaPantalla" type="hidden" runat="server" enableviewstate="false" />
    <!-- Panel detalle Articulo -->
    <asp:Panel ID="pnlDetArticulo" runat="server" Style="padding: 10px; display:none;"
        CssClass="DetallArticulo" enableviewstate="true">
        <table width="815">
            <tr>
                <td align="right" style="height: 20px;" valign="top">
                    <asp:ImageButton ID="ImgBtnDetCerrar" runat="server" ImageUrl="~/images/Bt_Cerrar.png"
                        ToolTip="Cerrar Detalle" enableviewstate="false" />
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="UpdatePanelModal" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False" enableviewstate="true">
            <ContentTemplate>
                <!-- Panel Info Proveedor -->
                <fsn:FSNPanelInfo ID="FSNPanelDatosProveDet" runat="server" ServicePath="~/App_Pages/EP/App_Services/EPConsultas.asmx" ServiceMethod="Proveedor_DatosContacto" Contenedor="pnlDetArticulo" TipoDetalle="1"  enableviewstate="false">
                </fsn:FSNPanelInfo>
                <!-- Panel Tooltip Comentario Adjunto -->
                <fsn:FSNPanelTooltip ID="pnlTooltipComAdj" runat="server" ServiceMethod="ComentarioEspecificacion" Contenedor="pnlDetArticulo"  enableviewstate="false">
                </fsn:FSNPanelTooltip>

                <table style="height: 480px">
                    <tr>
                        <td width="350" align="left" valign="top" rowspan="2" style="border-style:solid; border-width:1px; border-color:#666666; padding:2px; text-align: center; vertical-align: middle;">
                            <asp:Image ID="ImgDetArt" runat="server" ImageUrl="~/images/trans.gif" enableviewstate="false" />
                        </td>
                        <td rowspan="2" width="20">&nbsp;</td>
                        <td valign="top" width="445">
                            <table style="width: 100%;">
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:Label ID="LblDetNomArt" runat="server" Text="Label" CssClass="Rotulo" enableviewstate="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr><td>&nbsp;</td></tr>
                                <tr align="left" valign="top">
                                    <td>
                                        <fsn:FSNLinkInfo ID="FSNLinkProveDet" runat="server" PanelInfo="FSNPanelDatosProveDet" CssClass="LinkButtonInfo" enableviewstate="false"></fsn:FSNLinkInfo>
                                    </td>
                                </tr>
                                <tr><td>&nbsp;</td></tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:Panel ID="pnlAdjuntos" runat="server" CssClass="Rectangulo" Style="padding: 4px;" ScrollBars="Auto" Visible="false" enableviewstate="false">
                                            <asp:Label ID="LblAdjuntos" runat="server" CssClass="Rotulo"></asp:Label>
                                            <asp:Literal ID="litAdjuntos" runat="server"></asp:Literal>
                                            <asp:GridView ID="grdAdjuntos" runat="server" ShowHeader="False" AutoGenerateColumns="False"
                                                CellPadding="4" GridLines="None">
                                                <Columns>
                                                    <asp:TemplateField ShowHeader="False" ItemStyle-Wrap="False">
                                                        <ItemTemplate>
                                                            <asp:Literal ID="litFechaAdjunto" runat="server"></asp:Literal>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ShowHeader="False" ItemStyle-Width="300px">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="lnkAdjunto" runat="server" CssClass="Normal" style="text-decoration:none;"></asp:HyperLink>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="300px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ShowHeader="False">
                                                        <ItemTemplate>
                                                            <fsn:FSNImageTooltip ID="imgComentarioAdjunto" runat="server" PanelTooltip="pnlTooltipComAdj"
                                                                SkinID="ObservacionesPeq" />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="FilaPar2" />
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr><td>&nbsp;</td></tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:Panel ID="pnlCaracteristicas" runat="server" CssClass="Rectangulo" Style="padding: 4px;" ScrollBars="Auto" enableviewstate="false">
                                            <asp:Label ID="LblCaracteristicas" runat="server" CssClass="Rotulo"></asp:Label>
                                            <asp:GridView ID="lstAtributos" runat="server" AutoGenerateColumns="False" CellPadding="6"
                                                GridLines="None" ShowHeader="False" Width="100%" RowStyle-CssClass="Normal">
                                                <Columns>
                                                    <asp:BoundField DataField="Text" />
                                                    <asp:BoundField DataField="Value" />
                                                </Columns>
                                                <AlternatingRowStyle BackColor="#F1F1F1" />
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:Panel ID="pnlAtributosEspecificacion" runat="server" CssClass="Rectangulo" Style="padding: 4px;" ScrollBars="Auto" enableviewstate="false">
                                            <asp:Label ID="LblAtributosEspecificacion" runat="server" CssClass="Rotulo"></asp:Label>
                                            <asp:GridView ID="lstAtributosEspecificacion" runat="server" AutoGenerateColumns="False" CellPadding="6"
                                                GridLines="None" ShowHeader="False" Width="100%" RowStyle-CssClass="Normal">
                                                <Columns>
                                                    <asp:BoundField DataField="Text" />
                                                    <asp:BoundField DataField="Value" />
                                                </Columns>
                                                <AlternatingRowStyle BackColor="#F1F1F1" />
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" width="445" align="right">
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td align="right" colspan="2">
                                        <asp:Label ID="LblUniDet" runat="server" CssClass="Normal" Text="Unidades:" enableviewstate="false">
                                        </asp:Label>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="bottom">
                                        <asp:Label ID="LblPrecArt0" runat="server" CssClass="PrecioArticulo"
                                            Text='<%# DataBinder.Eval(Container, "DataItem.PRECIO_UC") * DataBinder.Eval(Container, "DataItem.FC_DEF") %>' enableviewstate="false">
                                        </asp:Label>
                                        <igpck:WebNumericEditor ID="WNumEdPrecArtDet" runat="server" Nullable="False" Visible="False" MinValue="0" CssClass="PrecioArticulo"
                                            Value='<%# DataBinder.Eval(Container, "DataItem.PRECIO_UC") * DataBinder.Eval(Container, "DataItem.FC_DEF") %>' Width="80px" enableviewstate="false">
                                        </igpck:WebNumericEditor> &nbsp; 
                                        <asp:Label ID="LblMonUniDet" runat="server" CssClass="Normal" Text='<%# DataBinder.Eval(Container, "DataItem.MON") & "/" & DataBinder.Eval(Container, "DataItem.UP_DEF") & " x" %>' enableviewstate="false">
                                        </asp:Label> &nbsp;
                                    </td>
                                    <td valign="bottom">
                                        <igpck:WebNumericEditor ID="WNumEdCantidadDet" runat="server" Nullable="False" Width="80px" Visible="false" enableviewstate="false">
                                            <Buttons SpinButtonsDisplay="OnRight"></Buttons>
                                        </igpck:WebNumericEditor>
                                     </td>
                                </tr>
                                <tr>
                                    <td valign="bottom" align="right" colspan="2">
                                        <br />
                                        <fsn:FSNButton ID="BtnAnyadirCestaDet" runat="server" Text="Añadir a cesta" enableviewstate="true"></fsn:FSNButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="MPEImgBtnArt" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="ImgBtnDetCerrar" Enabled="true"
    PopupControlID="pnlDetArticulo" RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="btnOcultoDet" enableviewstate="false">
    </ajx:ModalPopupExtender>
