﻿Imports Fullstep.FSNServer
Imports Fullstep
Imports Infragistics.Web.UI.GridControls

Public Class PaginadorWebHierarchical
    Inherits System.Web.UI.UserControl

    Const _ModuloIdioma As TiposDeDatos.ModulosIdiomas = Fullstep.FSNLibrary.TiposDeDatos.ModulosIdiomas.EP_PaginadorWebHierarchical
    Private _Textos As DataSet

    ''' <summary>
    ''' Devuelve el texto solicitado en el idioma que se haya definido.
    ''' </summary>
    Private ReadOnly Property Textos(ByVal iTexto As Integer) As String
        Get
            Dim pag As FSNWeb.FSNPage = CType(Me.Page, FSNWeb.FSNPage)
            If _Textos Is Nothing Then
                _Textos = CType(HttpContext.Current.Cache("Textos_" & pag.Idioma & "_" & _ModuloIdioma), DataSet)
            End If
            If _Textos Is Nothing Then
                Dim FSNDict As Dictionary = pag.FSNServer.Get_Dictionary
                FSNDict.LoadData(_ModuloIdioma, pag.Idioma)
                _Textos = FSNDict.Data
                pag.InsertarEnCache("Textos_" & pag.Idioma & "_" & _ModuloIdioma, _Textos)
            End If
            Return _Textos.Tables(0).Rows(iTexto).Item(1)
        End Get
    End Property
#Region "Total Registros y Numeración de páginas"
    ''' <summary>
    ''' Total registros
    ''' </summary>
    Public Property TotalRecordCount() As Integer
        Get
            Return m_TotalRecordCount
        End Get
        Set(ByVal value As Integer)
            m_TotalRecordCount = value
        End Set
    End Property
    Private m_TotalRecordCount As Integer
    ''' <summary>
    ''' Número de registros por página
    ''' </summary>
    Public Property PageSize() As Integer
        Get
            Return m_PageSize
        End Get
        Set(ByVal value As Integer)
            m_PageSize = value
        End Set
    End Property
    Private m_PageSize As Integer
    ''' Revisado por: blp. Fecha:30/01/2012
    ''' <summary>
    ''' Establece el total de registros y el número de registros por página
    ''' </summary>
    ''' <param name="totalRecordCount">Número de registros total</param>
    ''' <param name="initialPageSize">Número de registros por página</param>
    ''' <remarks>Llamada desde entregasPedidos. Máx. 0,1 seg.</remarks>
    Public Sub SetContext(ByVal totalRecordCount As Integer, ByVal initialPageSize As Integer)
        Me.TotalRecordCount = totalRecordCount
        Me.PageSize = initialPageSize
    End Sub
    ''' <summary>
    ''' Número de página en el que se encuentra el control
    ''' </summary>
    Public Property PageNumber() As Integer
        Get
            If Me.ViewState("PageNumber") IsNot Nothing Then
                Return Convert.ToInt32(Me.ViewState("PageNumber"))
            Else
                Me.ViewState.Add("PageNumber", 1)
                Return 1
            End If
        End Get

        Set(ByVal value As Integer)
            If Me.ViewState("PageNumber") IsNot Nothing Then
                Me.ViewState("PageNumber") = value
            Else
                Me.ViewState.Add("PageNumber", value)
            End If
        End Set
    End Property
    Protected ReadOnly Property PageIndex() As Integer
        Get
            If Me.PageNumber > 1 Then
                Return Me.PageNumber - 1
            Else
                Return 0
            End If
        End Get
    End Property
    ''' <summary>
    ''' Total páginas
    ''' </summary>
    Protected ReadOnly Property TotalNumberOfPages() As Integer
        Get
            If Me.TotalRecordCount > 0 Then
                Dim total As Double = Me.TotalRecordCount / Me.PageSize
                If total - Math.Round(total, System.MidpointRounding.AwayFromZero) > 0 Then total += 1
                Return total
            Else
                Return 0
            End If
        End Get
    End Property
    ''' Revisado por: blp. Fecha:30/01/2012
    ''' <summary>
    ''' Añade al dropdownlist ddlPage tantos elementos como páginas tiene la grid
    ''' </summary>
    ''' <remarks>llamada desde ActualizarPaginador. Máx. 0,1 seg.</remarks>
    Private Sub FillPageNumbersList()
        Me.ddlPage.Items.Clear()
        For i As Integer = 1 To Me.TotalNumberOfPages
            Me.ddlPage.Items.Add(New ListItem(i.ToString(), i.ToString()))
        Next
    End Sub
    ''' Revisado por: blp. Fecha:30/01/2012
    ''' <summary>
    ''' Define el valor seleccionado en el control ddlPage
    ''' </summary>
    ''' <remarks>Llamada desde ActualizarPaginador. Máx. 0,1 seg.</remarks>
    Private Sub FormatPageNumbersList()
        If Me.ddlPage.Items.Count >= Me.PageNumber Then
            Me.ddlPage.SelectedIndex = Me.PageIndex
        Else
            If ddlPage.Items.Count > 0 Then _
                Me.ddlPage.SelectedIndex = 0
        End If
    End Sub
#End Region
#Region "Formato"
    ''' Revisado por: blp. Fecha: 23/11/2011
    ''' <summary>
    ''' Procedimiento en el que configuramos los controles relacionados con la paginación
    ''' </summary>
    ''' <remarks>Llamada desde ActualizarPaginador
    ''' Tiempo máximo: 0 sec.</remarks>
    Private Sub FormatArrowControls()
        If Me.TotalNumberOfPages > 1 Then
            Dim pgNo As Integer = Me.PageNumber
            Me.btnFirstPage.Enabled = (pgNo <> 1)
            Me.btnPreviousPage.Enabled = (pgNo <> 1)
            Me.btnNextPage.Enabled = (pgNo <> Me.TotalNumberOfPages)
            Me.btnLastPage.Enabled = (pgNo <> Me.TotalNumberOfPages)

            If (pgNo <> 1) Then
                imFirstPage.ImageUrl = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/primero.gif"
                imPreviousPage.ImageUrl = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/anterior.gif"
            Else
                imFirstPage.ImageUrl = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/primero_desactivado.gif"
                imPreviousPage.ImageUrl = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/anterior_desactivado.gif"
            End If

            If (pgNo <> Me.TotalNumberOfPages) Then
                imNextPage.ImageUrl = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/siguiente.gif"
                imLastPage.ImageUrl = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/ultimo.gif"
            Else
                imNextPage.ImageUrl = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/siguiente_desactivado.gif"
                imLastPage.ImageUrl = ConfigurationManager.AppSettings("RUTA") & "App_Themes/" & Me.Page.Theme & "/images/ultimo_desactivado.gif"
            End If

            btnFirstPage.Visible = True
            btnPreviousPage.Visible = True
            lblPagina.Visible = True
            ddlPage.Visible = True
            lblPagTot.Visible = True
            btnNextPage.Visible = True
            btnLastPage.Visible = True
            imFirstPage.Visible = True
            imPreviousPage.Visible = True
            imNextPage.Visible = True
            imLastPage.Visible = True
        Else
            btnFirstPage.Visible = False
            btnPreviousPage.Visible = False
            lblPagina.Visible = False
            ddlPage.Visible = False
            lblPagTot.Visible = False
            btnNextPage.Visible = False
            btnLastPage.Visible = False
            imFirstPage.Visible = False
            imPreviousPage.Visible = False
            imNextPage.Visible = False
            imLastPage.Visible = False
        End If
    End Sub
#End Region
    ''' Revisado por: blp. Fecha:30/01/2012
    ''' <summary>
    ''' Método que se ejecuta en el prerender del control
    ''' </summary>
    ''' <param name="sender">Control que lanza el evento</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el evento PreRender. Máx. 0,1 seg.</remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        ActualizarPaginador()
    End Sub
    ''' Revisado por:blp. Fecha:23/11/2011
    ''' <summary>
    ''' Método que gestiona el cambio de página
    ''' </summary>
    ''' <remarks>Llamada desde los controles del paginador. Máx. 0,1 seg.</remarks>
    Protected Sub changePage(ByVal sender As Object, ByVal e As EventArgs)
        If TypeOf sender Is DropDownList Then
            Me.PageNumber = Convert.ToInt32(DirectCast(sender, DropDownList).SelectedValue)
        ElseIf TypeOf sender Is LinkButton Then
            Select Case DirectCast(sender, LinkButton).CommandArgument
                Case "first"
                    Me.PageNumber = 1
                    Exit Select
                Case "prev"
                    Me.PageNumber -= 1
                    Exit Select
                Case "next"
                    Me.PageNumber += 1
                    Exit Select
                Case "last"
                    Me.PageNumber = Me.TotalNumberOfPages
                    Exit Select
                Case Else
                    Exit Select
            End Select
        End If

        Me.RaiseChangePageEvent()
    End Sub
    Public Event PageChanged As EventHandler(Of PageSettingsChangedEventArgs)
    ''' Revisado por:blp. Fecha:23/11/2011
    ''' <summary>
    ''' Método que lanza un evento que incluye una serie de parámetros como argumentos del evento
    ''' </summary>
    ''' <remarks>Llamada desde changePage. Máx. 0,1 seg.</remarks>
    Private Sub RaiseChangePageEvent()
        RaiseEvent PageChanged(Me, New PageSettingsChangedEventArgs() With { _
                                .PageNumber = Me.PageNumber, _
                                .TotalNumberOfPages = Me.TotalNumberOfPages _
                                })
    End Sub
    ''' Revisado por: blp. Fecha:30/01/2012
    ''' <summary>
    ''' Actualiza el paginador para que muestre los datos correctos de página, total registros, etc.
    ''' </summary>
    ''' <param name="oPag">Paginador a modificar</param>
    ''' <remarks>Llamada desde entregasPedidos.aspx.vb->_Paginador_PageChanged y Paginasdor.ascx.vb->Page_PreRender. Máx. 0,1 seg.</remarks>
    Public Sub ActualizarPaginador(Optional ByVal oPag As PaginadorWebHierarchical = Nothing)
        If oPag Is Nothing Then oPag = Me

        oPag.FillPageNumbersList()
        oPag.FormatPageNumbersList()
        oPag.FormatArrowControls()

        'Cargar Textos
        oPag.lblPagina.Text = Textos(0)
        oPag.lblPagTot.Text = Textos(1) & " " & TotalNumberOfPages.ToString & "."
        oPag.lblTotal.Text = " " & Textos(2) & " " & TotalRecordCount.ToString

        oPag.upPaginador.Update()
    End Sub
End Class