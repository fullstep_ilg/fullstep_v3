﻿Imports Fullstep.FSNServer

Partial Public Class Paginador
    Inherits System.Web.UI.UserControl

    Const _ModuloIdioma As TiposDeDatos.ModulosIdiomas = TiposDeDatos.ModulosIdiomas.EP_Seguimiento
    Private _Textos As DataSet
    Private _SentidoOrdenacion As String
    Private _CampoOrden As String

    Public Event CambioOrden(ByVal CampoOrden As String, ByVal SentidoOrdenacion As String)
    Public Event Exportar()
    Public Event Imprimir()

    Public Property SentidoOrdenacion() As String
        Get
            If String.IsNullOrEmpty(_SentidoOrdenacion) Then
                If String.IsNullOrEmpty(ViewState("SentidoOrdenacion")) Then
                    Return "DESC"
                Else
                    Return ViewState("SentidoOrdenacion")
                End If
            Else
                Return _SentidoOrdenacion
            End If
        End Get
        Set(ByVal value As String)
            If _SentidoOrdenacion <> value Then
                _SentidoOrdenacion = value
                ViewState("SentidoOrdenacion") = _SentidoOrdenacion
            End If
        End Set
    End Property

    Public Property ListOrden() As ListItemCollection
        Get
            Return ddlOrdenacion.Items
        End Get
        Set(ByVal value As ListItemCollection)
            ddlOrdenacion.Items.Clear()
            For Each l As ListItem In value
                ddlOrdenacion.Items.Add(l)
            Next
        End Set
    End Property

    Public Property CampoOrden() As String
        Get
            If String.IsNullOrEmpty(_CampoOrden) Then
                If String.IsNullOrEmpty(ViewState("CampoOrden")) Then
                    Return "FECHA"
                Else
                    Return ViewState("CampoOrden")
                End If
            Else
                Return _CampoOrden
            End If
        End Get
        Set(ByVal value As String)
            If _CampoOrden <> value Then
                _CampoOrden = value
                ddlOrdenacion.SelectedValue = _CampoOrden
                ViewState("CampoOrden") = _CampoOrden
            End If
        End Set
    End Property

    Private Property Datasource() As PagedDataSource
        Get
            Dim o As Control = Me.NamingContainer
            While Not TypeOf o Is BaseDataList
                o = o.NamingContainer
            End While
            Return CType(o, BaseDataList).DataSource
        End Get
        Set(ByVal value As PagedDataSource)
            Dim o As Control = Me.NamingContainer
            While Not TypeOf o Is BaseDataList
                o = o.NamingContainer
            End While
            CType(o, BaseDataList).DataSource = value
        End Set
    End Property

    Private ReadOnly Property Textos(ByVal iTexto As Integer) As String
        Get
            Dim pag As IFSNPage = CType(Me.Page, IFSNPage)
            If _Textos Is Nothing Then
                _Textos = CType(HttpContext.Current.Cache("Textos_" & pag.Idioma & "_" & _ModuloIdioma), DataSet)
            End If
            If _Textos Is Nothing Then
                Dim FSNDict As FSNServer.Dictionary = pag.FSNServer.Get_Object(GetType(FSNServer.Dictionary))
                FSNDict.LoadData(_ModuloIdioma, pag.Idioma)
                _Textos = FSNDict.Data
                HttpContext.Current.Cache.Insert("Textos_" & pag.Idioma & "_" & _ModuloIdioma, _Textos, Nothing, Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            End If
            Return _Textos.Tables(0).Rows(iTexto).Item(1)
        End Get
    End Property

    ''' <summary>
    ''' Procedimiento en el que configuramos los controles relacionados con la paginación en función del contenido 
    ''' del objeto pds (pageddatasource). 
    ''' </summary>
    ''' <remarks>Llamada desde los controles que refrescan los resultados del accordion de pedidos
    ''' Tiempo máximo: 0 sec.</remarks>
    Public Sub InitPaginador()
        If Datasource IsNot Nothing Then

            If Datasource.PageCount > 1 Then

                'Actualizamos las combos de paginación
                Dim i As Integer
                ddlPage.Items.Clear()
                For i = 0 To Datasource.PageCount - 1
                    ddlPage.Items.Insert(i, New ListItem(i + 1, i))
                Next
                If Datasource.CurrentPageIndex >= ddlPage.Items.Count Then Datasource.CurrentPageIndex = ddlPage.Items.Count - 1
                ddlPage.Items(Datasource.CurrentPageIndex).Selected = True

                If (Datasource.CurrentPageIndex * Datasource.PageSize + Datasource.PageSize) > Datasource.DataSourceCount Then
                    lblResul.Text = CStr(Datasource.CurrentPageIndex * Datasource.PageSize + 1) & "-" & CStr(Datasource.DataSourceCount)
                    lblResulTot.Text = Datasource.DataSourceCount
                Else
                    lblResul.Text = CStr(Datasource.CurrentPageIndex * Datasource.PageSize + 1) & "-" & CStr(Datasource.CurrentPageIndex * Datasource.PageSize + Datasource.PageSize)
                    lblResulTot.Text = Datasource.DataSourceCount
                End If

                lblPagTot.Text = Datasource.PageCount

                If Datasource.IsFirstPage Then
                    btnFirstPage.Enabled = False
                    btnFirstPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero_desactivado.gif")
                    btnPreviousPage.Enabled = False
                    btnPreviousPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior_desactivado.gif")
                Else
                    btnFirstPage.Enabled = True
                    btnFirstPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero.gif")
                    btnPreviousPage.Enabled = True
                    btnPreviousPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior.gif")
                End If

                If Datasource.IsLastPage Then
                    btnNextPage.Enabled = False
                    btnNextPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente_desactivado.gif")
                    btnLastPage.Enabled = False
                    btnLastPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo_desactivado.gif")
                Else
                    btnNextPage.Enabled = True
                    btnNextPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente.gif")
                    btnLastPage.Enabled = True
                    btnLastPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo.gif")
                End If
            Else
                lblViendo.Visible = False
                lblResul.Visible = False
                lblResulDe.Visible = False
                lblResulTot.Visible = False
                btnFirstPage.Visible = False
                btnPreviousPage.Visible = False
                lblPagina.Visible = False
                ddlPage.Visible = False
                lblDe.Visible = False
                lblPagTot.Visible = False
                btnNextPage.Visible = False
                btnLastPage.Visible = False
            End If

            If Datasource.DataSourceCount = 0 Then
                imgbtnExportar.Enabled = False
                imgbtnExportar.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "excel_desactivado.gif")
                imgbtnImprimir.Enabled = False
                imgbtnImprimir.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "imprimir_desactivado.gif")
                lblOrdenacion.Visible = False
                ddlOrdenacion.Visible = False
                imgBtnOrdenacion.Visible = False
            Else
                imgbtnExportar.Enabled = True
                imgbtnExportar.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "excel.gif")
                imgbtnImprimir.Enabled = True
                imgbtnImprimir.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "imprimir.gif")
                imgBtnOrdenacion.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, IIf(SentidoOrdenacion = "ASC", "ascendente.gif", "descendente.gif"))
            End If

        End If
    End Sub

    Public Sub ActualizarControl()
        Dim o As Control = Me.NamingContainer
        While Not TypeOf o Is BaseDataList
            o = o.NamingContainer
        End While
        o.DataBind()
    End Sub

    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de primera página
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de primera página
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnFirstPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFirstPage.Click
        If Datasource IsNot Nothing Then
            Datasource.CurrentPageIndex = 0
            InitPaginador()
            ActualizarControl()
        End If
    End Sub

    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de página anterior
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de página anterior
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnPreviousPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPreviousPage.Click
        If Datasource IsNot Nothing Then
            Datasource.CurrentPageIndex = ddlPage.SelectedIndex - 1
            'Datasource.CurrentPageIndex = Datasource.CurrentPageIndex - 1
            InitPaginador()
            ActualizarControl()
        End If
    End Sub

    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de página siguiente
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de página siguiente
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnNextPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNextPage.Click
        If Datasource IsNot Nothing Then
            Datasource.CurrentPageIndex = ddlPage.SelectedIndex + 1
            'Datasource.CurrentPageIndex = Datasource.CurrentPageIndex + 1
            InitPaginador()
            ActualizarControl()
        End If
    End Sub

    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de última página
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de última página
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnLastPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLastPage.Click
        If Datasource IsNot Nothing Then
            Datasource.CurrentPageIndex = Datasource.PageCount - 1
            InitPaginador()
            ActualizarControl()
        End If
    End Sub

    ''' <summary>
    ''' Evento que se lanza al seleccionar alguna página de la combo de paginación
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: La combo de paginación
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub ddlPage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPage.SelectedIndexChanged
        If Datasource IsNot Nothing Then
            Datasource.CurrentPageIndex = CType(sender, DropDownList).SelectedIndex
            InitPaginador()
            ActualizarControl()
        End If
    End Sub

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblViendo.Text = Textos(106)
        lblResulDe.Text = Textos(75)
        lblPagina.Text = Textos(74)
        lblDe.Text = Textos(75)
        lblOrdenacion.Text = Textos(107)

        ''Dim control As Control = sender.FindControl("imgbtnExportar")
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(sender.FindControl("imgbtnExportar"))

        ''control = sender.FindControl("imgbtnImprimir")
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(sender.FindControl("imgbtnImprimir"))
    End Sub

    Private Sub imgBtnOrdenacion_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnOrdenacion.Click
        SentidoOrdenacion = IIf(SentidoOrdenacion = "ASC", "DESC", "ASC")
        RaiseEvent CambioOrden(CampoOrden, SentidoOrdenacion)
    End Sub

    Private Sub imgbtnExportar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnExportar.Click
        RaiseEvent Exportar()
    End Sub

    Private Sub imgbtnImprimir_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnImprimir.Click
        RaiseEvent Imprimir()
    End Sub

    Private Sub ddlOrdenacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOrdenacion.SelectedIndexChanged
        If CampoOrden <> ddlOrdenacion.SelectedValue Then
            CampoOrden = ddlOrdenacion.SelectedValue
            RaiseEvent CambioOrden(CampoOrden, SentidoOrdenacion)
        End If
    End Sub

End Class