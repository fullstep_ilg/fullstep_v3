﻿Imports Fullstep.FSNLibrary
Imports Fullstep.FSNServer

Partial Public Class DetCategoria
    Inherits System.Web.UI.UserControl

    Private m_CatN1 As Object
    Private m_CatN2 As Object
    Private m_CatN3 As Object
    Private m_CatN4 As Object
    Private m_CatN5 As Object
    Private m_Nivel As Integer

    Const _ModuloIdioma As TiposDeDatos.ModulosIdiomas = TiposDeDatos.ModulosIdiomas.EP_CatalogoWeb
    Private _Textos As DataSet

    Private ReadOnly Property Textos(ByVal iTexto As Integer) As String
        Get
            If _Textos Is Nothing Then
                Dim pag As IFSNPage = CType(Me.Page, IFSNPage)
                Dim FSNDict As FSNServer.Dictionary = pag.FSNServer.Get_Object(GetType(FSNServer.Dictionary))
                FSNDict.LoadData(_ModuloIdioma, pag.Idioma)
                _Textos = FSNDict.Data
            End If
            Return _Textos.Tables(0).Rows(iTexto).Item(1)
        End Get
    End Property

    ''' <summary>
    ''' Función que crea la tabla de adjuntos de la Categoria
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde:CargarAdjuntosCategoria
    ''' Tiempo máximo: 0 seg </remarks>
    Public Function CrearTablasAdjuntosCategoria()
        Dim dtAdjuntos As DataTable = New DataTable("ADJUNTOS_CATEGORIA")
        dtAdjuntos.Columns.Add("ID", GetType(System.Int32))
        dtAdjuntos.Columns.Add("NOMBRE", GetType(System.String))
        dtAdjuntos.Columns.Add("COMENTARIO", GetType(System.String))
        dtAdjuntos.Columns.Add("FECHA", GetType(System.DateTime))
        dtAdjuntos.Columns.Add("DATASIZE", GetType(System.Double))
        dtAdjuntos.Columns.Add("TIPO", GetType(TiposDeDatos.Adjunto.Tipo))
        dtAdjuntos.Columns.Add("CATN1", GetType(System.Int32))
        dtAdjuntos.Columns.Add("CATN2", GetType(System.Int32))
        dtAdjuntos.Columns.Add("CATN3", GetType(System.Int32))
        dtAdjuntos.Columns.Add("CATN4", GetType(System.Int32))
        dtAdjuntos.Columns.Add("CATN5", GetType(System.Int32))
        dtAdjuntos.Columns.Add("NIVEL", GetType(System.Int32))

        Return dtAdjuntos
    End Function

    ''' <summary>
    ''' Procedimiento que añade la tabla de adjuntos de la Categoria al dataset
    ''' </summary>
    ''' <param name="dsCategoria">DatSet con la categoria</param>
    ''' <param name="dtAdjuntos">Tabla de Adjuntos</param>
    ''' <remarks>
    ''' Llamada desde:CargarAdjuntosCategoria
    ''' Tiempo máximo: 0 seg </remarks>
    Public Sub AsociarTablaAdjuntosADataset(ByVal dsCategoria As DataSet, ByVal dtAdjuntos As DataTable)
        If dsCategoria.Tables("ADJUNTOS_CATEGORIA") Is Nothing Then
            dtAdjuntos.TableName = "ADJUNTOS_CATEGORIA" 'Por si no se le ha dado el nombre
            dsCategoria.Tables.Add(dtAdjuntos)
        End If
    End Sub

    ''' <summary>
    ''' Función que carga los archivos adjuntos de la categoria y los devuelve en dataTable
    ''' </summary>
    ''' <param name="CatN1">Categoria de nivel 1</param>
    ''' <param name="CatN2">Categoria de nivel 2</param>
    ''' <param name="CatN3">Categoria de nivel 3</param>
    ''' <param name="CatN4">Categoria de nivel 4</param>
    ''' <param name="CatN5">Categoria de nivel 5</param>
    ''' <param name="Nivel">Nivel al que pertenece la categoria</param>
    ''' <returns>Un array de DataRows con todos los datos de los archivos adjuntos de la categoria</returns>
    ''' <remarks>
    ''' Llamada desde: CargarDetalleCategoria
    ''' Tiempo máximo:0,5 seg</remarks>
    Private Function CargarAdjuntosCategoria(ByVal CatN1 As Object, ByVal CatN2 As Object, ByVal CatN3 As Object, ByVal CatN4 As Object, ByVal CatN5 As Object, ByVal Nivel As Integer) As DataTable
        Dim pag As FSNPage = CType(Me.Page, FSNPage)
        Dim dtAdjuntos As DataTable = CrearTablasAdjuntosCategoria()
        Dim oCategorias As CCategorias = pag.FSNServer.Get_Object(GetType(FSNServer.CCategorias))

        Dim oAdjs As CAdjuntos = oCategorias.CargarAdjuntos(CatN1, CatN2, CatN3, CatN4, CatN5, Nivel)
        Dim oTipo As TiposDeDatos.Adjunto.Tipo
        Select Case Nivel
            Case 1
                oTipo = TiposDeDatos.Adjunto.Tipo.Categoria1
            Case 2
                oTipo = TiposDeDatos.Adjunto.Tipo.Categoria2
            Case 3
                oTipo = TiposDeDatos.Adjunto.Tipo.Categoria3
            Case 4
                oTipo = TiposDeDatos.Adjunto.Tipo.Categoria4
            Case 5
                oTipo = TiposDeDatos.Adjunto.Tipo.Categoria5
        End Select
        For Each oAdj As Adjunto In oAdjs
            Dim fila As DataRow = dtAdjuntos.NewRow()
            fila.Item("ID") = oAdj.Id
            fila.Item("NOMBRE") = oAdj.Nombre
            fila.Item("COMENTARIO") = oAdj.Comentario
            fila.Item("FECHA") = oAdj.Fecha
            fila.Item("DATASIZE") = oAdj.dataSize
            fila.Item("TIPO") = oTipo
            fila.Item("CATN1") = CatN1
            fila.Item("CATN2") = CatN2
            fila.Item("CATN3") = CatN3
            fila.Item("CATN4") = CatN4
            fila.Item("CATN5") = CatN5
            fila.Item("NIVEL") = Nivel
            dtAdjuntos.Rows.Add(fila)
        Next

        Return dtAdjuntos
    End Function

    ''' <summary>
    ''' Procedimiento que carga el detalle de una categoria
    ''' </summary>
    ''' <param name="CatId">Identificador de la categoria. Se compone de su Id, 
    ''' lo anteceden los id de las categorias superiores si los hubiera, separados por un guión bajo _</param>
    ''' <remarks>
    ''' Llamada desde: Todas las páginas que utilicen el control
    ''' Tiempo máximo: 0,6 seg</remarks>
    Public Sub CargarDetalleCategoria(ByVal CatId As String)
        Dim pag As FSNPage = CType(Me.Page, FSNPage)

        If Not IsDBNull(CatId) Then
            '1. Recuperamos los datos de nivel y categorias

            Dim iID As Integer 'Recibe valor dentro del metodo AsignarValoresCategorias
            AsignarValoresCategorias(CatId, iID)

            Dim dsCategoria As DataSet
            Dim dtCategoria As DataTable
            Dim drCategoria As DataRow
            Dim dtAdjuntos As DataTable

            If Cache("DesCategoria_" & pag.Idioma) Is Nothing Then
                Dim oCCategorias As FSNServer.CCategorias = pag.FSNServer.Get_Object(GetType(FSNServer.CCategorias))
                dsCategoria = oCCategorias.CargarDetalleCategoria(m_CatN1, m_CatN2, m_CatN3, m_CatN4, m_CatN5, m_Nivel)
                dtCategoria = dsCategoria.Tables("CATEGORIA")
                drCategoria = dtCategoria.Rows.Find(CatId)
                dtAdjuntos = CargarAdjuntosCategoria(m_CatN1, m_CatN2, m_CatN3, m_CatN4, m_CatN5, m_Nivel)
                AsociarTablaAdjuntosADataset(dsCategoria, dtAdjuntos)

                pag.InsertarEnCache("DesCategoria_" & pag.Idioma, dsCategoria)
            Else
                dsCategoria = CType(Cache("DesCategoria_" & pag.Idioma), DataSet)
                drCategoria = dsCategoria.Tables("CATEGORIA").Rows.Find(CatId)
                If drCategoria Is Nothing Then
                    Dim oCCategorias As FSNServer.CCategorias = pag.FSNServer.Get_Object(GetType(FSNServer.CCategorias))
                    dsCategoria = oCCategorias.CargarDetalleCategoria(m_CatN1, m_CatN2, m_CatN3, m_CatN4, m_CatN5, m_Nivel)
                    dtCategoria = dsCategoria.Tables("CATEGORIA")
                    drCategoria = dtCategoria.Rows.Find(CatId)

                    dtAdjuntos = CargarAdjuntosCategoria(m_CatN1, m_CatN2, m_CatN3, m_CatN4, m_CatN5, m_Nivel)
                    AsociarTablaAdjuntosADataset(dsCategoria, dtAdjuntos)

                    pag.InsertarEnCache("DesCategoria_" & pag.Idioma, dsCategoria)
                Else
                    dsCategoria = CType(Cache("DesCategoria_" & pag.Idioma), DataSet)
                    dtAdjuntos = dsCategoria.Tables("ADJUNTOS_CATEGORIA")
                End If
            End If


            LblObsCategoria.Visible = Not IsDBNull(drCategoria.Item("OBS")) AndAlso Not String.IsNullOrEmpty(drCategoria.Item("OBS"))
            If LblObsCategoria.Visible Then
                Dim sObservaciones As String = drCategoria.Item("OBS")
                For i As Integer = 100 To Len(sObservaciones) Step 100
                    If CType(Mid(sObservaciones, (i - 99), i), String).IndexOf(" ") <= 0 Then
                        sObservaciones = sObservaciones.Insert(i, " ")
                    End If
                Next
                LblObsCategoria.Text = Me.Textos(126) & " " & sObservaciones
            End If

            If Not IsDBNull(drCategoria.Item("DEN")) AndAlso Not String.IsNullOrEmpty(drCategoria.Item("DEN")) Then
                LblDenCategoria.Text = drCategoria.Item("DEN")
            Else
                LblDenCategoria.Text = String.Empty
            End If

            If Not IsDBNull(drCategoria.Item("COD")) AndAlso Not String.IsNullOrEmpty(drCategoria.Item("COD")) Then
                LblCodCategoria.Text = "(" & drCategoria.Item("COD") & ")"
            Else
                LblCodCategoria.Text = String.Empty
            End If

            LblAdjuntos.Text = Me.Textos(138)


            If dtAdjuntos.Rows.Count > 0 Then
                LblAdjuntos.Visible = True
                grdAdjuntos.Visible = True
                pnlAdjuntos.Visible = True
                grdAdjuntos.DataSource = dtAdjuntos
                grdAdjuntos.DataBind()
            Else
                LblAdjuntos.Visible = False
                grdAdjuntos.Visible = False
                pnlAdjuntos.Visible = False
            End If

            UpdatePanelModal.Update()
            MPEImgBtnCat.Show()
        End If
    End Sub

    ''' <summary>
    ''' Evento que se genera cuando es activado un control que este dentro del Grid
    ''' </summary>
    ''' <param name="sender">El propio GridAdjuntos que ha generado el evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automática, cada vez que se clica un control dentro del Grid
    ''' Tiempo máximo: 0,5 seg</remarks>
    Private Sub grdAdjuntos_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdAdjuntos.RowCommand
        If e.CommandName = "Descargar" Then

            'recuperamos datos CommandName
            Dim sArgs() As String = Split(e.CommandArgument, "###")
            Dim AdjunDescargar As FSNServer.Adjunto = CType(Me.Page, FSNPage).FSNServer.Get_Object(GetType(FSNServer.Adjunto))
            AdjunDescargar.Id = sArgs(0)
            AdjunDescargar.Nombre = sArgs(1)
            AdjunDescargar.dataSize = sArgs(2)
            Dim oTipoAdjunto As TiposDeDatos.Adjunto.Tipo
            oTipoAdjunto = CType(sArgs(3), TiposDeDatos.Adjunto.Tipo)
            Dim sNombre As String = AdjunDescargar.Nombre
            AdjunDescargar.EscribirADisco(ConfigurationManager.AppSettings("temp") & "\", sNombre, oTipoAdjunto)
            Response.AddHeader("content-disposition", "attachment; filename=""" & AdjunDescargar.Nombre & """")
            Response.ContentType = "application/octet-stream"
            Response.WriteFile(ConfigurationManager.AppSettings("temp") & "\" & sNombre)
            Response.End()
        End If
    End Sub

    ''' <summary>
    ''' Evento que se genera cada vez que el Grid de Adjuntos rellena sus datos
    ''' </summary>
    ''' <param name="sender">El propio Grid que ha generado el evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automático, cada vez que se rellenan los datos del Grid
    ''' Tiempo máximo: 0 seg</remarks>
    Private Sub grdAdjuntos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdAdjuntos.RowDataBound
        Dim pag As IFSNPage = CType(Me.Page, IFSNPage)
        If e.Row.RowType = DataControlRowType.DataRow Then
            If TypeOf e.Row.DataItem Is DataRow Then
                Dim fila As DataRow = CType(e.Row.DataItem, DataRow)

                'litFechaAdjunto.text
                CType(e.Row.FindControl("litFechaAdjunto"), Literal).Text = FSNLibrary.FormatDate(fila.Item("FECHA"), pag.Usuario.DateFormat)
                'lnkbtnAdjunto.Text
                CType(e.Row.FindControl("lnkbtnAdjunto"), LinkButton).Text = _
                    fila.Item("NOMBRE") & " (" & FSNLibrary.FormatNumber(fila.Item("DATASIZE") / 1024, pag.Usuario.NumberFormat) & " kb.)"
                'lnkbtnAdjunto.CommandArgument
                CType(e.Row.FindControl("lnkbtnAdjunto"), LinkButton).CommandArgument = fila.Item("ID") & "###" & fila.Item("NOMBRE") & "###" & fila.Item("DATASIZE") & "###" & fila.Item("TIPO")

                CType(e.Row.FindControl("imgComentarioAdjunto"), FSNWebControls.FSNImageTooltip).ContextKey = fila.Item("ID") & " " & fila.Item("TIPO") & " " & CType(Me.Page, FSNPage).Idioma

                pag.ScriptMgr.RegisterPostBackControl(e.Row.FindControl("lnkbtnAdjunto"))
                If IsDBNull(fila.Item("COMENTARIO")) OrElse String.IsNullOrEmpty(fila.Item("COMENTARIO")) Then
                    CType(e.Row.FindControl("imgComentarioAdjunto"), FSNWebControls.FSNImageTooltip).Visible = False
                Else
                    CType(Me.FindControl("pnlTooltipComAdjCategoria"), FSNWebControls.FSNPanelTooltip).Text = fila.Item("COMENTARIO")
                    'CType(e.Row.FindControl("pnlTooltipComAdjCategoria"), FSNWebControls.FSNPanelTooltip).Text = fila.Item("COMENTARIO")
                    'CType(e.Row.FindControl("imgComentarioAdjunto"), FSNWebControls.FSNImageTooltip).ContextKey = fila.Item("COMENTARIO")
                End If

            ElseIf TypeOf e.Row.DataItem Is DataRowView Then
                Dim fila As DataRowView = CType(e.Row.DataItem, DataRowView)
                'litFechaAdjunto.text
                CType(e.Row.FindControl("litFechaAdjunto"), Literal).Text = FSNLibrary.FormatDate(fila.Item("FECHA"), pag.Usuario.DateFormat)
                'lnkbtnAdjunto.Text
                CType(e.Row.FindControl("lnkbtnAdjunto"), LinkButton).Text = _
                    fila.Item("NOMBRE") & " (" & FSNLibrary.FormatNumber(fila.Item("DATASIZE") / 1024, pag.Usuario.NumberFormat) & " kb.)"
                'lnkbtnAdjunto.CommandArgument
                CType(e.Row.FindControl("lnkbtnAdjunto"), LinkButton).CommandArgument = fila.Item("ID") & "###" & fila.Item("NOMBRE") & "###" & fila.Item("DATASIZE") & "###" & fila.Item("TIPO")

                CType(e.Row.FindControl("imgComentarioAdjunto"), FSNWebControls.FSNImageTooltip).ContextKey = fila.Item("ID") & " " & fila.Item("TIPO") & " " & CType(Me.Page, FSNPage).Idioma

                pag.ScriptMgr.RegisterPostBackControl(e.Row.FindControl("lnkbtnAdjunto"))
                If IsDBNull(fila.Item("COMENTARIO")) OrElse String.IsNullOrEmpty(fila.Item("COMENTARIO")) Then
                    CType(e.Row.FindControl("imgComentarioAdjunto"), FSNWebControls.FSNImageTooltip).Visible = False
                Else
                    CType(Me.FindControl("pnlTooltipComAdjCategoria"), FSNWebControls.FSNPanelTooltip).Text = fila.Item("COMENTARIO")
                    'CType(e.Row.FindControl("pnlTooltipComAdjCategoria"), FSNWebControls.FSNPanelTooltip).Text = fila.Item("COMENTARIO")
                    'CType(e.Row.FindControl("imgComentarioAdjunto"), FSNWebControls.FSNImageTooltip).ContextKey = fila.Item("COMENTARIO")
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Método que dado un string de Categorias da valor a las vbles a nivel de módulo: m_Nivel (de la categoria más inferior), m_CatN1, m_CatN2, m_CatN3, m_CatN4, m_CatN5
    ''' y cambia el valor de la vble pasada por referencia iID, asignándoile el ID de la Categoría más inferior
    ''' </summary>
    ''' <param name="catId">String de Categorias separadas por guiones bajos: La categoria más inferior y sus padres</param>
    ''' <param name="iID">Vble local a la que asignaremos el ID de la categoría más inferior</param>
    ''' <remarks>
    ''' Llamada desde: CargarDetalleCategoria
    ''' Tiempo máximo: 0 seg</remarks>
    Private Sub AsignarValoresCategorias(ByVal catId As String, ByRef iID As Integer)
        Dim arrCategorias As String() = Split(catId, "_")
        m_Nivel = arrCategorias.Count
        m_CatN1 = arrCategorias(0)
        iID = m_CatN1
        Select Case m_Nivel
            Case 1
                m_CatN2 = System.DBNull.Value
                m_CatN3 = System.DBNull.Value
                m_CatN4 = System.DBNull.Value
                m_CatN5 = System.DBNull.Value
            Case 2
                m_CatN2 = arrCategorias(1)
                m_CatN3 = System.DBNull.Value
                m_CatN4 = System.DBNull.Value
                m_CatN5 = System.DBNull.Value

                iID = m_CatN2
            Case 3
                m_CatN2 = arrCategorias(1)
                m_CatN3 = arrCategorias(2)
                m_CatN4 = System.DBNull.Value
                m_CatN5 = System.DBNull.Value

                iID = m_CatN3
            Case 4
                m_CatN2 = arrCategorias(1)
                m_CatN3 = arrCategorias(2)
                m_CatN4 = arrCategorias(3)
                m_CatN5 = System.DBNull.Value

                iID = m_CatN4
            Case 5
                m_CatN2 = arrCategorias(1)
                m_CatN3 = arrCategorias(2)
                m_CatN4 = arrCategorias(3)
                m_CatN5 = arrCategorias(4)

                iID = m_CatN5
        End Select
    End Sub

End Class