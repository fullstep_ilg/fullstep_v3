﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CancelarEmisionFavorito.ascx.vb" Inherits="Fullstep.FSNWeb.CancelarEmisionFavorito" EnableViewState="false"%>
    <!-- Panel Confirmación -->
    <input id="btnOcultoCancelFav" type="button" value="button" runat="server" style="display: none" enableviewstate="False" />
    <input id="hddAlturaPantalla" type="hidden" runat="server" enableviewstate="false" />
    <asp:Panel ID="pnlCancelFav" runat="server" BackColor="White" BorderColor="DimGray"
            BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Center" Style="display: none;min-width: 400px; padding: 20px" Width="450px">
        <fsn:FSNLinkInfo ID="LnkBtnCerrar" runat="server" style="text-align:right;float:right;">
            <asp:Image ImageUrl="~/images/Bt_Cerrar.png" id="ImgBtnCerrar" runat="server" SkinID="Cerrar" style="border:0px" />
        </fsn:FSNLinkInfo>
        <asp:UpdatePanel ID="upCancelFav" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
            <ContentTemplate>
                <table cellpadding="0" cellspacing="5" border="0" runat="server" id="tblContenido" visible="false">
                    <tr>
                        <td align="left">
                            <img src='~/images/Icono_Error_Amarillo_40x40.gif' runat="server" style="float:left;margin-right:10px;">
                            <asp:Label ID="lblConfirm1" runat="server" Text="" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblConfirm2" runat="server" Text="" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblConfirm3" runat="server" Text="" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"><fsn:FSNButton ID="btnAceptarConfirm" runat="server" Text="FSNButton" style="float:none;"></fsn:FSNButton></td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpeCancelFav" runat="server" BackgroundCssClass="modalBackground"
     PopupControlID="pnlCancelFav" TargetControlID="btnOcultoCancelFav" CancelControlID="LnkBtnCerrar">
        </ajx:ModalPopupExtender>
