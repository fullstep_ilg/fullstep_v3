﻿Imports Fullstep.FSNLibrary
Imports Fullstep.FSNServer

Partial Public Class DetArticuloPedido
    Inherits System.Web.UI.UserControl

    Const _ModuloIdioma As TiposDeDatos.ModulosIdiomas = TiposDeDatos.ModulosIdiomas.EP_CatalogoWeb
    Private _Textos As DataSet

    Private ReadOnly Property Textos(ByVal iTexto As Integer) As String
        Get
            If _Textos Is Nothing Then
                Dim pag As IFSNPage = CType(Me.Page, IFSNPage)
                Dim FSNDict As FSNServer.Dictionary = pag.FSNServer.Get_Object(GetType(FSNServer.Dictionary))
                FSNDict.LoadData(_ModuloIdioma, pag.Idioma)
                _Textos = FSNDict.Data
            End If
            Return _Textos.Tables(0).Rows(iTexto).Item(1)
        End Get
    End Property

    Public Property TipoPedido() As String
        Get
            Return LblOcTipoArticulo.Text
        End Get
        Set(ByVal value As String)
            LblOcTipoArticulo.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Función que devuelve los datos de una línea de pedido para mostrarlo en el detalle
    ''' </summary>
    ''' <param name="Orden">Identificador de la orden de entrega</param>
    ''' <param name="LineaId">Identificador de la línea de pedido</param>
    ''' <param name="EsDeFavorito">Variable booleana que indica si pertenece a un pedido favorito</param>
    ''' <returns>Un DataRow con los datos de la línea de pedido</returns>
    ''' <remarks>
    ''' Llamada desde:CargarDetalleArtículo
    ''' Tiempo máximo: 0,5 seg</remarks>
    Private Function DRowLinPedido(ByVal Orden As Integer, ByVal LineaId As Integer, Optional ByVal EsDeFavorito As Boolean = False) As DataRow
        Dim pag As FSNPage = CType(Me.Page, FSNPage)
        Dim linpedidocache As DataRow
        If EsDeFavorito Then
            If Cache("FavoritosLineasPedido_" & pag.Idioma) Is Nothing Then
                Dim oOrdenFavoritos As COrdenFavoritos = pag.FSNServer.Get_Object(GetType(FSNServer.COrdenFavoritos))
                oOrdenFavoritos.ID = Orden
                Dim dsLineas As DataSet = oOrdenFavoritos.DevolverLineas(pag.Idioma)
                Dim dtLineas As DataTable = dsLineas.Tables(0)
                If Not dtLineas.Columns.Contains("ORDEN") Then
                    dtLineas.Columns.Add("ORDEN", GetType(Integer))
                    For Each dr As DataRow In dtLineas.Rows
                        dr.Item("ORDEN") = Orden
                    Next
                End If
                Dim key() As DataColumn = {dtLineas.Columns("LINEAID")}
                dtLineas.PrimaryKey = key
                pag.InsertarEnCache("FavoritosLineasPedido_" & pag.Idioma, dtLineas)
                linpedidocache = dtLineas.Rows.Find(LineaId)
            Else
                linpedidocache = CType(Cache("FavoritosLineasPedido_" & pag.Idioma), DataTable).Rows.Find(LineaId)
                If linpedidocache Is Nothing Then
                    Dim oOrden As COrdenFavoritos = pag.FSNServer.Get_Object(GetType(FSNServer.COrdenFavoritos))
                    oOrden.ID = Orden
                    Dim dsLineas As DataSet = oOrden.DevolverLineas(pag.Idioma)
                    Dim dtLineas As DataTable = dsLineas.Tables(0)
                    If Not dtLineas.Columns.Contains("ORDEN") Then
                        dtLineas.Columns.Add("ORDEN", GetType(Integer))
                        For Each dr As DataRow In dtLineas.Rows
                            dr.Item("ORDEN") = Orden
                        Next
                    End If
                    Dim key() As DataColumn = {dtLineas.Columns("LINEAID")}
                    dtLineas.PrimaryKey = key
                    Dim dtLineasCache As DataTable = CType(Cache("FavoritosLineasPedido_" & pag.Idioma), DataTable)
                    dtLineasCache.Load(dtLineas.CreateDataReader())
                    linpedidocache = dtLineas.Rows.Find(LineaId)
                End If
            End If

        Else
            If Cache("LineasPedido_" & pag.Idioma) Is Nothing Then
                Dim oOrden As COrden = pag.FSNServer.Get_Object(GetType(FSNServer.COrden))
                oOrden.ID = Orden
                Dim dsLineas As DataSet = oOrden.DevolverLineas(pag.Idioma)
                Dim dtLineas As DataTable = dsLineas.Tables(0)
                If Not dtLineas.Columns.Contains("ORDEN") Then
                    dtLineas.Columns.Add("ORDEN", GetType(Integer))
                    For Each dr As DataRow In dtLineas.Rows
                        dr.Item("ORDEN") = Orden
                    Next
                End If
                Dim key() As DataColumn = {dtLineas.Columns("LINEAID")}
                dtLineas.PrimaryKey = key
                pag.InsertarEnCache("LineasPedido_" & pag.Idioma, dtLineas)
                linpedidocache = dtLineas.Rows.Find(LineaId)
            Else
                linpedidocache = CType(Cache("LineasPedido_" & pag.Idioma), DataTable).Rows.Find(LineaId)
                If linpedidocache Is Nothing Then
                    Dim oOrden As COrden = pag.FSNServer.Get_Object(GetType(FSNServer.COrden))
                    oOrden.ID = Orden
                    Dim dsLineas As DataSet = oOrden.DevolverLineas(pag.Idioma)
                    Dim dtLineas As DataTable = dsLineas.Tables(0)
                    If Not dtLineas.Columns.Contains("ORDEN") Then
                        dtLineas.Columns.Add("ORDEN", GetType(Integer))
                        For Each dr As DataRow In dtLineas.Rows
                            dr.Item("ORDEN") = Orden
                        Next
                    End If
                    Dim key() As DataColumn = {dtLineas.Columns("LINEAID")}
                    dtLineas.PrimaryKey = key
                    Dim dtLineasCache As DataTable = CType(Cache("LineasPedido_" & pag.Idioma), DataTable)
                    dtLineasCache.Load(dtLineas.CreateDataReader())
                    linpedidocache = dtLineas.Rows.Find(LineaId)
                End If
            End If
        End If
        Return linpedidocache
    End Function

    ''' <summary>
    ''' Procedimiento que crea la tabla de adjuntos de la línea de pedido
    ''' </summary>
    ''' <param name="dsOrdenes">DatSet con las ordenes de entrega del que sacar los datos de la línea</param>
    ''' <remarks>
    ''' Llamada desde:CargarADjuntosLínea
    ''' Tiempo máximo: 0 seg </remarks>
    Public Sub CrearTablasAdjuntosLinea(ByVal dsOrdenes As DataSet)
        If dsOrdenes.Tables("ADJUNTOS_LINEA") Is Nothing Then
            Dim dt As DataTable = New DataTable("ADJUNTOS_LINEA")
            dt.Columns.Add("ID", GetType(System.Int32))
            dt.Columns.Add("LINEA", GetType(System.Int32))
            dt.Columns.Add("NOMBRE", GetType(System.String))
            dt.Columns.Add("COMENTARIO", GetType(System.String))
            dt.Columns.Add("FECHA", GetType(System.DateTime))
            dt.Columns.Add("DATASIZE", GetType(System.Double))
            dt.Columns.Add("TIPO", GetType(TiposDeDatos.Adjunto.Tipo))
            Dim keylp() As DataColumn = {dt.Columns("ID")}
            dt.PrimaryKey = keylp
            dsOrdenes.Tables.Add(dt)
            keylp(0) = dsOrdenes.Tables("ADJUNTOS_LINEA").Columns("LINEA")
            dsOrdenes.Relations.Add("REL_LINEAS_ADJUNTOS", dsOrdenes.Tables("LINEASPEDIDO").PrimaryKey, keylp, False)
        End If
    End Sub

    ''' <summary>
    ''' Función que carga los archivos adjuntos de una línea de pedido
    ''' </summary>
    ''' <param name="dsOrdenes">El dataset con las ordenes de entrega</param>
    ''' <param name="Id">Identificador de la línea de pedido</param>
    ''' <param name="EsDeFavorito">Variable booleana que indica si es de pedido favorito</param>
    ''' <returns>Un array de DataRows con todos los datos de los archivos adjuntos de la línea de pedido</returns>
    ''' <remarks>
    ''' Llamada desde: CargarDetalleArtículo
    ''' Tiempo máximo:0,5 seg</remarks>
    Private Function CargarAdjuntosLinea(ByVal dsOrdenes As DataSet, ByVal Id As Integer, Optional ByVal EsDeFavorito As Boolean = False) As DataRow()
        Dim pag As FSNPage = CType(Me.Page, FSNPage)
        CrearTablasAdjuntosLinea(dsOrdenes)
        Dim dr() As DataRow
        dr = dsOrdenes.Tables("LINEASPEDIDO").Rows.Find(Id).GetChildRows("REL_LINEAS_ADJUNTOS")
        If EsDeFavorito Then
            If dr.Length = 0 Then
                Dim oLineaFavorito As CLineaFavoritos = pag.FSNServer.Get_Object(GetType(FSNServer.CLineaFavoritos))
                oLineaFavorito.ID = Id
                oLineaFavorito.Usuario = pag.FSNUser.Cod
                Dim oAdjs As CAdjuntos = oLineaFavorito.CargarAdjuntos(False)
                For Each oAdj As Adjunto In oAdjs
                    Dim fila As DataRow = dsOrdenes.Tables("ADJUNTOS_LINEA").NewRow()
                    fila.Item("ID") = oAdj.Id
                    fila.Item("LINEA") = Id
                    fila.Item("NOMBRE") = oAdj.Nombre
                    fila.Item("COMENTARIO") = oAdj.Comentario
                    fila.Item("FECHA") = oAdj.Fecha
                    fila.Item("DATASIZE") = oAdj.dataSize
                    fila.Item("TIPO") = TiposDeDatos.Adjunto.Tipo.Favorito_Linea_Pedido
                    dsOrdenes.Tables("ADJUNTOS_LINEA").Rows.Add(fila)
                Next
                dr = dsOrdenes.Tables("LINEASPEDIDO").Rows.Find(Id).GetChildRows("REL_LINEAS_ADJUNTOS")
            End If
        Else
            If dr.Length = 0 Then
                Dim oLinea As CLinea = pag.FSNServer.Get_Object(GetType(FSNServer.CLinea))
                oLinea.ID = Id
                Dim oAdjs As CAdjuntos = oLinea.CargarAdjuntos(False)
                For Each oAdj As Adjunto In oAdjs
                    Dim fila As DataRow = dsOrdenes.Tables("ADJUNTOS_LINEA").NewRow()
                    fila.Item("ID") = oAdj.Id
                    fila.Item("LINEA") = Id
                    fila.Item("NOMBRE") = oAdj.Nombre
                    fila.Item("COMENTARIO") = oAdj.Comentario
                    fila.Item("FECHA") = oAdj.Fecha
                    fila.Item("DATASIZE") = oAdj.dataSize
                    fila.Item("TIPO") = TiposDeDatos.Adjunto.Tipo.Linea_Pedido
                    dsOrdenes.Tables("ADJUNTOS_LINEA").Rows.Add(fila)
                Next
                dr = dsOrdenes.Tables("LINEASPEDIDO").Rows.Find(Id).GetChildRows("REL_LINEAS_ADJUNTOS")
            End If
        End If

        Return dr
    End Function



    ''' <summary>
    ''' Procedimiento que carga el detalle de un artículo desde la pantalla de recepciones
    ''' </summary>
    ''' <param name="dtOrdenes">El Datatable con las ordenes de entrega</param>
    ''' <param name="LineaId">Identificador de la línea de pedido</param>
    ''' <param name="EsDeFavorito">Variable booleana que indica si pertenece a un pedido favorito</param>
    ''' <remarks>
    ''' Llamada desde: Todas las páginas que utilicen el control
    ''' Tiempo máximo: 0,6 seg</remarks>
    Public Sub CargarDetalleArticulo(ByVal dtOrdenes As DataTable, ByVal LineaId As Integer, Optional ByVal EsDeFavorito As Boolean = False)
        Dim pag As FSNPage = CType(Me.Page, FSNPage)
        Dim linpedido() As DataRow = dtOrdenes.Select("LINEAPEDID=" & LineaId)
        'Si la linea de pedido no está en los datos que hemos recibido (puede ocurrir en REcepcion cuando el panel para efectuar recepciones muestra líneas que no están en el visor)
        'entonces tenemos que recuperar de bdd los tres datos que necesitamos de la línea para continuar: el ID de su orden, `la denominación del proveedor y el código de la moneda
        If linpedido.Length = 0 Then
            Dim oLinea As CLinea = pag.FSNServer.Get_Object(GetType(CLinea))
            oLinea.ID = LineaId
            ReDim linpedido(0)
            linpedido(0) = oLinea.DevolverProveOrdenMon()
        End If
        Dim linpedidocache As DataRow
        If EsDeFavorito Then
            linpedidocache = DRowLinPedido(linpedido(0)("ORDENID"), LineaId, True)
        Else
            linpedidocache = DRowLinPedido(linpedido(0)("ORDENID"), LineaId)
        End If
        ImgDetArt.ImageUrl = "../EPThumbnail.ashx?Prove=" & linpedidocache.Item("PROVE") & "&Art=" & linpedidocache.Item("CODART") & "&Tipo=Imagen&PonerImagen=True"
        Dim sCadena As String = String.Empty
        If pag.FSNUser.MostrarCodArt And Not IsDBNull(linpedidocache.Item("CODART")) AndAlso Not String.IsNullOrEmpty(linpedidocache.Item("CODART")) Then
            sCadena = linpedidocache.Item("CODART") & " - "
        End If
        sCadena = sCadena & linpedidocache.Item("DENART")
        If Not IsDBNull(linpedidocache.Item("ART_EXT")) AndAlso Not String.IsNullOrEmpty(linpedidocache.Item("ART_EXT")) Then
            sCadena = sCadena & " (" & Me.Textos(136) & ": " & linpedidocache.Item("ART_EXT") & ")"
        End If
        LblDetNomArt.Text = sCadena
        LblDescArtDet.Visible = Not IsDBNull(linpedidocache.Item("OBS")) AndAlso Not String.IsNullOrEmpty(linpedidocache.Item("OBS"))
        If LblDescArtDet.Visible Then
            LblDescArtDet.Text = Me.Textos(126) & ": " & linpedidocache.Item("OBS")
        End If

        'Especificaciones
        If Not IsDBNull(linpedidocache.Item("CODART")) AndAlso Not String.IsNullOrEmpty(linpedidocache.Item("CODART")) Then
            Dim sEspecificaciones As String
            Dim oCArticulos As FSNServer.cArticulos = pag.FSNServer.Get_Object(GetType(FSNServer.cArticulos))
            sEspecificaciones = oCArticulos.DevolverEspecificacionesArt(linpedidocache.Item("CODART"), linpedidocache.Item("PROVE"))
            pnlEspecificaciones.Visible = False
            If sEspecificaciones = "" Then
                LitEspecificaciones.Visible = False
            Else
                lblEspecificaciones.Text = Me.Textos(126)
                LitEspecificaciones.Text = "<p>" & sEspecificaciones & "</p>"
                pnlEspecificaciones.Visible = True
                LitEspecificaciones.Visible = True
            End If
        End If

        If pag.FSNUser.MostrarCodProve And Not IsDBNull(linpedidocache.Item("PROVE")) AndAlso Not String.IsNullOrEmpty(linpedidocache.Item("PROVE")) Then
            sCadena = linpedidocache.Item("PROVE") & " - "
        Else
            sCadena = String.Empty
        End If
        sCadena = sCadena & linpedido(0)("PROVEDEN")
        FSNLinkProveDet.Text = sCadena
        FSNLinkProveDet.ContextKey = linpedidocache.Item("PROVE")
        If (linpedidocache.Item("TIPORECEPCION") = 0) Then
            LblPrecArt0.Text = FSNLibrary.FormatNumber(linpedidocache.Item("PRECIOUNITARIO"), pag.Usuario.NumberFormat)
        Else
            LblPrecArt0.Text = FSNLibrary.FormatNumber(linpedidocache.Item("IMPORTE_PED"), pag.Usuario.NumberFormat)
        End If
        LblMonUniDet.Text = linpedido(0)("MONCOD") & "/" & linpedidocache.Item("UNIDAD")

        If Not IsDBNull(linpedidocache.Item("CODART")) AndAlso Not String.IsNullOrEmpty(linpedidocache.Item("CODART")) Then
            Dim oCArticulos As FSNServer.cArticulos = pag.FSNServer.Get_Object(GetType(FSNServer.cArticulos))
            Dim dsAtributos As DataSet = oCArticulos.DevolverAtributosArt(linpedidocache.Item("CODART"), linpedidocache.Item("PROVE"))
            Dim atribs As DataRowCollection = dsAtributos.Tables(0).Rows
            pnlCaracteristicas.Visible = atribs.Count > 0
            LblCaracteristicas.Text = Me.Textos(135)
            Dim mLista As New List(Of ListItem)
            For Each d As DataRow In atribs
                If IsDBNull(d.Item("VALOR")) Then
                    mLista.Add(New ListItem(d.Item("DEN") & ":", String.Empty))
                Else
                    Select Case d.Item("TIPO")
                        Case 1
                            mLista.Add(New ListItem(d.Item("DEN") & ":", d.Item("VALOR")))
                        Case 2
                            mLista.Add(New ListItem(d.Item("DEN") & ":", FSNLibrary.FormatNumber(d.Item("VALOR"), pag.Usuario.NumberFormat)))
                        Case 3
                            mLista.Add(New ListItem(d.Item("DEN") & ":", FSNLibrary.FormatDate(d.Item("VALOR"), pag.Usuario.DateFormat)))
                        Case 4
                            mLista.Add(New ListItem(d.Item("DEN") & ":", IIf(d.Item("VALOR"), Textos(69), Textos(70))))
                    End Select
                End If
            Next
            lstAtributos.DataSource = mLista
            lstAtributos.DataBind()
        Else
            pnlCaracteristicas.Visible = False
        End If


        litAdjuntos.Visible = False
        pnlAdjuntos.Visible = litAdjuntos.Visible Or grdAdjuntos.Visible

        Dim Altura As Integer = hddAlturaPantalla.Value
        If pnlCaracteristicas.Visible And grdAdjuntos.Visible Then
            pnlCaracteristicas.Style.Item("max-height") = CInt((Altura - 550) / 2) & "px"
            pnlAdjuntos.Style.Item("max-height") = CInt((Altura - 550) / 2) & "px"
        ElseIf pnlCaracteristicas.Visible Then
            pnlCaracteristicas.Style.Item("max-height") = Altura - 550 & "px"
        ElseIf grdAdjuntos.Visible Then
            pnlAdjuntos.Style.Item("max-height") = Altura - 550 & "px"
        End If

        UpdatePanelModal.Update()
        MPEImgBtnArt.Show()
    End Sub



    ''' <summary>
    ''' Procedimiento que carga el detalle de un artículo
    ''' </summary>
    ''' <param name="dsOrdenes">El DataSet con las ordenes de entrega</param>
    ''' <param name="LineaId">Identificador de la línea de pedido</param>
    ''' <param name="EsDeFavorito">Variable booleana que indica si pertenece a un pedido favorito</param>
    ''' <remarks>
    ''' Llamada desde: Todas las páginas que utilicen el control
    ''' Tiempo máximo: 0,6 seg</remarks>
    Public Sub CargarDetalleArticulo(ByVal dsOrdenes As DataSet, ByVal LineaId As Integer, Optional ByVal EsDeFavorito As Boolean = False)
        Dim pag As FSNPage = CType(Me.Page, FSNPage)
        Dim linpedido As DataRow = dsOrdenes.Tables("LINEASPEDIDO").Rows.Find(LineaId)
        Dim orden As DataRow = linpedido.GetParentRow("REL_ORDENES_LINEASPEDIDO")
        Dim linpedidocache As DataRow
        If EsDeFavorito Then
            linpedidocache = DRowLinPedido(orden.Item("ID"), LineaId, True)
        Else
            linpedidocache = DRowLinPedido(orden.Item("ID"), LineaId)
        End If
        ImgDetArt.ImageUrl = "../EPThumbnail.ashx?Prove=" & linpedidocache.Item("PROVE") & "&Art=" & linpedidocache.Item("CODART") & "&Tipo=Imagen&PonerImagen=True"
        Dim sCadena As String = String.Empty
        If pag.FSNUser.MostrarCodArt And Not IsDBNull(linpedidocache.Item("CODART")) AndAlso Not String.IsNullOrEmpty(linpedidocache.Item("CODART")) Then
            sCadena = linpedidocache.Item("CODART") & " - "
        End If
        sCadena = sCadena & linpedidocache.Item("DENART")
        If Not IsDBNull(linpedidocache.Item("ART_EXT")) AndAlso Not String.IsNullOrEmpty(linpedidocache.Item("ART_EXT")) Then
            sCadena = sCadena & " (" & Me.Textos(136) & ": " & linpedidocache.Item("ART_EXT") & ")"
        End If
        LblDetNomArt.Text = sCadena
        LblDescArtDet.Visible = Not IsDBNull(linpedidocache.Item("OBS")) AndAlso Not String.IsNullOrEmpty(linpedidocache.Item("OBS"))
        If LblDescArtDet.Visible Then
            LblDescArtDet.Text = Me.Textos(126) & ": " & linpedidocache.Item("OBS")
        End If
        'Especificaciones
        If Not IsDBNull(linpedidocache.Item("CODART")) AndAlso Not String.IsNullOrEmpty(linpedidocache.Item("CODART")) Then
            Dim sEspecificaciones As String
            Dim oCArticulos As FSNServer.cArticulos = pag.FSNServer.Get_Object(GetType(FSNServer.cArticulos))
            sEspecificaciones = oCArticulos.DevolverEspecificacionesArt(linpedidocache.Item("CODART"), linpedidocache.Item("PROVE"))
            pnlEspecificaciones.Visible = False
            If sEspecificaciones = "" Then
                LitEspecificaciones.Visible = False
            Else
                lblEspecificaciones.Text = Me.Textos(126)
                LitEspecificaciones.Text = "<p>" & sEspecificaciones & "</p>"
                pnlEspecificaciones.Visible = True
                LitEspecificaciones.Visible = True

            End If

        End If



        If pag.FSNUser.MostrarCodProve And Not IsDBNull(linpedidocache.Item("PROVE")) AndAlso Not String.IsNullOrEmpty(linpedidocache.Item("PROVE")) Then
            sCadena = linpedidocache.Item("PROVE") & " - "
        Else
            sCadena = String.Empty
        End If
        sCadena = sCadena & orden.Item("PROVEDEN")
        FSNLinkProveDet.Text = sCadena
        FSNLinkProveDet.ContextKey = linpedidocache.Item("PROVE")
        If (linpedidocache.Item("TIPORECEPCION") = 0) Then
            LblPrecArt0.Text = FSNLibrary.FormatNumber(linpedidocache.Item("PRECIOUNITARIO"), pag.Usuario.NumberFormat)
        Else
            LblPrecArt0.Text = FSNLibrary.FormatNumber(linpedidocache.Item("IMPORTE_PED"), pag.Usuario.NumberFormat)
        End If
        LblMonUniDet.Text = orden.Item("MON") & "/" & linpedidocache.Item("UNIDAD")

        If Not IsDBNull(linpedidocache.Item("CODART")) AndAlso Not String.IsNullOrEmpty(linpedidocache.Item("CODART")) Then
            Dim oCArticulos As FSNServer.cArticulos = pag.FSNServer.Get_Object(GetType(FSNServer.cArticulos))
            Dim dsAtributos As DataSet = oCArticulos.DevolverAtributosArt(linpedidocache.Item("CODART"), linpedidocache.Item("PROVE"))
            Dim atribs As DataRowCollection = dsAtributos.Tables(0).Rows
            pnlCaracteristicas.Visible = atribs.Count > 0
            LblCaracteristicas.Text = Me.Textos(135)
            Dim mLista As New List(Of ListItem)
            For Each d As DataRow In atribs
                If IsDBNull(d.Item("VALOR")) Then
                    mLista.Add(New ListItem(d.Item("DEN") & ":", String.Empty))
                Else
                    Select Case d.Item("TIPO")
                        Case 1
                            mLista.Add(New ListItem(d.Item("DEN") & ":", d.Item("VALOR")))
                        Case 2
                            mLista.Add(New ListItem(d.Item("DEN") & ":", FSNLibrary.FormatNumber(d.Item("VALOR"), pag.Usuario.NumberFormat)))
                        Case 3
                            mLista.Add(New ListItem(d.Item("DEN") & ":", FSNLibrary.FormatDate(d.Item("VALOR"), pag.Usuario.DateFormat)))
                        Case 4
                            mLista.Add(New ListItem(d.Item("DEN") & ":", IIf(d.Item("VALOR"), Textos(69), Textos(70))))
                    End Select
                End If
            Next
            lstAtributos.DataSource = mLista
            lstAtributos.DataBind()
        Else
            pnlCaracteristicas.Visible = False
        End If

        LblAdjuntos.Text = Me.Textos(138)
        If IsDBNull(linpedido.Item("OBSADJUN")) OrElse String.IsNullOrEmpty(linpedido.Item("OBSADJUN")) Then
            litAdjuntos.Visible = False
        Else
            litAdjuntos.Visible = True
            litAdjuntos.Text = "<p>" & linpedido.Item("OBSADJUN") & "</p>"
        End If
        Dim dr As DataRow()
        If EsDeFavorito Then
            dr = CargarAdjuntosLinea(dsOrdenes, LineaId, True)
        Else
            dr = CargarAdjuntosLinea(dsOrdenes, LineaId)
        End If
        If dr.Count > 0 Then
            grdAdjuntos.Visible = True
            grdAdjuntos.DataSource = dr
            grdAdjuntos.DataBind()
        Else
            grdAdjuntos.Visible = False
        End If
        pnlAdjuntos.Visible = litAdjuntos.Visible Or grdAdjuntos.Visible

        Dim Altura As Integer = hddAlturaPantalla.Value
        If pnlCaracteristicas.Visible And grdAdjuntos.Visible Then
            pnlCaracteristicas.Style.Item("max-height") = CInt((Altura - 550) / 2) & "px"
            pnlAdjuntos.Style.Item("max-height") = CInt((Altura - 550) / 2) & "px"
        ElseIf pnlCaracteristicas.Visible Then
            pnlCaracteristicas.Style.Item("max-height") = Altura - 550 & "px"
        ElseIf grdAdjuntos.Visible Then
            pnlAdjuntos.Style.Item("max-height") = Altura - 550 & "px"
        End If

        UpdatePanelModal.Update()
        MPEImgBtnArt.Show()
    End Sub

    ''' <summary>
    ''' Evento que se genera cuando es activado un control que este dentro del Grid
    ''' </summary>
    ''' <param name="sender">El propio GridAdjuntos que ha generado el evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automática, cada vez que se clica un control dentro del Grid
    ''' Tiempo máximo: 0,5 seg</remarks>
    Private Sub grdAdjuntos_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdAdjuntos.RowCommand
        If e.CommandName = "Descargar" Then
            Dim sArgs() As String = Split(e.CommandArgument, "###")
            Dim AdjunDescargar As FSNServer.Adjunto = CType(Me.Page, FSNPage).FSNServer.Get_Object(GetType(FSNServer.Adjunto))
            AdjunDescargar.Id = sArgs(0)
            AdjunDescargar.Nombre = sArgs(1)
            AdjunDescargar.dataSize = sArgs(2)
            Dim tipo As TiposDeDatos.Adjunto.Tipo
            If TipoPedido = "Favorito" Then
                tipo = TiposDeDatos.Adjunto.Tipo.Favorito_Linea_Pedido
            Else
                tipo = TiposDeDatos.Adjunto.Tipo.Linea_Pedido
            End If
            Dim sNombre As String = AdjunDescargar.Nombre
            AdjunDescargar.EscribirADisco(ConfigurationManager.AppSettings("temp") & "\", sNombre, tipo)
            Response.AddHeader("content-disposition", "attachment; filename=""" & AdjunDescargar.Nombre & """")
            Response.ContentType = "application/octet-stream"
            Response.WriteFile(ConfigurationManager.AppSettings("temp") & "\" & sNombre)
            Response.End()
        End If
    End Sub

    ''' <summary>
    ''' Evento que se genera cada vez que el Grid de Adjuntos rellena sus datos
    ''' </summary>
    ''' <param name="sender">El propio Grid que ha generado el evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automático, cada vez que se rellenan los datos del Grid
    ''' Tiempo máximo: 0 seg</remarks>
    Private Sub grdAdjuntos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdAdjuntos.RowDataBound
        Dim pag As IFSNPage = CType(Me.Page, IFSNPage)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim fila As DataRow = CType(e.Row.DataItem, DataRow)
            CType(e.Row.FindControl("litFechaAdjunto"), Literal).Text = FSNLibrary.FormatDate(fila.Item("FECHA"), pag.Usuario.DateFormat)
            CType(e.Row.FindControl("lnkbtnAdjunto"), LinkButton).Text = _
                fila.Item("NOMBRE") & " (" & FSNLibrary.FormatNumber(fila.Item("DATASIZE") / 1024, pag.Usuario.NumberFormat) & " kb.)"
            CType(e.Row.FindControl("lnkbtnAdjunto"), LinkButton).CommandArgument = fila.Item("ID") & "###" & fila.Item("NOMBRE") & "###" & fila.Item("DATASIZE")
            pag.ScriptMgr.RegisterPostBackControl(e.Row.FindControl("lnkbtnAdjunto"))
            If IsDBNull(fila.Item("COMENTARIO")) OrElse String.IsNullOrEmpty(fila.Item("COMENTARIO")) Then
                CType(e.Row.FindControl("imgComentarioAdjunto"), FSNWebControls.FSNImageTooltip).Visible = False
            End If
        End If
    End Sub

    ''' <summary>
    ''' Genera el script que especifica la altura máxima del control
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">EventArgs que contiene datos de eventos.</param>
    ''' <remarks>Se produce cuando el control de usuario se carga en el objeto Page.</remarks>
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.Page.ClientScript.IsStartupScriptRegistered(Me.GetType(), Me.ClientID & "_Init") Then
            Me.Page.ClientScript.RegisterStartupScript(Me.GetType, Me.ClientID & "_Init", _
                    "$get('" & hddAlturaPantalla.ClientID & "').value = window.screen.availHeight;", _
                    True)
        End If
    End Sub

End Class