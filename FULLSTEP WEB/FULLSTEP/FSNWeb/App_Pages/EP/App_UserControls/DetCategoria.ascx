﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DetCategoria.ascx.vb" Inherits="Fullstep.FSNWeb.DetCategoria" %>
    <input id="btnOcultoDet" type="button" value="button" runat="server" style="display: none" />
    <asp:Panel ID="pnlDetCategoria" runat="server" Style="padding: 10px; display:none;" CssClass="DetallArticulo">
                <table width="600">
                    <tr>
                        <td align="right" valign="top">
                            <asp:ImageButton ID="ImgBtnDetCerrar" runat="server" ImageUrl="~/images/Bt_Cerrar.png"
                                ToolTip="Cerrar Detalle" />
                        </td>
                    </tr>
                </table>
        <asp:UpdatePanel ID="UpdatePanelModal" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
            <ContentTemplate>
                <!-- Panel Tooltip Comentario Adjunto -->
                <span style="text-align:left;">
                <fsn:FSNPanelTooltip ID="pnlTooltipComAdjCategoria" runat="server" Contenedor="pnlDetCategoria" text="">
                </fsn:FSNPanelTooltip>
                </span>
                <table width="600">
                    <tr>
                        <td align="left" style="height: 20px; font-weight:bold; font-size:1.5em;" valign="top" valign="top">
                            <asp:Label ID="LblCodCategoria" runat="server"></asp:Label>
                            &nbsp;
                            &nbsp;
                            <asp:Label ID="LblDenCategoria" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="500px">
                    <tr>
                        <td rowspan="2" width="20%">
                            &nbsp;
                        </td>
                        <td align="left" valign="top">
                            <asp:Label ID="LblObsCategoria" runat="server" Text="Label" CssClass="Normal">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" width="100%">
                            <table style="width:100%;">
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                       <asp:Panel ID="pnlAdjuntos" runat="server" CssClass="Rectangulo" Style="padding: 4px;">
                                            <asp:Label ID="LblAdjuntos" runat="server" CssClass="Rotulo"></asp:Label>
                                            
                                            <asp:GridView ID="grdAdjuntos" runat="server" ShowHeader="False" AutoGenerateColumns="False"
                                                CellPadding="4" GridLines="None">
                                                <Columns>
                                                    <asp:TemplateField ShowHeader="False" ItemStyle-Wrap="False">
                                                        <ItemTemplate>
                                                            <asp:Literal ID="litFechaAdjunto" runat="server"></asp:Literal>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ShowHeader="False" ItemStyle-Width="300px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkbtnAdjunto" runat="server" CommandName="Descargar" CssClass="Normal"
                                                                Style="text-decoration: none;"></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="300px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ShowHeader="False">
                                                        <ItemTemplate>
                                                            <fsn:FSNImageTooltip ID="imgComentarioAdjunto" runat="server" PanelTooltip="pnlTooltipComAdjCategoria"
                                                                SkinID="ObservacionesPeq" ContextKey='' />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="FilaPar2" />
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="MPEImgBtnCat" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="ImgBtnDetCerrar" Enabled="true"
    PopupControlID="pnlDetCategoria" RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="btnOcultoDet">
    </ajx:ModalPopupExtender>
