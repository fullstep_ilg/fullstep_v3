﻿Imports Fullstep.FSNServer

Partial Public Class CancelarEmisionFavorito
    Inherits System.Web.UI.UserControl

    Const _ModuloIdioma As TiposDeDatos.ModulosIdiomas = TiposDeDatos.ModulosIdiomas.EP_Favoritos
    Private _Textos As DataSet

    Private ReadOnly Property Textos(ByVal iTexto As Integer) As String
        Get
            If _Textos Is Nothing Then
                Dim pag As IFSNPage = CType(Me.Page, IFSNPage)
                Dim FSNDict As FSNServer.Dictionary = pag.FSNServer.Get_Object(GetType(FSNServer.Dictionary))
                FSNDict.LoadData(_ModuloIdioma, pag.Idioma)
                _Textos = FSNDict.Data
            End If
            Return _Textos.Tables(0).Rows(iTexto).Item(1)
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.Page.ClientScript.IsStartupScriptRegistered(Me.GetType(), Me.ClientID & "_Init") Then
            Me.Page.ClientScript.RegisterStartupScript(Me.GetType, Me.ClientID & "_Init", _
                    "$get('" & hddAlturaPantalla.ClientID & "').value = window.screen.availHeight;", _
                    True)
        End If
        btnAceptarConfirm.OnClientClick = "$find('" & mpeCancelFav.ClientID & "').hide();"
    End Sub

    ''' <summary>
    ''' Método que carga en el control de usuario el contenido a mostrar
    ''' </summary>
    ''' <param name="oItemsNotIssuable">Lista de artículos que no pueden ser emitidos en el favorito</param>
    ''' <param name="message">Mensaje a mostrar al usuario. Valores:
    ''' 1-> Mensaje "No es posible emitir el pedido. Los siguientes artículos ya nos están disponibles en el catálogo o han sido trasladados a una categoría de la cual no es aprovisionador. Es necesario borrarlos del pedido si desea continuar con su emisión."
    ''' 2-> Mensaje "No es posible emitir el pedido. Ninguno de los artículos del favorito está disponible en el catálogo o bien han sido trasladados a una categoría de la cual no es aprovisionador."
    ''' 3-> Mensaje "No es posible modificar el pedido. Los siguientes artículos ya nos están disponibles en el catálogo o han sido trasladados a una categoría de la cual no es aprovisionador. Es necesario borrarlos del pedido si desea continuar con su emisión."
    ''' 4-> Mensaje "No es posible modificar el pedido. Ninguno de los artículos del favorito está disponible en el catálogo o bien han sido trasladados a una categoría de la cual no es aprovisionador."
    ''' </param>
    ''' <remarks>Llamada desde allí donde se instancie el control</remarks>
    Friend Sub CargarPanel(ByVal oItemsNotIssuable As DataTable, ByVal message As Integer)
        Select Case message
            Case 1, 3
                If message = 1 Then
                    lblConfirm1.Text = Textos(42)
                Else
                    lblConfirm1.Text = Textos(46)
                End If
                lblConfirm2.Text = Textos(43) & ":"
                lblConfirm2.Style("font-weight") = "bold"
                Dim sArticulos As String = "<blockquote>"

                For Each oRow As DataRow In oItemsNotIssuable.Rows
                    Dim sDen As String = oRow.Item("DEN1")
                    If oRow.Item("DEN2") <> String.Empty Then sDen += oRow.Item("DEN2")
                    sArticulos += oRow.Item("COD") & TextosSeparadores.espacioGuionEspacio & sDen & "<br>"
                Next
                sArticulos += "</blockquote>"
                lblConfirm3.Text = sArticulos
                lblConfirm3.Style("font-weight") = "bold"
            Case 2, 4
                If message = 2 Then
                    lblConfirm1.Text = Textos(45)
                Else
                    lblConfirm1.Text = Textos(47)
                End If
                lblConfirm2.Text = String.Empty
                lblConfirm3.Text = String.Empty
        End Select
        btnAceptarConfirm.Text = Textos(44)
        pnlCancelFav.Style("display") = "block"
        tblContenido.Visible = True
        upCancelFav.Update()
        mpeCancelFav.Show()
    End Sub

End Class