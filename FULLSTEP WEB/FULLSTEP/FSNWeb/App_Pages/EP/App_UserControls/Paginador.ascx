﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Paginador.ascx.vb" Inherits="Fullstep.FSNWeb.Paginador" %>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr><td colspan="2"><hr size="1" /></td></tr>
    <tr>
        <td align="left">
            <table cellpadding="4" border="0">
                <tr>
                    <td>
                        <asp:Label ID="lblViendo" runat="server" Text="DViendo Pedidos"></asp:Label>&nbsp;
                        <asp:Label ID="lblResul" runat="server" Text="0-0"></asp:Label>&nbsp;
                        <asp:Label ID="lblResulDe" runat="server" Text="Dde"></asp:Label>&nbsp;
                        <asp:Label ID="lblResulTot" runat="server" Text="0"></asp:Label>
                    </td>
                    <td>&nbsp;</td><td>&nbsp;</td>
                    <td><asp:ImageButton runat="server" ID="btnFirstPage" /></td>
                    <td><asp:ImageButton runat="server" ID="btnPreviousPage" /></td>
                    <td><asp:Label ID="lblPagina" runat="server" Text="DPágina"></asp:Label></td>
                    <td><asp:DropDownList ID="ddlPage" runat="server" CausesValidation="False" AutoPostBack="True"></asp:DropDownList></td>
                    <td>
                        <asp:Label ID="lblDe" runat="server" Text="Dde"></asp:Label>&nbsp;
                        <asp:Label ID="lblPagTot" runat="server" Text="1"></asp:Label>
                    </td>
                    <td><asp:ImageButton runat="server" ID="btnNextPage" /></td>
                    <td><asp:ImageButton runat="server" ID="btnLastPage" /></td>
                </tr>
            </table>
        </td>
        <td align="right">
            <table cellpadding="4" border="0">
                <tr>
                    <td><asp:Label ID="lblOrdenacion" runat="server" Text="DOrdenar por:"></asp:Label></td>
                    <td><asp:DropDownList ID="ddlOrdenacion" runat="server" AutoPostBack="True"></asp:DropDownList></td>
                    <td><asp:ImageButton ID="imgBtnOrdenacion" runat="server" /></td>
                    <td><asp:ImageButton ID="imgbtnExportar" runat="server" /></td>
                    <td><asp:ImageButton ID="imgbtnImprimir" runat="server" /></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td colspan="2"><hr size="1" /></td></tr>
</table>
