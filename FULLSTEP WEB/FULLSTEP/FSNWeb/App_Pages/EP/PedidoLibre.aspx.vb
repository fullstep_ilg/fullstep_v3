﻿Imports Fullstep.FSNLibrary.modUtilidades
Imports Fullstep.FSNServer

Partial Public Class PedidoLibre
    Inherits FSEPPage

    Dim Nivel, Cat1, Cat2, Cat3, Cat4, Cat5 As Short
    Dim dtLineas As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Master.CabTabPedidoLibre.Selected = True

        If Not lstUnidades Is Nothing Then
            ScriptMgr.RegisterAsyncPostBackControl(lstUnidades)
        End If

        If Not Page.IsPostBack Then
            Dim dsProveedoresPedLibre As New DataSet
            Dim bMostrarCodProv As Boolean

            CargarTextos()

            bMostrarCodProv = FSNUser.MostrarCodProve

            Cat1 = Val(Request("Cat1"))
            Cat2 = Val(Request("Cat2"))
            Cat3 = Val(Request("Cat3"))
            Cat4 = Val(Request("Cat4"))
            Cat5 = Val(Request("Cat5"))

            If Cat5 <> 0 Then
                Nivel = 5
            ElseIf Cat4 <> 0 Then
                Nivel = 4
            ElseIf Cat3 <> 0 Then
                Nivel = 3
            ElseIf Cat2 <> 0 Then
                Nivel = 2
            ElseIf Cat1 <> 0 Then
                Nivel = 1
            Else
                Nivel = 0
            End If

            'Rellenamos los datalist de categorías  y proveedores
            Dim oProveedores As CProveedores
            oProveedores = FSNServer.Get_Object(GetType(FSNServer.CProveedores))
            dlCategorias.DataSource = oProveedores.DevolverProveedoresPedLibre(Usuario.CodPersona, Nivel, Cat1, Cat2, Cat3, Cat4, Cat5)
            dlCategorias.DataBind()

            'Rellenamos la combo de unidades
            Dim oUnidades As CUnidadesPedido
            oUnidades = FSNServer.Get_Object(GetType(FSNServer.CUnidadesPedido))
            lstUnidades.DataSource = oUnidades.CargarTodasLasUnidades(Usuario.Idioma)
            lstUnidades.DataBind()
            If Not lstUnidades.Items Is Nothing AndAlso lstUnidades.Items.Count > 0 Then
                lstUnidades.Items(0).Selected = True
            End If

            'Ponemos el formato del usuario a los controles Cantidad y Precio
            If IsNumeric(lstUnidades.SelectedIndex) Then
                configurarDecimales(lstUnidades.SelectedValue, WNumEdCantidad)
                'Insertamos un script que controla el evento paste en el control
                Me.insertarScriptPaste(WNumEdCantidad.ClientID)
            End If

            WNumEdCantidad.MinValue = 0
            WNumEdCantidad.ValueDouble = 0

            WNumEdPrecio.MinValue = 0
            WNumEdPrecio.ValueDouble = 0

            'Creamos el datatable para las lineas de Pedido Libre
            dtLineas = New DataTable
            Dim oColumn As DataColumn

            oColumn = New DataColumn("Descr", GetType(System.String)) 'DESCRIPCION
            dtLineas.Columns.Add(oColumn)
            oColumn = New DataColumn("Cant", GetType(System.Double)) 'CANTIDAD
            dtLineas.Columns.Add(oColumn)
            oColumn = New DataColumn("Unidad", GetType(System.String)) 'UNIDAD
            dtLineas.Columns.Add(oColumn)
            oColumn = New DataColumn("PrecioUni", GetType(System.Double)) 'PRECIO UNITARIO (no estará visible)
            dtLineas.Columns.Add(oColumn)
            oColumn = New DataColumn("Precio", GetType(System.Double)) 'PRECIO
            dtLineas.Columns.Add(oColumn)
            oColumn = New DataColumn("Mon", GetType(System.String)) 'MONEDA
            dtLineas.Columns.Add(oColumn)

            ViewState.Add("Tabla", dtLineas)

        Else
            dtLineas = ViewState("Tabla")
        End If

    End Sub

    ''' <summary>
    ''' Procedimiento que recupera los textos de la BBDD y los carga en los controles
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: El evento load de la página
    ''' Tiempo: 0 seg.</remarks>
    Private Sub CargarTextos()

        ModuloIdioma = FSNLibrary.TiposDeDatos.ModulosIdiomas.EP_PedidoLibre
        lblProveedor.Text = Textos(4)

        ModuloIdioma = FSNLibrary.TiposDeDatos.ModulosIdiomas.EP_DatosContacto
        'lblTextDep.Text = Textos(2)
        'lblTextCargo.Text = Textos(3)
        'lblTextTfno1.Text = Textos(4)
        'lblTextTfno2.Text = Textos(5)
        'lblTextMov.Text = Textos(6)
        'lblTextFax.Text = Textos(7)
        'lblTextEmail.Text = Textos(8)

        ModuloIdioma = FSNLibrary.TiposDeDatos.ModulosIdiomas.EP_PedidoLibreRellenar
        lblContacto.Text = Replace(Textos(2), ")", ":")
        lblDescripcion.Text = Textos(3)
        txtDescripcion_TextBoxWatermarkExtender.WatermarkText = Textos(27)
        lblCantidad.Text = Textos(4)
        lblUnidad.Text = Textos(5)
        lblPrecio.Text = Textos(6)

        btnAnyadir.Text = Textos(7)
        lblLineasPedido.Text = Textos(12)
        ViewState("txtEliminar") = Textos(19)
        btnAnyadirACesta.Text = Textos(20)


        reqValDescr.ErrorMessage = Textos(21)

        ViewState("textArticulos") = Textos(26)

    End Sub

    ''' <summary>
    ''' Procedimiento que vacía los controles de líneas de pedido
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: btnAnyadirACesta y btnAnyadir
    ''' Tiempo máximo: 0 sec.
    ''' </remarks>
    Private Sub VaciarControlesPopup()
        txtDescripcion.Text = ""
        WNumEdCantidad.ValueDouble = 0
        lstUnidades.ClearSelection()
        WNumEdPrecio.Text = 0
    End Sub


    Protected Sub dlProveedores_OnItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs)
        Select Case e.CommandName
            Case "MostrarPanelLineas"
                Dim arrCodyDens() As String

                arrCodyDens = Split(e.CommandArgument, "###")

                ViewState("CodCat") = arrCodyDens(0)
                ViewState("CodProve") = arrCodyDens(2)
                ViewState("DenProve") = arrCodyDens(3)
                lblMon.Text = arrCodyDens(4)
                ViewState("Cat1") = CShort(Val(arrCodyDens(5)))
                ViewState("Cat2") = CShort(Val(arrCodyDens(6)))
                ViewState("Cat3") = CShort(Val(arrCodyDens(7)))
                ViewState("Cat4") = CShort(Val(arrCodyDens(8)))
                ViewState("Cat5") = CShort(Val(arrCodyDens(9)))

                lblCategoria.Text = arrCodyDens(1)
                lblProve.Text = arrCodyDens(2) & " - " & arrCodyDens(3)

                RecuperarNombreContacto()

                fsnlnkContacto.ContextKey = ViewState("CodProve")

                grdLineas.DataSource = dtLineas
                grdLineas.DataBind()

                updpnlPopupLineas.Update()
                mpeLin.Show()
        End Select
    End Sub

    ''' <summary>
    ''' Procedimiento que trae los datos del contacto del proveedor seleccionado y los vuelca en el panel panContacto
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: btnAnyadirACesta y btnAnyadir
    ''' Tiempo máximo: 0 sec.
    ''' </remarks>
    Private Sub RecuperarNombreContacto()

        Dim oContactos As CContactos = FSNServer.Get_Object(GetType(FSNServer.CContactos))
        Dim dsContacto As DataSet

        'Recuperamos los datos del contacto del proveedor
        dsContacto = oContactos.CargarDatosContacto(ViewState("CodProve"))

        ModuloIdioma = FSNLibrary.TiposDeDatos.ModulosIdiomas.EP_PedidoLibreRellenar
        lblContacto.Text = Replace(Textos(2), ")", ":")

        If dsContacto.Tables(0).Rows.Count = 1 Then
            fsnlnkContacto.Text = DBNullToStr(dsContacto.Tables(0).Rows(0)("CONTACTO")) & ")"
            lblContacto.Text = Replace(Textos(2), ")", ":")
            lblContacto.Visible = True

            fsnlnkContacto.Visible = True
        Else
            fsnlnkContacto.Text = ""
            lblContacto.Visible = False
            fsnlnkContacto.Visible = False
        End If

        dsContacto.Dispose()
        oContactos = Nothing
    End Sub

    ''' <summary>
    ''' Evento que se lanza al clickar sobre el botón "Añadir" y que, si pasa la validación, añadirá los valores que tenemos en las cajas de textos a una línea nueva de la grid de líneas de pedido
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' Tiempo máximo: 1 sec.
    ''' </remarks>
    Protected Sub btnAnyadir_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAnyadir.Click

        If Page.IsValid Then

            Dim oRow As DataRow

            oRow = dtLineas.NewRow

            oRow.Item("Descr") = txtDescripcion.Text
            oRow.Item("Cant") = WNumEdCantidad.ValueDouble
            oRow.Item("Unidad") = lstUnidades.Text
            oRow.Item("PrecioUni") = WNumEdPrecio.ValueDouble
            oRow.Item("Precio") = WNumEdPrecio.ValueDouble * WNumEdCantidad.ValueDouble
            oRow.Item("Mon") = lblMon.Text
            dtLineas.Rows.Add(oRow)

            ViewState.Add("Tabla", dtLineas)

            grdLineas.DataSource = dtLineas
            grdLineas.DataBind()

            VaciarControlesPopup()

        End If

        btnAnyadir.Focus()

    End Sub


    ''' <summary>
    ''' Evento que se lanza al clickar sobre el botón "Añadir a Cesta" y que añadirá el conjunto de todas las líneas de pedido que tenemos en la grid grdLineas a la cesta
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' Tiempo máximo: 1 sec.
    ''' </remarks>
    Protected Sub btnAnyadirACesta_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAnyadirACesta.Click

        If dtLineas.Rows.Count > 0 Then

            Dim oArticulo As CArticulo
            Dim oLinea As DataRow
            Dim iCant As Integer = 0

            For Each oLinea In dtLineas.Rows
                If oLinea("Descr") <> "" Then

                    oArticulo = Me.FSNServer.Get_Object(GetType(FSNServer.CArticulo))
                    With oArticulo
                        .Den = oLinea("Descr")
                        .Prove = ViewState("CodProve")
                        .proveden = ViewState("DenProve")
                        .PrecioUC = oLinea("PrecioUni")
                        .Cat1 = DBNullToSomething(ViewState("Cat1"))
                        .Cat2 = DBNullToSomething(ViewState("Cat2"))
                        .Cat3 = DBNullToSomething(ViewState("Cat3"))
                        .Cat4 = DBNullToSomething(ViewState("Cat4"))
                        .Cat5 = DBNullToSomething(ViewState("Cat5"))
                        .Categoria = ViewState("CodCat")
                        .RamaCategoria = lblCategoria.Text
                        .Cantidad = oLinea("Cant")
                        .Tipo = 1
                        .Mon = lblMon.Text
                        .UP = oLinea("Unidad")
                    End With

                    oArticulo.AnyadirACesta(FSNUser.CodPersona)
                End If
            Next

            VaciarControlesPopup()
            dtLineas.Clear()
            ViewState.Add("Tabla", dtLineas)

            Master.CabControlCesta.Actualizar()

            mpeLin.Hide()
            panProveedores.Enabled = True

        End If

    End Sub

    ''' <summary>
    ''' Evento que se lanza al clickar sobre el aspa "X" y que ocultará el panel "panProveedores".
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' Tiempo máximo: 1 sec.
    ''' </remarks>
    Protected Sub cmdCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnCerrarpnlLin.Click

        VaciarControlesPopup()
        dtLineas.Clear()
        ViewState.Add("Tabla", dtLineas)

        'panLineas.Visible = False
        panProveedores.Enabled = True
    End Sub

    ''' <summary>
    ''' Evento que se lanza cuando pinchamos sobre el botón eliminar de la grid, y en el que eliminaremos de la tabla el registro seleccionado, refrescando luego la grid.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' Tiempo máximo: 0 sec.
    ''' </remarks>
    Private Sub grdLineas_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdLineas.RowCommand
        Dim lafila As GridViewRow
        If e.CommandName = "EliminarFila" Then
            lafila = CType(e.CommandSource, LinkButton).NamingContainer

            dtLineas.Rows(lafila.RowIndex).Delete()

            ViewState.Add("Tabla", dtLineas)

            grdLineas.DataSource = dtLineas
            grdLineas.DataBind()

            btnAnyadir.Focus()
        End If
    End Sub

    Private Sub grdLineas_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdLineas.RowDataBound
        With e.Row
            If .RowType = DataControlRowType.DataRow Then
                Dim fila As DataRowView = CType(.DataItem, DataRowView)

                Dim oUnidadPedido As FSNServer.CUnidadPedido = Nothing
                If Not DBNullToSomething(fila.Item("Unidad")) = Nothing Then
                    oUnidadPedido = TodasLasUnidades.Item(Trim(fila.Item("Unidad")))
                    oUnidadPedido.AsignarFormatoaUnidadPedido(Me.Usuario.NumberFormat)
                End If
                Dim sCantidad As String = fila.Item("Cant")
                If Not oUnidadPedido Is Nothing Then
                    If oUnidadPedido.NumeroDeDecimales Is Nothing Then
                        Dim decimales As Integer = NumeroDeDecimales(fila.Item("Cant"))
                        oUnidadPedido.UnitFormat.NumberDecimalDigits = decimales
                    End If
                    sCantidad = FSNLibrary.modUtilidades.FormatNumber(CType(fila.Item("Cant"), Double), oUnidadPedido.UnitFormat)
                End If

                .Cells(1).Text = sCantidad
                .Cells(4).Text = FSNLibrary.FormatNumber(fila.Item("Precio"), Me.Usuario.NumberFormat)

                CType(.FindControl("btnEliminar"), FSNWebControls.FSNButton).Text = ViewState("txtEliminar")
            End If
        End With
    End Sub

#Region "Unidades"
    ''' <summary>
    ''' Propiedad que recupera el conjunto de las unidades de Pedido para su posterior uso en la página
    ''' </summary>
    Private ReadOnly Property TodasLasUnidades() As CUnidadesPedido
        Get
            Dim oUnidadesPedido As CUnidadesPedido = Me.FSNServer.Get_Object(GetType(FSNServer.CUnidadesPedido))
            If HttpContext.Current.Cache("TodasLasUnidades" & Me.Usuario.Idioma.ToString()) Is Nothing Then
                oUnidadesPedido.CargarTodasLasUnidades(Me.Usuario.Idioma)
                Me.InsertarEnCache("TodasLasUnidades" & Me.Usuario.Idioma.ToString(), oUnidadesPedido)
            Else
                oUnidadesPedido = HttpContext.Current.Cache("TodasLasUnidades" & Me.Usuario.Idioma.ToString())
            End If
            Return oUnidadesPedido
        End Get
    End Property

    ''' <summary>
    ''' Método que modifica el número de decimales de entrada en el control de cantidad, 
    ''' en función del número de decimales que puede llevar la unidad seleccionada en el dropdownlist
    ''' </summary>
    ''' <param name="sender">dropdownlist de unidades</param>
    ''' <param name="e">argumentos del combo</param>
    ''' <remarks>Se llama desde el combo de unidades. Tiempo máximo inferior a 0,3 seg</remarks>
    Private Sub lstUnidades_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstUnidades.SelectedIndexChanged
        Dim unidad As String = CType(sender, System.Web.UI.WebControls.DropDownList).SelectedValue

        configurarDecimales(unidad, WNumEdCantidad)
        'Insertamos un script que controla el evento paste en el control
        Me.insertarScriptPaste(WNumEdCantidad.ClientID)

        updpnlPopupLineas.Update()
    End Sub

    ''' <summary>
    ''' Método que recupera el número de decimales para una unidad de pedido dada y configura la presentación de los datos en los controles webnumericEdit
    ''' </summary>
    ''' <param name="unidad">Código de la unidad de pedido</param>
    ''' <param name="wNumericEdit">Control webNumericEdit a configurar</param>
    ''' <remarks>Llamada desde: la página, allí donde se configure un control webNumericEdit. Tiempo máximo: inferior a 0,3</remarks>
    Private Sub configurarDecimales(ByVal unidad As Object, ByRef wNumericEdit As Infragistics.Web.UI.EditorControls.WebNumericEditor)
        Dim oUnidadPedido As FSNServer.CUnidadPedido = Nothing
        'Hacemos una copia de la referencia cultural de la página, ya que algunos de los formatos es posible que los modifiquemos
        Dim refCultural As Globalization.CultureInfo = Threading.Thread.CurrentThread.CurrentCulture.Clone()

        If Not DBNullToSomething(unidad) = Nothing Then
            unidad = Trim(unidad.ToString)
            oUnidadPedido = TodasLasUnidades.Item(unidad)
            oUnidadPedido.AsignarFormatoaUnidadPedido(Me.Usuario.NumberFormat)
        End If
        If Not oUnidadPedido Is Nothing Then
            refCultural.NumberFormat = oUnidadPedido.UnitFormat
            If oUnidadPedido.NumeroDeDecimales Is Nothing Then
                refCultural.NumberFormat.NumberDecimalDigits = 15
                wNumericEdit.MinDecimalPlaces = 0
                wNumericEdit.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Double
            ElseIf oUnidadPedido.NumeroDeDecimales > 0 Then
                wNumericEdit.MinDecimalPlaces = oUnidadPedido.NumeroDeDecimales
                wNumericEdit.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Double
            ElseIf oUnidadPedido.NumeroDeDecimales = 0 Then
                wNumericEdit.MinDecimalPlaces = oUnidadPedido.NumeroDeDecimales
                wNumericEdit.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Int
            End If
            wNumericEdit.ClientEvents.TextChanged = "limiteDecimalesTextChanged"
            wNumericEdit.Attributes.Add("numeroDecimales", refCultural.NumberFormat.NumberDecimalDigits)
            wNumericEdit.Attributes.Add("separadorDecimales", refCultural.NumberFormat.NumberDecimalSeparator)
            ModuloIdioma = FSNLibrary.TiposDeDatos.ModulosIdiomas.EP_PedidoLibreRellenar
            wNumericEdit.Attributes.Add("avisoDecimales", Textos(28) & " " & oUnidadPedido.Codigo & ":")
        End If
        wNumericEdit.Culture = refCultural
    End Sub
#End Region

    ''' <summary>
    ''' Método que se lanza justo antes de generar el control en pantalla
    ''' </summary>
    ''' <param name="sender">control que lo lanza</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Evento que se lanza en el momento anterior a dibujar el control en pantalla. Tiempo máximo inferior a 0,1 seg</remarks>
    Private Sub updpnlPopupLineas_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles updpnlPopupLineas.PreRender
        If ScriptMgr.IsInAsyncPostBack Then
            configurarDecimales(lstUnidades.SelectedValue, WNumEdCantidad)
        End If
        'Insertamos un script que controla el evento paste en el control
        Me.insertarScriptPaste(WNumEdCantidad.ClientID)
    End Sub
End Class
