﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="rptViewerSinMenu.aspx.vb" Inherits="Fullstep.FSNWeb.rptViewerSinMenu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server" style="background-color: #FFFFFF">
    <div>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" HasCrystalLogo="False" ToolPanelView=None HasToggleGroupTreeButton="False" />
    </div>
    </form>
</body>
</html>
