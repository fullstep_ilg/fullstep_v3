﻿Imports System.Web.Script.Serialization

Public Class PedidoAbierto
    Inherits FSEPPage
    Private _dsResultado As DataSet

    Private _anio As Integer
    Private _numPedido As Integer
    Private _numOrden As Integer
    Private _idEmpresa As Integer
    Private _codArticulo As String
    Private _denArticulo As String
    Private _fecInicioDesde As Date
    Private _fecInicioHasta As Date
    Private _fecFinDesde As Date
    Private _fecFinHasta As Date
    Private _codProve As String

    Protected ReadOnly Property Pedidos_Abiertos() As DataSet
        Get
            _dsResultado = CType(Cache("dsPedidosAbiertos_" & FSNUser.Cod), DataSet)
            If _dsResultado Is Nothing Then
                Dim oPedidosAbiertos As FSNServer.CPedidoAbierto
                oPedidosAbiertos = FSNServer.Get_Object(GetType(FSNServer.CPedidoAbierto))
				_dsResultado = oPedidosAbiertos.Cargar_PedidosAbiertos(FSNUser.Cod, FSNUser.EPRestringirEmisionPedidosAbiertosEmpresaUsu, FSNUser.EPRestringirEmisionPedidosAbiertosEmpresasUONsPerfil,
												_anio, _numPedido, _numOrden, _idEmpresa, _codArticulo, _denArticulo, _fecInicioDesde, _fecInicioHasta, _fecFinDesde, _fecFinHasta, _codProve)

				Me.InsertarEnCache("dsPedidosAbiertos_" & FSNUser.Cod, _dsResultado)
                Return _dsResultado
            Else
                Return _dsResultado
            End If
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not FSNUser.EPPermitirEmitirPedidosDesdePedidoAbierto Then
            Response.Redirect(System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "Inicio.aspx", True)
        End If
        Master.CabTabPedidoAbierto.Selected = True
        ModuloIdioma = FSNLibrary.TiposDeDatos.ModulosIdiomas.EP_PedidoAbierto

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "NumberFormatUsu") Then
            With FSNUser
                Dim sVariablesJavascript As String = "var UsuNumberDecimalSeparator = '" & .NumberFormat.NumberDecimalSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberGroupSeparator='" & .NumberFormat.NumberGroupSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberNumDecimals='" & .NumberFormat.NumberDecimalDigits & "';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NumberFormatUsu", sVariablesJavascript, True)
            End With
        End If

        'Pasamos a javascript los datos de los filtros en la variable datosFiltrosAvanzados y el anio que seleccionaremos.
        Dim cPedidoAbierto As FSNServer.CPedidoAbierto
        cPedidoAbierto = FSNServer.Get_Object(GetType(FSNServer.CPedidoAbierto))
        Dim serializer As New JavaScriptSerializer
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "datosFiltrosAvanzados") Then
            Dim oDatosFiltrosAvanzados As Object = cPedidoAbierto.Cargar_FiltrosAvanzados_PedidoAbierto(FSNUser.Cod, FSNUser.EPRestringirEmisionPedidosAbiertosEmpresaUsu, FSNUser.EPRestringirEmisionPedidosAbiertosEmpresasUONsPerfil)
            Dim strJSON As String
            If oDatosFiltrosAvanzados Is Nothing Then
                strJSON = "null;_anioSeleccionadoInicio=0"
                _anio = 0
            Else
                strJSON = serializer.Serialize(oDatosFiltrosAvanzados(0)) & _
                        ";_anioSeleccionadoInicio=" & CType(oDatosFiltrosAvanzados(1), Integer)
                _anio = CType(oDatosFiltrosAvanzados(1), Integer)
            End If
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "datosFiltrosAvanzados", "<script>var datosFiltrosAvanzados =" & strJSON & "</script>")
        End If

        Cargar_TextosPantalla()

        If Not Page.IsPostBack Then
            Cache.Remove("dsPedidosAbiertos_" & FSNUser.Cod)

            If Request.QueryString("denart") IsNot Nothing Then _denArticulo = Request.QueryString("denart")
            With whdgPedidosAbiertos
                .Behaviors.Paging.PageSize = 5
                .GridView.Behaviors.Paging.PageSize = 5
            End With
        Else
            Select Case Request("__EVENTTARGET")
                Case btnFiltrarPedAbierto.ClientID
                    whdgPedidosAbiertos.GridView.Behaviors.Paging.PageIndex = 0
                    Cache.Remove("dsPedidosAbiertos_" & FSNUser.Cod)
                    Dim oFiltosAvanzados As Object = serializer.Deserialize(Of Object)(Request("__EVENTARGUMENT"))
                    _anio = oFiltosAvanzados("Anio")
                    _numPedido = oFiltosAvanzados("NumPedido")
                    _numOrden = oFiltosAvanzados("NumOrden")
                    _idEmpresa = oFiltosAvanzados("IdEmpresa")
                    _codArticulo = oFiltosAvanzados("CodArticulo")
                    _fecInicioDesde = oFiltosAvanzados("FecInicioDesde")
                    _fecInicioHasta = oFiltosAvanzados("FecInicioHasta")
                    _fecFinDesde = oFiltosAvanzados("FecFinDesde")
                    _fecFinHasta = oFiltosAvanzados("FecFinHasta")
                    _codProve = oFiltosAvanzados("CodProve")
                Case Else

            End Select
        End If
        Cargar_PedidosAbiertos()
    End Sub
    Private Sub Cargar_TextosPantalla()
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
            Dim sVariableJavascriptTextosPantalla As String = "var TextosPantalla = new Array();"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[0]='" & JSText(Textos(0)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[1]='" & JSText(Textos(1)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[2]='" & JSText(Textos(2)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[3]='" & JSText(Textos(3)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[4]='" & JSText(Textos(4)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[5]='" & JSText(Textos(5)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[6]='" & JSText(Textos(6)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[7]='" & JSText(Textos(7)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[8]='" & JSText(Textos(8)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[9]='" & JSText(Textos(9).ToUpper) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[10]='" & JSText(Textos(10)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[11]='" & JSText(Textos(11)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[12]='" & JSText(Textos(12)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[13]='" & JSText(Textos(13)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[14]='" & JSText(Textos(14)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[15]='" & JSText(Textos(15)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[16]='" & JSText(Textos(16)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[17]='" & JSText(Textos(17)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[18]='" & JSText(Textos(18)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[19]='" & JSText(Textos(19)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[20]='" & JSText(Textos(20)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[21]='" & JSText(Textos(21)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[22]='" & JSText(Textos(22)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[23]='" & JSText(Textos(23)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[24]='" & JSText(Textos(24)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[25]='" & JSText(Textos(25)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[26]='" & JSText(Textos(28)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[27]='" & JSText(Textos(29)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[28]='" & JSText(Textos(30)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[29]='" & JSText(Textos(31)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[30]='" & JSText(Textos(32)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[31]='" & JSText(Textos(33)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[32]='" & JSText(Textos(34)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[33]='" & JSText(Textos(35)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[34]='" & JSText(Textos(36)) & "';"
			sVariableJavascriptTextosPantalla &= "TextosPantalla[35]='" & JSText(Textos(37)) & "';"
			sVariableJavascriptTextosPantalla &= "TextosPantalla[36]='" & JSText(Textos(38)) & "';"
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextosPantalla, True)

            CType(whdgPedidosAbiertos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblSeleccionPedidoAbierto"), Label).Text = Textos(9).ToUpper
            CType(whdgPedidosAbiertos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(14)
            CType(whdgPedidosAbiertos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(15)
            whdgPedidosAbiertos.GridView.Columns("EMPRESA").Header.Text = Textos(1)
            whdgPedidosAbiertos.GridView.Columns("CODPROVE").Header.Text = Textos(10)
            whdgPedidosAbiertos.GridView.Columns("DENPROVE").Header.Text = Textos(11)
            whdgPedidosAbiertos.GridView.Columns("NUMPEDERP").Header.Text = Textos(12)
            whdgPedidosAbiertos.GridView.Columns("FECINI").Header.Text = Textos(3)
            whdgPedidosAbiertos.GridView.Columns("FECFIN").Header.Text = Textos(4)
            whdgPedidosAbiertos.GridView.Columns("TIPO").Header.Text = Textos(13)
        End If
    End Sub
    Private Sub Paginador()
        Dim pagerList As DropDownList = DirectCast(whdgPedidosAbiertos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
        pagerList.Items.Clear()
        For i As Integer = 1 To whdgPedidosAbiertos.GridView.Behaviors.Paging.PageCount
            pagerList.Items.Add(i.ToString())
        Next
        pagerList.SelectedIndex = whdgPedidosAbiertos.GridView.Behaviors.Paging.PageIndex
        CType(whdgPedidosAbiertos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = whdgPedidosAbiertos.GridView.Behaviors.Paging.PageCount

        Dim desactivado As Boolean = ((whdgPedidosAbiertos.GridView.Behaviors.Paging.PageCount = 1) OrElse (whdgPedidosAbiertos.GridView.Behaviors.Paging.PageIndex = 0))
        With CType(whdgPedidosAbiertos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero" & IIf(desactivado, "_desactivado", "") & ".gif"
            .Enabled = False
        End With
        With CType(whdgPedidosAbiertos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior" & IIf(desactivado, "_desactivado", "") & ".gif"
            .Enabled = False
        End With
        desactivado = (whdgPedidosAbiertos.GridView.Behaviors.Paging.PageCount = 1)
        With CType(whdgPedidosAbiertos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(desactivado, "_desactivado", "") & ".gif"
            .Enabled = Not desactivado
        End With
        With CType(whdgPedidosAbiertos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(desactivado, "_desactivado", "") & ".gif"
            .Enabled = Not desactivado
        End With
    End Sub
    Private Sub Cargar_PedidosAbiertos()
        With whdgPedidosAbiertos
            .Rows.Clear()
            .DataSource = Pedidos_Abiertos
            .GridView.DataSource = .DataSource
            .DataMember = "PEDIDOS_ABIERTOS"
            .DataBind()
        End With
        upGrid.Update()
        'Pasamos a javascript los proveedores que cumplan con los pedidos abiertos que mostramos
        Dim viewProveedores As New DataView(whdgPedidosAbiertos.DataSource.Tables("PEDIDOS_ABIERTOS"))
        Dim dtProveedores As DataTable = viewProveedores.ToTable(True, "CODPROVE", "NIF", "DENPROVE")
        Me.InsertarEnCache("dtProveedoresPedidosAbiertos_" & FSNUser.Cod, dtProveedores)
    End Sub
#Region "Eventos Grid"
    Private Sub whdgPedidosAbiertos_DataBound(sender As Object, e As System.EventArgs) Handles whdgPedidosAbiertos.DataBound
        Paginador()
    End Sub
    Private Sub whdgPedidosAbiertos_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgPedidosAbiertos.InitializeRow
        If e.Row.DataItem.Item.Row("TIPO") = TipoPedidoAbierto.Importe Then
            e.Row.Items(whdgPedidosAbiertos.GridView.Columns.FromKey("TIPO").Index).Text = Textos(26)
        Else
            e.Row.Items(whdgPedidosAbiertos.GridView.Columns.FromKey("TIPO").Index).Text = Textos(27)
        End If
    End Sub
    Private Sub whdgPedidosAbiertos_PageIndexChanged(sender As Object, e As Infragistics.Web.UI.GridControls.PagingEventArgs) Handles whdgPedidosAbiertos.PageIndexChanged
        Dim desactivado As Boolean = ((whdgPedidosAbiertos.GridView.Behaviors.Paging.PageCount = 1) OrElse (whdgPedidosAbiertos.GridView.Behaviors.Paging.PageIndex = 0))
        With CType(whdgPedidosAbiertos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero" & IIf(desactivado, "_desactivado.gif", ".gif")
            .Enabled = Not desactivado
            .CssClass = IIf(desactivado, "", "Link")
        End With
        With CType(whdgPedidosAbiertos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior" & IIf(desactivado, "_desactivado.gif", ".gif")
            .Enabled = Not desactivado
            .CssClass = IIf(desactivado, "", "Link")
        End With
        desactivado = ((whdgPedidosAbiertos.GridView.Behaviors.Paging.PageCount = 1) OrElse (whdgPedidosAbiertos.GridView.Behaviors.Paging.PageIndex = whdgPedidosAbiertos.GridView.Behaviors.Paging.PageCount - 1))
        With CType(whdgPedidosAbiertos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(desactivado, "_desactivado.gif", ".gif")
            .Enabled = Not desactivado
            .CssClass = IIf(desactivado, "", "Link")
        End With
        With CType(whdgPedidosAbiertos.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(desactivado, "_desactivado.gif", ".gif")
            .Enabled = Not desactivado
            .CssClass = IIf(desactivado, "", "Link")
        End With
    End Sub
#End Region
#Region "Page methods"
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function Obtener_Proveedores_PedidosAbiertos() As String
        Try
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim dtProveedores As DataTable = CType(HttpContext.Current.Cache("dtProveedoresPedidosAbiertos_" & FSNUser.Cod), DataTable)
            Dim dtResultados As DataTable = CType(HttpContext.Current.Cache("dsPedidosAbiertos_" & FSNUser.Cod), DataSet).Tables(0)
            Dim serializer As New JavaScriptSerializer
            Return serializer.Serialize(dtProveedores.Rows.OfType(Of DataRow).Select(Function(x) _
                                                    New With {Key .codProve = x("CODPROVE").ToString, .nifProve = x("NIF").ToString, .denProve = x("DENPROVE").ToString, _
                                                                .numPedidosAbiertos = dtResultados.Rows.OfType(Of DataRow).Where(Function(y) y("CODPROVE") = x("CODPROVE").ToString).ToList.Count}).ToList)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function Obtener_Lineas_PedidoAbierto(ByVal Orden As Integer) As String
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim cPedidoAbierto As FSNServer.CPedidoAbierto
            cPedidoAbierto = FSNServer.Get_Object(GetType(FSNServer.CPedidoAbierto))
            Dim serializer As New JavaScriptSerializer
            Return serializer.Serialize(cPedidoAbierto.Cargar_Lineas_PedidoAbierto(FSNUser.CodPersona, Orden, FSNUser.NumberFormat))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function Agregar_a_Cesta(ByVal NumPeticion As Integer, ByVal Orden_Ped_Abierto As Integer, _
                                           ByVal IdLineaPedido As Integer, ByVal Importe As Double, ByVal Cantidad As Double) As String
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")

            Dim oCPedidoAbierto As FSNServer.CPedidoAbierto = FSNServer.Get_Object(GetType(FSNServer.CPedidoAbierto))
            oCPedidoAbierto.Agregar_Cesta_PedidoAbierto(FSNUser.CodPersona, Orden_Ped_Abierto, IdLineaPedido, Cantidad, Importe)

            'Tras agregar a la cesta obtenemos la info de la linea de pedido abierto para mostrar al usuario lo pendiente y lo que tiene en cesta
            Dim dr As DataRow = FSNUser.InfoCesta(FSNUser.Cod)
            Dim dImp As Double
            If dr.Item("IMPORTE") > 0 Then
                dImp = CDbl(dr.Item("IMPORTE") * dr.Item("EQUIV"))
            Else
                dImp = 0
            End If

            Dim serializer As New JavaScriptSerializer
            Return serializer.Serialize({NumPeticion, FSNLibrary.FormatNumber(dImp, FSNUser.NumberFormat) & " " & dr.Item("MON").ToString(), dr.Item("NUMARTICULOS").ToString(),
                                         oCPedidoAbierto.Cargar_Lineas_PedidoAbierto(FSNUser.CodPersona, Orden_Ped_Abierto, FSNUser.NumberFormat, IdLineaPedido)})
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
End Class
