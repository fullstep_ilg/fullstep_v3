﻿<%@  Page Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Publica.master" CodeBehind="politicacookies.aspx.vb" Inherits="Fullstep.FSNWeb.politicacookies" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>  
    <script type="text/javascript" src="../js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../js/politicacookies.js"></script>
    <script type="text/javascript">
        
        $('document').ready(function () {
            if (areCookiesEnabled()) {
                $("#alerta").hide();
            }
        });    
    </script>  
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="CPH1" runat="server">
        <div class="textos" style="width:80%;margin:1em auto;" >
            <div id="alerta" class="CajaTexto" style="background-color:#ffc;">
                <img src="../images/Icono_Error_Amarillo_40x40.gif" alt="alerta" style="float:left;margin:0.8em;" />
                <p><asp:label ID="lblAlerta" runat="server" CssClass="textos">Alerta</asp:label></p>
            </div>
            <div style="clear:both;"></div>
            <h2 class="RotuloGrande"><asp:label ID="lblTitulo"  runat="server">Titulo</asp:label></h2>            
            <p><asp:label ID="Label3" runat="server">Texto de cookies</asp:label></p>
            <p><asp:label ID="Label4" runat="server">Texto de cookies</asp:label></p>
            <p><asp:label ID="Label5" runat="server">Texto de cookies</asp:label></p>
            <p><asp:label ID="Label6" runat="server">Texto de cookies</asp:label></p>

            <p >
                <asp:label ID="Label7"  runat="server" CssClass="Rotulo">Texto de cookies</asp:label>
                <asp:label ID="Label8" runat="server">Texto de cookies</asp:label>
            </p>
            <p><asp:label ID="Label9" runat="server">Texto de cookies</asp:label></p>

            <p>
                <asp:label ID="Label10" runat="server" CssClass="Rotulo">Texto de cookies</asp:label>
                <asp:label ID="Label11" runat="server">Texto de cookies</asp:label>
            </p>
            <p><asp:label ID="Label12" runat="server">Texto de cookies</asp:label></p>
            <div style="margin:0 auto;text-align:center;">
                <div class="CajaTexto" style="display: inline-block; padding: 2em;width:60%;">
                    <p>
                        <asp:Label ID="Label13" class="Rotulo" runat="server">Cookies en navegador(pers)</asp:Label>
                    </p>
                    <p style="margin:0 8em;text-align:left" class="textos">
                        <asp:Label ID="lblInstrucciones" runat="server" >instrucciones</asp:Label>
                    </p>
                    <p style="text-align: center;">
                        <asp:Image ID="imgExplorer" runat="server" ImageAlign="Middle" BorderStyle="Solid" Width="80%" />
                    </p>
                    
                </div>
            </div>

            <p style="text-align: center;">
                <a href="javascript:window.history.back()" id="aVolver">
                    <asp:Label ID="lblVolver" runat="server">Volver</asp:Label>
                </a>
            </p>
    </div>
</asp:Content>
