<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.Master"
	CodeBehind="ConfigurarFiltrosQA.aspx.vb" Inherits="Fullstep.FSNWeb.ConfigurarFiltrosQA"
	EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>

<asp:Content ID="ContentHead" ContentPlaceHolderID="head" runat="server">
	<script type="text/javascript">
		/*''' <summary>
		''' Muestra un mensaje
		''' </summary>
		''' <param name="pregunta">Texto a monstrar en el alert</param>       
		''' <returns>True / false dependiendo de si aceptas o no el mensaje</returns>
		''' <remarks>Llamada desde:=btnEliminar</remarks>*/
		function ConfirmarEliminar(pregunta) {
			if (confirm(pregunta))
				return true;
			else
				return false;
		}
		/*Descripcion: Funcion que inicializa la caja de texto del combo Subtipo a ""
		Parametros entrada:=
		elem:=componente input
		event:=evento
		Llamada desde: Al cambiar el valor del combo tipo
		Tiempo ejecucion:= 0seg.*/
		function ValueChanged_wddTipo_CambioTipo(elem, event) {
		    comboSubtipo = document.getElementById("x:1129115704.2:mkr:Input");
			if (comboSubtipo) comboSubtipo.value = "";
		}
	</script>
</asp:Content>
<asp:Content  ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
	<asp:HiddenField ID="Accion" runat="server" Value="" />
	<fsn:FSNPageHeader ID="FSNPageHeader" runat="server" VisibleBotonVolver="true"></fsn:FSNPageHeader>
	<asp:ValidationSummary ID="vsValidador" HeaderText="" DisplayMode="List" ShowMessageBox="true"
		ShowSummary="false" EnableClientScript="true" runat="server" ValidationGroup="obligatorio" />
	<table width="100%" border="0">
		<tr>
			<td nowrap="nowrap">
				<asp:Label ID="lblNombreFiltro" runat="server" CssClass="Etiqueta"></asp:Label>
			</td>
			<td width="45%">
				<asp:TextBox ID="txtNombreFiltro" runat="server" MaxLength="50" Width="395px"></asp:TextBox>
				<asp:RequiredFieldValidator ID="reqValNombre" runat="server" Display="None" ControlToValidate="txtNombreFiltro"
					ValidationGroup="obligatorio"></asp:RequiredFieldValidator>
			</td>
			<td nowrap="nowrap" align="left" colspan="3">
				<asp:Label ID="lblFiltroPorDefecto" runat="server" Text="dFiltro por defecto:" Font-Bold="true"></asp:Label>
				<asp:CheckBox ID="chkFiltroPorDefecto" runat="server" AutoPostBack="false" />
			</td>
		</tr>
		<tr>
			<asp:PlaceHolder ID="phFiltrosDisponiblesTodos" Visible="false" runat="server">
				<td width="25%" nowrap="nowrap">
					<asp:UpdatePanel ID="upTipoFormulario" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:RadioButtonList ID="RadioButtonList" runat="server" AutoPostBack="true" CssClass="Etiqueta"
								Visible="false">
								<asp:ListItem>DTodos Formularios:</asp:ListItem>
								<asp:ListItem Selected="true">DFormularios:</asp:ListItem>
							</asp:RadioButtonList>
							<asp:Label ID="lblFiltrosDisponibles" runat="server" Text="DFormularios:" CssClass="Etiqueta"
								Visible="false"></asp:Label>
						</ContentTemplate>
					</asp:UpdatePanel>
				</td>
				<td valign="bottom">
					<asp:UpdatePanel ID="upFiltrosDisponibles" runat="server" UpdateMode="Conditional"
						ChildrenAsTriggers="false">
						<ContentTemplate>
							<ig:WebDropDown ID="wddFiltrosDisponibles" runat="server" Width="398px" EnableAutoCompleteFirstMatch="false"
								DropDownContainerWidth="398px" DropDownContainerHeight="200px" AutoPostBack="true" 
								EnableClosingDropDownOnSelect="true" EnableClosingDropDownOnBlur="true" CurrentValue="">
							</ig:WebDropDown>
						</ContentTemplate>
					</asp:UpdatePanel>
				</td>
			</asp:PlaceHolder>
			<td align="right" valign="bottom">
				&nbsp;
			</td>
			<td align="right" valign="bottom">
				&nbsp;
			</td>
			<td align="right" valign="bottom">
				<asp:UpdatePanel ID="upBotones" runat="server" UpdateMode="conditional" ChildrenAsTriggers="false">
					<ContentTemplate>
						<table style="width: 100%" cellpadding="3" cellspacing="0" border="0">
							<tr align="right">
								<td style="width: 100%;">
									&nbsp;
								</td>
								<td>
									<fsn:FSNButton ID="btnEliminar" runat="server" Text="DEliminar" Visible="false"></fsn:FSNButton>
								</td>
								<td>
									<fsn:FSNButton ID="btnAceptar" runat="server" Text="DAceptar" Visible="false" ValidationGroup="obligatorio"></fsn:FSNButton>
								</td>
								<td>
									<fsn:FSNButton ID="btnCancelar" runat="server" Text="DCancelar" Visible="false"></fsn:FSNButton>
								</td>
							</tr>
						</table>
					</ContentTemplate>
				</asp:UpdatePanel>
			</td>
		</tr>
		<tr>
			<td colspan="5">
				&nbsp;
			</td>
		</tr>
	</table>
	<asp:UpdatePanel ID="upCriteriosBusquedaUno" runat="server" UpdateMode="conditional"
		ChildrenAsTriggers="false">
		<Triggers>
			<asp:AsyncPostBackTrigger ControlID="wddFiltrosDisponibles" EventName="SelectionChanged" />
			<asp:AsyncPostBackTrigger ControlID="RadioButtonList" EventName="SelectedIndexChanged" />
		</Triggers>
		<ContentTemplate>
			<asp:PlaceHolder ID="phConfiguracionFiltroUno" Visible="false" runat="server">
				<ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" CollapseControlID="pnlCabeceraBusquedaUno"
					ExpandControlID="pnlCabeceraBusquedaUno" ImageControlID="imgExpandir0" SuppressPostBack="True"
					TargetControlID="pnlParametrosUno" SkinID="FondoRojo">
				</ajx:CollapsiblePanelExtender>
				<asp:Panel ID="pnlCabeceraBusquedaUno" runat="server" CssClass="PanelCabecera" Style="cursor: pointer;
					width: 100%; height: 20px" Font-Bold="True">
					<table border="0">
						<tr>
							<td>
								<asp:Image ID="imgExpandir0" runat="server" SkinID="Contraer" />
							</td>
							<td style="vertical-align: top">
								<asp:Label ID="lblParametrosUno" runat="server" Text="DSeleccione los criterios de b�squeda para los resultados del filtro:"
									Font-Bold="True" ForeColor="White"></asp:Label>
							</td>
						</tr>
					</table>
				</asp:Panel>
				<asp:Panel ID="pnlParametrosUno" runat="server" CssClass="Rectangulo" Width="99%">
					<fsqa:BusquedaAvanzadaNC ID="BusquedaNCUno" runat="server"></fsqa:BusquedaAvanzadaNC>
					<fsqa:BusquedaAvanzadaCERT ID="BusquedaCertUno" runat="server"></fsqa:BusquedaAvanzadaCERT>
				</asp:Panel>
				<p>
					<br />
				</p>
			</asp:PlaceHolder>
		</ContentTemplate>
	</asp:UpdatePanel>
	<asp:UpdatePanel ID="upCamposUno" runat="server" UpdateMode="conditional" ChildrenAsTriggers="false">
		<Triggers>
			<asp:AsyncPostBackTrigger ControlID="wddFiltrosDisponibles" EventName="SelectionChanged" />
			<asp:AsyncPostBackTrigger ControlID="RadioButtonList" EventName="SelectedIndexChanged" />
		</Triggers>
		<ContentTemplate>
			<asp:PlaceHolder runat="server" ID="phCamposUno" Visible="false">
				<ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server" CollapseControlID="pnlCabeceraCamposUno"
					ExpandControlID="pnlCabeceraCamposUno" ImageControlID="imgExpandir2" SuppressPostBack="True"
					TargetControlID="pnlCamposUno" SkinID="FondoRojo">
				</ajx:CollapsiblePanelExtender>                            
				<asp:Panel ID="pnlCabeceraCamposUno" runat="server" CssClass="PanelCabecera" Style="cursor: pointer;
					width: 100%; height: 20px" Font-Bold="True">
					<table border="0">
						<tr>
							<td>
								<asp:Image ID="imgExpandir2" runat="server" SkinID="Contraer" />
							</td>
							<td style="vertical-align: top">
								<asp:Label ID="lblCabeceraCamposUno" runat="server" Text="DSeleccione los campos a visualizar en el filtro:"
									Font-Bold="True" ForeColor="White"></asp:Label>
							</td>
						</tr>
					</table>
				</asp:Panel>                   
				<asp:Panel ID="pnlCamposUno" runat="server" CssClass="Rectangulo" Style="width: 99%">
					<br />                     
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr style="padding-bottom: 5px">
							<td>
								<asp:Label ID="lblSeleccionarCampos" runat="server" Text="DSeleccione los campos de la solicitud a visualizar en el filtro:"
									CssClass="Rotulo"></asp:Label>
							</td>
							<td>
								<asp:Label ID="lblDatosGeneralesUno" runat="server" Text="DSeleccione los datos generales a visualizar en el filtro:"
									CssClass="Rotulo"></asp:Label>
							</td>
						</tr>
						<tr>
							<td valign="top" style="width: 50%; padding-right: 15px">
							<div id="divPanelUno" runat="server" style="overflow:auto; height:495px">
								<asp:Repeater ID="rpGrupos" runat="server">
									<ItemTemplate>
										<table border="0" cellpadding="0" cellspacing="0" width="99%" style="border-bottom: solid 1px #B2B2B2;
											border-left: solid 1px #B2B2B2; border-right: solid 1px #B2B2B2; border-top: solid 1px #B2B2B2">
											<tr>
												<td>
													<asp:Panel ID="pnlGrupo" runat="server" Style="width: 99%; padding-left: 5px; cursor: pointer;
														height: 20px" Font-Bold="True">
														<table border="0">
															<tr>
																<td>
																	<asp:Image ID="imgExpandir" runat="server" SkinID="ContraerFondoTrans" />
																</td>
																<td style="vertical-align: top">
																	<b>
																		<%#DataBinder.Eval(Container.DataItem, "NOMBRE")%></b>
																</td>
															</tr>
														</table>
													</asp:Panel>
												</td>
											</tr>
										</table>
										<asp:Panel ID="pnlCampos" runat="server" Style="width: 99%; height: 25px;">
											<asp:Repeater ID="rpCampos" runat="server" DataSource='<%#Container.DataItem.Row.GetChildRows("REL_GRUPO_CAMPOS")%>'>
												<HeaderTemplate>
													<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: White;
														border-right: solid 1px #B2B2B2; border-left: solid 1px #B2B2B2; border-bottom: solid 1px #B2B2B2;">
														<tr style="background-color: #FAFAFA; height: 20px">
															<td style="width: 40%; padding-left: 3px;">
																<asp:Label ID="lblCampos" runat="server" Text='<%# Textos(12)%>'></asp:Label>
															</td>
															<td align="center" style="width: 10%; border-right: solid 1px #B2B2B2; border-left: solid 1px #B2B2B2;">
																<asp:Label ID="lblVisible" runat="server" Text='<%# Textos(13)%>'></asp:Label>
															</td>
															<td style="width: 50%; padding-left: 3px">
																<asp:Label ID="lblNombrePersonalizado1" runat="server" Text='<%# Textos(14)%>'></asp:Label>
															</td>
														</tr>
												</HeaderTemplate>
												<ItemTemplate>
													<tr>
														<td style="width: 40%; padding-left: 3px; border-top: solid 1px #B2B2B2">
															<asp:Label ID="CampoFormulario_lblCampoGeneral" runat="server" Text='<%#Container.DataItem("NOMBRE")%>'></asp:Label>
														</td>
														<td align="center" style="width: 10%; border-right: solid 1px #B2B2B2; border-left: solid 1px #B2B2B2;
															border-top: solid 1px #B2B2B2">
															<asp:HiddenField ID="CampoFormularioID" Value='<%#Container.DataItem("ID")%>' runat="server" />
															<asp:CheckBox ID="CampoFormulario_Visible" runat="server" Checked='<%#Container.DataItem("VISIBLE_GRID")%>'
																AutoPostBack="true" />
														</td>
														<td style="width: 50%; padding-left: 3px; border-top: solid 1px #B2B2B2; padding-top: 2px;
															padding-bottom: 2px">
															<asp:TextBox ID="CampoFormulario_NombrePersonalizado" runat="server" Width="98%"
																AutoPostBack="true" Text='<%#Container.DataItem("NOMBRE_GRID")%>'></asp:TextBox>
														</td>
													</tr>
												</ItemTemplate>
												<FooterTemplate>
													</table>
												</FooterTemplate>
											</asp:Repeater>
										</asp:Panel>
										<ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" CollapseControlID="pnlGrupo"
											ExpandControlID="pnlGrupo" SkinID="FondoTrans2" ImageControlID="imgExpandir"
											SuppressPostBack="True" TargetControlID="pnlCampos">
										</ajx:CollapsiblePanelExtender>                                       
									</ItemTemplate>
								</asp:Repeater>
							</div> 
							</td>
							<td valign="top" style="width: 50%; padding-right: 15px">
								<table width="100%" style="border: solid 1px #B2B2B2" cellpadding="0" cellspacing="0">
									<asp:Repeater ID="RepeaterDatosGeneralesUno" runat="server">
										<HeaderTemplate>
											<tr style="height: 20px">
												<td style="width: 40%; padding-left: 3px;">
													<asp:Label ID="lblCamposGeneralesUno" runat="server" Text='<%# Textos(15)%>'></asp:Label>
												</td>
												<td align="center" style="width: 10%; border-right: solid 1px #B2B2B2; border-left: solid 1px #B2B2B2;">
													<asp:Label ID="lblVisible2Uno" runat="server" Text='<%# Textos(13)%>'></asp:Label>
												</td>
												<td style="width: 50%; padding-left: 3px">
													<asp:Label ID="lblNombrePersonalizado2Uno" runat="server" Text='<%# Textos(14)%>'></asp:Label>
												</td>
											</tr>
										</HeaderTemplate>
										<ItemTemplate>
											<tr style="background-color: White">
												<td style="width: 40%; padding-left: 3px; border-top: solid 1px #B2B2B2">
													<asp:Label ID="CampoGeneral_lblCampoGeneralUno" runat="server" Text='<%#Container.DataItem("NOMBRE")%>'></asp:Label>
												</td>
												<td align="center" style="width: 10%; border-right: solid 1px #B2B2B2; border-left: solid 1px #B2B2B2;
													border-top: solid 1px #B2B2B2">
													<asp:CheckBox ID="CampoGeneral_VisibleUno" runat="server" AutoPostBack="true" />
												</td>
												<td style="width: 50%; padding-left: 3px; border-top: solid 1px #B2B2B2; padding-top: 2px;
													padding-bottom: 2px">
													<asp:TextBox ID="CampoGeneral_NombrePersonalizadoUno" runat="server" Width="98%"
														AutoPostBack="true"></asp:TextBox>
												</td>
											</tr>
										</ItemTemplate>
									</asp:Repeater>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<br />                            
								<asp:Label ID="lblMensaje1Uno" runat="server" CssClass="Rotulo"></asp:Label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<asp:UpdatePanel runat="server" ID="upGridUno" UpdateMode="Conditional">
									<Triggers>
										<asp:AsyncPostBackTrigger ControlID="wddFiltrosDisponibles" EventName="SelectionChanged" />
										<asp:AsyncPostBackTrigger ControlID="RadioButtonList" EventName="SelectedIndexChanged" />
									</Triggers>
									<ContentTemplate>    
										<ig:WebHierarchicalDataGrid ID="whdgConfigurarFiltrosUno" runat="server" Width="99%" SkinID="ConfiguracionFiltros"
											AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="true" ExpansionColumnCss="hidExpColumn">
											<Behaviors>
												<ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
												<ig:ColumnMoving Enabled="true"></ig:ColumnMoving>
											</Behaviors>
										</ig:WebHierarchicalDataGrid>                         
									</ContentTemplate>
								</asp:UpdatePanel>
							</td>
						</tr>
					</table>                    
				</asp:Panel>                
			</asp:PlaceHolder>
		</ContentTemplate>
	</asp:UpdatePanel>
	<asp:UpdatePanel ID="upCriteriosBusquedaTodos" runat="server" UpdateMode="conditional"
		ChildrenAsTriggers="false">
		<Triggers>
			<asp:AsyncPostBackTrigger ControlID="wddFiltrosDisponibles" EventName="SelectionChanged" />
			<asp:AsyncPostBackTrigger ControlID="RadioButtonList" EventName="SelectedIndexChanged" />
		</Triggers>
		<ContentTemplate>
			<asp:PlaceHolder runat="server" ID="phConfiguracionFiltroTodos" Visible="false">
				<ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server" CollapseControlID="pnlCabeceraBusquedaTodos"
					ExpandControlID="pnlCabeceraBusquedaTodos" ImageControlID="imgExpandir1" SuppressPostBack="True"
					TargetControlID="pnlParametrosTodos" SkinID="FondoRojo">
				</ajx:CollapsiblePanelExtender>
				<asp:Panel ID="pnlCabeceraBusquedaTodos" runat="server" CssClass="PanelCabecera"
					Style="cursor: pointer; width: 100%; height: 20px" Font-Bold="True">
					<table border="0">
						<tr>
							<td>
								<asp:Image ID="imgExpandir1" runat="server" SkinID="Contraer" />
							</td>
							<td style="vertical-align: top">
								<asp:Label ID="lblParametrosTodos" runat="server" Text="DSeleccione los criterios de b�squeda para los resultados del filtro:"
									Font-Bold="True" ForeColor="White"></asp:Label>
							</td>
						</tr>
					</table>
				</asp:Panel>
				<asp:Panel ID="pnlParametrosTodos" runat="server" CssClass="Rectangulo" Width="99%">
					<fsqa:BusquedaAvanzadaNC ID="BusquedaNCTodos" runat="server"></fsqa:BusquedaAvanzadaNC>
					<fsqa:BusquedaAvanzadaCERT ID="BusquedaCertTodos" runat="server"></fsqa:BusquedaAvanzadaCERT>
				</asp:Panel>
				<p>
					<br />
				</p>
			</asp:PlaceHolder>
		</ContentTemplate>
	</asp:UpdatePanel>
	<asp:UpdatePanel ID="upCamposTodos" runat="server" UpdateMode="conditional" ChildrenAsTriggers="false">
		<Triggers>
			<asp:AsyncPostBackTrigger ControlID="wddFiltrosDisponibles" EventName="SelectionChanged" />
			<asp:AsyncPostBackTrigger ControlID="RadioButtonList" EventName="SelectedIndexChanged" />
		</Triggers>
		<ContentTemplate>
			<asp:PlaceHolder runat="server" ID="phCamposTodos" Visible="false">
				<ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender4" runat="server" CollapseControlID="pnlCabeceraCamposTodos"
					ExpandControlID="pnlCabeceraCamposTodos" ImageControlID="imgExpandir4" SuppressPostBack="True"
					TargetControlID="pnlCamposTodos" SkinID="FondoRojo">
				</ajx:CollapsiblePanelExtender>
				<asp:Panel ID="pnlCabeceraCamposTodos" runat="server" CssClass="PanelCabecera" Style="cursor: pointer;
					width: 100%; height: 20px" Font-Bold="True">
					<table border="0">
						<tr>
							<td>
								<asp:Image ID="imgExpandir4" runat="server" SkinID="Contraer" />
							</td>
							<td style="vertical-align: top">
								<asp:Label ID="lblCabeceraCamposTodos" runat="server" Text="DSeleccione los campos a visualizar en el filtro:"
									Font-Bold="True" ForeColor="White"></asp:Label>
							</td>
						</tr>
					</table>
				</asp:Panel>
				<asp:Panel ID="pnlCamposTodos" runat="server" CssClass="Rectangulo" Style="width: 99%">
					<br />
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr style="padding-bottom: 5px">
							<td>
								<asp:Label ID="lblDatosGeneralesTodos" runat="server" Text="DSeleccione los datos generales a visualizar en el filtro:"
									CssClass="Rotulo"></asp:Label>
							</td>
							<td>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td valign="top" style="width: 50%; padding-right: 15px">
								<table width="100%" style="border: solid 1px #B2B2B2" cellpadding="0" cellspacing="0">
									<asp:Repeater ID="RepeaterDatosGeneralesTodos" runat="server">
										<HeaderTemplate>
											<tr style="height: 20px">
												<td style="width: 40%; padding-left: 3px;">
													<asp:Label ID="lblCamposGeneralesTodos" runat="server" Text='<%# Textos(15)%>'></asp:Label>
												</td>
												<td align="center" style="width: 10%; border-right: solid 1px #B2B2B2; border-left: solid 1px #B2B2B2;">
													<asp:Label ID="lblVisible2Todos" runat="server" Text='<%# Textos(13)%>'></asp:Label>
												</td>
												<td style="width: 50%; padding-left: 3px">
													<asp:Label ID="lblNombrePersonalizado2Todos" runat="server" Text='<%# Textos(14)%>'></asp:Label>
												</td>
											</tr>
										</HeaderTemplate>
										<ItemTemplate>
											<tr style="background-color: White">
												<td style="width: 40%; padding-left: 3px; border-top: solid 1px #B2B2B2">
													<asp:Label ID="CampoGeneral_lblCampoGeneralTodos" runat="server" Text='<%#Container.DataItem("NOMBRE")%>'></asp:Label>
												</td>
												<td align="center" style="width: 10%; border-right: solid 1px #B2B2B2; border-left: solid 1px #B2B2B2;
													border-top: solid 1px #B2B2B2">
													<asp:CheckBox ID="CampoGeneral_VisibleTodos" runat="server" AutoPostBack="true" />
												</td>
												<td style="width: 50%; padding-left: 3px; border-top: solid 1px #B2B2B2; padding-top: 2px;
													padding-bottom: 2px">
													<asp:TextBox ID="CampoGeneral_NombrePersonalizadoTodos" runat="server" Width="98%"
														AutoPostBack="true"></asp:TextBox>
												</td>
											</tr>
										</ItemTemplate>
									</asp:Repeater>
								</table>
							</td>
							<td>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<br />                           
								<asp:Label ID="lblMensaje1Todos" runat="server" Text="Para modificar el orden de los campos desplace las columnas."
									CssClass="Rotulo"></asp:Label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<asp:UpdatePanel runat="server" ID="upGridTodos" UpdateMode="Conditional">
									<Triggers>
										<asp:AsyncPostBackTrigger ControlID="wddFiltrosDisponibles" EventName="SelectionChanged" />
										<asp:AsyncPostBackTrigger ControlID="RadioButtonList" EventName="SelectedIndexChanged" />
									</Triggers>
									<ContentTemplate>
										<ig:WebHierarchicalDataGrid ID="whdgConfigurarFiltrosTodos" runat="server" Height="200px" Width="99%" SkinID="ConfiguracionFiltros"
										AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="true" ExpansionColumnCss="hidExpColumn">
											<Behaviors>
												<ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
												<ig:ColumnMoving Enabled="true"></ig:ColumnMoving>
											</Behaviors>
										</ig:WebHierarchicalDataGrid>
									</ContentTemplate>
								</asp:UpdatePanel>
							</td>
						</tr>
					</table>
				</asp:Panel>
			</asp:PlaceHolder>
		</ContentTemplate>
	</asp:UpdatePanel>
	<script type="text/javascript" language="javascript">
		var ModalProgress = '<%= ModalProgress.ClientID %>';
	</script>
	  
	<ajx:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
		BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />

</asp:Content>
