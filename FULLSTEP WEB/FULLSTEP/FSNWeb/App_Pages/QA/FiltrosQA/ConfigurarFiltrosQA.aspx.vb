﻿Partial Public Class ConfigurarFiltrosQA
    Inherits FSNPage

    Private cFiltro As FSNServer.FiltroQA
    Private dsCampos As DataSet
    Private dsCamposGenerales As DataSet
    Private arrDatosGenerales() As String
    Private arrDatosGeneralesDen() As String
    Private m_sTextoCargando As String = "Cargando..."
    Private RepeaterDatosGeneral As Global.System.Web.UI.WebControls.Repeater
    Private sTextoCGVisible As String
    Private sTextoCGNombre As String
    Private sTextoCGID As String
    Private sTextoCGLbl As String
    Private Grid As Global.Infragistics.Web.UI.GridControls.WebHierarchicalDataGrid
    Private NCBusqueda As Global.Fullstep.FSNWeb.wucBusquedaAvanzadaNC
    Private CertBusqueda As Global.Fullstep.FSNWeb.wucBusquedaAvanzadaCert
#Region " FILTROS DISPONIBLES "
    ''' <summary>
    ''' Cargamos el desplegable con los filtros disponibles para el usuario
    ''' </summary>
    ''' <param name="iTipoSolicitud">2 No Conformidad   3 Certificado</param>
    ''' <remarks>Page_Load(). Tiempo máximo: 0 sg</remarks>
    Private Sub CargarFiltrosDisponibles(ByVal iTipoSolicitud As Byte)
        Dim cFiltros As FSNServer.FiltrosQA

        cFiltros = FSNServer.Get_Object(GetType(FSNServer.FiltrosQA))
        cFiltros.Tipo = IIf(iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado, 0, 1)
        Dim ds As DataSet = cFiltros.LoadDisponibles(iTipoSolicitud)
        Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem

        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            oItem = New Infragistics.Web.UI.ListControls.DropDownItem
            oItem.Value = ds.Tables(0).Rows(i).Item("ID")
            oItem.Text = ds.Tables(0).Rows(i).Item("DEN")
            wddFiltrosDisponibles.Items.Add(oItem)
        Next

        cFiltros = Nothing
    End Sub
    ''' <summary>
    ''' Cuando se selecciona un formulario, se cargan en el treview los campos de ese formulario y se chequean los campos generales marcados por defecto.
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>       
    ''' <remarks>Llamada desde: Al cambiar el dropdownlist con los formularios. Tiempo máximo: 1 sg.</remarks>
    Private Sub wddFiltrosDisponibles_SelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles wddFiltrosDisponibles.SelectionChanged
        If Not IsNumeric(wddFiltrosDisponibles.SelectedValue) Then Exit Sub
        If Session("QAIdFiltro") = wddFiltrosDisponibles.SelectedValue Then Exit Sub
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltroQA
        Dim lFiltroID As Long = CLng(wddFiltrosDisponibles.SelectedValue)

        If lFiltroID > 0 Then
            RepeaterDatosGeneral = RepeaterDatosGeneralesUno
            sTextoCGVisible = "CampoGeneral_VisibleUno"
            sTextoCGNombre = "CampoGeneral_NombrePersonalizadoUno"
            sTextoCGID = "CampoGeneralIDUno"
            sTextoCGLbl = "CampoGeneral_lblCampoGeneralUno"

            Grid = whdgConfigurarFiltrosUno

            NCBusqueda = BusquedaNCUno
            CertBusqueda = BusquedaCertUno

            If CertBusqueda.MostrarItemProveedores = -1 Then CertBusqueda.MostrarProveedores = 3

            Session("QAIdFiltro") = lFiltroID

            cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroQA))
            cFiltro.IDFiltro = lFiltroID
            cFiltro.Persona = FSNUser.CodPersona
            cFiltro.TipoSol = IIf(Request("Donde") = "VisorCert", TiposDeDatos.TipoDeSolicitud.Certificado, TiposDeDatos.TipoDeSolicitud.NoConformidad)
            cFiltro.UnForm = True

            dsCampos = cFiltro.LoadCampos(Idioma)

            rpGrupos.DataSource = dsCampos.Tables(0)
            rpGrupos.DataBind()

            whdgConfigurarFiltrosTodos.Rows.Clear()
            whdgConfigurarFiltrosTodos.Columns.Clear()
            whdgConfigurarFiltrosTodos.GridView.Columns.Clear()
            whdgConfigurarFiltrosUno.Rows.Clear()
            whdgConfigurarFiltrosUno.Columns.Clear()
            whdgConfigurarFiltrosUno.GridView.Columns.Clear()
            whdgConfigurarFiltrosUno.GridView.ClearDataSource()

            Dim ds As DataSet = cFiltro.LoadConfiguracionInstancia(Idioma)
            Dim dt As DataTable
            Dim datasrc As New DataSet
            dt = ds.Tables(0)
            datasrc.Tables.Add(dt.Copy())
            whdgConfigurarFiltrosUno.DataSource = datasrc
            whdgConfigurarFiltrosUno.GridView.DataSource = whdgConfigurarFiltrosUno.DataSource
            CrearColumnasDT(whdgConfigurarFiltrosUno, dt)
            whdgConfigurarFiltrosUno.DataBind()
            ConfigurarGrid(whdgConfigurarFiltrosUno)

            'Campos Generales
            CargaCamposGenerales(0)

            If Request("Donde") = "VisorCert" Then
                BusquedaCertUno.Filtro = lFiltroID
            Else
                BusquedaNCUno.Filtro = lFiltroID
            End If

            phConfiguracionFiltroUno.Visible = True
            phCamposUno.Visible = True
            phConfiguracionFiltroTodos.Visible = False
            phCamposTodos.Visible = False

            btnAceptar.Visible = True
            btnCancelar.Visible = True
        Else
            phConfiguracionFiltroUno.Visible = False
            phCamposUno.Visible = False
            phConfiguracionFiltroTodos.Visible = False
            phCamposTodos.Visible = False

            btnAceptar.Visible = False
            btnCancelar.Visible = False
        End If

        upBotones.Update()
        upFiltrosDisponibles.Update()
        
        Accion.Value = "SeleccionFiltro"
    End Sub
#End Region
    ''' <summary>
    ''' Pone el texto correspondiente en la etiqueta lblProcesando.
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>       
    ''' <remarks>Llamada desde: El prerender de la etiqueta. Tiempo máximo: 0sg.</remarks>
    Protected Sub LblProcesando_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, Label).Text = m_sTextoCargando ' Cargando...
    End Sub
    ''' <summary>
    ''' Carga de la pagina
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>       
    ''' <remarks>Llamada desde: El prerender de la etiqueta. Tiempo máximo: 2sg.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sAuxNombre As String = ""
        If (Request("IdFiltroUsu") = "0") Then
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados
            sAuxNombre = Textos(0)
        End If

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltroQA

        If Request("Donde") = "VisorCert" Then
            InicializarArraysCamposGeneralesCert()

            RadioButtonList.Visible = True
            lblFiltrosDisponibles.Visible = False
        Else
            InicializarArraysCamposGeneralesNC()

            RadioButtonList.Visible = False
            lblFiltrosDisponibles.Visible = True
        End If

        If IsPostBack Then Exit Sub

        Dim mMaster = CType(Me.Master, FSNWeb.Menu)
        If Request("Donde") = "VisorCert" Then
            mMaster.Seleccionar("Calidad", "Certificados")
        Else
            mMaster.Seleccionar("Calidad", "NoConformidades")
        End If

        CargarIdiomasControles()

        BusquedaCertTodos.IdentidadPanel = 0

        If Request("IdFiltroUsu") <> "" Then
            cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroQA))
            cFiltro.Persona = FSNUser.CodPersona
            cFiltro.TipoSol = IIf(Request("Donde") = "VisorCert", TiposDeDatos.TipoDeSolicitud.Certificado, TiposDeDatos.TipoDeSolicitud.NoConformidad)

            cFiltro.IDFiltroUsuario = Request("IdFiltroUsu")
            cFiltro.LoadConfiguracion()

            If (Request("IdFiltroUsu") = "0") Then ''0->Aun no creado
                cFiltro.IDFiltro = -1
                cFiltro.UnForm = False

                cFiltro.NombreFiltro = sAuxNombre
            End If

            If cFiltro.IDFiltro = Nothing Then
                If Request("Donde") = "VisorCert" Then
                    Response.Redirect("..\Certificados\Certificados.aspx")
                Else
                    Response.Redirect("..\NoConformidades\NoConformidades.aspx")
                End If
            End If

            If cFiltro.IDFiltro = -1 Then cFiltro.IDFiltro = 0

            If cFiltro.UnForm Then
                If (cFiltro.IDFiltroUsuario > 0) Then
                    phFiltrosDisponiblesTodos.Visible = True

                    RadioButtonList.Visible = False
                    lblFiltrosDisponibles.Visible = True
                    lblFiltrosDisponibles.Text = Me.RadioButtonList.Items(1).Text & " " & cFiltro.TextFiltroUsuario
                    wddFiltrosDisponibles.Visible = False
                End If

                RepeaterDatosGeneral = RepeaterDatosGeneralesUno
                sTextoCGVisible = "CampoGeneral_VisibleUno"
                sTextoCGNombre = "CampoGeneral_NombrePersonalizadoUno"
                sTextoCGID = "CampoGeneralIDUno"
                sTextoCGLbl = "CampoGeneral_lblCampoGeneralUno"

                Grid = whdgConfigurarFiltrosUno

                NCBusqueda = BusquedaNCUno
                CertBusqueda = Me.BusquedaCertUno

                BusquedaCertTodos.IdentidadPanel = 0

                phConfiguracionFiltroUno.Visible = True
                phCamposUno.Visible = True
                phConfiguracionFiltroTodos.Visible = False
                phCamposTodos.Visible = False

                RadioButtonList.Items(0).Selected = False
                RadioButtonList.Items(1).Selected = True
            Else
                If (cFiltro.IDFiltroUsuario > 0) Then
                    phFiltrosDisponiblesTodos.Visible = True

                    RadioButtonList.Visible = False
                    lblFiltrosDisponibles.Visible = True
                    lblFiltrosDisponibles.Text = Me.RadioButtonList.Items(0).Text
                    wddFiltrosDisponibles.Visible = False
                End If

                RepeaterDatosGeneral = RepeaterDatosGeneralesTodos
                sTextoCGVisible = "CampoGeneral_VisibleTodos"
                sTextoCGNombre = "CampoGeneral_NombrePersonalizadoTodos"
                sTextoCGID = "CampoGeneralIDTodos"
                sTextoCGLbl = "CampoGeneral_lblCampoGeneralTodos"

                Grid = whdgConfigurarFiltrosTodos

                NCBusqueda = BusquedaNCTodos
                CertBusqueda = Me.BusquedaCertTodos

                BusquedaCertTodos.IdentidadPanel = 1

                phConfiguracionFiltroUno.Visible = False
                phCamposUno.Visible = False
                phConfiguracionFiltroTodos.Visible = True
                phCamposTodos.Visible = True

                RadioButtonList.Items(0).Selected = True
                RadioButtonList.Items(1).Selected = False
            End If

            btnEliminar.Visible = (cFiltro.NFiltrosConfig > 1) AndAlso Not (Request("IdFiltroUsu") = "0")
            btnAceptar.Visible = True
            btnCancelar.Visible = True

            btnEliminar.Attributes.Add("onClick", "return ConfirmarEliminar('" & JSText(Textos(17)) & "');")

            Session("QAIdFiltro") = CStr(cFiltro.IDFiltro)
            Session("QAIdFiltroUsu") = CStr(cFiltro.IDFiltroUsuario)
            Session("QANombreFiltro") = Request.QueryString("NombreTabla")

            FSNPageHeader.TituloCabecera = cFiltro.NombreFiltro

            txtNombreFiltro.Text = cFiltro.NombreFiltro
            chkFiltroPorDefecto.Checked = IIf(cFiltro.PorDefecto = 1 Or cFiltro.PorDefecto = 3, 1, 0)

            If cFiltro.UnForm Then
                dsCampos = cFiltro.LoadCampos(Idioma)

                rpGrupos.DataSource = dsCampos.Tables(0)
                rpGrupos.DataBind()
            End If

            dsCamposGenerales = cFiltro.LoadCamposGenerales(Idioma)

            If cFiltro.UnForm Then
                whdgConfigurarFiltrosTodos.Rows.Clear()
                whdgConfigurarFiltrosTodos.Columns.Clear()
                whdgConfigurarFiltrosTodos.GridView.Columns.Clear()
                whdgConfigurarFiltrosUno.Rows.Clear()
                whdgConfigurarFiltrosUno.Columns.Clear()
                whdgConfigurarFiltrosUno.GridView.Columns.Clear()
                whdgConfigurarFiltrosUno.GridView.ClearDataSource()

                Dim ds As DataSet = cFiltro.LoadConfiguracionInstancia(Idioma)
                Dim dt As DataTable
                Dim datasrc As New DataSet
                dt = ds.Tables(0)
                datasrc.Tables.Add(dt.Copy())
                whdgConfigurarFiltrosUno.DataSource = datasrc
                whdgConfigurarFiltrosUno.GridView.DataSource = whdgConfigurarFiltrosUno.DataSource
                CrearColumnasDT(whdgConfigurarFiltrosUno, dt)
                whdgConfigurarFiltrosUno.DataBind()
                ConfigurarGrid(whdgConfigurarFiltrosUno)
            Else
                whdgConfigurarFiltrosTodos.Rows.Clear()
                whdgConfigurarFiltrosTodos.Columns.Clear()
                whdgConfigurarFiltrosTodos.GridView.Columns.Clear()
                whdgConfigurarFiltrosUno.Rows.Clear()
                whdgConfigurarFiltrosUno.Columns.Clear()
                whdgConfigurarFiltrosUno.GridView.Columns.Clear()
                whdgConfigurarFiltrosUno.GridView.ClearDataSource()

                Dim dt As DataTable
                Dim datasrc As New DataSet
                dt = cFiltro.LoadConfiguracionInstancia(Idioma).Tables(0)
                datasrc.Tables.Add(dt.Copy())
                whdgConfigurarFiltrosTodos.DataSource = datasrc
                whdgConfigurarFiltrosTodos.GridView.DataSource = whdgConfigurarFiltrosTodos.DataSource
                CrearColumnasDT(whdgConfigurarFiltrosTodos, dt)
                whdgConfigurarFiltrosTodos.DataBind()
                ConfigurarGrid(whdgConfigurarFiltrosTodos)
            End If

            'Campos Generales
            CargaCamposGenerales(cFiltro.IDFiltroUsuario)
        Else
            phFiltrosDisponiblesTodos.Visible = True
            If Request("Donde") = "VisorCert" Then
                CargarFiltrosDisponibles(TiposDeDatos.TipoDeSolicitud.Certificado)
            Else
                CargarFiltrosDisponibles(TiposDeDatos.TipoDeSolicitud.NoConformidad)
            End If
            Session("QAIdFiltro") = ""
            Session("QAIdFiltroUsu") = ""
            Session("QANombreFiltro") = ""
        End If

        LimpiarBusquedaAvanzada()

        Dim sURL As String = String.Empty
        If Request("IdFiltroUsu") <> "" Then
            sURL = "?FiltroUsuarioIdAnt=" & Session("QAIdFiltroUsu") & "&NombreTablaAnt=" & Session("QANombreFiltro") & "&FiltroIdAnt=" & Session("QAIdFiltro")
        End If

        If Request("Donde") = "VisorCert" Then
            FSNPageHeader.OnClientClickVolver = "window.location='" & ConfigurationManager.AppSettings("rutaQA2008") & "Certificados/Certificados.aspx" & sURL & "';return false;"
        Else
            FSNPageHeader.OnClientClickVolver = "window.location='" & ConfigurationManager.AppSettings("rutaQA2008") & "NoConformidades/NoConformidades.aspx" & sURL & "';return false;"
        End If

        Accion.Value = ""
        upTipoFormulario.Update()
    End Sub
    ''' <summary>
    ''' Carga los controles con el idioma del usuario.
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load. Tiempo máximo: 0sg.</remarks>
    Private Sub CargarIdiomasControles()
        FSNPageHeader.TituloCabecera = Textos(0)
        FSNPageHeader.TextoBotonVolver = Textos(1)
        RadioButtonList.Items(0).Text = Textos(53)  'Aplicar el filtro a todos los formularios
        RadioButtonList.Items(1).Text = Textos(2)  ' Formularios:
        lblFiltrosDisponibles.Text = Textos(2)
        lblNombreFiltro.Text = Textos(3) & ":"
        lblFiltroPorDefecto.Text = Textos(4) & ":" 'Filtro por defecto:

        btnEliminar.Text = Textos(5)
        btnAceptar.Text = Textos(6)
        btnCancelar.Text = Textos(7)

        lblParametrosUno.Text = Textos(8)
        lblParametrosTodos.Text = lblParametrosUno.Text

        lblCabeceraCamposUno.Text = Textos(9) 'Seleccione los campos a visualizar en el filtro
        lblCabeceraCamposTodos.Text = lblCabeceraCamposUno.Text
        If Request("Donde") = "VisorCert" Then
            lblSeleccionarCampos.Text = Textos(72) & ":" 'Seleccione los campos de la no conformidad a visualizar en el filtro
        Else
            lblSeleccionarCampos.Text = Textos(71) & ":" 'Seleccione los campos de la no conformidad a visualizar en el filtro
        End If

        lblDatosGeneralesUno.Text = Textos(11) & ":" 'Seleccione los datos generales a visualizar en el filtro
        lblDatosGeneralesTodos.Text = lblDatosGeneralesUno.Text

        lblMensaje1Uno.Text = Textos(16)
        lblMensaje1Todos.Text = lblMensaje1Uno.Text

        m_sTextoCargando = Textos(36)
        FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/noconformidad.png"
        FSNPageHeader.VisibleBotonVolver = True
        reqValNombre.ErrorMessage = Textos(39) 'Introduzca el nombre del filtro

        If Request("Donde") = "VisorCert" Then
            FSNPageHeader.TituloCabecera = Textos(52)
            FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/certificado.png"
        End If
    End Sub
    ''' <summary>
    ''' Proceso que se encarga de actualizar la grid inferior. 
    ''' </summary>
    ''' <remarks>Llamada desde:ConfigurarFiltrosQA_LoadComplete; Tiempo máximo:0,4seg.</remarks>
    Private Sub ActualizarGrid()
        Dim index, indexCampos As Integer
        Dim chkCampoVisible As CheckBox
        Dim txtCampoNombre As TextBox
        Dim key As String

        If RadioButtonList.Items(1).Selected Then
            RepeaterDatosGeneral = RepeaterDatosGeneralesUno
            sTextoCGVisible = "CampoGeneral_VisibleUno"
            sTextoCGNombre = "CampoGeneral_NombrePersonalizadoUno"
            sTextoCGID = "CampoGeneralIDUno"
            sTextoCGLbl = "CampoGeneral_lblCampoGeneralUno"

            Grid = whdgConfigurarFiltrosUno

            NCBusqueda = BusquedaNCUno
            CertBusqueda = BusquedaCertUno
        Else
            RepeaterDatosGeneral = RepeaterDatosGeneralesTodos
            sTextoCGVisible = "CampoGeneral_VisibleTodos"
            sTextoCGNombre = "CampoGeneral_NombrePersonalizadoTodos"
            sTextoCGID = "CampoGeneralIDTodos"
            sTextoCGLbl = "CampoGeneral_lblCampoGeneralTodos"

            Grid = whdgConfigurarFiltrosTodos

            NCBusqueda = BusquedaNCTodos
            CertBusqueda = BusquedaCertUno
        End If

        'Recorrer CamposGenerales
        For index = 0 To RepeaterDatosGeneral.Items.Count - 1
            chkCampoVisible = CType(RepeaterDatosGeneral.Items(index).FindControl(sTextoCGVisible), CheckBox)
            txtCampoNombre = CType(RepeaterDatosGeneral.Items(index).FindControl(sTextoCGNombre), TextBox)
            key = chkCampoVisible.Attributes(sTextoCGID)
            'Si hay que mostrar el campo le tendre que quitar la clase hiddenelement
            With Grid.GridView.Columns(key)
                .Hidden = Not chkCampoVisible.Checked
                If chkCampoVisible.Checked = True Then
                    .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                    .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                End If
            End With
            If txtCampoNombre.Text <> "" Then
                Grid.GridView.Columns(key).Header.Text = txtCampoNombre.Text
            Else
                Grid.GridView.Columns(key).Header.Text = CType(RepeaterDatosGeneral.Items(index).FindControl(sTextoCGLbl), Label).Text
            End If
        Next

        Dim rpCampos As Repeater
        Dim hid As HiddenField
        'Recorrer CamposFormulario
        For index = 0 To rpGrupos.Items.Count - 1
            rpCampos = (CType(rpGrupos.Items(index).FindControl("rpCampos"), Repeater))
            For indexCampos = 0 To rpCampos.Items.Count - 1
                chkCampoVisible = CType(rpCampos.Items(indexCampos).FindControl("CampoFormulario_Visible"), CheckBox)
                txtCampoNombre = CType(rpCampos.Items(indexCampos).FindControl("CampoFormulario_NombrePersonalizado"), TextBox)
                hid = CType(rpCampos.Items(indexCampos).FindControl("CampoFormularioID"), HiddenField)
                key = hid.Value
                With Grid.GridView.Columns("C_" + key)
                    .Hidden = Not chkCampoVisible.Checked
                    If chkCampoVisible.Checked = True Then
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End If
                End With
                If txtCampoNombre.Text <> "" Then
                    Grid.GridView.Columns("C_" + key).Header.Text = txtCampoNombre.Text
                Else
                    Grid.GridView.Columns("C_" + key).Header.Text = CType(rpCampos.Items(indexCampos).FindControl("CampoFormulario_lblCampoGeneral"), Label).Text
                End If
            Next
        Next

        If RadioButtonList.Items(1).Selected Then
            upGridUno.Update()
        Else
            upGridTodos.Update()
        End If
    End Sub
    ''' <summary>
    ''' Limpia el los controles de la busqueda avanzada.
    ''' </summary>
    ''' <remarks>Llamada desde:InicializarElementos; Tiempo máximo:0seg.</remarks>
    Private Sub LimpiarBusquedaAvanzada()
        LimpiarBusquedaAvanzadaUno()
        LimpiarBusquedaAvanzadaTodos()

        upCriteriosBusquedaUno.Update()
        upCriteriosBusquedaTodos.Update()
    End Sub
    ''' <summary>
    ''' Limpia el los controles de la busqueda avanzada.
    ''' </summary>
    ''' <remarks>Llamada desde:InicializarElementos; Tiempo máximo:0seg.</remarks>
    Private Sub LimpiarBusquedaAvanzadaUno()
        If Request("Donde") = "VisorCert" Then
            BusquedaCertUno.Materiales = ""
            BusquedaCertUno.TipoSolicitudes = ""
            BusquedaCertUno.VerDadosBaja = False

            BusquedaCertUno.MostrarProveedores = 3
            BusquedaCertUno.ProveedorTexto = ""
            BusquedaCertUno.ProveedorHidden = ""
            BusquedaCertUno.ProveedorDeBaja = 0

            BusquedaCertUno.Estados = ""
            BusquedaCertUno.Peticionario = ""

            BusquedaCertUno.FechasDe = 1
            BusquedaCertUno.FechaDesde = Nothing
            BusquedaCertUno.FechaHasta = Nothing

            BusquedaNCUno.Visible = False
        Else
            BusquedaNCUno.Tipo = 0
            BusquedaNCUno.SubTipo = ""
            BusquedaNCUno.Titulo = ""
            BusquedaNCUno.Proveedor = ""
            BusquedaNCUno.ProveedorDeBaja = False
            BusquedaNCUno.UnidadNegocio = ""
            BusquedaNCUno.MaterialHidden = ""
            BusquedaNCUno.MaterialTexto = ""
            BusquedaNCUno.ArticuloHidden = ""
            BusquedaNCUno.ArticuloTexto = ""
            BusquedaNCUno.FechaAlta = True
            BusquedaNCUno.FechaActualizacion = False
            BusquedaNCUno.FechaLimiteResolucion = False
            BusquedaNCUno.FechaCierre = False
            BusquedaNCUno.FechaDesde = Nothing
            BusquedaNCUno.FechaHasta = Nothing

            BusquedaNCUno.EstadoNCAbiertas = ""
            BusquedaNCUno.Peticionario = ""
            BusquedaNCUno.Revisor = ""

            BusquedaCertUno.Visible = False
        End If
    End Sub
    ''' <summary>
    ''' Limpia el los controles de la busqueda avanzada.
    ''' </summary>
    ''' <remarks>Llamada desde:InicializarElementos; Tiempo máximo:0seg.</remarks>
    Private Sub LimpiarBusquedaAvanzadaTodos()
        If Request("Donde") = "VisorCert" Then
            BusquedaCertTodos.Materiales = ""
            BusquedaCertTodos.TipoSolicitudes = ""
            BusquedaCertTodos.VerDadosBaja = False

            BusquedaCertTodos.MostrarProveedores = 3
            BusquedaCertTodos.ProveedorTexto = ""
            BusquedaCertTodos.ProveedorHidden = ""
            BusquedaCertTodos.ProveedorDeBaja = 0

            BusquedaCertTodos.Estados = ""
            BusquedaCertTodos.Peticionario = ""

            BusquedaCertTodos.FechasDe = 1
            BusquedaCertTodos.FechaDesde = Nothing
            BusquedaCertTodos.FechaHasta = Nothing

            BusquedaNCTodos.Visible = False
        Else
            BusquedaNCTodos.Tipo = 0
            BusquedaNCTodos.SubTipo = ""
            BusquedaNCTodos.Titulo = ""
            BusquedaNCTodos.Proveedor = ""
            BusquedaNCTodos.ProveedorDeBaja = False
            BusquedaNCTodos.UnidadNegocio = ""
            BusquedaNCTodos.MaterialHidden = ""
            BusquedaNCTodos.MaterialTexto = ""
            BusquedaNCTodos.ArticuloHidden = ""
            BusquedaNCTodos.ArticuloTexto = ""
            BusquedaNCTodos.FechaAlta = True
            BusquedaNCTodos.FechaActualizacion = False
            BusquedaNCTodos.FechaLimiteResolucion = False
            BusquedaNCTodos.FechaCierre = False
            BusquedaNCTodos.FechaDesde = Nothing
            BusquedaNCTodos.FechaHasta = Nothing

            BusquedaNCTodos.EstadoNCAbiertas = ""
            BusquedaNCTodos.Peticionario = ""
            BusquedaNCTodos.Revisor = ""

            BusquedaCertTodos.Visible = False
        End If
    End Sub
    ''' <summary>
    ''' Cargar en el módulo de busqueda avanzada los datos configurados en el filtro.
    ''' </summary>
    ''' <remarks>Llamada desde: Page_PreRender. Tiempo máximo: 1 sg.</remarks>
    Private Sub CargarBusquedaAvanzadaNC()
        Dim sMat As String
        Dim sFechaDesde As String = ""
        Dim sFechaHasta As String = ""

        cFiltro.TipoSol = TiposDeDatos.TipoDeSolicitud.NoConformidad
        cFiltro.LoadConfiguracionCamposBusqueda(Idioma)

        NCBusqueda.Filtro = cFiltro.IDFiltro
        If Not cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA") Is Nothing Then
            If cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows.Count > 0 Then
                'NCBusqueda.Identificador = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("IDENTIFICADOR")
                NCBusqueda.Tipo = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("TIPO")
                NCBusqueda.SubTipo = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("SUBTIPO")
                NCBusqueda.Titulo = DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("TITULO"))
                If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDORBAJA")) Then
                    NCBusqueda.ProveedorDeBaja = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDORBAJA")
                    If NCBusqueda.ProveedorDeBaja Then NCBusqueda.CambioFiltro_ProveedorBaja() 'Se q no estan cargados los proveedores de baja. Cargarlos.
                End If
                NCBusqueda.Proveedor = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDOR")
                NCBusqueda.UnidadNegocio = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("UNIDADNEGOCIO")
                sMat = DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("GMN1")) & "-" & DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("GMN2")) & "-" & DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("GMN3")) & "-" & DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("GMN4"))
                NCBusqueda.MaterialHidden = sMat
                NCBusqueda.MaterialTexto = DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("MATDEN"))

                NCBusqueda.ArticuloHidden = DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("ARTICULO"))
                NCBusqueda.ArticuloTexto = DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("ARTDEN"))
                'Fecha Alta
                If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_ALTA")) Then
                    NCBusqueda.FechaAlta = True
                    sFechaDesde = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_ALTA")
                End If
                    If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_ALTA")) Then
                        NCBusqueda.FechaAlta = True
                        sFechaHasta = DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_ALTA"))
                    End If
                    'Fecha Actualizacion
                    If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_ACTUALIZACION")) Then
                        NCBusqueda.FechaActualizacion = True
                        sFechaDesde = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_ACTUALIZACION")
                    End If
                    If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_ACTUALIZACION")) Then
                        NCBusqueda.FechaActualizacion = True
                        sFechaHasta = DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_ACTUALIZACION"))
                    End If

                    If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_LIMITERESOLUCION")) Then
                        NCBusqueda.FechaLimiteResolucion = True
                        sFechaDesde = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_LIMITERESOLUCION")
                    End If
                    If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_LIMITERESOLUCION")) Then
                        NCBusqueda.FechaLimiteResolucion = True
                        sFechaHasta = DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_LIMITERESOLUCION"))
                    End If

                    If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_CIERRE")) Then
                        NCBusqueda.FechaCierre = True
                        sFechaDesde = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_CIERRE")
                    End If
                    If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_CIERRE")) Then
                        NCBusqueda.FechaCierre = True
                        sFechaHasta = DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_CIERRE"))
                    End If

                    If sFechaDesde <> "" Then
                        NCBusqueda.FechaDesde = sFechaDesde
                    End If
                    If sFechaHasta <> "" Then
                        NCBusqueda.FechaHasta = sFechaHasta
                    End If

                    NCBusqueda.EstadoNCAbiertas = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("ESTADONCABIERTAS")
                    NCBusqueda.Peticionario = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PETICIONARIO")
                    NCBusqueda.Revisor = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("REVISOR")
                End If
            End If
    End Sub
    ''' <summary>
    ''' Inicializa los arrays con los campos generales.
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load. Tiempo máximo: 0sg.</remarks>
    Private Sub InicializarArraysCamposGeneralesNC()
        'NOMBRE DE LAS COLUMNAS DE LA GRID

        ReDim arrDatosGenerales(22)
        arrDatosGenerales(1) = "ID"
        arrDatosGenerales(2) = "TIPO"
        arrDatosGenerales(3) = "TITULO"
        arrDatosGenerales(4) = "CODPROVE"
        arrDatosGenerales(5) = "DENPROVE"
        arrDatosGenerales(6) = "UNQA"
        arrDatosGenerales(7) = "PET"
        arrDatosGenerales(8) = "REVISOR"
        arrDatosGenerales(9) = "FECALTA"
        arrDatosGenerales(10) = "FEC_ANULACION"
        arrDatosGenerales(11) = "FEC_LIM_RESOL"
        arrDatosGenerales(12) = "FEC_EMISION"
        arrDatosGenerales(13) = "FEC_RESPUESTA"
        arrDatosGenerales(14) = "FECACT"
        arrDatosGenerales(15) = "FEC_CIERRE"
        arrDatosGenerales(16) = "COMENT"
        arrDatosGenerales(17) = "COMENT_ALTA"
        arrDatosGenerales(18) = "COMENT_CIERRE"
        arrDatosGenerales(19) = "COMENT_REVISOR"
        arrDatosGenerales(20) = "COMENT_ANULACION"
        arrDatosGenerales(21) = "ESTADO_ACCIONES"
        arrDatosGenerales(22) = "PROVE_ERP"

        ReDim arrDatosGeneralesDen(22)
        For i = 1 To 9
            arrDatosGeneralesDen(i) = Textos(i + 17)
        Next
        arrDatosGeneralesDen(10) = Textos(37) '"Fecha anulación"
        For i = 11 To 19
            arrDatosGeneralesDen(i) = Textos(i + 16)
        Next
        arrDatosGeneralesDen(20) = Textos(38) '"Comentario anulación"
        arrDatosGeneralesDen(21) = Textos(69) '"Estado de las acciones"
        arrDatosGeneralesDen(22) = Textos(70) 'Proveedor ERP

        BusquedaCertTodos.Carga = Request("Donde")
        BusquedaCertUno.Carga = BusquedaCertTodos.Carga

        BusquedaNCTodos.Carga = BusquedaCertTodos.Carga
        BusquedaNCUno.Carga = BusquedaCertTodos.Carga
    End Sub
    ''' <summary>
    ''' Carga los repeter's con los campos generales para Certificados o No Conformidades
    ''' </summary>
    ''' <param name="lIdFiltroUsuario">Id del usuario</param>
    ''' <remarks>Llamada desde:Page_Load. Tiempo máximo: 0,5sg.</remarks>
    Private Sub CargaCamposGenerales(ByVal lIdFiltroUsuario As Long)
        Dim ds As DataSet
        Dim dt As DataTable
        Dim dtNewRow As DataRow

        ds = New DataSet

        dt = ds.Tables.Add("CAMPOS")

        dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
        dt.Columns.Add("CAMPO_ID", System.Type.GetType("System.String"))
        dt.Columns.Add("NOMBRE", System.Type.GetType("System.String"))

        Dim LongArrGen As Integer = UBound(arrDatosGenerales)
        For i = 1 To LongArrGen
            Dim bAdd As Boolean = True
            If arrDatosGenerales(i) = "PROVE_ERP" Then bAdd = Acceso.gbProveERPVisibleNC
            If bAdd Then
                dtNewRow = dt.NewRow
                dtNewRow.Item("ID") = i
                dtNewRow.Item("CAMPO_ID") = arrDatosGenerales(i)
                dtNewRow.Item("NOMBRE") = arrDatosGeneralesDen(i)
                dt.Rows.Add(dtNewRow)
            End If
        Next

        RepeaterDatosGeneral.DataSource = ds
        RepeaterDatosGeneral.DataBind()

        Dim chkCampoGeneral As CheckBox
        Dim txtNombre As TextBox
        For i = 0 To RepeaterDatosGeneral.Items.Count - 1
            chkCampoGeneral = CType(RepeaterDatosGeneral.Items(i).FindControl(sTextoCGVisible), CheckBox)
            chkCampoGeneral.Attributes.Add(sTextoCGID, arrDatosGenerales(i + 1))
            If lIdFiltroUsuario = 0 Then
                If Request("Donde") = "VisorCert" Then
                    'Estas columnas van en uwgConfigurarFiltros_InitializeLayout
                    If (Request("IdFiltroUsu") = "0") Then
                        If (i + 1 = ConfiguracionFiltrosQA_Cert.DenominacionProveedor) Or (i + 1 = ConfiguracionFiltrosQA_Cert.MatQA) _
                        Or (i + 1 = ConfiguracionFiltrosQA_Cert.Tipo) Or (i + 1 = ConfiguracionFiltrosQA_Cert.Estado) _
                        Or (i + 1 = ConfiguracionFiltrosQA_Cert.FechaSolicitud) Or (i + 1 = ConfiguracionFiltrosQA_Cert.FechaLimiteCumpl) _
                        Or (i + 1 = ConfiguracionFiltrosQA_Cert.Publicado) Or (i + 1 = ConfiguracionFiltrosQA_Cert.FechaDesPublicacion) _
                        Or (i + 1 = ConfiguracionFiltrosQA_Cert.CodigoPeticionario) Or (i + 1 = ConfiguracionFiltrosQA_Cert.FechaRespuestaProveedor) _
                        Or (i + 1 = ConfiguracionFiltrosQA_Cert.FechaExpiracion) Or (i + 1 = ConfiguracionFiltrosQA_Cert.Obligatorio) _
                        Or (i + 1 = ConfiguracionFiltrosQA_Cert.FechaCumplimentacion) Then
                            chkCampoGeneral.Checked = True
                        End If
                    Else
                        If (i + 1 = ConfiguracionFiltrosQA_Cert.DenominacionProveedor) Or (i + 1 = ConfiguracionFiltrosQA_Cert.MatQA) _
                        Or (i + 1 = ConfiguracionFiltrosQA_Cert.Tipo) Or (i + 1 = ConfiguracionFiltrosQA_Cert.Estado) _
                        Or (i + 1 = ConfiguracionFiltrosQA_Cert.FechaSolicitud) Or (i + 1 = ConfiguracionFiltrosQA_Cert.FechaLimiteCumpl) _
                        Or (i + 1 = ConfiguracionFiltrosQA_Cert.Publicado) Or (i + 1 = ConfiguracionFiltrosQA_Cert.FechaDesPublicacion) _
                        Or (i + 1 = ConfiguracionFiltrosQA_Cert.FechaExpiracion) Or (i + 1 = ConfiguracionFiltrosQA_Cert.FechaCumplimentacion) Then
                            chkCampoGeneral.Checked = True
                        End If
                    End If
                Else
                    If (i + 1 = ConfiguracionFiltrosQA_NC.Identificador) Or (i + 1 = ConfiguracionFiltrosQA_NC.Tipo) _
                    Or (i + 1 = ConfiguracionFiltrosQA_NC.CodigoProveedor) Or (i + 1 = ConfiguracionFiltrosQA_NC.DenominacionProveedor) _
                    Or (i + 1 = ConfiguracionFiltrosQA_NC.Peticionario) Or (i + 1 = ConfiguracionFiltrosQA_NC.FechaAlta) Then
                        chkCampoGeneral.Checked = True
                    End If
                End If
            ElseIf dsCamposGenerales.Tables(0).Rows.Count > 0 Then
                txtNombre = CType(RepeaterDatosGeneral.Items(i).FindControl(sTextoCGNombre), TextBox)
                If i > dsCamposGenerales.Tables(0).Rows.Count - 1 Then
                    chkCampoGeneral.Checked = False
                    txtNombre.Text = ""
                Else
                    chkCampoGeneral.Checked = dsCamposGenerales.Tables(0).Rows(i).Item("VISIBLE_GRID")
                    If chkCampoGeneral.Checked Then
                        If Not dsCamposGenerales.Tables(0).Rows(i).IsNull("NOMBRE_GRID") Then
                            txtNombre.Text = DBNullToStr(dsCamposGenerales.Tables(0).Rows(i).Item("NOMBRE_GRID"))
                        Else
                            txtNombre.Text = ""
                        End If
                    Else
                        txtNombre.Text = ""
                    End If
                End If
            End If
        Next
    End Sub
#Region "Grids"
    Private Sub CrearColumnasDT(ByVal migrid As Infragistics.Web.UI.GridControls.WebHierarchicalDataGrid, ByVal dt As DataTable)
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim nombre As String

        For i As Integer = 0 To dt.Columns.Count - 1
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            nombre = dt.Columns(i).ToString
            With campoGrid
                .Key = nombre
                .DataFieldName = nombre
                .Header.Text = nombre
                .Hidden = True
                .Header.CssClass = "hiddenElement headerNoWrap igg_FullstepHeaderCaption"
                .CssClass = "hiddenElement SinSalto"
            End With
            migrid.GridView.Columns.Insert(i, campoGrid)
            migrid.GridView.Columns(i).VisibleIndex = i
        Next
    End Sub
    ''' <summary>
    ''' Carga el gridview con los campos generales y, en caso de q no sea para todos los formularios, los campos del formulario seleccionado.
    ''' Si se trata de un filtro nuevo carga los campos con el tamaño por defecto y todos ocultos menos los generales por defecto.
    ''' Si es un filtro ya configurado pone a visible y el tamaño de los configurado.
    ''' </summary>
    ''' <remarks>Llamada desde: Al cargarse el gridview. Tiempo máximo: 1 sg.</remarks>
    Private Sub ConfigurarGrid(ByVal migrid As Infragistics.Web.UI.GridControls.WebHierarchicalDataGrid)
        For i = 0 To migrid.GridView.Columns.Count - 1
            If migrid.GridView.Columns(i).Type.FullName = "System.DateTime" Then
                migrid.GridView.Columns(i).FormatValue(FSNUser.DateFormat.ShortDatePattern)
            End If
        Next

        If Session("QAIdFiltroUsu") = "" Then
            'Campos generales
            For i = 1 To UBound(arrDatosGenerales)
                migrid.GridView.Columns(arrDatosGenerales(i)).Header.Text = arrDatosGeneralesDen(i)
            Next

            If Request("Donde") = "VisorCert" Then
                'Estas columnas van en CargaCamposGenerales
                'Si hay que mostrar el campo le tendre que quitar la clase hiddenelement
                With migrid.GridView
                    With .Columns("DENPROVE")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("MATQA")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("TIPO")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("ESTADO")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("FEC_SOL")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("FEC_LIM_CUMP")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("PUBL")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("VALIDADO")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("FEC_DESPUBL")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("FEC_CUMPLIM")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                End With

                If RadioButtonList.Items(0).Selected Then
                    migrid.GridView.Columns("FEC_EXPIRACION").Hidden = False
                    migrid.GridView.Columns("FEC_EXPIRACION").CssClass = migrid.GridView.Columns("FEC_EXPIRACION").CssClass.Replace("hiddenElement", String.Empty)
                    migrid.GridView.Columns("FEC_EXPIRACION").Header.CssClass = migrid.GridView.Columns("FEC_EXPIRACION").Header.CssClass.Replace("hiddenElement", String.Empty)
                End If
            Else
                With migrid.GridView
                    With .Columns("ID")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("TIPO")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("CODPROVE")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("DENPROVE")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("PET")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("FECALTA")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                End With
            End If
        ElseIf Session("QAIdFiltroUsu") = "0" Then
            'Campos generales
            For i = 1 To UBound(arrDatosGenerales)
                migrid.GridView.Columns(arrDatosGenerales(i)).Header.Text = arrDatosGeneralesDen(i)
            Next

            If Request("Donde") = "VisorCert" Then
                'Estas columnas van en CargaCamposGenerales
                With migrid.GridView
                    With .Columns("DENPROVE")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("MATQA")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("TIPO")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("ESTADO")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("FEC_SOL")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("FEC_LIM_CUMP")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("PUBL")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("VALIDADO")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("FEC_DESPUBL")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("FEC_CUMPLIM")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("FEC_RESP")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                    With .Columns("FEC_EXPIRACION")
                        .Hidden = False
                        .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                        .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                    End With
                End With
            End If
        Else
            'Configuracion de la grid para cuando quiera modificar un filtro existente.
            cFiltro.IDFiltro = strToDbl(Session(IIf(Request("Donde") = "VisorCert", "QAFiltroIdVCert", "QAFiltroId")))
            cFiltro.IDFiltroUsuario = Session(IIf(Request("Donde") = "VisorCert", "QAFiltroUsuarioIdVCert", "QAFiltroUsuarioId"))
            cFiltro.Persona = FSNUser.CodPersona
            cFiltro.TipoSol = IIf(Request("Donde") = "VisorCert", TiposDeDatos.TipoDeSolicitud.Certificado, TiposDeDatos.TipoDeSolicitud.NoConformidad)

            Dim dsCamposYCamposGenerales As DataSet = Nothing
            dsCamposYCamposGenerales = cFiltro.LoadCamposyCamposGenerales(Idioma)

            'Campos Generales
            For Each oRow As DataRow In dsCamposYCamposGenerales.Tables(0).Rows
                If oRow("CAMPO_GENERAL") = 1 Then
                    If (cFiltro.TipoSol = TiposDeDatos.TipoDeSolicitud.NoConformidad) Or
                        (cFiltro.TipoSol = TiposDeDatos.TipoDeSolicitud.Certificado) And (oRow("ID") <> ConfiguracionFiltrosQA_Cert.Provisional Or (oRow("ID") = ConfiguracionFiltrosQA_Cert.Provisional And Me.Acceso.gbPotAValProvisionalSelProve)) Then  'El dato provisional se mostrará en función del parámetro de config.

                        migrid.GridView.Columns(arrDatosGenerales(oRow("ID"))).Header.Text = If(oRow.IsNull("NOMBRE_GRID"), arrDatosGeneralesDen(oRow("ID")), oRow("NOMBRE_GRID"))

                        migrid.GridView.Columns(arrDatosGenerales(oRow("ID"))).VisibleIndex = oRow("POSICION_GRID")
                        If (CType(oRow("TAMANYO_GRID"), Long) = 0.0) Then
                            migrid.GridView.Columns(arrDatosGenerales(oRow("ID"))).Width = Unit.Pixel(100)
                        Else
                            migrid.GridView.Columns(arrDatosGenerales(oRow("ID"))).Width = CLng(oRow("TAMANYO_GRID"))
                        End If
                        'Si hay que mostrar el campo le tendre que quitar la clase hiddenelement
                        With migrid.GridView.Columns(arrDatosGenerales(oRow("ID")))
                            .Hidden = Not CBool(oRow("VISIBLE_GRID"))
                            If CBool(oRow("VISIBLE_GRID")) = True Then
                                .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                                .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                            End If
                        End With
                    End If
                Else
                    If migrid.GridView.Columns("C_" & oRow("ID").ToString).Index > -1 Then
                        If DBNullToStr(oRow("NOMBRE_GRID")) <> "" Then
                            migrid.GridView.Columns("C_" & oRow("ID").ToString).Header.Text = oRow("NOMBRE_GRID")
                        Else
                            migrid.GridView.Columns("C_" & oRow("ID").ToString).Header.Text = oRow("NOMBRE")
                        End If
                        migrid.GridView.Columns("C_" & oRow("ID").ToString).VisibleIndex = oRow("POSICION_GRID")
                        If (CType(oRow("TAMANYO_GRID"), Long) = 0.0) Then
                            migrid.GridView.Columns("C_" & oRow("ID").ToString).Width = Unit.Pixel(100)
                        Else
                            migrid.GridView.Columns("C_" & oRow("ID").ToString).Width = CLng(oRow("TAMANYO_GRID"))
                        End If
                        'Si hay que mostrar el campo le tendre que quitar la clase hiddenelement
                        With migrid.GridView.Columns("C_" & oRow("ID").ToString)
                            .Hidden = Not CBool(oRow("VISIBLE_GRID"))
                            If CBool(oRow("VISIBLE_GRID")) = True Then
                                .CssClass = .CssClass.Replace("hiddenElement", String.Empty)
                                .Header.CssClass = .Header.CssClass.Replace("hiddenElement", String.Empty)
                            End If
                        End With
                    End If
                End If
            Next
        End If
    End Sub
    ''' <summary>
    ''' Evento que salta al cargarse cada linea de la grid. Si se ha llegado desde el visor de certificados hay datos por cargar.
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde=sistema;Tiempo máximo=0seg.</remarks>
    Private Sub whdgConfigurarFiltrosUno_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgConfigurarFiltrosUno.InitializeRow
        If Request("Donde") = "VisorCert" Then
            whdgConfigurarFiltros_InitializeRow(sender, e, whdgConfigurarFiltrosUno)
        Else
            whdgConfigurarFiltrosNC_InitializeRow(sender, e, whdgConfigurarFiltrosUno)
        End If
    End Sub
    ''' <summary>
    ''' Evento que salta al cargarse cada linea de la grid. Si se ha llegado desde el visor de certificados hay datos por cargar.
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde=sistema;Tiempo máximo=0seg.</remarks>
    Private Sub whdgConfigurarFiltrosTodos_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgConfigurarFiltrosTodos.InitializeRow
        If Request("Donde") = "VisorCert" Then
            whdgConfigurarFiltros_InitializeRow(sender, e, whdgConfigurarFiltrosTodos)
        Else
            whdgConfigurarFiltrosNC_InitializeRow(sender, e, whdgConfigurarFiltrosTodos)
        End If
    End Sub
    ''' <summary>
    ''' Si se ha llegado desde el visor de certificados hay datos por cargar, concretamente el estado.
    ''' </summary>
    ''' <param name="sender">grid</param>
    ''' <param name="e">row de grid</param>        
    ''' <remarks>Llamada desde=whdgConfigurarFiltrosUno_InitializeRow            whdgConfigurarFiltrosTodos_InitializeRow;Tiempo máximo=0seg.</remarks>
    Private Sub whdgConfigurarFiltros_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs, ByVal migrid As Infragistics.Web.UI.GridControls.WebHierarchicalDataGrid)
        Dim iEstado As Integer = 2
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltroQA
        'Estado
        If e.Row.Items.FindItemByKey("CERTIFICADO").Value = 0 Then
            iEstado = 2
        ElseIf Not IsDBNull(e.Row.Items.FindItemByKey("FEC_EXPIRACION").Value) Then
            If e.Row.Items.FindItemByKey("DIFEXP").Value > (e.Row.Items.FindItemByKey("FSQA_PROX_EXPIRAR").Value + 1) Then
                iEstado = 3
            ElseIf e.Row.Items.FindItemByKey("DIFEXP").Value >= 0 _
            AndAlso e.Row.Items.FindItemByKey("DIFEXP").Value <= (e.Row.Items.FindItemByKey("FSQA_PROX_EXPIRAR").Value + 1) Then
                iEstado = 4
            Else
                iEstado = 5
            End If
        End If
        e.Row.Items.FindItemByKey("ESTADO").Text = Textos(55 + iEstado)

        For i = 0 To migrid.GridView.Columns.Count - 1
            If Left(migrid.GridView.Columns(i).Key.ToString, 2) = "C_" Then
                Dim Spli() As String = migrid.GridView.Columns(i).Key.ToString.Split("_")

                If Not IsNothing(e.Row.Items.FindItemByKey(migrid.GridView.Columns(i).Key).Value) Then
                    If migrid.GridView.Columns(i).Type.FullName = "System.Byte" Then
                        If Not IsNothing(e.Row.Items.FindItemByKey(migrid.GridView.Columns(i).Key).Value) Then
                            e.Row.Items.FindItemByKey(migrid.GridView.Columns(i).Key).Text = Textos(61)
                        Else
                            e.Row.Items.FindItemByKey(migrid.GridView.Columns(i).Key).Text = Textos(62)
                        End If
                    ElseIf migrid.GridView.Columns(i).Type.FullName = "System.Double" Then
                        If IsNumeric(e.Row.Items.FindItemByKey(migrid.GridView.Columns(i).Key).Value) Then
                            'Para campos numericos tipo telefono hay q quitar los decimales. Para los otros campos numericos 
                            'quitarlos o no carece de importancia (si hay decimales nunca se quitan).
                            e.Row.Items.FindItemByKey(migrid.GridView.Columns(i).Key).Text = QuitarDecimalSiTodoCeros(FSNLibrary.FormatNumber(e.Row.Items.FindItemByKey(migrid.GridView.Columns(i).Key).Value, FSNUser.NumberFormat))
                        End If
                    End If
                End If
            End If
            e.Row.Items.Item(i).Tooltip = e.Row.Items.Item(i).Text
        Next
    End Sub
    ''' <summary>
    ''' Si se ha llegado desde el visor de No conformidades hay datos por cargar
    ''' </summary>
    ''' <param name="sender">grid</param>
    ''' <param name="e">row de grid</param>        
    ''' <remarks>Llamada desde=whdgConfigurarFiltrosUno_InitializeRow /whdgConfigurarFiltrosTodos_InitializeRow;Tiempo mÃ¡ximo=0seg.</remarks>
    Private Sub whdgConfigurarFiltrosNC_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs, ByVal migrid As Infragistics.Web.UI.GridControls.WebHierarchicalDataGrid)
        Dim iEstado As Integer = 2
        'Comentario
        e.Row.Items.FindItemByKey(arrDatosGenerales(ConfiguracionFiltrosQA_NC.Comentarios)).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/coment.gif' style='text-align:center;'/>"

        For i = 0 To migrid.GridView.Columns.Count - 1
            If Left(migrid.GridView.Columns(i).Key.ToString, 2) = "C_" Then
                Dim Spli() As String = migrid.GridView.Columns(i).Key.ToString.Split("_")

                If Not IsNothing(e.Row.Items.FindItemByKey(migrid.GridView.Columns(i).Key).Value) Then
                    If migrid.GridView.Columns(i).Type.FullName = "System.Double" Then
                        If IsNumeric(e.Row.Items.FindItemByKey(migrid.GridView.Columns(i).Key).Value) Then
                            'Para campos numericos tipo telefono hay q quitar los decimales. Para los otros campos numericos 
                            'quitarlos o no carece de importancia (si hay decimales nunca se quitan).
                            e.Row.Items.FindItemByKey(migrid.GridView.Columns(i).Key).Text = QuitarDecimalSiTodoCeros(FSNLibrary.FormatNumber(e.Row.Items.FindItemByKey(migrid.GridView.Columns(i).Key).Value, FSNUser.NumberFormat))
                        End If
                    End If
                End If
            End If
            e.Row.Items.Item(i).Tooltip = e.Row.Items.Item(i).Text
        Next
    End Sub
#End Region
#Region "VisorCert"
    ''' <summary>
    ''' Inicializa los arrays con los campos generales de un certificado.
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load. Tiempo máximo: 0sg.</remarks>
    Private Sub InicializarArraysCamposGeneralesCert()
        Dim iLongitud As Integer = IIf(Me.FSNServer.TipoAcceso.gbPotAValProvisionalSelProve, 22, 21)
        'NOMBRE DE LAS COLUMNAS DE LA GRID
        ReDim arrDatosGenerales(iLongitud)
        arrDatosGenerales(1) = "ID"
        arrDatosGenerales(2) = "CODPROVE"
        arrDatosGenerales(3) = "DENPROVE"
        arrDatosGenerales(4) = "REAL"
        arrDatosGenerales(5) = "BAJA"
        arrDatosGenerales(6) = "MATQA"
        arrDatosGenerales(7) = "TIPO"
        arrDatosGenerales(8) = "ESTADO"
        arrDatosGenerales(9) = "FEC_SOL"
        arrDatosGenerales(10) = "FEC_LIM_CUMP"
        arrDatosGenerales(11) = "FEC_RESP"
        arrDatosGenerales(12) = "PUBL"
        arrDatosGenerales(13) = "FEC_PUBL"
        arrDatosGenerales(14) = "FEC_DESPUBL"
        arrDatosGenerales(15) = "FEC_ACT"
        arrDatosGenerales(16) = "PETICIONARIO"
        arrDatosGenerales(17) = "MODO_SOLIC"
        arrDatosGenerales(18) = "FEC_EXPIRACION"
        arrDatosGenerales(19) = "OBLIGATORIO"
        arrDatosGenerales(20) = "FEC_CUMPLIM"
        arrDatosGenerales(21) = "VALIDADO"
        If FSNServer.TipoAcceso.gbPotAValProvisionalSelProve Then arrDatosGenerales(22) = "PROVISIONAL"

        ReDim arrDatosGeneralesDen(iLongitud)
        arrDatosGeneralesDen(1) = Textos(18)
        arrDatosGeneralesDen(2) = Textos(21)
        arrDatosGeneralesDen(3) = Textos(22)
        For i = 4 To 14
            arrDatosGeneralesDen(i) = Textos(i + 36)
        Next
        arrDatosGeneralesDen(15) = Textos(30)
        arrDatosGeneralesDen(16) = Textos(24)
        arrDatosGeneralesDen(17) = Textos(51)
        arrDatosGeneralesDen(18) = Textos(63)
        arrDatosGeneralesDen(19) = Textos(64)
        arrDatosGeneralesDen(20) = Textos(65)
        arrDatosGeneralesDen(21) = Textos(66)
        If FSNServer.TipoAcceso.gbPotAValProvisionalSelProve Then arrDatosGeneralesDen(22) = Textos(68)

        BusquedaCertTodos.Carga = Request("Donde")
        BusquedaCertUno.Carga = BusquedaCertTodos.Carga
        BusquedaNCTodos.Carga = BusquedaCertTodos.Carga
        BusquedaNCUno.Carga = BusquedaCertTodos.Carga
    End Sub
    ''' <summary>
    ''' Cargar en el módulo de busqueda avanzada los datos configurados en el filtro.
    ''' </summary>
    ''' <remarks>Llamada desde: Page_PreRender. Tiempo máximo: 1 sg.</remarks>
    Private Sub CargarBusquedaAvanzadaCert()
        Dim sFechaDesde As String = ""
        Dim sFechaHasta As String = ""

        cFiltro.IDFiltroUsuario = Request("IdFiltroUsu")
        cFiltro.TipoSol = TiposDeDatos.TipoDeSolicitud.Certificado

        cFiltro.LoadConfiguracionCamposBusqueda(Idioma)

        CertBusqueda.Filtro = cFiltro.IDFiltro
        If Not cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA") Is Nothing Then
            If cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows.Count > 0 Then
                CertBusqueda.Materiales = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("MATERIALES")
                CertBusqueda.TipoSolicitudes = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("TIPOSOL")
                CertBusqueda.VerDadosBaja = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("BAJA")

                CertBusqueda.MostrarProveedores = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("QUEMOSTRAR")
                If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDOR")) Then
                    CertBusqueda.ProveedorTexto = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDOR")
                End If
                CertBusqueda.ProveedorHidden = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HIDPROVEEDOR")
                CertBusqueda.ProveedorDeBaja = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDORBAJA")

                CertBusqueda.Estados = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("ESTADOS")

                CertBusqueda.Peticionario = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PETICIONARIO")

                CertBusqueda.FechasDe = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("QUEFECHA")
                If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_FECHA")) Then
                    CertBusqueda.FechaDesde = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_FECHA")
                End If
                If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_FECHA")) Then
                    CertBusqueda.FechaHasta = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_FECHA")
                End If
            End If
        End If
        cFiltro = Nothing
    End Sub
    ''' <summary>
    ''' Determina la posición en bbdd de una columna "campo general" 
    ''' </summary>
    ''' <param name="Key">Key de la columna "campo general" de la q se desea conocer el indice para bbdd</param>
    ''' <returns>Posición</returns>
    ''' <remarks>Llamada desde: btnAceptar_Click: Tiempño maximo:0,1</remarks>
    Private Function DamePosicionWhdgConfigurarFiltros(ByVal Key As String) As Integer
        Dim index As Integer
        index = -1

        If cFiltro.TipoSol = TiposDeDatos.TipoDeSolicitud.NoConformidad Then
            Select Case Key
                Case "ID"
                    index = ConfiguracionFiltrosQA_NC.Identificador
                Case "TIPO"
                    index = ConfiguracionFiltrosQA_NC.Tipo
                Case "TITULO"
                    index = ConfiguracionFiltrosQA_NC.Titulo
                Case "CODPROVE"
                    index = ConfiguracionFiltrosQA_NC.CodigoProveedor
                Case "DENPROVE"
                    index = ConfiguracionFiltrosQA_NC.DenominacionProveedor
                Case "UNQA"
                    index = ConfiguracionFiltrosQA_NC.UnidadNegocio
                Case "PET"
                    index = ConfiguracionFiltrosQA_NC.Peticionario
                Case "REVISOR"
                    index = ConfiguracionFiltrosQA_NC.Revisor
                Case "FECALTA"
                    index = ConfiguracionFiltrosQA_NC.FechaAlta
                Case "FEC_ANULACION"
                    index = ConfiguracionFiltrosQA_NC.FechaAnulacion
                Case "FEC_LIM_RESOL"
                    index = ConfiguracionFiltrosQA_NC.FechaLimiteResolucion
                Case "FEC_EMISION"
                    index = ConfiguracionFiltrosQA_NC.FechaEmision
                Case "FEC_RESPUESTA"
                    index = ConfiguracionFiltrosQA_NC.FechaRespuestaProveedor
                Case "FECACT"
                    index = ConfiguracionFiltrosQA_NC.FechaActualizacion
                Case "FEC_CIERRE"
                    index = ConfiguracionFiltrosQA_NC.FechaCierre
                Case "COMENT"
                    index = ConfiguracionFiltrosQA_NC.Comentarios
                Case "COMENT_ALTA"
                    index = ConfiguracionFiltrosQA_NC.ComentarioApertura
                Case "COMENT_CIERRE"
                    index = ConfiguracionFiltrosQA_NC.ComentarioCierre
                Case "COMENT_REVISOR"
                    index = ConfiguracionFiltrosQA_NC.ComentarioRevisor
                Case "COMENT_ANULACION"
                    index = ConfiguracionFiltrosQA_NC.ComentarioAnulacion
                Case "ESTADO_ACCIONES"
                    index = ConfiguracionFiltrosQA_NC.EstadosAcciones
                Case "PROVE_ERP"
                    index = ConfiguracionFiltrosQA_NC.ProveedorERP
            End Select
        Else
            Select Case Key
                Case "ID"
                    index = ConfiguracionFiltrosQA_Cert.Identificador
                Case "CODPROVE"
                    index = ConfiguracionFiltrosQA_Cert.CodigoProveedor
                Case "DENPROVE"
                    index = ConfiguracionFiltrosQA_Cert.DenominacionProveedor
                Case "REAL"
                    index = ConfiguracionFiltrosQA_Cert.Real
                Case "BAJA"
                    index = ConfiguracionFiltrosQA_Cert.Baja
                Case "MATQA"
                    index = ConfiguracionFiltrosQA_Cert.MatQA
                Case "TIPO"
                    index = ConfiguracionFiltrosQA_Cert.Tipo
                Case "ESTADO"
                    index = ConfiguracionFiltrosQA_Cert.Estado
                Case "FEC_SOL"
                    index = ConfiguracionFiltrosQA_Cert.FechaSolicitud
                Case "FEC_LIM_CUMP"
                    index = ConfiguracionFiltrosQA_Cert.FechaLimiteCumpl
                Case "FEC_RESP"
                    index = ConfiguracionFiltrosQA_Cert.FechaRespuestaProveedor
                Case "PUBL"
                    index = ConfiguracionFiltrosQA_Cert.Publicado
                Case "FEC_PUBL"
                    index = ConfiguracionFiltrosQA_Cert.FechaPublicacion
                Case "FEC_DESPUBL"
                    index = ConfiguracionFiltrosQA_Cert.FechaDesPublicacion
                Case "FEC_ACT"
                    index = ConfiguracionFiltrosQA_Cert.FechaActualizacion
                Case "PETICIONARIO"
                    index = ConfiguracionFiltrosQA_Cert.CodigoPeticionario
                Case "MODO_SOLIC"
                    index = ConfiguracionFiltrosQA_Cert.Modo
                Case "FEC_EXPIRACION"
                    index = ConfiguracionFiltrosQA_Cert.FechaExpiracion
                Case "OBLIGATORIO"
                    index = ConfiguracionFiltrosQA_Cert.Obligatorio
                Case "FEC_CUMPLIM"
                    index = ConfiguracionFiltrosQA_Cert.FechaCumplimentacion
                Case "VALIDADO"
                    index = ConfiguracionFiltrosQA_Cert.Validado
                Case "PROVISIONAL"
                    index = ConfiguracionFiltrosQA_Cert.Provisional
            End Select
        End If
        Return index
    End Function
    ''' <summary>
    ''' Carga en una tabla lo q esta seleccionado de los campos de busqueda (1er tab de pantalla) 
    ''' </summary>
    ''' <param name="dt">tabla con lo q esta seleccionado de los campos de busqueda (1er tab de pantalla) </param>
    ''' <remarks>Llamada desde: btnAceptar_Click:  Tiempo maximo:0</remarks>
    Private Sub CrearTablaCamposbusqueda_Cert(ByRef dt As DataTable)
        Dim dtNewRow As DataRow

        dt.Columns.Add("MATERIALES", System.Type.GetType("System.String"))
        dt.Columns.Add("TIPOSOL", System.Type.GetType("System.String"))
        dt.Columns.Add("TIPOBAJA", System.Type.GetType("System.Boolean"))

        dt.Columns.Add("QUEMOSTRAR", System.Type.GetType("System.Int16"))
        dt.Columns.Add("PROVEEDOR", System.Type.GetType("System.String"))
        dt.Columns.Add("HIDPROVEEDOR", System.Type.GetType("System.String"))
        dt.Columns.Add("PROVEEDORBAJA", System.Type.GetType("System.Boolean"))

        dt.Columns.Add("ESTADOS", System.Type.GetType("System.String"))

        dt.Columns.Add("PETICIONARIO", System.Type.GetType("System.String"))

        dt.Columns.Add("QUEFECHA", System.Type.GetType("System.Int16"))
        dt.Columns.Add("DESDE_FECHA", System.Type.GetType("System.DateTime"))
        dt.Columns.Add("HASTA_FECHA", System.Type.GetType("System.DateTime"))

        dtNewRow = dt.NewRow
        Dim bConBusqueda As Boolean = False

        'Baja es un check -> no bConBusqueda = True        'FechasDe son 4 option ->no bConBusqueda = True
        If CertBusqueda.Materiales <> "" OrElse CertBusqueda.TipoSolicitudes <> "" OrElse CertBusqueda.MostrarProveedores <> 0 _
        OrElse CertBusqueda.ProveedorTexto <> "" OrElse CertBusqueda.ProveedorHidden <> "" OrElse CertBusqueda.Estados <> "" _
        OrElse CertBusqueda.Peticionario <> "" Then
            bConBusqueda = True
        End If

        dtNewRow.Item("MATERIALES") = CertBusqueda.Materiales
        dtNewRow.Item("TIPOSOL") = CertBusqueda.TipoSolicitudes
        dtNewRow.Item("TIPOBAJA") = CertBusqueda.VerDadosBaja

        dtNewRow.Item("QUEMOSTRAR") = CertBusqueda.MostrarProveedores
        dtNewRow.Item("PROVEEDOR") = CertBusqueda.ProveedorTexto
        dtNewRow.Item("HIDPROVEEDOR") = CertBusqueda.ProveedorHidden
        dtNewRow.Item("PROVEEDORBAJA") = CertBusqueda.ProveedorDeBaja

        dtNewRow.Item("ESTADOS") = CertBusqueda.Estados

        dtNewRow.Item("PETICIONARIO") = CertBusqueda.Peticionario

        dtNewRow.Item("QUEFECHA") = CertBusqueda.FechasDe
        If Not (CertBusqueda.FechaDesde = #12:00:00 AM#) Then
            dtNewRow.Item("DESDE_FECHA") = CertBusqueda.FechaDesde
            bConBusqueda = True
        End If
        If Not (CertBusqueda.FechaHasta = #12:00:00 AM#) Then
            dtNewRow.Item("HASTA_FECHA") = CertBusqueda.FechaHasta
            bConBusqueda = True
        End If

        If bConBusqueda Then dt.Rows.Add(dtNewRow)
    End Sub
#End Region
#Region " BOTONES "
    ''' <summary>
    ''' Al pulsar el botón Aceptar, se guarda el nombre del filtro, los campos que ha marcado como visibles y el tamaño de los mismos.
    ''' Y te redirige al visor de tareas para ver el nuevo filtro creado o modificado.
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>       
    ''' <remarks>Llamada desde: Al hacer click sobre el botón Aceptar. Tiempo máximo: 0sg.</remarks>
    Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If Page.IsValid Then
            'Por si el repeater no pierde focus. Entonces no ha sido pasado el cambio al grid. 
            ActualizarGrid()

            Dim sCadenaCampos As String = ""
            Dim ds As DataSet
            Dim dt As DataTable
            Dim dtNewRow As DataRow

            ds = New DataSet

            dt = ds.Tables.Add("CAMPOSCONFIGURACION")

            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("VISIBLE", System.Type.GetType("System.Int32"))
            dt.Columns.Add("POSICION", System.Type.GetType("System.Int32"))
            dt.Columns.Add("TAMANYO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("NOMBRE", System.Type.GetType("System.String"))
            dt.Columns.Add("CAMPO_GENERAL", System.Type.GetType("System.Int32"))

            If cFiltro Is Nothing Then
                cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroQA))
            End If
            If Session("QAIdFiltroUsu") = "" Then
                If RadioButtonList.Items(1).Selected Then
                    cFiltro.IDFiltro = wddFiltrosDisponibles.SelectedValue
                Else
                    cFiltro.IDFiltro = 0
                End If

                cFiltro.IDFiltroUsuario = 0
            Else
                cFiltro.IDFiltro = CLng(Session("QAIdFiltro"))
                cFiltro.IDFiltroUsuario = CLng(Session("QAIdFiltroUsu"))
            End If

            cFiltro.NombreFiltro = txtNombreFiltro.Text
            cFiltro.Persona = FSNUser.CodPersona
            cFiltro.TipoSol = IIf(Request("Donde") = "VisorCert", TiposDeDatos.TipoDeSolicitud.Certificado, TiposDeDatos.TipoDeSolicitud.NoConformidad)
            cFiltro.UnForm = (RadioButtonList.Items(1).Selected)

            dsCampos = cFiltro.LoadCampos(Idioma)

            Dim index As Integer

            If RadioButtonList.Items(1).Selected Then
                Grid = whdgConfigurarFiltrosUno

                NCBusqueda = BusquedaNCUno
                CertBusqueda = BusquedaCertUno
            Else
                Grid = whdgConfigurarFiltrosTodos

                NCBusqueda = BusquedaNCTodos
                CertBusqueda = BusquedaCertTodos
            End If

            With Grid.GridView.Columns
                Dim RowExiste() As DataRow
                For iIndex As Integer = 0 To .Count - 1
                    If Left(.Item(iIndex).Key, 2) = "C_" Then
                        RowExiste = dsCampos.Tables(1).Select("ID='" & Mid(.Item(iIndex).Key, 3).ToString & "'")

                        If (RowExiste.Length = 1) Then
                            sCadenaCampos = sCadenaCampos & Mid(.Item(iIndex).Key, 3) & "|" & IIf(.Item(iIndex).Hidden, "0", "1") & "|" & .Item(iIndex).VisibleIndex & "|" & .Item(iIndex).Width.Value & "#"
                            dtNewRow = dt.NewRow
                            dtNewRow.Item("ID") = Mid(.Item(iIndex).Key, 3)
                            dtNewRow.Item("VISIBLE") = Not .Item(iIndex).Hidden
                            dtNewRow.Item("POSICION") = .Item(iIndex).VisibleIndex
                            If (dtNewRow.Item("VISIBLE") = 1) And (.Item(iIndex).Width.Value = 0) Then
                                .Item(iIndex).Width = 100
                            End If
                            dtNewRow.Item("TAMANYO") = .Item(iIndex).Width.Value
                            If Not .Item(iIndex).Hidden Then 'Solo almaceno los nombres de aquellos que sean visibles!!!
                                dtNewRow.Item("NOMBRE") = .Item(iIndex).Header.Text
                            End If
                            dtNewRow.Item("CAMPO_GENERAL") = 0
                            dt.Rows.Add(dtNewRow)
                        End If
                    Else
                        index = DamePosicionWhdgConfigurarFiltros(.Item(iIndex).Key)

                        If index <> -1 Then
                            dtNewRow = dt.NewRow
                            dtNewRow.Item("ID") = index
                            dtNewRow.Item("VISIBLE") = Not .Item(iIndex).Hidden
                            dtNewRow.Item("POSICION") = .Item(iIndex).VisibleIndex
                            If (dtNewRow.Item("VISIBLE") = 1) And (.Item(iIndex).Width.Value = 0) Then
                                .Item(iIndex).Width = 100
                            End If
                            dtNewRow.Item("TAMANYO") = .Item(iIndex).Width.Value
                            If arrDatosGeneralesDen.Count > index Then
                                If Not .Item(iIndex).Header.Text.Equals(arrDatosGeneralesDen(index)) Then dtNewRow.Item("NOMBRE") = .Item(iIndex).Header.Text
                            End If
                            dtNewRow.Item("CAMPO_GENERAL") = 1
                            dt.Rows.Add(dtNewRow)
                        End If
                    End If
                Next
            End With

            'Meter los valores de la busqueda en un dataset
            dt = ds.Tables.Add("CAMPOSBUSQUEDA")

            Dim sURL As String
            If Request("Donde") = "VisorCert" Then
                CrearTablaCamposbusqueda_Cert(dt)
                sURL = "..\Certificados\Certificados.aspx"
            Else
                CrearTablaCamposbusqueda_NC(dt)
                sURL = "..\NoConformidades\NoConformidades.aspx"
            End If

            If chkFiltroPorDefecto.Checked Then
                cFiltro.PorDefecto = IIf(Request("IdFiltroUsu") = "0", 3, 1)
            Else
                cFiltro.PorDefecto = IIf(Request("IdFiltroUsu") = "0", 2, 0)
            End If

            cFiltro.CamposConfiguracion = ds
            cFiltro.GuardarConfiguracion()

            If RadioButtonList.Items(0).Selected Then
                If Request("IdFiltroUsu") = "0" Then
                    sURL = sURL & "?EraFiltro0=1&FiltroUsuarioIdAnt=" & cFiltro.IDFiltroUsuario & "&NombreTablaAnt=&FiltroIdAnt=&CargarBusquedaAvanzada=1"
                ElseIf Request("IdFiltroUsu") <> "" Then
                    sURL = sURL & "?EraFiltro0=0&FiltroUsuarioIdAnt=" & Session("QAIdFiltroUsu") & "&NombreTablaAnt=&FiltroIdAnt=&CargarBusquedaAvanzada=1"
                Else ' filtro nuevo
                    sURL = sURL & "?EraFiltro0=0&FiltroUsuarioIdAnt=" & cFiltro.IDFiltroUsuario & "&NombreTablaAnt=&FiltroIdAnt=&CargarBusquedaAvanzada=1"
                End If
            Else
                If Request("IdFiltroUsu") <> "" Then
                    sURL = sURL & "?FiltroUsuarioIdAnt=" & Session("QAIdFiltroUsu") & "&NombreTablaAnt=" & Session("QANombreFiltro") & "&FiltroIdAnt=" & Session("QAIdFiltro") & "&CargarBusquedaAvanzada=1"
                Else
                    ' filtro nuevo
                    Dim sNombreTabla As String = ""
                    sNombreTabla = "QA_FILTRO" & cFiltro.IDFiltro
                    sURL = sURL & "?FiltroUsuarioIdAnt=" & cFiltro.IDFiltroUsuario & "&NombreTablaAnt=" & sNombreTabla & "&FiltroIdAnt=" & cFiltro.IDFiltro & "&CargarBusquedaAvanzada=1"
                End If
            End If

            HttpContext.Current.Session("QApageNumberVCert") = "0"
            Response.Redirect(sURL)
        End If
    End Sub
    ''' <summary>
    ''' Carga en una tabla lo q esta seleccionado de los campos de busqueda (1er tab de pantalla) 
    ''' </summary>
    ''' <param name="dt">tabla con lo q esta seleccionado de los campos de busqueda (1er tab de pantalla) </param>
    ''' <remarks>Llamada desde: btnAceptar_Click:  Tiempo maximo:0</remarks>
    Private Sub CrearTablaCamposbusqueda_NC(ByRef dt As DataTable)
        Dim dtNewRow As DataRow

        dt.Columns.Add("TIPO", System.Type.GetType("System.String"))
        dt.Columns.Add("SUBTIPO", System.Type.GetType("System.String"))
        dt.Columns.Add("TITULO", System.Type.GetType("System.String"))
        dt.Columns.Add("PROVEEDOR", System.Type.GetType("System.String"))
        dt.Columns.Add("PROVEEDORBAJA", System.Type.GetType("System.Boolean"))
        dt.Columns.Add("UNIDADNEGOCIO", System.Type.GetType("System.String"))
        dt.Columns.Add("GMN1", System.Type.GetType("System.String"))
        dt.Columns.Add("GMN2", System.Type.GetType("System.String"))
        dt.Columns.Add("GMN3", System.Type.GetType("System.String"))
        dt.Columns.Add("GMN4", System.Type.GetType("System.String"))
        dt.Columns.Add("ARTICULO", System.Type.GetType("System.String"))
        dt.Columns.Add("DESDE_ALTA", System.Type.GetType("System.DateTime"))
        dt.Columns.Add("HASTA_ALTA", System.Type.GetType("System.DateTime"))
        dt.Columns.Add("DESDE_ACTUALIZACION", System.Type.GetType("System.DateTime"))
        dt.Columns.Add("HASTA_ACTUALIZACION", System.Type.GetType("System.DateTime"))
        dt.Columns.Add("DESDE_LIMITERESOLUCION", System.Type.GetType("System.DateTime"))
        dt.Columns.Add("HASTA_LIMITERESOLUCION", System.Type.GetType("System.DateTime"))
        dt.Columns.Add("DESDE_CIERRE", System.Type.GetType("System.DateTime"))
        dt.Columns.Add("HASTA_CIERRE", System.Type.GetType("System.DateTime"))
        dt.Columns.Add("ESTADONCABIERTAS", System.Type.GetType("System.String"))
        dt.Columns.Add("PETICIONARIO", System.Type.GetType("System.String"))
        dt.Columns.Add("REVISOR", System.Type.GetType("System.String"))

        dtNewRow = dt.NewRow
        Dim bConBusqueda As Boolean = False
        Dim arrMat(3) As String
        Dim i As Integer
        For i = 0 To 3
            arrMat(i) = ""
        Next
        If NCBusqueda.MaterialHidden <> "" Then
            arrMat = Split(NCBusqueda.MaterialHidden, "-")
            bConBusqueda = True
        End If

        If ((NCBusqueda.Tipo > 0) OrElse (NCBusqueda.SubTipo <> "") OrElse (NCBusqueda.Titulo <> "") OrElse (NCBusqueda.Proveedor <> "") OrElse (NCBusqueda.UnidadNegocio <> "") _
                    OrElse (NCBusqueda.ArticuloHidden <> "") OrElse (NCBusqueda.EstadoNCAbiertas <> "") OrElse (NCBusqueda.Peticionario <> "") OrElse (NCBusqueda.Revisor <> "")) Then
            bConBusqueda = True
        End If

        dtNewRow.Item("TIPO") = NCBusqueda.Tipo
        dtNewRow.Item("SUBTIPO") = NCBusqueda.SubTipo
        dtNewRow.Item("TITULO") = NCBusqueda.Titulo
        dtNewRow.Item("PROVEEDOR") = NCBusqueda.Proveedor
        dtNewRow.Item("PROVEEDORBAJA") = NCBusqueda.ProveedorDeBaja
        dtNewRow.Item("UNIDADNEGOCIO") = NCBusqueda.UnidadNegocio
        dtNewRow.Item("GMN1") = arrMat(0)
        dtNewRow.Item("GMN2") = arrMat(1)
        dtNewRow.Item("GMN3") = arrMat(2)
        dtNewRow.Item("GMN4") = arrMat(3)
        dtNewRow.Item("ARTICULO") = NCBusqueda.ArticuloHidden

        If Not (NCBusqueda.FechaDesde = #12:00:00 AM#) Then
            If NCBusqueda.FechaAlta Then
                dtNewRow.Item("DESDE_ALTA") = NCBusqueda.FechaDesde
            End If

            If NCBusqueda.FechaActualizacion Then
                dtNewRow.Item("DESDE_ACTUALIZACION") = NCBusqueda.FechaDesde
            End If

            If NCBusqueda.FechaLimiteResolucion Then
                dtNewRow.Item("DESDE_LIMITERESOLUCION") = NCBusqueda.FechaDesde
            End If

            If NCBusqueda.FechaCierre Then
                dtNewRow.Item("DESDE_CIERRE") = NCBusqueda.FechaDesde
            End If
            bConBusqueda = True
        End If

        If Not (NCBusqueda.FechaHasta = #12:00:00 AM#) Then
            If NCBusqueda.FechaAlta Then
                dtNewRow.Item("HASTA_ALTA") = NCBusqueda.FechaHasta
            End If

            If NCBusqueda.FechaActualizacion Then
                dtNewRow.Item("HASTA_ACTUALIZACION") = NCBusqueda.FechaHasta
            End If

            If NCBusqueda.FechaLimiteResolucion Then
                dtNewRow.Item("HASTA_LIMITERESOLUCION") = NCBusqueda.FechaHasta
            End If

            If NCBusqueda.FechaCierre Then
                dtNewRow.Item("HASTA_CIERRE") = NCBusqueda.FechaHasta
            End If
            bConBusqueda = True
        End If

        dtNewRow.Item("ESTADONCABIERTAS") = NCBusqueda.EstadoNCAbiertas
        dtNewRow.Item("PETICIONARIO") = NCBusqueda.Peticionario
        dtNewRow.Item("REVISOR") = NCBusqueda.Revisor

        If bConBusqueda Then dt.Rows.Add(dtNewRow)
    End Sub
    ''' <summary>
    ''' Elimina la configuración del filtro y te lleva a página de seguimiento.
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde: Al hacer click sobre el botón Eliminar. Tiempo máximo: 0 sg</remarks>
    Private Sub btnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroQA))
        cFiltro.IDFiltroUsuario = CLng(Session("QAIdFiltroUsu"))
        cFiltro.TipoSol = IIf(Request("Donde") = "VisorCert", TiposDeDatos.TipoDeSolicitud.Certificado, TiposDeDatos.TipoDeSolicitud.NoConformidad)
        cFiltro.UnForm = Not (Me.RadioButtonList.Items(0).Selected)
        cFiltro.EliminarConfiguracion()

        HttpContext.Current.Session("QApageNumberVCert") = "0"

        If Request("Donde") = "VisorCert" Then
            Response.Redirect("..\Certificados\Certificados.aspx")
        Else
            Response.Redirect("..\NoConformidades\NoConformidades.aspx")
        End If
        Accion.Value = "Eliminar"
    End Sub
    ''' <summary>
    ''' Al pulsar el botón Cancelar te lleva al visor de tareas
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>       
    ''' <remarks>Llamada desde: Al hacer click en el botón Cancelar. Tiempo máximo: 0sg.</remarks>
    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Dim sURL As String
        If Request("Donde") = "VisorCert" Then
            sURL = "..\Certificados\Certificados.aspx"
        Else
            sURL = "..\NoConformidades\NoConformidades.aspx"
        End If

        If Request("IdFiltroUsu") <> "" Then
            sURL = sURL & "?FiltroUsuarioIdAnt=" & Session("QAIdFiltroUsu") & "&NombreTablaAnt=" & Session("QANombreFiltro") & "&FiltroIdAnt=" & Session("QAIdFiltro")
        End If

        Response.Redirect(sURL)
        Accion.Value = "Cancelar"
    End Sub
#End Region
    ''' <summary>
    ''' La variable oculta Accion me indica que evento a pulsado el usuario.
    ''' Al no poder programar el evento del check del repeater esa accion no dispondra de ningun nombre
    ''' por lo que todos los demas al tener un nombre sabremos que accion ha pulsado.
    ''' </summary>
    ''' <param name="sender">las del evento</param>
    ''' <param name="e">las del evento</param>        
    ''' <remarks>Evento que salta al cargarse completamente la pagina; Tiempo máximo=0,3seg.</remarks>
    Private Sub ConfigurarFiltrosQA_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If IsPostBack Then
            If Accion.Value = "" Then
                ActualizarGrid()
            End If
        End If
        Accion.Value = ""
    End Sub
    ''' <summary>
    ''' Carga las propiedades del usercontrol wucBusquedaAvanzada
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>       
    ''' <remarks>Evento que salta antes de dibujarse la pagina; Tiempo máximo=0,1seg.</remarks>
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack() AndAlso Request("IdFiltroUsu") <> "" Then
            If Request("Donde") = "VisorCert" Then
                If Not (Request("IdFiltroUsu") = "0") Then
                    CargarBusquedaAvanzadaCert()
                    ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltroQA
                Else ''0->Aun no creado
                    CertBusqueda.CargaPorDefecto()
                End If
            Else
                CargarBusquedaAvanzadaNC()
            End If
        End If
    End Sub
    ''' <summary>
    ''' Los filtros pueden ser para un formulario o para todos. Esta función selecciona q va el filtro actual
    ''' va a ser para todos.
    ''' </summary>
    ''' <remarks>Llamada desde: RadioButtonList_SelectedIndexChanged. Tiempo máximo: 0,3sg.</remarks>
    Private Sub optFormTodos_CheckedChanged()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltroQA

        wddFiltrosDisponibles.Enabled = Not (RadioButtonList.Items(0).Selected)

        wddFiltrosDisponibles.CurrentValue = ""
        wddFiltrosDisponibles.SelectedValue = Nothing
        For i = 0 To wddFiltrosDisponibles.Items.Count - 1
            wddFiltrosDisponibles.Items(i).Selected = False
        Next
        upFiltrosDisponibles.Update()

        RepeaterDatosGeneral = RepeaterDatosGeneralesTodos
        sTextoCGVisible = "CampoGeneral_VisibleTodos"
        sTextoCGNombre = "CampoGeneral_NombrePersonalizadoTodos"
        sTextoCGID = "CampoGeneralIDTodos"
        sTextoCGLbl = "CampoGeneral_lblCampoGeneralTodos"

        Grid = whdgConfigurarFiltrosTodos

        NCBusqueda = BusquedaNCTodos
        CertBusqueda = BusquedaCertTodos

        If CertBusqueda.MostrarItemProveedores = -1 Then CertBusqueda.MostrarProveedores = 3

        BusquedaCertTodos.IdentidadPanel = 1

        CargaParaTodos()
    End Sub

    ''' <summary>
    ''' Los filtros pueden ser para un formulario o para todos. Esta función selecciona q va el filtro actual
    ''' va a ser para uno en concreto.
    ''' </summary>
    ''' <remarks>Llamada desde: RadioButtonList_SelectedIndexChanged. Tiempo máximo: 0,3sg.</remarks>
    Protected Sub optFormUn_CheckedChanged()
        wddFiltrosDisponibles.Enabled = (RadioButtonList.Items(1).Selected)

        RepeaterDatosGeneral = RepeaterDatosGeneralesUno
        sTextoCGVisible = "CampoGeneral_VisibleUno"
        sTextoCGNombre = "CampoGeneral_NombrePersonalizadoUno"
        sTextoCGID = "CampoGeneralIDUno"
        sTextoCGLbl = "CampoGeneral_lblCampoGeneralUno"

        Grid = whdgConfigurarFiltrosUno

        NCBusqueda = BusquedaNCUno
        CertBusqueda = BusquedaCertUno

        BusquedaCertTodos.IdentidadPanel = 0

        phConfiguracionFiltroUno.Visible = False
        phCamposUno.Visible = False
        phConfiguracionFiltroTodos.Visible = False
        phCamposTodos.Visible = False

        upFiltrosDisponibles.Update()
    End Sub
    ''' <summary>
    ''' El filtro actual va a ser para todos los formularios. Esta función carga los campos de busqueda.
    ''' </summary>
    ''' <remarks>Llamada desde: optFormTodos_CheckedChanged. Tiempo máximo: 0,3sg.</remarks>
    Private Sub CargaParaTodos()
        Session("QAIdFiltro") = -1

        cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroQA))
        cFiltro.IDFiltro = 0
        cFiltro.Persona = FSNUser.CodPersona
        cFiltro.TipoSol = IIf(Request("Donde") = "VisorCert", TiposDeDatos.TipoDeSolicitud.Certificado, TiposDeDatos.TipoDeSolicitud.NoConformidad)

        cFiltro.UnForm = False

        Grid.Rows.Clear()
        Grid.Columns.Clear()
        Grid.GridView.Columns.Clear()
        Dim dt As DataTable
        Dim datasrc As New DataSet
        dt = cFiltro.LoadConfiguracionInstancia(Idioma).Tables(0)
        datasrc.Tables.Add(dt.Copy())
        Grid.DataSource = datasrc
        CrearColumnasDT(Grid, dt)
        Grid.DataBind()
        ConfigurarGrid(Grid)

        'Campos Generales
        CargaCamposGenerales(0)

        If Request("Donde") = "VisorCert" Then
            CertBusqueda.Filtro = -1
        Else
            NCBusqueda.Filtro = -1
        End If

        phConfiguracionFiltroTodos.Visible = True
        phCamposTodos.Visible = True
        phConfiguracionFiltroUno.Visible = False
        phCamposUno.Visible = False

        btnAceptar.Visible = True
        btnCancelar.Visible = True

        upBotones.Update()
        Accion.Value = "SeleccionFiltro"
    End Sub
    ''' <summary>
    ''' Los filtros pueden ser para un formulario o para todos. Esta función selecciona cualde las dos opciones va a ser.
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>      
    ''' <remarks>Llamada desde: sistema. Tiempo máximo: 0,1 sg.</remarks>
    Protected Sub RadioButtonList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButtonList.SelectedIndexChanged
        If RadioButtonList.Items(0).Selected Then
            optFormTodos_CheckedChanged()
            wddFiltrosDisponibles.SelectedItemIndex = -1
        Else
            wddFiltrosDisponibles.CurrentValue = ""
            wddFiltrosDisponibles.SelectedValue = Nothing
            upFiltrosDisponibles.Update()

            optFormUn_CheckedChanged()
        End If

        upCriteriosBusquedaUno.Update()
        upCamposUno.Update()
        upCriteriosBusquedaTodos.Update()
        upCamposTodos.Update()
    End Sub
#Region "Fns Auxiliares"
    ''' <summary>
    ''' Recibe un numero y lo devuelve formateado segun configuraciÃ³n del usuario y aparte quita los decimales si son 0
    ''' </summary>
    ''' <param name="sValor">Numero</param>
    ''' <returns>numero formateado</returns>
    ''' <remarks>Llamadas desde: uwgCertificados_InitializeRow ; Tiempo maximo:0 </remarks>
    Function QuitarDecimalSiTodoCeros(ByVal sValor As String) As String
        Dim decimales As String = ""
        If FSNUser.PrecisionFmt > 0 AndAlso Right(sValor, FSNUser.PrecisionFmt + 1) = FSNUser.DecimalFmt & decimales.PadRight(FSNUser.PrecisionFmt, "0") Then
            sValor = Left(sValor, Len(sValor) - FSNUser.PrecisionFmt - 1)
        End If
        QuitarDecimalSiTodoCeros = sValor
    End Function
#End Region
End Class