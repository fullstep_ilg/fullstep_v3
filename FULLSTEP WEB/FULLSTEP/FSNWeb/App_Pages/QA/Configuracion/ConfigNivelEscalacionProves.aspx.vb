﻿Imports System.Web.Script.Serialization
Imports Fullstep.FSNServer
Public Class ConfigNivelEscalacionProves
    Inherits FSNPage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FSNPageHeader.VisibleBotonGuardar = True
        FSNPageHeader.OnClientClickGuardar = "GuardarCambios();return false;"

        Cargar_Textos_Pantalla()

        Dim oNivelesEscalacion As FSNServer.Escalaciones = FSNServer.Get_Object(GetType(FSNServer.Escalaciones))
        Dim lNivelesEscalacion As List(Of Escalacion) = oNivelesEscalacion.GetNivelesEscalacion

        Dim lNivelesEscalacionOptions As New List(Of Object)
        Dim iNivelesEscalacion As Integer = If(lNivelesEscalacion.Count = 4, lNivelesEscalacion.Count, lNivelesEscalacion.Count + 1)
        For i As Integer = 1 To iNivelesEscalacion
            lNivelesEscalacionOptions.Add(New With {.nivel = i, .denominacion = Textos(2) & " " & i})
        Next

        If Application("Nivel_UnQa") Is Nothing Then
            FSNServer.Load_UnQa()
            Application("Nivel_UnQa") = FSNServer.NivelesUnQa
        End If

        Dim oUNQA As FSNServer.UnidadNeg
        oUNQA = FSNServer.Get_Object(GetType(FSNServer.UnidadNeg))

        oUNQA.PYME = FSNUser.Pyme
        oUNQA.CargarIDNodoRaiz()

        oUNQA.UnidadesNeg_CargarIdiomas()

        Dim denominacion As String = String.Empty
        Dim lNivelesUnqa As New List(Of Object)
        lNivelesUnqa.Add(New With {.nivel = "", .denominacion = ""})
        For i As Integer = 0 To CInt(Application("Nivel_UnQa")) 'Falta poner los nombre de los niveles
            If i = 0 Then
                denominacion = Split(oUNQA.IdiomasColeccion.Item(FSNUser.Idioma.ToString), "#")(2)
            Else
                denominacion = Textos(1) & " " & i
            End If
            lNivelesUnqa.Add(New With {.nivel = i, .denominacion = denominacion})
        Next

        Dim oSolicitudes As Solicitudes = FSNServer.Get_Object(GetType(FSNServer.Solicitudes))
        oSolicitudes.LoadData(FSNUser.Cod, Idioma, bEsCombo:=1, iTipoSolicitud:=TiposDeDatos.TipoDeSolicitud.NoConformidad, bComboAltaNC:=True)

        Dim lNoConformidades = oSolicitudes.Data.Tables(0).Rows.OfType(Of DataRow).Select(Function(y) New With {.id = y("ID").ToString, .denominacion = If(IsDBNull(y("ID")), "", (y("COD") & " - " & y("DEN")).ToString)}).ToList

        Dim oIdiomasAplicacion As Idiomas = FSNServer.Get_Object(GetType(FSNServer.Idiomas))
        oIdiomasAplicacion.Load()

        Dim serializer As New JavaScriptSerializer
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "datosPantalla", "var datosPantalla = " & serializer.Serialize({lNivelesEscalacionOptions, lNivelesUnqa, lNoConformidades, lNivelesEscalacion,
                                                                                                                                 oIdiomasAplicacion.Idiomas, FSNUser.Idioma.ToString}) & ";", True)
    End Sub
    Private Sub Cargar_Textos_Pantalla()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfigNivelEscalacionProves
        FSNPageHeader.TituloCabecera = Textos(0)
        FSNPageHeader.UrlImagenCabecera = ConfigurationManager.AppSettings("ruta") & "Images\ConfigNivelEscalacionProves.png"
        FSNPageHeader.TextoBotonGuardar = Textos(8)

        lblDenominacion.Text = Textos(3) & ":"
        lblDescripcion.Text = Textos(4) & ":"
        lblNivelEstrucUniNeg.Text = Textos(5)
        lblNoConformidadEscalacion.Text = Textos(6)

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
            Dim sVariableJavascriptTextosPantalla As String = "var TextosPantalla = new Array();"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[0]='" & JSText(Textos(7)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[1]='" & JSText(Textos(2)) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextosPantalla, True)
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosAlert") Then
            Dim sVariableJavascriptTextosPantalla As String = "var TextosAlert = new Array();"
            sVariableJavascriptTextosPantalla &= "TextosAlert[0]='" & JSText(Textos(9)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosAlert[1]='" & JSText(Textos(10)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosAlert[2]='" & JSText(Textos(11)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosAlert[3]='" & JSText(Textos(12)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosAlert[4]='" & JSText(Textos(13)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosAlert[5]='" & JSText(Textos(14)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosAlert[6]='" & JSText(Textos(15)) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosAlert", sVariableJavascriptTextosPantalla, True)
        End If
    End Sub
#Region "Web Methods"
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Sub Guardar_Config_NivelesEscalacion(ByVal nivelEscalacion As String)
        Try
            Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim serializer As New JavaScriptSerializer()
            Dim oNivelEscalacion As FSNServer.Escalacion = serializer.Deserialize(Of Escalacion)(nivelEscalacion)
            Dim oNivelesEscalacion As FSNServer.Escalaciones = FSNServer.Get_Object(GetType(FSNServer.Escalaciones))

            oNivelesEscalacion.GuardarNivelEscalacion(oNivelEscalacion)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
End Class