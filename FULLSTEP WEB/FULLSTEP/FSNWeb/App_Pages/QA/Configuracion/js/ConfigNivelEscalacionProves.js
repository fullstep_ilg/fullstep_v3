﻿var iniNivelesEscalacion;
var nivelSeleccionado;
$(document).ready(function () {    
    Carga_Inicial_Pantalla();
    //Logica de datos cargados    
    Gestion_Check_BloqueoSelProveAdj();
    
    //#region Eventos
    $('#cboNivelEscalacion').on('change', function () { NivelEscalacion_Change(); });
    //#endregion
});
function Carga_Inicial_Pantalla() {
    //Cargamos los combos de la pantalla
    $('#lblBloqueoSelProveAdj').text(TextosPantalla[0]);
    //Niveles Escalacion
    $('#cboNivelEscalacion option').remove();
    var cboNivelesEscalacion = $('#cboNivelEscalacion').prop('options');
    $.each(datosPantalla[0], function () {
        cboNivelesEscalacion[cboNivelesEscalacion.length] = new Option(this.denominacion, this.nivel);
    });    
    //Niveles UNQA
    $('#cboNivelEstrucUniNeg option').remove();
    var cboNivelEstrucUniNeg = $('#cboNivelEstrucUniNeg').prop('options');
    $.each(datosPantalla[1], function () {
        cboNivelEstrucUniNeg[cboNivelEstrucUniNeg.length] = new Option(this.denominacion, this.nivel);
    });
    //Tipos No conformidad
    $('#cboTiposNoConformidad option').remove();
    var cboTiposNoConformidad = $('#cboTiposNoConformidad').prop('options');
    $.each(datosPantalla[2], function () {
        cboTiposNoConformidad[cboTiposNoConformidad.length] = new Option(this.denominacion, this.id);
    });
    
    $('#txtDenominacionMultiidioma').fsMultiLanguage({ languages: $.map(datosPantalla[4], function (x) { return x.Cod }), defaultValue: datosPantalla[5] });
    $('#txtDescripcionMultiidioma').fsMultiLanguage({ height: '10em', isTextArea: true, languages: $.map(datosPantalla[4], function (x) { return x.Cod }), defaultValue: datosPantalla[5] });
        
    iniNivelesEscalacion = datosPantalla[3];    
    if (iniNivelesEscalacion.length > 0) Cargar_Configuracion_NivelEscalacion(1);
    
    nivelSeleccionado = parseInt($('#cboNivelEscalacion').val());
};
function Gestion_Check_BloqueoSelProveAdj() {
    if (parseInt($('#cboNivelEscalacion').val()) === 1 || parseInt($('#cboNivelEscalacion').val()) === 2) $('[id$=BloqueoSelProveAdj]').hide();
    else $('[id$=BloqueoSelProveAdj]').show();
};
function NivelEscalacion_Change() {
    var tempNivelEscalacion = GenerarNivelValidacionPantalla();
    var iNivelEscalacion = $.grep(iniNivelesEscalacion, function (x) { return x.Nivel === tempNivelEscalacion.Nivel });
    if (iNivelEscalacion.length === 1) {
        iNivelEscalacion = iNivelEscalacion[0];
        if (JSON.stringify(tempNivelEscalacion) !== JSON.stringify(iNivelEscalacion)) {
            if (confirm(TextosAlert[6])) {
                nivelSeleccionado = parseInt($('#cboNivelEscalacion').val());
                Cargar_Configuracion_NivelEscalacion(nivelSeleccionado);
                Gestion_Check_BloqueoSelProveAdj();
            } else $('#cboNivelEscalacion').find('option[value=' + nivelSeleccionado + ']').prop('selected', true);
        } else {
            nivelSeleccionado = parseInt($('#cboNivelEscalacion').val());
            Cargar_Configuracion_NivelEscalacion(nivelSeleccionado);
            Gestion_Check_BloqueoSelProveAdj();
        };
    } else {
        if (confirm(TextosAlert[6])) {
            nivelSeleccionado = parseInt($('#cboNivelEscalacion').val());
            Cargar_Configuracion_NivelEscalacion(nivelSeleccionado);
            Gestion_Check_BloqueoSelProveAdj();
        }
    };    
};
function Cargar_Configuracion_NivelEscalacion(nivel) {
    var pNivelEscalacion = $.grep(iniNivelesEscalacion, function (x) { return x.Nivel === nivel });
    if (pNivelEscalacion.length === 0) {
        $('#cboNivelEstrucUniNeg').find('option[value=""]').prop('selected', true);
        $('#cboTiposNoConformidad').find('option[value=""]').prop('selected', true);
        $('#chkBloqueoSelProveAdj').prop('checked', false);
        $('#txtDenominacionMultiidioma').fsMultiLanguage('clearValues');
        $('#txtDescripcionMultiidioma').fsMultiLanguage('clearValues');
    } else {
        pNivelEscalacion = pNivelEscalacion[0];
        $('#cboNivelEstrucUniNeg').find('option[value=' + pNivelEscalacion.NivelUNQA + ']').prop('selected', true);
        $('#cboTiposNoConformidad').find('option[value=' + pNivelEscalacion.Solicitud + ']').prop('selected', true);
        $('#chkBloqueoSelProveAdj').prop('checked', pNivelEscalacion.Bloqueo_SelProve_Adj);
        $.each(pNivelEscalacion.Denominacion, function (key, value) {
            $('#txtDenominacionMultiidioma').fsMultiLanguage('setText', key, value);
        });
        $.each(pNivelEscalacion.Descripcion, function (key, value) {
            $('#txtDescripcionMultiidioma').fsMultiLanguage('setText', key, value);
        });
        $('#txtDenominacionMultiidioma').fsMultiLanguage('setDefaultValue');
        $('#txtDescripcionMultiidioma').fsMultiLanguage('setDefaultValue');
    }
};
function GuardarCambios() {    
    var tempNivelEscalacion = GenerarNivelValidacionPantalla();
    if (ValidacionesCorrectas(tempNivelEscalacion)) {
        var guardar = false;
        //Vamos a comprobar si hay alguna modificacion en la configuracion de nivel de escalacion actual. 
        //Para ello comparamos, de existir ya, la existente (pasado a string) con la actual en pantalla (pasado a string)
        if (iniNivelesEscalacion.length > 0 && $.grep(iniNivelesEscalacion, function (x) { return x.Nivel === tempNivelEscalacion.Nivel }).length !== 0) {
            if (JSON.stringify(tempNivelEscalacion) !== JSON.stringify($.grep(iniNivelesEscalacion, function (x) { return x.Nivel === tempNivelEscalacion.Nivel })[0])) guardar = true;
        } else guardar = true;
        if (guardar) {
            $('body').toggleClass("wait");
            $.when($.ajax({
                type: 'POST',
                url: rutaFS + 'QA/Configuracion/ConfigNivelEscalacionProves.aspx/Guardar_Config_NivelesEscalacion',
                data: JSON.stringify({ nivelEscalacion: JSON.stringify(tempNivelEscalacion) }),
                contentType: 'application/json;',
                async: true
            })).done(function (msg) {
                setTimeout(function () { $('body').toggleClass("wait"); $('body').css('cursor', 'auto'); }, 1000);
                iniNivelesEscalacion = $.grep(iniNivelesEscalacion, function (x) { return x.Nivel !== tempNivelEscalacion.Nivel });
                iniNivelesEscalacion.push(tempNivelEscalacion);
                if (iniNivelesEscalacion.length < 4 && ($('#cboNivelEscalacion option').length === iniNivelesEscalacion.length))
                    $('#cboNivelEscalacion').prop('options')[$('#cboNivelEscalacion').prop('options').length] = new Option(TextosPantalla[1] + ' ' + ($('#cboNivelEscalacion').prop('options').length + 1), ($('#cboNivelEscalacion').prop('options').length + 1));
            });
        }
    }
};
function ValidacionesCorrectas(tNivelEscalacion) {
    var correcto = true;
    if (tNivelEscalacion.NivelUNQA === -1) {
        alert(TextosAlert[1]);
        correcto = false;
        return false;
    } else {
        if (tNivelEscalacion.Nivel > 1 && $.grep(iniNivelesEscalacion, function (x) { return x.Nivel == (tNivelEscalacion.Nivel - 1) })[0].NivelUNQA < tNivelEscalacion.NivelUNQA) {
            alert(TextosAlert[0].replace('#####', (tNivelEscalacion.Nivel - 1)).replace('###',
                $($('#cboNivelEstrucUniNeg').find('option[value=' + $.grep(iniNivelesEscalacion, function (x) { return x.Nivel == (tNivelEscalacion.Nivel - 1) })[0].NivelUNQA + ']')[0]).text()));
            correcto = false;
            return false;
        }
    }
    if (tNivelEscalacion.Solicitud === 0) {
        alert(TextosAlert[2]);
        correcto = false;
        return false;
    };
    
    $.each(tNivelEscalacion.Denominacion, function (language, value) {
        if (value === '') {
            correcto = false;
            alert(TextosAlert[3]);
            return false;
        }
    });
    if (!correcto) return false;
    $.each(tNivelEscalacion.Descripcion, function (language, value) {
        if (value === '') {
            correcto = false;
            alert(TextosAlert[4]);
            return false;
        }
    });
    if (!correcto) return false;
    if ($.grep(iniNivelesEscalacion, function (x) { return (x.Solicitud == tNivelEscalacion.Solicitud && x.Nivel!==tNivelEscalacion.Nivel) }).length > 0) {
        correcto = false;
        alert(TextosAlert[5]);
        return false;
    }
    return correcto;
};
function GenerarNivelValidacionPantalla() {
    var error = false;
    var tempNivelEscalacion = {
        Nivel: nivelSeleccionado,
        NivelUNQA: ($('#cboNivelEstrucUniNeg').val() === '' ? -1 : parseInt($('#cboNivelEstrucUniNeg').val())),
        Denominacion: $('#txtDenominacionMultiidioma').fsMultiLanguage('getValues'),
        Descripcion: $('#txtDescripcionMultiidioma').fsMultiLanguage('getValues'),
        Solicitud: ($('#cboTiposNoConformidad').val() === '' ? 0 : parseInt($('#cboTiposNoConformidad').val())),
        Bloqueo_SelProve_Adj: $('#chkBloqueoSelProveAdj').prop('checked')
    };
    
    return tempNivelEscalacion;
};