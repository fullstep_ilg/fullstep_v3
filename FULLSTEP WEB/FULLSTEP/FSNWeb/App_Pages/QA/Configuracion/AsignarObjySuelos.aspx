﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AsignarObjySuelos.aspx.vb" Inherits="Fullstep.FSNWeb.AsignarObjySuelos"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
	<title></title>       

</head>
<body style="background-color: #FFFFFF; margin-top: 0px; margin-left: 0px; margin-right: 0px" onbeforeunload="comprobarsalida();">
	<script type="text/javascript">
	    var sProveContextKey = "";

		/*''' Revisado por: Jbg. Fecha: 20/10/2011
		''' <summary>
		''' Cierra sin hacer nada
		''' </summary>
		''' <param name="sender">parametro sistema</param>       
		''' <remarks>Llamada desde: btnCancelar ; Tiempo máximo: 0</remarks>*/
		function Cancelar(sender) {
			document.getElementById('boton').value = 2;
			window.close();

			return false;
		}
		/*''' Revisado por: Jbg. Fecha: 20/10/2011
		''' <summary>
		''' Marcar los cambios realizados, por si hay cambios y se selecciona otra Unidad para q grabe los cambios y carge el grid.
		''' Pq de esta función? pq primero salta cambio combo y luego rowupdating del grid. Entonces donde se lanza carga del grid
		''' si lo haces antes del rowupdating mal y despues no hay evento -> el propio rowupdating debe cargar cuando toque el grid 
		''' segun el nuevo valor del combo.
		''' </summary>
		''' <param name="elem">celda q cambia</param>
		''' <param name="event">evento</param>        
		''' <remarks>Llamada desde: grid clientside ExitedEditMode; Tiempo máximo:0</remarks>*/
		function ugExitedEditMode(elem, event) {
			if (event._editor.get_value() != event._editor.value) {
				hid_Cambios = document.getElementById("<%=hid_Cambios.ClientId%>");
				var res = hid_Cambios.value;
				var res1 = '@' + event.getCell().get_row().get_dataKey()[0] + '@';
				if (res.search(res1) == -1) {
					hid_Cambios.value = hid_Cambios.value + '@' + event.getCell().get_row().get_dataKey()[0] + '@';
				} 
			}
		}

		/*''' Revisado por: Jbg. Fecha: 20/10/2011
		''' <summary>
		''' Al cambiar la Unidad de negocio hay q recargar el grid. Antes de recargar en caso de q haya cambios en los 
		''' objetivos o suelos hay q dar la posibilidad de grabar.
		''' </summary>
		''' <param name="elem">is the object which is raising the event</param>
		''' <param name="event">is the RowSelectionChangedEventArgs</param>        
		''' <remarks>Llamada desde: wddUnidadNegocio /ClientEvents SelectionChanged ; Tiempo máximo: 0,1</remarks>*/
	    function SelectionChanged(elem, event) {
			document.getElementById('unidades').value = 1;
			document.getElementById('boton').value = 0;
			document.getElementById('proveedor').value = 0;					

			var oCombo = elem;

			if (document.getElementById('gridmodificado').value == '1') {
				if (confirm(confirmcancelar)) {
					document.getElementById('recargar').value = 'perder';
					document.getElementById('PosComboQA').value = elem.get_selectedItem().get_index();
					
					__doPostBack('', 'Perder');
					return true;
				}
				else {
					document.getElementById('recargar').value = '0';
					document.getElementById('unidades').value = 0;
					oCombo.selectItem(oCombo.get_items().getItem(document.getElementById('PosComboQA').value), true, true);
					oCombo.closeDropDown();
					oCombo.dispose;
					return false;
				}
			}
			else {
				document.getElementById('recargar').value = '1';
				document.getElementById('PosComboQA').value = elem.get_selectedItem().get_index();
				
				__doPostBack('UniNeg', '');
				return true;            
			}
		}

		// <summary>Al cambiar el proveedor hay q recargar el grid. Antes de recargar en caso de q haya cambios en los objetivos o suelos hay q dar la posibilidad de grabar</summary>		  
		// <remarks>Llamada desde: selected_Proveedor, selected_Proveedor</remarks>
	    function ProveedorCambiado(sProveCodOld, sProveDenOld, sProveCod) {	        
		    document.getElementById('unidades').value = 0;
		    document.getElementById('boton').value = 0;
		    document.getElementById('proveedor').value = 1;

		    if (document.getElementById('gridmodificado').value == '1') {
		        if (confirm(confirmcancelar)) {
		            document.getElementById('recargar').value = 'perder';

		            __doPostBack('', 'Perder');
		            return true;
		        }
		        else {
		            document.getElementById('recargar').value = '0';
		            document.getElementById('proveedor').value = 0;
		            document.getElementById("<%=hidProveedor.ClientID%>").value = sProveCodOld;
	                document.getElementById("<%=hdenProveedor.ClientID%>").value = sProveDenOld;
	                document.getElementById("<%=txtProveedor.ClientID%>").value = sProveCodOld + "-" + sProveDenOld;

		            return false;
		        }
		    }
		    else {
		        document.getElementById('recargar').value = '1';		        

		        __doPostBack('Prove', '');
		        return true;
		    }
		}

		botonexport = 0

		/*''' Revisado por: Jbg. Fecha: 20/10/2011
		''' <summary>
		''' Control necesario cuando se pulsan los botones de exportar a excell, html o pdf para que no salten los demas eventos 
		''' debido a que provocan el unload de la pagina
		''' </summary>
		''' <remarks>Llamada desde:	cada vez que se pincha en los botones de exportacion; Tiempo máximo: 0</remarks>*/
		function Boton1() {
			botonexport = 1;
		}

		/*''' Revisado por: Jbg. Fecha: 20/10/2011
		''' <summary>
		''' Control necesario cuando se va salir de la pantalla
		''' </summary>
		''' <remarks>Llamada desde:	cada vez que se pincha la X de la pantalla; Tiempo máximo:	0</remarks>*/
		function comprobarsalida() {		    
			if (botonexport == 0) {
			    if (document.getElementById('boton').value != 1 && document.getElementById('unidades').value != 1 && document.getElementById('proveedor').value != 1) {
					if (document.getElementById('gridmodificado').value == 1) {
							if (confirm(confirmsalir)) {
								document.getElementById('recargar').value = 'salida';
								__doPostBack("", "salida");
							}
							else {
								document.getElementById('recargar').value = 0;
							}
							if (document.getElementById('boton').value == 2) document.getElementById('boton').value = 1;							
					}
				}

			    document.getElementById('unidades').value = 0
			    document.getElementById('proveedor').value = 0
			}
			else {
				botonexport = botonexport - 1;
			}
		}
		/*''' Revisado por: Jbg. Fecha: 20/10/2011
		<summary>
		da vez q se cambia un objetivo o suleo se marca q ha habido un cambio y donde. var oIdProveedor
		</summary>
		<remarks>Llamada desde: NumericEditorProvider/ clientSide.TextChange</remarks>
		*/
		function ChangeValor(oEdit, newText, oEvent) {
			document.getElementById('gridmodificado').value = 1;                    
		}
		/*''' Revisado por: Jbg. Fecha: 20/10/2011
		<summary>
		Provoca el postback para hacer la esportación a excel
		</summary>
		<remarks>Llamada desde: Icono de exportar a Excel</remarks>
		*/
		function MostrarExcel() {
			Boton1()
			__doPostBack("", "Excel");
		}
		/*''' Revisado por: Jbg. Fecha: 20/10/2011
		<summary>
		Provoca el postback para hacer la esportación a Pdf
		</summary>
		<remarks>Llamada desde: Icono de exportar a Pdf</remarks>
		*/
		function MostrarPdf() {
			Boton1()
			__doPostBack("", "Pdf");
		}
        //Recoge el ID del proveedor seleccionado con el autocompletar
		function selected_Proveedor(sender, e) {		    
	        prove_seleccionado(e._value, e._text.split("-")[1]);
	    }
        //Abre el buscador de proveedores
		function AbrirBuscadorProveedor() {		   
		    var UNQAID = "";
		    var UNQADen = "";
		    if (!document.getElementById("wddUnidadNegocio").hidden) {
		        UNQAID = document.getElementById("ComboQA").value;
		        UNQADen = $find("wddUnidadNegocio").get_currentValue();
		    }
		    else {
		        UNQAID = document.getElementById("UnicoUnidadQA").value;
		        UNQADen = document.getElementById("lblUnicoUnidadQA").value;
		    }		    
		    window.open("<%=ConfigurationManager.AppSettings("RutaFS")%>_common/BuscadorProveedores.aspx?PM=false&desde=ObjSuelos&IDCONTROL=<%=txtProveedor.ClientID%>&MQAID=" + materialQAID + "&MQADen=" + document.getElementById("lblMaterial").innerText + "&UNQAID=" + UNQAID + "&UNQADen=" + UNQADen, "_blank", "width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes");            		    
		}
	    //Función que se ejecuta cuando se selecciona un proveedor desde el buscador de proveedores
        //sCIF no se utiliza pero se mantiene porque desde el buscador de proveedores se llama a esta función con esta interfaz
	    function prove_seleccionado(sProveCod, sProveDen) {	        
	        var sProveCodOld = "";
	        var sProveDenOld = "";

	        if (document.getElementById("<%=hidProveedor.ClientID%>").value != "") {
	            sProveCodOld = document.getElementById("<%=hidProveedor.ClientID%>").value;
	            sProveDenOld = document.getElementById("<%=hdenProveedor.ClientID%>").value;
	        }
	        document.getElementById("<%=hidProveedor.ClientID%>").value = sProveCod;
	        document.getElementById("<%=hdenProveedor.ClientID%>").value = sProveDen;
	        if (sProveCod!="") document.getElementById("<%=txtProveedor.ClientID%>").value = sProveCod + "-" + sProveDen;

	        ProveedorCambiado(sProveCodOld, sProveDenOld, sProveCod);
	    }
	    function txtProveedor_KeyUp(oTextBox) {
	        if (oTextBox.value == "") prove_seleccionado("", "");
	    }
	    function txtProveedor_TextChanged(oTextBox) {
	        if (oTextBox.value != "") {
	            ComprobarProveedor(oTextBox.value.split("-")[0]);
	        }
	        else {
	            prove_seleccionado("", "");
	        }
	    }
	    function ComprobarProveedor(sProveCod) {	        
	        //Comprobar proveedor
	        var UNQAID = "";
	        if (!document.getElementById("wddUnidadNegocio").hidden) {
	            UNQAID = document.getElementById("ComboQA").value;
	        }
	        else {
	            UNQAID = document.getElementById("UnicoUnidadQA").value;
	        }

	        var params = {
	            prefixText: sProveCod,
	            count: 1,
	            contextKey: usuCod + "&" + materialQAID + "&" + UNQAID    //sUsuCod + "&" + iIdMatQA + "&" + iIdUNQA
	        };
	        var respuesta;
	        $.when($.ajax({
	            type: "POST",
	            url: ruta + '/App_Pages/_Common/App_Services/AutoComplete.asmx/GetProveedoresObjSuelos',
	            contentType: "application/json; charset=utf-8",
	            data: JSON.stringify(params),
	            dataType: "json",
	            async: false
	        })).done(function (msg) {
	            var arArt = msg.d;
	            if (arArt.length == 0) {
	                document.getElementById("txtProveedor").value = "";
	            }
	            else {
	                var oArt = JSON.parse(arArt[0]);
	                prove_seleccionado(oArt.Second, oArt.First.split("-")[1]);
	            }
	        });
	    }
	    function SetProveContextKey() {	        
	        var UNQAID = "";	        
	        if (!document.getElementById("wddUnidadNegocio").hidden) {
	            UNQAID = document.getElementById("ComboQA").value;	            
	        }
	        else {
	            UNQAID = document.getElementById("UnicoUnidadQA").value;	            
	        }
	        	        	       
	        var oExtender = $find("txtProveedor_AutoCompleteExtender");
	        oExtender.set_contextKey(usuCod + "&" + materialQAID + "&" + UNQAID);
	    }
	    //<summary>Limpia las variables de contexto para el extender del código del proveedor</summary>  
	    //<remarks>Llamada desde:</remarks>
	    function ResetProveContextKey() {
	        sProveContextKey = "";
	    }	    
	</script>
	<form id="Form1" name="Form1" runat="server" method="post">
	<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
	</asp:ScriptManager>
	<asp:Panel ID="pnlTitulo" runat="server" Width="100%">
	<table style="width:100%;" border="0">
		<tr>
			<td style="padding:0;">
				<fsn:FSNPageHeader ID="FSNPageHeader" runat="server">
				</fsn:FSNPageHeader>
			</td>
		</tr>
		<tr>
			<td style="padding:0;">
				&nbsp;
			</td>
		</tr>
	</table>
	</asp:Panel>
	
	<asp:Panel ID="pnlParametros" runat="server" Width="100%">
		<table style="padding-top: 4px; padding-bottom: 4px; padding-left: 10px;width:98%;" border="0">
		<tr>
			<td style="width:300px;">
				<asp:Label ID="lblEtiqMaterial" runat="server" CssClass="Etiqueta">DMaterial</asp:Label>
			</td>        
			<td>
				<asp:Label ID="lblMaterial" runat="server">DMaterial</asp:Label>
			</td>    
			<td>&nbsp;</td>        
		</tr>    
		<tr>
			<td colspan="3">
				&nbsp;
			</td>
		</tr>         
		<tr style="margin-top:5px;">
			<td style="width:300px;">
				<asp:Label ID="lblUnidadNegocio" runat="server" CssClass="Etiqueta">DUnidad</asp:Label>
			</td>
			<td style="width:1000px;">
				<asp:Label ID="lblUnicoUnidadQA" runat="server" Font-Bold="true">DUnidad</asp:Label>
				<ig:WebDropDown ID="wddUnidadNegocio" runat="server" Width="448px" DropDownContainerWidth="448px" DropDownAnimationDuration="1" EnableDropDownAsChild="false" EnableCustomValues="false" 
					EnableClosingDropDownOnBlur="true" EnableClosingDropDownOnSelect="true" CurrentValue="">
					<ClientEvents SelectionChanged="SelectionChanged" />
				</ig:WebDropDown>
			</td>
			<td>&nbsp;</td>
		</tr>   
        <tr style="margin-top:5px;">
            <td style="width:300px;">
                <asp:Label ID="lblProveedor" runat="server" Text="DProveedor" CssClass="Etiqueta"></asp:Label>
            </td>
            <td style="width:1000px;">                
                <table style="background-color: White; width: 448px; border: solid 1px #BBBBBB;border-collapse:collapse;border-spacing: 0;">
                    <tr>
                        <td style="padding:0;">
                            <asp:TextBox ID="txtProveedor" runat="server" Width="400px" BorderWidth="0px" BackColor="White" Height="16px" onchange="txtProveedor_TextChanged(this);"
                                onkeyup="txtProveedor_KeyUp(this);SetProveContextKey();" onblur="ResetProveContextKey();"></asp:TextBox>
                            <ajx:AutoCompleteExtender ID="txtProveedor_AutoCompleteExtender" runat="server" CompletionSetCount="10" UseContextKey="true" 
                                DelimiterCharacters="" Enabled="True" MinimumPrefixLength="1" ServiceMethod="GetProveedoresObjSuelos" CompletionListElementID="divAutoCompleteList"
                                ServicePath="~/App_Pages/_Common/App_Services/AutoComplete.asmx" TargetControlID="txtProveedor" EnableCaching="False" OnClientItemSelected="selected_Proveedor">                            
                            </ajx:AutoCompleteExtender>
                            <div id="divAutoCompleteList"></div>
                        </td>
                        <td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px; cursor:pointer;">
                            <img ID="imgProveedorLupa" runat="server" src="Img_Ico_Lupa.gif" onclick="AbrirBuscadorProveedor();"/> 
                        </td>
                    </tr>
                    <asp:HiddenField ID="hidProveedor" runat="server" /> 
                    <asp:HiddenField ID="hdenProveedor" runat="server" />                            
                </table>                    
            </td>
            <td>&nbsp;</td>
        </tr>            
		<tr>
			<td colspan="3" align="right">                
				<table>
					<tr>
						<td>
							<asp:ImageButton ID="ibExcel" runat="server" SkinID="Excel" OnClientClick="MostrarExcel();return false;"
								AlternateText="Excel" />
						</td>
						<td>
							<asp:ImageButton ID="ibPdf" runat="server" SkinID="Pdf" OnClientClick="MostrarPdf();return false;"
								AlternateText="Pdf" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		</table>
	</asp:Panel>    	

	<div class="BusqProve">
		<asp:UpdatePanel ID="UpdDatos" ChildrenAsTriggers="false" EnableViewState="false" runat="server" UpdateMode="Conditional">
			<ContentTemplate>               
				<ig:WebDataGrid ID="wdgDatos" runat="server" Width="99%" Height="290px" AutoGenerateColumns="false" 
				CellSpacing="1" DataKeyFields="OBJSUELO_ID" EnableDataViewState="true"  >            
				<Columns>               
					<ig:BoundDataField DataFieldName="OBJSUELO_ID" Key="OBJSUELO_ID" Hidden="true"></ig:BoundDataField>
					<ig:BoundDataField DataFieldName="MATERIAL_QA" Key="MATERIAL_QA" Hidden="true"></ig:BoundDataField>
					<ig:BoundDataField DataFieldName="TIPO" Key="TIPO" Hidden="true"></ig:BoundDataField>
					<ig:BoundDataField DataFieldName="SUBTIPO" Key="SUBTIPO" Hidden="true"></ig:BoundDataField>                    
                    <ig:BoundDataField DataFieldName="PROVE" Key="PROVE" Hidden="true"></ig:BoundDataField>
					<ig:BoundDataField DataFieldName="DEN" Key="DEN" Width="70%" CssClass="AsignarObjDen"></ig:BoundDataField>
					<ig:BoundDataField DataFieldName="OBJETIVO" Key="OBJETIVO" Width="15%" CssClass="AsignarObjNum itemRightAligment"></ig:BoundDataField>
					<ig:BoundDataField DataFieldName="SUELO" Key="SUELO" Width="15%" CssClass="AsignarObjNum itemRightAligment"></ig:BoundDataField>                       
				</Columns>
				<EditorProviders>
					<ig:NumericEditorProvider ID="NumericEditor">
						<EditorControl DataMode="Double">
							<ClientEvents TextChanged ="ChangeValor"/> 
						</EditorControl>                    
					</ig:NumericEditorProvider>
				</EditorProviders>
				<Behaviors>
					<ig:Selection Enabled="true" CellClickAction="Cell" CellSelectType="Single"></ig:Selection>
					<ig:EditingCore Enabled="true" AutoCRUD="False">     
						<Behaviors>                                     
							<ig:CellEditing>
								<ColumnSettings>
									<ig:EditingColumnSetting ColumnKey="DEN" ReadOnly="true" />
									<ig:EditingColumnSetting ColumnKey="OBJETIVO" EditorID="NumericEditor" />
									<ig:EditingColumnSetting ColumnKey="SUELO" EditorID="NumericEditor" />
									<ig:EditingColumnSetting ColumnKey="OBJSUELO_ID" ReadOnly="True" />
									<ig:EditingColumnSetting ColumnKey="MATERIAL_QA" ReadOnly="True" />
									<ig:EditingColumnSetting ColumnKey="TIPO" ReadOnly="True" />
									<ig:EditingColumnSetting ColumnKey="SUBTIPO" ReadOnly="True" />
                                    <ig:EditingColumnSetting ColumnKey="PROVE" ReadOnly="True" />
								</ColumnSettings>                        
								<CellEditingClientEvents ExitedEditMode="ugExitedEditMode" />
								<EditModeActions EnableF2="False" MouseClick="Single" />
							</ig:CellEditing>
						</Behaviors>  
					</ig:EditingCore>                    
					<ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
				</Behaviors>
			</ig:WebDataGrid>
			</ContentTemplate>
		</asp:UpdatePanel>             
	</div>


	<table style="width:100%;">
		<tr>
			<td style="width:50%;">
				<fsn:FSNButton ID="btnAceptar" runat="server" Text="DAceptar" OnClientClick="document.getElementById('boton').value = 1; return true;">                    
				&nbsp;&nbsp;&nbsp;&nbsp;                    
				</fsn:FSNButton>
			</td>
			<td>
				<fsn:FSNButton ID="btnCancelar" runat="server" Text="DCancelar" Alineacion="Left" OnClientClick="return Cancelar();">                    
				&nbsp;&nbsp;&nbsp;&nbsp;                    
				</fsn:FSNButton>                        
			</td>
		</tr>
	</table>    
	   
	<ig:WebDocumentExporter ID="WebGridDocumentExporter1" runat="server"></ig:WebDocumentExporter>
	<ig:WebExcelExporter ID="WebGridExcelExporter1" runat="server"></ig:WebExcelExporter>
	
	<asp:HiddenField ID="boton" runat="server" />
	<asp:HiddenField ID="recargar" runat="server" />
	<asp:HiddenField ID="unidades" runat="server" />
    <asp:HiddenField ID="proveedor" runat="server" />
	<asp:HiddenField ID="gridmodificado" runat="server" />
	<asp:HiddenField ID="ComboQA" runat="server" />    
	<asp:HiddenField ID="PosComboQA" runat="server" />
    <asp:HiddenField ID="CodProveedor" runat="server" />
	<asp:HiddenField ID="UnicoUnidadQA" runat="server" />    
	<asp:HiddenField ID="hid_Cambios" runat="server" />
	</form>
</body>
</html>
