﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/EnBlanco.Master" CodeBehind="asignarUnidadesOrganizativas.aspx.vb" Inherits="Fullstep.FSNWeb.asignarUnidadesOrganizativas" %>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    <link rel="stylesheet" href="<%=ResolveClientUrl("~/js/jquery/plugins/styles/style.min.css")%>" type="text/css" />
    <script type="text/javascript" lang="javascript">
        $(document).ready(function () {            
            var datosUONsTree = $.parseJSON(datosUONs);           
            $('#tvUONs').jstree({                
                'core': {
                    'data': datosUONsTree,
                    'themes': { 'dots': false }
                },
                'plugins': ['checkbox'],
                checkbox: {
                    three_state: false
                }
            }).on('select_node.jstree', function (e, data) {                
                disableParents(data.node);
                disableChildren(data.node);
            }).on('deselect_node.jstree', function (e, data) {                
                enableParents(data.node);
                enableChildren(data.node);                
            }).on('open_node.jstree', function (e, data) {                
                if (data.node.state.selected || data.node.state.disabled) {
                    if (document.getElementById(data.node.id + '_anchor')) {
                        var s = document.getElementById(data.node.id + '_anchor').className;
                        if (data.node.state.disabled && !s.indexOf(' jstree-undetermined') >= 0) document.getElementById(data.node.id + '_anchor').children[0].className += ' jstree-undetermined';
                    }

                    $.each(data.node.children, function () {
                        //cambiar icono   
                        var oNodo = $('#tvUONs').jstree('get_node', this);
                        if (oNodo.state.disabled) document.getElementById(this + '_anchor').children[0].className += ' jstree-undetermined';
                        if (oNodo.state.opened && oNodo.children) {
                            $.each(oNodo.children, function () {
                                if ($('#tvUONs').jstree('get_node', this).state.disabled) document.getElementById(this + '_anchor').children[0].className += ' jstree-undetermined';
                            });
                        }
                    });
                }
            });
        });
        //<summary>Deshabilita los nodos padres de un nodo</summary>
        //<param name="oNodo">Nodo cuyos padres hay que deshabilitar</param>
        function disableParents(oNodo) {
            //En el array parents vienen los padres de todos los niveles superiores
            $.each(oNodo.parents, function () {
                if (!$('#tvUONs').jstree('get_node', this).state.disabled && document.getElementById(this + '_anchor')) document.getElementById(this + '_anchor').children[0].className += ' jstree-undetermined';
                $('#tvUONs').jstree('disable_node', $('#tvUONs').jstree('get_node', this));                
            });
        }
        //<summary>Deshabilita los nodos hijos de un nodo</summary>
        //<param name="oNodo">Nodo cuyos hijos hay que deshabilitar</param>
        function disableChildren(oNodo) {            
            $.each(oNodo.children, function () {
                //cambiar icono (el elemento puede no existir porque no se ha desplegado el árbol)
                var oElem = document.getElementById(this + '_anchor');
                if (oElem) oElem.children[0].className += ' jstree-undetermined';

                $('#tvUONs').jstree('disable_node', $('#tvUONs').jstree('get_node', this));
                disableChildren($('#tvUONs').jstree('get_node', this));
            });
        }
        //<summary>Habilita los nodos padres de un nodo</summary>
        //<param name="oNodo">Nodo cuyos padres hay que habilitar</param>
        function enableParents(oNodo) {
            //En el array parents vienen los padres de todos los niveles superiores
            $.each(oNodo.parents, function () {
                if (this != '#') {
                    var oNodo = $('#tvUONs').jstree('get_node', this);
                    if (comprobarHabilitarNodo(oNodo)) {
                        document.getElementById(this + '_anchor').children[0].className=document.getElementById(this + '_anchor').children[0].className.replace(' jstree-undetermined', '');
                        $('#tvUONs').jstree('enable_node', oNodo);
                    }
                }
            });
        }
        //<summary>Habilita los nodos hijos de un nodo</summary>
        //<param name="oNodo">Nodo cuyos hijos hay que habilitar</param>
        function enableChildren(oNodo) {
            $.each(oNodo.children, function () {
                //cambiar icono (el elemento puede no existir porque no se ha desplegado el árbol)
                var oElem = document.getElementById(this + '_anchor');
                if (oElem) oElem.children[0].className = oElem.children[0].className.replace(' jstree-undetermined', '');

                $('#tvUONs').jstree('enable_node', $('#tvUONs').jstree('get_node', this));
                enableChildren($('#tvUONs').jstree('get_node', this));
            });
        }
        //<summary>Comprueba si se puede habilitar un nodo (puede estar deshabilitado por otros hijos)</summary>
        //<param name="oNodo">Nodo a comprobar</param>
        function comprobarHabilitarNodo(oNodo) {           
            var bEnabled = true;
            $.each(oNodo.children, function () {
                if ($('#tvUONs').jstree('get_node', this).state.selected) {
                    bEnabled = false;
                    return false;
                }
                else {
                    bEnabled = comprobarHabilitarNodo($('#tvUONs').jstree('get_node', this))
                }

                if (!bEnabled) return false;
            });

            return bEnabled
        }
        //<summary>Asignación de las UONs seleccionadas a la UNQA</summary>
        function asignarUnidadesOrganizativas() {           
            var UONs = $('#tvUONs').jstree('get_selected');

            var params = { UNQA: document.getElementById("<%=hidUNQAID.ClientID%>").value, UONs: UONs };
            $.when($.ajax({
                type: "POST",
                url: "<%=ConfigurationManager.AppSettings("RutaFS")%>QA/Configuracion/asignarUnidadesOrganizativas.aspx/AsignarUnidadesOrganizativas",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(params),
                dataType: "json",
                async: false
            })).done(function (msg) {
                window.close();
            });        
        }                        
        $('#cmdCancelar').live('click', function () { window.close(); });
        $('#cmdAceptar').live('click', function () { asignarUnidadesOrganizativas(); });
    </script>
    <asp:HiddenField ID="hidUNQAID" runat="server"/>	
	<asp:label id="lblTitulo" style="display:block; margin:1em 2em;" runat="server" CssClass="Texto14 TextoResaltado Negrita"> DAsignación de unidades organizativas</asp:label>        
    <asp:label id="lblUNQA" style="display:block; margin:2em 2.5em 0em;" runat="server" CssClass="Texto12 TextoResaltado Negrita">DUON</asp:label>&nbsp;
	<asp:Label id="lblSeleccionar" style="display:block; margin:0em 2.5em 0.5em;" runat="server" CssClass="Texto12">DSeleccione las unidades organizativas relacionadas</asp:Label>   
    <div id="tvUONs" class="bordes" style="height:300px; width:88%; margin:0em 2.5em; overflow:auto;"></div> 
    <div style="text-align:center; margin-top:1em;">
        <asp:Label runat="server" ID="cmdAceptar" CssClass="botonRedondeado" ClientIDMode="Static" style="margin-right:1em;"></asp:Label>
        <asp:Label runat="server" ID="cmdCancelar" CssClass="botonRedondeado" ClientIDMode="Static"></asp:Label>	
    </div>
</asp:Content>
