﻿Partial Public Class AsignarObjySuelos
    Inherits FSNPage

    Private m_oMaterialQA As FSNServer.MaterialQA

#Region "Carga Inicial"
    ''' Revisado por: Jbg. Fecha: 20/10/2011
    ''' <summary>
    ''' Carga la pagina
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:1seg.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AsignarObjetivosySuelos

        m_oMaterialQA = FSNServer.Get_Object(GetType(FSNServer.MaterialQA))
        m_oMaterialQA.ID = Request("Material")

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "confirmcancelar", "<script>var confirmcancelar ="" " & Textos(8) & " "" </script>")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "confirmsalir", "<script>var confirmsalir="" " & Textos(9) & " "" </script>")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "materialQAID", "<script>var materialQAID=" & m_oMaterialQA.ID & " </script>")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "usuCod", "<script>var usuCod=""" & FSNUser.Cod & """ </script>")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ruta", "<script>var ruta=""" & System.Configuration.ConfigurationManager.AppSettings("ruta") & """ </script>")

        imgProveedorLupa.Src = ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Img_Ico_Lupa.gif"

        CargarTextos()

        Dim field As Infragistics.Web.UI.GridControls.BoundDataField = Me.wdgDatos.Columns.FromKey("OBJETIVO")
        field.DataFormatString = "{0:N" & FSNUser.NumberFormat.NumberDecimalDigits.ToString & "}"

        field = Me.wdgDatos.Columns.FromKey("SUELO")
        field.DataFormatString = "{0:N" & FSNUser.NumberFormat.NumberDecimalDigits.ToString & "}"

        If Not IsPostBack Then
            hid_Cambios.Value = ""

            gridmodificado.Value = 0

            FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/filtro_materiales_peq.gif"

            If Application("Nivel_UnQa") Is Nothing Then
                FSNServer.Load_UnQa()
                Application("Nivel_UnQa") = FSNServer.NivelesUnQa
            End If

            Dim oUnidadesQA As FSNServer.UnidadesNeg = FSNServer.Get_Object(GetType(FSNServer.UnidadesNeg))
            oUnidadesQA.UsuLoadData(FSNUser.Cod, Me.Idioma, Application("Nivel_UnQa"), TipoAccesoUNQAS.ConsultarObjetivosSuelos, TipoAccesoUNQAS.MantenerObjetivosSuelos)

            Dim dt As DataTable = oUnidadesQA.MontaDataSetUnaBanda()

            If dt.Rows.Count = 0 Then
                ComboQA.Value = 0

                UnicoUnidadQA.Value = 0
                lblUnicoUnidadQA.Text = ""

                wddUnidadNegocio.Visible = False
                lblUnidadNegocio.Visible = False

                btnAceptar.Visible = False
            ElseIf dt.Rows.Count < 2 Then
                ComboQA.Value = 0

                UnicoUnidadQA.Value = Split(dt.Rows(0)("IDPERMISO"), "#")(0)
                lblUnicoUnidadQA.Text = dt.Rows(0)("DEN")

                wddUnidadNegocio.Visible = False
                lblUnidadNegocio.Visible = False

                If Split(dt.Rows(0)("IDPERMISO"), "#")(1) <> TipoAccesoUNQAS.MantenerObjetivosSuelos Then
                    btnAceptar.Visible = False
                Else
                    btnAceptar.Visible = True
                End If
            Else
                ComboQA.Value = 0
                lblUnicoUnidadQA.Visible = False

                wddUnidadNegocio.TextField = "DEN"
                wddUnidadNegocio.ValueField = "IDPERMISO"
                wddUnidadNegocio.DataSource = dt
                wddUnidadNegocio.DataBind()
                wddUnidadNegocio.SelectedItemIndex = 0

                ComboQA.Value = Split(wddUnidadNegocio.SelectedItem.Value, "#")(0)
                PosComboQA.Value = Me.wddUnidadNegocio.SelectedItemIndex

                If Split(wddUnidadNegocio.SelectedItem.Value, "#")(1) <> TipoAccesoUNQAS.MantenerObjetivosSuelos Then
                    btnAceptar.Visible = False
                Else
                    btnAceptar.Visible = True
                End If
            End If

            ObtenerDatosGrid()
            wdgDatos.DataSource = m_oMaterialQA.Data
            wdgDatos.DataBind()

        ElseIf Request("__EVENTARGUMENT") = "Excel" Then
            ibExcel_Click()
        ElseIf Request("__EVENTARGUMENT") = "Pdf" Then
            ibPdf_Click()
        Else
            ObtenerDatosGrid()
            wdgDatos.DataSource = m_oMaterialQA.Data
            wdgDatos.DataBind()
        End If

    End Sub

    ''' Revisado por: Jbg. Fecha: 20/10/2011
    ''' <summary>
    ''' Establece los textos de los controles de acuerdo con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo máximo:0</remarks> 
    Private Sub CargarTextos()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AsignarObjetivosySuelos

        FSNPageHeader.TituloCabecera = Textos(0)
        lblEtiqMaterial.Text = Textos(13)
        lblMaterial.Text = Request("DenMaterial")
        lblUnidadNegocio.Text = Textos(7)
        btnAceptar.Text = Textos(5)
        btnCancelar.Text = Textos(6)
        lblProveedor.Text = Textos(15) & ":"

        Dim field As Infragistics.Web.UI.GridControls.BoundDataField = wdgDatos.Columns.FromKey("DEN")
        field.Header.Text = Textos(2)
        field = wdgDatos.Columns.FromKey("OBJETIVO")
        field.Header.Text = Textos(3)
        field = wdgDatos.Columns.FromKey("SUELO")
        field.Header.Text = Textos(4)
    End Sub
#End Region

#Region "WebDropDown"
    ''' Revisado por: Jbg. Fecha: 20/10/2011
    ''' <summary>
    ''' Al cambiar la Unidad de negocio hay q recargar el grid. Antes de recargar en caso de q haya cambios en los 
    ''' objetivos o suelos hay q dar la posibilidad de grabar.
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>       
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:0,1seg.</remarks>
    Private Sub wddUnidadNegocio_ValueChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownValueChangedEventArgs) Handles wddUnidadNegocio.ValueChanged
        If recargar.Value = "1" OrElse recargar.Value = "perder" Then
            If Split(wddUnidadNegocio.SelectedItem.Value, "#")(1) <> TipoAccesoUNQAS.MantenerObjetivosSuelos Then
                btnAceptar.Visible = False
            Else
                btnAceptar.Visible = True
            End If

            gridmodificado.Value = 0
            unidades.Value = 0
            proveedor.Value = 0
            boton.Value = 0

            ComboQA.Value = Split(wddUnidadNegocio.SelectedItem.Value, "#")(0)

            'Si había un proveedor seleccionado se comprueba que es válido para la nueva UNQA
            If hidProveedor.Value <> "" Then
                Dim oProves As FSNServer.Proveedores = FSNServer.Get_Object(GetType(FSNServer.Proveedores))
                oProves.CargarProvObjSuelos(FSNUser.Cod, m_oMaterialQA.ID, ComboQA.Value, hidProveedor.Value)
                If oProves.Data.Tables(0).Rows.Count = 0 Then
                    txtProveedor.Text = ""
                    hidProveedor.Value = ""
                    CodProveedor.Value = ""
                End If
            End If

            If recargar.Value = "1" Then
                CargarDatosGrid()

                hid_Cambios.Value = ""
                recargar.Value = "0"
            End If
        End If
    End Sub
#End Region

#Region "txtProveedor"
    ''' <summary>Al cambiar el proveedor hay q recargar el grid. Antes de recargar en caso de q haya cambios en los objetivos o suelos hay q dar la posibilidad de grabar</summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>       
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:0,1seg.</remarks>
    Private Sub txtProveedor_TextChanged(sender As Object, e As EventArgs) Handles txtProveedor.TextChanged
        If recargar.Value = "1" OrElse recargar.Value = "perder" Then
            btnAceptar.Visible = True

            gridmodificado.Value = 0
            unidades.Value = 0
            proveedor.Value = 0
            boton.Value = 0

            'CodProveedor contiene el valor que se utiliza para la carga del grid
            CodProveedor.Value = hidProveedor.Value

            If recargar.Value = "1" Then
                CargarDatosGrid()

                hid_Cambios.Value = ""
                recargar.Value = "0"
            End If
        End If
    End Sub
#End Region

#Region "Botones"
    ''' Revisado por: Jbg. Fecha: 20/10/2011
    ''' <summary>
    ''' Se hace el guadado de los datos cuando el usuario acepta 
    ''' </summary>
    ''' <param name="sender">sistema</param>
    ''' <param name="e">sistema</param>
    ''' <remarks>Llamadas:  Al pulsar el boton aceptar;Tiempo: Con 20 tipos de noconformidades - 0,1seg</remarks>
    Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If gridmodificado.Value = 1 Then GuardarDatosGrid()
        gridmodificado.Value = 0
        recargar.Value = "0"
        unidades.Value = 0
        proveedor.Value = 0
        boton.Value = 0
        CargarDatosGrid()
        Me.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Close", "window.close();", True)
    End Sub

    ''' Revisado por: Jbg. Fecha: 20/10/2011
    ''' <summary>
    ''' Exporta el grid de datos en excel
    ''' </summary>
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:0,1seg.</remarks>
    Private Sub ibExcel_Click()
        ObtenerDatosGrid()
        wdgDatos.DataSource = m_oMaterialQA.Data
        wdgDatos.DataBind()

        Dim fileName As String = HttpUtility.UrlEncode(Textos(0))
        fileName = fileName.Replace("+", "%20")

        WebGridExcelExporter1.DownloadName = fileName
        WebGridExcelExporter1.DataExportMode = Infragistics.Web.UI.GridControls.DataExportMode.DataInGridOnly

        WebGridExcelExporter1.Export(wdgDatos)
    End Sub

    ''' Revisado por: Jbg. Fecha: 20/10/2011
    ''' <summary>
    ''' Exporta el grid de datos en pdf
    ''' </summary>
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:0,1seg.</remarks>
    Private Sub ibPdf_Click()
        ObtenerDatosGrid()
        wdgDatos.DataSource = m_oMaterialQA.Data
        wdgDatos.DataBind()

        Dim fileName As String = HttpUtility.UrlEncode(Textos(0))
        fileName = fileName.Replace("+", "%20")

        With WebGridDocumentExporter1
            .DownloadName = fileName
            .EnableStylesExport = True

            .DataExportMode = Infragistics.Web.UI.GridControls.DataExportMode.DataInGridOnly
            .TargetPaperOrientation = Infragistics.Documents.Reports.Report.PageOrientation.Landscape

            .Margins = Infragistics.Documents.Reports.Report.PageMargins.GetPageMargins("Narrow")
            .TargetPaperSize = Infragistics.Documents.Reports.Report.PageSizes.GetPageSize("A4")
            .Export(wdgDatos)
        End With
    End Sub
#End Region

#Region "Fns Auxiliares"
    ''' Revisado por: Jbg. Fecha: 20/10/2011
    ''' <summary>Carga los datos por material y unidad de negocio</summary>
    ''' <remarks>Llamadas:Page_Load ibExcel_Click  ibPdf_Click CargarDatosGrid ;Tiempo:  Con 20 tipo de no conformidades - 0,1 seg</remarks>
    Private Sub ObtenerDatosGrid()
        If wddUnidadNegocio.Visible Then
            m_oMaterialQA.MaterialObjetivoySuelos(ComboQA.Value, CodProveedor.Value, Me.Idioma)
        Else
            m_oMaterialQA.MaterialObjetivoySuelos(UnicoUnidadQA.Value, CodProveedor.Value, Me.Idioma)
        End If
    End Sub

    ''' Revisado por: Jbg. Fecha: 20/10/2011
    ''' <summary>
    ''' Carga los datos de la grid por material y unidad de negocio
    ''' </summary>
    ''' <remarks>Llamadas:  Page_Load,cmdAceptar_click,UniNeg_SelectedIndexChanged;Tiempo:  Con 20 tipo de no conformidades - 0,1 seg</remarks>
    Private Sub CargarDatosGrid()
        hid_Cambios.Value = ""

        ObtenerDatosGrid()
        wdgDatos.DataSource = m_oMaterialQA.Data
        wdgDatos.DataBind()

        UpdDatos.Update()
    End Sub

    ''' Revisado por: Jbg. Fecha: 20/10/2011
    ''' <summary>
    ''' Guarda los datos de la grid por material y unidad de negocio
    ''' </summary>
    ''' <remarks>Page_Load (caso especial de salida), cmdAceptar_click,niNeg_SelectedIndexChanged;Tiempo:  Con 20 tipo de no conformidades - 0,1 seg</remarks>
    Private Sub GuardarDatosGrid()
        Dim dtNewObjSuelos As DataTable
        Dim keysNew(3) As DataColumn
        Dim findNew(3) As Object

        If txtProveedor.Text <> String.Empty Then
            ReDim keysNew(4)
            ReDim findNew(4)
        End If

        Dim DS As New DataSet
        dtNewObjSuelos = DS.Tables.Add("TEMP_NEW_OBJSUELOS")
        With dtNewObjSuelos
            .Columns.Add("MATERIAL_QA", System.Type.GetType("System.Int32"))
            .Columns.Add("TIPO", System.Type.GetType("System.Int32"))
            .Columns.Add("SUBTIPO", System.Type.GetType("System.Int32"))
            .Columns("SUBTIPO").AllowDBNull = True
            .Columns.Add("OBJETIVO", System.Type.GetType("System.Double"))
            .Columns("OBJETIVO").AllowDBNull = True
            .Columns.Add("SUELO", System.Type.GetType("System.Double"))
            .Columns("SUELO").AllowDBNull = True
            .Columns.Add("UNQA", System.Type.GetType("System.Int32"))
            .Columns.Add("USU", System.Type.GetType("System.String"))
            .Columns.Add("PROVE", System.Type.GetType("System.String"))
            If txtProveedor.Text = String.Empty Then .Columns("PROVE").AllowDBNull = True

            keysNew(0) = .Columns("MATERIAL_QA")
            keysNew(1) = .Columns("TIPO")
            keysNew(2) = .Columns("SUBTIPO")
            keysNew(3) = .Columns("UNQA")
            If txtProveedor.Text <> String.Empty Then keysNew(4) = .Columns("PROVE")

            .PrimaryKey = keysNew
        End With

        For Each oRow As Infragistics.Web.UI.GridControls.GridRecord In wdgDatos.Rows
            findNew(0) = oRow.Items.Item(1).Value
            findNew(1) = oRow.Items.Item(2).Value
            findNew(2) = oRow.Items.Item(3).Value
            If wddUnidadNegocio.Visible Then
                findNew(3) = ComboQA.Value
            Else
                findNew(3) = UnicoUnidadQA.Value
            End If
            If txtProveedor.Text <> String.Empty Then findNew(4) = hidProveedor.Value

            Dim dtNewRow As DataRow = dtNewObjSuelos.Rows.Find(findNew)
            If dtNewRow Is Nothing Then dtNewRow = dtNewObjSuelos.NewRow

            dtNewRow.Item("MATERIAL_QA") = oRow.Items.Item(wdgDatos.columns("MATERIAL_QA").index).Value
            dtNewRow.Item("TIPO") = oRow.Items.Item(wdgDatos.columns("TIPO").index).Value
            dtNewRow.Item("SUBTIPO") = oRow.Items.Item(wdgDatos.columns("SUBTIPO").index).Value
            dtNewRow.Item("OBJETIVO") = NothingToDBNull(oRow.Items.Item(wdgDatos.columns("OBJETIVO").index).Value)
            dtNewRow.Item("SUELO") = NothingToDBNull(oRow.Items.Item(wdgDatos.columns("SUELO").index).Value)
            If wddUnidadNegocio.Visible Then
                dtNewRow.Item("UNQA") = CInt(ComboQA.Value)
            Else
                dtNewRow.Item("UNQA") = CInt(UnicoUnidadQA.Value)
            End If
            If txtProveedor.Text <> String.Empty Then dtNewRow.Item("PROVE") = hidProveedor.Value
            dtNewRow.Item("USU") = FSNUser.Cod

            If dtNewRow.RowState = DataRowState.Detached Then dtNewObjSuelos.Rows.Add(dtNewRow)
        Next

        m_oMaterialQA.MaterialQAActualizarObjetivosySuelos(DS)
    End Sub

    ''' Revisado por: Jbg. Fecha: 20/10/2011
    ''' <summary>
    ''' Guarda los datos de una linea del grid por material y unidad de negocio
    ''' </summary>
    ''' <remarks>Page_Load (caso especial de salida), cmdAceptar_click,niNeg_SelectedIndexChanged;Tiempo:  Con 20 tipo de no conformidades - 0,1 seg</remarks>
    Private Sub GuardarUnDatoGrid(ByVal Linea As System.Collections.Hashtable)
        Dim dtNewObjSuelos As DataTable
        Dim keysNew(3) As DataColumn
        Dim findNew(3) As Object

        If txtProveedor.Text <> String.Empty Then
            ReDim keysNew(4)
            ReDim findNew(4)
        End If

        Dim DS As New DataSet
        dtNewObjSuelos = DS.Tables.Add("TEMP_NEW_OBJSUELOS")
        With dtNewObjSuelos
            .Columns.Add("MATERIAL_QA", System.Type.GetType("System.Int32"))
            .Columns.Add("TIPO", System.Type.GetType("System.Int32"))
            .Columns.Add("SUBTIPO", System.Type.GetType("System.Int32"))
            .Columns("SUBTIPO").AllowDBNull = True
            .Columns.Add("OBJETIVO", System.Type.GetType("System.Double"))
            .Columns("OBJETIVO").AllowDBNull = True
            .Columns.Add("SUELO", System.Type.GetType("System.Double"))
            .Columns("SUELO").AllowDBNull = True
            .Columns.Add("UNQA", System.Type.GetType("System.Int32"))
            .Columns.Add("USU", System.Type.GetType("System.String"))
            .Columns.Add("PROVE", System.Type.GetType("System.String"))
            If txtProveedor.Text = String.Empty Then .Columns("PROVE").AllowDBNull = True

            Dim dtNewRow As DataRow = .NewRow

            dtNewRow.Item("MATERIAL_QA") = Linea("MATERIAL_QA")
            dtNewRow.Item("TIPO") = Linea("TIPO")
            dtNewRow.Item("SUBTIPO") = Linea("SUBTIPO")
            dtNewRow.Item("OBJETIVO") = NothingToDBNull(Linea("OBJETIVO"))
            dtNewRow.Item("SUELO") = NothingToDBNull(Linea("SUELO"))
            If wddUnidadNegocio.Visible Then
                dtNewRow.Item("UNQA") = CInt(ComboQA.Value)
            Else
                dtNewRow.Item("UNQA") = CInt(UnicoUnidadQA.Value)
            End If
            If txtProveedor.Text <> String.Empty Then dtNewRow.Item("PROVE") = hidProveedor.Value
            dtNewRow.Item("USU") = FSNUser.Cod

            .Rows.Add(dtNewRow)
        End With

        m_oMaterialQA.MaterialQAActualizarObjetivosySuelos(DS)
    End Sub
#End Region

#Region "Grid"
    ''' Revisado por: Jbg. Fecha: 20/10/2011
    ''' <summary>
    ''' Al cambiar la Unidad de negocio hay q recargar el grid. Antes de recargar en caso de q haya cambios en los 
    ''' objetivos o suelos, si el usuario así lo indica, hay q grabar los cambios.
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>       
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:0,1seg.</remarks>
    Private Sub wdgDatos_RowUpdating(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowUpdatingEventArgs) Handles wdgDatos.RowUpdating
        If Request("recargar") = "perder" Then
            e.Cancel = True

            hid_Cambios.Value = Replace(hid_Cambios.Value, "@" & e.RowID("key")(0) & "@", "")

            If hid_Cambios.Value = "" Then
                CargarDatosGrid()

                wdgDatos.RequestFullAsyncRender()

                CargarTextos()

                Dim field As Infragistics.Web.UI.GridControls.BoundDataField = wdgDatos.Columns.FromKey("OBJETIVO")
                field.DataFormatString = "{0:N" & FSNUser.NumberFormat.NumberDecimalDigits.ToString & "}"

                field = wdgDatos.Columns.FromKey("SUELO")
                field.DataFormatString = "{0:N" & FSNUser.NumberFormat.NumberDecimalDigits.ToString & "}"

            End If

        ElseIf Request("recargar") = "salida" Then
            'Ha hecho cambios y ha cambiado la combo
            GuardarUnDatoGrid(e.Values)

            gridmodificado.Value = 0
        End If
    End Sub

#End Region
End Class