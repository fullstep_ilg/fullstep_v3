﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="ConfigNivelEscalacionProves.aspx.vb" Inherits="Fullstep.FSNWeb.ConfigNivelEscalacionProves" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../../js/jquery/plugins/styles/fsHierarchicalDropDown.css" rel="stylesheet" />
    <link href="../../../js/jquery/plugins/styles/fsMultiLanguage.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">    
    <fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
    <div class="Bordear" style="margin:1em; padding-bottom:2em;">
        <div class="Texto12 ColorFondoTab SeparadorInf" style="padding:0.5em; margin-bottom:1em;">
            <select id="cboNivelEscalacion" class="Negrita"></select>            
        </div>
        <ul style="font-size:0pt; margin:0em; padding:0em;">
            <li class="Texto12" style="display:inline-block; width:50%; vertical-align:top;">
                <div style="margin-left:0.5em;">
                    <asp:Label runat="server" ID="lblDenominacion" style="display:inline-block; width:10em; line-height:2em;"></asp:Label>
                    <div id="txtDenominacionMultiidioma" style="width:80%;"></div>
                </div>
                <div style="margin-top:0.5em; margin-left:0.5em;">
                    <asp:Label runat="server" ID="lblDescripcion" style="display:inline-block; min-width:10em; width:10%; line-height:2em;"></asp:Label>
                    <div id="txtDescripcionMultiidioma" style="display:inline-block; width:80%;"></div>
                </div>                
            </li>
            <li class="Texto12" style="display:inline-block; width:50%; vertical-align:top;"> 
                <div style="line-height:2em; display:inline-block;">
                    <asp:Label runat="server" ID="lblNivelEstrucUniNeg" style="display:inline-block;"></asp:Label>
                    <select id="cboNivelEstrucUniNeg" style="width:15em;"></select>        
                    <asp:Label runat="server" ID="lblNoConformidadEscalacion" style="display:block;"></asp:Label>
                    <select id="cboTiposNoConformidad" style="display:block; margin-top:0.2em; margin-bottom:0.5em; width:100%;"></select>
                    <input type="checkbox" id="chkBloqueoSelProveAdj" />
                    <label for="chkBloqueoSelProveAdj" id="lblBloqueoSelProveAdj"></label>
                </div>               
            </li>
        </ul>
    </div>    
</asp:Content>