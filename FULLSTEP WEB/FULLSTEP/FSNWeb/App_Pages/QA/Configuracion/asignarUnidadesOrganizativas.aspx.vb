﻿Public Class asignarUnidadesOrganizativas
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CargarTextos()
        CargarUnidadesOrganizativas()
    End Sub
    ''' <summary>Carga los textos de la página</summary>
    ''' <remarks>Llamada desde: Page_Load</remarks>
    Private Sub CargarTextos()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AsignarUnidadesOrganizativas

        lblTitulo.Text = Textos(0)
        lblSeleccionar.Text = Textos(1)

        cmdAceptar.Text = Textos(2)
        cmdCancelar.Text = Textos(3)
        lblUNQA.Text = Request("DenUNQA")
        hidUNQAID.Value = Request("IdUNQA")
    End Sub
    ''' <summary>Carga el árbol de unidades organizativas</summary>
    ''' <remarks>Llamada desde: Page_Load</remarks>
    Private Sub CargarUnidadesOrganizativas()
        Dim oUNQA As FSNServer.UnidadNeg = FSNServer.Get_Object(GetType(FSNServer.UnidadNeg))
        oUNQA.Id = Request("IdUNQA")
        Dim oUONs As List(Of FSNServer.jsTreeNode) = oUNQA.DevolverUnidadesOrganizativas()

        Dim serializer As New Script.Serialization.JavaScriptSerializer
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "datosUONs") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "datosUONs", "var datosUONs = '" & serializer.Serialize(oUONs) & "';", True)
        End If
    End Sub
    ''' <summary>Asigna unidades organizativas a una unidad de negocio</summary>
    ''' <param name="UNQA">Id de la unidad de negocio</param>
    ''' <param name="UONs">Códigos de las UONs asociadas</param>
    ''' <remarks>Llamadad desde asignarUnidadesOrganizativas.aspx</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Sub AsignarUnidadesOrganizativas(ByVal UNQA As Integer, ByVal UONs As String())
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim oUNQA As Fullstep.FSNServer.UnidadNeg = FSNServer.Get_Object(GetType(FSNServer.UnidadNeg))

        oUNQA.Id = UNQA
        oUNQA.AsociarUnidadesOrganizativas(UONs)
    End Sub
End Class