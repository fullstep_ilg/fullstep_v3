﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DetallePuntuacionPpmCargoTasa.aspx.vb" Inherits="Fullstep.FSNWeb.DetallePuntuacionPpmCargoTasa" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head  id="Head1" runat="server">
    <title></title>
</head>
<body class="FondoTablaDetallePuntuacion">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>    
                        
        <table style="position:absolute;z-index:10002; width:50%">
            <tr>
                <td valign="top" align="left" style="padding:0px;width:80px;height:55px">
                    <asp:Image ID="imgDetalle" runat="server" SkinID="PopUpProveedor" Visible="true" />
                </td>
            </tr>                
        </table>  

        <asp:Panel ID="Panel1" runat="server">                
            <table style="position:relative;z-index:10001;width:100%;" cellpadding="0" cellspacing="0">
            <tr>
                <td valign="middle" align="left" style="width:75%;padding-left:80px;height:30px;"  class='CapaTituloPanelInfo'>
                    <div style='width:100%; height:30px;'>                        
                        <asp:Label runat="server" ID="lblVarCodDen" Text="DlblVarCodDen" CssClass="TituloPopUpPanelInfo"></asp:Label> 
                    </div>
                </td>
                <td valign="middle" style="text-align:right; width:25%"  class='CapaTituloPanelInfo'>
                    <asp:ImageButton ID="btnImprimir" runat="server" SkinID="Imprimirph" OnClientClick="window.print();return false;"/>
                    <a runat="server" id="lblImprimir" class="TextoBlanco" href="javascript:window.print();">DImprimir</a>
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="middle" align="left" style="padding-left:80px;height:25px;" class='CapaSubTituloPanelInfo'>
                    <asp:Label runat="server" ID="lblDetalle" Text="dDetalle de puntuación" CssClass="SubTituloPopUpPanelInfo"></asp:Label>                    
                </td>                
            </tr>
            <tr>
                <td colspan="2" class="SeparadorCabeceraDetallePuntuacion"> &nbsp;</td>
            </tr>   
            </table>     
            <div style="height:98%;">   
                <table style="position:relative;z-index:10001;width:100%;" cellpadding="0" cellspacing="0">          
                    <tr>
                        <td colspan="2" class="FondoTablaDetallePuntuacion">
                            <div>
                                <asp:Label ID="lblDetPuntProve" runat ="server" Text ="DDetalle de puntuación del proveedor" CssClass="RotuloDetallePuntuacion"></asp:Label>
                                <asp:Label ID="lblDetPuntProveDat" runat ="server" Text ="DProvecod - proveden" CssClass="RotuloDetallePuntuacionBold"></asp:Label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="FondoTablaDetallePuntuacion">
                            <div>
                                <asp:Label ID="lblEnUni" runat ="server" Text ="Den la unidad de negocio" CssClass="RotuloDetallePuntuacion"></asp:Label>
                                <asp:Label ID="lblEnUniDat" runat ="server" Text ="DProvecod - proveden" CssClass="RotuloDetallePuntuacionBold"></asp:Label>
                            </div>
                        </td>
                    </tr>    
                    <tr>
                        <td colspan="2" class="SeparadorLineasDetallePuntuacion"> &nbsp;</td>
                    </tr> 
                    <tr>
                        <td colspan="2" class="FondoTablaDetallePuntuacion">
                            <table class="BordearDetallePuntuacion">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPunt" runat ="server" Text ="DPuntuacion" CssClass="RotuloDetallePuntuacionBoldLeft"></asp:Label>
                                    </td>
                                    <td style="width:3px">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPuntDat" runat ="server" Text ="DPuntuacion (Calificacion)" CssClass="RotuloDetallePuntuacionPuntCalf"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>            
                    <tr>
                        <td colspan="2" class="SeparadorLineasDetallePuntuacion"> &nbsp;</td>
                    </tr>       
                    <tr>
                        <td colspan="2" class="FondoTablaDetallePuntuacion">
                            <table style="width:95%;" class="FondoGrisTablaDetallePuntuacion">
                            <tr>
                            <td class="RotuloDetallePuntuacionFondoGris" >
                                <asp:Panel id="pnlPadre" runat="server" ScrollBars="None">
                                    <table id="tblPadre" runat="server" border="0" style="WIDTH: 100%; height:12px;" cellpadding="0" cellspacing="0">
                                    </table>
                                </asp:Panel>            
                            </td>
                            </tr>            
                            <tr>
                                <td class="RotuloDetallePuntuacionFondoGris">
                                    <div>
                                        <asp:Label ID="lblMatQa" runat ="server" Text ="DMaterial Qa" CssClass="RotuloDetallePuntuacionFondoGrisBold"></asp:Label>
                                        <asp:Label ID="lblMatQaDat" runat ="server" Text ="DDen Material Qa"></asp:Label>
                                    </div>
                                </td>
                            </tr>            
                            <tr>
                                <td class="RotuloDetallePuntuacionFondoGris">
                                    <div>
                                        <asp:Label ID="lblPeriodo" runat ="server" Text ="DPeriodo" CssClass="RotuloDetallePuntuacionFondoGrisBold"></asp:Label>
                                        <asp:Label ID="lblPeriodoDat" runat ="server" Text ="D6 meses"></asp:Label>
                                    </div>
                                </td>
                            </tr>      
                            </table>            
                        </td>   
                    </tr>                                       
                    <tr>
                        <td colspan="2" class="SeparadorLineasDetallePuntuacion"> &nbsp;</td>
                    </tr>   
                    <tr>
                        <td colspan="2" class="FondoTablaDetallePuntuacion">
                            <table style="width:95%;" class="FondoGrisTablaDetallePuntuacion">
                                <tr>            
                                    <td class="RotuloDetallePuntuacionFondoGrisBold">
                                        <asp:Label runat="server" ID="lblFormula" Text="dFormula:"></asp:Label>
                                    </td>
                                </tr>          
                                <tr>
                                    <td class="RotuloDetallePuntuacionFondoGrisPad" style="overflow:auto">
                                        <asp:Label runat="server" ID="lblFormulaTexto" Text="dFormulaCompleta"></asp:Label>
                                    </td>
                                </tr>    
                            </table>
                        </td>
                    </tr>                 
                    <tr>
                        <td colspan="2" class="SeparadorLineasDetallePuntuacion"> &nbsp;</td>
                    </tr>      
                    <tr>
                        <td colspan="2" class="FondoTablaDetallePuntuacion">
                            <table class="FondoGrisBorderTablaDetallePuntuacion">
                                <tr>
                                    <td>
                                        <asp:Panel id="panelX" runat="server" ScrollBars="None">
                                            <table id="tblVarsX" runat="server" style="WIDTH: 400px;" cellpadding="0" cellspacing="0"></table>
                                        </asp:Panel>
                                    </td>     
                               </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="SeparadorLineasDetallePuntuacion"> &nbsp;</td>
                    </tr>                 
                    <tr>                  
                        <td colspan="2" class="FondoTablaDetallePuntuacionMargin">	
                            <ig:WebHierarchicalDataGrid runat="server" ID="whdgDatos" SkinID="DetallePuntuacionPpmCargoTasa"
                                 Height="150px" Width="95%" ShowFooter="false"
                                AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="false" >
                                <Behaviors>
                                    <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
						            <ig:ColumnMoving Enabled="true"></ig:ColumnMoving>
                                    <ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting>  
                                </Behaviors>                                
                            </ig:WebHierarchicalDataGrid>
                            <div id="div_Label" runat="server" class="EtiquetaScroll" style="display:none;position:absolute">
                                <asp:Label ID="label2" runat="server" Font-Bold="true"></asp:Label>
                            </div>                              
                        </td>
                    </tr>                       
                </table>
            </div>         
        </asp:Panel>        
    </form>
</body>
</html>
