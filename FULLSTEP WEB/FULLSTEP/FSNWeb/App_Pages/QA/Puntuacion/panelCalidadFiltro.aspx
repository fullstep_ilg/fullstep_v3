﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="panelCalidadFiltro.aspx.vb"
    Inherits="Fullstep.FSNWeb.panelCalidadFiltro" MasterPageFile="~/App_Master/Menu.Master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    <link rel="stylesheet" href="<%=ResolveClientUrl("~/js/jquery/plugins/styles/token-input.css")%>"
        type="text/css" />
    <link rel="stylesheet" href="<%=ResolveClientUrl("~/js/jquery/plugins/styles/token-input-facebook.css")%>"
        type="text/css" />
    <link rel="stylesheet" href="<%=ResolveClientUrl("~/js/jquery/plugins/styles/style.min.css")%>"
        type="text/css" />
    <style>
        ul.token-input-list-facebook, div.token-input-dropdown-facebook, .materiales
        {
            width: 520px;
        }
        li.token-input-selected-token-facebook, li.token-input-token-facebook
        {
            border: 1px solid #ccd5e4;
            background-color: #eff2f7;
            color: #000;
        }
        
        #tvMaterialesQA
        {
            background-color: #FFF;
            border-color: #FFF;
            height: 200px;
            overflow-y: scroll;
        }
    </style>
    <div style="margin-left: 10px;">
        <asp:Label ID="lblTitulo" runat="server" CssClass="Rotulo" Font-Size="14px"></asp:Label></div>
    <table cellpadding="6" style="width: 100%;" border="0">
        <tr>
            <td style="width: 50%" valign="top">
                <div id="dContenedorMaterialesQA" class="dContenedor Rectangulo" style="overflow: visible;
                    width: 98%; resize: vertical;">
                    <table border="0" cellpadding="4">
                        <tbody>
                            <tr>
                                <td rowspan="4" valign="top" width="65">
                                    <asp:Image ID="Image2" runat="server" SkinID="Materiales" Width="53" Height="48px" />
                                </td>
                                <td style="width: 100%;">
                                    <asp:Label ID="lblTituloMateriales" runat="server" Text="lblTituloMateriales" CssClass="TituloF"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <asp:CheckBoxList ID="cblMaterialesQA" runat="server" DataSourceID="obsMaterialesQA"
                                        OnSelectedIndexChanged="cblMaterialesQA_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                    <asp:ObjectDataSource ID="obsMaterialesQA" runat="server" SelectMethod="GetData"
                                        TypeName="Fullstep.FSNServer.MaterialesQA">
                                        <SelectParameters>
                                            <asp:Parameter Name="sIdi" Type="String" />
                                            <asp:Parameter Name="iBaja" Type="Int32" DefaultValue="0" />
                                            <asp:Parameter Name="bSinCertificados" Type="Boolean" DefaultValue="1" />
                                            <asp:Parameter Name="lPyme" Type="Int16" DefaultValue="0" />
                                            <asp:Parameter Name="bResMatCom" Type="Boolean" DefaultValue="false" />
                                            <asp:Parameter Name="com" Type="String" DefaultValue="" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="materiales">
                                        <input id="txtMaterialesQA" type="text" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="tvMaterialesQA" class="materiales">
                                        <!--Contenido del arbol de materiales-->
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <br />
                <div id="dContenedorUNQA" class="dContenedor Rectangulo" style="height: 192px; overflow: auto;
                    width: 98%;">
                    <table border="0" cellpadding="4">
                        <tbody>
                            <tr>
                                <td rowspan="2" valign="top" width="65">
                                    <asp:Image runat="server" SkinID="UNegocio" Width="54" Height="54" />
                                </td>
                                <td>
                                    <asp:Label ID="lblTituloUNQA" runat="server" Text="lblTituloUNQA" CssClass="TituloF"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <asp:TreeView ID="tvUNQA" runat="server" ShowCheckBoxes="All">
                                        <DataBindings>
                                            <asp:TreeNodeBinding DataMember="System.Data.DataRowView" TextField="DEN" ValueField="ID"
                                                NavigateUrlField="ACCESO" SelectAction="None" />
                                        </DataBindings>
                                    </asp:TreeView>
                                    <asp:HiddenField ID="hfGrupoID" runat="server" />
                                    <asp:HiddenField ID="hfGrupoDEN" runat="server" />
                                    <asp:HiddenField ID="hfGrupoPERMISO" runat="server" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <br />
                <div id="dContenedorPuntuacionDesde" class="dContenedor Rectangulo" style="width: 98%;">
                    <table border="0" cellpadding="4">
                        <tbody>
                            <tr>
                                <td rowspan="2" valign="top" width="65">
                                    <asp:Image ID="Image3" runat="server" SkinID="Puntuacion" Width="60" Height="50" />
                                </td>
                                <td>
                                    <asp:Label ID="lblTituloPuntuacion" runat="server" CssClass="TituloF"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <table border="0" cellspacing="4" width="100%">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblPuntuacionDesde" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <igpck:WebNumericEditor ID="wnePuntuacionDesde" CssClass="CajaTexto" runat="server"></igpck:WebNumericEditor>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblPuntuacionHasta" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <igpck:WebNumericEditor ID="wnePuntuacionHasta" CssClass="CajaTexto" runat="server"></igpck:WebNumericEditor>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </td>
            <td valign="top" valign="top" style="width: 50%">
                <div id="dContenedorVarCal" class="dContenedor Rectangulo" style="height: 300px;
                    overflow: auto; width: 98%">
                    <table border="0" cellpadding="4">
                        <tbody>
                            <tr>
                                <td rowspan="2" valign="top" width="65">
                                    <asp:Image ID="Image1" runat="server" SkinID="VCalidad" Width="53" Height="40" />
                                </td>
                                <td>
                                    <asp:Label ID="lblTituloVarCal" runat="server" CssClass="TituloF"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <asp:UpdatePanel ID="upVarCal" runat="server" UpdateMode="conditional">
                                        <ContentTemplate>
                                            <asp:TreeView ID="tvVarCal" runat="server" ShowCheckBoxes="All" ExpandDepth="1">
                                                <DataBindings>
                                                    <asp:TreeNodeBinding DataMember="System.Data.DataRowView" NavigateUrlField="MATERIAL_QA"
                                                        TextField="DEN" ValueField="ID" SelectAction="None" />
                                                </DataBindings>
                                            </asp:TreeView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:HiddenField ID="hfPuntTotID" runat="server" />
                                    <asp:HiddenField ID="hfPuntTotDEN" runat="server" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <br />
                <div id="dContenedorProveedores" class="dContenedor Rectangulo" style="width: 98%;
                    height: 192px;">
                    <table border="0" cellpadding="4" width=100%>
                        <tbody>
                            <tr>
                                <td rowspan="2" valign="top" width="65">
                                    <asp:Image runat="server" SkinID="Proveedores" />
                                </td>
                                <td>
                                    <asp:Label ID="lblTituloProveedores" runat="server" CssClass="TituloF"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">                                                                        
                                    <table width="100%" cellpadding="2">
                                        <tr>
                                            <td width="70%">
                                                <asp:RadioButtonList ID="rblProveedores" runat="server">                                            
                                                </asp:RadioButtonList>
                                            </td>
                                            <td width="12em" valign="top" style="margin-right:10px;">
                                                <div id="divTipoProveedor" style="margin-top:25px;display:inline-block;" runat="server"></div>
                                            </td>      
                                        </tr>                                                                          
                                    </table>                                    
                                                                        
                                    <div style="margin-left:0px;display:inline-block;clear:both;width:100%;">
                                        <table border="0" cellspacing="4" width="100%">
                                            <tr>
                                                <td width="20%">
                                                    &nbsp;<asp:Label ID="lblDenProveedor" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtProveedor" runat="server" Width="230" Enabled="false"></asp:TextBox>
                                                    <input id="hProveCod" type="hidden" runat="server" />
                                                    <input id="hProveDen" type="hidden" runat="server" />                                                                                                    
                                                </td>
                                                <td>
                                                    <a style="cursor: pointer" onclick="javascript:BuscarProveedor()">
                                                        <asp:Image runat="server" ID="imgLupa" SkinID="LupaPeq" />
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <asp:CheckBox ID="chkProveedoresBaja" runat="server" />
                                                    <asp:Label ID="lblProveedoresBaja" runat="server"></asp:Label>
                                                </td>
                                            </tr>                                            
                                        </table>             
                                    </div>                                                                                             
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <br />
                <br />
                <br />
                <center>
                    <table border="0" cellspacing="4">
                        <tr>
                            <td valign="middle" align="center" width="100%">
                                <fsn:FSNButton ID="btnCargar" runat="server" Text="FSNButton"></fsn:FSNButton>
                            </td>
                        </tr>
                    </table>
                </center>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript" language="javascript">     
        /*''' <summary>
        ''' Mostrar una pantalla de selección de proveedores.
        ''' </summary>
        ''' <remarks>Llamada desde: imgBuscarProveedor; Tiempo máximo: 0</remarks>*/	    
        function BuscarProveedor() {
                window.open("<%=ConfigurationManager.AppSettings("RutaFS")%>_common/BuscadorProveedores.aspx?PM=false&Certif=1", "_blank", "width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes")
        }
   
        function prove_seleccionado2(sCIF, sProveCod, sProveDen)
	        {
	        document.getElementById("<%=hProveCod.ClientID%>").value = sProveCod;
	        document.getElementById("<%=txtProveedor.ClientID%>").value = sProveDen;
	        document.getElementById("<%=hProveDen.ClientID%>").value = sProveDen;
        
            var rblProveedores;
            rblProveedor=document.getElementById("<%=rblProveedores.ClientID%>_3");
            rblProveedor.checked=true;
	        }


        function InicializarProveedor(){
	        if(!document.getElementById("<%=rblProveedores.ClientID%>_3").checked){
	            document.getElementById("<%=hProveCod.ClientID%>").value = '';
		        document.getElementById("<%=txtProveedor.ClientID%>").value = '';
		            document.getElementById("<%=hProveDen.ClientID%>").value = '';
	        }
	        else{
		        if(document.getElementById("<%=hProveCod.ClientID%>").value== ''){
		            alert("<%=Textos(57)%>.");
		            document.getElementById("<%=rblProveedores.ClientID%>_3").checked=false;
		        }
	        }
        }

        function MostrarComboTipoProveedor(item){              
            if (document.getElementById("<%=rblProveedores.ClientID%>_1").checked){
                document.getElementById("<%=divTipoProveedor.ClientID%>").style.visibility="visible";
            }
            else{
                document.getElementById("<%=divTipoProveedor.ClientID%>").style.visibility="hidden";
            }
        }

        function ValidarList(ListID){
	        var val = document.getElementById(ListID);
            var col = val.getElementsByTagName("*");
            if ( col != null ) {
                for ( i = 0; i < col.length; i++ ) {
                    if (col.item(i).tagName == "INPUT") {
                        if (col.item(i).checked ) {
                            return true;
                        }
                    }
                }
                return false;
            }
        }

        function ValidarFiltro(){
        
            //si no tiene acceso al grupo validamos que chequee una unidad de negocio
            if(!ValidarList("<%=rblProveedores.ClientID%>")){
                    alert("<%=Textos(55)%>");
                    return false;
            }
            if(document.getElementById("<%=hfGrupoID.ClientID%>").value==''){
                if (!ValidarList("<%=tvUNQA.ClientID%>")){
                    alert("<%=Textos(56)%>");
                    return false;
                }   
            }
            //Si el tipo de filtro es GS o GSQA guardamos los nodos para luego utilizarlos
            //Si es QA se hace como antes (.net)
            if (tipofiltro != 0) {
                if (txtMaterialesQA.tokenInput("get").length==0 && $("[id$=hProveCod]").val()=="")
                {
                    alert(TextosJScript[31]);
                    return false;
                }else{
                    addAllNodes(false);
                }
            }
            return true;
        }


    </script>    
</asp:Content>
