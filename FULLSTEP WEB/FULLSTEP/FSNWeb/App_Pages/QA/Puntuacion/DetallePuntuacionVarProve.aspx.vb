﻿Imports System.Web.Script.Serialization
Imports Fullstep
Imports Fullstep.FSNLibrary
Partial Public Class DetallePuntuacionVarProve
    Inherits FSNPage
    Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
    Private Const TODOS_MATQA As String = "TODOS"
    Private Const MARCA_TODOS As String = "HIJOS"
    Private bExportando As Boolean = False
    Private _FiltroVarCal As Collection
    Private _FiltroUNQA As Collection
    Private _ProveCod As String
    Private _ProveDen As String
    Private _ProveCif As String
    Private _ProveEmail As String
    Private _ProveMatQA As String
    Private _HayError As Nullable(Of Boolean)
    Private _dsDetallePuntuacionVarProve As DataSet
#Region "Propiedades"
    ''' <summary>
    ''' Propiedad que contiene el dataset con la ficha de calidad del proveedor.
    ''' Si es la primera carga de la página obtiene los datos de BD y los cachea
    ''' </summary>
    ''' <value></value>
    ''' <returns>DataSet´</returns>
    ''' <remarks></remarks>
    Protected ReadOnly Property dsDetallePuntuacionVarProve() As DataSet
        Get
            If _dsDetallePuntuacionVarProve Is Nothing Then
                If Me.IsPostBack Then
                    _dsDetallePuntuacionVarProve = CType(Cache("dsDetallePuntuacionVarProve_" & FSNUser.Cod), DataSet)
                End If
                If _dsDetallePuntuacionVarProve Is Nothing Then
                    ObtenerDataSetDetallePuntuacionVarProve()
                    Me.InsertarEnCache("dsDetallePuntuacionVarProve_" & FSNUser.Cod, _dsDetallePuntuacionVarProve)
                End If
            End If
            Return _dsDetallePuntuacionVarProve
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que contiene la fecha de actualizaciÃ³n de las puntuaciones y la almacena en el ViewState.
    ''' </summary>
    ''' <value></value>
    ''' <returns>String con la fecha</returns>
    ''' <remarks></remarks>
    Protected Property FechaPuntuacion() As String
        Get
            Return ViewState("FechaPuntuacion")
        End Get
        Set(ByVal value As String)
            ViewState("FechaPuntuacion") = value
        End Set
    End Property
    Protected ReadOnly Property FiltroVarCal() As Collection
        Get
            If _FiltroVarCal Is Nothing Then
                _FiltroVarCal = New Collection

                Dim iVarCal As Integer = CType(Request.QueryString("VARCALID"), Integer)
                Dim iNivel As Integer = CType(Request.QueryString("VARCALNIVEL"), Integer)

                Dim PMVariablesCalidad As FSNServer.VariablesCalidad = FSNServer.Get_Object(GetType(FSNServer.VariablesCalidad))
                Dim dsVarCal As DataSet = PMVariablesCalidad.GetData_hds(FSNUser.Cod, Me.Idioma, FSNUser.Pyme, bTodas:=True)
                Dim dtVarCal As DataTable = dsVarCal.Tables(0)

                'Variable padre
                Dim dvVarCal As New DataView(dtVarCal, "ID='" & iVarCal & "-" & iNivel & "'", "orID", DataViewRowState.CurrentRows)
                If dvVarCal.Count > 0 Then
                    Dim NodoCol As New Collection()
                    NodoCol.Add(iVarCal, "ID")
                    NodoCol.Add(iNivel, "NIVEL")
                    NodoCol.Add(String.Empty, "PERMISO")
                    NodoCol.Add(dvVarCal(0)("DEN"), "DEN")
                    NodoCol.Add(String.Empty, "HIJOS")
                    If DBNullToStr(dvVarCal(0)("MATERIAL_QA")) = String.Empty Then
                        NodoCol.Add(TODOS_MATQA, "MATQA")
                    Else
                        NodoCol.Add(DBNullToStr(dvVarCal(0)("MATERIAL_QA")), "MATQA")
                    End If

                    _FiltroVarCal.Add(NodoCol, iVarCal & "-" & iNivel)

                    AgregarHijos(dtVarCal, iVarCal, iNivel, NodoCol)
                End If

                Session("CFiltroVarCalDetalleVarProve") = _FiltroVarCal
            End If
            Return _FiltroVarCal
        End Get
    End Property
    Private Sub AgregarHijos(ByVal dtVarCal As DataTable, ByVal iVarCal As Integer, ByVal iNivel As Integer, ByRef oNodo As Collection)
        Dim bMatHijos As Boolean = (oNodo("MATQA") = MARCA_TODOS)
        Dim sMatHijos As String = String.Empty

        Dim dvHijos As New DataView(dtVarCal, "PADRE='" & iVarCal & "-" & iNivel & "'", "orID", DataViewRowState.CurrentRows)
        If Not dvHijos Is Nothing AndAlso dvHijos.Count > 0 Then
            For Each drvHijo As DataRowView In dvHijos
                Dim sId As String = drvHijo("ID")
                Dim iIndex As Integer = sId.IndexOf("-")
                Dim iIDHijo As Integer = sId.Substring(0, iIndex)
                Dim iNivelHijo As Integer = sId.Substring(iIndex + 1)

                Dim NodoCol As New Collection()
                NodoCol.Add(iIDHijo, "ID")
                NodoCol.Add(iNivelHijo, "NIVEL")
                NodoCol.Add(String.Empty, "PERMISO")
                NodoCol.Add(drvHijo("DEN"), "DEN")
                NodoCol.Add(String.Empty, "HIJOS")
                If DBNullToStr(drvHijo("MATERIAL_QA")) = String.Empty Then
                    NodoCol.Add(TODOS_MATQA, "MATQA")
                Else
                    NodoCol.Add(DBNullToStr(drvHijo("MATERIAL_QA")), "MATQA")
                End If

                _FiltroVarCal.Add(NodoCol, sId)

                AgregarHijos(dtVarCal, iIDHijo, iNivelHijo, NodoCol)

                'Las variables compuestas tienen el material de las hijas
                If bMatHijos Then
                    If InStr("_" & sMatHijos & "_", "_" & NodoCol("MATQA") & "_") = 0 Then
                        If Not (sMatHijos = String.Empty) Then sMatHijos &= "_"
                        sMatHijos &= NodoCol("MATQA")
                    End If
                End If
            Next

            If bMatHijos Then
                oNodo.Remove("MATQA")
                oNodo.Add(sMatHijos, "MATQA")
            End If
        End If
    End Sub
    Protected ReadOnly Property FiltroUNQA() As Collection
        Get
            If _FiltroUNQA Is Nothing Then
                If _FiltroUNQA Is Nothing Then
                    _FiltroUNQA = New Collection
                    Dim oProve As FSNServer.Proveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                    Dim dtUNQAs As DataTable = oProve.DevolverUNQAs(ProveCod, FSNUser.Idioma)
                    If Not dtUNQAs Is Nothing AndAlso dtUNQAs.Rows.Count > 0 Then
                        For Each drUNQA As DataRow In dtUNQAs.Rows
                            Dim NodoCol As New Collection()
                            NodoCol.Add(drUNQA("UNQA"), "ID")
                            NodoCol.Add(drUNQA("NIVEL"), "NIVEL")
                            NodoCol.Add(String.Empty, "PERMISO")
                            NodoCol.Add(drUNQA("COD") & "-" & drUNQA("DEN"), "DEN")
                            NodoCol.Add(String.Empty, "HIJOS")

                            _FiltroUNQA.Add(NodoCol, drUNQA("UNQA"))
                        Next
                    End If

                    Session("CFiltroUNQADetalleVarProve") = _FiltroUNQA
                End If
            End If
            Return _FiltroUNQA
        End Get
    End Property
    Protected ReadOnly Property ProveCod() As String
        Get
            If _ProveCod Is Nothing Then
                _ProveCod = Request.QueryString("PROVECOD")
            End If
            Return _ProveCod
        End Get
    End Property
    Protected ReadOnly Property ProveDen() As String
        Get
            If _ProveDen Is Nothing Then
                _ProveDen = Request.QueryString("PROVEDEN")
            End If
            Return _ProveDen
        End Get
    End Property
    Protected ReadOnly Property ProveCif() As String
        Get
            If _ProveCif Is Nothing Then
                _ProveCif = Request.QueryString("PROVECIF")
            End If
            Return _ProveCif
        End Get
    End Property
    Protected ReadOnly Property ProveMatQA() As String
        Get
            If _ProveMatQA Is Nothing Then
                Dim oProve As FSNServer.Proveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                Dim dtMat As DataTable = oProve.DevolverMaterialesQA(_ProveCod, FSNUser.Idioma)
                If Not dtMat Is Nothing AndAlso dtMat.Rows.Count > 0 Then
                    For Each drMat As DataRow In dtMat.Rows
                        _ProveMatQA &= "," & drMat("MATERIAL_QA")
                    Next
                    _ProveMatQA &= ","
                End If

                Session("ProveMatQADetalleVarProve") = _ProveMatQA
            End If
            Return _ProveMatQA
        End Get
    End Property
    Private Shared _columnasVisiblesCliente As List(Of String)
#End Region
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Procedimiento de carga de la página. Establece el módulo de idioma, los textos de los botones y 
    ''' etiquetas, establece a q pagina y con q orden de grid de panelCalidad debo volver.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.FichaProveedor

        If Not IsPostBack Then
            Dim bVerNotifs As Boolean = False
            For Each tipo As EntidadNotificacion In FSNUser.getEntidadesNotificacion
                If tipo = EntidadNotificacion.Calificaciones Then
                    bVerNotifs = True
                    Exit For
                End If
            Next

            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.numeric.min.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/numericFormatted.js"))

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutaFS") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "';</script>")
            End If

            lblProveedor.Text = ProveCod & "-" & ProveDen & " " & Textos(0) & ProveCif

            imgDetalleProve.ContextKey = ProveCod

            ibDetalleProve.AlternateText = Textos(6)

            hProveCod.Value = ProveCod
            hProveDen.Value = ProveDen
        End If
        NumericEditor.EditorControl.Culture = System.Globalization.CultureInfo.CurrentCulture
        whdgProveedores_InitializeDataSource()
        whdgProveedores.DataBind()
        ConfigurarGrid()
        upWhdgProveedores.Update()

        Dim sMSIE7 As String = "var bMSIE7="
        If Request.Browser("Browser") = "IE" AndAlso (Request.Browser("MajorVersion") < 8) Then
            sMSIE7 = String.Format(IncludeScriptKeyFormat, "javascript", sMSIE7 & "true;")
        Else
            sMSIE7 = String.Format(IncludeScriptKeyFormat, "javascript", sMSIE7 & "false;")
        End If
        Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "sMSIE7", sMSIE7)
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
            Dim sVariableJavascriptTextosPantalla As String = "var TextosPantalla = new Array();"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[0]='" & JSText(Textos(58)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[1]='" & JSText(Textos(59)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[2]='" & JSText(Textos(60)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[3]='" & JSText(Textos(61)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[4]='" & JSText(Textos(62)) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextosPantalla, True)
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "NumberFormatUsu") Then
            With FSNUser
                Dim sVariablesJavascript As String = "var UsuNumberDecimalSeparator = '" & .NumberFormat.NumberDecimalSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberGroupSeparator='" & .NumberFormat.NumberGroupSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberNumDecimals='" & .NumberFormat.NumberDecimalDigits & "';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NumberFormatUsu", sVariablesJavascript, True)
            End With
        End If
    End Sub
    ''' <summary>
    ''' Datos de las unqa para la ficha de calidad
    ''' </summary>
    ''' <param name="dsDatosUNQA">Datos de las unqa</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerFiltroUNQA(ByVal dsDatosUNQA As DataSet) As Collection
        Dim cfiltroUNQA As New Collection
        Dim cfiltro As New Collection()
        If Not dsDatosUNQA.Tables(1).Rows.Count = 0 Then
            With dsDatosUNQA.Tables(1)
                cfiltro.Add(.Rows(0)("ID"), "ID")
                cfiltro.Add(0, "NIVEL")
                cfiltro.Add(.Rows(0)("DEN"), "DEN")
                cfiltro.Add(True, "FILTRO")
                cfiltro.Add(.Rows(0)("ACCESO"), "PERMISO")
                cfiltroUNQA.Add(cfiltro, .Rows(0)("ID"))
            End With
        End If
        ObtenerFiltroUNQAHijos(cfiltroUNQA, dsDatosUNQA.Tables(0), dsDatosUNQA.Tables(1).Rows(0)("ID"), 1)
        Return cfiltroUNQA
    End Function
    ''' <summary>
    ''' Rellenamos el arbol de los hijos de las unqa
    ''' </summary>
    ''' <param name="cfiltroUNQA">Colección de las unqa para la ficha de calidad</param>
    ''' <param name="dtDatosUNQA">Datos de las unqa</param>
    ''' <param name="IdPadre">IdPadre de los hijos a crear</param>
    ''' <param name="Nivel">Nivel de los hijos a crear</param>
    ''' <remarks></remarks>
    Private Sub ObtenerFiltroUNQAHijos(ByRef cfiltroUNQA As Collection, ByVal dtDatosUNQA As DataTable,
                                       ByVal IdPadre As Integer, ByVal Nivel As Integer)
        Dim cfiltro As Collection
        Dim sHijos As String = String.Empty
        For Each item As DataRow In dtDatosUNQA.Select("PADRE" & IIf(Nivel = 1, " IS NULL", "=" & IdPadre))
            If Not item("ACCESO") = 0 Then
                cfiltro = New Collection()
                cfiltro.Add(item("ID"), "ID")
                cfiltro.Add(Nivel, "NIVEL")
                cfiltro.Add(item("DEN"), "DEN")
                cfiltro.Add(False, "FILTRO")
                cfiltro.Add(item("ACCESO"), "PERMISO")
                cfiltroUNQA.Add(cfiltro, item("ID"))

                sHijos = sHijos & IIf(sHijos Is String.Empty, "", ",") & item("ID")
                ObtenerFiltroUNQAHijos(cfiltroUNQA, dtDatosUNQA, item("ID"), Nivel + 1)
            End If
        Next
        cfiltroUNQA.Item(IdPadre.ToString).Add(sHijos, "HIJOS")
    End Sub
    ''' <summary>
    ''' Datos de las variables de calidad para la ficha de calidad
    ''' </summary>
    ''' <param name="dsDatosUNQA">Datos de las variables de calidad</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerFiltroVarCal(ByVal dsDatosUNQA As DataSet) As Collection
        Dim cfiltroVarCal As New Collection
        Dim cfiltro As New Collection()
        With dsDatosUNQA.Tables(1)
            cfiltro.Add(.Rows(0)("ID"), "ID")
            cfiltro.Add("0", "NIVEL")
            cfiltro.Add(.Rows(0)("DEN"), "DEN")
            cfiltro.Add(True, "FILTRO")
            cfiltro.Add("TODOS", "MATQA")
            cfiltroVarCal.Add(cfiltro, .Rows(0)("ID").ToString)

            ObtenerFiltroVarCalHijos(cfiltroVarCal, dsDatosUNQA.Tables(0), .Rows(0)("ID"), 1)
        End With

        Return cfiltroVarCal
    End Function
    ''' <summary>
    ''' Rellenamos el arbol de los hijos de las varcal
    ''' </summary>
    ''' <param name="cfiltroVarCal">Colección de las unqa para la ficha de calidad</param>
    ''' <param name="dtDatosVarCal">Datos de las varcal</param>
    ''' <param name="IdPadre">IdPadre de los hijos a crear</param>
    ''' <param name="Nivel">Nivel de los hijos a crear</param>
    ''' <remarks></remarks>
    Private Sub ObtenerFiltroVarCalHijos(ByRef cfiltroVarCal As Collection, ByVal dtDatosVarCal As DataTable,
                                       ByVal IdPadre As String, ByVal Nivel As Integer)
        Dim cfiltro As Collection
        Dim sHijos As String = String.Empty
        Dim TieneHijos As Boolean = False
        Dim sPadreMatQA, sMatQA As String
        sMatQA = String.Empty
        For Each item As DataRow In dtDatosVarCal.Select("PADRE" & IIf(Nivel = 1, " IS NULL", "='" & IdPadre & "'"))
            TieneHijos = True
            cfiltro = New Collection()
            cfiltro.Add(Split(item("ID"), "-")(0), "ID")
            cfiltro.Add(Split(item("ID"), "-")(1), "NIVEL")
            cfiltro.Add(item("DEN"), "DEN")
            cfiltro.Add(False, "FILTRO")
            cfiltro.Add(item("MATERIAL_QA").ToString, "PERMISO")
            cfiltro.Add(item("MATERIAL_QA").ToString, "MATQA")
            sMatQA = IIf(IsDBNull(item("MATERIAL_QA")), "TODOS", item("MATERIAL_QA"))
            cfiltroVarCal.Add(cfiltro, item("ID"))

            sHijos = sHijos & IIf(sHijos Is String.Empty, "", ",") & item("ID")

            sPadreMatQA = cfiltroVarCal.Item(IdPadre.ToString)("MATQA")
            If dtDatosVarCal.Select("PADRE='" & item("ID") & "'").Length = 0 Then
                If cfiltroVarCal.Item(item("ID"))("MATQA") Is String.Empty Then
                    cfiltroVarCal.Item(item("ID")).REMOVE("MATQA")
                    cfiltroVarCal.Item(item("ID")).ADD(sMatQA, "MATQA")
                    cfiltroVarCal.Item(item("ID")).REMOVE("PERMISO")
                    cfiltroVarCal.Item(item("ID")).ADD(sMatQA, "PERMISO")
                End If
                If InStr("_" & sPadreMatQA & "_", "_" & sMatQA & "_") = 0 Then
                    cfiltroVarCal.Item(IdPadre.ToString).REMOVE("MATQA")
                    cfiltroVarCal.Item(IdPadre.ToString).ADD(IIf(sPadreMatQA Is String.Empty, sMatQA, sPadreMatQA & "_" & sMatQA), "MATQA")
                    cfiltroVarCal.Item(IdPadre.ToString).REMOVE("PERMISO")
                    cfiltroVarCal.Item(IdPadre.ToString).ADD(IIf(sPadreMatQA Is String.Empty, sMatQA, sPadreMatQA & "_" & sMatQA), "PERMISO")
                End If
            Else
                ObtenerFiltroVarCalHijos(cfiltroVarCal, dtDatosVarCal, item("ID"), Nivel + 1)
                If Not Nivel = 1 Then
                    For Each MatQA As String In cfiltroVarCal.Item(item("ID"))("MATQA").ToString.Split("_")
                        If InStr("_" & sPadreMatQA & "_", "_" & MatQA & "_") = 0 Then
                            cfiltroVarCal.Item(IdPadre.ToString).REMOVE("MATQA")
                            cfiltroVarCal.Item(IdPadre.ToString).ADD(IIf(sPadreMatQA Is String.Empty, sMatQA, sPadreMatQA & "_" & sMatQA), "MATQA")
                            cfiltroVarCal.Item(IdPadre.ToString).REMOVE("PERMISO")
                            cfiltroVarCal.Item(IdPadre.ToString).ADD(IIf(sPadreMatQA Is String.Empty, sMatQA, sPadreMatQA & "_" & sMatQA), "PERMISO")
                        End If
                    Next
                End If
            End If
        Next
        cfiltroVarCal.Item(IdPadre.ToString).Add(sHijos, "HIJOS")
    End Sub
    ''' <summary>
    ''' Obtiene un dataset con las puntuaciones de todas las UNQAs a las que el usuario tiene acceso
    ''' y de todas las Variables de calidad a las que el usuario tiene acceso y que puntuan para el proveedor
    ''' y genera la tabla que conforma la ficha con un registro por cada variable de calidad, y cada regsitro con las columna de puntuaciones, calificaciones y fechas de las puntauciones de calidad por cada unidad de negocio
    ''' </summary>
    ''' <remarks></remarks>
    Sub ObtenerDataSetDetallePuntuacionVarProve()
        Dim PMProveedor As FSNServer.Proveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
        'Obtengo las puntuaciones de todas las UNQAs a las que el usuario tiene acceso
        Dim sUNQA As String = ""
        For Each UNQA As Collection In FiltroUNQA
            sUNQA = sUNQA & IIf(sUNQA = "", "", ",") & UNQA("ID")
        Next
        'Obtengo las puntuaciones de todas las Variables de calidad a las que el usuario tiene acceso y que puntuan para el proveedor
        Dim sVarCal As String = ""
        For Each VarCal As Collection In FiltroVarCal
            Dim aMateriales() As String = Split(VarCal("MATQA"), "_")
            Dim bEncontrado As Boolean = False
            For iMat As Integer = 0 To aMateriales.Length - 1
                If aMateriales(iMat) = TODOS_MATQA Then
                    bEncontrado = True
                    Exit For
                End If
                If ProveMatQA.IndexOf("," & aMateriales(iMat) & ",") <> -1 Then
                    bEncontrado = True
                    Exit For
                End If
            Next
            If bEncontrado Then
                sVarCal = sVarCal & IIf(sVarCal = "", "", ",") & VarCal("ID") & "-" & VarCal("NIVEL")
            End If
        Next

        _dsDetallePuntuacionVarProve = PMProveedor.DevolverFichaCalidad(ProveCod, Me.FSNUser.Cod, sUNQA, sVarCal, Me.Idioma)
        If _dsDetallePuntuacionVarProve.Tables.Count > 0 Then
            ObtenerTablaDetallePuntuacionVarProve()
        End If
    End Sub
    ''' <summary>
    ''' Genera una tabla que necesitamos para mostrar la ficha de calidad. Un regsitro por cada variable de calidad, y cada regsitro con columnas de puntuación/calificación/fecha act por cada unidad de negocio
    ''' Una vez generada la tabla la añade al dataset _dsDetallePuntuacionVarProve y la rellena con los datos obtenidos de BD
    ''' </summary>
    ''' <remarks></remarks>
    Sub ObtenerTablaDetallePuntuacionVarProve()
        'Creamos la tabla que necesitamos para mostrar la ficha de calidad
        Dim dtVariables As DataTable = New DataTable("MaestroVariables")
        Dim dRow As DataRow
        Dim dColumna As DataColumn

        'DataView para buscar si las var. se aplican al proveedor. Se buscan en el dataset de puntuaciones. Si no están es que no aplican
        Dim dvFichaProve As New DataView(_dsDetallePuntuacionVarProve.Tables(0), "", "VARCAL,NIVEL", DataViewRowState.CurrentRows)

        dColumna = New DataColumn("VC_ID", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("VC_NIVEL", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dtVariables.PrimaryKey = New DataColumn() {dtVariables.Columns("VC_ID"), dtVariables.Columns("VC_NIVEL")}
        dColumna = New DataColumn("VC_DEN", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("FORMULA", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("LEYENDA", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("TIPO", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("SUBTIPO", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("MODIFICAR", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("OBSERVACIONES", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        'añadimos una columna por cada UNQA
        For Each UNQA As Collection In FiltroUNQA
            dColumna = New DataColumn("PUNT_" & UNQA("ID"), System.Type.GetType("System.Double"))
            dtVariables.Columns.Add(dColumna)
            dColumna = New DataColumn("CAL_" & UNQA("ID"), System.Type.GetType("System.String"))
            dtVariables.Columns.Add(dColumna)
            dColumna = New DataColumn("FECACT_" & UNQA("ID"), System.Type.GetType("System.DateTime"))
            dtVariables.Columns.Add(dColumna)
        Next
        'Añadimos las filas con las variables de calidad que el usuario puede ver y que afectan al proveedor
        For Each VarCal As Collection In FiltroVarCal
            Dim aMateriales() As String = Split(VarCal("MATQA"), "_")
            Dim bEncontrado As Boolean = False
            For iMat As Integer = 0 To aMateriales.Length - 1
                If aMateriales(iMat) = TODOS_MATQA Then
                    bEncontrado = True
                    Exit For
                End If
                If ProveMatQA.IndexOf("," & aMateriales(iMat) & ",") <> -1 Then
                    bEncontrado = True
                    Exit For
                End If
            Next
            If bEncontrado Then
                'Mirar si aplica
                If dvFichaProve.Find(New Object() {VarCal("ID"), VarCal("NIVEL")}) > -1 Then
                    dRow = dtVariables.NewRow
                    dRow("VC_ID") = VarCal("ID")
                    dRow("VC_NIVEL") = VarCal("NIVEL")
                    dRow("VC_DEN") = VarCal("DEN")
                    dtVariables.Rows.Add(dRow)
                End If
            End If
        Next
        _dsDetallePuntuacionVarProve.Tables.Add(dtVariables)
        _dsDetallePuntuacionVarProve.Relations.Add("REL_VAR_PUNT", New DataColumn() {_dsDetallePuntuacionVarProve.Tables("MaestroVariables").Columns("VC_ID"), _dsDetallePuntuacionVarProve.Tables("MaestroVariables").Columns("VC_NIVEL")}, New DataColumn() {_dsDetallePuntuacionVarProve.Tables(0).Columns("VARCAL"), _dsDetallePuntuacionVarProve.Tables(0).Columns("NIVEL")}, False)
        'Recorremos la tabla de variables de calidad y obtenemos sus puntuaciones por cada unqa
        Dim lObservaciones As Dictionary(Of String, String)
        Dim serializer As New JavaScriptSerializer
        For Each Row As DataRow In dtVariables.Rows
            lObservaciones = New Dictionary(Of String, String)
            For Each RowPuntuacion As DataRow In Row.GetChildRows("REL_VAR_PUNT")
                Row("FORMULA") = RowPuntuacion("FORMULA")
                Row("LEYENDA") = RowPuntuacion("LEYENDA")
                Row("TIPO") = RowPuntuacion("TIPO")
                Row("SUBTIPO") = RowPuntuacion("SUBTIPO")
                Row("MODIFICAR") = RowPuntuacion("MODIFICAR")
                Row("PUNT_" & RowPuntuacion("UNQA")) = RowPuntuacion("PUNT")
                Row("CAL_" & RowPuntuacion("UNQA")) = RowPuntuacion("CALIFICACION")
                Row("FECACT_" & RowPuntuacion("UNQA")) = RowPuntuacion("FECACT")
                If Not String.IsNullOrEmpty(RowPuntuacion("OBSERVACIONES").ToString) Then
                    lObservaciones("PUNT_" & RowPuntuacion("UNQA")) = RowPuntuacion("OBSERVACIONES")
                End If
                Dim _MesPuntuacion As Integer
                If FechaPuntuacion Is Nothing Then
                    If Not IsDBNull(RowPuntuacion("FECACT")) Then
                        _MesPuntuacion = Month(RowPuntuacion("FECACT"))
                        If _MesPuntuacion = 1 Then
                            FechaPuntuacion = ObtenerMes(12) & " " & Year(RowPuntuacion("FECACT")) - 1
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "FechaCalculo", "<script>var anyoCalculo=" & Year(RowPuntuacion("FECACT")) - 1 & ";var mesCalculo=12; </script>")
                        Else
                            FechaPuntuacion = ObtenerMes(Month(RowPuntuacion("FECACT")) - 1) & " " & Year(RowPuntuacion("FECACT"))
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "FechaCalculo", "<script>var anyoCalculo=" & Year(RowPuntuacion("FECACT")) & ";var mesCalculo=" & Month(RowPuntuacion("FECACT")) - 1 & "; </script>")
                        End If
                    End If
                End If
            Next
            Row("OBSERVACIONES") = if(lObservaciones.Count=0,"",serializer.Serialize(lObservaciones))
        Next
    End Sub
    ''' <summary>
    ''' Actualiza el dataset con la ficha del proveedor con las nuevas puntuaciones que recibe del webservice
    ''' </summary>
    ''' <remarks></remarks>
    Sub ActualizarTablaDetallePuntuacionVarProve(ByRef dsNuevaDetallePuntuacionVarProve As DataSet)
        'Relacionamos las nuevas puntaciones con la tabla de variables
        'Modificamos la variable interna _dsDetallePuntuacionVarProve par auqe no se cachee la simulacion
        dsNuevaDetallePuntuacionVarProve.Relations.Add("REL_VAR_NUEVASPUNT", New DataColumn() {dsNuevaDetallePuntuacionVarProve.Tables("MaestroVariables").Columns("VC_ID"), dsNuevaDetallePuntuacionVarProve.Tables("MaestroVariables").Columns("VC_NIVEL")}, New DataColumn() {dsNuevaDetallePuntuacionVarProve.Tables(2).Columns("VARCAL"), dsNuevaDetallePuntuacionVarProve.Tables(2).Columns("NIVEL")}, False)
        For Each Row As DataRow In dsNuevaDetallePuntuacionVarProve.Tables("MaestroVariables").Rows
            For Each RowPuntuacion As DataRow In Row.GetChildRows("REL_VAR_NUEVASPUNT")
                Row("PUNT_" & RowPuntuacion("UNQA")) = RowPuntuacion("VALOR")
                Row("CAL_" & RowPuntuacion("UNQA")) = RowPuntuacion("CAL")
                Row("FECACT_" & RowPuntuacion("UNQA")) = Now
            Next
        Next
    End Sub
    ''' <summary>
    ''' Procedimiento que estblece el datasource del WebHierarchicalDataGrid que contiene la ficha del proveedor
    ''' </summary>
    ''' <param name="dsNuevaDetallePuntuacionVarProve"></param>
    ''' <remarks></remarks>
    Private Sub whdgProveedores_InitializeDataSource(Optional ByVal dsNuevaDetallePuntuacionVarProve As DataSet = Nothing)
        Dim ds As New DataSet
        Dim dt As DataTable
        whdgProveedores.Rows.Clear()
        whdgProveedores.GridView.Rows.Clear()
        whdgProveedores.GridView.ClearDataSource()
        whdgProveedores.Columns.Clear()
        whdgProveedores.GridView.Columns.Clear()
        whdgProveedores.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings.Clear()

        If Not dsNuevaDetallePuntuacionVarProve Is Nothing Then
            dt = dsNuevaDetallePuntuacionVarProve.Tables("MaestroVariables")
            ds.Tables.Add(dt.Copy())
            whdgProveedores.DataSource = ds
            whdgProveedores.GridView.DataSource = ds
            CrearColumnasDT(dt)
            whdgProveedores.DataKeyFields = "VC_ID,VC_NIVEL"
        Else
            If dsDetallePuntuacionVarProve.Tables.Count > 0 Then
                dt = dsDetallePuntuacionVarProve.Tables("MaestroVariables")
                ds.Tables.Add(dt.Copy())
                whdgProveedores.DataSource = ds
                whdgProveedores.GridView.DataSource = ds
                CrearColumnasDT(dt)
                whdgProveedores.DataKeyFields = "VC_ID,VC_NIVEL"
            End If
        End If
    End Sub
    Private Sub CrearColumnasDT(ByVal dt As DataTable)
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim column As Infragistics.Web.UI.GridControls.EditingColumnSetting
        Dim nombre As String

        For i As Integer = 0 To dt.Columns.Count - 1
            column = New Infragistics.Web.UI.GridControls.EditingColumnSetting
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            nombre = dt.Columns(i).ToString
            With campoGrid
                .Key = nombre
                .DataFieldName = nombre
                .Header.Text = nombre
                .Header.Tooltip = nombre
                If Left(nombre, 6) = "FECACT" Then
                    .DataFormatString = "{0:" & FSNUser.DateFormat.ShortDatePattern.ToString & " " & FSNUser.DateFormat.ShortTimePattern.ToString & "}"
                Else
                    .DataFormatString = "{0:N" & FSNUser.NumberFormat.NumberDecimalDigits.ToString & "}"
                End If
            End With
            whdgProveedores.Columns.Add(campoGrid)
            whdgProveedores.GridView.Columns.Add(campoGrid)
            column.ColumnKey = nombre
            column.ReadOnly = True
            whdgProveedores.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings.Add(column)
        Next
    End Sub
    Private Sub AnyadirColumnaU(ByVal fieldname As String)
        Dim unboundField As New Infragistics.Web.UI.GridControls.UnboundField(True)

        unboundField.Key = fieldname
        whdgProveedores.Columns.Add(unboundField)
        whdgProveedores.GridView.Columns.Add(unboundField)
    End Sub
    ''' <summary>
    ''' Procedimiento que inicializa el grid que contiene la ficha del proveedor
    ''' Oculta las columnas que en principio no están visibles, y configura las cabeceras de las columnas, para añadirles los botones de cerrar, desplegar...
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ConfigurarGrid()
        whdgProveedores.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings("VC_DEN").ReadOnly = True
        whdgProveedores.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings("FORMULA").ReadOnly = True
        whdgProveedores.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings("LEYENDA").ReadOnly = True

        'COLUMNAS NO VISIBLES
        whdgProveedores.Columns("VC_ID").Hidden = True
        whdgProveedores.Columns("VC_NIVEL").Hidden = True
        whdgProveedores.Columns("FORMULA").Hidden = True
        whdgProveedores.Columns("LEYENDA").Hidden = True
        whdgProveedores.Columns("TIPO").Hidden = True
        whdgProveedores.Columns("SUBTIPO").Hidden = True
        whdgProveedores.Columns("MODIFICAR").Hidden = True
        whdgProveedores.Columns("OBSERVACIONES").Hidden = True

        'Variables
        Dim indiceColumnaMulti As Integer = 1
        whdgProveedores.Columns("VC_DEN").Header.Text = CabeceraVariables(Textos(2) & " " & Me.FechaPuntuacion)
        whdgProveedores.Columns("VC_DEN").Header.CssClass = "fp_celda_cabecera_vc"

        whdgProveedores.Columns("VC_DEN").Width = 350
        'formula/leyenda
        whdgProveedores.Columns("FORMULA").Header.CssClass = "punt_nivel0"
        whdgProveedores.Columns("FORMULA").Header.Text = CabeceraUNQAS("", Textos(29), "FORMULA")
        whdgProveedores.Columns("LEYENDA").Header.CssClass = "punt_nivel0"
        whdgProveedores.Columns("LEYENDA").Header.Text = CabeceraUNQAS("", Textos(30), "LEYENDA")
        whdgProveedores.Columns("FORMULA").Width = 250
        whdgProveedores.Columns("LEYENDA").Width = 250
        'puntuaciones y calificaciones
        Dim KeyPUNT As String
        Dim KeyCAL As String
        Dim KeyFecha As String
        Dim CodUNQA As String
        For Each UNQA As Collection In FiltroUNQA
            KeyPUNT = "PUNT_" & UNQA("ID")
            KeyCAL = "CAL_" & UNQA("ID")
            KeyFecha = "FECACT_" & UNQA("ID")

            'Control de edición para las puntuaciones
            whdgProveedores.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings(KeyPUNT).EditorID = "NumericEditor"

            'titulo cabecera
            CodUNQA = UNQA("DEN")
            If UNQA("NIVEL") <> 0 Then
                CodUNQA = Left(CodUNQA, InStr(CodUNQA, "-") - 1)
            End If
            whdgProveedores.Columns(KeyPUNT).Header.Tooltip = UNQA("DEN")
            whdgProveedores.Columns(KeyPUNT).Header.Text = Me.CabeceraUNQAS(UNQA("HIJOS"), CodUNQA, KeyPUNT, KeyCAL, KeyFecha)
            whdgProveedores.Columns(KeyCAL).Header.Text = Me.CabeceraUNQAS("", Textos(3), KeyCAL)
            whdgProveedores.Columns(KeyFecha).Header.Text = Me.CabeceraUNQAS("", Textos(4), KeyFecha)
            whdgProveedores.Columns(KeyFecha).FormatValue(FSNUser.DateFormat.ShortDatePattern & " " & FSNUser.DateFormat.ShortTimePattern)
            'Tamaños
            whdgProveedores.Columns(KeyPUNT).Width = 150
            whdgProveedores.Columns(KeyCAL).Width = 150
            whdgProveedores.Columns(KeyFecha).Width = 150
            'estilo de cabeceras por niveles
            whdgProveedores.Columns(KeyPUNT).Header.CssClass = "punt_nivel" & UNQA("NIVEL")
            whdgProveedores.Columns(KeyCAL).Header.CssClass = "punt_nivel" & UNQA("NIVEL")
            whdgProveedores.Columns(KeyFecha).Header.CssClass = "punt_nivel" & UNQA("NIVEL")
            'estilo de las celdas de calificaciones para todas el mismo
            whdgProveedores.Columns(KeyCAL).CssClass = "fp_celda_cal"
            whdgProveedores.Columns(KeyFecha).CssClass = "fp_celda_cal"
            'calificaciones no editables
            whdgProveedores.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings(KeyCAL).ReadOnly = True
            whdgProveedores.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings(KeyFecha).ReadOnly = True

            whdgProveedores.Columns(KeyPUNT).Hidden = False
            whdgProveedores.Columns(KeyCAL).Hidden = False
            whdgProveedores.Columns(KeyFecha).Hidden = False
            whdgProveedores.GridView.Columns(KeyPUNT).Hidden = False
            whdgProveedores.GridView.Columns(KeyCAL).Hidden = False
            whdgProveedores.GridView.Columns(KeyFecha).Hidden = False
            'si la unqa no está en el filtro la ocultamos
            If IsPostBack Then
                If _columnasVisiblesCliente Is Nothing Then _columnasVisiblesCliente = New List(Of String)
                whdgProveedores.Columns(KeyPUNT).Hidden = Not _columnasVisiblesCliente.Contains(KeyPUNT)
                whdgProveedores.Columns(KeyFecha).Hidden = Not _columnasVisiblesCliente.Contains(KeyFecha)
                whdgProveedores.GridView.Columns(KeyPUNT).Hidden = Not _columnasVisiblesCliente.Contains(KeyPUNT)
                whdgProveedores.GridView.Columns(KeyFecha).Hidden = Not _columnasVisiblesCliente.Contains(KeyFecha)
            Else
                whdgProveedores.Columns(KeyFecha).Hidden = True
            End If
        Next
    End Sub
    ''' <summary>
    ''' Escribe el html de salida de las cabeceras de las columnas de puntuaciones/calificacion/fechas y de las columnas de formula y leyenda
    ''' Calificación/Fecha/Formula y Leyenda solo llevan el icono de cerrar (sHijos="")
    ''' Puntaciones de las unidades de negocio llevan
    ''' Icono de cerrar, si sHijos != "" icono de desplegar, icono de ver calificacion(sKeyCalificacion != ""), icono de ver fecha de act (sKeyFecha != "")
    ''' </summary>
    ''' <param name="sHijos">Si se trata de la puntuación de una unidad de negoico, si esta tiene unidades hijas para ver si se muestra el icono de desplegar</param>
    ''' <param name="sTextoCabecera">Caption de la cabecera</param>
    ''' <param name="sKeyCabecera">Key de la columna</param>
    ''' <param name="sKeyCalificacion">Key de la columna de calificación asociada a la puntuacion</param>
    ''' <param name="sKeyFecha">Key de la columna de fecha asociada a la puntuacion</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function CabeceraUNQAS(ByVal sHijos As String, ByVal sTextoCabecera As String, ByVal sKeyCabecera As String, Optional ByVal sKeyCalificacion As String = "", Optional ByVal sKeyFecha As String = "") As String
        If sKeyCabecera = "FORMULA" OrElse sKeyCabecera = "LEYENDA" Then
            CabeceraUNQAS = "<div id=""contenedor_imagenes"">"
        Else
            CabeceraUNQAS = "<div style='background-color:#f1f1f1;' id=""contenedor_imagenes"">"
        End If
        'CabeceraUNQAS = CabeceraUNQAS & "<tr><td><nobr>" & IIf(sTextoCabecera.Length > 9, Left(sTextoCabecera, 8) & "...", sTextoCabecera) & "&nbsp;</td>"
        CabeceraUNQAS = CabeceraUNQAS & "<div style=""text-overflow:ellipsis; overflow:hidden;float:left; vertical-align:middle;" &
            IIf(Left(sKeyCabecera, 4) = "PUNT", "max-width:70px;", "") & "white-space:nowrap;"">" & sTextoCabecera & "&nbsp;</div>"
        If Left(sKeyCabecera, 4) = "PUNT" Then
            CabeceraUNQAS = CabeceraUNQAS & "<div style=""text-align:left; min-width:25px;"">"
        Else
            CabeceraUNQAS = CabeceraUNQAS & "<div style=""text-align:right; min-width:25px;float:rigth;"">"
        End If
        CabeceraUNQAS = CabeceraUNQAS & "</div></div>"
    End Function
    ''' <summary>
    ''' Genera el html de la cabecera de la columna de las variables de calidad con su nombre y un icono para ver la fórmula/leyenda
    ''' </summary>
    ''' <param name="sTextoCabecera">Nombre de la variable de calidad</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function CabeceraVariables(ByVal sTextoCabecera As String) As String
        CabeceraVariables = "<table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" id=""contenedor_imagenes"">"
        CabeceraVariables = CabeceraVariables & "<tr><td><nobr>" & sTextoCabecera & "&nbsp;</td>"
        CabeceraVariables = CabeceraVariables & "<td style=""text-align: right"">"
        CabeceraVariables = CabeceraVariables & "</td></tr></table>"
    End Function
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Función que se ejecuta por cada fila del whdgProveedores, en ella controlamos los estilos de las variables de calidad en función de su nivel
    ''' Y si la puntuación es editable o no en función de si la variable es de tip manual y siu el usuario tiene permiso para modificarla
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub whdgProveedores_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgProveedores.InitializeRow
        Dim KeyUNQA As String
        'Estilo de la variable de calidad por nivel
        Dim Nivel As Integer = 0
        Nivel = e.Row.Items.FindItemByKey("VC_NIVEL").Value
        e.Row.Items.FindItemByKey("VC_DEN").CssClass = "fp_celda_vc_" & Nivel

        e.Row.Items.FindItemByKey("FORMULA").CssClass = "SinSalto fp_fila_tabla" & Nivel
        e.Row.Items.FindItemByKey("FORMULA").Tooltip = DBNullToStr(e.Row.Items.FindItemByKey("FORMULA").Value)
        e.Row.Items.FindItemByKey("LEYENDA").CssClass = "SinSalto fp_fila_tabla" & Nivel
        e.Row.Items.FindItemByKey("LEYENDA").Tooltip = DBNullToStr(e.Row.Items.FindItemByKey("LEYENDA").Value)

        Dim serializer As New JavaScriptSerializer
        Dim Subtipo As Integer = 0
        Subtipo = DBNullToInteger(e.Row.Items.FindItemByKey("SUBTIPO").Value)
        Dim lObservaciones As Dictionary(Of String, String)
        If String.IsNullOrEmpty(e.Row.Items.FindItemByKey("OBSERVACIONES").Value.ToString) Then
            lObservaciones = New Dictionary(Of String, String)
        Else
            lObservaciones = serializer.Deserialize(Of Dictionary(Of String, String))(e.Row.Items.FindItemByKey("OBSERVACIONES").Value.ToString)
        End If
        'Formato puntuacion
        For Each UNQA As Collection In FiltroUNQA
            KeyUNQA = "PUNT_" & UNQA("ID")

            Select Case Subtipo
                Case 3  ', 4, 5, 6, 7, 8 '3-CERTIF 4-NC 5-PPM 6-CARGOS 7-TASA 8-Encuesta
                    If DBNullToStr(e.Row.Items.FindItemByKey(KeyUNQA).Value) <> "" Then
                        e.Row.Items.FindItemByKey(KeyUNQA).CssClass = "fp_fila_lnk_tabla" & Nivel
                    Else
                        e.Row.Items.FindItemByKey(KeyUNQA).CssClass = "fp_fila_tabla" & Nivel
                    End If
                Case Else
                    e.Row.Items.FindItemByKey(KeyUNQA).CssClass = "fp_fila_tabla" & Nivel
            End Select
        Next
    End Sub
    ''' <summary>
    ''' Obtiene la denominación del mes de la puntuación
    ''' </summary>
    ''' <param name="iMes"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ObtenerMes(ByVal iMes As Integer) As String
        Dim IndiceMes As Integer = 11
        IndiceMes = IndiceMes + iMes
        ObtenerMes = Textos(IndiceMes)
    End Function
    ''' <summary>
    ''' Crea la tabla dtVariables con las columnas (PROVE, UNQA, VARCAL, NIVEL, VALOR, USU)
    ''' Y la rellena con los datos de las variables manuales que han sido modificadas por el usurio en la ficha
    ''' </summary>
    ''' <param name="dtVariables"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ObtenerTablaVariablesManuales(ByRef dtVariables As DataTable) As Boolean
        'Creamos la tabla que necesitamos PARA GUARDAR LOS VALORE MANUALES
        Dim DataChanged As Boolean = False
        Dim dColumna As DataColumn
        dColumna = New DataColumn("PROVE", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("UNQA", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("VARCAL", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("NIVEL", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dtVariables.PrimaryKey = New DataColumn() {dtVariables.Columns("UNQA"), dtVariables.Columns("VARCAL"), dtVariables.Columns("NIVEL")}
        dColumna = New DataColumn("VALOR", System.Type.GetType("System.Double"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("USU", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("OBSERVACIONES", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        'Rellenamos la tabla
        Dim dRow As DataRow
        Dim dataKey() As Object
        Dim serializer As New JavaScriptSerializer
        Dim lObservaciones As Dictionary(Of String, String)
        For Each Row As Infragistics.Web.UI.GridControls.GridRecord In whdgProveedores.Rows
            If DBNullToInteger(Row.Items.Item(whdgProveedores.Columns("SUBTIPO").Index).Value) = 2 AndAlso DBNullToInteger(Row.Items.Item(whdgProveedores.Columns("MODIFICAR").Index).Value) = 1 Then
                dataKey = Row.DataKey
                Dim dr As DataRow = dsDetallePuntuacionVarProve.Tables("MaestroVariables").Rows.Find(dataKey)
                Dim KeyPUNT As String
                If String.IsNullOrEmpty(Row.Items.Item(whdgProveedores.Columns("OBSERVACIONES").Index).Value.ToString) Then
                    lObservaciones = New Dictionary(Of String, String)
                Else
                    lObservaciones = serializer.Deserialize(Of Dictionary(Of String, String))(Row.Items.Item(whdgProveedores.Columns("OBSERVACIONES").Index).Value)
                End If
                For Each UNQA As Collection In FiltroUNQA
                    KeyPUNT = "PUNT_" & UNQA("ID")

                    If Not (DBNullToSomething(Row.Items.Item(whdgProveedores.Columns(KeyPUNT).Index).Value) = DBNullToSomething(dr(KeyPUNT)) _
                            AndAlso Not lObservaciones.ContainsKey("PUNT_" & UNQA("ID"))) Then
                        dRow = dtVariables.NewRow
                        dRow("PROVE") = ProveCod
                        dRow("UNQA") = UNQA("ID")
                        dRow("VARCAL") = Row.Items.Item(whdgProveedores.Columns("VC_ID").Index).Value
                        dRow("NIVEL") = Row.Items.Item(whdgProveedores.Columns("VC_NIVEL").Index).Value
                        dRow("VALOR") = Numero(Row.Items.FindItemByKey(KeyPUNT).Value, FSNUser.DecimalFmt, FSNUser.ThousanFmt)
                        dRow("USU") = FSNUser.Cod
                        dRow("OBSERVACIONES") = If(lObservaciones.ContainsKey("PUNT_" & UNQA("ID")), lObservaciones("PUNT_" & UNQA("ID")), "")
                        dtVariables.Rows.Add(dRow)
                        DataChanged = True
                    End If
                Next
            End If
        Next
        Return DataChanged
    End Function
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function setColumnasVisibles(ByVal columnasVisibles As String) As Object
        Try
            _columnasVisiblesCliente = Split(columnasVisibles, ";").ToList()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function Get_Logged_User() As FSNServer.User
        Dim Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Return Usuario
    End Function
End Class
