﻿Public Partial Class DetallePuntuacionNc
    Inherits FSNPage
    Private Const TODOS_MATQA As String = "TODOS"
    Private _dsFichaProveedor As DataSet

    Private _Unidad As String
    Private _Subtipo As String
    Private _Variable As String
    Private _Nivel As String
    Private _ProveCod As String
    Private _ProveDen As String
    Private _dtDatosVC As DataRow
    Private _Puntos As String
    Private _Calif As String
    Private _DenUnidad As String
    Private _ProveMatQA As String
    Private _FiltroVarCal As Collection
    Private m_bSaltarLayout As Byte = 0
    Private _VarOpcionConf As VarCalOpcionConf

    Private IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"

    ''' <summary>
    ''' Obtiene la ficha del proveedor cuando viene de GS directamente 
    ''' </summary>
    ''' <remarks></remarks>
    Sub ObtenerTablaFichaProveedor()
        'Creamos la tabla que necesitamos para mostrar la ficha de calidad
        Dim dtVariables As DataTable = New DataTable("MaestroVariables")
        Dim dRow As DataRow
        Dim dColumna As DataColumn

        dColumna = New DataColumn("VC_ID", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("VC_NIVEL", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dtVariables.PrimaryKey = New DataColumn() {dtVariables.Columns("VC_ID"), dtVariables.Columns("VC_NIVEL")}
        dColumna = New DataColumn("VC_DEN", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("FORMULA", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("LEYENDA", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("TIPO", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("SUBTIPO", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("MODIFICAR", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        'añadimos una columna por cada UNQA

        dColumna = New DataColumn("PUNT_" & _Unidad, System.Type.GetType("System.Double"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("CAL_" & _Unidad, System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("FECACT_" & _Unidad, System.Type.GetType("System.DateTime"))
        dtVariables.Columns.Add(dColumna)

        'Añadimos las filas con las variables de calidad que el usuario puede ver y que afectan al proveedor
        For Each VarCal As Collection In FiltroVarCal
            Dim aMateriales() As String = Split(VarCal("MATQA"), "_")
            Dim bEncontrado As Boolean = False
            For iMat As Integer = 0 To aMateriales.Length - 1
                If aMateriales(iMat) = TODOS_MATQA Then
                    bEncontrado = True
                    Exit For
                End If
                If ProveMatQA.IndexOf("," & aMateriales(iMat) & ",") <> -1 Then
                    bEncontrado = True
                    Exit For
                End If
            Next
            If bEncontrado Then
                dRow = dtVariables.NewRow
                dRow("VC_ID") = VarCal("ID")
                dRow("VC_NIVEL") = VarCal("NIVEL")
                dRow("VC_DEN") = VarCal("DEN")
                dtVariables.Rows.Add(dRow)
            End If

        Next
        _dsFichaProveedor.Tables.Add(dtVariables)
        _dsFichaProveedor.Relations.Add("REL_VAR_PUNT", New DataColumn() {_dsFichaProveedor.Tables("MaestroVariables").Columns("VC_ID"), _dsFichaProveedor.Tables("MaestroVariables").Columns("VC_NIVEL")}, New DataColumn() {_dsFichaProveedor.Tables(0).Columns("VARCAL"), _dsFichaProveedor.Tables(0).Columns("NIVEL")}, False)
        'Recorremos la tabla de variables de calidad y obtenemos sus puntuaciones por cada unqa
        For Each Row As DataRow In dtVariables.Rows
            For Each RowPuntuacion As DataRow In Row.GetChildRows("REL_VAR_PUNT")
                Row("FORMULA") = RowPuntuacion("FORMULA")
                Row("LEYENDA") = RowPuntuacion("LEYENDA")
                Row("TIPO") = RowPuntuacion("TIPO")
                Row("SUBTIPO") = RowPuntuacion("SUBTIPO")
                Row("MODIFICAR") = RowPuntuacion("MODIFICAR")
                Row("PUNT_" & RowPuntuacion("UNQA")) = RowPuntuacion("PUNT")
                Row("CAL_" & RowPuntuacion("UNQA")) = RowPuntuacion("CALIFICACION")
                Row("FECACT_" & RowPuntuacion("UNQA")) = RowPuntuacion("FECACT")
                Dim _MesPuntuacion As Integer
                If FechaPuntuacion Is Nothing Then
                    If Not IsDBNull(RowPuntuacion("FECACT")) Then
                        _MesPuntuacion = Month(RowPuntuacion("FECACT"))
                        If _MesPuntuacion = 1 Then
                            FechaPuntuacion = ObtenerMes(12) & " " & Year(RowPuntuacion("FECACT")) - 1
                        Else
                            FechaPuntuacion = ObtenerMes(Month(RowPuntuacion("FECACT")) - 1) & " " & Year(RowPuntuacion("FECACT"))
                        End If
                    End If
                End If
            Next
        Next
    End Sub
    Protected ReadOnly Property ProveMatQA() As String
        Get
            If _ProveMatQA Is Nothing Then
                _ProveMatQA = Request.Form(Request.QueryString("h") & "hProveMatQA")

                If _ProveMatQA Is Nothing Then
                    _ProveMatQA = Session("ProveMatQA")
                Else
                    Session("ProveMatQA") = _ProveMatQA
                End If

                If _ProveMatQA Is Nothing Then
                    Response.Redirect("panelCalidadFiltro.aspx")
                End If
            End If
            Return _ProveMatQA
        End Get
    End Property
    Function ObtenerMes(ByVal iMes As Integer) As String
        Dim IndiceMes As Integer = 11
        IndiceMes = IndiceMes + iMes
        ObtenerMes = Textos(IndiceMes)
    End Function
    Protected ReadOnly Property FiltroVarCal() As Collection
        Get
            If _FiltroVarCal Is Nothing Then
                _FiltroVarCal = Session("CFiltroVarCal")
                If _FiltroVarCal Is Nothing Then
                    Response.Redirect("panelCalidadFiltro.aspx")
                End If
            End If
            Return _FiltroVarCal
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que contiene la fecha de actualización de las puntuaciones y la almacena en el ViewState.
    ''' </summary>
    ''' <value></value>
    ''' <returns>String con la fecha</returns>
    ''' <remarks></remarks>
    Protected Property FechaPuntuacion() As String
        Get
            Return ViewState("FechaPuntuacion")
        End Get
        Set(ByVal value As String)
            ViewState("FechaPuntuacion") = value
        End Set
    End Property
    ''' <summary>
    ''' Propiedad que contiene el datarow con los datos de la variable de la ficha de calidad del proveedor a mostrar.
    ''' </summary>
    ''' <value></value>
    ''' <returns>DataRow</returns>
    ''' <remarks>Llamada desde: MostrarPanelNC; Tiempo maximo:0</remarks>
    Protected ReadOnly Property dtDatosVC() As DataRow
        Get
            If _dtDatosVC Is Nothing Then

                If _dsFichaProveedor Is Nothing Then
                    Dim PMProveedor As FSNServer.Proveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                    _dsFichaProveedor = PMProveedor.DevolverFichaCalidad(_ProveCod, Me.FSNUser.Cod, _Unidad, _Variable, Me.Idioma)
                    If _dsFichaProveedor.Tables.Count > 0 Then
                        ObtenerTablaFichaProveedor()
                    End If
                    Me.InsertarEnCache("dsFichaProveedor_" & FSNUser.Cod, _dsFichaProveedor)
                Else
                    _dsFichaProveedor = CType(Cache("dsFichaProveedor_" & FSNUser.Cod), DataSet)
                End If

                For Each row As DataRow In _dsFichaProveedor.Tables("MaestroVariables").Rows
                    If row.Item("VC_ID") = _Variable AndAlso row.Item("VC_NIVEL") = _Nivel Then
                        _dtDatosVC = row

                        _Puntos = DBNullToStr(row.Item("PUNT_" & _Unidad))
                        _Calif = DBNullToStr(row.Item("CAL_" & _Unidad))

                        Exit For
                    End If
                Next
            End If
            Return _dtDatosVC
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que contiene la denominación de la unidad de la ficha de calidad del proveedor a mostrar.
    ''' </summary>
    ''' <value></value>
    ''' <returns>denominación de la unidad</returns>
    ''' <remarks>Llamada desde: MostrarPanelNC; Tiempo maximo:0</remarks>
    Protected ReadOnly Property DenUnidad() As String
        Get
            If _DenUnidad Is Nothing Then
                Dim FiltroUNQA As Collection = Session("CFiltroUNQA")

                For Each UNQA As Collection In FiltroUNQA
                    If _Unidad = UNQA("ID") Then
                        _DenUnidad = UNQA("DEN")
                        Exit For
                    End If
                Next
            End If

            Return _DenUnidad
        End Get
    End Property
    ''' <summary>
    ''' Datos de las variables de calidad para la ficha de calidad
    ''' </summary>
    ''' <param name="dsDatosUNQA">Datos de las variables de calidad</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerVarCal(ByVal dsDatosUNQA As DataSet) As Collection
        Dim cfiltroVarCal As New Collection
        Dim cfiltro As New Collection()
        With dsDatosUNQA.Tables(4)
            cfiltro.Add(_Variable, "ID")
            cfiltro.Add(_Nivel, "NIVEL")
            cfiltro.Add(.Rows(0)("DEN"), "DEN")
            cfiltro.Add(True, "FILTRO")
            cfiltro.Add("TODOS", "MATQA")
            cfiltroVarCal.Add(cfiltro, _Variable)
        End With

        Return cfiltroVarCal
    End Function
    ''' <summary>
    ''' Carga los textos de la página e inicializa los controles
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    ''' <remarks>Se produce cuando el control de servidor se carga en el objeto Page. Tiempo maximo:0</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dsDatos As DataSet
        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.PopupFichaProveedor

        _Unidad = Request("Key")
        _Subtipo = Request("Subtipo")
        _Variable = Request("Variable")
        _Nivel = Request("Nivel")
        _ProveCod = Request("codProv")
        _ProveDen = Request("DenProv")
        If Request("desdeGS") = 1 Then
            Dim oDetalle As FSNServer.Puntuacion = FSNServer.Get_Object(GetType(FSNServer.Puntuacion))
            dsDatos = oDetalle.DetallePuntuacion(_Variable, _Nivel, Me.Idioma, _ProveCod, _Unidad, _Subtipo)
            Session("CFiltroUNQA") = ObtenerUNQA(dsDatos)
            Session("CFiltroVarCal") = ObtenerVarCal(dsDatos)
        End If
        Dim sScript As String
        sScript = "var Usuario='" + FSNUser.Cod + "';"
        sScript = sScript + "var UrlCompletaDetalle='" + ConfigurationManager.AppSettings("rutaPM") + "noconformidad/detalleNoConformidad.aspx?volver=DetPuntNc&';"
        sScript = sScript + "var UrlCompletaComprobar = '" + ConfigurationManager.AppSettings("rutaFS") + "_common/comprobarEnProceso.aspx';"
        sScript = sScript + "var UrlCompletaComentario = '" + ConfigurationManager.AppSettings("rutaPM") + "seguimiento/NWcomentariossolic.aspx;'"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsCellClickHandler", sScript)

        sScript = "var DivScrll='" + div_Label.ClientID + "'; var lblScrll='" & label2.ClientID & "';"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsScrollVirtual", sScript)

        If Not Page.IsPostBack Then
            MostrarPanelNC()
        End If

        'Dim info As New Infragistics.Web.UI.GridControls.ColumnSummaryInfo
        'info.ColumnKey = "INSTANCIA"
        'info.Summaries.Add(Infragistics.Web.UI.GridControls.SummaryType.Sum)
        'wdgDatosX1.Behaviors.SummaryRow.ColumnSummaries.Add(info)

    End Sub
    ''' <summary>
    ''' Datos de las unqa para la ficha de calidad
    ''' </summary>
    ''' <param name="dsDatosUNQA">Datos de las unqa</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerUNQA(ByVal dsDatosUNQA As DataSet) As Collection
        Dim cfiltroUNQA As New Collection
        Dim cfiltro As New Collection()
        If Not dsDatosUNQA.Tables(3).Rows.Count = 0 Then
            With dsDatosUNQA.Tables(3)
                cfiltro.Add(_Unidad, "ID")
                cfiltro.Add(_Nivel, "NIVEL")
                cfiltro.Add(.Rows(0)("DEN"), "DEN")
                cfiltroUNQA.Add(cfiltro, _Variable)
            End With
        End If
        'ObtenerFiltroUNQAHijos(cfiltroUNQA, dsDatosUNQA.Tables(0), dsDatosUNQA.Tables(1).Rows(0)("ID"), 1)
        Return cfiltroUNQA
    End Function
    ''' <summary>
    ''' Carga los grids nada mas entrar
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde:Evento que salta despues de cargarse la pagina; Tiempo máximo=0,8 (Depende de los datos)</remarks>
    Private Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If _VarOpcionConf = VarCalOpcionConf.EvaluarFormula Then
            If Not Me.IsPostBack Then
                If lblEtiqX1.Visible Then Me.InsertarEnCache("dtDatosX1_" & FSNUser.Cod, wdgDatosX1.DataSource)
                If lblEtiqX2.Visible Then Me.InsertarEnCache("dtDatosX2_" & FSNUser.Cod, wdgDatosX2.DataSource)
                If lblEtiqX3.Visible Then Me.InsertarEnCache("dtDatosX3_" & FSNUser.Cod, wdgDatosX3.DataSource)
                If lblEtiqX4.Visible Then Me.InsertarEnCache("dtDatosX4_" & FSNUser.Cod, wdgDatosX4.DataSource)
                If lblEtiqX5.Visible Then Me.InsertarEnCache("dtDatosX5_" & FSNUser.Cod, wdgDatosX5.DataSource)
            Else
                m_bSaltarLayout = 1
                If lblEtiqX1.Visible Then
                    BorrarGrid(wdgDatosX1)
                    wdgDatosX1.DataSource = CType(Cache("dtDatosX1_" & FSNUser.Cod), DataTable)
                    CrearConfGrid(1, wdgDatosX1)
                End If
                If lblEtiqX2.Visible Then
                    BorrarGrid(wdgDatosX2)
                    wdgDatosX2.DataSource = CType(Cache("dtDatosX2_" & FSNUser.Cod), DataTable)
                    CrearConfGrid(2, wdgDatosX2)
                End If
                If lblEtiqX3.Visible Then
                    BorrarGrid(wdgDatosX3)
                    wdgDatosX3.DataSource = CType(Cache("dtDatosX3_" & FSNUser.Cod), DataTable)
                    CrearConfGrid(3, wdgDatosX3)
                End If
                If lblEtiqX4.Visible Then
                    BorrarGrid(wdgDatosX4)
                    wdgDatosX4.DataSource = CType(Cache("dtDatosX4_" & FSNUser.Cod), DataTable)
                    CrearConfGrid(4, wdgDatosX4)
                End If
                If lblEtiqX5.Visible Then
                    BorrarGrid(wdgDatosX5)
                    wdgDatosX5.DataSource = CType(Cache("dtDatosX5_" & FSNUser.Cod), DataTable)
                    CrearConfGrid(5, wdgDatosX5)
                End If
            End If
        Else
            tbDatosX.Style.Item(HtmlTextWriterStyle.Display) = "none"
        End If
    End Sub
#Region "Fns ModalPopup"
    ''' <summary>
    ''' Muestra el modal popup de No Conformidades
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo ejecucion:0,3seg.</remarks>
    Private Sub MostrarPanelNC()

        Me.lblVarCodDen.Text = dtDatosVC.Item("VC_DEN")
        Me.lblDetalle.Text = Textos(0) 'Detalle de puntuación
        Me.lblImprimir.InnerText = Textos(16)

        Dim oDetalle As FSNServer.Puntuacion = FSNServer.Get_Object(GetType(FSNServer.Puntuacion))
        Dim ds As DataSet = oDetalle.DetallePuntuacion(_Variable, _Nivel, Me.Idioma, _ProveCod, _Unidad, _Subtipo)

        Dim fila As New HtmlTableRow
        Dim celda As New HtmlTableCell
        Dim mi_label As New Label

        Dim TextAux As String

        '''''''''''''
        'Proveedor:
        Me.lblDetPuntProve.Text = Textos(5)

        Me.lblDetPuntProveDat.Text = _ProveCod & " - " & _ProveDen
        'Fin Proveedor:

        '''''''''''''
        'UnQA:
        Me.lblEnUni.Text = Textos(6)

        Me.lblEnUniDat.Text = DenUnidad
        'Fin UnQA:

        '''''''''''''
        'Puntuacion:
        Me.lblPunt.Text = Textos(35)

        Me.lblPuntDat.Text = modUtilidades.FormatNumber(_Puntos, FSNUser.NumberFormat) & "(" & _Calif & ")"
        'Fin Puntuacion:

        '''''''''''''
        'Lista Padres
        fila = New HtmlTableRow

        celda = New HtmlTableCell

        mi_label = New Label()
        mi_label.CssClass = "RotuloDetallePuntuacionFondoGrisBold"
        mi_label.Text = Textos(1) 'Variable:

        celda.Controls.Add(mi_label) 'etiqueta Variable

        For i As Integer = 1 To _Nivel
            mi_label = New Label()

            TextAux = "/"
            If i = _Nivel Then TextAux = ""

            mi_label.CssClass = "fp_celda_vc_" & CStr(i) & "_DetallePuntuacion"
            If i < _Nivel Then
                mi_label.Text = ds.Tables(0).Rows(0).Item("VAR" & CStr(i)) & TextAux
            Else
                mi_label.Text = Me.lblVarCodDen.Text
            End If

            celda.Controls.Add(mi_label) 'Padre de nivel i
        Next

        fila.Controls.Add(celda)
        Me.tblPadre.Controls.Add(fila)
        'Fin Lista Padres

        '''''''''''''
        'Material Qa:
        Me.lblMatQa.Text = Textos(2)

        Me.lblMatQaDat.Text = ""

        If DBNullToStr(ds.Tables(0).Rows(0).Item("MAT")) <> "" Then
            Me.lblMatQaDat.Text = ds.Tables(0).Rows(0).Item("MAT")
        End If
        'Fin Material Qa

        '''''''''''''
        'Periodo:
        Me.lblPeriodo.Text = Textos(3) 'Periodo:

        Me.lblPeriodoDat.Text = ds.Tables(0).Rows(0).Item("PERIODO") & " " & Textos(4) 'meses
        'Fin Periodo:

        '''''''''''''
        'Origen:
        Me.lblOrigen.Text = Textos(17) 'Origen:

        Me.lblOrigenDat.Text = ds.Tables(0).Rows(0).Item("ORIGEN")
        'Fin Origen:

        '''''''''''''
        'calculo del numero de no conformidades:
        Me.lblCalcNum.Text = Textos(18) 'calculo del numero de no conformidades:

        Select Case ds.Tables(0).Rows(0).Item("TIPOPOND")
            Case TipoPonderacionVariables.PondNCNumero
                Me.lblCalcNumDat.Text = Textos(19)
            Case TipoPonderacionVariables.PondNCMediaPesos
                Me.lblCalcNumDat.Text = Textos(20)
            Case TipoPonderacionVariables.PondNCSumaPesos
                Me.lblCalcNumDat.Text = Textos(21)
            Case Else
                Me.lblCalcNumDat.Text = ""
        End Select
        'Fin calculo del numero de no conformidades:

        '''''''''''''
        'Formula:
        Me.lblFormula.Text = Textos(36) 'Formula
        _VarOpcionConf = ds.Tables(0).Rows(0)("OPCION_CONF")
        If _VarOpcionConf = VarCalOpcionConf.MediaPonderadaSegunVarHermana Then
            lblFormulaTexto.Text = Textos(52).Replace("@VARCAL", ds.Tables(0).Rows(0)("DEN_VAR_POND"))
        Else
            Me.lblFormulaTexto.Text = ds.Tables(0).Rows(0).Item("FORMULA")
        End If
        'Fin Formula:

        ''''''''''''''
        'GrupoX1..7
        ''''''''''''''
        Dim bHayX1 As Boolean = False
        Dim bHayX2 As Boolean = False
        Dim bHayX3 As Boolean = False
        Dim bHayX4 As Boolean = False
        Dim bHayX5 As Boolean = False

        Dim IdsNcX1 As String = ""
        Dim IdsNcX2 As String = ""
        Dim IdsNcX3 As String = ""
        Dim IdsNcX4 As String = ""

        If _VarOpcionConf = VarCalOpcionConf.EvaluarFormula Then
            For Each RowBD As DataRow In ds.Tables(1).Rows
            fila = New HtmlTableRow
            fila.Height = "25px"

            celda = New HtmlTableCell
            celda.Style.Add("width", "60px")
            celda.Attributes.Add("class", "FondoGrisBorderTablaDetallePuntuacionCeldaGris")

            mi_label = New Label()
            mi_label.CssClass = "RotuloDetallePuntuacionFondoGrisBoldNoPad"
            mi_label.Text = RowBD.Item("VARIABLEX") 'Xi

            celda.Controls.Add(mi_label) 'etiqueta Xi

            fila.Controls.Add(celda)
            '''''
            celda = New HtmlTableCell
            celda.Style.Add("width", "350px")
            celda.Attributes.Add("class", "FondoGrisBorderTablaDetallePuntuacionCeldaGrisCentral")

            mi_label = New Label()
            mi_label.CssClass = "RotuloDetallePuntuacionFondoGrisBoldNoPad"
            mi_label.Text = DameTrozo(RowBD.Item("VARIABLEX"), DBNullToStr(ds.Tables(0).Rows(0).Item("DEN"))) 'Suelo ó Objetivo ó ...

            celda.Controls.Add(mi_label) 'Suelo ó Objetivo ó ...

            fila.Controls.Add(celda)
            ''''''
            celda = New HtmlTableCell
            celda.Style.Add("width", "100px")
            celda.Attributes.Add("class", "FondoGrisBorderTablaDetallePuntuacionCeldaWhite")

            mi_label = New Label()
            mi_label.CssClass = "RotuloDetallePuntuacionFondoGrisBoldNoPad"
            mi_label.Text = DBNullToDbl(RowBD.Item("VALORX")) 'Valor

            celda.Controls.Add(mi_label) 'Valor

            fila.Controls.Add(celda)

            Me.tblVarsX.Controls.Add(fila)

            If Not (DBNullToDbl(RowBD.Item("VALORX")) = 0) Then
                Select Case UCase(RowBD.Item("VARIABLEX"))
                    Case "X1"
                        bHayX1 = True
                        IdsNcX1 = DBNullToStr(RowBD.Item("NC_IDS"))
                    Case "X2"
                        bHayX2 = True
                        IdsNcX2 = DBNullToStr(RowBD.Item("NC_IDS"))
                    Case "X3"
                        bHayX3 = True
                        IdsNcX3 = DBNullToStr(RowBD.Item("NC_IDS"))
                    Case "X4"
                        bHayX4 = True
                        IdsNcX4 = DBNullToStr(RowBD.Item("NC_IDS"))
                    Case "X5"
                        bHayX5 = True
                    Case Else
                        'Objetivo/Suelo
                End Select
            End If
        Next
        '''''''''''''''''''
        Dim oNC As FSNServer.NoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
        Dim dsDatos As DataSet = oNC.DatosPuntuacionNC(bHayX1, IdsNcX1, bHayX2, IdsNcX2, bHayX3, IdsNcX3, bHayX4, IdsNcX4, bHayX5, _ProveCod, DBNullToStr(ds.Tables(0).Rows(0).Item("MATERIAL_QA")), _Unidad, Me.Idioma)
        Dim IdDatosGrid As Integer = -1

            For Each RowBD As DataRow In ds.Tables(1).Rows
                If Not (DBNullToDbl(RowBD.Item("VALORX")) = 0) Then
                    IdDatosGrid = IdDatosGrid + 1

                    Select Case UCase(RowBD.Item("VARIABLEX"))
                        Case "X1"
                            lblEtiqX1.Visible = True
                            wdgDatosX1.Visible = True

                            lblEtiqX1.Text = DameTrozo(RowBD.Item("VARIABLEX"), DBNullToStr(ds.Tables(0).Rows(0).Item("DEN")))
                            BorrarGrid(wdgDatosX1)
                            wdgDatosX1.DataSource = dsDatos.Tables(IdDatosGrid)
                            CrearConfGrid(1, wdgDatosX1)
                        Case "X2"
                            lblEtiqX2.Visible = True
                            wdgDatosX2.Visible = True

                            lblEtiqX2.Text = DameTrozo(RowBD.Item("VARIABLEX"), DBNullToStr(ds.Tables(0).Rows(0).Item("DEN")))
                            BorrarGrid(wdgDatosX2)
                            wdgDatosX2.DataSource = dsDatos.Tables(IdDatosGrid)
                            CrearConfGrid(2, wdgDatosX2)
                        Case "X3"
                            lblEtiqX3.Visible = True
                            wdgDatosX3.Visible = True

                            lblEtiqX3.Text = DameTrozo(RowBD.Item("VARIABLEX"), DBNullToStr(ds.Tables(0).Rows(0).Item("DEN")))
                            BorrarGrid(wdgDatosX3)
                            wdgDatosX3.DataSource = dsDatos.Tables(IdDatosGrid)
                            CrearConfGrid(3, wdgDatosX3)
                        Case "X4"
                            lblEtiqX4.Visible = True
                            wdgDatosX4.Visible = True

                            lblEtiqX4.Text = DameTrozo(RowBD.Item("VARIABLEX"), DBNullToStr(ds.Tables(0).Rows(0).Item("DEN")))
                            BorrarGrid(wdgDatosX4)
                            wdgDatosX4.DataSource = dsDatos.Tables(IdDatosGrid)
                            CrearConfGrid(4, wdgDatosX4)
                        Case "X5"
                            lblEtiqX5.Visible = True
                            wdgDatosX5.Visible = True

                            lblEtiqX5.Text = DameTrozo(RowBD.Item("VARIABLEX"), DBNullToStr(ds.Tables(0).Rows(0).Item("DEN")))
                            BorrarGrid(wdgDatosX5)
                            wdgDatosX5.DataSource = dsDatos.Tables(IdDatosGrid)
                            CrearConfGrid(5, wdgDatosX5)
                    End Select
                End If
            Next
        End If
        '''''''''''''''''''

    End Sub

    ''' <summary>
    ''' Saca de entre la Leyenda de las variables implicadas en una formula la descripción de la variable indicada
    ''' </summary>
    ''' <param name="Variable">Variable de la q sacar su descripción</param>
    ''' <param name="DenomVariable">Leyenda de las variables implicadas en una formula</param>
    ''' <returns>descripción de la variable indicada</returns>
    ''' <remarks>Llamada desde: MostrarPanelNC; Tiempo máximo:0</remarks>
    Private Function DameTrozo(ByVal Variable As String, ByVal DenomVariable As String) As String
        'X1: Open non-conformities;X2: Non-conformities closed within deadline;X3: Non-conformities closed out of deadline;X4: Non-conformities with a negative closure
        Dim Aux As String = ""
        If InStr(DenomVariable, Variable & ":") > 0 Then
            Aux = DenomVariable.Substring(InStr(DenomVariable, Variable & ":") - 1)
        Else
            Return ""
        End If
        'X3: Non-conformities closed out of deadline;X4: Non-conformities with a negative closure
        If InStr(Aux, ";") = 0 Then
            DameTrozo = Aux.Substring(4)
        Else
            DameTrozo = Aux.Substring(4, InStr(Aux, ";") - 5)
        End If
    End Function
#End Region

#Region "Grid Datos"
    Private Sub BorrarGrid(ByVal GridDatos As Infragistics.Web.UI.GridControls.WebDataGrid)
        GridDatos.Rows.Clear()
        GridDatos.Columns.Clear()
    End Sub
    Private Sub CrearConfGrid(ByVal Caso As Integer, ByVal GridDatos As Infragistics.Web.UI.GridControls.WebDataGrid)
        CrearColumnas(GridDatos)
        GridDatos.DataBind()
        GridDatos_InicializarLayout(Caso, GridDatos)
    End Sub
    Private Sub CrearColumnas(ByVal GridDatos As Infragistics.Web.UI.GridControls.WebDataGrid)
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField

        Dim nombre As String

        For i As Integer = 0 To GridDatos.DataSource.Columns.Count - 1
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            nombre = GridDatos.DataSource.Columns(i).ToString
            With campoGrid
                .Key = nombre
                .DataFieldName = nombre
                .Header.Text = nombre
            End With
            GridDatos.Columns.Add(campoGrid)
        Next
    End Sub
    ''' <summary>
    ''' Inicializa el aspecto de las columnas de un grid
    ''' </summary>
    ''' <param name="Caso">De q grid se trata</param>
    ''' <param name="GridDatos">Grid a inicializar</param>
    ''' <remarks>Llamada desde:MostrarPanelNC; Tiempo ejecucion:0,1seg.</remarks>
    Private Sub GridDatos_InicializarLayout(ByVal Caso As Integer, ByVal GridDatos As Infragistics.Web.UI.GridControls.WebDataGrid)
        If m_bSaltarLayout > 0 Then Exit Sub

        m_bSaltarLayout = 0
        With GridDatos
            Select Case Caso
                Case 1
                    .Columns("ID").Hidden = True
                    .Columns("REVISOR").Hidden = True
                    .Columns("ESTADO").Hidden = True

                    .Columns("INSTANCIA").Header.Text = Textos(22) 'Identificador
                    .Columns("TITULO").Header.Text = Textos(23) 'tipo/titulo
                    .Columns("FEC_ALTA").Header.Text = Textos(24) 'fecha alta
                    .Columns("ART4").Header.Text = Textos(15) 'articulo

                    .Columns("INSTANCIA").Width = Unit.Pixel(80) '12
                    .Columns("TITULO").Width = Unit.Pixel(245) '36
                    .Columns("FEC_ALTA").Width = Unit.Pixel(100) '15
                    .Columns("ART4").Width = Unit.Pixel(220) '37

                    .Columns("INSTANCIA").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                    .Columns("TITULO").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                    .Columns("FEC_ALTA").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                    .Columns("ART4").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                    .Columns("INSTANCIA").CssClass = "RotuloDetallePuntuacionNoLeftLink"
                    .Columns("TITULO").CssClass = "RotuloDetallePuntuacionNoLeftLink"
                    .Columns("FEC_ALTA").CssClass = "RotuloDetallePuntuacionNoLeftLink"
                    .Columns("ART4").CssClass = "RotuloDetallePuntuacionRightNoLeftLink"

                    .Columns("FEC_ALTA").FormatValue(FSNUser.DateFormat.ShortDatePattern)

                Case 2, 3, 4
                    .Columns("ID").Hidden = True
                    .Columns("REVISOR").Hidden = True
                    .Columns("ESTADO").Hidden = True

                    .Columns("INSTANCIA").Header.Text = Textos(22) 'identificador
                    .Columns("TITULO").Header.Text = Textos(23) 'tipo/titulo
                    .Columns("FEC_CIERRE").Header.Text = Textos(25) 'fecha cierre
                    .Columns("FEC_ALTA").Header.Text = Textos(24) 'fecha alta
                    .Columns("ART4").Header.Text = Textos(15) 'articulo

                    .Columns("INSTANCIA").Width = Unit.Pixel(80)
                    .Columns("TITULO").Width = Unit.Pixel(145) '21
                    .Columns("FEC_CIERRE").Width = Unit.Pixel(100)
                    .Columns("FEC_ALTA").Width = Unit.Pixel(100)
                    .Columns("ART4").Width = Unit.Pixel(220)

                    .Columns("INSTANCIA").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                    .Columns("TITULO").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                    .Columns("FEC_CIERRE").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                    .Columns("FEC_ALTA").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                    .Columns("ART4").Header.CssClass = "fp_celda_cabecera_vcDetalle"

                    .Columns("INSTANCIA").CssClass = "RotuloDetallePuntuacionNoLeftLink"
                    .Columns("TITULO").CssClass = "RotuloDetallePuntuacionNoLeftLink"
                    .Columns("FEC_CIERRE").CssClass = "RotuloDetallePuntuacionNoLeftLink"
                    .Columns("FEC_ALTA").CssClass = "RotuloDetallePuntuacionNoLeftLink"
                    .Columns("ART4").CssClass = "RotuloDetallePuntuacionRightNoLeftLink"

                    .Columns("FEC_CIERRE").FormatValue(FSNUser.DateFormat.ShortDatePattern)
                    .Columns("FEC_ALTA").FormatValue(FSNUser.DateFormat.ShortDatePattern)
                Case 5
                    .Columns("FECHA").Header.Text = Textos(14) 'fecha 
                    .Columns("FECHA").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                    .Columns("FECHA").Width = Unit.Pixel(160) '24
                    .Columns("FECHA").FormatValue(FSNUser.DateFormat.ShortDatePattern)
                    .Columns("FECHA").CssClass = "RotuloDetallePuntuacionLeftGrid"
                    'Footer
                    .Columns("FECHA").Footer.Text = Textos(13)
                    .Columns("FECHA").Footer.CssClass = "RotuloDetallePuntuacionRightGridBold"

                    .Columns("ARTICULO").Header.Text = Textos(15) 'articulo
                    .Columns("ARTICULO").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                    .Columns("ARTICULO").Width = Unit.Pixel(220)  '36
                    .Columns("ARTICULO").CssClass = "RotuloDetallePuntuacionLeftGrid"
                    .Columns("ARTICULO").Footer.CssClass = "RotuloDetallePuntuacionLeftGridBorderL"

                    .Columns("IMPORTE_FACTURADO").Header.Text = Textos(10) 'importe facturado
                    .Columns("IMPORTE_FACTURADO").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                    .Columns("IMPORTE_FACTURADO").Width = Unit.Pixel(165) '25
                    .Columns("IMPORTE_FACTURADO").CssClass = "RotuloDetallePuntuacionLeftGridPadR"
                    .Columns("IMPORTE_FACTURADO").Footer.CssClass = "RotuloDetallePuntuacionRightGridBold"

                    .Columns("MON").Header.Text = Textos(26) 'moneda
                    .Columns("MON").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                    .Columns("MON").Width = Unit.Pixel(100) '15
                    .Columns("MON").CssClass = "RotuloDetallePuntuacionLeftRightGridPadL"
                    .Columns("MON").Footer.CssClass = "RotuloDetallePuntuacionLeftRightGridBold"
            End Select
        End With
    End Sub
#End Region

End Class
