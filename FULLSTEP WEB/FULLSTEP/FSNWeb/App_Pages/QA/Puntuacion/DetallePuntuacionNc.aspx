﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DetallePuntuacionNc.aspx.vb" Inherits="Fullstep.FSNWeb.DetallePuntuacionNc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head  id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" language="javascript">
        var xmlHttp;
        /*''' <summary>
        ''' Crear el objeto para llamar con ajax a ComprobarEnProceso
        ''' </summary>
        ''' <remarks>Llamada desde: javascript ; Tiempo máximo: 0</remarks>*/		    
		function CreateXmlHttp()
		{
		    
			// Probamos con IE
			try
			{
				// Funcionará para JavaScript 5.0
				xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch(e)
			{
				try
				{
					xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch(oc)
				{   
					xmlHttp = null;
				}
			}

			// Si no se trataba de un IE, probamos con esto
			if(!xmlHttp && typeof XMLHttpRequest != "undefined")
			{
				xmlHttp = new XMLHttpRequest();
			}

			return xmlHttp;
		}
	    //Creamos el objeto xmlHttpRequest
		CreateXmlHttp();   
	    		
        /*''' <summary>
        ''' Responder al evento click en una celda del grid
        ''' </summary>
        ''' <param name="sender">parametro de sistema</param>
        ''' <param name="e">parametro de sistema</param>   
        ''' <remarks>Llamada desde: sistema; Tiempo máximo:0</remarks>*/
		function wdgDatosX_CellSelectionChanged(sender, e) {
		    var row = e.getSelectedCells().getItem(0).get_row();

		    var Instancia = row.get_cellByColumnKey("INSTANCIA").get_value();
		    var NoConformidad = row.get_cellByColumnKey("ID").get_value();
		    var Revisor = row.get_cellByColumnKey("REVISOR").get_value();
		    var Estado = row.get_cellByColumnKey("ESTADO").get_value();
            if(xmlHttp){
                if (ComprobarEnProceso(Instancia)) {
                    var FechaLocal;
                    FechaLocal = new Date();
                    var otz = FechaLocal.getTimezoneOffset();

                    window.open(UrlCompletaDetalle + "NoConformidad=" + NoConformidad + "&Otz=" + otz.toString(), "_blank", "width=950,height=545,status=yes,resizable=yes,scrollbars=yes,top=120,left=20");
                }
            }
        }
        /*
        ''' <summary>
        ''' Funcion que Comprueba el Estado En proceso de una instancia
        ''' </summary>
        ''' <param name="instancia">instancia</param>
        ''' <remarks>
        ''' Llamada desde: wdgDatosX_CellSelectionChanged; Tiempo máximo: 0,25 seg</remarks>*/        
        function ComprobarEnProceso(instancia) {
	        var params ="ID=" + instancia;
	        
	        xmlHttp.open("POST", UrlCompletaComprobar, false);
		    xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
		    xmlHttp.send(params);

		    if (xmlHttp.responseText.substr(0,1) != 0) {
		        window.open(UrlCompletaComentario + "Instancia=" + instancia + "&EnProceso=1","_blank", "width=600,height=300,status=yes,resizable=no,top=200,left=200")
		        return false;
		    } else {
		        return true;
		    }
		}
    </script>        
</head>
<body class="FondoTablaDetallePuntuacion">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>    

        <table style="position:absolute;z-index:10002; width:50%">
            <tr>
                <td valign="top" align="left" style="padding:0px;width:80px;height:55px">
                    <asp:Image ID="imgDetalle" runat="server" SkinID="PopUpProveedor" Visible="true" />
                </td>
            </tr>                
        </table>  

        <asp:Panel ID="Panel1" runat="server">  
            <table style="position:relative;z-index:10001;width:100%;" cellpadding="0" cellspacing="0">
            <tr>
                <td valign="middle" align="left" style="width:75%;padding-left:80px;height:30px;"  class='CapaTituloPanelInfo'>
                    <div style='width:100%; height:30px;'>                        
                        <asp:Label runat="server" ID="lblVarCodDen" Text="DlblVarCodDen" CssClass="TituloPopUpPanelInfo"></asp:Label> 
                    </div>
                </td>
                <td valign="middle" style="text-align:right; width:25%"  class='CapaTituloPanelInfo'>
                    <asp:ImageButton ID="btnImprimir" runat="server" SkinID="Imprimirph" OnClientClick="window.print();return false;"/> 
                    <a runat="server" id="lblImprimir" class="TextoBlanco" href="javascript:window.print();">DImprimir</a>
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="middle" align="left" style="padding-left:80px;height:25px;" class='CapaSubTituloPanelInfo'>
                    <asp:Label runat="server" ID="lblDetalle" Text="dDetalle de puntuación" CssClass="SubTituloPopUpPanelInfo"></asp:Label>                    
                </td>                
            </tr> 
            <tr>
                <td colspan="2" class="SeparadorCabeceraDetallePuntuacion"> &nbsp;</td>
            </tr>    
            </table> 
            <div style="height:98%;"> 
                <table style="position:relative;z-index:10001;width:100%;" cellpadding="0" cellspacing="0">         
                    <tr>
                        <td colspan="2" class="FondoTablaDetallePuntuacion">
                            <div>
                                <asp:Label ID="lblDetPuntProve" runat ="server" Text ="DDetalle de puntuación del proveedor" CssClass="RotuloDetallePuntuacion"></asp:Label>
                                &nbsp;
                                <asp:Label ID="lblDetPuntProveDat" runat ="server" Text ="DProvecod - proveden" CssClass="RotuloDetallePuntuacionBold"></asp:Label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="FondoTablaDetallePuntuacion">
                            <div>
                                <asp:Label ID="lblEnUni" runat ="server" Text ="Den la unidad de negocio" CssClass="RotuloDetallePuntuacion"></asp:Label>
                                &nbsp;
                                <asp:Label ID="lblEnUniDat" runat ="server" Text ="DProvecod - proveden" CssClass="RotuloDetallePuntuacionBold"></asp:Label>
                            </div>
                        </td>
                    </tr>    
                    <tr>
                        <td colspan="2" class="SeparadorLineasDetallePuntuacion"> &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="FondoTablaDetallePuntuacion">
                            <table class="BordearDetallePuntuacion">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPunt" runat ="server" Text ="DPuntuacion" CssClass="RotuloDetallePuntuacionBoldLeft"></asp:Label>
                                    </td>
                                    <td style="width:3px">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPuntDat" runat ="server" Text ="DPuntuacion (Calificacion)" CssClass="RotuloDetallePuntuacionPuntCalf"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>   
                    <tr>
                        <td colspan="2" class="SeparadorLineasDetallePuntuacion"> &nbsp;</td>
                    </tr>                                                                                   
                    <tr>
                        <td colspan="2" class="FondoTablaDetallePuntuacion">
                            <table style="width:96%;" class="FondoGrisTablaDetallePuntuacion">
                            <tr>
                            <td class="RotuloDetallePuntuacionFondoGris" >
                                <asp:Panel id="pnlPadre" runat="server" ScrollBars="None">
                                    <table id="tblPadre" runat="server" border="0" style="WIDTH: 100%; height:12px;" cellpadding="0" cellspacing="0">
                                    </table>
                                </asp:Panel>            
                            </td>
                            </tr>                                         
                            <tr>
                                <td class="RotuloDetallePuntuacionFondoGris">
                                    <div>
                                        <asp:Label ID="lblMatQa" runat ="server" Text ="DMaterial Qa" CssClass="RotuloDetallePuntuacionFondoGrisBold"></asp:Label>
                                        &nbsp;
                                        <asp:Label ID="lblMatQaDat" runat ="server" Text ="DDen Material Qa"></asp:Label>
                                    </div>
                                </td>
                            </tr>  
                            <tr>
                                <td class="RotuloDetallePuntuacionFondoGris">
                                    <div>
                                        <asp:Label ID="lblPeriodo" runat ="server" Text ="DPeriodo" CssClass="RotuloDetallePuntuacionFondoGrisBold"></asp:Label>
                                        &nbsp;
                                        <asp:Label ID="lblPeriodoDat" runat ="server" Text ="D6 meses"></asp:Label>
                                    </div>
                                </td>
                            </tr>    
                            <tr>
                                <td class="RotuloDetallePuntuacionFondoGris">
                                    <div>
                                        <asp:Label ID="lblOrigen" runat ="server" Text ="DOrigen" CssClass="RotuloDetallePuntuacionFondoGrisBold"></asp:Label>
                                        &nbsp;
                                        <asp:Label ID="lblOrigenDat" runat ="server" Text ="DIso - 9001"></asp:Label>
                                    </div>
                                </td>
                            </tr>  
                            <tr>
                                <td class="RotuloDetallePuntuacionFondoGris">
                                    <div>
                                        <asp:Label ID="lblCalcNum" runat ="server" Text ="DCalculo del num" CssClass="RotuloDetallePuntuacionFondoGrisBold"></asp:Label>
                                        &nbsp;
                                        <asp:Label ID="lblCalcNumDat" runat ="server" Text ="DNumero de no conformidades"></asp:Label>
                                    </div>                        
                                </td>
                            </tr>                              
                            </table>            
                        </td>   
                    </tr>                                              
                    <tr>
                        <td colspan="2" class="SeparadorLineasDetallePuntuacion"> &nbsp;</td>
                    </tr>                              
                    <tr>
                        <td colspan="2" class="FondoTablaDetallePuntuacion">
                            <table style="width:96%;" class="FondoGrisTablaDetallePuntuacion">
                                <tr>
                                    <td class="RotuloDetallePuntuacionFondoGrisBold">
                                        <asp:Label runat="server" ID="lblFormula" Text="dFormula:"></asp:Label>
                                    </td>
                                </tr>          
                                <tr>
                                    <td class="RotuloDetallePuntuacionFondoGrisPad" style="overflow:auto">
                                        <asp:Label runat="server" ID="lblFormulaTexto" Text="dFormulaCompleta"></asp:Label>
                                    </td>
                                </tr>    
                            </table>   
                        </td>
                    </tr>                
                    <tr>
                        <td colspan="2" class="SeparadorLineasDetallePuntuacion"> &nbsp;</td>
                    </tr>        
                    <tr>
                        <td colspan="2" class="FondoTablaDetallePuntuacion">
                            <table class="FondoGrisBorderTablaDetallePuntuacion">
                                <tr>
                                    <td>                
                                        <asp:Panel id="panelX" runat="server" ScrollBars="None">
                                            <table id="tblVarsX" runat="server" style="WIDTH: 400px;" cellpadding="0" cellspacing="0"></table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>                                
                    <tr>                  
                        <td colspan="2">	
                            <table class="FondoTablaDetallePuntuacionMargin" id="tbDatosX" runat="server">
                                <tr>
                                    <td style="width:100%;">
                                        <asp:Label runat="server" ID="lblEtiqX1" Text="dAbiertas" CssClass="RotuloDetallePuntuacionlabelNC" Height="20px" Visible="false"></asp:Label>                                             
                                        <ig:WebDataGrid ID="wdgDatosX1" runat="server" Visible="false"
                                        AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="false" >
                                            <Behaviors>
                                                <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
						                        <ig:ColumnMoving Enabled="true"></ig:ColumnMoving>
                                                <ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting> 
                                                <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Cell" >
                                                    <SelectionClientEvents CellSelectionChanged= "wdgDatosX_CellSelectionChanged" />
                                                </ig:Selection>
                                            </Behaviors> 
                                        </ig:WebDataGrid>
                                        <asp:Label runat="server" ID="lblEtiqX2" Text="dCerradas dentro" CssClass="RotuloDetallePuntuacionlabelNC" Height="20px" Visible="false"></asp:Label>
                                        <ig:WebDataGrid ID="wdgDatosX2" runat="server" Visible="false"
                                        AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="false" >
                                            <Behaviors>
                                                <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
						                        <ig:ColumnMoving Enabled="true"></ig:ColumnMoving>
                                                <ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting>  
                                                <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Cell" >
                                                    <SelectionClientEvents CellSelectionChanged= "wdgDatosX_CellSelectionChanged" />
                                                </ig:Selection>
                                            </Behaviors> 
                                        </ig:WebDataGrid>
                                        <asp:Label runat="server" ID="lblEtiqX3" Text="dCerradas fuera" CssClass="RotuloDetallePuntuacionlabelNC" Height="20px" Visible="false"></asp:Label>
                                        <ig:WebDataGrid ID="wdgDatosX3" runat="server" Visible="false"
                                        AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="false" >
                                            <Behaviors>
                                                <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
						                        <ig:ColumnMoving Enabled="true"></ig:ColumnMoving>
                                                <ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting>  
                                                <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Cell" >
                                                    <SelectionClientEvents CellSelectionChanged= "wdgDatosX_CellSelectionChanged" />
                                                </ig:Selection>
                                            </Behaviors> 
                                        </ig:WebDataGrid>
                                        <asp:Label runat="server" ID="lblEtiqX4" Text="dCerradas negativo" CssClass="RotuloDetallePuntuacionlabelNC" Height="20px" Visible="false"></asp:Label>
                                        <ig:WebDataGrid ID="wdgDatosX4" runat="server" Visible="false"
                                        AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="false" >
                                            <Behaviors>
                                                <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
						                        <ig:ColumnMoving Enabled="true"></ig:ColumnMoving>
                                                <ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting>  
                                                <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Cell" >
                                                    <SelectionClientEvents CellSelectionChanged= "wdgDatosX_CellSelectionChanged" />
                                                </ig:Selection>
                                            </Behaviors> 
                                        </ig:WebDataGrid>
                                        <asp:Label runat="server" ID="lblEtiqX5" Text="dFacturacion" CssClass="RotuloDetallePuntuacionlabelNC" Height="20px" Visible="false"></asp:Label>
                                        <ig:WebDataGrid ID="wdgDatosX5" runat="server" Visible="false" ShowFooter="true"
                                        AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="false" >
                                            <Behaviors>
                                                <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
						                        <ig:ColumnMoving Enabled="true"></ig:ColumnMoving>
                                                <ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting>
                                                <ig:SummaryRow AnimationType="Linear">
                                                    <ColumnSummaries>
                                                        <ig:ColumnSummaryInfo ColumnKey="IMPORTE_FACTURADO">
                                                            <Summaries>
                                                                <ig:Summary SummaryType="Sum" />
                                                            </Summaries>
                                                        </ig:ColumnSummaryInfo>
                                                    </ColumnSummaries>
                                                </ig:SummaryRow>
                                            </Behaviors> 
                                        </ig:WebDataGrid>
                                        <div id="div_Label" runat="server" class="EtiquetaScroll" style="display:none;position:absolute">
                                            <asp:Label ID="label2" runat="server" Font-Bold="true"></asp:Label>
                                        </div>                                         
                                    </td>
                                </tr>
                            </table>    
                        </td>
                    </tr>                      
                </table>
            </div>            
        </asp:Panel>        
    </form>
</body>
</html>
