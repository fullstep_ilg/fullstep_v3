﻿$(document).ready(function () {
    //Poner textos Cabecera
    $('#tblValoresCampo tr:first td:nth-child(1)').text(TextosCabecera[0]);
    $('#tblValoresCampo tr:first td:nth-child(2)').text(TextosCabecera[1]);
    $('#tblValoresCampo tr:first td:nth-child(3)').text(TextosCabecera[2]);
    $('#tblValoresCampo tr:first td:nth-child(4)').text(TextosCabecera[3]);
    $('#tblValoresCampo tr:first td:nth-child(5)').text(TextosCabecera[4]);
    $('#divPuntuacionGrid div:first').text(TextosCabecera[5]);
    var datosEncuestasPantalla = $.parseJSON(datosEncuestas);
    $('#tblValoresCampo').css('width', (63 + (datosEncuestasPantalla[1].length * 5)) + 'em');
    $('#divPuntos').tmpl(datosEncuestasPantalla[0]).appendTo('#divBotonesGrid');
    $('#tdCabeceraCampo').tmpl(datosEncuestasPantalla[1]).appendTo('#tblValoresCampo tr:first');
    $('#trValorCampo').tmpl(datosEncuestasPantalla[2]).appendTo('#tblValoresCampo');
    $('#trValorCampoVacio').tmpl(datosEncuestasPantalla[4]).appendTo('#tblValoresCampo');
    $('#divPuntuacion').tmpl(datosEncuestasPantalla[3]).appendTo('#divPuntuacionGrid');
    $('#divTotalPuntuacion').tmpl(datosEncuestasPantalla[4]).appendTo('#divPuntuacionGrid');
});
$('#divBotonesGrid div[id^=img]').live('click', function () {    
    var instancia = $(this).attr('id').replace('img', '');    
    window.open(rutaPM + "workflow/detalleSolicConsulta.aspx?Instancia=" + instancia + "&EsObservador=1", "_blank", 'width=950,height=600,status=yes,resizable=yes,scrollbars=yes,top=30,left=30');    
});
function getPlantillaAlineacion(index) {
    switch (index) {
        case 1:
        case 2:
        case 3:
        case 4:
            return "#tdValorCampoDerecha";
            break;
        default:
            return "#tdValorCampoIzquierda";
            break;

    }
}
$(window).resize(function () {
    if ($('#tblValoresCampo').outerWidth() < $('#divDatosGrid').outerWidth())
        $('#divPuntuacionGrid').css('right', ($('#divDatosGrid').outerWidth() - $('#tblValoresCampo').outerWidth()));
    else
        $('#divPuntuacionGrid').css('right', 0);
});