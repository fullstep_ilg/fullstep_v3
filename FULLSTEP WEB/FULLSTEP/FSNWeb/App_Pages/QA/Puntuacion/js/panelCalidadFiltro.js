var txtMaterialesQA;
var tvMaterialesQA;
var cerrojo = false;
var cont;
var tipofiltro;

$(document).ready(function () {  
    //por si hubiera nodos seleccionados y venimos de un update panel
    //los rellenamos de nuevo    
    if (tipofiltro == 0) {
        $(".materiales").hide();
    } else {
        txtMaterialesQA = $('#txtMaterialesQA');
        tvMaterialesQA = $('#tvMaterialesQA');
        inicializarTokenInput();
        inicializarTree();
        $("[id$=cblMaterialesQA]").hide();        
    };
});

function addAllNodes(bDoPost) {    
    var obj = { data: txtMaterialesQA.tokenInput("get") };
    var nodes = JSON.stringify(obj);
    $.ajax({
        method: "POST",
        url: rutaFS + 'QA/puntuacion/panelCalidadFiltro.aspx/addAllNodes',
        data: nodes,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async:false
    });
}

function addNode(key) {
    params = { key: key };
    $.when($.ajax({
        method: "POST",
        url: rutaFS + 'QA/puntuacion/panelCalidadFiltro.aspx/addNode',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: true
    }));
}

function getSelectedNodes() {    
    $.when($.ajax({
        method: "POST",
        url: rutaFS + 'QA/puntuacion/panelCalidadFiltro.aspx/getSelectedNodes',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
    })).done(function (msg) {
        cerrojo = true;
        txtMaterialesQA.tokenInput("addRange", msg.d);
        cerrojo = false;
        checkNodosSeleccionados();
    });
}

function contiene(nodo1, nodo2) {    
    n1 = nodo1.id.split("###");
    nivel1 = n1.length - 1;

    n2 = nodo2.id.split("###");
    nivel2 = n2.length - 1;

   if (nivel1 < nivel2) {
       switch (nivel1) {
            case 0:
                return n1[0]==n2[0];
                break;
            case 1:
                return n1[0] == n2[0] && n1[1] == n2[1];
                break;
            case 2:
                return n1[0]==n2[0] && n1[1]==n2[1] && n1[2]==n2[2];
                break;
            case 3:
                return n1[0]==n2[0] && n1[1]==n2[1] && n1[2]==n2[2] && n1[3]==n2[3];
                break;
            case 4:
                return n1[0]==n2[0] && n1[1]==n2[1] && n1[2]==n2[2] && n1[3]==n2[3] && n1[4]==n2[4];
                break;
        }
    }
    return false;
}

function inicializarTree() {
    $('#tvMaterialesQA').on('loaded.jstree', function () {
        $('#tvMaterialesQA').jstree('close_all');
        $('#tvMaterialesQA').jstree('open_node', '##');
    }).jstree({
        'core': {
            'data': {
                'type': 'POST',
                'dataType': 'json',
                'async': true,
                'contentType': 'application/json;',
                'url': rutaFS + 'QA/puntuacion/panelCalidadFiltro.aspx/Cargar_Nodo_Materiales',
                'data': function (node) {
                    if ($(node).attr('id') == '#') {
                        return '{ "operation" : "get_children", "key" : "" }';
                    }
                    else {
                        //get the children for this node
                        return '{ "operation" : "get_children", "key" : "' + $(node).attr("id") + '"} ';
                    };
                },
                'success': function (retval) {
                    return retval.d;
                }
            },
            multiple: true,
            undetermined: false
        },
        plugins: ["checkbox"],
        checkbox: {
            keep_selected_style: false,
            three_state: false,
            cascade: "up down"
        }
    }).on("changed.jstree", function (e, data) {
        //al modificar un nodo actualizar el tokenInput
        if (!cerrojo && !(data.action == 'ready')) {
            if (data.action == 'deselect_node') {
                txtMaterialesQA.tokenInput("remove", { id: data.node.id });

                //si el nodo a borrar est� contenido en alguno de los nodos del token input lo borramos
                $.each(txtMaterialesQA.tokenInput("get"), function () {
                    if (contiene(this, data.node)) {
                        txtMaterialesQA.tokenInput("remove", { id: this.id });
                    }
                });
            } else {
                txtMaterialesQA.tokenInput("add", { id: data.node.id, name: data.node.text });
            }
            //Rellenamos el token input con los nodos superiores del arbol
            $.each(tvMaterialesQA.jstree("get_top_checked", "full"), function () {
                if (this.id != "") {
                    txtMaterialesQA.tokenInput("add", { id: this.id, name: this.text });
                }
            });
            //Recuperamos el foco del arbol, ya que al a�adir/borrar tokens se queda en el tokenInput
            tvMaterialesQA.focus();
        }
    }).on("after_open.jstree", function (e, data) {
        checkNodosSeleccionados();
    }).on("loaded.jstree", function () {
        getSelectedNodes();
    });
}

function inicializarTokenInput() {
    txtMaterialesQA.tokenInput('panelCalidadFiltro.aspx/Obtener_Materiales', {
        theme: "facebook",
        hintText: TextosJScript[80],
        searchingText: TextosJScript[81],
        noResultsText: TextosJScript[82],
        minChars: 3,
        resultsLimit: 50,
        preventDuplicates: true,
        method: "POST",
        onAdd: function (item) {
            if (!cerrojo) {
                cerrojo = true;
                cont = false;
                //si el nodo a agregar ya est� contenido en la selecci�n no lo metemos
                $.each(txtMaterialesQA.tokenInput("get"), function () {
                    if (contiene(this, item)) {
                        cont = true;
                    }
                });
                if (cont) {
                    txtMaterialesQA.tokenInput("remove", { id: item.id });
                } else {
                    //si el nodo introducido contiene alguno de los nodos actuales, eliminamos sus nodos descendientes
                    $.each(txtMaterialesQA.tokenInput("get"), function () {
                        if (contiene(item, this)) {
                            txtMaterialesQA.tokenInput("remove", { id: this.id });
                        }
                    });

                }

                cerrojo = false;
            }
            checkNodosSeleccionados();
            if (tipofiltro == 0) {
                //Si el tipo de filtro es materiales de QA tenemos que actualizar el arbol de 
                //variables de QA, como y a estaba antes
                addAllNodes(true);
            }
        },
        onDelete: function (item) {
            if (!cerrojo) {
                cerrojo = true;
                tvMaterialesQA.jstree("uncheck_node", { id: item.id });
                checkNodosSeleccionados();
                cerrojo = false;
            }
        },
        onReady: function () {
            //Ponemos el display de la lista inline-block
            $("ul[class^=token-input-list]", txtMaterialesQA.parent()).css('display', 'inline-block');
            $("ul[class^=token-input-list]", txtMaterialesQA.parent()).css('vertical-align', 'middle');
        }
    });   
}

function checkNodosSeleccionados() {
    //cerrojo = true;
    $.each(txtMaterialesQA.tokenInput("get"), function () {
        tvMaterialesQA.jstree("check_node", { id: this.id });
    });
    //cerrojo = false;
}