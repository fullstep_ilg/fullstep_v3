﻿$(document).ready(function () {
    $('tr[mkr=sizeHeaderRow]').hide();
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(AlinearGrid);
    VisibilidadColumnas();
});
function AlinearGrid() {
    $('tr[mkr=sizeHeaderRow]').hide();
};
//***************************************************************************
//PANEL CALIDAD OCULTAR/MOSTRAR COLUMNAS
//***************************************************************************
/*<summary>
Oculta la columna identificada por columnaKey, adecua el colspan de la multicabecera y si son columnas del proveedor actualiza el contenido de hCamposOcultos
</summary>
<param name="columnaKey">Key de la columna a ocultar</param>
<param name="multicolumnaIndice">Indice de la multicabcera que se corresponde con la columna a ocultar</param>
<param name="guardarCamposOcultos">true si es una columna del proveedor</param>
<remarks>Llamada desde los iconos de cerrar de las cabeceras de las columnas</remarks>*/
function OcultarColumna(columnaKey, multicolumnaIndice,guardarCamposOcultos) {
    var _grid = $find($('[id$=wdgProveedores]').attr('id'));
    //Si son datos de proveedor guardamos la col oculta
    if(guardarCamposOcultos){                   
        var hCamposOcultos = $('[id$=hCamposOcultos]').val();
        if(hCamposOcultos!='') hCamposOcultos=hCamposOcultos+',';
        
        hCamposOcultos=hCamposOcultos+columnaKey;
        $('[id$=hCamposOcultos]').val(hCamposOcultos);
    }
    var columna = _grid.get_columns().get_columnFromKey(columnaKey);
    columna.set_hidden(true);
    VisibilidadColumnas();
};
/*<summary>
Oculta las columnas agrupadas bajo la variable de calidad a cerrar
</summary>
<param name="sColumnasUNQAIds">Lista de Ids de las unqas de las columnas de puntuaciones de la variable de calidad a ocultar</param>
<param name="multicolumnaIndice">Indice de la multicabcera  a ocultar</param>
<remarks>Llamada desde los iconos de cerrar de las cabeceras de las variables de calidad</remarks>*/
function OcultarColumnaVarCal(sColumnasUNQAIds, multicolumnaIndice) {		
    var _grid = $find($('[id$=wdgProveedores]').attr('id'));
    var aColumnasUNQAIds = sColumnasUNQAIds.split(",");
    var lColumnasUNQAIds = aColumnasUNQAIds.length;
    var iArray = 0;
    var columna,cabeceraColumna;     
    while (iArray < lColumnasUNQAIds) {
        columna = _grid.get_columns().get_columnFromKey("PUNT_" + aColumnasUNQAIds[iArray]);
        columna.set_hidden(true);
        columna = _grid.get_columns().get_columnFromKey("CAL_" + aColumnasUNQAIds[iArray]);
        columna.set_hidden(true);          
        iArray += 1;
    }
    VisibilidadColumnas();
};
/*<summary>
muestra todas las columnas de los datos del proveedor
</summary>
<param name="sColumnasProveedorKeys">Lista de Keys de las columnas del proveedor</param>
<remarks>Llamada desde el icono de desplegar de la cabecera de denominación del proveedor</remarks>*/
function DesplegarColumnaProveedor(sColumnasProveedorKeys) {
    var _grid = $find($('[id$=wdgProveedores]').attr('id'));
    $('[id$=hCamposOcultos]').val('');
    var aColumnasProveedorKeys = sColumnasProveedorKeys.split(",");
    var lColumnasProveedor = aColumnasProveedorKeys.length;
    var columna,cabeceraMultiColumna;
    var iArray = 0;
    while (iArray < lColumnasProveedor) {
        columna = _grid.get_columns().get_columnFromKey(aColumnasProveedorKeys[iArray]);
        if (columna.get_hidden()) columna.set_hidden(false);
               
        iArray += 1;
    }
    VisibilidadColumnas();
};
/*<summary>
Muestra la calificación de la columna de puntuaciones de una UNQA
</summary>
<param name="sKeyColumnaCal">Key de las columnas de calificación a mostrar</param>
<param name="IndiceMulticabecera">Indice de la multicabcera  de la columna</param>
<remarks>Llamada desde el icono de mostrar calificaciones de las cabeceras de unidades de negocio</remarks>*/
function DesplegarColumnaCal(sKeyColumnaCal, IndiceMulticabecera) {
    var _grid = $find($('[id$=wdgProveedores]').attr('id'));
    var columna = _grid.get_columns().get_columnFromKey(sKeyColumnaCal);
    if (columna.get_hidden()) columna.set_hidden(false);
    
    VisibilidadColumnas();
};
/*''' Revisado por: Jbg. Fecha: 28/11/2011
<summary>
Abre en popup la ventana de envio de email pasándole los email de contacto de calidad de los proveedores seleccionados en el grid
</summary>
<remarks>Llamada desde: btnEnviarEmail; Hay que pasar el  porque la página de enviar email es de PMweb. Tiempo máximo:0</remarks>*/
function EnviarEmail() {    
    var listaProveedores="";
    var selectedRow,proveEmail;
    var _grid = $find($('[id$=wdgProveedores]').attr('id'));
    if (_grid.get_behaviors().get_activation().get_activeCellResolved()) {
        selectedRow = _grid.get_behaviors().get_activation().get_activeCellResolved().get_row();
        proveEmail = selectedRow.get_cellByColumnKey("CON_EMAIL").get_value();
    }   
    if (proveEmail != null) {
        if (proveEmail == "##") {
            //Caso de múltiples contactos: obtenerlos
            $.when($.ajax({
                type: "POST",
                url: rutaFS + 'QA/Puntuacion/panelCalidad.aspx/ObtenerContactosQA',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ Proveedor: selectedRow.get_cellByColumnKey("PROVE_COD").get_value() }),
                async: false
            })).done(function (msg) {                           
                for (i = 0; i < msg.d.length; i++) {
                    listaProveedores = listaProveedores + msg.d[i] + ";"
                }
            })
        }
        else {
            if (proveEmail!="") listaProveedores = proveEmail + ";";
        }
    }          
    listaProveedores = String(listaProveedores).substring(0, listaProveedores.length - 1);

    window.open(rutaQA + '_Common/MailProveedores.aspx?Origen=1&Proveedores=' + listaProveedores, "_blank", "width=710,height=360,status=yes,resizable=no,top=180,left=200");
    return false;
};
/* Revisado por: Jbg. Fecha: 28/11/2011
<summary>
Abre en popup la ventana de visor de notificaciones
</summary>
<remarks>Llamada desde: div con imgVisor y lblVisor; Tiempo máximo:0</remarks>*/
function VisorNotif() {
    var _grid = $find($('[id$=wdgProveedores]').attr('id'));
    var listaProveedores = "";
    var listaDenProveedores='';
    var selectedRow,prove,proveD;

    for (i = 0; i < _grid.get_behaviors().get_selection().get_selectedRows().get_length(); i++) {
        selectedRow = _grid.get_behaviors().get_selection().get_selectedRows().getItem(i)
        prove = selectedRow.get_cellByColumnKey("PROVE_COD").get_value();
        proveD = selectedRow.get_cellByColumnKey("PROVE_NAME").get_value();
        if (prove != null){
            listaProveedores =  listaProveedores + "'" + prove + "',";
            listaDenProveedores =  listaDenProveedores + proveD + ",";
        }
    }

    listaProveedores = String(listaProveedores).substring(0, listaProveedores.length - 1);
    listaDenProveedores = String(listaDenProveedores).substring(0, listaDenProveedores.length - 1);
    window.open(rutaFS + '_Common/Notificaciones/Notificaciones.aspx?desdePanelFicha=1&Proveedores=' + listaProveedores + "&DenProveedores=" + listaDenProveedores, "_blank", "width=1000,height=730,status=yes,resizable=yes,top=10,left=10");
    return false;
}	
/*<summary>
Función a la que se invoca desde la página contactos.aspx cuando se ha modificado el contacto de calidad
Modifica el email de contacto en la celda y pone el hidden hRefrescarCache a 1 para indicar cuando se haga postback que hay datos qu ehan cambiado y que acceder a BD
</summary>
<remarks>Llamada desde: wdgProveedores; Hay que pasar el  porque la página de enviar email es de PMweb</remarks>*/
function actualizarContacto(sNewCon,lIDCon) {
    var _grid = $find($('[id$=wdgProveedores]').attr('id'));
    oRow = _grid.get_behaviors().get_selection().get_selectedCells().getItem(0).get_row();
    oCellEmail = oRow.get_cellByColumnKey("CON_EMAIL");
    oCellEmail.set_value(sNewCon);
    $('[id$=hRefrescarCache]').val('1');
}
/*<summary>
Abre el pop up de contactos del proveedor por si quiero cambiar el contacto de notificaciones
</summary>
<remarks>Llamada desde: wdgProveedores; Hay que pasar el  porque la página de enviar email es de PMweb</remarks>*/
function grid_CellClick(sender, e){
    VisibilidadColumnas();
    if (e.get_type() == "cell" && e.get_item().get_row().get_index()!==-1) {
        if (e.get_item().get_grid().get_behaviors().get_activation().get_activeCellResolved() !== null) {
            var cell = e.get_item().get_grid().get_behaviors().get_activation().get_activeCellResolved();               
            if (cell.get_column().get_key()=="CON_EMAIL") {
                var sProveCod = cell.get_row().get_cellByColumnKey("PROVE_COD").get_value();                
                window.open("Contactos.aspx?codProv=" + sProveCod + "&cellId=&GrabaSiempre=1&Selecc=null", "_blank", "width=625,height=400,status=yes,resizable=no,top=150,left=150");
            }
        }
    }
}
/*<summary>
Mostrar el Modal Popup asincronamente de UnQa o materiales gs
</summary>
<param name="Quien">1->Unqa    2->Mat Gs</param>
<param name="Datos">1->Name@#@Codigo    2->Name@#@Codigo@#@Materiales</param>        
<remarks>Llamada desde: wdgProveedores_InitializeRow lo establece ; Tiempo máximo: 0</remarks>*/
function MostrarArbol(Quien, Datos) {
    var btn;
    if (Quien == 1) btn = $('[id$=btnMostrarUnQa]').attr('id');
    else btn = $('[id$=btnMostrarMatQa]').attr('id');
    
    VisibilidadColumnas();
    __doPostBack(btn, Datos);
}
/*<summary>
Va a la ficha de calidad del proveedor clickeado, guardando en hiddens los datos necesarios del proveedor para 
la ficha y para volver de la ficha
</summary>
<remarks>Llamada desde: Enlace de proveedor de la columna de Denominación de proveedor</remarks>*/
function IrFicha(ProveCod, ProveDen, ProveCif, ProveMatQA, ProveEmail, HayError) {
    $('[id$=hProveCod]').val(ProveCod);       
    $('[id$=hProveDen]').val(ProveDen);
    $('[id$=hProveCif]').val(ProveCif);
    $('[id$=hProveMatQA]').val(ProveMatQA);
    $('[id$=hProveEmail]').val(ProveEmail);
    $('[id$=hHayError]').val(HayError);
    document.getElementById("aspnetForm").action = "fichaProveedor.aspx?C=0&h=" + $('[id$=hProveCod]').attr('name').replace('hProveCod', '') +"&HayError=" + $('[id$=hHayError]').attr('name');
    document.getElementById("aspnetForm").submit();
    return false;
}
/*<summary>
Cambia la imagen de sentido de la ordenación cuando se pulsa dicha imagen
</summary>
<remarks>Llamada desde: Icono de sentido de la ordenación</remarks>*/
function cambiarImagenOrdenacion(){
    if ((document.getElementById($("[id$=ibOrdenacion]")[0].id).src).toLowerCase().indexOf("ascendente.gif") > -1)
        document.getElementById($("[id$=hOrdenarSentido]")[0].id).value = rutaImagenes + 'Descendente.gif';
    else document.getElementById($("[id$=hOrdenarSentido]")[0].id).value = rutaImagenes + 'Ascendente.gif';
    
    VisibilidadColumnas();
}
/*<summary>
Provoca el postback para hacer la esportación a excel, y guarda las columnas visibles en cliente
</summary>
<remarks>Llamada desde: Icono de exportar a Excel</remarks>*/
function MostrarExcel() {
    VisibilidadColumnas();
    __doPostBack("", "Excel");
}
/*<summary>
Provoca el postback para hacer la esportación a Pdf, y guarda las columnas visibles en cliente
</summary>
<remarks>Llamada desde: Icono de exportar a Pdf</remarks>*/
function MostrarPdf() {
    VisibilidadColumnas();
    __doPostBack("", "Pdf");
}
/*<summary>
Guarda en el hidden hColumnasVisibles la lista de columnas visibles en el proveedor
</summary>
<remarks>Llamada desde: Icono de exportar a Pdf</remarks>*/
function VisibilidadColumnas() {    
    var _grid = $find($('[id$=wdgProveedores]').attr('id'));
    var colIndex;
    var sColumnasVisibles=';';

    for(colIndex = 0; colIndex < _grid.get_columns().get_length(); colIndex++) {
        var columna = _grid.get_columns().get_column(colIndex);
        if (!columna.get_hidden())  sColumnasVisibles=sColumnasVisibles+columna.get_key()+';';        
    }
    var hColumnasVisibles = $('[id$=hColumnasVisibles]').attr('id');
    $('[id$=hColumnasVisibles]').val(sColumnasVisibles);
}
function PrintForm(){
    var printContent = $('[id$=pnlDetalleErrorCalculoPuntuaciones]').attr('id');
    var windowUrl = 'about:blank';
    var uniqueName = new Date();
    var windowName = 'Print' + uniqueName.getTime();
    var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=0,height=0');

    printWindow.document.write(printContent.innerHTML);
    printWindow.document.close();
    printWindow.focus();
    printWindow.print();
    printWindow.close();
}
/* Revisado por: Jbg. Fecha: 28/11/2011
<summary> Guardar la puntuación </summary>
<remarks>Llamada desde: Div q contiene la imagen y lable de "Guardar Punt"  ; Tiempo máximo: 0</remarks>*/
function GuardarConf(){
    PageMethods.btnGuardarConf_Click($('[id$=hCamposOcultos]').val());
}
/*Paginacion*/
function IndexChanged() {
    VisibilidadColumnas();
    var dropdownlist = $('[id$=PagerPageList]')[0];
    var btnPager = $('[id$=btnPager]').attr('id');
    var selectedIndex = dropdownlist.selectedIndex;
    __doPostBack(btnPager, dropdownlist.selectedIndex);
}
function FirstPage() {
    VisibilidadColumnas();
    var dropdownlist = $('[id$=PagerPageList]')[0];
    var btnPager = $('[id$=btnPager]').attr('id');
    dropdownlist.options[0].selected = true;
    __doPostBack(btnPager, 0);
}
function PrevPage() {
    VisibilidadColumnas();
    var dropdownlist = $('[id$=PagerPageList]')[0];
    var btnPager = $('[id$=btnPager]').attr('id');
    var selectedIndex = dropdownlist.selectedIndex;
    if (selectedIndex - 1 >= 0) {
        dropdownlist.options[selectedIndex - 1].selected = true;
        __doPostBack(btnPager, dropdownlist.selectedIndex);
    }
}
function NextPage() {
    VisibilidadColumnas();
    var dropdownlist = $('[id$=PagerPageList]')[0];
    var btnPager = $('[id$=btnPager]').attr('id');
    var selectedIndex = dropdownlist.selectedIndex;
    if (selectedIndex + 1 <= dropdownlist.length - 1) {
        dropdownlist.options[selectedIndex + 1].selected = true;
        __doPostBack(btnPager, dropdownlist.selectedIndex);
    }
}
function LastPage() {
    VisibilidadColumnas();
    var dropdownlist = $('[id$=PagerPageList]')[0];
    var btnPager = $('[id$=btnPager]').attr('id');
    dropdownlist.options[dropdownlist.length - 1].selected = true;
    __doPostBack(btnPager, dropdownlist.length - 1);
}
