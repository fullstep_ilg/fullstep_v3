﻿Imports Infragistics.Web.UI
Imports Infragistics.Web.UI.GridControls
Imports Fullstep.FSNServer
Partial Public Class panelCalidad
    Inherits FSNPage
    Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
    Private Const TODOS_MATQA As String = "TODOS"
    Private _FiltroVarCal As Collection
    Private _FiltroUNQA As Collection
    Private _FiltroMaterialesQA As Collection
    Private _Proveedores As String
    Private _ProveedoresBaja As String
    Private _PuntuacionDesde As String
    Private _PuntuacionHasta As String
    Private _ProveCod As String
    Private _ProveDen As String
    Private _dsProveedores As DataSet
    Private _pageNumber As Integer = 0
    Private _iAncho_GeneralColumnas As Integer = 120 'Salvo prkove_den a 350 las demas width 120
    Private _iAncho_ColumnaMultiCab_1Unqa As Integer = 90 'Si solo hay una unica unqa la columna multicab
    'debe mostrar texto y botón de cerrar en un ancho de 120px. 90 texto 30 boton
    Private _iAncho_BotonMultiCab As Integer = 25 'La multicabecera tiene texto y 1 boton. Esto es el ancho
    'del boton
    ''' <summary>
    ''' Propiedad que contiene el dataset con los proveedores y sus puntuaciones
    ''' Si es la primera carga de la página o viene de uhn retorno de la ficha sin que hayan cambiado datos obtiene los datos de BD y los cachea
    ''' </summary>
    ''' <value></value>
    ''' <returns>DataSet´</returns>
    ''' <remarks></remarks>
    Protected ReadOnly Property dsProveedores() As DataSet
        Get
            If _dsProveedores Is Nothing Then
                If (Me.IsPostBack AndAlso hRefrescarCache.Value = "") OrElse Request.QueryString("C") = "" Then
                    _dsProveedores = CType(Cache("dsPanelProveedores_" & FSNUser.Cod), DataSet)
                End If
                If _dsProveedores Is Nothing Then
                    hRefrescarCache.Value = ""
                    ObtenerDataSetPanelCalidad()
                    Me.InsertarEnCache("dsPanelProveedores_" & FSNUser.Cod, _dsProveedores, CacheItemPriority.BelowNormal)
                End If
            End If
            Return _dsProveedores
        End Get
    End Property
    Protected ReadOnly Property FiltroVarCal() As Collection
        Get
            If _FiltroVarCal Is Nothing Then

                If Session("CFiltroVarCal") Is Nothing Then
                    Response.Redirect("panelCalidadFiltro.aspx")
                Else
                    _FiltroVarCal = New Collection
                    For Each VarCal As Collection In Session("CFiltroVarCal")
                        If VarCal("FILTRO") OrElse VarCal("NIVEL") = 0 Then
                            _FiltroVarCal.Add(VarCal)
                        End If
                    Next
                End If
            End If
            Return _FiltroVarCal
        End Get
    End Property
    ''' <summary>
    ''' Las unqas seleccionadas por el usurio de las cuales se va a mostrar la puntación en el panel
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected ReadOnly Property FiltroUNQA() As Collection
        Get
            If _FiltroUNQA Is Nothing Then

                If Session("CFiltroUNQA") Is Nothing Then
                    Response.Redirect("panelCalidadFiltro.aspx")
                Else
                    _FiltroUNQA = New Collection
                    For Each UNQA As Collection In Session("CFiltroUNQA")
                        If UNQA("FILTRO") Then
                            _FiltroUNQA.Add(UNQA)
                        End If
                    Next
                End If
            End If
            Return _FiltroUNQA
        End Get

    End Property
    Protected ReadOnly Property FiltroMaterialesQA() As Collection
        Get
            If _FiltroMaterialesQA Is Nothing Then
                _FiltroMaterialesQA = Session("CFiltroMaterialesQA")
            End If
            Return _FiltroMaterialesQA
        End Get

    End Property
    Protected ReadOnly Property Proveedores() As String
        Get
            If _Proveedores Is Nothing Then
                _Proveedores = Session("Proveedores")
            End If
            Return _Proveedores
        End Get
    End Property
    Protected ReadOnly Property ProveedoresBaja() As String
        Get
            If _ProveedoresBaja Is Nothing Then
                _ProveedoresBaja = Session("ProveedoresBaja")
            End If
            Return _ProveedoresBaja
        End Get
    End Property
    Protected ReadOnly Property ProveCod() As String
        Get
            If _ProveCod Is Nothing Then
                _ProveCod = Session("ProveCod")
            End If
            Return _ProveCod
        End Get
    End Property
    Protected ReadOnly Property ProveDen() As String
        Get
            If _ProveDen Is Nothing Then
                _ProveDen = Session("ProveDen")
            End If
            Return _ProveDen
        End Get
    End Property
    Dim _ColumnasVisiblesCliente As String
    Protected ReadOnly Property ColumnasVisiblesCliente() As String
        Get
            If _ColumnasVisiblesCliente Is Nothing Then
                _ColumnasVisiblesCliente = Me.hColumnasVisibles.Value
            End If
            Return _ColumnasVisiblesCliente
        End Get
    End Property
    Private _bExportando As Integer = 0
    Protected ReadOnly Property bExportando() As Boolean
        Get
            If _bExportando = 0 Then
                If Request("__EVENTARGUMENT") = "Excel" OrElse Request("__EVENTARGUMENT") = "Pdf" Then
                    _bExportando = 1
                Else
                    _bExportando = 2
                End If
            End If
            Return IIf(_bExportando = 1, True, False)
        End Get
    End Property
    Private bMSIE7 As Boolean = False
    ''' <summary>
    ''' Evento de carga de página, activamos el AsyncPostBackControl del botón de guardar configuración
    ''' Ponemos textos a los botones, Titulo, marcamos en el menu la opción de Panel de proveedores
    ''' Si es postback controlamos si lo han provocado los botones de Excel o PDf
    ''' Especificamos que por defecto no provoquen un AsyncPostBack todos los controles hijos del update panel del grid (para que el no se recargue todo el grid si por ejm. mostramos los Mat de GS)
    ''' </summary>
    ''' <param name="sender">Page</param>
    ''' <param name="e">EventArgs</param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.PuntProveedores
        Me.upWdgProveedores.ChildrenAsTriggers = False

        If Not Me.IsPostBack Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaQA", "<script>var rutaQA='" & System.Configuration.ConfigurationManager.AppSettings("rutaQA") & "';</script>")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaImagenes", "<script>var rutaImagenes='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "/App_Themes/" & Page.Theme & "/';</script>")

            Dim mMaster = CType(Me.Master, FSNWeb.Menu)
            mMaster.Seleccionar("Calidad", "PanelProveedores")
            'titulo
            Me.FSNPageHeader.TituloCabecera = Textos(51)
            Me.FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/filtro_punt_calidad.gif"
            ''volver
            Me.FSNPageHeader.VisibleBotonVolver = True
            Me.FSNPageHeader.TextoBotonVolver = Textos(32)
            Me.FSNPageHeader.OnClientClickVolver = "window.open(""panelCalidadFiltro.aspx"",""_self"");return false;"

            'CollapsiblePanel
            Me.imgVisor.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/enviar_email_color.gif"
            Me.imgEmail.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/enviar_email_color.gif"
            Me.imgConf.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/emitir.gif"
            Me.imgExcel.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Excel.png"
            Me.imgPDF.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/PDF.png"

            Me.lblVisor.Text = Textos(77)
            Me.lblEnviarEMail.Text = Textos(33)
            Me.lblConf.Text = Textos(19)
            Me.lblExcel.Text = Textos(78)
            Me.lblPDF.Text = Textos(79)

            imgExpandir.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/contraer.gif"
            CollapsiblePanelExtender1.CollapsedImage = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/expandir.gif"
            CollapsiblePanelExtender1.ExpandedImage = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/contraer.gif"
            CargarFiltros()
            CargarCamposOcultos()

            Me.ImgBtnDetCerrar.ToolTip = Textos(60)
            Me.ImgBtnDetCerrar2.ToolTip = Textos(60)
            Me.ImgBtnDetCerrar.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/Bt_Cerrar.png"
            Me.ImgBtnDetCerrar2.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/Bt_Cerrar.png"

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.Notificaciones
            CType(wdgProveedores.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(46)
            CType(wdgProveedores.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(48)
            'Vuelvo a cargar el modulo de PuntProveedores
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.PuntProveedores

            If bMSIE7 Then
                wdgProveedores.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("registrosPaginacionMSIE7")
            Else
                wdgProveedores.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion")
            End If
            CargarGrid()
        Else
            Select Case Request("__EVENTTARGET")
                Case btnPager.ClientID
                    _pageNumber = CType(Request("__EVENTARGUMENT"), Integer)
                Case btnMostrarUnQa.ClientID
                    ConfigurarPopUpUNQAS(Request("__EVENTARGUMENT"))
                    _pageNumber = wdgProveedores.Behaviors.Paging.PageIndex
                Case btnMostrarMatQa.ClientID
                    ConfigurarPopUpMATGS(Request("__EVENTARGUMENT"))
                    _pageNumber = wdgProveedores.Behaviors.Paging.PageIndex
            End Select
            If Request("__EVENTARGUMENT") = "Excel" Then
                ExportarExcel()
            ElseIf Request("__EVENTARGUMENT") = "Pdf" Then
                ExportarPdf()
            End If
            If Not (CType(Cache("dsPanelProveedores_" & FSNUser.Cod), DataSet) Is Nothing) Then
                RecargarGrid()
            End If
        End If
    End Sub
    Private Sub CargarGrid()
        wdgProveedores.Rows.Clear()
        Initialize_wdgProveedores()

        CrearColumnas()
        wdgProveedores.DataKeyFields = "PROVE_COD"
        ConfigurarGrid()
        wdgProveedores.DataBind()
    End Sub
    Private Sub RecargarGrid()

        wdgProveedores.Rows.Clear()
        Dim contcolumn = wdgProveedores.Columns.Count - 1
        For i = 13 To contcolumn
            wdgProveedores.Columns.RemoveAt(13)
        Next
        wdgProveedores.ClearDataSource()
        Initialize_wdgProveedores()
        If Not bExportando Then
            wdgProveedores.Behaviors.Paging.PageIndex = _pageNumber
        End If
        CrearColumnas()
        wdgProveedores.DataKeyFields = "PROVE_COD"
        ConfigurarGrid()

        wdgProveedores.DataBind()

    End Sub
    ''' <summary>
    ''' Carga la descripción de los filtros que usurio ha seleccionado en la página de filtros
    ''' </summary>
    ''' <remarks></remarks>
    Sub CargarFiltros()
        Dim oMaterialesSeleccionados As List(Of IGmn) = HttpContext.Current.Session("materialesseleccionados")
        Dim oEstructuraMateriales As EstructuraMateriales = FSNServer.Get_Object(GetType(FSNServer.EstructuraMateriales))
        oEstructuraMateriales.TipoEstructuraMateriales = HttpContext.Current.Session("tipofiltromaterial")
        lblCabeceraFiltros.Text = Textos(44)
        Dim sFiltros As String = ""
        Dim _sFiltros As String = ""
        _sFiltros = ""


        For Each oGmn As IGmn In oMaterialesSeleccionados
            If oGmn.GmnQAcod <> 0 AndAlso oEstructuraMateriales.TipoEstructuraMateriales <> Operadores.FiltroMaterial.FiltroMatQA And oGmn.nivel <> 0 Then
                oEstructuraMateriales.User = Me.FSNUser
                Dim denQA As String = oEstructuraMateriales.children("")(oGmn.GmnQAcod).Den
                _sFiltros = _sFiltros & denQA & "/" & oGmn.title & ","
            Else
                _sFiltros = _sFiltros & oGmn.title & ","
            End If

        Next
        If _sFiltros <> "" Then
            _sFiltros = "<b>" & Textos(9) & ": </b>" & Left(_sFiltros, Len(_sFiltros) - 1)
            sFiltros = sFiltros & _sFiltros
        End If
        _sFiltros = ""
        For Each UNQA As Collection In FiltroUNQA
            _sFiltros = _sFiltros & UNQA("DEN") & ","
        Next
        If _sFiltros <> "" Then
            _sFiltros = "<br><b>" & Textos(34) & ": </b>" & Left(_sFiltros, Len(_sFiltros) - 1)
            sFiltros = sFiltros & _sFiltros
        End If
        _sFiltros = ""
        For Each VarCal As Collection In FiltroVarCal
            _sFiltros = _sFiltros & VarCal("DEN") & ","
        Next
        If _sFiltros <> "" Then
            _sFiltros = "<br><b>" & Textos(50) & ": </b>" & Left(_sFiltros, Len(_sFiltros) - 1)
            sFiltros = sFiltros & _sFiltros
        End If

        Select Case Proveedores
            Case "1"
                sFiltros = sFiltros & "<br><b>" & Me.Textos(12) & "</b>: " & ProveDen
            Case "2"
                sFiltros = sFiltros & "<br><b>" & Me.Textos(13) & "</b>"
            Case "3"
                sFiltros = sFiltros & "<br><b>" & Me.Textos(14) & "</b>"
            Case "4"
                sFiltros = sFiltros & "<br><b>" & Me.Textos(15) & "</b>"
        End Select

        If ProveedoresBaja = "1" Then
            sFiltros = sFiltros & "<br><b>" & Me.Textos(16) & "</b>"
        End If
        If Session("bPuntuacionDesde") Then
            sFiltros = sFiltros & "<br><b>" & Me.Textos(36) & "</b>: " & FSNLibrary.FormatNumber(Session("PuntuacionDesde"), FSNUser.NumberFormat) & "</b>"
        End If
        If Session("bPuntuacionHasta") Then
            sFiltros = sFiltros & "<br><b>" & Me.Textos(37) & "</b>: " & FSNLibrary.FormatNumber(Session("PuntuacionHasta"), FSNUser.NumberFormat) & "</b>"
        End If
        lblFiltros.Text = sFiltros
    End Sub
    ''' <summary>
    ''' CArgamos en un hidden la lista de campos a ocultar en el grid, en función de la conf del usuario
    ''' </summary>
    ''' <remarks></remarks>
    Sub CargarCamposOcultos()
        hCamposOcultos.Value = Session("CamposOcultosPC")
    End Sub
    ''' <summary>
    ''' Obtiene de base de datos u dataset con la tabla de proveedores afectados por el filtro
    ''' y con otra tabla con sus puntuaciones
    ''' </summary>
    ''' <remarks></remarks>
    Sub ObtenerDataSetPanelCalidad()
        Dim PMProveedores As FSNServer.Proveedores = FSNServer.Get_Object(GetType(FSNServer.Proveedores))
        Dim oMaterialesSeleccionados As List(Of IGmn) = HttpContext.Current.Session("materialesseleccionados")
        'Obtengo las puntuaciones de todas las Variables de calidad a las que el usuario tiene acceso
        Dim sVarCal As String = ""
        For Each VarCal As Collection In FiltroVarCal
            sVarCal = sVarCal & IIf(sVarCal = "", "", ",") & VarCal("ID") & "-" & VarCal("NIVEL")
        Next

        Dim dt As New DataTable
        dt.Columns.Add("GMNQA", GetType(Integer))
        dt.Columns.Add("GMN1", GetType(String))
        dt.Columns.Add("GMN2", GetType(String))
        dt.Columns.Add("GMN3", GetType(String))
        dt.Columns.Add("GMN4", GetType(String))

        For Each oGmn As IGmn In oMaterialesSeleccionados
            dt.Rows.Add({IIf(oGmn.GmnQAcod = 0, Nothing, oGmn.GmnQAcod), oGmn.gmn1Cod, oGmn.gmn2Cod, oGmn.gmn3Cod, oGmn.gmn4Cod})
        Next

        _dsProveedores = PMProveedores.DevolverPanelCalidad(Me.FSNUser.Cod, dt,
                Session("FiltroUNQAProve"), Session("FiltroUNQAPunt"), sVarCal,
                IIf(Session("Proveedores") Is Nothing, 0, CShort(Session("Proveedores"))), Session("ProveCod"),
                IIf(Session("ProveedoresBaja") Is Nothing, 0, Session("ProveedoresBaja")), FSNUser.Idioma,
                FSNUser.QARestProvContacto, FSNUser.QARestProvMaterial, FSNUser.QARestProvEquipo)
        If _dsProveedores.Tables.Count > 0 Then
            ObtenerTablaPanelCalidad(_dsProveedores.Tables(0))
        End If
    End Sub
    ''' <summary>
    ''' Monta la tabla que sirve de origen de datos para el grid, añadiendo a la tabla de proveedores las columnas que representan las puntuacinoes para cada variable/unidad de negocio
    ''' </summary>
    ''' <param name="dtProveedores">Tabla donde está el resultado de ejecutar DevolverPanelCalidad</param>
    ''' <remarks></remarks>
    Sub ObtenerTablaPanelCalidad(ByRef dtProveedores As DataTable)
        'Añado a la Tabla 0 de Proveedores las columnas para cada puntuación por variable de calidad y unidad de negoico
        'REcorremos las colecciones de variables de calidad y unidades de negoico para añador las columnas
        Dim VarCalTotal As Integer = 0
        Dim NivelTotal As Integer = 0
        Dim iVarCal As Integer = 0

        For Each VarCal As Collection In FiltroVarCal
            If iVarCal = 0 Then
                VarCalTotal = VarCal("ID")
                NivelTotal = VarCal("NIVEL")
            End If
            For Each UNQA As Collection In FiltroUNQA
                Dim Columna As New DataColumn("PUNT_" & VarCal("ID") & "_" & VarCal("NIVEL") & "_" & UNQA("ID"), System.Type.GetType("System.Double"))
                dtProveedores.Columns.Add(Columna)
                Columna = New DataColumn("CAL_" & VarCal("ID") & "_" & VarCal("NIVEL") & "_" & UNQA("ID"), System.Type.GetType("System.String"))
                dtProveedores.Columns.Add(Columna)
                Columna = New DataColumn("APLICA_" & VarCal("ID") & "_" & VarCal("NIVEL") & "_" & UNQA("ID"), System.Type.GetType("System.String"))
                dtProveedores.Columns.Add(Columna)
            Next
            iVarCal = iVarCal + 1
        Next

        'Recorremos la tabla de Proveedores y por cada proveedor obtenemos sus puntuaciones
        'Y obtenemos la lista de materiales de QA
        Dim bEliminarProveedor As Boolean = False
        Dim TodasPuntuacionesNulas As Boolean = True
        Dim sRowsEliminar As String = ""
        Dim iRow As Integer = 0
        Dim sUNQASFiltro As String = ""
        For Each UNQA As Collection In FiltroUNQA
            sUNQASFiltro = sUNQASFiltro & UNQA("ID") & ","
        Next
        sUNQASFiltro = "," & sUNQASFiltro
        For Each Row As DataRow In dtProveedores.Rows
            bEliminarProveedor = False
            TodasPuntuacionesNulas = True
            For Each RowPuntuacion As DataRow In Row.GetChildRows("REL_PROVE_PUNT")
                If Not IsDBNull(RowPuntuacion("VARCAL")) AndAlso Not IsDBNull(RowPuntuacion("NIVEL")) AndAlso Not IsDBNull(RowPuntuacion("UNQA")) Then
                    TodasPuntuacionesNulas = False
                    'Si la puntación es de la variable Total Proveedor, y la unqa es del filtro comprobamos el filtro de puntuación desde hasta
                    If RowPuntuacion("VARCAL") = VarCalTotal AndAlso RowPuntuacion("NIVEL") = NivelTotal AndAlso sUNQASFiltro.IndexOf("," & RowPuntuacion("UNQA") & ",") <> -1 Then
                        If Session("bPuntuacionDesde") OrElse Session("bPuntuacionHasta") Then
                            If Session("bPuntuacionDesde") Then
                                If RowPuntuacion("PUNT") < Session("PuntuacionDesde") Then
                                    sRowsEliminar = sRowsEliminar & "," & iRow
                                    bEliminarProveedor = True
                                    Exit For
                                End If
                            End If
                            If Session("bPuntuacionHasta") Then
                                If RowPuntuacion("PUNT") > Session("PuntuacionHasta") Then
                                    sRowsEliminar = sRowsEliminar & "," & iRow
                                    bEliminarProveedor = True
                                    Exit For
                                End If
                            End If
                        End If
                    End If
                    If Not bEliminarProveedor Then
                        Row("PUNT_" & RowPuntuacion("VARCAL") & "_" & RowPuntuacion("NIVEL") & "_" & RowPuntuacion("UNQA")) = RowPuntuacion("PUNT")
                        Row("CAL_" & RowPuntuacion("VARCAL") & "_" & RowPuntuacion("NIVEL") & "_" & RowPuntuacion("UNQA")) = RowPuntuacion("CALIFICACION")
                        Row("APLICA_" & RowPuntuacion("VARCAL") & "_" & RowPuntuacion("NIVEL") & "_" & RowPuntuacion("UNQA")) = RowPuntuacion("APLICA")
                    End If
                End If
            Next
            If TodasPuntuacionesNulas AndAlso (Session("bPuntuacionDesde") OrElse Session("bPuntuacionHasta")) Then
                sRowsEliminar = sRowsEliminar & "," & iRow
                bEliminarProveedor = True
            End If
            If Not bEliminarProveedor Then
                Dim sMateriales As String = ""
                Dim sMaterialesId As String = ","
                For Each RowMATQA As DataRow In Row.GetChildRows("REL_PROVE_MATQA")
                    sMateriales = sMateriales & IIf(sMateriales = "", "", ";") & RowMATQA("MATERIAL")
                    sMaterialesId = sMaterialesId & RowMATQA("ID") & ","
                Next
                Row("MATERIAL_QA") = sMateriales
                Row("MATERIAL_QA_ID") = sMaterialesId
            End If
            iRow = iRow + 1
        Next
        If sRowsEliminar <> "" Then
            sRowsEliminar = Right(sRowsEliminar, Len(sRowsEliminar) - 1)
            Dim aRowsEliminar() As String = Split(sRowsEliminar, ",")
            Dim restaIndice As Integer = 0
            For indiceRow As Integer = 0 To aRowsEliminar.Length - 1
                dtProveedores.Rows.RemoveAt(aRowsEliminar(indiceRow) - restaIndice)
                restaIndice = restaIndice + 1
            Next
        End If
    End Sub
    Protected bOrdenar As Boolean = False
    ''' <summary>
    ''' Inicializa el DataSource del  wdgProveedores
    ''' </summary>
    Private Sub Initialize_wdgProveedores()
        If Not bExportando Then
            If Request.Browser("Browser") = "IE" AndAlso (Request.Browser("MajorVersion") < 8) Then
                bMSIE7 = True
            End If
            If dsProveedores.Tables.Count > 0 AndAlso dsProveedores.Tables(0).Rows.Count > 0 Then
                'comprobamos ordenar
                Dim vOrdenada As DataView = dsProveedores.Tables(0).DefaultView
                Dim sSentido As String = ""

                If (Not Me.IsPostBack) AndAlso (Not Request("OrdenVolver") Is Nothing) AndAlso (Request("OrdenVolver") <> "") Then
                    If Request("OrdenacionVolver").Contains("Descendente") Then
                        sSentido = " DESC"
                    ElseIf Request("OrdenacionVolver").Contains("Ascendente") Then
                        sSentido = " ASC"
                    End If
                    vOrdenada.Sort = Request("OrdenVolver") & sSentido
                ElseIf (Not Me.IsPostBack) AndAlso Session("OrdenacionPanel") = "" Then
                    Session("OrdenacionPanel") = "PROVE_NAME ASC"

                    vOrdenada.Sort = Session("OrdenacionPanel")
                    bOrdenar = True
                ElseIf hOrdenar.Value = "" Then
                    vOrdenada.Sort = Session("OrdenacionPanel")
                    bOrdenar = True
                Else
                    'si hay cambio de sentido
                    If hOrdenarSentido.Value.ToLower() <> ibOrdenacion.ImageUrl.ToLower() Then
                        If hOrdenarSentido.Value.Contains("Descendente") Then
                            sSentido = " DESC"
                        ElseIf hOrdenarSentido.Value.Contains("Ascendente") Then
                            sSentido = " ASC"
                        End If
                        vOrdenada.Sort = wddOrdenar.SelectedValue & sSentido
                        bOrdenar = True
                    Else
                        If hOrdenar.Value <> wddOrdenar.SelectedValue Then
                            bOrdenar = True
                        End If
                        If hOrdenarSentido.Value.Contains("Descendente") Then
                            sSentido = " DESC"
                        ElseIf hOrdenarSentido.Value.Contains("Ascendente") Then
                            sSentido = " ASC"
                        End If
                        vOrdenada.Sort = wddOrdenar.SelectedValue & sSentido
                    End If
                End If
                'Para asignarlo al WebDataGrid tiene que ser un Dataset
                If dsProveedores.Tables.Contains("TablaSort") Then
                    dsProveedores.Tables.Remove("TablaSort")
                End If
                dsProveedores.Tables.Add(vOrdenada.ToTable("TablaSort"))
                wdgProveedores.DataSource = dsProveedores
                wdgProveedores.DataMember = "TablaSort"
            Else
                wdgProveedores.Visible = True
                lblOrdenarPor.Visible = False
                wddOrdenar.Visible = False
                ibOrdenacion.Visible = False
                imgExcel.Visible = False
                lblExcel.Visible = False
            End If
        End If
    End Sub
    Private Sub CrearColumnas()
        If bMSIE7 Then
            wdgProveedores.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("registrosPaginacionMSIE7")
        Else
            wdgProveedores.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion")
        End If
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim nombre As String
        Dim tabla As DataTable
        If dsProveedores.Tables.Contains("TablaSort") Then
            tabla = DirectCast(wdgProveedores.DataSource, System.Data.DataSet).Tables("TablaSort")
        Else
            tabla = dsProveedores.Tables(0)
        End If

        'Las columnas con los datos del proveedor se agrupan en el groupfield DATOS_PROVEEDOR (creado desde aspx)
        Dim UNQAVisibles As Integer = FiltroUNQA.Count
        Dim indiceColumnaMulti As Integer = 3
        If bMSIE7 Then
            indiceColumnaMulti = 2
        End If
        Dim listaPreColumnasUNQAOcultar As String = ""
        'Generamos un grupo por cada variable de calidad
        For Each VarCal As Collection In FiltroVarCal
            Dim grupo = New GroupField
            grupo.Key = "VARCAL_" & VarCal("ID") & "_" & VarCal("NIVEL")
            listaPreColumnasUNQAOcultar = ""
            For Each UNQA As Collection In FiltroUNQA
                listaPreColumnasUNQAOcultar = listaPreColumnasUNQAOcultar & IIf(listaPreColumnasUNQAOcultar = "", "", ",") & VarCal("ID") & "_" & VarCal("NIVEL") & "_" & UNQA("ID")
            Next
            If bExportando Then
                grupo.Header.Text = ""
            Else
                grupo.Header.Text = PlantillaCabeceraMultiColumna(VarCal("DEN"), listaPreColumnasUNQAOcultar, indiceColumnaMulti)
            End If
            grupo.Header.CssClass = "fila_cabecera"

            wdgProveedores.Columns.Add(grupo)
            indiceColumnaMulti = indiceColumnaMulti + 1
            'Creo las columnas correspondientes a esta variable de calidad
            For i = 0 To tabla.Columns.Count - 1
                campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
                nombre = tabla.Columns(i).ToString
                If nombre.Contains("_" & VarCal("ID") & "_" & VarCal("NIVEL") & "_") Then
                    If Left(nombre, 5) = "PUNT_" Or Left(nombre, 4) = "CAL_" Or Left(nombre, 7) = "APLICA_" Then
                        With campoGrid
                            .Key = nombre
                            .DataFieldName = nombre
                            .Header.Text = nombre
                            .Header.Tooltip = nombre
                            If Left(nombre, 7) = "APLICA_" Then .Hidden = True
                        End With
                        grupo.Columns.Add(campoGrid)
                    End If
                End If
            Next
        Next

    End Sub
    ''' <summary>
    ''' Inicializa el el layout de wdgProveedores nada más ejecutarse el Initialize
    ''' Tras ir a fichaproveedor hay q volver a la pagina donde estuvieras.
    ''' </summary>
    Private Sub ConfigurarGrid()
        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.PuntProveedores
        If Not bExportando Then
            If (Not Me.IsPostBack) AndAlso (Not Request("PaginaVolver") Is Nothing) Then
                If Request("PaginaVolver") <> "" Then
                    wdgProveedores.Behaviors.Paging.PageIndex = Request("PaginaVolver")
                Else
                    wdgProveedores.Behaviors.Paging.PageIndex = 0
                End If
                _pageNumber = wdgProveedores.Behaviors.Paging.PageIndex
            End If
            ConfigurarCabecera()
            PaginaVolver.Value = wdgProveedores.Behaviors.Paging.PageIndex
        End If

    End Sub
    ''' <summary>
    ''' Configura las cabeceras de la grid
    ''' </summary>
    ''' <remarks></remarks>
    Sub ConfigurarCabecera()
        ColumnasNoVisibles()
        CabecerasColumnas()
        ColumnasNoVisiblesConfiguracion()
    End Sub
    ''' <summary>
    ''' Oculta las columnas no visibles en cliente
    ''' </summary>
    ''' <remarks></remarks>
    Sub ColumnasNoVisibles()
        With wdgProveedores
            .Columns("PROVE_NAME").Hidden = True
            .Columns("PROVE_CALIDAD_POT").Hidden = True
            .Columns("PROVE_CALIDAD_BAJA").Hidden = True
            .Columns("PROVE_CALIDAD_PROVISIONAL").Hidden = True
            .Columns("MATERIAL_QA_ID").Hidden = True
            .Columns("HAYERROR").Hidden = True
        End With
    End Sub
    ''' <summary>
    ''' Si el usuario tiene campos ocultos y el navegador no es MSIE7 ocultamos en el servidor las columnas
    ''' </summary>
    ''' <remarks></remarks>
    Sub ColumnasNoVisiblesConfiguracion()
        If hCamposOcultos.Value <> "" Then
            Dim CamposOcultosPC() As String = Split(hCamposOcultos.Value, ",")
            For Each Campo As String In CamposOcultosPC
                wdgProveedores.Columns(Campo).Hidden = True
            Next
        End If
    End Sub
    ''' <summary>
    ''' configura la cabecera de las columnas poniendo los caption, los iconos de cerrar, desplegar, ver 
    ''' calificaciones.., rellena el combo de ordenar con las columnas del grid y establece los estilos
    ''' y si vuelves de la ficha establece visualmente orden y ordenación (solo visual porque quien hace
    ''' el sort es uwgProveedores_InitializeDataSource)
    ''' </summary>
    ''' <remarks></remarks>
    Sub CabecerasColumnas()
        'TITULOS
        Dim indiceColumnaMulti As Integer = 1

        'Cabeceras Columnas proveedor
        wdgProveedores.Columns("PROVE_DEN").Header.Text = PlantillaCabecera("PROVE_COD,PROVE_CIF,CON_EMAIL,MATERIAL_QA,MATERIAL_GS,UNQAS", False, Textos(40), "PROVE_DEN", indiceColumnaMulti)
        wdgProveedores.Columns("PROVE_DEN").Header.CssClass = "fila_cabecera"

        If Not bMSIE7 Then indiceColumnaMulti = indiceColumnaMulti + 1

        With wdgProveedores
            .Columns("PROVE_COD").Header.Text = PlantillaCabecera("", True, Textos(22), "PROVE_COD", indiceColumnaMulti)
            .Columns("PROVE_COD").Header.CssClass = "fila_cabecera"
            .Columns("PROVE_CIF").Header.Text = PlantillaCabecera("", True, Textos(23), "PROVE_CIF", indiceColumnaMulti)
            .Columns("PROVE_CIF").Header.CssClass = "fila_cabecera"
            .Columns("CON_EMAIL").Header.Text = PlantillaCabecera("", True, Textos(27), "CON_EMAIL", indiceColumnaMulti)
            .Columns("CON_EMAIL").Header.CssClass = "fila_cabecera"
            .Columns("CON_EMAIL").Header.Tooltip = Textos(27)
            .Columns("MATERIAL_QA").Header.Text = PlantillaCabecera("", True, Textos(24), "MATERIAL_QA", indiceColumnaMulti)
            .Columns("MATERIAL_QA").Header.CssClass = "fila_cabecera"
            .Columns("MATERIAL_GS").Header.Text = PlantillaCabecera("", True, Textos(30), "MATERIAL_GS", indiceColumnaMulti)
            .Columns("MATERIAL_GS").CssClass = "centrado"
            .Columns("MATERIAL_GS").Header.CssClass = "fila_cabecera"
            .Columns("UNQAS").Header.Text = PlantillaCabecera("", True, Textos(34), "UNQAS", indiceColumnaMulti)
            .Columns("UNQAS").CssClass = "centrado"
            .Columns("UNQAS").Header.CssClass = "fila_cabecera"
            .Columns("UNQAS").Header.Tooltip = Textos(34)
            If IsPostBack Then
                .Columns("PROVE_COD").Hidden = Not VisibleCliente("PROVE_COD")
                .Columns("PROVE_CIF").Hidden = Not VisibleCliente("PROVE_CIF")
                .Columns("CON_EMAIL").Hidden = Not VisibleCliente("CON_EMAIL")
                .Columns("MATERIAL_QA").Hidden = Not VisibleCliente("MATERIAL_QA")
                .Columns("MATERIAL_GS").Hidden = Not VisibleCliente("MATERIAL_GS")
                .Columns("UNQAS").Hidden = Not VisibleCliente("UNQAS")
            End If
        End With

        If Not bMSIE7 Then
            wdgProveedores.Columns("PROVE_DEN").Width = Unit.Pixel(350)
            With wdgProveedores
                .Columns("PROVE_COD").Width = Unit.Pixel(120)
                .Columns("PROVE_CIF").Width = Unit.Pixel(120)
                .Columns("CON_EMAIL").Width = Unit.Pixel(120)
                .Columns("MATERIAL_QA").Width = Unit.Pixel(120)
                .Columns("MATERIAL_GS").Width = Unit.Pixel(120)
                .Columns("UNQAS").Width = Unit.Pixel(120)
            End With
        End If

        If Not IsPostBack Then
            lblOrdenarPor.Text = Textos(58)
            hOrdenar.Value = "PROVE_NAME"
            ibOrdenacion.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Ascendente.gif"
            hOrdenarSentido.Value = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Ascendente.gif"

            AñadirElementoOrden("PROVE_NAME", Textos(40))
            AñadirElementoOrden("PROVE_COD", Textos(22))
            AñadirElementoOrden("PROVE_CIF", Textos(23))
            AñadirElementoOrden("CON_EMAIL", Textos(27))
        End If
        indiceColumnaMulti = indiceColumnaMulti + 1
        Dim UNQACal_Hidden As String = ""
        Dim KeyUNQA As String
        Dim KeyCal As String
        Dim CodUNQA As String

        For Each VarCal As Collection In FiltroVarCal
            For Each UNQA As Collection In FiltroUNQA
                KeyUNQA = "PUNT_" & VarCal("ID") & "_" & VarCal("NIVEL") & "_" & UNQA("ID")
                KeyCal = "CAL_" & VarCal("ID") & "_" & VarCal("NIVEL") & "_" & UNQA("ID")
                CodUNQA = UNQA("DEN")
                If UNQA("NIVEL") <> 0 Then
                    CodUNQA = Left(CodUNQA, InStr(CodUNQA, "-") - 1)
                End If
                With DirectCast(wdgProveedores.Columns("VARCAL_" & VarCal("ID") & "_" & VarCal("NIVEL")), Infragistics.Web.UI.GridControls.GroupField)
                    .Columns(KeyUNQA).Header.Tooltip = UNQA("DEN")
                    .Columns(KeyUNQA).Header.Text = PlantillaCabecera("", True, CodUNQA, KeyUNQA, indiceColumnaMulti, True)
                    'estilo de cabeceras por niveles
                    .Columns(KeyUNQA).Header.CssClass = "pc_punt_nivel" & UNQA("NIVEL")
                    .Columns(KeyCal).Header.CssClass = "pc_punt_nivel" & UNQA("NIVEL")
                    .Columns(KeyCal).CssClass = "fp_celda_cal"
                    .Columns(KeyUNQA).CssClass = "centrado"
                    .Columns(KeyCal).Header.Text = PlantillaCabecera("", True, Textos(39), KeyCal, indiceColumnaMulti)
                    .Columns(KeyCal).Hidden = True
                    If Not bMSIE7 Then
                        .Columns(KeyCal).Width = Unit.Pixel(120)
                        .Columns(KeyUNQA).Width = Unit.Pixel(120)
                    Else
                        .Columns(KeyUNQA).Width = Unit.Pixel(40)
                        UNQACal_Hidden = UNQACal_Hidden & IIf(UNQACal_Hidden = "", "", ",") & KeyCal
                    End If
                    If IsPostBack Then
                        .Columns(KeyCal).Hidden = Not VisibleCliente(KeyCal)
                        .Columns(KeyUNQA).Hidden = Not VisibleCliente(KeyUNQA)
                    End If
                End With
                If Not Me.IsPostBack Then
                    'Añadir columnas a la combo de ordenar
                    AñadirElementoOrden("PUNT_" & VarCal("ID") & "_" & VarCal("NIVEL") & "_" & UNQA("ID"), VarCal("DEN") & "-" & UNQA("DEN"))
                End If
            Next
            indiceColumnaMulti = indiceColumnaMulti + 1
        Next
        If bMSIE7 Then
            Dim sUNQACal_Hidden As String = "var bMSIE7=true;var sUNQACal_Hidden='" & UNQACal_Hidden & "';"
            sUNQACal_Hidden = String.Format(IncludeScriptKeyFormat, "javascript", sUNQACal_Hidden)
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "sUNQACal_Hidden", sUNQACal_Hidden)
        Else
            Dim sMSIE7 As String = String.Format(IncludeScriptKeyFormat, "javascript", "var bMSIE7=false;")
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "sMSIE7", sMSIE7)
        End If

        If (Not IsPostBack) AndAlso (Not Request("OrdenVolver") Is Nothing) AndAlso (Request("OrdenVolver") <> "") Then
            wddOrdenar.SelectedValue = Request("OrdenVolver")
            hOrdenar.Value = wddOrdenar.SelectedValue

            ibOrdenacion.ImageUrl = Request("OrdenacionVolver")
            hOrdenarSentido.Value = ibOrdenacion.ImageUrl
        ElseIf (Not IsPostBack) Then
            Dim OrdenacionPanel As String = Session("OrdenacionPanel")

            If InStr(OrdenacionPanel, " DESC") > 0 Then
                OrdenacionPanel = Replace(OrdenacionPanel, " DESC", "")

                ibOrdenacion.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Descendente.gif"
            Else
                OrdenacionPanel = Replace(OrdenacionPanel, " ASC", "")

                ibOrdenacion.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Ascendente.gif"
            End If
            hOrdenarSentido.Value = ibOrdenacion.ImageUrl

            wddOrdenar.SelectedValue = OrdenacionPanel
            hOrdenar.Value = wddOrdenar.SelectedValue
        End If
    End Sub
    ''' <summary>
    ''' Genera el html de las cabeceras de las columnas de las variables de calidad que agrupan las puntuacines
    ''' </summary>
    ''' <param name="sTextoCabecera">Nombre de la variables</param>
    ''' <param name="listaPreColumnasUNQAOcultar">Lista de las columnas con las puntuaciones por unidad de negocio de la variable a ocultar si se oculta la variable</param>
    ''' <param name="iIndiceCabeceraMulti">Indice de la columna variable de calidad</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function PlantillaCabeceraMultiColumna(ByVal sTextoCabecera As String, ByVal listaPreColumnasUNQAOcultar As String, ByVal iIndiceCabeceraMulti As Integer) As String
        If bMSIE7 Then
            PlantillaCabeceraMultiColumna = "<table width=""100%"" cellpadding=""0"" cellspacing=""0""  id=""contenedor_imagenes""><tr>"
            PlantillaCabeceraMultiColumna = PlantillaCabeceraMultiColumna & "<td style=""text-align: center""><nobr>" & sTextoCabecera & "&nbsp;</td>"
            PlantillaCabeceraMultiColumna = PlantillaCabeceraMultiColumna & "<td style=""text-align: right"">"
            PlantillaCabeceraMultiColumna = PlantillaCabeceraMultiColumna & "<a href=""javascript:OcultarColumnaVarCal('" & listaPreColumnasUNQAOcultar & "'," & iIndiceCabeceraMulti & ")""><img src=""" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/cerrar.gif"" border=""0"" alt=""" & Textos(53) & """></a>&nbsp;"
            PlantillaCabeceraMultiColumna = PlantillaCabeceraMultiColumna & "</td></tr></table>"
        Else
            Dim auxWidth As Integer = (FiltroUNQA.Count * _iAncho_GeneralColumnas) - _iAncho_BotonMultiCab
            If FiltroUNQA.Count = 1 Then auxWidth = _iAncho_ColumnaMultiCab_1Unqa

            PlantillaCabeceraMultiColumna = "<div id=""contenedor_imagenes"">"
            PlantillaCabeceraMultiColumna = PlantillaCabeceraMultiColumna & "<div style=""text-overflow:ellipsis; overflow:hidden;float:left; vertical-align:middle;" &
                 "max-width:" & auxWidth & "px;" & "white-space:nowrap;"" title=""" & sTextoCabecera & """>" & sTextoCabecera & "&nbsp;</div>"
            PlantillaCabeceraMultiColumna = PlantillaCabeceraMultiColumna & "<div style=""text-align:right; min-width:25px;float:rigth;"">"
            PlantillaCabeceraMultiColumna = PlantillaCabeceraMultiColumna & "<a href=""javascript:OcultarColumnaVarCal('" & listaPreColumnasUNQAOcultar & "'," & iIndiceCabeceraMulti & ")""><img src=""" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/cerrar.gif"" border=""0"" alt=""" & Textos(53) & """></a>&nbsp;"
            PlantillaCabeceraMultiColumna = PlantillaCabeceraMultiColumna & "</div></div>"
        End If
    End Function
    ''' <summary>
    ''' Genera la cabecera de las columnas de la grid, con botón de desplegar y/o cerrar y/o ver calificación dependiendo de la columna
    ''' </summary>
    ''' <param name="sHijos">Lista de Keys/Ids de las columnas a desplegar</param>
    ''' <param name="bCerrar">true/aparece el botón de cerrar false/no aparece</param>
    ''' <param name="sTextoCabecera">Captión de la columna</param>
    ''' <param name="sKeyCabecera">Key de la columna</param>
    ''' <param name="iIndiceCabeceraMulti">Indice de la columna multicabecera que le corresponde</param>
    ''' <param name="bCalificacion">true/se muestra icono de ver calificacion</param>
    ''' <returns>Cadena con el html de la cabecera</returns>
    ''' <remarks>CabecerasColumnas</remarks>
    Function PlantillaCabecera(ByVal sHijos As String, ByVal bCerrar As Boolean, ByVal sTextoCabecera As String, ByVal sKeyCabecera As String, ByVal iIndiceCabeceraMulti As Integer, Optional ByVal bCalificacion As Boolean = False) As String
        If bMSIE7 Then
            PlantillaCabecera = "<table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" id=""contenedor_imagenes"">"
            PlantillaCabecera = PlantillaCabecera & "<tr><td><nobr>" & sTextoCabecera & "&nbsp;</td>"
            PlantillaCabecera = PlantillaCabecera & "<td style=""text-align: right"">"
        Else
            PlantillaCabecera = "<div id=""contenedor_imagenes"">"
            PlantillaCabecera = PlantillaCabecera & "<div style=""text-overflow:ellipsis; overflow:hidden;float:left; vertical-align:middle;"
            Select Case sKeyCabecera
                Case "PROVE_DEN"
                    PlantillaCabecera = PlantillaCabecera & "max-width:325px;"

                    PlantillaCabecera = PlantillaCabecera & "white-space:nowrap;"">" & sTextoCabecera & "&nbsp;</div>"
                    PlantillaCabecera = PlantillaCabecera & "<div style=""text-align:right; min-width:25px;float:rigth;"">"

                Case "PROVE_COD", "PROVE_CIF", "MATERIAL_QA", "MATERIAL_GS", "CON_EMAIL", "UNQAS"
                    PlantillaCabecera = PlantillaCabecera & "max-width:95px;"

                    PlantillaCabecera = PlantillaCabecera & "white-space:nowrap;"">" & CortarCabecera(sTextoCabecera) & "&nbsp;</div>"
                    PlantillaCabecera = PlantillaCabecera & "<div style=""text-align:right; min-width:25px;float:rigth;"">"

                Case Else
                    If Left(sKeyCabecera, 4) = "PUNT" Then
                        PlantillaCabecera = PlantillaCabecera & "max-width:70px;"
                        PlantillaCabecera = PlantillaCabecera & "white-space:nowrap;"">" & CortarCabecera(sTextoCabecera, True) & "&nbsp;</div>"
                        PlantillaCabecera = PlantillaCabecera & "<div style=""text-align:left; min-width:36px;"">"
                    Else
                        PlantillaCabecera = PlantillaCabecera & "max-width:95px;"
                        PlantillaCabecera = PlantillaCabecera & "white-space:nowrap;"">" & sTextoCabecera & "&nbsp;</div>"
                        PlantillaCabecera = PlantillaCabecera & "<div style=""text-align:left; min-width:18px;"">"
                    End If
            End Select
        End If

        If sHijos <> "" Then
            'Si columnas de datos de proveedor se muestran en el cliente 
            If iIndiceCabeceraMulti = 1 Then
                PlantillaCabecera = PlantillaCabecera & "<a href=""javascript:DesplegarColumnaProveedor('" & sHijos & "')""><img src=""" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/desplegar_columna.gif"" border=""0"" alt=""" & Textos(54) & """></a>&nbsp;"
            End If
        End If
        If bCalificacion Then
            PlantillaCabecera = PlantillaCabecera & "<a href=""javascript:DesplegarColumnaCal('" & Replace(sKeyCabecera, "PUNT", "CAL") & "'," & iIndiceCabeceraMulti & ")""><img src=""" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/calificacion.gif"" border=""0"" alt=""" & Textos(52) & """></a>&nbsp;"
        End If
        If bCerrar Then
            PlantillaCabecera = PlantillaCabecera & "<a href=""javascript:OcultarColumna('" & sKeyCabecera & "'," & iIndiceCabeceraMulti & "," & IIf(iIndiceCabeceraMulti = IIf(bMSIE7, 1, 2), "true", "false") & ")""><img src=""" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/cerrar.gif"" border=""0"" alt=""" & Textos(53) & """></a>"
        End If

        If bMSIE7 Then
            PlantillaCabecera = PlantillaCabecera & "</td></tr></table>"
        Else
            PlantillaCabecera = PlantillaCabecera & "</div></div>"
        End If
    End Function
    ''' <summary>
    ''' Inicializa las templated column de cada fila:Boton de de detalle de unqas,  botón de detalle de los materiales de GS e Imagen de tipo de proveedor
    ''' Pone el tooltip a todas las celdas con el código/denominación del proveedor
    ''' Formatea la puntuación y controla el texto que se muestra del material de QA 
    ''' </summary>
    ''' <param name="sender">sender</param>
    ''' <param name="e">roweventsarg</param>
    ''' <remarks></remarks>
    Private Sub wdgProveedores_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wdgProveedores.InitializeRow
        If Not bExportando Then
            'ImageButton de UNQAS
            e.Row.Items.FindItemByKey("UNQAS").Text = "<div align='center' 
                onclick='MostrarArbol(1,""" & e.Row.Items.FindItemByKey("PROVE_NAME").Value & "@#@" & e.Row.Items.FindItemByKey("PROVE_COD").Value & """)'>
                <img id='imgMostrarUnQa' src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/detalle.gif'/></div>"
            'ImageButton de MATERIAL_GS
            e.Row.Items.FindItemByKey("MATERIAL_GS").Text = "<div align='center' 
                onclick='MostrarArbol(2,""" & e.Row.Items.FindItemByKey("PROVE_NAME").Value & "@#@" & e.Row.Items.FindItemByKey("PROVE_COD").Value & "@#@" & e.Row.Items.FindItemByKey("MATERIAL_QA_ID").Value & """)'>
                <img id='imgMostrarMatGs' src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/detalle.gif'/></div>"

            'Image de Potencia/Panel de Calidad Activos/Baja
            Dim img As System.Web.UI.WebControls.Image = CType(e.Row.Items.FindItemByKey("PROVE_DEN").FindControl("ibtnTIPO_PROVE"), System.Web.UI.WebControls.Image)
            Dim Potencial As String = e.Row.Items.FindItemByKey("PROVE_CALIDAD_POT").Value
            Dim Baja As String = e.Row.Items.FindItemByKey("PROVE_CALIDAD_BAJA").Value
            Dim sFile As String = "tipocalidad_" & Potencial & "_" & Baja
            If Acceso.gbPotAValProvisionalSelProve AndAlso Potencial = "2" AndAlso e.Row.Items.FindItemByKey("PROVE_CALIDAD_PROVISIONAL").Value = "1" Then sFile &= "_" & e.Row.Items.FindItemByKey("PROVE_CALIDAD_PROVISIONAL").Value
            img.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/" & sFile & ".gif"
            Dim tooltip As String = ""
            If Potencial = "1" Then
                tooltip = Textos(28)
            ElseIf Potencial = "2" Then
                If e.Row.Items.FindItemByKey("PROVE_CALIDAD_PROVISIONAL").Value = "1" Then
                    tooltip = Textos(86)
                Else
                    tooltip = Textos(51)
                End If
            End If
            If Baja = "1" Then
                tooltip = tooltip & "/" & Textos(29)
            End If
            img.ToolTip = tooltip

            'ImageButton de Error
            If e.Row.Items.FindItemByKey("HAYERROR").Value > 0 Then
                Dim imgError As System.Web.UI.WebControls.ImageButton = CType(e.Row.Items.FindItemByKey("PROVE_DEN").FindControl("imgErrorCalPunt"), System.Web.UI.WebControls.ImageButton)
                imgError.Visible = True
                imgError.ToolTip = Textos(76)
                If Not IsNothing(imgError) Then ScriptMgr.RegisterAsyncPostBackControl(imgError)
            End If
            e.Row.Items.FindItemByKey("CON_EMAIL").CssClass = "SinSalto"
            e.Row.Items.FindItemByKey("MATERIAL_QA").CssClass = "SinSalto"
            e.Row.Items.FindItemByKey("MATERIAL_QA").Tooltip = e.Row.Items.FindItemByKey("MATERIAL_QA").Value

            If Not bMSIE7 Then
                'Tooltip proveedorees
                Dim lbProveDen As System.Web.UI.WebControls.LinkButton = CType(e.Row.Items.FindItemByKey("PROVE_DEN").FindControl("lbFichaProveedor"), System.Web.UI.WebControls.LinkButton)
                Dim sProveDen As String = e.Row.Items.FindItemByKey("PROVE_NAME").Value
                If Len(StrConv(sProveDen, VbStrConv.Uppercase)) > 35 AndAlso Request("__EVENTARGUMENT") <> "Excel" AndAlso Request("__EVENTARGUMENT") <> "Pdf" Then
                    lbProveDen.Text = Left(e.Row.Items.FindItemByKey("PROVE_NAME").Value, 35) & "..."
                Else
                    lbProveDen.Text = e.Row.Items.FindItemByKey("PROVE_NAME").Value
                End If
                e.Row.Items.FindItemByKey("PROVE_DEN").Tooltip = e.Row.Items.FindItemByKey("PROVE_NAME").Value
            Else
                e.Row.Items.FindItemByKey("PROVE_CIF").Tooltip = e.Row.Items.FindItemByKey("PROVE_COD").Value & "-" & e.Row.Items.FindItemByKey("PROVE_NAME").Value & Chr(13) & e.Row.Items.FindItemByKey("PROVE_CIF").Value
                e.Row.Items.FindItemByKey("CON_EMAIL").Tooltip = e.Row.Items.FindItemByKey("PROVE_COD").Value & "-" & e.Row.Items.FindItemByKey("PROVE_NAME").Value & Chr(13) & e.Row.Items.FindItemByKey("CON_EMAIL").Value
            End If

            'Contactos múltiples
            If e.Row.Items.FindItemByKey("CON_EMAIL").Value = "##" Then e.Row.Items.FindItemByKey("CON_EMAIL").Text = "(" & Textos(87) & ")"

            'Puntuacion y calificacion:Formato puntuacion, tooltip proveedor
            Dim KeyUNQA As String
                Dim KeyCal As String
                For Each VarCal As Collection In FiltroVarCal
                    For Each UNQA As Collection In FiltroUNQA
                        KeyUNQA = "PUNT_" & VarCal("ID") & "_" & VarCal("NIVEL") & "_" & UNQA("ID")
                        KeyCal = "CAL_" & VarCal("ID") & "_" & VarCal("NIVEL") & "_" & UNQA("ID")

                        'Comprobar que el material qa del proveedor (MATERIAL_QA_ID)
                        'se encuentra en la columna leida
                        Dim aMateriales() As String = Split(VarCal("MATQA"), "_")
                        Dim bEncontrado As Boolean = False
                        For iMat As Integer = 0 To aMateriales.Length - 1
                            If aMateriales(iMat) = TODOS_MATQA Then
                                bEncontrado = True
                                Exit For
                            End If
                            If e.Row.Items.FindItemByKey("MATERIAL_QA_ID").Value <> "" Then
                                If e.Row.Items.FindItemByKey("MATERIAL_QA_ID").Value.IndexOf("," & aMateriales(iMat) & ",") <> -1 Then
                                    bEncontrado = True
                                    Exit For
                                End If
                            End If
                        Next

                        If bEncontrado Then
                            e.Row.Items.FindItemByKey(KeyUNQA).Text = modUtilidades.FormatNumber(DBNullToDbl(e.Row.Items.FindItemByKey(KeyUNQA).Value), FSNUser.NumberFormat)
                        Else
                            e.Row.Items.FindItemByKey(KeyUNQA).Text = ""
                        End If
                        If bMSIE7 Then
                            e.Row.Items.FindItemByKey(KeyUNQA).Tooltip = e.Row.Items.FindItemByKey("PROVE_COD").Value & "-" & e.Row.Items.FindItemByKey("PROVE_NAME").Value
                            e.Row.Items.FindItemByKey(KeyCal).Tooltip = e.Row.Items.FindItemByKey("PROVE_COD").Value & "-" & e.Row.Items.FindItemByKey("PROVE_NAME").Value & Chr(13) & e.Row.Items.FindItemByKey(KeyCal).Value
                        End If
                    Next
                Next

                Dim sMaterialQA As String = e.Row.Items.FindItemByKey("MATERIAL_QA").Value
                If bMSIE7 Then
                    'ToolTip Material QA
                    If Len(sMaterialQA) > 70 AndAlso Request("__EVENTARGUMENT") <> "Excel" AndAlso Request("__EVENTARGUMENT") <> "Pdf" Then
                        e.Row.Items.FindItemByKey("MATERIAL_QA").Text = Left(e.Row.Items.FindItemByKey("MATERIAL_QA").Value, 70) & "..."
                        e.Row.Items.FindItemByKey("MATERIAL_QA").Tooltip = e.Row.Items.FindItemByKey("PROVE_COD").Value & "-" & e.Row.Items.FindItemByKey("PROVE_NAME").Value & Chr(13) & sMaterialQA
                    Else
                        e.Row.Items.FindItemByKey("MATERIAL_QA").Text = e.Row.Items.FindItemByKey("MATERIAL_QA").Value
                        e.Row.Items.FindItemByKey("MATERIAL_QA").Tooltip = e.Row.Items.FindItemByKey("PROVE_COD").Value & "-" & e.Row.Items.FindItemByKey("PROVE_NAME").Value
                    End If
                End If

                'Poner fondo gris a las columnas de las var. que no aplican
                'Generamos un grupo por cada variable de calidad
                For Each VarCal As Collection In FiltroVarCal
                    Dim tabla As DataTable = DirectCast(wdgProveedores.DataSource, System.Data.DataSet).Tables("TablaSort")
                    'Creo las columnas correspondientes a esta variable de calidad
                    For i = 0 To tabla.Columns.Count - 1
                        If tabla.Columns(i).ColumnName.Contains("_" & VarCal("ID") & "_" & VarCal("NIVEL") & "_") Then
                            If Left(tabla.Columns(i).ColumnName, 5) = "PUNT_" Or Left(tabla.Columns(i).ColumnName, 4) = "CAL_" Then
                                Dim sColumnName As String = Nothing
                                If Left(tabla.Columns(i).ColumnName, 5) = "PUNT_" Then
                                    sColumnName = "APLICA_" & tabla.Columns(i).ColumnName.Substring(5, tabla.Columns(i).ColumnName.Length - 5)
                                Else
                                    If Left(tabla.Columns(i).ColumnName, 4) = "CAL_" Then sColumnName = "APLICA_" & tabla.Columns(i).ColumnName.Substring(4, tabla.Columns(i).ColumnName.Length - 4)
                                End If

                                If Not e.Row.Items.FindItemByKey(sColumnName).Value Is DBNull.Value AndAlso Not CType(e.Row.Items.FindItemByKey(sColumnName).Value, Boolean) Then
                                    e.Row.Items.FindItemByKey(tabla.Columns(i).ColumnName).CssClass = "celdaGris"
                                End If
                            End If
                        End If
                    Next
                Next
            Else
                Dim KeyUNQA As String
            Dim KeyCal As String
            For Each VarCal As Collection In FiltroVarCal
                For Each UNQA As Collection In FiltroUNQA
                    KeyUNQA = "PUNT_" & VarCal("ID") & "_" & VarCal("NIVEL") & "_" & UNQA("ID")
                    KeyCal = "CAL_" & VarCal("ID") & "_" & VarCal("NIVEL") & "_" & UNQA("ID")

                    'Comprobar que el material qa del proveedor (MATERIAL_QA_ID)
                    'se encuentra en la columna leida
                    Dim aMateriales() As String = Split(VarCal("MATQA"), "_")
                    Dim bEncontrado As Boolean = False
                    For iMat As Integer = 0 To aMateriales.Length - 1
                        If aMateriales(iMat) = TODOS_MATQA Then
                            bEncontrado = True
                            Exit For
                        End If
                        If e.Row.Items.FindItemByKey("MATERIAL_QA_ID").Value <> "" Then
                            If e.Row.Items.FindItemByKey("MATERIAL_QA_ID").Value.IndexOf("," & aMateriales(iMat) & ",") <> -1 Then
                                bEncontrado = True
                                Exit For
                            End If
                        End If
                    Next

                    If bEncontrado Then
                        e.Row.Items.FindItemByKey(KeyUNQA).Text = modUtilidades.FormatNumber(DBNullToDbl(e.Row.Items.FindItemByKey(KeyUNQA).Value), FSNUser.NumberFormat)
                    Else
                        e.Row.Items.FindItemByKey(KeyUNQA).Text = ""
                    End If
                Next
            Next
        End If
    End Sub
    ''' <summary>
    '''Evento que se lanza cuando se pulsa el icono de ver detalle de las unidades de negocio o de los materiales de QA
    ''' </summary>
    ''' <param name="sender">wdgProveedores</param>
    ''' <param name="e"></param>
    ''' <remarks>HandleCommandEventArgs de ItemCommand</remarks>
    Private Sub wdgProveedores_ItemCommand(sender As Object, e As Infragistics.Web.UI.GridControls.HandleCommandEventArgs) Handles wdgProveedores.ItemCommand

        Select Case e.CommandName
            Case "ERR_CALC_PUNT"
                ConfigurarPopUpERR_CALC_PUNT()
        End Select
    End Sub
    ''' <summary>
    ''' muestra un pop up con un treeview de las unidades de negocio que tiene asociadas el proveedor
    ''' </summary>
    ''' <param name="Datos">Name@#@Codigo</param>
    ''' <remarks>Llamada desde: Page_load ; Tiempo máximo: 0</remarks>
    Private Sub ConfigurarPopUpUNQAS(ByVal Datos As String)
        Dim Prove() As String = Split(Datos, "@#@") 'Name@#@Codigo
        lbltituloUNQA.Text = "<span class=""TituloF"">" & Textos(34) & "</span><br><b>" & UCase(Prove(0)) & "</b>"
        upUNQATitulo.Update()
        Dim PMUnidadesQA As FSNServer.UnidadesNeg = FSNServer.Get_Object(GetType(FSNServer.UnidadesNeg))
        Dim dsUNQA As DataSet = PMUnidadesQA.ProveGetData_hds(Prove(1), Me.Idioma)
        tvUNQA.DataSource = New HierarchicalDataSet(dsUNQA, "ID", "PADRE")
        tvUNQA.DataBind()
        Dim Acceso As Boolean = False
        ConfigurarAccesoTreeViewUNQA(tvUNQA.Nodes, Acceso, Prove(1))
        EliminarUNQASinAcceso(tvUNQA.Nodes, tvUNQA.Nodes.Count)
        tvUNQA.CollapseAll()
        upUNQA.Update()
        mpeUNQAS.Show()
    End Sub
    ''' <summary>
    ''' Configura el treeview de unidades de negocio de un proveedor para mostrar un icono de chequeado para las unqas que tiene asociadas
    ''' </summary>
    ''' <param name="Nodos">coleccion de nodods del treeview</param>
    ''' <param name="Acceso">Si el nodo padre tiene acceso directo o no</param>
    ''' <param name="CodProve">Cod del proveedor</param>
    ''' <param name="ConAcceso">Si el nodo padre tiene acceso directo</param>
    ''' <remarks>ConfigurarPopUpUNQAS</remarks>
    Sub ConfigurarAccesoTreeViewUNQA(ByRef Nodos As TreeNodeCollection, ByRef Acceso As Boolean, ByVal CodProve As String, Optional ByVal ConAcceso As Boolean = False)
        For Each Nodo As TreeNode In Nodos
            If ConAcceso Then
                Nodo.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/chequeado.gif"
                Nodo.ToolTip = CodProve
                If Nodo.ChildNodes.Count > 0 Then
                    ConfigurarAccesoTreeViewUNQA(Nodo.ChildNodes, Acceso, CodProve, True)
                End If
            Else
                'Si tiene hijos y no tiene acceso directo, comprobamos acceso a hijos
                If Nodo.ChildNodes.Count > 0 Then
                    If Nodo.ToolTip = "" Then
                        Acceso = False
                        ConfigurarAccesoTreeViewUNQA(Nodo.ChildNodes, Acceso, CodProve)
                        If Acceso Then
                            Nodo.ToolTip = CodProve
                        End If
                    Else
                        Nodo.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/chequeado.gif"
                        ConfigurarAccesoTreeViewUNQA(Nodo.ChildNodes, Acceso, CodProve, True)
                    End If

                Else
                    If Nodo.ToolTip <> "" Then
                        Nodo.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/chequeado.gif"
                        Acceso = True
                    End If
                End If
            End If
        Next
    End Sub
    ''' <summary>
    ''' Elimina del treeview las unqas sin acceso
    ''' </summary>
    ''' <param name="Nodos">Coleccion de Nodos</param>
    ''' <param name="CountNodes">Numero de nodos de la col</param>
    ''' <remarks>ConfigurarPopUpUNQAS</remarks>
    Sub EliminarUNQASinAcceso(ByRef Nodos As TreeNodeCollection, ByRef CountNodes As Integer)
        Dim i As Integer = 0
        Do While i < CountNodes
            If Nodos(i).ToolTip = "" Then
                Nodos.Remove(Nodos(i))
                CountNodes = CountNodes - 1
            ElseIf Nodos(i).ChildNodes.Count > 0 Then
                EliminarUNQASinAcceso(Nodos(i).ChildNodes, Nodos(i).ChildNodes.Count)
                i = i + 1
            Else
                i = i + 1
            End If

        Loop

    End Sub
    ''' <summary>
    ''' muestra un pop up con un treeview de los materiales de GS que tiene asociados el proveedor
    ''' </summary>
    ''' <param name="Datos">Name@#@Codigo@#@MatQA</param>
    ''' <remarks>Llamada desde: Page_load ; Tiempo máximo: 0</remarks>
    Private Sub ConfigurarPopUpMATGS(ByVal Datos As String)
        Dim Prove() As String = Split(Datos, "@#@") 'Name@#@Codigo@#@MatQA

        lblTituloMatGS.Text = "<span class=""TituloF"">" & Textos(30) & "</span><br><b>" & UCase(Prove(0)) & "</b>"
        upMatGSTitulo.Update()

        Dim PMMatGS As FSNServer.Proveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
        Dim sMateriales As String = Prove(2)
        sMateriales = Mid(sMateriales, 2, Len(sMateriales) - 2)
        Dim dsMatGS As DataSet = PMMatGS.DevolverMaterialesGS_hds(Prove(1), sMateriales, Me.Idioma)

        tvMatGS.DataSource = New HierarchicalDataSet(dsMatGS, "ID", "PADRE")
        tvMatGS.DataBind()
        tvMatGS.CollapseAll()
        upMatGS.Update()
        mpeMatGS.Show()
    End Sub
    ''' <summary>
    ''' Se ejecuta cuando se pulsa en el icono de cambiar el sentido de la ordenación
    ''' </summary>
    ''' <param name="sender">ibOrdenacion</param>
    ''' <param name="e">ImageClickEventArgs del Click</param>
    ''' <remarks></remarks>
    Private Sub ibOrdenacion_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibOrdenacion.Click
        If ibOrdenacion.ImageUrl.Contains("Descendente") Then
            ibOrdenacion.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Ascendente.gif"

            Session("OrdenacionPanel") = wddOrdenar.SelectedValue & " ASC"
        Else
            ibOrdenacion.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Descendente.gif"

            Session("OrdenacionPanel") = wddOrdenar.SelectedValue & " DESC"
        End If
    End Sub
    ''' <summary>
    ''' Exporta la grid a Excel
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ExportarExcel()
        Exportar_AGridAuxiliar()
        wdgExcelExporter.DownloadName = Me.FSNPageHeader.TituloCabecera & ".xls"
        wdgExcelExporter.DataExportMode = DataExportMode.AllDataInDataSource
        wdgExcelExporter.Export(wdgProveedoresAuxiliar)
    End Sub
    ''' <summary>
    ''' Exporta la grid a pdf
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ExportarPdf()
        Exportar()
        With wdgPDFExporter
            .DownloadName = Me.FSNPageHeader.TituloCabecera & ".pdf"
            .Format = Infragistics.Documents.Reports.Report.FileFormat.PDF
            .TargetPaperSize = Infragistics.Documents.Reports.Report.PageSizes.A3
            .Margins = Infragistics.Documents.Reports.Report.PageMargins.GetPageMargins("Narrow")
            .TargetPaperOrientation = Infragistics.Documents.Reports.Report.PageOrientation.Landscape
            .Export(wdgProveedores)
        End With
        wdgProveedores.Behaviors.Paging.Enabled = True
    End Sub
    ''' <summary>
    ''' Configura la grid oculta y sin grupos para su posterior exportación
    ''' http://fsge.fullstep.com/App/expediente.aspx?exp=56214 Lo q se ha hecho en las captions, en dos lineas y escribiendo nosotros el html, y grupos hace q infragistics control webexcelexporter muestre mal los datos del primer proveedor
    ''' </summary>
    '''  <remarks>Llamada desde: ExportarExcel ; Tiempo máximo: 0</remarks>
    Private Sub Exportar_AGridAuxiliar()
        wdgProveedoresAuxiliar.Rows.Clear()
        wdgProveedoresAuxiliar.DataSource = dsProveedores.Tables("TablaSort")
        wdgProveedoresAuxiliar.DataMember = "TablaSort"

        Dim tabla As DataTable = dsProveedores.Tables("TablaSort")
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim nombre As String
        For Each VarCal As Collection In FiltroVarCal
            For i = 0 To tabla.Columns.Count - 1
                campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
                nombre = tabla.Columns(i).ToString
                If nombre.Contains("_" & VarCal("ID") & "_" & VarCal("NIVEL") & "_") Then
                    If Left(nombre, 5) = "PUNT_" Or Left(nombre, 4) = "CAL_" Then
                        With campoGrid
                            .Key = nombre
                            .DataFieldName = nombre
                            .Header.Text = nombre
                            .Header.Tooltip = nombre
                        End With
                        wdgProveedoresAuxiliar.Columns.Add(campoGrid)
                    End If
                End If
            Next
        Next

        ''''
        wdgProveedoresAuxiliar.DataKeyFields = "PROVE_COD"
        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.PuntProveedores

        With wdgProveedoresAuxiliar
            .Columns("PROVE_NAME").Header.Text = Textos(40)
            .Columns("PROVE_NAME").VisibleIndex = 0

            .Columns("PROVE_COD").Header.Text = Textos(22)
            .Columns("PROVE_COD").Hidden = Not VisibleCliente("PROVE_COD")

            .Columns("PROVE_CIF").Header.Text = Textos(23)
            .Columns("PROVE_CIF").Hidden = Not VisibleCliente("PROVE_CIF")

            .Columns("CON_EMAIL").Header.Text = Textos(27)
            .Columns("CON_EMAIL").Hidden = Not VisibleCliente("CON_EMAIL")

            .Columns("MATERIAL_QA").Header.Text = Textos(24)
            .Columns("MATERIAL_QA").Hidden = Not VisibleCliente("MATERIAL_QA")


            Dim KeyUNQA As String
            Dim KeyCal As String
            For Each VarCal As Collection In FiltroVarCal
                For Each UNQA As Collection In FiltroUNQA
                    KeyUNQA = "PUNT_" & VarCal("ID") & "_" & VarCal("NIVEL") & "_" & UNQA("ID")
                    KeyCal = "CAL_" & VarCal("ID") & "_" & VarCal("NIVEL") & "_" & UNQA("ID")
                    .Columns(KeyUNQA).Header.Text = VarCal("DEN") & "/" & UNQA("DEN")
                    .Columns(KeyCal).Header.Text = VarCal("DEN") & "/" & UNQA("DEN") & "/" & Textos(39)
                    .Columns(KeyUNQA).Hidden = Not VisibleCliente(KeyUNQA)
                    .Columns(KeyCal).Hidden = Not VisibleCliente(KeyCal)
                Next
            Next
        End With
        wdgProveedoresAuxiliar.DataBind()
    End Sub
    ''' <summary>
    ''' Configura la grid para su posterior exportación
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Exportar()
        wdgProveedores.Rows.Clear()
        Dim contcolumn = wdgProveedores.Columns.Count - 1
        For i = 13 To contcolumn
            wdgProveedores.Columns.RemoveAt(13)
        Next
        wdgProveedores.Behaviors.Paging.Enabled = False
        wdgProveedores.DataSource = dsProveedores
        wdgProveedores.DataMember = "TablaSort"
        CrearColumnas()
        wdgProveedores.DataKeyFields = "PROVE_COD"
        ConfigurarGrid()
        wdgProveedores.Columns("PROVE_DEN").Hidden = True

        With wdgProveedores
            .Columns("PROVE_CALIDAD_POT").Hidden = True
            .Columns("PROVE_CALIDAD_BAJA").Hidden = True
            .Columns("MATERIAL_QA_ID").Hidden = True
            .Columns("HAYERROR").Hidden = True
            .Columns("PROVE_NAME").Hidden = False
            .Columns("PROVE_NAME").Header.Text = Textos(40)
            .Columns("PROVE_NAME").Header.CssClass = "fila_cabecera"
            .Columns("PROVE_NAME").VisibleIndex = 0
            .Columns("PROVE_COD").Header.Text = Textos(22)
            .Columns("PROVE_COD").Hidden = Not VisibleCliente("PROVE_COD")
            .Columns("PROVE_COD").Header.CssClass = "fila_cabecera"
            .Columns("PROVE_CIF").Header.Text = Textos(23)
            .Columns("PROVE_CIF").Hidden = Not VisibleCliente("PROVE_CIF")
            .Columns("PROVE_CIF").Header.CssClass = "fila_cabecera"
            .Columns("CON_EMAIL").Header.Text = Textos(27)
            .Columns("CON_EMAIL").Hidden = Not VisibleCliente("CON_EMAIL")
            .Columns("CON_EMAIL").Header.CssClass = "fila_cabecera"
            .Columns("MATERIAL_QA").Header.Text = Textos(24)
            .Columns("MATERIAL_QA").Hidden = Not VisibleCliente("MATERIAL_QA")
            .Columns("MATERIAL_QA").Header.CssClass = "fila_cabecera"
            .Columns("MATERIAL_GS").Hidden = True
            .Columns("UNQAS").Hidden = True
        End With
        For i = 0 To 12 'COLUMNAS DE DATOS DE PROVEEDOR
            wdgProveedores.Columns(i).CssClass = "bordes"
        Next
        Dim KeyUNQA As String
        Dim KeyCal As String
        For Each VarCal As Collection In FiltroVarCal
            For Each UNQA As Collection In FiltroUNQA
                KeyUNQA = "PUNT_" & VarCal("ID") & "_" & VarCal("NIVEL") & "_" & UNQA("ID")
                KeyCal = "CAL_" & VarCal("ID") & "_" & VarCal("NIVEL") & "_" & UNQA("ID")
                With DirectCast(wdgProveedores.Columns("VARCAL_" & VarCal("ID") & "_" & VarCal("NIVEL")), Infragistics.Web.UI.GridControls.GroupField)
                    .Columns(KeyUNQA).Header.Text = VarCal("DEN") & "/" & UNQA("DEN")
                    .Columns(KeyCal).Header.Text = VarCal("DEN") & "/" & UNQA("DEN") & "/" & Textos(39)
                    .Columns(KeyUNQA).Header.CssClass = "fila_cabecera"
                    .Columns(KeyCal).Header.CssClass = "fila_cabecera"
                    .Columns(KeyUNQA).CssClass = "bordes"
                    .Columns(KeyCal).CssClass = "bordes"
                    .Columns(KeyUNQA).Hidden = Not VisibleCliente(KeyUNQA)
                    .Columns(KeyCal).Hidden = Not VisibleCliente(KeyCal)
                End With
            Next
        Next
        wdgProveedores.DataBind()
    End Sub
    ''' <summary>
    ''' Guarda para el usuario los filtro de unqas y variables de calidad y los campos de proveedor ocultos en el panel
    ''' </summary>
    ''' <param name="hCamposOcultos">los filtro de unqas y variables de calidad y los campos de proveedor ocultos en el panel</param> 
    ''' <remarks>Llamada desde: GuardarConf() es un PageMethods ; Tiempo máximo: 0</remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub btnGuardarConf_Click(ByVal hCamposOcultos As String)
        Dim PMUsuario As FSNServer.User

        PMUsuario = HttpContext.Current.Session("FSN_Server").Get_Object(GetType(FSNServer.User))
        HttpContext.Current.Session("CamposOcultosPC") = hCamposOcultos

        PMUsuario.Actualizar_ConfiguracionPanelCalidad(HttpContext.Current.Session("FSN_User").Cod, HttpContext.Current.Session("FiltroUNQA"), HttpContext.Current.Session("FiltroVarCal"), HttpContext.Current.Session("CamposOcultosPC"), HttpContext.Current.Session("OrdenacionPanel"))
    End Sub
    ''' <summary>
    ''' Modifica el texto de procesando del modal pop up de espera
    ''' </summary>
    ''' <param name="sender">lblProcesando</param>
    ''' <param name="e">EventArgs del PreRender</param>
    ''' <remarks></remarks>
    Protected Sub lblProcesando_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        CType(sender, Label).Text = Textos(59)
    End Sub
    ''' <summary>
    ''' Devuelve si al columna que se corresponde con la Key está o no visible en cliente
    ''' </summary>
    ''' <param name="Key">Clave de la columna</param>
    ''' <returns>True:visible en cliente/False: no vosoble en cliente</returns>
    ''' <remarks></remarks>
    Private Function VisibleCliente(ByVal Key As String) As Boolean
        Return ColumnasVisiblesCliente.Contains(";" & Key & ";")
    End Function
    ''' <summary>
    ''' Muestra el pop up del detalle de los errores que se han producido en el cÃ¡lculo de puntuaciones para el proveedor seleccionado en la fila del grid
    ''' </summary>
    Sub ConfigurarPopUpERR_CALC_PUNT()
        Dim Row = wdgProveedores.Behaviors.Activation.ActiveCell.Row
        Dim oDetalleError As FSNServer.Proveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
        Dim dsDetalleError As DataSet

        lblTituloPopUp.Text = Textos(73)
        lblSubTituloPopUp.Text = Textos(74)
        lnkPrint.Text = Textos(75)

        dsDetalleError = oDetalleError.DevolverDetalleErrorProveedor(Row.Items.FindItemByKey("PROVE_COD").Value, FSNUser.Cod, FSNUser.Idioma)

        Dim cinfo As New Globalization.CultureInfo(Me.Usuario.Idioma.RefCultural)
        Dim Mes As Integer = dsDetalleError.Tables(0).Rows(0)("MES")
        Mes = IIf(Mes = 1, 11, Mes - 2)

        lblInfoErrores.Text = Textos(61) & " <b>" & cinfo.DateTimeFormat.MonthNames(Mes).ToUpper & "</b>" &
            Textos(62) & " <b>" & dsDetalleError.Tables(0).Rows(0)("PROVEEDOR") & "</b>" &
            Textos(63) & " <b>" & dsDetalleError.Tables(0).Rows(0)("VARIABLECALIDAD") & "</b>" &
            Textos(64) & " <b>" & dsDetalleError.Tables(0).Rows(0)("UNIDADNEGOCIO") & "</b>"


        Select Case dsDetalleError.Tables(0).Rows(0)("TIPOERROR")
            Case 1
                lblTipoError.Text = "<b>" & Textos(65) & "</b><span style=""color:Red;"">" & Textos(66) & "</span>"
            Case 2
                lblTipoError.Text = "<b>" & Textos(65) & "</b><span style=""color:Red;"">" & Textos(67) & "</span>"
            Case 3
                lblTipoError.Text = "<b>" & Textos(65) & "</b><span style=""color:Red;"">" & Textos(68) & "</span>"
            Case 4
                lblTipoError.Text = "<b>" & Textos(65) & "</b><span style=""color:Red;"">" & Textos(69) & "</span>"
            Case 5
                lblTipoError.Text = "<b>" & Textos(65) & "</b><span style=""color:Red;"">" & Textos(70) & "</span>"
            Case 6
                lblTipoError.Text = "<b>" & Textos(65) & "</b><span style=""color:Red;"">" & Textos(71) & "</span>"
        End Select

        If Not CType(dsDetalleError.Tables(0).Rows(0)("PERMISOVARIABLECALIDAD"), Boolean) OrElse Not CType(dsDetalleError.Tables(0).Rows(0)("PERMISOUNQA"), Boolean) Then
            pnlDetalleFormula.Visible = False
        Else
            Dim VariablesFormula() As String = Split(DBNullToStr(dsDetalleError.Tables(0).Rows(0)("LEYENDA")), ";")
            Dim ValoresFormula() As String = Split(DBNullToStr(dsDetalleError.Tables(0).Rows(0)("VALORES_FORMULA")), ";")

            lblTituloFormula.Text = "<b>" & Textos(72) & "</b>"
            If dsDetalleError.Tables(0).Rows(0)("OPCION_CONF") = VarCalOpcionConf.MediaPonderadaSegunVarHermana Then
                lblFormula.Text = Textos(88).Replace("@VARCAL", dsDetalleError.Tables(0).Rows(0)("DEN_VAR_POND"))
            Else
                lblFormula.Text = dsDetalleError.Tables(0).Rows(0)("FORMULA")
            End If

            Dim dRow As HtmlControls.HtmlTableRow
            Dim dCell As HtmlControls.HtmlTableCell

            Dim NumeroVariablesFormula As Integer
            NumeroVariablesFormula = IIf(VariablesFormula.Length = 1, 0, VariablesFormula.Length - 2)
            For i As Integer = 0 To NumeroVariablesFormula
                If VariablesFormula(i).Length > 0 Then
                    dRow = New HtmlControls.HtmlTableRow

                    dCell = New HtmlControls.HtmlTableCell
                    dCell.Attributes.Add("class", "CeldaVariable" & IIf(Replace(Split(VariablesFormula(i), ":")(0), "X", "") <> dsDetalleError.Tables(0).Rows(0)("VARIABLEERROR"), "", "Rojo"))
                    dCell.Attributes.Add("style", "width:10%;")
                    dCell.InnerHtml = "<b>" & Split(VariablesFormula(i), ":")(0) & "</br>"
                    dRow.Cells.Add(dCell)

                    dCell = New HtmlControls.HtmlTableCell
                    dCell.Attributes.Add("class", "CeldaDefinicion" & IIf(Replace(Split(VariablesFormula(i), ":")(0), "X", "") <> dsDetalleError.Tables(0).Rows(0)("VARIABLEERROR"), "", "Rojo"))
                    dCell.Attributes.Add("style", "width:80%;")
                    dCell.InnerHtml = "<b>" & Split(VariablesFormula(i), ":")(1) & "</br>"
                    dRow.Cells.Add(dCell)

                    dCell = New HtmlControls.HtmlTableCell
                    dCell.Attributes.Add("class", "CeldaValor" & IIf(Replace(Split(VariablesFormula(i), ":")(0), "X", "") <> dsDetalleError.Tables(0).Rows(0)("VARIABLEERROR"), "", "Rojo"))
                    dCell.Attributes.Add("style", "width:10%;")
                    Dim sValor As String = String.Empty
                    If i < ValoresFormula.Length Then sValor = ValoresFormula(i).ToString
                    dCell.InnerHtml = "<b>" & sValor & "</br>"
                    dRow.Cells.Add(dCell)

                    tblDetalleFormulaError.Rows.Add(dRow)
                End If
            Next
        End If
        updDetalleError.Update()
        mErroresCalcPunt.Show()
    End Sub
    ''' <summary>
    ''' Cortar un texto simulando el RowStyleDefault-TextOverflow="Ellipsis" q en Firefox no funciona
    ''' </summary>
    ''' <param name="Texto">Texto a cortar simulando el RowStyleDefault-TextOverflow="Ellipsis" q en Firefox no funciona</param>
    ''' <param name="EsPunt">Indica si es una columna de puntuación o no. Si lo es hay dos botones si no lo es hay un botón.</param>
    ''' <returns>Texto cortado para simular el RowStyleDefault-TextOverflow="Ellipsis"</returns>
    ''' <remarks>Llamada desde:PlantillaCabecera; Tiempo maximo:0 </remarks>
    Private Function CortarCabecera(ByVal Texto As String, Optional ByVal EsPunt As Boolean = False) As String
        If Request.Browser("Browser") = "IE" Then 'En IE va bien Ellipsis
            Return Texto
        Else 'En FireFox no va
            If EsPunt AndAlso (Len(Texto) > 10) Then
                Return Left(Texto, 8) & "..."
            ElseIf (Len(Texto) > 16) Then
                Return Left(Texto, 13) & "..."
            Else
                Return Texto
            End If
        End If
    End Function
    ''' <summary>
    ''' Carga un registro en el combo de Ordenación
    ''' </summary>
    ''' <param name="value">Valor del combo</param>
    ''' <param name="text">Texto del combo</param>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0seg.</remarks>
    Private Sub AñadirElementoOrden(ByVal value As String, ByVal text As String)
        Dim oItem As New ListControls.DropDownItem
        oItem.Value = value
        oItem.Text = text
        wddOrdenar.Items.Add(oItem)
    End Sub
    ''' <summary>
    ''' Se ejecuta cunando cambia la selección del webdropdown
    ''' </summary>
    ''' <param name="sender">wddOrdenar</param>
    ''' <param name="e">EventArgs de SelectionChanged</param>
    ''' <remarks>Llamada desde:sistema; Tiempo máximo:0seg.</remarks>
    Private Sub wddOrdenar_SelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles wddOrdenar.SelectionChanged
        hOrdenar.Value = wddOrdenar.SelectedValue

        If ibOrdenacion.ImageUrl.Contains("Descendente") Then
            Session("OrdenacionPanel") = wddOrdenar.SelectedValue & " DESC"
        Else
            Session("OrdenacionPanel") = wddOrdenar.SelectedValue & " ASC"
        End If

        upWdgProveedores.Update()
    End Sub
    Private Sub wdgProveedores_DataBound(sender As Object, e As System.EventArgs) Handles wdgProveedores.DataBound
        Dim _pageCount As Integer
        Dim _rows As Integer
        Dim dt As DataTable = CType(wdgProveedores.DataSource, DataSet).Tables("TablaSort")
        _rows = dt.Rows.Count
        _pageCount = _rows \ wdgProveedores.Behaviors.Paging.PageSize
        _pageCount = _pageCount + (IIf(_rows Mod wdgProveedores.Behaviors.Paging.PageSize = 0, 0, 1))
        Paginador(_pageCount)
        upWdgProveedores.Update()
    End Sub
    Private Sub Paginador(ByVal _pageCount As Integer)
        Dim pagerList As DropDownList = DirectCast(wdgProveedores.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
        pagerList.Items.Clear()
        For i As Integer = 1 To _pageCount
            pagerList.Items.Add(i.ToString())
        Next
        CType(wdgProveedores.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = _pageCount
        If _pageCount > 0 Then pagerList.SelectedIndex = _pageNumber
        Dim Desactivado As Boolean = (_pageNumber = 0)
        With CType(wdgProveedores.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        With CType(wdgProveedores.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        Desactivado = (_pageCount = 1 OrElse _pageNumber + 1 = _pageCount)
        With CType(wdgProveedores.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        With CType(wdgProveedores.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
    End Sub

    ''' <summary>Procedimiento que asigna los contactos de QA</summary>
    ''' <param name="Proveedor">Cod. proveedor</param>      
    ''' <remarks>Llamada desde: Contactos.aspx</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ObtenerContactosQA(ByVal Proveedor As String) As String()
        Dim oContactos As String() = {}

        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oProveedor As FSNServer.Proveedor
        oProveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
        oProveedor.Cod = Proveedor
        oProveedor.CargarContactosQA(, True)
        If Not oProveedor.Contactos Is Nothing AndAlso oProveedor.Contactos.Tables.Count > 0 Then
            oContactos = oProveedor.Contactos.Tables(0).AsEnumerable().[Select](Function(r) r.Field(Of String)("EMAIL")).ToArray()
        End If
        Return oContactos
    End Function

    Private Sub wdgExcelExporter_GridRecordItemExported(sender As Object, e As GridRecordItemExportedEventArgs) Handles wdgExcelExporter.GridRecordItemExported
        If e.GridCell.Column.Key = "CON_EMAIL" Then
			If e.GridCell.Row.DataItem.Item("CON_EMAIL").ToString = "##" AndAlso e.GridCell.Row.DataItem.Item("CON_EMAIL_VARIOS").ToString = "##" Then
				Dim Contactos As String() = ObtenerContactosQA(e.GridCell.Row.DataItem.Item("PROVE_COD"))
				Dim Result As String = ""
				For Each Contacto As String In Contactos
					If Not Result = "" Then Result = Result & "; "
					Result = Result & Contacto
				Next
				e.WorksheetCell.Value = Result
			ElseIf e.GridCell.Row.DataItem.Item("CON_EMAIL").ToString = "##" AndAlso Not (e.GridCell.Row.DataItem.Item("CON_EMAIL_VARIOS").ToString = "##") Then
				e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item("CON_EMAIL_VARIOS")
            End If
        End If
    End Sub

    Private Sub wdgPDFExporter_GridRecordItemExporting(sender As Object, e As DocumentGridRecordItemExportingEventArgs) Handles wdgPDFExporter.GridRecordItemExporting
        If e.GridCell.Column.Key = "CON_EMAIL" Then
			If e.GridCell.Row.DataItem.Item("CON_EMAIL").ToString = "##" AndAlso e.GridCell.Row.DataItem.Item("CON_EMAIL_VARIOS").ToString = "##" Then
				Dim Contactos As String() = ObtenerContactosQA(e.GridCell.Row.DataItem.Item("PROVE_COD"))
				Dim Result As String = ""
				For Each Contacto As String In Contactos
					If Not Result = "" Then Result = Result & "; "
					Result = Result & Contacto
				Next
				e.ExportValue = Result
			ElseIf e.GridCell.Row.DataItem.Item("CON_EMAIL").ToString = "##" AndAlso Not (e.GridCell.Row.DataItem.Item("CON_EMAIL_VARIOS").ToString = "##") Then
				e.ExportValue = e.GridCell.Row.DataItem.Item("CON_EMAIL_VARIOS")
            End If
        End If
    End Sub

    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Sub CambiarContactos(ByVal Proveedor As String, ByVal Contactos As String)
        Try
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim ds As DataSet = HttpContext.Current.Cache("dsPanelProveedores_" & FSNUser.Cod)

            'Lo q se usa para exportar
            ds.Tables("TablaSort").Select("PROVE_COD='" & Proveedor & "'")(0)("CON_EMAIL") = Contactos
            ds.Tables("TablaSort").Select("PROVE_COD='" & Proveedor & "'")(0)("CON_EMAIL_VARIOS") = Contactos
            'Lo general
            ds.Tables(0).Select("PROVE_COD='" & Proveedor & "'")(0)("CON_EMAIL") = Contactos
            ds.Tables(0).Select("PROVE_COD='" & Proveedor & "'")(0)("CON_EMAIL_VARIOS") = Contactos

            HttpContext.Current.Cache.Insert("dsPanelProveedores_" & FSNUser.Cod, ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0), CacheItemPriority.BelowNormal, Nothing)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class