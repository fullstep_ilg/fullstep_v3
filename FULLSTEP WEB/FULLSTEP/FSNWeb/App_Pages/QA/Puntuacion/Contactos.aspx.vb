﻿Imports Infragistics.Web.UI.GridControls

Partial Public Class Contactos
    Inherits FSNPage
    ''' <summary>
    ''' Carga la pagina
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:1seg.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not Page.IsPostBack Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS='" & System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "';</script>")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "proveCod", "<script>var proveCod='" & Request("codProv") & "';</script>")
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.PuntProveedores
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "multiplesContactos", "<script>var multiplesContactos='" & Textos(87) & "';</script>")

            FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/proveedores_buscar.gif"

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.SelectorCamposProveQA
            CargarRecursos()

            Me.ProveCod.Value = Request("codProv")
            If Request("Completo") = Nothing Then
                Me.Completo.Value = 0
            Else
                Me.Completo.Value = Request("Completo")
            End If

            'Configurar columna check
            CType(wdgDatos.Columns("CALIDAD"), BoundCheckBoxField).ValueConverter = New IntegerBooleanConverter()
            CType(wdgDatos.Columns("CALIDAD"), BoundCheckBoxField).CheckBox.CheckedImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/CheckboxChecked.gif"
            CType(wdgDatos.Columns("CALIDAD"), BoundCheckBoxField).CheckBox.UncheckedImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/CheckboxUnchecked.gif"

            Dim oProveedor As FSNServer.Proveedor
            oProveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
            oProveedor.Cod = Request("codProv")
            oProveedor.CargarContactosQA()

            If oProveedor.Contactos.Tables.Count > 0 AndAlso oProveedor.Contactos.Tables(0).Rows.Count > 0 Then
                wdgDatos.DataSource = oProveedor.Contactos.Tables(0)
                wdgDatos.DataBind()
                'Caption de la grid de materiales:
                Dim field As Infragistics.Web.UI.GridControls.BoundDataField = Me.wdgDatos.Columns.FromKey("NOMBRE")
                field.Header.Text = Textos(12)

                field = Me.wdgDatos.Columns.FromKey("EMAIL")
                field.Header.Text = Textos(14)
            End If
        End If
    End Sub
    ''' <summary>Carga los textos de los controles</summary>
    ''' <remarks>Llamada desde: Page_Load</remarks>
    Private Sub CargarRecursos()
        FSNPageHeader.TituloCabecera = Request("codProv") & " - " & Textos(15)
        cmdAceptar.Text = Textos(26)
        cmdCancelar.Text = Textos(27)
    End Sub

    ''' <summary>Procedimiento que asigna los contactos de QA</summary>
    ''' <param name="Proveedor">Cod. proveedor</param>
    ''' <param name="Contactos">Array con los contactos de QA</param>    
    ''' <remarks>Llamada desde: Contactos.aspx</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Sub AsignarContactosQA(ByVal Proveedor As String, ByVal Contactos As Integer())
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oProveedor As FSNServer.Proveedor
        oProveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
        oProveedor.Cod = Proveedor
        oProveedor.AsignarContactosQA(Contactos)
    End Sub

    Private Class IntegerBooleanConverter
        Implements IBooleanConverter

        Public ReadOnly Property DefaultFalseValue As Object Implements IBooleanConverter.DefaultFalseValue
            Get
                Return 0
            End Get
        End Property

        Public ReadOnly Property DefaultTrueValue As Object Implements IBooleanConverter.DefaultTrueValue
            Get
                Return 1
            End Get
        End Property

        Public Function IsFalse(value As Object) As Boolean Implements IBooleanConverter.IsFalse
            Return (value <= 0)
        End Function

        Public Function IsTrue(value As Object) As Boolean Implements IBooleanConverter.IsTrue
            Return (value > 0)
        End Function
    End Class

End Class