﻿Imports Infragistics.Web.UI.GridControls

Partial Public Class DetallePuntuacionPpmCargoTasa
    Inherits FSNPage
    Private Const TODOS_MATQA As String = "TODOS"
    Private _dsFichaProveedor As DataSet
    Private _ProveMatQA As String
    Private _Unidad As String
    Private _Subtipo As String
    Private _Variable As String
    Private _Nivel As String
    Private _ProveCod As String
    Private _ProveDen As String
    Private _dtDatosVC As DataRow
    Private _Puntos As String
    Private _Calif As String
    Private _DenUnidad As String
    Private _MaterialQa As String
    Private _VarOpcionConf As VarCalOpcionConf
    Private _Periodo As Integer
    Private m_bSaltarLayout As Byte = 0
    Private m_bCargoX1 As Boolean = False
    Private m_bCargoX2 As Boolean = False
    Private m_sImpReper As String
    Private m_sImpFact As String
    Private m_sMoneda As String
    Private _FiltroVarCal As Collection
    Private IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
    Private FiltroUNQA As Collection

    ''' <summary>
    ''' Propiedad que contiene el datarow con los datos de la variable de la ficha de calidad del proveedor a mostrar.
    ''' </summary>
    ''' <value></value>
    ''' <returns>DataRow</returns>
    ''' <remarks>Llamada desde: MostrarPanelPpmCargoTasa; Tiempo maximo:0</remarks>
    Protected ReadOnly Property dtDatosVC() As DataRow
        Get
            If _dtDatosVC Is Nothing Then
                If _dsFichaProveedor Is Nothing Then
                    Dim PMProveedor As FSNServer.Proveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                    _dsFichaProveedor = PMProveedor.DevolverFichaCalidad(_ProveCod, FSNUser.Cod, _Unidad, _Variable, Me.Idioma)
                    If _dsFichaProveedor.Tables.Count > 0 Then
                        ObtenerTablaFichaProveedor()
                    End If
                    InsertarEnCache("dsFichaProveedor_" & FSNUser.Cod, _dsFichaProveedor)
                Else
                    _dsFichaProveedor = CType(Cache("dsFichaProveedor_" & FSNUser.Cod), DataSet)
                End If

                For Each row As DataRow In _dsFichaProveedor.Tables("MaestroVariables").Rows
                    If row.Item("VC_ID") = _Variable AndAlso row.Item("VC_NIVEL") = _Nivel Then
                        _dtDatosVC = row

                        _Puntos = DBNullToStr(row.Item("PUNT_" & _Unidad))
                        _Calif = DBNullToStr(row.Item("CAL_" & _Unidad))

                        Exit For
                    End If
                Next
            End If
            Return _dtDatosVC
        End Get
    End Property
    Sub ObtenerTablaFichaProveedor()
        'Creamos la tabla que necesitamos para mostrar la ficha de calidad
        Dim dtVariables As DataTable = New DataTable("MaestroVariables")
        Dim dRow As DataRow
        Dim dColumna As DataColumn

        dColumna = New DataColumn("VC_ID", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("VC_NIVEL", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dtVariables.PrimaryKey = New DataColumn() {dtVariables.Columns("VC_ID"), dtVariables.Columns("VC_NIVEL")}
        dColumna = New DataColumn("VC_DEN", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("FORMULA", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("LEYENDA", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("TIPO", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("SUBTIPO", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("MODIFICAR", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        'añadimos una columna por cada UNQA

        dColumna = New DataColumn("PUNT_" & _Unidad, System.Type.GetType("System.Double"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("CAL_" & _Unidad, System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("FECACT_" & _Unidad, System.Type.GetType("System.DateTime"))
        dtVariables.Columns.Add(dColumna)

        'Añadimos las filas con las variables de calidad que el usuario puede ver y que afectan al proveedor
        For Each VarCal As Collection In FiltroVarCal
            Dim aMateriales() As String = Split(VarCal("MATQA"), "_")
            Dim bEncontrado As Boolean = False
            For iMat As Integer = 0 To aMateriales.Length - 1
                If aMateriales(iMat) = TODOS_MATQA Then
                    bEncontrado = True
                    Exit For
                End If
                If ProveMatQA.IndexOf("," & aMateriales(iMat) & ",") <> -1 Then
                    bEncontrado = True
                    Exit For
                End If
            Next
            If bEncontrado Then
                dRow = dtVariables.NewRow
                dRow("VC_ID") = VarCal("ID")
                dRow("VC_NIVEL") = VarCal("NIVEL")
                dRow("VC_DEN") = VarCal("DEN")
                dtVariables.Rows.Add(dRow)
            End If

        Next
        _dsFichaProveedor.Tables.Add(dtVariables)
        _dsFichaProveedor.Relations.Add("REL_VAR_PUNT", New DataColumn() {_dsFichaProveedor.Tables("MaestroVariables").Columns("VC_ID"), _dsFichaProveedor.Tables("MaestroVariables").Columns("VC_NIVEL")}, New DataColumn() {_dsFichaProveedor.Tables(0).Columns("VARCAL"), _dsFichaProveedor.Tables(0).Columns("NIVEL")}, False)
        'Recorremos la tabla de variables de calidad y obtenemos sus puntuaciones por cada unqa
        For Each Row As DataRow In dtVariables.Rows
            For Each RowPuntuacion As DataRow In Row.GetChildRows("REL_VAR_PUNT")
                Row("FORMULA") = RowPuntuacion("FORMULA")
                Row("LEYENDA") = RowPuntuacion("LEYENDA")
                Row("TIPO") = RowPuntuacion("TIPO")
                Row("SUBTIPO") = RowPuntuacion("SUBTIPO")
                Row("MODIFICAR") = RowPuntuacion("MODIFICAR")
                Row("PUNT_" & RowPuntuacion("UNQA")) = RowPuntuacion("PUNT")
                Row("CAL_" & RowPuntuacion("UNQA")) = RowPuntuacion("CALIFICACION")
                Row("FECACT_" & RowPuntuacion("UNQA")) = RowPuntuacion("FECACT")
                Dim _MesPuntuacion As Integer
                If FechaPuntuacion Is Nothing Then
                    If Not IsDBNull(RowPuntuacion("FECACT")) Then
                        _MesPuntuacion = Month(RowPuntuacion("FECACT"))
                        If _MesPuntuacion = 1 Then
                            FechaPuntuacion = ObtenerMes(12) & " " & Year(RowPuntuacion("FECACT")) - 1
                        Else
                            FechaPuntuacion = ObtenerMes(Month(RowPuntuacion("FECACT")) - 1) & " " & Year(RowPuntuacion("FECACT"))
                        End If
                    End If
                End If
            Next
        Next
    End Sub
    Protected ReadOnly Property FiltroVarCal() As Collection
        Get
            If _FiltroVarCal Is Nothing Then
                _FiltroVarCal = Session("CFiltroVarCal")
                If _FiltroVarCal Is Nothing Then
                    Response.Redirect("panelCalidadFiltro.aspx")
                End If
            End If
            Return _FiltroVarCal
        End Get
    End Property
    Function ObtenerMes(ByVal iMes As Integer) As String
        Dim IndiceMes As Integer = 11
        IndiceMes = IndiceMes + iMes
        ObtenerMes = Textos(IndiceMes)
    End Function
    ''' <summary>
    ''' Propiedad que contiene la fecha de actualización de las puntuaciones y la almacena en el ViewState.
    ''' </summary>
    ''' <value></value>
    ''' <returns>String con la fecha</returns>
    ''' <remarks></remarks>
    Protected Property FechaPuntuacion() As String
        Get
            Return ViewState("FechaPuntuacion")
        End Get
        Set(ByVal value As String)
            ViewState("FechaPuntuacion") = value
        End Set
    End Property
    Protected ReadOnly Property ProveMatQA() As String
        Get
            If _ProveMatQA Is Nothing Then
                _ProveMatQA = Request.Form(Request.QueryString("h") & "hProveMatQA")

                If _ProveMatQA Is Nothing Then
                    _ProveMatQA = Session("ProveMatQA")
                Else
                    Session("ProveMatQA") = _ProveMatQA
                End If

                If _ProveMatQA Is Nothing Then
                    Response.Redirect("panelCalidadFiltro.aspx")
                End If
            End If
            Return _ProveMatQA
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que contiene la denominación de la unidad de la ficha de calidad del proveedor a mostrar.
    ''' </summary>
    ''' <value></value>
    ''' <returns>denominación de la unidad</returns>
    ''' <remarks>Llamada desde: MostrarPanelPpmCargoTasa; Tiempo maximo:0</remarks>
    Protected ReadOnly Property DenUnidad() As String
        Get
            If _DenUnidad Is Nothing Then


                For Each UNQA As Collection In FiltroUNQA
                    If _Unidad = UNQA("ID") Then
                        _DenUnidad = UNQA("DEN")
                        Exit For
                    End If
                Next
            End If

            Return _DenUnidad
        End Get
    End Property
    ''' <summary>
    ''' Carga los textos de la página e inicializa los controles
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    ''' <remarks>Llamada desde: Se produce cuando el control de servidor se carga en el objeto Page; Tiempo maximo:0</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dsDatos As DataSet
        Dim PMUnidadesQA As FSNServer.UnidadesNeg = FSNServer.Get_Object(GetType(FSNServer.UnidadesNeg))
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.PopupFichaProveedor

        _Unidad = Request("Key")
        _Subtipo = Request("Subtipo")
        _Variable = Request("Variable")
        _Nivel = Request("Nivel")
        _ProveCod = Request("codProv")
        _ProveDen = Request("DenProv")

        If Request("desdeGS") = 1 Then
            Dim oDetalle As FSNServer.Puntuacion = FSNServer.Get_Object(GetType(FSNServer.Puntuacion))
            dsDatos = oDetalle.DetallePuntuacion(_Variable, _Nivel, Me.Idioma, _ProveCod, _Unidad, _Subtipo)
            Session("CFiltroUNQA") = ObtenerUNQA(dsDatos)
            Session("CFiltroVarCal") = ObtenerVarCal(dsDatos)
        End If
        FiltroUNQA = Session("CFiltroUNQA")
        Dim sScript As String
        sScript = "var DivScrll='" + div_Label.ClientID + "'; var lblScrll='" & label2.ClientID & "';"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsScrollVirtual", sScript)

        MostrarPanelPpmCargoTasa()
    End Sub
    ''' <summary>
    ''' Datos de las variables de calidad para la ficha de calidad
    ''' </summary>
    ''' <param name="dsDatosUNQA">Datos de las variables de calidad</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerVarCal(ByVal dsDatosUNQA As DataSet) As Collection
        Dim cfiltroVarCal As New Collection
        Dim cfiltro As New Collection()
        With dsDatosUNQA.Tables(4)
            cfiltro.Add(_Variable, "ID")
            cfiltro.Add(_Nivel, "NIVEL")
            cfiltro.Add(.Rows(0)("DEN"), "DEN")
            cfiltro.Add(True, "FILTRO")
            cfiltro.Add("TODOS", "MATQA")
            cfiltroVarCal.Add(cfiltro, _Variable)
        End With

        Return cfiltroVarCal
    End Function
    ''' <summary>
    ''' Datos de las unqa para la ficha de calidad
    ''' </summary>
    ''' <param name="dsDatosUNQA">Datos de las unqa</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerUNQA(ByVal dsDatosUNQA As DataSet) As Collection
        Dim cfiltroUNQA As New Collection
        Dim cfiltro As New Collection()
        If Not dsDatosUNQA.Tables(3).Rows.Count = 0 Then
            With dsDatosUNQA.Tables(3)
                cfiltro.Add(_Unidad, "ID")
                cfiltro.Add(_Nivel, "NIVEL")
                cfiltro.Add(.Rows(0)("DEN"), "DEN")
                cfiltroUNQA.Add(cfiltro, _Variable)
            End With
        End If
        'ObtenerFiltroUNQAHijos(cfiltroUNQA, dsDatosUNQA.Tables(0), dsDatosUNQA.Tables(1).Rows(0)("ID"), 1)
        Return cfiltroUNQA
    End Function
    '''Revisado por: Jbg. Fecha: 23/11/2011
    ''' <summary>
    ''' Carga la grid nada mas entrar
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde:Evento que salta despues de cargarse la pagina; Tiempo máximo=0,8 (Depende de los datos)</remarks>
    Private Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If _VarOpcionConf = VarCalOpcionConf.EvaluarFormula Then
            whdgDatos.Rows.Clear()
            whdgDatos.Columns.Clear()
            whdgDatos.GridView.Columns.Clear()
            If Not IsPostBack Then
                Dim oPPms As FSNServer.Ppms = FSNServer.Get_Object(GetType(FSNServer.Ppms))
                Dim dsDatos As DataSet = oPPms.DatosPuntuacionPPM(_Subtipo, _ProveCod, _MaterialQa, _Unidad, _Periodo)

                InsertarEnCache("dtDatos_" & FSNUser.Cod, dsDatos)

                If _Subtipo = CalidadSubtipo.CalCargoProveedores Then
                    If dsDatos.Tables(1).Rows.Count > 0 Then
                        m_sImpReper = FSNLibrary.FormatNumber(dsDatos.Tables(1).Rows(0)("REPERCUTIDO"), FSNUser.NumberFormat)
                        m_sImpFact = FSNLibrary.FormatNumber(dsDatos.Tables(1).Rows(0)("FACTURADO"), FSNUser.NumberFormat)
                    End If

                    m_sMoneda = dsDatos.Tables(2).Rows(0)("MONCEN")
                End If

                whdgDatos.DataSource = dsDatos
            Else
                whdgDatos.DataSource = CType(Cache("dtDatos_" & FSNUser.Cod), DataSet)
            End If
            CrearColumnas()
            ConfigurarGrid()
            whdgDatos.DataMember = "Table"
            whdgDatos.DataBind()
        Else
            whdgDatos.Style.Item(HtmlTextWriterStyle.Display) = "none"
        End If
    End Sub
#Region "Fns ModalPopup"
    '''Revisado por: Jbg. Fecha: 23/11/2011
    ''' <summary>
    ''' Muestra el modal popup de PPM ó de Cargos ó de Tasas
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo ejecucion:0,3seg.</remarks>
    Private Sub MostrarPanelPpmCargoTasa()

        Me.lblVarCodDen.Text = dtDatosVC.Item("VC_DEN")
        Me.lblDetalle.Text = Textos(0) 'Detalle de puntuación
        Me.lblImprimir.InnerText = Textos(16)

        Dim oDetalle As FSNServer.Puntuacion = FSNServer.Get_Object(GetType(FSNServer.Puntuacion))
        Dim ds As DataSet = oDetalle.DetallePuntuacion(_Variable, _Nivel, Me.Idioma, _ProveCod, _Unidad, _Subtipo)

        Dim fila As New HtmlTableRow
        Dim celda As New HtmlTableCell
        Dim mi_label As New Label

        Dim TextAux As String

        '''''''''''''
        'Detalle de puntuación del proveedor
        Me.lblDetPuntProve.Text = Textos(5)

        Me.lblDetPuntProveDat.Text = _ProveCod & " - " & _ProveDen
        'Fin Detalle de puntuación del proveedor

        '''''''''''''
        'en la unidad de negocio
        Me.lblEnUni.Text = Textos(6)

        Me.lblEnUniDat.Text = DenUnidad
        'Fin en la unidad de negocio

        '''''''''''''
        'Puntuacion (Calificacion):
        Me.lblPunt.Text = Textos(35)

        Me.lblPuntDat.Text = modUtilidades.FormatNumber(_Puntos, FSNUser.NumberFormat) & " (" & _Calif & ")"
        'Fin Puntuacion:

        '''''''''''''
        'Lista Padres
        fila = New HtmlTableRow

        celda = New HtmlTableCell

        mi_label = New Label()
        mi_label.CssClass = "RotuloDetallePuntuacionFondoGrisBold"
        mi_label.Text = Textos(1) 'Variable:

        celda.Controls.Add(mi_label) 'etiqueta Variable

        For i As Integer = 1 To _Nivel
            mi_label = New Label()

            TextAux = "/"
            If i = _Nivel Then TextAux = ""

            mi_label.CssClass = "fp_celda_vc_" & CStr(i) & "_DetallePuntuacion"
            If i < _Nivel Then
                mi_label.Text = ds.Tables(0).Rows(0).Item("VAR" & CStr(i)) & TextAux
            Else
                mi_label.Text = Me.lblVarCodDen.Text
            End If

            celda.Controls.Add(mi_label) 'Padre de nivel i
        Next

        fila.Controls.Add(celda)
        Me.tblPadre.Controls.Add(fila)
        'Fin Lista Padres

        '''''''''''''
        'Material Qa:
        Me.lblMatQa.Text = Textos(2)

        Me.lblMatQaDat.Text = ""

        If DBNullToStr(ds.Tables(0).Rows(0).Item("MAT")) <> "" Then
            Me.lblMatQaDat.Text = ds.Tables(0).Rows(0).Item("MAT")
        End If
        'Fin Material Qa

        '''''''''''''
        'Periodo:
        Me.lblPeriodo.Text = Textos(3) 'Periodo:

        Me.lblPeriodoDat.Text = ds.Tables(0).Rows(0).Item("PERIODO") & " " & Textos(4) 'meses
        'Fin Periodo:

        '''''''''''''
        'Formula:
        Me.lblFormula.Text = Textos(36) 'Formula
        _VarOpcionConf = ds.Tables(0).Rows(0)("OPCION_CONF")
        If _VarOpcionConf = VarCalOpcionConf.MediaPonderadaSegunVarHermana Then
            lblFormulaTexto.Text = Textos(52).Replace("@VARCAL", ds.Tables(0).Rows(0)("DEN_VAR_POND"))
        Else
            Me.lblFormulaTexto.Text = ds.Tables(0).Rows(0).Item("FORMULA")
        End If
        'Fin Formula:

        ''''''''''''''
        'GrupoX1..4
        ''''''''''''''
        If _VarOpcionConf = VarCalOpcionConf.EvaluarFormula Then
            For Each RowBD As DataRow In ds.Tables(1).Rows
                fila = New HtmlTableRow
                fila.Height = "25px"

                celda = New HtmlTableCell
                celda.Style.Add("width", "60px")
                celda.Attributes.Add("class", "FondoGrisBorderTablaDetallePuntuacionCeldaGris")

                mi_label = New Label()
                mi_label.CssClass = "RotuloDetallePuntuacionFondoGrisBoldNoPad"
                mi_label.Text = RowBD.Item("VARIABLEX") 'Xi

                celda.Controls.Add(mi_label) 'etiqueta Xi

                fila.Controls.Add(celda)
                '''''
                celda = New HtmlTableCell
                celda.Style.Add("width", "180px")
                celda.Attributes.Add("class", "FondoGrisBorderTablaDetallePuntuacionCeldaGrisCentral")

                mi_label = New Label()
                mi_label.CssClass = "RotuloDetallePuntuacionFondoGrisBoldNoPad"
                mi_label.Text = DameTrozo(RowBD.Item("VARIABLEX"), DBNullToStr(ds.Tables(0).Rows(0).Item("DEN"))) 'Suelo ó Objetivo ó ...

                celda.Controls.Add(mi_label) 'Suelo ó Objetivo ó ...

                fila.Controls.Add(celda)
                ''''''
                celda = New HtmlTableCell
                celda.Style.Add("width", "100px")
                celda.Attributes.Add("class", "FondoGrisBorderTablaDetallePuntuacionCeldaWhite")

                mi_label = New Label()
                mi_label.CssClass = "RotuloDetallePuntuacionFondoGrisBoldNoPad"
                mi_label.Text = DBNullToDbl(RowBD.Item("VALORX")) 'Valor

                celda.Controls.Add(mi_label) 'Valor

                fila.Controls.Add(celda)

                Me.tblVarsX.Controls.Add(fila)
            Next
        End If
        '''''''''''''''''''''''
        _MaterialQa = DBNullToStr(ds.Tables(0).Rows(0).Item("MATERIAL_QA"))
        _Periodo = ds.Tables(0).Rows(0).Item("PERIODO")
        '''''''''''''''''''''''
        If _VarOpcionConf = VarCalOpcionConf.EvaluarFormula Then
            If _Subtipo = CalidadSubtipo.CalCargoProveedores Then
                For Each RowBD As DataRow In ds.Tables(1).Rows
                    If UCase(RowBD.Item("VARIABLEX")) = "X1" Then
                        m_bCargoX1 = True
                    Else 'If UCase(RowBD.Item("VARIABLEX")) = "X2" Then
                        m_bCargoX2 = True
                    End If
                Next
            End If
        End If
    End Sub

    ''' <summary>
    ''' Saca de entre la Leyenda de las variables implicadas en una formula la descripción de la variable indicada
    ''' </summary>
    ''' <param name="Variable">Variable de la q sacar su descripción</param>
    ''' <param name="DenomVariable">Leyenda de las variables implicadas en una formula</param>
    ''' <returns>descripción de la variable indicada</returns>
    ''' <remarks>Llamada desde: MostrarPanelPpmCargoTasa; Tiempo máximo:0</remarks>
    Private Function DameTrozo(ByVal Variable As String, ByVal DenomVariable As String) As String
        'X1: Open non-conformities;X2: Non-conformities closed within deadline;X3: Non-conformities closed out of deadline;X4: Non-conformities with a negative closure
        Dim Aux As String = ""
        If InStr(DenomVariable, Variable & ":") > 0 Then
            Aux = DenomVariable.Substring(InStr(DenomVariable, Variable & ":") - 1)
        Else
            Return ""
        End If
        'X3: Non-conformities closed out of deadline;X4: Non-conformities with a negative closure
        If InStr(Aux, ";") = 0 Then
            DameTrozo = Aux.Substring(4)
        Else
            DameTrozo = Aux.Substring(4, InStr(Aux, ";") - 5)
        End If
    End Function
#End Region
#Region "Grid Datos"
    Private Sub CrearColumnas()
        Dim campoGrid As BoundDataField
        Dim nombre As String

        For i As Integer = 0 To whdgDatos.DataSource.Tables(0).Columns.Count - 1
            campoGrid = New BoundDataField(True)
            nombre = whdgDatos.DataSource.Tables(0).Columns(i).ToString           
            With campoGrid
                .Key = nombre
                .DataFieldName = nombre
                .Header.Text = nombre
            End With
            whdgDatos.Columns.Add(campoGrid)
            whdgDatos.GridView.Columns.Add(campoGrid)
        Next
    End Sub
    Private Sub ConfigurarGrid()
        ''Antiguo InitializeLayout''
        '' Configura la grid que contiene los datos usados en la puntuación
        '' Oculta las columnas no están visibles pq no se corresponden con lo q se esta visualizando (_Subtipo 5:PPM 5:Cargos 7:Tasas)
        '' y configura los estilos, textos, ... de las cabeceras de las columnas
        whdgDatos.Columns("FECHA").Header.Text = Textos(14) 'Fecha
        whdgDatos.Columns("FECHA").Header.CssClass = "fp_celda_cabecera_vcDetalle"
        whdgDatos.Columns("FECHA").Width = Unit.Pixel(80)
        whdgDatos.Columns("FECHA").FormatValue(FSNUser.DateFormat.ShortDatePattern)
        whdgDatos.Columns("FECHA").CssClass = "RotuloDetallePuntuacionLeftGrid"
        whdgDatos.Columns("ARTICULO").Header.Text = Textos(15) 'Articulo
        whdgDatos.Columns("ARTICULO").Header.CssClass = "fp_celda_cabecera_vcDetalle"
        whdgDatos.Columns("ARTICULO").CssClass = "RotuloDetallePuntuacionLeftGrid"
        'footer
        If whdgDatos.Rows.Count > 0 Then
            whdgDatos.ShowFooter = True
            whdgDatos.Columns("FECHA").Footer.Text = Textos(13)
            whdgDatos.Columns("FECHA").Footer.CssClass = "RotuloDetallePuntuacionRightGridBold"
            whdgDatos.Columns("ARTICULO").Footer.CssClass = "RotuloDetallePuntuacionLeftGridBorderL"
        End If
        Select Case _Subtipo
            Case 5
                whdgDatos.Columns("PIEZAS_SERVIDAS").Header.Text = Textos(7) 'Piezas servidas 
                whdgDatos.Columns("PIEZAS_SERVIDAS").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                whdgDatos.Columns("PIEZAS_SERVIDAS").Width = Unit.Pixel(130)
                whdgDatos.Columns("PIEZAS_SERVIDAS").CssClass = "RotuloDetallePuntuacionRightGrid"

                whdgDatos.Columns("PIEZAS_DEFECTUOSAS").Header.Text = Textos(8) 'Piezas defectuosas
                whdgDatos.Columns("PIEZAS_DEFECTUOSAS").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                whdgDatos.Columns("PIEZAS_DEFECTUOSAS").Width = Unit.Pixel(130)
                whdgDatos.Columns("PIEZAS_DEFECTUOSAS").CssClass = "RotuloDetallePuntuacionLeftRightGrid"

                'Footer
                If whdgDatos.Rows.Count > 0 Then
                    whdgDatos.Columns("PIEZAS_SERVIDAS").Footer.CssClass = "RotuloDetallePuntuacionRightGridBold"
                    whdgDatos.Columns("PIEZAS_DEFECTUOSAS").Footer.CssClass = "RotuloDetallePuntuacionLeftRightGridBold"
                End If
            Case 6
                If m_bCargoX1 AndAlso m_bCargoX2 Then
                    whdgDatos.Columns("FECHA").Width = Unit.Pixel(78)
                    whdgDatos.Columns("IMPORTE_REPERCUTIDO").Width = Unit.Pixel(112)
                    whdgDatos.Columns("IMPORTE_FACTURADO").Width = Unit.Pixel(108)
                ElseIf m_bCargoX1 Then
                    whdgDatos.Columns("IMPORTE_REPERCUTIDO").Width = Unit.Pixel(215)
                Else
                    whdgDatos.Columns("IMPORTE_FACTURADO").Width = Unit.Pixel(215)
                End If

                If Not m_bCargoX1 Then whdgDatos.Columns("IMPORTE_REPERCUTIDO").Hidden = True

                If Not m_bCargoX2 Then whdgDatos.Columns("IMPORTE_FACTURADO").Hidden = True

                whdgDatos.Columns("IMPORTE_REPERCUTIDO").Header.Text = Textos(9)
                whdgDatos.Columns("IMPORTE_REPERCUTIDO").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                whdgDatos.Columns("IMPORTE_REPERCUTIDO").CssClass = "RotuloDetallePuntuacionRightGrid"
                whdgDatos.Columns("IMPORTE_FACTURADO").Header.Text = Textos(10)
                whdgDatos.Columns("IMPORTE_FACTURADO").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                whdgDatos.Columns("IMPORTE_FACTURADO").CssClass = "RotuloDetallePuntuacionLeftRightGrid"
                whdgDatos.Columns("MON").Header.Text = Textos(26)
                whdgDatos.Columns("MON").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                whdgDatos.Columns("MON").Width = Unit.Pixel(45)
                whdgDatos.Columns("MON").CssClass = "RotuloDetallePuntuacionLeftGrid"

                'Footer
                whdgDatos.Columns("IMPORTE_REPERCUTIDO").Footer.Text = m_sImpReper
                whdgDatos.Columns("IMPORTE_FACTURADO").Footer.Text = m_sImpFact
                whdgDatos.Columns("MON").Footer.Text = m_sMoneda
                whdgDatos.Columns("IMPORTE_REPERCUTIDO").Footer.CssClass = "RotuloDetallePuntuacionRightGridBold"
                whdgDatos.Columns("IMPORTE_FACTURADO").Footer.CssClass = "RotuloDetallePuntuacionLeftRightGridBold"
                whdgDatos.Columns("MON").Footer.CssClass = "RotuloDetallePuntuacionLeftRightGridBold"
            Case 7
                whdgDatos.Columns("ENVIOS_CORRECTOS").Header.Text = Textos(11)
                whdgDatos.Columns("ENVIOS_CORRECTOS").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                whdgDatos.Columns("ENVIOS_CORRECTOS").Width = Unit.Pixel(130)
                whdgDatos.Columns("ENVIOS_CORRECTOS").CssClass = "RotuloDetallePuntuacionRightGrid" '"RotuloDetallePuntuacionNoLeft"
                whdgDatos.Columns("TOTAL_ENVIOS").Header.Text = Textos(12)
                whdgDatos.Columns("TOTAL_ENVIOS").Header.CssClass = "fp_celda_cabecera_vcDetalle"
                whdgDatos.Columns("TOTAL_ENVIOS").Width = Unit.Pixel(130)
                whdgDatos.Columns("TOTAL_ENVIOS").CssClass = "RotuloDetallePuntuacionLeftRightGrid"

                'Footer
                If whdgDatos.Rows.Count > 0 Then
                    whdgDatos.Columns("ENVIOS_CORRECTOS").Footer.CssClass = "RotuloDetallePuntuacionRightGridBold"
                    whdgDatos.Columns("TOTAL_ENVIOS").Footer.CssClass = "RotuloDetallePuntuacionLeftRightGridBold"
                End If
        End Select

    End Sub
    ''' <summary>
    ''' Para calcular los totales
    ''' </summary>
    ''' <param name="sender">grid</param>
    ''' <param name="e">evento DataBound</param>
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:</remarks>
    Private Sub whdgDatos_DataBound(sender As Object, e As System.EventArgs) Handles whdgDatos.DataBound
        Dim total As Integer
        Dim hayCambio As Boolean = False
        For i = 0 To whdgDatos.Columns.Count - 1
            hayCambio = False
            total = 0
            For Each row As ContainerGridRecord In whdgDatos.GridView.Rows
                If IsNumeric(row.Items(i).Text) Then
                    total += Convert.ToDouble(row.Items(i).Text)
                    hayCambio = True
                End If
            Next
            If hayCambio Then
                whdgDatos.GridView.Columns(i).Footer.Text = total.ToString
            End If
        Next
    End Sub

    Private Sub whdgDatos_InitializeRow(sender As Object, e As RowEventArgs) Handles whdgDatos.InitializeRow
        If Not e.Row.Items.FindItemByKey("IMPORTE_REPERCUTIDO") Is Nothing AndAlso Not IsDBNull(e.Row.Items.FindItemByKey("IMPORTE_REPERCUTIDO").Value) Then
            e.Row.Items.FindItemByKey("IMPORTE_REPERCUTIDO").Text = FSNLibrary.FormatNumber(e.Row.Items.FindItemByKey("IMPORTE_REPERCUTIDO").Value, FSNUser.NumberFormat)
        End If
        If Not e.Row.Items.FindItemByKey("IMPORTE_FACTURADO") Is Nothing AndAlso Not IsDBNull(e.Row.Items.FindItemByKey("IMPORTE_FACTURADO").Value) Then
            e.Row.Items.FindItemByKey("IMPORTE_FACTURADO").Text = FSNLibrary.FormatNumber(e.Row.Items.FindItemByKey("IMPORTE_FACTURADO").Value, FSNUser.NumberFormat)
        End If
    End Sub
#End Region
End Class