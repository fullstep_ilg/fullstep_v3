﻿
Imports System
Imports System.Text
Imports System.Web.UI
Imports System.Web.UI.WebControls
Public Class RequiredFieldValidatorForCheckBoxLists
    Inherits BaseValidator
    Private Const SCRIPTBLOCK As String = "RFV4CL"
    Protected Overloads Overrides Function ControlPropertiesValid() As Boolean
        Dim ctrl As Control = FindControl(ControlToValidate)
        If ctrl IsNot Nothing Then
            Dim _listctrl As CheckBoxList = DirectCast(ctrl, CheckBoxList)
            Return (_listctrl IsNot Nothing)
        Else
            Return False
        End If
    End Function

    Protected Overloads Overrides Function EvaluateIsValid() As Boolean
        Return EvaluateIsChecked()
    End Function

    Protected Overloads Overrides Sub OnPreRender(ByVal e As EventArgs)
        MyBase.OnPreRender(e)
        If EnableClientScript Then
            Me.ClientScript()

        End If
    End Sub

    Private Sub ClientScript()
        Dim sb_Script As New StringBuilder()
        sb_Script.Append("<script language=""javascript"">")
        sb_Script.Append(vbCr)
        sb_Script.Append(vbCr)
        sb_Script.Append("function cb_verify(sender) {")
        sb_Script.Append(vbCr)
        sb_Script.Append("var val = document.getElementById(document.getElementById(sender.id).controltovalidate);")
        sb_Script.Append(vbCr)
        sb_Script.Append("var col = val.getElementsByTagName(""*"");")
        sb_Script.Append(vbCr)
        sb_Script.Append("if ( col != null ) {")
        sb_Script.Append(vbCr)
        sb_Script.Append("for ( i = 0; i < col.length; i++ ) {")
        sb_Script.Append(vbCr)
        sb_Script.Append("if (col.item(i).tagName == ""INPUT"") {")
        sb_Script.Append(vbCr)
        sb_Script.Append("if ( col.item(i).checked ) {")
        sb_Script.Append(vbCr)
        sb_Script.Append(vbCr)
        sb_Script.Append("return true;")
        sb_Script.Append(vbCr)
        sb_Script.Append("}")
        sb_Script.Append(vbCr)
        sb_Script.Append("}")
        sb_Script.Append(vbCr)
        sb_Script.Append("}")
        sb_Script.Append(vbCr)
        sb_Script.Append(vbCr)
        sb_Script.Append(vbCr)
        sb_Script.Append("return false;")
        sb_Script.Append(vbCr)
        sb_Script.Append("}")
        sb_Script.Append(vbCr)
        sb_Script.Append("}")
        sb_Script.Append(vbCr)
        sb_Script.Append("</script>")
        Page.ClientScript.RegisterClientScriptBlock([GetType](), SCRIPTBLOCK, sb_Script.ToString())
        Page.ClientScript.RegisterExpandoAttribute(ClientID, "evaluationfunction", "cb_verify")
    End Sub


    Private Function EvaluateIsChecked() As Boolean
        Dim _cbl As CheckBoxList = DirectCast(FindControl(ControlToValidate), CheckBoxList)
        For Each li As ListItem In _cbl.Items
            If li.Selected Then
                Return True
            End If
        Next
        Return False
    End Function
End Class

