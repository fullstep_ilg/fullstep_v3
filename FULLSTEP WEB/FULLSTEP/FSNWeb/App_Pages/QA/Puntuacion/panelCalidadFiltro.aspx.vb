﻿Imports Fullstep
Imports Fullstep.FSNServer
Imports System.Web.Script.Serialization

Partial Public Class panelCalidadFiltro
    Inherits FSNPage
    Private Const TODOS_MATQA As String = "TODOS"
    Private Const MARCA_TODOS As String = "HIJOS"
    Private UNQASProveedores As New Dictionary(Of Integer, String)
    Private _DUNQASProve As Dictionary(Of Integer, String)
    Private oparametros As Parametros    

    Protected Property DUNQASProve() As Dictionary(Of Integer, String)
        Get
            If _DUNQASProve Is Nothing Then
                _DUNQASProve = ViewState("DUNQASProve")
            End If
            Return _DUNQASProve
        End Get
        Set(ByVal value As Dictionary(Of Integer, String))
            ViewState("DUNQASProve") = value
        End Set
    End Property

    ''' <summary>
    ''' Evento de carga de página, se establece el modulo de idioma si el usurio tiene modo automatico que activa
    ''' el AsyncPostBackControl en la lista de check de materiales
    ''' y si no es postback se cargan los modulos de los filtros
    ''' </summary>
    ''' <param name="sender">Page</param>
    ''' <param name="e">EventArgs</param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.PuntProveedores
        'si el usuario tiene modo automatico activamos el chequeo automatico de variables en funcion de los materiales chequeados
        cblMaterialesQA.AutoPostBack = "true"
        ScriptMgr.RegisterAsyncPostBackControl(cblMaterialesQA)
        If Acceso.gbPotAValProvisionalSelProve Then CrearComboTiposProveedores()
        If Not Me.IsPostBack Then
            CargarFiltros()
            Seccion = "Panel de Proveedores"
            Dim mMaster = CType(Me.Master, FSNWeb.Menu)
            mMaster.Seleccionar("Calidad", "PanelProveedores")
            btnCargar.Attributes("onclick") = "return ValidarFiltro();"

            oParametros = FSNServer.Get_Object(GetType(FSNServer.Parametros))
            oparametros.LoadData()
            HttpContext.Current.Session("tipofiltromaterial") = oparametros.FiltroMaterialQA
        Else
            CargarTextosJScript()
        End If

        Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "tipofiltro", "<script>var tipofiltro=" & HttpContext.Current.Session("tipofiltromaterial") & "</script>")

    End Sub
#Region "Filtros"
    ''' <summary>
    ''' Carga los filtros del panel de calidad
    ''' </summary>
    ''' <remarks></remarks>    
    Sub CargarFiltros()
        lblTitulo.Text = Textos(51)
        CargarFiltrosGuardados()
        CargarModuloMaterialesQA()
        CargarModuloUNQA()
        CargarModuloVarCal()
        CargarModuloProveedores()
        CargarModuloPuntuacion()
        Me.btnCargar.Text = Textos(17)
        CargarTextosJScript()
    End Sub

    ''' <summary>Carga el combo de tipos de proveedor</summary>
    ''' <remarks>Llamada desde: CargarFiltros</remarks>
    ''' 
    Private Sub CrearComboTiposProveedores()
        Dim combo As New Infragistics.Web.UI.ListControls.WebDropDown

        combo.ID = "wddTipoProveedor"
        combo.EnableClosingDropDownOnSelect = True
        combo.DropDownContainerWidth = Unit.Pixel(150)
        combo.EnableDropDownAsChild = False
        combo.Style.Add("border", "0px")

        combo.Items.Add(New Infragistics.Web.UI.ListControls.DropDownItem(Textos(83), 0))  'Todos
        combo.Items.Add(New Infragistics.Web.UI.ListControls.DropDownItem(Textos(84), 1))  'Provisionales
        combo.Items.Add(New Infragistics.Web.UI.ListControls.DropDownItem(Textos(85), 2))  'No provisionales

        divTipoProveedor.Controls.Add(combo)
    End Sub

    ''' Revisado por: Jbg. Fecha: 20/10/2011
    ''' <summary>
    ''' Si es la primera visita al panel en la Session, carga la configuración del panel que el usuario tenga guardada
    ''' </summary>
    ''' <remarks>Llamada desde: CargarFiltros ; Tiempo maximo: 0,2</remarks>
    Sub CargarFiltrosGuardados()
        If Session("FiltroUNQA") = "" AndAlso Session("FiltroVarCal") = "" Then
            FSNUser.Cargar_ConfiguracionPanelCalidad(FSNUser.Cod)
            Session("FiltroUNQA") = FSNUser.QAFiltroUNQA
            Session("FiltroVarCal") = FSNUser.QAFiltroVarCal
            Session("CamposOcultosPC") = FSNUser.QACamposOcultos
            Session("OrdenacionPanel") = FSNUser.QAOrdenacion
        End If
    End Sub
    ''' <summary>
    ''' Cargar los textos de los Alerts
    ''' </summary>
    ''' <remarks>Llamada desde: CargarFiltros    Page_Load ; Tiempo maximo: 0,2</remarks>
    Private Sub CargarTextosJScript()
        Dim sVariableJavascriptTextosJScript As String = "var TextosJScript = new Array();"
        sVariableJavascriptTextosJScript &= "TextosJScript[31]='" & JSText(Textos(31)) & "';"
        sVariableJavascriptTextosJScript &= "TextosJScript[80]='" & JSText(Textos(80)) & "';"
        sVariableJavascriptTextosJScript &= "TextosJScript[81]='" & JSText(Textos(81)) & "';"
        sVariableJavascriptTextosJScript &= "TextosJScript[82]='" & JSText(Textos(82)) & "';"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosScript", sVariableJavascriptTextosJScript, True)
    End Sub
#End Region
#Region "Materiales QA"
    ''' <summary>
    ''' Pone el título al módulo y carga la lista de checks con los materiales de QA a través de un ObjectDataSource
    ''' </summary>
    ''' <remarks></remarks>
    Sub CargarModuloMaterialesQA()
        Me.lblTituloMateriales.Text = UCase(Me.Textos(9))
        cblMaterialesQA.DataTextField = "DEN_" & Idioma
        cblMaterialesQA.DataValueField = "ID"
        Dim parametros As ParameterCollection = obsMaterialesQA.SelectParameters
        parametros("sIdi").DefaultValue = Idioma
        parametros("bSinCertificados").DefaultValue = True
        parametros("lPyme").DefaultValue = Me.Usuario.Pyme
        parametros("bResMatCom").DefaultValue = Me.Usuario.QARestProvMaterial
        parametros("com").DefaultValue = Me.Usuario.CodPersona

    End Sub
    ''' <summary>
    ''' Chequeamos los materiales de la ultima busqueda
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cblMaterialesQA_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cblMaterialesQA.DataBound
        If Session("FiltroMaterialesQA") <> "" Then
            Dim sMaterialesQA As String = "," & Session("FiltroMaterialesQA") & ","
            For Each liItem As ListItem In cblMaterialesQA.Items
                If sMaterialesQA.IndexOf("," & liItem.Value & ",") >= 0 Then
                    liItem.Selected = True
                End If
            Next
        End If

    End Sub
    ''' <summary>
    ''' Asignar al obsMaterialesQA la instancia de la clase MaterialesQA generada por FSPMServer
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub obsMaterialesQA_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles obsMaterialesQA.ObjectCreating
        Dim PMMaterialesQA As FSNServer.MaterialesQA = FSNServer.Get_Object(GetType(FSNServer.MaterialesQA))
        e.ObjectInstance = PMMaterialesQA
    End Sub
    ''' <summary>Función que devuleve una colección con los materiales de QA chequeados y carga una variable de Session con los materiales</summary>
    ''' <returns>Colección con la lista de materiales de QA chequeados representados por colecciones de ID DEN</returns>
    ''' <remarks>btnCargar_Click</remarks>
    Function FiltroMaterialesQA() As Collection
        Dim sfiltroMaterialesQA As String = ""
        Dim cfiltroMaterialesQA As New Collection
        
        'Esto de momento lo vamos a mantener, únicamente para el filtroQA básico
        For Each liItem As ListItem In cblMaterialesQA.Items
            If liItem.Selected Then
                sfiltroMaterialesQA = IIf(sfiltroMaterialesQA = "", "", sfiltroMaterialesQA & ",") & liItem.Value
                Dim filtro As New Collection()
                filtro.Add(liItem.Value, "ID")
                filtro.Add(liItem.Text, "DEN")
                cfiltroMaterialesQA.Add(filtro)
            End If
        Next
        Session("FiltroMaterialesQA") = sfiltroMaterialesQA
        Session("dtFiltroMaterialesQA") = HttpContext.Current.Session("materialesseleccionados")
        Return cfiltroMaterialesQA
    End Function
#End Region
#Region "Unidades de Negocio"
    ''' <summary>
    ''' Pone el título al módulo y cargar el treeview con las unidades de negocio de QA, mostrando un check en las que el usuario tenga permiso
    ''' </summary>
    ''' <remarks></remarks>
    Sub CargarModuloUNQA()
        Me.lblTituloUNQA.Text = UCase(Me.Textos(34))
        Dim PMUnidadesQA As FSNServer.UnidadesNeg = FSNServer.Get_Object(GetType(FSNServer.UnidadesNeg))
        Dim dsUNQA As DataSet = PMUnidadesQA.UsuGetData_hds(FSNUser.Cod, TipoAccesoUNQAS.ConsultarPuntuacionesPanelCalidad, TipoAccesoUNQAS.ModificarPuntuacionesPanelCalidad, Me.Idioma, FSNUser.Pyme)
        tvUNQA.DataSource = New HierarchicalDataSet(dsUNQA.Tables(0).DefaultView, "ID", "PADRE")
        tvUNQA.DataBind()
        tvUNQA.ExpandAll()
        If Session("FiltroUNQA") <> "" Then
            ConfigurarTreeViewUNQA(tvUNQA.Nodes, "," & Session("FiltroUNQA") & ",")
        Else
            ConfigurarTreeViewUNQA(tvUNQA.Nodes)
        End If

        DUNQASProve = UNQASProveedores

        Dim HijosConAcceso As Boolean = False
        EliminarUNQASinAcceso(tvUNQA.Nodes, tvUNQA.Nodes.Count, HijosConAcceso)
        If dsUNQA.Tables(1).Rows.Count > 0 Then
            Me.hfGrupoID.Value = dsUNQA.Tables(1).Rows(0).Item(0)
            Me.hfGrupoDEN.Value = dsUNQA.Tables(1).Rows(0).Item(1)
            Me.hfGrupoPERMISO.Value = dsUNQA.Tables(1).Rows(0).Item(2)
        ElseIf dsUNQA.Tables(0).Rows.Count = 0 Then
            btnCargar.Visible = False
        End If
    End Sub

    ''' <summary>
    ''' Procedimiento que recorre los nodos del treeview de unqas
    ''' Conf del treeview por defecto. Todos los nodos tienen check/Todos están contraidos
    ''' Si no tiene acceso a la unqa y a la unqa padre tampoco no mostramos check (Hemos cargado el campo ACCESO en la propiedad NavigateUrl)
    ''' Expandimos los nodos cunado tiene acceso al menos a una unqa.
    ''' </summary>
    ''' <param name="Nodos">Coleccion de Nodos del treeview de UNQAs</param>
    ''' <param name="sChequarNodos">strng con una lista de nodods separados por coma a chequear</param>
    ''' <remarks></remarks>
    Sub ConfigurarTreeViewUNQA(ByRef Nodos As TreeNodeCollection, Optional ByVal sChequarNodos As String = "")
        For Each Nodo As TreeNode In Nodos
            Dim bAcceso As Boolean = IIf(Nodo.NavigateUrl = "0", False, True)

            Dim bHijos As Boolean = IIf(Nodo.ChildNodes.Count = 0, False, True)

            If Not bAcceso Then
                Nodo.ShowCheckBox = False
            Else
                Dim UNQAProves As String = Nodo.Value
                For Each NodoHijo As TreeNode In Nodo.ChildNodes
                    UNQAProves = UNQAProves & "," & NodoHijo.Value

                    For Each NodoNieto As TreeNode In NodoHijo.ChildNodes
                        UNQAProves = UNQAProves & "," & NodoNieto.Value
                    Next
                Next

                If Not Nodo.Parent Is Nothing Then
                    UNQAProves = UNQAProves & "," & Nodo.Parent.Value

                    If Not Nodo.Parent.Parent Is Nothing Then
                        UNQAProves = UNQAProves & "," & Nodo.Parent.Parent.Value
                    End If
                End If

                UNQASProveedores.Add(Nodo.Value, UNQAProves)

            End If
            'chequeamos las unidades de negoico de la ultima busqueda
            If sChequarNodos <> "" Then
                If sChequarNodos.IndexOf("," & Nodo.Value & ",") >= 0 Then
                    Nodo.Checked = True
                End If
            End If

            'Recorremos los hijos
            If bHijos Then
                ConfigurarTreeViewUNQA(Nodo.ChildNodes, sChequarNodos)
            End If

        Next

    End Sub

    ''' <summary>
    ''' Función recursiva a la que se le pasa el arbol de nodos y elimina las ramas en las cuales
    ''' el usuario no tiene permiso de consulta/modificación de puntuaciones para ninguno de sus nodos
    ''' </summary>
    ''' <param name="Nodos">nodos del arbol</param>
    ''' <param name="CountNodes">número de nodos</param>
    ''' <param name="HijosConAcceso">si el nodo tiene algún hijo con permiso del usuario</param>
    ''' <remarks></remarks>
    ''' ilg 271009
    Sub EliminarUNQASinAcceso(ByRef Nodos As TreeNodeCollection, ByRef CountNodes As Integer, Optional ByRef HijosConAcceso As Boolean = False)
        Dim i As Integer = 0
        Do While i < CountNodes
            If Nodos(i).NavigateUrl = "0" AndAlso Nodos(i).ChildNodes.Count = 0 Then
                Nodos.Remove(Nodos(i))
                CountNodes = CountNodes - 1
            ElseIf Nodos(i).NavigateUrl = "0" AndAlso Nodos(i).ChildNodes.Count > 0 Then
                'Nodos(i).NavigateUrl = ""
                EliminarUNQASinAcceso(Nodos(i).ChildNodes, Nodos(i).ChildNodes.Count, HijosConAcceso)

                If Not HijosConAcceso Or Nodos(i).ChildNodes.Count = 0 Then
                    Nodos.Remove(Nodos(i))
                    CountNodes = CountNodes - 1
                Else
                    If Not Nodos(i).Parent Is Nothing Then
                        HijosConAcceso = True
                    Else
                        HijosConAcceso = False
                    End If
                    i = i + 1
                End If
            Else
                'Nodos(i).NavigateUrl = ""
                If Nodos(i).ChildNodes.Count > 0 Then
                    EliminarUNQASinAcceso(Nodos(i).ChildNodes, Nodos(i).ChildNodes.Count, HijosConAcceso)

                    If Not HijosConAcceso Then
                        Nodos(i).Collapse()
                    End If
                End If

                If Not Nodos(i).Parent Is Nothing Then
                    HijosConAcceso = True
                End If

                i = i + 1
            End If

        Loop

    End Sub

    ''' <summary>Función que devuelve una colección con las unidades de negocio en las que el usurio puede consultar la puntuación.
    ''' Con la propiedad FILTRO se indica si se ha chequeado o no en el filtro.
    ''' Además obtiene 3 variables de Session
    ''' Session("FiltroUNQA"): Cadena de unqas.id separadas por coma que han sido chequeadas. La utilizaremos para mantener estas unqas chequeadas en el filtro durante la session del usuario
    ''' Session("FiltroUNQAPunt"):Cadena de unqas.id separadas por coma de las que se va a obtener la puntación=Session("FiltroUNQA")+El grupo si tiene acceso</summary>
    ''' Session("FiltroUNQAProve"):Cadenas de unqas.id separadas por coma, sobre las que se van a obtener los proveedores a mostrar en el panel. Si se chequea una planta se incluye tb su división
    ''' <returns>String con la lista de unidades de negocio chequeadas separadas por coma, representados por su ID</returns>
    ''' <remarks>btnCargar_Click</remarks>
    Function FiltroUNQA() As Collection
        'cfiltroUNQA: Colección con las unidades de negocio en las que el usuario puede consultar la puntuación.
        'Con la propiedad FILTRO se indica si se ha chequeado o no en el filtro.
        Dim cfiltroUNQA As New Collection
        'Cadena de unqas chequeadas
        Dim sfiltroUNQA As String = ""
        'cadena de unqas sobre als que obtener proveedores
        Dim sfiltroUNQA_PROVE As String = ""
        'Si el usuario tiene acceso al grupo lo añadimos al filtro
        If hfGrupoID.Value <> "" Then
            Dim cfiltro As New Collection()
            cfiltro.Add(hfGrupoID.Value, "ID")
            cfiltro.Add(0, "NIVEL")
            cfiltro.Add(hfGrupoDEN.Value, "DEN")
            cfiltro.Add("", "HIJOS")
            cfiltro.Add(True, "FILTRO")
            cfiltro.Add(hfGrupoPERMISO.Value, "PERMISO")
            cfiltroUNQA.Add(cfiltro, hfGrupoID.Value)
        End If
        ObtenerNodosChequeados(hfGrupoID.Value, cfiltroUNQA, sfiltroUNQA, tvUNQA.Nodes, , sfiltroUNQA_PROVE)
        DUNQASProve = Nothing

        Session("FiltroUNQA") = sfiltroUNQA
        If hfGrupoID.Value <> "" Then
            Session("FiltroUNQAPunt") = hfGrupoID.Value & IIf(sfiltroUNQA = "", "", "," & sfiltroUNQA)
        Else
            Session("FiltroUNQAPunt") = sfiltroUNQA
        End If
        Session("FiltroUNQAProve") = sfiltroUNQA_PROVE
        Return cfiltroUNQA
    End Function

#End Region
#Region "Variables de Calidad"
    ''' <summary>
    ''' Pone el título al módulo y carga en el Treeview las variables de calidad a las que el usuario tiene acceso
    ''' </summary>
    ''' <remarks></remarks>
    Sub CargarModuloVarCal()
        Me.lblTituloVarCal.Text = UCase(Me.Textos(50))
        Dim PMVariablesCalidad As FSNServer.VariablesCalidad = FSNServer.Get_Object(GetType(FSNServer.VariablesCalidad))
        Dim dsVarCal As DataSet = PMVariablesCalidad.GetData_hds(FSNUser.Cod, Me.Idioma, FSNUser.Pyme)
        tvVarCal.DataSource = New HierarchicalDataSet(dsVarCal.Tables(0).DefaultView, "ID", "PADRE")
        tvVarCal.DataBind()
        Dim iNivel As Integer = 1
        HttpContext.Current.Session("HijosNoVisibles") = dsVarCal.Tables(2).DefaultView
        CargarMaterialesQATreeViewVarCal(tvVarCal.Nodes, iNivel, IIf(Session("FiltroVarCal") = "", "", "," & Session("FiltroVarCal") & ","))
        'almaceno en hiddens el id y la den de la variable que representa la puntuación total
        Me.hfPuntTotID.Value = dsVarCal.Tables(1).Rows(0).Item(0)
        Me.hfPuntTotDEN.Value = dsVarCal.Tables(1).Rows(0).Item(1)
        'Si hay Materiales de QA y el modo es automático chequeados configuramos el treeview de variables
        If FSNUser.QAPPAuto AndAlso Session("FiltroMaterialesQA") <> "" Then
            ViewState("MaterialesChequeados") = "," & Session("FiltroMaterialesQA") & ","
            iNivel = 1
            ConfigurarTreeViewVarCal(tvVarCal.Nodes, ViewState("MaterialesChequeados"), iNivel)
        End If

    End Sub
    ''' <summary>
    ''' Carga en la propiedad NavigateUrl los materiales de qa que se corresponden con cada variables
    ''' </summary>
    ''' <param name="Nodos">coleccion de nodos del treeeview</param>
    ''' <param name="Nivel">nivel de la coleccion de nodods</param>
    ''' <param name="sChequarNodos">cadena con los nodos a chequear</param>
    ''' <param name="NodoPadre">nodo padre</param>
    ''' <remarks></remarks>
    Sub CargarMaterialesQATreeViewVarCal(ByRef Nodos As TreeNodeCollection, ByRef Nivel As Integer, ByVal sChequarNodos As String, Optional ByRef NodoPadre As TreeNode = Nothing)
        Dim _Nivel As Integer
        For Each Nodo As TreeNode In Nodos
            If sChequarNodos <> "" Then
                If sChequarNodos.IndexOf("," & Nodo.Value & ",") >= 0 Then
                    Nodo.Checked = True
                End If
            End If
            If Nodo.ChildNodes.Count > 0 Then
                CargarMaterialesQATreeViewVarCal(Nodo.ChildNodes, Nivel + 1, sChequarNodos, Nodo)

                If Nivel > 1 Then
                    Dim Invisible As String = DameMatHijosInvisibles(Nodo.ValuePath)
                    Nodo.NavigateUrl = Nodo.NavigateUrl & IIf(Invisible = "", "", "_" & Invisible)
                End If
            Else
                If Nodo.NavigateUrl = MARCA_TODOS Then
                    Nodo.NavigateUrl = DameMatHijosInvisibles(Nodo.ValuePath)
                ElseIf Nodo.NavigateUrl = "" Then
                    Nodo.NavigateUrl = TODOS_MATQA
                End If
            End If

            Dim NodoPadreRecorre As TreeNode = NodoPadre
            For _Nivel = Nivel - 1 To 1 Step -1
                If Not NodoPadreRecorre Is Nothing Then
                    NodoPadreRecorre.NavigateUrl = Replace(NodoPadreRecorre.NavigateUrl, MARCA_TODOS, "")

                    If NodoPadreRecorre.NavigateUrl = "" Then
                        NodoPadreRecorre.NavigateUrl = Nodo.NavigateUrl
                    Else
                        If InStr("_" & NodoPadreRecorre.NavigateUrl & "_", "_" & Nodo.NavigateUrl & "_") = 0 Then
                            NodoPadreRecorre.NavigateUrl = NodoPadreRecorre.NavigateUrl & "_" & Nodo.NavigateUrl
                        End If
                    End If
                    If _Nivel > 1 Then
                        NodoPadreRecorre = NodoPadreRecorre.Parent
                    End If
                End If
            Next
        Next
    End Sub

    Private Function DameMatHijosInvisibles(ByVal VarCal As String) As String
        Dim ListaMatQa As String = ""

        'split del Varcal por  / luego por -
        Dim Variables As String() = Split(VarCal, "/")
        Dim Nivel As Integer = UBound(Variables) + 1

        'en session el churro
        Dim Vista As DataView = CType(HttpContext.Current.Session("HijosNoVisibles"), DataView)
        Dim Rows As DataRowView()
        Select Case Nivel
            Case 2
                Vista.Sort = "VC1 ASC, VC2 ASC"
                Rows = Vista.FindRows(New Object() {Split(Variables(0), "-")(0), Split(Variables(1), "-")(0)})
            Case 3
                Vista.Sort = "VC1 ASC, VC2 ASC, VC3 ASC"
                Rows = Vista.FindRows(New Object() {Split(Variables(0), "-")(0), Split(Variables(1), "-")(0), Split(Variables(2), "-")(0)})
            Case 4
                Vista.Sort = "VC1 ASC, VC2 ASC, VC3 ASC, VC4 ASC"
                Rows = Vista.FindRows(New Object() {Split(Variables(0), "-")(0), Split(Variables(1), "-")(0), Split(Variables(2), "-")(0), Split(Variables(3), "-")(0)})
        End Select

        For Each row As DataRowView In Rows
            If Not DBNullToStr(row("VC" & CStr(Nivel + 1))) = "" Then
                If Not DBNullToStr(row("MATERIAL_QA")) = "" Then
                    If InStr("_" & ListaMatQa & "_", "_" & row("MATERIAL_QA") & "_") = 0 Then
                        If Not (ListaMatQa = "") Then ListaMatQa = ListaMatQa & "_"
                        ListaMatQa = ListaMatQa & row("MATERIAL_QA")
                    End If
                Else
                    If InStr("_" & ListaMatQa & "_", "_" & TODOS_MATQA & "_") = 0 Then
                        If Not (ListaMatQa = "") Then ListaMatQa = ListaMatQa & "_"
                        ListaMatQa = ListaMatQa & TODOS_MATQA
                    End If
                End If
            End If
        Next

        Return ListaMatQa
    End Function

    ''' <summary>
    ''' Función que devuleve una colección con las variables de calidad chequeadas en el filtro
    ''' Además carga una variable de Session con con la lista de variables de negocio chequeadas separadas por coma, representadas por pares ID-NIVEL
    ''' </summary>
    ''' <returns>Colección con las variables de calidad chequeadas. Cada variable es una colección con las keys ID DEN PADRE</returns>
    ''' <remarks></remarks>
    Function FiltroVarCal() As Collection
        Dim cfiltroVarCal As New Collection
        Dim sfiltroVarCal As String = ""
        'Primero inserto la var que representa a la punt total y que no está en el arbol
        sfiltroVarCal = hfPuntTotID.Value & "-0"
        Dim cfiltro As New Collection()
        cfiltro.Add(hfPuntTotID.Value, "ID")
        cfiltro.Add("0", "NIVEL")
        cfiltro.Add(hfPuntTotDEN.Value, "DEN")
        cfiltro.Add("", "HIJOS")
        cfiltro.Add(True, "FILTRO")
        cfiltro.Add(TODOS_MATQA, "MATQA")
        cfiltroVarCal.Add(cfiltro, hfPuntTotID.Value & "-0")
        ObtenerNodosChequeados(hfPuntTotID.Value & "-0", cfiltroVarCal, sfiltroVarCal, tvVarCal.Nodes, True)
        Session("FiltroVarCal") = sfiltroVarCal
        Return cfiltroVarCal

    End Function

#End Region
#Region "Proveedores"
    ''' <summary>
    ''' Cargar los filtros de Proveedores
    ''' </summary>
    ''' <remarks></remarks>
    Sub CargarModuloProveedores()
        Me.lblTituloProveedores.Text = UCase(Me.Textos(7))
        '1-Solo un proveedor, 2-Todos, 3-Potenciales, 4-Panel

        Dim _listItem As New ListItem
        _listItem.Attributes("onclick") = "InicializarProveedor();"
        If Acceso.gbPotAValProvisionalSelProve Then _listItem.Attributes("onclick") &= "MostrarComboTipoProveedor(this);"
        _listItem.Text = Textos(13)
        _listItem.Value = 2
        Me.rblProveedores.Items.Add(_listItem)
        _listItem = New ListItem
        _listItem.Attributes("onclick") = "InicializarProveedor();"
        If Acceso.gbPotAValProvisionalSelProve Then _listItem.Attributes("onclick") &= "MostrarComboTipoProveedor(this);"
        _listItem.Text = Textos(15)
        _listItem.Value = 4
        Me.rblProveedores.Items.Add(_listItem)
        _listItem = New ListItem
        _listItem.Attributes("onclick") = "InicializarProveedor();"
        If Acceso.gbPotAValProvisionalSelProve Then _listItem.Attributes("onclick") &= "MostrarComboTipoProveedor(this);"
        _listItem.Text = Textos(14)
        _listItem.Value = 3
        Me.rblProveedores.Items.Add(_listItem)
        _listItem = New ListItem
        _listItem.Attributes("onclick") = "InicializarProveedor();"
        If Acceso.gbPotAValProvisionalSelProve Then _listItem.Attributes("onclick") &= "MostrarComboTipoProveedor(this);"
        _listItem.Text = Textos(12)
        _listItem.Value = 1
        Me.rblProveedores.Items.Add(_listItem)

        lblDenProveedor.Text = Me.Textos(3)
        Me.lblProveedoresBaja.Text = Me.Textos(16)
        Dim sValue As Integer = 4
        Dim sTipoProveValue As String = "0"
        If Session("Proveedores") <> "" Then
            sValue = CInt(Session("Proveedores"))
            If sValue = 1 Then
                txtProveedor.Text = Session("ProveDen")
                hProveDen.Value = Session("ProveDen")
                hProveCod.Value = Session("ProveCod")
            ElseIf sValue >= 4 Then
                sTipoProveValue = (sValue - 4).ToString
                sValue = 4
            End If
        End If
        If Acceso.gbPotAValProvisionalSelProve Then CType(divTipoProveedor.FindControl("wddTipoProveedor"), Infragistics.Web.UI.ListControls.WebDropDown).SelectedValue = sTipoProveValue
        If sValue >= 4 Then
            divTipoProveedor.Style.Add("visibility", "visible")
        Else
            divTipoProveedor.Style.Add("visibility", "hidden")
        End If
        rblProveedores.SelectedValue = sValue

    End Sub
#End Region
#Region "Puntuacion"
    ''' <summary> 
    ''' Puntuacion: Cargar los filtros puntuación desde hasta
    ''' </summary> 
    Sub CargarModuloPuntuacion()
        Me.lblTituloPuntuacion.Text = UCase(Me.Textos(35))
        Me.lblPuntuacionDesde.Text = Me.Textos(36)
        Me.lblPuntuacionHasta.Text = Me.Textos(37)
        Me.wnePuntuacionDesde.Value = Session("PuntuacionDesde")
        Me.wnePuntuacionHasta.Value = Session("PuntuacionHasta")
    End Sub
#End Region
#Region "TreeView"
    ''' <summary>
    ''' Recorre el treeview para almacenar en una colección y en jun string los nodos chequeados
    ''' </summary>
    ''' <param name="cFiltros">Colección para almacenar los ID, DEN y PADRE de los nodos del treeview chequeados</param>
    ''' <param name="sFiltros">String para alamacenar una cadena con los valors de los nodos separados por comas</param>
    ''' <param name="Nodos">Coleccion de nodods del treeview a recorrer</param>
    ''' <param name="ValorPar">Para la coleccion de variabels de calidad, el valor de sus nodos es un par ID-NIVEL</param>
    ''' <remarks></remarks>
    Sub ObtenerNodosChequeados(ByVal IDNodoRaiz As String, ByRef cFiltros As Collection, ByRef sFiltros As String, ByRef Nodos As TreeNodeCollection, Optional ByVal ValorPar As Boolean = False, Optional ByRef sFiltrosProve As String = "")
        For Each Nodo As TreeNode In Nodos
            'ValorPar (Es variable de calidad, el value del nodo es par ID-NIVEL) Las variables de calidad las metemos todas en la colecciÃƒÂ³n
            'Las unqas que no tienen NavigateUrl son aquellas que se muestran en el arbol porque puede ver una de sus hijas, pero no puede ver puntuaciones ni proveedore directametne asociados a ella
            If ValorPar OrElse Nodo.NavigateUrl <> "0" Then
                Dim NodoCol As New Collection()
                If ValorPar Then
                    Dim aPar() As String = Nodo.Value.Split("-")
                    NodoCol.Add(aPar(0), "ID")
                    NodoCol.Add(aPar(1), "NIVEL")
                Else
                    NodoCol.Add(Nodo.Value, "ID")
                    NodoCol.Add(Nodo.Depth + 1, "NIVEL")
                End If

                NodoCol.Add(Nodo.NavigateUrl, "PERMISO")

                NodoCol.Add(Nodo.Text, "DEN")
                NodoCol.Add("", "HIJOS")

                If Nodo.Checked Then
                    If Not ValorPar Then
                        If sFiltrosProve = "" Then
                            sFiltrosProve = DUNQASProve(Nodo.Value)
                        Else
                            sFiltrosProve = sFiltrosProve & "," & DUNQASProve(Nodo.Value)
                        End If
                    End If
                    sFiltros = IIf(sFiltros = "", "", sFiltros & ",") & Nodo.Value

                    NodoCol.Add(True, "FILTRO")
                Else
                    NodoCol.Add(False, "FILTRO")
                End If
                'Si el var añadir el mat de qa a la col
                If ValorPar Then
                    NodoCol.Add(Nodo.NavigateUrl, "MATQA")
                End If
                If IDNodoRaiz <> "" Then
                    Dim _cFiltros As Collection = cFiltros(IDNodoRaiz)
                    Dim _hijos As String = _cFiltros("HIJOS")
                    _hijos = _hijos & IIf(_hijos = "", "", ",") & Nodo.Value
                    cFiltros(IDNodoRaiz).Remove("HIJOS")
                    cFiltros(IDNodoRaiz).Add(_hijos, "HIJOS")
                End If
                cFiltros.Add(NodoCol, Nodo.Value)
            End If
            If Nodo.ChildNodes.Count <> 0 Then
                Dim _IDNodoRaiz As String = Nodo.Value
                If Not ValorPar AndAlso Nodo.NavigateUrl = "0" Then
                    _IDNodoRaiz = IDNodoRaiz
                End If
                ObtenerNodosChequeados(_IDNodoRaiz, cFiltros, sFiltros, Nodo.ChildNodes, ValorPar, sFiltrosProve)
            End If
        Next

    End Sub
#End Region

    ''' <summary>
    ''' Almacena los filtros en variables de Session y llama a la página panelCalidad.aspx que es la que muestrar los resutlados de la búsqueda
    ''' </summary>
    ''' <param name="sender">btnCargar</param>
    ''' <param name="e">EventArgs</param>
    ''' <remarks></remarks>
    Private Sub btnCargar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCargar.Click
        If HttpContext.Current.Session("materialesseleccionados") Is Nothing Then
            HttpContext.Current.Session("materialesseleccionados") = New List(Of IGmn)
        End If
        Dim oMaterialesSeleccionados As List(Of IGmn) = HttpContext.Current.Session("materialesseleccionados")
        If oMaterialesSeleccionados.Count = 0 And hProveCod.Value = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "alertaNoMateriales", "alert('" + Me.Textos(31) + "');", True)
            Exit Sub
        End If
        ObtenerFiltros(True)
        'Server.Transfer("panelCalidad.aspx")
        Response.Redirect("panelCalidad.aspx?C=0")
    End Sub
    'Private Sub btnGuardarFiltro_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardarFiltro.Click
    'ObtenerFiltros()
    'End Sub
    ''' <summary>
    ''' Carga los filtros seleccionados en variables de session para que durante la session el usurio los tenga guardadoos
    ''' y para que esten disponibles en el panel para la selección de los proveedores
    ''' </summary>
    ''' <param name="bCargar">Si se va a cargar el panel de proveedores</param>
    ''' <remarks></remarks>
    Private Sub ObtenerFiltros(Optional ByVal bCargar As Boolean = False)
        If bCargar Then
            Session("CFiltroMaterialesQA") = FiltroMaterialesQA()
            'Context.Items.Add("FiltroMaterialesQA", FiltroMaterialesQA)
        Else
            FiltroMaterialesQA()
        End If
        If bCargar Then
            'Context.Items.Add("FiltroUNQA", FiltroUNQA)
            Session("CFiltroUNQA") = FiltroUNQA()
        Else
            FiltroUNQA()
        End If
        If bCargar Then
            'Context.Items.Add("FiltroVarCal", FiltroVarCal)
            Session("CFiltroVarCal") = FiltroVarCal()
        Else
            FiltroVarCal()
        End If
        If Acceso.gbPotAValProvisionalSelProve And rblProveedores.SelectedValue = 4 Then    'Se ha seleccionado proveedores del panel
            Session("Proveedores") = (CShort(rblProveedores.SelectedValue) + CShort(CType(divTipoProveedor.FindControl("wddTipoProveedor"), Infragistics.Web.UI.ListControls.WebDropDown).SelectedValue)).ToString
        Else
            Session("Proveedores") = rblProveedores.SelectedValue
        End If
        Session("ProveCod") = hProveCod.Value
        Session("ProveDen") = hProveDen.Value
        Session("ProveedoresBaja") = IIf(chkProveedoresBaja.Checked, 1, 0)
        Session("bPuntuacionDesde") = IIf(wnePuntuacionDesde.Text = "", False, True)
        Session("bPuntuacionHasta") = IIf(wnePuntuacionHasta.Text = "", False, True)
        Session("PuntuacionDesde") = IIf(wnePuntuacionDesde.Text = "", Nothing, wnePuntuacionDesde.Value)
        Session("PuntuacionHasta") = IIf(wnePuntuacionHasta.Text = "", Nothing, wnePuntuacionHasta.Value)
    End Sub

    ''' <summary>
    ''' Chequeo automático de variables de calidad en función de los materiales de QA seleccionados
    ''' </summary>
    ''' <param name="sender">cblMaterialesQA</param>
    ''' <param name="e">EventArgs</param>
    ''' <remarks></remarks>
    Public Sub cblMaterialesQA_SelectedIndexChanged() Handles cblMaterialesQA.SelectedIndexChanged
        Dim gmnestr As EstructuraMateriales = FSNServer.Get_Object(GetType(FSNServer.EstructuraMateriales))
        HttpContext.Current.Session("materialesseleccionados") = New List(Of IGmn)
        Dim gmnSeleccionados As List(Of IGmn) = HttpContext.Current.Session("materialesseleccionados")
        For Each liItem As ListItem In cblMaterialesQA.Items
            'Para que la lógica se comparta con el arbol de materiales
            If liItem.Selected Then
                Dim oGmn As IGmn = gmnestr.getNodo(liItem.Value)
                oGmn.Den = liItem.Text
                'eliminamos el usuario del objeto para no guardarlo en sesión
                oGmn.User = Nothing
                gmnSeleccionados.Add(oGmn)
            End If
        Next
        If Not FSNUser.QAPPAuto Then
            Exit Sub
        End If
        Dim MaterialesChequeados As String = ViewState("MaterialesChequeados")
        Dim NivelVarCal As Integer = 1
        If MaterialesChequeados = "" Then
            MaterialesChequeados = ","
        End If

        If gmnestr.TipoEstructuraMateriales = Operadores.FiltroMaterial.FiltroMatQA Then
            For Each ogmn In gmnSeleccionados
                MaterialesChequeados = MaterialesChequeados & ogmn.GmnQAcod & ","
            Next
        End If

        For Each liItem As ListItem In cblMaterialesQA.Items

            'Si no está en la lista y esta cheuqeado lo metemos en la lista
            If MaterialesChequeados.IndexOf("," & liItem.Value & ",") = -1 Then
                If liItem.Selected Then
                    MaterialesChequeados = MaterialesChequeados & liItem.Value & ","
                    Exit For
                End If
                'Si esta en la lista y lo deschequeamos lo sacamos de la lista
            ElseIf Not liItem.Selected Then
                While Not MaterialesChequeados.IndexOf("," & liItem.Value & ",") = -1
                    MaterialesChequeados = Replace(MaterialesChequeados, "," & liItem.Value & ",", ",")
                End While
                Exit For
            End If
        Next

        ViewState("MaterialesChequeados") = MaterialesChequeados
        ConfigurarTreeViewVarCal(tvVarCal.Nodes, MaterialesChequeados, NivelVarCal)
        upVarCal.Update()
    End Sub

    ''' <summary>
    ''' Chequea o oculta los checks de las variables de calidad en función de si sus materiales se corresponden con los materiales de QA seleccionados en el filtro
    ''' </summary>
    ''' <param name="Nodos">Coleccion de nodos del treeview</param>
    ''' <param name="listaMaterialesChequeados"> cadena con los ids separados por comas de los materiales de QA chequeados</param>
    ''' <param name="Nivel">Nivel de la colección de nodods</param>
    ''' <remarks></remarks>
    Sub ConfigurarTreeViewVarCal(ByRef Nodos As TreeNodeCollection, ByRef listaMaterialesChequeados As String, ByRef Nivel As Integer)
        Dim sMaterialesQANodo As String
        Dim iMat As Integer = 0
        For Each Nodo As TreeNode In Nodos
            sMaterialesQANodo = "_" & Nodo.NavigateUrl & "_"
            If sMaterialesQANodo.IndexOf("_" & TODOS_MATQA & "_") = -1 Then
                If listaMaterialesChequeados = "" OrElse listaMaterialesChequeados = "," Then
                    Nodo.Checked = False
                    Nodo.ShowCheckBox = True
                Else
                    Dim aMaterialesQANodo() As String = Split(Nodo.NavigateUrl, "_")
                    Dim bEncontrado As Boolean = False
                    For iMat = 0 To aMaterialesQANodo.Length - 1
                        If listaMaterialesChequeados.IndexOf("," & aMaterialesQANodo(iMat) & ",") <> -1 Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next
                    If bEncontrado Then
                        If Nivel <= FSNUser.QAPPNivel Then
                            Nodo.Checked = True
                        End If
                        Nodo.ShowCheckBox = True
                    Else
                        Nodo.Checked = False
                        Nodo.ShowCheckBox = False
                    End If
                End If
            End If
            If Nodo.ChildNodes.Count > 0 Then
                ConfigurarTreeViewVarCal(Nodo.ChildNodes, listaMaterialesChequeados, Nivel + 1)
            End If
        Next

    End Sub

    ''' <summary>
    ''' Obtiene una lista de todos los materiales a partir de una cadena de texto que 
    ''' debe estar contenida en los nodos a buscar
    ''' </summary>
    ''' <param name="q"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True), _
System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function Obtener_Materiales(ByVal q As String)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oMateriales As EstructuraMateriales = FSNServer.Get_Object(GetType(FSNServer.EstructuraMateriales))
            oMateriales.TipoEstructuraMateriales = HttpContext.Current.Session("tipofiltromaterial")
            oMateriales.User = FSNUser
            Return From omat As Fullstep.FSNServer.IGmn In oMateriales.devolverTodosLosMateriales(q)
                   Select id = omat.key, name = omat.title, children = True, omat.Den
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Carga los materiales descendientes de un nodo , dada un identificador de nodo gmnqa###gmngmn1###gmngmn2###gmngmn3###gmngmn4
    ''' </summary>
    ''' <param name="key"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True), _
System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function Cargar_Nodo_Materiales(key As String)
        Try
            Dim nivel As Dictionary(Of String, IGmn)
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oMateriales As EstructuraMateriales = FSNServer.Get_Object(GetType(FSNServer.EstructuraMateriales))
            oMateriales.TipoEstructuraMateriales = HttpContext.Current.Session("tipofiltromaterial")
            oMateriales.User = FSNUser
            nivel = oMateriales.children(key)
            Dim result = From omat As IGmn In nivel.Values
                   Select id = omat.key, text = omat.title, children = omat.hasChildren, icon = False, omat.Den

            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Añade un nodo a los nodos seleccionados
    ''' </summary>
    ''' <param name="key">clave del nodo</param>
    ''' <param name="den">Den del nodo</param>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True), _
System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub addNode(key As String, den As String)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim oMateriales As EstructuraMateriales = FSNServer.Get_Object(GetType(FSNServer.EstructuraMateriales))
        oMateriales.TipoEstructuraMateriales = HttpContext.Current.Session("tipofiltromaterial")
        oMateriales.addSeleccion(key)
    End Sub


    ''' <summary>
    ''' Guarda la lista de materiales seleccionados
    ''' </summary>
    ''' <param name="data">Lista de pares key, den de los nodos de estructura de materiales</param>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True), _
System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function addAllNodes(data As Object)
        Dim gmnSeleccionados As List(Of IGmn)
        HttpContext.Current.Session("materialesseleccionados") = New List(Of IGmn)

        gmnSeleccionados = HttpContext.Current.Session("materialesseleccionados")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim oMateriales As EstructuraMateriales = FSNServer.Get_Object(GetType(FSNServer.EstructuraMateriales))
        oMateriales.User = FSNUser
        oMateriales.TipoEstructuraMateriales = HttpContext.Current.Session("tipofiltromaterial")
        For Each nodo In data
            Dim oGmn As IGmn
            oGmn = oMateriales.getNodo(nodo("id"))
            Dim temp() As String = Split(nodo("name"), "-")
            oGmn.Den = temp(temp.Length - 1)
            'eliminamos el usuario del objeto para no guardarlo en sesión
            oGmn.User = Nothing
            gmnSeleccionados.Add(oGmn)
        Next
    End Function

    ''' <summary>
    ''' Devuelve los nodos seleccionados para cargar el arbol de materiales y el autocompletar
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True), _
System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function getSelectedNodes()
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        If HttpContext.Current.Session("materialesseleccionados") Is Nothing Then
            HttpContext.Current.Session("materialesseleccionados") = New List(Of IGmn)
        End If
        Dim gmnSeleccionados As List(Of IGmn) = HttpContext.Current.Session("materialesseleccionados")
        Return From omat As IGmn In gmnSeleccionados
                   Select id = omat.key, name = omat.title, omat.Den
    End Function
End Class
