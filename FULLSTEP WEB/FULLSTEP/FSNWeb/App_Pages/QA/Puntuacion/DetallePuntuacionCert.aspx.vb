﻿Public Partial Class DetallePuntuacionCert
    Inherits FSNPage
    Private Const TODOS_MATQA As String = "TODOS"
    Private _dsFichaProveedor As DataSet

    Private _Unidad As String
    Private _Subtipo As String
    Private _Variable As String
    Private _Nivel As String
    Private _ProveCod As String
    Private _ProveDen As String

    Private _dtDatosVC As DataRow
    Private _Puntos As String
    Private _Calif As String
    Private _DenUnidad As String
    Private _FecAct As DateTime

    Private _ProveMatQA As String

    Private _FiltroVarCal As Collection

    Private oInstancia As FSNServer.Instancia
    Private oGrupo As FSNServer.Grupo
    Private oTabItem As Infragistics.Web.UI.LayoutControls.ContentTabItem
    Private tblTablaTab As System.Web.UI.WebControls.Table

    Private Const Imagelocation As String = "~/images/3puntos.gif"
    Private Const ClaseFilaTabla As String = "ugfilatabla"

    ''' <summary>
    ''' Se obtiene la tabla de la ficha del proveedor cuando se viene desde GS
    ''' </summary>
    ''' <remarks></remarks>
    Sub ObtenerTablaFichaProveedor()
        'Creamos la tabla que necesitamos para mostrar la ficha de calidad
        Dim dtVariables As DataTable = New DataTable("MaestroVariables")
        Dim dRow As DataRow
        Dim dColumna As DataColumn

        dColumna = New DataColumn("VC_ID", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("VC_NIVEL", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dtVariables.PrimaryKey = New DataColumn() {dtVariables.Columns("VC_ID"), dtVariables.Columns("VC_NIVEL")}
        dColumna = New DataColumn("VC_DEN", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("FORMULA", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("LEYENDA", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("TIPO", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("SUBTIPO", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("MODIFICAR", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        'añadimos una columna por cada UNQA

        dColumna = New DataColumn("PUNT_" & _Unidad, System.Type.GetType("System.Double"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("CAL_" & _Unidad, System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("FECACT_" & _Unidad, System.Type.GetType("System.DateTime"))
        dtVariables.Columns.Add(dColumna)

        'Añadimos las filas con las variables de calidad que el usuario puede ver y que afectan al proveedor
        For Each VarCal As Collection In FiltroVarCal
            Dim aMateriales() As String = Split(VarCal("MATQA"), "_")
            Dim bEncontrado As Boolean = False
            For iMat As Integer = 0 To aMateriales.Length - 1
                If aMateriales(iMat) = TODOS_MATQA Then
                    bEncontrado = True
                    Exit For
                End If
                If ProveMatQA.IndexOf("," & aMateriales(iMat) & ",") <> -1 Then
                    bEncontrado = True
                    Exit For
                End If
            Next
            If bEncontrado Then
                dRow = dtVariables.NewRow
                dRow("VC_ID") = VarCal("ID")
                dRow("VC_NIVEL") = VarCal("NIVEL")
                dRow("VC_DEN") = VarCal("DEN")
                dtVariables.Rows.Add(dRow)
            End If

        Next
        _dsFichaProveedor.Tables.Add(dtVariables)
        _dsFichaProveedor.Relations.Add("REL_VAR_PUNT", New DataColumn() {_dsFichaProveedor.Tables("MaestroVariables").Columns("VC_ID"), _dsFichaProveedor.Tables("MaestroVariables").Columns("VC_NIVEL")}, New DataColumn() {_dsFichaProveedor.Tables(0).Columns("VARCAL"), _dsFichaProveedor.Tables(0).Columns("NIVEL")}, False)
        'Recorremos la tabla de variables de calidad y obtenemos sus puntuaciones por cada unqa
        For Each Row As DataRow In dtVariables.Rows
            For Each RowPuntuacion As DataRow In Row.GetChildRows("REL_VAR_PUNT")
                Row("FORMULA") = RowPuntuacion("FORMULA")
                Row("LEYENDA") = RowPuntuacion("LEYENDA")
                Row("TIPO") = RowPuntuacion("TIPO")
                Row("SUBTIPO") = RowPuntuacion("SUBTIPO")
                Row("MODIFICAR") = RowPuntuacion("MODIFICAR")
                Row("PUNT_" & RowPuntuacion("UNQA")) = RowPuntuacion("PUNT")
                Row("CAL_" & RowPuntuacion("UNQA")) = RowPuntuacion("CALIFICACION")
                Row("FECACT_" & RowPuntuacion("UNQA")) = RowPuntuacion("FECACT")
                Dim _MesPuntuacion As Integer
                If FechaPuntuacion Is Nothing Then
                    If Not IsDBNull(RowPuntuacion("FECACT")) Then
                        _MesPuntuacion = Month(RowPuntuacion("FECACT"))
                        If _MesPuntuacion = 1 Then
                            FechaPuntuacion = ObtenerMes(12) & " " & Year(RowPuntuacion("FECACT")) - 1
                        Else
                            FechaPuntuacion = ObtenerMes(Month(RowPuntuacion("FECACT")) - 1) & " " & Year(RowPuntuacion("FECACT"))
                        End If
                    End If
                End If
            Next
        Next
    End Sub
    ''' <summary>
    ''' Datos de las variables de calidad para la ficha de calidad
    ''' </summary>
    ''' <param name="dsDatosUNQA">Datos de las variables de calidad</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerVarCal(ByVal dsDatosUNQA As DataSet) As Collection
        Dim cfiltroVarCal As New Collection
        Dim cfiltro As New Collection()
        With dsDatosUNQA.Tables(4)
            cfiltro.Add(_Variable, "ID")
            cfiltro.Add(_Nivel, "NIVEL")
            cfiltro.Add(.Rows(0)("DEN"), "DEN")
            cfiltro.Add(True, "FILTRO")
            cfiltro.Add("TODOS", "MATQA")
            cfiltroVarCal.Add(cfiltro, _Variable)
        End With

        Return cfiltroVarCal
    End Function
    ''' <summary>
    ''' Propiedad que contiene el datarow con los datos de la variable de la ficha de calidad del proveedor a mostrar.
    ''' </summary>
    ''' <value></value>
    ''' <returns>DataRow</returns>
    ''' <remarks>Llamada desde: MostrarPanelCert; Tiempo maximo:0</remarks>
    Protected ReadOnly Property dtDatosVC() As DataRow
        Get
            If _dtDatosVC Is Nothing Then
                If _dsFichaProveedor Is Nothing Then
                    Dim PMProveedor As FSNServer.Proveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                    _dsFichaProveedor = PMProveedor.DevolverFichaCalidad(_ProveCod, Me.FSNUser.Cod, _Unidad, _Variable, Me.Idioma)
                    If _dsFichaProveedor.Tables.Count > 0 Then
                        ObtenerTablaFichaProveedor()
                    End If
                    Me.InsertarEnCache("dsFichaProveedor_" & FSNUser.Cod, _dsFichaProveedor)
                Else
                    _dsFichaProveedor = CType(Cache("dsFichaProveedor_" & Usuario.Cod), DataSet)
                End If
                For Each row As DataRow In _dsFichaProveedor.Tables("MaestroVariables").Rows
                    If row.Item("VC_ID") = _Variable AndAlso row.Item("VC_NIVEL") = _Nivel Then
                        _dtDatosVC = row

                        _Puntos = DBNullToStr(row.Item("PUNT_" & _Unidad))
                        _Calif = DBNullToStr(row.Item("CAL_" & _Unidad))

                        Exit For
                    End If
                Next
            End If
                Return _dtDatosVC
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que contiene la denominación de la unidad de la ficha de calidad del proveedor a mostrar.
    ''' </summary>
    ''' <value></value>
    ''' <returns>denominación de la unidad</returns>
    ''' <remarks>Llamada desde: MostrarPanelCert; Tiempo maximo:0</remarks>
    Protected ReadOnly Property DenUnidad() As String
        Get
            If _DenUnidad Is Nothing Then
                Dim iDesdeDetalleVarProve As Integer = CType(Request.QueryString("DesdeDetalleVarProve"), Integer)
                Dim FiltroUNQA As Collection = If(iDesdeDetalleVarProve = 0, Session("CFiltroUNQA"), Session("CFiltroUNQADetalleVarProve"))

                For Each UNQA As Collection In FiltroUNQA
                    If _Unidad = UNQA("ID") Then
                        _DenUnidad = UNQA("DEN")
                        Exit For
                    End If
                Next
            End If

            Return _DenUnidad
        End Get
    End Property
    ''' <summary>
    ''' Carga los textos de la página e inicializa los controles
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    ''' <remarks>Llamada desde:Se produce cuando el control de servidor se carga en el objeto Page. Tiempo maximo:0</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dsDatos As DataSet
        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.PopupFichaProveedor

        _Unidad = Request("Key")
        _Subtipo = Request("Subtipo")
        _Variable = Request("Variable")
        _Nivel = Request("Nivel")
        _ProveCod = Request("codProv")
        _ProveDen = Request("DenProv")
        If Request("desdeGS") = 1 Then
            Dim oDetalle As FSNServer.Puntuacion = FSNServer.Get_Object(GetType(FSNServer.Puntuacion))
            dsDatos = oDetalle.DetallePuntuacion(_Variable, _Nivel, Me.Idioma, _ProveCod, _Unidad, _Subtipo)
            Session("CFiltroUNQA") = ObtenerUNQA(dsDatos)
            Session("CFiltroVarCal") = ObtenerVarCal(dsDatos)
        End If
        MostrarPanelCert()

    End Sub
    ''' <summary>
    ''' Datos de las unqa para la ficha de calidad
    ''' </summary>
    ''' <param name="dsDatosUNQA">Datos de las unqa</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerUNQA(ByVal dsDatosUNQA As DataSet) As Collection
        Dim cfiltroUNQA As New Collection
        Dim cfiltro As New Collection()
        If Not dsDatosUNQA.Tables(3).Rows.Count = 0 Then
            With dsDatosUNQA.Tables(3)
                cfiltro.Add(_Unidad, "ID")
                cfiltro.Add(_Nivel, "NIVEL")
                cfiltro.Add(.Rows(0)("DEN"), "DEN")
                cfiltroUNQA.Add(cfiltro, _Variable)
            End With
        End If
        'ObtenerFiltroUNQAHijos(cfiltroUNQA, dsDatosUNQA.Tables(0), dsDatosUNQA.Tables(1).Rows(0)("ID"), 1)
        Return cfiltroUNQA
    End Function
    Protected ReadOnly Property FiltroVarCal() As Collection
        Get
            If _FiltroVarCal Is Nothing Then
                Dim iDesdeDetalleVarProve As Integer = CType(Request.QueryString("DesdeDetalleVarProve"), Integer)
                _FiltroVarCal = If(iDesdeDetalleVarProve = 0, Session("CFiltroVarCal"), Session("CFiltroVarCalDetalleVarProve"))
                If _FiltroVarCal Is Nothing Then
                    If iDesdeDetalleVarProve = 0 Then
                        Response.Redirect("panelCalidadFiltro.aspx")
                    Else
                        Response.End()
                    End If
                End If
            End If
            Return _FiltroVarCal
        End Get
    End Property
    Protected ReadOnly Property ProveMatQA() As String
        Get
            If _ProveMatQA Is Nothing Then
                Dim iDesdeDetalleVarProve As Integer = CType(Request.QueryString("DesdeDetalleVarProve"), Integer)

                _ProveMatQA = Request.Form(Request.QueryString("h") & "hProveMatQA")

                If _ProveMatQA Is Nothing Then
                    _ProveMatQA = If(iDesdeDetalleVarProve = 0, Session("ProveMatQA"), Session("ProveMatQADetalleVarProve"))
                Else
                    Session("ProveMatQA") = _ProveMatQA
                End If

                If _ProveMatQA Is Nothing Then
                    If iDesdeDetalleVarProve = 0 Then
                        Response.Redirect("panelCalidadFiltro.aspx")
                    Else
                        Response.End()
                    End If
                End If
            End If
            Return _ProveMatQA
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que contiene la fecha de actualización de las puntuaciones y la almacena en el ViewState.
    ''' </summary>
    ''' <value></value>
    ''' <returns>String con la fecha</returns>
    ''' <remarks></remarks>
    Protected Property FechaPuntuacion() As String
        Get
            Return ViewState("FechaPuntuacion")
        End Get
        Set(ByVal value As String)
            ViewState("FechaPuntuacion") = value
        End Set
    End Property
    Function ObtenerMes(ByVal iMes As Integer) As String
        Dim IndiceMes As Integer = 11
        IndiceMes = IndiceMes + iMes
        ObtenerMes = Textos(IndiceMes)
    End Function
#Region "Fns ModalPopup"
    ''' <summary>
    ''' Muestra el modal popup de Certificado
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo ejecucion:0,3seg.</remarks>
    Private Sub MostrarPanelCert()

        Me.lblVarCodDen.Text = dtDatosVC.Item("VC_DEN")
        Me.lblDetalle.Text = Textos(0) 'Detalle de puntuación
        Me.lblImprimir.InnerText = Textos(16)

        Dim oDetalle As FSNServer.Puntuacion = FSNServer.Get_Object(GetType(FSNServer.Puntuacion))
        Dim ds As DataSet = oDetalle.DetallePuntuacion(_Variable, _Nivel, Me.Idioma, _ProveCod, _Unidad, _Subtipo)

        Dim fila As New HtmlTableRow
        Dim celda As New HtmlTableCell
        Dim mi_label As New Label

        Dim TextAux As String

        '''''''''''''
        'Detalle de puntuación del proveedor
        Me.lblDetPuntProve.Text = Textos(5)

        Me.lblDetPuntProveDat.Text = _ProveCod & " - " & _ProveDen
        'Fin Detalle de puntuación del proveedor

        '''''''''''''
        'en la unidad de negocio
        Me.lblEnUni.Text = Textos(6)

        Me.lblEnUniDat.Text = DenUnidad
        'Fin en la unidad de negocio

        '''''''''''''
        'Puntuacion (Calificacion):
        Me.lblPunt.Text = Textos(35)

        Me.lblPuntDat.Text = modUtilidades.FormatNumber(_Puntos, FSNUser.NumberFormat) & " (" & _Calif & ")"
        'Fin Puntuacion:

        '''''''''''''
        'Lista Padres
        fila = New HtmlTableRow

        celda = New HtmlTableCell

        mi_label = New Label()
        mi_label.CssClass = "RotuloDetallePuntuacionFondoGrisBold"
        mi_label.Text = Textos(1) 'Variable:

        celda.Controls.Add(mi_label) 'etiqueta Variable

        For i As Integer = 1 To _Nivel
            mi_label = New Label()

            TextAux = "/"
            If i = _Nivel Then TextAux = ""

            mi_label.CssClass = "fp_celda_vc_" & CStr(i) & "_DetallePuntuacion"
            If i < _Nivel Then
                mi_label.Text = ds.Tables(0).Rows(0).Item("VAR" & CStr(i)) & TextAux
            Else
                mi_label.Text = Me.lblVarCodDen.Text
            End If

            celda.Controls.Add(mi_label) 'Padre de nivel i
        Next

        fila.Controls.Add(celda)
        Me.tblPadre.Controls.Add(fila)
        'Fin Lista Padres

        '''''''''''''
        'Material Qa:
        Me.lblMatQa.Text = Textos(2)

        Me.lblMatQaDat.Text = ""

        If DBNullToStr(ds.Tables(0).Rows(0).Item("MAT")) <> "" Then
            Me.lblMatQaDat.Text = ds.Tables(0).Rows(0).Item("MAT")
        End If
        'Fin Material Qa

        '''''''''''''
        'Origen:
        Me.lblOrigen.Text = Textos(17)

        Me.lblOrigenDat.Text = ds.Tables(0).Rows(0).Item("ORIGEN")
        'Fin Origen:

        '''''''''''''
        'Formula:
        Me.lblFormula.Text = Textos(36) 'Formula

        Me.lblFormulaTexto.Text = ds.Tables(0).Rows(0).Item("FORMULA")
        'Fin Formula:

        ''''''''''''''
        'GrupoX1..4
        ''''''''''''''
        For Each RowBD As DataRow In ds.Tables(1).Rows
            fila = New HtmlTableRow
            fila.Height = "25px"

            celda = New HtmlTableCell
            celda.Style.Add("width", "60px")
            celda.Attributes.Add("class", "FondoGrisBorderTablaDetallePuntuacionCeldaGris")

            mi_label = New Label()
            mi_label.CssClass = "RotuloDetallePuntuacionFondoGrisBoldNoPad"
            mi_label.Text = RowBD.Item("VARIABLEX") 'Xi

            celda.Controls.Add(mi_label) 'etiqueta Xi

            fila.Controls.Add(celda)
            '''''
            celda = New HtmlTableCell
            celda.Style.Add("width", "180px")
            celda.Attributes.Add("class", "FondoGrisBorderTablaDetallePuntuacionCeldaGrisCentral")

            mi_label = New Label()
            mi_label.CssClass = "RotuloDetallePuntuacionFondoGrisBoldNoPad"
            mi_label.Text = DameTrozo(RowBD.Item("VARIABLEX"), DBNullToStr(ds.Tables(0).Rows(0).Item("DEN"))) 'Suelo ó Objetivo ó ...

            celda.Controls.Add(mi_label) 'Suelo ó Objetivo ó ...

            fila.Controls.Add(celda)
            ''''''
            celda = New HtmlTableCell
            celda.Style.Add("width", "100px")
            celda.Attributes.Add("class", "FondoGrisBorderTablaDetallePuntuacionCeldaWhite")

            mi_label = New Label()
            mi_label.CssClass = "RotuloDetallePuntuacionFondoGrisBoldNoPad"
            mi_label.Text = DBNullToDbl(RowBD.Item("VALORX")) 'Valor

            celda.Controls.Add(mi_label) 'Valor

            fila.Controls.Add(celda)

            Me.tblVarsX.Controls.Add(fila)
        Next
        '''''''''''''''''''
        If IsDBNull(ds.Tables(0).Rows(0).Item("INSTANCIA")) _
        OrElse IsDBNull(ds.Tables(0).Rows(0).Item("VERSION")) Then
            Me.lblEnProceso.Visible = False
            Me.TblDetalleCertif.Visible = False
            Me.uwtGrupos.Visible = False
        ElseIf ds.Tables(0).Rows(0).Item("VERSION") = 0 Then
            Me.lblEnProceso.Visible = False
            Me.TblDetalleCertif.Visible = False
            Me.uwtGrupos.Visible = False
        Else
            Dim oCert As FSNServer.Certificado = FSNServer.Get_Object(GetType(FSNServer.Certificado))
            Dim dsDatos As DataSet = oCert.DatosPuntuacionCert(ds.Tables(0).Rows(0).Item("INSTANCIA"), ds.Tables(0).Rows(0).Item("VERSION"))

            Dim oInstancia As FSNServer.Instancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
            oInstancia.ID = ds.Tables(0).Rows(0).Item("INSTANCIA")

            If oInstancia.ComprobarEnProceso() = 1 Then
                Me.lblEnProceso.Visible = True
                lblEnProceso.Text = Textos(42)
                Me.TblDetalleCertif.Visible = False
                Me.uwtGrupos.Visible = False
            Else
                Me.lblEnProceso.Visible = False

                lblDetalleEtq.Text = Textos(27) 'Detalle certificado
                lblDetalleDat.Text = ds.Tables(0).Rows(0).Item("ORIGEN") & " / " & _ProveDen & ", " & _ProveCod

                lblFecAlta.Text = Textos(28) 'Fecha de alta
                lblFecResp.Text = Textos(29) 'Fecha de respuesta
                lblFecSolic.Text = Textos(30) 'Fecha de solicitud
                lblFecDespubli.Text = Textos(31) 'Fecha de despublicacion
                lblFecLimCumpl.Text = Textos(32) 'Fecha limite de cumplimentación
                lblVersion.Text = Textos(33) 'Version

                With dsDatos.Tables(0).Rows(0)
                    If Not (DBNullToStr(.Item("FEC_ALTA")) = "") Then lblFecAltaDat.Text = Format(.Item("FEC_ALTA"), Usuario.DateFormat.ShortDatePattern)
                    If Not (DBNullToStr(.Item("FEC_RESPUESTA")) = "") Then lblFecRespDat.Text = Format(.Item("FEC_RESPUESTA"), Usuario.DateFormat.ShortDatePattern)
                    If Not (DBNullToStr(.Item("FEC_SOLICITUD")) = "") Then lblFecSolicDat.Text = Format(.Item("FEC_SOLICITUD"), Usuario.DateFormat.ShortDatePattern)
                    If Not (DBNullToStr(.Item("FEC_DESPUB")) = "") Then lblFecDespubliDat.Text = Format(.Item("FEC_DESPUB"), Usuario.DateFormat.ShortDatePattern) & IIf(.Item("PUBLICADA") = 1, " (" & Textos(34) & ")", "")
                    If Not (DBNullToStr(.Item("FEC_LIM_CUMPLIM")) = "") Then lblFecLimCumplDat.Text = Format(.Item("FEC_LIM_CUMPLIM"), Usuario.DateFormat.ShortDatePattern)
                    If Not (DBNullToStr(.Item("FECHA_MODIF")) = "") Then
                        lblVersionDat.Text = Format(.Item("FECHA_MODIF"), Usuario.DateFormat.ShortDatePattern)
                        lblVersionDat.Text = lblVersionDat.Text & " " & Format(.Item("FECHA_MODIF"), Usuario.DateFormat.ShortTimePattern)
                    End If
                    lblVersionDat.Text = lblVersionDat.Text & " " & IIf(DBNullToStr(.Item("PROVEEDOR")) = "", .Item("PERSONA"), .Item("PROVEEDOR"))
                End With

                'Grupos:
                uwtGrupos.Tabs.Clear()

                CargarGrupos(ds.Tables(0).Rows(0).Item("INSTANCIA"), ds.Tables(0).Rows(0).Item("VERSION"))
            End If
        End If
    End Sub

    ''' <summary>
    ''' Saca de entre la Leyenda de las variables implicadas en una formula la descripción de la variable indicada
    ''' </summary>
    ''' <param name="Variable">Variable de la q sacar su descripción</param>
    ''' <param name="DenomVariable">Leyenda de las variables implicadas en una formula</param>
    ''' <returns>descripción de la variable indicada</returns>
    ''' <remarks>Llamada desde: MostrarPanelCert; Tiempo máximo:0</remarks>
    Private Function DameTrozo(ByVal Variable As String, ByVal DenomVariable As String) As String
        'X1: Open non-conformities;X2: Non-conformities closed within deadline;X3: Non-conformities closed out of deadline;X4: Non-conformities with a negative closure
        Dim Aux As String = ""
        If InStr(DenomVariable, Variable & ":") > 0 Then
            Aux = DenomVariable.Substring(InStr(DenomVariable, Variable & ":") - 1)
        Else
            Return ""
        End If
        'X3: Non-conformities closed out of deadline;X4: Non-conformities with a negative closure
        If InStr(Aux, ";") = 0 Then
            DameTrozo = Aux.Substring(4)
        Else
            DameTrozo = Aux.Substring(4, InStr(Aux, ";") - 5)
        End If
    End Function
#End Region

#Region "Tabs"
    ''' <summary>
    ''' No se esta en proceso el certificado, esta función carga los tabs.
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    ''' <remarks>Llamada desde: sistema; Tiempo ejecucion:0,3seg. </remarks>
    Private Sub DetallePuntuacionCert_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack() Then
            If Not Me.lblEnProceso.Visible Then CargarCamposTabla()
        End If
    End Sub

    ''' <summary>
    ''' Carga los grupos de la solicitud como pestanas del objeto UltraWebTab
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load(). Tiempo máximo: 1 sg.</remarks>    
    Private Sub CargarGrupos(ByVal Instancia As Long, ByVal lVersion As Long)
        Dim lIndex As Integer = 0

        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = Instancia
        oInstancia.Version = lVersion
        oInstancia.CargarCamposInstancia(Me.Idioma, Usuario.CodPersona)

        If Not oInstancia.Grupos.Grupos Is Nothing Then
            For Each oGrupo As FSNServer.Grupo In oInstancia.Grupos.Grupos
                oTabItem = New Infragistics.Web.UI.LayoutControls.ContentTabItem
                Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
                Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
                oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(Idioma), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
                oTabItem.Key = lIndex.ToString

                uwtGrupos.Tabs.Add(oTabItem)
                lIndex += 1
            Next
        End If
    End Sub

    Dim otblRow As System.Web.UI.WebControls.TableRow
    Dim otblCell As System.Web.UI.WebControls.TableCell
    Dim sAdjun As String
    Dim idAdjun As String

    ''' <summary>
    ''' Carga los campos en el detalle
    ''' </summary>
    ''' <remarks>Llamada desde: detalleSolicConsulta; Tiempo máximo: - 2seg </remarks>
    Private Sub CargarCamposTabla()
        Dim oDSRow As System.Data.DataRow
        Dim oDSRowAdjun As System.Data.DataRow

        Dim lIndex As Integer = 1

        Dim oLbl As System.Web.UI.HtmlControls.HtmlGenericControl
        Dim oImgPopUp As System.Web.UI.WebControls.HyperLink

        For Each oTabItem In uwtGrupos.Tabs
            oGrupo = oInstancia.Grupos.Grupos.Item(lIndex)

            tblTablaTab = New Table
            tblTablaTab.ID = "_tbl" & oTabItem.Key
            tblTablaTab.Width = Unit.Percentage(100)
            tblTablaTab.CellPadding = 2

            If oGrupo.DSCampos.Tables.Count > 0 Then
                For Each oDSRow In oGrupo.DSCampos.Tables(0).Rows
                    If oDSRow.Item("VISIBLE") = 1 Then

                        '------------------------------
                        'Nombre del campo
                        '------------------------------
                        otblRow = New System.Web.UI.WebControls.TableRow
                        otblCell = New System.Web.UI.WebControls.TableCell
                        otblCell.Width = Unit.Percentage(40)

                        If oDSRow.Item("SUBTIPO") <> TiposDeDatos.TipoGeneral.TipoDesglose Then
                            oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
                            oLbl.InnerHtml = DBNullToSomething(oDSRow.Item("DEN_" & Idioma))

                            otblCell.Controls.Add(oLbl)
                            otblCell.CssClass = ClaseFilaTabla
                            otblRow.Cells.Add(otblCell)
                            Me.tblTablaTab.Rows.Add(otblRow)

                            oDSRow.Item("DEN_" & Idioma) = oDSRow.Item("DEN_" & Idioma).ToString.Replace("'", "\'")
                            oDSRow.Item("DEN_" & Idioma) = oDSRow.Item("DEN_" & Idioma).ToString.Replace("""", "\""")
                        End If

                        '------------------------------
                        'Valor del campo
                        '------------------------------
                        otblCell = New System.Web.UI.WebControls.TableCell
                        otblCell.Width = Unit.Percentage(58)

                        If oDSRow.Item("SUBTIPO") <> TiposDeDatos.TipoGeneral.TipoDesglose Then
                            oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
                            oLbl.InnerHtml = EscribirValorCampo(oDSRow)

                            If oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoNumerico AndAlso (IsDBNull(oDSRow.Item("TIPO_CAMPO_GS")) OrElse oDSRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.Almacen) AndAlso oDSRow.Item("TIPO") <> "6" Then
                                otblCell.Controls.Add(CargarImporteEnTabla(oLbl))
                            Else
                                otblCell.Controls.Add(oLbl)
                            End If

                            otblCell.CssClass = ClaseFilaTabla
                            otblRow.Cells.Add(otblCell)

                            'POPUPS con la descripción completa de textos medio y largos o archivos adjuntos o partidas presupuestarias
                            If ((oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio OrElse oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo) AndAlso oDSRow.Item("TIPO") <> "6" AndAlso _
                                ((IsDBNull(oDSRow.Item("TIPO_CAMPO_GS")) AndAlso oDSRow.Item("INTRO") = "0") OrElse DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.DescrBreve OrElse DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.DescrDetallada OrElse DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Partida OrElse DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Activo OrElse DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.TipoPedido) AndAlso Not IsDBNull(oDSRow.Item("VALOR_TEXT"))) Then

                                oImgPopUp = New System.Web.UI.WebControls.HyperLink
                                oImgPopUp.ImageUrl = Imagelocation

                                If oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo Then
                                    oImgPopUp.NavigateUrl = "javascript:AbrirFicherosAdjuntos('" & ObtenerAbrirFicherosAdjuntos(idAdjun, oDSRow.Item("ID_CAMPO"), oInstancia.ID, 1) & "')"
                                ElseIf DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Partida Then
                                    oImgPopUp.NavigateUrl = "javascript:AbrirDetallePartida('" & ObtenerAbrirDetallePartida(oDSRow.Item("DEN_" & Idioma), oDSRow.Item("PRES5")) & "','" & oDSRow.Item("VALOR_TEXT") & "')"
                                ElseIf DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Activo Then
                                    oImgPopUp.NavigateUrl = "javascript:AbrirDetalleActivo('" & ObtenerAbrirDetalleActivo() & "','" & oDSRow.Item("VALOR_TEXT") & "')"
                                ElseIf DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.TipoPedido Then
                                    oImgPopUp.NavigateUrl = "javascript:AbrirDetalleTipoPedido('" & oDSRow.Item("VALOR_TEXT") & "')"
                                Else
                                    oImgPopUp.NavigateUrl = "javascript:AbrirPopUpTexto('" & ObtenerAbrirPopUpTexto(Replace(oDSRow.Item("DEN_" & Idioma), "'", "\'"), Replace(oDSRow.Item("VALOR_TEXT"), "'", "\'")) & "')"
                                End If
                                otblCell = New System.Web.UI.WebControls.TableCell
                                otblCell.Width = Unit.Percentage(2)
                                otblCell.CssClass = ClaseFilaTabla
                                otblCell.Controls.Add(oImgPopUp)
                                otblRow.Cells.Add(otblCell)
                            Else
                                otblCell.ColumnSpan = 2
                            End If
                        Else
                            CargarDesglose(oDSRow.Item("ID"), oDSRow.Item("DEN_" & Idioma))
                        End If

                        Me.tblTablaTab.Rows.Add(otblRow)
                    End If
                Next

                oTabItem.Controls.Add(Me.tblTablaTab)
            End If
            lIndex += 1
        Next
    End Sub

    ''' <summary>
    ''' Escribe la denominación del campo en el formato en el que se le muestra al usuario
    ''' </summary>
    ''' <param name="oDSRow">Datarow con los datos relacionados con el campo a tratar</param>
    ''' <returns>Devuelve un string con la denominación en el formato correcto y en el idioma del usuario (en el caso de campos multiusuario)</returns>
    ''' <remarks>Llamada desde: CargarCamposTabla; Tiempo máximo: 0,1 sg.</remarks>
    Private Function EscribirValorCampo(ByVal oDSRow As DataRow) As String
        Dim sValor As String

        '- Si es una TABLA EXTERNA ya sea de tipo string, numérico o fecha se obtiene la denominación de VALOR_TEXT_DEN
        '- Si es un CAMPO DEL SISTEMA tb se obtiene la denominación de VALOR_TEXT_DEN
        If oDSRow.Item("TIPO") = "6" Then
            If Not IsDBNull(oDSRow.Item("VALOR_TEXT_DEN")) Then
                sValor = DevolverTablaExterna(oDSRow.Item("VALOR_TEXT_DEN"))
            End If
        ElseIf (Not IsDBNull(oDSRow.Item("TIPO_CAMPO_GS")) AndAlso (oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.ProveedorAdj OrElse oDSRow.Item("TIPO_CAMPO_GS") >= TiposDeDatos.TipoCampoGS.Proveedor) AndAlso oDSRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.DenArticulo AndAlso oDSRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo AndAlso oDSRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.CodArticulo) Then
            If Not IsDBNull(oDSRow.Item("VALOR_TEXT_DEN")) Then
                If Not IsDBNull(oDSRow.Item("TIPO_CAMPO_GS")) AndAlso (oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.PRES1 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres2 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres3 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres4) Then
                    sValor = DevolverPresupuestosConPorcentajes(oDSRow.Item("VALOR_TEXT"), oDSRow.Item("VALOR_TEXT_DEN"))
                Else
                    sValor = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), Idioma)
                End If
                If oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.ProveedorAdj OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Proveedor OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.OrganizacionCompras OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Centro Then
                    sValor = oDSRow.Item("VALOR_TEXT") & " - " & sValor
                ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Material Then
                    sValor = DevolverMaterialConCodigoUltimoNivel(oDSRow.Item("VALOR_TEXT"), sValor)
                ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.CentroCoste Then
                    If InStrRev(oDSRow.Item("VALOR_TEXT"), "#") > 0 Then
                        sValor = Right(oDSRow.Item("VALOR_TEXT"), Len(oDSRow.Item("VALOR_TEXT")) - InStrRev(oDSRow.Item("VALOR_TEXT"), "#")) & " - " & sValor
                    Else
                        sValor = oDSRow.Item("VALOR_TEXT") & " - " & sValor
                    End If
                ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Partida Then
                    If InStrRev(oDSRow.Item("VALOR_TEXT"), "#") > 0 Then
                        If InStrRev(oDSRow.Item("VALOR_TEXT"), "|") > 0 Then 'si no está en el nivel 1, si no más abajo
                            sValor = Right(oDSRow.Item("VALOR_TEXT"), Len(oDSRow.Item("VALOR_TEXT")) - InStrRev(oDSRow.Item("VALOR_TEXT"), "|")) & " - " & sValor
                        Else
                            sValor = Right(oDSRow.Item("VALOR_TEXT"), Len(oDSRow.Item("VALOR_TEXT")) - InStrRev(oDSRow.Item("VALOR_TEXT"), "#")) & " - " & sValor
                        End If
                    Else
                        sValor = oDSRow.Item("VALOR_TEXT") & " - " & sValor
                    End If
                ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Activo Then
                    sValor = oDSRow.Item("VALOR_TEXT") & " - " & sValor
                ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.TipoPedido Then
                    sValor = oDSRow.Item("VALOR_TEXT") & " - " & sValor
                End If
            End If
        End If
        If sValor = "" Then 'por si el campo DEN está vacío. Todavía no se guardan los datos en este campo.
            Select Case oDSRow.Item("SUBTIPO")

                Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoTextoLargo
                    sValor = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
                    If (oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio OrElse oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo) AndAlso Len(sValor) > 80 Then
                        sValor = Mid(sValor, 1, 80) & "..."
                    End If

                Case TiposDeDatos.TipoGeneral.TipoNumerico
                    If Not IsDBNull(oDSRow.Item("VALOR_NUM")) Then
                        sValor = QuitarDecimalSiTodoCeros(FSNLibrary.FormatNumber(oDSRow.Item("VALOR_NUM"), Usuario.NumberFormat))
                    End If

                Case TiposDeDatos.TipoGeneral.TipoFecha
                    If IsDBNull(oDSRow.Item("VALOR_FEC")) Then
                        sValor = "&nbsp;"
                    Else
                        sValor = FormatDate(DevolverFechaRelativaA(DBNullToSomething(oDSRow.Item("VALOR_NUM")), DBNullToSomething(oDSRow.Item("VALOR_FEC"))), Usuario.DateFormat)
                    End If
                Case TiposDeDatos.TipoGeneral.TipoBoolean
                    If Not IsDBNull(oDSRow.Item("VALOR_BOOL")) Then
                        sValor = IIf(oDSRow.Item("VALOR_BOOL") = 1, Textos(43), Textos(44))
                    End If

                Case TiposDeDatos.TipoGeneral.TipoArchivo
                    sAdjun = ""
                    idAdjun = ""
                    Dim oDSRowAdjun As System.Data.DataRow
                    For Each oDSRowAdjun In oDSRow.GetChildRows("REL_CAMPO_ADJUN")
                        idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                        sAdjun += oDSRowAdjun.Item("NOM") + " (" + FSNLibrary.FormatNumber((oDSRowAdjun.Item("DATASIZE") / 1024), Usuario.NumberFormat) + " Kb.), "
                    Next
                    If sAdjun <> "" Then
                        sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
                        idAdjun = idAdjun.Substring(0, idAdjun.Length - 2)
                    End If

                    If Len(sAdjun) > 90 Then
                        sValor = Mid(sAdjun, 1, 90) & "..."
                    Else
                        sValor = sAdjun
                    End If

                    If sAdjun <> "" Then
                        Dim enlace As String
                        If oDSRow.GetChildRows("REL_CAMPO_ADJUN").Length > 1 Then
                            enlace = "<a href=""javascript:AbrirFicherosAdjuntos('" & ObtenerAbrirFicherosAdjuntos(idAdjun, oDSRow.Item("ID_CAMPO"), oInstancia.ID, 1) & "')"">" & sValor & "</a>"
                        Else
                            enlace = "<a href=""javascript:IrAAdjunto('" & ObtenerPathAdjunto(idAdjun) & "')"">" & sValor & "</a>"
                        End If

                        sValor = enlace
                    End If
            End Select

        End If

        If Trim(sValor) = "" Then sValor = "&nbsp;"

        EscribirValorCampo = sValor
    End Function

    ''' <summary>
    ''' Para que los importes salgan alineados a la derecha.
    ''' </summary>
    ''' <param name="oLbl">Control etiqueta</param>
    ''' <returns>Devuelve un HtmlTable</returns>
    ''' <remarks>LLamada desde: CargarCamposTabla; Tiempo máximo: 0,1 sg.</remarks>
    Private Function CargarImporteEnTabla(ByVal oLbl As HtmlGenericControl) As HtmlTable

        Dim Tbl As System.Web.UI.HtmlControls.HtmlTable
        Dim Row As System.Web.UI.HtmlControls.HtmlTableRow
        Dim Cell As System.Web.UI.HtmlControls.HtmlTableCell

        Row = New System.Web.UI.HtmlControls.HtmlTableRow
        Cell = New System.Web.UI.HtmlControls.HtmlTableCell
        Cell.Controls.Add(oLbl)
        Cell.Align = "right"
        Cell.Attributes("class") = "CampoSoloLectura"
        Row.Cells.Add(Cell)

        Tbl = New System.Web.UI.HtmlControls.HtmlTable
        Tbl.Align = "left"
        Tbl.Width = "100px"
        Tbl.CellPadding = 0
        Tbl.CellSpacing = 0
        Tbl.Rows.Add(Row)

        CargarImporteEnTabla = Tbl
    End Function

    ''' <summary>
    ''' Carga los campos del desglose en el detalle
    ''' </summary>
    ''' <param name="CampoID">Id en bbdd del campo padre</param>
    ''' <remarks>Llamada desde: detalleSolicConsulta/CargarCamposTabla; Tiempo máximo: Puede ser > 2seg dependiendo de las lineas de los desglose </remarks>
    Private Sub CargarDesglose(ByVal CampoID As Long, ByVal sTitulo As String)
        Dim oDS As DataSet

        Dim oRow As DataRow
        Dim oRowDesglose As DataRow

        Dim oCampo As FSNServer.Campo

        Dim tblDesglose As System.Web.UI.HtmlControls.HtmlTable
        Dim oRowTablaDesglose As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oCellTablaDesglose As System.Web.UI.HtmlControls.HtmlTableCell

        Dim oLbl As System.Web.UI.HtmlControls.HtmlGenericControl
        Dim oDivDesglose As System.Web.UI.HtmlControls.HtmlGenericControl
        Dim oImgPopUp As System.Web.UI.WebControls.HyperLink

        Dim nTotalLineasDesglose As Integer
        Dim nLineaDesglose As Integer = 1
        Dim findTheseVals(2) As Object
        Dim sAdjun As String = ""
        Dim idAdjun As String = ""

        oCampo = FSNServer.Get_Object(GetType(FSNServer.Campo))
        oCampo.Id = CampoID
        oDS = oCampo.LoadInstDesglose(Me.Idioma, oInstancia.ID, Usuario.CodPersona, oInstancia.Version, , , False, True)

        tblDesglose = New System.Web.UI.HtmlControls.HtmlTable
        tblDesglose.Style.Add("width ", "100%")

        '------------------------------
        'TITULO del desglose
        '------------------------------
        oRowTablaDesglose = New System.Web.UI.HtmlControls.HtmlTableRow
        oCellTablaDesglose = New System.Web.UI.HtmlControls.HtmlTableCell
        oCellTablaDesglose.InnerText = sTitulo
        Dim iColSpan As Integer = 1
        For Each oRow In oDS.Tables(0).Rows
            If oRow.Item("VISIBLE") = 1 Then
                iColSpan = iColSpan + 1
            End If
        Next

        oCellTablaDesglose.ColSpan = iColSpan
        oCellTablaDesglose.Attributes("class") = "cabeceraTituloDesglose"
        oRowTablaDesglose.Cells.Add(oCellTablaDesglose)
        tblDesglose.Rows.Add(oRowTablaDesglose)

        '------------------------------
        'CABECERA del desglose
        '------------------------------
        oRowTablaDesglose = New System.Web.UI.HtmlControls.HtmlTableRow
        For Each oRow In oDS.Tables(0).Rows
            If oRow.Item("VISIBLE") = 1 Then
                oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
                oLbl.InnerHtml = DBNullToSomething(oRow.Item("DEN_" & Idioma))

                oCellTablaDesglose = New System.Web.UI.HtmlControls.HtmlTableCell
                oCellTablaDesglose.ID = "COL_" + oRow.Item("ID").ToString
                oCellTablaDesglose.Controls.Add(oLbl)
                oCellTablaDesglose.Attributes("class") = "cabeceraDesglose"

                If ((oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio OrElse oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo) AndAlso oRow.Item("TIPO") <> "6" AndAlso _
                    (IsDBNull(oRow.Item("TIPO_CAMPO_GS")) AndAlso oRow.Item("INTRO") = 0) OrElse DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.DescrBreve OrElse DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.DescrDetallada OrElse DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Partida OrElse DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Activo) Then
                    oCellTablaDesglose.ColSpan = 2
                End If
                oRowTablaDesglose.Cells.Add(oCellTablaDesglose)
            End If
        Next
        tblDesglose.Rows.Add(oRowTablaDesglose)


        '------------------------------
        'DETALLE del desglose
        '------------------------------
        If oDS.Tables(0).Rows.Count > 0 Then
            nTotalLineasDesglose = 0
            For Each oRow In oDS.Tables("LINEAS").Rows
                If nTotalLineasDesglose < oRow.Item("LINEA") Then
                    nTotalLineasDesglose = oRow.Item("LINEA")
                End If
            Next
        End If

        Do While nLineaDesglose <= nTotalLineasDesglose
            oRowTablaDesglose = New System.Web.UI.HtmlControls.HtmlTableRow
            For Each oRow In oDS.Tables(0).Rows
                If oRow.Item("VISIBLE") = 1 Then
                    idAdjun = ""
                    sAdjun = ""

                    findTheseVals(0) = nLineaDesglose
                    findTheseVals(1) = oCampo.IdCopiaCampo
                    findTheseVals(2) = oRow.Item("ID")
                    oRowDesglose = oDS.Tables(2).Rows.Find(findTheseVals)

                    oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")

                    oCellTablaDesglose = New System.Web.UI.HtmlControls.HtmlTableCell
                    oCellTablaDesglose.Attributes("class") = IIf(nLineaDesglose Mod 2 = 0, "ugfilaalttablaDesglose", "ugfilatablaDesglose")
                    oCellTablaDesglose.NoWrap = True
                    If oRowDesglose Is Nothing Then
                        oLbl.InnerHtml = "&nbsp;"
                        'Añadimos la etiqueta a la celda
                        oCellTablaDesglose.Controls.Add(oLbl)
                        'Añadimos la celda a la fila
                        oRowTablaDesglose.Cells.Add(oCellTablaDesglose)
                    Else
                        '- Si es una TABLA EXTERNA ya sea de tipo string, numérico o fecha se obtiene la denominación de VALOR_TEXT_DEN
                        '- Si es un CAMPO DEL SISTEMA tb se obtiene la denominación de VALOR_TEXT_DEN
                        If oRow.Item("TIPO") = "6" Then
                            If Not IsDBNull(oRowDesglose.Item("VALOR_TEXT_DEN")) Then
                                oLbl.InnerHtml = DevolverTablaExterna(oRowDesglose.Item("VALOR_TEXT_DEN"))
                            End If
                        ElseIf (Not IsDBNull(oRowDesglose.Item("TIPO_CAMPO_GS")) AndAlso (oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.ProveedorAdj OrElse oRowDesglose.Item("TIPO_CAMPO_GS") >= TiposDeDatos.TipoCampoGS.Proveedor) AndAlso oRowDesglose.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.DenArticulo AndAlso oRowDesglose.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo AndAlso oRowDesglose.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.CodArticulo) Then
                            If Not IsDBNull(oRowDesglose.Item("VALOR_TEXT_DEN")) Then
                                If Not IsDBNull(oRowDesglose.Item("TIPO_CAMPO_GS")) AndAlso (oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.PRES1 OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres2 OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres3 OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres4) Then
                                    oLbl.InnerHtml = DevolverPresupuestosConPorcentajes(oRowDesglose.Item("VALOR_TEXT"), oRowDesglose.Item("VALOR_TEXT_DEN"))
                                Else
                                    oLbl.InnerHtml = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oRowDesglose.Item("VALOR_TEXT_DEN"), Idioma)
                                End If
                                If oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.ProveedorAdj OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Proveedor OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.OrganizacionCompras OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Centro Then
                                    oLbl.InnerHtml = oRowDesglose.Item("VALOR_TEXT") & " - " & oLbl.InnerHtml
                                ElseIf oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Material Then
                                    oLbl.InnerHtml = DevolverMaterialConCodigoUltimoNivel(oRowDesglose.Item("VALOR_TEXT"), oLbl.InnerHtml)
                                ElseIf oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.CentroCoste Then
                                    If InStrRev(oRowDesglose.Item("VALOR_TEXT"), "#") > 0 Then
                                        oLbl.InnerHtml = Right(oRowDesglose.Item("VALOR_TEXT"), Len(oRowDesglose.Item("VALOR_TEXT")) - InStrRev(oRowDesglose.Item("VALOR_TEXT"), "#")) & " - " & oLbl.InnerHtml
                                    Else
                                        oLbl.InnerHtml = oRowDesglose.Item("VALOR_TEXT") & " - " & oLbl.InnerHtml
                                    End If
                                ElseIf oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Partida Then
                                    If InStrRev(oRowDesglose.Item("VALOR_TEXT"), "#") > 0 Then
                                        If InStrRev(oRowDesglose.Item("VALOR_TEXT"), "|") > 0 Then 'si no está en el nivel 1, si no más abajo
                                            oLbl.InnerHtml = Right(oRowDesglose.Item("VALOR_TEXT"), Len(oRowDesglose.Item("VALOR_TEXT")) - InStrRev(oRowDesglose.Item("VALOR_TEXT"), "|")) & " - " & oLbl.InnerHtml
                                        Else
                                            oLbl.InnerHtml = Right(oRowDesglose.Item("VALOR_TEXT"), Len(oRowDesglose.Item("VALOR_TEXT")) - InStrRev(oRowDesglose.Item("VALOR_TEXT"), "#")) & " - " & oLbl.InnerHtml
                                        End If
                                    Else
                                        oLbl.InnerHtml = oRowDesglose.Item("VALOR_TEXT") & " - " & oLbl.InnerHtml
                                    End If
                                ElseIf oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Activo Then
                                    oLbl.InnerHtml = oRowDesglose.Item("VALOR_TEXT") & " - " & oLbl.InnerHtml
                                End If
                            End If
                        End If
                        If oLbl.InnerHtml = "" Then 'por si el campo DEN está vacío. Todavía no se guardan los datos en este campo.
                            Select Case oRow.Item("SUBTIPO")
                                Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoTextoLargo
                                    oLbl.InnerHtml = DBNullToSomething(oRowDesglose.Item("VALOR_TEXT"))

                                    If oRow.Item("INTRO") = "1" Then
                                        Dim oRowLista() As DataRow
                                        Dim Lista = New DataSet
                                        Lista.Merge(oRow.GetChildRows("REL_CAMPO_LISTA"))
                                        If Lista.Tables.Count > 0 Then
                                            If DBNullToSomething(oRowDesglose.Item("VALOR_NUM").ToString) <> Nothing Then
                                                oRowLista = Lista.Tables(0).Select("ID = " + oRowDesglose.Item("CAMPO_HIJO").ToString + " and ORDEN=" + oRowDesglose.Item("VALOR_NUM").ToString)
                                            Else
                                                oRowLista = Lista.Tables(0).Select("ID = " + oRowDesglose.Item("CAMPO_HIJO").ToString + " and VALOR_TEXT_" & Idioma & " =" + StrToSQLNULL(oRowDesglose.Item("VALOR_TEXT").ToString))
                                            End If
                                            If oRowLista.Length > 0 Then
                                                oLbl.InnerHtml = oRowLista(0).Item("VALOR_TEXT_" & Idioma)
                                            End If
                                        End If
                                    End If

                                    If (oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio OrElse oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo) AndAlso Len(oLbl.InnerHtml) > 80 Then
                                        oLbl.InnerHtml = Mid(oLbl.InnerHtml, 1, 80) & "..."
                                    End If

                                Case TiposDeDatos.TipoGeneral.TipoNumerico
                                    oCellTablaDesglose.Align = "right"
                                    If Not IsDBNull(oRowDesglose.Item("VALOR_NUM")) Then
                                        oLbl.InnerHtml = QuitarDecimalSiTodoCeros(FSNLibrary.FormatNumber(oRowDesglose.Item("VALOR_NUM"), Usuario.NumberFormat))
                                    End If
                                Case TiposDeDatos.TipoGeneral.TipoFecha
                                    If IsDBNull(oRowDesglose.Item("VALOR_FEC")) Then
                                        oLbl.InnerHtml = "&nbsp;"
                                    Else
                                        oLbl.InnerHtml = FormatDate(DevolverFechaRelativaA(DBNullToSomething(oRowDesglose.Item("VALOR_NUM")), DBNullToSomething(oRowDesglose.Item("VALOR_FEC"))), Usuario.DateFormat)
                                    End If

                                Case TiposDeDatos.TipoGeneral.TipoBoolean
                                    If Not IsDBNull(oRowDesglose.Item("VALOR_BOOL")) Then
                                        oLbl.InnerHtml = IIf(oRowDesglose.Item("VALOR_BOOL") = 1, Textos(43), Textos(44))
                                    End If
                                Case TiposDeDatos.TipoGeneral.TipoArchivo
                                    oLbl.Attributes("class") = "TipoArchivo"

                                    Dim oDSRowAdjun As DataRow
                                    For Each oDSRowAdjun In oRowDesglose.GetChildRows("REL_LINEA_ADJUNTO")
                                        idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                                        sAdjun += oDSRowAdjun.Item("NOM") + " (" + FSNLibrary.FormatNumber((DBNullToSomething(oDSRowAdjun.Item("DATASIZE")) / 1024), Usuario.NumberFormat) + " Kb.), "
                                    Next
                                    If sAdjun <> "" Then
                                        sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
                                        idAdjun = idAdjun.Substring(0, idAdjun.Length - 2)

                                        Dim enlace As String
                                        If oRowDesglose.GetChildRows("REL_LINEA_ADJUNTO").Length > 1 Then
                                            enlace = "<a href=""javascript:AbrirFicherosAdjuntos('" & ObtenerAbrirFicherosAdjuntos(idAdjun, oRow.Item("ID"), oInstancia.ID, 3) & "')"">" & sAdjun & "</a>"
                                        Else
                                            enlace = "<a href=""javascript:IrAAdjunto('" & ObtenerPathAdjunto(idAdjun, 3) & "')"">" & sAdjun & "</a>"
                                        End If

                                        oLbl.InnerHtml = enlace
                                    End If
                            End Select
                        End If
                        If oLbl.InnerHtml = "" Then
                            oLbl.InnerHtml = "&nbsp;"
                        End If

                        'Añadimos la etiqueta a la celda
                        oCellTablaDesglose.Controls.Add(oLbl)
                        'Añadimos la celda a la fila
                        oRowTablaDesglose.Cells.Add(oCellTablaDesglose)

                        If ((oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio OrElse oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo) AndAlso oRow.Item("TIPO") <> "6" AndAlso _
                                (IsDBNull(oRow.Item("TIPO_CAMPO_GS")) AndAlso oRow.Item("INTRO") = 0) _
                                OrElse DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.DescrBreve _
                                OrElse DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.DescrDetallada _
                                OrElse DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Partida _
                                OrElse DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Activo) Then

                            oCellTablaDesglose = New System.Web.UI.HtmlControls.HtmlTableCell
                            oCellTablaDesglose.Attributes("class") = IIf(nLineaDesglose Mod 2 = 0, "ugfilaalttablaDesglose", "ugfilatablaDesglose")

                            'Añadimos el botón para abrir el popup SOLO si hay archivos
                            If sAdjun <> "" OrElse Not IsDBNull(oRowDesglose.Item("VALOR_TEXT")) Then

                                oImgPopUp = New System.Web.UI.WebControls.HyperLink
                                oImgPopUp.ImageUrl = Imagelocation
                                If oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo Then
                                    oImgPopUp.NavigateUrl = "javascript:AbrirFicherosAdjuntos('" & ObtenerAbrirFicherosAdjuntos(idAdjun, oRow.Item("ID"), oInstancia.ID, 3) & "')"
                                ElseIf DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Partida Then
                                    oImgPopUp.NavigateUrl = "javascript:AbrirDetallePartida('" & ObtenerAbrirDetallePartida(oRow.Item("DEN_" & Idioma), oRow.Item("PRES5")) & "','" & oRowDesglose.Item("VALOR_TEXT") & "')"
                                ElseIf DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Activo Then
                                    oImgPopUp.NavigateUrl = "javascript:AbrirDetalleActivo('" & ObtenerAbrirDetalleActivo() & "','" & oRowDesglose.Item("VALOR_TEXT") & "')"
                                Else
                                    oImgPopUp.NavigateUrl = "javascript:AbrirPopUpTexto('" & ObtenerAbrirPopUpTexto(Replace(oRow.Item("DEN_" & Idioma), "'", "\'"), Replace(oRowDesglose.Item("VALOR_TEXT"), "'", "\'")) & "')"
                                End If
                                oCellTablaDesglose.Controls.Add(oImgPopUp)
                            Else
                                oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
                                oLbl.InnerHtml = "&nbsp;"
                                oCellTablaDesglose.Controls.Add(oLbl)
                            End If

                            'Añadimos la celda a la fila
                            oRowTablaDesglose.Cells.Add(oCellTablaDesglose)
                        End If
                    End If
                End If
            Next

            'Añadimos la fila a la tabla
            tblDesglose.Rows.Add(oRowTablaDesglose)
            nLineaDesglose = nLineaDesglose + 1
        Loop

        otblCell.Controls.Add(tblDesglose)
        otblCell.ColumnSpan = 3
        otblRow.Cells.Add(otblCell)
    End Sub

    ''' <summary>
    ''' Devuelve la denominación a mostrar de un campo que pertenece a una tabla externa.
    ''' </summary>
    ''' <param name="sValorTextDen">Valor guardado en VALOR_TEXT_DEN. Formato: s/n/d[primary key]#[campo mostrar]</param>
    ''' <returns>El campo a mostrar formateado (si es un nº o una fecha)</returns>
    ''' <remarks>Llamada desde: EscribirValorCampo  CargarDesglose; Tiempo máximo: 0 sg;</remarks>
    Private Function DevolverTablaExterna(ByVal sValorTextDen As String) As String
        Dim sValor As String
        Dim sTxt As String = sValorTextDen.ToString()
        Dim sTxt2 As String
        If sTxt.IndexOf("]#[") > 0 Then
            sTxt2 = sTxt.Substring(2, sTxt.IndexOf("]#[") - 2)
        Else
            sTxt2 = sTxt.Substring(2, sTxt.Length - 3)
        End If
        Select Case sTxt.Substring(0, 1)
            Case "s"
                sValor = sTxt2
            Case "n"
                sValor = CType(sTxt2, Double).ToString("N" & IIf((CType(sTxt2, Double) Mod 1) > 0, "", "0"), Usuario.NumberFormat)
            Case "d"
                sValor = CType(sTxt2, Date).ToString("d", Usuario.DateFormat)
        End Select
        If sTxt.IndexOf("]#[") > 0 Then
            sValor = sValor & " "
            sTxt2 = sTxt.Substring(sTxt.IndexOf("]#[") + 3, sTxt.Length - sTxt.IndexOf("]#[") - 5)
            Select Case sTxt.Substring(sTxt.Length - 1, 1)
                Case "s"
                    sValor = sValor & sTxt2
                Case "n"
                    sValor = CType(sTxt2, Double).ToString("N" & IIf((CType(sTxt2, Double) Mod 1) > 0, "", "0"), Usuario.NumberFormat)
                Case "d"
                    sValor = CType(sTxt2, Date).ToString("d", Usuario.DateFormat)
            End Select
        End If
        DevolverTablaExterna = sValor
    End Function

    ''' <summary>
    ''' Devuelve la denominación a mostrar de un campo que pertenece a una partida presupuestaria
    ''' </summary>
    ''' <param name="sValorText">Valor guardado en VALOR_TEXT. Codigo Partida</param>
    ''' <param name="sValorTextDen">Valor guardado en VALOR_TEXT_DEN. Porcentaje Partida</param>
    ''' <returns>El campo a mostrar formateado. Descripcion (Porcentaje)</returns>
    ''' <remarks>Llamada desde: EscribirValorCampo  CargarDesglose; Tiempo máximo: 0 sg;</remarks>
    Private Function DevolverPresupuestosConPorcentajes(ByVal sValorText As String, ByVal sValorTextDen As String) As String
        Dim arrPresupuestos() As String = sValorText.Split("#")
        Dim arrDenominaciones() As String = sValorTextDen.ToString().Split("#")
        Dim oPresup As String
        Dim iContadorPres As Integer
        Dim arrPresup(2) As String
        Dim dPorcent As Double
        Dim sValor As String

        iContadorPres = 0
        For Each oPresup In arrPresupuestos
            iContadorPres = iContadorPres + 1
            arrPresup = oPresup.Split("_")
            'Los presupuestos siempre llevan el porcentaje como 0.xx salvo cuando es 100% que llevan 1
            dPorcent = Numero(arrPresup(2), ".", "")
            sValor = arrDenominaciones(iContadorPres - 1) & " (" & dPorcent.ToString("0.00%", Usuario.NumberFormat) & "); " & sValor
        Next
        DevolverPresupuestosConPorcentajes = sValor
    End Function

    ''' <summary>
    ''' Devuelve la denominación a mostrar de un campo que pertenece a un material
    ''' </summary>
    ''' <param name="sValorText">Valor guardado en VALOR_TEXT. Cod Material</param>
    ''' <param name="sValorTextDen">Valor guardado en VALOR_TEXT_DEN. Den Material</param>
    ''' <returns>El campo a mostrar formateado. Cod Material - Den Material</returns>
    ''' <remarks>Llamada desde: EscribirValorCampo  CargarDesglose; Tiempo máximo: 0 sg;</remarks>
    Private Function DevolverMaterialConCodigoUltimoNivel(ByVal sValorText As String, ByVal sValorTextDen As String) As String
        Dim sMat As String = DBNullToSomething(sValorText)
        Dim arrMat(4) As String
        Dim lLongCod As Integer
        Dim i As Integer

        For i = 1 To 4
            arrMat(i) = ""
        Next i

        i = 1
        While Trim(sMat) <> ""
            Select Case i
                Case 1
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                Case 2
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                Case 3
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                Case 4
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
            End Select
            arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
            sMat = Mid(sMat, lLongCod + 1)
            i = i + 1
        End While

        If arrMat(4) <> "" Then
            Return arrMat(4) + " - " + sValorTextDen
        ElseIf arrMat(3) <> "" Then
            Return arrMat(3) + " - " + sValorTextDen
        ElseIf arrMat(2) <> "" Then
            Return arrMat(2) + " - " + sValorTextDen
        ElseIf arrMat(1) <> "" Then
            Return arrMat(1) + " - " + sValorTextDen
        End If
    End Function

    ''' <summary>
    ''' Quita los decimales si el nº es xx,00 para mostrarlo como un nº entero.
    ''' </summary>
    ''' <param name="sValor">Cadena con el nº</param>
    ''' <returns>Nº modificado</returns>
    ''' <remarks>Llamada desde: EscribirValorCampo  CargarDesglose; Tiempo máximo: 0 sg;</remarks>
    Private Function QuitarDecimalSiTodoCeros(ByVal sValor As String) As String
        Dim decimales As String = ""
        If Usuario.PrecisionFmt > 0 AndAlso Right(sValor, Usuario.PrecisionFmt + 1) = Usuario.DecimalFmt & decimales.PadRight(Usuario.PrecisionFmt, "0") Then
            sValor = Left(sValor, Len(sValor) - Usuario.PrecisionFmt - 1)
        End If
        QuitarDecimalSiTodoCeros = sValor
    End Function
#End Region

#Region "Llamadas javascript"
    ''' <summary>
    ''' Guarda el archivo a disco y llama a download.aspx pasandole el path para mostrarlo o descargarlo
    ''' </summary>
    ''' <param name="Adjuntoid">id del archivo</param>
    ''' <param name="Tipo">Tipo (1: si es un campo normal, 2: el archivo es nuevo, 3: es un campo de un desglose)</param>
    ''' <returns>El enlace para que se abra el archivo con su nombre, path y tamaño.</returns>
    ''' <remarks>Llamada desde: CargarCamposTabla  CargarDesglose; Tiempo máximo: 0 sg;</remarks>
    Private Function ObtenerPathAdjunto(ByVal Adjuntoid As Long, Optional ByVal Tipo As Integer = 1) As String
        Dim sPath As String
        Dim cAdjunto As FSNServer.Adjunto

        cAdjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
        cAdjunto.Id = Adjuntoid
        cAdjunto.LoadInstFromRequest(Tipo)
        If cAdjunto.DataSize = -1000 Then
            cAdjunto.LoadFromRequest(Tipo)
            sPath = cAdjunto.SaveAdjunToDisk(Tipo)
        Else
            sPath = cAdjunto.SaveAdjunToDisk(Tipo, True)
        End If

        Dim arrPath() As String = sPath.Split("\")
        ObtenerPathAdjunto = ConfigurationManager.AppSettings("rutaFS") & "_common/download.aspx?path=" + arrPath(UBound(arrPath) - 1) + "&nombre=" + arrPath(UBound(arrPath)) + "&datasize=" + cAdjunto.dataSize.ToString
    End Function

    ''' <summary>
    ''' Determina la url donde abrir la pantalla para ver una lista de archivos 
    ''' </summary>
    ''' <param name="sValor">Ids de Archivos</param>
    ''' <param name="idCampo">Id de campo donde dejar los cambios de haberlos</param>
    ''' <param name="sInstancia">Instancia actual</param>
    ''' <param name="sTipo">Tipo de archivos</param>
    ''' <returns>url donde mostrar los archivos</returns>
    ''' <remarks>Llamada desde: CargarCamposTabla  CargarDesglose; Tiempo máximo: 0 sg;</remarks>
    Private Function ObtenerAbrirFicherosAdjuntos(ByVal sValor As String, ByVal idCampo As String, ByVal sInstancia As String, ByVal sTipo As String)
        ObtenerAbrirFicherosAdjuntos = System.Configuration.ConfigurationManager.AppSettings("rutaPM") & "workflow/atachedfiles.aspx"
        ObtenerAbrirFicherosAdjuntos = ObtenerAbrirFicherosAdjuntos & "?Valor=" & sValor & "&Valor2="
        ObtenerAbrirFicherosAdjuntos = ObtenerAbrirFicherosAdjuntos & "&Campo=" & idCampo & "&tipo=" & sTipo & "&Instancia=" & sInstancia
        ObtenerAbrirFicherosAdjuntos = ObtenerAbrirFicherosAdjuntos & "&readOnly=1"
    End Function

    ''' <summary>
    ''' Determina la url donde abrir el popup para ver un texto. Todos los dataentry de tipo texto medio,largo y string. 
    ''' </summary>
    ''' <param name="titulo">Titulo para la pantalla popup</param>
    ''' <param name="texto">texto a ver</param>
    ''' <returns>url donde mostrar el texto</returns>
    ''' <remarks>Llamada desde: CargarCamposTabla  CargarDesglose; Tiempo máximo: 0 sg;</remarks>
    Private Function ObtenerAbrirPopUpTexto(ByVal titulo As String, ByVal texto As String)
        ObtenerAbrirPopUpTexto = System.Configuration.ConfigurationManager.AppSettings("rutaPM") & "_common/popUpTexto.aspx"
        ObtenerAbrirPopUpTexto = ObtenerAbrirPopUpTexto & "?titulo=" & titulo & "&texto=" & texto
    End Function

    ''' <summary>
    ''' Determina la url donde abrir la pantalla de partidas presupuestarias
    ''' </summary>
    ''' <param name="titulo">Titulo para la pantalla de partidas presupuestarias</param>
    ''' <param name="pres5">partida presupuestaria</param>
    ''' <returns>url donde mostrar las partidas presupuestarias</returns>
    ''' <remarks>Llamada desde: CargarCamposTabla  CargarDesglose; Tiempo máximo: 0 sg;</remarks>
    Private Function ObtenerAbrirDetallePartida(ByVal titulo As String, ByVal pres5 As String)
        ObtenerAbrirDetallePartida = System.Configuration.ConfigurationManager.AppSettings("rutaPM") & "_common/detallePartida.aspx"
        ObtenerAbrirDetallePartida = ObtenerAbrirDetallePartida & "?titulo=" & titulo
        ObtenerAbrirDetallePartida = ObtenerAbrirDetallePartida & "&pres5=" & pres5
    End Function

    ''' <summary>
    ''' Determina la url donde abrir la pantalla de activos
    ''' </summary>
    ''' <returns>url donde mostrar el activo</returns>
    ''' <remarks>Llamada desde: CargarCamposTabla  CargarDesglose; Tiempo máximo: 0 sg;</remarks>
    Private Function ObtenerAbrirDetalleActivo()
        ObtenerAbrirDetalleActivo = System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "sm/detalleactivo.aspx"
        ObtenerAbrirDetalleActivo = ObtenerAbrirDetalleActivo
    End Function
#End Region

End Class