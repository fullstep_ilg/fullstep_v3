﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DetallePuntuacionEncuesta.aspx.vb" Inherits="Fullstep.FSNWeb.DetallePuntuacionEncuesta"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head  id="Head1" runat="server">
	<title></title>
	<style type="text/css">
		.PieGridOverflowVisible{overflow:visible !important;}
		.itemBold {font-weight:bold;}
	</style>               
</head>
<body class="FondoTablaDetallePuntuacion">
	<form id="form1" runat="server">
		<asp:ScriptManager ID="ScriptManager1" runat="server">
			<CompositeScript>
				<Scripts>
					<asp:ScriptReference Path="~/js/jquery/jquery.min.js" />
					<asp:ScriptReference Path="~/js/jquery/jquery.tmpl.min.js" />
					<asp:ScriptReference Path="~/js/jquery/jquery-migrate.min.js" />
					<asp:ScriptReference Path="~/js/jsUtilities.js" />
					<asp:ScriptReference Path="js/detallePuntuacionEncuesta.js" />
				</Scripts>
			</CompositeScript>
		</asp:ScriptManager>    
		
		<asp:Image ID="imgDetalle" runat="server" SkinID="PopUpProveedor" style="position:absolute; top:1em; left:1.2em;" />
		<div class="CapaTituloPanelInfo" style="line-height:3.5em;">
			<asp:Label runat="server" ID="lblVarCodDen" CssClass="TituloPopUpPanelInfo" style="margin-left:5em;"></asp:Label>
            <div style="float:right;">
			    <label style="margin-right:2em;">
				    <asp:ImageButton ID="btnImprimir" runat="server" SkinID="Imprimirph" OnClientClick="window.print();return false;"/> 
				    <a runat="server" id="lblImprimir" class="TextoBlanco" href="javascript:window.print();">DImprimir</a>
			    </label>            
            </div>
		</div>
		<div class="CapaSubTituloPanelInfo" style="line-height:2em;">
			<asp:Label runat="server" ID="lblDetalle" CssClass="SubTituloPopUpPanelInfo" style="margin-left:7em;"></asp:Label>
		</div>
		<div class="FondoTablaDetallePuntuacion" style="margin:1.5em;">
			<asp:Label ID="lblDetPuntProve" runat ="server" CssClass="Texto14 TextoOscuro" style="margin-right:0.3em;"></asp:Label>
			<asp:Label ID="lblDetPuntProveDat" runat ="server" CssClass="Texto14 TextoOscuro Negrita" style="margin-right:0.3em;"></asp:Label>
			<asp:Label ID="lblEnUni" runat ="server" CssClass="Texto14 TextoOscuro" style="margin-right:0.3em;"></asp:Label>
			<asp:Label ID="lblEnUniDat" runat ="server" CssClass="Texto14 TextoOscuro Negrita"></asp:Label>
		</div>
		<div class="BordearDetallePuntuacion" style="display:inline-block; padding:0.2em;">
			<asp:Label ID="lblPunt" runat ="server"  CssClass="RotuloDetallePuntuacionBoldLeft"></asp:Label>
			<asp:Label ID="lblPuntDat" runat ="server" CssClass="RotuloDetallePuntuacionPuntCalf"></asp:Label>
		</div>
		<div class="FondoGrisTablaDetallePuntuacion" style="margin-top:1.5em; padding:0.5em;">
			<asp:Panel id="pnlPadre" runat="server" ScrollBars="None" style="line-height:1.5em;">
				<table id="tblPadre" runat="server" border="0" style="width:100%; line-height:2em;" cellpadding="0" cellspacing="0">
				</table>
			</asp:Panel>
			<div style="line-height:2em;">
				<asp:Label ID="lblMatQa" runat ="server" CssClass="RotuloDetallePuntuacionFondoGrisBold" style="margin-right:0.3em;"></asp:Label>
				<asp:Label ID="lblMatQaDat" runat ="server"></asp:Label>
			</div>
			<div style="line-height:2em;">
				<asp:Label ID="lblOrigen" runat ="server" CssClass="RotuloDetallePuntuacionFondoGrisBold"></asp:Label>
				<asp:Label ID="lblOrigenDat" runat ="server"></asp:Label>
			</div>
		</div>
		<div style="margin-top:1.5em; padding:0.5em;" class="FondoGrisTablaDetallePuntuacion">                                
			<div class="RotuloDetallePuntuacionFondoGrisBold" style="line-height:1.5em;">
				<asp:Label runat="server" ID="lblFormula"></asp:Label>
			</div>                                                                     
			<div class="RotuloDetallePuntuacionFondoGrisPad" style="line-height:1.5em; overflow:auto">
				<asp:Label runat="server" ID="lblFormulaTexto"></asp:Label>
			</div>                               
		</div>
		<div class="FondoGrisTablaDetallePuntuacion" style="margin-top:1.5em; padding:0.5em; height:15em; overflow:auto;">
			<asp:Repeater runat="server" ID="rptVariables">
				<ItemTemplate>                                                    
					<div style="line-height:2.5em;">
						<asp:Label runat="server" ID="lblCod" class="RotuloDetallePuntuacionFondoGrisBoldNoPad" style="display:inline-block; width:5em;" Text='<%#DataBinder.Eval(Container.DataItem, "VARIABLEX")%>'></asp:Label>                                                         
						<asp:Label runat="server" ID="lblDen" class="RotuloDetallePuntuacionFondoGrisBoldNoPad" Text='<%#DataBinder.Eval(Container.DataItem, "DEN")%>'></asp:Label>
					</div>                                                    
				</ItemTemplate>
			</asp:Repeater>
		</div>
		<div class="FondoTablaDetallePuntuacion" style="margin-top:1.5em; padding:0.5em;">
			<asp:Label runat="server" ID="lblEncuestas" CssClass="Texto12 Negrita"></asp:Label>
			<div style="position:relative; height:6em;">
				<div id="divBotonesGrid" style="position:absolute; top:0em; left:0em; bottom:0em; width:2em;">
					<div class="fp_celda_vc_1_DetallePuntuacion Negrita CollapsiblePanelContent" style="text-align:center; height:2em;"></div>
				</div>
				<div id="divDatosGrid" style="position:absolute; left:2em; right:8em; overflow:auto;">
					<table id="tblValoresCampo" style="border-spacing:0em; padding:0em;">
						<tr class="fp_celda_vc_1_DetallePuntuacion Negrita" style="line-height:1.8em;">
							<td class="CollapsiblePanelContent" style="width:8em; white-space:nowrap;"></td>
							<td class="CollapsiblePanelContent" style="width:10em; white-space:nowrap;"></td>
							<td class="CollapsiblePanelContent" style="width:14em; white-space:nowrap;"></td>
							<td class="CollapsiblePanelContent" style="width:14em; white-space:nowrap;"></td>
							<td class="CollapsiblePanelContent" style="width:14em; white-space:nowrap;"></td>
						</tr>
					</table>
				</div>
				<div id="divPuntuacionGrid" style="position:absolute; top:0em; right:0em; bottom:0em; width:8em;">
					<div class="fp_celda_vc_1_DetallePuntuacion Negrita CollapsiblePanelContent" style="text-align:center; line-height:2em;"></div>
				</div>                
			</div>           
			<div id="div_Label" runat="server" class="EtiquetaScroll" style="display:none;position:absolute">
				<asp:Label ID="label2" runat="server" Font-Bold="true"></asp:Label>
			</div>
		</div>          
	</form>
</body>
</html>
<script id="divPuntos" type="text/x-jquery-tmpl">
	<div id="img${idEncuesta}" class="fp_celda_vc_1_DetallePuntuacion Negrita CollapsiblePanelContent" style="text-align:center; cursor:pointer; height:2em; background:url(../../../Images/abrir_ptos_susp.gif) no-repeat center center"></div>
</script>
<script id="tdCabeceraCampo" type="text/x-jquery-tmpl">
	<td class="CollapsiblePanelContent" style="width:5em; white-space:nowrap;">${nombreValor}</td>
</script>
<script id="trValorCampo" type="text/x-jquery-tmpl">
	<tr style="line-height:2em;">
		{{each valoresCampo}}
			{{tmpl($value) getPlantillaAlineacion($index)}}
		{{/each}} 
	</tr>
</script>
<script id="tdValorCampoDerecha" type="text/x-jquery-tmpl">
	<td class="CollapsiblePanelContent" style="white-space:nowrap; padding:0em 0.5em;">${valorCampo}</td>
</script>
<script id="tdValorCampoIzquierda" type="text/x-jquery-tmpl">
	<td class="CollapsiblePanelContent" style="white-space:nowrap; text-align:right; padding:0em 0.5em;">${valorCampo}</td>
</script>
<script id="divPuntuacion" type="text/x-jquery-tmpl">
	<div class="fp_celda_vc_1_DetallePuntuacion Texto12 TextoOscuro Negrita CollapsiblePanelContent" style="text-align:right; line-height:2em; padding-right:0.5em;">${totalValor}</div>
</script>
<script id="trValorCampoVacio" type="text/x-jquery-tmpl">
	<tr style="line-height:2em;"><td colspan="${colspan}">&nbsp;</td></tr>
</script>
<script id="divTotalPuntuacion" type="text/x-jquery-tmpl">
	<div colspan="${colspan}" class="Texto14 TextoOscuro Negrita" style="position:absolute; left:-10em; line-height:2em;">${texto}</div>
	<div colspan="${colspan}" class="Texto14 TextoOscuro Negrita SeparadorRight SeparadorLeft SeparadorInf" style="left:0em; line-height:2em; text-align:right; padding-right:0.5em;">${totalPuntuacion}</div>
</script>