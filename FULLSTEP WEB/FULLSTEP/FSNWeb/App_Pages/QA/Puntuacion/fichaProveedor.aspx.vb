﻿Imports System.Web.Script.Serialization
Imports Fullstep
Imports Fullstep.FSNLibrary
Partial Public Class fichaProveedor
    Inherits FSNPage
    Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
    Private Const TODOS_MATQA As String = "TODOS"
    Private bExportando As Boolean = False
    Private _FiltroVarCal As Collection
    Private _FiltroUNQA As Collection
    Private _ProveCod As String
    Private _ProveDen As String
    Private _ProveCif As String
    Private _ProveEmail As String
    Private _ProveMatQA As String
    Private _HayError As Nullable(Of Boolean)
    Private _dsFichaProveedor As DataSet
#Region "Propiedades"
    ''' <summary>
    ''' Propiedad que contiene el dataset con la ficha de calidad del proveedor.
    ''' Si es la primera carga de la página obtiene los datos de BD y los cachea
    ''' </summary>
    ''' <value></value>
    ''' <returns>DataSet´</returns>
    ''' <remarks></remarks>
    Protected ReadOnly Property dsFichaProveedor() As DataSet
        Get
            If _dsFichaProveedor Is Nothing Then
                If Me.IsPostBack Then
                    _dsFichaProveedor = CType(Cache("dsFichaProveedor_" & FSNUser.Cod), DataSet)
                End If
                If _dsFichaProveedor Is Nothing Then
                    ObtenerDataSetFichaProveedor()
                    Me.InsertarEnCache("dsFichaProveedor_" & FSNUser.Cod, _dsFichaProveedor)
                End If
            End If
            Return _dsFichaProveedor
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que contiene la fecha de actualizaciÃ³n de las puntuaciones y la almacena en el ViewState.
    ''' </summary>
    ''' <value></value>
    ''' <returns>String con la fecha</returns>
    ''' <remarks></remarks>
    Protected Property FechaPuntuacion() As String
        Get
            Return ViewState("FechaPuntuacion")
        End Get
        Set(ByVal value As String)
            ViewState("FechaPuntuacion") = value
        End Set
    End Property
    Protected ReadOnly Property FiltroVarCal() As Collection
        Get
            If _FiltroVarCal Is Nothing Then
                _FiltroVarCal = Session("CFiltroVarCal")
                If _FiltroVarCal Is Nothing Then
                    Response.Redirect("panelCalidadFiltro.aspx")
                End If
            End If
            Return _FiltroVarCal
        End Get
    End Property
    Protected ReadOnly Property FiltroUNQA() As Collection
        Get
            If _FiltroUNQA Is Nothing Then
                _FiltroUNQA = Session("CFiltroUNQA")
                If _FiltroUNQA Is Nothing Then
                    Response.Redirect("panelCalidadFiltro.aspx")
                End If
            End If
            Return _FiltroUNQA
        End Get
    End Property
    Protected ReadOnly Property ProveCod() As String
        Get
            If _ProveCod Is Nothing Then
                _ProveCod = Request.Form(Request.QueryString("h") & "hProveCod")

                If _ProveCod Is Nothing Then
                    _ProveCod = Session("ProveCod")
                Else
                    Session("ProveCod") = _ProveCod
                End If
            End If
            Return _ProveCod
        End Get
    End Property
    Protected ReadOnly Property ProveDen() As String
        Get
            If _ProveDen Is Nothing Then
                _ProveDen = Request.Form(Request.QueryString("h") & "hProveDen")

                If _ProveDen Is Nothing Then
                    _ProveDen = Session("ProveDen")
                Else
                    Session("ProveDen") = _ProveDen
                End If
            End If
            Return _ProveDen
        End Get
    End Property
    Protected ReadOnly Property HayError() As String
        Get
            If _HayError Is Nothing Then
                If Request.Form(Request.QueryString("h") & "hHayError") Is Nothing Then
                    _HayError = Session("HayError")
                Else
                    _HayError = CType(strToNothing(Request.Form(Request.QueryString("h") & "hHayError")), Boolean)
                    Session("HayError") = _HayError
                End If
            End If
            Return _HayError
        End Get
    End Property
    Protected ReadOnly Property ProveCif() As String
        Get
            If _ProveCif Is Nothing Then
                _ProveCif = Request.Form(Request.QueryString("h") & "hProveCif")

                If _ProveCif Is Nothing Then
                    _ProveCif = Session("ProveCif")
                Else
                    Session("ProveCif") = _ProveCif
                End If
            End If
            Return _ProveCif
        End Get
    End Property
    Protected ReadOnly Property ProveEmail() As String
        Get
            If _ProveEmail Is Nothing Then
                _ProveEmail = Request.Form(Request.QueryString("h") & "hProveEmail")

                If _ProveEmail Is Nothing Then
                    _ProveEmail = Session("ProveEmail")
                Else
                    Session("ProveEmail") = _ProveEmail
                End If
            End If
            Return _ProveEmail
        End Get
    End Property
    Protected ReadOnly Property ProveMatQA() As String
        Get
            If _ProveMatQA Is Nothing Then
                _ProveMatQA = Request.Form(Request.QueryString("h") & "hProveMatQA")

                If _ProveMatQA Is Nothing Then
                    _ProveMatQA = Session("ProveMatQA")
                Else
                    Session("ProveMatQA") = _ProveMatQA
                End If

                If _ProveMatQA Is Nothing Then
                    Response.Redirect("panelCalidadFiltro.aspx")
                End If
            End If
            Return _ProveMatQA
        End Get
    End Property
    Private Shared _columnasVisiblesCliente As List(Of String)
#End Region
    Private Sub fichaProveedor_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim mMasterTotal As FSNWeb.Cabecera = CType(CType(Me.Master, FSNWeb.Menu).Master, FSNWeb.Cabecera)
        CType(mMasterTotal.FindControl("ScriptManager1"), ScriptManager).EnableScriptGlobalization = True
    End Sub
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Procedimiento de carga de la página. Establece el módulo de idioma, los textos de los botones y 
    ''' etiquetas, establece a q pagina y con q orden de grid de panelCalidad debo volver.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("cn") IsNot Nothing Then
            ObtenerValoresFichaCalidad(Request.QueryString("PROVECOD"))
        End If
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.FichaProveedor

        If Not IsPostBack Then
            Dim mMaster = CType(Me.Master, FSNWeb.Menu)
            mMaster.Seleccionar("Calidad", "PanelProveedores")

            FSNPageHeader.TituloCabecera = Textos(24)
            FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/filtro_punt_calidad.gif"
            ''volver

            If Request.QueryString("cn") IsNot Nothing Then
                FSNPageHeader.VisibleBotonVolverColaboracion = True
                FSNPageHeader.TextoBotonVolverColaboracion = Textos(57)
                FSNPageHeader.OnClientClickVolverColaboracion = "window.location.href='" &
                    System.Configuration.ConfigurationManager.AppSettings("rutaFS") &
                    "cn/cn_MuroUsuario.aspx?tipoMensajes=" & Request.QueryString("tipoMensajes") &
                    "&pagina=" & Request.QueryString("pagina") & "&scrollpagina=" & Request.QueryString("pagina") & "'; return false;"
            Else
                FSNPageHeader.VisibleBotonVolver = True
                FSNPageHeader.TextoBotonVolver = Textos(10)
                FSNPageHeader.OnClientClickVolver = "return Volver();"
            End If

            Dim bVerNotifs As Boolean = False
            For Each tipo As EntidadNotificacion In FSNUser.getEntidadesNotificacion
                If tipo = EntidadNotificacion.Calificaciones Then
                    bVerNotifs = True
                    Exit For
                End If
            Next

            imgVisor.Visible = bVerNotifs
            lblVisor.Visible = bVerNotifs

            imgVisor.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/enviar_email_color.gif"
            imgEmail.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/enviar_email_color.gif"
            imgExcel.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Excel.png"
            imgPDF.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/PDF.png"

            lblVisor.Text = Textos(54)
            lblExcel.Text = Textos(55)
            lblPDF.Text = Textos(56)

            If FSNUser.AccesoCN Then
                If Not Page.ClientScript.IsClientScriptBlockRegistered("EntidadColaboracion") Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "EntidadColaboracion",
                        "var TipoEntidadColaboracion='FP';" &
                        "var CodigoEntidadColaboracion ={identificador:'" & ProveCod &
                                                        "',texto:'" & JSText(ProveCod & "-" & ProveDen) & "'" &
                                                        ",proveedor:'" & JSText(ProveDen) & "'};", True)
                End If
            End If

            lblProveedor.Text = ProveCod & "-" & ProveDen & " " & Textos(0) & ProveCif

            imgDetalleProve.ContextKey = ProveCod

            ibDetalleProve.AlternateText = Textos(6)

            lblEnviarEMail.Text = Textos(11)
            lnkRecalcular.Text = Textos(7)
            lnkAlmacenar.Text = Textos(8)
            hProveEmail.Value = ProveEmail
            hProveCod.Value = ProveCod
            hProveDen.Value = ProveDen

            PaginaVolver.Value = Request.Form(Request.QueryString("h") & "PaginaVolver")
            OrdenVolver.Value = Request.Form(Request.QueryString("h") & "hOrdenar")
            OrdenacionVolver.Value = Request.Form(Request.QueryString("h") & "hOrdenarSentido")

            imgErrorCalculoPuntuaciones.Visible = HayError
        Else
            If Request("__EVENTARGUMENT") = "Excel" Then
                ExportarExcel()
            ElseIf Request("__EVENTARGUMENT") = "Pdf" Then
                ExportarPdf()
            End If
        End If
        NumericEditor.EditorControl.Culture = System.Globalization.CultureInfo.CurrentCulture
        whdgProveedores_InitializeDataSource()
        whdgProveedores.DataBind()
        ConfigurarGrid()
        upWhdgProveedores.Update()

        Dim sMSIE7 As String = "var bMSIE7="
        If Request.Browser("Browser") = "IE" AndAlso (Request.Browser("MajorVersion") < 8) Then
            sMSIE7 = String.Format(IncludeScriptKeyFormat, "javascript", sMSIE7 & "true;")
        Else
            sMSIE7 = String.Format(IncludeScriptKeyFormat, "javascript", sMSIE7 & "false;")
        End If
        Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "sMSIE7", sMSIE7)
        Dim ModalProgressClientID As String = CType(Master.Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalClientID", "<script>var ModalProgress = '" & ModalProgressClientID & "' </script>")
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
            Dim sVariableJavascriptTextosPantalla As String = "var TextosPantalla = new Array();"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[0]='" & JSText(Textos(58)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[1]='" & JSText(Textos(59)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[2]='" & JSText(Textos(60)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[3]='" & JSText(Textos(61)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[4]='" & JSText(Textos(62)) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextosPantalla, True)
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "NumberFormatUsu") Then
            With FSNUser
                Dim sVariablesJavascript As String = "var UsuNumberDecimalSeparator = '" & .NumberFormat.NumberDecimalSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberGroupSeparator='" & .NumberFormat.NumberGroupSeparator & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuNumberNumDecimals='" & .NumberFormat.NumberDecimalDigits & "';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NumberFormatUsu", sVariablesJavascript, True)
            End With
        End If
    End Sub
    ''' <summary>
    ''' Si no viene de la pantalla de filtros, sino de CN, cargamos los valores necesarios para cargar la información 
    ''' de la ficha de calidad del proveedor
    ''' </summary>
    ''' <param name="ProveCod">Código del proveedor</param>
    ''' <remarks></remarks>
    Private Sub ObtenerValoresFichaCalidad(ByVal ProveCod As String)
        Dim dsDatos As DataSet
        Dim PMUnidadesQA As FSNServer.UnidadesNeg = FSNServer.Get_Object(GetType(FSNServer.UnidadesNeg))
        dsDatos = PMUnidadesQA.UsuGetData_hds(FSNUser.Cod, TipoAccesoUNQAS.ConsultarPuntuacionesPanelCalidad,
                                              TipoAccesoUNQAS.ModificarPuntuacionesPanelCalidad, FSNUser.Idioma, FSNUser.Pyme)
        Dim IdGrupo As Integer = dsDatos.Tables(1).Rows(0)("ID")
        Session("CFiltroUNQA") = ObtenerFiltroUNQA(dsDatos)

        Dim PMVariablesCalidad As FSNServer.VariablesCalidad = FSNServer.Get_Object(GetType(FSNServer.VariablesCalidad))
        dsDatos = PMVariablesCalidad.GetData_hds(FSNUser.Cod, FSNUser.Idioma, FSNUser.Pyme)
        Dim IdVarCal0 As Integer = dsDatos.Tables(1).Rows(0)("ID")
        Session("CFiltroVarCal") = ObtenerFiltroVarCal(dsDatos)

        Dim PMProveedores As FSNServer.Proveedores = FSNServer.Get_Object(GetType(FSNServer.Proveedores))
        dsDatos = PMProveedores.DevolverPanelCalidad(FSNUser.Cod, New DataTable, "", IdGrupo, IdVarCal0 & "-0", 1, ProveCod, 0,
                    FSNUser.Idioma, FSNUser.QARestProvContacto, FSNUser.QARestProvMaterial, FSNUser.QARestProvEquipo)
        If Not dsDatos.Tables(0).Rows.Count = 0 Or Not dsDatos.Tables(2).Rows.Count = 0 Then
            Dim sMatQA As String = ","
            For Each item As DataRow In dsDatos.Tables(2).Rows
                sMatQA &= item("ID") & ","
            Next
            Session("ProveMatQA") = sMatQA

            With dsDatos.Tables(0)
                Session("ProveCod") = .Rows(0)("PROVE_COD")
                Session("ProveDen") = .Rows(0)("PROVE_NAME")
                Session("ProveCif") = .Rows(0)("PROVE_CIF")
                Session("ProveEmail") = .Rows(0)("CON_EMAIL")
                Session("HayError") = CType(.Rows(0)("HAYERROR"), Boolean)
            End With
        End If
    End Sub
    ''' <summary>
    ''' Datos de las unqa para la ficha de calidad
    ''' </summary>
    ''' <param name="dsDatosUNQA">Datos de las unqa</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerFiltroUNQA(ByVal dsDatosUNQA As DataSet) As Collection
        Dim cfiltroUNQA As New Collection
        Dim cfiltro As New Collection()
        If Not dsDatosUNQA.Tables(1).Rows.Count = 0 Then
            With dsDatosUNQA.Tables(1)
                cfiltro.Add(.Rows(0)("ID"), "ID")
                cfiltro.Add(0, "NIVEL")
                cfiltro.Add(.Rows(0)("DEN"), "DEN")
                cfiltro.Add(True, "FILTRO")
                cfiltro.Add(.Rows(0)("ACCESO"), "PERMISO")
                cfiltroUNQA.Add(cfiltro, .Rows(0)("ID"))
            End With
        End If
        ObtenerFiltroUNQAHijos(cfiltroUNQA, dsDatosUNQA.Tables(0), dsDatosUNQA.Tables(1).Rows(0)("ID"), 1)
        Return cfiltroUNQA
    End Function
    ''' <summary>
    ''' Rellenamos el arbol de los hijos de las unqa
    ''' </summary>
    ''' <param name="cfiltroUNQA">Colección de las unqa para la ficha de calidad</param>
    ''' <param name="dtDatosUNQA">Datos de las unqa</param>
    ''' <param name="IdPadre">IdPadre de los hijos a crear</param>
    ''' <param name="Nivel">Nivel de los hijos a crear</param>
    ''' <remarks></remarks>
    Private Sub ObtenerFiltroUNQAHijos(ByRef cfiltroUNQA As Collection, ByVal dtDatosUNQA As DataTable,
                                       ByVal IdPadre As Integer, ByVal Nivel As Integer)
        Dim cfiltro As Collection
        Dim sHijos As String = String.Empty
        For Each item As DataRow In dtDatosUNQA.Select("PADRE" & IIf(Nivel = 1, " IS NULL", "=" & IdPadre))
            If Not item("ACCESO") = 0 Then
                cfiltro = New Collection()
                cfiltro.Add(item("ID"), "ID")
                cfiltro.Add(Nivel, "NIVEL")
                cfiltro.Add(item("DEN"), "DEN")
                cfiltro.Add(False, "FILTRO")
                cfiltro.Add(item("ACCESO"), "PERMISO")
                cfiltroUNQA.Add(cfiltro, item("ID"))

                sHijos = sHijos & IIf(sHijos Is String.Empty, "", ",") & item("ID")
                ObtenerFiltroUNQAHijos(cfiltroUNQA, dtDatosUNQA, item("ID"), Nivel + 1)
            End If
        Next
        cfiltroUNQA.Item(IdPadre.ToString).Add(sHijos, "HIJOS")
    End Sub
    ''' <summary>
    ''' Datos de las variables de calidad para la ficha de calidad
    ''' </summary>
    ''' <param name="dsDatosUNQA">Datos de las variables de calidad</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerFiltroVarCal(ByVal dsDatosUNQA As DataSet) As Collection
        Dim cfiltroVarCal As New Collection
        Dim cfiltro As New Collection()
        With dsDatosUNQA.Tables(1)
            cfiltro.Add(.Rows(0)("ID"), "ID")
            cfiltro.Add("0", "NIVEL")
            cfiltro.Add(.Rows(0)("DEN"), "DEN")
            cfiltro.Add(True, "FILTRO")
            cfiltro.Add("TODOS", "MATQA")
            cfiltroVarCal.Add(cfiltro, .Rows(0)("ID").ToString)

            ObtenerFiltroVarCalHijos(cfiltroVarCal, dsDatosUNQA.Tables(0), .Rows(0)("ID"), 1)
        End With

        Return cfiltroVarCal
    End Function
    ''' <summary>
    ''' Rellenamos el arbol de los hijos de las varcal
    ''' </summary>
    ''' <param name="cfiltroVarCal">Colección de las unqa para la ficha de calidad</param>
    ''' <param name="dtDatosVarCal">Datos de las varcal</param>
    ''' <param name="IdPadre">IdPadre de los hijos a crear</param>
    ''' <param name="Nivel">Nivel de los hijos a crear</param>
    ''' <remarks></remarks>
    Private Sub ObtenerFiltroVarCalHijos(ByRef cfiltroVarCal As Collection, ByVal dtDatosVarCal As DataTable,
                                       ByVal IdPadre As String, ByVal Nivel As Integer)
        Dim cfiltro As Collection
        Dim sHijos As String = String.Empty
        Dim TieneHijos As Boolean = False
        Dim sPadreMatQA, sMatQA As String
        sMatQA = String.Empty
        For Each item As DataRow In dtDatosVarCal.Select("PADRE" & IIf(Nivel = 1, " IS NULL", "='" & IdPadre & "'"))
            TieneHijos = True
            cfiltro = New Collection()
            cfiltro.Add(Split(item("ID"), "-")(0), "ID")
            cfiltro.Add(Split(item("ID"), "-")(1), "NIVEL")
            cfiltro.Add(item("DEN"), "DEN")
            cfiltro.Add(False, "FILTRO")
            cfiltro.Add(item("MATERIAL_QA").ToString, "PERMISO")
            cfiltro.Add(item("MATERIAL_QA").ToString, "MATQA")
            sMatQA = IIf(IsDBNull(item("MATERIAL_QA")), "TODOS", item("MATERIAL_QA"))
            cfiltroVarCal.Add(cfiltro, item("ID"))

            sHijos = sHijos & IIf(sHijos Is String.Empty, "", ",") & item("ID")

            sPadreMatQA = cfiltroVarCal.Item(IdPadre.ToString)("MATQA")
            If dtDatosVarCal.Select("PADRE='" & item("ID") & "'").Length = 0 Then
                If cfiltroVarCal.Item(item("ID"))("MATQA") Is String.Empty Then
                    cfiltroVarCal.Item(item("ID")).REMOVE("MATQA")
                    cfiltroVarCal.Item(item("ID")).ADD(sMatQA, "MATQA")
                    cfiltroVarCal.Item(item("ID")).REMOVE("PERMISO")
                    cfiltroVarCal.Item(item("ID")).ADD(sMatQA, "PERMISO")
                End If
                If InStr("_" & sPadreMatQA & "_", "_" & sMatQA & "_") = 0 Then
                    cfiltroVarCal.Item(IdPadre.ToString).REMOVE("MATQA")
                    cfiltroVarCal.Item(IdPadre.ToString).ADD(IIf(sPadreMatQA Is String.Empty, sMatQA, sPadreMatQA & "_" & sMatQA), "MATQA")
                    cfiltroVarCal.Item(IdPadre.ToString).REMOVE("PERMISO")
                    cfiltroVarCal.Item(IdPadre.ToString).ADD(IIf(sPadreMatQA Is String.Empty, sMatQA, sPadreMatQA & "_" & sMatQA), "PERMISO")
                End If
            Else
                ObtenerFiltroVarCalHijos(cfiltroVarCal, dtDatosVarCal, item("ID"), Nivel + 1)
                If Not Nivel = 1 Then
                    For Each MatQA As String In cfiltroVarCal.Item(item("ID"))("MATQA").ToString.Split("_")
                        If InStr("_" & sPadreMatQA & "_", "_" & MatQA & "_") = 0 Then
                            cfiltroVarCal.Item(IdPadre.ToString).REMOVE("MATQA")
                            cfiltroVarCal.Item(IdPadre.ToString).ADD(IIf(sPadreMatQA Is String.Empty, sMatQA, sPadreMatQA & "_" & sMatQA), "MATQA")
                            cfiltroVarCal.Item(IdPadre.ToString).REMOVE("PERMISO")
                            cfiltroVarCal.Item(IdPadre.ToString).ADD(IIf(sPadreMatQA Is String.Empty, sMatQA, sPadreMatQA & "_" & sMatQA), "PERMISO")
                        End If
                    Next
                End If
            End If
        Next
        cfiltroVarCal.Item(IdPadre.ToString).Add(sHijos, "HIJOS")
    End Sub
    ''' <summary>
    ''' Obtiene un dataset con las puntuaciones de todas las UNQAs a las que el usuario tiene acceso
    ''' y de todas las Variables de calidad a las que el usuario tiene acceso y que puntuan para el proveedor
    ''' y genera la tabla que conforma la ficha con un registro por cada variable de calidad, y cada regsitro con las columna de puntuaciones, calificaciones y fechas de las puntauciones de calidad por cada unidad de negocio
    ''' </summary>
    ''' <remarks></remarks>
    Sub ObtenerDataSetFichaProveedor()
        Dim PMProveedor As FSNServer.Proveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
        'Obtengo las puntuaciones de todas las UNQAs a las que el usuario tiene acceso
        Dim sUNQA As String = ""
        For Each UNQA As Collection In FiltroUNQA
            sUNQA = sUNQA & IIf(sUNQA = "", "", ",") & UNQA("ID")
        Next
        'Obtengo las puntuaciones de todas las Variables de calidad a las que el usuario tiene acceso y que puntuan para el proveedor
        Dim sVarCal As String = ""
        For Each VarCal As Collection In FiltroVarCal
            Dim aMateriales() As String = Split(VarCal("MATQA"), "_")
            Dim bEncontrado As Boolean = False
            For iMat As Integer = 0 To aMateriales.Length - 1
                If aMateriales(iMat) = TODOS_MATQA Then
                    bEncontrado = True
                    Exit For
                End If
                If ProveMatQA.IndexOf("," & aMateriales(iMat) & ",") <> -1 Then
                    bEncontrado = True
                    Exit For
                End If
            Next
            If bEncontrado Then
                sVarCal = sVarCal & IIf(sVarCal = "", "", ",") & VarCal("ID") & "-" & VarCal("NIVEL")
            End If
        Next

        _dsFichaProveedor = PMProveedor.DevolverFichaCalidad(ProveCod, Me.FSNUser.Cod, sUNQA, sVarCal, Me.Idioma)
        If _dsFichaProveedor.Tables.Count > 0 Then
            ObtenerTablaFichaProveedor()
        End If
    End Sub
    ''' <summary>
    ''' Genera una tabla que necesitamos para mostrar la ficha de calidad. Un regsitro por cada variable de calidad, y cada regsitro con columnas de puntuación/calificación/fecha act por cada unidad de negocio
    ''' Una vez generada la tabla la añade al dataset _dsFichaProveedor y la rellena con los datos obtenidos de BD
    ''' </summary>
    ''' <remarks></remarks>
    Sub ObtenerTablaFichaProveedor()
        'Creamos la tabla que necesitamos para mostrar la ficha de calidad
        Dim dtVariables As DataTable = New DataTable("MaestroVariables")
        Dim dRow As DataRow
        Dim dColumna As DataColumn

        'DataView para buscar si las var. se aplican al proveedor. Se buscan en el dataset de puntuaciones. Si no están es que no aplican
        Dim dvFichaProve As New DataView(_dsFichaProveedor.Tables(0), "", "VARCAL,NIVEL", DataViewRowState.CurrentRows)

        dColumna = New DataColumn("VC_ID", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("VC_NIVEL", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dtVariables.PrimaryKey = New DataColumn() {dtVariables.Columns("VC_ID"), dtVariables.Columns("VC_NIVEL")}
        dColumna = New DataColumn("VC_DEN", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("FORMULA", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("LEYENDA", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("TIPO", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("SUBTIPO", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("MODIFICAR", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("OBSERVACIONES", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        'añadimos una columna por cada UNQA
        For Each UNQA As Collection In FiltroUNQA
            dColumna = New DataColumn("PUNT_" & UNQA("ID"), System.Type.GetType("System.Double"))
            dtVariables.Columns.Add(dColumna)
            dColumna = New DataColumn("CAL_" & UNQA("ID"), System.Type.GetType("System.String"))
            dtVariables.Columns.Add(dColumna)
            dColumna = New DataColumn("FECACT_" & UNQA("ID"), System.Type.GetType("System.DateTime"))
            dtVariables.Columns.Add(dColumna)
        Next
        'Añadimos las filas con las variables de calidad que el usuario puede ver y que afectan al proveedor
        For Each VarCal As Collection In FiltroVarCal
            Dim aMateriales() As String = Split(VarCal("MATQA"), "_")
            Dim bEncontrado As Boolean = False
            For iMat As Integer = 0 To aMateriales.Length - 1
                If aMateriales(iMat) = TODOS_MATQA Then
                    bEncontrado = True
                    Exit For
                End If
                If ProveMatQA.IndexOf("," & aMateriales(iMat) & ",") <> -1 Then
                    bEncontrado = True
                    Exit For
                End If
            Next
            If bEncontrado Then
                'Mirar si aplica
                If dvFichaProve.Find(New Object() {VarCal("ID"), VarCal("NIVEL")}) > -1 Then
                    dRow = dtVariables.NewRow
                    dRow("VC_ID") = VarCal("ID")
                    dRow("VC_NIVEL") = VarCal("NIVEL")
                    dRow("VC_DEN") = VarCal("DEN")
                    dtVariables.Rows.Add(dRow)
                End If
            End If
        Next
        _dsFichaProveedor.Tables.Add(dtVariables)
        _dsFichaProveedor.Relations.Add("REL_VAR_PUNT", New DataColumn() {_dsFichaProveedor.Tables("MaestroVariables").Columns("VC_ID"), _dsFichaProveedor.Tables("MaestroVariables").Columns("VC_NIVEL")}, New DataColumn() {_dsFichaProveedor.Tables(0).Columns("VARCAL"), _dsFichaProveedor.Tables(0).Columns("NIVEL")}, False)
        'Recorremos la tabla de variables de calidad y obtenemos sus puntuaciones por cada unqa
        Dim lObservaciones As Dictionary(Of String, String)
        Dim serializer As New JavaScriptSerializer
        For Each Row As DataRow In dtVariables.Rows
            lObservaciones = New Dictionary(Of String, String)
            For Each RowPuntuacion As DataRow In Row.GetChildRows("REL_VAR_PUNT")
                Row("FORMULA") = RowPuntuacion("FORMULA")
                Row("LEYENDA") = RowPuntuacion("LEYENDA")
                Row("TIPO") = RowPuntuacion("TIPO")
                Row("SUBTIPO") = RowPuntuacion("SUBTIPO")
                Row("MODIFICAR") = RowPuntuacion("MODIFICAR")
                Row("PUNT_" & RowPuntuacion("UNQA")) = RowPuntuacion("PUNT")
                Row("CAL_" & RowPuntuacion("UNQA")) = RowPuntuacion("CALIFICACION")
                Row("FECACT_" & RowPuntuacion("UNQA")) = RowPuntuacion("FECACT")
                If Not String.IsNullOrEmpty(RowPuntuacion("OBSERVACIONES").ToString) Then
                    lObservaciones("PUNT_" & RowPuntuacion("UNQA")) = RowPuntuacion("OBSERVACIONES")
                End If
                Dim _MesPuntuacion As Integer
                If FechaPuntuacion Is Nothing Then
                    If Not IsDBNull(RowPuntuacion("FECACT")) Then
                        _MesPuntuacion = Month(RowPuntuacion("FECACT"))
                        If _MesPuntuacion = 1 Then
                            FechaPuntuacion = ObtenerMes(12) & " " & Year(RowPuntuacion("FECACT")) - 1
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "FechaCalculo", "<script>var anyoCalculo=" & Year(RowPuntuacion("FECACT")) - 1 & ";var mesCalculo=12; </script>")
                        Else
                            FechaPuntuacion = ObtenerMes(Month(RowPuntuacion("FECACT")) - 1) & " " & Year(RowPuntuacion("FECACT"))
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "FechaCalculo", "<script>var anyoCalculo=" & Year(RowPuntuacion("FECACT")) & ";var mesCalculo=" & Month(RowPuntuacion("FECACT")) - 1 & "; </script>")
                        End If
                    End If
                End If
            Next
            Row("OBSERVACIONES") = if(lObservaciones.Count=0,"",serializer.Serialize(lObservaciones))
        Next
    End Sub
    ''' <summary>
    ''' Actualiza el dataset con la ficha del proveedor con las nuevas puntuaciones que recibe del webservice
    ''' </summary>
    ''' <remarks></remarks>
    Sub ActualizarTablaFichaProveedor(ByRef dsNuevaFichaProveedor As DataSet)
        'Relacionamos las nuevas puntaciones con la tabla de variables
        'Modificamos la variable interna _dsFichaProveedor par auqe no se cachee la simulacion
        dsNuevaFichaProveedor.Relations.Add("REL_VAR_NUEVASPUNT", New DataColumn() {dsNuevaFichaProveedor.Tables("MaestroVariables").Columns("VC_ID"), dsNuevaFichaProveedor.Tables("MaestroVariables").Columns("VC_NIVEL")}, New DataColumn() {dsNuevaFichaProveedor.Tables(2).Columns("VARCAL"), dsNuevaFichaProveedor.Tables(2).Columns("NIVEL")}, False)
        For Each Row As DataRow In dsNuevaFichaProveedor.Tables("MaestroVariables").Rows
            For Each RowPuntuacion As DataRow In Row.GetChildRows("REL_VAR_NUEVASPUNT")
                Row("PUNT_" & RowPuntuacion("UNQA")) = RowPuntuacion("VALOR")
                Row("CAL_" & RowPuntuacion("UNQA")) = RowPuntuacion("CAL")
                Row("FECACT_" & RowPuntuacion("UNQA")) = Now
            Next
        Next
    End Sub
    ''' <summary>
    ''' Procedimiento que estblece el datasource del WebHierarchicalDataGrid que contiene la ficha del proveedor
    ''' </summary>
    ''' <param name="dsNuevaFichaProveedor"></param>
    ''' <remarks></remarks>
    Private Sub whdgProveedores_InitializeDataSource(Optional ByVal dsNuevaFichaProveedor As DataSet = Nothing)
        Dim ds As New DataSet
        Dim dt As DataTable
        whdgProveedores.Rows.Clear()
        whdgProveedores.GridView.Rows.Clear()
        whdgProveedores.GridView.ClearDataSource()
        whdgProveedores.Columns.Clear()
        whdgProveedores.GridView.Columns.Clear()
        whdgProveedores.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings.Clear()

        If Not dsNuevaFichaProveedor Is Nothing Then
            dt = dsNuevaFichaProveedor.Tables("MaestroVariables")
            ds.Tables.Add(dt.Copy())
            whdgProveedores.DataSource = ds
            whdgProveedores.GridView.DataSource = ds
            CrearColumnasDT(dt)
            whdgProveedores.DataKeyFields = "VC_ID,VC_NIVEL"
        Else
            If dsFichaProveedor.Tables.Count > 0 Then
                dt = dsFichaProveedor.Tables("MaestroVariables")
                ds.Tables.Add(dt.Copy())
                whdgProveedores.DataSource = ds
                whdgProveedores.GridView.DataSource = ds
                CrearColumnasDT(dt)
                whdgProveedores.DataKeyFields = "VC_ID,VC_NIVEL"
            End If
        End If
    End Sub
    Private Sub CrearColumnasDT(ByVal dt As DataTable)
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim column As Infragistics.Web.UI.GridControls.EditingColumnSetting
        Dim nombre As String

        For i As Integer = 0 To dt.Columns.Count - 1
            column = New Infragistics.Web.UI.GridControls.EditingColumnSetting
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            nombre = dt.Columns(i).ToString
            With campoGrid
                .Key = nombre
                .DataFieldName = nombre
                .Header.Text = nombre
                .Header.Tooltip = nombre
                If Left(nombre, 6) = "FECACT" Then
                    .DataFormatString = "{0:" & FSNUser.DateFormat.ShortDatePattern.ToString & " " & FSNUser.DateFormat.ShortTimePattern.ToString & "}"
                Else
                    .DataFormatString = "{0:N" & FSNUser.NumberFormat.NumberDecimalDigits.ToString & "}"
                End If
            End With
            whdgProveedores.Columns.Add(campoGrid)
            whdgProveedores.GridView.Columns.Add(campoGrid)
            column.ColumnKey = nombre
            column.ReadOnly = True
            whdgProveedores.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings.Add(column)
        Next
    End Sub
    Private Sub AnyadirColumnaU(ByVal fieldname As String)
        Dim unboundField As New Infragistics.Web.UI.GridControls.UnboundField(True)

        unboundField.Key = fieldname
        whdgProveedores.Columns.Add(unboundField)
        whdgProveedores.GridView.Columns.Add(unboundField)
    End Sub
    ''' <summary>
    ''' Procedimiento que inicializa el grid que contiene la ficha del proveedor
    ''' Oculta las columnas que en principio no están visibles, y configura las cabeceras de las columnas, para añadirles los botones de cerrar, desplegar...
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ConfigurarGrid()
        whdgProveedores.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings("VC_DEN").ReadOnly = True
        whdgProveedores.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings("FORMULA").ReadOnly = True
        whdgProveedores.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings("LEYENDA").ReadOnly = True

        'COLUMNAS NO VISIBLES
        whdgProveedores.Columns("VC_ID").Hidden = True
        whdgProveedores.Columns("VC_NIVEL").Hidden = True
        whdgProveedores.Columns("FORMULA").Hidden = True
        whdgProveedores.Columns("LEYENDA").Hidden = True
        whdgProveedores.Columns("TIPO").Hidden = True
        whdgProveedores.Columns("SUBTIPO").Hidden = True
        whdgProveedores.Columns("MODIFICAR").Hidden = True
        whdgProveedores.Columns("OBSERVACIONES").Hidden = True

        'Variables
        Dim indiceColumnaMulti As Integer = 1
        whdgProveedores.Columns("VC_DEN").Header.Text = CabeceraVariables(Textos(2) & " " & Me.FechaPuntuacion)
        whdgProveedores.Columns("VC_DEN").Header.CssClass = "fp_celda_cabecera_vc"

        whdgProveedores.Columns("VC_DEN").Width = 350
        'formula/leyenda
        whdgProveedores.Columns("FORMULA").Header.CssClass = "punt_nivel0"
        whdgProveedores.Columns("FORMULA").Header.Text = CabeceraUNQAS("", Textos(29), "FORMULA")
        whdgProveedores.Columns("LEYENDA").Header.CssClass = "punt_nivel0"
        whdgProveedores.Columns("LEYENDA").Header.Text = CabeceraUNQAS("", Textos(30), "LEYENDA")
        whdgProveedores.Columns("FORMULA").Width = 250
        whdgProveedores.Columns("LEYENDA").Width = 250
        'puntuaciones y calificaciones
        Dim KeyPUNT As String
        Dim KeyCAL As String
        Dim KeyFecha As String
        Dim CodUNQA As String
        For Each UNQA As Collection In FiltroUNQA
            KeyPUNT = "PUNT_" & UNQA("ID")
            KeyCAL = "CAL_" & UNQA("ID")
            KeyFecha = "FECACT_" & UNQA("ID")

            'Control de edición para las puntuaciones
            whdgProveedores.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings(KeyPUNT).EditorID = "NumericEditor"

            'titulo cabecera
            CodUNQA = UNQA("DEN")
            If UNQA("NIVEL") <> 0 Then
                CodUNQA = Left(CodUNQA, InStr(CodUNQA, "-") - 1)
            End If
            whdgProveedores.Columns(KeyPUNT).Header.Tooltip = UNQA("DEN")
            whdgProveedores.Columns(KeyPUNT).Header.Text = Me.CabeceraUNQAS(UNQA("HIJOS"), CodUNQA, KeyPUNT, KeyCAL, KeyFecha)
            whdgProveedores.Columns(KeyCAL).Header.Text = Me.CabeceraUNQAS("", Textos(3), KeyCAL)
            whdgProveedores.Columns(KeyFecha).Header.Text = Me.CabeceraUNQAS("", Textos(4), KeyFecha)
            whdgProveedores.Columns(KeyFecha).FormatValue(FSNUser.DateFormat.ShortDatePattern & " " & FSNUser.DateFormat.ShortTimePattern)
            'Tamaños
            whdgProveedores.Columns(KeyPUNT).Width = 150
            whdgProveedores.Columns(KeyCAL).Width = 150
            whdgProveedores.Columns(KeyFecha).Width = 150
            'estilo de cabeceras por niveles
            whdgProveedores.Columns(KeyPUNT).Header.CssClass = "punt_nivel" & UNQA("NIVEL")
            whdgProveedores.Columns(KeyCAL).Header.CssClass = "punt_nivel" & UNQA("NIVEL")
            whdgProveedores.Columns(KeyFecha).Header.CssClass = "punt_nivel" & UNQA("NIVEL")
            'estilo de las celdas de calificaciones para todas el mismo
            whdgProveedores.Columns(KeyCAL).CssClass = "fp_celda_cal"
            whdgProveedores.Columns(KeyFecha).CssClass = "fp_celda_cal"
            'calificaciones no editables
            whdgProveedores.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings(KeyCAL).ReadOnly = True
            whdgProveedores.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings(KeyFecha).ReadOnly = True

            whdgProveedores.Columns(KeyPUNT).Hidden = False
            whdgProveedores.Columns(KeyCAL).Hidden = False
            whdgProveedores.Columns(KeyFecha).Hidden = False
            whdgProveedores.GridView.Columns(KeyPUNT).Hidden = False
            whdgProveedores.GridView.Columns(KeyCAL).Hidden = False
            whdgProveedores.GridView.Columns(KeyFecha).Hidden = False
            'si la unqa no está en el filtro la ocultamos
            If IsPostBack Then
                If _columnasVisiblesCliente Is Nothing Then _columnasVisiblesCliente = New List(Of String)
                whdgProveedores.Columns(KeyPUNT).Hidden = Not _columnasVisiblesCliente.Contains(KeyPUNT)
                whdgProveedores.Columns(KeyCAL).Hidden = Not _columnasVisiblesCliente.Contains(KeyCAL)
                whdgProveedores.Columns(KeyFecha).Hidden = Not _columnasVisiblesCliente.Contains(KeyFecha)
                whdgProveedores.GridView.Columns(KeyPUNT).Hidden = Not _columnasVisiblesCliente.Contains(KeyPUNT)
                whdgProveedores.GridView.Columns(KeyCAL).Hidden = Not _columnasVisiblesCliente.Contains(KeyCAL)
                whdgProveedores.GridView.Columns(KeyFecha).Hidden = Not _columnasVisiblesCliente.Contains(KeyFecha)
            Else
                ''ocultar calificacion
                whdgProveedores.Columns(KeyCAL).Hidden = True
                whdgProveedores.Columns(KeyFecha).Hidden = True
                If Not UNQA("FILTRO") Then
                    whdgProveedores.Columns(KeyPUNT).Hidden = True
                End If
            End If
        Next
    End Sub
    ''' <summary>
    ''' Escribe el html de salida de las cabeceras de las columnas de puntuaciones/calificacion/fechas y de las columnas de formula y leyenda
    ''' Calificación/Fecha/Formula y Leyenda solo llevan el icono de cerrar (sHijos="")
    ''' Puntaciones de las unidades de negocio llevan
    ''' Icono de cerrar, si sHijos != "" icono de desplegar, icono de ver calificacion(sKeyCalificacion != ""), icono de ver fecha de act (sKeyFecha != "")
    ''' </summary>
    ''' <param name="sHijos">Si se trata de la puntuación de una unidad de negoico, si esta tiene unidades hijas para ver si se muestra el icono de desplegar</param>
    ''' <param name="sTextoCabecera">Caption de la cabecera</param>
    ''' <param name="sKeyCabecera">Key de la columna</param>
    ''' <param name="sKeyCalificacion">Key de la columna de calificación asociada a la puntuacion</param>
    ''' <param name="sKeyFecha">Key de la columna de fecha asociada a la puntuacion</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function CabeceraUNQAS(ByVal sHijos As String, ByVal sTextoCabecera As String, ByVal sKeyCabecera As String, Optional ByVal sKeyCalificacion As String = "", Optional ByVal sKeyFecha As String = "") As String
        If sKeyCabecera = "FORMULA" OrElse sKeyCabecera = "LEYENDA" Then
            CabeceraUNQAS = "<div id=""contenedor_imagenes"">"
        Else
            CabeceraUNQAS = "<div style='background-color:#f1f1f1;' id=""contenedor_imagenes"">"
        End If
        'CabeceraUNQAS = CabeceraUNQAS & "<tr><td><nobr>" & IIf(sTextoCabecera.Length > 9, Left(sTextoCabecera, 8) & "...", sTextoCabecera) & "&nbsp;</td>"
        CabeceraUNQAS = CabeceraUNQAS & "<div style=""text-overflow:ellipsis; overflow:hidden;float:left; vertical-align:middle;" &
            IIf(Left(sKeyCabecera, 4) = "PUNT", "max-width:70px;", "") & "white-space:nowrap;"">" & sTextoCabecera & "&nbsp;</div>"
        If Left(sKeyCabecera, 4) = "PUNT" Then
            CabeceraUNQAS = CabeceraUNQAS & "<div style=""text-align:left; min-width:25px;"">"
        Else
            CabeceraUNQAS = CabeceraUNQAS & "<div style=""text-align:right; min-width:25px;float:rigth;"">"
        End If
        If sKeyCalificacion <> "" Then
            CabeceraUNQAS = CabeceraUNQAS & "<a href=""javascript:DesplegarColumnaCalificacion('" & sKeyCalificacion & "')""><img src=""" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/calificacion.gif"" border=""0"" alt=""" & Textos(26) & """></a>&nbsp;"
        End If
        If sKeyFecha <> "" Then
            CabeceraUNQAS = CabeceraUNQAS & "<a href=""javascript:DesplegarColumnaCalificacion('" & sKeyFecha & "')""><img src=""" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/fecha.gif"" border=""0"" alt=""" & Textos(4) & """></a>&nbsp;"
        End If
        If sHijos <> "" Then
            CabeceraUNQAS = CabeceraUNQAS & "<a href=""javascript:DesplegarColumnaUnqa('" & sHijos & "','" & Left(sKeyCabecera, sKeyCabecera.LastIndexOf("_")) & "')""><img src=""" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/desplegar_columna.gif"" border=""0"" alt=""" & Textos(28) & """></a>&nbsp;"
        End If
        CabeceraUNQAS = CabeceraUNQAS & "<a href=""javascript:OcultarColumna('" & sKeyCabecera & "')""><img src=""" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/cerrar.gif"" border=""0"" alt=""" & Textos(27) & """></a>"
        CabeceraUNQAS = CabeceraUNQAS & "</div></div>"
    End Function
    ''' <summary>
    ''' Genera el html de la cabecera de la columna de las variables de calidad con su nombre y un icono para ver la fórmula/leyenda
    ''' </summary>
    ''' <param name="sTextoCabecera">Nombre de la variable de calidad</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function CabeceraVariables(ByVal sTextoCabecera As String) As String
        CabeceraVariables = "<table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" id=""contenedor_imagenes"">"
        CabeceraVariables = CabeceraVariables & "<tr><td><nobr>" & sTextoCabecera & "&nbsp;</td>"
        CabeceraVariables = CabeceraVariables & "<td style=""text-align: right"">"
        CabeceraVariables = CabeceraVariables & "<a href=""javascript:DesplegarColumnaVariable('FORMULA','LEYENDA')""><img src=""" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/formula.gif"" border=""0"" alt=""" & Textos(25) & """></a>&nbsp;"
        CabeceraVariables = CabeceraVariables & "</td></tr></table>"
    End Function
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Función que se ejecuta por cada fila del whdgProveedores, en ella controlamos los estilos de las variables de calidad en función de su nivel
    ''' Y si la puntuación es editable o no en función de si la variable es de tip manual y siu el usuario tiene permiso para modificarla
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub whdgProveedores_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgProveedores.InitializeRow
        Dim KeyUNQA As String
        'Estilo de la variable de calidad por nivel
        Dim Nivel As Integer = 0
        Nivel = e.Row.Items.FindItemByKey("VC_NIVEL").Value
        e.Row.Items.FindItemByKey("VC_DEN").CssClass = "fp_celda_vc_" & Nivel

        e.Row.Items.FindItemByKey("FORMULA").CssClass = "SinSalto fp_fila_tabla" & Nivel
        e.Row.Items.FindItemByKey("FORMULA").Tooltip = DBNullToStr(e.Row.Items.FindItemByKey("FORMULA").Value)
        e.Row.Items.FindItemByKey("LEYENDA").CssClass = "SinSalto fp_fila_tabla" & Nivel
        e.Row.Items.FindItemByKey("LEYENDA").Tooltip = DBNullToStr(e.Row.Items.FindItemByKey("LEYENDA").Value)

        Dim serializer As New JavaScriptSerializer
        Dim Subtipo As Integer = 0
        Subtipo = DBNullToInteger(e.Row.Items.FindItemByKey("SUBTIPO").Value)
        Dim lObservaciones As Dictionary(Of String, String)
        If String.IsNullOrEmpty(e.Row.Items.FindItemByKey("OBSERVACIONES").Value.ToString) Then
            lObservaciones = New Dictionary(Of String, String)
        Else
            lObservaciones = serializer.Deserialize(Of Dictionary(Of String, String))(e.Row.Items.FindItemByKey("OBSERVACIONES").Value.ToString)
        End If
        'Formato puntuacion
        For Each UNQA As Collection In FiltroUNQA
            KeyUNQA = "PUNT_" & UNQA("ID")

            Select Case Subtipo
                Case 3, 4, 5, 6, 7, 8 '3-CERTIF 4-NC 5-PPM 6-CARGOS 7-TASA 8-Encuesta
                    If DBNullToStr(e.Row.Items.FindItemByKey(KeyUNQA).Value) <> "" Then
                        e.Row.Items.FindItemByKey(KeyUNQA).CssClass = "fp_fila_lnk_tabla" & Nivel
                    Else
                        e.Row.Items.FindItemByKey(KeyUNQA).CssClass = "fp_fila_tabla" & Nivel
                    End If
                Case Else
                    e.Row.Items.FindItemByKey(KeyUNQA).CssClass = "fp_fila_tabla" & Nivel
            End Select

            'Si la variable es de tipo manual y el usuario tiene permiso de modificación hacemos editable la puntuación
            If UNQA("PERMISO") = TipoAccesoUNQAS.ModificarPuntuacionesPanelCalidad AndAlso DBNullToInteger(e.Row.Items.FindItemByKey("SUBTIPO").Value) = 2 AndAlso DBNullToInteger(e.Row.Items.FindItemByKey("MODIFICAR").Value) = 1 Then
                e.Row.Items.FindItemByKey(KeyUNQA).CssClass = "fp_celda_editable" & If(lObservaciones.ContainsKey(KeyUNQA), " imagenNota", "")
                whdgProveedores.GridView.Behaviors.EditingCore.Behaviors.CellEditing.ColumnSettings(KeyUNQA).ReadOnly = False

                divRecalcular.Style.Item("display") = ""
                divAlmacenar.Style.Item("display") = ""
            End If
        Next
    End Sub
    ''' <summary>
    ''' Obtiene la denominación del mes de la puntuación
    ''' </summary>
    ''' <param name="iMes"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ObtenerMes(ByVal iMes As Integer) As String
        Dim IndiceMes As Integer = 11
        IndiceMes = IndiceMes + iMes
        ObtenerMes = Textos(IndiceMes)
    End Function
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Obtiene una tabla con las variables manuales modificadas por el usuario en la ficha.
    ''' Si el usuario ha pulsado Almacenar puntuaciones, guarda en base de datos las  nuevas puntuaciones
    ''' Se llama al servicio de calculo puntauciones pasandole un dataset son la tabla de las varibles manuales modificadas
    ''' Al servicio de calculo de puntuaciones se le indica si tiene o no que almacenar esas puntuaciones o solo es una simulacion
    ''' El servicio devuelve un dataset con las puntaaciones modificadas por el cambio de las variables manuales
    ''' Actualizamos la ficha con las nuevas puntuaciones
    ''' </summary>
    ''' <param name="bGuardar"></param>
    ''' <remarks></remarks>
    Private Sub ActualizaFichaProveedor(Optional ByVal bGuardar As Boolean = False)
        Dim dtVariables As DataTable = New DataTable("VariablesManuales")
        If ObtenerTablaVariablesManuales(dtVariables) Then
            'Dataset con las variables manuales
            Dim dsVariables As New DataSet
            dsVariables.Tables.Add(dtVariables)
            'Si hay que almacenar almancenamos los valores manuales
            If bGuardar Then
                Dim PMVariablesCalidad As FSNServer.VariablesCalidad = FSNServer.Get_Object(GetType(FSNServer.VariablesCalidad))
                PMVariablesCalidad.InsertarVariablesManuales(dsVariables)
            End If
            'Llamamos al webservice para que recalule la puntuación con los valores manuales, y le indicamos si tiene que guardar esta puntuación.
            Dim WebServiceQA As New FSQAWebService.QA_Puntuaciones
            Dim dsNuevasPuntuaciones As DataSet = WebServiceQA.SimularPuntuacion(dsVariables, Idioma, bGuardar, FSNUser.Cod, FSNUser.Password)
            Dim dtNuevasPuntuaciones As DataTable = dsNuevasPuntuaciones.Tables(0).Copy
            Dim dsNuevaFichaProveedor As DataSet = dsFichaProveedor.Copy

            dsNuevaFichaProveedor.Tables.Add(dtNuevasPuntuaciones)
            ActualizarTablaFichaProveedor(dsNuevaFichaProveedor)

            whdgProveedores_InitializeDataSource(dsNuevaFichaProveedor)
            whdgProveedores.DataBind()
            ConfigurarGrid()

            upWhdgProveedores.Update()
        Else
            'Alert de que no ha cambiado ningun dato
        End If
    End Sub
    ''' <summary>
    ''' Crea la tabla dtVariables con las columnas (PROVE, UNQA, VARCAL, NIVEL, VALOR, USU)
    ''' Y la rellena con los datos de las variables manuales que han sido modificadas por el usurio en la ficha
    ''' </summary>
    ''' <param name="dtVariables"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ObtenerTablaVariablesManuales(ByRef dtVariables As DataTable) As Boolean
        'Creamos la tabla que necesitamos PARA GUARDAR LOS VALORE MANUALES
        Dim DataChanged As Boolean = False
        Dim dColumna As DataColumn
        dColumna = New DataColumn("PROVE", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("UNQA", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("VARCAL", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("NIVEL", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dtVariables.PrimaryKey = New DataColumn() {dtVariables.Columns("UNQA"), dtVariables.Columns("VARCAL"), dtVariables.Columns("NIVEL")}
        dColumna = New DataColumn("VALOR", System.Type.GetType("System.Double"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("USU", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("OBSERVACIONES", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        'Rellenamos la tabla
        Dim dRow As DataRow
        Dim dataKey() As Object
        Dim serializer As New JavaScriptSerializer
        Dim lObservaciones As Dictionary(Of String, String)
        For Each Row As Infragistics.Web.UI.GridControls.GridRecord In whdgProveedores.Rows
            If DBNullToInteger(Row.Items.Item(whdgProveedores.Columns("SUBTIPO").Index).Value) = 2 AndAlso DBNullToInteger(Row.Items.Item(whdgProveedores.Columns("MODIFICAR").Index).Value) = 1 Then
                dataKey = Row.DataKey
                Dim dr As DataRow = dsFichaProveedor.Tables("MaestroVariables").Rows.Find(dataKey)
                Dim KeyPUNT As String
                If String.IsNullOrEmpty(Row.Items.Item(whdgProveedores.Columns("OBSERVACIONES").Index).Value.ToString) Then
                    lObservaciones = New Dictionary(Of String, String)
                Else
                    lObservaciones = serializer.Deserialize(Of Dictionary(Of String, String))(Row.Items.Item(whdgProveedores.Columns("OBSERVACIONES").Index).Value)
                End If
                For Each UNQA As Collection In FiltroUNQA
                    KeyPUNT = "PUNT_" & UNQA("ID")

                    If Not (DBNullToSomething(Row.Items.Item(whdgProveedores.Columns(KeyPUNT).Index).Value) = DBNullToSomething(dr(KeyPUNT)) _
                            AndAlso Not lObservaciones.ContainsKey("PUNT_" & UNQA("ID"))) Then
                        dRow = dtVariables.NewRow
                        dRow("PROVE") = ProveCod
                        dRow("UNQA") = UNQA("ID")
                        dRow("VARCAL") = Row.Items.Item(whdgProveedores.Columns("VC_ID").Index).Value
                        dRow("NIVEL") = Row.Items.Item(whdgProveedores.Columns("VC_NIVEL").Index).Value
                        dRow("VALOR") = Numero(Row.Items.FindItemByKey(KeyPUNT).Value, FSNUser.DecimalFmt, FSNUser.ThousanFmt)
                        dRow("USU") = FSNUser.Cod
                        dRow("OBSERVACIONES") = If(lObservaciones.ContainsKey("PUNT_" & UNQA("ID")), lObservaciones("PUNT_" & UNQA("ID")), "")
                        dtVariables.Rows.Add(dRow)
                        DataChanged = True
                    End If
                Next
            End If
        Next
        Return DataChanged
    End Function
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Exporta a excel la ficha de calidad que el usuario está visualizando
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ExportarExcel()
        Exportar()
        wdgExcelExporter.DownloadName = lblProveedor.Text & ".xls"
        wdgExcelExporter.Export(whdgProveedores)
    End Sub
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Exporta a pdf la ficha de calidad que el usuario está visualizando
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ExportarPdf()
        Exportar()
        With wdgPDFExporter
            .DownloadName = lblProveedor.Text & ".pdf"
            .Format = Infragistics.Documents.Reports.Report.FileFormat.PDF
            .TargetPaperOrientation = Infragistics.Documents.Reports.Report.PageOrientation.Landscape
            .Export(whdgProveedores)
        End With

    End Sub
    ''' <summary>
    ''' Prepara la grid dela ficha para exportarla
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Exportar()
        'Variables
        Dim indiceColumnaMulti As Integer = 1
        whdgProveedores.GridView.Columns("VC_DEN").Header.Text = Textos(2) & " " & Me.FechaPuntuacion & " - " & ProveDen
        'formula/leyenda
        whdgProveedores.GridView.Columns("FORMULA").Hidden = Not _columnasVisiblesCliente.Contains("FORMULA")
        whdgProveedores.GridView.Columns("FORMULA").Header.Text = Textos(29)
        whdgProveedores.GridView.Columns("LEYENDA").Hidden = Not _columnasVisiblesCliente.Contains("LEYENDA")
        whdgProveedores.GridView.Columns("LEYENDA").Header.Text = Textos(30)

        'puntuaciones y calificaciones
        Dim KeyPUNT As String
        Dim KeyCAL As String
        Dim KeyFEC As String
        For Each UNQA As Collection In FiltroUNQA
            KeyPUNT = "PUNT_" & UNQA("ID")
            KeyCAL = "CAL_" & UNQA("ID")
            KeyFEC = "FECACT_" & UNQA("ID")
            'titulo cabecera
            whdgProveedores.GridView.Columns(KeyPUNT).Header.Text = UNQA("DEN")
            whdgProveedores.GridView.Columns(KeyCAL).Header.Text = UNQA("DEN") & "/" & Textos(3)
            whdgProveedores.GridView.Columns(KeyFEC).Header.Text = UNQA("DEN") & "/" & Textos(4)
            'cambio de estilo porque en el pdf no se ve el gris claro
            whdgProveedores.GridView.Columns(KeyPUNT).Header.CssClass = "punt_nivel0"
            whdgProveedores.GridView.Columns(KeyCAL).Header.CssClass = "punt_nivel0"
            whdgProveedores.GridView.Columns(KeyFEC).Header.CssClass = "punt_nivel0"
            'mantener la visibilidad de columnas del cliente
            whdgProveedores.GridView.Columns(KeyPUNT).Hidden = Not _columnasVisiblesCliente.Contains(KeyPUNT)
            whdgProveedores.GridView.Columns(KeyCAL).Hidden = Not _columnasVisiblesCliente.Contains(KeyCAL)
            whdgProveedores.GridView.Columns(KeyFEC).Hidden = Not _columnasVisiblesCliente.Contains(KeyFEC)
        Next
    End Sub
    ''' <summary>
    ''' Muestra el Pop Up con la información de los errores que se han producido en el cálculo de puntuaciones
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub imgErrorCalculoPuntuaciones_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgErrorCalculoPuntuaciones.Click
        Dim oDetalleError As FSNServer.Proveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
        Dim dsDetalleError As DataSet

        lblTituloPopUp.Text = Textos(51)
        lblSubTituloPopUp.Text = Textos(52)
        lnkPrint.Text = Textos(53)

        dsDetalleError = oDetalleError.DevolverDetalleErrorProveedor(ProveCod, FSNUser.Cod, FSNUser.Idioma)

        Dim cinfo As New Globalization.CultureInfo(Me.Usuario.Idioma.RefCultural)
        Dim Mes As Integer = dsDetalleError.Tables(0).Rows(0)("MES")
        Mes = IIf(Mes = 1, 11, Mes - 2)

        Dim sInfoErrores As String = Textos(42)
        sInfoErrores = Replace(sInfoErrores, "$$$$$$", "<b>" & cinfo.DateTimeFormat.MonthNames(Mes).ToUpper & "</b>")
        sInfoErrores = Replace(sInfoErrores, "$$$$$", "<b>" & dsDetalleError.Tables(0).Rows(0)("PROVEEDOR") & "</b>")
        sInfoErrores = Replace(sInfoErrores, "$$$$", "<b>" & dsDetalleError.Tables(0).Rows(0)("VARIABLECALIDAD") & "</b>")
        sInfoErrores = Replace(sInfoErrores, "$$$", "<b>" & dsDetalleError.Tables(0).Rows(0)("UNIDADNEGOCIO") & "</b>")
        lblInfoErrores.Text = sInfoErrores


        Select Case dsDetalleError.Tables(0).Rows(0)("TIPOERROR")
            Case 1
                lblTipoError.Text = "<b>" & Textos(43) & "</b><span style=""color:Red;"">" & Textos(44) & "</span>"
            Case 2
                lblTipoError.Text = "<b>" & Textos(43) & "</b><span style=""color:Red;"">" & Textos(45) & "</span>"
            Case 3
                lblTipoError.Text = "<b>" & Textos(43) & "</b><span style=""color:Red;"">" & Textos(46) & "</span>"
            Case 4
                lblTipoError.Text = "<b>" & Textos(43) & "</b><span style=""color:Red;"">" & Textos(47) & "</span>"
            Case 5
                lblTipoError.Text = "<b>" & Textos(43) & "</b><span style=""color:Red;"">" & Textos(48) & "</span>"
            Case 6
                lblTipoError.Text = "<b>" & Textos(43) & "</b><span style=""color:Red;"">" & Textos(49) & "</span>"
        End Select

        If Not CType(dsDetalleError.Tables(0).Rows(0)("PERMISOVARIABLECALIDAD"), Boolean) OrElse Not CType(dsDetalleError.Tables(0).Rows(0)("PERMISOUNQA"), Boolean) Then
            pnlDetalleFormula.Visible = False
        Else
            Dim VariablesFormula() As String = Split(DBNullToStr(dsDetalleError.Tables(0).Rows(0)("LEYENDA")), ";")
            Dim ValoresFormula() As String = Split(DBNullToStr(dsDetalleError.Tables(0).Rows(0)("VALORES_FORMULA")), ";")

            lblTituloFormula.Text = "<b>" & Textos(29) & "</b>"
            If dsDetalleError.Tables(0).Rows(0)("OPCION_CONF") = VarCalOpcionConf.MediaPonderadaSegunVarHermana Then
                lblFormula.Text = Textos(63).Replace("@VARCAL", dsDetalleError.Tables(0).Rows(0)("DEN_VAR_POND"))
            Else
                lblFormula.Text = DBNullToStr(dsDetalleError.Tables(0).Rows(0)("FORMULA"))
            End If

            Dim dRow As HtmlControls.HtmlTableRow
            Dim dCell As HtmlControls.HtmlTableCell

            Dim NumeroVariablesFormula As Integer
            NumeroVariablesFormula = IIf(VariablesFormula.Length = 1, 0, VariablesFormula.Length - 2)
            For i As Integer = 0 To NumeroVariablesFormula
                If VariablesFormula(i).Length > 0 Then
                    dRow = New HtmlControls.HtmlTableRow

                    dCell = New HtmlControls.HtmlTableCell
                    dCell.Attributes.Add("class", "CeldaVariable" & IIf(Replace(Split(VariablesFormula(i), ":")(0), "X", "") <> dsDetalleError.Tables(0).Rows(0)("VARIABLEERROR"), "", "Rojo"))
                    dCell.Attributes.Add("style", "width:10%;")
                    dCell.InnerHtml = "<b>" & Split(VariablesFormula(i), ":")(0) & "</br>"
                    dRow.Cells.Add(dCell)

                    dCell = New HtmlControls.HtmlTableCell
                    dCell.Attributes.Add("class", "CeldaDefinicion" & IIf(Replace(Split(VariablesFormula(i), ":")(0), "X", "") <> dsDetalleError.Tables(0).Rows(0)("VARIABLEERROR"), "", "Rojo"))
                    dCell.Attributes.Add("style", "width:80%;")
                    dCell.InnerHtml = "<b>" & Split(VariablesFormula(i), ":")(1) & "</br>"
                    dRow.Cells.Add(dCell)

                    dCell = New HtmlControls.HtmlTableCell
                    dCell.Attributes.Add("class", "CeldaValor" & IIf(Replace(Split(VariablesFormula(i), ":")(0), "X", "") <> dsDetalleError.Tables(0).Rows(0)("VARIABLEERROR"), "", "Rojo"))
                    dCell.Attributes.Add("style", "width:10%;")
                    Dim sValor As String = String.Empty
                    If i < ValoresFormula.Length Then sValor = ValoresFormula(i).ToString
                    dCell.InnerHtml = "<b>" & sValor & "</br>"
                    dRow.Cells.Add(dCell)

                    tblDetalleFormulaError.Rows.Add(dRow)
                End If
            Next
        End If

        updDetalleError.Update()
        mErroresCalcPunt.Show()
    End Sub
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Función que se ejecuta cuando el usurio hace click en el botón de almacenar. Modifica la url del botón de Volver al panel de proveedores, para indicar al panel que como los datos han cambiado hay que actualizar la cahe´y volver a obtener las puntuaciones de BD
    ''' Ver coment ActualizaFichaProveedor.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub imgAlmacenar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAlmacenar.Click
        Dim sinicializaCache As String = "inicializaCache='0';"
        sinicializaCache = String.Format(IncludeScriptKeyFormat, "javascript", sinicializaCache)
        Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "sinicializaCache", sinicializaCache)

        ActualizaFichaProveedor(True)
    End Sub
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Función que se ejecuta cuando el usurio hace click en el botón de almacenar. Modifica la url del botón de Volver al panel de proveedores, para indicar al panel que como los datos han cambiado hay que actualizar la cahe´y volver a obtener las puntuaciones de BD
    ''' Ver coment ActualizaFichaProveedor.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkAlmacenar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAlmacenar.Click
        Dim sinicializaCache As String = "inicializaCache='0';"
        sinicializaCache = String.Format(IncludeScriptKeyFormat, "javascript", sinicializaCache)
        Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "sinicializaCache", sinicializaCache)

        ActualizaFichaProveedor(True)
    End Sub
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Función que se ejecuta cuando el usurio hace click en el botón de recalcular.Ver coment ActualizaFichaProveedor.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub imgRecalcular_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgRecalcular.Click
        ActualizaFichaProveedor(False)
    End Sub
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Función que se ejecuta cuando el usurio hace click en el botón de recalcular.Ver coment ActualizaFichaProveedor.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkRecalcular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRecalcular.Click
        ActualizaFichaProveedor(False)
    End Sub
    Private Sub wdgExcelExporter_GridRecordItemExported(sender As Object, e As Infragistics.Web.UI.GridControls.GridRecordItemExportedEventArgs) Handles wdgExcelExporter.GridRecordItemExported
        e.WorksheetCell.CellFormat.Alignment = Infragistics.Documents.Excel.HorizontalCellAlignment.Left
    End Sub

    ''' <summary>
    ''' Al imprimir la cabecera se marca q el estilo de toda la columna es "izquierda", sino, la 1ra fila tenía un estilo y el resto otro.
    ''' </summary>
    ''' <param name="sender">wdgPDFExporter</param>
    ''' <param name="e">evento lanzado</param>
    ''' <remarks>Llamada desde: Sistema; Tiempo maximo:0</remarks>
    Private Sub wdgPDFExporter_GridFieldCaptionExporting(sender As Object, e As Infragistics.Web.UI.GridControls.DocumentGridFieldCaptionExportingEventArgs) Handles wdgPDFExporter.GridFieldCaptionExporting
        DirectCast(e.Column, Infragistics.Web.UI.GridControls.BoundDataField).CssClass = "izquierda"
    End Sub

    ''' <summary>
    ''' Según el nivel de la variable se pone un padding mayor o menor para distinguir mejor las variables.
    ''' </summary>
    ''' <param name="sender">wdgPDFExporter</param>
    ''' <param name="e">evento lanzado</param>
    ''' <remarks>Llamada desde: Sistema; Tiempo maximo:0</remarks>
    Private Sub wdgPDFExporter_GridRecordItemExporting(sender As Object, e As Infragistics.Web.UI.GridControls.DocumentGridRecordItemExportingEventArgs) Handles wdgPDFExporter.GridRecordItemExporting
        If e.GridCell.Column.Key = "VC_DEN" AndAlso CType(Right(e.GridCell.CssClass.ToString, 1), Integer) > 1 Then
            e.CellElement.Paddings.Left = 10 * (CType(Right(e.GridCell.CssClass.ToString, 1), Integer) - 1)
        End If
    End Sub
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function setColumnasVisibles(ByVal columnasVisibles As String) As Object
        Try
            _columnasVisiblesCliente = Split(columnasVisibles, ";").ToList()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
