﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="panelCalidad.aspx.vb" Inherits="Fullstep.FSNWeb.panelCalidad"   MasterPageFile="~/App_Master/Menu.Master"%>
<asp:Content ID="Content2" runat="server" contentplaceholderid="head">
<style type="text/css">
        tbody > tr > td .hidExpColumn
        {
            width:0px !important;
        }
    </style>
<script type="text/javascript" language="javascript">
	var ModalProgress = '<%= ModalProgress.ClientID %>';
</script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH4" runat="server">
	<fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>	

	<asp:Panel ID="pnlCabeceraFiltros" runat="server" style="margin-top:0.5em;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="PanelCabecera">
			<tr style="cursor: pointer; font-weight: bold; height: 15px; width: 100%;">
				<td valign="middle" align="left" style="padding-top: 5px">
					<asp:Image ID="imgExpandir" runat="server" />
				</td>
				<td valign="middle" align="left"  width="100%">
					<asp:Label ID="lblCabeceraFiltros" runat="server" Text="DFILTROS"></asp:Label>
				</td>
			</tr>
		</table>
	</asp:Panel>
	<asp:Panel ID="pnlFiltros" runat="server">
		<table>
			<tr>
				<td>
					<asp:Label ID="lblFiltros" runat="server" Text="Label"></asp:Label>
				</td>
			</tr>
		</table>
	</asp:Panel>
	<ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" CollapseControlID="pnlCabeceraFiltros"
		ExpandControlID="pnlCabeceraFiltros" ImageControlID="imgExpandir" SuppressPostBack="True"
		TargetControlID="pnlFiltros">
	</ajx:CollapsiblePanelExtender>
	<br />
    <asp:Button runat="server" ID="btnPager" style="display:none;" />
    <asp:Button runat="server" ID="btnMostrarUnQa" style="display:none;" />
    <asp:Button runat="server" ID="btnMostrarMatQa" style="display:none;" />
	<asp:UpdatePanel ID="upWdgProveedores" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
		<ContentTemplate>
			<div class="CabeceraBotones" style="height:25px; clear:both; line-height:25px; width:98%;">                    
				<div style="margin-right:5px; height:25px;">
					<div class="botonDerechaCabecera" style="float:left;">  
						<div style="float:left; height:25px;">
							<asp:Label ID="lblOrdenarPor" runat="server" CssClass="Etiqueta" Text="DOrdenar Por:"></asp:Label>
							&nbsp;
						</div>
						<div style="float:left; height:25px;">
							<ig:WebDropDown ID="wddOrdenar" runat="server" AutoPostBack="true" Width="115px" DropDownContainerWidth="400px" DropDownAnimationDuration="1"
								EnableDropDownAsChild="false" EnableCustomValues="false" EnableClosingDropDownOnBlur="true" 
								EnableClosingDropDownOnSelect="true" CurrentValue=""
                                OnClientClick="VisibilidadColumnas();return true;">
							</ig:WebDropDown>                            
						</div>
						<div style="float:left; height:25px;">
							&nbsp;
							<asp:ImageButton ID="ibOrdenacion" runat="server" OnClientClick="cambiarImagenOrdenacion();return true;" style="vertical-align:middle;" />
						</div>
					</div>
					<div onclick="MostrarPdf()" class="botonDerechaCabecera" style="float:right; margin-right:15px;">            
						<asp:Image runat="server" ID="imgPDF" CssClass="imagenCabecera" />
						<asp:Label runat="server" ID="lblPDF" CssClass="fntLogin2" Text="PDF"></asp:Label>           
					</div>
					<div onclick="MostrarExcel()" class="botonDerechaCabecera" style="float:right;">            
						<asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
						<asp:Label runat="server" ID="lblExcel" CssClass="fntLogin2" Text="Excel"></asp:Label>           
					</div>
					<div onclick="GuardarConf()" class="botonDerechaCabecera" style="float:right;">  
						<asp:Image runat="server" ID="imgConf" CssClass="imagenCabecera" />
						<asp:Label runat="server" ID="lblConf" CssClass="fntLogin2" Text="Excel"></asp:Label>   
						<input runat="server" id="hCamposOcultos" type="hidden" />
					</div>
					<div onclick="return EnviarEmail();" class="botonDerechaCabecera" style="float:right;">  
						<asp:Image runat="server" ID="imgEmail" CssClass="imagenCabecera" />
						<asp:Label runat="server" ID="lblEnviarEMail" CssClass="fntLogin2" Text="dEnviarEMail"></asp:Label>    
					</div>                    
					<div onclick="VisorNotif()" class="botonDerechaCabecera" style="float:right;">  
						<asp:Image runat="server" ID="imgVisor" CssClass="imagenCabecera" />
						<asp:Label runat="server" ID="lblVisor" CssClass="fntLogin2" Text="dVisor"></asp:Label>   
					</div>
				</div>
			</div> 
			<input id="hOrdenar" type="hidden" runat="server" />
			<input id="hOrdenarSentido" type="hidden" runat="server" />
			<input id="hRefrescarCache" type="hidden" runat="server" value="" />
			<input id="PaginaVolver" runat="server" type="hidden" />
            <ig:WebDataGrid ID="wdgProveedores" runat="server" Height="530px" Browser="Xml" CssClass="pc_frameStyle"
                AutoGenerateBands="false"  AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="false" ExpansionColumnCss="hidExpColumn">
			    <Columns>
                    <ig:TemplateDataField Key="PROVE_DEN" Hidden="false">
                        <ItemTemplate>
                            <table cellpadding="0" cellspacing="0">
								<tr>                                            
									<td><asp:Image ID="ibtnTIPO_PROVE" runat="server"></asp:Image>&nbsp;</td>
									<td style="padding-right:5px;"><asp:ImageButton runat="server" ID="imgErrorCalPunt" SkinID="ErrCalPunt" Visible="false" CommandName="ERR_CALC_PUNT" /></td>
									<td>
										<nobr></nobr>
                                        <asp:LinkButton runat="server" ID="lbFichaProveedor" Text='<%# Eval("PROVE_NAME")%>' ToolTip='<%# Eval("PROVE_NAME")%>'
											OnClientClick='<%# "return IrFicha(""" & Eval("PROVE_COD") & """,""" & Eval("PROVE_NAME") & """,""" & Eval("PROVE_CIF") & """,""" & Eval("MATERIAL_QA_ID") & """,""" & Eval("CON_EMAIL") & """,""" & Eval("HAYERROR") & """);"%>'></asp:LinkButton>
								    </td>
								</tr>
							</table>                           
                        </ItemTemplate>
					</ig:TemplateDataField>                    
                    <ig:BoundDataField DataFieldName="PROVE_COD" Key="PROVE_COD" Hidden="false" Width="120px"></ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="PROVE_CIF" Key="PROVE_CIF" Hidden="false" Width="120px"></ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="CON_EMAIL" Key="CON_EMAIL" Hidden="false" Width="130px"></ig:BoundDataField>                    
                    <ig:BoundDataField DataFieldName="PROVE_NAME" Key="PROVE_NAME" Hidden="true" Width="120px"></ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="MATERIAL_QA" Key="MATERIAL_QA" Hidden="false" Width="120px"></ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="MATERIAL_QA_ID" Key="MATERIAL_QA_ID" Hidden="true" Width="120px"></ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="PROVE_CALIDAD_POT" Key="PROVE_CALIDAD_POT" Hidden="true" Width="120px"></ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="PROVE_CALIDAD_BAJA" Key="PROVE_CALIDAD_BAJA" Hidden="true" Width="120px"></ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="PROVE_CALIDAD_PROVISIONAL" Key="PROVE_CALIDAD_PROVISIONAL" Hidden="true" Width="120px"></ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="HAYERROR" Key="HAYERROR" Hidden="true" Width="120px"></ig:BoundDataField>                            
                    <ig:UnboundField Key="MATERIAL_GS" Hidden="false" Width="120px"></ig:UnboundField>
                    <ig:UnboundField Key="UNQAS" Hidden="false" Width="120px"></ig:UnboundField>                         
                </Columns>                           
                <Behaviors>
                    <ig:Activation Enabled="true"/>
                    <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Cell" ></ig:Selection> 
                    <ig:RowSelectors Enabled="false"></ig:RowSelectors>
                    <ig:Paging Enabled="true" PagerAppearance="Top">
						<PagerTemplate>
							<div class="CabeceraBotones" style="clear:both; float:left; width:100%;"> 
								<div style="float:left; padding:3px 0px 2px 0px; margin-left:10px;">
									<div style="clear: both; float: left; margin-right: 5px;">
                                        <asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" style="margin-right:2px;" />
                                        <asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" />
                                    </div>
                                    <div style="float: left; margin-right:5px;">
                                        <asp:Label ID="lblPage" runat="server" Text="dPage" Style="margin-right: 3px;"></asp:Label>
                                        <asp:DropDownList ID="PagerPageList" runat="server" Style="margin-right: 3px;" onchange="return IndexChanged()" />
                                        <asp:Label ID="lblOF" runat="server" Text="dOf" Style="margin-right: 3px;"></asp:Label>
                                        <asp:Label ID="lblCount" runat="server" />
                                    </div>
                                    <div style="float: left;">
                                        <asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-right:2px;" />
                                        <asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" />
                                    </div>
								</div>
							</div>						
						</PagerTemplate>
					</ig:Paging>
                </Behaviors>
                <ClientEvents Click="grid_CellClick"  />
            </ig:WebDataGrid> 
            <ig:WebDataGrid ID="wdgProveedoresAuxiliar" runat="server" Visible="false" AutoGenerateBands="false"  AutoGenerateColumns="false">
                <Columns>
                    <ig:BoundDataField DataFieldName="PROVE_NAME" Key="PROVE_NAME" Hidden="false" Width="120px"></ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="PROVE_COD" Key="PROVE_COD" Hidden="false" Width="120px"></ig:BoundDataField>                    
                    <ig:BoundDataField DataFieldName="PROVE_CIF" Key="PROVE_CIF" Hidden="false" Width="120px"></ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="CON_EMAIL" Key="CON_EMAIL" Hidden="false" Width="120px"></ig:BoundDataField>                    
                    <ig:BoundDataField DataFieldName="MATERIAL_QA" Key="MATERIAL_QA" Hidden="false" Width="120px"></ig:BoundDataField>
                </Columns>  
            </ig:WebDataGrid>
		</ContentTemplate>
		<Triggers>
			<asp:AsyncPostBackTrigger ControlID="wddOrdenar" EventName="SelectionChanged" />
			<asp:AsyncPostBackTrigger ControlID="ibOrdenacion" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnPager" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnMostrarUnQa" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnMostrarMatQa" EventName="Click" />
		</Triggers>
	</asp:UpdatePanel>
	<asp:Panel ID="pnlUNQA" runat="server" CssClass="modalPopup" Style="display: none;
		position: absolute; max-width: 500px; max-height: 300px; overflow: auto">
		<table border="0" cellpadding="0" cellspacing="0" id="contenedor_imagenes">
			<tr>
				<td>
					<asp:UpdatePanel ID="upUNQATitulo" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<nobr><table border="0" cellpadding="4">
		<tr>
		  <td valign="top" width="65"><asp:image ID="Image1" runat="server"  SkinID="UNegocio" width="54" height="54"/></td>
		  <td valign="top"><asp:Label runat="server" ID="lbltituloUNQA"></asp:Label></td>
		  </tr>
		</table>
						</ContentTemplate>
					</asp:UpdatePanel>
				</td>
				<td valign="top" style="text-align: right">
					<asp:Button ID="Button1" runat="server" Height="0" Text="Button" style="display:none;" Width="0" />
					<asp:ImageButton ID="ImgBtnDetCerrar" runat="server" 
						ToolTip="Cerrar Detalle" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<asp:UpdatePanel ID="upUNQA" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:TreeView ID="tvUNQA" runat="server">
								<DataBindings>
									<asp:TreeNodeBinding DataMember="System.Data.DataRowView" TextField="DEN" ValueField="ID"
										ToolTipField="ACCESO" SelectAction="None" />
								</DataBindings>
							</asp:TreeView>
						</ContentTemplate>
					</asp:UpdatePanel>
				</td>
			</tr>
		</table>
	</asp:Panel>
	<ajx:ModalPopupExtender ID="mpeUNQAS" CancelControlID="ImgBtnDetCerrar" Enabled="true"
		OkControlID="Button1" PopupControlID="pnlUNQA" TargetControlID="Button1" RepositionMode="RepositionOnWindowResizeAndScroll"
		runat="server">
	</ajx:ModalPopupExtender>
	<asp:Panel ID="pnlMatGS" runat="server" CssClass="modalPopup" Style="display: none; height: 300px; position: absolute; max-height: 300px; overflow: auto;">
		<table border="0" cellpadding="2" cellspacing="0" id="contenedor_imagenes" width="490">
			<tr>
				<td width="470">
					<asp:UpdatePanel ID="upMatGSTitulo" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<nobr>
								<table border="0" cellpadding="4">
									<tr>
										<td valign="top" width="65"><asp:image ID="Image2" runat="server" SkinID="Materiales" width="53" height="48"/></td>
										<td valign="top"><asp:Label runat="server" ID="lblTituloMatGS"></asp:Label></td>
									</tr>
								</table>
						</ContentTemplate>
					</asp:UpdatePanel>
				</td>
				<td width="20" valign="top" style="text-align: right">
					<asp:Button ID="Button2" runat="server" Text="Button" Style="display: none;" />
					<asp:ImageButton ID="ImgBtnDetCerrar2" runat="server" ToolTip="Cerrar Detalle" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<asp:UpdatePanel ID="upMatGS" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:TreeView ID="tvMatGS" runat="server">
								<DataBindings>
									<asp:TreeNodeBinding DataMember="System.Data.DataRowView" TextField="DEN" ValueField="ID"
										SelectAction="None" />
								</DataBindings>
							</asp:TreeView>
						</ContentTemplate>
					</asp:UpdatePanel>
				</td>
			</tr>
		</table>
	</asp:Panel>
	<ajx:ModalPopupExtender ID="mpeMatGS" CancelControlID="ImgBtnDetCerrar2" Enabled="true"
		OkControlID="Button2" PopupControlID="pnlMatGS" TargetControlID="Button2" RepositionMode="RepositionOnWindowResizeAndScroll"
		runat="server">
	</ajx:ModalPopupExtender>
	<input id="hProveCod" type="hidden" runat="server" />
	<input id="hProveDen" type="hidden" runat="server" />
	<input id="hProveCif" type="hidden" runat="server" />
	<input id="hProveMatQA" type="hidden" runat="server" />
	<input id="hProveEmail" type="hidden" runat="server" />
	<input id="hHayError" type="hidden" runat="server" />
	<asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
		<div style="position: relative; top: 30%; text-align: center;">
			<asp:Image ID="ImgProgress" runat="server" SkinID="ImgProgress" />
			<asp:Label ID="lblProcesando" runat="server" Text="DCargando" OnPreRender="lblProcesando_PreRender"></asp:Label>
		</div>
	</asp:Panel>
	<ajx:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
		BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />


    <ig:WebExcelExporter ID="wdgExcelExporter" runat="server"></ig:WebExcelExporter>
	<ig:WebDocumentExporter ID="wdgPDFExporter" runat="server" ExportMode="Download"></ig:WebDocumentExporter> 
	<input id="hColumnasVisibles" runat="server" type="hidden" />
	<ajx:ModalPopupExtender runat="server" ID="mErroresCalcPunt" Enabled="true" 
		TargetControlID="btnHidden"
		CancelControlID="imgCerrar"
		PopupControlID="pnlDetalleErrorCalculoPuntuaciones" 
		RepositionMode="RepositionOnWindowResizeAndScroll">
	</ajx:ModalPopupExtender>
	
	<asp:Panel runat="server" ID="pnlDetalleErrorCalculoPuntuaciones" CssClass="modalPopup"  style="display:none; width:750px; padding-bottom:20px;overflow:hidden;">
		<asp:Button runat="server" ID="btnHidden" style="display:none;"/>        
		<div style="float:left; position:absolute; z-index:10010;">
			<asp:Image runat="server" ID="imgPopUpErrCalcPunt" SkinId="TituloPopUpErrores" style="padding-top:5px; padding-left:10px;" />
		</div>
		
		<div style="width:100%; float:right; position:absolute; z-index:10010; text-align:right;">
			<asp:ImageButton runat="server" ID="imgCerrar" SkinId="Cerrar" style="padding-top:2px; padding-right:10px;" />
		</div>
		
		<asp:UpdatePanel ID="updDetalleError" runat="server" UpdateMode="Conditional" style="overflow:hidden;">
			<ContentTemplate> 
				<div class="CapaTituloPanelInfo" style="width:100%; position:relative; z-index:10000; height:30px; line-height:30px;">
					<div style="float:left;padding-left:75px; vertical-align:middle;">
						<asp:Label runat="server" ID="lblTituloPopUp" SkinId="TituloPanelInfo" style="padding-left:5px" Text="DIncidencia en el cÃ¡lculo de puntuaciones"></asp:Label>                          
					</div>
					<div style="float:right; padding-right:55px; vertical-align:middle;">                        
						<asp:ImageButton  runat="server" ID="imgPrint" SkinID="ImprimirPopUp" OnClientClick="PrintForm();return false;" style="vertical-align:middle;" />
						<asp:LinkButton runat="server" ID="lnkPrint" SkinId="lnkImprimirPopUp" OnClientClick="PrintForm();return false;" Text="DImprimir"></asp:LinkButton>                      
					</div>
				</div>                    
				  
				<div class="CapaSubTituloPanelInfo" style="width:100%; position:relative; z-index:10000; height:30px; line-height:30px;">
					<div style="padding-left:75px; vertical-align:middle;">
						<asp:Label runat="server" ID="lblSubTituloPopUp" SkinId="SubTituloPanelInfo" style="padding-left:5px" Text="DDetalle de la incidencia"></asp:Label>
					</div>
				</div>           
					
				<div style="width:100%; padding-top:15px;">
					<div style="padding-left:10px; padding-right:10px;">
						<asp:Label runat="server" ID="lblInfoErrores" CssClass="ContenidoNormal"></asp:Label>
					</div>                    
				</div> 
				
				<div style="width:100%; padding-top:15px;">
					<asp:Label runat="server" ID="lblTipoError" CssClass="Normal" style="padding-left:10px;"></asp:Label>
				</div>
				
				<asp:Panel runat="server" id="pnlDetalleFormula" style="padding-top:20px; padding-left:10px; padding-right:10px;">
					<div class="DetalleFormulaPopUp" style="width:100%;">
						<div style="width:100%;">
							<asp:Label runat="server" ID="lblTituloFormula" CssClass="Normal" style="padding-left:10px;"></asp:Label>
						</div>
						<div style="width:100%; padding-top:5px;">
							<asp:Label runat="server" ID="lblFormula" CssClass="Normal" style="padding-left:10px;"></asp:Label>
						</div>                            
					</div>
					<div style="width:100%; padding-top:20px;overflow:scroll;height:30em;">  
						<table runat="server" id="tblDetalleFormulaError" cellpadding="0" cellspacing="0" border="0" >
						
						</table>
					</div>
				</asp:Panel>
					   
			</ContentTemplate>
		</asp:UpdatePanel>        
	</asp:Panel>
    <script type="text/javascript">
        $(document).ready(function () {
            $('[id$=ImgBtnFirst]').live('click', function () { FirstPage(); });
            $('[id$=ImgBtnPrev]').live('click', function () { PrevPage(); });
            $('[id$=ImgBtnNext]').live('click', function () { NextPage(); });
            $('[id$=ImgBtnLast]').live('click', function () { LastPage(); });

        //		'Para ocultar o mostrar con javascript una columna de la grid, infragistics ha hecho que todos los elementos de cada columna compartan una misma clase css.
        //		'De ese modo, mediante una serie de funciones, recupera el nombre de esa clase css y modifica el estilo display de esa clase css para que todos los elementos 
        //		'que la comparten (la columna) se muestren o se oculten al mismo tiempo. 
        //		'El nombre de la clase se busca en la colección document.styleSheets. En esa colección se guardan un listado de las clases css usadas en la página (tanto internas como de enlaces externos).
        //		'En los postbacks, la función que agrega a esa colección las clases es: Infragistics.Web.UI.ControlMain.prototype.__appendStyles
        //		'Dentro de esta función, para agregar las clases css en IE, se usa el método addRule, que tiene 3 parámetros, uno de ellos opcional. 
        //		'El primero y el segundo, obligatorios, son: selector y declaration (http://www.javascriptkit.com/domref/stylesheet.shtml)
        //		'selector es el nombre de la clase css que se agrega (ej: .ig2d281703) y declaration es su contenido (ej: display:none;).
        //		'El problema es que el campo declaration es obligatorio, no puede ir vacío. De hecho, para añadir una clase css vacía bastaría con el parámetro declaration llevara un punto y coma ";"
        //		'Como no se ha hecho así, el método lanza un error, que los programadores de Infragistics capturan e ignoran mediante un catch(err)
        //		'El resultado es que esa clase deja de estar en la colección, por lo que, cuando se intenta ocultar una 
        //		'columna cuya clase css ya no está en document.styleSheets, se devuelve un error "'null' is null or not an object", 
        //		'dado que la función Infragistics.Web.UI.GridColumn.prototype.set_hidden no controla si el estilo existe porque 
        //		'necesariamente debería estar presente en la colección (ya que está presente en la página).
        //		'Para mantener la funcionalidad de ocultar la columna, he sobreescrito las funciones _get_hiddenStyleSheet y
        //      '_get_hiddenByChildrenStyleSheet  de infragistics, de modo que cuando una clase css vaya vacía, el parámetro declaration sea "{};" en vez de "".
        //		'Así, todas las clases css se guardan correctamente en la colección document.styleSheets.

            if ($IG.GridColumn) {
                $IG.GridColumn.prototype._get_hiddenStyleSheet = function () {
                    var css = this._get_hiddenCss();

                    if (!css)// To avoid going through all of the cells, the Hidden property should be assigned on the server to generate a css class for it
                    {
                        css = this._owner.get_id() + "_col" + this.get_index() + "_hidden";
                        /* OK 9/6/2012 119844 - Header text got merged after a column is hidden on client and an update panel on the page fires a postback. */
                        css = css.toLowerCase();
                        var lastSS = document.styleSheets[document.styleSheets.length - 1];
                        /* DAY 3-22-11 69834- Cannot hide column on the client in IE 9 */
                        if (typeof (lastSS.addRule) != "undefined" && !$util.IsIE9Plus)
                        //La función addRule necesita dos parámetros, sino da error
                        //lastSS.addRule("." + css);
                            lastSS.addRule("." + css, "{};");
                        else
                            lastSS.insertRule("." + css + "{}", lastSS.length);

                        if (this._headerElement) {
                            this._headerElement.className += (this._headerElement.className ? " " : "") + css;

                            if (this._owner._hasHeaderLayout) {
                                /* Set the hidden css on the sizeHeaderRow */
                                var sizeHeaderRowTH = this._owner._elements["sizeHeaderRow"].childNodes;
                                for (var i = 0; i < sizeHeaderRowTH.length; i++) {
                                    if (sizeHeaderRowTH[i].getAttribute("key") == this._key) {
                                        sizeHeaderRowTH[i].className += (sizeHeaderRowTH[i].className ? " " : "") + css;
                                        break;
                                    }
                                }

                            }
                        }

                        var rows = this._owner.get_rows();
                        for (var i = 0; i < rows.get_length(); i++) {
                            var cell = rows.get_row(i).get_cellByColumn(this);
                            var elem = cell.get_element();
                            if (elem)
                                elem.className += (elem.className ? " " : "") + css;
                        }

                        rows = this._owner._get_auxRows();
                        for (var i = 0; i < rows.length; i++) {
                            var cell = rows[i].get_cellByColumn(this);
                            var elem = cell.get_element();
                            if (elem)
                                elem.className += (elem.className ? " " : "") + css;
                        }

                        /* DAY 2/24/12 102953- JS exception when hiding column for first time on the client with a footer and no header */
                        if (this._footerElement)
                            this._footerElement.className += (this._footerElement.className ? " " : "") + css;

                        var args = { column: this, css: css };
                        this._owner._gridUtil._fireEvent(this._owner, "HideColumnCssCreated", args);

                        this._set_hiddenCss(css);
                    }

                    return $util.getStyleSheet(css);
                }
            }
            if ($IG.HeaderLayoutColumn) {
                $IG.HeaderLayoutColumn.prototype._get_hiddenByChildrenStyleSheet = function () {
                    var css = this._get_hiddenByChildrenCss();

                    if (!css)// To avoid going through all of the cells, the Hidden property should be assigned on the server to generate a css class for it
                    {
                        css = this._owner.get_id() + "_col" + this._key + "_hiddenByChildren";
                        var lastSS = document.styleSheets[document.styleSheets.length - 1];
                        /* DAY 3-22-11 69834- Cannot hide column on the client in IE 9 */
                        if (typeof (lastSS.addRule) != "undefined" && !$util.IsIE9Plus)
                            //lastSS.addRule("." + css);
                            lastSS.addRule("." + css, "{};");
                        else
                            lastSS.insertRule("." + css + "{}", lastSS.length);

                        if (this._headerElement)
                            this._headerElement.className += (this._headerElement.className ? " " : "") + css;

                        this._set_hiddenByChildrenCss(css);
                    }

                    return $util.getStyleSheet(css);
                }
            }
        }); 
    </script>
</asp:Content>