﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Contactos.aspx.vb" Inherits="Fullstep.FSNWeb.Contactos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
</head>
<body style="background-color: #FFFFFF; margin-top: 0px; margin-left: 0px; margin-right: 0px">
    <script type="text/javascript">
        /*''' <summary>
        ''' Al seelccionar un contacto este se convierte en el contacto por de defecto del proveedor
        ''' </summary>
        ''' <param name="sender">objeto grid</param>
        ''' <param name="e">evento de pantalla</param>        
        ''' <remarks>Llamada desde: sistema; Tiempo máximo: 0,1</remarks>*/    
        function wdgDatos_CellSelectionChanged(sender, e) {
            var sProveCod = document.getElementById('ProveCod').value;
            var lId = document.getElementById('IDContacto').value;
            var email = '';
            var nom = '';

            if (e.getSelectedRows().get_length() == 1) {
                lId = e.getSelectedRows().getItem(0).get_cellByColumnKey("ID").get_value();
                email = e.getSelectedRows().getItem(0).get_cellByColumnKey("EMAIL").get_value();
                nom = e.getSelectedRows().getItem(0).get_cellByColumnKey("NOMBRE").get_value();
            }

            document.getElementById('EmailContacto').value = email;
            document.getElementById('EmailContactoCompleto').value = nom + " (" + email + ")";
            
            Fullstep.FSNWeb.Consultas.GrabarContactoPanel(lId, sProveCod, OnGetInfoQSComplete);
        }
        /*''' <summary>
        ''' Tras grabar el contacto por de defecto del proveedor lo muestra en el panel de calidad
        ''' </summary>
        ''' <param name="result">resultado de la grabación</param>
        ''' <remarks>Llamada desde: serviceio web de grabación; Tiempo máximo: 0</remarks>*/
        function OnGetInfoQSComplete(result) {
            var p = window.opener;
                       
            if ((document.getElementById('Completo').value == 0) || (document.getElementById('Completo').value != 1)) {
                p.actualizarContacto(document.getElementById('EmailContacto').value, parseInt(result))
            }
            else {
                p.actualizarContacto(document.getElementById('EmailContactoCompleto').value, parseInt(result))
            }
            window.close();
        }        
    </script>
    
<form id="frmContactos" name="frmContactos" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">            
		<CompositeScript>
			<Scripts>
				<asp:ScriptReference Path="~/js/jquery/jquery.min.js" />
				<asp:ScriptReference Path="~/js/jquery/jquery.tmpl.min.js" />
				<asp:ScriptReference Path="~/js/jquery/jquery-migrate.min.js" />				
			</Scripts>
		</CompositeScript>		
    </asp:ScriptManager> 
    
    <script type="text/javascript">
        $('#cmdCancelar').live('click', function () { window.close(); });
        $('#cmdAceptar').live('click', function () { asignarContactos(); });

        function asignarContactos() {            
            var contactos = new Array;
            var eMail = "";
            var oGrid = $find("wdgDatos");
            for (i = 0; i < oGrid.get_rows().get_length() ; i++) {
                var oRow = oGrid.get_rows().get_row(i);
                if (oRow.get_cellByColumnKey("CALIDAD").get_value() == 1) {
                    contactos.push(oRow.get_cellByColumnKey("ID").get_value());
                    eMail = oRow.get_cellByColumnKey("EMAIL").get_value();
                }
            }
            $.when($.ajax({
                type: "POST",
                url: rutaFS + 'QA/Puntuacion/Contactos.aspx/AsignarContactosQA',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ Proveedor:proveCod, Contactos: contactos }),
                async: false,                
            })).done(function (msg) {                
                var oGridProves = $("[id$=wdgProveedores]", window.opener.document);
                oGridProves = window.opener.$find(oGridProves[0].id);
                var CacheContactos;
                switch (contactos.length) {
                    case 0:
                        oGridProves.get_behaviors().get_activation().get_activeCell().set_value("");
                        oGridProves.get_behaviors().get_activation().get_activeCell().set_text("");

                        CacheContactos = "";

                        break;
                    case 1:
                        oGridProves.get_behaviors().get_activation().get_activeCell().set_value(eMail);
                        oGridProves.get_behaviors().get_activation().get_activeCell().set_text(eMail);

                        CacheContactos = eMail;

                        break;
                    default:
                        oGridProves.get_behaviors().get_activation().get_activeCell().set_value("##");
                        oGridProves.get_behaviors().get_activation().get_activeCell().set_text("(" + multiplesContactos + ")");

                        CacheContactos = "##";

                        break;
                }
                $.ajax({
                    type: "POST",
                    url: rutaFS + 'QA/Puntuacion/panelCalidad.aspx/CambiarContactos',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({ Proveedor: proveCod, Contactos: CacheContactos }),
                    dataType: "json",
                    async: false
                });

                window.close();
            })          
        }
    </script>
     
    <asp:Panel ID="pnlTitulo" runat="server" Width="100%">
        <table width="100%" cellpadding="0" border="0">
            <tr>
                <td>
                    <fsn:FSNPageHeader ID="FSNPageHeader" runat="server">
                    </fsn:FSNPageHeader>
                </td>
            </tr>        
        </table>                   
    </asp:Panel>    

    <asp:Panel ID="pnlGrid" runat="server" Width="100%" style="padding-top:2em;">
        <div class="BusqProve">
            <ig:WebDataGrid ID="wdgDatos" runat="server" Width="600px" Height="290px" ShowHeader="true" AutoGenerateColumns="false">
                <Columns>
                    <ig:BoundCheckBoxField DataFieldName="CALIDAD" Key="CALIDAD" Width="5%" ></ig:BoundCheckBoxField>
                    <ig:BoundDataField DataFieldName="NOMBRE" Key="NOMBRE" Width="55%" CssClass="ListItemLink"></ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="EMAIL" Key="EMAIL" Width="40%" CssClass="ListItemLink"></ig:BoundDataField>                                                    
                    <ig:BoundDataField DataFieldName="ID" Key="ID" Hidden="true"></ig:BoundDataField>
                </Columns>
                <Behaviors> 
                    <ig:EditingCore Enabled="true" AutoCRUD="False">
                        <Behaviors>
                            <ig:CellEditing >
                            <EditModeActions EnableF2="false" MouseClick="Single"/>                                                                                
								<ColumnSettings>
									<ig:EditingColumnSetting ColumnKey="CALIDAD"  EditorID="NumericEditor" />
                                    <ig:EditingColumnSetting ColumnKey="NOMBRE" ReadOnly="true"/>
                                    <ig:EditingColumnSetting ColumnKey="EMAIL" ReadOnly="true"/>
                                    <ig:EditingColumnSetting ColumnKey="ID" ReadOnly="true"/>
								</ColumnSettings>                        
							</ig:CellEditing>
						</Behaviors>  
                    </ig:EditingCore>                                                               
                    <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                </Behaviors>                
            </ig:WebDataGrid>
        </div>
    </asp:Panel>
        
    <div style="text-align:center; margin-top:2em;">
        <asp:Label runat="server" ID="cmdAceptar" Text="DAceptar" CssClass="botonRedondeado" ClientIDMode="Static" style="margin-right:1em;"></asp:Label>
        <asp:Label runat="server" ID="cmdCancelar" Text="DCancelar" CssClass="botonRedondeado" ClientIDMode="Static"></asp:Label>	
    </div>              			
    
    <asp:HiddenField ID="ProveCod" runat="server" />
    <asp:HiddenField ID="IDContacto" runat="server" />
    <asp:HiddenField ID="Completo" runat="server" />
    <asp:HiddenField ID="EmailContacto" runat="server" />
    <asp:HiddenField ID="EmailContactoCompleto" runat="server" />
    </form>
</body>
</html>
