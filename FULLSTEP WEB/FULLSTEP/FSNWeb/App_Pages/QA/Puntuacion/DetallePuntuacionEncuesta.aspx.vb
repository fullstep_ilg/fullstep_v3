﻿Imports System.Web.Script.Serialization
Imports Infragistics.Web.UI.GridControls

Partial Public Class DetallePuntuacionEncuesta
    Inherits FSNPage
    Private Const TODOS_MATQA As String = "TODOS"
    Private _dsFichaProveedor As DataSet
    Private _Unidad As String
    Private _Subtipo As String
    Private _Variable As String
    Private _Nivel As String
    Private _ProveCod As String
    Private _ProveDen As String
    Private _dtDatosVC As DataRow
    Private _Puntos As String
    Private _Calif As String
    Private _DenUnidad As String
    Private _ProveMatQA As String
    Private _FiltroVarCal As Collection
    Private m_bSaltarLayout As Byte = 0
    Private IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
    ''' <summary>
    ''' Obtiene la ficha del proveedor cuando viene de GS directamente 
    ''' </summary>
    ''' <remarks></remarks>
    Sub ObtenerTablaFichaProveedor()
        'Creamos la tabla que necesitamos para mostrar la ficha de calidad
        Dim dtVariables As DataTable = New DataTable("MaestroVariables")
        Dim dRow As DataRow
        Dim dColumna As DataColumn

        dColumna = New DataColumn("VC_ID", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("VC_NIVEL", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dtVariables.PrimaryKey = New DataColumn() {dtVariables.Columns("VC_ID"), dtVariables.Columns("VC_NIVEL")}
        dColumna = New DataColumn("VC_DEN", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("FORMULA", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("LEYENDA", System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("TIPO", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("SUBTIPO", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("MODIFICAR", System.Type.GetType("System.Int32"))
        dtVariables.Columns.Add(dColumna)
        'añadimos una columna por cada UNQA

        dColumna = New DataColumn("PUNT_" & _Unidad, System.Type.GetType("System.Double"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("CAL_" & _Unidad, System.Type.GetType("System.String"))
        dtVariables.Columns.Add(dColumna)
        dColumna = New DataColumn("FECACT_" & _Unidad, System.Type.GetType("System.DateTime"))
        dtVariables.Columns.Add(dColumna)

        'Añadimos las filas con las variables de calidad que el usuario puede ver y que afectan al proveedor
        For Each VarCal As Collection In FiltroVarCal
            Dim aMateriales() As String = Split(VarCal("MATQA"), "_")
            Dim bEncontrado As Boolean = False
            For iMat As Integer = 0 To aMateriales.Length - 1
                If aMateriales(iMat) = TODOS_MATQA Then
                    bEncontrado = True
                    Exit For
                End If
                If ProveMatQA.IndexOf("," & aMateriales(iMat) & ",") <> -1 Then
                    bEncontrado = True
                    Exit For
                End If
            Next
            If bEncontrado Then
                dRow = dtVariables.NewRow
                dRow("VC_ID") = VarCal("ID")
                dRow("VC_NIVEL") = VarCal("NIVEL")
                dRow("VC_DEN") = VarCal("DEN")
                dtVariables.Rows.Add(dRow)
            End If
        Next

        _dsFichaProveedor.Tables.Add(dtVariables)
        _dsFichaProveedor.Relations.Add("REL_VAR_PUNT", New DataColumn() {_dsFichaProveedor.Tables("MaestroVariables").Columns("VC_ID"), _dsFichaProveedor.Tables("MaestroVariables").Columns("VC_NIVEL")}, New DataColumn() {_dsFichaProveedor.Tables(0).Columns("VARCAL"), _dsFichaProveedor.Tables(0).Columns("NIVEL")}, False)
        'Recorremos la tabla de variables de calidad y obtenemos sus puntuaciones por cada unqa
        For Each Row As DataRow In dtVariables.Rows
            For Each RowPuntuacion As DataRow In Row.GetChildRows("REL_VAR_PUNT")
                Row("FORMULA") = RowPuntuacion("FORMULA")
                Row("LEYENDA") = RowPuntuacion("LEYENDA")
                Row("TIPO") = RowPuntuacion("TIPO")
                Row("SUBTIPO") = RowPuntuacion("SUBTIPO")
                Row("MODIFICAR") = RowPuntuacion("MODIFICAR")
                Row("PUNT_" & RowPuntuacion("UNQA")) = RowPuntuacion("PUNT")
                Row("CAL_" & RowPuntuacion("UNQA")) = RowPuntuacion("CALIFICACION")
                Row("FECACT_" & RowPuntuacion("UNQA")) = RowPuntuacion("FECACT")
                Dim _MesPuntuacion As Integer
                If FechaPuntuacion Is Nothing Then
                    If Not IsDBNull(RowPuntuacion("FECACT")) Then
                        _MesPuntuacion = Month(RowPuntuacion("FECACT"))
                        If _MesPuntuacion = 1 Then
                            FechaPuntuacion = ObtenerMes(12) & " " & Year(RowPuntuacion("FECACT")) - 1
                        Else
                            FechaPuntuacion = ObtenerMes(Month(RowPuntuacion("FECACT")) - 1) & " " & Year(RowPuntuacion("FECACT"))
                        End If
                    End If
                End If
            Next
        Next
    End Sub
    Protected ReadOnly Property ProveMatQA() As String
        Get
            If _ProveMatQA Is Nothing Then
                _ProveMatQA = Request.Form(Request.QueryString("h") & "hProveMatQA")

                If _ProveMatQA Is Nothing Then
                    _ProveMatQA = Session("ProveMatQA")
                Else
                    Session("ProveMatQA") = _ProveMatQA
                End If

                If _ProveMatQA Is Nothing Then
                    Response.Redirect("panelCalidadFiltro.aspx")
                End If
            End If
            Return _ProveMatQA
        End Get
    End Property
    Function ObtenerMes(ByVal iMes As Integer) As String
        Dim IndiceMes As Integer = 11
        IndiceMes = IndiceMes + iMes
        ObtenerMes = Textos(IndiceMes)
    End Function
    Protected ReadOnly Property FiltroVarCal() As Collection
        Get
            If _FiltroVarCal Is Nothing Then
                _FiltroVarCal = Session("CFiltroVarCal")
                If _FiltroVarCal Is Nothing Then
                    Response.Redirect("panelCalidadFiltro.aspx")
                End If
            End If
            Return _FiltroVarCal
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que contiene la fecha de actualización de las puntuaciones y la almacena en el ViewState.
    ''' </summary>
    ''' <value></value>
    ''' <returns>String con la fecha</returns>
    ''' <remarks></remarks>
    Protected Property FechaPuntuacion() As String
        Get
            Return ViewState("FechaPuntuacion")
        End Get
        Set(ByVal value As String)
            ViewState("FechaPuntuacion") = value
        End Set
    End Property
    ''' <summary>
    ''' Propiedad que contiene el datarow con los datos de la variable de la ficha de calidad del proveedor a mostrar.
    ''' </summary>
    ''' <value></value>
    ''' <returns>DataRow</returns>
    ''' <remarks>Llamada desde: MostrarPanelNC; Tiempo maximo:0</remarks>
    Protected ReadOnly Property dtDatosVC() As DataRow
        Get
            If _dtDatosVC Is Nothing Then
                If _dsFichaProveedor Is Nothing Then
                    Dim PMProveedor As FSNServer.Proveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                    _dsFichaProveedor = PMProveedor.DevolverFichaCalidad(_ProveCod, Me.FSNUser.Cod, _Unidad, _Variable, Me.Idioma)
                    If _dsFichaProveedor.Tables.Count > 0 Then
                        ObtenerTablaFichaProveedor()
                    End If
                    Me.InsertarEnCache("dsFichaProveedor_" & FSNUser.Cod, _dsFichaProveedor)
                Else
                    _dsFichaProveedor = CType(Cache("dsFichaProveedor_" & FSNUser.Cod), DataSet)
                End If

                For Each row As DataRow In _dsFichaProveedor.Tables("MaestroVariables").Rows
                    If row.Item("VC_ID") = _Variable AndAlso row.Item("VC_NIVEL") = _Nivel Then
                        _dtDatosVC = row

                        _Puntos = DBNullToStr(row.Item("PUNT_" & _Unidad))
                        _Calif = DBNullToStr(row.Item("CAL_" & _Unidad))

                        Exit For
                    End If
                Next
            End If
            Return _dtDatosVC
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que contiene la denominación de la unidad de la ficha de calidad del proveedor a mostrar.
    ''' </summary>
    ''' <value></value>
    ''' <returns>denominación de la unidad</returns>
    ''' <remarks>Llamada desde: MostrarPanelNC; Tiempo maximo:0</remarks>
    Protected ReadOnly Property DenUnidad() As String
        Get
            If _DenUnidad Is Nothing Then
                Dim FiltroUNQA As Collection = Session("CFiltroUNQA")

                For Each UNQA As Collection In FiltroUNQA
                    If _Unidad = UNQA("ID") Then
                        _DenUnidad = UNQA("DEN")
                        Exit For
                    End If
                Next
            End If

            Return _DenUnidad
        End Get
    End Property
    ''' <summary>
    ''' Datos de las variables de calidad para la ficha de calidad
    ''' </summary>
    ''' <param name="dsDatosUNQA">Datos de las variables de calidad</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerVarCal(ByVal dsDatosUNQA As DataSet) As Collection
        Dim cfiltroVarCal As New Collection
        Dim cfiltro As New Collection()
        With dsDatosUNQA.Tables(4)
            cfiltro.Add(_Variable, "ID")
            cfiltro.Add(_Nivel, "NIVEL")
            cfiltro.Add(.Rows(0)("DEN"), "DEN")
            cfiltro.Add(True, "FILTRO")
            cfiltro.Add("TODOS", "MATQA")
            cfiltroVarCal.Add(cfiltro, _Variable)
        End With

        Return cfiltroVarCal
    End Function
    ''' <summary>Carga los textos de la página e inicializa los controles</summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    ''' <remarks>Se produce cuando el control de servidor se carga en el objeto Page. Tiempo maximo:0</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dsDatos As DataSet
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.PopupFichaProveedor

        _Unidad = Request("Key")
        _Subtipo = Request("Subtipo")
        _Variable = Request("Variable")
        _Nivel = Request("Nivel")
        _ProveCod = Request("codProv")
        _ProveDen = Request("DenProv")
        If Request("desdeGS") = 1 Then
            Dim oDetalle As FSNServer.Puntuacion = FSNServer.Get_Object(GetType(FSNServer.Puntuacion))
            dsDatos = oDetalle.DetallePuntuacion(_Variable, _Nivel, Me.Idioma, _ProveCod, _Unidad, _Subtipo)
            Session("CFiltroUNQA") = ObtenerUNQA(dsDatos)
            Session("CFiltroVarCal") = ObtenerVarCal(dsDatos)
        End If
        Dim sScript As String
        sScript = "var Usuario='" + FSNUser.Cod + "';"
        sScript &= "var rutaPM='" + ConfigurationManager.AppSettings("rutaPM") + "';"
        sScript &= "var rutaFS='" + ConfigurationManager.AppSettings("rutaFS") + "';"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsCellClickHandler", sScript)

        sScript = "var DivScrll='" + div_Label.ClientID + "'; var lblScrll='" & label2.ClientID & "';"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsScrollVirtual", sScript)

        MostrarPanelEncuestas()
    End Sub
    ''' <summary>
    ''' Datos de las unqa para la ficha de calidad
    ''' </summary>
    ''' <param name="dsDatosUNQA">Datos de las unqa</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerUNQA(ByVal dsDatosUNQA As DataSet) As Collection
        Dim cfiltroUNQA As New Collection
        Dim cfiltro As New Collection()
        If Not dsDatosUNQA.Tables(3).Rows.Count = 0 Then
            With dsDatosUNQA.Tables(3)
                cfiltro.Add(_Unidad, "ID")
                cfiltro.Add(_Nivel, "NIVEL")
                cfiltro.Add(.Rows(0)("DEN"), "DEN")
                cfiltroUNQA.Add(cfiltro, _Variable)
            End With
        End If
        'ObtenerFiltroUNQAHijos(cfiltroUNQA, dsDatosUNQA.Tables(0), dsDatosUNQA.Tables(1).Rows(0)("ID"), 1)
        Return cfiltroUNQA
    End Function
#Region "Fns ModalPopup"
    ''' <summary>Muestra el modal popup de Encuestas</summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo ejecucion:0,3seg.</remarks>
    Private Sub MostrarPanelEncuestas()
        lblVarCodDen.Text = dtDatosVC.Item("VC_DEN")
        lblDetalle.Text = Textos(0) 'Detalle de puntuación
        lblImprimir.InnerText = Textos(16)

        Dim oDetalle As FSNServer.Puntuacion = FSNServer.Get_Object(GetType(FSNServer.Puntuacion))
        Dim ds As DataSet = oDetalle.DetallePuntuacion(_Variable, _Nivel, Me.Idioma, _ProveCod, _Unidad, _Subtipo)
        Dim fila As New HtmlTableRow
        Dim celda As New HtmlTableCell
        Dim mi_label As New Label
        Dim TextAux As String

        'Proveedor:
        lblDetPuntProve.Text = Textos(5)
        lblDetPuntProveDat.Text = _ProveCod & " - " & _ProveDen
        'UnQA:
        lblEnUni.Text = Textos(6)
        lblEnUniDat.Text = DenUnidad
        'Puntuacion:
        lblPunt.Text = Textos(35)
        lblPuntDat.Text = modUtilidades.FormatNumber(_Puntos, FSNUser.NumberFormat) & "(" & _Calif & ")"
        'Lista Padres
        fila = New HtmlTableRow
        celda = New HtmlTableCell
        mi_label = New Label()
        mi_label.CssClass = "RotuloDetallePuntuacionFondoGrisBold"
        mi_label.Text = Textos(1) 'Variable:
        celda.Controls.Add(mi_label) 'etiqueta Variable
        For i As Integer = 1 To _Nivel
            mi_label = New Label()

            TextAux = "/"
            If i = _Nivel Then TextAux = ""

            mi_label.CssClass = "fp_celda_vc_" & CStr(i) & "_DetallePuntuacion"
            If i < _Nivel Then
                mi_label.Text = ds.Tables(0).Rows(0).Item("VAR" & CStr(i)) & TextAux
            Else
                mi_label.Text = lblVarCodDen.Text
            End If

            celda.Controls.Add(mi_label) 'Padre de nivel i
        Next
        fila.Controls.Add(celda)
        tblPadre.Controls.Add(fila)
        'Material Qa:
        lblMatQa.Text = Textos(2)
        lblMatQaDat.Text = ""
        If DBNullToStr(ds.Tables(0).Rows(0).Item("MAT")) <> "" Then lblMatQaDat.Text = ds.Tables(0).Rows(0).Item("MAT")
        'Origen:
        lblOrigen.Text = Textos(17) 'Origen:
        lblOrigenDat.Text = ds.Tables(0).Rows(0).Item("ORIGEN")
        'Formula:
        lblFormula.Text = Textos(36) 'Formula
        lblFormulaTexto.Text = ds.Tables(0).Rows(0).Item("FORMULA")

        ''''''''''''''
        'GrupoXi
        '''''''''''''' 
        Dim dvCampos As New DataView(ds.Tables(1))
        Dim dtCampos As DataTable = dvCampos.ToTable(True, "VARIABLEX")
        dtCampos.Columns.Add("DEN", GetType(String))
        For Each RowBD As DataRow In dtCampos.Rows
            RowBD("DEN") = DameTrozo(RowBD.Item("VARIABLEX"), DBNullToStr(ds.Tables(0).Rows(0).Item("DEN")))
        Next
        rptVariables.DataSource = dtCampos
        rptVariables.DataBind()

        'Encuestas    
        lblEncuestas.Text = Textos(45)
        Dim dsDatos As DataSet
        If Not IsPostBack Then
            Dim oEnc As FSNServer.Encuesta = FSNServer.Get_Object(GetType(FSNServer.Encuesta))
            dsDatos = oEnc.DatosPuntuacionEncuesta(_ProveCod, _Unidad, _Nivel, _Variable, ds.Tables(0).Rows(0).Item("FORMULA"))

            InsertarEnCache("dtDatos_" & FSNUser.Cod, dsDatos)
        Else
            dsDatos = CType(Cache("dtDatos_" & FSNUser.Cod), DataSet)
            m_bSaltarLayout = 1
        End If
        Dim lNombresCampoEncuesta As New List(Of Object)
        'Obtener los nombres de las columnas
        For Each iColumn As DataColumn In dsDatos.Tables("Encuestas").Columns
            If Not {"IdEncuesta", "FecAlta", "FecCumpl", "Peticionario", "Titulo", "Valor"}.Contains(iColumn.ColumnName) Then
                lNombresCampoEncuesta.Add(New With {.nombreValor = iColumn.ColumnName})
            End If
        Next
        Dim lValoresCampoEncuesta As New List(Of Object)
        Dim lValoresCampo As List(Of Object)
        Dim lEncuestas As New List(Of Object)
        Dim lTotalValoresCampo As New List(Of Object)
        'Obtener los valores de las encuestas
        For Each iRow As DataRow In dsDatos.Tables("Encuestas").Rows
            lValoresCampo = New List(Of Object)
            For Each iColumn As DataColumn In dsDatos.Tables("Encuestas").Columns
                Select Case iColumn.ColumnName
                    Case "IdEncuesta"
                        lValoresCampo.Add(New With {.valorCampo = iRow(iColumn.ColumnName)})
                    Case "Valor"
                        lTotalValoresCampo.Add(New With {.totalValor = modUtilidades.FormatNumber(DBNullToDbl(iRow(iColumn.ColumnName)), FSNUser.NumberFormat)})
                    Case Else
                        Select Case iColumn.DataType
                            Case System.Type.GetType("System.DateTime")
                                If IsDBNull(iRow(iColumn.ColumnName)) Then
                                    lValoresCampo.Add(New With {.valorCampo = String.Empty})
                                Else
                                    lValoresCampo.Add(New With {.valorCampo = modUtilidades.FormatDate(DBNullToSomething(iRow(iColumn.ColumnName)), FSNUser.DateFormat)})
                                End If
                            Case System.Type.GetType("System.Int16"), System.Type.GetType("System.Double")
                                lValoresCampo.Add(New With {.valorCampo = modUtilidades.FormatNumber(DBNullToDbl(iRow(iColumn.ColumnName)), FSNUser.NumberFormat)})
                            Case Else
                                lValoresCampo.Add(New With {.valorCampo = iRow(iColumn.ColumnName)})
                        End Select
                End Select
            Next
            lValoresCampoEncuesta.Add(New With {.idEncuesta = iRow("IdEncuesta"), .valoresCampo = lValoresCampo})
            lEncuestas.Add(New With {.idEncuesta = iRow("IdEncuesta")})
        Next

        Dim serializer As New JavaScriptSerializer
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "datosEncuestas") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "datosEncuestas", "var datosEncuestas = '" &
                                      serializer.Serialize({lEncuestas, lNombresCampoEncuesta, lValoresCampoEncuesta, lTotalValoresCampo, New With {.colspan = (lNombresCampoEncuesta.Count + 5),
                                                                .texto = Textos(50), .totalPuntuacion = modUtilidades.FormatNumber(_Puntos, FSNUser.NumberFormat)}}) & "';", True)
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosCabecera") Then
            Dim sVariableJavascriptTextosJScript As String = "var TextosCabecera = new Array();"
            sVariableJavascriptTextosJScript &= "TextosCabecera[0]='" & JSText(Textos(22)) & "';"
            sVariableJavascriptTextosJScript &= "TextosCabecera[1]='" & JSText(Textos(46)) & "';"
            sVariableJavascriptTextosJScript &= "TextosCabecera[2]='" & JSText(Textos(47)) & "';"
            sVariableJavascriptTextosJScript &= "TextosCabecera[3]='" & JSText(Textos(49)) & "';"
            sVariableJavascriptTextosJScript &= "TextosCabecera[4]='" & JSText(Textos(51)) & "';"
            sVariableJavascriptTextosJScript &= "TextosCabecera[5]='" & JSText(Textos(48)) & "';"
            sVariableJavascriptTextosJScript &= "TextosCabecera[6]='" & JSText(Textos(50)) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosCabecera", sVariableJavascriptTextosJScript, True)
        End If
    End Sub
    ''' <summary>
    ''' Saca de entre la Leyenda de las variables implicadas en una formula la descripción de la variable indicada
    ''' </summary>
    ''' <param name="Variable">Variable de la q sacar su descripción</param>
    ''' <param name="DenomVariable">Leyenda de las variables implicadas en una formula</param>
    ''' <returns>descripción de la variable indicada</returns>
    ''' <remarks>Llamada desde: MostrarPanelNC; Tiempo máximo:0</remarks>
    Private Function DameTrozo(ByVal Variable As String, ByVal DenomVariable As String) As String
        Dim Aux As String = ""
        If InStr(DenomVariable, Variable & ":") > 0 Then
            Aux = DenomVariable.Substring(InStr(DenomVariable, Variable & ":") - 1)
        Else
            Return ""
        End If
        If InStr(Aux, ";") = 0 Then
            DameTrozo = Aux.Substring((Variable & ":").Length + 1)
        Else
            DameTrozo = Aux.Substring((Variable & ":").Length + 1, InStr(Aux, ";") - (Variable & ":").Length - 2)
        End If
    End Function
#End Region
End Class