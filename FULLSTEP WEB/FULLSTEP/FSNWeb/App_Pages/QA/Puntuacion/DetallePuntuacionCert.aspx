﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DetallePuntuacionCert.aspx.vb" Inherits="Fullstep.FSNWeb.DetallePuntuacionCert" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head  id="Head1" runat="server">
    <title runat="server" id="PageCaption"></title>
    <script type="text/javascript" language="javascript">
        /* <summary>
        ''' Abre el popup para ver una lista de archivos   
        ''' </summary>
        ''' <param name="urlcompleta">url donde mostrar el archivo con todos los parametros cargados</param>
        ''' <remarks>Llamada desde: Todos los dataentry de tipo archivo q contengan mas de un archivo; Tiempo máximo: 0,3</remarks>*/        
        function AbrirFicherosAdjuntos(urlcompleta){
            window.open(urlcompleta, "_blank", "width=750,height=330,status=yes,resizable=no,top=200,left=200")
        }
        /* <summary>
        ''' Abre el popup para ver un texto   
        ''' </summary>
        ''' <param name="urlcompleta">url donde mostrar el texto</param>
        ''' <remarks>Llamada desde: Todos los dataentry de tipo texto medio,largo y string ; Tiempo máximo: 0,3</remarks>*/
        function AbrirPopUpTexto(urlcompleta) {
            window.open(urlcompleta, "_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250")
        }
        /* <summary>
        ''' Abre la pantalla de partidas presupuestarias
        ''' </summary>
        ''' <param name="urlcompleta">url donde mostrar la partida</param>
        ''' <param name="texto">partida</param>        
        ''' <remarks>Llamada desde: Todos los dataentry de tipo partida presupuestaria ; Tiempo máximo: 0,3</remarks>*/
        function AbrirDetallePartida(urlcompleta, texto) {
            window.open(urlcompleta + "&texto=" + escape(texto), "_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250")
        }
        /* <summary>
        ''' Abre la pantalla de activos
        ''' </summary>
        ''' <param name="urlcompleta">url donde mostrar el activo</param>
        ''' <param name="texto">activo</param>        
        ''' <remarks>Llamada desde: Todos los dataentry de tipo activo ; Tiempo máximo: 0,3</remarks>*/
        function AbrirDetalleActivo(urlcompleta, valor) {
            window.open(urlcompleta + "&codActivo=" + escape(valor), "_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
        }
        /* <summary>
        ''' Abre la pantalla de activos
        ''' </summary>
        ''' <param name="urlcompleta">url donde mostrar el activo</param>
        ''' <param name="texto">activo</param>        
        ''' <remarks>Llamada desde: Todos los dataentry de tipo activo ; Tiempo máximo: 0,3</remarks>*/
        function AbrirDetalleTipoPedido(texto) {
            window.open("../_common/DetalleTipoPedido.aspx?cod=" + texto, "_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");        
        }
        /* <summary>
        ''' Abre el popup para ver un archivo
        ''' </summary>
        ''' <param name="urlcompleta">url donde mostrar el archivo con todos los parametros cargados</param>
        ''' <remarks>Llamada desde: Todos los dataentry de tipo archivo q contengan un unico archivo; Tiempo máximo: 0,3</remarks>*/
        function IrAAdjunto(urlcompleta) {
            var NombreArchivo = urlcompleta.split("&nombre=")[1].split("&datasize=")[0]
            var NombreArchivoCodificado = encodeURIComponent(NombreArchivo)
            var URLCodificada = urlcompleta.replace(NombreArchivo, NombreArchivoCodificado)

            var newWindow = window.open(URLCodificada, "_blank", "width=600,height=275,status=no,resizable=yes,top=200,left=200");
            newWindow.focus();

        }    

    </script>
</head>
<body class="FondoTablaDetallePuntuacion">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>    
          <asp:Panel ID="Panel1" runat="server">        
            <div style='width:98%;z-index:10000; position:absolute;'>
                <div style='height:30px;' class='CapaTituloPanelInfo'>&nbsp;</div>
                <div style='height:25px;' class='CapaSubTituloPanelInfo'>&nbsp;</div>
            </div>

            <table style="position:absolute;z-index:10002; width:50%">
                <tr>
                    <td valign="top" align="left" style="padding:0px;width:80px;height:55px">
                        <asp:Image ID="imgDetalle" runat="server" SkinID="PopUpProveedor" Visible="true" />
                    </td>
                </tr>                
            </table>      
            
            <table style="position:absolute;z-index:10001;width:98%;" cellpadding="0" cellspacing="0">
            <tr>
                <td valign="middle" align="left" style="width:75%;padding-left:80px;height:30px;">
                    <asp:Label runat="server" ID="lblVarCodDen" Text="DlblVarCodDen" CssClass="TituloPopUpPanelInfo"></asp:Label>  
                </td>  
                <td valign="middle" style="text-align:right; width:25%">
                    <asp:ImageButton ID="btnImprimir" runat="server" SkinID="Imprimirph" OnClientClick="window.print();return false;"/>
                    <a runat="server" id="lblImprimir" class="TextoBlanco" href="javascript:window.print();">DImprimir</a>
                </td>
            </tr>
            <tr>
                <td valign="middle" align="left" style="width:75%;padding-left:80px;height:25px;">
                    <asp:Label runat="server" ID="lblDetalle" Text="dDetalle de puntuación" CssClass="SubTituloPopUpPanelInfo"></asp:Label>
                </td>
                <td style="width:50px">
                    &nbsp;
                </td>
            </tr>   
            <tr>
                <td colspan="2" class="SeparadorCabeceraDetallePuntuacion"> &nbsp;</td>
            </tr>         
            <tr>
                <td colspan="2" class="FondoTablaDetallePuntuacion">
                    <div>
                        <asp:Label ID="lblDetPuntProve" runat ="server" Text ="DDetalle de puntuación del proveedor" CssClass="RotuloDetallePuntuacion"></asp:Label>
                        &nbsp;
                        <asp:Label ID="lblDetPuntProveDat" runat ="server" Text ="DProvecod - proveden" CssClass="RotuloDetallePuntuacionBold"></asp:Label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="FondoTablaDetallePuntuacion">
                    <div>
                        <asp:Label ID="lblEnUni" runat ="server" Text ="Den la unidad de negocio" CssClass="RotuloDetallePuntuacion"></asp:Label>
                        &nbsp;
                        <asp:Label ID="lblEnUniDat" runat ="server" Text ="DProvecod - proveden" CssClass="RotuloDetallePuntuacionBold"></asp:Label>
                    </div>
                </td>
            </tr>   
            <tr>
                <td colspan="2" class="SeparadorLineasDetallePuntuacion"> &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" class="FondoTablaDetallePuntuacion">
                    <table class="BordearDetallePuntuacion">
                        <tr>
                            <td>
                                <asp:Label ID="lblPunt" runat ="server" Text ="DPuntuacion" CssClass="RotuloDetallePuntuacionBoldLeft"></asp:Label>
                            </td>
                            <td style="width:3px">
                                &nbsp;
                            </td>
                            <td>
                                <asp:Label ID="lblPuntDat" runat ="server" Text ="DPuntuacion (Calificacion)" CssClass="RotuloDetallePuntuacionPuntCalf"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>              
            <tr>
                <td colspan="2" class="SeparadorLineasDetallePuntuacion"> &nbsp;</td>
            </tr>       
            <tr>
                <td colspan="2" class="FondoTablaDetallePuntuacion">
                    <table style="width:95%;" class="FondoGrisTablaDetallePuntuacion">
                    <tr>
                    <td class="RotuloDetallePuntuacionFondoGris" >
                        <asp:Panel id="pnlPadre" runat="server" ScrollBars="None">
                            <table id="tblPadre" runat="server" border="0" style="WIDTH: 100%; height:12px;" cellpadding="0" cellspacing="0">
                            </table>
                        </asp:Panel>            
                    </td>
                    </tr>            
                    <tr>
                        <td class="RotuloDetallePuntuacionFondoGris">
                            <div>
                                <asp:Label ID="lblMatQa" runat ="server" Text ="DMaterial Qa" CssClass="RotuloDetallePuntuacionFondoGrisBold"></asp:Label>
                                &nbsp;
                                <asp:Label ID="lblMatQaDat" runat ="server" Text ="DDen Material Qa"></asp:Label>
                            </div>
                        </td>
                    </tr>            
                    <tr>
                        <td class="RotuloDetallePuntuacionFondoGris">
                            <div>
                                <asp:Label ID="lblOrigen" runat ="server" Text ="DOrigen" CssClass="RotuloDetallePuntuacionFondoGrisBold"></asp:Label>
                                &nbsp;
                                <asp:Label ID="lblOrigenDat" runat ="server" Text ="DIso - 900"></asp:Label>
                            </div>
                        </td>
                    </tr>      
                    </table>            
                </td>   
            </tr>                                       
            <tr>
                <td colspan="2" class="SeparadorLineasDetallePuntuacion"> &nbsp;</td>
            </tr> 
            <tr>
                <td colspan="2" class="FondoTablaDetallePuntuacion">
                    <table style="width:95%;" class="FondoGrisTablaDetallePuntuacion">
                        <tr>            
                            <td class="RotuloDetallePuntuacionFondoGrisBold">
                                <asp:Label runat="server" ID="lblFormula" Text="dFormula:"></asp:Label>
                            </td>
                        </tr>          
                        <tr>
                            <td class="RotuloDetallePuntuacionFondoGrisPad" style="overflow:auto">
                                <asp:Label runat="server" ID="lblFormulaTexto" Text="dFormulaCompleta"></asp:Label>
                            </td>
                        </tr>    
                    </table>
                </td>
            </tr>              
            <tr>
                <td colspan="2" class="SeparadorLineasDetallePuntuacion"> &nbsp;</td>
            </tr>      
            <tr>
                <td colspan="2" class="FondoTablaDetallePuntuacion">
                    <table style="width:60%;" class="FondoGrisBorderTablaDetallePuntuacion">
                        <tr>
                            <td>
                                <asp:Panel id="panelX" runat="server" ScrollBars="None">
                                    <table id="tblVarsX" runat="server" style="WIDTH: 400px;" cellpadding="0" cellspacing="0"></table>
                                </asp:Panel>
                            </td>     
                       </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="SeparadorLineasDetallePuntuacion"> &nbsp;</td>
            </tr>                
            <tr>                  
                <td colspan="2" class="FondoTablaDetallePuntuacion">
                    <table id="TblDetalleCertif" class="BordearDetallePuntuacion2" width="95%" runat="server">
                        <tr class="FondoLineaBorderTablaDetallePuntuacion">           
                            <td colspan ="4">
                                <asp:Label runat="server" ID="lblDetalleEtq" Text="dDetalle certificado" CssClass="RotuloDetallePuntuacionFondoGrisBold"></asp:Label>
                                &nbsp;
                                <asp:Label runat="server" ID="lblDetalleDat" Text="dIso proveden provecod" CssClass="RotuloDetallePuntuacionNoLeft"></asp:Label>
                            </td>
                        </tr>
                        <tr class="FondoGrisTablaDetallePuntuacion">
                            <td><asp:Label runat="server" ID="lblFecAlta" Text="dFecha de alta" CssClass="RotuloDetallePuntuacionBoldAzul"></asp:Label></td>
                            <td><asp:Label runat="server" ID="lblFecAltaDat" Text="" CssClass="RotuloDetallePuntuacionFondoGris"></asp:Label></td>
                            <td><asp:Label runat="server" ID="lblFecResp" Text="dFecha de respuesta" CssClass="RotuloDetallePuntuacionBoldAzul"></asp:Label></td>
                            <td><asp:Label runat="server" ID="lblFecRespDat" Text="" CssClass="RotuloDetallePuntuacionFondoGris"></asp:Label></td>
                        </tr>
                        <tr class="FondoGrisTablaDetallePuntuacion">
                            <td><asp:Label runat="server" ID="lblFecSolic" Text="dFecha de solicitud" CssClass="RotuloDetallePuntuacionBoldAzul"></asp:Label></td>
                            <td><asp:Label runat="server" ID="lblFecSolicDat" Text="" CssClass="RotuloDetallePuntuacionFondoGris"></asp:Label></td>
                            <td><asp:Label runat="server" ID="lblFecDespubli" Text="dFecha de despublicacion" CssClass="RotuloDetallePuntuacionBoldAzul"></asp:Label></td>
                            <td><asp:Label runat="server" ID="lblFecDespubliDat" Text="" CssClass="RotuloDetallePuntuacionFondoGris"></asp:Label></td>
                        </tr>
                        <tr class="FondoGrisTablaDetallePuntuacion">
                            <td><asp:Label runat="server" ID="lblFecLimCumpl" Text="dFecha limite de cumplimentación" CssClass="RotuloDetallePuntuacionBoldAzul"></asp:Label></td>
                            <td><asp:Label runat="server" ID="lblFecLimCumplDat" Text="" CssClass="RotuloDetallePuntuacionFondoGris"></asp:Label></td>
                            <td><asp:Label runat="server" ID="lblVersion" Text="dVersion" CssClass="RotuloDetallePuntuacionBoldAzul"></asp:Label></td>
                            <td><asp:Label runat="server" ID="lblVersionDat" Text="" CssClass="RotuloDetallePuntuacionFondoGris"></asp:Label></td>
                        </tr>
                    </table>
                    <asp:Label runat="server" ID="lblEnProceso" Text="dEn proceso" CssClass="RotuloDetallePuntuacionBoldMargin"></asp:Label>
                </td>   
            </tr> 
            <tr>
                <td colspan="2" class="SeparadorLineasDetallePuntuacion"></td>
            </tr>   
            <tr>
                <td colspan="2" class="FondoTablaDetallePuntuacion">
                    <iglc:WebTab ID="uwtGrupos" runat="server" Width="95%" Height="98%"></iglc:WebTab>                
                </td>
            </tr>    
            <tr>
                <td colspan="2" class="SeparadorLineasDetallePuntuacion"></td>
            </tr>                     
        </table>
        </asp:Panel>
    </form>
</body>
</html>
