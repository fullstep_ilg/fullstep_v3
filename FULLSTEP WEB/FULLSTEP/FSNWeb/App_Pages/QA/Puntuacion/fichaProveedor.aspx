﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.Master" CodeBehind="fichaProveedor.aspx.vb" Inherits="Fullstep.FSNWeb.fichaProveedor" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    <script type="text/javascript" language="javascript">
        var _grid;
        var tableCabecera;
        var tableFilas;
        var divScrollH;
        /* Inicializa la variable _grid */
        function InitializeGrid() {
            _grid = $find("<%=whdgProveedores.ClientId%>");
        };
        /* <summary>
       Oculta la columna que indica la Key de columnaKey
       </summary>
       <param name="columnaKey">Key de la columna</param>
       <remarks>Llamada desde: Icono de cerrar de la columnas</remarks> */
        function OcultarColumna(columnaKey) {
            var columna = _grid.get_gridView().get_columns().get_columnFromKey(columnaKey)
            columna.set_hidden(true);
            VisibilidadColumnas();
        }
        /* <summary>
       Muestra las columnas de puntaciones de las unidades de negocio que se indican en los parametros de entrada
       </summary>
       <param name="sIdHijos">Lisa de Ids de las unidades de negocio a mostrar</param>
       <param name="sPreKeyCabecera">El prefijo de las key de las columnas de punt a desplegar</param>
       <remarks>Llamada desde: Icono de desplegar de la columnas de unidadades de negoico</remarks> */
        function DesplegarColumnaUnqa(sIdHijos, sPreKeyCabecera) {
            var aColumnasKeys = sIdHijos.split(",");
            var lColumnas = aColumnasKeys.length;
            var iArray = 0;
            var columna;
            while (iArray < lColumnas) {
                columna = _grid.get_gridView().get_columns().get_columnFromKey(sPreKeyCabecera + "_" + aColumnasKeys[iArray]);
                if (columna.get_hidden()) {
                    columna.set_hidden(false);
                }
                iArray += 1;
            };
            VisibilidadColumnas();
        };
        /* <summary>
        Muestra las columnas de de fórmula y leyenda
        </summary>
        <param name="KeyFormula">Key de la columna de la formuka</param>
        <param name="KeyLeyenda">Key de la columna de la leyenda</param>
        <remarks>Llamada desde: Icono de fórmula dela columna de la variable de calidad</remarks>*/
        function DesplegarColumnaVariable(KeyFormula, KeyLeyenda) {
            var columna = _grid.get_gridView().get_columns().get_columnFromKey(KeyFormula)
            if (columna.get_hidden()) {
                columna.set_hidden(false);
            }
            columna = _grid.get_gridView().get_columns().get_columnFromKey(KeyLeyenda)
            if (columna.get_hidden()) {
                columna.set_hidden(false);
            }
            VisibilidadColumnas();
        };
        /* <summary>
        Muestra las columnas de de calificación y fecha de actualización de las puntuaciones
        </summary>
        <param name="KeyCalificacion">Key de la columna de la calificación o de fecha de act</param>
        <remarks>Llamada desde: Icono de ver calif o de ver fecha de la col de unidades de negocio</remarks> */
        function DesplegarColumnaCalificacion(KeyCalificacion) {
            var columna = _grid.get_gridView().get_columns().get_columnFromKey(KeyCalificacion)
            if (columna.get_hidden()) {
                columna.set_hidden(false);
            }
            VisibilidadColumnas();
        }
        /* <summary>
        Vuelve al panel de calidad inicializaCache indica si se han almacenadopunt y por lo tanto hay que 
        inicializar la caché. Tambien le indico a panelCalidad en q pagina y con q orden de grid estaba.
        </summary>
        <param name="KeyCalificacion">Key de la columna de la calificación o de fecha de act</param>
        <remarks>Llamada desde: btnVolver</remarks> */
        function Volver() {
            try {
                window.location.href = 'panelCalidad.aspx?C=' + inicializaCache + '&PaginaVolver=' + document.getElementById("<%=PaginaVolver.ClientID%>").value + '&OrdenVolver=' + document.getElementById("<%=OrdenVolver.ClientID%>").value + '&OrdenacionVolver=' + document.getElementById("<%=OrdenacionVolver.ClientID%>").value
            } catch (err) {
                try {
                    window.location.href = 'panelCalidad.aspx?PaginaVolver=' + document.getElementById("<%=PaginaVolver.ClientID%>").value + '&OrdenVolver=' + document.getElementById("<%=OrdenVolver.ClientID%>").value + '&OrdenacionVolver=' + document.getElementById("<%=OrdenacionVolver.ClientID%>").value
                } catch (err2) {
                    window.location.href = 'panelCalidad.aspx'
                }
            }
            return false;
        };
        /* ''' Revisado por: Jbg. Fecha: 28/11/2011
        <summary>
        Abre en popup la ventana de envio de email pasándole el email del contacato de calidad del proveedor
        </summary>
        <remarks>Llamada desde: btnEnviarEmail; Hay que pasar el  porque la página de enviar email es de PMweb</remarks> */
        function EnviarEmail() {
            var listaProveedores = '';
            var hProveEmail = document.getElementById("<%=hProveEmail.ClientID%>");
            var hProveCod = document.getElementById("<%=hProveCod.ClientID%>");

            if (hProveEmail.value == "##") {
                //Caso de múltiples contactos: obtenerlos
                $.when($.ajax({
                    type: "POST",
                    url: rutaFS + 'QA/Puntuacion/panelCalidad.aspx/ObtenerContactosQA',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ Proveedor: hProveCod.value }),
                    async: false
                })).done(function (msg) {
                    for (i = 0; i < msg.d.length; i++) {
                        listaProveedores = listaProveedores + msg.d[i] + ";"
                    };
                });

                listaProveedores = String(listaProveedores).substring(0, listaProveedores.length - 1);
            } else {
                listaProveedores = hProveEmail.value;
            }

            window.open("<%=System.Configuration.ConfigurationManager.AppSettings("rutaQA")%>_common/MailProveedores.aspx?Origen=1&Proveedores=" + listaProveedores, "_blank", "width=710,height=360,status=yes,resizable=no,top=180,left=200");
            return false;
        };
        function MostrarPopUpVariableManual(den,valor,observacion) {
            OcultarCargando();
            $('#pnlVariableManual #lblVariableManual').text(den);
            $('#pnlVariableManual input').numericFormatted('val', valor);
            $('#pnlVariableManual textarea').val(observacion);
            MostrarFondoPopUp();
            $('#pnlVariableManual').show();
            CentrarPopUp($('#pnlVariableManual'));
            $('#pnlVariableManual input').focus();
        }
        function MostrarFondoPopUp() {
            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').css('z-index', 10002);
            $('#popupFondo').show();
        };
        function OcultarCargando() {
            var modalprog = $find(ModalProgress);
            if (modalprog) modalprog.hide();
        };
        var rowVariableManualSeleccionada, cellVariableManualSeleccionada;
        /* <summary>
        Muestra el detalle de una variable ppm ó nc ó certificado ó cargo ó tasa ó encuesta
        </summary>
        <remarks>Llamada desde: whdgProveedores SelectionClientEvents</remarks>*/
        function grid_CellClick(sender, e) {
            var hProveCod = document.getElementById("<%=hProveCod.ClientID%>");
            var hProveDen = document.getElementById("<%=hProveDen.ClientID%>");

            if (e.get_type() == "cell" && e.get_item().get_row().get_index() !== -1) {
                if (e.get_item().get_grid().get_behaviors().get_activation().get_activeCellResolved() !== null) {
                    var cell = e.get_item().get_grid().get_behaviors().get_activation().get_activeCellResolved();
                    var row = cell.get_row();
                    var Unidad = cell.get_column().get_key();
                    
                    if ($(cell.get_element()).hasClass('fp_celda_editable')) {
                        MostrarCargando();
                        rowVariableManualSeleccionada = row;
                        cellVariableManualSeleccionada = cell.get_column().get_key();
                        
                        var sObservaciones;
                        var lObservaciones = $.parseJSON(row.get_cellByColumnKey('OBSERVACIONES').get_value());
                        if (lObservaciones == '' || lObservaciones == null) sObservaciones = '';
                        else sObservaciones = lObservaciones[cellVariableManualSeleccionada];
                        MostrarPopUpVariableManual(row.get_cellByColumnKey('VC_DEN').get_value(), row.get_cellByColumnKey(cell.get_column().get_key()).get_value(), sObservaciones);                        
                    } else {
                        if (Unidad.toString().search("PUNT") > -1) {
                            Unidad = Unidad.toString().substring(5);//PUNT_?

                            //Si la celda está vacía que no haga nada
                            var valor = row.get_cellByColumnKey(cell.get_column().get_key()).get_value();
                            //Si valor=0 la comparación valor=="" devuelve true porque convierte "" a número, es decir a 0
                            if (typeof (valor) == "string" && valor == "") return false;
                        } else {
                            if (Unidad.toString().search("CAL") > -1) {
                                Unidad = Unidad.toString().substring(4);//CAL_?
                            } else {
                                if (Unidad.toString().search("FECACT") > -1) {
                                    Unidad = Unidad.toString().substring(7);//FECACT_?
                                } else {
                                    e.set_cancel(true);
                                    return false;
                                }
                            }
                        }

                        var Subtipo = row.get_cellByColumnKey("SUBTIPO").get_value();
                        var Variable = row.get_cellByColumnKey("VC_ID").get_value();
                        var Nivel = row.get_cellByColumnKey("VC_NIVEL").get_value();
                        var Aplica = row.get_cellByColumnKey(cell.get_column().get_key()).get_value();

                        if (Subtipo == 0 || Subtipo == 1 || Subtipo == 2 || Subtipo == null || (Aplica == null)) {
                            e.set_cancel(true);
                            return false;
                        } else {
                            switch (Subtipo) {
                                case 3:
                                    window.open("DetallePuntuacionCert.aspx?Key=" + Unidad + "&Subtipo=" + Subtipo + "&Variable=" + Variable + "&Nivel=" + Nivel + "&codProv=" + hProveCod.value + "&DenProv=" + hProveDen.value, "_blank", "width=950,height=545,status=yes,resizable=yes,scrollbars=yes,top=120,left=20");
                                    break;
                                case 4:
                                    window.open("DetallePuntuacionNc.aspx?Key=" + Unidad + "&Subtipo=" + Subtipo + "&Variable=" + Variable + "&Nivel=" + Nivel + "&codProv=" + hProveCod.value + "&DenProv=" + hProveDen.value, "_blank", "width=735,height=580,status=yes,resizable=yes,scrollbars=yes,top=90,left=250");
                                    break;
                                case 8:
                                    window.open("DetallePuntuacionEncuesta.aspx?Key=" + Unidad + "&Subtipo=" + Subtipo + "&Variable=" + Variable + "&Nivel=" + Nivel + "&codProv=" + hProveCod.value + "&DenProv=" + hProveDen.value, "_blank", "width=720,height=620,status=yes,resizable=yes,scrollbars=yes,top=70,left=250");
                                    break;
                                default:
                                    window.open("DetallePuntuacionPpmCargoTasa.aspx?Key=" + Unidad + "&Subtipo=" + Subtipo + "&Variable=" + Variable + "&Nivel=" + Nivel + "&codProv=" + hProveCod.value + "&DenProv=" + hProveDen.value, "_blank", "width=720,height=620,status=yes,resizable=yes,scrollbars=yes,top=70,left=250");
                                    break;
                            }
                        }
                    }
                }
            }
        }
        /*''' Revisado por: Jbg. Fecha: 20/10/2011
        <summary>
        Provoca el postback para hacer la esportación a excel, y guarda las columnas visibles en cliente
        </summary>
        <remarks>Llamada desde: Icono de exportar a Excel</remarks> */
        function MostrarExcel() {
            VisibilidadColumnas();
            __doPostBack("", "Excel");
        };
        /*''' Revisado por: Jbg. Fecha: 20/10/2011
        <summary>
        Provoca el postback para hacer la esportación a Pdf, y guarda las columnas visibles en cliente
        </summary>
        <remarks>Llamada desde: Icono de exportar a Pdf</remarks> */
        function MostrarPdf() {
            VisibilidadColumnas();
            __doPostBack("", "Pdf");
        };
        /*''' <summary>
        Guarda una lista separada por ; de las keys de las columnas visibles en cliente
        </summary>
        <remarks>Llamada desde: ibExcel y ibPdf</remarks> */
        function VisibilidadColumnas() {
            var colIndex;
            var sColumnasVisibles = '';
            for (colIndex = 0; colIndex < _grid.get_gridView().get_columns().get_length() ; colIndex++) {
                var columna = _grid.get_gridView().get_columns().get_column(colIndex);
                if (!columna.get_hidden()) {
                    sColumnasVisibles = sColumnasVisibles + columna.get_key() + ';'
                };
            };
            
            $.ajax({
                type: "POST",
                url: rutaFS + 'QA/Puntuacion/fichaProveedor.aspx/setColumnasVisibles',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ columnasVisibles: sColumnasVisibles }),
                async: true
            });
	        return true;
        };
        function PrintForm() {
            var printContent = document.getElementById('<%= pnlDetalleErrorCalculoPuntuaciones.ClientID %>');
            var windowUrl = 'about:blank';
            var uniqueName = new Date();
            var windowName = 'Print' + uniqueName.getTime();
            var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=0,height=0');

            printWindow.document.write(printContent.innerHTML);
            printWindow.document.close();
            printWindow.focus();
            printWindow.print();
            printWindow.close();
        };
        /*Revisado por: Jbg. Fecha: 20/10/2011
        <summary>
        Abre en popup la ventana de visor de notificaciones
        </summary>
        <remarks>Llamada desde: div con imgVisor y lblVisor; Tiempo máximo:0</remarks> */
        function VisorNotif() {
            var ProveCod = document.getElementById('<%= hProveCod.ClientID %>');
            var ProveDen = document.getElementById('<%= hProveDen.ClientID %>');

            window.open("<%=System.Configuration.ConfigurationManager.AppSettings("rutaFS")%>_common/Notificaciones/Notificaciones.aspx?desdePanelFicha=1&Proveedores='" + ProveCod.value + "'&DenProveedores=" + ProveDen.value, "_blank", "width=1000,height=730,status=yes,resizable=yes,scrollbars=yes,top=10,left=10");
            return false;
        };
        /* ''' Hago que sólo las celdas que he marcado como editables lo sean, sino salgo */
        function whdgProveedores_CellEditing_EnteringEditMode(sender, eventArgs) {
            var cell = eventArgs.getCell();
            var clase = cell._element.className;
            eventArgs.set_cancel(true);
        }
        $(document).ready(function () {
            $('#pnlVariableManual input').numeric({ decimal: '' + usuario.DecimalFmt + '', negative: true }).numericFormatted({
                decimalSeparator: UsuNumberDecimalSeparator,
                groupSeparator: UsuNumberGroupSeparator,
                decimalDigits: UsuNumberNumDecimals
            });
            $('#pnlVariableManual #btnAceptar').text(TextosPantalla[0]);
            $('#pnlVariableManual #btnCancelar').text(TextosPantalla[1]);
            $('#pnlVariableManual #lblValor').text(TextosPantalla[2]);
            $('#pnlVariableManual #lblObservaciones').text(TextosPantalla[3]);
            //		'Para ocultar o mostrar con javascript una columna de la grid, infragistics ha hecho que todos los elementos de cada columna compartan una misma clase css.
            //		'De ese modo, mediante una serie de funciones, recupera el nombre de esa clase css y modifica el estilo display de esa clase css para que todos los elementos 
            //		'que la comparten (la columna) se muestren o se oculten al mismo tiempo. 
            //		'El nombre de la clase se busca en la colección document.styleSheets. En esa colección se guardan un listado de las clases css usadas en la página (tanto internas como de enlaces externos).
            //		'El los postbacks, la función que agrega a esa colección las clases es: Infragistics.Web.UI.ControlMain.prototype.__appendStyles
            //		'Dentro de esta función, para agregar las clases css en IE, se usa el método addRule, que tiene 3 parámetros, uno de ellos opcional. 
            //		'El primero y el segundo, obligatorios, son: selector y declaration (http://www.javascriptkit.com/domref/stylesheet.shtml)
            //		'selector es el nombre de la clase css que se agrega (ej: .ig2d281703) y declaration es su contenido (ej: display:none;).
            //		'El problema es que el campo declaration es obligatorio, no puede ir vacío. De hecho, para añadir una clase css vacía bastaría con el parámetro declaration llevara un punto y coma ";"
            //		'Como no se ha hecho así, el método lanza un error, que los programadores de Infragistics capturan e ignoran mediante un catch(err)
            //		'El resultado es que esa clase deja de estar en la colección, por lo que, cuando se intenta ocultar una 
            //		'columna cuya clase css ya no está en document.styleSheets, se devuelve un error "'null' is null or not an object", 
            //		'dado que la función Infragistics.Web.UI.GridColumn.prototype.set_hidden no controla si el estilo existe porque 
            //		'necesariamente debería estar presente en la colección (ya que está presente en la página).
            //		'Para mantener la funcionalidad de ocultar la columna, he sobreescrito la función _get_hiddenStyleSheet de infragistics, 
            //		'de modo que cuando una clase css vaya vacía, el parámetro declaration sea "{};" en vez de "".
            //		'Así, todas las clases css se guardan correctamente en la colección document.styleSheets.
            if ($IG.GridColumn) {
                $IG.GridColumn.prototype._get_hiddenStyleSheet = function () {
                    var css = this._get_hiddenCss();

                    if (!css)// To avoid going through all of the cells, the Hidden property should be assigned on the server to generate a css class for it
                    {
                        css = this._owner.get_id() + "_col" + this.get_index() + "_hidden";
                        /* OK 9/6/2012 119844 - Header text got merged after a column is hidden on client and an update panel on the page fires a postback. */
                        css = css.toLowerCase();
                        var lastSS = document.styleSheets[document.styleSheets.length - 1];
                        /* DAY 3-22-11 69834- Cannot hide column on the client in IE 9 */
                        if (typeof (lastSS.addRule) != "undefined" && !$util.IsIE9Plus)
                            //La función addRule necesita dos parámetros, sino da error
                            //lastSS.addRule("." + css);
                            lastSS.addRule("." + css, "{};");
                        else
                            lastSS.insertRule("." + css + "{}", lastSS.length);

                        if (this._headerElement) {
                            this._headerElement.className += (this._headerElement.className ? " " : "") + css;

                            if (this._owner._hasHeaderLayout) {
                                /* Set the hidden css on the sizeHeaderRow */
                                var sizeHeaderRowTH = this._owner._elements["sizeHeaderRow"].childNodes;
                                for (var i = 0; i < sizeHeaderRowTH.length; i++) {
                                    if (sizeHeaderRowTH[i].getAttribute("key") == this._key) {
                                        sizeHeaderRowTH[i].className += (sizeHeaderRowTH[i].className ? " " : "") + css;
                                        break;
                                    }
                                }

                            }
                        }

                        var rows = this._owner.get_rows();
                        for (var i = 0; i < rows.get_length() ; i++) {
                            var cell = rows.get_row(i).get_cellByColumn(this);
                            var elem = cell.get_element();
                            if (elem)
                                elem.className += (elem.className ? " " : "") + css;
                        }

                        rows = this._owner._get_auxRows();
                        for (var i = 0; i < rows.length; i++) {
                            var cell = rows[i].get_cellByColumn(this);
                            var elem = cell.get_element();
                            if (elem)
                                elem.className += (elem.className ? " " : "") + css;
                        }

                        /* DAY 2/24/12 102953- JS exception when hiding column for first time on the client with a footer and no header */
                        if (this._footerElement)
                            this._footerElement.className += (this._footerElement.className ? " " : "") + css;

                        var args = { column: this, css: css };
                        this._owner._gridUtil._fireEvent(this._owner, "HideColumnCssCreated", args);

                        this._set_hiddenCss(css);
                    }

                    return $util.getStyleSheet(css);
                }
            }
        });
        $(document).on('click', '#btnCancelar', function () {
            CerrarPopUpVariableManual();
        });
        $(document).on('click', '#btnAceptar', function () {
            if (ComprobarValorObservacion()) {                
                rowVariableManualSeleccionada.get_cellByColumnKey(cellVariableManualSeleccionada).set_value($('#pnlVariableManual input').val());
                var lObservaciones = $.parseJSON(rowVariableManualSeleccionada.get_cellByColumnKey("OBSERVACIONES").get_value());
                if(lObservaciones=='' || lObservaciones==null) lObservaciones={};
                lObservaciones[cellVariableManualSeleccionada] = $('#pnlVariableManual textarea').val();
                rowVariableManualSeleccionada.get_cellByColumnKey("OBSERVACIONES").set_value(JSON.stringify(lObservaciones));
                
                if (lObservaciones[cellVariableManualSeleccionada] == '')
                    $(rowVariableManualSeleccionada.get_cellByColumnKey(cellVariableManualSeleccionada).get_element()).removeClass('imagenNota')
                else if(!$(rowVariableManualSeleccionada.get_cellByColumnKey(cellVariableManualSeleccionada).get_element()).hasClass('imagenNota'))
                    $(rowVariableManualSeleccionada.get_cellByColumnKey(cellVariableManualSeleccionada).get_element()).addClass('imagenNota');

                $('#pnlVariableManual input').val('');
                $('#pnlVariableManual textarea').val('');
                $('#popupFondo').hide();
                $('#pnlVariableManual').hide();
            } else {
                alert(TextosPantalla[4]);
            }
        });
        function CerrarPopUpVariableManual() {
            $('#pnlVariableManual input').val('');
            $('#pnlVariableManual textarea').val('');
            $('#popupFondo').hide();
            $('#pnlVariableManual').hide();
            rowVariableManualSeleccionada = null;
            cellVariableManualSeleccionada = null;
        };
        function ComprobarValorObservacion() {
            if ($('#pnlVariableManual textarea').val() !== '' && $('#pnlVariableManual input').val() == '') return false;
            return true;
        };
    </script>
    <style>
        tbody>tr>td.igg_FullstepSelectedCell{
            color:#000;
            background-image:none;
            background-color:transparent;
        }
    </style>
    <div style="margin-top: 10px; clear: both; width: 99%;">
        <fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
    </div>
    <table style="width: 99%;">
        <tbody>
            <tr>
                <td>
                    <asp:ImageButton runat="server" ID="imgErrorCalculoPuntuaciones" SkinID="ErrCalPunt" Style="float: left;" />
                    <fsn:FSNLinkInfo ID="imgDetalleProve" runat="server" Style="text-decoration: none;" PanelInfo="FSNPanelDatosProveedorQA">
                        <asp:ImageButton ID="ibDetalleProve" SkinID="Ayuda" BorderWidth="0" runat="server" />
                    </fsn:FSNLinkInfo>
                    <input id="hProveCod" type="hidden" runat="server" />
                    <input id="hProveDen" type="hidden" runat="server" />
                    <asp:Label ID="lblProveedor" runat="server" CssClass="Rotulo"></asp:Label>
                </td>
            </tr>
        </tbody>
    </table>

    <div class="CabeceraBotones" style="height: 25px; clear:both; line-height:25px; width:99%;">
        <div id="divAlmacenar" runat="server" class="botonDerechaCabecera" style="float: right; margin-right: 15px; display: none;">
            <asp:ImageButton ID="imgAlmacenar" runat="server" SkinID="Almacenar" />
            <asp:LinkButton ID="lnkAlmacenar" runat="server" SkinID="lnkFicha"></asp:LinkButton>
        </div>
        <div id="divRecalcular" runat="server" class="botonDerechaCabecera" style="float: right; display: none;">
            <asp:ImageButton ID="imgRecalcular" runat="server" SkinID="Recalcular" />
            <asp:LinkButton ID="lnkRecalcular" runat="server" SkinID="lnkFicha"></asp:LinkButton>
        </div>
        <div onclick="MostrarPdf();" class="botonDerechaCabecera" style="float: right;">
            <asp:Image runat="server" ID="imgPDF" CssClass="imagenCabecera" />
            <asp:Label runat="server" ID="lblPDF" CssClass="fntLogin2" Text="PDF"></asp:Label>
        </div>
        <div onclick="MostrarExcel();" class="botonDerechaCabecera" style="float: right;">
            <asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
            <asp:Label runat="server" ID="lblExcel" CssClass="fntLogin2" Text="Excel"></asp:Label>
        </div>
        <div onclick="return EnviarEmail();" class="botonDerechaCabecera" style="float: right;">
            <asp:Image runat="server" ID="imgEmail" CssClass="imagenCabecera" />
            <asp:Label runat="server" ID="lblEnviarEMail" CssClass="fntLogin2" Text="dEnviarEMail"></asp:Label>
            <input id="hProveEmail" type="hidden" runat="server" />
        </div>
        <div onclick="VisorNotif()" class="botonDerechaCabecera" style="float: right;">
            <asp:Image runat="server" ID="imgVisor" CssClass="imagenCabecera" />
            <asp:Label runat="server" ID="lblVisor" CssClass="fntLogin2" Text="dVisor"></asp:Label>
        </div>
    </div>    

    <asp:UpdatePanel ID="upWhdgProveedores" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="imgRecalcular" EventName="click" />
            <asp:AsyncPostBackTrigger ControlID="lnkRecalcular" EventName="click" />
            <asp:AsyncPostBackTrigger ControlID="imgAlmacenar" EventName="click" />
            <asp:AsyncPostBackTrigger ControlID="lnkAlmacenar" EventName="click" />
        </Triggers>
        <ContentTemplate>          
            <ig:WebHierarchicalDataGrid ID="whdgProveedores" runat="server" Width="99%" Height="550px" CssClass="pc_frameStyle"
                AutoGenerateBands="false"
                AutoGenerateColumns="false"
                EnableAjax="true"
                EnableAjaxViewState="false"
                EnableDataViewState="true">
                <EditorProviders>
                    <ig:NumericEditorProvider ID="NumericEditor">
                        <EditorControl DataMode="Double" Culture="es-ES"></EditorControl>
                    </ig:NumericEditorProvider>
                </EditorProviders>
                <Behaviors>
                    <ig:Activation Enabled="true" />
                    <ig:Selection RowSelectType="Single" Enabled="True" CellClickAction="Cell"></ig:Selection>
                    <ig:RowSelectors Enabled="false"></ig:RowSelectors>
                    <ig:ColumnResizing Enabled="true">
                        <ColumnSettings>
                            <ig:ColumnResizeSetting ColumnKey="VC_DEN" EnableResize="false" />
                        </ColumnSettings>
                    </ig:ColumnResizing>
                    <ig:EditingCore Enabled="true" EnableInheritance="True" BatchUpdating="true">
                        <Behaviors>
                            <ig:CellEditing Enabled="true" EnableInheritance="True">
                                <CellEditingClientEvents EnteringEditMode="whdgProveedores_CellEditing_EnteringEditMode" />
                                <EditModeActions EnableF2="false" MouseClick="Single" />
                            </ig:CellEditing>
                        </Behaviors>
                    </ig:EditingCore>
                </Behaviors>
                <ClientEvents Initialize="InitializeGrid" Click="grid_CellClick" />
            </ig:WebHierarchicalDataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="pnlVariableManual" class="popupCN" style="display: none; position: absolute; z-index: 10003; height: 25em; width: 25em; padding: 10px;">
        <span id="lblVariableManual" class="EtiquetaGrande" style="display: block; margin: 0.2em;"></span>
        <span id="lblValor" class="Rotulo" style="display: block; margin: 0.5em 1em;">DValor</span>
        <input type="text" style="width: 10em; margin: 0em 1em;" />
        <span id="lblObservaciones" class="Rotulo" style="display: block; margin: 0.5em 1em;">DObservaciones</span>
        <textarea id="txtComentarioRechazoEscalacion" rows="10" cols="35" style="display: block; margin: 0.5em 1em;"></textarea>
        <div style="text-align: center; margin: 1em;">
            <span id="btnAceptar" class="botonRedondeado">DAceptar</span>
            <span id="btnCancelar" class="botonRedondeado">DCancelar</span>
        </div>
    </div>
        
    <ig:WebExcelExporter ID="wdgExcelExporter" runat="server"></ig:WebExcelExporter>
    <ig:WebDocumentExporter ID="wdgPDFExporter" runat="server" ExportMode="Download"></ig:WebDocumentExporter>
    <input id="PaginaVolver" name="PaginaVolver" runat="server" type="hidden" />
    <input id="OrdenVolver" name="OrdenVolver" runat="server" type="hidden" />
    <input id="OrdenacionVolver" name="OrdenacionVolver" runat="server" type="hidden" />

    <ajx:ModalPopupExtender runat="server" ID="mErroresCalcPunt" Enabled="true"
        TargetControlID="btnHidden"
        CancelControlID="imgCerrar"
        PopupControlID="pnlDetalleErrorCalculoPuntuaciones"
        RepositionMode="RepositionOnWindowResizeAndScroll">
    </ajx:ModalPopupExtender>
    <asp:Panel runat="server" ID="pnlDetalleErrorCalculoPuntuaciones" CssClass="modalPopup" Style="display: none; width: 750px; padding-bottom: 20px; overflow: hidden;">
        <asp:Button runat="server" ID="btnHidden" Style="display: none;" />
        <div style="float: left; position: absolute; z-index: 10010;">
            <asp:Image runat="server" ID="imgPopUpErrCalcPunt" SkinID="TituloPopUpErrores" Style="padding-top: 5px; padding-left: 10px;" />
        </div>

        <div style="width: 100%; float: right; position: absolute; z-index: 10010; text-align: right;">
            <asp:ImageButton runat="server" ID="imgCerrar" SkinID="Cerrar" Style="padding-top: 2px; padding-right: 10px;" />
        </div>

        <asp:UpdatePanel ID="updDetalleError" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="imgErrorCalculoPuntuaciones" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <div class="CapaTituloPanelInfo" style="width: 100%; position: relative; z-index: 10000; height: 30px; line-height: 30px;">
                    <div style="float: left; padding-left: 75px; vertical-align: middle;">
                        <asp:Label runat="server" ID="lblTituloPopUp" SkinID="TituloPanelInfo" Style="padding-left: 5px" Text="DIncidencia en el cÃ¡lculo de puntuaciones"></asp:Label>
                    </div>
                    <div style="float: right; padding-right: 55px; vertical-align: middle;">
                        <asp:ImageButton runat="server" ID="imgPrint" SkinID="ImprimirPopUp" OnClientClick="PrintForm();return false;" Style="vertical-align: middle;" />
                        <asp:LinkButton runat="server" ID="lnkPrint" SkinID="lnkImprimirPopUp" OnClientClick="PrintForm();return false;" Text="DImprimir"></asp:LinkButton>
                    </div>
                </div>

                <div class="CapaSubTituloPanelInfo" style="width: 100%; position: relative; z-index: 10000; height: 30px; line-height: 30px;">
                    <div style="padding-left: 75px; vertical-align: middle;">
                        <asp:Label runat="server" ID="lblSubTituloPopUp" SkinID="SubTituloPanelInfo" Style="padding-left: 5px" Text="DDetalle de la incidencia"></asp:Label>
                    </div>
                </div>

                <div style="width: 100%; padding-top: 15px;">
                    <div style="padding-left: 10px; padding-right: 10px;">
                        <asp:Label runat="server" ID="lblInfoErrores" CssClass="ContenidoNormal"></asp:Label>
                    </div>
                </div>

                <div style="width: 100%; padding-top: 15px;">
                    <asp:Label runat="server" ID="lblTipoError" CssClass="Normal" Style="padding-left: 10px;"></asp:Label>
                </div>

                <asp:Panel runat="server" ID="pnlDetalleFormula" Style="padding-top: 20px; padding-left: 10px; padding-right: 10px;">
                    <div class="DetalleFormulaPopUp" style="width: 100%;">
                        <div style="width: 100%;">
                            <asp:Label runat="server" ID="lblTituloFormula" CssClass="Normal" Style="padding-left: 10px;"></asp:Label>
                        </div>
                        <div style="width: 100%; padding-top: 5px;">
                            <asp:Label runat="server" ID="lblFormula" CssClass="Normal" Style="padding-left: 10px;"></asp:Label>
                        </div>
                    </div>
                    <div style="width: 100%; padding-top: 20px; height: 30em; overflow: scroll;">
                        <table runat="server" id="tblDetalleFormulaError" cellpadding="0" cellspacing="0" border="0">
                        </table>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <fsn:FSNPanelInfo ID="FSNPanelDatosProveedorQA" runat="server" ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_DatosProveedor" TipoDetalle="1" />
</asp:Content>
