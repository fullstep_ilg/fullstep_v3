﻿Public Partial Class CertifSolicitadosOK
    Inherits FSNPage

    ''' <summary>
    ''' Carga los datos para mostrarlos en la pagina de ha sido posible solicitar/renovar
    ''' </summary>
    ''' <param name="sender">las del evento</param>
    ''' <param name="e">las del evento</param>        
    ''' <remarks>Llamada desde: Certificados\Certificados.aspx.vb/EjecutarSolicitarRenovar ; Tiempo máximo=0,1seg.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim bPublicados As Boolean
        Dim bWorkflow As Boolean
        Dim oRow As DataRow
        Dim oCelda As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oFila As System.Web.UI.HtmlControls.HtmlTableRow

        Dim sCertificadosEnProceso As String = Request("EnProceso")
        Dim sCertificados As String = Request("Certificados")

        Dim sIdi As String = FSNUser.Idioma.ToString

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.CertifSolicitadosOK

        imgEnProceso.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Icono_Error_Amarillo.gif"

        'MONTAR CABECERA
        '===============
        FSNPageHeader.VisibleBotonVolver = True
        FSNPageHeader.TextoBotonVolver = Textos(11)
        FSNPageHeader.OnClientClickVolver = "window.open(""" & Replace(Request.QueryString("volver"), "**", "&") & """,""_self"");return false;"

        Select Case Request("Accion")
            Case "solicitarCertificados"
                FSNPageHeader.TituloCabecera = Textos(0)
                Me.lblMensaje.Text = Textos(3)
            Case "renovarCertificados"
                FSNPageHeader.TituloCabecera = Textos(2)
                Me.lblMensaje.Text = Textos(4)
        End Select

        FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/certificados.gif"

        Me.lblMensajeW.Text = Textos(5)
        Me.lblProv1.Text = Textos(6)
        Me.lblProv2.Text = Textos(6)
        Me.lblProv3.Text = Textos(6)

        Me.lblTipoCertif1.Text = Textos(7)
        Me.lblTipoCertif2.Text = Textos(7)
        Me.lblTipoCertif3.Text = Textos(7)
        Me.lblIdent.Text = Textos(8)
        Me.lblAprobador.Text = Textos(9)

        'Ahora va generando la tabla de certificados emitidos y los que han ido a workflow:
        Dim oCertificados As FSNServer.Certificados
        If sCertificados <> "" Then
            oCertificados = FSNServer.Get_Object(GetType(FSNServer.Certificados))
            oCertificados.DevolverCertificadosEmitidos(sCertificados, sIdi)
            If oCertificados.Data.Tables(0).Rows.Count > 0 Then
                For Each oRow In oCertificados.Data.Tables(0).Rows
                    'Ahora añade el detalle de los items:
                    If oRow.Item("ESTADO") = TipoEstadoSolic.CertificadoPub Then
                        bPublicados = True

                        oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                        oCelda.InnerText = DBNullToSomething(oRow.Item("PROVECOD")) & " - " & DBNullToSomething(oRow.Item("PROVEDEN"))
                        oFila.Cells.Add(oCelda)
                        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                        oCelda.InnerText = DBNullToSomething(oRow.Item("TIPOCERTIF"))
                        oFila.Cells.Add(oCelda)

                        oFila.Attributes("class") = "fntLogin"
                        Me.tblPub.Rows.Add(oFila)


                    Else
                        bWorkflow = True
                        oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                        oCelda.InnerText = DBNullToSomething(oRow.Item("PROVECOD")) & " - " & DBNullToSomething(oRow.Item("PROVEDEN"))
                        oFila.Cells.Add(oCelda)
                        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                        oCelda.InnerText = DBNullToSomething(oRow.Item("TIPOCERTIF"))
                        oFila.Cells.Add(oCelda)
                        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                        oCelda.InnerText = DBNullToSomething(oRow.Item("INSTANCIA"))
                        oFila.Cells.Add(oCelda)

                        oFila.Attributes("class") = "fntLogin"
                        Me.tblWork.Rows.Add(oFila)
                    End If

                Next
            End If
        End If

        'Comprobamos si hay algun certificado en proceso
        If sCertificadosEnProceso <> "" Then
            Dim oListaCert() As String
            Dim oDatos() As String
            oListaCert = sCertificadosEnProceso.Split(";")
            Dim i As Integer


            Me.lblEnProceso.Text = Textos(10)

            For i = 0 To oListaCert.Length - 1
                oDatos = oListaCert(i).Split("_")

                'Lineas detalle

                oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                'Proveedor
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = oDatos(0) & "-" & oDatos(1)
                oFila.Cells.Add(oCelda)
                'Tipo Certificado
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = oDatos(2)
                oFila.Cells.Add(oCelda)
                oFila.Attributes("class") = "fntLogin"
                Me.tblEnProcesoIn.Rows.Add(oFila)

            Next

            Me.Linea3.Visible = True
            tblEnProceso.Visible = True
            tblEnProcesoIn.Visible = True
        Else
            Me.Linea3.Visible = False
            tblEnProceso.Visible = False
            tblEnProcesoIn.Visible = False
            tblEnProcesoIn.Visible = False
        End If

        If bWorkflow = False Then
            Me.Linea2.Visible = False
            Me.tblWork.Visible = False
            Me.lblMensajeW.Visible = False
        End If

        If bPublicados = False Then
            Me.Linea1.Visible = False
            Me.tblPub.Visible = False
            Me.lblMensaje.Visible = False
        End If

        oCertificados = Nothing
    End Sub

End Class