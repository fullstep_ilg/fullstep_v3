﻿Imports Fullstep.FSNServer
Imports Infragistics.Web.UI
Imports Infragistics.Web.UI.GridControls
Imports Infragistics.Documents.Reports
Imports Infragistics.Documents.Reports.Report
Imports System.Web.Script.Serialization

Public Class Certificados
    Inherits FSNPage
    Private m_bConConfiguracionAnterior As Boolean = False
    Private _pagerControl As wucPagerControl
    Private _dsCertificados As DataSet
    Private _pageNumber As Integer = 0
    ''' <summary>
    ''' Devuelve los certificados
    ''' </summary>
    ''' <returns>Dataset con los certificados</returns>
    ''' <remarks>Llamada desde: CargarCertificados, Tiempo maximo: 0,2</remarks>
    Protected ReadOnly Property Certificados() As DataSet
        Get
            Dim bOrdenacionEnBDVacio As Boolean = False
            Dim bFilterBool As Boolean
            If CType(hFiltered.Value, Boolean) OrElse CType(hGrouped.Value, Boolean) Then
                whgCertificados.GridView.Behaviors.Paging.PageIndex = _pageNumber
                Dim dsAuxiliar As New DataSet
                dsAuxiliar = CType(Cache("dsCertificados_" & FSNUser.Cod), DataSet).Copy
                Dim sFiltro As String = String.Empty
                Dim sFilterCondition As String = String.Empty
                Dim fColumns As String() = Split(hFilteredColumns.Value, "#")
                Dim fConditions As String() = Split(hFilteredCondition.Value, "#")
                Dim fValues As String() = Split(hFilteredValue.Value, "#")
                If Not fColumns.Length = 1 OrElse fColumns(0) IsNot String.Empty Then
                    For i As Integer = 0 To fColumns.Length - 1
                        bFilterBool = False
                        sFilterCondition = ""
                        If Left(fColumns(i), 5) = "BOOL_" Then
                            fColumns(i) = Replace(fColumns(i), "BOOL_", "")
                        End If
                        Select Case UCase(dsAuxiliar.Tables("CERTIFICADOS").Columns(fColumns(i)).DataType.FullName)
                            Case "SYSTEM.DATETIME"
                                Select Case CType(fConditions(i), Integer)
                                    Case 2
                                        sFilterCondition = " < '" & fValues(i) & "'"
                                    Case 3
                                        sFilterCondition = " > '" & fValues(i) & "'"
                                    Case 5
                                        sFilterCondition = " = '" & Today & "'"
                                    Case Else
                                        sFilterCondition = " = '" & fValues(i) & "'"
                                End Select
                            Case "SYSTEM.STRING"
                                Select Case CType(fConditions(i), Integer)
                                    Case 2
                                        sFilterCondition = " NOT LIKE '" & fValues(i) & "'"
                                    Case 3
                                        sFilterCondition = " LIKE '" & fValues(i) & "%'"
                                    Case 4
                                        sFilterCondition = " LIKE '%" & fValues(i) & "'"
                                    Case 5
                                        sFilterCondition = " LIKE '%" & fValues(i) & "%'"
                                    Case 6
                                        sFilterCondition = " NOT LIKE '%" & fValues(i) & "%'"
                                    Case 7
                                        sFilterCondition = " IS NULL"
                                    Case 8
                                        sFilterCondition = " IS NOT NULL"
                                    Case Else
                                        sFilterCondition = " LIKE '" & fValues(i) & "'"
                                End Select
                            Case "SYSTEM.BOOLEAN", "SYSTEM.BYTE"
                                bFilterBool = True
                                sFilterCondition = "( 1=1 )"
                                'Todo el filtrado lo hace infragistics
                            Case "SYSTEM.INT32"
                                'unicas dos excepciones PUBLICADA y VALIDADO: deben comportarse como booleen/byte
                                If (fColumns(i) = "PUBLICADA" OrElse fColumns(i) = "VALIDADO") Then
                                    bFilterBool = True
                                    sFilterCondition = "( 1=1 )"
                                    'Todo el filtrado lo hace infragistics
                                End If
                            Case Else
                        End Select
                        If Not bFilterBool Then
                            sFilterCondition = "(" & fColumns(i) & sFilterCondition & ")"
                        End If
                        sFiltro = IIf(sFiltro = String.Empty, sFilterCondition, sFiltro & " AND " & sFilterCondition)
                    Next
                    With dsAuxiliar.DefaultViewManager
                        With .DataViewSettings("CERTIFICADOS")
                            .RowFilter = sFiltro
                        End With
                    End With
                End If
                _dsCertificados = New DataSet
                _dsCertificados.Tables.Add(dsAuxiliar.DefaultViewManager.DataViewSettings("CERTIFICADOS").Table.DefaultView.ToTable.Copy)
            Else

                Dim cFiltro As FiltroQA
                cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroQA))
                cFiltro.TipoSol = TiposDeDatos.TipoDeSolicitud.Certificado

                'Cargar La ordenacion del Filtro (por BBDD) 
                'o la de por defecto (por cookie o por "fecha de solicitud de más a menos recientes" sino hay cooki)
                Dim sOrdenacion As String = "FEC_SOLICITUD DESC"
                If hOrdered.Value = "" Then
                    If Session("QAFiltroUsuarioIdVCert") = "" Then
                        Dim cookie As HttpCookie
                        cookie = Request.Cookies("ORDENACION_QA_CERT")
                        If Not cookie Is Nothing Then
                            sOrdenacion = cookie.Value
                            bOrdenacionEnBDVacio = (sOrdenacion = "")
                        End If
                    Else
                        cFiltro.IDFiltroUsuario = Session("QAFiltroUsuarioIdVCert")
                        cFiltro.cargarOrdenacionFiltro()
                        sOrdenacion = cFiltro.Ordenacion
                        bOrdenacionEnBDVacio = (sOrdenacion = "")
                        If sOrdenacion = "" Then sOrdenacion = "FEC_SOLICITUD DESC"
                    End If
                    If InStr(sOrdenacion, "DATAKEY") = 0 Then sOrdenacion = IIf(sOrdenacion = "", "DATAKEY ASC", sOrdenacion & ",DATAKEY ASC")
                    hOrdered.Value = sOrdenacion
                Else
                    sOrdenacion = hOrdered.Value
                    bOrdenacionEnBDVacio = False
                End If

                If bOrdenacionEnBDVacio Then
                    'Nada mas entrar si la ordenación es nula se pone como ordenación: FEC_SOLICITUD DESC, DATAKEY ASC
                    '   La columna DATAKEY va siempre, oculta pero va
                    '   La columna FEC_SOLICITUD, puede ir o no. Si va ningun problema eoc no es SortedColumn
                    Dim dsCamposYCamposGenerales As DataSet = Nothing
                    dsCamposYCamposGenerales = cFiltro.LoadCamposyCamposGenerales(Idioma)
                    For Each oRow As DataRow In dsCamposYCamposGenerales.Tables(0).Rows
                        If CType(oRow("ID"), Integer) = 9 Then
                            bOrdenacionEnBDVacio = False
                            Exit For
                        End If
                    Next
                    'Tengo en bOrdenacionEnBDVacio si FEC_SOLICITUD va o no. bOrdenacionEnBDVacio = true-> NO VA
                End If

                _pageNumber = IIf(HttpContext.Current.Session("QApageNumberVCert") = "", 0, HttpContext.Current.Session("QApageNumberVCert"))

                Dim _pageSize As Integer = System.Configuration.ConfigurationManager.AppSettings("PageSize")
                cFiltro.Visor_DevolverCertificados(FSNUser.Cod, FSNUser.CodPersona, FSNUser.Idioma,
                    FSNUser.UON1, FSNUser.UON2, FSNUser.UON3, FSNUser.Dep,
                    FSNUser.QARestCertifUsu, FSNUser.QARestCertifUO,
                    FSNUser.QARestCertifDep, FSNUser.QARestProvMaterial, FSNUser.QARestProvEquipo, FSNUser.QARestProvContacto,
                    FSNUser.QARestCertifUNQAUsu, FSNUser.QARestCertifUNQA,
                    Busqueda.Materiales, Busqueda.TipoSolicitudes, Busqueda.VerDadosBaja,
                    Busqueda.MostrarProveedores, Busqueda.ProveedorHidden, Busqueda.ProveedorDeBaja, Busqueda.Estados, Busqueda.Peticionario,
                    Busqueda.FechasDe, Busqueda.FechaDesde, Busqueda.FechaHasta, Session("QANombreTablaVCert"), sOrdenacion,
                    _pageNumber + 1, _pageSize, (Not IsPostBack OrElse Not CType(hCacheLoaded.Value, Boolean)), CType(hFiltered.Value, Boolean))

                Dim PKCol(0) As DataColumn
                PKCol(0) = cFiltro.CertificadosVisor.Tables(0).Columns("DATAKEY")
                cFiltro.CertificadosVisor.Tables(0).PrimaryKey = PKCol

                Dim parentCols(1) As DataColumn
                Dim childCols(1) As DataColumn

                cFiltro.CertificadosVisor.Tables(0).TableName = "CERTIFICADOS"
                cFiltro.CertificadosVisor.Tables(1).TableName = "PETICIONARIO"
                If (IsPostBack AndAlso CType(hCacheLoaded.Value, Boolean)) Then
                    cFiltro.CertificadosVisor.Tables.Add(CType(Cache("dtProve_MaterialesQA_" & FSNUser.Cod), DataTable).Copy)
                End If
                cFiltro.CertificadosVisor.Tables(2).TableName = "MATERIAL"

                parentCols(0) = cFiltro.CertificadosVisor.Tables("CERTIFICADOS").Columns("PROVE_COD")
                parentCols(1) = cFiltro.CertificadosVisor.Tables("CERTIFICADOS").Columns("TIPO_CERTIFICADO")

                childCols(0) = cFiltro.CertificadosVisor.Tables("MATERIAL").Columns("PROVE_COD")
                childCols(1) = cFiltro.CertificadosVisor.Tables("MATERIAL").Columns("TIPO_CERTIFICADO")

                cFiltro.CertificadosVisor.Relations.Add("REL_CERTIFICADO_MATERIAL", parentCols, childCols, False)

                Dim sDenMaterial As String = ""
                Dim sIdMaterial As String = ""

                For Each oRow0 As DataRow In cFiltro.CertificadosVisor.Tables("CERTIFICADOS").Rows
                    sDenMaterial = ""
                    sIdMaterial = ""
                    For Each oRow1 As DataRow In oRow0.GetChildRows("REL_CERTIFICADO_MATERIAL")
                        sDenMaterial = sDenMaterial & IIf(sDenMaterial = "", "", ",") & oRow1("DEN_MATERIAL")
                        sIdMaterial = sIdMaterial & IIf(sIdMaterial = "", "", ",") & oRow1("ID_MATERIAL")
                    Next

                    oRow0("DEN_MATERIAL") = sDenMaterial
                    oRow0("ID_MATERIAL") = sIdMaterial
                Next

                Dim parentColsNo(0) As DataColumn
                Dim childColsNo(0) As DataColumn

                parentColsNo(0) = cFiltro.CertificadosVisor.Tables("CERTIFICADOS").Columns("TIPO_CERTIFICADO")

                childColsNo(0) = cFiltro.CertificadosVisor.Tables("PETICIONARIO").Columns("TIPO_CERTIFICADO")

                cFiltro.CertificadosVisor.Relations.Add("REL_CERTIFICADO_NOPETICIONARIO", parentColsNo, childColsNo, False)

                For Each oRow0 As DataRow In cFiltro.CertificadosVisor.Tables("CERTIFICADOS").Rows
                    For Each oRow1 As DataRow In oRow0.GetChildRows("REL_CERTIFICADO_NOPETICIONARIO")
                        oRow0("NOPETIC") = oRow1("NOPETIC")
                    Next
                Next

                cFiltro.CertificadosVisor.Relations.Clear()

                ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados

                _dsCertificados = New DataSet
                _dsCertificados.Tables.Add(cFiltro.CertificadosVisor.Tables("CERTIFICADOS").Copy)
                ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados
                _dsCertificados.Tables(0).TableName = "CERTIFICADOS"

                cFiltro = Nothing
            End If

            Dim sortedColumns() As String = Split(hOrdered.Value, ",")
            Dim groupedColumns() As String = Split(Session("ColsGroupBy" & Session("QAFiltroUsuarioIdVCert")), "#")
            Dim sItemSinOrder As String
            Dim sGrupoSinOrder As String
            Dim bNoOrdenar As Boolean
            For Each item As String In sortedColumns
                item = item.Replace("C_", "BOOL_C_")
                item = item.Replace("FEBOOL_C_", "FEC_")

                item = item.Replace("PUBLICADA", "BOOL_PUBLICADA")
                item = item.Replace("VALIDADO", "BOOL_VALIDADO")
                item = item.Replace("REAL", "BOOL_REAL")
                item = item.Replace("BAJA", "BOOL_BAJA")

                If Not item = "" AndAlso Not whgCertificados.GridView.Columns.Item(Split(item, " ")(0)) Is Nothing Then
                    sItemSinOrder = Trim(Replace(Replace(UCase(item), " ASC", ""), " DESC", ""))
                    bNoOrdenar = False
                    'Comprueba q las columnas por las q ordenar no esten en las columnas a agrupar o el SortedColumns.Add fallara
                    For Each sGrupoConOrder As String In groupedColumns
                        sGrupoSinOrder = Trim(Replace(Replace(UCase(sGrupoConOrder), " ASC", ""), " DESC", ""))
                        If sGrupoSinOrder = sItemSinOrder Then
                            bNoOrdenar = True
                            Exit For
                        End If
                    Next

                    If Not bNoOrdenar Then
                        If Not bOrdenacionEnBDVacio Then 'Si hay datos creo q siempre esta bien
                            whgCertificados.GridView.Behaviors.Sorting.SortedColumns.Add(Split(item, " ")(0), _
                                IIf(Split(item, " ")(1) = "DESC", Infragistics.Web.UI.SortDirection.Descending, Infragistics.Web.UI.SortDirection.Ascending))
                        ElseIf Split(item, " ")(0) = "DATAKEY" Then
                            whgCertificados.GridView.Behaviors.Sorting.SortedColumns.Add(Split(item, " ")(0), _
                                IIf(Split(item, " ")(1) = "DESC", Infragistics.Web.UI.SortDirection.Descending, Infragistics.Web.UI.SortDirection.Ascending))
                        End If
                    End If
                End If
            Next

            For Each item As String In groupedColumns
                If Not item = "" Then whgCertificados.GroupingSettings.GroupedColumns.Add(Split(item, " ")(0), _
                    IIf(Split(item, " ")(1) = "DESC", Infragistics.Web.UI.GridControls.GroupingSortDirection.Descending, Infragistics.Web.UI.GridControls.GroupingSortDirection.Ascending))
            Next
            'para q el hOrdered quede fijado
            upPager.Update()
            Return _dsCertificados
        End Get
    End Property
    ''' <summary>
    ''' Al iniciar la pagina establece EnableScriptGlobalization
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub Cert_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ScriptMgr.EnableScriptGlobalization = True
    End Sub
#Region "Inicio"
    ''' <summary>
    ''' Cargar la pagina
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0,2</remarks>
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados
        CargarIdiomas()

        FSAL_Personalizado_Para_Visores()

        If Not IsPostBack Then
            Cache.Remove("filtroNumCert_" & FSNUser.Cod)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaQA", "<script>var rutaTheme = '" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "'</script>")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaQA", "<script>var rutaQA = '" & ConfigurationManager.AppSettings("rutaQA") & "'</script>")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaQA2008", "<script>var rutaQA2008 = '" & ConfigurationManager.AppSettings("rutaQA2008") & "'</script>")

            Session("arrDatosGeneralesVCer") = Nothing

            Seccion = "Certificados"
            Dim mMaster = CType(Me.Master, FSNWeb.Menu)
            mMaster.Seleccionar("Calidad", "Certificados")
            Dim mMasterTotal As FSNWeb.Cabecera = CType(mMaster.Master, FSNWeb.Cabecera)
            Dim scriptReference As ScriptReference
            scriptReference = New ScriptReference
            scriptReference.Path = ConfigurationManager.AppSettings("ruta") & "js/jsUpdateProgress.js"
            CType(mMasterTotal.FindControl("ScriptManager1"), ScriptManager).Scripts.Add(scriptReference)

            whgCertificados.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")
            whgCertificados.GridView.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")
        Else
            Dim RecargarGrid As Boolean = True
            For index As Integer = 0 To dlFiltrosConfigurados.Items.Count - 1
                If Request("__EVENTTARGET") = CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).UniqueID Then
                    RecargarGrid = False
                    Exit For
                End If
            Next
            Select Case Request("__EVENTTARGET")
                Case btnPager.ClientID
                    _pageNumber = CType(Request("__EVENTARGUMENT"), Integer)
                    Session("QApageNumberVCert") = CType(Request("__EVENTARGUMENT"), String)
                Case btnPagerBD.ClientID
                    _pageNumber = CType(Request("__EVENTARGUMENT"), Integer)
                    Session("QApageNumberVCert") = CType(Request("__EVENTARGUMENT"), String)
                Case btnBuscarBusquedaAvanzada.UniqueID
                    RecargarGrid = False
                Case btnOrder.ClientID
                    Exit Sub
                Case btnRecargaCache.ClientID
                    RecargarCache()
                    Exit Sub
            End Select

            If RecargarGrid Then CargarCertificados()
            Select Case Request("__EVENTTARGET")
                Case "Excel"
                    CargarGridExportacion()
                    ExportarExcel()
                Case "PDF"
                    CargarGridExportacion()
                    ExportarPDF()
                Case Else
                    updGridCertificados.Update()
            End Select
        End If

        Dim FiltroUsu As String = IIf(Session("QAFiltroUsuarioIdVCert") = "", "0", Session("QAFiltroUsuarioIdVCert"))
        btnConfigurar.Attributes.Add("onClick", "window.location='../FiltrosQA/ConfigurarFiltrosQA.aspx?IDFiltroUsu=" & FiltroUsu & "&NombreTabla=" & Session("QANombreTablaVCert") & "&Donde=VisorCert';return false;")

        With dpFechaLimCumplim
            .EditModeFormat = FSNUser.DateFormat.ShortDatePattern
            .DisplayModeFormat = FSNUser.DateFormat.ShortDatePattern
        End With

        FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/Certificado.png"
        If FSNUser.QAPermitirEnviarCertificado Then
            CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgEnviar"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/enviarcertificado.gif"
        Else
            CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("pnlEnviar"), System.Web.UI.WebControls.Panel).Visible = False
        End If
        If FSNUser.QAPermitirRenovarCertificado Then
            CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgRenovar"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/renovar.gif"
        Else
            CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("pnlRenovar"), System.Web.UI.WebControls.Panel).Visible = False
        End If
        If FSNUser.QAPermitirSolicitarCertificado Then
            CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgSolicitar"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/solicitarCertificado.gif"
        Else
            CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("pnlSolicitar"), System.Web.UI.WebControls.Panel).Visible = False
        End If

        CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgExcel"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Excel.png"
        CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgPDF"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/PDF.png"
        CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgEmail"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/enviar_email_color.gif"

        imgCalendarSolicitarRenovar.Src = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/calendar.png"
        imgAyudaLimiteCumplimentacion.Src = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/help.gif"
        imgAyudaDespublicacion.Src = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/help.gif"

        Dim ModalProgressClientID As String = CType(Master.Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalClientID", "<script>var ModalProgress = '" & ModalProgressClientID & "' </script>")

        Dim ProveAnimationClientID As String = FSNPanelDatosProveedorQA.AnimationClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ProveAnimationClientID", "<script>var ProveAnimationClientID = '" & ProveAnimationClientID & "' </script>")

        Dim ProveDynamicPopulateClienteID As String = FSNPanelDatosProveedorQA.DynamicPopulateClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ProveDynamicPopulateClienteID", "<script>var ProveDynamicPopulateClienteID = '" & ProveDynamicPopulateClienteID & "' </script>")

        Dim PeticionarioAnimationClientID As String = FSNPanelDatosPersonaQA.AnimationClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PeticionarioAnimationClientID", "<script>var PeticionarioAnimationClientID = '" & PeticionarioAnimationClientID & "' </script>")

        Dim PeticionarioDynamicPopulateClienteID As String = FSNPanelDatosPersonaQA.DynamicPopulateClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PeticionarioDynamicPopulateClienteID", "<script>var PeticionarioDynamicPopulateClienteID = '" & PeticionarioDynamicPopulateClienteID & "' </script>")

        Dim ProcesoAnimationClientID As String = FSNPanelEnProceso.AnimationClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ProcesoAnimationClientID", "<script>var ProcesoAnimationClientID = '" & ProcesoAnimationClientID & "' </script>")

        Dim ProcesoDynamicPopulateClienteID As String = FSNPanelEnProceso.DynamicPopulateClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ProcesoDynamicPopulateClienteID", "<script>var ProcesoDynamicPopulateClienteID = '" & ProcesoDynamicPopulateClienteID & "' </script>")
    End Sub
    ''' <summary>Inicializa los arrays con los campos generales de un certificado.</summary>
    ''' <remarks>Llamada desde:Page_Load. Tiempo máximo: 0sg.</remarks>
    Private Function InicializarArraysCamposGeneralesCert() As String()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltroQA

        Dim iLongitud As Integer = IIf(Me.FSNServer.TipoAcceso.gbPotAValProvisionalSelProve, 22, 21)

        Dim arrDatosGeneralesDen(iLongitud) As String
        arrDatosGeneralesDen(1) = Textos(18)
        arrDatosGeneralesDen(2) = Textos(21)
        arrDatosGeneralesDen(3) = Textos(22)
        For i = 4 To 14
            arrDatosGeneralesDen(i) = Textos(i + 36)
        Next
        arrDatosGeneralesDen(15) = Textos(30)
        arrDatosGeneralesDen(16) = Textos(24)
        arrDatosGeneralesDen(17) = Textos(51)
        arrDatosGeneralesDen(18) = Textos(63)
        arrDatosGeneralesDen(19) = Textos(64)
        arrDatosGeneralesDen(20) = Textos(65)
        arrDatosGeneralesDen(21) = Textos(66)
        If FSNServer.TipoAcceso.gbPotAValProvisionalSelProve Then arrDatosGeneralesDen(22) = Textos(68)

        Return arrDatosGeneralesDen
    End Function
    Private Sub CargarIdiomas()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados

        FSNPageHeader.TituloCabecera = Textos(0) 'certificados

        lblBusquedaAvanzada.Text = Textos(1) 'Seleccione los criterios de búsqueda generales
        btnBuscarBusquedaAvanzada.Text = Textos(2) 'buscar
        btnLimpiarBusquedaAvanzada.Text = Textos(3) 'Limpiar
        btnAnyadirFiltros.Text = Textos(4) 'Añadir filtros
        btnConfigurar.Text = Textos(6) ' Configurar

        whgCertificados.GroupingSettings.EmptyGroupAreaText = Textos(7) 'Desplace una columna aquí para agrupar por ese concepto

        CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lnkSolicitar"), Label).Text = Textos(50)
        CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lnkEnviar"), Label).Text = Textos(51)
        CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lnkRenovar"), Label).Text = Textos(52)

        CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblExcel"), Label).Text = Textos(68)
        CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPDF"), Label).Text = Textos(69)
        CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblEmail"), Label).Text = Textos(71)

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.CertifCalendario

        lblAyudaLimiteCumplimentacion.Text = Textos(17)
        lblAyudaDespublicacion.Text = Textos(18)

        ImgBtnCerrarSolicitarRenovar.ToolTip = Textos(2)
        lblSubTitulo.Text = Textos(11)
        lblFechaLimiteCumplimentacion.Text = Textos(13)
        lblFechaDespublicacion.Text = Textos(15)
        cmdContinuar.Text = Textos(1)
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPopUpCertificados") Then
            Dim sVariableJavascriptTextosPopUpCertificados As String = "var TextosPopUpCertificados = new Array();"
            sVariableJavascriptTextosPopUpCertificados &= "TextosPopUpCertificados[0]='" & JSText(Textos(19)) & "';"
            sVariableJavascriptTextosPopUpCertificados &= "TextosPopUpCertificados[1]='" & JSText(Textos(20)) & "';"
            sVariableJavascriptTextosPopUpCertificados &= "TextosPopUpCertificados[2]='" & JSText(Textos(12)) & "';"
            sVariableJavascriptTextosPopUpCertificados &= "TextosPopUpCertificados[3]='" & JSText(Textos(14)) & "';"
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados
            sVariableJavascriptTextosPopUpCertificados &= "TextosPopUpCertificados[4]='" & JSText(Textos(16)) & "';"
            sVariableJavascriptTextosPopUpCertificados &= "TextosPopUpCertificados[5]='" & JSText(Textos(15)) & "';"
            sVariableJavascriptTextosPopUpCertificados &= "TextosPopUpCertificados[6]='" & JSText(Textos(49)) & "';"

            sVariableJavascriptTextosPopUpCertificados &= "TextosPopUpCertificados[7]='" & JSText(Textos(74)) & "';" 'No puede seleccionar un tipo de certificado del que no es peticionario.
            sVariableJavascriptTextosPopUpCertificados &= "TextosPopUpCertificados[8]='" & JSText(Textos(75)) & "';" 'No puede seleccionar tipos de certificados de los que no es peticionario. ¿Desea  continuar con el resto de certificados seleccionados?
            sVariableJavascriptTextosPopUpCertificados &= "TextosPopUpCertificados[9]='" & JSText(Textos(76)) & "';" 'No puede seleccionar tipos de certificados de los que no es peticionario
            sVariableJavascriptTextosPopUpCertificados &= "TextosPopUpCertificados[10]='" & JSText(Textos(77)) & "';"
            sVariableJavascriptTextosPopUpCertificados &= "TextosPopUpCertificados[11]='" & JSText(Textos(78)) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPopUpCertificados", sVariableJavascriptTextosPopUpCertificados, True)
        End If

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Notificaciones
        CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(46)
        CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(48)
    End Sub
    Private Sub RecargarCache()
        Dim cFiltro As FiltroQA
        cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroQA))
        cFiltro.TipoSol = TiposDeDatos.TipoDeSolicitud.Certificado
        Dim _pageSize As Integer = System.Configuration.ConfigurationManager.AppSettings("PageSize")
        cFiltro.Visor_DevolverCertificados(FSNUser.Cod, FSNUser.CodPersona, FSNUser.Idioma,
            FSNUser.UON1, FSNUser.UON2, FSNUser.UON3, FSNUser.Dep,
            FSNUser.QARestCertifUsu, FSNUser.QARestCertifUO,
            FSNUser.QARestCertifDep, FSNUser.QARestProvMaterial, FSNUser.QARestProvEquipo, FSNUser.QARestProvContacto,
            FSNUser.QARestCertifUNQAUsu, FSNUser.QARestCertifUNQA,
            Busqueda.Materiales, Busqueda.TipoSolicitudes, Busqueda.VerDadosBaja,
            Busqueda.MostrarProveedores, Busqueda.ProveedorHidden, Busqueda.ProveedorDeBaja, Busqueda.Estados, Busqueda.Peticionario,
            Busqueda.FechasDe, Busqueda.FechaDesde, Busqueda.FechaHasta, Session("QANombreTablaVCert"), hOrdered.Value,
            0, 0, True, True)

        Dim PKCol(0) As DataColumn
        PKCol(0) = cFiltro.CertificadosVisor.Tables(0).Columns("DATAKEY")
        cFiltro.CertificadosVisor.Tables(0).PrimaryKey = PKCol

        Dim parentCols(1) As DataColumn
        Dim childCols(1) As DataColumn

        cFiltro.CertificadosVisor.Tables(0).TableName = "CERTIFICADOS"
        cFiltro.CertificadosVisor.Tables(1).TableName = "PETICIONARIO"
        cFiltro.CertificadosVisor.Tables(2).TableName = "MATERIAL"

        parentCols(0) = cFiltro.CertificadosVisor.Tables("CERTIFICADOS").Columns("PROVE_COD")
        parentCols(1) = cFiltro.CertificadosVisor.Tables("CERTIFICADOS").Columns("TIPO_CERTIFICADO")

        childCols(0) = cFiltro.CertificadosVisor.Tables("MATERIAL").Columns("PROVE_COD")
        childCols(1) = cFiltro.CertificadosVisor.Tables("MATERIAL").Columns("TIPO_CERTIFICADO")

        cFiltro.CertificadosVisor.Relations.Add("REL_CERTIFICADO_MATERIAL", parentCols, childCols, False)

        Dim sDenMaterial As String = ""
        Dim sIdMaterial As String = ""

        For Each oRow0 As DataRow In cFiltro.CertificadosVisor.Tables("CERTIFICADOS").Rows
            sDenMaterial = ""
            sIdMaterial = ""

            For Each oRow1 As DataRow In oRow0.GetChildRows("REL_CERTIFICADO_MATERIAL")
                sDenMaterial = sDenMaterial & IIf(sDenMaterial = "", "", ",") & oRow1("DEN_MATERIAL")
                sIdMaterial = sIdMaterial & IIf(sIdMaterial = "", "", ",") & oRow1("ID_MATERIAL")
            Next

            oRow0("DEN_MATERIAL") = sDenMaterial
            oRow0("ID_MATERIAL") = sIdMaterial
        Next

        Dim parentColsNo(0) As DataColumn
        Dim childColsNo(0) As DataColumn
        parentColsNo(0) = cFiltro.CertificadosVisor.Tables("CERTIFICADOS").Columns("TIPO_CERTIFICADO")
        childColsNo(0) = cFiltro.CertificadosVisor.Tables("PETICIONARIO").Columns("TIPO_CERTIFICADO")
        cFiltro.CertificadosVisor.Relations.Add("REL_CERTIFICADO_NOPETICIONARIO", parentColsNo, childColsNo, False)

        For Each oRow0 As DataRow In cFiltro.CertificadosVisor.Tables("CERTIFICADOS").Rows
            For Each oRow1 As DataRow In oRow0.GetChildRows("REL_CERTIFICADO_NOPETICIONARIO")
                oRow0("NOPETIC") = oRow1("NOPETIC")
            Next
        Next
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados

        cFiltro.CertificadosVisor.Relations.Clear()
        InsertarEnCache("dsCertificados_" & FSNUser.Cod, cFiltro.CertificadosVisor, CacheItemPriority.BelowNormal)
        InsertarEnCache("dtProve_MaterialesQA_" & FSNUser.Cod, cFiltro.CertificadosVisor.Tables(2))
        hCacheLoaded.Value = 1
        updFiltros.Update()
    End Sub
    ''' <summary>
    ''' Antes de dibujar la pagina establece filtros busqueda y datos paginados
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub Certificados_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            LimpiarBusquedaAvanzada()
            cargarSituacionActual()

            CargarCertificados()
        End If
    End Sub
    ''' <summary>
    ''' Metodo para registrar tiempos de FSAL del cargado del grid principal en la carga total de la pagina y en posteriores cargas parciales.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FSAL_Personalizado_Para_Visores()

        ''Codigo para guardar datos del FSAL recogidos desde FSNLibrary y así poder utilizarlos en el PostBack cuando se cargue el Grid.
        ''Si es primera llamada a la pagina y no se han enviado los tiempos para update, se pasa el idRegistro inicial para localizar el registro a modificar
        ''si no, al hacer una insercion de un nuevo registro se pasa el idRegistro de la carga actual.
        ''
        If HttpContext.Current.Items("FECHAPET") IsNot Nothing AndAlso IsDate(HttpContext.Current.Items("FECHAPET")) AndAlso
           HttpContext.Current.Items("IDREGISTRO") IsNot Nothing AndAlso HttpContext.Current.Items("IDREGISTRO") <> "" Then
            bActivadoFSAL.Value = "1"
            Dim fechaIniFSAL8 As DateTime = HttpContext.Current.Items("FECHAPET")
            sFechaIniFSAL8.Value = Format(fechaIniFSAL8, "yyyy-MM-dd HH:mm:ss") & "." & Format(fechaIniFSAL8.Millisecond, "000")
            If bEnviadoFSAL8Inicial.Value = "0" AndAlso Not IsPostBack Then
                sIdRegistroFSAL1.Value = HttpContext.Current.Items("IDREGISTRO")
                sPagina.Value = HttpContext.Current.Request.Url.AbsoluteUri.Split("?")(0) 'para quitar ?=prove=
                iP.Value = HttpContext.Current.Request.UserHostAddress
            ElseIf bEnviadoFSAL8Inicial.Value = "1" AndAlso HttpContext.Current.Request.Headers("x-ajax") Is Nothing Then
                sProducto.Value = System.Configuration.ConfigurationManager.AppSettings("FSAL_Origen") '"FULLSTEP WEB ó FULLSTEP PORTAL"
                sPagina.Value = HttpContext.Current.Request.Url.AbsoluteUri.Split("?")(0) 'para quitar ?=prove=
                iPost.Value = 3
                iP.Value = HttpContext.Current.Request.UserHostAddress
                sUsuCod.Value = DirectCast(Session("FSN_User"), Fullstep.FSNServer.User).Cod
                sPaginaOrigen.Value = sPagina.Value
                sNavegador.Value = HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT") ' Obtener el navegador
                Dim rndRandom As New Random
                sIdRegistroFSAL8.Value = fechaIniFSAL8.Ticks.ToString() & Format(rndRandom.Next(1, 99999999), "00000000")
                sProveCod.Value = ""
                If HttpContext.Current.Request.Url.AbsoluteUri.Split("?").Length > 1 Then
                    sQueryString.Value = HttpContext.Current.Request.Url.AbsoluteUri.Split("?")(1)
                Else
                    sQueryString.Value = ""
                End If
            End If
        End If
        ''
        ''Fin codigo FSAL

    End Sub
#End Region
#Region "Carga de datos"
    ''' <summary>
    ''' Evento que se lanza al acceder a datos y que usamos para decirle al origen de datos cual va a ser su objeto.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: En la carga de la página VisorSolicitudes.aspx; Tiempo máximo: 0 sg.</remarks>
    Private Sub odsFiltrosConfigurados_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles odsFiltrosConfigurados.ObjectCreating
        Dim FiltrosQA As FSNServer.FiltrosQA
        FiltrosQA = FSNServer.Get_Object(GetType(FSNServer.FiltrosQA))
        FiltrosQA.Tipo = 0
        e.ObjectInstance = FiltrosQA
    End Sub
    ''' <summary>
    ''' Evento que se lanza cuando el objeto de origen de datos recupera los datos. Lo usamos para pasarle los parámetros que necesita su método SELECT configurado.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: En la carga de la página VisorSolicitudes.aspx; Tiempo máximo: 0 sg.</remarks>
    Private Sub odsFiltrosConfigurados_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsFiltrosConfigurados.Selecting
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados
        e.InputParameters.Item("sPersona") = FSNUser.CodPersona
    End Sub
    Private Sub CargarCertificados()
        With whgCertificados
            .Rows.Clear()
            .GridView.ClearDataSource()
            .Columns.Clear()
            .GridView.Columns.Clear()
            CreacionEdicionColumnasGrid()
            .DataSource = Certificados
            .GridView.DataSource = .DataSource
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados
            .DataMember = "CERTIFICADOS"
            .DataKeyFields = "DATAKEY"
            .DataBind()
            updGridCertificados.Update()
        End With
    End Sub

    ''' <summary>
    ''' Cargar Grid Exportacion
    ''' </summary>
    ''' <remarks>Llamada desde: page_load; Tiempo mÃ¡ximo: 0 sg.</remarks>
    Private Sub CargarGridExportacion()

        Dim ds As DataSet
        RecargarCache()
        ds = Certificados

        With whgCertificadosExportacion
            .Rows.Clear()
            .GridView.ClearDataSource()
            .Columns.Clear()
            .GridView.Columns.Clear()
            .DataSource = ds
            .GridView.DataSource = .DataSource
            CreacionEdicionColumnasGridExportacion()
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados
            .DataMember = "CERTIFICADOS"
            .DataKeyFields = "DATAKEY"
            .DataBind()
            updGridCertificados.Update()
        End With
    End Sub
    Private Sub CreacionEdicionColumnasGrid()
        whgCertificados.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")
        whgCertificados.GridView.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")

        ObtenerColumnasGrid(whgCertificados.UniqueID)
        CrearColumnasGrid(whgCertificados.UniqueID)
    End Sub
    Private Sub CreacionEdicionColumnasGridExportacion()
        ObtenerColumnasGrid(whgCertificadosExportacion.UniqueID)
        CrearColumnasGrid(whgCertificadosExportacion.UniqueID)
    End Sub
    ''' <summary>
    ''' Inicializa las columnas del grid con los campos generales
    ''' </summary>
    ''' <remarks>Llamada desde=Page_Load uwgCertificados_InitializeLayout// ; Tiempo máximo:0seg.</remarks>
    Private Sub ObtenerColumnasGrid(ByVal gridId As String)
        Dim arrDatosGeneralesDen() As String = InicializarArraysCamposGeneralesCert()

        Dim grid As WebHierarchicalDataGrid = CType(Me.FindControl(gridId), WebHierarchicalDataGrid)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados
        If Session("QAFiltroUsuarioIdVCert") = "" OrElse Session("QAFiltroUsuarioIdVCert") = "0" Then
            'NOMBRE DE LAS COLUMNAS DE LA GRID!!!
            Dim camposGrid As New List(Of Infragistics.Web.UI.GridControls.BoundDataField)
            Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "DATAKEY"
                .DataFieldName = "DATAKEY"
                .Header.Text = "DATAKEY"
                .Width = Unit.Pixel(0)
                .Hidden = True
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "NUM_VERSION"
                .DataFieldName = "NUM_VERSION"
                .Header.Text = "NUM_VERSION"
                .Width = Unit.Pixel(0)
                .Hidden = True
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "ESTADO"
                .DataFieldName = "ESTADO"
                .Header.Text = "ESTADO"
                .Width = Unit.Pixel(0)
                .Hidden = True
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "PROVE_DEN"
                .DataFieldName = "PROVE_DEN"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = Textos(18)
                .Header.Tooltip = Textos(18)
                .Width = Unit.Pixel(200)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "DEN_TIPO_CERTIFICADO"
                .DataFieldName = "DEN_TIPO_CERTIFICADO"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = Textos(19)
                .Header.Tooltip = Textos(19)
                .Width = Unit.Pixel(200)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "ESTADO_DEN"
                .DataFieldName = "ESTADO_DEN"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = Textos(20)
                .Header.Tooltip = Textos(20)
                .Width = Unit.Pixel(200)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "OBLIGATORIO"
                .DataFieldName = "OBLIGATORIO"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = Textos(70)
                .Header.Tooltip = Textos(70)
                .Width = Unit.Pixel(40)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "DEN_MATERIAL"
                .DataFieldName = "DEN_MATERIAL"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = Textos(21)
                .Header.Tooltip = Textos(21)
                .Width = Unit.Pixel(130)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "NOMBREPETICIONARIO"
                .DataFieldName = "NOMBREPETICIONARIO"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = Textos(22)
                .Header.Tooltip = Textos(22)
                .Width = Unit.Pixel(130)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "FEC_SOLICITUD"
                .DataFieldName = "FEC_SOLICITUD"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = Textos(23)
                .Header.Tooltip = Textos(23)
                .Width = Unit.Pixel(130)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "FEC_LIM_CUMPLIM"
                .DataFieldName = "FEC_LIM_CUMPLIM"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = Textos(24)
                .Header.Tooltip = Textos(24)
                .Width = Unit.Pixel(145)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "FEC_RESPUESTA"
                .DataFieldName = "FEC_RESPUESTA"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = Textos(25)
                .Header.Tooltip = Textos(25)
                .Width = Unit.Pixel(130)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "FEC_EXPIRACION"
                .DataFieldName = "FEC_EXPIRACION"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = Textos(26)
                .Header.Tooltip = Textos(26)
                .Width = Unit.Pixel(130)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "PUBLICADA"
                .DataFieldName = "PUBLICADA"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = Textos(27)
                .Header.Tooltip = Textos(27)
                .Width = Unit.Pixel(130)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "BOOL_PUBLICADA"
                .DataFieldName = "BOOL_PUBLICADA"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = Textos(27)
                .Header.Tooltip = Textos(27)
                .Width = Unit.Pixel(130)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "VALIDADO"
                .DataFieldName = "VALIDADO"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = Textos(80)
                .Header.Tooltip = Textos(80)
                .Width = Unit.Pixel(130)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "BOOL_VALIDADO"
                .DataFieldName = "BOOL_VALIDADO"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = Textos(80)
                .Header.Tooltip = Textos(80)
                .Width = Unit.Pixel(130)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "FEC_DESPUB"
                .DataFieldName = "FEC_DESPUB"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = Textos(28)
                .Header.Tooltip = Textos(28)
                .Width = Unit.Pixel(130)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "FEC_CUMPLIM"
                .DataFieldName = "FEC_CUMPLIM"
                .Header.CssClass = "headerNoWrap"
                .Header.Text = Textos(73)
                .Header.Tooltip = Textos(73)
                .Width = Unit.Pixel(130)
                .Hidden = False
                .CssClass = "itemSeleccionable SinSalto"
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "NOPETIC"
                .DataFieldName = "NOPETIC"
                .Header.Text = "NOPETIC"
                .Width = Unit.Pixel(0)
                .Hidden = True
            End With
            camposGrid.Add(campoGrid)

            Session("arrDatosGeneralesVCer") = camposGrid
        Else
            For i As Integer = 0 To grid.Columns.Count - 1
                grid.Columns(i).Hidden = True
            Next
            Dim cFiltro As FiltroQA
            cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroQA))
            cFiltro.IDFiltro = strToDbl(Session("QAFiltroIdVCert"))
            cFiltro.IDFiltroUsuario = Session("QAFiltroUsuarioIdVCert")
            cFiltro.TipoSol = TiposDeDatos.TipoDeSolicitud.Certificado
            cFiltro.Persona = FSNUser.CodPersona

            Dim dsCamposYCamposGenerales As DataSet = Nothing
            Dim iPosImgEstado As Integer = -1
            Dim iPosImgProve As Integer = -1

            dsCamposYCamposGenerales = cFiltro.LoadCamposyCamposGenerales(Idioma)
            Dim camposGrid As New List(Of Infragistics.Web.UI.GridControls.BoundDataField)
            Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
            Dim campoGridBool As Infragistics.Web.UI.GridControls.BoundDataField

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "DATAKEY"
                .DataFieldName = "DATAKEY"
                .Header.Text = "DATAKEY"
                .Width = Unit.Pixel(0)
                .Hidden = True
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "NUM_VERSION"
                .DataFieldName = "NUM_VERSION"
                .Header.Text = "NUM_VERSION"
                .Width = Unit.Pixel(0)
                .Hidden = True
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "ESTADO"
                .DataFieldName = "ESTADO"
                .Header.Text = "ESTADO"
                .Width = Unit.Pixel(0)
                .Hidden = True
            End With
            camposGrid.Add(campoGrid)

            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            With campoGrid
                .Key = "NOPETIC"
                .DataFieldName = "NOPETIC"
                .Header.Text = "NOPETIC"
                .Width = Unit.Pixel(0)
                .Hidden = True
            End With
            camposGrid.Add(campoGrid)

            For Each oRow As DataRow In dsCamposYCamposGenerales.Tables(0).Rows
                campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
                With campoGrid
                    .Header.CssClass = "headerNoWrap"
                    If (CType(oRow("TAMANYO_GRID"), Double) = 0.0) Then
                        .Width = Unit.Pixel(130)
                    Else
                        .Width = Unit.Pixel(CType(oRow("TAMANYO_GRID"), Double))
                    End If
                    .Hidden = Not CType(oRow("VISIBLE_GRID"), Boolean)
                    .CssClass = "SinSalto"
                    If CType(oRow("CAMPO_GENERAL"), Boolean) Then
                        Select Case CType(oRow("ID"), Integer)
                            Case 1
                                EstablecerPropCampoGen(campoGrid, "INSTANCIA", "INSTANCIA", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(1), arrDatosGeneralesDen(1))
                            Case 2
                                EstablecerPropCampoGen(campoGrid, "PROVE_COD", "PROVE_COD", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(2), arrDatosGeneralesDen(2))
                            Case 3
                                EstablecerPropCampoGen(campoGrid, "PROVE_DEN", "PROVE_DEN", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(3), arrDatosGeneralesDen(3))
                            Case 4
                                EstablecerPropCampoGen(campoGrid, "REAL", "REAL", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(4), arrDatosGeneralesDen(4))
                            Case 5
                                EstablecerPropCampoGen(campoGrid, "BAJA", "BAJA", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(5), arrDatosGeneralesDen(5))
                            Case 6
                                EstablecerPropCampoGen(campoGrid, "DEN_MATERIAL", "DEN_MATERIAL", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(6), arrDatosGeneralesDen(6))
                            Case 7
                                EstablecerPropCampoGen(campoGrid, "DEN_TIPO_CERTIFICADO", "DEN_TIPO_CERTIFICADO", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(7), arrDatosGeneralesDen(7))
                            Case 8
                                EstablecerPropCampoGen(campoGrid, "ESTADO_DEN", "ESTADO_DEN", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(8), arrDatosGeneralesDen(8))
                            Case 9
                                EstablecerPropCampoGen(campoGrid, "FEC_SOLICITUD", "FEC_SOLICITUD", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(9), arrDatosGeneralesDen(9))
                            Case 10
                                EstablecerPropCampoGen(campoGrid, "FEC_LIM_CUMPLIM", "FEC_LIM_CUMPLIM", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(10), arrDatosGeneralesDen(10))
                            Case 11
                                EstablecerPropCampoGen(campoGrid, "FEC_RESPUESTA", "FEC_RESPUESTA", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(11), arrDatosGeneralesDen(11))
                            Case 12
                                EstablecerPropCampoGen(campoGrid, "PUBLICADA", "PUBLICADA", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(12), arrDatosGeneralesDen(12))
                            Case 13
                                EstablecerPropCampoGen(campoGrid, "FEC_PUBLICACION", "FEC_PUBLICACION", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(13), arrDatosGeneralesDen(13))
                            Case 14
                                EstablecerPropCampoGen(campoGrid, "FEC_DESPUB", "FEC_DESPUB", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(14), arrDatosGeneralesDen(14))
                            Case 15
                                EstablecerPropCampoGen(campoGrid, "FEC_ACT", "FEC_ACT", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(15), arrDatosGeneralesDen(15))
                            Case 16
                                EstablecerPropCampoGen(campoGrid, "NOMBREPETICIONARIO", "NOMBREPETICIONARIO", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(16), arrDatosGeneralesDen(16))
                            Case 17
                                EstablecerPropCampoGen(campoGrid, "MODO_SOLIC", "MODO_SOLIC", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(17), arrDatosGeneralesDen(17))
                            Case 18
                                EstablecerPropCampoGen(campoGrid, "FEC_EXPIRACION", "FEC_EXPIRACION", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(18), arrDatosGeneralesDen(18))
                            Case 19
                                EstablecerPropCampoGen(campoGrid, "OBLIGATORIO", "OBLIGATORIO", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(19), arrDatosGeneralesDen(19), "System.Boolean")
                            Case 20
                                EstablecerPropCampoGen(campoGrid, "FEC_CUMPLIM", "FEC_CUMPLIM", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(20), arrDatosGeneralesDen(20))
                            Case 21
                                EstablecerPropCampoGen(campoGrid, "VALIDADO", "VALIDADO", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(21), arrDatosGeneralesDen(21))
                            Case 22
                                If Me.Acceso.gbPotAValProvisionalSelProve Then EstablecerPropCampoGen(campoGrid, "PROVISIONAL", "PROVISIONAL", oRow("NOMBRE_GRID"), arrDatosGeneralesDen(22), arrDatosGeneralesDen(22))
                            Case Else
                                .Key = "C_" & oRow("ID")
                                .DataFieldName = "C_" & oRow("ID")
                        End Select
                    Else
                        .Key = "C_" & oRow("ID")
                        .DataFieldName = "C_" & oRow("ID")
                    End If
                    If grid.Columns(campoGrid.Key) IsNot Nothing Then grid.Columns(campoGrid.Key).Hidden = Not CType(oRow("VISIBLE_GRID"), Boolean)
                End With
                If campoGrid.Key <> String.Empty Then camposGrid.Add(campoGrid)

                If CType(oRow("CAMPO_GENERAL"), Boolean) Then
                    If (CType(oRow("ID"), Integer) = 12) OrElse (CType(oRow("ID"), Integer) = 5) _
                    OrElse (CType(oRow("ID"), Integer) = 4) OrElse (CType(oRow("ID"), Integer) = 21) Then 'Boolean q no sea OBLIGATORIO
                        campoGridBool = New Infragistics.Web.UI.GridControls.BoundDataField(True)
                        With campoGridBool
                            .Header.CssClass = campoGrid.Header.CssClass
                            .Header.Text = campoGrid.Header.Text
                            .Header.Tooltip = campoGrid.Header.Tooltip
                            .Width = campoGrid.Width
                            .Hidden = campoGrid.Hidden
                            .CssClass = campoGrid.CssClass
                            .Key = "BOOL_" & campoGrid.Key
                            .DataFieldName = "BOOL_" & campoGrid.Key
                        End With

                        If grid.Columns(campoGridBool.Key) IsNot Nothing Then grid.Columns(campoGridBool.Key).Hidden = Not CType(oRow("VISIBLE_GRID"), Boolean)
                        camposGrid.Add(campoGridBool)
                    End If
                ElseIf oRow("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoBoolean Then
                    campoGridBool = New Infragistics.Web.UI.GridControls.BoundDataField(True)
                    With campoGridBool
                        .Header.CssClass = campoGrid.Header.CssClass
                        .Header.Text = campoGrid.Header.Text
                        .Header.Tooltip = campoGrid.Header.Tooltip
                        .Width = campoGrid.Width
                        .Hidden = campoGrid.Hidden
                        .CssClass = campoGrid.CssClass
                        .Key = "BOOL_" & campoGrid.Key
                        .DataFieldName = "BOOL_" & campoGrid.Key
                    End With

                    If grid.Columns(campoGridBool.Key) IsNot Nothing Then grid.Columns(campoGridBool.Key).Hidden = Not CType(oRow("VISIBLE_GRID"), Boolean)
                    camposGrid.Add(campoGridBool)
                End If
            Next
            Session("arrDatosGeneralesVCer") = camposGrid

            cFiltro = Nothing
        End If
    End Sub
    Private Sub EstablecerPropCampoGen(ByRef oCampoGrid As Infragistics.Web.UI.GridControls.BoundDataField, ByVal sKey As String, ByVal sDataFieldName As String, ByVal oNombreCampo As Object, ByVal sHeaderText As String, ByVal sHeaderTooltip As String, Optional sDataType As String = Nothing)
        oCampoGrid.Key = sKey
        oCampoGrid.DataFieldName = sDataFieldName
        oCampoGrid.Header.Text = If(oNombreCampo Is DBNull.Value, sHeaderText, oNombreCampo)
        oCampoGrid.Header.Tooltip = If(oNombreCampo Is DBNull.Value, sHeaderTooltip, oNombreCampo)
        If sDataType IsNot Nothing Then oCampoGrid.DataType = sDataType
    End Sub
    ''' <summary>
    ''' Crear Columnas Grid
    ''' </summary>
    ''' <param name="gridId">Identificador del grid</param>
    ''' <remarks>Llamada desde=CreacionEdicionColumnasGrid uwgCertificados_InitializeLayout// ; Tiempo mÃ¡ximo:0seg.</remarks>
    Private Sub CrearColumnasGrid(ByVal gridId As String)
        Dim grid As WebHierarchicalDataGrid = CType(Me.FindControl(gridId), WebHierarchicalDataGrid)
        Dim campoGridTemplate As Infragistics.Web.UI.GridControls.UnboundField
        Dim campoGridBoundTemplate As Infragistics.Web.UI.GridControls.BoundDataField
        campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
        With campoGridBoundTemplate
            .Key = "EN_PROCESO"
            .DataFieldName = "EN_PROCESO"
            .Header.Text = ""
            .Header.Tooltip = ""
            .Header.CssClass = "celdaImagenWebHierarchical headerNoWrap"
            .Width = Unit.Pixel(20)
            .Hidden = False
            .CssClass = "celdaImagenWebHierarchical SinSalto"
        End With
        If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
            grid.Columns.Add(campoGridBoundTemplate)
            AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
        End If
        If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
            grid.GridView.Columns.Add(campoGridBoundTemplate)
            AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
        End If
        For Each field As Infragistics.Web.UI.GridControls.BoundDataField In CType(Session("arrDatosGeneralesVCer"), List(Of Infragistics.Web.UI.GridControls.BoundDataField))
            Select Case field.Key
                Case "PROVE_DEN"
                    campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoGridBoundTemplate
                        .Key = "PROVE_COD"
                        .DataFieldName = "PROVE_COD"
                        .Header.Text = ""
                        .Header.Tooltip = ""
                        .Hidden = True
                    End With
                    If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridBoundTemplate)
                        AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
                    End If
                    If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridBoundTemplate)
                        AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
                    End If
                    campoGridTemplate = New Infragistics.Web.UI.GridControls.UnboundField(True)
                    With campoGridTemplate
                        .Key = "IMAGE_PROVE"
                        .Header.Text = ""
                        .Header.Tooltip = ""
                        .Header.CssClass = "celdaImagenWebHierarchical headerNoWrap"
                        .Width = Unit.Pixel(20)
                        .Hidden = False
                        .CssClass = "celdaImagenWebHierarchical itemSeleccionable SinSalto"
                    End With
                    If grid.Columns(campoGridTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridTemplate)
                        AnyadirTextoAColumnaNoFiltrada(gridId, campoGridTemplate.Key, False, False)
                    Else
                        grid.Columns(campoGridTemplate.Key).Hidden = field.Hidden
                    End If
                    If grid.GridView.Columns.Item(campoGridTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridTemplate)
                        AnyadirTextoAColumnaNoFiltrada(gridId, campoGridTemplate.Key, False, False)
                    Else
                        grid.GridView.Columns.Item(campoGridTemplate.Key).Hidden = field.Hidden
                    End If
                Case "ESTADO_DEN"
                    campoGridTemplate = New Infragistics.Web.UI.GridControls.UnboundField(True)
                    With campoGridTemplate
                        .Key = "IMAGE_ESTADO"
                        .Header.Text = ""
                        .Header.Tooltip = ""
                        .Header.CssClass = "celdaImagenWebHierarchical headerNoWrap"
                        .Width = Unit.Pixel(22)
                        .Hidden = False
                        .CssClass = "celdaImagenWebHierarchical SinSalto"
                    End With
                    If grid.Columns(campoGridTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridTemplate)
                        AnyadirTextoAColumnaNoFiltrada(gridId, campoGridTemplate.Key, False, False)
                    Else
                        grid.Columns(campoGridTemplate.Key).Hidden = field.Hidden
                    End If
                    If grid.GridView.Columns.Item(campoGridTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridTemplate)
                        AnyadirTextoAColumnaNoFiltrada(gridId, campoGridTemplate.Key, False, False)
                    Else
                        grid.GridView.Columns.Item(campoGridTemplate.Key).Hidden = field.Hidden
                    End If
                Case "NOMBREPETICIONARIO"
                    campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                    With campoGridBoundTemplate
                        .Key = "PER"
                        .DataFieldName = "PER"
                        .Header.Text = ""
                        .Header.Tooltip = ""
                        .Hidden = True
                    End With
                    If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.Columns.Add(campoGridBoundTemplate)
                        AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
                    End If
                    If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
                        grid.GridView.Columns.Add(campoGridBoundTemplate)
                        AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
                    End If

            End Select
            If grid.Columns(field.Key) Is Nothing Then
                grid.Columns.Add(field)
                AnyadirTextoAColumnaNoFiltrada(gridId, field.Key, True, True)
            End If
            If grid.GridView.Columns.Item(field.Key) Is Nothing Then
                grid.GridView.Columns.Add(field)
                AnyadirTextoAColumnaNoFiltrada(gridId, field.Key, True, True)
            End If

            If Left(field.Key.ToString, 5) = "BOOL_" Then
                grid.Columns(Replace(field.Key.ToString, "BOOL_", "")).Hidden = True
            End If
        Next
        campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
        With campoGridBoundTemplate
            .Key = "PLAZO_CUMPL"
            .DataFieldName = "PLAZO_CUMPL"
            .Header.Text = ""
            .Header.Tooltip = ""
            .Hidden = True
        End With
        If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
            grid.Columns.Add(campoGridBoundTemplate)
            AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
        End If
        If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
            grid.GridView.Columns.Add(campoGridBoundTemplate)
            AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
        End If
        campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
        With campoGridBoundTemplate
            .Key = "ID_CERTIF"
            .DataFieldName = "ID_CERTIF"
            .Header.Text = ""
            .Header.Tooltip = ""
            .Hidden = True
        End With
        If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
            grid.Columns.Add(campoGridBoundTemplate)
            AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
        End If
        If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
            grid.GridView.Columns.Add(campoGridBoundTemplate)
            AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
        End If
        campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
        With campoGridBoundTemplate
            .Key = "DEN_TIPO_CERTIFICADO"
            .DataFieldName = "DEN_TIPO_CERTIFICADO"
            .Header.Text = ""
            .Header.Tooltip = ""
            .Hidden = True
        End With
        If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
            grid.Columns.Add(campoGridBoundTemplate)
            AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
        End If
        If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
            grid.GridView.Columns.Add(campoGridBoundTemplate)
            AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
        End If
        campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
        With campoGridBoundTemplate
            .Key = "INSTANCIA"
            .DataFieldName = "INSTANCIA"
            .Header.Text = ""
            .Header.Tooltip = ""
            .Hidden = True
        End With
        If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
            grid.Columns.Add(campoGridBoundTemplate)
            AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
        End If
        If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
            grid.GridView.Columns.Add(campoGridBoundTemplate)
            AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
        End If
        campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
        With campoGridBoundTemplate
            .Key = "CON_EMAIL"
            .DataFieldName = "CON_EMAIL"
            .Header.Text = ""
            .Header.Tooltip = ""
            .Hidden = True
        End With
        If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
            grid.Columns.Add(campoGridBoundTemplate)
            AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
        End If
        If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
            grid.GridView.Columns.Add(campoGridBoundTemplate)
            AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
        End If
        campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
        With campoGridBoundTemplate
            .Key = "OBLIGATORIO"
            .DataFieldName = "OBLIGATORIO"
            .Header.Text = ""
            .Header.Tooltip = ""
            .Hidden = True
        End With
        If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
            grid.Columns.Add(campoGridBoundTemplate)
            AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
        End If
        If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
            grid.GridView.Columns.Add(campoGridBoundTemplate)
            AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
        End If

        campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField(True)
        With campoGridBoundTemplate
            .Key = "VALIDADO"
            .DataFieldName = "VALIDADO"
            .Header.CssClass = "headerNoWrap"
            .Header.Text = Textos(80)
            .Header.Tooltip = Textos(80)
            .Width = Unit.Pixel(130)
            .Hidden = True
            .CssClass = "itemSeleccionable SinSalto"
        End With
        If grid.Columns(campoGridBoundTemplate.Key) Is Nothing Then
            grid.Columns.Add(campoGridBoundTemplate)
            AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
        End If
        If grid.GridView.Columns.Item(campoGridBoundTemplate.Key) Is Nothing Then
            grid.GridView.Columns.Add(campoGridBoundTemplate)
            AnyadirTextoAColumnaNoFiltrada(gridId, campoGridBoundTemplate.Key, False, False)
        End If
    End Sub
    Private Sub AnyadirTextoAColumnaNoFiltrada(ByVal gridId As String, ByVal fieldKey As String, ByVal EnableFilter As Boolean, ByVal EnableResize As Boolean)
        Dim grid As WebHierarchicalDataGrid = CType(Me.FindControl(gridId), WebHierarchicalDataGrid)
        Dim fieldFilter As Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldFilter = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
        fieldFilter.ColumnKey = fieldKey
        fieldFilter.Enabled = EnableFilter
        fieldFilter.BeforeFilterAppliedAltText = Textos(72) 'Filtro no aplicado
        grid.Behaviors.Filtering.ColumnSettings.Add(fieldFilter)
        grid.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldFilter)

        Dim fieldResize As Infragistics.Web.UI.GridControls.ColumnResizeSetting
        fieldResize = New Infragistics.Web.UI.GridControls.ColumnResizeSetting
        fieldResize.ColumnKey = fieldKey
        fieldResize.EnableResize = EnableResize
        grid.Behaviors.ColumnResizing.ColumnSettings.Add(fieldResize)
        grid.GridView.Behaviors.ColumnResizing.ColumnSettings.Add(fieldResize)
    End Sub
#End Region
#Region "Filtros"
    ''' <summary>
    ''' Si hay cambio en el tab activo, refleja el cambio en las variables de pantalla y de sesion.
    ''' </summary>
    ''' <param name="fsnTab">Tab activo</param>
    ''' <remarks>Llamada desde: fsnTabFiltro_Click; Tiempo máximo: 0,3 sg.</remarks>
    Private Sub CambioFiltro(ByVal fsnTab As FSNWebControls.FSNTab)
        HttpContext.Current.Cache.Remove("dsCertificados_" & FSNUser.Cod)
        HttpContext.Current.Cache.Remove("dtProve_MaterialesQA_" & FSNUser.Cod)
        Dim arrParametros() As String
        arrParametros = Split(CType(fsnTab, FSNWebControls.FSNTab).CommandArgument, "#")
        Dim sCompara As String = IIf(arrParametros(0) = 0, "", arrParametros(0))
        If Not Session("QAFiltroUsuarioIdVCert") = sCompara Then
            'solo se queda chequeada la pestaña del filtro chequeada
            Dim NumeroCertificados As Integer
            Dim lNumCert As Dictionary(Of String, Integer) = CType(Cache("filtroNumCert_" & FSNUser.Cod), Dictionary(Of String, Integer))
            For index As Integer = 0 To dlFiltrosConfigurados.Items.Count - 1
                With CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab)
                    .Selected = False
                    If lNumCert.ContainsKey(Split(.CommandArgument, "#")(0)) Then
                        NumeroCertificados = lNumCert(Split(.CommandArgument, "#")(0))
                        .Text = Replace(.Text, "(" & lNumCert(Split(.CommandArgument, "#")(0)) & ")", "")
                    Else
                        If Split(.CommandArgument, "#")(0) = 0 Then
                            NumeroCertificados = ObtenerDataCountCertificados_NoAsync("", "")
                        Else
                            NumeroCertificados = ObtenerDataCountCertificados_NoAsync(Split(.CommandArgument, "#")(0), Split(.CommandArgument, "#")(1))
                        End If
                    End If
                    .Text = AjustarAnchoTextoPixels(.Text, 140, .Font.Name, 12, False, "(" & NumeroCertificados & ")")
                End With
            Next
            CType(fsnTab, FSNWebControls.FSNTab).Selected = True

            Session("arrDatosGeneralesVCer") = Nothing
            If arrParametros(0) = 0 Then
                Session("QAFiltroUsuarioIdVCert") = "0"
                Session("QANombreTablaVCert") = ""
                Session("QAFiltroIdVCert") = "0"
            Else
                Session("QAFiltroUsuarioIdVCert") = arrParametros(0)
                Session("QANombreTablaVCert") = arrParametros(1)
                Session("QAFiltroIdVCert") = arrParametros(2)
            End If
            Session("QApageNumberVCert") = "0"

            QAFiltroUsuarioIdVCert.Value = Session("QAFiltroUsuarioIdVCert")
            QANombreTablaVCert.Value = Session("QANombreTablaVCert")
            QAFiltroIdVCert.Value = Session("QAFiltroIdVCert")

            hFiltered.Value = 0
            hOrdered.Value = ""
            whgCertificados.GridView.Behaviors.Sorting.SortedColumns.Clear()
            whgCertificados.GroupingSettings.GroupedColumns.Clear()

            Dim FiltroUsu As String = IIf(Session("QAFiltroUsuarioIdVCert") = "", "0", Session("QAFiltroUsuarioIdVCert"))
            btnConfigurar.Attributes.Add("onClick", "window.location='../FiltrosQA/ConfigurarFiltrosQA.aspx?IDFiltroUsu=" & FiltroUsu & "&NombreTabla=" & Session("QANombreTablaVCert") & "&Donde=VisorCert';return false;")

            LimpiarBusquedaAvanzada()
            CargarBusquedaAvanzada()
            hCacheLoaded.Value = 0
            updFiltros.Update()
        End If
    End Sub
    ''' <summary>
    ''' Carga en la grid las columnas y las instancias del filtro seleccionado. 
    ''' Carga el desplegable con las columnas, el nº de instancias por estado y activa la pestaña del filtro.
    ''' Muestra el botón de Configurar para hacer modificaciones en la configuración del filtro.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: Al hacer click sobre una de las pestañas de los filtros. Tiempo máximo: 0 sg.</remarks>
    Protected Sub fsnTabFiltro_Click(ByVal sender As Object, ByVal e As EventArgs)
        whgCertificados.Behaviors.Filtering.ColumnSettings.Clear()
        whgCertificados.GridView.Behaviors.Filtering.ColumnSettings.Clear()

        whgCertificados.Behaviors.Filtering.ColumnFilters.Clear()
        whgCertificados.GridView.Behaviors.Filtering.ColumnFilters.Clear()

        whgCertificados.GridView.Behaviors.Filtering.ApplyFilter()
        whgCertificados.Columns.Clear()
        whgCertificados.GridView.Columns.Clear()

        CambioFiltro(CType(sender, FSNWebControls.FSNTab))
        whgCertificados.GridView.Behaviors.Paging.PageIndex = 0

        CargarCertificados()
    End Sub
    ''' <summary>
    ''' Refleja en el control paginador los ultimos cambios en los certificados a mostrar
    ''' </summary>
    ''' <param name="_pageCount">Numero de paginas</param>
    ''' <remarks>Llamada desde:whgCertificados_DataBound; Tiempo maximo: 0,2 </remarks>
    Private Sub Paginador(ByVal _pageCount As Integer)
        Dim pagerList As DropDownList = DirectCast(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
        pagerList.Items.Clear()
        For i As Integer = 1 To _pageCount
            pagerList.Items.Add(i.ToString())
        Next
        CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = _pageCount
        If _pageCount > 0 Then pagerList.SelectedIndex = _pageNumber
        Dim Desactivado As Boolean = (_pageNumber = 0)
        With CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        With CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        Desactivado = (_pageCount = 1 OrElse _pageNumber + 1 = _pageCount)
        With CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        With CType(whgCertificados.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
    End Sub
#End Region
#Region "Eventos Grid"
    ''' <summary>
    ''' Tras traer datos (Bound) actualiza el Paginador
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub whgCertificados_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles whgCertificados.DataBound
        Dim _pageCount As Integer
        Dim _rows As Integer
        Dim dt As DataTable = CType(whgCertificados.DataSource, DataSet).Tables("CERTIFICADOS")
        If CType(hFiltered.Value, Boolean) Then
            _rows = dt.Rows.Count
        Else
            Dim Tab As FSNWebControls.FSNTab
            For index As Integer = 0 To dlFiltrosConfigurados.Items.Count - 1
                Tab = CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab)
                If Tab.Selected Then
                    Dim filtroNumCert As Dictionary(Of String, Integer)
                    filtroNumCert = CType(HttpContext.Current.Cache("filtroNumCert_" & FSNUser.Cod), Dictionary(Of String, Integer))
                    Dim filtro As Integer = If(Split(Tab.CommandArgument, "#")(0) = "", 0, Split(Tab.CommandArgument, "#")(0))
                    If filtroNumCert.ContainsKey(filtro) Then
                        _rows = filtroNumCert(filtro)
                    Else
                        If filtro = 0 Then
                            _rows = ObtenerDataCountCertificados_NoAsync("", "")
                        Else
                            _rows = ObtenerDataCountCertificados_NoAsync(filtro, Split(Tab.CommandArgument, "#")(1))
                        End If
                    End If
                    Exit For
                End If
            Next
        End If
        _pageCount = _rows \ System.Configuration.ConfigurationManager.AppSettings("PageSize")
        _pageCount = _pageCount + (IIf(_rows Mod System.Configuration.ConfigurationManager.AppSettings("PageSize") = 0, 0, 1))
        Paginador(_pageCount)
        updGridCertificados.Update()
    End Sub
    ''' <summary>
    ''' Tras ordenar actualiza el grid
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub whgCertificados_ColumnSorted(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.SortingEventArgs) Handles whgCertificados.ColumnSorted
        Dim sName As String
        Dim sortCriteria As String = String.Empty
        Dim sortDirection As String = String.Empty
        Dim bdatabind As Boolean = False
        Dim sColsGroupBy As String = "ColsGroupBy" & Session("QAFiltroUsuarioIdVCert")

        If e.SortedColumns.Count > 0 Then
            e.SortedColumns.Remove(CType(sender, Infragistics.Web.UI.GridControls.ContainerGrid).Behaviors.Sorting.SortedColumns.Item("DATAKEY"))
            e.SortedColumns.Add(CType(sender, Infragistics.Web.UI.GridControls.ContainerGrid).Columns.Item("DATAKEY"), Infragistics.Web.UI.SortDirection.Ascending)
            For Each sortColumn As Infragistics.Web.UI.GridControls.SortedColumnInfo In e.SortedColumns
                sName = sortColumn.ColumnKey.ToString.Replace("BOOL_", "")
                sortDirection = IIf(sortColumn.SortDirection = Infragistics.Web.UI.SortDirection.Descending, " DESC", " ASC")
                sortCriteria = IIf(sortCriteria Is String.Empty, sName & sortDirection, sortCriteria & "," & sName & sortDirection)
            Next
        End If
        If Session("QAFiltroUsuarioIdVCert") = "" Then
            'Lo almacenamos en cookie
            Dim cookie As HttpCookie
            cookie = Request.Cookies("ORDENACION_QA_CERT")
            If cookie Is Nothing Then
                cookie = New HttpCookie("ORDENACION_QA_CERT")
            End If
            cookie.Value = sortCriteria
            cookie.Expires = Date.MaxValue
            Response.AppendCookie(cookie)
        Else
            'Lo Almacenamos en BBDD
            Dim cFiltro As FiltroQA
            cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroQA))
            cFiltro.IDFiltroUsuario = Session("QAFiltroUsuarioIdVCert")
            cFiltro.TipoSol = TiposDeDatos.TipoDeSolicitud.Certificado
            cFiltro.Ordenacion = sortCriteria

            cFiltro.GuardarOrdenacion()

            cFiltro = Nothing
        End If
        hOrdered.Value = sortCriteria

        updGridCertificados.Update()
    End Sub
    ''' <summary>
    ''' Tras filtrarse actualiza el grid
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub whgCertificados_DataFiltered(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.FilteredEventArgs) Handles whgCertificados.DataFiltered
        updGridCertificados.Update()
    End Sub
    ''' <summary>
    ''' Tras agrupar actualiza el grid
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub whgCertificados_GroupedColumnsChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GroupedColumnsChangedEventArgs) Handles whgCertificados.GroupedColumnsChanged
        Dim sColsGroupBy As String = "ColsGroupBy" & Session("QAFiltroUsuarioIdVCert")
        Dim groupedColumnList As String = String.Empty
        For Each column As Infragistics.Web.UI.GridControls.GroupedColumn In e.GroupedColumns
            groupedColumnList = IIf(groupedColumnList Is String.Empty, String.Empty, "#")
            groupedColumnList &= column.ColumnKey & IIf(column.SortDirection = Infragistics.Web.UI.GridControls.GroupingSortDirection.Ascending, " ASC", " DESC")
        Next
        Session(sColsGroupBy) = groupedColumnList

        updGridCertificados.Update()
    End Sub
    ''' <summary>
    ''' Rellena los campos q no vienen desde bbdd, es decir, las denominaciones, imagenes, ...
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub whgCertificados_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whgCertificados.InitializeRow
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados
        e.Row.Height = Unit.Pixel(25)
        Dim cellIndex, gridCellIndex As Integer
        Dim items As Infragistics.Web.UI.GridControls.GridRecordItemCollection = e.Row.Items

        If Not IsDBNull(e.Row.DataItem.Item.Row.Item("EN_PROCESO")) AndAlso e.Row.DataItem.Item.Row.Item("EN_PROCESO") = 1 Then
            gridCellIndex = whgCertificados.GridView.Columns.FromKey("EN_PROCESO").Index
            With items(gridCellIndex)
                .Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Icono_Error_Amarillo.gif" & "'/>"
            End With
        Else
            items(gridCellIndex).Text = ""
        End If
        ''Proveedor
        Dim Potencial As String = e.Row.DataItem.Item.Row.Item("REAL")
        Dim Baja As String = e.Row.DataItem.Item.Row.Item("BAJA")
        Dim sFile As String = "tipocalidad_" & Potencial & "_" & Baja
        If Acceso.gbPotAValProvisionalSelProve AndAlso Potencial = "2" AndAlso e.Row.DataItem.Item.Row.Item("PROVISIONAL") = "1" Then sFile &= "_" & e.Row.DataItem.Item.Row.Item("PROVISIONAL")
        If whgCertificados.GridView.Columns.Item("PROVE_DEN") IsNot Nothing Then
            gridCellIndex = whgCertificados.GridView.Columns.FromKey("IMAGE_PROVE").Index
            With items(gridCellIndex)
                .Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/" & sFile & ".gif" & "'/>"
            End With
        End If
        'Publicado
        If whgCertificados.GridView.Columns.Item("BOOL_PUBLICADA") IsNot Nothing Then
            gridCellIndex = whgCertificados.GridView.Columns.FromKey("BOOL_PUBLICADA").Index
            If DBNullToBoolean(e.Row.DataItem.Item.Row.Item("BOOL_PUBLICADA")) Then
                items(gridCellIndex).Text = Textos(13)
            Else
                items(gridCellIndex).Text = Textos(14)
            End If
        End If
        'Validado()
        If whgCertificados.GridView.Columns.Item("BOOL_VALIDADO") IsNot Nothing Then
            gridCellIndex = whgCertificados.GridView.Columns.FromKey("BOOL_VALIDADO").Index
            If IsDBNull(e.Row.DataItem.Item.Row.Item("BOOL_VALIDADO")) Then
                items(gridCellIndex).Text = "" '"Vacio" No se valida
            ElseIf DBNullToBoolean(e.Row.DataItem.Item.Row.Item("BOOL_VALIDADO")) = True Then
                items(gridCellIndex).Text = Textos(13) ' "SÃ­"            
            ElseIf DBNullToBoolean(e.Row.DataItem.Item.Row.Item("BOOL_VALIDADO")) = False Then
                items(gridCellIndex).Text = Textos(14) '"No"
            Else
                items(gridCellIndex).Text = "" '"Vacio" No se valida
            End If
        End If
        If whgCertificados.GridView.Columns.Item("VALIDADO") IsNot Nothing Then
            gridCellIndex = whgCertificados.GridView.Columns.FromKey("VALIDADO").Index
            If DBNullToInteger(e.Row.DataItem.Item.Row.Item("VALIDADO")).Equals(1) Then
                items(gridCellIndex).Text = Textos(13) ' "SÃ­"            
            ElseIf DBNullToInteger(e.Row.DataItem.Item.Row.Item("VALIDADO")).Equals(0) Then
                items(gridCellIndex).Text = Textos(14) '"No"
            Else
                items(gridCellIndex).Text = "" '"Vacio" No se valida
            End If
        End If
        'Real
        If whgCertificados.GridView.Columns.Item("BOOL_REAL") IsNot Nothing Then
            gridCellIndex = whgCertificados.GridView.Columns.FromKey("BOOL_REAL").Index
            If e.Row.DataItem.Item.Row.Item("BOOL_REAL") Then   '2-Real
                items(gridCellIndex).Text = Textos(13) ' "SÃ­"
            Else '1-potencial 0-El stored no lo acepta (FSQ_GET_CERTIFICADOS)
                items(gridCellIndex).Text = Textos(14) '"No"
            End If
        End If
        'Baja
        If whgCertificados.GridView.Columns.Item("BOOL_BAJA") IsNot Nothing Then
            gridCellIndex = whgCertificados.GridView.Columns.FromKey("BOOL_BAJA").Index
            If DBNullToBoolean(e.Row.DataItem.Item.Row.Item("BOOL_BAJA")) Then
                items(gridCellIndex).Text = Textos(13) ' "Sí"
            Else
                items(gridCellIndex).Text = Textos(14) '"No"
            End If
        End If
        'Modo
        If whgCertificados.GridView.Columns.Item("MODO_SOLIC") IsNot Nothing Then
            gridCellIndex = whgCertificados.GridView.Columns.FromKey("MODO_SOLIC").Index
            If e.Row.DataItem.Item.Row.Item("MODO_SOLIC") = 0 Then
                items(gridCellIndex).Text = Textos(60)
            Else
                items(gridCellIndex).Text = Textos(61)
            End If
        End If
        'Opcional
        If whgCertificados.GridView.Columns.Item("OBLIGATORIO") IsNot Nothing Then
            gridCellIndex = whgCertificados.GridView.Columns.FromKey("OBLIGATORIO").Index
            If DBNullToBoolean(e.Row.DataItem.Item.Row.Item("OBLIGATORIO")) Then
                With items(gridCellIndex)
                    .Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "Images/Asterisco.png'/>" '"No"
                    .CssClass = "itemCenterAligment SinSalto"
                End With
            Else
                items(gridCellIndex).Text = "" ' "Sí"
            End If
        End If

        If Me.Acceso.gbPotAValProvisionalSelProve Then
            'Provisional
            If whgCertificados.GridView.Columns.Item("PROVISIONAL") IsNot Nothing Then
                gridCellIndex = whgCertificados.GridView.Columns.FromKey("PROVISIONAL").Index
                If DBNullToBoolean(e.Row.DataItem.Item.Row.Item("PROVISIONAL")) Then
                    items(gridCellIndex).Text = Textos(13)
                Else
                    items(gridCellIndex).Text = Textos(14)
                End If
            End If
        End If
        If whgCertificados.GridView.Columns.Item("ESTADO_DEN") IsNot Nothing Then
            cellIndex = whgCertificados.GridView.Columns.FromKey("ESTADO_DEN").Index
            gridCellIndex = whgCertificados.GridView.Columns.FromKey("IMAGE_ESTADO").Index
            Select Case CType(e.Row.DataItem.Item.Row.Item("ESTADO"), Integer)
                Case 0 'Autom
                    items(cellIndex).CssClass = "certificadoOpcional SinSalto"
                Case 1 'Sin solicitar
                    items(cellIndex).CssClass = "certificadoOpcional SinSalto"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_Opcional.gif" & "'/>"
                Case 2 'Pendiente de cumplimentar
                    If Not IsDBNull(e.Row.DataItem.Item.Row.Item("NUM_VERSION")) AndAlso Not CType(e.Row.DataItem.Item.Row.Item("NUM_VERSION"), Integer) = 0 Then
                        e.Row.CssClass = e.Row.CssClass & " itemSeleccionable"
                    End If
                    items(cellIndex).CssClass = "certificadoPendiente SinSalto"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_Pendiente.gif" & "'/>"
                    If Not e.Row.Items.FindItemByKey("FEC_LIM_CUMPLIM") Is Nothing AndAlso Not IsDBNull(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM")) Then
                        If CType(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM"), Date) < Date.Today Then
                            e.Row.Items.FindItemByKey("FEC_LIM_CUMPLIM").CssClass = "datoRojo SinSalto"
                        End If
                    End If
                Case 3 'Vigente
                    e.Row.CssClass = e.Row.CssClass & " itemSeleccionable SinSalto"
                    items(cellIndex).CssClass = "certificadoVigente SinSalto"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_Vigente.gif" & "'/>"
                Case 4 'Próximo a expirar
                    e.Row.CssClass = e.Row.CssClass & " itemSeleccionable SinSalto"
                    items(cellIndex).CssClass = "certificadoExpirado SinSalto"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_Expirado.gif" & "'/>"
                    If Not e.Row.Items.FindItemByKey("FEC_LIM_CUMPLIM") Is Nothing AndAlso Not IsDBNull(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM")) Then
                        If CType(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM"), Date) < Date.Today Then
                            e.Row.Items.FindItemByKey("FEC_LIM_CUMPLIM").CssClass = "datoRojo SinSalto"
                        End If
                    End If
                Case 5 'Expirado
                    e.Row.CssClass = e.Row.CssClass & " itemSeleccionable SinSalto"
                    items(cellIndex).CssClass = "certificadoExpirado SinSalto"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_Expirado.gif" & "'/>"
                    If Not e.Row.Items.FindItemByKey("FEC_LIM_CUMPLIM") Is Nothing AndAlso Not IsDBNull(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM")) Then
                        If CType(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM"), Date) < Date.Today Then
                            e.Row.Items.FindItemByKey("FEC_LIM_CUMPLIM").CssClass = "datoRojo SinSalto"
                        End If
                    End If
                Case 6 'Pendiente de Validar
                    e.Row.CssClass = e.Row.CssClass & " itemSeleccionable SinSalto"
                    items(cellIndex).CssClass = "certificadoPendienteValidar SinSalto"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_PendienteValidar.png" & "'/>"
                Case 7
                    e.Row.CssClass = e.Row.CssClass & " itemSeleccionable SinSalto"
                    items(cellIndex).CssClass = "certificadoNoValidado SinSalto"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_NoValidado.png" & "'/>"
            End Select
        End If
        For Each item As GridField In whgCertificados.GridView.Columns
            If item.Type = System.Type.GetType("System.DateTime") Then
                gridCellIndex = whgCertificados.GridView.Columns.FromKey(item.Key).Index
                CType(items(gridCellIndex).Column, Infragistics.Web.UI.GridControls.BoundDataField).DataFormatString = "{0:" & FSNUser.DateFormat.ShortDatePattern & "}"
                CType(items(gridCellIndex).Column, Infragistics.Web.UI.GridControls.BoundDataField).CssClass = "itemCenterAligment"
            ElseIf Left(item.Key, 7) = "BOOL_C_" Then
                If Not (whgCertificados.GridView.Columns.FromKey(item.Key).Hidden) Then
                    gridCellIndex = whgCertificados.GridView.Columns.FromKey(item.Key).Index
                    If e.Row.DataItem.Item.Row.Item(item.Key) IsNot System.DBNull.Value Then
                        If e.Row.DataItem.Item.Row.Item(item.Key) Then
                            items(gridCellIndex).Text = Textos(13) ' "Sí"            
                        Else
                            items(gridCellIndex).Text = Textos(14) '"No"
                        End If
                    Else
                        items(gridCellIndex).Text = ""
                    End If
                End If
            End If

            If item.Key = "ID" Or item.Key = "INSTANCIA" Then
                item.CssClass = "itemRightAligment"
            End If
        Next

        If e.Row.DataItem.Item.Row.Item("NOPETIC") = 1 AndAlso (Not whgCertificados.GridView.Columns.FromKey("DEN_TIPO_CERTIFICADO") Is Nothing) Then
            gridCellIndex = whgCertificados.GridView.Columns.FromKey("DEN_TIPO_CERTIFICADO").Index
            items(gridCellIndex).CssClass = "certificadoNoPeticionario SinSalto"
        End If
    End Sub
    Private Sub whgCertificadosExportacion_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whgCertificadosExportacion.InitializeRow
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados
        e.Row.Height = Unit.Pixel(25)
        Dim cellIndex, gridCellIndex As Integer
        Dim items As Infragistics.Web.UI.GridControls.GridRecordItemCollection = e.Row.Items

        If Not IsDBNull(e.Row.DataItem.Item.Row.Item("EN_PROCESO")) AndAlso e.Row.DataItem.Item.Row.Item("EN_PROCESO") = 1 Then
            gridCellIndex = whgCertificadosExportacion.GridView.Columns.FromKey("EN_PROCESO").Index
            With items(gridCellIndex)
                .Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Icono_Error_Amarillo.gif" & "'/>"
            End With
        Else
            items(gridCellIndex).Text = ""
        End If
        ''Proveedor
        Dim Potencial As String = e.Row.DataItem.Item.Row.Item("REAL")
        Dim Baja As String = e.Row.DataItem.Item.Row.Item("BAJA")
        If whgCertificadosExportacion.GridView.Columns.Item("PROVE_DEN") IsNot Nothing Then
            gridCellIndex = whgCertificadosExportacion.GridView.Columns.FromKey("IMAGE_PROVE").Index
            With items(gridCellIndex)
                .Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/tipocalidad_" & Potencial & "_" & Baja & ".gif" & "'/>"
            End With
        End If
        'Publicado
        If whgCertificadosExportacion.GridView.Columns.Item("PUBLICADA") IsNot Nothing Then
            gridCellIndex = whgCertificadosExportacion.GridView.Columns.FromKey("PUBLICADA").Index
            If DBNullToBoolean(e.Row.DataItem.Item.Row.Item("PUBLICADA")) Then
                items(gridCellIndex).Text = Textos(13) ' "Sí"            
            Else
                items(gridCellIndex).Text = Textos(14) '"No"
            End If
        End If
        'Validado
        If whgCertificadosExportacion.GridView.Columns.Item("VALIDADO") IsNot Nothing Then
            gridCellIndex = whgCertificadosExportacion.GridView.Columns.FromKey("VALIDADO").Index
            If DBNullToBoolean(e.Row.DataItem.Item.Row.Item("VALIDADO")) Then
                items(gridCellIndex).Text = Textos(13) ' "Sí"            
            Else
                items(gridCellIndex).Text = Textos(14) '"No"
            End If
        End If
        'Real
        If whgCertificadosExportacion.GridView.Columns.Item("REAL") IsNot Nothing Then
            gridCellIndex = whgCertificadosExportacion.GridView.Columns.FromKey("REAL").Index
            If e.Row.DataItem.Item.Row.Item("REAL") = 2 Then    '1-Potencial, 2-Real
                items(gridCellIndex).Text = Textos(13) ' "Sí"
            Else
                items(gridCellIndex).Text = Textos(14) '"No"
            End If
        End If
        'Baja
        If whgCertificadosExportacion.GridView.Columns.Item("BAJA") IsNot Nothing Then
            gridCellIndex = whgCertificadosExportacion.GridView.Columns.FromKey("BAJA").Index
            If DBNullToBoolean(e.Row.DataItem.Item.Row.Item("BAJA")) Then
                items(gridCellIndex).Text = Textos(13) ' "Sí"
            Else
                items(gridCellIndex).Text = Textos(14) '"No"
            End If
        End If
        'Modo
        If whgCertificadosExportacion.GridView.Columns.Item("MODO_SOLIC") IsNot Nothing Then
            gridCellIndex = whgCertificadosExportacion.GridView.Columns.FromKey("MODO_SOLIC").Index
            If e.Row.DataItem.Item.Row.Item("MODO_SOLIC") = 0 OrElse e.Row.DataItem.Item.Row.Item("MODO_SOLIC") = 1 Then
                items(gridCellIndex).Text = Textos(60)
            Else
                items(gridCellIndex).Text = Textos(61)
            End If
        End If
        'Opcional
        If whgCertificadosExportacion.GridView.Columns.Item("OBLIGATORIO") IsNot Nothing Then
            gridCellIndex = whgCertificadosExportacion.GridView.Columns.FromKey("OBLIGATORIO").Index
            If DBNullToBoolean(e.Row.DataItem.Item.Row.Item("OBLIGATORIO")) Then
                With items(gridCellIndex)
                    .Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "Images/Asterisco.png'/>" '"No"
                    .CssClass = "itemCenterAligment SinSalto"
                End With
            Else
                items(gridCellIndex).Text = "" ' "Sí"
            End If
        End If
        If whgCertificadosExportacion.GridView.Columns.Item("ESTADO_DEN") IsNot Nothing Then
            cellIndex = whgCertificadosExportacion.GridView.Columns.FromKey("ESTADO_DEN").Index
            gridCellIndex = whgCertificadosExportacion.GridView.Columns.FromKey("IMAGE_ESTADO").Index
            Select Case CType(e.Row.DataItem.Item.Row.Item("ESTADO"), Integer)
                Case 0 'Autom
                    items(cellIndex).CssClass = "certificadoOpcional SinSalto"
                Case 1 'Sin solicitar
                    items(cellIndex).CssClass = "certificadoOpcional SinSalto"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_Opcional.gif" & "'/>"
                Case 2 'Pendiente de cumplimentar
                    items(cellIndex).CssClass = "certificadoPendiente SinSalto"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_Pendiente.gif" & "'/>"
                    If Not e.Row.Items.FindItemByKey("FEC_LIM_CUMPLIM") Is Nothing AndAlso Not IsDBNull(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM")) Then
                        If CType(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM"), Date) < Date.Today Then
                            e.Row.Items.FindItemByKey("FEC_LIM_CUMPLIM").CssClass = "datoRojo SinSalto"
                        End If
                    End If
                Case 3 'Vigente
                    e.Row.CssClass = e.Row.CssClass & " itemSeleccionable SinSalto"
                    items(cellIndex).CssClass = "certificadoVigente SinSalto"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_Vigente.gif" & "'/>"
                Case 4 'Próximo a expirar
                    e.Row.CssClass = e.Row.CssClass & " itemSeleccionable SinSalto"
                    items(cellIndex).CssClass = "certificadoExpirado SinSalto"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_Expirado.gif" & "'/>"
                    If Not e.Row.Items.FindItemByKey("FEC_LIM_CUMPLIM") Is Nothing AndAlso Not IsDBNull(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM")) Then
                        If CType(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM"), Date) < Date.Today Then
                            e.Row.Items.FindItemByKey("FEC_LIM_CUMPLIM").CssClass = "datoRojo SinSalto"
                        End If
                    End If
                Case 5 'Expirado
                    e.Row.CssClass = e.Row.CssClass & " itemSeleccionable SinSalto"
                    items(cellIndex).CssClass = "certificadoExpirado SinSalto"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_Expirado.gif" & "'/>"
                    If Not e.Row.Items.FindItemByKey("FEC_LIM_CUMPLIM") Is Nothing AndAlso Not IsDBNull(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM")) Then
                        If CType(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM"), Date) < Date.Today Then
                            e.Row.Items.FindItemByKey("FEC_LIM_CUMPLIM").CssClass = "datoRojo SinSalto"
                        End If
                    End If
                Case 6 'Pendiente de Validar
                    e.Row.CssClass = e.Row.CssClass & " itemSeleccionable SinSalto"
                    items(cellIndex).CssClass = "certificadoPendienteValidar SinSalto"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_PendienteValidar.png" & "'/>"
                Case 7 'No validado
                    e.Row.CssClass = e.Row.CssClass & " itemSeleccionable SinSalto"
                    items(cellIndex).CssClass = "certificadoNoValidado SinSalto"
                    items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_NoValidado.png" & "'/>"
            End Select
        End If
        For Each item As Object In whgCertificadosExportacion.GridView.Columns
            If item.Type = System.Type.GetType("System.DateTime") Then
                gridCellIndex = whgCertificadosExportacion.GridView.Columns.FromKey(item.Key).Index
                CType(items(gridCellIndex).Column, Infragistics.Web.UI.GridControls.BoundDataField).DataFormatString = "{0:" & FSNUser.DateFormat.ShortDatePattern & "}"
            ElseIf item.Type = System.Type.GetType("System.Byte") _
            AndAlso Left(item.Key, 2) = "C_" _
            AndAlso Not (item.Hidden) _
            AndAlso e.Row.DataItem.Item.Row.Item(item.Key) IsNot System.DBNull.Value Then
                gridCellIndex = whgCertificadosExportacion.GridView.Columns.FromKey(item.Key).Index
                If e.Row.DataItem.Item.Row.Item(item.Key) Then
                    items(gridCellIndex).Text = Textos(13) ' "Sí"            
                Else
                    items(gridCellIndex).Text = Textos(14) '"No"
                End If
            End If

        Next
    End Sub
    ''Codigo para guardar datos del FSAL cuando el grid termine de cargarse en el servidor.
    ''Llama a la funcion de registro con diferentes parametros dependiendo de si hay que actualizar o registrar uno nuevo.
    ''
    Private Sub whgCertificados_Unload(sender As Object, e As System.EventArgs) Handles whgCertificados.Unload

        If bActivadoFSAL.Value = "1" Then
            Dim fechaFinFSAL8 As DateTime = DateTime.UtcNow
            If bEnviadoFSAL8Inicial.Value = "0" AndAlso IsPostBack Then
                Dim sArgs(5) As Object
                sArgs(0) = 8
                sArgs(1) = sFechaIniFSAL8.Value
                sArgs(2) = Format(fechaFinFSAL8, "yyyy-MM-dd HH:mm:ss") & "." & Format(fechaFinFSAL8.Millisecond, "000")
                sArgs(3) = sIdRegistroFSAL1.Value
                sArgs(4) = sPagina.Value
                sArgs(5) = iP.Value
                System.Threading.ThreadPool.QueueUserWorkItem(New System.Threading.WaitCallback(AddressOf CallFSALRegistrarActualizarAcceso), sArgs)
            ElseIf bEnviadoFSAL8Inicial.Value = "1" AndAlso HttpContext.Current.Request.Headers("x-ajax") Is Nothing Then
                Dim sArgs(12) As Object
                sArgs(0) = 1
                sArgs(1) = sProducto.Value
                sArgs(2) = sFechaIniFSAL8.Value
                sArgs(3) = Format(fechaFinFSAL8, "yyyy-MM-dd HH:mm:ss") & "." & Format(fechaFinFSAL8.Millisecond, "000")
                sArgs(4) = sPagina.Value
                sArgs(5) = iPost.Value
                sArgs(6) = iP.Value
                sArgs(7) = sUsuCod.Value
                sArgs(8) = sPaginaOrigen.Value
                sArgs(9) = sNavegador.Value
                sArgs(10) = sIdRegistroFSAL8.Value
                sArgs(11) = sProveCod.Value
                sArgs(12) = sQueryString.Value
                System.Threading.ThreadPool.QueueUserWorkItem(New System.Threading.WaitCallback(AddressOf CallFSALRegistrarActualizarAcceso), sArgs)
            End If
        End If

    End Sub
    ''
    ''Fin codigo FSAL
#End Region
#Region "Mantenimiento Busqueda Avanzada"
    ''' <summary>
    ''' Limpia los criterios de busqueda avanzada.
    ''' Aparte si resulta estas accediendo a la página por primera vez, mostraremos por defecto el resultado de buscar todos los materiales,
    ''' todos los tipos de certificados y todos los proveedores (potenciales y del panel de calidad), y ordenaremos los resultados por fecha
    ''' de solicitud de más a menos recientes.
    ''' </summary>
    ''' <remarks>Llamada desde: fsnTabFiltro_Click // btnLimpiarBusquedaAvanzada_Click //  Page_LoadComplete. Tiempo máximo: 0 sg.</remarks>
    Private Sub LimpiarBusquedaAvanzada(Optional ByVal CargarPorDefecto As Boolean = False)
        Busqueda.Materiales = ""
        Busqueda.TipoSolicitudes = ""
        Busqueda.VerDadosBaja = False
        Busqueda.MostrarProveedores = 3
        Busqueda.ProveedorTexto = ""
        Busqueda.ProveedorHidden = ""
        Busqueda.ProveedorDeBaja = 0
        Busqueda.Estados = ""
        Busqueda.Peticionario = ""
        Busqueda.FechasDe = 1
        Busqueda.FechaDesde = Nothing
        Busqueda.FechaHasta = Nothing

        If CargarPorDefecto Then
            Busqueda.CargaPorDefecto()
            AlmacenarBusquedaAvanzada()
        End If
        upBusquedaAvanzada.Update()
    End Sub
    ''' <summary>
    ''' Carga la ultima situacion conocida del usuario.
    ''' </summary>
    ''' <remarks>Llamada desde= Page_load ; Tiempo ejecucion:=0,1</remarks>
    Private Sub cargarSituacionActual()
        Dim FiltroUsuarioIdAnt, NombreTablaAnt, FiltroIdAnt As String

        FiltroUsuarioIdAnt = Request.QueryString("FiltroUsuarioIdAnt")
        NombreTablaAnt = Request.QueryString("NombreTablaAnt")
        FiltroIdAnt = Request.QueryString("FiltroIdAnt")
        FiltroIdAnt = IIf(FiltroIdAnt = "", 0, FiltroIdAnt)

        Dim arrParametros() As String = Nothing
        Dim bEncontradoTab As Boolean = False
        Dim Tab As FSNWebControls.FSNTab = Nothing
        'Seleccionar el filtro
        If FiltroUsuarioIdAnt <> "" Then 'Ha venido de volver (Seleccionamos el filtro que tenia seleccionado anteriormente)
            m_bConConfiguracionAnterior = True
            Dim sTabSeleccionado As String = FiltroUsuarioIdAnt & "#" & NombreTablaAnt & "#" & FiltroIdAnt
            For index As Integer = 0 To dlFiltrosConfigurados.Items.Count - 1
                If sTabSeleccionado = CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).CommandArgument Then
                    arrParametros = Split(CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).CommandArgument, "#")
                    CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).Selected = True
                    Tab = CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab)
                    bEncontradoTab = True
                    Exit For
                End If
            Next
        Else
            m_bConConfiguracionAnterior = False
        End If
        If Not bEncontradoTab Then ' Si no encuentra ninguno, o es la primera vez, selecciono el primero
            arrParametros = Split(CType(dlFiltrosConfigurados.Items(0).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).CommandArgument, "#")
            CType(dlFiltrosConfigurados.Items(0).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).Selected = True
            Tab = CType(dlFiltrosConfigurados.Items(0).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab)
        End If

        If arrParametros(0) = 0 Then
            Session("QAFiltroUsuarioIdVCert") = ""
            Session("QANombreTablaVCert") = ""
            Session("QAFiltroIdVCert") = ""
        Else
            Session("QAFiltroUsuarioIdVCert") = arrParametros(0)
            Session("QANombreTablaVCert") = arrParametros(1)
            Session("QAFiltroIdVCert") = arrParametros(2)
        End If

        QAFiltroUsuarioIdVCert.Value = Session("QAFiltroUsuarioIdVCert")
        QANombreTablaVCert.Value = Session("QANombreTablaVCert")
        QAFiltroIdVCert.Value = Session("QAFiltroIdVCert")

        CargarBusquedaAvanzada()

        Dim NumeroCertificados As Integer
        Dim TextoTab As String
        If arrParametros(0) = 0 Then
            NumeroCertificados = ObtenerDataCountCertificados_NoAsync("", "")
        Else
            NumeroCertificados = ObtenerDataCountCertificados_NoAsync(arrParametros(0), arrParametros(1))
        End If
        Tab.Attributes.Add("NumCertificados", NumeroCertificados)
        TextoTab = "(" & NumeroCertificados & ")"
        Tab.Text = AjustarAnchoTextoPixels(Tab.Text, 140, Tab.Font.Name, 12, False, TextoTab)

        Dim filtroNumCert As New Dictionary(Of String, Integer)
        filtroNumCert(IIf(arrParametros(0) = "", 0, arrParametros(0))) = NumeroCertificados
        InsertarEnCache("filtroNumCert_" & FSNUser.Cod, filtroNumCert)

        Dim FiltroUsu As String = IIf(Session("QAFiltroUsuarioIdVCert") = "", "0", Session("QAFiltroUsuarioIdVCert"))
        btnConfigurar.Attributes.Add("onClick", "window.location='../FiltrosQA/ConfigurarFiltrosQA.aspx?IDFiltroUsu=" & FiltroUsu & "&NombreTabla=" & Session("QANombreTablaVCert") & "&Donde=VisorCert';return false;")

        hCacheLoaded.Value = 0
        updFiltros.Update()
    End Sub
    ''' <summary>
    ''' Cargar en el módulo de busqueda avanzada los datos configurados en el filtro.
    ''' Se hace con una variable de session porque cuando se carga la pagina se necesita acceder a esta funcion 2 veces. una vez que se carga completamente la pagina y otra antes
    ''' </summary>
    ''' <param name="UsarCargarBusquedaAvanzada">Resulta q el Request.QueryString("CargarBusquedaAvanzada") se queda a 1 si configuras un filtro mientras no cambies de filtro. Entonces btnBuscarBusquedaAvanzada_Click q carga en el controluser Busqueda     
    '''                                          lo q hay en pantalla a menos q Request.QueryString("CargarBusquedaAvanzada") sea no nulo. En este caso lo lee de bbdd. Este parametro impide q vaya a bbdd.</param> 
    ''' <remarks>Llamada desde: Page_PreRender. Tiempo máximo: 1 sg.</remarks>
    Private Sub CargarBusquedaAvanzada(Optional ByVal UsarCargarBusquedaAvanzada As Boolean = True)
        If Not m_bConConfiguracionAnterior OrElse ((Not Request.QueryString("CargarBusquedaAvanzada") Is Nothing) AndAlso UsarCargarBusquedaAvanzada) Then
            If Session("QAFiltroUsuarioIdVCert") <> "" Then
                If Session("QA_BusquedaAvanzadaVCert" & Session("QAFiltroUsuarioIdVCert").ToString) Is Nothing _
                OrElse ((Not Request.QueryString("CargarBusquedaAvanzada") Is Nothing) AndAlso UsarCargarBusquedaAvanzada) Then
                    Dim sFechaDesde As String = ""
                    Dim sFechaHasta As String = ""
                    Dim cFiltro As FiltroQA
                    cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroQA))

                    cFiltro.IDFiltroUsuario = Session("QAFiltroUsuarioIdVCert")
                    cFiltro.TipoSol = TiposDeDatos.TipoDeSolicitud.Certificado
                    cFiltro.LoadConfiguracionCamposBusqueda(Idioma)

                    Busqueda.Filtro = cFiltro.IDFiltro
                    If Not cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA") Is Nothing Then
                        If cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows.Count > 0 Then
                            Busqueda.Materiales = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("MATERIALES")
                            Busqueda.TipoSolicitudes = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("TIPOSOL")
                            Busqueda.VerDadosBaja = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("BAJA")

                            Busqueda.MostrarProveedores = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("QUEMOSTRAR")
                            If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDOR")) Then
                                Busqueda.ProveedorTexto = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDOR")
                            End If
                            Busqueda.ProveedorHidden = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HIDPROVEEDOR")
                            Busqueda.ProveedorDeBaja = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDORBAJA")

                            Busqueda.Estados = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("ESTADOS")

                            Busqueda.Peticionario = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PETICIONARIO")

                            Busqueda.FechasDe = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("QUEFECHA")
                            If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_FECHA")) Then
                                Busqueda.FechaDesde = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_FECHA")
                            End If
                            If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_FECHA")) Then
                                Busqueda.FechaHasta = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_FECHA")
                            End If
                            upBusquedaAvanzada.Update()
                        End If
                    End If
                    cFiltro = Nothing
                    AlmacenarBusquedaAvanzada()
                Else
                    Dim arrbusquedaTemporal As New ArrayList
                    arrbusquedaTemporal = CType(Session("QA_BusquedaAvanzadaVCert" & Session("QAFiltroUsuarioIdVCert").ToString), ArrayList)

                    Busqueda.Materiales = arrbusquedaTemporal(0)
                    Busqueda.TipoSolicitudes = arrbusquedaTemporal(1)
                    Busqueda.VerDadosBaja = arrbusquedaTemporal(2)

                    Busqueda.MostrarProveedores = arrbusquedaTemporal(3)
                    Busqueda.ProveedorTexto = arrbusquedaTemporal(4)
                    Busqueda.ProveedorHidden = arrbusquedaTemporal(5)
                    Busqueda.ProveedorDeBaja = arrbusquedaTemporal(6)

                    Busqueda.Estados = arrbusquedaTemporal(7)

                    Busqueda.Peticionario = arrbusquedaTemporal(8)

                    Busqueda.FechasDe = arrbusquedaTemporal(9)
                    Busqueda.FechaDesde = arrbusquedaTemporal(10)
                    Busqueda.FechaHasta = arrbusquedaTemporal(11)

                    upBusquedaAvanzada.Update()
                End If
            Else
                If Not Session("QA_BusquedaAvanzadaVCert0") Is Nothing Then
                    Dim arrbusquedaTemporal As New ArrayList
                    arrbusquedaTemporal = CType(Session("QA_BusquedaAvanzadaVCert0"), ArrayList)

                    Busqueda.Materiales = arrbusquedaTemporal(0)
                    Busqueda.TipoSolicitudes = arrbusquedaTemporal(1)
                    Busqueda.VerDadosBaja = arrbusquedaTemporal(2)

                    Busqueda.MostrarProveedores = arrbusquedaTemporal(3)
                    Busqueda.ProveedorTexto = arrbusquedaTemporal(4)
                    Busqueda.ProveedorHidden = arrbusquedaTemporal(5)
                    Busqueda.ProveedorDeBaja = arrbusquedaTemporal(6)

                    Busqueda.Estados = arrbusquedaTemporal(7)

                    Busqueda.Peticionario = arrbusquedaTemporal(8)

                    Busqueda.FechasDe = arrbusquedaTemporal(9)
                    Busqueda.FechaDesde = arrbusquedaTemporal(10)
                    Busqueda.FechaHasta = arrbusquedaTemporal(11)

                    upBusquedaAvanzada.Update()
                Else
                    LimpiarBusquedaAvanzada(True)
                End If
            End If
        Else
            If Session("QAFiltroUsuarioIdVCert") <> "" Then
                If Not Session("QA_BusquedaAvanzadaVCert" & Session("QAFiltroUsuarioIdVCert").ToString) Is Nothing Then
                    Dim arrbusquedaTemporal As New ArrayList
                    arrbusquedaTemporal = CType(Session("QA_BusquedaAvanzadaVCert" & Session("QAFiltroUsuarioIdVCert").ToString), ArrayList)

                    Busqueda.Materiales = arrbusquedaTemporal(0)
                    Busqueda.TipoSolicitudes = arrbusquedaTemporal(1)
                    Busqueda.VerDadosBaja = arrbusquedaTemporal(2)

                    Busqueda.MostrarProveedores = arrbusquedaTemporal(3)
                    Busqueda.ProveedorTexto = arrbusquedaTemporal(4)
                    Busqueda.ProveedorHidden = arrbusquedaTemporal(5)
                    Busqueda.ProveedorDeBaja = arrbusquedaTemporal(6)

                    Busqueda.Estados = arrbusquedaTemporal(7)

                    Busqueda.Peticionario = arrbusquedaTemporal(8)

                    Busqueda.FechasDe = arrbusquedaTemporal(9)
                    Busqueda.FechaDesde = arrbusquedaTemporal(10)
                    Busqueda.FechaHasta = arrbusquedaTemporal(11)

                    upBusquedaAvanzada.Update()
                End If
            Else
                If Not Session("QA_BusquedaAvanzadaVCert0") Is Nothing Then
                    Dim arrbusquedaTemporal As New ArrayList
                    arrbusquedaTemporal = CType(Session("QA_BusquedaAvanzadaVCert0"), ArrayList)

                    Busqueda.Materiales = arrbusquedaTemporal(0)
                    Busqueda.TipoSolicitudes = arrbusquedaTemporal(1)
                    Busqueda.VerDadosBaja = arrbusquedaTemporal(2)

                    Busqueda.MostrarProveedores = arrbusquedaTemporal(3)
                    Busqueda.ProveedorTexto = arrbusquedaTemporal(4)
                    Busqueda.ProveedorHidden = arrbusquedaTemporal(5)
                    Busqueda.ProveedorDeBaja = arrbusquedaTemporal(6)

                    Busqueda.Estados = arrbusquedaTemporal(7)

                    Busqueda.Peticionario = arrbusquedaTemporal(8)

                    Busqueda.FechasDe = arrbusquedaTemporal(9)
                    Busqueda.FechaDesde = arrbusquedaTemporal(10)
                    Busqueda.FechaHasta = arrbusquedaTemporal(11)

                    upBusquedaAvanzada.Update()
                End If
            End If
        End If
        Dim parametrosBusqueda As New ParametrosBusqueda
        With parametrosBusqueda
            .Materiales = Busqueda.Materiales
            .TipoSolicitudes = Busqueda.TipoSolicitudes
            .VerDadosBaja = Busqueda.VerDadosBaja
            .MostrarProveedores = Busqueda.MostrarProveedores
            .ProveedorHidden = Busqueda.ProveedorHidden
            .ProveedorDeBaja = Busqueda.ProveedorDeBaja
            .Estados = Busqueda.Estados
            .Peticionario = Busqueda.Peticionario
            .FechasDe = Busqueda.FechasDe
            .FechaDesde = Busqueda.FechaDesde
            .FechaHasta = Busqueda.FechaHasta
            .Orden = hOrdered.Value
        End With
        Session("parametrosBusqueda") = parametrosBusqueda
    End Sub
    ''' <summary>
    ''' Almacena en una variable de session la configuracion que tiene el usuario en la busqueda avanzada, para 
    ''' que al ir al detalle de la noConformidad y al volver pueda cargar la configuracion que tenia
    ''' </summary>
    ''' <remarks>Llamada desde=uwgNocertificados_ActiveCellChange; Tiempo máximo=0,1seg.</remarks>
    Private Sub AlmacenarBusquedaAvanzada()
        Dim arrbusquedaTemporal As New ArrayList

        arrbusquedaTemporal.Add(Busqueda.Materiales)
        arrbusquedaTemporal.Add(Busqueda.TipoSolicitudes)
        arrbusquedaTemporal.Add(Busqueda.VerDadosBaja)

        arrbusquedaTemporal.Add(Busqueda.MostrarProveedores)
        arrbusquedaTemporal.Add(Busqueda.ProveedorTexto)
        arrbusquedaTemporal.Add(Busqueda.ProveedorHidden)
        arrbusquedaTemporal.Add(Busqueda.ProveedorDeBaja)

        arrbusquedaTemporal.Add(Busqueda.Estados)

        arrbusquedaTemporal.Add(Busqueda.Peticionario)

        arrbusquedaTemporal.Add(Busqueda.FechasDe)
        arrbusquedaTemporal.Add(Busqueda.FechaDesde)
        arrbusquedaTemporal.Add(Busqueda.FechaHasta)

        If Session("QAFiltroUsuarioIdVCert") = "" Then
            Session("QA_BusquedaAvanzadaVCert0") = arrbusquedaTemporal 'Filtro
        Else
            Session("QA_BusquedaAvanzadaVCert" & Session("QAFiltroUsuarioIdVCert").ToString) = arrbusquedaTemporal
        End If
    End Sub
    ''' FunciÃ³n que actualiza los contadores de las pestaÃ±as
    ''' CUANDO SE HACE UNA LECTURA DE AQUI SE HACE UNA LECTURA DE BBDD (NO USA CACHE)
    ''' <param name="IDFiltroUsuario"></param>
    ''' <param name="NombreTablaVCert"></param>
    ''' <returns>numero de certificados q cumplen</returns>
    ''' <remarks>Llamada desde: RecargarCertificados. // Tiempo mÃ¡ximo: 0,9seg (Segun el nÂº de Certificados)</remarks>
    Private Function ObtenerDataCountCertificados_NoAsync(ByVal IDFiltroUsuario As String, ByVal NombreTablaVCert As String) As String
        Dim cFiltro As FiltroQA
        cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroQA))
        cFiltro.TipoSol = TiposDeDatos.TipoDeSolicitud.Certificado

        Dim sMateriales As String = ""
        Dim sTipoSolicitudes As String = ""
        Dim bVerDadosBaja As Boolean = False
        Dim sMostrarProveedores As String = "3"
        Dim sProveedorTexto As String = ""
        Dim sProveedorHidden As String = ""
        Dim bProveedorDeBaja As Boolean = False
        Dim sEstados As String = ""
        Dim sPeticionario As String = ""
        Dim iFechasDe As Byte = 0
        Dim dtFechaDesde As Date
        Dim dtFechaHasta As Date

        If Not (IDFiltroUsuario = "") Then
            If Not Session("QA_BusquedaAvanzadaVCert" & IDFiltroUsuario) Is Nothing Then
                Dim arrbusquedaTemporal As New ArrayList
                arrbusquedaTemporal = CType(Session("QA_BusquedaAvanzadaVCert" & IDFiltroUsuario), ArrayList)

                sMateriales = arrbusquedaTemporal(0)
                sTipoSolicitudes = arrbusquedaTemporal(1)
                bVerDadosBaja = arrbusquedaTemporal(2)

                sMostrarProveedores = arrbusquedaTemporal(3)
                sProveedorHidden = arrbusquedaTemporal(5)
                bProveedorDeBaja = arrbusquedaTemporal(6)

                sEstados = arrbusquedaTemporal(7)

                sPeticionario = arrbusquedaTemporal(8)

                iFechasDe = arrbusquedaTemporal(9)
                dtFechaDesde = arrbusquedaTemporal(10)
                dtFechaHasta = arrbusquedaTemporal(11)
            Else
                cFiltro.IDFiltroUsuario = IDFiltroUsuario

                cFiltro.LoadConfiguracionCamposBusqueda(Idioma)

                If Not cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA") Is Nothing Then
                    If cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows.Count > 0 Then
                        sMateriales = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("MATERIALES")
                        sTipoSolicitudes = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("TIPOSOL")
                        bVerDadosBaja = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("BAJA")
                        sMostrarProveedores = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("QUEMOSTRAR")
                        If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDOR")) Then
                            sProveedorTexto = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDOR")
                        End If
                        sProveedorHidden = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HIDPROVEEDOR")
                        bProveedorDeBaja = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDORBAJA")
                        sEstados = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("ESTADOS")
                        sPeticionario = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PETICIONARIO")
                        iFechasDe = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("QUEFECHA")
                        If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_FECHA")) Then
                            dtFechaDesde = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_FECHA")
                        End If
                        If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_FECHA")) Then
                            dtFechaHasta = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_FECHA")
                        End If
                    End If
                End If
                Dim arrbusquedaTemporal As New ArrayList

                arrbusquedaTemporal.Add(sMateriales)
                arrbusquedaTemporal.Add(sTipoSolicitudes)
                arrbusquedaTemporal.Add(bVerDadosBaja)
                arrbusquedaTemporal.Add(sMostrarProveedores)
                arrbusquedaTemporal.Add(sProveedorTexto)
                arrbusquedaTemporal.Add(sProveedorHidden)
                arrbusquedaTemporal.Add(bProveedorDeBaja)
                arrbusquedaTemporal.Add(sEstados)
                arrbusquedaTemporal.Add(sPeticionario)
                arrbusquedaTemporal.Add(iFechasDe)
                arrbusquedaTemporal.Add(dtFechaDesde)
                arrbusquedaTemporal.Add(dtFechaHasta)

                Session("QA_BusquedaAvanzadaVCert" & IDFiltroUsuario) = arrbusquedaTemporal
            End If
        ElseIf Not Session("QA_BusquedaAvanzadaVCert0") Is Nothing Then
            Dim arrbusquedaTemporal As New ArrayList
            arrbusquedaTemporal = CType(Session("QA_BusquedaAvanzadaVCert0"), ArrayList)

            sMateriales = arrbusquedaTemporal(0)
            sTipoSolicitudes = arrbusquedaTemporal(1)
            bVerDadosBaja = arrbusquedaTemporal(2)

            sMostrarProveedores = arrbusquedaTemporal(3)
            sProveedorHidden = arrbusquedaTemporal(5)
            bProveedorDeBaja = arrbusquedaTemporal(6)

            sEstados = arrbusquedaTemporal(7)

            sPeticionario = arrbusquedaTemporal(8)

            iFechasDe = arrbusquedaTemporal(9)
            dtFechaDesde = arrbusquedaTemporal(10)
            dtFechaHasta = arrbusquedaTemporal(11)
        End If

        Dim Num As Integer = cFiltro.Visor_DevolverNumCertificados(FSNUser.Cod, FSNUser.CodPersona, FSNUser.Idioma,
                FSNUser.UON1, FSNUser.UON2, FSNUser.UON3, FSNUser.Dep,
                FSNUser.QARestCertifUsu, FSNUser.QARestCertifUO, FSNUser.QARestCertifDep,
                FSNUser.QARestProvMaterial, FSNUser.QARestProvEquipo, FSNUser.QARestProvContacto,
                FSNUser.QARestCertifUNQAUsu, FSNUser.QARestCertifUNQA,
                sMateriales, sTipoSolicitudes,
                bVerDadosBaja, sMostrarProveedores, sProveedorHidden, bProveedorDeBaja, sEstados, sPeticionario, iFechasDe,
                dtFechaDesde, dtFechaHasta, NombreTablaVCert)

        cFiltro = Nothing

        Return Num
    End Function
    ''' <summary>
    ''' Evento que salta al darle al boton de "Buscar" del buscador
    ''' Realiza la busqueda de las certificados que coincidan con los criterios de busqueda
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo máximo=0,6 (depende de las certificados)</remarks>
    Private Sub btnBuscarBusquedaAvanzada_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarBusquedaAvanzada.Click
        AlmacenarBusquedaAvanzada()
        Session("QApageNumberVCert") = "0"
        Dim arrParametros() As String = Nothing
        Dim Tab As FSNWebControls.FSNTab
        Dim TextoTab As String
        For index As Integer = 0 To dlFiltrosConfigurados.Items.Count - 1
            Tab = CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab)
            If Tab.Selected Then
                arrParametros = Split(CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).CommandArgument, "#")

                Dim NumeroCertificados As Integer
                If arrParametros(0) = 0 Then
                    NumeroCertificados = ObtenerDataCountCertificados_NoAsync("", "")
                Else
                    NumeroCertificados = ObtenerDataCountCertificados_NoAsync(arrParametros(0), arrParametros(1))
                End If
                Dim lNumCert As Dictionary(Of String, Integer) = CType(Cache("filtroNumCert_" & FSNUser.Cod), Dictionary(Of String, Integer))
                If lNumCert.ContainsKey(arrParametros(0)) Then
                    Dim iPos As Integer = Tab.Text.IndexOf("(")
                    If iPos >= 0 Then Tab.Text = Tab.Text.Substring(0, iPos)
                End If
                lNumCert(arrParametros(0)) = NumeroCertificados
                Tab.Attributes.Add("NumCertificados", NumeroCertificados)
                TextoTab = "(" & NumeroCertificados & ")"
                Tab.Text = AjustarAnchoTextoPixels(Tab.Text, 140, Tab.Font.Name, 12, False, TextoTab)
                Exit For
                End If
        Next
        hCacheLoaded.Value = 0
        CargarCertificados()
        whgCertificados.GridView.Behaviors.Selection.SelectedRows.Clear()
        updFiltros.Update()
        updGridCertificados.Update()
        CargarBusquedaAvanzada(False)
    End Sub
    ''' <summary>
    ''' Limpiar los criterios de busqueda
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo máximo=0</remarks>
    Private Sub btnLimpiarBusquedaAvanzada_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiarBusquedaAvanzada.Click
        LimpiarBusquedaAvanzada()
        AlmacenarBusquedaAvanzada()
    End Sub
#End Region
#Region "Exportación"
    ''' <summary>
    ''' Establece las columnas agrupadas y/o ordenadas y/o filtradas para el grid de exportacion.
    ''' </summary>
    ''' <remarks>Llamada desde: ExportarExcel  ExportarPdf; Tiempo máximo:0,1</remarks>
    Private Sub EstablecerOpcionesUsuario()
        For Each item As Infragistics.Web.UI.GridControls.SortedColumnInfo In whgCertificados.GridView.Behaviors.Sorting.SortedColumns
            With whgCertificadosExportacion
                .GridView.Behaviors.Sorting.SortedColumns.Add(item.ColumnKey.ToString.Replace("BOOL_", ""), item.SortDirection)
            End With
        Next
        For Each item As Infragistics.Web.UI.GridControls.GroupedColumn In whgCertificados.GroupingSettings.GroupedColumns
            If item.ColumnKey.ToString.Contains("BOOL_") Then
                Dim itemBool As New Infragistics.Web.UI.GridControls.GroupedColumn
                itemBool.ColumnKey = item.ColumnKey.ToString.Replace("BOOL_", "")
                itemBool.SortDirection = item.SortDirection

                whgCertificadosExportacion.GroupingSettings.GroupedColumns.Add(itemBool)
            Else
                whgCertificadosExportacion.GroupingSettings.GroupedColumns.Add(item)
            End If
        Next
        For Each item As Infragistics.Web.UI.GridControls.ColumnFilter In whgCertificados.GridView.Behaviors.Filtering.ColumnFilters
            If item.ColumnKey.ToString.Contains("BOOL_") Then
                Dim itemBool As New Infragistics.Web.UI.GridControls.ColumnFilter
                itemBool.ColumnKey = item.ColumnKey.ToString.Replace("BOOL_", "")
                itemBool.Condition = item.Condition

                whgCertificadosExportacion.GridView.Behaviors.Filtering.ColumnFilters.Add(itemBool)
            Else
                whgCertificadosExportacion.GridView.Behaviors.Filtering.ColumnFilters.Add(item)
            End If
        Next
    End Sub
    ''' <summary>
    ''' Exporta a Excel
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
    Private Sub ExportarExcel()
        EstablecerOpcionesUsuario()
        updGridCertificados.Update()
        Dim fileName As String = HttpUtility.UrlEncode(Regex.Replace(FSNPageHeader.TituloCabecera, "[^\w\.@]", "_"))
        fileName = fileName.Replace("+", "%20")

        With whgCertificadosExportacion
            .Columns.FromKey("EN_PROCESO").Hidden = True
            If Not .Columns.FromKey("IMAGE_PROVE") Is Nothing Then .Columns.FromKey("IMAGE_PROVE").Hidden = True
            If Not .Columns.FromKey("IMAGE_ESTADO") Is Nothing Then .Columns.FromKey("IMAGE_ESTADO").Hidden = True
            .GridView.Columns.FromKey("EN_PROCESO").Hidden = True
            If Not .GridView.Columns.FromKey("IMAGE_PROVE") Is Nothing Then .GridView.Columns.FromKey("IMAGE_PROVE").Hidden = True
            If Not .GridView.Columns.FromKey("IMAGE_ESTADO") Is Nothing Then .GridView.Columns.FromKey("IMAGE_ESTADO").Hidden = True
        End With

        With wdgExcelExporter
            .DownloadName = fileName
            .DataExportMode = DataExportMode.AllDataInDataSource
            .EnableStylesExport = False
            .WorkbookFormat = Infragistics.Documents.Excel.WorkbookFormat.Excel2007
            .Export(whgCertificadosExportacion)
        End With
    End Sub
    Private Sub wdgExcelExporter_Exported(sender As Object, e As Infragistics.Web.UI.GridControls.ExcelExportedEventArgs) Handles wdgExcelExporter.Exported
        e.Worksheet.Name = Regex.Replace(Textos(0), "[^\w\.@]", "_")
    End Sub
    ''' <summary>
    ''' Da formato a las columnas fecha. Pone los textos a las columnas booleanas. 
    ''' </summary>
    ''' <param name="sender">grid exportacion</param>
    ''' <param name="e">evento</param>
    ''' <remarks>Llamada desde: Sistema;  Tiempo máximo:0,1</remarks>
    Private Sub wdgExcelExporter_GridRecordItemExported(sender As Object, e As Infragistics.Web.UI.GridControls.GridRecordItemExportedEventArgs) Handles wdgExcelExporter.GridRecordItemExported
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados
        Select Case e.GridCell.Column.Key
            Case "PUBLICADA"
                e.WorksheetCell.Value = IIf(Not IsDBNull(e.GridCell.Value) AndAlso e.GridCell.Value = 1, Textos(13), Textos(14))
            Case "VALIDADO"
                e.WorksheetCell.Value = IIf(Not IsDBNull(e.GridCell.Value) AndAlso e.GridCell.Value = 1, Textos(13), Textos(14))
            Case Else
                Select Case e.GridCell.Column.Type
                    Case System.Type.GetType("System.Boolean")
                        If e.GridCell.Value IsNot System.DBNull.Value Then e.WorksheetCell.Value = IIf(e.GridCell.Value, Textos(13), Textos(14))
                    Case System.Type.GetType("System.DateTime")
                        e.WorksheetCell.CellFormat.FormatString = FSNUser.DateFormat.ShortDatePattern
                    Case Else
                End Select
        End Select
    End Sub
    ''' <summary>
    ''' Exporta a PDF
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
    Private Sub ExportarPDF()
        EstablecerOpcionesUsuario()
        updGridCertificados.Update()
        Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
        fileName = fileName.Replace("+", "%20")
        wdgPDFExporter.DownloadName = fileName

        With whgCertificadosExportacion
            .Columns.FromKey("EN_PROCESO").Hidden = True
            If Not .Columns.FromKey("IMAGE_PROVE") Is Nothing Then .Columns.FromKey("IMAGE_PROVE").Hidden = True
            If Not .Columns.FromKey("IMAGE_ESTADO") Is Nothing Then .Columns.FromKey("IMAGE_ESTADO").Hidden = True
            .GridView.Columns.FromKey("EN_PROCESO").Hidden = True
            If Not .GridView.Columns.FromKey("IMAGE_PROVE") Is Nothing Then .GridView.Columns.FromKey("IMAGE_PROVE").Hidden = True
            If Not .GridView.Columns.FromKey("IMAGE_ESTADO") Is Nothing Then .GridView.Columns.FromKey("IMAGE_ESTADO").Hidden = True
        End With
        With wdgPDFExporter
            .EnableStylesExport = True

            .DataExportMode = DataExportMode.AllDataInDataSource
            .TargetPaperOrientation = PageOrientation.Landscape

            .Margins = PageMargins.GetPageMargins("Narrow")
            .TargetPaperSize = PageSizes.GetPageSize("A4")

            .Export(whgCertificadosExportacion)
        End With
    End Sub
    ''' <summary>
    ''' Da formato a las columnas fecha. Pone los textos a las columnas booleanas.
    ''' </summary>
    ''' <param name="sender">grid exportacion</param>
    ''' <param name="e">evento</param>
    ''' <remarks>Llamada desde: Sistema;  Tiempo máximo:0,1</remarks>
    Private Sub wdgPDFExporter_GridRecordItemExporting(sender As Object, e As Infragistics.Web.UI.GridControls.DocumentGridRecordItemExportingEventArgs) Handles wdgPDFExporter.GridRecordItemExporting
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados
        Select Case e.GridCell.Column.Key
            Case "PUBLICADA"
                e.ExportValue = IIf(Not IsDBNull(e.GridCell.Value) AndAlso e.GridCell.Value = 1, Textos(13), Textos(14))
            Case "VALIDADO"
                e.ExportValue = IIf(Not IsDBNull(e.GridCell.Value) AndAlso e.GridCell.Value = 1, Textos(13), Textos(14))
            Case Else
                Select Case e.GridCell.Column.Type
                    Case System.Type.GetType("System.Boolean")
                        If e.GridCell.Value IsNot System.DBNull.Value Then e.ExportValue = IIf(e.GridCell.Value, Textos(13), Textos(14))
                    Case System.Type.GetType("System.DateTime")
                        If IsDBNull(e.ExportValue) Then
                            e.ExportValue = ""
                        Else
                            e.ExportValue = FormatDate(e.ExportValue, FSNUser.DateFormat)
                        End If
                    Case Else
                End Select
        End Select
    End Sub
#End Region
#Region "Page Methods"
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function SolicitarRenovar(ByVal Renovar As Boolean, ByVal EstablecerTodo As Boolean, _
                ByVal FechaDesPub As Object, ByVal FechaLimCumplim As Object, ByVal Certificados As Object) As String
        Try
            Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oCertificados As FSNServer.Certificados = FSNServer.Get_Object(GetType(FSNServer.Certificados))
            Dim listaCertificados As String = String.Empty
            Dim listaCertificadosEnProceso As String = String.Empty
            Dim dtCertificados As DataTable

            dtCertificados = GenerarDataTableCertificados(Certificados, CType(FechaLimCumplim, DateTime), _
                                     listaCertificadosEnProceso)

            Dim InfoCertificados As String = String.Empty
            Dim oCadena() As String
            Dim sTipoCertificado, sProveedor, sIdCertificado, sInstancia As String

            oCertificados.Solicitar_Renovar(dtCertificados, Renovar, User.CodPersona, User.Email, _
                                            CType(FechaDesPub, DateTime), CType(FechaLimCumplim, DateTime), _
                                            InfoCertificados, CType(EstablecerTodo, Boolean))

            oCadena = Split(InfoCertificados, ",")
            For i = 0 To UBound(oCadena)
                'Nuevos Certificados
                If oCadena(i) <> "" Then
                    sTipoCertificado = Split(oCadena(i), "&")(0)
                    sProveedor = Split(oCadena(i), "&")(1)
                    sIdCertificado = Split(oCadena(i), "&")(2)
                    sInstancia = Split(oCadena(i), "&")(3)
                    For j = 0 To dtCertificados.Rows.Count - 1
                        If ((sTipoCertificado = dtCertificados.Rows(j).Item("TIPO_CERTIF")) And (sProveedor = dtCertificados.Rows(j).Item("PROVE"))) Then
                            If listaCertificados <> "" Then listaCertificados &= ","
                            listaCertificados &= sIdCertificado
                        End If
                    Next
                End If
            Next

            Dim sVolver As String = ""
            Dim sURL As String = ""
            Dim sFiltroUsuarioIdAnt As String = ""
            sFiltroUsuarioIdAnt = IIf(HttpContext.Current.Session("QAFiltroUsuarioIdVCert") = "", 0, HttpContext.Current.Session("QAFiltroUsuarioIdVCert"))
            Dim sFiltroIdAnt As String = ""
            sFiltroIdAnt = IIf(HttpContext.Current.Session("QAFiltroIdVCert") = "", 0, HttpContext.Current.Session("QAFiltroIdVCert"))
            'En /script/frames.aspx se cambian los * por & para los request de la pagina, y los ** por *
            sVolver = ConfigurationManager.AppSettings("rutaQA2008") + "Certificados/Certificados.aspx?FiltroUsuarioIdAnt=" & sFiltroUsuarioIdAnt & "**NombreTablaAnt=" & HttpContext.Current.Session("QANombreTablaVCert") & "**FiltroIdAnt=" & sFiltroIdAnt
            Dim sAccion As String = IIf(Renovar, "renovarCertificados", "solicitarCertificados")
            Return "Certificados=" & listaCertificados & "&Accion=" & sAccion & "&EnProceso=" & listaCertificadosEnProceso & "&volver=" & sVolver
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function Enviar() As String
        Dim sVolver As String = ""
        Dim sURL As String = ""
        Dim sFiltroUsuarioIdAnt As String = ""
        sFiltroUsuarioIdAnt = IIf(HttpContext.Current.Session("QAFiltroUsuarioIdVCert") = "", 0, HttpContext.Current.Session("QAFiltroUsuarioIdVCert"))
        Dim sFiltroIdAnt As String = ""
        sFiltroIdAnt = IIf(HttpContext.Current.Session("QAFiltroIdVCert") = "", 0, HttpContext.Current.Session("QAFiltroIdVCert"))
        'En /script/frames.aspx se cambian los * por & para los request de la pagina, y los ** por *
        sVolver = ConfigurationManager.AppSettings("rutaQA2008") + "Certificados/Certificados.aspx?FiltroUsuarioIdAnt=" & sFiltroUsuarioIdAnt & "**NombreTablaAnt=" & HttpContext.Current.Session("QANombreTablaVCert") & "**FiltroIdAnt=" & sFiltroIdAnt

        Return ConfigurationManager.AppSettings("rutaQA") & "certificados/altacertificado.aspx?TipoSolicit=#####&Proveedores=####" & "&volver=" & sVolver
    End Function
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Sub CargarCache()
        Dim Busqueda As ParametrosBusqueda = CType(HttpContext.Current.Session("ParametrosBusqueda"), ParametrosBusqueda)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim cFiltro As FiltroQA
        cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroQA))
        cFiltro.TipoSol = TiposDeDatos.TipoDeSolicitud.Certificado
        Dim _pageSize As Integer = System.Configuration.ConfigurationManager.AppSettings("PageSize")
        cFiltro.Visor_DevolverCertificados(FSNUser.Cod, FSNUser.CodPersona, FSNUser.Idioma,
            FSNUser.UON1, FSNUser.UON2, FSNUser.UON3, FSNUser.Dep,
            FSNUser.QARestCertifUsu, FSNUser.QARestCertifUO,
            FSNUser.QARestCertifDep, FSNUser.QARestProvMaterial, FSNUser.QARestProvEquipo, FSNUser.QARestProvContacto,
            FSNUser.QARestCertifUNQAUsu, FSNUser.QARestCertifUNQA,
            Busqueda.Materiales, Busqueda.TipoSolicitudes, Busqueda.VerDadosBaja,
            Busqueda.MostrarProveedores, Busqueda.ProveedorHidden, Busqueda.ProveedorDeBaja, Busqueda.Estados, Busqueda.Peticionario,
            Busqueda.FechasDe, Busqueda.FechaDesde, Busqueda.FechaHasta, HttpContext.Current.Session("QANombreTablaVCert"), Busqueda.Orden,
            0, 0, True, True)

        Dim PKCol(0) As DataColumn
        PKCol(0) = cFiltro.CertificadosVisor.Tables(0).Columns("DATAKEY")
        cFiltro.CertificadosVisor.Tables(0).PrimaryKey = PKCol

        Dim parentCols(1) As DataColumn
        Dim childCols(1) As DataColumn

        cFiltro.CertificadosVisor.Tables(0).TableName = "CERTIFICADOS"
        cFiltro.CertificadosVisor.Tables(1).TableName = "PETICIONARIO"
        cFiltro.CertificadosVisor.Tables(2).TableName = "MATERIAL"

        parentCols(0) = cFiltro.CertificadosVisor.Tables("CERTIFICADOS").Columns("PROVE_COD")
        parentCols(1) = cFiltro.CertificadosVisor.Tables("CERTIFICADOS").Columns("TIPO_CERTIFICADO")

        childCols(0) = cFiltro.CertificadosVisor.Tables("MATERIAL").Columns("PROVE_COD")
        childCols(1) = cFiltro.CertificadosVisor.Tables("MATERIAL").Columns("TIPO_CERTIFICADO")

        cFiltro.CertificadosVisor.Relations.Add("REL_CERTIFICADO_MATERIAL", parentCols, childCols, False)

        Dim sDenMaterial As String = ""
        Dim sIdMaterial As String = ""

        For Each oRow0 As DataRow In cFiltro.CertificadosVisor.Tables("CERTIFICADOS").Rows
            sDenMaterial = ""
            sIdMaterial = ""
            For Each oRow1 As DataRow In oRow0.GetChildRows("REL_CERTIFICADO_MATERIAL")
                sDenMaterial = sDenMaterial & IIf(sDenMaterial = "", "", ",") & oRow1("DEN_MATERIAL")
                sIdMaterial = sIdMaterial & IIf(sIdMaterial = "", "", ",") & oRow1("ID_MATERIAL")
            Next
            oRow0("DEN_MATERIAL") = sDenMaterial
            oRow0("ID_MATERIAL") = sIdMaterial
        Next

        Dim parentColsNo(0) As DataColumn
        Dim childColsNo(0) As DataColumn
        parentColsNo(0) = cFiltro.CertificadosVisor.Tables("CERTIFICADOS").Columns("TIPO_CERTIFICADO")
        childColsNo(0) = cFiltro.CertificadosVisor.Tables("PETICIONARIO").Columns("TIPO_CERTIFICADO")
        cFiltro.CertificadosVisor.Relations.Add("REL_CERTIFICADO_NOPETICIONARIO", parentColsNo, childColsNo, False)
        For Each oRow0 As DataRow In cFiltro.CertificadosVisor.Tables("CERTIFICADOS").Rows
            For Each oRow1 As DataRow In oRow0.GetChildRows("REL_CERTIFICADO_NOPETICIONARIO")
                oRow0("NOPETIC") = oRow1("NOPETIC")
            Next
        Next

        Dim pag As New FSNPage
        pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados

        cFiltro.CertificadosVisor.Relations.Clear()
        HttpContext.Current.Cache.Insert("dsCertificados_" & FSNUser.Cod, cFiltro.CertificadosVisor, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0),
                                         CacheItemPriority.BelowNormal, Nothing)
        HttpContext.Current.Cache.Insert("dtProve_MaterialesQA_" & FSNUser.Cod, cFiltro.CertificadosVisor.Tables(2), Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
    End Sub
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ObtenerDataCountCertificados(ByVal TextoTab As String, ByVal FontFamily As String, ByVal IDFiltroUsuario As String, ByVal NombreTablaVCert As String) As String
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim cFiltro As FiltroQA
        cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroQA))
        cFiltro.TipoSol = TiposDeDatos.TipoDeSolicitud.Certificado

        Dim sMateriales As String = ""
        Dim sTipoSolicitudes As String = ""
        Dim bVerDadosBaja As Boolean = False
        Dim sMostrarProveedores As String = "3"
        Dim sProveedorTexto As String = ""
        Dim sProveedorHidden As String = ""
        Dim bProveedorDeBaja As Boolean = False
        Dim sEstados As String = ""
        Dim sPeticionario As String = ""
        Dim iFechasDe As Byte = 0
        Dim dtFechaDesde As Date
        Dim dtFechaHasta As Date

        Dim filtroNumCert As Dictionary(Of String, Integer)
        Dim nombreCache As String = "filtroNumCert_" & FSNUser.Cod
        If HttpContext.Current.Cache(nombreCache) Is Nothing Then
            filtroNumCert = New Dictionary(Of String, Integer)
            HttpContext.Current.Cache.Insert(nombreCache, filtroNumCert, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0), CacheItemPriority.Normal, Nothing)
        Else
            filtroNumCert = CType(HttpContext.Current.Cache(nombreCache), Dictionary(Of String, Integer))
        End If

        Dim Num As Integer
        If filtroNumCert.ContainsKey(If(String.IsNullOrEmpty(IDFiltroUsuario), "0", IDFiltroUsuario)) Then
            Num = -1
        Else
            If Not (IDFiltroUsuario = "") Then
                If Not HttpContext.Current.Session("QA_BusquedaAvanzadaVCert" & IDFiltroUsuario) Is Nothing Then
                    Dim arrbusquedaTemporal As New ArrayList
                    arrbusquedaTemporal = CType(HttpContext.Current.Session("QA_BusquedaAvanzadaVCert" & IDFiltroUsuario), ArrayList)

                    sMateriales = arrbusquedaTemporal(0)
                    sTipoSolicitudes = arrbusquedaTemporal(1)
                    bVerDadosBaja = arrbusquedaTemporal(2)

                    sMostrarProveedores = arrbusquedaTemporal(3)
                    sProveedorHidden = arrbusquedaTemporal(5)
                    bProveedorDeBaja = arrbusquedaTemporal(6)

                    sEstados = arrbusquedaTemporal(7)

                    sPeticionario = arrbusquedaTemporal(8)

                    iFechasDe = arrbusquedaTemporal(9)
                    dtFechaDesde = arrbusquedaTemporal(10)
                    dtFechaHasta = arrbusquedaTemporal(11)
                Else
                    cFiltro.IDFiltroUsuario = IDFiltroUsuario

                    cFiltro.LoadConfiguracionCamposBusqueda(FSNUser.Idioma.ToString)

                    If Not cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA") Is Nothing Then
                        If cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows.Count > 0 Then
                            sMateriales = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("MATERIALES")
                            sTipoSolicitudes = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("TIPOSOL")
                            bVerDadosBaja = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("BAJA")
                            sMostrarProveedores = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("QUEMOSTRAR")
                            If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDOR")) Then
                                sProveedorTexto = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDOR")
                            End If
                            sProveedorHidden = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HIDPROVEEDOR")
                            bProveedorDeBaja = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDORBAJA")
                            sEstados = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("ESTADOS")
                            sPeticionario = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PETICIONARIO")
                            iFechasDe = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("QUEFECHA")
                            If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_FECHA")) Then
                                dtFechaDesde = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_FECHA")
                            End If
                            If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_FECHA")) Then
                                dtFechaHasta = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_FECHA")
                            End If
                        End If
                    End If
                    Dim arrbusquedaTemporal As New ArrayList

                    arrbusquedaTemporal.Add(sMateriales)
                    arrbusquedaTemporal.Add(sTipoSolicitudes)
                    arrbusquedaTemporal.Add(bVerDadosBaja)
                    arrbusquedaTemporal.Add(sMostrarProveedores)
                    arrbusquedaTemporal.Add(sProveedorTexto)
                    arrbusquedaTemporal.Add(sProveedorHidden)
                    arrbusquedaTemporal.Add(bProveedorDeBaja)
                    arrbusquedaTemporal.Add(sEstados)
                    arrbusquedaTemporal.Add(sPeticionario)
                    arrbusquedaTemporal.Add(iFechasDe)
                    arrbusquedaTemporal.Add(dtFechaDesde)
                    arrbusquedaTemporal.Add(dtFechaHasta)

                    HttpContext.Current.Session("QA_BusquedaAvanzadaVCert" & IDFiltroUsuario) = arrbusquedaTemporal
                End If
            ElseIf Not HttpContext.Current.Session("QA_BusquedaAvanzadaVCert0") Is Nothing Then
                Dim arrbusquedaTemporal As New ArrayList
                arrbusquedaTemporal = CType(HttpContext.Current.Session("QA_BusquedaAvanzadaVCert0"), ArrayList)

                sMateriales = arrbusquedaTemporal(0)
                sTipoSolicitudes = arrbusquedaTemporal(1)
                bVerDadosBaja = arrbusquedaTemporal(2)

                sMostrarProveedores = arrbusquedaTemporal(3)
                sProveedorHidden = arrbusquedaTemporal(5)
                bProveedorDeBaja = arrbusquedaTemporal(6)

                sEstados = arrbusquedaTemporal(7)

                sPeticionario = arrbusquedaTemporal(8)

                iFechasDe = arrbusquedaTemporal(9)
                dtFechaDesde = arrbusquedaTemporal(10)
                dtFechaHasta = arrbusquedaTemporal(11)
            End If

            Num = cFiltro.Visor_DevolverNumCertificados(FSNUser.Cod, FSNUser.CodPersona, FSNUser.Idioma,
                FSNUser.UON1, FSNUser.UON2, FSNUser.UON3, FSNUser.Dep,
                FSNUser.QARestCertifUsu, FSNUser.QARestCertifUO, FSNUser.QARestCertifDep,
                FSNUser.QARestProvMaterial, FSNUser.QARestProvEquipo, FSNUser.QARestProvContacto,
                FSNUser.QARestCertifUNQAUsu, FSNUser.QARestCertifUNQA,
                sMateriales, sTipoSolicitudes,
                bVerDadosBaja, sMostrarProveedores, sProveedorHidden, bProveedorDeBaja, sEstados, sPeticionario, iFechasDe,
                dtFechaDesde, dtFechaHasta, NombreTablaVCert)

            filtroNumCert(strToInt(IDFiltroUsuario)) = Num
            HttpContext.Current.Cache(nombreCache) = filtroNumCert
        End If

        cFiltro = Nothing

        Dim serializer As New JavaScriptSerializer
        Return serializer.Serialize(New With {.numero = Num, .texto = AjustarAnchoTextoPixels(TextoTab, 140, FontFamily, 12, False, " (" & Num & ")")})
    End Function
#End Region
#Region "Funciones Utiles"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Certificados"></param>
    ''' <param name="FechaLimiteCumplimentacion"></param>
    ''' <param name="listaCertificadosEnProceso"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function GenerarDataTableCertificados(ByVal Certificados As Object, _
            ByVal FechaLimiteCumplimentacion As Nullable(Of DateTime), _
            ByRef listaCertificadosEnProceso As String) As DataTable
        Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim dtCertificados As New DataTable
        dtCertificados.Columns.Add("TIPO_CERTIF", System.Type.GetType("System.Int32"))
        dtCertificados.Columns.Add("PROVE", System.Type.GetType("System.String"))
        dtCertificados.Columns.Add("INSTANCIA", System.Type.GetType("System.Int64"))
        dtCertificados.Columns.Add("IDCERTIFICADO", System.Type.GetType("System.Int64"))
        dtCertificados.Columns.Add("ENVIARMAIL", System.Type.GetType("System.Int32"))

        Dim drCertificado As DataRow
        For Each certificado As Object In Certificados
            If Not CType(certificado("EnProceso"), Boolean) Then
                drCertificado = dtCertificados.NewRow

                With drCertificado
                    .Item("TIPO_CERTIF") = Split(certificado("DataKey"), "#")(1) 'field("TIPO_CERTIFICADO")
                    .Item("PROVE") = Split(certificado("DataKey"), "#")(0) 'field("PROVE_COD")
                    .Item("INSTANCIA") = 0
                    .Item("IDCERTIFICADO") = 0
                    .Item("ENVIARMAIL") = 1
                End With
                dtCertificados.Rows.Add(drCertificado)
            Else
                If listaCertificadosEnProceso <> "" Then listaCertificadosEnProceso = listaCertificadosEnProceso & ";"
                listaCertificadosEnProceso &= Split(certificado("DataKey"), "#")(0) 'field("PROVE_COD")--
                listaCertificadosEnProceso &= "_" & certificado("ProveDen") 'field("PROVE_DEN")--
                listaCertificadosEnProceso &= "_" & certificado("DenTipoCertificado") 'field("DEN_TIPO_CERTIFICADO")--
            End If
        Next

        Return dtCertificados
    End Function

    ''Codigo para guardar datos del FSAL, funcion llamada desde el evento 'Unload' del Grid de Solicitudes.
    ''
    ''' <summary>
    ''' Procedimiento incoporporado para realizar la llamada de forma asíncrona a través de un Thread
    ''' </summary>
    ''' <param name="sArgs">Array con los argumentos de llamada a FSNServer.FSAL</param>
    ''' <remarks></remarks>
    Private Sub CallFSALRegistrarActualizarAcceso(ByVal sArgs As Object)
        Dim m_FSNServer As New FSNServer.FSAL

        If DirectCast(sArgs, Array).GetUpperBound(0) = 12 Then
            m_FSNServer.RegistrarAcceso(sArgs(0), sArgs(1), sArgs(2), sArgs(3), sArgs(4), sArgs(5), sArgs(6), sArgs(7), sArgs(8), sArgs(9), sArgs(10), sArgs(11), sArgs(12))
        ElseIf DirectCast(sArgs, Array).GetUpperBound(0) = 5 Then
            m_FSNServer.ActualizarAcceso(sArgs(0), sArgs(1), sArgs(2), sArgs(3), sArgs(4), sArgs(5))
        End If

    End Sub
    ''
    ''Fin codigo FSAL
#End Region
    Private Class ParametrosBusqueda
        Private _materiales As String
        Public Property Materiales() As String
            Get
                Return _materiales
            End Get
            Set(ByVal value As String)
                _materiales = value
            End Set
        End Property
        Private _tipoSolicitudes As String
        Public Property TipoSolicitudes() As String
            Get
                Return _tipoSolicitudes
            End Get
            Set(ByVal value As String)
                _tipoSolicitudes = value
            End Set
        End Property
        Private _verDadosBaja As Boolean
        Public Property VerDadosBaja() As Boolean
            Get
                Return _verDadosBaja
            End Get
            Set(ByVal value As Boolean)
                _verDadosBaja = value
            End Set
        End Property
        Private _mostrarProveedores As String
        Public Property MostrarProveedores() As String
            Get
                Return _mostrarProveedores
            End Get
            Set(ByVal value As String)
                _mostrarProveedores = value
            End Set
        End Property
        Private _proveedorHidden As String
        Public Property ProveedorHidden() As String
            Get
                Return _proveedorHidden
            End Get
            Set(ByVal value As String)
                _proveedorHidden = value
            End Set
        End Property
        Private _proveedorDeBaja As Boolean
        Public Property ProveedorDeBaja() As Boolean
            Get
                Return _proveedorDeBaja
            End Get
            Set(ByVal value As Boolean)
                _proveedorDeBaja = value
            End Set
        End Property
        Private _estados As String
        Public Property Estados() As String
            Get
                Return _estados
            End Get
            Set(ByVal value As String)
                _estados = value
            End Set
        End Property
        Private _peticionario As String
        Public Property Peticionario() As String
            Get
                Return _peticionario
            End Get
            Set(ByVal value As String)
                _peticionario = value
            End Set
        End Property
        Private _fechasDe As Byte
        Public Property FechasDe() As Byte
            Get
                Return _fechasDe
            End Get
            Set(ByVal value As Byte)
                _fechasDe = value
            End Set
        End Property
        Private _fechaDesde As Date
        Public Property FechaDesde() As Date
            Get
                Return _fechaDesde
            End Get
            Set(ByVal value As Date)
                _fechaDesde = value
            End Set
        End Property
        Private _fechaHasta As Date
        Public Property FechaHasta() As Date
            Get
                Return _fechaHasta
            End Get
            Set(ByVal value As Date)
                _fechaHasta = value
            End Set
        End Property
        Private _orden As String
        Public Property Orden() As String
            Get
                Return _orden
            End Get
            Set(ByVal value As String)
                _orden = value
            End Set
        End Property
    End Class
End Class