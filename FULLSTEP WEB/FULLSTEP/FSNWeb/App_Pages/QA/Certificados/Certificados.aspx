<%@ Page Language="vb" Debug="true" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.Master" CodeBehind="Certificados.aspx.vb" Inherits="Fullstep.FSNWeb.Certificados" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
	<script type="text/javascript">
		var SelectedRows=[];
        var SelectedRowCount = 0;
        var LoadFilter = false;
		$(document).ready(function () {            
			$('#imgExpandir').attr('src',rutaTheme + '/images/expandir.gif');
			$('#imgExpandir').show();
			$('[id$=btnAnyadirFiltros]').live('click',function(){
				window.location.href= rutaFS + 'QA/FiltrosQA/ConfigurarFiltrosQA.aspx?Donde=VisorCert';
			});
			$('#chkEstablecerTodo').click(function(){
				var dpFechaLimiteCumplimentacion = $find("<%=dpFechaLimCumplim.ClientID%>");
				if(this.checked){
					dpFechaLimiteCumplimentacion.set_enabled(false);
				}else{
					dpFechaLimiteCumplimentacion.set_enabled(true);
				}
			});
			$('[id$=ImgBtnCerrarSolicitarRenovar]').click(function(){				
				$('#popupFondo').hide();
				$('#pnlSolicitarRenovar').hide();
			});
			$('#pnlBusqueda').live('click',function(){
				if($('#pnlParametros').is(':visible')){
					$('#imgExpandir').attr('src',rutaTheme + '/images/expandir.gif');
					$('#pnlParametros').hide();                    
				}else{
					$('#imgExpandir').attr('src',rutaTheme + '/images/contraer.gif');
					$('#pnlParametros').show();
				}
			});
			$('[id$=ImgBtnFirst]').live('click',function(){ FirstPage();});
			$('[id$=ImgBtnPrev]').live('click',function(){ PrevPage();});
			$('[id$=ImgBtnNext]').live('click',function(){ NextPage();});
			$('[id$=ImgBtnLast]').live('click',function(){ LastPage();});
			$('[id$=btnBuscarBusquedaAvanzada]').live('click',function(){
				$('[id$=hFiltered]').val(0);
				SelectedRows=[];
				SelectedRowCount=0;
			});
			$('[id$=fsnTabFiltro]').live('click',function(){
				$('[id$=hFiltered]').val(0);
			});    
			
			var idTab=0;
			var nomTabla,fontFamily;
			$.each($('[id$=fsnTabFiltro]'),function(){
			    var tab=$(this);
			    textoTab=tab.text();
			    fontFamily=tab.css('font-family');			    
			    idTab=(tab.attr('idTab')==0?'':tab.attr('idTab'));
			    nomTabla=tab.attr('nomTabla');
			    $.when($.ajax({
			        type: "POST",
			        url: rutaFS + 'QA/Certificados/Certificados.aspx/ObtenerDataCountCertificados',
			        contentType: "application/json; charset=utf-8",
			        data: JSON.stringify({TextoTab:textoTab,FontFamily:fontFamily,IDFiltroUsuario:idTab,NombreTablaVCert:nomTabla}),
			        dataType: "json",
			        async: true
			    })).done(function (msg) {
			        var resultado = $.parseJSON(msg.d);
			        if (resultado.numero!==-1){
			            tab.attr('NumCertificados',resultado.numero);
			            tab.text(resultado.texto);
			        };
			    });  
			});
		});		
		function ExportarExcel(){
            if (parseInt($('[id$=hCacheLoaded]').val())==0){
                var textoCargando = $('[id$=LblProcesando]').text();
				$('[id$=LblProcesando]').text(TextosPopUpCertificados[10]);
                $('.modalBackground').css('z-index',100101);
                $('[id$=panelUpdateProgress]').css('z-index',100102);
                $('[id$=panelUpdateProgress] div').css('top','10%');
				MostrarCargando();
				$.ajax({       
					type: 'POST',
					url: rutaFS + 'QA/Certificados/Certificados.aspx/CargarCache',
					contentType: 'application/json; charset=utf-8',
					dataType: 'json',
					async: true,
					success:function(){
						$('[id$=hCacheLoaded]').val(1);
						OcultarCargando();
                        $('[id$=LblProcesando]').text(textoCargando);	
                        $('[id$=panelUpdateProgress] div').css('top','30%');
                        __doPostBack('Excel','');
					}
				});
            }else{
			    __doPostBack('Excel','');
            };
		};
		function ExportarPDF(){
            if (parseInt($('[id$=hCacheLoaded]').val())==0){
                var textoCargando = $('[id$=LblProcesando]').text();
				$('[id$=LblProcesando]').text(TextosPopUpCertificados[10]);
                $('.modalBackground').css('z-index',100101);
                $('[id$=panelUpdateProgress]').css('z-index',100102);
                $('[id$=panelUpdateProgress] div').css('top','10%');
				MostrarCargando();
				$.ajax({       
					type: 'POST',
					url: rutaFS + 'QA/Certificados/Certificados.aspx/CargarCache',
					contentType: 'application/json; charset=utf-8',
					dataType: 'json',
					async: true,
					success:function(){
						$('[id$=hCacheLoaded]').val(1);
						OcultarCargando();
                        $('[id$=LblProcesando]').text(textoCargando);	
                        $('[id$=panelUpdateProgress] div').css('top','30%');
                        __doPostBack('PDF','');
					}
				});
            }else{
			    __doPostBack('PDF','');
            };			
		};
		function OcultarAyuda() {
			/*''' <summary>
			''' Ocultar la ayuda Fecha Limite Cumplimentacion � la ayuda Fecha Despublicacion
			''' </summary>
			''' <remarks>Llamada desde: Boton ayuda Fecha Despublicacion</remarks>*/
			if (event.srcElement.id != "<%=imgAyudaLimiteCumplimentacion.ClientID%>") {
				$("#sAyudaLimiteCumplimentacion").hide();
				$("#iAyudaLimiteCumplimentacion").hide();
			} else { MostrarAyudaLimiteCumplimentacion() }

			if (event.srcElement.id != "<%=imgAyudaDespublicacion.ClientID%>") {
				$("#sAyudaDespublicacion").hide();
				$("#iAyudaDespublicacion").hide();
			} else { MostrarAyudaDespublicacion() }

			if(event.srcElement.id == 'chkEstablecerTodo') return true;

			return false;
		};
		function MostrarAyudaLimiteCumplimentacion() {
			/*''' <summary>
			''' Mostrar ayuda Fecha Limite Cumplimentacion
			''' </summary>
			''' <remarks>Llamada desde; Boton ayuda Fecha Limite Cumplimentacion</remarks>*/
			$('#divAyuda').css('top', $('[id$=imgAyudaLimiteCumplimentacion]').position().top);
			$('#divAyuda').css('left', $('[id$=imgAyudaLimiteCumplimentacion]').position().left + 25);

			$("#sAyudaLimiteCumplimentacion").show();
			$("#iAyudaLimiteCumplimentacion").show();
		};
		function MostrarAyudaDespublicacion() {
			$('#divAyuda').css('top', $('[id$=imgAyudaLimiteCumplimentacion]').position().top);
			$('#divAyuda').css('left', $('[id$=imgAyudaLimiteCumplimentacion]').position().left + 25);

			$("#sAyudaDespublicacion").show();
			$("#iAyudaDespublicacion").css('top', $('[id$=imgAyudaDespublicacion]').position().top - $('[id$=imgAyudaLimiteCumplimentacion]').position().top);
			$("#iAyudaDespublicacion").show();
		};
		function whgCertificados_CellClick(sender, e) {
		    /*''' <summary>
		    ''' Se mira si se ha pulsado sobre una celda q muestra popup o va al detalle de certificados
		    ''' </summary>
		    ''' <param name="sender">Origen del evento</param>
		    ''' <param name="e">evento</param>        
		    ''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>*/         
			if (e.get_type() == "cell" && e.get_item().get_row().get_index()!==-1) {
				if (e.get_item().get_grid().get_behaviors().get_activation().get_activeCellResolved() !== null) {
					var cell = e.get_item().get_grid().get_behaviors().get_activation().get_activeCellResolved();

					posicionX=$(cell.get_element()).position().left + ($(cell.get_element()).innerWidth() / 4);
					posicionY=$(cell.get_element()).position().top + 20;

					switch (cell.get_column().get_key()) {
						case "EN_PROCESO":
							e.set_cancel(true)
							break;
						case "IMAGE_PROVE":
						case "PROVE_COD":
						case "PROVE_DEN":
							FSNMostrarPanel(ProveAnimationClientID, e, ProveDynamicPopulateClienteID, cell.get_row().get_cellByColumnKey("PROVE_COD").get_value(),null,null,posicionX,posicionY);                            
							break;
						case "NOMBREPETICIONARIO":
							if (cell.get_row().get_cellByColumnKey("PER").get_value() != ""){
								FSNMostrarPanel(PeticionarioAnimationClientID, e, PeticionarioDynamicPopulateClienteID, cell.get_row().get_cellByColumnKey("PER").get_value(),null,null,posicionX,posicionY);
							}
							break;
						default:
						    var estado = cell.get_row().get_cellByColumnKey('ESTADO').get_value();
						    var num_version = cell.get_row().get_cellByColumnKey('NUM_VERSION').get_value();
						    if(estado!=0 && estado!=1 && (estado!=2 || num_version!=0)){
							    //Redirigir a detalleCertificado si no esta en proceso
							    IdFiltroUsu=document.getElementById("<%=QAFiltroUsuarioIdVCert.ClientID%>").value;
							    IdFiltro=document.getElementById("<%=QAFiltroIdVCert.ClientID%>").value;
						        TablaNombre=document.getElementById("<%=QANombreTablaVCert.ClientID%>").value;
							    $.when($.ajax({       
								    type: 'POST',
								    url: rutaFS + '_Common/App_services/Consultas.asmx/ComprobarEnProceso',
								    data: JSON.stringify({ contextKey: cell.get_row().get_cellByColumnKey('INSTANCIA').get_value() }),
								    contentType: 'application/json; charset=utf-8',
								    dataType: 'json',
								    async: true
							    })).done(function (msg) {
								    if (msg.d=="1"){                    
									    FSNMostrarPanel(ProcesoAnimationClientID,event,ProcesoDynamicPopulateClienteID,2,null,null,posicionX,posicionY);						
								    }else{
									    TempCargando();
									    var FechaLocal
									    FechaLocal = new Date()
									    svolver="*volver=<%=ConfigurationManager.AppSettings("rutaQA2008")%>Certificados/Certificados.aspx?FiltroUsuarioIdAnt=" + IdFiltroUsu + "**NombreTablaAnt=" + TablaNombre + "**FiltroIdAnt=" + IdFiltro  					   
									    sref="<%=ConfigurationManager.AppSettings("rutaQA")%>frames.aspx?pagina=certificados/detalleCertificado.aspx?Certificado=" + cell.get_row().get_cellByColumnKey('ID_CERTIF').get_value() + "*otz=" + FechaLocal.getTimezoneOffset() 
									    window.open(sref + svolver,"_self");	
								    };
							    });   
						    }        
						break;                                                                                                       
					}
				}
			}
		};
		function whgCertificados_RowSelected(sender,e){            
		    /*''' <summary>
		    ''' Mira si la fila es seleccionable, de no serlo, la deselecciona. Despues mantiene la colecci�n q se us apara solicitar / renovar /enviar.
		    ''' </summary>
		    ''' <param name="sender">Origen del evento</param>
		    ''' <param name="e">evento</param>        
		    ''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>*/
			var Add,Delete;
			if(sender.get_behaviors().get_selection().get_selectedRowsResolved().length<=SelectedRowCount){
				var RowDelete;
				for(i=SelectedRows.length-1;i>=0;i--){				
					RowDelete=SelectedRows[i];
					Delete=true;
					$.each(sender.get_behaviors().get_selection().get_selectedRowsResolved(),function(){
						if(RowDelete.DataKey==this.get_idPair().key[0]){
							Delete=false;
							return false;
						}
					});
					if(Delete) SelectedRows.splice(i,1);
				}
			};			
			var PosiblesMensajeNoPetic=0;
			var NumMensajeNoPetic=0;
			if (sender.get_behaviors().get_selection().get_selectedRows().get_length()>0)
				PosiblesMensajeNoPetic=sender.get_behaviors().get_selection().get_selectedRows().get_length();

			for (i = 0; i < sender.get_behaviors().get_selection().get_selectedRows().get_length(); i++) {
				var row=sender.get_behaviors().get_selection().get_selectedRows().getItem(i);
				var nopetic=row.get_cellByColumnKey('NOPETIC').get_value();
				if (nopetic==1) {
					sender.get_behaviors().get_selection().get_selectedRows().remove(sender.get_behaviors().get_selection().get_selectedRows().getItem(i));
					i = i - 1;     
					
					NumMensajeNoPetic=1;
				}
			};
			if ((PosiblesMensajeNoPetic > 0) && (NumMensajeNoPetic > 0)){
				if (PosiblesMensajeNoPetic==1){
					alert(TextosPopUpCertificados[7]);
				} else {
					if (sender.get_behaviors().get_selection().get_selectedRows().get_length()>0){
						if (!confirm(TextosPopUpCertificados[8])){
							for (i = 0; i < sender.get_behaviors().get_selection().get_selectedRows().get_length(); i++) {
								sender.get_behaviors().get_selection().get_selectedRows().remove(sender.get_behaviors().get_selection().get_selectedRows().getItem(i));
								i = i - 1;                                
							}
						}
					} else {
						alert(TextosPopUpCertificados[9]);
					}
				}
			};
			var key,Plazo_Cumplimentacion,En_Proceso,Prove_Den,Id_Certif,Den_Tipo_Certif,Con_Email,Obligatorio;
			for(j=0;j<sender.get_behaviors().get_selection().get_selectedRows().get_length();j++){
				Add=true;                
				$.each(SelectedRows,function(){
					if(this.DataKey==sender.get_behaviors().get_selection().get_selectedRows().getItemID(j).key[0]){
						Add=false;
						return false;
					}
				});
				if(Add) {
					var row=sender.get_behaviors().get_selection().get_selectedRows().getItem(j);
					key=row.get_cellByColumnKey('DATAKEY').get_value();
					Plazo_Cumplimentacion=row.get_cellByColumnKey('PLAZO_CUMPL').get_value();
					En_Proceso=row.get_cellByColumnKey('EN_PROCESO').get_value();
					Prove_Den=row.get_cellByColumnKey('PROVE_DEN').get_value();
					Id_Certif=row.get_cellByColumnKey('ID_CERTIF').get_value();
					Den_Tipo_Certif=row.get_cellByColumnKey('DEN_TIPO_CERTIFICADO').get_value();
					Con_Email=row.get_cellByColumnKey('CON_EMAIL').get_value();
					Obligatorio=row.get_cellByColumnKey('OBLIGATORIO').get_value();
					var item ={DataKey:key,PlazoCumplimentacion:Plazo_Cumplimentacion,EnProceso:En_Proceso,
							ProveDen:Prove_Den,IdCertificado:Id_Certif,DenTipoCertificado:Den_Tipo_Certif,
							ConEmail:Con_Email,Obligatorio:Obligatorio};
					SelectedRows.push(item);				
				}
			};
			SelectedRowCount=SelectedRows.length;
		};
		function SolicitarRenovarCertificados(Renovar){
			if(SelectedRowCount==0){
				alert(TextosPopUpCertificados[4]);
				return false;
			};
			$('#popupFondo').css('height', $(document).height());
			$('#popupFondo').show();
			var whGrid = $find("<%= whgCertificados.ClientID %>");
			$('#chkEstablecerTodo').attr('checked', false);
			var diasPlazoCumplimentacion=null;
			var todosOpcionales=true;
			$.each(SelectedRows,function(index){
				if(this.EnProceso!=1){
					if(diasPlazoCumplimentacion==null && this.PlazoCumplimentacion!=-1){
						diasPlazoCumplimentacion=this.PlazoCumplimentacion;
					}else{
						if(this.PlazoCumplimentacion<diasPlazoCumplimentacion && this.PlazoCumplimentacion!=-1) diasPlazoCumplimentacion=this.PlazoCumplimentacion;
					}
					if (todosOpcionales && this.Obligatorio) todosOpcionales=false;
				}
			});		
			if(todosOpcionales){
				$.ajax({
					type: "POST",
					url: rutaFS + 'QA/Certificados/Certificados.aspx/SolicitarRenovar',
					contentType: "application/json; charset=utf-8",
					data: JSON.stringify({ Renovar:Renovar, 
											EstablecerTodo:true,
											FechaDesPub:null,
											FechaLimCumplim:null,
											Certificados: SelectedRows }),
					dataType: "json",
					async: false,
					success:function(msg){
						if(Renovar){
							window.open('certifsolicitadosOK.aspx?' + msg.d,'_self');
						}else{
							__doPostBack('<%= updGridCertificados.UniqueID %>','Solicitar');
						}
						$('#pnlSolicitarRenovar').hide();
						$('#popupFondo').hide();
					}
				});
			}else{	
				var today=new Date();
				var dpFechaLimiteCumplimentacion = $find("<%=dpFechaLimCumplim.ClientID%>");
				dpFechaLimiteCumplimentacion.set_enabled(true);
				if(diasPlazoCumplimentacion!=null){
					dpFechaLimiteCumplimentacion.set_value(today.setDate(today.getDate() + diasPlazoCumplimentacion));
					dpFechaLimiteCumplimentacion.set_text(today.getDate_UsuShortPattern());
				}
				if(SelectedRowCount>1){
					$('#lblEstablecerTodo').text(TextosPopUpCertificados[2]);
					$('#divEstablecerTodo').show();
					$('#lblVariosCertificados').text(TextosPopUpCertificados[3]);
					$('#divInfoVariosCertificados').show();
				}else{
					$('#divEstablecerTodo').hide();
					$('#lblVariosCertificados').text('');
					$('#divInfoVariosCertificados').hide();
				}
				$('[id$=lblTituloCertificados]').text(TextosPopUpCertificados[0]);
				$('#pnlSolicitarRenovar').show();
				CentrarPopUp($('#pnlSolicitarRenovar'));
				$('[id$=cmdContinuar]').attr('Renovar',Renovar);
				$('[id$=cmdContinuar]').css('margin-left', ($('#divBotonContinuar').outerWidth() - $('[id$=cmdContinuar]').outerWidth())/2);			
			}
		};
		function SolicitarRenovar(){
			var Renovar = ($('[id$=cmdContinuar]').attr('Renovar')=="true");
			var EstablecerTodo = $('#chkEstablecerTodo').is(':checked');
			var whGrid = $find("<%= whgCertificados.ClientID %>");
			var dpFechaDespublicacion = $find("<%=dpFechaDesPub.ClientID%>");
			var dpFechaLimiteCumplimentacion = $find("<%=dpFechaLimCumplim.ClientID%>");

			if (!EstablecerTodo && dpFechaLimiteCumplimentacion.get_value()==null){
				alert(TextosPopUpCertificados[6]);
				return false;
			}

			if(Renovar) MostrarCargando();

			$.ajax({
				type: "POST",
				url: rutaFS + 'QA/Certificados/Certificados.aspx/SolicitarRenovar',
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify({ Renovar:Renovar, 
										EstablecerTodo:EstablecerTodo,
										FechaDesPub:dpFechaDespublicacion.get_value(),
										FechaLimCumplim:dpFechaLimiteCumplimentacion.get_value(),
										Certificados: SelectedRows }),
				dataType: "json",
				async: false,
				success:function(msg){
					if(Renovar){
						window.open('certifsolicitadosOK.aspx?' + msg.d,'_self');
					}else{
						__doPostBack('<%= updGridCertificados.UniqueID %>','Solicitar');
					}
				}
			});
			$('#pnlSolicitarRenovar').hide();
			$('#popupFondo').hide();
		};
		function EnviarCertificados(){
			if(SelectedRowCount==0){
				alert(TextosPopUpCertificados[4]);
				return false;
			}
			var tiposCertificadoDiferente=false;
			var tipoCertificado=0;
			$.each(SelectedRows,function(){
				if(tipoCertificado==0) tipoCertificado=this.DataKey.split('#')[1];         
				if(this.DataKey.split('#')[1]!=tipoCertificado){
					tiposCertificadoDiferente=true;
					return false;
				}
			});
			if(tiposCertificadoDiferente){
				alert(TextosPopUpCertificados[5]);
				return false;
			}
			var proveedores='';
			$.each(SelectedRows,function(){
				if(proveedores!=0) proveedores+=',';         
				proveedores+="'" + this.DataKey.split('#')[0] + "'";
			});
			$.ajax({
				type: "POST",
				url: rutaFS + 'QA/Certificados/Certificados.aspx/Enviar',
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				async: false,
				success:function(msg){
					document.location.href=msg.d.replace('#####',tipoCertificado).replace('####',proveedores);
				}
			});	
			return false;
		};
		function EnviarEmail() {
			/*<summary>
			Abre en popup la ventana de envio de email pas�ndole los email de contacato de calidad de los proveedores seleccionados en el grid
			</summary>
			<remarks>Llamada desde: btnEnviarEmail; Hay que pasar el  porque la p�gina de enviar email es de PMweb. Tiempo m�ximo:0</remarks>*/
			var listaProveedores = '';
			$.each(SelectedRows,function(){
				if(this.ConEmail || listaProveedores.search(this.ConEmail) == -1){
					if(listaProveedores!='') listaProveedores+=';';         
					listaProveedores+=this.ConEmail;
				}
			});
			window.open('<%=System.Configuration.ConfigurationManager.AppSettings("rutaQA")%>_common/MailProveedores.aspx?Origen=2&Proveedores=' + listaProveedores, '_blank', 'width=710,height=360,status=yes,resizable=no scrollbars=yes,top=180,left=200');
			return false;
		};
		function IndexChanged(){
			var dropdownlist = $('[id$=PagerPageList]')[0];
            Pager(dropdownlist.selectedIndex);
		}
		function FirstPage(){
            Pager(0);
		}
		function PrevPage(){
			var dropdownlist = $('[id$=PagerPageList]')[0];
			if(dropdownlist.selectedIndex-1>=0){
                Pager(dropdownlist.selectedIndex-1);
			};
		};
		function NextPage(){
			var dropdownlist = $('[id$=PagerPageList]')[0];
			if(dropdownlist.selectedIndex+1<=dropdownlist.length-1){
                Pager(dropdownlist.selectedIndex+1);
			};
		};
		function LastPage(){
			var dropdownlist = $('[id$=PagerPageList]')[0];
            Pager(dropdownlist.length-1);
		};
        function Pager(page){
            var dropdownlist = $('[id$=PagerPageList]')[0];
			var btnPager = $('[id$=btnPager]').attr('id');
            dropdownlist.options[page].selected = true;
            __doPostBack(btnPager,page);
        };
        /*
        ''' <summary>
        ''' Fin function que filtra el grid (el "embudo")
        ''' </summary>
        ''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0seg.</remarks>*/     
		function whgCertificados_Filtered(sender,e){
            OcultarCargando();
            LoadFilter=false;
        }
        /*
        ''' <summary>
        ''' Function que filtra el grid (el "embudo")
        ''' </summary>
        ''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0seg.</remarks>*/     
        function whgCertificados_Filtering(sender, e) {
            if (LoadFilter==false){
                LoadFilter=true;
                MostrarCargando();

                var gridFiltered=false;
			    var filteredColumns,filteredCondition,filteredValue;
                var grid1erFilterValueVacio=false;
			    $('[id$=hFilteredColumns]').val('');
			    $('[id$=hFilteredCondition]').val('');
			    $('[id$=hFilteredValue]').val('');
			    for (i=0;i<e.get_columnFilters().length;i++){
				    if(e.get_columnFilters()[i].get_condition().get_rule()!=0){
					    gridFiltered=true;                    
					    filteredColumns=$('[id$=hFilteredColumns]').val();
					    filteredCondition=$('[id$=hFilteredCondition]').val();
					    filteredValue=$('[id$=hFilteredValue]').val();
					    $('[id$=hFilteredColumns]').val((filteredColumns==''?e.get_columnFilters()[i].get_columnKey():filteredColumns +"#"+e.get_columnFilters()[i].get_columnKey()));
					    $('[id$=hFilteredCondition]').val((filteredCondition==''?e.get_columnFilters()[i].get_condition().get_rule():filteredCondition +"#"+e.get_columnFilters()[i].get_condition().get_rule()));

                        if (grid1erFilterValueVacio){
                            //Dos filtros. Valor primer filtro es vacio y Valor Segundo Filtro no es vacio (9001)
                            //      hFilteredColumns es PROVE_DEN#TIPO_CERTIFICADO
                            //      hFilteredValue es 90001
                            //falla vb pq length por split # en un caso es 2 y en el otro es 1. Debe ser el hFilteredValue #9001
                            grid1erFilterValueVacio =false;
                            $('[id$=hFilteredValue]').val("#"+e.get_columnFilters()[i].get_condition().get_value());
                            filteredValue=$('[id$=hFilteredValue]').val();
                        }
                        else{
					        $('[id$=hFilteredValue]').val((filteredValue==''?e.get_columnFilters()[i].get_condition().get_value():filteredValue +"#"+e.get_columnFilters()[i].get_condition().get_value()));
                        }
                        if ((e.get_columnFilters()[i].get_condition().get_value()=='')&&(filteredValue=='')){
                            grid1erFilterValueVacio =true;
                        }
				    }    
			    };			
			    if(gridFiltered) {
				    $('[id$=hFiltered]').val(1);                
			    }else{
				    $('[id$=hFiltered]').val(0);
			    };
                if (parseInt($('[id$=hCacheLoaded]').val())==0){
                    var columnFilter = e.get_columnFilters()[e.get_columnFilters().length-1];                
                    e.set_cancel(true);
				    MostrarCargando();
				    $.ajax({       
					    type: 'POST',
					    url: rutaFS + 'QA/Certificados/Certificados.aspx/CargarCache',
					    contentType: 'application/json; charset=utf-8',
					    dataType: 'json',
					    async: true,
					    success:function(){
						    $('[id$=hCacheLoaded]').val(1);                        
						    OcultarCargando();
                            sender.get_behaviors().get_filtering().add_columnFilter(columnFilter);
                            sender.get_behaviors().get_filtering().applyFilters();
					    }
				    });
                };	
            };
		};
        /*
        ''' <summary>
        ''' Fin function que ordena el grid
        ''' </summary>
        ''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0seg.</remarks>*/     
		function whgCertificados_Sorted(sender,e){
            OcultarCargando();
        }
		function whgCertificados_Sorting(sender,e) { 
            /*''' <summary>
		    ''' Ordena los datos por la columna indicada, si es q es ordenable por ese concepto.
		    ''' </summary>
		    ''' <param name="sender">Origen del evento</param>
		    ''' <param name="e">evento</param>        
		    ''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>*/          
			switch (e.get_column().get_key()) {
				case "EN_PROCESO":
				case "IMAGE_PROVE":
				case "IMAGE_ESTADO":
				case "DEN_MATERIAL":
					e.set_cancel(true);
					break;
				default:
                    MostrarCargando();

					var sortSentence;
					if (e.get_clear()){
						sortSentence=e.get_column().get_key() + " " + (e.get_sortDirection()==1?"ASC":"DESC");
					}else{
						sortSentence=$('[id$=hOrdered]').val() + "," + e.get_column().get_key() + " " + (e.get_sortDirection()==1?"ASC":"DESC");
					};
                    sortSentence = sortSentence.replace("BOOL_","");

					$('[id$=hOrdered]').val(sortSentence);
					if($('[id$=hFiltered]').val()==1){
						var btnOrder = $('[id$=btnOrder]').attr('id');
						__doPostBack(btnOrder,'');
					};
					break;
			};
		};
		function OcultarCargando() {
			var modalprog = $find(ModalProgress);
			if (modalprog) modalprog.hide();
		};
        /*
        ''' <summary>
        ''' Fin function que agrupa el grid
        ''' </summary>
        ''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0seg.</remarks>*/     
		function whgCertificados_GroupedColumnsChanged(sender,e){
            OcultarCargando();
        }
       /*
        ''' <summary>
        ''' function que agrupa el grid
        ''' </summary>
        ''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0seg.</remarks>*/   
        function whgCertificados_GroupedColumnsChanging(sender,e){
            if (parseInt($('[id$=hCacheLoaded]').val())==0){                
				var column = e.get_effectedColumn();
				MostrarCargando();
                e.set_cancel(true);
				var antes = new Date();				
				$.ajax({       
					type: 'POST',
					url: rutaFS + 'QA/Certificados/Certificados.aspx/CargarCache',
					contentType: 'application/json; charset=utf-8',
					dataType: 'json',
					async: true,
					success:function(){                         
						$('[id$=hCacheLoaded]').val(1);
                        OcultarCargando();  
                        
                        if(e.get_action()==1) $find("<%= whgCertificados.ClientID %>").get_groupingSettings().get_groupedColumns().remove(column);
                        else $find("<%= whgCertificados.ClientID %>").get_groupingSettings().get_groupedColumns().add(column.get_columnKey(),column.get_sortingDirection());                                                                    
					}
				});
			}else{               
                MostrarCargando();   

                if(e.get_groupedColumns().get_length()==1 && e.get_action()==1) $('[id$=hGrouped]').val(0);
                else $('[id$=hGrouped]').val(1);
            };
        };
        function whgCertificados_Initialize(sender,e){
            ig_controls.<%= whgCertificados.ClientID %>._callbackManager.setTimeout(120000);
            ig_controls.<%= whgCertificados.ClientID %>.get_gridView()._callbackManager.setTimeout(120000);
            /*Codigo para guardar datos del FSAL cuando se cargue el Grid.
              Si es primera llamada a la pagina y no se han enviado los tiempos para update, el booleano estara a 'False'.
              Entonces se hara la update y se pondra a 'True' para hacer la insercion de un nuevo registro. */
            if ($('#bActivadoFSAL').val() == '1') {
                if ($('#bEnviadoFSAL8Inicial').val() == '0') {
                    $('#bEnviadoFSAL8Inicial').val('1');
                }
            }
            /*Fin codigo FSAL*/
        };
        function whgCertificados_AJAXResponseError(sender,e){
            e.set_cancel(true);	
            alert(TextosPopUpCertificados[11]);
        };
        function whgCertificadosExportacion_Initialize(sender,e){
            ig_controls.<%= whgCertificadosExportacion.ClientID %>._callbackManager.setTimeout(60000);
            ig_controls.<%= whgCertificadosExportacion.ClientID %>.get_gridView()._callbackManager.setTimeout(60000);
        };
	</script>
	<ig:WebExcelExporter ID="wdgExcelExporter" runat="server"></ig:WebExcelExporter>
	<ig:WebDocumentExporter ID="wdgPDFExporter" runat="server" ExportMode="Download"></ig:WebDocumentExporter>
	<div style="clear:both; float:left; width:100%;">
		<fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
	</div>
	<div id="pnlBusqueda" class="PanelCabecera" style="clear:both; float:left; width:100%; height:20px; margin-top:10px; cursor:pointer;">
		<img id="imgExpandir" alt="" src="" style="display:none; vertical-align:middle;" />
		<asp:Label ID="lblBusquedaAvanzada" runat="server" Text="DSeleccione..." CssClass="Texto12 Negrita TextoClaro" style="line-height:20px;"></asp:Label>
	</div>
	<div id="pnlParametros" class="Rectangulo" style="display:none;">
		<div style="clear:both; float:left; width:100%;">
			<asp:UpdatePanel ID="upBusquedaAvanzada" runat="server" UpdateMode="Conditional">
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="btnLimpiarBusquedaAvanzada" EventName="Click" />
				</Triggers>
				<ContentTemplate>
					<fsqa:BusquedaAvanzadaCERT ID="Busqueda" runat="server"></fsqa:BusquedaAvanzadaCERT>
				</ContentTemplate>
			</asp:UpdatePanel>
		</div>
		<div style="clear:both; float:left; width:100%; margin-top:10px;">
			<div style="float:left; margin-left:10px;">
				<fsn:FSNButton ID="btnBuscarBusquedaAvanzada" runat="server" Text="DBuscar"></fsn:FSNButton>                
			</div>
			<div style="float:left; margin-left:10px;">
				<fsn:FSNButton ID="btnLimpiarBusquedaAvanzada" runat="server" Text="DLimpiar"></fsn:FSNButton>
			</div>
		</div>
	</div>
	<asp:ObjectDataSource ID="odsFiltrosConfigurados" runat="server" SelectMethod="LoadConfigurados"
		TypeName="Fullstep.FSNServer.FiltrosQA">
		<SelectParameters>
			<asp:Parameter Name="sPersona" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<div style="clear:both; float:left; width:100%; margin-top:15px;">
		<asp:UpdatePanel runat="server" ID="updFiltros" UpdateMode="Conditional">
			<ContentTemplate>
				<asp:HiddenField ID="hCacheLoaded" runat="server" Value="0" />
				<asp:HiddenField ID="QAFiltroUsuarioIdVCert" runat="server" Value="0" />
				<asp:HiddenField ID="QAFiltroIdVCert" runat="server" Value="0" />
				<asp:HiddenField ID="QANombreTablaVCert" runat="server" Value="" />
				<div style="float:left;">
					<asp:DataList ID="dlFiltrosConfigurados" runat="server" DataSourceID="odsFiltrosConfigurados"
						RepeatDirection="Horizontal" CellPadding="0" RepeatLayout="Table" RepeatColumns="4">
						<ItemTemplate>                                    
							<fsn:FSNTab ID="fsnTabFiltro" SkinID="PestanyasFiltros" Alineacion="Left" runat="server"
								Visible="true" Text='<%# IIf(Eval("ID")=0,Textos(0),Eval("NOM_FILTRO"))%>' 
								ToolTip='<%# IIf(Eval("ID")=0,"",Eval("NOM_FILTRO")) %>' OnClick="fsnTabFiltro_Click"
								CommandArgument='<%# Eval("ID") & "#" & Eval("NOM_TABLA")  & "#" & Eval("IDFILTRO") %>'
                                idTab='<%# Eval("ID")%>' nomTabla='<%# Eval("NOM_TABLA")%>' idFiltro='<%# Eval("IDFILTRO")%>'
								BorderStyle="None" BorderColor="#AAAAAA" BorderWidth="1px">
							</fsn:FSNTab>                             
						</ItemTemplate>
					</asp:DataList>
				</div>
				<div style="float:right; margin-right:10px;">
					<fsn:FSNButton ID="btnAnyadirFiltros" runat="server" Text="DA�adir filtros"></fsn:FSNButton>
				</div>	
				<div style="float:right; margin-right:10px;">
					<fsn:FSNButton ID="btnConfigurar" runat="server" Text="Configurar"></fsn:FSNButton>
				</div>			
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
	<asp:Button runat="server" ID="btnOrder" style="display:none;" />
	<asp:Button runat="server" ID="btnFilter" style="display:none;" />
	<asp:Button runat="server" ID="btnRecargaCache" style="display:none;" />    
	<asp:UpdatePanel runat="server" ID="upPager" UpdateMode="Conditional">
		<Triggers>
			<asp:AsyncPostBackTrigger ControlID="btnOrder" EventName="Click" />
			<asp:AsyncPostBackTrigger ControlID="btnFilter" EventName="Click" />
			<asp:AsyncPostBackTrigger ControlID="btnRecargaCache" EventName="Click" />            
		</Triggers>
		<ContentTemplate>
			<asp:HiddenField runat="server" ID="hFiltered" Value="0" />
			<asp:HiddenField runat="server" ID="hFilteredColumns" Value="" />
			<asp:HiddenField runat="server" ID="hFilteredCondition" Value="" />
			<asp:HiddenField runat="server" ID="hFilteredValue" Value="" />
			<asp:HiddenField runat="server" ID="hOrdered" Value="" />
            <asp:HiddenField runat="server" ID="hGrouped" Value="0" />
		</ContentTemplate>
	</asp:UpdatePanel>
	<asp:Button runat="server" ID="btnPager" style="display:none;" />	
	<asp:Button runat="server" ID="btnPagerBD" style="display:none;" />
	<div style="clear:both; float:left; width:100%;">
		<asp:UpdatePanel ID="updGridCertificados" ChildrenAsTriggers="false" runat="server" UpdateMode="Conditional">						
			<Triggers>
				<asp:AsyncPostBackTrigger ControlID="btnBuscarBusquedaAvanzada" EventName="Click" />
				<asp:AsyncPostBackTrigger ControlID="btnPager" EventName="Click" />
				<asp:AsyncPostBackTrigger ControlID="btnPagerBD" EventName="Click" />
			</Triggers>
			<ContentTemplate>
                <asp:HiddenField runat="server" ID="sProducto" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="sFechaIniFSAL8" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="sPagina" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="iPost" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="iP" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="sUsuCod" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="sPaginaOrigen" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="sNavegador" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="sIdRegistroFSAL8" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="sProveCod" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="sQueryString" ClientIDMode="Static" />
				<ig:WebHierarchicalDataGrid runat="server" ID="whgCertificadosExportacion" 
					Visible="false" AutoGenerateBands="false"
					AutoGenerateColumns="false" Width="100%" EnableAjax="true" 
					EnableAjaxViewState="false" EnableDataViewState="false">
                    <ClientEvents Initialize="whgCertificadosExportacion_Initialize"/>
                    <GroupingSettings EnableColumnGrouping="True" />
					<Behaviors>
						<ig:Filtering Enabled="true"></ig:Filtering>
						<ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting>
						<ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
					</Behaviors>
				</ig:WebHierarchicalDataGrid>
				<ig:WebHierarchicalDataGrid runat="server" ID="whgCertificados" AutoGenerateBands="false"
					AutoGenerateColumns="false" Width="100%" EnableAjax="true" 
					EnableAjaxViewState="false" EnableDataViewState="false" >
					<GroupingSettings EnableColumnGrouping="True" GroupAreaVisibility="Visible"/>
                    <ClientEvents Click="whgCertificados_CellClick" 
                        GroupedColumnsChanging="whgCertificados_GroupedColumnsChanging"
                        GroupedColumnsChanged="whgCertificados_GroupedColumnsChanged"
                        AJAXResponseError="whgCertificados_AJAXResponseError" 
                        Initialize="whgCertificados_Initialize" />
					<Behaviors>                    
						<ig:Activation Enabled="true"/>
						<ig:Filtering Alignment="Top" Enabled="true" Visibility="Visible" AnimationEnabled="false">
							<FilteringClientEvents DataFiltering="whgCertificados_Filtering" DataFiltered ="whgCertificados_Filtered"/>
						</ig:Filtering>
						<ig:RowSelectors Enabled="true" EnableInheritance="true">
							<RowSelectorClientEvents RowSelectorClicked="whgCertificados_RowSelected" />                            
						</ig:RowSelectors>
						<ig:Selection Enabled="true" RowSelectType="Multiple" CellSelectType="None" ColumnSelectType="None">
						</ig:Selection>
						<ig:Sorting Enabled="true" SortingMode="Multi" SortingClientEvents-ColumnSorting="whgCertificados_Sorting" SortingClientEvents-ColumnSorted="whgCertificados_Sorted" ></ig:Sorting>
						<ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
						<ig:ColumnMoving Enabled="false"></ig:ColumnMoving>								
						<ig:Paging Enabled="true" PagerAppearance="Top">
							<PagerTemplate>
								<div class="CabeceraBotones" style="clear:both; float:left; width:100%;"> 
									<div style="float:left; padding:3px 0px 2px 0px; margin-left:10px;">
										<div style="clear: both; float: left; margin-right: 5px;">
											<asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" style="margin-right:2px;" />
											<asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" />
										</div>
										<div style="float: left; margin-right:5px;">
											<asp:Label ID="lblPage" runat="server" Text="dPage" Style="margin-right: 3px;"></asp:Label>
											<asp:DropDownList ID="PagerPageList" runat="server" Style="margin-right: 3px;" onchange="return IndexChanged()" />
											<asp:Label ID="lblOF" runat="server" Text="dOf" Style="margin-right: 3px;"></asp:Label>
											<asp:Label ID="lblCount" runat="server" />
										</div>
										<div style="float: left;">
											<asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-right:2px;" />
											<asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" />
										</div>
									</div>
									<div style="float:right; margin:5px 5px 0px 0px;">
										<asp:Panel runat="server" ID="pnlSolicitar" onclick="SolicitarRenovarCertificados(false);" class="botonDerechaCabecera" style="float:left;">
											<asp:Image runat="server" ID="imgSolicitar" CssClass="imagenCabecera" />
											<asp:Label runat="server" ID="lnkSolicitar" CssClass="fntLogin2" style="margin-left:3px;" Text="dSolicitar"></asp:Label>
										</asp:Panel>
										<asp:Panel runat="server" ID="pnlRenovar" onclick="SolicitarRenovarCertificados(true);" class="botonDerechaCabecera" style="float:left;">           
											<asp:Image runat="server" ID="imgRenovar" CssClass="imagenCabecera" />
											<asp:Label runat="server" ID="lnkRenovar" CssClass="fntLogin2" style="margin-left:3px;" Text="dRenovar"></asp:Label>           
										</asp:Panel>
										<asp:Panel runat="server" ID="pnlEnviar" onclick="EnviarCertificados()" class="botonDerechaCabecera" style="float:left;">										       
											<asp:Image runat="server" ID="imgEnviar" CssClass="imagenCabecera" />
											<asp:Label runat="server" ID="lnkEnviar" CssClass="fntLogin2" style="margin-left:3px;" Text="dEnviar"></asp:Label>           
										</asp:Panel>
										<div onclick="ExportarExcel()" class="botonDerechaCabecera" style="float:left;">            
											<asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
											<asp:Label runat="server" ID="lblExcel" CssClass="fntLogin2" style="margin-left:3px;" Text="dExcel"></asp:Label>           
										</div>
										<div onclick="ExportarPDF()" class="botonDerechaCabecera" style="float:left;">            
											<asp:Image runat="server" ID="imgPDF" CssClass="imagenCabecera" />
											<asp:Label runat="server" ID="lblPDF" CssClass="fntLogin2" style="margin-left:3px;" Text="dPDF"></asp:Label>           
										</div>
										<div onclick="EnviarEmail()" class="botonDerechaCabecera" style="float:left; margin-left:15px;">            
											<asp:Image runat="server" ID="imgEmail" CssClass="imagenCabecera" />
											<asp:Label runat="server" ID="lblEmail" CssClass="fntLogin2" style="margin-left:3px;" Text="dEmail"></asp:Label>           
										</div>
									</div>
								</div>						
							</PagerTemplate>
						</ig:Paging>						
					</Behaviors>					
				</ig:WebHierarchicalDataGrid>                
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
	<div id="pnlSolicitarRenovar" class="modalPopup" style="display:none; position:absolute; z-index:1002; max-width:400px; max-height:300px;">        
		<div style="clear:both; float:left; width:100%; position:relative;" onclick="return OcultarAyuda()">
			<div style="position:relative; clear:both; float:left; width:100%; margin-bottom:20px;">
				<div style="position:absolute; top:2px; left:2px; z-index:1;">
					<img id="imgCalendarSolicitarRenovar" alt="" runat="server" src="" style="max-width:80px; max-height:70px;" />
				</div>
				<div style="position:absolute; width:30px; height:30px; top:2px; right:10px; cursor:pointer;">
					<asp:Image ID="ImgBtnCerrarSolicitarRenovar" runat="server" SkinID="Cerrar" ToolTip="Cerrar Detalle" />
				</div>
				<div style="float:left; width:100%; height:35px;" class="CapaTituloPanelInfo">
					<asp:Label ID="lblTituloCertificados" runat="server" CssClass="TituloPopUpPanelInfo" style="line-height:35px; margin-left:100px;">DCertificados</asp:Label>						 
				</div>
				<div style="float:left; width:100%; height:20px;" class="CapaSubTituloPanelInfo">
					<asp:Label ID="lblSubTitulo" runat="server" CssClass="Texto12 Negrita TextoClaro" style="line-height:20px; margin-left:100px;">DFecha de cumplimentaci�n</asp:Label>
				</div>				
			</div>
			<div style="clear:both; float:left; width:100%;">
				<div style="clear:both; float:left; padding:10px;">
					<div id="divEstablecerTodo" style="clear:both; float:left; width:100%;">
						<input type="checkbox" id="chkEstablecerTodo" class="captionBlueSmall" style="margin-left:5px;" />
						<span id="lblEstablecerTodo" class="captionBlueSmall"></span>						
					</div>
					<div style="clear:both; float:left; width:100%; margin-top:5px;">
						<asp:Label ID="lblFechaLimiteCumplimentacion" runat="server" CssClass="captionBlue" style="margin-left:5px;">DIntroduzca la fecha l�mite de cumplimentaci�n</asp:Label>
					</div>
					<div id="divInfoVariosCertificados" style="clear:both; float:left; width:100%; margin-top:5px;">
						<span id="lblVariosCertificados" class="captionBlueSmall" style="margin-left:5px;">DPara todos los certificados</span>
					</div>
					<div style="clear:both; float:left; width:100%; margin-top:5px;">
						<igpck:WebDatePicker runat="server" ID="dpFechaLimCumplim" NullText="" SkinID="Calendario"></igpck:WebDatePicker>
						<img id="imgAyudaLimiteCumplimentacion" runat="server" src="" alt="" style="border:0px; margin-left:5px; cursor:pointer;" />
					</div>
					<div style="clear:both; float:left; width:100%; margin-top:5px;">
						<asp:Label id="lblFechaDespublicacion" runat="server" CssClass="captionBlueSmall" style="color:Black;">DIntroduzca la fecha de despublicaci�n</asp:Label>
					</div>
					<div style="clear:both; float:left; width:100%; margin-top:5px;">
						<igpck:WebDatePicker runat="server" ID="dpFechaDesPub" NullText="" SkinID="Calendario"></igpck:WebDatePicker>
						<img id="imgAyudaDespublicacion" runat="server" src="" alt="" style="border:0px; margin-left:5px; cursor:pointer;" />
					</div>
				</div>
				<div id="divBotonContinuar" style="clear:both; float:left; width:100%; text-align:center; margin-bottom:10px;">
					<fsn:FSNButton ID="cmdContinuar" runat="server" Text="DAceptar" Alineacion="Left" OnClientClick="SolicitarRenovar()" ></fsn:FSNButton>
				</div>
			</div>
			<div id="divAyuda" style="background-color:Transparent; z-index:11000; position:absolute;">
				<span id="sAyudaLimiteCumplimentacion" class="hint" style="display: none;">
					<asp:Label runat="server" ID="lblAyudaLimiteCumplimentacion" 
						Style="display:inline; color:#000; text-align:justify;" CssClass="parrafo"></asp:Label>
				</span>
				<span id="iAyudaLimiteCumplimentacion" class="hint-pointer" style="z-index:100300;">&nbsp;</span> 
				<span id="sAyudaDespublicacion" class="hint2" style="display: none;">
					<asp:Label runat="server" ID="lblAyudaDespublicacion" 
						Style="display:inline; color:#000; text-align:justify;" CssClass="parrafo"></asp:Label>
				</span>
				<span id="iAyudaDespublicacion" class="hint-pointer2" style="z-index:100300; display:none;">&nbsp;</span>
			</div>		
		</div>
	</div>
	<fsqa:MostrarPanelArbol ID="PanelSimple" runat="server"></fsqa:MostrarPanelArbol>
	<fsqa:MostrarPanelArbol ID="PanelProve" runat="server"></fsqa:MostrarPanelArbol>
	
	<fsn:FSNPanelInfo ID="FSNPanelDatosProveedorQA" runat="server" 
		ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_DatosProveedorQA" 
		TipoDetalle="1" />
	<fsn:FSNPanelInfo ID="FSNPanelDatosPersonaQA" runat="server" 
		ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_DatosPersona" 
		TipoDetalle="2"/>
	<fsn:FSNPanelInfo ID="FSNPanelEnProceso" runat="server" 
		ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_TextoEnProceso" 
		TipoDetalle="4" 
		ImagenPopUp="~/App_Themes/<%=Me.Page.Theme%>/images/Icono_Error_Amarillo_40x40.gif" 
		Height="120px" Width="370px"/>

    <asp:HiddenField runat="server" ID="bActivadoFSAL" ClientIDMode="Static" Value="0" />
    <asp:HiddenField runat="server" ID="bEnviadoFSAL8Inicial" ClientIDMode="Static" Value="0" />
    <asp:HiddenField runat="server" ID="sIdRegistroFSAL1" ClientIDMode="Static" />

</asp:Content>
