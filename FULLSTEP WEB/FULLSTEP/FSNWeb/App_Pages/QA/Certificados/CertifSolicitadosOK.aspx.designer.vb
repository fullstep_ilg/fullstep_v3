﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class CertifSolicitadosOK

    '''<summary>
    '''FSNPageHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FSNPageHeader As Global.Fullstep.FSNWebControls.FSNPageHeader

    '''<summary>
    '''lblMensaje control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMensaje As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Linea1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Linea1 As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''tblPub control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblPub As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''lblProv1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProv1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTipoCertif1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTipoCertif1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblMensajeW control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMensajeW As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Linea2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Linea2 As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''tblWork control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblWork As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''lblProv2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProv2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTipoCertif2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTipoCertif2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblIdent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblIdent As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAprobador control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAprobador As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Linea3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Linea3 As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''tblEnProceso control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblEnProceso As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''imgEnProceso control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgEnProceso As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''lblEnProceso control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEnProceso As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''tblEnProcesoIn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblEnProcesoIn As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''lblProv3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProv3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTipoCertif3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTipoCertif3 As Global.System.Web.UI.WebControls.Label
End Class
