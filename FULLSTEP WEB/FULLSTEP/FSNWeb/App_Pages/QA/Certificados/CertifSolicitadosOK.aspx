﻿<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="~/App_Master/Menu.Master" CodeBehind="CertifSolicitadosOK.aspx.vb" Inherits="Fullstep.FSNWeb.CertifSolicitadosOK" %>


<asp:Content ID="Content1" ContentPlaceHolderID="CPH4" runat="server">
<script>
    /*
    ''' <summary>
    ''' Volver de esta pagina al Visor de Certificados
    ''' </summary>
    ''' <remarks>Llamada desde: FSNPageHeader.OnClientClickVolver; Tiempo máximo</remarks>*/
    function IrACertificados() {
        window.open("certificados.aspx", "_self")
    }
	
</script>
<table id="tblTitulo" cellspacing="1" cellpadding="1" width="100%" border="0" >
    <tr>
        <td>
            <fsn:FSNPageHeader ID="FSNPageHeader" runat="server">
            </fsn:FSNPageHeader>
        </td>
    </tr>
</table>
			
<table id="Table1" style="width: 100%;" cellspacing="1" cellpadding="1" border="0">
    <tr>
        <td style="padding-left: 10px;">
            <asp:Label ID="lblMensaje" runat="server" CssClass="InfoEnvioCertificados" Width="100%">Label</asp:Label>
        </td>
    </tr>
    <tr id="Linea1" height="40%" runat="server">
        <td style="padding-left: 10px; vertical-align: top; height: 40%;">
            <table id="tblPub" width="100%" border="0" runat="server" class="ColorFondoTab">
                <tr>
                    <td width="60%">
                        <asp:Label ID="lblProv1" runat="server" CssClass="CabeceraInfoEnvioCertificados" Width="100%">DProveedor</asp:Label>
                    </td>
                    <td width="35%">
                        <asp:Label ID="lblTipoCertif1" runat="server" CssClass="CabeceraInfoEnvioCertificados" Width="100%">Label</asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblMensajeW" runat="server" CssClass="ColorFondoTab" Width="100%">Label</asp:Label>
        </td>
    </tr>
    <tr id="Linea2" height="40%" runat="server">
        <td valign="top" height="40%">
            <table id="tblWork" width="100%" border="0" runat="server">
                <tr>
                    <td style="border-bottom: cornflowerblue thin solid">
                        <asp:Label ID="lblProv2" runat="server" CssClass="CabeceraInfoEnvioCertificados" Width="100%">DProveedor</asp:Label>
                    </td>
                    <td style="border-bottom: cornflowerblue thin solid">
                        <asp:Label ID="lblTipoCertif2" runat="server" CssClass="CabeceraInfoEnvioCertificados" Width="100%">Label</asp:Label>
                    </td>
                    <td style="border-bottom: cornflowerblue thin solid">
                        <asp:Label ID="lblIdent" runat="server" CssClass="CabeceraInfoEnvioCertificados" Width="100%">Label</asp:Label>
                    </td>
                    <td style="border-bottom: cornflowerblue thin solid">
                        <asp:Label ID="lblAprobador" runat="server" CssClass="CabeceraInfoEnvioCertificados" Width="100%">Label</asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr id="Linea3" height="40%" runat="server" width="100%">
        <td>
            <table id="tblEnProceso" width="100%" border="0" runat="server">
                <tr>
                    <td colspan="4">
                        <p>
                            <table cellpadding="4" width="75%">
                                <tr>
                                    <td style="width: 23px">
                                        <asp:Image runat="server" ID="imgEnProceso" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblEnProceso" runat="server" CssClass="InfoEnvioCertificados"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="tblEnProcesoIn" cellspacing="3" cellpadding="3" width="100%" border="0"
                            runat="server" class="ColorFondoTab">
                            <tr>
                                <td width="65%">
                                    <asp:Label ID="lblProv3" runat="server" CssClass="CabeceraInfoEnvioCertificados" Width="100%">DProveedor</asp:Label>
                                </td>
                                <td width="35%">
                                    <asp:Label ID="lblTipoCertif3" runat="server" CssClass="CabeceraInfoEnvioCertificados" Width="100%">dTipo Certif.</asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</asp:Content>

