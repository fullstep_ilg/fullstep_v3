﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="PanelEscalacionProves.aspx.vb" Inherits="Fullstep.FSNWeb.PanelEscalacionProves" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link href="../../../js/jquery/plugins/styles/fsHierarchicalGrid.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
	<fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
	<div id="pnlBusqueda" class="PanelCabecera Bordear" style="margin:1em 0.5em 0em 0.5em; line-height:2em; cursor:pointer;">        
		<asp:Image runat="server" ID="imgCollapsePanelBusqueda" SkinID="Contraer" style="vertical-align:middle;" />
		<asp:Image runat="server" ID="imgExpandPanelBusqueda" SkinID="Expandir" style="vertical-align:middle;" CssClass="Ocultar" />
		<asp:Label runat="server" ID="lblPanelBusqueda" CssClass="Negrita" Text="dSeleccione los criterios de busqueda generales"></asp:Label>
	</div>
	<div class="ColorFondoTab" style="display:none; padding:0.5em; margin:0em 0.5em;">
		<asp:Label runat="server" ID="lblProveedor" CssClass="Negrita"></asp:Label>
		<div class="Bordear" style="display:inline-block; line-height:1.5em; background-color:white;">
            <asp:HiddenField runat="server" ID="hProveedor" ClientIDMode="Static" />
            <asp:TextBox ID="txtProveedor" runat="server" Width="15em" BorderWidth="0px" BackColor="White" Height="16px" ClientIDMode="Static"
                onchange="txtProveedor_TextChanged(this);"
                onkeyup="txtProveedor_KeyUp(this);SetProveContextKey();" 
                onblur="ResetProveContextKey();">
            </asp:TextBox>
            <asp:ImageButton runat="server" ID="imgProveedorLupa" SkinID="BuscadorLupa" style="vertical-align:middle;" OnClientClick="return AbrirBuscadorProveedor();" />            
            <ajx:AutoCompleteExtender ID="txtProveedor_AutoCompleteExtender" runat="server" CompletionSetCount="10" 
                UseContextKey="true" ClientIDMode="Static"
                DelimiterCharacters="" Enabled="True" MinimumPrefixLength="1" EnableCaching="False" 
                ServiceMethod="GetProveedoresPanelEscalacion"
                ServicePath="~/App_Pages/_Common/App_Services/AutoComplete.asmx" 
                TargetControlID="txtProveedor" 
                CompletionListElementID="ajxAutoComplete"
                OnClientItemSelected="Proveedor_Item_Seleccionado">
            </ajx:AutoCompleteExtender>
            <div id="ajxAutoComplete"></div>
		</div>        
		<asp:Label runat="server" ID="lblUNQA" CssClass="Negrita" style="margin-left:0.5em;"></asp:Label>
		<div style="display:inline-table; vertical-align:middle;" >
			<ig:WebDropDown ID="wddUNQA" runat="server" Width="20em" Height="20px"
				EnableAutoCompleteFirstMatch="false"
                EnableMultipleSelection="true"
				DropDownContainerWidth="320px"
				DropDownContainerHeight="200px"
				EnableClosingDropDownOnSelect="false"
				CurrentValue=""
				MultipleSelectionType="Checkbox">
			</ig:WebDropDown>
		</div>
		<asp:Label runat="server" ID="lblNivelEscalacion" CssClass="Negrita" style="margin-left:0.5em;"></asp:Label>
		<div style="display:inline-table; vertical-align:middle;" >
			<ig:WebDropDown ID="wddNivelEscalacion" runat="server" Width="15em" Height="20px"
				EnableAutoCompleteFirstMatch="false"
				DropDownContainerWidth="320px"
				DropDownContainerHeight="100px"
				EnableClosingDropDownOnSelect="true"
				CurrentValue=""
				MultipleSelectionType="Checkbox">
			</ig:WebDropDown>
		</div>
		<label style="line-height:1.5em;">
			<input type="checkbox" id="chkEscalacionPendienteAprobar" style="vertical-align:middle;"/>
			<span id="lblEscalacionPendienteAprobar" style="vertical-align:middle;"></span>
		</label>
        <div style="margin-top:1em; margin-left:22em;" >
            <label style="cursor:pointer;">
                <asp:CheckBox runat="server" ID="chkMostrarProveedoresBaja" />
                <span id="lblMostrarProveedoresBaja"></span>
            </label>
           
            <label style="cursor:pointer;margin-left:13em;">                
                <asp:CheckBox runat="server" ID="chkMostrarHistoricoEscalaciones" />
                <span id="lblMostrarHistoricoEscalaciones"></span>
            </label>              
        </div>        
        
        <div style="margin-top:1em; margin-bottom:0.5em;">
            <span id="btnBuscar" class="botonRedondeado" style="margin-right:0.5em;"></span>
            <span id="btnLimpiar" class="botonRedondeado"></span>
        </div>
	</div>
    <asp:Button runat="server" ID="btnPaginador" style="display:none;" ClientIDMode="Static" />
    <asp:Button runat="server" ID="btnBuscador" style="display:none;" ClientIDMode="Static" />
    <asp:Button runat="server" ID="btnRechazarEscalacion" style="display:none;" ClientIDMode="Static" />
    <asp:Button runat="server" ID="btnExcel" style="display:none;" ClientIDMode="Static" />       
    
    <ig:WebExcelExporter ID="whdgExcelExporter" runat="server" ExportMode="Download"></ig:WebExcelExporter>
    <asp:UpdatePanel runat="server" ID="upGrid" style="margin-top:1em" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnBuscador" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnPaginador" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnRechazarEscalacion" EventName="Click" />            
        </Triggers>
        <ContentTemplate>
            <ig:WebHierarchicalDataGrid runat="server" ID="whdgPanelEscalacion" Width="100%"        
		        AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjaxViewState="true"
		        DataMember="INFO_ESCALACION" DataKeyFields="ID" EnableAjax="true" EnableViewState="true"
                InitialDataBindDepth="1" InitialExpandDepth="0">
                <ClientEvents Click="whdgPanelEscalacion_Click" />
                <ExpandButton ImageUrl="../../../Images/masExpand.png" HoverImageUrl="../../../Images/masExpand.png" PressedImageUrl="../../../Images/masExpand.png" />
                <CollapseButton ImageUrl="../../../Images/menosCollapse.png" HoverImageUrl="../../../Images/menosCollapse.png" PressedImageUrl="../../../Images/menosCollapse.png"  />                                
                <Columns>
                    <ig:BoundDataField Key="ID" DataFieldName="ID" Hidden="true"></ig:BoundDataField>
                    <ig:BoundDataField Key="PROVE" DataFieldName="PROVE" Hidden="true"></ig:BoundDataField>
                    <ig:BoundDataField Key="SOLICITUD" DataFieldName="SOLICITUD" Hidden="true"></ig:BoundDataField>
                    <ig:BoundDataField Key="UNQA" DataFieldName="UNQA" Hidden="true"></ig:BoundDataField>
                    <ig:BoundDataField Key="NOCONF_ESCALACION" DataFieldName="NOCONF_ESCALACION" Hidden="true"></ig:BoundDataField>
                    <ig:BoundDataField Key="INSTANCIA_ESCALACION" DataFieldName="INSTANCIA_ESCALACION" Hidden="true"></ig:BoundDataField>
                    <ig:BoundDataField Key="PROVEDEN" DataFieldName="PROVEDEN" Hidden="false" CssClass="SinSalto" Header-CssClass="SinSalto"></ig:BoundDataField>
                    <ig:BoundDataField Key="UNQA1" DataFieldName="UNQA1" Hidden="false"></ig:BoundDataField>
                    <ig:BoundDataField Key="UNQA2" DataFieldName="UNQA2" Hidden="false"></ig:BoundDataField>
                    <ig:BoundDataField Key="UNQA3" DataFieldName="UNQA3" Hidden="false"></ig:BoundDataField>
                    <ig:BoundDataField Key="NIVEL_ESCALACION" DataFieldName="NIVEL_ESCALACION" Hidden="false"></ig:BoundDataField>
                    <ig:BoundDataField Key="ESTADO" DataFieldName="ESTADO" Hidden="true"></ig:BoundDataField>
                    <ig:BoundDataField Key="ESTADO_DEN" DataFieldName="ESTADO_DEN" Hidden="false"></ig:BoundDataField>
                    <ig:BoundDataField Key="FECHA" DataFieldName="FECHA" Hidden="false"></ig:BoundDataField>
                    <ig:TemplateDataField Key="NOCONF_ESCALACION_DEN" CssClass="SinSalto" Header-CssClass="SinSalto">
                    </ig:TemplateDataField>
                    <ig:TemplateDataField Key="APROBAR_RECHAZAR" CssClass="SinSalto" Header-CssClass="SinSalto">
                    </ig:TemplateDataField>
                    <ig:BoundDataField Key="FECHAHISTORICO" DataFieldName="FECACT" CssClass="SinSalto" Header-CssClass="SinSalto"></ig:BoundDataField>
                    <ig:BoundDataField Key="COMENTARIO" DataFieldName="COMENTARIO" CssClass="SinSalto" Header-CssClass="SinSalto"></ig:BoundDataField>
		        </Columns>
		        <Bands>                                
			        <ig:Band Key="NOCONFORMIDAD" DataMember="NOCONFORMIDADES_ESCALACION" DataKeyFields="PROVE_ESCALACION,NOCONFORMIDAD,PROVE,UNQAS"
                        AutoGenerateColumns="false" ShowFooter="false" Width="70%">
				        <Columns>
                            <ig:BoundDataField Key="PROVE_ESCALACION" DataFieldName="PROVE_ESCALACION" Hidden="true"></ig:BoundDataField>
                            <ig:BoundDataField Key="NOCONFORMIDAD" DataFieldName="NOCONFORMIDAD" Hidden="true"></ig:BoundDataField>
                            <ig:BoundDataField Key="INSTANCIA" DataFieldName="INSTANCIA" Hidden="true"></ig:BoundDataField>
                            <ig:BoundDataField Key="NOCONF_ESCALACION" DataFieldName="NOCONF_ESCALACION" Hidden="false" CssClass="SinSalto" Header-CssClass="SinSalto"></ig:BoundDataField>
                            <ig:BoundDataField Key="FECALTA" DataFieldName="FECALTA" Hidden="false"></ig:BoundDataField>
                            <ig:BoundDataField Key="UNQA1" DataFieldName="UNQA1" Hidden="false"></ig:BoundDataField>
                            <ig:BoundDataField Key="UNQA2" DataFieldName="UNQA2" Hidden="false"></ig:BoundDataField>
                            <ig:BoundDataField Key="UNQA3" DataFieldName="UNQA3" Hidden="false"></ig:BoundDataField>
                            <ig:BoundDataField Key="ESTADO" DataFieldName="ESTADO" Hidden="false"></ig:BoundDataField>
                            <ig:BoundDataField Key="PROVE" DataFieldName="PROVE" Hidden="true"></ig:BoundDataField>
                            <ig:BoundDataField Key="UNQAS" DataFieldName="UNQAS" Hidden="true"></ig:BoundDataField>                            
                            <ig:TemplateDataField Key="DETALLE" CssClass="SinSalto" Header-CssClass="SinSalto"></ig:TemplateDataField>
                            <ig:BoundDataField Key="COMENTARIO" DataFieldName="COMENTARIO" CssClass="SinSalto" Header-CssClass="SinSalto" Hidden="false"></ig:BoundDataField>
				        </Columns>
			        </ig:Band>            
		        </Bands>
                <Behaviors>
                    <ig:Paging Enabled="true" PagerAppearance="Top">                            
                        <PagerTemplate>
                            <div class="CabeceraBotones" style="text-align:left;">                                            
                                <div style="display:inline-block; width:40%; margin:0.5em 0em; vertical-align:middle;">
                                    <asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" style="margin-left:2em; vertical-align:bottom;" />
                                    <asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" style="margin-left:0.4em; vertical-align:bottom;" />
                                    <asp:Label ID="lblPage" runat="server" Style="margin-left:0.6em;"></asp:Label>
                                    <asp:DropDownList ID="PagerPageList" runat="server" Style="margin-left:0.4em;" onchange="return IndexChanged();"></asp:DropDownList>
                                    <asp:Label ID="lblOF" runat="server" Style="margin-left:0.6em;"></asp:Label>
                                    <asp:Label ID="lblCount" runat="server" Style="margin-left:0.5em;"></asp:Label>
                                    <asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-left:0.6em; vertical-align:bottom;" />
                                    <asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" style="margin-left:0.4em; vertical-align:bottom;" />                            
                                </div>
                                <div style="display:inline-block; width:60%; vertical-align:middle;">
                                    <div onclick="ExportarExcel()" class="botonDerechaCabecera" style="float:right;">            
                                        <asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
                                        <asp:Label runat="server" ID="lblExcel" CssClass="fntLogin2" style="line-height:2em; margin-left:3px;"></asp:Label>           
                                    </div>                                                        
                                </div>
                            </div>
                        </PagerTemplate>
                    </ig:Paging>
                    <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                    <ig:Filtering Enabled="true"></ig:Filtering>
                    <ig:Sorting SortingMode="Single" Enabled="true"></ig:Sorting>
                </Behaviors>
	        </ig:WebHierarchicalDataGrid>            
        </ContentTemplate>
    </asp:UpdatePanel>
    <ig:WebHierarchicalDataGrid runat="server" ID="whdgPanelEscalacionExcel" Width="100%"        
		AutoGenerateBands="false" AutoGenerateColumns="false" 
		DataMember="INFO_ESCALACION" DataKeyFields="ID"
        InitialDataBindDepth="1" InitialExpandDepth="0">
        <Columns>
            <ig:BoundDataField Key="ID" DataFieldName="ID" Hidden="true"></ig:BoundDataField>
            <ig:BoundDataField Key="PROVE" DataFieldName="PROVE" Hidden="true"></ig:BoundDataField>
            <ig:BoundDataField Key="SOLICITUD" DataFieldName="SOLICITUD" Hidden="true"></ig:BoundDataField>
            <ig:BoundDataField Key="UNQA" DataFieldName="UNQA" Hidden="true"></ig:BoundDataField>
            <ig:BoundDataField Key="NOCONF_ESCALACION" DataFieldName="NOCONF_ESCALACION" Hidden="true"></ig:BoundDataField>
            <ig:BoundDataField Key="INSTANCIA_ESCALACION" DataFieldName="INSTANCIA_ESCALACION" Hidden="true"></ig:BoundDataField>
            <ig:BoundDataField Key="PROVEDEN" DataFieldName="PROVEDEN" Hidden="false" CssClass="SinSalto" Header-CssClass="SinSalto"></ig:BoundDataField>
            <ig:BoundDataField Key="UNQA1" DataFieldName="UNQA1" Hidden="false"></ig:BoundDataField>
            <ig:BoundDataField Key="UNQA2" DataFieldName="UNQA2" Hidden="false"></ig:BoundDataField>
            <ig:BoundDataField Key="UNQA3" DataFieldName="UNQA3" Hidden="false"></ig:BoundDataField>
            <ig:BoundDataField Key="NIVEL_ESCALACION" DataFieldName="NIVEL_ESCALACION" Hidden="false"></ig:BoundDataField>
            <ig:BoundDataField Key="ESTADO" DataFieldName="ESTADO" Hidden="true"></ig:BoundDataField>
            <ig:BoundDataField Key="ESTADO_DEN" DataFieldName="ESTADO_DEN" Hidden="false"></ig:BoundDataField>
            <ig:BoundDataField Key="FECHA" DataFieldName="FECHA" Hidden="false"></ig:BoundDataField>
            <ig:TemplateDataField Key="NOCONF_ESCALACION_DEN" CssClass="SinSalto" Header-CssClass="SinSalto">
            </ig:TemplateDataField>
            <ig:TemplateDataField Key="APROBAR_RECHAZAR" CssClass="SinSalto" Header-CssClass="SinSalto">
            </ig:TemplateDataField>
            <ig:BoundDataField Key="FECHAHISTORICO" DataFieldName="FECACT" Hidden="false" CssClass="SinSalto" Header-CssClass="SinSalto"></ig:BoundDataField>
            <ig:BoundDataField Key="COMENTARIO" DataFieldName="COMENTARIO" Hidden="false" CssClass="SinSalto" Header-CssClass="SinSalto"></ig:BoundDataField>
		</Columns>
		<Bands>                                
			<ig:Band Key="NOCONFORMIDAD" DataMember="NOCONFORMIDADES_ESCALACION" DataKeyFields="PROVE_ESCALACION,NOCONFORMIDAD,PROVE,UNQAS"
                AutoGenerateColumns="false" ShowFooter="false" Width="70%">
				<Columns>
                    <ig:BoundDataField Key="PROVE_ESCALACION" DataFieldName="PROVE_ESCALACION" Hidden="true"></ig:BoundDataField>
                    <ig:BoundDataField Key="NOCONFORMIDAD" DataFieldName="NOCONFORMIDAD" Hidden="true"></ig:BoundDataField>
                    <ig:BoundDataField Key="INSTANCIA" DataFieldName="INSTANCIA" Hidden="true"></ig:BoundDataField>
                    <ig:BoundDataField Key="NOCONF_ESCALACION" DataFieldName="NOCONF_ESCALACION" Hidden="false" CssClass="SinSalto" Header-CssClass="SinSalto"></ig:BoundDataField>
                    <ig:BoundDataField Key="FECALTA" DataFieldName="FECALTA" Hidden="false"></ig:BoundDataField>
                    <ig:BoundDataField Key="UNQA1" DataFieldName="UNQA1" Hidden="false"></ig:BoundDataField>
                    <ig:BoundDataField Key="UNQA2" DataFieldName="UNQA2" Hidden="false"></ig:BoundDataField>
                    <ig:BoundDataField Key="UNQA3" DataFieldName="UNQA3" Hidden="false"></ig:BoundDataField>
                    <ig:BoundDataField Key="ESTADO" DataFieldName="ESTADO" Hidden="false"></ig:BoundDataField>
                    <ig:BoundDataField Key="PROVE" DataFieldName="PROVE" Hidden="true"></ig:BoundDataField>
                    <ig:BoundDataField Key="UNQAS" DataFieldName="UNQAS" Hidden="true"></ig:BoundDataField>
                    <ig:TemplateDataField Key="DETALLE" CssClass="SinSalto" Header-CssClass="SinSalto"></ig:TemplateDataField>
                    <ig:BoundDataField Key="COMENTARIO" DataFieldName="COMENTARIO" Hidden="false" CssClass="SinSalto" Header-CssClass="SinSalto"></ig:BoundDataField>
				</Columns>
			</ig:Band>            
		</Bands>
        <Behaviors>
            <ig:Sorting SortingMode="Single" Enabled="true"></ig:Sorting>
        </Behaviors>
	</ig:WebHierarchicalDataGrid>
    <asp:Button runat="server" ID="btnHiddenComentariosRechazar" style="display:none;"/>     
    <asp:Panel id="pnlComentariosRechazar" runat="server" class="popupCN" style="position:absolute;">        
        <div class="popupCN">
            <span id="lbComentariosRechazar" style="display:block; margin:1em;">DIntroduzca el motivo del rechazo:</span>
            <textarea id="txtComentarioRechazoEscalacion" rows="10" cols="40" style="display:block; margin:1em;"></textarea>
            <div style="text-align:center; margin:1em;">
                <span id="btnAceptarRechazo" class="botonRedondeado">DAceptar</span>
                <span id="btnCancelarRechazo" class="botonRedondeado">DCancelar</span>
            </div>
            <span style="display:block; margin:1em;"></span>
        </div>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpeComentariosRechazar" runat="server" PopupControlID="pnlComentariosRechazar"
        TargetControlID="btnHiddenComentariosRechazar"        
        BackgroundCssClass="modalBackground">
    </ajx:ModalPopupExtender>  
    <script type="text/javascript" language="javascript">
        var ModalComentariosRechazar='<%=mpeComentariosRechazar.ClientID%>';
    </script>

</asp:Content>
