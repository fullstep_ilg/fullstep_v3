﻿$(document).ready(function () {
    Cargar_Textos_Pantalla();
    $('#pnlComentariosRechazar').hide();   
    $('#pnlBusqueda').on('click', function () {
        $('[id$=imgCollapsePanelBusqueda]').toggle();
        $('[id$=imgExpandPanelBusqueda]').toggle();
        $('.ColorFondoTab').toggle();
    });
    
    $('#btnBuscar').on('click', function () { Buscar(); });
    $('#btnLimpiar').on('click', function () { Limpiar(); });
    $('[id$=btnAprobar]').live('click', function () {
        var grid =$find($('[id$=whdgPanelEscalacion]').attr('id'));
        var row = grid.get_gridView().get_rows().get_row(parseInt($(this).attr('ROW')));
        var iSolicitud = row.get_cellByColumnKey("SOLICITUD").get_value();
        var sProve = row.get_cellByColumnKey("PROVE").get_value();
        var unqa = row.get_cellByColumnKey("UNQA").get_value();
        var idEscalacionProveedor = row.get_cellByColumnKey("ID").get_value();

        var sref = rutaPM + 'frames.aspx?';
        sref += '&pagina=noConformidad/altaNoConformidad.aspx?TipoSolicit=' + iSolicitud + '*prove=' + sProve + '*unqa=' + unqa;
        sref += '*idEscalacionProve=' + idEscalacionProveedor + '*desdeInicio=' + rutaFS + 'QA/EscalacionProveedores/PanelEscalacionProves.aspx?';
        sref += 'prove=' + datosPantalla.prove + '**UNQA=' + datosPantalla.unqa + '**nivelEscalacion=' + datosPantalla.nivelescalacion;
        sref += '**espdte=' + datosPantalla.escalacionesPendientes + '**provebaja=' + datosPantalla.proveedoresBaja + '**pag=' + datosPantalla.pag;
        window.open(sref, "_self");
        return false;
    }); 
    $('[id$=btnRechazar]').live('click', function () {        
        idEscalacionProv = parseInt($(this).attr('ESCALACION_PROVE'));                
        $('#lbComentariosRechazar').text(TextosPantalla[5]);
        $('#btnAceptarRechazo').text(TextosPantalla[6]);
        $('#btnCancelarRechazo').text(TextosPantalla[7]);
        $('#btnAceptarRechazo').show();
        $('#txtComentarioRechazoEscalacion').val('');
        $find(ModalComentariosRechazar).show();
        return false;
    });
    $('#btnCancelarRechazo').live('click', function () {        
        $('#txtComentarioRechazoEscalacion').val('');
        $find(ModalComentariosRechazar).hide();
    });
    $('#btnAceptarRechazo').live('click', function () {        
        var comentarioRechazo = $('#txtComentarioRechazoEscalacion').val()
        $('#txtComentarioRechazoEscalacion').val('');
        $find(ModalComentariosRechazar).hide();

        var btnRechazar = $('#btnRechazarEscalacion').attr('id');
        __doPostBack(btnRechazar, JSON.stringify({                                    
            idEscalacionProveedor: idEscalacionProv,
            proveCod: datosPantalla.prove,
            UNQA: datosPantalla.unqa,
            nivelEscalacion: datosPantalla.nivelescalacion,
            escalacionPendiente: datosPantalla.escalacionesPendientes,
            proveBaja: datosPantalla.proveedoresBaja,
            pag: datosPantalla.pag,
            comentarioRechazo: comentarioRechazo            
        }));
    });    
    $('#chkEscalacionPendienteAprobar').prop('checked', (datosPantalla.escalacionesPendientes == 1 ? true : false));
    $('[id$=chkMostrarProveedoresBaja]').prop('checked', (datosPantalla.proveedoresBaja == 1 ? true : false));
    $('[id$=chkMostrarHistoricoEscalaciones]').prop('checked', (datosPantalla.historicoEscalaciones == 1 ? true : false));
    $('.ColorFondoTab').show();
});
//#region Popup Cargando...
var Redirigir=false;
Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);
function beginReq(sender, args) {
    if (String(args._postBackElement.id).search(/btnAprobar/i) != -1) Redirigir = true;
    MostrarCargando();
};
function endReq(sender, args) {
    if (Redirigir) Redirigir = false;
    else OcultarCargando();
};
function OcultarCargando() {
    var modalprog = $find(ModalProgress);
    if (modalprog) modalprog.hide();
};
//#endregion
//#region Paginación
$('[id$=ImgBtnFirst]').live('click', function () { FirstPage(); });
$('[id$=ImgBtnPrev]').live('click', function () { PrevPage(); });
$('[id$=ImgBtnNext]').live('click', function () { NextPage(); });
$('[id$=ImgBtnLast]').live('click', function () { LastPage(); });
function IndexChanged() {
    var dropdownlist = $('[id$=PagerPageList]')[0];
    Pager(dropdownlist.selectedIndex);
}
function FirstPage() {
    Pager(0);
}
function PrevPage() {
    var dropdownlist = $('[id$=PagerPageList]')[0];
    if (dropdownlist.selectedIndex - 1 >= 0) {
        Pager(dropdownlist.selectedIndex - 1);
    };
};
function NextPage() {
    var dropdownlist = $('[id$=PagerPageList]')[0];
    if (dropdownlist.selectedIndex + 1 <= dropdownlist.length - 1) Pager(dropdownlist.selectedIndex + 1);    
};
function LastPage() {
    var dropdownlist = $('[id$=PagerPageList]')[0];
    Pager(dropdownlist.length - 1);
};
function Pager(page) {    
    var dropdownlist = $('[id$=PagerPageList]')[0];
    var btnPaginador = $('[id$=btnPaginador]').attr('id');
    dropdownlist.options[page].selected = true;
    
    __doPostBack(btnPaginador, JSON.stringify({
        pagina: ($('[id$=PagerPageList]').val() == '' ? 1 : $('[id$=PagerPageList]').val()),
        proveedoresBaja: ($('[id$=chkMostrarProveedoresBaja]').prop('checked')),
        historicoEscalaciones: ($('[id$=chkMostrarHistoricoEscalaciones]').prop('checked'))
    }));
};
//#endregion
var idEscalacionProv;
var codProveBuscador = '';
function Cargar_Textos_Pantalla() {
    $('#lblEscalacionPendienteAprobar').text(TextosPantalla[0]);
    $('#lblMostrarProveedoresBaja').text(TextosPantalla[1]);    
    $('#btnBuscar').text(TextosPantalla[2]);
    $('#btnLimpiar').text(TextosPantalla[3]);
    $('#lblMostrarHistoricoEscalaciones').text(TextosPantalla[4]);
    $('#lbComentariosRechazar').text(TextosPantalla[5]);
    $('#btnAceptarRechazo').text(TextosPantalla[6]);
    $('#btnCancelarRechazo').text(TextosPantalla[7]);
};
function txtProveedor_KeyUp(oTextBox) {
    if (oTextBox.value == "") Proveedor_Seleccionado("", "");
};
function txtProveedor_TextChanged(oTextBox) {
    if (oTextBox.value != "") ComprobarProveedor(oTextBox.value);
};
function ComprobarProveedor(sProveCod) {
    var params = {
        prefixText: sProveCod,
        count: 1,
        contextKey: ''
    };
    var respuesta;
    $.when($.ajax({
        type: "POST",
        url: ruta + '/App_Pages/_Common/App_Services/AutoComplete.asmx/GetProveedoresPanelEscalacion',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        if (msg.d.length == 0) Proveedor_Seleccionado('', '');
        else {
            iProve = $.parseJSON(msg.d[0]);
            Proveedor_Seleccionado(iProve.Second, iProve.First);
        };
    });
};
function Proveedor_Item_Seleccionado(sender,e) {
    Proveedor_Seleccionado(e._value, e._value + " - " + e._text.split("-")[1]);
}
function Proveedor_Seleccionado(codProve, denProve) {
    codProveBuscador = codProve;
    $('#hProveedor').val(codProveBuscador);
    if (codProveBuscador != "") $('#txtProveedor').val(denProve);
};
//<summary>Limpia las variables de contexto para el extender del código del proveedor</summary>  
//<remarks>Llamada desde:</remarks>
function ResetProveContextKey() {
    codProveBuscador = "";
};
function SetProveContextKey() {   
    var oExtender = $find("txtProveedor_AutoCompleteExtender");
    oExtender.set_contextKey(codProveBuscador);
};
function AbrirBuscadorProveedor() {
    window.open(rutaFS + '_Common/BuscadorProveedores.aspx?PM=false&desde=VisorCert&IDCONTROL=' + $('#txtProveedor').attr('id') + '&IdControlHid=' + $('#hProveedor').attr('id') + '&Tipo=3&Baja=' + ($('[id$=chkMostrarProveedoresBaja]').prop('checked')? 1 : 0), '_blank', 'width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes');
    return false;
};
function Buscar() {
    var wDropDownUNQA = $find($('[id$=wddUNQA]').attr('id'));
    var wDropDownNivelesEscalacion = $find($('[id$=wddNivelEscalacion]').attr('id'));
    var unqas = wDropDownUNQA.get_selectedItems();
    var nivelEscalacion = wDropDownNivelesEscalacion.get_selectedItem();
    var btnBuscador = $('#btnBuscador').attr('id');
    
    datosPantalla.prove = $('#hProveedor').val();
    datosPantalla.unqa = (unqas==null?'':$.map(unqas, function (x) { return x.get_value(); }).join(','));
    datosPantalla.nivelescalacion = (nivelEscalacion==null?0:nivelEscalacion.get_value());
    datosPantalla.escalacionesPendientes = $('#chkEscalacionPendienteAprobar').prop('checked');
    datosPantalla.proveedoresBaja = $('[id$=chkMostrarProveedoresBaja]').prop('checked');
    datosPantalla.historicoEscalaciones = $('[id$=chkMostrarHistoricoEscalaciones]').prop('checked');
    datosPantalla.pag = $('[id$=PagerPageList]').val();
    
    __doPostBack(btnBuscador, JSON.stringify({
        proveCod: datosPantalla.prove,
        UNQA: datosPantalla.unqa,
        nivelEscalacion: datosPantalla.nivelescalacion,
        escalacionPendiente: datosPantalla.escalacionesPendientes,
        proveBaja: datosPantalla.proveedoresBaja,
        historicoEscalaciones: datosPantalla.historicoEscalaciones,
        pag : datosPantalla.pag
    }));
};
function Limpiar() {
    $('#hProveedor').val('');
    $('#txtProveedor').val('');
    var wDropDownUNQA = $find($('[id$=wddUNQA]').attr('id'));
    var wDropDownNivelesEscalacion = $find($('[id$=wddNivelEscalacion]').attr('id'));
    
    $.each(wDropDownUNQA.get_selectedItems(), function () {
        this.unselect();
    });
    
    wDropDownUNQA.set_currentValue("", true);
    if (wDropDownNivelesEscalacion.get_selectedItem() !== null) {
        wDropDownNivelesEscalacion.set_selectedItemIndex(0);
        wDropDownNivelesEscalacion.set_currentValue("", true);
    }
    $('#chkEscalacionPendienteAprobar').prop('checked', true);
    $('[id$=chkMostrarProveedoresBaja]').prop('checked', false);
    $('[id$=chkMostrarHistoricoEscalaciones]').prop('checked', false);

};
function whdgPanelEscalacion_Click(sender, e) {    
    var grid = $find($('[id$=whdgPanelEscalacion]').attr('id'));
    var gridView = grid.get_gridView();

    if (e.get_type() == "cell") {
        if (e.get_item().get_column().get_key() == "NOCONF_ESCALACION_DEN" && e.get_item().get_row().get_cellByColumnKey('INSTANCIA_ESCALACION').get_value() == '') return false;
        
        var wDropDownUNQA = $find($('[id$=wddUNQA]').attr('id'));
        var wDropDownNivelesEscalacion = $find($('[id$=wddNivelEscalacion]').attr('id'));
        var unqas = wDropDownUNQA.get_selectedItems();
        var nivelEscalacion = wDropDownNivelesEscalacion.get_selectedItem();
        var svolver;
        
        svolver = '*volver=' + rutaQA + 'EscalacionProveedores/PanelEscalacionProves.aspx?';
        svolver += 'prove=' + datosPantalla.prove + '**UNQA=' + datosPantalla.unqa + '**nivelEscalacion=' + datosPantalla.nivelescalacion;
        svolver += '**espdte=' + datosPantalla.escalacionesPendientes + '**provebaja=' + datosPantalla.proveedoresBaja + '**historicoEscalaciones=' + datosPantalla.historicoEscalaciones +  '**pag=' + datosPantalla.pag;
        
        if (e.get_item().get_column().get_key() == "NOCONF_ESCALACION_DEN") {
            var FechaLocal;
            FechaLocal = new Date();

            sref = rutaPM + 'frames.aspx?pagina=noconformidad/detalleNoConformidad.aspx?NoConformidad=' + e.get_item().get_row().get_cellByColumnKey('NOCONF_ESCALACION').get_value();
            sref += '*Instancia='+ e.get_item().get_row().get_cellByColumnKey('INSTANCIA_ESCALACION').get_value() +'*otz=' + FechaLocal.getTimezoneOffset();
            window.open(sref + svolver, "_self");
        }

        if (e.get_item().get_column().get_key() == "DETALLE") {
            if (e.get_item().get_row().get_cellByColumnKey('NOCONFORMIDAD').get_value() != 0) {
                var FechaLocal;
                FechaLocal = new Date();

                sref = rutaPM + 'frames.aspx?pagina=noconformidad/detalleNoConformidad.aspx?NoConformidad=' + e.get_item().get_row().get_cellByColumnKey('NOCONFORMIDAD').get_value();
                sref += '*Instancia=' + e.get_item().get_row().get_cellByColumnKey('INSTANCIA').get_value() + '*otz=' + FechaLocal.getTimezoneOffset();
                window.open(sref + svolver, "_self");
            }            
        }
        
        if (e.get_item().get_column().get_key() == "COMENTARIO") {
            if (e.get_item().get_row().get_cellByColumnKey('COMENTARIO').get_value() != "") {                                
                var comentarioRechazo = e.get_item().get_row().get_cellByColumnKey('COMENTARIO').get_value()
                $('#lbComentariosRechazar').text(TextosPantalla[8]);
                $('#btnCancelarRechazo').text(TextosPantalla[9]);
                $('#btnAceptarRechazo').hide();
                $('#txtComentarioRechazoEscalacion').val(comentarioRechazo);
                $find(ModalComentariosRechazar).show();                
            }            
        }

    };
};
function ExportarExcel() {
    var wDropDownUNQA = $find($('[id$=wddUNQA]').attr('id'));
    var wDropDownNivelesEscalacion = $find($('[id$=wddNivelEscalacion]').attr('id'));
    var unqas = wDropDownUNQA.get_selectedItems();
    var nivelEscalacion = wDropDownNivelesEscalacion.get_selectedItem();    
    var btnExcel = $('#btnExcel').attr('id');
    datosPantalla.prove = $('#hProveedor').val();
    datosPantalla.unqa = (unqas == null ? '' : $.map(unqas, function (x) { return x.get_value(); }).join(','));
    datosPantalla.nivelescalacion = (nivelEscalacion == null ? 0 : nivelEscalacion.get_value());
    datosPantalla.escalacionesPendientes = $('#chkEscalacionPendienteAprobar').prop('checked');
    datosPantalla.proveedoresBaja = $('[id$=chkMostrarProveedoresBaja]').prop('checked');
    datosPantalla.historicoEscalaciones = $('[id$=chkMostrarHistoricoEscalaciones]').prop('checked');
    datosPantalla.pag = $('[id$=PagerPageList]').val();

    __doPostBack(btnExcel, JSON.stringify({
        proveCod: datosPantalla.prove,
        UNQA: datosPantalla.unqa,
        nivelEscalacion: datosPantalla.nivelescalacion,
        escalacionPendiente: datosPantalla.escalacionesPendientes,
        proveBaja: datosPantalla.proveedoresBaja,
        historicoEscalaciones: datosPantalla.historicoEscalaciones,
        pag: datosPantalla.pag
    }));    
};