﻿Imports System.Web.Script.Serialization
Imports Infragistics.Web.UI.GridControls

Public Class PanelEscalacionProves
    Inherits FSNPage
    Private _codProve As String
    Private _UNQA As String
    Private _nivelEscalacion As Integer
    Private _escalacionesPendiente As Boolean = True
    Private _provesBaja As Boolean
    Private _historicoEscalaciones As Boolean
    Public ReadOnly Property dsPanelEscalacion() As DataSet
        Get
            Dim _dsPanelEscalacion As DataSet
            Dim _oResultado As Object = CType(Cache("oPanelEscalacion_" & Usuario.Cod), Object)
            If _oResultado Is Nothing Then
                Dim oNivelesEscalacion As FSNServer.Escalaciones = FSNServer.Get_Object(GetType(FSNServer.Escalaciones))
                _oResultado = oNivelesEscalacion.Panel_Escalacion_Get_Data(FSNUser.Cod, _codProve, _UNQA, _nivelEscalacion, _escalacionesPendiente, _provesBaja, FSNUser.Idioma, _historicoEscalaciones)
                InsertarEnCache("oPanelEscalacion_" & FSNUser.Cod, _oResultado, CacheItemPriority.BelowNormal)
            End If
                _dsPanelEscalacion = _oResultado(0)
            Return _dsPanelEscalacion
        End Get
    End Property
    Public ReadOnly Property maxNivelUNQA_NivelEscalacion As Integer
        Get
            'Columnas Fijas-> Tres niveles de unqa
            Dim _maxNivelUNQA_NivelEscalacion As Integer = 3
            Return _maxNivelUNQA_NivelEscalacion
        End Get
    End Property
    Public ReadOnly Property maxNivelUNQA_NoConformidades As Integer
        Get
            'Columnas Fijas-> Tres niveles de unqa
            Dim _maxNivelUNQA_NoConformidades As Integer = 3
            Return _maxNivelUNQA_NoConformidades
        End Get
    End Property
    Public ReadOnly Property PermisosAprobarRechazarEscalacion_UNQA() As Dictionary(Of Integer, List(Of Integer))
        Get
            Dim dPermisosEscalacionUNQA As Dictionary(Of Integer, List(Of Integer)) = Cache("dPermisosEscalacion_UNQA_" & Usuario.Cod)
            If dPermisosEscalacionUNQA Is Nothing Then
                Dim oPanelEscalacion As FSNServer.Escalaciones = FSNServer.Get_Object(GetType(FSNServer.Escalaciones))
                dPermisosEscalacionUNQA = oPanelEscalacion.Panel_Escalacion_Get_PermisosAprobarRechazar_UNQA(Usuario.Cod)
                InsertarEnCache("dPermisosEscalacion_UNQA_" & FSNUser.Cod, dPermisosEscalacionUNQA)
            End If
            Return dPermisosEscalacionUNQA
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.PanelEscalacionProveedores

        CType(whdgPanelEscalacion.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgExcel"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/Images/Excel.png"
        Dim serializer As New JavaScriptSerializer
        If IsPostBack Then
            Select Case Request("__EVENTTARGET")
                Case btnPaginador.ClientID
                    Dim filtros As Object = serializer.Deserialize(Of Object)(Request("__EVENTARGUMENT"))
                    _historicoEscalaciones = filtros("historicoEscalaciones")
                    _provesBaja = filtros("proveedoresBaja")
                    Dim pagina As Integer = filtros("pagina")

                    Paginador((dsPanelEscalacion.Tables("INFO_ESCALACION").Rows.Count \ ConfigurationManager.AppSettings("registrosPaginacion")) +
                          If(dsPanelEscalacion.Tables("INFO_ESCALACION").Rows.Count Mod ConfigurationManager.AppSettings("registrosPaginacion") = 0, 0, 1), pagina)
                    whdgPanelEscalacion.GridView.Behaviors.Paging.PageIndex = CInt(pagina) - 1
                Case btnBuscador.ClientID
                    'recuperamos los valores de filtrado
                    Dim filtros As Object = serializer.Deserialize(Of Object)(Request("__EVENTARGUMENT"))
                    _codProve = filtros("proveCod")
                    _UNQA = filtros("UNQA")
                    _nivelEscalacion = filtros("nivelEscalacion")
                    _escalacionesPendiente = filtros("escalacionPendiente")
                    _provesBaja = filtros("proveBaja")
                    _historicoEscalaciones = filtros("historicoEscalaciones")
                    Cache.Remove("oPanelEscalacion_" & Usuario.Cod)

                    Paginador((dsPanelEscalacion.Tables("INFO_ESCALACION").Rows.Count \ ConfigurationManager.AppSettings("registrosPaginacion")) +
                            If(dsPanelEscalacion.Tables("INFO_ESCALACION").Rows.Count Mod ConfigurationManager.AppSettings("registrosPaginacion") = 0, 0, 1), 1)
                    whdgPanelEscalacion.GridView.Behaviors.Paging.PageIndex = 0

                    With whdgPanelEscalacion
                        .Rows.Clear()
                        .GridView.Rows.Clear()
                    End With
                Case btnRechazarEscalacion.ClientID
                    Dim oEscalacionProve As FSNServer.Escalaciones = FSNServer.Get_Object(GetType(FSNServer.Escalaciones))
                    Dim datos As Object = serializer.Deserialize(Of Object)(Request("__EVENTARGUMENT"))
                    _codProve = datos("proveCod")
                    _UNQA = datos("UNQA")
                    _nivelEscalacion = datos("nivelEscalacion")
                    _escalacionesPendiente = datos("escalacionPendiente")
                    _provesBaja = datos("proveBaja")
                    oEscalacionProve.Panel_Escalacion_Rechazar_Escalacion(datos("idEscalacionProveedor"), datos("comentarioRechazo"))
                    Cache.Remove("oPanelEscalacion_" & Usuario.Cod)
                    With whdgPanelEscalacion
                        .Rows.Clear()
                        .GridView.Rows.Clear()
                    End With
                Case btnExcel.ClientID
                    Dim filtros As Object = serializer.Deserialize(Of Object)(Request("__EVENTARGUMENT"))
                    _codProve = filtros("proveCod")
                    _UNQA = filtros("UNQA")
                    _nivelEscalacion = filtros("nivelEscalacion")
                    _escalacionesPendiente = filtros("escalacionPendiente")
                    _provesBaja = filtros("proveBaja")
                    _historicoEscalaciones = filtros("historicoEscalaciones")
                    Cache.Remove("oPanelEscalacion_" & Usuario.Cod)
                    ConfigurarGrid_WidthText(True)

                    With whdgPanelEscalacionExcel
                        .DataSource = dsPanelEscalacion
                        .GridView.DataSource = .DataSource
                        .DataBind()
                    End With

                    If _historicoEscalaciones Then
                        whdgPanelEscalacionExcel.GridView.Columns("APROBAR_RECHAZAR").Hidden = True
                        whdgPanelEscalacionExcel.GridView.Columns("FECHAHISTORICO").Hidden = False
                        whdgPanelEscalacionExcel.GridView.Columns("COMENTARIO").Hidden = False
                        whdgPanelEscalacionExcel.Bands(0).Columns("COMENTARIO").Hidden = False
                    Else
                        whdgPanelEscalacionExcel.GridView.Columns("APROBAR_RECHAZAR").Hidden = False
                        whdgPanelEscalacionExcel.GridView.Columns("FECHAHISTORICO").Hidden = True
                        whdgPanelEscalacionExcel.GridView.Columns("COMENTARIO").Hidden = True
                        whdgPanelEscalacionExcel.Bands(0).Columns("COMENTARIO").Hidden = True
                    End If

                    ExportarExcel()
                    Exit Sub
                Case Else
                    _historicoEscalaciones = chkMostrarHistoricoEscalaciones.Checked
                    _provesBaja = chkMostrarProveedoresBaja.Checked
            End Select

            ConfigurarGrid_WidthText(True)
        Else
            Cargar_Textos_Pantalla()
            Dim mMaster = CType(Me.Master, FSNWeb.Menu)
            mMaster.Seleccionar("Calidad", "PanelEscalacionProveedores")

            Cache.Remove("oPanelEscalacion_" & Usuario.Cod)
            Cache.Remove("dPermisosEscalacion_UNQA_" & FSNUser.Cod)

            _escalacionesPendiente = True
            _provesBaja = False
            If Not String.IsNullOrEmpty(Request.QueryString("prove")) Then
                _codProve = Request.QueryString("prove")
                hProveedor.Value = _codProve
                Dim oProve As FSNServer.Proveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                oProve.Cod = _codProve
                oProve.Load(FSNUser.Idioma.ToString, SoloTablaProveedor:=True)
                txtProveedor.Text = oProve.Den
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("UNQA")) Then _UNQA = Request.QueryString("UNQA")
            If Not String.IsNullOrEmpty(Request.QueryString("nivelEscalacion")) Then _nivelEscalacion = Request.QueryString("nivelEscalacion")
            If Not String.IsNullOrEmpty(Request.QueryString("espdte")) Then _escalacionesPendiente = CType(Request.QueryString("espdte"), Boolean)
            If Not String.IsNullOrEmpty(Request.QueryString("provebaja")) Then _provesBaja = Request.QueryString("provebaja")
            If Not String.IsNullOrEmpty(Request.QueryString("historicoEscalaciones")) Then _historicoEscalaciones = Request.QueryString("historicoEscalaciones")
            If Not String.IsNullOrEmpty(Request.QueryString("pag")) Then whdgPanelEscalacion.GridView.Behaviors.Paging.PageIndex = CType(Request.QueryString("pag"), Integer) - 1

            Dim oPanelEscalacion As FSNServer.Escalaciones = FSNServer.Get_Object(GetType(FSNServer.Escalaciones))
            With wddUNQA
                .TextField = "DEN"
                .ValueField = "ID"
                .DataSource = oPanelEscalacion.Panel_Escalacion_Get_UNQAs(FSNUser.Cod, FSNUser.Idioma.ToString)
                .DataBind()
                If Not String.IsNullOrEmpty(_UNQA) Then
                    For Each item As String In Split(_UNQA, ",")
                        .Items.FindItemByValue(item).Selected = True
                    Next
                End If
            End With
            With wddNivelEscalacion
                .TextField = "DEN"
                .ValueField = "NIVEL"
                .DataSource = oPanelEscalacion.Panel_Escalacion_Get_NivelesEscalacion(FSNUser.Idioma.ToString, ModuloIdioma, 5)
                .DataBind()
                .SelectedValue = _nivelEscalacion
            End With

            Dim ModalProgressClientID As String = CType(Master.Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalClientID", "var ModalProgress='" & ModalProgressClientID & "';", True)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaQA", "var rutaQA='" & ConfigurationManager.AppSettings("rutaQA2008") & "';", True)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "datosPantalla", "var datosPantalla=" & serializer.Serialize(New With {.prove = NothingToDBNull(_codProve).ToString,
                                                                                                                                            .unqa = NothingToDBNull(_UNQA).ToString,
                                                                                                                                            .nivelescalacion = _nivelEscalacion,
                                                                                                                                            .escalacionesPendientes = Convert.ToInt32(_escalacionesPendiente),
                                                                                                                                            .proveedoresBaja = Convert.ToInt32(_provesBaja),
                                                                                                                                            .historicoEscalaciones = Convert.ToInt32(_historicoEscalaciones),
                                                                                                                                            .pag = whdgPanelEscalacion.GridView.Behaviors.Paging.PageIndex + 1}) & ";", True)

            Paginador((dsPanelEscalacion.Tables("INFO_ESCALACION").Rows.Count \ ConfigurationManager.AppSettings("registrosPaginacion")) +
                              If(dsPanelEscalacion.Tables("INFO_ESCALACION").Rows.Count Mod ConfigurationManager.AppSettings("registrosPaginacion") = 0, 0, 1), 1)
            whdgPanelEscalacion.GridView.Behaviors.Paging.PageSize = ConfigurationManager.AppSettings("registrosPaginacion")

            ConfigurarGrid_WidthText(False)
        End If

        With whdgPanelEscalacion
            .DataSource = dsPanelEscalacion
            .GridView.DataSource = .DataSource
            .DataBind()
        End With

        upGrid.Update()
    End Sub
    Public Sub Cargar_Textos_Pantalla()
        FSNPageHeader.TituloCabecera = Textos(0)
        FSNPageHeader.UrlImagenCabecera = ConfigurationManager.AppSettings("ruta") & "Images\PanelEscalacionProves.png"

        lblPanelBusqueda.Text = Textos(1)
        lblProveedor.Text = Textos(2) & ": "
        lblUNQA.Text = Textos(3) & ": "
        lblNivelEscalacion.Text = Textos(4) & ": "

        With whdgPanelEscalacion
            CType(.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), Image).ToolTip = Textos(19)
            CType(.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), Image).ToolTip = Textos(20)
            CType(.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image).ToolTip = Textos(21)
            CType(.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image).ToolTip = Textos(22)
            CType(.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(23)
            CType(.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(24)
            CType(.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblExcel"), Label).Text = Textos(25)
        End With

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
            Dim sVariableJavascriptTextosPantalla As String = "var TextosPantalla = new Array();"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[0]='" & JSText(Textos(5)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[1]='" & JSText(Textos(6)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[2]='" & JSText(Textos(7)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[3]='" & JSText(Textos(8)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[4]='" & JSText(Textos(40)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[5]='" & JSText(Textos(37)) & ":" & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[6]='" & JSText(Textos(38)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[7]='" & JSText(Textos(39)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[8]='" & JSText(Textos(42)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[9]='" & JSText(Textos(43)) & "';"

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextosPantalla, True)
        End If
    End Sub
    Private Sub Paginador(ByVal _pageCount As Integer, ByVal _pageNumber As Integer)
        Dim pagerList As DropDownList = DirectCast(whdgPanelEscalacion.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
        pagerList.Items.Clear()
        If _pageCount = 0 Then pagerList.Items.Add("")
        For i As Integer = 1 To _pageCount
            pagerList.Items.Add(i.ToString())
        Next
        CType(whdgPanelEscalacion.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = _pageCount
        If _pageCount > 0 Then pagerList.SelectedIndex = _pageNumber - 1
        Dim Desactivado As Boolean = (_pageNumber = 1)
        With CType(whdgPanelEscalacion.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        With CType(whdgPanelEscalacion.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        Desactivado = (_pageCount = 1 OrElse _pageNumber = _pageCount)
        With CType(whdgPanelEscalacion.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        With CType(whdgPanelEscalacion.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
    End Sub
    Private Sub ConfigurarGrid_WidthText(ByVal SoloText As Boolean)
        Dim columnasGrid As Infragistics.Web.UI.GridControls.GridFieldCollection = whdgPanelEscalacion.Columns
        Dim columnasGridExcel As Infragistics.Web.UI.GridControls.GridFieldCollection = whdgPanelEscalacionExcel.Columns
        Dim campoGrid As BoundDataField
        Dim campoGridTemplate As TemplateDataField

        campoGrid = columnasGrid("PROVEDEN")
        With campoGrid
            If Not SoloText Then .Width = Unit.Pixel(420)
            .Header.CssClass = "SinSalto"
            .Header.Text = Textos(2)
        End With

        For i As Integer = 1 To maxNivelUNQA_NivelEscalacion
            campoGrid = columnasGrid("UNQA" & i)
            If Not SoloText Then campoGrid.Width = If(maxNivelUNQA_NivelEscalacion = 1, Unit.Pixel(160), Unit.Pixel(80))
            campoGrid.Header.Text = If(maxNivelUNQA_NivelEscalacion = 1, Textos(3), Textos(32) & " " & i)
        Next

        campoGrid = columnasGrid("NIVEL_ESCALACION")
        With campoGrid
            If Not SoloText Then .Width = Unit.Pixel(120)
            .Header.Text = Textos(4)
        End With

        campoGrid = columnasGrid("ESTADO_DEN")
        With campoGrid
            If Not SoloText Then .Width = Unit.Pixel(180)
            .Header.Text = Textos(9)
        End With

        campoGrid = columnasGrid("FECHA")
        With campoGrid
            If Not SoloText Then .Width = Unit.Pixel(100)
            .Header.Text = Textos(10)
        End With

        campoGridTemplate = columnasGrid("NOCONF_ESCALACION_DEN")
        With campoGridTemplate
            .Header.Text = Textos(11)
            .ItemTemplate = New CustomItemTemplate_NOCONF_ESCALACION_DEN
        End With

        campoGridTemplate = columnasGrid("APROBAR_RECHAZAR")
        With campoGridTemplate
            .ItemTemplate = New CustomItemTemplate_APROBAR_RECHAZAR
        End With

        campoGrid = columnasGrid("FECHAHISTORICO")
        With campoGrid
            .Width = Unit.Pixel(100)
            .Header.Text = Textos(41)
        End With

        campoGrid = columnasGrid("COMENTARIO")
        With campoGrid
            .Width = Unit.Pixel(100)
            .Header.Text = Textos(42)
        End With

        campoGrid = whdgPanelEscalacion.Bands(0).Columns("COMENTARIO")
        With campoGrid
            .Width = Unit.Pixel(100)
            .Header.Text = Textos(42)
        End With

        If _historicoEscalaciones Then
            whdgPanelEscalacion.GridView.Columns("APROBAR_RECHAZAR").Hidden = True
            whdgPanelEscalacion.GridView.Columns("FECHAHISTORICO").Hidden = False
            whdgPanelEscalacion.GridView.Columns("COMENTARIO").Hidden = False
            whdgPanelEscalacion.Bands(0).Columns("COMENTARIO").Hidden = False
        Else
            whdgPanelEscalacion.GridView.Columns("APROBAR_RECHAZAR").Hidden = False
            whdgPanelEscalacion.GridView.Columns("FECHAHISTORICO").Hidden = True
            whdgPanelEscalacion.GridView.Columns("COMENTARIO").Hidden = True
            whdgPanelEscalacion.Bands(0).Columns("COMENTARIO").Hidden = True
        End If

        For i As Integer = 1 To maxNivelUNQA_NoConformidades
            campoGrid = whdgPanelEscalacion.Bands(0).Columns("UNQA" & i)
            If Not SoloText Then campoGrid.Width = If(maxNivelUNQA_NoConformidades = 1, Unit.Pixel(160), Unit.Pixel(80))
            campoGrid.Header.Text = If(maxNivelUNQA_NoConformidades = 1, Textos(3), Textos(32) & " " & i)
        Next

        campoGrid = whdgPanelEscalacion.Bands(0).Columns("ESTADO")
        With campoGrid
            If Not SoloText Then .Width = Unit.Pixel(180)
            .Header.Text = Textos(9)
        End With

        campoGridTemplate = whdgPanelEscalacion.Bands(0).Columns("DETALLE")
        With campoGridTemplate
            If Not SoloText Then .Width = Unit.Pixel(100)
            .ItemTemplate = New CustomItemTemplate_DETALLE
        End With

        '-------------

        campoGrid = columnasGridExcel("PROVEDEN")
        With campoGrid
            .Header.CssClass = "SinSalto"
            .Header.Text = Textos(2)
        End With

        For i As Integer = 1 To maxNivelUNQA_NivelEscalacion
            campoGrid = columnasGridExcel("UNQA" & i)
            campoGrid.Width = If(maxNivelUNQA_NivelEscalacion = 1, Unit.Pixel(160), Unit.Pixel(80))
            campoGrid.Header.Text = If(maxNivelUNQA_NivelEscalacion = 1, Textos(3), Textos(32) & " " & i)
        Next

        campoGrid = columnasGridExcel("NIVEL_ESCALACION")
        With campoGrid
            .Width = Unit.Pixel(120)
            .Header.Text = Textos(4)
        End With

        campoGrid = columnasGridExcel("ESTADO_DEN")
        With campoGrid
            .Width = Unit.Pixel(180)
            .Header.Text = Textos(9)
        End With

        campoGrid = columnasGridExcel("FECHA")
        With campoGrid
            .Width = Unit.Pixel(100)
            .Header.Text = Textos(10)
        End With

        campoGridTemplate = columnasGridExcel("NOCONF_ESCALACION_DEN")
        With campoGridTemplate
            .Header.Text = Textos(11)
            .ItemTemplate = New CustomItemTemplate_NOCONF_ESCALACION_DEN
        End With

        campoGridTemplate = columnasGridExcel("APROBAR_RECHAZAR")
        With campoGridTemplate
            .ItemTemplate = New CustomItemTemplate_APROBAR_RECHAZAR
        End With

        campoGrid = whdgPanelEscalacionExcel.Bands(0).Columns("FECALTA")
        With campoGrid
            .Width = Unit.Pixel(100)
            .Header.Text = Textos(17)
        End With

        campoGrid = columnasGridExcel("FECHAHISTORICO")
        With campoGrid
            .Width = Unit.Pixel(100)
            .Header.Text = Textos(41)
        End With

        campoGrid = columnasGridExcel("COMENTARIO")
        With campoGrid
            .Width = Unit.Pixel(100)
            .Header.Text = Textos(42)
        End With

        campoGrid = whdgPanelEscalacionExcel.Bands(0).Columns("COMENTARIO")
        With campoGrid
            .Width = Unit.Pixel(100)
            .Header.Text = Textos(42)
        End With

        For i As Integer = 1 To maxNivelUNQA_NoConformidades
            campoGrid = whdgPanelEscalacionExcel.Bands(0).Columns("UNQA" & i)
            campoGrid.Width = If(maxNivelUNQA_NoConformidades = 1, Unit.Pixel(160), Unit.Pixel(80))
            campoGrid.Header.Text = If(maxNivelUNQA_NoConformidades = 1, Textos(3), Textos(32) & " " & i)
        Next

        campoGrid = whdgPanelEscalacionExcel.Bands(0).Columns("ESTADO")
        With campoGrid
            .Width = Unit.Pixel(180)
            .Header.Text = Textos(9)
        End With

        campoGridTemplate = whdgPanelEscalacionExcel.Bands(0).Columns("DETALLE")
        With campoGridTemplate
            .Width = Unit.Pixel(100)
            .ItemTemplate = New CustomItemTemplate_DETALLE
        End With
    End Sub
    Private Sub whdgPanelEscalacion_InitializeRow(sender As Object, e As RowEventArgs) Handles whdgPanelEscalacion.InitializeRow
        Dim items As Infragistics.Web.UI.GridControls.GridRecordItemCollection = e.Row.Items
        Dim nivel As Integer = CType(e.Row, ContainerGridRecord).Level

        If nivel = 0 Then
            e.Row.Items.FindItemByKey("PROVEDEN").Tooltip = e.Row.DataItem.Item.Row.Item("PROVEDEN")
            Dim tienePermiso As Boolean = False
            If IsDBNull(e.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION")) AndAlso e.Row.DataItem.Item.Row.Item("ESTADO") = 0 Then
                Select Case e.Row.DataItem.Item.Row.Item("NIVEL_ESCALACION")
                    Case 1
                        tienePermiso = PermisosAprobarRechazarEscalacion_UNQA(PermisoAprobarRechazar_Escalacion_UNQA.Nivel_1).Contains(e.Row.DataItem.Item.Row.Item("UNQA"))
                    Case 2
                        tienePermiso = PermisosAprobarRechazarEscalacion_UNQA(PermisoAprobarRechazar_Escalacion_UNQA.Nivel_2).Contains(e.Row.DataItem.Item.Row.Item("UNQA"))
                    Case 3
                        tienePermiso = PermisosAprobarRechazarEscalacion_UNQA(PermisoAprobarRechazar_Escalacion_UNQA.Nivel_3).Contains(e.Row.DataItem.Item.Row.Item("UNQA"))
                    Case Else
                        tienePermiso = PermisosAprobarRechazarEscalacion_UNQA(PermisoAprobarRechazar_Escalacion_UNQA.Nivel_4).Contains(e.Row.DataItem.Item.Row.Item("UNQA"))
                End Select
            End If

            If tienePermiso Then
                With CType(e.Row.Items.FindItemByKey("APROBAR_RECHAZAR").FindControl("btnAprobar"), System.Web.UI.WebControls.Button)
                    .Text = Textos(12)
                    .Attributes.Add("ROW", e.Row.Index)
                    .Style.Item("display") = "inline-block"
                End With
                With CType(e.Row.Items.FindItemByKey("APROBAR_RECHAZAR").FindControl("btnRechazar"), System.Web.UI.WebControls.Button)
                    .Text = Textos(13)
                    .Attributes.Add("ESCALACION_PROVE", e.Row.DataItem.Item.Row("ID"))
                    .Style.Item("display") = "inline-block"
                End With
            Else
                CType(e.Row.Items.FindItemByKey("APROBAR_RECHAZAR").FindControl("btnAprobar"), System.Web.UI.WebControls.Button).Style.Item("display") = "none"
                CType(e.Row.Items.FindItemByKey("APROBAR_RECHAZAR").FindControl("btnRechazar"), System.Web.UI.WebControls.Button).Style.Item("display") = "none"
            End If

            Select Case DBNullToInteger(e.Row.DataItem.Item.Row.Item("ESTADO"))
                Case 1
                    e.Row.Items.FindItemByKey("ESTADO").CssClass = "nivelEscalacionEnCurso"
                Case 2
                    e.Row.Items.FindItemByKey("ESTADO").CssClass = ""
                Case Else
                    e.Row.Items.FindItemByKey("ESTADO").CssClass = "nivelEscalacionPendiente"
                    e.Row.Items.FindItemByKey("NIVEL_ESCALACION").CssClass = "nivelEscalacionPendiente"
            End Select
            If IsDBNull(e.Row.DataItem.Item.Row.Item("ESTADO_NOCONF")) Then
                CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = ""
            Else
                Select Case DBNullToInteger(e.Row.DataItem.Item.Row.Item("ESTADO_NOCONF"))
                    Case TipoEstadoNoConformidad.Guardada
                        CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = e.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(34)
                    Case TipoEstadoNoConformidad.Abierta
                        CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = e.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(26)
                    Case TipoEstadoNoConformidad.CierrePosEnPlazo
                        CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = e.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(28)
                    Case TipoEstadoNoConformidad.CierrePosFueraPlazo
                        CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = e.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(29)
                    Case TipoEstadoNoConformidad.CierreNegativo
                        CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = e.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(27)
                    Case TipoEstadoNoConformidad.CierreNegSinRevisar, TipoEstadoNoConformidad.CierrePosSinRevisar
                        CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = e.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(30)
                    Case TipoEstadoNoConformidad.Anulada
                        CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = e.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(31)
                    Case TipoEstadoNoConformidad.CierrePosPendiente
                        CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = e.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(35)
                    Case Else
                        CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = ""
                End Select
                e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").Tooltip = CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text
            End If
            If (DBNullToStr(e.Row.Items.FindItemByKey("COMENTARIO").Value) <> Nothing) Then
                e.Row.Items(whdgPanelEscalacion.GridView.Columns.FromKey("COMENTARIO").Index).Text =
                        "<img class='imgTexto' src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/coment.gif' />" &
                        "<span>" & e.Row.DataItem.Item.Row("COMENTARIO") & "</span>"
            End If

        ElseIf nivel = 1 Then
            Select Case DBNullToInteger(e.Row.DataItem.Item.Row.Item("ESTADO"))
                Case TipoEstadoNoConformidad.Abierta
                    e.Row.Items.FindItemByKey("ESTADO").Text = Textos(26)
                Case TipoEstadoNoConformidad.CierrePosEnPlazo
                    e.Row.Items.FindItemByKey("ESTADO").Text = Textos(28)
                Case TipoEstadoNoConformidad.CierrePosFueraPlazo
                    e.Row.Items.FindItemByKey("ESTADO").Text = Textos(29)
                Case TipoEstadoNoConformidad.CierreNegativo
                    e.Row.Items.FindItemByKey("ESTADO").Text = Textos(27)
                Case TipoEstadoNoConformidad.CierreNegSinRevisar, TipoEstadoNoConformidad.CierrePosSinRevisar
                    e.Row.Items.FindItemByKey("ESTADO").Text = Textos(30)
                Case TipoEstadoNoConformidad.Anulada
                    e.Row.Items.FindItemByKey("ESTADO").Text = Textos(31)
                Case TipoEstadoNoConformidad.CierrePosPendiente
                    e.Row.Items.FindItemByKey("ESTADO").Text = Textos(35)
                Case Else
                    e.Row.Items.FindItemByKey("ESTADO").Text = ""
            End Select
            If e.Row.Items.FindItemByKey("NOCONFORMIDAD").Value = 0 Then
                Dim sValor As String = e.Row.Items.FindItemByKey("NOCONF_ESCALACION").Value
                e.Row.Items.FindItemByKey("NOCONF_ESCALACION").Text = Textos(36) & " " & sValor.Substring(sValor.Length - 1)
            End If
            If (DBNullToStr(e.Row.Items.FindItemByKey("COMENTARIO").Value) <> Nothing) Then
                e.Row.Items.FindItemByKey("COMENTARIO").Text =
                        "<img class='imgTexto' src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/coment.gif' />" &
                        "<span>" & e.Row.DataItem.Item.Row("COMENTARIO") & "</span>"
            End If
            CType(e.Row.Items.FindItemByKey("DETALLE").FindControl("lblVerDetalle"), System.Web.UI.WebControls.Label).Text = Textos(18)
            CType(e.Row.Items.FindItemByKey("DETALLE").FindControl("lblVerDetalle"), System.Web.UI.WebControls.Label).Style.Item(HtmlTextWriterStyle.Display) = IIf(e.Row.Items.FindItemByKey("NOCONFORMIDAD").Value = 0, "none", "inline-block")
        End If
    End Sub
    Private Sub whdgPanelEscalacionExcel_InitializeRow(sender As Object, e As RowEventArgs) Handles whdgPanelEscalacionExcel.InitializeRow
        Dim items As Infragistics.Web.UI.GridControls.GridRecordItemCollection = e.Row.Items
        Dim nivel As Integer = CType(e.Row, ContainerGridRecord).Level

        If nivel = 0 Then
            Dim tienePermiso As Boolean = False
            If IsDBNull(e.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION")) AndAlso e.Row.DataItem.Item.Row.Item("ESTADO") = 0 Then
                Select Case e.Row.DataItem.Item.Row.Item("NIVEL_ESCALACION")
                    Case 1
                        tienePermiso = PermisosAprobarRechazarEscalacion_UNQA(PermisoAprobarRechazar_Escalacion_UNQA.Nivel_1).Contains(e.Row.DataItem.Item.Row.Item("UNQA"))
                    Case 2
                        tienePermiso = PermisosAprobarRechazarEscalacion_UNQA(PermisoAprobarRechazar_Escalacion_UNQA.Nivel_2).Contains(e.Row.DataItem.Item.Row.Item("UNQA"))
                    Case 3
                        tienePermiso = PermisosAprobarRechazarEscalacion_UNQA(PermisoAprobarRechazar_Escalacion_UNQA.Nivel_3).Contains(e.Row.DataItem.Item.Row.Item("UNQA"))
                    Case Else
                        tienePermiso = PermisosAprobarRechazarEscalacion_UNQA(PermisoAprobarRechazar_Escalacion_UNQA.Nivel_4).Contains(e.Row.DataItem.Item.Row.Item("UNQA"))
                End Select
            End If
            If tienePermiso Then
                With CType(e.Row.Items.FindItemByKey("APROBAR_RECHAZAR").FindControl("btnAprobar"), System.Web.UI.WebControls.Button)
                    .Text = Textos(12)
                    .Attributes.Add("ROW", e.Row.Index)
                    .Style.Item("display") = "inline-block"
                End With
                With CType(e.Row.Items.FindItemByKey("APROBAR_RECHAZAR").FindControl("btnRechazar"), System.Web.UI.WebControls.Button)
                    .Text = Textos(13)
                    .Attributes.Add("ESCALACION_PROVE", e.Row.DataItem.Item.Row("ID"))
                    .Style.Item("display") = "inline-block"
                End With
            Else
                CType(e.Row.Items.FindItemByKey("APROBAR_RECHAZAR").FindControl("btnAprobar"), System.Web.UI.WebControls.Button).Style.Item("display") = "none"
                CType(e.Row.Items.FindItemByKey("APROBAR_RECHAZAR").FindControl("btnRechazar"), System.Web.UI.WebControls.Button).Style.Item("display") = "none"
            End If
            Select Case DBNullToInteger(e.Row.DataItem.Item.Row.Item("ESTADO"))
                Case 1
                    e.Row.Items.FindItemByKey("ESTADO").CssClass = "nivelEscalacionEnCurso"
                Case 2
                    e.Row.Items.FindItemByKey("ESTADO").CssClass = ""
                Case Else
                    e.Row.Items.FindItemByKey("ESTADO").CssClass = "nivelEscalacionPendiente"
                    e.Row.Items.FindItemByKey("NIVEL_ESCALACION").CssClass = "nivelEscalacionPendiente"
            End Select
            If IsDBNull(e.Row.DataItem.Item.Row.Item("ESTADO_NOCONF")) Then
                CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = ""
            Else
                Select Case DBNullToInteger(e.Row.DataItem.Item.Row.Item("ESTADO_NOCONF"))
                    Case TipoEstadoNoConformidad.Guardada
                        CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = e.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(34)
                    Case TipoEstadoNoConformidad.Abierta
                        CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = e.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(26)
                    Case TipoEstadoNoConformidad.CierrePosEnPlazo
                        CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = e.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(28)
                    Case TipoEstadoNoConformidad.CierrePosFueraPlazo
                        CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = e.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(29)
                    Case TipoEstadoNoConformidad.CierreNegativo
                        CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = e.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(27)
                    Case TipoEstadoNoConformidad.CierreNegSinRevisar, TipoEstadoNoConformidad.CierrePosSinRevisar
                        CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = e.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(30)
                    Case TipoEstadoNoConformidad.Anulada
                        CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = e.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(31)
                    Case TipoEstadoNoConformidad.CierrePosPendiente
                        CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = e.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(35)
                    Case Else
                        CType(e.Row.Items.FindItemByKey("NOCONF_ESCALACION_DEN").FindControl("lblVerDetalleNoConf"), System.Web.UI.WebControls.Label).Text = ""
                End Select
            End If
        ElseIf nivel = 1 Then
            Select Case DBNullToInteger(e.Row.DataItem.Item.Row.Item("ESTADO"))
                Case TipoEstadoNoConformidad.Abierta
                    e.Row.Items.FindItemByKey("ESTADO").Text = Textos(26)
                Case TipoEstadoNoConformidad.CierrePosEnPlazo
                    e.Row.Items.FindItemByKey("ESTADO").Text = Textos(28)
                Case TipoEstadoNoConformidad.CierrePosFueraPlazo
                    e.Row.Items.FindItemByKey("ESTADO").Text = Textos(29)
                Case TipoEstadoNoConformidad.CierreNegativo
                    e.Row.Items.FindItemByKey("ESTADO").Text = Textos(27)
                Case TipoEstadoNoConformidad.CierreNegSinRevisar, TipoEstadoNoConformidad.CierrePosSinRevisar
                    e.Row.Items.FindItemByKey("ESTADO").Text = Textos(30)
                Case TipoEstadoNoConformidad.Anulada
                    e.Row.Items.FindItemByKey("ESTADO").Text = Textos(31)
                Case TipoEstadoNoConformidad.CierrePosPendiente
                    e.Row.Items.FindItemByKey("ESTADO").Text = Textos(35)
                Case Else
                    e.Row.Items.FindItemByKey("ESTADO").Text = ""
            End Select
            If e.Row.Items.FindItemByKey("NOCONFORMIDAD").Value = 0 Then
                Dim sValor As String = e.Row.Items.FindItemByKey("NOCONF_ESCALACION").Value
                e.Row.Items.FindItemByKey("NOCONF_ESCALACION").Text = Textos(36) & " " & sValor.Substring(sValor.Length - 1)
            End If
            CType(e.Row.Items.FindItemByKey("DETALLE").FindControl("lblVerDetalle"), System.Web.UI.WebControls.Label).Text = Textos(18)
        End If


    End Sub
    Private Sub ExportarExcel()
        Dim fileName As String = FSNPageHeader.TituloCabecera
        fileName = Replace(fileName, "á", "a")
        fileName = Replace(fileName, "é", "e")
        fileName = Replace(fileName, "í", "i")
        fileName = Replace(fileName, "ó", "o")
        fileName = Replace(fileName, "ú", "u")
        HttpUtility.UrlEncode(Regex.Replace(fileName, "[^\w\.@]", "_"))
        fileName = fileName.Replace("+", "%20")

        With whdgExcelExporter
            whdgPanelEscalacionExcel.GridView.Behaviors.Sorting.SortedColumns.Clear()
            For Each item As Infragistics.Web.UI.GridControls.SortedColumnInfo In Me.whdgPanelEscalacion.GridView.Behaviors.Sorting.SortedColumns
                whdgPanelEscalacionExcel.GridView.Behaviors.Sorting.SortedColumns.Add(item.ColumnKey.ToString, item.SortDirection)
            Next

            .DownloadName = fileName
            .DataExportMode = DataExportMode.AllDataInDataSource
            .EnableStylesExport = False
            .Export(whdgPanelEscalacionExcel)
        End With
    End Sub
    Private Sub whdgExcelExporter_GridRecordItemExported(sender As Object, e As GridRecordItemExportedEventArgs) Handles whdgExcelExporter.GridRecordItemExported
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.PanelEscalacionProveedores
        Select Case e.GridCell.Column.Key
            Case "NOCONF_ESCALACION_DEN"
                Dim col As Infragistics.Documents.Excel.WorksheetColumn = e.Worksheet.Columns.Item(e.WorksheetCell.ColumnIndex)
                col.SetWidth(500, Infragistics.Documents.Excel.WorksheetColumnWidthUnit.Pixel)

                If IsDBNull(e.GridCell.Row.DataItem.Item.Row.Item("ESTADO_NOCONF")) Then
                    e.WorksheetCell.Value = ""
                Else
                    Select Case DBNullToInteger(e.GridCell.Row.DataItem.Item.Row.Item("ESTADO_NOCONF"))
                        Case TipoEstadoNoConformidad.Guardada
                            e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(34)
                        Case TipoEstadoNoConformidad.Abierta
                            e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(26)
                        Case TipoEstadoNoConformidad.CierrePosEnPlazo
                            e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(28)
                        Case TipoEstadoNoConformidad.CierrePosFueraPlazo
                            e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(29)
                        Case TipoEstadoNoConformidad.CierreNegativo
                            e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(27)
                        Case TipoEstadoNoConformidad.CierreNegSinRevisar, TipoEstadoNoConformidad.CierrePosSinRevisar
                            e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(30)
                        Case TipoEstadoNoConformidad.Anulada
                            e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(31)
                        Case TipoEstadoNoConformidad.CierrePosPendiente
                            e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION_DEN") & " - " & Textos(35)
                        Case Else
                            e.WorksheetCell.Value = ""
                    End Select
                End If
            Case "ESTADO_DEN"
            Case "ESTADO"
                If DirectCast(e.GridCell.Row, Infragistics.Web.UI.GridControls.ContainerGridRecord).Level = 0 Then
                    'Estado_Den
                Else
                    If e.GridCell.Row.DataItem.Item.Row.Item("NOCONFORMIDAD") <> 0 Then
                        Select Case DBNullToInteger(e.GridCell.Row.DataItem.Item.Row.Item("ESTADO"))
                            Case TipoEstadoNoConformidad.Abierta
                                e.WorksheetCell.Value = Textos(26)
                            Case TipoEstadoNoConformidad.CierrePosEnPlazo
                                e.WorksheetCell.Value = Textos(28)
                            Case TipoEstadoNoConformidad.CierrePosFueraPlazo
                                e.WorksheetCell.Value = Textos(29)
                            Case TipoEstadoNoConformidad.CierreNegativo
                                e.WorksheetCell.Value = Textos(27)
                            Case TipoEstadoNoConformidad.CierreNegSinRevisar, TipoEstadoNoConformidad.CierrePosSinRevisar
                                e.WorksheetCell.Value = Textos(30)
                            Case TipoEstadoNoConformidad.Anulada
                                e.WorksheetCell.Value = Textos(31)
                            Case TipoEstadoNoConformidad.CierrePosPendiente
                                e.WorksheetCell.Value = Textos(35)
                            Case Else
                                e.WorksheetCell.Value = ""
                        End Select
                    Else
                        e.WorksheetCell.Value = ""
                    End If
                End If
            Case "NOCONF_ESCALACION"
                If e.GridCell.Row.DataItem.Item.Row.Item("NOCONFORMIDAD") = 0 Then
                    Dim sValor As String = e.GridCell.Row.DataItem.Item.Row.Item("NOCONF_ESCALACION")
                    e.WorksheetCell.Value = Textos(36) & " " & sValor.Substring(sValor.Length - 1)
                End If
            Case "DETALLE"
                e.WorksheetCell.Value = ""
            Case "FECHA", "FECALTA", "FECHAHISTORICO"
                If IsDate(e.WorksheetCell.Value) Then e.WorksheetCell.Value = Format(CDate(e.WorksheetCell.Value), FSNUser.DateFormat.ShortDatePattern)
            Case "COMENTARIO"
                Dim col As Infragistics.Documents.Excel.WorksheetColumn = e.Worksheet.Columns.Item(e.WorksheetCell.ColumnIndex)
                col.CellFormat.WrapText = Infragistics.Documents.Excel.ExcelDefaultableBoolean.False
                col.SetWidth(500, Infragistics.Documents.Excel.WorksheetColumnWidthUnit.Pixel)
            Case Else

        End Select
    End Sub
    Private Class CustomItemTemplate_NOCONF_ESCALACION_DEN
        Implements ITemplate
#Region "ITemplate Members"
        Public Sub InstantiateIn(ByVal container As Control) Implements ITemplate.InstantiateIn
            Dim lblNoConfEscalacionDen As New Label()
            lblNoConfEscalacionDen.ID = "lblVerDetalleNoConf"
            lblNoConfEscalacionDen.CssClass = "Link Subrayado"

            container.Controls.Add(lblNoConfEscalacionDen)

        End Sub
#End Region
    End Class
    Private Class CustomItemTemplate_APROBAR_RECHAZAR
        Implements ITemplate
#Region "ITemplate Members"
        Public Sub InstantiateIn(ByVal container As Control) Implements ITemplate.InstantiateIn
            Dim divBotones As New HtmlGenericControl
            divBotones.TagName = "div"
            divBotones.Style.Add("text-align", "center")

            Dim btnAprobar As New Button
            btnAprobar.ID = "btnAprobar"
            btnAprobar.Style.Add("display", "none")
            btnAprobar.Style.Add("margin-right", "1em")

            Dim btnRechazar As New Button
            btnRechazar.ID = "btnRechazar"
            btnRechazar.Style.Add("display", "none")

            divBotones.Controls.Add(btnAprobar)
            divBotones.Controls.Add(btnRechazar)

            container.Controls.Add(divBotones)
        End Sub
#End Region
    End Class
    Private Class CustomItemTemplate_DETALLE
        Implements ITemplate
#Region "ITemplate Members"
        Public Sub InstantiateIn(ByVal container As Control) Implements ITemplate.InstantiateIn
            Dim divLabel As New HtmlGenericControl
            divLabel.TagName = "div"
            divLabel.Style.Add("text-align", "center")

            Dim lblVerDetalle As New Label
            lblVerDetalle.ID = "lblVerDetalle"
            lblVerDetalle.CssClass = "Link"

            divLabel.Controls.Add(lblVerDetalle)

            container.Controls.Add(divLabel)
        End Sub
#End Region
    End Class

#Region "Eventos Sorted/Filter + Fns Auxiliares"
    Private Sub whdgPanelEscalacion_ColumnSorted(sender As Object, e As SortingEventArgs) Handles whdgPanelEscalacion.ColumnSorted

        Paginador((dsPanelEscalacion.Tables("INFO_ESCALACION").Rows.Count \ ConfigurationManager.AppSettings("registrosPaginacion")) +
                            If(dsPanelEscalacion.Tables("INFO_ESCALACION").Rows.Count Mod ConfigurationManager.AppSettings("registrosPaginacion") = 0, 0, 1), 1)
    End Sub

    Private Sub whdgPanelEscalacion_DataFiltered(sender As Object, e As FilteredEventArgs) Handles whdgPanelEscalacion.DataFiltered
        Dim dsAuxiliar As New DataSet
        dsAuxiliar = CType(CType(Cache("oPanelEscalacion_" & FSNUser.Cod), Object)(0), DataSet).Copy

        Dim sFiltro As String = String.Empty
        Dim sFilterCondition As String = String.Empty
        Dim bNoEsAll As Boolean
        Dim bNoEsFecha As Boolean

        For Each Filtr As ColumnFilter In e.ColumnFilters
            bNoEsAll = True
            bNoEsFecha = True
            sFilterCondition = ""
            If Filtr.ColumnKey = "FECHA" Then
                bNoEsFecha = False
                Select Case DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleDateNode).Rule
                    Case DateTimeFilterRules.All
                        bNoEsAll = False
                    Case DateTimeFilterRules.Before
                        sFilterCondition = "(" & Filtr.ColumnKey & " < '" & DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleDateNode).Value & " 00:00:00')"
                    Case DateTimeFilterRules.After
                        sFilterCondition = "(" & Filtr.ColumnKey & " > '" & DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleDateNode).Value & " 23:59:59')"
                    Case DateTimeFilterRules.Today
                        sFilterCondition = "(" & Filtr.ColumnKey & " >= '" & Today.ToShortDateString & " 00:00:00') AND (" & Filtr.ColumnKey & " <= '" & Today.ToShortDateString & " 23:59:59')"
                    Case DateTimeFilterRules.Tomorrow
                        sFilterCondition = "(" & Filtr.ColumnKey & " >= '" & Today.AddDays(1).ToShortDateString & " 00:00:00') AND (" & Filtr.ColumnKey & " <= '" & Today.AddDays(1).ToShortDateString & " 23:59:59')"
                    Case DateTimeFilterRules.IsNull
                        sFilterCondition = "(" & Filtr.ColumnKey & " IS NULL)"
                    Case DateTimeFilterRules.IsNotNull
                        sFilterCondition = "(" & Filtr.ColumnKey & " IS NOT NULL)"
                    Case DateTimeFilterRules.DoesNotEqual
                        sFilterCondition = "((" & Filtr.ColumnKey & " <= '" & DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleDateNode).Value & " 00:00:00') OR (" & Filtr.ColumnKey & " >= '" & DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleDateNode).Value & " 23:59:59'))"
                    Case DateTimeFilterRules.Equals
                        sFilterCondition = "(" & Filtr.ColumnKey & " >= '" & DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleDateNode).Value & " 00:00:00') AND (" & Filtr.ColumnKey & " <= '" & DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleDateNode).Value & " 23:59:59')"
                    Case DateTimeFilterRules.LastMonth
                        Dim Mes As Integer = IIf(Today.Month = 1, 12, Today.Month - 1)
                        Dim Anno As Integer = IIf(Today.Month = 1, Today.Year - 1, Today.Year)
                        Dim Dia As Date = DateSerial(Anno, Mes, 1)
                        Dim DiaF As Date = DameUltimoDiaMes(Anno, Mes)
                        sFilterCondition = "(" & Filtr.ColumnKey & " >= '" & Dia.ToShortDateString & " 00:00:00') AND (" & Filtr.ColumnKey & " <= '" & DiaF.ToShortDateString & " 23:59:59')"
                    Case DateTimeFilterRules.LastQuarter
                        Dim Mes As Integer = IIf(Today.Month <= 3, 10, IIf(Today.Month <= 6, 1, IIf(Today.Month <= 9, 4, 7)))
                        Dim Anno As Integer = IIf(Today.Month >= 3, Today.Year - 1, Today.Year)
                        Dim Dia As Date = DateSerial(Anno, Mes, 1)
                        Dim DiaF As Date = DameUltimoDiaMes(Anno, Mes + 2)
                        sFilterCondition = "(" & Filtr.ColumnKey & " >= '" & Dia.ToShortDateString & " 00:00:00') AND (" & Filtr.ColumnKey & " <= '" & DiaF.ToShortDateString & " 23:59:59')"
                    Case DateTimeFilterRules.LastWeek
                        Dim Dia As Date
                        Dim DiaF As Date
                        Select Case Today.DayOfWeek
                            Case DayOfWeek.Sunday
                                Dia = Today.AddDays(-7)
                            Case DayOfWeek.Monday
                                Dia = Today.AddDays(-8)
                            Case DayOfWeek.Tuesday
                                Dia = Today.AddDays(-9)
                            Case DayOfWeek.Wednesday
                                Dia = Today.AddDays(-10)
                            Case DayOfWeek.Thursday
                                Dia = Today.AddDays(-11)
                            Case DayOfWeek.Friday
                                Dia = Today.AddDays(-12)
                            Case DayOfWeek.Saturday
                                Dia = Today.AddDays(-13)
                        End Select
                        DiaF = Dia.AddDays(6)
                        sFilterCondition = "(" & Filtr.ColumnKey & " >= '" & Dia.ToShortDateString & " 00:00:00') AND (" & Filtr.ColumnKey & " <= '" & DiaF.ToShortDateString & " 23:59:59')"
                    Case DateTimeFilterRules.LastYear
                        Dim Dia As Date = DateSerial(Today.Year - 1, 1, 1)
                        Dim DiaF As Date = DateSerial(Today.Year - 1, 12, 31)
                        sFilterCondition = "(" & Filtr.ColumnKey & " >= '" & Dia.ToShortDateString & " 00:00:00') AND (" & Filtr.ColumnKey & " <= '" & DiaF.ToShortDateString & " 23:59:59')"
                    Case DateTimeFilterRules.NextMonth
                        Dim Mes As Integer = IIf(Today.Month = 12, 1, Today.Month)
                        Dim Anno As Integer = IIf(Today.Month = 12, Today.Year + 1, Today.Year)
                        Dim Dia As Date = DateSerial(Anno, Mes + 1, 1)
                        Dim DiaF As Date = DameUltimoDiaMes(Anno, Mes + 1)
                        sFilterCondition = "(" & Filtr.ColumnKey & " >= '" & Dia.ToShortDateString & " 00:00:00') AND (" & Filtr.ColumnKey & " <= '" & DiaF.ToShortDateString & " 23:59:59')"
                    Case DateTimeFilterRules.NextQuarter
                        Dim Mes As Integer = IIf(Today.Month <= 3, 4, IIf(Today.Month <= 6, 7, IIf(Today.Month <= 9, 10, 1)))
                        Dim Anno As Integer = IIf(Today.Month >= 10, Today.Year + 1, Today.Year)
                        Dim Dia As Date = DateSerial(Anno, Mes, 1)
                        Dim DiaF As Date = DameUltimoDiaMes(Anno, Mes + 2)
                        sFilterCondition = "(" & Filtr.ColumnKey & " >= '" & Dia.ToShortDateString & " 00:00:00') AND (" & Filtr.ColumnKey & " <= '" & DiaF.ToShortDateString & " 23:59:59')"
                    Case DateTimeFilterRules.NextWeek
                        Dim Dia As Date
                        Dim DiaF As Date
                        Select Case Today.DayOfWeek
                            Case DayOfWeek.Sunday
                                Dia = Today.AddDays(7)
                            Case DayOfWeek.Monday
                                Dia = Today.AddDays(6)
                            Case DayOfWeek.Tuesday
                                Dia = Today.AddDays(5)
                            Case DayOfWeek.Wednesday
                                Dia = Today.AddDays(4)
                            Case DayOfWeek.Thursday
                                Dia = Today.AddDays(3)
                            Case DayOfWeek.Friday
                                Dia = Today.AddDays(2)
                            Case DayOfWeek.Saturday
                                Dia = Today.AddDays(1)
                        End Select
                        DiaF = Dia.AddDays(6)
                        sFilterCondition = "(" & Filtr.ColumnKey & " >= '" & Dia.ToShortDateString & " 00:00:00') AND (" & Filtr.ColumnKey & " <= '" & DiaF.ToShortDateString & " 23:59:59')"
                    Case DateTimeFilterRules.NextYear
                        Dim Dia As Date = DateSerial(Today.Year + 1, 1, 1)
                        Dim DiaF As Date = DateSerial(Today.Year + 1, 12, 31)
                        sFilterCondition = "(" & Filtr.ColumnKey & " >= '" & Dia.ToShortDateString & " 00:00:00') AND (" & Filtr.ColumnKey & " <= '" & DiaF.ToShortDateString & " 23:59:59')"
                    Case DateTimeFilterRules.ThisMonth
                        Dim Dia As Date = DateSerial(Today.Year, Today.Month, 1)
                        Dim DiaF As Date = DameUltimoDiaMes(Today.Year, Today.Month)
                        sFilterCondition = "(" & Filtr.ColumnKey & " >= '" & Dia.ToShortDateString & " 00:00:00') AND (" & Filtr.ColumnKey & " <= '" & DiaF.ToShortDateString & " 23:59:59')"
                    Case DateTimeFilterRules.ThisQuarter
                        Dim Mes As Integer = IIf(Today.Month <= 3, 1, IIf(Today.Month <= 6, 4, IIf(Today.Month <= 9, 7, 10)))
                        Dim Dia As Date = DateSerial(Today.Year, Mes, 1)
                        Dim DiaF As Date = DameUltimoDiaMes(Today.Year, Mes + 2)
                        sFilterCondition = "(" & Filtr.ColumnKey & " >= '" & Dia.ToShortDateString & " 00:00:00') AND (" & Filtr.ColumnKey & " <= '" & DiaF.ToShortDateString & " 23:59:59')"
                    Case DateTimeFilterRules.ThisWeek
                        Dim Dia As Date
                        Dim DiaF As Date
                        Select Case Today.DayOfWeek
                            Case DayOfWeek.Monday
                                Dia = Today.AddDays(-1)
                                DiaF = Today.AddDays(5)
                            Case DayOfWeek.Tuesday
                                Dia = Today.AddDays(-2)
                                DiaF = Today.AddDays(4)
                            Case DayOfWeek.Wednesday
                                Dia = Today.AddDays(-3)
                                DiaF = Today.AddDays(3)
                            Case DayOfWeek.Thursday
                                Dia = Today.AddDays(-4)
                                DiaF = Today.AddDays(2)
                            Case DayOfWeek.Friday
                                Dia = Today.AddDays(-5)
                                DiaF = Today.AddDays(1)
                            Case DayOfWeek.Saturday
                                Dia = Today.AddDays(-6)
                                DiaF = Today
                            Case DayOfWeek.Sunday
                                Dia = Today
                                DiaF = Today.AddDays(7)
                        End Select
                        sFilterCondition = "(" & Filtr.ColumnKey & " >= '" & Dia.ToShortDateString & " 00:00:00') AND (" & Filtr.ColumnKey & " <= '" & DiaF.ToShortDateString & " 23:59:59')"
                    Case DateTimeFilterRules.ThisYear
                        Dim Dia As Date = DateSerial(Today.Year, 1, 1)
                        Dim DiaF As Date = DateSerial(Today.Year, 12, 31)
                        sFilterCondition = "(" & Filtr.ColumnKey & " >= '" & Dia.ToShortDateString & " 00:00:00') AND (" & Filtr.ColumnKey & " <= '" & DiaF.ToShortDateString & " 23:59:59')"
                    Case DateTimeFilterRules.YearToDate
                        Dim Dia As Date = DateSerial(Today.Year, 1, 1)
                        sFilterCondition = "(" & Filtr.ColumnKey & " >= '" & Dia.ToShortDateString & " 00:00:00') AND (" & Filtr.ColumnKey & " <= '" & Today.ToShortDateString & " 23:59:59')"
                    Case DateTimeFilterRules.Yesterday
                        sFilterCondition = "(" & Filtr.ColumnKey & " >= '" & Today.AddDays(-1).ToShortDateString & " 00:00:00') AND (" & Filtr.ColumnKey & " <= '" & Today.AddDays(-1).ToShortDateString & " 23:59:59')"
                End Select
            ElseIf Filtr.ColumnKey = "NIVEL_ESCALACION" Then
                Select Case DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleNumberNode).Rule
                    Case NumericFilterRules.All
                        bNoEsAll = False
                    Case NumericFilterRules.Equals
                        sFilterCondition = " = '" & DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleNumberNode).Value & "'"
                    Case NumericFilterRules.DoesNotEqual
                        sFilterCondition = " <> '" & DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleNumberNode).Value & "'"
                    Case NumericFilterRules.GreaterThan
                        sFilterCondition = " > '" & DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleNumberNode).Value & "'"
                    Case NumericFilterRules.GreaterThanOrEqualTo
                        sFilterCondition = " >= '" & DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleNumberNode).Value & "'"
                    Case NumericFilterRules.IsNotNull
                        sFilterCondition = " IS NOT NULL"
                    Case NumericFilterRules.IsNull
                        sFilterCondition = " IS NULL"
                    Case NumericFilterRules.LessThan
                        sFilterCondition = " < '" & DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleNumberNode).Value & "'"
                    Case NumericFilterRules.LessThanOrEqualTo
                        sFilterCondition = " <= '" & DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleNumberNode).Value & "'"
                End Select
            Else
                Select Case DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleTextNode).Rule
                    Case TextFilterRules.All
                        bNoEsAll = False
                    Case 2
                        sFilterCondition = " NOT LIKE '" & DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleTextNode).Value & "'"
                    Case 3
                        sFilterCondition = " LIKE '" & DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleTextNode).Value & "%'"
                    Case 4
                        sFilterCondition = " LIKE '%" & DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleTextNode).Value & "'"
                    Case 5
                        sFilterCondition = " LIKE '%" & DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleTextNode).Value & "%'"
                    Case 6
                        sFilterCondition = " NOT LIKE '%" & DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleTextNode).Value & "%'"
                    Case 7
                        sFilterCondition = " IS NULL"
                    Case 8
                        sFilterCondition = " IS NOT NULL"
                    Case Else
                        sFilterCondition = " LIKE '" & DirectCast((Filtr.Condition), Infragistics.Web.UI.GridControls.RuleTextNode).Value & "'"
                End Select
            End If
            If bNoEsAll Then
                If bNoEsFecha Then
                    sFilterCondition = "(" & Filtr.ColumnKey & sFilterCondition & ")"
                Else
                    sFilterCondition = sFilterCondition
                End If
                sFiltro = IIf(sFiltro = String.Empty, sFilterCondition, sFiltro & " AND " & sFilterCondition)
            End If
        Next
        With dsAuxiliar.DefaultViewManager
            With .DataViewSettings("INFO_ESCALACION")
                .RowFilter = sFiltro
            End With
        End With
        Dim auxRowsCount As Integer = dsAuxiliar.DefaultViewManager.DataViewSettings("INFO_ESCALACION").Table.DefaultView.ToTable.Rows.Count
        If auxRowsCount = 0 Then auxRowsCount = 1

        Paginador((auxRowsCount \ ConfigurationManager.AppSettings("registrosPaginacion")) +
                            If(auxRowsCount Mod ConfigurationManager.AppSettings("registrosPaginacion") = 0, 0, 1), 1)
    End Sub

    Private Function DameUltimoDiaMes(ByVal Anno As Integer, ByVal mes As Integer) As Date
        Dim UltimoDiaMes As Date = DateSerial(Anno, mes, 28)
        Try
            If DateSerial(Anno, mes, 29).Month = mes Then UltimoDiaMes = DateSerial(Anno, mes, 29)
            If DateSerial(Anno, mes, 30).Month = mes Then UltimoDiaMes = DateSerial(Anno, mes, 30)
            If DateSerial(Anno, mes, 31).Month = mes Then UltimoDiaMes = DateSerial(Anno, mes, 31)
        Catch ex As Exception
        End Try

        Return UltimoDiaMes
    End Function

#End Region
End Class