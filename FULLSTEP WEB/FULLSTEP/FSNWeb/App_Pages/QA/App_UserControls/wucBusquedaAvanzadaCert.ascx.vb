﻿Public Partial Class wucBusquedaAvanzadaCert
    Inherits System.Web.UI.UserControl

    Private mlFiltro As Long = 0
    Private oTextos As DataTable
    Private pag As FSNPage
    Private msCarga As String = ""

#Region " PROPIEDADES "

#Region "Tipo"
    ''' <summary>
    ''' Material QA
    ''' </summary>
    ''' <returns>Lista de Materiales QA por los q filtrar</returns>
    ''' <remarks>Llamada desde: PMWeb2008/Certificados/Certificados.aspx/ObtenerDataTableCertificados; Tiempo maximo:0</remarks>
    Public Property Materiales() As String '(Seleccion Multiple)
        Get
            Dim sMateriales As String = ""
            For i = 0 To wddMateriales.Items.Count - 1
                If wddMateriales.Items(i).Selected Then
                    If sMateriales <> "" Then sMateriales += ","
                    sMateriales = sMateriales + wddMateriales.Items(i).Value
                End If
            Next
            Materiales = sMateriales
        End Get

        Set(ByVal Value As String)
            Dim sTexto As String = ""
            If Value <> "" Then
                For i = 0 To wddMateriales.Items.Count - 1
                    If wddMateriales.Items(i).Value <> "" Then
                        If buscarElemento(Value, wddMateriales.Items(i).Value) Then
                            wddMateriales.Items(i).Selected = True
                            If sTexto <> "" Then sTexto += ","
                            sTexto = sTexto + wddMateriales.Items(i).Text
                        Else
                            wddMateriales.Items(i).Selected = False
                        End If
                    End If
                Next
            Else
                If wddMateriales.SelectedItems.Count > 0 Then
                    For i = 0 To wddMateriales.Items.Count - 1
                        wddMateriales.Items(i).Selected = False
                    Next
                End If
            End If
            wddMateriales.CurrentValue = sTexto
        End Set
    End Property

    ''' <summary>
    ''' Tipo de Certificado
    ''' </summary>
    ''' <returns>Lista de Tipos de Certificado por los q filtrar</returns>
    ''' <remarks>Llamada desde: PMWeb2008/Certificados/Certificados.aspx/ObtenerDataTableCertificados; Tiempo maximo:0</remarks>
    Public Property TipoSolicitudes() As String '(Seleccion Multiple)
        Get
            Dim sTipoSolicitudes As String = ""
            For i = 0 To wddTipoSolicitudes.Items.Count - 1
                If wddTipoSolicitudes.Items(i).Selected Then
                    If sTipoSolicitudes <> "" Then sTipoSolicitudes += ","
                    sTipoSolicitudes = sTipoSolicitudes + wddTipoSolicitudes.Items(i).Value
                End If
            Next
            TipoSolicitudes = sTipoSolicitudes
        End Get

        Set(ByVal Value As String)
            Dim sTexto As String = ""
            If Value <> "" Then
                For i = 0 To wddTipoSolicitudes.Items.Count - 1
                    If wddTipoSolicitudes.Items(i).Value <> "" Then
                        If buscarElemento(Value, wddTipoSolicitudes.Items(i).Value) Then
                            wddTipoSolicitudes.Items(i).Selected = True
                            If sTexto <> "" Then sTexto += ","
                            sTexto = sTexto + wddTipoSolicitudes.Items(i).Text
                        Else
                            wddTipoSolicitudes.Items(i).Selected = False
                        End If
                    End If
                Next
            Else
                If wddTipoSolicitudes.SelectedItems.Count > 0 Then
                    For i = 0 To wddTipoSolicitudes.Items.Count - 1
                        wddTipoSolicitudes.Items(i).Selected = False
                    Next
                End If
            End If
            wddTipoSolicitudes.CurrentValue = sTexto
        End Set
    End Property

    ''' <summary>
    ''' Ver tipos de certificados dados de baja
    ''' </summary>
    ''' <returns>Ver tipos de certificados dados de baja o no</returns>
    ''' <remarks>Llamada desde: PMWeb2008/Certificados/Certificados.aspx/ObtenerDataTableCertificados; Tiempo maximo:0</remarks>
    Public Property VerDadosBaja() As Boolean
        Get
            VerDadosBaja = chkVerDadosBaja.Checked
        End Get
        Set(ByVal Value As Boolean)
            chkVerDadosBaja.Checked = Value
        End Set
    End Property
#End Region

#Region " Tab Proveedores"
    ''' <summary>
    ''' Todos los proves ó solo los de Panel de Calidad ó solo potenciales
    ''' </summary>
    ''' <returns>Todos los proves ó solo los de Panel de Calidad ó solo potenciales</returns>
    ''' <remarks>Llamada desde: PMWeb2008/Certificados/Certificados.aspx/ObtenerDataTableCertificados; Tiempo maximo:0</remarks>
    Public Property MostrarProveedores() As Long
        Get
            If wddMostrarProveedores.SelectedValue <> Nothing Then
                MostrarProveedores = CLng(wddMostrarProveedores.SelectedValue)
            Else
                MostrarProveedores = 0
            End If
        End Get
        Set(ByVal Value As Long)
            wddMostrarProveedores.CurrentValue = ""
            If Value > 0 Then
                wddMostrarProveedores.SelectedValue = Value
            Else
                wddMostrarProveedores.SelectedValue = "0"
                wddMostrarProveedores.SelectedItemIndex = -1
            End If
        End Set
    End Property
    ''' <summary>
    ''' Todos los proves ó solo los de Panel de Calidad ó solo potenciales es el selectedvalue pero recien entrado a configurar
    ''' tienes selectedvalue pero no SelectedItem
    ''' </summary>
    ''' <returns>Todos los proves ó solo los de Panel de Calidad ó solo potenciales</returns>
    ''' <remarks>Llamada desde: PMWeb2008/Certificados/Certificados.aspx/ObtenerDataTableCertificados; Tiempo maximo:0</remarks>
    Public ReadOnly Property MostrarItemProveedores() As Long
        Get
            MostrarItemProveedores = CLng(wddMostrarProveedores.SelectedItemIndex)
        End Get
    End Property

    ''' <summary>
    ''' Denominación del proveedor
    ''' </summary>
    ''' <returns>Denominación del proveedor</returns>
    ''' <remarks>Llamada desde: PMWeb2008/Certificados/Certificados.aspx/ObtenerDataTableCertificados; Tiempo maximo:0</remarks>
    Public Property ProveedorTexto() As String
        Get
            ProveedorTexto = txtProveedor.Text
        End Get
        Set(ByVal Value As String)
            txtProveedor.Text = Value
        End Set
    End Property

    ''' <summary>
    ''' Almacenar para la BBDD el codigo del proveedor
    ''' </summary>
    ''' <returns>codigo del proveedor</returns>
    ''' <remarks>Llamada desde: PMWeb2008/Certificados/Certificados.aspx/ObtenerDataTableCertificados; Tiempo maximo:0</remarks>
    Public Property ProveedorHidden() As String
        Get
            ProveedorHidden = hidProveedor.Value
        End Get
        Set(ByVal Value As String)
            hidProveedor.Value = Value
        End Set
    End Property

    ''' <summary>
    ''' Ver proveedores dados de baja
    ''' </summary>
    ''' <returns>Ver proveedores dados de baja o no</returns>
    ''' <remarks>Llamada desde: PMWeb2008/Certificados/Certificados.aspx/ObtenerDataTableCertificados; Tiempo maximo:0</remarks>
    Public Property ProveedorDeBaja() As Boolean
        Get
            ProveedorDeBaja = chkProveedorDeBaja.Checked
        End Get
        Set(ByVal Value As Boolean)
            chkProveedorDeBaja.Checked = Value
        End Set
    End Property

    ''' <summary>
    ''' En ConfigurarFiltrosQA hay dos wucBusquedaAvanzadaCert. Para todos los forms y para un form. Se usa para diferenciar
    ''' </summary>
    ''' <remarks>Llamada desde: javascript/BuscarProveedor; Tiempo maximo:0</remarks>
    Public WriteOnly Property IdentidadPanel() As String
        Set(ByVal Value As String)
            hidIdentidadPanel.Value = Value
        End Set
    End Property
#End Region

#Region " Tab Estado"
    ''' <summary>
    ''' Estados de certificados
    ''' </summary>
    ''' <returns>Lista de Estados de certificados por los q filtrar</returns>
    ''' <remarks>Llamada desde: PMWeb2008/Certificados/Certificados.aspx/ObtenerDataTableCertificados; Tiempo maximo:0</remarks>
    Public Property Estados() As String '(Seleccion Multiple)
        Get
            Dim sEstados As String = ""
            For i = 0 To wddEstados.Items.Count - 1
                If wddEstados.Items(i).Selected Then
                    If sEstados <> "" Then sEstados += ","
                    sEstados = sEstados + wddEstados.Items(i).Value
                End If
            Next
            Estados = sEstados
        End Get
        Set(ByVal Value As String)
            Dim sTexto As String = ""
            If Value <> "" Then
                For i = 0 To wddEstados.Items.Count - 1
                    If wddEstados.Items(i).Value <> "" Then
                        If buscarElemento(Value, wddEstados.Items(i).Value) Then
                            wddEstados.Items(i).Selected = True
                            If sTexto <> "" Then sTexto += ","
                            sTexto = sTexto + wddEstados.Items(i).Text
                        Else
                            wddEstados.Items(i).Selected = False
                        End If
                    End If
                Next
            Else
                If wddEstados.SelectedItems.Count > 0 Then
                    For i = 0 To wddEstados.Items.Count - 1
                        wddEstados.Items(i).Selected = False
                    Next
                End If
            End If
            wddEstados.CurrentValue = sTexto
        End Set
    End Property
#End Region

#Region " Tab Peticionario"
    ''' <summary>
    ''' Codigo peticionario (Codigo persona)
    ''' </summary>
    ''' <returns>Lista de Codigos peticionario (Codigos persona) por los q filtrar</returns>
    ''' <remarks>Llamada desde: PMWeb2008/Certificados/Certificados.aspx/ObtenerDataTableCertificados; Tiempo maximo:0</remarks>
    Public Property Peticionario() As String '(Seleccion Multiple)
        Get
            Dim sPeticionario As String = ""
            For i = 0 To wddPeticionario.Items.Count - 1
                If wddPeticionario.Items(i).Selected Then
                    If sPeticionario <> "" Then sPeticionario += ","
                    sPeticionario = sPeticionario + wddPeticionario.Items(i).Value
                End If
            Next
            Peticionario = sPeticionario
        End Get

        Set(ByVal Value As String)
            Dim sTexto As String = ""
            If Value <> "" Then
                For i = 0 To wddPeticionario.Items.Count - 1
                    If wddPeticionario.Items(i).Value <> "" Then
                        If buscarElemento(Value, wddPeticionario.Items(i).Value) Then
                            wddPeticionario.Items(i).Selected = True
                            If sTexto <> "" Then sTexto += ","
                            sTexto = sTexto + wddPeticionario.Items(i).Text
                        Else
                            wddPeticionario.Items(i).Selected = False
                        End If
                    End If
                Next
            Else
                If wddPeticionario.SelectedItems.Count > 0 Then
                    For i = 0 To wddPeticionario.Items.Count - 1
                        wddPeticionario.Items(i).Selected = False
                    Next
                End If
            End If
            wddPeticionario.CurrentValue = sTexto
        End Set
    End Property
#End Region

#Region " Tab Fechas"
    ''' <summary>
    ''' F. solicitud/renovación/envío: Última  fecha en la que se ha renovado, solicitado o enviado un certificado.
    '''	Fecha límite cumplimentación CERTIFICADO.FEC_LIM_CUMPLIM
    '''	F.actualización CERTIFICADO.FECACT
    '''	F.Expiración CERTIFICADO.FEC_EXPIRACION
    ''' </summary>
    ''' <returns>Fecha por la q se filtra</returns>
    ''' <remarks>Llamada desde: PMWeb2008/Certificados/Certificados.aspx/ObtenerDataTableCertificados; Tiempo maximo:0</remarks>
    Public Property FechasDe() As Byte
        Get
            If optFecSol.Checked Then
                FechasDe = 1
            ElseIf optFecLim.Checked Then
                FechasDe = 2
            ElseIf optFecAct.Checked Then
                FechasDe = 3
            ElseIf optFecExp.Checked Then
                FechasDe = 4
            End If
        End Get
        Set(ByVal Value As Byte)
            optFecSol.Checked = False
            optFecLim.Checked = False
            optFecAct.Checked = False
            optFecExp.Checked = False

            Select Case Value
                Case 1
                    optFecSol.Checked = True
                Case 2
                    optFecLim.Checked = True
                Case 3
                    optFecAct.Checked = True
                Case 4
                    optFecExp.Checked = True
            End Select
        End Set
    End Property

    ''' <summary>
    ''' Fecha desde
    ''' </summary>
    ''' <returns>Fecha desde</returns>
    ''' <remarks>Llamada desde: PMWeb2008/Certificados/Certificados.aspx/ObtenerDataTableCertificados; Tiempo maximo:0</remarks>
    Public Property FechaDesde() As Date
        Get
            FechaDesde = IIf(dtDesde.Value <> Nothing, dtDesde.Value, CDate(Nothing))
        End Get
        Set(ByVal Value As Date)
            dtDesde.Value = Value
        End Set
    End Property

    ''' <summary>
    ''' Fecha hasta
    ''' </summary>
    ''' <returns>Fecha hasta</returns>
    ''' <remarks>Llamada desde: PMWeb2008/Certificados/Certificados.aspx/ObtenerDataTableCertificados; Tiempo maximo:0</remarks>
    Public Property FechaHasta() As Date
        Get
            FechaHasta = IIf(dtHasta.Value <> Nothing, dtHasta.Value, CDate(Nothing))
        End Get
        Set(ByVal Value As Date)
            dtHasta.Value = Value
        End Set
    End Property
#End Region

#Region "Filtro"
    ''' <summary>
    ''' ID del filtro personalizado en bbdd
    ''' </summary>
    ''' <value>ID del filtro personalizado en bbdd</value>
    ''' <remarks>Llamada desde: PMWeb2008/Certificados/Certificados.aspx/ObtenerDataTableCertificados; Tiempo maximo:0</remarks>
    Public WriteOnly Property Filtro() As Long
        Set(ByVal Value As Long)
            mlFiltro = Value
        End Set
    End Property

    ''' <summary>
    ''' Establece si hay q cargar los componentes o no, si es q no, pq? pq se esta configurando una no conformidad
    ''' </summary>
    ''' <remarks>Llamada desde: ConfigurarFiltroQA InicializarArraysCamposGeneralesNC y  InicializarArraysCamposGeneralesCert; Tiempo máximo:0</remarks>
    Public Property Carga() As String
        Get
            Carga = msCarga
        End Get
        Set(ByVal Value As String)
            msCarga = Value
        End Set
    End Property
#End Region
#End Region

#Region "Fns de Carga y Auxiliares"
    ''' <summary>
    ''' Carga la combo de "Tipo de Proveedor" con los posibles valores
    ''' </summary>
    ''' <remarks>Llamada desde=Page_Load; Tiempo máximo=0seg.</remarks>
    Private Sub CargarMostrarProveedores()        
        wddMostrarProveedores.Items.Add(New Infragistics.Web.UI.ListControls.DropDownItem(pag.Textos(11), 2))   ' Panel de calidad  
        If pag.Acceso.gbPotAValProvisionalSelProve Then
            wddMostrarProveedores.Items.Add(New Infragistics.Web.UI.ListControls.DropDownItem(pag.Textos(24), 4))   ' Panel de calidad provisional  
            wddMostrarProveedores.Items.Add(New Infragistics.Web.UI.ListControls.DropDownItem(pag.Textos(25), 5))   ' Panel de calidad no provisional
        End If
        wddMostrarProveedores.Items.Add(New Infragistics.Web.UI.ListControls.DropDownItem(pag.Textos(12), 1))   ' Potencial        
        wddMostrarProveedores.Items.Add(New Infragistics.Web.UI.ListControls.DropDownItem(pag.Textos(13), 3))   ' Todos
    End Sub

    ''' <summary>
    ''' Carga la combo de estados con los posibles valores
    ''' </summary>
    ''' <remarks>Llamada desde=Page_Load; Tiempo máximo=0seg.</remarks>
    Private Sub CargarEstados()
        Dim oItem As New Infragistics.Web.UI.ListControls.DropDownItem

        oItem.Value = 1
        oItem.Text = pag.Textos(14)  'Sin solicitar 
        wddEstados.Items.Add(oItem)
        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 2
        oItem.Text = pag.Textos(15)   'Pendiente de cumplimentar
        wddEstados.Items.Add(oItem)
        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 6
        oItem.Text = pag.Textos(22)   'Pendiente de Validar ::: WEBFSWSTEXT -> MODULO 140 - ID 23 - Pendiente de validar
        wddEstados.Items.Add(oItem)
        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 7
        oItem.Text = pag.Textos(23)   'Pendiente de Validar ::: WEBFSWSTEXT -> MODULO 140 - ID 24 - No Validado
        wddEstados.Items.Add(oItem)
        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 3
        oItem.Text = pag.Textos(16)   'Vigentes
        wddEstados.Items.Add(oItem)
        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 4
        oItem.Text = pag.Textos(17)   'Próximos a expirar
        wddEstados.Items.Add(oItem)
        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 5
        oItem.Text = pag.Textos(18)   'Expirados
        wddEstados.Items.Add(oItem)
        oItem = New Infragistics.Web.UI.ListControls.DropDownItem

    End Sub

    ''' <summary>
    ''' Carga el desplegable con los peticionarios. Si se le pasa el id de un filtro, solo se obtendrán
    ''' los peticionarios de las solicitudes que utilizan el formulario relacionado con el filtro.
    ''' </summary>
    ''' <remarks>LLamada desde el Page_Load y desde la propiedad Filtro; Tiempo máximo: 0sg</remarks>
    Private Sub CargarPeticionarios()
        Dim oCertificado As FSNServer.Certificado

        oCertificado = pag.FSNServer.Get_Object(GetType(FSNServer.Certificado))
        wddPeticionario.TextField = "PETICIONARIO"
        wddPeticionario.ValueField = "PER"

        wddPeticionario.DataSource = oCertificado.CargarPeticionarios(pag.Usuario.Cod, _
            pag.Usuario.QARestCertifUsu, pag.Usuario.QARestCertifUO, pag.Usuario.QARestCertifDep, _
            pag.Usuario.QARestProvMaterial, pag.Usuario.QARestProvEquipo, pag.Usuario.QARestProvContacto)

        wddPeticionario.DataBind()
        oCertificado = Nothing
    End Sub

    ''' <summary>
    ''' Carga la combo con los Tipos de Certificado
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,3seg.</remarks>
    Private Sub CargarTiposSolicitudes()

        Dim oSolicitudes As FSNServer.Solicitudes

        wddTipoSolicitudes.Items.Clear()

        oSolicitudes = pag.FSNServer.Get_Object(GetType(FSNServer.Solicitudes))
        oSolicitudes.LoadCertificados(pag.Idioma, , pag.FSNUser.Cod, Me.chkVerDadosBaja.Checked, _
            RestricSolicUsu:=pag.FSNUser.QARestCertifUsu, RestricSolicUO:=pag.FSNUser.QARestCertifUO, _
            RestricSolicDep:=pag.FSNUser.QARestCertifDep)

        wddTipoSolicitudes.TextField = "DEN"
        wddTipoSolicitudes.ValueField = "ID"

        wddTipoSolicitudes.DataSource = oSolicitudes.Data

        wddTipoSolicitudes.DataBind()

        oSolicitudes = Nothing
    End Sub

    ''' <summary>
    ''' Carga la combo con los materiales QA
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,3seg.</remarks>
    Private Sub CargarMateriales()

        Dim oMaterialesQA As FSNServer.MaterialesQA
        oMaterialesQA = pag.FSNServer.Get_Object(GetType(FSNServer.MaterialesQA))

        wddMateriales.TextField = "DEN_" & pag.Idioma
        wddMateriales.ValueField = "ID"

        wddMateriales.DataSource = oMaterialesQA.GetData(pag.Idioma, , False, pag.FSNUser.Pyme, pag.FSNUser.QARestProvMaterial, pag.FSNUser.CodPersona)

        wddMateriales.DataBind()

        oMaterialesQA = Nothing
    End Sub
#End Region

#Region "Fns Auxiliares"
    ''' <summary>
    ''' Nos indica si un nodo esta dentro de una lista o no
    ''' </summary>
    ''' <param name="listaElementos">Cadena de nodos seleccionada</param>
    ''' <param name="valorABuscar">Valor del nodo a buscar en la lista</param>        
    ''' <returns>True o False si a enontrado o no el nodo</returns>
    ''' <remarks>Llamada desde=SyncChanges//CargarCertificados; Tiempo máximo=0,2seg.</remarks>
    Private Function buscarElemento(ByVal listaElementos As String, ByVal valorABuscar As String) As Boolean
        Dim pos As Integer
        Dim elementosSeleccionados() As String
        Dim bEncontrado As Boolean = False
        pos = InStr(listaElementos, valorABuscar)
        If pos > 0 Then
            If Len(listaElementos) = Len(valorABuscar) Then 'El nodo coincide con la seleccion
                Return True
            Else
                'If pos - 1 > 0 Then
                bEncontrado = False
                elementosSeleccionados = Split(listaElementos, ",")
                For i = 0 To UBound(elementosSeleccionados)
                    If elementosSeleccionados(i) = valorABuscar Then
                        bEncontrado = True
                        Exit For
                    End If
                Next
                Return bEncontrado
                'Else
                '    Return True

                'End If

            End If
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Carga los controles con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load; Tiempo máximo:=0seg.</remarks>
    Private Sub CargarIdiomasControles()
        pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.BusquedaAvanzadaCert

        lblProveedores.Text = pag.Textos(0)
        lblEstados.Text = pag.Textos(1)
        lblPeticionario.Text = pag.Textos(2)
        chkProveedorDeBaja.Text = pag.Textos(3)
        lblFechas.Text = pag.Textos(4)

        lblFSol.Text = pag.Textos(5)
        lblFlim.Text = pag.Textos(6)
        lblFAct.Text = pag.Textos(7)
        lblFExp.Text = pag.Textos(8)

        lblDesde.Text = pag.Textos(9)
        lblHasta.Text = pag.Textos(10)

        lblMaterial.Text = pag.Textos(19)
        lblTipoCert.Text = pag.Textos(20)
        chkVerDadosBaja.Text = pag.Textos(21)
    End Sub

    ''' <summary>
    ''' Inicializa los componentes y funciones javascript de los elementos
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0seg.</remarks>
    Private Sub InicializacionControles()
        txtProveedor.Attributes.Add("onkeydown", "javascript:return eliminarProveedor(event)")

        imgProveedorLupa.Attributes.Add("onClick", "javascript:BuscarProveedor();return false;")

        'Funcion que sirve para eliminar el Proveedor 
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarProveedor") Then
            Dim sScript As String = vbCrLf & _
            "function eliminarProveedor(event) {" & vbCrLf & _
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                    " return true; " & vbCrLf & _
            " } else { " & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    " var sCampo ='" & txtProveedor.ClientID & "'" & vbCrLf & _
                    " var sCampoHid='" & hidProveedor.ClientID & "'" & vbCrLf & _
                    " campo = document.getElementById(sCampo)" & vbCrLf & _
                    " if (campo) {  " & vbCrLf & _
                    "   campo.value = ''; " & vbCrLf & _
                    "   campoHidden = document.getElementById(sCampoHid)" & vbCrLf & _
                    "   if (campoHidden) { campoHidden.value = ''; } " & vbCrLf & _
                    " } " & vbCrLf & _
                    " else " & vbCrLf & _
                    " { " & vbCrLf & _
                    "   sCampo = sCampo.replace(""Uno"", ""Todos"")" & vbCrLf & _
                    "   sCampoHid = sCampoHid.replace(""Uno"", ""Todos"")" & vbCrLf & _
                    "   campo = document.getElementById(sCampo)" & vbCrLf & _
                    "   if (campo) { campo.value = ''; }" & vbCrLf & _
                    "   campoHidden = document.getElementById(sCampoHid)" & vbCrLf & _
                    "   if (campoHidden) { campoHidden.value = ''; } " & vbCrLf & _
                    " } " & vbCrLf & _
            "   } " & vbCrLf & _
            " return false; " & vbCrLf & _
            " }" & vbCrLf & _
            " }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarProveedor", sScript, True)
        End If
        
        chkVerDadosBaja.Visible = True

        If Not Page.ClientScript.IsClientScriptBlockRegistered("BuscarProveedor") Then
            Dim sScript As String = "function BuscarProveedor() {" & vbCrLf & _
            "   var combo = '" & wddMostrarProveedores.ClientID & "'" & vbCrLf & _
            "   var txProve = '" & txtProveedor.ClientID & "'" & vbCrLf & _
            "   var hdProve = '" & hidProveedor.ClientID & "'" & vbCrLf & _
            "   var chProve = '" & chkProveedorDeBaja.ClientID & "'" & vbCrLf & _
            "   var EsUno = document.getElementById('" & hidIdentidadPanel.ClientID & "')" & vbCrLf & _
            "   if (EsUno) {}" & vbCrLf & _
            "   else { " & vbCrLf & _
            "       combo = combo.replace('Uno' , 'Todos')" & vbCrLf & _
            "       txProve = txProve.replace('Uno' , 'Todos')" & vbCrLf & _
            "       hdProve = hdProve.replace('Uno' , 'Todos')" & vbCrLf & _
            "       chProve = chProve.replace('Uno' , 'Todos')" & vbCrLf & _
            "   } " & vbCrLf & _
            "   var wddMostrarProveedores = $find(combo) " & vbCrLf & _
            "   var rutaURL = """"" & vbCrLf & _
            "   if (wddMostrarProveedores.get_selectedItem()){ " & vbCrLf & _
            "       var iTipo=wddMostrarProveedores.get_selectedItem().get_value()" & vbCrLf & _
            "       rutaURL='" & ConfigurationManager.AppSettings("RutaFS") & "_common/BuscadorProveedores.aspx?PM=false&desde=VisorCert&IdControl=' + txProve + '&IdControlHid=' + hdProve + '&Tipo=' + iTipo + '&Baja=';" & vbCrLf & _
            "   }else{ " & vbCrLf & _
            "       rutaURL='" & ConfigurationManager.AppSettings("RutaFS") & "_common/BuscadorProveedores.aspx?PM=false&desde=VisorCert&IdControl=' + txProve + '&IdControlHid=' + hdProve + '&Tipo=3&Baja=';" & vbCrLf & _
            "   } " & vbCrLf & _
            "   if (document.getElementById(chProve).checked){ " & vbCrLf & _
            "       rutaURL = rutaURL + '0'" & vbCrLf & _
            "   }else{ " & vbCrLf & _
            "       rutaURL = rutaURL + '1'" & vbCrLf & _
            "   } " & vbCrLf & _
            "   window.open(rutaURL,'_blank', 'width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes') " & vbCrLf & _
            " }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "BuscarProveedor", sScript, True)
        End If
    End Sub

#End Region

#Region "Eventos Ctrls Pagina"
    ''' <summary>
    ''' Evento que salta al cargarse la pagina.
    ''' Inicializa los controles. Carga los combos con los datos.
    ''' </summary>
    ''' <param name="sender">propios del evento</param>
    ''' <param name="e">propios del evento</param>          
    ''' <remarks>Tiempo máximo:1seg.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If msCarga = "VisorNC" Then Exit Sub

        pag = CType(Me.Page, FSNPage)
        pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.BusquedaAvanzadaCert

        InicializacionControles()

        If IsPostBack Then Exit Sub

        CargarIdiomasControles()

        CargarTiposSolicitudes()
        CargarMateriales()

        CargarMostrarProveedores()
        CargarEstados()
        CargarPeticionarios()


    End Sub

    ''' <summary>
    ''' Recarga el combo de Certificados 
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo máximo=0,2seg.</remarks>
    Protected Sub chkVerDadosBaja_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkVerDadosBaja.CheckedChanged
        CargarTiposSolicitudes()
        Me.wddTipoSolicitudes.CurrentValue = ""

        upTipoSolicitudes.Update()
    End Sub

#End Region

#Region "Acceder a la pagina por primera vez ..."
    ''' <summary>
    ''' Al acceder a la página por primera vez, mostraremos por defecto el resultado de buscar todos los materiales, todos los tipos de 
    ''' certificados y todos los proveedores (potenciales y del panel de calidad), y ordenaremos los resultados por fecha de solicitud 
    ''' de más a menos recientes.
    ''' </summary>
    ''' <remarks>Llamada desde: Certificados\Certificados.aspx.vb/LimpiarBusquedaAvanzada
    ''' FiltrosQA\ConfigurarFiltrosQA.aspx.vb/Page_PreRender; Tiempo máximo:0,1</remarks>
    Public Sub CargaPorDefecto()
        Dim sTexto As String = ""

        wddMostrarProveedores.SelectedValue = 3

        ''--restos ctrls
        Me.chkVerDadosBaja.Checked = False

        Me.txtProveedor.Text = ""
        Me.hidProveedor.Value = ""
        Me.chkProveedorDeBaja.Checked = False

        Me.wddEstados.SelectedItems.Clear()

        Me.wddPeticionario.SelectedItems.Clear()

        Me.optFecSol.Checked = True
        Me.dtDesde.Value = Nothing
        Me.dtHasta.Value = Nothing
    End Sub
#End Region


End Class