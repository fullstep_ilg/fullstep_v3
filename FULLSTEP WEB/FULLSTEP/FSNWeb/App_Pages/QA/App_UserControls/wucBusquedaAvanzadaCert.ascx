﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="wucBusquedaAvanzadaCert.ascx.vb" Inherits="Fullstep.FSNWeb.wucBusquedaAvanzadaCert" %>
<asp:Panel ID="PnlBusquedaAvanzadaInt" runat="server" Width="100%">
<table width="100%" style="table-layout:fixed" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<table width="100%" class="Rectangulo" border="0">
				<tr>
					<td width="80px"><asp:Label id="lblMaterial" runat="server" Text="dMaterial:" Font-Bold="true"></asp:Label></td>
					<td width="320px">
						<ig:WebDropDown ID="wddMateriales" runat="server" Width="98%" EnableMultipleSelection="true" 
							EnableClosingDropDownOnSelect="false" DropDownContainerWidth="298px" 
							EnableDropDownAsChild="false" CurrentValue="">
						</ig:WebDropDown>
					</td>                
					<td width="50px"><asp:label id="lblTipoCert" runat="server" Font-Bold="true" Text="DTipo:"></asp:label></td>
					<td width="320px">  
						<asp:UpdatePanel ID="upTipoSolicitudes" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<ig:WebDropDown ID="wddTipoSolicitudes" runat="server" Width="98%" EnableMultipleSelection="true" 
								EnableClosingDropDownOnSelect="false" DropDownContainerWidth="296px" 
								EnableDropDownAsChild="false" CurrentValue="">
							</ig:WebDropDown>
						</ContentTemplate>	
						</asp:UpdatePanel> 				                   
					</td>
					<td width="250px">
						<asp:CheckBox id="chkVerDadosBaja" Text="dVer tipos de Certificados de baja" runat="server" AutoPostBack="true" OnCheckedChanged="chkVerDadosBaja_CheckedChanged" />
					</td>                
				</tr>
			</table> 
		</td>
	</tr>
	<tr>
		<td  style="padding-bottom:5px; width:40%;">
			<fieldset>
				<legend style="height:100%"><asp:Label ID="lblProveedores" runat="server" Text="DProveedores"></asp:Label></legend>
				<table width="100%">
					<tr>
						<td style="width:50%;">
							<ig:WebDropDown ID="wddMostrarProveedores" runat="server" Width="95%" EnableMultipleSelection="false" 
								EnableClosingDropDownOnSelect="true" DropDownContainerWidth="250px" 
								EnableDropDownAsChild="false" CurrentValue="">
							</ig:WebDropDown>                        
						</td>
						<td style="width:50%;">
							<table style="background-color:White;width:200px;height:20px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<asp:TextBox ID="txtProveedor" runat="server" Width="175px" BorderWidth="0px"></asp:TextBox>
									</td>
									<td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
										<asp:ImageButton id="imgProveedorLupa" runat="server" SkinID="BuscadorLupa" /> 
									</td>
								</tr>                        
								<asp:HiddenField ID="hidProveedor" runat="server" />      
								<asp:HiddenField ID="hidIdentidadPanel" runat="server" />            
							</table>                           
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<asp:CheckBox id="chkProveedorDeBaja" Text="dMostrar proveedores dados de baja" runat="server"/>
						</td>
					</tr>
				</table>                                                 
			</fieldset>
		</td>
		<td style="padding-bottom:5px; width:30%;">
			<fieldset>
				<legend style="height:100%"><asp:Label ID="lblEstados" runat="server" Text="DEstado"></asp:Label></legend>
				<table width="100%">
					<tr style="height:47px;">
						<td valign="top">                
							<ig:WebDropDown ID="wddEstados" runat="server" Width="95%"  EnableMultipleSelection="true" 
								EnableClosingDropDownOnSelect="false" DropDownContainerWidth="294px" 
								EnableDropDownAsChild="false" CurrentValue="">
							</ig:WebDropDown>
						</td>
					</tr>                                                 
				</table>                
			</fieldset>                
		</td>
		<td style="padding-bottom:5px; width:30%;">
			<fieldset>
				<legend style="height:100%"><asp:Label ID="lblPeticionario" runat="server" Text="DPeticionario"></asp:Label></legend>
				<table width="100%">
					<tr style="height:47px;">
						<td valign="top">  
							<ig:WebDropDown ID="wddPeticionario" runat="server" Width="95%"  EnableMultipleSelection="true" 
								EnableClosingDropDownOnSelect="false" DropDownContainerWidth="294px" 
								EnableDropDownAsChild="false" CurrentValue="">
							</ig:WebDropDown>                        
						</td>
					</tr>                                                   
				</table>
			</fieldset>
		</td>        
	</tr>    
	<tr>
		<td colspan="2">
			<fieldset>
				<legend style="vertical-align:top"><asp:Label ID="lblFechas" runat="server" Text="dFechas"></asp:Label></legend>
				<table style="height:60px" width="95%">
					<tr>
						<td><asp:RadioButton runat="server" ID="optFecSol" GroupName="TipoFecha" /><asp:label id="lblFSol" Text="dF.Solicitud/Renovación/Envio" runat="server"/></td>
						<td><asp:RadioButton runat="server" ID="optFecLim" GroupName="TipoFecha" /><asp:label id="lblFlim" Text="dF.limite cumplimentación" runat="server"/></td>
						<td><asp:label id="lblDesde" Text="dDesde" runat="server" /></td>
						<td><igpck:WebDatePicker runat="server" ID="dtDesde" Width="150px" SkinID="Calendario"></igpck:WebDatePicker></td>
					</tr>
					
					<tr>
						<td><asp:RadioButton runat="server" ID="optFecAct" GroupName="TipoFecha" /><asp:label id="lblFAct" Text="dF.actualización" runat="server"/></td>
						<td><asp:RadioButton runat="server" ID="optFecExp" GroupName="TipoFecha" /><asp:label id="lblFExp" Text="dF.Expiración" runat="server"/></td>
						<td><asp:label id="lblHasta" Text="dHasta" runat="server" /></td>
						<td><igpck:WebDatePicker runat="server" ID="dtHasta" Width="150px" SkinID="Calendario"></igpck:WebDatePicker></td>
					</tr>                                                        
				</table>
			</fieldset>
		</td>
		<td></td>
	</tr>   
</table>

</asp:Panel>