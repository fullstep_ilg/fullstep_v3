﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MostrarPanelArbol.ascx.vb" Inherits="Fullstep.FSNWeb.MostrarPanelArbol" %>
    <asp:Panel ID="pnlProveedor_Peticionario" runat="server" style="display:none; position:absolute;width:400px;height:305px;
    max-height:305px; max-width:400px; overflow: auto;">	    
	    <div style='width:100%;z-index:10000; position:absolute;'>
            <div style='width:100%; height:35px;' class='CapaTituloPanelInfo'>&nbsp;</div>
            <div style='width:100%; height:20px;' class='CapaSubTituloPanelInfo'>&nbsp;</div>
        </div>       
        <table border="0" style="z-index:10002;width:100%; max-width:400px;position:absolute;border-bottom: #8c8c8c 1px solid;border-left: #8c8c8c 1px solid;border-top: #8c8c8c 1px solid; border-right: #8c8c8c 1px solid;" cellpadding="2" cellspacing="0">
            <tr class='CapaTituloPanelInfo'>
                <td valign="bottom" align="left" style="width:270px;padding-left:80px">
                    <asp:UpdatePanel ID="upTitulo" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label runat="server" ID="lblTituloPanelInfo" Text="Proveedor" CssClass="TituloPopUpPanelInfo"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>                    
                </td>
                <td valign="top" style="text-align: right">
                    <asp:Button ID="Button3" runat="server" Height="0" Text="Button" Visible="true" Width="0" style="display:none" />
                    <asp:ImageButton ID="ImgBtnDetCerrarProveedor_Peticionario" runat="server" SkinID="Cerrar" ToolTip="Cerrar Detalle"/>
	            </td>
            </tr>
            <tr class='CapaSubTituloPanelInfo'>
                <td valign="bottom" align="left" style="width:270px;padding-left:80px">
                    <asp:Label runat="server" ID="lblSubTituloPanelInfo" Text="Información detallada" CssClass="SubTituloPopUpPanelInfo"></asp:Label></td>
                <td style="width:50px"></td>
            </tr>
            <tr style="background-color:#F5F5F5; max-width:400px;" >
                <td colspan="2">
                    <asp:UpdatePanel ID="upDetalleProveedor_Peticionario" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>	                                                   
                            <asp:Panel id="pnlDetalleProveedor_Peticionario" runat="server">
			                    <asp:Label ID="lblInfoPanel" runat="server" CssClass="Normal"></asp:Label>
			                </asp:Panel>				      
                        </ContentTemplate>        
                    </asp:UpdatePanel>                    
                </td>
            </tr>   
            <tr runat="server" style="background-color:#F5F5F5; max-width:400px; max-height:200px; overflow: auto;">         
                <td colspan="2">
                    <asp:UpdatePanel ID="upMatGS" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:TreeView ID="tvMatGS" runat="server">
                                <DataBindings>
                                    <asp:TreeNodeBinding DataMember="System.Data.DataRowView" TextField="DEN" ValueField="ID"
                                        SelectAction="None" />
                                </DataBindings>
                            </asp:TreeView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <table style="background:transparent;position:absolute;z-index:10003; max-width:400px;">
            <tr>
                <td valign="top" align="left" style="padding:0px;width:80px;height:60px">
                    <asp:Image ID="imgPanelInfoPeticionario" runat="server" SkinID="PopUpProveedor" Visible="false" />
                    <asp:Image ID="imgPanelInfoProveedor" runat="server" SkinID="PopUpProveedor" Visible="true" />
                </td>
            </tr>                
        </table>
    </asp:Panel>
    <ajx:ModalPopupExtender ID="mpeProveedor_Peticionario" CancelControlID="ImgBtnDetCerrarProveedor_Peticionario"
        Enabled="true" OkControlID="Button3"  PopupControlID="pnlProveedor_Peticionario" 
        TargetControlID="Button3" RepositionMode="RepositionOnWindowResizeAndScroll" runat="server">
    </ajx:ModalPopupExtender>   
