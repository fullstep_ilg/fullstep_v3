﻿Public Partial Class MostrarPanelArbol
    Inherits System.Web.UI.UserControl

    Private m_sTipo As String
    Private m_sCodigo As String
    Private m_sMateriales As String
    Private m_bMatGs As Boolean
    Private pag As FSNPage

    ''' <summary>
    ''' Si es un modal popup q muestra información de proveedor o peticionario
    ''' </summary>
    ''' <returns>muestra información de proveedor o peticionario</returns>
    ''' <remarks>Llamada desde: Visor Certificados; Tiempo:0</remarks>
    Public Property Tipo() As String
        Get
            Tipo = m_sTipo
        End Get
        Set(ByVal value As String)
            m_sTipo = value
        End Set
    End Property

    ''' <summary>
    ''' Codigo del proveedor o peticionario a mostrar en el modal popup
    ''' </summary>
    ''' <returns>Codigo del proveedor o peticionario</returns>
    ''' <remarks>Llamada desde: Visor Certificados; Tiempo:0</remarks>
    Public Property Codigo() As String
        Get
            Codigo = m_sCodigo
        End Get
        Set(ByVal value As String)
            m_sCodigo = value
        End Set
    End Property

    ''' <summary>
    ''' Material Qa del proveedor a mostrar en el modal popup
    ''' </summary>
    ''' <returns>Material Qa del proveedor a mostrar en el modal popup</returns>
    ''' <remarks>Llamada desde: Visor Certificados; Tiempo:0</remarks>
    Public Property Materiales()
        Get
            Materiales = m_sMateriales
        End Get
        Set(ByVal value)
            m_sMateriales = value
        End Set
    End Property

    ''' <summary>
    ''' Carga la pagina
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde: Sistema; Tiempo máximo:0seg.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pag = CType(Me.Page, FSNPage)
    End Sub

    ''' <summary>
    ''' Muestra las paneles de informacion del proveedor o peticionario.
    '''      Tipo =  ("Proveedor") --> Panel datos Proveedor
    '''      Tipo =  ("Peticionario") --> Panel datos Peticionario
    ''' </summary>
    ''' <remarks>Llamada desde: VisorCertificados; Tiempo máximo: 0,3</remarks>
    Public Sub MostrarPanel()
        If Codigo = "null" Then Exit Sub

        Dim sTexto As String = ""
        pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorCertificados
        If Tipo = "Proveedor" Then
            lblTituloPanelInfo.Text = pag.Textos(18) '"Proveedor"
            imgPanelInfoPeticionario.Visible = False
            imgPanelInfoProveedor.Visible = True
            Dim oProve As Fullstep.FSNServer.Proveedor = pag.FSNServer.Get_Object(GetType(FSNServer.Proveedor))
            oProve.Cod = Codigo
            oProve.LoadDatosQA(pag.FSNUser.Idioma)

            sTexto = "<br/>"
            sTexto = sTexto & "<b>" & pag.Textos(29) & ":</b> " & oProve.Cod & "<br/>"
            sTexto = sTexto & "<b>" & pag.Textos(30) & ":</b> " & oProve.Den & "<br/>"
            sTexto = sTexto & "<b>" & pag.Textos(31) & ":</b> " & oProve.NIF & "<br/>"
            sTexto = sTexto & "<b>" & pag.Textos(32) & ":</b> " & oProve.Dir & "<br/>"
            sTexto = sTexto & "<b>" & pag.Textos(33) & ":</b> " & oProve.CP & "<br/>"
            sTexto = sTexto & "<b>" & pag.Textos(34) & ":</b> " & oProve.Pob & "<br/>"
            sTexto = sTexto & "<b>" & pag.Textos(35) & ":</b> " & oProve.PaiDen & "<br/>"
            sTexto = sTexto & "<b>" & pag.Textos(36) & ":</b> " & oProve.ProviDen & "<br/>"
            sTexto = sTexto & "<b>" & pag.Textos(37) & ":</b> " & oProve.MonCod & " - " & DBNullToSomething(oProve.MonDen) & "<br/>"

            sTexto = sTexto & "<b>" & pag.Textos(38) & ": </b>" & _
                              "<a class=""Normal"" href=""mailto:" & oProve.Email & """>" & oProve.Email & "</a><br/>"
            oProve = Nothing

            Me.tvMatGS.Visible = True

            CargarMatGs()

        ElseIf Tipo = "Peticionario" Then
            Dim oPersona As Fullstep.FSNServer.Persona = pag.FSNServer.Get_Object(GetType(FSNServer.Persona))
            oPersona.LoadData(Codigo, True)

            lblTituloPanelInfo.Text = pag.Textos(22) '"Peticionario"            

            imgPanelInfoPeticionario.Visible = True
            imgPanelInfoProveedor.Visible = False

            With oPersona
                sTexto = "<br/>"
                sTexto = sTexto & "<b>" & pag.Textos(29) & ":</b> " & .Codigo & "<br/>"
                sTexto = sTexto & "<b>" & pag.Textos(39) & ":</b> " & .Nombre & "<br/>"
                sTexto = sTexto & "<b>" & pag.Textos(40) & ":</b> " & .Apellidos & "<br/>"
                sTexto = sTexto & "<b>" & pag.Textos(41) & ":</b> " & .Cargo & "<br/>"
                sTexto = sTexto & "<b>" & pag.Textos(42) & ":</b> " & _
                            "<a class=""Normal"" href=""callto:" & .Telefono & """>" & .Telefono & "</a><br/>"
                sTexto = sTexto & "<b>" & pag.Textos(43) & ":</b> " & .Fax & "<br/>"
                sTexto = sTexto & "<b>" & pag.Textos(44) & ":</b> " & _
                         "<a class=""Normal"" href=""mailto:" & .EMail & """>" & .EMail & "</a><br/>"
                sTexto = sTexto & "<b>" & pag.Textos(45) & ":</b> " & .DenDepartamento & "<br/>"
                sTexto = sTexto & "<b>" & pag.Textos(46) & ":</b> " & .UONs & "<br/>"
            End With
            Me.tvMatGS.Visible = False
        End If

        If sTexto <> "" Then
            lblSubTituloPanelInfo.Text = pag.Textos(47)
            ImgBtnDetCerrarProveedor_Peticionario.ToolTip = pag.Textos(48)
            lblInfoPanel.Text = sTexto
            upTitulo.Update()
            upDetalleProveedor_Peticionario.Update()
            mpeProveedor_Peticionario.Show()
        End If
    End Sub

    ''' <summary>
    ''' Carga un arbol con los diferentes materiales de qa del proveedor
    ''' </summary>
    ''' <remarks>Llamada desde: MostrarPanel; Tiempo:0</remarks>
    Private Sub CargarMatGs()
        Dim PMProveedor As FSNServer.Proveedor = pag.FSNServer.Get_Object(GetType(FSNServer.Proveedor))
        Dim ds As DataSet = PMProveedor.DevolverMaterialesGS_hds(Codigo, Materiales, pag.Idioma, True)

        tvMatGS.DataSource = New HierarchicalDataSet(ds.Tables(0).DefaultView, "ID", "PADRE")
        tvMatGS.DataBind()
        tvMatGS.CollapseAll()

        upMatGS.Update()
    End Sub

End Class