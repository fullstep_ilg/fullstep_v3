﻿Partial Public Class wucBusquedaAvanzadaNC
    Inherits System.Web.UI.UserControl


    'Private FSPMServer As FSNServer.Root
    Private mlFiltro As Long = 0
    Private oTextos As DataTable
    Private pag As FSNPage
    Private msCarga As String = ""

#Region " PROPIEDADES "

    Public Property Titulo() As String
        Get
            Titulo = txtTitulo.Text
        End Get
        Set(ByVal Value As String)
            txtTitulo.Text = Value
        End Set
    End Property


    Public Property Tipo() As Long '(Seleccion Multiple)
        Get
            If wddTipo.SelectedValue <> Nothing Then
                Tipo = CLng(wddTipo.SelectedValue)
            Else
                Tipo = 0
            End If

        End Get
        Set(ByVal Value As Long)
            wddTipo.CurrentValue = ""
            If Value > 0 Then
                wddTipo.SelectedValue = Value
                SubTipo = ""
                CargarSubtipo(Value)
            Else
                wddTipo.SelectedValue = "0"
                wddTipo.SelectedItemIndex = -1
            End If

        End Set
    End Property

    Public Property SubTipo() As String '(Seleccion Multiple)
        Get
            Dim sSubTipo As String = ""
            For i = 0 To wddSubtipo.Items.Count - 1
                If wddSubtipo.Items(i).Selected Then
                    If sSubTipo <> "" Then sSubTipo += ","
                    sSubTipo = sSubTipo + wddSubtipo.Items(i).Value
                End If
            Next
            SubTipo = sSubTipo
        End Get
        Set(ByVal Value As String)
            Dim sTexto As String = ""
            If Value <> "" Then
                For i = 0 To wddSubtipo.Items.Count - 1
                    If wddSubtipo.Items(i).Value <> "" Then
                        If buscarElemento(Value, wddSubtipo.Items(i).Value) Then
                            wddSubtipo.Items(i).Selected = True
                            If sTexto <> "" Then sTexto += ","
                            sTexto = sTexto + wddSubtipo.Items(i).Text
                        End If
                    End If
                Next
            Else
                If wddSubtipo.SelectedItems.Count > 0 Then
                    For i = 0 To wddSubtipo.Items.Count - 1
                        wddSubtipo.Items(i).Selected = False
                    Next
                End If
            End If
            wddSubtipo.CurrentValue = sTexto
        End Set
    End Property


    Public Property Proveedor() As String '(Seleccion Multiple)

        Get
            Dim sProveedores As String = ""
            For i = 0 To wddProveedor.Items.Count - 1
                If wddProveedor.Items(i).Selected Then
                    If sProveedores <> "" Then sProveedores += ","
                    sProveedores = sProveedores + wddProveedor.Items(i).Value
                End If
            Next
            Proveedor = sProveedores
        End Get

        Set(ByVal Value As String)
            Dim sTexto As String = ""
            If Value <> "" Then
                For i = 0 To wddProveedor.Items.Count - 1
                    If wddProveedor.Items(i).Value <> "" Then
                        If buscarElemento(Value, wddProveedor.Items(i).Value) Then
                            wddProveedor.Items(i).Selected = True
                            If sTexto <> "" Then sTexto += ","
                            sTexto = sTexto + wddProveedor.Items(i).Text
                        End If
                    End If
                Next
            Else
                If wddProveedor.SelectedItems.Count > 0 Then
                    For i = 0 To wddProveedor.Items.Count - 1
                        wddProveedor.Items(i).Selected = False
                    Next
                End If
            End If
            'If sTexto = "" Then
            '    sTexto = "(Cualquier Proveedor)"
            'End If
            wddProveedor.CurrentValue = sTexto
        End Set
    End Property

    Public Property UnidadNegocio() As String '(Seleccion Multiple)
        Get
            Dim sUnidadNegocio As String = ""
            For i = 0 To wddUNQA.Items.Count - 1
                If wddUNQA.Items(i).Selected Then
                    If sUnidadNegocio <> "" Then sUnidadNegocio += ","
                    sUnidadNegocio = sUnidadNegocio + wddUNQA.Items(i).Value
                End If
            Next
            UnidadNegocio = sUnidadNegocio
        End Get

        Set(ByVal Value As String)
            Dim sTexto As String = ""
            If Value <> "" Then
                For i = 0 To wddUNQA.Items.Count - 1
                    If wddUNQA.Items(i).Value <> "" Then
                        If buscarElemento(Value, wddUNQA.Items(i).Value) Then
                            wddUNQA.Items(i).Selected = True
                            If sTexto <> "" Then sTexto += ","
                            sTexto = sTexto + wddUNQA.Items(i).Text
                        End If
                    End If
                Next
            Else
                If wddUNQA.SelectedItems.Count > 0 Then
                    For i = 0 To wddUNQA.Items.Count - 1
                        wddUNQA.Items(i).Selected = False
                    Next
                End If
            End If
            wddUNQA.CurrentValue = sTexto
        End Set
    End Property


    Public Property Peticionario() As String '(Seleccion Multiple)
        Get
            Dim sPeticionario As String = ""
            For i = 0 To wddPeticionario.Items.Count - 1
                If wddPeticionario.Items(i).Selected Then
                    If sPeticionario <> "" Then sPeticionario += ","
                    sPeticionario = sPeticionario + wddPeticionario.Items(i).Value
                End If
            Next
            Peticionario = sPeticionario
        End Get

        Set(ByVal Value As String)
            Dim sTexto As String = ""
            If Value <> "" Then
                For i = 0 To wddPeticionario.Items.Count - 1
                    If wddPeticionario.Items(i).Value <> "" Then
                        If buscarElemento(Value, wddPeticionario.Items(i).Value) Then
                            wddPeticionario.Items(i).Selected = True
                            If sTexto <> "" Then sTexto += ","
                            sTexto = sTexto + wddPeticionario.Items(i).Text
                        End If
                    End If
                Next
            Else
                If wddPeticionario.SelectedItems.Count > 0 Then
                    For i = 0 To wddPeticionario.Items.Count - 1
                        wddPeticionario.Items(i).Selected = False
                    Next
                End If
            End If
            'If sTexto = "" Then
            '    sTexto = "(Cualquier peticionario)"
            'End If
            wddPeticionario.CurrentValue = sTexto
        End Set
    End Property

    Public Property Revisor() As String '(Seleccion Multiple)
        Get
            Dim sRevisor As String = ""
            For i = 0 To wddRevisor.Items.Count - 1
                If wddRevisor.Items(i).Selected Then
                    If sRevisor <> "" Then sRevisor += ","
                    sRevisor = sRevisor + wddRevisor.Items(i).Value
                End If
            Next
            Revisor = sRevisor
        End Get

        Set(ByVal Value As String)
            Dim sTexto As String = ""
            If Value <> "" Then

                For i = 0 To wddRevisor.Items.Count - 1
                    If wddRevisor.Items(i).Value <> "" Then
                        If buscarElemento(Value, wddRevisor.Items(i).Value) Then
                            wddRevisor.Items(i).Selected = True
                            If sTexto <> "" Then sTexto += ","
                            sTexto = sTexto + wddRevisor.Items(i).Text
                        End If
                    End If
                Next
            Else
                If wddRevisor.SelectedItems.Count > 0 Then
                    For i = 0 To wddRevisor.Items.Count - 1
                        wddRevisor.Items(i).Selected = False
                    Next
                End If
            End If
            'If sTexto = "" Then
            '    sTexto = "(Cualquier revisor)"
            'End If
            wddRevisor.CurrentValue = sTexto
        End Set
    End Property


    Public Property MaterialTexto() As String
        Get
            MaterialTexto = txtMaterial.Text
        End Get
        Set(ByVal Value As String)
            txtMaterial.Text = Value
        End Set
    End Property
    'Almacenar para la BBDD
    Public Property MaterialHidden() As String
        Get
            MaterialHidden = hidMaterial.Value
        End Get
        Set(ByVal Value As String)
            '(gmn1-gmn2-gmn3-gmn4)
            hidMaterial.Value = Value
        End Set
    End Property

    Public Property ArticuloTexto() As String
        Get
            ArticuloTexto = txtArticulo.Text
        End Get
        Set(ByVal Value As String)
            txtArticulo.Text = Value
        End Set
    End Property
    'Almacenar para la BBDD
    Public Property ArticuloHidden() As String
        Get
            ArticuloHidden = hidArticulo.Value
        End Get
        Set(ByVal Value As String)
            '(gmn1-gmn2-gmn3-gmn4)
            hidArticulo.Value = Value
        End Set
    End Property

    Public Property FechaAlta() As Boolean
        Get
            FechaAlta = chkFechaAlta.Checked
        End Get
        Set(ByVal Value As Boolean)
            chkFechaAlta.Checked = Value
        End Set
    End Property

    Public Property FechaActualizacion() As Boolean
        Get
            FechaActualizacion = chkFechaActualizacion.Checked
        End Get
        Set(ByVal Value As Boolean)
            chkFechaActualizacion.Checked = Value
        End Set
    End Property
    Public Property FechaLimiteResolucion() As Boolean
        Get
            FechaLimiteResolucion = chkFechaLimiteResolucion.Checked
        End Get
        Set(ByVal Value As Boolean)
            chkFechaLimiteResolucion.Checked = Value
        End Set
    End Property
    Public Property FechaCierre() As Boolean
        Get
            FechaCierre = chkFechaCierre.Checked
        End Get
        Set(ByVal Value As Boolean)
            chkFechaCierre.Checked = Value
        End Set
    End Property



    Public Property FechaDesde() As Date
        Get
            FechaDesde = IIf(dteDesde.Value <> Nothing, dteDesde.Value, CDate(Nothing))
        End Get
        Set(ByVal Value As Date)
            dteDesde.Value = Value
        End Set
    End Property

    Public Property FechaHasta() As Date
        Get
            FechaHasta = IIf(dteHasta.Value <> Nothing, dteHasta.Value, CDate(Nothing))
        End Get
        Set(ByVal Value As Date)
            dteHasta.Value = Value
        End Set
    End Property


    Public Property EstadoNCAbiertas() As String '(Seleccion Multiple)
        Get
            Dim sEstados As String = ""
            For i = 0 To wddEstado.Items.Count - 1
                If wddEstado.Items(i).Selected Then
                    If sEstados <> "" Then sEstados += ","
                    sEstados = sEstados + wddEstado.Items(i).Value
                End If
            Next
            EstadoNCAbiertas = sEstados
        End Get

        Set(ByVal Value As String)
            Dim sTexto As String = ""
            If Value <> "" Then
                For i = 0 To wddEstado.Items.Count - 1
                    If wddEstado.Items(i).Value <> "" Then
                        If buscarElemento(Value, wddEstado.Items(i).Value) Then
                            wddEstado.Items(i).Selected = True
                            If sTexto <> "" Then sTexto += ","
                            sTexto = sTexto + wddEstado.Items(i).Text
                        End If
                    End If
                Next
            Else
                If wddEstado.SelectedItems.Count > 0 Then
                    For i = 0 To wddEstado.Items.Count - 1
                        wddEstado.Items(i).Selected = False
                    Next
                End If
            End If
            wddEstado.CurrentValue = sTexto
        End Set

    End Property



    Public WriteOnly Property Filtro() As Long
        Set(ByVal Value As Long)
            mlFiltro = Value
        End Set
    End Property


    ''' <summary>
    ''' Establece si hay q cargar los componentes o no, si es q no, pq? pq se esta configurando un certificado
    ''' </summary>
    ''' <remarks>Llamada desde: ConfigurarFiltroQA InicializarArraysCamposGeneralesNC y  InicializarArraysCamposGeneralesCert; Tiempo máximo:0</remarks>
    Public Property Carga() As String
        Get
            Carga = msCarga
        End Get
        Set(ByVal Value As String)
            msCarga = Value
        End Set
    End Property

    ''' <summary>
    ''' Ver proveedores dados de baja
    ''' </summary>
    ''' <returns>Ver proveedores dados de baja o no</returns>
    ''' <remarks>Llamada desde: CargarProveedores; Tiempo maximo:0</remarks>
    Public Property ProveedorDeBaja() As Boolean
        Get
            ProveedorDeBaja = chkProveedorDeBaja.Checked
        End Get
        Set(ByVal Value As Boolean)
            chkProveedorDeBaja.Checked = Value
        End Set
    End Property
#End Region

#Region "Carga de datos. Carga Combos"
    ''' <summary>
    ''' Proceso que carga el combo de proveedores. Ordenados por la denominacion
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,3seg.</remarks>
    Private Sub CargarProveedores()
        Dim oProveedores As FSNServer.Proveedores
        If Not IsNothing(pag) Then
            oProveedores = pag.FSNServer.Get_Object(GetType(FSNServer.Proveedores))
            oProveedores.Carga_Proveedores_QA(sUser:=pag.Usuario.Cod,
                                            bConNoConformidades:=True,
                                            bFuerzaBaja:=IIf(Me.chkProveedorDeBaja.Checked, False, True),
                                            bOrdenacionDenominacion:=True,
                                            RestricProvMat:=pag.Usuario.QARestProvMaterial,
                                            RestricProvEqp:=pag.Usuario.QARestProvEquipo,
                                            RestricProvCon:=pag.Usuario.QARestProvContacto)
            wddProveedor.DataSource = oProveedores.Data
            wddProveedor.TextField = "DEN"
            wddProveedor.ValueField = "COD"
            wddProveedor.DataBind()
        End If
        oProveedores = Nothing

    End Sub

    ''' <summary>
    ''' Proceso que carga el combo de Unidades de Negocio.
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,3seg.</remarks>
    Private Sub CargarUnidadesNegocio()
        Dim oUnidadesQA As FSNServer.UnidadesNeg

        oUnidadesQA = pag.FSNServer.Get_Object(GetType(FSNServer.UnidadesNeg))

        oUnidadesQA.UsuLoadData(pag.Usuario.Cod, pag.Usuario.Idioma, 3, TipoAccesoUNQAS.ConsultarNoConformidades)

        Dim dt As DataTable = oUnidadesQA.MontaDataSetUnaBanda()

        wddUNQA.DataSource = dt
        wddUNQA.TextField = "DEN"
        wddUNQA.ValueField = "ID"
        wddUNQA.DataBind()

        oUnidadesQA = Nothing
    End Sub


    ''' <summary>
    ''' Carga el desplegable con los peticionarios. Si se le pasa el id de un filtro, solo se obtendrán
    ''' los peticionarios de las solicitudes que utilizan el formulario relacionado con el filtro.
    ''' </summary>
    ''' <remarks>LLamada desde el Page_Load y desde la propiedad Filtro; Tiempo máximo: 0sg</remarks>
    Private Sub CargarPeticionarios()
        Dim oNoConformidad As FSNServer.NoConformidad

        oNoConformidad = pag.FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
        wddPeticionario.TextField = "PETICIONARIO"
        wddPeticionario.ValueField = "COD"

        wddPeticionario.DataSource = oNoConformidad.CargarPeticionarios(pag.Usuario.Cod, pag.FSNUser.QARestNoConformUsu, pag.FSNUser.QARestNoConformUO, pag.FSNUser.QARestNoConformDep, pag.FSNUser.QARestProvMaterial, pag.FSNUser.QARestProvEquipo, pag.FSNUser.QARestProvContacto)
        wddPeticionario.DataBind()
        oNoConformidad = Nothing
    End Sub

    ''' <summary>
    ''' Proceso que carga el combo de Revisores.
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,3seg.</remarks>
    Private Sub CargarRevisores()
        Dim oNoConformidad As FSNServer.NoConformidad

        oNoConformidad = pag.FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
        wddRevisor.TextField = "REVISOR"
        wddRevisor.ValueField = "COD"

        wddRevisor.DataSource = oNoConformidad.CargarRevisores(pag.FSNUser.Cod, pag.FSNUser.QARestNoConformUsu, pag.FSNUser.QARestNoConformUO, pag.FSNUser.QARestNoConformDep, pag.FSNUser.QARestProvMaterial, pag.FSNUser.QARestProvEquipo, pag.FSNUser.QARestProvContacto)
        wddRevisor.DataBind()
        oNoConformidad = Nothing
    End Sub

    ''' <summary>
    ''' Proceso que carga el combo de Tipo con los tipos de las solicitudes..
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub CargarTiposSolicitudes()
        Dim oSolicitudes As FSNServer.Solicitudes
        oSolicitudes = pag.FSNServer.Get_Object(GetType(FSNServer.Solicitudes))

        oSolicitudes.LoadData(pag.Usuario.Cod, pag.Idioma, bEsCombo:=1, iTipoSolicitud:=TiposDeDatos.TipoDeSolicitud.NoConformidad, sCampoOrdenacion:="DEN")

        Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem

        For Each oRow As DataRow In oSolicitudes.Data.Tables(0).Rows
            oItem = New Infragistics.Web.UI.ListControls.DropDownItem
            oItem.Value = DBNullToDbl(oRow.Item("ID"))
            If IsDBNull(oRow.Item("COD")) Then
                oItem.Text = ""
            Else
                oItem.Text = oRow.Item("COD") & " - " & oRow.Item("DEN")
            End If

            wddTipo.Items.Add(oItem)
        Next


        wddTipo.EnableAutoFiltering = Infragistics.Web.UI.ListControls.AutoFiltering.Client
        wddTipo.AutoFilterQueryType = Infragistics.Web.UI.ListControls.AutoFilterQueryTypes.Contains
        wddTipo.AutoFilterSortOrder = Infragistics.Web.UI.ListControls.DropDownAutoFilterSortOrder.Ascending


        oSolicitudes = Nothing
    End Sub
    ''' <summary>
    ''' Carga la combo con los distintos valores de los campos_gs subtipo de la solicitud
    ''' </summary>
    ''' <param name="lSolicitud">ID Tipo solicitud</param>
    ''' <remarks>Llamada desde=wddSubtipo_ItemsRequested; Tiempo máximo=0seg.</remarks>
    Private Sub CargarSubtipo(ByVal lSolicitud As Integer)
        Dim oFiltro As FSNServer.FiltroQA
        Dim ds As DataSet
        If pag Is Nothing Then
            pag = CType(Me.Page, FSNPage)
        End If
        oFiltro = pag.FSNServer.Get_Object(GetType(FSNServer.FiltroQA))
        wddSubtipo.SelectedValue = ""
        SubTipo = ""
        wddSubtipo.Items.Clear()

        If lSolicitud > 0 Then
            ds = oFiltro.CargarSubtiposSolicitud(lSolicitud, sIdi:=pag.Usuario.Idioma)
            If ds.Tables(0).Rows.Count > 0 Then
                wddSubtipo.DataSource = ds
                wddSubtipo.ValueField = "ORDEN"
                wddSubtipo.TextField = "SUBTIPO"
                wddSubtipo.DataBind()
                wddSubtipo.EnableClosingDropDownOnSelect = False
            Else
                wddSubtipo.EnableClosingDropDownOnSelect = True
            End If
        End If

        oFiltro = Nothing
    End Sub

    ''' <summary>
    ''' Carga la combo de estados con los posibles valores
    ''' </summary>
    ''' <remarks>Llamada desde=Page_Load; Tiempo máximo=0seg.</remarks>
    Private Sub CargarEstadosNoConformidadesAbiertas()
        Dim oItem As New Infragistics.Web.UI.ListControls.DropDownItem

        oItem.Value = 1
        oItem.Text = pag.Textos(17)  ' "Sin respuesta por parte del proveedor"
        wddEstado.Items.Add(oItem)
        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 2
        oItem.Text = pag.Textos(18)  ' "Con acciones pendientes de revisar por el peticionario"
        wddEstado.Items.Add(oItem)
        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 3
        oItem.Text = pag.Textos(19) ' "Con acciones pendientes de finalizar por el proveedor"
        wddEstado.Items.Add(oItem)
        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 4
        oItem.Text = pag.Textos(20)  ' "Con acciones pendientes de finalizar por el peticionario"
        wddEstado.Items.Add(oItem)
        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 5
        oItem.Text = pag.Textos(21)  ' "Pendientes de cerrar"
        wddEstado.Items.Add(oItem)

    End Sub

#End Region


    ''' <summary>
    ''' Evento que salta al cargarse la pagina.
    ''' Inicializa los controles. Carga los combos con los datos.
    ''' </summary>
    ''' <param name="sender">propios del evento</param>
    ''' <param name="e">propios del evento</param>          
    ''' <remarks>Tiempo máximo:1seg.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If msCarga = "VisorCert" Then Exit Sub

        pag = CType(Me.Page, FSNPage)
        pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.BusquedaAvanzadaNC

        If Not Page.ClientScript.IsClientScriptBlockRegistered("cambioTipo") Then
            Dim sScript As String = "function cambioTipo(sender, eventArgs) {" & vbCrLf & _
                " var combo2 = $find('" & wddSubtipo.ClientID & "')" & vbCrLf & _
                    " combo2.loadItems();" & vbCrLf & _
                " } " & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "cambioTipo", sScript, True)
        End If


        If IsPostBack Then Exit Sub

        CargarIdiomasControles()


        'Proveedores
        CargarProveedores()

        'Unidades de Negocio
        CargarUnidadesNegocio()

        'Tipos de solicitudes
        CargarTiposSolicitudes()

        CargarPeticionarios()

        'Cargar Revisores
        CargarRevisores()

        'Estado NoConformidades Abiertas
        CargarEstadosNoConformidadesAbiertas()

        InicializacionControles()



    End Sub


    ''' <summary>
    ''' Carga los controles con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load; Tiempo máximo:=0seg.</remarks>
    Private Sub CargarIdiomasControles()


        pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.BusquedaAvanzadaNC


        lblTitulo.Text = pag.Textos(0) & ":"
        lblTipo.Text = pag.Textos(1) & ":"
        lblSubtipo.Text = pag.Textos(2) & ":"

        lblProveedor.Text = pag.Textos(3) & ":"
        lblUNQA.Text = pag.Textos(4) & ":"
        lblPeticionario.Text = pag.Textos(5) & ":"

        lblRevisor.Text = pag.Textos(6) & ":"
        lblMaterial.Text = pag.Textos(7) & ":"
        lblArticulo.Text = pag.Textos(8) & ":"

        lblFechas.Text = pag.Textos(9)
        chkFechaAlta.Text = pag.Textos(10)
        chkFechaActualizacion.Text = pag.Textos(11)
        chkFechaLimiteResolucion.Text = pag.Textos(12)
        chkFechaCierre.Text = pag.Textos(13)

        lblDesde.Text = pag.Textos(14)
        lblHasta.Text = pag.Textos(15)

        lblEstados.Text = pag.Textos(16)

        chkProveedorDeBaja.Text = pag.Textos(22)
    End Sub

    ''' <summary>
    ''' Inicializa los componentes y funciones javascript de los elementos
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0seg.</remarks>
    Private Sub InicializacionControles()

        imgMaterialLupa.Attributes.Add("onClick", "javascript:window.open('" & ConfigurationManager.AppSettings("rutaPM") & "_common/materiales.aspx?desde=WebPart&IdControl=" & Me.ClientID & "_" & txtMaterial.ID & "&MaterialGS_BBDD=" & Me.ClientID & "_" & hidMaterial.ID & "','_blank', 'width=550,height=550,status=yes,resizable=no,top=100,left=200');")
        txtMaterial.Attributes.Add("onkeydown", "javascript:return eliminarMaterial(event)")

        txtArticulo.Attributes.Add("onkeydown", "javascript:return eliminarArticulo(event)")

        'Funcion que sirve para eliminar el material y el articulo relacionado
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarMaterial") Then
            Dim sScript As String = "function eliminarMaterial(event) {" & vbCrLf & _
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                    " return true; " & vbCrLf & _
            " } else { " & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    " campo = document.getElementById('" & txtMaterial.ClientID & "')" & vbCrLf & _
                    " if (campo) { campo.value = ''; } " & vbCrLf & _
                    " campoHidden = document.getElementById('" & hidMaterial.ClientID & "')" & vbCrLf & _
                    " if (campoHidden) { campoHidden.value = ''; } " & vbCrLf & _
                    " campoArticulo = document.getElementById('" & txtArticulo.ClientID & "')" & vbCrLf & _
                    " if (campoArticulo) { campoArticulo.value = ''; } " & vbCrLf & _
                    " campoHiddenArticulo = document.getElementById('" & hidArticulo.ClientID & "')" & vbCrLf & _
                    " if (campoHiddenArticulo) { campoHiddenArticulo.value = ''; } " & vbCrLf & _
            " } " & vbCrLf & _
            " return false; " & vbCrLf & _
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarMaterial", sScript, True)
        End If

        'Funcion que sirve para eliminar el articulo relacionado
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarArticulo") Then
            Dim sScript As String = "function eliminarArticulo(event) {" & vbCrLf & _
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                    " return true; " & vbCrLf & _
            " } else { " & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    " campoArticulo = document.getElementById('" & txtArticulo.ClientID & "')" & vbCrLf & _
                    " if (campoArticulo) { campoArticulo.value = ''; } " & vbCrLf & _
                    " campoHiddenArticulo = document.getElementById('" & hidArticulo.ClientID & "')" & vbCrLf & _
                    " if (campoHiddenArticulo) { campoHiddenArticulo.value = ''; } " & vbCrLf & _
            " } " & vbCrLf & _
            " return false; " & vbCrLf & _
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarArticulo", sScript, True)
        End If

        'Funcion que sirve para abrir el buscador de articulos 
        Dim sCodArticuloID As String = Me.ClientID & "_" & hidArticulo.ID
        Dim sDenArticuloID As String = Me.ClientID & "_" & txtArticulo.ID
        Dim sMaterialBD As String = Me.ClientID & "_" & hidMaterial.ID
        Dim sMaterialDen As String = Me.ClientID & "_" & txtMaterial.ID

        If Not Page.ClientScript.IsClientScriptBlockRegistered("openArticulo") Then
            Dim sOpen As String = "window.open('" & ConfigurationManager.AppSettings("RutaFS") & "_common/BuscadorArticulos.aspx?desde=WucBusquedaNC&IdMaterialBD=" & sMaterialBD & "&IdMaterialDen=" & sMaterialDen & "&IdControlCodArt=" & sCodArticuloID & "&IdControlDenArt=" & sDenArticuloID & "&TabContainer=" & Me.ID

            Dim sScript As String = "function openArticulo() {" & vbCrLf & _
            "   var MatAct = document.getElementById('" & hidMaterial.ClientID & "').value;" & vbCrLf & _
            "   " & sOpen & "&mat=' + MatAct, '_blank', 'width=850,height=630,status=yes,resizable=no,top=200,left=200');" & vbCrLf & _
            "   return false;" & vbCrLf & _
            "}" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "return openArticulo();", sScript, True)

            imgArticuloLupa.Attributes.Add("onClick", "openArticulo()")
        End If

        wddTipo.ClientEvents.DropDownClosing = "cambioTipo"

        dteDesde.NullText = ""

        dteHasta.NullText = ""

        chkProveedorDeBaja.Visible = True
    End Sub

    ''' <summary>
    ''' Evento que salta al cambiar el valor de la combo TIPO. Carga los valores de subtipo relacionados con el tipo seleccionado.
    ''' </summary>
    ''' <param name="sender">propios del evento</param>
    ''' <param name="e">propios del evento</param>        
    ''' <remarks>Llamada desde; Tiempo máximo=0,1seg.</remarks>
    Private Sub wddSubtipo_ItemsRequested(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownItemsRequestedEventArgs) Handles wddSubtipo.ItemsRequested
        Dim lSolicitud As Long = 0

        If IsNumeric(wddTipo.SelectedValue) Then
            lSolicitud = wddTipo.SelectedValue
        End If

        If lSolicitud > 0 Then
            CargarSubtipo(lSolicitud)
        Else
            wddSubtipo.Items.Clear()
        End If
    End Sub

    ''' <summary>
    ''' Nos indica si un nodo esta dentro de una lista o no
    ''' </summary>
    ''' <param name="listaElementos">Cadena de nodos seleccionada</param>
    ''' <param name="valorABuscar">Valor del nodo a buscar en la lista</param>        
    ''' <returns>True o False si a enontrado o no el nodo</returns>
    ''' <remarks>Llamada desde=SyncChanges//CargarCertificados; Tiempo máximo=0,2seg.</remarks>
    Private Function buscarElemento(ByVal listaElementos As String, ByVal valorABuscar As String) As Boolean
        Dim pos As Integer
        Dim elementosSeleccionados() As String
        Dim bEncontrado As Boolean = False
        pos = InStr(listaElementos, valorABuscar)
        If pos > 0 Then
            If Len(listaElementos) = Len(valorABuscar) Then 'El nodo coincide con la seleccion
                Return True
            Else
                If pos - 1 > 0 Then
                    bEncontrado = False
                    elementosSeleccionados = Split(listaElementos, ",")
                    For i = 0 To UBound(elementosSeleccionados)
                        If elementosSeleccionados(i) = valorABuscar Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next
                    Return bEncontrado
                Else
                    Return True

                End If

            End If
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Recarga el combo de Certificados 
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo máximo=0,2seg.</remarks>
    Protected Sub chkProveedorDeBaja_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkProveedorDeBaja.CheckedChanged
        CargarProveedores()
        Me.wddProveedor.CurrentValue = ""

        upProveedor.Update()
    End Sub

    Public Sub CambioFiltro_ProveedorBaja()
        CargarProveedores()
        Me.wddProveedor.CurrentValue = ""

        upProveedor.Update()
    End Sub

End Class