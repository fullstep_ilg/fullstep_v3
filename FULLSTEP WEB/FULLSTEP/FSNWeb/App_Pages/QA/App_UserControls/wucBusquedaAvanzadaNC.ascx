﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="wucBusquedaAvanzadaNC.ascx.vb"
    Inherits="Fullstep.FSNWeb.wucBusquedaAvanzadaNC" %>
<script type="text/javascript">
    //la funcion ValueChanged_wddTipo_CambioTipo esta en las paginas donde se ha introducido el componente
</script>
<asp:Panel ID="PnlBusquedaAvanzadaInt" runat="server" Width="100%">
    <table width="100%" style="table-layout: fixed" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td style="width: 80px; padding-top: 5px; padding-left: 5px">
                <asp:Label ID="lblTitulo" runat="server" Text="dTitulo:" Font-Bold="true"></asp:Label>
            </td>
            <td style="width: 350px; padding-top: 5px;" colspan="2">
                <asp:TextBox ID="txtTitulo" runat="server" Width="200px"></asp:TextBox>
            </td>
            <td style="width: 80px; padding-top: 5px;">
                <asp:Label ID="lblTipo" runat="server" Text="dTipo" Font-Bold="true"></asp:Label>
            </td>
            <td style="width: 200px; padding-top: 5px;">
                <div style="background-color: White; width: 200px">
                    <ig:WebDropDown ID="wddTipo" runat="server" Width="200px" EnableClosingDropDownOnSelect="true"
                        DropDownAnimationDuration="1" DropDownContainerWidth="200" EnableDropDownAsChild="false"
                        CurrentValue="">
                        <ClientEvents ValueChanged="ValueChanged_wddTipo_CambioTipo" />
                    </ig:WebDropDown>
                </div>
            </td>
            <td style="width: 80px; padding-top: 5px;">
                <asp:Label ID="lblSubtipo" runat="server" Text="dSubtipo" Font-Bold="true" Visible="true"></asp:Label>
            </td>
            <td style="width: 200px; padding-top: 5px;">
                <div style="background-color: White; width: 200px">
                    <ig:WebDropDown ID="wddSubtipo" runat="server" Width="200px" TextField="Text" ValueField="Value"
                        EnableAutoCompleteFirstMatch="false" DropDownContainerWidth="200px" DropDownContainerHeight="200px"
                        EnableMultipleSelection="true" EnableDropDownAsChild="false" CurrentValue="">
                    </ig:WebDropDown>
                </div>
            </td>
        </tr>
        <tr>
            <td style="width: 80px; padding-top: 5px; padding-bottom: 5px; padding-left: 5px">
                <asp:Label ID="lblProveedor" runat="server" Text="dProveedor:" Font-Bold="true"></asp:Label>
            </td>
            <td style="padding-top: 5px; padding-bottom: 5px;">                                               
                <div style="background-color: White; width: 200px;">
                    <asp:UpdatePanel ID="upProveedor" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>                    
                        <ig:WebDropDown ID="wddProveedor" runat="server" Width="200px" EnableMultipleSelection="true"
                        EnableClosingDropDownOnSelect="false" DropDownContainerWidth="200px" EnableDropDownAsChild="false"
                        CurrentValue="">
                        </ig:WebDropDown>                    
                    </ContentTemplate>	
                    </asp:UpdatePanel> 	
                </div>                                
            </td>
            <td style="width: 150px; padding-top: 5px; padding-bottom: 5px;">
                <asp:CheckBox id="chkProveedorDeBaja" Text="dVer proveedores de baja" runat="server" AutoPostBack="true" OnCheckedChanged="chkProveedorDeBaja_CheckedChanged" />
            </td>
            <td style="width: 80px; padding-top: 5px; padding-bottom: 5px;">
                <asp:Label ID="lblUNQA" runat="server" Text="dU.negocio:" Font-Bold="true"></asp:Label>
            </td>
            <td style="width: 200px; padding-top: 5px; padding-bottom: 5px;">
                <div style="background-color: White; width: 200px">
                    <ig:WebDropDown ID="wddUNQA" runat="server" Width="200px" EnableMultipleSelection="true"
                        EnableClosingDropDownOnSelect="false" DropDownAnimationDuration="1" EnableDropDownAsChild="false"
                        CurrentValue="">
                    </ig:WebDropDown>
                </div>
            </td>
            <td style="width: 80px; padding-top: 5px; padding-bottom: 5px;">
                <asp:Label ID="lblPeticionario" runat="server" Text="dPeticionario:" Font-Bold="true"></asp:Label>
            </td>
            <td style="width: 200px; padding-top: 5px; padding-bottom: 5px;">
                <div style="background-color: White; width: 200px">
                    <ig:WebDropDown ID="wddPeticionario" runat="server" Width="200px" EnableMultipleSelection="true"
                        EnableClosingDropDownOnSelect="false" EnableDropDownAsChild="false" CurrentValue="">
                    </ig:WebDropDown>
                </div>
            </td>
        </tr>
        <tr>
            <td style="padding-bottom: 5px; padding-left: 5px">
                <asp:Label ID="lblRevisor" runat="server" Text="dRevisor:" Font-Bold="true"></asp:Label>
            </td>
            <td style="padding-bottom: 5px;" colspan="2">
                <div style="background-color: White; width: 200px">
                    <ig:WebDropDown ID="wddRevisor" runat="server" Width="200px" EnableMultipleSelection="true"
                        EnableClosingDropDownOnSelect="false" EnableDropDownAsChild="false" CurrentValue="">
                    </ig:WebDropDown>
                </div>
            </td>
            <td style="padding-bottom: 5px;">
                <asp:Label ID="lblMaterial" runat="server" Text="dMaterial:" Font-Bold="true"></asp:Label>
            </td>
            <td style="padding-bottom: 5px;">
                <table style="background-color: White; width: 200px; height: 20px; border: solid 1px #BBBBBB"
                    cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:TextBox ID="txtMaterial" runat="server" Width="175px" BorderWidth="0px"></asp:TextBox>
                        </td>
                        <td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
                            <asp:ImageButton ID="imgMaterialLupa" runat="server" SkinID="BuscadorLupa" />
                        </td>
                    </tr>
                    <asp:HiddenField ID="hidMaterial" runat="server" />
                </table>
            </td>
            <td style="padding-bottom: 5px;">
                <asp:Label ID="lblArticulo" runat="server" Text="dArticulo:" Font-Bold="true"></asp:Label>
            </td>
            <td style="padding-bottom: 5px;">
                <table style="background-color: White; width: 200px; height: 20px; border: solid 1px #BBBBBB"
                    cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:TextBox ID="txtArticulo" runat="server" Width="175px" BorderWidth="0px"></asp:TextBox>
                        </td>
                        <td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
                            <asp:ImageButton ID="imgArticuloLupa" runat="server" SkinID="BuscadorLupa" />
                        </td>
                    </tr>
                    <asp:HiddenField ID="hidArticulo" runat="server" />
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="7" style="padding-top: 5px; padding-bottom: 5px; padding-left: 5px;
                padding-right: 15px">
                <table width="100%">
                    <tr>
                        <td>
                            <fieldset>
                                <legend style="vertical-align: top">
                                    <asp:Label ID="lblFechas" runat="server" Text="dFechas"></asp:Label></legend>
                                <table style="height: 60px" width="95%">
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkFechaAlta" Text="dF. alta" runat="server" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkFechaActualizacion" Text="F.actualización" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDesde" Text="dDesde" runat="server" />
                                        </td>
                                        <td>
                                            <igpck:WebDatePicker runat="server" ID="dteDesde" Width="200px" SkinID="Calendario">
                                            </igpck:WebDatePicker>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkFechaLimiteResolucion" Text="dF. límite resolución" runat="server" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkFechaCierre" Text="F.cierre" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblHasta" Text="dHasta" runat="server" />
                                        </td>
                                        <td>
                                            <igpck:WebDatePicker runat="server" ID="dteHasta" Width="200px" SkinID="Calendario">
                                            </igpck:WebDatePicker>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                        <td>
                            <fieldset>
                                <legend>
                                    <asp:Label ID="lblEstados" runat="server" Text="Estado de las acciones"></asp:Label></legend>
                                <table style="height: 60px;" width="95%">
                                    <tr>
                                        <td align="center">
                                            <div style="background-color: White; width: 300px; text-align: left">
                                                <ig:WebDropDown runat="server" ID="wddEstado" EnableMultipleSelection="true" MultipleSelectionType="Checkbox"
                                                    EnableClosingDropDownOnSelect="false" Width="300px" DropDownContainerWidth="300px"
                                                    EnableDropDownAsChild="false" CurrentValue="">
                                                </ig:WebDropDown>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
