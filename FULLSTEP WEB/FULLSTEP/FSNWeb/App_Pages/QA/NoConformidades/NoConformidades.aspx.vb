﻿Imports System.Web.Script.Serialization
Imports Fullstep.FSNServer
Imports Infragistics.Web.UI.GridControls

Partial Public Class NoConformidades
    Inherits FSNPage

    Private arrDatosGenerales() As String
    Private arrDatosGeneralesDen() As String
    Private arrOrdenEstados(8) As String
    Private arrTextosEstados(8) As String
    Private arrTextosEstadosMini(8) As String
    Private arrTextosComentarios(8) As String
    Private m_bMostrarComentario As Boolean
    Private m_bMostrarIndicadorEnProceso As Boolean
    Private m_bExportando As Boolean = False
    Private m_sTextoCargando As String = "Cargando..."
    Private m_sTextoEnProceso As String = "La no conformidad está siendo tramitada en estos momentos. En breves instantes estará disponible para su gestión."
    Private m_bConConfiguracionAnterior As Boolean = False
    Private _pageNumber As Integer = 0
    Private _rowsPage As Integer = 50
    Private _NoConfAprobar As List(Of DatosNoConf)
    Private _NoConfRechazar As List(Of DatosNoConf)
#Region "Inicializacion..."
    ''' <summary>
    ''' Inicializa los arrays con los campos generales
    ''' </summary>
    ''' <remarks>Llamada desde=Page_Load uwgNoConformidades_InitializeLayout// ; Tiempo máximo:0seg.</remarks>
    Private Sub InicializarArraysCamposGenerales()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorNoConformidades
        If Session("arrDatosGenerales") Is Nothing Then
            'NOMBRE DE LAS COLUMNAS DE LA GRID!!!
            ReDim arrDatosGenerales(22)
            arrDatosGenerales(1) = "ID"
            arrDatosGenerales(2) = "TIPO"
            arrDatosGenerales(3) = "TITULO"
            arrDatosGenerales(4) = "CODPROVE"
            arrDatosGenerales(5) = "DENPROVE"
            arrDatosGenerales(6) = "UNQA"
            arrDatosGenerales(7) = "PET"
            arrDatosGenerales(8) = "REVISOR"
            arrDatosGenerales(9) = "FECALTA"
            arrDatosGenerales(10) = "FEC_ANULACION"
            arrDatosGenerales(11) = "FEC_LIM_RESOL"
            arrDatosGenerales(12) = "FEC_EMISION"
            arrDatosGenerales(13) = "FEC_RESPUESTA"
            arrDatosGenerales(14) = "FECACT"
            arrDatosGenerales(15) = "FEC_CIERRE"
            arrDatosGenerales(16) = "COMENT"
            arrDatosGenerales(17) = "COMENT_ALTA"
            arrDatosGenerales(18) = "COMENT_CIERRE"
            arrDatosGenerales(19) = "COMENT_REVISOR"
            arrDatosGenerales(20) = "COMENT_ANULACION"
            arrDatosGenerales(21) = "ESTADO_ACCIONES"
            arrDatosGenerales(22) = "PROVE_ERP"

            Session("arrDatosGenerales") = arrDatosGenerales

            ReDim arrDatosGeneralesDen(22)
            For i = 1 To 9
                arrDatosGeneralesDen(i) = Textos(i + 18)
            Next
            arrDatosGeneralesDen(10) = Textos(67) '"Fecha anulación"
            For i = 11 To 19
                arrDatosGeneralesDen(i) = Textos(i + 17)
            Next
            arrDatosGeneralesDen(20) = Textos(68) '"Comentario anulación"
            arrDatosGeneralesDen(21) = Textos(75) '"Estado de las acciones"
            arrDatosGeneralesDen(22) = Textos(85) 'Proveedor en ERP
            Session("arrDatosGeneralesDen") = arrDatosGeneralesDen

            arrOrdenEstados(0) = EstadosNoConformidad.Guardada
            arrOrdenEstados(1) = EstadosNoConformidad.Abierta
            arrOrdenEstados(2) = EstadosNoConformidad.CierrePdteRevisar
            arrOrdenEstados(3) = EstadosNoConformidad.CierrePosIN
            arrOrdenEstados(4) = EstadosNoConformidad.CierrePosOUT
            arrOrdenEstados(5) = EstadosNoConformidad.CierreNoEficaz
            arrOrdenEstados(6) = EstadosNoConformidad.Anulada
            arrOrdenEstados(7) = EstadosNoConformidad.Todas
            arrOrdenEstados(8) = EstadosNoConformidad.EnCursoDeAprobacion
            Session("arrOrdenEstados") = arrOrdenEstados

            arrTextosEstados(0) = Textos(9)  '"Guardadas"
            arrTextosEstados(1) = Textos(8)  '"Abiertas"
            arrTextosEstados(2) = Textos(11) '"Cierre eficaz dentro de plazo"
            arrTextosEstados(3) = Textos(12) '"Cierre eficaz fuera de plazo"
            arrTextosEstados(4) = Textos(13) '"Cierre no eficaz"
            arrTextosEstados(5) = Textos(10) '"Pendiente de revisar cierre"
            arrTextosEstados(6) = Textos(14) '"Todas"
            arrTextosEstados(7) = Textos(69) '"Anuladas"
            arrTextosEstados(8) = Textos(86) '"En curso de aprobación"
            Session("arrTextosEstados") = arrTextosEstados

            arrTextosEstadosMini(0) = Textos(9)  '"Guardadas"
            arrTextosEstadosMini(1) = Textos(8)  '"Abiertas"
            arrTextosEstadosMini(2) = Textos(70) '"Cierre eficaz dentro plazo"
            arrTextosEstadosMini(3) = Textos(71) '"Cierre eficaz fuera plazo" 
            arrTextosEstadosMini(4) = Textos(13) '"Cierre no eficaz"
            arrTextosEstadosMini(5) = Textos(72) '"Pdte revisar cierre"
            arrTextosEstadosMini(6) = Textos(14) '"Todas"
            arrTextosEstadosMini(7) = Textos(73) '"Anuladas"
            arrTextosEstadosMini(8) = Textos(87) '"En curso de aprobación"
            Session("arrTextosEstadosMini") = arrTextosEstadosMini

            arrTextosComentarios(0) = Textos(34) 'Comentario apertura
            arrTextosComentarios(1) = Textos(44) 'Usuario
            arrTextosComentarios(2) = Textos(35) 'Comentario cierre
            arrTextosComentarios(3) = Textos(36) 'Comentario revisor
            arrTextosComentarios(4) = Textos(26) 'Revisor
            arrTextosComentarios(5) = Textos(43) 'Fecha
            arrTextosComentarios(6) = Textos(64) 'Informacion Detallada
            arrTextosComentarios(7) = Textos(65) 'Cerrar detalle
            arrTextosComentarios(8) = Textos(68) 'Comentario anulación            
            Session("arrTextosComentarios") = arrTextosComentarios
        Else
            arrDatosGenerales = CType(Session("arrDatosGenerales"), Array)
            arrDatosGeneralesDen = CType(Session("arrDatosGeneralesDen"), Array)
            arrTextosEstados = CType(Session("arrTextosEstados"), Array)
            arrOrdenEstados = CType(Session("arrOrdenEstados"), Array)
            arrTextosComentarios = CType(Session("arrTextosComentarios"), Array)
            arrTextosEstadosMini = CType(Session("arrTextosEstadosMini"), Array)
        End If
    End Sub
    ''' <summary>
    ''' Pone el texto correspondiente en la etiqueta lblProcesando.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: El prerender de la etiqueta. Tiempo máximo: 0sg.</remarks>
    Protected Sub LblProcesando_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, Label).Text = m_sTextoCargando 'Cargando...
    End Sub
    ''' <summary>
    ''' Evento que se lanza al acceder a datos y que usamos para decirle al origen de datos cual va a ser su objeto.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: En la carga de la página VisorSolicitudes.aspx; Tiempo máximo: 0 sg.</remarks>
    Private Sub odsFiltrosConfigurados_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles odsFiltrosConfigurados.ObjectCreating
        Dim FiltrosQA As FSNServer.FiltrosQA
        FiltrosQA = FSNServer.Get_Object(GetType(FSNServer.FiltrosQA))
        FiltrosQA.Tipo = 1
        e.ObjectInstance = FiltrosQA
    End Sub
    ''' <summary>
    ''' Evento que se lanza cuando el objeto de origen de datos recupera los datos. Lo usamos para pasarle los parámetros que necesita su método SELECT configurado.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: En la carga de la página VisorSolicitudes.aspx; Tiempo máximo: 0 sg.</remarks>
    Private Sub odsFiltrosConfigurados_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsFiltrosConfigurados.Selecting
        e.InputParameters.Item("sPersona") = FSNUser.CodPersona
    End Sub
    ''' <summary>
    ''' Carga de los elementos con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo=0,2</remarks>
    Private Sub CargarIdiomas()
        FSNPageHeader.TituloCabecera = Textos(0)
        'lblTitulo.Text = Textos(0) ' No conformidades
        lblIdentificador.Text = Textos(1) & ":" ' Identificador
        btnBuscarIdentificador.Text = Textos(2) ' Buscar
        lblTipo2.Text = Textos(3) & ":" 'Tipo
        btnAlta.Text = Textos(4) 'Alta de no conformidad
        lblBusquedaAvanzada.Text = Textos(5) 'Seleccione los criterios de búsqueda generales
        btnBuscarBusquedaAvanzada.Text = Textos(2)
        btnLimpiarBusquedaAvanzada.Text = Textos(6) 'Limpiar
        btnAnyadirFiltros.Text = Textos(7) 'Añadir filtros

        btnConfigurar.Text = Textos(16) ' Configurar

        whdgNoConformidades.GroupingSettings.EmptyGroupAreaText() = Textos(18) 'Desplace una columna aquí para agrupar por ese concepto'
        m_sTextoEnProceso = Textos(41) '"La no conformidad está siendo tramitada en estos momentos, y no se puede modificar. En breves instantes estará disponible para su gestión."
        m_sTextoCargando = Textos(42)

        ibEnviarMail.AlternateText = Textos(17) 'Enviar email a proveedores
    End Sub
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Carga la pagina
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo máximo:1seg.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorNoConformidades

        FSAL_Personalizado_Para_Visores()

        If Not IsPostBack Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaQA", "<script>var rutaQA = '" & ConfigurationManager.AppSettings("rutaQA") & "' </script>")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaQA2008", "<script>var rutaQA2008 = '" & ConfigurationManager.AppSettings("rutaQA2008") & "' </script>")

            Session("Ordenacion") = ""
            Session("arrDatosGenerales") = Nothing
            InicializarArraysCamposGenerales()

            '1. Configurar opción de Menú
            Seccion = "No Conformidades"
            Dim mMaster = CType(Me.Master, FSNWeb.Menu)
            mMaster.Seleccionar("Calidad", "NoConformidades")
            Dim mMasterTotal As FSNWeb.Cabecera = CType(mMaster.Master, FSNWeb.Cabecera)
            Dim scriptReference As ScriptReference
            scriptReference = New ScriptReference
            scriptReference.Path = ConfigurationManager.AppSettings("ruta") & "js/jsUpdateProgress.js"
            CType(mMasterTotal.FindControl("ScriptManager1"), ScriptManager).Scripts.Add(scriptReference)
            CargarIdiomas()

            CargarEstados()

            DesseleccionarEstados()
            '2. Comprobar si viene de configurar un filtro para cargarle ese filtro por defecto
            cargarSituacionActual()

            '3.Configurar estados a mostrar
            SeleccionarEstado()
            'LA CARGA DE LAS NOCONFORMIDADES LAS HACEMOS UNA VEZ QUE TENGAMOS CARGADA LA PAGINA EN EL EVENTO LOADCOMPLETE!!!!
            '4.Cargar el combo de los tipos de nocnoformidades del alta
            CargarTiposSolicitudes()

            btnConfigurar.Visible = (Session("QAFiltroUsuarioId") <> "")

            whdgNoConformidades.Behaviors.Paging.PageSize = _rowsPage
            whdgNoConformidades.GridView.Behaviors.Paging.PageSize = _rowsPage
        Else
            Select Case Request("__EVENTTARGET")
                Case btnPager.ClientID
                    Dim oSerializer As New JavaScriptSerializer
                    Dim oParam As Object = oSerializer.Deserialize(Of Object)(Request("__EVENTARGUMENT"))
                    _pageNumber = CType(oParam("selectedIndex"), Integer)
                    _NoConfAprobar = oSerializer.Deserialize(Of List(Of DatosNoConf))(oParam("noconfAprobar"))
                    _NoConfRechazar = oSerializer.Deserialize(Of List(Of DatosNoConf))(oParam("noconfRechazar"))
            End Select
            InicializarArraysCamposGenerales()
            If Request("__EVENTARGUMENT") = "Excel" Then
                Exportar(1)
            ElseIf Request("__EVENTARGUMENT") = "Pdf" Then
                Exportar(2)
            End If

            If (Not (CType(Cache("dtNoConformidades_" & FSNUser.Cod), DataTable) Is Nothing) And Not (Request("__EVENTTARGET").Contains("fsnTabFiltro"))) Then
                RecargarGrid()
            End If
        End If

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Notificaciones
        CType(whdgNoConformidades.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(46)
        CType(whdgNoConformidades.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(48)
        'Vuelvo a cargar el modulo de VisorNoConformidades
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorNoConformidades
        FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/noconformidad.png"

        Dim ModalProgressClientID As String = CType(Master.Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalClientID", "<script>var ModalProgress = '" & ModalProgressClientID & "' </script>")

        Dim ProveAnimationClientID As String = FSNPanelDatosProveedorQA.AnimationClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ProveAnimationClientID", "<script>var ProveAnimationClientID = '" & ProveAnimationClientID & "' </script>")

        Dim ProveDynamicPopulateClienteID As String = FSNPanelDatosProveedorQA.DynamicPopulateClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ProveDynamicPopulateClienteID", "<script>var ProveDynamicPopulateClienteID = '" & ProveDynamicPopulateClienteID & "' </script>")

        Dim PeticionarioAnimationClientID As String = FSNPanelDatosPersonaQA.AnimationClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PeticionarioAnimationClientID", "<script>var PeticionarioAnimationClientID = '" & PeticionarioAnimationClientID & "' </script>")

        Dim PeticionarioDynamicPopulateClienteID As String = FSNPanelDatosPersonaQA.DynamicPopulateClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PeticionarioDynamicPopulateClienteID", "<script>var PeticionarioDynamicPopulateClienteID = '" & PeticionarioDynamicPopulateClienteID & "' </script>")

        Dim RevisorAnimationClientID As String = FSNPanelDatosRevisorQA.AnimationClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RevisorAnimationClientID", "<script>var RevisorAnimationClientID = '" & RevisorAnimationClientID & "' </script>")

        Dim RevisorDynamicPopulateClienteID As String = FSNPanelDatosRevisorQA.DynamicPopulateClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RevisorDynamicPopulateClienteID", "<script>var RevisorDynamicPopulateClienteID = '" & RevisorDynamicPopulateClienteID & "' </script>")

        Dim ProcesoAnimationClientID As String = FSNPanelEnProceso.AnimationClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ProcesoAnimationClientID", "<script>var ProcesoAnimationClientID = '" & ProcesoAnimationClientID & "' </script>")

        Dim ProcesoDynamicPopulateClienteID As String = FSNPanelEnProceso.DynamicPopulateClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ProcesoDynamicPopulateClienteID", "<script>var ProcesoDynamicPopulateClienteID = '" & ProcesoDynamicPopulateClienteID & "' </script>")

        Dim ComentAnimationClientID As String = FSNPanelComentarioQA.AnimationClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ComentAnimationClientID", "<script>var ComentAnimationClientID = '" & ComentAnimationClientID & "' </script>")

        Dim ComentDynamicPopulateClienteID As String = FSNPanelComentarioQA.DynamicPopulateClientID
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ComentDynamicPopulateClienteID", "<script>var ComentDynamicPopulateClienteID = '" & ComentDynamicPopulateClienteID & "' </script>")
    End Sub
    ''' <summary>
    ''' Evento que salta despues de cargarse la pagina
    ''' Carga la grid con las noConformidades y limpia el buscador
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo máximo=0,8 (Depende de las noConformidades)</remarks>
    Private Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        'La busqueda avanzada la cargo despues de que se haya cargado la pagina, xq sino no ha cargado El control de usuario y no tiene los
        'valores de las combos, y no carga ningun valor!
        If Not IsPostBack Then
            LimpiarBusquedaAvanzada()
            CargarBusquedaAvanzada()
            RecargarNoConformidades()

            Dim sColsGroupBy As String = "ColsGroupBy" & Session("QAFiltroUsuarioId")
        End If
    End Sub
    ''' <summary>
    ''' Metodo para registrar tiempos de FSAL del cargado del grid principal en la carga total de la pagina y en posteriores cargas parciales.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FSAL_Personalizado_Para_Visores()
        ''Codigo para guardar datos del FSAL recogidos desde FSNLibrary y así poder utilizarlos en el PostBack cuando se cargue el Grid.
        ''Si es primera llamada a la pagina y no se han enviado los tiempos para update, se pasa el idRegistro inicial para localizar el registro a modificar
        ''si no, al hacer una insercion de un nuevo registro se pasa el idRegistro de la carga actual.
        If HttpContext.Current.Items("FECHAPET") IsNot Nothing AndAlso IsDate(HttpContext.Current.Items("FECHAPET")) AndAlso
           HttpContext.Current.Items("IDREGISTRO") IsNot Nothing AndAlso HttpContext.Current.Items("IDREGISTRO") <> "" Then
            bActivadoFSAL.Value = "1"
            Dim fechaIniFSAL8 As DateTime = HttpContext.Current.Items("FECHAPET")
            sFechaIniFSAL8.Value = Format(fechaIniFSAL8, "yyyy-MM-dd HH:mm:ss") & "." & Format(fechaIniFSAL8.Millisecond, "000")
            If bEnviadoFSAL8Inicial.Value = "0" AndAlso Not IsPostBack Then
                sIdRegistroFSAL1.Value = HttpContext.Current.Items("IDREGISTRO")
                sPagina.Value = HttpContext.Current.Request.Url.AbsoluteUri.Split("?")(0) 'para quitar ?=prove=
                iP.Value = HttpContext.Current.Request.UserHostAddress
            ElseIf bEnviadoFSAL8Inicial.Value = "1" AndAlso HttpContext.Current.Request.Headers("x-ajax") Is Nothing Then
                sProducto.Value = System.Configuration.ConfigurationManager.AppSettings("FSAL_Origen") '"FULLSTEP WEB ó FULLSTEP PORTAL"
                sPagina.Value = HttpContext.Current.Request.Url.AbsoluteUri.Split("?")(0) 'para quitar ?=prove=
                iPost.Value = 3
                iP.Value = HttpContext.Current.Request.UserHostAddress
                sUsuCod.Value = DirectCast(Session("FSN_User"), Fullstep.FSNServer.User).Cod
                sPaginaOrigen.Value = sPagina.Value
                sNavegador.Value = HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT") ' Obtener el navegador
                Dim rndRandom As New Random
                sIdRegistroFSAL8.Value = fechaIniFSAL8.Ticks.ToString() & Format(rndRandom.Next(1, 99999999), "00000000")
                sProveCod.Value = ""
                If HttpContext.Current.Request.Url.AbsoluteUri.Split("?").Length > 1 Then
                    sQueryString.Value = HttpContext.Current.Request.Url.AbsoluteUri.Split("?")(1)
                Else
                    sQueryString.Value = ""
                End If
            End If
        End If
        ''
        ''Fin codigo FSAL
    End Sub
#End Region
    ''' <summary>
    ''' Nos lleva a la pagina que lleva el proceso de añadir nuevos filtros de usuario
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo máximo=0,1</remarks>
    Private Sub btnAnyadirFiltros_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnyadirFiltros.Click
        Response.Redirect("..\FiltrosQA\ConfigurarFiltrosQA.aspx?Donde=VisorNC")
    End Sub
    ''' <summary>
    ''' Carga la ultima situacion conocida del usuario.
    ''' </summary>
    ''' <remarks>Llamada desde= Page_load ; Tiempo ejecucion:=0,1</remarks>
    Private Sub cargarSituacionActual()
        Dim FiltroUsuarioIdAnt, NombreTablaAnt, FiltroIdAnt As String

        FiltroUsuarioIdAnt = Request.QueryString("FiltroUsuarioIdAnt")
        NombreTablaAnt = Request.QueryString("NombreTablaAnt")
        FiltroIdAnt = Request.QueryString("FiltroIdAnt")

        Dim arrParametros() As String = Nothing
        Dim bEncontradoTab As Boolean = False
        'Seleccionar el filtro
        If FiltroUsuarioIdAnt <> "" Then 'Ha venido de volver (Seleccionamos el filtro que tenia seleccionado anteriormente)
            m_bConConfiguracionAnterior = True
            Dim sTabSeleccionado As String = FiltroUsuarioIdAnt & "#" & NombreTablaAnt & "#" & FiltroIdAnt
            For index As Integer = 0 To dlFiltrosConfigurados.Items.Count - 1
                If sTabSeleccionado = CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).CommandArgument Then
                    arrParametros = Split(CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).CommandArgument, "#")
                    CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).Selected = True
                    bEncontradoTab = True
                    Exit For
                End If
            Next
        Else
            m_bConConfiguracionAnterior = False
        End If
        If Not bEncontradoTab Then ' Si no encuentra ninguno, o es la primera vez, selecciono el primero
            arrParametros = Split(CType(dlFiltrosConfigurados.Items(0).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).CommandArgument, "#")
            CType(dlFiltrosConfigurados.Items(0).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).Selected = True
        End If

        If arrParametros(0) = 0 Then
            Session("QAFiltroUsuarioId") = ""
            Session("QANombreTabla") = ""
            Session("QAFiltroId") = ""
        Else
            Session("QAFiltroUsuarioId") = arrParametros(0)
            Session("QANombreTabla") = arrParametros(1)
            Session("QAFiltroId") = arrParametros(2)
        End If
        btnConfigurar.Attributes.Add("onClick", "window.location='../FiltrosQA/ConfigurarFiltrosQA.aspx?IDFiltroUsu=" & Session("QAFiltroUsuarioId") & "&NombreTabla=" & Session("QANombreTabla") & "&Donde=VisorNC';return false;")

        QAFiltroUsuarioIdVNoConf.Value = IIf(Session("QAFiltroUsuarioId") = "", 0, Session("QAFiltroUsuarioId"))
        QAFiltroIdVNoConf.Value = IIf(Session("QAFiltroId") = "", 0, Session("QAFiltroId"))

        'Seleccionar el estado
        If Request.QueryString("Estado") <> "" Then
            Session("FilaEstado") = Request("Estado")
        End If

        CargarBusquedaAvanzada()
    End Sub
    ''' <summary>
    ''' Carga la combo con las Tipo de NoConformidades para dar de alta
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,3seg.</remarks>
    Private Sub CargarTiposSolicitudes()
        Dim oSolicitudes As Solicitudes
        oSolicitudes = FSNServer.Get_Object(GetType(FSNServer.Solicitudes))
        oSolicitudes.LoadData(FSNUser.Cod, Idioma, bEsCombo:=1, iTipoSolicitud:=TiposDeDatos.TipoDeSolicitud.NoConformidad, bComboAltaNC:=True, bEscalacion:=True)

        wddTipoSolicitudes.DataSource = oSolicitudes.Data
        wddTipoSolicitudes.TextField = "DEN"
        wddTipoSolicitudes.ValueField = "ID"
        wddTipoSolicitudes.DataBind()

        wddTipoSolicitudes.Items.RemoveAt(0)
        If wddTipoSolicitudes.Items.Count > 0 Then
            wddTipoSolicitudes.SelectedItemIndex = 0
        End If

        oSolicitudes = Nothing
    End Sub
    ''' <summary>
    ''' Función que actualiza el visor cuando se pincha en los diferentes estados. Son los pasos comunes a realizar cuando se cambia de estado.
    ''' Actualiza los contadores de las pestañas, de los estados y las solicitudes del gridview.
    ''' CUANDO SE HACE UNA LECTURA DE AQUI SE HACE UNA LECTURA DE BBDD (NO USA CACHE)
    ''' </summary>
    ''' <remarks>Llamada desde: Los clicks de todos los estados. // Tiempo máximo: 2seg (Segun el nº de noConformidades)
    ''' </remarks>
    Private Sub RecargarNoConformidades()
        Dim ds As New DataSet
        Dim dt As DataTable
        whdgNoConformidades.Rows.Clear()
        whdgNoConformidades.Columns.Clear()
        whdgNoConformidades.GridView.Columns.Clear()
        dt = ObtenerDataTableNoConformidades()
        ds.Tables.Add(dt.Copy())
        whdgNoConformidades.DataSource = ds
        CrearColumnasDT(dt)
        whdgNoConformidades.DataKeyFields = "ID"
        whdgNoConformidades.DataBind()
        ConfigurarGrid()
    End Sub
    Private Sub RecargarGrid()
        Dim ds As New DataSet
        Dim dt As DataTable
        whdgNoConformidades.Rows.Clear()
        whdgNoConformidades.Columns.Clear()
        whdgNoConformidades.GridView.Columns.Clear()
        If Not m_bExportando Then whdgNoConformidades.GridView.Behaviors.Paging.PageIndex = _pageNumber

        dt = CType(Cache("dtNoConformidades_" & FSNUser.Cod), DataTable)
        ds.Tables.Add(dt.Copy())
        whdgNoConformidades.DataSource = ds
        CrearColumnasDT(dt)
        whdgNoConformidades.DataKeyFields = "ID"
        whdgNoConformidades.DataBind()
        ConfigurarGrid()
    End Sub
    ''' <summary>
    ''' Función que actualiza el visor cuando se pincha en los diferentes estados. Son los pasos comunes a realizar cuando se cambia de estado.
    ''' Actualiza los contadores de las pestañas, de los estados y las solicitudes del gridview.
    ''' CUANDO SE HACE UNA LECTURA DE AQUI SE HACE UNA LECTURA DE BBDD (NO USA CACHE)
    ''' </summary>
    ''' <returns>Devuelve datatable con las noConformidades</returns>
    ''' <remarks>Llamada desde: RecargarNoConformidades. // Tiempo máximo: 2seg (Segun el nº de noConformidades)
    ''' </remarks>
    Private Function ObtenerDataTableNoConformidades() As DataTable
        Dim cFiltro As FiltroQA
        cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroQA))
        cFiltro.TipoSol = TiposDeDatos.TipoDeSolicitud.NoConformidad

        Dim iEstado As Integer
        iEstado = CInt(Session("FilaEstado"))
        Dim sGMN1 As String = ""
        Dim sGMN2 As String = ""
        Dim sGMN3 As String = ""
        Dim sGMN4 As String = ""
        Dim lIdentificador As Long = 0
        Dim mi_boton As New Fullstep.FSNWebControls.FSNButton

        If Busqueda.MaterialHidden <> "" Then
            Dim arrMat(4) As String
            arrMat = Split(Busqueda.MaterialHidden, "-")
            For i As Integer = 0 To UBound(arrMat)
                Select Case i
                    Case 0
                        sGMN1 = arrMat(0)
                    Case 1
                        sGMN2 = arrMat(1)
                    Case 2
                        sGMN3 = arrMat(2)
                    Case 3
                        sGMN4 = arrMat(3)
                End Select
            Next
        End If

        If txtIdentificador.Text <> "" Then
            If IsNumeric(txtIdentificador.Text) Then
                lIdentificador = CLng(txtIdentificador.Text)
            End If
        End If
        'Cargar La ordenacion del Filtro (por BBDD) o de la por defecto (por cookie)
        Dim sOrdenacion As String = ""
        If Session("Ordenacion") = "" Then
            If Session("QAFiltroUsuarioId") <> "" Then
                cFiltro.IDFiltroUsuario = Session("QAFiltroUsuarioId")
                cFiltro.cargarOrdenacionFiltro()
                sOrdenacion = cFiltro.Ordenacion
            Else
                Dim cookie As HttpCookie
                cookie = Request.Cookies("ORDENACION_QA")
                If Not cookie Is Nothing Then
                    sOrdenacion = cookie.Value
                End If
            End If
            Session("Ordenacion") = sOrdenacion
        Else
            sOrdenacion = Session("Ordenacion")
        End If

        cFiltro.Visor_DevolverNoConformidades(FSNUser.Cod, FSNUser.Idioma, iEstado, FSNUser.QARestNoConformUsu, FSNUser.QARestNoConformUO, FSNUser.QARestNoConformDep, FSNUser.QARestProvMaterial, FSNUser.QARestProvEquipo, FSNUser.QARestProvContacto, lIdentificador, Busqueda.Tipo, Busqueda.SubTipo, Busqueda.Titulo, Busqueda.Proveedor, Busqueda.Peticionario, Busqueda.Revisor _
                                              , Busqueda.FechaDesde, Busqueda.FechaHasta, Busqueda.FechaAlta, Busqueda.FechaActualizacion, Busqueda.FechaLimiteResolucion, Busqueda.FechaCierre, sGMN1, sGMN2, sGMN3, sGMN4, Busqueda.ArticuloHidden, Busqueda.EstadoNCAbiertas, Busqueda.UnidadNegocio, sNombreTabla:=Session("QANombreTabla"), sOrden:=sOrdenacion)

        For index As Integer = 1 To 9
            mi_boton = UpdatePanel1.FindControl("btnBoton" & index)
            Select Case mi_boton.Attributes("Estado")
                Case EstadosNoConformidad.Guardada
                    mi_boton.Text = arrTextosEstadosMini(mi_boton.Attributes("Estado")) & " (" + CStr(cFiltro.ncGuardadas) + ")"
                    mi_boton.ToolTip = arrTextosEstados(mi_boton.Attributes("Estado")) & " (" + CStr(cFiltro.ncGuardadas) + ")"
                Case EstadosNoConformidad.EnCursoDeAprobacion
                    mi_boton.Text = arrTextosEstadosMini(mi_boton.Attributes("Estado")) & " (" + CStr(cFiltro.ncEnCursoDeAprobacion) + ")"
                    mi_boton.ToolTip = arrTextosEstados(mi_boton.Attributes("Estado")) & " (" + CStr(cFiltro.ncEnCursoDeAprobacion) + ")"
                Case EstadosNoConformidad.Abierta
                    mi_boton.Text = arrTextosEstadosMini(mi_boton.Attributes("Estado")) & " (" + CStr(cFiltro.ncAbiertas) + ")"
                    mi_boton.ToolTip = arrTextosEstados(mi_boton.Attributes("Estado")) & " (" + CStr(cFiltro.ncAbiertas) + ")"
                Case EstadosNoConformidad.CierrePosIN
                    mi_boton.Text = arrTextosEstadosMini(mi_boton.Attributes("Estado")) & " (" + CStr(cFiltro.ncCierreEficazDentroPlazo) + ")"
                    mi_boton.ToolTip = arrTextosEstados(mi_boton.Attributes("Estado")) & " (" + CStr(cFiltro.ncCierreEficazDentroPlazo) + ")"
                Case EstadosNoConformidad.CierrePosOUT
                    mi_boton.Text = arrTextosEstadosMini(mi_boton.Attributes("Estado")) & " (" + CStr(cFiltro.ncCierreEficazFueraPlazo) + ")"
                    mi_boton.ToolTip = arrTextosEstados(mi_boton.Attributes("Estado")) & " (" + CStr(cFiltro.ncCierreEficazFueraPlazo) + ")"
                Case EstadosNoConformidad.CierreNoEficaz
                    mi_boton.Text = arrTextosEstadosMini(mi_boton.Attributes("Estado")) & " (" + CStr(cFiltro.ncCierreNoEficaz) + ")"
                    mi_boton.ToolTip = arrTextosEstados(mi_boton.Attributes("Estado")) & " (" + CStr(cFiltro.ncCierreNoEficaz) + ")"
                Case EstadosNoConformidad.CierrePdteRevisar
                    mi_boton.Text = arrTextosEstadosMini(mi_boton.Attributes("Estado")) & " (" + CStr(cFiltro.ncPendientesRevisarCierre) + ")"
                    mi_boton.ToolTip = arrTextosEstados(mi_boton.Attributes("Estado")) & " (" + CStr(cFiltro.ncPendientesRevisarCierre) + ")"
                Case EstadosNoConformidad.Anulada
                    mi_boton.Text = arrTextosEstadosMini(mi_boton.Attributes("Estado")) & " (" + CStr(cFiltro.ncAnuladas) + ")"
                    mi_boton.ToolTip = arrTextosEstados(mi_boton.Attributes("Estado")) & " (" + CStr(cFiltro.ncAnuladas) + ")"
                Case EstadosNoConformidad.Todas
                    mi_boton.Text = arrTextosEstadosMini(mi_boton.Attributes("Estado")) & " (" + CStr(cFiltro.ncTodas) + ")"
                    mi_boton.ToolTip = arrTextosEstados(mi_boton.Attributes("Estado")) & " (" + CStr(cFiltro.ncTodas) + ")"
            End Select
        Next

        whdgNoConformidades.Rows.Clear()
        whdgNoConformidades.Columns.Clear()
        whdgNoConformidades.GridView.Columns.Clear()

        Dim EstadoAcciones As Integer
        cFiltro.NoConformidadesVisor.Columns.Add("ESTADO_ACCIONES")
        For Each dr As DataRow In cFiltro.NoConformidadesVisor.Rows
            EstadoAcciones = 0

            If dr("ESTADO") = TipoEstadoNoConformidad.Abierta Then
                If IsDBNull(dr("FEC_RESPUESTA")) Then
                    EstadoAcciones = 8
                ElseIf dr("ACC_PDTECERRAR") = 1 Then
                    EstadoAcciones = 9
                Else
                    If dr("ACC_PDTEREVISARPET") Then EstadoAcciones += 1
                    If dr("ACC_PDTEFINALIZARPROVE") Then EstadoAcciones += 2
                    If dr("ACC_PDTEFINALIZARPET") Then EstadoAcciones += 4
                End If

                Dim modIdiomaActual As Integer = ModuloIdioma
                ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorNoConformidades
                dr("ESTADO_ACCIONES") = IIf(EstadoAcciones = 0, "", Textos(75 + EstadoAcciones))
                ModuloIdioma = modIdiomaActual
            End If
        Next

        InsertarEnCache("dtNoConformidades_" & FSNUser.Cod, cFiltro.NoConformidadesVisor, CacheItemPriority.BelowNormal)

        'Mirar si hay alguna instancia en proceso en las noConformidades
        m_bMostrarIndicadorEnProceso = (cFiltro.NoConformidadesVisor.Select("EN_PROCESO=1").Count > 0)

        'Mirar si hay algun comentario
        If cFiltro.NoConformidadesVisor.Columns.Item("COMENTARIOS") Is Nothing Then
            m_bMostrarComentario = False
        Else
            m_bMostrarComentario = (cFiltro.NoConformidadesVisor.Select("COMENTARIOS=1").Count > 0)
        End If

        ObtenerDataTableNoConformidades = cFiltro.NoConformidadesVisor
        cFiltro = Nothing
    End Function
    ''' <summary>
    ''' Configura la grid para su posterior exportación. Exporta la Grid a una hoja excel o a Pdf
    ''' </summary>
    ''' <param name="iTipoExportacion"> Tipo Exportacion (Excel = 1 // Pdf = 2)</param>
    ''' <remarks>Llamada desde:Page_load; Tiempo ejecucion:0,3seg.</remarks>
    Private Sub Exportar(ByVal iTipoExportacion As Byte)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorNoConformidades
        m_bExportando = True

        Dim bEstadoComent As Boolean
        Dim bEstadoIndicadorEnProceso As Boolean
        whdgNoConformidades.GridView.Behaviors.Paging.Enabled = False
        RecargarGrid()
        m_bMostrarComentario = Not (whdgNoConformidades.Columns("INDICADOR_EN_PROCESO").Hidden)
        If m_bMostrarComentario Then
            If Session("QAFiltroUsuarioId") = "" Then
                whdgNoConformidades.Columns("COMENT_ALTA").Hidden = False
                whdgNoConformidades.Columns("COMENT_ALTA").Header.Text = Textos(34) '"Comentario apertura" 
                whdgNoConformidades.Columns("COMENT_CIERRE").Hidden = False
                whdgNoConformidades.Columns("COMENT_CIERRE").Header.Text = Textos(35) '"Comentario cierre"
                whdgNoConformidades.Columns("COMENT_REVISOR").Hidden = False
                whdgNoConformidades.Columns("COMENT_REVISOR").Header.Text = Textos(36) '"Comentario revisor"
                whdgNoConformidades.Columns("COMENT_ANULACION").Hidden = False
                whdgNoConformidades.Columns("COMENT_ANULACION").Header.Text = Textos(68) '"Comentario anulación"
            End If
        End If
        'Guardar los estados de las columnas
        bEstadoComent = whdgNoConformidades.Columns("COMENT").Hidden
        bEstadoIndicadorEnProceso = whdgNoConformidades.Columns("INDICADOR_EN_PROCESO").Hidden

        whdgNoConformidades.Columns("COMENT").Hidden = True

        whdgNoConformidades.Columns("INDICADOR_EN_PROCESO").Hidden = True
        If m_bMostrarIndicadorEnProceso Then
            whdgNoConformidades.Columns("EN_PROCESO").Header.Text = Textos(38) ' "En Proceso"
            whdgNoConformidades.Columns("EN_PROCESO").VisibleIndex = 0
            whdgNoConformidades.Columns("EN_PROCESO").Hidden = False
        End If

        If iTipoExportacion = 1 Then
            wdgExcelExporter.DownloadName = FSNPageHeader.TituloCabecera & ".xls"
            wdgExcelExporter.Export(whdgNoConformidades)
        Else
            wdgPDFExporter.DownloadName = FSNPageHeader.TituloCabecera & ".pdf"
            wdgPDFExporter.Format = Infragistics.Documents.Reports.Report.FileFormat.PDF
            wdgPDFExporter.Export(whdgNoConformidades)
        End If

        'Establecer la situacion anterior a la exportacion
        whdgNoConformidades.Columns("COMENT").Hidden = bEstadoComent
        If m_bMostrarComentario Then
            whdgNoConformidades.Columns.Remove(whdgNoConformidades.Columns("COMENT_ALTA"))
            whdgNoConformidades.Columns.Remove(whdgNoConformidades.Columns("COMENT_CIERRE"))
            whdgNoConformidades.Columns.Remove(whdgNoConformidades.Columns("COMENT_REVISOR"))
            whdgNoConformidades.Columns.Remove(whdgNoConformidades.Columns("COMENT_ANULACION"))
        End If
        whdgNoConformidades.Columns("INDICADOR_EN_PROCESO").Hidden = bEstadoIndicadorEnProceso
        whdgNoConformidades.Columns("EN_PROCESO").Hidden = True
        m_bExportando = False
        whdgNoConformidades.GridView.Behaviors.Paging.Enabled = True
    End Sub
#Region "Filtros NoConformidad. Cargar no conformidades desde botones de filtro"
    ''' <summary>
    ''' Carga en la grid las columnas y las instancias del filtro seleccionado. 
    ''' Carga el desplegable con las columnas, el nº de instancias por estado y activa la pestaña del filtro.
    ''' Muestra el botón de Configurar para hacer modificaciones en la configuración del filtro.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: Al hacer click sobre una de las pestañas de los filtros. Tiempo máximo: 0 sg.</remarks>
    Protected Sub fsnTabFiltro_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim arrParametros() As String
        arrParametros = Split(CType(sender, FSNWebControls.FSNTab).CommandArgument, "#")

        Session("Ordenacion") = ""
        If arrParametros(0) = 0 Then
            Session("QAFiltroUsuarioId") = ""
            Session("QANombreTabla") = ""
            Session("QAFiltroId") = ""
        Else
            Session("QAFiltroUsuarioId") = arrParametros(0)
            Session("QANombreTabla") = arrParametros(1)
            Session("QAFiltroId") = arrParametros(2)
        End If

        QAFiltroUsuarioIdVNoConf.Value = IIf(Session("QAFiltroUsuarioId") = "", 0, Session("QAFiltroUsuarioId"))
        QAFiltroIdVNoConf.Value = IIf(Session("QAFiltroId") = "", 0, Session("QAFiltroId"))

        btnConfigurar.Visible = (Session("QAFiltroUsuarioId") <> "")
        btnConfigurar.Attributes.Add("onClick", "window.location='../FiltrosQA/ConfigurarFiltrosQA.aspx?IDFiltroUsu=" & Session("QAFiltroUsuarioId") & "&NombreTabla=" & Session("QANombreTabla") & "&Donde=VisorNC';return false;")

        LimpiarBusquedaAvanzada()

        CargarBusquedaAvanzada()

        DesseleccionarEstados()

        SeleccionarEstado()

        'limpiar el filtering tras cada cambio de filtro
        whdgNoConformidades.Behaviors.Filtering.ColumnSettings.Clear()
        whdgNoConformidades.GridView.Behaviors.Filtering.ColumnSettings.Clear()

        whdgNoConformidades.Behaviors.Filtering.ColumnFilters.Clear()
        whdgNoConformidades.GridView.Behaviors.Filtering.ColumnFilters.Clear()

        whdgNoConformidades.GridView.Behaviors.Filtering.ApplyFilter()

        RecargarNoConformidades()

        'solo se queda chequeada la pestaña del filtro chequeada
        For index As Integer = 0 To dlFiltrosConfigurados.Items.Count - 1
            CType(dlFiltrosConfigurados.Items(index).FindControl("fsnTabFiltro"), FSNWebControls.FSNTab).Selected = False
        Next
        CType(sender, FSNWebControls.FSNTab).Selected = True
    End Sub
#End Region
#Region "Estados NoConformidad. Cargar no conformidades desde botones de de estado"
    ''' <summary>
    ''' Desselecciona todos los botones de estados y pone la columna de los iconos a no visible.
    ''' </summary>
    ''' <remarks>Llamada desde: btnGuardadas_Click, btnAbiertas_Click, btnPendientesRevisarCierre_Click, btnCierreDentroPlazo_Click, btnCierreFueraPlazo_Click, btnCierreNoEficaz_Click, btnTodas_Click; Tiempo máximo: 0 sg.</remarks>
    Private Sub DesseleccionarEstados()
        Dim mi_boton As New Fullstep.FSNWebControls.FSNButton
        Session("FilaEstado") = Nothing
        For i = 1 To 9
            mi_boton = UpdatePanel1.FindControl("btnBoton" & i)
            mi_boton.Selected = False
        Next
        whdgNoConformidades.GridView.ScrollTop = 0
    End Sub
    '''' <summary>
    '''' Selecciona el estado activo cuando se carga la página sin postback.
    '''' Si no hay estado activo (primera vez) se activa el estado "Guardadas".
    '''' Actualizo la ordenacion de la grid
    '''' </summary>
    '''' <remarks>Llamada desde: Page_Load(). Tiempo Máximo: 0 sg.</remarks>
    Private Sub SeleccionarEstado()
        Dim mi_boton As New Fullstep.FSNWebControls.FSNButton
        If Session("FilaEstado") <> Nothing Then
            For index As Integer = 1 To 9
                mi_boton = UpdatePanel1.FindControl("btnBoton" & index)
                If mi_boton.Attributes("Estado") = Session("FilaEstado") Then
                    mi_boton.Selected = True
                End If
            Next
        Else
            Session("FilaEstado") = btnBoton2.Attributes.Item("Estado") 'Selecciona el de abiertas
            btnBoton2.Selected = True
        End If
    End Sub
    ''' <summary>
    ''' Carga los estados en las que se puede encontrar las noConformidades en las pestañas
    ''' </summary>
    ''' <remarks>Llamada desde=Page_Load; Tiempo máximo=0seg.</remarks>
    Private Sub CargarEstados()
        Dim mi_boton As New Fullstep.FSNWebControls.FSNButton
        'No Conformidades
        For i = 1 To 9
            mi_boton = UpdatePanel1.FindControl("btnBoton" & i)
            mi_boton.Attributes.Add("Estado", arrOrdenEstados(i - 1))
            mi_boton.Text = arrTextosEstadosMini(i - 1)
            mi_boton.ToolTip = arrTextosEstados(i - 1)
            mi_boton.Visible = True
        Next
    End Sub
    ''' <summary>
    ''' Carga en la grid las solicitudes guardadas y marca como seleccionado este botón.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>LLamada desde: Al hacer click en este botón. Tiempo máximo: 0 sg.</remarks>
    Private Sub btnBoton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBoton1.Click, btnBoton2.Click, btnBoton3.Click, btnBoton4.Click, btnBoton5.Click, btnBoton6.Click, btnBoton7.Click, btnBoton8.Click, btnBoton9.Click
        DesseleccionarEstados()
        CType(sender, FSNWebControls.FSNButton).Selected = True
        Session("FilaEstado") = CType(sender, FSNWebControls.FSNButton).Attributes("Estado")
        RecargarNoConformidades()
    End Sub
#End Region
#Region "Busqueda Avanzada. Cargar no conformidades desde botones de buscar"
    ''' <summary>
    ''' Limpia los criterios de busqueda avanzada.
    ''' </summary>
    ''' <remarks>Llamada desde: fsnTabFiltro_Click // btnLimpiarBusquedaAvanzada_Click //  Page_LoadComplete. Tiempo máximo: 0 sg.</remarks>
    Private Sub LimpiarBusquedaAvanzada()
        Busqueda.Tipo = 0
        Busqueda.SubTipo = ""
        Busqueda.Titulo = ""
        Busqueda.Proveedor = ""
        Busqueda.ProveedorDeBaja = False
        Busqueda.UnidadNegocio = ""
        Busqueda.MaterialHidden = ""
        Busqueda.MaterialTexto = ""
        Busqueda.ArticuloHidden = ""
        Busqueda.ArticuloTexto = ""
        Busqueda.FechaAlta = True
        Busqueda.FechaActualizacion = False
        Busqueda.FechaLimiteResolucion = False
        Busqueda.FechaCierre = False
        Busqueda.FechaDesde = Nothing
        Busqueda.FechaHasta = Nothing
        Busqueda.EstadoNCAbiertas = ""
        Busqueda.Peticionario = ""
        Busqueda.Revisor = ""
        upBusquedaAvanzada.Update()
    End Sub
    ''' <summary>
    ''' Cargar en el módulo de busqueda avanzada los datos configurados en el filtro.
    ''' Se hace con una variable de session porque cuando se carga la pagina se necesita acceder a esta funcion 2 veces. una vez que se carga completamente la pagina y otra antes
    ''' </summary>
    ''' <remarks>Llamada desde: Page_PreRender. Tiempo máximo: 1 sg.</remarks>
    Private Sub CargarBusquedaAvanzada()
        If Not m_bConConfiguracionAnterior Or (Not Request.QueryString("CargarBusquedaAvanzada") Is Nothing) Then
            If Session("QAFiltroUsuarioId") <> "" Then
                Dim sMat As String
                Dim sFechaDesde As String = ""
                Dim sFechaHasta As String = ""
                Dim cFiltro As FiltroQA
                cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroQA))

                cFiltro.IDFiltroUsuario = Session("QAFiltroUsuarioId")
                cFiltro.TipoSol = TiposDeDatos.TipoDeSolicitud.NoConformidad
                cFiltro.LoadConfiguracionCamposBusqueda(Idioma)

                Busqueda.Filtro = cFiltro.IDFiltro
                If Not cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA") Is Nothing Then
                    If cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows.Count > 0 Then
                        Busqueda.Tipo = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("TIPO")
                        Busqueda.SubTipo = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("SUBTIPO")
                        Busqueda.Titulo = DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("TITULO"))
                        If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDORBAJA")) Then Busqueda.ProveedorDeBaja = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDORBAJA")
                        Busqueda.CambioFiltro_ProveedorBaja()
                        Busqueda.Proveedor = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PROVEEDOR")
                        Busqueda.UnidadNegocio = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("UNIDADNEGOCIO")
                        sMat = DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("GMN1")) & "-" & DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("GMN2")) & "-" & DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("GMN3")) & "-" & DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("GMN4"))
                        Busqueda.MaterialHidden = sMat
                        Busqueda.MaterialTexto = DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("MATDEN"))

                        Busqueda.ArticuloHidden = DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("ARTICULO"))
                        Busqueda.ArticuloTexto = DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("ARTDEN"))
                        'Fecha Alta
                        If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_ALTA")) Then
                            Busqueda.FechaAlta = True
                            sFechaDesde = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_ALTA")
                        End If
                        If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_ALTA")) Then
                            Busqueda.FechaAlta = True
                            sFechaHasta = DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_ALTA"))
                        End If
                        'Fecha Actualizacion
                        If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_ACTUALIZACION")) Then
                            Busqueda.FechaActualizacion = True
                            sFechaDesde = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_ACTUALIZACION")
                        End If
                        If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_ACTUALIZACION")) Then
                            Busqueda.FechaActualizacion = True
                            sFechaHasta = DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_ACTUALIZACION"))
                        End If

                        If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_LIMITERESOLUCION")) Then
                            Busqueda.FechaLimiteResolucion = True
                            sFechaDesde = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_LIMITERESOLUCION")
                        End If
                        If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_LIMITERESOLUCION")) Then
                            Busqueda.FechaLimiteResolucion = True
                            sFechaHasta = DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_LIMITERESOLUCION"))
                        End If

                        If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_CIERRE")) Then
                            Busqueda.FechaCierre = True
                            sFechaDesde = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("DESDE_CIERRE")
                        End If
                        If Not IsDBNull(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_CIERRE")) Then
                            Busqueda.FechaCierre = True
                            sFechaHasta = DBNullToStr(cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("HASTA_CIERRE"))
                        End If

                        If sFechaDesde <> "" Then
                            Busqueda.FechaDesde = sFechaDesde
                        End If
                        If sFechaHasta <> "" Then
                            Busqueda.FechaHasta = sFechaHasta
                        End If

                        If (Not Busqueda.FechaActualizacion And Not Busqueda.FechaLimiteResolucion And Not Busqueda.FechaCierre) Then
                            Busqueda.FechaAlta = True
                        End If

                        Busqueda.EstadoNCAbiertas = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("ESTADONCABIERTAS")
                        Busqueda.Peticionario = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("PETICIONARIO")
                        Busqueda.Revisor = cFiltro.CamposConfiguracion.Tables("CAMPOSBUSQUEDA").Rows(0).Item("REVISOR")

                        upBusquedaAvanzada.Update()
                    End If
                End If
                cFiltro = Nothing
                AlmacenarBusquedaAvanzada()
            Else
                If Not Session("QA_BusquedaAvanzada0") Is Nothing Then
                    Dim arrbusquedaTemporal As New ArrayList
                    arrbusquedaTemporal = CType(Session("QA_BusquedaAvanzada0"), ArrayList)
                    Busqueda.Tipo = arrbusquedaTemporal(0)
                    Busqueda.SubTipo = arrbusquedaTemporal(1)
                    Busqueda.Titulo = arrbusquedaTemporal(2)
                    Busqueda.Proveedor = arrbusquedaTemporal(3)
                    Busqueda.UnidadNegocio = arrbusquedaTemporal(4)
                    Busqueda.MaterialHidden = arrbusquedaTemporal(5)
                    Busqueda.MaterialTexto = arrbusquedaTemporal(6)
                    Busqueda.ArticuloHidden = arrbusquedaTemporal(7)
                    Busqueda.ArticuloTexto = arrbusquedaTemporal(8)
                    Busqueda.FechaAlta = arrbusquedaTemporal(9)
                    Busqueda.FechaActualizacion = arrbusquedaTemporal(10)
                    Busqueda.FechaLimiteResolucion = arrbusquedaTemporal(11)
                    Busqueda.FechaCierre = arrbusquedaTemporal(12)
                    Busqueda.FechaDesde = arrbusquedaTemporal(13)
                    Busqueda.FechaHasta = arrbusquedaTemporal(14)
                    Busqueda.EstadoNCAbiertas = arrbusquedaTemporal(15)
                    Busqueda.Peticionario = arrbusquedaTemporal(16)
                    Busqueda.Revisor = arrbusquedaTemporal(17)
                    Busqueda.ProveedorDeBaja = arrbusquedaTemporal(18)
                    upBusquedaAvanzada.Update()
                End If
            End If
        Else
            If Session("QAFiltroUsuarioId") <> "" Then
                If Not Session("QA_BusquedaAvanzada") Is Nothing Then
                    Dim arrbusquedaTemporal As New ArrayList
                    arrbusquedaTemporal = CType(Session("QA_BusquedaAvanzada"), ArrayList)
                    Busqueda.Tipo = arrbusquedaTemporal(0)
                    Busqueda.SubTipo = arrbusquedaTemporal(1)
                    Busqueda.Titulo = arrbusquedaTemporal(2)
                    Busqueda.Proveedor = arrbusquedaTemporal(3)
                    Busqueda.UnidadNegocio = arrbusquedaTemporal(4)
                    Busqueda.MaterialHidden = arrbusquedaTemporal(5)
                    Busqueda.MaterialTexto = arrbusquedaTemporal(6)
                    Busqueda.ArticuloHidden = arrbusquedaTemporal(7)
                    Busqueda.ArticuloTexto = arrbusquedaTemporal(8)
                    Busqueda.FechaAlta = arrbusquedaTemporal(9)
                    Busqueda.FechaActualizacion = arrbusquedaTemporal(10)
                    Busqueda.FechaLimiteResolucion = arrbusquedaTemporal(11)
                    Busqueda.FechaCierre = arrbusquedaTemporal(12)
                    Busqueda.FechaDesde = arrbusquedaTemporal(13)
                    Busqueda.FechaHasta = arrbusquedaTemporal(14)
                    Busqueda.EstadoNCAbiertas = arrbusquedaTemporal(15)
                    Busqueda.Peticionario = arrbusquedaTemporal(16)
                    Busqueda.Revisor = arrbusquedaTemporal(17)
                    Busqueda.ProveedorDeBaja = arrbusquedaTemporal(18)
                    upBusquedaAvanzada.Update()
                End If
            Else
                If Not Session("QA_BusquedaAvanzada0") Is Nothing Then
                    Dim arrbusquedaTemporal As New ArrayList
                    arrbusquedaTemporal = CType(Session("QA_BusquedaAvanzada0"), ArrayList)
                    Busqueda.Tipo = arrbusquedaTemporal(0)
                    Busqueda.SubTipo = arrbusquedaTemporal(1)
                    Busqueda.Titulo = arrbusquedaTemporal(2)
                    Busqueda.Proveedor = arrbusquedaTemporal(3)
                    Busqueda.UnidadNegocio = arrbusquedaTemporal(4)
                    Busqueda.MaterialHidden = arrbusquedaTemporal(5)
                    Busqueda.MaterialTexto = arrbusquedaTemporal(6)
                    Busqueda.ArticuloHidden = arrbusquedaTemporal(7)
                    Busqueda.ArticuloTexto = arrbusquedaTemporal(8)
                    Busqueda.FechaAlta = arrbusquedaTemporal(9)
                    Busqueda.FechaActualizacion = arrbusquedaTemporal(10)
                    Busqueda.FechaLimiteResolucion = arrbusquedaTemporal(11)
                    Busqueda.FechaCierre = arrbusquedaTemporal(12)
                    Busqueda.FechaDesde = arrbusquedaTemporal(13)
                    Busqueda.FechaHasta = arrbusquedaTemporal(14)
                    Busqueda.EstadoNCAbiertas = arrbusquedaTemporal(15)
                    Busqueda.Peticionario = arrbusquedaTemporal(16)
                    Busqueda.Revisor = arrbusquedaTemporal(17)
                    Busqueda.ProveedorDeBaja = arrbusquedaTemporal(18)
                    upBusquedaAvanzada.Update()
                End If
            End If
        End If
    End Sub
    ''' <summary>
    ''' Almacena en una variable de session la configuracion que tiene el usuario en la busqueda avanzada, para 
    ''' que al ir al detalle de la noConformidad y al volver pueda cargar la configuracion que tenia
    ''' </summary>
    ''' <remarks>Llamada desde=uwgNoConformidades_ActiveCellChange; Tiempo máximo=0,1seg.</remarks>
    Private Sub AlmacenarBusquedaAvanzada()
        Dim arrbusquedaTemporal As New ArrayList
        arrbusquedaTemporal.Add(Busqueda.Tipo)
        arrbusquedaTemporal.Add(Busqueda.SubTipo)
        arrbusquedaTemporal.Add(Busqueda.Titulo)
        arrbusquedaTemporal.Add(Busqueda.Proveedor)
        arrbusquedaTemporal.Add(Busqueda.UnidadNegocio)
        arrbusquedaTemporal.Add(Busqueda.MaterialHidden)
        arrbusquedaTemporal.Add(Busqueda.MaterialTexto)
        arrbusquedaTemporal.Add(Busqueda.ArticuloHidden)
        arrbusquedaTemporal.Add(Busqueda.ArticuloTexto)
        arrbusquedaTemporal.Add(Busqueda.FechaAlta)
        arrbusquedaTemporal.Add(Busqueda.FechaActualizacion)
        arrbusquedaTemporal.Add(Busqueda.FechaLimiteResolucion)
        arrbusquedaTemporal.Add(Busqueda.FechaCierre)
        arrbusquedaTemporal.Add(Busqueda.FechaDesde)
        arrbusquedaTemporal.Add(Busqueda.FechaHasta)
        arrbusquedaTemporal.Add(Busqueda.EstadoNCAbiertas)
        arrbusquedaTemporal.Add(Busqueda.Peticionario)
        arrbusquedaTemporal.Add(Busqueda.Revisor)
        arrbusquedaTemporal.Add(Busqueda.ProveedorDeBaja)
        If Session("QAFiltroUsuarioId") <> "" Then
            Session("QA_BusquedaAvanzada") = arrbusquedaTemporal
        Else
            Session("QA_BusquedaAvanzada0") = arrbusquedaTemporal 'Filtro NoConformidades
        End If
    End Sub
    ''' <summary>
    ''' Limpiar los criterios de busqueda
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo máximo=0</remarks>
    Private Sub btnLimpiarBusquedaAvanzada_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiarBusquedaAvanzada.Click
        LimpiarBusquedaAvanzada()
        AlmacenarBusquedaAvanzada()
    End Sub
    ''' <summary>
    ''' Evento que salta al darle al boton de "Buscar" del buscador
    ''' Realiza la busqueda de las noconformidades que coincidan con los criterios de busqueda
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo máximo=0,6 (depende de las noconformidades)</remarks>
    Private Sub btnBuscarBusquedaAvanzada_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarBusquedaAvanzada.Click
        If Page.IsValid Then
            Dim sColsGroupBy As String = "ColsGroupBy" & Session("QAFiltroUsuarioId")
            Session(sColsGroupBy) = ""

            PaginaVolver.Value = 1
            whdgNoConformidades.GridView.ScrollTop = 0
            RecargarNoConformidades()

            If m_bMostrarIndicadorEnProceso Then whdgNoConformidades.Columns("INDICADOR_EN_PROCESO").Hidden = False

            AlmacenarBusquedaAvanzada()
            UpdatePanel1.Update()
        End If
    End Sub
#End Region
#Region "Busqueda rápida. Cargar no conformidades desde botones de buscar po ID"
    ''' <summary>
    ''' Evento que salta al darle al boton de "Buscar" del la "cabecera de la pagina"
    ''' Realiza la busqueda de las noconformidades que coincidan con los criterios de busqueda
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo máximo=0,6 (depende de las noconformidades)</remarks>
    Private Sub btnBuscarIdentificador_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarIdentificador.Click
        If Page.IsValid Then
            whdgNoConformidades.GridView.ScrollTop = 0
            RecargarNoConformidades()
            UpdatePanel1.Update()
        End If
    End Sub
#End Region
#Region "Propiedades de la grid"
    Private Sub CrearColumnasDT(ByVal dt As DataTable)
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim nombre As String

        'Columnas de aprobar y rechazar para las NCs en curso de aprobación
        If Session("FilaEstado") = EstadosNoConformidad.EnCursoDeAprobacion Then
            Dim modIdiomaActual As Integer = ModuloIdioma
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorNoConformidades

            If Not m_bExportando Then
                AddColumnTemplate("chkAprobarCheckAll", "btnAprobar", "TMPL_ACCION_APROBAR", Textos(88), "cbAprobar", 1)
                AddColumnTemplate("chkRechazarCheckAll", "btnRechazar", "TMPL_ACCION_RECHAZAR", Textos(89), "cbRechazar", 2)

                'TMPL_PDTE
                Dim field As New TemplateDataField
                field.ItemTemplate = New ImgTemplate("imgPdte", System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Img_Ico_Alerta.gif")
                field.Key = "TMPL_PDTE"
                field.Header.CssClass = "celdaImagenWebHierarchical headerNoWrap"
                field.CssClass = "celdaImagenWebHierarchical itemSeleccionable SinSalto"
                field.VisibleIndex = 3
                field.Width = Unit.Pixel(20)
                field.Hidden = False
                If whdgNoConformidades.GridView.Columns(field.Key) Is Nothing Then whdgNoConformidades.GridView.Columns.Add(field)
                If whdgNoConformidades.Columns(field.Key) Is Nothing Then whdgNoConformidades.Columns.Add(field)

                'TMPL_ERROR
                field = New TemplateDataField
                field.ItemTemplate = New ImgTemplate("imgError", System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Icono_Error_Amarillo.gif")
                field.Key = "TMPL_ERROR"
                field.Header.CssClass = "celdaImagenWebHierarchical headerNoWrap"
                field.CssClass = "celdaImagenWebHierarchical itemSeleccionable SinSalto"
                field.VisibleIndex = 4
                field.Width = Unit.Pixel(20)
                field.Hidden = False
                If whdgNoConformidades.GridView.Columns(field.Key) Is Nothing Then whdgNoConformidades.GridView.Columns.Add(field)
                If whdgNoConformidades.Columns(field.Key) Is Nothing Then whdgNoConformidades.Columns.Add(field)

                ModuloIdioma = modIdiomaActual
            End If
        End If

        whdgNoConformidades.Behaviors.Paging.PageSize = _rowsPage
        whdgNoConformidades.GridView.Behaviors.Paging.PageSize = _rowsPage
        For i As Integer = 0 To dt.Columns.Count - 1
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            nombre = dt.Columns(i).ToString
            If (nombre <> "EN_PROCESO") Then
                With campoGrid
                    .Key = nombre
                    .DataFieldName = nombre
                    .Header.Text = nombre
                    .Header.Tooltip = nombre
                    .Header.CssClass = "headerNoWrap igg_FullstepHeaderCaption"
                    .CssClass = "SinSalto"
                    .Hidden = True
                End With
                whdgNoConformidades.Columns.Add(campoGrid)
                whdgNoConformidades.GridView.Columns.Add(campoGrid)
            Else
                AnyadirColumnaU("EN_PROCESO")
                whdgNoConformidades.Columns("EN_PROCESO").Hidden = True
            End If
        Next
        If whdgNoConformidades.Columns("INDICADOR_EN_PROCESO") Is Nothing Then
            AnyadirColumnaU("INDICADOR_EN_PROCESO")
            whdgNoConformidades.Columns("INDICADOR_EN_PROCESO").Hidden = True
        End If
        If whdgNoConformidades.Columns("COMENT") Is Nothing Then
            AnyadirColumnaU("COMENT")
        End If
        If whdgNoConformidades.Columns("COMENT_ALTA") Is Nothing Then
            AnyadirColumnaU("COMENT_ALTA")
            whdgNoConformidades.Columns("COMENT_ALTA").Hidden = True
        End If
        If whdgNoConformidades.Columns("COMENT_CIERRE") Is Nothing Then
            AnyadirColumnaU("COMENT_CIERRE")
            whdgNoConformidades.Columns("COMENT_CIERRE").Hidden = True
        End If
        If whdgNoConformidades.Columns("COMENT_REVISOR") Is Nothing Then
            AnyadirColumnaU("COMENT_REVISOR")
            whdgNoConformidades.Columns("COMENT_REVISOR").Hidden = True
        End If
        If whdgNoConformidades.Columns("COMENT_ANULACION") Is Nothing Then
            AnyadirColumnaU("COMENT_ANULACION")
            whdgNoConformidades.Columns("COMENT_ANULACION").Hidden = True
        End If
        If whdgNoConformidades.Columns("ESTADO_ACCIONES") Is Nothing Then
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            nombre = "ESTADO_ACCIONES"

            With campoGrid
                .Key = nombre
                .DataFieldName = nombre
                .Header.Text = nombre
                .Header.Tooltip = nombre
                .Header.CssClass = "headerNoWrap igg_FullstepHeaderCaption"
                .CssClass = "SinSalto"
                .Hidden = True
            End With
            whdgNoConformidades.Columns.Add(campoGrid)
            whdgNoConformidades.GridView.Columns.Add(campoGrid)
        End If
    End Sub
    Private Sub AddColumnTemplate(ByVal idHeaderCheck As String, ByVal idHeaderButton As String, ByVal key As String, ByVal buttonText As String, ByVal idChck As String, Optional ByVal index As Integer = -1)
        Dim field As New TemplateDataField
        field.HeaderTemplate = New CheckAndButtonTemplate(idHeaderCheck, idHeaderButton, buttonText)
        field.ItemTemplate = New CheckboxTemplate(idChck)
        field.Header.Tooltip = buttonText

        field.Key = key
        field.Header.CssClass = "itemCenterAligment"
        field.CssClass = "itemCenterAligment"
        field.VisibleIndex = index
        field.Width = Unit.Pixel(110)
        field.Hidden = False

        If whdgNoConformidades.GridView.Columns(field.Key) Is Nothing Then whdgNoConformidades.GridView.Columns.Add(field)
        If whdgNoConformidades.Columns(field.Key) Is Nothing Then whdgNoConformidades.Columns.Add(field)
    End Sub
    Private Sub AnyadirColumnaU(ByVal fieldname As String)
        Dim unboundField As New Infragistics.Web.UI.GridControls.UnboundField(True)
        unboundField.Key = fieldname
        whdgNoConformidades.Columns.Add(unboundField)
        whdgNoConformidades.GridView.Columns.Add(unboundField)
    End Sub
    ''' <summary>
    ''' Inicializa la grid.
    ''' Visualiza las columnas del filtro si es el caso, sino con las columnas del antiguo grid de noconformidades
    ''' </summary>
    Private Sub ConfigurarGrid()
        Dim arrDatosGeneralesDen() As String = InicializarArraysCamposGeneralesNC()

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorNoConformidades
        m_bMostrarComentario = False
        InicializarArraysCamposGenerales()
        whdgNoConformidades.GridView.ShowHeader = True

        For i = 0 To whdgNoConformidades.Columns.Count - 1
            If whdgNoConformidades.Columns(i).Type.FullName = "System.DateTime" Then
                whdgNoConformidades.Columns(i).FormatValue(FSNUser.DateFormat.ShortDatePattern)
            End If
        Next

        If Session("QAFiltroUsuarioId") = "" Then
            whdgNoConformidades.Columns("ID").Hidden = False
            whdgNoConformidades.Columns("TITULO").Hidden = False
            whdgNoConformidades.Columns("FECALTA").Hidden = False
            whdgNoConformidades.Columns("FEC_ANULACION").Hidden = False
            whdgNoConformidades.Columns("FEC_CIERRE").Hidden = False
            whdgNoConformidades.Columns("CODPROVE").Hidden = False
            whdgNoConformidades.Columns("DENPROVE").Hidden = False
            whdgNoConformidades.Columns("PROVE_ERP").Hidden = Not Me.Acceso.gbProveERPVisibleNC
            whdgNoConformidades.Columns("PET").Hidden = False
            whdgNoConformidades.Columns("ARTICULO").Hidden = False
            whdgNoConformidades.Columns("COMENT").Hidden = False

            Select Case Session("FilaEstado")
                Case EstadosNoConformidad.CierrePosIN, EstadosNoConformidad.CierrePosOUT, EstadosNoConformidad.CierreNoEficaz
                    whdgNoConformidades.Columns("FEC_CIERRE").Hidden = False
                    whdgNoConformidades.Columns("FEC_ANULACION").Hidden = True
                Case EstadosNoConformidad.Anulada
                    whdgNoConformidades.Columns("FEC_ANULACION").Hidden = False
                    whdgNoConformidades.Columns("FEC_CIERRE").Hidden = True
                Case EstadosNoConformidad.Todas
                    whdgNoConformidades.Columns("FEC_CIERRE").Hidden = False
                    whdgNoConformidades.Columns("FEC_ANULACION").Hidden = False
                Case EstadosNoConformidad.EnCursoDeAprobacion
                    whdgNoConformidades.Columns("COMENT").Hidden = True
                    whdgNoConformidades.Columns("COMENT_ALTA").Hidden = True
                    whdgNoConformidades.Columns("COMENT_CIERRE").Hidden = True
                    whdgNoConformidades.Columns("COMENT_REVISOR").Hidden = True
                    whdgNoConformidades.Columns("COMENT_ANULACION").Hidden = True
                Case Else
                    whdgNoConformidades.Columns("FEC_CIERRE").Hidden = True
                    whdgNoConformidades.Columns("FEC_ANULACION").Hidden = True
            End Select
            whdgNoConformidades.Columns("ESTADO_ACCIONES").Hidden = False

            whdgNoConformidades.Columns("ID").Header.Text = arrDatosGeneralesDen(1) 'Identificador
            whdgNoConformidades.Columns("TITULO").Header.Text = arrDatosGeneralesDen(2) & "/" & arrDatosGeneralesDen(3) '"Tipo/Titulo"
            whdgNoConformidades.Columns("FECALTA").Header.Text = arrDatosGeneralesDen(9) ' "Fecha de alta"
            whdgNoConformidades.Columns("FEC_ANULACION").Header.Text = Textos(67) '"Fecha anulación"
            whdgNoConformidades.Columns("FEC_CIERRE").Header.Text = arrDatosGeneralesDen(15) ' "Fecha cierre"
            whdgNoConformidades.Columns("CODPROVE").Header.Text = arrDatosGeneralesDen(4) '"Proveedor"
            whdgNoConformidades.Columns("DENPROVE").Header.Text = arrDatosGeneralesDen(5) '"Denominación Proveedor"
            whdgNoConformidades.Columns("PET").Header.Text = arrDatosGeneralesDen(7) '"Peticionario"
            whdgNoConformidades.Columns("ARTICULO").Header.Text = Textos(37)
            whdgNoConformidades.Columns("COMENT").Header.Text = arrDatosGeneralesDen(16) '"Comentarios"
            whdgNoConformidades.Columns("ESTADO_ACCIONES").Header.Text = arrDatosGeneralesDen(21) '"Estado de las acciones"
            whdgNoConformidades.Columns("PROVE_ERP").Header.Text = arrDatosGeneralesDen(22) 'Proveedor en ERP

            whdgNoConformidades.Columns("ID").Width = Unit.Pixel(100)
            whdgNoConformidades.Columns("TITULO").Width = Unit.Pixel(130)
            whdgNoConformidades.Columns("FECALTA").Width = Unit.Pixel(115)
            whdgNoConformidades.Columns("FEC_ANULACION").Width = Unit.Pixel(115)
            whdgNoConformidades.Columns("FEC_CIERRE").Width = Unit.Pixel(115)
            whdgNoConformidades.Columns("CODPROVE").Width = Unit.Pixel(130)
            whdgNoConformidades.Columns("DENPROVE").Width = Unit.Pixel(180)
            whdgNoConformidades.Columns("PROVE_ERP").Width = Unit.Pixel(220)
            whdgNoConformidades.Columns("PET").Width = Unit.Pixel(150)
            whdgNoConformidades.Columns("ARTICULO").Width = Unit.Pixel(150)
            whdgNoConformidades.Columns("COMENT").Width = Unit.Pixel(100)
            whdgNoConformidades.Columns("COMENT").VisibleIndex = whdgNoConformidades.Columns.Count - 1
            whdgNoConformidades.Columns("ESTADO_ACCIONES").Width = Unit.Pixel(120)
        Else
            Dim cFiltro As FiltroQA
            cFiltro = FSNServer.Get_Object(GetType(FSNServer.FiltroQA))
            cFiltro.IDFiltro = Session("QAFiltroId")
            cFiltro.IDFiltroUsuario = Session("QAFiltroUsuarioId")
            cFiltro.TipoSol = TiposDeDatos.TipoDeSolicitud.NoConformidad
            cFiltro.Persona = FSNUser.CodPersona

            Dim dsCamposYCamposGenerales As DataSet = Nothing
            dsCamposYCamposGenerales = cFiltro.LoadCamposyCamposGenerales(Idioma)

            For Each oRow As DataRow In dsCamposYCamposGenerales.Tables(0).Rows
                If oRow("CAMPO_GENERAL") = 1 Then 'Tratamiento campos Generales
                    whdgNoConformidades.Columns(arrDatosGenerales(oRow("ID"))).Header.Text = If(oRow.IsNull("NOMBRE_GRID"), arrDatosGeneralesDen(oRow("ID")), oRow("NOMBRE_GRID"))
                    whdgNoConformidades.Columns(arrDatosGenerales(oRow("ID"))).Header.Tooltip = If(oRow.IsNull("NOMBRE_GRID"), arrDatosGeneralesDen(oRow("ID")), oRow("NOMBRE_GRID"))
                    whdgNoConformidades.Columns(arrDatosGenerales(oRow("ID"))).VisibleIndex = oRow("POSICION_GRID")
                    whdgNoConformidades.Columns(arrDatosGenerales(oRow("ID"))).Width = CLng(oRow("TAMANYO_GRID"))
                    If arrDatosGenerales(oRow("ID")) = "PROVE_ERP" Then
                        whdgNoConformidades.Columns(arrDatosGenerales(oRow("ID"))).Hidden = Not (Me.Acceso.gbProveERPVisibleNC And CBool(oRow("VISIBLE_GRID")))
                    Else
                        whdgNoConformidades.Columns(arrDatosGenerales(oRow("ID"))).Hidden = Not CBool(oRow("VISIBLE_GRID"))
                    End If

                    If arrDatosGenerales(oRow("ID")) = "COMENT" Then
                        m_bMostrarComentario = CBool(oRow("VISIBLE_GRID"))
                    End If
                Else
                    'Campos formulario
                    If CBool(oRow("VISIBLE_GRID")) = True Then
                        If DBNullToStr(oRow("NOMBRE_GRID")) <> "" Then
                            whdgNoConformidades.Columns("C_" & oRow("ID").ToString).Header.Text = oRow("NOMBRE_GRID")
                            whdgNoConformidades.Columns("C_" & oRow("ID").ToString).Header.Tooltip = oRow("NOMBRE_GRID")
                        Else
                            whdgNoConformidades.Columns("C_" & oRow("ID").ToString).Header.Text = oRow("NOMBRE")
                            whdgNoConformidades.Columns("C_" & oRow("ID").ToString).Header.Tooltip = oRow("NOMBRE")
                        End If
                        whdgNoConformidades.Columns("C_" & oRow("ID").ToString).VisibleIndex = oRow("POSICION_GRID")
                        whdgNoConformidades.Columns("C_" & oRow("ID").ToString).Width = CLng(oRow("TAMANYO_GRID"))
                        whdgNoConformidades.Columns("C_" & oRow("ID").ToString).Hidden = Not CBool(oRow("VISIBLE_GRID"))
                    End If
                End If
            Next
            cFiltro = Nothing
        End If

        If whdgNoConformidades.Columns("INDICADOR_EN_PROCESO").Hidden = False Then
            whdgNoConformidades.Columns("INDICADOR_EN_PROCESO").VisibleIndex = 0
        End If
    End Sub
    ''' <summary>Inicializa los arrays con los campos generales de un certificado.</summary>
    ''' <remarks>Llamada desde:Page_Load. Tiempo máximo: 0sg.</remarks>
    Private Function InicializarArraysCamposGeneralesNC() As String()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ConfiguracionFiltroQA

        Dim arrDatosGeneralesDen(22) As String
        For i As Integer = 1 To 9
            arrDatosGeneralesDen(i) = Textos(i + 17)
        Next
        arrDatosGeneralesDen(10) = Textos(37)
        For i As Integer = 11 To 19
            arrDatosGeneralesDen(i) = Textos(i + 16)
        Next
        arrDatosGeneralesDen(20) = Textos(38)
        arrDatosGeneralesDen(21) = Textos(69)
        arrDatosGeneralesDen(22) = Textos(70)

        Return arrDatosGeneralesDen
    End Function
    ''' <summary>
    ''' Evento que salta al cargarse cada linea de la grid
    ''' Si no es en la exportacion, comprueba si tiene o no comentario para insertar la imagen, o si la instancia esta en proceso
    ''' En la exportacion campo lo de en proceso por las cadenas (SI/NO)
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo máximo=0seg.</remarks>
    Private Sub whdgNoConformidades_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgNoConformidades.InitializeRow
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorNoConformidades

        'Las columnas UnboundField se crean en CrearColumnasDT y van una detras de otra -> cuando se crea EN_PROCESO no tiene pq estar COMENTARIOS, etc
        'por eso preguntar por existencia de columna amtes de usar
        If Not m_bExportando Then
            'Comentario
            If Session("QAFiltroUsuarioId") = "" Then
                If (Not whdgNoConformidades.Columns("COMENT") Is Nothing) AndAlso (DBNullToDbl(e.Row.Items.FindItemByKey("COMENTARIOS").Value)) > 0 Then
                    'Añade el icono para ver el comentario
                    e.Row.Items.FindItemByKey("COMENT").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/coment.gif' style='text-align:center;'/>"
                    m_bMostrarComentario = True
                End If
            Else
                If (Not whdgNoConformidades.Columns("COMENT") Is Nothing) AndAlso
                    ((DBNullToStr(e.Row.Items.FindItemByKey("COMENT_CIERRE").Value) <> Nothing OrElse DBNullToStr(e.Row.Items.FindItemByKey("COMENT_ALTA").Value) <> Nothing OrElse DBNullToStr(e.Row.Items.FindItemByKey("COMENT_REVISOR").Value) <> Nothing) And m_bMostrarComentario) Then
                    'Añade el icono para ver el comentario
                    e.Row.Items.FindItemByKey("COMENT").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/coment.gif' style='text-align:center;'/>"
                    m_bMostrarComentario = True
                End If
            End If

            'En proceso
            If CType(Session("FilaEstado"), Integer) <> EstadosNoConformidad.EnCursoDeAprobacion Then
                If (Not whdgNoConformidades.Columns("INDICADOR_EN_PROCESO") Is Nothing) AndAlso (e.Row.Items.FindItemByKey("EN_PROCESO").Value = 1) Then
                    e.Row.Items.FindItemByKey("INDICADOR_EN_PROCESO").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Icono_Error_Amarillo.gif' style='text-align:center;'/>"
                    e.Row.Items.FindItemByKey("INDICADOR_EN_PROCESO").Tooltip = m_sTextoEnProceso
                    m_bMostrarIndicadorEnProceso = True
                End If
            End If
        Else
            If (Not whdgNoConformidades.Columns("EN_PROCESO") Is Nothing) AndAlso IsNumeric(e.Row.Items.FindItemByKey("EN_PROCESO").Value) Then
                If e.Row.Items.FindItemByKey("EN_PROCESO").Value = 1 Then
                    e.Row.Items.FindItemByKey("EN_PROCESO").Value = Textos(39) ' "Sí"
                    m_bMostrarIndicadorEnProceso = True
                Else
                    e.Row.Items.FindItemByKey("EN_PROCESO").Value = Textos(40) '"No"
                End If
            End If
            If m_bMostrarComentario Then
                Dim oNoConformidad As NoConformidad
                Dim ds As DataSet
                oNoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
                ds = oNoConformidad.CargarComentarios(e.Row.Items.FindItemByKey("ID").Value, 0)
                oNoConformidad = Nothing

                For Each oRow In ds.Tables(0).Rows
                    'Comentario Apertura
                    If (Not whdgNoConformidades.Columns("COMENT_ALTA") Is Nothing) AndAlso DBNullToDbl(oRow.Item("TIPO")) = 1 Then
                        whdgNoConformidades.Columns("COMENT_ALTA").Hidden = False
                        If Session("QANombreTabla") = "" Then e.Row.Items.FindItemByKey("COMENT_ALTA").Value = DBNullToStr(oRow.Item("COMENT"))
                    End If

                    'Comentario Cierre
                    If (Not whdgNoConformidades.Columns("COMENT_CIERRE") Is Nothing) AndAlso DBNullToDbl(oRow.Item("TIPO")) = 2 Then
                        whdgNoConformidades.Columns("COMENT_CIERRE").Hidden = False
                        If Session("QANombreTabla") = "" Then e.Row.Items.FindItemByKey("COMENT_CIERRE").Value = DBNullToStr(oRow.Item("COMENT"))
                    End If
                    'Comentario Revisor
                    If (Not whdgNoConformidades.Columns("COMENT_REVISOR") Is Nothing) AndAlso DBNullToDbl(oRow.Item("TIPO")) = 3 Then
                        whdgNoConformidades.Columns("COMENT_REVISOR").Hidden = False
                        If Session("QANombreTabla") = "" Then e.Row.Items.FindItemByKey("COMENT_REVISOR").Value = DBNullToStr(oRow.Item("COMENT"))
                    End If
                    'Comentario Anulacion
                    If (Not whdgNoConformidades.Columns("COMENT_ANULACION") Is Nothing) AndAlso DBNullToDbl(oRow.Item("TIPO")) = 4 Then
                        whdgNoConformidades.Columns("COMENT_ANULACION").Hidden = False
                        If Session("QANombreTabla") = "" Then e.Row.Items.FindItemByKey("COMENT_ANULACION").Value = DBNullToStr(oRow.Item("COMENT"))
                    End If
                Next
            End If
        End If
        For i = 0 To whdgNoConformidades.Columns.Count - 1
            If whdgNoConformidades.Columns(i).Key = "TMPL_ACCION_APROBAR" Then
                Dim chkAprobar As CheckBox = CType(e.Row.Items(i).FindControl("cbAprobar"), CheckBox)
                'Si no tiene asociada accion tipo aprobar o rechazar se deshabilitan los checks                
                chkAprobar.Enabled = Not (DBNullToInteger(e.Row.DataItem.Item.Row.Item("ACCION_APROBAR")) = 0)
                Dim oSerializer As New JavaScriptSerializer
                If _NoConfAprobar IsNot Nothing Then chkAprobar.Checked = (_NoConfAprobar.Find(Function(o) o.id = CType(e.Row.DataItem.Item.Row.Item("ID"), Integer)) IsNot Nothing)
            ElseIf whdgNoConformidades.Columns(i).Key = "TMPL_ACCION_RECHAZAR" Then
                Dim chkRechazar As CheckBox = CType(e.Row.Items(i).FindControl("cbRechazar"), CheckBox)
                chkRechazar.Enabled = Not (DBNullToInteger(e.Row.DataItem.Item.Row.Item("ACCION_RECHAZAR")) = 0)
                Dim oSerializer As New JavaScriptSerializer
                If _NoConfRechazar IsNot Nothing Then chkRechazar.Checked = (_NoConfRechazar.Find(Function(o) o.id = CType(e.Row.DataItem.Item.Row.Item("ID"), Integer)) IsNot Nothing)
            ElseIf whdgNoConformidades.Columns(i).Key = "TMPL_PDTE" Then
                Dim oImg As Image = CType(e.Row.Items(i).FindControl("imgPdte"), Image)
                oImg.Visible = If(Not m_bExportando, (CType(e.Row.DataItem.Item.Row.Item("PDTE"), Integer) = 1), False)
            ElseIf whdgNoConformidades.Columns(i).Key = "TMPL_ERROR" Then
                Dim oImg As Image = CType(e.Row.Items(i).FindControl("imgError"), Image)
                oImg.Visible = If(Not m_bExportando, (CType(e.Row.DataItem.Item.Row.Item("EST_VALIDACION"), Integer) > 1 And CType(e.Row.DataItem.Item.Row.Item("EST_VALIDACION"), Integer) < 99), False)
                If oImg.Visible Then
                    oImg.Attributes.Add("title", IIf(CType(e.Row.DataItem.Item.Row.Item("EST_VALIDACION"), Integer) = 9,
                                    Textos(90) & " " & e.Row.DataItem.Item.Row.Item("FECHA_VALIDACION") & " " & Mid(Textos(91), 1, InStr(Textos(91), ".")) & " " & Textos(92),
                                    Textos(90) & " " & e.Row.DataItem.Item.Row.Item("FECHA_VALIDACION") & " " & Textos(91) & " " & e.Row.DataItem.Item.Row.Item("ERROR_VALIDACION")))
                End If
            ElseIf Left(whdgNoConformidades.Columns(i).Key, 2) = "C_" AndAlso Not whdgNoConformidades.Columns(i).Hidden Then
                If whdgNoConformidades.Columns(i).Type.FullName = "System.Byte" Then
                    ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorNoConformidades
                    If Not e.Row.DataItem Is DBNull.Value Then
                        If e.Row.DataItem.Equals(1) Then
                            e.Row.Items.FindItemByKey(whdgNoConformidades.Columns(i).Key).Text = Textos(39) '"Si"
                        Else
                            e.Row.Items.FindItemByKey(whdgNoConformidades.Columns(i).Key).Text = Textos(40) '"No"
                        End If
                    End If
                    'Si se mira en el fsgsbatchupdate se ve q:
                    '   TipoNumerico    -> ALTER TABLE ...FLOAT     -> System.Double
                    '   TipoBoolean     -> ALTER TABLE ...TINYINT   -> System.Byte
                ElseIf whdgNoConformidades.Columns(i).Type.FullName = "System.Double" Then
                    If IsNumeric(e.Row.DataItem) Then
                        'Para campos numericos tipo telefono hay q quitar los decimales. Para los otros campos numericos 
                        'quitarlos o no carece de importancia (si hay decimales nunca se quitan).
                        e.Row.Items.FindItemByKey(whdgNoConformidades.Columns(i).Key).Text = QuitarDecimalSiTodoCeros(FSNLibrary.FormatNumber(e.Row.Items.FindItemByKey(whdgNoConformidades.Columns(i).Key).Value, FSNUser.NumberFormat))
                    End If
                End If
            End If
        Next
    End Sub
    ''' <summary>
    ''' Evento que salta al Ordenar una columna
    ''' Si se trata de un filtro configurado lo guarda en la BBDD sino en una cookie
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo máximo=0,2seg.</remarks>
    Private Sub whdgNoConformidades_ColumnSorted(sender As Object, e As Infragistics.Web.UI.GridControls.SortingEventArgs) Handles whdgNoConformidades.ColumnSorted
        Dim sName As String
        Dim sortCriteria As String = String.Empty
        Dim sortDirection As String = String.Empty
        Dim bdatabind As Boolean = False
        Dim sColsGroupBy As String = "ColsGroupBy"

        If e.SortedColumns.Count > 0 Then
            For Each sortColumn As Infragistics.Web.UI.GridControls.SortedColumnInfo In e.SortedColumns
                sName = sortColumn.ColumnKey
                sortDirection = IIf(sortColumn.SortDirection = Infragistics.Web.UI.SortDirection.Descending, " DESC", " ASC")
                sortCriteria = IIf(sortCriteria Is String.Empty, sName & sortDirection, sortCriteria & "," & sName & sortDirection)
            Next
        End If
        'Lo almacenamos en cookie
        Dim cookie As HttpCookie
        cookie = Request.Cookies("ORDENACION_QA_NC")
        If cookie Is Nothing Then
            cookie = New HttpCookie("ORDENACION_QA_NC")
        End If
        cookie.Value = sortCriteria
        cookie.Expires = Date.MaxValue
        Response.AppendCookie(cookie)

        UpdatePanel1.Update()
    End Sub
    Private Sub whdgNoConformidades_GroupedColumnsChanged(sender As Object, e As Infragistics.Web.UI.GridControls.GroupedColumnsChangedEventArgs) Handles whdgNoConformidades.GroupedColumnsChanged
        Dim sColsGroupBy As String = "ColsGroupBy"
        Dim groupedColumnList As String = String.Empty
        For Each column As Infragistics.Web.UI.GridControls.GroupedColumn In e.GroupedColumns
            groupedColumnList = IIf(groupedColumnList Is String.Empty, String.Empty, "#")
            groupedColumnList &= column.ColumnKey & IIf(column.SortDirection = Infragistics.Web.UI.GridControls.GroupingSortDirection.Ascending, " ASC", " DESC")
        Next
        Session(sColsGroupBy) = groupedColumnList
        UpdatePanel1.Update()
    End Sub
    ''' <summary>
    ''' Evento que salta al agrupar una columna de la grid.
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo máximo=0seg.</remarks>
    Private Sub whdgNoConformidades_DataFiltered(sender As Object, e As Infragistics.Web.UI.GridControls.FilteredEventArgs) Handles whdgNoConformidades.DataFiltered
        UpdatePanel1.Update()
    End Sub
    ''' <summary>
    ''' Evento que salta una vez que se cargan los datos
    ''' mustra las columnas de en proceso y del comentario si es el caso
    ''' </summary>
    ''' <param name="sender">Propios del evento.</param>
    ''' <param name="e">Propios del evento</param>        
    ''' <remarks>Llamada desde:Evento que salta al pinchar en los filtros de la grid; Tiempo máximo:0seg,</remarks>
    Private Sub whdgNoConformidades_DataBound(sender As Object, e As System.EventArgs) Handles whdgNoConformidades.DataBound
        If Not whdgNoConformidades.Columns("COMENT") Is Nothing Then
            whdgNoConformidades.Columns("COMENT").Hidden = Not m_bMostrarComentario
        End If
        If Not whdgNoConformidades.Columns("INDICADOR_EN_PROCESO") Is Nothing Then
            whdgNoConformidades.Columns("INDICADOR_EN_PROCESO").Hidden = Not m_bMostrarIndicadorEnProceso
        End If
        If whdgNoConformidades.Columns("INDICADOR_EN_PROCESO").Hidden = False Then
            whdgNoConformidades.Columns("INDICADOR_EN_PROCESO").Width = Unit.Pixel(15)
        End If

        Dim _pageCount As Integer
        Dim _rows As Integer
        Dim dt As DataTable = CType(whdgNoConformidades.DataSource, DataSet).Tables(0)
        _rows = dt.Rows.Count
        _pageCount = _rows \ _rowsPage
        _pageCount = _pageCount + (IIf(_rows Mod _rowsPage = 0, 0, 1))
        Paginador(_pageCount)
        UpdatePanel1.Update()
    End Sub
    Private Sub Paginador(ByVal _pageCount As Integer)
        Dim pagerList As DropDownList = DirectCast(whdgNoConformidades.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
        pagerList.Items.Clear()
        For i As Integer = 1 To _pageCount
            pagerList.Items.Add(i.ToString())
        Next
        CType(whdgNoConformidades.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = _pageCount
        If _pageCount > 0 Then pagerList.SelectedIndex = _pageNumber
        Dim Desactivado As Boolean = (_pageNumber = 0)
        With CType(whdgNoConformidades.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        With CType(whdgNoConformidades.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        Desactivado = (_pageCount = 1 OrElse _pageNumber + 1 = _pageCount)
        With CType(whdgNoConformidades.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        With CType(whdgNoConformidades.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
    End Sub
    ''Codigo para guardar datos del FSAL cuando el grid termine de cargarse en el servidor.
    ''Llama a la funcion de registro con diferentes parametros dependiendo de si hay que actualizar o registrar uno nuevo.
    Private Sub whdgNoConformidades_Unload(sender As Object, e As System.EventArgs) Handles whdgNoConformidades.Unload
        If bActivadoFSAL.Value = "1" Then
            Dim fechaFinFSAL8 As DateTime = DateTime.UtcNow
            If bEnviadoFSAL8Inicial.Value = "0" AndAlso IsPostBack Then
                Dim sArgs(5) As Object
                sArgs(0) = 8
                sArgs(1) = sFechaIniFSAL8.Value
                sArgs(2) = Format(fechaFinFSAL8, "yyyy-MM-dd HH:mm:ss") & "." & Format(fechaFinFSAL8.Millisecond, "000")
                sArgs(3) = sIdRegistroFSAL1.Value
                sArgs(4) = sPagina.Value
                sArgs(5) = iP.Value
                System.Threading.ThreadPool.QueueUserWorkItem(New System.Threading.WaitCallback(AddressOf CallFSALRegistrarActualizarAcceso), sArgs)
            ElseIf bEnviadoFSAL8Inicial.Value = "1" AndAlso HttpContext.Current.Request.Headers("x-ajax") Is Nothing Then
                Dim sArgs(12) As Object
                sArgs(0) = 1
                sArgs(1) = sProducto.Value
                sArgs(2) = sFechaIniFSAL8.Value
                sArgs(3) = Format(fechaFinFSAL8, "yyyy-MM-dd HH:mm:ss") & "." & Format(fechaFinFSAL8.Millisecond, "000")
                sArgs(4) = sPagina.Value
                sArgs(5) = iPost.Value
                sArgs(6) = iP.Value
                sArgs(7) = sUsuCod.Value
                sArgs(8) = sPaginaOrigen.Value
                sArgs(9) = sNavegador.Value
                sArgs(10) = sIdRegistroFSAL8.Value
                sArgs(11) = sProveCod.Value
                sArgs(12) = sQueryString.Value
                System.Threading.ThreadPool.QueueUserWorkItem(New System.Threading.WaitCallback(AddressOf CallFSALRegistrarActualizarAcceso), sArgs)
            End If
        End If
    End Sub
    ''
    ''Fin codigo FSAL
#End Region
#Region "Fns Auxiliares"
    ''' <summary>
    ''' Recibe un numero y lo devuelve formateado segun configuraciÃ³n del usuario y aparte quita los decimales si son 0
    ''' </summary>
    ''' <param name="sValor">Numero</param>
    ''' <returns>numero formateado</returns>
    ''' <remarks>Llamadas desde: uwgCertificados_InitializeRow ; Tiempo maximo:0 </remarks>
    Function QuitarDecimalSiTodoCeros(ByVal sValor As String) As String
        Dim decimales As String = ""
        If FSNUser.PrecisionFmt > 0 AndAlso Right(sValor, FSNUser.PrecisionFmt + 1) = FSNUser.DecimalFmt & decimales.PadRight(FSNUser.PrecisionFmt, "0") Then
            sValor = Left(sValor, Len(sValor) - FSNUser.PrecisionFmt - 1)
        End If
        QuitarDecimalSiTodoCeros = sValor
    End Function
    ''Codigo para guardar datos del FSAL, funcion llamada desde el evento 'Unload' del Grid de Solicitudes.
    ''
    ''' <summary>
    ''' Procedimiento incoporporado para realizar la llamada de forma asíncrona a través de un Thread
    ''' </summary>
    ''' <param name="sArgs">Array con los argumentos de llamada a FSNServer.FSAL</param>
    ''' <remarks></remarks>
    Private Sub CallFSALRegistrarActualizarAcceso(ByVal sArgs As Object)
        Dim m_FSNServer As New FSNServer.FSAL

        If DirectCast(sArgs, Array).GetUpperBound(0) = 12 Then
            m_FSNServer.RegistrarAcceso(sArgs(0), sArgs(1), sArgs(2), sArgs(3), sArgs(4), sArgs(5), sArgs(6), sArgs(7), sArgs(8), sArgs(9), sArgs(10), sArgs(11), sArgs(12))
        ElseIf DirectCast(sArgs, Array).GetUpperBound(0) = 5 Then
            m_FSNServer.ActualizarAcceso(sArgs(0), sArgs(1), sArgs(2), sArgs(3), sArgs(4), sArgs(5))
        End If
    End Sub

    ''
    ''Fin codigo FSAL
#End Region
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function Obtener_NoConformidades_Aprobar() As IList
        Try
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim dtNoConf As DataTable = CType(HttpContext.Current.Cache("dtNoConformidades_" & FSNUser.Cod), DataTable)
            Dim dvNoConf As New DataView(dtNoConf, "ACCION_APROBAR>0", "ACCION_APROBAR", DataViewRowState.CurrentRows)
            Return dvNoConf.ToTable.Rows.OfType(Of DataRow).Select(Function(x) New With {Key .id = x("ID"), .bloque = x("BLOQUE"), .accion = x("ACCION_APROBAR")}).ToList()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function Obtener_NoConformidades_Rechazar() As IList
        Try
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim dtNoConf As DataTable = CType(HttpContext.Current.Cache("dtNoConformidades_" & FSNUser.Cod), DataTable)
            Dim dvNoConf As New DataView(dtNoConf, "ACCION_RECHAZAR>0", "ACCION_RECHAZAR", DataViewRowState.CurrentRows)
            Return dvNoConf.ToTable.Rows.OfType(Of DataRow).Select(Function(x) New With {Key .id = x("ID"), .bloque = x("BLOQUE"), .accion = x("ACCION_RECHAZAR")}).ToList()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Sub AprobarNoConformidades(ByVal oNoConformidades As Object)
        RealizarAccion(oNoConformidades)
    End Sub
    <System.Web.Services.WebMethod(True),
      System.Web.Script.Services.ScriptMethod()>
    Public Shared Sub RechazarNoConformidades(ByVal oNoConformidades As Object)
        RealizarAccion(oNoConformidades)
    End Sub

    Private Shared Sub RealizarAccion(ByVal oNoConf As Object)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Try
            Dim sInstanciasActualizarEnProceso As String = ""
            Dim sInstanciasValidar As String = ""
            Dim cInstancia As Instancia
            cInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))

            Dim dsXML As New DataSet
            dsXML.Tables.Add("SOLICITUD")
            Dim drSolicitud As DataRow
            With dsXML.Tables("SOLICITUD").Columns
                .Add("TIPO_PROCESAMIENTO_XML")
                .Add("TIPO_DE_SOLICITUD")
                .Add("COMPLETO")
                .Add("CODIGOUSUARIO")
                .Add("INSTANCIA")
                .Add("USUARIO")
                .Add("USUARIO_EMAIL")
                .Add("USUARIO_IDIOMA")
                .Add("ACCION")
                .Add("IDACCION")
                .Add("COMENTARIO")
                .Add("BLOQUE_ORIGEN")
                .Add("THOUSANFMT")
                .Add("DECIAMLFMT")
                .Add("PRECISIONFMT")
                .Add("DATEFMT")
                .Add("REFCULTURAL")
                .Add("TIPOEMAIL")
                .Add("IDTIEMPOPROC")
            End With

            Dim oSerializer As New JavaScriptSerializer
            Dim oNoConformidades As List(Of DatosNoConf) = oSerializer.Deserialize(Of List(Of DatosNoConf))(oNoConf)

            For Each noconf As DatosNoConf In oNoConformidades
                If Not String.IsNullOrEmpty(sInstanciasActualizarEnProceso) Then sInstanciasActualizarEnProceso &= ","
                If Not String.IsNullOrEmpty(sInstanciasValidar) Then sInstanciasValidar &= "#"

                sInstanciasActualizarEnProceso &= noconf.id
                sInstanciasValidar &= noconf.id & "|" & noconf.bloque & "|" & noconf.accion & "||" & TiposDeDatos.TipoDeSolicitud.NoConformidad
            Next

            If Not String.IsNullOrEmpty(sInstanciasActualizarEnProceso) Then
                Dim cInstancias As Instancias
                cInstancias = FSNServer.Get_Object(GetType(FSNServer.Instancias))
                cInstancias.Actualizar_En_Proceso2(sInstanciasActualizarEnProceso, FSNUser.CodPersona)

                Dim xmlName As String
                Dim lIDTiempoProc As Long
                For Each info As String In Split(sInstanciasValidar, "#")
                    dsXML.Tables("SOLICITUD").Rows.Clear()

                    cInstancia.ID = Split(info, "|")(0)
                    cInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, FSNUser.Cod, strToLong(Split(info, "|")(1)))
                    drSolicitud = dsXML.Tables("SOLICITUD").NewRow
                    With drSolicitud
                        .Item("TIPO_PROCESAMIENTO_XML") = CInt(TipoProcesamientoXML.FSNWeb_PM_AprobacionRechazoMultiple)
                        .Item("TIPO_DE_SOLICITUD") = CInt(TiposDeDatos.TipoDeSolicitud.NoConformidad)
                        .Item("COMPLETO") = 1
                        .Item("CODIGOUSUARIO") = FSNUser.Cod
                        .Item("INSTANCIA") = Split(info, "|")(0)
                        .Item("USUARIO") = FSNUser.CodPersona
                        .Item("USUARIO_EMAIL") = FSNUser.Email
                        .Item("USUARIO_IDIOMA") = FSNUser.IdiomaCod
                        .Item("ACCION") = ""
                        .Item("IDACCION") = Split(info, "|")(2)
                        .Item("COMENTARIO") = ""
                        .Item("BLOQUE_ORIGEN") = Split(info, "|")(1)
                        .Item("THOUSANFMT") = FSNUser.ThousanFmt
                        .Item("DECIAMLFMT") = FSNUser.DecimalFmt
                        .Item("PRECISIONFMT") = FSNUser.PrecisionFmt
                        .Item("DATEFMT") = FSNUser.DateFmt
                        .Item("REFCULTURAL") = FSNUser.Idioma.RefCultural
                        .Item("TIPOEMAIL") = FSNUser.TipoEmail
                        .Item("IDTIEMPOPROC") = lIDTiempoProc
                    End With
                    xmlName = FSNUser.Cod & "#" & Split(info, "|")(0) & "#" & Split(info, "|")(1)
                    dsXML.Tables("SOLICITUD").Rows.Add(drSolicitud)

                    cInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=TiempoProcesamiento.InicioDisco)
                    If ConfigurationManager.AppSettings("SERVIDOR_TRATAMIENTO_INSTANCIAS") = "0" Then
                        'Se hace con un StringWriter porque el método GetXml del dataset no incluye el esquema
                        Dim oSW As New System.IO.StringWriter()
                        dsXML.WriteXml(oSW, XmlWriteMode.WriteSchema)

                        Dim oSrvTrataInst As New FSNWebServiceXML.TratamientoInstancias
                        oSrvTrataInst.TransferirXMLEnEpera(oSW.ToString(), xmlName)
                    Else
                        dsXML.WriteXml(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#FSNWEB.xml", XmlWriteMode.WriteSchema)
                        If IO.File.Exists(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml") Then _
                            IO.File.Delete(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
                        FileSystem.Rename(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#FSNWEB.xml",
                                          ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
                    End If
                Next
                cInstancias = Nothing
            End If
        Catch ex As Exception
            HttpContext.Current.Session("AjaxException") = ex
            Throw ex
        End Try
    End Sub

#Region "Template columna CHECKBOX"
    Private Class ImgTemplate
        Implements ITemplate
        Private _id As String
        Private _sImg As String

        ''' <summary>
        ''' Rellena un Item del webdatagrid 
        ''' </summary>
        ''' <param name="container">Item del webdatagrid a rellenar</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Public Sub InstantiateIn(ByVal container As System.Web.UI.Control) Implements System.Web.UI.ITemplate.InstantiateIn
            Dim img As New Image
            img.ID = _id
            img.Width = Unit.Pixel(20)
            img.ImageUrl = _sImg
            img.CssClass = "Image16"
            container.Controls.Add(img)
        End Sub
        Public Sub New(ByVal Id As String, ByVal sImg As String)
            _id = Id
            _sImg = sImg
        End Sub
    End Class
    Private Class CheckboxTemplate
        Implements ITemplate
        Private _id As String
        ''' <summary>
        ''' Rellena un Item del webdatagrid 
        ''' </summary>
        ''' <param name="container">Item del webdatagrid a rellenar</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Public Sub InstantiateIn(ByVal container As System.Web.UI.Control) Implements System.Web.UI.ITemplate.InstantiateIn
            Dim chk As New CheckBox
            chk.ID = _id
            chk.Width = Unit.Pixel(25)
            container.Controls.Add(chk)
        End Sub
        Public Sub New(ByVal Id As String)
            _id = Id
        End Sub
    End Class
#End Region
#Region "Template cabecera BUTTON"
    Private Class CheckAndButtonTemplate
        Implements ITemplate
        Private _texto As String
        Private _idCheck As String
        Private _idBoton As String
        ''' <summary>
        ''' Rellena un Item del webdatagrid 
        ''' </summary>
        ''' <param name="container">Item del webdatagrid a rellenar</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Public Sub InstantiateIn(ByVal container As System.Web.UI.Control) Implements System.Web.UI.ITemplate.InstantiateIn
            Dim divContenedor As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
            Dim chk As New CheckBox
            Dim divBoton As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
            Dim lblBoton As New System.Web.UI.HtmlControls.HtmlGenericControl("span")

            chk.Style.Add("margin-right", "10px")
            chk.ID = _idCheck
            With divContenedor
                .Style.Add("position", "relative")
                .Style.Add("clear", "both")
                .Style.Add("float", "left")
                .Style.Add("width", "100%")
                .Style.Add("height", "100%")
                .Style.Add("text-align", "center")
            End With
            With divBoton
                .ID = _idBoton
                .Attributes.Add("class", "botonRedondeado")
            End With
            lblBoton.InnerText = _texto
            divBoton.Controls.Add(lblBoton)
            divContenedor.Controls.Add(chk)
            divContenedor.Controls.Add(divBoton)
            container.Controls.Add(divContenedor)
        End Sub
        Public Sub New(ByVal IdCheck As String, ByVal IdBoton As String, ByVal Texto As String)
            _idCheck = IdCheck
            _idBoton = IdBoton
            _texto = Texto
        End Sub
    End Class
#End Region
    <Serializable()>
    Private Class DatosNoConf
        Public id As Integer
        Public bloque As Integer
        Public accion As Integer
    End Class

End Class