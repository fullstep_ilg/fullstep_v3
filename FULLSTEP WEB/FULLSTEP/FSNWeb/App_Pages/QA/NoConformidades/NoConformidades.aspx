﻿<%@ Page Language="vb" Debug="true" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.Master"  CodeBehind="NoConformidades.aspx.vb" Inherits="Fullstep.FSNWeb.NoConformidades" %>

<asp:Content ID="ContentHead" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
	var timeoutID = 0;
	var x = 0;
	var y = 0;
	var bAprobarAllChecked = false;
	var bRechazarAllChecked = false;
	var noconfAprobar = [];
	var noconfRechazar = [];
	
	/*<summary>
	Obtiene las posiciones del cursor, necesario para el FireFox
	</summary>
	<remarks>Llamada desde: Grid_MouseOverHadler; Tiempo=0seg.</remarks>*/
	function Posicion(event) {
	    x = event.clientX;
	    y = event.clientY;
	};
	/*<summary>
	Provoca el postback para hacer la exportación a excel
	</summary>
	<remarks>Llamada desde: Icono de exportar a Excel</remarks>*/
	function MostrarExcel() {
	    __doPostBack("", "Excel");
	};
	/*<summary>
	Provoca el postback para hacer la exportación a Pdf
	</summary>
	<remarks>Llamada desde: Icono de exportar a Pdf</remarks>*/
	function MostrarPdf() {
	    __doPostBack("", "Pdf");
	};	
    /*Revisado por: Jbg. Fecha: 26/10/2011
	<summary>
	Abre en popup la ventana de envio de email pasándole los email de contacato de calidad de los proveedores seleccionados en el grid
	</summary>
	<remarks>Llamada desde: btnEnviarEmail; /remarks>*/
	function EnviarEmail() {
	    _grid = $find("<%=whdgNoConformidades.ClientId%>");
	    var parentgrid = _grid.get_gridView();
	    var listaProveedores = '';
	    var selectedRow;
	    var proveEmail;
	    for (i = 0; i < parentgrid.get_behaviors().get_selection().get_selectedRows().get_length() ; i++) {
	        selectedRow = parentgrid.get_behaviors().get_selection().get_selectedRows().getItem(i);
	        proveEmail = selectedRow.get_cellByColumnKey("CON_EMAIL").get_value();
	        if (proveEmail != null)
	            if (listaProveedores.search(proveEmail) == -1)
	                listaProveedores = listaProveedores + proveEmail + ';';
	    }
	    listaProveedores = String(listaProveedores).substring(0, listaProveedores.length - 1);
	    window.open("<%=System.Configuration.ConfigurationManager.AppSettings("rutaQA")%>_common/MailProveedores.aspx?Origen=3&Proveedores=" + listaProveedores, "_blank", "width=710,height=360,status=yes,resizable=no,top=180,left=200");
	    return false;
	};    
    /* <summary>
	''' Responder al evento click en una celda del grid
	''' </summary>
	''' <remarks>Llamada desde: sistema; Tiempo máximo:0</remarks>*/
    function grid_CellClick(sender, e) {        
        if (e.get_type() == "cell" && e.get_item().get_row().get_index() !== -1) {
            if (e.get_item().get_grid().get_behaviors().get_activation().get_activeCellResolved() !== null) {
                var cell = e.get_item().get_grid().get_behaviors().get_activation().get_activeCellResolved();

                posicionX = $(cell.get_element()).position().left + ($(cell.get_element()).innerWidth() / 4);
                posicionY = $(cell.get_element()).position().top + 20;

                switch (cell.get_column().get_key()) {
                    case "TMPL_ACCION_APROBAR":
                        break;
                    case "TMPL_ACCION_RECHAZAR":
                        break;
                    case "EN_PROCESO":
                    case "INDICADOR_EN_PROCESO":
                        e.set_cancel(true)
                        break;
                    case "CODPROVE":
                    case "DENPROVE":
                        FSNMostrarPanel(ProveAnimationClientID, e, ProveDynamicPopulateClienteID, cell.get_row().get_cellByColumnKey("CODPROVE").get_value(), null, null, posicionX, posicionY);
                        break;
                    case "REVISOR":
                        if (cell.get_row().get_cellByColumnKey("CODREV").get_value() != "") {
                            FSNMostrarPanel(RevisorAnimationClientID, e, RevisorDynamicPopulateClienteID, cell.get_row().get_cellByColumnKey("CODREV").get_value(), null, null, posicionX, posicionY);
                        }
                        break;
                    case "PET":
                    case "CODPET":
                        if (cell.get_row().get_cellByColumnKey("CODPET").get_value() != "")
                            FSNMostrarPanel(PeticionarioAnimationClientID, e, PeticionarioDynamicPopulateClienteID, cell.get_row().get_cellByColumnKey("CODPET").get_value(), null, null, posicionX, posicionY);
                        break;
                    case "COMENT":
                    case "COMENT_ALTA":
                    case "COMENT_CIERRE":
                    case "COMENT_REVISOR":
                        var lInstancia = cell.get_row().get_cellByColumnKey('ID').get_value();
                        var icomentarios = 0;
                        if (cell.get_column().get_key() == "COMENT") {
                            iComentarios = 0;
                        } else if (cell.get_column().get_key() == "COMENT_ALTA") {
                            iComentarios = 1;
                        } else if (cell.get_column().get_key() == "COMENT_CIERRE") {
                            iComentarios = 2;
                        } else if (cell.get_column().get_key() == "COMENT_REVISOR") {
                            iComentarios = 3;
                        }
                        var valor = ""
                        valor = icomentarios + "#" + lInstancia;
                        
                        posicionX -= $("[id$=FSNPanelComentarioQA_pnlInfo]").width();
                        FSNMostrarPanel(ComentAnimationClientID, e, ComentDynamicPopulateClienteID, valor, null, null, posicionX, posicionY);
                        break;
                    default:
                        var estado = cell.get_row().get_cellByColumnKey('EN_PROCESO').get_value();
                        if (estado != 1) {
                            //Redirigir a detalleNoConformidad si no esta en proceso
                            IdFiltroUsu = document.getElementById("<%=QAFiltroUsuarioIdVNoConf.ClientID%>").value;
                            IdFiltro = document.getElementById("<%=QAFiltroIdVNoConf.ClientID%>").value;

                            $.when($.ajax({
                                type: 'POST',
                                url: rutaFS + '_Common/App_services/Consultas.asmx/ComprobarEnProceso',
                                data: JSON.stringify({ contextKey: cell.get_row().get_cellByColumnKey('ID').get_value() }),
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'json',
                                async: true
                            })).done(function (msg) {
                                if (msg.d == "1") {
                                    FSNMostrarPanel(ProcesoAnimationClientID, event, ProcesoDynamicPopulateClienteID, 2, null, null, posicionX, posicionY);
                                } else {
                                    TempCargando();
                                    var FechaLocal
                                    FechaLocal = new Date()
                                    svolver = "*volver=<%=ConfigurationManager.AppSettings("rutaQA2008")%>NoConformidades/NoConformidades.aspx?FiltroUsuarioIdAnt=" + IdFiltroUsu + "**NombreTablaAnt=<%=Session("QANombreTabla")%>**FiltroIdAnt=" + IdFiltro + "**estado=<%=Session("FilaEstado")%>"
                                    sref = "<%=ConfigurationManager.AppSettings("rutaQA")%>frames.aspx?pagina=noconformidad/detalleNoConformidad.aspx?NoConformidad=" + cell.get_row().get_cellByColumnKey('NOCONFORMIDAD').get_value() + "*otz=" + FechaLocal.getTimezoneOffset()

                                    window.open(sref + svolver, "_self");
                                };
                            });
                        }
                        break;
                }
            }
        }
    };	
	/*Descripcion: Funcion que inicializa la caja de texto del combo Subtipo a ""
	Parametros entrada:=
		elem:=componente input
		event:=evento
	Llamada desde: Al cambiar el valor del combo tipo
	Tiempo ejecucion:= 0seg.*/
    function ValueChanged_wddTipo_CambioTipo(elem, event) {
        comboSubtipo = document.getElementById("x:1129115704.2:mkr:Input")
        if (comboSubtipo)
            comboSubtipo.value = ""
    };
    /*<summary>
    ''' Ordena los datos por la columna indicada.
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">evento</param>        
    ''' <remarks>Llamada desde: Sistema ; Tiempo máximo: 0</remarks>*/
    function whdgNoConformidades_Sorting(sender, e) {
        switch (e.get_column().get_key()) {
            case "INDICADOR_EN_PROCESO":
            case "COMENT":
                e.set_cancel(true);
                break;
            default:
                var sortSentence;
                sortSentence = e.get_column().get_key() + " " + (e.get_sortDirection() == 1 ? "ASC" : "DESC");
                break;
        }
    };
    /*<summary>
    ''' Function que filtra el grid (el "embudo")
    ''' </summary>
    ''' <remarks>Llamada desde: sistema; Tiempo máximo:0seg.</remarks>*/     
    function whdgNoConformidades_Filtering(sender, e) {
        var gridFiltered = false;
        var filteredColumns, filteredCondition, filteredValue;
        var grid1erFilterValueVacio = false;
        $('[id$=hFilteredColumns]').val('');
        $('[id$=hFilteredCondition]').val('');
        $('[id$=hFilteredValue]').val('');
        for (i = 0; i < e.get_columnFilters().length; i++) {
            if (e.get_columnFilters()[i].get_condition().get_rule() != 0) {
                gridFiltered = true;
                filteredColumns = $('[id$=hFilteredColumns]').val();
                filteredCondition = $('[id$=hFilteredCondition]').val();
                filteredValue = $('[id$=hFilteredValue]').val();
                $('[id$=hFilteredColumns]').val((filteredColumns == '' ? e.get_columnFilters()[i].get_columnKey() : filteredColumns + "#" + e.get_columnFilters()[i].get_columnKey()));
                $('[id$=hFilteredCondition]').val((filteredCondition == '' ? e.get_columnFilters()[i].get_condition().get_rule() : filteredCondition + "#" + e.get_columnFilters()[i].get_condition().get_rule()));
                if (grid1erFilterValueVacio) {
                    grid1erFilterValueVacio = false;
                    $('[id$=hFilteredValue]').val("#" + e.get_columnFilters()[i].get_condition().get_value());
                    filteredValue = $('[id$=hFilteredValue]').val();
                }
                else {
                    $('[id$=hFilteredValue]').val((filteredValue == '' ? e.get_columnFilters()[i].get_condition().get_value() : filteredValue + "#" + e.get_columnFilters()[i].get_condition().get_value()));
                }
                if ((e.get_columnFilters()[i].get_condition().get_value() == '') && (filteredValue == '')) {
                    grid1erFilterValueVacio = true;
                }
            }
        }
    };
    /*<summary>
    ''' Function que se ejecuta en la carga del Grid en cliente
    ''' </summary>
    ''' <remarks>Llamada desde: evento del Grid whdgNoConformidades; Tiempo máximo:0seg.</remarks>*/     
    function whdgNoConformidades_Initialize(sender, e) {
        /*Codigo para guardar datos del FSAL cuando se cargue el Grid.
          Si es primera llamada a la pagina y no se han enviado los tiempos para update, el booleano estara a 'False'.
          Entonces se hara la update y se pondra a 'True' para hacer la insercion de un nuevo registro. */
        if ($('#bActivadoFSAL').val() == '1') {
            if ($('#bEnviadoFSAL8Inicial').val() == '0') {
                $('#bEnviadoFSAL8Inicial').val('1');
            }
        }
        /*Fin codigo FSAL*/
    };
    function IndexChanged() {
        var dropdownlist = $('[id$=PagerPageList]')[0];
        var btnPager = $('[id$=btnPager]').attr('id');
        var selectedIndex = dropdownlist.selectedIndex;

        var oParameter = {};
        oParameter.noconfAprobar = JSON.stringify(noconfAprobar);
        oParameter.noconfRechazar = JSON.stringify(noconfRechazar);        
        oParameter.selectedIndex = dropdownlist.selectedIndex;
        __doPostBack(btnPager, JSON.stringify(oParameter));
    };
    function FirstPage() {
        var dropdownlist = $('[id$=PagerPageList]')[0];
        var btnPager = $('[id$=btnPager]').attr('id');
        var oParameter = {};
        oParameter.noconfAprobar = JSON.stringify(noconfAprobar);
        oParameter.noconfRechazar = JSON.stringify(noconfRechazar);        
        oParameter.selectedIndex = 0;
        dropdownlist.options[0].selected = true;
        __doPostBack(btnPager, JSON.stringify(oParameter));
    };
    function PrevPage() {
        var dropdownlist = $('[id$=PagerPageList]')[0];
        var btnPager = $('[id$=btnPager]').attr('id');
        var selectedIndex = dropdownlist.selectedIndex;        
        if (selectedIndex - 1 >= 0) {
            dropdownlist.options[selectedIndex - 1].selected = true;

            var oParameter = {};
            oParameter.noconfAprobar = JSON.stringify(noconfAprobar);
            oParameter.noconfRechazar = JSON.stringify(noconfRechazar);           
            oParameter.selectedIndex = dropdownlist.selectedIndex;

            __doPostBack(btnPager, JSON.stringify(oParameter));
        }
    };
    function NextPage() {        
        var dropdownlist = $('[id$=PagerPageList]')[0];
        var btnPager = $('[id$=btnPager]').attr('id');
        var selectedIndex = dropdownlist.selectedIndex;
        if (selectedIndex + 1 <= dropdownlist.length - 1) {
            dropdownlist.options[selectedIndex + 1].selected = true;

            var oParameter = {};
            oParameter.noconfAprobar = JSON.stringify(noconfAprobar);
            oParameter.noconfRechazar = JSON.stringify(noconfRechazar);            
            oParameter.selectedIndex = dropdownlist.selectedIndex;

            __doPostBack(btnPager,  JSON.stringify(oParameter));
        }
    };
    function LastPage() {
        var dropdownlist = $('[id$=PagerPageList]')[0];
        var btnPager = $('[id$=btnPager]').attr('id');
        dropdownlist.options[dropdownlist.length - 1].selected = true;

        var oParameter = {};
        oParameter.noconfAprobar = JSON.stringify(noconfAprobar);
        oParameter.noconfRechazar = JSON.stringify(noconfRechazar);        
        oParameter.selectedIndex = dropdownlist.length - 1;
        __doPostBack(btnPager, JSON.stringify(oParameter));
    };
    function AltaNoconformidad() {        
        var o = $find($('[id$=wddTipoSolicitudes]').attr('id'));
        if (o.get_selectedItems().length>0) {
            window.open(rutaQA + 'frames.aspx?pagina=NoConformidad/altaNoConformidad.aspx?TipoSolicit=' + o.get_selectedItems()[0].get_value(), '_top');
        }
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">	
    <script type="text/javascript">        
        //Click en el checkbox "Aprobar" de una fila del grid de Solicitudes
        $('[id$=cbAprobar]').live('click', function () {            
            var oGrid = $find($('[id$=whdgNoConformidades]').attr('id'));
            var rowIndex = oGrid.get_gridView().get_behaviors().get_activation().get_activeCell().get_row().get_index();            
            var ID = oGrid.get_gridView().get_rows().get_row(rowIndex).get_cellByColumnKey("ID").get_value();
            var BLOQUE = oGrid.get_gridView().get_rows().get_row(rowIndex).get_cellByColumnKey("BLOQUE").get_value();
            var ACCION_APROBAR = oGrid.get_gridView().get_rows().get_row(rowIndex).get_cellByColumnKey("ACCION_APROBAR").get_value();
            var noconf = { id: ID, bloque: BLOQUE, accion: ACCION_APROBAR };

            if ($(this).prop('checked') && $.grep(noconfAprobar, function (o) { return o.id == ID; }).length == 0) {                
                noconfAprobar.push(noconf);
                noconfRechazar = $.grep(noconfRechazar, function (o) { return o.id !== noconf.id; });

                $('[id$=cbRechazar]')[rowIndex].checked = false;
                $('[id$=chkRechazarCheckAll]').prop('checked', false);
                bRechazarAllChecked = false;                
            }
            else {
                noconfAprobar = $.grep(noconfAprobar, function (o) { return o.id !== noconf.id; });

                $('[id$=chkAprobarCheckAll]').prop('checked', false);                
                bAprobarAllChecked = false;                
            }
        });
        //Click en el checkbox "Rechazar" de una fila del grid de Solicitudes
        $('[id$=cbRechazar]').live('click', function () {            
            var oGrid = $find($('[id$=whdgNoConformidades]').attr('id'));
            var rowIndex = oGrid.get_gridView().get_behaviors().get_activation().get_activeCell().get_row().get_index();
            var ID = oGrid.get_gridView().get_rows().get_row(rowIndex).get_cellByColumnKey("ID").get_value();
            var BLOQUE = oGrid.get_gridView().get_rows().get_row(rowIndex).get_cellByColumnKey("BLOQUE").get_value();
            var ACCION_RECHAZAR = oGrid.get_gridView().get_rows().get_row(rowIndex).get_cellByColumnKey("ACCION_RECHAZAR").get_value();
            var noconf = { id: ID, bloque: BLOQUE, accion: ACCION_RECHAZAR };

            if ($(this).prop('checked') && $.grep(noconfRechazar, function (o) { return o.id == ID; }).length == 0) {
                noconfRechazar.push(noconf);
                noconfAprobar = $.grep(noconfAprobar, function (o) { return o.id !== noconf.id; });

                $('[id$=cbAprobar]')[rowIndex].checked = false;
                $('[id$=chkAprobarCheckAll]').prop('checked', false);
                bAprobarAllChecked = false;
            }
            else {
                noconfRechazar = $.grep(noconfRechazar, function (o) { return o.id !== noconf.id; });

                $('[id$=chkRechazarCheckAll]').prop('checked', false);
                bRechazarAllChecked = false;
            }
        });

        //Click en el checkbox "Aprobar" de la cabecera de la columna de aprobar del grid
        $('[id$=chkAprobarCheckAll]').live('click', function () { CheckAll(true, this.checked); })
        //Click en el checkbox "Rechazar" de la cabecera de la columna de rechazar del grid
        $('[id$=chkRechazarCheckAll]').live('click', function () { CheckAll(false, this.checked); })
        //Realiza el check en todas las filas de la columna de aprobar o rechazar del grid
        function CheckAll(bAprobar, bCheck) {           
            //Obtener los datos de todas las no conformidades que se pueden aprobar o rechazar
            $.when($.ajax({
                type: "POST",
                url: rutaFS + 'QA/NoConformidades/NoConformidades.aspx/' + (bAprobar ? 'Obtener_NoConformidades_Aprobar' : 'Obtener_NoConformidades_Rechazar'),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false
            })).done(function (msg) {
                if (bAprobar) {
                    bAprobarAllChecked = bCheck;
                    bRechazarAllChecked = !bCheck;
                }
                else {
                    bRechazarAllChecked = bCheck;
                    bAprobarAllChecked = !bCheck;
                }
                
                //Rellenar los arrays noconfAprobar y noconfRechazar. Hacer el check de las filas de la página actual                
                var oGrid = $find($('[id$=whdgNoConformidades]').attr('id'));
                noconfAprobar = [];
                noconfRechazar = [];
                $.each(msg.d, function () {                    
                    var noconf = { id: this.id, bloque: this.bloque, accion: this.accion };

                    if (bAprobar) {
                        if (bCheck) noconfAprobar.push(noconf);                                                                       
                    }
                    else {
                        if (bCheck) noconfRechazar.push(noconf);                                                                      
                    }                    
                });
                
                var sCheckAllID = (bAprobar ? "chkRechazarCheckAll" : "chkAprobarCheckAll");
                if ($('[id$=' + sCheckAllID + ']').prop('checked'))  $('[id$=' + sCheckAllID + ']').prop('checked', !bCheck);

                //Marcar los checks de la página visible
                var sCheckID = (bAprobar ? "cbAprobar" : "cbRechazar");
                var sCheckID2 = (bAprobar ? "cbRechazar" : "cbAprobar");                
                var oGrid = $find($('[id$=whdgNoConformidades]').attr('id'));
                for (i = 0; i < oGrid.get_gridView().get_rows().get_length() ; i++) {                    
                    if (!$('[id$=' + sCheckID + ']')[i].disabled) $('[id$=' + sCheckID + ']')[i].checked = bCheck;
                    if ($('[id$=' + sCheckID2 + ']')[i].checked && !$('[id$=' + sCheckID2 + ']')[i].disabled) $('[id$=' + sCheckID2 + ']')[i].checked = !bCheck;
                }
            });
        }

        $('[id$=btnAprobar]').live('click', function () {
            var oGrid = $('[id$=whdgNoConformidades]').attr('id');            
            MostrarCargando()
            for (i = 0; i < $find(oGrid).get_gridView().get_rows().get_length() ; i++) {
                if ($('[id$=cbAprobar]')[i].checked) {                    
                    $find(oGrid).get_gridView().get_rows().get_row(i).get_element().style.display = 'none';
                }
            }
            $.when($.ajax({
                type: "POST",
                url: rutaFS + 'QA/NoConformidades/NoConformidades.aspx/AprobarNoConformidades',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ oNoConformidades: JSON.stringify(noconfAprobar) }),
                dataType: "json",
                async: false,
                success: function () {
                    setTimeout('OcultarCargando()', 500);
                }
            }));
        });
        $('[id$=btnRechazar]').live('click', function () {
            var oGrid = $('[id$=whdgNoConformidades]').attr('id');            
            MostrarCargando();
            for (i = 0; i < $find(oGrid).get_gridView().get_rows().get_length() ; i++) {
                if ($('[id$=cbRechazar]')[i].checked) {                    
                    $find(oGrid).get_gridView().get_rows().get_row(i).get_element().style.display = 'none';
                }
            }
            $.when($.ajax({
                type: "POST",
                url: rutaFS + 'QA/NoConformidades/NoConformidades.aspx/RechazarNoConformidades',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ oNoConformidades: JSON.stringify(noconfRechazar) }),
                dataType: "json",
                async: false,
                success: function () {
                    setTimeout('OcultarCargando()', 500);
                }
            }));
        });
        function OcultarCargando() {
            var modalprog = $find(ModalProgress);
            if (modalprog) modalprog.hide();
        };
    </script>
	<fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
	<asp:UpdatePanel ID="upPrincipal" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<table style="width:100%;" class="Rectangulo" border="0">
				<tr>
					<td style="width:100px; text-align:right;">
						<asp:Label ID="lblIdentificador" runat="server" Text="Identificador:" Font-Bold="true"></asp:Label>
					</td>
					<td style="width:150px;">
						<asp:TextBox ID="txtIdentificador" runat="server" Width="150px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revBuscarId" ControlToValidate="txtIdentificador" runat="server" Display="Dynamic"
							EnableClientScript="False" ValidationExpression="\d+">
                        </asp:RegularExpressionValidator>
					</td>
					<td>
						<fsn:FSNButton ID="btnBuscarIdentificador" runat="server" Alineacion="Left"></fsn:FSNButton>
					</td>
					<td style="width:50px;">
						<asp:Label ID="lblTipo2" runat="server" Font-Bold="true"></asp:Label>
					</td>
					<td style="width:320px;">
						<ig:WebDropDown ID="wddTipoSolicitudes" runat="server" Width="320px" Height="20px"
							EnableAutoCompleteFirstMatch="false" DropDownContainerWidth="320px" DropDownContainerHeight="200px"
							EnableClosingDropDownOnSelect="true" CurrentValue="">
						</ig:WebDropDown>
					</td>
					<td style="width:200px;">
						<fsn:FSNButton ID="btnAlta" runat="server" Alineacion="Left" style="white-space:nowrap;" OnClientClick="AltaNoconformidad()"></fsn:FSNButton>
					</td>
				</tr>
			</table>
		</ContentTemplate>
	</asp:UpdatePanel>		 
	<asp:Panel ID="pnlCabeceraParametros" runat="server" Width="100%">
		<table style="width:100%; border:0px; border-collapse: collapse; border-spacing: 0; padding:0px;">
			<tr>
				<td style="vertical-align:middle; text-align:left; padding-top:5px;">
					<asp:Panel runat="server" ID="pnlBusquedaAvanzada" Style="cursor: pointer" Font-Bold="True"
						Height="20px" Width="100%" CssClass="PanelCabecera">
						<table style="border:0px; border-collapse: collapse; border-spacing: 0; padding:0px;">
							<tr>
								<td style="padding-top: 2px">
									<asp:Image ID="imgExpandir" runat="server" SkinID="Expandir" />
								</td>
								<td style="vertical-align: top; padding-top: 2px">
									<asp:Label ID="lblBusquedaAvanzada" runat="server" Font-Bold="True" ForeColor="White"></asp:Label>
								</td>
							</tr>
						</table>
					</asp:Panel>
				</td>
			</tr>
		</table>
	</asp:Panel>
	<asp:Panel ID="pnlParametros" runat="server" CssClass="Rectangulo" Width="99%">
		<asp:UpdatePanel ID="upBusquedaAvanzada" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<fsqa:BusquedaAvanzadaNC ID="Busqueda" runat="server"></fsqa:BusquedaAvanzadaNC>
				<table style="width:80%; border:0px; border-collapse: collapse; border-spacing: 0; padding:4px;">
					<tr>
						<td style="width:60px;">
							<fsn:FSNButton ID="btnBuscarBusquedaAvanzada" runat="server" Text="DBuscar" Alineacion="left"></fsn:FSNButton>
						</td>
						<td style="width:60px;">
							<fsn:FSNButton ID="btnLimpiarBusquedaAvanzada" runat="server" Text="DLimpiar" Alineacion="Right"></fsn:FSNButton>
						</td>
						<td colspan="6"></td>
					</tr>
				</table>
			</ContentTemplate>
		</asp:UpdatePanel>
	</asp:Panel>
	<ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" CollapseControlID="pnlCabeceraParametros"
		ExpandControlID="pnlCabeceraParametros" ImageControlID="imgExpandir" SuppressPostBack="true"
		TargetControlID="pnlParametros" Collapsed="true" SkinID="FondoRojo">
	</ajx:CollapsiblePanelExtender>
	<br />
    <asp:UpdatePanel runat="server" ID="upPager" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="hFilteredColumns" Value="" />
            <asp:HiddenField runat="server" ID="hFilteredCondition" Value="" />
            <asp:HiddenField runat="server" ID="hFilteredValue" Value="" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button runat="server" ID="btnPager" style="display:none;" />
	<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnPager" EventName="Click" />
		</Triggers>
		<ContentTemplate>
            <asp:HiddenField ID="QAFiltroUsuarioIdVNoConf" runat="server" Value="0" />
			<asp:HiddenField ID="QAFiltroIdVNoConf" runat="server" Value="0" />
            <asp:HiddenField runat="server" ID="sProducto" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="sFechaIniFSAL8" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="sPagina" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="iPost" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="iP" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="sUsuCod" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="sPaginaOrigen" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="sNavegador" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="sIdRegistroFSAL8" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="sProveCod" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="sQueryString" ClientIDMode="Static" />
			<table style="width:100%; border:0px; border-collapse: collapse; border-spacing: 0; padding:0px;">
				<tr>
					<td>
						<table style="border:0px; border-collapse: collapse; border-spacing: 0; padding:0px;">
							<tr>
								<td style="vertical-align:bottom;">
									<asp:DataList ID="dlFiltrosConfigurados" runat="server" DataSourceID="odsFiltrosConfigurados"
										RepeatDirection="Horizontal" CellPadding="0" RepeatLayout="Table" RepeatColumns="5">
										<ItemTemplate>
											<td>
												<fsn:FSNTab ID="fsnTabFiltro" SkinID="PestanyasFiltros" Alineacion="Left" runat="server"
													Visible="true" Text='<%# IIf(Eval("ID")=0,Textos(0),Mid(Eval("NOM_FILTRO"),1,20)) & IIf(Len(Eval("NOM_FILTRO"))>20,"...","") %>'
													ToolTip='<%# IIf(Eval("ID")=0,"",Eval("NOM_FILTRO")) %>' OnClick="fsnTabFiltro_Click" 
													CommandArgument='<%# Eval("ID") & "#" & Eval("NOM_TABLA")  & "#" & Eval("IDFILTRO") %>'
													BorderStyle="None" BorderColor="#AAAAAA" BorderWidth="1px"></fsn:FSNTab>
											</td>
										</ItemTemplate>
									</asp:DataList>
								</td>
								<td style="width:10px;">
									&nbsp;
								</td>
								<td>
									<fsn:FSNButton ID="btnAnyadirFiltros" runat="server" Text="DAñadir filtros"></fsn:FSNButton>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table style="width:99%; border:0px; border-collapse: collapse; border-spacing: 0; padding:0px;">
				<tr>
					<td>
						<asp:Panel ID="panTabsEstados" runat="server" BackColor="#aaaaaa" Height="28px" Width="100%">
							<div style="border: 1px solid #aaaaaa;">
								<table id="tblEstados" style="width:100%; border:0px;">
									<tr>
										<td style="white-space:nowrap;">
											<fsn:FSNButton ID="btnBoton1" runat="server" Alineacion="left" Font-Size="11px" SkinID="SubmenuFiltros"
												ColorFondo="" ColorFondoHover="" ImagenesBordes="" Text="Abiertas()" />
										</td>
                                        <td style="white-space:nowrap;">
											<fsn:FSNButton ID="btnBoton9" runat="server" Alineacion="left" Font-Size="11px" SkinID="SubmenuFiltros"
												ColorFondo="" ColorFondoHover="" ImagenesBordes="" Text="dEn curso de aprobacion()" />
										</td>
										<td style="white-space:nowrap;">
											<fsn:FSNButton ID="btnBoton2" runat="server" Alineacion="left" Font-Size="11px" SkinID="SubmenuFiltros"
												ColorFondo="" ColorFondoHover="" ImagenesBordes="" Text="Guardadas()" />
										</td>
										<td style="white-space:nowrap;">
											<fsn:FSNButton ID="btnBoton3" runat="server" Alineacion="left" Font-Size="11px" SkinID="SubmenuFiltros"
												ColorFondo="" ColorFondoHover="" ImagenesBordes="" Text="Pendientes revisar cierre()" />
										</td>
										<td style="white-space:nowrap;">
											<fsn:FSNButton ID="btnBoton4" runat="server" Alineacion="left" Font-Size="11px" SkinID="SubmenuFiltros"
												ColorFondo="" ColorFondoHover="" ImagenesBordes="" Text="Cierre eficaz dentro plazo()" />
										</td>
										<td style="white-space:nowrap;">
											<fsn:FSNButton ID="btnBoton5" runat="server" Alineacion="left" Font-Size="11px" SkinID="SubmenuFiltros"
												ColorFondo="" ColorFondoHover="" ImagenesBordes="" Text="Cierre eficaz fuera plazo()" />
										</td>
										<td style="white-space:nowrap;">
											<fsn:FSNButton ID="btnBoton6" runat="server" Alineacion="left" Font-Size="11px" SkinID="SubmenuFiltros"
												ColorFondo="" ColorFondoHover="" ImagenesBordes="" Text="Cierre no eficaz()" />
										</td>
										<td style="white-space:nowrap;">
											<fsn:FSNButton ID="btnBoton7" runat="server" Alineacion="left" Font-Size="11px" SkinID="SubmenuFiltros"
												ColorFondo="" ColorFondoHover="" ImagenesBordes="" Text="dAnuladas()" />
										</td>
										<td style="white-space:nowrap;">
											<fsn:FSNButton ID="btnBoton8" runat="server" Alineacion="left" Font-Size="11px" SkinID="SubmenuFiltros"
												ColorFondo="" ColorFondoHover="" ImagenesBordes="" Text="dTodas()" />
										</td>
									</tr>
								</table>
							</div>
						</asp:Panel>
					</td>
				</tr>
				<tr>
					<td>
						<asp:Panel ID="panSinPaginacion" runat="server" Visible="True" Width="100%">
							<div style="border: 1px solid #CCCCCC;">
								<table border="0" width="100%">
									<tr>
										<td align="right" width="15%">
											<table border="0">
												<tr>
													<td>
														<fsn:FSNButton ID="btnConfigurar" runat="server" Text="Configurar"></fsn:FSNButton>
													</td>
													<td>
														<asp:ImageButton ID="ibExcel" runat="server" SkinID="Excel" OnClientClick="MostrarExcel();return false;"
															AlternateText='Excel' />
													</td>
													<td>
														<asp:ImageButton ID="ibPDF" runat="server" SkinID="Pdf" OnClientClick="MostrarPdf();return false;"
															AlternateText='Pdf' />
													</td>
													<td>
														<asp:ImageButton ID="ibEnviarMail" runat="server" SkinID="EnviarMail" OnClientClick="return EnviarEmail();"
															AlternateText='' />
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</div>
						</asp:Panel>
					</td>
				</tr>
				<tr>
					<td>
						<div onmouseover="Posicion(event)">
							<input id="PaginaVolver" runat="server" type="hidden" />
                            <ig:WebHierarchicalDataGrid ID="whdgNoConformidades" runat="server" Width="100%" Height="500px" SkinID="Visor" Browser="Xml"
                                AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="true">
                                <ClientEvents Initialize="whdgNoConformidades_Initialize"/>
                                <GroupingSettings EnableColumnGrouping="True" GroupAreaVisibility="Visible"></GroupingSettings>
                                <Behaviors>
                                    <ig:Activation Enabled="true"/>
                                    <ig:Filtering Alignment="Top" Enabled="true" Visibility="Visible" AnimationEnabled="false">
                                        <FilteringClientEvents DataFiltering="whdgNoConformidades_Filtering" />
                                        <ColumnSettings>
						                    <ig:ColumnFilteringSetting ColumnKey="INDICADOR_EN_PROCESO" Enabled="false" />
                                            <ig:ColumnFilteringSetting ColumnKey="COMENT" Enabled="false" />
					                    </ColumnSettings>
                                    </ig:Filtering> 
                                    <ig:RowSelectors Enabled="true" EnableInheritance="true"></ig:RowSelectors>
						            <ig:Selection Enabled="true" RowSelectType="Single" CellSelectType="Single" ColumnSelectType="None"></ig:Selection>                						    						    
                                    <ig:Sorting Enabled="true" SortingMode="Multi" SortingClientEvents-ColumnSorting="whdgNoConformidades_Sorting"></ig:Sorting>
						            <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
						            <ig:ColumnMoving Enabled="true"></ig:ColumnMoving>
                                    <ig:Paging Enabled="true" PagerAppearance="Top">
							            <PagerTemplate>
								            <div class="CabeceraBotones" style="clear:both; float:left; width:100%;"> 
									            <div style="float:left; padding:3px 0px 2px 0px; margin-left:10px;">
										            <div style="clear: both; float: left; margin-right: 5px;">
                                                        <asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" style="margin-right:2px;" />
                                                        <asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" />
                                                    </div>
                                                    <div style="float: left; margin-right:5px;">
                                                        <asp:Label ID="lblPage" runat="server" Text="dPage" Style="margin-right: 3px;"></asp:Label>
                                                        <asp:DropDownList ID="PagerPageList" runat="server" Style="margin-right: 3px;" onchange="return IndexChanged()" />
                                                        <asp:Label ID="lblOF" runat="server" Text="dOf" Style="margin-right: 3px;"></asp:Label>
                                                        <asp:Label ID="lblCount" runat="server" />
                                                    </div>
                                                    <div style="float: left;">
                                                        <asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-right:2px;" />
                                                        <asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" />
                                                    </div>
									            </div>
								            </div>						
							            </PagerTemplate>
						            </ig:Paging>
                                </Behaviors>
                                <ClientEvents Click="grid_CellClick" />
							</ig:WebHierarchicalDataGrid>
						</div>
					</td>
				</tr>
			</table>
			<div id="div_Label" runat="server" class="EtiquetaScroll" style="display: none; position: absolute">
				<asp:Label ID="label2" runat="server" Font-Bold="true"></asp:Label>
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>
	<asp:ObjectDataSource ID="odsFiltrosConfigurados" runat="server" SelectMethod="LoadConfigurados"
		TypeName="Fullstep.FSNServer.FiltrosQA">
		<SelectParameters>
			<asp:Parameter Name="sPersona" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:UpdatePanel ID="updPanelHidden" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<asp:HiddenField ID="hid_Identificador" runat="server" />
			<asp:HiddenField ID="hid_IdNC" runat="server" />
			<asp:HiddenField ID="hid_CodRevisor" runat="server" />
			<asp:HiddenField ID="hid_CodPersonaRevisor" runat="server" />
			<asp:HiddenField ID="hid_Estado" runat="server" />
			<asp:HiddenField ID="hid_CodPeticionario" runat="server" />
			<asp:HiddenField ID="hid_CodProveedor" runat="server" />
			<asp:HiddenField ID="hid_Proveedor" runat="server" />
			<asp:HiddenField ID="hid_Cellkey" runat="server" />
			<asp:HiddenField ID="hid_Otz" runat="server" />
		</ContentTemplate>
	</asp:UpdatePanel>
	<ig:WebExcelExporter ID="wdgExcelExporter" runat="server"></ig:WebExcelExporter>
	<ig:WebDocumentExporter ID="wdgPDFExporter" runat="server" ExportMode="Download"></ig:WebDocumentExporter> 
	<asp:Panel ID="pnlProceso" runat="server" CssClass="modalPopup" Style="display: none;
		position: absolute; max-width: 400px; max-height: 300px">
		<table border="0" cellpadding="0" cellspacing="0" id="contenedor_imagenes" width="100%">
			<tr style="padding-bottom: 10px">
				<td colspan="2" style="padding-left: 20px">
					<table border="0" style="width: 100%">
						<tr>
							<td valign="bottom" style="width: 15%">
								<asp:Image ID="Image1" runat="server" SkinID="ErrorGrande" />
							</td>
							<td valign="middle" align="left">
								<asp:UpdatePanel ID="upTituloEnProceso" runat="server" UpdateMode="Conditional">
									<ContentTemplate>
										<asp:Panel ID="pnlTituloEnProceso" runat="server">
											<asp:Label runat="server" ID="lbltituloEnProceso" Text="En proceso..." CssClass="RotuloGrande"></asp:Label>
										</asp:Panel>
									</ContentTemplate>
								</asp:UpdatePanel>
							</td>
							<td valign="top" style="text-align: right">
								<asp:ImageButton ID="ImgBtnDetCerrar" runat="server" SkinID="Cerrar" ToolTip="Cerrar Detalle" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding-left: 20px">
					<asp:UpdatePanel ID="upTextoEnProceso" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:Panel ID="pnlEnProcesoIn" runat="server" Width="350px">
								<asp:Label ID="lblEnProceso" runat="server" Text="La no conformidad está siendo tramitada en estos momentos. En breves instantes estará disponible para su gestión."></asp:Label>
							</asp:Panel>
						</ContentTemplate>
					</asp:UpdatePanel>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="2" style="width: 100%; padding-left: 45%; padding-bottom: 10px">
					<fsn:FSNButton ID="btnAceptarPanel" runat="server" Text="Aceptar" Alineacion="Left"></fsn:FSNButton>
				</td>
			</tr>
		</table>
	</asp:Panel>
	<ajx:ModalPopupExtender ID="mpeProceso" CancelControlID="ImgBtnDetCerrar"
		Enabled="true" OkControlID="btnAceptarPanel"  PopupControlID="pnlProceso" 
		TargetControlID="Image1" RepositionMode="RepositionOnWindowResizeAndScroll" runat="server">
	</ajx:ModalPopupExtender>	  
	<asp:Panel ID="pnlComentarios" runat="server" CssClass="modalPopup" Style="display: none;
		position: absolute; max-width: 400px; max-height: 300px; z-index: 9999; margin-top: -200px;
		margin-left: -200px; background-color: #FAFAFA">
		<div style='width: 400px; z-index: 10000; position: absolute;'>
			<div style='width: 400px; height: 30px;' class='CapaTituloPanelInfo'>
				&nbsp;</div>
			<div style='width: 400px; height: 15px;' class='CapaSubTituloPanelInfo'>
				&nbsp;</div>
		</div>
		<table border="0" style="z-index: 10002; width: 401px; position: absolute; border-bottom: #8c8c8c 1px solid;
			border-left: #8c8c8c 1px solid; border-top: #8c8c8c 1px solid; border-right: #8c8c8c 1px solid;"
			cellpadding="0" cellspacing="0">
			<tr>
				<td valign="bottom" align="left" style="width: 270px; padding-left: 80px">
					<asp:UpdatePanel runat="server" ID="upTituloComentarios" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:Label runat="server" ID="Label1" Text="Comentarios" CssClass="TituloPopUpPanelInfo"></asp:Label>
						</ContentTemplate>
					</asp:UpdatePanel>
				</td>
				<td valign="top" style="text-align: right">
					<asp:Button ID="Button2" runat="server" Height="0" Text="Button" Visible="true" Width="0"
						Style="display: none" />
					<asp:ImageButton ID="ImgBtnDetCerrarComentarios" runat="server" SkinID="Cerrar" ToolTip="Cerrar Detalle" />
				</td>
			</tr>
			<tr>
				<td valign="bottom" align="left" style="width: 270px; padding-left: 80px">
					<asp:UpdatePanel runat="server" ID="upSubTituloComentarios" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:Label runat="server" ID="Label3" Text="Información detallada" CssClass="SubTituloPopUpPanelInfo"></asp:Label>
						</ContentTemplate>
					</asp:UpdatePanel>                    
				</td>
				<td style="width: 50px">
				</td>
			</tr>
			<tr style="background-color: #FAFAFA">
				<td colspan="2" style="height: 25px">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="2" style="background-color: White">
					<asp:UpdatePanel ID="upComentarios" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:Panel ID="upComentariosIN" runat="server" ScrollBars="Vertical">
								<table id="tblComentarios" runat="server" border="0" style="width: 382px; background-color: white;
									padding-left: 10px; padding-right: 10px" cellpadding="0" cellspacing="0">
								</table>
							</asp:Panel>
						</ContentTemplate>
					</asp:UpdatePanel>
				</td>
			</tr>
			<tr style="background-color: #FAFAFA">
				<td colspan="2" style="height: 10px">
					&nbsp;
				</td>
			</tr>
		</table>
		<table style="background: transparent; position: absolute; z-index: 10003; width: 100%">
			<tr>
				<td valign="top" align="left" style="padding: 0px; width: 80px; height: 60px">
					<asp:Image ID="imgComentarios" runat="server" SkinID="PopUpProveedor" Visible="true" />
				</td>
			</tr>
		</table>
	</asp:Panel>     
	<ajx:ModalPopupExtender ID="mpeComentarios" CancelControlID="ImgBtnDetCerrarComentarios"
		Enabled="true" OkControlID="Button2"  PopupControlID="pnlComentarios" 
		TargetControlID="Button2" RepositionMode="RepositionOnWindowResizeAndScroll" runat="server">
	</ajx:ModalPopupExtender>			 
	<asp:Panel ID="pnlProveedor_Peticionario" runat="server" Style="display: none; position: absolute;
		max-width: 400px; max-height: 300px">
		<div style='width: 400px; z-index: 10000; position: absolute;'>
			<div style='width: 400px; height: 35px;' class='CapaTituloPanelInfo'>
				&nbsp;</div>
			<div style='width: 400px; height: 20px;' class='CapaSubTituloPanelInfo'>
				&nbsp;</div>
		</div>
		<table border="0" style="z-index: 10002; width: 400px; position: absolute; border-bottom: #8c8c8c 1px solid;
			border-left: #8c8c8c 1px solid; border-top: #8c8c8c 1px solid; border-right: #8c8c8c 1px solid;"
			cellpadding="2" cellspacing="0">
			<tr>
				<td valign="bottom" align="left" style="width: 270px; padding-left: 80px">
					<asp:UpdatePanel ID="upTitulo" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:Label runat="server" ID="lblTituloPanelInfo" Text="Proveedor" CssClass="TituloPopUpPanelInfo"></asp:Label>
						</ContentTemplate>
					</asp:UpdatePanel>
				</td>
				<td valign="top" style="text-align: right">
					<asp:Button ID="Button3" runat="server" Height="0" Text="Button" Visible="true" Width="0"
						Style="display: none" />
					<asp:ImageButton ID="ImgBtnDetCerrarProveedor_Peticionario" runat="server" SkinID="Cerrar"
						ToolTip="Cerrar Detalle" />
				</td>
			</tr>
			<tr>
				<td valign="bottom" align="left" style="width: 270px; padding-left: 80px">
					<asp:UpdatePanel ID="upSubTitulo" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:Label runat="server" ID="lblSubTituloPanelInfo" Text="Información detallada"
								CssClass="SubTituloPopUpPanelInfo"></asp:Label>
						</ContentTemplate>
					</asp:UpdatePanel>
				</td>
				<td style="width: 50px">
				</td>
			</tr>
			<tr style="background-color: #F5F5F5">
				<td colspan="2">
					<asp:UpdatePanel ID="upDetalleProveedor_Peticionario" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:Panel ID="pnlDetalleProveedor_Peticionario" runat="server">
								<asp:Label ID="lblInfoPanel" runat="server" CssClass="Normal"></asp:Label>
							</asp:Panel>
						</ContentTemplate>
					</asp:UpdatePanel>
				</td>
			</tr>
		</table>
		<table style="background: transparent; position: absolute; z-index: 10003">
			<tr>
				<td valign="top" align="left" style="padding: 0px; width: 80px; height: 60px">
					<asp:Image ID="imgPanelInfoPeticionario" runat="server" SkinID="PopUpProveedor" Visible="false" />
					<asp:Image ID="imgPanelInfoProveedor" runat="server" SkinID="PopUpProveedor" Visible="true" />
				</td>
			</tr>
		</table>
	</asp:Panel>	
	<ajx:ModalPopupExtender ID="mpeProveedor_Peticionario" CancelControlID="ImgBtnDetCerrarProveedor_Peticionario"
		Enabled="true" OkControlID="Button3"  PopupControlID="pnlProveedor_Peticionario" 
		TargetControlID="Button3" RepositionMode="RepositionOnWindowResizeAndScroll" runat="server">
	</ajx:ModalPopupExtender>
	<fsn:FSNPanelInfo ID="FSNPanelDatosProveedorQA" runat="server" 
		ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_DatosProveedorQA" 
		TipoDetalle="1" />
	<fsn:FSNPanelInfo ID="FSNPanelDatosPersonaQA" runat="server" 
		ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_DatosPersona" 
		TipoDetalle="2"/>
    <fsn:FSNPanelInfo ID="FSNPanelDatosRevisorQA" runat="server" 
		ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_DatosPersona" 
		TipoDetalle="3"/>
    <fsn:FSNPanelInfo ID="FSNPanelComentarioQA" runat="server" 
        ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_ComentarioQA" 
        TipoDetalle="5" Height="200px"/>
	<fsn:FSNPanelInfo ID="FSNPanelEnProceso" runat="server" 
		ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_TextoEnProceso" 
		TipoDetalle="4" 
		ImagenPopUp="~/App_Themes/<%=Me.Page.Theme%>/images/Icono_Error_Amarillo_40x40.gif" 
		Height="120px" Width="370px"/>	  
	<script type="text/javascript">
	    $(document).ready(function () {
	        $('[id$=ImgBtnFirst]').live('click', function () { FirstPage(); });
	        $('[id$=ImgBtnPrev]').live('click', function () { PrevPage(); });
	        $('[id$=ImgBtnNext]').live('click', function () { NextPage(); });
	        $('[id$=ImgBtnLast]').live('click', function () { LastPage(); });
	    });        
	</script>
    <asp:HiddenField runat="server" ID="bActivadoFSAL" ClientIDMode="Static" Value="0" />
    <asp:HiddenField runat="server" ID="bEnviadoFSAL8Inicial" ClientIDMode="Static" Value="0" />
    <asp:HiddenField runat="server" ID="sIdRegistroFSAL1" ClientIDMode="Static" />
</asp:Content>