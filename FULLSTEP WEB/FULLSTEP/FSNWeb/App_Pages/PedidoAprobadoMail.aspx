﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PedidoAprobadoMail.aspx.vb" Inherits="Fullstep.FSNWeb.PedidoAprobadoMail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    <style type="text/css">
        html, body, form{ height: 100%; width:100%; margin:0px }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divContenedor" style="width:100%; height:100%;">
        <div id="divCabecera" class="CabeceraPedAprobado">
            <a href="http://www.fullstep.com" onfocus="blur()">
                <asp:Image ID="imgLogo" runat="server" ImageAlign="Middle" SkinID="LogoCab" />
				<br>
				<img src="http://demo.fullstep.net/templates/img/fondo_rayas.gif" width="50%" height="8" alt="" border="0"/>
            </a>
        </div>
        <div id="divSeparador" class="SeparadorPedAprobado">
            
        </div>
        <div id="divTexto" class="TextoPedAprobado">
            <asp:Label ID="lblTexto" CssClass="TextoAprobacion" runat="server" Text="Texto de prueba"></asp:Label>
        </div>
        <div id="divEnlaceCerrar" class="CerrarPedAprobado">
            <a id="linkCerrar" class="EnlaceCerrar" href="#" runat="server" style="cursor:pointer"  onclick="javascript:window.top.close(); return false;">DCerrar</a>
        </div>
    </div>
    </form>
</body>
</html>
