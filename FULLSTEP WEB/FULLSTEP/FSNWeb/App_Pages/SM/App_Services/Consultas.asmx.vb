﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web
Imports System.Text
Imports Fullstep
Imports Fullstep.FSNLibrary

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<System.Web.Script.Services.ScriptService()> _
<ToolboxItem(False)> _
Public Class SMConsultas
    Inherits System.Web.Services.WebService

    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle de una persona
    ''' </summary>
    ''' <param name="contextKey">Código del usuario</param>
    ''' <returns>Un String con los datos de la persona en formato html</returns>
    <System.Web.Services.WebMethod(True)> _
    Public Function Obtener_DatosUsuario(ByVal contextKey As String) As String
        Dim oRoot As Fullstep.FSNServer.Root = HttpContext.Current.Session("FS_SM_Server")
        Dim oUsr As Fullstep.FSNServer.User = oRoot.Get_Object(GetType(Fullstep.FSNServer.User))
        Dim dtPersona As DataTable = oUsr.DevolverPersona(contextKey)

        Dim sCod As String = String.Empty
        Dim sNombre As String = String.Empty
        Dim sApe As String = String.Empty
        Dim sCargo As String = String.Empty
        Dim sTelf As String = String.Empty
        Dim sFax As String = String.Empty
        Dim sEMail As String = String.Empty
        Dim sDep As String = String.Empty
        Dim sOrg As String = String.Empty
        If Not dtPersona Is Nothing AndAlso dtPersona.Rows.Count > 0 Then
            Dim drPer As DataRow = dtPersona.Rows(0)

            sCod = DBNullToStr(drPer("COD"))
            sNombre = DBNullToStr(drPer("NOM"))
            sApe = DBNullToStr(drPer("APE"))
            sCargo = DBNullToStr(drPer("CAR"))
            sTelf = DBNullToStr(drPer("TFNO"))
            sFax = DBNullToStr(drPer("FAX"))
            sEMail = DBNullToStr(drPer("EMAIL"))
            sDep = DBNullToStr(drPer("DEPDEN"))
            sOrg = DBNullToStr(drPer("ORG"))
        End If

        Dim oFSUsuario As FSNServer.User = Session("FS_SM_User")
        Dim oFSSMServer As Fullstep.FSNServer.Root = Session("FS_SM_Server")
        Dim oDict As FSNServer.Dictionary = oFSSMServer.Get_Object(GetType(Fullstep.FSNServer.Dictionary))
        oDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.HistoricoPartidasPresupuestarias, oFSUsuario.Idioma)

        Dim sr As New StringBuilder()
        sr.Append("<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(14).Item(1) & ":</b> " & sCod & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(15).Item(1) & ":</b> " & sNombre & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(16).Item(1) & ":</b> " & sApe & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(17).Item(1) & ":</b> " & sCargo & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(18).Item(1) & ":</b> " & sTelf & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(19).Item(1) & ":</b> " & sFax & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(20).Item(1) & ":</b> " & sEMail & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(21).Item(1) & ":</b> " & sDep & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(22).Item(1) & ":</b> " & sOrg & "<br/>")

        Return sr.ToString
    End Function

End Class