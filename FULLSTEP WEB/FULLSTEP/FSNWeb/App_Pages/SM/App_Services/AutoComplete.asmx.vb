﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports Fullstep
Imports Fullstep.FSNLibrary

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class SMAutoComplete
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(True)> _
    Public Function GetPartidasPresupuestarias(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Dim FSSMServer As FSNServer.Root = HttpContext.Current.Session("FS_SM_Server")
        Dim FSSMUser As FSNServer.User = Session("FS_SM_User")
        Dim cPres5 As FSNServer.PartidasPRES5 = FSSMServer.Get_Object(GetType(FSNServer.PartidasPRES5))
        Dim MiDataSet As New DataTable
        Dim arrayPalabras As String()
        Dim sbusqueda

        Dim UON() As String = Split(Split(contextKey, "@@")(0), "#")
        MiDataSet = cPres5.GetPartidasPresupuestarias(UON(0), UON(1), UON(2), UON(3), _
                                                      FSSMUser.Cod, FSSMUser.Idioma, _
                                                      Split(contextKey, "@@")(1))
        arrayPalabras = Split(prefixText)

        For i = 1 To arrayPalabras.Length - 1 'desde la segunda palabra
            arrayPalabras(i) = " " & arrayPalabras(i)
        Next

        sbusqueda = From Datos In MiDataSet _
                         Let w = UCase(DBNullToStr("COD")) _
                         Where Datos.Field(Of String)("COD").ToUpper Like "" & strToSQLLIKE(prefixText, True) & "*" Or Datos.Field(Of String)("DEN").ToUpper Like "" & strToSQLLIKE(prefixText, True) & "*" _
                         Select Datos.Item("COD") & "-" & Datos.Item("DEN") Distinct.Take(count).ToArray()
        Dim resul() As String
        ReDim resul(UBound(sbusqueda))
        For i As Integer = 0 To UBound(sbusqueda)
            resul(i) = sbusqueda(i).ToString
        Next
        Return resul
    End Function

End Class

