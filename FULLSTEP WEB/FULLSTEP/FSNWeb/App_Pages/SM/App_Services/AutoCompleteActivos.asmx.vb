﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class AutoCompleteActivos
    Inherits System.Web.Services.WebService

    'The AutoCompleteExtender does not work within a UserControl at this time. Quoting Dino Esposito in MSDN Magazine Feb 2007: -
    'A callable page method is a public static (or Shared in Visual Basic.NET) method defined in the codebehind class and decorated 
    'with the same WebMethod attribute used for Web service methods. At present, this is limited to ASPX pages-both inline and 
    'codebehind code-but might be extended in the future to user controls and custom controls.
    '[...]
    '...implementing the method as a WebService or moving the functionality back up to the page

    ''' <summary>
    ''' Devuelve la lista para autocompletar el texto introducido en las búsquedas por código de activo.
    ''' </summary>
    ''' <param name="prefixText">String con el texto actual</param>
    ''' <param name="count">Integer con el número de elementos que hay que retornar</param>
    ''' <param name="contextKey">String con el contexto específico del usuario o de la página.</param>
    ''' <returns>Un array de String con la lista de elementos</returns>
    ''' <remarks>Método vinculado a un control AutoCompleteExtender: VisorActivos.ascx</remarks>
    <System.Web.Services.WebMethod(True)> _
    Public Function Autocompletar_Codigo(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Return Autocompletar("COD", prefixText, count, contextKey)
    End Function

    ''' <summary>
    ''' Devuelve la lista para autocompletar el texto introducido en las búsquedas por denominación de activo.
    ''' </summary>
    ''' <param name="prefixText">String con el texto actual</param>
    ''' <param name="count">Integer con el número de elementos que hay que retornar</param>
    ''' <param name="contextKey">String con el contexto específico del usuario o de la página.</param>
    ''' <returns>Un array de String con la lista de elementos</returns>
    ''' <remarks>Método vinculado a un control AutoCompleteExtender: VisorActivos.ascx</remarks>
    <System.Web.Services.WebMethod(True)> _
    Public Function Autocompletar_Denominacion(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Return Autocompletar("DEN_ACTIVO", prefixText, count, contextKey)
    End Function

    ''' <summary>
    ''' Devuelve la lista para autocompletar el texto introducido en las búsquedas por número de ERP de activo.
    ''' </summary>
    ''' <param name="prefixText">String con el texto actual</param>
    ''' <param name="count">Integer con el número de elementos que hay que retornar</param>
    ''' <param name="contextKey">String con el contexto específico del usuario o de la página.</param>
    ''' <returns>Un array de String con la lista de elementos</returns>
    ''' <remarks>Método vinculado a un control AutoCompleteExtender: VisorActivos.ascx</remarks>
    <System.Web.Services.WebMethod(True)> _
    Public Function Autocompletar_NumeroERP(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Return Autocompletar("COD_ERP", prefixText, count, contextKey)
    End Function

    ''' <summary>
    ''' Devuelve la lista para autocompletar el texto introducido en las búsquedas por centro de activo.
    ''' </summary>
    ''' <param name="prefixText">String con el texto actual</param>
    ''' <param name="count">Integer con el número de elementos que hay que retornar</param>
    ''' <param name="contextKey">String con el contexto específico del usuario o de la página.</param>
    ''' <returns>Un array de String con la lista de elementos</returns>
    ''' <remarks>Método vinculado a un control AutoCompleteExtender: VisorActivos.ascx</remarks>
    <System.Web.Services.WebMethod(True)> _
    Public Function Autocompletar_Centro(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        'Return Autocompletar("ID_DEN_CENTRO", prefixText, count, contextKey)
        Dim targetField As String = "ID_DEN_CENTRO"
        Dim result As New List(Of String)
        If HttpContext.Current.Cache("Activos_" & contextKey) Is Nothing Then
            Return Nothing
        Else
            Dim dtActivos As DataTable = CType(HttpContext.Current.Cache("Activos_" & contextKey), DataTable)
            Dim pattern As String() = Split(prefixText)
            Dim matches As IEnumerable(Of String) = (From Datos In dtActivos _
                        Where pattern.All(Function(palabra) UCase(DBNullToSomething(Datos(targetField)).ToString()) Like String.Concat("*", strToSQLLIKE(palabra, True), "*")) _
                        Select Datos(targetField)).Cast(Of String).Distinct.Take(10)

            For Each match As String In matches
                result.Add(match)
            Next
            Return result.ToArray
        End If
    End Function

    ''' <summary>
    ''' Devuelve los elementos que cumplen con los criterios de autocompletar para el campo del origen de datos seleccionado.
    ''' </summary>
    ''' <param name="targetField">String con nombre del campo del origen de datos del GridView</param>
    ''' <param name="prefixText">String con el texto actual</param>
    ''' <param name="count">Integer con el número de elementos que hay que retornar</param>
    ''' <param name="contextKey">String con el contexto específico del usuario o de la página.</param>
    ''' <returns>Un array de String con la lista de elementos</returns>
    ''' <remarks>Esta función se invoca desde todos los métodos de este servicio web</remarks>
    Private Function Autocompletar(ByVal targetField As String, ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Dim result As New List(Of String)
        If HttpContext.Current.Cache("Activos_" & contextKey) Is Nothing Then
            Return Nothing
        Else
            Dim dtActivos As DataTable = CType(HttpContext.Current.Cache("Activos_" & contextKey), DataTable)
            Dim pattern As String() = Split(prefixText)
            Dim matches As IEnumerable(Of DataRow) = From Datos In dtActivos _
                        Where pattern.All(Function(palabra) UCase(DBNullToSomething(Datos(targetField)).ToString()) Like String.Concat("*", strToSQLLIKE(palabra, True), "*")) _
                        Select Datos Distinct.Take(10)

            For Each match As DataRow In matches
                result.Add(match(targetField).ToString())
            Next
            Return result.ToArray
        End If
    End Function
End Class