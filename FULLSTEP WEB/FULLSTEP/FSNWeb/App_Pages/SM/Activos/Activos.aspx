﻿<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/App_Master/Menu.master"
    CodeBehind="Activos.aspx.vb" Inherits="Fullstep.FSNWeb.Activos" Title="" %>

<%@ Register TagPrefix="fsn" TagName="VisorActivos" Src="VisorActivos.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    <table cellspacing="1" cellpadding="3" width="100%" border="0">
        <tr>
            <td valign="middle" colspan="2">
                <table cellpadding="4" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td width="65">
                            <asp:Image ID="imgMantenimiento" runat="server" ImageUrl="~/images/dolar.JPG" Style="margin-bottom: 0px" />
                        </td>
                        <td>
                            <asp:Label ID="lblTitulo" runat="server" CssClass="RotuloGrande" Text="DActivos"> 
                            </asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <fsn:VisorActivos runat="server" ID="ucVisorActivos" />
</asp:Content>
