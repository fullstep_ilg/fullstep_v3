﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuscadorActivos.aspx.vb"
    Inherits="Fullstep.FSNWeb.BuscadorActivos" %>

<%@ Register TagPrefix="fsn" TagName="VisorActivos" Src="VisorActivos.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
</head>
<script type="text/javascript">

function Seleccionar()
{    
    var sCOD;
    var hfCOD=document.getElementById('<%= ucVisorActivos.ActivoCOD.ClientID %>');
    if(hfCOD!=null)
        sCOD=hfCOD.value; 
     
    var sDEN;
    var hfDEN=document.getElementById('<%= ucVisorActivos.ActivoDEN.ClientID %>');
    if(hfDEN!=null)
        sDEN = hfDEN.value;

    var sCCCOD
    var hfCCCOD = document.getElementById('<%= ucVisorActivos.ActivoCCCOD.ClientID %>');
    if (hfCCCOD != null)
        sCCCOD = hfCCCOD.value;
        
    var sUON
    var hfUON=document.getElementById('<%= ucVisorActivos.ActivoUON.ClientID %>');
    if (hfUON != null)
        sUON = hfUON.value;    

    window.opener.Activo_seleccionado(document.Form1.IDCONTROL.value, sCOD, sCOD + ' - ' + sDEN, document.Form1.IDCentroCoste.value, sCCCOD,sUON);
    window.close();
    
}

</script>
<body style="background-color: #FFFFFF;">
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <input runat="server" id="IDCONTROL" type="hidden" name="IDCONTROL" />
        <input runat="server" id="IDCentroCoste" type="hidden" name="IDCentroCoste" />
        <table cellspacing="1" cellpadding="3" width="100%" border="0">
            <tr>
                <td valign="middle" colspan="2">
                    <table cellpadding="4" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td width="65">
                                <asp:Image ID="imgDetalle" runat="server" ImageUrl="~/images/dolars_view.JPG" Style="margin-bottom: 0px"/>
                            </td>
                            <td>
                                <asp:Label ID="lblTitulo" runat="server" CssClass="RotuloGrande" Text="DActivos"> 
                                </asp:Label>
                            </td>
                            <td align="right" valign="top">
                                <asp:ImageButton ID="imgCerrar" runat="server" SkinID="Cerrar" OnClientClick="javascript:self.close()" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <fsn:VisorActivos runat="server" ID="ucVisorActivos" AllowSelection="true" />
                </td>
            </tr>
            <tr>
                <td align="right" width="50%">
                    <fsn:FSNButton ID="btnSeleccionar" runat="server" Text="DSeleccionar" Alineacion="Right"
                        OnClientClick="Seleccionar()"></fsn:FSNButton>
                </td>
                <td align="left" width="50%">
                    <fsn:FSNButton ID="btnCancelar" runat="server" Text="DCancelar" Alineacion="Left"
                        OnClientClick="window.close()"></fsn:FSNButton>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
