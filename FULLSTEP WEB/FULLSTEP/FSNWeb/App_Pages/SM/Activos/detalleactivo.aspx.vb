﻿Public Partial Class detalleactivo
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Activos

        Me.lblTitulo.Text = Textos(16)
        Me.lblCodigo.Text = Textos(1) & ":"
        Me.lblDenominacion.Text = Textos(2) & ":"
        Me.lblCentro.Text = Textos(10) & ":"
        Me.lblEmpresa.Text = Textos(4) & ":"
        Me.lblNumeroERP.Text = Textos(3) & ":"


        If Not String.IsNullOrEmpty(Request("codActivo")) Then


            Dim oActivos As FSNServer.Activos = FSNServer.Get_Object(GetType(FSNServer.Activos))
            Dim bHayIntegracionSalida As Boolean = oActivos.HayIntegracionSentidoSalida()
            lblNumeroERP.Visible = bHayIntegracionSalida
            lblNumeroERPBD.Visible = lblNumeroERP.Visible

            Dim oActivo As FSNServer.Activo = FSNServer.Get_Object(GetType(FSNServer.Activo))
            Dim ds As DataSet = oActivo.DetalleActivo(Idioma, Request("codActivo"))
            If ds.Tables(0).Rows.Count > 0 Then
                lblCodigoBD.Text = ds.Tables(0).Rows(0).Item("COD")
                lblDenominacionBD.Text = DBNullToSomething(ds.Tables(0).Rows(0).Item("DEN"))
                lblCentroBD.Text = DBNullToSomething(ds.Tables(0).Rows(0).Item("CENTRO_SM"))
                If Not String.IsNullOrEmpty(DBNullToSomething(ds.Tables(0).Rows(0).Item("DEN_CENTRO"))) Then
                    lblCentroBD.Text = lblCentroBD.Text & " - " & ds.Tables(0).Rows(0).Item("DEN_CENTRO")
                End If
                lblEmpresaBD.Text = DBNullToSomething(ds.Tables(0).Rows(0).Item("DEN_EMP"))
                lblNumeroERPBD.Text = DBNullToSomething(ds.Tables(0).Rows(0).Item("COD_ERP"))
            End If

        End If

    End Sub

End Class