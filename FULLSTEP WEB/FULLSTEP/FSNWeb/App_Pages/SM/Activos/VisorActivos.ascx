﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VisorActivos.ascx.vb"
    Inherits="Fullstep.FSNWeb.VisorActivos" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<script type="text/javascript">

function AsignarValores(sCOD,sDEN,sCCCOD,sUON){
                        
    var hfCOD=document.getElementById("ucVisorActivos_hfCOD");   
    if(hfCOD!=null)
        hfCOD.value=sCOD;        
        
    var hfDEN=document.getElementById("ucVisorActivos_hfDEN");       
    if(hfDEN!=null)
        hfDEN.value=sDEN;
        
    var hfCCCOD=document.getElementById("ucVisorActivos_hfCCCOD");       
    if(hfCCCOD!=null)
        hfCCCOD.value=sCCCOD;
        
    var hfUON=document.getElementById("ucVisorActivos_hfUON");  
    if(hfUON!=null)
        hfUON.value=sUON;
                
}

function show_buscadorCentroCoste(){

    var centro="";
    
//todo 1515 de momento no pasar nada. El control de busqueda de centros espera las uons concatenedas
//    var txtCentro=document.getElementById("ucVisorActivos_txtCentro");
//    if(txtCentro!=null)
//    {    
//        centro=txtCentro.value;
//    }
        
    window.open("../../PM/CentrosCoste/centrosCoste.aspx?Valor=" + escape(centro) + "&VerUON=0","_blank","width=750,height=475,status=yes,resizable=no,top=200,left=200");
}

    //se le llama desde el buscador de centros de coste
function CentroCoste_seleccionado(IDControl, sUON1, sUON2, sUON3, sUON4, sDen) {
    var sValor = '';

    //lo que se guarda en valor_text
    if (sUON1)
        sValor = sUON1;
    if (sUON2)
        sValor = sValor + '#' + sUON2;
    if (sUON3)
        sValor = sValor + '#' + sUON3;
    if (sUON4)
        sValor = sValor + '#' + sUON4;

    document.getElementById("<%=txtCentro.ClientID%>").value = sDen
    
}

</script>

<style type="text/css">
    .style1
    {
        width: 315px;
    }
    .style2
    {
        width: 153px;
    }
    .style3
    {
        width: 237px;
    }
    .style4
    {
        width: 147px;
    }
    .style5
    {
        width: 2px;
    }
</style>
<table width="100%" cellpadding="0" border="0">
    <tr>
        <td valign="middle" align="left">
            <asp:Panel runat="server" ID="pnlBusquedaAvanzada" Style="cursor: pointer" Font-Bold="True"
                Height="25px">
                <asp:Image ID="imgExpandir" runat="server" />
                <asp:Label ID="lblBusquedaAvanzada" runat="server" Text="DBúsqueda Avanzada" class="Rotulo">
                </asp:Label>
            </asp:Panel>
        </td>
    </tr>
</table>
<asp:Panel ID="pnlParametros" runat="server" CssClass="Rectangulo">
    <table width="100%">
        <tr>
            <td class="style2">
                <asp:Label ID="lblCodigo" runat="server" Text="DCódigo:" CssClass="Etiqueta"></asp:Label>
            </td>
            <td class="style1">
                <asp:TextBox ID="txtCodigo" runat="server" Width="235px"></asp:TextBox>
                <cc1:AutoCompleteExtender ID="AutoCompleteExtender_Codigo" runat="server" TargetControlID="txtCodigo"
                    CompletionInterval="500" CompletionSetCount="10" ServicePath="../App_Services/AutoCompleteActivos.asmx"
                    ServiceMethod="Autocompletar_Codigo" DelimiterCharacters="" MinimumPrefixLength="3"
                    EnableCaching="false" UseContextKey="true">
                </cc1:AutoCompleteExtender>
            </td>
            <td class="style4">
                <asp:Label ID="lblDenominacion" runat="server" Text="DDenominación:" CssClass="Etiqueta"></asp:Label>
            </td>
            <td class="style3">
                <asp:TextBox ID="txtDenominacion" runat="server" Width="235px"></asp:TextBox>
                <cc1:AutoCompleteExtender ID="AutoCompleteExtender_Denominacion" runat="server" TargetControlID="txtDenominacion"
                    CompletionInterval="500" CompletionSetCount="10" ServicePath="../App_Services/AutoCompleteActivos.asmx"
                    ServiceMethod="Autocompletar_Denominacion" DelimiterCharacters="" MinimumPrefixLength="3"
                    EnableCaching="false" UseContextKey="true">
                </cc1:AutoCompleteExtender>
            </td>
            <td class="style5">
            </td>
            <td rowspan="2" align="right" valign="middle">
                <fsn:FSNButton ID="btnBuscar" runat="server" Text="DBuscar" Style="margin-left: 0px"
                    OnClientClick="TempCargando2()" ></fsn:FSNButton>
            </td>
        </tr>
        <tr>
            <td class="style2">
                <asp:Label ID="lblEmpresa" runat="server" Text="DEmpresa" CssClass="Etiqueta"></asp:Label>
            </td>
            <td class="style4">
                <asp:DropDownList ID="ddlEmpresa" runat="server" Width="239px">
                </asp:DropDownList>
                <asp:Label ID="lblEmpresaPredef" runat="server" Visible="false">
                </asp:Label>
            </td>
            <td class="style4">
                <asp:Label ID="lblCentro" runat="server" Text="DCentro:" CssClass="Etiqueta"></asp:Label>
            </td>
            <td class="style3">
                <asp:TextBox ID="txtCentro" runat="server" Width="235px"></asp:TextBox>
                <cc1:AutoCompleteExtender ID="AutoCompleteExtender_Centro" runat="server" TargetControlID="txtCentro"
                    CompletionInterval="500" CompletionSetCount="10" ServicePath="../App_Services/AutoCompleteActivos.asmx"
                    ServiceMethod="Autocompletar_Centro" DelimiterCharacters="" MinimumPrefixLength="3"
                    EnableCaching="false" UseContextKey="true">
                </cc1:AutoCompleteExtender>
                <asp:Label ID="lblCentroPredef" runat="server" Visible="false">
                </asp:Label>
            </td>
            <td class="style5">
                <asp:ImageButton ID="imgbtnBuscarCentro" runat="server" ImageUrl="~/images/buscar.gif"
                    OnClientClick="show_buscadorCentroCoste();"></asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td class="style2">
                <asp:Label ID="lblNumeroERP" runat="server" Text="DNúmero ERP:" CssClass="Etiqueta"></asp:Label>
            </td>
            <td class="style1">
                <asp:TextBox ID="txtNumeroERP" runat="server" Width="235px"></asp:TextBox>
                <cc1:AutoCompleteExtender ID="AutoCompleteExtender_NumeroERP" runat="server" TargetControlID="txtNumeroERP"
                    CompletionInterval="500" CompletionSetCount="10" ServicePath="../App_Services/AutoCompleteActivos.asmx"
                    ServiceMethod="Autocompletar_NumeroERP" DelimiterCharacters="" MinimumPrefixLength="3"
                    EnableCaching="false" UseContextKey="true">
                </cc1:AutoCompleteExtender>
            </td>
        </tr>
    </table>
</asp:Panel>
<br />
<ajx:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" CollapseControlID="lblBusquedaAvanzada"
    ExpandControlID="lblBusquedaAvanzada" ImageControlID="imgExpandir" SuppressPostBack="true"
    TargetControlID="pnlParametros">
</ajx:CollapsiblePanelExtender>
<asp:UpdatePanel ID="upnlActivos" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
    <ContentTemplate>
        <div id="ContenedorpnlActivos" style="height:359px; overflow-y:scroll;">
        <asp:Panel ID="pnlActivos" runat="server">
            <asp:GridView ID="gridActivos" runat="server" AutoGenerateColumns="False" CellPadding="4"
                AllowPaging="true" Width="100%" BackColor="#CCCCCC">
                <PagerSettings Position="Top" />
                <HeaderStyle CssClass="PanelCabecera" />
                <EmptyDataTemplate>
                    <asp:Label runat="server" CssClass="EtiquetaGrande" ID="lblEmptyDataText" Text="DNo se han encontrado datos"></asp:Label>
                </EmptyDataTemplate>
                <SelectedRowStyle ForeColor="White" BackColor="#3366cc" />
                <PagerStyle CssClass="Rectangulo" />
                <PagerTemplate>
                    <asp:Panel ID="pnlPaginador" runat="server" Height="29px" Width="100%">
                        <table border="0" width="100%" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:ImageButton ID="ImgBtnFirst" runat="server" CommandArgument="first" CommandName="page"
                                        OnPreRender="ImgBtnFirst_PreRender" />&nbsp;&nbsp;
                                    <asp:ImageButton ID="ImgBtnPrev" runat="server" CommandArgument="prev" CommandName="page"
                                        OnPreRender="ImgBtnPrev_PreRender" />
                                    &nbsp;
                                    <asp:Label ID="LblPagina" runat="server" Font-Size="Small" ForeColor="#535353" OnPreRender="LblPagina_PreRender"
                                        Text="DPágina" Width="42px"></asp:Label>
                                    &nbsp;
                                    <asp:DropDownList ID="CmbBoxNumPags" runat="server" AutoPostBack="True" OnPreRender="CmbBoxNumPags_PreRender"
                                        OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" Width="48px">
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:Label ID="LblDe" runat="server" Font-Size="Small" ForeColor="#535353" OnPreRender="LblDe_PreRender"
                                        Text="DDe"></asp:Label>
                                    &nbsp;
                                    <asp:Label ID="LblTotal" runat="server" Font-Size="Small" ForeColor="#535353" OnPreRender="LblTotal_PreRender"></asp:Label>
                                    &nbsp;
                                    <asp:ImageButton ID="ImgBtnNext" runat="server" CommandArgument="next" CommandName="page"
                                        OnPreRender="ImgBtnNext_PreRender" />
                                    &nbsp;
                                    <asp:ImageButton ID="ImgBtnLast" runat="server" CommandArgument="last" CommandName="page"
                                        OnPreRender="ImgBtnLast_PreRender" />
                                    &nbsp;&nbsp;
                                </td>
                                <td nowrap>
                                    <asp:Panel ID="pnlOrdenacion" runat="server" Visible="true" Width="100%">
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="right">
                                                    <table border="0">
                                                        <tr>
                                                            <td nowrap>
                                                                <asp:Label ID="lblOrdenarPor" runat="server" CssClass="Etiqueta" Text="DOrdenar Por:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlOrdenarPor" runat="server" AutoPostBack="True" Width="150px"
                                                                    OnSelectedIndexChanged="ddlOrdenarPor_SelectedIndexChanged" OnPreRender="CargarListaOrdenacion">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="ibOrdenacion" runat="server" OnClick="ibOrdenacion_Click" OnPreRender="SetOrderByImage" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </PagerTemplate>
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="" SortExpression="ID" ReadOnly="True"
                        ItemStyle-Width="10%" Visible="false">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="COD" HeaderText="DCodigo" SortExpression="COD" ReadOnly="True"
                        ItemStyle-Width="10%">
                        <ItemStyle HorizontalAlign="Left" Wrap="false" />
                    </asp:BoundField>
                    <%--MUY IMPORTANTE NO CAMBIAR SI ES POSIBLE, EL SORTEXPRESSION DE ESTOS CAMPOS--%>
                    <%--SI SE CAMBIAN, MODIFICAR TAMBIEN EN EL gridActivos_RowDataBound--%>
                    <asp:BoundField DataField="DEN_ACTIVO" HeaderText="DDenominacion" SortExpression="DEN_ACTIVO"
                        ReadOnly="True" ItemStyle-Width="25%">
                        <ItemStyle HorizontalAlign="Left" Wrap="false" />
                    </asp:BoundField>
                    <asp:BoundField DataField="COD_ERP" HeaderText="DNumeroERP" SortExpression="COD_ERP"
                        ReadOnly="True" ItemStyle-Width="20%">
                        <ItemStyle HorizontalAlign="Left" Wrap="false" />
                    </asp:BoundField>
                    <asp:BoundField DataField="DEN_EMPRESA" HeaderText="DEmpresa" SortExpression="DEN_EMPRESA"
                        ReadOnly="True" ItemStyle-Width="20%">
                        <ItemStyle HorizontalAlign="Left" Wrap="false" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ID_DEN_CENTRO" HeaderText="DCentro" SortExpression="CENTRO_SM"
                        ReadOnly="True" ItemStyle-Width="25%">
                        <ItemStyle HorizontalAlign="Left" Wrap="false" />
                    </asp:BoundField>
                    <asp:BoundField DataField="UONS" HeaderText="" SortExpression="COD" ReadOnly="True"
                        ItemStyle-Width="10%" Visible="false">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
            <br />
        </asp:Panel>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="imgbtnBuscarCentro" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
<asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress" Style="display: none">
    <div style="position: relative; top: 30%; text-align: center;">
        <asp:Image ID="ImgProgress" runat="server" SkinID="ImgProgress" />
        <asp:Label ID="LblProcesando" runat="server" ForeColor="Black" Text="DCargando ..."
            OnPreRender="LblProcesando_PreRender"></asp:Label>
    </div>
</asp:Panel>
<cc1:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
    BehaviorID="myModalProgress" BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
<input type="hidden" id="hfCOD" runat="server" />
<input type="hidden" id="hfDEN" runat="server" />
<input type="hidden" id="hfCCCOD" runat="server" />
<input type="hidden" id="hfCCDEN" runat="server" />
<input type="hidden" id="hfCCEMP" runat="server" />
<input type="hidden" id="hfCCEMPDEN" runat="server" />
<input type="hidden" id="hfUsuarioPM" runat="server" />
<input type="hidden" id="hfUON" runat="server" />
