﻿Partial Public Class Activos
    Inherits FSNPage

    ''' <summary>
    ''' Evento Page_Load de la página
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumennto de evento</param>
    ''' <remarks>Se utiliza para establcer ciertas configuraciones de inicio, traducción de textos, etc</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Activos
        If Not IsPostBack Then
            Dim mMaster = CType(Me.Master, FSNWeb.Menu)
            mMaster.Seleccionar("CtrlPresup", "Activos")

            lblTitulo.Text = Textos(0)
        End If
    End Sub

End Class