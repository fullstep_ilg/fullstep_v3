﻿<System.Web.Script.Services.ScriptService()> _
Partial Public Class VisorActivos
    Inherits System.Web.UI.UserControl

    Private Const DefaultOrderByField As String = "COD"

    Private m_bAllowSelection As Boolean
    Private m_sUsuarioPM As String
    Private m_CentroSM As FSNServer.Centro_SM

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CentroSM() As FSNServer.Centro_SM
        Get
            Return m_CentroSM
        End Get
        Set(ByVal value As FSNServer.Centro_SM)
            m_CentroSM = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property UsuarioPM() As String
        Get
            Return m_sUsuarioPM
        End Get
        Set(ByVal value As String)
            m_sUsuarioPM = value
        End Set
    End Property

    Public ReadOnly Property ActivoCOD() As HtmlInputHidden
        Get
            Return hfCOD
        End Get
    End Property

    Public ReadOnly Property ActivoDEN() As HtmlInputHidden
        Get
            Return hfDEN
        End Get
    End Property

    Public ReadOnly Property ActivoCCCOD() As HtmlInputHidden
        Get
            Return hfCCCOD
        End Get
    End Property

    Public ReadOnly Property ActivoUON() As HtmlInputHidden
        Get
            Return hfUON
        End Get
    End Property

    Public Property AllowSelection() As Boolean
        Get
            Return m_bAllowSelection
        End Get
        Set(ByVal value As Boolean)
            m_bAllowSelection = value
        End Set
    End Property

    ''' <summary>
    ''' Devuelve el objeto IFSNUser con información del usuario de la página que contiene al control
    ''' </summary>
    ''' <returns>Un objeto IFSNUser con el usuario</returns>

    Public ReadOnly Property Usuario() As FSNServer.User
        Get
            Return CType(Page, FSNPage).Usuario
        End Get
    End Property

    ''' <summary>
    ''' Devuelve el idioma de la página que contiene al control
    ''' </summary>
    ''' <returns>Un String con el idioma</returns>
    Public ReadOnly Property Idioma() As String
        Get
            Return CType(Page, FSNPage).Idioma
        End Get
    End Property

    ''' <summary>
    ''' Devuelve el módulo del idioma de la página que contiene al control
    ''' </summary>
    ''' <returns>Un valor ModulosIdiomas con el idioma</returns>
    Public Property ModuloIdioma() As FSNLibrary.TiposDeDatos.ModulosIdiomas
        Get
            Return CType(Page, FSNPage).ModuloIdioma
        End Get
        Set(ByVal value As FSNLibrary.TiposDeDatos.ModulosIdiomas)
            CType(Page, FSNPage).ModuloIdioma = value
        End Set
    End Property

    ''' <summary>
    ''' Devuelve el objeto Root de la página que contiene al control
    ''' </summary>
    ''' <returns>Un valor de tipo FSNServer.Root</returns>
    Public ReadOnly Property FSNServer() As FSNServer.Root
        Get
            Return CType(Page, FSNPage).FSNServer
        End Get
    End Property

    ''' <summary>
    ''' Devuelve el objeto User de la página que contiene al control
    ''' </summary>
    ''' <returns>Un valor de tipo FSNServer.User</returns>
    Public ReadOnly Property FSNUser() As FSNServer.User
        Get
            Return CType(Page, FSNPage).FSNUser
        End Get
    End Property

    ''' <summary>
    ''' Devuelve la traducción para el texto del índice especificado
    ''' </summary>
    ''' <returns>Un String con el texto traducido</returns>
    Public ReadOnly Property Textos(ByVal iTexto As Integer) As String
        Get
            If ModuloIdioma <> TiposDeDatos.ModulosIdiomas.Activos Then
                ModuloIdioma = TiposDeDatos.ModulosIdiomas.Activos
            End If
            Return CType(Page, FSNPage).Textos(iTexto)
        End Get
    End Property

    ''' <summary>
    ''' Devuelve el objeto ScriptManager de la página que contiene al control
    ''' </summary>
    ''' <returns>Objeto de tipo ScriptManager</returns>
    Public ReadOnly Property ScriptMgr() As System.Web.UI.ScriptManager
        Get
            Return CType(Page, FSNPage).ScriptMgr
        End Get
    End Property

    ''' <summary>
    ''' Devuelve todos los activos. El resultado se almacena en la caché.
    ''' </summary>
    ''' <returns>Un DataTable con los activos</returns>
    ''' <remarks>Tiempo máximo: 3 sec</remarks>
    Public ReadOnly Property PageData() As DataTable
        Get
            If HttpContext.Current.Cache(IdentificadorActivosEnCache) Is Nothing Then
                Return CargarActivos()
            Else
                Return HttpContext.Current.Cache(IdentificadorActivosEnCache)
            End If
        End Get
    End Property

    ''' <summary>
    ''' Devuelve el identificador en la caché del DataTable de los activos
    ''' </summary>
    ''' <returns>Un string con el identificador</returns>
    ''' <remarks></remarks>
    Private ReadOnly Property IdentificadorActivosEnCache() As String
        Get
            Return "Activos_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()
        End Get
    End Property

    ''' <summary>
    ''' Devuelve el identificador en la caché del DataView resultado de la última búsqueda
    ''' </summary>
    ''' <returns>Un string con el identificador</returns>
    ''' <remarks></remarks>
    Private ReadOnly Property IdentificadorUltimaBusquedaEnCache() As String
        Get
            Return "ActivosLastSearch_" & Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()
        End Get
    End Property

    ''' <summary>
    ''' Devuelve la clave en la colección de cookies del elemento que almacena el campo de ordenación
    ''' </summary>
    ''' <returns>Un String con la clave</returns>
    ''' <remarks></remarks>
    Private ReadOnly Property OrderByFieldCookieName() As String
        Get
            Return Me.GetType.FullName + "_ORDERBY_FIELD"
        End Get
    End Property

    ''' <summary>
    ''' Devuelve la clave en la colección de cookies del elemento que almacena el sentido de ordenación
    ''' </summary>
    ''' <returns>Un String con la clave</returns>
    ''' <remarks></remarks>
    Private ReadOnly Property OrderByDirectionCookieName() As String
        Get
            Return Me.GetType.FullName + "_ORDERBY_DIR"
        End Get
    End Property

    ''' <summary>
    ''' Devuelve o establece el campo por el cual se ordena el grid de Activos
    ''' </summary>
    ''' <returns>Un String con el nombre del campo.</returns>
    ''' <remarks></remarks>
    Private Property CampoOrdenacion() As String
        Get
            If String.IsNullOrEmpty(ViewState.Item("CampoOrdenacion")) Then
                Return DefaultOrderByField
            Else
                Return ViewState.Item("CampoOrdenacion")
            End If
        End Get
        Set(ByVal value As String)
            ViewState.Item("CampoOrdenacion") = value
        End Set
    End Property

    ''' <summary>
    ''' Devuelve o establece el sentido de ordenación del grid de Activos
    ''' </summary>
    ''' <returns>Un valor SortDirection con el sentido de ordenación</returns>
    ''' <remarks></remarks>
    Private Property SentidoOrdenacion() As SortDirection
        Get
            If ViewState.Item("SentidoOrdenacion") Is Nothing Then
                Return SortDirection.Ascending
            Else
                Return CType(ViewState.Item("SentidoOrdenacion"), SortDirection)
            End If
        End Get
        Set(ByVal value As SortDirection)
            ViewState.Item("SentidoOrdenacion") = value
        End Set
    End Property

    ''' Revisado por: blp. Fecha: 30/11/2011
    ''' <summary>
    ''' Evento Page_Load de la página
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumennto de evento</param>
    ''' <remarks>Carga los textos, permisos, datos de los controles y cierta configuración de presentación. Máx. 0,5 seg.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Activos
        If Not IsPostBack() Then

            If Not HttpContext.Current.Cache(IdentificadorActivosEnCache) Is Nothing Then
                HttpContext.Current.Cache.Remove(IdentificadorActivosEnCache)
            End If
            If Not HttpContext.Current.Cache(IdentificadorUltimaBusquedaEnCache) Is Nothing Then
                HttpContext.Current.Cache.Remove(IdentificadorUltimaBusquedaEnCache)
            End If

            CargarTextos()

            Session("HayIntegracionDesdeERP") = False

            Dim oActivos As FSNServer.Activos = FSNServer.Get_Object(GetType(FSNServer.Activos))
            Dim bHayIntegracionSalida As Boolean = oActivos.HayIntegracionSentidoSalida()
            lblNumeroERP.Visible = bHayIntegracionSalida
            txtNumeroERP.Visible = bHayIntegracionSalida
            Session("HayIntegracionDesdeERP") = bHayIntegracionSalida
        
        If Not m_CentroSM Is Nothing Then
            hfCCCOD.Value = m_CentroSM.Codigo
            hfCCDEN.Value = m_CentroSM.Denominacion
            If m_CentroSM.Empresa.HasValue Then
                hfCCEMP.Value = m_CentroSM.Empresa
            End If
            hfUON.Value = m_CentroSM.UONS.ToString()
        End If

        hfUsuarioPM.Value = m_sUsuarioPM

        If Not m_CentroSM Is Nothing Then
            txtCentro.Visible = False
            imgbtnBuscarCentro.Visible = False
            lblCentroPredef.Visible = True
            If Not String.IsNullOrEmpty(m_CentroSM.Denominacion) Then
                lblCentroPredef.Text = String.Format("{0} - {1}", If(m_CentroSM.UONS.UON4 <> String.Empty, m_CentroSM.UONS.UON4, If(m_CentroSM.UONS.UON3 <> String.Empty, m_CentroSM.UONS.UON3, If(m_CentroSM.UONS.UON2 <> String.Empty, m_CentroSM.UONS.UON2, If(m_CentroSM.UONS.UON1 <> String.Empty, m_CentroSM.UONS.UON1, String.Empty)))), m_CentroSM.Denominacion)
            Else
                lblCentroPredef.Text = If(m_CentroSM.UONS.UON4 <> String.Empty, m_CentroSM.UONS.UON4, If(m_CentroSM.UONS.UON3 <> String.Empty, m_CentroSM.UONS.UON3, If(m_CentroSM.UONS.UON2 <> String.Empty, m_CentroSM.UONS.UON2, If(m_CentroSM.UONS.UON1 <> String.Empty, m_CentroSM.UONS.UON1, String.Empty))))
            End If
        End If



            Dim cEmpresas As FSNServer.CEmpresas = FSNServer.Get_Object(GetType(FSNServer.CEmpresas))
        cEmpresas.CargarListaEmpresas()

        If Not m_CentroSM Is Nothing AndAlso m_CentroSM.Empresa.HasValue Then
            ddlEmpresa.Visible = False
            lblEmpresaPredef.Visible = True
            Dim empresa As FSNServer.CEmpresa = cEmpresas.Find(AddressOf FindEmpresa)
            If Not empresa Is Nothing Then
                lblEmpresaPredef.Text = empresa.Nif & " - " & empresa.Den
                hfCCEMPDEN.Value = empresa.Den
            End If
        Else
            ddlEmpresa.DataTextField = "NifDen"
            ddlEmpresa.DataValueField = "Den"
            ddlEmpresa.DataSource = cEmpresas
            ddlEmpresa.DataBind()
        End If


        'todo 1550 queda pendiente para la tarea 1550: modificar FSNPage para cargar FSNServer.User en Session.
        'Las propiedades ya estan creadas en la clase User.
        '<FSNServer.User>.SMAltaActivos
        '<FSNServer.User>.SMModificarActivos
        '<FSNServer.User>.SMEliminarActivos

        imgExpandir.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "contraer_rojo.gif")
        CollapsiblePanelExtender1.CollapsedImage = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "expandir_rojo.gif")
        CollapsiblePanelExtender1.ExpandedImage = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "contraer_rojo.gif")

        OrdenacionSegunCookie()

        txtCodigo.MaxLength = FSNServer.LongitudesDeCodigos.giLongCodACTIVO
        txtDenominacion.MaxLength = 200
        txtNumeroERP.MaxLength = 100
        txtCentro.MaxLength = FSNServer.LongitudesDeCodigos.giLongCodCENTROCOSTE + 100

        If IsNumeric(System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion")) Then
            gridActivos.PageSize = CInt(System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion"))
        End If
        gridActivos.DataSource = GetGridDataSource()
        gridActivos.DataBind()

        AutoCompleteExtender_Codigo.ContextKey = Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()
        AutoCompleteExtender_Denominacion.ContextKey = Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()
        AutoCompleteExtender_NumeroERP.ContextKey = Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()
        AutoCompleteExtender_Centro.ContextKey = Me.Usuario.CodPersona & "_" & Me.Usuario.Idioma.ToString()
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "ScriptBlockCargando") Then
            Dim sScript = vbCrLf & "var tmpcargando2;" & vbCrLf & _
                "function MostrarCargando2() {" & vbCrLf & _
                "window.clearTimeout(tmpcargando2);" & vbCrLf & _
                "var modalprog=$find('myModalProgress');" & vbCrLf & _
                "if (modalprog) modalprog.show();" & vbCrLf & _
                "}" & vbCrLf & _
                "function TempCargando2() {" & vbCrLf & _
                "tmpcargando2 = window.setTimeout(MostrarCargando2, 2000);" & vbCrLf & _
                "}"
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "ScriptBlockCargando", sScript, True)
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "ScriptBlockCargando2") Then
            Dim sScript = vbCrLf & "function OcultarCargando2() {" & vbCrLf & _
                "var modalprog=$find('myModalProgress');" & vbCrLf & _
                "if (modalprog) modalprog.hide();" & vbCrLf & _
                "window.clearTimeout(tmpcargando2);" & vbCrLf & _
                "}"
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "ScriptBlockCargando2", sScript, True)
        End If
    End Sub

    ''' <summary>
    ''' Actualiza el DataTable de activos subyacente en la caché de la página
    ''' </summary>
    ''' <returns>Un DataTable con los activos.</returns>
    ''' <remarks>Tiempo máximo 3 sec</remarks>
    Private Function CargarActivos() As DataTable
        Dim oActivos As FSNServer.Activos = FSNServer.Get_Object(GetType(FSNServer.Activos))
        Dim dt As DataTable
        Dim centroCoste As String = hfCCCOD.Value
        Dim empresa As Nullable(Of Integer)
        If IsNumeric(hfCCEMP.Value) Then
            empresa = CInt(hfCCEMP.Value)
        End If
        'De momento se presupone que si hay usuario de PM se esta utilizando el buscador (m_bAllowSelection=true) (propiedad)
        If Not String.IsNullOrEmpty(hfUsuarioPM.Value) Then
            Dim UONs As String = hfUON.Value
            If Not String.IsNullOrEmpty(UONs) Then
                Dim UON1 As String = Nothing
                Dim UON2 As String = Nothing
                Dim UON3 As String = Nothing
                Dim UON4 As String = Nothing
                Dim aUON() As String = UONs.Split("#")
                If aUON.Length >= 1 Then
                    UON1 = aUON(0)
                    If aUON.Length >= 2 Then
                        UON2 = aUON(1)
                        If aUON.Length >= 3 Then
                            UON3 = aUON(2)
                            If aUON.Length = 4 Then
                                UON4 = aUON(3)
                            End If
                        End If
                    End If
                End If
                If empresa.HasValue Then
                    dt = oActivos.ObtenerActivos_CC_Imputacion(Idioma, hfUsuarioPM.Value, UON1, UON2, UON3, UON4, empresa)
                Else
                    dt = oActivos.ObtenerActivos_CC_Imputacion(Idioma, hfUsuarioPM.Value, UON1, UON2, UON3, UON4)
                End If
            Else
                If empresa.HasValue Then
                    dt = oActivos.ObtenerActivos_CC_Imputacion(Idioma, hfUsuarioPM.Value, , , , , empresa)
                Else
                    dt = oActivos.ObtenerActivos_CC_Imputacion(Idioma, hfUsuarioPM.Value)
                End If
            End If
        Else
            If empresa.HasValue Then
                dt = oActivos.ObtenerActivos_CC_Imputacion(Idioma, Me.Usuario.Cod, , , , , empresa)
            Else
                dt = oActivos.ObtenerActivos_CC_Imputacion(Idioma, Me.Usuario.Cod)
            End If
        End If
        CType(Me.Page, FSNPage).InsertarEnCache(IdentificadorActivosEnCache, dt)
        Return dt
    End Function

    ''' <summary>
    ''' Devuelve el origen de datos del control GridView
    ''' </summary>
    ''' <returns>Un DataView con el origen de datos</returns>
    ''' <remarks>El origen de datos del grid tiene en cuenta los filtros y la ordenación</remarks>
    Private Function GetGridDataSource() As DataView
        Dim result As DataView = HttpContext.Current.Cache(IdentificadorUltimaBusquedaEnCache)
        If result Is Nothing Then
            Dim dtActivos As DataTable = PageData
            If Not dtActivos Is Nothing Then
                result = New DataView(dtActivos)
            End If
        End If
        Sort(result)
        Return result
    End Function

    ''' <summary>
    ''' Devuelve el resultado de la busqueda. El resultado se devuelve ordenado.
    ''' </summary>
    ''' <returns>Un DataView con el resultado de la búsqueda</returns>
    ''' <remarks></remarks>
    Private Function Search() As DataView
        HttpContext.Current.Cache.Remove(IdentificadorUltimaBusquedaEnCache)
        Dim result As DataView = Nothing
        Dim dtActivos As DataTable = PageData
        If Not dtActivos Is Nothing AndAlso dtActivos.Rows.Count > 0 Then
            'valor por defecto
            result = dtActivos.Clone.DefaultView

            Dim patternCodigo As String = "*"
            Dim patternDenominacion As String = "*"
            Dim patternCodigoERP As String = "*"
            Dim patternEmpresa As String = "*"
            Dim patternCentro As String = "*"

            If Not String.IsNullOrEmpty(txtCodigo.Text) Then patternCodigo = String.Concat("*", strToSQLLIKE(UCase(txtCodigo.Text), True), "*")
            If Not String.IsNullOrEmpty(txtDenominacion.Text) Then patternDenominacion = String.Concat("*", strToSQLLIKE(UCase(txtDenominacion.Text), True), "*")
            If Not String.IsNullOrEmpty(txtNumeroERP.Text) Then patternCodigoERP = String.Concat("*", strToSQLLIKE(UCase(txtNumeroERP.Text), True), "*")
            If Not String.IsNullOrEmpty(hfCCCOD.Value) Then
                patternCentro = String.Concat("*", strToSQLLIKE(UCase(hfCCCOD.Value), True), "*")
            Else
                If Not String.IsNullOrEmpty(txtCentro.Text) Then patternCentro = String.Concat("*", strToSQLLIKE(UCase(txtCentro.Text), True), "*")
            End If
            If Not String.IsNullOrEmpty(hfCCEMPDEN.Value) Then
                patternEmpresa = String.Concat("*", strToSQLLIKE(UCase(hfCCEMPDEN.Value), True), "*")
            Else
                If Not String.IsNullOrEmpty(ddlEmpresa.SelectedItem.Value) Then patternEmpresa = String.Concat("*", strToSQLLIKE(UCase(ddlEmpresa.SelectedItem.Value), True), "*")
            End If

            Dim match As EnumerableRowCollection(Of DataRow) = From Datos In dtActivos _
                        Where (UCase(DBNullToStr(Datos("COD")).ToString()) Like patternCodigo) _
                        And (UCase(DBNullToStr(Datos("DEN_ACTIVO")).ToString()) Like patternDenominacion) _
                        And (UCase(DBNullToStr(Datos("COD_ERP")).ToString()) Like patternCodigoERP) _
                        And (UCase(DBNullToStr(Datos("DEN_EMPRESA")).ToString()) Like patternEmpresa) _
                        And (UCase(DBNullToStr(Datos("ID_DEN_CENTRO")).ToString()) Like patternCentro) _
                        Select Datos

            If match.Count > 0 Then result = match.AsDataView
            Sort(result)

            CType(Me.Page, FSNPage).InsertarEnCache(IdentificadorUltimaBusquedaEnCache, result)
        End If
        Return result
    End Function

    ''' <summary>
    ''' Ordena el DataView pasado como argumento. La ordenación se hace 
    ''' sobre el campo establecido por la propiedad 'CampoOrdenación'.
    ''' </summary>
    ''' <param name="data">DataView que se quiere ordenar</param>
    ''' <remarks></remarks>
    Private Sub Sort(ByVal data As DataView)
        If Not data Is Nothing Then
            Dim sortExpression As String = CampoOrdenacion
            If SentidoOrdenacion = SortDirection.Ascending Then
                sortExpression += " ASC"
            Else
                sortExpression += " DESC"
            End If
            data.Sort = sortExpression
        End If
    End Sub

    ''' <summary>
    ''' Inicializa todos los textos en la primera carga de la página
    ''' </summary>
    Private Sub CargarTextos()
        lblBusquedaAvanzada.Text = Textos(5)
        lblCodigo.Text = Textos(1) & ":"
        lblDenominacion.Text = Textos(2) & ":"
        lblNumeroERP.Text = Textos(3) & ":"
        lblCentro.Text = Textos(10) & ":"
        lblEmpresa.Text = Textos(4) & ":"
        btnBuscar.Text = Textos(6)
        gridActivos.Columns(1).HeaderText = Textos(1)
        gridActivos.Columns(2).HeaderText = Textos(2)
        gridActivos.Columns(3).HeaderText = Textos(3)
        gridActivos.Columns(4).HeaderText = Textos(4)
        gridActivos.Columns(5).HeaderText = Textos(10)
    End Sub

#Region "Paginacion"
    ''' <summary>
    ''' Evento que ocurre cada vez que se cambia de página
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza la información cacheada para no acceder a la base de datos. Tiempo máximo 0 sg.</remarks>
    Private Sub gridActivos_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridActivos.PageIndexChanging
        SetPage(e.NewPageIndex)
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando se está presentando el control
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para configurar el control ImageButton de la paginación</remarks>
    Protected Sub ImgBtnFirst_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        If Not gridActivos.PageIndex = 0 Then
            CType(sender, ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero.gif")
            CType(sender, ImageButton).Enabled = True
        Else
            CType(sender, ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero_desactivado.gif")
            CType(sender, ImageButton).Enabled = False
        End If
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando se está presentando el control
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para configurar el control ImageButton de la paginación</remarks>
    Protected Sub ImgBtnPrev_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        If Not gridActivos.PageIndex = 0 Then
            CType(sender, ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior.gif")
            CType(sender, ImageButton).Enabled = True
        Else
            CType(sender, ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior_desactivado.gif")
            CType(sender, ImageButton).Enabled = False
        End If
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando se está presentando el control
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para configurar el control ImageButton de la paginación</remarks>
    Protected Sub ImgBtnNext_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        If gridActivos.PageIndex = gridActivos.PageCount - 1 Then
            CType(sender, ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente_desactivado.gif")
            CType(sender, ImageButton).Enabled = False
        Else
            CType(sender, ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente.gif")
            CType(sender, ImageButton).Enabled = True
        End If
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando se está presentando el control
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para configurar el control ImageButton de la paginación</remarks>
    Protected Sub ImgBtnLast_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        If gridActivos.PageIndex = gridActivos.PageCount - 1 Then
            CType(sender, ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo_desactivado.gif")
            CType(sender, ImageButton).Enabled = False
        Else
            CType(sender, ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo.gif")
            CType(sender, ImageButton).Enabled = True
        End If
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando cambia la selección en el control
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para establecer la página actual en el GridView</remarks>
    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        SetPage(CType(sender, DropDownList).SelectedItem.Value - 1)
    End Sub

    ''' <summary>
    ''' Cambia la página actual en el GridView
    ''' </summary>
    ''' <param name="pageIndex">Integer con el índice de la página actual</param>
    ''' <remarks>La paginación utiliza los datos de la última búsqueda realizada; 
    ''' si no existe se utilizan todos los datos de la caché</remarks>
    Private Sub SetPage(ByVal pageIndex As Integer)
        gridActivos.PageIndex = pageIndex
        gridActivos.DataSource = GetGridDataSource()
        gridActivos.DataBind()
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando se está presentando el control
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para establecer el número de la página en el combo</remarks>
    Protected Sub CmbBoxNumPags_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, DropDownList).Items.Clear()
        For i As Integer = 1 To gridActivos.PageCount
            CType(sender, DropDownList).Items.Add(i)
        Next
        CType(sender, DropDownList).SelectedIndex = gridActivos.PageIndex
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando se está presentando el control
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para establecer el texto de la label</remarks>
    Protected Sub LblDe_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, Label).Text = Textos(8)
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando se está presentando el control
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para establecer el texto de la label</remarks>
    Protected Sub LblTotal_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, Label).Text = gridActivos.PageCount
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando se está presentando el control
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para establecer el texto de la label</remarks>
    Protected Sub LblPagina_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, Label).Text = Textos(7)
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando se está presentando el control
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para ocultar columnas en el GridView y mostrar (siempre) la fila de paginación,
    ''' aún cuando el número de registros es menor que PagerSize</remarks>
    Private Sub gridActivos_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridActivos.PreRender
        Dim grid As GridView = CType(sender, GridView)
        If Not grid Is Nothing Then
            If Not CBool(Session("HayIntegracionDesdeERP")) Then
                grid.Columns(3).Visible = False
            End If
            Dim pagerRow As GridViewRow = grid.TopPagerRow
            If Not pagerRow Is Nothing Then
                pagerRow.Visible = True
                Dim lblOrdenarPor As Label = TryCast(pagerRow.FindControl("lblOrdenarPor"), Label)
                lblOrdenarPor.Text = Textos(9) & ":"
            End If
        End If
    End Sub

#End Region

#Region "Ordenacion"
    ''' <summary>
    ''' Carga la lista de los campos de ordenación
    ''' </summary>
    ''' <remarks>Tiempo máximo 0 sg.</remarks>
    Protected Sub CargarListaOrdenacion(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddlOrdenarPor1 As DropDownList = CType(sender, DropDownList)
        If Not ddlOrdenarPor1 Is Nothing Then
            ddlOrdenarPor1.Items.Clear()
            ddlOrdenarPor1.Items.Add(New ListItem(Textos(1), "COD"))
            ddlOrdenarPor1.Items.Add(New ListItem(Textos(2), "DEN_ACTIVO"))
            If CBool(Session("HayIntegracionDesdeERP")) Then
                ddlOrdenarPor1.Items.Add(New ListItem(Textos(3), "COD_ERP"))
            End If
            ddlOrdenarPor1.Items.Add(New ListItem(Textos(4), "DEN_EMPRESA"))
            ddlOrdenarPor1.Items.Add(New ListItem(Textos(10), "CENTRO_SM"))
            ddlOrdenarPor1.SelectedValue = CampoOrdenacion
        End If
    End Sub

    ''' <summary>
    ''' Lee el campo y el sentido de ordenación de las cookies almacenadas. Si no hay cookies,
    ''' el campo de ordenación por defecto es el código del activo y el sentido de ordenación 
    ''' es ascendente.
    ''' </summary>
    ''' <remarks>Tiempo máximo 0sg.</remarks>
    Private Sub OrdenacionSegunCookie()
        Dim cookie As HttpCookie
        cookie = Request.Cookies(OrderByFieldCookieName)
        If cookie Is Nothing OrElse String.IsNullOrEmpty(cookie.Value) _
            OrElse (Not CBool(Session("HayIntegracionDesdeERP")) And cookie.Value = "COD_ERP") Then
            CampoOrdenacion = DefaultOrderByField
        Else
            CampoOrdenacion = cookie.Value
            cookie.Expires = DateTime.Now.AddDays(30)
            Response.AppendCookie(cookie)
        End If
        cookie = Request.Cookies(OrderByDirectionCookieName)
        If cookie Is Nothing OrElse String.IsNullOrEmpty(cookie.Value) Then
            SentidoOrdenacion = SortDirection.Ascending
        Else
            SentidoOrdenacion = cookie.Value
            cookie.Expires = DateTime.Now.AddDays(30)
            Response.AppendCookie(cookie)
        End If
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando se hace click sobre el control ImageButton que
    ''' se utiliza para establecer el sentido de ordenación de la búsqueda.
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para establcer la imagen del control ImageButton</remarks>
    Protected Sub SetOrderByImage(ByVal sender As Object, ByVal e As EventArgs)
        Dim ib As ImageButton = CType(sender, ImageButton)
        If SentidoOrdenacion = SortDirection.Ascending Then
            ib.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ascendente.gif")
        Else
            ib.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "descendente.gif")
        End If
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando cambia la selección en el control
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para aplicar la ordenación en el GridView. Tiempo máximo 2 sg.</remarks>
    Protected Sub ddlOrdenarPor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlOrdenarPor1 As DropDownList = TryCast(sender, DropDownList)
        If ddlOrdenarPor1 IsNot Nothing Then
            gridActivos.Sort(ddlOrdenarPor1.SelectedValue, SentidoOrdenacion)
        End If
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando se invoca el método Sort del control GridView
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para aplicar los criterios de ordenación</remarks>
    Private Sub gridActivos_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gridActivos.Sorting
        If CampoOrdenacion <> e.SortExpression Then
            CampoOrdenacion = e.SortExpression
            Dim cookie As HttpCookie = Request.Cookies(OrderByFieldCookieName)
            If cookie Is Nothing Then
                cookie = New HttpCookie(OrderByFieldCookieName, CampoOrdenacion)
            Else
                cookie.Value = CampoOrdenacion
            End If
            cookie.Expires = DateTime.Now.AddDays(30)
            Response.AppendCookie(cookie)
        End If

        If SentidoOrdenacion <> e.SortDirection Then
            SentidoOrdenacion = e.SortDirection
            Dim cookie As HttpCookie = Request.Cookies(OrderByDirectionCookieName)
            If cookie Is Nothing Then
                cookie = New HttpCookie(OrderByDirectionCookieName, SentidoOrdenacion)
            Else
                cookie.Value = SentidoOrdenacion
            End If
            cookie.Expires = DateTime.Now.AddDays(30)
            Response.AppendCookie(cookie)
        End If

        gridActivos.DataSource = GetGridDataSource()
        gridActivos.DataBind()
    End Sub

    ''' <summary>
    ''' Ordena la grid ascendente o descendentemente por el criterio seleccionado en el desplegable.
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>se utiliza para aplicar el sentido de ordenación en el GridView</remarks>
    Protected Sub ibOrdenacion_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If SentidoOrdenacion = SortDirection.Ascending Then
            SentidoOrdenacion = SortDirection.Descending
        Else
            SentidoOrdenacion = SortDirection.Ascending
        End If
        gridActivos.Sort(CampoOrdenacion, SentidoOrdenacion)
    End Sub
#End Region

#Region "Busqueda y selección"
    ''' <summary>
    ''' Evento que ocurre cuando se hace click en el control.
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para ejecutar la búsqueda sobre los datos cacheados. 
    ''' Si la consulta dura más de 2 seg. aparece el popup de 'Cargando...'</remarks>
    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        gridActivos.DataSource = Search()
        gridActivos.DataBind()
        If ScriptMgr.IsInAsyncPostBack Then
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "ScriptClosing", "OcultarCargando2();", True)
        End If
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando se carga cada fila del GridView
    ''' </summary>
    ''' <param name="sender">objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para asignar el texto traducido a la etiqueta de la fila que se muestra en el 
    ''' GridView cuando no hay datos para mostrar</remarks>
    Private Sub gridActivos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gridActivos.RowDataBound
        If e.Row.RowType = DataControlRowType.EmptyDataRow Then
            Dim lblEmptyDataText As Label = e.Row.FindControl("lblEmptyDataText")
            If Not lblEmptyDataText Is Nothing Then
                lblEmptyDataText.Text = Textos(12)
            End If
        ElseIf e.Row.RowType = DataControlRowType.DataRow Then
            Dim dataItem As DataRowView = CType(e.Row.DataItem, DataRowView)

            Dim iMaximoCaracteresCodigo As Integer = 10
            Dim iMaximoCaracteresDen As Integer = 20
            Dim iMaximoCaracteresERP As Integer = 10
            Dim iMaximoCaracteresEmpresa As Integer = 18
            Dim iMaximoCaracteresCentro As Integer = 20
            If Not CBool(Session("HayIntegracionDesdeERP")) Then
                iMaximoCaracteresCodigo = 12
                iMaximoCaracteresDen = 23
                iMaximoCaracteresERP = 0
                iMaximoCaracteresEmpresa = 21
                iMaximoCaracteresCentro = 22
            End If

            Dim sTextoLinea As String = ""
            If CBool(Session("HayIntegracionDesdeERP")) Then
                sTextoLinea = dataItem("COD") & dataItem("DEN_ACTIVO") & dataItem("COD_ERP") & dataItem("DEN_EMPRESA") & dataItem("ID_DEN_CENTRO")
            Else
                sTextoLinea = dataItem("COD") & dataItem("DEN_ACTIVO") & dataItem("DEN_EMPRESA") & dataItem("ID_DEN_CENTRO")
            End If
            Dim iLenDEN_ACTIVO As Integer = Len(DBNullToStr(dataItem("DEN_ACTIVO")))
            Dim iLenCODIGO As Integer = Len(DBNullToStr(dataItem("COD")))
            Dim iLenERP As Integer = Len(DBNullToStr(dataItem("COD_ERP")))
            Dim iLenDEN_EMPRESA As Integer = Len(DBNullToStr(dataItem("DEN_EMPRESA")))
            Dim iLenID_DEN_CENTRO As Integer = Len(DBNullToStr(dataItem("ID_DEN_CENTRO")))
            Dim iTotalCaracteresDondeReducir As Integer = (iLenDEN_ACTIVO + iLenDEN_EMPRESA + iLenID_DEN_CENTRO)
            Dim oGridActivos As GridView = CType(sender, GridView)
            Dim iIndexCeldaDEN_ACTIVO As Integer = ColumnIndexBySortExpression(CType(sender, GridView).Columns, "DEN_ACTIVO")
            Dim iIndexCeldaCODIGO As Integer = ColumnIndexBySortExpression(CType(sender, GridView).Columns, "COD")
            Dim iIndexCeldaERP As Integer = ColumnIndexBySortExpression(CType(sender, GridView).Columns, "COD_ERP")
            Dim iIndexCeldaDEN_EMPRESA As Integer = ColumnIndexBySortExpression(CType(sender, GridView).Columns, "DEN_EMPRESA")
            Dim iIndexCeldaID_DEN_CENTRO As Integer = ColumnIndexBySortExpression(CType(sender, GridView).Columns, "CENTRO_SM")
            Dim bAcortadoDEN_ACTIVO As Boolean = False
            Dim bAcortadoCODIGO As Boolean = False
            Dim bAcortadoERP As Boolean = False
            Dim bAcortadoDEN_EMPRESA As Boolean = False
            Dim bAcortadoID_DEN_CENTRO As Boolean = False
            Dim iLongitudes() As Double = {iLenDEN_ACTIVO, iLenDEN_EMPRESA, iLenID_DEN_CENTRO}
            With e.Row
                'Acortamos el texto de CODIGO
                If iIndexCeldaCODIGO > 0 AndAlso iLenCODIGO > iMaximoCaracteresCodigo Then
                    .Cells(iIndexCeldaCODIGO).Text = Left(dataItem("COD"), iMaximoCaracteresCodigo)
                    bAcortadoCODIGO = True
                End If
                'Acortamos el texto de DEN_ACTIVO
                If iIndexCeldaDEN_ACTIVO > 0 AndAlso iLenDEN_ACTIVO > iMaximoCaracteresDen Then
                    .Cells(iIndexCeldaDEN_ACTIVO).Text = Left(dataItem("DEN_ACTIVO"), iMaximoCaracteresDen)
                    bAcortadoDEN_ACTIVO = True
                End If
                'Acortamos el texto de ERP
                If CBool(Session("HayIntegracionDesdeERP")) AndAlso iIndexCeldaERP > 0 AndAlso iLenERP > iMaximoCaracteresERP Then
                    .Cells(iIndexCeldaERP).Text = Left(dataItem("COD_ERP"), iMaximoCaracteresERP)
                    bAcortadoERP = True
                End If
                'Acortamos el texto de DEN_EMPRESA. 
                If iIndexCeldaDEN_EMPRESA > 0 AndAlso iLenDEN_EMPRESA > iMaximoCaracteresEmpresa Then
                    .Cells(iIndexCeldaDEN_EMPRESA).Text = Left(dataItem("DEN_EMPRESA"), iMaximoCaracteresEmpresa)
                    bAcortadoDEN_EMPRESA = True
                End If
                'Acortamos el texto de ID_DEN_CENTRO. 
                If iIndexCeldaID_DEN_CENTRO > 0 AndAlso iLenID_DEN_CENTRO > iMaximoCaracteresCentro Then
                    .Cells(iIndexCeldaID_DEN_CENTRO).Text = Left(dataItem("ID_DEN_CENTRO"), iMaximoCaracteresCentro)
                    bAcortadoID_DEN_CENTRO = True
                End If

                If bAcortadoCODIGO Then
                    .Cells(iIndexCeldaCODIGO).ToolTip = dataItem("COD")
                    .Cells(iIndexCeldaCODIGO).Text = .Cells(iIndexCeldaCODIGO).Text & "..."
                End If
                If bAcortadoDEN_ACTIVO Then
                    .Cells(iIndexCeldaDEN_ACTIVO).ToolTip = dataItem("DEN_ACTIVO")
                    .Cells(iIndexCeldaDEN_ACTIVO).Text = .Cells(iIndexCeldaDEN_ACTIVO).Text & "..."
                End If
                If bAcortadoERP Then
                    .Cells(iIndexCeldaERP).ToolTip = dataItem("COD_ERP")
                    .Cells(iIndexCeldaERP).Text = .Cells(iIndexCeldaDEN_ACTIVO).Text & "..."
                End If
                If bAcortadoDEN_EMPRESA Then
                    .Cells(iIndexCeldaDEN_EMPRESA).ToolTip = dataItem("DEN_EMPRESA")
                    .Cells(iIndexCeldaDEN_EMPRESA).Text = .Cells(iIndexCeldaDEN_EMPRESA).Text & "..."
                End If
                If bAcortadoID_DEN_CENTRO Then
                    .Cells(iIndexCeldaID_DEN_CENTRO).ToolTip = dataItem("ID_DEN_CENTRO")
                    .Cells(iIndexCeldaID_DEN_CENTRO).Text = .Cells(iIndexCeldaID_DEN_CENTRO).Text & "..."
                End If
            End With

            If m_bAllowSelection Then
                e.Row.Attributes.Add("onMouseOver", "this.style.cursor='hand';")
                'Dim dataItem As DataRowView = CType(e.Row.DataItem, DataRowView)
                Dim ext As String = String.Format("AsignarValores('{1}','{2}','{3}','{4}');", e.Row.RowIndex, dataItem("COD"), JSText(dataItem("DEN_ACTIVO")), dataItem("ID_DEN_CENTRO"), dataItem("UONS"))
                e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(CType(sender, GridView), "Select$" & e.Row.RowIndex) & ";" & ext)
            End If
        End If
    End Sub


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="writer"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        If m_bAllowSelection Then
            If gridActivos.Rows.Count > 0 Then
                For Each row As GridViewRow In gridActivos.Rows
                    If row.RowType = DataControlRowType.DataRow Then
                        Page.ClientScript.RegisterForEventValidation(gridActivos.UniqueID, "Select$" & row.RowIndex)
                    End If
                Next
            End If
        End If
        MyBase.Render(writer)
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub gridActivos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridActivos.DataBound
        gridActivos.SelectedIndex = -1
    End Sub
#End Region

    ''' <summary>
    ''' Evento que ocurre cuando se está presentando el control.
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para establcer el texto a la label del popup 'Cargando...'</remarks>
    Protected Sub LblProcesando_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, Label).Text = Textos(11)
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UONs"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ExtraerCentroCoste(ByVal UONs As String) As String
        'el ultimo codigo de la cadena es el codigo del centro de coste
        If Not String.IsNullOrEmpty(UONs) Then
            Dim items As String() = UONs.Split(New Char() {"#"})
            If items.Length > 0 Then
                Return items(items.Length - 1)
            End If
        End If
        Return Nothing
    End Function

    Private Function FindEmpresa(ByVal empresa As FSNServer.CEmpresa) As Boolean
        If IsNumeric(hfCCEMP.Value) Then
            Return empresa.ID = CInt(hfCCEMP.Value)
        Else
            Return False
        End If
    End Function

#Region "GridActivos helpers"

    ''' <summary>
    ''' Funcion que nos devuelve el índice de una columna concreta buscando por su SortExpression
    ''' </summary>
    ''' <param name="Columns">Colección de columnas (ejemplo:columnas de un gridview)</param>
    ''' <param name="SortExpression">Valor que buscamos como SortExpression de la columna</param>
    ''' <returns>El índice de la columna buscada</returns>
    ''' <remarks>Llamada desde gridActivos_RowDataBound. Tiempo máx inferior a 1 seg</remarks>
    Private Function ColumnIndexBySortExpression(ByVal Columns As DataControlFieldCollection, ByVal SortExpression As String) As Integer
        For i As Integer = 0 To Columns.Count - 1
            If Columns(i).SortExpression = SortExpression Then
                Return i
            End If
        Next
        Return -1
    End Function

#End Region


End Class