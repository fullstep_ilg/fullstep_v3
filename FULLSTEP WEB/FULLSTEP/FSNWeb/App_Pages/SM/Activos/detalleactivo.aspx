﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="detalleactivo.aspx.vb"
    Inherits="Fullstep.FSNWeb.detalleactivo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title ></title>
</head>
<body style="background-color: #FFFFFF; padding-left: 5px; padding-top: 10px; min-height: 250px;" onload="window.focus();">
    <form id="form1" runat="server">
    <table cellspacing="1" cellpadding="4" width="100%" border="0">
        <tr>
            <td width="5%">
                <img src="../images/dolar.JPG" />
            </td>
            <td width="90%" align="center" nowrap>
                <asp:Label ID="lblTitulo" runat="server" cssClass="RotuloGrande"></asp:Label>
            </td>
            <td width="5%">
                <asp:ImageButton ID="imgCerrar" runat="server" SkinID="Cerrar" OnClientClick="javascript:self.close()" />
            </td>
        </tr>
    </table>
    <table cellspacing="1" cellpadding="4" width="100%" border="0">
        <tr>
            <td style="width:35%">
                <asp:Label ID="lblCodigo" runat="server" CssClass="Etiqueta"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblCodigoBD" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width:35%">
                <asp:Label ID="lblDenominacion" runat="server" CssClass="Etiqueta"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblDenominacionBD" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width:35%">
                <asp:Label ID="lblCentro" runat="server" CssClass="Etiqueta"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblCentroBD" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width:35%">
                <asp:Label ID="lblEmpresa" runat="server" CssClass="Etiqueta"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblEmpresaBD" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width:35%">
                <asp:Label ID="lblNumeroERP" runat="server" CssClass="Etiqueta"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblNumeroERPBD" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
