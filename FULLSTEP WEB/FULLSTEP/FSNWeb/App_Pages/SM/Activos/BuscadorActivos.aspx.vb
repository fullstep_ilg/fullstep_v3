﻿Public Partial Class BuscadorActivos
    Inherits FSNPage

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Activos
        
        If Not IsPostBack Then

            lblTitulo.Text = Textos(15)
            btnSeleccionar.Text = Textos(13)
            btnCancelar.Text = Textos(14)

            IDCONTROL.Value = Request("IdControl")
            IDCentroCoste.Value = Request("CCEntryId")

            If Not String.IsNullOrEmpty(Request("UONs")) Then
                Dim SMServer As FSNServer.Root = TryCast(Session("FSN_Server"), FSNServer.Root)
                Dim oCentroSM As FSNServer.Centro_SM = SMServer.Get_Object(GetType(FSNServer.Centro_SM))
                oCentroSM.Load(Idioma, Request("UONs"))
                ucVisorActivos.CentroSM = oCentroSM
            End If
            ucVisorActivos.UsuarioPM = FSNUser.Cod
        End If
    End Sub
End Class