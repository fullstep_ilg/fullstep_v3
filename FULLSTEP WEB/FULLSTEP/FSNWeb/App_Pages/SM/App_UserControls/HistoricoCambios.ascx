﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="HistoricoCambios.ascx.vb" Inherits="Fullstep.FSNWeb.HistoricoCambios" %>


<script type="text/javascript">
    var timeoutID = 0;
    var x = 0
    var y = 0
    
    /*<summary>
    Obtiene las posiciones del cursor, necesario para el FireFox
    </summary>
    <remarks>Llamada desde: UltraWebGrid_MouseOverHadler; Tiempo=0seg.</remarks>*/
    function Posicion(event) {
        x = event.clientX;
        y = event.clientY;
    }    

    /* ''' <summary>
    ''' Responder al evento click en una celda del grid
    ''' </summary>
    ''' <remarks>Llamada desde: sistema; Tiempo máximo:0</remarks>*/
    function grid_CellClick(sender, e) {
        if (e.get_type() == "cell" && e.get_item().get_row().get_index() !== -1) {
            var celda = e.get_item();
            var row = celda.get_row();
            var skey = celda.get_column().get_key();

            switch (skey) {
                case "USU":
                    if ((row.get_cellByColumnKey("USU").get_value() != '') && (row.get_cellByColumnKey("USU").get_value() != null)) {
                        posicionX = $(celda.get_element()).position().left + ($(celda.get_element()).innerWidth() / 4);
                        posicionY = $(celda.get_element()).position().top + 20;
                        //FSNMostrarPanel('<%= FSNPanelDatosUsuario.AnimationClientID %>', event, '<%= FSNPanelDatosUsuario.DynamicPopulateClientID %>', row.get_cellByColumnKey("USU").get_value(), '<%= Me.NamingContainer.FindControl("pnlPartidaPresupuestaria").ClientID %>');
                        FSNMostrarPanel('<%= FSNPanelDatosUsuario.AnimationClientID %>', e, '<%= FSNPanelDatosUsuario.DynamicPopulateClientID %>', row.get_cellByColumnKey("USU").get_value(), '<%= Me.NamingContainer.FindControl("pnlPartidaPresupuestaria").ClientID %>', null, posicionX, posicionY);
                        return false;
                    }
                    break;
                case "COMENTARIO":
                    if (row.get_cellByColumnKey("COMENTARIO").get_value() == null && document.getElementById('<%= pPresup.ClientID %>').value == "0") {
                        return false;
                    } else {
                        __doPostBack('<%= hidEvent.UniqueID %>', JSON.stringify({ Id: row.get_cellByColumnKey("ID").get_value(), Coment: row.get_cellByColumnKey("COMENT").get_value(), UsuComent: row.get_cellByColumnKey("USU_COMENT").get_value(), Fecha: row.get_cellByColumnKey("FECHA").get_value() }));
                        return false;
                    }
                default:
                    return false;
            }
        }
    }

    function OcultarCargandoPopUp() {
        $find('mpeComent').hide();
        document.getElementById('<%= pnlCargandoPopUp.ClientID %>').style.display = "none";
    }
</script>

<asp:UpdatePanel runat="server" ID="updTitulo" UpdateMode="Conditional" style="margin-top:0.2em;">
    <ContentTemplate>
        <fsn:FSNPageHeader ID="FSNPageHeaderHist" runat="server" UrlImagenCabecera="~/images/ControlPresupuestario.gif"/>
    </ContentTemplate>
</asp:UpdatePanel>           
         
<asp:UpdatePanel ID="upPartidaPresup" runat="server" UpdateMode="Conditional" style="margin:0.5em;">
    <ContentTemplate> 
        <asp:Label ID="lblPartidaPresupuestaria" runat="server" Text="dPartida Presupuestaria"></asp:Label> 
        <asp:HiddenField runat="server" ID="pConsulta" />
        <asp:HiddenField runat="server" ID="pPresup" />                                                                                                   
    </ContentTemplate>
</asp:UpdatePanel>                        
   
<div class="ColorFondoTab" style="height:25vh; border:solid 1px Gray; margin:0.5em;">                                    
    <asp:UpdatePanel ID="upHistorico" runat="server" UpdateMode="Conditional" >
        <ContentTemplate>  
            <asp:HiddenField ID="hidEvent" runat="server"/>      
            <ig:WebDataGrid ID="wdgHistorico" runat="server" Height="221px" AutoGenerateBands="false" AutoGenerateColumns="false"
             EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="true">
                <Behaviors>
                    <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Row"></ig:Selection> 
                    <ig:RowSelectors Enabled="false" RowNumbering="false"></ig:RowSelectors>
                    <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                </Behaviors>
                <ClientEvents Click="grid_CellClick" />
            </ig:WebDataGrid>            
        </ContentTemplate>
    </asp:UpdatePanel>
</div>   

<fsn:FSNPanelInfo ID="FSNPanelDatosUsuario" runat="server" ServicePath="~/App_Pages/_Common/App_Services/Consultas.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="3"/>    
<asp:Panel runat="server" ID="pnlCargandoPopUp" style="display:none;">
    <div style="clear:both; position:absolute; top:0; left:0; width:100%; height:100%; z-index:100001;" class="modalBackground"></div>
    <div class="updateProgress" style="top:50%; left:40%; z-index:100002;">
        <div style="position: relative; top: 30%; text-align: center;">
            <asp:Image ID="ImgProgress" runat="server" SkinId="ImgProgress" />
        </div>            
    </div>
</asp:Panel>
<asp:Button runat="server" ID="btnHiddenComent" style="display:none;"/>   
<ajx:ModalPopupExtender runat="server" ID="mpeComent"  
    CancelControlID="ImgBtnCerrarComent" OnCancelScript="OcultarCargandoPopUp()"   
    PopupControlID="pnlComent"  
    TargetControlID="btnHiddenComent"
    BehaviorID="mpeComent">
</ajx:ModalPopupExtender> 
<asp:Panel runat="server" ID="pnlComent" CssClass="BorderedPopUp" style="display:none; z-index:100003; width:450px;">
    <div style="background-color:White;">        
        <div style="float:left; margin-left:1%; margin-top:10px; margin-bottom:10px; margin-right:1%; width:90%;">            
            <asp:UpdatePanel ID="upInfoComent" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Label runat="server" ID="lblID" style="display:none" ></asp:Label>
                    <asp:Label runat="server" ID="lblInfoComent" Text="Usuario" CssClass="Etiqueta"></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>                                                           
        </div>          
        <div style="float:right; margin-top:5px; margin-right:5px;">                                
            <asp:ImageButton ID="ImgBtnCerrarComent" runat="server" SkinID="Cerrar" ToolTip="Cerrar Detalle"/>                                                 
        </div>                                 
        <div style="clear:both; padding-left:10px; padding-right:10px; height:150px; overflow:auto;">      
            <asp:UpdatePanel ID="upComent" runat="server" UpdateMode="Conditional">                     
                <ContentTemplate>                                            
                    <asp:Label ID="lblComent" runat="server" CssClass="Normal" Height="145px" Width="100%"></asp:Label>
                    <asp:TextBox ID="txtComent" runat="server" CssClass="Normal" TextMode="MultiLine" Height="145px" Width="100%"></asp:TextBox>	                                             	                
                </ContentTemplate>
            </asp:UpdatePanel>                                                                                                                                                                                                                                              
        </div>          
        <div style="clear:both; padding-bottom:30px; padding-top:10px; width:100%;">            
            <asp:UpdatePanel ID="upBotones" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel id="pnConsulta" runat="server">
                        <div style="clear:both; float:none; width:100%; padding-left:50%; padding-right:50%;">                                                   
                            <fsn:FSNButton ID="cmdCerrarComent" runat="server" Text="Cerrar" Alineacion="Left"></fsn:FSNButton>                                                                                                                                                                                                                                                                                                                               
                        </div>
                    </asp:Panel>
                    <asp:Panel id="pnEdicion" runat="server">
                        <div style="width:48%; float:left;">                                                                                                                          
                            <fsn:FSNButton ID="cmdAceptar" runat="server" Text="Aceptar" Alineacion="Right"></fsn:FSNButton>                                                                                                                                                                                                                                                                                                                                      
                        </div>   
                        <div style="width:48%; float:right;"> 
                            <fsn:FSNButton ID="cmdCancelar" runat="server" Text="Cancelar" Alineacion="Left"></fsn:FSNButton>       
                        </div>
                    </asp:Panel>     
                </ContentTemplate>                                 
            </asp:UpdatePanel>                                                                                         
        </div>          
    </div>                
</asp:Panel>