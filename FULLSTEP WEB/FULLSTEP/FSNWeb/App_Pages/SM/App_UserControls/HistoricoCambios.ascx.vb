﻿Imports Fullstep.FSNLibrary
Imports Fullstep.FSNServer

Partial Public Class HistoricoCambios
    Inherits System.Web.UI.UserControl

    Private Const _ModuloIdioma As TiposDeDatos.ModulosIdiomas = TiposDeDatos.ModulosIdiomas.HistoricoPartidasPresupuestarias

    Private _oParentPage As FSNPage
    Private _sPres0 As String
    Private _sPres1 As String
    Private _sPres2 As String
    Private _sPres3 As String
    Private _sPres4 As String
    Private _Textos As DataSet
    Private _bModoEdicion As Boolean

    Public Event OnUsuClick(ByVal Usu As String)
    Public Event OnComentClick(ByVal ID As Integer, ByVal Fecha As Date, ByVal Comentario As String, ByVal UsuComent As String, ByVal bConsulta As Boolean, ByVal bPresup As Boolean)

#Region " Propiedades "
	Public Property Pres0() As String
		Get
			Return _sPres0
		End Get
		Set(ByVal value As String)
			_sPres0 = value
		End Set
	End Property
	Public Property Pres1() As String
		Get
			Return _sPres1
		End Get
		Set(ByVal value As String)
			_sPres1 = value
		End Set
	End Property
	Public Property Pres2() As String
		Get
			Return _sPres2
		End Get
		Set(ByVal value As String)
			_sPres2 = value
		End Set
	End Property
	Public Property Pres3() As String
		Get
			Return _sPres3
		End Get
		Set(ByVal value As String)
			_sPres3 = value
		End Set
	End Property
	Public Property Pres4() As String
        Get
            Return _sPres4
        End Get
        Set(ByVal value As String)
            _sPres4 = value
        End Set
    End Property
	Private _esAnyo As Boolean
	Public Property EsAnyo() As Boolean
		Get
			Return _esAnyo
		End Get
		Set(ByVal value As Boolean)
			_esAnyo = value
		End Set
	End Property
	Private _anyo As Integer
	Public Property Anyo() As Integer
		Get
			Return _anyo
		End Get
		Set(ByVal value As Integer)
			_anyo = value
		End Set
	End Property
#End Region

#Region " Eventos página "
	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If _oParentPage Is Nothing Then
            _oParentPage = CType(Me.Page, FSNPage)
        End If
        If Page.IsPostBack Then
            Select Case Request("__EVENTTARGET")
                Case hidEvent.UniqueID
                    Dim oDatos As Object
                    Dim serializer As New Script.Serialization.JavaScriptSerializer
                    oDatos = serializer.Deserialize(Of Dictionary(Of String, String))(Request("__EVENTARGUMENT"))
                    SelectionChanged(oDatos)
            End Select
            Exit Sub
        Else
            MostrarHistorico()
        End If
    End Sub

    ''' <summary>
    ''' Obtiene los datos del control
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load; Tiempo máximo:0seg.</remarks>
    ''' 
    Private Sub ObtenerDatos()
        'Denominación de la partida
        Dim oRoot As Fullstep.FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oPPres As Fullstep.FSNServer.PartidasPRES5 = oRoot.Get_Object(GetType(Fullstep.FSNServer.PartidasPRES5))
        Dim sPPresDen As String = oPPres.DenominacionPartidaPRES5(_oParentPage.FSNUser.Idioma, _sPres0, _sPres1, _sPres2, _sPres3, _sPres4)
        If sPPresDen Is Nothing Then sPPresDen = String.Empty
        CType(Me.Page, FSNPage).InsertarEnCache("sPPresDen_" & _oParentPage.FSNUser.Cod, sPPresDen)

		'Histórico de la partida
		Dim dtHistoricoPPres As DataTable = oPPres.HistoricoPartida(_sPres0, _sPres1, _sPres2, _sPres3, _sPres4, Anyo)
		CType(Me.Page, FSNPage).InsertarEnCache("dtHistorico_" & _oParentPage.FSNUser.Cod, dtHistoricoPPres)

		'Obtener permisos del usuario sobre el centro de coste de la partida        
		Dim dtPermisos As DataTable = oPPres.PermisosUsuCCPartida(_oParentPage.FSNUser.Cod, _sPres0, _sPres1, _sPres2, _sPres3, _sPres4)
        CType(Me.Page, FSNPage).InsertarEnCache("dtPermisos_" & _oParentPage.FSNUser.Cod, dtPermisos)

        Dim bConsulta As Boolean = False
        Dim bPresup As Boolean = False

        If Not dtPermisos Is Nothing AndAlso dtPermisos.Rows.Count > 0 Then
            bConsulta = SQLBinaryToBoolean(dtPermisos.Rows(0)("CONSULTA"))
            bPresup = SQLBinaryToBoolean(dtPermisos.Rows(0)("PRESUP"))
        End If

        pConsulta.Value = IIf(bConsulta, "1", "0")
        pPresup.Value = IIf(bPresup, "1", "0")
    End Sub

	''' <summary>
	''' Carga los datos del control
	''' </summary>
	''' <remarks>Llamada desde: Page_Load; Tiempo máximo:0seg.</remarks>
	Public Sub CargarHistorico()
        cmdCancelar.Attributes.Add("onclick", "OcultarCargandoPopUp();return false;")
        cmdCerrarComent.Attributes.Add("onclick", "OcultarCargandoPopUp();return false;")
        cmdAceptar.Attributes.Add("onclick", "OcultarCargandoPopUp();")
        If _oParentPage Is Nothing Then
            _oParentPage = CType(Me.Page, FSNPage)
        End If
        'Asignar textos
        CargarIdiomas()

        'Obtener datos
        ObtenerDatos()

        'Datos de la partida        
        lblPartidaPresupuestaria.Text = CType(Cache("sPPresDen_" & _oParentPage.FSNUser.Cod), String)
        upPartidaPresup.Update()

        'Mostrar el histórico de la partida 
        MostrarHistorico()
    End Sub

    ''' <summary>
    ''' Carga de los elementos con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:CargarHistorico; Tiempo máximo=0,2</remarks>
    Private Sub CargarIdiomas()
        FSNPageHeaderHist.TituloCabecera = Texto(0)
        updTitulo.Update()
    End Sub

	''' <summary>
	''' Carga los datos del histórico en el grid
	''' </summary>
	''' <remarks>Llamada desde:CargarHistorico, uwgHistorico_InitializeDataSource, cmdAceptar_Click; Tiempo máximo=0,2</remarks>
	Private Sub MostrarHistorico()
		_oParentPage = CType(Me.Page, FSNPage)
		With wdgHistorico
			.Rows.Clear()
			.Columns.Clear()
			.DataSource = CType(Cache("dtHistorico_" & _oParentPage.FSNUser.Cod), DataTable)
			If .DataSource Is Nothing Then Exit Sub
			CrearColumnas()
			ConfigurarGrid()
			.DataBind()
		End With
		upHistorico.Update()
		CType(Page, ControlPresupuestario).ActualizarPopUpHistorico()
	End Sub
#End Region

#Region " Grid "
	Private Sub CrearColumnas()
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim nombre As String

		With wdgHistorico
			Dim view As New DataView(.DataSource)
			Dim dt As DataTable
			If Anyo = 0 Then
				dt = view.ToTable(True)
			Else
				dt = view.ToTable(True, "ID", "FECHA", "USU", "PRES", "COMENT", "USU_COMENT")
			End If

			For Each dr As DataColumn In dt.Columns
				campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
				nombre = .DataSource.Columns(dr.ColumnName).ToString

				With campoGrid
					.Key = nombre
					.DataFieldName = nombre
					.Header.Text = nombre
				End With

				.Columns.Add(campoGrid)
			Next
		End With
	End Sub

	Private Sub AnyadirColumna(ByVal fieldname As String)
		Dim unboundField As New Infragistics.Web.UI.GridControls.UnboundField(True)

		unboundField.Key = fieldname
		wdgHistorico.Columns.Add(unboundField)
	End Sub

	Private Sub ConfigurarGrid()
		With wdgHistorico
			'columnas no visibles
			.Columns("ID").Hidden = True
			If Anyo = 0 Then .Columns("MON").Hidden = True
			.Columns("PRES").Hidden = True
			If Anyo = 0 Then .Columns("BAJALOG").Hidden = True
			.Columns("COMENT").Hidden = True
			.Columns("USU_COMENT").Hidden = True

			'columnas añadidas
			Dim pos As Integer
			pos = .Columns.Count
			AnyadirColumna("PRES_MON")
			If Anyo = 0 Then AnyadirColumna("BAJA")
			AnyadirColumna("COMENTARIO")
			.Columns("PRES_MON").Header.Text = Texto(6)
			If Anyo = 0 Then .Columns("BAJA").Header.Text = Texto(7)
			.Columns("COMENTARIO").Header.Text = Texto(8)

			.Columns("FECHA").Header.Text = Texto(2) 'Fecha actualización
			.Columns("FECHA").FormatValue(_oParentPage.FSNUser.DateFormat.ShortDatePattern & " " & _oParentPage.FSNUser.DateFormat.ShortTimePattern) '"g"
			.Columns("FECHA").Width = Unit.Pixel(125)

			If Anyo = 0 Then
				If CType(Session("FSSMPargen"), FSNServer.PargensSMs).Item(Pres0).Plurianual Then .Columns("FECINI").Hidden = True
				.Columns("FECINI").Header.Text = Texto(4) 'Fecha inicio
				.Columns("FECINI").FormatValue(_oParentPage.FSNUser.DateFormat.ShortDatePattern)
				.Columns("FECINI").Width = Unit.Pixel(80)

				If CType(Session("FSSMPargen"), FSNServer.PargensSMs).Item(Pres0).Plurianual Then .Columns("FECFIN").Hidden = True
				.Columns("FECFIN").Header.Text = Texto(5) 'Fecha fin
				.Columns("FECFIN").FormatValue(_oParentPage.FSNUser.DateFormat.ShortDatePattern)
				.Columns("FECFIN").Width = Unit.Pixel(80)
			End If

			.Columns("USU").Header.Text = Texto(3) 'Usuario
			.Columns("USU").Width = Unit.Pixel(60)

			If Anyo = 0 Then
				If Not .Columns("DEN_SPA") Is Nothing Then .Columns("DEN_SPA").Width = Unit.Pixel(95)
				If Not .Columns("DEN_ENG") Is Nothing Then .Columns("DEN_ENG").Width = Unit.Pixel(95)
				If Not .Columns("DEN_GER") Is Nothing Then .Columns("DEN_GER").Width = Unit.Pixel(95)
				If Not .Columns("DEN_FRA") Is Nothing Then .Columns("DEN_FRA").Width = Unit.Pixel(95)

				.Columns("BAJA").Width = Unit.Pixel(40)
			End If

			.Columns("PRES_MON").Width = Unit.Pixel(100)
			.Columns("COMENTARIO").Width = Unit.Pixel(100)
		End With
	End Sub

	''' <summary>
	''' Evento que salta al cargarse cada linea de la grid
	''' Se asigna el aspecto a cada columna
	''' </summary>
	''' <param name="sender">del evento</param>
	''' <param name="e">del evento</param>        
	''' <remarks>Llamada desde=sistema;Tiempo máximo=0seg.</remarks>
	Private Sub wdgHistorico_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wdgHistorico.InitializeRow
        e.Row.Items.FindItemByKey("PRES_MON").Value = FormatNumber(DBNullToDbl(e.Row.DataItem.Item("PRES")), _oParentPage.FSNUser.NumberFormat) & e.Row.DataItem.Item("MON")

        Dim strComent As String = DBNullToStr(e.Row.Items.FindItemByKey("COMENT").Value)
		If String.IsNullOrEmpty(strComent) Then
			Dim bPresup As Boolean = False
			Dim dtPermisos As DataTable = CType(Cache("dtPermisos_" & _oParentPage.FSNUser.Cod), DataTable)
			If Not dtPermisos Is Nothing AndAlso dtPermisos.Rows.Count > 0 Then
				bPresup = SQLBinaryToBoolean(dtPermisos.Rows(0)("PRESUP"))
			End If

			If bPresup Then
				e.Row.Items.FindItemByKey("COMENTARIO").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/Historico.gif' style='text-align:center;'/>"
			End If
		Else
			If strComent.Length > 12 Then
                strComent = strComent.Substring(0, 10) & " ..."
                strComent = strComent.Replace(Environment.NewLine, " ")
                e.Row.Items.FindItemByKey("COMENTARIO").Value = strComent
            Else
                e.Row.Items.FindItemByKey("COMENTARIO").Value = strComent
            End If
        End If

        If e.Row.Items.FindItemByKey("BAJALOG").Value Then
            e.Row.Items.FindItemByKey("BAJA").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/baja.gif' style='text-align:center;'/>"
        End If
    End Sub

    Private Sub SelectionChanged(ByVal datos As Object)
        Dim bConsulta As Boolean = False
        Dim bPresup As Boolean = False

		If pConsulta.Value = "1" Then bConsulta = True

		If pPresup.Value = "1" Then bPresup = True

		'Si no se ha introducido comentarios y no se tienen permisos de presupuestación no se puede introducir nuevos comentarios
		Dim strComent As String = DBNullToStr(datos("Coment"))
        MostrarPanelComentario(datos("Id"), datos("Fecha"), strComent, DBNullToStr(datos("UsuComent")), bConsulta, bPresup)

    End Sub

#End Region

#Region " Eventos controles "

    ''' <summary>
    ''' Evento que salta al hacer click sobre el botón de aceptar los cambios en el comentario
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde=sistema;Tiempo máximo=0seg.</remarks>
    Private Sub cmdAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        Dim iID As Integer = CType(lblID.Text, Integer)

        'Actualizar el comentario
        Dim oRoot As Fullstep.FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oPres5 As Fullstep.FSNServer.PartidasPRES5 = oRoot.Get_Object(GetType(Fullstep.FSNServer.PartidasPRES5))
        oPres5.ActualizarComentHistorico(iID, txtComent.Text, _oParentPage.FSNUser.Cod)

        'Dim oPPres As Fullstep.FSNServer.PartidasPRES5 = oRoot.Get_Object(GetType(Fullstep.FSNServer.PartidasPRES5))
        'Dim dtHistoricoPPres As DataTable = oPPres.HistoricoPartida(_sPres0, _sPres1, _sPres2, _sPres3, _sPres4)
        'Cache("dtHistorico_" & _oParentPage.FSNUser.Cod) = dtHistoricoPPres
        Dim dtHistorico As DataTable = CType(Cache("dtHistorico_" & _oParentPage.FSNUser.Cod), DataTable)
        Dim dvHistorico As DataView = New DataView(dtHistorico)
        dvHistorico.Sort = "ID"
        Dim iIndex As Integer = dvHistorico.Find(iID)
        If iIndex >= 0 Then
            dvHistorico(iIndex)("COMENT") = txtComent.Text
        End If

        'Cerrar la ventana
        mpeComent.Hide()

        'Actualizar histórico
        MostrarHistorico()
    End Sub

#End Region

#Region " Panel comentario "

    ''' <summary>
    ''' Muestra el panel de comentario   
    ''' </summary>
    ''' <param name="ID">ID del comentario</param> 
    ''' <param name="Fecha">Fecha de la modificación</param>
    ''' <param name="Comentario">Valor de la columna comentario</param>   
    ''' <param name="UsuComent">Usuario que ha hecho el comentario</param>
    ''' <param name="bConsulta">Permiso de consulta</param>
    ''' <param name="bPresup">Permiso de presupuestación</param>    
    ''' <remarks>Llamada desde=Historico_OnComentClick;Tiempo máximo=0seg.</remarks>
    Private Sub MostrarPanelComentario(ByVal ID As Integer, ByVal Fecha As Date, ByVal Comentario As String, ByVal UsuComent As String, ByVal bConsulta As Boolean, ByVal bPresup As Boolean)
        _bModoEdicion = (bConsulta And bPresup)

        CargarIdiomasComentario()
        CargarDatosComentario(ID, Fecha, Comentario, UsuComent, bConsulta, bPresup)

        pnConsulta.Visible = Not _bModoEdicion
        pnEdicion.Visible = _bModoEdicion

        upInfoComent.Update()
        upComent.Update()
        upBotones.Update()

        mpeComent.Show()
    End Sub

    ''' <summary>
    ''' Carga de los elementos de la pantalla de info de Comentario con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:Historico_OnComentClick; Tiempo máximo=0,2</remarks>
    Private Sub CargarIdiomasComentario()
        cmdCerrarComent.Text = Texto(1)
        cmdAceptar.Text = Texto(12)
        cmdCancelar.Text = Texto(13)
    End Sub

    ''' <summary>
    ''' Obtiene los datos del usuario
    ''' </summary>
    ''' <param name="ID">ID del comentario</param> 
    ''' <param name="Fecha">Fecha de la modificación</param>
    ''' <param name="Comentario">Valor de la columna comentario</param>   
    ''' <param name="UsuComent">Usuario que ha hecho el comentario</param>
    ''' <param name="bConsulta">Permiso de consulta</param>
    ''' <param name="bPresup">Permiso de presupuestación</param> 
    ''' <remarks>Llamada desde: Historico_OnComentClick; Tiempo máximo:0seg.</remarks>
    Private Sub CargarDatosComentario(ByVal ID As Integer, ByVal Fecha As Date, ByVal Comentario As String, ByVal UsuComent As String, ByVal bConsulta As Boolean, ByVal bPresup As Boolean)
        lblID.Text = ID
        lblInfoComent.Text = ComponerInfoComent(Fecha, UsuComent)

        txtComent.Visible = _bModoEdicion
        lblComent.Visible = Not _bModoEdicion

        txtComent.Text = IIf(_bModoEdicion, Comentario, String.Empty)
        lblComent.Text = IIf(Not _bModoEdicion, Comentario, String.Empty)
    End Sub

    ''' <summary>
    ''' Obtiene los datos del usuario
    ''' </summary>
    ''' <param name="Fecha">Fecha de la modificación</param>
    ''' <param name="Usu">Usuario que ha hecho el comentario</param>
    ''' <remarks>Llamada desde: Historico_OnComentClick; Tiempo máximo:0seg.</remarks>
    Private Function ComponerInfoComent(ByVal Fecha As Date, ByVal Usu As String) As String
        Dim strInfo As String = Texto(10)

        strInfo &= " " & Fecha.ToString("g")

        If Not Usu Is Nothing AndAlso Usu <> "" Then
            strInfo &= " (" & Texto(11) & " "

            Dim oRoot As Fullstep.FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oUsr As Fullstep.FSNServer.User = oRoot.Get_Object(GetType(Fullstep.FSNServer.User))
            Dim dtPersona As DataTable = oUsr.DevolverPersona(Usu)
            If Not dtPersona Is Nothing AndAlso dtPersona.Rows.Count > 0 Then
                Dim drPer As DataRow = dtPersona.Rows(0)
                strInfo &= DBNullToStr(drPer("NOM")) & " " & DBNullToStr(drPer("APE"))
            End If
            strInfo &= "):"
        End If

        Return strInfo
    End Function

#End Region

    Private Function Texto(ByVal iTexto As Integer) As String
        If _Textos Is Nothing Then
            _Textos = CType(HttpContext.Current.Cache("Textos_" & _oParentPage.FSNUser.Idioma.ToString() & "_" & _ModuloIdioma), DataSet)
        End If
        If _Textos Is Nothing Then
            Dim oFSNServer As Fullstep.FSNServer.Root = Session("FSN_Server")
            Dim FSSMDict As Dictionary = oFSNServer.Get_Object(GetType(FSNServer.Dictionary))
            FSSMDict.LoadData(_ModuloIdioma, _oParentPage.FSNUser.Idioma)
            _Textos = FSSMDict.Data
            CType(Me.Page, FSNPage).InsertarEnCache("Textos_" & _oParentPage.FSNUser.Idioma.ToString() & "_" & _ModuloIdioma, _Textos)
        End If
        Return _Textos.Tables(0).Rows(iTexto).Item(1)
    End Function

End Class