﻿Imports Fullstep.FSNLibrary
Imports Fullstep.FSNServer

Partial Public Class MantenimientoControlPresupuestacion
	Inherits FSNPage

	Private _UON1 As String
	Public Property UON1() As String
		Get
			Return _UON1
		End Get
		Set(ByVal value As String)
			_UON1 = value
		End Set
	End Property
	Private _UON2 As String
	Public Property UON2() As String
		Get
			Return _UON2
		End Get
		Set(ByVal value As String)
			_UON2 = value
		End Set
	End Property
	Private _UON3 As String
	Public Property UON3() As String
		Get
			Return _UON3
		End Get
		Set(ByVal value As String)
			_UON3 = value
		End Set
	End Property
	Private _UON4 As String
	Public Property UON4() As String
		Get
			Return _UON4
		End Get
		Set(ByVal value As String)
			_UON4 = value
		End Set
	End Property
	Private _PRES0 As String
	Public Property PRES0() As String
		Get
			Return _PRES0
		End Get
		Set(ByVal value As String)
			_PRES0 = value
		End Set
	End Property
	Private _PRES1 As String
	Public Property PRES1() As String
		Get
			Return _PRES1
		End Get
		Set(ByVal value As String)
			_PRES1 = value
		End Set
	End Property
	Private _PRES2 As String
	Public Property PRES2() As String
		Get
			Return _PRES2
		End Get
		Set(ByVal value As String)
			_PRES2 = value
		End Set
	End Property
	Private _PRES3 As String
	Public Property PRES3() As String
		Get
			Return _PRES3
		End Get
		Set(ByVal value As String)
			_PRES3 = value
		End Set
	End Property
	Private _PRES4 As String
	Public Property PRES4() As String
		Get
			Return _PRES4
		End Get
		Set(ByVal value As String)
			_PRES4 = value
		End Set
	End Property
	Private _PermisoAlta As Boolean
	Public Property PermisoAlta() As Boolean
		Get
			Return _PermisoAlta
		End Get
		Set(ByVal value As Boolean)
			_PermisoAlta = value
		End Set
	End Property
	Private _PermisoModificacion As Boolean
	Public Property PermisoModificacion() As Boolean
		Get
			Return _PermisoModificacion
		End Get
		Set(ByVal value As Boolean)
			_PermisoModificacion = value
		End Set
	End Property
	Private _PermisoPresupuestacion As Boolean
	Public Property PermisoPresupuestacion() As Boolean
		Get
			Return _PermisoPresupuestacion
		End Get
		Set(ByVal value As Boolean)
			_PermisoPresupuestacion = value
		End Set
	End Property
	Private _SituacionAltaPartida As Boolean
	Public Property SituacionAltaPartida() As Boolean
		Get
			Return _SituacionAltaPartida
		End Get
		Set(ByVal value As Boolean)
			_SituacionAltaPartida = value
		End Set
	End Property
	Private _esAnyo As Boolean
	Public Property EsAnyo() As Boolean
		Get
			Return _esAnyo
		End Get
		Set(ByVal value As Boolean)
			_esAnyo = value
		End Set
	End Property
	Private _anyo As Integer
	Public Property Anyo() As Integer
		Get
			Return _anyo
		End Get
		Set(ByVal value As Integer)
			_anyo = value
		End Set
	End Property
	Private _esPlurianual As Boolean
	Public Property EsPlurianual() As Boolean
		Get
			Return _esPlurianual
		End Get
		Set(ByVal value As Boolean)
			_esPlurianual = value
		End Set
	End Property
	Private _nivel As Integer
	Public Property Nivel() As Integer
		Get
			Return _nivel
		End Get
		Set(ByVal value As Integer)
			_nivel = value
		End Set
	End Property
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		ModuloIdioma = TiposDeDatos.ModulosIdiomas.MantenimientoControlPresupuestacion

		SituacionAltaPartida = Not Request.QueryString("alta") Is Nothing
		EsAnyo = CType(Request.QueryString("esAnyo"), Boolean)
		EsPlurianual = CType(Request.QueryString("esplurianual"), Boolean)
		btnAceptar.CommandArgument = If(EsAnyo, 1, 0) & "|·#·|" & If(EsPlurianual, 1, 0)

		CargarPermisos()
		If Not Page.IsPostBack Then
			btnCancelar.Attributes.Add("onclick", "window.close();return false;")
			ftCreditoFinal.ValidChars = FSNUser.DecimalFmt

			CargarTextosPantallaYLongitudes()

			'si Not Request.QueryString("alta") Is Nothing estamos en un alta
			'Actualizamos la propiedad SituacionAltaPartida para saber si estamos en el caso
			'de un alta de una partida o en situación de modificación
			If SituacionAltaPartida Then
				pnlPartida.Visible = Not EsAnyo
				pnlAnyo.Visible = EsAnyo
				lblAnyo.Visible = False
				lblCodigo.Visible = False
				pnlDenominacion.Visible = False
				pnlBajaLogica.Visible = False
				pnlPresupuestacion.Visible = PermisoPresupuestacion
				pnlDenominaciones.Visible = Not EsAnyo
				lblLitMoneda.Visible = Not EsAnyo
				cboMonedaPartidaPresupuestaria.Visible = Not EsAnyo
				pnlFechas.Visible = Not EsPlurianual
				pnlObservaciones.Visible = False
				If Not EsAnyo Then ObtenerIdiomasAplicacionYMonedas()
			Else
				pnlPartida.Visible = Not EsAnyo
				pnlAnyo.Visible = EsAnyo
				txtAnyo.Visible = False
				txtCodigo.Visible = False
				pnlPresupuestacion.Visible = PermisoPresupuestacion
				pnlDenominacion.Visible = Not (PermisoModificacion AndAlso EsAnyo)
				pnlDenominaciones.Visible = PermisoModificacion AndAlso Not EsAnyo
				pnlBajaLogica.Visible = Not EsAnyo
				lblLitMoneda.Visible = Not EsAnyo
				cboMonedaPartidaPresupuestaria.Visible = Not EsAnyo
				pnlFechas.Visible = Not EsPlurianual
				pnlObservaciones.Visible = True

				If EsAnyo Then
					ObtenerDatosBaseDatosAnyo(Request.QueryString("id"), Anyo)
				Else
					ObtenerDatosBaseDatos()
				End If
			End If
		End If
	End Sub

	''' <summary>
	''' 
	''' </summary>
	''' <remarks></remarks>
	Private Sub CargarTextosPantallaYLongitudes()
		lblLitCodigo.Text = IIf(PermisoPresupuestacion, "(*)" & Textos(0), Textos(0)) & ":"

		lblLitDenominacion.Text = IIf(PermisoPresupuestacion, "(*)" & Textos(1), Textos(1)) & ":"
		lblLitDenominaciones.Text = IIf(PermisoPresupuestacion, "(*)" & Textos(1), Textos(1))

		FecInicio.NullText = ""
		FecFin.NullText = ""
		lblLitFecInicio.Text = Textos(2)
		lblLitFecFin.Text = Textos(3)

		lblLitCreditoFinal.Text = Textos(4)
		lblLitMoneda.Text = Textos(5)

		chkDarBajaLogica.Text = Textos(6)

		lblLitObservaciones.Text = Textos(7)

		btnAceptar.Text = Textos(8)
		btnCancelar.Text = Textos(9)

		btnOk.Text = Textos(10)

		Dim oPartidaPres5 As PartidasPRES5 = Me.FSNServer.Get_Object(GetType(PartidasPRES5))
		Dim dtLongitudesCodigos As DataTable = oPartidaPres5.ObtenerLongitudesCodigos
		With dtLongitudesCodigos
			If PRES1 Is Nothing Then
				txtCodigo.MaxLength = .Select("NOMBRE='PRES5_1'")(0).Item("LONGITUD")
			ElseIf PRES2 Is Nothing Then
				txtCodigo.MaxLength = .Select("NOMBRE='PRES5_2'")(0).Item("LONGITUD")
			ElseIf PRES3 Is Nothing Then
				txtCodigo.MaxLength = .Select("NOMBRE='PRES5_3'")(0).Item("LONGITUD")
			ElseIf PRES4 Is Nothing Then
				txtCodigo.MaxLength = .Select("NOMBRE='PRES5_4'")(0).Item("LONGITUD")
			End If
		End With

		lblLitAnyo.Text = Textos(20) & ":"
	End Sub

	''' <summary>
	''' 
	''' </summary>
	''' <remarks></remarks>
	Private Sub CargarPermisos()
		Dim UONs() As String = Split(Request.QueryString("uons"), ";;")
		UON1 = UONs(0)
		UON2 = UONs(1)
		UON3 = UONs(2)
		UON4 = UONs(3)
		PRES0 = Request.QueryString("pres0")
		PRES0 = IIf(PRES0 = "", Nothing, PRES0)
		PRES1 = Request.QueryString("pres1")
		PRES1 = IIf(PRES1 = "", Nothing, PRES1)
		PRES2 = Request.QueryString("pres2")
		If String.IsNullOrEmpty(PRES2) Then
			PRES2 = Nothing
			If EsAnyo AndAlso Anyo = 0 AndAlso Not SituacionAltaPartida Then
				Anyo = PRES1
				PRES1 = Nothing
			End If
		End If
		PRES3 = Request.QueryString("pres3")
		If String.IsNullOrEmpty(PRES3) Then
			PRES3 = Nothing
			If EsAnyo AndAlso Anyo = 0 AndAlso Not SituacionAltaPartida Then
				Anyo = PRES2
				PRES2 = Nothing
			End If
		End If
		PRES4 = Request.QueryString("pres4")
		If String.IsNullOrEmpty(PRES4) Then
			PRES4 = Nothing
			If EsAnyo AndAlso Anyo = 0 AndAlso Not SituacionAltaPartida Then
				Anyo = PRES3
				PRES3 = Nothing
			End If
		End If

		Nivel = If(Not PRES4 Is Nothing, 4, If(Not PRES3 Is Nothing, 3, If(Not PRES2 Is Nothing, 2, 1)))
		Dim Permisos() As String = Split(Request.QueryString("permisos"), ";;")
		PermisoAlta = IIf(Permisos(0) = "", False, True)
		PermisoModificacion = IIf(Permisos(1) = "", False, True)
		PermisoPresupuestacion = IIf(Permisos(2) = "", False, True) AndAlso ((EsPlurianual AndAlso EsAnyo) _
			OrElse (Not EsPlurianual AndAlso Nivel = CType(Session("FSSMPargen"), FSNServer.PargensSMs).Item(PRES0).ImputacionNivel))

	End Sub

	''' <summary>
	''' 
	''' </summary>
	''' <remarks></remarks>
	Private Sub ObtenerDatosBaseDatos()
		Dim oPartidaPresupuestaria As PartidasPRES5 = Me.FSNServer.Get_Object(GetType(PartidasPRES5))
		Dim rDatosPartidaPRES5 As DataSet
		rDatosPartidaPRES5 = oPartidaPresupuestaria.DevolverDatosPartidaPRES5(PRES0, PRES1, PRES2, PRES3, PRES4,
															 IIf(PRES2 Is Nothing, True, False))

		With rDatosPartidaPRES5.Tables(1).Rows(0)
			lblCodigo.Text = .Item("COD")
			txtCodigo.Text = .Item("COD")

			rptDenominaciones.DataSource = rDatosPartidaPRES5.Tables(0)
			rptDenominaciones.DataBind()

			'Pasar el cultureinfo a una propiedad del usuario
			FecInicio.Value = DBNullToSomething(rDatosPartidaPRES5.Tables(1).Rows(0)("FECINI"))
			'Pasar el cultureinfo a una propiedad del usuario
			FecFin.Value = DBNullToSomething(rDatosPartidaPRES5.Tables(1).Rows(0)("FECFIN"))


			txtCreditoFinal.Text = .Item("PRES").ToString

			If .Item("MON") Is DBNull.Value AndAlso PRES2 Is Nothing AndAlso PermisoPresupuestacion Then
				With cboMonedaPartidaPresupuestaria
					.DataValueField = "COD"
					.DataTextField = "COD"

					.DataSource = rDatosPartidaPRES5.Tables(2)
					.DataBind()

					lblLitMoneda.Visible = True
					cboMonedaPartidaPresupuestaria.Visible = True
					cboMonedaPartidaPresupuestaria.SelectedValue = rDatosPartidaPRES5.Tables(3).Rows(0)("MONCEN")
				End With
			Else
				lblLitMoneda.Visible = False
				cboMonedaPartidaPresupuestaria.Visible = False
			End If

			chkDarBajaLogica.Checked = CType(.Item("BAJALOG"), Boolean)
		End With
	End Sub

	Private Sub ObtenerDatosBaseDatosAnyo(ByVal IdPartida As Integer, ByVal Anyo As Integer)
		Dim oPartidaPresupuestaria As PartidasPRES5 = FSNServer.Get_Object(GetType(PartidasPRES5))

		Dim dr As DataRow = oPartidaPresupuestaria.DevolverDatosPartidaPRES5Anyo(IdPartida, Anyo)
		lblAnyo.Text = dr("ANYO")
		txtAnyo.Text = dr("ANYO")
		txtCreditoFinal.Text = dr("PRES")
	End Sub

	''' <summary>
	''' Obtiene la información de la partida presupuestaria a mostrar. Denominaciones y en su caso las monedas
	''' </summary>
	''' <remarks></remarks>
	Private Sub ObtenerIdiomasAplicacionYMonedas()
		Dim oIdiomas As FSNServer.Idiomas = FSNServer.Get_Object(GetType(FSNServer.Idiomas))

		rptDenominaciones.DataSource = oIdiomas.Get_IdiomasActivos
		rptDenominaciones.DataBind()

		If PRES1 Is Nothing AndAlso PermisoPresupuestacion Then
			lblLitMoneda.Visible = True
			cboMonedaPartidaPresupuestaria.Visible = True

			Dim oMonedas As Monedas = FSNServer.Get_Object(GetType(Monedas))

			With cboMonedaPartidaPresupuestaria
				.DataValueField = "COD"
				.DataTextField = "COD"

				Dim dtMonedas As DataTable = oMonedas.Get_Monedas(FSNUser.Idioma).Tables(0)
				dtMonedas.DefaultView.Sort = "COD ASC"

				.DataSource = dtMonedas
				.DataBind()

				cboMonedaPartidaPresupuestaria.SelectedValue = oMonedas.Get_MonedaCentral(FSNUser.Idioma)("COD")
			End With
		Else
			lblLitMoneda.Visible = False
			cboMonedaPartidaPresupuestaria.Visible = False
		End If

	End Sub

	Private Sub rptDenominaciones_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptDenominaciones.ItemDataBound
		With e.Item
			If CType(.FindControl("lblCodIdioma"), Label).Text = FSNUser.Idioma Then
				CType(.FindControl("txtDenominacion"), TextBox).CausesValidation = True
			End If
		End With
	End Sub

	Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
		Dim EsAnyo As Boolean = CType(Split(btnAceptar.CommandArgument, "|·#·|")(0), Boolean)
		If ComprobacionesGuardado(EsAnyo) Then 'Utilizamos el commandargument para saber si es el alta de un año o de una partida (esanyo|·#·|esplurianual)
			Dim dtDenominaciones As New DataTable
			Dim rDenominacion As DataRow

			dtDenominaciones.Columns.Add("ID", System.Type.GetType("System.Int64"))
			dtDenominaciones.Columns.Add("PRES0", System.Type.GetType("System.String"))
			dtDenominaciones.Columns.Add("PRES1", System.Type.GetType("System.String"))
			dtDenominaciones.Columns.Add("PRES2", System.Type.GetType("System.String"))
			dtDenominaciones.Columns.Add("PRES3", System.Type.GetType("System.String"))
			dtDenominaciones.Columns.Add("PRES4", System.Type.GetType("System.String"))
			dtDenominaciones.Columns.Add("COD", System.Type.GetType("System.String"))
			dtDenominaciones.Columns.Add("DEN", System.Type.GetType("System.String"))
			dtDenominaciones.Columns.Add("ALTA", System.Type.GetType("System.Int16"))

			Dim InsertOK As Integer = 1
			Dim CreditoFinal As Double

			If SituacionAltaPartida Then
				If Not EsAnyo Then
					If PRES1 Is Nothing Then
						PRES1 = txtCodigo.Text
					ElseIf PRES2 Is Nothing Then
						PRES2 = txtCodigo.Text
					ElseIf PRES3 Is Nothing Then
						PRES3 = txtCodigo.Text
					ElseIf PRES4 Is Nothing Then
						PRES4 = txtCodigo.Text
					End If
					For Each Denominacion As RepeaterItem In rptDenominaciones.Items
						If CType(Denominacion.FindControl("txtDenominacion"), TextBox).Text <> "" Then
							rDenominacion = dtDenominaciones.NewRow
							rDenominacion("PRES0") = PRES0
							rDenominacion("PRES1") = PRES1
							rDenominacion("PRES2") = PRES2
							rDenominacion("PRES3") = PRES3
							rDenominacion("PRES4") = PRES4
							rDenominacion("COD") = CType(Denominacion.FindControl("lblCodIdioma"), Label).Text
							rDenominacion("DEN") = CType(Denominacion.FindControl("txtDenominacion"), TextBox).Text
							rDenominacion("ALTA") = 1

							dtDenominaciones.Rows.Add(rDenominacion)
						End If
					Next
				End If

				CreditoFinal = strToDbl(txtCreditoFinal.Text)
				Dim oPartidaPresupuestaria As PartidasPRES5 = Me.FSNServer.Get_Object(GetType(PartidasPRES5))
				If EsAnyo Then
					InsertOK = oPartidaPresupuestaria.AgregarAnyoPartidaPresupuestaria(Request.QueryString("id"), PRES0, PRES1, PRES2, PRES3, PRES4, txtAnyo.Text, CreditoFinal, FSNUser.Cod)
				Else
					InsertOK = oPartidaPresupuestaria.AgregarPartidaPresupuestaria(UON1, UON2, UON3, UON4,
																				PRES0, PRES1, PRES2, PRES3, PRES4,
																				dtDenominaciones, DBNullToSomething(FecInicio.Value), DBNullToSomething(FecFin.Value),
																				IIf(cboMonedaPartidaPresupuestaria.SelectedValue = "", Nothing, cboMonedaPartidaPresupuestaria.SelectedValue),
																				IIf(txtCreditoFinal.Text = String.Empty, Nothing, CreditoFinal),
																				FSNUser.Cod)
				End If
			Else
				If Not EsAnyo Then
					For Each Denominacion As RepeaterItem In rptDenominaciones.Items
						If CType(Denominacion.FindControl("txtDenominacion"), TextBox).Text <> "" Then
							rDenominacion = dtDenominaciones.NewRow
							rDenominacion("PRES0") = PRES0
							rDenominacion("PRES1") = PRES1
							rDenominacion("PRES2") = PRES2
							rDenominacion("PRES3") = PRES3
							rDenominacion("PRES4") = PRES4
							rDenominacion("COD") = CType(Denominacion.FindControl("lblCodIdioma"), Label).Text
							rDenominacion("DEN") = CType(Denominacion.FindControl("txtDenominacion"), TextBox).Text
							rDenominacion("ALTA") = 0

							dtDenominaciones.Rows.Add(rDenominacion)
						End If
					Next

				End If

				CreditoFinal = strToDbl(txtCreditoFinal.Text)
				Dim oPartidaPresupuestaria As PartidasPRES5 = Me.FSNServer.Get_Object(GetType(PartidasPRES5))
				If EsAnyo Then
					oPartidaPresupuestaria.ModificarAnyoPartidaPresupuestaria(Request.QueryString("id"), PRES0, PRES1, PRES2, PRES3, PRES4, Anyo, CreditoFinal, txtObservaciones.Text, FSNUser.Cod)
				Else
					oPartidaPresupuestaria.ModificarPartidaPresupuestaria(UON1, UON2, UON3, UON4,
																			PRES0, PRES1, PRES2, PRES3, PRES4, chkDarBajaLogica.Checked,
																			dtDenominaciones, DBNullToSomething(FecInicio.Value), DBNullToSomething(FecFin.Value),
																			IIf(cboMonedaPartidaPresupuestaria.SelectedValue = "", Nothing, cboMonedaPartidaPresupuestaria.SelectedValue),
																			IIf(txtCreditoFinal.Text = String.Empty, Nothing, CreditoFinal),
																			txtObservaciones.Text, FSNUser.Cod)
				End If
			End If

			Select Case InsertOK
				Case 0
					lblError.Text = Textos(19)
					upError.Update()
					mpError.Show()
				Case 1
					If Not (Page.ClientScript.IsStartupScriptRegistered("CloseWindow")) Then
						Page.ClientScript.RegisterStartupScript(System.Type.GetType("System.String"), "CloseWindow", "<script>window.opener.ActualizarGrid();window.close();</script>")
					End If
				Case 2
					lblError.Text = Textos(18)
					upError.Update()
					mpError.Show()
			End Select
		End If
	End Sub

	''' <summary>
	''' Valida los datos necesarios para guardar los cambios de la partida presupuestaria.
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	Private Function ComprobacionesGuardado(ByVal EsAnyo As Boolean) As Boolean
		Dim ComprobacionesCorrectas As Integer = 0
		Dim sIdiomaDenominacion As String = ""

		If EsAnyo Then
			If String.IsNullOrEmpty(txtAnyo.Text) Then ComprobacionesCorrectas = 8
			If PermisoPresupuestacion AndAlso ComprobacionesCorrectas = 0 AndAlso String.IsNullOrEmpty(txtCreditoFinal.Text) Then
				ComprobacionesCorrectas = 6
			End If
		Else
			If PermisoAlta AndAlso String.IsNullOrEmpty(txtCodigo.Text) Then ComprobacionesCorrectas = 1

			If (PermisoModificacion Or PermisoAlta) AndAlso ComprobacionesCorrectas = 0 Then
				For Each rItem As RepeaterItem In rptDenominaciones.Items
					With CType(rItem.FindControl("txtDenominacion"), TextBox)
						If .CausesValidation AndAlso .Text = "" Then
							sIdiomaDenominacion = CType(rItem.FindControl("lblIdioma"), Label).Text
							ComprobacionesCorrectas = 2
						End If
					End With
				Next
			End If

			If PermisoPresupuestacion AndAlso ComprobacionesCorrectas = 0 Then
				If Not String.IsNullOrEmpty(txtCreditoFinal.Text) AndAlso Not IsNumeric(Replace(txtCreditoFinal.Text, FSNUser.DecimalFmt, Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator)) Then
					ComprobacionesCorrectas = 6
				End If
			End If
		End If

		Select Case ComprobacionesCorrectas
			Case 1
				lblError.Text = Textos(11)
			Case 2
				lblError.Text = Textos(12) & sIdiomaDenominacion
			Case 3
				lblError.Text = Textos(13)
			Case 4
				lblError.Text = Textos(14)
			Case 5
				lblError.Text = Textos(15)
			Case 6
				lblError.Text = Textos(16)
			Case 7
				lblError.Text = Textos(17)
			Case 8
				lblError.Text = "dError año" 'Textos(17)
			Case Else
				Return True
		End Select

		upError.Update()
		mpError.Show()

		Return False
	End Function
End Class