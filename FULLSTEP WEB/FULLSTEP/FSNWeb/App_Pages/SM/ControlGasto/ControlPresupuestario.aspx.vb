﻿Imports System.Web.Script.Serialization
Imports Fullstep.FSNLibrary
Imports Fullstep.FSNServer
Imports Infragistics.Web.UI
Imports Infragistics.Web.UI.GridControls

Partial Public Class ControlPresupuestario
	Inherits FSNPage
	Private exportedRows As Integer = 0
	Dim dsResult As DataSet
	Dim den1, den2, den3, den4 As String

#Region "Propiedades Cache"
	Private _PermisoAltaPartidaPresupuestaria As Boolean
	Public Property PermisoAltaPartidaPresupuestaria() As Boolean
		Get
			_PermisoAltaPartidaPresupuestaria = CType(Cache("oPermisoAltaPartidaPresupuestaria_" & FSNUser.Cod), Boolean)
			Return _PermisoAltaPartidaPresupuestaria
		End Get
		Set(ByVal value As Boolean)
			_PermisoAltaPartidaPresupuestaria = value
			Me.InsertarEnCache("oPermisoAltaPartidaPresupuestaria_" & FSNUser.Cod, _PermisoAltaPartidaPresupuestaria)
		End Set
	End Property
	Private _PermisoModificacionPartidaPresupuestaria As Boolean
	Public Property PermisoModificacionPartidaPresupuestaria() As Boolean
		Get
			_PermisoModificacionPartidaPresupuestaria = CType(Cache("oPermisoModificacionPartidaPresupuestaria_" & FSNUser.Cod), Boolean)
			Return _PermisoModificacionPartidaPresupuestaria
		End Get
		Set(ByVal value As Boolean)
			_PermisoModificacionPartidaPresupuestaria = value
			Me.InsertarEnCache("oPermisoModificacionPartidaPresupuestaria_" & FSNUser.Cod, _PermisoModificacionPartidaPresupuestaria)
		End Set
	End Property
	Private _PermisoPresupuestacionPartidaPresupuestaria As Boolean
	Public Property PermisoPresupuestacionPartidaPresupuestaria() As Boolean
		Get
			_PermisoPresupuestacionPartidaPresupuestaria = CType(Cache("oPermisoPresupuestacionPartidaPresupuestaria_" & FSNUser.Cod), Boolean)
			Return _PermisoPresupuestacionPartidaPresupuestaria
		End Get
		Set(ByVal value As Boolean)
			_PermisoPresupuestacionPartidaPresupuestaria = value
			Me.InsertarEnCache("oPermisoPresupuestacionPartidaPresupuestaria_" & FSNUser.Cod, _PermisoPresupuestacionPartidaPresupuestaria)
		End Set
	End Property
	Private _oPartidasPresupuestarias As DataSet
	''' <summary>
	''' Devuelve las Partidas Presupuestarias
	''' </summary>
	''' <param name="bRecargarDatos">True->Mete en cache las Partidas Presupuestarias</param>
	''' <returns>Partidas Presupuestarias</returns>
	''' <remarks>Llamada desde:  FechaMax     AplicarOpcionesBusqueda      InitializeGrid      ExportarExcel; Tiempo máximo: 0,2</remarks>
	Protected ReadOnly Property PartidasPresupuestarias(Optional ByVal bRecargarDatos As Boolean = False) As DataSet
		Get
			Dim sValoresCentroCoste() As String = Split(hidCC.Value, "#")
			Dim sUON1, sUON2, sUON3, sUON4 As String
			sUON1 = Nothing : sUON2 = Nothing : sUON3 = Nothing : sUON4 = Nothing
			sUON1 = sValoresCentroCoste(0)
			If UBound(sValoresCentroCoste) >= 1 Then sUON2 = sValoresCentroCoste(1)
			If UBound(sValoresCentroCoste) >= 2 Then sUON3 = sValoresCentroCoste(2)
			If UBound(sValoresCentroCoste) >= 3 Then sUON4 = sValoresCentroCoste(3)

			If _oPartidasPresupuestarias Is Nothing Then
				If Me.IsPostBack Then
					_oPartidasPresupuestarias = CType(Cache("oPartidasPresupuestarias_" & FSNUser.Cod), DataSet)

					If _oPartidasPresupuestarias Is Nothing Then
						bRecargarDatos = True
					Else
						If Not _oPartidasPresupuestarias.Tables("UONS") Is Nothing Then
							With _oPartidasPresupuestarias.Tables("UONS").Rows(0)
								If .Item("UON1") <> sUON1 OrElse .Item("UON2") <> sUON2 _
								OrElse .Item("UON3") <> sUON3 OrElse .Item("UON4") <> sUON4 Then
									bRecargarDatos = True
								End If
							End With
						Else
							bRecargarDatos = True
						End If
					End If
				Else
					bRecargarDatos = True
				End If
			End If

			If bRecargarDatos Then
				Dim oPres5 As PRES5 = Me.FSNServer.Get_Object(GetType(PRES5))
				_oPartidasPresupuestarias = oPres5.DevolverArbolPartidas(sUON1, sUON2, sUON3, sUON4, FSNUser.Cod, FSNUser.Idioma)

				Me.InsertarEnCache("oPartidasPresupuestarias_" & FSNUser.Cod, _oPartidasPresupuestarias)
			End If

			Return _oPartidasPresupuestarias
		End Get
	End Property
	Private _sFiltroGrid As String
	Public Property sFiltroGrid() As String
		Get
			_sFiltroGrid = CType(Cache("oFiltroGrid_" & FSNUser.Cod), String)
			Return _sFiltroGrid
		End Get
		Set(ByVal value As String)
			_sFiltroGrid = value
			Me.InsertarEnCache("oFiltroGrid_" & FSNUser.Cod, _sFiltroGrid)
		End Set
	End Property
#End Region
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		ModuloIdioma = TiposDeDatos.ModulosIdiomas.PartidasPresupuestarias

		imgExcel.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Excel.png"
		ScriptMgr.EnablePageMethods = True

		If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "PARGEN_SM") Then
			Dim serializer As New JavaScriptSerializer
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PARGEN_SM",
				"var PARGEN_SM=" & serializer.Serialize(CType(Session("FSSMPargen"), FSNServer.PargensSMs)) & ";", True)
		End If

		If Not Page.IsPostBack Then
			'Hacemos que la opción de menu se mantenga seleccionada
			Seccion = "ControlPresupuestario"
			Dim mMaster = CType(Me.Master, FSNWeb.Menu)
			mMaster.Seleccionar("CtrlPresup", "ControlPresupuestario")

			imgCCLupa.Attributes.Add("onclick", "LlamadaBuscadorCC();return false;")
			btnBuscar.Attributes.Add("onclick", "return ComprobarCentroCoste();")
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaTheme", "<script>var rutaTheme = '" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "'</script>")
			CargarRecursos()
			CargarDatosIniciales()
			InitializeGrid()

			For Each pargenSM As PargenSM In CType(Session("FSSMPargen"), FSNServer.PargensSMs)
				If Not pargenSM.Plurianual Then
					upFechas.Visible = True
					Exit For
				End If
			Next
		Else
			Dim RecargarGrid As Boolean = True
			btnBuscar.CommandArgument = ""

			Select Case Request("__EVENTTARGET")
				Case btnBuscar.UniqueID
					btnBuscar.CommandArgument = "1"
					ObtenerPartidasPresupuestarias(True)
					RecargarGrid = False
				Case btnHidden.UniqueID
					ObtenerPartidasPresupuestarias(True)
					RecargarGrid = False
				Case hidEvent.UniqueID
					Dim oDatos As Object
					Dim serializer As New Script.Serialization.JavaScriptSerializer
					oDatos = serializer.Deserialize(Of Dictionary(Of String, String))(Request("__EVENTARGUMENT"))
					SelectionChanged(oDatos)
					RecargarGrid = False
			End Select
			'Voy a recargarGrid si no tengo que volver a sacar el arbol
			If RecargarGrid Then RecargaGrid()
			Select Case Request("__EVENTARGUMENT")
				Case "Excel"
					ExportarExcel()
			End Select
		End If
		RegistrarScripts()
	End Sub
	''' <summary>
	''' Se cargan los textos de los literales de la pantalla
	''' </summary>
	''' <remarks></remarks>
	Private Sub CargarRecursos()
		FSNPageHeader.TituloCabecera = Textos(0)

		lblLitCentroCoste.Text = Textos(1) & ":"
		lblLitBuscarPartida.Text = Textos(2) & ":"
		chkVerBajasLogicas.Text = Textos(3)
		btnBuscar.Text = Textos(4)
		lblLitFechaInicio.Text = Textos(5) & ":"
		lblLitFechaFin.Text = Textos(6) & ":"
		lblExcel.Text = "Excel"
	End Sub
	''' <summary>
	''' Para las redirecciones cargamos variables de script para redirigir
	''' </summary>
	''' <remarks></remarks>
	Private Sub RegistrarScripts()
		Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")
		Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaSM", "<script>var rutaSM = '" & ConfigurationManager.AppSettings("rutaSM") & "' </script>")
		Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "msgErrorCentroCoste", "<script>var msgErrorCentroCoste = '" & JSText(Textos(12)) & "' </script>")
		Dim ModalProgressClientID As String = CType(Master.Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
		Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalClientID", "<script>var ModalProgressClientID = '" & ModalProgressClientID & "' </script>")
	End Sub
	''' <summary>
	''' 
	''' </summary>
	''' <remarks></remarks>
	Private Sub CargarDatosIniciales()
		'Pasar el cultureinfo a una propiedad del usuario
		FecInicio.NullText = ""
		FecFin.NullText = ""

		Dim oCC As FSNServer.Centro_SM = Me.FSNServer.Get_Object(GetType(FSNServer.Centro_SM))
		oCC.DevolverCCUnico(FSNUser.Cod, FSNUser.Idioma)

		If oCC.Codigo <> "" Then
			'Tiene un unico Centro de coste
			lblCentroCoste.Text = oCC.Codigo & " - " & oCC.Denominacion
			lblCentroCoste.Visible = True

			txtCC.Visible = False
			imgCCLupa.Visible = False

			hidCC.Value = oCC.UONS.UON1 & "#" & oCC.UONS.UON2 & "#" & oCC.UONS.UON3 & "#" & oCC.UONS.UON4

			ObtenerPartidasPresupuestarias(True)
		Else
			lblCentroCoste.Visible = False

			txtCC.Visible = True
			imgCCLupa.Visible = True

			txtPartida.Enabled = False
		End If

		oCC = Nothing
	End Sub
	Public Function FechaMax() As DateTime
		Dim dt, dtMax As Nullable(Of DateTime)

		For Each oTable As DataTable In PartidasPresupuestarias.Tables
			If Not oTable.TableName = "UONS" Then
				dt = From Datos In oTable
					 Where Not String.IsNullOrEmpty(Datos.Item("FECFIN").ToString)
					 Select IIf(Datos.Item("FECFIN") IsNot Nothing, CType(Datos.Item("FECFIN"), DateTime), Nothing)
					 Distinct.Max()

				If Not dtMax.HasValue Or dt > dtMax Then
					dtMax = dt
				End If
			End If
		Next

		Return IIf(dtMax.HasValue, dtMax, Nothing)
	End Function
	''' <summary>
	''' Inicializa los permisos del usuario sobre las partidas presupuestarias
	''' </summary>
	''' <param name="bRecargarDatos"></param>
	''' <remarks></remarks>
	Private Sub ObtenerPartidasPresupuestarias(ByVal bRecargarDatos As Boolean)
		sFiltroGrid = ""

		If bRecargarDatos OrElse hidPermisosCargados.Value = "0" Then
			Dim sValoresCentroCoste() As String = Split(hidCC.Value, "#")
			Dim sUON1, sUON2, sUON3, sUON4 As String

			sUON1 = sValoresCentroCoste(0)
			sUON2 = sValoresCentroCoste(1)
			sUON3 = sValoresCentroCoste(2)
			sUON4 = sValoresCentroCoste(3)

			txtPartida_AutoCompleteExtender.ContextKey = sUON1 & "#" & sUON2 & "#" & sUON3 & "#" & sUON4 & "@@" &
															IIf(chkVerBajasLogicas.Checked, "1", "0")

			Dim rCentroCoste As FSNServer.Centro_SM = Me.FSNServer.Get_Object(GetType(FSNServer.Centro_SM))
			Dim rPermisosCentroCoste As DataRow
			rPermisosCentroCoste = rCentroCoste.DevolverPermisosCentroCoste(sUON1, sUON2, sUON3, sUON4, FSNUser.Cod)

			If Not rPermisosCentroCoste Is Nothing Then
				PermisoAltaPartidaPresupuestaria = rPermisosCentroCoste("ALTA")
				PermisoModificacionPartidaPresupuestaria = rPermisosCentroCoste("MODIF")
				PermisoPresupuestacionPartidaPresupuestaria = rPermisosCentroCoste("PRESUP")

				hidPermisosCargados.Value = "1"

				hidPermisosCentroCoste.Value = IIf(PermisoAltaPartidaPresupuestaria, "alta", "") & ";;" &
					IIf(PermisoModificacionPartidaPresupuestaria, "modif", "") & ";;" &
					IIf(PermisoPresupuestacionPartidaPresupuestaria, "presup", "")
			End If
		End If

		AplicarOpcionesBusqueda(bRecargarDatos)
	End Sub
	''' <summary>
	''' Obtiene las partidas presupuestarias para la carga del grid. Controla si lo tiene que hacer de base de datos o de Cache.
	''' </summary>
	''' <param name="bRecargarDatos">Si tiene que coger de la cache o recargar de base de datos</param>
	''' <remarks></remarks>
	Private Sub AplicarOpcionesBusqueda(Optional ByVal bRecargarDatos As Boolean = False)
		'Cargar partidas del año en curso
		Dim dsPartidasPresupuestarias As New DataSet
		dsPartidasPresupuestarias = PartidasPresupuestarias(bRecargarDatos).Copy

		Filtrar(dsPartidasPresupuestarias)
		ActualizarGrid(dsPartidasPresupuestarias)
	End Sub
	''' <summary>
	''' Con las condiciones de la busqueda filtra los resultados para mostrar la información requerida.
	''' </summary>
	''' <param name="dsDatos">El dataset de la información con las partidas presupuestarias</param>
	''' <remarks></remarks>
	Private Sub Filtrar(ByRef dsDatos As DataSet)
		Dim sFiltroTexto As String = String.Empty
		Dim sFiltroFechas As String = String.Empty
		Dim sFiltroBusqueda As String = String.Empty
		Dim sFiltro As String = String.Empty

		If Not chkVerBajasLogicas.Checked Then
			sFiltro = "(BAJALOG=0) "
		End If

		If Not (FecInicio.Value = Date.MinValue AndAlso FecFin.Value = Date.MinValue) Then
			If Not FecInicio.Value Is Nothing And Not FecFin.Value Is Nothing Then
				sFiltroFechas = "(((FECINI >= '" & CType(FecInicio.Value, DateTime).ToString &
									"' AND FECINI <= '" & CType(FecFin.Value, DateTime).ToString &
								"') OR FECINI IS NULL) OR ((FECFIN >= '" & CType(FecInicio.Value, DateTime).ToString &
									"' AND FECFIN <= '" & CType(FecFin.Value, DateTime).ToString & "') OR FECFIN IS NULL))"
			ElseIf Not FecInicio.Value Is Nothing Then
				sFiltroFechas = "(FECINI > '" & CType(FecInicio.Value, DateTime).ToString & "' OR FECINI IS NULL) "
			ElseIf Not FecFin.Value Is Nothing Then
				sFiltroFechas = "(FECINI < '" & CType(FecFin.Value, DateTime).ToString & "' OR FECFIN IS NULL) "
			End If
		End If
		sFiltro = IIf(sFiltroFechas Is String.Empty, sFiltro,
					  IIf(sFiltro Is String.Empty, sFiltroFechas, sFiltro & " AND " & sFiltroFechas))

		For i As Integer = dsDatos.Tables.Count - 2 To 0 Step -1
			Dim oTable As DataTable = dsDatos.Tables(i)

			With dsDatos.DefaultViewManager
				.Dispose()
				If Not txtPartida.Text = "" Then
					If Not sFiltroGrid = "" Then
						sFiltroTexto = "(CODEN LIKE '%" & strToSQLLIKE(DblQuote(txtPartida.Text)) & "%' AND " & sFiltroGrid & ")"
					Else
						sFiltroTexto = "(CODEN LIKE '%" & strToSQLLIKE(DblQuote(txtPartida.Text)) & "%')"
					End If
				Else
					If Not sFiltroGrid = "" Then
						sFiltroTexto = "(" & sFiltroGrid & ")"
					End If
				End If

				If sFiltro Is String.Empty Then
					If Not sFiltroTexto Is String.Empty Then
						If sFiltroBusqueda Is String.Empty Then
							sFiltroBusqueda = sFiltroTexto
						Else
							sFiltroBusqueda = sFiltroTexto & " OR (" & sFiltroBusqueda & ")"
						End If
					End If
				Else
					If sFiltroTexto Is String.Empty Then
						If sFiltroBusqueda Is String.Empty Then
							sFiltroBusqueda = IIf(oTable.TableName = "PRES5_NIV0", String.Empty, sFiltro)
						Else
							sFiltroBusqueda = IIf(oTable.TableName = "PRES5_NIV0", String.Empty, "(" & sFiltro & ") OR ") & "(" & sFiltroBusqueda & ")"
						End If
					Else
						If sFiltroBusqueda Is String.Empty Then
							sFiltroBusqueda = "(" & IIf(oTable.TableName = "PRES5_NIV0", String.Empty, sFiltro & " AND ") & sFiltroTexto & ")"
						Else
							sFiltroBusqueda = "(" & IIf(oTable.TableName = "PRES5_NIV0", String.Empty, sFiltro & " AND ") & sFiltroTexto & ") OR (" & sFiltroBusqueda & ")"
						End If
					End If
				End If

				With .DataViewSettings(oTable.TableName)
					.RowFilter = sFiltroBusqueda
				End With
				'Hago que no mire esto para la tabla PRES5_NIV1 para que no le aplique filtro a las de nivel 0.
				'Estas vienen sin fechas y por el filtro no aparecerían pero necesito que estén para poder agregarles de niveles inferiores
				'que si tendrán fechas
				If i > 1 AndAlso Not sFiltroBusqueda Is String.Empty Then
					sFiltroBusqueda = String.Empty
					Dim sFiltroAux As String = String.Empty
					For Each row As DataRow In .DataViewSettings(oTable.TableName).Table.DefaultView.ToTable.Rows.OfType(Of DataRow).Where(Function(x) x("ESANYO") = 0).ToList
						For j As Integer = 0 To i - 1
							If j = 0 Then
								sFiltroAux = "(PRES0='" & row("PRES0") & "' "
							Else
								sFiltroAux = sFiltroAux & " AND PRES" & j & "='" & row("PRES" & j) & "' "
							End If
						Next

						If Not sFiltroBusqueda.Contains(sFiltroAux) Then
							sFiltroBusqueda = sFiltroBusqueda & sFiltroAux & ") OR "
						End If
					Next
				End If
				If Not sFiltroBusqueda Is String.Empty Then
					sFiltroBusqueda = Left(sFiltroBusqueda, sFiltroBusqueda.Length - 3)
				End If
			End With
		Next
	End Sub
	Private Sub RecargaGrid()
		With whdgPartidas
			.DataSource = CType(Cache("oPartidasResult_" & FSNUser.Cod), DataSet)
			.DataBind()
		End With
	End Sub
	''' <summary>
	''' Con los datos obtenidos y filtrados actualizamos el grid, paginando y con los datos
	''' </summary>
	''' <param name="dsPartidasPresupuestarias">El dataset que contiene los datos</param>
	''' <remarks></remarks>
	Public Sub ActualizarGrid(ByVal dsPartidasPresupuestarias As DataSet)
		Dim filasTotal As Integer = dsPartidasPresupuestarias.DefaultViewManager.DataViewSettings(0).Table.DefaultView.ToTable.Rows.Count
		Dim filasNivel0 As Integer
		Dim paginas As Integer = Math.Ceiling(filasTotal / ConfigurationManager.AppSettings("PartidasPorPagina"))

		If txtCC.Text = "" And hidCCDen.Value <> "" Then txtCC.Text = hidCCDen.Value

		dsResult = New DataSet
		For Each oTable As DataTable In dsPartidasPresupuestarias.Tables
			If Not oTable.TableName = "UONS" Then
				dsResult.Tables.Add(dsPartidasPresupuestarias.DefaultViewManager.DataViewSettings(oTable.TableName).Table.DefaultView.ToTable.Copy)
			End If
		Next

		Dim col As DataColumn() = New DataColumn(4) {}
		col(0) = dsResult.Tables(0).Columns("KEY_SIN_ESPACIO")
		dsResult.Tables(0).PrimaryKey = col
		col(0) = dsResult.Tables(1).Columns("PRES0")
		col(1) = dsResult.Tables(1).Columns("KEY_SIN_ESPACIO")
		dsResult.Tables(1).PrimaryKey = col
		col(0) = dsResult.Tables(2).Columns("PRES0")
		col(1) = dsResult.Tables(2).Columns("PRES1")
		col(2) = dsResult.Tables(2).Columns("KEY_SIN_ESPACIO")
		dsResult.Tables(2).PrimaryKey = col
		col(0) = dsResult.Tables(3).Columns("PRES0")
		col(1) = dsResult.Tables(3).Columns("PRES1")
		col(2) = dsResult.Tables(3).Columns("PRES2")
		col(3) = dsResult.Tables(3).Columns("KEY_SIN_ESPACIO")
		dsResult.Tables(3).PrimaryKey = col

		Dim parentCols() As DataColumn
		Dim childCols() As DataColumn

		For i As Integer = 0 To dsResult.Tables.Count - 2
			ReDim parentCols(i)
			ReDim childCols(i)
			For j As Integer = 0 To i
				parentCols(j) = dsResult.Tables(i).Columns("PRES" & j)
				childCols(j) = dsResult.Tables(i + 1).Columns("PRES" & j)
			Next
			dsResult.Relations.Add("RELACION_" & i + 1, parentCols, childCols, False)
		Next
		InsertarEnCache("oPartidasResult_" & FSNUser.Cod, dsResult)

		Dim NoNivel0 As Boolean = (filasNivel0 = 0)
		If Not NoNivel0 AndAlso paginas = 0 Then paginas = 1

		With whdgPartidas
			.Rows.Clear()
			.GridView.Rows.Clear()
			.Columns.Clear()
			.GridView.Columns.Clear()
			'Dejo las bandas porque están definidas en el .aspx pero borro sus columnas
			For i As Integer = 4 To 1 Step -1
				Dim miBanda As New Band
				Select Case i
					Case 1
						miBanda = .Bands("PRES5_NIV1")
					Case 2
						miBanda = .Bands("PRES5_NIV1").Bands("PRES5_NIV2")
					Case 3
						miBanda = .Bands("PRES5_NIV1").Bands("PRES5_NIV2").Bands("PRES5_NIV3")
					Case 4
						miBanda = .Bands("PRES5_NIV1").Bands("PRES5_NIV2").Bands("PRES5_NIV3").Bands("PRES5_NIV4")
				End Select
				miBanda.Columns.Clear()
			Next

			.DataSource = dsResult
			.GridView.DataSource = .DataSource
			CrearColumnasGrid(whdgPartidas)
			ConfigurarGrid()
			.DataBind()

			.RefreshBehaviors()
		End With

		If NoNivel0 AndAlso paginas = 0 Then
			With whdgPartidas
				.Columns.Clear()
				.GridView.Columns.Clear()
			End With
			imgExcel.Visible = False
		Else
			imgExcel.Visible = True
		End If
	End Sub
	''' <summary>
	''' Actualiza la información que se muestra en el pop up del historico de cambios
	''' </summary>
	''' <remarks></remarks>
	Public Sub ActualizarPopUpHistorico()
		upPartidasPresupuestarias.Update()
	End Sub
#Region "Grid"
	Private Sub whdgPartidas_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgPartidas.InitializeRow
		Dim DefaultColor As Drawing.Color = whdgPartidas.ForeColor
		Dim row As ContainerGridRecord = e.Row
		Dim nivel As Integer = row.Level

		If Not DirectCast(DirectCast(e.Row.DataItem, Infragistics.Web.UI.Framework.Data.DataSetNode).Item, System.Data.DataRowView).DataView.ToTable.Rows.OfType(Of DataRow).Where(Function(x) x("PLURIANUAL") = 0).Any Then
			e.Row.Items.FindItemByKey("FECINI").Column.Hidden = True
			e.Row.Items.FindItemByKey("FECFIN").Column.Hidden = True
		End If

		If nivel = 0 Then
			den1 = ""
			den2 = ""
			den3 = ""
			den4 = ""
			e.Row.Items.FindItemByKey("FECINI").Text = ""
			e.Row.Items.FindItemByKey("FECFIN").Text = ""
			If Not String.IsNullOrEmpty(e.Row.Items.FindItemByKey("DEN").Value) Then e.Row.Items.FindItemByKey("CODEN").Column.Header.Text = e.Row.Items.FindItemByKey("DEN").Value
			If Not String.IsNullOrEmpty(e.Row.Items.FindItemByKey("DEN1").Value) Then den1 = e.Row.Items.FindItemByKey("DEN1").Value
			If Not String.IsNullOrEmpty(e.Row.Items.FindItemByKey("DEN2").Value) Then den2 = e.Row.Items.FindItemByKey("DEN2").Value
			If Not String.IsNullOrEmpty(e.Row.Items.FindItemByKey("DEN3").Value) Then den3 = e.Row.Items.FindItemByKey("DEN3").Value
			If Not String.IsNullOrEmpty(e.Row.Items.FindItemByKey("DEN4").Value) Then den4 = e.Row.Items.FindItemByKey("DEN4").Value
		Else
			Select Case nivel
				Case 1
					If Not String.IsNullOrEmpty(den1) Then e.Row.Items.FindItemByKey("CODEN").Column.Header.Text = den1
				Case 2
					If Not String.IsNullOrEmpty(den2) Then e.Row.Items.FindItemByKey("CODEN").Column.Header.Text = den2
				Case 3
					If Not String.IsNullOrEmpty(den3) Then e.Row.Items.FindItemByKey("CODEN").Column.Header.Text = den3
				Case 4
					If Not String.IsNullOrEmpty(den4) Then e.Row.Items.FindItemByKey("CODEN").Column.Header.Text = den4
			End Select

			If Not DirectCast(DirectCast(e.Row.DataItem, Infragistics.Web.UI.Framework.Data.DataSetNode).Item, System.Data.DataRowView).DataView.ToTable.Rows.OfType(Of DataRow).Where(Function(x) x("ESANYO") = 0).Any Then
				e.Row.Items.FindItemByKey("CODEN").Column.Header.Text = Textos(24)
			End If
		End If

		If e.Row.DataItem.Item("BAJALOG") = 1 Then
			e.Row.CssClass = "PartidaNoVigente"
		Else
			e.Row.CssClass = e.Row.CssClass.Replace("PartidaNoVigente", String.Empty)
		End If

		If e.Row.DataItem.Item("PRESUPUESTADO") IsNot DBNull.Value Then
			e.Row.Items.FindItemByKey("PRESUPUESTADO").Text = FormatNumber(e.Row.Items.FindItemByKey("PRESUPUESTADO").Value, Me.Usuario.NumberFormat) & " " & e.Row.Items.FindItemByKey("MON").Value
		Else
			e.Row.Items.FindItemByKey("PRESUPUESTADO").Text = ""
		End If

		If e.Row.DataItem.Item("COMPROMETIDO") IsNot DBNull.Value Then
			e.Row.Items.FindItemByKey("COMPROMETIDO").Text = FormatNumber(e.Row.Items.FindItemByKey("COMPROMETIDO").Value, Me.Usuario.NumberFormat) & " " & e.Row.Items.FindItemByKey("MON").Value
		Else
			e.Row.Items.FindItemByKey("COMPROMETIDO").Text = ""
		End If

		If e.Row.DataItem.Item("SOLICITADO") IsNot DBNull.Value Then
			e.Row.Items.FindItemByKey("SOLICITADO").Text = FormatNumber(e.Row.Items.FindItemByKey("SOLICITADO").Value, Me.Usuario.NumberFormat) & " " & e.Row.Items.FindItemByKey("MON").Value
		Else
			e.Row.Items.FindItemByKey("SOLICITADO").Text = ""
		End If

		If e.Row.DataItem.Item("DISPONIBLE") IsNot DBNull.Value Then
			e.Row.Items.FindItemByKey("DISPONIBLE").Text = FormatNumber(e.Row.Items.FindItemByKey("DISPONIBLE").Value, Me.Usuario.NumberFormat) & " " & e.Row.Items.FindItemByKey("MON").Value
		Else
			e.Row.Items.FindItemByKey("DISPONIBLE").Text = ""
		End If

		With e.Row.Items.FindItemByKey("EXTERNO")
			If IsNumeric(e.Row.DataItem.Item("EXTERNO")) AndAlso e.Row.DataItem.Item("EXTERNO") = 1 AndAlso nivel > 0 Then
				.Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/Externo.gif' style='text-align:center;'/>"
				If (PermisoModificacionPartidaPresupuestaria Or PermisoPresupuestacionPartidaPresupuestaria) AndAlso nivel > 0 AndAlso e.Row.Items.FindItemByKey("ESANYO").Value = 0 Then
					If e.Row.DataItem.Item("CONTROLCAMBIOS") = 0 Then
						e.Row.Items.FindItemByKey("AGREGAR").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/Candado_close.png' style='text-align:center;'/>"
						e.Row.Items.FindItemByKey("ACCION").Text = ""
					Else
						e.Row.Items.FindItemByKey("AGREGAR").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/Candado_open.png' style='text-align:center;'/>"
						e.Row.Items.FindItemByKey("ACCION").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/Editar.png' style='text-align:center;'/>"
					End If
				Else
					e.Row.Items.FindItemByKey("AGREGAR").Text = ""
					e.Row.Items.FindItemByKey("ACCION").Text = ""
				End If
			Else
				.Text = ""
				With e.Row.Items.FindItemByKey("AGREGAR")
					If PermisoAltaPartidaPresupuestaria AndAlso nivel < 4 AndAlso e.Row.Items.FindItemByKey("BAJALOG").Value = 0 AndAlso e.Row.Items.FindItemByKey("ESANYO").Value = 0 Then
						.Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/Agregar.gif' style='text-align:center;'/>"
					Else
						.Text = ""
					End If
				End With

				If nivel > 0 Then
					With e.Row.Items.FindItemByKey("ACCION")
						If PermisoModificacionPartidaPresupuestaria Or PermisoPresupuestacionPartidaPresupuestaria Then
							.Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/Editar.png' style='text-align:center;'/>"
						Else
							.Text = ""
						End If
					End With
				End If
			End If
		End With

		With e.Row.Items.FindItemByKey("IMGHISTORICO")
			If e.Row.DataItem.Item("HISTORICO") > 1 Then
				.Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/Historico.gif' style='text-align:center;'/>"
			Else
				.Text = ""
			End If
		End With
	End Sub
	'Funcion que emula el evento CellSelectionChanged
	Private Sub SelectionChanged(ByVal datos As Object)
		Select Case datos("key")
			Case "IMGHISTORICO"
				ModalCargando.Hide()
				mpeHistorico.Show()

				With Historico
					Dim Pres(4) As String
					For i As Integer = 0 To 4
						Pres(i) = datos("Pres" & i)
					Next
					.EsAnyo = datos("esAnyo")
					.Pres0 = Pres(0)
					.Pres1 = Pres(1)
					.Pres2 = Pres(2)
					If String.IsNullOrEmpty(.Pres2) And .EsAnyo Then
						.Anyo = .Pres1
						.Pres1 = Nothing
					End If
					.Pres3 = Pres(3)
					If String.IsNullOrEmpty(.Pres3) And .EsAnyo Then
						.Anyo = .Pres2
						.Pres2 = Nothing
					End If
					.Pres4 = Pres(4)
					If String.IsNullOrEmpty(.Pres4) And .EsAnyo Then
						.Anyo = .Pres3
						.Pres3 = Nothing
					End If

					.CargarHistorico()
				End With
				RecargaGrid()
			Case "AGREGAR"
				'Si llega aqui es que estamos abriendo o cerrando el control de cambios para una partida presupuestaria externa
				Dim sValoresCentroCoste() As String = Split(hidCC.Value, "#")
				Dim sUON1, sUON2, sUON3, sUON4 As String

				sUON1 = sValoresCentroCoste(0)
				sUON2 = sValoresCentroCoste(1)
				sUON3 = sValoresCentroCoste(2)
				sUON4 = sValoresCentroCoste(3)

				Dim Pres(4) As String
				For i As Integer = 0 To 4
					Pres(i) = datos("Pres" & i)
				Next

				Dim oPartidaPresupuestaria As PartidasPRES5 = Me.FSNServer.Get_Object(GetType(PartidasPRES5))
				Dim dsNew As DataSet = oPartidaPresupuestaria.ModificarControlCambiosPartidaPresupuestaria(sUON1, sUON2, sUON3, sUON4,
																						Pres(0), Pres(1), Pres(2), Pres(3), Pres(4),
																						IIf(datos("Cambios") = 0, 1, 0), FSNUser.Cod, FSNUser.Idioma)
				InsertarEnCache("oPartidasPresupuestarias_" & FSNUser.Cod, dsNew)

				Filtrar(dsNew)

				ActualizarGrid(dsNew)
		End Select
	End Sub
	Private Sub CrearColumnasGrid(ByVal migrid As Infragistics.Web.UI.GridControls.WebHierarchicalDataGrid)
		Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
		Dim nombre As String

		If migrid.ID = whdgPartidas.ID Then
			With migrid
				.DataMember = "PRES5_NIV0"
				.DataKeyFields = "PRES0"
				For i As Integer = 0 To .DataSource.Tables("PRES5_NIV0").Columns.Count - 1
					campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
					nombre = .DataSource.Tables("PRES5_NIV0").Columns(i).ToString
					With campoGrid
						.Key = nombre
						.DataFieldName = nombre
						.Header.Text = nombre
					End With

					.Columns.Insert(i, campoGrid)
					.GridView.Columns.Insert(i, campoGrid)
				Next
			End With
		End If
		For j As Integer = 1 To migrid.DataSource.Tables.Count - 1
			If migrid.DataSource.Tables("PRES5_NIV" & j).Rows.Count > 0 Then
				With migrid
					For i As Integer = 0 To .DataSource.Tables("PRES5_NIV" & j).Columns.Count - 1
						campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
						nombre = .DataSource.Tables("PRES5_NIV" & j).Columns(i).ToString
						With campoGrid
							.Key = nombre
							.DataFieldName = nombre
							.Header.Text = nombre
						End With

						Select Case j
							Case 1
								.Bands("PRES5_NIV1").Columns.Insert(i, campoGrid)
								.Bands("PRES5_NIV1").Columns(i).VisibleIndex = i
							Case 2
								.Bands("PRES5_NIV1").Bands("PRES5_NIV2").Columns.Insert(i, campoGrid)
								.Bands("PRES5_NIV1").Bands("PRES5_NIV2").Columns(i).VisibleIndex = i
							Case 3
								.Bands("PRES5_NIV1").Bands("PRES5_NIV2").Bands("PRES5_NIV3").Columns.Insert(i, campoGrid)
								.Bands("PRES5_NIV1").Bands("PRES5_NIV2").Bands("PRES5_NIV3").Columns(i).VisibleIndex = i
							Case 4
								.Bands("PRES5_NIV1").Bands("PRES5_NIV2").Bands("PRES5_NIV3").Bands("PRES5_NIV4").Columns.Insert(i, campoGrid)
								.Bands("PRES5_NIV1").Bands("PRES5_NIV2").Bands("PRES5_NIV3").Bands("PRES5_NIV4").Columns(i).VisibleIndex = i
						End Select
					Next
				End With
			End If
		Next
	End Sub
	Private Sub AnyadirColumna(ByVal fieldname As String, ByVal i As Integer, ByVal banda As Integer)
		Dim unboundField As New Infragistics.Web.UI.GridControls.UnboundField(True)

		unboundField.Key = fieldname
		With whdgPartidas
			If banda = 0 Then
				whdgPartidas.Columns.Insert(i, unboundField)
				whdgPartidas.GridView.Columns.Insert(i, unboundField)
			Else
				Select Case banda
					Case 1
						.Bands("PRES5_NIV1").Columns.Insert(i, unboundField)
					Case 2
						.Bands("PRES5_NIV1").Bands("PRES5_NIV2").Columns.Insert(i, unboundField)
					Case 3
						.Bands("PRES5_NIV1").Bands("PRES5_NIV2").Bands("PRES5_NIV3").Columns.Insert(i, unboundField)
					Case 4
						.Bands("PRES5_NIV1").Bands("PRES5_NIV2").Bands("PRES5_NIV3").Bands("PRES5_NIV4").Columns.Insert(i, unboundField)
				End Select
			End If
		End With
	End Sub
	Private Sub InitializeGrid()
		'Antes InitializeDataSource
		If hidCC.Value = "" Then
			'whdgPartidas.DisplayLayout.NoDataMessage = Textos(13)
		Else
			ModuloIdioma = TiposDeDatos.ModulosIdiomas.PartidasPresupuestarias
			Dim dsPartidasPresupuestarias As New DataSet

			If hidRecargar.Value = "1" Then
				dsPartidasPresupuestarias = PartidasPresupuestarias(True)
				hidRecargar.Value = ""
			Else
				dsPartidasPresupuestarias = PartidasPresupuestarias.Copy
			End If

			Filtrar(dsPartidasPresupuestarias)

			upPartidasPresupuestarias.Update()
		End If
	End Sub
	Private Sub ConfigurarGrid()
		'Antes InitializeLayout
		Dim oPargenSMs As PargensSMs = FSNServer.Get_Object(GetType(FSNServer.PargensSMs))
		Dim TextosCabecera As DataRow
		TextosCabecera = oPargenSMs.GetHeadersPartidasPresupuestarias(FSNUser.Idioma)

		With whdgPartidas
			With .Columns("EXTERNO")
				.VisibleIndex = 0
				.Width = 30
				.Header.Text = ""
				.Header.CssClass = "sinBorde"
				.CssClass = "sinBorde"
			End With
			With .Columns("CODEN")
				.VisibleIndex = 2
				.Header.Text = Textos(0)
				.CssClass = "sinBorde"
				.Header.CssClass = "sinBorde"
			End With
			AnyadirColumna("AGREGAR", 3, 0)
			With .Columns("AGREGAR")
				.VisibleIndex = 3
				.Width = 30
				.Header.CssClass = "sinBorde"
				.CssClass = "sinBorde"
			End With
			AnyadirColumna("ACCION", 4, 0)
			With .Columns("ACCION")
				.VisibleIndex = 4
				.Width = 30
				.Header.CssClass = "sinBorde"
				.CssClass = "sinBorde"
			End With
			With .Columns("FECINI")
				.VisibleIndex = 5
				.Width = 80
				.FormatValue(FSNUser.DateFormat.ShortDatePattern)
				.Header.Text = Textos(10)
			End With
			With .Columns("FECFIN")
				.VisibleIndex = 6
				.Width = 80
				.FormatValue(FSNUser.DateFormat.ShortDatePattern)
				.Header.Text = Textos(11)
			End With
			With .Columns("PRESUPUESTADO")
				.VisibleIndex = 7
				.Width = 130
				.Header.Text = TextosCabecera("PRESUPUESTADO")
			End With
			With .Columns("COMPROMETIDO")
				.VisibleIndex = 8
				.Width = 130
				.Header.Text = TextosCabecera("COMPROMETIDO")
			End With
			With .Columns("SOLICITADO")
				.VisibleIndex = 9
				.Width = 130
				.Header.Text = TextosCabecera("SOLICITADO")
			End With
			With .Columns("DISPONIBLE")
				.VisibleIndex = 10
				.Width = 130
				.Header.Text = TextosCabecera("DISPONIBLE")
			End With
			AnyadirColumna("IMGHISTORICO", 11, 0)
			With .Columns("IMGHISTORICO")
				.Width = 30
				.Header.CssClass = "sinBorde"
				.CssClass = "sinBorde"
			End With
			'Oculto columnas
			.Columns("PRES0").Hidden = True
			.Columns("DEN").Hidden = True
			.Columns("CONTROLCAMBIOS").Hidden = True
			.Columns("MON").Hidden = True
			.Columns("BAJALOG").Hidden = True
			.Columns("HISTORICO").Hidden = True
			.Columns("KEY_SIN_ESPACIO").Hidden = True
			.Columns("DEN1").Hidden = True
			.Columns("DEN2").Hidden = True
			.Columns("DEN3").Hidden = True
			.Columns("DEN4").Hidden = True
			.Columns("ESANYO").Hidden = True
			.Columns("ID").Hidden = True
			.Columns("PLURIANUAL").Hidden = True
		End With

		With whdgPartidas.GridView
			With .Columns("EXTERNO")
				.VisibleIndex = 0
				.Width = 30
				.Header.Text = ""
				.Header.CssClass = "sinBorde"
				.CssClass = "sinBorde"
			End With
			With .Columns("CODEN")
				.VisibleIndex = 2
				.Header.Text = Textos(0)
				.CssClass = "sinBorde"
				.Header.CssClass = "sinBorde"
			End With
			With .Columns("AGREGAR")
				.VisibleIndex = 3
				.Width = 30
				.Header.CssClass = "sinBorde"
				.CssClass = "sinBorde"
			End With
			With .Columns("ACCION")
				.VisibleIndex = 4
				.Width = 30
				.Header.CssClass = "sinBorde"
				.CssClass = "sinBorde"
			End With
			With .Columns("FECINI")
				.VisibleIndex = 5
				.Width = 80
				.FormatValue(FSNUser.DateFormat.ShortDatePattern)
				.Header.Text = Textos(10)
			End With
			With .Columns("FECFIN")
				.VisibleIndex = 6
				.Width = 80
				.FormatValue(FSNUser.DateFormat.ShortDatePattern)
				.Header.Text = Textos(11)
			End With
			With .Columns("PRESUPUESTADO")
				.VisibleIndex = 7
				.Width = 130
				.Header.Text = TextosCabecera("PRESUPUESTADO")
			End With
			With .Columns("COMPROMETIDO")
				.VisibleIndex = 8
				.Width = 130
				.Header.Text = TextosCabecera("COMPROMETIDO")
			End With
			With .Columns("SOLICITADO")
				.VisibleIndex = 9
				.Width = 130
				.Header.Text = TextosCabecera("SOLICITADO")
			End With
			With .Columns("DISPONIBLE")
				.VisibleIndex = 10
				.Width = 130
				.Header.Text = TextosCabecera("DISPONIBLE")
			End With
			With .Columns("IMGHISTORICO")
				.Width = 30
				.Header.CssClass = "sinBorde"
				.CssClass = "sinBorde"
			End With
			'Oculto columnas
			.Columns("PRES0").Hidden = True
			.Columns("DEN").Hidden = True
			.Columns("CONTROLCAMBIOS").Hidden = True
			.Columns("MON").Hidden = True
			.Columns("BAJALOG").Hidden = True
			.Columns("HISTORICO").Hidden = True
			.Columns("KEY_SIN_ESPACIO").Hidden = True
			.Columns("DEN1").Hidden = True
			.Columns("DEN2").Hidden = True
			.Columns("DEN3").Hidden = True
			.Columns("DEN4").Hidden = True
			.Columns("ESANYO").Hidden = True
			.Columns("ID").Hidden = True
			.Columns("PLURIANUAL").Hidden = True
		End With

		'Para las bandas
		For j As Integer = 1 To whdgPartidas.DataSource.Tables.Count - 1
			If whdgPartidas.DataSource.Tables("PRES5_NIV" & j).Rows.Count > 0 Then
				Dim migrid As Infragistics.Web.UI.GridControls.Band
				Select Case j
					Case 1
						migrid = whdgPartidas.Bands("PRES5_NIV1")
					Case 2
						migrid = whdgPartidas.Bands("PRES5_NIV1").Bands("PRES5_NIV2")
					Case 3
						migrid = whdgPartidas.Bands("PRES5_NIV1").Bands("PRES5_NIV2").Bands("PRES5_NIV3")
					Case Else
						migrid = whdgPartidas.Bands("PRES5_NIV1").Bands("PRES5_NIV2").Bands("PRES5_NIV3").Bands("PRES5_NIV4")
				End Select

				With migrid
					AnyadirColumna("AGREGAR", 3, j)
					AnyadirColumna("ACCION", 4, j)
					AnyadirColumna("IMGHISTORICO", 11, j)

					Dim colNum As Integer = 16
					For i As Integer = 0 To j
						.Columns("PRES" & i).VisibleIndex = colNum
						colNum += 1
						.Columns("PRES" & i).Hidden = True
					Next
					With .Columns("EXTERNO")
						.VisibleIndex = 0
						.Width = 30
						.Header.Text = ""
						.CssClass = "sinBorde"
						.Header.CssClass = "sinBorde"
					End With
					With .Columns("CODEN")
						.VisibleIndex = 2
						.CssClass = "sinBorde"
						.Header.CssClass = "sinBorde"
					End With
					With .Columns("AGREGAR")
						.VisibleIndex = 3
						.Width = 30
						.CssClass = "sinBorde"
						.Header.CssClass = "sinBorde"
					End With
					With .Columns("ACCION")
						.VisibleIndex = 4
						.Width = 30
						.CssClass = "sinBorde"
						.Header.CssClass = "sinBorde"
					End With
					With .Columns("FECINI")
						.VisibleIndex = 5
						.Width = 80
						.FormatValue(FSNUser.DateFormat.ShortDatePattern)
						.Header.Text = Textos(10)
					End With
					With .Columns("FECFIN")
						.VisibleIndex = 6
						.Width = 80
						.FormatValue(FSNUser.DateFormat.ShortDatePattern)
						.Header.Text = Textos(11)
					End With
					With .Columns("PRESUPUESTADO")
						.VisibleIndex = 7
						.Width = 130
						.Header.Text = TextosCabecera("PRESUPUESTADO")
					End With
					With .Columns("COMPROMETIDO")
						.VisibleIndex = 8
						.Width = 130
						.Header.Text = TextosCabecera("COMPROMETIDO")
					End With
					With .Columns("SOLICITADO")
						.VisibleIndex = 9
						.Width = 130
						.Header.Text = TextosCabecera("SOLICITADO")
					End With
					With .Columns("DISPONIBLE")
						.VisibleIndex = 10
						.Width = 130
						.Header.Text = TextosCabecera("DISPONIBLE")
						.CssClass = "sinBorde"
						.Header.CssClass = "sinBorde"
					End With
					With .Columns("IMGHISTORICO")
						.VisibleIndex = 11
						.Width = 30
						.CssClass = "sinBorde"
						.Header.CssClass = "sinBorde"
					End With
					'Oculto columnas
					.Columns("DEN").Hidden = True
					.Columns("CONTROLCAMBIOS").Hidden = True
					.Columns("MON").Hidden = True
					.Columns("BAJALOG").Hidden = True
					.Columns("HISTORICO").Hidden = True
					.Columns("KEY_SIN_ESPACIO").Hidden = True
					.Columns("ESANYO").Hidden = True
					.Columns("ID").Hidden = True
					.Columns("PLURIANUAL").Hidden = True
				End With
			End If
		Next
	End Sub
	Private Sub ConfigurarExcel(ByVal MostrarFechasPartida As Boolean)
		Dim oPargenSMs As PargensSMs = FSNServer.Get_Object(GetType(FSNServer.PargensSMs))
		Dim TextosCabecera As DataRow
		TextosCabecera = oPargenSMs.GetHeadersPartidasPresupuestarias(FSNUser.Idioma)

		Dim cabecera0, cabecera1, cabecera2, cabecera3, cabecera4 As String
		cabecera0 = String.Empty : cabecera1 = String.Empty : cabecera2 = String.Empty : cabecera3 = String.Empty : cabecera4 = String.Empty
		With whdgPartidasExport
			If Not String.IsNullOrEmpty(.DataSource.TABLES("PRES5_NIV0").Rows(0)("DEN")) Then cabecera0 = .DataSource.TABLES("PRES5_NIV0").Rows(0)("DEN")
			If Not String.IsNullOrEmpty(.DataSource.TABLES("PRES5_NIV0").Rows(0)("DEN1")) Then cabecera1 = .DataSource.TABLES("PRES5_NIV0").Rows(0)("DEN1")
			If Not String.IsNullOrEmpty(.DataSource.TABLES("PRES5_NIV0").Rows(0)("DEN2")) Then cabecera2 = .DataSource.TABLES("PRES5_NIV0").Rows(0)("DEN2")
			If Not String.IsNullOrEmpty(.DataSource.TABLES("PRES5_NIV0").Rows(0)("DEN3")) Then cabecera3 = .DataSource.TABLES("PRES5_NIV0").Rows(0)("DEN3")
			If Not String.IsNullOrEmpty(.DataSource.TABLES("PRES5_NIV0").Rows(0)("DEN4")) Then cabecera4 = .DataSource.TABLES("PRES5_NIV0").Rows(0)("DEN4")

			.Columns("PRES0").VisibleIndex = 13
			.Columns("PRES0").Hidden = True
			With .Columns("DEN")
				.Width = 300
				.Header.Text = cabecera0
			End With
			With .Columns("FECINI")
				.Width = 75
				.FormatValue(FSNUser.DateFormat.ShortDatePattern)
				.Header.Text = Textos(10)
			End With
			With .Columns("FECFIN")
				.Width = 75
				.FormatValue(FSNUser.DateFormat.ShortDatePattern)
				.Header.Text = Textos(11)
			End With
			With .Columns("PRESUPUESTADO")
				.Width = 100
				.Header.Text = TextosCabecera("PRESUPUESTADO")
			End With
			With .Columns("COMPROMETIDO")
				.Width = 100
				.Header.Text = TextosCabecera("COMPROMETIDO")
			End With
			With .Columns("SOLICITADO")
				.Width = 100
				.Header.Text = TextosCabecera("SOLICITADO")
			End With
			With .Columns("DISPONIBLE")
				.Width = 100
				.Header.Text = TextosCabecera("DISPONIBLE")
			End With
			'Oculto columnas
			.Columns("EXTERNO").Hidden = True
			.Columns("CODEN").Hidden = True
			.Columns("CONTROLCAMBIOS").Hidden = True
			.Columns("MON").Hidden = True
			.Columns("BAJALOG").Hidden = True
			.Columns("HISTORICO").Hidden = True
			.Columns("KEY_SIN_ESPACIO").Hidden = True
			.Columns("PRES0").Hidden = True
			.Columns("FECINI").Hidden = Not MostrarFechasPartida
			.Columns("FECFIN").Hidden = Not MostrarFechasPartida
		End With

		With whdgPartidasExport.GridView
			Dim colNum As Integer = 13
			.Columns("PRES0").VisibleIndex = 13
			.Columns("PRES0").Hidden = True
			With .Columns("DEN")
				.Width = 300
				.Header.Text = cabecera0
			End With
			With .Columns("FECINI")
				.Width = 75
				.FormatValue(FSNUser.DateFormat.ShortDatePattern)
				.Header.Text = Textos(10)
			End With
			With .Columns("FECFIN")
				.Width = 75
				.FormatValue(FSNUser.DateFormat.ShortDatePattern)
				.Header.Text = Textos(11)
			End With
			With .Columns("PRESUPUESTADO")
				.Width = 100
				.Header.Text = TextosCabecera("PRESUPUESTADO")
			End With
			With .Columns("COMPROMETIDO")
				.Width = 100
				.Header.Text = TextosCabecera("COMPROMETIDO")
			End With
			With .Columns("SOLICITADO")
				.Width = 100
				.Header.Text = TextosCabecera("SOLICITADO")
			End With
			With .Columns("DISPONIBLE")
				.Width = 100
				.Header.Text = TextosCabecera("DISPONIBLE")
			End With
			'Oculto columnas
			.Columns("EXTERNO").Hidden = True
			.Columns("CODEN").Hidden = True
			.Columns("CONTROLCAMBIOS").Hidden = True
			.Columns("MON").Hidden = True
			.Columns("BAJALOG").Hidden = True
			.Columns("HISTORICO").Hidden = True
			.Columns("KEY_SIN_ESPACIO").Hidden = True
			.Columns("PRES0").Hidden = True
			.Columns("FECINI").Hidden = Not MostrarFechasPartida
			.Columns("FECFIN").Hidden = Not MostrarFechasPartida
		End With

		'Para las bandas
		For j As Integer = 1 To whdgPartidasExport.DataSource.Tables.Count - 1
			If whdgPartidasExport.DataSource.Tables("PRES5_NIV" & j).Rows.Count > 0 Then
				Dim migrid As Infragistics.Web.UI.GridControls.Band
				Select Case j
					Case 1
						migrid = whdgPartidasExport.Bands("PRES5_NIV1")
						migrid.Columns("DEN").Header.Text = cabecera1
					Case 2
						migrid = whdgPartidasExport.Bands("PRES5_NIV1").Bands("PRES5_NIV2")
						migrid.Columns("DEN").Header.Text = cabecera2
					Case 3
						migrid = whdgPartidasExport.Bands("PRES5_NIV1").Bands("PRES5_NIV2").Bands("PRES5_NIV3")
						migrid.Columns("DEN").Header.Text = cabecera3
					Case Else
						migrid = whdgPartidasExport.Bands("PRES5_NIV1").Bands("PRES5_NIV2").Bands("PRES5_NIV3").Bands("PRES5_NIV4")
						migrid.Columns("DEN").Header.Text = cabecera4
				End Select
				If Not CType(whdgPartidasExport.DataSource, DataSet).Tables(migrid.DataMember).Rows.OfType(Of DataRow).Where(Function(x) x("ESANYO") = 0).Any Then
					migrid.Columns("DEN").Header.Text = Textos(24)
				End If
				With migrid
					Dim colNum As Integer = 13
					For i As Integer = 0 To j
						.Columns("PRES" & i).Hidden = True
					Next
					With .Columns("DEN")
						.Width = 300 - (22 * j)
					End With
					With .Columns("FECINI")
						.Width = 75
						.FormatValue(FSNUser.DateFormat.ShortDatePattern)
						.Header.Text = Textos(10)
					End With
					With .Columns("FECFIN")
						.Width = 75
						.FormatValue(FSNUser.DateFormat.ShortDatePattern)
						.Header.Text = Textos(11)
					End With
					With .Columns("PRESUPUESTADO")
						.Width = 100
						.Header.Text = TextosCabecera("PRESUPUESTADO")
					End With
					With .Columns("COMPROMETIDO")
						.Width = 100
						.Header.Text = TextosCabecera("COMPROMETIDO")
					End With
					With .Columns("SOLICITADO")
						.Width = 100
						.Header.Text = TextosCabecera("SOLICITADO")
					End With
					With .Columns("DISPONIBLE")
						.Width = 100
						.Header.Text = TextosCabecera("DISPONIBLE")
					End With

					'Oculto columnas
					.Columns("EXTERNO").Hidden = True
					.Columns("CODEN").Hidden = True
					.Columns("CONTROLCAMBIOS").Hidden = True
					.Columns("MON").Hidden = True
					.Columns("BAJALOG").Hidden = True
					.Columns("HISTORICO").Hidden = True
					.Columns("KEY_SIN_ESPACIO").Hidden = True
					.Columns("PRES0").Hidden = True
					.Columns("ID").Hidden = True
					.Columns("ESANYO").Hidden = True
					.Columns("PLURIANUAL").Hidden = True
					.Columns("FECINI").Hidden = Not MostrarFechasPartida
					.Columns("FECFIN").Hidden = Not MostrarFechasPartida
				End With
			End If
		Next
	End Sub
	Private Sub whdgPartidas_InitializeBand(sender As Object, e As Infragistics.Web.UI.GridControls.BandEventArgs) Handles whdgPartidas.InitializeBand
		e.Band.Behaviors.Paging.PageSize = ConfigurationManager.AppSettings("PartidasPorPagina")
	End Sub
#End Region
#Region "Export Excel"
	Private Sub ExportarExcel()
		'Exporta a Excel:
		With whdgPartidasExport
			Dim dsPartidasPresupuestarias As New DataSet
			dsPartidasPresupuestarias = PartidasPresupuestarias.Copy

			Filtrar(dsPartidasPresupuestarias)

			dsPartidasPresupuestarias.Tables.Remove("UONS")

			For i As Integer = dsPartidasPresupuestarias.Tables.Count - 1 To 0 Step -1
				If dsPartidasPresupuestarias.Tables(i).DefaultView.ToTable.Rows.Count = 0 Then
					dsPartidasPresupuestarias.Relations.Remove("RELACION_" & i)
					dsPartidasPresupuestarias.Tables.RemoveAt(i)
				End If
			Next

			.DataSource = dsPartidasPresupuestarias
			CrearColumnasGrid(whdgPartidasExport)
			.InitialDataBindDepth = -1
			.DataBind()

			ConfigurarExcel(dsPartidasPresupuestarias.Tables(0).Rows.OfType(Of DataRow).Where(Function(x) x("PLURIANUAL") = 0).Any)
			excelExporter.DownloadName = "PartidasPresupuestarias.xls"
			excelExporter.DataExportMode = DataExportMode.AllDataInDataSource
			excelExporter.Export(whdgPartidasExport)
		End With
	End Sub
	Private Sub excelExporter_RowExported(sender As Object, e As Infragistics.Web.UI.GridControls.ExcelRowExportedEventArgs) Handles excelExporter.RowExported
		System.Math.Max(System.Threading.Interlocked.Increment(exportedRows), exportedRows - 1)
	End Sub
	Private Sub excelExporter_GridRecordItemExporting(sender As Object, e As GridRecordItemExportingEventArgs) Handles excelExporter.GridRecordItemExporting
		If {"PRESUPUESTADO", "COMPROMETIDO", "SOLICITADO", "DISPONIBLE"}.Contains(e.GridCell.Column.Key) Then e.WorksheetCell.CellFormat.FormatString = "0.00"
	End Sub
#End Region
	<System.Web.Services.WebMethodAttribute(),
	System.Web.Script.Services.ScriptMethodAttribute()>
	Public Shared Function GetPartidasPresupuestarias(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
		Dim FSSMServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
		Dim FSSMUser As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
		Dim cPres5 As FSNServer.PartidasPRES5 = FSSMServer.Get_Object(GetType(FSNServer.PartidasPRES5))
		Dim MiDataSet As New DataTable
		Dim arrayPalabras As String()
		Dim sbusqueda

		Dim UON() As String = Split(Split(contextKey, "@@")(0), "#")
		MiDataSet = cPres5.GetPartidasPresupuestarias(UON(0), UON(1), UON(2), UON(3),
													  FSSMUser.Cod, FSSMUser.Idioma,
													  Split(contextKey, "@@")(1))
		arrayPalabras = Split(prefixText)

		For i = 1 To arrayPalabras.Length - 1 'desde la segunda palabra
			arrayPalabras(i) = " " & arrayPalabras(i)
		Next

		sbusqueda = From Datos In MiDataSet
					Let w = UCase(DBNullToStr("COD"))
					Where Datos.Field(Of String)("COD").ToUpper Like "" & strToSQLLIKE(prefixText, True) & "*" Or Datos.Field(Of String)("DEN").ToUpper Like "" & strToSQLLIKE(prefixText, True) & "*"
					Select Datos.Item("COD") & "-" & Datos.Item("DEN") Distinct.Take(count).ToArray()
		Dim resul() As String
		ReDim resul(UBound(sbusqueda))
		For i As Integer = 0 To UBound(sbusqueda)
			resul(i) = sbusqueda(i).ToString
		Next
		Return resul
	End Function
End Class