﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MantenimientoControlPresupuestacion.aspx.vb" Inherits="Fullstep.FSNWeb.MantenimientoControlPresupuestacion" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>    
</head>
<body style="background: white">
	<form id="form1" runat="server">
		<asp:ScriptManager ID="ScriptManager1" runat="server">
			<CompositeScript>
				<Scripts>
					<asp:ScriptReference Path="../../../js/jquery/jquery.min.js" />
				</Scripts>
			</CompositeScript>
		</asp:ScriptManager>

		<div id="capa" style="overflow: hidden; margin-top:1em;">
			<asp:Panel runat="server" ID="pnlPartida">
				<div style="clear: both; margin-left: 5px; line-height: 20px;">					
					<asp:Label ID="lblLitCodigo" runat="server" SkinID="EtiquetaNegrita"></asp:Label>				
					<asp:TextBox ID="txtCodigo" runat="server" Style="vertical-align: middle; width: 300px; margin-left:1em;"></asp:TextBox>
					<asp:Label runat="server" ID="lblCodigo" style="margin-left:1em;"></asp:Label>
				</div>

				<asp:Panel runat="server" ID="pnlDenominacion" Style="clear: both; margin-left: 5px; padding-top: 5px;">					
					<asp:Label runat="server" ID="lblLitDenominacion" SkinID="EtiquetaNegrita"></asp:Label>					
					<asp:Label runat="server" ID="lblDenominacion" style="margin-left:1em;"></asp:Label>
				</asp:Panel>

				<asp:Panel runat="server" ID="pnlDenominaciones" Style="margin-left: 5px; padding-top: 5px;">
					<fieldset style="border-radius: 5px;">
						<legend>
							<asp:Label runat="server" ID="lblLitDenominaciones" SkinID="EtiquetaNegrita"></asp:Label>
						</legend>
						<div style="overflow: auto; max-height: 90px; padding-top: 5px;">
							<asp:Repeater runat="server" ID="rptDenominaciones">
								<ItemTemplate>
									<div style="line-height: 25px;">										
										<asp:Label runat="server" ID="lblIdioma" Text='<%#DataBinder.Eval(Container.DataItem, "DEN") & ":"%>' style="display:block;"></asp:Label>
										<asp:Label runat="server" ID="lblCodIdioma" Text='<%#DataBinder.Eval(Container.DataItem,"COD")%>' Visible="false"></asp:Label>
										<asp:TextBox runat="server" ID="txtDenominacion" Style="width: 270px;" MaxLength="200" Text='<%#DataBinder.Eval(Container.DataItem,"DENOMINACION")%>'></asp:TextBox>
										<asp:RequiredFieldValidator ID="vDenominacion" runat="server" Enabled="false" ControlToValidate="txtDenominacion"
											Style="width: 5px;" ValidationGroup="Denominaciones" ErrorMessage="" EnableViewState="true">
										</asp:RequiredFieldValidator>
									</div>
								</ItemTemplate>
							</asp:Repeater>
						</div>
					</fieldset>
				</asp:Panel>
			</asp:Panel>
			<asp:Panel runat="server" ID="pnlAnyo" style="margin-left:1em;">
				<asp:Label runat="server" ID="lblLitAnyo" SkinID="EtiquetaNegrita" style="display:inline-block; width:6em;"></asp:Label>					
				<asp:TextBox runat="server" ID="txtAnyo"  Style="vertical-align: middle; width: 4em; margin-left:0.5em;"></asp:TextBox>
				<asp:Label runat="server" ID="lblAnyo" style="margin-left:0.3em;"></asp:Label>
				<ajx:FilteredTextBoxExtender ID="ftAnyo" runat="server" TargetControlID="txtAnyo" FilterType="Custom, Numbers" />
			</asp:Panel>
			<asp:Panel runat="server" ID="pnlPresupuestacion" Style="margin-left:1em; padding-top: 5px;">
				<asp:Panel runat="server" ID="pnlFechas" style="line-height: 25px;">
					<asp:Label runat="server" ID="lblLitFecInicio" SkinID="EtiquetaNegrita" style="display:inline-block; vertical-align:middle;"></asp:Label>:
					<igpck:WebDatePicker ID="FecInicio" runat="server" DropDownCalendarID="calendarioPicker" Width="90px" style="display:inline-table; vertical-align:middle; margin-left:0.5em;"></igpck:WebDatePicker>
					<igpck:WebMonthCalendar runat="server" ID="calendarioPicker"></igpck:WebMonthCalendar>
					<asp:Label runat="server" ID="lblLitFecFin" SkinID="EtiquetaNegrita" style="display:inline-block; vertical-align:middle; margin-left:0.5em;"></asp:Label>:
					<igpck:WebDatePicker ID="FecFin" runat="server" DropDownCalendarID="calendarioPicker" Width="90px" style="display:inline-table; vertical-align:middle; margin-left:0.5em;"></igpck:WebDatePicker>
				</asp:Panel>
				<div style="margin-right:1em; line-height: 25px;">
					<asp:Label runat="server" ID="lblLitCreditoFinal" SkinID="EtiquetaNegrita" style="display:inline-block; width:6em;"></asp:Label>:				
					<asp:TextBox runat="server" ID="txtCreditoFinal" Style="vertical-align: middle; width: 86px; text-align:right; display:inline-block; margin-left:0.3em;"></asp:TextBox>
					<ajx:FilteredTextBoxExtender ID="ftCreditoFinal" runat="server" TargetControlID="txtCreditoFinal" FilterType="Custom, Numbers" />
					<asp:Label runat="server" ID="lblLitMoneda" SkinID="EtiquetaNegrita" style="display:inline-block; margin-left:0.5em;"></asp:Label>
					<div style="display:inline-block;"><asp:DropDownList runat="server" ID="cboMonedaPartidaPresupuestaria" Style="width: 90px;"></asp:DropDownList></div>
				</div>
			</asp:Panel>

			<asp:Panel runat="server" ID="pnlBajaLogica" Style="clear: both; margin-left: 5px; padding-top: 5px;">
				<asp:CheckBox runat="server" ID="chkDarBajaLogica" CssClass="Etiqueta" />
			</asp:Panel>

			<asp:Panel runat="server" ID="pnlObservaciones" Style="clear: both; margin-left: 5px; padding-top: 5px;">
				<div style="width: 100%;">
					<asp:Label runat="server" ID="lblLitObservaciones"></asp:Label>:
				</div>
				<div style="width: 100%;">
					<asp:TextBox runat="server" ID="txtObservaciones" TextMode="MultiLine" Style="width: 96%; height: 40px;"></asp:TextBox>
				</div>
			</asp:Panel>
		</div>
		<div style="padding-top: 10px; width:49%; font-size:0em; display:inline-block;">
			<fsn:FSNButton ID="btnAceptar" runat="server" style="margin-right:0.5em;" />
		</div>
		<div style="padding-top: 10px; width:49%; font-size:0em; display:inline-block;">
			<fsn:FSNButton ID="btnCancelar" runat="server" Alineacion="Left" style="margin-left:0.5em;" />
		</div>

		<ajx:ModalPopupExtender ID="mpError" runat="server"
			TargetControlID="btnHidden"
			PopupControlID="pnError"
			CancelControlID="btnOk"
			BackgroundCssClass="modalBackground" />

		<asp:Button runat="server" ID="btnHidden" Style="display: none;" />
		<asp:Panel runat="server" ID="pnError" CssClass="BorderedPopUp" Style="display: none; padding: 10px; width: 250px;">
			<div style="padding-right: 10px;">
				<asp:Image runat="server" ID="imgError" SkinID="ErrorGrande" />
			</div>
			<div style="margin-left: 10px;">
				<asp:UpdatePanel runat="server" ID="upError" UpdateMode="Conditional">
					<ContentTemplate>
						<asp:Label runat="server" ID="lblError"></asp:Label>
					</ContentTemplate>
				</asp:UpdatePanel>
			</div>
			<div style="clear: both; float: none; margin-left: 50%; margin-right: 50%; padding-top: 10px;">
				<fsn:FSNButton ID="btnOk" runat="server" Style="float: none;" />
			</div>
		</asp:Panel>
		<script type="text/javascript">
            $(window).ready(function () {
                $(window).focus();
                $(window).select();
                $(window).height($("#capa").height() + 100);
                $(window).blur(function () {
                    if ($("*:focus").parents("#capa").length == 0) {
                        $(window).focus();
                        $(window).select();
                    }
                });
            });
		</script>
	</form>
</body>
</html>
