<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ControlPresupuestario.aspx.vb" Inherits="Fullstep.FSNWeb.ControlPresupuestario" MasterPageFile="~/App_Master/Menu.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function LlamadaBuscadorCC() {
            var valor = '';
            var UON1 = '';
            var UON2 = '';
            var UON3 = '';
            var UON4 = '';
            var aux;

            oElem = window.document.getElementById('<%= hidCC.clientID %>')
            if (oElem) {
                valor = oElem.value
            }
            if (valor != '') {
                aux = valor.split('#');
                for (i = 0; i < aux.length; i++)
                    if (i == 0)
                        UON1 = aux[i];
                    else
                        if (i == 1)
                            UON2 = aux[i];
                        else
                            if (i == 2)
                                UON3 = aux[i];
                            else
                                if (i == 3)
                                    UON4 = aux[i];

            }
            window.open(rutaFS + "PM/CentrosCoste/centrosCoste.aspx?VerUON=1&UON1=" + UON1 + "&UON2=" + UON2 + "&UON3=" + UON3 + "&UON4=" + UON4 + "&desde=ControlPresupuestario", "ventanaCentroCoste", "width=750,height=475,status=yes,resizable=no,top=200,left=200")
        }

        function CentroCoste_seleccionado(control, sUON1, sUON2, sUON3, sUON4, sDen) {
            var sAux = '';

            //lo que se guarda en valor_text
            if (sUON1)
                sAux = sUON1;
            else
                sAux = '#'
            if (sUON2)
                sAux = sAux + '#' + sUON2;
            else
                sAux = sAux + '#'
            if (sUON3)
                sAux = sAux + '#' + sUON3;
            else
                sAux = sAux + '#'
            if (sUON4)
                sAux = sAux + '#' + sUON4;
            else
                sAux = sAux + '#'

            oElem = window.document.getElementById('<%= hidCC.ClientID %>')
            if (oElem.value != "") {
                window.document.getElementById('<%= hidDefault.ClientID %>').value = "0";
            }
            if (oElem) {
                oElem.value = sAux
            }
            oElem = window.document.getElementById('<%= txtCC.ClientID %>')
            if (oElem) {
                oElem.value = sDen
                window.document.getElementById('<%= hidCCDen.ClientID %>').value = sDen
                if (sDen != '') {
                    document.getElementById('<%= txtPartida.ClientID %>').disabled = false;

                    var AutoComplete = $find('<%= txtPartida_AutoCompleteExtender.ClientID %>')
                    var chkBajaLogica = document.getElementById('<%= chkVerBajasLogicas.ClientID %>')
                    if (chkBajaLogica.checked) {
                        AutoComplete.set_contextKey(sAux + "@@1")
                    } else {
                        AutoComplete.set_contextKey(sAux + "@@0")
                    }
                }
            }
            window.document.getElementById('<%= hidPermisosCargados.clientID %>').value = "0"
        }

        //Para abrir los popup de edici�n y creaci�n

        function whdgPartidas_SelectionChanged(sender, e) {
            var cell = e.getSelectedCells().getItem(0);

            if (cell) {
                var skey = cell.get_column().get_key();
                var row = cell.get_row()
                if (cell.get_value() == "") {
                    return false;
                }
                rowSel = row
                var SelRow = rowSel.get_index() + "#"
                if (skey == "IMGHISTORICO")
                    SelRow = ''
                while (rowSel._owner._parentRow) {
                    SelRow = rowSel._owner._parentRow._index + "#" + SelRow
                    rowSel = rowSel._owner._parentRow
                }
                document.getElementById('<%= hidSelRow.ClientID %>').value = SelRow

                var permisos = document.getElementById('<%= hidPermisosCentroCoste.ClientID %>').value
                var alta = permisos.split(";;")[0]
                var modif = permisos.split(";;")[1]
                var presup = permisos.split(";;")[2]

                var parametros = "permisos=" + permisos
                var spres0 = "";
                var spres1 = "";
                var spres2 = "";
                var spres3 = "";
                var spres4 = "";
                var scambios = 0;
                spres2, spres3, spres4, scambios;
                if (row.get_cellByColumnKey("PRES0")) spres0 = row.get_cellByColumnKey("PRES0").get_value();
                if (row.get_cellByColumnKey("PRES1")) spres1 = row.get_cellByColumnKey("PRES1").get_value();
                if (row.get_cellByColumnKey("PRES2")) spres2 = row.get_cellByColumnKey("PRES2").get_value();
                if (row.get_cellByColumnKey("PRES3")) spres3 = row.get_cellByColumnKey("PRES3").get_value();
                if (row.get_cellByColumnKey("PRES4")) spres4 = row.get_cellByColumnKey("PRES4").get_value();
                if (row.get_cellByColumnKey("CONTROLCAMBIOS")) scambios = row.get_cellByColumnKey("CONTROLCAMBIOS").get_value();

                switch (skey) {
                    case "AGREGAR":
                        if (cell.get_value().split("images/")[1].split(".")[0] == "Agregar") {
                            if (alta == "") {
                                return false;
                            } else { parametros = parametros + "&alta=1" }
                        } else {
                            //Es una partida de tipo externo
                            __doPostBack('<%= hidEvent.UniqueID %>', JSON.stringify({ key: skey, Pres0: spres0, Pres1: spres1, Pres2: spres2, Pres3: spres3, Pres4: spres4, Cambios: scambios }));
                            return false;
                        }
                        break;

                    case "ACCION":
                        if (modif == "" && presup == "") {
                            return false;
                        }
                        break;

                    case "IMGHISTORICO":
                        if (row.get_cellByColumnKey("HISTORICO").get_value() == 0) {
                        }
                        //Tiene que abrir el hist�rico
                        __doPostBack('<%= hidEvent.UniqueID %>', JSON.stringify({ key: skey, Pres0: spres0, Pres1: spres1, Pres2: spres2, Pres3: spres3, Pres4: spres4 }));
                        return false;
                        break;
                    default:
                        return false;
                        break;
                }
                for (i = 0; i <= 4; i++) {
                    if (row.get_cellByColumnKey("PRES" + i)) {
                        if (row.get_cellByColumnKey("PRES" + i).get_value()) {
                            parametros = parametros + "&pres" + i + "=" + row.get_cellByColumnKey("PRES" + i).get_value()
                        }
                    } else {
                        parametros = parametros + "&pres" + i + "="
                    }

                }

                parametros = parametros + "&uons=" + document.getElementById('<%= hidCC.ClientID %>').value.replace(/#/g, ";;")
                newwindow = window.open(rutaSM + "ControlGasto/MantenimientoControlPresupuestacion.aspx?" + parametros, "_blank", "width=400,height=350,status=yes,resizable=yes,top=200,left=200")
                if (window.focus) { newwindow.focus() }
                return false;
            }
        }

        /*
        Comprobamos que exista un centro de coste seleccionado para la busqueda
        */
        function ComprobarCentroCoste() {
            if (document.getElementById('<%= txtCC.ClientID %>')) {
                if (document.getElementById('<%= txtCC.ClientID %>').value == '') {
                    alert(msgErrorCentroCoste);
                    return false;
                }
            }
        }

        function CambioAutoComplete() {
            var AutoComplete = $find('<%= txtPartida_AutoCompleteExtender.ClientID %>');
            var hidden = document.getElementById('<%= hidCC.ClientID %>').value
            var chkBajaLogica = document.getElementById('<%= chkVerBajasLogicas.ClientID %>')
            if (chkBajaLogica.checked) {
                AutoComplete.set_contextKey(hidden + "@@1")
            } else {
                AutoComplete.set_contextKey(hidden + "@@0")
            }
        }

        function ActualizarGrid() {
            document.getElementById('<%= hidRecargar.ClientID %>').value = "1"
            __doPostBack('<%= btnHidden.UniqueID %>', '')
        }

        /*
        <summary>Provoca el postback para hacer la esportaci�n a excel</summary>
        <remarks>Llamada desde: Icono de exportar a Excel</remarks>
        */
        function ExportarExcel() {
            __doPostBack("", "Excel");
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
    <fsn:FSNPageHeader ID="FSNPageHeader" runat="server" UrlImagenCabecera="~/images/ControlPresupuestario.gif"></fsn:FSNPageHeader>
    <div style="margin-top:0.5em; padding-left:10px; line-height:20px;">
        <asp:Label  ID="lblLitCentroCoste" runat="server" SkinID="EtiquetaNegrita" style="display:inline-block; width:10em; vertical-align:middle; margin-right:1em;"></asp:Label>
        <asp:TextBox ID="txtCC" runat="server" Width="500px" ReadOnly="true"></asp:TextBox>                        
        <asp:ImageButton id="imgCCLupa" runat="server" SkinID="BuscadorLupa" /> 
        <asp:Label ID="lblCentroCoste" runat="server" Visible="false"></asp:Label>
        <asp:HiddenField ID="hidCC" runat="server"/>
        <asp:HiddenField ID="hidCCDen" runat="server"/>
        <asp:HiddenField ID="hidDefault" runat="server" Value="1"/>
    </div>    
    <div style="padding-top:10px; padding-left:10px;">    
        <asp:Label ID="lblLitBuscarPartida" runat="server" SkinID="EtiquetaNegrita" style="display:inline-block; width:10em; vertical-align:middle; margin-right:1em;"></asp:Label>
        <asp:TextBox ID="txtPartida" runat="server" Width="500px">
        </asp:TextBox> 
        <ajx:AutoCompleteExtender ID="txtPartida_AutoCompleteExtender" runat="server"
            TargetControlID="txtPartida" MinimumPrefixLength="2"
            ServiceMethod="GetPartidasPresupuestarias"
            CompletionInterval="1000" UseContextKey="true">
        </ajx:AutoCompleteExtender>         
        <asp:CheckBox ID="chkVerBajasLogicas" runat="server" Text="dVer bajas l�gicas" AutoPostBack="false" onClick="CambioAutoComplete()" />    
        <div style="display:inline-block; vertical-align:middle; margin-left:1em;">
            <fsn:FSNButton ID="btnBuscar" runat="server" Text="DBuscar" Alineacion="Left"></fsn:FSNButton>
        </div>
    </div>    
    <div style="padding-top:10px; padding-left:10px;">
        <asp:Label ID="lblLitFechaInicio" runat="server" SkinID="EtiquetaNegrita" style="display:inline-block; width:10em; vertical-align:middle; margin-right:1em;"></asp:Label>
        <asp:UpdatePanel runat="server" ID="upFecIni" UpdateMode="Conditional" style="display:inline-block; vertical-align:middle;">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <igpck:WebDatePicker ID="FecInicio" runat="server" DropDownCalendarID="calendarioPicker" SkinID="Calendario" Width="90px"></igpck:WebDatePicker>
                <igpck:WebMonthCalendar runat="server" ID="calendarioPicker"></igpck:WebMonthCalendar>
            </ContentTemplate>
        </asp:UpdatePanel>  
        <asp:Label ID="lblLitFechaFin" runat="server" SkinID="EtiquetaNegrita" style="display:inline-block; width:10em; vertical-align:middle; margin-right:1em; margin-left:2em;"></asp:Label>
        <asp:UpdatePanel runat="server" ID="upFecFin" UpdateMode="Conditional" style="display:inline-block; vertical-align:middle;">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <igpck:WebDatePicker ID="FecFin" runat="server" dropdowncalendarID="calendarioPicker" SkinID="Calendario" Width="90px"></igpck:WebDatePicker>
            </ContentTemplate>
        </asp:UpdatePanel> 
    </div> 
    <hr /> 
    <asp:HiddenField runat="server" ID="hidSelRow" />
    <ig:WebExcelExporter ID="excelExporter" runat="server"></ig:WebExcelExporter>
    <div style="padding:0px 10px; margin:1em;">
        <asp:UpdatePanel runat="server" ID="upPartidasPresupuestarias" UpdateMode="Conditional">
            <Triggers>                
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <asp:HiddenField ID="hidPermisosCargados" runat="server"/>
                <asp:HiddenField ID="hidPermisosCentroCoste" runat="server"/>
                <asp:HiddenField ID="hidRecargar" runat="server"/>
                <asp:HiddenField ID="hidEvent" runat="server"/>
                <div class="CabeceraBotones" style="line-height:2em; text-align:right;">
                    <div onclick="ExportarExcel()" class="botonDerechaCabecera" style="display:inline-block; float:none;">            
						<asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
						<asp:Label runat="server" ID="lblExcel" CssClass="fntLogin2" style="margin-left:3px;" Text="dExcel"></asp:Label>           
					</div>
                </div>
                <ig:WebHierarchicalDataGrid ID="whdgPartidas" runat="server" Width="100%" AutoGenerateColumns="false" AutoGenerateBands="false"
					EnableAjax="False" ShowHeader="True" DataKeyFields="PRES0">
                    <ExpandCollapseAnimation SlideOpenDirection="Auto" SlideOpenDuration="300" SlideCloseDirection="Auto" SlideCloseDuration="300" />
                    <Bands>
                        <ig:Band Key="PRES5_NIV1" DataMember="PRES5_NIV1" DataKeyFields="KEY_SIN_ESPACIO" AutoGenerateColumns="false" ShowHeader="true">
                            <Behaviors>
                                <ig:Paging Enabled="true" EnableInheritance="true" PagerAppearance="Bottom" PagerMode="NumericFirstLast"></ig:Paging>
                                <ig:Sorting SortingMode="Multi" Enabled="true" />
                                <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true">
                                    <ColumnSettings>
						                <ig:ColumnFilteringSetting ColumnKey="EXTERNO" Enabled="false" />
                                        <ig:ColumnFilteringSetting ColumnKey="AGREGAR" Enabled="false" />
                                        <ig:ColumnFilteringSetting ColumnKey="ACCION" Enabled="false" />
                                        <ig:ColumnFilteringSetting ColumnKey="IMGHISTORICO" Enabled="false" />
					                </ColumnSettings>
                                </ig:Filtering>
                            </Behaviors>
                            <Bands>
                                <ig:Band Key="PRES5_NIV2" DataMember="PRES5_NIV2" DataKeyFields="KEY_SIN_ESPACIO" AutoGenerateColumns="false" ShowHeader="true">
                                    <Behaviors>
                                        <ig:Paging Enabled="true" EnableInheritance="true" PagerAppearance="Bottom" PagerMode="NumericFirstLast"></ig:Paging>
                                        <ig:Sorting SortingMode="Multi" Enabled="true" />
                                        <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true">
                                            <ColumnSettings>
						                        <ig:ColumnFilteringSetting ColumnKey="EXTERNO" Enabled="false" />
                                                <ig:ColumnFilteringSetting ColumnKey="AGREGAR" Enabled="false" />
                                                <ig:ColumnFilteringSetting ColumnKey="ACCION" Enabled="false" />
                                                <ig:ColumnFilteringSetting ColumnKey="IMGHISTORICO" Enabled="false" />
					                        </ColumnSettings>
                                        </ig:Filtering>
                                    </Behaviors>
                                    <Bands>
                                        <ig:Band Key="PRES5_NIV3" DataMember="PRES5_NIV3" DataKeyFields="KEY_SIN_ESPACIO" AutoGenerateColumns="false" ShowHeader="true">
                                            <Behaviors>
                                                <ig:Paging Enabled="true" EnableInheritance="true" PagerAppearance="Bottom" PagerMode="NumericFirstLast"></ig:Paging>
                                                <ig:Sorting SortingMode="Multi" Enabled="true" />
                                                <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true">
                                                    <ColumnSettings>
						                                <ig:ColumnFilteringSetting ColumnKey="EXTERNO" Enabled="false" />
                                                        <ig:ColumnFilteringSetting ColumnKey="AGREGAR" Enabled="false" />
                                                        <ig:ColumnFilteringSetting ColumnKey="ACCION" Enabled="false" />
                                                        <ig:ColumnFilteringSetting ColumnKey="IMGHISTORICO" Enabled="false" />
					                                </ColumnSettings>
                                                </ig:Filtering>
                                            </Behaviors>
                                            <Bands>
                                                <ig:Band Key="PRES5_NIV4" DataMember="PRES5_NIV4" DataKeyFields="PRES4" AutoGenerateColumns="false" ShowHeader="true">
                                                    <Behaviors>
                                                        <ig:Paging Enabled="true" EnableInheritance="true" PagerAppearance="Bottom" PagerMode="NumericFirstLast"></ig:Paging>
                                                        <ig:Sorting SortingMode="Multi" Enabled="true" />
                                                        <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true">
                                                            <ColumnSettings>
						                                        <ig:ColumnFilteringSetting ColumnKey="EXTERNO" Enabled="false" />
                                                                <ig:ColumnFilteringSetting ColumnKey="AGREGAR" Enabled="false" />
                                                                <ig:ColumnFilteringSetting ColumnKey="ACCION" Enabled="false" />
                                                                <ig:ColumnFilteringSetting ColumnKey="IMGHISTORICO" Enabled="false" />
					                                        </ColumnSettings>
                                                        </ig:Filtering>
                                                    </Behaviors>
                                                </ig:Band>
                                            </Bands>
                                        </ig:Band>
                                    </Bands>
                                </ig:Band>
                            </Bands>
                        </ig:Band>
                    </Bands>
                    <Behaviors>                        
                        <ig:Activation Enabled="true"/>
                        <ig:ColumnResizing Enabled="true" />
                        <ig:Selection Enabled="true"  RowSelectType="Single" CellSelectType="Single" ColumnSelectType="Single" CellClickAction="Cell">
                            <SelectionClientEvents CellSelectionChanged="whdgPartidas_SelectionChanged" />
                        </ig:Selection>
                        <ig:RowSelectors Enabled="false" RowNumbering="false"></ig:RowSelectors>                        
                        <ig:Paging Enabled="true" EnableInheritance="true" PagerAppearance="Top" PagerMode="NumericFirstLast"></ig:Paging>
                        <ig:Sorting SortingMode="Multi" Enabled="true" />
                        <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true">
                            <ColumnSettings>
						        <ig:ColumnFilteringSetting ColumnKey="EXTERNO" Enabled="false" />
                                <ig:ColumnFilteringSetting ColumnKey="AGREGAR" Enabled="false" />
                                <ig:ColumnFilteringSetting ColumnKey="ACCION" Enabled="false" />
                                <ig:ColumnFilteringSetting ColumnKey="IMGHISTORICO" Enabled="false" />
					        </ColumnSettings>
                        </ig:Filtering>
                    </Behaviors>
                </ig:WebHierarchicalDataGrid>
                <ig:WebHierarchicalDataGrid ID="whdgPartidasExport" runat="server" AutoGenerateColumns="false" AutoGenerateBands="false"
                     DataKeyFields="PRES0" Visible="false">
                    <Columns>
                        <ig:BoundDataField Key="EXTERNO" DataFieldName="EXTERNO" Hidden="true" VisibleIndex="0"></ig:BoundDataField>
                        <ig:BoundDataField Key="DEN" DataFieldName="DEN" Hidden="false" VisibleIndex="1"></ig:BoundDataField>
                        <ig:BoundDataField Key="CODEN" DataFieldName="CODEN" Hidden="false" VisibleIndex="2"></ig:BoundDataField>
                        <ig:BoundDataField Key="FECINI" DataFieldName="FECINI" Hidden="false" VisibleIndex="3"></ig:BoundDataField>
                        <ig:BoundDataField Key="FECFIN" DataFieldName="FECFIN" Hidden="false" VisibleIndex="4"></ig:BoundDataField>
                        <ig:BoundDataField Key="PRESUPUESTADO" DataFieldName="PRESUPUESTADO" Hidden="false" VisibleIndex="5"></ig:BoundDataField>
                        <ig:BoundDataField Key="COMPROMETIDO" DataFieldName="COMPROMETIDO" Hidden="false" VisibleIndex="6"></ig:BoundDataField>
                        <ig:BoundDataField Key="SOLICITADO" DataFieldName="SOLICITADO" Hidden="false" VisibleIndex="7"></ig:BoundDataField>
                        <ig:BoundDataField Key="DISPONIBLE" DataFieldName="DISPONIBLE" Hidden="false" VisibleIndex="8"></ig:BoundDataField>
                        <ig:BoundDataField Key="CONTROLCAMBIOS" DataFieldName="CONTROLCAMBIOS" Hidden="false" VisibleIndex="9"></ig:BoundDataField>
                        <ig:BoundDataField Key="MON" DataFieldName="MON" Hidden="false" VisibleIndex="10"></ig:BoundDataField>
                        <ig:BoundDataField Key="BAJALOG" DataFieldName="BAJALOG" Hidden="false" VisibleIndex="11"></ig:BoundDataField>
                        <ig:BoundDataField Key="HISTORICO" DataFieldName="HISTORICO" Hidden="false" VisibleIndex="12"></ig:BoundDataField>
                        <ig:BoundDataField Key="PRES0" DataFieldName="PRES0" Hidden="false" VisibleIndex="13"></ig:BoundDataField>
                        <ig:BoundDataField Key="KEY_SIN_ESPACIO" DataFieldName="KEY_SIN_ESPACIO" Hidden="true" VisibleIndex="14"></ig:BoundDataField>
                    </Columns>
                    <Bands>
                        <ig:Band Key="PRES5_NIV1" DataMember="PRES5_NIV1" DataKeyFields="KEY_SIN_ESPACIO" AutoGenerateColumns="false">
                            <Bands>
                                <ig:Band Key="PRES5_NIV2" DataMember="PRES5_NIV2" DataKeyFields="KEY_SIN_ESPACIO" AutoGenerateColumns="false">
                                    <Bands>
                                        <ig:Band Key="PRES5_NIV3" DataMember="PRES5_NIV3" DataKeyFields="KEY_SIN_ESPACIO" AutoGenerateColumns="false">
                                            <Bands>
                                                <ig:Band Key="PRES5_NIV4" DataMember="PRES5_NIV4" DataKeyFields="PRES4" AutoGenerateColumns="false">
                                                </ig:Band>
                                            </Bands>
                                        </ig:Band>
                                    </Bands>
                                </ig:Band>
                            </Bands>
                        </ig:Band>
                    </Bands>
                </ig:WebHierarchicalDataGrid>
            </ContentTemplate>
        </asp:UpdatePanel>        
    </div> 
    <ajx:ModalPopupExtender ID="ModalCargando" runat="server" 
        TargetControlID="btnHidden2"
	    BackgroundCssClass="modalBackground" 
	    PopupControlID="pnlCargando" 
	    BehaviorID="ModalCargando">
    </ajx:ModalPopupExtender>        
    <asp:Button runat="server" ID="btnHidden2" style="display:none;"/>
    <asp:Panel runat="server" ID="pnlCargando" style="display:none;">
        <div class="updateProgress">
            <div style="position: relative; top: 30%; text-align: center;">
                <asp:Image ID="ImgProgress" runat="server" SkinId="ImgProgress" />
            </div>            
        </div>
    </asp:Panel>           
    <ajx:ModalPopupExtender runat="server" ID="mpeHistorico"
        PopupControlID="pnlPartidaPresupuestaria"    
        TargetControlID="btnHidden" 
        CancelControlID="btnCerrar"
        OkControlID="imgCerrarHistorico"
        BackgroundCssClass="modalBackground">
    </ajx:ModalPopupExtender>
    <asp:Button runat="server" ID="btnHidden" style="display:none;"/> 
    <asp:Panel runat="server" ID="pnlPartidaPresupuestaria" style="display:none;" CssClass="BorderedPopUp">
        <div>    
            <div style="position:absolute; top:18px; right:12px;">
                <asp:ImageButton runat="server" ID="imgCerrarHistorico" SkinID="Cerrar" />
            </div>
            <div style="clear:both; float:left; width:800px; padding:10px;">                 
                <fssm:HistoricoCambios ID="Historico" runat="server"></fssm:HistoricoCambios>
            </div>
            <div style="clear:both; margin-top:10px; width:800px; padding-left:50%; padding-right:50%">                 
                <fsn:FSNButton ID="btnCerrar" runat="server" Text="Cerrar" style="float:none;" />
            </div>
        </div>        
    </asp:Panel>      
    <!-- Panel Cargando... -->
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(MostrarCargando);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(OcultarCargando);

        function OcultarCargando() {
            $find(ModalProgressClientID).hide();
        }
    </script>
</asp:Content>
