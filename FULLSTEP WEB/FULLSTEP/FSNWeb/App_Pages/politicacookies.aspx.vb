﻿Imports Fullstep.FSNServer
Imports Fullstep.FSNWeb.FSNPage

Public Class politicacookies
    Inherits System.Web.UI.Page

    'composición de FSNPage en lugar de herencia para evitar "sortear" el login
    Private pag As FSNPage = New FSNPage

    Private Function getNavegador() As String
        Dim nav As String = Request.Browser.Browser

        Select Case nav
            Case "Firefox"
                Return "MOZ"
            Case "Chrome"
                Return "CHR"
            Case "Safari"
                Return "SAF"
            Case Else
                Return "IE"
        End Select

    End Function


    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
        pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.PoliticaCookies
        Me.Title = System.Configuration.ConfigurationManager.AppSettings.Item("TituloVentanas_" & pag.Idioma)
        Me.lblAlerta.Text = pag.Textos(0)
        Me.lblTitulo.Text = pag.Textos(1)
        Me.Label3.Text = pag.Textos(2)
        Me.Label4.Text = pag.Textos(3)
        Me.Label5.Text = pag.Textos(4)
        Me.Label6.Text = pag.Textos(5)
        Me.Label7.Text = pag.Textos(6)
        Me.Label8.Text = pag.Textos(7)
        Me.Label9.Text = pag.Textos(8)
        Me.Label10.Text = pag.Textos(9)
        Me.Label11.Text = pag.Textos(10)
        Me.Label12.Text = pag.Textos(11)
        Me.Label13.Text = pag.Textos(12) & " " & Request.Browser.Browser
        Me.lblVolver.Text = pag.Textos(13)
        Me.imgExplorer.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/" & pag.Idioma & "_" & Me.getNavegador() & ".png"
        'mejora de accesibilidad añadiendo title a imagen
        Me.imgExplorer.Attributes("alt") = Me.Label13.Text
        Me.lblInstrucciones.Text = pag.Textos(14)
        Select Case Me.getNavegador
            Case "MOZ"
                Me.lblInstrucciones.Text = pag.Textos(15)
            Case "CHR"
                Me.lblInstrucciones.Text = pag.Textos(16)
            Case "OPR"
                Me.lblInstrucciones.Text = pag.Textos(17)
            Case "SAF"
                Me.lblInstrucciones.Text = pag.Textos(18)
        End Select
    End Sub

End Class