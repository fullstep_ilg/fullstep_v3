﻿Public Class PedidoAprobadoMail
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Login
        lblTexto.Text = Server.UrlDecode(Request("Texto"))
        linkCerrar.InnerText = Me.Textos(12)
    End Sub

End Class