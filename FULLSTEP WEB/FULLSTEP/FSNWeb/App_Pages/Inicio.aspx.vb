﻿Partial Public Class Inicio
    Inherits FSNPage

    Private ReadOnly Property HayAutenticacionWindows() As Boolean
        Get
            If Session("HayAutenticacionWindows") Is Nothing Then
                Dim FSNServer As FSNServer.Root = Session("FSN_Server")
                If Not FSNServer Is Nothing Then
                    Return FSNServer.TipoDeAutenticacion = TiposDeAutenticacion.Windows
                Else
                    FSNServer = New FSNServer.Root
                    Session("HayAutenticacionWindows") = FSNServer.TipoDeAutenticacion = TiposDeAutenticacion.Windows
                    Return FSNServer.TipoDeAutenticacion = TiposDeAutenticacion.Windows
                End If
            Else
                Return Session("HayAutenticacionWindows")
            End If
        End Get
    End Property
    ''' <summary>
    ''' Inicia el proceso de editar un control WebPart.
    ''' </summary>
    ''' <param name="webPart">Webpart a editar</param>
    ''' <remarks>Llamada desde: clicked.</remarks>
    Public Overridable Sub BeginWebPartEditing(ByVal webPart As WebPart)
        If WebPartManager1.SelectedWebPart Is Nothing Then
            WebPartManager1.BeginWebPartEditing(webPart)
        End If
    End Sub
    ''' <summary>
    ''' Crea un nuevo verbo para que aparezca en los webparts. Este verbo va a hacer la edición pero
    ''' antes vamos a llamar al show del ModalPopupExtender
    ''' </summary>
    ''' <remarks>Llamada desde: OnCreateVerbs de la WebPartZone.</remarks>
    Sub editar(ByVal sender As Object, ByVal e As WebPartVerbsEventArgs)
        Dim arr As ArrayList = New ArrayList
        Dim v As WebPartVerb = New WebPartVerb("v1", New WebPartEventHandler(AddressOf clicked))
        v.ImageUrl = "images/informe.gif"
        arr.Add(v)
        e.Verbs = New WebPartVerbCollection(arr)
    End Sub
    ''' <summary>
    ''' Cuando se haga click sobre el verbo creado, se llamará a BeginWebPartEditing para que ponga ese
    ''' webpart en modo edición.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: Al hacer click sobre el verbo.</remarks>
    Public Sub clicked(ByVal sender As Object, ByVal e As WebPartEventArgs)
        BeginWebPartEditing(e.WebPart)
    End Sub
    ''' <summary>
    ''' Inicializa el WebPartManager en modo edición
    ''' </summary>
    ''' <remarks>Se produce cuando el control de servidor se carga en el objeto Page.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		ModuloIdioma = TiposDeDatos.ModulosIdiomas.Webparts
		For Each iWebPart As WebPart In WebPartManager1.WebParts
			If iWebPart.WebBrowsableObject.GetType().BaseType.Name = GetType(UserControls_FSNInfoSoporte).Name Then
				CType(iWebPart.WebBrowsableObject, UserControls_FSNInfoSoporte).Imagen = "~/App_Themes/" & String.Format("{0}/images/{1}", Me.Page.Theme, "telefono.jpg")
			End If
		Next

		If Not IsPostBack() Then
			Try
				WebPartManager1.DisplayMode = WebPartManager.EditDisplayMode
			Catch ex As ArgumentException
				Response.Redirect(ConfigurationManager.AppSettings("ruta") & "default.aspx")
				Response.End()
			End Try

			CargarTextos()
		End If
	End Sub
	''' <summary>
	''' Establece el modo catálogo en el WebPartManager
	''' </summary>
	''' <remarks>Se produce cuando se hace clic en el control FSNButton btnCatalogo.</remarks>
	Protected Sub btnCatalogo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCatalogo.Click
		Try
			WebPartManager1.DisplayMode = WebPartManager.CatalogDisplayMode
		Catch ex As ArgumentException
			Response.Redirect(ConfigurationManager.AppSettings("ruta") & "default.aspx")
			Response.End()
		End Try
	End Sub
	''' Revisado por: blp. Fecha: 31/10/2012
	''' <summary>
	''' Determina si se puede agregar el control en función de la propiedad AuthorizeWebPart y los permisos del usuario
	''' </summary>
	''' <param name="sender">Origen del evento.</param>
	''' <param name="e">WebPartAuthorizationEventArgs que contiene los datos del evento.</param>
	''' <remarks>Se produce cuando se llama al método IsAuthorized para determinar si se puede agregar un control WebPart o un control de servidor a una página.</remarks>
	Private Sub WebPartManager1_AuthorizeWebPart(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.WebParts.WebPartAuthorizationEventArgs) Handles WebPartManager1.AuthorizeWebPart
		Select Case e.AuthorizationFilter
			Case "PM"
				e.IsAuthorized = FSNUser.AccesoPM _
					And Acceso.gsAccesoFSPM <> TiposDeDatos.TipoAccesoFSPM.SinAcceso
			Case "QA"
				e.IsAuthorized = FSNUser.AccesoQA _
					And Acceso.gsAccesoFSQA <> TiposDeDatos.TipoAccesoFSQA.SinAcceso
			Case "QAPuntuacion"
				e.IsAuthorized = FSNUser.AccesoQA _
					And Acceso.gsAccesoFSQA <> TiposDeDatos.TipoAccesoFSQA.SinAcceso _
					And FSNUser.QAPuntuacionProveedores
			Case "QAMateriales"
				e.IsAuthorized = FSNUser.AccesoQA _
					And Acceso.gsAccesoFSQA <> TiposDeDatos.TipoAccesoFSQA.SinAcceso _
					And FSNUser.QAMantenimientoMat
			Case "QACertificados"
				e.IsAuthorized = FSNUser.AccesoQA _
					And Acceso.gsAccesoFSQA <> TiposDeDatos.TipoAccesoFSQA.SinAcceso _
					And Acceso.gbAccesoQACertificados
			Case "QANoConformidades"
				e.IsAuthorized = FSNUser.AccesoQA _
					And Acceso.gsAccesoFSQA <> TiposDeDatos.TipoAccesoFSQA.SinAcceso _
					And Acceso.gbAccesoQANoConformidad
			Case "QAUNQA"
				e.IsAuthorized = FSNUser.AccesoQA _
					And Acceso.gsAccesoFSQA <> TiposDeDatos.TipoAccesoFSQA.SinAcceso _
					And FSNUser.QAMantenimientoUNQA
			Case "EP"
				e.IsAuthorized = Acceso.gbAccesoFSEP And FSNUser.AccesoEP
			Case "EPAprovisionador"
				e.IsAuthorized = Acceso.gbAccesoFSEP And FSNUser.AccesoEP _
					And (FSNUser.EPTipo = TipoAccesoFSEP.SoloAprovisionador Or FSNUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador)
			Case "EPAprobador"
				e.IsAuthorized = Acceso.gbAccesoFSEP And FSNUser.AccesoEP _
					And (FSNUser.EPTipo = TipoAccesoFSEP.SoloAprobador Or FSNUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador)
			Case "SM"
				e.IsAuthorized = Acceso.gbAccesoFSSM And FSNUser.AccesoSM
			Case "Oculto"
				e.IsAuthorized = False
			Case "IMAutofacturas"
				e.IsAuthorized = (Acceso.g_bAccesoFSFA AndAlso FSNUser.AccesoFACT) _
					AndAlso FSNUser.IMVisorAutofacturas
			Case "IMFacturas"
				e.IsAuthorized = (Acceso.g_bAccesoFSFA AndAlso FSNUser.AccesoFACT)
		End Select
	End Sub
	''' Revisado por:blp. Fecha: 19/11/2012
	''' <summary>
	''' Muestra u oculta las cabeceras de las zonas dependiendo del modo de presentación.
	''' </summary>
	''' <param name="sender">Origen del evento.</param>
	''' <param name="e">WebPartDisplayModeEventArgs que contiene los datos del evento.</param>
	''' <remarks>Se produce después de que el modo de presentación actual en una página de elementos Web ha cambiado.</remarks>
	Protected Sub WebPartManager1_DisplayModeChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.WebParts.WebPartDisplayModeEventArgs) Handles WebPartManager1.DisplayModeChanged
		If WebPartManager1.DisplayMode.Equals(WebPartManager.CatalogDisplayMode) Then
			MiCatalogo.SelectedCatalogPartID = MiCatalogo.CatalogParts(0).ID
			MiCatalogo.DataBind()
			btnCatalogo.Style("display") = "none"
			ZoneCentro.Style("display") = "none"
			ZoneIzquierda.Style("display") = "none"
			ZoneDerecha.Style("display") = "none"
		Else
			If WebPartManager1.DisplayMode.Equals(WebPartManager.BrowseDisplayMode) Then _
			WebPartManager1.DisplayMode = WebPartManager.EditDisplayMode
			btnCatalogo.Style("display") = "block"
			ZoneCentro.Style("display") = "block"
			ZoneIzquierda.Style("display") = "block"
			ZoneDerecha.Style("display") = "block"
			Dim str As New StringBuilder()
			For i As Integer = 0 To 40
				str.Append("&nbsp; ")
			Next
			ZoneIzquierda.HeaderText = str.ToString()
			ZoneCentro.HeaderText = str.ToString()
			ZoneDerecha.HeaderText = str.ToString()
		End If
	End Sub
	''' <summary>
	''' Carga los textos en función del idioma de la página.
	''' </summary>
	''' <remarks>
	''' La página debe implementar la interface IFSNPage.
	''' Llamadas desde: Page_Load
	''' </remarks>
	Private Sub CargarTextos()
		btnCatalogo.Text = Textos(27)
		MiCatalogo.HeaderCloseVerb.Text = "<-- " & Textos(160)

		EditorWebParts.ErrorText = Textos(32)
		EditorWebParts.InstructionText = Textos(178)
		EditorWebParts.HeaderText = Textos(34)
		EditorWebParts.HeaderCloseVerb.Description = Textos(35)
		EditorWebParts.HeaderCloseVerb.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/Bt_Cerrar.png"
		EditorWebParts.CancelVerb.Description = Textos(36)
		EditorWebParts.CancelVerb.Text = Textos(39)
		EditorWebParts.ApplyVerb.Visible = False
		EditorWebParts.OKVerb.Description = Textos(38)
		EditorWebParts.OKVerb.Text = Textos(41)

		ZoneIzquierda.RestoreVerb.Description = Textos(42) & " '{0}'"
		ZoneCentro.RestoreVerb.Description = Textos(42) & " '{0}'"
		ZoneDerecha.RestoreVerb.Description = Textos(42) & " '{0}'"
		ZoneIzquierda.DeleteVerb.Description = Textos(43) & " '{0}'"
		ZoneCentro.DeleteVerb.Description = Textos(43) & " '{0}'"
		ZoneDerecha.DeleteVerb.Description = Textos(43) & " '{0}'"
		ZoneIzquierda.MinimizeVerb.Description = Textos(44) & " '{0}'"
		ZoneCentro.MinimizeVerb.Description = Textos(44) & " '{0}'"
		ZoneDerecha.MinimizeVerb.Description = Textos(44) & " '{0}'"

		WebPartManager1.DeleteWarning = Textos(54)
	End Sub
	''' <summary>
	''' Se produce después de agregar un control WebPart dinámico u otro control de servidor a una zona WebPartZoneBase para indicar que el control se agregó correctamente.
	''' Comprueba se se añade un WebPart de búsqueda de artículos y, en tal caso, llama de forma asíncrona al servicio autocompletar
	''' para que se genera en caché la tabla de artículos.
	''' </summary>
	''' <param name="sender">Origen del evento.</param>
	''' <param name="e">Objeto WebPartEventArgs que contiene los datos del evento. </param>
	''' <remarks></remarks>
	Private Sub WebPartManager1_WebPartAdded(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.WebParts.WebPartEventArgs) Handles WebPartManager1.WebPartAdded
		If TypeOf e.WebPart Is PMWebControls.FSPMWebPartAltaSolicitudes Then
			CType(e.WebPart, PMWebControls.FSPMWebPartAltaSolicitudes).Actualizar()
		ElseIf TypeOf e.WebPart Is PMWebControls.FSPMWebPartBusquedaSolicitudes Then
			CType(e.WebPart, PMWebControls.FSPMWebPartBusquedaSolicitudes).Actualizar()
		ElseIf TypeOf e.WebPart Is PMWebControls.FSPMWebPartFiltrosDisponibles Then
			CType(e.WebPart, PMWebControls.FSPMWebPartFiltrosDisponibles).Actualizar()
		ElseIf TypeOf e.WebPart Is PMWebControls.FSPMWebPartMonitorizacionSolicitudes Then
			CType(e.WebPart, PMWebControls.FSPMWebPartMonitorizacionSolicitudes).Actualizar()
		ElseIf TypeOf e.WebPart Is PMWebControls.FSPMWebPartSolicitudesPendientes Then
			CType(e.WebPart, PMWebControls.FSPMWebPartSolicitudesPendientes).Actualizar()
		ElseIf TypeOf e.WebPart Is PMWebControls.FSQAWebPartNoConfAbiertas Then
			CType(e.WebPart, PMWebControls.FSQAWebPartNoConfAbiertas).Actualizar()
		ElseIf TypeOf e.WebPart Is PMWebControls.FSQAWebPartNoConformidadesPend Then
			CType(e.WebPart, PMWebControls.FSQAWebPartNoConformidadesPend).Actualizar()
		ElseIf TypeOf e.WebPart Is PMWebControls.FSQAWebPartAltaNoConformidades Then
			CType(e.WebPart, PMWebControls.FSQAWebPartAltaNoConformidades).Actualizar()
		ElseIf TypeOf e.WebPart Is PMWebControls.FSQAWebPartCertificados Then
			CType(e.WebPart, PMWebControls.FSQAWebPartCertificados).Actualizar()
		ElseIf TypeOf e.WebPart Is PMWebControls.FSQAWebPartPanelCalidad Then
			CType(e.WebPart, PMWebControls.FSQAWebPartPanelCalidad).Actualizar()
		ElseIf TypeOf e.WebPart Is FSNWebControls.FSNAyuda Then
			CType(e.WebPart, FSNWebControls.FSNAyuda).Actualizar()
		ElseIf TypeOf e.WebPart Is PMWebControls.FSPMWebPartOtrasURLs Then
			CType(e.WebPart, PMWebControls.FSPMWebPartOtrasURLs).Actualizar()
		ElseIf TypeOf e.WebPart Is PMWebControls.FSPMWebPartFacturas Then
			CType(e.WebPart, PMWebControls.FSPMWebPartFacturas).Actualizar()
		End If

	End Sub
	''' <summary>
	''' Llama al mÃ©todo que enlace el DataList del elemento con los datos.
	''' </summary>
	''' <param name="sender">Origen del evento</param>
	''' <param name="e">AccordionItemEventArgs que contiene los datos del evento.</param>
	''' <remarks>Se produce al realizar el enlace de datos de un elemento del control Accordion.</remarks>
	Protected Sub AccorCatalogo_ItemDataBound(ByVal sender As Object, ByVal e As AjaxControlToolkit.AccordionItemEventArgs)
        If (e.ItemType = AjaxControlToolkit.AccordionItemType.Content) Then
            Dim lista As DataList = CType(e.AccordionItem.FindControl("lstWebParts"), DataList)
            Dim hidID As HiddenField = CType(e.AccordionItem.FindControl("hddCatalogID"), HiddenField)
            BindWebPartsList(lista, hidID.Value)
        ElseIf e.ItemType = AjaxControlToolkit.AccordionItemType.Header Then
            Dim lbl As Label = CType(e.AccordionItem.FindControl("lblcabecera"), Label)
            lbl.Text = Textos(161)
        End If
    End Sub
    ''' <summary>
    ''' Realiza el enlace a daots del DataList del control Accordion.
    ''' </summary>
    ''' <param name="Lista">DataList a enlazar</param>
    ''' <param name="ID">ID del catÃ¡logo del que se quieren cargar los elementos.</param>
    ''' <remarks>Llamadas desde: AccorCatalogo_ItemDataBound</remarks>
    Private Sub BindWebPartsList(ByVal Lista As DataList, ByVal ID As String)
        Lista.DataSource = MiCatalogo.Descriptions(ID)
        Lista.DataBind()
    End Sub
    ''' <summary>
    ''' Cambia el color de fondo, el color del texto, el texto e introduce una imagen en el botón cuando se hac
    ''' click sobre él. 
    ''' </summary>
    ''' <param name="sender">Botón sobre el que se ha hecho click.</param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: Al hacer click sobre cualquier botón, para añadir ese WebPart a la página de inicio. Tiempo máximo: 0sg.</remarks>
    Protected Sub fsnButtonAnadir_Click(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, FSNWebControls.FSNButton).Text = "<img src=" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/checked.gif" & " border=0>" & Textos(162)
        CType(sender, FSNWebControls.FSNButton).ColorFondo = System.Drawing.ColorTranslator.FromHtml("#FFFF80")
        CType(sender, FSNWebControls.FSNButton).ImagenesBordes = System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/Bg_Esquinas_Bt_Ctg.PNG"
        CType(sender, FSNWebControls.FSNButton).ColorFondoHover = System.Drawing.ColorTranslator.FromHtml("#FFFF80")
        CType(sender, FSNWebControls.FSNButton).ForeColor = Drawing.Color.Black
        MiCatalogo.AddWebPart(CType(sender, FSNWebControls.FSNButton).CommandArgument, "ZoneIzquierda")
    End Sub
    ''' <summary>
    ''' Por cada WebPart que se muestra en la zona Catálogo, escribimos el texto en el botón 
    ''' y el texto explicativo de ese WebPart que está en la tabla de idiomas.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: Al cargarse el catálogo. Tiempo máximo: 2 sg;</remarks>
    Protected Sub lstWebParts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        If ((e.Item.ItemType = ListItemType.Item) OrElse (e.Item.ItemType = ListItemType.AlternatingItem)) Then
            Dim btn As FSNWebControls.FSNButton
            Dim lbl As Label

            btn = CType(e.Item.FindControl("Button1"), FSNWebControls.FSNButton)
            btn.Text = Textos(30)

            lbl = CType(e.Item.FindControl("lblDescripcion"), Label)
            If Not lbl Is Nothing Then
                Dim hidID As HiddenField = CType(e.Item.FindControl("hddID"), HiddenField)
                Select Case hidID.Value
                    Case "gwpInfoSoporte"
						lbl.Text = Textos(163)
						CType(e.Item.FindControl("Image1"), Image).ImageUrl = "~/App_Themes/" & String.Format("{0}/images/{1}", Me.Page.Theme, "telefono.jpg")
					Case "PedidosCurso"
                        lbl.Text = Textos(164)
                    Case "CestaVirtual"
                        lbl.Text = Textos(165)
                    Case "PedidosFavoritos"
                        lbl.Text = Textos(166)
                    Case "FSEPWebPartBusqArticulos"
                        lbl.Text = Textos(167)
                    Case "FSPMWebPartAltaSolicitudes"
                        lbl.Text = Textos(168)
                    Case "FSPMWebPartSolicitudesPendientes"
                        lbl.Text = Textos(169)
                    Case "FSPMWebPartBusquedaSolicitudes"
                        lbl.Text = Textos(170)
                    Case "FSPMWebPartMonitorizacionSolicitudes"
                        lbl.Text = Textos(171)
                    Case "FSPMWebPartFiltrosDisponibles"
                        lbl.Text = Textos(172)
                    Case "FSQAWebPartNoConfAbiertas"
                        lbl.Text = Textos(173)
                    Case "FSQAWebPartNoConformidadesPend"
                        lbl.Text = Textos(174)
                    Case "FSQAWebPartAltaNoConformidades"
                        lbl.Text = Textos(175)
                    Case "FSQAWebPartCertificados"
                        lbl.Text = Textos(176)
                    Case "FSQAWebPartPanelCalidad"
                        lbl.Text = Textos(177)
                    Case "FSNAyuda"
                        lbl.Text = Textos(233)
                    Case "FSPMWebPartOtrasURLs"
                        lbl.Text = Textos(240)
                    Case "FSPMWebPartFacturas"
                        lbl.Text = Textos(256)
                    Case "FSPMWebPartAltaFacturas"
                        lbl.Text = Textos(258)
                    Case "FSPMWebPartBusquedaFacturas"
                        lbl.Text = Textos(273)
                    Case "FSPMWebPartFacturasPendientes"
                        lbl.Text = Textos(266)
                End Select
            End If
        End If
    End Sub
    ''' <summary>
    ''' Pone el texto correspondiente en la etiqueta lblProcesando.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: El prerender de la etiqueta. Tiempo máximo: 0sg.</remarks>
    Protected Sub LblProcesando_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, Label).Text = Textos(225)
    End Sub
    ''' <summary>
    ''' Si al aplicar los cambios no cumple con todas las validaciones, se cierra el modalpopupextender
    ''' y no deja ver los mensajes. Poniendo el show en el prerender nos deja ver dichos mensajes y 
    ''' realizar los cambios necesarios para guardar correctamente las propiedades del WebPart.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: El prerender de la página. Tiempo máximo: 0 sg.</remarks>
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not WebPartManager1.SelectedWebPart Is Nothing Then
            pnlEditorZone_ModalPopupExtender.Show()
        End If
    End Sub
End Class
