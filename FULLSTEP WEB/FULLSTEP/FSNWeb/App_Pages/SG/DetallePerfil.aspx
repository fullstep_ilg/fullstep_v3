﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="DetallePerfil.aspx.vb" Inherits="Fullstep.FSNWeb.DetallePerfil" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
	<script id="divPermisosContentMenu" type="text/x-jquery-tmpl">
		<div id='divMenu_' class='SeparadorRight' style='display:inline-block; *display:inline; zoom:1; font-size:12px; width:35%; margin-left:5px; margin-top:5px; margin-bottom:5px; vertical-align:top;'></div>			
		<div id='divAcciones_' class='SeparadorLeft' style='display:inline-block; *display:inline; zoom:1; width:60%; margin:0px -1px; padding:5px 0px; vertical-align:top;'>
			<p id='lblTituloAccionesMenu_' class='Texto14 CustomCaption SeparadorInf' style='display:none; line-height:2em; margin-left:20px; margin-top:0px; margin-bottom:0px;'></p>
			<div id='divAccionesMenu_' style='margin:5px 10px;'></div> 
		</div>
	</script>
	<script id="divPermisosContent" type="text/x-jquery-tmpl">
		<div id='divAccionesMenu_' style='margin:5px 10px;'></div> 
	</script>
	<script id="optionMenuItem" type="text/x-jquery-tmpl">
		<div style='line-height:20px; margin:2px 0px;'>
			<input id='chkAccion_${value}' name='chkAccion_${value}' type='checkbox' style='vertical-align:middle;' />
			<label for='chkAccion_${value}' class='Texto12' style='vertical-align:middle;'>${text}</label>
			<img alt='' id='imgAccion_${value}' src='${imgSrc}' style='{{if imgSrc==''}}display:none;{{/if}}cursor:pointer; vertical-align:middle; margin-left:5px;' />
		</div>
	</script>
    <script id="divOpcion" type="text/x-jquery-tmpl">
        <div id="divOpcion_${modulo}" style="display:inline-block; *display:inline; zoom:1; width:20%; line-height:20px; margin-top:5px; margin-left:10px; margin-bottom:5px;">
			<input id="chkAcceso_${modulo}" name="chkAcceso_${modulo}" type="checkbox" dependantPanel="divOpciones_${dependantPanel}" style="clear:both; float:left;" />
            <label id="lblAcceso_${modulo}" for="chkAcceso_${modulo}" class="Texto12" style="float:left;"/>
		</div>
    </script>
    <script id="divOpciones" type="text/x-jquery-tmpl">
        <div id="divOpciones_${key}" style="display:none; margin-top:5px;">
		    <div id="divCabeceraAcciones_${key}" dependantPanel="divPermisos_${key}" class="CollapsiblePanelHeader CollapsiblePanelHeaderExpand" style="margin:0em 1em; line-height:2em;">
			    <span id="lblCabecera_${key}" class="Rotulo Texto14" style="margin:5px; display:none;"></span>
		    </div>        
		    <div id="divPermisos_${key}" class="CollapsiblePanelContent" style="display:none; margin:0em 1em; font-size:0;"></div>
	    </div>
    </script>
	<fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
	<div id="divCabeceraDatosGenerales" dependantPanel="divDatosGenerales" class="CollapsiblePanelHeader CollapsiblePanelHeaderCollapse" style="margin:0.5em 1em 0em 1em; line-height:2em;">
		<asp:Label runat="server" ID="lblCabeceraDatosGenerales" CssClass="Texto14 Rotulo" style="margin:5px;"></asp:Label>
	</div>
	<div id="divDatosGenerales" class="CollapsiblePanelContent" style="margin:0em 1em;">
		<div style="margin-top:5px; line-height:20px; margin-left:5px;">
			<asp:Label runat="server" ID="lblCodigo" CssClass="Texto12 CustomCaption"></asp:Label>
			<input type="text" id="txtCodigoPerfil" class="Texto12 CajaTexto" style="margin-left:10px;" />			
			<asp:Label runat="server" ID="lblDenominacion" CssClass="Texto12 CustomCaption" style="margin-left:20px; margin-right:10px;"></asp:Label>
            <div style="display:inline-block; *display:inline; zoom:1; vertical-align:middle;">
			    <fsn:FSNTextBoxMultiIdioma ID="txtDenominacion" runat="server" TextBoxCssClass="CajaTexto" Width="275px"></fsn:FSNTextBoxMultiIdioma>
            </div>
		</div>		
	</div>
    <div id="divPanelesModulos" style="margin-bottom:1em;"></div>	
	<div id="popupUONsPerfil" class="popupCN" style="display:none; position:absolute; z-index:1004; padding:10px; height:60%; min-width:40%; max-width:70%;">		
		<div id="btnCerrarUONsPerfil" class="CerrarPopUp" style="float:right; width:30px; height:30px;"></div>
		<div id="divCabeceraUONsPerfil" style="clear:both; float:left; width:100%; margin-bottom:10px;">
			<img alt="" style="float:left;"/>
			<span class="Texto16 TextoAlternativo Negrita" style="float:left; margin-left:10px;"></span>
		</div>
		<div id="UONsPerfilTree" class="Bordear" style="clear:both; float:left; position:relative; width:100%; overflow:auto;"></div>
		<div id="divBotonesUONs" style="position:relative; clear:both; float:left; width:100%; margin-top:15px; text-align:center;">
			<div id="btnAceptarUONs" class="botonRedondeado" style="margin-right:5px;">
				<span id="lblAceptarUONs" style="line-height:23px;"></span>
			</div>
			<div id="btnCancelarUONs" class="botonRedondeado" style="margin-left:5px;">
				<span id="lblCancelarUONs" style="line-height:23px;"></span>
			</div>
		</div>
	</div>
    <div id="popupQlikUONsPerfil" class="popupCN" style="display:none; position:absolute; z-index:1004; padding:10px; height:60%; min-width:40%; max-width:70%;">		
		<div id="btnCerrarQlikUONsPerfil" class="CerrarPopUp" style="float:right; width:30px; height:30px;"></div>
		<div id="divCabeceraQlikUONsPerfil" style="clear:both; float:left; width:100%; margin-bottom:10px;">
			<img alt="" style="float:left;"/>
			<span class="Texto16 TextoAlternativo Negrita" style="float:left; margin-left:10px;"></span>
		</div>
		<div id="QlikUONsPerfilTree" class="Bordear" style="clear:both; float:left; position:relative; width:100%; overflow:auto;"></div>
		<div id="divBotonesQlikUONs" style="position:relative; clear:both; float:left; width:100%; margin-top:15px; text-align:center;">
			<div id="btnAceptarQlikUONs" class="botonRedondeado" style="margin-right:5px;">
				<span id="lblAceptarQlikUONs" style="line-height:23px;"></span>
			</div>
			<div id="btnCancelarQlikUONs" class="botonRedondeado" style="margin-left:5px;">
				<span id="lblCancelarQlikUONs" style="line-height:23px;"></span>
			</div>
		</div>
	</div>
    <div id="popupQlikGMNsPerfil" class="popupCN" style="display:none; position:absolute; z-index:1004; padding:10px; height:60%; min-width:40%; max-width:70%;">		
		<div id="btnCerrarQlikGMNsPerfil" class="CerrarPopUp" style="float:right; width:30px; height:30px;"></div>
		<div id="divCabeceraQlikGMNsPerfil" style="clear:both; float:left; width:100%; margin-bottom:10px;">
			<img alt="" style="float:left;"/>
			<span class="Texto16 TextoAlternativo Negrita" style="float:left; margin-left:10px;"></span>
		</div>
		<div id="QlikGMNsPerfilTree" class="Bordear" style="clear:both; float:left; position:relative; width:100%; overflow:auto;"></div>
		<div id="divBotonesQlikGMNs" style="position:relative; clear:both; float:left; width:100%; margin-top:15px; text-align:center;">
			<div id="btnAceptarQlikGMNs" class="botonRedondeado" style="margin-right:5px;">
				<span id="lblAceptarQlikGMNs" style="line-height:23px;"></span>
			</div>
			<div id="btnCancelarQlikGMNs" class="botonRedondeado" style="margin-left:5px;">
				<span id="lblCancelarQlikGMNs" style="line-height:23px;"></span>
			</div>
		</div>
	</div>
    <div id="popupUNQAPerfil" class="popupCN" style="display:none; position:absolute; z-index:1004; padding:10px; height:60%; min-width:40%; max-width:70%;">		
		<div id="btnCerrarUNQAPerfil" class="CerrarPopUp" style="float:right; width:30px; height:30px;"></div>
		<div id="divCabeceraUNQAPerfil" style="clear:both; float:left; width:100%; margin-bottom:10px;">
			<img alt="" style="float:left;"/>
			<span class="Texto16 TextoAlternativo Negrita" style="float:left; margin-left:10px;"></span>
		</div>
		<div id="UNQAPerfilTree" class="Bordear" style="clear:both; float:left; position:relative; width:100%; overflow:auto;"></div>
		<div id="divBotonesUNQA" style="position:relative; clear:both; float:left; width:100%; margin-top:15px; text-align:center;">
			<div id="btnAceptarUNQA" class="botonRedondeado" style="margin-right:5px;">
				<span id="lblAceptarUNQA" style="line-height:23px;"></span>
			</div>
			<div id="btnCancelarUNQA" class="botonRedondeado" style="margin-left:5px;">
				<span id="lblCancelarUNQA" style="line-height:23px;"></span>
			</div>
		</div>
	</div>

</asp:Content>
