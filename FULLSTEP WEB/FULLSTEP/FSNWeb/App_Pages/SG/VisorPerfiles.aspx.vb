﻿Imports Infragistics.Web.UI.GridControls
Imports Infragistics.Documents.Reports.Report

Public Class VisorPerfiles
    Inherits FSNPage
    Private desdeGS As Boolean = False
#Region "Cache"
    Private _dsPerfilesSeguridad As DataSet
    Protected ReadOnly Property oPerfilesSeguridad() As DataSet
        Get
            If _dsPerfilesSeguridad Is Nothing Then
                If IsPostBack Then
                    _dsPerfilesSeguridad = CType(Cache("dsPerfilesSeguridad" & FSNUser.Cod), DataSet)
                    If _dsPerfilesSeguridad Is Nothing Then
                        Dim oPerfiles As FSNServer.Perfiles
                        oPerfiles = FSNServer.Get_Object(GetType(FSNServer.Perfiles))
                        _dsPerfilesSeguridad = oPerfiles.Load(FSNUser.Idioma)

                        Me.InsertarEnCache("dsPerfilesSeguridad" & FSNUser.Cod, _dsPerfilesSeguridad)
                    End If
                Else
                    Dim oPerfiles As FSNServer.Perfiles
                    oPerfiles = FSNServer.Get_Object(GetType(FSNServer.Perfiles))
                    _dsPerfilesSeguridad = oPerfiles.Load(FSNUser.Idioma)

                    Me.InsertarEnCache("dsPerfilesSeguridad" & FSNUser.Cod, _dsPerfilesSeguridad)
                End If
            End If
            Return _dsPerfilesSeguridad
        End Get
    End Property
#End Region
#Region "Inicio"
    Private Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString("desdeGS") IsNot Nothing Then
            desdeGS = True
            Me.MasterPageFile = "~/App_Master/EnBlanco.Master"
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not desdeGS Then
                Dim mMaster = CType(Me.Master, FSNWeb.Menu)
                mMaster.Seleccionar("Seguridad", "PerfilesSeguridad")
            End If
            whgPerfiles.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")
            whgPerfiles.GridView.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize")
            If Not Request.QueryString("pag") Is Nothing Then
                whgPerfiles.GridView.Behaviors.Paging.PageIndex = CType(Request.QueryString("pag"), Integer)
            End If
            Cargar_PerfilesSeguridad()
        Else
            Cargar_PerfilesSeguridad()
            Select Case Request("__EVENTTARGET")
                Case btnRecargaPerfiles.ClientID
                    updPerfiles.Update()
                Case "Excel"
                    ExportarExcel()
                Case "PDF"
                    ExportarPDF()
            End Select
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "desdeGS") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "desdeGS", "<script>var desdeGS=" & IIf(Request.QueryString("desdeGS") IsNot Nothing, "true", "false") & ";</script>")
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutaTheme") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaTheme", "<script>var rutaTheme='" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "';</script>")
        End If

        FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/perfiles.png"

        CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgAgregarPerfil"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/nuevo.png"
        CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgCopiarPerfil"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/copiar.png"
        CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgCompararPerfil"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/comparar.png"
        CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgEliminarPerfil"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/eliminar.png"
        CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgExcel"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/Excel.png"
        CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgPDF"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/PDF.png"

        CargarTextos()
    End Sub
    Private Sub CargarTextos()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorPerfiles

        FSNPageHeader.TituloCabecera = Textos(0)
        whgPerfiles.GroupingSettings.EmptyGroupAreaText = Textos(19) 'Desplace una columna aquí para agrupar por ese concepto

        With CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label)
            .Text = Textos(5)
        End With
        With CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label)
            .Text = Textos(6)
        End With

        CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lnkAgregarPerfil"), Label).Text = Textos(7)
        CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lnkCopiarPerfil"), Label).Text = Textos(8)
        CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lnkCompararPerfil"), Label).Text = Textos(9)
        CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lnkEliminarPerfil"), Label).Text = Textos(10)
        CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblExcel"), Label).Text = Textos(11)
        CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPDF"), Label).Text = Textos(12)

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosAlerta") Then
            Dim sVariableJavascriptTextosAlerta As String = "var TextosAlerta = new Array();"
            For i As Integer = 0 To 3
                sVariableJavascriptTextosAlerta &= "TextosAlerta[" & i & "]='" & JSText(Textos(i + 13)) & "';"
            Next
            sVariableJavascriptTextosAlerta &= "TextosAlerta[4]='" & JSText(Textos(20)) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosAlerta", sVariableJavascriptTextosAlerta, True)
        End If
    End Sub
#End Region
#Region "whgPerfiles"
    Private Sub Cargar_PerfilesSeguridad()
        With whgPerfiles
            .Rows.Clear()
            .DataSource = oPerfilesSeguridad
            .GridView.DataSource = .DataSource
            If Not IsPostBack Then
                CrearColumnas_Acceso()
                ConfigurarGrid_PerfilesSeguridad()
            End If
            .DataMember = "PERFILESSEGURIDAD"
            .DataKeyFields = "ID"
            .DataBind()
        End With
    End Sub
    ''' <summary>
    ''' Creamos las columnas de la grid. Lo hacemos dinamico por no saber cuantos modulos activos hay
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CrearColumnas_Acceso()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorPerfiles
        Dim camposGrid As New List(Of BoundDataField)
        Dim campoGrid As BoundDataField
        Dim camposCheckGrid As New List(Of BoundCheckBoxField)
        Dim campoCheckGrid As BoundCheckBoxField

        campoGrid = New BoundDataField(True)
        With campoGrid
            .Key = "ID"
            .DataFieldName = "ID"
            .Hidden = True
        End With
        camposGrid.Add(campoGrid)

        campoGrid = New BoundDataField(True)
        With campoGrid
            .Key = "COD"
            .DataFieldName = "COD"
            .Header.Text = Textos(2)
            .Width = Unit.Pixel(100)
            .Hidden = False
            .CssClass = "itemSeleccionable"
            .Header.CssClass = "itemSeleccionable"
        End With
        camposGrid.Add(campoGrid)

        campoGrid = New BoundDataField(True)
        With campoGrid
            .Key = "DEN"
            .DataFieldName = "DEN"
            .Header.Text = Textos(3)
            .Hidden = False
            .CssClass = "itemSeleccionable"
            .Header.CssClass = "itemSeleccionable"
        End With
        camposGrid.Add(campoGrid)

        If Acceso.gbAccesoContratos Then
            campoCheckGrid = New BoundCheckBoxField(True)
            With campoCheckGrid
                .Key = TipoAccesoModulos.FSCM
                .DataFieldName = "FSCM"
                .Header.Text = Textos(4) & " FSCM"
                .Width = Unit.Pixel(100)
                .Hidden = False
                .CssClass = "itemCenterAligment itemSeleccionable"
                .Header.CssClass = "itemSeleccionable"
            End With
            camposCheckGrid.Add(campoCheckGrid)
        End If
        If Acceso.gbAccesoFSEP Then
            campoCheckGrid = New BoundCheckBoxField(True)
            With campoCheckGrid
                .Key = TipoAccesoModulos.FSEP
                .DataFieldName = "FSEP"
                .Header.Text = Textos(4) & " FSEP"
                .Width = Unit.Pixel(100)
                .Hidden = False
                .CssClass = "itemCenterAligment itemSeleccionable"
                .Header.CssClass = "itemSeleccionable"
            End With
            camposCheckGrid.Add(campoCheckGrid)
        End If
        If Acceso.gbAccesoFSGS Then
            campoCheckGrid = New BoundCheckBoxField(True)
            With campoCheckGrid
                .Key = TipoAccesoModulos.FSGS
                .DataFieldName = "FSGS"
                .Header.Text = Textos(4) & " FSGS"
                .Width = Unit.Pixel(100)
                .Hidden = False
                .CssClass = "itemCenterAligment itemSeleccionable"
                .Header.CssClass = "itemSeleccionable"
            End With
            camposCheckGrid.Add(campoCheckGrid)
        End If
        If Acceso.g_bAccesoFSFA Then
            campoCheckGrid = New BoundCheckBoxField(True)
            With campoCheckGrid
                .Key = TipoAccesoModulos.FSIM
                .DataFieldName = "FSIM"
                .Header.Text = Textos(4) & " FSIM"
                .Width = Unit.Pixel(100)
                .Hidden = False
                .CssClass = "itemCenterAligment itemSeleccionable"
                .Header.CssClass = "itemSeleccionable"
            End With
            camposCheckGrid.Add(campoCheckGrid)
        End If

        If Acceso.g_bAccesoIS Then
            campoCheckGrid = New BoundCheckBoxField(True)
            With campoCheckGrid
                .Key = TipoAccesoModulos.FSIS
                .DataFieldName = "FSIS"
                .Header.Text = Textos(4) & " FSIS"
                .Width = Unit.Pixel(100)
                .Hidden = False
                .CssClass = "itemCenterAligment itemSeleccionable"
                .Header.CssClass = "itemSeleccionable"
            End With
            camposCheckGrid.Add(campoCheckGrid)
        End If

        If Not Acceso.gsAccesoFSPM = FSNLibrary.TipoAccesoFSPM.SinAcceso Then
            campoCheckGrid = New BoundCheckBoxField(True)
            With campoCheckGrid
                .Key = TipoAccesoModulos.FSPM
                .DataFieldName = "FSPM"
                .Header.Text = Textos(4) & " FSPM"
                .Width = Unit.Pixel(100)
                .Hidden = False
                .CssClass = "itemCenterAligment itemSeleccionable"
                .Header.CssClass = "itemSeleccionable"
            End With
            camposCheckGrid.Add(campoCheckGrid)
        End If

        If Not Acceso.gsAccesoFSQA = FSNLibrary.TipoAccesoFSQA.SinAcceso Then
            campoCheckGrid = New BoundCheckBoxField(True)
            With campoCheckGrid
                .Key = TipoAccesoModulos.FSQA
                .DataFieldName = "FSQA"
                .Header.Text = Textos(4) & " FSQA"
                .Width = Unit.Pixel(100)
                .Hidden = False
                .CssClass = "itemCenterAligment itemSeleccionable"
                .Header.CssClass = "itemSeleccionable"
            End With
            camposCheckGrid.Add(campoCheckGrid)
        End If

        If Acceso.gbAccesoFSSM Then
            campoCheckGrid = New BoundCheckBoxField(True)
            With campoCheckGrid
                .Key = TipoAccesoModulos.FSSM
                .DataFieldName = "FSSM"
                .Header.Text = Textos(4) & " FSSM"
                .Width = Unit.Pixel(100)
                .Hidden = False
                .CssClass = "itemCenterAligment itemSeleccionable"
                .Header.CssClass = "itemSeleccionable"
            End With
            camposCheckGrid.Add(campoCheckGrid)
        End If

        If Acceso.gbAccesoFSBI Then
            campoCheckGrid = New BoundCheckBoxField(True)
            With campoCheckGrid
                .Key = TipoAccesoModulos.FSBI
                .DataFieldName = "FSBI"
                .Header.Text = Textos(4) & " FSBI"
                .Width = Unit.Pixel(100)
                .Hidden = False
                .CssClass = "itemCenterAligment itemSeleccionable"
                .Header.CssClass = "itemSeleccionable"
            End With
            camposCheckGrid.Add(campoCheckGrid)
        End If

        If Acceso.gbAccesoFSAL Then
            campoCheckGrid = New BoundCheckBoxField(True)
            With campoCheckGrid
                .Key = TipoAccesoModulos.FSAL
                .DataFieldName = "FSAL"
                .Header.Text = Textos(4) & " FSAL"
                .Width = Unit.Pixel(100)
                .Hidden = False
                .CssClass = "itemCenterAligment itemSeleccionable"
                .Header.CssClass = "itemSeleccionable"
            End With
            camposCheckGrid.Add(campoCheckGrid)
        End If

        For Each field As BoundDataField In camposGrid
            whgPerfiles.Columns.Add(field)
            whgPerfiles.GridView.Columns.Add(field)

            Dim fieldFilter As Infragistics.Web.UI.GridControls.ColumnFilteringSetting
            fieldFilter = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
            fieldFilter.ColumnKey = field.Key
            fieldFilter.Enabled = True
            whgPerfiles.Behaviors.Filtering.ColumnSettings.Add(fieldFilter)
            whgPerfiles.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldFilter)

            Dim fieldResize As Infragistics.Web.UI.GridControls.ColumnResizeSetting
            fieldResize = New Infragistics.Web.UI.GridControls.ColumnResizeSetting
            fieldResize.ColumnKey = field.Key
            fieldResize.EnableResize = True
            whgPerfiles.Behaviors.ColumnResizing.ColumnSettings.Add(fieldResize)
            whgPerfiles.GridView.Behaviors.ColumnResizing.ColumnSettings.Add(fieldResize)
        Next

        For Each field As BoundCheckBoxField In camposCheckGrid
            field.CheckBox.CheckedImageUrl = ConfigurationManager.AppSettings("ruta") & "images\baja.gif"
            whgPerfiles.Columns.Add(field)
            whgPerfiles.GridView.Columns.Add(field)

            Dim fieldFilter As Infragistics.Web.UI.GridControls.ColumnFilteringSetting
            fieldFilter = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
            fieldFilter.ColumnKey = field.Key
            fieldFilter.Enabled = True
            whgPerfiles.Behaviors.Filtering.ColumnSettings.Add(fieldFilter)
            whgPerfiles.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldFilter)

            Dim fieldResize As Infragistics.Web.UI.GridControls.ColumnResizeSetting
            fieldResize = New Infragistics.Web.UI.GridControls.ColumnResizeSetting
            fieldResize.ColumnKey = field.Key
            fieldResize.EnableResize = True
            whgPerfiles.Behaviors.ColumnResizing.ColumnSettings.Add(fieldResize)
            whgPerfiles.GridView.Behaviors.ColumnResizing.ColumnSettings.Add(fieldResize)
        Next
    End Sub
    ''' <summary>
    ''' Recuperamos las opciones que ha dejado el usuario anteriormente en el visor si es que vuelve a la pantalla tras ir
    ''' al detalle o a la comparativa
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ConfigurarGrid_PerfilesSeguridad()
        If Session("PerfilesGroupedColumns") IsNot Nothing Then
            For Each column As GroupedColumn In CType(Session("PerfilesGroupedColumns"), GroupedColumns)
                whgPerfiles.GroupingSettings.GroupedColumns.Add(column)
            Next
        End If
        If Session("PerfilesSortedColumns") Is Nothing Then
            whgPerfiles.GridView.Behaviors.Sorting.SortedColumns.Add("DEN", Infragistics.Web.UI.SortDirection.Ascending)
        Else
            For Each column As SortedColumnInfo In CType(Session("PerfilesSortedColumns"), SortedColumnInfoCollection)
                whgPerfiles.GridView.Behaviors.Sorting.SortedColumns.Add(column.ColumnKey, column.SortDirection)
            Next
        End If
        If Session("PerfilesColumnFilters") IsNot Nothing Then
            For Each column As ColumnFilter In CType(Session("PerfilesColumnFilters"), ColumnFilters)
                whgPerfiles.GridView.Behaviors.Filtering.ColumnFilters.Add(column)
            Next
        End If
    End Sub
    Private Sub whgPerfilesSeguridad_DataBound(sender As Object, e As System.EventArgs) Handles whgPerfiles.DataBound
        Paginador()
    End Sub
    Private Sub whgPerfiles_DataFiltered(sender As Object, e As Infragistics.Web.UI.GridControls.FilteredEventArgs) Handles whgPerfiles.DataFiltered
        Session("PerfilesColumnFilters") = e.ColumnFilters
        Paginador()
    End Sub
    Private Sub whgPerfiles_ColumnSorted(sender As Object, e As Infragistics.Web.UI.GridControls.SortingEventArgs) Handles whgPerfiles.ColumnSorted
        Session("PerfilesSortedColumns") = e.SortedColumns
        Paginador()
    End Sub
    Private Sub whgPerfiles_GroupedColumnsChanged(sender As Object, e As Infragistics.Web.UI.GridControls.GroupedColumnsChangedEventArgs) Handles whgPerfiles.GroupedColumnsChanged
        Session("PerfilesGroupedColumns") = e.GroupedColumns
        Paginador()
    End Sub
    Private Sub whgPerfiles_PageIndexChanged(sender As Object, e As Infragistics.Web.UI.GridControls.PagingEventArgs) Handles whgPerfiles.PageIndexChanged
        Dim desactivado As Boolean = ((whgPerfiles.GridView.Behaviors.Paging.PageCount = 1) OrElse (whgPerfiles.GridView.Behaviors.Paging.PageIndex = 0))
        With CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero" & IIf(desactivado, "_desactivado.gif", ".gif")
            .Enabled = Not desactivado
            .CssClass = IIf(desactivado, "", "Link")
        End With
        With CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior" & IIf(desactivado, "_desactivado.gif", ".gif")
            .Enabled = Not desactivado
            .CssClass = IIf(desactivado, "", "Link")
        End With
        desactivado = ((whgPerfiles.GridView.Behaviors.Paging.PageCount = 1) OrElse (whgPerfiles.GridView.Behaviors.Paging.PageIndex = whgPerfiles.GridView.Behaviors.Paging.PageCount - 1))
        With CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(desactivado, "_desactivado.gif", ".gif")
            .Enabled = Not desactivado
            .CssClass = IIf(desactivado, "", "Link")
        End With
        With CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(desactivado, "_desactivado.gif", ".gif")
            .Enabled = Not desactivado
            .CssClass = IIf(desactivado, "", "Link")
        End With
    End Sub
#End Region
#Region "Funciones"
    Private Sub Paginador()
        Dim pagerList As DropDownList = DirectCast(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
        pagerList.Items.Clear()
        For i As Integer = 1 To whgPerfiles.GridView.Behaviors.Paging.PageCount
            pagerList.Items.Add(i.ToString())
        Next
        pagerList.SelectedIndex = whgPerfiles.GridView.Behaviors.Paging.PageIndex
        CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = whgPerfiles.GridView.Behaviors.Paging.PageCount

        Dim desactivado As Boolean = ((whgPerfiles.GridView.Behaviors.Paging.PageCount = 1) OrElse (whgPerfiles.GridView.Behaviors.Paging.PageIndex = 0))
        With CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero" & IIf(desactivado, "_desactivado", "") & ".gif"
            .Enabled = False
        End With
        With CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior" & IIf(desactivado, "_desactivado", "") & ".gif"
            .Enabled = False
        End With
        desactivado = (whgPerfiles.GridView.Behaviors.Paging.PageCount = 1)
        With CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(desactivado, "_desactivado", "") & ".gif"
            .Enabled = Not desactivado
        End With
        With CType(whgPerfiles.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), System.Web.UI.WebControls.Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(desactivado, "_desactivado", "") & ".gif"
            .Enabled = Not desactivado
        End With
    End Sub
#End Region
#Region "Exportación"
    ''' <summary>
    ''' Exporta a Excel
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
    Private Sub ExportarExcel()
        Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
        fileName = fileName.Replace("+", "%20")

        With wdgExcelExporter
            .DownloadName = fileName
            .DataExportMode = DataExportMode.AllDataInDataSource

            .Export(whgPerfiles)
        End With
    End Sub
    Private Sub wdgExcelExporter_GridRecordItemExported(sender As Object, e As Infragistics.Web.UI.GridControls.GridRecordItemExportedEventArgs) Handles wdgExcelExporter.GridRecordItemExported
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorPerfiles
        If e.GridCell.Column.Type = System.Type.GetType("System.Boolean") Then
            e.WorksheetCell.Value = IIf(e.GridCell.Value, Textos(17), Textos(18))
        End If
    End Sub
    ''' <summary>
    ''' Exporta a PDF
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
    Private Sub ExportarPDF()
        Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
        fileName = fileName.Replace("+", "%20")
        wdgPDFExporter.DownloadName = fileName

        whgPerfiles.Behaviors.RowSelectors.Enabled = False

        With wdgPDFExporter
            .EnableStylesExport = True
            .DataExportMode = DataExportMode.AllDataInDataSource
            .TargetPaperOrientation = PageOrientation.Landscape
            .Margins = PageMargins.GetPageMargins("Narrow")
            .TargetPaperSize = PageSizes.GetPageSize("A4")

            .Export(whgPerfiles)
        End With
        whgPerfiles.Behaviors.RowSelectors.Enabled = True
    End Sub
    Private Sub wdgPDFExporter_GridRecordItemExporting(sender As Object, e As Infragistics.Web.UI.GridControls.DocumentGridRecordItemExportingEventArgs) Handles wdgPDFExporter.GridRecordItemExporting
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorPerfiles
        Select e.GridCell.Column.Key
            Case "COD", "DEN"
            Case Else
                e.ExportValue = IIf(e.GridCell.Value, Textos(17), Textos(18))
        End Select
    End Sub
#End Region
#Region "PageMethods"
    ''' <summary>
    ''' Actualiza el acceso a un modulo seleccionado de un perfil
    ''' </summary>
    ''' <param name="idPerfil"></param>
    ''' <param name="tipoAcceso"></param>
    ''' <param name="checked"></param>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub UpdatePerfil(ByVal idPerfil As Integer, ByVal tipoAcceso As Integer, ByVal checked As Boolean)
        Try
            Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim dsPerfilesSeguridad As DataSet
            dsPerfilesSeguridad = CType(HttpContext.Current.Cache("dsPerfilesSeguridad" & User.Cod), DataSet)

            Dim columnKey As String = String.Empty
            Select Case tipoAcceso
                Case TipoAccesoModulos.FSSG
                    columnKey = "FSSG"
                Case TipoAccesoModulos.FSGS
                    columnKey = "FSGS"
                Case TipoAccesoModulos.FSPM
                    columnKey = "FSPM"
                Case TipoAccesoModulos.FSQA
                    columnKey = "FSQA"
                Case TipoAccesoModulos.FSEP
                    columnKey = "FSEP"
                Case TipoAccesoModulos.FSSM
                    columnKey = "FSSM"
                Case TipoAccesoModulos.FSCM
                    columnKey = "FSCM"
                Case TipoAccesoModulos.FSIM
                    columnKey = "FSIM"
                Case TipoAccesoModulos.FSIS
                    columnKey = "FSIS"
                Case TipoAccesoModulos.FSBI
                    columnKey = "FSBI"
                Case TipoAccesoModulos.FSAL
                    columnKey = "FSAL"
            End Select
            dsPerfilesSeguridad.Tables(0).Select("ID=" & idPerfil)(0)(columnKey) = checked

            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oPerfil As FSNServer.Perfiles = FSNServer.Get_Object(GetType(FSNServer.Perfiles))

            oPerfil.UpdatePerfil_Acceso(idPerfil, columnKey, checked)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Mira si el perfil esta asignado a algun usuario. Si es asi no se podra borrar
    ''' </summary>
    ''' <param name="idPerfil"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function ComprobarPosibleEliminarPerfil(ByVal idPerfil As Integer)
        Try
            Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oPerfil As FSNServer.Perfiles = FSNServer.Get_Object(GetType(FSNServer.Perfiles))

            Return oPerfil.ComprobarPosibleEliminarPerfil(idPerfil)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Elimina un perfil. En base de datos y lo quita de la cache para cuando se recargue el grid tras la eliminación
    ''' </summary>
    ''' <param name="idPerfil"></param>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub DeletePerfil(ByVal idPerfil As Integer)
        Try
            Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oPerfil As FSNServer.Perfiles = FSNServer.Get_Object(GetType(FSNServer.Perfiles))

            oPerfil.DeletePerfil(idPerfil)

            Dim dsPerfilesSeguridad As DataSet
            dsPerfilesSeguridad = CType(HttpContext.Current.Cache("dsPerfilesSeguridad" & User.Cod), DataSet)

            dsPerfilesSeguridad.Tables(0).Rows.Remove(dsPerfilesSeguridad.Tables(0).Select("ID=" & idPerfil)(0))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
End Class
