﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="VisorPerfiles.aspx.vb" Inherits="Fullstep.FSNWeb.VisorPerfiles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
	<script type="text/javascript">
	    $(document).ready(function () {
	        if (desdeGS) $('body').addClass('fondoMasterEnBlanco');
	        $('[id$=ImgBtnFirst]').live('click', function () { FirstPage(); });
	        $('[id$=ImgBtnPrev]').live('click', function () { PrevPage(); });
	        $('[id$=ImgBtnNext]').live('click', function () { NextPage(); });
	        $('[id$=ImgBtnLast]').live('click', function () { LastPage(); });
	    });
        function ExportarExcel() {
            __doPostBack('Excel', '');
        }
        function ExportarPDF() {
            __doPostBack('PDF', '');
        }
		function IndexChanged() {
			var dropdownlist = $('[id$=PagerPageList]')[0];
			var grid = $find($('[id$=whgPerfiles]').attr('id')).get_gridView();
			var newValue = dropdownlist.selectedIndex;
			grid.get_behaviors().get_paging().set_pageIndex(newValue);
		}
		function FirstPage() {
			var grid = $find($('[id$=whgPerfiles]').attr('id')).get_gridView();
			grid.get_behaviors().get_paging().set_pageIndex(0);
		}
		function PrevPage() {
			var grid = $find($('[id$=whgPerfiles]').attr('id')).get_gridView();
			var dropdownlist = $('[id$=PagerPageList]')[0];
			if (grid.get_behaviors().get_paging().get_pageIndex() > 0) {
				grid.get_behaviors().get_paging().set_pageIndex(grid.get_behaviors().get_paging().get_pageIndex() - 1);
			}
		}
		function NextPage() {
			var grid = $find($('[id$=whgPerfiles]').attr('id')).get_gridView();
			var dropdownlist = $('[id$=PagerPageList]')[0];
			if (grid.get_behaviors().get_paging().get_pageIndex() < grid.get_behaviors().get_paging().get_pageCount() - 1) {
				grid.get_behaviors().get_paging().set_pageIndex(grid.get_behaviors().get_paging().get_pageIndex() + 1);
			}
		}
		function LastPage() {
			var grid = $find($('[id$=whgPerfiles]').attr('id')).get_gridView();
			grid.get_behaviors().get_paging().set_pageIndex(grid.get_behaviors().get_paging().get_pageCount() - 1);
		}
		function whgPerfiles_PageIndexChanged() {
			var grid = $find($('[id$=whgPerfiles]').attr('id')).get_gridView();
			var dropdownlist = $('[id$=PagerPageList]')[0];

			dropdownlist.options[grid.get_behaviors().get_paging().get_pageIndex()].selected = true;
		}
		function whgPerfiles_CellClick(sender, e) {
            /*Cuando hace click en una celda, si es el código o la denominación redirige al detalle del perfil*/
		    if (e.get_type() == "cell" && e.get_item().get_row().get_index() !== -1) {
			    if (e.get_item().get_grid().get_behaviors().get_activation().get_activeCellResolved() !== null) {
					var cell = e.get_item().get_grid().get_behaviors().get_activation().get_activeCellResolved();
					switch (cell.get_column().get_key()) {
						case "COD":
						case "DEN":
							var idPerfil = cell.get_row().get_cellByColumnKey("ID").get_value();
							window.location.href = rutaFS + 'SG/DetallePerfil.aspx?idPerfil=' + idPerfil + '&pag=' + e.get_item().get_grid().get_behaviors().get_paging().get_pageIndex() + (desdeGS ? '&desdeGS=1' : '');
							break;
						default:
							break;
					}
				}                    
			}                                
		}
		function whgPerfiles_CellUpdating(sender, e) {
            /*Actualiza el acceso a un modulo del perfil*/
			var idPerfil = e.get_cell().get_row().get_cellByColumnKey("ID").get_value();
			var tipoAcceso = e.get_cell().get_column().get_key();
			var checked = e.get_newValue();
			if (idPerfil == 0) return false;
			$.ajax({
				type: 'POST',
				url: rutaFS + 'SG/VisorPerfiles.aspx/UpdatePerfil',
				data: JSON.stringify({ idPerfil: idPerfil, tipoAcceso: tipoAcceso, checked: checked }),
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				async: true
			});
		}
		function AgregarPerfil() {
            /*Redirige a la pantalla del detalle de perfil en modo de agregar*/
			var grid = $find($('[id$=whgPerfiles]').attr('id'));
			window.location.href = rutaFS + 'SG/DetallePerfil.aspx?idPerfil=0&pag=' + grid.get_gridView().get_behaviors().get_paging().get_pageIndex() + (desdeGS ? '&desdeGS=1' : '');
		}
		function CopiarPerfil() {
		    /*Comprueba si hay un perfil seleccionado y si es asi lo reeenvia a la pantalla del detalle perfil con los datos
		    del perfil seleccionado pero sin codigo ni denominacion para poder crear uno nuevo*/
			var grid = $find($('[id$=whgPerfiles]').attr('id'));
			var selectedRows = grid.get_gridView().get_behaviors().get_selection().get_selectedRows();
			if (selectedRows.get_length() != 1) {
				alert(TextosAlerta[0]);
				return false;
			}
			var idPerfil = selectedRows.getItem(0).get_cellByColumnKey("ID").get_value();
			window.location.href = rutaFS + 'SG/DetallePerfil.aspx?action=copy&idPerfil=' + idPerfil + '&pag=' + grid.get_gridView().get_behaviors().get_paging().get_pageIndex() + (desdeGS ? '&desdeGS=1' : '');
		}
		function CompararPerfil() {
		    /*Comprueba si hay un perfil seleccionado y si es asi lo reeenvia a la pantalla de comparar perfil con los datos
		    del perfil seleccionado*/
		    var grid = $find($('[id$=whgPerfiles]').attr('id'));
		    var selectedRows = grid.get_gridView().get_behaviors().get_selection().get_selectedRows();
		    if (selectedRows.get_length() == 0) {
		        alert(TextosAlerta[4]);
		        return false;
		    }
		    var idPerfil = '';
		    for(i=0;i<selectedRows.get_length();i++){
		        idPerfil=idPerfil + (idPerfil==''?'':',') + selectedRows.getItem(i).get_cellByColumnKey("ID").get_value();
            }
		    window.location.href = rutaFS + 'SG/ComparativaPerfiles.aspx?idPerfil=' + idPerfil + '&pag=' + grid.get_gridView().get_behaviors().get_paging().get_pageIndex() + (desdeGS ? '&desdeGS=1' : '');
		}
		function EliminarPerfil() {
		    /*Comprueba si hay un perfil seleccionado y si es comprueba que no este asociado a ningun usuario para ver si 
		    se puede borrar*/
			var grid = $find($('[id$=whgPerfiles]').attr('id')).get_gridView();
			var selectedRows = grid.get_behaviors().get_selection().get_selectedRows();
			if (selectedRows.get_length() != 1) {
				alert(TextosAlerta[1]);
				return false;
			}
			var idPerfil = selectedRows.getItem(0).get_cellByColumnKey("ID").get_value();
			var codigoPerfil = selectedRows.getItem(0).get_cellByColumnKey("COD").get_value();
			var denominacionPerfil = selectedRows.getItem(0).get_cellByColumnKey("DEN").get_value();
			$.when($.ajax({
				type: 'POST',
				url: rutaFS + 'SG/VisorPerfiles.aspx/ComprobarPosibleEliminarPerfil',
				data: JSON.stringify({ idPerfil: idPerfil }),
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				async: true
			})).done(function (msg) {
				var posibleEliminar = msg.d;
				if (posibleEliminar) {
					var message = TextosAlerta[3].replace('#####', denominacionPerfil);
					message = message.replace('###', codigoPerfil);
					if (confirm(message)) {
						$.when($.ajax({
							type: 'POST',
							url: rutaFS + 'SG/VisorPerfiles.aspx/DeletePerfil',
							data: JSON.stringify({ idPerfil: idPerfil }),
							contentType: 'application/json; charset=utf-8',
							dataType: 'json',
							async: true
						})).done(function () {
						    var btnRecargaPerfiles = $('[id$=btnRecargaPerfiles]').attr('id');
						    selectedRows.clear();
							__doPostBack(btnRecargaPerfiles, '');
						});
					}
				} else {
					alert(TextosAlerta[2]);
					return false;
				}
			});
		}
	</script>
	<ig:WebExcelExporter ID="wdgExcelExporter" runat="server"></ig:WebExcelExporter>
	<ig:WebDocumentExporter ID="wdgPDFExporter" runat="server" ExportMode="Download"></ig:WebDocumentExporter>
	<fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
	<asp:Button runat="server" ID="btnRecargaPerfiles" style="display:none;" />
	<asp:UpdatePanel runat="server" ID="updPerfiles" UpdateMode="Conditional" style="margin-top:0.5em;">
		<Triggers>
			<asp:AsyncPostBackTrigger ControlID="btnRecargaPerfiles" EventName="Click" />
		</Triggers>
		<ContentTemplate>
			<ig:WebHierarchicalDataGrid runat="server" ID="whgPerfiles" AutoGenerateBands="false"
				AutoGenerateColumns="false" Width="100%" EnableAjax="true"
				EnableAjaxViewState="false" EnableDataViewState="false">
				<GroupingSettings EnableColumnGrouping="True" GroupAreaVisibility="Visible"></GroupingSettings>
				<ClientEvents Click="whgPerfiles_CellClick" />            
				<Behaviors>                
					<ig:Activation Enabled="true"/>
					<ig:Filtering Alignment="Top" Enabled="true" Visibility="Visible" AnimationEnabled="false"></ig:Filtering>                 
					<ig:RowSelectors Enabled="true"></ig:RowSelectors>
					<ig:Selection Enabled="true" RowSelectType="Multiple" CellSelectType="None" ColumnSelectType="None"></ig:Selection>
					<ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting>
					<ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
					<ig:ColumnMoving Enabled="false"></ig:ColumnMoving>
					<ig:Paging Enabled="true" PagerAppearance="Top">
						<PagingClientEvents PageIndexChanged="whgPerfiles_PageIndexChanged" />
						<PagerTemplate>
							<div class="CabeceraBotones" style="clear:both; float:left; width:100%;"> 
								<div style="float:left; padding:3px 0px 2px 0px; margin-left:10px;">
									<div style="clear:both; float:left; margin-right: 5px;">
										<asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" style="margin-right:2px;" />
										<asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" />
									</div>
									<div style="float:left; margin-right:5px;">
										<asp:Label ID="lblPage" runat="server" Text="dPage" Style="margin-right: 3px;"></asp:Label>
										<asp:DropDownList ID="PagerPageList" runat="server" Style="margin-right: 3px;" onchange="return IndexChanged()" />
										<asp:Label ID="lblOF" runat="server" Text="dOf" Style="margin-right: 3px;"></asp:Label>
										<asp:Label ID="lblCount" runat="server" />
									</div>
									<div style="float:left;">
										<asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-right:2px;" />
										<asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" />
									</div>
								</div>	
								<div style="float:right; padding:3px 0px 2px 0px;">                            
									<div onclick="AgregarPerfil();" class="botonDerechaCabecera" style="float:left; line-height:22px;">
										<asp:Image runat="server" ID="imgAgregarPerfil" CssClass="imagenCabecera" />
										<asp:Label runat="server" ID="lnkAgregarPerfil" CssClass="fntLogin2" style="margin-left:3px;" Text="dAñadir perfil"></asp:Label>
									</div>
									<div onclick="CopiarPerfil();" class="botonDerechaCabecera" style="float:left; line-height:22px;">           
										<asp:Image runat="server" ID="imgCopiarPerfil" CssClass="imagenCabecera" />
										<asp:Label runat="server" ID="lnkCopiarPerfil" CssClass="fntLogin2" style="margin-left:3px;" Text="dCopiar perfil"></asp:Label>           
									</div>
									<div onclick="CompararPerfil()" class="botonDerechaCabecera" style="float:left; line-height:22px;">										       
										<asp:Image runat="server" ID="imgCompararPerfil" CssClass="imagenCabecera" />
										<asp:Label runat="server" ID="lnkCompararPerfil" CssClass="fntLogin2" style="margin-left:3px;" Text="dComparar perfiles"></asp:Label>           
									</div>
									<div onclick="EliminarPerfil()" class="botonDerechaCabecera" style="float:left; line-height:22px;">										       
										<asp:Image runat="server" ID="imgEliminarPerfil" CssClass="imagenCabecera" />
										<asp:Label runat="server" ID="lnkEliminarPerfil" CssClass="fntLogin2" style="margin-left:3px;" Text="dEliminar perfil"></asp:Label>           
									</div>
									<div onclick="ExportarExcel()" class="botonDerechaCabecera" style="float:left; line-height:22px;">            
										<asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
										<asp:Label runat="server" ID="lblExcel" CssClass="fntLogin2" style="margin-left:3px;" Text="dExcel"></asp:Label>           
									</div>
									<div onclick="ExportarPDF()" class="botonDerechaCabecera" style="float:left; line-height:22px;">            
										<asp:Image runat="server" ID="imgPDF" CssClass="imagenCabecera" />
										<asp:Label runat="server" ID="lblPDF" CssClass="fntLogin2" style="margin-left:3px;" Text="dPDF"></asp:Label>           
									</div>
								</div>
							</div>
						</PagerTemplate>
					</ig:Paging>                
					<ig:EditingCore AutoCRUD="false" BatchUpdating="true">
						<EditingClientEvents CellValueChanging="whgPerfiles_CellUpdating" />
					</ig:EditingCore>
				</Behaviors>           
			</ig:WebHierarchicalDataGrid>
		</ContentTemplate>
	</asp:UpdatePanel>		
</asp:Content>