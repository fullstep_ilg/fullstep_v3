﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VisorPerfiles

    '''<summary>
    '''wdgExcelExporter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wdgExcelExporter As Global.Infragistics.Web.UI.GridControls.WebExcelExporter

    '''<summary>
    '''wdgPDFExporter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wdgPDFExporter As Global.Infragistics.Web.UI.GridControls.WebDocumentExporter

    '''<summary>
    '''FSNPageHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FSNPageHeader As Global.Fullstep.FSNWebControls.FSNPageHeader

    '''<summary>
    '''btnRecargaPerfiles control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRecargaPerfiles As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''updPerfiles control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updPerfiles As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''whgPerfiles control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents whgPerfiles As Global.Infragistics.Web.UI.GridControls.WebHierarchicalDataGrid
End Class
