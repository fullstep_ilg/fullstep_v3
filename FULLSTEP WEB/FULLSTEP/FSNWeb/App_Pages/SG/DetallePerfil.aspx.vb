﻿Imports System.Web.Script.Serialization
Imports Fullstep.FSNLibrary.TiposDeDatos

Public Class DetallePerfil
    Inherits FSNPage
    Private desdeGS As Boolean = False
#Region "Inicio"
    Private Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString("desdeGS") IsNot Nothing Then
            desdeGS = True
            Me.MasterPageFile = "~/App_Master/EnBlanco.Master"
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Cargeremos todos los item de posible visualización en la pantalla. Cada item que pueda tener opciones
        'checkeables las tendremos en la propiedad options del item. Así, cada vez que pinchemos en un item miraremos
        'si tiene options y si es asi los cargaremos en la parte derecha de la pantalla.
        'Cada vez que checkeemos o descheckeemos una opción mantendremos una variable json eliminando o agregando la acción.
        Dim idPerfil As Integer
        If Request.QueryString("idPerfil") Is Nothing Then
            idPerfil = 0
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "tipoDetalleCopia", "<script>var tipoDetalleCopia = 'new'; </script>")
        Else
            idPerfil = CType(Request.QueryString("idPerfil"), Integer)
            If Request.QueryString("action") = "copy" Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "tipoDetalleCopia", "<script>var tipoDetalleCopia = 'copy'; </script>")
            Else
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "tipoDetalleCopia", "<script>var tipoDetalleCopia = 'detail'; </script>")
            End If
        End If
        Dim mostrarBotones As Boolean = True
        If idPerfil = 0 OrElse Request.QueryString("action") = "copy" OrElse Request.QueryString("usu") IsNot Nothing Then
            mostrarBotones = False
        End If
        With FSNPageHeader
            .UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/perfiles.png"
            .VisibleBotonEliminarPerfil = True
            If Not mostrarBotones Then .OcultarBotonEliminarPerfil = True
            .OnClientClickEliminarPerfil = "EliminarPerfil(); return false;"
            .VisibleBotonCompararPerfil = True
            If Not mostrarBotones Then .OcultarBotonCompararPerfil = True
            .OnClientClickCompararPerfil = "CompararPerfil(); return false;"
            .VisibleBotonCopiarPerfil = True
            If Not mostrarBotones Then .OcultarBotonCopiarPerfil = True
            .OnClientClickCopiarPerfil = "CopiarPerfil(); return false;"
            .VisibleBotonAgregarPerfil = True
            If Not mostrarBotones Then .OcultarBotonAgregarPerfil = True
            .OnClientClickAgregarPerfil = "AgregarPerfil(); return false;"
            .VisibleBotonGuardar = True
            .OnClientClickGuardar = "GuardarCambios(" & IIf(Request.QueryString("usu") IsNot Nothing, "true", "false") & ");return false;"
            .VisibleBotonVolver = (Request.QueryString("usu") Is Nothing)
            .OnClientClickVolver = "window.location.href='" & ConfigurationManager.AppSettings("rutaFS") & "SG/VisorPerfiles.aspx?pag=" & CType(Request.QueryString("pag"), Integer) & IIf(desdeGS, "&desdeGS=1", "") & "';return false;"
        End With

        If Not desdeGS Then
            Dim mMaster = CType(Me.Master, FSNWeb.Menu)
            mMaster.Seleccionar("Seguridad", "PerfilesSeguridad")
        End If
        CargarTextos()
        Cargar_PerfilSeguridad(idPerfil)

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "desdeGS") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "desdeGS", "<script>var desdeGS=" & IIf(Request.QueryString("desdeGS") IsNot Nothing, "true", "false") & ";</script>")
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "ModalClientID") Then
            Dim ModalProgressClientID As String
            If desdeGS Then
                ModalProgressClientID = CType(Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
            Else
                ModalProgressClientID = CType(Master.Master.FindControl("ModalProgress"), AjaxControlToolkit.ModalPopupExtender).ClientID
            End If
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalClientID", "var ModalProgress='" & ModalProgressClientID & "'", True)
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "accBIRestringirVisibilidadQlikApps") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accBIRestringirVisibilidadQlikApps",
                                                        vbCrLf & "<script>" &
                                                        vbCrLf & "   var acTipoAccesoModulosFSBI=" & TipoAccesoModulos.FSBI & ";" &
                                                        vbCrLf & "   var acTipoAccesoModulosFSPM=" & TipoAccesoModulos.FSPM & ";" &
                                                        vbCrLf & "   var acTipoAccesoModulosFSIM=" & TipoAccesoModulos.FSIM & ";" &
                                                        vbCrLf & "   var acTipoAccesoModulosFSQA=" & TipoAccesoModulos.FSQA & ";" &
                                                        vbCrLf & "   var acTipoAccesoModulosFSCM=" & TipoAccesoModulos.FSCM & ";" &
                                                        vbCrLf & "   var accBIRestringirVisibilidadQlikAppsUONs=" & AccionesDeSeguridad.BIRestringirVisibilidadQlikAppsUONs & ";" &
                                                        vbCrLf & "   var accBIRestringirVisibilidadQlikAppsGMNs=" & AccionesDeSeguridad.BIRestringirVisibilidadQlikAppsGMNs & ";" &
                                                        vbCrLf & "   var accQARestringirConsultaCertificadossUnidadesNegocio=" & AccionesDeSeguridad.QARestringirConsultaCertificadossUnidadesNegocio & ";" &
                                                        vbCrLf & "</script>")
        End If
    End Sub
    Private Sub CargarTextos()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.DetallePerfil
        FSNPageHeader.TituloCabecera = Textos(0)
        With FSNPageHeader
            If .VisibleBotonGuardar Then .TextoBotonGuardar = Textos(1)
            If .VisibleBotonAgregarPerfil Then .TextoBotonAgregarPerfil = Textos(2)
            .TextoBotonCopiarPerfil = Textos(3)
            .TextoBotonCompararPerfil = Textos(4)
            .TextoBotonEliminarPerfil = Textos(5)
            .TextoBotonVolver = Textos(6)
        End With

        lblCabeceraDatosGenerales.Text = Textos(7)
        lblCodigo.Text = Textos(8) & ":"
        lblDenominacion.Text = Textos(9) & ":"
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosAlerta") Then
            Dim sVariableJavascriptTextosAlerta As String = "var TextosAlerta = new Array();"
            For i As Integer = 0 To 2
                sVariableJavascriptTextosAlerta &= "TextosAlerta[" & i & "]='" & JSText(Textos(i + 20)) & "';"
            Next
            sVariableJavascriptTextosAlerta &= "TextosAlerta[3]='" & JSText(Textos(27)) & "';"
            sVariableJavascriptTextosAlerta &= "TextosAlerta[4]='" & JSText(Textos(28)) & "';"
            sVariableJavascriptTextosAlerta &= "TextosAlerta[5]='" & JSText(Textos(29)) & "';"
            sVariableJavascriptTextosAlerta &= "TextosAlerta[6]='" & JSText(Textos(30)) & "';"
            sVariableJavascriptTextosAlerta &= "var TextosPantalla = new Array();"
            sVariableJavascriptTextosAlerta &= "TextosPantalla[0]='" & JSText(Textos(23)) &
                "'; TextosPantalla[1]='" & JSText(Textos(24)) &
                "'; TextosPantalla[2]='" & JSText(Textos(25)) &
                "'; TextosPantalla[3]='" & JSText(Textos(32)) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosAlerta", sVariableJavascriptTextosAlerta, True)
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosModulos") Then
            Dim sVariableJavascriptTextosModulos As String = "var TextosModulos = new Array();"
            sVariableJavascriptTextosModulos &= "TextosModulos[0]='" & JSText(Textos(19)) &
                "';TextosModulos[" & TipoAccesoModulos.FSGS & "]='" & JSText(Textos(10)) &
                "'; TextosModulos[" & TipoAccesoModulos.FSPM & "]='" & JSText(Textos(11)) &
                "'; TextosModulos[" & TipoAccesoModulos.FSEP & "]='" & JSText(Textos(12)) &
                "'; TextosModulos[" & TipoAccesoModulos.FSCM & "]='" & JSText(Textos(13)) &
                "'; TextosModulos[" & TipoAccesoModulos.FSSM & "]='" & JSText(Textos(14)) &
                "'; TextosModulos[" & TipoAccesoModulos.FSIM & "]='" & JSText(Textos(15)) &
                "'; TextosModulos[" & TipoAccesoModulos.FSQA & "]='" & JSText(Textos(16)) &
                "'; TextosModulos[" & TipoAccesoModulos.FSIS & "]='" & JSText(Textos(17)) &
                "'; TextosModulos[" & TipoAccesoModulos.FSCN & "]='" & JSText(Textos(18)) &
                "'; TextosModulos[" & TipoAccesoModulos.FSBI & "]='" & JSText(Textos(26)) &
                "'; TextosModulos[" & TipoAccesoModulos.FSAL & "]='" & JSText(Textos(31)) &
                "'; TextosModulos[" & TipoAccesoModulos.FSSOL & "]='" & JSText(Textos(33)) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosModulos", sVariableJavascriptTextosModulos, True)
        End If
    End Sub
    ''' <summary>
    ''' Obtenemos los datos que mostraremos como opciones en la pantalla dependiendo de los modulos activos. 
    ''' Ademas, si se pasa un querystring con un id de perfil se cargaran los datos del perfil seleccionado.
    ''' En el caso de que vengamos en tipo copia perfil no cargaremos en pantalla el codigo y denominaciones del perfil
    ''' y pasaremos que estamos en tipo copia a json para que al guardar agregue un nuevo perfil
    ''' En caso de que vengamos en tipo detalle cargaremos todos los datos del perfil
    ''' En caso de que vengamos en tipo agregar no cargaremos datos de ningun perfil, solo mostraremos en pantalla las opciones
    ''' TODOS ESTOS DATOS LOS METEMOS EN CACHE PARA QUE AL CARGAR LA PANTALLA SE ENVIEN POR WEBMETHOD EN FORMATO JSON
    ''' </summary>
    ''' <param name="idPerfil"></param>
    ''' <remarks></remarks>
    Private Sub Cargar_PerfilSeguridad(ByVal idPerfil As Integer)
        Try
            Dim oPerfil As FSNServer.Perfiles = FSNServer.Get_Object(GetType(FSNServer.Perfiles))
            Dim Acceso As TiposDeDatos.ParametrosGenerales = FSNServer.TipoAcceso
            Dim AccesoI As TiposDeDatos.ParametrosIntegracion = FSNServer.TipoAccesoI
            Dim ModulosActivos As String = String.Empty
            'Vemos que modulos estan activos en la instalación y los pasamos para cargar los datos.
            If Acceso.gbAccesoFSGS Then _
                ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSGS, "," & TipoAccesoModulos.FSGS)
            If Not Acceso.gsAccesoFSPM = FSNLibrary.TipoAccesoFSPM.SinAcceso Then _
                ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSPM, "," & TipoAccesoModulos.FSPM)
            If Acceso.gbAccesoFSEP Then _
                ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSEP, "," & TipoAccesoModulos.FSEP)
            If Acceso.gbAccesoContratos Then _
                ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSCM, "," & TipoAccesoModulos.FSCM)
            If Acceso.gbAccesoFSSM Then _
                ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSSM, "," & TipoAccesoModulos.FSSM)
            If Acceso.g_bAccesoFSFA Then _
                ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSIM, "," & TipoAccesoModulos.FSIM)
            If Not Acceso.gsAccesoFSQA = FSNLibrary.TipoAccesoFSQA.SinAcceso Then _
                ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSQA, "," & TipoAccesoModulos.FSQA)
            If Acceso.g_bAccesoIS Then _
                ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSIS, "," & TipoAccesoModulos.FSIS)
            If Acceso.gbAccesoFSCN Then _
                ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSCN, "," & TipoAccesoModulos.FSCN)
            If Acceso.gbAccesoFSBI Then _
                ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSBI, "," & TipoAccesoModulos.FSBI)
            If Acceso.gbAccesoFSAL Then _
                ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSAL, "," & TipoAccesoModulos.FSAL)
            If (Not Acceso.gsAccesoFSPM = FSNLibrary.TipoAccesoFSPM.SinAcceso) Or Acceso.gbAccesoContratos Or (Not Acceso.gsAccesoFSQA = FSNLibrary.TipoAccesoFSQA.SinAcceso) Or Acceso.g_bAccesoFSFA Then _
                ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSSOL, "," & TipoAccesoModulos.FSSOL)

            Dim oInfoPerfil As FSNServer.Perfil = oPerfil.Obtener_Perfil(idPerfil, FSNUser.Idioma, ModulosActivos, Acceso, AccesoI)
            oInfoPerfil.IdiomaDefecto = FSNUser.IdiomaCod
            If Request.QueryString("action") = "copy" Then
                oInfoPerfil.Id = 0
                oInfoPerfil.Codigo = ""
                Dim denominaciones As New List(Of String)
                Dim denominacion As String
                For Each key As String In oInfoPerfil.Denominaciones.Keys
                    denominaciones.Add(key)
                Next
                For Each key As String In denominaciones
                    denominacion = oInfoPerfil.Denominaciones(key)
                    oInfoPerfil.Denominaciones.Remove(key)
                    oInfoPerfil.Denominaciones(key) = ""
                Next
            End If
            Me.InsertarEnCache("oPerfiles_Seguridad_" & FSNUser.Cod, oInfoPerfil)
            With txtDenominacion
                .itemList = oInfoPerfil.Denominaciones
                .IdiomaSeleccionado = FSNUser.IdiomaCod
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
#Region "Page Methods"
    ''' <summary>
    ''' Mandamos los datos guardados en cache en formato jSon, y seleccionaremos y cargaremos los datos en la parte de cliente
    ''' Lo serializamos en .net y lo pasamos en formato string para luego parsearlo en cliente ya que algunos tipos de objeto no
    ''' se serializan bien, por ejemplo los dictionary
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function Obtener_Datos_Perfil() As Object
        Try
            Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim serializer As New JavaScriptSerializer()

            Dim oInfoPerfil As FSNServer.Perfil = TryCast(HttpContext.Current.Cache("oPerfiles_Seguridad_" & User.Cod), FSNServer.Perfil)
            Return serializer.Serialize(DirectCast(oInfoPerfil, Object)).ToString
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function Guardar(ByVal Perfil As Object) As Integer
        Try
            Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim serializer As New JavaScriptSerializer()
            Dim oInfoPerfil As FSNServer.Perfil
            oInfoPerfil = serializer.Deserialize(Of FSNServer.Perfil)(serializer.Serialize(DirectCast(Perfil, Object)).ToString)

            Dim oPerfil As FSNServer.Perfiles = FSNServer.Get_Object(GetType(FSNServer.Perfiles))
            If oInfoPerfil.Id = 0 Then
                oInfoPerfil.Id = oPerfil.AddPerfil(oInfoPerfil)
            Else
                oPerfil.UpdatePerfil(oInfoPerfil)
            End If
            Return oInfoPerfil.Id
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Comprobamos si el perfil no esta asociado a ningun usuario para poder borrarse
    ''' </summary>
    ''' <param name="idPerfil"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function ComprobarPosibleEliminarPerfil(ByVal idPerfil As Integer)
        Try
            Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oPerfil As FSNServer.Perfiles = FSNServer.Get_Object(GetType(FSNServer.Perfiles))

            Return oPerfil.ComprobarPosibleEliminarPerfil(idPerfil)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Borramos el perfil
    ''' </summary>
    ''' <param name="idPerfil"></param>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Sub DeletePerfil(ByVal idPerfil As Integer)
        Try
            Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oPerfil As FSNServer.Perfiles = FSNServer.Get_Object(GetType(FSNServer.Perfiles))

            oPerfil.DeletePerfil(idPerfil)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Comprobamos si el código de perfil a insertar ya existe en bd.
    ''' </summary>
    ''' <param name="CodPerfil"></param>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function Comprobar_CodigoPerfilValido(ByVal IdPerfil As Integer, ByVal CodPerfil As String) As Boolean
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oPerfil As FSNServer.Perfiles = FSNServer.Get_Object(GetType(FSNServer.Perfiles))
            Return oPerfil.Comprobar_CodigoPerfilValido(IdPerfil, CodPerfil)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
End Class
