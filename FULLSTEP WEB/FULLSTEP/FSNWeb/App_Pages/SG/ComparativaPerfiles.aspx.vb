﻿Imports Infragistics.Web.UI.GridControls
Imports Infragistics.Documents.Reports.Report

Public Class ComparativaPerfiles
    Inherits FSNPage
    Private popup As Boolean = False
    Private desdeGS As Boolean = False
#Region "Cache"
    Protected ReadOnly Property oPerfilesComparativa() As DataSet
        Get
            Return CType(Cache("dsPerfilesComparativa" & FSNUser.Cod), DataSet)
        End Get
    End Property
    Protected ReadOnly Property oPerfilesComparativaDenominaciones(ByVal IdPerfil As Integer) As DataRow
        Get
            Return CType(Cache("dsPerfilesComparativa" & FSNUser.Cod), DataSet).Tables(1).Select("ID=" & IdPerfil)(0)
        End Get
    End Property
    Private Sub CargarDatosComparativa(ByVal IdsPerfiles As String)
        Dim oPerfiles As FSNServer.Perfiles = FSNServer.Get_Object(GetType(FSNServer.Perfiles))
        Dim ds As DataSet = oPerfiles.CompararPerfiles(IdsPerfiles, FSNUser.Idioma.ToString, GetModulosActivos())
        Dim texto As String
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ComparativaPerfiles
        For Each modulo As String In Split(GetModulosActivos(), ",")
            Select Case modulo
                Case TipoAccesoModulos.FSGS
                    texto = Textos(3)
                Case TipoAccesoModulos.FSPM
                    texto = Textos(4)
                Case TipoAccesoModulos.FSEP
                    texto = Textos(5)
                Case TipoAccesoModulos.FSCM
                    texto = Textos(6)
                Case TipoAccesoModulos.FSSM
                    texto = Textos(7)
                Case TipoAccesoModulos.FSIM
                    texto = Textos(8)
                Case TipoAccesoModulos.FSQA
                    texto = Textos(9)
                Case TipoAccesoModulos.FSIS
                    texto = Textos(10)
                Case TipoAccesoModulos.FSCN
                    texto = Textos(11)
                Case TipoAccesoModulos.FSBI
                    texto = Textos(20)
                Case TipoAccesoModulos.FSAL
                    texto = Textos(21)
                Case Else
                    texto = ""
            End Select
            ds.Tables(0).Rows.Find(modulo)("DEN") = texto
        Next
        Me.InsertarEnCache("dsPerfilesComparativa" & FSNUser.Cod, ds)
    End Sub
    Private Function GetModulosActivos() As String
        Dim ModulosActivos As String = String.Empty
        'Vemos que modulos estan activos en la instalación y los pasamos para cargar los datos.
        If Acceso.gbAccesoFSGS Then _
            ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSGS, "," & TipoAccesoModulos.FSGS)
        If Not Acceso.gsAccesoFSPM = FSNLibrary.TipoAccesoFSPM.SinAcceso Then _
            ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSPM, "," & TipoAccesoModulos.FSPM)
        If Acceso.gbAccesoFSEP Then _
            ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSEP, "," & TipoAccesoModulos.FSEP)
        If Acceso.gbAccesoContratos Then _
            ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSCM, "," & TipoAccesoModulos.FSCM)
        If Acceso.gbAccesoFSSM Then _
            ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSSM, "," & TipoAccesoModulos.FSSM)
        If Acceso.g_bAccesoFSFA Then _
            ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSIM, "," & TipoAccesoModulos.FSIM)
        If Not Acceso.gsAccesoFSQA = FSNLibrary.TipoAccesoFSQA.SinAcceso Then _
            ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSQA, "," & TipoAccesoModulos.FSQA)
        If Acceso.g_bAccesoIS Then _
            ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSIS, "," & TipoAccesoModulos.FSIS)
        If Acceso.gbAccesoFSCN Then _
            ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSCN, "," & TipoAccesoModulos.FSCN)
        If Acceso.gbAccesoFSBI Then _
            ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSBI, "," & TipoAccesoModulos.FSBI)
        If Acceso.gbAccesoFSAL Then _
            ModulosActivos &= IIf(ModulosActivos Is String.Empty, TipoAccesoModulos.FSAL, "," & TipoAccesoModulos.FSAL)
        Return ModulosActivos
    End Function
#End Region
#Region "Inicio"
    Private Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString("popup") IsNot Nothing Then
            popup = True
            Me.MasterPageFile = "~/App_Master/EnBlanco.Master"
        End If
        If Request.QueryString("desdeGS") IsNot Nothing Then
            desdeGS = True
            Me.MasterPageFile = "~/App_Master/EnBlanco.Master"
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Utilizamos el WebDataGrid en vez de el WebHierarchicalDataGrid debido a que este ultimo aun teniendo la posibilidad del selfreference
        'para que por bandas se rellene con las mismas columnas, no tiene la posibilidad de fijar columnas y las bandas tienen un padding que hace
        'que las columnas no aparezcan alineadas a traves de las bandas.
        Dim idPerfiles As String = String.Empty
        If Not IsPostBack Then
            If Not popup AndAlso Not desdeGS Then
                Dim mMaster = CType(Me.Master, FSNWeb.Menu)
                mMaster.Seleccionar("Seguridad", "PerfilesSeguridad")
            End If
            idPerfiles = Request.QueryString("idPerfil")
            CargarDatosComparativa(idPerfiles)
            CrearColumnas_Comparativa(idPerfiles.Split(","))
            CargarComparativa()
            whdgComparativaPerfiles.Behaviors.ColumnFixing.FixedColumns.Add(New FixedColumnInfo("TIENEHIJOS"))
            whdgComparativaPerfiles.Behaviors.ColumnFixing.FixedColumns.Add(New FixedColumnInfo("DEN"))
            updComparativaPerfiles.Update()
        Else
            Select Case Request("__EVENTTARGET")
                Case btnRecargarComparativa.ClientID
                    idPerfiles = Request("__EVENTARGUMENT")
                    CargarDatosComparativa(idPerfiles)
                    CrearColumnas_Comparativa(idPerfiles.Split(","))
                    CargarComparativa()
                    whdgComparativaPerfiles.Behaviors.ColumnFixing.FixedColumns.Add(New FixedColumnInfo("TIENEHIJOS"))
                    whdgComparativaPerfiles.Behaviors.ColumnFixing.FixedColumns.Add(New FixedColumnInfo("DEN"))
                    updComparativaPerfiles.Update()
                Case btnRecargaExpand.ClientID
                    ExpandRows(CType(Request("__EVENTARGUMENT"), Integer))
                    CargarComparativa()
                    updComparativaPerfiles.Update()
                Case btnRecargaCollapse.ClientID
                    Dim ds As DataSet = oPerfilesComparativa
                    CollapseRows(ds, CType(Request("__EVENTARGUMENT"), Integer))
                    Me.InsertarEnCache("dsPerfilesComparativa" & FSNUser.Cod, ds)
                    CargarComparativa()
                    updComparativaPerfiles.Update()
                Case btnExportar.ClientID
                    CargarComparativa()
                    Select Case Request("__EVENTARGUMENT")
                        Case "Excel"
                            ExportarExcel()
                        Case "PDF"
                            ExportarPDF()
                    End Select
                Case Else
                    CargarComparativa()
                    updComparativaPerfiles.Update()
            End Select
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "IdPerfiles") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "IdPerfiles", "<script>var IdPerfiles='" & idPerfiles & "';var PerfilesSeleccionados=" & idPerfiles.Split(",").Length & "</script>")
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "desdeGS") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "desdeGS", "<script>var desdeGS=" & IIf(Request.QueryString("desdeGS") IsNot Nothing, "true", "false") & ";</script>")
        End If
        With FSNPageHeader
            .UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/perfiles.png"
            .VisibleBotonVolver = Not popup
            .OnClientClickVolver = "window.location.href='" & ConfigurationManager.AppSettings("rutaFS") & "SG/VisorPerfiles.aspx?pag=" & CType(Request.QueryString("pag"), Integer) & IIf(desdeGS, "&desdeGS=1", "") & "';return false;"
        End With
        imgOpcionesComparativa.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/comparar.png"
        imgExcel.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/Excel.png"
        imgPDF.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/PDF.png"
        imgCabeceraOpcionesComparativa.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/images/perfiles.png"

        CargarTextos()
    End Sub
    Private Sub CargarTextos()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ComparativaPerfiles

        FSNPageHeader.TituloCabecera = Textos(0)
        FSNPageHeader.TextoBotonVolver = Textos(1)
        lblOpcionesComparativa.Text = Textos(2)
        lblExcel.Text = Textos(12)
        lblPDF.Text = Textos(13)
        lblAceptarOpcionesComparativa.Text = Textos(14)
        lblCancelarOpcionesComparativa.Text = Textos(15)
        lblCabeceraOpcionesComparativa.Text = Textos(18)
        lblDescripcionOpcionesComparativa.Text = Textos(19)
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
            Dim sVariableJavascriptTextosPantalla As String = "var TextosPantalla = new Array();"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[0]='" & JSText(Textos(19)) & ":';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextosPantalla, True)
        End If
    End Sub
    Private Sub ExpandRows(ByVal Id As Integer)
        Dim ds As DataSet = oPerfilesComparativa
        ds.Tables(0).Rows.Find(Id)("EXPANDED") = True
        For Each row As DataRow In ds.Tables(0).Select("IDPADRE=" & Id)
            row("VISIBLE") = True
        Next
        Me.InsertarEnCache("dsPerfilesComparativa" & FSNUser.Cod, ds)
    End Sub
    Private Sub CollapseRows(ByRef ds As DataSet, ByVal Id As Integer)
        ds.Tables(0).Rows.Find(Id)("EXPANDED") = False
        For Each row As DataRow In ds.Tables(0).Select("IDPADRE=" & Id)
            row("VISIBLE") = False
            If ds.Tables(0).Select("IDPADRE=" & row("ID")).Length > 0 Then
                CollapseRows(ds, row("ID"))
            End If
        Next
    End Sub
#End Region
#Region "WebHierarchicalDataGrid"
    Private Sub CrearColumnas_Comparativa(ByVal IdsPerfiles() As String)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ComparativaPerfiles
        whdgComparativaPerfiles.Rows.Clear()
        whdgComparativaPerfiles.Columns.Clear()
        Dim camposGrid As New List(Of BoundDataField)
        Dim campo As BoundDataField

        campo = New BoundDataField(True)
        With campo
            .Key = "ID"
            .Hidden = True
            .DataFieldName = "ID"
        End With
        camposGrid.Add(campo)

        campo = New BoundDataField(True)
        With campo
            .Key = "EXPANDED"
            .Hidden = True
            .DataFieldName = "EXPANDED"
        End With
        camposGrid.Add(campo)

        campo = New BoundDataField(True)
        With campo
            .Key = "TIENEHIJOS"
            .Hidden = False
            .DataFieldName = "TIENEHIJOS"
            .Header.Text = ""
            .Width = Unit.Pixel(25)
        End With
        camposGrid.Add(campo)

        campo = New BoundDataField(True)
        With campo
            .Key = "DEN"
            .Hidden = False
            .DataFieldName = "DEN"
            .Header.Text = "DENOMINACION"
            .Width = Unit.Pixel(400)
        End With
        camposGrid.Add(campo)

        Dim camposCheckGrid As New List(Of BoundCheckBoxField)
        Dim campoCheck As BoundCheckBoxField
        Dim dPerfil As DataRow
        For Each id As String In IdsPerfiles
            campoCheck = New BoundCheckBoxField(True)
            dPerfil = oPerfilesComparativaDenominaciones(id)
            With campoCheck
                .Key = id
                .DataFieldName = id
                .Header.Text = dPerfil("COD") & " - " & dPerfil("DEN")
                .Hidden = False
                .CheckBox.PartialImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/vacio.png"
                .CheckBox.CheckedImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/aprobado.gif"
            End With
            camposCheckGrid.Add(campoCheck)
        Next
        For Each field As BoundDataField In camposGrid
            whdgComparativaPerfiles.Columns.Add(field)
        Next
        For Each field As BoundCheckBoxField In camposCheckGrid
            whdgComparativaPerfiles.Columns.Add(field)
        Next
    End Sub
    Private Sub CargarComparativa()
        With whdgComparativaPerfiles
            .Rows.Clear()
            .DataSource = oPerfilesComparativa.Tables(0).Select("VISIBLE=1").CopyToDataTable
            .DataMember = "ESQUEMA"
            .DataKeyFields = "ID"
            .DataBind()
        End With
    End Sub
    Private Sub whdgComparativaPerfiles_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgComparativaPerfiles.InitializeRow
        Dim cellIndex As Integer
        cellIndex = whdgComparativaPerfiles.Columns.FromKey("TIENEHIJOS").Index
        If CType(e.Row.DataItem.Item("TIENEHIJOS"), Boolean) Then
            e.Row.Items(cellIndex).CssClass = "Link"
            If CType(e.Row.DataItem.Item("EXPANDED"), Boolean) Then
                e.Row.Items(cellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Me.Page.Theme & "/images/ighg_Collapse.gif" & "'/>"
            Else
                e.Row.Items(cellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Me.Page.Theme & "/images/ighg_Expand.gif" & "'/>"
            End If
        Else
            e.Row.Items(cellIndex).Text = ""
        End If
        cellIndex = whdgComparativaPerfiles.Columns.FromKey("DEN").Index
        e.Row.Items(cellIndex).Text = "<span style='margin-left:" & 5 * CType(e.Row.DataItem.Item("NIVEL"), Integer) & "px; display:block;'>" & e.Row.DataItem.Item("DEN") & "</span>"

        If e.Row.DataItem.Item("RESALTARFILA") Then
            For Each celda As BoundDataField In whdgComparativaPerfiles.Columns
                Select Case celda.Key
                    Case "ID", "EXPANDED", "TIENEHIJOS", "DEN"
                    Case Else
                        cellIndex = whdgComparativaPerfiles.Columns.FromKey(celda.Key).Index
                        e.Row.Items(cellIndex).CssClass = "ResaltarFilaGrid"
                End Select
            Next
        End If
    End Sub
#End Region
#Region "Page Methods"
    <System.Web.Services.WebMethod(True), _
    System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function PerfilesComparativa(ByVal idPerfiles As String) As List(Of FSNServer.cn_fsTreeViewItem)
        Try
            Dim User As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim listaPerfiles As New List(Of Integer)
            For Each idPerfil As String In idPerfiles.Split(",")
                listaPerfiles.Add(idPerfil)
            Next
            Dim oPerfiles As New List(Of FSNServer.cn_fsTreeViewItem)
            Dim oPerfil As FSNServer.cn_fsTreeViewItem
            Dim dsPerfilesSeguridad As DataSet
            dsPerfilesSeguridad = CType(HttpContext.Current.Cache("dsPerfilesComparativa" & User.Cod), DataSet)
            For Each perfil As DataRow In dsPerfilesSeguridad.Tables(1).Rows
                oPerfil = New FSNServer.cn_fsTreeViewItem
                With oPerfil
                    .value = perfil("ID")
                    .text = perfil("COD") & " - " & perfil("DEN")
                    .type = 5
                    .selectable = True
                    .checked = listaPerfiles.Contains(perfil("ID"))
                End With
                oPerfiles.Add(oPerfil)
            Next
            Return oPerfiles
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
#Region "Exportacion"
    Private Sub ExportarExcel()
        Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
        fileName = fileName.Replace("+", "%20")

        whdgComparativaPerfiles.Columns.FromKey("TIENEHIJOS").Hidden = True
        With wdgExcelExporter
            .DownloadName = fileName
            .DataExportMode = DataExportMode.DataInGridOnly
            .Export(whdgComparativaPerfiles)
        End With
        whdgComparativaPerfiles.Columns.FromKey("TIENEHIJOS").Hidden = False
    End Sub
    Private Sub wdgExcelExporter_GridRecordItemExported(sender As Object, e As Infragistics.Web.UI.GridControls.GridRecordItemExportedEventArgs) Handles wdgExcelExporter.GridRecordItemExported
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ComparativaPerfiles
        e.WorksheetCell.CellFormat.Fill = New Infragistics.Documents.Excel.CellFillPattern(New Infragistics.Documents.Excel.WorkbookColorInfo(Drawing.Color.Transparent), Nothing, Infragistics.Documents.Excel.FillPatternStyle.None)
        Select Case e.GridCell.Column.Key
            Case "DEN"
                With e.WorksheetCell
                    For I As Integer = 0 To CType(e.GridCell.Row.DataItem.Item("NIVEL"), Integer)
                        .Value = "   " & .Value
                    Next
                End With
            Case Else
                If Not IsDBNull(e.GridCell.Value) Then
                    With e.WorksheetCell
                        .Value = IIf(e.GridCell.Value, Textos(16), Textos(17))
                    End With
                End If
        End Select
    End Sub
    Private Sub ExportarPDF()
        Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
        fileName = fileName.Replace("+", "%20")
        wdgPDFExporter.DownloadName = fileName

        whdgComparativaPerfiles.Columns.FromKey("TIENEHIJOS").Hidden = True
        With wdgPDFExporter
            .EnableStylesExport = False
            .DataExportMode = DataExportMode.DataInGridOnly
            .TargetPaperOrientation = PageOrientation.Landscape
            .Margins = PageMargins.GetPageMargins("Narrow")
            .TargetPaperSize = PageSizes.GetPageSize("A4")

            .Export(whdgComparativaPerfiles)
        End With
        whdgComparativaPerfiles.Columns.FromKey("TIENEHIJOS").Hidden = False
    End Sub
    Private Sub wdgPDFExporter_GridFieldCaptionExporting(sender As Object, e As Infragistics.Web.UI.GridControls.DocumentGridFieldCaptionExportingEventArgs) Handles wdgPDFExporter.GridFieldCaptionExporting
        e.CellElement.Background = New Infragistics.Documents.Reports.Report.Background(Infragistics.Documents.Reports.Graphics.Colors.LightGray)
    End Sub
    Private Sub wdgPDFExporter_GridRecordItemExporting(sender As Object, e As Infragistics.Web.UI.GridControls.DocumentGridRecordItemExportingEventArgs) Handles wdgPDFExporter.GridRecordItemExporting
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ComparativaPerfiles
        Select Case e.GridCell.Column.Key
            Case "DEN"
                e.ExportValue = ""
                e.CellElement.Paddings.Left = 10 * CType(e.GridCell.Row.DataItem.Item("NIVEL"), Integer)
                e.ExportValue = e.GridCell.Row.DataItem.Item("DEN").ToString
            Case Else
                e.ExportValue = IIf(Not IsDBNull(e.GridCell.Value) AndAlso e.GridCell.Value, Textos(16), Textos(17))
        End Select
    End Sub
#End Region
End Class

