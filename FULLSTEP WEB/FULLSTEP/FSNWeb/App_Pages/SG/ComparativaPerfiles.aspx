﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="ComparativaPerfiles.aspx.vb" Inherits="Fullstep.FSNWeb.ComparativaPerfiles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH4" runat="server">
	<script type="text/javascript">
		function whdgComparativaPerfiles_Click(sender, e) {
		    if (e.get_type() == "cell") {
		        var Id = e.get_item().get_row().get_cellByColumnKey("ID").get_value();
				if (e.get_item().get_row().get_cellByColumnKey("EXPANDED").get_value()) {
				    __doPostBack($('[id$=btnRecargaCollapse]').attr('id'), Id);			        
				}else{                    
                    __doPostBack($('[id$=btnRecargaExpand]').attr('id'),Id);
				}
			}
        }
        var oPerfilesComparativaInicio,oPerfilesComparativa,timeout;
        function OpcionesComparativa() {
            if (oPerfilesComparativaInicio == undefined) {
                $.when($.ajax({
                    type: 'POST',
                    url: rutaFS + 'SG/ComparativaPerfiles.aspx/PerfilesComparativa',
                    data: JSON.stringify({ idPerfiles: IdPerfiles }),
                    contentType: 'application/json;',
                    dataType: 'json',
                    async: true
                })).done(function (msg) {
                    oPerfilesComparativaInicio = msg.d;
                    oPerfilesComparativa = msg.d;

                    $('#tvPerfilesComparativa').tree({
                        data: oPerfilesComparativa,
                        autoOpen: true,
                        selectable: true,
                        checkbox: true
                    });
                    if (desdeGS) setTimeout(function () { MostrarPopUp(); }, 100);
                    else MostrarPopUp();
                });
                $('#tvPerfilesComparativa :checkbox').live('click', function () {
                    var id = $(this).attr('id').replace('chktvPerfilesComparativa-', '');
                    var checked = $(this).attr('checked') == undefined ? false : true;
                    $.each(oPerfilesComparativa, function () {
                        if (this.value == id) {
                            this.checked = checked;
                            if (checked) PerfilesSeleccionados += 1;
                            else PerfilesSeleccionados -= 1;
                            return false;
                        }
                    });                    
                });
                $('#btnCancelarOpcionesComparativa,#btnCerrarPopUpOpcionesComparativa').click(function () {
                    oPerfilesComparativa = $.parseJSON(JSON.stringify(oPerfilesComparativaInicio));
                    $('#popupFondo').hide();
                    $('#divPerfilesComparativa').hide();
                });
                $('#btnAceptarOpcionesComparativa').click(function () {
                    if (PerfilesSeleccionados == 0) {
                        alert(TextosPantalla[0]);
                        return false;
                    };
                    var Ids = '';
                    $.each(oPerfilesComparativa, function () {
                        if (this.checked) Ids = (Ids == '' ? '' : Ids + ',') + this.value;
                    });
                    $('#popupFondo').hide();
                    $('#divPerfilesComparativa').hide();
                    __doPostBack($('[id$=btnRecargarComparativa]').attr('id'), Ids)
                });
            } else {     
                MostrarPopUp();
            }
        }
        function MostrarPopUp() {
            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').show();
            $('#divPerfilesComparativa').show();
            var windowH = $(window).height();
            $('#divPerfilesComparativa').css('height', windowH * 0.6);
            $('#tvPerfilesComparativa').css('height', $('#divPerfilesComparativa').height() - 20 - $('#divBotones').outerHeight() - $('#divTitulo').outerHeight() - $('#divDescripcion').outerHeight());            
            CentrarPopUp($('#divPerfilesComparativa'));
        };
        $(window).resize(function () {
            if ($('#divPerfilesComparativa').is(':visible')) MostrarPopUp();
        });
        function ExportarExcel() { __doPostBack($('[id$=btnExportar]').attr('id'), 'Excel'); };
        function ExportarPDF() { __doPostBack($('[id$=btnExportar]').attr('id'), 'PDF'); };
	</script>
    <ig:WebExcelExporter runat="server" ID="wdgExcelExporter"></ig:WebExcelExporter>
    <ig:WebDocumentExporter runat="server" ID="wdgPDFExporter"></ig:WebDocumentExporter>
	<div style="clear:both; float:left; width:100%;">
		<fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
        <asp:Button runat="server" ID="btnExportar" style="display:none;"/>
        <asp:Button runat="server" ID="btnRecargarComparativa" style="display:none;"/>
        <asp:Button runat="server" ID="btnRecargaExpand" style="display:none;"/>
        <asp:Button runat="server" ID="btnRecargaCollapse" style="display:none;"/>
	</div>
    <div class="CabeceraBotones" style="width:100%; height:25px; clear:both; line-height:25px;">
        <div style="float:left;">
            <div onclick="OpcionesComparativa()" class="botonIzquierdaCabecera">
                <asp:Image runat="server" ID="imgOpcionesComparativa" CssClass="imagenCabecera" />
			    <asp:Label runat="server" ID="lblOpcionesComparativa"></asp:Label> 
		    </div>
		</div>
        <div style="float:right;">				
			<div onclick="ExportarExcel()" class="botonDerechaCabecera" style="float:left;">            
				<asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
				<asp:Label runat="server" ID="lblExcel" CssClass="fntLogin2" style="margin-left:3px;" Text="dExcel"></asp:Label>           
			</div>
			<div onclick="ExportarPDF()" class="botonDerechaCabecera" style="float:left;">            
				<asp:Image runat="server" ID="imgPDF" CssClass="imagenCabecera" />
				<asp:Label runat="server" ID="lblPDF" CssClass="fntLogin2" style="margin-left:3px;" Text="dPDF"></asp:Label>           
			</div>
		</div>
    </div>
    <asp:UpdatePanel runat="server" ID="updComparativaPerfiles" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnRecargarComparativa" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnRecargaExpand" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnRecargaCollapse" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <ig:WebDataGrid ID="whdgComparativaPerfiles" runat="server" 
		        AutoGenerateColumns="false" DataKeyFields="ID" Width="100%">
		        <ClientEvents Click="whdgComparativaPerfiles_Click" />
		        <Behaviors>                     
			        <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>                 
			        <ig:ColumnFixing ShowFixButtons="false" ShowRightSeparator="true" AutoAdjustCells="true"/>                    
		        </Behaviors>
	        </ig:WebDataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divPerfilesComparativa" class="popupCN" style="display:none; position:absolute; z-index:1004; height:60%; min-width:40%; max-width:70%; padding:10px;">
        <div id="btnCerrarPopUpOpcionesComparativa" class="CerrarPopUp" style="float:right; width:30px; height:30px;"></div>
        <div id="divTitulo" style="clear:both; float:left; width:100%;">
            <asp:Image runat="server" ID="imgCabeceraOpcionesComparativa" />
            <asp:Label runat="server" ID="lblCabeceraOpcionesComparativa" CssClass="Texto16 TextoAlternativo Negrita"></asp:Label>
        </div>
        <div id="divDescripcion" style="clear:both; float:left; width:100%; margin-top:5px;">
            <asp:Label runat="server" ID="lblDescripcionOpcionesComparativa" CssClass="Texto12"></asp:Label>
        </div>
        <div id="tvPerfilesComparativa" class="Bordear" style="clear:both; float:left; position:relative; width:100%; overflow:auto; margin-right:5px;"></div>
        <div id="divBotones" style="position:relative; clear:both; float:left; width:100%; margin-top:15px; text-align:center;">             
            <div id="btnAceptarOpcionesComparativa" class="botonRedondeado" style="margin-right:5px;">
                <asp:Label runat="server" ID="lblAceptarOpcionesComparativa" style="line-height:23px;"></asp:Label>
		    </div>
		    <div id="btnCancelarOpcionesComparativa" class="botonRedondeado" style="margin-left:5px;">
			    <asp:Label runat="server" ID="lblCancelarOpcionesComparativa" style="line-height:23px;"></asp:Label>
		    </div>
        </div>
    </div>
</asp:Content>