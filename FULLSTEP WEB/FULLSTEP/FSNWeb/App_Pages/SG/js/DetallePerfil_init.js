﻿var oPerfil, oPerfilOriginal;
var UONsLoaded = false;
var QlikUONsLoaded = false;
var QlikGMNsLoaded = false;
var UNQALoaded = false;
var isDeschequeoAscendente = false;
var UONsAux;
var QlikUONsAux;
var QlikGMNsAux;
var UNQAsAux;
$(document).ready(function () {

    if (typeof (EnBlancoMaster) !== 'undefined' && EnBlancoMaster) $('body').addClass('fondoMasterEnBlanco');
    /*Gestionamos todos los cambios realizados en pantalla en variables json. 
    Una variable sera el perfil inicial y otra guardara todos los cambios.
    Cada vez que se guarden cambios ambas variables se igualaran. 
    En estas variables tendremos todos los datos del perfil cargado, codigo, denominaciones, acciones, uons....*/
    $('#lblAceptarUONs').text(TextosPantalla[0]);
    $('#lblCancelarUONs').text(TextosPantalla[1]);
    $('#divCabeceraUONsPerfil span').text(TextosPantalla[2]);
    $('#divCabeceraUONsPerfil img').attr('src', ruta + 'images/UON.png');

    $('#lblAceptarUNQA').text(TextosPantalla[0]);
    $('#lblCancelarUNQA').text(TextosPantalla[1]);
    $('#divCabeceraUNQAPerfil span').text(TextosPantalla[2]);
    $('#divCabeceraUNQAPerfil img').attr('src', ruta + 'images/UNQA.png');

    $('#lblAceptarQlikUONs').text(TextosPantalla[0]);
    $('#lblCancelarQlikUONs').text(TextosPantalla[1]);
    $('#divCabeceraQlikUONsPerfil span').text(TextosPantalla[2]);
    $('#divCabeceraQlikUONsPerfil img').attr('src', ruta + 'images/UON.png');
    $('#lblAceptarQlikGMNs').text(TextosPantalla[0]);
    $('#lblCancelarQlikGMNs').text(TextosPantalla[1]);
    $('#divCabeceraQlikGMNsPerfil span').text(TextosPantalla[3]);
    $('#divCabeceraQlikGMNsPerfil img').attr('src', ruta + 'images/UON.png');
    /*Obtenemos en formato string todos los datos que mostraremos en la pantalla*/
    $.when($.ajax({
        type: 'POST',
        url: rutaFS + 'SG/DetallePerfil.aspx/Obtener_Datos_Perfil',
        contentType: 'application/json;',
        async: true
    })).done(function (msg) {
        /*Parseamos el string a JSON para facilitar el manejo de datos*/
        oPerfilOriginal = $.parseJSON(msg.d);
        oPerfil = $.parseJSON(msg.d);
        $('#txtCodigoPerfil').val(oPerfil.Codigo);
        $('#txtDenominacion').val(oPerfil.Denominaciones[oPerfil.IdiomaDefecto]);
        /*Por cada modulo activo mostraremos el check de si el perfil tiene o no acceso a dicho modulo
        si tiene acceso habilitaremos el panel del menu de las acciones asociadas a dicho modulo.
        Dependera de en que tipo estemos en la pantalla. Si es tipo copia, cargaremos datos menos codigo y denominacion.*/
        $.each(oPerfil.AccesosPerfil, function (key, value) {            
            switch (key) {                
                case '9','12':
                    break;
                default:
                    $('#divOpcion').tmpl({ modulo: key, dependantPanel: (oPerfil.MenuAcciones[key].length == 0 ? 0 : key) }).appendTo($('#divDatosGenerales'));
                    break;
            };
            $('#chkAcceso_' + key).attr('checked', (tipoDetalleCopia == 'new') ? !value : value);
            $('#lblAcceso_' + key).text(TextosModulos[key]);
            $('#divOpciones').tmpl({ key: key }).appendTo($('#divPanelesModulos'));
            if (tipoDetalleCopia !== 'new' && value) {
                $('#' + $('#chkAcceso_' + key).attr('dependantPanel')).show();
                $('#lblCabecera_' + key).show();
            }            
            $('#lblCabecera_' + key).text(TextosModulos[key]);
            if ($('#chkAcceso_' + key).length == 0 && value) $('#divOpciones_' + key).show();
            /*Si el modulo no tiene mas que un menu hijo, cargaremos directamente las acciones sin cargar el menu*/
            if (oPerfil.MenuAcciones[key].length == 1 && oPerfil.MenuAcciones[key][0].children.length == 0) {
                $('#divPermisos_' + key).append($('#divPermisosContent').html());
                $('#divAccionesMenu_').attr('id', 'divAccionesMenu_' + key);
                $('#divAcciones_').attr('id', 'divAcciones_' + key);
                $('#lblTituloAccionesMenu_').attr('id', 'lblTituloAccionesMenu_' + key);

                var options = BuscarOptions(oPerfil.MenuAcciones[key], 0, ['0_' + oPerfil.MenuAcciones[key][0].value]);
                $('#divAccionesMenu_' + key).empty();
                // Crear lista para los post
                $('#divAccionesMenu_' + key).append($('<ul style="list-style:none; padding:0px; margin:0px;">'));
                var ul = $('#divAccionesMenu_' + key + ' ul');
                $('#optionMenuItem').tmpl(options).appendTo(ul);
                $.each(oPerfil.AccionesPerfil, function () {
                    $('#chkAccion_' + this).attr('checked', true);
                });
            } else {
                $('#divPermisos_' + key).append($('#divPermisosContentMenu').html());
                $('#divMenu_').attr('id', 'divMenu_' + key);
                $('#divAccionesMenu_').attr('id', 'divAccionesMenu_' + key);
                $('#divAcciones_').attr('id', 'divAcciones_' + key);
                $('#lblTituloAccionesMenu_').attr('id', 'lblTituloAccionesMenu_' + key);
                $('#divMenu_' + key).tree({
                    data: oPerfil.MenuAcciones[key],
                    autoOpen: true,
                    selectable: true
                });
            }
        });
        if ($('#chkAcceso_2').attr('checked') && $('#chkAcceso_4').attr('checked')) $('[id$=lbly]').show();
        else $('[id$=lbly]').hide();
    });
    $('#imgDatosGenerales').attr('src', rutaFS + 'Images/expandir.gif');
    $('.CollapsiblePanelHeader').live('click', function () {
        $(this).toggleClass('CollapsiblePanelHeaderExpand').toggleClass('CollapsiblePanelHeaderCollapse');
        $('#' + $(this).attr('dependantPanel')).toggle();
    });
    $('.treeTipo3').live('click', function () {
        var id = $(this).attr('id');
        var tipoAcceso = parseInt(id.split('-')[0].split('_')[1]);
        var items = id.replace(id.split('-')[0] + '-', '').split('-');
        var options = BuscarOptions(oPerfil.MenuAcciones[tipoAcceso], 0, items);
        $('#divAcciones_' + tipoAcceso + ' p.SeparadorInf').hide();
        if (options.length == 0) {
            $('#lblTituloAccionesMenu_' + tipoAcceso).text();
            $('#divAccionesMenu_' + tipoAcceso).empty();
        } else {
            $('#lblTituloAccionesMenu_' + tipoAcceso).text($(this).text());
            $('#divAccionesMenu_' + tipoAcceso).empty();
            // Crear lista para los post
            $('#divAccionesMenu_' + tipoAcceso).append($('<ul style="list-style:none; padding:0px; margin:0px;">'));
            var ul = $('#divAccionesMenu_' + tipoAcceso + ' ul');
            $('#optionMenuItem').tmpl(options).appendTo(ul);
            $.each(oPerfil.AccionesPerfil, function () {
                $('#chkAccion_' + this).attr('checked', true);
            });
            $('#divAcciones_' + tipoAcceso + ' p.SeparadorInf').show();
        }
    });
    $('[id^=chkAcceso_]').live('click', function () {
        var id = parseInt($(this).attr('id').split('_')[1]);
        var checked = $(this).attr('checked') == undefined ? false : true;
        oPerfil.AccesosPerfil[id] = checked;
        if (checked) {
            //En caso de marcar al acceso al modulo FSBI, marcamos todos los check del arbol de UONs y de GMNs para Qlik
            if ($(this).attr('id') == 'chkAcceso_' + acTipoAccesoModulosFSBI.toString()) {
                DesmarcarTodasQlikUONS();
                DesmarcarTodasQlikGMNS();
                MarcarTodasQlikUONS();
                MarcarTodasQlikGMNS();
            }

            //Si está activo el PM o el IM o el QA o el CM se muestra el grupo de Seguridad de solicitudes
            if ($(this).attr('id') == 'chkAcceso_' + acTipoAccesoModulosFSPM.toString() || $(this).attr('id') == 'chkAcceso_' + acTipoAccesoModulosFSIM.toString() ||
                $(this).attr('id') == 'chkAcceso_' + acTipoAccesoModulosFSQA.toString() || $(this).attr('id') == 'chkAcceso_' + acTipoAccesoModulosFSCM.toString()) {
                if ($('#chkAcceso_' + acTipoAccesoModulosFSPM.toString()).attr('checked') || $('#chkAcceso_' + acTipoAccesoModulosFSIM.toString()).attr('checked') ||
                    $('#chkAcceso_' + acTipoAccesoModulosFSQA.toString()).attr('checked') || $('#chkAcceso_' + acTipoAccesoModulosFSCM.toString()).attr('checked')) {
                    $('#lblCabecera_12').show();
                    $('#divOpciones_12').show();
                }
            }
            
            $('#' + $(this).attr('dependantPanel')).show();
            $('#lblCabecera_' + id).show();
        } else {            
            //En caso de desmarcar al acceso al modulo FSBI, desmarcamos todos los check del arbol de UONs y de GMNs para Qlik
            if ($(this).attr('id') == 'chkAcceso_' + acTipoAccesoModulosFSBI.toString()) {
                DesmarcarTodasQlikUONS();
                DesmarcarTodasQlikGMNS();
            }
            $('#lblCabecera_' + id).hide();

            //Si no está activo ni el PM, ni el IM, ni el QA ni el CM se oculta el grupo de Seguridad de solicitudes
            if ($(this).attr('id') == 'chkAcceso_' + acTipoAccesoModulosFSPM.toString() || $(this).attr('id') == 'chkAcceso_' + acTipoAccesoModulosFSIM.toString() ||
                $(this).attr('id') == 'chkAcceso_' + acTipoAccesoModulosFSQA.toString() || $(this).attr('id') == 'chkAcceso_' + acTipoAccesoModulosFSCM.toString()) {
                if ($('#chkAcceso_' + acTipoAccesoModulosFSPM.toString()).attr('checked') == undefined && $('#chkAcceso_' + acTipoAccesoModulosFSIM.toString()).attr('checked') == undefined &&
                    $('#chkAcceso_' + acTipoAccesoModulosFSQA.toString()).attr('checked') == undefined && $('#chkAcceso_' + acTipoAccesoModulosFSCM.toString()).attr('checked') == undefined) {
                    $('#lblCabecera_12').hide();
                    $('#divOpciones_12').hide();
                }
            }                                                
            
            $('#lblCabecera_' + id).hide();
            $('#' + $(this).attr('dependantPanel')).hide();            
        };
        if ($('#chkAcceso_2').attr('checked') && $('#chkAcceso_4').attr('checked')) $('#lbly').show();
        else $('#lbly').hide();
    });
    $('[id^=chkAccion_]').live('click', function () {
        var id = parseInt($(this).attr('id').split('_')[1]);
        var checked = $(this).attr('checked') == undefined ? false : true;
        if (checked) {
            oPerfil.AccionesPerfil[id] = id;
            if (typeof (oPerfil.AccionesDependientesCheckCheck[id]) !== 'undefined') {
                $.each(oPerfil.AccionesDependientesCheckCheck[id], function () {
                    $('#chkAccion_' + this).attr('checked', true);
                    oPerfil.AccionesPerfil[this] = this;
                });
            }
            if (typeof (oPerfil.AccionesDependientesCheckUnCheck[id]) !== 'undefined') {
                $.each(oPerfil.AccionesDependientesCheckUnCheck[id], function () {
                    $('#chkAccion_' + this).attr('checked', false);
                    delete oPerfil.AccionesPerfil[this];
                });
            }
        } else {
            delete oPerfil.AccionesPerfil[id];
            if (typeof (oPerfil.AccionesDependientesUnCheckCheck[id]) !== 'undefined') {
                $.each(oPerfil.AccionesDependientesUnCheckCheck[id], function () {
                    $('#chkAccion_' + this).attr('checked', true);
                    oPerfil.AccionesPerfil[this] = this;
                });
            }
            if (typeof (oPerfil.AccionesDependientesUnCheckUnCheck[id]) !== 'undefined') {
                $.each(oPerfil.AccionesDependientesUnCheckUnCheck[id], function () {
                    $('#chkAccion_' + this).attr('checked', false);
                    delete oPerfil.AccionesPerfil[this];
                });
            }
            //En caso de desmarcar la restriccion de visibilidad segun UONs para Qlik, marcamos todos los check del arbol de UONs para Qlik
            if ($(this).attr('id') == 'chkAccion_' + accBIRestringirVisibilidadQlikAppsUONs.toString()) {
                DesmarcarTodasQlikUONS();
                MarcarTodasQlikUONS();
            }
            //En caso de desmarcar la restriccion de visibilidad segun materiales para Qlik, marcamos todos los check del arbol de GMNs para Qlik
            if ($(this).attr('id') == 'chkAccion_' + accBIRestringirVisibilidadQlikAppsGMNs.toString()) {
                DesmarcarTodasQlikGMNS();
                MarcarTodasQlikGMNS();
            }
        }
    });
    $('[id^=imgAccion_]').live('click', function () {
        //Detectamos que opcion de arbol se ha seleccionado. 
        //Si es el arbol de UONs o de GMNs de Qlik se muestran sus arboles, si no se muestra el arbol de UONs general
        if ($(this).attr('id') == 'imgAccion_' + accBIRestringirVisibilidadQlikAppsUONs.toString()) {
            QlikUONsAux = $.parseJSON(JSON.stringify(oPerfil.QlikUONS));
            if (QlikUONsLoaded) $('#QlikUONsPerfilTree').tree('destroy');
            QlikUONsLoaded = true;
            $('#QlikUONsPerfilTree').empty();
            $('#QlikUONsPerfilTree').tree({
                data: oPerfil.QlikUONS,
                autoOpen: true,
                selectable: true,
                checkbox: true,
                checkChildren: true
            });
            MostrarPopUpQlikUONs();
        }
        else if ($(this).attr('id') == 'imgAccion_' + accBIRestringirVisibilidadQlikAppsGMNs.toString()) {
            QlikGMNsAux = $.parseJSON(JSON.stringify(oPerfil.QlikGMNS));
            if (QlikGMNsLoaded) $('#QlikGMNsPerfilTree').tree('destroy');
            QlikGMNsLoaded = true;
            $('#QlikGMNsPerfilTree').empty();
            $('#QlikGMNsPerfilTree').tree({
                data: oPerfil.QlikGMNS,
                autoOpen: true,
                selectable: true,
                checkbox: true,
                checkChildren: true
            });
            MostrarPopUpQlikGMNs();
        }
        else if ($(this).attr('id') == 'imgAccion_' + accQARestringirConsultaCertificadossUnidadesNegocio.toString()) {
            UNQAAux = $.parseJSON(JSON.stringify(oPerfil.UNQA));
            if (UNQALoaded) $('#UNQAPerfilTree').tree('destroy');
            UNQALoaded = true;
            $('#UNQAPerfilTree').empty();
            $('#UNQAPerfilTree').tree({
                data: oPerfil.UNQA,
                autoOpen: true,
                selectable: true,
                checkbox: true,
                checkChildren: true
            });
            MostrarPopUpUNQA();
        }
        else {
            UONsAux = $.parseJSON(JSON.stringify(oPerfil.UONS));
            if (UONsLoaded) $('#UONsPerfilTree').tree('destroy');
            UONsLoaded = true;
            $('#UONsPerfilTree').empty();
            $('#UONsPerfilTree').tree({
                data: oPerfil.UONS,
                autoOpen: true,
                selectable: true,
                checkbox: true,
                checkChildren: true
            });
            MostrarPopUp();
        }
    });

    $('#btnAceptarUONs').live('click', function () {
        hijos($('ul.tree>ul>li>:checkbox'));
        oPerfil.UONS = $.parseJSON(JSON.stringify(UONsAux));
        $('#popupFondo').hide();
        $('#popupUONsPerfil').hide();
    });
    $('#btnCancelarUONs,#btnCerrarUONsPerfil').live('click', function () {
        $('#popupFondo').hide();
        $('#popupUONsPerfil').hide();
    });
    $('#UONsPerfilTree :checkbox').live('click', function () {
        /*Gestionamos las UON del perfil. El problema es gestionar estas UON en la variable json de la pantalla
        ya que hay que navegar a traves del arbol que creamos con las unidades organizativas*/
        var codes = $(this).attr('id').replace('chkUONsPerfilTree-', '').split('-');
        var checked = $(this).attr('checked') == undefined ? false : true;
        $.each(UONsAux, function (key0, value0) {
            if (this.value == codes[0].split('_')[1]) {
                if (codes.length == 1) {
                    UONsAux[key0].checked = checked;
                    chequeoDescendente(UONsAux[key0], checked);
                    return false;
                } else {
                    $.each(this.children, function (key1, value1) {
                        if (this.value == codes[1].split('_')[1]) {
                            if (codes.length == 2) {
                                UONsAux[key0].children[key1].checked = checked;
                                chequeoDescendente(UONsAux[key0].children[key1], checked);
                                if (!checked) {
                                    isDeschequeoAscendente = true;
                                    UONsAux[key0].checked = false;
                                    isDeschequeoAscendente = false;
                                }
                                return false;
                            } else {
                                $.each(this.children, function (key2, value2) {
                                    if (this.value == codes[2].split('_')[1]) {
                                        if (codes.length == 3) {
                                            UONsAux[key0].children[key1].children[key2].checked = checked;
                                            chequeoDescendente(UONsAux[key0].children[key1].children[key2], checked);
                                            if (!checked) {
                                                //Se le pasa el key del elemento que se ha pinchado ya que todos los hijos ya están desmarcados y no hay que volver a comprobarlos
                                                remarcarHijos('UONsPerfilTree', UONsAux[key0].children[key1], key2);
                                                isDeschequeoAscendente = true;
                                                UONsAux[key0].children[key1].checked = false;
                                                UONsAux[key0].checked = false;
                                                isDeschequeoAscendente = false;
                                            }
                                            return false;
                                        } else {
                                            $.each(this.children, function (key3, value3) {
                                                if (this.value == codes[3].split('_')[1]) {
                                                    UONsAux[key0].children[key1].children[key2].children[key3].checked = checked;
                                                    chequeoDescendente(UONsAux[key0].children[key1].children[key2].children[key3], checked);
                                                    if (!checked) {
                                                        //Se le pasa null a la key, ya que al ser el último elemento no tiene hijos
                                                        remarcarHijos('UONsPerfilTree', UONsAux[key0].children[key1], null);
                                                        isDeschequeoAscendente = true;
                                                        UONsAux[key0].children[key1].children[key2].checked = false;
                                                        UONsAux[key0].children[key1].checked = false;
                                                        UONsAux[key0].checked = false;
                                                        isDeschequeoAscendente = false;
                                                    }
                                                    return false;
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }

        });

        //al seleccionar o deseleccionar un nodo expandimos la operación a sus nodos hijos (gráficamente)
        $(this).parent().find(':checkbox').prop('checked', checked);
        if (!checked) {
            if (!isDeschequeoAscendente) {
                $(this).parents('li').children(':checkbox').prop('checked', checked);
            }
        }
    });

    $('#btnAceptarQlikUONs').live('click', function () {
        if (oPerfil.AccionesPerfil[accBIRestringirVisibilidadQlikAppsUONs]) {
            hijos($('ul.tree>ul>li>:checkbox'));
            oPerfil.QlikUONS = $.parseJSON(JSON.stringify(QlikUONsAux));
        }
        $('#popupFondo').hide();
        $('#popupQlikUONsPerfil').hide();
    });
    $('#btnCancelarQlikUONs,#btnCerrarQlikUONsPerfil').live('click', function () {
        $('#popupFondo').hide();
        $('#popupQlikUONsPerfil').hide();
    });
    $('#QlikUONsPerfilTree :checkbox').live('click', function () {
        /*Gestionamos las UON para Qlik (BI) del perfil. El problema es gestionar estas UON en la variable json de la pantalla
        ya que hay que navegar a traves del arbol que creamos con las unidades organizativas*/
        var codes = $(this).attr('id').replace('chkQlikUONsPerfilTree-', '').split('-');
        var checked = $(this).attr('checked') == undefined ? false : true;
        $.each(QlikUONsAux, function (key0, value0) {
            if (this.value == codes[0].split('_')[1]) {
                if (codes.length == 1) {
                    QlikUONsAux[key0].checked = checked;
                    chequeoDescendente(QlikUONsAux[key0], checked);
                    return false;
                } else {
                    $.each(this.children, function (key1, value1) {
                        if (this.value == codes[1].split('_')[1]) {
                            if (codes.length == 2) {
                                QlikUONsAux[key0].children[key1].checked = checked;
                                chequeoDescendente(QlikUONsAux[key0].children[key1], checked);
                                if (!checked) {
                                    isDeschequeoAscendente = true;
                                    QlikUONsAux[key0].checked = false;
                                    isDeschequeoAscendente = false;
                                }
                                return false;
                            } else {
                                $.each(this.children, function (key2, value2) {
                                    if (this.value == codes[2].split('_')[1]) {
                                        if (codes.length == 3) {
                                            QlikUONsAux[key0].children[key1].children[key2].checked = checked;
                                            chequeoDescendente(QlikUONsAux[key0].children[key1].children[key2], checked);
                                            if (!checked) {
                                                //Se le pasa el key del elemento que se ha pinchado ya que todos los hijos ya están desmarcados y no hay que volver a comprobarlos
                                                remarcarHijos('QlikUONsPerfilTree', QlikUONsAux[key0].children[key1], key2);
                                                isDeschequeoAscendente = true;
                                                QlikUONsAux[key0].children[key1].checked = false;
                                                QlikUONsAux[key0].checked = false;
                                                isDeschequeoAscendente = false;
                                            }
                                            return false;
                                        } else {
                                            $.each(this.children, function (key3, value3) {
                                                if (this.value == codes[3].split('_')[1]) {
                                                    QlikUONsAux[key0].children[key1].children[key2].children[key3].checked = checked;
                                                    chequeoDescendente(QlikUONsAux[key0].children[key1].children[key2].children[key3], checked);
                                                    if (!checked) {
                                                        //Se le pasa null a la key, ya que al ser el último elemento no tiene hijos
                                                        remarcarHijos('QlikUONsPerfilTree', QlikUONsAux[key0].children[key1], null);
                                                        isDeschequeoAscendente = true;
                                                        QlikUONsAux[key0].children[key1].children[key2].checked = false;
                                                        QlikUONsAux[key0].children[key1].checked = false;
                                                        QlikUONsAux[key0].checked = false;
                                                        isDeschequeoAscendente = false;
                                                    }
                                                    return false;
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }

        });

        //al seleccionar o deseleccionar un nodo expandimos la operación a sus nodos hijos (gráficamente)
        $(this).parent().find(':checkbox').prop('checked', checked);
        if (!checked) {
            if (!isDeschequeoAscendente) {
                $(this).parents('li').children(':checkbox').prop('checked', checked);
            }
        }
    });

    $('#btnAceptarQlikGMNs').live('click', function () {
        if (oPerfil.AccionesPerfil[accBIRestringirVisibilidadQlikAppsGMNs]) {
            hijos($('ul.tree>ul>li>:checkbox'));
            oPerfil.QlikGMNS = $.parseJSON(JSON.stringify(QlikGMNsAux));
        }
        $('#popupFondo').hide();
        $('#popupQlikGMNsPerfil').hide();
    });
    $('#btnCancelarQlikGMNs,#btnCerrarQlikGMNsPerfil').live('click', function () {
        $('#popupFondo').hide();
        $('#popupQlikGMNsPerfil').hide();
    });
    $('#QlikGMNsPerfilTree :checkbox').live('click', function () {
        /*Gestionamos los GMN para Qlik (BI) del perfil. El problema es gestionar estos GMN en la variable json de la pantalla
        ya que hay que navegar a traves del arbol que creamos con lois materiales*/
        var codes = $(this).attr('id').replace('chkQlikGMNsPerfilTree-', '').split('-');
        var checked = $(this).attr('checked') == undefined ? false : true;
        $.each(QlikGMNsAux, function (key0, value0) {
            if (this.value == codes[0].split('_')[1]) {
                if (codes.length == 1) {
                    QlikGMNsAux[key0].checked = checked;
                    chequeoDescendente(QlikGMNsAux[key0], checked);
                    return false;
                } else {
                    $.each(this.children, function (key1, value1) {
                        if (this.value == codes[1].split('_')[1]) {
                            if (codes.length == 2) {
                                QlikGMNsAux[key0].children[key1].checked = checked;
                                chequeoDescendente(QlikGMNsAux[key0].children[key1], checked);
                                if (!checked) {
                                    isDeschequeoAscendente = true;
                                    QlikGMNsAux[key0].checked = false;
                                    isDeschequeoAscendente = false;
                                }
                                return false;
                            } else {
                                $.each(this.children, function (key2, value2) {
                                    if (this.value == codes[2].split('_')[1]) {
                                        if (codes.length == 3) {
                                            QlikGMNsAux[key0].children[key1].children[key2].checked = checked;
                                            chequeoDescendente(QlikGMNsAux[key0].children[key1].children[key2], checked);
                                            if (!checked) {
                                                //Se le pasa el key del elemento que se ha pinchado ya que todos los hijos ya están desmarcados y no hay que volver a comprobarlos
                                                remarcarHijos('QlikGMNsPerfilTree', QlikGMNsAux[key0].children[key1], key2);
                                                isDeschequeoAscendente = true;
                                                QlikGMNsAux[key0].children[key1].checked = false;
                                                QlikGMNsAux[key0].checked = false;
                                                isDeschequeoAscendente = false;
                                            }
                                            return false;
                                        } else {
                                            $.each(this.children, function (key3, value3) {
                                                if (this.value == codes[3].split('_')[1]) {
                                                    if (codes.length == 4) {
                                                        QlikGMNsAux[key0].children[key1].children[key2].children[key3].checked = checked;
                                                        chequeoDescendente(QlikGMNsAux[key0].children[key1].children[key2].children[key3], checked);
                                                        if (!checked) {
                                                            //Se le pasa el key del elemento que se ha pinchado ya que todos los hijos ya están desmarcados y no hay que volver a comprobarlos
                                                            remarcarHijos('QlikGMNsPerfilTree', QlikGMNsAux[key0].children[key1].children[key2], key3);
                                                            isDeschequeoAscendente = true;
                                                            QlikGMNsAux[key0].children[key1].children[key2].checked = false;
                                                            QlikGMNsAux[key0].children[key1].checked = false;
                                                            QlikGMNsAux[key0].checked = false;
                                                            isDeschequeoAscendente = false;
                                                        }
                                                        return false;
                                                    } else {
                                                        $.each(this.children, function (key4, value4) {
                                                            if (this.value == codes[4].split('_')[1]) {
                                                                QlikGMNsAux[key0].children[key1].children[key2].children[key3].children[key4].checked = checked;
                                                                chequeoDescendente(QlikGMNsAux[key0].children[key1].children[key2].children[key3].children[key4], checked);
                                                                if (!checked) {
                                                                    //Se le pasa null a la key, ya que al ser el último elemento no tiene hijos
                                                                    remarcarHijos('QlikGMNsPerfilTree', QlikGMNsAux[key0].children[key1], null);
                                                                    isDeschequeoAscendente = true;
                                                                    QlikGMNsAux[key0].children[key1].children[key2].children[key3].checked = false;
                                                                    QlikGMNsAux[key0].children[key1].children[key2].checked = false;
                                                                    QlikGMNsAux[key0].children[key1].checked = false;
                                                                    QlikGMNsAux[key0].checked = false;
                                                                    isDeschequeoAscendente = false;
                                                                }
                                                                return false;
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }

        });

        //al seleccionar o deseleccionar un nodo expandimos la operación a sus nodos hijos (gráficamente)
        $(this).parent().find(':checkbox').prop('checked', checked);
        if (!checked) {
            if (!isDeschequeoAscendente) {
                $(this).parents('li').children(':checkbox').prop('checked', checked);
            }
        }
    });

    $('#btnAceptarUNQA').live('click', function () {
        if (oPerfil.AccionesPerfil[accQARestringirConsultaCertificadossUnidadesNegocio]) {
            hijos($('ul.tree>ul>li>:checkbox'));
            oPerfil.UNQA = $.parseJSON(JSON.stringify(UNQAAux));
        }
        $('#popupFondo').hide();
        $('#popupUNQAPerfil').hide();
    });
    $('#btnCancelarUNQA,#btnCerrarUNQAPerfil').live('click', function () {
        $('#popupFondo').hide();
        $('#popupUNQAPerfil').hide();
    });

    $('#UNQAPerfilTree :checkbox').live('click', function () {
        /*Gestionamos los UNQA del perfil. El problema es gestionar estos UNQA en la variable json de la pantalla
        ya que hay que navegar a traves del arbol que creamos con lois UNQA*/
        var codes = $(this).attr('id').replace('chkUNQAPerfilTree-', '').split('-');
        var checked = $(this).attr('checked') == undefined ? false : true;
        $.each(UNQAAux, function (key0, value0) {
            if (this.value == codes[0].split('_')[1]) {
                if (codes.length == 1) {
                    UNQAAux[key0].checked = checked;
                    chequeoDescendente(UNQAAux[key0], checked);
                    return false;
                } else {
                    $.each(this.children, function (key1, value1) {
                        if (this.value == codes[1].split('_')[1]) {
                            if (codes.length == 2) {
                                UNQAAux[key0].children[key1].checked = checked;
                                chequeoDescendente(UNQAAux[key0].children[key1], checked);
                                if (!checked) {
                                    isDeschequeoAscendente = true;
                                    UNQAAux[key0].checked = false;
                                    isDeschequeoAscendente = false;
                                }
                                return false;
                            } else {
                                $.each(this.children, function (key2, value2) {
                                    if (this.value == codes[2].split('_')[1]) {
                                        if (codes.length == 3) {
                                            UNQAAux[key0].children[key1].children[key2].checked = checked;
                                            chequeoDescendente(UNQAAux[key0].children[key1].children[key2], checked);
                                            if (!checked) {
                                                //Se le pasa el key del elemento que se ha pinchado ya que todos los hijos ya están desmarcados y no hay que volver a comprobarlos
                                                remarcarHijos('UNQAPerfilTree', UNQAAux[key0].children[key1], key2);
                                                isDeschequeoAscendente = true;
                                                UNQAAux[key0].children[key1].checked = false;
                                                UNQAAux[key0].checked = false;
                                                isDeschequeoAscendente = false;
                                            }
                                            return false;
                                        } else {
                                            $.each(this.children, function (key3, value3) {
                                                if (this.value == codes[3].split('_')[1]) {
                                                    if (codes.length == 4) {
                                                        UNQAAux[key0].children[key1].children[key2].children[key3].checked = checked;
                                                        chequeoDescendente(UNQAAux[key0].children[key1].children[key2].children[key3], checked);
                                                        if (!checked) {
                                                            //Se le pasa el key del elemento que se ha pinchado ya que todos los hijos ya están desmarcados y no hay que volver a comprobarlos
                                                            remarcarHijos('UNQAPerfilTree', UNQAAux[key0].children[key1].children[key2], key3);
                                                            isDeschequeoAscendente = true;
                                                            UNQAAux[key0].children[key1].children[key2].checked = false;
                                                            UNQAAux[key0].children[key1].checked = false;
                                                            UNQAAux[key0].checked = false;
                                                            isDeschequeoAscendente = false;
                                                        }
                                                        return false;
                                                    }
                                                   
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }

        });

        //al seleccionar o deseleccionar un nodo expandimos la operación a sus nodos hijos (gráficamente)
        $(this).parent().find(':checkbox').prop('checked', checked);
        if (!checked) {
            if (!isDeschequeoAscendente) {
                $(this).parents('li').children(':checkbox').prop('checked', checked);
            }
        }
    });



    function chequeoDescendente(nodo, value) {
        /**
        * <summary>chequea o deschequea los descendientes de un nodo de la estructura UON</summary>
        * <param name="nodo">Nodo</param>
        * <param name="value">Estado de check true o false </param>
        */
        if (nodo.children != undefined) {
            for (i in nodo.children) {
                nodo.children[i].checked = value;
                chequeoDescendente(nodo.children[i], value);
            }
        }
    }

    function hijos(nodo) {
        $.each(nodo, function () {
            if (this.checked) {
                chequearNodo(this, true);
            } else {
                hijos(nodo.parent);
            }
        });
    }

    function isNivelCompletoUon(nodo) {
        /**Comprueba si el nivel bajo un nodo de la estructura Uons está completo
        /* <param name="nodo">Nodo a comprobar</parma>
        /* <returns>true o false</returns>*/

        var completo = true;
        for (i in nodo.children) {
            if (!nodo.children[i].checked) {
                completo = false;
            }
        }
        return completo;
    }

    function remarcarHijos(arbol, nodo, key) {
        //Si se descheckea el padre por descheckear uno de los hijos, se marcan los hijos que siguen checkeados graficamente
        if (nodo.children != undefined) {
            for (i in nodo.children) {
                if (key == null || key != i) {
                    //En funcion del parametro arbol, se remarca uno u otro. 
                    switch(arbol) {
                        case 'UONsPerfilTree':
                            nodo.children[i].checked = $("#UONsPerfilTree :checkbox[id$='" + nodo.children[i].nivel + "_" + nodo.children[i].value + "']").prop('checked');
                            break;
                        case 'QlikUONsPerfilTree':
                            nodo.children[i].checked = $("#QlikUONsPerfilTree :checkbox[id$='" + nodo.children[i].nivel + "_" + nodo.children[i].value + "']").prop('checked');
                            break;
                        case 'QlikGMNsPerfilTree':
                            nodo.children[i].checked = $("#QlikGMNsPerfilTree :checkbox[id$='" + nodo.children[i].nivel + "_" + nodo.children[i].value + "']").prop('checked');
                            break;
                        case 'UNQAAuxPerfilTree':
                            nodo.children[i].checked = $("#UNQAAuxPerfilTree :checkbox[id$='" + nodo.children[i].nivel + "_" + nodo.children[i].value + "']").prop('checked');
                            break;
                    }
                    remarcarHijos(arbol, nodo.children[i], null);
                }
            }
        }

    }


    function completarNiveles(nodo) {
        //chequea el nodo padre, si sus descencientes están todos checked. Recursiva.

        if (nodo) {
            var padre = nodo.parents('li').parents('li').eq(0);
            if (isNivelCompleto(padre)) {
                padre.closest('li').find(':checkbox').prop('checked', true);
                completarNiveles(padre.closest('li').find(':checkbox').eq(0));
            }
        }
    }

    function isNivelCompleto(nodo) {
        /*<summary>Comprueba si un nodo (CHECK) tiene todos sus hijos chequeados</summary>
        * <param name="nodo">Nodo (input checkbox)</param>
        */
        var completo = true;
        var hijos = nodo.children().children('li').find(':checkbox');
        hijos.each(function () {
            if (!$(this).is(':checked')) {
                completo = false;
            }
        });
        return completo;
    }

    function MarcarTodasQlikUONS() {
        for (iUON0 = 0; iUON0 < oPerfil.QlikUONS.length; iUON0++) {
            nivelUON0 = oPerfil.QlikUONS[iUON0];
            for (iUON1 = 0; iUON1 < nivelUON0.children.length; iUON1++) {
                nivelUON1 = nivelUON0.children[iUON1];
                nivelUON1.checked = true;
            }
        }
    }

    function DesmarcarTodasQlikUONS() {
        for (iUON0 = 0; iUON0 < oPerfil.QlikUONS.length; iUON0++) {
            nivelUON0 = oPerfil.QlikUONS[iUON0];
            for (iUON1 = 0; iUON1 < nivelUON0.children.length; iUON1++) {
                nivelUON1 = nivelUON0.children[iUON1];
                nivelUON1.checked = false;
                for (iUON2 = 0; iUON2 < nivelUON1.children.length; iUON2++) {
                    nivelUON2 = nivelUON1.children[iUON2];
                    nivelUON2.checked = false;
                    for (iUON3 = 0; iUON3 < nivelUON2.children.length; iUON3++) {
                        nivelUON3 = nivelUON2.children[iUON3];
                        nivelUON3.checked = false;
                    }
                }
            }
        }
    }

    function MarcarTodasQlikGMNS() {
        for (iGMN0 = 0; iGMN0 < oPerfil.QlikGMNS.length; iGMN0++) {
            nivelGMN0 = oPerfil.QlikGMNS[iGMN0];
            for (iGMN1 = 0; iGMN1 < nivelGMN0.children.length; iGMN1++) {
                nivelGMN1 = nivelGMN0.children[iGMN1];
                nivelGMN1.checked = true;
            }
        }
    }

    function DesmarcarTodasQlikGMNS() {
        for (iGMN0 = 0; iGMN0 < oPerfil.QlikGMNS.length; iGMN0++) {
            nivelGMN0 = oPerfil.QlikGMNS[iGMN0];
            for (iGMN1 = 0; iGMN1 < nivelGMN0.children.length; iGMN1++) {
                nivelGMN1 = nivelGMN0.children[iGMN1];
                nivelGMN1.checked = false;
                for (iGMN2 = 0; iGMN2 < nivelGMN1.children.length; iGMN2++) {
                    nivelGMN2 = nivelGMN1.children[iGMN2];
                    nivelGMN2.checked = false;
                    for (iGMN3 = 0; iGMN3 < nivelGMN2.children.length; iGMN3++) {
                        nivelGMN3 = nivelGMN2.children[iGMN3];
                        nivelGMN3.checked = false;
                        for (iGMN4 = 0; iGMN4 < nivelGMN3.children.length; iGMN4++) {
                            nivelGMN4 = nivelGMN3.children[iGMN4];
                            nivelGMN4.checked = false;
                        }
                    }
                }
            }
        }
    }

    function MostrarPopUp() {
        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        $('#popupUONsPerfil').show();
        $('#UONsPerfilTree').css('height', $('#popupUONsPerfil').height() - 25 - $('#divBotonesUONs').outerHeight() - $('#divCabeceraUONsPerfil').outerHeight());
        setTimeout(function () { CentrarPopUp($('#popupUONsPerfil')); }, 5);
    };
    function MostrarPopUpQlikUONs() {
        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        $('#popupQlikUONsPerfil').show();
        $('#QlikUONsPerfilTree').css('height', $('#popupQlikUONsPerfil').height() - 25 - $('#divBotonesQlikUONs').outerHeight() - $('#divCabeceraQlikUONsPerfil').outerHeight());
        setTimeout(function () { CentrarPopUp($('#popupQlikUONsPerfil')); }, 5);
    };
    function MostrarPopUpQlikGMNs() {
        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        $('#popupQlikGMNsPerfil').show();
        $('#QlikGMNsPerfilTree').css('height', $('#popupQlikGMNsPerfil').height() - 25 - $('#divBotonesQlikGMNs').outerHeight() - $('#divCabeceraQlikGMNsPerfil').outerHeight());
        setTimeout(function () { CentrarPopUp($('#popupQlikGMNsPerfil')); }, 5);
    };
    function MostrarPopUpUNQA() {
        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        $('#popupUNQAPerfil').show();
        $('#UNQAPerfilTree').css('height', $('#popupUNQAPerfil').height() - 25 - $('#divBotonesUNQA').outerHeight() - $('#divCabeceraUNQAPerfil').outerHeight());
        setTimeout(function () { CentrarPopUp($('#popupUNQAPerfil')); }, 5);
    };

    $(window).resize(function () {
        if ($('#popupUONsPerfil').is(':visible')) MostrarPopUp();
        if ($('#popupQlikUONsPerfil').is(':visible')) MostrarPopUpQlikUONs();
        if ($('#popupQlikGMNsPerfil').is(':visible')) MostrarPopUpQlikGMNs();
        if ($('#popupUNQAPerfil').is(':visible')) MostrarPopUpUNQA();
    });
});
function hayModificaciones() {
    /*La forma de ver si hay modificaciones es igualar las dos variables json que tenemos activas en pantalla
    parseadas a tipo string, es decir, comparamos dos cadenas de texto, si hay alguna diferencia sera que hay cambios*/
    return JSON.stringify(oPerfil) !== JSON.stringify(oPerfilOriginal);
}
function comprobarDatos() {
    if (oPerfil.Codigo == '') {
        alert(TextosAlerta[3]);
        return false;
    };
    var comprobacionesCorrectas = true;
    if (comprobacionesCorrectas) {
        $.each(oPerfil.Denominaciones, function () {
            if (this == '') {
                comprobacionesCorrectas = false;
                return false;
            }
        });
    };
    if (!comprobacionesCorrectas) {
        alert(TextosAlerta[4]);
        return false;
    };
    $.when($.ajax({
        type: 'POST',
        url: rutaFS + 'SG/DetallePerfil.aspx/Comprobar_CodigoPerfilValido',
        data: JSON.stringify({ IdPerfil: oPerfil.Id, CodPerfil: oPerfil.Codigo }),
        contentType: 'application/json;',
        async: false
    })).done(function (msg) {
        if (!msg.d) {
            alert(TextosAlerta[6]);
            comprobacionesCorrectas = false;
        };
    });
    return comprobacionesCorrectas;        
}
function BuscarOptions(MenuItems, nivel, items) {
    /*Busca las acciones de cada menu para mostrar los check*/
    for (var j = 0; j < MenuItems.length; j++) {
        if (parseInt(MenuItems[j].value) == parseInt(items[nivel].split('_')[1])) {
            if (nivel == items.length - 1) {
                return MenuItems[j].options;
            } else {
                return BuscarOptions(MenuItems[j].children, nivel + 1, items);
            }
            j = MenuItems.length;
        }
    }
}
function AgregarPerfil() {
    if (hayModificaciones())
        if (confirm(TextosAlerta[0])) Guardar();
    window.location.href = rutaFS + 'SG/DetallePerfil.aspx?idPerfil=0' + (desdeGS ? '&desdeGS=1' : '');
}
function GuardarCambios(avisoCambioTodosPerfiles) {  
    oPerfil.Codigo = $('#txtCodigoPerfil').val();
    oPerfil.Denominaciones = $('#txtDenominacion').textBoxMultiIdioma('getItems');
    if (hayModificaciones() && comprobarDatos())
        if (avisoCambioTodosPerfiles) { if (confirm(TextosAlerta[5])) Guardar(false); }
        else Guardar(true);       
}
function Guardar(mostrarBotones) {
    $('body').toggleClass("wait");
    $.when($.ajax({
        type: 'POST',
        url: rutaFS + 'SG/DetallePerfil.aspx/Guardar',
        data: JSON.stringify({ Perfil: oPerfil }),
        contentType: 'application/json;',
        async: true
    })).done(function (msg) {
        if (oPerfil.Id == 0) oPerfil.Id = parseInt(msg.d);
        oPerfilOriginal = $.parseJSON(JSON.stringify(oPerfil));
        if (mostrarBotones) {
            FSNHeader_MostrarBoton('AgregarPerfil');
            FSNHeader_MostrarBoton('CopiarPerfil');
            FSNHeader_MostrarBoton('EliminarPerfil');
            FSNHeader_MostrarBoton('CompararPerfil');
        }
        setTimeout(function () { $('body').toggleClass("wait"); $('body').css('cursor', 'auto'); }, 500);
    });
}
function EliminarPerfil() {
    var params = JSON.stringify({ idPerfil: oPerfil.Id });
    $.when($.ajax({
        type: 'POST',
        url: rutaFS + 'SG/DetallePerfil.aspx/ComprobarPosibleEliminarPerfil',
        data: params,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: true
    })).done(function (msg) {
        var posibleEliminar = msg.d;
        if (posibleEliminar) {
            var codigoPerfil = $('#txtCodigoPerfil').val();
            var denominacionPerfil = $('#txtDenominacion').textBoxMultiIdioma('getItem', $('#txtDenominacion').textBoxMultiIdioma('getDefaultLanguage'));
            var message = TextosAlerta[2].replace('#####', denominacionPerfil);
            message = message.replace('###', codigoPerfil);
            if (confirm(message)) {
                $.when($.ajax({
                    type: 'POST',
                    url: rutaFS + 'SG/DetallePerfil.aspx/DeletePerfil',
                    data: params,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    async: true
                })).done(function () {
                    window.location.href = rutaFS + 'SG/VisorPerfiles.aspx' + (desdeGS ? '?desdeGS=1' : '');
                });
            }
        } else {
            alert(TextosAlerta[1]);
            return false;
        }
    });
}
function CopiarPerfil() {
    oPerfil.Codigo = $('#txtCodigoPerfil').val();
    oPerfil.Denominaciones = $('#txtDenominacion').textBoxMultiIdioma('getItems');
    if (hayModificaciones())
        if (confirm(TextosAlerta[0])) Guardar();
    window.location.href = rutaFS + 'SG/DetallePerfil.aspx?action=copy&idPerfil=' + oPerfil.Id + (desdeGS ? '&desdeGS=1' : '');

}
function CompararPerfil() {
    if (hayModificaciones())
        if (confirm(TextosAlerta[0])) Guardar();
    window.open(rutaFS + 'SG/ComparativaPerfiles.aspx?popup=1&idPerfil=' + oPerfil.Id + (desdeGS ? '&desdeGS=1' : ''), 'Comparativa', '_blank');
}
function OcultarCargando() {
    var modalprog = $find(ModalProgress);
    if (modalprog) modalprog.hide();
}