﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web
Imports System.Text
Imports System.IO
Imports System.Globalization
Imports Irony.Parsing

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<System.Web.Script.Services.ScriptService()> _
<ToolboxItem(False)> _
Public Class CN
    Inherits System.Web.Services.WebService

#Region "Carga inicial"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="tipo"></param>
    ''' <param name="pagina"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function ObtenerMensajesUsuario(ByVal tipo As Integer, ByVal grupo As Integer, ByVal categoria As Integer, _
                                           ByVal megusta As Boolean, ByVal pagina As Integer, ByVal timezoneoffset As Double, _
                                           ByVal historico As Boolean, ByVal discrepancias As Boolean, _
                                           ByVal factura As Integer, ByVal linea As Integer) As List(Of FSNServer.cnMensaje)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocnMensajes As FSNServer.cnMensajes
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")

            ocnMensajes = FSNServer.Get_Object(GetType(FSNServer.cnMensajes))

            Dim LimiteMensajesCargados As String = CType(System.Configuration.ConfigurationManager.AppSettings("LimiteMensajesCargados"), Integer)
            Dim oMensajes As New List(Of FSNServer.cnMensaje)
            With CN_Usuario
                oMensajes = ocnMensajes.ObtenerMensajesUsuario(.Cod, .Idioma, .DateFormat.ShortDatePattern, .NumberFormat, .UON1, .UON2, .UON3, .Dep, _
                                                               .AccesoEP, .AccesoGS, .AccesoPM, .AccesoQA, .AccesoSM, tipo, grupo, categoria, _
                                                               megusta, LimiteMensajesCargados, pagina, timezoneoffset, historico, discrepancias, factura, linea)
            End With

            Return oMensajes
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene las categorÃ­as en las que el usuario tiene mensajes para leer
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Cargar_Categorias_MensajesUsuario() As List(Of FSNServer.cn_fsTreeViewItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocnCategorias As FSNServer.cnCategorias
            ocnCategorias = FSNServer.Get_Object(GetType(FSNServer.cnCategorias))

            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oCategorias As New List(Of FSNServer.cn_fsTreeViewItem)
            With CN_Usuario
                oCategorias = ocnCategorias.Cargar_Categorias_MensajesUsuario(.Cod, .Idioma, .AccesoEP, .AccesoGS, .AccesoPM, .AccesoQA, .AccesoSM)
            End With

            Return oCategorias
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
#Region "Nuevo Mensaje"
    ''' <summary>
    ''' Crea un objeto tipo mensaje para añadirlo a pantalla directamente y seguir despues guardando en base de datos.
    ''' </summary>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    Public Function GenerarMensaje(ByVal tipo As Integer, ByVal mensaje As Object, ByVal categoria As Object, ByVal adjuntos As Object) As FSNServer.cnMensaje
        Try
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oMensajeHTML As New FSNServer.cnMensaje

            With oMensajeHTML
                .IdMensaje = 0
                .FechaAlta = Date.Now
                .EnProceso = "x" & mensaje("id") & "x"
                .Categoria = New FSNServer.cnCategoria
                .InfoUsuario = New FSNServer.cnUsuario
                .MeGusta = New FSNServer.cnMeGusta
                .Categoria.Id = categoria("id")
                .Categoria.Denominacion = categoria("denominacion")
                .InfoUsuario.Cod = CN_Usuario.Cod
                .InfoUsuario.Nombre = CN_Usuario.Nombre
                .InfoUsuario.SituacionUO = CN_Usuario.DepDenominacion
                .InfoUsuario.ImagenUrl = "Thumbnail.ashx?t=0&u=" & CN_Usuario.CNGuidImagenUsu
                .Titulo = mensaje("titulo")
                .Contenido = mensaje("contenido").ToString
                .PermisoEditar = (.InfoUsuario.Cod = CN_Usuario.Cod)
                .Leido = True
                .Oculto = False
                .MensajeHistorico = False
                .Tipo = CType(tipo, Integer)
                If .Tipo = 2 Then
                    .Fecha = DateTime.Parse(mensaje("fecha") & " " & mensaje("hora"), CN_Usuario.DateFormat)
                    .MesCorto = StrConv(MonthName(CType(.Fecha, DateTime).Month, True), VbStrConv.ProperCase)
                    .Dia = CType(.Fecha, DateTime).Day
                    .Hora = CType(.Fecha, DateTime).ToString("t")
                    Dim FechaHora As DateTime
                    Dim TimeZoneOffSet As Double = CType(mensaje("timezoneOffset"), Integer)
                    FechaHora = TimeValue((Math.Abs(TimeZoneOffSet) \ 60) & ":" & Math.Round((((-TimeZoneOffSet / 60) - (-TimeZoneOffSet \ 60)) * 60), 2))
                    .Cuando = CType(.Fecha, DateTime).ToString("dddd, dd MMMM yyyy HH:mm", CultureInfo.CreateSpecificCulture(CN_Usuario.RefCultural())) & _
                        " (GMT" & IIf(TimeZoneOffSet = 0, "", IIf(TimeZoneOffSet > 0, " -", " +") & FechaHora.ToString("hh:mm")) & ")"
                    .Donde = mensaje("donde")
                End If
                .Leido = True
                .MeGusta.MeGusta = False
                Dim adjunto As FSNServer.cnAdjunto
                .Adjuntos = New List(Of FSNServer.cnAdjunto)
                For Each item As Object In adjuntos
                    If Not item.Count = 0 Then
                        adjunto = New FSNServer.cnAdjunto
                        With adjunto
                            .EnProceso = "*" & mensaje("id") & "*"
                            .Nombre = item("nombre")
                            .Size = modUtilidades.Numero(item("size"), CN_Usuario.NumberFormat.NumberDecimalSeparator)
                            .SizeUnit = item("sizeunit")
                            .SizeToString = modUtilidades.FormatNumber(.Size, CN_Usuario.NumberFormat) & " " & .SizeUnit
                            .TipoAdjunto = item("tipoadjunto")
                            .Url = item("url")
                            Select Case .TipoAdjunto
                                Case 0
                                    .Thumbnail_Url = ConfigurationManager.AppSettings("ruta") & "images/link.png"
                                Case 1, 2
                                    .Thumbnail_Url = ConfigurationManager.AppSettings("rutaFS") & "cn/Thumbnail.ashx?f=" & _
                                        Replace(.Url, ConfigurationManager.AppSettings("temp"), "")
                                Case 10
                                    .Thumbnail_Url = ConfigurationManager.AppSettings("ruta") & "images/Attach.png"
                                Case 11
                                    .Thumbnail_Url = ConfigurationManager.AppSettings("ruta") & "images/excelAttach.png"
                                Case 12
                                    .Thumbnail_Url = ConfigurationManager.AppSettings("ruta") & "images/wordAttach.png"
                                Case 13
                                    .Thumbnail_Url = ConfigurationManager.AppSettings("ruta") & "images/pdfAttach.png"
                            End Select
                        End With
                        .Adjuntos.Add(adjunto)
                    End If
                Next
            End With

            Return oMensajeHTML
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Guarda en base de datos el mensaje.
    ''' </summary>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function InsertarMensaje(ByVal tipo As Integer, ByVal mensaje As Object, ByVal categoria As Object, _
                                    ByVal para As Object, ByVal adjuntos As Object, ByVal imagenesCKEditor As Object, _
                                    ByVal factura As Integer, ByVal linea As Integer) As Integer
        Try
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oMensaje As New FSNServer.cnMensaje
            Dim Contenido As String = mensaje("contenido").ToString
            For Each item As Object In imagenesCKEditor
                item("guidID") = Guid.NewGuid.ToString
                Contenido = Replace(Contenido, HttpUtility.HtmlEncode(item("href")), "Thumbnail.ashx?f=" & item("guidID") & "&t=1")
            Next
            With oMensaje
                .IdMensaje = 0
                .FechaAlta = DateAdd(DateInterval.Minute, CType(mensaje("timezoneOffset"), Integer), DateTime.Parse(mensaje("fechaAlta"), CN_Usuario.DateFormat))
                .EnProceso = "x" & mensaje("id") & "x"
                .Categoria = New FSNServer.cnCategoria
                .InfoUsuario = New FSNServer.cnUsuario
                .MeGusta = New FSNServer.cnMeGusta
                .Categoria.Id = categoria("id")
                .Categoria.Denominacion = categoria("denominacion")
                .InfoUsuario.Cod = CN_Usuario.Cod
                .Titulo = mensaje("titulo")
                .Contenido = Contenido
                .Tipo = CType(tipo, Integer)
                If .Tipo = 2 Then
                    .Fecha = DateAdd(DateInterval.Minute, CType(mensaje("timezoneOffset"), Integer), DateTime.Parse(mensaje("fecha") & " " & mensaje("hora"), CN_Usuario.DateFormat))
                    .MesCorto = StrConv(MonthName(CType(.Fecha, DateTime).Month, True), VbStrConv.ProperCase)
                    .Dia = CType(.Fecha, DateTime).Day
                    .Hora = CType(.Fecha, DateTime).ToString("t")
                    Dim FechaHora As DateTime
                    Dim TimeZoneOffSet As Double = CType(mensaje("timezoneOffset"), Integer)
                    FechaHora = TimeValue((Math.Abs(TimeZoneOffSet) \ 60) & ":" & Math.Round((((-TimeZoneOffSet / 60) - (-TimeZoneOffSet \ 60)) * 60), 2))
                    .Cuando = CType(.Fecha, DateTime).ToString("dddd, dd MMMM yyyy HH:mm", CultureInfo.CreateSpecificCulture(CN_Usuario.RefCultural())) & _
                        " (GMT" & IIf(TimeZoneOffSet = 0, "", IIf(TimeZoneOffSet > 0, " -", " +") & FechaHora.ToString("hh:mm")) & ")"
                    .Donde = mensaje("donde")
                End If

                .Para = New FSNServer.cnMensajePara
                ObtenerPara_Mensaje(.Para, para)

                Dim adjunto As FSNServer.cnAdjunto
                .Adjuntos = New List(Of FSNServer.cnAdjunto)
                For Each item As Object In adjuntos
                    If Not item.Count = 0 Then
                        adjunto = New FSNServer.cnAdjunto
                        With adjunto
                            .Nombre = item("nombre")
                            .Size = modUtilidades.Numero(item("size"), CN_Usuario.NumberFormat.NumberDecimalSeparator)
                            .SizeUnit = item("sizeunit")
                            .SizeToString = modUtilidades.FormatNumber(.Size, CN_Usuario.NumberFormat) & " " & .SizeUnit
                            .TipoAdjunto = item("tipoadjunto")
                            .Url = item("url")
                        End With
                        .Adjuntos.Add(adjunto)
                    End If
                Next
            End With

            Dim dtAdjuntos As New DataTable
            dtAdjuntos.Columns.Add("ID", GetType(System.Int64))
            dtAdjuntos.Columns.Add("NOMBRE", GetType(System.String))
            dtAdjuntos.Columns.Add("GUID", GetType(System.String))
            dtAdjuntos.Columns.Add("SIZE", GetType(System.Double))
            dtAdjuntos.Columns.Add("SIZEUNIT", GetType(System.String))
            dtAdjuntos.Columns.Add("TIPOADJUNTO", GetType(System.Int64))
            dtAdjuntos.Columns.Add("URL", GetType(System.String))

            Try
                Dim rAdjunto As DataRow
                If Not oMensaje.Adjuntos.Count = 0 Then
                    For Each iAdjunto As FSNServer.cnAdjunto In oMensaje.Adjuntos
                        rAdjunto = dtAdjuntos.NewRow
                        With rAdjunto
                            .Item("NOMBRE") = iAdjunto.Nombre
                            .Item("GUID") = Guid.NewGuid.ToString & "." & Split(iAdjunto.Nombre, ".")(Split(iAdjunto.Nombre, ".").Length - 1)
                            iAdjunto.Guid = .Item("GUID")
                            .Item("SIZE") = iAdjunto.Size
                            .Item("SIZEUNIT") = iAdjunto.SizeUnit

                            .Item("TIPOADJUNTO") = iAdjunto.TipoAdjunto
                            .Item("URL") = iAdjunto.Url

                        End With
                        dtAdjuntos.Rows.Add(rAdjunto)
                    Next
                End If
                For Each item As Object In imagenesCKEditor
                    rAdjunto = dtAdjuntos.NewRow
                    With rAdjunto
                        .Item("NOMBRE") = ""
                        .Item("GUID") = item("guidID")
                        .Item("SIZE") = 0
                        .Item("SIZEUNIT") = ""
                        .Item("TIPOADJUNTO") = 0
                        .Item("URL") = System.Configuration.ConfigurationManager.AppSettings("temp") & "\" & item("path") & "\" & item("filename")
                    End With
                    dtAdjuntos.Rows.Add(rAdjunto)
                Next
            Catch ex As Exception
                Throw ex
            End Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim IdMensaje As Integer
            If factura = 0 AndAlso linea = 0 Then
                Dim ocnMensajes As FSNServer.cnMensajes
                ocnMensajes = FSNServer.Get_Object(GetType(FSNServer.cnMensajes))
                IdMensaje = ocnMensajes.InsertarMensaje(oMensaje, dtAdjuntos)
            Else
                Dim ocnDiscrepancias As FSNServer.cnDiscrepancias
                ocnDiscrepancias = FSNServer.Get_Object(GetType(FSNServer.cnDiscrepancias))
                IdMensaje = ocnDiscrepancias.InsertarDiscrepancia(oMensaje, factura, linea, dtAdjuntos)
            End If
            Return IdMensaje
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Guarda en base de datos el mensaje actualizado.
    ''' </summary>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Sub ActualizarMensaje(ByVal tipo As Integer, ByVal mensaje As Object, ByVal categoria As Object, ByVal para As Object, _
                    ByVal imagenesCKEditor As Object, ByVal adjuntos As Object)
        Try
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oMensaje As New FSNServer.cnMensaje

            With oMensaje
                .IdMensaje = CType(mensaje("id"), Integer)
                .FechaAlta = DateAdd(DateInterval.Minute, CType(mensaje("timezoneOffset"), Integer), DateTime.Parse(mensaje("fechaAlta"), CN_Usuario.DateFormat))
                .EnProceso = "x" & mensaje("id") & "x"
                .Categoria = New FSNServer.cnCategoria
                .InfoUsuario = New FSNServer.cnUsuario
                .MeGusta = New FSNServer.cnMeGusta
                .Categoria.Id = categoria("id")
                .Categoria.Denominacion = categoria("denominacion")
                .InfoUsuario.Cod = CN_Usuario.Cod
                .Titulo = mensaje("titulo")
                .Contenido = mensaje("contenido").ToString
                .Tipo = CType(tipo, Integer)
                If .Tipo = 2 Then
                    .Fecha = DateAdd(DateInterval.Minute, CType(mensaje("timezoneOffset"), Integer), DateTime.Parse(mensaje("fecha") & " " & mensaje("hora"), CN_Usuario.DateFormat))
                    .MesCorto = StrConv(MonthName(CType(.Fecha, DateTime).Month, True), VbStrConv.ProperCase)
                    .Dia = CType(.Fecha, DateTime).Day
                    .Hora = CType(.Fecha, DateTime).ToString("t")
                    Dim FechaHora As DateTime
                    Dim TimeZoneOffSet As Double = CType(mensaje("timezoneOffset"), Integer)
                    FechaHora = TimeValue((Math.Abs(TimeZoneOffSet) \ 60) & ":" & Math.Round((((-TimeZoneOffSet / 60) - (-TimeZoneOffSet \ 60)) * 60), 2))
                    .Cuando = CType(.Fecha, DateTime).ToString("dddd, dd MMMM yyyy HH:mm", CultureInfo.CreateSpecificCulture(CN_Usuario.RefCultural())) & _
                        " (GMT" & IIf(TimeZoneOffSet = 0, "", IIf(TimeZoneOffSet > 0, " -", " +") & FechaHora.ToString("hh:mm")) & ")"
                    .Donde = mensaje("donde")
                End If


                Dim adjunto As FSNServer.cnAdjunto
                .Adjuntos = New List(Of FSNServer.cnAdjunto)
                For Each item As Object In adjuntos
                    If Not item.Count = 0 Then
                        adjunto = New FSNServer.cnAdjunto
                        With adjunto
                            .Nombre = item("nombre")
                            .Size = modUtilidades.Numero(item("size"), CN_Usuario.NumberFormat.NumberDecimalSeparator)
                            .SizeUnit = item("sizeunit")
                            .SizeToString = modUtilidades.FormatNumber(.Size, CN_Usuario.NumberFormat) & " " & .SizeUnit
                            .TipoAdjunto = item("tipoadjunto")
                            .Url = item("url")
                        End With
                        .Adjuntos.Add(adjunto)
                    End If
                Next
            End With

            Dim dtAdjuntos As New DataTable
            dtAdjuntos.Columns.Add("ID", GetType(System.Int64))
            dtAdjuntos.Columns.Add("NOMBRE", GetType(System.String))
            dtAdjuntos.Columns.Add("GUID", GetType(System.String))
            dtAdjuntos.Columns.Add("SIZE", GetType(System.Double))
            dtAdjuntos.Columns.Add("SIZEUNIT", GetType(System.String))
            dtAdjuntos.Columns.Add("TIPOADJUNTO", GetType(System.Int64))
            dtAdjuntos.Columns.Add("URL", GetType(System.String))
            Try
                Dim rAdjunto As DataRow
                If Not oMensaje.Adjuntos.Count = 0 Then
                    For Each iAdjunto As FSNServer.cnAdjunto In oMensaje.Adjuntos
                        rAdjunto = dtAdjuntos.NewRow
                        With rAdjunto
                            .Item("NOMBRE") = iAdjunto.Nombre
                            .Item("GUID") = Guid.NewGuid.ToString & "." & Split(iAdjunto.Nombre, ".")(Split(iAdjunto.Nombre, ".").Length - 1)
                            iAdjunto.Guid = .Item("GUID")
                            .Item("SIZE") = iAdjunto.Size
                            .Item("SIZEUNIT") = iAdjunto.SizeUnit
                            .Item("TIPOADJUNTO") = iAdjunto.TipoAdjunto
                            .Item("URL") = iAdjunto.Url
                        End With
                        dtAdjuntos.Rows.Add(rAdjunto)
                    Next
                End If
                For Each item As Object In imagenesCKEditor
                    rAdjunto = dtAdjuntos.NewRow
                    With rAdjunto
                        .Item("NOMBRE") = ""
                        .Item("GUID") = item("guidID")
                        .Item("SIZE") = 0
                        .Item("SIZEUNIT") = ""
                        .Item("TIPOADJUNTO") = 0
                        .Item("URL") = System.Configuration.ConfigurationManager.AppSettings("temp") & "\" & item("path") & "\" & item("filename")
                    End With
                    dtAdjuntos.Rows.Add(rAdjunto)
                Next
            Catch ex As Exception
                Throw ex
            End Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocnMensajes As FSNServer.cnMensajes
            ocnMensajes = FSNServer.Get_Object(GetType(FSNServer.cnMensajes))
            ocnMensajes.ActualizarMensaje(oMensaje, dtAdjuntos)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
#Region "UserProfile"
    ''' <summary>
    ''' Guarda la foto en el directorio y, en base de datos, las notificaciones en el Perfil de Usuario.
    ''' </summary>
    ''' <param name="pathFoto">Path de la foto del perfil de usuario</param>
    ''' <param name="userProfile">Lista de notificaciones del perfil de usuario</param>
    ''' <remarks></remarks>
    ''' <revisado>JVS 08/02/2012</revisado>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Sub UpdateUserProfile(ByVal pathFoto As String, ByVal userProfile As Object)
        Try
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim bNotificarResumenActividad As Boolean = IIf(userProfile("notificarResumenActividad") = 1, True, False)
            Dim bNotificarIncluidoGrupo As Boolean = IIf(userProfile("notificarIncluidoGrupo") = 1, True, False)
            Dim bConfiguracionDesocultar As Boolean = IIf(userProfile("configuracionDesocultar") = 1, True, False)

            If pathFoto <> "" Then
                '1.-si hay foto guardamos la foto en bd
                '2.-cambiar las propiedades que tiene el usuario.
                Dim sNombreImagen As String = CreaImagenThubmnail(pathFoto, 80)
                CN_Usuario.CN_Actualizar_Imagen_Usuario(sNombreImagen, CN_Usuario.Cod, CN_Usuario.CNGuidImagenUsu)
                CN_Usuario.CNGuidImagenUsu = CN_Usuario.CNGuidImagenUsu
            End If

            CN_Usuario.CNNotificarResumenActividad = bNotificarResumenActividad
            CN_Usuario.CNNotificarIncluidoGrupo = bNotificarIncluidoGrupo
            CN_Usuario.CNConfiguracionDesocultar = bConfiguracionDesocultar
            CN_Usuario.CN_Update_Opciones_Usuario(CN_Usuario.Cod, bNotificarResumenActividad, bNotificarIncluidoGrupo, bConfiguracionDesocultar)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Crea thumbnail de la foto de Usuario.
    ''' </summary>
    ''' <param name="pathOrigen">Path origen de la foto del perfil de usuario</param>
    ''' <param name="width">Anchura de la foto</param>
    ''' <remarks></remarks>
    ''' <revisado>JVS 14/02/2012</revisado>
    Private Function CreaImagenThubmnail(ByVal pathOrigen As String, ByVal width As Integer) As String
        Try
            ' Declaraciones
            Dim sNuevoGuid As String
            Dim objImage, objThumbnail As System.Drawing.Image
            Dim strUserImagePath, strImageFilename As String
            Dim shtWidth, shtHeight As Short
            Dim sExtension As String
            ' Obtener path de la carpeta de imÃƒÆ’Ã‚Â¡genes en el servidor
            strUserImagePath = System.Configuration.ConfigurationManager.AppSettings("temp") & "\"
            sExtension = Mid(Trim(pathOrigen), (InStrRev(Trim(pathOrigen), ".") + 1))
            ' Recuperar nombre de fichero para redimensionar de la lista de parÃƒÆ’Ã‚Â¡metros
            sNuevoGuid = Guid.NewGuid.ToString
            strImageFilename = strUserImagePath & sNuevoGuid
            ' Recupera el fichero
            objImage = Drawing.Image.FromFile(pathOrigen)
            ' Recuperar ancho de la lista de parÃƒÆ’Ã‚Â¡metros
            If width = Nothing Then
                shtWidth = objImage.Width
            ElseIf width < 1 Then
                shtWidth = 100
            Else
                shtWidth = width
            End If
            ' CÃƒÆ’Ã‚Â¡lculo de una altura proporcional a la anchura
            shtHeight = objImage.Height / (objImage.Width / shtWidth)
            ' Crear thumbnail
            objThumbnail = objImage.GetThumbnailImage(shtWidth, shtHeight, Nothing, System.IntPtr.Zero)
            ' Enviar al cliente
            objThumbnail.Save(strImageFilename, FormatoImagen(sExtension))
            ' Liberar
            objImage.Dispose()
            objThumbnail.Dispose()
            Return strImageFilename
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sExt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function FormatoImagen(ByVal sExt As String) As System.Drawing.Imaging.ImageFormat
        Select Case sExt.ToLower
            Case "bmp"
                Return System.Drawing.Imaging.ImageFormat.MemoryBmp
            Case "emf"
                Return System.Drawing.Imaging.ImageFormat.Emf
            Case "exif"
                Return System.Drawing.Imaging.ImageFormat.Exif
            Case "gif"
                Return System.Drawing.Imaging.ImageFormat.Gif
            Case "ico"
                Return System.Drawing.Imaging.ImageFormat.Icon
            Case "jpg", "jpeg"
                Return System.Drawing.Imaging.ImageFormat.Jpeg
            Case "png"
                Return System.Drawing.Imaging.ImageFormat.Png
            Case "tiff"
                Return System.Drawing.Imaging.ImageFormat.Tiff
            Case "wmf"
                Return System.Drawing.Imaging.ImageFormat.Wmf
            Case Else
                Return System.Drawing.Imaging.ImageFormat.Jpeg
        End Select
    End Function
#End Region
#Region "Categorias"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="MiembrosCategoria"></param>
    ''' <param name="Miembros"></param>
    ''' <remarks></remarks>
    Private Sub ObtenerMiembros_Categoria(ByRef MiembrosCategoria As FSNServer.cnMiembroCateg, ByVal Miembros As Object)
        Try
            Dim row As DataRow
            For Each item As Object In Miembros
                Select Case item.key
                    Case "UON"
                        For Each iUON As Object In item.value
                            If MiembrosCategoria.Para_UON Is Nothing Then
                                MiembrosCategoria.Para_UON = New DataTable

                                MiembrosCategoria.Para_UON.Columns.Add("UON1", GetType(System.String))
                                MiembrosCategoria.Para_UON.Columns.Add("UON2", GetType(System.String))
                                MiembrosCategoria.Para_UON.Columns.Add("UON3", GetType(System.String))
                                MiembrosCategoria.Para_UON.Columns.Add("DEP", GetType(System.String))
                            End If
                            If iUON("UON1") Is Nothing AndAlso iUON("UON2") Is Nothing AndAlso iUON("UON3") Is Nothing AndAlso iUON("DEP") Is Nothing Then
                                MiembrosCategoria.Para_UON_UON0 = True
                            Else
                                row = MiembrosCategoria.Para_UON.NewRow
                                row("UON1") = iUON("UON1")
                                row("UON2") = iUON("UON2")
                                row("UON3") = iUON("UON3")
                                row("DEP") = iUON("DEP")

                                MiembrosCategoria.Para_UON.Rows.Add(row)
                            End If
                        Next
                    Case "GRUPO"
                        For Each iGrupo As Object In item.value
                            If iGrupo("TIPOGRUPO") Is Nothing Then
                                Dim idGrupo As DataRow
                                If MiembrosCategoria.Para_Grupos_Grupos Is Nothing Then
                                    MiembrosCategoria.Para_Grupos_Grupos = New DataTable
                                    MiembrosCategoria.Para_Grupos_Grupos.Columns.Add("GRUPO", GetType(System.Int64))
                                End If
                                idGrupo = MiembrosCategoria.Para_Grupos_Grupos.NewRow
                                idGrupo("GRUPO") = iGrupo("IDGRUPO")

                                MiembrosCategoria.Para_Grupos_Grupos.Rows.Add(idGrupo)
                            Else
                                Select Case iGrupo("TIPOGRUPO")
                                    Case "EP"
                                        MiembrosCategoria.Para_Grupo_EP = True
                                    Case "GS"
                                        MiembrosCategoria.Para_Grupo_GS = True
                                    Case "PM"
                                        MiembrosCategoria.Para_Grupo_PM = True
                                    Case "QA"
                                        MiembrosCategoria.Para_Grupo_QA = True
                                    Case "SM"
                                        MiembrosCategoria.Para_Grupo_SM = True
                                End Select
                            End If
                        Next
                End Select
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_ListaCategorias(ByVal parentValues As Object) As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oCategorias As New List(Of FSNServer.cn_fsItem)
            Dim keyCateg As String = String.Empty

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            For Each item As Object In parentValues
                keyCateg = item.key
            Next
            Dim idCateg As Integer = CType(parentValues(keyCateg), Integer)
            With CN_Usuario
                oCategorias = ocn_Data.Obtener_ListaCategorias(.Cod, .Idioma, .CNRestriccionOrganizarRed, idCateg)
            End With

            Return oCategorias
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_ListaCategorias1() As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oCategorias As New List(Of FSNServer.cn_fsItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oCategorias = ocn_Data.Obtener_ListaCategorias(.Cod, .Idioma, .CNRestriccionOrganizarRed)
            End With

            Return oCategorias
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Carga de los datos de la categoría
    ''' </summary>
    ''' <returns>Objeto de tipo cnCategoria</returns>
    ''' <remarks>Llamado desde: cnCategorias_ui.js  ; Tiempo máximo: 0,1</remarks>
    ''' <revisado>JVS 07/03/2012</revisado>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Load_Datos_Categoria(ByVal iIdCat As Integer) As FSNServer.cnCategoria
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim ocnCategoria As FSNServer.cnCategoria

            ocnCategoria = FSNServer.Get_Object(GetType(FSNServer.cnCategoria))
            ocnCategoria.LoadCategoria(iIdCat, CN_Usuario.Idioma)

            Return ocnCategoria
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Carga los miembros de la red con permiso de publicación en la categoría
    ''' </summary>
    ''' <param name="IdCategoria"></param>
    ''' <remarks>Llamada desde: cnCategorias.aspx; Tiempo máximo:0,2</remarks>
    ''' <revisado>JVS 08/03/2012</revisado>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_Miembros_Categoria(ByVal idCategoria As Nullable(Of Integer)) As List(Of Object)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocnCategoria As FSNServer.cnCategoria
            ocnCategoria = FSNServer.Get_Object(GetType(FSNServer.cnCategoria))

            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oMiembros As New List(Of Object)
            With CN_Usuario
                oMiembros = ocnCategoria.Load_Miembros_Categoria(.Idioma, idCategoria)
            End With

            Return oMiembros
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtener UONs para asignar a la Categoría
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_ListaTodasCategorias() As List(Of FSNServer.cn_fsTreeViewItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocnCategoria As FSNServer.cnCategoria
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oUONs As New List(Of FSNServer.cn_fsTreeViewItem)

            ocnCategoria = FSNServer.Get_Object(GetType(FSNServer.cnCategoria))
            With CN_Usuario
                oUONs = ocnCategoria.Obtener_UONs_Categoria(.Cod, .Idioma)
            End With

            Return oUONs
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtener grupos para asignar a la Categoría
    ''' </summary>
    ''' <returns>List(Of cn_fsItem)</returns>
    ''' <remarks>Llamado desde: cn_Categorias_ui.js  ; Tiempo máximo: 0,3</remarks>
    ''' <revisado>JVS 13/03/2012</revisado>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_GruposCategorias() As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocnCategoria As FSNServer.cnCategoria
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oGruposCategoria As New List(Of FSNServer.cn_fsItem)

            ocnCategoria = FSNServer.Get_Object(GetType(FSNServer.cnCategoria))
            With CN_Usuario
                oGruposCategoria = ocnCategoria.Obtener_Grupos_Categoria(.Cod, .Idioma)
            End With

            Return oGruposCategoria
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Guarda los datos de la categoría.
    ''' </summary>
    ''' <param name="IdCateg">Id de categoría</param>
    ''' <param name="PermisoPublic">Permiso para publicar (privado, público, sin permiso)</param>
    ''' <param name="Despublicado">Despublicado</param>
    ''' <param name="DenomCat">Denominación</param>
    ''' <param name="Miembros">Lista de miembros que pueden asignar mensajes a la categoría</param>
    ''' <remarks></remarks>
    ''' <revisado>JVS 13/02/2012</revisado>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Sub UpdateCategoria(ByVal IdCateg As Integer,
                               ByVal PermisoPublic As PermisoPublicacion,
                               ByVal Despublicado As Boolean,
                               ByVal DenomCat As Object, ByVal Miembros As Object)
        Try
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocnCategoria As FSNServer.cnCategoria
            ocnCategoria = FSNServer.Get_Object(GetType(FSNServer.cnCategoria))

            Dim MiembrosCategorias As FSNServer.cnMiembroCateg = FSNServer.Get_Object(GetType(FSNServer.cnMiembroCateg))

            ObtenerMiembros_Categoria(MiembrosCategorias, Miembros)

            ocnCategoria.Id = IdCateg

            'Denominación multiidioma
            Dim CNDenomCat As FSNServer.cnMultiIdioma
            Dim den As Object

            CNDenomCat = New FSNServer.cnMultiIdioma
            For Each den In DenomCat("CNDenomCat")
                CNDenomCat(den("value")) = den("text")
            Next

            ocnCategoria.Update_Categoria(CN_Usuario.Idioma, PermisoPublic, Despublicado, CNDenomCat, MiembrosCategorias.Para_UON, MiembrosCategorias.Para_Grupos_Grupos)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Inserta los datos de la categoría.
    ''' </summary>
    ''' <param name="IdCategoriaPadre">Id de categoría padre</param>
    ''' <param name="Nivel">Nivel </param>
    ''' <param name="PermisoPublic">Permiso para publicar (privado, público, sin permiso)</param>
    ''' <param name="Despublicado">Despublicado</param>
    ''' <param name="FechaAlta">Fecha de alta</param>
    ''' <param name="DenomCat">Denominación</param>
    ''' <param name="Miembros">Lista de miembros que pueden asignar mensajes a la categoría</param>
    ''' <remarks></remarks>
    ''' <revisado>JVS 14/03/2012</revisado>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Sub InsertarCategoria(ByVal IdCategoriaPadre As Integer,
                                ByVal Nivel As Integer,
                                ByVal PermisoPublic As PermisoPublicacion,
                                ByVal Despublicado As Boolean,
                                ByVal FechaAlta As DateTime,
                                ByVal DenomCat As Object,
                                ByVal Miembros As Object)
        Try
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocnCategoria As FSNServer.cnCategoria
            ocnCategoria = FSNServer.Get_Object(GetType(FSNServer.cnCategoria))

            Dim MiembrosCategorias As FSNServer.cnMiembroCateg = FSNServer.Get_Object(GetType(FSNServer.cnMiembroCateg))

            ObtenerMiembros_Categoria(MiembrosCategorias, Miembros)

            ocnCategoria.CategoriaPadre = IdCategoriaPadre
            ocnCategoria.Nivel = Nivel

            'Denominación multiidioma
            Dim CNDenomCat As FSNServer.cnMultiIdioma
            Dim den As Object

            CNDenomCat = New FSNServer.cnMultiIdioma
            For Each den In DenomCat("CNDenomCat")
                CNDenomCat(den("value")) = den("text")
            Next

            ocnCategoria.Insertar_Categoria(CN_Usuario.Cod, CN_Usuario.Idioma, PermisoPublic, Despublicado, FechaAlta, CNDenomCat, MiembrosCategorias.Para_UON, MiembrosCategorias.Para_Grupos_Grupos)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Borra los datos de la categoría.
    ''' </summary>
    ''' <remarks></remarks>
    ''' <revisado>JVS 13/02/2012</revisado>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Sub DeleteCategoria(ByVal IdCateg As Integer)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocnCategoria As FSNServer.cnCategoria
            ocnCategoria = FSNServer.Get_Object(GetType(FSNServer.cnCategoria))
            ocnCategoria.Id = IdCateg
            ocnCategoria.Delete_Categoria()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Si la categorÃ­a tiene mensajes no se deja eliminar
    ''' </summary>
    ''' <param name="IdCateg">categorÃ­a</param>
    ''' <returns>Si la categorÃ­a tiene mensajes</returns>
    ''' <remarks>Llamado desde: cnCategorias_ui.js  ; Tiempo mÃ¡ximo: 0,1</remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function ComprobarDeleteCategoria(ByVal IdCateg As Integer) As Boolean
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocnCategoria As FSNServer.cnCategoria

            ocnCategoria = FSNServer.Get_Object(GetType(FSNServer.cnCategoria))
            ocnCategoria.Id = IdCateg

            Return ocnCategoria.ComprobarDeleteCategoria()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
#Region "Respuestas"
    ''' <summary>
    ''' Crea un objeto tipo respuesta para añadirlo a pantalla directamente y seguir despues guardando en base de datos.
    ''' </summary>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function GenerarRespuesta(ByVal idMensaje As Integer, ByVal idRespuesta As Integer, ByVal mensaje As Object, ByVal citado As Object, ByVal adjuntos As Object) As FSNServer.cnMensaje
        Try
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oMensajeHTML As New FSNServer.cnMensaje

            With oMensajeHTML
                .Categoria = New FSNServer.cnCategoria
                .InfoUsuario = New FSNServer.cnUsuario
                .MeGusta = New FSNServer.cnMeGusta

                .Respuestas = New List(Of FSNServer.cnRespuesta)
                Dim iRespuesta As New FSNServer.cnRespuesta
                With iRespuesta
                    .IdRespuesta = 0
                    .EnProceso = "xr" & idRespuesta & "rx"
                    .IdMensaje = idMensaje
                    .Contenido = mensaje("contenido")
                    .FechaAlta = Date.Now
                    .InfoUsuario = New FSNServer.cnUsuario
                    .InfoUsuario.Cod = CN_Usuario.Cod
                    .InfoUsuario.Nombre = CN_Usuario.Nombre
                    .InfoUsuario.SituacionUO = CN_Usuario.DepDenominacion
                    .InfoUsuario.ImagenUrl = "Thumbnail.ashx?t=0&u=" & CN_Usuario.CNGuidImagenUsu
                    If Not citado.Count = 0 Then
                        .UsuarioCitado = New FSNServer.cnUsuario
                        .UsuarioCitado.Nombre = citado("nombreUsu")
                        .UsuarioCitado.SituacionUO = citado("situacionUO")
                    End If
                    .IdMensajeCitado = IIf(idRespuesta = 0, Nothing, idRespuesta)
                    .MeGusta = New FSNServer.cnMeGusta
                    .MeGusta.MeGusta = False
                    .Visible = True
                    .Leido = True
                    Dim adjunto As FSNServer.cnAdjunto
                    .Adjuntos = New List(Of FSNServer.cnAdjunto)
                    For Each item As Object In adjuntos
                        If Not item.Count = 0 Then
                            adjunto = New FSNServer.cnAdjunto
                            With adjunto
                                .EnProceso = "*" & idRespuesta & "*"
                                .Nombre = item("nombre")
                                .Size = modUtilidades.Numero(item("size"), CN_Usuario.NumberFormat.NumberDecimalSeparator)
                                .SizeUnit = item("sizeunit")
                                .SizeToString = modUtilidades.FormatNumber(.Size, CN_Usuario.NumberFormat) & " " & .SizeUnit
                                .TipoAdjunto = item("tipoadjunto")
                                .Url = item("url")
                                Select Case .TipoAdjunto
                                    Case 1, 2
                                        .Thumbnail_Url = ConfigurationManager.AppSettings("rutaFS") & "cn/Thumbnail.ashx?f=" & _
                                            Replace(.Url, ConfigurationManager.AppSettings("temp"), "")
                                    Case 10
                                        .Thumbnail_Url = ConfigurationManager.AppSettings("ruta") & "images/Attach.png"
                                    Case 11
                                        .Thumbnail_Url = ConfigurationManager.AppSettings("ruta") & "images/excelAttach.png"
                                    Case 12
                                        .Thumbnail_Url = ConfigurationManager.AppSettings("ruta") & "images/wordAttach.png"
                                    Case 13
                                        .Thumbnail_Url = ConfigurationManager.AppSettings("ruta") & "images/pdfAttach.png"
                                End Select
                            End With
                            .Adjuntos.Add(adjunto)
                        End If
                    Next
                End With
                .Respuestas.Add(iRespuesta)
            End With

            Return oMensajeHTML
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Guarda la respuesta al mensaje en base de datos
    ''' </summary>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function ResponderMensaje(ByVal idMensaje As Integer, ByVal idRespuesta As Integer, ByVal mensaje As Object, _
                                     ByVal citado As Object, ByVal adjuntos As Object, ByVal imagenesCKEditor As Object) As Integer
        Try
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oRespuesta As New FSNServer.cnRespuesta
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocnMensajes As FSNServer.cnMensajes
            ocnMensajes = FSNServer.Get_Object(GetType(FSNServer.cnMensajes))
            Dim Contenido As String = mensaje("contenido").ToString
            For Each item As Object In imagenesCKEditor
                item("guidID") = Guid.NewGuid.ToString
                Contenido = Replace(Contenido, HttpUtility.HtmlEncode(item("href")), "Thumbnail.ashx?f=" & item("guidID") & "&t=1")
            Next

            With oRespuesta
                .IdMensaje = idMensaje
                .FechaAlta = DateAdd(DateInterval.Minute, CType(mensaje("timezoneOffset"), Integer), DateTime.Parse(mensaje("fechaAlta"), CN_Usuario.DateFormat))
                .InfoUsuario = New FSNServer.cnUsuario
                .InfoUsuario.Cod = CN_Usuario.Cod
                .Contenido = Contenido
                .MeGusta = New FSNServer.cnMeGusta
                .MeGusta.MeGusta = False
                .IdMensajeCitado = IIf(idRespuesta = 0, Nothing, idRespuesta)

                Dim adjunto As FSNServer.cnAdjunto
                .Adjuntos = New List(Of FSNServer.cnAdjunto)
                For Each item As Object In adjuntos
                    If Not item.Count = 0 Then
                        adjunto = New FSNServer.cnAdjunto
                        With adjunto
                            .Nombre = item("nombre")
                            .Size = modUtilidades.Numero(item("size"), CN_Usuario.NumberFormat.NumberDecimalSeparator)
                            .SizeUnit = item("sizeunit")
                            .SizeToString = modUtilidades.FormatNumber(.Size, CN_Usuario.NumberFormat) & " " & .SizeUnit
                            .TipoAdjunto = item("tipoadjunto")
                            .Url = item("url")
                        End With
                        .Adjuntos.Add(adjunto)
                    End If
                Next
            End With

            Dim dtAdjuntos As New DataTable
            dtAdjuntos.Columns.Add("ID", GetType(System.Int64))
            dtAdjuntos.Columns.Add("NOMBRE", GetType(System.String))
            dtAdjuntos.Columns.Add("GUID", GetType(System.String))
            dtAdjuntos.Columns.Add("SIZE", GetType(System.Double))
            dtAdjuntos.Columns.Add("SIZEUNIT", GetType(System.String))
            dtAdjuntos.Columns.Add("TIPOADJUNTO", GetType(System.Int64))
            dtAdjuntos.Columns.Add("URL", GetType(System.String))

            Try
                Dim rAdjunto As DataRow
                If Not oRespuesta.Adjuntos.Count = 0 Then
                    For Each iAdjunto As FSNServer.cnAdjunto In oRespuesta.Adjuntos
                        rAdjunto = dtAdjuntos.NewRow
                        With rAdjunto
                            .Item("NOMBRE") = iAdjunto.Nombre
                            .Item("GUID") = Guid.NewGuid.ToString & "." & Split(iAdjunto.Nombre, ".")(Split(iAdjunto.Nombre, ".").Length - 1)
                            iAdjunto.Guid = .Item("GUID")
                            .Item("SIZE") = iAdjunto.Size
                            .Item("SIZEUNIT") = iAdjunto.SizeUnit
                            .Item("TIPOADJUNTO") = iAdjunto.TipoAdjunto
                            .Item("URL") = iAdjunto.Url
                        End With
                        dtAdjuntos.Rows.Add(rAdjunto)
                    Next
                End If
                For Each item As Object In imagenesCKEditor
                    rAdjunto = dtAdjuntos.NewRow
                    With rAdjunto
                        .Item("NOMBRE") = ""
                        .Item("GUID") = item("guidID")
                        .Item("SIZE") = 0
                        .Item("SIZEUNIT") = ""
                        .Item("TIPOADJUNTO") = 0
                        .Item("URL") = System.Configuration.ConfigurationManager.AppSettings("temp") & "\" & item("path") & "\" & item("filename")
                    End With
                    dtAdjuntos.Rows.Add(rAdjunto)
                Next
            Catch ex As Exception
                Throw ex
            End Try
            Dim IdRespuestaNueva As Integer = ocnMensajes.InsertarRespuesta(oRespuesta, dtAdjuntos)
            Return IdRespuestaNueva
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function MeGustaMensaje(ByVal idMensaje As Integer, ByVal idRespuesta As Integer, ByVal timezoneoffset As Double) As FSNServer.cnMensaje
        Try
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")

            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocnMensajes As FSNServer.cnMensajes
            ocnMensajes = FSNServer.Get_Object(GetType(FSNServer.cnMensajes))

            With CN_Usuario
                Return ocnMensajes.InsertarMeGusta(.Cod, .Idioma, .DateFormat.ShortDatePattern, _
                                                   .NumberFormat, idMensaje, idRespuesta, timezoneoffset)
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
#Region "Mensaje Para"
    ''' <summary>
    ''' Obtiene las categorías en las que el usuario tiene permiso de emitir un mensaje
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Categorias_Permiso_EmitirMensajeUsuario(ByVal Consulta As Boolean) As List(Of FSNServer.cn_fsTreeViewItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocnCategorias As FSNServer.cnCategorias
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oCategorias As New List(Of FSNServer.cn_fsTreeViewItem)

            ocnCategorias = FSNServer.Get_Object(GetType(FSNServer.cnCategorias))
            With CN_Usuario
                oCategorias = ocnCategorias.Categorias_Permiso_EmitirMensajeUsuario(.Cod, .Idioma, .UON1, .UON2, .UON3, .Dep, _
                                                               .AccesoEP, .AccesoGS, .AccesoPM, .AccesoQA, .AccesoSM, Consulta)
            End With

            Return oCategorias
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="idCategoria"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_DatosAdicionales_Mensaje(ByVal idMensaje As Nullable(Of Integer), ByVal idCategoria As Nullable(Of Integer)) As List(Of Object)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocnInfo As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oInfo As New List(Of Object)

            ocnInfo = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oInfo = ocnInfo.Obtener_DatosAdicionales_Mensaje(.Cod, .Idioma, idMensaje, idCategoria, _
                    .UON1, .UON2, .UON3, .Dep, .CNRestriccionEnvioUON, .CNRestriccionEnvioDEP, _
                    .EquipoUsu, .CNRestriccionEnvioProveEquipoComprasUsu)
            End With

            Return oInfo
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function UON_Permiso_EmitirMensajeUsuario() As List(Of FSNServer.cn_fsTreeViewItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oUONs As New List(Of FSNServer.cn_fsTreeViewItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oUONs = ocn_Data.Obtener_UONsPara(.Cod, .Idioma, .UON1, .UON2, .UON3, .Dep, .CNRestriccionEnvioUON, .CNRestriccionEnvioDEP)
            End With

            Return oUONs
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function ProveedoresContacto_Permiso_EmitirMensajeUsuario(ByVal Codigo As String, ByVal NIF As String, ByVal Denominacion As String) As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oGMNProcesoCompra As New List(Of FSNServer.cn_fsItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oGMNProcesoCompra = ocn_Data.ProveedoresContacto_Permiso_EmitirMensajeUsuario(.Cod, .EquipoUsu, .CNRestriccionEnvioProveMatUsu, .CNRestriccionEnvioProveEquipoComprasUsu, .CNRestriccionEnvioConContactoQA, _
                                                                        Codigo, NIF, Denominacion)
            End With

            Return oGMNProcesoCompra
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function GruposCorporativos_Permiso_EmitirMensajeUsuario() As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))

            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oGMNProcesoCompra As New List(Of FSNServer.cn_fsItem)
            With CN_Usuario
                oGMNProcesoCompra = ocn_Data.GruposCorporativos_Permiso_EmitirMensajeUsuario(.Cod, .Idioma, .UON1, .UON2, .UON3, .Dep)
            End With

            Return oGMNProcesoCompra
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function EquiposCompra_Permiso_EmitirMensajeUsuario() As List(Of FSNServer.cn_fsTreeViewItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oEquiposCompra As New List(Of FSNServer.cn_fsTreeViewItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oEquiposCompra = ocn_Data.EquiposCompra_Permiso_EmitirMensajeUsuario(.Cod, IIf(.EquipoUsu Is Nothing, "", .EquipoUsu), .CNRestriccion_EstrucCompras_EnvioEquipoComprasUsu)
            End With

            Return oEquiposCompra
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_MaterialesQA_Proveedor() As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oMaterialesQA As New List(Of FSNServer.cn_fsItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oMaterialesQA = ocn_Data.Obtener_MaterialesQA_Proveedor(.Idioma, .Pyme)
            End With

            Return oMaterialesQA
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_MaterialesGS_Proveedor() As List(Of FSNServer.cn_fsTreeViewItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oMaterialesGS As New List(Of FSNServer.cn_fsTreeViewItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oMaterialesGS = ocn_Data.Obtener_MaterialesGS_Proveedor(.Cod, .Idioma, .Pyme, .CNRestriccionEnvioProveMatUsu)
            End With

            Return oMaterialesGS
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
#Region "Buscador entidades"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_ProceCompras_Anyos() As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oAniosProcesoCompra As New List(Of FSNServer.cn_fsItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oAniosProcesoCompra = ocn_Data.Obtener_ProceCompras_Anyos(.Cod, .UON1, .UON2, .UON3, .Dep, .EquipoUsu, _
                    .GSRestriccion_ProceCompra_MaterialUsu, .GSRestriccion_ProceCompra_AsignadoUsuario, .GSRestriccion_ProceCompra_AsignadoEquipo, _
                                    .GSRestriccion_ProceCompra_DepartamentoUsuario, .GSRestriccion_ProceCompra_UnidadOrganizativaUsuario, _
                                    .GSRestriccion_ProceCompra_AbiertoPorUsuario, .GSRestriccion_ProceCompra_ResponsableUsuario, _
                                    .GSRestriccion_ProceCompra_UONsPerfilUsuario)
            End With

            Return oAniosProcesoCompra
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>Devuelve los diferentes códigos GMN1 de los procesos de compra que puede consultar el usuario</summary>
    ''' <param name="parentValues">parentValues</param>
    ''' <returns>Lista de cn_fsItem con los datos</returns>
    ''' <revision>LTG 26/08/2013</revision>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_ProceCompras_GMNs(ByVal parentValues As Object) As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim parentNames As New List(Of FSNServer.cn_fsItem)
            Dim parent As FSNServer.cn_fsItem

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            For Each item As Object In parentValues
                parent = New FSNServer.cn_fsItem
                parent.text = item.key
                parentNames.Add(parent)
            Next
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oGMNProcesoCompra As New List(Of FSNServer.cn_fsItem)
            With CN_Usuario
                oGMNProcesoCompra = ocn_Data.Obtener_ProceCompras_GMNs(.Cod, .Idioma, .UON1, .UON2, .UON3, .Dep, .EquipoUsu, _
                    .GSRestriccion_ProceCompra_MaterialUsu, .GSRestriccion_ProceCompra_AsignadoUsuario, .GSRestriccion_ProceCompra_AsignadoEquipo, _
                                    .GSRestriccion_ProceCompra_DepartamentoUsuario, .GSRestriccion_ProceCompra_UnidadOrganizativaUsuario, _
                                    .GSRestriccion_ProceCompra_AbiertoPorUsuario, .GSRestriccion_ProceCompra_ResponsableUsuario, _
                                    .GSRestriccion_ProceCompra_UONsPerfilUsuario, CType(parentValues(parentNames(0).text), Integer))
            End With

            Return oGMNProcesoCompra
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_ProceCompras_CodDen(ByVal parentValues As Object) As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim parentNames As New List(Of FSNServer.cn_fsItem)
            Dim parent As FSNServer.cn_fsItem

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            For Each item As Object In parentValues
                parent = New FSNServer.cn_fsItem
                parent.text = item.key
                parentNames.Add(parent)
            Next
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oGMNProcesoCompra As New List(Of FSNServer.cn_fsItem)
            With CN_Usuario
                oGMNProcesoCompra = ocn_Data.Obtener_ProceCompras_CodDen(.Cod, .UON1, .UON2, .UON3, .Dep, .EquipoUsu, _
                    .GSRestriccion_ProceCompra_MaterialUsu, .GSRestriccion_ProceCompra_AsignadoUsuario, .GSRestriccion_ProceCompra_AsignadoEquipo, _
                                    .GSRestriccion_ProceCompra_DepartamentoUsuario, .GSRestriccion_ProceCompra_UnidadOrganizativaUsuario, _
                                    .GSRestriccion_ProceCompra_AbiertoPorUsuario, .GSRestriccion_ProceCompra_ResponsableUsuario, .GSRestriccion_ProceCompra_UONsPerfilUsuario, _
                                    CType(parentValues(parentNames(0).text), Integer), parentValues(parentNames(1).text))
            End With

            Return oGMNProcesoCompra
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_BuscadorNoConf_Tipos() As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oTiposNoConformidad As New List(Of FSNServer.cn_fsItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oTiposNoConformidad = ocn_Data.Obtener_BuscadorEntidad_Tipos(.Cod, .Idioma, _
                                            TiposDeDatos.TipoDeSolicitud.NoConformidad)
            End With

            Return oTiposNoConformidad
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_BuscadorNoConf_Proveedores() As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oProveedoresNoConformidad As New List(Of FSNServer.cn_fsItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oProveedoresNoConformidad = ocn_Data.Obtener_BuscadorEntidad_Proveedores(.Cod, _
                    RestricProvMat:=.QARestProvMaterial, RestricProvEqp:=.QARestProvEquipo, RestricProvCon:=.QARestProvContacto, _
                    TipoEntidad:=TiposDeDatos.TipoDeSolicitud.NoConformidad)
            End With

            Return oProveedoresNoConformidad
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_BuscadorNoConf_Identificador(ByVal parentValues As Object) As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim parentNames As New List(Of FSNServer.cn_fsItem)
            Dim parent As FSNServer.cn_fsItem

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            For Each item As Object In parentValues
                parent = New FSNServer.cn_fsItem
                parent.text = item.key
                parentNames.Add(parent)
            Next
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oIdentificadoresNoConformidad As New List(Of FSNServer.cn_fsItem)
            Dim TipoNoConformidad As Integer
            If parentValues(parentNames(0).text) = String.Empty Then
                TipoNoConformidad = 0
            Else
                TipoNoConformidad = CType(parentValues(parentNames(0).text), Integer)
            End If
            With CN_Usuario
                oIdentificadoresNoConformidad = ocn_Data.Obtener_BuscadorEntidad_Identificador(.Cod, .Idioma, _
                    TiposDeDatos.TipoDeSolicitud.NoConformidad, TipoNoConformidad, parentValues(parentNames(1).text), _
                    .QARestCertifUsu, .QARestCertifUO, .QARestCertifDep, .QARestProvMaterial, .QARestProvEquipo, .QARestProvContacto)
            End With

            Return oIdentificadoresNoConformidad
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_BuscadorCert_Tipos() As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oTiposCertificado As New List(Of FSNServer.cn_fsItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oTiposCertificado = ocn_Data.Obtener_BuscadorEntidad_Tipos(.Cod, .Idioma, _
                                            TiposDeDatos.TipoDeSolicitud.Certificado)
            End With

            Return oTiposCertificado
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_BuscadorCert_Proveedores() As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oProveedoresCertificado As New List(Of FSNServer.cn_fsItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oProveedoresCertificado = ocn_Data.Obtener_BuscadorEntidad_Proveedores(.Cod, _
                    RestricProvMat:=.QARestProvMaterial, RestricProvEqp:=.QARestProvEquipo, RestricProvCon:=.QARestProvContacto, _
                    TipoEntidad:=TiposDeDatos.TipoDeSolicitud.Certificado)
            End With

            Return oProveedoresCertificado
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_BuscadorCert_Identificador(ByVal parentValues As Object) As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim parentNames As New List(Of FSNServer.cn_fsItem)
            Dim parent As FSNServer.cn_fsItem

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            For Each item As Object In parentValues
                parent = New FSNServer.cn_fsItem
                parent.text = item.key
                parentNames.Add(parent)
            Next
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oIdentificadoresCertificado As New List(Of FSNServer.cn_fsItem)
            Dim TipoCertificado As Integer
            If parentValues(parentNames(0).text) = String.Empty Then
                TipoCertificado = 0
            Else
                TipoCertificado = CType(parentValues(parentNames(0).text), Integer)
            End If
            With CN_Usuario
                oIdentificadoresCertificado = ocn_Data.Obtener_BuscadorEntidad_Identificador(.Cod, .Idioma, _
                        TiposDeDatos.TipoDeSolicitud.Certificado, TipoCertificado, parentValues(parentNames(1).text), _
                        .QARestCertifUsu, .QARestCertifUO, .QARestCertifDep, .QARestProvMaterial, .QARestProvEquipo, .QARestProvContacto)
            End With

            Return oIdentificadoresCertificado
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_BuscadorFichaCalidad_Proveedores() As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oIdentificadoresCertificado As New List(Of FSNServer.cn_fsItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oIdentificadoresCertificado = ocn_Data.Obtener_BuscadorEntidad_Proveedores(.Cod, _
                    RestricProvMat:=.QARestProvMaterial, RestricProvEqp:=.QARestProvEquipo, RestricProvCon:=.QARestProvContacto)
            End With

            Return oIdentificadoresCertificado
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Comprobar_Buscador_Identificador(ByVal TipoEntidad As Integer, ByVal Identificador As Integer) As FSNServer.cn_fsItem
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oEntidad As FSNServer.cn_fsItem

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oEntidad = ocn_Data.Comprobar_BuscadorEntidad_Identificador(.Cod, .Idioma, _
                                            TipoEntidad, Identificador, .QARestCertifUsu, .QARestCertifUO, .QARestCertifDep, _
                                            .QARestProvMaterial, .QARestProvEquipo, .QARestProvContacto)
            End With

            Return oEntidad
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Comprobar_Buscador_Proveedor(ByVal CodProve As String) As FSNServer.cn_fsItem
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oProveedor As FSNServer.cn_fsItem

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oProveedor = ocn_Data.Comprobar_Buscador_Proveedor(.Cod, CodProve:=CodProve, _
                    RestricProvMat:=.QARestProvMaterial, RestricProvEqp:=.QARestProvEquipo, RestricProvCon:=.QARestProvContacto)
            End With

            Return oProveedor
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Comprobar_Buscador_ProceCompra(ByVal Anio As Integer, ByVal GMN1 As String, ByVal Cod As Integer) As FSNServer.cn_fsItem
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oProcesoCompra As New List(Of FSNServer.cn_fsItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oProcesoCompra = ocn_Data.Obtener_ProceCompras_CodDen(.Cod, .UON1, .UON2, .UON3, .Dep, .EquipoUsu, _
                    .GSRestriccion_ProceCompra_MaterialUsu, .GSRestriccion_ProceCompra_AsignadoUsuario, .GSRestriccion_ProceCompra_AsignadoEquipo, _
                                    .GSRestriccion_ProceCompra_DepartamentoUsuario, .GSRestriccion_ProceCompra_UnidadOrganizativaUsuario, _
                                    .GSRestriccion_ProceCompra_AbiertoPorUsuario, .GSRestriccion_ProceCompra_ResponsableUsuario, .GSRestriccion_ProceCompra_UONsPerfilUsuario, _
                                    Anio, GMN1, Cod)
            End With

            If oProcesoCompra.Count = 0 Then
                Return Nothing
            Else
                With oProcesoCompra.Item(0)
                    .value = Anio & "#" & GMN1 & "#" & Cod
                    .text = Anio & "/" & GMN1 & "/" & .text
                End With
                Return oProcesoCompra.Item(0)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_BuscadorSolicitudes_Tipos() As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oTiposSolicitud As New List(Of FSNServer.cn_fsItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oTiposSolicitud = ocn_Data.Obtener_BuscadorSolicitudes_Tipos(.Cod, .Idioma)
            End With

            Return oTiposSolicitud
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_BuscadorSolicitudes_Identificador(ByVal parentValues As Object) As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim parentNames As New List(Of FSNServer.cn_fsItem)
            Dim parent As FSNServer.cn_fsItem

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            For Each item As Object In parentValues
                parent = New FSNServer.cn_fsItem
                parent.text = item.key
                parentNames.Add(parent)
            Next
            Dim TipoSolicitud As Integer
            If parentValues(parentNames(0).text) = "" Then
                TipoSolicitud = 0
            Else
                TipoSolicitud = CType(parentValues(parentNames(0).text), Integer)
            End If
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oIdentificadoresSolicitud As New List(Of FSNServer.cn_fsItem)
            With CN_Usuario
                oIdentificadoresSolicitud = ocn_Data.Obtener_BuscadorSolicitudes_Identificador(.Cod, .Idioma, TipoSolicitud)
            End With

            Return oIdentificadoresSolicitud
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_BuscadorContrato_Tipos() As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oTiposContrato As New List(Of FSNServer.cn_fsItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oTiposContrato = ocn_Data.Obtener_BuscadorContrato_Tipos(.Cod, .Idioma)
            End With

            Return oTiposContrato
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_BuscadorContrato_Proveedor(ByVal parentValues As Object) As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim parentNames As New List(Of FSNServer.cn_fsItem)
            Dim parent As FSNServer.cn_fsItem

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            For Each item As Object In parentValues
                parent = New FSNServer.cn_fsItem
                parent.text = item.key
                parentNames.Add(parent)
            Next
            Dim TipoSolicitud As Integer
            If parentValues(parentNames(0).text) = "" Then
                TipoSolicitud = 0
            Else
                TipoSolicitud = CType(parentValues(parentNames(0).text), Integer)
            End If
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oProveedoresContrato As New List(Of FSNServer.cn_fsItem)
            With CN_Usuario
                oProveedoresContrato = ocn_Data.Obtener_BuscadorContrato_Proveedor(.Cod, .Idioma, TipoSolicitud)
            End With

            Return oProveedoresContrato
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_BuscadorContrato_Identificador(ByVal parentValues As Object) As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim parentNames As New List(Of FSNServer.cn_fsItem)
            Dim parent As FSNServer.cn_fsItem

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            For Each item As Object In parentValues
                parent = New FSNServer.cn_fsItem
                parent.text = item.key
                parentNames.Add(parent)
            Next
            Dim TipoSolicitud As Integer
            If parentValues(parentNames(0).text) = "" Then
                TipoSolicitud = 0
            Else
                TipoSolicitud = CType(parentValues(parentNames(0).text), Integer)
            End If
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oIdentificadoresSolicitudCompra As New List(Of FSNServer.cn_fsItem)
            With CN_Usuario
                oIdentificadoresSolicitudCompra = ocn_Data.Obtener_BuscadorContrato_Identificador(.Cod, .Idioma, _
                                                                                TipoSolicitud, parentValues(parentNames(1).text))
            End With

            Return oIdentificadoresSolicitudCompra
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_Pedidos_Anyos() As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oAniosPedidos As New List(Of FSNServer.cn_fsItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oAniosPedidos = ocn_Data.Obtener_Pedidos(.CodPersona, .PermisoVerPedidosCCImputables, 1)
            End With

            Return oAniosPedidos
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="parentValues"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_Pedidos_Cestas(ByVal parentValues As Object) As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim parentNames As New List(Of FSNServer.cn_fsItem)
            Dim parent As FSNServer.cn_fsItem

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            For Each item As Object In parentValues
                parent = New FSNServer.cn_fsItem
                parent.text = item.key
                parentNames.Add(parent)
            Next

            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oAniosPedidos As New List(Of FSNServer.cn_fsItem)
            With CN_Usuario
                oAniosPedidos = ocn_Data.Obtener_Pedidos(.CodPersona, .PermisoVerPedidosCCImputables, 2, _
                                                         CType(parentValues(parentNames(0).text), Integer))
            End With

            Return oAniosPedidos
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="parentValues"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_Pedidos_Pedidos(ByVal parentValues As Object) As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim parentNames As New List(Of FSNServer.cn_fsItem)
            Dim parent As FSNServer.cn_fsItem

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            For Each item As Object In parentValues
                parent = New FSNServer.cn_fsItem
                parent.text = item.key
                parentNames.Add(parent)
            Next

            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oAniosPedidos As New List(Of FSNServer.cn_fsItem)
            With CN_Usuario
                oAniosPedidos = ocn_Data.Obtener_Pedidos(.CodPersona, .PermisoVerPedidosCCImputables, 3, _
                                                         CType(parentValues(parentNames(0).text), Integer), _
                                                         CType(parentValues(parentNames(1).text), Integer))
            End With

            Return oAniosPedidos
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Anio"></param>
    ''' <param name="NumPedido"></param>
    ''' <param name="NumOrden"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Comprobar_Buscador_Pedido(ByVal Anio As Integer, ByVal NumPedido As Integer, ByVal NumOrden As Integer) As FSNServer.cn_fsItem
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oPedido As New List(Of FSNServer.cn_fsItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oPedido = ocn_Data.Obtener_Pedidos(.CodPersona, .PermisoVerPedidosCCImputables, 0, Anio, NumPedido, NumOrden)
            End With

            If oPedido.Count = 1 Then
                With oPedido(0)
                    .value = Anio & "#" & NumPedido & "#" & NumOrden
                    .text = Anio & "/" & NumPedido & "/" & NumOrden
                End With
                Return oPedido(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
#Region "Comprobar Permisos"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Anyo"></param>
    ''' <param name="GMN1"></param>
    ''' <param name="Proce"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Comprobar_PermisoConsulta_ProceCompra(ByVal Anyo As Integer, ByVal GMN1 As String, ByVal Proce As Integer) As String
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oProcesoCompra As New List(Of FSNServer.cn_fsItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oProcesoCompra = ocn_Data.Obtener_ProceCompras_CodDen(.Cod, .UON1, .UON2, .UON3, .Dep, .EquipoUsu, _
                    .GSRestriccion_ProceCompra_MaterialUsu, .GSRestriccion_ProceCompra_AsignadoUsuario, .GSRestriccion_ProceCompra_AsignadoEquipo, _
                                    .GSRestriccion_ProceCompra_DepartamentoUsuario, .GSRestriccion_ProceCompra_UnidadOrganizativaUsuario, _
                                    .GSRestriccion_ProceCompra_AbiertoPorUsuario, .GSRestriccion_ProceCompra_ResponsableUsuario, .GSRestriccion_ProceCompra_UONsPerfilUsuario, _
                                    Anyo, GMN1, Proce)
            End With

            If oProcesoCompra.Count = 0 Then
                Return Nothing
            Else
                Return ConfigurationManager.AppSettings("rutaPM") & "seguimiento/detalleproceso.aspx?cn=1&Anyo=" & Anyo & "&GMN1=" & GMN1 & "&Proce=" & Proce
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="TipoEntidad"></param>
    ''' <param name="Identificador"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Comprobar_PermisoConsulta_Identificador(ByVal TipoEntidad As Integer, ByVal Identificador As Integer) As String
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oEntidad As FSNServer.cn_fsItem

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oEntidad = ocn_Data.Comprobar_BuscadorEntidad_Identificador(.Cod, .Idioma, _
                                            TipoEntidad, Identificador, .QARestCertifUsu, .QARestCertifUO, .QARestCertifDep, _
                                            .QARestProvMaterial, .QARestProvEquipo, .QARestProvContacto, True)
            End With

            If Not CType(oEntidad.value, Integer) = 0 Then
                Select Case TipoEntidad
                    Case 1 'Solicitud de compra
                        Return ConfigurationManager.AppSettings("rutaQA") & "frames.aspx?pagina=" &
                            HttpContext.Current.Server.UrlEncode("workflow/comprobaraprob.aspx?Instancia=" &
                            CType(oEntidad.value, Integer)) & "*cn=1*volver=" &
                            ConfigurationManager.AppSettings("rutaFS") & "CN/cn_MuroUsuario.aspx?[###]"
                    Case 2 'Certificado
                        Return ConfigurationManager.AppSettings("rutaQA") & "frames.aspx?pagina=" &
                            HttpContext.Current.Server.UrlEncode("certificados/detalleCertificadoPMWEB.aspx?Certificado=" &
                            CType(oEntidad.value, Integer)) & "*cn=1"
                    Case 3 'No conformidad
                        Return ConfigurationManager.AppSettings("rutaQA") & "frames.aspx?pagina=" &
                            HttpContext.Current.Server.UrlEncode("noconformidad/DetalleNoConformidad.aspx?NoConformidad=" &
                            CType(oEntidad.value, Integer)) & "*cn=1*otz=[##]*volver=" &
                            ConfigurationManager.AppSettings("rutaFS") & "CN/cn_MuroUsuario.aspx?[###]"
                    Case 5 'Contrato
                        Return ConfigurationManager.AppSettings("rutaQA") & "frames.aspx?pagina=" &
                            HttpContext.Current.Server.UrlEncode("contratos/ComprobarAprobContratos.aspx?Instancia=" &
                            CType(oEntidad.value, Integer)) & "*cn=1*volver=" &
                            ConfigurationManager.AppSettings("rutaFS") & "CN/cn_MuroUsuario.aspx?[###]"
                    Case Else
                        Return Nothing
                End Select
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Comprobar_PermisoConsulta_FichaCalidad(ByVal ProveCod As String) As String
        Try
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            If Not CN_Usuario.QAPuntuacionProveedores Then Return Nothing
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oProveedor As New List(Of FSNServer.cn_fsItem)
            Dim ocn_Data As FSNServer.cn_Data

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oProveedor = ocn_Data.Obtener_BuscadorEntidad_Proveedores(.Cod, ProveCod:=ProveCod, _
                    RestricProvMat:=.QARestProvMaterial, RestricProvEqp:=.QARestProvEquipo, RestricProvCon:=.QARestProvContacto)
            End With

            If oProveedor.Item(0).value = ProveCod Then
                Return ConfigurationManager.AppSettings("rutaFS") & "QA/Puntuacion/fichaProveedor.aspx?PROVECOD=" & ProveCod & "&cn=1"
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Anio"></param>
    ''' <param name="NumPedido"></param>
    ''' <param name="NumOrden"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Comprobar_PermisoConsulta_Pedido(ByVal Anio As Integer, ByVal NumPedido As Integer, ByVal NumOrden As Integer) As String
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oPedido As New List(Of FSNServer.cn_fsItem)

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                oPedido = ocn_Data.Obtener_Pedidos(.CodPersona, .PermisoVerPedidosCCImputables, 0, Anio, NumPedido, NumOrden)
            End With

            If oPedido.Count = 1 Then
                If oPedido.Item(0).value = Anio Then
                    Return ConfigurationManager.AppSettings("rutaEP") &
                        "Seguimiento.aspx?Anyo=" & Anio & "&NumPedido=" & NumPedido & "&NumOrden=" & NumOrden & "&cn=1"
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Comprobar_PermisoConsulta_Factura(ByVal IdFactura As Integer) As String
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim ocn_Data As FSNServer.cn_Data

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            Dim oFact As Dictionary(Of String, Integer) = ocn_Data.Comprobar_PermisoConsulta_Factura(CN_Usuario.Cod, CN_Usuario.Idioma, IdFactura)
            If oFact.Count = 0 Then
                Return Nothing
            Else
                Return ConfigurationManager.AppSettings("rutaFS") & _
                    "PM/Facturas/DetalleFactura.aspx?ID=" & oFact("idFact") & "&gestor=" & oFact("gestorFact") & "&receptor=" & oFact("receptorFact")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
#Region "Destinatarios"
    ''' <summary>
    ''' Guarda en base de datos el mensaje.
    ''' </summary>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Sub Modificar_Destinatarios_Mensaje(ByVal idMensaje As Integer, ByVal para As Object)
        Try
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oMensaje As New FSNServer.cnMensaje
            With oMensaje
                .IdMensaje = idMensaje
                .InfoUsuario = New FSNServer.cnUsuario
                .InfoUsuario.Cod = CN_Usuario.Cod
                .Para = New FSNServer.cnMensajePara
                ObtenerPara_Mensaje(.Para, para)
            End With

            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocnMensajes As FSNServer.cnMensajes
            ocnMensajes = FSNServer.Get_Object(GetType(FSNServer.cnMensajes))

            ocnMensajes.ModificarDestinatarios(oMensaje)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
#Region "Notificaciones"
    ''' <summary>
    ''' Comprueba los mensajes nuevos del usuario conectado
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Comprobar_Notificaciones() As List(Of FSNServer.cn_fsItem)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim ocnMensajes As FSNServer.cnMensajes
            Dim oContadores As New List(Of FSNServer.cn_fsItem)

            ocnMensajes = FSNServer.Get_Object(GetType(FSNServer.cnMensajes))
            With CN_Usuario
                oContadores = ocnMensajes.Comprobar_Notificaciones(.Cod)
            End With
            Return oContadores
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene el mensaje urgente que se debe mostrar en el pop up
    ''' </summary>
    ''' <param name="idMensaje">El id del mensaje que se quiere recuperar</param>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_Mensaje(ByVal idMensaje As Integer, ByVal timezoneoffset As Double) As FSNServer.cnMensaje
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim ocnMensajes As FSNServer.cnMensajes
            Dim oMensaje As New FSNServer.cnMensaje

            ocnMensajes = FSNServer.Get_Object(GetType(FSNServer.cnMensajes))
            With CN_Usuario
                oMensaje = ocnMensajes.Obtener_Mensaje(.Cod, .Idioma, .DateFormat.ShortDatePattern, _
                                                       .NumberFormat, idMensaje, timezoneoffset)
            End With
            Return oMensaje
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="idMensaje"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function MensajeUrgente_Leido(ByVal idMensaje As Integer, ByVal timezoneoffset As Double) As FSNServer.cnMensaje
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim ocnMensajes As FSNServer.cnMensajes
            Dim oMensaje As New FSNServer.cnMensaje

            ocnMensajes = FSNServer.Get_Object(GetType(FSNServer.cnMensajes))
            With CN_Usuario
                oMensaje = ocnMensajes.MensajeUrgente_Leido(.Cod, .Idioma, .DateFormat.ShortDatePattern, _
                                                            .NumberFormat, idMensaje, timezoneoffset)
            End With
            Return oMensaje
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
#Region "Utiles"
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function FechaEvento_Completa(ByVal anio As Integer, ByVal mes As Integer, ByVal dia As Integer, _
                                         ByVal hora As Integer, ByVal minuto As Integer, ByVal gmt As Integer) As String
        Try
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim FechaEvento As New Date(anio, mes, dia, hora, minuto, 0)
            Dim Fecha As DateTime
            Fecha = TimeValue(-(gmt \ 60) & ":" & Math.Round((((-gmt / 60) - (-gmt \ 60)) * 60), 2))

            Return FechaEvento.ToString("dddd, dd MMMM yyyy HH:mm", CultureInfo.CreateSpecificCulture(CN_Usuario.RefCultural)) & _
                " (GMT" & IIf(gmt = 0, "", IIf(gmt > 0, " -", " +") & Fecha.ToString("hh:mm")) & ")"
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function NombreMesAbr(ByVal mes As Integer) As String
        Try
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")

            Return StrConv(MonthName(mes, True), VbStrConv.ProperCase)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
#Region "Otras funciones"
    ''' <summary>
    ''' Oculta o muestra el mensaje seleccionado
    ''' </summary>
    ''' <param name="idMensaje"></param>
    ''' <param name="ocultar"></param>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Sub OcultarMostrarMensaje(ByVal idMensaje As Integer, ByVal ocultar As Boolean)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim ocnMensajes As FSNServer.cnMensajes

            ocnMensajes = FSNServer.Get_Object(GetType(FSNServer.cnMensajes))
            With CN_Usuario
                ocnMensajes.OcultarMostrarMensaje(.Cod, idMensaje, ocultar)
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="ParaMensaje"></param>
    ''' <param name="para"></param>
    ''' <remarks></remarks>
    Private Sub ObtenerPara_Mensaje(ByRef ParaMensaje As FSNServer.cnMensajePara, ByVal para As Object)
        Dim row As DataRow
        For Each item As Object In para
            Select Case item.key
                Case "UON"
                    For Each iUON As Object In item.value
                        If ParaMensaje.Para_UON Is Nothing Then
                            ParaMensaje.Para_UON = New DataTable

                            ParaMensaje.Para_UON.Columns.Add("UON1", GetType(System.String))
                            ParaMensaje.Para_UON.Columns.Add("UON2", GetType(System.String))
                            ParaMensaje.Para_UON.Columns.Add("UON3", GetType(System.String))
                            ParaMensaje.Para_UON.Columns.Add("DEP", GetType(System.String))
                            ParaMensaje.Para_UON.Columns.Add("USU", GetType(System.String))
                        End If
                        If iUON("UON1") Is Nothing AndAlso iUON("UON2") Is Nothing AndAlso iUON("UON3") Is Nothing AndAlso iUON("DEP") Is Nothing AndAlso iUON("USU") Is Nothing Then
                            ParaMensaje.Para_UON_UON0 = True
                        Else
                            row = ParaMensaje.Para_UON.NewRow
                            row("UON1") = iUON("UON1")
                            row("UON2") = iUON("UON2")
                            row("UON3") = iUON("UON3")
                            row("DEP") = iUON("DEP")
                            row("USU") = iUON("USU")

                            ParaMensaje.Para_UON.Rows.Add(row)
                        End If
                    Next
                Case "PROVE"
                    Dim contactoProveedor As DataRow
                    For Each iProve As Object In item.value
                        If iProve("TIPOPROVE") IsNot Nothing Then
                            Select Case iProve("TIPOPROVE")
                                Case 1
                                    ParaMensaje.Para_Prove_Tipo = 1
                                Case 2
                                    ParaMensaje.Para_Prove_Tipo = 2
                                Case 3
                                    ParaMensaje.Para_Prove_Tipo = 3
                                Case 4
                                    ParaMensaje.Para_Prove_Tipo = 4
                                Case 5
                                    ParaMensaje.Para_Prove_Tipo = 5
                                    ParaMensaje.Para_Material_QA = CType(iProve("MATQA"), Integer)
                                Case 6
                                    ParaMensaje.Para_Prove_Tipo = 6
                                    ParaMensaje.Para_Material_GMN1 = iProve("GMN1")
                                    ParaMensaje.Para_Material_GMN2 = iProve("GMN2")
                                    ParaMensaje.Para_Material_GMN3 = iProve("GMN3")
                                    ParaMensaje.Para_Material_GMN4 = iProve("GMN4")
                                Case Else
                                    If ParaMensaje.Para_Prove_Con Is Nothing Then
                                        ParaMensaje.Para_Prove_Con = New DataTable
                                        ParaMensaje.Para_Prove_Con.Columns.Add("PROVE", GetType(System.String))
                                        ParaMensaje.Para_Prove_Con.Columns.Add("CON", GetType(System.Int64))
                                    End If
                                    contactoProveedor = ParaMensaje.Para_Prove_Con.NewRow
                                    contactoProveedor("PROVE") = iProve("CODIGOPROVE")
                                    contactoProveedor("CON") = iProve("IDCONTACTO")

                                    ParaMensaje.Para_Prove_Con.Rows.Add(contactoProveedor)
                            End Select
                        End If
                    Next
                Case "PROCECOMPRAS"
                    For Each iProceCompra As Object In item.value
                        If iProceCompra("TIPOPARAPROCECOMPRA") IsNot Nothing Then
                            With ParaMensaje
                                Select Case iProceCompra("TIPOPARAPROCECOMPRA")
                                    Case "Todos"
                                        .Para_ProcesoCompra_Compradores = True
                                        .Para_ProcesoCompra_Invitados = True
                                        .Para_ProcesoCompra_Proveedores = True
                                        .Para_ProcesoCompra_Responsable = True
                                    Case "Responsable"
                                        .Para_ProcesoCompra_Responsable = True
                                    Case "Invitados"
                                        .Para_ProcesoCompra_Invitados = True
                                    Case "Compradores"
                                        .Para_ProcesoCompra_Compradores = True
                                    Case "Proveedores"
                                        .Para_ProcesoCompra_Proveedores = True
                                End Select
                                .Para_ProceCompra_Anyo = CType(iProceCompra("ANYO"), Integer)
                                .Para_ProceCompra_GMN1 = iProceCompra("GMN1")
                                .Para_ProceCompra_Cod = CType(iProceCompra("COD"), Integer)
                            End With
                        End If
                    Next
                Case "ESTRUCCOMPRAS"
                    Dim estrucCompras As DataRow
                    For Each iEstrucCompras As Object In item.value
                        If iEstrucCompras("TIPOESTRUCCOMPRAS") IsNot Nothing Then
                            Select Case iEstrucCompras("TIPOESTRUCCOMPRAS")
                                Case 3
                                    ParaMensaje.Para_EstructuraCompras_MaterialGS = True
                                    ParaMensaje.Para_Material_GMN1 = iEstrucCompras("GMN1")
                                    ParaMensaje.Para_Material_GMN2 = iEstrucCompras("GMN2")
                                    ParaMensaje.Para_Material_GMN3 = iEstrucCompras("GMN3")
                                    ParaMensaje.Para_Material_GMN4 = iEstrucCompras("GMN4")
                                Case Else
                                    If ParaMensaje.Para_EstructuraCompras_Equipo Is Nothing Then
                                        ParaMensaje.Para_EstructuraCompras_Equipo = New DataTable
                                        ParaMensaje.Para_EstructuraCompras_Equipo.Columns.Add("EQP", GetType(System.String))
                                        ParaMensaje.Para_EstructuraCompras_Equipo.Columns.Add("USU", GetType(System.String))
                                    End If
                                    estrucCompras = ParaMensaje.Para_EstructuraCompras_Equipo.NewRow
                                    With estrucCompras
                                        Select Case iEstrucCompras("TIPOESTRUCCOMPRAS")
                                            Case "1"
                                                estrucCompras("EQP") = iEstrucCompras("CODIGO")
                                                estrucCompras("USU") = Nothing
                                            Case "2"
                                                estrucCompras("EQP") = Nothing
                                                estrucCompras("USU") = iEstrucCompras("CODIGO")
                                        End Select
                                    End With
                                    ParaMensaje.Para_EstructuraCompras_Equipo.Rows.Add(estrucCompras)
                            End Select
                        End If
                    Next
                Case "GRUPO"
                    For Each iGrupo As Object In item.value
                        If iGrupo("TIPOGRUPO") Is Nothing Then
                            Dim idGrupo As DataRow
                            If ParaMensaje.Para_Grupos_Grupos Is Nothing Then
                                ParaMensaje.Para_Grupos_Grupos = New DataTable
                                ParaMensaje.Para_Grupos_Grupos.Columns.Add("GRUPO", GetType(System.Int64))
                            End If
                            idGrupo = ParaMensaje.Para_Grupos_Grupos.NewRow
                            idGrupo("GRUPO") = iGrupo("IDGRUPO")

                            ParaMensaje.Para_Grupos_Grupos.Rows.Add(idGrupo)
                        Else
                            Select Case iGrupo("TIPOGRUPO")
                                Case "EP"
                                    ParaMensaje.Para_Grupo_EP = True
                                Case "GS"
                                    ParaMensaje.Para_Grupo_GS = True
                                Case "PM"
                                    ParaMensaje.Para_Grupo_PM = True
                                Case "QA"
                                    ParaMensaje.Para_Grupo_QA = True
                                Case "SM"
                                    ParaMensaje.Para_Grupo_SM = True
                            End Select
                        End If
                    Next
            End Select
        Next
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Comprobar_HayMensajesHistoricos() As Boolean
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocn_Data As FSNServer.cn_Data
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")

            ocn_Data = FSNServer.Get_Object(GetType(FSNServer.cn_Data))
            With CN_Usuario
                Return ocn_Data.Comprobar_HayMensajesHistoricos(.Cod)
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
#Region "Buscador"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="tipoOrdenacion"></param>
    ''' <param name="textoBusqueda"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function ObtenerResultadosBusqueda(ByVal tipoOrdenacion As Integer, ByVal textoBusqueda As String, _
                                              ByVal timezoneoffset As Double) As Object
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ocnMensajes As FSNServer.cnMensajes
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim _grammar As New SearchGrammar
            Dim _language As LanguageData
            Dim _parser As Parser
            Dim _parseTree As ParseTree

            _language = New LanguageData(_grammar)
            _parser = New Parser(_language)
            Try
                _parser.Parse(textoBusqueda.ToLower, "<source>")
            Catch ex As Exception
                Throw ex
            Finally
                _parseTree = _parser.Context.CurrentParseTree
            End Try

            Dim iRunner = TryCast(_grammar, ICanRunSample)
            Dim args = New RunSampleArgs(_language, textoBusqueda.ToLower, _parseTree)
            Dim stringFTS As String = iRunner.RunSample(args)
            Dim oMensajes As New List(Of FSNServer.cnMensaje)

            ocnMensajes = FSNServer.Get_Object(GetType(FSNServer.cnMensajes))
            With CN_Usuario
                oMensajes = ocnMensajes.ObtenerResultadosBusqueda(.Cod, .Idioma, CultureInfo.CreateSpecificCulture(CN_Usuario.RefCultural).LCID, _
                                            .DateFormat.ShortDatePattern, .NumberFormat, tipoOrdenacion, stringFTS, timezoneoffset)
            End With

            If oMensajes.Count > 0 Then
                Dim oMensajesYPalabras As New List(Of Object)
                Dim listaPalabras As New List(Of String)
                For Each item As Irony.Parsing.Token In args.ParsedSample.Tokens
                    If item.Terminal.ToString = "Phrase" Then
                        listaPalabras.Add(Replace(item.Text, """", ""))
                    ElseIf item.Terminal.ToString = "Term" Then
                        listaPalabras.Add(item.Text)
                    End If
                Next

                oMensajesYPalabras.Add(oMensajes)
                oMensajesYPalabras.Add(listaPalabras)

                Return oMensajesYPalabras
            Else
                Return oMensajes
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function AyudaBuscador()
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oTextos As DataTable = HttpContext.Current.Cache("Textos_" & CN_Usuario.Idioma.ToString & "_" & TiposDeDatos.ModulosIdiomas.Colaboracion).Tables(0)
            Dim oAyudaBuscador As New FSNServer.cn_fsTreeViewItem
            Dim oTextoAyudaBuscador As FSNServer.cn_fsTreeViewItem
            With oAyudaBuscador
                .value = oTextos.Rows(122).Item(1)
                .text = oTextos.Rows(123).Item(1)
                .children = New List(Of FSNServer.cn_fsTreeViewItem)
                For i As Integer = 124 To 140 Step 2
                    oTextoAyudaBuscador = New FSNServer.cn_fsTreeViewItem
                    oTextoAyudaBuscador.value = oTextos.Rows(i).Item(1)
                    oTextoAyudaBuscador.text = oTextos.Rows(i + 1).Item(1)
                    .children.Add(oTextoAyudaBuscador)
                Next
            End With

            Return oAyudaBuscador
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
End Class