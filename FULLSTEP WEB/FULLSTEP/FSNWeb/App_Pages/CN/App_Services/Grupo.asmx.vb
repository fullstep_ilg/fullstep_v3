﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web
Imports System.Text
Imports System.IO
Imports System.Globalization
Imports System.Threading

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<System.Web.Script.Services.ScriptService()> _
<ToolboxItem(False)> _
Public Class Grupo
    Inherits System.Web.Services.WebService
    ''' <summary>
    ''' Carga de los datos del grupo
    ''' </summary>
    ''' <param name="iIdGrupo">grupo</param>
    ''' <returns>Objeto de tipo cnGrupo</returns>
    ''' <remarks>Llamado desde: cnGrupos_ui.js  ; Tiempo máximo: 0,1</remarks>
    ''' <revisado>JVS 07/03/2012</revisado>
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Load_Datos_Grupo(ByVal iIdGrupo As Integer) As FSNServer.cnGrupo
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

        Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim ocnGrupo As FSNServer.cnGrupo
        ocnGrupo = FSNServer.Get_Object(GetType(FSNServer.cnGrupo))

        ocnGrupo.LoadGrupo(iIdGrupo)

        Return ocnGrupo
    End Function
    ''' <summary>
    ''' Carga los miembros del grupo
    ''' </summary>
    ''' <param name="IdGrupo">grupo</param>
    ''' <remarks>Llamada desde: cnGrupos.aspx; Tiempo máximo:0,2</remarks>
    ''' <revisado>JVS 08/03/2012</revisado>
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_Miembros_Grupo(ByVal idGrupo As Nullable(Of Integer)) As List(Of Object)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim ocnGrupo As FSNServer.cnGrupo
        ocnGrupo = FSNServer.Get_Object(GetType(FSNServer.cnGrupo))

        Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim oMiembros As New List(Of Object)
        With CN_Usuario
            oMiembros = ocnGrupo.Load_Miembros_Grupo(.Idioma, idGrupo)
        End With

        Return oMiembros
    End Function
    ''' <summary>
    ''' Obtener UONs para asignar a la Categoría
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_ListaTodosGrupos() As List(Of FSNServer.cn_fsTreeViewItem)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim ocnGrupo As FSNServer.cnGrupo
        ocnGrupo = FSNServer.Get_Object(GetType(FSNServer.cnGrupo))

        Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim oUONs As New List(Of FSNServer.cn_fsTreeViewItem)
        With CN_Usuario
            oUONs = ocnGrupo.Obtener_UONs_Grupo(.Idioma)
        End With

        Return oUONs
    End Function
    ''' <summary>
    ''' Guarda los datos del grupo.
    ''' </summary>
    ''' <param name="IdGrupo">Id de Grupo</param>
    ''' <param name="DenomGrupo">Denominaciones de Grupo</param>
    ''' <param name="Miembros">Lista de miembros que pueden asignar al grupo</param>
    ''' <remarks></remarks>
    ''' <revisado>JVS 13/02/2012</revisado>
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Sub UpdateGrupo(ByVal IdGrupo As Integer,
                           ByVal DenomGrupo As Object,
                           ByVal Miembros As Object)
        Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim ocnGrupo As FSNServer.cnGrupo
        ocnGrupo = FSNServer.Get_Object(GetType(FSNServer.cnGrupo))

        Dim MiembrosGrupos As FSNServer.cnMiembroGrupo = FSNServer.Get_Object(GetType(FSNServer.cnMiembroGrupo))

        ObtenerMiembros_Grupo(MiembrosGrupos, Miembros)

        ocnGrupo.IdGrupo = IdGrupo

        Dim CNNomGrupo As FSNServer.cnMultiIdioma
        Dim CNDescrGrupo As FSNServer.cnMultiIdioma
        Dim den As Object

        CNNomGrupo = New FSNServer.cnMultiIdioma
        CNDescrGrupo = New FSNServer.cnMultiIdioma
        For Each den In DenomGrupo("CNNomGrupo")
            CNNomGrupo(den("value")) = den("text")
        Next

        For Each den In DenomGrupo("CNDescrGrupo")
            CNDescrGrupo(den("value")) = den("text")
        Next

        ocnGrupo.Update_Grupo(CN_Usuario.Idioma, CNNomGrupo, CNDescrGrupo, MiembrosGrupos.Para_UON)

        Notificar_AltaGrupo(MiembrosGrupos.Para_UON, IdGrupo)
    End Sub
    ''' <summary>
    ''' Inserta los datos del grupo.
    ''' </summary>
    ''' <param name="DenomGrupo">Denominaciones de Grupo</param>
    ''' <param name="Miembros">Lista de miembros que pueden asignar al grupo</param>
    ''' <remarks></remarks>
    ''' <revisado>JVS 14/03/2012</revisado>
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Sub InsertarGrupo(ByVal DenomGrupo As Object, ByVal Miembros As Object)
        Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim ocnGrupo As FSNServer.cnGrupo
        ocnGrupo = FSNServer.Get_Object(GetType(FSNServer.cnGrupo))

        Dim MiembrosGrupos As FSNServer.cnMiembroGrupo = FSNServer.Get_Object(GetType(FSNServer.cnMiembroGrupo))

        ObtenerMiembros_Grupo(MiembrosGrupos, Miembros)

        Dim CNNomGrupo As New FSNServer.cnMultiIdioma
        Dim CNDescrGrupo As New FSNServer.cnMultiIdioma
        Dim den As Object

        For Each den In DenomGrupo("CNNomGrupo")
            CNNomGrupo(den("value")) = den("text")
        Next

        For Each den In DenomGrupo("CNDescrGrupo")
            CNDescrGrupo(den("value")) = den("text")
        Next

        Dim idGrupo As Integer = ocnGrupo.Insertar_Grupo(CN_Usuario.Cod, CN_Usuario.Idioma, CNNomGrupo, CNDescrGrupo, MiembrosGrupos.Para_UON)

        Notificar_AltaGrupo(MiembrosGrupos.Para_UON, idGrupo)
    End Sub
    ''' <summary>
    ''' Borra los datos del Grupo.
    ''' </summary>
    ''' <remarks></remarks>
    ''' <revisado>JVS 13/02/2012</revisado>
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Sub DeleteGrupo(ByVal IdGrupo As Integer)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim ocnGrupo As FSNServer.cnGrupo
        ocnGrupo = FSNServer.Get_Object(GetType(FSNServer.cnGrupo))
        ocnGrupo.IdGrupo = IdGrupo
        ocnGrupo.Delete_Grupo()
    End Sub
    Private Sub ObtenerMiembros_Grupo(ByRef MiembrosGrupo As FSNServer.cnMiembroGrupo, ByVal Miembros As Object)
        Dim row As DataRow
        For Each item As Object In Miembros
            Select Case item.key
                Case "UON"
                    For Each iUON As Object In item.value
                        If MiembrosGrupo.Para_UON Is Nothing Then
                            MiembrosGrupo.Para_UON = New DataTable

                            MiembrosGrupo.Para_UON.Columns.Add("UON1", GetType(System.String))
                            MiembrosGrupo.Para_UON.Columns.Add("UON2", GetType(System.String))
                            MiembrosGrupo.Para_UON.Columns.Add("UON3", GetType(System.String))
                            MiembrosGrupo.Para_UON.Columns.Add("DEP", GetType(System.String))
                            MiembrosGrupo.Para_UON.Columns.Add("USU", GetType(System.String))
                            MiembrosGrupo.Para_UON.Columns.Add("NOTIFICAR", GetType(System.Boolean))
                        End If
                        If iUON("UON1") Is Nothing AndAlso iUON("UON2") Is Nothing AndAlso iUON("UON3") Is Nothing AndAlso iUON("DEP") Is Nothing AndAlso iUON("USU") Is Nothing Then
                            MiembrosGrupo.Para_UON_UON0 = True
                        Else
                            row = MiembrosGrupo.Para_UON.NewRow
                            row("UON1") = iUON("UON1")
                            row("UON2") = iUON("UON2")
                            row("UON3") = iUON("UON3")
                            row("DEP") = iUON("DEP")
                            row("USU") = iUON("USU")
                            row("NOTIFICAR") = True

                            MiembrosGrupo.Para_UON.Rows.Add(row)
                        End If
                    Next
            End Select
        Next
    End Sub
    Private Sub Notificar_AltaGrupo(ByVal Notificados As DataTable, ByVal idGrupo As Integer)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Try
            Dim oGrupo As FSNServer.cnGrupo
            oGrupo = FSNServer.Get_Object(GetType(FSNServer.cnGrupo))
            oGrupo.Notificar_AltaGrupo(ConfigurationManager.AppSettings("mailFromCN"), Notificados, idGrupo)
        Catch ex As Exception
            Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim tmpExcepcion As Exception = ex
            Dim oErrores As FSNServer.Errores
            oErrores = FSNServer.Get_Object(GetType(FSNServer.Errores))
            oErrores.Create("Notificar.vb\Notificar_Actividad_FSCN", CN_Usuario.Cod, tmpExcepcion.GetType().FullName, tmpExcepcion.Message, tmpExcepcion.StackTrace, "", "")
            oErrores = Nothing
        End Try
    End Sub
End Class