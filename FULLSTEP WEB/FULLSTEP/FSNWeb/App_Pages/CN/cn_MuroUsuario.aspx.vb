﻿Imports System.IO
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports System.Web.Script.Serialization

Public Class cn_MuroUsuario
    Inherits FSNPage

    Private m_desdeGS As Boolean = False
    ''' <summary>
    ''' Evento preinit en el que configuramos la apariencia de algunos controles
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' <revisado>JVS 10/04/2012</revisado>
    Private Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Not String.IsNullOrEmpty(Request.QueryString("desdeGS")) Then
            m_desdeGS = True
        End If
        If m_desdeGS Then
            Me.MasterPageFile = "~/App_Master/EnBlanco.Master"
        Else
            Me.MasterPageFile = "~/App_Master/Menu.Master"
        End If
    End Sub
#Region "Carga inicial"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Expires = 0
        If Not m_desdeGS Then
            Dim Master = CType(Me.Master, FSNWeb.Menu)
            Master.Seleccionar("Colaboracion", "")
        End If
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Colaboracion
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "LimiteMensajesCargados") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "LimiteMensajesCargados", "<script>var LimiteMensajesCargados =" & System.Configuration.ConfigurationManager.AppSettings("LimiteMensajesCargados") & "</script>")
        End If
        If Not IsPostBack Then
            With FSNPageHeader
                .TituloCabecera = Textos(0) 'Colaboración
            End With
        End If
    End Sub
#End Region
End Class