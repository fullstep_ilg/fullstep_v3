﻿Imports System.IO
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports System.Web.Script.Serialization
Imports Infragistics.Web.UI
Imports Infragistics.Web.UI.GridControls
Imports Infragistics.Documents.Reports
Imports Infragistics.Documents.Reports.Report

Public Class cn_Categorias
    Inherits FSNPage
    Private m_desdeGS As Boolean = False
    ''' <summary>
    ''' Evento preinit en el que configuramos la apariencia de algunos controles
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' <revisado>JVS 10/04/2012</revisado>
    Private Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Not String.IsNullOrEmpty(Request.QueryString("desdeGS")) Then
            m_desdeGS = True
        End If

        If m_desdeGS Then
            Me.MasterPageFile = "~/App_Master/EnBlanco.Master"
        Else
            Me.MasterPageFile = "~/App_Master/Menu.Master"
        End If
    End Sub
#Region "Carga inicial"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not m_desdeGS Then
            Dim Master = CType(Me.Master, FSNWeb.Menu)
            Master.Seleccionar("Colaboracion", "")
        End If
        If Not FSNUser.CNPermisoAdministrarCategorias Then
            Response.Redirect(System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "Inicio.aspx", True)
        End If
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "'</script>")
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.CategoriasCN

        If Not Page.IsPostBack Then
            Dim cnCategorias As FSNServer.cnCategorias = FSNServer.Get_Object(GetType(FSNServer.cnCategorias))
            Me.InsertarEnCache("oCategorias_" & FSNUser.Cod, cnCategorias.Load_Categorias(FSNUser.Cod, FSNUser.Idioma.ToString, FSNUser.CNRestriccionOrganizarRed))

            CargaImagenes()
            With FSNPageHeader
                .TituloCabecera = Textos(0) 'Colaboración
            End With

            With whdgCategorias
                .IsSelfReference = True
                .Columns("Cat").Header.Text = Textos(42)  'Categorí­a
                .Columns("Mensajes").Header.Text = Textos(38)  'Mensajes
                .Columns("PermisosImg").Header.Text = Textos(39)  'Permisos
            End With

            lblTitulo.Text = Textos(41)  'Administración de la red de colaboración. Categorías
            lblExcel.Text = Textos(43) & "  " 'Exportar Excel
            lblPDF.Text = Textos(44) & "  " 'Exportar PDF
            CargarGrid()
        Else
            Select Case Request("__EVENTTARGET")
                Case btnAceptarEx.ClientID
                    If Request("__EVENTARGUMENT") = "Reload" Then
                        Cache.Remove("oCategorias_" & FSNUser.Cod)
                        Dim cnCategorias As FSNServer.cnCategorias = FSNServer.Get_Object(GetType(FSNServer.cnCategorias))
                        Me.InsertarEnCache("oCategorias_" & FSNUser.Cod, cnCategorias.Load_Categorias(FSNUser.Cod, FSNUser.Idioma.ToString, FSNUser.CNRestriccionOrganizarRed))
                        CargarGrid()
                        upGridCategorias.Update()
                    End If
                Case "ExportarExcel"
                    CargarGrid()
                    ExportarExcel()
                Case "ExportarPDF"
                    CargarGrid()
                    ExportarPDF()
            End Select
        End If
        CargarTextos()
    End Sub
    ''' <summary>
    ''' Guarda los textos de la pantalla en una variable javascript para poder acceder a ellos desde jquery
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load; Tiempo máximo: 0,1 sg</remarks>
    ''' <revisado>JVS 13/03/2012</revisado>
    Private Sub CargarTextos()
        Dim sVariableJavascriptTextos As String = "<script>var TextosCat = new Array();"
        Dim oIdiomas As FSNServer.Idiomas
        Dim iIndiceIdiomas As Integer

        oIdiomas = FSNServer.Get_Object(GetType(FSNServer.Idiomas))

        oIdiomas.Load()

        sVariableJavascriptTextos &= "var IdiomasCod = new Array(); var IdiomasTxt = new Array();"

        iIndiceIdiomas = 0
        For Each oIdioma In oIdiomas.Idiomas
            sVariableJavascriptTextos &= "IdiomasCod[" & iIndiceIdiomas & "]='" & oIdioma.Cod & "';"
            sVariableJavascriptTextos &= "IdiomasTxt[" & iIndiceIdiomas & "]='" & oIdioma.Den & "';"
            iIndiceIdiomas += 1
        Next

        For i As Integer = 1 To 37
            sVariableJavascriptTextos &= "TextosCat[" & i & "]='" & JSText(Textos(i)) & "';"
        Next
        sVariableJavascriptTextos &= "TextosCat[" & 38 & "]='" & JSText(Textos(40)) & "';"
        sVariableJavascriptTextos &= "TextosCat[" & 39 & "]='" & JSText(Textos(45)) & "';"
        sVariableJavascriptTextos &= "TextosCat[" & 40 & "]='" & JSText(Textos(46)) & "';"

        Dim cnCategorias As FSNServer.cnCategorias = FSNServer.Get_Object(GetType(FSNServer.cnCategorias))
        Dim iDespub As Integer = cnCategorias.Despublicados_Categorias(FSNUser.Cod, FSNUser.Idioma.ToString, FSNUser.CNRestriccionOrganizarRed)

        sVariableJavascriptTextos &= "var MostrarLnkDespublic = " & iDespub & ";"

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextos & "</script>")
    End Sub
    ''' <summary>
    ''' Guarda valores de variables en una variable javascript para poder acceder a ellas desde jquery
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load; Tiempo máximo: 0,1 sg</remarks>
    ''' <revisado>JVS 12/04/2012</revisado>
#End Region
#Region "WebHierarchicalDataGridControl"
    Private Sub whdgCategorias_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgCategorias.InitializeRow
        Dim index As Integer
        index = e.Row.Items.FindItemByKey("PermisosImg").Index
        Dim imgItem As Infragistics.Web.UI.GridControls.GridRecordItem
        imgItem = e.Row.Items(index)
        Dim img As Image
        img = imgItem.FindControl("imgPermisos")
        If IsDBNull(e.Row.Items.FindItemByKey("Permisos").Value) Then
            img.Visible = False
        ElseIf e.Row.Items.FindItemByKey("Permisos").Value = False Then
            img.ImageUrl = "~/images/Candado_close.png"
            img.Visible = True
        ElseIf e.Row.Items.FindItemByKey("Permisos").Value = True Then
            img.ImageUrl = "~/images/Candado_open.png"
            img.Visible = True
        End If
        If e.Row.Items.FindItemByKey("NoPublicado").Value Then
            e.Row.CssClass = "Texto12 TextoResaltado Link"
        Else
            e.Row.CssClass = "Texto12 Link"
        End If
    End Sub
    Private Sub CargarGrid()
        ' Recupera los datos de la caché
        Dim ds As DataSet = TryCast(Me.Cache("oCategorias_" & FSNUser.Cod), DataSet)
        ' Configura la fuente de datos y actualiza
        With whdgCategorias
            .Rows.Clear()
            .DataSource = ds
            .DataMember = "CN_CAT"
            .DataKeyFields = "ID"
            .Key = "CN_CAT"
            .DataBind()
        End With
    End Sub
#End Region
#Region "Export"
    ''' <summary>
    ''' Exporta a Excel
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
    ''' <revisado>JVS 23/02/2012</revisado>
    Private Sub ExportarExcel()
        Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
        fileName = fileName.Replace("+", "%20")

        With excelExporter
            .DownloadName = fileName
            .DataExportMode = DataExportMode.AllDataInDataSource

            .Export(whdgCategorias)
        End With
    End Sub
    Private Sub excelExporter_GridRecordItemExported(sender As Object, e As Infragistics.Web.UI.GridControls.GridRecordItemExportedEventArgs) Handles excelExporter.GridRecordItemExported
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.CategoriasCN
        If e.GridCell.Column.Key = "PermisosImg" Then
            If IsDBNull(e.GridCell.Row.DataItem.ITEM("PERMISOPARAPUB")) Then
                e.WorksheetCell.Value = Textos(27)
            Else
                Select Case e.GridCell.Row.DataItem.ITEM("PERMISOPARAPUB")
                    Case True
                        e.WorksheetCell.Value = Textos(26)
                    Case False
                        e.WorksheetCell.Value = Textos(25)
                End Select
            End If
        End If
    End Sub
    ''' <summary>
    ''' Exporta a PDF
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
    ''' <revisado>JVS 23/02/2012</revisado>
    Private Sub ExportarPDF()
        Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
        fileName = fileName.Replace("+", "%20")
        pdfExporter.DownloadName = fileName

        With pdfExporter
            .EnableStylesExport = True

            .DataExportMode = DataExportMode.AllDataInDataSource
            .TargetPaperOrientation = PageOrientation.Landscape

            .Margins = PageMargins.GetPageMargins("Narrow")
            .TargetPaperSize = PageSizes.GetPageSize("A4")

            .Export(whdgCategorias)
        End With
    End Sub
    ''' <summary>
    ''' Carga las imagenes correspondientes en los controles
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0seg.</remarks>
    ''' <revisado>JVS 23/02/2012</revisado>
    Private Sub CargaImagenes()
        imgPDF.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/PDF.png"
        imgExcel.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/Excel.png"
    End Sub
#End Region
End Class