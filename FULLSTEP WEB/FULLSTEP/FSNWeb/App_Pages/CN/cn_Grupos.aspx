﻿<%@ Page  Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="cn_Grupos.aspx.vb" Inherits="Fullstep.FSNWeb.cn_Grupos" %>
<asp:Content ID="GruposCN" ContentPlaceHolderID="CPH4" runat="server">
    <div id="divCabecera" style="clear: both; float: left; width: 100%;">
        <fsn:FSNPageHeader ID="FSNPageHeader" runat="server" UrlImagenCabecera="~/images/ColaboracionHeader.png" />
    </div>
    <!-- Menú izquierda -->
    <div id="divMenu" class="divMenu"></div>
    <div id="divPrincipal" class="divPrincipal" style="display:none;">
        <div id="divTituloMain" class="SeparadorInf" style="clear: both; float: left; padding: 10px; margin-bottom: 15px; margin-left: 0px;">
            <asp:Label ID="lblTitulo" runat="server" CssClass="Texto14 Negrita TextoResaltado"></asp:Label>
        </div>
        <asp:Button ID="btnAceptarEx" runat="server" Style="display:none;"/>
        <ig:WebHierarchicalDataSource ID="WHDSGrupos" runat="server">
            <DataRelations>
                <ig:DataRelation ParentColumns="IDGRUPO" ParentDataViewID="DataSource_Grupos"
                    ChildColumns="IDGRUPO" ChildDataViewID="DataSource_Miembros" />
            </DataRelations>
            <DataViews>
                <ig:DataView ID="DataSource_Grupos" DataSourceID="odsGrupos" />
                <ig:DataView ID="DataSource_Miembros" DataSourceID="odsMiembros" />
            </DataViews>
        </ig:WebHierarchicalDataSource>
        <asp:ObjectDataSource ID="odsGrupos" runat="server" SelectMethod="Load_Grupos" TypeName="Fullstep.FSNServer.cnGrupos">
            <SelectParameters>
                <asp:Parameter Name="sUsuCod" Type="String" />
                <asp:Parameter Name="sIdioma" Type="String" />
                <asp:Parameter Name="vRestricc" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="odsMiembros" runat="server" SelectMethod="CN_Load_Miembros_Grupos"
            TypeName="Fullstep.FSNServer.cnMiembros" />
        <ig:WebExcelExporter ID="excelExporter" runat="server"></ig:WebExcelExporter>
        <ig:WebDocumentExporter ID="pdfExporter" runat="server" ExportMode="Download"></ig:WebDocumentExporter>
        <div style="clear: both; float: left; padding: 10px; width:95%;">            
            <iglc:WebTab ID="wtGrupos" runat="server" Height="100%" Width="100%">
                <Tabs>
                    <iglc:ContentTabItem runat="server" Text="DGrupos">
                        <Template>
                            <div class="CabeceraBotones" style="height: 25px; clear: both; line-height: 25px; width: 100%;">
                                <div style="float: right; margin-right: 5px; height: 25px;">
                                    <div onclick="ExportarExcel()" class="botonDerechaCabecera" style="float: left; margin-left: 5px;">
                                        <asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
                                        <asp:Label runat="server" ID="lblExcel" CssClass="fntLogin2" Text="Excel"></asp:Label>
                                    </div>
                                    <div onclick="ExportarPDF()" class="botonDerechaCabecera" style="float: left; margin-left: 5px;">
                                        <asp:Image runat="server" ID="imgPDF" CssClass="imagenCabecera" />
                                        <asp:Label runat="server" ID="lblPDF" CssClass="fntLogin2" Text="PDF"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <asp:UpdatePanel ID="UPGrupos" runat="server" UpdateMode="Conditional">
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnAceptarEx" EventName="Click" />
                                </Triggers>
                                <ContentTemplate>
                                    <ig:WebHierarchicalDataGrid ID="WHDGGrupos" runat="server" Width="100%" AutoGenerateColumns="false"
                                        AutoGenerateBands="false" DataKeyFields="IDGRUPO" CssClass="Texto12" ShowHeader="True">
                                        <ExpandCollapseAnimation SlideOpenDirection="Auto" SlideOpenDuration="300" SlideCloseDirection="Auto"
                                            SlideCloseDuration="300" />
                                        <Behaviors>
                                            <ig:Paging PagerAppearance="Bottom" PageSize="10" Enabled="true" />
                                            <ig:Filtering />
                                            <ig:ColumnResizing Enabled="true" />
                                            <ig:Selection RowSelectType="Single" Enabled="true" CellClickAction="Row">
                                                <SelectionClientEvents RowSelectionChanging="WHDG_RowSelectionChanging" />
                                            </ig:Selection>
                                        </Behaviors>
                                        <Columns>
                                            <ig:BoundDataField DataFieldName="NOMGRUPO" Key="NomGrupo" Header-Text="DGrupo" Width="30%" />
                                            <ig:BoundDataField DataFieldName="DESCRGRUPO" Key="DescrGrupo" Header-Text="DDescripción"
                                                Width="60%" />
                                            <ig:BoundDataField DataFieldName="MENSAJES" Key="Mensajes" Header-Text="DMensajes"
                                                Width="10%" />
                                        </Columns>
                                        <Bands>
                                            <ig:Band DataMember="DataSource_Miembros" Key="DetalleGrupo" AutoGenerateColumns="false"
                                                DataKeyFields="ESTRUC, NOMBRE" ShowHeader="true">
                                                <Columns>
                                                    <ig:BoundDataField DataFieldName="NOMBRE" Key="Nombre" Header-Text="DNombre" Width="20%" />
                                                    <ig:BoundDataField DataFieldName="EMAIL" Key="Email" Header-Text="DEmail" Width="10%" />
                                                    <ig:BoundDataField DataFieldName="CARGO" Key="Cargo" Header-Text="DCargo" Width="10%" />
                                                    <ig:BoundDataField DataFieldName="ESTRUC" Key="Estruc" Header-Text="DEstructura organizativa"
                                                        Width="60%" />
                                                </Columns>
                                                <Behaviors>
                                                    <ig:Paging PagerAppearance="Bottom" PageSize="2" Enabled="true" />
                                                </Behaviors>
                                            </ig:Band>
                                        </Bands>
                                    </ig:WebHierarchicalDataGrid>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </Template>
                    </iglc:ContentTabItem>
                    <iglc:ContentTabItem runat="server" Key="Grupo" Text="DNuevo Grupo">
                        <Template>
                            <div style="clear: both; float: left; width: 100%; margin: 0px 0px 5px 0px;">
                                <fieldset style="margin: 15px 15px 0px 15px;">
                                    <legend id="lblNombreGrupo" class="TextoResaltado"></legend>
                                    <div id='divNombresGrupo' style="width: 55%; margin: 5px; padding-top: 5px; padding-right: 5px;
                                        padding-bottom: 5px; padding-left: 5px; float: left;">
                                    </div>
                                </fieldset>
                                <fieldset style="margin: 15px 15px 0px 15px;">
                                    <legend id="lblDescripGrupo" class="TextoResaltado"></legend>
                                    <div id='divDescripcionesGrupo' style="width: 55%; margin: 5px; padding-top: 5px;
                                        padding-right: 5px; padding-bottom: 5px; padding-left: 5px; float: left;">
                                    </div>
                                </fieldset>
                            </div>
                            <div style="clear: both; float: left; width: 100%; margin: 0px 0px 5px 0px;">
                                <div id="divMiembrosCategoria" style="width: 45%; padding-top: 5px; padding-right: 5px;
                                    padding-bottom: 5px; padding-left: 30px; float: left;">
                                    <div style="width: 100%; clear: both; float: left;">
                                        <span id="lblMiembros" class="Texto12 TextoResaltado">DMiembros del grupo</span>
                                    </div>
                                    <div id="divParaDestinatarios" style="clear: both; float: left; width: 100%;">
                                    </div>
                                </div>
                            </div>
                            <div id="divBotones" style="clear: both; float: left; width: 95%; padding-left: 5px;
                                padding-bottom: 10px; margin-top: 10px;">
                                <div id="btnAceptarGrupo" class="botonRedondeado" style="float:left;">
                                    <span id="lblGrupoAceptar">DAceptar</span>
                                </div>
                                <div id="btnCancelarGrupo" class="botonRedondeado" style="float:left; margin-left: 10px;">
                                    <span id="lblGrupoCancelar">DCancelar</span>
                                </div>
                                <div style="float:left; margin-left:10px;">
                                    <span id="lblGrupoGuardadoOK" class="Texto14 Negrita TextoAlternativo" style="display:none;"></span>
                                </div>
                                <div id="btnEliminarGrupo" class="botonRedondeado" style="float:right; margin-right: 5px;">
                                    <span id="lblGrupoEliminar">DEliminar</span>
                                </div>
                            </div>
                        </Template>
                    </iglc:ContentTabItem>
                </Tabs>
                <ClientEvents SelectedIndexChanged="SelectedIndexChanged" />
            </iglc:WebTab>
        </div>
    </div>
    <div id="popupEliminarConfirm" class="popupCN" style="display: none; position: absolute;
        z-index: 1005;">
        <div style="position: relative; clear: both; float: left; width: 100%; margin-top: 15px;
            text-align: center;">
            <div style="width: 100%; clear: both; float: left;">
                <span id="lblPreguntaEliminarConfirm" style="float: left" class="Texto12"></span>
            </div>
        </div>
        <div style="position: relative; clear: both; float: left; width: 100%; margin-top: 15px;
            text-align: center;">
            <div id="btnAceptarEliminarConfirm" class="botonRedondeado" style="margin-right: 5px;">
                <span id="lblAceptarEliminarConfirm" style="line-height: 23px;">DAceptar</span>
            </div>
            <div id="btnCancelarEliminarConfirm" class="botonRedondeado" style="margin-left: 5px;">
                <span id="lblCancelarEliminarConfirm" style="line-height: 23px;">DCancelar</span>
            </div>
        </div>
    </div>
</asp:Content>
