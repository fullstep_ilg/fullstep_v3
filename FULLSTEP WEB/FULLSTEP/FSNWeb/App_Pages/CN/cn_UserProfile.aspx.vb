﻿Imports System.IO
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports System.Web.Script.Serialization

Public Class cn_UserProfile
    Inherits FSNPage
    Private m_desdeGS As Boolean = False
    ''' <summary>
    ''' Evento preinit en el que configuramos la apariencia de algunos controles
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' <revisado>JVS 10/04/2012</revisado>
    Private Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Not String.IsNullOrEmpty(Request.QueryString("desdeGS")) Then
            m_desdeGS = True
        End If

        If m_desdeGS Then
            Me.MasterPageFile = "~/App_Master/EnBlanco.Master"
        Else
            Me.MasterPageFile = "~/App_Master/Menu.Master"
        End If
    End Sub
#Region "Carga inicial"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not m_desdeGS Then
            Dim Master = CType(Me.Master, FSNWeb.Menu)
            Master.Seleccionar("Colaboracion", "")
        End If

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "'</script>")
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.PerfilUsuarioCN
        If Not IsPostBack Then
            With FSNUser
                With FSNPageHeader
                    .TituloCabecera = Textos(0) 'Colaboración
                End With
                lblTitulo.Text = Textos(15)  'Perfil de usuario de la red de colaboración
                lblNombreEtiq.Text = Textos(16)  'Nombre:
                lblNombre.Text = .Nombre
                lblFotoEtiq.Text = Textos(17)  'Foto:
                lblModifFoto.Text = Textos(28)  'Modificar foto perfil

                imgFoto.ImageUrl = "Thumbnail.ashx?t=0"

                lblNotificaciones.Text = Textos(18)  'Notificaciones ví­a email

                CheckBoxListaNotificaciones.Items(0).Text = Textos(19)  'Recibir un email con el resumen de la nueva actividad en la red de colaboración
                CheckBoxListaNotificaciones.Items(1).Text = Textos(20)  'Recibir un email cuando me incluyen en un grupo corporativo

                CheckBoxListaNotificaciones.Items(0).Selected = .CNNotificarResumenActividad
                CheckBoxListaNotificaciones.Items(1).Selected = .CNNotificarIncluidoGrupo

                lblConfiguraciones.Text = Textos(21)  'Opciones de configuración

                CheckBoxListaConfiguraciones.Items(0).Text = Textos(22)  'Volver a mostrar automáticamente los mensajes ocultos en los que haya actividad
                CheckBoxListaConfiguraciones.Items(0).Selected = .CNConfiguracionDesocultar

                lblGrupos.Text = Textos(23)  'Mis grupos corporativos

                If IsNumeric(System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion")) Then
                    WebHierarchicalDataGridGrupos.Bands(0).Behaviors.Paging.PageSize = CInt(System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion"))
                End If
                WebHierarchicalDataGridGrupos.Bands(0).Columns("Estruc").Header.Text = Textos(24)  'Estructura organizativa
                WebHierarchicalDataGridGrupos.Bands(0).Columns("Nombre").Header.Text = Textos(25)  'Nombre
                WebHierarchicalDataGridGrupos.Bands(0).Columns("Email").Header.Text = Textos(26)  'Email
                WebHierarchicalDataGridGrupos.Bands(0).Columns("Cargo").Header.Text = Textos(27)  'Cargo

                WebHierarchicalDataGridGrupos.DataSourceID = "WebHierarchicalDataSource1"

                odsGrupos.DataBind()
                odsMiembros.DataBind()
            End With
        End If
        CargarTextos()
    End Sub
#End Region
#Region "Object Data Sources"
    Private Sub odsGrupos_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles odsGrupos.ObjectCreating
        e.ObjectInstance = FSNServer.Get_Object(GetType(FSNServer.cnGrupos))
    End Sub
    Protected Sub odsGrupos_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsGrupos.Selecting
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.PerfilUsuarioCN
        e.InputParameters.Item("sUsuCod") = FSNUser.Cod
        e.InputParameters.Item("sIdioma") = FSNUser.Idioma.ToString
    End Sub
    Private Sub odsMiembros_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles odsMiembros.ObjectCreating
        e.ObjectInstance = FSNServer.Get_Object(GetType(FSNServer.cnMiembros))
    End Sub
    Private Sub odsMiembros_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsMiembros.Selecting
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.PerfilUsuarioCN
    End Sub
#End Region
#Region "funciones de control de pantalla"
    ''' <summary>
    ''' Guarda los textos de la pantalla en una variable javascript para poder acceder a ellos desde jquery
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarTextos()
        Dim sVariableJavascriptTextos As String = "<script>var TextosUser = new Array();"

        For i = 1 To 15
            sVariableJavascriptTextos &= "TextosUser[" & i & "]='" & JSText(Textos(i)) & "';"
        Next
        sVariableJavascriptTextos &= "TextosUser[16]='" & JSText(Textos(29)) & "';"   'Aceptar
        sVariableJavascriptTextos &= "TextosUser[17]='" & JSText(Textos(30)) & "';"   'Cancelar
        sVariableJavascriptTextos &= "TextosUser[18]='" & JSText(Textos(31)) & "';"   'Cancelar

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextos & "</script>")
    End Sub
#End Region
End Class