﻿
Imports System.IO
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports System.Web.Script.Serialization
Imports Infragistics.Web.UI
Imports Infragistics.Web.UI.GridControls
Imports Infragistics.Documents.Reports
Imports Infragistics.Documents.Reports.Report
Public Class cn_Grupos
    Inherits FSNPage
    Private m_desdeGS As Boolean = False
    ''' <summary>
    ''' Evento preinit en el que configuramos la apariencia de algunos controles
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' <revisado>JVS 10/04/2012</revisado>
    Private Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Not String.IsNullOrEmpty(Request.QueryString("desdeGS")) Then
            m_desdeGS = True
        End If

        If m_desdeGS Then
            Me.MasterPageFile = "~/App_Master/EnBlanco.Master"
        Else
            Me.MasterPageFile = "~/App_Master/Menu.Master"
        End If
    End Sub
#Region "Carga inicial"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not m_desdeGS Then
            Dim Master = CType(Me.Master, FSNWeb.Menu)
            Master.Seleccionar("Colaboracion", "")
        End If
        If Not FSNUser.CNPermisoAdministrarGrupos Then
            Response.Redirect(System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "Inicio.aspx", True)
        End If
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "'</script>")
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.GruposCN

        WHDGGrupos.DataSourceID = "WHDSGrupos"

        If Not Page.IsPostBack Then
            CargaImagenes()
            With FSNPageHeader
                .TituloCabecera = Textos(0) 'Colaboración
            End With

            wtGrupos.Tabs(0).Text = Textos(10) 'Grupos

            If IsNumeric(System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion")) Then
                WHDGGrupos.Behaviors.Paging.PageSize = CInt(System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion"))
                WHDGGrupos.Bands(0).Behaviors.Paging.PageSize = CInt(System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion"))
            End If

            WHDGGrupos.Columns("NomGrupo").Header.Text = Textos(23)  'Grupo
            WHDGGrupos.Columns("DescrGrupo").Header.Text = Textos(24)  'Descripción
            WHDGGrupos.Columns("Mensajes").Header.Text = Textos(29)  'Mensajes
            WHDGGrupos.Bands(0).Columns("Nombre").Header.Text = Textos(25)  'Nombre
            WHDGGrupos.Bands(0).Columns("Email").Header.Text = Textos(26)  'Email
            WHDGGrupos.Bands(0).Columns("Cargo").Header.Text = Textos(27)  'Cargo
            WHDGGrupos.Bands(0).Columns("Estruc").Header.Text = Textos(28)  'Estructura organizativa

            wtGrupos.Tabs(1).Text = Textos(30) 'Nuevo Grupo
            lblTitulo.Text = Textos(31)  'Administración de la red de colaboración. Grupos

            lblExcel.Text = Textos(32) & "  " 'Exportar Excel
            lblPDF.Text = Textos(33) & "  " 'Exportar PDF

            CargarTextos()
        Else
            Select Case Request("__EVENTTARGET")
                Case btnAceptarEx.ClientID
                    If Request("__EVENTARGUMENT") = "Reload" Then
                        CargarGrid()
                        UPGrupos.Update()
                    End If
                Case "ExportarExcel"
                    CargarGrid()
                    ExportarExcel()
                Case "ExportarPDF"
                    CargarGrid()
                    ExportarPDF()
            End Select
        End If
    End Sub
    ''' <summary>
    ''' Guarda los textos de la pantalla en una variable javascript para poder acceder a ellos desde jquery
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load; Tiempo máximo: 0,1 sg</remarks>
    ''' <revisado>JVS 13/03/2012</revisado>
    Private Sub CargarTextos()
        Dim sVariableJavascriptTextos As String = "<script>var TextosGrupos = new Array();"
        Dim oIdiomas As FSNServer.Idiomas
        Dim iIndiceIdiomas As Integer

        oIdiomas = FSNServer.Get_Object(GetType(FSNServer.Idiomas))

        oIdiomas.Load()

        sVariableJavascriptTextos &= "var IdiomasCod = new Array(); var IdiomasTxt = new Array();"
        For i As Integer = 1 To 22
            sVariableJavascriptTextos &= "TextosGrupos[" & i & "]='" & JSText(Textos(i)) & "';"
        Next
        sVariableJavascriptTextos &= "TextosGrupos[" & 23 & "]='" & JSText(Textos(30)) & "';"
        sVariableJavascriptTextos &= "TextosGrupos[" & 24 & "]='" & JSText(Textos(34)) & "';"

        iIndiceIdiomas = 0
        For Each oIdioma In oIdiomas.Idiomas
            sVariableJavascriptTextos &= "IdiomasCod[" & iIndiceIdiomas & "]='" & oIdioma.Cod & "';"
            sVariableJavascriptTextos &= "IdiomasTxt[" & iIndiceIdiomas & "]='" & oIdioma.Den & "';"
            iIndiceIdiomas += 1
        Next

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextos & "</script>")
    End Sub
    Private Sub CargarGrid()
        odsGrupos.DataBind()
        odsMiembros.DataBind()
        UPGrupos.Update()
    End Sub
#End Region
#Region "Object Data Sources"
    Private Sub odsGrupos_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles odsGrupos.ObjectCreating
        e.ObjectInstance = FSNServer.Get_Object(GetType(FSNServer.cnGrupos))
    End Sub
    Protected Sub odsGrupos_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsGrupos.Selecting
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.GruposCN
        e.InputParameters.Item("sUsuCod") = FSNUser.Cod
        e.InputParameters.Item("sIdioma") = FSNUser.Idioma.ToString
        e.InputParameters.Item("vRestricc") = FSNUser.CNRestriccionOrganizarRed
    End Sub
    Private Sub odsMiembros_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles odsMiembros.ObjectCreating
        e.ObjectInstance = FSNServer.Get_Object(GetType(FSNServer.cnMiembros))
    End Sub
    Private Sub odsMiembros_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsMiembros.Selecting
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.GruposCN
    End Sub
#End Region
#Region "WebHierarchicalDataGridControl"
    ''' <summary>
    ''' Se ejecuta cada vez que se inicializa una fila del grid
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento</param>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>  
    ''' <remarks>Llamada desde: Sistema ; Tiempo máximo: 0</remarks>
    Private Sub WHDGGrupos_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles WHDGGrupos.InitializeRow
        If DirectCast(DirectCast(e.Row, Infragistics.Web.UI.GridControls.ContainerGridRecord).DataItem, Infragistics.Web.UI.DataSourceControls.WebHierarchyData).Type.ToString = "DataSource_Miembros" Then
            'Miembros no pinchable, luego no es link
            e.Row.CssClass = "Texto12"
        Else
            e.Row.CssClass = "Texto12 Link"
        End If
    End Sub
#End Region
#Region "Export"
    ''' <summary>
    ''' Exporta a Excel
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
    ''' <revisado>JVS 23/02/2012</revisado>
    Private Sub ExportarExcel()
        Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
        fileName = fileName.Replace("+", "%20")

        With excelExporter
            .DownloadName = fileName
            .DataExportMode = DataExportMode.AllDataInDataSource

            .Export(WHDGGrupos)
        End With
    End Sub
    ''' <summary>
    ''' Exporta a PDF
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
    ''' <revisado>JVS 23/02/2012</revisado>
    Private Sub ExportarPDF()
        Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
        fileName = fileName.Replace("+", "%20")
        pdfExporter.DownloadName = fileName

        With pdfExporter
            .EnableStylesExport = True

            .DataExportMode = DataExportMode.AllDataInDataSource
            .TargetPaperOrientation = PageOrientation.Landscape

            .Margins = PageMargins.GetPageMargins("Narrow")
            .TargetPaperSize = PageSizes.GetPageSize("A4")

            .Export(WHDGGrupos)
        End With
    End Sub
    ''' <summary>
    ''' Carga las imagenes correspondientes en los controles
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0seg.</remarks>
    ''' <revisado>JVS 23/02/2012</revisado>
    Private Sub CargaImagenes()
        imgPDF.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/PDF.png"
        imgExcel.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/Excel.png"

    End Sub
#End Region
End Class