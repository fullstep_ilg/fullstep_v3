﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="cn_UserProfile.aspx.vb" Inherits="Fullstep.FSNWeb.cn_UserProfile" %>
<asp:Content ID="UserProfileCN" ContentPlaceHolderID="CPH4" runat="server">
    <div id="divCabecera" style="clear: both; float: left; width: 100%;">
        <fsn:FSNPageHeader ID="FSNPageHeader" runat="server" UrlImagenCabecera="~/images/ColaboracionHeader.png" />
    </div>
    <!-- Menú izquierda -->
    <div id="divMenu" class="divMenu"></div>
    <div id="divPrincipal" class="divPrincipal" style="display:none;">
        <div id="divTituloMain" class="SeparadorInf" style="clear:both; float:left; margin:15px 0px 15px 10px;">
            <asp:Label ID="lblTitulo" runat="server" CssClass="Texto14 Negrita TextoResaltado"></asp:Label>
        </div>
        <div style="clear:both; float:left; margin-top:5px; margin-left:10px;">
            <asp:Label ID="lblNombreEtiq" runat="server" Text="DNombre:" CssClass="Texto12"></asp:Label>
            <asp:Label ID="lblNombre" runat="server" CssClass="Texto12"></asp:Label>
        </div>        
        <div id="divImagenAdjuntos" style="clear:both; float:left; margin-top:5px; margin-left:10px;">    
            <div id="divFotoEtiq" style="float:left;">
                <asp:Label ID="lblFotoEtiq" runat="server" Text="DFoto:" CssClass="Texto12"></asp:Label>
            </div>
            <div class="fileupload-content">
                <div id="tablaadjuntos" class="files" usu="true" style="float:left; margin-left:5px;">
                    <div class="preview" style="float:left; width:85px; text-align:center;">
                        <asp:Image ID="imgFoto" runat="server" CssClass="LoggedUserImage" />
                    </div>
                </div>
                <div class="fileupload-progressbar"></div>
            </div>  
                  
            <div id="Perfil" style="clear:both; float:left; margin-top:5px; height:25px;">
                <div id="ModificarFotoPerfil" class="OtrasOpciones">
                    <asp:Label ID="lblModifFoto" runat="server" Text="DModificar la foto de perfil" CssClass="Texto12 Subrayado"></asp:Label>
                </div>
            </div>      
        </div>        
        <div id="divTituloNotificaciones" class="SeparadorInf" style="clear:both; float:left; margin-top:10px; margin-left:10px;">
            <img alt="" id="imgNotificaciones" />
            <asp:Label ID="lblNotificaciones" runat="server" Text="DNotificaciones vía email" CssClass="Texto12 Negrita"></asp:Label>
        </div>
        <div id="divNotif" style="clear:both; float:left; margin-top:5px; margin-left:10px;">
            <asp:CheckBoxList ID="CheckBoxListaNotificaciones" runat="server" CssClass="Texto12">
                <asp:ListItem Value="1">DRecibir un email con el resumen de la nueva actividad en la red de colaboración</asp:ListItem>
                <asp:ListItem Value="2">DRecibir un email cuando me incluyen en un grupo corporativo</asp:ListItem>
            </asp:CheckBoxList>
        </div>
        <div id="divConfig" class="SeparadorInf" style="clear:both; float:left; margin-top:10px; margin-left:10px;">
            <img alt="" id="imgConfiguraciones" />
            <asp:Label ID="lblConfiguraciones" runat="server" Text="DConfiguraciones" CssClass="Texto12 Negrita"></asp:Label>
        </div>
        <div id="div2" style="clear:both; float:left; margin-top:5px; margin-left:10px;">
            <asp:CheckBoxList ID="CheckBoxListaConfiguraciones" runat="server" CssClass="Texto12">
                <asp:ListItem Value="1">DVolver a mostrar automáticamente los mensajes ocultos en los que haya actividad</asp:ListItem>
            </asp:CheckBoxList>
        </div>
        <div id="divBotones" style="clear:both; float:left; padding-right:5%; margin-top:10px; margin-left:10px;">
            <div id="btnAceptarUserProfile" class="botonRedondeado" style="float:left;">
                <span id="lblUserProfileAceptar">DAceptar</span>
            </div>
            <div id="btnCancelarUserProfile" class="botonRedondeado" style="float:left; margin-left:10px;">
                <span id="lblUserProfileCancelar">DCancelar</span>
            </div>
            <div style="float:left; margin-left:10px;">
                <span id="lblPerfilGuardadoOK" class="Texto14 Negrita TextoAlternativo" style="display:none;"></span>
            </div>
        </div>
        <div id="divGrupos" class="SeparadorInf" style="clear:both; float:left; margin-top:10px; margin-left:10px;">
            <img alt="" id="imgGrupos" />
            <asp:Label ID="lblGrupos" runat="server" Text="DMis grupos corporativos" CssClass="Texto12 Negrita"></asp:Label>
        </div>
        <ig:WebHierarchicalDataSource ID="WebHierarchicalDataSource1" runat="server">
            <DataRelations>
                <ig:DataRelation ParentColumns="IDGRUPO" ParentDataViewID="DataSource_Grupos"
                    ChildColumns="IDGRUPO" ChildDataViewID="DataSource_Miembros" />
            </DataRelations>
            <DataViews>
                <ig:DataView ID="DataSource_Grupos" DataSourceID="odsGrupos" />
                <ig:DataView ID="DataSource_Miembros" DataSourceID="odsMiembros" />
            </DataViews>
        </ig:WebHierarchicalDataSource>
        <asp:ObjectDataSource ID="odsGrupos" runat="server" SelectMethod="CN_Load_Grupos_Usuario" TypeName="Fullstep.FSNServer.cnGrupos">
            <SelectParameters>
                <asp:Parameter Name="sUsuCod" Type="String" />
                <asp:Parameter Name="sIdioma" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="odsMiembros" runat="server" SelectMethod="CN_Load_Miembros_Grupos"
            TypeName="Fullstep.FSNServer.cnMiembros" />
        <div style="clear:both; float:left; margin-left:10px; margin-right:10px;">
            <ig:WebHierarchicalDataGrid ID="WebHierarchicalDataGridGrupos" runat="server" Width="100%"
                AutoGenerateColumns="false" AutoGenerateBands="false" DataKeyFields="IDGRUPO"
                CssClass="Texto12" ShowHeader="false">            
                <ExpandCollapseAnimation SlideOpenDirection="Auto" SlideOpenDuration="300" SlideCloseDirection="Auto"
                    SlideCloseDuration="300" />
                <Columns>
                    <ig:BoundDataField DataFieldName="DENGRUPO" Key="DenGrupo" Header-Text="" />
                </Columns>
                <Bands>
                    <ig:Band DataMember="DataSource_Miembros" Key="DetalleGrupo" AutoGenerateColumns="false"
                        DataKeyFields="ESTRUC, NOMBRE" ShowHeader="true">
                        <Columns>
                            <ig:BoundDataField DataFieldName="ESTRUC" Key="Estruc" Width="60%" />
                            <ig:BoundDataField DataFieldName="NOMBRE" Key="Nombre" Width="20%" />
                            <ig:BoundDataField DataFieldName="EMAIL" Key="Email" Width="10%" />
                            <ig:BoundDataField DataFieldName="CARGO" Key="Cargo" Width="10%" />
                        </Columns>
                        <Behaviors>
                            <ig:Paging PagerAppearance="Bottom" PageSize="10" Enabled="true" />
                        </Behaviors>
                    </ig:Band>
                </Bands>
            </ig:WebHierarchicalDataGrid>  
        </div>      
    </div>
</asp:Content>