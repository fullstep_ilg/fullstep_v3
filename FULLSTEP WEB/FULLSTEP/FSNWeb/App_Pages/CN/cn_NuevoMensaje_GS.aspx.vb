﻿Public Class cn_NuevoMensaje_GS
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "valoresProcesoCompra") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "valoresProcesoCompra", _
                            "<script>var proceAnio = " & Request.QueryString("proceanio") & _
                            ";proceMat ='" & Request.QueryString("procemat") & _
                            "';proceCod=" & Request.QueryString("procecod") & "</script>")
        End If
    End Sub
End Class