﻿//CN_NUEVOMENSAJE_GS_INIT
var anio, gmn1, cod, item;
var CKEDITOR_BASEPATH = ruta + 'ckeditor/';
var usuario;
var MessageLoaded = false;
$(document).ready(function () {
    $.when($.ajax({
        type: "POST",
        url: rutaFS + '_Common/App_Services/User.asmx/Get_Logged_User',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true
    })).done(function (msg) {
        usuario = msg.d;
        if (usuario != null && usuario.AccesoCN) Cargar_Opciones_CN();
    });
});
function Cargar_Opciones_CN() {           
    $.get(rutaFS + 'CN/html/_nuevo_mensaje.htm', function (menuNuevoMensaje) {
        menuNuevoMensaje = menuNuevoMensaje.replace(/src="/gi, 'src="' + ruta);
        $('#divNuevosMensajes').prepend(menuNuevoMensaje);
        $.each($('#divNuevosMensajes [id]'), function () {
            $(this).attr('id', $(this).attr('id') + 'Master');
        });
        EstablecerTextosMenuMensajeNuevo();
        $('#divNuevoMensajeParaMaster').removeAttr('new');                    
        $('#txtNuevoMensajeTituloMaster').autoGrow();                    

        var idioma;
        idioma = usuario.RefCultural.substring(0, 2);
        $('#txtEventoFechaMaster').datepicker({
            showOn: 'both',
            buttonImage: ruta + 'Images/colorcalendar.png',
            buttonImageOnly: true,
            buttonText: '',
            dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
            showAnim: 'slideDown'
        });
        $.datepicker.setDefaults($.datepicker.regional[idioma]);
        $('#NuevoTemaMaster').addClass('ItemNuevoMensajeSeleccionado');

        if (usuario.CNPermisoCrearMensajesUrgentes) {
            $('#NuevoMensajeUrgenteMaster').show();
        }

        $("#formFileupload").attr("action", rutaFS + 'CN/FileTransferHandler.ashx');                    
        $.when($.get(rutaFS + 'CN/html/_adjuntos.tmpl.htm', function (templates) {
            $('body').append(templates);
        })).done(function () {
            $('#fileupload').fileupload();
            $('#fileupload').show();
            $('#fileupload .fileinput-button').css('right', '');
            $('#fileupload .fileinput-button').css('left', -10000);
            $('#fileupload .fileinput-button').css('top', -10000);
            $('#fileupload .fileinput-button').live('mouseleave', function () {
                $('#fileupload .fileinput-button').css('left', -10000);
                $('#fileupload .fileinput-button').css('top', -10000);
                $('.Seleccionable').removeClass('Seleccionable');
            });
        });
        $.get(rutaFS + 'CN/html/_entidades.tmpl.htm', function (templates) {
            $('body').append(templates);                        
            MostrarFormularioNuevoMensaje(true, true, 'NuevoTemaMaster', 0, false);
        });
    });  
}
function onCKEditorComplete() {
    anio = proceAnio; gmn1 = proceMat; cod = proceCod;
    var editor = CKEDITOR.instances['CKENuevoMensajeMaster'];
    if (!MessageLoaded) {
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'CN/App_Services/CN.asmx/Obtener_DatosAdicionales_Mensaje',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ idMensaje: null, idCategoria: null }),
            dataType: "json",
            async: false
        })).done(function (msg) {
            var data = msg.d;
            if (data[0].Id != 0) {
                $('#tvNuevoMensajeCategoriasMaster').tree('selectNodeValue', data[0].Id);
                SeleccionarCategoria('Master');

                $.when($.ajax({
                    type: "POST",
                    url: rutaFS + 'CN/App_Services/CN.asmx/Comprobar_Buscador_ProceCompra',
                    data: JSON.stringify({ Anio: anio, GMN1: gmn1, Cod: cod }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true
                })).done(function (msg) {
                    if (msg.d) {
                        item = {
                            identificador: msg.d.value,
                            texto: msg.d.text
                        }
                        $('#txtNuevoMensajeTituloMaster').val(Textos[52] + ' ' + item.texto);
                        $('#txtNuevoMensajeTituloMaster').removeClass('textoDefecto');

                        var editor = CKEDITOR.instances['CKENuevoMensajeMaster'];

                        editor.insertHtml('<a type="entidad" href="PC#' + item.identificador + '">' + $('#entidadProcesoCompra').tmpl(item).text() + '</a>&nbsp;');
                    } else { return false; }
                    Control_Panel_Para('divParaProceComprasMaster', 'Master');
                    MessageLoaded = true;
                });
            }
        });
    }   
}
function SelectProceComprasDefaultOptions() {
    $('#cboParaProceComprasAnyoMaster').fsCombo('selectValue', anio);
    $('#cboParaProceComprasGMN1Master').fsCombo('selectValue', gmn1);
    $('#cboParaProceComprasCodDenMaster').fsCombo('selectValue', cod);
}
function closeWindowForm() {
    window.open('', '_self', '');
    window.close();
}