﻿//CN_MURO_UI.JS
$(window).scroll(function () {
    FijarOpcionesNuevoMensajeTrasScroll();
    FijarMenuTrasScroll();
    if ($('#popup').is(':visible')) {
        CentrarPopUp($('#popup'));
    }   
});
$('#divSiguientes').live('click', function () {
    var tipo = $('#divMuro').attr('tipoMensajes');
    var pagina = parseInt($('#divSiguientes').attr('pagina')) + 1;
    var grupo = 0; var categoria = 0; var megusta = false;
    $('#divSiguientes').attr('pagina', pagina);
    $('#divMuro').cn_ObtenerMensajesUsuario({
        WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/ObtenerMensajesUsuario',
        tipo: tipo,
        grupo: grupo,
        categoria: categoria,
        megusta: megusta,
        pagina: pagina,
        historico: false,
        discrepancias:false
    });
});
$('#divHistorico').live('click', function () {
    var tipo = $('#divMuro').attr('tipoMensajes');
    var pagina = parseInt($('#divHistorico').attr('pagina')) + 1;
    $('#divHistorico').attr('pagina', pagina);
    var grupo = 0; var categoria = 0; var megusta = false;
    $('#divMuro').cn_ObtenerMensajesUsuario({
        WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/ObtenerMensajesUsuario',
        tipo: tipo,
        grupo: grupo,
        categoria: categoria,
        megusta: megusta,
        pagina: pagina,
        historico: true,
        discrepancias:false
    });
});
$('#divOcultos').live('click', function () {
    $('[id^=msg_]').show();
    $('#divOcultos').hide();
});
$('#divNotificaciones').live('click', function () {
    CargarMensajes(0, 0, 0, false, 1,false);
    $('#divNotificaciones').hide();
});
$('#imgBuscador').live('click', function () {    
    Obtener_ResultadosBusqueda();
});
$('#lblTipoOrdenacionFecha').live('click', function () {
    if (!$(this).hasClass('Link')) return false;    
    $(this).removeClass('Link');
    $(this).removeClass('Subrayado');
    $('#lblTipoOrdenacionCoincidencias').addClass('Link');
    $('#lblTipoOrdenacionCoincidencias').addClass('Subrayado');
    Obtener_ResultadosBusqueda();
});
$('#lblTipoOrdenacionCoincidencias').live('click', function () {
    if (!$(this).hasClass('Link')) return false;    
    $(this).removeClass('Link');
    $(this).removeClass('Subrayado');
    $('#lblTipoOrdenacionFecha').addClass('Link');
    $('#lblTipoOrdenacionFecha').addClass('Subrayado');
    Obtener_ResultadosBusqueda();
});
var resultados;
$('#txtBuscador').live(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
    var code;
    if (event.keyCode) code = event.keyCode;
    else if (event.which) code = event.which;

    switch (code) {
        case KEY.RETURN:
            Obtener_ResultadosBusqueda();
            return false;
            break;
        default:
            break;
    }
});
$('#lnkAyudaBuscador').live('click', function () {
    if ($('#popupAyudaBuscador').length == 0) {
        $.get(rutaFS + 'CN/html/_PopUp_AyudaBuscador.htm', function (ayudaBuscador) {
            $('#Contenido').append(ayudaBuscador);

            $('#lblAyudaBuscador').text(Textos[108]);

            $.when($.ajax({
                type: "POST",
                url: rutaFS + 'CN/App_Services/CN.asmx/AyudaBuscador',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true
            })).done(function (msg) {
                $('#tituloAyudaBuscador').tmpl(msg.d).appendTo($('#tblAyudaBuscador'));
                $('#infoAyudaBuscador').tmpl(msg.d.children).appendTo($('#tblAyudaBuscador'));

                CentrarPopUp($('#popupAyudaBuscador'));
                $('#popupFondo').css('height', $(document).height());
                $('#popupFondo').show();
                $('#popupAyudaBuscador').show();
            });

            $('#btnCerrarPopUpAyudaBuscador').live('click', function () {
                $('#popupFondo').hide();
                $('#popupAyudaBuscador').hide();
            });
        });
    } else {
        CentrarPopUp($('#popupAyudaBuscador'));
        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        $('#popupAyudaBuscador').show();
    }
});
function FijarMenuTrasScroll() {
    if ($(window).scrollTop() > MenuTop) {
        $('#divMenu').addClass('divFijarArribaMenu');
        $('.divMuro').addClass('divPosicionarTrasFijacion');
        $('#masterBlancoCN #divMuro').width('80%');
        contenidoHeigth = $(window).height() - $('#divLoggedUser').height();
        $('#divSeccionesMenu').height(contenidoHeigth);
    } else {
        $('#divMenu').removeClass('divFijarArribaMenu');
        $('.divMuro').removeClass('divPosicionarTrasFijacion');
        $('#masterBlancoCN #divMuro').width('100%');
        contenidoHeigth = $(window).height() - $('#divSeccionesMenu').offset().top + $(window).scrollTop();
        $('#divSeccionesMenu').height(contenidoHeigth);
    }
}
function FijarOpcionesNuevoMensajeTrasScroll() {
    if ($(window).scrollTop() > MenuTop) {
        $('#divNuevosMensajes').css({ 'width': $('#divNuevosMensajes').width() });
        $('#divNuevosMensajes').addClass('divFijarArribaNuevoMensaje');
        if ($('#divNuevoMensaje').is(':visible')) {
            $('#divNuevosMensajes').addClass('divNuevoMensajeAbierto');
            $('#divNuevosMensajes').css({ 'margin-top': MenuTop });
        } else {
            $('#divNuevosMensajes').addClass('divNuevoMensajeCerrado');
        };
        if ($('#divCabeceraBusqueda').is(':visible')) {
            $('#divCabeceraBusqueda').css({ 'width': $('#divCabeceraBusqueda').width() });
            $('#divCabeceraBusqueda').addClass('divFijarArribaNuevoMensaje');
            $('#divCabeceraBusqueda').addClass('divNuevoMensajeCerrado');
            $('#divMuroResultadosBusqueda').css({ 'margin-top': $('#divCabeceraBusqueda').height(), 'margin-left': '5px' });
        } else $('#divMuro').css({ 'margin-top': $('#divNuevosMensajes').height(), 'margin-left': '5px' });
        
    } else {
        $('#divNuevosMensajes').removeClass('divFijarArribaNuevoMensaje');
        $('#divNuevosMensajes').removeClass('divNuevoMensajeCerrado');
        $('#divNuevosMensajes').removeClass('divNuevoMensajeAbierto');
        $('#divNuevosMensajes').css({ 'position': 'relative' });
        $('#divNuevosMensajes').css({ 'margin-top': '' });
        $('#divCabeceraBusqueda').removeClass('divFijarArribaNuevoMensaje');
        $('#divCabeceraBusqueda').removeClass('divNuevoMensajeCerrado');
        $('#divCabeceraBusqueda').removeClass('divNuevoMensajeAbierto');
        $('#divCabeceraBusqueda').css({ 'position': 'relative' });
        $('#divCabeceraBusqueda').css({ 'margin-top': '' });
        $('#divMuro').css({ 'margin-top': 0, 'margin-left': '0px' });
    }
}