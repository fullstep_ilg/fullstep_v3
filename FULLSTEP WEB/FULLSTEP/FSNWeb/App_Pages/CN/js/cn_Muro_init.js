﻿//CN_MURO_INIT.JS
//===========================================
//CODIGO QUE SE EJECUTA TRAS CARGAR LA PAGINA
//===========================================
var paginaMuroUsuario = true;
var MenuTop;
$(document).ready(function () {
    if (typeof (EnBlancoMaster) !== 'undefined' && EnBlancoMaster) $('body').addClass('fondoMasterEnBlanco');
    $('div.Pie').closest('tr').hide();
    $('#imgCargando').attr('src', ruta + 'images/CargandoMuro.gif');

    $.get(rutaFS + 'CN/html/_menu_cn.htm', function (menu) {
        menu = menu.replace(/src="/gi, 'src="' + ruta);
        $('#divMenu').append(menu);
        $('#divCargando').show();
        CargarMenu();
        MenuTop = $('#divMenu').offset().top;

        if (usuario.CNPermisoCrearMensajes) {
            $.get(rutaFS + 'CN/html/_nuevo_mensaje.htm', function (menuNuevoMensaje) {
                menuNuevoMensaje = menuNuevoMensaje.replace(/src="/gi, 'src="' + ruta);
                $('#divNuevosMensajes').prepend(menuNuevoMensaje);
                EstablecerTextosMenuMensajeNuevo();
                $('#txtNuevoMensajeTitulo').autoGrow();
                var idioma;
                idioma = usuario.RefCultural.substring(0, 2);
                $('#txtEventoFecha').datepicker({
                    showOn: 'both',
                    buttonImage: ruta + 'images/colorcalendar.png',
                    buttonImageOnly: true,
                    buttonText: '',
                    dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
                    showAnim: 'slideDown'
                });
                $.datepicker.setDefaults($.datepicker.regional[UsuLanguageTag]);
                $('#NuevoTema').addClass('ItemNuevoMensajeSeleccionado');

                if (usuario.CNPermisoCrearMensajesUrgentes) {
                    $('#NuevoMensajeUrgente').show();
                }
            });
        } else {
            $('#divNuevosMensajes').remove();
        }

        //=========================
        //  CARGAR MENSAJES USUARIO-MURO
        //=========================    
        // Carga de los templates
        $.when($.get(rutaFS + 'CN/html/_mensajes.tmpl.htm', function (templates) {
            $('body').append(templates);
        })).done(function () {
            var tipo, grupo, categoria, megusta, pagina, paginaHistorico, scrollpagina;
            tipo = 0; grupo = 0; categoria = 0; megusta = false; pagina = 1; paginaHistorico = 0; scrollpagina = 0;

            if (window.location.search != '') {
                var values = window.location.search.substring(1).split('&');
                var queryString = {};
                var value;

                $.each(values, function () {
                    value = this.split('=')[1];
                    switch (this.split('=')[0]) {
                        case 'tipoMensajes':
                            tipo = value;
                            break;
                        case 'pagina':
                            pagina = value;
                            break;
                        case 'paginaHistorico':
                            paginaHistorico = value;
                            break;
                        case 'scrollpagina':
                            scrollpagina = value;
                            break;
                    }
                });
            }

            $('#divMuro').attr('tipoMensajes', tipo);
            $('#divSiguientes').attr('pagina', pagina);
            $('#divHistorico').attr('pagina', paginaHistorico);

            $('.MenuItemMenuOpcionSeleccionado').removeClass('MenuItemMenuOpcionSeleccionado');
            switch (parseInt(tipo)) {
                case 1:
                    $('#OptionTemas').addClass('MenuItemMenuOpcionSeleccionado');
                    break;
                case 2:
                    $('#OptionEventos').addClass('MenuItemMenuOpcionSeleccionado');
                    break;
                case 3:
                    $('#OptionNoticias').addClass('MenuItemMenuOpcionSeleccionado');
                    break;
                case 4:
                    $('#OptionPreguntas').addClass('MenuItemMenuOpcionSeleccionado');
                    break;
                case 5:
                    $('#OptionMensajesUrgentes').addClass('MenuItemMenuOpcionSeleccionado');
                    break;
                case 6:
                    $('#OptionMeGusta').addClass('MenuItemMenuOpcionSeleccionado');
                    megusta = true;
                    break;
                default:
                    $('#OptionMiContenido').addClass('MenuItemMenuOpcionSeleccionado');
                    break;
            }
            for (i = 1; i <= pagina; i++) {
                CargarMensajes(tipo, grupo, categoria, megusta, pagina, false, scrollpagina);
            }
        });
    });
});