﻿//CN_MENU.JS
var contenidoHeigth;
var tipo, grupo, categoria, megusta;
function CargarMenu() {
    if (typeof (usuario) == 'undefined') {
        $.when($.ajax({
            type: "POST",
            url: rutaFS + '_Common/App_Services/User.asmx/Get_Logged_User',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false
        })).done(function (msg) {
            usuario = msg.d;
            CargarMenu();
            if (usuario != null && usuario.AccesoCN) Cargar_Opciones_CN();
        });
        return false;
    } else {
        $('#divMenu #imgUsuImagen').one('load', function () {
            $('#divNombreUsuario').tmpl().appendTo('#divLoggedUser');
            $('#lblUsuNombre').text(usuario.Nombre);
        });
        $('#divMenu #imgUsuImagen').attr('src', rutaFS + 'CN/Thumbnail.ashx?t=0&d=' + new Date());               

        $('#divUsuImagen,#divUsuNombre').live('click', function () {
            TempCargando();            
            window.location.href = rutaFS + 'cn/cn_UserProfile.aspx?' + ((typeof (EnBlancoMaster) !== 'undefined' && EnBlancoMaster) ? 'desdeGS=1' : '');            
        });

        $('#lblGruposTitulo').text(Textos[9]);
        $('#lblGrupos').text(Textos[10]);
        $('#Grupos').attr('title', Textos[10]);
        $('#lblAdminGrupos').text(Textos[11]);
        $('#AdminGrupos').attr('title', Textos[11]);
        $('#divGrupos').show();
        $('#lblCategoriasTitulo').text(Textos[12]);
        $('#lblCategorias').text(Textos[13]);
        $('#Categorias').attr('title', Textos[13]);
        $('#lblAdminCategorias').text(Textos[14]);
        $('#AdminCategorias').attr('title', Textos[14]);
        $('#divCategorias').show();
        $('#lblMensajesTitulo').text(Textos[1]);
        $('#lblMiContenido').text(Textos[2]);
        $('#MiContenido').attr('title', Textos[2]);
        $('#lblTemas').text(Textos[3]);
        $('#Temas').attr('title', Textos[3]);
        $('#lblEventos').text(Textos[4]);
        $('#Eventos').attr('title', Textos[4]);
        $('#lblNoticias').text(Textos[5]);
        $('#Noticias').attr('title', Textos[5]);
        $('#lblPreguntas').text(Textos[6]);
        $('#Preguntas').attr('title', Textos[6]);
        $('#lblMensajesUrgentes').text(Textos[7]);
        $('#MensajesUrgentes').attr('title', Textos[7]);
        $('#lblMeGusta').text(Textos[8]);
        $('#MeGusta').attr('title', Textos[8]);
        $('#divMensajes').show();

        $('#OptionGrupos').live('click', function () {
            $(this).toggleClass('MenuItemExpand').toggleClass('MenuItemCollapse');
            $('#tvGruposUsuarioItem').toggle();
        });

        $('#OptionCategorias').live('click', function () {
            $(this).toggleClass('MenuItemExpand').toggleClass('MenuItemCollapse');
            $('#tvCategoriasUsuarioItem').toggle();
        });

        //============================
        //  CARGAR GRUPOS DEL USUARIO
        //============================
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'CN/App_Services/CN.asmx/GruposCorporativos_Permiso_EmitirMensajeUsuario',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false
        })).done(function (msg) {
            $.each(msg.d, function () {
                $('#ulGruposUsuario').append('<li id="OptionTipoGrupo_' + this.value + '" class="MenuItemMenuOpcion" value="' + this.value + '">' + this.text + '</li>');
            })
        });

        if (usuario.CNPermisoAdministrarGrupos) {
            $('#AdminGrupos').show();
        } else {
            $('#AdminGrupos').remove();
        }

        //===============================
        //  CARGAR CATEGORIAS DEL USUARIO
        //===============================    
        Cargar_Categorias_MensajesUsuario('#tvCategoriasUsuario');
        if (usuario.CNPermisoAdministrarCategorias) {
            $('#AdminCategorias').show();
        } else {
            $('#AdminCategorias').remove();
        }
        $('.MenuItemMenuOpcion').live('click', function () {            
            grupo = 0; categoria = 0; megusta = false;
            $('.MenuItemMenuOpcion').removeClass('MenuItemMenuOpcionSeleccionado');
            switch (this.id.replace('Option', '')) {
                case "MiContenido":
                    tipo = 0;
                    break;
                case "Temas":
                    tipo = 1;
                    break;
                case "Eventos":
                    tipo = 2;
                    break;
                case "Noticias":
                    tipo = 3;
                    break;
                case "Preguntas":
                    tipo = 4;
                    break;
                case "MensajesUrgentes":
                    tipo = 5;
                    break;
                case "MeGusta":
                    tipo = 6;
                    megusta = true;
                    break;
                case "AdminCategorias":
                    TempCargando();
                    window.location.href = rutaFS + 'CN/cn_Categorias.aspx?'+ ((typeof (EnBlancoMaster) !== 'undefined' && EnBlancoMaster)?'desdeGS=1':'');                    
                    break;
                case "AdminGrupos":
                    TempCargando();
                    window.location.href = rutaFS + 'cn/cn_Grupos.aspx?' + ((typeof (EnBlancoMaster) !== 'undefined' && EnBlancoMaster) ? 'desdeGS=1' : '');                    
                    break;
                case "Grupos":
                case "Categorias":
                default:
                    switch (this.id.replace('Option', '').split("_")[0]) {
                        case "TipoGrupo":
                            tipo = 0;
                            grupo = $(this).attr('value');
                            break;
                        default:
                            break;
                    }
                    break;
            }
            if (tipo != undefined) {
                $(this).addClass('MenuItemMenuOpcionSeleccionado');
                $('#divResultadosBusqueda').hide();
                $('#divPrincipal').show();
                if (typeof (paginaMuroUsuario) == 'undefined') {
                    document.location.href = rutaFS + 'CN/cn_MuroUsuario.aspx?tipoMensajes=' + tipo + '&pagina=1&scrollpagina=0' + ((typeof (EnBlancoMaster) !== 'undefined' && EnBlancoMaster) ? '&desdeGS=1' : '');
                } else {
                    CargarMensajes(tipo, grupo, categoria, megusta, 1, false);
                }
            }
        });
        $("#tvCategoriasUsuario .treeTipo0,#tvCategoriasUsuario .treeTipo2").live('click', function () {
            categoria = $(this).attr('itemValue');
            if (paginaMuroUsuario == undefined) {
                document.location.href = "cn_MuroUsuario.aspx?tipoMensajes=0&pagina=1&scrollpagina=0" + ((typeof (EnBlancoMaster) !== 'undefined' && EnBlancoMaster) ? '&desdeGS=1' : '');
            } else {
                $('#divMuro ul').remove();
                CargarMensajes(0, 0, categoria, false, 1, false);
            }
        });        
        contenidoHeigth = $(window).height() - $('#divSeccionesMenu').offset().top;
        $('#divSeccionesMenu').height(contenidoHeigth);
    }
}
function Obtener_Contadores_Notificaciones() {
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'CN/App_Services/CN.asmx/Comprobar_Notificaciones',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true
    })).done(function (msg) {
        $('#lnkNotificacionesCN').hide();     
        $('#AlertNotificacionCNMiContenido').hide();
        $('#AlertNotificacionCNTemas').hide();
        $('#AlertNotificacionCNEventos').hide();
        $('#AlertNotificacionCNNoticias').hide();
        $('#AlertNotificacionCNPreguntas').hide();
        $.each(msg.d, function () {
            switch (this.value) {
                case "0":
                    $('#NumeroAlertNotificacionCNMiContenido').text(parseInt(this.text));
                    $('#AlertNotificacionCNMiContenido').show();
                    break;
                case "1":
                    $('#NumeroAlertNotificacionCNTemas').text(parseInt(this.text));
                    $('#AlertNotificacionCNTemas').show();
                    break;
                case "2":
                    $('#NumeroAlertNotificacionCNEventos').text(parseInt(this.text));
                    $('#AlertNotificacionCNEventos').show();
                    break;
                case "3":
                    $('#NumeroAlertNotificacionCNNoticias').text(parseInt(this.text));
                    $('#AlertNotificacionCNNoticias').show();
                    break;
                case "4":
                    $('#NumeroAlertNotificacionCNPreguntas').text(parseInt(this.text));
                    $('#AlertNotificacionCNPreguntas').show();
                    break;
            }
        })
    });
}
function Cargar_Categorias_MensajesUsuario(tree) {
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'CN/App_Services/CN.asmx/Cargar_Categorias_MensajesUsuario',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
    })).done(function (msg) {
        var data = msg.d;
        $(tree).tree({
            data: data,
            autoOpen: true,
            selectable: true
        });
    });
}