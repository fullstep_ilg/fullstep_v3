﻿$(document).ready(function () {
    if (typeof (EnBlancoMaster) !== 'undefined' && EnBlancoMaster) $('body').addClass('fondoMasterEnBlanco');
    $('#imgNotificaciones').attr('src', ruta + 'images/Notificaciones.png');
    $('#imgConfiguraciones').attr('src', ruta + 'images/configuracion.png');
    $('#imgGrupos').attr('src', ruta + 'images/GruposCN.png');
    $('#divCargando').hide();
    EstablecerTextosProfile();
    $.get(rutaFS + 'CN/html/_menu_cn.htm', function (menu) {
        menu = menu.replace(/src="/gi, 'src="' + ruta);
        $('#divMenu').append(menu);
        CargarMenu();
        $('#divPrincipal').show();
    });
    $.when($.get(rutaFS + 'CN/html/_adjuntos.tmpl.htm', function (templates) {
        $('body').append(templates);
    })).done(function () {
        $('#fileupload').fileupload({ removePrevious: true, acceptFileTypes: /(\.|\/)(gif|jpe?g|png|tiff|bmp|ico)$/i });
        $('#fileupload').show();
        $('#fileupload .fileinput-button').css('right', '');
        $('#fileupload .fileinput-button').css('left', -10000);
        $('#fileupload .fileinput-button').css('top', -10000);
        $('#fileupload .fileinput-button').live('mouseleave', function () {
            $('#fileupload .fileinput-button').css('left', -10000);
            $('#fileupload .fileinput-button').css('top', -10000)
            $('.Seleccionable').removeClass('Seleccionable');
        });
    });
});
function EstablecerTextosProfile() {
    $('#lblUserProfileAceptar').text(TextosUser[16]);
    $('#btnAceptarUserProfile').attr('title', TextosUser[16]);
    $('#lblUserProfileCancelar').text(TextosUser[17]);
    $('#btnCancelarUserProfile').attr('title', TextosUser[17]);
}