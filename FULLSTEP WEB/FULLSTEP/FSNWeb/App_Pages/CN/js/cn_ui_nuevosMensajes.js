﻿//CN_UI_NUEVOSMENSAJES.JS
//=========================
//  MENU NUEVO MENSAJE
//=========================
var timeoutNuevoMensaje;
var CargarEntidadColaboracion = true;
var CrearDivParaMensaje = true;
var tipoAux, MasterAux, IdMenAux;
var guardando = false;
$('[id^=btnAceptarNuevoMensaje]').live('click', function () {
    var Master = ($(this).attr('id').indexOf('Master') >= 0 ? 'Master' : '');
    var tipo = $('.ItemNuevoMensajeSeleccionado').attr('id');
    if (ComprobarDatosMensajeNuevo(tipo, Master)) {
        GuardarMensajeBD(tipo, Master,$(this).attr('IdMensaje'));
    }
    $('#divNuevoMensajePara' + Master).attr('new', '');
});
$('[id^=btnCancelarNuevoMensaje]').live('click', function (event) {
    var Master = ($(this).attr('id').indexOf('Master') >= 0 ? true : false);
    MostrarFormularioNuevoMensaje(false, Master);
    $('#divNuevoMensajePara').attr('new', '');
    if ($.isFunction(window.closeWindowForm)) {
        closeWindowForm();
    }
    $('li.ItemNuevoMensajeSeleccionado').removeClass('ItemNuevoMensajeSeleccionado');
    $('#NuevoTema').addClass('ItemNuevoMensajeSeleccionado');
    return false;
});
$('#txtNuevoMensajeDefectoTitulo').live('focus', function () {
    var Tipo = $('li.ItemNuevoMensajeSeleccionado').attr('id');
    MostrarFormularioNuevoMensaje(true, false, Tipo, 0, true);
});
$('li.ItemNuevoMensaje').live('click', function (event) {
    $('li.ItemNuevoMensajeSeleccionado').removeClass('ItemNuevoMensajeSeleccionado');
    $(this).addClass('ItemNuevoMensajeSeleccionado');
    var Tipo = $(this).attr('id');
    var MasterPage = (Tipo.indexOf('Master') >= 0 ? true : false);
    MostrarFormularioNuevoMensaje(true, MasterPage, Tipo, 0);
    return false;
});
$('[id^=txtNuevoMensajeTitulo]').live('focus', function () {
    if ($(this).hasClass('textoDefecto')) $(this).val('');
    $(this).removeClass('textoDefecto');
    $(this).addClass('TextoOscuro');    
});
$('[id^=txtEventoDonde]').live('focus', function () {
    $(this).removeClass('textoDefecto');
    $(this).addClass('TextoOscuro');
    $(this).val('');
});
$('[id^=txtEventoHora]').live('focus',function () {
    if ($(this).hasClass('textoDefecto')) {
        $(this).removeClass('textoDefecto');
        $(this).addClass('TextoOscuro');
    }
    $(this).select();
});
$('[id^=NuevoMensajeEnlace]').live('mouseenter mouseleave', function () {
    $(this).toggleClass('Seleccionable');
});
$('[id^=NuevoMensajeEtiqueta]').live('click', function () {
    $('#divNuevoMensajeNuevasEtiquetas').append($('#divNuevoMensajeNuevaEtiqueta').text());
    $('#divNuevoMensajeEtiquetas').show();
});
$('[id^=btnAgregarEtiqueta]').live('click', function () {
    $('#divNuevoMensajeNuevasEtiquetas').append($('#divNuevoMensajeNuevaEtiqueta').text());
});
$('[id^=divSelCategoria]').live('click', function () {
    var Master = ($(this).attr('id').indexOf('Master') >= 0 ? 'Master' : '');
    if (!($('#divNuevoMensajeCategorias' + Master).is(':visible'))) {
        $('#divNuevoMensajeCategorias' + Master).show();
    } else {
        $('#divNuevoMensajeCategorias' + Master).hide();
    }
});
$('[id^=txtSelCategoria]').live('focus', function () {
    var Master = ($(this).attr('id').indexOf('Master') >= 0 ? 'Master' : '');
    $('#divNuevoMensajeCategorias' + Master).show();
});
$('[id^=txtSelCategoria]').live(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
    var Master = ($(this).attr('id').indexOf('Master') >= 0 ? 'Master' : '');
    var code;
    if (event.keyCode) code = event.keyCode;
    else if (event.which) code = event.which;

    switch (code) {
        case KEY.RETURN:
            SeleccionarCategoria(Master);
            break;
        case KEY.DEL:
        case KEY.BACKSPACE:
            $(this).attr('itemValue', '');
            break;
        default:
            EncontrarItemTreeView('tvNuevoMensajeCategorias' + Master, 'txtSelCategoria' + Master, code);
            break;
    }
});
$('[id^=tvNuevoMensajeCategorias] .treeTipo0,[id^=tvNuevoMensajeCategorias] .treeTipo2').live('click', function () {
    var Master = ($(this).attr('id').split('-')[0].indexOf('Master') >= 0 ? 'Master' : '');
    SeleccionarCategoria(Master);
});
$('[id^=txtParaProveMatGS]').live('focus', function () {
    var Master = ($(this).attr('id').indexOf('Master') >= 0 ? 'Master' : '');
    $('#tvParaProveMatGS' + Master).show();
    if ($('#tvParaProveMatGS' + Master).attr('cargar') == '') {
        CargarMaterialesGS('tvParaProveMatGS' + Master, 'tvParaEstrucComprasMatGS' + Master, 'txtParaProveMatGS' + Master, 'txtParaEstrucComprasMatGS' + Master);
    }
});
$('[id^=txtParaProveMatGS]').live(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
    var Master = ($(this).attr('id').indexOf('Master') >= 0 ? 'Master' : '');
    var code;
    if (event.keyCode) code = event.keyCode;
    else if (event.which) code = event.which;
    
    switch (code) {
        case KEY.RETURN:
            SeleccionarProveMatGS('tvParaProveMatGS' + Master, 'txtParaProveMatGS' + Master, 'txtParaEstrucComprasMatGS' + Master);
            break;
        case KEY.DEL:
        case KEY.BACKSPACE:
            $('#txtParaProveMatGS' + Master).attr('itemValue', '');
            $('#txtParaEstrucComprasMatGS' + Master).attr('itemValue', '');
            break;
        default:
            EncontrarItemTreeView('tvParaProveMatGS' + Master, 'txtParaProveMatGS' + Master, code);
            break;
    }
});
$('[id^=txtParaEstrucComprasMatGS]').live('focus',function () {
    var Master = ($(this).attr('id').indexOf('Master') >= 0 ? 'Master' : '');
    $('#tvParaEstrucComprasMatGS' + Master).show();
    if ($('#tvParaEstrucComprasMatGS' + Master).attr('cargar') == '') {
        CargarMaterialesGS('tvParaEstrucComprasMatGS' + Master, 'tvParaProveMatGS' + Master, 'txtParaEstrucComprasMatGS' + Master, 'txtParaProveMatGS' + Master);
    }
});
$('[id^=txtParaEstrucComprasMatGS]').live(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
    var code;
    if (event.keyCode) code = event.keyCode;
    else if (event.which) code = event.which;

    switch (code) {
        case KEY.RETURN:
            SeleccionarProveMatGS('tvParaEstrucComprasMatGS' + Master, 'txtParaEstrucComprasMatGS' + Master, 'txtParaProveMatGS' + Master);
            break;
        case KEY.DEL:
        case KEY.BACKSPACE:
            $('#txtParaEstrucComprasMatGS' + Master).attr('itemValue', '');
            $('#txtParaProveMatGS' + Master).attr('itemValue', '');
            break;
        default:
            EncontrarItemTreeView('tvParaEstrucComprasMatGS' + Master, 'txtParaEstrucComprasMatGS' + Master, code);
            break;
    }
});
$('body').click(function (event) {
    var OcultarCategoria = $(event.target).parents('[id^=divNuevoMensajeCategoria]').length == 0;
    var OcultarPara = ($(event.target).parents('[id^=divNuevoMensajePara]').length == 0 && $(event.target).parents('[id^=divParaDestinatarios]').length == 0);
    var OcultarProveMatGS = $(event.target).parents('[id^=divParaProveMatGS]').length == 0;

    if (OcultarCategoria) {
        $('[id^=divNuevoMensajeCategorias]').hide();
    }

    if (OcultarPara) {
        $('[id^=divOpcionesPara]').hide();
    }
    
    if (OcultarProveMatGS) {
        $('#tvParaProveMatGSMaster').hide();
        $('#tvParaProveMatGS').hide();
    }
});
$('#divNuevoMensajeParaOpciones .ItemNuevoMensajePara,#divNuevoMensajeParaOpcionesMaster .ItemNuevoMensajePara,#divParaOpciones .ItemNuevoMensajePara').live('click', function () {
    var Master = ($(this).attr('id').indexOf('Master') >= 0 ? 'Master' : '');
    var IdNuevaSeleccion = $(this).attr('id');
    Control_Panel_Para(IdNuevaSeleccion, Master);
});
$('.ItemPara').live('click', function () {
    $('.ItemParaSeleccionado').removeClass('ItemParaSeleccionado');
    $(this).addClass('ItemParaSeleccionado');
});
$('[id^=divListaPara]').live('click', function () {
    var Master = ($(this).attr('id').indexOf('Master') >= 0 ? 'Master' : '');
    if (!($('#divOpcionesPara' + Master).is(':visible')) && $('#divListaPara' + Master).children().length == 0) {
        if ($('.ItemNuevoMensajeParaSeleccionado').length !== 0) {
            var IdSeleccionado = $('.ItemNuevoMensajeParaSeleccionado').attr('id').replace('Master', '');
            $('#' + IdSeleccionado + Master).removeClass('ItemNuevoMensajeParaSeleccionado');
            $('#' + IdSeleccionado + 'Panel' + Master).hide();
        }
        $('#divOpcionesPara' + Master).show();
        $('#divParaUON' + Master).addClass('ItemNuevoMensajeParaSeleccionado');
        $('#divParaUONPanel' + Master).show();
        Control_Panel_Para('divParaUON' + Master, Master);
    }
});
$('[id^=divListaPara]').live('focusout',function () {
    $('.ItemParaSeleccionado').removeClass("ItemParaSeleccionado");
});
$('[id^=divListaPara]').live(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
    var Master = ($(this).attr('id').indexOf('Master') >= 0 ? 'Master' : '');
    if ($('#divListaPara' + Master).attr('usu') != '') return false;
    var code;
    if (event.keyCode) code = event.keyCode;
    else if (event.which) code = event.which;

    if (code == KEY.DEL || code == KEY.BACKSPACE) {
        if ($('.ItemParaSeleccionado').length > 0) {
            switch ($('.ItemParaSeleccionado').attr('id').split('-')[0]) {
                case 'itemParaProve':
                    if ($('#divListaPara' + Master + ' [id^=itemParaProve-]').length == 1) {
                        $('.ItemParaProveedor').addClass('Link');
                        $('.ItemParaProveedor span').removeClass('LinkDisabled');

                        $('#divParaProveBuscar' + Master).addClass('Link');
                        $('#divParaProveBuscar' + Master + ' span').removeClass('LinkDisabled');

                        if ($('[id^=itemParaEstrucCompras-MatGS]').length == 0) {
                            $('#txtParaProveMatGS' + Master).removeAttr('disabled');
                            $('#txtParaEstrucComprasMatGS' + Master).removeAttr('disabled');
                        }

                        if ($('#divParaProve' + Master).attr('cargar') != '') {
                            $('#cboParaProveMatQA' + Master).fsCombo('enable');
                        }
                    }
                    break;
                case 'itemParaProceCompras':
                    if ($('.ItemParaSeleccionado').attr('id').split('-')[1] == 'Todos') {
                        if ($('#cboParaProceComprasCodDen' + Master).fsCombo('getSelectedValue') != '') {
                            $('.ItemParaProceCompras').addClass('Link');
                            $('.ItemParaProceCompras span').removeClass('LinkDisabled');
                        }
                    }
                    break;
                case 'itemParaEstrucCompras':
                    if ($('.ItemParaSeleccionado').attr('id').split('-')[1] == 'MatGS') {
                        if ($('[id^=itemParaProve-MatGS]').length == 0) {
                            $('#txtParaProveMatGS' + Master).removeAttr('disabled');
                            $('#txtParaEstrucComprasMatGS' + Master).removeAttr('disabled');
                        }
                        $('.ItemParaEstrucCompras').addClass('Link');
                        $('.ItemParaEstrucCompras span').removeClass('LinkDisabled');
                    }
                    break;
            }
            $('.ItemParaSeleccionado').remove();
        }
    }
    return false;
});
$('[id^=divParaProveBuscar]').live('click', function () {
    var Master = ($(this).attr('id').indexOf('Master') >= 0 ? 'Master' : '');
    if ($(this).hasClass('Link')) {
        var params;
        $('#divParaProveLista' + Master).empty();
        params = { Codigo: $('#txtParaProveBuscarCodigo' + Master).val(), NIF: $('#txtParaProveBuscarNIF' + Master).val(), Denominacion: $('#txtParaProveBuscarDenominacion' + Master).val() };
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'CN/App_Services/CN.asmx/ProveedoresContacto_Permiso_EmitirMensajeUsuario',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(params),
            dataType: "json",
            async: true
        })).done(function (msg) {
            var data = msg.d;
            if (msg.d.length == 0) {
                $('#divParaProveBusquedaProve' + Master).hide();
            } else {
                $('#divParaProveBusquedaProve' + Master).show();
                 
                $.each(data, function () {
                    $('#divParaProveLista' + Master).append('<div id="divitemParaProve-Uno-' + this.value + '" class="ItemParaProveedor Link"><span class="Texto12 Subrayado">' + this.text + '</span></div>')
                });
            }
        });
    }
});

//=========================
//  FUNCIONES NUEVO MENSAJE
//=========================
function EstablecerTextosMenuMensajeNuevo() {
    $('[id^=lblNuevoTema]').text(Textos[15]);
    $('[id^=NuevoTema]').attr('title', Textos[15]);
    $('[id^=lblNuevoEvento]').text(Textos[16]);
    $('[id^=NuevoEvento]').attr('title', Textos[16]);
    $('[id^=lblNuevaNoticia]').text(Textos[17]);
    $('[id^=NuevaNoticia]').attr('title', Textos[17]);
    $('[id^=lblNuevoPregunta]').text(Textos[18]);
    $('[id^=NuevoPregunta]').attr('title', Textos[18]);
    $('[id^=lblNuevoMensajeUrgente]').text(Textos[19]);
    $('[id^=NuevoMensajeUrgente]').attr('title', Textos[19]);
    $('[id^=txtEventoDonde]').val(Textos[81]);
    $('[id^=lblTituloPopUpNuevoMensaje]').text(Textos[0]);
    if ($('#divMuro').length > 0) {        
        $('#txtNuevoMensajeDefectoTitulo').val(Textos[22]);
    }   
}
function MostrarFormularioNuevoMensaje(show, MasterPage, Tipo, IdMensaje, inicializar, contenidoEditor) {
    var Master = (MasterPage ? 'Master' : '');
    $('.ItemNuevoMensajeParaSeleccionado').removeClass('ItemNuevoMensajeParaSeleccionado');
    if (show) {
        $('#' + Tipo).addClass('ItemNuevoMensajeSeleccionado');    

        if ($('#divMuro').length !== 0) {
            MostrarFormularioRespuesta(false, $('#fileupload').attr('mensaje'));
        }
        $('#fileupload').attr('mensaje', '');

        $('#divNuevoMensaje' + Master).show();
        $('#divBotones' + Master).show();

        $('#divNuevoMensajeDefecto' + Master).hide();

        $('#tablaadjuntos').remove();
        $('#divListaAdjuntos' + Master).append('<div class="fileupload-content"><div id="tablaadjuntos" class="files"></div><div class="fileupload-progressbar"></div></div>');

        $('#btnAceptarNuevoMensaje' + Master).attr('IdMensaje', IdMensaje);
        switch (Tipo) {
            case 'NuevoTema' + Master:
                $('#divNuevoMensajeFechaHora' + Master).hide();
                if ($('#txtNuevoMensajeTitulo' + Master).hasClass('textoDefecto')) $('#txtNuevoMensajeTitulo' + Master).val(Textos[80]);
                break;
            case 'NuevoEvento' + Master:
                var today = new Date();
                var day = today.getDate();
                var month = today.getMonth();
                var year = today.getFullYear();

                if (typeof ($('#txtEventoHora' + Master).attr('ready')) == 'undefined') {
                    $('#txtEventoHora' + Master).mask("99:99");
                    $('#txtEventoHora' + Master).attr('ready', '');
                }
                $('#txtEventoFecha' + Master).datepicker('setDate', today);
                $('#txtEventoHora' + Master).val(today.getHours() + ':' + ((today.getMinutes() < 10) ? '0' + today.getMinutes() : today.getMinutes()));

                if ($('#txtNuevoMensajeTitulo' + Master).hasClass('textoDefecto')) $('#txtNuevoMensajeTitulo' + Master).val(Textos[70]);
                $('#divNuevoMensajeFechaHora' + Master).show();
                break;
            case 'NuevaNoticia' + Master:
                if ($('#txtNuevoMensajeTitulo' + Master).hasClass('textoDefecto')) $('#txtNuevoMensajeTitulo' + Master).val(Textos[71]);
                $('#divNuevoMensajeFechaHora' + Master).hide();
                break;
            case 'NuevaPregunta' + Master:
                if ($('#txtNuevoMensajeTitulo' + Master).hasClass('textoDefecto')) $('#txtNuevoMensajeTitulo' + Master).val(Textos[72]);
                $('#divNuevoMensajeFechaHora' + Master).hide();
                break;
            case 'NuevoMensajeUrgente' + Master:
                if ($('#txtNuevoMensajeTitulo' + Master).hasClass('textoDefecto')) $('#txtNuevoMensajeTitulo' + Master).val(Textos[73]);
                $('#divNuevoMensajeFechaHora' + Master).hide();
                break;
        };
        if ($('#divNuevoMensaje' + Master).attr('textos') == '') {
            EstablecerTextosMensajeNuevo();
        };
        Categorias_Permiso_EmitirUsuario('#tvNuevoMensajeCategorias' + Master);
        var editor = CKEDITOR.instances['CKENuevoMensaje' + Master];
        var htmlEditor = '';
        if (editor) {
            htmlEditor = editor.getData();
            editor.destroy(true);
        }
        if (typeof (contenidoEditor) !== 'undefined') htmlEditor = contenidoEditor;
        editor = CKEDITOR.replace('CKENuevoMensaje' + Master, { customConfig: ruta + 'ckeditor/configCN.js',
            on: { instanceReady: function (ev) {
                if (htmlEditor !== '') ev.sender.insertHtml(htmlEditor);
                ImageUploadPlugin(ev.editor.name);
                MostrarCrearMensaje(MasterPage);
                $('#txtNuevoMensajeTitulo' + Master).focus();
                ev.editor.on('paste', function (ev) { CKEditorPaste(ev); });
            },
                mode: function (ev) {
                    if (guardando) {
                        guardando = false;
                        timeoutNuevoMensaje = setTimeout(function () { clearTimeout(timeoutNuevoMensaje); GuardarMensajeBD(tipoAux, MasterAux, IdMenAux); }, 50);
                    }
                }
            }
        });       
        if ($('#btnAceptarNuevoMensaje' + Master).attr('IdMensaje') == '0' && CrearDivParaMensaje) {
            CrearDivParaMensaje = false;
            $('#divNuevoMensajeParaOpciones' + Master).empty();
            $('#divParaDestinatarios' + Master).empty();
            $.get(rutaFS + 'CN/html/_mensaje_para.htm', function (paraOpciones) {
                paraOpciones = paraOpciones.replace(/src="/gi, 'src="' + ruta);
                $('#divNuevoMensajeParaOpciones' + Master).append(paraOpciones);
                $.each($('#divNuevoMensajeParaOpciones' + Master + ' [id]'), function () {
                    $(this).attr('id', $(this).attr('id') + Master);
                });
                EstablecerTextosMensajeNuevo('Para');
                $('#divListaPara' + Master).attr('usu', '');
                if (!usuario.AccesoGS) {
                    $('[id^=divParaProceCompras]').remove();
                    $('[id^=divParaEstrucCompras]').remove();
                    $('[id^=divParaProceComprasPanel]').remove();
                    $('[id^=divParaEstrucComprasPanel]').remove();
                }
                if ($('#divNuevoMensajePara' + Master).attr('new') == '') {
                    if (typeof(categoria)!=='undefined' && categoria !== 0) {
                        $('#tvNuevoMensajeCategorias' + Master).tree('selectNodeValue', categoria);
                        SeleccionarCategoria(Master);
                    }
                    if (!$('#txtSelCategoria' + Master).attr("itemValue") || $('#txtSelCategoria' + Master).attr("itemValue") == '') {
                        $.when($.ajax({
                            type: "POST",
                            url: rutaFS + 'CN/App_Services/CN.asmx/Obtener_DatosAdicionales_Mensaje',
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify({ idMensaje: (IdMensaje == 0 ? null : IdMensaje), idCategoria: null }),
                            dataType: "json",
                            async: false
                        })).done(function (msg) {
                            var data = msg.d;
                            if (data && data[0].Id != 0) {
                                $('#tvNuevoMensajeCategorias' + Master).tree('selectNodeValue', data[0].Id);
                                SeleccionarCategoria(Master);
                                CargarParaAnterior(msg.d[1], Master);
                            }
                        });
                    }
                }
            });
        }
    } else {
        var editor = CKEDITOR.instances['CKENuevoMensaje' + Master];
        if (editor) { editor.destroy(true) };

        CargarEntidadColaboracion = true;
        CrearDivParaMensaje = true;
        $('#btnAceptarNuevoMensaje' + Master).removeAttr('IdMensaje');

        $('#txtNuevoMensajeTitulo' + Master).val('');
        $('#txtNuevoMensajeTitulo' + Master).addClass('textoDefecto');

        $('#txtEventoFecha' + Master).val('');
        $('#txtEventoHora' + Master).val('');
        $('#txtEventoDonde' + Master).val('');
        
        $('#divListaAdjuntos' + Master + ' #tablaadjuntos').empty();

        if ($('#tvNuevoMensajeCategorias' + Master).children().length != 0) {
            var node = $('#tvNuevoMensajeCategorias' + Master).tree('deselectAll');
        }
        $('#tvNuevoMensajeCategorias' + Master + ' .selected').removeClass('selected');        

        $('#txtSelCategoria' + Master).removeAttr("itemValue")
        $('#txtSelCategoria' + Master).val('');

        $('#divListaPara' + Master + ' div').remove();

        $('#divParaUONPanel' + Master).hide();
        $('#tvParaUONTree' + Master + ' .selected').removeClass('selected');
        $('#txtParaUON' + Master).val('');

        $('#divParaProvePanel' + Master).hide();
        $('#divParaProceComprasPanel' + Master).hide();
        $('#divParaEstrucComprasPanel' + Master).hide();
        $('#divParaGruposPanel' + Master).hide();

        if (MasterPage) {
            $('#popupFondo').hide();
            $('#popupNuevoMensaje').hide();
        } else {
            $('#divNuevoMensaje').hide();
            $('#divBotones').hide();

            $('#divNuevoMensajeDefecto').show();
        }
    }

    $('[id^=NuevoMensajeAdjunto]').live('mouseenter', function () {
        $(this).addClass('Seleccionable');
        $('#fileupload .fileinput-button').css('width', $(this).outerWidth());
        $('#fileupload .fileinput-button').css('height', $(this).outerHeight());
        if ($(this).attr('id').indexOf('Master') == 0) {
            $('#fileupload .fileinput-button').css('left', $(this).position().left);
            $('#fileupload .fileinput-button').css('top', $(this).position().top);
        } else {
            $('#fileupload .fileinput-button').css('left', $(this).offset().left);
            $('#fileupload .fileinput-button').css('top', $(this).offset().top);
        }
        $('#fileupload .fileinput-button').css('z-index', 10000);
        $('#fileupload .fileinput-button').css('right', '');
    });

    if (inicializar) {
        $('#txtNuevoMensajeTitulo' + Master).val('');
        setTimeout(function () { $('#txtNuevoMensajeTitulo' + Master).focus() }, 10);
    }
    return false;
}
function MostrarCrearMensaje(MasterPage) {
    if (MasterPage) {
        CentrarPopUpAncho($('#popupNuevoMensaje'));
        $('#popupNuevoMensaje').show();

        $('#divNuevoMensaje').hide();
        $('#divBotones').hide();

        $('#divNuevoMensajeDefecto').show();
    } else {
        if ($('#divNuevoMensaje').is(':visible')) {
            if ($('#divNuevosMensajes').hasClass('divFijarArriba')) {
                $('#divNuevosMensajes').removeClass('divFijarArriba');
                $('#divMuro').css({ 'margin-top': 0 });
                $(window).scrollTop($('#divCabecera').offset().top + 43);
            }
        }
    }
    SCAYTready('CKENuevoMensaje' + (MasterPage ? 'Master' : ''));
    if ($.isFunction(window.onCKEditorComplete)) {
        onCKEditorComplete();
    }
    if (CargarEntidadColaboracion) {
        CargarEntidadColaboracion = false;
        EnlazarEntidadColaboracion();
    }
}
function EnlazarEntidadColaboracion() {
    if (typeof (TipoEntidadColaboracion) != 'undefined') {
        switch (TipoEntidadColaboracion) {
            case 'PC':
                CKEDITOR.plugins.registered.FSProcesoCompra.setSelectedValue(CodigoEntidadColaboracion);
                break;
            case 'SC':
                $('#txtNuevoMensajeTituloMaster').val(TextosCKEditor[5] + ' ' + CodigoEntidadColaboracion.texto);
                $('#txtNuevoMensajeTituloMaster').removeClass('textoDefecto');
                $('#txtNuevoMensajeTituloMaster').addClass('TextoOscuro');
                CKEDITOR.plugins.registered.FSSolicitudCompra.setSelectedValue(CodigoEntidadColaboracion);
                break;
            case 'CO':
                $('#txtNuevoMensajeTituloMaster').val(TextosCKEditor[6] + ' ' + CodigoEntidadColaboracion.texto + ' (' + CodigoEntidadColaboracion.proveedor + ')');
                $('#txtNuevoMensajeTituloMaster').removeClass('textoDefecto');
                $('#txtNuevoMensajeTituloMaster').addClass('TextoOscuro');
                CKEDITOR.plugins.registered.FSContrato.setSelectedValue(CodigoEntidadColaboracion);
                break;
            case 'NC':
                $('#txtNuevoMensajeTituloMaster').val(TextosCKEditor[7] + ' ' + CodigoEntidadColaboracion.identificador + ' - ' + CodigoEntidadColaboracion.texto + ' (' + CodigoEntidadColaboracion.proveedor + ')');
                $('#txtNuevoMensajeTituloMaster').removeClass('textoDefecto');
                $('#txtNuevoMensajeTituloMaster').addClass('TextoOscuro'); 
                CKEDITOR.plugins.registered.FSNoConformidad.setSelectedValue(CodigoEntidadColaboracion);
                break;
            case 'CE':
                $('#txtNuevoMensajeTituloMaster').val(TextosCKEditor[8] + ' ' + CodigoEntidadColaboracion.identificador + ' - ' + CodigoEntidadColaboracion.texto + ' (' + CodigoEntidadColaboracion.proveedor + ')');
                $('#txtNuevoMensajeTituloMaster').removeClass('textoDefecto');
                $('#txtNuevoMensajeTituloMaster').addClass('TextoOscuro');
                CKEDITOR.plugins.registered.FSCertificado.setSelectedValue(CodigoEntidadColaboracion);
                break;
            case 'FP':
                $('#txtNuevoMensajeTituloMaster').val(TextosCKEditor[9] + ' - (' + CodigoEntidadColaboracion.identificador + ') ' + CodigoEntidadColaboracion.proveedor);
                $('#txtNuevoMensajeTituloMaster').removeClass('textoDefecto');
                $('#txtNuevoMensajeTituloMaster').addClass('TextoOscuro');
                CKEDITOR.plugins.registered.FSFichaCalidad.setSelectedValue(CodigoEntidadColaboracion);
                break;
            default:
                break;
        }
    }
}
function Control_Panel_Para(IdNuevaSeleccion, Master) {
    var IdSeleccionado = $('.ItemNuevoMensajeParaSeleccionado').attr('id');
    if (IdSeleccionado) {
        $('#' + IdSeleccionado).removeClass('ItemNuevoMensajeParaSeleccionado');
        $('#' + IdSeleccionado.replace('Master', '') + 'Panel' + Master).hide();
    }

    $('#' + IdNuevaSeleccion).addClass('ItemNuevoMensajeParaSeleccionado');
    $('#' + IdNuevaSeleccion.replace('Master', '') + 'Panel' + Master).show();
    
    $('#divOpcionesPara' + Master).show();
    switch (IdNuevaSeleccion) {
        case 'divParaUON' + Master:
            if ($('#divParaUON' + Master).attr('cargar') == '') {
                $('#txtParaUON' + Master).attr('disabled', true);

                EstablecerTextosMensajeNuevo('ParaUON');

                $.when($.ajax({
                    type: "POST",
                    url: rutaFS + 'CN/App_Services/CN.asmx/UON_Permiso_EmitirMensajeUsuario',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false
                })).done(function (msg) {
                    var data = msg.d;
                    $('#tvParaUONTree' + Master).tree({
                        data: data,
                        autoOpen: true,
                        selectable: false,
                        onOpenParentsDone: function () { setScrollSelected("tvParaUONTree" + Master, 100) }
                    });
                });

                $('#tvParaUONTree' + Master + ' .treeTipo3').live('click', function () {
                    if ($('#divListaPara' + Master + ' #' + $(this).attr("id").replace('tvParaUONTree' + Master, 'itemParaUON')).length == 0) {
                        $('#divListaPara' + Master).prepend('<div class="ItemPara" id="' + $(this).attr('id').replace('tvParaUONTree' + Master, 'itemParaUON') + '">' + $(this).text() + '</div>');
                    }
                });

                $('#txtParaUON' + Master).live(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
                    var code;
                    if (event.keyCode) code = event.keyCode;
                    else if (event.which) code = event.which;
                    switch (true) {
                        case (code == KEY.RETURN):
                            var selectedItem = $('#tvParaUONTree' + Master + ' span.treeSeleccionable.selected');
                            if (selectedItem.length != 0) {
                                if ($('#divListaPara' + Master + ' #' + $(selectedItem).attr("id").replace('tvParaUONTree' + Master, 'itemParaUON')).length == 0) {
                                    $('#divListaPara' + Master).prepend('<div class="ItemPara" id="' + $(selectedItem).attr('id').replace('tvParaUONTree' + Master, 'itemParaUON') + '">' + $(selectedItem).text() + '</div>');
                                };
                            };
                            event.preventDefault();
                            return false;
                            break;
                        case ((code >= 9 && code <= 45) || (code >= 91 && code <= 93) || (code >= 112 && code <= 186)):
                            break;
                        default:
                            EncontrarItemTreeView('tvParaUONTree' + Master, 'txtParaUON' + Master, code);
                            break;
                    }
                });

                $('#txtParaUON' + Master).removeAttr("disabled");
                $('#divParaUON' + Master).removeAttr('cargar');
            }
            ComprobarValoresIniciales('divParaUON' + Master);
            $('#txtParaUON' + Master).focus();
            break;
        case 'divParaProve' + Master:
            if ($('#divParaProve' + Master).attr('cargar') == '') {
                EstablecerTextosMensajeNuevo('ParaProve');

                var optionsProveMatQA = { valueField: 'value', textField: 'text', isDropDown: false, MinimumPrefixLength: 0, itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, adjustWidthToSpace: true, WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_MaterialesQA_Proveedor' };

                $('#cboParaProveMatQA' + Master).fsCombo(optionsProveMatQA);

                $('.ItemParaProveedor').live('click', function () {
                    if ($(this).hasClass('Link')) {
                        var idDiv = (this.id).replace('div', '');
                        if ($('#divListaPara' + Master + ' #' + idDiv).length == 0) {
                            var texto;
                            var UnicoProveedor = false;
                            switch (idDiv.split('-')[1]) {
                                case 'MatGS' + Master:
                                    if ($('#txtParaProveMatGS' + Master).attr('itemValue') && $('#txtParaProveMatGS' + Master).attr('itemValue') != '') {
                                        texto = $(this).find('span').text() + ' ' + $('#txtParaProveMatGS' + Master).val();
                                        idDiv = idDiv + '-' + $('#txtParaProveMatGS' + Master).attr('itemValue');

                                        $('#txtParaEstrucComprasMatGS' + Master).attr('disabled', 'disabled');
                                    } else { return false; }
                                    break;
                                case 'MatQA' + Master:
                                    if ($('#cboParaProveMatQA' + Master).fsCombo('getSelectedValue') == '') {
                                        return false;
                                    } else {
                                        var texto = $(this).find('span').text() + ' ' + $('#cboParaProveMatQA' + Master).fsCombo('getSelectedText'); ;
                                        idDiv = idDiv + '-' + $('#cboParaProveMatQA' + Master).fsCombo('getSelectedValue');
                                    }
                                    break;
                                case 'Uno' + Master:
                                    texto = $(this).find('span').text();
                                    UnicoProveedor = true;
                                    break;
                                default:
                                    texto = $(this).find('span').text();
                                    break;
                            }
                            $('.ItemParaProveedor').removeClass('Link');
                            $('.ItemParaProveedor span').addClass('LinkDisabled');

                            $('#divParaProveBuscar' + Master).removeClass('Link');
                            $('#divParaProveBuscar' + Master + ' span').addClass('LinkDisabled');

                            $('#cboParaProveMatQA' + Master).fsCombo('disable');
                            $('#txtParaProveMatGS' + Master).attr('disabled', 'disabled');

                            if (UnicoProveedor) {
                                $('[id^=divitemParaProve-Uno-]').addClass('Link');
                                $('[id^=divitemParaProve-Uno-] span').removeClass('LinkDisabled');

                                $('#divParaProveBuscar').addClass('Link');
                                $('#divParaProveBuscar span').removeClass('LinkDisabled');
                            }
                            $('#divListaPara' + Master).prepend('<div class="ItemPara" id="' + idDiv.replace('Master', '') + '">' + texto + '</div>');
                        }
                    }
                });
                $('#divParaProve' + Master).removeAttr('cargar');
            }
            ComprobarValoresIniciales('divParaProve' + Master);
            break;
        case 'divParaProceCompras' + Master:
            if ($('#divParaProceCompras' + Master).attr('cargar') == '') {
                EstablecerTextosMensajeNuevo('ParaProceCompras');

                $('.ItemParaProceCompras' + Master).removeClass('Link');
                $('.ItemParaProceCompras' + Master + ' span').addClass('LinkDisabled');
                                
                var optionsAnyo = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_ProceCompras_Anyos' };
                var optionsGMN = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, disableParentEmptyValue: true, fsComboParents: 'cboParaProceComprasAnyo' + Master, itemListWidth: 250, WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_ProceCompras_GMNs' };
                var optionsProcesos = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, disableParentEmptyValue: true, fsComboParents: 'cboParaProceComprasAnyo' + Master + ',cboParaProceComprasGMN1' + Master, adjustWidthToSpace: true, WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_ProceCompras_CodDen' };

                $('#cboParaProceComprasAnyo' + Master).fsCombo(optionsAnyo);
                $('#cboParaProceComprasGMN1' + Master).fsCombo(optionsGMN);
                $('#cboParaProceComprasCodDen' + Master).fsCombo(optionsProcesos);
                $('#cboParaProceComprasCodDen' + Master).fsCombo('onSelectedValueChange', function () { MostrarOpcionesProceCompras(Master) });
                    
                if ($.isFunction(window.SelectProceComprasDefaultOptions)) {
                    SelectProceComprasDefaultOptions();
                } else {
                    $('#cboParaProceComprasAnyo' + Master).fsCombo('selectValue', new Date().getFullYear());
                    if ($('#cboParaProceComprasAnyo' + Master).fsCombo('getSelectedValue') == '') {
                        $('#cboParaProceComprasAnyo' + Master).fsCombo('selectItem', $('#cboParaProceComprasAnyo' + Master).fsCombo('itemsCount'));
                        $('#cboParaProceComprasGMN1' + Master).fsCombo('selectItem', 2);
                    }
                }

                $('.ItemParaProceCompras').live('click', function () {
                    if ($(this).hasClass('Link')) {
                        var idDiv = (this.id).replace('div', '');
                        idDiv += '-' + $('#cboParaProceComprasAnyo' + Master).fsCombo('getSelectedValue');
                        idDiv += '-' + $('#cboParaProceComprasGMN1' + Master).fsCombo('getSelectedValue');
                        idDiv += '-' + $('#cboParaProceComprasCodDen' + Master).fsCombo('getSelectedValue');
                        if ($('#divListaPara' + Master + ' #' + idDiv.replace('Master', '')).length == 0) {
                            if (idDiv.split('-')[1] == 'Todos' + Master) {
                                $('#divListaPara' + Master + ' [id^=itemParaProceCompras-]').remove();
                                $('.ItemParaProceCompras').removeClass('Link');
                                $('.ItemParaProceCompras span').addClass('LinkDisabled');
                            }
                            if ($('#divListaPara' + Master + ' [id^=itemParaProceCompras-]').length == 3) {
                                $('#divListaPara' + Master + ' [id^=itemParaProceCompras-]').remove();
                                $('.ItemParaProceCompras').removeClass('Link');
                                $('.ItemParaProceCompras span').addClass('LinkDisabled');
                                var idDivTodos = $('[id^=divitemParaProceCompras-Todos]').attr('id').replace('div', '');
                                $('#divListaPara' + Master).prepend('<div class="ItemPara" id="' + idDivTodos.replace('Master', '') + '">' + $('#divitemParaProceCompras-Todos' + Master).find('span').text() + '</div>');
                            } else {
                                $('#divListaPara' + Master).prepend('<div class="ItemPara" id="' + idDiv.replace('Master', '') + '">' + $(this).find('span').text() + '</div>');
                            }
                        }
                    }
                });

                $('#divParaProceCompras' + Master).removeAttr('cargar');
            }
            ComprobarValoresIniciales('divParaProceCompras' + Master);
            break;
        case 'divParaEstrucCompras' + Master:
            if ($('#divParaEstrucCompras' + Master).attr('cargar') == '') {
                EstablecerTextosMensajeNuevo('ParaEstrucCompras');

                $.when($.ajax({
                    type: "POST",
                    url: rutaFS + 'CN/App_Services/CN.asmx/EquiposCompra_Permiso_EmitirMensajeUsuario',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false
                })).done(function (msg) {
                    var data = msg.d;
                    $('#tvParaEstrucComprasEquipos' + Master).tree({
                        data: data,
                        autoOpen: true,
                        selectable: true,
                        onOpenParentsDone: function () { setScrollSelected("tvParaEstrucComprasEquipos" + Master, 100) }
                    });
                });

                $('#tvParaEstrucComprasEquipos' + Master + ' .treeTipo4').live('click', function () {
                    var usu = ($(this).attr("nivel") == 1) ? 'Usu' : 'Eqp';
                    if ($('#divListaPara' + Master + ' #' + this.id.replace('tvParaEstrucComprasEquipos' + Master, 'itemParaEstrucCompras-' + usu)).length == 0) {
                        $('#divListaPara' + Master).prepend('<div class="ItemPara" id="' + this.id.replace('tvParaEstrucComprasEquipos' + Master, 'itemParaEstrucCompras-' + usu) + '">' + $(this).text() + '</div>');
                        $('#txtParaEstrucComprasEquipo' + Master).val('');
                    }
                });

                $(".ItemParaEstrucCompras").live('click', function () {
                    if ($(this).hasClass('Link')) {
                        if ($('#divListaPara' + Master + ' [id^=' + this.id.replace('div', '') + ']').length == 0) {
                            if ($('#txtParaEstrucComprasMatGS' + Master).attr('itemValue') && $('#txtParaEstrucComprasMatGS' + Master).attr('itemValue') != '') {
                                $('#divListaPara' + Master).prepend('<div class="ItemPara" id="' + this.id.replace('div', '').replace('Master', '') + '-' + $('#txtParaEstrucComprasMatGS' + Master).attr('itemValue') + '">' + $(this).find('span').text() + $('#txtParaEstrucComprasMatGS' + Master).val() + '</div>');

                                $('.ItemParaEstrucCompras').removeClass('Link');
                                $('.ItemParaEstrucCompras span').addClass('LinkDisabled');

                                $('#txtParaEstrucComprasMatGS' + Master).attr('disabled', 'disabled');
                                $('#txtParaProveMatGS' + Master).attr('disabled', 'disabled');
                            } else { return false; }
                        }
                    }
                });

                $('#txtParaEstrucComprasEquipo' + Master).live(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
                    var code;
                    if (event.keyCode) code = event.keyCode;
                    else if (event.which) code = event.which;

                    switch (code) {
                        case KEY.RETURN:
                            var selectedItem = $('#tvParaEstrucComprasEquipos' + Master + ' span.treeSeleccionable.selected');
                            if (selectedItem.length != 0) {
                                var usu = (selectedItem.attr("nivel") == 1) ? 'Usu' : 'Eqp';
                                if ($('#divListaPara' + Master + ' #' + $(selectedItem).attr("id").replace('tvParaEstrucComprasEquipos' + Master, 'itemParaEstrucCompras-' + usu)).length == 0) {
                                    $('#divListaPara' + Master).prepend('<div class="ItemPara" id="' + $(selectedItem).attr('id').replace('tvParaEstrucComprasEquipos' + Master, 'itemParaEstrucCompras-' + usu) + '">' + $(selectedItem).text() + '</div>');
                                }
                            }
                            $('#txtParaEstrucComprasEquipo' + Master).val('');
                            $('#tvParaEstrucComprasEquipos' + Master + ' span.treeSeleccionable.selected').removeClass('selected');
                            event.preventDefault();
                            return false;
                            break;
                        default:
                            EncontrarItemTreeView('tvParaEstrucComprasEquipos' + Master, 'txtParaEstrucComprasEquipo' + Master, code);
                            break;
                    }
                });

                $('#divParaEstrucCompras' + Master).removeAttr('cargar');
            }
            ComprobarValoresIniciales('divParaEstrucCompras' + Master);
            break;
        case 'divParaGrupos' + Master:
            if ($('#divParaGrupos' + Master).attr('cargar') == '') {
                EstablecerTextosMensajeNuevo('ParaGrupos');

                var optionsGrupos = { valueField: "value", textField: "text", isDropDown: false, MinimumPrefixLength: 0, itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, adjustWidthToSpace: true, WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/GruposCorporativos_Permiso_EmitirMensajeUsuario' };

                $('#cboParaGruposGruposCorporativos' + Master).fsCombo(optionsGrupos);
                $('#cboParaGruposGruposCorporativos' + Master).fsCombo('onSelectedValueChange', function () { CargarParaGruposCorporativos(Master) });

                $('.ItemParaGrupo').live('click', function () {
                    var idDiv = (this.id).replace('div', '');
                    if ($('#divListaPara' + Master + ' #' + idDiv.replace('Master', '')).length == 0) {
                        $('#divListaPara' + Master).prepend('<div class="ItemPara" id="' + idDiv.replace('Master', '') + '">' + $(this).find('span').text() + '</div>');
                    }
                });

                $('#divParaGrupos' + Master).removeAttr('cargar');
            }
            ComprobarValoresIniciales('divParaGrupos' + Master);
            break;
        default:
            break;
    }
}
function ComprobarDatosMensajeNuevo(tipo, Master) {
    if ($('[id^=btnAceptarNuevoMensaje]').attr('IdMensaje') != '0') return true;
    if (CKEDITOR.instances['CKENuevoMensaje' + Master].getData() == '') {
        alert(Textos[93]);
        return false;
    }
    if ($('#txtSelCategoria' + Master).attr('itemValue') == undefined || $('#txtSelCategoria' + Master).attr('itemValue') == '') {
        alert(Textos[94]);
        return false;
    }
    if ($('#divListaPara' + Master + ' .ItemPara').length==0) {
        alert(Textos[95]);
        return false;
    }
    switch(tipo){
        case 'NuevoEvento':
            var fecha = $('#txtEventoFecha' + Master).val();
            if (fecha == '') {
                alert(Textos[96]);
                return false;
            } else {
                var dia, mes, anio;
                for (i = 0; i < fecha.split(UsuMaskSeparator).length; i++) {
                    switch (UsuMask.split(UsuMaskSeparator)[i]) {
                        case 'dd':
                            dia = fecha.split(UsuMaskSeparator)[i];
                            break;
                        case 'MM':
                            mes = fecha.split(UsuMaskSeparator)[i];
                            break;
                        case 'yyyy':
                            anio = fecha.split(UsuMaskSeparator)[i];
                            break;
                    }
                }
                if (!IsValidDate(dia, mes, anio)) {
                    alert(Textos[97]);
                    return false;
                }
            }
            if ($('#txtEventoHora' + Master).val() == '') {
                alert(Textos[98]);
                return false;
            } else {
                var hora = $('#txtEventoHora' + Master).val().split(':')[0];
                var minutos = $('#txtEventoHora' + Master).val().split(':')[1];
                if (hora < 0 || hora > 23) {
                    alert(Textos[113]);
                    return false;
                }
                if (minutos < 0 || minutos > 59) {
                    alert(Textos[113]);
                    return false;
                }
            }
            if ($('#txtEventoDonde' + Master).val() == '' || $('#txtEventoDonde' + Master).hasClass('textoDefecto')) {
                alert(Textos[99]);
                return false;
            }
            break;
    }    
    return true;
}
function GuardarMensajeBD(tipo, Master, IdMen) {
    var IdMensaje = parseInt(IdMen);
    var params='';
    var tipo;
    var mensaje='';
    var categoria='';
    var para = {};
    var fechaEvento;
    //TIPO
    switch(tipo){
        case 'NuevoTema' + Master:
            tipo = 1;
            break;
        case 'NuevoEvento' + Master:
            tipo = 2;
            var fecha = $('#txtEventoFecha').val();
            var dia, mes, anio, hora, minuto;
            for (i = 0; i < fecha.split(UsuMaskSeparator).length; i++) {
                switch (UsuMask.split(UsuMaskSeparator)[i]) {
                    case 'dd':
                        dia = fecha.split(UsuMaskSeparator)[i];
                        break;
                    case 'MM':
                        mes = fecha.split(UsuMaskSeparator)[i];
                        break;
                    case 'yyyy':
                        anio = fecha.split(UsuMaskSeparator)[i];
                        break;
                }
            }
            hora = $('#txtEventoHora').val().split(":")[0];
            minuto = $('#txtEventoHora').val().split(":")[1];
            fechaEvento = new Date(anio, mes, dia, hora, minuto);      
            break;
        case 'NuevaNoticia' + Master:
            tipo = 3;
            break;
        case 'NuevaPregunta' + Master:
            tipo = 4;
            break;
        case 'NuevoMensajeUrgente' + Master:
            tipo = 5;
            break;
    }    
    var idNuevo;
    if (IdMensaje == 0) {
        idNuevo = 0;
        while ($('[EnProceso=x' + idNuevo + 'x]').length != 0) {
            idNuevo++;
        }
    } else {
        idNuevo = IdMensaje;
    }
    var fechaAlta = new Date();
    var fechaAltaMensaje = UsuMask.replace('dd', fechaAlta.getDate()).replace('MM', fechaAlta.getMonth() + 1).replace('yyyy', fechaAlta.getFullYear());
    fechaAltaMensaje += ' ' + fechaAlta.getHours() + ':' + fechaAlta.getMinutes() + ':' + fechaAlta.getSeconds();
    var editor = CKEDITOR.instances['CKENuevoMensaje' + Master];
    var CKEditorImages = [];
    if (editor.mode == 'source') {
        guardando = true;
        tipoAux = tipo;
        MasterAux = Master;
        IdMenAux = IdMen;
        editor.setMode('wysiwyg');
        return false;
    }
    $.each(editor.document.getElementsByTag('img').$, function () {
        if ($(this).attr("type") == "CKEditorImage") CKEditorImages.push({ href: $(this).attr('src'), path: '' + $(this).attr('id') + '', filename: '' + $(this).attr('filename') + '', guidID: '' });
    });
    //modifico las url de las imagenes de los link a entidades
    var contenido = $('<div>' + editor.getData() + '</div>');
    $.each($('a[type=entidad] img', contenido), function () {
        img = $(this);
        img = img.attr('src', img.attr('src').replace(ruta, ''));
        $('img', $(this)).replaceWith(img);
    });
    //MENSAJE   
    var mensaje =
    {
        id: '' + idNuevo + '',
        fechaAlta:'' + fechaAltaMensaje + '',
        titulo: $('#txtNuevoMensajeTitulo' + Master).hasClass('textoDefecto') ? '' : '' + $('#txtNuevoMensajeTitulo' + Master).val() + '',
        contenido: '' + editor.getData() + '',
        fecha: '' + $('#txtEventoFecha' + Master).val() + '',
        hora: '' + $('#txtEventoHora' + Master).val() + '',
        timezoneOffset: fechaAlta.getTimezoneOffset(),
        donde: '' + $('#txtEventoDonde' + Master).val() + ''
    }
    //CATEGORIA
    categoria = 
    {
        id: $('#txtSelCategoria' + Master).attr('itemValue'),
        denominacion: '' + $('#txtSelCategoria' + Master).val() + ''
    };

    //ADJUNTOS
    var listaAdjuntos = [];
    $.each($('#divListaAdjuntos' + Master + ' #tablaadjuntos').children(), function () {
        listaAdjuntos.push({
            nombre: $(this).attr('data-name'),
            size: $(this).attr('data-size'),
            sizeunit: $(this).attr('data-sizeunit'),
            tipoadjunto: $(this).attr('data-tipo'),
            url: $(this).attr('data-url'),
            folder: $(this).attr('data-folder')
        });
    });
    params = { tipo: tipo, mensaje: mensaje, categoria: categoria, adjuntos: listaAdjuntos };    
    if (Master == '' || IdMensaje!==0) {
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'CN/App_Services/CN.asmx/GenerarMensaje',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(params),
            dataType: "json",
            async: false
        })).done(function (msg) {
            if (tipo != 5) {
                var cn = { posts: [] };
                var mensaje = msg.d;
                mensaje.FechaAlta = eval("new " + mensaje.FechaAlta.slice(1, -1));
                mensaje.FechaActualizacion = eval("new " + mensaje.FechaActualizacion.slice(1, -1));

                mensaje.FechaAltaRelativa = relativeTime(mensaje.FechaAlta);
                mensaje.FechaActualizacionRelativa = relativeTime(mensaje.FechaActualizacion);

                if (IdMensaje !== 0) mensaje.IdMensaje = IdMensaje;
                cn.posts.push(mensaje);

                var ul;

                if (IdMensaje == 0) {
                    ul = $('#divMuro>ul:first');
                    $('#itemMensaje').tmpl(cn.posts).prependTo(ul);

                    if ($('#divMuro').css('display') == 'none') {
                        $('#divMuro').fadeIn()
                    }
                } else {
                    var adjuntos =$('#msg_' + IdMensaje + ' .msgAdjuntos>div');                                      
                    var mensajePrevio = $('#msg_' + IdMensaje).prev();
                    var respuestas = $('#divRespuestasMensaje_' + IdMensaje);
                    $('#msg_' + IdMensaje).remove();
                    if (mensajePrevio.length == 0) {
                        ul = $('#divMuro ul');
                        $('#itemMensaje').tmpl(cn.posts).prependTo(ul);
                    } else {
                        mensajePrevio.after($('#itemMensaje').tmpl(cn.posts));
                    }
                    $('#divRespuestasMensaje_' + IdMensaje).append(respuestas.html());
                    $.each(adjuntos, function () {
                        $('#msg_' + IdMensaje + ' .msgAdjuntos').prepend($(this));
                    });  
                }
                $('[id^=msgMeGusta_]').text(Textos[8]);
                $('[id^=msgResponder_]').text(Textos[77]);
                $('[id^=msgDestinatarios_]').text(Textos[78]);
                $('[id^=msgEditar_]').text(Textos[83]);
                $('[id^=msgNuevaRespuestaDefault_]').val(Textos[84]);
                $('[id^=msgDescargar__]').text(Textos[79]);
                $('[id^=btnResponder_]').text(Textos[20]);
                $('[id^=btnResponder_]').attr('title', Textos[20]);
                $('[id^=btnCancelarResponder_]').text(Textos[21]);
                $('[id^=btnCancelarResponder_]').attr('title', Textos[21]);
                $('[id^=msgNoLeido]').text(Textos[129]);
                $('[id^=msgInfoCategoria]').text(Textos[130]);
                $('[id^=msgInfoCategoria]').text(Textos[110]);
            }
        });
    }
    mensaje.contenido = contenido.html();
    var paraMensaje = ObtenerParaMensaje(Master);
    params = { tipo: tipo, mensaje: mensaje, categoria: categoria, para: paraMensaje, adjuntos: listaAdjuntos, imagenesCKEditor: CKEditorImages, factura: 0, linea: 0 };
    if (Master == '') {
        MostrarFormularioNuevoMensaje(false, false);
    } else {
        MostrarFormularioNuevoMensaje(false, true);
    }
    if (IdMensaje=='0') {
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'CN/App_Services/CN.asmx/InsertarMensaje',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(params),
            dataType: "json",
            async: true
        })).done(function (msg) {
            if (Master == '') {
                if (tipo != 5) {
                    var id = msg.d;
                    $.each($('[EnProceso=x' + idNuevo + 'x]'), function () {
                        $(this).attr('id', $(this).attr('id').replace('_' + idNuevo, '_' + id));
                        $(this).attr('EnProceso', '');
                    });
                    $('[name=msgRespuestaNueva_' + idNuevo + ']').attr('name', $('[name=msgRespuestaNueva_' + idNuevo + ']').attr('name').replace('_' + idNuevo, '_' + id));
                    $('#btnResponder_' + id).text(Textos[20]);
                    $('#btnResponder_' + id).attr('title', Textos[20]);
                    $('#msgAdjuntoRespuesta_' + id + ' span').text(Textos[28]);
                }
            } else {
                if ($.isFunction(window.closeWindowForm)) {
                    closeWindowForm();
                }
            }
        });
    } else {  
        $.ajax({
            type: "POST",
            url: rutaFS + 'CN/App_Services/CN.asmx/ActualizarMensaje',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(params),
            dataType: "json",
            async: true        
        });
    }    
}
function ObtenerParaMensaje(Master) {
    //PARA
    var UON0, UON1, UON2, UON3, DEP, USU;
    var tipoProve, codigoProve, idContacto, idMatQA, gmn1, gmn2, gmn3, gmn4, prove;
    var anyo, cod, proceCompras, num;
    var tipoEstrucCompras, codigoEstrucCompras;
    var tipoGrupo, idGrupo, grupo;
    var para = { UON: [], PROVE: [], PROCECOMPRAS: [], ESTRUCCOMPRAS: [], GRUPO: [] };
    $.each($('#divListaPara' + Master + ' .ItemPara'), function () {
        switch ($(this).attr("id").split("-")[0]) {
            case 'itemParaUON':
                UON0 = true;
                UON1 = null;
                UON2 = null;
                UON3 = null;
                DEP = null;
                USU = null;
                for (i = 1; i < $(this).attr("id").split("-").length; i++) {
                    switch ($(this).attr("id").split("-")[i].split("_")[0]) {
                        case "UON1":
                            UON0 = false;
                            UON1 = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "UON2":
                            UON0 = false;
                            UON2 = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "UON3":
                            UON0 = false;
                            UON3 = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "DEP":
                            UON0 = false;
                            DEP = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "USU":
                            UON0 = false;
                            USU = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                    }
                }
                if (UON0) {
                    codigo = { UON1: null, UON2: null, UON3: null, DEP: null, USU: null };
                } else {
                    codigo = { UON1: UON1, UON2: UON2, UON3: UON3, DEP: DEP, USU: USU };
                }
                para.UON.push(codigo)
                break;
            case "itemParaProve":
                tipoProve = null; codigoProve = null; idContacto = null; idMatQA = null;
                gmn1 = null; gmn2 = null; gmn3 = null; gmn4 = null;
                switch ($(this).attr("id").split("-")[1]) {
                    case "Todos":
                        tipoProve = 1;
                        break;
                    case "Calidad":
                        tipoProve = 2;
                        break;
                    case "Potenciales":
                        tipoProve = 3;
                        break;
                    case "Reales":
                        tipoProve = 4;
                        break;
                    case "MatQA":
                        tipoProve = 5;
                        idMatQA = $(this).attr("id").split("-")[2]
                        break;
                    case "MatGS":
                        tipoProve = 6;
                        num = $(this).attr("id").split("-").length;
                        switch (true) {
                            case (num > 5):
                                gmn4 = $(this).attr("id").split("-")[5];
                            case (num > 4):
                                gmn3 = $(this).attr("id").split("-")[4];
                            case (num > 3):
                                gmn2 = $(this).attr("id").split("-")[3];
                            case (num > 2):
                                gmn1 = $(this).attr("id").split("-")[2];
                                break;
                        }
                        break;
                    case "Uno":
                        tipoProve = 0;
                        idContacto = $(this).attr('id').split('-')[$(this).attr('id').split('-').length - 1];
                        codigoProve = $(this).attr('id').replace('itemParaProve-Uno-', '').replace('-' + idContacto, '');
                        break;
                }
                prove = { TIPOPROVE: tipoProve, CODIGOPROVE: codigoProve, IDCONTACTO: idContacto, MATQA: idMatQA, GMN1: gmn1, GMN2: gmn2, GMN3: gmn3, GMN4: gmn4 };
                para.PROVE.push(prove);
                break;
            case "itemParaProceCompras":
                anyo = $(this).attr("id").split("-")[2];
                gmn1 = $(this).attr("id").split("-")[3];
                cod = $(this).attr("id").split("-")[4];
                proceCompras = { ANYO: anyo, GMN1: gmn1, COD: cod, TIPOPARAPROCECOMPRA: $(this).attr("id").split("-")[1] };
                para.PROCECOMPRAS.push(proceCompras);
                break;
            case "itemParaEstrucCompras":
                tipoEstrucCompras = null; codigoEstrucCompras = null;
                gmn1 = null; gmn2 = null; gmn3 = null; gmn4 = null;
                //TIPO: 1-Equipo de compra, 2-Usuario
                switch ($(this).attr("id").split("-")[1]) {
                    case 'Eqp':
                        tipoEstrucCompras = 1;
                        codigoEstrucCompras = $(this).attr("id").split("-")[2].split("_")[1];
                        break;
                    case 'Usu':
                        tipoEstrucCompras = 2;
                        codigoEstrucCompras = $(this).attr("id").split("-")[3].split("_")[1];
                        break;
                    case 'MatGS':
                        tipoEstrucCompras = 3;
                        num = $(this).attr("id").split("-").length;
                        switch (true) {
                            case (num > 5):
                                gmn4 = $(this).attr("id").split("-")[5];
                            case (num > 4):
                                gmn3 = $(this).attr("id").split("-")[4];
                            case (num > 3):
                                gmn2 = $(this).attr("id").split("-")[3];
                            case (num > 2):
                                gmn1 = $(this).attr("id").split("-")[2];
                                break;
                        }
                        break;
                }
                estrucCompras = { TIPOESTRUCCOMPRAS: tipoEstrucCompras, CODIGO: codigoEstrucCompras, GMN1: gmn1, GMN2: gmn2, GMN3: gmn3, GMN4: gmn4 };
                para.ESTRUCCOMPRAS.push(estrucCompras);
                break;
            case "itemParaGrupo":
                tipoGrupo = null, idGrupo = null;
                switch ($(this).attr("id").split("-")[1]) {
                    case "EP":
                        tipoGrupo = 'EP';
                        break;
                    case "GS":
                        tipoGrupo = 'GS';
                        break;
                    case "PM":
                        tipoGrupo = 'PM';
                        break;
                    case "QA":
                        tipoGrupo = 'QA';
                        break;
                    case "SM":
                        tipoGrupo = 'SM';
                        break;
                    default:
                        idGrupo = $(this).attr("id").split("-")[2];
                        break;
                }
                grupo = { TIPOGRUPO: tipoGrupo, IDGRUPO: idGrupo };
                para.GRUPO.push(grupo);
                break;
        }
    });
    return para;
}
function EstablecerTextosMensajeNuevo(tipo) {
    switch (tipo) {
        case 'Para':
            $('[id^=lblNuevoMensajePara]').text(Textos[33]);
            $('[id^=divParaUON]').attr('title', Textos[34]);
            $('[id^=divParaProve]').attr('title', Textos[35]);
            $('[id^=divParaProceCompras]').attr('title', Textos[36]);
            $('[id^=divParaEstrucCompras]').attr('title', Textos[37]);
            $('[id^=divParaGrupos]').attr('title', Textos[38]);
            break;
        case 'ParaUON':
            $('[id^=lblParaUON]').text(Textos[39]);
            break;
        case 'ParaProve':
            $('[id^=lblParaProveTodos]').text(Textos[40]);
            $('[id^=lblParaProveCalidad]').text(Textos[41]);
            $('[id^=lblParaProvePotencialesCalidad]').text(Textos[42]);
            $('[id^=lblParaProveRealesCalidad]').text(Textos[43]);

            $('[id^=lblParaProveMaterialGS]').text(Textos[44] + ':');
            $('[id^=lblParaProveMaterialQA]').text(Textos[45] + ':');

            $('[id^=lblParaProveBuscar]').text(Textos[50]);
            $('[id^=lblParaProveBuscarPor]').text(Textos[46] + ':');
            $('[id^=lblParaProveBuscarCodigo]').text(Textos[47]);
            $('[id^=lblParaProveBuscarNIF]').text(Textos[48]);
            $('[id^=lblParaProveBuscarDenominacion]').text(Textos[49]);            

            $('[id^=lblParaProveedores]').text(Textos[51]);
            break;
        case 'ParaProceCompras':
            $('[id^=lblParaProceCompra]').text(Textos[52]);
            $('[id^=lblParaProceComprasTodosIntegrantes]').text(Textos[53]);
            $('[id^=lblParaProceComprasResponsable]').text(Textos[54]);
            $('[id^=lblParaProceComprasInvitados]').text(Textos[55]);
            $('[id^=lblParaProceComprasCompradores]').text(Textos[56]);
            $('[id^=lblParaProceComprasProveedores]').text(Textos[57]);
            $('[id^=lblParaProceCompraAnio]').text(Textos[74]);
            $('[id^=lblParaProceCompraGMN1]').text(Textos[75]);
            $('[id^=lblParaProceCompraCodDen]').text(Textos[76]);            
            break;
        case 'ParaEstrucCompras':
            $('[id^=lblParaEstrucComprasMatGS]').text(Textos[58]);
            $('[id^=lblParaEstrucComprasEquipo]').text(Textos[59]);
            break;
        case 'ParaGrupos':
            $('[id^=lblParaGruposEP]').text(Textos[61]);
            $('[id^=lblParaGruposGS]').text(Textos[62]);
            $('[id^=lblParaGruposPM]').text(Textos[63]);
            $('[id^=lblParaGruposQA]').text(Textos[64]);
            $('[id^=lblParaGruposSM]').text(Textos[65]);

            $('[id^=lblParaGruposGruposCorporativos]').text(Textos[67] + ':');
            break;        
        default:
            $('[id^=lblNuevoMensajeAceptar]').text(Textos[20]);
            $('[id^=btnAceptarNuevoMensaje]').attr('title', Textos[20]);
            $('[id^=lblNuevoMensajeCancelar]').text(Textos[21]);
            $('[id^=btnCancelarNuevoMensaje]').attr('title', Textos[20]);

            $('[id^=lblAgregarEtiqueta]').text(Textos[31]);
            $('[id^=btnAgregarEtiqueta]').attr('title', Textos[31]);
            
            $('[id^=lblNuevoMensajeAdjunto]').text(Textos[28]);
            $('[id^=NuevoMensajeAdjunto]').attr('title', Textos[28]);
            $('[id^=lblNuevoMensajeEnlace]').text(Textos[29]);
            $('[id^=NuevoMensajeEnlace]').attr('title', Textos[29]);
            $('[id^=lblNuevoMensajeEtiqueta]').text(Textos[30]);
            $('[id^=NuevoMensajeEtiqueta]').attr('title', Textos[30]);

            $('[id^=lblNuevoMensajeCategoria]').text(Textos[32]);
            $('[id^=divSelCategoria]').attr('title', Textos[13]);            

            $('#divNuevoMensaje').removeAttr('textos');
            break;
    }
}
// ===========================================
// =========== FUNCIONES UTILES ==============
// ===========================================
function SeleccionarCategoria(Master) {
    var selectedCategoria = $('#tvNuevoMensajeCategorias' + Master + ' .selected');
    var texto;
    var txtCategoria = $('#txtSelCategoria' + Master);
    txtCategoria.attr('itemValue', selectedCategoria.attr('itemValue'));
    texto = selectedCategoria.text();
    while ($('#' + selectedCategoria.attr('parentId')).length > 0) {
        selectedCategoria = $('#' + selectedCategoria.attr('parentId'));
        texto = selectedCategoria.text() + ">" + texto;
    }
    txtCategoria.val(texto);
}
function SeleccionarProveMatGS(tree, input, inputAux) {
    var selectedMatGS = $('#' + tree).tree('getSelectedNode');
    var texto,value;
    var txtMatGS = $('#' + input);
    var txtMatGSAux=$('#' + inputAux);
    value = selectedMatGS.value;
    texto = selectedMatGS.text;
    while (selectedMatGS.parent.value!==null) {
        selectedMatGS = selectedMatGS.parent;
        value = selectedMatGS.value + '-' + value;
        texto = selectedMatGS.value + " - " + texto;
    }
    txtMatGS.attr("itemValue", value);
    txtMatGS.val(texto);
    txtMatGSAux.attr("itemValue", value);
    txtMatGSAux.val(texto);
    $('#' + tree).fadeOut();
}
function MostrarOpcionesProceCompras(Master) {
    if ($('#cboParaProceComprasCodDen' + Master).fsCombo('getSelectedValue') == '') {
        $('.ItemParaProceCompras').removeClass('Link');
        $('.ItemParaProceCompras span').addClass('LinkDisabled');
    } else {
        $('.ItemParaProceCompras').addClass('Link');
        $('.ItemParaProceCompras span').removeClass('LinkDisabled');
    }
}
function CargarParaGruposCorporativos(Master) {
    if ($('#cboParaGruposGruposCorporativos' + Master).fsCombo('getSelectedValue') != '') {
        var value = $('#cboParaGruposGruposCorporativos' + Master).fsCombo('getSelectedValue');
        var text = $('#cboParaGruposGruposCorporativos' + Master).fsCombo('getSelectedText');

        if ($('#divListaPara' + Master + ' #itemParaGrupo-Uno-' + value).length == 0) {
            $('#divListaPara' + Master).prepend('<div class="ItemPara" id="itemParaGrupo-Uno-' + value + '">' + text + '</div>');
        }

        $('#cboParaGruposGruposCorporativos' + Master).fsCombo('selectValue', '');
    }
}
function CargarMaterialesGS(tree, treeAux,input,inputAux) {
    $('#' + tree).removeAttr('cargar');
    if ($('#' + treeAux).attr('cargar')=='') {
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'CN/App_Services/CN.asmx/Obtener_MaterialesGS_Proveedor',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true
        })).done(function (msg) {
            var data = msg.d;
            $('#' + tree).tree({
                data: data,
                selectable: true
            });
        });
    } else {
        var data = $('#' + treeAux).tree('getTreeData');
        $('#' + tree).tree({
            data: data,
            selectable: true
        });
    }

    $('#' + tree + ' .treeTipo3').live('click', function () {
        SeleccionarProveMatGS(tree, input, inputAux);
    });
}
function ComprobarValoresIniciales(item) {  
    switch (item) {
        case 'divParaProve':
            if ($('#divListaPara [id^=itemParaProve-]').length != 0) {
                $('.ItemParaProveedor').removeClass('Link');
                $('.ItemParaProveedor span').addClass('LinkDisabled');

                $('#divParaProveBuscar').removeClass('Link');
                $('#divParaProveBuscar span').addClass('LinkDisabled');
                
                $('#txtParaProveMatGS').attr('disabled', 'disabled');
                $('#cboParaProveMatQA').fsCombo('disable');

            }
            if ($('#divListaPara [id^=itemParaProve-Uno-]').length != 0) {
                $('[id^=divitemParaProve-Uno-]').addClass('Link');
                $('[id^=divitemParaProve-Uno-] span').removeClass('LinkDisabled');

                $('#divParaProveBuscar').addClass('Link');
                $('#divParaProveBuscar span').removeClass('LinkDisabled');
            }
            break;
        case 'divParaProceCompras':
            if ($('#divListaPara [id^=itemParaProceCompras-]').length != 0) {
                var anyo = $('#divListaPara [id^=itemParaProceCompras-]').attr('id').split('-')[2];
                var gmn1 = $('#divListaPara [id^=itemParaProceCompras-]').attr('id').split('-')[3];
                var cod = $('#divListaPara [id^=itemParaProceCompras-]').attr('id').split('-')[4];

                $('#cboParaProceComprasAnyo').fsCombo('selectValue',anyo);
                $('#cboParaProceComprasGMN1').fsCombo('selectValue',gmn1);
                $('#cboParaProceComprasCodDen').fsCombo('selectValue', cod);

                $('.ItemParaProceCompras').removeClass('Link');
                $('.ItemParaProceCompras span').addClass('LinkDisabled');
            }
            break;
        case 'divParaEstrucCompras':
            if ($('#divListaPara [id^=itemParaEstrucCompras-MatGS]').length != 0) {
                $('#txtParaEstrucComprasMatGS').attr('disabled', 'disabled');
                $('#txtParaProveMatGS').attr('disabled', 'disabled');

                $('.ItemParaEstrucCompras').removeClass('Link');
                $('.ItemParaEstrucCompras span').addClass('LinkDisabled');
            }
            break;
    }
    $('#divNuevoMensajePara').removeAttr('new');
}
function CalcularScroll(element) {
    var scrollTop = 0;
    $.each(element.children('li'), function () {        
        if ($(this).children('span').hasClass('selected')) {
            return false;
        } else {
            if ($(this).hasClass('folder')) { 
                if ($(this).children('.toggler').hasClass('closed')) {
                    scrollTop += $(this).children('span')[0].offsetHeight;
                } else {
                    scrollTop += $(this).children('span')[0].offsetHeight;
                    scrollTop += CalcularScroll($(this).children('ul'));
                    return false;
                }
            } else {
                scrollTop += $(this).children('span')[0].offsetHeight;
            }        
        }
    });
    return scrollTop;
}
function SCAYTready(instance) {
    if (CKEDITOR.instances[instance] != undefined && CKEDITOR.plugins.scayt.isScaytReady(CKEDITOR.instances[instance])) {
        var lang;
        switch (usuario.Idioma_CodigoUniversal) {
            case 'es':
                lang = 'es_ES';
                break;
            case 'en':
                lang = 'en_US';
                break;
            case 'de':
                lang = 'de_DE';
                break;
            default:
                lang = 'es_ES';
                break;
        }
        CKEDITOR.plugins.scayt.instances[instance].setLang(lang);
    }    
}
/* TECLAS POSIBLES PULSADAS PARA SU POSIBLE TRATAMIENTO */
var KEY = {
    UP: 38,
    DOWN: 40,
    DEL: 46,
    TAB: 9,
    RETURN: 13,
    ESC: 27,
    COMMA: 188,
    PAGEUP: 33,
    PAGEDOWN: 34,
    BACKSPACE: 8
};