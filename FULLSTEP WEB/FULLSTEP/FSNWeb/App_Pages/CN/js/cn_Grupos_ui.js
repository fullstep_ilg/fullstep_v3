﻿var timeoutNuevoMensaje;
$('#btnEliminarGrupo').live('click', function () {
    var confirmar = confirm(TextosGrupos[19]);
    if (confirmar) {
        //Aceptar Eliminar
        $('#divCargando').show();
        BorrarGrupoBD(parseInt($('#btnAceptarGrupo').attr('IdGrupo')));
        $('#divCargando').hide();
        var btnAceptarEx = $('[id$=btnAceptarEx]').attr('id');
        CargarTab(0);
        __doPostBack(btnAceptarEx, 'Reload');
    }
});
$('#btnAceptarGrupo').live('click', function () {
    $('#divCargando').show();
    var info = { CNNomGrupo: [], CNDescrGrupo: [] };
    var gruponom, grupodes;
    var long = IdiomasCod.length;
    for (var i = 0; i < long; i++) {
        gruponom = { value: IdiomasCod[i], text: $('#txtNomGrupo_' + IdiomasCod[i]).val() };
        grupodes = { value: IdiomasCod[i], text: $('#txtDescrGrupo_' + IdiomasCod[i]).val() };

        info.CNNomGrupo.push(gruponom);
        info.CNDescrGrupo.push(grupodes);
    }

    var IdGrupo = parseInt($(this).attr('IdGrupo'))
    if (GuardarGrupoBD(info, IdGrupo)) {
        $('#divCargando').hide();
        var btnAceptarEx = $('[id$=btnAceptarEx]').attr('id');
        if (IdGrupo == 0) {
            CargarTab(0);
            __doPostBack(btnAceptarEx, 'Reload');
        }
        else {
            var tab = $find($('[id$=wtGrupos]').attr('id'));
            tab._itemCollection._items[1].set_text($('#txtNomGrupo_' + usuario.IdiomaCod).attr('value'));
            __doPostBack(btnAceptarEx, 'Reload');
        }
    }
});
$('#btnAceptarEliminarConfirm').live('click', function () {
    $('#divCargando').show();
    BorrarGrupoBD(parseInt($('#btnAceptarGrupo').attr('IdGrupo')));
    $('#divCargando').hide();
    $('#popupFondo').hide();
    $('#popupEliminarConfirm').css('display', 'none');
    var btnAceptarEx = $('[id$=btnAceptarEx]').attr('id');
    __doPostBack(btnAceptarEx, 'Reload');
    CargarTab(0);
});
$('#btnCancelarGrupo').live('click', function () {
    CargarTab(0);
});
$('.ItemNuevoMensajePara').live('click', function () {
    $('#divOpcionesPara').show();

    var IdSeleccionado = $('.ItemNuevoMensajeParaSeleccionado').attr('id');
    $('#' + IdSeleccionado).removeClass('ItemNuevoMensajeParaSeleccionado');
    $('#' + IdSeleccionado + 'Panel').hide();

    var IdNuevaSeleccion = $(this).attr('id');
    $('#' + IdNuevaSeleccion).addClass('ItemNuevoMensajeParaSeleccionado');
    $('#' + IdNuevaSeleccion + 'Panel').show();

    Control_Panel_Para(IdNuevaSeleccion);
});
$('#divListaPara').live('click', function () {
    if (!($('#divOpcionesPara').is(':visible')) && $('#divListaPara').children().length == 0) {
        var IdSeleccionado = $('.ItemNuevoMensajeParaSeleccionado').attr('id');
        $('#' + IdSeleccionado).removeClass('ItemNuevoMensajeParaSeleccionado');
        $('#' + IdSeleccionado + 'Panel').hide();

        $('#divOpcionesPara').show();
        $('#divParaUON').addClass('ItemNuevoMensajeParaSeleccionado');
        $('#divParaUONPanel').show();
        Control_Panel_Para("divParaUON");
    }
});
$('#divListaPara').focusout(function () {
    $('.ItemParaSeleccionado').removeClass("ItemParaSeleccionado");
});
$('#divListaPara').live(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
    var code;
    if (event.keyCode) code = event.keyCode;
    else if (event.which) code = event.which;

    if (code == KEY.DEL || code == KEY.BACKSPACE) {
        $('.ItemParaSeleccionado').remove();
    }
    return false;
});
function ExportarExcel() {
    /*
    ''' <summary>
    ''' Exporta a Excel
    ''' </summary>
    ''' <remarks>Llamada desde: onclick del div q contiene al link excel y a la img excel; Tiempo máximo:0,5</remarks>
    ''' <revisado>JVS 23/02/2012</revisado>
    */
    __doPostBack('ExportarExcel', '')
}
function ExportarPDF() {
    /*
    ''' <summary>
    ''' Exporta a Pdf
    ''' </summary>
    ''' <remarks>Llamada desde: onclick del div q contiene al link pdf y a la img pdf; Tiempo máximo:0,5</remarks>
    ''' <revisado>JVS 23/02/2012</revisado>
    */
    __doPostBack('ExportarPDF', '')
}
function EditarGrupo() {
    /*
    ''' <summary>
    ''' Edita categoría
    ''' </summary>
    ''' <remarks>Llamada desde: onclick de lnkEditarCategoria; Tiempo máximo:0,5</remarks>
    ''' <revisado>JVS 24/02/2012</revisado>
    */
    __doPostBack('EditarGrupo', '')
}
function CargarDatosGrupo(idGrupo) {
    var params = { iIdGrupo: idGrupo };

    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'cn/App_Services/Grupo.asmx/Load_Datos_Grupo',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        var oGrupo = msg.d;

        var long = IdiomasCod.length;
        for (var i = 0; i < long; i++) {
            if (idGrupo == 0) {
                $('#txtNomGrupo_' + IdiomasCod[i]).val('');
                $('#txtDescrGrupo_' + IdiomasCod[i]).val('');
            }
            else {
                $('#txtNomGrupo_' + IdiomasCod[i]).val(oGrupo.NomGrupo[IdiomasCod[i]]);
                $('#txtDescrGrupo_' + IdiomasCod[i]).val(oGrupo.DescrGrupo[IdiomasCod[i]]);
            }
        }
    });
    //Cargar tree miembros del grupo
    GetMiembrosGrupo(idGrupo);
}
function GetMiembrosGrupo(idGrupo) {
    // Carga los miembros del grupo
    $('#divParaDestinatarios').empty();
    $('#divMiembrosGrupo').css('display', '');
    $.get(rutaFS + 'cn/html/_mensaje_para.htm', function (paraOpciones) {
        paraOpciones = paraOpciones.replace(/src="/gi, 'src="' + ruta);
        $('#divParaDestinatarios').append(paraOpciones);
        $('#divParaGrupos').remove();
        $('#divParaProve').remove();
        $('#divParaProceCompras').remove();
        $('#divParaEstrucCompras').remove();
        $('#divParaProvePanel').remove();
        $('#divParaProceComprasPanel').remove();
        $('#divParaEstrucComprasPanel').remove();
        Control_Panel_Para("divParaUON");
        if (idGrupo != 0) {
            if (!$('#txtSelCategoria').attr("itemValue") || $('#txtSelCategoria').attr("itemValue") == '') {
                $.when($.ajax({
                    type: "POST",
                    url: rutaFS + 'cn/App_Services/Grupo.asmx/Obtener_Miembros_Grupo',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({ idGrupo: idGrupo }),
                    dataType: "json",
                    async: false
                })).done(function (msg) {
                    $('#tvNuevoMensajeCategorias').tree('selectNodeValue', idGrupo);
                    SeleccionarGrupo();

                    CargarParaAnterior(msg.d[0], "");
                });
            }
        }
    });
}
function SeleccionarGrupo() {
    var selectedGrupo = $('#tvNuevoMensajeCategorias.selected');
    var texto;
    var txtGrupo = $('#txtSelCategoria');
    txtGrupo.attr("itemValue", selectedGrupo.attr("itemValue"));
    texto = selectedGrupo.text();
    while ($('#' + selectedGrupo.attr("parentId")).length > 0) {
        selectedGrupo = $('#' + selectedGrupo.attr("parentId"));
        texto = selectedGrupo.text() + ">" + texto;
    }
    txtGrupo.val(texto);
}
function Control_Panel_Para(Id) {
    switch (Id) {
        case 'divParaUON':
            if ($('#divParaUON').attr('cargar') == '') {
                $('#txtParaUON').attr('disabled', true);

                EstablecerTextosGrupo('ParaUON');

                $.when($.ajax({
                    type: "POST",
                    url: rutaFS + 'cn/App_Services/Grupo.asmx/Obtener_ListaTodosGrupos',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true
                })).done(function (msg) {
                    var data = msg.d;
                    $('#tvParaUONTree').tree({
                        data: data,
                        autoOpen: true,
                        selectable: false
                    });
                });
                $('#tvParaUONTree .treeTipo3').live('click', function () {
                    if ($('#divListaPara #' + $(this).attr("id").replace('tvParaUONTree', 'itemParaUON')).length == 0) {
                        $('#divListaPara').append('<div class="ItemPara" id="' + $(this).attr('id').replace('tvParaUONTree', 'itemParaUON') + '">' + $(this).text() + '</div>');
                    }
                });

                $('#txtParaUON').bind(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
                    var code;
                    if (event.keyCode) code = event.keyCode;
                    else if (event.which) code = event.which;

                    switch (code) {
                        case KEY.RETURN:
                            var selectedItem = $('#tvParaUONTree span.treeSeleccionable.selected');
                            if (selectedItem.length != 0) {
                                if ($('#divListaPara #' + $(selectedItem).attr("id").replace('tvParaUONTree', 'itemParaUON')).length == 0) {
                                    $('#divListaPara').append('<div class="ItemPara" id="' + $(selectedItem).attr('id').replace('tvParaUONTree', 'itemParaUON') + '">' + $(selectedItem).text() + '</div>');
                                }
                            }
                            $('#txtParaUON').val('');
                            $('#tvParaUONTree span.treeSeleccionable.selected').removeClass('selected');
                            event.preventDefault();
                            return false;
                            break;
                        default:
                            EncontrarItemTreeView('tvParaUONTree', 'txtParaUON', code);
                            break;
                    }
                });

                $('#txtParaUON').removeAttr("disabled");
                $('#divParaUON').removeAttr('cargar');
            }

            $('#txtParaUON').focus();
            break;
        default:
            break;
    }
}
function CalcularScroll(element) {
    var scrollTop = 0;
    $.each(element.children('li'), function () {
        if ($(this).children('span').hasClass('selected')) {
            return false;
        } else {
            if ($(this).hasClass('folder')) {
                if ($(this).children('.toggler').hasClass('closed')) {
                    scrollTop += $(this).children('span')[0].offsetHeight;
                } else {
                    scrollTop += $(this).children('span')[0].offsetHeight;
                    scrollTop += CalcularScroll($(this).children('ul'));
                    return false;
                }
            } else {
                scrollTop += $(this).children('span')[0].offsetHeight;
            }
        }
    });
    return scrollTop;
}
var GuardadoOK;
function GuardarGrupoBD(denomGrupo, CNIdGrupo) {
    var params = '';
    var grupo = '';
    var paraGrupo = ObtenerParaGrupo();
    var validar;
    validar = ValidarDatos(denomGrupo, paraGrupo);

    if (validar == 1) {
        $('#divCargando').hide();
        alert(TextosGrupos[21]);
        return false;
    }
    if (validar == 2) {
        $('#divCargando').hide();
        alert(TextosGrupos[22]);
        return false;
    }
    if (CNIdGrupo == 0) {
        //GRUPO
        params = { DenomGrupo: denomGrupo, Miembros: paraGrupo };

        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'cn/App_Services/Grupo.asmx/InsertarGrupo',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(params),
            dataType: "json",
            async: false
        })).done(function (msg) {
            $('#lblGrupoGuardadoOK').show();
            $('#lblGrupoGuardadoOK').text(TextosGrupos[24]);
            GuardadoOK = setTimeout(function () { clearTimeout(GuardadoOK); $('#lblGrupoGuardadoOK').hide(); }, 3000);
        });
    } else {
        params = { IdGrupo: CNIdGrupo, DenomGrupo: denomGrupo, Miembros: paraGrupo };
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'cn/App_Services/Grupo.asmx/UpdateGrupo',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(params),
            dataType: "json",
            async: true
        })).done(function (msg) {
            $('#lblGrupoGuardadoOK').show();
            $('#lblGrupoGuardadoOK').text(TextosGrupos[24]);
            GuardadoOK = setTimeout(function () { clearTimeout(GuardadoOK); $('#lblGrupoGuardadoOK').hide(); }, 3000);
        });
    }
    return true;
}
function BorrarGrupoBD(CNIdGrupo) {
    var params = { IdGrupo: CNIdGrupo }

    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'cn/App_Services/Grupo.asmx/DeleteGrupo',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        $('#divCargando').hide();
    });
}
function EstablecerTextosGrupo(tipo) {
    switch (tipo) {
        case 'ParaUON':
            $('#lblParaUON').text(TextosGrupos[22]);
            break;
    }
}
function CargarParaAnterior(datos) {
    $.each(datos, function () {
        var texto = this.text;
        switch (this.value.split('-')[0]) {
            case 'itemParaGrupos':
                switch (this.value.split('-')[1]) {
                    case 'EP':
                        texto = 'EP';
                        break;
                    case 'GS':
                        texto = 'GS';
                        break;
                    case 'PM':
                        texto = 'PM';
                        break;
                    case 'QA':
                        texto = 'QA';
                        break;
                    case 'SM':                        
                        texto = 'SM';
                        break;
                }
                break;
        }
        $('#divListaPara').append('<div class="ItemPara" id="' + this.value + '">' + texto + '</div>');
    });
}
function ObtenerParaGrupo() {
    //PARA
    var UON0, UON1, UON2, UON3, DEP, USU;
    var tipoProve, codigoProve, idContacto, idMatQA, gmn1, gmn2, gmn3, gmn4, prove;
    var anyo, cod, proceCompras, num;
    var tipoEstrucCompras, codigoEstrucCompras;
    var tipoGrupo, idGrupo, grupo;
    var para = { UON: [], PROVE: [], PROCECOMPRAS: [], ESTRUCCOMPRAS: [], GRUPO: [] };
    $.each($('#divListaPara .ItemPara'), function () {
        switch ($(this).attr("id").split("-")[0]) {
            case "itemParaUON":
                UON0 = true;
                UON1 = null;
                UON2 = null;
                UON3 = null;
                DEP = null;
                USU = null;
                for (i = 1; i < $(this).attr("id").split("-").length; i++) {
                    switch ($(this).attr("id").split("-")[i].split("_")[0]) {
                        case "UON1":
                            UON0 = false;
                            UON1 = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "UON2":
                            UON0 = false;
                            UON2 = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "UON3":
                            UON0 = false;
                            UON3 = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "DEP":
                            UON0 = false;
                            DEP = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "USU":
                            UON0 = false;
                            //Cuando el codigo de usuario tiene el caracter '_' no lo recoge entero, lo coge a partir 
                            //USU = $(this).attr("id").split("-")[i].split("_")[1];
                            USU = ($(this).attr("id").split("-")[i]).substring(4, ($(this).attr("id").split("-")[i]).length);
                            break;
                    }
                }
                if (UON0) {
                    codigo = { UON1: null, UON2: null, UON3: null, DEP: null, USU: null };
                } else {
                    codigo = { UON1: UON1, UON2: UON2, UON3: UON3, DEP: DEP, USU: USU };
                }
                para.UON.push(codigo)
                break;
        }
    });
    return para;
}
function ValidarDatos(denomGrupo, paraGrupo) {
    //validar que el grupo tiene nombre
    var numTot = denomGrupo.CNNomGrupo.length
    for (var i = 0; i < numTot; i++) {
        if (denomGrupo.CNNomGrupo[i].text.length == 0)
            return 1;
    }
    //validar que hay al menos 2 miembros
    if(paraGrupo.UON.length < 2)
        return 2;
    
    return 0;
}