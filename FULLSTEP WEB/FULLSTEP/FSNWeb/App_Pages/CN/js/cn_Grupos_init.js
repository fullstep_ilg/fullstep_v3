﻿var rowSelect = false;
$(document).ready(function () {
    if (typeof (EnBlancoMaster) !== 'undefined' && EnBlancoMaster) $('body').addClass('fondoMasterEnBlanco');
    $('#imgCargando').attr('src', ruta + $('#imgCargando').attr('src'));    
    $('#divCargando').hide();
    $.get(rutaFS + 'CN/html/_menu_cn.htm', function (menu) {
        menu = menu.replace(/src="/gi, 'src="' + ruta);
        $('#divMenu').append(menu);
        CargarMenu();
        $('.MenuItemMenuOpcionSeleccionado').removeClass('MenuItemMenuOpcionSeleccionado');
        $('#AdminGrupos').addClass('MenuItemMenuOpcionSeleccionado');      
        $('#divPrincipal').show();
    });
    $('#btnAceptarGrupo').attr('IdGrupo', 0);
    EstablecerTextosGrupos();
    CrearCajasTextoGrupos();
});
function EstablecerTextosGrupos() {
    var long = IdiomasCod.lenght

    $('#lblDescripGrupo').text(TextosGrupos[13]);
    $('#lblNombreGrupo').text(TextosGrupos[14]);
    $('#lblMiembros').text(TextosGrupos[15]);
    $('#lblGrupoAceptar').text(TextosGrupos[16]);
    $('#btnAceptarGrupo').attr('title', TextosGrupos[16]);
    $('#lblGrupoCancelar').text(TextosGrupos[17]);
    $('#btnCancelarGrupo').attr('title', TextosGrupos[17]);
    $('#lblGrupoEliminar').text(TextosGrupos[18]);
    $('#btnEliminarGrupo').attr('title', TextosGrupos[18]);
    $('#lblPreguntaEliminarConfirm').text(TextosGrupos[19]);
    $('#lblAceptarEliminarConfirm').text(TextosGrupos[16]);
    $('#btnAceptarEliminarConfirm').attr('title', TextosGrupos[16]);
    $('#lblCancelarEliminarConfirm').text(TextosGrupos[17]);
    $('#btnCancelarEliminarConfirm').attr('title', TextosGrupos[17]);
}
function CrearCajasTextoGrupos() {
    var long = IdiomasCod.length;

    for (var i = 0; i < long; i++) {
        $('#divNombresGrupo').append('<div style="width: 100%; clear: both; float: left;"><span id="lblNomGrupo_' + IdiomasCod[i] + '" class="Texto12" style="margin-bottom: 3px;">' + IdiomasTxt[i] + '</span></div> <div style="width: 100%; clear: both; float: left;"><input type="text" id="txtNomGrupo_' + IdiomasCod[i] + '" class="CajaTexto" style="width:360px; margin-top: 3px; margin-bottom: 3px;" /></div>');
        $('#divDescripcionesGrupo').append('<div style="width: 100%; clear: both; float: left;"><span id="lblDescrGrupo_' + IdiomasCod[i] + '" class="Texto12">' + IdiomasTxt[i] + '</span></div> <div style="width: 100%; clear: both; float: left;"><input type="text" id="txtDescrGrupo_' + IdiomasCod[i] + '" class="CajaTexto" style="width:720px; margin-top: 3px; margin-bottom: 3px;" /></div>');
    }
}
//''' <summary>
//''' Si se pulsa una fila de la banda de grupos, se va al mantenimiento para editar el grupo.
//''' Si se pulsa una fila de la banda de miembros del grupo, no hace nada.
//''' </summary>
//''' <remarks>Llamada desde: cn_Grupos.aspx.vb ; Tiempo máximo: 0</remarks>
function WHDG_RowSelectionChanging(webDataGrid, eventArgs) {
    eventArgs.set_cancel(true);

    if (webDataGrid._get_band().get_key() == '') {
        rowSelect = true;

        var selectedDataKey = eventArgs.getNewSelectedRows().getRow(0).get_dataKey();
        var idGrupo = selectedDataKey[0];

        $('#btnAceptarGrupo').attr('IdGrupo', idGrupo);
        $('#btnEliminarGrupo').css('display', '');

        CargarDatosGrupo(idGrupo);

        var tab = $find($('[id$=wtGrupos]').attr('id'));
        tab._itemCollection._items[1].set_text($('#txtNomGrupo_' + usuario.IdiomaCod).val());

        CargarTab(1);
    }
}
function SelectedIndexChanged(sender, e) {
    if (!rowSelect) {
        $('#btnAceptarGrupo').attr('IdGrupo', 0);
        CargarDatosGrupo(0);
        $('#btnEliminarGrupo').css('display', 'none');
        var tab = $find($('[id$=wtGrupos]').attr('id'));
        tab._itemCollection._items[1].set_text(TextosGrupos[23]);
    }
    rowSelect = false
}
function CargarTab(numTab) {
    var tab = $find($('[id$=wtGrupos]').attr('id'));
    tab.set_selectedIndex(numTab);
}