﻿//CN_POPUP_BUSCADORENTIDADES.JS
var cn_popup_buscadorEntidades;
$('#btnAceptarBusquedaPopUp').live('click', function () {
    var ocultar = false;
    switch ($('#btnCancelarBusquedaPopUp').attr('popup')) {
        case 'BuscadorProcesoCompra':
            var anio = $('#cboBuscadorProceComprasAnyo').fsCombo('getSelectedValue');
            var material = $('#cboBuscadorProceComprasGMN1').fsCombo('getSelectedValue');
            var id = $('#cboBuscadorProceComprasCodDen').fsCombo('getSelectedValue');
            if (anio == '' || material == '' || id == '') {
                alert(Textos[100]);
            } else {
                var item = {
                    identificador: anio + '#' + material + '#' + id,
                    texto: anio + '/' + material + '/' + $('#cboBuscadorProceComprasCodDen').fsCombo('getSelectedText')
                }
                CKEDITOR.plugins.registered.FSProcesoCompra.setSelectedValue(item);

                $('#popupBuscadorProcesoCompra').hide();
                ocultar = true;
            }
            break;
        case 'BuscadorPedido':
            var anio = $('#cboBuscadorPedidoAnyo').fsCombo('getSelectedValue');
            var numpedido = $('#cboBuscadorPedidoNumCesta').fsCombo('getSelectedValue');
            var numorden = $('#cboBuscadorPedidoNumPedido').fsCombo('getSelectedValue');
            if (anio == '' || material == '' || id == '') {
                alert(Textos[100]);
            } else {
                var item = {
                    identificador: anio + '#' + numpedido + '#' + numorden,
                    texto: anio + '/' + numpedido + '/' + numorden
                }
                CKEDITOR.plugins.registered.FSPedido.setSelectedValue(item);

                $('#popupBuscadorPedido').hide();
                ocultar = true;
            }
            break;
        case 'BuscadorSolicitudCompra':
            if ($('#cboBuscadorSolicitudCompraIdentificador').fsCombo('getSelectedValue') == '') {
                alert(Textos[100]);
            } else {
                var item = {
                    identificador: $('#cboBuscadorSolicitudCompraIdentificador').fsCombo('getSelectedValue'),
                    texto: $('#cboBuscadorSolicitudCompraIdentificador').fsCombo('getSelectedText')
                }
                CKEDITOR.plugins.registered.FSSolicitudCompra.setSelectedValue(item);

                $('#popupBuscadorSolicitudCompra').hide();
                ocultar = true;
            }
            break;
        case 'BuscadorContrato':
            if ($('#cboBuscadorContratoIdentificador').fsCombo('getSelectedValue') == '') {
                alert(Textos[100]);
            } else {
                var item = {
                    identificador: $('#cboBuscadorContratoIdentificador').fsCombo('getSelectedValue'),
                    texto: $('#cboBuscadorContratoIdentificador').fsCombo('getSelectedText')
                }
                CKEDITOR.plugins.registered.FSContrato.setSelectedValue(item);

                $('#popupBuscadorContrato').hide();
                ocultar = true;
            }
            break;
        case 'BuscadorNoConformidad':
            if ($('#cboBuscadorNoConformidadIdentificador').fsCombo('getSelectedValue') == '') {
                alert(Textos[100]);
            } else {
                var item = {
                    identificador: $('#cboBuscadorNoConformidadIdentificador').fsCombo('getSelectedValue'),
                    texto: $('#cboBuscadorNoConformidadIdentificador').fsCombo('getSelectedText')
                }
                CKEDITOR.plugins.registered.FSNoConformidad.setSelectedValue(item);

                $('#popupBuscadorNoConformidad').hide();
                ocultar = true;
            }
            break;
        case 'BuscadorCertificado':
            if ($('#cboBuscadorCertificadoIdentificador').fsCombo('getSelectedValue') == '') {
                alert(Textos[100]);
            } else {
                var item = {
                    identificador: $('#cboBuscadorCertificadoIdentificador').fsCombo('getSelectedValue'),
                    texto: $('#cboBuscadorCertificadoIdentificador').fsCombo('getSelectedText')
                }
                CKEDITOR.plugins.registered.FSCertificado.setSelectedValue(item);

                $('#popupBuscadorCertificado').hide();
                ocultar = true;
            }
            break;
        case 'BuscadorFichaCalidad':
            if ($('#cboBuscadorFichaCalidadProveedor').fsCombo('getSelectedValue') == '') {
                alert(Textos[100]);
            } else {
                var item = {
                    identificador: $('#cboBuscadorFichaCalidadProveedor').fsCombo('getSelectedValue'),
                    texto: $('#cboBuscadorFichaCalidadProveedor').fsCombo('getSelectedText')
                }
                CKEDITOR.plugins.registered.FSFichaCalidad.setSelectedValue(item);

                $('#popupBuscadorFichaCalidad').hide();
                ocultar = true;
            }
            break;
        default:
            break;
    }

    if (ocultar) {
        $('#popup').hide();
        $('#popupFondo').css('z-index', '1001');
        if ($('#popupNuevoMensaje').length == 0) $('#popupFondo').hide();        
    }
});

$('#btnCancelarBusquedaPopUp').live('click',function () {
    switch ($('#btnCancelarBusquedaPopUp').attr('popup')) {
        case 'BuscadorProcesoCompra':            
            $('#popupBuscadorProcesoCompra').hide();
            break;
        case 'BuscadorPedido':
            $('#popupBuscadorPedido').hide();
            break;
        case 'BuscadorSolicitudCompra':
            $('#popupBuscadorSolicitudCompra').hide();
            break;
        case 'BuscadorContrato':
            $('#popupBuscadorContrato').hide();
            break;
        case 'BuscadorNoConformidad':
            $('#popupBuscadorNoConformidad').hide();
            break;
        case 'BuscadorCertificado':
            $('#popupBuscadorCertificado').hide();
            break;
        case 'BuscadorFichaCalidad':
            $('#popupBuscadorFichaCalidad').hide();
            break;
        default:
            break;
    }
    $('#popupFondo').css('z-index', '1001');
    $('#popup').hide();
    if($('#popupNuevoMensaje').length==0) $('#popupFondo').hide();    
});