﻿var timeoutNuevoMensaje;
$('#btnAceptarCategoria').live('click', function () {
    var CNPermisoPublic = $('#cboParaCatPermisoPublic').fsCombo('getSelectedValue');
    var CNDespublicado = ($('#chkPublicada').attr('checked')) != 'checked'

    //denominación multiidioma
    var info = { CNDenomCat: [] };
    var categden
    var long = IdiomasCod.length;
    for (var i = 0; i < long; i++) {
        categden = { value: IdiomasCod[i], text: $('#txtDenCat_' + IdiomasCod[i]).val() };
        info.CNDenomCat.push(categden);
    }
    var IdCategoria = parseInt($(this).attr('IdCategoria'));
    var btnAceptarEx = $('[id$=btnAceptarEx]').attr('id');
    if (GuardarCategoriaBD(CNPermisoPublic, info, CNDespublicado, IdCategoria)) {
        if (IdCategoria == 0) {
            $('#divTabCategorias').show();
            $('#tabCategorias').addClass('tabActive');
            $('#divTabDetalleCategoria').hide();
            $('#tabDetalleCategoria').removeClass('tabActive');
            $('[id^=cboParaCategorias]').fsCombo('collapseDropDown');
            $('#cboParaCatPermisoPublic').fsCombo('collapseDropDown');
            $('#tabDetalleCategoria').text(TextosCat[38]);
            __doPostBack(btnAceptarEx, 'Reload');
        }
        else {
            $('#tabDetalleCategoria').text($('#txtDenCat_' + usuario.IdiomaCod).attr('value'));
            __doPostBack(btnAceptarEx, 'Reload');
        }
    }
});
$('#btnEliminarCategoria').live('click', function () {
    //''' <summary>
    //''' boton que elimina la categoria si es q no hay mensajes
    //''' </summary>
    //''' <remarks>Llamada desde: Sistema ; Tiempo máximo: 0,2</remarks>
    if (ComprobarEliminarCategoria(parseInt($('#btnAceptarCategoria').attr('IdCategoria')))) {
        alert(TextosCat[40]);
    }
    else {
        var confirmar = confirm(TextosCat[35]);
        if (confirmar) {
            //Aceptar Eliminar
            $('#divCargando').show();
            BorrarCategoriaBD(parseInt($('#btnAceptarCategoria').attr('IdCategoria')));
            $('#divCargando').hide();
            $('#divTabCategorias').show();
            $('#tabCategorias').addClass('tabActive');
            $('#divTabDetalleCategoria').hide();
            $('#tabDetalleCategoria').removeClass('tabActive');
            $('#tabDetalleCategoria').text(TextosCat[38]);
            var btnAceptarEx = $('[id$=btnAceptarEx]').attr('id');
            __doPostBack(btnAceptarEx, 'Reload');
        }
    }
});
$('#btnCancelarCategoria').live('click', function () {
    $('#divTabCategorias').show();
    $('#tabCategorias').addClass('tabActive');
    $('#divTabDetalleCategoria').hide();
    $('#tabDetalleCategoria').removeClass('tabActive');
    $('#tabDetalleCategoria').text(TextosCat[38]);
});
$('#divListaPara').live('click', function () {
    if (!($('#divOpcionesPara').is(':visible')) && $('#divListaPara').children().length == 0) {
        var IdSeleccionado = $('.ItemNuevoMensajeParaSeleccionado').attr('id');
        $('#' + IdSeleccionado).removeClass('ItemNuevoMensajeParaSeleccionado');
        $('#' + IdSeleccionado + 'Panel').hide();

        $('#divOpcionesPara').show();
        $('#divParaUON').addClass('ItemNuevoMensajeParaSeleccionado');
        $('#divParaUONPanel').show();
        ControlPanelPara("divParaUON");
    }
});
$('#divParaDestinatarios .ItemNuevoMensajePara').live('click', function () {
    $('#divOpcionesPara').show();

    var IdSeleccionado = $('.ItemNuevoMensajeParaSeleccionado').attr('id');
    $('#' + IdSeleccionado).removeClass('ItemNuevoMensajeParaSeleccionado');
    $('#' + IdSeleccionado + 'Panel').hide();

    var IdNuevaSeleccion = $(this).attr('id');
    $('#' + IdNuevaSeleccion).addClass('ItemNuevoMensajeParaSeleccionado');
    $('#' + IdNuevaSeleccion + 'Panel').show();

    ControlPanelPara(IdNuevaSeleccion);
});
$('#divListaPara').focusout(function () {
    $('.ItemParaSeleccionado').removeClass("ItemParaSeleccionado");
});
$('#divListaPara').live(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
    var code;
    if (event.keyCode) code = event.keyCode;
    else if (event.which) code = event.which;

    if (code == KEY.DEL || code == KEY.BACKSPACE) {
        $('.ItemParaSeleccionado').remove();
    }
    return false;
});
function ExportarExcel() {
    /*
    ''' <summary>
    ''' Exporta a Excel
    ''' </summary>
    ''' <remarks>Llamada desde: onclick del div q contiene al link excel y a la img excel; Tiempo máximo:0,5</remarks>
    ''' <revisado>JVS 23/02/2012</revisado>
    */
    __doPostBack('ExportarExcel', '')
}
function ExportarPDF() {
    /*
    ''' <summary>
    ''' Exporta a Pdf
    ''' </summary>
    ''' <remarks>Llamada desde: onclick del div q contiene al link pdf y a la img pdf; Tiempo máximo:0,5</remarks>
    ''' <revisado>JVS 23/02/2012</revisado>
    */
    __doPostBack('ExportarPDF', '')
}
function CreaComboCategoriasPadre(numCombo) {
    var nombreCombo = '#cboParaCategorias' + numCombo;
    var nombredivCombo = 'cboParaCategorias' + numCombo;
    var cboPadres = '';

    if (numCombo == 1) {
        var optionsCategoria = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, WebMethodURL: rutaFS + 'cn/App_Services/CN.asmx/Obtener_ListaCategorias1' };
    }
    else {
        for (var i = 1; i < numCombo; i++) {
            cboPadres = cboPadres + 'cboParaCategorias' + i;
            if (i < numCombo - 1) cboPadres = cboPadres + ',';
        }
        var optionsCategoria = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, fsComboParents: cboPadres, WebMethodURL: rutaFS + 'cn/App_Services/CN.asmx/Obtener_ListaCategorias' };
    }
    $('#divCombos').append('<span id="' + nombredivCombo + '" style="float:left; width:200px; margin:0px 5px;"></span>');

    $(nombreCombo).fsCombo(optionsCategoria);
    $(nombreCombo).fsCombo('onSelectedValueChange', function () {
        var valor = $(nombreCombo).fsCombo('getSelectedValue');
        var numCboHijo = parseInt(nombreCombo.substring(18));
        numCboHijo++;
        CargarCategorias(numCombo, valor, nombreCombo);
    });
    $(nombreCombo).fsCombo('refresh');
    if ($(nombreCombo).fsCombo('itemsCount') > 1) {
        $(nombreCombo).fsCombo('enable');
        $('#divCatPadre').css('display', '');
    }
    else {
        $(nombreCombo).fsCombo('destroy');
        $(nombreCombo).remove();
        if (numCombo == 1) $('#divCatPadre').css('display', 'none');
    }
}
function CargarCategorias(numCombo, valor) {
    var nuevoNum = numCombo + 1
    var nombreComboNuevo = '#cboParaCategorias' + nuevoNum;
    var nombreComboAnt = '#cboParaCategorias' + numCombo;
    if (valor == '') {
        $(nombreComboNuevo).fsCombo('destroy');
        $(nombreComboNuevo).remove();
    }
    else {
        CreaComboCategoriasPadre(nuevoNum);
    }
}
function CargarDatosCategoria(idCateg) {
    var params = { iIdCat: idCateg };
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'cn/App_Services/CN.asmx/Load_Datos_Categoria',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        var oCat = msg.d;
        //Textos denominación
        var long = IdiomasCod.length;
        for (var i = 0; i < long; i++) {
            $('#txtDenCat_' + IdiomasCod[i]).val(oCat.Denominaciones[IdiomasCod[i]]);
        }
        $('#cboParaCatPermisoPublic').fsCombo('selectItem', oCat.PermisoParaPub + 1);
        //Cargar combos categoría padre
        if (oCat.PathCategoria == '') {
            $('#divCatPadre').css('display', 'none');
        } else {
            $('#divCatPadre').css('display', '');
            $('#divCombos').css('display', 'none');
            $('#lblPathCateg').text(oCat.PathCategoria);
        }
        if (oCat.Despublicado) {
            $('#chkPublicada').attr('checked', false);
        } else {
            $('#chkPublicada').attr('checked', true);
        }
        $('#divMiembrosCategoria').css('display', 'none');
    });
    GetMiembrosCategoria(idCateg, true);    
}
function GetMiembrosCategoria(idCateg, loadMiembros) {
    // Carga los miembros que van a poder publicar mensajes en esa categoría    
    $('#divMiembrosCategoria').css('display', 'none');
    if (loadMiembros) {
        $('#divParaDestinatarios').empty();
        $.get(rutaFS + 'cn/html/_mensaje_para.htm', function (paraOpciones) {
            paraOpciones = paraOpciones.replace(/src="/gi, 'src="' + ruta);
            $('#divParaDestinatarios').append(paraOpciones);
            $('#divParaProve').remove();
            $('#divParaProceCompras').remove();
            $('#divParaEstrucCompras').remove();
            $('#divParaProvePanel').remove();
            $('#divParaProceComprasPanel').remove();
            $('#divParaEstrucComprasPanel').remove();
            $('#divParaGruposPanel .divLista').remove(); //quitamos la lista de grupos corporativos por defecto (usuarios de QA, de GS,...) que no aplica aquí
            ControlPanelPara("divParaUON");
            if (idCateg != 0) {
                if (!$('#txtSelCategoria').attr("itemValue") || $('#txtSelCategoria').attr("itemValue") == '') {
                    $.when($.ajax({
                        type: "POST",
                        url: rutaFS + 'cn/App_Services/CN.asmx/Obtener_Miembros_Categoria',
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify({ idCategoria: idCateg }),
                        dataType: "json",
                        async: false
                    })).done(function (msg) {
                        $('#tvNuevoMensajeCategorias').tree('selectNodeValue', idCateg);
                        SeleccionarCategoria();
                        CargarParaAnterior(msg.d[0], '');
                    });
                }
            }
        });
    }
    switch (parseInt($('#cboParaCatPermisoPublic').fsCombo('getSelectedValue'))) {
        case 0:
            $('#divMiembrosCategoria').css('display', '');
            break;
        default:
            $('#divMiembrosCategoria').css('display', 'none');
            break;
    }
}
function SeleccionarCategoria() {
    var selectedCategoria = $('#tvNuevoMensajeCategorias .selected');
    var texto;
    var txtCategoria = $('#txtSelCategoria');
    txtCategoria.attr("itemValue", selectedCategoria.attr("itemValue"));
    texto = selectedCategoria.text();
    while ($('#' + selectedCategoria.attr("parentId")).length > 0) {
        selectedCategoria = $('#' + selectedCategoria.attr("parentId"));
        texto = selectedCategoria.text() + ">" + texto;
    }
    txtCategoria.val(texto);
}
function ControlPanelPara(Id) {
    switch (Id) {
        case 'divParaUON':
            if ($('#divParaUON').attr('cargar') == '') {
                $('#txtParaUON').attr('disabled', true);
                EstablecerTextosCategoria('ParaUON');

                $.when($.ajax({
                    type: "POST",
                    url: rutaFS + 'cn/App_Services/CN.asmx/Obtener_ListaTodasCategorias',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true
                })).done(function (msg) {
                    var data = msg.d;
                    $('#tvParaUONTree').tree({
                        data: data,
                        autoOpen: true,
                        selectable: false
                    });
                });
                $('#tvParaUONTree .treeTipo3').live('click', function () {
                    if ($('#divListaPara #' + $(this).attr("id").replace('tvParaUONTree', 'itemParaUON')).length == 0) {
                        $('#divListaPara').append('<div class="ItemPara" id="' + $(this).attr('id').replace('tvParaUONTree', 'itemParaUON') + '">' + $(this).text() + '</div>');
                    }
                });
                $('#txtParaUON').bind(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
                    var code;
                    if (event.keyCode) code = event.keyCode;
                    else if (event.which) code = event.which;

                    switch (code) {
                        case KEY.RETURN:
                            var selectedItem = $('#tvParaUONTree span.treeSeleccionable.selected');
                            if (selectedItem.length != 0) {
                                if ($('#divListaPara #' + $(selectedItem).attr("id").replace('tvParaUONTree', 'itemParaUON')).length == 0) {
                                    $('#divListaPara').append('<div class="ItemPara" id="' + $(selectedItem).attr('id').replace('tvParaUONTree', 'itemParaUON') + '">' + $(selectedItem).text() + '</div>');
                                }
                            }
                            $('#txtParaUON').val('');
                            $('#tvParaUONTree span.treeSeleccionable.selected').removeClass('selected');
                            event.preventDefault();
                            return false;
                            break;
                        default:
                            EncontrarItemTreeView('tvParaUONTree', 'txtParaUON', code);
                            break;
                    }
                });
                $('#txtParaUON').removeAttr("disabled");
                $('#divParaUON').removeAttr('cargar');
            }
            $('#txtParaUON').focus();
            break;
        case 'divParaGrupos':
            if ($('#divParaGrupos').attr('cargar') == '') {
                EstablecerTextosCategoria('ParaGrupos');
                var optionsGrupos = { valueField: "value", textField: "text", isDropDown: false, MinimumPrefixLength: 0, itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, adjustWidthToSpace: true, WebMethodURL: rutaFS + 'cn/App_Services/CN.asmx/Obtener_GruposCategorias' };
                $('#cboParaGruposGruposCorporativos').fsCombo(optionsGrupos);
                $('#cboParaGruposGruposCorporativos').fsCombo('onSelectedValueChange', function () { CargarParaGruposCorporativosCateg() });
                $('.ItemParaGrupo').live('click', function () {
                    var idDiv = (this.id).replace('div', '');
                    if ($('#divListaPara #' + idDiv).length == 0) {
                        $('#divListaPara').append('<div class="ItemPara" id="' + idDiv + '">' + $(this).find('span').text() + '</div>');
                    }
                });
                $('#divParaGrupos').removeAttr('cargar');
            }
            break;
        default:
            break;
    }
}
function CalcularScroll(element) {
    var scrollTop = 0;
    $.each(element.children('li'), function () {
        if ($(this).children('span').hasClass('selected')) {
            return false;
        } else {
            if ($(this).hasClass('folder')) {
                if ($(this).children('.toggler').hasClass('closed')) {
                    scrollTop += $(this).children('span')[0].offsetHeight;
                } else {
                    scrollTop += $(this).children('span')[0].offsetHeight;
                    scrollTop += CalcularScroll($(this).children('ul'));
                    return false;
                }
            } else {
                scrollTop += $(this).children('span')[0].offsetHeight;
            }
        }
    });
    return scrollTop;
}
var GuardadoOK;
function GuardarCategoriaBD(CNPermisoPublic, CNDenomCat, CNDespublicado, CNIdCat) {
    var CNIdCategoria = parseInt(CNIdCat);
    var params = '';
    var categoria = '';
    var paraCategoria = ObtenerParaCategoria();
    var validar;

    validar = ValidarDatos(CNDenomCat, CNPermisoPublic, paraCategoria);

    if (validar == 1) {        
        alert(TextosCat[36]);
        return false;
    }
    if (validar == 2) {
        alert(TextosCat[37]);
        return false;
    }
    var CNFechaAlta = new Date();
    var CNNivel;

    if (CNIdCategoria == 0) {
        //CATEGORIA
        CNNivel = 0;
        var numCombos = parseInt($('[id^=cboParaCategorias]').length);
        var valor = $('[id=cboParaCategorias' + numCombos + ']').fsCombo('getSelectedValue');
        if (valor == "") {
            numCombos = numCombos - 1
        }
        if (numCombos == 0) {
            numCombos = 1
            CNNivel = 1;
        }
        if (numCombos == -1) {
            CNNivel = 1;
            CNIdCategoriaPadre = 0;
        }else {
            var nombreCombo = '#cboParaCategorias' + numCombos;
            var CNIdCategoriaPadre
            if ($(nombreCombo).fsCombo('getSelectedValue') != "") {
                CNIdCategoriaPadre = $(nombreCombo).fsCombo('getSelectedValue');
            }
            else {
                CNIdCategoriaPadre = 0;
            }
            if (CNNivel == 0) CNNivel = numCombos + 1;
        }
        if (CNIdCategoriaPadre.length == 0) CNIdCategoriaPadre = 0;
        params = { IdCategoriaPadre: CNIdCategoriaPadre, Nivel: CNNivel, PermisoPublic: CNPermisoPublic, Despublicado: CNDespublicado, FechaAlta: CNFechaAlta, DenomCat: CNDenomCat, Miembros: paraCategoria };

        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'cn/App_Services/CN.asmx/InsertarCategoria',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(params),
            dataType: "json",
            async: false
        })).done(function () {
            $('#lblCategoriaGuardadaOK').show();
            GuardadoOK = setTimeout(function () { clearTimeout(GuardadoOK); $('#lblCategoriaGuardadaOK').hide(); }, 3000);
        });
    } else {
        //CATEGORIA
        params = { IdCateg: CNIdCategoria, PermisoPublic: CNPermisoPublic, Despublicado: CNDespublicado, DenomCat: CNDenomCat, Miembros: paraCategoria };
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'cn/App_Services/CN.asmx/UpdateCategoria',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(params),
            dataType: "json",
            async: true
        })).done(function () {
            $('#lblCategoriaGuardadaOK').show();            
            GuardadoOK = setTimeout(function () { clearTimeout(GuardadoOK); $('#lblCategoriaGuardadaOK').hide(); },3000);
        });
    }
    return true;
}
function BorrarCategoriaBD(CNIdCat) {
    var CNIdCategoria = parseInt(CNIdCat);
    var params = { IdCateg: CNIdCategoria }
    
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'cn/App_Services/CN.asmx/DeleteCategoria',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        $('#divCargando').hide();
    });
}
function EstablecerTextosCategoria(tipo) {
    switch (tipo) {
        case 'ParaUON':
            $('#lblParaUON').text(TextosCat[28]);
            break;
        case 'ParaGrupos':
            $('#lblParaGruposEP').text(TextosCat[29]);
            $('#lblParaGruposGS').text(TextosCat[30]);
            $('#lblParaGruposPM').text(TextosCat[31]);
            $('#lblParaGruposQA').text(TextosCat[32]);
            $('#lblParaGruposSM').text(TextosCat[33]);
            $('#lblParaGruposGruposCorporativos').text(TextosCat[34] + ':');
            break;
    }
}
function CargarParaGruposCorporativosCateg() {
    if ($('#cboParaGruposGruposCorporativos').fsCombo('getSelectedValue') != "") {
        var value = $('#cboParaGruposGruposCorporativos').fsCombo('getSelectedValue');
        var text = $('#cboParaGruposGruposCorporativos').fsCombo('getSelectedText');

        if ($('#divListaPara #itemParaGrupo-Uno-' + value).length == 0) {
            $('#divListaPara').append('<div class="ItemPara" id="itemParaGrupo-Uno-' + value + '">' + text + '</div>');
        }

        $('#cboParaGruposGruposCorporativos').fsCombo('selectValue', '');
    }
}
function CargarParaAnterior(datos) {
    $.each(datos, function () {
        var texto = this.text;
        switch (this.value.split('-')[0]) {
            case 'itemParaGrupo':
                switch (this.value.split('-')[1]) {
                    case 'EP':
                        texto = 'EP';
                        break;
                    case 'GS':
                        texto = 'GS';
                        break;
                    case 'PM':
                        texto = 'PM';
                        break;
                    case 'QA':
                        texto = 'QA';
                        break;
                    case 'SM':
                        texto = 'SM';
                        break;
                }
                break;
        }

        $('#divListaPara').append('<div class="ItemPara" id="' + this.value + '">' + texto + '</div>');
    });
}
function ObtenerParaCategoria() {
    //PARA
    var UON0, UON1, UON2, UON3, DEP;
    var tipoProve, codigoProve, idContacto, idMatQA, gmn1, gmn2, gmn3, gmn4, prove;
    var anyo, cod, proceCompras, num;
    var tipoEstrucCompras, codigoEstrucCompras;
    var tipoGrupo, idGrupo, grupo;
    var para = { UON: [], PROVE: [], PROCECOMPRAS: [], ESTRUCCOMPRAS: [], GRUPO: [] };
    $.each($('#divListaPara .ItemPara'), function () {
        switch ($(this).attr("id").split("-")[0]) {
            case "itemParaUON":
                UON0 = true;
                UON1 = null;
                UON2 = null;
                UON3 = null;
                DEP = null;
                for (i = 1; i < $(this).attr("id").split("-").length; i++) {
                    switch ($(this).attr("id").split("-")[i].split("_")[0]) {
                        case "UON1":
                            UON0 = false;
                            UON1 = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "UON2":
                            UON0 = false;
                            UON2 = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "UON3":
                            UON0 = false;
                            UON3 = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "DEP":
                            UON0 = false;
                            DEP = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                    }
                }
                if (UON0) {
                    codigo = { UON1: null, UON2: null, UON3: null, DEP: null };
                } else {
                    codigo = { UON1: UON1, UON2: UON2, UON3: UON3, DEP: DEP };
                }
                para.UON.push(codigo)
                break;
            case "itemParaGrupo":
                tipoGrupo = null, idGrupo = null;
                switch ($(this).attr("id").split("-")[1]) {
                    case "EP":
                        tipoGrupo = 'EP';
                        break;
                    case "GS":
                        tipoGrupo = 'GS';
                        break;
                    case "PM":
                        tipoGrupo = 'PM';
                        break;
                    case "QA":
                        tipoGrupo = 'QA';
                        break;
                    case "SM":
                        tipoGrupo = 'SM';
                        break;
                    default:
                        idGrupo = $(this).attr("id").split("-")[2];
                        break;
                }
                grupo = { TIPOGRUPO: tipoGrupo, IDGRUPO: idGrupo };
                para.GRUPO.push(grupo);
                break;
        }
    });
    return para;
}
function ValidarDatos(denomCat, permisoPublic, paraCateg) {
    //validar que la categoría tiene denominación
    var numTot = denomCat.CNDenomCat.length
    for (var i = 0; i < numTot; i++) {
        if (denomCat.CNDenomCat[i].text.length == 0)
            return 1;
    }
    //validar que hay al menos 1 miembro
    if (permisoPublic == '0' && (paraCateg.UON.length < 1 && paraCateg.GRUPO.length < 1))
        return 2;

    return 0;
}
//''' <summary>
//''' Comprobar si la categoria tiene mensajes
//''' </summary>
//''' <param name="CNIdCat">categoria</param>
//''' <remarks>Llamada desde: btnEliminarCategoria.live('click'... ; Tiempo máximo: 0</remarks>
function ComprobarEliminarCategoria(CNIdCat) {
    var CNIdCategoria = parseInt(CNIdCat);
    var params = { IdCateg: CNIdCategoria }
    var Res

    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'cn/App_Services/CN.asmx/ComprobarDeleteCategoria',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        Res = msg.d;
    });
    return Res;
}