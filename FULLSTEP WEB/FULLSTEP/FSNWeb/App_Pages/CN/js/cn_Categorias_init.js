﻿var newCategory = true;
$(document).ready(function () {
    if (typeof (EnBlancoMaster) !== 'undefined' && EnBlancoMaster) $('body').addClass('fondoMasterEnBlanco');
    $('#divCargando').hide();
    $.get(rutaFS + 'CN/html/_menu_cn.htm', function (menu) {
        menu = menu.replace(/src="/gi, 'src="' + ruta);
        $('#divMenu').append(menu);
        CargarMenu();
        $('.MenuItemMenuOpcionSeleccionado').removeClass('MenuItemMenuOpcionSeleccionado');
        $('#AdminCategorias').addClass('MenuItemMenuOpcionSeleccionado');
        $('#divPrincipal').show();
    });
    if (MostrarLnkDespublic == 0) {
        $('#OcultarDespublicadas').hide();
    }
    $('#btnAceptarCategoria').attr('IdCategoria', 0);
    EstablecerTextosCategorias();
    CrearCajasTextoCategorias();
    CreaCombos();
    $('#tabCategorias').live('click', function () {
        $('#divTabCategorias').show();
        $('#tabCategorias').addClass('tabActive');
        $('#divTabDetalleCategoria').hide();
        $('#tabDetalleCategoria').removeClass('tabActive');
        $('[id^=cboParaCategorias]').fsCombo('collapseDropDown');
        $('#cboParaCatPermisoPublic').fsCombo('collapseDropDown');
        $('#tabDetalleCategoria').text(TextosCat[38]);
    });
    $('#tabDetalleCategoria').live('click', function () {
        $('#divTabCategorias').hide();
        $('#tabCategorias').removeClass('tabActive');
        if (!($('#divTabDetalleCategoria').is(':visible'))) {
            GetMiembrosCategoria(0, true);
            $('[id^=txtDenCat_]').val('');
            $('#cboParaCatPermisoPublic').fsCombo('selectItem', 3);
            $('#btnAceptarCategoria').attr('IdCategoria', 0);
            $('#btnEliminarCategoria').css('display', 'none');
            //Cargar combos categoría padre
            $('#divCombos').empty();
            CreaComboCategoriasPadre(1);
            $('#divCombos').css('display', '');
            $('#divPathCategoria').css('display', 'none');
            //Cargar tree miembros con permisos de publicación en la categoría
            $('#cboParaCatPermisoPublic').fsCombo('onSelectedValueChange', function () {
                GetMiembrosCategoria(0, false);
            });
        }
        $('#divTabDetalleCategoria').show();
        $('#tabDetalleCategoria').addClass('tabActive');
    });
});
function EstablecerTextosCategorias() {
    $('#lblCatPadre').text(TextosCat[24]);
    $('#lnkOcultarDespublicadas').text(TextosCat[15]);
    $('#lblDenomCat').text(TextosCat[17]);
    $('#lblPermiso').text(TextosCat[18]);
    $('#lblMiembros').text(TextosCat[19]);
    $('#lblPublicada').text(TextosCat[20]);
    $('#lblCategoriaAceptar').text(TextosCat[21]);
    $('#btnAceptarCategoria').attr('title', TextosCat[21]);
    $('#lblCategoriaCancelar').text(TextosCat[22]);
    $('#btnCancelarCategoria').attr('title', TextosCat[22]);
    $('#lblCategoriaEliminar').text(TextosCat[23]);
    $('#btnEliminarCategoria').attr('title', TextosCat[23]);
    //$('#lblPreguntaEliminarConfirm').text(TextosCat[35]);
    $('#lblAceptarEliminarConfirm').text(TextosCat[21]);
    $('#btnAceptarEliminarConfirm').attr('title', TextosCat[21]);
    $('#lblCancelarEliminarConfirm').text(TextosCat[22]);
    $('#btnCancelarEliminarConfirm').attr('title', TextosCat[22]);
    $('#lblCategoriaGuardadaOK').text(TextosCat[39]);
    $('#tabCategorias').text(TextosCat[13]);
    $('#tabDetalleCategoria').text(TextosCat[38]);
}
function CreaCombos() {
    var items = { 0: { value: '0', text: TextosCat[25] }, 1: { value: '1', text: TextosCat[26] }, 2: { value: '2', text: TextosCat[27]} }
    var optionsPermiso = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: false, autoComplete: true, data: items };

    $('#cboParaCatPermisoPublic').fsCombo(optionsPermiso);    
}
function onClient_click() {
    var ocultar = $('#lnkOcultarDespublicadas').text() == TextosCat[15];
    var grid = $find($('[id$=whdgCategorias]').attr('id'));
    var rowsCount = grid.get_gridView().get_rows().get_length();
    var columnsCount = grid.get_gridView().get_columns().get_length();
    var padreoculto;

    var gridRows = grid.get_gridView().get_rows();
    for (var i = 0; i < rowsCount; i++) {
        var cellValue = gridRows.get_row(i).get_cellByColumnKey("NoPublicado").get_value();
        if (cellValue & ocultar) {
            gridRows.get_row(i).get_element().style.display = 'none';
            if (gridRows.get_row(i).get_rowIslands()[0])
                gridRows.get_row(i).get_rowIslands()[0].get_element().style.display = 'none';
        }
        else {
            if (cellValue & !ocultar) {
                gridRows.get_row(i).get_element().style.display = '';
                if (gridRows.get_row(i).get_rowIslands()[0])
                    gridRows.get_row(i).get_rowIslands()[0].get_element().style.display = '';

            }
            else {
                if (!cellValue) {
                    var childGridView = gridRows.get_row(i).get_rowIslands()[0];
                    if (childGridView)
                        GetChildGridView(childGridView, ocultar)
                }
            }
        }
    }
    if (ocultar) {
        $('#lnkOcultarDespublicadas').text(TextosCat[16]);
    }
    else {
        $('#lnkOcultarDespublicadas').text(TextosCat[15]);
    }
}
function GetChildGridView(gridView, ocultar) {
    var rows = gridView.get_rows();
    for (var i = 0; i < rows.get_length(); i++) {
        var childGridView = rows.get_row(i).get_rowIslands()[0];
        var cellValue = rows.get_row(i).get_cellByColumnKey("NoPublicado").get_value();
        if (cellValue) {
            if (ocultar) {
                rows.get_row(i).get_element().style.display = 'none';
            }
            else {
                rows.get_row(i).get_element().style.display = '';
            }
        }
        else {
            if (childGridView) {
                GetChildGridView(childGridView, ocultar);
            }
        }
    }
}
function whdgCategorias_Click(webDataGrid, e, a, c, vb) {
    e.set_cancel(true)
    var idCateg = e.getNewSelectedRows().getRow(0).get_dataKey()[0];

    $('#btnAceptarCategoria').attr('IdCategoria', idCateg);
    $('#btnEliminarCategoria').css('display', '');
    $('#divCombos').css('display', 'none');
    $('#divPathCategoria').css('display', '');

    CargarDatosCategoria(idCateg);

    $('#tabDetalleCategoria').text($('#txtDenCat_' + usuario.IdiomaCod).val());
    $('#divTabCategorias').hide();
    $('#tabCategorias').removeClass('tabActive');
    $('#divTabDetalleCategoria').show();
    $('#tabDetalleCategoria').addClass('tabActive');
    //Cargar tree miembros con permisos de publicación en la categoría
    $('#cboParaCatPermisoPublic').fsCombo('onSelectedValueChange', function () {
        GetMiembrosCategoria(idCateg, false);
    });
}
function CrearCajasTextoCategorias() {
    var long = IdiomasCod.length;
    for (var i = 0; i < long; i++) {
        $('#divDenomCat').append('<div style="width: 100%; clear: both; float: left;"><span id="lblDenCat_' + IdiomasCod[i] + '" class="Texto12" style="margin-bottom: 3px;">' + IdiomasTxt[i] + '</span></div> <div style="width: 100%; clear: both; float: left;"><input type="text" id="txtDenCat_' + IdiomasCod[i] + '" class="CajaTexto" style="width:500px; margin-top: 3px; margin-bottom: 3px;" /></div>');
    }
}