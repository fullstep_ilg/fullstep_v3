﻿var guardando = false;
$('#DiscrepanciabtnAceptarNuevoMensaje').live('click', function () {
    if (ComprobarDatosDiscrepanciaNueva()) {
        GuardarDiscrepancia();
    }
    $('#divNuevoMensajeParaDiscrepancia').attr('new', '');
});
$('#DiscrepanciabtnCancelarNuevoMensaje').live('click', function (event) {
    MostrarFormularioNuevaDiscrepancia(false);
    $('#divNuevoMensajePara').attr('new', '');
    return false;
});
//=========================
//  FUNCIONES NUEVO MENSAJE
//=========================
function ComprobarDatosDiscrepanciaNueva() {
    if (CKEDITOR.instances['DiscrepanciaCKENuevoMensaje'].getData() == '') {
        alert(Textos[93]);
        return false;
    }
    if ($('#DiscrepanciatxtSelCategoria').attr('itemValue') == undefined || $('#DiscrepanciatxtSelCategoria').attr('itemValue') == '') {
        alert(Textos[94]);
        return false;
    }
    if ($('#DiscrepanciadivListaPara .ItemPara').length == 0) {
        alert(Textos[95]);
        return false;
    }
    return true;
}
function GuardarDiscrepancia() {
    var params = ''; var tipo = 1; var mensaje = ''; var categoria = '';
    var para = {}; var fechaEvento; var idNuevo = 0;
    var fechaAlta = new Date();
    var fechaAltaMensaje = UsuMask.replace('dd', fechaAlta.getDate()).replace('MM', fechaAlta.getMonth() + 1).replace('yyyy', fechaAlta.getFullYear());
    fechaAltaMensaje += ' ' + fechaAlta.getHours() + ':' + fechaAlta.getMinutes() + ':' + fechaAlta.getSeconds();
    var editor = CKEDITOR.instances['DiscrepanciaCKENuevoMensaje'];
    var CKEditorImages = [];
    if (editor.mode == 'source') {
        guardando = true;
        editor.setMode('wysiwyg');
        return false;
    }
    $.each(editor.document.getElementsByTag('img').$, function () {
        if ($(this).attr("type") == "CKEditorImage") CKEditorImages.push({ href: $(this).attr('src'), path: '' + $(this).attr('id') + '', filename: '' + $(this).attr('filename') + '', guidID: '' });
    });
    //modifico las url de las imagenes de los link a entidades
    var contenido = $('<div>' + editor.getData() + '</div>');
    $.each($('a[type=entidad] img', contenido), function () {
        img = $(this);
        img = img.attr('src', img.attr('src').replace(ruta, ''));
        $('img', $(this)).replaceWith(img);
    }); 
    //MENSAJE   
    var mensaje =
    {
        id: '' + idNuevo + '',
        fechaAlta: '' + fechaAltaMensaje + '',
        titulo: $('#DiscrepanciatxtNuevoMensajeTitulo').hasClass('textoDefecto') ? '' : '' + $('#DiscrepanciatxtNuevoMensajeTitulo').val() + '',
        contenido: '' + contenido.html() + '',
        fecha: '',
        hora: '',
        timezoneOffset: fechaAlta.getTimezoneOffset(),
        donde: ''
    }
    //CATEGORIA
    categoria =
    {
        id: $('#DiscrepanciatxtSelCategoria').attr('itemValue'),
        denominacion: '' + $('#DiscrepanciatxtSelCategoria').val() + ''
    };

    //ADJUNTOS
    var listaAdjuntos = [];
    $.each($('#DiscrepanciadivListaAdjuntos #tablaadjuntos').children(), function () {
        listaAdjuntos.push({
            nombre: $(this).attr('data-name'),
            size: $(this).attr('data-size'),
            sizeunit: $(this).attr('data-sizeunit'),
            tipoadjunto: $(this).attr('data-tipo'),
            url: $(this).attr('data-url')
        });
    });
    var paraMensaje = ObtenerParaDiscrepancia();
    params = { tipo: tipo, mensaje: mensaje, categoria: categoria, para: paraMensaje, adjuntos: listaAdjuntos, imagenesCKEditor: CKEditorImages, factura: idFactura, linea: linea };

    MostrarFormularioNuevaDiscrepancia(false);
        
    $('#imgDiscrepancias_' + linea).show();
    $('#imgDiscrepanciasCerradas_' + linea).hide();
    $.ajax({
        type: "POST",
        url: rutaFS + 'CN/App_Services/CN.asmx/InsertarMensaje',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: true
    });
}
function ObtenerParaDiscrepancia() {
    //PARA
    var UON0, UON1, UON2, UON3, DEP, USU;
    var tipoProve, codigoProve, idContacto, idMatQA, gmn1, gmn2, gmn3, gmn4, prove;
    var anyo, cod, proceCompras, num;
    var tipoEstrucCompras, codigoEstrucCompras;
    var tipoGrupo, idGrupo, grupo;
    var para = { UON: [], PROVE: [], PROCECOMPRAS: [], ESTRUCCOMPRAS: [], GRUPO: [] };
    $.each($('#DiscrepanciadivListaPara .ItemPara'), function () {
        switch ($(this).attr("id").split("-")[0]) {
            case 'itemParaUON':
                UON0 = true;
                UON1 = null;
                UON2 = null;
                UON3 = null;
                DEP = null;
                USU = null;
                for (i = 1; i < $(this).attr("id").split("-").length; i++) {
                    switch ($(this).attr("id").split("-")[i].split("_")[0]) {
                        case "UON1":
                            UON0 = false;
                            UON1 = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "UON2":
                            UON0 = false;
                            UON2 = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "UON3":
                            UON0 = false;
                            UON3 = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "DEP":
                            UON0 = false;
                            DEP = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "USU":
                            UON0 = false;
                            USU = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                    }
                }
                if (UON0) {
                    codigo = { UON1: null, UON2: null, UON3: null, DEP: null, USU: null };
                } else {
                    codigo = { UON1: UON1, UON2: UON2, UON3: UON3, DEP: DEP, USU: USU };
                }
                para.UON.push(codigo)
                break;
            case "itemParaProve":
                tipoProve = null; codigoProve = null; idContacto = null; idMatQA = null;
                gmn1 = null; gmn2 = null; gmn3 = null; gmn4 = null;
                switch ($(this).attr("id").split("-")[1]) {
                    case "Todos":
                        tipoProve = 1;
                        break;
                    case "Calidad":
                        tipoProve = 2;
                        break;
                    case "Potenciales":
                        tipoProve = 3;
                        break;
                    case "Reales":
                        tipoProve = 4;
                        break;
                    case "MatQA":
                        tipoProve = 5;
                        idMatQA = $(this).attr("id").split("-")[2]
                        break;
                    case "MatGS":
                        tipoProve = 6;
                        num = $(this).attr("id").split("-").length;
                        switch (true) {
                            case (num > 5):
                                gmn4 = $(this).attr("id").split("-")[5];
                            case (num > 4):
                                gmn3 = $(this).attr("id").split("-")[4];
                            case (num > 3):
                                gmn2 = $(this).attr("id").split("-")[3];
                            case (num > 2):
                                gmn1 = $(this).attr("id").split("-")[2];
                                break;
                        }
                        break;
                    case "Uno":
                        tipoProve = 0;
                        idContacto = $(this).attr('id').split('-')[$(this).attr('id').split('-').length - 1];
                        codigoProve = $(this).attr('id').replace('itemParaProve-Uno-', '').replace('-' + idContacto, '');
                        break;
                }
                prove = { TIPOPROVE: tipoProve, CODIGOPROVE: codigoProve, IDCONTACTO: idContacto, MATQA: idMatQA, GMN1: gmn1, GMN2: gmn2, GMN3: gmn3, GMN4: gmn4 };
                para.PROVE.push(prove);
                break;
            case "itemParaProceCompras":
                anyo = $(this).attr("id").split("-")[2];
                gmn1 = $(this).attr("id").split("-")[3];
                cod = $(this).attr("id").split("-")[4];
                proceCompras = { ANYO: anyo, GMN1: gmn1, COD: cod, TIPOPARAPROCECOMPRA: $(this).attr("id").split("-")[1] };
                para.PROCECOMPRAS.push(proceCompras);
                break;
            case "itemParaEstrucCompras":
                tipoEstrucCompras = null; codigoEstrucCompras = null;
                gmn1 = null; gmn2 = null; gmn3 = null; gmn4 = null;
                //TIPO: 1-Equipo de compra, 2-Usuario
                switch ($(this).attr("id").split("-")[1]) {
                    case 'Eqp':
                        tipoEstrucCompras = 1;
                        codigoEstrucCompras = $(this).attr("id").split("-")[2].split("_")[1];
                        break;
                    case 'Usu':
                        tipoEstrucCompras = 2;
                        codigoEstrucCompras = $(this).attr("id").split("-")[3].split("_")[1];
                        break;
                    case 'MatGS':
                        tipoEstrucCompras = 3;
                        num = $(this).attr("id").split("-").length;
                        switch (true) {
                            case (num > 5):
                                gmn4 = $(this).attr("id").split("-")[5];
                            case (num > 4):
                                gmn3 = $(this).attr("id").split("-")[4];
                            case (num > 3):
                                gmn2 = $(this).attr("id").split("-")[3];
                            case (num > 2):
                                gmn1 = $(this).attr("id").split("-")[2];
                                break;
                        }
                        break;
                }
                estrucCompras = { TIPOESTRUCCOMPRAS: tipoEstrucCompras, CODIGO: codigoEstrucCompras, GMN1: gmn1, GMN2: gmn2, GMN3: gmn3, GMN4: gmn4 };
                para.ESTRUCCOMPRAS.push(estrucCompras);
                break;
            case "itemParaGrupo":
                tipoGrupo = null, idGrupo = null;
                switch ($(this).attr("id").split("-")[1]) {
                    case "EP":
                        tipoGrupo = 'EP';
                        break;
                    case "GS":
                        tipoGrupo = 'GS';
                        break;
                    case "PM":
                        tipoGrupo = 'PM';
                        break;
                    case "QA":
                        tipoGrupo = 'QA';
                        break;
                    case "SM":
                        tipoGrupo = 'SM';
                        break;
                    default:
                        idGrupo = $(this).attr("id").split("-")[2];
                        break;
                }
                grupo = { TIPOGRUPO: tipoGrupo, IDGRUPO: idGrupo };
                para.GRUPO.push(grupo);
                break;
        }
    });
    return para;
}
//==============================================
//============== DISCREPANCIAS =================
//==============================================
function MostrarFormularioNuevaDiscrepancia(show, idFactura, numFactura, linea, pedido, albaran) {
    $('#DiscrepanciadivSelCategoria').hide();
    if (show) {
        $('#fileupload').attr('mensaje', '');

        $('#DiscrepanciadivNuevoMensaje').show();
        $('#DiscrepanciadivBotones').show();

        $('#DiscrepanciadivNuevoMensajeDefecto').hide();

        $('#tablaadjuntos').remove();
        $('#DiscrepanciadivListaAdjuntos').append('<div id="tablaadjuntos" class="files"></div>');

        $('#DiscrepanciabtnAceptarNuevoMensaje').attr('IdMensaje', 0);

        $('#DiscrepanciadivNuevoMensajeFechaHora').hide();
        if ($('#DiscrepanciatxtNuevoMensajeTitulo').hasClass('textoDefecto')) $('#DiscrepanciatxtNuevoMensajeTitulo').removeClass('textoDefecto');
        $('#divCabeceraDiscrepanciaDiscrepancia').css('display', '');

        var editor = CKEDITOR.instances['DiscrepanciaCKENuevoMensaje'];
        var htmlEditor = '';
        if (editor) {
            htmlEditor = editor.getData();
            editor.destroy(true);
        }
        var CodigoEntidadColaboracion = { identificador: idFactura + '/' + linea,
            texto: TextosCKEditor[23] + ' ' + numFactura + ' - ' + TextosCKEditor[24] + ' ' + linea + ' (' + TextosCKEditor[18] + ': ' + pedido + (albaran == '' ? '' : ' ' + TextosCKEditor[25] + ' ' + albaran) + ')'
        };
        editor = CKEDITOR.replace('DiscrepanciaCKENuevoMensaje', { customConfig: ruta + 'ckeditor/configCN.js',
            on: { instanceReady: function (ev) {
                ev.sender.insertHtml(htmlEditor);
                ImageUploadPlugin(ev.editor.name);
                MostrarCrearDiscrepancia(CodigoEntidadColaboracion);
                $('#DiscrepanciatxtNuevoMensajeTitulo').focus();
                ev.editor.on('paste', function (ev) { CKEditorPaste(ev); });
            },
                mode: function (ev) { if (guardando) { guardando = false; timeoutNuevoMensaje = setTimeout(function () { clearTimeout(timeoutNuevoMensaje); GuardarDiscrepancia() }, 50); } }
            }
        });
        scayt_READY = setInterval(function () { SCAYTready('DiscrepanciaCKENuevoMensaje') }, 500);
        $('#DiscrepanciatxtNuevoMensajeTitulo').val(TextosCKEditor[26] + ' ' + CodigoEntidadColaboracion.texto);

        if ($('#DiscrepanciabtnAceptarNuevoMensaje').attr('IdMensaje') == '0') {
            $('#DiscrepanciadivNuevoMensajeParaOpciones').empty();
            $('#DiscrepanciadivParaDestinatarios').empty();
            $.get(rutaFS + 'CN/html/_mensaje_para.htm', function (paraOpciones) {
                paraOpciones = paraOpciones.replace(/src="/gi, 'src="' + ruta);
                $('#DiscrepanciadivNuevoMensajeParaOpciones').append(paraOpciones);
                $.each($('#DiscrepanciadivNuevoMensajeParaOpciones' + ' [id]'), function () {
                    $(this).attr('id', 'Discrepancia' + $(this).attr('id'));
                });
                $('#DiscrepanciadivParaOpciones').hide();
                $('#DiscrepanciadivListaPara').attr('usu', '');
                if ($('#DiscrepanciadivNuevoMensajePara').attr('new') == '') {
                    CargarDatos_NuevaDiscrepancia(idFactura, linea);
                }
            });
        }
    } else {
        var editor = CKEDITOR.instances['DiscrepanciaCKENuevoMensaje'];
        if (editor) { editor.destroy(true) };

        $('#DiscrepanciabtnAceptarNuevoMensaje').removeAttr('IdMensaje');

        $('#DiscrepanciadivListaAdjuntos' + ' #tablaadjuntos').empty();

        $('#DiscrepanciatxtSelCategoria').removeAttr("itemValue")
        $('#DiscrepanciatxtSelCategoria').val('');

        $('#DiscrepanciadivListaPara' + ' div').remove();
                
        $('#popupFondo').hide();
        $('#popupNuevaDiscrepancia').hide();

        $('#DiscrepanciatxtNuevoMensajeTitulo').val('');
    }
    $('[id^=DiscrepanciaNuevoMensajeAdjunto]').live('mouseenter', function () {
        $(this).addClass('Seleccionable');
        $('#fileupload .fileinput-button').css('width', $(this).outerWidth());
        $('#fileupload .fileinput-button').css('height', $(this).outerHeight());
        if ($(this).attr('id').indexOf('Master') == 0) {
            $('#fileupload .fileinput-button').css('left', $(this).position().left);
            $('#fileupload .fileinput-button').css('top', $(this).position().top);
        } else {
            $('#fileupload .fileinput-button').css('left', $(this).offset().left);
            $('#fileupload .fileinput-button').css('top', $(this).offset().top);
        }
        $('#fileupload .fileinput-button').css('z-index', 10000);
        $('#fileupload .fileinput-button').css('right', '');
    });

    return false;
}
function MostrarCrearDiscrepancia(CodigoEntidadColaboracion) {   
    CKEDITOR.plugins.registered.FSDiscrepancia.setSelectedValue(CodigoEntidadColaboracion);
}
function CargarDatos_NuevaDiscrepancia(factura, linea) {
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'PM/App_Services/Facturas.asmx/Obtener_Datos_NuevaDiscrepancia',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ Prove: CodProve }),
        dataType: "json",
        async: false
    })).done(function (msg) {
        var data = msg.d;

        if (data != '' && data != null) {
            $('#DiscrepanciatxtSelCategoria').attr('itemValue', data[0].value);
            $('#DiscrepanciatxtSelCategoria').val(data[0].text);
            $.each(data, function (item) {
                if (item != 0)
                    $('#DiscrepanciadivListaPara').prepend('<div class="ItemPara" id="itemParaProve-Uno-' + this.value + '">' + this.text + '</div>');
            });
        }
        $('#popupNuevaDiscrepancia').show();
    });
}
/* TECLAS POSIBLES PULSADAS PARA SU POSIBLE TRATAMIENTO */
var KEY = {
    UP: 38,
    DOWN: 40,
    DEL: 46,
    TAB: 9,
    RETURN: 13,
    ESC: 27,
    COMMA: 188,
    PAGEUP: 33,
    PAGEDOWN: 34,
    BACKSPACE: 8
};