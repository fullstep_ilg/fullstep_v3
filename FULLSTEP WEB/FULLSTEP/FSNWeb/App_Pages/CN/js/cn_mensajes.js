﻿//CN_MENSAJES.JS
(function ($) {
    $.fn.cn_ObtenerMensajesUsuario = function (options) {
        var wall = this;
        var wallId = this.attr('id');
        $('#divCargando').show();
        var cn = { posts: [] };
        var timeoffset = new Date().getTimezoneOffset();
        var GestionarSiguientesHistorico = true;
        var MostrarOcultos = $('#divOcultos').is(':visible'); var MostrarSiguientes = false; var MostrarHistorico = false;
        $.when($.ajax({
            type: "POST",
            url: options.WebMethodURL,
            data: JSON.stringify({ tipo: options.tipo, grupo: options.grupo, categoria: options.categoria,
                megusta: options.megusta,
                pagina: options.pagina, timezoneoffset: timeoffset, historico: options.historico,
                discrepancias: (options.discrepancias ? options.discrepancias : false),
                factura: (options.factura ? options.factura : 0), linea: (options.linea ? options.linea : 0)
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true
        })).done(function (msg) {
            mensajesCN = msg.d;
            if (!mensajesCN) { $('#divCargando').hide(); return false; }
            if ((!options.historico && mensajesCN.length == 0 && options.pagina > 1) || (!options.historico && mensajesCN.length < LimiteMensajesCargados)) {
                MostrarSiguientes = false;
                GestionarSiguientesHistorico = false;
                $.when($.ajax({
                    type: "POST",
                    url: rutaFS + 'CN/App_Services/CN.asmx/Comprobar_HayMensajesHistoricos',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true
                })).done(function (msg) {
                    if (msg.d) MostrarHistorico = true;
                    OpcionesOtrosMensajes(MostrarSiguientes, MostrarOcultos, MostrarHistorico);
                });
            }
            if (options.historico && mensajesCN.length > 0) MostrarHistorico = true;
            $.each(mensajesCN, function () {
                this.FechaAlta = eval("new " + this.FechaAlta.slice(1, -1));
                this.FechaActualizacion = eval("new " + this.FechaActualizacion.slice(1, -1));

                this.FechaAltaRelativa = relativeTime(this.FechaAlta);
                this.FechaActualizacionRelativa = relativeTime(this.FechaActualizacion);

                //modifico las url de las imagenes de los link a entidades
                var contenido = $('<div>' + this.Contenido + '</div>');
                $.each($('a[type=entidad] img', contenido), function () {
                    img = $(this);
                    img = img.attr('src', img.attr('src').replace(img.attr('src').split('images/')[0], ''));
                    img = img.attr('src', ruta + img.attr('src'));
                    $('img', $(this)).replaceWith(img);
                });
                $.each($('img[type=CKEditorImage]', contenido), function () {
                    img = $(this);
                    img = img.attr('src', img.attr('src').replace(img.attr('src').split('Thumbnail.ashx')[0], ''));
                    img = img.attr('src', rutaFS + 'CN/' + img.attr('src'));
                    $('img', $(this)).replaceWith(img);
                });
                this.Contenido = contenido.html();
                if (this.Respuestas) {
                    $.each(this.Respuestas, function () {
                        this.FechaAlta = eval("new " + this.FechaAlta.slice(1, -1));
                        this.FechaActualizacion = eval("new " + this.FechaActualizacion.slice(1, -1));

                        this.FechaAltaRelativa = relativeTime(this.FechaAlta);
                        this.FechaActualizacionRelativa = relativeTime(this.FechaActualizacion);
                    });
                }
                if (this.Oculto) MostrarOcultos = true;
                cn.posts.push(this);
            });

            $('#divMensajesResultadosBusqueda').empty();
            // Crear lista para los post
            $('#divOpcionesOtrosMensajes').before($('<ul>'));
            var ul = $('#' + wallId + ' ul').last();
            if (options.discrepancias) {
                $('#itemDiscrepancia').tmpl(cn.posts).appendTo(ul);
            } else {
                $('#itemMensaje').tmpl(cn.posts).appendTo(ul);
            };
            $('#divCargando').hide();
            if (cn.posts.length > 0) {
                $('#' + wallId).show();
                if (msg.d.length == LimiteMensajesCargados) {
                    MostrarSiguientes = options.historico ? false : true;
                }
            }
            if (GestionarSiguientesHistorico) OpcionesOtrosMensajes(MostrarSiguientes, MostrarOcultos, MostrarHistorico);
            if (options.scroll) $(window).scrollTop(options.scroll);
            /*====================================
            =========== TEMPLATES INFO ME GUSTA
            =====================================*/
            $('[id^=msgTmplMeGusta_]').text(Textos[85]);
            $.each($('[id^=msgTmplLeGusta_]'), function () {
                $(this).text(Textos[86].replace('###', $(this).text()));
            });
            $.each($('[id^=msgTmplLeGustaMeGusta_]'), function () {
                $(this).text(Textos[87].replace('###', $(this).text()));
            });
            $.each($('[id^=msgTmplMeGustaLesGusta_]'), function () {
                $(this).html(Textos[88].replace('@@@', $(this).html().replace('$$$', Textos[91])));
            });
            $.each($('[id^=msgTmplLeGustaMeGustaLesGusta_]'), function () {
                $(this).html(Textos[89].replace('@@@', $(this).html().replace($(this).text().replace($(this).children('span').text(), ''), '').replace('$$$', Textos[91])).replace('###', $(this).text().replace($(this).children('span').text(), '')));
            });
            $.each($('[id^=msgTmplLesGusta_]'), function () {
                $(this).html(Textos[90].replace('@@@', $(this).html().replace($(this).text().replace($(this).children('span').text(), ''), '').replace('$$$', Textos[91])).replace('###', $(this).text().replace($(this).children('span').text(), '')));
            });
            /*====================================
            =========== FIN TEMPLATES INFO ME GUSTA
            =====================================*/
            $('[id^=msgAdjuntoRespuesta_] span').text(Textos[28]);
            $('[id^=msgMeGusta_]').text(Textos[8]);
            $('[id^=msgResponder_]').text(Textos[77]);
            $('[id^=msgDestinatarios_]').text(Textos[78]);
            $('[id^=msgEditar_]').text(Textos[83]);
            $('[id^=msgNuevaRespuestaDefault_]').val(Textos[84]);
            $('[id^=msgDescargar__]').text(Textos[79]);
            $('[id^=btnResponder_] span').text(Textos[20]);
            $('[id^=btnResponder_] span').attr('title', Textos[20]);
            $('[id^=btnCancelarResponder_] span').text(Textos[21]);
            $('[id^=btnCancelarResponder_] span').attr('title', Textos[21]);
            $('[id^=msgNoLeido]').text(Textos[109]);
            $('[id^=msgInfoCategoria]').text(Textos[110]);
            $.each($('[id^=VerRespuestas_]'), function () {
                $(this).text(Textos[92].replace('###', $(this).text()) + '>>');
            });

            if (options.onDone) {
                options.onDone()
            }
            setInterval(ActualizarFechaAltaRelativa, 15000);
        });
    };

    function ActualizarFechaAltaRelativa() {
        $.each($('[date]'), function () {
            $(this).text(relativeTime($(this).attr("date")));
        });
    }
})(jQuery);