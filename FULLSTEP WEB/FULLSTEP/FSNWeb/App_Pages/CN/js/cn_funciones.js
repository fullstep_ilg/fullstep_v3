﻿//CN_FUNCIONES.JS
//=========================
//  FUNCIONES UTILES
//=========================
function CargarMensajes(tipo, grupo, categoria, megusta, pagina, historico, scroll) {
    $('#divMuro ul').remove();
    $('#divSiguientes').hide();
    $('#divHistorico').hide();

    $('#divMuro').attr('tipoMensajes', tipo);
    $('#divSiguientes').attr('pagina', '1');
    $('#divHistorico').attr('pagina', '0');

    FijarMenuTrasScroll();
    FijarOpcionesNuevoMensajeTrasScroll();

    //=================================
    //  CARGAR CONTADORES MENSAJE MENU
    //=================================
    Obtener_Contadores_Notificaciones();

    $('#divMuro').cn_ObtenerMensajesUsuario({
        WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/ObtenerMensajesUsuario',
        tipo: tipo,
        grupo: grupo,
        categoria: categoria,
        megusta: megusta,
        pagina: pagina,
        historico: historico,
        scroll:scroll
    });
}
function ObtenerDestinatarios(idMensaje, restriccionModificar) {
    $('#divParaDestinatarios').empty();
    $.get(rutaFS + 'CN/html/_mensaje_para.htm', function (paraOpciones) {
        paraOpciones = paraOpciones.replace(/src="/gi, 'src="' + ruta);
        $('#divParaDestinatarios').append(paraOpciones);

        if (restriccionModificar) {
            $('#divListaPara').removeAttr('usu');
        } else { $('#divListaPara').attr('usu', ''); }

        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'CN/App_Services/CN.asmx/Obtener_DatosAdicionales_Mensaje',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ idMensaje: idMensaje, idCategoria: null }),
            dataType: "json",
            async: false
        })).done(function (msg) {
            var data = msg.d;
            CargarParaAnterior(msg.d[1],'');

            if (restriccionModificar) {
                $('#divParaOpciones').hide();
                $('#divBotonesDestinatarios').hide();
            } else {                
                $('#divBotonesDestinatarios').show();
            }
            $('#lblAceptarDestinatarios').text(Textos[20]);
            $('#lblCancelarDestinatarios').text(Textos[21]);

            $('#itemParaGrupo-EP').text(Textos[61]);
            $('#itemParaGrupo-GS').text(Textos[62]);
            $('#itemParaGrupo-PM').text(Textos[63]);
            $('#itemParaGrupo-QA').text(Textos[64]);
            $('#itemParaGrupo-SM').text(Textos[65]);

            CentrarPopUp($('#popupDestinatarios'));
            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').show();
            $('#popupDestinatarios').show();
        });
    });
}
function CargarParaAnterior(datos, Master) {
    $.each(datos, function () {
        var texto = this.text;
        switch (this.value.split('-')[0]) {
            case 'itemParaProve':
                switch (this.value.split('-')[1]) {
                    case 'Todos':
                        texto = Textos[40];
                        break;
                    case 'Calidad':
                        texto = Textos[41];
                        break;
                    case 'Potenciales':
                        texto = Textos[42];
                        break;
                    case "Reales":
                        texto = Textos[43];
                        break;
                    case "MatGS":
                        texto = Textos[44] + ' ' + this.text;
                        break;
                    case "MatQA":
                        texto = Textos[45] + ' ' + this.text;
                        break;
                    default:
                        texto = this.text;
                        break;
                }
                break;
            case 'itemParaProceCompras':
                switch (this.value.split('-')[1]) {
                    case 'Todos':
                        texto = Textos[53];
                        break;
                    case 'Responsable':
                        texto = Textos[54];
                        break;
                    case 'Invitados':
                        texto = Textos[55];
                        break;
                    case 'Compradores':
                        texto = Textos[56];
                        break;
                    case 'Proveedores':
                        texto = Textos[57];
                        break;
                }
                break;
            case 'itemParaEstrucCompras':
                if (this.value.split('-')[1] == 'MatGS') {
                    texto = Textos[58] + ' ' + this.text;
                }
                break;
            case 'itemParaGrupos':
                switch (this.value.split('-')[1]) {
                    case 'EP':
                        texto = Textos[61];
                        break;
                    case 'GS':
                        texto = Textos[62];
                        break;
                    case 'PM':
                        texto = Textos[63];
                        break;
                    case 'QA':
                        texto = Textos[64];
                        break;
                    case 'SM':
                        texto = Textos[65];
                        break;
                }
                break;
        }
        $('#divListaPara' + Master).append('<div class="ItemPara" id="' + this.value + '">' + texto + '</div>');
    });
}
function Categorias_Permiso_EmitirUsuario(tree, Consulta) {
    var consulta = (Consulta)?true:false;
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'CN/App_Services/CN.asmx/Categorias_Permiso_EmitirMensajeUsuario',
        data: JSON.stringify({ Consulta: consulta }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
    })).done(function (msg) {
        var data = msg.d;        
        $(tree).tree({
            data: data,
            autoOpen: true,
            selectable: true
        });
    });
}
function Editar_Mensaje(idMensaje) {
    var timeoffset = new Date().getTimezoneOffset();
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'CN/App_Services/CN.asmx/Obtener_Mensaje',
        data: JSON.stringify({ idMensaje: idMensaje, timezoneoffset: timeoffset }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
    })).done(function (msg) {
        var tipoMensaje;
        var mensaje = msg.d;
        switch (mensaje.Tipo) {
            case 1:
                tipoMensaje = 'NuevoTemaMaster';
                $('#txtNuevoMensajeTituloMaster').val(Textos[80]);
                break;
            case 2:
                tipoMensaje = 'NuevoEventoMaster';
                break;
            case 3:
                tipoMensaje = 'NuevaNoticiaMaster';
                $('#txtNuevoMensajeTituloMaster').val(Textos[71]);
                break;
            case 4:
                tipoMensaje = 'NuevaPreguntaMaster';
                $('#txtNuevoMensajeTituloMaster').val(Textos[72]);
                break;
            case 5:
                tipoMensaje = 'NuevoMensajeUrgenteMaster';
                $('#txtNuevoMensajeTituloMaster').val(Textos[73]);
                break;
            default:
                return false;
                break;
        }
        $('[id^=btnAceptarNuevoMensaje]').attr('IdMensaje', 0);
        MostrarFormularioNuevoMensaje(true, true, tipoMensaje, mensaje.IdMensaje, false, mensaje.Contenido);
        $('[id^=btnAceptarNuevoMensaje]').attr('IdMensaje', idMensaje);
        if (mensaje.Tipo == 2) {
            $('#txtEventoFechaMaster').datepicker('setDate', eval("new " + mensaje.Fecha.slice(1, -1)));
            $('#txtEventoHoraMaster').val(mensaje.Hora);

            $('#txtEventoDondeMaster').removeClass('textoDefecto');
            $('#txtEventoDondeMaster').addClass('TextoOscuro');
            $('#txtEventoDondeMaster').val(mensaje.Donde);
        };
        if (mensaje.Titulo == '') {
            $('#txtNuevoMensajeTituloMaster').addClass('textoDefecto');
            $('#txtNuevoMensajeTituloMaster').removeClass('TextoOscuro');
        } else {
            $('#txtNuevoMensajeTituloMaster').removeClass('textoDefecto');
            $('#txtNuevoMensajeTituloMaster').addClass('TextoOscuro');
            $('#txtNuevoMensajeTituloMaster').val(mensaje.Titulo);
        };
        $('#divListaAdjuntosMaster').prepend($('#msg_' + idMensaje + ' .msgAdjuntos').html());
        if (typeof (edicionDiscrepancia) !== 'undefined' && edicionDiscrepancia) $('#divMenuNuevoMensajeMaster').hide();
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'CN/App_Services/CN.asmx/Obtener_DatosAdicionales_Mensaje',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ idMensaje: mensaje.IdMensaje, idCategoria: null }),
            dataType: "json",
            async: false
        })).done(function (msg) {
            var data = msg.d;
            if (data[0].Id != 0) {
                $('#tvNuevoMensajeCategoriasMaster').tree('selectNodeValue', data[0].Id);
                SeleccionarCategoria('Master');
            }
        });
        var heightContenido = $('#Contenido').height();
        CentrarPopUp($('#popupNuevoMensaje'));
        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        $('#Contenido').css('height', heightContenido);
        $('#popupNuevoMensaje').show();
    });
}
function Obtener_ResultadosBusqueda() {
    $('#divPrincipal #divMuro ul').remove();
    if (resultados) clearInterval(resultados);
    var textoBuscado = $('#txtBuscador').val();
    if (textoBuscado == '') return false;
    var tipoOrdenacion = ($('#lblTipoOrdenacionCoincidencias').hasClass('Link')) ? 0 : 1;
    $('#divPrincipal').hide();
    $('#divCargando').show();
    var divResultados = $('#divMensajesResultadosBusqueda');
    divResultados.empty();
    var cn = { posts: [] };
    var timeoffset = new Date().getTimezoneOffset();
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'CN/App_Services/CN.asmx/ObtenerResultadosBusqueda',
        data: JSON.stringify({ tipoOrdenacion: tipoOrdenacion, textoBusqueda: textoBuscado, timezoneoffset: timeoffset }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true
    })).done(function (msg) {
        if (msg.d && msg.d.length > 0) {
            var mensajesBuscador = msg.d[0];
            var palabrasBuscador = msg.d[1];
            $.each(mensajesBuscador, function () {
                this.FechaAlta = eval("new " + this.FechaAlta.slice(1, -1));
                this.FechaActualizacion = eval("new " + this.FechaActualizacion.slice(1, -1));

                this.FechaAltaRelativa = relativeTime(this.FechaAlta);
                this.FechaActualizacionRelativa = relativeTime(this.FechaActualizacion);

                if (this.Respuestas) {
                    $.each(this.Respuestas, function () {
                        this.FechaAlta = eval("new " + this.FechaAlta.slice(1, -1));
                        this.FechaActualizacion = eval("new " + this.FechaActualizacion.slice(1, -1));

                        this.FechaAltaRelativa = relativeTime(this.FechaAlta);
                        this.FechaActualizacionRelativa = relativeTime(this.FechaActualizacion);
                    });
                }

                cn.posts.push(this);
            });
            // Crear lista para los post
            var ul = $('<ul>').appendTo(divResultados);

            $('#itemMensaje').tmpl(cn.posts).appendTo(ul);
        }
        $('[id^=lblTituloResultadoBusqueda]').text(Textos[101]);
        $('[id^=lblInfoResultadosBusqueda]').text(Textos[102]);
        $('[id^=lblNumResultadosBusqueda]').text(Textos[103].replace('##', cn.posts.length));
        $('[id^=lblObjetivoResultadosBusqueda]').text(Textos[104]);
        $('[id^=lblTipoOrdenacion]').text(Textos[105]);
        $('[id^=lblTipoOrdenacionFecha]').text(Textos[106]);
        $('[id^=lblTipoOrdenacionCoincidencias]').text(Textos[107]);
        $('[id^=lblTextoResultadosBusqueda]').text(textoBuscado);

        $('#divCargando').hide();
        $('#divResultadosBusqueda').show();
        if (cn.posts.length > 0) {
            $.each(palabrasBuscador, function () {
                divResultados.highlight(this);
            });
            divResultados.show();
        }
        /*====================================
        =========== TEMPLATES INFO ME GUSTA
        =====================================*/
        $('[id^=msgTmplMeGusta_]').text(Textos[85]);
        $.each($('[id^=msgTmplLeGusta_]'), function () {
            $(this).text(Textos[86].replace('###', $(this).text()));
        });
        $.each($('[id^=msgTmplLeGustaMeGusta_]'), function () {
            $(this).text(Textos[87].replace('###', $(this).text()));
        });
        $.each($('[id^=msgTmplMeGustaLesGusta_]'), function () {
            $(this).html(Textos[88].replace('@@@', $(this).html().replace('$$$', Textos[91])));
        });
        $.each($('[id^=msgTmplLeGustaMeGustaLesGusta_]'), function () {
            $(this).html(Textos[89].replace('@@@', $(this).html().replace($(this).text().replace($(this).children('span').text(), ''), '').replace('$$$', Textos[91])).replace('###', $(this).text().replace($(this).children('span').text(), '')));
        });
        $.each($('[id^=msgTmplLesGusta_]'), function () {
            $(this).html(Textos[90].replace('@@@', $(this).html().replace($(this).text().replace($(this).children('span').text(), ''), '').replace('$$$', Textos[91])).replace('###', $(this).text().replace($(this).children('span').text(), '')));
        });
        /*====================================
        =========== FIN TEMPLATES INFO ME GUSTA
        =====================================*/
        $('[id^=msgAdjuntoRespuesta_] span').text(Textos[28]);
        $('[id^=msgMeGusta_]').text(Textos[8]);
        $('[id^=msgResponder_]').text(Textos[77]);
        $('[id^=msgDestinatarios_]').text(Textos[78]);
        $('[id^=msgEditar_]').text(Textos[83]);
        $('[id^=msgNuevaRespuestaDefault_]').val(Textos[84]);
        $('[id^=msgDescargar__]').text(Textos[79]);
        $('[id^=btnResponder_]').text(Textos[20]);
        $('[id^=btnResponder_]').attr('title', Textos[20]);
        $('[id^=btnCancelarResponder_]').text(Textos[21]);
        $('[id^=btnCancelarResponder_]').attr('title', Textos[21]);
        $('[id^=msgNoLeido]').text(Textos[109]);
        $('[id^=msgInfoCategoria]').text(Textos[110]);
        $.each($('[id^=VerRespuestas_]'), function () {
            $(this).text(Textos[92].replace('###', $(this).text()) + '>>');
        });
    });
}
function OpcionesOtrosMensajes(MostrarSiguientes, MostrarOcultos, MostrarHistorico) {
    if (MostrarSiguientes || MostrarOcultos || MostrarHistorico) {
        $('#divOpcionesOtrosMensajes').show();
        if (MostrarSiguientes) {
            $('#divSiguientes').show();
            $('#lblSiguientes').text(Textos[68]);
        } else { $('#divSiguientes').hide(); }
        if (MostrarOcultos) {
            $('#divOcultos').show();
            $('#lblOcultos').text(Textos[69]);
        }
        if (MostrarHistorico) {
            $('#divHistorico').show();
            $('#lblHistorico').text(Textos[111]);
        }
    } else {
        $('#divOpcionesOtrosMensajes').hide();
    }
}