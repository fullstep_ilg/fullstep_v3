﻿//CN_UI_MEGUSTA.JS
//=========================
//  ME GUSTA
//=========================
$('[id^=lnkMeGusta_]').live('click', function (event) {
    var idRespuesta = $(this).attr('id').split('_')[2];
    var idMensaje = $(this).attr('id').split('_')[1];
    Ocultar_ListaUsuariosMeGusta();
    $('#divListaUsuariosMeGusta_' + idMensaje + '_' + idRespuesta).css('top', $(this).position().top + 25);
    $('#divListaUsuariosMeGusta_' + idMensaje + '_' + idRespuesta).css('left', $(this).position().left);
    $('#divListaUsuariosMeGusta_' + idMensaje + '_' + idRespuesta).show('fast');
    $('#divListaUsuariosMeGusta_' + idMensaje + '_' + idRespuesta).focus();
    return false;
});

$('[id^=btnCerrarListaUsuarios_]').live('click', function (event) {
    Ocultar_ListaUsuariosMeGusta();
});

function Ocultar_ListaUsuariosMeGusta() {
    $('[id^=divListaUsuariosMeGusta_]').hide();
}

$('[id^=msgMeGusta]').live('click', function () {
    var idRespuesta = ($(this).attr('id').split('_')[2] == '') ? '0' : $(this).attr('id').split('_')[2];
    var idMensaje = $(this).attr('id').split('_')[1];
    var timeoffset = new Date().getTimezoneOffset();
    params = { idMensaje: idMensaje, idRespuesta: idRespuesta, timezoneoffset: timeoffset };
    $('#msgMeGusta_' + idMensaje + '_' + (idRespuesta == 0 ? '' : idRespuesta)).hide();
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'CN/App_Services/CN.asmx/MeGustaMensaje',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        var mensaje = msg.d;        
        $('#divMeGusta_' + idMensaje + '_' + (idRespuesta == 0 ? '' : idRespuesta)).remove();
        if (idRespuesta == 0) {
            $('#MeGustaInfo').tmpl(mensaje).prependTo($('#divRespuestasMensaje_' + idMensaje));
        } else {
            $.each(mensaje.Respuestas, function () {
                if (this.IdRespuesta == idRespuesta) {
                    $('#MeGustaInfo').tmpl(this).appendTo($('#msgRespuestaContenido_' + idMensaje + '_' + idRespuesta));
                    return false;
                }
            });
        }
        /*====================================
        =========== TEMPLATES INFO ME GUSTA
        =====================================*/
        var tmpl;
        tmpl = $('#msgTmplMeGusta_' + idMensaje + '_' + (idRespuesta == 0 ? '' : idRespuesta));
        if (tmpl.length>0) tmpl.text(Textos[85]);
        tmpl = $('#msgTmplLeGusta_' + idMensaje + '_' + (idRespuesta == 0 ? '' : idRespuesta));
        if (tmpl.length > 0) tmpl.text(Textos[86].replace('###', tmpl.text()));
        tmpl=$('#msgTmplLeGustaMeGusta_' + idMensaje + '_' + (idRespuesta == 0 ? '' : idRespuesta));
        if (tmpl.length > 0) tmpl.text(Textos[87].replace('###', tmpl.text()));
        tmpl = $('#msgTmplMeGustaLesGusta_' + idMensaje + '_' + (idRespuesta == 0 ? '' : idRespuesta))
        if (tmpl.length > 0) tmpl.html(Textos[88].replace('@@@', tmpl.html().replace('$$$', Textos[91])));
        tmpl =$('#msgTmplLeGustaMeGustaLesGusta_' + idMensaje + '_' + (idRespuesta == 0 ? '' : idRespuesta));
        if (tmpl.length > 0) tmpl.html(Textos[89].replace('@@@', tmpl.html().replace(tmpl.text().replace(tmpl.children('span').text(), ''), '').replace('$$$', Textos[91])).replace('###', tmpl.text().replace(tmpl.children('span').text(), '')));
        tmpl=$('[id^=msgTmplLesGusta_' + idMensaje + '_' + (idRespuesta == 0 ? '' : idRespuesta));
        if (tmpl.length > 0) tmpl.html(Textos[90].replace('@@@', tmpl.html().replace(tmpl.text().replace(tmpl.children('span').text(), ''), '').replace('$$$', Textos[91])).replace('###', tmpl.text().replace(tmpl.children('span').text(), '')));
        /*====================================
        =========== FIN TEMPLATES INFO ME GUSTA
        =====================================*/
    });
    MostrarFormularioRespuesta(false, idMensaje);
});
