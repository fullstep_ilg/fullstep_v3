﻿//CN_MENSAJES_UI.JS
$('[id^=msgContenido_] a').live('click', function () {
    if ($(this).attr('type') == 'entidad') {
        var tipoEntidad = $(this).attr('href').split("#")[0];
        var identificador = $(this).attr('href').split("#")[1];
        var MethodURL, param;
        switch (tipoEntidad) {
            case 'PC':
                MethodURL = '/Comprobar_PermisoConsulta_ProceCompra';
                param = { Anyo: $(this).attr('href').split("#")[1], GMN1: $(this).attr('href').split("#")[2], Proce: $(this).attr('href').split("#")[3] };
                break;
            case 'SC':
                MethodURL = '/Comprobar_PermisoConsulta_Identificador';
                param = { TipoEntidad: 1, Identificador: identificador };
                break;
            case 'CO':
                MethodURL = '/Comprobar_PermisoConsulta_Identificador';
                param = { TipoEntidad: 5, Identificador: identificador };
                break;
            case 'NC':
                MethodURL = '/Comprobar_PermisoConsulta_Identificador';
                param = { TipoEntidad: 3, Identificador: identificador };
                break;
            case 'CE':
                MethodURL = '/Comprobar_PermisoConsulta_Identificador';
                param = { TipoEntidad: 2, Identificador: identificador };
                break;
            case 'FP':
                MethodURL = '/Comprobar_PermisoConsulta_FichaCalidad';
                param = { ProveCod: identificador };
                break;
            case 'PE':
                MethodURL = '/Comprobar_PermisoConsulta_Pedido';
                param = { Anio: $(this).attr('href').split("#")[1], NumPedido: $(this).attr('href').split("#")[2], NumOrden: $(this).attr('href').split("#")[3] };
                break;
            case 'FA':
                MethodURL = '/Comprobar_PermisoConsulta_Factura';
                param = { IdFactura: identificador.split('/')[0] };
                break;
        }
        var FechaLocal, url, targetUrl;
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'CN/App_Services/CN.asmx' + MethodURL,
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true
        })).done(function (msg) {
            if (msg.d) {
                switch (tipoEntidad) {
                    case 'PC':
                        targetUrl = msg.d;
                        window.open(targetUrl, 'ProceCompra', '_blank,');
                        break;
                    case 'SC':
                    case 'CO':
                        TempCargando();
                        targetUrl = msg.d;
                        targetUrl = targetUrl.replace('[###]', 'tipoMensajes=' + $('#divMuro').attr('tipoMensajes') + '**pagina=' + $('#divSiguientes').attr('pagina') + '**scrollpagina=' + $(window).scrollTop());
                        window.location.href = targetUrl;
                        break;
                    case 'NC':
                    case 'CE':
                        TempCargando();
                        FechaLocal = new Date();
                        targetUrl = msg.d.replace('[##]', FechaLocal.getTimezoneOffset());
                        targetUrl = targetUrl.replace('[###]', 'tipoMensajes=' + $('#divMuro').attr('tipoMensajes') + '**pagina=' + $('#divSiguientes').attr('pagina') + '**scrollpagina=' + $(window).scrollTop());
                        window.location.href = targetUrl;
                        break;
                    case 'FP':
                    case 'PE':
                    case 'FA':
                        TempCargando();
                        targetUrl = msg.d;
                        window.location.href = targetUrl + '&tipoMensajes=' + $('#divMuro').attr('tipoMensajes') + '&pagina=' + $('#divSiguientes').attr('pagina') + '&scrollpagina=' + $(window).scrollTop();
                        break;                        
                }
            } else {
                alert(Textos[114]);
                return false;
            }
        });
        return false;
    }
});
$('.msgAdjuntoImage').live('click', function () {
    if (!$(this).hasClass('Link')) return false;
    var adjun = $(this).attr('data-url');
    if ($("#popupThumbnail").length == 0) {
        $.when($.get(rutaFS + 'CN/html/_PopUp_Thumbnail.htm', function (popUp) {
            $('#Contenido').append(popUp);
        })).done(function () {
            $.get(rutaFS + 'CN/html/_thumbnail.htm', function (templateThumbnail) {
                $('#popupThumbnail').append(templateThumbnail);
                $('#divAdjuntoThumbnail').append('<img id="msgAdjuntoThumbnail" src="' + adjun + '" style="max-height:100%; max-width:100%;" />');
                $('#popupFondo').css('height', $(document).height());
                $('#popupFondo').show();
                $('#popupThumbnail').show();
                var width = 100;
                while ($('#popupThumbnail').outerHeight() < $('#msgAdjuntoThumbnail').outerHeight() + 30) {
                    $('#msgAdjuntoThumbnail').css('max-width', width - 1 + '%');
                    width = width - 1;
                }
                CentrarPopUp($('#popupThumbnail'));
            });
            $('#btnCerrarPopUpThumbnail').live('click', function () {
                $('#popupFondo').hide();
                $('#popupThumbnail').hide();
            });
        });
    } else {
        $('#divAdjuntoThumbnail').empty();
        $('#divAdjuntoThumbnail').append('<img id="msgAdjuntoThumbnail" src="' + adjun + '" style="max-height:100%;max-width:100%;"/>');
        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        $('#popupThumbnail').show();
        var width = 100;
        while ($('#popupThumbnail').outerHeight() < $('#msgAdjuntoThumbnail').outerHeight() + 30) {
            $('#msgAdjuntoThumbnail').css('max-width', width - 1 + '%');
            width = width - 1;
        }
        CentrarPopUp($('#popupThumbnail'));
    }
    return false;
});
$('[id^=msgAdjunVideo_] img').live('mouseenter', function () {
    if ($(this).closest('.template-download').find('.video-js-box').is(':visible')) {
        $(this).css('margin-left', '-160px');
    } else {
        $(this).css('margin-left', '-80px');
    }
});
$('[id^=msgAdjunVideo_] img').live('mouseleave', function () {
    if ($(this).closest('.template-download').find('.video-js-box').is(':visible')) {
        $(this).css('margin-left', '-160px');
    } else {
        $(this).css('margin-left', '0px');
    }
});
$('[id^=msgAdjunVideo_] img').live('click', function () {
    if ($(this).closest('.template-download').find('.video-js-box').is(':visible')) {
        $(this).css('margin-left', '-80px');
        $(this).closest('.template-download').find('.video-js-box').hide();
        if (jQuery.support.html5Clone) {
            $(this).closest('.template-download').find('video').get(0).pause();
        }
    } else {
        $(this).css('margin-left', '-160px');
        $(this).closest('.template-download').find('.video-js-box').show();
        if (jQuery.support.html5Clone) {
            $(this).closest('.template-download').find('video').get(0).play();
        }
    }
});
$('[id^=msgDescargar_]').live('click', function () {
    var filename = $(this).attr('data-file');
    var eparameter = ($(this).attr('EnProceso') == '' ? '' : '&e=1');    
    window.location.href = rutaFS + '_Common/downloadfile.aspx?f=' + filename + eparameter;    
});
$('[id^=VerRespuestas_]').live('click', function () {
    if ($(this).parents("#divMensajeUrgente").length > 0) $('#divMensajeUrgente').css('max-height', '');
    var idMensaje = $(this).attr('id').replace('VerRespuestas_', '');
    $.each($('[id^=msgRespuesta_' + idMensaje + '_]'), function () {
        $(this).show();
    });

    $('#divVerTodasRespuestas_' + idMensaje).hide();
    if ($(this).parents("#divMensajeUrgente").length > 0) {
        CentrarPopUp($('#popupMensajeUrgente'));
        $('#divMensajeUrgente').css('max-height', $('#popupMensajeUrgente').height() - 85);
    }
});
$('[id^=msgDestinatarios]').live('click', function () {
    MostrarFormularioNuevoMensaje(false, false);
    $('#divNuevoMensajeParaOpciones').empty();
    $('#divParaDestinatarios').empty();
    var idMensaje = $(this).attr('id').split('_')[1];
    var restriccionModificar = ($(this).attr('usu') == undefined);
    if ($('#lblModificarDestinatarios').length == 0) {
        $.when($.get(rutaFS + 'CN/html/_PopUp_Destinatarios.htm', function (popUpDestinatarios) {
            $('body').append(popUpDestinatarios);
        })).done(function () {
            $('#btnAceptarDestinatarios').attr('idMensaje', idMensaje);
            ObtenerDestinatarios(idMensaje, restriccionModificar);

            $('#lblModificarDestinatarios').text(Textos[78]);

            $('#btnCerrarPopUpDestinatarios').live('click', function () {
                $('#btnAceptarDestinatarios').removeAttr('idMensaje');
                $('#popupFondo').hide();
                $('#popupDestinatarios').hide();
            });
            $('#btnCancelarDestinatarios').live('click', function () {
                $('#btnAceptarDestinatarios').removeAttr('idMensaje');
                $('#popupFondo').hide();
                $('#popupDestinatarios').hide();
            });
            $('#btnAceptarDestinatarios').live('click', function () {
                var paraMensaje = ObtenerParaMensaje('');
                params = { idMensaje: $('#btnAceptarDestinatarios').attr('idMensaje'), para: paraMensaje };
                $.ajax({
                    type: "POST",
                    url: rutaFS + 'CN/App_Services/CN.asmx/Modificar_Destinatarios_Mensaje',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(params),
                    dataType: "json",
                    async: true
                });
                $('#popupFondo').hide();
                $('#popupDestinatarios').hide();
            });
        });
    } else {
        $('#btnAceptarDestinatarios').attr('idMensaje', idMensaje);
        ObtenerDestinatarios(idMensaje, restriccionModificar);
    }
    return false;
});
$('[id^=msgEditar]').live('click', function () {
    var idRespuesta = ($(this).attr('id').split('_')[2] == '') ? '0' : $(this).attr('id').split('_')[2];
    var idMensaje = $(this).attr('id').split('_')[1];
    if (idRespuesta == 0) {
        if ($('#divNuevosMensajesMaster').length == 0) {
            $.get(rutaFS + 'CN/html/_nuevo_mensaje.htm', function (menuNuevoMensaje) {
                menuNuevoMensaje = menuNuevoMensaje.replace(/src="/gi, 'src="' + ruta);
                $('body').prepend('<div id="popupNuevoMensaje" class="popupCN" style="display:none; position:absolute; z-index:1002; width:85%; max-height:80%; text-align:left; padding:5px; overflow:auto;"></div>');
                $('#popupNuevoMensaje').prepend(menuNuevoMensaje);
                $.each($('#popupNuevoMensaje [id]'), function () {
                    $(this).attr('id', $(this).attr('id') + 'Master');
                });
                EstablecerTextosMenuMensajeNuevo();
                $('#txtNuevoMensajeTituloMaster').autoGrow();
                $('#txtEventoFechaMaster').datepicker({
                    showOn: 'both',
                    buttonImage: ruta + 'images/colorcalendar.png',
                    buttonImageOnly: true,
                    buttonText: '',
                    dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
                    showAnim: 'slideDown'
                });
                $.datepicker.setDefaults($.datepicker.regional[usuario.RefCultural]);
                if (usuario.CNPermisoCrearMensajesUrgentes) $('#NuevoMensajeUrgenteMaster').show();
                Editar_Mensaje(idMensaje);
            });
        } else {
            Editar_Mensaje(idMensaje);
        }
        $('#divNuevoMensajeOtrasOpcionesMaster').hide();
    }
});
$('[id^=msgOcultarMensaje_]').live('click', function () {
    var idMensaje = $(this).attr('id').replace('msgOcultarMensaje_', '');
    $('#msg_' + idMensaje).hide();
    $('#msgOcultarMensaje_' + idMensaje).addClass('Ocultar');
    $('#msgMostrarMensaje_' + idMensaje).removeClass('Ocultar');
    $('#divOcultos').show();
    $.ajax({
        type: "POST",
        url: rutaFS + 'CN/App_Services/CN.asmx/OcultarMostrarMensaje',
        data: JSON.stringify({ idMensaje: idMensaje, ocultar: true }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true
    });
});
$('[id^=msgMostrarMensaje_]').live('click', function () {
    var idMensaje = $(this).attr('id').replace('msgMostrarMensaje_', '');
    $('#msgMostrarMensaje_' + idMensaje).addClass('Ocultar');
    $('#msgOcultarMensaje_' + idMensaje).removeClass('Ocultar');
    $.ajax({
        type: "POST",
        url: rutaFS + 'CN/App_Services/CN.asmx/OcultarMostrarMensaje',
        data: JSON.stringify({ idMensaje: idMensaje, ocultar: false }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true
    });
});
$('[id^=msgLink_]').live('click', function () {
    window.open($(this).attr('data-file'), 'link');
});
$('.divMuro>ul>li').live('mouseenter mouseleave', function () {
    var idMensaje = $(this).attr('id').replace('msg_', '');
    if ($('#msgMostrarMensaje_' + idMensaje).hasClass('Ocultar')) $('#msgOcultarMensaje_' + idMensaje).toggleClass('Ocultar');
});