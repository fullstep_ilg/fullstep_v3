﻿//CN_USERPROFILE_UI.JS
$('#btnAceptarUserProfile').live('click',function () {
    $('#divCargando').show();
    var CNNotificarResumenActividad = ($('[id$=CheckBoxListaNotificaciones_0]').attr('checked')) ? 1 : 0;
    var CNNotificarIncluidoGrupo = ($('[id$=CheckBoxListaNotificaciones_1]').attr('checked')) ? 1 : 0;
    var CNConfiguracionDesocultar = ($('[id$=CheckBoxListaConfiguraciones_0]').attr('checked')) ? 1 : 0;

    var pathFoto = ($('.template-download').attr('data-url'));
    GuardarUserProfileBD(pathFoto, CNNotificarResumenActividad, CNNotificarIncluidoGrupo, CNConfiguracionDesocultar);
});
$('#btnCancelarUserProfile').live('click',function () {
    document.location.href=rutaFS + "cn/cn_MuroUsuario.aspx?tipo=1&pagina=1&scrollpagina=0";
});
$('#ModificarFotoPerfil').live('mouseenter', function () {
    $('#ModificarFotoPerfil').addClass('Seleccionable');
    $('#fileupload .fileinput-button').css('width', $('#ModificarFotoPerfil').outerWidth());
    $('#fileupload .fileinput-button').css('height', $('#ModificarFotoPerfil').outerHeight());
    $('#fileupload .fileinput-button').css('left', $('#ModificarFotoPerfil').position().left);
    $('#fileupload .fileinput-button').css('top', $('#ModificarFotoPerfil').position().top);
    $('#fileupload .fileinput-button').css('right', '');
});
var GuardadoOK;
function GuardarUserProfileBD(path, notificarResumenActividad, notificarIncluidoGrupo, configuracionDesocultar) {
    var params = '';
    var userProfile = '';
    //USER PROFILE
    var userProfile={
        notificarResumenActividad: '' + notificarResumenActividad + '',
        notificarIncluidoGrupo: '' + notificarIncluidoGrupo + '',
        configuracionDesocultar: '' + configuracionDesocultar + ''
    };
    if (path == undefined) {
        path = '';
    };
    params = { pathFoto: path, userProfile: userProfile };
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'CN/App_Services/CN.asmx/UpdateUserProfile',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function () {
        $('#lblPerfilGuardadoOK').show();
        $('#lblPerfilGuardadoOK').text(TextosUser[18]);
        $('#divMenu #imgUsuImagen').attr('src', rutaFS + 'CN/Thumbnail.ashx?t=0&d=' + new Date());
        $('#divCargando').hide();
        GuardadoOK = setTimeout(function () { clearTimeout(GuardadoOK); $('#lblPerfilGuardadoOK').hide(); }, 3000);
    });
}
