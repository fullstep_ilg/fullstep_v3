﻿//CN_UI_RESPUESTAS.JS
//=========================
//  RESPUESTAS
//=========================
$('[id^=msgResponder]').live('click', function () {
    MostrarFormularioNuevoMensaje(false, false);
    var idRespuesta = ($(this).attr('id').split('_')[2] == '') ? '0' : $(this).attr('id').split('_')[2];
    var idMensaje = $(this).attr('id').split('_')[1];
    var idDiscrepancia = $(this).attr('id').split('_')[3];
    $('#msgRespuestaNombreCitado_' + idMensaje).val();
    MostrarFormularioRespuesta(true, idMensaje, idRespuesta, idDiscrepancia);
});
$('[id^=btnCancelarResponder]').live('click', function (event) {
    var idMensaje = this.id.replace('btnCancelarResponder_', '');
    MostrarFormularioRespuesta(false, idMensaje);
});
$('[id^=btnResponder]').live('click', function (event) {
    var idMensaje = this.id.replace('btnResponder_', '');
    var idRespuesta = $(this).attr('IdRespuesta');
    var idDiscrepancia = $(this).attr('IdDiscrepancia');
    var editor = CKEDITOR.instances['msgRespuestaNueva_' + idMensaje];
    if (editor.getData() == '') {
        alert(Textos[112]);
        return false;
    } else {
        var CKEditorImages = [];
        $.each(editor.document.getElementsByTag('img').$, function () {
            CKEditorImages.push({ href: this.href, path: '' + $(this).attr('id') + '', filename: '' + $(this).attr('filename') + '', guidID: '' });
        });
        var contenido = editor.getData();
        var citado;
        var idNuevo = 0;
        while ($('[EnProceso=xr' + idNuevo + 'rx]').length != 0) {
            idNuevo++;
        };
        if (idRespuesta == "0") {
            citado = {};
        } else {
            citado = { nombreUsu: $('#msgUsuRespuestaNombre_' + idMensaje + '_' + idRespuesta).text(),
                situacionUO: $('#msgUsuRespuestaSituacionUO_' + idMensaje + '_' + idRespuesta).text()
            }
        }
        var fechaAlta = new Date();
        var fechaAltaMensaje = UsuMask.replace('dd', fechaAlta.getDate()).replace('MM', fechaAlta.getMonth() + 1).replace('yyyy', fechaAlta.getFullYear());
        fechaAltaMensaje += ' ' + fechaAlta.getHours() + ':' + fechaAlta.getMinutes() + ':' + fechaAlta.getSeconds();
        var mensaje =
        {
            fechaAlta: '' + fechaAltaMensaje + '',
            contenido: '' + CKEDITOR.instances['msgRespuestaNueva_' + idMensaje].getData() + '',
            timezoneOffset: fechaAlta.getTimezoneOffset()
        }
        //ADJUNTOS
        var listaAdjuntos = [];
        $.each($('#divListaAdjuntosRespuesta_' + idMensaje + ' #tablaadjuntos').children(), function () {
            listaAdjuntos.push({
                nombre: $(this).attr('data-name'),
                size: $(this).attr('data-size'),
                sizeunit: $(this).attr('data-sizeunit'),
                tipoadjunto: $(this).attr('data-tipo'),
                url: $(this).attr('data-url')
            });
        });
        params = { idMensaje: idMensaje, idRespuesta: idRespuesta, mensaje: mensaje, citado: citado, adjuntos: listaAdjuntos };
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'CN/App_Services/CN.asmx/GenerarRespuesta',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(params),
            dataType: "json",
            async: false
        })).done(function (msg) {
            var mensaje = msg.d;
            mensaje.Respuestas[0].FechaAlta = eval("new " + mensaje.Respuestas[0].FechaAlta.slice(1, -1));
            mensaje.Respuestas[0].FechaActualizacion = eval("new " + mensaje.Respuestas[0].FechaActualizacion.slice(1, -1));

            mensaje.Respuestas[0].FechaAltaRelativa = relativeTime(mensaje.Respuestas[0].FechaAlta);
            mensaje.Respuestas[0].FechaActualizacionRelativa = relativeTime(mensaje.Respuestas[0].FechaActualizacion);
            mensaje.Respuestas[0].IdDiscrepancia = idDiscrepancia;
            var div = $('#divRespuestas_' + idMensaje);
            $('#Respuesta').tmpl(mensaje.Respuestas[0]).appendTo(div);
            $('[id^=msgMeGusta_]').text(Textos[8]);
            $('[id^=msgResponder_]').text(Textos[77]);
            $('[id^=msgDescargar__]').text(Textos[79]);
            $('#msgNuevaRespuestaDefault_' + idMensaje).show();
        });
        $.extend(params, { imagenesCKEditor: CKEditorImages });
        $.when($.ajax({
            type: "POST",
            url: rutaFS + 'CN/App_Services/CN.asmx/ResponderMensaje',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(params),
            dataType: "json",
            async: true
        })).done(function (msg) {
            var id = msg.d;
            $.each($('[EnProceso=xr' + idNuevo + 'rx]'), function () {
                if ($(this).attr('IdRespuesta') == '0') {
                    $(this).attr('IdRespuesta', id);
                } else {
                    $(this).attr('id', $(this).attr('id').replace('_' + idNuevo, '_' + id));
                }
                $(this).attr('EnProceso', '');
            });
        });
        MostrarFormularioRespuesta(false, idMensaje);
    }
});
$('[id^=msgNuevaRespuestaDefault_]').live('focus', function () {
    var id = $(this).attr('id').replace('msgNuevaRespuestaDefault_', '');
    $(this).blur();
    MostrarFormularioRespuesta(true, id, 0);
});
$('.msgRespuesta .Link').live('mouseenter', function () {
    $('.msgRespuesta .Link').addClass('Seleccionable');
    $('#fileupload .fileinput-button').css('width', $(this).outerWidth());
    $('#fileupload .fileinput-button').css('height', $(this).outerHeight());
    $('#fileupload .fileinput-button').css('left', $(this).offset().left);
    $('#fileupload .fileinput-button').css('top', $(this).offset().top);
    $('#fileupload .fileinput-button').css('z-index', 10000);
});
function MostrarFormularioRespuesta(show, idMensaje, idRespuesta, idDiscrepancia) {
    var txtRespuesta = 'msgRespuestaNueva_' + idMensaje;
    var editor = CKEDITOR.instances[txtRespuesta];
    if (show) {
        if (editor) {
            editor.destroy(true);
        }
        editor = CKEDITOR.replace(txtRespuesta, { customConfig: ruta + 'ckeditor/configMinimo.js',
            on: { 
                instanceReady: function (ev) {
                    this.focus(); ev.sender.insertHtml('');
                    ImageUploadPlugin(ev.editor.name);
                    ev.editor.on('paste', function (ev) { CKEditorPaste(ev); });
                } 
            } 
        });
        scayt_READY = setInterval(function () { SCAYTready(txtRespuesta) }, 500);  
        $('#divRespuestaNueva_' + idMensaje).show();
        $('#btnResponder_' + idMensaje).attr("IdRespuesta", idRespuesta);
        $('#btnResponder_' + idMensaje).attr("IdDiscrepancia", idDiscrepancia);
        if (idRespuesta == 0) {
            $('#msgRespuestaNombreCitado_' + idMensaje).text($('#msgUsuNombre_' + idMensaje).text());
        } else {
            $('#msgRespuestaNombreCitado_' + idMensaje).text($('#msgUsuRespuestaNombre_' + idMensaje + '_' + idRespuesta).text());
        }

        $('#fileupload').attr('mensaje', idMensaje);
        $('#msgNuevaRespuestaDefault_' + idMensaje).hide();
        $('#tablaadjuntos').remove();
        $('#divListaAdjuntosRespuesta_' + idMensaje).append('<div class="fileupload-content"><div id="tablaadjuntos" class="files"></div><div class="fileupload-progressbar"></div></div>');
    } else {
        if (idMensaje == undefined) {
            return false;
        } else {
            if (editor) { editor.destroy(true) };
            $('[id^=divListaAdjuntosRespuesta_' + idMensaje + ']').empty();
            $('#divRespuestaNueva_' + idMensaje).hide();
            $('#btnResponder_' + idMensaje).removeAttr("IdRespuesta");
            $('#msgNuevaRespuestaDefault_' + idMensaje).show();
        }
    }
}