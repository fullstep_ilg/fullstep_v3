﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="cn_MuroUsuario.aspx.vb" Inherits="Fullstep.FSNWeb.cn_MuroUsuario" %>
<asp:Content ID="MuroUsuarioCN" ContentPlaceHolderID="CPH4" runat="server">
	<div id="divCabecera" style="clear:both; position:relative; float:left; width:100%;">
		<fsn:FSNPageHeader ID="FSNPageHeader" runat="server" UrlImagenCabecera="~/images/ColaboracionHeader.png" />
		<div style="position:absolute; width:100%; bottom:2px; left:0px;">
			<div id="lnkAyudaBuscador" class="Link" style="float:right; margin-right:20px; margin-left:5px;">
				<span class="Link TextoAyuda Texto12 Negrita Subrayado" style="line-height:20px;">?</span>
			</div>
			<img id="imgBuscador" src="../../Images/Buscador.png" class="Link Image20" style="float:right; margin-right:20px;"/>
			<input id="txtBuscador" type="text" class="CajaTexto" style="float:right; width:300px; padding-left:2px; padding-right:2px;" />             
		</div>
	</div>
	<div id="divMenu" class="divMenu"></div>
	<div id="divPrincipal" class="divPrincipal">
		<div id="divNuevosMensajes" class="SeparadorInf" style="width:95%; padding-bottom:5px;"></div>
		<div id="divNotificaciones" class="SeparadorSup" style="display:none; clear:both; float:left; width:100%; height:40px; text-align:center; padding:5px 0px 5px 0px;">
			<div id="divPanelNotificaciones" class="Link">
				<span class="Texto12">DNotificaciones</span>
			</div>
		</div>
		<div id="divMuro" class="divMuro">
			<div id="divOpcionesOtrosMensajes" class="divOpcionesOtrosMensajes" style="display:none;">
				<div id="divHistorico" style="float:left; padding:5px; display:none;">
					<span id="lblHistorico" class="Link"></span>
				</div>
				<div id="divSiguientes" style="float:left; padding:5px;">
					<span id="lblSiguientes" class="Link"></span>
				</div>
				<div id="divOcultos" style="display:none; float:right; padding:5px;">
					<span id="lblOcultos" class="Link"></span>
				</div>
			</div>
		</div>    
	</div>
	<div id="divResultadosBusqueda" class="divPrincipal" style="display:none;">
		<div id="divCabeceraBusqueda" class="SeparadorInf" style="width:95%; padding-bottom:5px;">
			<div class="SeparadorInf" style="clear:both; float:left; width:100%; margin-top:15px;">
				<span id="lblTituloResultadoBusqueda" class="Texto14 Negrita TextoResaltado" style="margin-left:10px; margin-top:10px;"></span>
			</div>
			<div style="clear:both; float:left; width:100%; padding:15px 0px 5px 0px;">
				<div style="margin-left:10px;">
					<span id="lblInfoResultadosBusqueda">DSe ha encontrado un total de</span>
					<span id="lblNumResultadosBusqueda" class="Negrita">## mensajes</span>
					<span id="lblObjetivoResultadosBusqueda">Dpara</span>
					<span id="lblTextoResultadosBusqueda" class="Negrita"></span>
				</div>
			</div>
			<div class="FondoHeader" style="clear:both; float:left; width:100%;">
				<div style="margin-left:10px;">
					<span id="lblTipoOrdenacion" class="Negrita" style="padding-right:5px;">DOrdenar por:</span>
					<span id="lblTipoOrdenacionFecha" style="padding-right:5px;">DMÃ¡s reciente (por defecto)</span>|<span id="lblTipoOrdenacionCoincidencias" class="Subrayado Link"  style="padding-left:5px;">DCoincidencias</span>
				</div>
			</div>
		</div>
		<div id="divMensajesResultadosBusqueda" class="divMuro"></div>
	</div>
	<div id="divCargando" style="display:none; float:left; width:75%; margin-top:30px; text-align:center;">
		<img id="imgCargando" alt="" src="" />
	</div>
	<div id="divNuevoMensajeNuevaEtiqueta" style="float:left; display:none;">
		<div class="NuevoMensajeEtiqueta">
			<input type="text" class="CajaTexto" style="width:90px;" />
			<img alt="" src="../../images/eliminar.gif" class="Image16" />
		</div>
	</div>  
</asp:Content>