﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_Master/Menu.master" CodeBehind="cn_Categorias.aspx.vb" Inherits="Fullstep.FSNWeb.cn_Categorias" %>
<asp:Content ID="CategoriasCN" ContentPlaceHolderID="CPH4" runat="server">
    <ig:WebExcelExporter ID="excelExporter" runat="server"></ig:WebExcelExporter>
    <ig:WebDocumentExporter ID="pdfExporter" runat="server" ExportMode="Download"></ig:WebDocumentExporter>
    <div id="divCabecera" style="clear: both; float: left; width: 100%;">
        <fsn:FSNPageHeader ID="FSNPageHeader" runat="server" UrlImagenCabecera="~/images/ColaboracionHeader.png" />
    </div>
    <!-- Menú izquierda -->
    <div id="divMenu" class="divMenu"></div>
    <div id="divPrincipal" class="divPrincipal" style="display: none;">
        <div id="divTituloMain" class="SeparadorInf" style="clear: both; float: left; width:100%; margin-bottom: 15px;">
            <asp:Label ID="lblTitulo" runat="server" CssClass="Texto14 Negrita TextoResaltado" style="float:left; padding: 10px;"></asp:Label>
        </div>
        <div id="tabCategorias" style="clear:both; float:left; margin-left:10px;" class="tabRedondeado tabActive">
            <span id="lbltabCategorias" class="Texto14 TextoClaro Negrita" style="line-height:15px;"></span>
        </div>
        <div id="tabDetalleCategoria" style="float:left; margin-left:3px;" class="tabRedondeado">
            <span id="lbltabDetalleCategoria" class="Texto14 TextoClaro Negrita" style="line-height:15px;"></span>
        </div>
        <div id="divTabCategorias" style="clear:both; float:left; width:95%; margin-left:10px;">
            <div class="CabeceraBotones" style="clear: both; float:left; width:100%; height: 25px; line-height: 25px;">
                <div style="float: right; margin-right: 5px; height: 25px;">
                    <div id="OcultarDespublicadas" class="Enlace" style="float: left; margin-left: 5px;">
                        <a id="lnkOcultarDespublicadas" class="Texto12 TextoResaltado Subrayado Link" 
                            onclick="javascript:onClient_click()"></a>
                    </div>
                    <div onclick="ExportarExcel()" class="botonDerechaCabecera" style="float: left; margin-left: 5px;">
                        <asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
                        <asp:Label runat="server" ID="lblExcel" CssClass="fntLogin2" Text="Excel"></asp:Label>
                    </div>
                    <div onclick="ExportarPDF()" class="botonDerechaCabecera" style="float: left; margin-left: 5px;">
                        <asp:Image runat="server" ID="imgPDF" CssClass="imagenCabecera" />
                        <asp:Label runat="server" ID="lblPDF" CssClass="fntLogin2" Text="PDF"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="CabeceraBotones" style="clear:both; float:left; width:100%;">
                <asp:Button ID="btnAceptarEx" runat="server" Text="Button" Style="display: none;"></asp:Button>
                <asp:UpdatePanel ID="upGridCategorias" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnAceptarEx" EventName="Click" />
                    </Triggers>
                    <ContentTemplate>
                        <ig:WebHierarchicalDataGrid ID="whdgCategorias" runat="server"
                            Width="100%" AutoGenerateColumns="false" AutoGenerateBands="false" CssClass="Texto12"
                            ShowHeader="true" InitialExpandDepth="0" InitialDataBindDepth="100" EnableAjax="false">                                                      
                            <ExpandCollapseAnimation SlideOpenDirection="Auto" SlideOpenDuration="300" 
                                SlideCloseDirection="Auto" SlideCloseDuration="300" />
                            <Behaviors>
                                <ig:ColumnResizing Enabled="true" />
                                <ig:Selection Enabled="true" RowSelectType="Single" CellSelectType="None" ColumnSelectType="None" CellClickAction="Row">
                                    <SelectionClientEvents RowSelectionChanging="whdgCategorias_Click" />
                                </ig:Selection>
                            </Behaviors>
                            <Columns>                                
                                <ig:BoundDataField DataFieldName="DENCAT" Key="Cat" Width="70%" />
                                <ig:BoundDataField DataFieldName="MENSAJES" Key="Mensajes" Width="20%" />
                                <ig:BoundDataField DataFieldName="PERMISOPARAPUB" Key="Permisos" Hidden="true" />
                                <ig:BoundDataField DataFieldName="DESPUBENRED" Key="NoPublicado" Hidden="true" />
                                <ig:TemplateDataField Key="PermisosImg" Width="10%">
                                    <ItemTemplate>
                                        <asp:Image ID="imgPermisos" runat="server" />
                                    </ItemTemplate>
                                </ig:TemplateDataField>
                            </Columns>
                        </ig:WebHierarchicalDataGrid>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div id="divTabDetalleCategoria" class="Bordear" style="display:none; clear:both; float:left; width:95%; margin-left:10px;">
            <div style="position:relative; clear:both; float:left; width:100%;">
                <div style="clear:both; float:left; width:100%; height:100%; margin:0px 0px 5px 0px;">
                    <div id="divCatPadre" style="float: left; width:100%;">
                        <div style="width:100%; clear:both; float:left;">
                            <span id="lblCatPadre" style="float:left; padding:5px;" class="Texto12 TextoResaltado"></span>
                        </div>
                        <div id="divCombos" style="clear:both; float:left; width:100%;"></div>
                        <div id="divPathCategoria" style="clear:both; float:left; width:100%;">
                            <span id="lblPathCateg" style="float: left; width: 100%; padding:0px 5px;"></span>
                        </div>
                    </div>
                    <div style="clear:both; float:left; min-width:610px;">
                        <fieldset style="margin: 15px 7px 0px 5px;">
                            <legend id="lblDenomCat" class="TextoResaltado"></legend>
                            <div id='divDenomCat' style="width: 55%; margin: 5px; padding: 5px; float: left;">
                            </div>
                        </fieldset>
                        <div style="clear:both; float:left; padding: 5px; white-space:nowrap;">                            
                            <span id="lblPermiso" style="clear:both; float: left; padding-bottom:3px;" class="Texto12 TextoResaltado"></span>                            
                            <div id="cboParaCatPermisoPublic" style="clear:both; float:left; width:200px;"></div>
                        </div>                        
                        <div id="divPublic" style="float:left; padding:5px; margin-top:20px;">
                            <span id="lblPublicada" style="padding-bottom:3px;" class="Texto12 TextoResaltado"></span>
                            <input id="chkPublicada" type="checkbox" />
                        </div>
                        <div id="divMiembrosCategoria" style="float:left; padding:5px; max-width:325px;">
                            <span id="lblMiembros" style="clear:both; float:left; padding-bottom:3px;" class="Texto12 TextoResaltado"></span>
                            <div id="divParaDestinatarios" style="clear:both; float:left; min-width:300px; max-width:400px;"></div>
                        </div>
                        <div id="divBotones" style="clear:both; float:left; margin-top:10px; margin-bottom:10px; margin-left:250px; min-width:350px;">
                            <div style="float:left;">
                                <div id="btnAceptarCategoria" class="botonRedondeado" style="float:left;">
                                    <span id="lblCategoriaAceptar">DAceptar</span>
                                </div>
                                <div id="btnCancelarCategoria" class="botonRedondeado" style="float:left; margin-left: 10px;">
                                    <span id="lblCategoriaCancelar">DCancelar</span>
                                </div>                                        
                            </div>
                            <div id="btnEliminarCategoria" class="botonRedondeado" style="float:left; margin-left:10px;">
                                <span id="lblCategoriaEliminar">DEliminar</span>
                            </div>
                            <div style="clear:both; float:left;">
                                <span id="lblCategoriaGuardadaOK" class="Texto14 Negrita TextoAlternativo" style="display:none;"></span>
                            </div>                                                 
                        </div>
                    </div>
                </div>                
            </div>            
        </div>
    </div>
    <div id="popupEliminarConfirm" class="popupCN" style="display: none; position: absolute;
        z-index: 1005;">
        <div style="position: relative; clear: both; float: left; width: 100%; margin-top: 15px;
            text-align: center;">
            <div style="width: 100%; clear: both; float: left;">
                <span id="lblPreguntaEliminarConfirm" style="float: left" class="Texto12"></span>
            </div>
        </div>
        <div style="position: relative; clear: both; float: left; width: 100%; margin-top: 15px; text-align: center;">
            <div id="btnAceptarEliminarConfirm" class="botonRedondeado" style="margin-right: 5px;">
                <span id="lblAceptarEliminarConfirm" style="line-height: 23px;">DAceptar</span>
            </div>
            <div id="btnCancelarEliminarConfirm" class="botonRedondeado" style="margin-left: 5px;">
                <span id="lblCancelarEliminarConfirm" style="line-height: 23px;">DCancelar</span>
            </div>
        </div>
    </div>
</asp:Content>
