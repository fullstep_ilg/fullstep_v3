﻿Imports System.Web
Imports System.Web.Services
Imports System.IO

Public Class Thumbnail
    Implements System.Web.IHttpHandler, IRequiresSessionState, IReadOnlySessionState
    Public ReadOnly Property TempRoot As String
        Get
            Return ConfigurationManager.AppSettings("temp") & "\"
        End Get
    End Property
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim filename As String
        Dim byteBuffer() As Byte
        Select Case context.Request("t")
            Case 0
                Dim CN_Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
                If context.Request("u") Is Nothing Then
                    If CN_Usuario.CNTieneImagen Then
                        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
                        Dim oMensajes As FSNServer.cnMensajes = FSNServer.Get_Object(GetType(FSNServer.cnMensajes))
                        byteBuffer = oMensajes.CN_Mensajes_GetImagenUsuario(CN_Usuario.CNGuidImagenUsu)
                    Else
                        filename = HttpContext.Current.Server.MapPath("~/Images/FSCN_NO_USU_IMAGE.png")
                        Dim input As System.IO.FileStream = New System.IO.FileStream(filename, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                        Dim buffer(input.Length - 1) As Byte
                        input.Read(buffer, 0, buffer.Length)
                        input.Close()
                        byteBuffer = buffer
                    End If
                Else
                    If context.Request("u") Is String.Empty Then
                        filename = HttpContext.Current.Server.MapPath("~/Images/FSCN_NO_IMAGE.png")
                        Dim input As System.IO.FileStream = New System.IO.FileStream(filename, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                        Dim buffer(input.Length - 1) As Byte
                        input.Read(buffer, 0, buffer.Length)
                        input.Close()
                        byteBuffer = buffer
                    Else
                        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
                        Dim dataDir As String = ConfigurationManager.AppSettings("temp") & "\users"
                        Dim oMensajes As FSNServer.cnMensajes = FSNServer.Get_Object(GetType(FSNServer.cnMensajes))
                        byteBuffer = oMensajes.CN_Mensajes_GetImagenUsuario(context.Request("u"))
                    End If
                End If
            Case 1, 2
                Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
                Dim oMensajes As FSNServer.cnMensajes = FSNServer.Get_Object(GetType(FSNServer.cnMensajes))
                byteBuffer = oMensajes.CN_Mensajes_GetAdjuntoMensaje(context.Request("f"))
            Case Else
                filename = TempRoot & context.Request("f")
                Dim input As System.IO.FileStream = New System.IO.FileStream(filename, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Dim buffer(input.Length - 1) As Byte
                input.Read(buffer, 0, buffer.Length)
                input.Close()
                byteBuffer = buffer
        End Select
        If byteBuffer IsNot Nothing Then
            context.Response.ContentType = context.Request("c")
            context.Response.BinaryWrite(byteBuffer)
        End If
    End Sub
    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
End Class