﻿Imports System.IO

Partial Public Class GestionaAjax
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request("accion") = "2" Then
            AñadirAdjuntoDesgloseDetalle()
        ElseIf Request("accion") = "3" Then
            If Not (Request("tipo") = "1" And Request("instancia") = "0") Then
                GuardarComentario(Request("adjunto"), Request("tipo"), Request("coment"), Request("instancia"))
            End If
        ElseIf Request("accion") = "4" Then
            ComprobarEliminarTarea(0)
        ElseIf Request("accion") = "5" Then
            ComprobarEliminarTarea(1)
        ElseIf Request("accion") = "6" Then
            LeerPlantillaExcel(Server.UrlDecode(Request("path")))
        ElseIf Request("accion") = "7" Then
            ComprobarMemoriaJustificativa()
        ElseIf Request("accion") = "8" Then
            CrearAdjuntosCopiados(False)
        ElseIf Request("accion") = "9" Then
            CrearAdjuntosCopiados(True)
        ElseIf Request("accion") = "10" Then
            AprobarRechazarInstancia(Request("idaccion"), Request("bloque"), Request("instancia"), Server.UrlDecode(Request("coment")))
        ElseIf Request("accion") = "11" Then
            GuardarComentarioInstancia(Request("instancia"), Server.UrlDecode(Request("coment")))
        Else
            Select Case Request("tipo")
                Case "3" 'Añadir Adjunto Desglose
                    AñadirAdjuntoDesglose()
                Case Else 'Añadir Adjunto
                    If Request("EsContrato") = 1 Then
                        AñadirAdjuntoContrato()
                    Else
                        AñadirAdjunto()
                    End If
            End Select
        End If
    End Sub
    ''' <summary>
    ''' Cuando se copia una fila de un desglose esta funcion crea los adjuntos que se copian
    ''' </summary>
    ''' <param name="bDefecto">True si se esta aÃ±adiendo una fila con adjuntos por defecto</param>
    ''' <remarks></remarks>
    Public Sub CrearAdjuntosCopiados(ByVal bDefecto As Boolean)
        Dim idAdjuntosAct As String = Request("AdjuntosAct")
        Dim idAdjuntosNew As String = Request("AdjuntosNew")
        idAdjuntosAct = idAdjuntosAct.Replace("xx", ",")
        idAdjuntosNew = idAdjuntosNew.Replace("xx", ",")
        Dim arrAdjuntos As String()
        If idAdjuntosAct = "" And idAdjuntosNew = "" Then
            Dim responde As HttpResponse = HttpContext.Current.Response

            Response.Clear()
            Response.ContentType = "text/plain"
            Response.Write(String.Empty)
            Response.End()
        Else
            Dim oAdjuntos As FSNServer.Adjuntos
            oAdjuntos = FSNServer.Get_Object(GetType(FSNServer.Adjuntos))
            arrAdjuntos = oAdjuntos.CrearAdjuntosCopiados(idAdjuntosAct, idAdjuntosNew, bDefecto)

            Dim responde As HttpResponse = HttpContext.Current.Response
            Response.Clear()
            Response.ContentType = "text/plain"
            Response.Write(String.Join("xx", arrAdjuntos))
            Response.End()
        End If
    End Sub
    ''' <summary>
    ''' Comprobar Memoria Justificativa
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load ; Tiempo máximo: 0,2</remarks>
    Public Sub ComprobarMemoriaJustificativa()
        Dim respuesta As Boolean = False
        If Not Session("idAdjunto_MemJustificativa") = Nothing Then
            respuesta = True
        End If

        Dim responde As HttpResponse = HttpContext.Current.Response
        Response.Clear()
        Response.ContentType = "text/plain"

        Response.Write(IIf(respuesta = True, "1", "0"))
        Response.End()
    End Sub
    ''' <summary>
    ''' Funcion que comprueba si la tarea o el proyecto tiene horas imputadas
    ''' </summary>
    ''' <param name="bTipo">0 si es una tarea en concreto y 1 si es el proyecto el que se comprueba</param>
    ''' <remarks></remarks>
    Private Sub ComprobarEliminarTarea(ByVal bTipo As Byte)
        Dim oProyecto As FSNServer.Proyecto
        Dim sValidacion As String = Nothing

        oProyecto = FSNServer.Get_Object(GetType(FSNServer.Proyecto))
        Select Case bTipo
            Case 0
                sValidacion = oProyecto.ComprobarEliminarTarea(CLng(Request("Tarea")), 0)
            Case 1
                sValidacion = oProyecto.ComprobarEliminarTarea(0, CLng(Request("Proyecto")))
        End Select

        Dim responde As HttpResponse = HttpContext.Current.Response
        Response.Clear()
        Response.ContentType = "text/plain"
        Response.Write(sValidacion)
        Response.End()
    End Sub
    ''' <summary>
    ''' Lee la plantilla y devuelve su contenido para añadirlo al grid de desglose de actividad
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LeerPlantillaExcel(ByVal sPlantilla As String)
        Dim oProyecto As FSNServer.Proyecto

        oProyecto = FSNServer.Get_Object(GetType(FSNServer.Proyecto))
        Dim sIdi As String = ""

        sIdi = ConfigurationManager.AppSettings("idioma")
        Dim sValores As String
        sValores = oProyecto.LeerPlantillaExcel(Server.UrlDecode(sPlantilla), sIdi)

        Dim responde As HttpResponse = HttpContext.Current.Response
        Response.Clear()
        Response.ContentType = "text/plain"

        Response.Write(sValores)
        Response.End()
    End Sub
    ''' <summary>
    ''' Crea el control con los adjuntos que estan dentro de un desglose
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub AñadirAdjuntoDesglose()
        Dim oTblInput As New System.Web.UI.HtmlControls.HtmlTable
        Dim oTblCellInput As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oTblRowInput As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oHidTipo As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidFavorito As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidCampo As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidInstancia As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidID As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim hidNomAdjuntos As New HtmlControls.HtmlInputHidden
        Dim sNombreAdjuntos As String = Nothing
        Dim bSoloUnArchivo As Boolean = False
        Dim oWidth As System.Web.UI.WebControls.Unit
        Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oAdjuntos As FSNServer.Adjuntos
        Dim idCampo As Integer
        Dim idAdjuntos As String = String.Empty
        Dim idAdjuntosNew As String = String.Empty
        Dim NombreAdjunto As String = String.Empty
        Dim Instancia As Long
        Dim bFavorito As Boolean
        Dim iTipo As Integer
        Dim EntryID As String = Nothing
        Dim SoloLectura As String = Nothing
        Dim Origen As String
        Dim sDataDefecto As String = Nothing
        Dim oUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim sIdi As String = oUser.Idioma.ToString()
        Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))

        oAdjuntos = FSWSServer.Get_Object(GetType(FSNServer.Adjuntos))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.Adjuntos, sIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        'Recogemos el QueryString
        If Request("defecto") <> "" Then
            sDataDefecto = Request("defecto")
        End If
        If Request("tipo") <> "" Then
            iTipo = Request("tipo")
        End If
        If Request("campo") <> "" Then
            idCampo = Request("campo")
        End If
        If Request("instancia") <> "" Then
            Instancia = Request("instancia")
        End If
        If Request("adjunto") <> "" Then
            idAdjuntos = Request("adjunto")
        End If
        If Request("adjuntoNew") <> "" Then
            idAdjuntosNew = Request("adjuntoNew")
        End If
        If Request("nombre") <> "" Then
            NombreAdjunto = Request("nombre")
        End If
        If Request("EntryID") <> "" Then
            EntryID = Request("EntryID")
        End If
        If Request("readOnly") <> "" Then
            SoloLectura = Request("readOnly")
        End If
        If Request("origen") <> "" Then
            Origen = Request("origen")
        End If

        Dim objAdjuntos() As String = System.Text.RegularExpressions.Regex.Split(idAdjuntos, "xx")
        Dim objAdjuntosNew() As String = System.Text.RegularExpressions.Regex.Split(idAdjuntosNew, "xx")

        Dim TotalAdjuntos As Short = IIf(objAdjuntos.Length = 1 And objAdjuntos(0) = "", 0, objAdjuntos.Length) + IIf(objAdjuntosNew.Length = 1 And objAdjuntosNew(0) = "", 0, objAdjuntosNew.Length)
        If Not String.IsNullOrEmpty(idAdjuntos) Then idAdjuntos = Replace(idAdjuntos, "xx", ",")
        If Not String.IsNullOrEmpty(idAdjuntosNew) Then idAdjuntosNew = Replace(idAdjuntosNew, "xx", ",")

        'Obtenemos lo adjuntos
        If Instancia = 0 Then
            oAdjuntos.Load(iTipo, idAdjuntos, idAdjuntosNew, bFavorito)
        Else
            oAdjuntos.LoadInst(iTipo, idAdjuntos, idAdjuntosNew)
            'Si es una adjunto metido desde Añadir por valor por defecto. La información
            'esta en LINEA_DESGLOSE_ADJUN
            Dim sPatron As String = sDataDefecto
            Dim sPatron1 As String = sDataDefecto & ",*"
            Dim sPatron2 As String = "*," & sDataDefecto & ",*"
            Dim sPatron3 As String = "*," & sDataDefecto
            If idAdjuntos Like sPatron Or idAdjuntos Like sPatron1 Or idAdjuntos Like sPatron2 Or idAdjuntos Like sPatron3 Then
                oAdjuntos.Load(iTipo, idAdjuntos, idAdjuntosNew)
            End If
        End If

        oTblInput = New System.Web.UI.HtmlControls.HtmlTable
        oTblRowInput = New System.Web.UI.HtmlControls.HtmlTableRow
        oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell

        oTblInput.Width = oWidth.ToString
        oTblInput.CellPadding = 0
        oTblInput.CellSpacing = 0
        oTblInput.Border = 0
        oTblInput.Style.Item("display") = "inline"

        If oAdjuntos.Data.Tables.Count = 0 OrElse oAdjuntos.Data.Tables(0).Rows.Count = 0 Then
            'SI NO TIENE ADJUNTOS
            oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell
            hidNomAdjuntos.ID = EntryID & "__hidNombresAdj"
            hidNomAdjuntos.Value = ""

            Dim oLink As System.Web.UI.HtmlControls.HtmlAnchor
            oLink = New System.Web.UI.HtmlControls.HtmlAnchor
            oLink.ID = EntryID + "__t"
            oLink.Style.Item("display") = "none"
            oLink.Style("cursor") = "pointer"
            oLink.Style("with") = "150px"
            oLink.Attributes.Add("class", "enlaceAdj")
            oTblCellInput.Align = "right"
            oTblCellInput.Controls.Add(oLink)
            oTblCellInput.Controls.Add(hidNomAdjuntos)

            If Not SoloLectura = "true" Then
                Dim Enlace As New HtmlControls.HtmlAnchor
                Enlace.InnerText = oTextos.Rows(1).Item(1)
                Enlace.Attributes("onclick") = "show_atach_file('" + Instancia.ToString() + "', '" + idAdjuntos.ToString() + "','" + iTipo.ToString() + "', '" + EntryID + "','" & Me.AppRelativeVirtualPath & "',0)"
                Enlace.Style("text-decoration") = "underline"
                Enlace.Style("cursor") = "pointer"
                Enlace.Style("color") = "#0000FF"
                Enlace.Attributes.Add("class", "enlaceAdj")
                oTblCellInput.Controls.Add(Enlace)
            End If

            oTblRowInput.Cells.Add(oTblCellInput)
            oTblInput.Rows.Add(oTblRowInput)
            oTblInput.Width = "100%"
        Else 'SI TIENE ADJUNTOS
            oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell

            Dim oLink As System.Web.UI.HtmlControls.HtmlAnchor
            oLink = New System.Web.UI.HtmlControls.HtmlAnchor

            If oAdjuntos.Data.Tables(0).Rows.Count = 1 Then
                Dim TextoAdjunto As String = oAdjuntos.Data.Tables(0).Rows(0)("NOM").ToString + " (" + FSNLibrary.FormatNumber(oAdjuntos.Data.Tables(0).Rows(0)("DATASIZE"), oUser.NumberFormat) + " Kb)"
                oLink.InnerText = AjustarAnchoTextoPixels(TextoAdjunto, 150, "Verdana", 12, _
                                                 False)
                oLink.Attributes("onclick") = "descargarAdjunto('" & oAdjuntos.Data.Tables(0).Rows(0)("id") & "', '" & oAdjuntos.Data.Tables(0).Rows(0)("tipo") & "', '" & Instancia.ToString & "','" & EntryID & "')"
                sNombreAdjuntos = oAdjuntos.Data.Tables(0).Rows(0)("NOM").ToString + " (" + FSNLibrary.FormatNumber(oAdjuntos.Data.Tables(0).Rows(0)("DATASIZE"), oUser.NumberFormat) + " Kb)"
            Else
                Dim tipos As String = ""
                Dim TextoAdjunto As String = ""

                For i = 0 To oAdjuntos.Data.Tables(0).Rows.Count - 1
                    If tipos = "" Then
                        tipos = oAdjuntos.Data.Tables(0).Rows(i)("id").ToString & "," & oAdjuntos.Data.Tables(0).Rows(i)("tipo").ToString
                        TextoAdjunto = oAdjuntos.Data.Tables(0).Rows(i)("NOM").ToString + " (" + FSNLibrary.FormatNumber(oAdjuntos.Data.Tables(0).Rows(i)("DATASIZE"), oUser.NumberFormat) + " Kb)"
                    Else
                        tipos = tipos & "xx" & oAdjuntos.Data.Tables(0).Rows(i)("id").ToString & "," & oAdjuntos.Data.Tables(0).Rows(i)("tipo").ToString
                        TextoAdjunto = TextoAdjunto & "; " & oAdjuntos.Data.Tables(0).Rows(i)("NOM").ToString + " (" + FSNLibrary.FormatNumber(oAdjuntos.Data.Tables(0).Rows(i)("DATASIZE"), oUser.NumberFormat) + " Kb)"
                    End If
                    sNombreAdjuntos = IIf(sNombreAdjuntos = "", oAdjuntos.Data.Tables(0).Rows(i)("NOM") & " (" & oAdjuntos.Data.Tables(0).Rows(i)("DATASIZE") & " kb)", sNombreAdjuntos & ", " & oAdjuntos.Data.Tables(0).Rows(i)("NOM") & " (" & oAdjuntos.Data.Tables(0).Rows(i)("DATASIZE") & " kb)")
                Next
                'Si tiene mas de 1 adjunto

                oLink.InnerText = AjustarAnchoTextoPixels(TextoAdjunto, 150, "Verdana", 12, _
                                                False)
                oLink.Attributes("onclick") = "descargarAdjuntos('" + idAdjuntos.ToString + "', '" + idAdjuntosNew.ToString + "', '" & Instancia.ToString & "', '" & tipos.ToString & "','" & EntryID & "')"
            End If
            oLink.ID = EntryID + "__t"
            oLink.Attributes("text-align") = "left"
            oLink.Style("cursor") = "pointer"
            oLink.Attributes.Add("class", "enlaceAdj")
            oTblCellInput.Width = "150px"
            oTblCellInput.Controls.Add(oLink)

            'Meto el hidden con los nombre de todos los adjuntos
            hidNomAdjuntos.ID = EntryID & "__hidNombresAdj"
            hidNomAdjuntos.Value = sNombreAdjuntos
            oTblCellInput.Controls.Add(hidNomAdjuntos)

            oTblCellInput.Align = "right"

            oTblRowInput.Cells.Add(oTblCellInput)

            If Not SoloLectura = "true" Then
                Dim Enlace As New HtmlControls.HtmlAnchor
                Enlace.InnerText = oTextos.Rows(1).Item(1)
                Enlace.HRef = "#"
                Enlace.Attributes("onclick") = "show_atach_file('" + Instancia.ToString() + "', '0', '" + iTipo.ToString + "', '" + EntryID + "','" & Me.AppRelativeVirtualPath & "',0)"
                Enlace.Style("text-decoration") = "underline"
                Enlace.Style("cursor") = "pointer"
                Enlace.Style("padding-left") = "7px"
                Enlace.Style("color") = "#0000FF"
                Enlace.Attributes.Add("class", "enlaceAdj")
                oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell
                oTblCellInput.Controls.Add(Enlace)
                oTblCellInput.Align = "right"
                oTblCellInput.Width = "70px"
                oTblRowInput.Cells.Add(oTblCellInput)
            End If

            'Detalle

            oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell
            Dim EnlaceAdjuntos As New HtmlControls.HtmlAnchor
            EnlaceAdjuntos.InnerText = oTextos.Rows(3).Item(1)
            EnlaceAdjuntos.Style("cursor") = "pointer"
            EnlaceAdjuntos.Style("text-decoration") = "underline"
            EnlaceAdjuntos.Style("padding-left") = "7px"
            EnlaceAdjuntos.Style("color") = "#0000FF"
            EnlaceAdjuntos.Attributes.Add("class", "enlaceAdj")
            EnlaceAdjuntos.Attributes("onclick") = "show_atach_fileDesglose('" + Instancia.ToString() + "', '" + idAdjuntos.ToString + "','" + idAdjuntosNew + "', '" + iTipo.ToString + "', '" + EntryID + "',0)"
            oTblCellInput.Controls.Add(EnlaceAdjuntos)
            oTblCellInput.Align = "right"
            oTblCellInput.Width = "70px"
            oTblRowInput.Cells.Add(oTblCellInput)
            oTblInput.Style.Add("table-layout", "fixed")
            oTblInput.Rows.Add(oTblRowInput)
        End If

        Dim outHTML As String
        outHTML = RenderizarControl(oTblInput)

        Dim responde As HttpResponse = HttpContext.Current.Response
        Response.Clear()
        Response.ContentType = "text/plain"

        Response.Write(outHTML)
        Response.End()
    End Sub
    ''' <summary>
    ''' Añade el adjunto a la tabla que los contiene
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub AñadirAdjunto()
        Dim oTblInput As System.Web.UI.HtmlControls.HtmlTable
        Dim oTblCellInput As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oTblRowInput As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oTblOpciones As System.Web.UI.HtmlControls.HtmlTable
        Dim oTblCellOpciones As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oTblRowOpciones As System.Web.UI.HtmlControls.HtmlTableRow
        Dim hidNomAdjuntos As New HtmlControls.HtmlInputHidden
        Dim sNombreAdjuntos As String = Nothing
        Dim idCampo As Integer
        Dim idAdjuntos As String = String.Empty
        Dim idAdjuntosNew As String = String.Empty
        Dim NombreAdjunto As String = String.Empty
        Dim Instancia As Long
        Dim bFavorito As Boolean
        Dim iTipo As Integer
        Dim EntryID As String = Nothing
        Dim SoloLectura As String = Nothing
        Dim Origen As String = Nothing
        Dim bConContrato As Boolean
        Dim oUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim sIdi As String = oUser.Idioma.ToString()
        If sIdi = Nothing Then sIdi = ConfigurationManager.AppSettings("idioma")
        Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.Adjuntos, sIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        'Recogemos el QueryString
        If Request("tipo") <> "" Then iTipo = Request("tipo")
        If Request("campo") <> "" Then idCampo = Request("campo")
        If Request("instancia") <> "" Then Instancia = Request("instancia")
        If Request("adjunto") <> "" Then idAdjuntos = Request("adjunto")
        If Request("adjuntoNew") <> "" Then idAdjuntosNew = Request("adjuntoNew")
        If Not Request("MemJustificativa") Is Nothing Then
            While Session("idAdjunto_MemJustificativa") = Nothing
                Threading.Thread.Sleep(400)
            End While
            idAdjuntosNew = Session("idAdjunto_MemJustificativa").ToString
            SoloLectura = False
        End If

        If Request("nombre") <> "" Then NombreAdjunto = Request("nombre")
        If Request("EntryID") <> "" Then EntryID = Request("EntryID")
        If Request("readOnly") <> "" Then SoloLectura = Request("readOnly")
        If Request("origen") <> "" Then Origen = Request("origen")

        bConContrato = False
        If Request("IdContrato") <> "" And Request("IdContrato") <> "0" Then bConContrato = True
        If idAdjuntos <> "" Then idAdjuntos = Replace(idAdjuntos, "xx", ",")
        If idAdjuntosNew <> "" Then idAdjuntosNew = Replace(idAdjuntosNew, "xx", ",")

        Dim oAdjuntos As FSNServer.Adjuntos
        oAdjuntos = FSWSServer.Get_Object(GetType(FSNServer.Adjuntos))

        If Not (idAdjuntos = "" AndAlso idAdjuntosNew = "") Then
            'Obtenemos lo adjuntos
            If Instancia = 0 Then
                oAdjuntos.Load(iTipo, idAdjuntos, idAdjuntosNew, bFavorito)
            Else
                oAdjuntos.LoadInst(iTipo, idAdjuntos, idAdjuntosNew)
                'Si es una adjunto metido desde Añadir por valor por defecto. La información
                'esta en LINEA_DESGLOSE_ADJUN
                If oAdjuntos.Data.Tables(0).Rows.Count = 0 Then
                    oAdjuntos.Load(iTipo, idAdjuntos, idAdjuntosNew)
                End If
            End If
        End If

        oTblInput = New System.Web.UI.HtmlControls.HtmlTable
        oTblRowInput = New System.Web.UI.HtmlControls.HtmlTableRow
        oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell

        oTblInput.Width = Unit.Percentage(100).ToString
        oTblInput.CellPadding = 0
        oTblInput.CellSpacing = 0
        oTblInput.Border = 0

        If oAdjuntos.Data Is Nothing OrElse (oAdjuntos.Data.Tables.Count = 0 OrElse oAdjuntos.Data.Tables(0).Rows.Count = 0) Then
            'SI NO TIENE ADJUNTOS
            'Puede que le quede el adjunto del Contrato
            Dim sNombreAdjuntoContrato As String = ""
            If bConContrato Then
                Dim arrAux() As String
                If NombreAdjunto <> "" Then
                    arrAux = Split(NombreAdjunto, ";")
                    sNombreAdjuntoContrato = arrAux(0)
                End If
                'Anteriormente tiene una adjunto de contratos
                oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell

                If sNombreAdjuntoContrato <> "" Then
                    '--Si tiene algun contrato adjunto
                    Dim EnlaceDescargarTodos As New HtmlControls.HtmlAnchor
                    EnlaceDescargarTodos.InnerText = sNombreAdjuntoContrato
                    EnlaceDescargarTodos.Style("text-decoration") = "underline"
                    EnlaceDescargarTodos.Style("color") = "#0000FF"
                    EnlaceDescargarTodos.Style("cursor") = "pointer"
                    EnlaceDescargarTodos.Attributes.Add("class", "enlaceAdj")
                    EnlaceDescargarTodos.ID = -1
                    EnlaceDescargarTodos.Attributes("onclick") = "descargarAdjuntoContrato('" + Instancia.ToString + "', '" & Request("IdContrato") & "','" & sNombreAdjuntoContrato & "')"
                    oTblCellInput.Controls.Add(EnlaceDescargarTodos)
                Else
                    'Añadir el archivo contrato
                    Dim lblFichero As New Label

                    'Enlace fichero
                    lblFichero.Text = sNombreAdjuntoContrato
                    lblFichero.ID = -1
                    oTblCellInput.Controls.Add(lblFichero)
                End If

                Dim inputID As New HtmlControls.HtmlInputHidden
                inputID.ID = "Adj_" & "-1"
                inputID.Value = "-1" & "###" & "1" & "###" & Instancia.ToString
                oTblCellInput.Align = "left"
                oTblCellInput.Controls.Add(inputID)
                oTblRowInput.Cells.Add(oTblCellInput)
            End If
            oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell
            If SoloLectura.ToLower = "false" Then
                Dim Enlace As New HtmlControls.HtmlAnchor
                Enlace.InnerText = oTextos.Rows(1).Item(1)
                Enlace.Attributes("onclick") = "show_atach_file('" + Instancia.ToString() + "', '" + idAdjuntos.ToString + "', '" + iTipo.ToString + "', '" + EntryID + "','" & Origen & "','" & Request("IdContrato") & "')"
                Enlace.Attributes.Add("class", "enlaceAdj")
                Enlace.Style("text-decoration") = "underline"
                Enlace.Style("color") = "#0000FF"
                Enlace.Style("cursor") = "pointer"
                oTblCellInput.Controls.Add(Enlace)
                oTblCellInput.Align = "right"
            End If
            'oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell
            Dim oLink As System.Web.UI.HtmlControls.HtmlAnchor
            oLink = New System.Web.UI.HtmlControls.HtmlAnchor
            oLink.ID = EntryID + "_t"
            oLink.Style.Item("display") = "none"
            oLink.HRef = "#"
            oLink.Attributes.Add("class", "enlaceAdj")
            hidNomAdjuntos.ID = EntryID & "__hidNombresAdj"
            hidNomAdjuntos.Value = sNombreAdjuntoContrato
            oTblCellInput.Controls.Add(oLink)
            oTblCellInput.Controls.Add(hidNomAdjuntos)
            oTblRowInput.Cells.Add(oTblCellInput)
            oTblInput.Rows.Add(oTblRowInput)
        Else
            Dim sNombreAdjuntoContrato As String = ""
            If bConContrato Then
                If NombreAdjunto <> "" Then
                    Dim arrAux() As String
                    arrAux = Split(NombreAdjunto, ";")
                    sNombreAdjuntoContrato = arrAux(0)
                End If
                'Anteriormente tiene una adjunto de contratos
                oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell

                If sNombreAdjuntoContrato <> "" Then
                    '--Si tiene algun contrato adjunto
                    Dim EnlaceDescargarTodos As New HtmlControls.HtmlAnchor
                    EnlaceDescargarTodos.InnerText = sNombreAdjuntoContrato
                    EnlaceDescargarTodos.Style("text-decoration") = "underline"
                    EnlaceDescargarTodos.Style("color") = "#0000FF"
                    EnlaceDescargarTodos.Style("cursor") = "pointer"
                    EnlaceDescargarTodos.Attributes.Add("class", "enlaceAdj")
                    EnlaceDescargarTodos.ID = -1
                    EnlaceDescargarTodos.Attributes("onclick") = "descargarAdjuntoContrato('" + Instancia.ToString + "', '" & Request("IdContrato") & "','" & sNombreAdjuntoContrato & "')"
                    oTblCellInput.Controls.Add(EnlaceDescargarTodos)
                Else
                    'Añadir el archivo contrato
                    Dim lblFichero As New Label

                    'Enlace fichero
                    lblFichero.Text = sNombreAdjuntoContrato
                    lblFichero.ID = -1
                    oTblCellInput.Controls.Add(lblFichero)
                End If

                Dim inputID As New HtmlControls.HtmlInputHidden
                inputID.ID = "Adj_" & "-1"
                inputID.Value = "-1" & "###" & "1" & "###" & Instancia.ToString
                oTblCellInput.Align = "left"
                oTblCellInput.Controls.Add(inputID)
                oTblRowInput.Cells.Add(oTblCellInput)
            End If
            'SI TIENE ADJUNTOS
            oTblOpciones = New System.Web.UI.HtmlControls.HtmlTable
            oTblRowOpciones = New System.Web.UI.HtmlControls.HtmlTableRow
            oTblCellOpciones = New System.Web.UI.HtmlControls.HtmlTableCell

            oTblOpciones.CellPadding = 3
            oTblOpciones.CellSpacing = 0
            oTblOpciones.Border = 0
            oTblOpciones.Align = "right"
            oTblOpciones.Style.Item("display") = "inline"

            oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell

            If SoloLectura.ToLower = "false" Then
                Dim Enlace As New HtmlControls.HtmlAnchor
                Enlace.InnerText = oTextos.Rows(1).Item(1)
                Enlace.Attributes("onclick") = "show_atach_file('" + Instancia.ToString() + "', '0', '" + iTipo.ToString + "', '" + EntryID + "','" & Origen & "','" & Request("IdContrato") & "')"
                Enlace.Style("text-decoration") = "underline"
                Enlace.Attributes.Add("class", "enlaceAdj")
                Enlace.Style("color") = "#0000FF"
                Enlace.Style("cursor") = "pointer"
                oTblCellOpciones.Controls.Add(Enlace)
            End If

            Dim oLink As System.Web.UI.HtmlControls.HtmlAnchor
            oLink = New System.Web.UI.HtmlControls.HtmlAnchor
            oLink.ID = EntryID + "_t"
            oLink.Style.Item("display") = "none"
            oLink.HRef = "#"
            oLink.Attributes.Add("class", "enlaceAdj")
            oTblCellOpciones.Controls.Add(oLink)
            'oTblCellInput.Align = "right"

            oTblRowOpciones.Cells.Add(oTblCellOpciones)

            If oAdjuntos.Data.Tables(0).Rows.Count > 1 Then
                'Si tiene mas de un adjunto
                'Descargar Todos
                oTblCellOpciones = New System.Web.UI.HtmlControls.HtmlTableCell
                Dim EnlaceDescargarTodos As New HtmlControls.HtmlAnchor
                EnlaceDescargarTodos.InnerText = oTextos.Rows(4).Item(1)
                EnlaceDescargarTodos.Style("text-decoration") = "underline"
                EnlaceDescargarTodos.Style("color") = "#0000FF"
                EnlaceDescargarTodos.Style("cursor") = "pointer"
                EnlaceDescargarTodos.Attributes.Add("class", "enlaceAdj")
                Dim tipos As String = ""
                For i = 0 To oAdjuntos.Data.Tables(0).Rows.Count - 1
                    If tipos = "" Then
                        tipos = oAdjuntos.Data.Tables(0).Rows(i)("id").ToString & "," & oAdjuntos.Data.Tables(0).Rows(i)("tipo").ToString
                    Else
                        tipos = tipos & "xx" & oAdjuntos.Data.Tables(0).Rows(i)("id").ToString & "," & oAdjuntos.Data.Tables(0).Rows(i)("tipo").ToString
                    End If
                Next
                EnlaceDescargarTodos.Attributes("onclick") = "descargarAdjuntos('" + idAdjuntos.ToString + "', '" + idAdjuntosNew.ToString + "', '" & Instancia.ToString & "', '" & tipos.ToString & "','" & EntryID & "')"
                oTblCellOpciones.Controls.Add(EnlaceDescargarTodos)
                'oTblCellInput.Align = "right"
                oTblRowOpciones.Cells.Add(oTblCellOpciones)

                If SoloLectura.ToLower = "false" Then
                    'Eliminar Todos
                    oTblCellOpciones = New System.Web.UI.HtmlControls.HtmlTableCell
                    Dim EnlaceEliminarTodos As New HtmlControls.HtmlAnchor
                    EnlaceEliminarTodos.InnerText = oTextos.Rows(6).Item(1)
                    EnlaceEliminarTodos.Style("text-decoration") = "underline"
                    EnlaceEliminarTodos.Attributes.Add("class", "enlaceAdj")
                    EnlaceEliminarTodos.Style("color") = "#0000FF"
                    EnlaceEliminarTodos.Style("cursor") = "pointer"
                    EnlaceEliminarTodos.Attributes("onclick") = "eliminarAdjuntos('" + EntryID + "'," & IIf(bConContrato, 1, 0) & ")"
                    oTblCellOpciones.Controls.Add(EnlaceEliminarTodos)
                    oTblCellOpciones.Align = "right"
                    oTblRowOpciones.Cells.Add(oTblCellOpciones)
                End If
            End If

            oTblOpciones.Rows.Add(oTblRowOpciones)

            oTblCellInput.Controls.Add(oTblOpciones)
            oTblRowInput.Cells.Add(oTblCellInput)
            oTblInput.Rows.Add(oTblRowInput)

            'Cargo los adjuntos
            For i = 0 To oAdjuntos.Data.Tables(0).Rows.Count - 1
                Dim filaContenedora As New HtmlControls.HtmlTableRow
                Dim celdaContenedora As New HtmlControls.HtmlTableCell
                Dim tablaContenido As New HtmlControls.HtmlTable
                Dim filaContenido As HtmlControls.HtmlTableRow
                Dim celdaContenido As HtmlControls.HtmlTableCell
                Dim EnlaceFichero As New HtmlControls.HtmlAnchor
                Dim lblPersona As New Label
                Dim imgEliminar As New HtmlControls.HtmlImage
                Dim imgSustituir As New HtmlControls.HtmlImage
                Dim txtComentarios As New TextBox
                Dim txtComentariosReadOnly As New HtmlControls.HtmlTextArea
                Dim hidComentario As New HtmlControls.HtmlInputHidden
                Dim hidAdjMemJustificativa As New HtmlControls.HtmlInputHidden
                Dim oAdjunto As FSNServer.Adjunto

                oAdjunto = FSWSServer.Get_Object(GetType(FSNServer.Adjunto))
                oAdjunto.Id = oAdjuntos.Data.Tables(0).Rows(i)("ID")

                tablaContenido.Style.Add("table-layout", "fixed")

                filaContenido = New HtmlControls.HtmlTableRow
                celdaContenido = New HtmlControls.HtmlTableCell
                celdaContenido.Width = "30%"
                celdaContenido.Attributes.Add("max-width", "30%")
                filaContenido.Cells.Add(celdaContenido)
                celdaContenido = New HtmlControls.HtmlTableCell
                celdaContenido.Width = Unit.Percentage(49).ToString
                filaContenido.Cells.Add(celdaContenido)
                If SoloLectura.ToLower = "false" Then
                    celdaContenido = New HtmlControls.HtmlTableCell
                    celdaContenido.Width = Unit.Percentage(4).ToString
                    filaContenido.Cells.Add(celdaContenido)
                    celdaContenido = New HtmlControls.HtmlTableCell
                    celdaContenido.Width = Unit.Percentage(4).ToString
                    filaContenido.Cells.Add(celdaContenido)
                End If
                tablaContenido.Rows.Add(filaContenido)

                filaContenido = New HtmlControls.HtmlTableRow
                'Enlace fichero
                EnlaceFichero.InnerText = oAdjuntos.Data.Tables(0).Rows(i)("NOM") & " (" & oAdjuntos.Data.Tables(0).Rows(i)("DATASIZE") & " kb)"
                sNombreAdjuntos = IIf(sNombreAdjuntos = "", oAdjuntos.Data.Tables(0).Rows(i)("NOM") & " (" & oAdjuntos.Data.Tables(0).Rows(i)("DATASIZE") & " kb)", sNombreAdjuntos & ", " & oAdjuntos.Data.Tables(0).Rows(i)("NOM") & " (" & oAdjuntos.Data.Tables(0).Rows(i)("DATASIZE") & " kb)")
                EnlaceFichero.ID = oAdjuntos.Data.Tables(0).Rows(i)("ID")
                EnlaceFichero.Attributes("onclick") = "descargarAdjunto('" & oAdjuntos.Data.Tables(0).Rows(i)("id") & "', '" & oAdjuntos.Data.Tables(0).Rows(i)("tipo") & "', '" & Instancia.ToString & "','" & EntryID & "')"
                EnlaceFichero.Style("text-decoration") = "underline"
                EnlaceFichero.Attributes.Add("class", "enlaceAdj")
                EnlaceFichero.Style("color") = "#0000FF"
                EnlaceFichero.Style("cursor") = "pointer"
                celdaContenido = New HtmlControls.HtmlTableCell
                celdaContenido.Width = "30%"
                celdaContenido.Attributes.Add("word-wrap", "break-word")
                'Input Hidden con el ID y tipo del adjunto y la instancia
                Dim inputID As New HtmlControls.HtmlInputHidden
                inputID.ID = "Adj_" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                inputID.Value = oAdjuntos.Data.Tables(0).Rows(i)("ID") & "###" & oAdjuntos.Data.Tables(0).Rows(i)("tipo") & "###" & Instancia.ToString
                celdaContenido.Controls.Add(EnlaceFichero)
                celdaContenido.Controls.Add(inputID)
                If Not Request("MemJustificativa") Is Nothing Then
                    hidAdjMemJustificativa.ID = "__hidIdMemJustif"
                    hidAdjMemJustificativa.Value = oAdjuntos.Data.Tables(0).Rows(i)("ID")
                    celdaContenido.Controls.Add(hidAdjMemJustificativa)
                End If
                filaContenido.Cells.Add(celdaContenido)

                'Persona que adjunto el documento
                celdaContenido = New HtmlControls.HtmlTableCell
                celdaContenido.Width = Unit.Percentage(49).ToString
                lblPersona.Text = oAdjuntos.Data.Tables(0).Rows(i)("NOMBRE") & "( " & oAdjuntos.Data.Tables(0).Rows(i)("FECALTA") & " )"
                lblPersona.CssClass = "fntLogin"
                celdaContenido.Controls.Add(lblPersona)
                filaContenido.Cells.Add(celdaContenido)

                'Eliminar adjunto
                If SoloLectura.ToLower = "false" Then
                    celdaContenido = New HtmlControls.HtmlTableCell
                    celdaContenido.Width = Unit.Percentage(4).ToString
                    imgEliminar.Alt = oTextos.Rows(0).Item(1)
                    imgEliminar.ID = "Eliminar_" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                    imgEliminar.Src = ConfigurationManager.AppSettings("ruta") & "images/eliminar.gif"
                    imgEliminar.Attributes("onclick") = "borrarAdjunto('" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "', '" & oAdjuntos.Data.Tables(0).Rows(i)("tipo").ToString & "', '" & Instancia.ToString & "','" & EntryID.ToString & "','" & Origen & "','" & IIf(bConContrato, Request("IdContrato"), 0) & "')"
                    celdaContenido.Controls.Add(imgEliminar)
                    celdaContenido.Align = "right"
                    filaContenido.Cells.Add(celdaContenido)
                End If

                'Sustituir adjunto
                If SoloLectura.ToLower = "false" Then
                    celdaContenido = New HtmlControls.HtmlTableCell
                    celdaContenido.Width = Unit.Percentage(4).ToString
                    imgEliminar.ID = "Sustituir_" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                    imgSustituir.Alt = oTextos.Rows(2).Item(1)
                    imgSustituir.Attributes("onclick") = "sustituirAdjunto('" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "', '" & oAdjuntos.Data.Tables(0).Rows(i)("tipo").ToString & "', '" & Instancia.ToString & "','" & EntryID.ToString & "','" & Origen & "', '" + Replace(Server.UrlEncode(oAdjuntos.Data.Tables(0).Rows(i)("NOM").ToString()), "'", "\'") + "')"
                    imgSustituir.Src = ConfigurationManager.AppSettings("rutaPM") & "_common/images/sustituir.gif"
                    celdaContenido.Controls.Add(imgSustituir)
                    celdaContenido.Align = "right"
                    filaContenido.Cells.Add(celdaContenido)
                End If

                tablaContenido.Rows.Add(filaContenido)

                'Añado el textArea para los comentarios
                filaContenido = New HtmlControls.HtmlTableRow

                hidComentario.ID = "__hidComent" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                hidComentario.Value = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"))
                If SoloLectura.ToLower = "false" Then
                    txtComentarios.ID = EntryID & "__text" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                    txtComentarios.TextMode = TextBoxMode.MultiLine
                    txtComentarios.Height = Unit.Pixel(33)
                    txtComentarios.MaxLength = 500
                    txtComentarios.Text = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"))
                    txtComentarios.Style("overflow") = "hidden"
                    txtComentarios.Attributes("onfocus") = "aumentarTamanoCajaTexto('" + EntryID.ToString + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "',0)"
                    txtComentarios.Attributes("onblur") = "reducirTamanoCajaTexto('" + EntryID.ToString + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("TIPO").ToString + "','" + Instancia.ToString + "',1,0)"
                    txtComentarios.Attributes("onchange") = "return validarLengthyCortarStringLargo(this,500,'" & oTextos.Rows(9).Item(1) & "'); "
                    txtComentarios.Width = Unit.Percentage(90)
                    'txtComentarios.Columns = 100
                    celdaContenido = New HtmlControls.HtmlTableCell
                    celdaContenido.Attributes("colspan") = "4"
                    celdaContenido.Width = "100%"
                    celdaContenido.Align = "left"
                    celdaContenido.Controls.Add(txtComentarios)
                    celdaContenido.Controls.Add(hidComentario)
                    filaContenido.Cells.Add(celdaContenido)
                    tablaContenido.Rows.Add(filaContenido)
                Else
                    If oAdjuntos.Data.Tables(0).Rows(i)("COMENT").ToString.Trim <> "" Then
                        Dim bAjustarTamanoCajaTexto As Boolean
                        Dim posicion As Integer
                        txtComentariosReadOnly.ID = "__text" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                        txtComentariosReadOnly.Cols = 55
                        txtComentariosReadOnly.Rows = 2
                        posicion = InStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"), Chr(13) & Chr(10))
                        If posicion > 0 Then
                            posicion = InStr(posicion + 2, oAdjuntos.Data.Tables(0).Rows(i)("COMENT"), Chr(13) & Chr(10))
                            If posicion > 0 Then
                                txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")).Substring(0, posicion - 1) & " ...."
                                bAjustarTamanoCajaTexto = True
                            Else
                                If Len(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")) > 110 Then
                                    bAjustarTamanoCajaTexto = True
                                    txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")).Substring(0, 90) & " ...."
                                    posicion = 90
                                Else
                                    txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"))
                                End If
                            End If
                        Else
                            If Len(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")) > 110 Then
                                bAjustarTamanoCajaTexto = True
                                txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")).Substring(0, 90) & " ...."
                                posicion = 90
                            Else
                                txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"))
                            End If

                        End If

                        txtComentariosReadOnly.Style("border") = "none"
                        txtComentariosReadOnly.Style("width") = "90%"
                        txtComentariosReadOnly.Style("overflow") = "hidden"
                        txtComentariosReadOnly.Style("background-color") = "Transparent"
                        If bAjustarTamanoCajaTexto Then
                            txtComentariosReadOnly.Attributes("onmouseover") = "aumentarTamanoCajaTexto('" + EntryID.ToString + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "',1)"
                            txtComentariosReadOnly.Attributes("onblur") = "reducirTamanoCajaTexto('" + EntryID.ToString + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("TIPO").ToString + "','" + Instancia.ToString + "',0,'" + (posicion - 1).ToString + "')"
                        End If
                        txtComentariosReadOnly.Attributes("readonly") = "true"
                        celdaContenido = New HtmlControls.HtmlTableCell
                        celdaContenido.Attributes("colspan") = "4"
                        celdaContenido.Width = "100%"
                        celdaContenido.Align = "center"
                        celdaContenido.Controls.Add(txtComentariosReadOnly)
                        celdaContenido.Controls.Add(hidComentario)
                        filaContenido.Cells.Add(celdaContenido)
                        tablaContenido.Rows.Add(filaContenido)
                    End If
                End If
                'Añado un Width a la tabla de contenido
                tablaContenido.Width = "100%"
                'Añado la tabla con el adjunto a la tabla global
                celdaContenedora.ColSpan = 3
                celdaContenedora.Controls.Add(tablaContenido)
                'Meto el hidden con los nombre de todos los adjuntos
                If i = oAdjuntos.Data.Tables(0).Rows.Count - 1 Then
                    hidNomAdjuntos.ID = EntryID & "__hidNombresAdj"
                    hidNomAdjuntos.Value = sNombreAdjuntos
                    celdaContenido.Controls.Add(hidNomAdjuntos)
                End If
                filaContenedora.Cells.Add(celdaContenedora)
                oTblInput.Rows.Add(filaContenedora)
            Next
        End If

        Dim outHTML As String
        outHTML = RenderizarControl(oTblInput)

        Dim responde As HttpResponse = HttpContext.Current.Response
        Response.Clear()
        Response.ContentType = "text/plain"

        Response.Write(outHTML)
        Response.End()
    End Sub
    ''' <summary>
    ''' Añade el adjunto a la tabla que los contiene
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub AñadirAdjuntoContrato()
        Dim oTblInput As System.Web.UI.HtmlControls.HtmlTable
        Dim oTblCellInput As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oTblRowInput As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oTblOpciones As System.Web.UI.HtmlControls.HtmlTable
        Dim oTblCellOpciones As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oTblRowOpciones As System.Web.UI.HtmlControls.HtmlTableRow
        Dim hidNomAdjuntos As New HtmlControls.HtmlInputHidden
        Dim sNombreAdjuntos As String = Nothing
        Dim idCampo As Integer
        Dim idAdjuntos As String = String.Empty
        Dim idAdjuntosNew As String = String.Empty
        Dim NombreAdjunto As String = String.Empty
        Dim idContrato As Long
        Dim idArchivoContrato As Long
        Dim Instancia As Long
        Dim iTipo As Integer
        Dim EntryID As String = Nothing
        Dim SoloLectura As String = Nothing
        Dim Origen As String = Nothing
        Dim bConContrato As Boolean
        Dim lDataSize As Long
        Dim oUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim sIdi As String = oUser.Idioma.ToString()
        If sIdi = Nothing Then sIdi = ConfigurationManager.AppSettings("idioma")
        Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.Adjuntos, sIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        'Recogemos el QueryString
        If Request("tipo") <> "" Then
            iTipo = Request("tipo")
        End If
        If Request("campo") <> "" Then
            idCampo = Request("campo")
        End If
        If Request("instancia") <> "" Then
            Instancia = Request("instancia")
        End If
        If Request("adjunto") <> "" Then
            idAdjuntos = Request("adjunto")
        End If
        If Request("adjuntoNew") <> "" Then
            idAdjuntosNew = Request("adjuntoNew")
        End If
        If Request("nombre") <> "" Then
            NombreAdjunto = Request("nombre")
        End If
        If Request("EntryID") <> "" Then
            EntryID = Request("EntryID")
        End If
        If Request("readOnly") <> "" Then
            SoloLectura = Request("readOnly")
        End If
        If Request("origen") <> "" Then
            Origen = Request("origen")
        End If
        If Request("datasize") <> "" Then
            lDataSize = Request("datasize")
        End If
        If Request("IdContrato") <> "" And Request("IdContrato") <> "0" Then
            bConContrato = True
            idContrato = CLng(Request("IdContrato"))
        End If
        If idAdjuntos <> "" Then
            idAdjuntos = Replace(idAdjuntos, "xx", ",")
        End If
        If idAdjuntosNew <> "" Then
            idAdjuntosNew = Replace(idAdjuntosNew, "xx", ",")
        End If

        idArchivoContrato = CLng(Request("IdArchivoContrato"))

        Dim oAdjuntos As FSNServer.Adjuntos
        oAdjuntos = FSWSServer.Get_Object(GetType(FSNServer.Adjuntos))

        'Obtenemos lo adjuntos
        If Not Request("Borrando") Is Nothing Then
            oAdjuntos.LoadInst_Adj_Contrato(0, 0, idCampo)
        Else
            'Hemos eliminado el adjunto de Contrato y no recuperara ningun adjunto
            oAdjuntos.LoadInst_Adj_Contrato(idContrato, idArchivoContrato, idCampo)
        End If

        oTblInput = New System.Web.UI.HtmlControls.HtmlTable
        oTblRowInput = New System.Web.UI.HtmlControls.HtmlTableRow
        oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell

        oTblInput.Width = Unit.Percentage(100).ToString
        oTblInput.CellPadding = 0
        oTblInput.CellSpacing = 0
        oTblInput.Border = 0
        oTblInput.Style.Item("display") = "inline"

        If oAdjuntos.Data.Tables.Count = 0 OrElse oAdjuntos.Data.Tables(0).Rows.Count = 0 Then
            'SI NO TIENE ADJUNTOS
            Dim sNombreAdjuntoContrato As String = ""
            oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell
            If SoloLectura.ToLower = "false" Then
                Dim Enlace As New HtmlControls.HtmlAnchor
                Enlace.InnerText = oTextos.Rows(1).Item(1)
                Enlace.Attributes("onclick") = "show_atach_file_Contrato('" + Instancia.ToString() + "', 0,'" + EntryID + "','" & Origen & "','" & Request("IdContrato") & "','')"
                Enlace.Attributes.Add("class", "enlaceAdj")
                Enlace.Style("text-decoration") = "underline"
                Enlace.Style("color") = "#0000FF"
                oTblCellInput.Controls.Add(Enlace)
                oTblCellInput.Align = "right"
            End If
            'oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell
            Dim oLink As System.Web.UI.HtmlControls.HtmlAnchor
            oLink = New System.Web.UI.HtmlControls.HtmlAnchor
            oLink.ID = EntryID + "_t"
            oLink.Style.Item("display") = "none"
            oLink.HRef = "#"
            oLink.Attributes.Add("class", "enlaceAdj")
            hidNomAdjuntos.ID = EntryID & "__hidNombresAdj"
            hidNomAdjuntos.Value = sNombreAdjuntoContrato
            oTblCellInput.Controls.Add(oLink)
            oTblCellInput.Controls.Add(hidNomAdjuntos)
            oTblRowInput.Cells.Add(oTblCellInput)
            oTblInput.Rows.Add(oTblRowInput)
        Else
            Dim sNombreAdjuntoContrato As String = ""

            'SI TIENE ADJUNTOS
            oTblOpciones = New System.Web.UI.HtmlControls.HtmlTable
            oTblRowOpciones = New System.Web.UI.HtmlControls.HtmlTableRow
            oTblCellOpciones = New System.Web.UI.HtmlControls.HtmlTableCell

            oTblOpciones.CellPadding = 3
            oTblOpciones.CellSpacing = 0
            oTblOpciones.Border = 0
            oTblOpciones.Align = "right"
            oTblOpciones.Style.Item("display") = "inline"

            oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell

            Dim oLink As System.Web.UI.HtmlControls.HtmlAnchor
            oLink = New System.Web.UI.HtmlControls.HtmlAnchor
            oLink.ID = EntryID + "_t"
            oLink.Style.Item("display") = "none"
            oLink.HRef = "#"
            oLink.Attributes.Add("class", "enlaceAdj")
            oTblCellOpciones.Controls.Add(oLink)

            oTblRowOpciones.Cells.Add(oTblCellOpciones)

            If oAdjuntos.Data.Tables(0).Rows.Count > 1 Then
                'Si tiene mas de un adjunto
                'Descargar Todos
                oTblCellOpciones = New System.Web.UI.HtmlControls.HtmlTableCell
                Dim EnlaceDescargarTodos As New HtmlControls.HtmlAnchor
                EnlaceDescargarTodos.InnerText = oTextos.Rows(4).Item(1)
                EnlaceDescargarTodos.Style("text-decoration") = "underline"
                EnlaceDescargarTodos.Style("color") = "#0000FF"
                EnlaceDescargarTodos.Attributes.Add("class", "enlaceAdj")
                Dim tipos As String = ""
                For i = 0 To oAdjuntos.Data.Tables(0).Rows.Count - 1
                    If tipos = "" Then
                        tipos = oAdjuntos.Data.Tables(0).Rows(i)("id").ToString & ",5"
                    Else
                        tipos = tipos & "xx" & oAdjuntos.Data.Tables(0).Rows(i)("id").ToString & ",5"
                    End If
                Next
                EnlaceDescargarTodos.Attributes("onclick") = "descargarAdjuntos('" + idAdjuntos.ToString + "', '" + idAdjuntosNew.ToString + "', '" & Instancia.ToString & "', '" & tipos.ToString & "','" & EntryID & "')"
                oTblCellOpciones.Controls.Add(EnlaceDescargarTodos)
                'oTblCellInput.Align = "right"
                oTblRowOpciones.Cells.Add(oTblCellOpciones)

                If SoloLectura.ToLower = "false" Then
                    'Eliminar Todos
                    oTblCellOpciones = New System.Web.UI.HtmlControls.HtmlTableCell
                    Dim EnlaceEliminarTodos As New HtmlControls.HtmlAnchor
                    EnlaceEliminarTodos.InnerText = oTextos.Rows(6).Item(1)
                    EnlaceEliminarTodos.Style("text-decoration") = "underline"
                    EnlaceEliminarTodos.Attributes.Add("class", "enlaceAdj")
                    EnlaceEliminarTodos.Style("color") = "#0000FF"
                    EnlaceEliminarTodos.Attributes("onclick") = "eliminarAdjuntos('" + EntryID + "'," & IIf(bConContrato, 1, 0) & ")"
                    oTblCellOpciones.Controls.Add(EnlaceEliminarTodos)
                    oTblCellOpciones.Align = "right"
                    oTblRowOpciones.Cells.Add(oTblCellOpciones)
                End If
            End If

            oTblOpciones.Rows.Add(oTblRowOpciones)
            oTblCellInput.Controls.Add(oTblOpciones)
            oTblRowInput.Cells.Add(oTblCellInput)
            oTblInput.Rows.Add(oTblRowInput)

            'Cargo los adjuntos
            For i = 0 To oAdjuntos.Data.Tables(0).Rows.Count - 1
                Dim filaContenedora As New HtmlControls.HtmlTableRow
                Dim celdaContenedora As New HtmlControls.HtmlTableCell
                Dim tablaContenido As New HtmlControls.HtmlTable
                Dim filaContenido As HtmlControls.HtmlTableRow
                Dim celdaContenido As HtmlControls.HtmlTableCell
                Dim EnlaceFichero As New HtmlControls.HtmlAnchor
                Dim lblPersona As New Label
                Dim imgEliminar As New HtmlControls.HtmlImage
                Dim imgSustituir As New HtmlControls.HtmlImage
                Dim txtComentarios As New TextBox
                Dim txtComentariosReadOnly As New HtmlControls.HtmlTextArea
                Dim hidComentario As New HtmlControls.HtmlInputHidden
                Dim hidAdjMemJustificativa As New HtmlControls.HtmlInputHidden
                Dim oAdjunto As FSNServer.Adjunto

                oAdjunto = FSWSServer.Get_Object(GetType(FSNServer.Adjunto))
                oAdjunto.Id = oAdjuntos.Data.Tables(0).Rows(i)("ID")

                tablaContenido.Style.Add("table-layout", "fixed")

                filaContenido = New HtmlControls.HtmlTableRow
                celdaContenido = New HtmlControls.HtmlTableCell
                celdaContenido.Width = "30%"
                celdaContenido.Attributes.Add("max-width", "30%")
                filaContenido.Cells.Add(celdaContenido)
                celdaContenido = New HtmlControls.HtmlTableCell
                celdaContenido.Width = Unit.Percentage(49).ToString
                filaContenido.Cells.Add(celdaContenido)
                If SoloLectura.ToLower = "false" Then
                    celdaContenido = New HtmlControls.HtmlTableCell
                    celdaContenido.Width = Unit.Percentage(4).ToString
                    filaContenido.Cells.Add(celdaContenido)
                    celdaContenido = New HtmlControls.HtmlTableCell
                    celdaContenido.Width = Unit.Percentage(4).ToString
                    filaContenido.Cells.Add(celdaContenido)
                End If
                tablaContenido.Rows.Add(filaContenido)

                filaContenido = New HtmlControls.HtmlTableRow
                'Enlace fichero
                EnlaceFichero.InnerText = oAdjuntos.Data.Tables(0).Rows(i)("NOM") '& " (" & oAdjuntos.Data.Tables(0).Rows(i)("DATASIZE") & " kb)"
                sNombreAdjuntos = IIf(sNombreAdjuntos = "", oAdjuntos.Data.Tables(0).Rows(i)("NOM"), sNombreAdjuntos & ", " & oAdjuntos.Data.Tables(0).Rows(i)("NOM"))
                EnlaceFichero.ID = oAdjuntos.Data.Tables(0).Rows(i)("ID")
                EnlaceFichero.Attributes("onclick") = "descargarAdjuntoContrato(" + Instancia.ToString + ", " + DBNullToInteger(oAdjuntos.Data.Tables(0).Rows(i)("ID_CONTRATO")).ToString + ", " + DBNullToInteger(oAdjuntos.Data.Tables(0).Rows(i)("ID")).ToString + ", '" + DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("NOM")) + "'," + DBNullToInteger(lDataSize).ToString + ")"
                EnlaceFichero.Style("text-decoration") = "underline"
                EnlaceFichero.Attributes.Add("class", "enlaceAdj")
                EnlaceFichero.Style("color") = "#0000FF"
                celdaContenido = New HtmlControls.HtmlTableCell
                celdaContenido.Width = "30%"
                celdaContenido.Attributes.Add("word-wrap", "break-word")
                'Input Hidden con el ID y tipo del adjunto y la instancia
                Dim inputID As New HtmlControls.HtmlInputHidden
                inputID.ID = "Adj_" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                inputID.Value = oAdjuntos.Data.Tables(0).Rows(i)("ID") & "###5###" & Instancia.ToString
                celdaContenido.Controls.Add(EnlaceFichero)
                celdaContenido.Controls.Add(inputID)

                filaContenido.Cells.Add(celdaContenido)

                'Persona que adjunto el documento
                celdaContenido = New HtmlControls.HtmlTableCell
                celdaContenido.Width = Unit.Percentage(49).ToString
                lblPersona.Text = oAdjuntos.Data.Tables(0).Rows(i)("NOMBRE") & "( " & oAdjuntos.Data.Tables(0).Rows(i)("FECALTA") & " )"
                lblPersona.CssClass = "fntLogin"
                celdaContenido.Controls.Add(lblPersona)
                filaContenido.Cells.Add(celdaContenido)

                'Eliminar adjunto
                If SoloLectura.ToLower = "false" Then
                    celdaContenido = New HtmlControls.HtmlTableCell
                    celdaContenido.Width = Unit.Percentage(4).ToString
                    imgEliminar.Alt = oTextos.Rows(0).Item(1)
                    imgEliminar.ID = "Eliminar_" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                    imgEliminar.Src = ConfigurationManager.AppSettings("ruta") & "images/eliminar.gif"
                    imgEliminar.Attributes("onclick") = "borrarAdjuntoContrato('" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "', '5', '" & Instancia.ToString & "','" & EntryID.ToString & "','" & Origen & "','" & IIf(bConContrato, Request("IdContrato"), 0) & "')"
                    celdaContenido.Controls.Add(imgEliminar)
                    celdaContenido.Align = "right"
                    filaContenido.Cells.Add(celdaContenido)
                End If

                'Sustituir adjunto
                If SoloLectura.ToLower = "false" Then
                    celdaContenido = New HtmlControls.HtmlTableCell
                    celdaContenido.Width = Unit.Percentage(4).ToString
                    imgEliminar.ID = "Sustituir_" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                    imgSustituir.Alt = oTextos.Rows(2).Item(1)
                    imgSustituir.Attributes("onclick") = "sustituirAdjuntoContrato('" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "', '5', '" & Instancia.ToString & "','" & EntryID.ToString & "','" & Origen & "', '" + Replace(Server.UrlEncode(oAdjuntos.Data.Tables(0).Rows(i)("NOM").ToString()), "'", "\'") + "')"
                    imgSustituir.Src = ConfigurationManager.AppSettings("rutaPM") & "_common/images/sustituir.gif"
                    celdaContenido.Controls.Add(imgSustituir)
                    celdaContenido.Align = "right"
                    filaContenido.Cells.Add(celdaContenido)
                End If

                tablaContenido.Rows.Add(filaContenido)

                'Añado el textArea para los comentarios
                filaContenido = New HtmlControls.HtmlTableRow

                hidComentario.ID = "__hidComent" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                hidComentario.Value = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"))
                If SoloLectura.ToLower = "false" Then
                    txtComentarios.ID = EntryID & "__text" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                    txtComentarios.TextMode = TextBoxMode.MultiLine
                    txtComentarios.Height = Unit.Pixel(33)
                    txtComentarios.MaxLength = 500
                    txtComentarios.Text = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"))
                    txtComentarios.Style("overflow") = "hidden"
                    txtComentarios.Attributes("onfocus") = "aumentarTamanoCajaTexto('" + EntryID.ToString + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "',0)"
                    txtComentarios.Attributes("onfocusout") = "reducirTamanoCajaTextoContrato('" + EntryID.ToString + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "', '1','" + Instancia.ToString + "',1,0," + idArchivoContrato.ToString + ")"
                    txtComentarios.Attributes("onchange") = "return validarLengthyCortarStringLargo(this,500,'" & oTextos.Rows(9).Item(1) & "'); "
                    txtComentarios.Columns = 100
                    celdaContenido = New HtmlControls.HtmlTableCell
                    celdaContenido.Attributes("colspan") = "4"
                    celdaContenido.Width = "100%"
                    celdaContenido.Align = "left"
                    celdaContenido.Controls.Add(txtComentarios)
                    celdaContenido.Controls.Add(hidComentario)
                    filaContenido.Cells.Add(celdaContenido)
                    tablaContenido.Rows.Add(filaContenido)
                Else
                    If oAdjuntos.Data.Tables(0).Rows(i)("COMENT").ToString.Trim <> "" Then
                        Dim bAjustarTamanoCajaTexto As Boolean
                        Dim posicion As Integer
                        txtComentariosReadOnly.ID = "__text" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                        txtComentariosReadOnly.Cols = 55
                        txtComentariosReadOnly.Rows = 2
                        posicion = InStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"), Chr(13) & Chr(10))
                        If posicion > 0 Then
                            posicion = InStr(posicion + 2, oAdjuntos.Data.Tables(0).Rows(i)("COMENT"), Chr(13) & Chr(10))
                            If posicion > 0 Then
                                txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")).Substring(0, posicion - 1) & " ...."
                                bAjustarTamanoCajaTexto = True
                            Else
                                If Len(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")) > 110 Then
                                    bAjustarTamanoCajaTexto = True
                                    txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")).Substring(0, 90) & " ...."
                                    posicion = 90
                                Else
                                    txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"))
                                End If
                            End If
                        Else
                            If Len(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")) > 110 Then
                                bAjustarTamanoCajaTexto = True
                                txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")).Substring(0, 90) & " ...."
                                posicion = 90
                            Else
                                txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"))
                            End If
                        End If

                        txtComentariosReadOnly.Style("border") = "none"
                        txtComentariosReadOnly.Style("width") = "90%"
                        txtComentariosReadOnly.Style("overflow") = "hidden"
                        txtComentariosReadOnly.Style("background-color") = "Transparent"
                        If bAjustarTamanoCajaTexto Then
                            txtComentariosReadOnly.Attributes("onmouseover") = "aumentarTamanoCajaTexto('" + EntryID.ToString + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "',1)"
                            txtComentariosReadOnly.Attributes("onblur") = "reducirTamanoCajaTextoContrato('" + EntryID.ToString + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "', '1','" + Instancia.ToString + "',0,'" + (posicion - 1).ToString + "'," + idArchivoContrato.ToString + ")"
                        End If
                        txtComentariosReadOnly.Attributes("readonly") = "true"
                        celdaContenido = New HtmlControls.HtmlTableCell
                        celdaContenido.Attributes("colspan") = "4"
                        celdaContenido.Width = "100%"
                        celdaContenido.Align = "center"
                        celdaContenido.Controls.Add(txtComentariosReadOnly)
                        celdaContenido.Controls.Add(hidComentario)
                        filaContenido.Cells.Add(celdaContenido)
                        tablaContenido.Rows.Add(filaContenido)
                    End If
                End If

                'Añado la tabla con el adjunto a la tabla global
                celdaContenedora.ColSpan = 3
                celdaContenedora.Controls.Add(tablaContenido)
                'Meto el hidden con los nombre de todos los adjuntos
                If i = oAdjuntos.Data.Tables(0).Rows.Count - 1 Then
                    hidNomAdjuntos.ID = EntryID & "__hidNombresAdj"
                    hidNomAdjuntos.Value = sNombreAdjuntos
                    celdaContenido.Controls.Add(hidNomAdjuntos)
                End If
                filaContenedora.Cells.Add(celdaContenedora)
                oTblInput.Rows.Add(filaContenedora)
            Next
        End If

        Dim outHTML As String
        outHTML = RenderizarControl(oTblInput)

        Dim responde As HttpResponse = HttpContext.Current.Response
        Response.Clear()
        Response.ContentType = "text/plain"

        Response.Write(outHTML)
        Response.End()
    End Sub
    ''' <summary>
    ''' Crea el control con los adjuntos que estan dentro de un desglose en la pantalla de detalle
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub AñadirAdjuntoDesgloseDetalle()
        Dim oTblInput As New System.Web.UI.WebControls.Table
        Dim idAdjuntos As String = String.Empty
        Dim idAdjuntosNew As String = String.Empty
        Dim NombreAdjunto As String = String.Empty
        Dim Instancia As Long
        Dim bFavorito As Boolean
        Dim iTipo As Integer
        Dim EntryID As String = Nothing
        Dim SoloLectura As String = Nothing
        Dim Origen As String = Nothing
        Dim sDataDefecto As String = Nothing
        Dim oUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim sIdi As String = oUser.Idioma.ToString()
        If sIdi = Nothing Then sIdi = ConfigurationManager.AppSettings("idioma")
        Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.Adjuntos, sIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        Try
            'Recogemos el QueryString
            If Request("defecto") <> "" Then
                sDataDefecto = Request("defecto")
            End If
            If Request("tipo") <> "" Then
                iTipo = Request("tipo")
            End If
            If Request("instancia") <> "" Then
                Instancia = Request("instancia")
            End If
            If Request("adjunto") <> "" Then
                idAdjuntos = Request("adjunto")
            End If
            If Request("adjuntoNew") <> "" Then
                idAdjuntosNew = Request("adjuntoNew")
            End If

            If Request("EntryID") <> "" Then
                EntryID = Request("EntryID")
            End If
            If Request("readOnly") <> "" Then
                SoloLectura = Request("readOnly")
            End If
            If Request("origen") <> "" Then
                Origen = Request("origen")
            End If
            If idAdjuntos <> "" Then
                idAdjuntos = Replace(idAdjuntos, "xx", ",")
            End If
            If idAdjuntosNew <> "" Then
                idAdjuntosNew = Replace(idAdjuntosNew, "xx", ",")
            End If

            Dim oAdjuntos As FSNServer.Adjuntos
            oAdjuntos = FSWSServer.Get_Object(GetType(FSNServer.Adjuntos))

            'Obtenemos lo adjuntos
            If Instancia = 0 Then
                oAdjuntos.Load(iTipo, idAdjuntos, idAdjuntosNew, bFavorito)
            Else
                oAdjuntos.LoadInst(iTipo, idAdjuntos, idAdjuntosNew)
                'Si es una adjunto metido desde Añadir por valor por defecto. La información
                'esta en LINEA_DESGLOSE_ADJUN
                Dim sPatron As String = sDataDefecto
                Dim sPatron1 As String = sDataDefecto & ",*"
                Dim sPatron2 As String = "*," & sDataDefecto & ",*"
                Dim sPatron3 As String = "*," & sDataDefecto
                If idAdjuntos Like sPatron Or idAdjuntos Like sPatron1 Or idAdjuntos Like sPatron2 Or idAdjuntos Like sPatron3 Then
                    oAdjuntos.Load(iTipo, idAdjuntos, idAdjuntosNew)
                End If
            End If

            Dim tablaContenido As New HtmlControls.HtmlTable
            Dim filaContenido As HtmlControls.HtmlTableRow
            Dim celdaContenido As HtmlControls.HtmlTableCell
            tablaContenido.Style("width") = "100%"
            tablaContenido.CellSpacing = 0
            tablaContenido.CellPadding = 3

            'Cargo los adjuntos
            For i = 0 To oAdjuntos.Data.Tables(0).Rows.Count - 1
                Dim EnlaceFichero As New HtmlControls.HtmlAnchor
                Dim lblPersona As New Label
                Dim lblFechaAdjunto As New Label
                Dim imgEliminar As New HtmlControls.HtmlImage
                Dim imgSustituir As New HtmlControls.HtmlImage
                Dim txtComentarios As New TextBox
                Dim txtComentariosReadOnly As New HtmlControls.HtmlTextArea
                Dim DivComentarios As New HtmlControls.HtmlGenericControl("DIV")
                Dim imgPreviaAdjun As New Image
                Dim hidComentario As New HtmlControls.HtmlInputHidden

                Try
                    Dim sClaseFila As String = "ugfilatablaImPar"
                    If i Mod 2 = 0 Then
                        sClaseFila = "ugfilatablaPar"
                    End If

                    filaContenido = New HtmlControls.HtmlTableRow
                    filaContenido.Style("width") = "100%"
                    filaContenido.Attributes.Add("class", sClaseFila)

                    'Enlace fichero
                    EnlaceFichero.InnerText = oAdjuntos.Data.Tables(0).Rows(i)("NOM") & " (" & oAdjuntos.Data.Tables(0).Rows(i)("DATASIZE") & " kb)"
                    EnlaceFichero.ID = oAdjuntos.Data.Tables(0).Rows(i)("ID")
                    EnlaceFichero.Attributes("onclick") = "descargarAdjunto('" & oAdjuntos.Data.Tables(0).Rows(i)("id") & "', '" & oAdjuntos.Data.Tables(0).Rows(i)("tipo") & "', '" & Instancia.ToString & "', '" & EntryID & "')"
                    EnlaceFichero.Style("text-decoration") = "underline"
                    'EnlaceFichero.Attributes.Add("class", "enlaceAdj")
                    EnlaceFichero.HRef = "#"
                    EnlaceFichero.Style("color") = "#0000FF"
                    celdaContenido = New HtmlControls.HtmlTableCell
                    celdaContenido.Controls.Add(EnlaceFichero)
                    celdaContenido.Width = Unit.Percentage(45).ToString
                    filaContenido.Cells.Add(celdaContenido)

                    'Input Hidden con el ID y tipo del adjunto y la instancia
                    Dim inputID As New HtmlControls.HtmlInputHidden
                    inputID.ID = "Adj_" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                    inputID.Value = oAdjuntos.Data.Tables(0).Rows(i)("ID") & "###" & oAdjuntos.Data.Tables(0).Rows(i)("tipo") & "###" & Instancia.ToString
                    celdaContenido.Controls.Add(EnlaceFichero)
                    celdaContenido.Controls.Add(inputID)
                    filaContenido.Cells.Add(celdaContenido)

                    'Fecha en la que se adjunto el documento
                    celdaContenido = New HtmlControls.HtmlTableCell
                    celdaContenido.Width = Unit.Percentage(20).ToString
                    lblFechaAdjunto.Text = oAdjuntos.Data.Tables(0).Rows(i)("FECALTA").ToString
                    lblFechaAdjunto.CssClass = "fntLogin"
                    celdaContenido.Controls.Add(lblFechaAdjunto)
                    filaContenido.Cells.Add(celdaContenido)

                    'Persona que adjunto el documento
                    celdaContenido = New HtmlControls.HtmlTableCell
                    celdaContenido.Width = Unit.Percentage(25).ToString
                    lblPersona.Text = oAdjuntos.Data.Tables(0).Rows(i)("NOMBRE").ToString
                    lblPersona.CssClass = "fntLogin"
                    celdaContenido.Controls.Add(lblPersona)
                    filaContenido.Cells.Add(celdaContenido)

                    'Eliminar adjunto
                    If Not SoloLectura.ToLower = "true" Then
                        celdaContenido = New HtmlControls.HtmlTableCell
                        celdaContenido.Width = Unit.Percentage(3).ToString
                        celdaContenido.Align = "right"
                        imgEliminar.Alt = oTextos.Rows(0).Item(1)
                        imgEliminar.ID = "Eliminar_" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                        imgEliminar.Src = ConfigurationManager.AppSettings("ruta") & "images/eliminar.gif"
                        imgEliminar.Attributes("onclick") = "javascript:p=window.opener;p.borrarAdjunto('" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "', '" & oAdjuntos.Data.Tables(0).Rows(i)("tipo").ToString & "', '" & Instancia.ToString & "','" & EntryID & "','" & Origen & "'); borrarAdjuntoDetalle('" + EntryID + "', '" + SoloLectura + "','" & Origen & "')"
                        celdaContenido.Controls.Add(imgEliminar)
                        filaContenido.Cells.Add(celdaContenido)
                    End If

                    'Sustituir adjunto
                    If Not SoloLectura.ToLower = "true" Then
                        celdaContenido = New HtmlControls.HtmlTableCell
                        celdaContenido.Width = Unit.Percentage(3).ToString
                        celdaContenido.Align = "right"
                        imgEliminar.ID = "Sustituir_" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                        imgSustituir.Alt = oTextos.Rows(2).Item(1)
                        imgSustituir.Attributes("onclick") = "javascript:sustituirAdjuntoDetalle('" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "', '" & oAdjuntos.Data.Tables(0).Rows(i)("tipo").ToString & "', '" & Instancia.ToString & "','" & EntryID & "','" & Origen & "','" & Replace(Server.UrlEncode(oAdjuntos.Data.Tables(0).Rows(i)("NOM").ToString()), "'", "\'") & "')"
                        imgSustituir.Src = ConfigurationManager.AppSettings("rutaPM") & "_common/images/sustituir.gif"
                        celdaContenido.Controls.Add(imgSustituir)
                        filaContenido.Cells.Add(celdaContenido)
                    End If

                    tablaContenido.Rows.Add(filaContenido)

                    'Añado el textArea para los comentarios
                    filaContenido = New HtmlControls.HtmlTableRow
                    filaContenido.Attributes.Add("class", sClaseFila)

                    hidComentario.ID = "__hidComent" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                    hidComentario.Value = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"))
                    If Not SoloLectura.ToLower = "true" Then
                        txtComentarios.ID = EntryID & "__text" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                        txtComentarios.TextMode = TextBoxMode.MultiLine
                        txtComentarios.Height = Unit.Pixel(33)
                        txtComentarios.MaxLength = 500
                        txtComentarios.Style("overflow") = "hidden"
                        txtComentarios.Text = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"))
                        txtComentarios.Attributes("onfocus") = "aumentarTamanoCajaTexto('" + EntryID + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "',0)"
                        txtComentarios.Attributes("onfocusout") = "reducirTamanoCajaTexto('" + EntryID + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("TIPO").ToString + "','" + Instancia.ToString + "',1,0)"
                        txtComentarios.Width = "600"
                        celdaContenido = New HtmlControls.HtmlTableCell
                        celdaContenido.Attributes("colspan") = "5"
                        celdaContenido.Width = Unit.Pixel(700).ToString
                        celdaContenido.Style("align") = "center"
                        celdaContenido.Controls.Add(txtComentarios)
                        celdaContenido.Controls.Add(hidComentario)
                        filaContenido.Cells.Add(celdaContenido)
                        tablaContenido.Rows.Add(filaContenido)
                    Else
                        If oAdjuntos.Data.Tables(0).Rows(i)("COMENT").ToString.Trim <> "" Then
                            Dim bAjustarTamanoCajaTexto As Boolean
                            Dim posicion As Integer
                            txtComentariosReadOnly.ID = EntryID & "__text" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                            txtComentariosReadOnly.Cols = 55
                            txtComentariosReadOnly.Rows = 2
                            posicion = InStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"), Chr(13) & Chr(10))
                            If posicion > 0 Then
                                posicion = InStr(posicion + 2, oAdjuntos.Data.Tables(0).Rows(i)("COMENT"), Chr(13) & Chr(10))
                                If posicion > 0 Then
                                    txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")).Substring(0, posicion - 1) & " ...."
                                    bAjustarTamanoCajaTexto = True
                                Else
                                    If Len(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")) > 110 Then
                                        bAjustarTamanoCajaTexto = True
                                        txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")).Substring(0, 90) & " ...."
                                        posicion = 90
                                    Else
                                        txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"))
                                    End If
                                End If
                            Else
                                If Len(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")) > 110 Then
                                    bAjustarTamanoCajaTexto = True
                                    txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")).Substring(0, 90) & " ...."
                                    posicion = 90
                                Else
                                    txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"))
                                End If
                            End If

                            txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"))
                            txtComentariosReadOnly.Style("border") = "none"
                            txtComentariosReadOnly.Style("width") = "600px"
                            txtComentariosReadOnly.Style("overflow") = "hidden"
                            txtComentariosReadOnly.Style("background-color") = "Transparent"
                            If bAjustarTamanoCajaTexto Then
                                txtComentariosReadOnly.Attributes("onmouseover") = "aumentarTamanoCajaTexto('" + EntryID + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "',1)"
                                txtComentariosReadOnly.Attributes("onblur") = "reducirTamanoCajaTexto('" + EntryID + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("TIPO").ToString + "','" + Instancia.ToString + "',0,'" + (posicion - 1).ToString + "')"
                            End If
                            txtComentariosReadOnly.Attributes("readonly") = "true"
                            txtComentariosReadOnly.Attributes("background-color") = "Transparent"
                            celdaContenido = New HtmlControls.HtmlTableCell
                            celdaContenido.Attributes("colspan") = "3"
                            celdaContenido.Width = Unit.Pixel(700).ToString
                            celdaContenido.Style("align") = "center"
                            celdaContenido.Controls.Add(txtComentariosReadOnly)
                            celdaContenido.Controls.Add(hidComentario)
                            filaContenido.Cells.Add(celdaContenido)
                            tablaContenido.Rows.Add(filaContenido)
                        End If
                    End If
                Catch ex As Exception

                End Try
            Next

            If Not SoloLectura.ToLower = "true" Then
                Dim butAñadir As New HtmlControls.HtmlInputButton

                butAñadir.Value = oTextos.Rows(7).Item(1)
                butAñadir.Attributes.Add("class", "botonPMWEB")
                butAñadir.Style("margin-top") = "10px"
                butAñadir.Attributes("onclick") = "show_atach_file('" + Instancia.ToString() + "', '" + idAdjuntos.ToString + "', '" + iTipo.ToString + "', '" + EntryID + "','" + Origen + "', 0)"

                filaContenido = New HtmlControls.HtmlTableRow
                celdaContenido = New HtmlControls.HtmlTableCell
                celdaContenido.Controls.Add(butAñadir)
                filaContenido.Cells.Add(celdaContenido)
                tablaContenido.Rows.Add(filaContenido)
            End If

            Dim outHTML As String
            outHTML = RenderizarControl(tablaContenido)

            Dim responde As HttpResponse = HttpContext.Current.Response
            Response.Clear()
            Response.ContentType = "text/plain"

            Response.Write(outHTML)
            Response.End()
        Catch ex As Exception

        End Try
    End Sub
    ''' <summary>
    ''' Guarda el comentario del adjunto
    ''' </summary>
    ''' <param name="idAdjunto">id del adjunto cuyo comentario se guardara</param>
    ''' <param name="tipo">tipo del adjunto</param>
    ''' <param name="Comentario">Comentario a grabar</param>
    ''' <param name="Intancia">Instancia del adjunto</param>
    ''' <remarks></remarks>
    Private Sub GuardarComentario(ByVal idAdjunto As String, ByVal tipo As String, ByVal Comentario As String, ByVal Intancia As Integer)
        Dim oAdjunto As FSNServer.Adjunto
        oAdjunto = Me.FSNServer.Get_Object(GetType(FSNServer.Adjunto))
        oAdjunto.GuardarComentarioAdjunto(CInt(idAdjunto), CInt(tipo), Comentario, Intancia)
    End Sub
    ''' <summary>
    ''' Devuelve la salida HTML del control
    ''' </summary>
    ''' <param name="ctrl">control que se pasa para obtener como salida el codigo HTML</param>
    ''' <returns>el codigo HTML del control</returns>
    ''' <remarks></remarks>
    Public Function RenderizarControl(ByVal ctrl As Control) As String
        Dim sb As New StringBuilder
        Dim tw As New System.IO.StringWriter(sb)
        Dim hw As New HtmlTextWriter(tw)

        ctrl.RenderControl(hw)
        Return sb.ToString()
    End Function
    ''' <summary>
    ''' Funcion que realiza la aprobación o el rechazo de una instancia, la llamada se realiza desde el detalle popup
    ''' </summary>
    ''' <param name="lAccion">id de la accion a realizar</param>
    ''' <param name="lBloque">id del bloque</param>
    ''' <param name="lInstancia">id de la instancia</param>
    ''' <param name="sComentario">Comentario</param>
    ''' <remarks></remarks>
    Private Sub AprobarRechazarInstancia(ByVal lAccion As Long, ByVal lBloque As Long, ByVal lInstancia As Long, ByVal sComentario As String)
        Dim bProcesar As Boolean
        Dim Instancias As FSNServer.Instancias
        Dim oInstancia As FSNServer.Instancia
        Instancias = FSNServer.Get_Object(GetType(FSNServer.Instancias))
        bProcesar = Instancias.ComprobarSiEstaProcesada(lInstancia, lBloque)
        If Not bProcesar Then
            Instancias.Actualizar_En_Proceso2(lInstancia, Usuario.CodPersona)

            Dim dsXML As New DataSet
            dsXML.Tables.Add("SOLICITUD")
            Dim drSolicitud As DataRow
            With dsXML.Tables("SOLICITUD").Columns
                .Add("TIPO_PROCESAMIENTO_XML")
                .Add("TIPO_DE_SOLICITUD")
                .Add("COMPLETO")
                .Add("CODIGOUSUARIO")
                .Add("INSTANCIA")
                .Add("USUARIO")
                .Add("USUARIO_EMAIL")
                .Add("USUARIO_IDIOMA")
                .Add("ACCION")
                .Add("IDACCION")
                .Add("COMENTARIO")
                .Add("BLOQUE_ORIGEN")
                .Add("THOUSANFMT")
                .Add("DECIAMLFMT")
                .Add("PRECISIONFMT")
                .Add("DATEFMT")
                .Add("REFCULTURAL")
                .Add("TIPOEMAIL")
                .Add("IDTIEMPOPROC")
            End With

            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
            Dim lIDTiempoProc As Long
            oInstancia.ID = lInstancia
            oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, FSNUser.Cod, lBloque)
            drSolicitud = dsXML.Tables("SOLICITUD").NewRow
            With drSolicitud
                .Item("TIPO_PROCESAMIENTO_XML") = CInt(TipoProcesamientoXML.FSNWeb_PM_AprobacionRechazoMultiple)
                .Item("TIPO_DE_SOLICITUD") = CInt(TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras)
                .Item("COMPLETO") = 1
                .Item("CODIGOUSUARIO") = FSNUser.Cod
                .Item("INSTANCIA") = lInstancia
                .Item("USUARIO") = FSNUser.CodPersona
                .Item("USUARIO_EMAIL") = FSNUser.Email
                .Item("USUARIO_IDIOMA") = FSNUser.IdiomaCod
                .Item("ACCION") = ""
                .Item("IDACCION") = lAccion
                .Item("COMENTARIO") = sComentario
                .Item("BLOQUE_ORIGEN") = lBloque
                .Item("THOUSANFMT") = FSNUser.ThousanFmt
                .Item("DECIAMLFMT") = FSNUser.DecimalFmt
                .Item("PRECISIONFMT") = FSNUser.PrecisionFmt
                .Item("DATEFMT") = FSNUser.DateFmt
                .Item("REFCULTURAL") = FSNUser.Idioma.RefCultural
                .Item("TIPOEMAIL") = FSNUser.TipoEmail
                .Item("IDTIEMPOPROC") = lIDTiempoProc
            End With
            dsXML.Tables("SOLICITUD").Rows.Add(drSolicitud)

            Dim xmlName As String = FSNUser.Cod & "#" & lInstancia & "#" & lBloque
            oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=TiempoProcesamiento.InicioDisco)
            If ConfigurationManager.AppSettings("SERVIDOR_TRATAMIENTO_INSTANCIAS") = "0" Then
                'Se hace con un StringWriter porque el método GetXml del dataset no incluye el esquema
                Dim oSW As New System.IO.StringWriter()
                dsXML.WriteXml(oSW, XmlWriteMode.WriteSchema)

                Dim oSrvTrataInst As New FSNWebServiceXML.TratamientoInstancias
                oSrvTrataInst.TransferirXMLEnEpera(oSW.ToString(), xmlName)
            Else
                dsXML.WriteXml(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#FSNWEB.xml", XmlWriteMode.WriteSchema)
                If File.Exists(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml") Then _
                    File.Delete(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
                FileSystem.Rename(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#FSNWEB.xml",
                                  ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
            End If
        End If
    End Sub
    ''' <summary>
    ''' Procedimiento que guarda en una variable de sesion el comentario que se guardar en el detalle popup para que sea accesible desde PmWeb2008
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GuardarComentarioInstancia(ByVal lInstancia As Long, ByVal sComentario As String)
        Session("COMENT" & lInstancia) = sComentario
        If Session("sSession") <> Nothing AndAlso Session("COMENT" & lInstancia.ToString) <> Nothing Then 'para pasar el comentario a PMWeb2008
            Session("COMENT" & lInstancia.ToString) = sComentario
        End If
    End Sub
End Class