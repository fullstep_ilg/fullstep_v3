﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.IO
Imports System.Data.OleDb
Imports System.Web.Script.Serialization

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()>
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class UtilesDesglose
	Inherits System.Web.Services.WebService
	'Debido a que los datos que carguemos los tendremos que utilizar en varias funciones creamos variables globales
#Region "Variables Globales"
	Dim FSNUser As FSNServer.User
	Dim FSNServer As FSNServer.Root
	Dim iTipoSolicitud As TiposDeDatos.TipoDeSolicitud
	Dim oFormasPago As FSNServer.FormasPago
	Dim oMonedas As FSNServer.Monedas
	Dim oUnidades As FSNServer.Unidades
	Dim oPaises As FSNServer.Paises
	Dim oDests As FSNServer.Destinos
	Dim oAlmacenes As FSNServer.Almacenes
	Dim oTiposPedido As FSNServer.TiposPedido
	Dim oPersonas As FSNServer.Personas
	Dim oMaterial As FSNServer.GruposMaterial
	Dim oArticulo As FSNServer.Articulos
	Dim oProveedor As FSNServer.Proveedores
	Dim oUnidadOrg As FSNServer.UnidadesOrg, oPres As FSNServer.UnidadesOrg
	Dim Textos As DataTable
	Dim oPresupsFavoritos1 As FSNServer.PresupuestosFavoritos
	Dim oPresupsFavoritos2 As FSNServer.PresupuestosFavoritos
	Dim oPresupsFavoritos3 As FSNServer.PresupuestosFavoritos
	Dim oPresupsFavoritos4 As FSNServer.PresupuestosFavoritos
	Dim oDepartamento As FSNServer.Departamentos
	Dim oCentroCoste As FSNServer.CentrosCoste
	Dim oPRES5 As FSNServer.PRES5
	Dim oActivo As FSNServer.Activos
	Dim oOrganizacionesCompras As FSNServer.OrganizacionesCompras
	Dim oCentro As FSNServer.Centros
	Dim oUONs As FSNServer.UnidadesOrg
	Dim oCentrosCoste As FSNServer.CentrosCoste
	Dim numeroLineas, nivelPres1, nivelPres2, nivelPres3, nivelPres4 As Integer
	Dim idSolicitudExcel As Long
	'Pasamos los posibles valores de los campos GS que haremos lista
	Dim TiposCampoGS As TiposDeDatos.TipoCampoGS() = {TiposDeDatos.TipoCampoGS.FormaPago, TiposDeDatos.TipoCampoGS.Moneda, TiposDeDatos.TipoCampoGS.Almacen,
									TiposDeDatos.TipoCampoGS.Pais, TiposDeDatos.TipoCampoGS.Provincia, TiposDeDatos.TipoCampoGS.Dest,
									TiposDeDatos.TipoCampoGS.PRES1, TiposDeDatos.TipoCampoGS.Pres2, TiposDeDatos.TipoCampoGS.Pres3, TiposDeDatos.TipoCampoGS.Pres4,
									TiposDeDatos.TipoCampoGS.Unidad, TiposDeDatos.TipoCampoGS.UnidadPedido, TiposDeDatos.TipoCampoGS.OrganizacionCompras, TiposDeDatos.TipoCampoGS.AnyoPartida}
	Dim TiposCampoGSNoTrasladablesExcel As TiposDeDatos.TipoCampoGS() = {TiposDeDatos.TipoCampoGS.PrecioUnitarioAdj, TiposDeDatos.TipoCampoGS.ProveedorAdj, TiposDeDatos.TipoCampoGS.CantidadAdj,
						TiposDeDatos.TipoCampoGS.TotalLineaAdj, TiposDeDatos.TipoCampoGS.TotalLineaPreadj, TiposDeDatos.TipoCampoGS.NumSolicitERP,
						TiposDeDatos.TipoCampoGS.ImporteSolicitudesVinculadas, TiposDeDatos.TipoCampoGS.ListadosPersonalizados, TiposDeDatos.TipoCampoGS.ProveedorERP,
						TiposDeDatos.TipoCampoGS.TipoPedido, TiposDeDatos.TipoCampoGS.Comprador, TiposDeDatos.TipoCampoGS.ArchivoContrato, TiposDeDatos.TipoCampoGS.ArchivoEspecific}
    Dim orgComprasOutDesglose, centroOutDesglose, UnidadOrganizativaOutDesglose, destinoOutDesglose As String
    Dim listNombresCabecera As List(Of String)
	Dim ordenCampos As String
	Dim tieneCentroDesglose As Boolean
	Dim oAnyoPartida As FSNServer.PartidaPRES5
#End Region
#Region "Generar Excel"
	<WebMethod(EnableSession:=True)>
	Public Function GenerarExcelDesglose(ByVal idCampoDesglose As Long, ByVal orgCompras As String, ByVal centro As String, ByVal destino As String,
											 ByVal favorito As Boolean, ByVal idSolicitud As Long, ByVal idInstancia As Long, ByVal UnidadOrganizativa As String, ByVal CentroCoste As String _
											 , ByVal Partida0 As String, ByVal Partida As String) As String
		'CONTRASEÑA DE LAS MACROS DE LA EXCEL = FullStep967 '
		Dim spath As String = ConfigurationManager.AppSettings("temp") & "\"
		Dim fileName As String = Guid.NewGuid.ToString & ".xls"
		Dim sConnectionString As String
		Dim excelConnection As OleDb.OleDbConnection = Nothing
		Dim objCommand As New OleDb.OleDbCommand()
		Dim CodSolicitud As String = ""
		Dim numMaximoRegistrosCampoGS As Integer = 0
		Dim NivelPartida As Integer = 0
		Try
			FSNUser = HttpContext.Current.Session("FSN_User")
			FSNServer = HttpContext.Current.Session("FSN_Server")
			If idSolicitud > 0 Then
				Dim oSolicitud As FSNServer.Solicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
				oSolicitud.ID = idSolicitud
				oSolicitud.Load(FSNUser.Idioma)
				If idInstancia > 0 Then
					fileName = FSNUser.Cod & "_" & oSolicitud.Codigo & "_" & idInstancia & ".xls"
				Else
					fileName = FSNUser.Cod & "_" & oSolicitud.Codigo & ".xls"
				End If
				idSolicitudExcel = oSolicitud.ID
				oSolicitud = Nothing
			Else
				If idInstancia > 0 Then
					Dim oInstancia As FSNServer.Instancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
					oInstancia.ID = idInstancia
					oInstancia.Load(FSNUser.Idioma)
					Dim oSolicitud As FSNServer.Solicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
					oSolicitud.ID = oInstancia.Solicitud.ID
					oSolicitud.Load(FSNUser.Idioma)
					fileName = FSNUser.Cod & "_" & oSolicitud.Codigo & "_" & oInstancia.ID & ".xls"
					idSolicitudExcel = oSolicitud.ID
					oSolicitud = Nothing
					oInstancia = Nothing
				End If
			End If
			fileName = Regex.Replace(fileName, "[^\w\.@]", "_")
			File.Copy(Server.MapPath(".") & "\PLANTILLA_DESGLOSE_MATERIAL.xls", spath & fileName, True)
			sConnectionString = String.Format(ConfigurationManager.AppSettings("ExcelConnectionString"), spath & fileName)

			'Quitamos la propiedad de solo lectura a la copia realizada
			Dim attributes As FileAttributes = File.GetAttributes(spath & fileName)
			attributes = RemoveReadOnlyAttribute(attributes)
			File.SetAttributes(spath & fileName, attributes)

			'Establecer una conexion con el origen de datos.
			excelConnection = New OleDb.OleDbConnection(sConnectionString)
			excelConnection.Open()
			objCommand.Connection = excelConnection

			'Cargamos los textos
			Dim oDict As FSNServer.Dictionary
			oDict = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
			oDict.LoadData(TiposDeDatos.ModulosIdiomas.DesgloseControl, FSNUser.Idioma)
			Textos = oDict.Data.Tables(0)

			Dim dtSheets As DataTable = excelConnection.GetSchema("Tables")
			'Miramos si en la plantilla de excel ya esta generada la hoja de las provincias.
			'Lo tenemos ya en la plantilla debido a que es algo que tarda mucho en traspasar
			'Si en algun momento se modificaran los datos de provincias deberiamos borrar la hoja y 
			'se reharia con los datos de base de datos.
			If Not dtSheets.Rows.OfType(Of DataRow).Where(Function(x) x("TABLE_NAME") = "DATOSPROVINCIAS").Any Then
				ActualizarPlantilla()
				excelConnection.Close()
				objCommand.Dispose()

				File.Copy(Server.MapPath(".") & "\PLANTILLA_DESGLOSE_MATERIAL.xls", spath & fileName, True)
				sConnectionString = String.Format(ConfigurationManager.AppSettings("ExcelConnectionString"), spath & fileName)
				File.SetAttributes(spath & fileName, attributes)

				excelConnection = New OleDb.OleDbConnection(sConnectionString)
				excelConnection.Open()
				objCommand = New OleDb.OleDbCommand()
				objCommand.Connection = excelConnection
			End If

			Dim infoDesglose As Object = HttpContext.Current.Cache("infoDesglose_" & FSNUser.Cod & "_" & idCampoDesglose)
			iTipoSolicitud = CType(infoDesglose(0), TiposDeDatos.TipoDeSolicitud)
			Dim ds As DataSet = CType(infoDesglose(1), DataSet)
			ds.Tables(0).TableName = "CONFIG"

			ordenCampos = "ORDEN"
			If {TiposDeDatos.TipoDeSolicitud.Certificado, TiposDeDatos.TipoDeSolicitud.NoConformidad}.Contains(iTipoSolicitud) Then ordenCampos = "ORDEN1"

			Dim nombreTablaValoresDefecto As String = "LINEAS"
			If ds.Tables.Contains("DEFECTO") Then nombreTablaValoresDefecto = "DEFECTO"

			Dim orgComprasDesglose As List(Of DataRow) = ds.Tables("CONFIG").Rows.OfType(Of DataRow).Where(Function(x) Not IsDBNull(x("TIPO_CAMPO_GS")) _
																															 AndAlso x("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.OrganizacionCompras _
																															 AndAlso x("ESCRITURA") = 0).ToList

			tieneCentroDesglose = ds.Tables("CONFIG").Rows.OfType(Of DataRow).Where(Function(x) Not IsDBNull(x("TIPO_CAMPO_GS")) _
																															 AndAlso x("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Centro).Any

			Dim centroDesglose As List(Of DataRow) = ds.Tables("CONFIG").Rows.OfType(Of DataRow).Where(Function(x) Not IsDBNull(x("TIPO_CAMPO_GS")) _
																															 AndAlso x("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Centro _
																															 AndAlso x("ESCRITURA") = 0).ToList
			Dim UnidadOrganizativaDesglose As List(Of DataRow) = ds.Tables("CONFIG").Rows.OfType(Of DataRow).Where(Function(x) Not IsDBNull(x("TIPO_CAMPO_GS")) _
																															 AndAlso x("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.UnidadOrganizativa _
																															 AndAlso x("ESCRITURA") = 0).ToList
			Dim CentroCosteDesglose As List(Of DataRow) = ds.Tables("CONFIG").Rows.OfType(Of DataRow).Where(Function(x) Not IsDBNull(x("TIPO_CAMPO_GS")) _
																															 AndAlso x("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.CentroCoste _
																															 AndAlso x("ESCRITURA") = 0).ToList

			Dim destinoDesglose As List(Of DataRow) = ds.Tables("CONFIG").Rows.OfType(Of DataRow).Where(Function(x) Not IsDBNull(x("TIPO_CAMPO_GS")) _
																															 AndAlso x("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Dest _
																														 AndAlso x("ESCRITURA") = 0).ToList

			Dim PartidaDesglose As List(Of DataRow) = ds.Tables("CONFIG").Rows.OfType(Of DataRow).Where(Function(x) Not IsDBNull(x("TIPO_CAMPO_GS")) _
																															 AndAlso x("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Partida _
																															 AndAlso x("ESCRITURA") = 0).ToList

			If ds.Tables(nombreTablaValoresDefecto).Rows.Count > 0 Then
				Dim rOrgCompras As New List(Of DataRow)
				If orgComprasDesglose.Count = 1 Then rOrgCompras = ds.Tables(nombreTablaValoresDefecto).Rows.OfType(Of DataRow).Where(Function(y) y("LINEA") = 1 AndAlso y("CAMPO_HIJO") = orgComprasDesglose.First.Item("ID")).ToList
				If rOrgCompras.Count = 1 Then
					orgComprasOutDesglose = rOrgCompras.First.Item("VALOR_TEXT").ToString
				Else
					orgComprasOutDesglose = orgCompras
				End If

				Dim rCentro As New List(Of DataRow)
				If centroDesglose.Count = 1 Then rCentro = ds.Tables(nombreTablaValoresDefecto).Rows.OfType(Of DataRow).Where(Function(y) y("LINEA") = 1 AndAlso y("CAMPO_HIJO") = centroDesglose.First.Item("ID")).ToList
				If rCentro.Count = 1 Then
					centroOutDesglose = rCentro.First.Item("VALOR_TEXT").ToString
				Else
					centroOutDesglose = centro
				End If

				Dim rUnidadOrganizativa As New List(Of DataRow)
				If UnidadOrganizativaDesglose.Count = 1 Then rUnidadOrganizativa = ds.Tables(nombreTablaValoresDefecto).Rows.OfType(Of DataRow).Where(Function(y) y("LINEA") = 1 AndAlso y("CAMPO_HIJO") = UnidadOrganizativaDesglose.First.Item("ID")).ToList
                If rUnidadOrganizativa.Count = 1 Then
                    UnidadOrganizativaOutDesglose = rUnidadOrganizativa.First.Item("VALOR_TEXT").ToString
                Else
                    UnidadOrganizativaOutDesglose = UnidadOrganizativa
                End If
            End If

			listNombresCabecera = New List(Of String)
			'Creamos una hoja en excel con la configuracion de los campos del desglose, para poder validar, tipo de dato...
			Dim cabeceraLineas As String = String.Empty, cabeceraLineasValues As String = String.Empty
			Crear_HojaExcel_ConfiguracionCampos(objCommand, ds.Tables("CONFIG"), cabeceraLineas, cabeceraLineasValues, numMaximoRegistrosCampoGS, NivelPartida)

			'Para los campos de desglose de tipo lista, traspasamos sus valores
			Crear_HojaExcel_ValoresCamposLista(objCommand, ds.Tables("CONFIG"), ds.Tables(1))

			'Para los campos que aun no siendo de tipo desglose, crearemos un combo, traspasamos sus valores
			'Por tanto, en esta funcion cargaremos en las variables globales sus datos
			Dim createCamposGS As String = String.Empty, insertCamposGS As String = String.Empty
			Cargar_Valores_CamposDesglose(objCommand, ds.Tables("CONFIG"), ds.Tables(nombreTablaValoresDefecto), numMaximoRegistrosCampoGS, createCamposGS, insertCamposGS)

			'Si se ha definido una linea por defecto la exportamos al excel
			If ds.Tables.Contains("DEFECTO") Then
				If ds.Tables("DEFECTO").Rows.Count > 0 Then
					Dim filasDesglose As DataRow() = ds.Tables("LINEAS").Rows.OfType(Of DataRow).Where(Function(x) DBNullToInteger(x("TIPO_CAMPO_GS")) = 3000).ToArray
					If filasDesglose.Count > 0 Then
						For i As Integer = 1 To ds.Tables("DEFECTO").AsEnumerable.Max(Function(linea) linea("LINEA"))
							Dim filaLinea As DataRow = ds.Tables("DEFECTO").NewRow
							With filaLinea
								.Item("LINEA") = i
								.Item("CAMPO_PADRE") = filasDesglose.First.Item("CAMPO_PADRE")
								.Item("CAMPO_HIJO") = filasDesglose.First.Item("CAMPO_HIJO")
								.Item("FECACT") = filasDesglose.First.Item("FECACT")
								.Item("ID_ATRIB_GS") = filasDesglose.First.Item("ID_ATRIB_GS")
								.Item("TIPO_CAMPO_GS") = filasDesglose.First.Item("TIPO_CAMPO_GS")
								.Item("VALOR_BOOL") = filasDesglose.First.Item("VALOR_BOOL")
								.Item("VALOR_NUM") = filasDesglose.First.Item("VALOR_NUM")
								.Item("VALOR_FEC") = filasDesglose.First.Item("VALOR_FEC")
								.Item("VALOR_TEXT") = filasDesglose.First.Item("VALOR_TEXT")
							End With
							ds.Tables("DEFECTO").Rows.Add(filaLinea)
						Next
					End If
				End If
			End If
			Crear_HojaExcel_Lineas(objCommand, cabeceraLineas, cabeceraLineasValues, ds.Tables(nombreTablaValoresDefecto), ds.Tables("CONFIG"), Textos(15)(1), Textos(16)(1), favorito, NivelPartida)

			'Para los campos de tipo GS que se ha determinado crear un combo, pasamos los posibles valores que podran seleccionar y que los hemos cargado anteriormente
			Crear_HojaExcel_CamposGS(objCommand, ds.Tables("CONFIG"), TiposCampoGS, numMaximoRegistrosCampoGS, createCamposGS, insertCamposGS)

			'Pasamos los textos que utilizaremos en la excel
			Crear_HojaExcel_Textos(objCommand, Textos)

			Return fileName
		Catch ex As Exception
			Throw ex
		Finally
			excelConnection.Close()
			objCommand = Nothing
			excelConnection = Nothing
		End Try
	End Function
	Private Sub Crear_HojaExcel_ConfiguracionCampos(ByVal objCommand As OleDb.OleDbCommand, ByVal dtConfiguracionCampos As DataTable,
														ByRef cabeceraLineas As String, ByRef cabeceraLineasValues As String, ByRef numMaximoRegistrosCampoGS As Integer, ByRef NivelPartida As Integer)
		Dim sConsulta As String = "CREATE TABLE [CONFIGCAMPOS] (ID INT,CABECERA MEMO,TIPO_CAMPO_GS INT,TIPO INT,SUBTIPO INT,INTRO INT,MINNUM FLOAT,MAXNUM FLOAT,MINFEC DATE,MAXFEC DATE,"
		sConsulta &= "MAXLENGTH INT,OBLIGATORIO INT,ANYADIR_ART INT,NOMBRECAMPODESGLOSE MEMO,PRES5 MEMO,FORMATO MEMO,CARGAR_REL_PROVE_ART4 TINYINT,CARGAR_ULT_ADJ TINYINT,NIVELPRES TINYINT)"
		objCommand.CommandText = sConsulta
		objCommand.ExecuteNonQuery()

		'Buscar tipocampogs de cod art si es obligatorio, y si lo es quitarlo a material y denominacion de articulo, prevalece lo establecido en cod art
		If dtConfiguracionCampos.Rows.OfType(Of DataRow).Where(Function(x) (DBNullToInteger(x("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.CodArticulo OrElse DBNullToInteger(x("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo) _
																	   AndAlso x("OBLIGATORIO") = 1).Any Then
			For Each item As DataRow In dtConfiguracionCampos.Rows.OfType(Of DataRow).Where(Function(y) DBNullToInteger(y("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Material _
																									  OrElse DBNullToInteger(y("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.DenArticulo).ToArray
				item("OBLIGATORIO") = 0
			Next
		End If
		objCommand.CommandText = "INSERT INTO [CONFIGCAMPOS$] VALUES(@ID,@CABECERA,@TIPO_CAMPO_GS,@TIPO,@SUBTIPO,@INTRO,@MINNUM,@MAXNUM,@MINFEC,@MAXFEC,@MAXLENGTH,@OBLIGATORIO,@ANYADIR_ART,@NOMBRECAMPODESGLOSE,@PRES5,@FORMATO,@CARGAR_REL_PROVE_ART4,@CARGAR_ULT_ADJ,@NIVELPRES)"
		Dim listaColumnasDesglose As List(Of DataRow) = dtConfiguracionCampos.Rows.OfType(Of DataRow).Where(Function(x) DBNullToInteger(x("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NumLinea _
																						OrElse (((x("VISIBLE") = 1 AndAlso x("ESCRITURA") = 1 AndAlso x("CARGAR_ULT_ADJ") = 0) _
																									OrElse (x("VISIBLE") = 1 AndAlso x("ESCRITURA") = 1 AndAlso {TiposDeDatos.TipoCampoGS.PrecioUnitario, TiposDeDatos.TipoCampoGS.Proveedor}.Contains(DBNullToInteger(x("TIPO_CAMPO_GS"))))) _
																								AndAlso (Not TiposCampoGSNoTrasladablesExcel.Contains(DBNullToInteger(x("TIPO_CAMPO_GS"))) _
																										 OrElse (DBNullToInteger(x("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.PrecioUnitario _
																												 AndAlso (Not iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoNegociado _
																														  AndAlso Not (iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoExpress _
																																   AndAlso FSNServer.TipoAcceso.gbArticulosNegociadosPedExpress)))))).OrderBy(Function(y) y(ordenCampos)).ToList
		For Each colDesglose As DataRow In listaColumnasDesglose
			Select Case DBNullToInteger(colDesglose("TIPO_CAMPO_GS"))
				Case TiposDeDatos.TipoCampoGS.Material
					For gmn As Integer = 1 To 4
						With FSNServer.TipoAcceso
							objCommand.Parameters.Clear()
							objCommand.Parameters.AddWithValue("@ID", colDesglose.Item("ID"))
							Select Case gmn
								Case 1
									objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, .gbGMN1Cod, "STRING"))
									cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
								Case 2
									objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, .gbGMN2Cod, "STRING"))
									cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
								Case 3
									objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, .gbGMN3Cod, "STRING"))
									cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
								Case Else
									objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, .gbGMN4Cod, "STRING"))
									cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
							End Select
							objCommand.Parameters.AddWithValue("@TIPO_CAMPO_GS", colDesglose("TIPO_CAMPO_GS"))
							objCommand.Parameters.AddWithValue("@TIPO", colDesglose("TIPO"))
							objCommand.Parameters.AddWithValue("@SUBTIPO", colDesglose("SUBTIPO"))
							objCommand.Parameters.AddWithValue("@INTRO", colDesglose("INTRO"))
							objCommand.Parameters.AddWithValue("@MINNUM", colDesglose("MINNUM"))
							objCommand.Parameters.AddWithValue("@MAXNUM", colDesglose("MAXNUM"))
							objCommand.Parameters.AddWithValue("@MINFEC", colDesglose("MINFEC"))
							objCommand.Parameters.AddWithValue("@MAXFEC", colDesglose("MAXFEC"))
							Select Case gmn
								Case 1
									objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongCodGMN1)
								Case 2
									objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongCodGMN2)
								Case 3
									objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongCodGMN3)
								Case Else
									objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongCodGMN4)
							End Select
							objCommand.Parameters.AddWithValue("@OBLIGATORIO", colDesglose("OBLIGATORIO"))
							objCommand.Parameters.AddWithValue("@ANYADIR_ART", colDesglose("ANYADIR_ART"))
							objCommand.Parameters.AddWithValue("@NOMBRECAMPODESGLOSE", colDesglose("DEN_" & FSNUser.Idioma.ToString))
							objCommand.Parameters.AddWithValue("@PRES5", DBNull.Value)
							objCommand.Parameters.AddWithValue("@FORMATO", DBNull.Value)
							objCommand.Parameters.AddWithValue("@CARGAR_REL_PROVE_ART4", DBNull.Value)
							objCommand.Parameters.AddWithValue("@CARGAR_ULT_ADJ", DBNull.Value)
							objCommand.Parameters.AddWithValue("@NIVELPRES", DBNull.Value)
							objCommand.ExecuteNonQuery()
						End With
					Next
				Case TiposDeDatos.TipoCampoGS.UnidadOrganizativa
					For nivel As Integer = 1 To 3
						With FSNServer.TipoAcceso
							objCommand.Parameters.Clear()
							objCommand.Parameters.AddWithValue("@ID", colDesglose.Item("ID"))
							Select Case nivel
								Case 1
									objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, .gbEmpresa & " - " & .gbUON1Cod, "STRING"))
									cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
								Case 2
									objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, .gbEmpresa & " - " & .gbUON2Cod, "STRING"))
									cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
								Case Else
									objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, .gbEmpresa & " - " & .gbUON3Cod, "STRING"))
									cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
							End Select
							objCommand.Parameters.AddWithValue("@TIPO_CAMPO_GS", colDesglose("TIPO_CAMPO_GS"))
							objCommand.Parameters.AddWithValue("@TIPO", colDesglose("TIPO"))
							objCommand.Parameters.AddWithValue("@SUBTIPO", colDesglose("SUBTIPO"))
							objCommand.Parameters.AddWithValue("@INTRO", colDesglose("INTRO"))
							objCommand.Parameters.AddWithValue("@MINNUM", colDesglose("MINNUM"))
							objCommand.Parameters.AddWithValue("@MAXNUM", colDesglose("MAXNUM"))
							objCommand.Parameters.AddWithValue("@MINFEC", colDesglose("MINFEC"))
							objCommand.Parameters.AddWithValue("@MAXFEC", colDesglose("MAXFEC"))
							Select Case nivel
								Case 1
									objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongCodUON1)
								Case 2
									objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongCodUON2)
								Case Else
									objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongCodUON3)
							End Select
							objCommand.Parameters.AddWithValue("@OBLIGATORIO", colDesglose("OBLIGATORIO"))
							objCommand.Parameters.AddWithValue("@ANYADIR_ART", colDesglose("ANYADIR_ART"))
							objCommand.Parameters.AddWithValue("@NOMBRECAMPODESGLOSE", colDesglose("DEN_" & FSNUser.Idioma.ToString))
							objCommand.Parameters.AddWithValue("@PRES5", DBNull.Value)
							objCommand.Parameters.AddWithValue("@FORMATO", DBNull.Value)
							objCommand.Parameters.AddWithValue("@CARGAR_REL_PROVE_ART4", DBNull.Value)
							objCommand.Parameters.AddWithValue("@CARGAR_ULT_ADJ", DBNull.Value)
							objCommand.Parameters.AddWithValue("@NIVELPRES", DBNull.Value)
							objCommand.ExecuteNonQuery()
						End With
					Next
				Case TiposDeDatos.TipoCampoGS.CentroCoste
					For nivel As Integer = 1 To 4
						With FSNServer.TipoAcceso
							objCommand.Parameters.Clear()
							objCommand.Parameters.AddWithValue("@ID", colDesglose.Item("ID"))
							Select Case nivel
								Case 1
									objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, colDesglose.Item("DEN_" & FSNUser.Idioma.ToString).ToString & " - UO1", "STRING"))
									cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
								Case 2
									objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, colDesglose.Item("DEN_" & FSNUser.Idioma.ToString).ToString & " - UO2", "STRING"))
									cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
								Case 3
									objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, colDesglose.Item("DEN_" & FSNUser.Idioma.ToString).ToString & " - UO3", "STRING"))
									cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
								Case Else
									objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, colDesglose.Item("DEN_" & FSNUser.Idioma.ToString).ToString & " - UO4", "STRING"))
									cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
							End Select
							objCommand.Parameters.AddWithValue("@TIPO_CAMPO_GS", colDesglose("TIPO_CAMPO_GS"))
							objCommand.Parameters.AddWithValue("@TIPO", colDesglose("TIPO"))
							objCommand.Parameters.AddWithValue("@SUBTIPO", colDesglose("SUBTIPO"))
							objCommand.Parameters.AddWithValue("@INTRO", colDesglose("INTRO"))
							objCommand.Parameters.AddWithValue("@MINNUM", colDesglose("MINNUM"))
							objCommand.Parameters.AddWithValue("@MAXNUM", colDesglose("MAXNUM"))
							objCommand.Parameters.AddWithValue("@MINFEC", colDesglose("MINFEC"))
							objCommand.Parameters.AddWithValue("@MAXFEC", colDesglose("MAXFEC"))
							Select Case nivel
								Case 1
									objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongCodUON1)
								Case 2
									objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongCodUON2)
								Case 3
									objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongCodUON3)
								Case Else
									objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongCodUON4)
							End Select
							objCommand.Parameters.AddWithValue("@OBLIGATORIO", colDesglose("OBLIGATORIO"))
							objCommand.Parameters.AddWithValue("@ANYADIR_ART", colDesglose("ANYADIR_ART"))
							objCommand.Parameters.AddWithValue("@NOMBRECAMPODESGLOSE", colDesglose("DEN_" & FSNUser.Idioma.ToString))
							objCommand.Parameters.AddWithValue("@PRES5", DBNull.Value)
							objCommand.Parameters.AddWithValue("@FORMATO", DBNull.Value)
							objCommand.Parameters.AddWithValue("@CARGAR_REL_PROVE_ART4", DBNull.Value)
							objCommand.Parameters.AddWithValue("@CARGAR_ULT_ADJ", DBNull.Value)
							objCommand.Parameters.AddWithValue("@NIVELPRES", DBNull.Value)
							objCommand.ExecuteNonQuery()
						End With
					Next
				Case TiposDeDatos.TipoCampoGS.Partida
					'Mirando GenerarValoresDesglose_Validaciones se ve q espera Ó un campo centro coste antes de la partida Ó q se le indiquen los codigos de partida. Por ello no se mete aquí­ columnas para uon.

					oPRES5 = FSNServer.Get_Object(GetType(FSNServer.PRES5))
					NivelPartida = oPRES5.DameNivelAceptablePartida(colDesglose("PRES5"))
					For partida As Integer = 1 To NivelPartida
						With FSNServer.TipoAcceso
							objCommand.Parameters.Clear()
							objCommand.Parameters.AddWithValue("@ID", colDesglose.Item("ID"))
							Select Case partida
								Case 1
									objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, colDesglose.Item("DEN_" & FSNUser.Idioma.ToString).ToString & " - PRES5_NIV1", "STRING"))
									cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
								Case 2
									objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, colDesglose.Item("DEN_" & FSNUser.Idioma.ToString).ToString & " - PRES5_NIV2", "STRING"))
									cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
								Case 3
									objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, colDesglose.Item("DEN_" & FSNUser.Idioma.ToString).ToString & " - PRES5_NIV3", "STRING"))
									cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
								Case Else
									objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, colDesglose.Item("DEN_" & FSNUser.Idioma.ToString).ToString & " - PRES5_NIV4", "STRING"))
									cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
							End Select
							objCommand.Parameters.AddWithValue("@TIPO_CAMPO_GS", colDesglose("TIPO_CAMPO_GS"))
							objCommand.Parameters.AddWithValue("@TIPO", colDesglose("TIPO"))
							objCommand.Parameters.AddWithValue("@SUBTIPO", colDesglose("SUBTIPO"))
							objCommand.Parameters.AddWithValue("@INTRO", colDesglose("INTRO"))
							objCommand.Parameters.AddWithValue("@MINNUM", colDesglose("MINNUM"))
							objCommand.Parameters.AddWithValue("@MAXNUM", colDesglose("MAXNUM"))
							objCommand.Parameters.AddWithValue("@MINFEC", colDesglose("MINFEC"))
							objCommand.Parameters.AddWithValue("@MAXFEC", colDesglose("MAXFEC"))
							Select Case partida
								Case 1
									objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongPRES5_NIV1Cod)
								Case 2
									objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongPRES5_NIV2Cod)
								Case 3
									objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongPRES5_NIV3Cod)
								Case 4
									objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongPRES5_NIV4Cod)
								Case Else
									objCommand.Parameters.AddWithValue("@MAXLENGTH", DBNull.Value)
							End Select
							objCommand.Parameters.AddWithValue("@OBLIGATORIO", colDesglose("OBLIGATORIO"))
							objCommand.Parameters.AddWithValue("@ANYADIR_ART", colDesglose("ANYADIR_ART"))
							objCommand.Parameters.AddWithValue("@NOMBRECAMPODESGLOSE", colDesglose("DEN_" & FSNUser.Idioma.ToString))
							objCommand.Parameters.AddWithValue("@PRES5", colDesglose("PRES5"))
							objCommand.Parameters.AddWithValue("@FORMATO", DBNull.Value)
							objCommand.Parameters.AddWithValue("@CARGAR_REL_PROVE_ART4", DBNull.Value)
							objCommand.Parameters.AddWithValue("@CARGAR_ULT_ADJ", DBNull.Value)
							objCommand.Parameters.AddWithValue("@NIVELPRES", DBNull.Value)
							objCommand.ExecuteNonQuery()
						End With
					Next
				Case TiposDeDatos.TipoCampoGS.Almacen
					objCommand.Parameters.Clear()
					objCommand.Parameters.AddWithValue("@ID", colDesglose.Item("ID"))
					objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, Replace(colDesglose.Item("DEN_" & FSNUser.Idioma.ToString).ToString, ".", ""), "STRING"))
					objCommand.Parameters.AddWithValue("@TIPO_CAMPO_GS", colDesglose("TIPO_CAMPO_GS"))
					objCommand.Parameters.AddWithValue("@TIPO", colDesglose("TIPO"))
					objCommand.Parameters.AddWithValue("@SUBTIPO", colDesglose("SUBTIPO"))
					objCommand.Parameters.AddWithValue("@INTRO", colDesglose("INTRO"))
					objCommand.Parameters.AddWithValue("@MINNUM", colDesglose("MINNUM"))
					objCommand.Parameters.AddWithValue("@MAXNUM", colDesglose("MAXNUM"))
					objCommand.Parameters.AddWithValue("@MINFEC", colDesglose("MINFEC"))
					objCommand.Parameters.AddWithValue("@MAXFEC", colDesglose("MAXFEC"))
					objCommand.Parameters.AddWithValue("@MAXLENGTH", DBNull.Value)
					objCommand.Parameters.AddWithValue("@OBLIGATORIO", colDesglose("OBLIGATORIO"))
					objCommand.Parameters.AddWithValue("@ANYADIR_ART", 0)
					objCommand.Parameters.AddWithValue("@NOMBRECAMPODESGLOSE", colDesglose("DEN_" & FSNUser.Idioma.ToString))
					objCommand.Parameters.AddWithValue("@PRES5", DBNull.Value)
					objCommand.Parameters.AddWithValue("@FORMATO", DBNull.Value)
					objCommand.Parameters.AddWithValue("@CARGAR_REL_PROVE_ART4", DBNull.Value)
					objCommand.Parameters.AddWithValue("@CARGAR_ULT_ADJ", DBNull.Value)
					objCommand.Parameters.AddWithValue("@NIVELPRES", DBNull.Value)
					objCommand.ExecuteNonQuery()
					cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
				Case TiposDeDatos.TipoCampoGS.PRES1
					'Para los NO Favoritos
					oPres = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
					nivelPres1 = oPres.GetNivelPres(1)
					'Para los Favoritos
					oPresupsFavoritos1 = FSNServer.Get_Object(GetType(FSNServer.PresupuestosFavoritos))
					oPresupsFavoritos1.LoadData(FSNUser.Cod, 1)
					If oPresupsFavoritos1.Data.Tables(0).Rows.Count > numMaximoRegistrosCampoGS Then numMaximoRegistrosCampoGS = oPresupsFavoritos1.Data.Tables(0).Rows.Count
					'

					Dim oDict As FSNServer.Dictionary
					oDict = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
					oDict.LoadData(TiposDeDatos.ModulosIdiomas.DesgloseControl, FSNUser.Idioma)
					Textos = oDict.Data.Tables(0)

					objCommand.Parameters.Clear()
					objCommand.Parameters.AddWithValue("@ID", colDesglose.Item("ID"))
					objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, Replace(colDesglose.Item("DEN_" & FSNUser.Idioma.ToString).ToString, ".", ""), "STRING"))
					objCommand.Parameters.AddWithValue("@TIPO_CAMPO_GS", colDesglose("TIPO_CAMPO_GS"))
					objCommand.Parameters.AddWithValue("@TIPO", colDesglose("TIPO"))
					objCommand.Parameters.AddWithValue("@SUBTIPO", colDesglose("SUBTIPO"))
					objCommand.Parameters.AddWithValue("@INTRO", colDesglose("INTRO"))
					objCommand.Parameters.AddWithValue("@MINNUM", colDesglose("MINNUM"))
					objCommand.Parameters.AddWithValue("@MAXNUM", colDesglose("MAXNUM"))
					objCommand.Parameters.AddWithValue("@MINFEC", colDesglose("MINFEC"))
					objCommand.Parameters.AddWithValue("@MAXFEC", colDesglose("MAXFEC"))
					objCommand.Parameters.AddWithValue("@MAXLENGTH", colDesglose("MAX_LENGTH"))
					objCommand.Parameters.AddWithValue("@OBLIGATORIO", colDesglose("OBLIGATORIO"))
					objCommand.Parameters.AddWithValue("@ANYADIR_ART", 0)
					objCommand.Parameters.AddWithValue("@NOMBRECAMPODESGLOSE", colDesglose("DEN_" & FSNUser.Idioma.ToString))
					objCommand.Parameters.AddWithValue("@PRES5", DBNull.Value)
					objCommand.Parameters.AddWithValue("@FORMATO", DBNull.Value)
					objCommand.Parameters.AddWithValue("@CARGAR_REL_PROVE_ART4", DBNull.Value)
					objCommand.Parameters.AddWithValue("@CARGAR_ULT_ADJ", DBNull.Value)
					objCommand.Parameters.AddWithValue("@NIVELPRES", nivelPres1)
					objCommand.ExecuteNonQuery()
					cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
				Case TiposDeDatos.TipoCampoGS.Pres2
					'Para los NO van a ser Favoritos
					oPres = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
					nivelPres2 = oPres.GetNivelPres(2)
					'Para los Favoritos
					oPresupsFavoritos2 = FSNServer.Get_Object(GetType(FSNServer.PresupuestosFavoritos))
					oPresupsFavoritos2.LoadData(FSNUser.Cod, 2)
					If oPresupsFavoritos2.Data.Tables(0).Rows.Count > numMaximoRegistrosCampoGS Then numMaximoRegistrosCampoGS = oPresupsFavoritos2.Data.Tables(0).Rows.Count
					'
					objCommand.Parameters.Clear()
					objCommand.Parameters.AddWithValue("@ID", colDesglose.Item("ID"))
					objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, Replace(colDesglose.Item("DEN_" & FSNUser.Idioma.ToString).ToString, ".", ""), "STRING"))
					objCommand.Parameters.AddWithValue("@TIPO_CAMPO_GS", colDesglose("TIPO_CAMPO_GS"))
					objCommand.Parameters.AddWithValue("@TIPO", colDesglose("TIPO"))
					objCommand.Parameters.AddWithValue("@SUBTIPO", colDesglose("SUBTIPO"))
					objCommand.Parameters.AddWithValue("@INTRO", colDesglose("INTRO"))
					objCommand.Parameters.AddWithValue("@MINNUM", colDesglose("MINNUM"))
					objCommand.Parameters.AddWithValue("@MAXNUM", colDesglose("MAXNUM"))
					objCommand.Parameters.AddWithValue("@MINFEC", colDesglose("MINFEC"))
					objCommand.Parameters.AddWithValue("@MAXFEC", colDesglose("MAXFEC"))
					objCommand.Parameters.AddWithValue("@MAXLENGTH", colDesglose("MAX_LENGTH"))
					objCommand.Parameters.AddWithValue("@OBLIGATORIO", colDesglose("OBLIGATORIO"))
					objCommand.Parameters.AddWithValue("@ANYADIR_ART", 0)
					objCommand.Parameters.AddWithValue("@NOMBRECAMPODESGLOSE", colDesglose("DEN_" & FSNUser.Idioma.ToString))
					objCommand.Parameters.AddWithValue("@PRES5", DBNull.Value)
					objCommand.Parameters.AddWithValue("@FORMATO", DBNull.Value)
					objCommand.Parameters.AddWithValue("@CARGAR_REL_PROVE_ART4", DBNull.Value)
					objCommand.Parameters.AddWithValue("@CARGAR_ULT_ADJ", DBNull.Value)
					objCommand.Parameters.AddWithValue("@NIVELPRES", nivelPres2)
					objCommand.ExecuteNonQuery()
					cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
				Case TiposDeDatos.TipoCampoGS.Pres3
					'Para los NO van a ser Favoritos
					oPres = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
					nivelPres3 = oPres.GetNivelPres(3)
					'Para los Favoritos
					oPresupsFavoritos3 = FSNServer.Get_Object(GetType(FSNServer.PresupuestosFavoritos))
					oPresupsFavoritos3.LoadData(FSNUser.Cod, 3)
					If oPresupsFavoritos3.Data.Tables(0).Rows.Count > numMaximoRegistrosCampoGS Then numMaximoRegistrosCampoGS = oPresupsFavoritos3.Data.Tables(0).Rows.Count
					'
					objCommand.Parameters.Clear()
					objCommand.Parameters.AddWithValue("@ID", colDesglose.Item("ID"))
					objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, Replace(colDesglose.Item("DEN_" & FSNUser.Idioma.ToString).ToString, ".", ""), "STRING"))

					objCommand.Parameters.AddWithValue("@TIPO_CAMPO_GS", colDesglose("TIPO_CAMPO_GS"))
					objCommand.Parameters.AddWithValue("@TIPO", colDesglose("TIPO"))
					objCommand.Parameters.AddWithValue("@SUBTIPO", colDesglose("SUBTIPO"))
					objCommand.Parameters.AddWithValue("@INTRO", colDesglose("INTRO"))
					objCommand.Parameters.AddWithValue("@MINNUM", colDesglose("MINNUM"))
					objCommand.Parameters.AddWithValue("@MAXNUM", colDesglose("MAXNUM"))
					objCommand.Parameters.AddWithValue("@MINFEC", colDesglose("MINFEC"))
					objCommand.Parameters.AddWithValue("@MAXFEC", colDesglose("MAXFEC"))
					objCommand.Parameters.AddWithValue("@MAXLENGTH", colDesglose("MAX_LENGTH"))
					objCommand.Parameters.AddWithValue("@OBLIGATORIO", colDesglose("OBLIGATORIO"))
					objCommand.Parameters.AddWithValue("@ANYADIR_ART", 0)
					objCommand.Parameters.AddWithValue("@NOMBRECAMPODESGLOSE", colDesglose("DEN_" & FSNUser.Idioma.ToString))
					objCommand.Parameters.AddWithValue("@PRES5", DBNull.Value)
					objCommand.Parameters.AddWithValue("@FORMATO", DBNull.Value)
					objCommand.Parameters.AddWithValue("@CARGAR_REL_PROVE_ART4", DBNull.Value)
					objCommand.Parameters.AddWithValue("@CARGAR_ULT_ADJ", DBNull.Value)
					objCommand.Parameters.AddWithValue("@NIVELPRES", nivelPres3)
					objCommand.ExecuteNonQuery()
					cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
				Case TiposDeDatos.TipoCampoGS.Pres4
					'Para los NO van a ser Favoritos
					oPres = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
					nivelPres4 = oPres.GetNivelPres(4)
					'Para los Favoritos
					oPresupsFavoritos4 = FSNServer.Get_Object(GetType(FSNServer.PresupuestosFavoritos))
					oPresupsFavoritos4.LoadData(FSNUser.Cod, 4)
					If oPresupsFavoritos4.Data.Tables(0).Rows.Count > numMaximoRegistrosCampoGS Then numMaximoRegistrosCampoGS = oPresupsFavoritos4.Data.Tables(0).Rows.Count
					'  
					objCommand.Parameters.Clear()
					objCommand.Parameters.AddWithValue("@ID", colDesglose.Item("ID"))
					objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, Replace(colDesglose.Item("DEN_" & FSNUser.Idioma.ToString).ToString, ".", ""), "STRING"))

					objCommand.Parameters.AddWithValue("@TIPO_CAMPO_GS", colDesglose("TIPO_CAMPO_GS"))
					objCommand.Parameters.AddWithValue("@TIPO", colDesglose("TIPO"))
					objCommand.Parameters.AddWithValue("@SUBTIPO", colDesglose("SUBTIPO"))
					objCommand.Parameters.AddWithValue("@INTRO", colDesglose("INTRO"))
					objCommand.Parameters.AddWithValue("@MINNUM", colDesglose("MINNUM"))
					objCommand.Parameters.AddWithValue("@MAXNUM", colDesglose("MAXNUM"))
					objCommand.Parameters.AddWithValue("@MINFEC", colDesglose("MINFEC"))
					objCommand.Parameters.AddWithValue("@MAXFEC", colDesglose("MAXFEC"))
					objCommand.Parameters.AddWithValue("@MAXLENGTH", colDesglose("MAX_LENGTH"))
					objCommand.Parameters.AddWithValue("@OBLIGATORIO", colDesglose("OBLIGATORIO"))
					objCommand.Parameters.AddWithValue("@ANYADIR_ART", 0)
					objCommand.Parameters.AddWithValue("@NOMBRECAMPODESGLOSE", colDesglose("DEN_" & FSNUser.Idioma.ToString))
					objCommand.Parameters.AddWithValue("@PRES5", DBNull.Value)
					objCommand.Parameters.AddWithValue("@FORMATO", DBNull.Value)
					objCommand.Parameters.AddWithValue("@CARGAR_REL_PROVE_ART4", DBNull.Value)
					objCommand.Parameters.AddWithValue("@CARGAR_ULT_ADJ", DBNull.Value)
					objCommand.Parameters.AddWithValue("@NIVELPRES", nivelPres4)
					objCommand.ExecuteNonQuery()
					cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
				Case TiposDeDatos.TipoCampoGS.PrecioUnitarioAdj, TiposDeDatos.TipoCampoGS.ProveedorAdj, TiposDeDatos.TipoCampoGS.CantidadAdj,
						TiposDeDatos.TipoCampoGS.TotalLineaAdj, TiposDeDatos.TipoCampoGS.TotalLineaPreadj, TiposDeDatos.TipoCampoGS.NumSolicitERP,
						TiposDeDatos.TipoCampoGS.ImporteSolicitudesVinculadas, TiposDeDatos.TipoCampoGS.ListadosPersonalizados, TiposDeDatos.TipoCampoGS.ProveedorERP,
						TiposDeDatos.TipoCampoGS.TipoPedido, TiposDeDatos.TipoCampoGS.Comprador
					'ESTOS CAMPOS NO SE TRASLADAN A LA EXCEL (ALGUNOS NI SIQUIERA SE PUEDEN PONER EN UN DESGLOSE)
				Case Else 'Si es un tipo de archivo o campo calculado no lo pasamos a la excel
					If Not {TiposDeDatos.TipoGeneral.TipoArchivo}.Contains(colDesglose("SUBTIPO")) _
							AndAlso Not (colDesglose("TIPO") = TipoCampoPredefinido.Calculado AndAlso colDesglose("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo AndAlso Not IsDBNull(colDesglose("ID_CALCULO"))) Then
						objCommand.Parameters.Clear()
						objCommand.Parameters.AddWithValue("@ID", colDesglose.Item("ID"))
						Select Case colDesglose("SUBTIPO")
							Case TiposDeDatos.TipoGeneral.TipoNumerico
								objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, Replace(colDesglose.Item("DEN_" & FSNUser.Idioma.ToString).ToString, ".", ""), "FLOAT"))
							Case TiposDeDatos.TipoGeneral.TipoFecha
								objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, Replace(colDesglose.Item("DEN_" & FSNUser.Idioma.ToString).ToString, ".", ""), "DATE"))
							Case Else
								objCommand.Parameters.AddWithValue("@CABECERA", CrearCadenaCamposExcel(cabeceraLineas, Replace(colDesglose.Item("DEN_" & FSNUser.Idioma.ToString).ToString, ".", ""), "STRING"))
						End Select
						objCommand.Parameters.AddWithValue("@TIPO_CAMPO_GS", colDesglose("TIPO_CAMPO_GS"))
						objCommand.Parameters.AddWithValue("@TIPO", colDesglose("TIPO"))
						objCommand.Parameters.AddWithValue("@SUBTIPO", colDesglose("SUBTIPO"))
						objCommand.Parameters.AddWithValue("@INTRO", colDesglose("INTRO"))
						objCommand.Parameters.AddWithValue("@MINNUM", colDesglose("MINNUM"))
						objCommand.Parameters.AddWithValue("@MAXNUM", colDesglose("MAXNUM"))
						objCommand.Parameters.AddWithValue("@MINFEC", If(colDesglose("FECHA_VALOR_NO_ANT_SIS") = 1, Now.Date.ToShortDateString, colDesglose("MINFEC")))
						objCommand.Parameters.AddWithValue("@MAXFEC", colDesglose("MAXFEC"))
						Select Case DBNullToInteger(colDesglose("TIPO_CAMPO_GS"))
							Case TiposDeDatos.TipoCampoGS.CodArticulo, TiposDeDatos.TipoCampoGS.NuevoCodArticulo
								objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongCodART)
							Case TiposDeDatos.TipoCampoGS.Persona
								objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongCodPER)
							Case TiposDeDatos.TipoCampoGS.Departamento
								objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongCodDEP)
							Case TiposDeDatos.TipoCampoGS.Activo
								objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongCodACTIVO)
							Case TiposDeDatos.TipoCampoGS.Comprador
								objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongCodCOM)
							Case TiposDeDatos.TipoCampoGS.Proveedor, TiposDeDatos.TipoCampoGS.ProveContacto, TiposDeDatos.TipoCampoGS.ProveedorERP
								objCommand.Parameters.AddWithValue("@MAXLENGTH", FSNServer.LongitudesDeCodigos.giLongCodPROVE)
							Case Else
								objCommand.Parameters.AddWithValue("@MAXLENGTH", colDesglose("MAX_LENGTH"))
						End Select
						objCommand.Parameters.AddWithValue("@OBLIGATORIO", colDesglose("OBLIGATORIO"))
						objCommand.Parameters.AddWithValue("@ANYADIR_ART", 0)
						objCommand.Parameters.AddWithValue("@NOMBRECAMPODESGLOSE", colDesglose("DEN_" & FSNUser.Idioma.ToString))
						objCommand.Parameters.AddWithValue("@PRES5", DBNull.Value)
						If colDesglose("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then
							objCommand.Parameters.AddWithValue("@FORMATO", "m/d/yyyy")
						Else
							objCommand.Parameters.AddWithValue("@FORMATO", DBNull.Value)
						End If
						If DBNullToInteger(colDesglose("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Proveedor Then
							objCommand.Parameters.AddWithValue("@CARGAR_REL_PROVE_ART4", colDesglose("CARGAR_REL_PROVE_ART4"))
						Else
							objCommand.Parameters.AddWithValue("@CARGAR_REL_PROVE_ART4", DBNull.Value)
						End If
						objCommand.Parameters.AddWithValue("@CARGAR_ULT_ADJ", colDesglose("CARGAR_ULT_ADJ"))
						objCommand.Parameters.AddWithValue("@NIVELPRES", DBNull.Value)
						objCommand.ExecuteNonQuery()
						cabeceraLineasValues &= If(String.IsNullOrEmpty(cabeceraLineasValues), "", ",") & "@CAMPO_" & If(String.IsNullOrEmpty(cabeceraLineasValues), 1, Split(cabeceraLineasValues, ",").Length + 1)
					End If
			End Select
		Next
	End Sub
	Private Sub Crear_HojaExcel_ValoresCamposLista(ByVal objCommand As OleDb.OleDbCommand, ByVal dtConfiguracionCampos As DataTable, ByVal dtValoresCamposLista As DataTable)
		Dim dv As New DataView(dtValoresCamposLista)
		Dim createCamposLista As String = String.Empty, insertCamposLista As String = String.Empty
		Dim camposLista As Object = dtConfiguracionCampos.Rows.OfType(Of DataRow).Where(Function(x) x("INTRO") = 1).Select(Function(y) New With {Key .id = y("ID"), .subtipo = y("SUBTIPO")}).ToList()
		If camposLista.Count = 0 Then
			'No hay datos que exportar, pero creamos la hoja para que no casque la macro de excel
			objCommand.CommandText = "CREATE TABLE [DATOSCAMPOSLISTA] (a STRING)"
			objCommand.ExecuteNonQuery()
		Else
			For Each campoLista As Object In camposLista
				createCamposLista &= If(String.IsNullOrEmpty(createCamposLista), "", ",") & "VAL_" & campoLista.id
				Select Case campoLista.subtipo
					Case 2
						createCamposLista &= " FLOAT"
					Case 3
						createCamposLista &= " DATE"
					Case Else
						createCamposLista &= " MEMO"
				End Select
				createCamposLista &= ",ORD_" & campoLista.id & " FLOAT"
				insertCamposLista &= If(String.IsNullOrEmpty(insertCamposLista), "", ",") & "@DENLISTA_" & campoLista.id & ",@VALORLISTA_" & campoLista.id
			Next
			objCommand.CommandText = "CREATE TABLE [DATOSCAMPOSLISTA] (" & createCamposLista & ")"
			objCommand.ExecuteNonQuery()

			Dim lista As DataRow()
			objCommand.CommandText = "INSERT INTO [DATOSCAMPOSLISTA$] VALUES(" & insertCamposLista & ")"
			For Each itemsLista As DataRow In dv.ToTable(True, "ORDEN").Rows
				objCommand.Parameters.Clear()
				For Each campoLista As Object In camposLista
					lista = dtValoresCamposLista.Select("ID=" & campoLista.id & " AND ORDEN=" & itemsLista("ORDEN"))
					If lista.Length = 0 Then
						objCommand.Parameters.AddWithValue("@DENLISTA_" & campoLista.id, DBNull.Value)
						objCommand.Parameters.AddWithValue("@VALORLISTA_" & campoLista.id, DBNull.Value)
					Else
						Select Case campoLista.subtipo
							Case 2
								objCommand.Parameters.AddWithValue("@DENLISTA_" & campoLista.id, lista(0)("VALOR_NUM"))
							Case 3
								objCommand.Parameters.AddWithValue("@DENLISTA_" & campoLista.id, lista(0)("VALOR_FEC"))
							Case Else
								objCommand.Parameters.AddWithValue("@DENLISTA_" & campoLista.id, lista(0)("VALOR_TEXT_" & FSNUser.Idioma.ToString))
						End Select
						objCommand.Parameters.AddWithValue("@VALORLISTA_" & campoLista.id, lista(0)("ORDEN"))
					End If
				Next
				objCommand.ExecuteNonQuery()
			Next
		End If
	End Sub
	Private Sub Cargar_Valores_CamposDesglose(ByVal objCommand As OleDbCommand, ByVal dtConfiguracionCampos As DataTable, ByVal dtLineasDefecto As DataTable,
											 ByRef numMaximoRegistrosCampoGS As Integer, ByRef createCamposGS As String, ByRef insertCamposGS As String)
		'Los datos los posibles valores se exportan a excel por columnas, es decir, cada campo de desglose tendra sus posibles valores en una hoja de excel (DATOSCAMPOSGS) en cada columna
		'Por tanto, para pasar todos los datos de todos los campos habra tantas filas como el numero maximo de registros a traspasar ya que se deberan hacer tantas insert como lineas hay
		For Each itemsGS As DataRow In dtConfiguracionCampos.Rows.OfType(Of DataRow).Where(Function(x) (DBNullToInteger(x("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NumLinea _
																					OrElse (((x("VISIBLE") = 1 AndAlso x("ESCRITURA") = 1 AndAlso x("CARGAR_ULT_ADJ") = 0) _
																								OrElse (x("VISIBLE") = 1 AndAlso x("ESCRITURA") = 1 AndAlso {TiposDeDatos.TipoCampoGS.PrecioUnitario, TiposDeDatos.TipoCampoGS.Proveedor}.Contains(DBNullToInteger(x("TIPO_CAMPO_GS"))))) _
																							AndAlso (Not TiposCampoGSNoTrasladablesExcel.Contains(DBNullToInteger(x("TIPO_CAMPO_GS"))) _
																									 OrElse (DBNullToInteger(x("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.PrecioUnitario _
																											 AndAlso (Not iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoNegociado _
																													  AndAlso Not (iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoExpress _
																															   AndAlso FSNServer.TipoAcceso.gbArticulosNegociadosPedExpress)))))) _
																				   AndAlso TiposCampoGS.Contains(DBNullToInteger(x("TIPO_CAMPO_GS")))).OrderBy(Function(y) y(ordenCampos)).ToList


			Select Case itemsGS("TIPO_CAMPO_GS")
				Case TiposDeDatos.TipoCampoGS.FormaPago
					oFormasPago = FSNServer.Get_Object(GetType(FSNServer.FormasPago))
					oFormasPago.LoadData(FSNUser.Idioma.ToString())
					If oFormasPago.Data.Tables(0).Rows.Count > numMaximoRegistrosCampoGS Then numMaximoRegistrosCampoGS = oFormasPago.Data.Tables(0).Rows.Count
				Case TiposDeDatos.TipoCampoGS.Moneda
					oMonedas = FSNServer.Get_Object(GetType(FSNServer.Monedas))
					oMonedas.LoadData(FSNUser.Idioma.ToString())
					If oMonedas.Data.Tables(0).Rows.Count > numMaximoRegistrosCampoGS Then numMaximoRegistrosCampoGS = oMonedas.Data.Tables(0).Rows.Count
				Case TiposDeDatos.TipoCampoGS.Pais
					If oPaises Is Nothing Then
						oPaises = FSNServer.Get_Object(GetType(FSNServer.Paises))
						oPaises.LoadData(FSNUser.Idioma)
					End If
					If oPaises.Data.Tables(0).Rows.Count > numMaximoRegistrosCampoGS Then numMaximoRegistrosCampoGS = oPaises.Data.Tables(0).Rows.Count
				Case TiposDeDatos.TipoCampoGS.Dest
					oDests = FSNServer.Get_Object(GetType(FSNServer.Destinos))
					oDests.LoadData(FSNUser.Idioma.ToString(), FSNUser.CodPersona)
					If oDests.Data.Tables(0).Rows.Count > numMaximoRegistrosCampoGS Then numMaximoRegistrosCampoGS = oDests.Data.Tables(0).Rows.Count
				Case TiposDeDatos.TipoCampoGS.Almacen
					oAlmacenes = FSNServer.Get_Object(GetType(FSNServer.Almacenes))
					oAlmacenes.LoadData(If(String.IsNullOrEmpty(centroOutDesglose), Nothing, centroOutDesglose), If(Not String.IsNullOrEmpty(destinoOutDesglose) AndAlso Not tieneCentroDesglose, destinoOutDesglose, Nothing))
					If oAlmacenes.Data.Tables(0).Rows.Count > numMaximoRegistrosCampoGS Then numMaximoRegistrosCampoGS = oAlmacenes.Data.Tables(0).Rows.Count
				Case TiposDeDatos.TipoCampoGS.Unidad, TiposDeDatos.TipoCampoGS.UnidadPedido
					If oUnidades Is Nothing Then
						oUnidades = FSNServer.Get_Object(GetType(FSNServer.Unidades))
						oUnidades.LoadData(FSNUser.Idioma.ToString())
						If oUnidades.Data.Tables(0).Rows.Count > numMaximoRegistrosCampoGS Then numMaximoRegistrosCampoGS = oUnidades.Data.Tables(0).Rows.Count
					End If
				Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
					If oOrganizacionesCompras Is Nothing Then 'cargamos solo la primera vez
						oOrganizacionesCompras = FSNServer.Get_Object(GetType(FSNServer.OrganizacionesCompras))
						oOrganizacionesCompras.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil)
					End If
			End Select
			If Not itemsGS("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Provincia AndAlso Not itemsGS("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.AnyoPartida Then
				createCamposGS &= If(String.IsNullOrEmpty(createCamposGS), "", ",") & "DEN_" & itemsGS("id") & " STRING,VAL_" & itemsGS("id") & " STRING"
				insertCamposGS &= If(String.IsNullOrEmpty(insertCamposGS), "", ",") & "@DENCAMPOGS_" & itemsGS("id") & ",@VALORCAMPOGS_" & itemsGS("id")
			End If
		Next
	End Sub
	Private Sub Crear_HojaExcel_CamposGS(ByVal objCommand As OleDb.OleDbCommand, ByVal dtConfiguracionCampos As DataTable, ByVal TiposCampoGS As TiposDeDatos.TipoCampoGS(),
											 ByVal numMaximoRegistrosCampoGS As Integer, ByVal createCamposGS As String, ByVal insertCamposGS As String)
		Dim s As String = ""

		objCommand.CommandText = "CREATE TABLE [DATOSCAMPOSGS] (" & If(String.IsNullOrEmpty(createCamposGS), "a string", createCamposGS) & ")"
		objCommand.ExecuteNonQuery()
		'Las insert son filas de excel. Cada campo es el valor de un campo de desglose diferente: valor1Campo1,valor1Campo2,valor1Campo3..... por tanto haremos tantas insert como el numero maximos de valores de un campo
		For i As Integer = 0 To numMaximoRegistrosCampoGS - 1
			objCommand.Parameters.Clear()
			For Each itemsGS As DataRow In dtConfiguracionCampos.Rows.OfType(Of DataRow).Where(Function(x) (DBNullToInteger(x("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NumLinea _
																					OrElse (((x("VISIBLE") = 1 AndAlso x("ESCRITURA") = 1 AndAlso x("CARGAR_ULT_ADJ") = 0) _
																								OrElse (x("VISIBLE") = 1 AndAlso x("ESCRITURA") = 1 AndAlso {TiposDeDatos.TipoCampoGS.PrecioUnitario, TiposDeDatos.TipoCampoGS.Proveedor}.Contains(DBNullToInteger(x("TIPO_CAMPO_GS"))))) _
																							AndAlso (Not TiposCampoGSNoTrasladablesExcel.Contains(DBNullToInteger(x("TIPO_CAMPO_GS"))) _
																									 OrElse (DBNullToInteger(x("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.PrecioUnitario _
																											 AndAlso (Not iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoNegociado _
																													  AndAlso Not (iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoExpress _
																															   AndAlso FSNServer.TipoAcceso.gbArticulosNegociadosPedExpress)))))) _
																					AndAlso TiposCampoGS.Contains(DBNullToInteger(x("TIPO_CAMPO_GS")))).OrderBy(Function(y) y(ordenCampos)).ToList
				Select Case itemsGS("TIPO_CAMPO_GS")
					Case TiposDeDatos.TipoCampoGS.FormaPago
						objCommand.Parameters.AddWithValue("@DENCAMPOGS_" & itemsGS("ID"), If(oFormasPago.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value, oFormasPago.Data.Tables(0).Rows(i)("COD") & " - " & oFormasPago.Data.Tables(0).Rows(i)("DEN")))
						objCommand.Parameters.AddWithValue("@VALORCAMPOGS_" & itemsGS("ID"), If(oFormasPago.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value, oFormasPago.Data.Tables(0).Rows(i)("COD")))
					Case TiposDeDatos.TipoCampoGS.Moneda
						objCommand.Parameters.AddWithValue("@DENCAMPOGS_" & itemsGS("ID"), If(oMonedas.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value, oMonedas.Data.Tables(0).Rows(i)("COD") & " - " & oMonedas.Data.Tables(0).Rows(i)("DEN")))
						objCommand.Parameters.AddWithValue("@VALORCAMPOGS_" & itemsGS("ID"), If(oMonedas.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value, oMonedas.Data.Tables(0).Rows(i)("COD")))
					Case TiposDeDatos.TipoCampoGS.Unidad, TiposDeDatos.TipoCampoGS.UnidadPedido
						objCommand.Parameters.AddWithValue("@DENCAMPOGS_" & itemsGS("ID"), If(oUnidades.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value, oUnidades.Data.Tables(0).Rows(i)("COD") & " - " & oUnidades.Data.Tables(0).Rows(i)("DEN")))
						objCommand.Parameters.AddWithValue("@VALORCAMPOGS_" & itemsGS("ID"), If(oUnidades.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value, oUnidades.Data.Tables(0).Rows(i)("COD")))
					Case TiposDeDatos.TipoCampoGS.Pais
						objCommand.Parameters.AddWithValue("@DENCAMPOGS_" & itemsGS("ID"), If(oPaises.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value, oPaises.Data.Tables(0).Rows(i)("COD") & " - " & oPaises.Data.Tables(0).Rows(i)("DEN")))
						objCommand.Parameters.AddWithValue("@VALORCAMPOGS_" & itemsGS("ID"), If(oPaises.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value, oPaises.Data.Tables(0).Rows(i)("COD")))
					Case TiposDeDatos.TipoCampoGS.Dest
						objCommand.Parameters.AddWithValue("@DENCAMPOGS_" & itemsGS("ID"), If(oDests.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value, oDests.Data.Tables(0).Rows(i)("COD") & " - " & oDests.Data.Tables(0).Rows(i)("DEN")))
						objCommand.Parameters.AddWithValue("@VALORCAMPOGS_" & itemsGS("ID"), If(oDests.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value, oDests.Data.Tables(0).Rows(i)("COD")))
					Case TiposDeDatos.TipoCampoGS.Almacen
						objCommand.Parameters.AddWithValue("@DENCAMPOGS_" & itemsGS("ID"), If(oAlmacenes.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value, oAlmacenes.Data.Tables(0).Rows(i)("COD") & " - " & oAlmacenes.Data.Tables(0).Rows(i)("DEN")))
						objCommand.Parameters.AddWithValue("@VALORCAMPOGS_" & itemsGS("ID"), If(oAlmacenes.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value, oAlmacenes.Data.Tables(0).Rows(i)("ID")))
					Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
						objCommand.Parameters.AddWithValue("@DENCAMPOGS_" & itemsGS("ID"), If(oOrganizacionesCompras.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value, oOrganizacionesCompras.Data.Tables(0).Rows(i)("CODDEN")))
						objCommand.Parameters.AddWithValue("@VALORCAMPOGS_" & itemsGS("ID"), If(oOrganizacionesCompras.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value, oOrganizacionesCompras.Data.Tables(0).Rows(i)("COD")))
					Case TiposDeDatos.TipoCampoGS.PRES1
						If Not (oPresupsFavoritos1.Data.Tables(0).Rows.Count - 1 < i) Then
							For niv As Integer = 1 To oPresupsFavoritos1.Data.Tables(0).Rows(i)("NIVEL")
								If niv = 1 Then
									s = oPresupsFavoritos1.Data.Tables(0).Rows(i)("PRES" & CStr(niv))
								Else
									s = s & " / " & oPresupsFavoritos1.Data.Tables(0).Rows(i)("PRES" & CStr(niv))
								End If
							Next
						End If
						objCommand.Parameters.AddWithValue("@DENCAMPOGS_" & itemsGS("ID"), If(oPresupsFavoritos1.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value,
																							  oPresupsFavoritos1.Data.Tables(0).Rows(i)("ANYO") & " - " & s))
						objCommand.Parameters.AddWithValue("@VALORCAMPOGS_" & itemsGS("ID"), If(oPresupsFavoritos1.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value,
																								oPresupsFavoritos1.Data.Tables(0).Rows(i)("NIVEL") & "_" & oPresupsFavoritos1.Data.Tables(0).Rows(i)("PRESID") & "_1"))
					Case TiposDeDatos.TipoCampoGS.Pres2
						If Not (oPresupsFavoritos2.Data.Tables(0).Rows.Count - 1 < i) Then
							For niv As Integer = 1 To oPresupsFavoritos2.Data.Tables(0).Rows(i)("NIVEL")
								If niv = 1 Then
									s = oPresupsFavoritos2.Data.Tables(0).Rows(i)("PRES" & CStr(niv))
								Else
									s = s & " / " & oPresupsFavoritos2.Data.Tables(0).Rows(i)("PRES" & CStr(niv))
								End If
							Next
						End If
						objCommand.Parameters.AddWithValue("@DENCAMPOGS_" & itemsGS("ID"), If(oPresupsFavoritos2.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value,
																							  oPresupsFavoritos2.Data.Tables(0).Rows(i)("ANYO") & " - " & s))
						objCommand.Parameters.AddWithValue("@VALORCAMPOGS_" & itemsGS("ID"), If(oPresupsFavoritos2.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value,
																								oPresupsFavoritos2.Data.Tables(0).Rows(i)("NIVEL") & "_" & oPresupsFavoritos2.Data.Tables(0).Rows(i)("PRESID") & "_1"))
					Case TiposDeDatos.TipoCampoGS.Pres3
						If Not (oPresupsFavoritos3.Data.Tables(0).Rows.Count - 1 < i) Then
							For niv As Integer = 1 To oPresupsFavoritos3.Data.Tables(0).Rows(i)("NIVEL")
								If niv = 1 Then
									s = oPresupsFavoritos3.Data.Tables(0).Rows(i)("PRES" & CStr(niv))
								Else
									s = s & " / " & oPresupsFavoritos3.Data.Tables(0).Rows(i)("PRES" & CStr(niv))
								End If
							Next
						End If
						objCommand.Parameters.AddWithValue("@DENCAMPOGS_" & itemsGS("ID"), If(oPresupsFavoritos3.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value, s))
						objCommand.Parameters.AddWithValue("@VALORCAMPOGS_" & itemsGS("ID"), If(oPresupsFavoritos3.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value,
																								oPresupsFavoritos3.Data.Tables(0).Rows(i)("NIVEL") & "_" & oPresupsFavoritos3.Data.Tables(0).Rows(i)("PRESID") & "_1"))
					Case TiposDeDatos.TipoCampoGS.Pres4
						If Not (oPresupsFavoritos4.Data.Tables(0).Rows.Count - 1 < i) Then
							For niv As Integer = 1 To oPresupsFavoritos4.Data.Tables(0).Rows(i)("NIVEL")
								If niv = 1 Then
									s = oPresupsFavoritos4.Data.Tables(0).Rows(i)("PRES" & CStr(niv))
								Else
									s = s & " / " & oPresupsFavoritos4.Data.Tables(0).Rows(i)("PRES" & CStr(niv))
								End If
							Next
						End If
						objCommand.Parameters.AddWithValue("@DENCAMPOGS_" & itemsGS("ID"), If(oPresupsFavoritos4.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value, s))
						objCommand.Parameters.AddWithValue("@VALORCAMPOGS_" & itemsGS("ID"), If(oPresupsFavoritos4.Data.Tables(0).Rows.Count - 1 < i, DBNull.Value,
																								oPresupsFavoritos4.Data.Tables(0).Rows(i)("NIVEL") & "_" & oPresupsFavoritos4.Data.Tables(0).Rows(i)("PRESID") & "_1"))
				End Select
			Next
			objCommand.CommandText = "INSERT INTO [DATOSCAMPOSGS$] VALUES(" & insertCamposGS & ")"
			objCommand.ExecuteNonQuery()
		Next
	End Sub
	Private Sub Crear_HojaExcel_Textos(ByVal objCommand As OleDb.OleDbCommand, ByVal Textos As DataTable)
		'PASAMOS LOS TEXTOS NECESARIOS (VALIDACIONES)
		objCommand.CommandText = "CREATE TABLE [TEXTOS] (ID INT,TEXTO MEMO)"
		objCommand.ExecuteNonQuery()

		'Pasamos el id de solicitud porque no dejaremos cargar excel de otras solicitudes
		objCommand.CommandText = "INSERT INTO [TEXTOS$] VALUES(-10,'" & idSolicitudExcel & "')"
		objCommand.ExecuteNonQuery()
        'Pasaremos a esta pestaña valores que necesitaremos para hacer despues validaciones
        '-4 código de la unidad organizativa que está fuera del desglose
        '-3 codigo de destino de la solicitud que esta fuera del desglose (si hay otro dentro cogeremos ese)
        '-2 codigo de centro de la solicitud que esta fuera del desglose (si hay otro dentro cogeremos ese)
        '-1 codigo de la organizacion de compras de la solicitud que esta fuera del desglose (si hay otro dentro cogeremos ese)
        '0 tipo de solicitud
        objCommand.CommandText = "INSERT INTO [TEXTOS$] VALUES(39,'" & Replace(Textos(38)(1), "'", "''") & "')"
        objCommand.ExecuteNonQuery()
        objCommand.CommandText = "INSERT INTO [TEXTOS$] VALUES(40,'" & Replace(Textos(39)(1), "'", "''") & "')"
        objCommand.ExecuteNonQuery()
        objCommand.CommandText = "INSERT INTO [TEXTOS$] VALUES(3000,'" & numeroLineas & "')"
        objCommand.ExecuteNonQuery()
        objCommand.CommandText = "INSERT INTO [TEXTOS$] VALUES(-4,'" & UnidadOrganizativaOutDesglose & "')"
		objCommand.ExecuteNonQuery()
		objCommand.CommandText = "INSERT INTO [TEXTOS$] VALUES(-3,'" & destinoOutDesglose & "')"
		objCommand.ExecuteNonQuery()
		objCommand.CommandText = "INSERT INTO [TEXTOS$] VALUES(-2,'" & centroOutDesglose & "')"
		objCommand.ExecuteNonQuery()
		objCommand.CommandText = "INSERT INTO [TEXTOS$] VALUES(-1,'" & orgComprasOutDesglose & "')"
		objCommand.ExecuteNonQuery()
		objCommand.CommandText = "INSERT INTO [TEXTOS$] VALUES(0,'" & iTipoSolicitud & "')"
		objCommand.ExecuteNonQuery()

		'Si
		objCommand.CommandText = "INSERT INTO [TEXTOS$] VALUES(16,'" & Textos(15)(1) & "')"
		objCommand.ExecuteNonQuery()
		'No
		objCommand.CommandText = "INSERT INTO [TEXTOS$] VALUES(17,'" & Textos(16)(1) & "')"
		objCommand.ExecuteNonQuery()

		For i As Integer = 31 To Textos.Rows.Count - 1
			objCommand.CommandText = "INSERT INTO [TEXTOS$] VALUES(" & i + 1 & ",'" & Replace(Textos(i)(1), "'", "''") & "')"
			objCommand.ExecuteNonQuery()
		Next

        oPres = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
		nivelPres1 = oPres.GetNivelPres(1)
		Dim s As String = Textos(46)(1) & " - " & Textos(58)(1) & "1"
		For i As Integer = 2 To nivelPres1
			s = s & " / " & Textos(58)(1) & CStr(i)
		Next
		objCommand.CommandText = "INSERT INTO [TEXTOS$] VALUES(2003,'" & Replace(Textos(57)(1) & s, "'", "''") & "')"
		objCommand.ExecuteNonQuery()

		nivelPres2 = oPres.GetNivelPres(2)
		s = Textos(46)(1) & " - " & Textos(58)(1) & "1"
		For i As Integer = 2 To nivelPres2
			s = s & " / " & Textos(58)(1) & CStr(i)
		Next
		objCommand.CommandText = "INSERT INTO [TEXTOS$] VALUES(2002,'" & Replace(Textos(57)(1) & s, "'", "''") & "')"
		objCommand.ExecuteNonQuery()

		nivelPres3 = oPres.GetNivelPres(3)
		s = Textos(58)(1) & "1"
		For i As Integer = 2 To nivelPres3
			s = s & " / " & Textos(58)(1) & CStr(i)
		Next
		objCommand.CommandText = "INSERT INTO [TEXTOS$] VALUES(2001,'" & Replace(Textos(57)(1) & s, "'", "''") & "')"
		objCommand.ExecuteNonQuery()

		nivelPres4 = oPres.GetNivelPres(4)
		s = Textos(58)(1) & "1"
		For i As Integer = 2 To nivelPres4
			s = s & " / " & Textos(58)(1) & CStr(i)
		Next
		objCommand.CommandText = "INSERT INTO [TEXTOS$] VALUES(2000,'" & Replace(Textos(57)(1) & s, "'", "''") & "')"
		objCommand.ExecuteNonQuery()

    End Sub
	Private Sub Crear_HojaExcel_Lineas(ByVal objCommand As OleDb.OleDbCommand, ByVal cabeceraLineas As String, ByVal cabeceraLineasValues As String, ByVal dtLineasDefecto As DataTable, ByVal dtConfiguracionCampos As DataTable,
										   ByVal TextoSi As String, ByVal TextoNo As String, ByVal favorito As Boolean, ByVal NivelPartida As Integer)
		Dim nivelUO As Integer
		Dim TextPres As String
		numeroLineas = dtLineasDefecto.AsEnumerable.Max(Function(linea) linea("LINEA"))

		'Creamos la hoja de lineas con las lineas ya existentes de haberlas
		objCommand.CommandText = "CREATE TABLE [LINEASDESGLOSE] (" & cabeceraLineas & ")"
		objCommand.ExecuteNonQuery()

		Dim lineaExistente, pais, provincia, formaPago, moneda, unidad, destino, almacen, pres As DataRow
		Dim centroCoste As String
		Dim oProvincias As FSNServer.Provincias
		Dim dtCamposTraspasar As DataTable = Nothing
		pais = Nothing
		If numeroLineas > 0 Then
			objCommand.CommandText = "INSERT INTO [LINEASDESGLOSE$] VALUES(" & cabeceraLineasValues & ")"
			For numeroLinea As Integer = 1 To numeroLineas
				Dim numLinea As Integer = numeroLinea
				objCommand.Parameters.Clear()
				dtCamposTraspasar = dtConfiguracionCampos.Rows.OfType(Of DataRow).Where(Function(x) DBNullToInteger(x("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NumLinea _
																					OrElse (((x("VISIBLE") = 1 AndAlso x("ESCRITURA") = 1 AndAlso x("CARGAR_ULT_ADJ") = 0) _
																									OrElse (x("VISIBLE") = 1 AndAlso x("ESCRITURA") = 1 AndAlso {TiposDeDatos.TipoCampoGS.PrecioUnitario, TiposDeDatos.TipoCampoGS.Proveedor, TiposDeDatos.TipoCampoGS.PRES1, TiposDeDatos.TipoCampoGS.Pres2, TiposDeDatos.TipoCampoGS.Pres3, TiposDeDatos.TipoCampoGS.Pres4}.Contains(DBNullToInteger(x("TIPO_CAMPO_GS"))))) _
																							AndAlso (Not TiposCampoGSNoTrasladablesExcel.Contains(DBNullToInteger(x("TIPO_CAMPO_GS"))) _
																									 OrElse (DBNullToInteger(x("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.PrecioUnitario _
																											 AndAlso (Not iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoNegociado _
																														  AndAlso Not (iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoExpress _
																																   AndAlso FSNServer.TipoAcceso.gbArticulosNegociadosPedExpress)))))).OrderBy(Function(y) y(ordenCampos)).CopyToDataTable
				For Each campoLinea As DataRow In dtCamposTraspasar.Rows
					If dtLineasDefecto.Rows.OfType(Of DataRow).Where(Function(x) x("LINEA") = numLinea AndAlso x("CAMPO_HIJO") = campoLinea("ID")).Count = 0 Then
						lineaExistente = Nothing
					Else
						lineaExistente = dtLineasDefecto.Rows.OfType(Of DataRow).Where(Function(x) x("LINEA") = numLinea AndAlso x("CAMPO_HIJO") = campoLinea("ID")).First
					End If

					If lineaExistente IsNot Nothing AndAlso (Not String.IsNullOrEmpty(lineaExistente("VALOR_TEXT").ToString) OrElse Not String.IsNullOrEmpty(lineaExistente("VALOR_NUM").ToString) _
							OrElse Not String.IsNullOrEmpty(lineaExistente("VALOR_BOOL").ToString) OrElse Not String.IsNullOrEmpty(lineaExistente("VALOR_FEC").ToString)) Then
						Select Case DBNullToInteger(lineaExistente("TIPO_CAMPO_GS"))
							Case TiposDeDatos.TipoCampoGS.Material
								'Para indicar el material pedimos 3 datos GMN1,GMN2 y GMN3
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, lineaExistente("VALOR_TEXT").ToString.Substring(0, FSNServer.LongitudesDeCodigos.giLongCodGMN1))
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, lineaExistente("VALOR_TEXT").ToString.Substring(FSNServer.LongitudesDeCodigos.giLongCodGMN1, FSNServer.LongitudesDeCodigos.giLongCodGMN2))
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count,
																	lineaExistente("VALOR_TEXT").ToString.Substring(FSNServer.LongitudesDeCodigos.giLongCodGMN1 + FSNServer.LongitudesDeCodigos.giLongCodGMN2, FSNServer.LongitudesDeCodigos.giLongCodGMN3))
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count,
										lineaExistente("VALOR_TEXT").ToString.Substring(FSNServer.LongitudesDeCodigos.giLongCodGMN1 + FSNServer.LongitudesDeCodigos.giLongCodGMN2 + FSNServer.LongitudesDeCodigos.giLongCodGMN3, FSNServer.LongitudesDeCodigos.giLongCodGMN4))
							Case TiposDeDatos.TipoCampoGS.UnidadOrganizativa
								'Para indicar la unidad organizativa pedimos 3 datos UO1,UO2 y UO3
								nivelUO = Split(lineaExistente("VALOR_TEXT").ToString, " - ").Length
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, Split(lineaExistente("VALOR_TEXT").ToString, " - ")(0))
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, If(Split(lineaExistente("VALOR_TEXT").ToString, " - ").Length < 2, DBNull.Value, Split(lineaExistente("VALOR_TEXT").ToString, " - ")(1)))
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, If(Split(lineaExistente("VALOR_TEXT").ToString, " - ").Length < 3, DBNull.Value, Split(lineaExistente("VALOR_TEXT").ToString, " - ")(2)))
							Case TiposDeDatos.TipoCampoGS.CentroCoste
								'Para indicar el centro de coste pedimos 3 datos UO1,UO2,UO3 y UO4
								centroCoste = lineaExistente("VALOR_TEXT").ToString & "###"
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, Split(centroCoste, "#")(0))
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, Split(centroCoste, "#")(1))
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, Split(centroCoste, "#")(2))
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, Split(centroCoste, "#")(3))
							Case TiposDeDatos.TipoCampoGS.PRES1
								If oPresupsFavoritos1 Is Nothing Then
									oPresupsFavoritos1 = FSNServer.Get_Object(GetType(FSNServer.PresupuestosFavoritos))
									oPresupsFavoritos1.LoadData(FSNUser.Cod, 1)
								End If
								If oPresupsFavoritos1.Data.Tables(0).Select("NIVEL=" & Split(lineaExistente("VALOR_TEXT").ToString, "_")(0) & " AND PRESID=" & Split(lineaExistente("VALOR_TEXT").ToString, "_")(1)).Count = 1 Then
									pres = oPresupsFavoritos1.Data.Tables(0).Select("NIVEL=" & Split(lineaExistente("VALOR_TEXT").ToString, "_")(0) & " AND PRESID=" & Split(lineaExistente("VALOR_TEXT").ToString, "_")(1)).First()
									TextPres = pres("ANYO") & " - " & pres("PRES1")
									If pres("NIVEL") > 1 Then TextPres = TextPres & " / " & pres("PRES2")
									If pres("NIVEL") > 2 Then TextPres = TextPres & " / " & pres("PRES3")
									If pres("NIVEL") > 3 Then TextPres = TextPres & " / " & pres("PRES4")
								Else
									Dim pres1NewRow As DataRow = oPresupsFavoritos1.Data.Tables(0).NewRow
									Dim oPres1 As FSNServer.PresProyectosNivel1 = FSNServer.Get_Object(GetType(FSNServer.PresProyectosNivel1))
									Select Case Split(lineaExistente("VALOR_TEXT").ToString, "_")(0)
										Case 1
											oPres1.LoadData(Split(lineaExistente("VALOR_TEXT").ToString, "_")(1))
											pres1NewRow("PRES1") = oPres1.Data.Tables(0).Rows(0)("PRES1")
										Case 2
											oPres1.LoadData(Nothing, Split(lineaExistente("VALOR_TEXT").ToString, "_")(1))
											pres1NewRow("PRES1") = oPres1.Data.Tables(0).Rows(0)("PRES1")
											pres1NewRow("PRES2") = oPres1.Data.Tables(0).Rows(0)("PRES2")
										Case 3
											oPres1.LoadData(Nothing, Nothing, Split(lineaExistente("VALOR_TEXT").ToString, "_")(1))
											pres1NewRow("PRES1") = oPres1.Data.Tables(0).Rows(0)("PRES1")
											pres1NewRow("PRES2") = oPres1.Data.Tables(0).Rows(0)("PRES2")
											pres1NewRow("PRES3") = oPres1.Data.Tables(0).Rows(0)("PRES3")
										Case 4
											oPres1.LoadData(Nothing, Nothing, Nothing, Split(lineaExistente("VALOR_TEXT").ToString, "_")(1))
											pres1NewRow("PRES1") = oPres1.Data.Tables(0).Rows(0)("PRES1")
											pres1NewRow("PRES2") = oPres1.Data.Tables(0).Rows(0)("PRES2")
											pres1NewRow("PRES3") = oPres1.Data.Tables(0).Rows(0)("PRES3")
											pres1NewRow("PRES4") = oPres1.Data.Tables(0).Rows(0)("PRES4")
									End Select
									pres1NewRow("ANYO") = oPres1.Data.Tables(0).Rows(0)("ANYO")
									pres1NewRow("NIVEL") = Split(lineaExistente("VALOR_TEXT").ToString, "_")(0)
									pres1NewRow("PRESID") = Split(lineaExistente("VALOR_TEXT").ToString, "_")(1)

									TextPres = pres1NewRow("ANYO") & " - " & pres1NewRow("PRES1")
									If pres1NewRow("NIVEL") > 1 Then TextPres = TextPres & " / " & pres1NewRow("PRES2")
									If pres1NewRow("NIVEL") > 2 Then TextPres = TextPres & " / " & pres1NewRow("PRES3")
									If pres1NewRow("NIVEL") > 3 Then TextPres = TextPres & " / " & pres1NewRow("PRES4")
								End If
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, TextPres)
							Case TiposDeDatos.TipoCampoGS.Pres2
								If oPresupsFavoritos2 Is Nothing Then
									oPresupsFavoritos2 = FSNServer.Get_Object(GetType(FSNServer.PresupuestosFavoritos))
									oPresupsFavoritos2.LoadData(FSNUser.Cod, 2)
								End If
								If oPresupsFavoritos2.Data.Tables(0).Select("NIVEL=" & Split(lineaExistente("VALOR_TEXT").ToString, "_")(0) & " AND PRESID=" & Split(lineaExistente("VALOR_TEXT").ToString, "_")(1)).Count = 1 Then
									pres = oPresupsFavoritos2.Data.Tables(0).Select("NIVEL=" & Split(lineaExistente("VALOR_TEXT").ToString, "_")(0) & " AND PRESID=" & Split(lineaExistente("VALOR_TEXT").ToString, "_")(1)).First()
									TextPres = pres("ANYO") & " - " & pres("PRES1")
									If pres("NIVEL") > 1 Then TextPres = TextPres & " / " & pres("PRES2")
									If pres("NIVEL") > 2 Then TextPres = TextPres & " / " & pres("PRES3")
									If pres("NIVEL") > 3 Then TextPres = TextPres & " / " & pres("PRES4")
								Else
									Dim pres2NewRow As DataRow = oPresupsFavoritos2.Data.Tables(0).NewRow
									Dim oPres2 As FSNServer.PresContablesNivel1 = FSNServer.Get_Object(GetType(FSNServer.PresContablesNivel1))
									Select Case Split(lineaExistente("VALOR_TEXT").ToString, "_")(0)
										Case 1
											oPres2.LoadData(Split(lineaExistente("VALOR_TEXT").ToString, "_")(1))
											pres2NewRow("PRES1") = oPres2.Data.Tables(0).Rows(0)("PRES1")
										Case 2
											oPres2.LoadData(Nothing, Split(lineaExistente("VALOR_TEXT").ToString, "_")(1))
											pres2NewRow("PRES1") = oPres2.Data.Tables(0).Rows(0)("PRES1")
											pres2NewRow("PRES2") = oPres2.Data.Tables(0).Rows(0)("PRES2")
										Case 3
											oPres2.LoadData(Nothing, Nothing, Split(lineaExistente("VALOR_TEXT").ToString, "_")(1))
											pres2NewRow("PRES1") = oPres2.Data.Tables(0).Rows(0)("PRES1")
											pres2NewRow("PRES2") = oPres2.Data.Tables(0).Rows(0)("PRES2")
											pres2NewRow("PRES3") = oPres2.Data.Tables(0).Rows(0)("PRES3")
										Case 4
											oPres2.LoadData(Nothing, Nothing, Nothing, Split(lineaExistente("VALOR_TEXT").ToString, "_")(1))
											pres2NewRow("PRES1") = oPres2.Data.Tables(0).Rows(0)("PRES1")
											pres2NewRow("PRES2") = oPres2.Data.Tables(0).Rows(0)("PRES2")
											pres2NewRow("PRES3") = oPres2.Data.Tables(0).Rows(0)("PRES3")
											pres2NewRow("PRES4") = oPres2.Data.Tables(0).Rows(0)("PRES4")
									End Select
									pres2NewRow("ANYO") = oPres2.Data.Tables(0).Rows(0)("ANYO")
									pres2NewRow("NIVEL") = Split(lineaExistente("VALOR_TEXT").ToString, "_")(0)
									pres2NewRow("PRESID") = Split(lineaExistente("VALOR_TEXT").ToString, "_")(1)

									TextPres = pres2NewRow("ANYO") & " - " & pres2NewRow("PRES1")
									If pres2NewRow("NIVEL") > 1 Then TextPres = TextPres & " / " & pres2NewRow("PRES2")
									If pres2NewRow("NIVEL") > 2 Then TextPres = TextPres & " / " & pres2NewRow("PRES3")
									If pres2NewRow("NIVEL") > 3 Then TextPres = TextPres & " / " & pres2NewRow("PRES4")
								End If
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, TextPres)
							Case TiposDeDatos.TipoCampoGS.Pres3
								If oPresupsFavoritos3 Is Nothing Then
									oPresupsFavoritos3 = FSNServer.Get_Object(GetType(FSNServer.PresupuestosFavoritos))
									oPresupsFavoritos3.LoadData(FSNUser.Cod, 3)
								End If
								If oPresupsFavoritos3.Data.Tables(0).Select("NIVEL=" & Split(lineaExistente("VALOR_TEXT").ToString, "_")(0) & " AND PRESID=" & Split(lineaExistente("VALOR_TEXT").ToString, "_")(1)).Count = 1 Then
									pres = oPresupsFavoritos3.Data.Tables(0).Select("NIVEL=" & Split(lineaExistente("VALOR_TEXT").ToString, "_")(0) & " AND PRESID=" & Split(lineaExistente("VALOR_TEXT").ToString, "_")(1)).First()
									TextPres = pres("PRES1")
									If pres("NIVEL") > 1 Then TextPres = TextPres & " / " & pres("PRES2")
									If pres("NIVEL") > 2 Then TextPres = TextPres & " / " & pres("PRES3")
									If pres("NIVEL") > 3 Then TextPres = TextPres & " / " & pres("PRES4")
								Else
									Dim pres3NewRow As DataRow = oPresupsFavoritos3.Data.Tables(0).NewRow
									Dim oPres3 As FSNServer.PresConceptos3Nivel1 = FSNServer.Get_Object(GetType(FSNServer.PresConceptos3Nivel1))
									Select Case Split(lineaExistente("VALOR_TEXT").ToString, "_")(0)
										Case 1
											oPres3.LoadData(Split(lineaExistente("VALOR_TEXT").ToString, "_")(1))
											pres3NewRow("PRES1") = oPres3.Data.Tables(0).Rows(0)("PRES1")
										Case 2
											oPres3.LoadData(Nothing, Split(lineaExistente("VALOR_TEXT").ToString, "_")(1))
											pres3NewRow("PRES1") = oPres3.Data.Tables(0).Rows(0)("PRES1")
											pres3NewRow("PRES2") = oPres3.Data.Tables(0).Rows(0)("PRES2")
										Case 3
											oPres3.LoadData(Nothing, Nothing, Split(lineaExistente("VALOR_TEXT").ToString, "_")(1))
											pres3NewRow("PRES1") = oPres3.Data.Tables(0).Rows(0)("PRES1")
											pres3NewRow("PRES2") = oPres3.Data.Tables(0).Rows(0)("PRES2")
											pres3NewRow("PRES3") = oPres3.Data.Tables(0).Rows(0)("PRES3")
										Case 4
											oPres3.LoadData(Nothing, Nothing, Nothing, Split(lineaExistente("VALOR_TEXT").ToString, "_")(1))
											pres3NewRow("PRES1") = oPres3.Data.Tables(0).Rows(0)("PRES1")
											pres3NewRow("PRES2") = oPres3.Data.Tables(0).Rows(0)("PRES2")
											pres3NewRow("PRES3") = oPres3.Data.Tables(0).Rows(0)("PRES3")
											pres3NewRow("PRES4") = oPres3.Data.Tables(0).Rows(0)("PRES4")
									End Select
									pres3NewRow("NIVEL") = Split(lineaExistente("VALOR_TEXT").ToString, "_")(0)
									pres3NewRow("PRESID") = Split(lineaExistente("VALOR_TEXT").ToString, "_")(1)

									TextPres = pres3NewRow("PRES1")
									If pres3NewRow("NIVEL") > 1 Then TextPres = TextPres & " / " & pres3NewRow("PRES2")
									If pres3NewRow("NIVEL") > 2 Then TextPres = TextPres & " / " & pres3NewRow("PRES3")
									If pres3NewRow("NIVEL") > 3 Then TextPres = TextPres & " / " & pres3NewRow("PRES4")
								End If
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, TextPres)
							Case TiposDeDatos.TipoCampoGS.Pres4
								If oPresupsFavoritos4 Is Nothing Then
									oPresupsFavoritos4 = FSNServer.Get_Object(GetType(FSNServer.PresupuestosFavoritos))
									oPresupsFavoritos4.LoadData(FSNUser.Cod, 4)
								End If
								If oPresupsFavoritos4.Data.Tables(0).Select("NIVEL=" & Split(lineaExistente("VALOR_TEXT").ToString, "_")(0) & " AND PRESID=" & Split(lineaExistente("VALOR_TEXT").ToString, "_")(1)).Count = 1 Then
									pres = oPresupsFavoritos4.Data.Tables(0).Select("NIVEL=" & Split(lineaExistente("VALOR_TEXT").ToString, "_")(0) & " AND PRESID=" & Split(lineaExistente("VALOR_TEXT").ToString, "_")(1)).First()
									TextPres = pres("PRES1")
									If pres("NIVEL") > 1 Then TextPres = TextPres & " / " & pres("PRES2")
									If pres("NIVEL") > 2 Then TextPres = TextPres & " / " & pres("PRES3")
									If pres("NIVEL") > 3 Then TextPres = TextPres & " / " & pres("PRES4")
								Else
									Dim pres4NewRow As DataRow = oPresupsFavoritos4.Data.Tables(0).NewRow
									Dim oPres4 As FSNServer.PresConceptos4Nivel1 = FSNServer.Get_Object(GetType(FSNServer.PresConceptos4Nivel1))
									Select Case Split(lineaExistente("VALOR_TEXT").ToString, "_")(0)
										Case 1
											oPres4.LoadData(Split(lineaExistente("VALOR_TEXT").ToString, "_")(1))
											pres4NewRow("PRES1") = oPres4.Data.Tables(0).Rows(0)("PRES1")
										Case 2
											oPres4.LoadData(Nothing, Split(lineaExistente("VALOR_TEXT").ToString, "_")(1))
											pres4NewRow("PRES1") = oPres4.Data.Tables(0).Rows(0)("PRES1")
											pres4NewRow("PRES2") = oPres4.Data.Tables(0).Rows(0)("PRES2")
										Case 3
											oPres4.LoadData(Nothing, Nothing, Split(lineaExistente("VALOR_TEXT").ToString, "_")(1))
											pres4NewRow("PRES1") = oPres4.Data.Tables(0).Rows(0)("PRES1")
											pres4NewRow("PRES2") = oPres4.Data.Tables(0).Rows(0)("PRES2")
											pres4NewRow("PRES3") = oPres4.Data.Tables(0).Rows(0)("PRES3")
										Case 4
											oPres4.LoadData(Nothing, Nothing, Nothing, Split(lineaExistente("VALOR_TEXT").ToString, "_")(1))
											pres4NewRow("PRES1") = oPres4.Data.Tables(0).Rows(0)("PRES1")
											pres4NewRow("PRES2") = oPres4.Data.Tables(0).Rows(0)("PRES2")
											pres4NewRow("PRES3") = oPres4.Data.Tables(0).Rows(0)("PRES3")
											pres4NewRow("PRES4") = oPres4.Data.Tables(0).Rows(0)("PRES4")
									End Select
									pres4NewRow("NIVEL") = Split(lineaExistente("VALOR_TEXT").ToString, "_")(0)
									pres4NewRow("PRESID") = Split(lineaExistente("VALOR_TEXT").ToString, "_")(1)

									TextPres = pres4NewRow("PRES1")
									If pres4NewRow("NIVEL") > 1 Then TextPres = TextPres & " / " & pres4NewRow("PRES2")
									If pres4NewRow("NIVEL") > 2 Then TextPres = TextPres & " / " & pres4NewRow("PRES3")
									If pres4NewRow("NIVEL") > 3 Then TextPres = TextPres & " / " & pres4NewRow("PRES4")
								End If
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, TextPres)
							Case TiposDeDatos.TipoCampoGS.FormaPago
								Dim lFormaPago As List(Of DataRow) = oFormasPago.Data.Tables(0).Select("COD='" & lineaExistente("VALOR_TEXT").ToString & "'").ToList
								If lFormaPago.Any Then
									formaPago = lFormaPago.First
									objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, formaPago("COD") & " - " & formaPago("DEN"))
								Else
									objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, DBNull.Value)
								End If
							Case TiposDeDatos.TipoCampoGS.Moneda
								Dim lMonedas As List(Of DataRow) = oMonedas.Data.Tables(0).Select("COD='" & lineaExistente("VALOR_TEXT").ToString & "'").ToList
								If lMonedas.Any Then
									moneda = lMonedas.First
									objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, moneda("COD") & " - " & moneda("DEN"))
								Else
									objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, DBNull.Value)
								End If
							Case TiposDeDatos.TipoCampoGS.Unidad, TiposDeDatos.TipoCampoGS.UnidadPedido
								Dim lUnidades As List(Of DataRow) = oUnidades.Data.Tables(0).Select("COD='" & lineaExistente("VALOR_TEXT").ToString & "'").ToList
								If lUnidades.Any Then
									unidad = lUnidades.First
									objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, unidad("COD") & " - " & unidad("DEN"))
								Else
									objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, DBNull.Value)
								End If
							Case TiposDeDatos.TipoCampoGS.Dest
								If oDests.Data.Tables(0).Select("COD='" & lineaExistente("VALOR_TEXT").ToString & "'").Count = 1 Then
									destino = oDests.Data.Tables(0).Select("COD='" & lineaExistente("VALOR_TEXT").ToString & "'").First
								Else
									Dim destinoDefecto As FSNServer.Destinos = FSNServer.Get_Object(GetType(FSNServer.Destinos))
									destinoDefecto.LoadData(FSNUser.Idioma.ToString(), FSNUser.CodPersona, Split(lineaExistente("VALOR_TEXT").ToString, " - ")(0))
									Dim idestino As DataRow = oDests.Data.Tables(0).NewRow
									idestino("COD") = destinoDefecto.Data.Tables(0).Rows(0)("COD")
									idestino("DEN") = destinoDefecto.Data.Tables(0).Rows(0)("DEN")
									oDests.Data.Tables(0).Rows.Add(idestino)
									destino = oDests.Data.Tables(0).Rows.OfType(Of DataRow).Last
								End If
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, destino("COD") & " - " & destino("DEN"))
							Case TiposDeDatos.TipoCampoGS.Almacen
								Dim lAlmacenes As List(Of DataRow) = oAlmacenes.Data.Tables(0).Select("ID='" & lineaExistente("VALOR_NUM").ToString & "'").ToList
								If lAlmacenes.Any Then
									almacen = lAlmacenes.First
									objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, almacen("COD") & " - " & almacen("DEN"))
								Else
									objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, DBNull.Value)
								End If
							Case TiposDeDatos.TipoCampoGS.Pais
								pais = oPaises.Data.Tables(0).Select("COD='" & lineaExistente("VALOR_TEXT").ToString & "'").First
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, pais("COD") & " - " & pais("DEN"))
							Case TiposDeDatos.TipoCampoGS.Provincia
								oProvincias = FSNServer.Get_Object(GetType(FSNServer.Provincias))
								oProvincias.Pais = pais("COD")
								oProvincias.LoadData(FSNUser.Idioma.ToString)
								provincia = oProvincias.Data.Tables(0).Select("COD='" & lineaExistente("VALOR_TEXT").ToString & "'").First
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, provincia("COD") & " - " & provincia("DEN"))
							Case TiposDeDatos.TipoCampoGS.IniSuministro, TiposDeDatos.TipoCampoGS.FinSuministro
								Dim iTipoFecha As TiposDeDatos.TipoFecha
								iTipoFecha = DBNullToSomething(DBNullToSomething(lineaExistente("VALOR_NUM")))
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, DevolverFechaRelativaA(iTipoFecha, DBNullToSomething(lineaExistente("VALOR_FEC")), favorito))
							Case TiposDeDatos.TipoCampoGS.Departamento
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, Trim(Left(lineaExistente("VALOR_TEXT").ToString, FSNServer.LongitudesDeCodigos.giLongCodDEP)))
							Case TiposDeDatos.TipoCampoGS.CodArticulo, TiposDeDatos.TipoCampoGS.NuevoCodArticulo
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, Trim(Left(lineaExistente("VALOR_TEXT").ToString, FSNServer.LongitudesDeCodigos.giLongCodART)))
							Case TiposDeDatos.TipoCampoGS.Persona
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, Trim(Left(lineaExistente("VALOR_TEXT").ToString, FSNServer.LongitudesDeCodigos.giLongCodPER)))
							Case TiposDeDatos.TipoCampoGS.Departamento
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, Trim(Left(lineaExistente("VALOR_TEXT").ToString, FSNServer.LongitudesDeCodigos.giLongCodDEP)))
							Case TiposDeDatos.TipoCampoGS.Activo
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, Trim(Left(lineaExistente("VALOR_TEXT").ToString, FSNServer.LongitudesDeCodigos.giLongCodACTIVO)))
							Case TiposDeDatos.TipoCampoGS.Comprador
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, Trim(Left(lineaExistente("VALOR_TEXT").ToString, FSNServer.LongitudesDeCodigos.giLongCodCOM)))
							Case TiposDeDatos.TipoCampoGS.Proveedor, TiposDeDatos.TipoCampoGS.ProveContacto, TiposDeDatos.TipoCampoGS.ProveedorERP
								objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, Trim(Left(lineaExistente("VALOR_TEXT").ToString, FSNServer.LongitudesDeCodigos.giLongCodPROVE)))
							Case Else 'Si es un tipo de archivo o campo calculado no lo pasamos a la excel
								Dim columnaTipo As DataRow = dtConfiguracionCampos.Rows.OfType(Of DataRow).Where(Function(x) x("ID") = lineaExistente("CAMPO_HIJO")).First
								If Not {TiposDeDatos.TipoGeneral.TipoArchivo}.Contains(columnaTipo("SUBTIPO")) _
										AndAlso Not (columnaTipo("TIPO") = TipoCampoPredefinido.Calculado AndAlso columnaTipo("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo AndAlso Not IsDBNull(columnaTipo("ID_CALCULO"))) Then
									Select Case columnaTipo("SUBTIPO")
										Case TiposDeDatos.TipoGeneral.TipoNumerico
											objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, lineaExistente("VALOR_NUM").ToString)
										Case TiposDeDatos.TipoGeneral.TipoFecha
											objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, lineaExistente("VALOR_FEC"))
										Case TiposDeDatos.TipoGeneral.TipoBoolean
											objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, If(CType(lineaExistente("VALOR_BOOL"), Boolean), TextoSi, TextoNo))
										Case TiposDeDatos.TipoGeneral.TipoArchivo
											'No pasamos los de tipo archivo
										Case Else
											objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, lineaExistente("VALOR_TEXT").ToString)
									End Select
								End If
						End Select
					Else 'Si no hay valor exportamos nulo / 'Si es un tipo de archivo o campo calculado no lo pasamos a la excel
						If Not {TiposDeDatos.TipoGeneral.TipoArchivo}.Contains(campoLinea("SUBTIPO")) _
										AndAlso Not (campoLinea("TIPO") = TipoCampoPredefinido.Calculado AndAlso campoLinea("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo AndAlso Not IsDBNull(campoLinea("ID_CALCULO"))) Then
							Select Case DBNullToInteger(campoLinea("TIPO_CAMPO_GS"))
								Case TiposDeDatos.TipoCampoGS.Material, TiposDeDatos.TipoCampoGS.CentroCoste
									objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, DBNull.Value)
									objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, DBNull.Value)
									objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, DBNull.Value)
									objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, DBNull.Value)
								Case TiposDeDatos.TipoCampoGS.Partida
									objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, DBNull.Value)
									If NivelPartida > 1 Then objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, DBNull.Value)
									If NivelPartida > 2 Then objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, DBNull.Value)
									If NivelPartida > 3 Then objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, DBNull.Value)
								Case TiposDeDatos.TipoCampoGS.UnidadOrganizativa
									objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, DBNull.Value)
									objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, DBNull.Value)
									objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, DBNull.Value)
								Case Else
									objCommand.Parameters.AddWithValue("@CAMPO_" & objCommand.Parameters.Count, DBNull.Value)
							End Select
						End If
					End If
				Next
				objCommand.ExecuteNonQuery()
			Next
		End If
	End Sub
#End Region
#Region "Cargar Excel"
    <WebMethod(EnableSession:=True)>
    Public Function CargarExcelDesglose(ByVal idCampoDesglose As Long, ByVal fileName As String, ByVal idCampoUnidadArticulo As Long, ByVal idSolicitudActual As Long, ByVal sCentroCostePartida As String _
    , ByVal sPartida0AnyoImputacion As String, ByVal sPartidaAnyoImputacion As String, ByVal sCentroCosteAnyoImputacion As String) As String
        Dim spath As String = ConfigurationManager.AppSettings("temp") & "\"
        Dim sConnectionString As String = String.Format(ConfigurationManager.AppSettings("ExcelConnectionString"), spath & fileName)
        Dim excelConnection As OleDb.OleDbConnection = Nothing
        Dim objCommand As OleDb.OleDbCommand
        Dim serializer As New JavaScriptSerializer

        Try
            'Establecer una conexion con el origen de datos.
            excelConnection = New OleDb.OleDbConnection(sConnectionString)
            excelConnection.Open()

            Dim dsResultado, dsLecturaHojaExcel As DataSet
            dsLecturaHojaExcel = New DataSet
            dsResultado = New DataSet
            Dim oledbDataAdapter As New OleDbDataAdapter
            'Obtenemos los datos de los campos que existen en la excel para poder hacer luego validaciones 
            'y obtener datos completos para crear nuevas lineas
            objCommand = New OleDbCommand("SELECT * FROM [CONFIGCAMPOS$]", excelConnection)
            oledbDataAdapter.SelectCommand = objCommand
            oledbDataAdapter.Fill(dsResultado)
            dsLecturaHojaExcel.Tables.Add(dsResultado.Tables(0).Copy)
            dsLecturaHojaExcel.Tables(0).TableName = "CONFIGCAMPOS"

            FSNUser = HttpContext.Current.Session("FSN_User")
            Dim infoDesglose As Object = HttpContext.Current.Cache("infoDesglose_" & FSNUser.Cod & "_" & idCampoDesglose)
            Dim ds As DataSet = CType(infoDesglose(1), DataSet)

            dsResultado = New DataSet
            objCommand = New OleDbCommand("SELECT * FROM [DATOSCAMPOSLISTA$]", excelConnection)
            oledbDataAdapter.SelectCommand = objCommand
            oledbDataAdapter.Fill(dsResultado)
            dsLecturaHojaExcel.Tables.Add(dsResultado.Tables(0).Copy)
            dsLecturaHojaExcel.Tables(1).TableName = "DATOSCAMPOSLISTA"

            dsResultado = New DataSet
            objCommand = New OleDbCommand("SELECT * FROM [DATOSCAMPOSGS$]", excelConnection)
            oledbDataAdapter.SelectCommand = objCommand
            oledbDataAdapter.Fill(dsResultado)
            dsLecturaHojaExcel.Tables.Add(dsResultado.Tables(0).Copy)
            dsLecturaHojaExcel.Tables(2).TableName = "DATOSCAMPOSGS"

            dsResultado = New DataSet
            objCommand = New OleDbCommand("SELECT * FROM [DATOSPROVINCIAS$]", excelConnection)
            oledbDataAdapter.SelectCommand = objCommand
            oledbDataAdapter.Fill(dsResultado)
            dsLecturaHojaExcel.Tables.Add(dsResultado.Tables(0).Copy)
            dsLecturaHojaExcel.Tables(3).TableName = "DATOSPROVINCIAS"

            dsResultado = New DataSet
            objCommand = New OleDbCommand("SELECT * FROM [DATOSPROVINCIASVALUES$]", excelConnection)
            oledbDataAdapter.SelectCommand = objCommand
            oledbDataAdapter.Fill(dsResultado)
            dsLecturaHojaExcel.Tables.Add(dsResultado.Tables(0).Copy)
            dsLecturaHojaExcel.Tables(4).TableName = "DATOSPROVINCIASVALUES"

            dsResultado = New DataSet
            objCommand = New OleDbCommand("SELECT * FROM [LINEAS$]", excelConnection)
            oledbDataAdapter.SelectCommand = objCommand
            oledbDataAdapter.Fill(dsResultado)
            dsLecturaHojaExcel.Tables.Add(dsResultado.Tables(0).Copy)
            dsLecturaHojaExcel.Tables(5).TableName = "LINEAS"

            'Hay un limite de lineas posibles de cargar que vendra determinado por un entrada en el web.config
            If dsLecturaHojaExcel.Tables(5).Rows.Count > CType(ConfigurationManager.AppSettings("MaximoNumeroLineasImportarExcel"), Integer) Then Return serializer.Serialize(New With {Key .valoresLinea = "", .errores = "", .error = 2})

            dsResultado = New DataSet
            objCommand = New OleDbCommand("SELECT * FROM [TEXTOS$]", excelConnection)
            oledbDataAdapter.SelectCommand = objCommand
            oledbDataAdapter.Fill(dsResultado)
            dsLecturaHojaExcel.Tables.Add(dsResultado.Tables(0).Copy)
            dsLecturaHojaExcel.Tables(6).TableName = "INFOSOLICITUD"

            idSolicitudExcel = CType(dsLecturaHojaExcel.Tables("INFOSOLICITUD").Rows.OfType(Of DataRow).Where(Function(x) x("ID") = -10).First()("TEXTO"), Long)
            iTipoSolicitud = CType(dsLecturaHojaExcel.Tables("INFOSOLICITUD").Rows.OfType(Of DataRow).Where(Function(x) x("ID") = 0).First()("TEXTO"), TiposDeDatos.TipoDeSolicitud)
            orgComprasOutDesglose = dsLecturaHojaExcel.Tables("INFOSOLICITUD").Rows.OfType(Of DataRow).Where(Function(x) x("ID") = -1).First()("TEXTO").ToString
            centroOutDesglose = dsLecturaHojaExcel.Tables("INFOSOLICITUD").Rows.OfType(Of DataRow).Where(Function(x) x("ID") = -2).First()("TEXTO").ToString
            destinoOutDesglose = dsLecturaHojaExcel.Tables("INFOSOLICITUD").Rows.OfType(Of DataRow).Where(Function(x) x("ID") = -3).First()("TEXTO").ToString
            UnidadOrganizativaOutDesglose = dsLecturaHojaExcel.Tables("INFOSOLICITUD").Rows.OfType(Of DataRow).Where(Function(x) x("ID") = -4).First()("TEXTO").ToString

            If Not idSolicitudExcel = idSolicitudActual Then Return serializer.Serialize(New With {Key .valoresLinea = "", .errores = "", .error = 1})
            Dim valoresLineasCamposDesglose As New List(Of Dictionary(Of String, KeyValuePair(Of String, String)))
            Dim erroresLinea As New List(Of KeyValuePair(Of String, KeyValuePair(Of String, String))) 'linea-campotipogs-valor error
            GenerarValoresDesglose_Validaciones(dsLecturaHojaExcel, valoresLineasCamposDesglose, erroresLinea, idCampoUnidadArticulo, sCentroCostePartida, sPartida0AnyoImputacion, sPartidaAnyoImputacion, sCentroCosteAnyoImputacion)

            Return serializer.Serialize(New With {Key .valoresLinea = valoresLineasCamposDesglose, .errores = erroresLinea, .error = 0})
        Catch ex As Exception
            Throw ex
        Finally
            excelConnection.Close()
            objCommand = Nothing
            excelConnection = Nothing
        End Try
    End Function
    Private Sub GenerarValoresDesglose_Validaciones(ByVal dsLecturaHojaExcel As DataSet, ByRef valoresLineasCamposDesglose As List(Of Dictionary(Of String, KeyValuePair(Of String, String))),
                                                        ByRef erroresLinea As List(Of KeyValuePair(Of String, KeyValuePair(Of String, String))), ByVal idCampoUnidadArticulo As Long, ByVal sCentroCostePartida As String _
                                                        , ByVal sPartida0AnyoImputacion As String, ByVal sPartidaAnyoImputacion As String, ByVal sCentroCosteAnyoImputacion As String)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")

        Dim oDict As FSNServer.Dictionary
        oDict = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.DesgloseControl, FSNUser.Idioma)
        Textos = oDict.Data.Tables(0)

        'Hacer comprobaciones y poner valores para los campos Pais y Provincia
        Dim drPais, drPres, drDepartamento(), drCentroCoste(), drPartidaSM(), drActivo(), drOrganizacionCompras(), drCentro(), drAlmacen(), drAnyoPartida() As DataRow
        Dim paisCod, provinciaCod, GMN1Cod, GMN2Cod, GMN3Cod, GMN4Cod, DenArticulo, UON1, UON2, UON3, campoUO, organizacionCompras, centro, destino, proveedor, sProveSuministrador As String
        Dim PRES_Anyo, PRES_UON1, PRES_UON2, PRES_UON3, PRES_Cod, centroCosteUO1, centroCosteUO2, centroCosteUO3, centroCosteUO4, partidaNiv0, partidaNiv1, partidaNiv2, partidaNiv3, partidaNiv4, anyoPRES1, anyoPRES2 As String
        Dim PRES1(4), PRES2(4), PRES3(4), PRES4(4) As String
        Dim provinciaValueIndex, nivelSeleccionMaterial, idCampoMaterial, idCampoDenArticulo, nivelUON, idCampoCentroCoste, idCampoUnidad, idCampoProveedor, idCampoPrecioUnitario As Integer
        Dim ValidarArticuloLinea, TieneOrgComprasDesglose, TieneCentroDesglose, TieneUnidadOrganizativaDesglose, TieneCentroCosteDesglose As Boolean
        Dim infoArticuloValidado As Object = Nothing
        Dim valoresLinea As Dictionary(Of String, KeyValuePair(Of String, String)) 'id-value-den
        TieneOrgComprasDesglose = False : TieneCentroDesglose = False
        'VALIDAREMOS UNICAMENTE SI SE HA ESTABLECIDO VALOR EN LA EXCEL
        For Each linea As DataRow In dsLecturaHojaExcel.Tables("LINEAS").Rows
            'Inicializamos las variables por cada linea
            paisCod = String.Empty : provinciaCod = String.Empty : GMN1Cod = String.Empty : GMN2Cod = String.Empty : GMN3Cod = String.Empty : GMN4Cod = String.Empty : DenArticulo = String.Empty
            UON1 = String.Empty : UON2 = String.Empty : UON3 = String.Empty : campoUO = String.Empty : organizacionCompras = String.Empty : centro = String.Empty : destino = String.Empty
            proveedor = String.Empty : PRES_Anyo = String.Empty : PRES_UON1 = String.Empty : PRES_UON2 = String.Empty : PRES_UON3 = String.Empty : PRES_Cod = String.Empty
            centroCosteUO1 = String.Empty : centroCosteUO2 = String.Empty : centroCosteUO3 = String.Empty : centroCosteUO4 = String.Empty
            partidaNiv0 = String.Empty : partidaNiv1 = String.Empty : partidaNiv2 = String.Empty : partidaNiv3 = String.Empty : partidaNiv4 = String.Empty : anyoPRES1 = String.Empty : anyoPRES2 = String.Empty
            PRES1 = {"", "", "", ""} : PRES2 = {"", "", "", ""} : PRES3 = {"", "", "", ""} : PRES4 = {"", "", "", ""}
            provinciaValueIndex = 0 : nivelSeleccionMaterial = 0 : idCampoMaterial = 0 : idCampoDenArticulo = 0 : nivelUON = 0 : idCampoCentroCoste = 0
            idCampoUnidad = 0 : idCampoProveedor = 0 : idCampoPrecioUnitario = 0
            ValidarArticuloLinea = False : drCentroCoste = Nothing : sProveSuministrador = String.Empty
            valoresLinea = New Dictionary(Of String, KeyValuePair(Of String, String))
            For Each campo As DataRow In dsLecturaHojaExcel.Tables("CONFIGCAMPOS").Rows
                Select Case DBNullToInteger(campo("TIPO_CAMPO_GS"))
                    Case TiposDeDatos.TipoCampoGS.Proveedor
                        idCampoProveedor = CType(campo("ID"), Integer)
                        If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                            oProveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedores))
                            oProveedor.LoadData(FSNUser.Idioma.ToString, linea(campo("CABECERA")).ToString, PMRestricProveMat:=FSNUser.PMRestricProveMat, bUsaLike:=False)
                            If oProveedor.Data.Tables(0).Rows.Count = 0 Then 'El codigo de proveedor indicado en el excel no puede agregarse
                                erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                                                                                  New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), linea(campo("CABECERA")).ToString)))
                            Else
                                proveedor = linea(campo("CABECERA")).ToString
                                If DBNullToBoolean(campo("CARGAR_REL_PROVE_ART4")) Then sProveSuministrador = linea(campo("CABECERA")).ToString
                                valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(oProveedor.Data.Tables(0).Rows(0)("COD"), oProveedor.Data.Tables(0).Rows(0)("COD") & " - " & oProveedor.Data.Tables(0).Rows(0)("DEN"))
                            End If
                        End If
                    Case TiposDeDatos.TipoCampoGS.Material 'Obtenemos todos los campos que identifican el campo. Comprobamos que esta bien introducido, es decir, por ejemplo que no se haya indicado el nivel 2 y no el 1
                        idCampoMaterial = CType(campo("ID"), Integer) 'Guardamos el id del material para luego utilizarlo en articulo
                        Select Case campo("CABECERA").ToString.ToUpper
                            Case FSNServer.TipoAcceso.gbGMN1Cod.ToUpper
                                GMN1Cod = linea(campo("CABECERA")).ToString
                            Case FSNServer.TipoAcceso.gbGMN2Cod.ToUpper
                                GMN2Cod = linea(campo("CABECERA")).ToString
                            Case FSNServer.TipoAcceso.gbGMN3Cod.ToUpper
                                GMN3Cod = linea(campo("CABECERA")).ToString
                            Case FSNServer.TipoAcceso.gbGMN4Cod.ToUpper
                                GMN4Cod = linea(campo("CABECERA")).ToString
                                If Not String.IsNullOrEmpty(GMN1Cod & GMN2Cod & GMN3Cod & GMN4Cod) Then
                                    If (Not String.IsNullOrEmpty(GMN4Cod) AndAlso (String.IsNullOrEmpty(GMN1Cod) OrElse String.IsNullOrEmpty(GMN2Cod) OrElse String.IsNullOrEmpty(GMN3Cod))) _
                                            OrElse (Not String.IsNullOrEmpty(GMN3Cod) AndAlso (String.IsNullOrEmpty(GMN1Cod) OrElse String.IsNullOrEmpty(GMN2Cod))) _
                                            OrElse (Not String.IsNullOrEmpty(GMN2Cod) AndAlso String.IsNullOrEmpty(GMN1Cod)) Then
                                        erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                                                New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), GMN1Cod & " - " & GMN2Cod & " - " & GMN3Cod)))
                                    Else
                                        nivelSeleccionMaterial = Convert.ToInt32(Not String.IsNullOrEmpty(GMN1Cod)) + Convert.ToInt32(Not String.IsNullOrEmpty(GMN2Cod)) _
                                                                        + Convert.ToInt32(Not String.IsNullOrEmpty(GMN3Cod)) + Convert.ToInt32(Not String.IsNullOrEmpty(GMN4Cod))
                                        'Obtenemos con los codigos GMN el material, con esto debemos tener el key=concatenacion de todos los GMN y value=cod-denominacion del material en el nivel
                                        oMaterial = FSNServer.Get_Object(GetType(FSNServer.GruposMaterial))
                                        oMaterial.LoadData(GMN1Cod, GMN2Cod, GMN3Cod, GMN4Cod, FSNUser.Idioma.ToString, FSNUser.CodPersona, , FSNUser.FSPMRestrMatUsu, , nivelSeleccionMaterial)
                                        If oMaterial.Data.Tables(nivelSeleccionMaterial - 1).Rows.Count = 1 Then
                                            Dim iLongCod1 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                                            Dim iLongCod2 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                                            Dim iLongCod3 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                                            Dim iLongCod4 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN4

                                            If Len(GMN1Cod) < iLongCod1 Then GMN1Cod = Space(iLongCod1 - Len(GMN1Cod)) & GMN1Cod
                                            If Len(GMN2Cod) < iLongCod2 Then GMN2Cod = Space(iLongCod2 - Len(GMN2Cod)) & GMN2Cod
                                            If Len(GMN3Cod) < iLongCod3 Then GMN3Cod = Space(iLongCod3 - Len(GMN3Cod)) & GMN3Cod
                                            If Len(GMN4Cod) < iLongCod4 Then GMN4Cod = Space(iLongCod4 - Len(GMN4Cod)) & GMN4Cod

                                            valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(GMN1Cod & GMN2Cod & GMN3Cod & GMN4Cod, oMaterial.Data.Tables(nivelSeleccionMaterial - 1).Rows(0)("DEN_" & FSNUser.Idioma.ToString))
                                        Else
                                            erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                                                New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), GMN1Cod & " - " & GMN2Cod & " - " & GMN3Cod)))
                                        End If
                                    End If
                                End If
                        End Select
                    Case TiposDeDatos.TipoCampoGS.CodArticulo, TiposDeDatos.TipoCampoGS.NuevoCodArticulo
                        If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                            ValidarArticuloLinea = True
                            infoArticuloValidado = New With {Key .id = CType(campo("ID"), Integer), .valor = linea(campo("CABECERA")).ToString,
                                                                .anyadirArticulo = CType(campo("ANYADIR_ART"), Boolean), .nombreCampoDesglose = campo("NOMBRECAMPODESGLOSE")}
                        End If
                    Case TiposDeDatos.TipoCampoGS.DenArticulo 'Si la denominacion del articulo esta vacia es un articulo generico o es un articulo nuevo(si tiene permiso lo ponemos)
                        idCampoDenArticulo = CType(campo("ID"), Integer) 'Guardamos el id de la denominacion para luego utilizarlo en articulo
                        If String.IsNullOrEmpty(DenArticulo) Then
                            valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(linea(campo("CABECERA")).ToString, linea(campo("CABECERA")).ToString)
                            DenArticulo = linea(campo("CABECERA")).ToString
                        Else
                            valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(DenArticulo, DenArticulo)
                        End If
                    Case TiposDeDatos.TipoCampoGS.PRES1
                        If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                            Try
                                drPres = dsLecturaHojaExcel.Tables("DATOSCAMPOSGS").Select("DEN_" & campo("ID") & "='" & linea(campo("CABECERA")).ToString & "'").First

                                valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(drPres("VAL_" & campo("id")), Replace(linea(campo("CABECERA")).ToString, "/", "-") & " (100" & FSNUser.NumberFormat.NumberDecimalSeparator & "00%);")

                            Catch ex As Exception
                                'No esta en la lista de Favoritos
                                drPres = Nothing

                                If nivelPres1 = 0 Then
                                    oPres = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
                                    nivelPres1 = oPres.GetNivelPres(1)
                                End If
                                Dim DatoPresup As String() = Split(linea(campo("CABECERA")).ToString, "/")
                                anyoPRES1 = Trim(Split(DatoPresup(0), "-")(0))
                                PRES1(0) = Trim(Split(DatoPresup(0), "-")(1))
                                Try
                                    If nivelPres1 > 1 Then PRES1(1) = Trim(DatoPresup(1))
                                    If nivelPres1 > 2 Then PRES1(2) = Trim(DatoPresup(2))
                                    If nivelPres1 > 3 Then PRES1(3) = Trim(DatoPresup(3))
                                Catch ex2 As Exception
                                    'El nivel mÃ¡ximo puede ser 3 pero te llega 2 niveles
                                End Try
                                Dim Nivel As Integer = Convert.ToInt32(Not String.IsNullOrEmpty(PRES1(0))) + Convert.ToInt32(Not String.IsNullOrEmpty(PRES1(1))) _
                                                                                        + Convert.ToInt32(Not String.IsNullOrEmpty(PRES1(2))) + Convert.ToInt32(Not String.IsNullOrEmpty(PRES1(3)))
                                If Not (Nivel) = 0 AndAlso (UBound(DatoPresup) + 1 <= nivelPres1) Then
                                    Dim dtPres As DataTable
                                    nivelPres1 = (Convert.ToInt32(Not String.IsNullOrEmpty(PRES1(0))) + Convert.ToInt32(Not String.IsNullOrEmpty(PRES1(1))) _
                                                                    + Convert.ToInt32(Not String.IsNullOrEmpty(PRES1(2))) + Convert.ToInt32(Not String.IsNullOrEmpty(PRES1(3))))
                                    If FSNUser.PMPresUO Then
                                        oPres.CargarPresupuestos(1, 0, anyoPRES1, FSNUser.UON1, FSNUser.UON2, FSNUser.UON3, FSNUser.Idioma.ToString, PRES1(nivelPres1 - 1))
                                        dtPres = oPres.Data.Tables(nivelPres1)
                                    Else
                                        dtPres = oPres.Get_Pres(1, PRES1(0), PRES1(1), PRES1(2), PRES1(3), strToInt(anyoPRES1))
                                    End If
                                    If dtPres.Rows.Count = 1 Then
                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(Nivel & "_" & dtPres(0)("ID") & "_1",
                                                                      anyoPRES1 & " - " & PRES1(0) & IIf(String.IsNullOrEmpty(PRES1(1)), "", " - " & PRES1(1)) &
                                                                  IIf(String.IsNullOrEmpty(PRES1(2)), "", " - " & PRES1(2)) &
                                                                  IIf(String.IsNullOrEmpty(PRES1(3)), "", " - " & PRES1(3)) &
                                                                      " (100" & FSNUser.NumberFormat.NumberDecimalSeparator & "00%);")
                                    Else
                                        erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                                                                New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), anyoPRES1.ToString & " - " & PRES1(0).ToString &
                                                                                                IIf(String.IsNullOrEmpty(PRES1(1)), "", " / " & PRES1(1)) &
                                                                                                IIf(String.IsNullOrEmpty(PRES1(2)), "", " / " & PRES1(2)) &
                                                                                                IIf(String.IsNullOrEmpty(PRES1(3)), "", " / " & PRES1(3)))))
                                    End If
                                Else
                                    erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                    New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), linea(campo("CABECERA")).ToString)))
                                End If
                            End Try
                        End If
                    Case TiposDeDatos.TipoCampoGS.Pres2
                        If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                            Try
                                drPres = dsLecturaHojaExcel.Tables("DATOSCAMPOSGS").Select("DEN_" & campo("ID") & "='" & linea(campo("CABECERA")).ToString & "'").First

                                valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(drPres("VAL_" & campo("id")), Replace(linea(campo("CABECERA")).ToString, "/", "-") & " (100" & FSNUser.NumberFormat.NumberDecimalSeparator & "00%);")

                            Catch ex As Exception
                                'No esta en la lista de Favoritos
                                drPres = Nothing

                                If nivelPres2 = 0 Then
                                    oPres = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
                                    nivelPres2 = oPres.GetNivelPres(2)
                                End If
                                Dim DatoPresup As String() = Split(linea(campo("CABECERA")).ToString, "/")
                                anyoPRES2 = Trim(Split(DatoPresup(0), "-")(0))
                                PRES2(0) = Trim(Split(DatoPresup(0), "-")(1))
                                Try
                                    If nivelPres2 > 1 Then PRES2(1) = Trim(DatoPresup(1))
                                    If nivelPres2 > 2 Then PRES2(2) = Trim(DatoPresup(2))
                                    If nivelPres2 > 3 Then PRES2(3) = Trim(DatoPresup(3))
                                Catch ex2 As Exception
                                    'El nivel mÃ¡ximo puede ser 3 pero te llega 2 niveles
                                End Try
                                Dim Nivel As Integer = Convert.ToInt32(Not String.IsNullOrEmpty(PRES2(0))) + Convert.ToInt32(Not String.IsNullOrEmpty(PRES2(1))) _
                                                                                    + Convert.ToInt32(Not String.IsNullOrEmpty(PRES2(2))) + Convert.ToInt32(Not String.IsNullOrEmpty(PRES2(3)))
                                If Not (Nivel) = 0 AndAlso (UBound(DatoPresup) + 1 <= nivelPres2) Then
                                    Dim dtPres As DataTable
                                    nivelPres2 = (Convert.ToInt32(Not String.IsNullOrEmpty(PRES2(0))) + Convert.ToInt32(Not String.IsNullOrEmpty(PRES2(1))) _
                                                                + Convert.ToInt32(Not String.IsNullOrEmpty(PRES2(2))) + Convert.ToInt32(Not String.IsNullOrEmpty(PRES2(3))))
                                    If FSNUser.PMPresUO Then
                                        oPres.CargarPresupuestos(2, 0, anyoPRES2, FSNUser.UON1, FSNUser.UON2, FSNUser.UON3, FSNUser.Idioma.ToString, PRES2(nivelPres2 - 1))
                                        dtPres = oPres.Data.Tables(nivelPres2)
                                    Else
                                        dtPres = oPres.Get_Pres(2, PRES2(0), PRES2(1), PRES2(2), PRES2(3), strToInt(anyoPRES2))
                                    End If
                                    If dtPres.Rows.Count = 1 Then
                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(Nivel & "_" & dtPres(0)("ID") & "_1",
                                                              anyoPRES2 & " - " & PRES2(0) & IIf(String.IsNullOrEmpty(PRES2(1)), "", " - " & PRES2(1)) &
                                                              IIf(String.IsNullOrEmpty(PRES2(2)), "", " - " & PRES2(2)) &
                                                              IIf(String.IsNullOrEmpty(PRES2(3)), "", " - " & PRES2(3)) &
                                                              " (100" & FSNUser.NumberFormat.NumberDecimalSeparator & "00%);")
                                    Else
                                        erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                                                        New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), anyoPRES2.ToString & " - " & PRES2(0).ToString &
                                                                                        IIf(String.IsNullOrEmpty(PRES2(1)), "", " / " & PRES2(1)) &
                                                                                        IIf(String.IsNullOrEmpty(PRES2(2)), "", " / " & PRES2(2)) &
                                                                                        IIf(String.IsNullOrEmpty(PRES2(3)), "", " / " & PRES2(3)))))
                                    End If
                                Else
                                    erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                        New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), linea(campo("CABECERA")).ToString)))
                                End If
                            End Try
                        End If
                    Case TiposDeDatos.TipoCampoGS.Pres3
                        If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                            Try
                                drPres = dsLecturaHojaExcel.Tables("DATOSCAMPOSGS").Select("DEN_" & campo("ID") & "='" & linea(campo("CABECERA")).ToString & "'").First

                                valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(drPres("VAL_" & campo("id")), Replace(linea(campo("CABECERA")).ToString, "/", "-") & " (100" & FSNUser.NumberFormat.NumberDecimalSeparator & "00%);")

                            Catch ex As Exception
                                'No esta en la lista de Favoritos
                                drPres = Nothing

                                If nivelPres3 = 0 Then
                                    oPres = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
                                    nivelPres3 = oPres.GetNivelPres(3)
                                End If
                                Dim DatoPresup As String() = Split(linea(campo("CABECERA")).ToString, "/")
                                PRES3(0) = Trim(DatoPresup(0))
                                Try
                                    If nivelPres3 > 1 Then PRES3(1) = Trim(DatoPresup(1))
                                    If nivelPres3 > 2 Then PRES3(2) = Trim(DatoPresup(2))
                                    If nivelPres3 > 3 Then PRES3(3) = Trim(DatoPresup(3))
                                Catch ex2 As Exception
                                    'El nivel mÃ¡ximo puede ser 3 pero te llega 2 niveles
                                End Try
                                Dim Nivel As Integer = Convert.ToInt32(Not String.IsNullOrEmpty(PRES3(0))) + Convert.ToInt32(Not String.IsNullOrEmpty(PRES3(1))) _
                                                                                        + Convert.ToInt32(Not String.IsNullOrEmpty(PRES3(2))) + Convert.ToInt32(Not String.IsNullOrEmpty(PRES3(3)))
                                If Not (Nivel) = 0 AndAlso (UBound(DatoPresup) + 1 <= nivelPres3) Then
                                    Dim dtPres As DataTable
                                    nivelPres3 = (Convert.ToInt32(Not String.IsNullOrEmpty(PRES3(0))) + Convert.ToInt32(Not String.IsNullOrEmpty(PRES3(1))) _
                                                                    + Convert.ToInt32(Not String.IsNullOrEmpty(PRES3(2))) + Convert.ToInt32(Not String.IsNullOrEmpty(PRES3(3))))
                                    If FSNUser.PMPresUO Then
                                        oPres.CargarPresupuestos(3, 0, 0, FSNUser.UON1, FSNUser.UON2, FSNUser.UON3, FSNUser.Idioma.ToString, PRES3(nivelPres3 - 1))
                                        dtPres = oPres.Data.Tables(nivelPres3)
                                    Else
                                        dtPres = oPres.Get_Pres(3, PRES3(0), PRES3(1), PRES3(2), PRES3(3), 0)
                                    End If
                                    If dtPres.Rows.Count = 1 Then
                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(Nivel & "_" & dtPres(0)("ID") & "_1",
                                                                  PRES3(0) & IIf(String.IsNullOrEmpty(PRES3(1)), "", " - " & PRES3(1)) &
                                                              IIf(String.IsNullOrEmpty(PRES3(2)), "", " - " & PRES3(2)) &
                                                              IIf(String.IsNullOrEmpty(PRES3(3)), "", " - " & PRES3(3)) &
                                                              " (100" & FSNUser.NumberFormat.NumberDecimalSeparator & "00%);")
                                    Else
                                        erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                                                        New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), PRES3(0).ToString &
                                                                                        IIf(String.IsNullOrEmpty(PRES3(1)), "", " / " & PRES3(1)) &
                                                                                        IIf(String.IsNullOrEmpty(PRES3(2)), "", " / " & PRES3(2)) &
                                                                                        IIf(String.IsNullOrEmpty(PRES3(3)), "", " / " & PRES3(3)))))
                                    End If
                                Else
                                    erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), linea(campo("CABECERA")).ToString)))
                                End If
                            End Try
                        End If
                    Case TiposDeDatos.TipoCampoGS.Pres4
                        If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                            Try
                                drPres = dsLecturaHojaExcel.Tables("DATOSCAMPOSGS").Select("DEN_" & campo("ID") & "='" & linea(campo("CABECERA")).ToString & "'").First

                                valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(drPres("VAL_" & campo("id")), Replace(linea(campo("CABECERA")).ToString, "/", "-") & " (100" & FSNUser.NumberFormat.NumberDecimalSeparator & "00%);")

                            Catch ex As Exception
                                'No esta en la lista de Favoritos
                                drPres = Nothing

                                If nivelPres4 = 0 Then
                                    oPres = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
                                    nivelPres4 = oPres.GetNivelPres(4)
                                End If
                                Dim DatoPresup As String() = Split(linea(campo("CABECERA")).ToString, "/")
                                PRES4(0) = Trim(DatoPresup(0))
                                Try
                                    If nivelPres4 > 1 Then PRES4(1) = Trim(DatoPresup(1))
                                    If nivelPres4 > 2 Then PRES4(2) = Trim(DatoPresup(2))
                                    If nivelPres4 > 3 Then PRES4(3) = Trim(DatoPresup(3))
                                Catch ex2 As Exception
                                    'El nivel mÃ¡ximo puede ser 3 pero te llega 2 niveles
                                End Try
                                Dim Nivel As Integer = Convert.ToInt32(Not String.IsNullOrEmpty(PRES4(0))) + Convert.ToInt32(Not String.IsNullOrEmpty(PRES4(1))) _
                                                                                        + Convert.ToInt32(Not String.IsNullOrEmpty(PRES4(2))) + Convert.ToInt32(Not String.IsNullOrEmpty(PRES4(3)))
                                If Not (Nivel) = 0 AndAlso (UBound(DatoPresup) + 1 <= nivelPres4) Then
                                    Dim dtPres As DataTable
                                    nivelPres4 = (Convert.ToInt32(Not String.IsNullOrEmpty(PRES4(0))) + Convert.ToInt32(Not String.IsNullOrEmpty(PRES4(1))) _
                                                                + Convert.ToInt32(Not String.IsNullOrEmpty(PRES4(2))) + Convert.ToInt32(Not String.IsNullOrEmpty(PRES4(3))))
                                    If FSNUser.PMPresUO Then
                                        oPres.CargarPresupuestos(4, 0, 0, FSNUser.UON1, FSNUser.UON2, FSNUser.UON3, FSNUser.Idioma.ToString, PRES4(nivelPres4 - 1))
                                        dtPres = oPres.Data.Tables(nivelPres4)
                                    Else
                                        dtPres = oPres.Get_Pres(4, PRES4(0), PRES4(1), PRES4(2), PRES4(3), 0)
                                    End If
                                    If dtPres.Rows.Count = 1 Then
                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(Nivel & "_" & dtPres(0)("ID") & "_1",
                                                                  PRES4(0) & IIf(String.IsNullOrEmpty(PRES4(1)), "", " - " & PRES4(1)) &
                                                                  IIf(String.IsNullOrEmpty(PRES4(2)), "", " - " & PRES4(2)) &
                                                                  IIf(String.IsNullOrEmpty(PRES4(3)), "", " - " & PRES4(3)) &
                                                                  " (100" & FSNUser.NumberFormat.NumberDecimalSeparator & "00%);")
                                    Else
                                        erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                                                                New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), PRES4(0).ToString &
                                                                                                IIf(String.IsNullOrEmpty(PRES4(1)), "", " / " & PRES4(1)) &
                                                                                                IIf(String.IsNullOrEmpty(PRES4(2)), "", " / " & PRES4(2)) &
                                                                                                IIf(String.IsNullOrEmpty(PRES4(3)), "", " / " & PRES4(3)))))
                                    End If
                                Else
                                    erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                    New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), linea(campo("CABECERA")).ToString)))
                                End If
                            End Try
                        End If
                    Case TiposDeDatos.TipoCampoGS.Persona
                        If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                            oPersonas = FSNServer.Get_Object(GetType(FSNServer.Personas))
                            Dim sUON1, sUON2, sUON3, sDEP As String
                            sUON1 = Nothing : sUON2 = Nothing : sUON3 = Nothing : sDEP = Nothing
                            If FSNUser.PMRestringirPersonasUOUsuario OrElse FSNUser.PMRestringirPersonasDepartamentoUsuario Then
                                sUON1 = FSNUser.UON1
                                sUON2 = FSNUser.UON2
                                sUON3 = FSNUser.UON3
                                If FSNUser.PMRestringirPersonasDepartamentoUsuario Then sDEP = FSNUser.Dep
                            End If
                            oPersonas.LoadData(sCod:=linea(campo("CABECERA")).ToString, sUON1:=sUON1, sUON2:=sUON2, sUON3:=sUON3, sDEP:=sDEP, bPM:=True)
                            If oPersonas.Data.Tables(0).Rows.Count = 1 Then
                                With oPersonas.Data.Tables(0).Rows(0)
                                    valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(linea(campo("CABECERA")).ToString, .Item("NOM") & " " & .Item("APE") & " (" & .Item("EMAIL") & ")")
                                End With
                            Else
                                erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                                                New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), linea(campo("CABECERA")).ToString)))
                            End If
                        End If
                    Case TiposDeDatos.TipoCampoGS.UnidadOrganizativa 'Obtenemos todos los campos que identifican el campo. Comprobamos que esta bien introducido, es decir, por ejemplo que no se haya indicado el nivel 2 y no el 1
                        TieneUnidadOrganizativaDesglose = True
                        Select Case campo("CABECERA").ToString.ToUpper
                            Case FSNServer.TipoAcceso.gbEmpresa.ToUpper & " - " & FSNServer.TipoAcceso.gbUON1Cod.ToUpper
                                UON1 = linea(campo("CABECERA")).ToString
                            Case FSNServer.TipoAcceso.gbEmpresa.ToUpper & " - " & FSNServer.TipoAcceso.gbUON2Cod.ToUpper
                                UON2 = linea(campo("CABECERA")).ToString
                            Case Else
                                UON3 = linea(campo("CABECERA")).ToString
                                If Not String.IsNullOrEmpty(UON1 & UON2 & UON3) Then
                                    If (Not String.IsNullOrEmpty(UON3) AndAlso (String.IsNullOrEmpty(UON1) OrElse String.IsNullOrEmpty(UON2))) _
                                            OrElse (Not String.IsNullOrEmpty(UON2) AndAlso String.IsNullOrEmpty(UON1)) Then
                                        erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                                                    New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), UON1 & " - " & UON2 & " - " & UON3)))
                                    Else
                                        If oUnidadOrg Is Nothing Then 'Cargamos la primera vez que se necesite y mantenemos cargadas todas las un org. (no hay metodo para obtener segun uons)
                                            oUnidadOrg = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
                                            oUnidadOrg.CargarTodasUnidadesOrganizativas(FSNUser.Cod, FSNUser.Idioma.ToString, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil)
                                            nivelUON = Convert.ToInt32(Not String.IsNullOrEmpty(UON1)) + Convert.ToInt32(Not String.IsNullOrEmpty(UON2)) + Convert.ToInt32(Not String.IsNullOrEmpty(UON3))
                                        End If
                                        'campoUO = ods
                                        If nivelUON > 0 Then 'Nivel 0, no ha introducido valor para la UON
                                            Select Case nivelUON
                                                Case 1
                                                    If oUnidadOrg.Data.Tables(1).AsEnumerable.Where(Function(x) x("COD") = UON1).Any Then
                                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(UON1,
                                                                                UON1 & " - " & oUnidadOrg.Data.Tables(1).AsEnumerable.ToList.Find(Function(x) x("COD") = UON1)("DEN"))
                                                    Else
                                                        erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                                                                                                          New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), UON1 & " - " & UON2 & " - " & UON3)))
                                                    End If
                                                Case 2
                                                    If oUnidadOrg.Data.Tables(2).AsEnumerable.Where(Function(x) x("UON1") = UON1 AndAlso x("COD") = UON2).Any Then
                                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(UON1 & "-" & UON2,
                                                                                UON1 & " - " & UON2 & " - " & oUnidadOrg.Data.Tables(2).AsEnumerable.ToList.Find(Function(x) x("UON1") = UON1 AndAlso x("COD") = UON2)("DEN"))
                                                    Else
                                                        erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                                                                                                          New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), UON1 & " - " & UON2 & " - " & UON3)))
                                                    End If
                                                Case Else
                                                    If oUnidadOrg.Data.Tables(3).AsEnumerable.Where(Function(x) x("UON1") = UON1 AndAlso x("UON2") = UON2 AndAlso x("COD") = UON3).Any Then
                                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(UON1 & "-" & UON2 & "-" & UON3,
                                                                                UON1 & " - " & UON2 & " - " & UON3 & " - " & oUnidadOrg.Data.Tables(3).AsEnumerable.ToList.Find(Function(x) x("UON1") = UON1 AndAlso x("UON2") = UON2 AndAlso x("COD") = UON3)("DEN"))
                                                    Else
                                                        erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                                                                                                          New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), UON1 & " - " & UON2 & " - " & UON3)))
                                                    End If
                                            End Select
                                        End If
                                    End If
                                End If
                        End Select
                    Case TiposDeDatos.TipoCampoGS.Departamento
                        If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                            oDepartamento = FSNServer.Get_Object(GetType(FSNServer.Departamentos))
                            If (Not String.IsNullOrEmpty(UON3) AndAlso (String.IsNullOrEmpty(UON1) OrElse String.IsNullOrEmpty(UON2))) _
                                        OrElse (Not String.IsNullOrEmpty(UON2) AndAlso String.IsNullOrEmpty(UON1)) Then
                                erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                                            New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), linea(campo("CABECERA")).ToString)))
                            Else 'Si se ha indicado unidad organizativa miraremos si el departamento esta en esa unidad
                                Select Case Convert.ToInt32(Not String.IsNullOrEmpty(UON1)) + Convert.ToInt32(Not String.IsNullOrEmpty(UON2)) + Convert.ToInt32(Not String.IsNullOrEmpty(UON3))
                                    Case 0
                                        oDepartamento.LoadData()
                                    Case 1
                                        oDepartamento.LoadData(1, UON1)
                                    Case 2
                                        oDepartamento.LoadData(2, UON1, UON2)
                                    Case Else
                                        oDepartamento.LoadData(3, UON1, UON2, UON3)
                                End Select
                            End If
                            drDepartamento = oDepartamento.Data.Tables(0).Rows.OfType(Of DataRow).Where(Function(x) Not IsDBNull(x("COD")) AndAlso x("COD") = linea(campo("CABECERA")).ToString).ToArray
                            If drDepartamento.Any AndAlso drDepartamento.Count = 1 Then
                                valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(linea(campo("CABECERA")).ToString, drDepartamento(0)("COD") & " - " & drDepartamento(0)("DEN"))
                            Else
                                erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                                                                                  New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), linea(campo("CABECERA")).ToString)))
                            End If
                        End If
                    Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
                        TieneOrgComprasDesglose = True
                        If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                            If oOrganizacionesCompras Is Nothing Then 'cargamos solo la primera vez
                                oOrganizacionesCompras = FSNServer.Get_Object(GetType(FSNServer.OrganizacionesCompras))
                                oOrganizacionesCompras.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil)
                            End If
                            drOrganizacionCompras = oOrganizacionesCompras.Data.Tables(0).Rows.OfType(Of DataRow).Where(Function(x) x("COD") = linea(campo("CABECERA")).ToString).ToArray
                            If drOrganizacionCompras.Any AndAlso drOrganizacionCompras.Count = 1 Then
                                organizacionCompras = linea(campo("CABECERA")).ToString
                                valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(linea(campo("CABECERA")).ToString, drOrganizacionCompras(0)("COD") & " - " & drOrganizacionCompras(0)("DEN"))
                            Else
                                erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                            New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), linea(campo("CABECERA")).ToString)))
                            End If
                        End If
                    Case TiposDeDatos.TipoCampoGS.Centro
                        TieneCentroDesglose = True
                        If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                            oCentro = FSNServer.Get_Object(GetType(FSNServer.Centros))
                            If String.IsNullOrEmpty(organizacionCompras) Then 'no tiene organizacion de compras vinculado
                                oCentro.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil)
                            Else
                                oCentro.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil, DBNullToStr(organizacionCompras))
                            End If
                            drCentro = oCentro.Data.Tables(0).Rows.OfType(Of DataRow).Where(Function(x) x("COD") = linea(campo("CABECERA")).ToString).ToArray
                            If drCentro.Any AndAlso drCentro.Count = 1 Then
                                centro = linea(campo("CABECERA")).ToString
                                valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(linea(campo("CABECERA")).ToString, drCentro(0)("COD") & " - " & drCentro(0)("DEN"))
                            Else
                                erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                            New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), linea(campo("CABECERA")).ToString)))
                            End If
                        End If
                    Case TiposDeDatos.TipoCampoGS.Dest
                        If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                            destino = dsLecturaHojaExcel.Tables("DATOSCAMPOSGS").Select("DEN_" & campo("ID") & "='" & linea(campo("CABECERA")).ToString & "'").First.Item("VAL_" & campo("ID"))
                            valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(
                                    dsLecturaHojaExcel.Tables("DATOSCAMPOSGS").Select("DEN_" & campo("ID") & "='" & linea(campo("CABECERA")).ToString & "'").First.Item("VAL_" & campo("ID")), linea(campo("CABECERA")).ToString)
                        End If
                    Case TiposDeDatos.TipoCampoGS.Almacen
                        If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                            oAlmacenes = FSNServer.Get_Object(GetType(FSNServer.Almacenes))
                            If String.IsNullOrEmpty(centro) AndAlso Not String.IsNullOrEmpty(centroOutDesglose) Then centro = centroOutDesglose
                            If String.IsNullOrEmpty(destino) AndAlso Not String.IsNullOrEmpty(destinoOutDesglose) Then destino = destinoOutDesglose
                            oAlmacenes.LoadData(If(String.IsNullOrEmpty(centro), Nothing, centro), If(String.IsNullOrEmpty(destino), Nothing, destino))
                            drAlmacen = dsLecturaHojaExcel.Tables("DATOSCAMPOSGS").Select("DEN_" & campo("ID") & "='" & linea(campo("CABECERA")).ToString & "'")
                            drAlmacen = oAlmacenes.Data.Tables(0).Rows.OfType(Of DataRow).Where(Function(x) x("ID") = drAlmacen.First.Item("VAL_" & campo("id"))).ToArray
                            If drAlmacen.Any AndAlso drAlmacen.Count = 1 Then
                                valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(drAlmacen(0)("ID"), drAlmacen(0)("COD") & " - " & drAlmacen(0)("DEN"))
                            Else
                                erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), linea(campo("CABECERA")).ToString)))
                            End If
                        End If
                    Case TiposDeDatos.TipoCampoGS.CentroCoste 'Obtenemos todos los campos que identifican el campo. Comprobamos que esta bien introducido, es decir, por ejemplo que no se haya indicado el nivel 2 y no el 1
                        TieneCentroCosteDesglose = True
                        Select Case campo("CABECERA").ToString.ToUpper
                            Case campo("NOMBRECAMPODESGLOSE").ToString.ToUpper & " - UO1"
                                centroCosteUO1 = linea(campo("CABECERA")).ToString
                            Case campo("NOMBRECAMPODESGLOSE").ToString.ToUpper & " - UO2"
                                centroCosteUO2 = linea(campo("CABECERA")).ToString
                            Case campo("NOMBRECAMPODESGLOSE").ToString.ToUpper & " - UO3"
                                centroCosteUO3 = linea(campo("CABECERA")).ToString
                            Case Else
                                centroCosteUO4 = linea(campo("CABECERA")).ToString
                                idCampoCentroCoste = CType(campo("ID"), Integer)
                                If Not String.IsNullOrEmpty(centroCosteUO1 & centroCosteUO2 & centroCosteUO3 & centroCosteUO4) Then
                                    If (Not String.IsNullOrEmpty(centroCosteUO4) AndAlso (String.IsNullOrEmpty(centroCosteUO3) OrElse String.IsNullOrEmpty(centroCosteUO2) OrElse String.IsNullOrEmpty(centroCosteUO1))) _
                                            OrElse (Not String.IsNullOrEmpty(centroCosteUO3) AndAlso (String.IsNullOrEmpty(centroCosteUO2) OrElse String.IsNullOrEmpty(centroCosteUO1))) _
                                            OrElse (Not String.IsNullOrEmpty(centroCosteUO2) AndAlso String.IsNullOrEmpty(centroCosteUO1)) Then
                                        erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                    New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), centroCosteUO1 & " - " & centroCosteUO2 & " - " & centroCosteUO3 & " - " & centroCosteUO4)))
                                    Else
                                        oCentroCoste = FSNServer.Get_Object(GetType(FSNServer.CentrosCoste))
                                        oCentroCoste.LoadData(FSNUser.Cod, FSNUser.Idioma.ToString, True)
                                        Select Case Convert.ToInt32(Not String.IsNullOrEmpty(centroCosteUO1)) + Convert.ToInt32(Not String.IsNullOrEmpty(centroCosteUO2)) _
                                                    + Convert.ToInt32(Not String.IsNullOrEmpty(centroCosteUO3)) + Convert.ToInt32(Not String.IsNullOrEmpty(centroCosteUO4))
                                            Case 1
                                                drCentroCoste = oCentroCoste.Data.Tables(1).Rows.OfType(Of DataRow).Where(Function(x) x("COD") = centroCosteUO1).ToArray
                                            Case 2
                                                drCentroCoste = oCentroCoste.Data.Tables(2).Rows.OfType(Of DataRow).Where(Function(x) x("UON1") = centroCosteUO1 _
                                                                                                                                  AndAlso x("COD") = centroCosteUO2).ToArray
                                            Case 3
                                                drCentroCoste = oCentroCoste.Data.Tables(3).Rows.OfType(Of DataRow).Where(Function(x) x("UON1") = centroCosteUO1 _
                                                                                                                                  AndAlso x("UON2") = centroCosteUO2 _
                                                                                                                                  AndAlso x("COD") = centroCosteUO3).ToArray
                                            Case 4
                                                drCentroCoste = oCentroCoste.Data.Tables(4).Rows.OfType(Of DataRow).Where(Function(x) x("UON1") = centroCosteUO1 _
                                                                                                                                  AndAlso x("UON2") = centroCosteUO2 _
                                                                                                                                  AndAlso x("UON3") = centroCosteUO3 _
                                                                                                                                  AndAlso x("COD") = centroCosteUO4).ToArray
                                            Case Else

                                        End Select
                                        If drCentroCoste.Any AndAlso drCentroCoste.Count = 1 Then
                                            valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(
                                                    centroCosteUO1 & If(String.IsNullOrEmpty(centroCosteUO2), "", "#" & centroCosteUO2 &
                                                                        If(String.IsNullOrEmpty(centroCosteUO3), "", "#" & centroCosteUO3 &
                                                                        If(String.IsNullOrEmpty(centroCosteUO4), "", "#" & centroCosteUO4))),
                                                    drCentroCoste(0)("DEN"))
                                        Else
                                            erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                    New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), centroCosteUO1 & " - " & centroCosteUO2 & " - " & centroCosteUO3 & " - " & centroCosteUO4)))
                                        End If
                                    End If
                                End If
                        End Select
                    Case TiposDeDatos.TipoCampoGS.AnyoPartida
                        If FSNServer.TipoAcceso.gbAccesoFSSM And partidaNiv0 = String.Empty Then
                            Dim arUONS As String() = sCentroCosteAnyoImputacion.Split("#")
                            centroCosteUO1 = IIf(sCentroCosteAnyoImputacion <> String.Empty, sCentroCosteAnyoImputacion.Split("#")(0), String.Empty)
                            If arUONS.Length > 1 Then centroCosteUO2 = IIf(sCentroCosteAnyoImputacion <> String.Empty, sCentroCosteAnyoImputacion.Split("#")(1), String.Empty)
                            If arUONS.Length > 2 Then centroCosteUO3 = IIf(sCentroCosteAnyoImputacion <> String.Empty, sCentroCosteAnyoImputacion.Split("#")(2), String.Empty)
                            If arUONS.Length > 3 Then centroCosteUO4 = IIf(sCentroCosteAnyoImputacion <> String.Empty, sCentroCosteAnyoImputacion.Split("#")(3), String.Empty)

                            partidaNiv0 = IIf(sPartida0AnyoImputacion <> String.Empty, sPartida0AnyoImputacion, String.Empty)

                            sPartidaAnyoImputacion = Replace(sPartidaAnyoImputacion, sCentroCosteAnyoImputacion & "#", "")
                            If Not String.IsNullOrEmpty(sPartidaAnyoImputacion) Then
                                Dim arPartida As String() = sPartidaAnyoImputacion.Split("|")
                                partidaNiv1 = IIf(sPartidaAnyoImputacion <> String.Empty, sPartidaAnyoImputacion.Split("|")(0), String.Empty)
                                If arPartida.Length > 1 Then partidaNiv2 = IIf(sPartidaAnyoImputacion <> String.Empty, sPartidaAnyoImputacion.Split("|")(1), String.Empty)
                                If arPartida.Length > 2 Then partidaNiv3 = IIf(sPartidaAnyoImputacion <> String.Empty, sPartidaAnyoImputacion.Split("|")(2), String.Empty)
                                If arPartida.Length > 3 Then partidaNiv4 = IIf(sPartidaAnyoImputacion <> String.Empty, sPartidaAnyoImputacion.Split("|")(3), String.Empty)
                            End If
                        End If

                        If String.IsNullOrEmpty(centroCosteUO1 & If(String.IsNullOrEmpty(centroCosteUO2), "", "#" & centroCosteUO2 &
                                                If(String.IsNullOrEmpty(centroCosteUO3), "", "#" & centroCosteUO3 &
                                                If(String.IsNullOrEmpty(centroCosteUO4), "", "#" & centroCosteUO4)))) Then
                            If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                                New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), Textos(60)(1))))

                        ElseIf String.IsNullOrEmpty(partidaNiv1 & If(String.IsNullOrEmpty(partidaNiv2), "", "#" & partidaNiv2 &
                                                    If(String.IsNullOrEmpty(partidaNiv3), "", "#" & partidaNiv3 &
                                                    If(String.IsNullOrEmpty(partidaNiv4), "", "#" & partidaNiv4)))) Then
                            If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                                    New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), Textos(61)(1))))
                        Else
                            oAnyoPartida = FSNServer.Get_Object(GetType(FSNServer.PartidaPRES5))
                            Dim partida As String = centroCosteUO1 & If(String.IsNullOrEmpty(centroCosteUO2), "", "#" & centroCosteUO2 &
                                                                If(String.IsNullOrEmpty(centroCosteUO3), "", "#" & centroCosteUO3 &
                                                            If(String.IsNullOrEmpty(centroCosteUO4), "", "#" & centroCosteUO4))) &
                                                    "#" &
                                                    partidaNiv1 & If(String.IsNullOrEmpty(partidaNiv2), "", "|" & partidaNiv2 &
                                                            If(String.IsNullOrEmpty(partidaNiv3), "", "|" & partidaNiv3 &
                                                            If(String.IsNullOrEmpty(partidaNiv4), "", "|" & partidaNiv4)))
                            oAnyoPartida.LoadDataAnyoPartida(partidaNiv0, partida)
                            If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                                drAnyoPartida = oAnyoPartida.DataAnyoPartida.Tables(0).Rows.OfType(Of DataRow).Where(Function(x) x("ANYO") = linea(campo("CABECERA")).ToString).ToArray
                                If drAnyoPartida.Any AndAlso drAnyoPartida.Count = 1 Then
                                    valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(linea(campo("CABECERA")).ToString, linea(campo("CABECERA")).ToString)
                                Else
                                    erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                    New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), linea(campo("CABECERA")).ToString)))
                                End If
                            ElseIf oAnyoPartida.DataAnyoPartida.Tables(0).Rows.Count = 1 Then
                                valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(oAnyoPartida.DataAnyoPartida.Tables(0).Rows(0)("ANYO"), oAnyoPartida.DataAnyoPartida.Tables(0).Rows(0)("ANYO"))
                            End If
                        End If

                    Case TiposDeDatos.TipoCampoGS.Partida 'Obtenemos todos los campos que identifican el campo. Comprobamos que esta bien introducido, es decir, por ejemplo que no se haya indicado el nivel 2 y no el 1
                        If FSNServer.TipoAcceso.gbAccesoFSSM And centroCosteUO1 = String.Empty Then
                            Dim arUONS As String() = sCentroCostePartida.Split("#")
                            centroCosteUO1 = IIf(sCentroCostePartida <> String.Empty, sCentroCostePartida.Split("#")(0), String.Empty)
                            If arUONS.Length > 1 Then centroCosteUO2 = IIf(sCentroCostePartida <> String.Empty, sCentroCostePartida.Split("#")(1), String.Empty)
                            If arUONS.Length > 2 Then centroCosteUO3 = IIf(sCentroCostePartida <> String.Empty, sCentroCostePartida.Split("#")(2), String.Empty)
                            If arUONS.Length > 3 Then centroCosteUO4 = IIf(sCentroCostePartida <> String.Empty, sCentroCostePartida.Split("#")(3), String.Empty)
                        End If

                        Dim TextoNoCentro As String = ""
                        If String.IsNullOrEmpty(centroCosteUO1 & If(String.IsNullOrEmpty(centroCosteUO2), "", "#" & centroCosteUO2 &
                                                If(String.IsNullOrEmpty(centroCosteUO3), "", "#" & centroCosteUO3 &
                                                If(String.IsNullOrEmpty(centroCosteUO4), "", "#" & centroCosteUO4)))) Then
                            TextoNoCentro = Textos(59)(1)
                        End If

                        Dim DenPartida As String = ""
                        Dim CodigoCompletadoCentro As String = ""
                        Dim DescripCodigoCompletadoCentro As String = ""
                        oPRES5 = FSNServer.Get_Object(GetType(FSNServer.PRES5))
                        Dim NivelPartida As Integer = oPRES5.DameNivelAceptablePartida(campo("PRES5"))
                        partidaNiv0 = campo("PRES5")
                        Dim bPlurianual As Boolean = False
                        If Not Session("FSSMPargen") Is Nothing Then
                            Dim configuracion As FSNServer.PargenSM
                            configuracion = Session("FSSMPargen").Item(partidaNiv0)
                            If Not configuracion Is Nothing Then
                                bPlurianual = configuracion.Plurianual
                            End If
                        End If
                        Select Case campo("CABECERA").ToString.ToUpper
                            Case campo("NOMBRECAMPODESGLOSE").ToString.ToUpper & " - PRES5_NIV1"
                                partidaNiv1 = linea(campo("CABECERA")).ToString
                                If NivelPartida = 1 Then
                                    If Not GenerarValoresDesglose_Validaciones_Partida(partidaNiv1, partidaNiv2, partidaNiv3, partidaNiv4, centroCosteUO1, centroCosteUO2, centroCosteUO3, centroCosteUO4, campo("PRES5"), DenPartida, bPlurianual _
                                                                                       , TieneCentroCosteDesglose, CodigoCompletadoCentro, DescripCodigoCompletadoCentro) Then
                                        If Not String.IsNullOrEmpty(partidaNiv1 & partidaNiv2 & partidaNiv3 & partidaNiv4) Then erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                    New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), partidaNiv1 & If(String.IsNullOrEmpty(partidaNiv2), "", " - " & partidaNiv2 &
                                                        If(String.IsNullOrEmpty(partidaNiv3), "", " - " & partidaNiv3 &
                                                        If(String.IsNullOrEmpty(partidaNiv4), "", " - " & partidaNiv4))) & TextoNoCentro)))
                                    ElseIf Not String.IsNullOrEmpty(CodigoCompletadoCentro) Then
                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(CodigoCompletadoCentro &
                                                "#" &
                                                partidaNiv1 & If(String.IsNullOrEmpty(partidaNiv2), "", "|" & partidaNiv2 &
                                                        If(String.IsNullOrEmpty(partidaNiv3), "", "|" & partidaNiv3 &
                                                        If(String.IsNullOrEmpty(partidaNiv4), "", "|" & partidaNiv4))),
                                                DenPartida)

                                        'Para q anyo coja el centro del desglose
                                        Dim arUONS As String() = CodigoCompletadoCentro.Split("#")
                                        centroCosteUO1 = IIf(CodigoCompletadoCentro <> String.Empty, CodigoCompletadoCentro.Split("#")(0), String.Empty)
                                        If arUONS.Length > 1 Then centroCosteUO2 = IIf(CodigoCompletadoCentro <> String.Empty, CodigoCompletadoCentro.Split("#")(1), String.Empty)
                                        If arUONS.Length > 2 Then centroCosteUO3 = IIf(CodigoCompletadoCentro <> String.Empty, CodigoCompletadoCentro.Split("#")(2), String.Empty)
                                        If arUONS.Length > 3 Then centroCosteUO4 = IIf(CodigoCompletadoCentro <> String.Empty, CodigoCompletadoCentro.Split("#")(3), String.Empty)
                                        'Para poner en el desglose el centro q se ha determinado
                                        valoresLinea(CType(idCampoCentroCoste, Integer).ToString) = New KeyValuePair(Of String, String)(CodigoCompletadoCentro, DescripCodigoCompletadoCentro)

                                    Else
                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(
                                                centroCosteUO1 & If(String.IsNullOrEmpty(centroCosteUO2), "", "#" & centroCosteUO2 &
                                                        If(String.IsNullOrEmpty(centroCosteUO3), "", "#" & centroCosteUO3 &
                                                        If(String.IsNullOrEmpty(centroCosteUO4), "", "#" & centroCosteUO4))) &
                                                "#" &
                                                partidaNiv1 & If(String.IsNullOrEmpty(partidaNiv2), "", "|" & partidaNiv2 &
                                                        If(String.IsNullOrEmpty(partidaNiv3), "", "|" & partidaNiv3 &
                                                        If(String.IsNullOrEmpty(partidaNiv4), "", "|" & partidaNiv4))),
                                                DenPartida)
                                    End If
                                End If
                            Case campo("NOMBRECAMPODESGLOSE").ToString.ToUpper & " - PRES5_NIV2"
                                partidaNiv2 = linea(campo("CABECERA")).ToString
                                If NivelPartida = 2 Then
                                    If Not GenerarValoresDesglose_Validaciones_Partida(partidaNiv1, partidaNiv2, partidaNiv3, partidaNiv4, centroCosteUO1, centroCosteUO2, centroCosteUO3, centroCosteUO4, campo("PRES5"), DenPartida, bPlurianual _
                                                                                       , TieneCentroCosteDesglose, CodigoCompletadoCentro, DescripCodigoCompletadoCentro) Then
                                        If Not String.IsNullOrEmpty(partidaNiv1 & partidaNiv2 & partidaNiv3 & partidaNiv4) Then erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                    New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), partidaNiv1 & If(String.IsNullOrEmpty(partidaNiv2), "", " - " & partidaNiv2 &
                                                    If(String.IsNullOrEmpty(partidaNiv3), "", " - " & partidaNiv3 &
                                                    If(String.IsNullOrEmpty(partidaNiv4), "", " - " & partidaNiv4))) & TextoNoCentro)))
                                    ElseIf Not String.IsNullOrEmpty(CodigoCompletadoCentro) Then
                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(CodigoCompletadoCentro &
                                                "#" &
                                                partidaNiv1 & If(String.IsNullOrEmpty(partidaNiv2), "", "|" & partidaNiv2 &
                                                        If(String.IsNullOrEmpty(partidaNiv3), "", "|" & partidaNiv3 &
                                                        If(String.IsNullOrEmpty(partidaNiv4), "", "|" & partidaNiv4))),
                                                DenPartida)

                                        'Para q anyo coja el centro del desglose
                                        Dim arUONS As String() = CodigoCompletadoCentro.Split("#")
                                        centroCosteUO1 = IIf(CodigoCompletadoCentro <> String.Empty, CodigoCompletadoCentro.Split("#")(0), String.Empty)
                                        If arUONS.Length > 1 Then centroCosteUO2 = IIf(CodigoCompletadoCentro <> String.Empty, CodigoCompletadoCentro.Split("#")(1), String.Empty)
                                        If arUONS.Length > 2 Then centroCosteUO3 = IIf(CodigoCompletadoCentro <> String.Empty, CodigoCompletadoCentro.Split("#")(2), String.Empty)
                                        If arUONS.Length > 3 Then centroCosteUO4 = IIf(CodigoCompletadoCentro <> String.Empty, CodigoCompletadoCentro.Split("#")(3), String.Empty)
                                        'Para poner en el desglose el centro q se ha determinado
                                        valoresLinea(CType(idCampoCentroCoste, Integer).ToString) = New KeyValuePair(Of String, String)(CodigoCompletadoCentro, DescripCodigoCompletadoCentro)

                                    Else
                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(
                                            centroCosteUO1 & If(String.IsNullOrEmpty(centroCosteUO2), "", "#" & centroCosteUO2 &
                                                    If(String.IsNullOrEmpty(centroCosteUO3), "", "#" & centroCosteUO3 &
                                                    If(String.IsNullOrEmpty(centroCosteUO4), "", "#" & centroCosteUO4))) &
                                            "#" &
                                            partidaNiv1 & If(String.IsNullOrEmpty(partidaNiv2), "", "|" & partidaNiv2 &
                                                    If(String.IsNullOrEmpty(partidaNiv3), "", "|" & partidaNiv3 &
                                                    If(String.IsNullOrEmpty(partidaNiv4), "", "|" & partidaNiv4))),
                                            DenPartida)
                                    End If
                                End If
                            Case campo("NOMBRECAMPODESGLOSE").ToString.ToUpper & " - PRES5_NIV3"
                                partidaNiv3 = linea(campo("CABECERA")).ToString
                                If NivelPartida = 3 Then
                                    If Not GenerarValoresDesglose_Validaciones_Partida(partidaNiv1, partidaNiv2, partidaNiv3, partidaNiv4, centroCosteUO1, centroCosteUO2, centroCosteUO3, centroCosteUO4, campo("PRES5"), DenPartida, bPlurianual _
                                                                                       , TieneCentroCosteDesglose, CodigoCompletadoCentro, DescripCodigoCompletadoCentro) Then
                                        If Not String.IsNullOrEmpty(partidaNiv1 & partidaNiv2 & partidaNiv3 & partidaNiv4) Then erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                    New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), partidaNiv1 & If(String.IsNullOrEmpty(partidaNiv2), "", " - " & partidaNiv2 &
                                                        If(String.IsNullOrEmpty(partidaNiv3), "", " - " & partidaNiv3 &
                                                        If(String.IsNullOrEmpty(partidaNiv4), "", " - " & partidaNiv4))) & TextoNoCentro)))
                                    ElseIf Not String.IsNullOrEmpty(CodigoCompletadoCentro) Then
                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(CodigoCompletadoCentro &
                                                "#" &
                                                partidaNiv1 & If(String.IsNullOrEmpty(partidaNiv2), "", "|" & partidaNiv2 &
                                                        If(String.IsNullOrEmpty(partidaNiv3), "", "|" & partidaNiv3 &
                                                        If(String.IsNullOrEmpty(partidaNiv4), "", "|" & partidaNiv4))),
                                                DenPartida)

                                        'Para q anyo coja el centro del desglose
                                        Dim arUONS As String() = CodigoCompletadoCentro.Split("#")
                                        centroCosteUO1 = IIf(CodigoCompletadoCentro <> String.Empty, CodigoCompletadoCentro.Split("#")(0), String.Empty)
                                        If arUONS.Length > 1 Then centroCosteUO2 = IIf(CodigoCompletadoCentro <> String.Empty, CodigoCompletadoCentro.Split("#")(1), String.Empty)
                                        If arUONS.Length > 2 Then centroCosteUO3 = IIf(CodigoCompletadoCentro <> String.Empty, CodigoCompletadoCentro.Split("#")(2), String.Empty)
                                        If arUONS.Length > 3 Then centroCosteUO4 = IIf(CodigoCompletadoCentro <> String.Empty, CodigoCompletadoCentro.Split("#")(3), String.Empty)
                                        'Para poner en el desglose el centro q se ha determinado
                                        valoresLinea(CType(idCampoCentroCoste, Integer).ToString) = New KeyValuePair(Of String, String)(CodigoCompletadoCentro, DescripCodigoCompletadoCentro)

                                    Else
                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(
                                                centroCosteUO1 & If(String.IsNullOrEmpty(centroCosteUO2), "", "#" & centroCosteUO2 &
                                                        If(String.IsNullOrEmpty(centroCosteUO3), "", "#" & centroCosteUO3 &
                                                        If(String.IsNullOrEmpty(centroCosteUO4), "", "#" & centroCosteUO4))) &
                                                "#" &
                                                partidaNiv1 & If(String.IsNullOrEmpty(partidaNiv2), "", "|" & partidaNiv2 &
                                                        If(String.IsNullOrEmpty(partidaNiv3), "", "|" & partidaNiv3 &
                                                        If(String.IsNullOrEmpty(partidaNiv4), "", "|" & partidaNiv4))),
                                                DenPartida)
                                    End If
                                End If
                            Case Else
                                partidaNiv4 = linea(campo("CABECERA")).ToString
                                If NivelPartida = 4 Then
                                    If Not GenerarValoresDesglose_Validaciones_Partida(partidaNiv1, partidaNiv2, partidaNiv3, partidaNiv4, centroCosteUO1, centroCosteUO2, centroCosteUO3, centroCosteUO4, campo("PRES5"), DenPartida, bPlurianual _
                                                                                       , TieneCentroCosteDesglose, CodigoCompletadoCentro, DescripCodigoCompletadoCentro) Then
                                        If Not String.IsNullOrEmpty(partidaNiv1 & partidaNiv2 & partidaNiv3 & partidaNiv4) Then erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), partidaNiv1 & If(String.IsNullOrEmpty(partidaNiv2), "", " - " & partidaNiv2 &
                                                    If(String.IsNullOrEmpty(partidaNiv3), "", " - " & partidaNiv3 &
                                                    If(String.IsNullOrEmpty(partidaNiv4), "", " - " & partidaNiv4))) & TextoNoCentro)))
                                    ElseIf Not String.IsNullOrEmpty(CodigoCompletadoCentro) Then
                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(CodigoCompletadoCentro &
                                                "#" &
                                                partidaNiv1 & If(String.IsNullOrEmpty(partidaNiv2), "", "|" & partidaNiv2 &
                                                        If(String.IsNullOrEmpty(partidaNiv3), "", "|" & partidaNiv3 &
                                                        If(String.IsNullOrEmpty(partidaNiv4), "", "|" & partidaNiv4))),
                                                DenPartida)

                                        'Para q anyo coja el centro del desglose
                                        Dim arUONS As String() = CodigoCompletadoCentro.Split("#")
                                        centroCosteUO1 = IIf(CodigoCompletadoCentro <> String.Empty, CodigoCompletadoCentro.Split("#")(0), String.Empty)
                                        If arUONS.Length > 1 Then centroCosteUO2 = IIf(CodigoCompletadoCentro <> String.Empty, CodigoCompletadoCentro.Split("#")(1), String.Empty)
                                        If arUONS.Length > 2 Then centroCosteUO3 = IIf(CodigoCompletadoCentro <> String.Empty, CodigoCompletadoCentro.Split("#")(2), String.Empty)
                                        If arUONS.Length > 3 Then centroCosteUO4 = IIf(CodigoCompletadoCentro <> String.Empty, CodigoCompletadoCentro.Split("#")(3), String.Empty)
                                        'Para poner en el desglose el centro q se ha determinado
                                        valoresLinea(CType(idCampoCentroCoste, Integer).ToString) = New KeyValuePair(Of String, String)(CodigoCompletadoCentro, DescripCodigoCompletadoCentro)

                                    Else
                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(
                                            centroCosteUO1 & If(String.IsNullOrEmpty(centroCosteUO2), "", "#" & centroCosteUO2 &
                                                    If(String.IsNullOrEmpty(centroCosteUO3), "", "#" & centroCosteUO3 &
                                                    If(String.IsNullOrEmpty(centroCosteUO4), "", "#" & centroCosteUO4))) &
                                            "#" &
                                            partidaNiv1 & If(String.IsNullOrEmpty(partidaNiv2), "", "|" & partidaNiv2 &
                                                    If(String.IsNullOrEmpty(partidaNiv3), "", "|" & partidaNiv3 &
                                                    If(String.IsNullOrEmpty(partidaNiv4), "", "|" & partidaNiv4))),
                                            DenPartida)
                                    End If
                                End If

                        End Select
                    Case TiposDeDatos.TipoCampoGS.Activo
                        If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                            If (Not String.IsNullOrEmpty(centroCosteUO4) AndAlso (String.IsNullOrEmpty(centroCosteUO3) OrElse String.IsNullOrEmpty(centroCosteUO2) OrElse String.IsNullOrEmpty(centroCosteUO1))) _
                                        OrElse (Not String.IsNullOrEmpty(centroCosteUO3) AndAlso (String.IsNullOrEmpty(centroCosteUO2) OrElse String.IsNullOrEmpty(centroCosteUO1))) _
                                        OrElse (Not String.IsNullOrEmpty(centroCosteUO2) AndAlso String.IsNullOrEmpty(centroCosteUO1)) Then 'El centro de coste esta bien, sino error
                                erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                            New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), linea(campo("CABECERA")).ToString)))
                            Else
                                oActivo = FSNServer.Get_Object(GetType(FSNServer.Activos))
                                oActivo.ObtenerActivos_CC_Imputacion(FSNUser.Idioma.ToString, FSNUser.Cod, centroCosteUO1, centroCosteUO2, centroCosteUO3, centroCosteUO4)
                                drActivo = oActivo.Data.Rows.OfType(Of DataRow).Where(Function(x) x("COD") = linea(campo("CABECERA")).ToString).ToArray
                                If drActivo.Any AndAlso drActivo.Count = 1 Then
                                    valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(linea(campo("CABECERA")).ToString, drActivo(0)("COD") & " - " & drActivo(0)("DEN_ACTIVO"))
                                    valoresLinea.Remove(idCampoCentroCoste)
                                    valoresLinea(idCampoCentroCoste.ToString) = New KeyValuePair(Of String, String)(drActivo(0)("UONS"), drActivo(0)("ID_DEN_CENTRO"))
                                Else
                                    erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                            New KeyValuePair(Of String, String)(campo("NOMBRECAMPODESGLOSE"), linea(campo("CABECERA")).ToString)))
                                End If
                            End If
                        End If
                    Case TiposDeDatos.TipoCampoGS.Pais
                        If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                            drPais = dsLecturaHojaExcel.Tables("DATOSCAMPOSGS").Select("DEN_" & campo("ID") & "='" & linea(campo("CABECERA")).ToString & "'").First
                            paisCod = drPais("VAL_" & campo("id")) 'Guardamos en una variable el valor del pais por si hay luego un campo provincia
                            valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(paisCod, linea(campo("CABECERA")).ToString)
                        End If
                    Case TiposDeDatos.TipoCampoGS.Provincia
                        If Not String.IsNullOrEmpty(paisCod) AndAlso Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                            provinciaValueIndex = dsLecturaHojaExcel.Tables("DATOSPROVINCIAS").Rows.IndexOf(dsLecturaHojaExcel.Tables("DATOSPROVINCIAS").Select(paisCod & "='" & linea(campo("CABECERA")).ToString & "'").First)
                            provinciaCod = dsLecturaHojaExcel.Tables("DATOSPROVINCIASVALUES").Rows(provinciaValueIndex)(paisCod)
                            valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(provinciaCod, linea(campo("CABECERA")).ToString)
                        End If
                    Case TiposDeDatos.TipoCampoGS.FormaPago, TiposDeDatos.TipoCampoGS.Moneda, TiposDeDatos.TipoCampoGS.Unidad, TiposDeDatos.TipoCampoGS.UnidadPedido
                        If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                            If DBNullToInteger(campo("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Unidad Then idCampoUnidad = CType(campo("ID"), Integer).ToString
                            valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(
                                    dsLecturaHojaExcel.Tables("DATOSCAMPOSGS").Select("DEN_" & campo("ID") & "='" & linea(campo("CABECERA")).ToString & "'").First.Item("VAL_" & campo("ID")), linea(campo("CABECERA")).ToString)
                        End If
                    Case TiposDeDatos.TipoCampoGS.PrecioUnitario
                        idCampoPrecioUnitario = CType(campo("ID"), Integer)
                        If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                            Dim _DecimalFmt As String = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator
                            Dim sAux As String = linea(campo("CABECERA")).ToString
                            If InStr(sAux, FSNUser.NumberFormat.NumberDecimalSeparator.ToString) = 0 Then
                                sAux = sAux.Replace(_DecimalFmt, FSNUser.NumberFormat.NumberDecimalSeparator.ToString)
                                valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(sAux, sAux)
                            Else
                                valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(linea(campo("CABECERA")).ToString, linea(campo("CABECERA")).ToString)
                            End If
                        End If
                    Case Else 'TiposDeDatos.TipoCampoGS.SinTipo 
                        If Not String.IsNullOrEmpty(linea(campo("CABECERA")).ToString) Then
                            Select Case DBNullToInteger(campo("SUBTIPO"))
                                Case TiposDeDatos.TipoGeneral.TipoNumerico
                                    Dim _DecimalFmt As String = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator
                                    Dim sAux As String = linea(campo("CABECERA")).ToString
                                    If InStr(sAux, FSNUser.NumberFormat.NumberDecimalSeparator.ToString) = 0 Then

                                        sAux = sAux.Replace(_DecimalFmt, FSNUser.NumberFormat.NumberDecimalSeparator.ToString)
                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(sAux, sAux)
                                    Else
                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(linea(campo("CABECERA")).ToString, linea(campo("CABECERA")).ToString)
                                    End If
                                Case TiposDeDatos.TipoGeneral.TipoFecha
                                    Dim _UsuDateFormat As String = FSNUser.DateFormat.ShortDatePattern.ToString

                                    Dim d As Integer = Day(linea(campo("CABECERA")))
                                    Dim m As Integer = Month(linea(campo("CABECERA")))
                                    Dim y As Integer = Year(linea(campo("CABECERA")))

                                    Dim sAux As String
                                    If LCase(Left(_UsuDateFormat, 2)) = "mm" Then
                                        sAux = CStr(m) & "/" & CStr(d) & "/" & CStr(y)
                                    Else
                                        sAux = CStr(d) & "/" & CStr(m) & "/" & CStr(y)
                                    End If

                                    valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(sAux, sAux)
                                Case TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoTextoLargo
                                    If campo("INTRO") = 1 Then
                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(DBNullToInteger(dsLecturaHojaExcel.Tables("DATOSCAMPOSLISTA").Select("VAL_" & campo("ID") & "='" & linea(campo("CABECERA")).ToString & "'").First.Item("ORD_" & campo("ID"))), linea(campo("CABECERA")).ToString)
                                    Else
                                        valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(linea(campo("CABECERA")).ToString, linea(campo("CABECERA")).ToString)
                                    End If
                                Case TiposDeDatos.TipoGeneral.TipoBoolean
                                    valoresLinea(CType(campo("ID"), Integer).ToString) = New KeyValuePair(Of String, String)(If(linea(campo("CABECERA")).ToString.ToUpper = Textos(15)(1).ToString.ToUpper, "1", "0"), linea(campo("CABECERA")).ToString)
                            End Select
                        End If
                End Select
            Next
            If ValidarArticuloLinea Then 'Si tenemos que validar el articulo de la linea anterior (lo hacemos tras recorrer todos los campos porque necesitamos orgcompras, centro y prove)
                If FSNServer.TipoAcceso.gbUsar_OrgCompras Then
                    If Not TieneOrgComprasDesglose Then organizacionCompras = IIf(String.IsNullOrEmpty(organizacionCompras), IIf(String.IsNullOrEmpty(orgComprasOutDesglose), Nothing, orgComprasOutDesglose), organizacionCompras)
                    If Not TieneCentroDesglose Then centro = IIf(String.IsNullOrEmpty(centro), IIf(String.IsNullOrEmpty(centroOutDesglose), Nothing, centroOutDesglose), centro)
                End If
                If Not TieneUnidadOrganizativaDesglose And UnidadOrganizativaOutDesglose <> String.Empty Then
                    Dim arUONs As String() = UnidadOrganizativaOutDesglose.Split(New String() {" - "}, StringSplitOptions.None)
                    UON1 = arUONs(0)
                    UON2 = arUONs(1)
                    UON3 = arUONs(2)
                End If
                If FSNServer.TipoAcceso.gbAccesoFSSM And UON1 = String.Empty Then
                    'Si hay SM y no hay UONs se toman las del centro de coste (si hay)
                    Dim arUONS As String() = sCentroCostePartida.Split("#")
                    UON1 = IIf(TieneCentroCosteDesglose, centroCosteUO1, IIf(sCentroCostePartida <> String.Empty, sCentroCostePartida.Split("#")(0), String.Empty))
                    If arUONS.Length > 1 Then UON2 = IIf(TieneCentroCosteDesglose, centroCosteUO2, IIf(sCentroCostePartida <> String.Empty, sCentroCostePartida.Split("#")(1), String.Empty))
                    If arUONS.Length > 2 Then UON3 = IIf(TieneCentroCosteDesglose, centroCosteUO3, IIf(sCentroCostePartida <> String.Empty, sCentroCostePartida.Split("#")(2), String.Empty))
                End If
                oArticulo = FSNServer.Get_Object(GetType(FSNServer.Articulos))
                If Not {TiposDeDatos.TipoDeSolicitud.PedidoExpress, TiposDeDatos.TipoDeSolicitud.PedidoNegociado}.Contains(iTipoSolicitud) Then
                    oArticulo.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil, sCod:=infoArticuloValidado.valor, bCoincid:=True, sIdioma:=FSNUser.Idioma,
                                        bCargarUltAdj:=True, sCodOrgCompras:=organizacionCompras, sCodCentro:=centro, UsarOrgCompras:=FSNServer.TipoAcceso.gbUsar_OrgCompras,
                                        RestringirMaterialUsuario:=AccionesDeSeguridad.PMRestringirMaterialAsignadoUsuario, UON1:=UON1, UON2:=UON2, UON3:=UON3, TopAutoComplete:=1,
                                       CodProveedorSumiArt:=sProveSuministrador, AccesoFSEP:=FSNServer.TipoAcceso.gbAccesoFSEP)
                Else
                    Dim iFiltrarNegociados As Integer
                    Dim bCargarUltAdj As Boolean
                    If iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoNegociado Then
                        bCargarUltAdj = True
                        iFiltrarNegociados = 1 'FILTRA POR ARTÍCULOS NEGOCIADOS                

                        oArticulo.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil, sCod:=infoArticuloValidado.valor, bCoincid:=True, sIdioma:=FSNUser.Idioma,
                                            bCargarUltAdj:=bCargarUltAdj, sCodOrgCompras:=organizacionCompras, sCodCentro:=centro, CodProveedor:=proveedor, Negociados:=iFiltrarNegociados,
                                            RestringirPedidosArticulosUONUsuario:=FSNUser.RestringirPedidosArticulosUONUsuario, RestringirPedidosArticulosUONPerfil:=FSNUser.RestringirPedidosArticulosUONPerfil,
                                            UsarOrgCompras:=FSNServer.TipoAcceso.gbUsar_OrgCompras, AccesoFSSM:=FSNServer.TipoAcceso.gbAccesoFSSM, RestringirMaterialUsuario:=AccionesDeSeguridad.PMRestringirMaterialAsignadoUsuario,
                                            UON1:=UON1, UON2:=UON2, UON3:=UON3, TopAutoComplete:=1, CodProveedorSumiArt:=sProveSuministrador, AccesoFSEP:=FSNServer.TipoAcceso.gbAccesoFSEP)
                    ElseIf iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoExpress Then
                        bCargarUltAdj = False
                        If FSNServer.TipoAcceso.gbArticulosNegociadosPedExpress Then
                            bCargarUltAdj = True
                            iFiltrarNegociados = 3 'FILTRA POR ARTÍCULOS NEGOCIADOS O NO
                        Else
                            iFiltrarNegociados = 2 'FILTRA ARTICULOS NO NEGOCIADOS
                        End If

                        oArticulo.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil, sCod:=infoArticuloValidado.valor.ToString, bCoincid:=True, sIdioma:=FSNUser.Idioma,
                                            bCargarUltAdj:=bCargarUltAdj, sCodOrgCompras:=organizacionCompras, sCodCentro:=centro, CodProveedor:=proveedor, Negociados:=iFiltrarNegociados,
                                            UsarOrgCompras:=FSNServer.TipoAcceso.gbUsar_OrgCompras, RestringirMaterialUsuario:=AccionesDeSeguridad.PMRestringirMaterialAsignadoUsuario, TopAutoComplete:=1,
                                           CodProveedorSumiArt:=sProveSuministrador, AccesoFSEP:=FSNServer.TipoAcceso.gbAccesoFSEP)
                    End If
                End If

                If Not oArticulo.Data.Tables(0).Rows.Count = 1 Then 'No hay articulo con ese codigo. 'Si tiene la opcion "Permitir códigos que no estén en el maestro de artículos" se trasladarán el Material y la Denominación
                    If infoArticuloValidado.anyadirArticulo Then
                        valoresLinea(infoArticuloValidado.id.ToString) = New KeyValuePair(Of String, String)(infoArticuloValidado.valor.ToString, infoArticuloValidado.valor.ToString)
                    Else 'NO tiene opcion de codigos que no estan en el maestro, por lo que el valor de codigo, denominacion de articulo y el material seran vacios
                        valoresLinea.Remove(idCampoMaterial)
                        erroresLinea.Add(New KeyValuePair(Of String, KeyValuePair(Of String, String))(dsLecturaHojaExcel.Tables("LINEAS").Rows.IndexOf(linea) + 1,
                                                                                                        New KeyValuePair(Of String, String)(infoArticuloValidado.nombreCampoDesglose, infoArticuloValidado.valor.ToString)))
                    End If
                Else
                    With oArticulo.Data.Tables(0).Rows(0)
                        'Añadimos los valores de codigo de articulo
                        valoresLinea(infoArticuloValidado.id.ToString) = New KeyValuePair(Of String, String)(oArticulo.Data.Tables(0).Rows(0).Item("COD").ToString, oArticulo.Data.Tables(0).Rows(0).Item("COD").ToString)
                        'Establecemos el valor para el campo material independientemente de lo que se haya indicado en el excel
                        'Por si se ha añadido anteriormente los valores para el material los borramos
                        If Not idCampoMaterial = 0 Then 'Si hay campo material lo borramos y actualizamos
                            valoresLinea.Remove(idCampoMaterial)

                            Dim iLongCod1 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                            Dim iLongCod2 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                            Dim iLongCod3 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                            Dim iLongCod4 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN4

                            GMN1Cod = .Item("GMN1")
                            GMN2Cod = .Item("GMN2")
                            GMN3Cod = .Item("GMN3")
                            GMN4Cod = .Item("GMN4")

                            If Len(GMN1Cod) < iLongCod1 Then GMN1Cod = Space(iLongCod1 - Len(GMN1Cod)) & GMN1Cod
                            If Len(GMN2Cod) < iLongCod2 Then GMN2Cod = Space(iLongCod2 - Len(GMN2Cod)) & GMN2Cod
                            If Len(GMN3Cod) < iLongCod3 Then GMN3Cod = Space(iLongCod3 - Len(GMN3Cod)) & GMN3Cod
                            If Len(GMN4Cod) < iLongCod4 Then GMN4Cod = Space(iLongCod4 - Len(GMN4Cod)) & GMN4Cod

                            valoresLinea(idCampoMaterial) = New KeyValuePair(Of String, String)(GMN1Cod & GMN2Cod & GMN3Cod & GMN4Cod,
                                            If(String.IsNullOrEmpty(.Item("GMN4")), If(String.IsNullOrEmpty(.Item("GMN3")), If(String.IsNullOrEmpty(.Item("GMN2")), .Item("GMN1"), .Item("GMN2")), .Item("GMN3")), .Item("GMN4")) & " - " & .Item("DENGMN"))
                        End If
                        'Si no hay campo unidad (significa que era de solo lectura) lo cargamos del articulo
                        If idCampoUnidad = 0 Then valoresLinea(idCampoUnidadArticulo) = New KeyValuePair(Of String, String)(.Item("UNI"), .Item("DENUNI"))
                        If Not CType(.Item("GENERICO"), Boolean) Then 'Si no es generico, partiendo de la info del codigo de articulo cargamos los campos material,den articulo
                            If Not idCampoDenArticulo = 0 Then 'Significa que existe campo denominacion articulo. Borramos lo que hay y cargamos la del articulo
                                valoresLinea.Remove(idCampoDenArticulo)
                                valoresLinea(idCampoDenArticulo) = New KeyValuePair(Of String, String)(.Item("DEN"), .Item("DEN"))
                            End If
                            'Cargamos todos los campos que tengan marcada la opcion de cargar de la ultima adjudicacion
                            For Each dr As DataRow In dsLecturaHojaExcel.Tables("CONFIGCAMPOS").Rows.OfType(Of DataRow).Where(Function(x) DBNullToInteger(x("CARGAR_ULT_ADJ")) = 1).ToList
                                Select Case DBNullToInteger(dr("TIPO_CAMPO_GS"))
                                    Case TiposDeDatos.TipoCampoGS.PrecioUnitario
                                        If Not idCampoPrecioUnitario = 0 AndAlso Not IsDBNull(.Item("PREC_ULT_ADJ")) Then
                                            valoresLinea.Remove(idCampoPrecioUnitario)
                                        End If
                                    Case TiposDeDatos.TipoCampoGS.Proveedor
                                        If Not idCampoProveedor = 0 AndAlso Not IsDBNull(.Item("CODPROV_ULT_ADJ")) Then
                                            valoresLinea.Remove(idCampoProveedor)
                                            valoresLinea(idCampoProveedor.ToString) = New KeyValuePair(Of String, String)(.Item("CODPROV_ULT_ADJ").ToString, .Item("CODPROV_ULT_ADJ").ToString & " - " & .Item("DENPROV_ULT_ADJ").ToString)
                                        End If
                                    Case Else

                                End Select
                            Next
                        ElseIf DenArticulo = "" Then 'Gestamp Procesos / 2016 / 2669: Cargan un articulo generico SIN denominaciÃ³n. Se queda vacio en el desglose. No, se mete la denominaciÃ³n generica.
                            If Not idCampoDenArticulo = 0 Then 'Significa que existe campo denominaciÃ³n articulo. Borramos lo que hay y cargamos la del articulo
                                valoresLinea.Remove(idCampoDenArticulo)
                                valoresLinea(idCampoDenArticulo) = New KeyValuePair(Of String, String)(.Item("DEN"), .Item("DEN"))
                            End If
                        End If
                    End With
                End If
            End If
            valoresLineasCamposDesglose.Add(valoresLinea)
            DenArticulo = String.Empty
        Next
    End Sub
#End Region
#Region "Utilidades"
    ''' <summary>
    ''' De los atributos del fichero elimina el solo lectura para poder editarlo
    ''' </summary>
    ''' <param name="attributes">atributos del fichero</param>
    ''' <returns></returns>
    Private Function RemoveReadOnlyAttribute(ByVal attributes As FileAttributes) As FileAttributes
		Return attributes And Not FileAttributes.ReadOnly
	End Function
	''' <summary>
	''' 
	''' </summary>
	''' <param name="cabeceraLineas">cadena con todos los campos a crear en el excel</param>
	''' <param name="CabeceraCampo">Nombre del campo</param>
	''' <param name="tipoCampo">tipo campo (string,float..)</param>
	Private Function CrearCadenaCamposExcel(ByRef cabeceraLineas As String, ByVal CabeceraCampo As String, ByVal tipoCampo As String) As String
		Dim nombreCabeceraCampo As String = String.Empty

		Dim i As Integer = listNombresCabecera.Where(Function(x) x = CabeceraCampo).Count
		nombreCabeceraCampo = Left(CabeceraCampo & If(i = 0, "", "(" & i & ")"), 60)
		listNombresCabecera.Add(CabeceraCampo)

		cabeceraLineas &= If(String.IsNullOrEmpty(cabeceraLineas), "", ",") & "[" & nombreCabeceraCampo & "] " & tipoCampo
		Return nombreCabeceraCampo
	End Function
	Private Sub ActualizarPlantilla()
		Dim excelConnectionPlantilla As OleDb.OleDbConnection = Nothing
		Dim objCommandPlantilla As New OleDb.OleDbCommand()
		Try
			Dim sConnectionStringPlantilla As String = String.Format(ConfigurationManager.AppSettings("ExcelConnectionString"), Server.MapPath(".") & "\PLANTILLA_DESGLOSE_MATERIAL.xls")
			'Quitamos la propiedad de solo lectura a la copia realizada
			Dim attributes As FileAttributes = File.GetAttributes(Server.MapPath(".") & "\PLANTILLA_DESGLOSE_MATERIAL.xls")
			Dim attributesReadOnly As FileAttributes = RemoveReadOnlyAttribute(attributes)
			File.SetAttributes(Server.MapPath(".") & "\PLANTILLA_DESGLOSE_MATERIAL.xls", attributesReadOnly)

			'Establecer una conexion con el origen de datos.
			excelConnectionPlantilla = New OleDb.OleDbConnection(sConnectionStringPlantilla)
			excelConnectionPlantilla.Open()
			objCommandPlantilla.Connection = excelConnectionPlantilla

			Exportar_Provincias_Por_Pais(objCommandPlantilla, oPaises, FSNUser.Idioma.ToString, FSNServer)

			File.SetAttributes(Server.MapPath(".") & "\PLANTILLA_DESGLOSE_MATERIAL.xls", attributes)
		Catch ex As Exception
			Throw ex
		Finally
			excelConnectionPlantilla.Close()
			objCommandPlantilla = Nothing
			excelConnectionPlantilla = Nothing
		End Try
	End Sub
	Private Sub Exportar_Provincias_Por_Pais(ByVal objCmd As OleDb.OleDbCommand, ByVal oPaises As FSNServer.Paises, ByVal Idioma As String, ByVal FSNServer As FSNServer.Root)
		oPaises = FSNServer.Get_Object(GetType(FSNServer.Paises))
		oPaises.LoadData(FSNUser.Idioma)

		Dim sPaises As String
		sPaises = String.Join(",", oPaises.Data.Tables(0).Rows.OfType(Of DataRow).Select(Function(x) "[" & x("COD") & "] STRING").ToList)
		objCmd.CommandText = "CREATE TABLE [DATOSPROVINCIAS] (" & sPaises & ")"
		objCmd.ExecuteNonQuery()
		objCmd.CommandText = "CREATE TABLE [DATOSPROVINCIASVALUES] (" & sPaises & ")"
		objCmd.ExecuteNonQuery()
		Dim oProvincias As FSNServer.Provincias
		oProvincias = FSNServer.Get_Object(GetType(FSNServer.Provincias))
		Dim dProvincias As New Dictionary(Of String, List(Of KeyValuePair(Of String, String)))
		Dim numMaximoProvincias As Integer = 0
		Dim insertProvincias As String = String.Empty, insertProvinciasValues As String = String.Empty
		For Each pais As DataRow In oPaises.Data.Tables(0).Rows
			oProvincias.Pais = pais("COD")
			oProvincias.LoadData(Idioma)
			dProvincias(pais("COD")) = oProvincias.Data.Tables(0).Rows.OfType(Of DataRow).Select(Function(x) New With {Key .cod = x("COD"), .den = x("COD") & " - " & x("DEN")}).ToList.ConvertAll(Function(y) New KeyValuePair(Of String, String)(y.cod, y.den))
			If oProvincias.Data.Tables(0).Rows.Count > numMaximoProvincias Then numMaximoProvincias = oProvincias.Data.Tables(0).Rows.Count
			insertProvincias &= If(String.IsNullOrEmpty(insertProvincias), "", ",") & "@DENPROVINCIA_" & pais("COD")
			insertProvinciasValues &= If(String.IsNullOrEmpty(insertProvinciasValues), "", ",") & "@VALPROVINCIA_" & pais("COD")
		Next

		For i As Integer = 0 To numMaximoProvincias - 1
			objCmd.Parameters.Clear()
			For Each pais As KeyValuePair(Of String, List(Of KeyValuePair(Of String, String))) In dProvincias
				objCmd.Parameters.AddWithValue("@DENPROVINCIA_" & pais.Key, If(pais.Value.Count - 1 < i, DBNull.Value, pais.Value(i).Value))
			Next
			objCmd.CommandText = "INSERT INTO [DATOSPROVINCIAS$] VALUES(" & insertProvincias & ")"
			objCmd.ExecuteNonQuery()

			objCmd.Parameters.Clear()
			For Each pais As KeyValuePair(Of String, List(Of KeyValuePair(Of String, String))) In dProvincias
				objCmd.Parameters.AddWithValue("@VALPROVINCIA_" & pais.Key, If(pais.Value.Count - 1 < i, DBNull.Value, pais.Value(i).Key))
			Next
			objCmd.CommandText = "INSERT INTO [DATOSPROVINCIASVALUES$] VALUES(" & insertProvinciasValues & ")"
			objCmd.ExecuteNonQuery()
		Next
	End Sub

    Private Function GenerarValoresDesglose_Validaciones_Partida(ByVal partidaNiv1 As String, ByVal partidaNiv2 As String, ByVal partidaNiv3 As String, ByVal partidaNiv4 As String,
                                                                     ByVal centroCosteUO1 As String, ByVal centroCosteUO2 As String, ByVal centroCosteUO3 As String, ByVal centroCosteUO4 As String,
                                                                     ByVal Pres5Niv0 As String, ByRef DenPartida As String, ByVal Plurianual As Boolean,
                                                                     ByVal TieneCentroCosteDesglose As Boolean, ByRef CodigoCompletadoCentro As String, ByRef DescripCodigoCompletadoCentro As String) As Boolean
        Dim drPartidaSM() As DataRow
        Dim bNoIndicado As Boolean = False

        If Not String.IsNullOrEmpty(partidaNiv1 & partidaNiv2 & partidaNiv3 & partidaNiv4) Then
            If (Not String.IsNullOrEmpty(centroCosteUO4) AndAlso (String.IsNullOrEmpty(centroCosteUO3) OrElse String.IsNullOrEmpty(centroCosteUO2) OrElse String.IsNullOrEmpty(centroCosteUO1))) _
                OrElse (Not String.IsNullOrEmpty(centroCosteUO3) AndAlso (String.IsNullOrEmpty(centroCosteUO2) OrElse String.IsNullOrEmpty(centroCosteUO1))) _
                OrElse (Not String.IsNullOrEmpty(centroCosteUO2) AndAlso String.IsNullOrEmpty(centroCosteUO1)) Then 'El centro de coste esta bien, sino error
                GenerarValoresDesglose_Validaciones_Partida = False
            ElseIf (Not TieneCentroCosteDesglose) AndAlso String.IsNullOrEmpty(centroCosteUO4) AndAlso String.IsNullOrEmpty(centroCosteUO3) AndAlso String.IsNullOrEmpty(centroCosteUO2) AndAlso String.IsNullOrEmpty(centroCosteUO1) Then
                'El centro de coste a nivel CAMPO NO esta indicado. Luego error.
                GenerarValoresDesglose_Validaciones_Partida = False
            Else
                If (Not String.IsNullOrEmpty(partidaNiv4) AndAlso (String.IsNullOrEmpty(partidaNiv3) OrElse String.IsNullOrEmpty(partidaNiv2) OrElse String.IsNullOrEmpty(partidaNiv1))) _
                    OrElse (Not String.IsNullOrEmpty(partidaNiv3) AndAlso (String.IsNullOrEmpty(partidaNiv2) OrElse String.IsNullOrEmpty(partidaNiv1))) _
                    OrElse (Not String.IsNullOrEmpty(partidaNiv2) AndAlso String.IsNullOrEmpty(partidaNiv1)) Then
                    GenerarValoresDesglose_Validaciones_Partida = False
                Else
                    oPRES5.Partidas_LoadData(Pres5Niv0, FSNUser.Cod, FSNUser.Idioma.ToString, Plurianual, centroCosteUO1 & If(String.IsNullOrEmpty(centroCosteUO2), "", "#" & centroCosteUO2 &
                                            If(String.IsNullOrEmpty(centroCosteUO3), "", "#" & centroCosteUO3 &
                                            If(String.IsNullOrEmpty(centroCosteUO4), "", "#" & centroCosteUO4))))
                    If String.IsNullOrEmpty(centroCosteUO1 & If(String.IsNullOrEmpty(centroCosteUO2), "", "#" & centroCosteUO2 &
                                            If(String.IsNullOrEmpty(centroCosteUO3), "", "#" & centroCosteUO3 &
                                            If(String.IsNullOrEmpty(centroCosteUO4), "", "#" & centroCosteUO4)))) Then 'Si no se ha indicado centro de coste buscaremos directamente la partida
                        bNoIndicado = True

                        Select Case Convert.ToInt32(Not String.IsNullOrEmpty(partidaNiv1)) + Convert.ToInt32(Not String.IsNullOrEmpty(partidaNiv2)) _
                                    + Convert.ToInt32(Not String.IsNullOrEmpty(partidaNiv3)) + Convert.ToInt32(Not String.IsNullOrEmpty(partidaNiv4))
                            Case 1
                                drPartidaSM = oPRES5.Data.Tables(5).Rows.OfType(Of DataRow).Where(Function(x) x("COD") = partidaNiv1).ToArray
                            Case 2
                                drPartidaSM = oPRES5.Data.Tables(6).Rows.OfType(Of DataRow).Where(Function(x) x("COD") = partidaNiv1 & "|" & partidaNiv2).ToArray
                            Case 3
                                drPartidaSM = oPRES5.Data.Tables(7).Rows.OfType(Of DataRow).Where(Function(x) x("COD") = partidaNiv1 & "|" & partidaNiv2 & "|" & partidaNiv3).ToArray
                            Case Else
                                drPartidaSM = oPRES5.Data.Tables(8).Rows.OfType(Of DataRow).Where(Function(x) x("COD") = partidaNiv1 & "|" & partidaNiv2 & "|" & partidaNiv3 & "|" & partidaNiv4).ToArray
                        End Select
                    Else
                        Select Case Convert.ToInt32(Not String.IsNullOrEmpty(partidaNiv1)) + Convert.ToInt32(Not String.IsNullOrEmpty(partidaNiv2)) _
                                    + Convert.ToInt32(Not String.IsNullOrEmpty(partidaNiv3)) + Convert.ToInt32(Not String.IsNullOrEmpty(partidaNiv4))
                            Case 1
                                drPartidaSM = oPRES5.Data.Tables(5).Rows.OfType(Of DataRow).Where(Function(x) x("UON1").ToString = centroCosteUO1 AndAlso x("UON2").ToString = centroCosteUO2 _
                                                                                                          AndAlso x("UON3").ToString = centroCosteUO3 AndAlso x("UON4").ToString = centroCosteUO4 _
                                                                                                          AndAlso x("COD") = partidaNiv1).ToArray
                            Case 2
                                drPartidaSM = oPRES5.Data.Tables(6).Rows.OfType(Of DataRow).Where(Function(x) x("UON1").ToString = centroCosteUO1 AndAlso x("UON2").ToString = centroCosteUO2 _
                                                                                                          AndAlso x("UON3").ToString = centroCosteUO3 AndAlso x("UON4").ToString = centroCosteUO4 _
                                                                                                          AndAlso x("COD") = partidaNiv1 & "|" & partidaNiv2).ToArray
                            Case 3
                                drPartidaSM = oPRES5.Data.Tables(7).Rows.OfType(Of DataRow).Where(Function(x) x("UON1").ToString = centroCosteUO1 AndAlso x("UON2").ToString = centroCosteUO2 _
                                                                                                            AndAlso x("UON3").ToString = centroCosteUO3 AndAlso x("UON4").ToString = centroCosteUO4 _
                                                                                                            AndAlso x("COD") = partidaNiv1 & "|" & partidaNiv2 & "|" & partidaNiv3).ToArray
                            Case Else
                                drPartidaSM = oPRES5.Data.Tables(8).Rows.OfType(Of DataRow).Where(Function(x) x("UON1").ToString = centroCosteUO1 AndAlso x("UON2").ToString = centroCosteUO2 _
                                                                                                          AndAlso x("UON3").ToString = centroCosteUO3 AndAlso x("UON4").ToString = centroCosteUO4 _
                                                                                                          AndAlso x("COD") = partidaNiv1 & "|" & partidaNiv2 & "|" & partidaNiv3 & "|" & partidaNiv4).ToArray
                        End Select
                    End If
                    If drPartidaSM.Any AndAlso drPartidaSM.Count = 1 Then
                        If bNoIndicado = True Then
                            CodigoCompletadoCentro = drPartidaSM(0)("UON1").ToString &
                                                    If(String.IsNullOrEmpty(drPartidaSM(0)("UON2").ToString), "", "#" & drPartidaSM(0)("UON2").ToString &
                                                    If(String.IsNullOrEmpty(drPartidaSM(0)("UON3").ToString), "", "#" & drPartidaSM(0)("UON3").ToString &
                                                    If(String.IsNullOrEmpty(drPartidaSM(0)("UON4").ToString), "", "#" & drPartidaSM(0)("UON4").ToString)))

                            'Para completar en la línea el centro coste q he determinado
                            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
                            oCentroCoste = FSNServer.Get_Object(GetType(FSNServer.CentrosCoste))
                            oCentroCoste.LoadData(FSNUser.Cod, FSNUser.Idioma.ToString, True)

                            Dim drCentroCoste() As DataRow
                            If Not String.IsNullOrEmpty(drPartidaSM(0)("UON4").ToString) Then
                                drCentroCoste = oCentroCoste.Data.Tables(4).Rows.OfType(Of DataRow).Where(Function(x) x("UON1") = drPartidaSM(0)("UON1").ToString _
                                                                                                                                  AndAlso x("UON2") = drPartidaSM(0)("UON2").ToString _
                                                                                                                                  AndAlso x("UON3") = drPartidaSM(0)("UON3").ToString _
                                                                                                                                  AndAlso x("COD") = drPartidaSM(0)("UON4").ToString).ToArray
                            ElseIf Not String.IsNullOrEmpty(drPartidaSM(0)("UON3").ToString) Then
                                drCentroCoste = oCentroCoste.Data.Tables(3).Rows.OfType(Of DataRow).Where(Function(x) x("UON1") = drPartidaSM(0)("UON1").ToString _
                                                                                                                                  AndAlso x("UON2") = drPartidaSM(0)("UON2").ToString _
                                                                                                                                  AndAlso x("COD") = drPartidaSM(0)("UON3").ToString).ToArray
                            ElseIf Not String.IsNullOrEmpty(drPartidaSM(0)("UON2").ToString) Then
                                drCentroCoste = oCentroCoste.Data.Tables(2).Rows.OfType(Of DataRow).Where(Function(x) x("UON1") = drPartidaSM(0)("UON1").ToString _
                                                                                                                                  AndAlso x("COD") = drPartidaSM(0)("UON2").ToString).ToArray
                            Else
                                drCentroCoste = oCentroCoste.Data.Tables(2).Rows.OfType(Of DataRow).Where(Function(x) x("COD") = drPartidaSM(0)("UON1").ToString).ToArray
                            End If

                            If drCentroCoste.Any Then DescripCodigoCompletadoCentro = drCentroCoste(0)("DEN")

                        End If

                        DenPartida = drPartidaSM(0)("DEN")
                        GenerarValoresDesglose_Validaciones_Partida = True
                    Else
                        GenerarValoresDesglose_Validaciones_Partida = False
                    End If
                End If
            End If
        Else
            GenerarValoresDesglose_Validaciones_Partida = False
        End If
    End Function
#End Region
End Class