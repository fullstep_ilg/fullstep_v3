
    Public Class modificarDenMaterial
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblDen As System.Web.UI.WebControls.Label
        Protected WithEvents tblDenominacion As System.Web.UI.HtmlControls.HtmlTable
        Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
        Protected WithEvents cmdAceptar As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents cmdCancelar As System.Web.UI.HtmlControls.HtmlInputButton

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

    Private sMensaje As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oCelda As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oFila As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oText As System.Web.UI.WebControls.TextBox
        Dim oLabel As System.Web.UI.WebControls.Label
        Dim oIdiomas As FSNServer.Idiomas
        Dim oIdioma As FSNServer.Idioma
        Dim oMaterialQA As FSNServer.MaterialQA

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ModificarDenMatQA

        Me.lblTitulo.Text = Textos(0)
        Me.cmdAceptar.Value = Textos(1)
        Me.cmdCancelar.Value = Textos(2)
        sMensaje = Textos(5)

        'Carga los idiomas:
        oIdiomas = FSNServer.Get_Object(GetType(FSNServer.Idiomas))
        oIdiomas.Load()

        'Carga las denominaciones actuales del material de QA:
        oMaterialQA = FSNServer.Get_Object(GetType(FSNServer.MaterialQA))
        oMaterialQA.ID = Request("Material")
        oMaterialQA.Load()

        'Carga 1� el idioma del usuario:
        oIdioma = oIdiomas.Idiomas.Item(Idioma)
        oFila = New System.Web.UI.HtmlControls.HtmlTableRow
        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
        oLabel = New System.Web.UI.WebControls.Label
        oLabel.Text = oIdioma.Den & " :"
        oLabel.CssClass = "parrafo"
        oCelda.Controls.Add(oLabel)
        oFila.Cells.Add(oCelda)

        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
        oText = New System.Web.UI.WebControls.TextBox
        oText.ID = "txtDen_" & oIdioma.Cod
        oText.Text = DBNullToSomething(oMaterialQA.Den(oIdioma.Cod))
        oText.Width = Unit.Pixel(400)
        oText.MaxLength = 200
        oCelda.Controls.Add(oText)
        oFila.Cells.Add(oCelda)
        Me.tblDenominacion.Rows.Add(oFila)

        'ahora carga el resto de idiomas
        For Each oIdioma In oIdiomas.Idiomas
            If oIdioma.Cod <> Idioma Then
                oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oLabel = New System.Web.UI.WebControls.Label
                oLabel.Text = oIdioma.Den & " :"
                oLabel.CssClass = "parrafo"
                oCelda.Controls.Add(oLabel)
                oFila.Cells.Add(oCelda)

                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oText = New System.Web.UI.WebControls.TextBox
                oText.ID = "txtDen_" & oIdioma.Cod
                oText.Text = DBNullToSomething(oMaterialQA.Den(oIdioma.Cod))
                oText.Width = Unit.Pixel(400)
                oText.MaxLength = 200
                oCelda.Controls.Add(oText)
                oFila.Cells.Add(oCelda)

                Me.tblDenominacion.Rows.Add(oFila)
            End If
        Next

        oIdiomas = Nothing
        oMaterialQA = Nothing
    End Sub

    Private Sub cmdAceptar_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAceptar.ServerClick
        'Guarda las denominaciones en todos los idiomas
        Dim sNewDen As String
        Dim oMaterialQA As FSNServer.MaterialQA
        Dim oText As System.Web.UI.WebControls.TextBox
        Dim oIdiomas As FSNServer.Idiomas
        Dim oIdioma As FSNServer.Idioma

        'Comprueba que el idioma del usuario por lo menos est� relleno:
        oText = Me.FindControl("txtDen_" & Idioma)
        If oText.Text = "" Then
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>alert('" & sMensaje & "')</script>")
            Exit Sub
        Else
            sNewDen = JSText(oText.Text)
        End If

        oMaterialQA = FSNServer.Get_Object(GetType(FSNServer.MaterialQA))
        oMaterialQA.ID = Request("Material")

        oIdiomas = FSNServer.Get_Object(GetType(FSNServer.Idiomas))
        oIdiomas.Load()
        For Each oIdioma In oIdiomas.Idiomas
            oText = Me.FindControl("txtDen_" & oIdioma.Cod)
            oMaterialQA.Den(oIdioma.Cod) = oText.Text
        Next

        oMaterialQA.ModificarDenominaciones()
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>actualizarArbol('" & sNewDen & "')</script>")

        oMaterialQA = Nothing
        oIdiomas = Nothing
    End Sub
    End Class

