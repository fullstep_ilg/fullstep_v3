
    Public Class eliminarMaterial
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblMaterial As System.Web.UI.WebControls.Label
        Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
        Protected WithEvents cmdAceptar As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents cmdCancelar As System.Web.UI.HtmlControls.HtmlInputButton
        
        Protected WithEvents lblProveedores As System.Web.UI.WebControls.Label
        Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private oMaterialQA As FSNServer.MaterialQA

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim sIdi As String = Session("FSN_User").Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.EliminarMaterialesQA, sIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)
        Dim sCont As String
        Dim sMensaje As String
        'Comprueba si existen variables puntuadas para alg�n proveedor de los tipos de certificados del material 
        oMaterialQA = FSWSServer.Get_Object(GetType(FSNServer.MaterialQA))
        oMaterialQA.ID = Request("Material")
        oMaterialQA.ProveedoresVarCalPuntuadas()

        If oMaterialQA.Data.Tables.Count > 0 Then
            If oMaterialQA.Data.Tables(0).Rows.Count > 0 Then
                Me.lblMensaje.Text = oTextos.Rows(5).Item(1)
                sCont = oMaterialQA.Data.Tables(0).Rows(0).Item(0)
                sMensaje = oTextos.Rows(7).Item(1)
                sMensaje = Replace(sMensaje, "@CONTPROVE", sCont)
            Else
                Me.lblProveedores.Visible = False
                Me.lblMensaje.Text = oTextos.Rows(1).Item(1)
            End If
        Else
            Me.lblProveedores.Visible = False
            Me.lblMensaje.Text = oTextos.Rows(1).Item(1)
        End If

        Me.lblTitulo.Text = oTextos.Rows(0).Item(1)

        Me.cmdAceptar.Value = oTextos.Rows(2).Item(1)
        Me.cmdCancelar.Value = oTextos.Rows(3).Item(1)
        Me.lblMaterial.Text = Request("DenMaterial")
        Me.lblProveedores.Text = sMensaje

        oTextos = Nothing

    End Sub


    Private Sub cmdAceptar_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAceptar.ServerClick
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oUser As FSNServer.User
        oUser = Session("FSN_User")
        oMaterialQA.EliminarMaterialQA()
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(),"init", "<script>actualizarArbol()</script>")
    End Sub

        Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Unload
            oMaterialQA = Nothing
        End Sub
    End Class

