Imports Infragistics.WebUI.UltraWebGrid

Public Class UnidadesNegocio
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents cmdAnyaNeg As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdModifNeg As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdEliminarNeg As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents uwtUnidades As Infragistics.WebUI.UltraWebNavigator.UltraWebTree
    Protected WithEvents uwgProveedores As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
    Protected WithEvents lblProveedores As System.Web.UI.WebControls.Label
    Protected WithEvents txtCodigo As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents txtDenominacion As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents txtCIF As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents ImageButton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents hypBuscar As System.Web.UI.WebControls.LinkButton
    Protected WithEvents nodoAnterior As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents chkProveedorBaja As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkUnidadesBaja As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cmdBajaLogica As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents chkUnidadesBajaValor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tblProveedores As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents ListBox1 As System.Web.UI.WebControls.ListBox
    Protected WithEvents proveedoresCount As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents div_proveedores As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents cmdCargar As System.Web.UI.WebControls.Button
    Protected WithEvents UltraWebGridExcelExporter1 As Infragistics.WebUI.UltraWebGrid.ExcelExport.UltraWebGridExcelExporter
    Protected WithEvents slcTipoProveedor As System.Web.UI.HtmlControls.HtmlSelect
    Protected WithEvents lMaxID As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents idNodoGrupo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cmdCancelar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents datosModificados As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents numProvesPag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents buscar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cadenaUnidadesNegocio As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cadenaUNQA As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cmdAceptar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cadenaUNQAAnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents grupo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Sort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblCodigo As System.Web.UI.WebControls.Label
    Protected WithEvents lblDenominacion As System.Web.UI.WebControls.Label
    Protected WithEvents lblCIF As System.Web.UI.WebControls.Label
    Protected WithEvents linkPDF As System.Web.UI.WebControls.LinkButton
    Protected WithEvents linkHTML As System.Web.UI.WebControls.LinkButton
    Protected WithEvents linkExcel As System.Web.UI.WebControls.LinkButton
    Protected WithEvents tblImprimir As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents uwgProveedoresAux As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
    Protected WithEvents cmdImprimir As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents textoImprimir As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cadenaPermisosUNQA As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cont As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cmdUONs As System.Web.UI.HtmlControls.HtmlInputButton
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private oUnidadesNeg As FSNServer.UnidadesNeg
    Private sTipoProveedores(4) As String
    Private idPyme As Long
    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
"<script language=""{0}"">{1}</script>"
    Private labels() As String = {"", "Primero", "Anterior", "Siguiente", "Ultimo"}
    Private m_bCambioPagina As Boolean
    Private m_lContFila As Long
    Private m_idiomaTablaProveedores(6) As String
    Private oStringWriter As New System.IO.StringWriter
    Private m_sSi As String
    Private m_sNo As String
    Private pds As New PagedDataSource


    ''' <summary>
    ''' Carga la pagina
    ''' </summary>
    ''' <param name="sender">propios del evento</param>
    ''' <param name="e">propios del evento</param>    
    ''' <remarks>Llamada desde=propia pagina  ; Tiempo m�ximo=0seg.</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.Expires = -1
        Dim sIdi As String = FSNUser.Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If
        Dim i As Integer
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.UnidadesNegocio

        idPyme = FSNUser.Pyme

        lblTitulo.Text = Textos(0) 'Unidades de negocio
        cmdAnyaNeg.Value = Textos(1)
        cmdModifNeg.Value = Textos(2) 'Modificar unidad de negocio
        cmdEliminarNeg.Value = Textos(3)
        cmdBajaLogica.Value = Textos(5) 'Dar de baja unidad de negocio
        cmdUONs.Value = Textos(36)

        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrUnidadesNegocio = {};var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(Textos(4)) + " ';"  'Seleccione una unidad
        sClientTexts += "arrTextosML[1] = '" + JSText(Textos(5)) + " ';"  'Dar de baja unidad de negocio
        sClientTexts += "arrTextosML[2] = '" + JSText(Textos(6)) + " ';"  'Deshacer baja unidad de negocio
        sClientTexts += "arrTextosML[3] = '" + JSText(Textos(21)) + " ';"  'Para deshacer la baja l�gica de la unidad de negocio seleccionada, primero debe deshacer la baja l�gica de la unidad de negocio padre
        sClientTexts += "arrTextosML[4] = '" + JSText(Textos(22)) + " ';" 'Si da de baja l�gica la unidad de negocio seleccionada, tambi�n se dar�n de baja l�gica las unidades de negocio que dependen de ella. �Desea continuar con la baja l�gica?
        sClientTexts += "arrTextosML[5] = '" + JSText(Textos(23)) + " ';" 'Para eliminar esta unidad de negocio primero debe de eliminar las unidades hijas.
        sClientTexts += "arrTextosML[6] = '" + JSText(Textos(33)) + " ';" 'No tiene permiso para asignar proveedores para la Unidad de Negocio:
        sClientTexts += "arrTextosML[7] = '" + JSText(Textos(34)) + " ';" 'La unidad de negocio a asignar se encuentra ya asignada al proveedor
        sClientTexts += "arrTextosML[8] = '" + JSText(Textos(35)) + " ';" 'Tiene descendientes asignados
        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        Page.ClientScript.RegisterStartupScript(Me.GetType(),"TextosMICliente", sClientTexts)

        If Not Page.IsPostBack Then
            CargarArbolUnidades(sIdi, Me.chkUnidadesBaja.Checked, idPyme)
        End If

        sTipoProveedores(1) = Textos(7) '"Mostrar todos los proveedores"
        sTipoProveedores(2) = Textos(8) '"Mostrar s�lo los proveedores de panel de calidad"
        sTipoProveedores(3) = Textos(9) '"Mostrar s�lo proveedores potenciales"
        labels(1) = Textos(10) 'Primero
        labels(2) = Textos(11) 'Anterior
        labels(3) = Textos(12) 'Siguiente
        labels(4) = Textos(13) 'Ultimo
        Me.chkUnidadesBaja.Text = Textos(14) 'Incluir unidades dadas de baja

        lblCodigo.Text = Textos(15) '
        lblDenominacion.Text = Textos(16) '
        lblCIF.Text = Textos(17) & ":" '
        Me.chkProveedorBaja.Text = Textos(18) 'Incluir proveedores dados de baja
        Me.hypBuscar.Text = Textos(19) '
        Me.lblProveedores.Text = Textos(20)

        Me.cmdAceptar.Value = Textos(28) 'Aceptar
        Me.cmdCancelar.Value = Textos(29) 'Cancelar
        textoImprimir.Value = Textos(30) 'Imprimir/Exportar

        m_idiomaTablaProveedores(0) = Textos(24) 'Den.proveedor
        m_idiomaTablaProveedores(1) = Textos(25) 'proveedor
        m_idiomaTablaProveedores(2) = Textos(17) 'CIF
        m_idiomaTablaProveedores(3) = Textos(0) 'Unidades de negocio
        m_idiomaTablaProveedores(4) = Textos(26) 'Potencial
        m_idiomaTablaProveedores(5) = Textos(27) 'Baja

        m_sSi = Textos(31)
        m_sNo = Textos(32)

        CargarComboTipoProveedores()

        cmdAnyaNeg.Visible = FSNUser.QAMantenimientoUNQA
        cmdModifNeg.Visible = FSNUser.QAMantenimientoUNQA
        cmdEliminarNeg.Visible = FSNUser.QAMantenimientoUNQA
        cmdBajaLogica.Visible = FSNUser.QAMantenimientoUNQA

        If Not FSNUser.QAMantenimientoUNQA Then
            uwtUnidades.Width = 650
            chkUnidadesBaja.Style.Item("Left") = 670
        End If

        Dim oIdiomas As FSNServer.Idiomas
        'Carga los idiomas:
        oIdiomas = FSNServer.Get_Object(GetType(FSNServer.Idiomas))
        oIdiomas.Load()

        cont.Value = oIdiomas.Idiomas.Count

    End Sub

    Private Sub CargarArbolUnidades(ByVal sIdi As String, ByVal bBaja As Boolean, ByVal lIDPyme As Long)
        '************************************************************************
        '*** Descripci�n:   Procedure que carga el arbol con las Unidades de negocio
        '*** Par�metros de entrada:     sIdi--> c�digo de idioma con el que se cargar�n las denominaciones de los nodos
        '                               bBaja-->(true/false) Si se muestra o no los nodos que estan de bajas
        '                               lIdPyme--> Id de la pyme del usuario, si estuviera trabajando en modo pyme, 0 si no.
        '*** Llamada desde: propia pagina  						                              
        '*** Tiempo m�ximo: 0,43seg  						                                       
        '************************************************************
        'Dim inicio As Double, final As Double, tiempoTranscurrido As Double
        'inicio = Timer
        Dim oUnidad As DataRow
        Dim oUnidadNegHijas As DataRow
        Dim oUnidadNegNietas As DataRow

        Dim node As Infragistics.WebUI.UltraWebNavigator.Node
        Dim oUnidadNeg As DataRow


        Dim oUnidadesNegHijas As FSNServer.UnidadesNeg
        Dim oUnidadesNegNietas As FSNServer.UnidadesNeg

        Me.uwtUnidades.ClearAll()

        Dim sUser As String = FSNUser.Cod

        oUnidadesNeg = FSNServer.Get_Object(GetType(FSNServer.UnidadesNeg))
        oUnidadesNeg.LoadData(sIdi:=sIdi, Usuario:=FSNUser.Cod, bBaja:=bBaja, lIDPyme:=lIDPyme) 'Cargar las Unidades de Negocio de Primer nivel
        If oUnidadesNeg.Data.Tables.Count > 1 Then
            grupo.Value = oUnidadesNeg.Data.Tables(1).Rows(0).Item("COD") & "-" & oUnidadesNeg.Data.Tables(1).Rows(0).Item("DEN")
        End If

        Dim newNode As Infragistics.WebUI.UltraWebNavigator.Node
        Dim newNodeHijo As Infragistics.WebUI.UltraWebNavigator.Node
        Dim newNodeNieto As Infragistics.WebUI.UltraWebNavigator.Node

        For Each oUnidadNeg In oUnidadesNeg.Data.Tables(0).Rows
            'Ir a�adiendo las unidades de negocio a los nodos para a�adir al arbol
            newNode = New Infragistics.WebUI.UltraWebNavigator.Node
            newNode.Text = DBNullToStr(oUnidadNeg.Item("COD")) & "-" & DBNullToStr(oUnidadNeg.Item("DEN"))

            If (Not FSNUser.QAMantenimientoUNQA) Then
                If oUnidadNeg.Item("Permiso") Then
                    cadenaPermisosUNQA.Value = IIf(cadenaPermisosUNQA Is Nothing, "", cadenaPermisosUNQA.Value) & oUnidadNeg.Item("ID") & "#"
                Else
                    newNode.Enabled = False
                End If
            Else
                cadenaPermisosUNQA.Value = cadenaPermisosUNQA.Value & oUnidadNeg.Item("ID") & "#"
            End If

            newNode.DataKey = oUnidadNeg.Item("ID")

            If oUnidadNeg.Item("BAJALOG") = 1 Then
                newNode.ImageUrl = "images/xroja.gif"
                newNode.Tag = "BAJA"
            Else
                newNode.Tag = "NOBAJA"
            End If
            oUnidadesNegHijas = FSNServer.Get_Object(GetType(FSNServer.UnidadesNeg))
            oUnidadesNegHijas.LoadData(sIdi:=sIdi, Usuario:=FSNUser.Cod, lIDPadre:=oUnidadNeg.Item("ID"), bBaja:=bBaja, lIDPyme:=lIDPyme) 'Cargar las Unidades de Negocio de Segundo nivel

            For Each oUnidadNegHijas In oUnidadesNegHijas.Data.Tables(0).Rows
                'Carga las Unidades de Negocio QA
                newNodeHijo = New Infragistics.WebUI.UltraWebNavigator.Node
                newNodeHijo.Text = DBNullToStr(oUnidadNegHijas.Item("COD")) & "-" & DBNullToStr(oUnidadNegHijas.Item("DEN"))

                If (Not FSNUser.QAMantenimientoUNQA) Then
                    If oUnidadNegHijas.Item("Permiso") Then
                        cadenaPermisosUNQA.Value = IIf(cadenaPermisosUNQA Is Nothing, "", cadenaPermisosUNQA.Value) & oUnidadNegHijas.Item("ID") & "#"
                    Else
                        newNodeHijo.Enabled = False
                    End If
                Else
                    cadenaPermisosUNQA.Value = cadenaPermisosUNQA.Value & oUnidadNegHijas.Item("ID") & "#"
                End If

                newNodeHijo.DataKey = oUnidadNegHijas.Item("ID")

                If oUnidadNegHijas.Item("BAJALOG") = 1 Then
                    newNodeHijo.ImageUrl = "images/xroja.gif"
                    newNodeHijo.Tag = "BAJA"
                Else
                    newNodeHijo.Tag = "NOBAJA"
                End If

                oUnidadesNegNietas = FSNServer.Get_Object(GetType(FSNServer.UnidadesNeg))
                oUnidadesNegNietas.LoadData(sIdi:=sIdi, Usuario:=FSNUser.Cod, lIDPadre:=oUnidadNegHijas.Item("ID"), lIDAbuelo:=oUnidadNeg.Item("ID"), bBaja:=bBaja, lIDPyme:=lIDPyme)
                For Each oUnidadNegNietas In oUnidadesNegNietas.Data.Tables(0).Rows
                    'Carga las Unidades de Negocio QA
                    newNodeNieto = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNodeNieto.Text = DBNullToStr(oUnidadNegNietas.Item("COD")) & "-" & DBNullToStr(oUnidadNegNietas.Item("DEN"))

                    newNodeNieto.DataKey = oUnidadNegNietas.Item("ID")

                    If oUnidadNegNietas.Item("BAJALOG") = 1 Then
                        newNodeNieto.ImageUrl = "images/xroja.gif"
                        newNodeNieto.Tag = "BAJA"
                    Else
                        newNodeNieto.Tag = "NOBAJA"
                    End If

                    If (Not FSNUser.QAMantenimientoUNQA) Then
                        If oUnidadNegNietas.Item("Permiso") Then
                            cadenaPermisosUNQA.Value = IIf(cadenaPermisosUNQA Is Nothing, "", cadenaPermisosUNQA.Value) & oUnidadNegNietas.Item("ID") & "#"
                        Else
                            newNodeNieto.Enabled = False
                        End If
                    Else
                        cadenaPermisosUNQA.Value = IIf(cadenaPermisosUNQA Is Nothing, "", cadenaPermisosUNQA.Value) & oUnidadNegNietas.Item("ID") & "#"
                    End If

                    If FSNUser.QAMantenimientoUNQA Then
                        newNodeHijo.Nodes.Add(newNodeNieto)
                    ElseIf oUnidadNegNietas.Item("Permiso") Then
                        newNodeHijo.Nodes.Add(newNodeNieto)
                    End If

                Next
                oUnidadesNegNietas = Nothing

                If FSNUser.QAMantenimientoUNQA Then
                    newNode.Nodes.Add(newNodeHijo)
                ElseIf oUnidadNegHijas.Item("Permiso") Then
                    newNode.Nodes.Add(newNodeHijo)
                ElseIf newNodeHijo.Nodes.Count > 0 Then
                    newNode.Nodes.Add(newNodeHijo)
                End If

            Next
            oUnidadesNegHijas = Nothing

            If FSNUser.QAMantenimientoUNQA Then
                uwtUnidades.Nodes.Add(newNode)
            ElseIf oUnidadNeg.Item("Permiso") Then
                uwtUnidades.Nodes.Add(newNode)
            ElseIf newNode.Nodes.Count > 0 Then
                uwtUnidades.Nodes.Add(newNode)
            End If

        Next

        Me.uwtUnidades.DataKeyOnClient = True
        Me.uwtUnidades.ExpandAll()

        'final = Timer
        'tiempoTranscurrido = (final - inicio)
    End Sub

    Private Sub hypBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hypBuscar.Click
        CargarDatosProveedoresRelacionados()
    End Sub

    Private Sub CargarProveedoresConUnidades()

        '************************************************************************
        '*** Descripci�n:   Procedure que carga los datos de los proveedores relacionados con las unidades de negocio de la grid proveedores
        '*** Par�metros de entrada: ninguno
        '*** Llamada desde: propia pagina  						                              
        '*** Tiempo m�ximo: 0,28seg.  						                                       
        '************************************************************
        Dim i As Long
        Dim sIdi As String = FSNUser.Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        Dim sUNQAs As String = Request("cadenaUNQACheckeados")

        If buscar.Value = "1" And Me.m_bCambioPagina = True Then
            sUNQAs = Request("cadenaUNQAAnt")
        Else
            cadenaUNQAAnt.Value = Request("cadenaUNQACheckeados")
        End If


        Dim sAux(), arrDatos() As String
        Dim sCadenaIDsUNQA As String
        Dim sCadenaAux As String
        If sUNQAs <> "" Then
            sAux = sUNQAs.Split(";")
            For i = 0 To UBound(sAux)
                If sAux(i) <> "" Then
                    arrDatos = sAux(i).Split("#")
                    sCadenaAux = arrDatos(0).Replace("/", "")
                    If sCadenaAux <> "" Then
                        If sCadenaIDsUNQA <> "" Then sCadenaIDsUNQA = sCadenaIDsUNQA + ","
                        sCadenaIDsUNQA = sCadenaIDsUNQA + sCadenaAux
                    End If
                End If
            Next
        End If


        Dim oProves As FSNServer.Proveedores
        oProves = FSNServer.Get_Object(GetType(FSNServer.Proveedores))
        oProves.ProveedoresConUnidadesNegocio(FSNUser.Cod, sIdi, _
            FSNUser.QARestProvContacto, FSNUser.QARestProvMaterial, FSNUser.QARestProvEquipo, _
            txtCodigo.Value, txtDenominacion.Value, txtCIF.Value, chkProveedorBaja.Checked, slcTipoProveedor.Value, _
            sCadenaIDsUNQA, True, FSNUser.Pyme, Sort.Value)
        proveedoresCount.Value = oProves.Data.Tables(0).Rows.Count

        If proveedoresCount.Value > 0 Then
            uwgProveedores.Visible = True
            Page.ClientScript.RegisterStartupScript(Me.GetType(),"VisualizarGrid", "<script>VisualizarGrid();</script>")

            pds.DataSource = oProves.Data.Tables(0).DefaultView
            InitPagedDataSource()


            For i = 0 To uwgProveedores.Rows.Count - 1
                ''Antiguo  uwgProveedores_InitializeRow
                uwgProveedores_InitializeRow_Manual(i, uwgProveedores.Rows(i))
            Next

            uwgProveedores.Height = Unit.Pixel((uwgProveedores.Rows.Count * 75) + 40)
        Else
            uwgProveedores.Visible = False
        End If

        oProves = Nothing
    End Sub


    Private Sub CargarComboTipoProveedores()
        '************************************************************************
        '*** Descripci�n:   Procedure carga la combo de filtro de Tipo de proveedores con los 3 clases de tipos.
        '*** Par�metros de entrada: ninguno
        '*** Llamada desde: propia pagina  						                              
        '*** Tiempo m�ximo: 0seg  						                                       
        '************************************************************

        Dim i As Byte
        Dim item As System.Web.UI.WebControls.ListItem
        item = New System.Web.UI.WebControls.ListItem


        slcTipoProveedor.Items.Clear()

        For i = 1 To 3
            item = New System.Web.UI.WebControls.ListItem
            item.Value = i
            item.Text = sTipoProveedores(i)
            slcTipoProveedor.Items.Add(item)

        Next

        If Request("slcTipoProveedor") <> "" Then
            slcTipoProveedor.Value = Request("slcTipoProveedor")
        End If
    End Sub

    ''' <summary>
    ''' Procedure que inicializa la grid proveedores customizada
    ''' </summary>
    ''' <param name="sender">propios del evento</param>
    ''' <param name="e">propios del evento</param>
    ''' <remarks>Llamada desde: propia pagina; Tiempo m�ximo: 0seg.</remarks>
    Private Sub uwgProveedores_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwgProveedores.InitializeLayout

        uwgProveedores.Bands(0).Columns.FromKey("UNQA").Hidden = True
        uwgProveedores.Bands(0).Columns.FromKey("UNIDADNEGOCIO").Hidden = True
        uwgProveedores.Bands(0).Columns.Add("ANYADIR_REL")

        uwgProveedores.Bands(0).Columns.FromKey("ANYADIR_REL").Move(5)

        uwgProveedores.Bands(0).Columns.FromKey("DEN").Width = Unit.Pixel(350)
        uwgProveedores.Bands(0).Columns.FromKey("DEN").CellStyle.Wrap = True
        uwgProveedores.Bands(0).Columns.FromKey("DEN").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No

        uwgProveedores.Bands(0).Columns.FromKey("COD").Width = Unit.Pixel(100)
        uwgProveedores.Bands(0).Columns.FromKey("COD").CellStyle.Wrap = True
        uwgProveedores.Bands(0).Columns.FromKey("COD").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No

        uwgProveedores.Bands(0).Columns.FromKey("CIF").Width = Unit.Pixel(110)
        uwgProveedores.Bands(0).Columns.FromKey("CIF").CellStyle.Wrap = True
        uwgProveedores.Bands(0).Columns.FromKey("CIF").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No

        uwgProveedores.Bands(0).Columns.FromKey("ANYADIR_REL").Width = Unit.Pixel(250)

        uwgProveedores.Bands(0).Columns.FromKey("DEN").Header.Caption = m_idiomaTablaProveedores(0)
        uwgProveedores.Bands(0).Columns.FromKey("COD").Header.Caption = m_idiomaTablaProveedores(1)
        uwgProveedores.Bands(0).Columns.FromKey("CIF").Header.Caption = m_idiomaTablaProveedores(2)
        uwgProveedores.Bands(0).Columns.FromKey("ANYADIR_REL").Header.Caption = m_idiomaTablaProveedores(3)

        uwgProveedores.Bands(0).Columns.FromKey("DEN").Header.Style.HorizontalAlign = HorizontalAlign.Center
        uwgProveedores.Bands(0).Columns.FromKey("COD").Header.Style.HorizontalAlign = HorizontalAlign.Center
        uwgProveedores.Bands(0).Columns.FromKey("CIF").Header.Style.HorizontalAlign = HorizontalAlign.Center
        uwgProveedores.Bands(0).Columns.FromKey("ANYADIR_REL").Header.Style.HorizontalAlign = HorizontalAlign.Center

        uwgProveedores.Bands(0).Columns.FromKey("COD").CellStyle.HorizontalAlign = HorizontalAlign.Center
        uwgProveedores.Bands(0).Columns.FromKey("CIF").CellStyle.HorizontalAlign = HorizontalAlign.Center

        If Not FSNUser.QAMantenimientoProv Then
            uwgProveedores.Bands(0).Columns.FromKey("POTENCIAL").Type = Infragistics.WebUI.UltraWebGrid.ColumnType.CheckBox
            uwgProveedores.Bands(0).Columns.FromKey("BAJA_CALIDAD").Type = Infragistics.WebUI.UltraWebGrid.ColumnType.CheckBox

            uwgProveedores.Bands(0).Columns.FromKey("POTENCIAL").Width = Unit.Pixel(70)
            uwgProveedores.Bands(0).Columns.FromKey("BAJA_CALIDAD").Width = Unit.Pixel(70)

            uwgProveedores.Bands(0).Columns.FromKey("POTENCIAL").Header.Caption = m_idiomaTablaProveedores(4)
            uwgProveedores.Bands(0).Columns.FromKey("BAJA_CALIDAD").Header.Caption = m_idiomaTablaProveedores(5)

            uwgProveedores.Bands(0).Columns.FromKey("POTENCIAL").Header.Style.HorizontalAlign = HorizontalAlign.Center
            uwgProveedores.Bands(0).Columns.FromKey("BAJA_CALIDAD").Header.Style.HorizontalAlign = HorizontalAlign.Center

            uwgProveedores.Bands(0).Columns.FromKey("POTENCIAL").CellStyle.HorizontalAlign = HorizontalAlign.Center
            uwgProveedores.Bands(0).Columns.FromKey("BAJA_CALIDAD").CellStyle.HorizontalAlign = HorizontalAlign.Center

            uwgProveedores.Bands(0).Columns.FromKey("POTENCIAL").AllowUpdate = AllowUpdate.No
            uwgProveedores.Bands(0).Columns.FromKey("BAJA_CALIDAD").AllowUpdate = AllowUpdate.No

            uwgProveedores.Bands(0).Columns.FromKey("CHECKPOTENCIAL").Hidden = True
            uwgProveedores.Bands(0).Columns.FromKey("CHECKBAJA_CALIDAD").Hidden = True
        Else
            uwgProveedores.Bands(0).Columns.FromKey("POTENCIAL").Hidden = True
            uwgProveedores.Bands(0).Columns.Add("CHECKPOTENCIAL")
            uwgProveedores.Bands(0).Columns.FromKey("CHECKPOTENCIAL").Move(5)
            uwgProveedores.Bands(0).Columns.FromKey("CHECKPOTENCIAL").Width = Unit.Pixel(70)
            uwgProveedores.Bands(0).Columns.FromKey("CHECKPOTENCIAL").Header.Caption = m_idiomaTablaProveedores(4)
            uwgProveedores.Bands(0).Columns.FromKey("CHECKPOTENCIAL").Header.Style.HorizontalAlign = HorizontalAlign.Center
            uwgProveedores.Bands(0).Columns.FromKey("CHECKPOTENCIAL").CellStyle.HorizontalAlign = HorizontalAlign.Center
            uwgProveedores.Bands(0).Columns.FromKey("CHECKPOTENCIAL").AllowUpdate = AllowUpdate.Yes
            uwgProveedores.Bands(0).Columns.FromKey("CHECKPOTENCIAL").Hidden = False

            uwgProveedores.Bands(0).Columns.FromKey("BAJA_CALIDAD").Hidden = True
            uwgProveedores.Bands(0).Columns.Add("CHECKBAJA_CALIDAD")
            uwgProveedores.Bands(0).Columns.FromKey("CHECKBAJA_CALIDAD").Move(5)
            uwgProveedores.Bands(0).Columns.FromKey("CHECKBAJA_CALIDAD").Width = Unit.Pixel(70)
            uwgProveedores.Bands(0).Columns.FromKey("CHECKBAJA_CALIDAD").Header.Caption = m_idiomaTablaProveedores(5)
            uwgProveedores.Bands(0).Columns.FromKey("CHECKBAJA_CALIDAD").Header.Style.HorizontalAlign = HorizontalAlign.Center
            uwgProveedores.Bands(0).Columns.FromKey("CHECKBAJA_CALIDAD").CellStyle.HorizontalAlign = HorizontalAlign.Center
            uwgProveedores.Bands(0).Columns.FromKey("CHECKBAJA_CALIDAD").AllowUpdate = AllowUpdate.Yes
            uwgProveedores.Bands(0).Columns.FromKey("CHECKBAJA_CALIDAD").Hidden = False
        End If

        ' Enable paging by default
        uwgProveedores.DisplayLayout.Pager.AllowPaging = True
        uwgProveedores.DisplayLayout.Pager.StyleMode = Infragistics.WebUI.UltraWebGrid.PagerStyleMode.CustomLabels
    End Sub

    Private Sub chkUnidadesBaja_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUnidadesBaja.CheckedChanged
        '************************************************************************
        '*** Descripci�n:   Procedure que salta al checkear la chech de Mostrar Unidades dados de baja
        '                   Cargar� las unidades de negocio que esten o no dados de baja logica
        '*** Par�metros de entrada: ninguno (propios del evento)
        '*** Llamada desde: propia pagina  						                              
        '*** Tiempo m�ximo: 0,48seg.  						                                       
        '************************************************************
        Dim sIdi As String = FSNUser.Idioma.ToString()

        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If
        CargarArbolUnidades(sIdi, Me.chkUnidadesBaja.Checked, idPyme)
        chkUnidadesBajaValor.Value = IIf(chkUnidadesBaja.Checked, "1", "0")
    End Sub

    ''' <summary>
    ''' Procedure que se ejecuta al recorrer la grid de proveedores por cada linea.
    ''' </summary>
    ''' <param name="indice">Indice que tiene la fila</param>
    ''' <param name="Row">La fila en de la grid en la que esta </param>
    ''' <remarks>Llamada desde: CargarProveedoresConUnidades; Tiempo m�ximo: 0seg.</remarks>
    Private Sub uwgProveedores_InitializeRow_Manual(ByVal indice As Integer, ByVal Row As Infragistics.WebUI.UltraWebGrid.UltraGridRow)
        Row.Height = Unit.Pixel(75)

        Dim tc As Infragistics.WebUI.UltraWebGrid.TemplatedColumn = CType(uwgProveedores.Bands(0).Columns.FromKey("ANYADIR_REL"), Infragistics.WebUI.UltraWebGrid.TemplatedColumn)

        Dim lstUnidadesNegocio As ListBox = CType(tc.CellItems(indice).findControl("lstUnidadesNegocio"), ListBox)

        Dim imagen1 As ImageButton = CType(tc.CellItems(indice).findControl("imgAnyadir"), ImageButton)

        imagen1.Attributes.Add("onclick", "return ActualizarLista('1','" & lstUnidadesNegocio.ClientID & "','" & indice & "');")
        imagen1.CausesValidation = False

        Dim imagen2 As ImageButton = CType(tc.CellItems(indice).findControl("imgEliminar"), ImageButton)

        imagen2.Attributes.Add("onclick", "return ActualizarLista('2','" & lstUnidadesNegocio.ClientID & "','" & indice & "');")
        imagen2.CausesValidation = False


        Dim sUNQAs As String = ""
        Dim sDenUNQAs As String = ""

        Dim oUNQAs() As String
        Dim oDenUONQAs() As String
        If (Not Row.Cells.FromKey("UNQA").Value Is Nothing) And Row.Cells.FromKey("UNQA").Value <> "" Then
            sUNQAs = Row.Cells.FromKey("UNQA").Value
            sDenUNQAs = Row.Cells.FromKey("UNIDADNEGOCIO").Value
            If sUNQAs <> "" Then
                oUNQAs = Split(sUNQAs, ";")
                oDenUONQAs = Split(sDenUNQAs, ";")
            End If
        End If

        Dim i As Integer
        Dim itemList As ListItem

        If sUNQAs <> "" Then
            For i = 0 To oUNQAs.Length - 1
                itemList = New ListItem
                itemList.Value = oUNQAs(i)
                itemList.Text = oDenUONQAs(i)
                itemList.Attributes.Add("title", oDenUONQAs(i))
                lstUnidadesNegocio.Items.Add(itemList)
            Next
        End If

        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHid.ID = "hidUnidadesNegocio_UNQAS_" & indice
        oHid.Value = IIf(sUNQAs = "", sUNQAs, sUNQAs & ";")
        Me.FindControl("Form1").Controls.Add(oHid)

        If FSNUser.QAMantenimientoProv Then
            '-------------- POTENCIAL -------------
            Dim tcP As Infragistics.WebUI.UltraWebGrid.TemplatedColumn = CType(uwgProveedores.Bands(0).Columns.FromKey("CHECKPOTENCIAL"), Infragistics.WebUI.UltraWebGrid.TemplatedColumn)

            Dim chkPotencial As CheckBox = CType(tcP.CellItems(indice).findControl("chkPotencial"), CheckBox)

            'chkPotencial.Attributes.Add("onclick", "return CambioValor('" & chkPotencial.ClientID & "','" & indice & "');")
            chkPotencial.CausesValidation = False

            chkPotencial.Checked = Row.Cells.FromKey("POTENCIAL").Value

            '-------------- BAJA -------------
            Dim tcB As Infragistics.WebUI.UltraWebGrid.TemplatedColumn = CType(uwgProveedores.Bands(0).Columns.FromKey("CHECKBAJA_CALIDAD"), Infragistics.WebUI.UltraWebGrid.TemplatedColumn)

            Dim chkBajaCalidad As CheckBox = CType(tcB.CellItems(indice).findControl("chkBajaCalidad"), CheckBox)
            chkBajaCalidad.CausesValidation = False

            chkBajaCalidad.Checked = Row.Cells.FromKey("BAJA_CALIDAD").Value
        End If
    End Sub


    Private Sub uwgProveedores_PageIndexChanged(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.PageEventArgs) Handles uwgProveedores.PageIndexChanged
        '************************************************************************
        '*** Descripci�n:   Procedure que se ejecuta al cambiar de pagina en la paginaci�n de la grid
        '                   Actualiza la pagina de la paginaci�n y carga los nuevos datos de los proveedores con las unidades de negocio
        '*** Par�metros de entrada: ninguno (propios del evento)                             
        '*** Llamada desde: propia pagina  						                              
        '*** Tiempo m�ximo: 3,69seg.  						                                       
        '************************************************************

        m_bCambioPagina = True

        ActualizarPagina()

        CargarProveedoresConUnidades()


    End Sub

    Private Sub BindData()

        '************************************************************************
        '*** Descripci�n:   Procedure que manda los datos a la grid y si se ha cambiado de pagina almacena los datos
        '*** Par�metros de entrada: ninguno                             
        '*** Llamada desde: propia pagina  						                              
        '*** Tiempo m�ximo: 2,20seg.  						                                       
        '************************************************************

        If (buscar.Value = 1) And m_bCambioPagina = True Then
            AlmacenarDatos()
        End If


        uwgProveedores.DataBind()


        Page.ClientScript.RegisterStartupScript(Me.GetType(),"VisualizarGrid", "<script>VisualizarGrid();</script>")
    End Sub


    Private Sub uwgProveedores_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles uwgProveedores.PreRender

        '************************************************************************
        '*** Descripci�n:   Inicializa la paginacion de la grid
        '*** Par�metros de entrada: ninguno (propios del evento)                            
        '*** Llamada desde: propia pagina  						                              
        '*** Tiempo m�ximo: 0seg.  						                                       
        '************************************************************
        uwgProveedores.DisplayLayout.Pager.CustomLabels = labels
        uwgProveedores.DisplayLayout.Pager.CurrentPageIndex = 1
        uwgProveedores.DisplayLayout.Pager.PageCount = 5
        pds.CurrentPageIndex = 0
    End Sub

    Private Sub ActualizarPagina()
        '************************************************************************
        '*** Descripci�n:   Actualiza el indicador de la pagina actual de la paginaci�n de la grid
        '                   ej: 1 de 5 ( Primero    Anterior    Siguiente   �ltimo)
        '*** Par�metros de entrada: ninguno
        '*** Llamada desde: propia pagina  						                              
        '*** Tiempo m�ximo: 0seg  						                                       
        '************************************************************

        Dim iTotalRows As Integer = Fix(proveedoresCount.Value)
        Dim iLastPage As Integer = Fix(Math.Ceiling(CType(iTotalRows, [Double]) / uwgProveedores.DisplayLayout.Pager.PageSize))

        ' Get data from the viewstate
        Dim iCurrentPage As Integer = 0
        If IsPostBack Then
            iCurrentPage = Fix(ViewState("CurrentPageIndex"))
        End If
        ' Do the math for the next page, and disable labels appropriately
        Select Case uwgProveedores.DisplayLayout.Pager.CurrentPageIndex
            Case 1 ' X of Y label
                ' This one will never get clicked
            Case 2 ' First Page
                iCurrentPage = 1

            Case 3 ' Prev Page
                If iCurrentPage <> 1 Then
                    iCurrentPage -= 1
                End If

            Case 4 ' Next Page
                If iCurrentPage <> iLastPage Then
                    iCurrentPage += 1
                End If

            Case 5 ' Last Page
                iCurrentPage = iLastPage
        End Select


        ' Remove unnecessary labels
        If iCurrentPage = 1 Then
            labels(1) = ""
            labels(2) = ""
        ElseIf iCurrentPage = iLastPage Then
            labels(3) = ""
            labels(4) = ""
        End If

        ' save the current page for next time
        ViewState.Add("CurrentPageIndex", iCurrentPage)

        ' Set the X of Y label
        labels(0) = "&nbsp;" & iCurrentPage & " de " & iLastPage & "&nbsp;"
        uwgProveedores.DisplayLayout.Pager.CurrentPageIndex = iCurrentPage

        If iLastPage < 2 Then
            uwgProveedores.DisplayLayout.Pager.AllowPaging = False
        End If
    End Sub

    ''' <summary>
    ''' Procedure que almacena los datos que se muestran de la grid
    ''' Se crea un dataset con los datos y luego se manda a la clase para almacenarlos...
    ''' </summary>
    ''' <remarks>Llamada desde: BindData      cmdAceptar_ServerClick; Tiempo m�ximo: 2seg.  </remarks>
    Private Sub AlmacenarDatos()
        Dim i, x As Integer
        Dim DS As DataSet
        Dim dt, dt2 As DataTable
        Dim dtNewRow As DataRow

        Dim sCodProve, sIdUNQAs As String
        Dim arrUNQA() As String
        Dim iPotencial As Byte
        Dim bBaja As Boolean
        Dim oUnidadNegQA As FSNServer.UnidadNeg
        oUnidadNegQA = FSNServer.Get_Object(GetType(FSNServer.UnidadNeg))

        DS = New DataSet

        dt = DS.Tables.Add("PROVE_UNQA")
        dt.Columns.Add("PROVE", System.Type.GetType("System.String"))
        dt.Columns.Add("UNQA", System.Type.GetType("System.Int32"))

        dt2 = DS.Tables.Add("PROVE_POTENCIAL_BAJA")
        dt2.Columns.Add("PROVE", System.Type.GetType("System.String"))
        dt2.Columns.Add("POTENCIAL", System.Type.GetType("System.Byte"))
        dt2.Columns.Add("BAJA", System.Type.GetType("System.Byte"))

        Dim tcP As Infragistics.WebUI.UltraWebGrid.TemplatedColumn
        Dim tcB As Infragistics.WebUI.UltraWebGrid.TemplatedColumn
        If FSNUser.QAMantenimientoProv Then
            tcp = CType(uwgProveedores.Bands(0).Columns.FromKey("CHECKPOTENCIAL"), Infragistics.WebUI.UltraWebGrid.TemplatedColumn)
            tcB = CType(uwgProveedores.Bands(0).Columns.FromKey("CHECKBAJA_CALIDAD"), Infragistics.WebUI.UltraWebGrid.TemplatedColumn)
        End If

        For i = 0 To uwgProveedores.Rows.Count - 1

            sCodProve = uwgProveedores.Rows(i).Cells.FromKey("COD").Value()

            sIdUNQAs = Request("hidUnidadesNegocio_UNQAS_" & i)

            If sIdUNQAs <> "" Then
                arrUNQA = Split(sIdUNQAs, ";")
                For x = 0 To UBound(arrUNQA)
                    If arrUNQA(x) <> "" Then
                        dtNewRow = dt.NewRow

                        dtNewRow.Item("PROVE") = sCodProve
                        dtNewRow.Item("UNQA") = arrUNQA(x)
                        If dtNewRow.RowState = DataRowState.Detached Then
                            dt.Rows.Add(dtNewRow)
                        End If
                    End If
                Next
            Else
                dtNewRow = dt.NewRow

                dtNewRow.Item("PROVE") = sCodProve
                dtNewRow.Item("UNQA") = 0
                If dtNewRow.RowState = DataRowState.Detached Then
                    dt.Rows.Add(dtNewRow)
                End If

            End If

            If FSNUser.QAMantenimientoProv Then
                Dim chkPotencial As CheckBox = CType(tcP.CellItems(i).findControl("chkPotencial"), CheckBox)
                iPotencial = (chkPotencial.Checked = True)

                Dim chkBajaCalidad As CheckBox = CType(tcB.CellItems(i).findControl("chkBajaCalidad"), CheckBox)
                bBaja = chkBajaCalidad.Checked
            Else
                iPotencial = uwgProveedores.Rows(i).Cells.FromKey("POTENCIAL").Value

                bBaja = uwgProveedores.Rows(i).Cells.FromKey("BAJA_CALIDAD").Value
            End If

            dtNewRow = dt2.NewRow
            dtNewRow.Item("PROVE") = sCodProve

            If iPotencial = 0 Then
                iPotencial = 2
            Else
                iPotencial = 1
            End If

            dtNewRow.Item("POTENCIAL") = iPotencial
            dtNewRow.Item("BAJA") = bBaja
            If dtNewRow.RowState = DataRowState.Detached Then
                dt2.Rows.Add(dtNewRow)
            End If

        Next

        oUnidadNegQA.AlmacenarRelacionesProveedores(DS)


        oUnidadNegQA = Nothing
    End Sub

    Private Sub cmdAceptar_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAceptar.ServerClick
        '************************************************************************
        '*** Descripci�n:   Procedure evento del boton aceptar--> Almacena los datos que muestra la grid de proveedores  
        '*** Par�metros de entrada: sender, e; Propios del evento...
        '*** Llamada desde: propia pagina  						                              
        '*** Tiempo m�ximo: 2seg.  						                                       
        '************************************************************

        AlmacenarDatos()
        uwgProveedores.Visible = False

    End Sub

    Private Sub CargarProveedoresConUnidades2()

        '************************************************************************
        '*** Descripci�n:   Procedure que carga los datos de los proveedores relacionados con las unidades de negocio de la grid que hay que exportar
        '*** Par�metros de entrada: ninguno
        '*** Llamada desde: propia pagina  						                              
        '*** Tiempo m�ximo: 0,15seg.  						                                       
        '************************************************************

        Dim i As Integer
        Dim sIdi As String = FSNUser.Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        Dim sUNQAs As String = Request("cadenaUNQACheckeados")
        Dim sAux(), arrDatos() As String
        Dim sCadenaIDsUNQA As String
        Dim sCadenaAux As String
        If sUNQAs <> "" Then
            sAux = sUNQAs.Split(";")
            For i = 0 To UBound(sAux)
                If sAux(i) <> "" Then
                    arrDatos = sAux(i).Split("#")
                    sCadenaAux = arrDatos(0).Replace("/", "")
                    If sCadenaAux <> "" Then
                        If sCadenaIDsUNQA <> "" Then sCadenaIDsUNQA = sCadenaIDsUNQA + ","
                        sCadenaIDsUNQA = sCadenaIDsUNQA + sCadenaAux
                    End If
                End If
            Next
        End If

        Dim oProves As FSNServer.Proveedores
        oProves = FSNServer.Get_Object(GetType(FSNServer.Proveedores))
        oProves.ProveedoresConUnidadesNegocio(FSNUser.Cod, sIdi, _
            FSNUser.QARestProvContacto, FSNUser.QARestProvMaterial, FSNUser.QARestProvEquipo, _
            txtCodigo.Value, txtDenominacion.Value, txtCIF.Value, chkProveedorBaja.Checked, slcTipoProveedor.Value, _
            sCadenaIDsUNQA, True)

        uwgProveedoresAux.DataSource = oProves.Data.Tables(0)

        oProves = Nothing

    End Sub


    Private Sub linkExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles linkExcel.Click
        '************************************************************************
        '*** Descripci�n:   Procedure que exporta a Excel   
        '*** Par�metros de entrada: sender, e; Propios del evento...
        '*** Llamada desde: propia pagina  						                              
        '*** Tiempo m�ximo: 0,18seg.  						                                       
        '************************************************************


        uwgProveedoresAux.DisplayLayout.Pager.AllowPaging = False
        uwgProveedoresAux.DisplayLayout.Pager.PageSize = 0



        CargarProveedoresConUnidades2()
        uwgProveedoresAux.DataBind()

        uwgProveedoresAux.Bands(0).Columns.FromKey("UNQA").Hidden = True
        uwgProveedoresAux.Bands(0).Columns.FromKey("DEN").Width = Unit.Percentage(30)
        uwgProveedoresAux.Bands(0).Columns.FromKey("COD").Width = Unit.Percentage(10)
        uwgProveedoresAux.Bands(0).Columns.FromKey("CIF").Width = Unit.Percentage(10)
        uwgProveedoresAux.Bands(0).Columns.FromKey("UNIDADNEGOCIO").Width = Unit.Percentage(30)
        uwgProveedoresAux.Bands(0).Columns.FromKey("POTENCIAL").Width = Unit.Percentage(10)
        uwgProveedoresAux.Bands(0).Columns.FromKey("BAJA_CALIDAD").Width = Unit.Percentage(10)

        uwgProveedoresAux.Bands(0).Columns.FromKey("DEN").Header.Caption = m_idiomaTablaProveedores(0)
        uwgProveedoresAux.Bands(0).Columns.FromKey("COD").Header.Caption = m_idiomaTablaProveedores(1)
        uwgProveedoresAux.Bands(0).Columns.FromKey("CIF").Header.Caption = m_idiomaTablaProveedores(2)
        uwgProveedoresAux.Bands(0).Columns.FromKey("UNIDADNEGOCIO").Header.Caption = m_idiomaTablaProveedores(3)
        uwgProveedoresAux.Bands(0).Columns.FromKey("POTENCIAL").Header.Caption = m_idiomaTablaProveedores(4)
        uwgProveedoresAux.Bands(0).Columns.FromKey("BAJA_CALIDAD").Header.Caption = m_idiomaTablaProveedores(5)

        uwgProveedoresAux.Bands(0).Columns.FromKey("DEN").Header.Style.HorizontalAlign = HorizontalAlign.Center
        uwgProveedoresAux.Bands(0).Columns.FromKey("COD").Header.Style.HorizontalAlign = HorizontalAlign.Center
        uwgProveedoresAux.Bands(0).Columns.FromKey("CIF").Header.Style.HorizontalAlign = HorizontalAlign.Center
        uwgProveedoresAux.Bands(0).Columns.FromKey("UNIDADNEGOCIO").Header.Style.HorizontalAlign = HorizontalAlign.Center
        uwgProveedoresAux.Bands(0).Columns.FromKey("POTENCIAL").Header.Style.HorizontalAlign = HorizontalAlign.Center
        uwgProveedoresAux.Bands(0).Columns.FromKey("BAJA_CALIDAD").Header.Style.HorizontalAlign = HorizontalAlign.Center


        uwgProveedoresAux.Bands(0).Columns.FromKey("COD").CellStyle.HorizontalAlign = HorizontalAlign.Center
        uwgProveedoresAux.Bands(0).Columns.FromKey("CIF").CellStyle.HorizontalAlign = HorizontalAlign.Center
        uwgProveedoresAux.Bands(0).Columns.FromKey("POTENCIAL").CellStyle.HorizontalAlign = HorizontalAlign.Center
        uwgProveedoresAux.Bands(0).Columns.FromKey("BAJA_CALIDAD").CellStyle.HorizontalAlign = HorizontalAlign.Center


        'Exporta a Excel:
        UltraWebGridExcelExporter1.DownloadName = "UnidadesNegocio"
        Me.UltraWebGridExcelExporter1.Export(Me.uwgProveedoresAux)

        uwgProveedoresAux = Nothing


    End Sub

    Private Sub GenerarHTMLPrint()
        '************************************************************************
        '*** Descripci�n:   Generera una pagina HTML para exportar a HTML y a PDF.
        '***                Se aplica a la 2� grid ya que no tiene paginaci�n.                    
        '*** Llamada desde: propia pagina  						                              
        '*** Tiempo m�ximo: 0,14seg.  						                                       
        '************************************************************

        Dim oHtmlTextWriter As New System.Web.UI.HtmlTextWriter(oStringWriter)
        Dim iLinea As Integer
        Dim iCol As Integer
        uwgProveedoresAux.DisplayLayout.Pager.AllowPaging = False
        uwgProveedoresAux.DisplayLayout.Pager.PageSize = 0

        CargarProveedoresConUnidades2()
        uwgProveedoresAux.DataBind()

        uwgProveedoresAux.Bands(0).Columns.FromKey("UNQA").Hidden = True
        uwgProveedoresAux.Bands(0).Columns.FromKey("DEN").Width = Unit.Percentage(30)
        uwgProveedoresAux.Bands(0).Columns.FromKey("COD").Width = Unit.Percentage(10)
        uwgProveedoresAux.Bands(0).Columns.FromKey("CIF").Width = Unit.Percentage(10)
        uwgProveedoresAux.Bands(0).Columns.FromKey("UNIDADNEGOCIO").Width = Unit.Percentage(30)
        uwgProveedoresAux.Bands(0).Columns.FromKey("POTENCIAL").Width = Unit.Percentage(10)
        uwgProveedoresAux.Bands(0).Columns.FromKey("BAJA_CALIDAD").Width = Unit.Percentage(10)

        uwgProveedoresAux.Bands(0).Columns.FromKey("DEN").Header.Caption = m_idiomaTablaProveedores(0)
        uwgProveedoresAux.Bands(0).Columns.FromKey("COD").Header.Caption = m_idiomaTablaProveedores(1)
        uwgProveedoresAux.Bands(0).Columns.FromKey("CIF").Header.Caption = m_idiomaTablaProveedores(2)
        uwgProveedoresAux.Bands(0).Columns.FromKey("UNIDADNEGOCIO").Header.Caption = m_idiomaTablaProveedores(3)
        uwgProveedoresAux.Bands(0).Columns.FromKey("POTENCIAL").Header.Caption = m_idiomaTablaProveedores(4)
        uwgProveedoresAux.Bands(0).Columns.FromKey("BAJA_CALIDAD").Header.Caption = m_idiomaTablaProveedores(5)

        oHtmlTextWriter.WriteLine("<HTML>")
        oHtmlTextWriter.WriteLine("<HEAD>")
        oHtmlTextWriter.WriteLine("<meta http-equiv=""Content-Type"" content=""text/html; charset=windows-1252"">")
        oHtmlTextWriter.WriteLine("<STYLE>@import url( " & ConfigurationManager.AppSettings("ruta") + "App_Themes/" & Me.Theme & ".css );</STYLE>")
        oHtmlTextWriter.WriteLine("</HEAD>")
        oHtmlTextWriter.WriteLine("<BODY>")

        'Va generando la p�gina HTML con los datos de la grid:
        oHtmlTextWriter.WriteLine("<TABLE border=0>")
        oHtmlTextWriter.WriteLine("<TR><TD>")
        oHtmlTextWriter.WriteLine("<label class=titulo>" & Me.lblTitulo.Text & "</label>")
        oHtmlTextWriter.WriteLine("</TD></TR>")

        oHtmlTextWriter.WriteLine("<TR><TD><TABLE border=0>")
        'La cabecera:
        oHtmlTextWriter.WriteLine("<TR>")
        For iCol = 0 To Me.uwgProveedoresAux.Bands(0).Columns.Count - 1
            If uwgProveedoresAux.Bands(0).Columns(iCol).Hidden = False Then
                oHtmlTextWriter.WriteLine("<TD class=CaptionBlueSmallBold>")
                oHtmlTextWriter.WriteLine(uwgProveedoresAux.Bands(0).Columns(iCol).Header.Caption)
                oHtmlTextWriter.WriteLine("</TD>")
            End If
        Next
        oHtmlTextWriter.WriteLine("</TR>")

        'Los datos:
        For iLinea = 0 To Me.uwgProveedoresAux.Rows.Count - 1
            oHtmlTextWriter.WriteLine("<TR class=VisorRow>")
            For iCol = 0 To Me.uwgProveedoresAux.Bands(0).Columns.Count - 1
                If uwgProveedoresAux.Bands(0).Columns(iCol).Hidden = False Then
                    oHtmlTextWriter.WriteLine("<TD>")
                    Dim sValor As String
                    sValor = uwgProveedoresAux.Rows(iLinea).Cells(iCol).Value()
                    oHtmlTextWriter.WriteLine(sValor)
                    oHtmlTextWriter.WriteLine("</TD>")
                End If
            Next
            oHtmlTextWriter.WriteLine("</TR>")
        Next
        uwgProveedoresAux = Nothing
        oHtmlTextWriter.WriteLine("</TD></TR></TABLE>")
        oHtmlTextWriter.WriteLine("</BODY>")
        oHtmlTextWriter.WriteLine("</HTML>")

    End Sub


    Private Sub linkHTML_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles linkHTML.Click
        '************************************************************************
        '*** Descripci�n: Evento click HTML --> Exporta a HTML                                     
        '*** Par�metros de entrada: sender, e; Propios del evento...

        '*** Llamada desde: propia pagina  						                              
        '*** Tiempo m�ximo: 0,15seg.  						                                       
        '************************************************************

        GenerarHTMLPrint()

        '''''' Guardamos el HTML en un archivo
        Dim sPath As String
        Dim sBytes As Long
        Dim oFolder As System.IO.Directory

        sPath = ConfigurationManager.AppSettings("temp") + "\" + GenerateRandomPath()
        If Not oFolder.Exists(sPath) Then
            oFolder.CreateDirectory(sPath)
        End If
        sPath += "\" + "UnidadesNegocio.html"

        Dim oStreamWriter As New System.IO.StreamWriter(sPath, False, System.Text.Encoding.UTF8)

        oStreamWriter.Write(oStringWriter.ToString)
        oStreamWriter.Close()

        ''''''' Ponemos el html a descargar
        Dim ofile As New System.IO.FileInfo(sPath)
        sBytes = ofile.Length
        Dim arrPath2() As String = sPath.Split("\")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "imprimir", "<script>var newWindow = window.open(""" & ConfigurationManager.AppSettings("rutaFS") & "_Common/download.aspx?path=" & arrPath2(UBound(arrPath2) - 1) & "&nombre=" & Server.UrlEncode(arrPath2(UBound(arrPath2))) & "&datasize=" & CStr(sBytes) & """,""_blank"",""width=600,height=275,status=no,resizable=yes,top=200,left=200,menubar=yes"");</script>")
        Page.ClientScript.RegisterStartupScript(Me.GetType(),"VisualizarGrid", "<script>VisualizarGrid();</script>")
        ActualizarPagina()

    End Sub

    Private Sub linkPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles linkPDF.Click
        '************************************************************************
        '*** Descripci�n: Evento click PDF -->Exporta a PDF                                     
        '*** Par�metros de entrada: sender, e; Propios del evento...
        '*** Llamada desde: propia pagina  						                              
        '*** Tiempo m�ximo: 0,48seg.  						                                       
        '************************************************************

        GenerarHTMLPrint()

        '''''' Guardamos el HTML en un archivo
        Dim sPathBase As String
        Dim sPathHTML As String
        Dim sPathPDF As String
        Dim sBytes As Long
        Dim oFolder As System.IO.Directory

        sPathBase = ConfigurationManager.AppSettings("temp") + "\" + GenerateRandomPath()
        If Not oFolder.Exists(sPathBase) Then
            oFolder.CreateDirectory(sPathBase)
        End If
        sPathHTML = sPathBase + "\" + "UnidadesNegocio.html"
        sPathPDF = sPathBase + "\" + "UnidadesNegocio.pdf"

        Dim oStreamWriter As New System.IO.StreamWriter(sPathHTML, False, System.Text.Encoding.UTF8)
        oStreamWriter.Write(oStringWriter.ToString)
        oStreamWriter.Close()

        ''' Pasamos el HTML a PDF
        ConvertHTMLToPDF(sPathHTML, sPathPDF)

        ''''''' Ponemos el PDF a descargar
        Dim ofile As New System.IO.FileInfo(sPathPDF)
        sBytes = ofile.Length
        Dim arrPath2() As String = sPathPDF.Split("\")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "imprimir", "<script>var newWindow = window.open(""" & ConfigurationManager.AppSettings("rutaFS") & "_Common/download.aspx?path=" & arrPath2(UBound(arrPath2) - 1) & "&nombre=" & Server.UrlEncode(arrPath2(UBound(arrPath2))) & "&datasize=" & CStr(sBytes) & """,""_blank"",""width=600,height=275,status=no,resizable=yes,top=200,left=200,menubar=yes"");</script>")
        Page.ClientScript.RegisterStartupScript(Me.GetType(),"VisualizarGrid", "<script>VisualizarGrid();</script>")
        ActualizarPagina()

    End Sub

    Private Sub uwgProveedores_SortColumn(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.SortColumnEventArgs) Handles uwgProveedores.SortColumn
        '************************************************************************
        '*** Descripci�n: Ordenaci�n de la grid  uwgProveedores por parte del servidor 
        '*** Par�metros de entrada: sender, e; Propios del evento...
        '*** Llamada desde: propia pagina  						                              
        '*** Tiempo m�ximo: 0,21seg  						                                      
        '************************************************************

        uwgProveedores.DisplayLayout.Pager.CurrentPageIndex = 302
        ViewState.Add("CurrentPageIndex", 1)
        ActualizarPagina()

        Dim sSortCriteria As String = ""
        If uwgProveedores.DisplayLayout.Bands(0).SortedColumns.Count > 0 Then
            sSortCriteria = ""
            Dim cols As IEnumerator = uwgProveedores.DisplayLayout.Bands(0).SortedColumns.GetEnumerator()
            While cols.MoveNext()
                sSortCriteria += CType(cols.Current, UltraGridColumn).Key & " " & IIf(CType(cols.Current, UltraGridColumn).SortIndicator = SortIndicator.Descending, " DESC,", " ASC,")
            End While
            sSortCriteria = sSortCriteria.TrimEnd(New Char() {","c})
        End If

        Sort.Value = sSortCriteria


        CargarProveedoresConUnidades()

    End Sub


    Private Sub cmdCancelar_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelar.ServerClick
        '************************************************************************
        '*** Descripci�n: Oculta la grid de proveedores
        '*** Par�metros de entrada: sender, e; Propios del evento...
        '*** Llamada desde: propia pagina  						                              
        '*** Tiempo m�ximo: 0seg  						                                      
        '************************************************************

        uwgProveedores.Visible = False

    End Sub

    ''' <summary>
    ''' Procedimiento en el que configuramos los controles relacionados con la paginaci�n en funci�n del contenido 
    ''' del objeto pds (pageddatasource). 
    ''' </summary>
    ''' <remarks>Llamada desde=Propia pagina
    ''' Tiempo m�ximo: 0 sec.</remarks>
    Private Sub InitPagedDataSource()

        pds.AllowPaging = True
        pds.PageSize = ConfigurationManager.AppSettings("registrosPaginacion")
        pds.CurrentPageIndex = Fix(ViewState("CurrentPageIndex")) - 1
        uwgProveedores.DataSource = pds
        Me.BindData()
    End Sub

    Private Sub uwgProveedoresAux_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgProveedoresAux.InitializeRow

        If (Not e.Row.Cells.FromKey("POTENCIAL").Value Is Nothing) Then
            e.Row.Cells.FromKey("POTENCIAL").Value = IIf(e.Row.Cells.FromKey("POTENCIAL").Value = 1, m_sSi, m_sNo)
        End If
        If (Not e.Row.Cells.FromKey("BAJA_CALIDAD").Value Is Nothing) Then
            e.Row.Cells.FromKey("BAJA_CALIDAD").Value = IIf(e.Row.Cells.FromKey("BAJA_CALIDAD").Value = 1, m_sSi, m_sNo)
        End If
    End Sub

    ''' <summary>
    ''' Procedure que carga los datos de los proveedores relacionados con las unidades de negocio de la grid proveedores
    ''' </summary>
    ''' <param name="sender">propios del evento</param>
    ''' <param name="e">propios del evento</param>        
    ''' <remarks>Llamada desde=propia pagina  ; Tiempo m�ximo=0,29seg.</remarks>
    Private Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        CargarDatosProveedoresRelacionados()
    End Sub

    Private Sub CargarDatosProveedoresRelacionados()
        '************************************************************************
        '*** Descripci�n:   Procedure que carga los datos de los proveedores relacionados con las unidades de negocio de la grid proveedores
        '*** Par�metros de entrada: ninguno (propios del evento)
        '*** Llamada desde: propia pagina  						                              
        '*** Tiempo m�ximo: 0,29seg.  						                                       
        '************************************************************


        buscar.Value = "1"
        uwgProveedores.DisplayLayout.EnableInternalRowsManagement = True
        uwgProveedores.DisplayLayout.Pager.CurrentPageIndex = 1
        uwgProveedores.DisplayLayout.Pager.PageSize = ConfigurationManager.AppSettings("registrosPaginacion")
        uwgProveedores.DisplayLayout.Pager.PageCount = 4
        ViewState.Add("CurrentPageIndex", 1)
        Sort.Value = ""
        CargarProveedoresConUnidades()

        ActualizarPagina()
    End Sub
End Class

