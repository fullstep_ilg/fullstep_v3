
    Public Class altaMaterialQA
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
        Protected WithEvents uwtMateriales As Infragistics.WebUI.UltraWebNavigator.UltraWebTree
        Protected WithEvents uwtTiposCertif As Infragistics.WebUI.UltraWebNavigator.UltraWebTree
        Protected WithEvents lblMat As System.Web.UI.WebControls.Label
        Protected WithEvents lblCertif As System.Web.UI.WebControls.Label
        Protected WithEvents tblDenominacion As System.Web.UI.HtmlControls.HtmlTable
        Protected WithEvents lblDen As System.Web.UI.WebControls.Label
        Protected WithEvents cmdAceptar As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents cmdCancelar As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents Idioma As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents lblVolver As System.Web.UI.WebControls.Label

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Const AltaScriptKey As String = "AltaIncludeScript"
        Private Const AltaFileName As String = "js/jsAltaMat.js"
        Private Const IncludeScriptFormat As String = ControlChars.CrLf & _
  "<script type=""{0}"" src=""{1}""></script>"
        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
    "<script language=""{0}"">{1}</script>"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.AltaMaterialQA
        If Page.IsPostBack Then Exit Sub

        Dim oCelda As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oFila As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oText As System.Web.UI.WebControls.TextBox
        Dim oLabel As System.Web.UI.WebControls.Label
        Dim oIdiomas As FSNServer.Idiomas = FSNServer.Get_Object(GetType(FSNServer.Idiomas))
        Dim oIdioma As FSNServer.Idioma
        Dim node As Infragistics.WebUI.UltraWebNavigator.Node
        Dim oSolicitudes As FSNServer.Solicitudes
        Dim osolicitud As DataRow
        Dim sIdi As String = FSNUser.Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        Idioma.Value = sIdi

        Me.lblTitulo.Text = Textos(0)
        Me.lblCertif.Text = Textos(1)
        Me.lblMat.Text = Textos(2)
        Me.cmdAceptar.Value = Textos(5)
        Me.cmdCancelar.Value = Textos(6)
        Me.lblDen.Text = Textos(7)

        'Texto volver
        Me.lblVolver.Text = Chr(171) & Textos(12)  'Link volver
        '************* Carga las denominaciones *************
        oIdiomas.Load()

        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(Textos(8)) + " " + oIdiomas.Idiomas.Item(sIdi).Den + " ';"   'Debe introducir la denominaci�n en xxx
        sClientTexts += "arrTextosML[1] = '" + JSText(Textos(9)) + "';"  'Seleccione al menos un certificado
        sClientTexts += "arrTextosML[2] = '" + JSText(Textos(10)) + "';"  'Seleccione al menos un material de GS
        sClientTexts += "arrTextosML[3] = '" + JSText(Textos(11)) + " ';"   'Imposible asignar el material de GS.\r Ya est� asignado al siguiente material de QA:
        sClientTexts += "var arrDenominacionesMat = new Array();"

        For Each oIdioma In oIdiomas.Idiomas  'Carga en un array tb los diferentes idiomas
            sClientTexts += "arrDenominacionesMat[arrDenominacionesMat.length] = '" + oIdioma.Cod + "';"
        Next
        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        Page.ClientScript.RegisterStartupScript(Me.GetType(),"TextosMICliente", sClientTexts)


        'Carga 1� el idioma del usuario:
        oIdioma = oIdiomas.Idiomas.Item(sIdi)
        oFila = New System.Web.UI.HtmlControls.HtmlTableRow
        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
        oLabel = New System.Web.UI.WebControls.Label
        oLabel.Text = oIdioma.Den & " :"
        oLabel.CssClass = "parrafo"
        oCelda.Controls.Add(oLabel)
        oFila.Cells.Add(oCelda)

        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
        oText = New System.Web.UI.WebControls.TextBox
        oText.ID = "txtDen_" & oIdioma.Cod
        oText.Width = Unit.Pixel(400)
        oText.MaxLength = 200
        oCelda.Controls.Add(oText)
        oFila.Cells.Add(oCelda)
        Me.tblDenominacion.Rows.Add(oFila)

        'ahora carga el resto de idiomas
        For Each oIdioma In oIdiomas.Idiomas
            If oIdioma.Cod <> sIdi Then
                oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oLabel = New System.Web.UI.WebControls.Label
                oLabel.Text = oIdioma.Den & " :"
                oLabel.CssClass = "parrafo"
                oCelda.Controls.Add(oLabel)
                oFila.Cells.Add(oCelda)

                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oText = New System.Web.UI.WebControls.TextBox
                oText.ID = "txtDen_" & oIdioma.Cod
                oText.Width = Unit.Pixel(400)
                oText.MaxLength = 200
                oCelda.Controls.Add(oText)
                oFila.Cells.Add(oCelda)

                Me.tblDenominacion.Rows.Add(oFila)
            End If
        Next
        oIdiomas = Nothing


        '************* Carga los tipos de certificados en el �rbol *************
        node = uwtTiposCertif.Nodes.Add(Textos(3), "raiz")  'Carga el nodo ra�z

        oSolicitudes = FSNServer.Get_Object(GetType(FSNServer.Solicitudes))
        oSolicitudes.LoadCertificados(sIdi)

        For Each osolicitud In oSolicitudes.Data.Tables(0).Rows  'Carga los tipos de certificados
            node.Nodes.Add(osolicitud.Item("COD") & "-" & osolicitud.Item("DEN"), osolicitud.Item("ID"))
        Next
        oSolicitudes = Nothing
        Me.uwtTiposCertif.Levels(1).LevelCheckBoxes = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.True
        Me.uwtTiposCertif.ExpandAll()


        '************* Carga el �rbol de materiales *************
        Dim oGruposMaterial As FSNServer.MaterialesQA
        oGruposMaterial = FSNServer.Get_Object(GetType(FSNServer.MaterialesQA))
        Dim dsMaterialesGS As DataSet
        dsMaterialesGS = oGruposMaterial.GetMaterialesGS(sIdi, , FSNUser.Pyme)
        oGruposMaterial = Nothing
        Me.uwtMateriales.ClearAll()

        Me.uwtMateriales.DataSource = dsMaterialesGS.Tables(0).DefaultView
        Me.uwtMateriales.Levels(0).LevelImage = "../_common/images/carpetaCommodityTxiki.gif"
        Me.uwtMateriales.Levels(0).RelationName = "REL_NIV1_NIV2"
        Me.uwtMateriales.Levels(0).ColumnName = "DEN"
        Me.uwtMateriales.Levels(0).LevelKeyField = "COD"
        Me.uwtMateriales.Levels(0).LevelCheckBoxes = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.True
        Me.uwtMateriales.Levels(0).CheckboxColumnName = "TIENE_MATERIAL"

        Me.uwtMateriales.Levels(1).LevelImage = "../_common/images/carpetaFamilia.gif"
        Me.uwtMateriales.Levels(1).RelationName = "REL_NIV2_NIV3"
        Me.uwtMateriales.Levels(1).ColumnName = "DEN"
        Me.uwtMateriales.Levels(1).LevelKeyField = "COD"
        Me.uwtMateriales.Levels(1).LevelCheckBoxes = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.True
        Me.uwtMateriales.Levels(1).CheckboxColumnName = "TIENE_MATERIAL"

        Me.uwtMateriales.Levels(2).LevelImage = "../_common/images/carpetaGrupo.gif"
        Me.uwtMateriales.Levels(2).RelationName = "REL_NIV3_NIV4"
        Me.uwtMateriales.Levels(2).ColumnName = "DEN"
        Me.uwtMateriales.Levels(2).LevelKeyField = "COD"
        Me.uwtMateriales.Levels(2).LevelCheckBoxes = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.True
        Me.uwtMateriales.Levels(2).CheckboxColumnName = "TIENE_MATERIAL"

        Me.uwtMateriales.Levels(3).LevelImage = "../_common/images/carpetaCerrada.gif"
        Me.uwtMateriales.Levels(3).ColumnName = "DEN"
        Me.uwtMateriales.Levels(3).LevelKeyField = "COD"
        Me.uwtMateriales.Levels(3).LevelCheckBoxes = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.True
        Me.uwtMateriales.Levels(3).CheckboxColumnName = "TIENE_MATERIAL"

        Me.uwtMateriales.DataBind()
        Me.uwtMateriales.DataKeyOnClient = True

        Dim includeScript As String

        If Not Page.ClientScript.IsClientScriptIncludeRegistered(AltaScriptKey) Then
            includeScript = String.Format(IncludeScriptFormat, "text/javascript", AltaFileName)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(),AltaScriptKey, includeScript)
        End If
        dsMaterialesGS = Nothing
    End Sub
    ''' <summary>
    ''' Al leer cada nodo se comprueba si el nodo leido pertenece al material QA Seleccionado.
    ''' Si pertenece, lo deja checkeado para que pueda deseleccionarlo.
    ''' Sino, pertenece a otro material QA por lo que el check estara deshabilitado y tampoco podra elegir ninguno de sus padres...
    ''' </summary>
    ''' <param name="sender">propios del evento</param>
    ''' <param name="e">propios del evento</param>        
    ''' <remarks>Llamada desde=Evento que salta al cargarse el arbol (Databind); Tiempo m�ximo:=>0,3seg.</remarks>
    Private Sub uwtMateriales_NodeBound(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebNavigator.WebTreeNodeEventArgs) Handles uwtMateriales.NodeBound
        Dim oNode As Infragistics.WebUI.UltraWebNavigator.Node
        Dim aux() As String

        If e.Node.Checked = True Then
            aux = e.Node.Text.Split("(-*)")
            If UBound(aux) > 0 Then
                e.Node.Text = aux(0)
                e.Node.Title = Textos(13) & ": " & aux(1).Replace("-*)", "")
            End If
            e.Node.Expanded = False
            e.Node.Checked = False
            If Not e.Node.Parent Is Nothing Then
                e.Node.CheckBox = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.False
                e.Node.Enabled = True
                oNode = e.Node.Parent
                While Not oNode Is Nothing
                    oNode.CheckBox = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.False
                    oNode.Enabled = True
                    oNode.Expanded = False
                    oNode = oNode.Parent
                End While
            End If
        End If
    End Sub
End Class

