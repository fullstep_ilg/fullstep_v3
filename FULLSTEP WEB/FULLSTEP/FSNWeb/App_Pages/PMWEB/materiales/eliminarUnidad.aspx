<%@ Register TagPrefix="igtbl" Namespace="Infragistics.WebUI.UltraWebGrid" Assembly="Infragistics.WebUI.UltraWebGrid.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="eliminarUnidad.aspx.vb" Inherits="Fullstep.FSNWeb.eliminarUnidad"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<script>
		function actualizarArbol()
		{
		
		/*
			�************************************************************
			�*** Descripci�n: Elimina/Lo pone de baja logica un nodo del arbol de unidades de negocio          	  	                         	                   
			�*** Llamada desde: propia pagina (VB) 							                
			�*** Tiempo m�ximo: 0seg  							               
			�************************************************************
		*/
			//Elimina del �rbol el material que hemos eliminado
			var p=window.opener  
			var oTree = p.igtree_getTreeById("uwtUnidades")
			var oNode=oTree.getSelectedNode()
			if (oNode)
			  {
				oNode.remove()				  
				
			  }
			window.close() 
		}
		function Init() {
		/*
			�************************************************************
			�*** Descripci�n: Funcion que se ejecuta al cargar la pagina, devuelve el foco, sino aparece en un segundo plano...
			�*** Llamada desde: propia pagina  							                
			�*** Tiempo m�ximo: 0seg  							               
			�************************************************************
		*/
			window.focus();
		}
	</script>	
	<body onload="Init()">
		<form id="Form1" method="post" runat="server">
			<input id="accion" type="hidden" name="accion" runat="server"> <input id="IDUNQA" type="hidden" name="IDUNQA" runat="server">
			<asp:label id="lblTitulo" style="Z-INDEX: 106; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server"
				Width="90%" Height="3px" CssClass="titulo"> DEliminaci�n unidad de negocio</asp:label>
			<asp:label id="lblUnidad" style="Z-INDEX: 108; LEFT: 24px; POSITION: absolute; TOP: 48px" runat="server"
				Width="90%" Height="3px" CssClass="captionBlue">DUnidad</asp:label>
			<TABLE id="tblCabecera" style="Z-INDEX: 109; LEFT: 24px; POSITION: absolute; TOP: 80px; HEIGHT: 20px"
				cellSpacing="1" cellPadding="1" width="90%" border="0">
				<TR>
					<TD><asp:label id="lblMensaje" runat="server" Width="100%" CssClass="SinDatos">Label</asp:label></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblMensaje2" runat="server" Width="100%" CssClass="parrafo">DVer variables calidad relacionadas</asp:label></TD>
				</TR>
				<TR vAlign="top" height="60">
					<TD vAlign="top"></TD>
				</TR>
			</TABLE>
			<TABLE id="Table1" style="Z-INDEX: 107; LEFT: 24px; POSITION: absolute; TOP: 232px" cellSpacing="1"
				cellPadding="1" width="90%" border="0">
				<TR>
					<TD align="center"><INPUT class="botonPMWEB" id="cmdAceptar" type="button" value="DAceptar" name="cmdAceptar" runat="server"></TD>
					<TD align="center"><INPUT class="botonPMWEB" id="cmdCancelar" onclick="window.close()" type="button" value="Cancelar"
							name="cmdCancelar" runat="server"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</html>
