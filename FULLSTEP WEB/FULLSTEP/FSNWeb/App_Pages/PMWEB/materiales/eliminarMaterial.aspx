<%@ Page Language="vb" AutoEventWireup="false" Codebehind="eliminarMaterial.aspx.vb" Inherits="Fullstep.FSNWeb.eliminarMaterial"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<script>
		function actualizarArbol()
		{
			//Elimina del �rbol el material que hemos eliminado
			var p=window.opener  
			var oTree = p.igtree_getTreeById("uwtMateriales")
			var oNode=oTree.getSelectedNode()
			if (oNode)
			  {
				oNode.remove()
			  }
			window.close() 
		}
	</script>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:label id="lblTitulo" style="Z-INDEX: 103; LEFT: 16px; POSITION: absolute; TOP: 15px" runat="server"
				CssClass="titulo" Height="3px" Width="90%"> DAsignaci�n materiales de GS</asp:label>
			<TABLE id="Table1" style="Z-INDEX: 104; LEFT: 35px; POSITION: absolute; TOP: 210px" cellSpacing="1"
				cellPadding="1" width="90%" border="0">
				<TR>
					<TD align="center"><INPUT class="botonPMWEB" id="cmdAceptar" type="button" value="DAceptar" name="cmdAceptar" runat="server"></TD>
					<TD align="center"><INPUT class="botonPMWEB" id="cmdCancelar" onclick="window.close()" type="button" value="Cancelar"
							name="cmdCancelar" runat="server"></TD>
				</TR>
			</TABLE>
			<asp:label id="lblMaterial" style="Z-INDEX: 101; LEFT: 35px; POSITION: absolute; TOP: 55px"
				runat="server" CssClass="captionBlue" Height="3px" Width="90%">DMaterial</asp:label>
			<TABLE id="tblCabecera" style="Z-INDEX: 105; LEFT: 35px; POSITION: absolute; TOP: 90px; HEIGHT: 20px"
				cellSpacing="1" cellPadding="1" width="90%" border="0">
				<TR>
					<TD><asp:label id="lblProveedores" runat="server" CssClass="SinDatos" Height="25px" Width="100%">Label</asp:label></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblMensaje" runat="server" CssClass="parrafo" Height="25px" Width="100%">DSeleccione los materiales de GS relacionados</asp:label></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</html>