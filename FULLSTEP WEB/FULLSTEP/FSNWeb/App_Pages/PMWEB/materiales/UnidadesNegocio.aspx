<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Register TagPrefix="igtbl" Namespace="Infragistics.WebUI.UltraWebGrid" Assembly="Infragistics.WebUI.UltraWebGrid.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UnidadesNegocio.aspx.vb" Inherits="Fullstep.FSNWeb.UnidadesNegocio"%>
<%@ Register TagPrefix="ignav" Namespace="Infragistics.WebUI.UltraWebNavigator" Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="igcmbo" Namespace="Infragistics.WebUI.WebCombo" Assembly="Infragistics.WebUI.WebCombo.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="cc1" Namespace="Infragistics.WebUI.UltraWebGrid.ExcelExport" Assembly="Infragistics.WebUI.UltraWebGrid.ExcelExport.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>

	<script language="javascript">
		var arrNewUNQA = {} //Contiene el array con los nuevos nodos introducidos, ya que el DataKey = ID del UNQA no se recupera hasta el postBack de la pagina
							//y si se realiza operaciones con el nuevo nodo se necesita ese DataKey
			
		function ComprobarNodoBaja(treeId, nodeId){
		/*
			�************************************************************
			�*** Descripci�n: Cuando selecciona un nodo muestra el texto del boton baja logica (Deshacer baja logica) o (Dar de baja unidad de negocio) 
							   dependiendo si esta o no dado de baja               	
			�*** Par�metros de entrada: propios del evento    	                         	                   
			�*** Llamada desde: propia pagina  							                
			�*** Tiempo m�ximo: 0seg  							               
			�************************************************************
		*/
			var oNode = igtree_getNodeById(nodeId)		
			if (oNode) {
				tag = oNode.getTag();
				if (window.document.Form1.cmdBajaLogica) {
					if (tag == 'BAJA') {
						window.document.Form1.cmdBajaLogica.value = arrTextosML[2] //Deshacer baja logica
					}
					else {
						window.document.Form1.cmdBajaLogica.value = arrTextosML[1] // DDar de baja unidad de negocio
					}
				}
						
			}

		}
			
		function Realizar_Deshacer_bajaLogica() {
		/*
			�************************************************************
			�*** Descripci�n: function que da de baja logica a un nodo seleccionado en caso de que este dado de baja logica, 
							  o deshace la baja logica si estuviera dado ya de baja.           	
			�*** Par�metros de entrada: ninguno  	                         	                   
			�*** Llamada desde: propia pagina  							                
			�*** Tiempo m�ximo: 0,03seg  							               
			�************************************************************
		*/
		
			var oTree = igtree_getTreeById("uwtUnidades")
			var oNode=oTree.getSelectedNode()
			var a = false;
			accion = 0
			sCadenaIDs = ''
			nodoAnterior=null;

			if (oNode) {
				tag = oNode.getTag();
				
				idUNQA=oNode.getDataKey();
				if (idUNQA==null)
					idUNQA = arrNewUNQA[oNode.Id] //Obtener el ID del nodo del array ya que es un nuevo nodo

				sCadenaIDs=idUNQA + ","
				if (tag=='BAJA') {
						bMostrarMensaje = false
						oNodoPadre =  oNode.getParent()
						if (oNodoPadre) {
							tagPadre = oNodoPadre.getTag();
							
							if (tagPadre=='BAJA') {
								alert(arrTextosML[3])
								bMostrarMensaje = true
							}
						}
								
					if (!bMostrarMensaje) {
						//Si el nodo no tiene ningun nodo padre dado de baja, deshacer la baja
						deshacerBajaLogicaNodo(oNode,oNode.hasChildren())
						oNode.setTag("NOBAJA")
						window.document.Form1.cmdBajaLogica.value=arrTextosML[1]
						accion=1
					}
				}
				else {

					//Comprobacion si el nodo seleccionado tiene algun hijo no dado de baja
					bMostrarMensaje = false
					if (oNode.hasChildren()==true) {
						var onodeChild1=oNode.getFirstChild()
						while ((onodeChild1) && (bMostrarMensaje == false)) {
							tag1 = onodeChild1.getTag();
							if (tag1=='NOBAJA') 
								bMostrarMensaje = true;	
								
							if (onodeChild1.hasChildren()==true) {
								var onodeChild2=onodeChild1.getFirstChild()								
								while ((onodeChild2) && (bMostrarMensaje == false)) {
									tag2 = onodeChild2.getTag();
									if (tag2=='NOBAJA') 
										bMostrarMensaje = true;	
									onodeChild2 = onodeChild2.getNextSibling()
								}
							}
						
							onodeChild1 = onodeChild1.getNextSibling()
						}
					
					} // fin del if (oNode.hasChildren()==true)
					
					if (bMostrarMensaje==true)
						a = confirm(arrTextosML[4]) //"Si da de baja l�gica la unidad de negocio seleccionada, tambi�n se dar�n de baja l�gica las unidades de negocio que dependen de ella. �Desea continuar con la baja l�gica?")
					if ((bMostrarMensaje == false) || (bMostrarMensaje == true && a)) {	
					//***************************************************************
						if (a) {
								//Primero se dan de baja los hijos
								var onodeChild1=oNode.getFirstChild()
								while ((onodeChild1)) {
									tag1 = onodeChild1.getTag();
									idUNQA1=onodeChild1.getDataKey();
									if (idUNQA1==null)
										idUNQA1 = arrNewUNQA[onodeChild1.Id] //Obtener el ID del nodo del array ya que es un nuevo nodo

									if (nodoAnterior) {
										nodoAnterior.remove()
										nodoAnterior=null
									}
									if (tag1=='NOBAJA')  {
										var onodeChild2=onodeChild1.getFirstChild()
										while ((onodeChild2)) {
											tag2 = onodeChild2.getTag();
											idUNQA2=onodeChild2.getDataKey();
											if (idUNQA2==null)
												idUNQA2 = arrNewUNQA[onodeChild2.Id] //Obtener el ID del nodo del array ya que es un nuevo nodo

											if (nodoAnterior) {
												nodoAnterior.remove()
												nodoAnterior=null
											}
											if (tag2=='NOBAJA')  {
												if (window.document.Form1.chkUnidadesBajaValor.value == "1") {
													darDeBajaNodo(onodeChild2)
													onodeChild2.setTag("BAJA")
												}else {
													nodoAnterior = onodeChild2
												}
												sCadenaIDs=sCadenaIDs + idUNQA2 + ","
											}
											onodeChild2 = onodeChild2.getNextSibling()
										}//fin del while onodeChild2
										if (nodoAnterior) {
												nodoAnterior.remove()
												nodoAnterior=null
											}	
										
										if (window.document.Form1.chkUnidadesBajaValor.value == "1") {
											darDeBajaNodo(onodeChild1)
											onodeChild1.setTag("BAJA")
											
										}else {
											nodoAnterior = onodeChild1
										}
											
										sCadenaIDs=sCadenaIDs + idUNQA1 + ","
									} // fin del if (tag1=='NOBAJA')
									
									onodeChild1 = onodeChild1.getNextSibling()
									
								} //fin del while 	onodeChild1						
								if (nodoAnterior) {
									nodoAnterior.remove()
									nodoAnterior=null
								}
							
						} //fin del (a) --> Si tiene hijos el nodo principal
						

						if (window.document.Form1.chkUnidadesBajaValor.value == "1") {
							darDeBajaNodo(oNode,bMostrarMensaje)
							oNode.setTag("BAJA")
						}else 
							oNode.remove()						
						
						window.document.Form1.cmdBajaLogica.value=arrTextosML[2]//"DDeshacer baja de unidad de negocio"
						accion=2
					} // fin if ((bMostrarMensaje == false) || (bMostrarMensaje == true && a)) {	
				} //fin del else
					
								
				if ((accion == 1) || (accion == 2)) {
					//Accion = 1 -->Deshacer baja logica	   // Accion = 2 --> Realizar baja logica
					idUNQAs = sCadenaIDs.substring(0,sCadenaIDs.length-1)    //quitar la ultima ","
					window.open("RealizarOperacionesOcultas.aspx?Accion="+accion+"&idUNQAs="+idUNQAs,"iframeWSServer")	
				}
			} // fin del if del oNode		
		} //fin del function

		/*
		''' <summary>
		''' function que da de baja al nodo seleccionado se a�ade la imagen en javascript que indica que esta dado de baja 	
		''' </summary>
		''' <param name="nodo">El nodo que hay que dar de baja logica </param>
		''' <remarks>Llamada desde: propia pagina; Tiempo m�ximo: 0seg.</remarks>*/				
		function darDeBajaNodo(nodo) {			
			if (nodo.element) {
				//A�adir la imagen 'xroja.gif
				imagen = "<IMG src=\'images/xroja.gif\' align=absMiddle igimg='1'>"
				aux = nodo.element.innerHTML.indexOf("<INPUT")
				inicio = nodo.element.innerHTML.indexOf(">", aux)
				inicio = inicio + 1 //1 = cadena >
				nueva_cadena = nodo.element.innerHTML.substring(0, inicio) + imagen
				nueva_cadena = nueva_cadena + nodo.element.innerHTML.substring(inicio, nodo.element.innerHTML.length)
				nodo.element.innerHTML = nueva_cadena
			}
		}

		/*
		''' <summary>
		''' function que deshace la baja logica	
		''' </summary>
		''' <param name="nodo">El nodo que hay que dar de baja logica</param>
		''' <param name="tieneHijos">Si el nodo tiene o no hijos.</param>
		''' <remarks>Llamada desde: propia pagina; Tiempo m�ximo: 0seg.</remarks>*/						
		function deshacerBajaLogicaNodo(nodo, tieneHijos) {
			if (nodo.element) {
				//Buscar la imagen 'xroja.gif para quitarla	//Si tiene hijos hay q buscar la segunda imagen, la primera es (+) de los hijos
				inicio = nodo.element.innerHTML.indexOf("<IMG")	        
				if (tieneHijos) 
					inicio = nodo.element.innerHTML.indexOf("<IMG", inicio + 1)
								
				fin = nodo.element.innerHTML.indexOf(">",inicio)
				nueva_cadena = nodo.element.innerHTML.substring(0, inicio)		
				nueva_cadena = nueva_cadena + nodo.element.innerHTML.substring(fin + 1, nodo.element.innerHTML.length)			
				nodo.element.innerHTML = nueva_cadena
			}
        }

        function devolverFocoAlta() {
	        alta.focus()
        }
			
	    /*
		�************************************************************
		�*** Descripci�n: function actualiza la denominaci�n del nodo (accion = 2) o a�ade un nuevo nodo (accion = 1)
		�*** Par�metros de entrada: accion-->(1,2) Indica si se a�ade un nuevo nodo o se modifica el nombre
									id_padre--> id del nodo padre
									id_abuelo--> id del nodo abuelo
									id_nodo_nuevo--> id nuevo a a�adir			                        	                   
		�*** Llamada desde: altaUnidadNeg.aspx  							                
		�*** Tiempo m�ximo: 0,01seg  							               
		�************************************************************
		*/
	    function actualizarDenArbol(accion, ID_PADRE,ID_ABUELO,texto, ID_NODO_NUEVO) {			    
		    var oTree = igtree_getTreeById("uwtUnidades")
		    var oNode=oTree.getSelectedNode()
		    if (oNode) 
		    {
			    if (accion ==1) { //Alta nuevo nodo										
				    oNodoAnyadido = oNode.addChild(texto)
				    if (oNodoAnyadido)
					    arrNewUNQA[oNodoAnyadido.Id] = ID_NODO_NUEVO

			    } else {	
				    oNode.setText(texto)						 
			    }
		    }
		    else
			    if (accion ==1) {			
				    newNode = oTree.addRoot(texto)
				    if (newNode) {
					    arrNewUNQA[newNode.Id] = ID_NODO_NUEVO
					    newNode.setTag("NOBAJA")
				    }
			    }
		    window.focus()		
	    }	
		
	    /*
		�************************************************************
		�*** Descripci�n: function Elimina el nodo seleccionado
			
		�*** Par�metros de entrada: ninguno
														   
		�*** Llamada desde:  propia pagina							                
		�*** Tiempo m�ximo: 0,328sg  							               
		�************************************************************
	    */
	    function eliminarUnidad()
	    {		
		    var oTree = igtree_getTreeById("uwtUnidades")
		    var oNode=oTree.getSelectedNode()
			
		    if (oNode)
		    {
			    if (oNode.hasChildren()==true)
				    alert(arrTextosML[5])
			    else {
				    sId = oNode.getDataKey()
				    if (sId==null)
					    sId = arrNewUNQA[oNode.Id] //Obtener el ID del nodo del array ya que es un nuevo nodo
		            var newWindow = window.open("eliminarUnidad.aspx?Unidad=" + sId + "&DenUnidad=" + oNode.getText(), "_blank", "width=400,height=280,status=yes,resizable=no,top=200,left=250");					
			    }
		    }
		    else
			    alert(arrTextosML[0])
				
		    window.focus();
	    }

        //<summary>Abre la p�gina de asignaci�n de unidades organizativas a UNQAs</summary>
	    function AsignarUnidadesOrganizativas() {
	        var oTree = igtree_getTreeById("uwtUnidades");
	        var oNode = oTree.getSelectedNode();
	        if (oNode) {
	            sId = oNode.getDataKey();
	            if (sId == null) sId = arrNewUNQA[oNode.Id]; //Obtener el ID del nodo del array ya que es un nuevo nodo

	            var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>QA/Configuracion/asignarUnidadesOrganizativas.aspx?IdUNQA=" + sId + "&DenUNQA=" + oNode.getText(), "_blank", "width=520,height=470,status=yes,resizable=no,top=200,left=250");
	        }
	        else {
	            alert(arrTextosML[0]);
	        }
	    }

	    /*''' <summary>
	    ''' evento que se ejecuta al dar al boton de A�adir nueva Unidad de negocio o Modificar Unidad de negocio: A�ade o modifica una unidad de negocio.	
	    ''' </summary>
	    ''' <param name="accion">//accion = 1 --> Alta   ///accion = 2 -->Modificacion	</param>
	    ''' <returns>Explicaci�n retorno de la funci�n</returns>
	    ''' <remarks>Llamada desde:  cmdA�adir  cmdModif	; Tiempo m�ximo: 0,4  seg</remarks>*/				
	    function MantenimientoNodo(accion) {
		    idiomas = document.getElementById("cont").value;
		    he = 180 + (20 * idiomas);        
					
		    den_nodo="";id_nodo="";
		    den_padre = ""; id_padre = "";
		    den_abuelo = ""; id_abuelo = "";
		
		    var oTree = igtree_getTreeById("uwtUnidades")
		    var oNode=oTree.getSelectedNode()
					
		    if (oNode) {
			    nivel = oNode.getLevel()					  
			    if ((nivel==0) || (nivel==1) || (nivel==2)) {
				    den_nodo = oNode.getText();
				    id_nodo = oNode.getDataKey();
				    if (id_nodo==null)
					    id_nodo = arrNewUNQA[oNode.Id] //Obtener el ID del nodo del array ya que es un nuevo nodo
				
				    oPadre = oNode.getParent();
				    if (oPadre)  {
					    id_padre = oPadre.getDataKey();
					    if (id_padre==null)
						    id_padre = arrNewUNQA[oPadre.Id]

					    den_padre = oPadre.getText();

					    oAbuelo = oPadre.getParent();
					    if (oAbuelo) {
						    id_abuelo = oAbuelo.getDataKey();
						    if (id_abuelo == null)
							    id_abuelo = arrNewUNQA[oAbuelo.Id]

						    den_abuelo = oAbuelo.getText();
					    }					
				    }
			    }
			    if (((accion == 1) && ((nivel == 0) || (nivel == 1))) || (accion == 2)) {
			        var alta = window.open("altaUnidadNeg.aspx?ID_NODO=" + id_nodo + "&DEN_NODO=" + den_nodo + "&ID_PADRE=" + id_padre + "&DEN_PADRE=" + den_padre + "&Accion=" + accion + "&grupo=" + window.document.Form1.grupo.value + "&ID_ABUELO=" + id_abuelo + "&DEN_ABUELO=" + den_abuelo, "_blank", "width=500,height=" + he + ",status=yes,resizable=no,top=100,left=250")
			        alta.focus();
			    }
            } else {
                if (accion == 2)
                    alert(arrTextosML[0])
                else {
                    alta = window.open("altaUnidadNeg.aspx?ID_NODO=" + id_nodo + "&DEN_NODO=" + den_nodo + "&ID_PADRE=" + id_padre + "&DEN_PADRE=" + den_padre + "&Accion=" + accion + "&grupo=" + window.document.Form1.grupo.value + "&ID_ABUELO=" + id_abuelo + "&DEN_ABUELO=" + den_abuelo, "_blank", "width=400,height=" + he + ",status=yes,resizable=no,top=100,left=250")
                    alta.focus();
                }
		    } //fin del if nodo							
	    } //fin function
			
			
	    /*
		�************************************************************
		�*** Descripci�n: evento que se ejecuta al dar al clickar en las imagenes de a�adir/Eliminar elementos en la lista
							cuando es insertar un nuevo elemento, recorre la lista para mirar si ya se habia a�adido o no anteriormente el elemento para no volver a a�adirlo		
							
		�*** Par�metros de entrada: //accion = 1 --> Alta   ///accion = 2 -->Eliminar	elemento de la lista
									listaID--> Identificador de la lista donde se a�ade/elimina
									indice--> indice de la fila donde se esta modificando
									
		�*** Par�metros de salida:	false--> Devolver el control 								                        	                
		�*** Llamada desde:  propia pagina							                
		�*** Tiempo m�ximo: 0,45seg  							               
		�************************************************************
	    */
	    function ActualizarLista(accion , listaID, indice) {	    
		    arrCheckeados=window.document.Form1.cadenaUNQACheckeados.value.split(";")
		
		    window.document.Form1.datosModificados.value = "1"
		    arrUnidadesNegocio = window.document.Form1.cadenaUNQA.value.split(";")
		    arrPermisosUnidadesNegocio = window.document.Form1.cadenaPermisosUNQA.value.split("#")
		    oLista = document.getElementById(listaID)

		    if(accion=='1'){
			    //Buscamos ahora si el usuario tiene permiso de asignar proveedores en la UN seleccionada
			    for (i=0;i<arrCheckeados.length-1;i++){
				    permiso=false;
				    sIdChecked=arrCheckeados[i].split("#")[0]
				    sValor = arrCheckeados[i].split("#")[1] //Separar el codigo de la denominacion de la unidad de negocio
				
				    //Comprobamos si tiene permiso para asignar proveedores en el nodo checkeado
				    for(j=0;j<arrPermisosUnidadesNegocio.length-1;j++){
					    if(arrPermisosUnidadesNegocio[j]==sIdChecked){
						    permiso=true;    
						    break;
					    }
				    }
				    if(!permiso){
					    alert(arrTextosML[6] + arrCheckeados[i].split("#")[1]);
					    return false;
				    }else{
					    if (oLista) {
						    encontrado = false;	
						    for (k =0;k <oLista.options.length; k++) {	//Busca si ya esta en la lista					
							    if (oLista.options[k].value == sIdChecked) {
								    encontrado = true;
								    break;	
							    }
						    }
						    if (encontrado){
							    alert(arrTextosML[7] + "(" + arrCheckeados[i].split("#")[1] + ")");
							    return false;
						    }else{
							    var oTree = igtree_getTreeById("uwtUnidades")
							    var node=igtree_getNodeById(arrCheckeados[i].split("#")[2])   
							
							    if (node.getParent()){
								    sIdPadre = node.getParent().getDataKey();
								    if (sIdPadre==null)
									    sIdPadre = arrNewUNQA[node.getParent().Id] //Obtener el ID del nodo del array ya que es un nuevo nodo
								
								    for (l =0;l <oLista.options.length; l++) {	//Busca si ya esta en la lista					
									    if (oLista.options[l].value == sIdPadre) {
										    encontrado = true;
										    break;	
									    }
								    }
								    if (encontrado){
									    alert("'" + arrCheckeados[i].split("#")[1] + "'" + arrTextosML[8] + "'" + node.getParent().Element.innerText + "'");
									    return false;
								    }
								    if(node.getParent().getParent()){
									    sIdAbuerlo = node.getParent().getParent().getDataKey();
									    if (sIdAbuerlo==null)
										    sIdAbuerlo = arrNewUNQA[node.getParent().getParent().Id] //Obtener el ID del nodo del array ya que es un nuevo nodo    
								
									    for (m =0;m <oLista.options.length; m++) {	//Busca si ya esta en la lista					
										    if (oLista.options[m].value == sIdAbuerlo) {
											    encontrado = true;
											    break;	
										    }
									    }
									    if(encontrado){
										    alert("'" + arrCheckeados[i].split("#")[1] + "'" + arrTextosML[8] + "'" + node.getParent().getParent().Element.innerText + "'");
										    return false;
									    }
								    }
							    }
							
							    arrHijos = null;
							    arrHijos = node.getChildNodes()
							
							    if (arrHijos.length>0){
								    for (r in arrHijos) {
									    sIdHijo = arrHijos[r].getDataKey();
									    if (sIdHijo==null)
										    sIdHijo = arrNewUNQA[arrHijos[r].Id]
									
									    for (p =0;p <oLista.options.length; p++) {	//Busca si ya esta en la lista					
										    if (oLista.options[p].value == sIdHijo) {
											    encontrado = true;
											    break;	
										    }
									    }

									    if (encontrado) {
										    alert("'" + arrCheckeados[i].split("#")[1] + "'" + arrTextosML[8] + "'" + arrHijos[r].Element.innerText + "'");
										    return false;
									    } else {
										    arrNietos = null;
										    arrNietos = arrHijos[r].getChildNodes();

										    if (arrNietos.length > 0) {
											    for (q in arrNietos) {
												    sIdNieto = arrNietos[q].getDataKey();
												    if (sIdNieto == null)
													    sIdNieto = arrNewUNQA[arrNietos[q].Id]

												    for (s = 0; s < oLista.options.length; s++) {	//Busca si ya esta en la lista					
													    if (oLista.options[s].value == sIdNieto) {
														    encontrado = true;
														    break;
													    }
												    }
												    if (encontrado) {
													    alert("'" + arrCheckeados[i].split("#")[1] + "'" + arrTextosML[8] + "'" + arrNietos[q].Element.innerText + "'");
													    return false;
												    }
											    }
										    }
									    }
								    }
							    }
							
							    oLista.options[oLista.options.length]=new Option(sValor,sIdChecked)
							    oHidden = document.getElementById("hidUnidadesNegocio_UNQAS_" +indice);
							    if (oHidden) {
								    oHidden.value = oHidden.value + sIdChecked + ";"
							    }   
						    }
					    }
				    }
			    }
		    }else 
			    //Eliminar elemento de la lista
			    for (i = oLista.options.length-1;i >=0; i--) {
				    if (oLista.options[i].selected == true) { 
					    oHidden = document.getElementById("hidUnidadesNegocio_UNQAS_" +indice);
					
					    permiso=false;
					
					    //Comprobamos si tiene permiso para asignar proveedores en el nodo checkeado
					    for(j=0;j<arrPermisosUnidadesNegocio.length-1;j++){
						    if (arrPermisosUnidadesNegocio[j] == oHidden.value.split(";")[i]) {
							    permiso=true;    
							    break;
						    }
					    }
					
					    if(!permiso){
						    alert(arrTextosML[6] + arrCheckeados[i].split("#")[1]);
						    return false;
					    }else{
						    oHidden.value = oHidden.value.replace(oLista.options[i].value + ";",'')
						    oLista.options[i]=null;
					    }			        	
				    }
			    }
		    return false;
	    }//Fin function

	    /*
		�************************************************************
		�*** Descripci�n: evento que se ejecuta al dar al clickar un nodo
						    comprueba si hay un nodo hijo seleccionado para no seleccionarlo.
							
		�*** Par�metros de entrada: propias del evento									
		�*** Par�metros de salida:	ninguno								                        	                
		�*** Llamada desde:  propia pagina							                
		�*** Tiempo m�ximo: 0,15seg.  							               
		�************************************************************
	    */
	    function NodeChecked_AnyadirUnidadNegocio(treeId, nodeId, bChecked) {		   
		    var oTree = igtree_getTreeById("uwtUnidades")
		    var node=igtree_getNodeById(nodeId)			
		    oEvent = oTree.Events.NodeChecked[0]
		    oTree.Events.NodeChecked[0]=""
		    if (node.getTag()=='BAJA') {					
				    node.setChecked(false)
		    }else
			    gestionArray(node,bChecked)
		
		    oTree.Events.NodeChecked[0] = oEvent;		
	    }
	    /*
		�************************************************************
		�*** Descripci�n: comprueba si hay un nodo hijo seleccionado para no seleccionarlo.
							
		�*** Par�metros de entrada: oNodo --> Nodo seleccionado
									bSeleccionado = Si el nodo esta o no selecionado									
		�*** Par�metros de salida:	ninguno								                        	                
		�*** Llamada desde:  propia pagina							                
		�*** Tiempo m�ximo: 0,15seg.  							               
		�************************************************************
	    */
	    function gestionArray (oNodo, bSeleccionado) {	    	
		    oNodoPadre = null;oNodoAbuelo=null;

		    sId = oNodo.getDataKey();
		    if (sId==null)
			    sId = arrNewUNQA[oNodo.Id] //Obtener el ID del nodo del array ya que es un nuevo nodo
		    sCadena = window.document.Form1.cadenaUNQA.value
		    sCheckeados = window.document.Form1.cadenaUNQACheckeados.value
	
		    if(!bSeleccionado) { //Si estaba en el array, lo sacamos					
			    sCadenaBuscar = '/' + sId + '/&' + oNodo.getText() + ';'
			    sCadena = sCadena.replace(sCadenaBuscar, '')
			
			    sCheckeado = sId + '#' + oNodo.getText() + '#' + oNodo.Id + ';'
			    sCheckeados = sCheckeados.replace(sCheckeado,'')
			
			    //Quitamos tambien sus hijos y nietos
			    arrHijos = null;
			    arrHijos = oNodo.getChildNodes()
			
			    for (j in arrHijos) {
				    sIdHijo = arrHijos[j].getDataKey();
				    if (sIdHijo==null)
					    sIdHijo = arrNewUNQA[arrHijos[j].Id]
				
				    sCadenaBuscar = '/' + sIdHijo + '/&' + arrHijos[j].getText() + ';'
				    sCadena = sCadena.replace(sCadenaBuscar, '')
				
				    arrNietos = null;
				    arrNietos = arrHijos[j].getChildNodes();
				
				    for(k in arrNietos){
					    sIdNieto=arrNietos[k].getDataKey();
					    if(sIdNieto==null)
						    sIdNieto=arrNewUNQA[arrNietos[k].Id]
					
					    sCadenaBuscar = '/' + sIdNieto + '/&' + arrNietos[k].getText(); + ';'
					    sCadena = sCadena.replace(sCadenaBuscar, '')
				    }					    				
			    }
			    //Si no tiene hermanos ni sobrinos seleccionados quito tambien al padre
			
			    if (oNodo.getParent()){
				    arrHermanos=null;
				    arrHermanos=oNodo.getParent().getChildNodes();
			
				    HermanosSobrinos=false;
				
				    for (j in arrHermanos) {
					    if(arrHermanos[j].getChecked()){
						    HermanosSobrinos=true;    
					    }			    	    
					
					    arrSobrinos = null;
					    arrSobrinos = arrHermanos[j].getChildNodes();
					
					    for(k in arrSobrinos){
						    if(arrSobrinos[k].getChecked()){
							    HermanosSobrinos=true;    
						    }
					    }					    				
				    }
				    if(!HermanosSobrinos){
					    sIdPadre=oNodo.getParent().getDataKey();
					    if(sIdPadre==null)
						    sIdPadre=arrNewUNQA[oNodo.getParent().Id]
					
					    sCadenaBuscar = '/' + sIdPadre + '/&' + oNodo.getParent().getText() + ';'
					    sCadena = sCadena.replace(sCadenaBuscar, '')
				    }
				    if(oNodo.getParent().getParent()){
					    //Si no tiene tios ni primos seleccionados quito tambien al abuelo
					    arrTios=null;
					    arrTios=oNodo.getParent().getParent().getChildNodes();
					
					    TiosPrimos=false;
				
					    for (j in arrTios) {
						    if(arrTios[j].getChecked()){
							    TiosPrimos=true;    
						    }			    	    
						
						    arrPrimos = null;
						    arrPrimos = arrTios[j].getChildNodes();
						
						    for(k in arrPrimos){
							    if(arrPrimos[k].getChecked()){
								    TiosPrimos=true;    
							    }
						    }					    				
					    }
					    if(!TiosPrimos){
						    sIdAbuelo=oNodo.getParent().getParent().getDataKey();
						    if(sIdAbuelo==null)
							    sIdAbuelo=arrNewUNQA[oNodo.getParent().getParent().Id]
						
						    sCadenaBuscar = '/' + sIdAbuelo + '/&' + oNodo.getParent().getParent().getText() + ';'
						    sCadena = sCadena.replace(sCadenaBuscar, '')
					    }
				    }
			    } 
		    }else { //Si no estaba en el array, lo metemos, a no ser que est� ya seleccionado el padre o abuelo del nodo
			    sCheckeado = sId + '#' + oNodo.getText() + '#' + oNodo.Id + ';'
			    sCheckeados = sCheckeados + sCheckeado
			
			    oNodoPadre = oNodo.getParent();	        
			    if (oNodoPadre){
				    if (!oNodoPadre.getChecked()){   
					    oNodoAbuelo = oNodoPadre.getParent();
					    if (oNodoAbuelo){//Si tiene abuelo esta en el 2� nivel, si no, esta en el 1er nivel, ya que si tiene padre
						    if (!oNodoAbuelo.getChecked()){
							    //Ha checkeado un nieto sin padre ni abuelo seleccionado.
							    //A�ado, abuelo, padre y nieto.
							    //A�adimos al padre
							    sIdPadre=oNodoPadre.getDataKey();
							    if(sIdPadre==null)
								    sIdPadre=arrNewUNQA[oNodoPadre.Id]
							
							    sCadenaBuscar = '/' + sIdPadre + '/&' + oNodoPadre.getText() + ';'
							    sCadena = sCadena.replace(sCadenaBuscar, '')
							
							    sCadena = sCadena + '/' + sIdPadre + '/&'+ oNodoPadre.getText() + ';'
							
							    //A�adimos al abuelo
							    sIdAbuelo=oNodoAbuelo.getDataKey();
							    if(sIdAbuelo==null)
								    sIdAbuelo=arrNewUNQA[oNodoAbuelo.Id]
							
							    sCadenaBuscar = '/' + sIdAbuelo + '/&' + oNodoAbuelo.getText() + ';'
							    sCadena = sCadena.replace(sCadenaBuscar, '')

							    sCadena = sIdAbuelo + '/&' + oNodoAbuelo.getText() + ';' + '/' + sCadena 
							
							    //A�ado al checkeado
							    sCadena = sCadena + '/' + sId + '/&'+ oNodo.getText() + ';'
						    }
					    }
					    else {
						    //Ha seleccionado un padre sin abuelo seleccionado.
						    //A�ado al padre y todos sus hijos
						    //Comprobacion del 2� Nivel
						    arrHijos = null;
						    arrHijos = oNodo.getChildNodes()
						
						    //A�ado a el mismo
						    sCadena = sCadena + '/' + sId + '/&'+ oNodo.getText() + ';'
						
						    //A�ado los hijos
						    for (j in arrHijos) {
							    sIdHijo=arrHijos[j].getDataKey();
							    if(sIdHijo==null)
								    sIdHijo=arrNewUNQA[arrHijos[j].Id]
							
							    sCadena=sCadena.replace('/' + sIdHijo + '/&'+ arrHijos[j].getText() + ';','')
							    sCadena = sCadena + '/' + sIdHijo + '/&'+ arrHijos[j].getText() + ';'
						    }
					    }
				    }
			    }else {
				    //Ha seleccionado un abuelo.
				    //A�ado abuelo y todos los padres e hijos.
				    arrHijos = oNodo.getChildNodes()
								
				    //Debo a�adir todos sus hijos y nietos
				    //Hijos
				    for(j in arrHijos){
					    sIdHijo=arrHijos[j].getDataKey();
					    if(sIdHijo==null)
						    sIdHijo=arrNewUNQA[arrHijos[j].Id]
					
					    sCadena = sCadena.replace( '/' + sIdHijo + '/&'+ arrHijos[j].getText() + ';','')
					    sCadena = sCadena + '/' + sIdHijo + '/&'+ arrHijos[j].getText() + ';'   
					
					    arrNietos = arrHijos[j].getChildNodes()
					    for (k in arrNietos) {
						    sIdNieto=arrNietos[k].getDataKey();
						    if(sIdNieto==null)
							    sIdNieto=arrNewUNQA[arrNietos[k].Id]
						
						    sCadena = sCadena.replace( '/' + sIdNieto + '/&'+ arrNietos[k].getText() + ';'	,'')
						    sCadena = sCadena + '/' + sIdNieto + '/&'+ arrNietos[k].getText() + ';'	
					    }
				    }
				    sCadena = sCadena.replace( '/' + sId + '/&'+ oNodo.getText() + ';','')
				    sCadena = sCadena + '/' + sId + '/&'+ oNodo.getText() + ';'  				
			    }
		    }
		    window.document.Form1.cadenaUNQA.value = sCadena;
		    window.document.Form1.cadenaUNQACheckeados.value = sCheckeados;
	    }	
	    /*
		�************************************************************
		�*** Descripci�n:	Recorre los elementos del arbol de unidades de negocio buscando el elemento  valorEnNodo. 
							Una vez encontrado comprueba si alguno de sus elementos est� a�adido ya en la lista para no a�adirlo.
		
		�*** Par�metros de entrada: valorEnNodo --> Valor de la lista que hay que comparar
									valorABuscar--> id de la Unidad de negocio que hemos seleccionado en el arbol para a�adir									
		�*** Par�metros de salida:	true/false --> Si ha encontrado o no el elemento en la lista								                        	                
		�*** Llamada desde:  propia pagina							                
		�*** Tiempo m�ximo:  0,45seg   							               
		�************************************************************
	    */
	    function BuscarValor (valorEnNodo, valorABuscar) {		   	
		    oTree = igtree_getTreeById("uwtUnidades")
		    arrHijos = oTree.getNodes()
		    oNodo = null;
		
		    for (l in arrHijos) {
			    if (arrHijos[l].getDataKey()==valorEnNodo)
				    {
				    oNodo = arrHijos[l]
				    break;
				    }
		    }
		    encontradoF = false;
		    if (oNodo) {
			    if (oNodo.getDataKey() == valorABuscar) {
				    encontradoF = true
			
			    }else {
				    arrHijos = oNodo.getChildNodes()
				    for (l in arrHijos) {

					    if (arrHijos[l].getDataKey()==valorABuscar) {
						    encontradoF = true;
						    break;
					    }else {
						    arrNietos = arrHijos[l].getChildNodes()
						    for (j in arrNietos) {
							    if (arrNietos[j].getDataKey() == valorABuscar) {
								    encontradoF = true;
								    break;
							    }
						    }//fin del for j Nietos

					    } //fin del else
				    } //fin del for l
			    }//fin del else
		    } //fin del oNodo
		
	        return encontradoF;				
	    } //fin del function 
	
	    /*
		�************************************************************
		�*** Descripci�n: function que visualiza la grid de proveedores		
		�*** Par�metros de entrada: ninguno			                        	                   
		�*** Llamada desde:  propia pagina							                
		�*** Tiempo m�ximo: 0seg  							               
		�************************************************************
	    */
	    function VisualizarGrid() {	        
		    div = document.getElementById("div_proveedores")
		    if (div)
			    div.style.visibility='visible'
	    }

	    /*
	    ''' <summary>
	    ''' function que oculta/visualiza la tabla de exportar
	    ''' </summary>
	    ''' <param name="event">evento de pantalla</param>
	    ''' <remarks>Llamada desde: cmdImprimir/onclick ; Tiempo m�ximo: 0</remarks>		
	    */	
	    function Imprimir(event)
	    {
		    if (document.getElementById("tblImprimir").style.visibility!='hidden')
			    {
			    document.getElementById("tblImprimir").style.visibility='hidden'
			    }

		    var mx
		    var my
		
		    mx = event.clientX - 30
		    my = event.clientY;  

		    document.getElementById("tblImprimir").style.visibility='visible'
		    document.getElementById("tblImprimir").style.top = my + document.body.scrollTop + "px";
		    document.getElementById("tblImprimir").style.left = mx + "px"
	    }
	    /*
	    ''' <summary>
	    ''' Cierra la tabla con los botones para imprimir
	    ''' </summary>
	    ''' <remarks>Llamada desde: cmdImprimir/onmouseover ; Tiempo m�ximo: 0</remarks>*/	
	    function CerrarPopUp()
	    {
		    document.getElementById("tblImprimir").style.visibility='hidden'
	    }		
	    /*
	    �************************************************************
	    �*** Descripci�n: function que escribe la denominacion del boton Imprimir/exportar			
	    �*** Par�metros de entrada: ninguno			                        	                   
	    �*** Llamada desde:  propia pagina							                
	    �*** Tiempo m�ximo: 0seg  							               
	    �************************************************************
	    */
	    function Init(){	    	
		    window.document.Form1.cmdImprimir.value=window.document.Form1.textoImprimir.value

        }						
	</script>	
	<body onload="Init()">
		<form id="Form1" method="post" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <CompositeScript>
                <Scripts>
                    <asp:ScriptReference Path="../alta/js/AdjacentHTML.js" />
                    <asp:ScriptReference Path="../materiales/js/jsAltaMat.js" />
                </Scripts>
            </CompositeScript>
        </asp:ScriptManager>
			<uc1:menu id="Menu1" runat="server" OpcionMenu="Configuracion" OpcionSubMenu="UnidadesNegocio"></uc1:menu>
			<input type="hidden" name="textoImprimir" id="textoImprimir" runat="server">

			<igtbl:ultrawebgrid id="uwgProveedoresAux"
				runat="server" Visible="False" Height="42px" Width="254px">
				<DisplayLayout AllowSortingDefault="Yes" RowHeightDefault="20px" Version="4.00" HeaderClickActionDefault="SortSingle"
					BorderCollapseDefault="Separate" Name="Ultrawebgrid1" CellClickActionDefault="RowSelect" AllowUpdateDefault="Yes">
					<AddNewBox>
						<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
						</Style>
					</AddNewBox>
					<Pager PageSize="4" StyleMode="PrevNext" AllowPaging="True">
						<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
						</Style>
					</Pager>
					<RowSelectorStyleDefault BorderWidth="0px" BorderStyle="Solid"></RowSelectorStyleDefault>
					<FrameStyle Width="254px" BorderWidth="0px" Font-Size="8pt" Font-Names="Verdana" BorderStyle="Solid"
						Height="42px" CssClass="visorFrame"></FrameStyle>
					<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
						<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
					</FooterStyleDefault>
					<SelectedHeaderStyleDefault BorderStyle="Solid"></SelectedHeaderStyleDefault>
					<EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
					<SelectedRowStyleDefault BorderWidth="0px" BorderStyle="Solid"></SelectedRowStyleDefault>
					<RowStyleDefault BorderWidth="1px" BorderColor="LightGray" BorderStyle="Solid"></RowStyleDefault>
				</DisplayLayout>
				<Bands>
					<igtbl:UltraGridBand FixedHeaderIndicator="None" CellClickAction="RowSelect" RowSelectors="No">
						<HeaderStyle CssClass="Cabecera Texto12"></HeaderStyle>
						<RowAlternateStyle CssClass="ugfilaalttabla"></RowAlternateStyle>
						<RowTemplateStyle BorderColor="White" BorderStyle="Ridge" BackColor="White">
							<BorderDetails WidthLeft="3px" WidthTop="3px" WidthRight="3px" WidthBottom="3px"></BorderDetails>
						</RowTemplateStyle>
						<RowStyle CssClass="ugfilatabla"></RowStyle>
					</igtbl:UltraGridBand>
				</Bands>
			</igtbl:ultrawebgrid>
			<cc1:ultrawebgridexcelexporter id="UltraWebGridExcelExporter1" runat="server"></cc1:ultrawebgridexcelexporter>
			
			<input id="chkUnidadesBajaValor" type="hidden" value="0" name="chkUnidadesBajaValor" runat="server">
			<input id="grupo" type="hidden" name="grupo" runat="server"> 
			<input id="cadenaUNQA" type="hidden" name="cadenaUNQA" runat="server">
			<input id="cadenaPermisosUNQA" type="hidden" name="cadenaPermisosUNQA" runat="server">
			<input id="cadenaUNQAAnt" type="hidden" name="cadenaUNQAAnt" runat="server"> 
			<input id="buscar" type="hidden" name="buscar" runat="server">
			<input id="cadenaUNQACheckeados" type="hidden" name="cadenaUNQACheckeados" runat="server">
			<input id="datosModificados" type="hidden" name="datosModificados" runat="server">
			<input id="proveedoresCount" type="hidden" name="proveedoresCount" runat="server">
			<input id="nodoAnterior" type="hidden" value="0" name="nodoAnterior" runat="server">
			<input id="sort" name="sort" runat="server" type="hidden" />

			<table style="margin-left: 16px;">
				<tr>
					<td>
						<img alt="" src="images/filtro_ud_negocio.gif" style="width: 54px; height: 54px" />
					</td>
					<td>
						<asp:label id="lblTitulo"  runat="server" CssClass="titulo" Height="3px" Width="100%">Materiales de QA</asp:label>
					</td>
				</tr>
			</table>
			
			<div style="clear:both; float:left; margin-left:16px;">
				<ignav:ultrawebtree id="uwtUnidades" runat="server" CssClass="igTree" Height="223px" Width="400px" CheckBoxes="True" 
                    WebTreeTarget="HierarchicalTree" ExpandImage="ig_treePlus.gif" Indentation="20" ImageDirectory="../images/"
					Cursor="Default" CollapseImage="ig_treeMinus.gif">
					<SelectedNodeStyle ForeColor="White" BackColor="Navy"></SelectedNodeStyle>
					<Levels>
						<ignav:Level LevelHoverClass="" LevelHiliteClass="" ColumnName="" TargetFrameName="" ImageColumnName=""
							LevelClass="" LevelImage="" Index="0" LevelKeyField="" RelationName="" CheckboxColumnName=""
							LevelIslandClass="" TargetUrlName=""></ignav:Level>
						<ignav:Level LevelHoverClass="" LevelHiliteClass="" ColumnName="" TargetFrameName="" ImageColumnName=""
							LevelClass="" LevelImage="" Index="1" LevelKeyField="" RelationName="" CheckboxColumnName=""
							LevelIslandClass="" TargetUrlName=""></ignav:Level>
					</Levels>
					<ClientSideEvents NodeChecked="NodeChecked_AnyadirUnidadNegocio" NodeClick="ComprobarNodoBaja"></ClientSideEvents>
				</ignav:ultrawebtree> 
			</div>  

			<div style="float:left;">
			<table>
			    <tr><td>
				    <input language="javascript" class="botonPMWEB" id="cmdAnyaNeg" style=" WIDTH: 205px; HEIGHT: 24px"
					    onclick="MantenimientoNodo(1)" type="button" value="DA�adir material nuevo" name="cmdAnyaMat" runat="server"/>
			    </td></tr>
			    <tr><td>
				    <input language="javascript" class="botonPMWEB" id="cmdModifNeg" style=" WIDTH: 205px; HEIGHT: 24px"
					    onclick="MantenimientoNodo(2)" type="button" value="DModificar nombre" name="cmdModifNombre"
					    runat="server"/> 
			    </td></tr>
			    <tr><td>
				    <input language="javascript" class="botonPMWEB" id="cmdEliminarNeg" style=" WIDTH: 205px; HEIGHT: 24px"
					    onclick="eliminarUnidad()" type="button" value="DEliminar Unidad Neg" name="cmdEliminar" runat="server"/>
			    </td></tr>
			    <tr><td>
				    <input language="javascript" class="botonPMWEB" id="cmdBajaLogica" style=" WIDTH: 205px; HEIGHT: 24px"
					    onclick="Realizar_Deshacer_bajaLogica()" type="button" value="DDar de baja unidad de negocio"
					    name="cmdBajaLogica" runat="server"/>
			    </td></tr>
                <tr><td>
				    <input language="javascript" class="botonPMWEB" id="cmdUONs" style=" WIDTH: 205px; HEIGHT: 24px"
					    onclick="AsignarUnidadesOrganizativas();" type="button" value="DAsignar unidades organizativas"
					    name="cmdUONs" runat="server"/>
			    </td></tr>	                
                <tr><td>&nbsp;</td></tr>		    			    
			    <tr><td>&nbsp;</td></tr>
			    <tr><td>&nbsp;</td></tr>
			    <tr><td>
				    <asp:checkbox id="chkUnidadesBaja"
					    runat="server" CssClass="parrafo" Text="dIncluir unidades dadas de baja" AutoPostBack="True"></asp:checkbox>
			    </td></tr>
			</table>
			</div>      				

			<div style="clear:both; margin-left:16px;margin-top:10px; float:left; width: 950px;">
				<asp:label id="lblProveedores" 
				runat="server" CssClass="titulo" Height="20px" Width="100%">dProveedores</asp:label>
				<table class="igTree" 
				width="950px" cellpadding="2" cellspacing="2">
				<tr vAlign="middle">
					<td class="parrafo"><asp:label id="lblCodigo" runat="server" CssClass="parrafo" Height="20px" Width="100%">dC�digo:</asp:label></td>
					<td><input id="txtCodigo" type="text" name="txtCodigo" runat="server"></td>
					<td class="parrafo"><asp:label id="lblDenominacion" runat="server" CssClass="parrafo" Height="20px" Width="100%">dDenominaci�n:</asp:label></td>
					<td><input id="txtDenominacion" type="text" maxLength="200" size="30" name="txtDenominacion"
							runat="server"></td>
					<td class="parrafo" style="WIDTH: 64px"><asp:label id="lblCIF" runat="server" CssClass="parrafo" Height="20px" Width="100%">dCIF:</asp:label></td>
					<td><input id="txtCIF" style="WIDTH: 112px; HEIGHT: 22px" type="text" size="13" name="txtCIF"
							runat="server"></td>
				</tr>
				<tr>
					<td colSpan="2"><asp:checkbox id="chkProveedorBaja" runat="server" CssClass="parrafo" Text="dIncluir proveedores dados de baja"></asp:checkbox></td>
					<td colSpan="2"><select class="Combo" id="slcTipoProveedor" style="WIDTH: 100%" name="slcTipoProveedor"
							runat="server"></select>
					</td>
					<td></td>
					<td><asp:linkbutton id="hypBuscar" runat="server" CssClass="parrafo">Buscar</asp:linkbutton>&nbsp;&nbsp;&nbsp;&nbsp;<asp:imagebutton id="ImageButton1" runat="server" ImageUrl="../_common/images/buscar.gif"></asp:imagebutton></td>
				</tr>
			</table>
			
			<div id="div_proveedores" style="VISIBILITY: hidden;" runat="server">
				<table id="tblProv" style="width:950px;">
					<TBODY>
						<tr>
							<td align="right" colSpan="2"><INPUT class="botonPMWEB" id="cmdAceptar" style="WIDTH: 101px; HEIGHT: 24px" type="button" value="Aceptar"
									name="cmdAceptar" runat="server">&nbsp; <INPUT class="botonPMWEB" id="cmdCancelar" style="WIDTH: 101px; HEIGHT: 24px" type="button"
									value="dCancelar" name="cmdCancelar" runat="server">&nbsp; <INPUT language="javascript" class="botonPMWEB" id="cmdImprimir" onmouseover="CerrarPopUp()"
									style="WIDTH: 134px; HEIGHT: 24px" onclick="Imprimir(event)" type="button" value="dImprimir/Exportar" name="cmdImprimir" runat="server">&nbsp;</td>
						</tr>
						<tr>
							<td colSpan="2">
							
							<igtbl:ultrawebgrid id="uwgProveedores" runat="server" Height="450px" Width="950px">
									<DisplayLayout  AllowSortingDefault="Yes" RowHeightDefault="20px" Version="4.00" HeaderClickActionDefault="SortSingle"
										BorderCollapseDefault="Separate" Name="uwgProveedores" CellClickActionDefault="RowSelect">
										<AddNewBox>
											<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
										</Style>
										</AddNewBox>
										<Pager PageSize="4" StyleMode="PrevNext" AllowPaging="True">
											<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
											</Style>
										</Pager>
										<HeaderStyleDefault CssClass="VisorHead"></HeaderStyleDefault>
										<RowSelectorStyleDefault BorderWidth="0px" BorderStyle="Solid"></RowSelectorStyleDefault>
										<FrameStyle Width="950px" BorderWidth="0px" Font-Size="8pt" Font-Names="Verdana" BorderStyle="Solid"
											Height="450px" CssClass="visorFrame"></FrameStyle>
										<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
											<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
										</FooterStyleDefault>
										
										<SelectedHeaderStyleDefault BorderStyle="Solid"></SelectedHeaderStyleDefault>
										<EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
										<SelectedRowStyleDefault BorderWidth="0px" BorderStyle="Solid"></SelectedRowStyleDefault>
										<RowStyleDefault BorderWidth="1px" BorderColor="LightGray" BorderStyle="Solid"></RowStyleDefault>
									</DisplayLayout>
									<Bands>
										<igtbl:UltraGridBand  FixedHeaderIndicator="None" CellClickAction="RowSelect" RowSelectors="No">
											<HeaderStyle CssClass="CabeceraPMWEB Texto12"></HeaderStyle>
											<RowAlternateStyle CssClass="ugfilaalttabla"></RowAlternateStyle>										
													<Columns>
												<igtbl:TemplatedColumn Key="ANYADIR_REL" Type="CheckBox" DataType="System.Boolean" HeaderClickAction="Select"
													BaseColumnName="ANYADIR_REL" AllowUpdate="Yes">
													<CellStyle Width="30px"></CellStyle>
													<CellTemplate>
														<table border="0" cellpadding="0" cellspacing="0" align="center">
															<tr height="60px">
																<td valign="middle" align="center">
																	<table border="0" cellpadding="0" cellspacing="0">
																		<tr>
																			<td valign="bottom">
																				<asp:ImageButton ID="imgAnyadir" runat="server" ImageUrl="images/picoDcha.gif" />
																			</td>
																		</tr>
																		<tr>
																			<td valign="top">
																				<asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="images/picoIzqu.gif" />
																			</td>
																		</tr>
																	</table>
																</td>
																<td valign="top">
																	<table cellpadding="0" cellspacing="0">
																		<tr valign="top">
																			<td>
																				<asp:ListBox id="lstUnidadesNegocio" runat="server" Height="70px" Width="250px" SelectionMode="Multiple"></asp:ListBox>															
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</CellTemplate>
													<Footer Key="ANYADIR_REL"></Footer>
													<Header ClickAction="Select" Key="ANYADIR_REL"></Header>
												</igtbl:TemplatedColumn>

                                                <igtbl:TemplatedColumn Key="CHECKPOTENCIAL" Type="CheckBox" DataType="System.Boolean" HeaderClickAction="Select" BaseColumnName="CHECKPOTENCIAL" AllowUpdate="Yes" Hidden="true">
                                                    <CellTemplate>
                                                        <asp:CheckBox ID="chkPotencial" runat="server" />
                                                    </CellTemplate>
			                                        <Footer Key="CHECKPOTENCIAL"></Footer>
													<Header ClickAction="Select" Key="CHECKPOTENCIAL"></Header>
                                                </igtbl:TemplatedColumn>

                                                <igtbl:TemplatedColumn Key="CHECKBAJA_CALIDAD" Type="CheckBox" DataType="System.Boolean" HeaderClickAction="Select" BaseColumnName="CHECKBAJA_CALIDAD" AllowUpdate="Yes" Hidden="true">
                                                    <CellTemplate>
                                                        <asp:CheckBox ID="chkBajaCalidad" runat="server" />
                                                    </CellTemplate>
			                                        <Footer Key="CHECKBAJA_CALIDAD"></Footer>
													<Header ClickAction="Select" Key="CHECKBAJA_CALIDAD"></Header>
                                                </igtbl:TemplatedColumn>
											</Columns>
											<RowTemplateStyle BorderColor="White" BorderStyle="Ridge" BackColor="White">
												<BorderDetails WidthLeft="3px" WidthTop="3px" WidthRight="3px" WidthBottom="3px"></BorderDetails>
											</RowTemplateStyle>
											<RowStyle CssClass="ugfilatabla"></RowStyle>
										</igtbl:UltraGridBand>
									</Bands>
								</igtbl:ultrawebgrid></td>
						</tr>
					</TBODY>
				</table>
			</div>
			</div>

			<IFRAME id="iframeWSServer" style=" VISIBILITY: hidden; WIDTH: 112px; HEIGHT: 56px"
				name="iframeWSServer" src="../blank.htm"></IFRAME>
			<TABLE class="ugMenuItem" id="tblImprimir" style="Z-INDEX: 112; VISIBILITY: hidden; WIDTH: 88px; HEIGHT: 60px; position: absolute;"
				cellSpacing="0" cellPadding="0" width="70" border="0" runat="server">
				<TR>
					<TD class="ugMenuItem"><asp:linkbutton id="linkExcel" runat="server" CssClass="ugMenu">Excel</asp:linkbutton></TD>
				</TR>
				<TR>
					<TD class="ugMenuItem"><asp:linkbutton id="linkHTML" runat="server" CssClass="ugMenu">HTML</asp:linkbutton></TD>
				</TR>
				<TR>
					<TD class="ugMenuItem"><asp:linkbutton id="linkPDF" runat="server" CssClass="ugMenu">PDF</asp:linkbutton></TD>
				</TR>
			</TABLE>
			<input id="cont" type="hidden" value="0" name="cont" runat="server">
		</form>
	</body>
</html>
