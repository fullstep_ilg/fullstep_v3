/*''' <summary>
''' Crea la estructura de hiddens necesaria para grabar una asignaci�n de Certificados
''' </summary>
''' <remarks>Llamada desde: asignarTiposCertif.aspx; Tiempo m�ximo:0,2</remarks>*/
function MontarFormularioMatModifCertif()
{
  if (!document.forms["frmSubmit"])
		document.body.insertAdjacentHTML("beforeEnd","<form name=frmSubmit action=../materiales/guardarmaterial.aspx target=iframeWSServer id=frmSubmit method=post style='visibility:hidden;position:absolute;top:0;left:0'></form>")
  else
		document.forms["frmSubmit"].innerHTML = ""

  
  sInput = "GEN_Accion"
  anyadirInput(sInput,"")
  
  sInput = "GEN_Material"
  anyadirInput(sInput,"")
  
  
  for (i=0;i<arrNewCertif.length;i++) 
	{
		anyadirInput("NEWCERTIF_" + arrNewCertif[i] ,arrNewCertif[i])
	}
		
  for (i=0;i<arrDeleteCertif.length;i++) 
	{
		anyadirInput("DELETECERTIF_" + arrDeleteCertif[i] ,arrDeleteCertif[i])
	}
			
}
/*''' <summary>
''' Crea la estructura de hiddens necesaria para grabar una asignaci�n de Materiales
''' </summary>
''' <remarks>Llamada desde: asignarMateriales.aspx ; Tiempo m�ximo: 0,2</remarks>*/
function MontarFormularioMatModifGMN()
{
  if (!document.forms["frmSubmit"])
		document.body.insertAdjacentHTML("beforeEnd","<form name=frmSubmit action=../materiales/guardarmaterial.aspx target=iframeWSServer id=frmSubmit method=post style='visibility:hidden;position:absolute;top:0;left:0'></form>")
  else
		document.forms["frmSubmit"].innerHTML = ""

  
  sInput = "GEN_Accion"
  anyadirInput(sInput,"")
  
  sInput = "GEN_Material"
  anyadirInput(sInput,"")
  
  
  for (i=0;i<arrGMN1New.length;i++)
		{
		anyadirInput("NEWGMN1_" + arrGMN1New[i] ,arrGMN1New[i])
		}
		
	for (i=0;i<arrGMN2New.length;i++)
		{
		anyadirInput("NEWGMN2_" + arrGMN2New[i] ,arrGMN2New[i])
		}
		
	for (i=0;i<arrGMN3New.length;i++)
		{
		anyadirInput("NEWGMN3_" + arrGMN3New[i] ,arrGMN3New[i])
		}
						
	for (i=0;i<arrGMN4New.length;i++)
		{
		anyadirInput("NEWGMN4_" + arrGMN4New[i] ,arrGMN4New[i])
		}	
		
	for (i=0;i<arrGMN1Delete.length;i++)
		{
		anyadirInput("DELETEGMN1_" + arrGMN1Delete[i] ,arrGMN1Delete[i])
		}
		
	for (i=0;i<arrGMN2Delete.length;i++)
		{
		anyadirInput("DELETEGMN2_" + arrGMN2Delete[i] ,arrGMN2Delete[i])
		}
		
	for (i=0;i<arrGMN3Delete.length;i++)
		{
		anyadirInput("DELETEGMN3_" + arrGMN3Delete[i] ,arrGMN3Delete[i])
		}
						
	for (i=0;i<arrGMN4Delete.length;i++)
		{
		anyadirInput("DELETEGMN4_" + arrGMN4Delete[i] ,arrGMN4Delete[i])
		}	
		
}

/*''' <summary>
''' Crea la estructura de hiddens necesaria para grabar una asignaci�n de Materiales
''' </summary>
''' <remarks>Llamada desde: altaMaterialQA.aspx; Tiempo m�ximo: 0,2</remarks>*/
function MontarFormularioMatSubmit()
{
	if (!document.forms["frmSubmit"])
		document.body.insertAdjacentHTML("beforeEnd","<form name=frmSubmit action=../materiales/guardarmaterial.aspx target=fraWSServer id=frmSubmit method=post style='visibility:hidden;position:absolute;top:0;left:0'></form>")
	else
		document.forms["frmSubmit"].innerHTML = ""

  
	sInput = "GEN_Accion"
	anyadirInput(sInput,"")

	for (i=0;i<arrDenominacionesMat.length;i++) 
		{
		    anyadirInput("GEN_txt" + arrDenominacionesMat[i], document.forms["frmAlta"].elements["txtDen_" + arrDenominacionesMat[i]].value)		
		}
		
	for (i=0;i<arrCertificados.length;i++)
		{
		anyadirInput("CERTIF_" + arrCertificados[i] ,arrCertificados[i])
		}
		
	for (i=0;i<arrGMN1.length;i++)
		{
		anyadirInput("GMN1_" + arrGMN1[i] ,arrGMN1[i])
		}
		
	for (i=0;i<arrGMN2.length;i++)
		{
		anyadirInput("GMN2_" + arrGMN2[i] ,arrGMN2[i])
		}
		
	for (i=0;i<arrGMN3.length;i++)
		{
		anyadirInput("GMN3_" + arrGMN3[i] ,arrGMN3[i])
		}
						
	for (i=0;i<arrGMN4.length;i++)
		{
		anyadirInput("GMN4_" + arrGMN4[i] ,arrGMN4[i])
		}	
}

/*''' <summary>
''' Crea la estructura de hiddens necesaria para grabar Unidades QA
''' </summary>
''' <remarks>Llamada desde: AltaUnidadNeg.aspx ; Tiempo m�ximo: 0,2</remarks>*/
function MontarFormularioUNQASubmit () {
	
		if (!document.forms["frmSubmit"])
			document.body.insertAdjacentHTML("beforeEnd","<form name=frmSubmit action=../materiales/RealizarOperacionesOcultas.aspx target=fraWSServer id=frmSubmit method=post style='visibility:hidden;position:absolute;top:0;left:0'></form>")
		else
			document.forms["frmSubmit"].innerHTML = ""


		sIdi = document.forms["Form1"].elements["txt_Idioma"].value

		if (window.document.forms["Form1"].elements["accion_local"]) {
			sInput = "Accion"
			accion = 3
			if (window.document.forms["Form1"].elements["accion_local"].value==2)
				accion = 4

			anyadirInput(sInput,accion)
		}
		
		if (window.document.forms["Form1"].elements["txtCod"]) {
			sInput = "CODUNQA"
			anyadirInput(sInput,window.document.forms["Form1"].elements["txtCod"].value)
		}
		
		if (window.document.forms["Form1"].elements["ID_NODO"]) {
			sInput = "UNQA1"
			anyadirInput(sInput,window.document.forms["Form1"].elements["ID_NODO"].value)
		}
		if (window.document.forms["Form1"].elements["ID_PADRE"]) {
			sInput = "UNQA2"
			anyadirInput(sInput,window.document.forms["Form1"].elements["ID_PADRE"].value)
		}
		
		if (window.document.forms["Form1"].elements["txtDen_" +  sIdi]) {		
			sInput = "DEN"
			anyadirInput(sInput, document.forms["Form1"].elements["txtDen_" + sIdi].value)

		}
		

		for (i=0;i<arrDenominacionesUNQA.length;i++) 
			{
			anyadirInput("COD_" + arrDenominacionesUNQA[i], document.forms["Form1"].elements["txtDen_" + arrDenominacionesUNQA[i]].value)			
			anyadirInput("EXISTE_" + arrDenominacionesUNQA[i] ,arrDenominacionesUNQA_E[i])


            }
        if (window.document.forms["Form1"].elements["sMensaje"]) {
            sInput = "sMensaje"
            anyadirInput(sInput, window.document.forms["Form1"].elements["sMensaje"].value)
        }

}

/*''' <summary>
''' A�ade un hidden a la pantalla con la informaci�n proporcionada
''' </summary>
''' <param name="sInput">Id del hidden</param>
''' <param name="valor">valor del hidden</param>        
''' <remarks>Llamada desde: MontarFormularioMatModifCertif      MontarFormularioMatModifGMN     MontarFormularioMatSubmit
        MontarFormularioUNQASubmit; Tiempo m�ximo: 0</remarks>>*/
function anyadirInput(sInput,valor)
	{
	var s
		if (valor=="null")
			{
			valor = ""
			}
		if (valor==null)
			{
			valor = ""
			}
	
	var f = document.forms["frmSubmit"]
	if (!f.elements[sInput])
		{
		s="<INPUT type='hidden' name='" + sInput + "'>\n"
		f.insertAdjacentHTML("beforeEnd",s)
		f.elements[sInput].value = valor
		}
	}