
Public Class altaUnidadNeg
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblVolver As System.Web.UI.WebControls.Label
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblCodigo As System.Web.UI.WebControls.Label
    Protected WithEvents Textbox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents cmdAceptar As System.Web.UI.HtmlControls.HtmlInputButton

    Protected WithEvents denominacion As System.Web.UI.WebControls.Label
    Protected WithEvents UNQA_Nodo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents UNQA_Padre As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ID_NODO As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ID_PADRE As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tblDenominacion As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblDen As System.Web.UI.WebControls.Label
    Protected WithEvents txt_Idioma As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents accion_local As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtCod As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents cmdCancelar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cont As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents sMensaje As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
"<script language=""{0}"">{1}</script>"
    Private m_sMensaje1 As String
    Private m_sMensaje2 As String

    ''' <summary>
    ''' Evento de sistema, carga de pantalla sin detalle para q se de alta una unidad, si la "accion" es 1
    ''' y con el detalle de una unidad para mantenerla, si la "accion" es 2
    ''' </summary>
    ''' <param name="sender">parametro de sistema</param>
    ''' <param name="e">parametro de sistema</param>            
    ''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0 </remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim sIdi As String = Session("FSN_User").Idioma.ToString()
        Dim moduloID As Long
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.A�adirUnidadNeg, sIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        accion_local.Value = Request("Accion")  'Accion = 1 Alta nuevo nodo // Accion = 2 Modificacion nodo

        Me.cmdAceptar.Value = oTextos.Rows(1).Item(1)
        Me.cmdCancelar.Value = oTextos.Rows(2).Item(1)
        If accion_local.Value = "1" Then
            Me.lblTitulo.Text = oTextos.Rows(0).Item(1)
        Else
            Me.lblTitulo.Text = oTextos.Rows(7).Item(1)
        End If
        Me.lblCodigo.Text = oTextos.Rows(3).Item(1)
        Me.lblDen.Text = oTextos.Rows(8).Item(1)

        m_sMensaje2 = oTextos.Rows(9).Item(1) '10 Debe introducir la denominaci�n en
        m_sMensaje1 = oTextos.Rows(10).Item(1) '11 Debe introducir el c�digo de la Unidad de negocio
        sMensaje.Value = oTextos.Rows(11).Item(1)  '12 El c�digo introducido ya existe

        ID_NODO.Value = 0
        Dim sGrupo As String
        If Request("grupo") <> "" Then
            sGrupo = Request("grupo")
            sGrupo = Mid(sGrupo, InStr(sGrupo, "-") + 1, Len(sGrupo))


        End If
        moduloID = 482
        Dim sPadre As String
        If Request("DEN_NODO") <> "" Then
            UNQA_Nodo.Value = Request("DEN_NODO")
            sPadre = Mid(UNQA_Nodo.Value, InStr(UNQA_Nodo.Value, "-") + 1, Len(UNQA_Nodo.Value))
            ID_NODO.Value = Request("ID_NODO")
            moduloID = 483
        End If
        Dim sAbuelo As String
        If Request("DEN_PADRE") <> "" Then
            UNQA_Padre.Value = Request("DEN_PADRE")
            sAbuelo = Mid(UNQA_Padre.Value, InStr(UNQA_Padre.Value, "-") + 1, Len(UNQA_Padre.Value))
            ID_PADRE.Value = Request("ID_PADRE")
            moduloID = 484
        End If
        Dim sBisAbuelo As String = ""
        Dim sAux As String
        If Request("DEN_ABUELO") <> "" Then
            sAux = Request("DEN_ABUELO")
            sBisAbuelo = Mid(sAux, InStr(sAux, "-") + 1, Len(sAux))
        End If

        'm_sMensaje1 = oTextos.Rows(5).Item(1) 'Debe introducir el c�digo y la denominaci�n
        denominacion.Text = ""
        If sBisAbuelo <> "" Then
            denominacion.Text = sPadre & " / " & sAbuelo & " / " & sBisAbuelo & " / " & sGrupo
        ElseIf UNQA_Padre.Value <> "" Then
            denominacion.Text = sPadre & " / " & sAbuelo & " / " & sGrupo
        ElseIf UNQA_Nodo.Value <> "" Then
            denominacion.Text = sPadre & " / " & sGrupo
        End If

        If denominacion.Text = "" Then
            denominacion.Visible = False
        End If


        If accion_local.Value = 2 Then
            Dim aux(2) As String
            Dim sCodigo_denominacion As String
            sCodigo_denominacion = Request("DEN_NODO")
            aux = Split(sCodigo_denominacion, "-")
            Me.txtCod.Value = aux(0)
            moduloID -= 1
        End If


        Dim longitud As Long

        Dim oUnidadNeg As FSNServer.UnidadNeg

        oUnidadNeg = FSWSServer.Get_Object(GetType(FSNServer.UnidadNeg))
        longitud = oUnidadNeg.CargarLongitudCodUNQA(moduloID)

        oUnidadNeg = Nothing

        Me.txtCod.MaxLength = longitud

        '************* Carga las denominaciones *************

        CargarDenominaciones(accion_local.Value, ID_NODO.Value)


    End Sub

    Private Sub CargarDenominaciones(ByVal accion As Byte, ByVal idNodo As Long)

        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim sIdi As String = Session("FSN_User").Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If
        Me.txt_Idioma.Value = sIdi

        Dim oCelda As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oFila As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oText As System.Web.UI.WebControls.TextBox
        Dim oLabel As System.Web.UI.WebControls.Label
        Dim i As Integer



        Dim oUnidadNeg As FSNServer.UnidadNeg

        oUnidadNeg = FSWSServer.Get_Object(GetType(FSNServer.UnidadNeg))
        If accion = 2 Then oUnidadNeg.Id = idNodo
        oUnidadNeg.UnidadesNeg_CargarIdiomas()

        Dim sValor As String = oUnidadNeg.IdiomasColeccion.Item(sIdi)
        Dim arrDatos() As String
        Dim sCodIdi, sDenIdi, sValorIdi As String
        Dim bExiste As Boolean
        If sValor <> "" Then
            arrDatos = sValor.Split("#")
            sCodIdi = arrDatos(0)
            sDenIdi = arrDatos(1)
            sValorIdi = arrDatos(2)
            bExiste = arrDatos(3)

            'Carga 1� el idioma del usuario:
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oLabel = New System.Web.UI.WebControls.Label
            oLabel.Text = sDenIdi & ":"
            oLabel.CssClass = "parrafo"
            oCelda.Controls.Add(oLabel)
            oFila.Cells.Add(oCelda)

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oText = New System.Web.UI.WebControls.TextBox
            oText.ID = "txtDen_" & UCase(sCodIdi)
            oText.Width = Unit.Pixel(200)
            oText.MaxLength = 200
            oText.Text = sValorIdi
            oCelda.Controls.Add(oText)
            oFila.Cells.Add(oCelda)

            Me.tblDenominacion.Rows.Add(oFila)

        End If

        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrDenominacionesUNQA = new Array();var arrDenominacionesUNQA_E = new Array();"
        Dim sIdioma As String
        cont.Value = 0
        For i = 0 To oUnidadNeg.IdiomasNodo.Tables(0).Rows.Count - 1
            If oUnidadNeg.IdiomasNodo.Tables(0).Rows(i).Item("COD").ToString() = sIdi Then
                sIdioma = oLabel.Text
            Else
                oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oLabel = New System.Web.UI.WebControls.Label
                oLabel.Text = oUnidadNeg.IdiomasNodo.Tables(0).Rows(i).Item("DEN").ToString() & ":"
                If oUnidadNeg.IdiomasNodo.Tables(0).Rows(i).Item("COD").ToString() = sIdi Then
                    sIdioma = oLabel.Text
                End If
                oLabel.CssClass = "parrafo"
                oCelda.Controls.Add(oLabel)
                oFila.Cells.Add(oCelda)

                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oText = New System.Web.UI.WebControls.TextBox
                oText.ID = "txtDen_" & UCase(oUnidadNeg.IdiomasNodo.Tables(0).Rows(i).Item("COD").ToString())
                oText.Width = Unit.Pixel(200)
                oText.MaxLength = 200
                oText.Text = oUnidadNeg.IdiomasNodo.Tables(0).Rows(i).Item("DEN_NODO").ToString()
                oCelda.Controls.Add(oText)
                oFila.Cells.Add(oCelda)

                Me.tblDenominacion.Rows.Add(oFila)
            End If

            sClientTexts += "arrDenominacionesUNQA[arrDenominacionesUNQA.length] = '" + oUnidadNeg.IdiomasNodo.Tables(0).Rows(i).Item("COD").ToString() + "';"
            sClientTexts += "arrDenominacionesUNQA_E[arrDenominacionesUNQA_E.length] = '" + oUnidadNeg.IdiomasNodo.Tables(0).Rows(i).Item("EXISTE").ToString() + "';"
            cont.Value = cont.Value + 1
        Next


        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(m_sMensaje2) + " " + sIdioma + " ';"   'Debe introducir la denominaci�n en xxx
        sClientTexts += "arrTextosML[1] = '" + JSText(m_sMensaje1) + " ';"    'Debe introducir el c�digo de la unidad de negocio


        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        Page.ClientScript.RegisterStartupScript(Me.GetType(),"TextosMICliente", sClientTexts)
        oUnidadNeg = Nothing
    End Sub


End Class

