Public Class mantenimientomat
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents cmdAnyaMat As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdModifNombre As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdEliminar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdAsignarCertif As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdAsignarMatGS As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents uwtMateriales As Infragistics.WebUI.UltraWebNavigator.UltraWebTree
    Protected WithEvents cmdSelector As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdGuardarConf As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents whdgMateriales As Infragistics.Web.UI.GridControls.WebHierarchicalDataGrid
    Protected WithEvents tblCarga As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents cmdCargar As System.Web.UI.WebControls.Button
    Protected WithEvents linkExcel As New System.Web.UI.WebControls.LinkButton
    Protected WithEvents linkPDF As New System.Web.UI.WebControls.LinkButton
    Protected WithEvents linkHTML As New System.Web.UI.WebControls.LinkButton
    Protected WithEvents wdgExcelExporter As Infragistics.Web.UI.GridControls.WebExcelExporter
    Protected WithEvents wdgPDFExporter As Infragistics.Web.UI.GridControls.WebDocumentExporter
    Protected WithEvents lblVolver As System.Web.UI.WebControls.Label
    Protected WithEvents cmdAsignarObjSuelos As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cont As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents updGridMateriales As System.Web.UI.UpdatePanel
    Protected WithEvents btnPager As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Const IncludeScriptFormat As String = ControlChars.CrLf & _
"<script type=""{0}"" src=""{1}""></script>"
    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
"<script language=""{0}"">{1}</script>"

    Private m_sArrCaptionGrid(11) As String
    Private oMaterialesQA As FSNServer.MaterialesQA
    Private oStringWriter As New System.IO.StringWriter
    Private _pageNumber As Integer = 0

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '************************************************************
        '*** Descripci�n: Carga la pagina de mantenimiento de materiales              *****
        '*** Llamada desde: la opcion de menu   						*****                    
        '*** Tiempo m�ximo:  						*****                    
        '************************************************************
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.MantenimientoMat

        Dim oUnidadesQA As FSNServer.UnidadesNeg

        Me.lblTitulo.Text = Textos(0)
        Me.cmdAnyaMat.Value = Textos(3)
        Me.cmdModifNombre.Value = Textos(4)
        Me.cmdEliminar.Value = Textos(5)
        Me.cmdAsignarCertif.Value = Textos(6)
        Me.cmdAsignarMatGS.Value = Textos(7)
        Me.cmdCargar.Text = Textos(8)
        Me.cmdSelector.Value = Textos(9)
        Me.cmdGuardarConf.Value = Textos(10)
        Me.cmdAsignarObjSuelos.Value = Textos(28)

        m_sArrCaptionGrid(0) = Textos(13)  'Material de QA
        m_sArrCaptionGrid(1) = Textos(16)  'GS1
        m_sArrCaptionGrid(2) = Textos(17)  'Den.nivel 1 
        m_sArrCaptionGrid(3) = Textos(18)   'GS2
        m_sArrCaptionGrid(4) = Textos(19)   'Den.nivel 2
        m_sArrCaptionGrid(5) = Textos(20)   'GS3
        m_sArrCaptionGrid(6) = Textos(21)   'Den.nivel 3
        m_sArrCaptionGrid(7) = Textos(22)  'GS4
        m_sArrCaptionGrid(8) = Textos(23)  'Den.nivel 4
        m_sArrCaptionGrid(9) = Textos(14)  'Tipos de certificado
        m_sArrCaptionGrid(10) = Textos(12)  'Desplace una columna aqu� para agrupar por ese concepto
        Me.linkExcel.Text = Textos(24)
        Me.linkHTML.Text = Textos(25)
        Me.linkPDF.Text = Textos(26)
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Notificaciones
        CType(whdgMateriales.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(46)
        CType(whdgMateriales.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(48)
        'Vuelvo a cargar el modulo de MantenimientoMat
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.MantenimientoMat

        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(Textos(15)) + " ';"  'Seleccione un material
        sClientTexts += "var arrDenominacionesMat = new Array();"
        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)


        If Not Page.IsPostBack Then
            CargarArbolMateriales()
            Me.tblCarga.Visible = False
            whdgMateriales.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion")
            whdgMateriales.GridView.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion")
        Else
            Select Case Request("__EVENTTARGET")
                Case btnPager.ClientID
                    _pageNumber = CType(Request("__EVENTARGUMENT"), Integer)
                    Session("QApageNumberMat") = CType(Request("__EVENTARGUMENT"), String)
            End Select
            If Not (CType(Cache("oMaterial_" & FSNUser.Cod), DataSet) Is Nothing) Then
                RecargarGrid()
            End If
            Select Case Request("__EVENTTARGET")
                Case "Excel"
                    ExportarExcel()
                Case "PDF"
                    ExportarPDF()
                Case "HTML"
                    ExportarHTML()
                Case Else
                    updGridMateriales.Update()
            End Select
        End If

        'De momento el bot�n de guardar configuraci�n estar� invisible:
        Me.cmdGuardarConf.Visible = False

        If Not FSNUser.AsigObjetivoySuelo Then

            Me.cmdAsignarObjSuelos.Style.Item("visibility") = "hidden"
        Else
            oUnidadesQA = FSNServer.Get_Object(GetType(FSNServer.UnidadesNeg))
            oUnidadesQA.UsuLoadData(FSNUser.Cod, Idioma, Application("Nivel_UnQa"), TipoAccesoUNQAS.ConsultarObjetivosSuelos)

            If oUnidadesQA.UsuData.Tables(0).Rows.Count = 0 Then
                Me.cmdAsignarObjSuelos.Style.Item("visibility") = "hidden"
            End If
        End If

        Dim oIdiomas As FSNServer.Idiomas
        'Carga los idiomas:
        oIdiomas = FSNServer.Get_Object(GetType(FSNServer.Idiomas))
        oIdiomas.Load()

        cont.Value = oIdiomas.Idiomas.Count

    End Sub

    Private Sub RecargarGrid()
        With whdgMateriales
            .Rows.Clear()
            .Columns.Clear()
            .GridView.Columns.Clear()
            .GridView.ClearDataSource()
            .GridView.Behaviors.Paging.PageIndex = _pageNumber
            .DataSource = CType(Cache("oMaterial_" & FSNUser.Cod), DataSet)
            .GridView.DataSource = .DataSource

            CrearColumnasDs()
            .DataMember = "Table"
            .DataKeyFields = "MATERIAL_QA,GMN1,GMN2,GMN3,GMN4"
            .DataBind()
            ConfigurarGrid()
            updGridMateriales.Update()
        End With
    End Sub

    ''' <summary>
    ''' Carga el treeView de materiales de QA con el idioma del usuario.
    ''' Si estamos trabajando en modo pymes, se pasa tb el ID de la pyme del usuario
    ''' </summary>
    Private Sub CargarArbolMateriales()

        Dim sIdi As String = FSNUser.Idioma.ToString()
        Dim dsMaterialesQA As DataSet
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        Me.uwtMateriales.ClearAll()

        oMaterialesQA = FSNServer.Get_Object(GetType(FSNServer.MaterialesQA))

        dsMaterialesQA = oMaterialesQA.GetData(sIdi, , False, FSNUser.Pyme, FSNUser.QARestProvMaterial, FSNUser.CodPersona)
        Me.uwtMateriales.DataSource = dsMaterialesQA.Tables(0).DefaultView


        Me.uwtMateriales.Levels(0).ColumnName = "DEN_" + sIdi
        Me.uwtMateriales.Levels(0).LevelKeyField = "ID"
        Me.uwtMateriales.Levels(0).LevelCheckBoxes = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.True
        Me.uwtMateriales.Levels(0).RelationName = "REL_MAT_CERTIF"

        Me.uwtMateriales.Levels(1).ColumnName = "DEN_CERTIFICADO"
        Me.uwtMateriales.Levels(1).LevelKeyField = "COD"
        Me.uwtMateriales.Levels(1).LevelCheckBoxes = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.False

        Me.uwtMateriales.DataBind()
        Me.uwtMateriales.DataKeyOnClient = True

        dsMaterialesQA = Nothing
        oMaterialesQA = Nothing

    End Sub

    ''' Revisado por: Jbg. Fecha: 20/10/2011
    ''' <summary>
    ''' Carga la grid con la relaci�n de materiales 
    ''' </summary>
    ''' <param name="sender">cmdCargar</param>
    ''' <param name="e">parametro de sistema</param>
    ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,3</remarks>
    Private Sub cmdCargar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCargar.Click

        Dim i As Integer
        Dim sFiltroMat As String
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oUser As FSNServer.User = Session("FSN_User")

        Dim sIdi As String = Session("FSN_User").Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        sFiltroMat = ""

        For i = 0 To uwtMateriales.Nodes.Count - 1
            If uwtMateriales.Nodes(i).Checked = True Then
                If sFiltroMat <> "" Then
                    sFiltroMat = sFiltroMat & "_"
                End If
                sFiltroMat = sFiltroMat & uwtMateriales.Nodes(i).DataKey
            End If
        Next i

        If sFiltroMat = "" Then
            Exit Sub
        Else
            sFiltroMat = sFiltroMat & "_"
            Me.tblCarga.Visible = True
        End If

        'Llama a la b�squeda de materiales para cargar la grid:
        oMaterialesQA = FSWSServer.Get_Object(GetType(FSNServer.MaterialesQA))
        oMaterialesQA.DevolverMaterialesQA(sFiltroMat, sIdi)
        Dim ds As New DataSet
        Dim dt As DataTable
        If oMaterialesQA.Data.Tables.Count > 0 Then
            whdgMateriales.Rows.Clear()
            whdgMateriales.Columns.Clear()
            whdgMateriales.GridView.Columns.Clear()
            dt = oMaterialesQA.Data.Tables(0)
            ds.Tables.Add(dt.Copy())
            whdgMateriales.DataSource = ds
            CrearColumnas(dt)
            whdgMateriales.DataMember = "Table"
            whdgMateriales.DataKeyFields = "MATERIAL_QA,GMN1,GMN2,GMN3,GMN4"
            whdgMateriales.DataBind()
        End If
        ConfigurarGrid()
        Me.InsertarEnCache("oMaterial_" & FSNUser.Cod, ds)
    End Sub
    Private Sub CrearColumnas(ByVal dt As DataTable)

        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim nombre As String

        whdgMateriales.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion")
        whdgMateriales.GridView.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion")
        For i As Integer = 0 To dt.Columns.Count - 1
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            nombre = dt.Columns(i).ToString
            With campoGrid
                .Key = nombre
                .DataFieldName = nombre
                .Header.Text = nombre
                .Header.Tooltip = nombre
                .Header.CssClass = "headerNoWrap igg_FullstepHeaderCaption"
                .CssClass = "SinSalto"
            End With
            whdgMateriales.Columns.Add(campoGrid)
            whdgMateriales.GridView.Columns.Add(campoGrid)
        Next
        'A�ade la columna de los tipos de certificados
        AnyadirColumnaU("CERTIFICADOS")
    End Sub

    Private Sub CrearColumnasDs()

        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim nombre As String

        whdgMateriales.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion")
        whdgMateriales.GridView.Behaviors.Paging.PageSize = System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion")
        For i As Integer = 0 To whdgMateriales.DataSource.Tables(0).Columns.Count - 1
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            nombre = whdgMateriales.DataSource.Tables(0).Columns(i).ToString
            With campoGrid
                .Key = nombre
                .DataFieldName = nombre
                .Header.Text = nombre
                .Header.CssClass = "headerNoWrap igg_FullstepHeaderCaption"
                .CssClass = "SinSalto"
            End With
            whdgMateriales.Columns.Add(campoGrid)
            whdgMateriales.GridView.Columns.Add(campoGrid)
        Next
        'A�ade la columna de los tipos de certificados
        AnyadirColumnaU("CERTIFICADOS")
    End Sub

    Private Sub AnyadirColumnaU(ByVal fieldname As String)
        Dim unboundField As New Infragistics.Web.UI.GridControls.UnboundField(True)

        unboundField.Key = fieldname
        whdgMateriales.Columns.Add(unboundField)
        whdgMateriales.GridView.Columns.Add(unboundField)
    End Sub

    Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Unload
        oMaterialesQA = Nothing
    End Sub

    Private Sub whdgMateriales_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgMateriales.InitializeRow
        'Carga en la columna de certificados los certificados de ese material de QA
        If oMaterialesQA Is Nothing Then Exit Sub

        Dim oRow As DataRow
        Dim sCertif As String = String.Empty

        For Each oRow In oMaterialesQA.Data.Tables(1).Rows
            If oRow.Item("MATERIAL") = e.Row.Items.FindItemByKey("MATERIAL").Value Then
                If sCertif <> "" Then sCertif = sCertif & ";"
                sCertif = sCertif & oRow.Item("COD")
            End If
        Next
        e.Row.Items.FindItemByKey("CERTIFICADOS").Value = sCertif
        e.Row.Items.FindItemByKey("CERTIFICADOS").Tooltip = sCertif
    End Sub

    Private Sub whdgMateriales_DataBound(sender As Object, e As System.EventArgs) Handles whdgMateriales.DataBound
        Dim _pageCount As Integer
        Dim _rows As Integer
        Dim dt As DataTable = CType(whdgMateriales.DataSource, DataSet).Tables(0)

        _rows = dt.Rows.Count
        _pageCount = _rows \ System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion")
        _pageCount = _pageCount + (IIf(_rows Mod System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion") = 0, 0, 1))
        Paginador(_pageCount)
        updGridMateriales.Update()
    End Sub
    ''' <summary>
    ''' Tras ordenar actualiza el grid
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub whdgMateriales_ColumnSorted(sender As Object, e As Infragistics.Web.UI.GridControls.SortingEventArgs) Handles whdgMateriales.ColumnSorted
        Dim sName As String
        Dim sortCriteria As String = String.Empty
        Dim sortDirection As String = String.Empty
        Dim bdatabind As Boolean = False
        Dim sColsGroupBy As String = "ColsGroupBy"

        If e.SortedColumns.Count > 0 Then
            For Each sortColumn As Infragistics.Web.UI.GridControls.SortedColumnInfo In e.SortedColumns
                sName = sortColumn.ColumnKey
                sortDirection = IIf(sortColumn.SortDirection = Infragistics.Web.UI.SortDirection.Descending, " DESC", " ASC")
                sortCriteria = IIf(sortCriteria Is String.Empty, sName & sortDirection, sortCriteria & "," & sName & sortDirection)
            Next
        End If
        'Lo almacenamos en cookie
        Dim cookie As HttpCookie
        cookie = Request.Cookies("ORDENACION_QA_MAT")
        If cookie Is Nothing Then
            cookie = New HttpCookie("ORDENACION_QA_MAT")
        End If
        cookie.Value = sortCriteria
        cookie.Expires = Date.MaxValue
        Response.AppendCookie(cookie)


        updGridMateriales.Update()
    End Sub

    ''' <summary>
    ''' Tras filtrarse actualiza el grid
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub whdgMateriales_DataFiltered(sender As Object, e As Infragistics.Web.UI.GridControls.FilteredEventArgs) Handles whdgMateriales.DataFiltered
        updGridMateriales.Update()
    End Sub
    ''' <summary>
    ''' Tras agrupar actualiza el grid
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Private Sub whdgMateriales_GroupedColumnsChanged(sender As Object, e As Infragistics.Web.UI.GridControls.GroupedColumnsChangedEventArgs) Handles whdgMateriales.GroupedColumnsChanged
        Dim sColsGroupBy As String = "ColsGroupBy"
        Dim groupedColumnList As String = String.Empty
        For Each column As Infragistics.Web.UI.GridControls.GroupedColumn In e.GroupedColumns
            groupedColumnList = IIf(groupedColumnList Is String.Empty, String.Empty, "#")
            groupedColumnList &= column.ColumnKey & IIf(column.SortDirection = Infragistics.Web.UI.GridControls.GroupingSortDirection.Ascending, " ASC", " DESC")
        Next
        Session(sColsGroupBy) = groupedColumnList

        updGridMateriales.Update()
    End Sub

    Private Sub ConfigurarGrid()
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oUser As FSNServer.User
        Dim oConfCampos As FSNServer.ConfCampos

        oUser = Session("FSN_User")

        oConfCampos = FSWSServer.Get_Object(GetType(FSNServer.ConfCampos))
        oConfCampos.DevolverConfiguracionVisible(oUser.Cod, "mantenimientoMat")

        whdgMateriales.Columns("MATERIAL").Hidden = True
        whdgMateriales.Columns("CERTIFICADOS").Header.CssClass = "headerNoWrap igg_FullstepHeaderCaption"
        whdgMateriales.Columns("CERTIFICADOS").CssClass = "TablaLink SinSalto"
        whdgMateriales.Columns("CERTIFICADOS").Width = Unit.Percentage(30)

        'Comprueba si se van a ocultar las columnas de la grid o no:
        If oConfCampos.Data.Tables(0).Rows.Count > 0 Then
            If oConfCampos.Data.Tables(0).Rows(0).Item("MATERIAL_QA_VISIBLE") = 0 Then
                whdgMateriales.Columns("MATERIAL_QA").Hidden = True
            End If
            If oConfCampos.Data.Tables(0).Rows(0).Item("GMN1_VISIBLE") = 0 Then
                whdgMateriales.Columns("GMN1").Hidden = True
            End If
            If oConfCampos.Data.Tables(0).Rows(0).Item("DEN_GMN1_VISIBLE") = 0 Then
                whdgMateriales.Columns("DEN_GMN1").Hidden = True
            End If
            If oConfCampos.Data.Tables(0).Rows(0).Item("GMN2_VISIBLE") = 0 Then
                whdgMateriales.Columns("GMN2").Hidden = True
            End If
            If oConfCampos.Data.Tables(0).Rows(0).Item("DEN_GMN2_VISIBLE") = 0 Then
                whdgMateriales.Columns("DEN_GMN2").Hidden = True
            End If
            If oConfCampos.Data.Tables(0).Rows(0).Item("GMN3_VISIBLE") = 0 Then
                whdgMateriales.Columns("GMN3").Hidden = True
            End If
            If oConfCampos.Data.Tables(0).Rows(0).Item("DEN_GMN3_VISIBLE") = 0 Then
                whdgMateriales.Columns("DEN_GMN3").Hidden = True
            End If
            If oConfCampos.Data.Tables(0).Rows(0).Item("GMN4_VISIBLE") = 0 Then
                whdgMateriales.Columns("GMN4").Hidden = True
            End If
            If oConfCampos.Data.Tables(0).Rows(0).Item("DEN_GMN4_VISIBLE") = 0 Then
                whdgMateriales.Columns("DEN_GMN4").Hidden = True
            End If
            If oConfCampos.Data.Tables(0).Rows(0).Item("CERTIFICADOS_VISIBLE") = 0 Then
                whdgMateriales.Columns("CERTIFICADOS").Hidden = True
            End If
        End If
        oConfCampos = Nothing
        'Caption de la grid de materiales:
        whdgMateriales.Columns("MATERIAL_QA").Header.Text = m_sArrCaptionGrid(0)
        whdgMateriales.Columns("GMN1").Header.Text = m_sArrCaptionGrid(1)
        whdgMateriales.Columns("DEN_GMN1").Header.Text = m_sArrCaptionGrid(2)
        whdgMateriales.Columns("GMN2").Header.Text = m_sArrCaptionGrid(3)
        whdgMateriales.Columns("DEN_GMN2").Header.Text = m_sArrCaptionGrid(4)
        whdgMateriales.Columns("GMN3").Header.Text = m_sArrCaptionGrid(5)
        whdgMateriales.Columns("DEN_GMN3").Header.Text = m_sArrCaptionGrid(6)
        whdgMateriales.Columns("GMN4").Header.Text = m_sArrCaptionGrid(7)
        whdgMateriales.Columns("DEN_GMN4").Header.Text = m_sArrCaptionGrid(8)
        whdgMateriales.Columns("CERTIFICADOS").Header.Text = m_sArrCaptionGrid(9)
        whdgMateriales.Columns("MATERIAL_QA").Header.Tooltip = m_sArrCaptionGrid(0)
        whdgMateriales.Columns("GMN1").Header.Tooltip = m_sArrCaptionGrid(1)
        whdgMateriales.Columns("DEN_GMN1").Header.Tooltip = m_sArrCaptionGrid(2)
        whdgMateriales.Columns("GMN2").Header.Tooltip = m_sArrCaptionGrid(3)
        whdgMateriales.Columns("DEN_GMN2").Header.Tooltip = m_sArrCaptionGrid(4)
        whdgMateriales.Columns("GMN3").Header.Tooltip = m_sArrCaptionGrid(5)
        whdgMateriales.Columns("DEN_GMN3").Header.Tooltip = m_sArrCaptionGrid(6)
        whdgMateriales.Columns("GMN4").Header.Tooltip = m_sArrCaptionGrid(7)
        whdgMateriales.Columns("DEN_GMN4").Header.Tooltip = m_sArrCaptionGrid(8)
        whdgMateriales.Columns("CERTIFICADOS").Header.Tooltip = m_sArrCaptionGrid(9)
        whdgMateriales.GroupingSettings.EmptyGroupAreaText = m_sArrCaptionGrid(10)

    End Sub

    Private Sub Paginador(ByVal _pageCount As Integer)
        Dim pagerList As DropDownList = DirectCast(whdgMateriales.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
        pagerList.Items.Clear()
        For i As Integer = 1 To _pageCount
            pagerList.Items.Add(i.ToString())
        Next
        CType(whdgMateriales.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = _pageCount
        If _pageCount > 0 Then pagerList.SelectedIndex = _pageNumber
        Dim Desactivado As Boolean = (_pageNumber = 0)
        With CType(whdgMateriales.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        With CType(whdgMateriales.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        Desactivado = (_pageCount = 1 OrElse _pageNumber + 1 = _pageCount)
        With CType(whdgMateriales.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
        With CType(whdgMateriales.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), Image)
            .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(Desactivado, "_desactivado", "") & ".gif"
            If Not Desactivado Then .CssClass = "Link"
            .Enabled = Not Desactivado
        End With
    End Sub

    ''' <summary>
    ''' Exporta a Excel
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo m�ximo:0,1</remarks>
    Private Sub ExportarExcel()
        updGridMateriales.Update()
        Dim fileName As String = Me.lblTitulo.Text

        With wdgExcelExporter
            .DownloadName = fileName
            .DataExportMode = Infragistics.Web.UI.GridControls.DataExportMode.AllDataInDataSource
            .EnableStylesExport = False
			.WorkbookFormat = Infragistics.Documents.Excel.WorkbookFormat.Excel2007
            .Export(whdgMateriales)
        End With
    End Sub

    ''' <summary>
    ''' Exporta a PDF
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo m�ximo:0,1</remarks>
    Private Sub ExportarPDF()
        updGridMateriales.Update()
        Dim fileName As String = Me.lblTitulo.Text

        With wdgPDFExporter
            .DownloadName = fileName
            .DataExportMode = Infragistics.Web.UI.GridControls.DataExportMode.AllDataInDataSource
            .EnableStylesExport = False
            .Export(whdgMateriales)
        End With
    End Sub

    Private Sub ExportarHTML()

        GenerarHTMLPrint()

        '''''' Guardamos el HTML en un archivo
        Dim oFolder As System.IO.Directory
        Dim sPath As String

        sPath = ConfigurationManager.AppSettings("temp") + "\" + GenerateRandomPath()
        If Not oFolder.Exists(sPath) Then
            oFolder.CreateDirectory(sPath)
        End If
        sPath += "\" + "materials.html"

        Dim oStreamWriter As New System.IO.StreamWriter(sPath, False, System.Text.Encoding.UTF8)

        oStreamWriter.Write(oStringWriter.ToString)
        oStreamWriter.Close()

        ''''''' Ponemos el HTML a descargar
        Dim sBytes As Long
        Dim ofile As New System.IO.FileInfo(sPath)
        sBytes = ofile.Length
        Dim arrPath2() As String = sPath.Split("\")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "imprimir", "<script>var newWindow = window.open(""" & ConfigurationManager.AppSettings("rutaFS") & "_Common/download.aspx?path=" & arrPath2(UBound(arrPath2) - 1) & "&nombre=" & Server.UrlEncode(arrPath2(UBound(arrPath2))) & "&datasize=" & CStr(sBytes) & """,""_blank"",""width=600,height=275,status=no,resizable=yes,top=200,left=200,menubar=yes"");</script>")
    End Sub

    Private Sub GenerarHTMLPrint()
        Dim oHtmlTextWriter As New System.Web.UI.HtmlTextWriter(oStringWriter)
        Dim iLinea As Integer
        Dim iCol As Integer
        oHtmlTextWriter.WriteLine("<html>")
        oHtmlTextWriter.WriteLine("<head runat='server'>")
        oHtmlTextWriter.WriteLine("<meta http-equiv=""Content-Type"" content=""text/html; charset=windows-1252"">")
        oHtmlTextWriter.WriteLine("<STYLE>@import url( " & ConfigurationManager.AppSettings("ruta") + "App_Themes/" & Me.Theme & ".css );</STYLE>")
        oHtmlTextWriter.WriteLine("</head>")
        oHtmlTextWriter.WriteLine("<BODY>")

        'Va generando la p�gina HTML con los datos de la grid:
        oHtmlTextWriter.WriteLine("<TABLE border=0>")
        oHtmlTextWriter.WriteLine("<TR><TD>")
        oHtmlTextWriter.WriteLine("<label id=lblTitulo class=titulo>" & Me.lblTitulo.Text & "</label>")
        oHtmlTextWriter.WriteLine("</TD></TR>")

        Dim sFiltroMat As String
        sFiltroMat = ""
        For iLinea = 0 To uwtMateriales.Nodes.Count - 1
            If uwtMateriales.Nodes(iLinea).Checked = True Then
                If sFiltroMat <> "" Then sFiltroMat = sFiltroMat & ", "
                sFiltroMat = sFiltroMat & uwtMateriales.Nodes(iLinea).Text
            End If
        Next iLinea

        oHtmlTextWriter.WriteLine("<TR><TD>")
        oHtmlTextWriter.WriteLine("<label class=fntLogin>" & Me.lblTitulo.Text & ":" & sFiltroMat & "</label>")
        oHtmlTextWriter.WriteLine("</TD></TR>")


        oHtmlTextWriter.WriteLine("<TR><TD><TABLE border=0>")
        'La cabecera:
        oHtmlTextWriter.WriteLine("<TR>")
        For iCol = 0 To whdgMateriales.Columns.Count - 1
            If whdgMateriales.Columns(iCol).Hidden = False Then
                oHtmlTextWriter.WriteLine("<TD class=CaptionBlueSmallBold>")
                oHtmlTextWriter.WriteLine(whdgMateriales.Columns(iCol).Header.Text)
                oHtmlTextWriter.WriteLine("</TD>")
            End If
        Next
        oHtmlTextWriter.WriteLine("</TR>")

        'Los datos:
        For iLinea = 0 To whdgMateriales.Rows.Count - 1
            oHtmlTextWriter.WriteLine("<TR class=VisorRow>")
            For iCol = 0 To whdgMateriales.Columns.Count - 1
                If whdgMateriales.Columns(iCol).Hidden = False Then
                    oHtmlTextWriter.WriteLine("<TD>")
                    oHtmlTextWriter.WriteLine(whdgMateriales.Rows(iLinea).Items(iCol).Value)
                    oHtmlTextWriter.WriteLine("</TD>")
                End If
            Next
            oHtmlTextWriter.WriteLine("</TR>")
        Next
        oHtmlTextWriter.WriteLine("</TD></TR></TABLE>")
        oHtmlTextWriter.WriteLine("</BODY>")
        oHtmlTextWriter.WriteLine("</html>")

    End Sub
End Class
