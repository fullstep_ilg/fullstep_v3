
    Public Class asignarTiposCertif
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
        Protected WithEvents lblMaterial As System.Web.UI.WebControls.Label
        Protected WithEvents lblSeleccionar As System.Web.UI.WebControls.Label
        Protected WithEvents uwtTiposCertif As Infragistics.WebUI.UltraWebNavigator.UltraWebTree
        Protected WithEvents cmdAceptar As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents cmdCancelar As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents Material As System.Web.UI.HtmlControls.HtmlInputHidden

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Const AltaScriptKey As String = "AltaIncludeScript"
        Private Const AltaFileName As String = "js/jsAltaMat.js"
        Private Const IncludeScriptFormat As String = ControlChars.CrLf & _
 "<script type=""{0}"" src=""{1}""></script>"
        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
    "<script language=""{0}"">{1}</script>"

    ''' <summary>
    ''' Cargar la pagina
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>     
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0,2</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oUser As FSNServer.User
        Dim node As Infragistics.WebUI.UltraWebNavigator.Node
        Dim oSolicitudes As FSNServer.Solicitudes
        Dim osolicitud As DataRow
        Dim oRow As DataRow

        Dim sIdi As String = Session("FSN_User").Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.AsignaCertifAMatQA, sIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        oUser = Session("FSN_User")

        Me.lblTitulo.Text = oTextos.Rows(0).Item(1)
        Me.lblSeleccionar.Text = oTextos.Rows(1).Item(1)
        Me.cmdAceptar.Value = oTextos.Rows(2).Item(1)
        Me.cmdCancelar.Value = oTextos.Rows(3).Item(1)

        Me.lblMaterial.Text = Request("DenMaterial")
        Me.Material.Value = Request("Material")


        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(oTextos.Rows(5).Item(1)) + " ';"   'Seleccione al menos un certificado
        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        Page.ClientScript.RegisterStartupScript(Me.GetType(),"TextosMICliente", sClientTexts)


        '************* Carga los tipos de certificados en el �rbol *************
        node = uwtTiposCertif.Nodes.Add(oTextos.Rows(4).Item(1), "raiz")  'Carga el nodo ra�z

        oSolicitudes = FSWSServer.Get_Object(GetType(FSNServer.Solicitudes))
        oSolicitudes.LoadCertificados(sIdi, Request("Material"), , , "S.COD")

        Me.uwtTiposCertif.Levels(1).LevelCheckBoxes = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.True

        Dim newNode As Infragistics.WebUI.UltraWebNavigator.Node

        For Each osolicitud In oSolicitudes.Data.Tables(0).Rows  'Carga los tipos de certificados
            newNode = New Infragistics.WebUI.UltraWebNavigator.Node
            newNode.Text = osolicitud.Item("COD") & " - " & osolicitud.Item("DEN")
            newNode.Tag = osolicitud.Item("MATERIAL")
            If DBNullToSomething(osolicitud.Item("MATERIAL")) = Request("Material") Then
                newNode.Checked = True
            End If
            newNode.DataKey = osolicitud.Item("ID")
            node.Nodes.Add(newNode)
        Next

        Me.uwtTiposCertif.DataKeyOnClient = True
        Me.uwtTiposCertif.ExpandAll()

        oSolicitudes = Nothing

        Dim includeScript As String
        If Not Page.ClientScript.IsClientScriptIncludeRegistered(AltaScriptKey) Then
            includeScript = String.Format(IncludeScriptFormat, "text/javascript", AltaFileName)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(),AltaScriptKey, includeScript)
        End If

    End Sub

    End Class

