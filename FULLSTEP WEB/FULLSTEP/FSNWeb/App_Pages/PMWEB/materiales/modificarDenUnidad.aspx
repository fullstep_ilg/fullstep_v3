<%@ Page Language="vb" AutoEventWireup="false" Codebehind="modificarDenUnidad.aspx.vb" Inherits="Fullstep.FSNWeb.modificarDenUnidad"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">

	<script>
		function actualizarArbol(sNewDen)
		{
			var p=window.opener  
			p.actualizarDenArbol(sNewDen)
			window.close() 
		}		
	</script>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 8px" height="80"
				cellSpacing="1" cellPadding="1" width="100%" border="0">
				<TR>
					<TD>
						<asp:label id="lblTitulo" runat="server" Width="100%" CssClass="titulo" Height="3px">DModificación de material de QA</asp:label></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 46.98%">
						<asp:textbox id="txtDen" runat="server" Width="100%"></asp:textbox>
					</TD>
				</TR>
				<TR>
					<TD vAlign="bottom" align="center">
						<TABLE id="tblBotones" cellSpacing="1" cellPadding="1" width="300" border="0">
							<TR>
								<TD align="right"><INPUT class="botonPMWEB" id="cmdAceptar" type="button" value="DAceptar" name="cmdAceptar" runat="server"></TD>
								<TD><INPUT class="botonPMWEB" id="cmdCancelar" onclick="window.close()" type="button" value="DCancelar"
										name="cmdCancelar" runat="server"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
			&nbsp;
		</form>
	</body>
</html>
