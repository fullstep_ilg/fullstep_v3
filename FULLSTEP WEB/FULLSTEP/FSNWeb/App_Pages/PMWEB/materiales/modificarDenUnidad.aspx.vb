
    Public Class modificarDenUnidad
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
        Protected WithEvents cmdAceptar As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents cmdCancelar As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents txtDen As System.Web.UI.WebControls.TextBox

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private sMensaje As String

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim sIdi As String = Session("FSN_User").Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.ModificarDenUnidadNeg, sIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        Me.lblTitulo.Text = oTextos.Rows(0).Item(1)
        Me.cmdAceptar.Value = oTextos.Rows(1).Item(1)
        Me.cmdCancelar.Value = oTextos.Rows(2).Item(1)

        sMensaje = oTextos.Rows(4).Item(1)

        If Not Page.IsPostBack Then
            Me.txtDen.Text = Request("DenUnidad")
        End If
    End Sub

    Private Sub cmdAceptar_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAceptar.ServerClick
        'Guarda la denominacion
        Dim sNewDen As String
        Dim oUnidadNeg As FSNServer.UnidadNeg

        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")

        If txtDen.Text = "" Then
            Page.ClientScript.RegisterStartupScript(Me.GetType(),"claveStartUpScript", "<script>alert('" & sMensaje & "')</script>")
            Exit Sub
        Else
            sNewDen = JSText(txtDen.Text)
        End If

        oUnidadNeg = FSWSServer.Get_Object(GetType(FSNServer.UnidadNeg))
        oUnidadNeg.Id = Request("Unidad")
        oUnidadNeg.Den = txtDen.Text

        'oUnidadNeg.ModificarDenominacion()
        'Si no ha habido ning�n error al guardar se actualiza la denominaci�n en el �rbol
        Page.ClientScript.RegisterStartupScript(Me.GetType(),"claveStartUpScript", "<script>actualizarArbol('" & sNewDen & "')</script>")
        oUnidadNeg = Nothing
    End Sub
    End Class

