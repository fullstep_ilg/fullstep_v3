
    Public Class comprobarAsignarMatGS
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Comprueba que el material de QA que se ha seleccionado no est� asignado 
            'a otro material del QA:
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
            Dim oUser As FSNServer.User
        Dim sIdi As String = Session("FSN_User").Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If
        Dim oMaterialesQA As FSNServer.MaterialesQA

        oUser = Session("FSN_User")

            Dim sGMN1 As String = Request("GMN1")
            Dim sGMN2 As String = Request("GMN2")
            Dim sGMN3 As String = Request("GMN3")
            Dim sGMN4 As String = Request("GMN4")

            oMaterialesQA = FSWSServer.Get_Object(GetType(FSNServer.MaterialesQA))
            oMaterialesQA.ComprobarAsignacionMatQA(sGMN1, sGMN2, sGMN3, sGMN4, sIdi, Request("MaterialQA"))

            If oMaterialesQA.Data.Tables(0).Rows.Count > 0 Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(),"init", "<script>window.parent.imposibleAsignarMatQA(""" & oMaterialesQA.Data.Tables(0).Rows(0).Item("DEN_" & sIdi) & """,""" & Request("treeId") & """,""" & Request("nodeId") & """)</script>")
            Else
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(),"init", "<script>window.parent.asignarMaterialQA(""" & Request("treeId") & """,""" & Request("nodeId") & """)</script>")
            End If

            oMaterialesQA = Nothing
        End Sub

    End Class

