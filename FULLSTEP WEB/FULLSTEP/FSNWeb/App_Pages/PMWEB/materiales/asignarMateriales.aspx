<%@ Register TagPrefix="ignav" Namespace="Infragistics.WebUI.UltraWebNavigator" Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="asignarMateriales.aspx.vb" Inherits="Fullstep.FSNWeb.asignarMateriales"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	
	<script>
		var arrGMN1New = new Array()
		var arrGMN2New = new Array()
		var arrGMN3New = new Array() 
		var arrGMN4New = new Array()
		
		var arrGMN1Delete = new Array()
		var arrGMN2Delete = new Array()
		var arrGMN3Delete = new Array() 
		var arrGMN4Delete = new Array()
		/*''' <summary>
		''' Crea la estructura de datos necesaria para grabar una asignaci�n de materiales
		''' </summary>
		''' <remarks>Llamada desde: cmdAceptar /onclick; Tiempo m�ximo:0</remarks>*/		
		function asignarMateriales()
		{
		if ((arrGMN1New.length>0) || (arrGMN2New.length>0) || (arrGMN3New.length>0) || (arrGMN4New.length>0)  || (arrGMN1Delete.length>0) || (arrGMN2Delete.length>0) || (arrGMN3Delete.length>0) || (arrGMN4Delete.length>0))
		 {
		   var Material=document.forms["Form1"].elements["Material"].value
		   
		   MontarFormularioMatModifGMN()
		   document.forms["frmSubmit"].elements["GEN_Accion"].value ="modifMat"
		   document.forms["frmSubmit"].elements["GEN_Material"].value=Material
		   document.forms["frmSubmit"].submit()
		 }
		}
		
		function imposibleAsignarMatQA(DenMatQA,treeId, nodeId)
		{
			alert(arrTextosML[1] + '\r' + DenMatQA)
			var oTree =igtree_getTreeById(treeId)
			var node=igtree_getNodeById(nodeId)
			s=oTree.Events.NodeChecked[0]
			oTree.Events.NodeChecked[0]=""
			node.setChecked(false)
			oTree.Events.NodeChecked[0]=s
		}
		
		function asignarMaterialQA(treeId, nodeId)
		{
		  var oTree =igtree_getTreeById(treeId)
		 
		  //inhabilita el evento nodeclick para que no se lance cuando chequeemos los padres o hijos
		  s=oTree.Events.NodeChecked[0]
		  oTree.Events.NodeChecked[0]=""
		 
		  var node=igtree_getNodeById(nodeId)
		
		  //Guarda en el array de materiales:
		  AnyadirArray(node)
			
		  //Se checkean todos los hijos
		  if (node.hasChildren()==true)
			{
			   var onodeChild1=node.getFirstChild()
			   while (onodeChild1)
					{
						onodeChild1.setChecked(true)
						AnyadirArray(onodeChild1)
						
						if (onodeChild1.hasChildren()==true)
						 {
						  var onodeChild2=onodeChild1.getFirstChild()
						  while (onodeChild2)
						   {
							 onodeChild2.setChecked(true)
							 AnyadirArray(onodeChild2)
							 
							 if (onodeChild2.hasChildren()==true)
							  {
							   var onodeChild3=onodeChild2.getFirstChild()
							   while (onodeChild3)
								 {
								  onodeChild3.setChecked(true)
								  AnyadirArray(onodeChild3)
								  
								  onodeChild3 = onodeChild3.getNextSibling()
								 }
							  }
							 onodeChild2 = onodeChild2.getNextSibling()
						   }
						 }
						 
						onodeChild1 = onodeChild1.getNextSibling()
					  }
			   }
			   
			oTree.Events.NodeChecked[0]=s
		}
		
		
		</SCRIPT>
		<script type="text/javascript"><!--
		/*
		''' <summary>
		''' A�ade o elimina del array el material de GS que queremos relacionar con el Material QA
		''' </summary>
		''' <param name="treeId">Id del arbol</param>
		''' <param name="nodeId">Id del nodo</param>        
		''' <param name="bChecked">Si se ha checkeado o no el nodo</param>        
		''' <remarks>Llamada desde:=Evento que salta al checkear un nodo del arbol; Tiempo m�ximo:= 0,3seg.</remarks>
		*/
		function uwtMateriales_NodeChecked(treeId, nodeId, bChecked){
		  var oTree =igtree_getTreeById(treeId)
		  var node=igtree_getNodeById(nodeId)
		  if (bChecked==true)
			  {
				asignarMaterialQA(treeId,nodeId)
			  }
		  else 
			 {
				//inhabilita el evento nodeclick para que no se lance cuando chequeemos los padres o hijos
				s=oTree.Events.NodeChecked[0]
				oTree.Events.NodeChecked[0]=""
				 
				//Elimina del array:
				EliminarArray(node)
				
				//se deschequean los padres
				var onodeParent=node.getParent()
				while (onodeParent)
				 {
				   onodeParent.setChecked(false)
				   //Elimina del array:
				   EliminarArray(onodeParent)
				   onodeParent = onodeParent.getParent()
				 }
				 
				//se deschequean los hijos
				if (node.hasChildren()==true)
				  {
				   var onodeChild1=node.getFirstChild()
				   while (onodeChild1)
					  {
						onodeChild1.setChecked(false)
						//Elimina del array:
						EliminarArray(onodeChild1)
					
						if (onodeChild1.hasChildren()==true)
						 {
						  var onodeChild2=onodeChild1.getFirstChild()
						  while (onodeChild2)
						   {
							 onodeChild2.setChecked(false)
							 //Elimina del array:
							EliminarArray(onodeChild2)
						
							 if (onodeChild2.hasChildren()==true)
							  {
							   var onodeChild3=onodeChild2.getFirstChild()
							   while (onodeChild3)
								 {
								  onodeChild3.setChecked(false)
								  //Elimina del array:
								  EliminarArray(onodeChild3)
								  onodeChild3 = onodeChild3.getNextSibling()
								 }
							  }
							 onodeChild2 = onodeChild2.getNextSibling()
						   }
						 }
						 
						onodeChild1 = onodeChild1.getNextSibling()
					  }  
				  }
				  
				 oTree.Events.NodeChecked[0]=s
			 }
			 
		}
		
		/*''' <summary>
		''' Eliminar de los arrays q indican cual es la asignaci�n actual un material
		''' </summary>
		''' <param name="node">nodo a eliminar de los arrays</param>
		''' <remarks>Llamada desde: uwtMateriales_NodeChecked ; Tiempo m�ximo:0</remarks>*/		
		function EliminarArray(node)
		 {
			var Material=document.forms["Form1"].elements["Material"].value
			
			var iLevel=node.getLevel()
			
			//Elimina del array
				switch (iLevel)
				{
				case 0: //GMN1
				   if (node.getTag()!=Material)
					 {
						for (i=0;i<arrGMN1New.length;i++)
						{
						if (arrGMN1New[i] == node.getDataKey())
						  {
							arrGMN1New.splice(i,1)
							break;
						  }
						}
					  }
					else
					 {
					 arrGMN1Delete[arrGMN1Delete.length]=node.getDataKey()
					 }
					break;
					
				case 1: //GMN2
					if (node.getTag()!=Material)
					 {
						for (i=0;i<arrGMN2New.length;i++)
						{
						if (arrGMN2New[i] == node.getParent().getDataKey() + "&&&" + node.getDataKey())
						  {
							arrGMN2New.splice(i,1)
							break;
						  }
						}
					  }
					else
					 {
					 arrGMN2Delete[arrGMN2Delete.length]=node.getParent().getDataKey() + "&&&" + node.getDataKey()
					 }
					break;
					
				case 2: //GMN3  
					if (node.getTag()!=Material)
					 {
						for (i=0;i<arrGMN3New.length;i++)
						{
						if (arrGMN3New[i] == node.getParent().getParent().getDataKey() + "&&&" + node.getParent().getDataKey() + "&&&" + node.getDataKey()    )
						  {
							arrGMN3New.splice(i,1)
							break;
						  }
						}
					  }
					else
					 {
					 arrGMN3Delete[arrGMN3Delete.length]=node.getParent().getParent().getDataKey() + "&&&" + node.getParent().getDataKey() + "&&&" + node.getDataKey()    
					 }
					break;
					
				case 3: //GMN4   
					if (node.getTag()!=Material)  //Si no estaba asignado de antes
					 {
						for (i=0;i<arrGMN4New.length;i++)
						{
						if (arrGMN4New[i] == node.getParent().getParent().getParent().getDataKey() + "&&&" + node.getParent().getParent().getDataKey() + "&&&" + node.getParent().getDataKey() + "&&&" + node.getDataKey())
						  {
							arrGMN4New.splice(i,1)
							break;
						  }
						}
					  }
					else
					 {
					 arrGMN4Delete[arrGMN4Delete.length]=node.getParent().getParent().getParent().getDataKey() + "&&&" + node.getParent().getParent().getDataKey() + "&&&" + node.getParent().getDataKey() + "&&&" + node.getDataKey()    
					 }
					break;
				}
			
			
		 }
	 /*''' <summary>
	 ''' A�adir a los arrays q indican cual es la asignaci�n actual un material
	 ''' </summary>
	 ''' <param name="node">nodo a a�adir de los arrays</param>
	 ''' <remarks>Llamada desde: uwtMateriales_NodeChecked ; Tiempo m�ximo:0</remarks>*/			 
	function AnyadirArray(node)
		{
		//Guarda en el array de nuevos materiales:
		var Material=document.forms["Form1"].elements["Material"].value
		
		var iLevel=node.getLevel()
		
		switch (iLevel)
			{
				case 0: //GMN1
					if (node.getTag()==Material) //si estaba marcado de antes se comprueba si est� en la de eliminar.Si es as� se borra de ella
					 {
						for (i=0;i<arrGMN1Delete.length;i++)
						{
						if (arrGMN1Delete[i] == node.getDataKey())
						  {
							arrGMN1Delete.splice(i,1)
							break;
						  }
						}
					 }
					else //si no estaba seleccionado antes se tiene que a�adir
						arrGMN1New[arrGMN1New.length]=node.getDataKey()
					break;
					
				case 1: //GMN2
					if (node.getTag()==Material) //si estaba marcado de antes se comprueba si est� en la de eliminar.Si es as� se borra de ella
					 {
						for (i=0;i<arrGMN2Delete.length;i++)
						{
						if (arrGMN2Delete[i] == node.getParent().getDataKey() + "&&&" + node.getDataKey())
						  {
							arrGMN2Delete.splice(i,1)
							break;
						  }
						}
					 }
					else
						arrGMN2New[arrGMN2New.length]=node.getParent().getDataKey() + "&&&" + node.getDataKey()
					break;
					
				case 2: //GMN3
					if (node.getTag()==Material) //si estaba marcado de antes se comprueba si est� en la de eliminar.Si es as� se borra de ella
					 {
						for (i=0;i<arrGMN3Delete.length;i++)
						{
						if (arrGMN3Delete[i] == node.getParent().getParent().getDataKey() + "&&&" + node.getParent().getDataKey() + "&&&" + node.getDataKey()    )
						  {
							arrGMN3Delete.splice(i,1)
							break;
						  }
						}
					 }
					else
						arrGMN3New[arrGMN3New.length]=node.getParent().getParent().getDataKey() + "&&&" + node.getParent().getDataKey() + "&&&" + node.getDataKey()    
					break;
					
				case 3: //GMN4
					if (node.getTag()==Material) //si estaba marcado de antes se comprueba si est� en la de eliminar.Si es as� se borra de ella
					 {
						for (i=0;i<arrGMN4Delete.length;i++)
						{
						if (arrGMN4Delete[i] == node.getParent().getParent().getParent().getDataKey() + "&&&" + node.getParent().getParent().getDataKey() + "&&&" + node.getParent().getDataKey() + "&&&" + node.getDataKey())
						  {
							arrGMN4Delete.splice(i,1)
							break;
						  }
						}
					 }
					else
						arrGMN4New[arrGMN4New.length]=node.getParent().getParent().getParent().getDataKey() + "&&&" + node.getParent().getParent().getDataKey() + "&&&" + node.getParent().getDataKey() + "&&&" + node.getDataKey()    
					break;	
			}
		
	  }	  
--></script>	
	<body>
		<form id="Form1" method="post" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <CompositeScript>
                <Scripts>
                    <asp:ScriptReference Path="../alta/js/AdjacentHTML.js" />
                </Scripts>
            </CompositeScript>
        </asp:ScriptManager>
			<IFRAME id="iframeWSServer" style="Z-INDEX: 102; LEFT: 8px; VISIBILITY: hidden; POSITION: absolute; TOP: 200px"
				name="iframeWSServer" src="../blank.htm"></IFRAME>
			<asp:label id="lblTitulo" style="Z-INDEX: 100; LEFT: 16px; POSITION: absolute; TOP: 15px" runat="server"
				Width="90%" Height="3px" CssClass="titulo"> DAsignaci�n materiales de GS</asp:label><INPUT id="Material" style="Z-INDEX: 107; LEFT: 8px; POSITION: absolute; TOP: 8px" type="hidden"
				size="1" name="Material" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 105; LEFT: 35px; POSITION: absolute; TOP: 420px" cellSpacing="1"
				cellPadding="1" width="85%" border="0">
				<TR>
					<TD align="center"><INPUT class="botonPMWEB" id="cmdAceptar" type="button" value="DAceptar" name="cmdAceptar" runat="server"
							onclick="asignarMateriales()"></TD>
					<TD align="center"><INPUT class="botonPMWEB" id="cmdCancelar" onclick="window.close()" type="button" value="Cancelar"
							name="cmdCancelar" runat="server"></TD>
				</TR>
			</TABLE>
			<ignav:ultrawebtree id="uwtMateriales" style="Z-INDEX: 104; LEFT: 35px; POSITION: absolute; TOP: 104px"
				runat="server" Height="300px" CssClass="igTree" width="85%" WebTreeTarget="HierarchicalTree">
				<SelectedNodeStyle ForeColor="White" BackColor="Navy"></SelectedNodeStyle>
				<Levels>
					<ignav:Level Index="0"></ignav:Level>
					<ignav:Level Index="1"></ignav:Level>
				</Levels>
				<ClientSideEvents NodeChecked="uwtMateriales_NodeChecked"></ClientSideEvents>
			</ignav:ultrawebtree>
			<asp:label id="lblMaterial" style="Z-INDEX: 101; LEFT: 35px; POSITION: absolute; TOP: 55px"
				runat="server" Width="85%" Height="3px" CssClass="captionBlue">DMaterial</asp:label>&nbsp;
			<asp:Label id="lblSeleccionar" style="Z-INDEX: 103; LEFT: 35px; POSITION: absolute; TOP: 84px"
				runat="server" Width="80%" CssClass="parrafo">DSeleccione los materiales de GS relacionados</asp:Label>
		</form>
	</body>
</html>
