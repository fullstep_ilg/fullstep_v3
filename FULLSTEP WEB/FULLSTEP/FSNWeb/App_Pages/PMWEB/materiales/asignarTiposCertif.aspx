<%@ Page Language="vb" AutoEventWireup="false" Codebehind="asignarTiposCertif.aspx.vb" Inherits="Fullstep.FSNWeb.asignarTiposCertif"%>
<%@ Register TagPrefix="ignav" Namespace="Infragistics.WebUI.UltraWebNavigator" Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>

	<script>		
		var arrNewCertif = new Array()
		var arrDeleteCertif = new Array()
		
		function actualizarArbol()
		{
			var p=window.opener  
			var oTree = p.igtree_getTreeById("uwtMateriales")
			var oNode=oTree.getSelectedNode()
			if (oNode)
			  {
				//Primero elimina todos los tipos de certificados
				var children = oNode.getChildNodes();
				for (c in children) 
				 {
					var childSubNode = children[c];
					childSubNode.remove()
				 } 

				//Ahora a�ade los nuevos que se han seleccionado
				var oTreeCert = igtree_getTreeById("uwtTiposCertif")
				var nodes = oTreeCert.getNodes()
				var onodeCert = nodes[0]

				onodeCert = onodeCert.getFirstChild()
				while (onodeCert)
				  {
					if (onodeCert.getChecked()== true)
						oNode.addChild(onodeCert.getText())
							
					onodeCert = onodeCert.getNextSibling()
				  }
			  }
			window.close() 
		}
		/*''' <summary>
		''' Crea la estructura de datos necesaria para grabar una asignaci�n de Certificados
		''' </summary>
		''' <remarks>Llamada desde: cmdAceptar/onClick; Tiempo m�ximo: 0,3 </remarks>*/		
		function asignarCertificados()
		{
			var oTree = igtree_getTreeById("uwtTiposCertif")
			var nodes = oTree.getNodes()
			var node = nodes[0]
			var Material=document.forms["Form1"].elements["Material"].value
			var bChequedado=false
			
			node = node.getFirstChild()
			while (node)
			{
				if (node.getChecked()== true)
				  { //Si est� chequeado y antes no estaba asignado se asigna al array
					bChequedado=true
					if (node.getTag()!=Material)
					  {
						arrNewCertif[arrNewCertif.length]=node.getDataKey()
					  }
				  }
				else  //si no est� chequeado y antes si que estaba seleccionado se a�ade al array de borrados
				  {
					if (node.getTag()==Material)
					  {
						arrDeleteCertif[arrDeleteCertif.length]=node.getDataKey()
					  }
				  }
						
				node = node.getNextSibling()
			}
			
			if (bChequedado==false)
				alert(arrTextosML[0])
			else
			 {
				if ((arrNewCertif.length>0) || (arrDeleteCertif.length>0))
				{
					MontarFormularioMatModifCertif()
					document.forms["frmSubmit"].elements["GEN_Accion"].value ="modCertif"
					document.forms["frmSubmit"].elements["GEN_Material"].value=Material
					document.forms["frmSubmit"].submit()
				}
				else
					window.close()
			 }
			 
		}
		
	</script>	
	<body>
		<form id="Form1" method="post" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <CompositeScript>
                <Scripts>
                    <asp:ScriptReference Path="../alta/js/AdjacentHTML.js" />
                    <asp:ScriptReference Path="../materiales/js/jsAltaMat.js" />
                </Scripts>
            </CompositeScript>
        </asp:ScriptManager>
		<IFRAME id="iframeWSServer" style="Z-INDEX: 103; LEFT: 8px; VISIBILITY: hidden; POSITION: absolute; TOP: 200px"
				name="iframeWSServer" src="../blank.htm"></IFRAME>
				
			<asp:label id="lblTitulo" style="Z-INDEX: 100; LEFT: 16px; POSITION: absolute; TOP: 15px" runat="server"
				Width="90%" Height="3px" CssClass="titulo">DAsignaci�n de tipos de certificados</asp:label><INPUT id="Material" style="Z-INDEX: 106; LEFT: 8px; POSITION: absolute; TOP: 8px" type="hidden"
				size="1" name="Material" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 105; LEFT: 35px; POSITION: absolute; TOP: 420px" cellSpacing="1"
				cellPadding="1" width="85%" border="0">
				<TR>
					<TD align="center"><INPUT class="botonPMWEB" id="cmdAceptar" type="button" value="Seleccionar" name="cmdAceptar"
							runat="server" onclick="asignarCertificados()"></TD>
					<TD align="center"><INPUT class="botonPMWEB" id="cmdCancelar" onclick="window.close()" type="button" value="Cancelar"
							name="cmdCancelar" runat="server"></TD>
				</TR>
			</TABLE>
			<ignav:ultrawebtree id="uwtTiposCertif" style="Z-INDEX: 104; LEFT: 35px; POSITION: absolute; TOP: 104px"
				runat="server" Height="300px" CssClass="igTree" width="85%" WebTreeTarget="HierarchicalTree">
				<SelectedNodeStyle ForeColor="White" BackColor="Navy"></SelectedNodeStyle>
				<Levels>
					<ignav:Level Index="0"></ignav:Level>
					<ignav:Level Index="1"></ignav:Level>
				</Levels>
			</ignav:ultrawebtree>
			<asp:label id="lblMaterial" style="Z-INDEX: 101; LEFT: 35px; POSITION: absolute; TOP: 55px"
				runat="server" Width="85%" Height="3px" CssClass="captionBlue">DMaterial</asp:label>&nbsp;
			<asp:Label id="lblSeleccionar" style="Z-INDEX: 103; LEFT: 35px; POSITION: absolute; TOP: 84px"
				runat="server" Width="80%" CssClass="parrafo">DSeleccione los tipos de certificados relacionados</asp:Label>
		</form>
	</body>
</html>
