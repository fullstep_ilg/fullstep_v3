<%@ Page Language="vb" AutoEventWireup="false" Codebehind="modificarDenMaterial.aspx.vb" Inherits="Fullstep.FSNWeb.modificarDenMaterial"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<script>
		function actualizarArbol(sNewDen)
		{
			var p=window.opener  
			p.actualizarDenArbol(sNewDen)
			window.close()
		}
	</script>	
	<body>
		<form id="Form1" method="post" runat="server">
			&nbsp;
			<table id="Table1" cellspacing="1" cellpadding="1" style="width:100%; height:90%;" border="0">
				<tr height="8%">
					<td>
						<asp:label id="lblTitulo" runat="server" Width="100%" CssClass="titulo" Height="3px">DModificación de material de QA</asp:label>
					</td>
				</tr>
				<tr height="80%">
					<td>
						<table id="tblDenominacion" height="100%" cellSpacing="1" cellPadding="1" width="100%"
							border="0" runat="server">
							<tr style="height:2px;">
								<td></td>
								<td></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="15%">
					<td align="center" vAlign="bottom">
						<table id="tblBotones" cellSpacing="1" cellPadding="1" width="300" border="0">
							<tr>
								<td align="right">
									<input class="botonPMWEB" id="cmdAceptar" type="button" value="DAceptar" runat="server"/>
								</td>
								<td>
									<input class="botonPMWEB" id="cmdCancelar" type="button" value="DCancelar" onclick="window.close()" runat="server">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
