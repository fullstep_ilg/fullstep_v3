
    Public Class eliminarUnidad
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
        Protected WithEvents cmdAceptar As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents cmdCancelar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents lblUnidad As System.Web.UI.WebControls.Label
        Protected WithEvents accion As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
        Protected WithEvents lblMensaje2 As System.Web.UI.WebControls.Label
        Protected WithEvents IDUNQA As System.Web.UI.HtmlControls.HtmlInputHidden

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region



        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim sIdi As String = Session("FSN_User").Idioma.ToString()
        Me.IDUNQA.Value = Request("Unidad")
        Dim sDenUnidad = Request("DenUnidad")


        If Page.IsPostBack Then Exit Sub
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.EliminarUnidadNeg, sIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        Me.lblTitulo.Text = oTextos.Rows(0).Item(1)
        
        'Primero se comprueba si la Unidad de Negocio tiene Unidades hijas dados de baja logica
        Dim cont As Integer
        Dim oUnidadNeg As FSNServer.UnidadNeg
        oUnidadNeg = FSWSServer.Get_Object(GetType(FSNServer.UnidadNeg))
        oUnidadNeg.Id = IDUNQA.Value

        cont = oUnidadNeg.ComprobarUnidadesHijas()
        Dim bBajaLogica As Boolean
        If cont > 0 Then
            lblMensaje.Text = oTextos.Rows(7).Item(1) '"�Atenci�n! La unidad de negocio seleccionada tiene unidades de negocio hijas dadas de baja l�gica."
            lblMensaje2.Text = oTextos.Rows(8).Item(1) '"Si continua con la eliminaci�n, la unidad de negocio ser� dada de baja l�gica para no perder los datos que dependen de ella."
            bBajaLogica = True
            accion.Value = 1
        Else
            Dim bDatosRelacionados, bUsuariosRelacionados, bProveedoresRelacionados As Boolean
            Dim bDatosRelacionados2, bPuntProveedoresRelacionados, bPPMsRelacionado As Boolean
            Dim sMensaje As String
            Dim sMensaje21 As String = oTextos.Rows(9).Item(1) '"Si continua con la eliminaci�n, perder� la relaci�n de esta unidad de negocio con dichos datos."
            Dim sMensaje22 As String = oTextos.Rows(10).Item(1) '"Si continua con la eliminaci�n, la unidad de negocio ser� dado de baja l�gica para no perder los datos relacionados."

            'Comprueba si existen datos relacionados
            oUnidadNeg.ObtenerDatosRelacionados()


            If oUnidadNeg.Data.Tables.Count > 0 Then
                sMensaje = oTextos.Rows(11).Item(1) '"�Atenci�n! La unidad de negocio seleccionada tiene relacionados: "

                Dim sTemp1 As String = " " & oTextos.Rows(12).Item(1) '" usuarios"
                Dim sTempY As String = " " & oTextos.Rows(13).Item(1) '" y"
                Dim sTemp2 As String = " " & oTextos.Rows(14).Item(1) '" proveedores"
                Dim sTemp3 As String = " " & oTextos.Rows(15).Item(1) '" puntuaciones"
                Dim sTemp4 As String = " " & oTextos.Rows(16).Item(1) '" no conformidades"
                Dim sTemp5 As String = " " & oTextos.Rows(17).Item(1) '" y datos necesarios para el calculo de PPM's"
                Dim sTemp6 As String = " " & oTextos.Rows(18).Item(1) '" cargos a proveedores"
                Dim sTemp7 As String = " " & oTextos.Rows(19).Item(1) '" tasas de servicio"
                Dim sAux As String

                If oUnidadNeg.Data.Tables(0).Rows(0).Item(0) > 0 Then
                    'Usuarios
                    bDatosRelacionados = True
                    sMensaje = sMensaje & sTemp1
                End If

                If oUnidadNeg.Data.Tables(1).Rows(0).Item(0) > 0 Then
                    'Proveedores
                    If bDatosRelacionados Then
                        sMensaje = sMensaje & "#y#" & sTemp2
                    Else
                        sMensaje = sMensaje & sTemp2
                    End If
                    bDatosRelacionados = True
                End If
                If oUnidadNeg.Data.Tables(2).Rows(0).Item(0) > 0 Then
                    'Puntuacion proveedores
                    If bDatosRelacionados Then
                        sMensaje = Replace(sMensaje, "#y#", ",")
                        sMensaje = sMensaje & "#y#" & sTemp3
                    Else
                        sMensaje = sMensaje & sTemp3
                    End If
                    bDatosRelacionados2 = True
                End If


                If oUnidadNeg.Data.Tables(3).Rows(0).Item(0) > 0 Then
                    'No Conformidades
                    If bDatosRelacionados Or bDatosRelacionados2 Then
                        sMensaje = Replace(sMensaje, "#y#", ",")
                        sMensaje = sMensaje & "#y#" & sTemp4
                    Else
                        sMensaje = sMensaje & sTemp4
                    End If

                    bDatosRelacionados2 = True
                End If

                If oUnidadNeg.Data.Tables(4).Rows(0).Item(0) > 0 Then
                    'PPMs
                    If bDatosRelacionados Or bDatosRelacionados2 Then
                        sMensaje = Replace(sMensaje, "#y#", ",")
                        sMensaje = sMensaje & "#y#" & sTemp5
                    Else
                        sMensaje = sMensaje & sTemp5
                    End If
                    bDatosRelacionados2 = True
                End If
                If oUnidadNeg.Data.Tables(5).Rows(0).Item(0) > 0 Then
                    'Cargos
                    If bDatosRelacionados Or bDatosRelacionados2 Then
                        sMensaje = Replace(sMensaje, "#y#", ",")
                        sMensaje = sMensaje & "#y#" & sTemp6
                    Else
                        sMensaje = sMensaje & sTemp6
                    End If

                    bDatosRelacionados2 = True
                End If
                If oUnidadNeg.Data.Tables(6).Rows(0).Item(0) > 0 Then
                    'Tasas de Servicio
                    If bDatosRelacionados Or bDatosRelacionados2 Then
                        sMensaje = Replace(sMensaje, "#y#", ",")
                        sMensaje = sMensaje & "#y#" & sTemp7
                    Else
                        sMensaje = sMensaje & sTemp7
                    End If
                    bDatosRelacionados2 = True
                End If
                sMensaje = Replace(sMensaje, "#y#", sTempY)

                If Not bDatosRelacionados And Not bDatosRelacionados2 Then

                    Me.lblMensaje.Visible = False
                    Me.lblMensaje2.Text = oTextos.Rows(20).Item(1) '"La unidad de negocio XXXXX no tiene ning�n dato relacionado. �Confirma la eliminaci�n de la unidad de negocio XXXX?"
                    Me.lblMensaje2.Text = Replace(Me.lblMensaje2.Text, "XXXXX", sDenUnidad)
                    Me.lblMensaje2.Text = Replace(Me.lblMensaje2.Text, "XXXXX", sDenUnidad)

                    accion.Value = "2"
                ElseIf bDatosRelacionados2 Then
                    Me.lblMensaje.Text = sMensaje
                    Me.lblMensaje2.Text = sMensaje22
                    accion.Value = "1"  '--> Dar de baja logica
                Else
                    lblMensaje.Text = sMensaje
                    lblMensaje2.Text = sMensaje21
                    accion.Value = "2" '--> aunque tenga alguna relacion (usuario o proveedores), se puede eliminar
                End If
            Else

                'No tiene ningun dato relacionado y se puede eliminar
                Me.lblMensaje.Visible = False
                Me.lblMensaje2.Text = oTextos.Rows(20).Item(1) '"La unidad de negocio XXXXX no tiene ning�n dato relacionado. �Confirma la eliminaci�n de la unidad de negocio XXXX?"
                Me.lblMensaje2.Text = Replace(Me.lblMensaje2.Text, "XXXXX", sDenUnidad)
                Me.lblMensaje2.Text = Replace(Me.lblMensaje2.Text, "XXXXX", sDenUnidad)

                accion.Value = "2"
            End If
        End If

        oUnidadNeg = Nothing
        Me.cmdAceptar.Value = oTextos.Rows(4).Item(1)
        Me.cmdCancelar.Value = oTextos.Rows(5).Item(1)
        Me.lblUnidad.Text = Request("DenUnidad")
    End Sub


    Private Sub cmdAceptar_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAceptar.ServerClick
        '************************************************************************
        '*** Descripci�n: Elimina/Da de baja logica un nodo seleccionado dependiento si tiene o no nodos hijos
        '*** Par�metros de entrada: sender, e; Propios del evento...
        '*** Llamada desde: propia pagina  						                              
        '*** Tiempo m�ximo: 0,328seg  						                                      
        '************************************************************

        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oUser As FSNServer.User
        oUser = Session("FSN_User")
        Dim oUnidadNeg As FSNServer.UnidadNeg
        oUnidadNeg = FSWSServer.Get_Object(GetType(FSNServer.UnidadNeg))
        oUnidadNeg.Id = Me.idUNQA.value
        If accion.Value = "1" Then
            'El nodo tiene nodos hijos dados de baja. --> Realizar baja logica del nodo
            oUnidadNeg.Realizar_Deshacer_BajaLogica(oUnidadNeg.Id, True)
        ElseIf accion.Value = "2" Then
            oUnidadNeg.EliminarUnidadNeg()
        End If

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(),"init", "<script>actualizarArbol()</script>")
        oUnidadNeg = Nothing
    End Sub

    End Class

