<%@ Page Language="vb" AutoEventWireup="false" Codebehind="selectorCamposMatQA.aspx.vb" Inherits="Fullstep.FSNWeb.selectorCamposMatQA"%>
<%@ Register TagPrefix="ignav" Namespace="Infragistics.WebUI.UltraWebNavigator" Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<script type="text/javascript"><!--
		function refrescarConfiguracion()
		{
			var grid = window.opener.igtbl_getGridById("uwgMateriales")
			if (grid)
			 {
			 //Se va recorriendo el �rbol y poniendo visible o no:
			 var oTree =igtree_getTreeById("uwtCampos")
			 var nodes = oTree.getNodes()
			 var onode = nodes[0]
			 
			 onode = onode.getFirstChild()
			 while (onode)
			  {
				for (i=0;i<11;i++)
				 {
					var oCol = window.opener.igtbl_getGridById("uwgMateriales").Bands[0].Columns[i]; 
					if (oCol)
					 {	
						if (oCol.Key==onode.getDataKey())  
						 {
							if (onode.getChecked()==true)
								oCol.setHidden(false)
							else
								oCol.setHidden(true)
							break
						 }
					 }
				 }
				onode = onode.getNextSibling()
			  }
			 }	
			 window.close() 
		}
		
		function uwtCampos_InitializeTree(treeId){
			var grid = window.opener.igtbl_getGridById("uwgMateriales")
			if (grid)
			 {
			 //Se va recorriendo el �rbol y chequeando o no:
			 var oTree =igtree_getTreeById(treeId)
			 var nodes = oTree.getNodes()
			 var onode = nodes[0]
			 
			 onode = onode.getFirstChild()
			 while (onode)
			  {
				for (i=0;i<11;i++)
				 {
					var oCol = window.opener.igtbl_getGridById("uwgMateriales").Bands[0].Columns[i]; 
					if (oCol)
					 {	
						if (oCol.Key==onode.getDataKey())  
						 {
							if (oCol.getHidden()==false)
								onode.setChecked(true)
							break
						 }
					 }
				 }
				onode = onode.getNextSibling()
			  }
			 }	
		}
--></script>
	<body>
		<form id="Form1" method="post" runat="server">
			<ignav:ultrawebtree id="uwtCampos" style="Z-INDEX: 101; LEFT: 32px; POSITION: absolute; TOP: 16px" runat="server"
				width="90%" WebTreeTarget="HierarchicalTree" Height="240px" CssClass="igTree">
				<SelectedNodeStyle ForeColor="White" BackColor="Navy"></SelectedNodeStyle>
				<Levels>
					<ignav:Level Index="0"></ignav:Level>
					<ignav:Level Index="1"></ignav:Level>
				</Levels>
				<ClientSideEvents InitializeTree="uwtCampos_InitializeTree"></ClientSideEvents>
			</ignav:ultrawebtree><INPUT class="botonPMWEB" id="cmdGuardar" style="Z-INDEX: 102; LEFT: 160px; POSITION: absolute; TOP: 260px"
				type="button" value="DGuardar" runat="server" onclick ="refrescarConfiguracion()">
		</form>
	</body>
</html>
