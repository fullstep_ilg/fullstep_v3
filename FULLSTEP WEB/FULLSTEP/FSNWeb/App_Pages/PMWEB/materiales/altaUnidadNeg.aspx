<%@ Page Language="vb" AutoEventWireup="false" Codebehind="altaUnidadNeg.aspx.vb" Inherits="Fullstep.FSNWeb.altaUnidadNeg"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head runat="server">
		<title>altaUnidadNeg</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>    

	<script>
		function Cancelar()
		{
			window.close()				
		}
		function Aceptar() {
		/*
			�************************************************************
			�*** Descripci�n: Almacena el nuevo codigo y las distintas denominaciones de la UNQA introducidos.
			�*** Par�metros de entrada: ninguno    	                         	                   
			�*** Llamada desde: propia pagina  							                
			�*** Tiempo m�ximo: 0,2seg  							               
			�************************************************************
		*/
			var sIdi = document.forms["Form1"].elements["txt_Idioma"].value;
			sCod = ""
			if (window.document.forms["Form1"].elements["txtCod"]) {
				sCod = window.document.forms["Form1"].elements["txtCod"].value;
			}

			//Comprueba que se  introduzca la denominaci�n por lo menos en el idioma del usuario
			if (sCod=="")
			{
				alert(arrTextosML[1])
				return false
			}

			sDen = ""
			if (document.forms["Form1"].elements["txtDen_" + sIdi])
				sDen = document.forms["Form1"].elements["txtDen_" + sIdi].value
					
			//Comprueba que se  introduzca la denominaci�n por lo menos en el idioma del usuario
			if (sDen=="")
			{
				alert(arrTextosML[0])
				return false
			}		
			MontarFormularioUNQASubmit()

			document.forms["frmSubmit"].submit()
			window.close()
		}
	</script>
	<body>
		<form id="Form1" method="post" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
                <CompositeScript>
                    <Scripts>
                        <asp:ScriptReference Path="../alta/js/AdjacentHTML.js" />
                        <asp:ScriptReference Path="../materiales/js/jsAltaMat.js" />
                    </Scripts>
                </CompositeScript>
            </asp:ScriptManager>
			<input id="cont" type="hidden" value="0" name="cont" runat="server"> <input id="UNQA_Nodo" type="hidden" name="UNQA_Nodo" runat="server">
			<input id="UNQA_Padre" type="hidden" name="UNQA_Padre" runat="server"> <input id="ID_NODO" type="hidden" name="ID_NODO" runat="server"><input id="ID_PADRE" type="hidden" name="ID_PADRE" runat="server">
			<input id="txt_Idioma" type="hidden" name="txt_Idioma" runat="server"> <input id="accion_local" type="hidden" name="accion_local" runat="server">
			<input id="sMensaje" type="hidden" name="sMensaje" runat="server" value="" />
			<table id="Table1" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 16px" 
				cellSpacing="1" cellPadding="1" width="100%" border="0">
				<TR>
					<TD style="BORDER-BOTTOM: cornflowerblue thin solid">
						<asp:label id="lblTitulo" runat="server" Width="100%" Height="15px" CssClass="titulo"> DA�adir unidad de negocio</asp:label>
					</TD>
				</TR>
				<tr>
					<td>
						<TABLE id="Table2" height="40" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<td class="Normal" colSpan="2">
								<asp:label id="denominacion" CssClass="captionBlueSmall" Runat="server">dDenominacion nodos Padre</asp:label>
								</td>
							</TR>
							<tr>
								<td colSpan="2"></td>
							</tr>
							<TR>
								<TD style="WIDTH: 100px">
									<asp:label id="lblCodigo" runat="server" CssClass="captionBlue"> DCodigo</asp:label>
								</TD>
								<td>
									<input id="txtCod" type="text" name="txtCod" runat="server">
								</td>
							</TR>
						</TABLE>
						<table id="Table3" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD style="WIDTH: 114px" colSpan="2">
								<asp:label id="lblDen" runat="server" Width="100%" CssClass="captionBlue"> DDenominaciones</asp:label>
								</TD>
							</TR>
							<tr>
								<td colSpan="2">
									<div style="OVERFLOW: auto">
										<TABLE id="tblDenominacion" height="100%" cellSpacing="1" cellPadding="1" width="50%" border="0"
											runat="server">
											<TR>
												<TD></TD>
												<TD></TD>
											</TR>
										</TABLE>
									</div>
								</td>
							</tr>
							<TR>
								<TD colSpan="2"></TD>
							</TR>
							<TR>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td align="center">
						<INPUT class="botonPMWEB" id="cmdAceptar" onclick="Aceptar()" type="button" value="DAceptar"
							name="cmdAceptar" runat="server">
						<INPUT class="botonPMWEB" id="cmdCancelar" onclick="Cancelar()" type="button" value="DCancelar"
							name="cmdCancelar" runat="server">
					</td>
				</tr>
			</TABLE>
			&nbsp; <IFRAME id="iframeWSServer" style="Z-INDEX: 104; LEFT: 736px; VISIBILITY: hidden; WIDTH: 112px; POSITION: absolute; TOP: 24px; HEIGHT: 56px"
				name="iframeWSServer" src="../blank.htm"></IFRAME>
		</form>
	</body>
</html>
