

Public Class RealizarOperacionesOcultas
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Dim iAccion As Integer
            Dim sIDUNQas As String
            Dim bBaja As Boolean

            Dim oItem As System.Collections.Specialized.NameValueCollection
        Dim oUser As FSNServer.User = Session("FSN_User")
            Dim loop1 As Integer
            Dim arr1() As String
            Dim sRequest As String
            Dim oRequest As Object
            Dim sProve As String
            Dim iPotencial As Byte
        Dim bBajaCalidad As Boolean


            iAccion = Request("Accion")
        Dim FSQAServer As FSNServer.Root = Session("FSN_Server")
            Dim oUnidadNegQA As FSNServer.UnidadNeg
            oUnidadNegQA = FSQAServer.Get_Object(GetType(FSNServer.UnidadNeg))

        If (iAccion = 1 Or iAccion = 2) Then
            sIDUNQas = Request("idUNQAs")
            If iAccion = 1 Then
                'Deshacer la Baja logica de una unidad de negocio
                bBaja = False
            Else
                'Realizar Baja logica
                bBaja = True
            End If


            oUnidadNegQA.Realizar_Deshacer_BajaLogica(sIDUNQas, bBaja)

        ElseIf ((iAccion = 3) Or (iAccion = 4)) Then
            'Alta//Modificacion de una Unidad de negocio 
            Dim sCod, sDen As String
            Dim lUNQA, lUNQA1, lUNQA2 As Long
            Dim DS As DataSet
            Dim dt As DataTable
            Dim dtNewRow As DataRow
            Dim oRequest2 As Object
            Dim sMensaje As String = Request("sMensaje")
            DS = New DataSet

            dt = DS.Tables.Add("IDIOMAS")

            dt.Columns.Add("COD", System.Type.GetType("System.String"))
            dt.Columns.Add("DEN", System.Type.GetType("System.String"))
            dt.Columns.Add("EXISTE", System.Type.GetType("System.String"))

            oItem = Request.Form
            arr1 = oItem.AllKeys
            For loop1 = 0 To arr1.GetUpperBound(0)
                sRequest = arr1(loop1).ToString
                oRequest = sRequest.Split("_")

                Select Case oRequest(0)
                    Case "CODUNQA"
                        sCod = Request(sRequest)
                    Case "DEN"
                        sDen = Request(sRequest)
                    Case "UNQA1"
                        If Request(sRequest) <> "" Then lUNQA1 = Request(sRequest)
                    Case "UNQA2"
                        If Request(sRequest) <> "" Then lUNQA2 = Request(sRequest)
                    Case "COD"

                        dtNewRow = dt.NewRow

                        dtNewRow.Item("COD") = oRequest(1)
                        dtNewRow.Item("DEN") = Request(sRequest)
                        dtNewRow.Item("EXISTE") = Request("EXISTE_" & oRequest(1))


                        If dtNewRow.RowState = DataRowState.Detached Then
                            dt.Rows.Add(dtNewRow)
                        End If
                End Select
            Next
            oUnidadNegQA.Cod = sCod
            oUnidadNegQA.UNQA1 = lUNQA1
            oUnidadNegQA.UNQA2 = lUNQA2
            Dim lID As Long
            Dim lIdPyme As Long
            Dim bExiste As Boolean
            If iAccion = 3 Then
                lIdPyme = oUser.Pyme
                oUnidadNegQA.PYME = lIdPyme
                bExiste = oUnidadNegQA.ComprobarCodigo
                If bExiste Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "MensajeExiste", "<script>MostraMensaje('" & sMensaje & "');</script>")
                Else
                    lID = oUnidadNegQA.AņadirUnidadNegocio(DS)
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "actualizarArbol", "<script>actualizarArbol(1,'" & lUNQA1 & "','" & lUNQA2 & "','" & sCod & "','" & sDen & "','" & lID & "');</script>")
                End If


            Else
                oUnidadNegQA.Id = lUNQA1
                oUnidadNegQA.ModificarNodo(DS, False)
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "actualizarArbol", "<script>actualizarArbol(2,'" & lUNQA1 & "','" & lUNQA2 & "','" & sCod & "','" & sDen & "','" & lID & "');</script>")
            End If
       

        End If

        oUnidadNegQA = Nothing

    End Sub

    End Class


