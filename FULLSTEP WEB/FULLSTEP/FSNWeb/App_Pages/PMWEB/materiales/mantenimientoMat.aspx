<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="mantenimientoMat.aspx.vb" Inherits="Fullstep.FSNWeb.mantenimientomat"%>
<%@ Register TagPrefix="ignav" Namespace="Infragistics.WebUI.UltraWebNavigator" Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head runat="server">
		<title>materiales</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <style type="text/css">
            #whdgMateriales .ig_FullstepPMWebControl
            {
                border-width:1px;               
            }
            #whdgMateriales .igg_FullstepPMWebPager
            {
                padding:4px 0;
            }
            #whdgMateriales tr.igg_FullstepPMWebFilterRow > td
            {
                height: 22px;
                padding: 0;
            }
            #whdgMateriales .igg_FullstepPMWebItem > tr > td
            {
                padding-top:0px;
                padding-bottom:0px;
            }
        </style>
	</head>
	<script type="text/javascript">
    
		function AltaMaterial()
		{
			window.open("altaMaterialQA.aspx","_self")
		}
		/*
		''' <summary>
		''' Modificar la denominaci�n del material
		''' </summary>      
		''' <remarks>Llamada desde: cmdModifNombre.onclick ; Tiempo m�ximo: 0</remarks>*/			
		function modificarDen()
		{
			idiomas = document.getElementById("cont").value
			he = 100 + (20*idiomas)

			var oTree = igtree_getTreeById("uwtMateriales")
			var oNode=oTree.getSelectedNode()
			
			if (oNode){
				if (oNode.getLevel()==0){
					var newWindow = window.open("modificarDenMaterial.aspx?Material=" + oNode.getDataKey() ,"_blank", "width=500,height=" + he + ",status=yes,resizable=no,top=200,left=250")
                    
				} else
					alert(arrTextosML[0])
			}
			else
				alert(arrTextosML[0])
		}
			
		function actualizarDenArbol(sNewDen)
		{
			var oTree = igtree_getTreeById("uwtMateriales")
			var oNode=oTree.getSelectedNode()
			
			if (oNode)
				{
				oNode.setText(sNewDen)
				}
		}
			
		function asignarTiposCertif()
		{
			var oTree = igtree_getTreeById("uwtMateriales")
			var oNode=oTree.getSelectedNode()
			
			if (oNode){
				if (oNode.getLevel()==0){
					var newWindow = window.open("asignarTiposCertif.aspx?Material=" + oNode.getDataKey() + "&DenMaterial=" + oNode.getText() ,"_blank", "width=520,height=470,status=yes,resizable=no,top=200,left=250")
                    
				} else
					alert(arrTextosML[0])
			}
			else
				alert(arrTextosML[0])
		}
			
		function asignarMateriales()
		{
			var oTree = igtree_getTreeById("uwtMateriales")
			var oNode=oTree.getSelectedNode()
			
			if (oNode){
				if (oNode.getLevel()==0){
					var newWindow = window.open("asignarMateriales.aspx?Material=" + oNode.getDataKey() + "&DenMaterial=" + oNode.getText() ,"_blank", "width=520,height=470,status=yes,resizable=no,top=200,left=250")
                    
				} else
					alert(arrTextosML[0])
			}
			else
				alert(arrTextosML[0])
		}
			
		function eliminarMaterial()
		{
			var oTree = igtree_getTreeById("uwtMateriales")
			var oNode=oTree.getSelectedNode()
			
			if (oNode){
				if (oNode.getLevel()==0){
					var newWindow = window.open("eliminarMaterial.aspx?Material=" + oNode.getDataKey() + "&DenMaterial=" + oNode.getText() ,"_blank", "width=410,height=250,status=yes,resizable=no,top=200,left=250")
                    
				} else
					alert(arrTextosML[0])
			}
			else
				alert(arrTextosML[0])
		}
		/*
		''' Revisado por: Jbg. Fecha: 28/11/2011
		''' <summary>
		''' Llamar al selector de campos
		''' </summary>
		''' <remarks>Llamada desde: cmdSelector.onclick ; Tiempo m�ximo: 0</remarks>*/
		function selectorCampos()
		{
				var newWindow = window.open("../_common/selectorCampos.aspx?sFormulario=mantenimientoMat" ,"_blank", "width=385,height=290,status=yes,resizable=no,top=200,left=250")	 
                
		}
		
		/*''' <summary>
		''' Controla q todo este relleno y llama a la pantalla q graba en bbdd la asignaci�n de objetivos y suelos
		''' </summary>
		''' <remarks>Llamada desde: cmdAsignarObjSuelos /onclick; Tiempo m�ximo:0</remarks>*/	
		function asignarObjySuelos()
		{			
			var oTree = igtree_getTreeById("uwtMateriales")
			var oNode=oTree.getSelectedNode()
			
			if (oNode) {
				if (oNode.getLevel() == 0) 
					var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaQA2008") & "Configuracion/AsignarObjySuelos.aspx?" %>Material=" + oNode.getDataKey() + "&DenMaterial=" + oNode.getText(), "_blank", "width=800,height=500,status=yes,resizable=no,top=200,left=250");                    
				else alert(arrTextosML[0])
			} else alert(arrTextosML[0])
		}       
	</script>	
	<body>
    <script type="text/javascript">
        function ExportaraExcel() {
            __doPostBack('Excel', '');
        }
        function ExportaraPDF() {
            __doPostBack('PDF', '');
        }
        function ExportaraHTML() {
            __doPostBack('HTML', '');
        }
        function mostrarColumnas(columnas) {
            var grid = $find("<%= whdgMateriales.ClientID %>");
            for (j = 0; j < grid.get_gridView().get_columns().get_length(); j++) {
                grid.get_gridView().get_columns().get_column(j).set_hidden(true)
                for (i = 0; i < columnas.length; i++) {
                    if (grid.get_gridView().get_columns().get_column(j).get_key() == columnas[i]) {
                        grid.get_gridView().get_columns().get_columnFromKey(columnas[i]).set_hidden(false)
                    }
                }
            }
        }
        /*
        ''' <summary>
        ''' Ordena los datos por la columna indicada.
        ''' </summary>
        ''' <param name="sender">Origen del evento</param>
        ''' <param name="e">evento</param>        
        ''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>*/
        function whdgMateriales_Sorting(sender, e) {
            var sortSentence;
            sortSentence = e.get_column().get_key() + " " + (e.get_sortDirection() == 1 ? "ASC" : "DESC");
        }
        /*Paginacion*/
        function IndexChanged() {
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            var selectedIndex = dropdownlist.selectedIndex;
            __doPostBack(btnPager, dropdownlist.selectedIndex);
        }
        function FirstPage() {
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            dropdownlist.options[0].selected = true;
            __doPostBack(btnPager, 0);
        }
        function PrevPage() {
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            var selectedIndex = dropdownlist.selectedIndex;
            if (selectedIndex - 1 >= 0) {
                dropdownlist.options[selectedIndex - 1].selected = true;
                __doPostBack(btnPager, dropdownlist.selectedIndex);
            }
        }
        function NextPage() {
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            var selectedIndex = dropdownlist.selectedIndex;
            if (selectedIndex + 1 <= dropdownlist.length - 1) {
                dropdownlist.options[selectedIndex + 1].selected = true;
                __doPostBack(btnPager, dropdownlist.selectedIndex);
            }
        }
        function LastPage() {
            var dropdownlist = $('[id$=PagerPageList]')[0];
            var btnPager = $('[id$=btnPager]').attr('id');
            dropdownlist.options[dropdownlist.length - 1].selected = true;
            __doPostBack(btnPager, dropdownlist.length - 1);
        }
        /*
        ''' <summary>
        ''' Function que filtra el grid (el "embudo")
        ''' </summary>
        ''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0seg.</remarks>*/
        function whdgMateriales_Filtering(sender, e) {
            var gridFiltered = false;
            var filteredColumns, filteredCondition, filteredValue;
            var grid1erFilterValueVacio = false;
            $('[id$=hFilteredColumns]').val('');
            $('[id$=hFilteredCondition]').val('');
            $('[id$=hFilteredValue]').val('');
            for (i = 0; i < e.get_columnFilters().length; i++) {
                if (e.get_columnFilters()[i].get_condition().get_rule() != 0) {
                    gridFiltered = true;
                    filteredColumns = $('[id$=hFilteredColumns]').val();
                    filteredCondition = $('[id$=hFilteredCondition]').val();
                    filteredValue = $('[id$=hFilteredValue]').val();
                    $('[id$=hFilteredColumns]').val((filteredColumns == '' ? e.get_columnFilters()[i].get_columnKey() : filteredColumns + "#" + e.get_columnFilters()[i].get_columnKey()));
                    $('[id$=hFilteredCondition]').val((filteredCondition == '' ? e.get_columnFilters()[i].get_condition().get_rule() : filteredCondition + "#" + e.get_columnFilters()[i].get_condition().get_rule()));
                    if (grid1erFilterValueVacio) {
                        grid1erFilterValueVacio = false;
                        $('[id$=hFilteredValue]').val("#" + e.get_columnFilters()[i].get_condition().get_value());
                        filteredValue = $('[id$=hFilteredValue]').val();
                    }
                    else {
                        $('[id$=hFilteredValue]').val((filteredValue == '' ? e.get_columnFilters()[i].get_condition().get_value() : filteredValue + "#" + e.get_columnFilters()[i].get_condition().get_value()));
                    }
                    if ((e.get_columnFilters()[i].get_condition().get_value() == '') && (filteredValue == '')) {
                        grid1erFilterValueVacio = true;
                    }
                }
            }
        }
        
    </script>
		<form id="Form1" method="post" runat="server">
         <asp:ScriptManager ID="ScrMateriales" runat="server"></asp:ScriptManager>        
		<uc1:menu ID="Menu1" runat="server" OpcionMenu="Configuracion" OpcionSubMenu="Materiales">
		</uc1:menu>

		<table id="tblTitulo" border="0" width="90%" style="vertical-align:middle;">
			<tr>
				<td style="width:1px;">
					<img src="images/filtro_materiales.gif" />                
				</td>
				<td>
					<asp:Label ID="lblTitulo"
						runat="server" CssClass="titulo">DMateriales de QA</asp:Label>                               
				</td>
			</tr>
		</table>
		
		<div style="clear:both; float:left; margin-left:16px;">
			<ignav:ultrawebtree id="uwtMateriales" runat="server" cssclass="igTree" height="205px" width="464px" webtreetarget="HierarchicalTree">
			<selectednodestyle forecolor="White" backcolor="Navy"></selectednodestyle>
			<levels>
				<ignav:Level Index="0"></ignav:Level>
				<ignav:Level Index="1"></ignav:Level>
			</levels>
			</ignav:ultrawebtree>
		</div>
		
		<div style="float:left;">
			<table>
			<tr><td>
				<input language="javascript" class="botonPMWEB" id="cmdAsignarMatGS" style="width: 192px; height: 24px" 
				onclick="asignarMateriales()" type="button" value="DAsignar materiales de GS" name="cmdAsignarMatGS" runat="server">
			</td></tr>
			<tr><td>
				<input language="javascript" class="botonPMWEB" id="cmdAsignarObjSuelos"  style="width: 192px; height: 24px"
				onclick="asignarObjySuelos()" type="button" value="DAsignar Objetivos y suelos" name="cmdAsignarObjSuelos" runat="server">
			</td></tr>
			<tr><td>
				<input language="javascript" class="botonPMWEB" id="cmdAsignarCertif"  style="width: 192px; height: 24px"
				onclick="asignarTiposCertif()" type="button" value="DAsignar tipos de certificados" name="cmdAsignarCertif" runat="server">
			</td></tr>
			<tr><td>
				<input language="javascript" class="botonPMWEB" id="cmdEliminar"  style="width: 192px; height: 24px"
				onclick="eliminarMaterial()" type="button" value="DEliminar material" name="cmdEliminar" runat="server">
			</td></tr>
			<tr><td>
				<input language="javascript" class="botonPMWEB" id="cmdModifNombre"  style="width: 192px; height: 24px"
				onclick="modificarDen()" type="button" value="DModificar nombre" name="cmdModifNombre" runat="server">
			</td></tr>
			<tr><td>
				<input language="javascript" class="botonPMWEB" id="cmdAnyaMat"  style="width: 192px; height: 24px" 
				onclick="AltaMaterial()" type="button" value="DA�adir material nuevo" name="cmdAnyaMat" runat="server">
			</td></tr>
			</table>
		</div>
		
		<div style="clear:both; margin-left:16px;margin-top:10px; float:left; width: 99%;">
			<div>
			<asp:Button ID="cmdCargar" Style=""
				runat="server" CssClass="botonPMWEB" Height="24px" Width="408px" Text="DCargar materiales de GS relacionados">
			</asp:Button>
			 </div>  
            <ig:WebExcelExporter ID="wdgExcelExporter" runat="server"></ig:WebExcelExporter>
	        <ig:WebDocumentExporter ID="wdgPDFExporter" runat="server" ExportMode="Download"></ig:WebDocumentExporter> 
			<table id="tblCarga" cellspacing="1" cellpadding="1" width="98%" border="0" runat="server">
			<tr style="float:right;">
				<td style="height: 32px; width: 100%; text-align: right;">
					<table id="Table1" cellspacing="1" cellpadding="1" border="0">
						<tr>
							<td>
								<input language="javascript" class="botonPMWEB" id="cmdSelector" style="width: 150px;
									height: 24px" onclick="selectorCampos()" type="button" value="DSelector de campos"
									name="cmdSelector" runat="server">
							</td>
							<td>
								<input language="javascript" class="botonPMWEB" id="cmdGuardarConf" style="width: 150px;
									height: 24px" type="button" value="DGuardar configuraci�n" name="cmdGuardarConf"
									runat="server">
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
                <asp:UpdatePanel runat="server" ID="upPager" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:HiddenField runat="server" ID="hFilteredColumns" Value="" />
                        <asp:HiddenField runat="server" ID="hFilteredCondition" Value="" />
                        <asp:HiddenField runat="server" ID="hFilteredValue" Value="" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:Button runat="server" ID="btnPager" style="display:none;" />	
                <asp:UpdatePanel ID="updGridMateriales" ChildrenAsTriggers="false" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnPager" EventName="Click" />
			        </Triggers>
			        <ContentTemplate>
                        <ig:WebHierarchicalDataGrid ID="whdgMateriales" runat="server" Width="100%"
                            AutoGenerateBands="false" AutoGenerateColumns="false" 
                            EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="false">
                            <GroupingSettings EnableColumnGrouping="True" GroupAreaVisibility="Visible"></GroupingSettings>
                            <Behaviors>
                                <ig:Activation Enabled="true"/>
                                <ig:Filtering Alignment="Top" Enabled="true" Visibility="Visible" AnimationEnabled="false">
                                    <FilteringClientEvents DataFiltering="whdgMateriales_Filtering" />
                                </ig:Filtering>                 						    						    
						        <ig:Sorting Enabled="true" SortingMode="Multi" SortingClientEvents-ColumnSorting="whdgMateriales_Sorting"></ig:Sorting>
						        <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
						        <ig:ColumnMoving Enabled="false"></ig:ColumnMoving>
                                <ig:Paging Enabled="true" PagerAppearance="Top">
							        <PagerTemplate>
								        <div class="CabeceraBotones" style="clear:both; float:left; width:100%;"> 
									        <div style="float:left; padding:3px 0px 2px 0px; margin-left:10px;">
										        <div style="clear: both; float: left; margin-right: 5px;">
                                                    <asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" style="margin-right:2px;" />
                                                    <asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" />
                                                </div>
                                                <div style="float: left; margin-right:5px;">
                                                    <asp:Label ID="lblPage" runat="server" Text="dPage" Style="margin-right: 3px;"></asp:Label>
                                                    <asp:DropDownList ID="PagerPageList" runat="server" Style="margin-right: 3px;" onchange="return IndexChanged()" />
                                                    <asp:Label ID="lblOF" runat="server" Text="dOf" Style="margin-right: 3px;"></asp:Label>
                                                    <asp:Label ID="lblCount" runat="server" />
                                                </div>
                                                <div style="float: left;">
                                                    <asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-right:2px;" />
                                                    <asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" />
                                                </div>
									        </div>
									        <div style="float:right; margin:5px 5px 0px 0px;">
                                                <asp:LinkButton ID="linkExcel" runat="server" OnClientClick="ExportaraExcel()" style="margin-right:10px;" CssClass="ugMenu">Excel</asp:LinkButton>
                                                <asp:LinkButton ID="linkHTML" runat="server" OnClientClick="ExportaraHTML()" style="margin-right:10px;" CssClass="ugMenu">HTML</asp:LinkButton>
                                                <asp:LinkButton ID="linkPDF" runat="server" OnClientClick="ExportaraPDF()" style="margin-right:10px;" CssClass="ugMenu">PDF</asp:LinkButton>										    
									        </div>
								        </div>						
							        </PagerTemplate>
						        </ig:Paging>
                            </Behaviors>                       
                        </ig:WebHierarchicalDataGrid>   
                    </ContentTemplate>
                </asp:UpdatePanel>                     
				</td>
			</tr>
		</table>
		</div>
		<input id="cont" type="hidden" value="0" name="cont" runat="server">
		</form>
	</body>
</html>
<script type="text/javascript">
    $(document).ready(function () {
        $('[id$=ImgBtnFirst]').live('click', function () { FirstPage(); });
        $('[id$=ImgBtnPrev]').live('click', function () { PrevPage(); });
        $('[id$=ImgBtnNext]').live('click', function () { NextPage(); });
        $('[id$=ImgBtnLast]').live('click', function () { LastPage(); });

//      'Incidencia 36019: Error al hacer set_hidden para IE11
//		'Para ocultar o mostrar con javascript una columna de la grid, infragistics ha hecho que todos los elementos de cada columna compartan una misma clase css.
//		'De ese modo, mediante una serie de funciones, recupera el nombre de esa clase css y modifica el estilo display de esa clase css para que todos los elementos 
//		'que la comparten (la columna) se muestren o se oculten al mismo tiempo. 
//		'El nombre de la clase se busca en la colecci�n document.styleSheets. En esa colecci�n se guardan un listado de las clases css usadas en la p�gina (tanto internas como de enlaces externos).
//		'El los postbacks, la funci�n que agrega a esa colecci�n las clases es: Infragistics.Web.UI.ControlMain.prototype.__appendStyles
//		'Dentro de esta funci�n, para agregar las clases css en IE, se usa el m�todo addRule, que tiene 3 par�metros, uno de ellos opcional. 
//		'El primero y el segundo, obligatorios, son: selector y declaration (http://www.javascriptkit.com/domref/stylesheet.shtml)
//		'selector es el nombre de la clase css que se agrega (ej: .ig2d281703) y declaration es su contenido (ej: display:none;).
//		'El problema es que el campo declaration es obligatorio, no puede ir vac�o. De hecho, para a�adir una clase css vac�a bastar�a con el par�metro declaration llevara un punto y coma ";"
//		'Como no se ha hecho as�, el m�todo lanza un error, que los programadores de Infragistics capturan e ignoran mediante un catch(err)
//		'El resultado es que esa clase deja de estar en la colecci�n, por lo que, cuando se intenta ocultar una 
//		'columna cuya clase css ya no est� en document.styleSheets, se devuelve un error "'null' is null or not an object", 
//		'dado que la funci�n Infragistics.Web.UI.GridColumn.prototype.set_hidden no controla si el estilo existe porque 
//		'necesariamente deber�a estar presente en la colecci�n (ya que est� presente en la p�gina).
//		'Para mantener la funcionalidad de ocultar la columna, he sobreescrito la funci�n _get_hiddenStyleSheet de infragistics, 
//		'de modo que cuando una clase css vaya vac�a, el par�metro declaration sea "{};" en vez de "".
//		'As�, todas las clases css se guardan correctamente en la colecci�n document.styleSheets.
		
        if ($IG.GridColumn) {
            $IG.GridColumn.prototype._get_hiddenStyleSheet = function () {
            var css = this._get_hiddenCss();

            if (!css)// To avoid going through all of the cells, the Hidden property should be assigned on the server to generate a css class for it
            {
                css = this._owner.get_id() + "_col" + this.get_index() + "_hidden";
                /* OK 9/6/2012 119844 - Header text got merged after a column is hidden on client and an update panel on the page fires a postback. */
                css = css.toLowerCase();
                var lastSS = document.styleSheets[document.styleSheets.length - 1];
                /* DAY 3-22-11 69834- Cannot hide column on the client in IE 9 */
                if (typeof (lastSS.addRule) != "undefined" && !$util.IsIE9Plus)
                    //La funci�n addRule necesita dos par�metros, sino da error
                    //lastSS.addRule("." + css);
                    lastSS.addRule("." + css, "{};");
                else
                    lastSS.insertRule("." + css + "{}", lastSS.length);

                if (this._headerElement) {
                    this._headerElement.className += (this._headerElement.className ? " " : "") + css;

                    if (this._owner._hasHeaderLayout) {
                        /* Set the hidden css on the sizeHeaderRow */
                        var sizeHeaderRowTH = this._owner._elements["sizeHeaderRow"].childNodes;
                        for (var i = 0; i < sizeHeaderRowTH.length; i++) {
                            if (sizeHeaderRowTH[i].getAttribute("key") == this._key) {
                                sizeHeaderRowTH[i].className += (sizeHeaderRowTH[i].className ? " " : "") + css;
                                break;
                            }
                        }

                    }
                }

                var rows = this._owner.get_rows();
                for (var i = 0; i < rows.get_length(); i++) {
                    var cell = rows.get_row(i).get_cellByColumn(this);
                    var elem = cell.get_element();
                    if (elem)
                        elem.className += (elem.className ? " " : "") + css;
                }

                rows = this._owner._get_auxRows();
                for (var i = 0; i < rows.length; i++) {
                    var cell = rows[i].get_cellByColumn(this);
                    var elem = cell.get_element();
                    if (elem)
                        elem.className += (elem.className ? " " : "") + css;
                }

                /* DAY 2/24/12 102953- JS exception when hiding column for first time on the client with a footer and no header */
                if (this._footerElement)
                    this._footerElement.className += (this._footerElement.className ? " " : "") + css;

                var args = { column: this, css: css };
                this._owner._gridUtil._fireEvent(this._owner, "HideColumnCssCreated", args);

                this._set_hiddenCss(css);
            }

            return $util.getStyleSheet(css);
        }
    }
    });
</script>

