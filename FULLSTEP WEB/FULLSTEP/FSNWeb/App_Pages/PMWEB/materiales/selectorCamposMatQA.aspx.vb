
    Public Class selectorCamposMatQA
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents cmdGuardar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents uwtCampos As Infragistics.WebUI.UltraWebNavigator.UltraWebTree

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
            Dim oUser As FSNServer.User
        Dim sIdi As String = Session("FSN_User").Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If
        Dim node As Infragistics.WebUI.UltraWebNavigator.Node

        Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.SelectorCamposMatQA, sIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        oUser = Session("FSN_User")

            Me.cmdGuardar.Value = oTextos.Rows(2).Item(1)


            '************* Carga los tipos de certificados en el �rbol *************
            node = uwtCampos.Nodes.Add(oTextos.Rows(1).Item(1), "raiz")  'Carga el nodo ra�z

            Me.uwtCampos.Levels(1).LevelCheckBoxes = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.True

            Dim newNode As Infragistics.WebUI.UltraWebNavigator.Node

            'Ahora va a�adiendo los campos a mostrar:
            newNode = New Infragistics.WebUI.UltraWebNavigator.Node
            newNode.Text = oTextos.Rows(3).Item(1)  'Material de QA
            newNode.DataKey = "MATERIAL_QA"
            node.Nodes.Add(newNode)

            newNode = New Infragistics.WebUI.UltraWebNavigator.Node
            newNode.Text = oTextos.Rows(4).Item(1)  'GS1
            newNode.DataKey = "GMN1"
            node.Nodes.Add(newNode)

            newNode = New Infragistics.WebUI.UltraWebNavigator.Node
            newNode.Text = oTextos.Rows(5).Item(1)  'Den.nivel 1
            newNode.DataKey = "DEN_GMN1"
            node.Nodes.Add(newNode)

            newNode = New Infragistics.WebUI.UltraWebNavigator.Node
            newNode.Text = oTextos.Rows(6).Item(1)  'GS2
            newNode.DataKey = "GMN2"
            node.Nodes.Add(newNode)

            newNode = New Infragistics.WebUI.UltraWebNavigator.Node
            newNode.Text = oTextos.Rows(7).Item(1)  'Den.nivel 2
            newNode.DataKey = "DEN_GMN2"
            node.Nodes.Add(newNode)

            newNode = New Infragistics.WebUI.UltraWebNavigator.Node
            newNode.Text = oTextos.Rows(8).Item(1)  'GS3
            newNode.DataKey = "GMN3"
            node.Nodes.Add(newNode)

            newNode = New Infragistics.WebUI.UltraWebNavigator.Node
            newNode.Text = oTextos.Rows(9).Item(1)  'Den.nivel 3
            newNode.DataKey = "DEN_GMN3"
            node.Nodes.Add(newNode)

            newNode = New Infragistics.WebUI.UltraWebNavigator.Node
            newNode.Text = oTextos.Rows(10).Item(1)  'GS4
            newNode.DataKey = "GMN4"
            node.Nodes.Add(newNode)

            newNode = New Infragistics.WebUI.UltraWebNavigator.Node
            newNode.Text = oTextos.Rows(11).Item(1)  'Den.nivel 4
            newNode.DataKey = "DEN_GMN4"
            node.Nodes.Add(newNode)

            newNode = New Infragistics.WebUI.UltraWebNavigator.Node
            newNode.Text = oTextos.Rows(12).Item(1)  'Tipos de certificado
            newNode.DataKey = "CERTIFICADOS"
            node.Nodes.Add(newNode)

            Me.uwtCampos.DataKeyOnClient = True
            Me.uwtCampos.ExpandAll()

            oTextos = Nothing
            oDict = Nothing
            oUser = Nothing
        End Sub

    End Class

