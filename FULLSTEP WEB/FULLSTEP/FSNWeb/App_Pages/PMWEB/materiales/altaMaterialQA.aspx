<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="altaMaterialQA.aspx.vb" Inherits="Fullstep.FSNWeb.altaMaterialQA" EnableSessionState="True" enableViewState="False"%>
<%@ Register TagPrefix="ignav" Namespace="Infragistics.WebUI.UltraWebNavigator" Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
	</head>		

	<style type="text/css">
		html,body
		{
			height:100%!important;
		}
	</style>
	<script type="text/javascript"><!--
		function HistoryHaciaAtras(num) {
			//Va hacia atras tantas paginas como indique el parametro num
			window.history.go(num);
		}
		function Cancelar() {
			window.open("mantenimientoMat.aspx", "_self");
		}

		/*
		''' <summary>
		''' Realiza la grabaci�n en bbdd de un nuevo material de QA
		''' </summary>
		''' <remarks>Llamada desde: cmdAceptar; Tiempo m�ximo: 0,3</remarks>*/
		function CrearMaterial() {
			var sIdi = document.forms["frmAlta"].elements["Idioma"].value

			//Comprueba que se  introduzca la denominaci�n por lo menos en el idioma del usuario
			if (document.forms["frmAlta"].elements["txtDen_" + sIdi].value == "") {
				alert(arrTextosML[0])
				return false
			}
			//comprueba que se seleccione al menos un material:
			if (arrGMN1.length == 0 && arrGMN2.length == 0 && arrGMN3.length == 0 && arrGMN4.length == 0) {
				alert(arrTextosML[2])
				return false
			}

			MontarFormularioMatSubmit()

			document.forms["frmSubmit"].elements["GEN_Accion"].value = "alta"
			document.forms["frmSubmit"].submit()
		}

		function imposibleAsignarMatQA(DenMatQA, treeId, nodeId) {
			alert(arrTextosML[3] + '\r' + DenMatQA);
			var oTree = igtree_getTreeById(treeId);
			var node = igtree_getNodeById(nodeId);
			s = oTree.Events.NodeChecked[0];
			oTree.Events.NodeChecked[0] = "";
			node.setChecked(false);
			oTree.Events.NodeChecked[0] = s;
		}

		function asignarMaterialQA(treeId, nodeId) {
			var oTree = igtree_getTreeById(treeId)

			//inhabilita el evento nodeclick para que no se lance cuando chequeemos los padres o hijos
			s = oTree.Events.NodeChecked[0]
			oTree.Events.NodeChecked[0] = ""

			var node = igtree_getNodeById(nodeId)

			//Guarda en el array de materiales:
			AnyadirArray(node)

			//Se checkean todos los hijos
			if (node.hasChildren() == true) {
				var onodeChild1 = node.getFirstChild()
				while (onodeChild1) {
					onodeChild1.setChecked(true)
					AnyadirArray(onodeChild1)

					if (onodeChild1.hasChildren() == true) {
						var onodeChild2 = onodeChild1.getFirstChild()
						while (onodeChild2) {
							onodeChild2.setChecked(true)
							AnyadirArray(onodeChild2)

							if (onodeChild2.hasChildren() == true) {
								var onodeChild3 = onodeChild2.getFirstChild()
								while (onodeChild3) {
									onodeChild3.setChecked(true)
									AnyadirArray(onodeChild3)

									onodeChild3 = onodeChild3.getNextSibling()
								}
							}
							onodeChild2 = onodeChild2.getNextSibling()
						}
					}
					onodeChild1 = onodeChild1.getNextSibling()
				}
			}
			oTree.Events.NodeChecked[0] = s
		}

		var arrCertificados = new Array();
		var arrGMN1 = new Array();
		var arrGMN2 = new Array();
		var arrGMN3 = new Array();
		var arrGMN4 = new Array();

		function uwtTiposCertif_NodeChecked(treeId, nodeId, bChecked) {
			var node = igtree_getNodeById(nodeId);

			if (bChecked == true) {
				//lo a�ade al array   
				arrCertificados[arrCertificados.length] = node.getTag()
			}
			else
			//lo quita del array
			{
				for (i = 0; i < arrCertificados.length; i++) {
					if (arrCertificados[i] == node.getTag()) {
						arrCertificados.splice(i, 1)
						break;
					}
				}
			}
		}

		/*
		''' <summary>
		''' Meter en el array el material de gs seleccionado al material QA
		''' </summary>
		''' <param name="treeId">Id del arbol</param>
		''' <param name="nodeId">id del nodo</param>   
		''' <param name="bChecked">Si un nodo a sido checkeado o no</param>       
		''' <remarks>Llamada desde=Evento que salta cuando chequeamos un material; Tiempo m�ximo</remarks>
		*/
		function uwtMateriales_NodeChecked(treeId, nodeId, bChecked) {
			var oTree = igtree_getTreeById(treeId)
			var node = igtree_getNodeById(nodeId)

			if (bChecked == true) {
				//Si se ha seleccionado un material 1� comprueba que no exista en BD para otro material de QA:
				var iLevel = node.getLevel()
				var GMN1
				var GMN2
				var GMN3
				var GMN4

				switch (iLevel) {
					case 0:
						GMN1 = node.getDataKey()
						GMN2 = ""
						GMN3 = ""
						GMN4 = ""
						break;
					case 1:
						GMN1 = node.getParent().getDataKey()
						GMN2 = node.getDataKey()
						GMN3 = ""
						GMN4 = ""
						break;
					case 2:
						GMN1 = node.getParent().getParent().getDataKey()
						GMN2 = node.getParent().getDataKey()
						GMN3 = node.getDataKey()
						GMN4 = ""
						break;
					case 3:
						GMN1 = node.getParent().getParent().getParent().getDataKey()
						GMN2 = node.getParent().getParent().getDataKey()
						GMN3 = node.getParent().getDataKey()
						GMN4 = node.getDataKey()
						break;
				}
				asignarMaterialQA(treeId, nodeId)
			}
			else {
				//inhabilita el evento nodeclick para que no se lance cuando chequeemos los padres o hijos
				s = oTree.Events.NodeChecked[0]
				oTree.Events.NodeChecked[0] = ""

				//Elimina del array:
				EliminarArray(node)

				//se deschequean los padres
				var onodeParent = node.getParent()
				while (onodeParent) {
					onodeParent.setChecked(false)
					//Elimina del array:
					EliminarArray(onodeParent)
					onodeParent = onodeParent.getParent()
				}

				//se deschequean los hijos
				if (node.hasChildren() == true) {
					var onodeChild1 = node.getFirstChild()
					while (onodeChild1) {
						onodeChild1.setChecked(false)
						//Elimina del array:
						EliminarArray(onodeChild1)

						if (onodeChild1.hasChildren() == true) {
							var onodeChild2 = onodeChild1.getFirstChild()
							while (onodeChild2) {
								onodeChild2.setChecked(false)
								//Elimina del array:
								EliminarArray(onodeChild2)

								if (onodeChild2.hasChildren() == true) {
									var onodeChild3 = onodeChild2.getFirstChild()
									while (onodeChild3) {
										onodeChild3.setChecked(false)
										//Elimina del array:
										EliminarArray(onodeChild3)
										onodeChild3 = onodeChild3.getNextSibling()
									}
								}
								onodeChild2 = onodeChild2.getNextSibling()
							}
						}

						onodeChild1 = onodeChild1.getNextSibling()
					}
				}
				oTree.Events.NodeChecked[0] = s
			}
		}


		function EliminarArray(node) {
			var iLevel = node.getLevel()

			//Elimina del array
			switch (iLevel) {
				case 0: //GMN1
					for (i = 0; i < arrGMN1.length; i++) {
						if (arrGMN1[i] == node.getDataKey()) {
							arrGMN1.splice(i, 1)
							break;
						}
					}
					break;
				case 1: //GMN2
					for (i = 0; i < arrGMN2.length; i++) {
						if (arrGMN2[i] == node.getParent().getDataKey() + "&&&" + node.getDataKey()) {
							arrGMN2.splice(i, 1)
							break;
						}
					}
					break;
				case 2: //GMN3  
					for (i = 0; i < arrGMN3.length; i++) {
						if (arrGMN3[i] == node.getParent().getParent().getDataKey() + "&&&" + node.getParent().getDataKey() + "&&&" + node.getDataKey()) {
							arrGMN3.splice(i, 1)
							break;
						}
					}
					break;
				case 3: //GMN4   
					for (i = 0; i < arrGMN4.length; i++) {
						if (arrGMN4[i] == node.getParent().getParent().getParent().getDataKey() + "&&&" + node.getParent().getParent().getDataKey() + "&&&" + node.getParent().getDataKey() + "&&&" + node.getDataKey()) {
							arrGMN4.splice(i, 1)
							break;
						}
					}
					break;
			}
		}

		function AnyadirArray(node) {
			//Guarda en el array de materiales:
			var iLevel = node.getLevel()

			switch (iLevel) {
				case 0: //GMN1
					arrGMN1[arrGMN1.length] = node.getDataKey()
					break;
				case 1: //GMN2
					arrGMN2[arrGMN2.length] = node.getParent().getDataKey() + "&&&" + node.getDataKey()
					break;
				case 2: //GMN3
					arrGMN3[arrGMN3.length] = node.getParent().getParent().getDataKey() + "&&&" + node.getParent().getDataKey() + "&&&" + node.getDataKey()
					break;
				case 3: //GMN4
					arrGMN4[arrGMN4.length] = node.getParent().getParent().getParent().getDataKey() + "&&&" + node.getParent().getParent().getDataKey() + "&&&" + node.getParent().getDataKey() + "&&&" + node.getDataKey()
					break;
			}

		}		 
--></script>	
	<body style="height:100%;">
		<form id="frmAlta" method="post" runat="server" style="height:100%;">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
                <CompositeScript>
                    <Scripts>
                        <asp:ScriptReference Path="../alta/js/AdjacentHTML.js" />
                        <asp:ScriptReference Path="../materiales/js/jsAltaMat.js" />
                    </Scripts>
                </CompositeScript>
            </asp:ScriptManager>
			<iframe id="iframeWSServer" style="display:none;" name="iframeWSServer" src="../blank.htm"></iframe>
			<input id="Idioma" type="hidden" size="1" name="Idioma" runat="server"/>
			<div style="clear:both; float:left; width:100%;">
				<uc1:menu id="Menu1" runat="server" OpcionMenu="Configuracion" OpcionSubMenu="Materiales"></uc1:menu>
			</div>
			<div style="clear:both; float:left; margin-left:15px;">            
				<img alt="" src="images/filtro_materiales.gif" style="vertical-align:middle;"/>
				<asp:label id="lblTitulo" runat="server" CssClass="titulo">DAlta de material de QA</asp:label>
			</div>
			<div style="float:right; margin-top:10px; margin-right:10px;">
				<a href="javascript:HistoryHaciaAtras(-1)"><asp:Label id="lblVolver" runat="server" CssClass="volver">DVolver</asp:Label></a>
			</div>
			<div style="clear:both; float:left; margin-left:15px; margin-top:10px;">
				<table runat="server" id="tblDenominacion" cellspacing="2" cellpadding="2" border="0" style="width:100%;">
					<tr><td>
						<asp:label id="lblDen" runat="server" CssClass="captionBlueSmallBold">DDenominaciones</asp:label>
					</td></tr>
				</table>
			</div>
			<div style="clear:both; float:left; width:100%; margin-left:15px; margin-top:10px; height:55%;">
				<div style="float:left; width:45%; margin-right:2%; height:100%;">
					<div style="float:left; width:100%;">
						<asp:label id="lblCertif" runat="server" CssClass="captionBlueSmallBold">DSeleccione los tipos de certificados relacionados</asp:label>
					</div>
					<div style="float:left; width:100%; margin-top:5px; height:92%; padding-bottom:3%; overflow:hidden;">
						<ignav:ultrawebtree id="uwtTiposCertif" runat="server" CssClass="igTree" Height="100%" width="96%"
							WebTreeTarget="HierarchicalTree">
							<SelectedNodeStyle ForeColor="White" BackColor="Navy"></SelectedNodeStyle>
							<Levels>
								<ignav:Level Index="0"></ignav:Level>
								<ignav:Level Index="1"></ignav:Level>
							</Levels>
							<ClientSideEvents NodeChecked="uwtTiposCertif_NodeChecked"></ClientSideEvents>
						</ignav:ultrawebtree>
					</div>
				</div>
				<div style="float:left; width:45%; margin-right:2%; height:100%;">
					<div style="float:left; width:100%;">
						<asp:label id="lblMat" runat="server" CssClass="captionBlueSmallBold">DSeleccione los materiales de GS relacionados</asp:label>
					</div>
					<div style="float:left; width:100%; margin-top:5px; height:92%; padding-bottom:3%; overflow:hidden;">
						<ignav:ultrawebtree id="uwtMateriales" runat="server" CssClass="igTree" Height="100%" Width="96%" WebTreeTarget="HierarchicalTree">
							<SelectedNodeStyle ForeColor="White" BackColor="Navy"></SelectedNodeStyle>
							<Levels>
								<ignav:Level Index="0"></ignav:Level>
								<ignav:Level Index="1"></ignav:Level>
							</Levels>
							<ClientSideEvents NodeChecked="uwtMateriales_NodeChecked"></ClientSideEvents>
						</ignav:ultrawebtree>
					</div>
				</div>            
			</div>
			<div style="clear:both; float:left; width:100%; text-align:center; margin-top:2%;">
				<input class="botonPMWEB" id="cmdAceptar" onclick="return CrearMaterial()" type="button" value="DAceptar" runat="server"/>
				<input class="botonPMWEB" id="cmdCancelar" onclick="Cancelar()" type="button" value="DCancelar" runat="server"/>
			</div>
		</form>
	</body>
</html>
