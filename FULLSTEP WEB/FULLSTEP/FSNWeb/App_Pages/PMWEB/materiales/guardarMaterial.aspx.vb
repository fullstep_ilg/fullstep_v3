
    Public Class guardarMaterial
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

    ''' <summary>
    ''' Realiza las operaciones de BBDD internamente llamando desde otra pagina.
    ''' </summary>
    ''' <param name="sender">propios del evento</param>
    ''' <param name="e">propios del evento</param>        
    ''' <remarks>Llamada desde=altaMaterailQA.aspx (jsAltaMat.js); Tiempo m�ximo>=1seg.</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim ds As DataSet
        Dim oMaterialQA As FSNServer.MaterialQA

        Dim sIdi As String = FSNUser.Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If
        Dim sAccion As String


        sAccion = Request("GEN_Accion")

        oMaterialQA = FSNServer.Get_Object(GetType(FSNServer.MaterialQA))

        Select Case sAccion
            Case "alta"
                ds = GenerarDatasetAlta(Request)
                oMaterialQA.Create(ds, FSNUser.Pyme)
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "init", "<script>window.open(""" & "mantenimientoMat.aspx" & """,""fraWSMain"")</script>")
            Case "modCertif"
                ds = GenerarDataSetModifCertif(Request)
                oMaterialQA.ID = Request("GEN_Material")
                oMaterialQA.ModificarCertificados(ds)
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "init", "<script>window.parent.actualizarArbol()</script>")
            Case "modifMat"
                ds = GenerarDataSetModifGMN(Request)
                oMaterialQA.ID = Request("GEN_Material")
                oMaterialQA.ModificarAsignacionMatGS(ds)
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "init", "<script>window.parent.close()</script>")
        End Select

        oMaterialQA = Nothing

    End Sub

    Private Function GenerarDatasetAlta(ByVal Request As System.Web.HttpRequest) As DataSet
        Dim oItem As System.Collections.Specialized.NameValueCollection
        Dim oUser As FSNServer.User = Session("FSN_User")
        Dim loop1 As Integer
        Dim arr1() As String
        Dim DS As DataSet
        Dim dtMaterial As DataTable
        Dim dtCertif As DataTable
        Dim dtGMN1 As DataTable
        Dim dtGMN2 As DataTable
        Dim dtGMN3 As DataTable
        Dim dtGMN4 As DataTable
        Dim keysCertif(0) As DataColumn
        Dim findCertif(0) As Object
        Dim keysGMN1(0) As DataColumn
        Dim findGMN1(0) As Object
        Dim keysGMN2(1) As DataColumn
        Dim findGMN2(1) As Object
        Dim keysGMN3(2) As DataColumn
        Dim findGMN3(2) As Object
        Dim keysGMN4(3) As DataColumn
        Dim findGMN4(3) As Object
        Dim oIdiomas As FSNServer.Idiomas
        Dim oIdioma As FSNServer.Idioma
        Dim FSPMServer As FSNServer.Root = Session("FSN_Server")
        Dim sRequest As String
        Dim oRequest As Object
        Dim dtNewRow As DataRow
        Dim bInsertar As Boolean

        oIdiomas = FSNServer.Get_Object(GetType(FSNServer.Idiomas))
        oIdiomas.Load()

        DS = New DataSet

        'Crea la tabla temporal para el nuevo material del QA:
        dtMaterial = DS.Tables.Add("TEMP_MATERIALES_QA")
        dtMaterial.Columns.Add("ID", System.Type.GetType("System.Int32"))
        For Each oIdioma In oIdiomas.Idiomas
            dtMaterial.Columns.Add("DEN_" & oIdioma.Cod, System.Type.GetType("System.String"))
            dtMaterial.Columns("DEN_" & oIdioma.Cod).AllowDBNull = True
        Next

        'Crea la tabla temporal para los certificados asignados al material:
        dtCertif = DS.Tables.Add("TEMP_MAT_CERTIFICADOS")
        dtCertif.Columns.Add("TIPO_CERTIFICADO", System.Type.GetType("System.Int32"))
        keysCertif(0) = dtCertif.Columns("TIPO_CERTIFICADO")
        dtCertif.PrimaryKey = keysCertif

        'Crea la tabla temporal para los gmn1 del GS asignados al material:
        dtGMN1 = DS.Tables.Add("TEMP_GMN1_MATQA")
        dtGMN1.Columns.Add("GMN1", System.Type.GetType("System.String"))
        keysGMN1(0) = dtGMN1.Columns("GMN1")
        dtGMN1.PrimaryKey = keysGMN1

        'Crea la tabla temporal para los gmN2 del GS asignados al material:
        dtGMN2 = DS.Tables.Add("TEMP_GMN2_MATQA")
        dtGMN2.Columns.Add("GMN1", System.Type.GetType("System.String"))
        dtGMN2.Columns.Add("GMN2", System.Type.GetType("System.String"))
        keysGMN2(0) = dtGMN2.Columns("GMN1")
        keysGMN2(1) = dtGMN2.Columns("GMN2")
        dtGMN2.PrimaryKey = keysGMN2

        'Crea la tabla temporal para los gmN3 del GS asignados al material:
        dtGMN3 = DS.Tables.Add("TEMP_GMN3_MATQA")
        dtGMN3.Columns.Add("GMN1", System.Type.GetType("System.String"))
        dtGMN3.Columns.Add("GMN2", System.Type.GetType("System.String"))
        dtGMN3.Columns.Add("GMN3", System.Type.GetType("System.String"))
        keysGMN3(0) = dtGMN3.Columns("GMN1")
        keysGMN3(1) = dtGMN3.Columns("GMN2")
        keysGMN3(2) = dtGMN3.Columns("GMN3")
        dtGMN3.PrimaryKey = keysGMN3

        'Crea la tabla temporal para los gmN4 del GS asignados al material:
        dtGMN4 = DS.Tables.Add("TEMP_GMN4_MATQA")
        dtGMN4.Columns.Add("GMN1", System.Type.GetType("System.String"))
        dtGMN4.Columns.Add("GMN2", System.Type.GetType("System.String"))
        dtGMN4.Columns.Add("GMN3", System.Type.GetType("System.String"))
        dtGMN4.Columns.Add("GMN4", System.Type.GetType("System.String"))
        keysGMN4(0) = dtGMN4.Columns("GMN1")
        keysGMN4(1) = dtGMN4.Columns("GMN2")
        keysGMN4(2) = dtGMN4.Columns("GMN3")
        keysGMN4(3) = dtGMN4.Columns("GMN4")
        dtGMN4.PrimaryKey = keysGMN4


        'Crea la nueva fila del material de QA:
        If Request("GEN_Material") = Nothing Then
            dtNewRow = dtMaterial.NewRow
            For Each oIdioma In oIdiomas.Idiomas
                If Request("GEN_txt" & oIdioma.Cod) = "" Then
                    dtNewRow.Item("DEN_" & oIdioma.Cod) = System.DBNull.Value
                Else
                    dtNewRow.Item("DEN_" & oIdioma.Cod) = Request("GEN_txt" & oIdioma.Cod)
                End If
            Next
            dtMaterial.Rows.Add(dtNewRow)
        End If


        oItem = Request.Form
        arr1 = oItem.AllKeys
        For loop1 = 0 To arr1.GetUpperBound(0)
            sRequest = arr1(loop1).ToString

            oRequest = sRequest.Split("_")

            bInsertar = False

            Select Case oRequest(0)

                Case "CERTIF"
                    findCertif(0) = oRequest(1)
                    dtNewRow = dtCertif.Rows.Find(findCertif)
                    If dtNewRow Is Nothing Then
                        dtNewRow = dtCertif.NewRow
                    End If

                    dtNewRow.Item("TIPO_CERTIFICADO") = oRequest(1)

                    If dtNewRow.RowState = DataRowState.Detached Then
                        dtCertif.Rows.Add(dtNewRow)
                    End If

                Case "GMN1"
                    findGMN1(0) = oRequest(1)
                    dtNewRow = dtGMN1.Rows.Find(findGMN1)
                    If dtNewRow Is Nothing Then
                        dtNewRow = dtGMN1.NewRow
                    End If

                    dtNewRow.Item("GMN1") = oRequest(1)

                    If dtNewRow.RowState = DataRowState.Detached Then
                        dtGMN1.Rows.Add(dtNewRow)
                    End If

                Case "GMN2"
                    'Si est� en las la tabla de los GMN1 no se carga en la de gmn2
                    findGMN1(0) = Split(oRequest(1), "&&&")(0)
                    If dtGMN1.Rows.Find(findGMN1) Is Nothing Then
                        findGMN2(0) = Split(oRequest(1), "&&&")(0)
                        findGMN2(1) = Split(oRequest(1), "&&&")(1)

                        dtGMN1.Rows.Find(findGMN1)

                        dtNewRow = dtGMN2.Rows.Find(findGMN2)
                        If dtNewRow Is Nothing Then
                            dtNewRow = dtGMN2.NewRow
                        End If

                        dtNewRow.Item("GMN1") = Split(oRequest(1), "&&&")(0)
                        dtNewRow.Item("GMN2") = Split(oRequest(1), "&&&")(1)

                        If dtNewRow.RowState = DataRowState.Detached Then
                            dtGMN2.Rows.Add(dtNewRow)
                        End If
                    End If

                Case "GMN3"
                    'Si no est� insertado en GMN2 ni GMN1 guarda en GMN3
                    findGMN2(0) = Split(oRequest(1), "&&&")(0)
                    findGMN2(1) = Split(oRequest(1), "&&&")(1)

                    If dtGMN2.Rows.Find(findGMN2) Is Nothing Then
                        'Comprueba que no est� en GMN1
                        findGMN1(0) = Split(oRequest(1), "&&&")(0)
                        If dtGMN1.Rows.Find(findGMN1) Is Nothing Then
                            bInsertar = True
                        End If
                    End If

                    If bInsertar = True Then
                        findGMN3(0) = Split(oRequest(1), "&&&")(0)
                        findGMN3(1) = Split(oRequest(1), "&&&")(1)
                        findGMN3(2) = Split(oRequest(1), "&&&")(2)

                        dtNewRow = dtGMN3.Rows.Find(findGMN3)
                        If dtNewRow Is Nothing Then
                            dtNewRow = dtGMN3.NewRow
                        End If

                        dtNewRow.Item("GMN1") = Split(oRequest(1), "&&&")(0)
                        dtNewRow.Item("GMN2") = Split(oRequest(1), "&&&")(1)
                        dtNewRow.Item("GMN3") = Split(oRequest(1), "&&&")(2)

                        If dtNewRow.RowState = DataRowState.Detached Then
                            dtGMN3.Rows.Add(dtNewRow)
                        End If
                    End If

                Case "GMN4"
                    'Si no est� insertado en GMN3 ni GMN2 ni GMN1 guarda en GMN4
                    findGMN3(0) = Split(oRequest(1), "&&&")(0)
                    findGMN3(1) = Split(oRequest(1), "&&&")(1)
                    findGMN3(2) = Split(oRequest(1), "&&&")(2)
                    If dtGMN3.Rows.Find(findGMN3) Is Nothing Then
                        findGMN2(0) = Split(oRequest(1), "&&&")(0)
                        findGMN2(1) = Split(oRequest(1), "&&&")(1)
                        If dtGMN2.Rows.Find(findGMN2) Is Nothing Then
                            findGMN1(0) = Split(oRequest(1), "&&&")(0)
                            If dtGMN1.Rows.Find(findGMN1) Is Nothing Then
                                bInsertar = True
                            End If
                        End If
                    End If

                    If bInsertar = True Then
                        findGMN4(0) = Split(oRequest(1), "&&&")(0)
                        findGMN4(1) = Split(oRequest(1), "&&&")(1)
                        findGMN4(2) = Split(oRequest(1), "&&&")(2)
                        findGMN4(3) = Split(oRequest(1), "&&&")(3)

                        dtNewRow = dtGMN4.Rows.Find(findGMN4)
                        If dtNewRow Is Nothing Then
                            dtNewRow = dtGMN4.NewRow
                        End If

                        dtNewRow.Item("GMN1") = Split(oRequest(1), "&&&")(0)
                        dtNewRow.Item("GMN2") = Split(oRequest(1), "&&&")(1)
                        dtNewRow.Item("GMN3") = Split(oRequest(1), "&&&")(2)
                        dtNewRow.Item("GMN4") = Split(oRequest(1), "&&&")(3)

                        If dtNewRow.RowState = DataRowState.Detached Then
                            dtGMN4.Rows.Add(dtNewRow)
                        End If
                    End If
            End Select

        Next loop1


        oIdiomas = Nothing

        Return DS

    End Function

    Private Function GenerarDataSetModifCertif(ByVal Request As System.Web.HttpRequest) As DataSet
        Dim oItem As System.Collections.Specialized.NameValueCollection
        Dim oUser As FSNServer.User = Session("FSN_User")
        Dim loop1 As Integer
        Dim arr1() As String
        Dim DS As DataSet
        Dim dtNewCertif As DataTable
        Dim dtDeleteCertif As DataTable
        Dim keysNewCertif(1) As DataColumn
        Dim findNewCertif(1) As Object
        Dim keysDeleteCertif(1) As DataColumn
        Dim findDeleteCertif(1) As Object
        Dim FSPMServer As FSNServer.Root = Session("FSN_Server")
        Dim sRequest As String
        Dim oRequest As Object
        Dim dtNewRow As DataRow
        Dim lMaterial As Long

        lMaterial = Request("GEN_Material")


        DS = New DataSet

        'Crea la tabla temporal para los nuevos certificados asignados al material:
        dtNewCertif = DS.Tables.Add("TEMP_MAT_NEW_CERTIFICADOS")
        dtNewCertif.Columns.Add("TIPO_CERTIFICADO", System.Type.GetType("System.Int32"))
        dtNewCertif.Columns.Add("MATERIAL", System.Type.GetType("System.Int32"))
        keysNewCertif(0) = dtNewCertif.Columns("MATERIAL")
        keysNewCertif(1) = dtNewCertif.Columns("TIPO_CERTIFICADO")
        dtNewCertif.PrimaryKey = keysNewCertif

        'Crea la tabla temporal para los nuevos certificados asignados al material:
        dtDeleteCertif = DS.Tables.Add("TEMP_MAT_DELETED_CERTIFICADOS")
        dtDeleteCertif.Columns.Add("TIPO_CERTIFICADO", System.Type.GetType("System.Int32"))
        dtDeleteCertif.Columns.Add("MATERIAL", System.Type.GetType("System.Int32"))
        keysDeleteCertif(0) = dtDeleteCertif.Columns("MATERIAL")
        keysDeleteCertif(1) = dtDeleteCertif.Columns("TIPO_CERTIFICADO")
        dtDeleteCertif.PrimaryKey = keysDeleteCertif


        oItem = Request.Form
        arr1 = oItem.AllKeys
        For loop1 = 0 To arr1.GetUpperBound(0)
            sRequest = arr1(loop1).ToString

            oRequest = sRequest.Split("_")

            Select Case oRequest(0)

                Case "NEWCERTIF"
                    findNewCertif(0) = lMaterial
                    findNewCertif(1) = oRequest(1)
                    dtNewRow = dtNewCertif.Rows.Find(findNewCertif)
                    If dtNewRow Is Nothing Then
                        dtNewRow = dtNewCertif.NewRow
                    End If

                    dtNewRow.Item("MATERIAL") = lMaterial
                    dtNewRow.Item("TIPO_CERTIFICADO") = oRequest(1)

                    If dtNewRow.RowState = DataRowState.Detached Then
                        dtNewCertif.Rows.Add(dtNewRow)
                    End If

                Case "DELETECERTIF"
                    findDeleteCertif(0) = lMaterial
                    findDeleteCertif(1) = oRequest(1)
                    dtNewRow = dtNewCertif.Rows.Find(findDeleteCertif)
                    If dtNewRow Is Nothing Then
                        dtNewRow = dtDeleteCertif.NewRow
                    End If

                    dtNewRow.Item("MATERIAL") = lMaterial
                    dtNewRow.Item("TIPO_CERTIFICADO") = oRequest(1)

                    If dtNewRow.RowState = DataRowState.Detached Then
                        dtDeleteCertif.Rows.Add(dtNewRow)
                    End If

            End Select

        Next loop1

        Return DS

    End Function

    Private Function GenerarDataSetModifGMN(ByVal Request As System.Web.HttpRequest) As DataSet
        Dim oItem As System.Collections.Specialized.NameValueCollection
        Dim oUser As FSNServer.User = Session("FSN_User")
        Dim loop1 As Integer
        Dim arr1() As String
        Dim DS As DataSet
        Dim dtNewGMN1 As DataTable
        Dim dtNewGMN2 As DataTable
        Dim dtNewGMN3 As DataTable
        Dim dtNewGMN4 As DataTable
        Dim dtDeleteGMN1 As DataTable
        Dim dtDeleteGMN2 As DataTable
        Dim dtDeleteGMN3 As DataTable
        Dim dtDeleteGMN4 As DataTable
        Dim keysGMN1(0) As DataColumn
        Dim findGMN1(0) As Object
        Dim keysGMN2(1) As DataColumn
        Dim findGMN2(1) As Object
        Dim keysGMN3(2) As DataColumn
        Dim findGMN3(2) As Object
        Dim keysGMN4(3) As DataColumn
        Dim findGMN4(3) As Object
        Dim FSPMServer As FSNServer.Root = Session("FSN_Server")
        Dim sRequest As String
        Dim oRequest As Object
        Dim dtNewRow As DataRow
        Dim lMaterial As Long
        Dim bInsertar As Boolean


        lMaterial = Request("GEN_Material")

        DS = New DataSet

        'Crea la tabla temporal para los nuevos gmn1 del GS asignados al material:
        dtNewGMN1 = DS.Tables.Add("TEMP_NEW_GMN1_MATQA")
        dtNewGMN1.Columns.Add("GMN1", System.Type.GetType("System.String"))
        keysGMN1(0) = dtNewGMN1.Columns("GMN1")
        dtNewGMN1.PrimaryKey = keysGMN1

        'Crea la tabla temporal para los nuevos gmN2 del GS asignados al material:
        dtNewGMN2 = DS.Tables.Add("TEMP_NEW_GMN2_MATQA")
        dtNewGMN2.Columns.Add("GMN1", System.Type.GetType("System.String"))
        dtNewGMN2.Columns.Add("GMN2", System.Type.GetType("System.String"))
        keysGMN2(0) = dtNewGMN2.Columns("GMN1")
        keysGMN2(1) = dtNewGMN2.Columns("GMN2")
        dtNewGMN2.PrimaryKey = keysGMN2

        'Crea la tabla temporal para los nuevos gmN3 del GS asignados al material:
        dtNewGMN3 = DS.Tables.Add("TEMP_NEW_GMN3_MATQA")
        dtNewGMN3.Columns.Add("GMN1", System.Type.GetType("System.String"))
        dtNewGMN3.Columns.Add("GMN2", System.Type.GetType("System.String"))
        dtNewGMN3.Columns.Add("GMN3", System.Type.GetType("System.String"))
        keysGMN3(0) = dtNewGMN3.Columns("GMN1")
        keysGMN3(1) = dtNewGMN3.Columns("GMN2")
        keysGMN3(2) = dtNewGMN3.Columns("GMN3")
        dtNewGMN3.PrimaryKey = keysGMN3

        'Crea la tabla temporal para los nuevos gmN4 del GS asignados al material:
        dtNewGMN4 = DS.Tables.Add("TEMP_NEW_GMN4_MATQA")
        dtNewGMN4.Columns.Add("GMN1", System.Type.GetType("System.String"))
        dtNewGMN4.Columns.Add("GMN2", System.Type.GetType("System.String"))
        dtNewGMN4.Columns.Add("GMN3", System.Type.GetType("System.String"))
        dtNewGMN4.Columns.Add("GMN4", System.Type.GetType("System.String"))
        keysGMN4(0) = dtNewGMN4.Columns("GMN1")
        keysGMN4(1) = dtNewGMN4.Columns("GMN2")
        keysGMN4(2) = dtNewGMN4.Columns("GMN3")
        keysGMN4(3) = dtNewGMN4.Columns("GMN4")
        dtNewGMN4.PrimaryKey = keysGMN4


        'Crea la tabla temporal para los gmn1 del GS eliminados:
        dtDeleteGMN1 = DS.Tables.Add("TEMP_DELETED_GMN1_MATQA")
        dtDeleteGMN1.Columns.Add("GMN1", System.Type.GetType("System.String"))
        keysGMN1(0) = dtDeleteGMN1.Columns("GMN1")
        dtDeleteGMN1.PrimaryKey = keysGMN1

        'Crea la tabla temporal para los nuevos gmN2 del GS eliminados:
        dtDeleteGMN2 = DS.Tables.Add("TEMP_DELETED_GMN2_MATQA")
        dtDeleteGMN2.Columns.Add("GMN1", System.Type.GetType("System.String"))
        dtDeleteGMN2.Columns.Add("GMN2", System.Type.GetType("System.String"))
        keysGMN2(0) = dtDeleteGMN2.Columns("GMN1")
        keysGMN2(1) = dtDeleteGMN2.Columns("GMN2")
        dtDeleteGMN2.PrimaryKey = keysGMN2

        'Crea la tabla temporal para los nuevos gmN3 del GS eliminados:
        dtDeleteGMN3 = DS.Tables.Add("TEMP_DELETED_GMN3_MATQA")
        dtDeleteGMN3.Columns.Add("GMN1", System.Type.GetType("System.String"))
        dtDeleteGMN3.Columns.Add("GMN2", System.Type.GetType("System.String"))
        dtDeleteGMN3.Columns.Add("GMN3", System.Type.GetType("System.String"))
        keysGMN3(0) = dtDeleteGMN3.Columns("GMN1")
        keysGMN3(1) = dtDeleteGMN3.Columns("GMN2")
        keysGMN3(2) = dtDeleteGMN3.Columns("GMN3")
        dtDeleteGMN3.PrimaryKey = keysGMN3

        'Crea la tabla temporal para los nuevos gmN4 del GS eliminados:
        dtDeleteGMN4 = DS.Tables.Add("TEMP_DELETED_GMN4_MATQA")
        dtDeleteGMN4.Columns.Add("GMN1", System.Type.GetType("System.String"))
        dtDeleteGMN4.Columns.Add("GMN2", System.Type.GetType("System.String"))
        dtDeleteGMN4.Columns.Add("GMN3", System.Type.GetType("System.String"))
        dtDeleteGMN4.Columns.Add("GMN4", System.Type.GetType("System.String"))
        keysGMN4(0) = dtDeleteGMN4.Columns("GMN1")
        keysGMN4(1) = dtDeleteGMN4.Columns("GMN2")
        keysGMN4(2) = dtDeleteGMN4.Columns("GMN3")
        keysGMN4(3) = dtDeleteGMN4.Columns("GMN4")
        dtDeleteGMN4.PrimaryKey = keysGMN4


        oItem = Request.Form
        arr1 = oItem.AllKeys
        For loop1 = 0 To arr1.GetUpperBound(0)
            sRequest = arr1(loop1).ToString

            oRequest = sRequest.Split("_")

            bInsertar = False

            Select Case oRequest(0)

                Case "NEWGMN1"
                    findGMN1(0) = oRequest(1)
                    dtNewRow = dtNewGMN1.Rows.Find(findGMN1)
                    If dtNewRow Is Nothing Then
                        dtNewRow = dtNewGMN1.NewRow
                    End If
                    dtNewRow.Item("GMN1") = oRequest(1)

                    If dtNewRow.RowState = DataRowState.Detached Then
                        dtNewGMN1.Rows.Add(dtNewRow)
                    End If

                Case "NEWGMN2"
                    'Si est� en las la tabla de los GMN1 no se carga en la de gmn2
                    findGMN1(0) = Split(oRequest(1), "&&&")(0)
                    If dtnewGMN1.Rows.Find(findGMN1) Is Nothing Then
                        findGMN2(0) = Split(oRequest(1), "&&&")(0)
                        findGMN2(1) = Split(oRequest(1), "&&&")(1)

                        dtNewGMN1.Rows.Find(findGMN1)

                        dtNewRow = dtNewGMN2.Rows.Find(findGMN2)
                        If dtNewRow Is Nothing Then
                            dtNewRow = dtNewGMN2.NewRow
                        End If

                        dtNewRow.Item("GMN1") = Split(oRequest(1), "&&&")(0)
                        dtNewRow.Item("GMN2") = Split(oRequest(1), "&&&")(1)

                        If dtNewRow.RowState = DataRowState.Detached Then
                            dtNewGMN2.Rows.Add(dtNewRow)
                        End If
                    End If

                Case "NEWGMN3"
                    'Si no est� insertado en GMN2 ni GMN1 guarda en GMN3
                    findGMN2(0) = Split(oRequest(1), "&&&")(0)
                    findGMN2(1) = Split(oRequest(1), "&&&")(1)

                    If dtNewGMN2.Rows.Find(findGMN2) Is Nothing Then
                        'Comprueba que no est� en GMN1
                        findGMN1(0) = Split(oRequest(1), "&&&")(0)
                        If dtNewGMN1.Rows.Find(findGMN1) Is Nothing Then
                            bInsertar = True
                        End If
                    End If

                    If bInsertar = True Then
                        findGMN3(0) = Split(oRequest(1), "&&&")(0)
                        findGMN3(1) = Split(oRequest(1), "&&&")(1)
                        findGMN3(2) = Split(oRequest(1), "&&&")(2)

                        dtNewRow = dtNewGMN3.Rows.Find(findGMN3)
                        If dtNewRow Is Nothing Then
                            dtNewRow = dtNewGMN3.NewRow
                        End If

                        dtNewRow.Item("GMN1") = Split(oRequest(1), "&&&")(0)
                        dtNewRow.Item("GMN2") = Split(oRequest(1), "&&&")(1)
                        dtNewRow.Item("GMN3") = Split(oRequest(1), "&&&")(2)

                        If dtNewRow.RowState = DataRowState.Detached Then
                            dtNewGMN3.Rows.Add(dtNewRow)
                        End If
                    End If

                Case "NEWGMN4"
                    'Si no est� insertado en GMN3 ni GMN2 ni GMN1 guarda en GMN4
                    findGMN3(0) = Split(oRequest(1), "&&&")(0)
                    findGMN3(1) = Split(oRequest(1), "&&&")(1)
                    findGMN3(2) = Split(oRequest(1), "&&&")(2)
                    If dtNewGMN3.Rows.Find(findGMN3) Is Nothing Then
                        findGMN2(0) = Split(oRequest(1), "&&&")(0)
                        findGMN2(1) = Split(oRequest(1), "&&&")(1)
                        If dtNewGMN2.Rows.Find(findGMN2) Is Nothing Then
                            findGMN1(0) = Split(oRequest(1), "&&&")(0)
                            If dtNewGMN1.Rows.Find(findGMN1) Is Nothing Then
                                bInsertar = True
                            End If
                        End If
                    End If

                    If bInsertar = True Then
                        findGMN4(0) = Split(oRequest(1), "&&&")(0)
                        findGMN4(1) = Split(oRequest(1), "&&&")(1)
                        findGMN4(2) = Split(oRequest(1), "&&&")(2)
                        findGMN4(3) = Split(oRequest(1), "&&&")(3)

                        dtNewRow = dtNewGMN4.Rows.Find(findGMN4)
                        If dtNewRow Is Nothing Then
                            dtNewRow = dtNewGMN4.NewRow
                        End If

                        dtNewRow.Item("GMN1") = Split(oRequest(1), "&&&")(0)
                        dtNewRow.Item("GMN2") = Split(oRequest(1), "&&&")(1)
                        dtNewRow.Item("GMN3") = Split(oRequest(1), "&&&")(2)
                        dtNewRow.Item("GMN4") = Split(oRequest(1), "&&&")(3)

                        If dtNewRow.RowState = DataRowState.Detached Then
                            dtNewGMN4.Rows.Add(dtNewRow)
                        End If
                    End If

                Case "DELETEGMN1"
                    findGMN1(0) = oRequest(1)
                    dtNewRow = dtDeleteGMN1.Rows.Find(findGMN1)
                    If dtNewRow Is Nothing Then
                        dtNewRow = dtDeleteGMN1.NewRow
                    End If
                    dtNewRow.Item("GMN1") = oRequest(1)

                    If dtNewRow.RowState = DataRowState.Detached Then
                        dtDeleteGMN1.Rows.Add(dtNewRow)
                    End If

                Case "DELETEGMN2"
                    findGMN2(0) = Split(oRequest(1), "&&&")(0)
                    findGMN2(1) = Split(oRequest(1), "&&&")(1)

                    dtNewRow = dtDeleteGMN2.Rows.Find(findGMN2)
                    If dtNewRow Is Nothing Then
                        dtNewRow = dtDeleteGMN2.NewRow
                    End If

                    dtNewRow.Item("GMN1") = Split(oRequest(1), "&&&")(0)
                    dtNewRow.Item("GMN2") = Split(oRequest(1), "&&&")(1)

                    If dtNewRow.RowState = DataRowState.Detached Then
                        dtDeleteGMN2.Rows.Add(dtNewRow)
                    End If

                Case "DELETEGMN3"
                    findGMN3(0) = Split(oRequest(1), "&&&")(0)
                    findGMN3(1) = Split(oRequest(1), "&&&")(1)
                    findGMN3(2) = Split(oRequest(1), "&&&")(2)

                    dtNewRow = dtDeleteGMN3.Rows.Find(findGMN3)
                    If dtNewRow Is Nothing Then
                        dtNewRow = dtDeleteGMN3.NewRow
                    End If

                    dtNewRow.Item("GMN1") = Split(oRequest(1), "&&&")(0)
                    dtNewRow.Item("GMN2") = Split(oRequest(1), "&&&")(1)
                    dtNewRow.Item("GMN3") = Split(oRequest(1), "&&&")(2)

                    If dtNewRow.RowState = DataRowState.Detached Then
                        dtDeleteGMN3.Rows.Add(dtNewRow)
                    End If

                Case "DELETEGMN4"
                    findGMN4(0) = Split(oRequest(1), "&&&")(0)
                    findGMN4(1) = Split(oRequest(1), "&&&")(1)
                    findGMN4(2) = Split(oRequest(1), "&&&")(2)
                    findGMN4(3) = Split(oRequest(1), "&&&")(3)

                    dtNewRow = dtDeleteGMN4.Rows.Find(findGMN4)
                    If dtNewRow Is Nothing Then
                        dtNewRow = dtDeleteGMN4.NewRow
                    End If

                    dtNewRow.Item("GMN1") = Split(oRequest(1), "&&&")(0)
                    dtNewRow.Item("GMN2") = Split(oRequest(1), "&&&")(1)
                    dtNewRow.Item("GMN3") = Split(oRequest(1), "&&&")(2)
                    dtNewRow.Item("GMN4") = Split(oRequest(1), "&&&")(3)

                    If dtNewRow.RowState = DataRowState.Detached Then
                        dtDeleteGMN4.Rows.Add(dtNewRow)
                    End If
            End Select

        Next loop1

        Return DS

    End Function

    End Class

