
    Public Class asignarMateriales
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
        Protected WithEvents lblMaterial As System.Web.UI.WebControls.Label
        Protected WithEvents lblSeleccionar As System.Web.UI.WebControls.Label
        Protected WithEvents Material As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents cmdAceptar As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents cmdCancelar As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents uwtMateriales As Infragistics.WebUI.UltraWebNavigator.UltraWebTree

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Const AltaScriptKey As String = "AltaIncludeScript"
        Private Const AltaFileName As String = "js/jsAltaMat.js"
        Private Const IncludeScriptFormat As String = ControlChars.CrLf & _
 "<script type=""{0}"" src=""{1}""></script>"
        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
    "<script language=""{0}"">{1}</script>"

    ''' <summary>
    ''' Carga la pagina de asignacion de materiales GS a materiales QA
    ''' </summary>
    ''' <param name="sender">propios del evento</param>
    ''' <param name="e">propios del evento</param>        
    ''' <remarks>Llamada desde= Al cargar la pagina; Tiempo m�ximo=>1seg.</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AsignarMaterialesAMatQA
        Dim sIdi As String = FSNUser.Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        Me.lblTitulo.Text = Textos(0)
        Me.lblSeleccionar.Text = Textos(1)
        Me.cmdAceptar.Value = Textos(2)
        Me.cmdCancelar.Value = Textos(3)
        Me.lblMaterial.Text = Request("DenMaterial")
        Me.Material.Value = Request("Material")


        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(Textos(4)) + " ';"   'Seleccione al menos un material de GS
        sClientTexts += "arrTextosML[1] = '" + JSText(Textos(6)) + " ';"   'Imposible asignar el material de GS.\r Ya est� asignado al siguiente material de QA:
        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        Page.ClientScript.RegisterStartupScript(Me.GetType(),"TextosMICliente", sClientTexts)


        '************* Carga el �rbol de materiales *************
        Dim oGruposMaterial As FSNServer.MaterialesQA
        oGruposMaterial = FSNServer.Get_Object(GetType(FSNServer.MaterialesQA))
        Dim dsMaterialesGS As DataSet
        dsMaterialesGS = oGruposMaterial.GetMaterialesGS(sIdi, Request("Material"), FSNUser.Pyme)
        Me.uwtMateriales.ClearAll()

        Me.uwtMateriales.DataSource = dsMaterialesGS.Tables(0).DefaultView
        Me.uwtMateriales.Levels(0).LevelImage = "../_common/images/carpetaCommodityTxiki.gif"
        Me.uwtMateriales.Levels(0).RelationName = "REL_NIV1_NIV2"
        Me.uwtMateriales.Levels(0).ColumnName = "DEN"
        Me.uwtMateriales.Levels(0).LevelKeyField = "COD"
        Me.uwtMateriales.Levels(0).LevelCheckBoxes = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.True
        Me.uwtMateriales.Levels(0).CheckboxColumnName = "ES_QA"

        Me.uwtMateriales.Levels(1).LevelImage = "../_common/images/carpetaFamilia.gif"
        Me.uwtMateriales.Levels(1).RelationName = "REL_NIV2_NIV3"
        Me.uwtMateriales.Levels(1).ColumnName = "DEN"
        Me.uwtMateriales.Levels(1).LevelKeyField = "COD"
        Me.uwtMateriales.Levels(1).LevelCheckBoxes = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.True
        Me.uwtMateriales.Levels(1).CheckboxColumnName = "ES_QA"

        Me.uwtMateriales.Levels(2).LevelImage = "../_common/images/carpetaGrupo.gif"
        Me.uwtMateriales.Levels(2).RelationName = "REL_NIV3_NIV4"
        Me.uwtMateriales.Levels(2).ColumnName = "DEN"
        Me.uwtMateriales.Levels(2).LevelKeyField = "COD"
        Me.uwtMateriales.Levels(2).LevelCheckBoxes = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.True
        Me.uwtMateriales.Levels(2).CheckboxColumnName = "ES_QA"

        Me.uwtMateriales.Levels(3).LevelImage = "../_common/images/carpetaCerrada.gif"
        Me.uwtMateriales.Levels(3).ColumnName = "DEN"
        Me.uwtMateriales.Levels(3).LevelKeyField = "COD"
        Me.uwtMateriales.Levels(3).LevelCheckBoxes = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.True
        Me.uwtMateriales.Levels(3).CheckboxColumnName = "ES_QA"

        Me.uwtMateriales.DataBind()
        Me.uwtMateriales.DataKeyOnClient = True


        oGruposMaterial = Nothing

        Dim includeScript As String
        If Not Page.ClientScript.IsClientScriptIncludeRegistered(AltaScriptKey) Then
            includeScript = String.Format(IncludeScriptFormat, "text/javascript", AltaFileName)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(),AltaScriptKey, includeScript)
        End If
    End Sub

    ''' <summary>
    ''' Al leer cada nodo se comprueba si el nodo leido pertenece al material QA Seleccionado.
    ''' Si pertenece, lo deja checkeado para que pueda deseleccionarlo.
    ''' Sino, pertenece a otro material QA por lo que el check estara deshabilitado y tampoco podra elegir ninguno de sus padres...
    ''' </summary>
    ''' <param name="sender">propios del evento</param>
    ''' <param name="e">propios del evento</param>        
    ''' <remarks>Llamada desde=Evento que salta al cargarse el arbol (Databind); Tiempo m�ximo:=>0,3seg.</remarks>
    Private Sub uwtMateriales_NodeBound(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebNavigator.WebTreeNodeEventArgs) Handles uwtMateriales.NodeBound
        Dim oNode As Infragistics.WebUI.UltraWebNavigator.Node
        Dim ds As DataSet
        Dim oRows() As DataRow
        Dim sWhere As String = ""

        If e.Node.Checked = True Then
            e.Node.Tag = Request("Material")
            e.Node.Expanded = True

            If Not e.Node.Parent Is Nothing Then
                oNode = e.Node.Parent
                While Not oNode Is Nothing
                    'oNode.Tag = 1 'Indica que tien un nodo seleccionable
                    oNode.Expanded = True
                    oNode = oNode.Parent
                End While
            End If
        Else
            ds = sender.datasource.dataviewmanager.dataset
            If e.Node.Level = 3 Then
                sWhere = " GMN1 = " & StrToSQLNULL(e.Node.Parent.Parent.Parent.DataKey) & " AND GMN2 = " & StrToSQLNULL(e.Node.Parent.Parent.DataKey) & " AND GMN3= " & StrToSQLNULL(e.Node.Parent.DataKey) & " AND "
            End If
            If e.Node.Level = 2 Then
                sWhere = " GMN1 = " & StrToSQLNULL(e.Node.Parent.Parent.DataKey) & " AND GMN2 = " & StrToSQLNULL(e.Node.Parent.DataKey) & " AND"
            End If
            If e.Node.Level = 1 Then
                sWhere = " GMN1 = " & StrToSQLNULL(e.Node.Parent.DataKey) & " AND"
            End If
            sWhere = sWhere & " COD= " & StrToSQLNULL(e.Node.DataKey)
            oRows = ds.Tables(e.Node.Level).Select(sWhere)
            If oRows.Length > 0 Then
                'si el material qa es distinto al request bloquearlo
                If (oRows(0).Item("TIENE_MATERIAL") = 1) AndAlso (oRows(0).Item("MATERIAL_QA") <> Request("Material")) Then
                    e.Node.Title = Textos(7) & ": " & DBNullToStr(oRows(0).Item("DEN_MATERIALQA_RELACIONADO"))
                    If Not e.Node.Parent Is Nothing Then
                        e.Node.CheckBox = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.False
                        e.Node.Enabled = True
                        e.Node.Expanded = False

                        oNode = e.Node.Parent
                        While Not oNode Is Nothing
                            oNode.CheckBox = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.False
                            oNode.Enabled = True
                            e.Node.Expanded = False
                            oNode = oNode.Parent
                        End While
                    End If
                Else
                    e.Node.Expanded = True
                End If
            End If

            End If
    End Sub
    End Class

