Public Class frames
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents fraWS As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' <summary>
    ''' Cargar la pagina
    ''' </summary>
    ''' <param name="sender">pagina</param>
    ''' <param name="e">Evento de sistema</param>        
    ''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sQuery As String = ""
        sQuery = Request("pagina")
        If sQuery <> "" Then
            sQuery = Replace(sQuery, "*", "&")
            sQuery = Replace(sQuery, "&&", "*")
            If CtrlPosibleXSS(sQuery) Then
                Response.Redirect(ConfigurationManager.AppSettings("ruta") & "default.aspx")
                Exit Sub
            End If
        End If

        Dim str As String = "<frame id=fraWSHeader src='blank.htm' scrolling=no noresize><frame id=fraWSMain name=fraWSMain src='" & sQuery & "' ><frame name=fraWSServer id=fraWSServer src='blank.htm' >"
        fraWS.InnerHtml = str
    End Sub
End Class

