﻿Imports System.ComponentModel
Imports System.Web.Script.Services
Imports System.Web.Services

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<System.Web.Script.Services.ScriptService()>
<ToolboxItem(False)>
Public Class ConsultasPMWEB
    Inherits System.Web.Services.WebService

#Region "Persona"

    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle de una persona
    ''' </summary>
    ''' <param name="contextKey">Código de persona</param>
    ''' <returns>Un String con los datos de la persona en formato html</returns>
    <System.Web.Services.WebMethod(True)>
    Public Function Obtener_DatosPersona(ByVal contextKey As System.String) As System.String
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim oServer As New FSNServer.Root

        oServer.Login(FSNUser.Cod, FSNUser.Password)
        Dim oPer As FSNServer.Persona = oServer.Get_Object(GetType(FSNServer.Persona))
        oPer.LoadData(contextKey, True) 'bCtlBajaLog:=False Problema de que el peticionario esta de baja y nos fallaba en el popup.

        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.DetallePersona, FSNUser.Idioma)
        Dim sr As New StringBuilder()
        With oPer
            sr.Append("<b>" & oDict.Data.Tables(0).Rows(0).Item(1) & ":</b> " & .Codigo & "<br/>")
            sr.Append("<b>" & oDict.Data.Tables(0).Rows(1).Item(1) & ":</b> " & .Nombre & "<br/>")
            sr.Append("<b>" & oDict.Data.Tables(0).Rows(2).Item(1) & ":</b> " & .Apellidos & "<br/>")
            sr.Append("<b>" & oDict.Data.Tables(0).Rows(3).Item(1) & ":</b> " & .Cargo & "<br/>")
            sr.Append("<b>" & oDict.Data.Tables(0).Rows(4).Item(1) & ":</b> " &
                        "<a class=""Normal aPMWeb"" href=""callto:" & .Telefono & """>" & .Telefono & "</a><br/>")
            sr.Append("<b>" & oDict.Data.Tables(0).Rows(5).Item(1) & ":</b> " & .Fax & "<br/>")
            sr.Append("<b>" & oDict.Data.Tables(0).Rows(6).Item(1) & ":</b> " &
                      "<a class=""Normal aPMWeb"" href=""mailto:" & .EMail & """>" & .EMail & "</a><br/>")
            sr.Append("<b>" & oDict.Data.Tables(0).Rows(7).Item(1) & ":</b> " & .DenDepartamento & "<br/>")
            sr.Append("<b>" & oDict.Data.Tables(0).Rows(8).Item(1) & ":</b> " & .UONs & "<br/>")
        End With

        Return sr.ToString()

    End Function

    <System.Web.Services.WebMethod(True)>
    Public Function CargarDatosDestinos(ByVal sID As System.String) As Object
        'Dim oPer As FSNServer.CPersona
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim sr As New StringBuilder()
        Dim oCDestinos As Fullstep.FSNServer.CDestinos = FSNServer.Get_Object(GetType(Fullstep.FSNServer.CDestinos))

        oCDestinos.CargarUnDestino(FSNUser.Idioma, sID)
        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.EP_Destinos, FSNUser.Idioma)
        Dim lTextos As New List(Of String)
        lTextos.Add(oDict.Data.Tables(0).Rows(3).Item(1) & ": ")
        lTextos.Add(oDict.Data.Tables(0).Rows(4).Item(1) & ": ")
        lTextos.Add(oDict.Data.Tables(0).Rows(5).Item(1) & ": ")
        lTextos.Add(oDict.Data.Tables(0).Rows(6).Item(1) & ": ")
        lTextos.Add(oDict.Data.Tables(0).Rows(7).Item(1) & ": ")
        lTextos.Add(oDict.Data.Tables(0).Rows(17).Item(1) & ": ")
        lTextos.Add(oDict.Data.Tables(0).Rows(18).Item(1) & ": ")
        lTextos.Add(oDict.Data.Tables(0).Rows(19).Item(1) & ": ")
        lTextos.Add(oDict.Data.Tables(0).Rows(2).Item(1) & "")
        lTextos.Add(oDict.Data.Tables(0).Rows(20).Item(1) & "")
        Return {oCDestinos(0), lTextos}
    End Function
#End Region

#Region "Proveedor"

    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle de un proveedor
    ''' </summary>
    ''' <param name="contextKey">Código de proveedor</param>
    ''' <returns>Un String con los datos del proveedor en formato html</returns>
    <System.Web.Services.WebMethod(True)>
    Public Function Obtener_DatosProveedor(ByVal contextKey As System.String) As System.String

        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")

        Dim oProve As FSNServer.Proveedor

        oProve = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
        oProve.Cod = contextKey

        oProve.Load(FSNUser.Idioma)


        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.DetalleProveedor, FSNUser.Idioma)
        Dim sr As New StringBuilder()

        sr.Append("<b>" & oDict.Data.Tables(0).Rows(0).Item(1) & ":</b> " & oProve.Cod & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(1).Item(1) & ":</b> " & oProve.Den & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(2).Item(1) & ":</b> " & oProve.NIF & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(3).Item(1) & ":</b> " & oProve.Dir & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(4).Item(1) & ":</b> " & oProve.CP & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(5).Item(1) & ":</b> " & oProve.Pob & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(6).Item(1) & ":</b> " & oProve.PaiDen & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(7).Item(1) & ":</b> " & oProve.ProviDen & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(8).Item(1) & ":</b> " & oProve.MonCod & " - " & DBNullToSomething(oProve.MonDen) & "<br/>")

        Return sr.ToString()
    End Function

#End Region

#Region "Empresa"

    ''' <summary>
    ''' Función que se encarga de rellenar los datos de detalle de una empresa
    ''' </summary>
    ''' <param name="contextKey">ID empresa</param>
    ''' <returns>Un String con los datos de la empresa en formato html</returns>
    <System.Web.Services.WebMethod(True)>
    Public Function Obtener_DatosEmpresa(ByVal contextKey As System.String) As System.String
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")


        Dim oEmpresa As FSNServer.Empresa


        oEmpresa = FSNServer.Get_Object(GetType(FSNServer.Empresa))
        oEmpresa.ID = contextKey

        oEmpresa.Load(FSNUser.Idioma)


        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.DetalleEmpresa, FSNUser.Idioma)
        Dim sr As New StringBuilder()

        sr.Append("<b>" & oDict.Data.Tables(0).Rows(0).Item(1) & ":</b> " & oEmpresa.Den & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(1).Item(1) & ":</b> " & oEmpresa.NIF & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(2).Item(1) & ":</b> " & oEmpresa.Dir & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(3).Item(1) & ":</b> " & oEmpresa.CP & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(4).Item(1) & ":</b> " & oEmpresa.Poblacion & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(5).Item(1) & ":</b> " & oEmpresa.Pais & "<br/>")
        sr.Append("<b>" & oDict.Data.Tables(0).Rows(6).Item(1) & ":</b> " & oEmpresa.Provincia & "<br/>")

        Return sr.ToString()
    End Function
#End Region

#Region "DropDowns"
    ''' Revisado por: sra. Fecha: 22/03/2013
    ''' <summary>
    ''' FunciÃ³n que devuelve un string formateado en modo JSON con los datos obtenidos
    ''' </summary>
    ''' <param name="contextKey">TipoGS y cod de pais, ... cuando son combos que dependen de otras. Ambos separados por tres arrobas</param>
    ''' <returns>String con las datos obtgenidos</returns>
    ''' <remarks>Llamada desde los webdropdown de campos del sistema. Tiempo mÃ¡ximo inferior a 0,5 seg.</remarks>
    <System.Web.Services.WebMethod(True)>
    Public Function ObtenerDatos_DropDown(ByVal contextKey As System.String) As String
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim sResultado As String = String.Empty
        Dim dsResultado As DataSet = Nothing

        Dim sDatos As String() = Split(contextKey, "@@@")
        Dim iTipoGS As Integer = sDatos(1)

        Select Case iTipoGS
            Case TiposDeDatos.TipoCampoGS.Provincia
                Dim sPaisCod As String = sDatos(0)
                Dim oProvis As FSNServer.Provincias
                oProvis = FSNServer.Get_Object(GetType(FSNServer.Provincias))
                oProvis.Pais = DBNullToStr(sPaisCod)
                oProvis.LoadData(FSNUser.Idioma.ToString())
                dsResultado = oProvis.Data
            Case TiposDeDatos.TipoCampoGS.Almacen
                Dim datos As String = sDatos(0)
                Dim sCentroCod As String = ""
                Dim sCodDest As String = ""
                If datos.Contains("@@DEST_") = True Then
                    sCodDest = datos.Substring(7)
                Else
                    sCentroCod = datos
                End If

                Dim oAlmac As FSNServer.Almacenes
                oAlmac = FSNServer.Get_Object(GetType(FSNServer.Almacenes))
                If sCentroCod <> String.Empty Then
                    oAlmac.LoadData(sCentroCod)
                ElseIf sCodDest <> String.Empty Then
                    oAlmac.LoadData(, sCodDest)
                Else
                    oAlmac.LoadData()
                End If
                dsResultado = oAlmac.Data
            Case TiposDeDatos.TipoCampoGS.Centro
                Dim sCodOrgCompras As String = sDatos(0)
                Dim oCentros As FSNServer.Centros
                oCentros = FSNServer.Get_Object(GetType(FSNServer.Centros))
                If sCodOrgCompras <> String.Empty Then
                    oCentros.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil, sCodOrgCompras)
                Else
                    oCentros.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil)
                End If
                dsResultado = oCentros.Data
            Case TiposDeDatos.TipoCampoGS.Departamento
                Dim sDepartamentoCod As String = sDatos(0)
                Dim oDepartamentos As FSNServer.Departamentos
                oDepartamentos = FSNServer.Get_Object(GetType(FSNServer.Departamentos))
                If sDepartamentoCod <> String.Empty Then
                    Dim vUnidadesOrganizativas(3) As String
                    vUnidadesOrganizativas = Split(sDepartamentoCod, " - ")
                    Select Case UBound(vUnidadesOrganizativas)
                        Case 0
                            If sDepartamentoCod <> "" Then oDepartamentos.LoadData(0)
                        Case 1
                            oDepartamentos.LoadData(1, vUnidadesOrganizativas(0))
                        Case 2
                            oDepartamentos.LoadData(2, vUnidadesOrganizativas(0), vUnidadesOrganizativas(1))
                        Case 3
                            oDepartamentos.LoadData(3, vUnidadesOrganizativas(0), vUnidadesOrganizativas(1), vUnidadesOrganizativas(2))
                    End Select
                Else ''Mostrar todas los departamentos
                    oDepartamentos.LoadData()
                End If
                dsResultado = oDepartamentos.Data
            Case TiposDeDatos.TipoCampoGS.Moneda
                Dim oMonedas As FSNServer.Monedas
                oMonedas = FSNServer.Get_Object(GetType(FSNServer.Monedas))
                oMonedas.LoadData(FSNUser.Idioma.ToString())
                dsResultado = oMonedas.Data
            Case TiposDeDatos.TipoCampoGS.FormaPago
                Dim oFormasPago As FSNServer.FormasPago
                oFormasPago = FSNServer.Get_Object(GetType(FSNServer.FormasPago))
                oFormasPago.LoadData(FSNUser.Idioma.ToString())
                dsResultado = oFormasPago.Data
            Case TiposDeDatos.TipoCampoGS.Unidad, TiposDeDatos.TipoCampoGS.UnidadPedido
                Dim oUnidades As FSNServer.Unidades
                oUnidades = FSNServer.Get_Object(GetType(FSNServer.Unidades))
                oUnidades.LoadData(FSNUser.Idioma.ToString())
                dsResultado = oUnidades.Data
            Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
                Dim oOrganizacionesCompras As FSNServer.OrganizacionesCompras
                oOrganizacionesCompras = FSNServer.Get_Object(GetType(FSNServer.OrganizacionesCompras))
                oOrganizacionesCompras.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil)
                dsResultado = oOrganizacionesCompras.Data
            Case TiposDeDatos.TipoCampoGS.Pais
                Dim oPaises As FSNServer.Paises
                oPaises = FSNServer.Get_Object(GetType(FSNServer.Paises))
                oPaises.LoadData(FSNUser.Idioma.ToString())
                dsResultado = oPaises.Data
            Case TiposDeDatos.TipoCampoGS.Dest
                Dim oDests As FSNServer.Destinos
                oDests = FSNServer.Get_Object(GetType(FSNServer.Destinos))
                oDests.LoadData(FSNUser.Idioma.ToString(), FSNUser.CodPersona)
                dsResultado = oDests.Data
            Case TiposDeDatos.TipoCampoGS.Contacto
                Dim sProve As String = sDatos(0)
                Dim oContactos As FSNServer.Contactos
                oContactos = FSNServer.Get_Object(GetType(FSNServer.Contactos))

                oContactos.Proveedor = sProve
                oContactos.LoadData()

                dsResultado = oContactos.Data
            Case TiposDeDatos.TipoCampoGS.ProveedorERP
                Dim sDatosProveERP As String() = Split(sDatos(0), "###")
                Dim sProve As String = sDatosProveERP(0)
                Dim sOrgCompras As String = sDatosProveERP(1)
                Dim oProvesERP As FSNServer.CProveERPs
                oProvesERP = FSNServer.Get_Object(GetType(FSNServer.CProveERPs))
                dsResultado = oProvesERP.CargarProveedoresERPtoDS(sProve, sOrgCompras)
            Case TiposDeDatos.TipoCampoGS.AnyoPartida
                sDatos = Split(contextKey, "@@")
                Dim sPartida0 As String = sDatos(0)
                Dim sPartidaCod As String = Trim(sDatos(1))
                Dim oAnyoPartida As FSNServer.PartidaPRES5
                oAnyoPartida = FSNServer.Get_Object(GetType(FSNServer.PartidaPRES5))
                oAnyoPartida.LoadDataAnyoPartida(DBNullToStr(sPartida0), DBNullToStr(sPartidaCod))
                dsResultado = oAnyoPartida.DataAnyoPartida
        End Select

        For Each oRow As DataRow In dsResultado.Tables(0).Rows
            If iTipoGS = TiposDeDatos.TipoCampoGS.Contacto Then
                If oRow("ID") Is System.DBNull.Value AndAlso oRow("CONTACTO") Is System.DBNull.Value AndAlso dsResultado.Tables(0).Rows.Count = 1 Then
                    sResultado = sResultado & "{""Valor"":"""",""Texto"":""""},"
                ElseIf Not (oRow("ID") Is System.DBNull.Value AndAlso oRow("CONTACTO") Is System.DBNull.Value) Then
                    sResultado = sResultado & "{""Valor"":""" & HttpUtility.JavaScriptStringEncode(oRow("ID"))
                    sResultado = sResultado & """,""Texto"":""" & HttpUtility.JavaScriptStringEncode(oRow("CONTACTO"))
                    sResultado = sResultado & """},"
                End If
            ElseIf iTipoGS = TiposDeDatos.TipoCampoGS.AnyoPartida Then
                sResultado = sResultado & "{""Valor"":""" & HttpUtility.JavaScriptStringEncode(oRow("ANYO"))
                sResultado = sResultado & """,""Texto"":""" & HttpUtility.JavaScriptStringEncode(oRow("ANYO"))
                sResultado = sResultado & """},"
            Else
                If oRow("COD") Is System.DBNull.Value AndAlso oRow("DEN") Is System.DBNull.Value AndAlso dsResultado.Tables(0).Rows.Count = 1 Then
                    sResultado = sResultado & "{""Valor"":"""",""Texto"":""""},"
                Else
                    If Not (oRow("COD") Is System.DBNull.Value AndAlso oRow("DEN") Is System.DBNull.Value) Then
                        If iTipoGS = TiposDeDatos.TipoCampoGS.Almacen Then
                            sResultado = sResultado & "{""Valor"":""" & HttpUtility.JavaScriptStringEncode(oRow("ID"))
                        Else
                            sResultado = sResultado & "{""Valor"":""" & HttpUtility.JavaScriptStringEncode(oRow("COD"))
                        End If

                        sResultado = sResultado & """,""Texto"":""" & HttpUtility.JavaScriptStringEncode(oRow("COD")) & " - " & HttpUtility.JavaScriptStringEncode(oRow("DEN"))

                        If iTipoGS = TiposDeDatos.TipoCampoGS.Dest Then
                            sResultado = sResultado & HttpUtility.JavaScriptStringEncode(If(IsDBNull(oRow("POB")), "", " (" & oRow("POB") & ")"))
                        End If

                        sResultado = sResultado & """},"
                    End If
                End If
            End If
        Next

        If Len(sResultado) > 0 Then
            sResultado = Left(sResultado, Len(sResultado) - 1)
        End If
        Return sResultado
    End Function

    ''' Revisado por: blp. Fecha: 04/10/2011
    ''' <summary>
    ''' Función que devuelve un string formateado en modo JSON con la lista seleccionada
    ''' </summary>
    ''' <param name="contextKey">Id de la lista y valor seleccionado en el campo padre</param>
    ''' <returns>String con la lista y nombre del campo a usar en el template para construir luego el combo a partir del json. Ambos separados por tres arrobas</returns>
    ''' <remarks>Llamada desde los webdropdown de almacenes. Tiempo máximo inferior a 1 seg.</remarks>
    <System.Web.Services.WebMethod(True)>
    Public Function Obtener_Lista(ByVal contextKey As System.String) As String
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")

        Dim arrAux() As String
        Dim iOrden As Integer = 0
        Dim lIdCampo As Long = 0
        Dim lInstancia As Long = 0
        Dim lBloque As Long = 0
        Dim lSolicitud As Long = 0
        Dim bValorNum As Boolean = False
        Dim oCampo As Object
        Dim sResultado As String = String.Empty
        Dim sDatos As String() = Split(contextKey, "@@@")
        Dim sLista As String = sDatos(0)
        Dim sTemplate As String = sDatos(1)

        arrAux = Split(CType(sLista, String), "#")

        If IsNumeric(arrAux(0)) Then
            lIdCampo = CLng(arrAux(0))
        End If

        If IsNumeric(arrAux(1)) Then
            iOrden = CInt(arrAux(1))
        End If

        If IsNumeric(arrAux(2)) Then
            lInstancia = CInt(arrAux(2))
        End If

        If IsNumeric(arrAux(3)) Then
            lBloque = CLng(arrAux(3))
        End If

        If IsNumeric(arrAux(4)) Then
            lSolicitud = CLng(arrAux(4))
        End If

        If UBound(arrAux) = 5 AndAlso IsNumeric(arrAux(5)) Then
            bValorNum = (arrAux(5) = 1)
        End If

        oCampo = FSNServer.Get_Object(GetType(FSNServer.Campo))
        'lIdCampo --> Id del campo del que queremos mostrar los datos (Lista hija)
        'iOrdenPadre --> Orden del indice de la lista de la lista padre

        Dim dsLista As DataSet = oCampo.DevolverLista(lIdCampo, iOrden, FSNUser.Idioma, lInstancia, lBloque, lSolicitud, FSNUser.CodPersona, FSNUser.DateFmt, bValorNum)

        For Each oRow As DataRow In dsLista.Tables(0).Rows
            If oRow("ORDEN") Is System.DBNull.Value AndAlso oRow("DEN") Is System.DBNull.Value AndAlso dsLista.Tables(0).Rows.Count = 1 Then
                sResultado = sResultado & "{""Valor"":"""",""Texto"":""""},"
            ElseIf oRow("ORDEN") = 0 AndAlso oRow("DEN") = String.Empty Then
                'No vamos a mostrar el primer valor cuando esté vacío para que no se vea "0 - " en el desplegable...
            Else
                If Not (oRow("ORDEN") Is System.DBNull.Value AndAlso oRow("DEN") Is System.DBNull.Value) Then _
                    sResultado = sResultado & "{""Valor"":""" & oRow("ORDEN") & """,""Texto"":""" & HttpUtility.JavaScriptStringEncode(If(IsDBNull(oRow("DEN")), "", oRow("DEN"))) & """},"
            End If
        Next

        oCampo = Nothing

        If Len(sResultado) > 0 Then
            sResultado = Left(sResultado, Len(sResultado) - 1)
        End If
        Return sResultado
    End Function
    ''' <summary>Función que devuelve un string formateado en modo JSON con la lista seleccionada</summary>
    ''' <param name="contextKey">Id de la lista y valor seleccionado en el campo padre: IdCampo#GMNs@@@TipoGS</param>
    ''' <returns>String con la lista y nombre del campo a usar en el template para construir luego el combo a partir del json. Ambos separados por tres arrobas</returns>    
    <System.Web.Services.WebMethod(True)>
    Public Function Obtener_Lista_CampoPadre_Material(ByVal contextKey As System.String)
        Dim lIdCampo As Long = 0
        Dim sResultado As String = String.Empty

        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")

        Dim sDatos As String() = Split(contextKey, "@@@")
        Dim arrAux As String() = sDatos(0).Split("#")

        If IsNumeric(arrAux(0)) Then lIdCampo = CLng(arrAux(0))
        If arrAux(1) <> String.Empty Then
            Dim sGMN1 As String = String.Empty
            Dim sGMN2 As String = String.Empty
            Dim sGMN3 As String = String.Empty
            Dim sGMN4 As String = String.Empty

            Dim sMat = arrAux(1)
            Dim i As Integer = 1
            While sMat <> ""
                Select Case i
                    Case 1
                        sGMN1 = sMat.Substring(0, FSNServer.LongitudesDeCodigos.giLongCodGMN1).Trim
                        sMat = sMat.Substring(FSNServer.LongitudesDeCodigos.giLongCodGMN1)
                    Case 2
                        sGMN2 = sMat.Substring(0, FSNServer.LongitudesDeCodigos.giLongCodGMN2).Trim
                        sMat = sMat.Substring(FSNServer.LongitudesDeCodigos.giLongCodGMN2)
                    Case 3
                        sGMN3 = sMat.Substring(0, FSNServer.LongitudesDeCodigos.giLongCodGMN3).Trim
                        sMat = sMat.Substring(FSNServer.LongitudesDeCodigos.giLongCodGMN3)
                    Case 4
                        sGMN4 = sMat.Substring(0, FSNServer.LongitudesDeCodigos.giLongCodGMN4).Trim
                        sMat = sMat.Substring(FSNServer.LongitudesDeCodigos.giLongCodGMN4)
                End Select

                i += 1
            End While

            Dim oCampo As Object = FSNServer.Get_Object(GetType(FSNServer.Campo))
            Dim dsLista As DataSet = oCampo.DevolverListaCampoPadreMaterial(lIdCampo, sGMN1, sGMN2, sGMN3, sGMN4, FSNUser.Idioma)

            For Each oRow As DataRow In dsLista.Tables(0).Rows
                If oRow("ORDEN") Is System.DBNull.Value AndAlso oRow("DEN") Is System.DBNull.Value AndAlso dsLista.Tables(0).Rows.Count = 1 Then
                    sResultado = sResultado & "{""Valor"":"""",""Texto"":""""},"
                ElseIf oRow("ORDEN") = 0 AndAlso oRow("DEN") = String.Empty Then
                    'No vamos a mostrar el primer valor cuando esté vacío para que no se vea "0 - " en el desplegable...
                Else
                    If Not (oRow("ORDEN") Is System.DBNull.Value AndAlso oRow("DEN") Is System.DBNull.Value) Then
                        sResultado = sResultado & "{""Valor"":""" & oRow("ORDEN") & """,""Texto"":""" & HttpUtility.JavaScriptStringEncode(If(IsDBNull(oRow("DEN")), "", oRow("DEN"))) & """},"
                    End If
                End If
            Next
        End If

        If Len(sResultado) > 0 Then sResultado = Left(sResultado, Len(sResultado) - 1)
        Return sResultado
    End Function

#End Region

#Region "Presupuestos"

    <WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function Obtener_DatosPresupuesto(ByVal IDPres As String, ByVal Campo As String, ByVal EsCabecera As String, ByVal Instancia As String, ByVal Solicitud As String, ByVal Articulo As String, ByVal cadenaOrgComp As String) As JSonResponse
        Dim objJson As New JSonResponse
        Dim CadenaOrgCompras As String = cadenaOrgComp
        objJson.Mensaje = ""
        If CadenaOrgCompras <> Nothing Then
            Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
            Dim oOrganizacionesCompras As FSNServer.OrganizacionesCompras
            Dim bTodasSonMismoTipoERP As Boolean
            Dim sOrgCompra As String

            oOrganizacionesCompras = FSWSServer.Get_Object(GetType(FSNServer.OrganizacionesCompras))
            bTodasSonMismoTipoERP = oOrganizacionesCompras.OrgsAdmitenMultiplesPresupuestos(cadenaOrgComp)

            If bTodasSonMismoTipoERP Then
                If InStr(CadenaOrgCompras, ",") > 0 Then
                    sOrgCompra = Mid(CadenaOrgCompras, 1, InStr(CadenaOrgCompras, ",") - 1) 'como son del mismo tipo, pasamos la primera OrgCompra para ver si acepta o no multiples pres
                Else
                    sOrgCompra = CadenaOrgCompras
                End If
                sOrgCompra = sOrgCompra.Replace("'", "")
                objJson.OrgCompras = sOrgCompra
            Else
                Dim alerta As String
                Dim iTipo As Integer
                Dim mcadenapresupuestosplural As String
                Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
                Dim sIdi As String = Session("FSN_User").Idioma.ToString()

                oDict.LoadData(TiposDeDatos.ModulosIdiomas.Presupuestos, sIdi)
                Dim oTextos As DataTable = oDict.Data.Tables(0)

                Select Case EsCabecera
                    Case TiposDeDatos.TipoCampoGS.PRES1
                        iTipo = 1
                    Case TiposDeDatos.TipoCampoGS.Pres2
                        iTipo = 2
                    Case TiposDeDatos.TipoCampoGS.Pres3
                        iTipo = 3
                    Case TiposDeDatos.TipoCampoGS.Pres4
                        iTipo = 4
                End Select

                Dim oUnidadesCadenaPresupuesto As FSNServer.UnidadesOrg 'La utilizo para obtener la cadena de BD del tipo de presupuesto seleccionado
                oUnidadesCadenaPresupuesto = FSWSServer.Get_Object(GetType(FSNServer.UnidadesOrg))
                oUnidadesCadenaPresupuesto.CargarCadenaPresupuesto(iTipo, sIdi)
                mcadenapresupuestosplural = oUnidadesCadenaPresupuesto.Data.Tables(0).Rows(0).Item(1)
                alerta = oTextos.Rows(34).Item(1) & " " & mcadenapresupuestosplural & "!"
                objJson.Mensaje = alerta
            End If
        Else
            objJson.OrgCompras = ""
        End If
        objJson.Input = IDPres
        objJson.Campo = Campo
        objJson.EsCabecera = EsCabecera
        objJson.Instancia = Instancia
        objJson.Solicitud = Solicitud
        objJson.Articulo = Articulo
        Return objJson
    End Function

    Public Class JSonResponse
        Private _input As String
        Private _campo As String
        Private _EsCabecera As String
        Private _Instancia As String
        Private _Solicitud As String
        Private _Articulo As String
        Private _OrgCompras As String
        Private _mensaje As String

        Public Property Input() As String
            Get
                Return _input
            End Get
            Set(ByVal value As String)
                _input = value
            End Set
        End Property
        Public Property Campo() As String
            Get
                Return _campo
            End Get
            Set(ByVal value As String)
                _campo = value
            End Set
        End Property
        Public Property EsCabecera() As String
            Get
                Return _EsCabecera
            End Get
            Set(ByVal value As String)
                _EsCabecera = value
            End Set
        End Property
        Public Property Instancia() As String
            Get
                Return _Instancia
            End Get
            Set(ByVal value As String)
                _Instancia = value
            End Set
        End Property
        Public Property Solicitud() As String
            Get
                Return _Solicitud
            End Get
            Set(ByVal value As String)
                _Solicitud = value
            End Set
        End Property
        Public Property Articulo() As String
            Get
                Return _Articulo
            End Get
            Set(ByVal value As String)
                _Articulo = value
            End Set
        End Property
        Public Property OrgCompras() As String
            Get
                Return _OrgCompras
            End Get
            Set(ByVal value As String)
                _OrgCompras = value
            End Set
        End Property
        Public Property Mensaje() As String
            Get
                Return _mensaje
            End Get
            Set(ByVal value As String)
                _mensaje = value
            End Set
        End Property

        Public Sub New()

        End Sub
    End Class
#End Region

#Region "Servicio"
    ''' <summary>
    ''' Procedimiento que carga los datos del servicio
    ''' </summary>
    ''' <param name="servicio">Id del servicio</param>
    ''' <param name="campoId">Id del campo</param>
    ''' <param name="Instancia">Instancia</param>
    ''' <param name="Peticionario">Usuario conectado</param>
    ''' <returns>JSonServicio</returns>
    ''' <remarks>Llamada desde:jsAlta.js/ ; Tiempo máximo: 0,2sg</remarks>
    <WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function Obtener_Servicio(ByVal servicio As String, ByVal campoId As String, ByVal Instancia As Long, ByVal Peticionario As String,
                                     ByVal Empresa As Integer, ByVal Proveedor As String, ByVal Contacto As Integer,
                                     ByVal Moneda As String, ByVal FechaInicio As String, ByVal FechaExpiracion As String,
                                     ByVal Alerta As Integer, ByVal RepetirEmail As Integer, ByVal ImporteDesde As Double, ByVal ImporteHasta As Double) As JSonServicio
        Dim objJson As New JSonServicio
        Dim url As String
        Dim paramEnt As DataTable
        Dim paramSal As DataTable
        Dim i As Integer

        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oServicio As FSNServer.Servicio
        Dim oParam As FSNServer.ParametroServicio
        Dim paramsEnt As New Dictionary(Of String, FSNServer.ParametroServicio)
        Dim paramsSal As New Dictionary(Of String, FSNServer.ParametroServicio)
        Dim paramsCamposGenericos As New Dictionary(Of String, FSNServer.ParametroServicio)

        oServicio = FSWSServer.Get_Object(GetType(FSNServer.Servicio))
        oServicio.LoadData(servicio, campoId, Instancia, Peticionario)
        url = oServicio.Data.Tables("URL").Rows(0).Item("URL")
        paramEnt = oServicio.Data.Tables("PARAM_ENTRADA")
        paramSal = oServicio.Data.Tables("PARAM_SALIDA")


        For i = 0 To paramEnt.Rows.Count - 1
            oParam = New FSNServer.ParametroServicio
            oParam.Den = paramEnt.Rows(i).Item("DEN")
            oParam.Directo = paramEnt.Rows(i).Item("DIRECTO")
            oParam.valorText = DBNullToStr(paramEnt.Rows(i).Item("VALOR_TEXT"))
            oParam.valorNum = DBNullToDbl(paramEnt.Rows(i).Item("VALOR_NUM"))
            oParam.valorFec = DateToigDate(DBNullToSomething(paramEnt.Rows(i).Item("VALOR_FEC")))
            oParam.valorBool = DBNullToInteger(paramEnt.Rows(i).Item("VALOR_BOOL"))
            oParam.Tipo = DBNullToInteger(paramEnt.Rows(i).Item("TIPO"))
            oParam.TipoCampo = DBNullToInteger(paramEnt.Rows(i).Item("TIPO_CAMPO"))
            oParam.EsSubcampo = DBNullToInteger(paramEnt.Rows(i).Item("ES_SUBCAMPO"))
            oParam.Campo = DBNullToInteger(paramEnt.Rows(i).Item("CAMPO"))
            paramsEnt(oParam.Den) = oParam
        Next

        For Each campoEntradaGenerico As Integer In {TipoCampo.Peticionario, TipoCampo.Moneda, TipoCampo.Proveedor, TipoCampo.Contacto,
                                                    TipoCampo.Empresa, TipoCampo.FechaInicio, TipoCampo.FechaExpiracion,
                                                    TipoCampo.MostrarAlerta, TipoCampo.EnviarMail, TipoCampo.ImporteDesde, TipoCampo.ImporteHasta}
            oParam = New FSNServer.ParametroServicio
            oParam.Den = ""
            oParam.Directo = 0
            oParam.TipoCampo = campoEntradaGenerico
            oParam.EsSubcampo = 0
            oParam.Campo = 0
            oParam.valorText = ""
            oParam.valorNum = 0
            oParam.valorFec = New Date()
            oParam.valorBool = False
            Select Case campoEntradaGenerico
                Case TipoCampo.Peticionario
                    oParam.Den = "Peticionario"
                    oParam.Tipo = 6
                    oParam.valorText = If(oServicio.Data.Tables.Count > 3, oServicio.Data.Tables("PARAM_PETICIONARIO").Rows(0).Item("PETICIONARIO").ToString(), Peticionario)
                Case TipoCampo.Moneda
                    oParam.Den = "Moneda"
                    oParam.Tipo = 6
                    oParam.valorText = If(oServicio.Data.Tables.Count > 4, oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("MON").ToString(), Moneda)
                Case TipoCampo.Proveedor
                    oParam.Den = "Proveedor"
                    oParam.Tipo = 6
                    oParam.valorText = If(oServicio.Data.Tables.Count > 4, oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("PROVE").ToString(), Proveedor)
                Case TipoCampo.Contacto
                    oParam.Den = "Contacto"
                    oParam.Tipo = 2
                    oParam.valorNum = If(oServicio.Data.Tables.Count > 4, DBNullToDbl(oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("CONTACTO")), Contacto)
                Case TipoCampo.Empresa
                    oParam.Den = "Empresa"
                    oParam.Tipo = 6
                    oParam.valorNum = If(oServicio.Data.Tables.Count > 4, oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("EMPRESA").ToString(), Empresa)
                Case TipoCampo.FechaInicio
                    oParam.Den = "FechaInicio"
                    oParam.Tipo = 3
                    oParam.valorFec = If(oServicio.Data.Tables.Count > 4, DBNullToSomething(oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("FECINICIO")), FechaInicio)
                Case TipoCampo.FechaExpiracion
                    oParam.Den = "FechaExpiracion"
                    oParam.Tipo = 3
                    oParam.valorFec = If(oServicio.Data.Tables.Count > 4, DBNullToSomething(oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("FECFIN")), FechaExpiracion)
                Case TipoCampo.MostrarAlerta
                    oParam.Den = "MostrarAlerta"
                    oParam.Tipo = 2
                    oParam.valorNum = If(oServicio.Data.Tables.Count > 4, DBNullToDbl(oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("ALERTA")), Alerta)
                Case TipoCampo.EnviarMail
                    oParam.Den = "RepetirMail"
                    oParam.Tipo = 2
                    oParam.valorNum = If(oServicio.Data.Tables.Count > 4, DBNullToDbl(oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("REPETIR_EMAIL")), RepetirEmail)
                Case TipoCampo.ImporteDesde
                    oParam.Den = "ImporteDesde"
                    oParam.Tipo = 2
                    oParam.valorNum = If(oServicio.Data.Tables.Count > 4, DBNullToDbl(oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("IMPORTE_DESDE")), ImporteDesde)
                Case TipoCampo.ImporteHasta
                    oParam.Den = "ImporteHasta"
                    oParam.Tipo = 2
                    oParam.valorNum = If(oServicio.Data.Tables.Count > 4, DBNullToDbl(oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("IMPORTE_HASTA")), ImporteHasta)
                Case Else
                    oParam.Den = ""
                    oParam.valorText = ""
            End Select
            paramsCamposGenericos(oParam.Den) = oParam
        Next

        For i = 0 To paramSal.Rows.Count - 1
            oParam = New FSNServer.ParametroServicio
            oParam.Den = paramSal.Rows(i).Item("DEN")
            oParam.IndError = paramSal.Rows(i).Item("INDICADOR_ERROR")
            oParam.Campo = paramSal.Rows(i).Item("CAMPO")
            paramsSal(oParam.Den) = oParam
        Next

        objJson.Url = url
        objJson.ParamsEntrada = paramsEnt
        objJson.ParamsSalida = paramsSal
        objJson.ParamsCamposGenericos = paramsCamposGenericos

        Return objJson
    End Function
    Public Class JSonServicio
        Private _url As String
        Public Property Url() As String
            Get
                Return _url
            End Get
            Set(ByVal value As String)
                _url = value
            End Set
        End Property
        Private _ParamsEntrada As Dictionary(Of String, FSNServer.ParametroServicio)
        Public Property ParamsEntrada() As Dictionary(Of String, FSNServer.ParametroServicio)
            Get
                Return _ParamsEntrada
            End Get
            Set(ByVal value As Dictionary(Of String, FSNServer.ParametroServicio))
                _ParamsEntrada = value
            End Set
        End Property
        Private _ParamsSalida As Dictionary(Of String, FSNServer.ParametroServicio)
        Public Property ParamsSalida() As Dictionary(Of String, FSNServer.ParametroServicio)
            Get
                Return _ParamsSalida
            End Get
            Set(ByVal value As Dictionary(Of String, FSNServer.ParametroServicio))
                _ParamsSalida = value
            End Set
        End Property
        Private _ParamsCamposGenericos As Dictionary(Of String, FSNServer.ParametroServicio)
        Public Property ParamsCamposGenericos() As Dictionary(Of String, FSNServer.ParametroServicio)
            Get
                Return _ParamsCamposGenericos
            End Get
            Set(ByVal value As Dictionary(Of String, FSNServer.ParametroServicio))
                _ParamsCamposGenericos = value
            End Set
        End Property

        Public Sub New()
        End Sub
    End Class
#End Region

#Region "Articulo"

    <Services.WebMethod(EnableSession:=True)>
    <Script.Services.ScriptMethod()>
    Public Function Detalle_Articulo(ByVal codArt As String, ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String) As Object
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        Dim oArts As FSNServer.Articulos
        Dim oUONData As FSNServer.UnidadesOrg
        Dim oAtributos As List(Of Fullstep.FSNServer.UnidadesOrg.Atributo)
        Dim oAtribUONDistintas As List(Of Fullstep.FSNServer.UnidadesOrg.Atributo)
        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.BusquedaArticulos, FSNUser.Idioma)
        Dim fila As String
        Dim txtUO As String = ""
        Dim lblUon() As String
        Dim numUon As Integer
        Dim result As New Dictionary(Of String, String)

        result("lblTitulo") = oDict.Data.Tables(0).Rows(4).Item(1) 'Artículo
        result("lblSubTitulo") = oDict.Data.Tables(0).Rows(61).Item(1) 'Detalle de artículo
        result("lblMaterial") = oDict.Data.Tables(0).Rows(1).Item(1) & ": " 'Material: 
        result("lblCodigo") = oDict.Data.Tables(0).Rows(2).Item(1) & ": " 'Código: 
        result("lblDenominacion") = oDict.Data.Tables(0).Rows(3).Item(1) & ": " 'Denominación: 

        'Saco la información del artículo para la cabecera del detalle
        oArts = FSNServer.Get_Object(GetType(FSNServer.Articulos))
        oArts.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil, sCod:=codArt, AccesoFSEP:=FSNServer.TipoAcceso.gbAccesoFSEP)
        If oArts.Data.Tables.Count > 0 Then
            If oArts.Data.Tables(0).Rows.Count > 0 Then
                result("txtMaterial") = oArts.Data.Tables(0).Rows(0).Item("GMN4") & " - " & oArts.Data.Tables(0).Rows(0).Item("DENGMN")
                result("txtCodigo") = oArts.Data.Tables(0).Rows(0).Item("COD")
                result("txtDenominacion") = oArts.Data.Tables(0).Rows(0).Item("DEN")
            End If
        End If
        oUONData = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
        Dim hsUON As New HashSet(Of String)
        Dim aux As String
        Dim listaUON(,) As String

        If UON1 <> "" Then
            oAtribUONDistintas = oUONData.CargarAtributosArticuloDeUnidadOrg(codArt, UON1, UON2, UON3)
            'Recojo los atributos posibles y cargo las UONs en un HashSet para evitar duplicidades
            If oAtribUONDistintas.Count > 0 Then
                For Each oAtributo As FSNServer.UnidadesOrg.Atributo In oAtribUONDistintas
                    aux = oAtributo.UON1
                    If DBNullToStr(oAtributo.UON2) <> "" Then
                        aux += "-" + oAtributo.UON2
                        If DBNullToStr(oAtributo.UON3) <> "" Then
                            aux += "-" + oAtributo.UON3
                        End If
                    End If
                    hsUON.Add(aux)
                Next
                'Si me devuelve de más de una UON tendré que mostrarlo en varias tablas
                If hsUON.Count > 1 Then
                    numUon = hsUON.Count
                    Dim i, j As Integer
                    Dim strArr() As String
                    ReDim Preserve listaUON(3, numUon)
                    For i = 0 To numUon - 1
                        strArr = (hsUON.ElementAt(i)).Split("-")
                        For j = 1 To strArr.Length
                            listaUON(j, i) = Trim(strArr(j - 1))
                        Next
                        ReDim Preserve lblUon(i + 1)
                        lblUon(i) = oUONData.CargarDenUnidadOrganizativa(listaUON(1, i), listaUON(2, i), listaUON(3, i))
                    Next
                Else
                    'Si hay una UON seleccionada en la solicitud recoge su código y denominación
                    ReDim listaUON(3, 1)
                    listaUON(1, 0) = Trim(UON1)
                    listaUON(2, 0) = Trim(UON2)
                    listaUON(3, 0) = Trim(UON3)
                    ReDim lblUon(1)
                    lblUon(0) = oUONData.CargarDenUnidadOrganizativa(Trim(UON1), Trim(UON2), Trim(UON3))
                    numUon = 1
                End If
            End If
        ElseIf UON1 = "" Then
            'Si no hay UON seleccionada en la solicitud carga todas las UONs posibles
            oUONData.CargarUnidadconArticulo(codArt)
            If oUONData.Data.Tables(0).Rows.Count > 0 Then
                numUon = 0
                For i As Integer = 0 To oUONData.Data.Tables(0).Rows.Count - 1
                    ReDim Preserve listaUON(3, i)
                    If DBNullToSomething(oUONData.Data.Tables(0).Rows(i).Item("UON1")) <> Nothing Then
                        txtUO = oUONData.Data.Tables(0).Rows(i).Item("UON1")
                        listaUON(1, i) = oUONData.Data.Tables(0).Rows(i).Item("UON1")
                        If DBNullToSomething(oUONData.Data.Tables(0).Rows(i).Item("UON2")) <> Nothing Then
                            txtUO += "-" + oUONData.Data.Tables(0).Rows(i).Item("UON2")
                            listaUON(2, i) = oUONData.Data.Tables(0).Rows(i).Item("UON2")
                            If DBNullToSomething(oUONData.Data.Tables(0).Rows(i).Item("UON3")) <> Nothing Then
                                txtUO += "-" + oUONData.Data.Tables(0).Rows(i).Item("UON3")
                                listaUON(3, i) = oUONData.Data.Tables(0).Rows(i).Item("UON3")
                            End If
                        End If
                        txtUO += "-" + oUONData.Data.Tables(0).Rows(i).Item("DENOMINACION")
                    End If
                    ReDim Preserve lblUon(i + 1)
                    lblUon(i) = txtUO
                    numUon += 1
                Next
            End If
        End If
        result("numUon") = numUon
        For j As Integer = 0 To numUon - 1
            result("lblUON" & j.ToString) = lblUon(j)
        Next

        'Saco los atributos para el articulo y la UON seleccionados o para las UON de articulo si no hubiera ninguna seleccionada
        For i As Integer = 0 To numUon - 1
            oAtributos = oUONData.CargarAtributosArticuloDeUnidadOrg(codArt, listaUON(1, i), listaUON(2, i), listaUON(3, i))
            If oAtributos.Count > 0 Then
                fila = ""
                For Each oAtributo As FSNServer.UnidadesOrg.Atributo In oAtributos
                    If oAtributo.Tipo = 4 Then
                        If oAtributo.Valor = "SI" Then
                            oAtributo.Valor = oDict.Data.Tables(0).Rows(12).Item(1)
                        ElseIf oAtributo.Valor = "NO" Then
                            oAtributo.Valor = oDict.Data.Tables(0).Rows(13).Item(1)
                        End If
                    End If
                    fila = fila & "<tr class='SinSalto'><td class='Negrita'>" & oAtributo.Denominacion & "</td><td>" & oAtributo.Valor & "</td></tr>"
                Next
                result("tabla" & i.ToString) = fila
                result("lblAtributos" & i.ToString) = oDict.Data.Tables(0).Rows(62).Item(1) 'Atributos
            Else
                result("tabla" & i.ToString) = ""
            End If
        Next

        Return result

    End Function
#End Region

#Region "Comprador"
    ' Función que comprueba si un comprador es válido
    <System.Web.Services.WebMethod(True)>
    Public Function Comprobar_Comprador(ByVal comprador As String, ByVal valor As String, ByVal tipo As String) As System.String
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim oPersonas As FSNServer.Personas
        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        Dim sIdi As String = Session("FSN_User").Idioma.ToString()
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.Usuarios, sIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        Dim mensaje As String = ""
        Dim m_sUO As String = Nothing
        Dim m_sMaterial As String = Nothing
        Select Case tipo
            Case "UON"
                m_sUO = valor
                mensaje = oTextos.Rows(20).Item(1)
            Case "OrgComp"
                m_sUO = valor
                mensaje = oTextos.Rows(21).Item(1)
            Case "Mat"
                m_sMaterial = valor
                mensaje = oTextos.Rows(22).Item(1)
        End Select
        oPersonas = FSNServer.Get_Object(GetType(FSNServer.Personas))
        oPersonas.LoadData(sCod:=comprador, bPM:=True, bCtlBajaLog:=True, sEqp:="", sUO:=m_sUO, sMaterial:=m_sMaterial)
        If oPersonas.Data.Tables(0).Rows.Count > 0 Then
            mensaje = ""
        End If
        Return mensaje

    End Function
#End Region

#Region "Adjudicaciones"
    <System.Web.Services.WebMethod(True)>
    Public Function Devolver_Adjudicaciones_Linea_Instancia(ByVal lInstancia As Long, ByVal lCampo As Long, ByVal lLinea As Long, ByVal bPreadj As Boolean) As Object
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim oInstancias As FSNServer.Instancias = FSNServer.Get_Object(GetType(Fullstep.FSNServer.Instancias))
        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        Dim oUser As FSNServer.User = Session("FSN_User")
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.UltimasAdjudicaciones, oUser.Idioma)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        Dim textos As New List(Of String)
        If bPreadj Then
            textos.Add(oDict.GetText(14))
        Else
            textos.Add(oDict.GetText(12))
        End If
        textos.Add(oDict.GetText(15)) 'Detalle
        textos.Add(oDict.GetText(13))
        Dim textosCabeceraTabla As New List(Of String)
        If bPreadj Then
            textosCabeceraTabla.Add(oDict.GetText(16)) 'Fecha
            textosCabeceraTabla.Add(oDict.GetText(17)) 'Usuario
        End If
        textosCabeceraTabla.Add(oDict.GetText(11)) 'Proceso
        textosCabeceraTabla.Add(oDict.GetText(4)) 'Proveedor
        textosCabeceraTabla.Add(oDict.GetText(5)) 'Denominación de proveedor
        textosCabeceraTabla.Add(oDict.GetText(6)) 'Precio
        textosCabeceraTabla.Add(oDict.GetText(10)) 'Moneda
        textosCabeceraTabla.Add(oDict.GetText(7)) 'Cantidad
        textosCabeceraTabla.Add("%")
        textosCabeceraTabla.Add(oDict.GetText(8)) 'Fecha ini
        textosCabeceraTabla.Add(oDict.GetText(9)) 'Fecha fin

        Dim adjs As New List(Of Object)
        For Each dr As DataRow In oInstancias.Devolver_Adjudicaciones_Linea_Instancia(lInstancia, lCampo, lLinea, bPreadj).Tables(0).Rows
            Dim adj As Object
            If bPreadj Then
                adj = New With {
                .Fecha = FormatDateTime(DateAdd(DateInterval.Hour, TimeZoneInfo.Local.GetUtcOffset(Now).Hours, dr("F_UTC0"))) _
                , .Usu = dr("NOMUSU") _
                , .proce = dr("ANYO") & "/" & dr("GMN1_PROCE") & "/" & dr("PROCE") _
                , .prove = dr("PROVE") _
                , .den = dr("DEN") _
                , .precio = FSNLibrary.FormatNumber(dr("PREC_VALIDO"), oUser.NumberFormat) _
                , .mon = dr("MON") _
                , .cantidad = FSNLibrary.FormatNumber(DBNullToDbl(dr("CANT")), oUser.NumberFormat) _
                , .porcen = FSNLibrary.FormatNumber(DBNullToDbl(dr("PORCEN")), oUser.NumberFormat) _
                , .fecini = FormatDate(dr("FECINI"), oUser.DateFormat) _
                , .fecfin = FormatDate(dr("FECFIN"), oUser.DateFormat) _
                , .Ultimo = dr("ULTIMO")
            }
            Else
                adj = New With {
                .proce = dr("ANYO") & "/" & dr("GMN1_PROCE") & "/" & dr("PROCE") _
                , .prove = dr("PROVE") _
                , .den = dr("DEN") _
                , .precio = FSNLibrary.FormatNumber(dr("PREC_VALIDO"), oUser.NumberFormat) _
                , .mon = dr("MON") _
                , .cantidad = FSNLibrary.FormatNumber(DBNullToDbl(dr("CANT")), oUser.NumberFormat) _
                , .porcen = FSNLibrary.FormatNumber(DBNullToDbl(dr("PORCEN")), oUser.NumberFormat) _
                , .fecini = FormatDate(dr("FECINI"), oUser.DateFormat) _
                , .fecfin = FormatDate(dr("FECFIN"), oUser.DateFormat)
            }
            End If

            adjs.Add(adj)
        Next
        Return {textos, textosCabeceraTabla, adjs}

    End Function
#End Region

#Region "CentrosCoste-Partidas"
    ''' <summary>
    ''' Carga las partidas de un centro dado. En el caso de haber solo una partida devuelve su código y denominación.
    ''' </summary>
    ''' <param name="sPRES5">Texto con el presupuesto de nivel 0 (Raiz)</param>
    ''' <param name="sCodCentroCoste">Centro del q cargar las partidas</param>
    ''' <returns>Objeto con el codigo y denominación de la partida en caso de haya solo una partida. Eoc lleva dos strings vacios</returns>
    ''' <remarks>Llamada desde: copiarFilaVacia         borrarPartidaoActivoRelacionado; tiempo máximo:0,2</remarks>
    <WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function Obtener_PartidasCentro(ByVal sPRES5 As String, ByVal sCodCentroCoste As String) As JsonCentroPartida
        Dim objJson As New JsonCentroPartida
        Dim oPartida As FSNServer.Partida
        Dim dPartidas As DataSet
        Dim sValor, sDenominacion As String
        sDenominacion = ""
        sValor = ""
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        oPartida = FSNServer.Get_Object(GetType(FSNServer.Partida))
        Dim sIdi As String = FSNUser.Idioma.ToString()

        Dim cPartidas As Fullstep.FSNServer.PRES5
        cPartidas = FSNServer.Get_Object(GetType(FSNServer.PRES5))

        Dim bPlurianual As Boolean = False
        If Not Session("FSSMPargen") Is Nothing Then
            Dim configuracion As FSNServer.PargenSM
            configuracion = Session("FSSMPargen").Item(sPRES5)
            If Not configuracion Is Nothing Then
                bPlurianual = configuracion.Plurianual
            End If
        End If

        If sCodCentroCoste <> "" Then
            dPartidas = cPartidas.Partidas_LoadData(sPRES5, FSNUser.Cod, sIdi, bPlurianual, sCodCentroCoste)
            sCodCentroCoste = sCodCentroCoste & "#"
        Else
            dPartidas = cPartidas.Partidas_LoadData(sPRES5, FSNUser.Cod, sIdi, bPlurianual)
        End If

        'Del stored viene indicado como IMPUTABLE=1 la partidas a nivel minimo seleccionable. Este nivel es un valor entre 1 y 4. Del stored las partidas llegan a partir de la tabla 5.
        Dim iNivelMinimo As Integer
        For iNivel As Integer = 5 To 8
            If dPartidas.Tables(iNivel).Rows.Count > 0 Then
                If dPartidas.Tables(iNivel).Rows(0).Item("IMPUTABLE") = 1 Then
                    iNivelMinimo = iNivel
                    Exit For
                End If
            End If
        Next

        If iNivelMinimo <> 0 AndAlso dPartidas.Tables(iNivelMinimo).Rows.Count = 1 Then
            If sCodCentroCoste = "" Then
                sCodCentroCoste = dPartidas.Tables(iNivelMinimo).Rows(0).Item("UON1") & "#"
                If DBNullToStr(dPartidas.Tables(iNivelMinimo).Rows(0).Item("UON2")) <> "" Then sCodCentroCoste = sCodCentroCoste & dPartidas.Tables(iNivelMinimo).Rows(0).Item("UON2") & "#"
                If DBNullToStr(dPartidas.Tables(iNivelMinimo).Rows(0).Item("UON3")) <> "" Then sCodCentroCoste = sCodCentroCoste & dPartidas.Tables(iNivelMinimo).Rows(0).Item("UON3") & "#"
                If DBNullToStr(dPartidas.Tables(iNivelMinimo).Rows(0).Item("UON4")) <> "" Then sCodCentroCoste = sCodCentroCoste & dPartidas.Tables(iNivelMinimo).Rows(0).Item("UON4") & "#"
            End If

            sValor = sCodCentroCoste & dPartidas.Tables(iNivelMinimo).Rows(0).Item("COD")
            sDenominacion = dPartidas.Tables(iNivelMinimo).Rows(0).Item("DEN")
        End If
        oPartida = Nothing
        objJson.Texto = sValor
        objJson.Denominacion = sDenominacion
        Return objJson
    End Function

    Public Class JsonCentroPartida
        Private _text As String
        Public Property Texto() As String
            Get
                Return _text
            End Get
            Set(ByVal value As String)
                _text = value
            End Set
        End Property
        Private _den As String
        Public Property Denominacion() As String
            Get
                Return _den
            End Get
            Set(ByVal value As String)
                _den = value
            End Set
        End Property
        Public Sub New()
        End Sub
    End Class

    <WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function Obtener_CentroCoste(ByVal bverUON As Boolean) As JsonCentroPartida

        Dim objJson As New JsonCentroPartida
        Dim oCentrosCoste As FSNServer.CentrosCoste
        Dim dCentros As DataSet
        Dim sValor, sDenominacion As String
        sDenominacion = ""
        sValor = ""
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim FSNUser As FSNServer.User = Session("FSN_User")
        oCentrosCoste = FSNServer.Get_Object(GetType(FSNServer.CentrosCoste))
        Dim sIdi As String = FSNUser.Idioma.ToString()
        dCentros = oCentrosCoste.LoadData(FSNUser.Cod, sIdi, bverUON)

        'Del stored viene indicado con valor centro_sm si el centro de coste está a ese nivel 
        Dim iLevelCentro As Integer = 1
        For iLevel As Integer = 0 To dCentros.Tables.Count - 1
            If dCentros.Tables(iLevel).Rows.Count > 0 Then
                If Not IsDBNull(dCentros.Tables(iLevel).Rows(0).Item("CENTRO_SM")) Then
                    iLevelCentro = iLevel
                    Exit For
                End If
            End If
        Next
        If dCentros.Tables(iLevelCentro).Rows.Count = 1 Then
            sDenominacion = dCentros.Tables(iLevelCentro).Rows(0).Item("DEN").ToString
            Select Case iLevelCentro
                Case 4
                    sValor = dCentros.Tables(iLevelCentro).Rows(0).Item("UON1") & "#" & dCentros.Tables(iLevelCentro).Rows(0).Item("UON2") & "#" & dCentros.Tables(iLevelCentro).Rows(0).Item("UON3") & "#" & dCentros.Tables(iLevelCentro).Rows(0).Item("COD")
                Case 3
                    sValor = dCentros.Tables(iLevelCentro).Rows(0).Item("UON1") & "#" & dCentros.Tables(iLevelCentro).Rows(0).Item("UON2") & "#" & dCentros.Tables(iLevelCentro).Rows(0).Item("COD")
                Case 2
                    sValor = dCentros.Tables(iLevelCentro).Rows(0).Item("UON1") & "#" & dCentros.Tables(iLevelCentro).Rows(0).Item("COD")
                Case 1
                    sValor = dCentros.Tables(iLevelCentro).Rows(0).Item("COD")
            End Select
        End If

        oCentrosCoste = Nothing
        objJson.Texto = sValor
        objJson.Denominacion = sDenominacion
        Return objJson
    End Function
#End Region

#Region "CambiosMoneda"
    <WebMethod(EnableSession:=True)>
    Public Function Obtener_CambioMoneda(ByVal sCodMoneda As String) As String
        Dim oMonedas As FSNServer.Monedas
        Dim sCambio As String = "1"
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")

        oMonedas = FSNServer.Get_Object(GetType(FSNServer.Monedas))
        oMonedas.LoadData("SPA", sCodMoneda, , True, True)

        If oMonedas.Data.Tables(0).Rows.Count = 1 Then
            sCambio = oMonedas.Data.Tables(0).Rows(0).Item("EQUIV").ToString
        End If
        Return sCambio
    End Function
#End Region

#Region "Lineas Asociadas"
    ' Función que comprueba si una linea está asociada a un item o linea de pedido
    <System.Web.Services.WebMethod(True)>
    Public Function Comprobar_Lineas(ByVal idDesglose As String, ByVal numLinea As String) As System.String
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim oInstancia As FSNServer.Instancia = FSNServer.Get_Object(GetType(Fullstep.FSNServer.Instancia))
        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        Dim sIdi As String = Session("FSN_User").Idioma.ToString()

        Dim result As Boolean
        Dim mensaje As String = ""

        result = oInstancia.ComprobarLineasAsociadas(idDesglose, numLinea)
        If result = True Then
            mensaje = oDict.Texto(TiposDeDatos.ModulosIdiomas.AccionSolicitud, 11, sIdi)
        End If
        Return mensaje

    End Function
#End Region

End Class