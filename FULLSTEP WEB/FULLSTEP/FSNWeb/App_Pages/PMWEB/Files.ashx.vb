﻿Imports System.Web
Imports System.Web.Services

Public Class Files
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"

        Select Case context.Request.HttpMethod
            Case "GET"
                Dim filename As String = context.Request("file")
                Dim input As System.IO.FileStream = New System.IO.FileStream(ConfigurationManager.AppSettings("temp") & "\" & filename, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Dim buffer(input.Length - 1) As Byte
                Dim byteBuffer() As Byte
                input.Read(buffer, 0, buffer.Length)
                input.Close()
                byteBuffer = buffer

                context.Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                context.Response.AddHeader("Content-Disposition", "attachment;filename=" & filename)
                context.Response.BinaryWrite(byteBuffer)
                context.Response.End()
            Case "POST"
                Dim filename As String = Guid.NewGuid.ToString
                Dim file = context.Request.Files(0)

                file.SaveAs(ConfigurationManager.AppSettings("temp") & "\" & filename & ".xls")
                context.Response.Write(filename & ".xls")
        End Select
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class