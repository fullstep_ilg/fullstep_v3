<%@ Page CodeBehind="frames.aspx.vb" Language="vb" AutoEventWireup="false" Inherits="Fullstep.FSNWeb.frames" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">

	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
	</head>
	<frameset runat="server" id="fraWS" rows="0,*,0" frameborder="no">
		<frame id="fraWSHeader" src="blank.htm" scrolling="no"></frame>
		<frame id="fraWSMain" src=""></frame>
		<frame id="fraWSServer" src="blank.htm" scrolling="no"></frame>
	</frameset>
</html>