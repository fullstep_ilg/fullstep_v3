<%@ Page Language="vb" AutoEventWireup="false" Codebehind="eliminarNoConformidad.aspx.vb" Inherits="Fullstep.FSNWeb.eliminarNoConformidad"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<script>	
		/*''' <summary>
		''' Nos lleva a la pagina del detalle de la NoConformidad
		''' </summary>
		''' <param name="IdNoConf">no conformidad a cargar</param>
		''' <remarks>Llamada desde: Page_Load; Tiempo m�ximo: 0</remarks>*/		
		function Refrescar(IdNoConf)
		{
			if (IdNoConf==null) 
				window.open("<%=ConfigurationManager.AppSettings("rutaQA2008") &"NoConformidades/NoConformidades.aspx"%>","_parent");   
			else{
				var FechaLocal;
				FechaLocal = new Date();
				var otz = FechaLocal.getTimezoneOffset();	
				
				window.open("detalleNoConformidad.aspx?NoConformidad=" + IdNoConf + "&Otz=" + otz.toString(),"_parent");
			}
		}
	</script>
	
	<body>
		<form id="Form1" method="post" runat="server">
		</form>
	</body>
</html>
