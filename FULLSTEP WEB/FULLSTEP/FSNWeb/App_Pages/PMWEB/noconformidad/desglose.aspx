﻿<%@ Register TagPrefix="uc2" TagName="desglose" Src="../alta/desglose.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="desglose.aspx.vb" Inherits="Fullstep.FSNWeb.noconfdesglose"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	
	<script language="javascript" type="text/javascript">
		/* Revisado por: blp. Fecha: 03/10/2011
		''' <summary>
		''' javascript para cuando das a guardar cambios y cerrar 
		'''		Coge toda la información de los entrys y la mete/quita de los html de la pantalla q contiene al desglose.
		''' </summary>       
		''' <remarks>Llamada desde:	script/noconformidad/desglose.aspx page_load; Tiempo máximo: 0 </remarks>*/			
		function actualizarCampoDesgloseYCierre()
		{

			var bMensajePorMostrar = document.getElementById("bMensajePorMostrar")

			if (bMensajePorMostrar.value != "1") {
				if (comprobarTextoLargo() == false)
					return false		
				actualizarCampoDesglose()
				if (window.opener.document.getElementById("Notificar"))
					window.opener.document.getElementById("Notificar").value = document.getElementById("Notificar").value
				window.close() 		
			}else
				bMensajePorMostrar.value = "0";
			bMensajePorMostrar = null;
		}

		/*
		''' Revisado por: blp. Fecha: 03/10/2011
		''' función que lanza la función actualizarCampoDesglose y CalcularCamposCalculados de la página de origen
		''' llamada desde evento onclick del botón cmdCalcular. Máximo < 1 seg
		*/
		function CalcularDesgloseCalculados()
		{
			actualizarCampoDesglose()
			var p = window.opener
			p.CalcularCamposCalculados()
			p = null;
		}

		/* Revisado por: blp. Fecha: 03/10/2011
		''' <summary>
		''' javascript para cuando das a guardar cambios y cerrar 
		'''		Coge toda la información de los entrys y la mete/quita de los html de la pantalla q contiene al desglose.
		''' </summary>       
		''' <remarks>Llamada desde:	actualizarCampoDesgloseYCierre CalcularDesgloseCalculados; Tiempo máximo: 0 </remarks>*/	
		function actualizarCampoDesglose()
		{
			var p = window.opener
			var linea = new Array()
			var oDesglose = p.fsGeneralEntry_getById(document.getElementById("INPUTDESGLOSE").value)
			//uwtGrupos__ctl0_147_fsentry2293__numRows

			var oTbl = document.getElementById(arrDesgloses[0])
			var sPre = arrDesgloses[0].replace("tblDesglose", "")
			var iFilas = oTbl.rows.length - 1
			oTbl = null;
			var iMaxIndex = document.getElementById(sPre + "numRows").value
			var k;
			var oUni;
			var oMat;
			for (i = 1; i <= iMaxIndex; i++)
			{
				linea = new Array()
				if (document.getElementById(sPre + i.toString() + "_Deleted") || document.getElementById(sPre + "_" + i.toString() + "_Deleted") )
				{
					for (var oCampo in arrCampos)
					{
						var s = arrCampos[oCampo]
						var re = /fsentry/
						while (s.search(re)>=0)
						{
							s = s.replace(re,"fsdsentry_" + i.toString() + "_")
						}
						oCampo = fsGeneralEntry_getById(s)

						if (oCampo) {
							if (oCampo.tipoGS == 118)
								linea[linea.length] = s + "_*"
							else
								linea[linea.length] = s

							if (oCampo.tipo == 8) {
								k = linea.length
								linea[k] = new Array()
								linea[k][0] = document.getElementById(oCampo.id + "__hAct").value
								linea[k][1] = document.getElementById(oCampo.id + "__hNew").value
								linea[k][2] = oCampo.getValue();
							}
							else
								if (oCampo.tipoGS == 118) {
								k = linea.length
								linea[k] = new Array()
								linea[k][0] = oCampo.getDataValue()
								linea[k][1] = oCampo.articuloGenerico;
								linea[k][2] = oCampo.DenArticuloModificado;
								linea[k][3] = oCampo.codigoArticulo;
								if (oCampo.idEntryUNI) {
									oUni = fsGeneralEntry_getById(oCampo.idEntryUNI)
									if ((!oUni) && (oCampo.UnidadDependent)) { //La Unidad está oculta
										linea[linea.length] = oCampo.idEntryUNI
										linea[linea.length] = oCampo.UnidadDependent.value
									}
		                        }
		                        oMat = fsGeneralEntry_getById(oCampo.dependentfield)
		                        if (!oMat) //el material está oculto
		                        {
		                            linea[linea.length] = oCampo.dependentfield
		                            linea[linea.length] = oCampo.Dependent.value
		                        }
							}
							else
								if ((oCampo.tipoGS == 119) || (oCampo.tipoGS == 104)) {
								linea[linea.length] = oCampo.getDataValue()
								if (oCampo.tipoGS == 104 || oCampo.tipoGS == 119) {
									oMat = fsGeneralEntry_getById(oCampo.dependentfield)
									if (!oMat) //el material está oculto
									{
										linea[linea.length] = oCampo.dependentfield
										linea[linea.length] = oCampo.Dependent.value
									}
								}
								if (oCampo.idEntryUNI) {
									oUni = fsGeneralEntry_getById(oCampo.idEntryUNI)
									if ((!oUni) && (oCampo.UnidadDependent)) {
										linea[linea.length] = oCampo.idEntryUNI
										linea[linea.length] = oCampo.UnidadDependent.value
									}
								}
							}
							else
								if (oCampo.tipoGS == 45) {
								k = linea.length
								linea[k] = new Array()
								linea[k][0] = oCampo.getDataValue()
								linea[k][1] = oCampo.MonRepercutido;
								linea[k][2] = oCampo.CambioRepercutido;
							}
							else
								if ((oCampo.tipoGS == 0) && (oCampo.intro == 1) && ((oCampo.tipo == 2) || (oCampo.tipo == 3)))
									linea[linea.length] = oCampo.getValue()
								else {
									if (oCampo.tipo == 2) {
										if (oCampo.getDataValue() != null) {
											if (oCampo.tipoGS == 125) {
											    linea[linea.length] = oCampo.getDataValue()
											}
											else
												linea[linea.length] = oCampo.getDataValue().toString().replace(",", ".")
										} else
											linea[linea.length] = oCampo.getDataValue()
									}
									else
										if ((oCampo.tipoGS == 0) && (oCampo.intro == 1) && ((oCampo.tipo == 1) || (oCampo.tipo == 5) || (oCampo.tipo == 6))) {
										    k = linea.length
										    linea[k] = new Array()
										    linea[k][0] = oCampo.getDataValue()
										    linea[k][1] = oCampo.getValue()
									    }
						                else {
						                    if (oCampo.intro == 2) {
						                        linea[linea.length] = oCampo.getValue();
						                    }
						                    else {
						                        linea[linea.length] = oCampo.getDataValue();
						                    }
						                }
								}
						}					
					}
					oDesglose.addDesgloseLine(linea)
				}
				else
					{
					for (oCampo in arrCampos)
						{
						s=arrCampos[oCampo]
						re=/fsentry/
						while (s.search(re)>=0)
							{
							s = s.replace(re,"fsdsentry_" + i.toString() + "_")
							}
						oCampo = fsGeneralEntry_getById(s)
						linea[linea.length]=s
						}
						
					oDesglose.removeDesgloseLine(linea)
					}
					
				}
			oNumRows = p.document.getElementById(oDesglose.id + "__numRows")
			oNumRows.value = iFilas
			oNumRows = p.document.getElementById(oDesglose.id + "__numTotRows")
			oNumRows.value = iMaxIndex
			p = null;
			linea = null;
			oDesglose = null;
			sPre = null;
			iFilas = null;
			iMaxIndex = null;
			oCampo = null;
			s = null;
			re = null;
			k = null;
			oUni = null;
			oMat = null;
		}
			
		function localEval(s) {
		    eval(s);
		}
		function init() {
		    resize();
		}
		/* Revisado por: blp. Fecha: 03/10/2011
		''' <summary>
		''' Redimensionar los desgloses de la pantalla.
		''' </summary>   
		''' <remarks>Llamada desde: javascript; Tiempo máximo: 0</remarks>*/
		function resize()
			{
				var sDiv;
				for (i = 0; i < arrDesgloses.length; i++)
				{
				sDiv = arrDesgloses[i].replace("tblDesglose","divDesglose")
				document.getElementById(sDiv).style.width = parseFloat(document.body.offsetWidth) - 20 + 'px';
				document.getElementById(sDiv).style.height = getWindowSize("Height") - 100 + 'px';
			}
			sDiv = null;
		}
	</script>	
	<body onload="init()">
		<form id="frmAlta" method="post" runat="server">
			<asp:ScriptManager ID="ScriptManager1" runat="server">
                <CompositeScript>
                    <Scripts>
                        <asp:ScriptReference Name="ExtenderBase.BaseScripts.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Name="Common.Common.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Name="Compat.Timer.Timer.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Name="Animation.Animations.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Name="Animation.AnimationBehavior.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Name="PopupExtender.PopupBehavior.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Name="AutoComplete.AutoCompleteBehavior.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Path="../alta/js/jsAlta.js" />
                        <asp:ScriptReference Path="~/js/jquery/jquery.min.js" />
                        <asp:ScriptReference Path="~/js/jquery/jquery.tmpl.min.js" />
                        <asp:ScriptReference Path="~/js/jquery/jquery-migrate.min.js" />
                        <asp:ScriptReference Path="~/js/jsUtilities.js" />
                    </Scripts>
                </CompositeScript>
			</asp:ScriptManager>

			<input type="hidden" id="bMensajePorMostrar" name="bMensajePorMostrar" value="0">			
			<IFRAME id="iframeWSServer" style="Z-INDEX: 103; LEFT: 8px; VISIBILITY: hidden; POSITION: absolute; TOP: 200px"
				name="iframeWSServer" src="../blank.htm"></IFRAME>
			<table width="100%" border="0">
				<tr>
					<td width="70%"><asp:label id="lblTitulo" style="Z-INDEX: 101" runat="server" CssClass="captionBlue">Alta de solicitud tipo:</asp:label><asp:label id="lblTituloData" runat="server" CssClass="captionDarkBlue" Width="488px">Alta de solicitud tipo:</asp:label></td>
					<td align="left"><INPUT class="botonPMWEB" id="cmdCalcular" onclick="CalcularDesgloseCalculados()" type="button"
							value="Calcular" name="cmdCalcular" runat="server">
					</td>
					<td align="left"><asp:button id="cmdGuardar" runat="server" CssClass="botonPMWEB" Text="Button"></asp:button></td>
				</tr>
				<tr>
					<td colSpan="3"><asp:label id="lblSubTitulo" style="Z-INDEX: 102" runat="server" CssClass="captionBlue">Grupo: Nombre del campo</asp:label></td>
				</tr>
				<tr>
					<td colSpan="3"><uc2:desglose id="ucDesglose" runat="server"></uc2:desglose></td>
				</tr>
			</table>
			<div id="divDropDowns" style="Z-INDEX: 101; VISIBILITY: hidden; POSITION: absolute; TOP: 0px"></div>
			<input id="INPUTDESGLOSE" type="hidden" name="INPUTDESGLOSE" runat="server"> <input id="Instancia" type="hidden" name="Instancia" runat="server">
			<INPUT id="Solicitud" type="hidden" name="Solicitud" runat="server"><INPUT id="Notificar" style="Z-INDEX: 104; LEFT: 336px; POSITION: absolute; TOP: 16px"
				type="hidden" name="Notificar">
			<script type="text/javascript">
				if (window.onresize) {
					window.onresize = resize;
				}
				else {
					document.onresize = resize;
				}
			</script>
		</form>
	</body>
</html>
