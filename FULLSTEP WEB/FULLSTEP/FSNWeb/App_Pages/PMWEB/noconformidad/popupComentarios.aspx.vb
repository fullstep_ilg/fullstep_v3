Public Class popupComentarios
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents TextoApertura As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents TextoCierre As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Protected WithEvents phApertura As System.Web.UI.WebControls.PlaceHolder
    Protected WithEvents phCierre As System.Web.UI.WebControls.PlaceHolder
    Protected WithEvents lblComentarioApertura As System.Web.UI.WebControls.Label
    Protected WithEvents lblComentarioCierre As System.Web.UI.WebControls.Label
    Protected WithEvents btnCerrar As System.Web.UI.HtmlControls.HtmlInputButton

    ''' <summary>
    ''' Muestra los comentarios de la no conformidad. Solo los que haya. Respeta los saltos de linea.
    ''' </summary>
    ''' <param name="sender">pantalla</param>
    ''' <param name="e">parametro de sistema</param>            
    ''' <returns>Nada, es un evento de sistema</returns>
    ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oDict As FSNServer.Dictionary
        Dim oTextos As DataTable
        Dim sIdi As String

        Dim sTextoApertura As String = Replace(Request("textoApertura"), "@VBCRLF@", vbCrLf)
        Dim sTextoCierre As String = Replace(Request("textoCierre"), "@VBCRLF@", vbCrLf)

        Me.TextoApertura.Value = sTextoApertura
        Me.TextoCierre.Value = sTextoCierre

        sIdi = Session("FSN_User").Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        oDict = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.NoConformidades, sIdi)
        oTextos = oDict.Data.Tables(0)

        'Textos:
        Me.btnCerrar.Value = oTextos.Rows(33).Item(1)
        Me.lblComentarioApertura.Text = oTextos.Rows(34).Item(1)
        Me.lblComentarioCierre.Text = oTextos.Rows(35).Item(1)

        If Request("textoApertura") = Nothing Then
            Me.phApertura.Visible = False
        End If
        If Request("textoCierre") = Nothing Then
            Me.phCierre.Visible = False
        End If

    End Sub

End Class
