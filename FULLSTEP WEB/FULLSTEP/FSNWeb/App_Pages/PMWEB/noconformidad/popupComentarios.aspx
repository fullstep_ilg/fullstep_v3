<%@ Page Language="vb" AutoEventWireup="false" Codebehind="popupComentarios.aspx.vb" Inherits="Fullstep.FSNWeb.popupComentarios" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name=vs_defaultClientScript content="JavaScript">
		<meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body>
	<input type="hidden" id="TextoApertura" NAME="TextoApertura" runat="server"> <input type="hidden" id="TextoCierre" NAME="TextoCierre" runat="server">
	<asp:PlaceHolder ID="phApertura" Runat="server">
	<TABLE WIDTH=100% HEIGHT = 200 BORDER = 0>
	<tr><td><asp:Label id="lblComentarioApertura" runat="server" class="captionBlue"></asp:Label></td></tr>
	<tr><td width=100% HEIGHT =100%><TEXTAREA name=txtComentarioApertura READONLY id=txtComentarioApertura style='width:100%;height:200;' type=text><%=Server.HtmlEncode(TextoApertura.Value)%></TEXTAREA></td></tr>
	</table>
	</asp:PlaceHolder>
	<asp:PlaceHolder ID="phCierre" Runat="server">
	<TABLE WIDTH=100% HEIGHT = 200 BORDER = 0>
	<tr><td><asp:Label id="lblComentarioCierre" runat="server" class="captionBlue"></asp:Label></td></tr>
	<tr><td width=100% HEIGHT =100%><TEXTAREA name=txtComentarioCierre READONLY id=txtComentarioCierre style='width:100%;height:200;' type=text><%=Server.HtmlEncode(TextoCierre.Value)%></TEXTAREA></td></tr>
	</table>
	</asp:PlaceHolder>
	<p></p>
	<center><input type=button id="btnCerrar" onclick="window.close()" class="botonPMWEB" runat="server"></center>
	
  </body>
</html>
