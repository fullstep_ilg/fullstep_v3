
    Public Class cierreRevisor
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    Protected WithEvents lblId As System.Web.UI.WebControls.Label
    Protected WithEvents lblIdBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecAlta As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecAltaBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblProve As System.Web.UI.WebControls.Label
    Protected WithEvents lblProveBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecCierre As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecCierreBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblTituloNeg As System.Web.UI.WebControls.Label
    Protected WithEvents NoConformidad As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtComent As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents cmdAceptar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdCancelar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents lblaprobarpor As System.Web.UI.WebControls.Label
    Protected WithEvents LlblRevisor As System.Web.UI.WebControls.Label
    Protected WithEvents FSNPageHeader As Fullstep.FSNWebControls.FSNPageHeader
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private oNoConformidad As FSNServer.NoConformidad

    ''' <summary>
    ''' Pantalla para cerrar una no conformidad definitivamente pq es el revisor quien da el visto bueno al cierre.
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>Llamada desde: La propia p�gina, cada vez que se carga
    ''' Tiempo m�ximo:0,1</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oRevisor As FSNServer.User
        Dim srevisor As String

        Dim sIdi As String = FSNUser.Idioma.ToString()

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.CierreNoConformidadConRevisor

        'Textos de idiomas:
        Me.lblId.Text = Textos(0)
        Me.lblFecAlta.Text = Textos(2)
        Me.lblMensaje.Text = Textos(4)
        Me.cmdAceptar.Value = Textos(5)
        Me.cmdCancelar.Value = Textos(6)

        FSNPageHeader.TituloCabecera = Textos(9)

        Me.NoConformidad.Value = Request("NoConformidad")

        'Carga los datos de la no conformidad:
        oNoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
        oNoConformidad.Instancia = Request("Instancia")
        oNoConformidad.CargarDatosDesdeInstancia()


        'Datos de la no conformidad:
        If Request("Positivo") = True Then
            Me.lblTituloNeg.Visible = False
        Else
            Me.lblTituloNeg.Text = Textos(7)
        End If

        Me.lblIdBD.Text = oNoConformidad.Instancia
        Me.lblFecAltaBD.Text = FormatDate(oNoConformidad.FechaAlta, FSNUser.DateFormat)

        'Cargamos los datos dl revisor
        srevisor = Request("codRevisor")
        oRevisor = FSNServer.Get_Object(GetType(FSNServer.User))
        oRevisor.LoadUserData(srevisor)
        Me.lblaprobarpor.Text = Textos(10)
        Me.LlblRevisor.Text = oRevisor.Nombre

        oRevisor = Nothing
    End Sub

    Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Unload
        oNoConformidad = Nothing
    End Sub

    ''' <summary>
    ''' Cierra una noConformidad
    ''' Prepara la no conformidad para que la vea el revisor
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo m�ximo:0,3</remarks>
    Private Sub cmdAceptar_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.ServerClick
        Dim bPositivo As Boolean

        If Request("Positivo") = True Then
            bPositivo = True
        Else
            bPositivo = False
        End If

        Dim NotificadosProveedor As String = Replace(Request("NotifProv"), ";", "#")
        Dim NotificadosInternos As String = Replace(Request("NotifInt"), ";", "#")

        'Le indicamos q existe la figura del revisor
        'Si se trata de una NCE y est� en estado 8 no se pasa a True el param bRevisor porque ya ha pasado por ah� (el revisor ya ha aprobado): 
        'Flujo de estados de una NCE con revisor: 1->5->8->2,3        
        Dim bRevisor As Boolean = Not (oNoConformidad.Es_Noconformidad_Escalacion And oNoConformidad.Estado = TipoEstadoNoConformidad.CierrePosPendiente)
        oNoConformidad.CierreNoConformidad(bPositivo, FSNUser.Email, VacioToNothing(txtComent.InnerText), bRevisor, FSNUser.CodPersona, NotificadosProveedor, NotificadosInternos)
        Response.Redirect(ConfigurationManager.AppSettings("rutaQA2008") & "NoConformidades/NoConformidades.aspx")
    End Sub
End Class

