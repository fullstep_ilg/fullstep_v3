<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="cierreRevisor.aspx.vb" Inherits="Fullstep.FSNWeb.cierreRevisor"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<script>
		/*''' <summary>
		''' Volver al detalle por cancelaci�n de cierre
		''' </summary>
		''' <remarks>Llamada desde: cmdCancelar.onclick; Tiempo m�ximo: 0 </remarks>
		revisado por ilg(15/11/2011)
		*/				
		function IrADetalle()
		{
			var FechaLocal;
			FechaLocal = new Date();
			var otz = FechaLocal.getTimezoneOffset();		
		
			IdNoConf=document.getElementById("NoConformidad").value  
			window.open("detalleNoConformidad.aspx?NoConformidad=" + IdNoConf + "&Otz=" + otz.toString() + "&Volver=<%=Request("Volver")%>" ,"_self")
		}
		
		function validarLength(e,l)
		{
			if (e.value.length>l)
				return false
			else
				return true
		}

		function textCounter(e, l) 
		{ 
			if (e.value.length > l) 
				e.value = e.value.substring(0, l); 
		} 
	</script>	
	<body>
		<form id="Form1" method="post" runat="server">
           <asp:ScriptManager ID="sm" runat="server">
            </asp:ScriptManager>
			<input id="NoConformidad" style="Z-INDEX: 106; LEFT: 8px; WIDTH: 128px; POSITION: absolute; TOP: 108px; HEIGHT: 22px"
				type="hidden" size="16" name="NoConformidad" runat="server"/>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<uc1:menu ID="Menu1" runat="server" OpcionMenu="Calidad" OpcionSubMenu="NoConformidades">
					</uc1:menu> 
				</td>
			</tr>
			<tr>
				<td>
					<fsn:FSNPageHeader runat="server" ID="FSNPageHeader" UrlImagenCabecera="./images/noconformidades2.gif">
					</fsn:FSNPageHeader>
				</td>
			</tr>
			<tr>
				<td class="fondoCabecera" style="padding-left:10px;">
					<table class="bordeadoAzul" id="tblCabecera" style="z-index: 102; height: 70px" cellspacing="1" cellpadding="1" width="100%" border="0">
						<tr>
							<td colspan="4">
								<asp:Label ID="lblTituloNeg" Style="z-index: 104;" runat="server" CssClass="TituloSinDatos" Width="30%">lblTituloNeg</asp:Label>
							</td>
						</tr>
						<tr>
							<td class="fntRed" colspan="4">
								<asp:Label ID="lblaprobarpor" runat="server" Width="240px">DEl Cierre tiene q ser aprobado por</asp:Label>&nbsp;<asp:Label
									ID="LlblRevisor" runat="server" Width="202px">DNombre del revisor</asp:Label>
							</td>
						</tr>
						<tr>
							<td width="7%">
								<asp:Label ID="lblId" runat="server" CssClass="captionBlue" Width="100%"></asp:Label>
							</td>
							<td width="20%">
								<asp:Label ID="lblIdBD" runat="server" CssClass="label" Width="100%"></asp:Label>
							</td>
							<td width="7%">
								<asp:Label ID="lblFecAlta" runat="server" CssClass="captionBlue" Width="100%"></asp:Label>
							</td>
							<td width="20%">
								<asp:Label ID="lblFecAltaBD" runat="server" CssClass="label" Width="100%"></asp:Label>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="padding-left:10px;">
					<table id="tblMensajes" style="z-index: 103; width:100%;" cellspacing="1" cellpadding="1" border="0">
						<tr>
							<td height="5%">
								<asp:Label ID="lblMensaje" runat="server" CssClass="fntLogin" Width="100%"></asp:Label>
							</td>
						</tr>
						<tr>
							<td style="height:250px;">
								<textarea onkeypress="return validarLength(this,500)" id="txtComent" onkeydown="textCounter(this,500)"
									onkeyup="textCounter(this,500)" style="width: 95%; height: 100%" name="txtComentGestor"
									onchange="return validarLength(this,500)" runat="server"></textarea>
							</td>
						</tr>
						<tr>
							<td align="center" height="15%">
								<table id="tblBotones" cellspacing="1" cellpadding="1" border="0" style="width:300px;">
									<tr>
										<td>
											<input class="botonPMWEB" id="cmdAceptar" type="button" value="cmdAceptar" name="cmdAceptar"
												runat="server"/>
										</td>
										<td>
											<input class="botonPMWEB" id="cmdCancelar" onclick="IrADetalle()" type="button" value="cmdCancelar"
												name="cmdCancelar" runat="server"/>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table> 
				</td>
			</tr>
		</table>
		</form>
	</body>
</html>
