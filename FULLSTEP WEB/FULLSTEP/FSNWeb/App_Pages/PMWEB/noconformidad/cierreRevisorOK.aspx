<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="cierreRevisorOK.aspx.vb" Inherits="Fullstep.FSNWeb.cierreRevisorOK"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
	</head>
	<script>
		/*
		''' <summary>
		''' Volver a la pagina de revisor por cancelaci�n de cierre
		''' </summary>
		''' <remarks>Llamada desde:cmdCancelar; Tiempo m�ximo:0</remarks>*/
		function IrADetalle()
		{
			var FechaLocal;
			FechaLocal = new Date();
			var otz = FechaLocal.getTimezoneOffset();	
			IdNoConf=document.getElementById("NoConformidad").value  
			window.open("detalleNoConformidad.aspx?NoConformidad=" + IdNoConf  + "&Otz=" + otz.toString() + "&Volver=<%=Request("Volver")%>" ,"_self")
		}
				
		function validarLength(e,l)
				{
					if (e.value.length>l)
						return false
					else
						return true
				}

				function textCounter(e, l) 
				{ 
					if (e.value.length > l) 
						e.value = e.value.substring(0, l); 
				} 
		function BuscarPeticionario()
				{
				 var Cod=document.getElementById("txtPeticionario").value   
				 
				 if (Cod!=null && Cod!="")
				 {
				     var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detallepersona.aspx?CodPersona=" + Cod.toString(),"_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
                     newWindow.focus();
				 }
				}
	</script>	
<body>
	<form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="sm" runat="server">
    </asp:ScriptManager>
	<uc1:menu ID="Menu1" runat="server" OpcionMenu="Calidad" OpcionSubMenu="NoConformidades">
	</uc1:menu>    
	<input id="NoConformidad" style="z-index: 101; left: 8px; width: 128px; position: absolute;
		top: 108px; height: 22px" type="hidden" size="16" name="NoConformidad" runat="server" />
	<input id="txtPeticionario" style="z-index: 104; left: 176px; width: 128px; position: absolute;
		top: 108px; height: 22px" type="hidden" size="16" name="txtPeticionario" runat="server" />
	<table id="tblCabecera" cellspacing="0" cellpadding="0" style="width: 100%" border="0">
		<tr>
			<td colspan="4" style="width: 100%;">
				<fsn:FSNPageHeader runat="server" ID="FSNPageHeader" UrlImagenCabecera="./images/noconformidades2.gif">
				</fsn:FSNPageHeader>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="height: 20px; padding-left: 10px;">
				<asp:Label ID="lblRechazo" runat="server" Width="100%" CssClass="TituloSinDatos">DRechazo de Cierre Efectivo</asp:Label>
			</td>
		</tr>
		<tr>
			<td style="padding-left: 10px;">
				<asp:Label ID="lblID" runat="server" CssClass="captionBlue">DIdentificador</asp:Label>
				<asp:Label ID="lblIdBD" runat="server" CssClass="label">DIdentificadorBD</asp:Label>
			</td>
			<td>
				<asp:Label ID="lblresponsable" runat="server" CssClass="captionBlue">DlblResponsable</asp:Label>
				<asp:Label ID="lblresponsableBD" runat="server" CssClass="label">DlblResponsableBD</asp:Label>
			</td>
			<td>
				<asp:Label ID="lblCierreEfectivo" runat="server" CssClass="captionBlue">lblFecCierre</asp:Label>
				<asp:Label ID="lblCierreEfectivoBD" runat="server" CssClass="label">lblFecCierreBD</asp:Label>
			</td>
		</tr>
	</table>
	<table id="tblMensajes" style="z-index: 100; left: 16px; position: absolute; top: 220px; width:80%;"
		cellspacing="1" cellpadding="1" border="0">
		<tr>
			<td>
				<asp:Label ID="lblMensaje" runat="server" Width="100%" CssClass="fntLogin"></asp:Label>
			</td>
		</tr>
		<tr>
			<td>
				<textarea onkeypress="return validarLength(this,500)" id="txtComent" onkeydown="textCounter(this,500)"
					onkeyup="textCounter(this,500)" style="width: 100%; height: 200px" name="txtComentGestor"
					onchange="return validarLength(this,500)" runat="server"></textarea>
			</td>
		</tr>
		<tr>
			<td align="center">
				<table id="tblBotones" cellspacing="1" cellpadding="1" width="300" border="0">
					<tr>
						<td>
							<asp:Button runat="server" ID="cmdAceptar" CssClass="botonPMWEB" />
						</td>
						<td>
							<asp:Button runat="server" ID="cmdCancelar" CssClass="botonPMWEB"  />                            
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</form>
</body>
</html>
