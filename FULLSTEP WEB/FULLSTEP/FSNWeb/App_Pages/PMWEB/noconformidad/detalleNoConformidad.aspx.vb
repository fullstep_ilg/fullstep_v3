Public Class detalleNoConformidad
    Inherits FSNPage
#Region " Web Form Designer Generated Code "
    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    End Sub
    Protected WithEvents Solicitud As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents uwtGrupos As Infragistics.WebUI.UltraWebTab.UltraWebTab
    Protected WithEvents tblAcciones As System.Web.UI.WebControls.Table
    Protected WithEvents lblFecResolver As System.Web.UI.WebControls.Label
    Protected WithEvents lblCamposObligatorios As System.Web.UI.WebControls.Label
    Protected WithEvents lblDirigidaA As System.Web.UI.WebControls.Label
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitFecAlta As System.Web.UI.WebControls.Label
    Protected WithEvents lblProveedor As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecAlta As System.Web.UI.WebControls.Label
    Protected WithEvents lblVersion As System.Web.UI.WebControls.Label
    Protected WithEvents Instancia As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents NoConformidad As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtEnviar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblFechaResolver As System.Web.UI.WebControls.Label
    Protected WithEvents fsentryNoConfFecDesde As DataEntry.GeneralEntry
    Protected WithEvents Version As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents VersionInicialCargada As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblLitID As System.Web.UI.WebControls.Label
    Protected WithEvents lblID As System.Web.UI.WebControls.Label
    Protected WithEvents lblContacto As System.Web.UI.WebControls.Label
    Protected WithEvents lblContactoBD As System.Web.UI.WebControls.Label
    Protected WithEvents LblRevisorCorto As System.Web.UI.WebControls.Label
    Protected WithEvents LbltxtRevisorBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblCierreInforme As System.Web.UI.WebControls.Label
    Protected WithEvents lblComentRechazo As System.Web.UI.WebControls.Label
    Protected WithEvents lbltxtComentrechazo As System.Web.UI.WebControls.Label
    Protected WithEvents lblComentCierre As System.Web.UI.WebControls.Label
    Protected WithEvents lbltxtComentCierre As System.Web.UI.WebControls.Label
    Protected WithEvents codRevisor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents AccesoRevisor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblvolver As System.Web.UI.WebControls.Label
    Protected WithEvents cmdMail As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents Email As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cadenaespera As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents FuerzaDesgloseLectura As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Proveedor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents DenProveedor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Tipo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents FecAlta As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblProgreso As System.Web.UI.WebControls.TextBox
    Protected WithEvents imgProgreso As System.Web.UI.WebControls.Image
    Protected WithEvents tblProgreso As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents BotonCalcular As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ComboQA As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents UnicoUnidadQA As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents EstadoNoConf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Workflow As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblUnidadQA As System.Web.UI.WebControls.Label
    Protected WithEvents lblUnicoUnidadQA As System.Web.UI.WebControls.Label
    Protected WithEvents diferenciaUTCServidor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents indiceCombo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ComboL As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ComboV As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblLitPeticionario As System.Web.UI.WebControls.Label
    Protected WithEvents lblPeticionario As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitFecCierre As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecCierre As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitEstado As System.Web.UI.WebControls.Label
    Protected WithEvents lblEstado As System.Web.UI.WebControls.Label
    Protected WithEvents lblInfoNoConformidad As System.Web.UI.WebControls.Label
    Protected WithEvents imgCollapseFechasResolucion As System.Web.UI.WebControls.Image
    Protected WithEvents imgExpandFechasResolucion As System.Web.UI.WebControls.Image
    Protected WithEvents imgCollapseNotificaciones As System.Web.UI.WebControls.Image
    Protected WithEvents imgExpandNotificaciones As System.Web.UI.WebControls.Image
    Protected WithEvents wddNotificadosInternos As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents imgBuscarNotificadosInternos As System.Web.UI.WebControls.ImageButton
    Protected WithEvents wddNotificadosProveedor As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents pnlNotificaciones As System.Web.UI.WebControls.Panel
    Protected WithEvents CodigoRevisorAnterior As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtComent As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblComentarios As System.Web.UI.WebControls.Label
    Protected WithEvents lblSeleccioneNotificados As System.Web.UI.WebControls.Label
    Protected WithEvents gvRegistroMails As System.Web.UI.WebControls.GridView
    Protected WithEvents FSNPageHeader As Fullstep.FSNWebControls.FSNPageHeader
    Protected WithEvents lblTituloFechasResolucion As System.Web.UI.WebControls.Label
    Protected WithEvents lblNotificados As System.Web.UI.WebControls.Label
    Protected WithEvents lblContactoInterno As System.Web.UI.WebControls.Label
    Protected WithEvents lblAsunto As System.Web.UI.WebControls.Label
    Protected WithEvents lblDe As System.Web.UI.WebControls.Label
    Protected WithEvents lblPara As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
    Protected WithEvents lblEnviado As System.Web.UI.WebControls.Label
    Protected WithEvents lblTituloHistoricoNotificaciones As System.Web.UI.WebControls.Label
    Protected WithEvents Menu1 As Object
    Protected WithEvents lblCierreRevisor As System.Web.UI.WebControls.Label
    Protected WithEvents LineaRevisor As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents wddUnidadQA As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents wddRevisor As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents wddVersion As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents lblProveERP As System.Web.UI.WebControls.Label
    Protected WithEvents wddProveERP As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents lblProveERPNoEdit As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecApliMejoras As System.Web.UI.WebControls.Label
    Protected WithEvents txtFecApliMejoras As System.Web.UI.WebControls.Label
    Protected WithEvents fsentryFecApliMejoras As DataEntry.GeneralEntry
    Protected WithEvents pnlTituloNotificaciones As System.Web.UI.WebControls.Panel
    Protected WithEvents uwPopUpAcciones As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
    Protected WithEvents uwPopUpListados As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
    Protected WithEvents HyperDetalle As Global.System.Web.UI.WebControls.HyperLink
    Private designerPlaceholderDeclaration As System.Object
#End Region
#Region "Init - Cache"
    Private _oNoConformidad As FSNServer.NoConformidad
    Protected ReadOnly Property oNoConformidad() As FSNServer.NoConformidad
        Get
            If _oNoConformidad Is Nothing Then
                If Me.IsPostBack Then
                    _oNoConformidad = CType(Cache("oNoConformidad_" & FSNUser.Cod), FSNServer.NoConformidad)
                Else
                    _oNoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
                    _oNoConformidad.ID = Request("NoConformidad")

                    If Request("Version") <> Nothing Then
                        _oNoConformidad.Load(FSNUser.Idioma.ToString, Request("Version"))
                    Else
                        _oNoConformidad.Load(FSNUser.Idioma.ToString)
                    End If

                    If Request("Version") <> Nothing Then _oNoConformidad.Version = Request("Version")
                    enCursoDeAprobacion = False
                    If _oNoConformidad.Workflow > 0 AndAlso (_oNoConformidad.Estado = TipoEstadoNoConformidad.EnCursoDeAprobacion OrElse _oNoConformidad.Estado = TipoEstadoNoConformidad.Guardada) Then
                        enCursoDeAprobacion = True
                    End If
                    Me.InsertarEnCache("oNoConformidad_" & FSNUser.Cod, _oNoConformidad)
                End If
            End If
            Return _oNoConformidad
        End Get
    End Property
    Private _oInstancia As FSNServer.Instancia
    Protected ReadOnly Property oInstancia() As FSNServer.Instancia
        Get
            If _oInstancia Is Nothing Then
                If Me.IsPostBack Then
                    _oInstancia = CType(Cache("oInstancia_" & FSNUser.Cod), FSNServer.Instancia)
                Else
                    _oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))

                    If Request("version") <> Nothing Then
                        _oInstancia.ID = Request("Instancia")
                    Else
                        _oInstancia.ID = oNoConformidad.Instancia
                    End If

                    _oInstancia.Version = oNoConformidad.Version

                    Me.InsertarEnCache("oInstancia_" & FSNUser.Cod, _oInstancia)
                End If
            End If
            Return _oInstancia
        End Get
    End Property
    Private _oDSEstados As DataSet
    Protected ReadOnly Property oNoConformidadEstados() As DataSet
        Get
            If _oDSEstados Is Nothing Then
                If Me.IsPostBack Then
                    _oDSEstados = CType(Cache("oDSEstados_" & FSNUser.Cod), DataSet)
                Else
                    _oDSEstados = oNoConformidad.CargarEstadosComentariosInternosAcciones(Idioma, FSNUser.Cod, m_sTituloAcciones)

                    Me.InsertarEnCache("oDSEstados_" & FSNUser.Cod, _oDSEstados)
                End If
            End If
            Return _oDSEstados
        End Get
    End Property
    Private _oAcciones As DataTable
    Protected ReadOnly Property oNoConformidadAcciones() As DataTable
        Get
            If _oAcciones Is Nothing Then
                If Me.IsPostBack Then
                    _oAcciones = CType(Cache("oAcciones_" & FSNUser.Cod), DataTable)
                Else
                    Dim NoMostrarBotonesAcciones As Boolean = True
                    If PosibleEditar And (oNoConformidad.Estado = TipoEstadoNoConformidad.Guardada Or oNoConformidad.Estado = TipoEstadoNoConformidad.Abierta) Then NoMostrarBotonesAcciones = False
                    _oAcciones = oNoConformidad.CargarBotonesAprobarRechazarAcciones(m_sTituloAcciones, NoMostrarBotonesAcciones)

                    Me.InsertarEnCache("oAcciones_" & FSNUser.Cod, _oAcciones)
                End If
            End If
            Return _oAcciones
        End Get
    End Property
    Private _oInstanciaGrupos As FSNServer.Grupos
    Protected ReadOnly Property oInstanciaGrupos() As FSNServer.Grupos
        Get
            If _oInstanciaGrupos Is Nothing Then
                If Me.IsPostBack Then
                    _oInstanciaGrupos = CType(Cache("oInstanciaGrupos_" & FSNUser.Cod), FSNServer.Grupos)
                Else
                    oInstancia.CargarCamposInstancia(FSNUser.Idioma.ToString, IIf(enCursoDeAprobacion, FSNUser.CodPersona, IIf(PosibleEditar, oNoConformidad.PeticionarioCod, FSNUser.CodPersona)), oNoConformidad.Proveedor, enCursoDeAprobacion)

                    _oInstanciaGrupos = oInstancia.Grupos

                    Me.InsertarEnCache("oInstanciaGrupos_" & FSNUser.Cod, _oInstanciaGrupos)
                End If
            End If
            Return _oInstanciaGrupos
        End Get
    End Property
    Private _PosibleEditar As Boolean = True
    Protected Property PosibleEditar() As Boolean
        Get
            ViewState("PosibleEditar") = _PosibleEditar
            Return ViewState("PosibleEditar")
        End Get
        Set(ByVal value As Boolean)
            _PosibleEditar = value
        End Set
    End Property
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
#End Region
    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
    Private m_sTituloAcciones(5) As String
    'Indica si estamos con un revisor y q la nc esta cerrada din revisar
    Private mbPagNCRevisor As Boolean
    '
    Private mbEsNCEscalacionEstadoPdteValidar As Boolean
    Private enCursoDeAprobacion As Boolean

    ''' <summary>
    ''' Carga de la pantalla. Para q tras un guardado si todas las acciones han pasado a estar "finalizadas
    ''' y revisadas" y el usuario no ten�a permiso para "cerrar antes de q este todo finalizado y revisadas" 
    ''' se oculta el TD q contiene los botones de cierre, as� con javascript se puede volver a mostrar el TD
    ''' y con el los botones.
    ''' </summary>
    ''' <param name="sender">pantalla</param>
    ''' <param name="e">parametro de sistema</param>            
    ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        'Tema de la cadena espera
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Espera

        lblProgreso.Text = Textos(1)

        'Cargamos los textos de la pantalla
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.DetalleNoConformidad

        If Page.IsPostBack Then
            CargarGruposCamposInstancia(True, FSNUser.Idioma.ToString)
            CargarParticipantes()

            imgCollapseFechasResolucion.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Theme & "/images/ig_menu_scrollup.gif"
            imgExpandFechasResolucion.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Theme & "/images/ig_menu_scrolldown.gif"

            imgCollapseNotificaciones.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Theme & "/images/ig_menu_scrollup.gif"
            imgExpandNotificaciones.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Theme & "/images/ig_menu_scrolldown.gif"

            Select Case Request("__EVENTTARGET")
                Case "wddNotificadosInternos"
                    If Request("__EVENTARGUMENT") <> "" Then
                        CargarNotificadosInternos(Request("__EVENTARGUMENT"), False)
                        Exit Sub
                    End If
                Case Else
                    Exit Sub
            End Select
        End If

        imgBuscarNotificadosInternos.Attributes.Add("onclick", "BuscarNotificadoInterno();return false;")

        imgCollapseFechasResolucion.Attributes.Add("onclick", "OcultarFechasResolucion();return false;")
        imgExpandFechasResolucion.Attributes.Add("onclick", "OcultarFechasResolucion();return false;")

        If oNoConformidad.Es_Noconformidad_Escalacion_Hist Then
            pnlTituloNotificaciones.Visible = False
        Else
            imgCollapseNotificaciones.Attributes.Add("onclick", "OcultarNotificaciones();return false;")
            imgExpandNotificaciones.Attributes.Add("onclick", "OcultarNotificaciones();return false;")
        End If

        imgCollapseFechasResolucion.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Theme & "/images/ig_menu_scrollup.gif"
        imgExpandFechasResolucion.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Theme & "/images/ig_menu_scrolldown.gif"

        imgCollapseNotificaciones.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Theme & "/images/ig_menu_scrollup.gif"
        imgExpandNotificaciones.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Theme & "/images/ig_menu_scrolldown.gif"

        ConfigurarCabecera()
        VisualizarLinkFlujo()
        RegistrarScripts()
        Dim sIdi As String = FSNUser.Idioma.ToString()

        'Definimos las variables PosibleEditar y PosibleReabrir
        Dim PosibleReabrir As Boolean = True

        'Cargamos los datos de la no conformidad
        CargarDatosNoConformidad(PosibleEditar, sIdi)

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "proveERP", "var proveERP='" & DBNullToStr(oNoConformidad.ProveedorERP) & "';", True)
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "esNoConf_Escalacion",
                    "var esNoConf_Escalacion='" & oNoConformidad.Es_Noconformidad_Escalacion.ToString.ToLower & "';", True)

        If FSNUser.AccesoCN Then
            If Not Page.ClientScript.IsClientScriptBlockRegistered("EntidadColaboracion") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "EntidadColaboracion",
                    "var TipoEntidadColaboracion='NC';" &
                    "var CodigoEntidadColaboracion ={identificador:" & oNoConformidad.Instancia &
                                                    ",texto:'" & JSText(oNoConformidad.TipoDen) & "'" &
                                                    ",proveedor:'" & JSText(oNoConformidad.ProveDen) & "'};",
                                                    True)
            End If
        End If

        'Ponemos los textos a los controles de la pantalla
        CargarTextosPantalla()

        'CARGAMOS LOS NOTIFICADOS DEL PROVEEDOR Y LOS INTERNOS EN LOS DROPDOWN
        '=====================================================================
        CargarNotificadosProveedor()

        CargarNotificadosInternos("", True)

        'Creamos un datatable donde estar�n las unidades de negocio
        Dim UnidadesNegocio As New DataTable
        If Session("VolverdetalleNoconformidad") = "DetPuntNc" Then
            PosibleEditar = False
            PosibleReabrir = False

            Menu1.Visible = False

            If mbPagNCRevisor Then
                If Request("cn") Is Nothing Then
                    FSNPageHeader.OnClientClickVolver = "javascript:HistoryHaciaAtras();return false;"
                End If
            End If
        End If

        '=========================================================='
        'La no conformidad solo es editable si:
        '   - Tiene estado Guardada y el que accede es el peticionario
        '   - Tiene estado Abierta y el que accede es el peticionario
        '   - Tiene estado Abierta y el que accede no es el peticionario 
        '   pero tiene el Permiso de Modificar no conf. emitidas por otro usuario
        'La no conformidad se puede reabrir si:
        '   - Tiene estado Cerrada (en alguna de sus variantes) y es el 
        '   peticionario con permiso para reabrir no conformidades cerradas por el
        '   - Tiene estado Cerrada (en alguna de sus variantes) y no es el 
        '   peticionario pero tiene permiso para reabrir no conformidades cerradas por otro usuario
        If oNoConformidad.Es_Noconformidad_Escalacion_Hist Then
            PosibleEditar = False
        ElseIf oNoConformidad.ProveedorBaja Then
            'Si la no conformidad es de proveedor de baja no podr� editar.
            PosibleEditar = False
        ElseIf mbPagNCRevisor Then
            'El revisor solo aprobar/rechazar/imprimir
            PosibleEditar = False
            PosibleReabrir = False
        Else
            Select Case oNoConformidad.Estado
                Case TipoEstadoNoConformidad.Guardada
                    If oNoConformidad.PeticionarioCod = FSNUser.CodPersona Then
                        'Es el peticionario, pero tenemos que ver si sigue teniendo permiso de Emitir en esta unidad de negocio,
                        'es decir, Permiso de Emitir, Reabrir=>Emitir o Modificar=>Emitir
                        CargarUnidadesNegocio(PosibleEditar, UnidadesNegocio, True, sIdi, TipoAccesoUNQAS.EmitirNoConformidades)

                        If UnidadesNegocio.Select("ID=" & oNoConformidad.UnidadQA).Length = 0 AndAlso Not oNoConformidad.Es_Noconformidad_Escalacion Then
                            PosibleEditar = False
                        End If
                    Else
                        PosibleEditar = False
                    End If
                Case TipoEstadoNoConformidad.Abierta
                    If oNoConformidad.PeticionarioCod = FSNUser.CodPersona Then
                        'Es el peticionario, pero tenemos que ver si sigue teniendo permiso de Emitir en esta unidad de negocio,
                        'es decir, Permiso de Emitir, Reabrir=>Emitir o Modificar=>Emitir
                        CargarUnidadesNegocio(PosibleEditar, UnidadesNegocio, True, sIdi, TipoAccesoUNQAS.EmitirNoConformidades)

                        If UnidadesNegocio.Select("ID=" & oNoConformidad.UnidadQA).Length = 0 AndAlso Not oNoConformidad.Es_Noconformidad_Escalacion Then
                            PosibleEditar = False
                        End If
                    Else
                        CargarUnidadesNegocio(PosibleEditar, UnidadesNegocio, True, sIdi, TipoAccesoUNQAS.ModificarNoConformidadesOtroUsuario)

                        If UnidadesNegocio.Select("ID=" & oNoConformidad.UnidadQA).Length = 0 AndAlso Not oNoConformidad.Es_Noconformidad_Escalacion Then
                            PosibleEditar = False
                        End If
                    End If
                Case TipoEstadoNoConformidad.Anulada
                    PosibleEditar = False
                Case TipoEstadoNoConformidad.EnCursoDeAprobacion
                    PosibleEditar = True
                    PosibleReabrir = False
                    CargarUnidadesNegocio(PosibleEditar, UnidadesNegocio, True, sIdi, TipoAccesoUNQAS.EmitirNoConformidades)
                Case Else
                    CargarUnidadesNegocio(PosibleEditar, UnidadesNegocio, True, sIdi, TipoAccesoUNQAS.ReabrirNoConformidades)

                    If oNoConformidad.PeticionarioCod = FSNUser.CodPersona Then
                        If Not FSNUser.QAReabrirNoConf Then
                            PosibleReabrir = False
                        End If
                    Else
                        If UnidadesNegocio.Select("ID=" & oNoConformidad.UnidadQA).Length = 0 AndAlso Not oNoConformidad.Es_Noconformidad_Escalacion Then
                            PosibleReabrir = False
                        End If
                    End If
            End Select
        End If

        '============================================
        'CARGA DEL COMBO DE LAS UNIDADES DE NEGOCIO
        '============================================
        'Ahora ya tenemos todas las variables para saber si la no conformidad es editable o no
        CargarUnidadesNegocio((PosibleEditar AndAlso Not oNoConformidad.Es_Noconformidad_Escalacion), UnidadesNegocio, False, sIdi)

        '============================================
        'CARGA DEL COMBO DE LOS REVISORES
        '============================================
        If Not FSNServer.TipoAcceso.gbFSQA_Revisor Then
            'La instalaci�n no es de revisor, por lo que no hay que cargar el combo de revisores

            'Si FSQA_REVISOR DE PARGEN_INTERNO = 0
            AccesoRevisor.Value = 0
            LbltxtRevisorBD.Visible = False
            LblRevisorCorto.Visible = False
            wddRevisor.Visible = False
            CodigoRevisorAnterior.Value = ""
        Else
            ' Este input oculto se a�ada para controlar en javascript
            'si FSQA_REVISOR DE PARGEN_INTERNO es 0 � 1
            AccesoRevisor.Value = 1

            'Si el acceso a la no conformidad es de lectura 
            'que no haga la carga del combo si no que cargue directamente en la etiqueta el revisor de la no conf
            'Si el acceso es de escritura se carga el combo como siempre.

            'Texto captions Revisor
            LblRevisorCorto.Text = Textos(36)

            CodigoRevisorAnterior.Value = ""

            If Not PosibleEditar Then
                'Si no se puede editar la no conformidad obtenemos el nombre del revisor de la no conformidad
                'y lo ponemos en el label. Si no hay revisor ocultamos el label
                If oNoConformidad.Revisor = "" Then
                    'No se muestra las etiquetas de revisor pq etan vacias
                    LbltxtRevisorBD.Visible = False
                    LblRevisorCorto.Visible = False
                    wddRevisor.Visible = False
                Else
					wddRevisor.Visible = False
					LbltxtRevisorBD.Visible = True
					LbltxtRevisorBD.Text = oNoConformidad.RevisorNombre
					codRevisor.Value = oNoConformidad.Revisor
				End If
            Else
                CargarRevisores()
            End If
        End If

        If (oNoConformidad.Estado > TipoEstadoNoConformidad.Abierta) OrElse (oNoConformidad.ProveedorBaja) Then
            FuerzaDesgloseLectura.Value = "1"
        Else
            FuerzaDesgloseLectura.Value = "0"
        End If

        CargarGruposCamposInstancia(PosibleEditar, sIdi)
        CargarParticipantes()

        diferenciaUTCServidor.Value = oInstancia.DiferenciaUTCServidor

        '******************* Montar la tabla Comentarios ****************************
        MontarTablaComentarios(PosibleEditar, sIdi)
        '******************* Montar la tabla de acciones ****************************
        MontarTablaAcciones(PosibleEditar)
        'Cargo el combo de versiones seg�n los permisos que se han establecido hasta este momento
        CargarComboVersiones()

        'Obtener email
        Dim oProves As FSNServer.Proveedores
        oProves = FSNServer.Get_Object(GetType(FSNServer.Proveedores))
        oProves.CargarEmailProv(Proveedor.Value, , , True)
        If oProves.Data.Tables(0).Rows.Count > 0 Then Email.Value = DBNullToSomething(oProves.Data.Tables(0).Rows(0)("EMAIL"))

        MostrarSegunPosibilidadEdicionYPagRevisor(PosibleEditar, PosibleReabrir)

        CargarRegistroMails()
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "esNoconfEscalacion", "var esNoconfEscalacion=" & oNoConformidad.Es_Noconformidad_Escalacion.ToString.ToLower & ";", True)
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "nivelEscalacion", "var nivelEscalacion=" & oNoConformidad.NivelEscalacion & ";", True)
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "idEscalacionProve", "var idEscalacionProve=" & oNoConformidad.IdEscalacionProve & ";", True)
    End Sub
    ''' <summary>
    ''' Configura la cabecera, botones visibles, muestra en el menu las posibles acciones
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ConfigurarCabecera()
        Dim bMapper As Boolean = False
        With FSNPageHeader
            If Request("cn") IsNot Nothing Then
                .VisibleBotonVolverColaboracion = True
                .OnClientClickVolverColaboracion = "HistoryHaciaAtras();return false;"
            Else
                .VisibleBotonVolver = True
            End If
            .VisibleBotonCalcular = Not oNoConformidad.Es_Noconformidad_Escalacion_Hist
            .VisibleBotonMail = Not oNoConformidad.Es_Noconformidad_Escalacion_Hist
            .VisibleBotonImpExp = Not oNoConformidad.Es_Noconformidad_Escalacion_Hist
            .VisibleBotonEmitir = Not oNoConformidad.Es_Noconformidad_Escalacion_Hist
            .VisibleBotonEliminar = Not oNoConformidad.Es_Noconformidad_Escalacion_Hist
            .VisibleBotonGuardar = Not oNoConformidad.Es_Noconformidad_Escalacion_Hist
            .VisibleBotonCierrePositivo = Not oNoConformidad.Es_Noconformidad_Escalacion_Hist
            .VisibleBotonCierreNegativo = Not oNoConformidad.Es_Noconformidad_Escalacion_Hist
            .VisibleBotonReabrir = Not oNoConformidad.Es_Noconformidad_Escalacion_Hist
            'Eventos de cliente asociados a los botones de la cabecera
            .OnClientClickCalcular = "return CalcularCamposCalculados()"
            .OnClientClickMail = "EnviarMail();return false;"
            .OnClientClickImpExp = "return cmdImpExp_onclick()"

            Dim oValidacionesIntegracion As FSNServer.PM_ValidacionesIntegracion
            oValidacionesIntegracion = FSNServer.Get_Object(GetType(FSNServer.PM_ValidacionesIntegracion))
            If oValidacionesIntegracion.ControlaConMaper(TablasIntegracion.PPM) Then
                bMapper = True
            End If
            .OnClientClickEmitir = "return Guardar(1," + bMapper.ToString.ToLower + ");"
            .OnClientClickEliminar = "EliminarVersion();return false;"
            .OnClientClickGuardar = "return Guardar(0," + bMapper.ToString.ToLower + ");"
            If Not mbPagNCRevisor Then
                If Request("cn") Is Nothing Then
                    .OnClientClickVolver = "HistoryHaciaAtras();return false;"
                End If
                .OnClientClickCierrePositivo = "return Guardar(2," + bMapper.ToString.ToLower + ");"
                .OnClientClickCierreNegativo = "return Guardar(3," + bMapper.ToString.ToLower + ");"
            End If
            .OnClientClickReabrir = "Reabrir();return false;"
        End With

        If enCursoDeAprobacion Then
            oInstancia.DevolverEtapaActual(Idioma, FSNUser.CodPersona)
            FSNPageHeader.OnClientClickGuardar = "return Guardar(0, false, true, null, " + oInstancia.Etapa.ToString() + ");"

            If oInstancia.RolActual = 0 Or oInstancia.Etapa = 0 Then
                FSNPageHeader.VisibleBotonAccion1 = False
                FSNPageHeader.VisibleBotonGuardar = False
                FSNPageHeader.VisibleBotonEnviarAFavoritos = False
            Else
                Dim oRol As FSNServer.Rol = FSNServer.Get_Object(GetType(FSNServer.Rol))
                oRol.Id = oInstancia.RolActual
                oRol.Bloque = oInstancia.Etapa
                If Not ClientScript.IsStartupScriptRegistered("oRol_Id") Then _
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "oRol_Id", "var oRol_Id=" & oRol.Id & ";", True)

                CargarAcciones(oRol, bMapper)
                CargarListados(oRol)
            End If
        Else
            FSNPageHeader.VisibleBotonAccion1 = False
            FSNPageHeader.VisibleBotonListados = False
        End If
    End Sub
    ''' <summary>Configura las posibles acciones de cabecera</summary>
    Private Sub CargarAcciones(ByRef oRol As FSNServer.Rol, ByVal bMapper As Boolean)
        Dim oDSAcciones As DataSet
        If oInstancia.Estado = TipoEstadoSolic.Rechazada = True Then
            oDSAcciones = oRol.CargarAcciones(Idioma, True)
        Else
            oDSAcciones = oRol.CargarAcciones(Idioma)
        End If

        Dim oPopMenu As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu = Me.uwPopUpAcciones
        Dim blnAprobar As Boolean = False
        Dim blnRechazar As Boolean = False
        Dim iAccionesRestarOtrasAcciones As Integer
        Dim iOtrasAcciones As Integer
        Dim sEjecutarAccion As String
        If oDSAcciones.Tables.Count > 0 Then
            If oDSAcciones.Tables(0).Rows.Count > 1 Then
                'Comprobamos si entre las acciones hay de tipo aprobar y rechazar
                For Each oRow As DataRow In oDSAcciones.Tables(0).Rows
                    If DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
                        blnAprobar = True
                        iAccionesRestarOtrasAcciones = iAccionesRestarOtrasAcciones + 1
                        If blnRechazar Then Exit For
                    ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
                        blnRechazar = True
                        iAccionesRestarOtrasAcciones = iAccionesRestarOtrasAcciones + 1
                        If blnAprobar Then Exit For
                    End If
                Next
                iOtrasAcciones = oDSAcciones.Tables(0).Rows.Count - iAccionesRestarOtrasAcciones
                Dim cont As Integer = 0
                For Each oRow In oDSAcciones.Tables(0).Rows
                    sEjecutarAccion = "Guardar(0, " + bMapper.ToString.ToLower + ", false, " + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + "," + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, True.ToString.ToLower, False.ToString.ToLower)) + ",'" + (IIf(oRow.Item("GUARDA") = 1, True.ToString.ToLower, False.ToString.ToLower)) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, True.ToString.ToLower, False.ToString.ToLower)) + "'); "
                    If iOtrasAcciones > 3 AndAlso DBNullToSomething(oRow.Item("RA_APROBAR")) <> 1 AndAlso DBNullToSomething(oRow.Item("RA_RECHAZAR")) <> 1 Then
                        Dim oItem As Infragistics.WebUI.UltraWebNavigator.Item

                        If IsDBNull(oRow.Item("DEN")) Then
                            oItem = oPopMenu.Items.Add("&nbsp;")
                        Else
                            If oRow.Item("DEN") = "" Then
                                oItem = oPopMenu.Items.Add("&nbsp;")
                            Else
                                oItem = oPopMenu.Items.Add(DBNullToStr(oRow.Item("DEN")))
                            End If
                        End If
                        oItem.TargetUrl = sEjecutarAccion
                    ElseIf DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
                        FSNPageHeader.OnClientClickAprobar = sEjecutarAccion + "return false;"
                    ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
                        FSNPageHeader.OnClientClickRechazar = sEjecutarAccion + "return false;"
                    Else
                        If cont = 0 Then
                            FSNPageHeader.OnClientClickAccion1 = sEjecutarAccion + "return false;"
                            FSNPageHeader.TextoBotonAccion1 = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                            FSNPageHeader.VisibleBotonAccion1 = True
                            cont = 1
                        ElseIf cont = 1 Then
                            FSNPageHeader.OnClientClickAccion2 = sEjecutarAccion + "return false;"
                            FSNPageHeader.TextoBotonAccion2 = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                            FSNPageHeader.VisibleBotonAccion2 = True
                            cont = 2
                        Else
                            FSNPageHeader.OnClientClickAccion3 = sEjecutarAccion + "return false;"
                            FSNPageHeader.TextoBotonAccion3 = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                            FSNPageHeader.VisibleBotonAccion3 = True
                        End If
                    End If
                Next
                If blnAprobar Then
                    FSNPageHeader.VisibleBotonAprobar = True
                End If
                If blnRechazar Then
                    FSNPageHeader.VisibleBotonRechazar = True
                End If
                If iOtrasAcciones > 3 Then
                    FSNPageHeader.VisibleBotonAccion1 = True
                    FSNPageHeader.TextoBotonAccion1 = Textos(96)
                    FSNPageHeader.OnClientClickAccion1 = "return mostrarMenuAcciones(event,1);"
                End If
            Else
                If oDSAcciones.Tables(0).Rows.Count = 0 Then
                    FSNPageHeader.VisibleBotonAccion1 = False
                Else
                    Dim oRow As DataRow
                    oRow = oDSAcciones.Tables(0).Rows(0)
                    sEjecutarAccion = "Guardar(0, " + bMapper.ToString.ToLower + ", false, " + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(DBNullToSomething(oRow.Item("CUMP_OBL_ROL")) = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("GUARDA")) = 1, "true", "false")) + "'," + CStr(oInstancia.InstanciaBloqueBloq) + ",'" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                    If DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
                        FSNPageHeader.VisibleBotonAccion1 = False
                        FSNPageHeader.VisibleBotonAprobar = True
                        FSNPageHeader.OnClientClickAprobar = sEjecutarAccion
                    ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
                        FSNPageHeader.VisibleBotonAccion1 = False
                        FSNPageHeader.VisibleBotonRechazar = True
                        FSNPageHeader.OnClientClickRechazar = sEjecutarAccion
                    Else
                        FSNPageHeader.VisibleBotonAccion1 = True
                        FSNPageHeader.OnClientClickAccion1 = sEjecutarAccion
                        FSNPageHeader.TextoBotonAccion1 = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                    End If
                End If
            End If
        Else
            FSNPageHeader.VisibleBotonAccion1 = False
        End If
    End Sub
    ''' <summary>Configura los posibles listados de cabecera</summary>
    Private Sub CargarListados(ByRef oRol As FSNServer.Rol)
        'Cargar listados
        Dim oDSListados As DataSet
        oDSListados = oRol.CargarListados(Idioma)

        Dim oPopMenuListados As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu = Me.uwPopUpListados

        If oDSListados.Tables.Count > 0 Then
            If oDSListados.Tables(0).Rows.Count > 1 Then
                FSNPageHeader.VisibleBotonListados = True
                FSNPageHeader.OnClientClickListados = "return mostrarMenuListados(event);"
                For Each oRow In oDSListados.Tables(0).Rows
                    Dim oItemListados As Infragistics.WebUI.UltraWebNavigator.Item = oPopMenuListados.Items.Add(oRow.Item("DEN"))
                    oItemListados.TargetUrl = "javascript:LanzarListado('" + oRow.Item("ARCHIVO_RPT") + "'," + CStr(oInstancia.ID) + ")"
                Next
            Else
                If Not (oDSListados.Tables(0).Rows.Count = 0) Then
                    Dim oRow As DataRow = oDSListados.Tables(0).Rows(0)
                    FSNPageHeader.VisibleBotonListados = True
                    FSNPageHeader.OnClientClickListados = "LanzarListado('" + oRow.Item("ARCHIVO_RPT") + "'," + CStr(oInstancia.ID) + "); return false;"
                    FSNPageHeader.TextoBotonListados = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                End If
            End If
        End If
    End Sub
    ''' <summary>
    ''' Da valor a los textos que aparecen en pantalla
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarTextosPantalla()

        lblUnidadQA.Text = Textos(41)
        With FSNPageHeader
            If Request("cn") IsNot Nothing Then
                .TextoBotonVolverColaboracion = Textos(82)
            Else
                .TextoBotonVolver = Textos(37)
            End If
            .TextoBotonCalcular = Textos(40)
            .TextoBotonMail = Textos(39)
            .TextoBotonImpExp = Textos(22)
            .TextoBotonEmitir = Textos(9)
            .TextoBotonEliminar = Textos(1)
            .TextoBotonGuardar = Textos(2)
            .TextoBotonCierrePositivo = IIf(Not mbPagNCRevisor, IIf(mbEsNCEscalacionEstadoPdteValidar, Textos(86), Textos(3)), Textos(26))
            .TextoBotonCierreNegativo = IIf(Not mbPagNCRevisor, IIf(mbEsNCEscalacionEstadoPdteValidar, Textos(87), Textos(4)), Textos(27))
            .TextoBotonReabrir = Textos(38)
            .TextoBotonAnular = Textos(74) '"Anular"
            .TextoBotonAprobar = Textos(26)
            .TextoBotonRechazar = Textos(27)
            .TextoBotonListados = Textos(95)
        End With

        lblVersion.Text = Textos(7)
        If oNoConformidad.Es_Noconformidad_Escalacion Then
            lblFecResolver.Text = Textos(91)
        Else
            lblFecResolver.Text = Textos(8)
        End If
        lblCamposObligatorios.Text = Textos(84)
        m_sTituloAcciones(1) = Textos(23)  'Estado actual
        m_sTituloAcciones(2) = Textos(24)  'com.
        m_sTituloAcciones(3) = Textos(25)  'Estado interno
        m_sTituloAcciones(4) = Textos(26) 'Aprobar
        m_sTituloAcciones(5) = Textos(27)  'Rechazar
        lblTituloFechasResolucion.Text = Textos(64)
        lblNotificados.Text = Textos(65)
        lblContacto.Text = Textos(67)
        lblContactoInterno.Text = Textos(68)
        lblTituloHistoricoNotificaciones.Text = Textos(66)
        lblAsunto.Text = Textos(69)
        lblDe.Text = Textos(70)
        lblPara.Text = Textos(71)
        lblFecha.Text = Textos(72)
        lblEnviado.Text = Textos(73)
        lblComentarios.Text = Textos(77)
        lblProveERP.Text = Textos(88) & ":"
        lblFecApliMejoras.Text = Textos(90)
        HyperDetalle.Text = Textos(92)
    End Sub
    Private Sub RegistrarScripts()
        Dim sScript As String
        Dim sVolver As String

        If Request("volver") <> "" Then
            sVolver = Request("volver")
            sVolver = Replace(sVolver, "*", "&")
            Session("VolverdetalleNoconformidad") = sVolver
        Else
            Session("VolverdetalleNoconformidad") = ""
        End If

        If Request("TipoSolicit") <> Nothing Then
            Session("FiltroNoConformidadAltaNoConformidad") = Request("TipoSolicit")
        End If

        Dim sClientTextVars As String
        sClientTextVars = ""
        sClientTextVars += "vdecimalfmt='" + FSNUser.DecimalFmt + "';"
        sClientTextVars += "vthousanfmt='" + FSNUser.ThousanFmt + "';"
        sClientTextVars += "vprecisionfmt='" + FSNUser.PrecisionFmt + "';"
        sClientTextVars += "vprecisionfmt='" + FSNUser.PrecisionFmt + "';"
        sClientTextVars += "vDatefmt='" + FSNUser.DateFormat.ShortDatePattern + "';"

        sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsUser", sClientTextVars)

        If Not Page.ClientScript.IsClientScriptBlockRegistered("varFila") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varFila", "<script>var sFila = '" & JSText(Textos(83)) & "' </script>")
        '****************************************************************************
        Dim ilGMN1 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN1
        Dim ilGMN2 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN2
        Dim ilGMN3 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN3
        Dim ilGMN4 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN4
        sScript = ""
        sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
        sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
        sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
        sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "LongitudesCodigosKey", sScript)

        If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayDesgloses") Then
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")
        End If

        'En jsAlta.js hay para las funciones de a�adir linea y copia linea
        '       $create(AjaxControlToolkit.AutoCompleteBehavior ...rutaPM...
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")
        'para el buscador de art�culos
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaQA", "<script>var rutaQA = '" & ConfigurationManager.AppSettings("rutaQA") & "' </script>")

        Me.FindControl("frmDetalle").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())

        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(Textos(13)) + "';"  '�Desea eliminar la versi�n de la no conformidad?
        sClientTexts += "arrTextosML[1] = '" + JSText(Textos(20)) + "';"  'Faltan campos obligatorios.
        sClientTexts += "arrTextosML[2] = '" + JSText(Textos(21)) + "';"  'Seleccione la fecha l�mite de resoluci�n de la no conformidad
        sClientTexts += "arrTextosML[3] = '" + JSText(Textos(29)) + "';"  'Algunas fechas de inicio y/o fechas de fin de acci�n son posteriores a sus fechas limite correspondientes. Reviselos para poder Continuar.
        sClientTexts += "arrTextosML[4] = '" + JSText(Textos(42)) + "';"  'Seleccione un unidad qa
        sClientTexts += "arrTextosML[5] = '" + JSText(Textos(43)) + "';" 'Error Ajax
        sClientTexts += "arrTextosML[6] = '" + JSText(Textos(44)) + "';" 'Error de Eliminaci�n o retorno incontrolado
        sClientTexts += "arrTextosML[7] = '" + JSText(Textos(45)) + "';" 'en proceso
        sClientTexts += "arrTextosML[8] = '" + JSText(Textos(46)) + "';" 'Guardar.En este caso en vez de un alert mostraremos un confirm:		                
        'Si el usuario confirma la acci�n, antes de llamar a guardarInstancia procederemos a eliminar la versi�n de tipo 3
        sClientTexts += "arrTextosML[9] = '" + JSText(Textos(47)) + "';" 'Si no es misma versi�n en funci�n del tipo mostraremos un  
        'alert y el usuario no podr� guardar los cambios. Tipo Usu Qa
        sClientTexts += "arrTextosML[10] = '" + JSText(Textos(48)) + "';" 'Si no es misma versi�n en funci�n del tipo mostraremos un  
        'alert y el usuario no podr� guardar los cambios. Tipo Proveedor
        sClientTexts += "arrTextosML[11] = '" + JSText(Textos(49)) + "';" 'Eliminar.En este caso en vez de un alert mostraremos un confirm:		                
        'Si el usuario confirma la acci�n, antes de llamar a guardarInstancia procederemos a eliminar la versi�n de tipo 3
        sClientTexts += "arrTextosML[12] = '" + JSText(Textos(50)) + "';" 'Cerrar.En este caso en vez de un alert mostraremos un confirm:		                
        'Si el usuario confirma la acci�n, antes de llamar a guardarInstancia procederemos a eliminar la versi�n de tipo 3
        sClientTexts += "arrTextosML[13] = '" + JSText(Textos(60)) + "';" 'Seleccione al menos un contacto del proveedor
        sClientTexts += "arrTextosML[14] = '" + JSText(Textos(76)) + "';" 'Seleccione al menos un contacto del proveedor
        sClientTexts += "arrTextosML[15] = '" + JSText(Textos(78)) + "';"  'Anular.En este caso en vez de un alert mostraremos un confirm:		                
        'Si el usuario confirma la acci�n, antes de llamar a guardarInstancia procederemos a eliminar la versi�n de tipo 3
        sClientTexts += "arrTextosML[16] = '" + JSText(Textos(81)) + "';"  'Se ha modificado el revisor de la no conformidad y no se han guardado 
        'los cambios, antes de proceder a cerrar la no conformidad, pulse guardar los cambios."
        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)
    End Sub
    Private Sub VisualizarLinkFlujo()
        Dim dsPermisosRol As DataSet
        dsPermisosRol = Session("FSN_User").VerDetallesPersona(oInstancia.ID)
        If dsPermisosRol.Tables.Count > 0 Then
            If oNoConformidad.Workflow > 0 Then  'Si no hay workflow no muestra el hiperv�nculo para el detalle del workflow
                If dsPermisosRol.Tables(0).Rows(0).Item("VER_FLUJO") = 1 Then    'Si el rol no tiene permiso para ver el detalle del flujo.
                    HyperDetalle.Visible = True
                    HyperDetalle.NavigateUrl = "../seguimiento/NWhistoricoestados.aspx?Instancia=" + CStr(oInstancia.ID)
                Else
                    HyperDetalle.Visible = False
                End If
            Else
                HyperDetalle.Visible = False
            End If
        End If
    End Sub
    ''' <summary>
    ''' Carga los datos de la no conformidad y de la instancia
    ''' </summary>
    ''' <param name="PosibleEditar">Si es posible editar la NC segun permisos, revisor, usuario conectado, ... etc</param>
    ''' <param name="sIdi">idioma</param>
    ''' <remarks>Llamada desde: Page_Load;  tiempo maximo:0,1</remarks>
    Private Sub CargarDatosNoConformidad(ByRef PosibleEditar As Boolean,
                                         ByVal sIdi As String)
        'Cargamos la variable de pantalla q indica si estamos con un revisor y q la nc esta cerrada din revisar
        mbPagNCRevisor = False
        If (oNoConformidad.Revisor = FSNUser.Cod) AndAlso
        ((oNoConformidad.Estado = TipoEstadoNoConformidad.CierrePosSinRevisar) OrElse (oNoConformidad.Estado = TipoEstadoNoConformidad.CierreNegSinRevisar)) Then
            mbPagNCRevisor = True

            Response.Cookies("LinkVolver").Value = Request.Cookies("LinkVolver").Value - 1

            FSNPageHeader.OnClientClickCierrePositivo = ""
            FSNPageHeader.OnClientClickCierreNegativo = "Rechazar_onclick();return false;"
        End If

        EstadoNoConf.Value = oNoConformidad.Estado
        Workflow.Value = oNoConformidad.Workflow

        If Request("Version") <> Nothing Then
            If Request("Version") <> oNoConformidad.UltimaVersion Or oNoConformidad.Instancia <> Request("Instancia") Then
                'No es la versi�n actual. Ocultamos botones de Guardar y emitir
                FSNPageHeader.VisibleBotonGuardar = False
                FSNPageHeader.VisibleBotonEmitir = False

                PosibleEditar = False
            End If
        End If

        oInstancia.Version = oNoConformidad.Version

        'SI EXISTE ALGUN CAMPO DE TIPO=3 (CALCULADO) HACEMOS VISIBLE EL BOTON DE CALCULAR
        FSNPageHeader.VisibleBotonCalcular = oInstanciaGrupos.Grupos.OfType(Of FSNServer.Grupo).Where(Function(x) x.DSCampos.Tables(0).Rows.OfType(Of DataRow).Where(Function(y) y("TIPO") = TipoCampoPredefinido.Calculado).Any).Any()
        If FSNPageHeader.VisibleBotonCalcular Then BotonCalcular.Value = 1

        'Carga los datos de la no conformidad:
        fsentryNoConfFecDesde.DateFormat = FSNUser.DateFormat
        fsentryNoConfFecDesde.Valor = oNoConformidad.FechaLimResol
        lblFechaResolver.Text = oNoConformidad.FechaLimResol.ToString("d", FSNUser.DateFormat)
        fsentryFecApliMejoras.DateFormat = FSNUser.DateFormat
        fsentryFecApliMejoras.Valor = oNoConformidad.FechaAplicPlan
        If IsDate(oNoConformidad.FechaAplicPlan) AndAlso oNoConformidad.FechaAplicPlan <> "#12:00:00 AM#" Then
            txtFecApliMejoras.Text = oNoConformidad.FechaAplicPlan.ToString("d", FSNUser.DateFormat)
        Else
            txtFecApliMejoras.Text = ""
        End If
        NoConformidad.Value = oNoConformidad.ID
        Proveedor.Value = oNoConformidad.Proveedor
        DenProveedor.Value = oNoConformidad.ProveDen
        Tipo.Value = oNoConformidad.TipoDen
        FecAlta.Value = oNoConformidad.FechaAlta

        Dim Estado As String = ""
        Select Case oNoConformidad.Estado
            Case TipoEstadoNoConformidad.Guardada
                Estado = Textos(55)

                txtComent.Visible = True
                txtComent.Text = oNoConformidad.ComentAlta

                lblComentarios.Visible = True
            Case TipoEstadoNoConformidad.Abierta
                Estado = Textos(54)

                txtComent.Visible = False
                lblComentarios.Visible = False
            Case TipoEstadoNoConformidad.CierreNegativo
                Estado = Textos(56)

                txtComent.Visible = False
                lblComentarios.Visible = False
            Case TipoEstadoNoConformidad.CierrePosEnPlazo
                Estado = Textos(57)

                txtComent.Visible = False
                lblComentarios.Visible = False
            Case TipoEstadoNoConformidad.CierrePosFueraPlazo
                Estado = Textos(58)

                txtComent.Visible = False
                lblComentarios.Visible = False
            Case TipoEstadoNoConformidad.CierreNegSinRevisar, TipoEstadoNoConformidad.CierrePosSinRevisar
                Estado = Textos(59)

                txtComent.Visible = False
                lblComentarios.Visible = False
            Case TipoEstadoNoConformidad.Anulada
                Estado = Textos(75) '"Anulada"
                txtComent.Visible = False
                lblComentarios.Visible = False
            Case TipoEstadoNoConformidad.EnCursoDeAprobacion
                Estado = Textos(93) 'En curso
                txtComent.Visible = True
                txtComent.Text = oNoConformidad.ComentAlta
                lblComentarios.Visible = True
            Case Else 'Pdte validar cierre
                Estado = Textos(85)

                txtComent.Visible = False
                lblComentarios.Visible = False
        End Select

        If Not mbPagNCRevisor Then
            LineaRevisor.Visible = False
        Else
            LineaRevisor.Visible = True
            'Etiqueta de estado de cierre en la cabecera para revisor
            If oNoConformidad.Estado = TipoEstadoNoConformidad.CierrePosSinRevisar Then
                lblCierreRevisor.Text = Textos(79)
            Else
                lblCierreRevisor.Text = Textos(80)
            End If
        End If

        FSNPageHeader.TituloCabecera = oNoConformidad.TipoDen

        lblInfoNoConformidad.Text = oNoConformidad.Instancia & " - " & oNoConformidad.ProveDen & " (" & oNoConformidad.Proveedor & ") " &
            oNoConformidad.FechaAlta.ToString("d", FSNUser.DateFormat) & " " &
            "(" & Estado & " - " & oNoConformidad.Peticionario & ")"

        mbEsNCEscalacionEstadoPdteValidar = False
        If oNoConformidad.Es_Noconformidad_Escalacion AndAlso oNoConformidad.EstadoEscalacionProvePdte Then
            mbEsNCEscalacionEstadoPdteValidar = True
        End If
    End Sub
    ''' <summary>
    ''' Dependiendo de si ObtenerUnidadesNegocio es True o False, unicamente obtiene las unidades de negocio
    ''' de las que tiene permiso para modificar o carga el combo de unidades de negocio
    ''' </summary>
    ''' <param name="PosibleEditar">Parametro que modificaremos si no tiene permiso para editar la NC</param>
    ''' <param name="UnidadesNegocio">Tabla donde estar�n o cargaremos las unidades de negocio</param>
    ''' <param name="ObtenerUnidadesNegocio">Si cargaos combo o si obtenemos las unidades de negocio por primera vez</param>
    ''' <param name="sIdi">Idioma del usuario</param>
    ''' <remarks></remarks>
    Private Sub CargarUnidadesNegocio(ByRef PosibleEditar As Boolean, ByRef UnidadesNegocio As DataTable,
                                  ByVal ObtenerUnidadesNegocio As Boolean, ByVal sIdi As String,
                                  Optional ByVal TipoPermisoConsulta As Integer = TipoAccesoUNQAS.ConsultarNoConformidades)
        If ObtenerUnidadesNegocio Then
            Dim oUnidadesQA As FSNServer.UnidadesNeg
            oUnidadesQA = FSNServer.Get_Object(GetType(FSNServer.UnidadesNeg))

            oUnidadesQA.UsuLoadData(FSNUser.Cod, sIdi,
                                    Application("Nivel_UnQa"),
                                    TipoPermisoConsulta)

            UnidadesNegocio = oUnidadesQA.MontaDataSetUnaBanda("", True)
        Else
            If PosibleEditar Then
                'Cargo el combo con las unidades de negocio en las que tenga permiso de emisi�n.
                'Como si Reabri=>Emitir y Modificar=>Emitir, cargo tb los que tengan permiso 3 y 9
                UnidadesNegocio.DefaultView.RowFilter() = "IDPERMISO LIKE '%#1' OR IDPERMISO LIKE '%#3' OR IDPERMISO LIKE '%#9'"

                'Se pasan las unidades de negocio y solo tendriamos que cargar el combo
                Select Case UnidadesNegocio.DefaultView.Count
                    Case 0
                        ComboQA.Value = 0
						UnicoUnidadQA.Value = oNoConformidad.UnidadQA
						lblUnicoUnidadQA.Text = ""
                        wddUnidadQA.Visible = False
                    Case 1
                        ComboQA.Value = 0
                        UnicoUnidadQA.Value = UnidadesNegocio.DefaultView.Item(0)("ID")
                        lblUnicoUnidadQA.Text = UnidadesNegocio.DefaultView.Item(0)("DEN")
                        wddUnidadQA.Visible = False
                    Case Else
                        ComboQA.Value = 1
                        lblUnicoUnidadQA.Visible = False
                        wddUnidadQA.DataSource = UnidadesNegocio.DefaultView.ToTable(True, "Id", "Den")
                        wddUnidadQA.TextField = "DEN"
                        wddUnidadQA.ValueField = "ID"
                        wddUnidadQA.DataBind()
                        'seleccionamos la unidad de negocio correspondiente a la no conformidad
                        For Each Item As Infragistics.Web.UI.ListControls.DropDownItem In wddUnidadQA.Items
                            If Item.Value = oNoConformidad.UnidadQA Then
                                wddUnidadQA.SelectedItemIndex = Item.Index
                                lblUnicoUnidadQA.Text = Item.Text
                                Exit For
                            End If
                        Next

                        If wddUnidadQA.SelectedItem Is Nothing Then
                            'Le han quitado el permiso para modificar una NC que es suya 
                            'en la unidad de negocio en la que la emiti�.
                            Dim oUnidadesQA As FSNServer.UnidadesNeg
                            oUnidadesQA = FSNServer.Get_Object(GetType(FSNServer.UnidadesNeg))

                            PosibleEditar = False

                            wddUnidadQA.Visible = False
                            lblUnicoUnidadQA.Visible = True
                            UnicoUnidadQA.Value = oNoConformidad.UnidadQA
                            lblUnicoUnidadQA.Text = oUnidadesQA.LoadUnQa(sIdi, oNoConformidad.UnidadQA).Rows(0)("UNIDAD")
                        End If
                End Select

                'Si es posible editar la NC se muestra el combo de Prove ERP
                If oNoConformidad.Estado = TipoEstadoNoConformidad.Abierta Or oNoConformidad.Estado = TipoEstadoNoConformidad.Guardada Or oNoConformidad.Estado = TipoEstadoNoConformidad.EnCursoDeAprobacion Then
                    lblProveERPNoEdit.Visible = False
                    wddProveERP.Visible = True
                Else
                    lblProveERPNoEdit.Visible = True
                    wddProveERP.Visible = False
                End If
            Else
                Dim oUnidadesQA As FSNServer.UnidadesNeg
                oUnidadesQA = FSNServer.Get_Object(GetType(FSNServer.UnidadesNeg))

                lblUnicoUnidadQA.Text = oUnidadesQA.LoadUnQa(sIdi, oNoConformidad.UnidadQA).Rows(0)("UNIDAD")
                wddUnidadQA.Visible = False
                UnicoUnidadQA.Value = oNoConformidad.UnidadQA

                'Si NO es posible editar la NC se muestra el label de Prove ERP
                lblProveERPNoEdit.Visible = True
                wddProveERP.Visible = False
            End If
        End If

        'Visibilidad de Prove ERP seg�n UNQA
        If Not wddUnidadQA.Visible And Not lblUnicoUnidadQA.Visible Then
            wddProveERP.Style.Item(HtmlTextWriterStyle.Display) = "none"
            lblProveERP.Style.Item(HtmlTextWriterStyle.Display) = "none"
            lblProveERPNoEdit.Style.Item(HtmlTextWriterStyle.Display) = "none"
        Else
            'A partir de la UNQA se obtiene la visibilidad de PROVE_ERP y la empresa
            Dim oProveVis As Integer() = ProveERPVisible(oNoConformidad.UnidadQA)
            Dim bProveERPVisible As Boolean = CType(oProveVis(0), Boolean)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "set_emp", "var emp=" & oProveVis(1) & ";", True)

            lblProveERP.Style.Item(HtmlTextWriterStyle.Display) = IIf(bProveERPVisible, "inline-block", "none")
            Dim sProveERPDen As String = String.Empty
            If PosibleEditar Then
                wddProveERP.Style.Item(HtmlTextWriterStyle.Display) = IIf(bProveERPVisible, "inline-block", "none")
                If bProveERPVisible Then
                    wddProveERP.ClientEvents.DropDownOpening = "wddProveERP_DropDownOpening"
                    wddUnidadQA.ClientEvents.SelectionChanged = "wddUnidadQA_SelectionChanged"
                End If
            Else
                lblProveERPNoEdit.Style.Item(HtmlTextWriterStyle.Display) = IIf(bProveERPVisible, "inline-block", "none")
                If bProveERPVisible Then
                    'Hasta que no tengo la empresa no puedo obtener la denominaci�n del prove ERP porque puede tener el mismo c�digo y distintas denominaciones para cada empresa
                    Dim oProvesERP As FSNServer.CProveERPs = FSNServer.Get_Object(GetType(FSNServer.CProveERPs))
                    Dim dsProveERp As DataSet = oProvesERP.CargarProveedoresERPtoDS(oNoConformidad.Proveedor,, IIf(oNoConformidad.ProveedorERP Is Nothing, "", oNoConformidad.ProveedorERP))
                    If Not dsProveERp Is Nothing AndAlso dsProveERp.Tables.Count > 0 AndAlso dsProveERp.Tables(0).Rows.Count > 0 Then sProveERPDen = dsProveERp.Tables(0).Rows(0)("DEN")
                End If
            End If
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "proveERPDen") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "proveERPDen", "var proveERPDen='" & sProveERPDen & "';", True)
            Else
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "actProveERPDen", "proveERPDen='" & sProveERPDen & "';", True)
            End If
        End If
    End Sub
    ''' <summary>Devuelve si el campo de proveedor en ERP debe estar visible</summary>
    ''' <param name="UNQA">Id de la UNQA</param>
    ''' <returns>booleano indicando la visibilidad</returns>
    ''' <remarks>llamada desde: ConfigurarControles</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ProveERPVisible(ByVal UNQA As Integer) As Integer()
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oProve As FSNServer.Proveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
        Dim dtDatos As DataTable = oProve.VisibilidadProveERP_NC(UNQA).Tables(0)
        Return New Integer() {dtDatos.Rows(0)("VISIBLE"), dtDatos.Rows(0)("EMPRESA")}
    End Function
    ''' <summary>
    ''' Carga el combo de revisores si la instalaci�n contempla revisores y si tiene permiso de modificar la NC
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarRevisores()
        'Cargar Combo Revisor
        Dim oRevisores As FSNServer.Users 'A�ADIDO QA FIGURA REVISOR

        oRevisores = FSNServer.Get_Object(GetType(FSNServer.Users))
        oRevisores.LoadData(FSNUser.FSQARestRevisorUO, FSNUser.FSQARestRevisorDep, FSNUser.UON1, FSNUser.UON2, FSNUser.UON3, FSNUser.Dep)

        wddRevisor.DataSource = oRevisores.Data
        wddRevisor.TextField = "DEN"
        wddRevisor.ValueField = "COD"
        wddRevisor.DataBind()

        Dim bRevisorEncontrado As Boolean = False
        'seleccionamos el revisor dado de alta por defecto
        For Each Item As Infragistics.Web.UI.ListControls.DropDownItem In wddRevisor.Items
            If Item.Value = oNoConformidad.Revisor Then
                wddRevisor.SelectedItemIndex = Item.Index
                Me.LbltxtRevisorBD.Text = Item.Text
                bRevisorEncontrado = True
                Exit For
            End If
        Next

		If Not bRevisorEncontrado Then
			wddRevisor.Visible = False
			LbltxtRevisorBD.Visible = True
			LbltxtRevisorBD.Text = oNoConformidad.RevisorNombre
			codRevisor.Value = oNoConformidad.Revisor
		End If

		CodigoRevisorAnterior.Value = IIf(oNoConformidad.Revisor Is Nothing, "", oNoConformidad.Revisor)
    End Sub
    ''' <summary>
    ''' Carga los datos de la no conformidad y de la instancia
    ''' </summary>
    ''' <param name="PosibleEditar">Si es posible editar la NC segun permisos, revisor, usuario conectado, ... etc</param>
    ''' <param name="sIdi">idioma</param>
    ''' <remarks>Llamada desde: Page_Load;  tiempo maximo:0,1</remarks>
    Private Sub CargarGruposCamposInstancia(ByVal PosibleEditar As Boolean, ByVal sIdi As String)
        Dim oGrupo As FSNServer.Grupo
        Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
        Dim oRow As DataRow = Nothing
        Dim oRowAccion As DataRow
        Dim oucDesglose As desgloseControl
        Dim oucCampos As campos
        Dim blnAlgunaAccion As Boolean = False
        Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim lIndex As Integer = 0

        'Rellena los grupos y campos de la instancia:
        uwtGrupos.Tabs.Clear()
        AplicarEstilosTab(uwtGrupos)

        Dim oDSEstados As DataSet = oNoConformidadEstados
        Dim oAcciones As DataTable = Nothing
        If PosibleEditar And oNoConformidad.Estado <= TipoEstadoNoConformidad.Abierta Then
            oAcciones = oNoConformidadAcciones
        End If

        Version.Value = oInstancia.Version
        VersionInicialCargada.Value = oInstancia.Version
        Instancia.Value = oInstancia.ID

        If Not oInstanciaGrupos.Grupos Is Nothing Then 'oInstancia.Grupos.Grupos Is Nothing Then '
            If mbPagNCRevisor Then
                'vamos a recorrernos que acciones se han solicitado
                For Each oRow In oNoConformidad.Acciones.Rows
                    'hay que localizar el registro de la configuraci�n de cumplimentacion del desglose de acciones para hacer valer este dato en lugar del que traiga de conf-cumplimentaci�n
                    Dim find(0) As Object
                    Dim key(0) As DataColumn
                    Dim oRowCONFCUMP As DataRow

                    oGrupo = oInstancia.Grupos.Grupos.Item(oRow.Item("GRUPO").ToString)
                    key(0) = oGrupo.DSCampos.Tables(0).Columns("ID_CAMPO")
                    oGrupo.DSCampos.Tables(0).PrimaryKey = key
                    find(0) = oRow.Item("COPIA_CAMPO")
                    oRowCONFCUMP = oGrupo.DSCampos.Tables(0).Rows.Find(find)
                    oRowCONFCUMP.Item("VISIBLE") = oRow.Item("SOLICITAR")
                Next
            End If

            For Each oGrupo In oInstanciaGrupos.Grupos
                Dim blnHayAccion As Boolean  'para indicar si se tiene que mostrar o no el tab, si no tiene ninguna accion mostrarlo
                If oNoConformidad.Acciones.Rows.Count = 0 Then
                    blnHayAccion = True
                    blnAlgunaAccion = False
                Else
                    blnAlgunaAccion = True
                    For Each oRowAccion In oNoConformidad.Acciones.Rows
                        If oRowAccion.Item("GRUPO") = oGrupo.Id Then
                            If (oRowAccion.Item("SOLICITAR") = 1) Or (oRowAccion.Item("SOLICITAR") = 0 And PosibleEditar) Then
                                blnHayAccion = True
                                Exit For
                            Else
                                blnHayAccion = False
                                Exit For
                            End If
                        Else
                            blnHayAccion = True
                        End If
                    Next
                End If

                If blnHayAccion Then
                    oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden

                    oInputHidden.ID = "txtPre_" + lIndex.ToString
                    oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                    Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
                    Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
                    oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(sIdi), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
                    oTabItem.Key = lIndex.ToString
                    oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Hidden
                    uwtGrupos.Tabs.Add(oTabItem)
                    lIndex += 1

                    If oGrupo.NumCampos <= 1 Then
                        For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                            If oRow.Item("VISIBLE") = 1 Then
                                Exit For
                            End If
                        Next

                        If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Desglose Or oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoDesglose Then
                            If oRow.Item("VISIBLE") = 0 Then
                                uwtGrupos.Tabs.Remove(oTabItem)
                            Else
                                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                                oTabItem.ContentPane.UserControlUrl = "..\alta\desglose.ascx"
                                oucDesglose = oTabItem.ContentPane.UserControl
                                oucDesglose.ID = oGrupo.Id.ToString
                                oucDesglose.Campo = oRow.Item("ID_CAMPO")
                                oucDesglose.TabContainer = uwtGrupos.ClientID
                                oucDesglose.Instancia = oInstancia.ID
                                oucDesglose.Version = oInstancia.Version
                                oucDesglose.TieneIdCampo = True
                                oucDesglose.Ayuda = DBNullToSomething(oRow.Item("AYUDA_" & Idioma))
                                oucDesglose.SoloLectura = Not PosibleEditar 'OrElse oRow.Item("ESCRITURA") = 0  
                                oucDesglose.FuerzaLectura = (oNoConformidad.Estado > TipoEstadoNoConformidad.Abierta) OrElse (oNoConformidad.ProveedorBaja)
                                oucDesglose.Proveedor = oNoConformidad.Proveedor
                                oucDesglose.Acciones = oAcciones
                                oucDesglose.Titulo = DBNullToSomething(oRow.Item("DEN_" + sIdi))
                                oucDesglose.DSEstados = oDSEstados
                                oucDesglose.TipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad
                                oucDesglose.EnCursoDeAprobacion = enCursoDeAprobacion
                                oInputHidden.Value = oucDesglose.ClientID
                            End If
                        Else
                            oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                            oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                            oucCampos = oTabItem.ContentPane.UserControl
                            oucCampos.Instancia = oInstancia.ID
                            oucCampos.ID = oGrupo.Id.ToString
                            oucCampos.dsCampos = oGrupo.DSCampos
                            oucCampos.IdGrupo = oGrupo.Id
                            oucCampos.Idi = sIdi
                            oucCampos.TabContainer = uwtGrupos.ClientID
                            oucCampos.Version = oInstancia.Version
                            oucCampos.Proveedor = oNoConformidad.Proveedor
                            oucCampos.DSEstados = oDSEstados
                            oucCampos.Acciones = oAcciones
                            oucCampos.SoloLectura = Not PosibleEditar
                            oucCampos.FuerzaDesgloseLectura = (oNoConformidad.Estado > TipoEstadoNoConformidad.Abierta) OrElse (oNoConformidad.ProveedorBaja)
                            oucCampos.InstanciaMoneda = oInstancia.Moneda
                            oucCampos.TipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad
                            oucCampos.EnCursoDeAprobacion = enCursoDeAprobacion
                            oInputHidden.Value = oucCampos.ClientID
                            oucCampos.Formulario = oInstancia.Solicitud.Formulario
                            oucCampos.ObjInstancia = oInstancia
                        End If
                    Else
                        oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                        oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                        oucCampos = oTabItem.ContentPane.UserControl
                        oucCampos.Instancia = oInstancia.ID
                        oucCampos.ID = oGrupo.Id.ToString
                        oucCampos.dsCampos = oGrupo.DSCampos
                        oucCampos.IdGrupo = oGrupo.Id
                        oucCampos.Idi = sIdi
                        oucCampos.TabContainer = uwtGrupos.ClientID
                        oucCampos.Version = oInstancia.Version
                        oucCampos.Proveedor = oNoConformidad.Proveedor
                        oucCampos.Acciones = oAcciones
                        oucCampos.DSEstados = oDSEstados
                        oucCampos.SoloLectura = Not PosibleEditar
                        oucCampos.FuerzaDesgloseLectura = Not PosibleEditar
                        oucCampos.InstanciaMoneda = oInstancia.Moneda
                        oucCampos.TipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad
                        oucCampos.EnCursoDeAprobacion = enCursoDeAprobacion
                        oInputHidden.Value = oucCampos.ClientID
                        oucCampos.Formulario = oInstancia.Solicitud.Formulario
                        oucCampos.ObjInstancia = oInstancia
                    End If

                    Me.FindControl("frmDetalle").Controls.Add(oInputHidden)
                End If
            Next
        End If

        Dim TodasAccionesFinalizadasRevisadas As Boolean = True

        If Not oNoConformidad.Data Is Nothing Then
            If oNoConformidad.EstadosComentariosInternosAcciones.Rows.Count > 0 Then
                For Each oRow In oNoConformidad.EstadosComentariosInternosAcciones.Rows
                    If oRow.Item("ESTADO_INT") <> TipoEstadoAcciones.FinalizadaRevisada Then
                        TodasAccionesFinalizadasRevisadas = False
                        Exit For
                    End If
                Next
            ElseIf blnAlgunaAccion Then
                TodasAccionesFinalizadasRevisadas = False
            End If
        End If

        If FSNUser.QACierreNoConf Then
            If TodasAccionesFinalizadasRevisadas Then
                lblSeleccioneNotificados.Text = Textos(63)
            Else
                lblSeleccioneNotificados.Text = Textos(62)
            End If
        Else
            If TodasAccionesFinalizadasRevisadas Then
                lblSeleccioneNotificados.Text = Textos(62)
            Else
                lblSeleccioneNotificados.Text = Textos(61)

                FSNPageHeader.OcultarBotonCierrePositivo = True
                FSNPageHeader.OcultarBotonCierreNegativo = True
            End If
        End If

        Me.FindControl("frmDetalle").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())
    End Sub
    ''' <summary>Carga la pesta�a de participantes si es necesario</summary>
    Private Sub CargarParticipantes()
        Dim oRol As FSNServer.Rol = FSNServer.Get_Object(GetType(FSNServer.Rol))
        oRol.Id = oInstancia.RolActual
        oRol.Bloque = oInstancia.Etapa
        oRol.CargarParticipantes(Idioma, oInstancia.ID)
        If oRol.Participantes.Tables(0).Rows.Count > 0 Then
            Dim oTabItem As New Infragistics.WebUI.UltraWebTab.Tab
            oTabItem.Text = Textos(94) 'Participantes
            oTabItem.Key = "PARTICIPANTES"

            uwtGrupos.Tabs.Add(oTabItem)

            oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
            oTabItem.ContentPane.UserControlUrl = "../alta/participantes.ascx"

            Dim oucParticipantes As participantes = oTabItem.ContentPane.UserControl
            oucParticipantes.Idi = Idioma
            oucParticipantes.TabContainer = uwtGrupos.ClientID
            oucParticipantes.dsParticipantes = oRol.Participantes

            Dim oInputHidden As New System.Web.UI.HtmlControls.HtmlInputHidden
            oInputHidden.ID = "txtPre_PARTICIPANTES"
            oInputHidden.Value = oucParticipantes.ClientID
            Me.FindControl("frmDetalle").Controls.Add(oInputHidden)
        End If
    End Sub
    ''' <summary>
    ''' Montar la tabla Comentarios 
    ''' </summary>
    ''' <param name="PosibleEditar">Si es posible editar la NC segun permisos, revisor, usuario conectado, ... etc</param>
    ''' <param name="sIdi">idioma</param>
    ''' <remarks>Llamadad desde: Page_Load; Tiempo maximo:0,1</remarks>
    Private Sub MontarTablaComentarios(ByVal PosibleEditar As Boolean, ByVal sIdi As String)
        If FSNServer.TipoAcceso.gbFSQA_Revisor Then
            'FSQA_REVISOR = 1 EN PARGEN_INTERNO
            If (oNoConformidad.Estado = TipoEstadoNoConformidad.Abierta) And
                ((oNoConformidad.AccionEstado = TipoAccionSolicitud.RechazoEficazRevisor) Or
                (oNoConformidad.AccionEstado = TipoAccionSolicitud.RechazoNoEficazRevisor)) And
                 (UCase(FSNUser.Cod) = UCase(oNoConformidad.PeticionarioCod)) And
                 (UCase(oNoConformidad.Revisor) = UCase(oNoConformidad.AccionEstadoPersona)) Then
                'La noConformidad esta abierta
                'El usuario q entra es el peticionario
                'La no Conformidad ha sido rechazada por el revisor
                'el peticionario no  ha cambiado  el revisor

                'Etiqueta de estado de cierre en la cabecera
                If oNoConformidad.AccionEstado = TipoAccionSolicitud.RechazoEficazRevisor Then
                    lblCierreInforme.Text = Textos(34)
                Else
                    lblCierreInforme.Text = Textos(35)
                End If

                'TEXTOS DE LOS LABELS.
                lblComentRechazo.Text = Textos(32)
                lblComentCierre.Text = Textos(33)

                'Comentarios
                oNoConformidad.CargarComentarioCierre()
                lbltxtComentCierre.Text = oNoConformidad.ComentCierre
                lbltxtComentrechazo.Text = oNoConformidad.AccionEstadoComentario

            ElseIf (UCase(FSNUser.Cod) = UCase(oNoConformidad.Revisor)) _
            AndAlso (oNoConformidad.Estado > TipoEstadoNoConformidad.Abierta) Then
                oNoConformidad.CargarComentarioCierre()
                lblComentCierre.Text = Textos(33)
                lbltxtComentCierre.Text = oNoConformidad.ComentCierre
                lblCierreInforme.Visible = False
                lblComentRechazo.Visible = False
                lbltxtComentrechazo.Visible = False
            Else 'no se muestran comentarios
                lblCierreInforme.Visible = False
                lblComentRechazo.Visible = False
                lblComentCierre.Visible = False
                lbltxtComentrechazo.Visible = False
                lbltxtComentCierre.Visible = False
            End If
        Else
            lblCierreInforme.Visible = False
            lblComentRechazo.Visible = False
            lblComentCierre.Visible = False
            lbltxtComentrechazo.Visible = False
            lbltxtComentCierre.Visible = False
        End If
    End Sub
    ''' <summary>
    ''' Muestra en la cabecera las acciones con susu fechas de resoluci�n y si son solicitadas
    ''' </summary>
    ''' <param name="PosibleEditar">Parametro que modificaremos si no tiene permiso para editar la NC</param>
    ''' <remarks>Llamada desde:page_load; Tiempo m�ximo:0</remarks>
    Private Sub MontarTablaAcciones(ByVal PosibleEditar As Boolean)
        Dim iColumna As Integer
        Dim iFila As Integer
        Dim oTable As System.Web.UI.WebControls.Table
        Dim otblRow As System.Web.UI.WebControls.TableRow
        Dim otblCell As System.Web.UI.WebControls.TableCell
        Dim olabel As System.Web.UI.WebControls.Label
        Dim oEntryAccion As DataEntry.GeneralEntry
        Dim Editable As Boolean = True

        If oNoConformidad.Estado <> TipoEstadoNoConformidad.Abierta And oNoConformidad.Estado <> TipoEstadoNoConformidad.Guardada Then
            If Not oNoConformidad.Es_Noconformidad_Escalacion OrElse oNoConformidad.Estado <> TipoEstadoNoConformidad.CierrePosPendiente Then
                Editable = False
            End If
        End If

        If oNoConformidad.Acciones.Rows.Count > 0 Then
            iColumna = 1
            iFila = 0
            For Each oRow In oNoConformidad.Acciones.Rows
                If (oRow.Item("SOLICITAR") = 1) Or (oRow.Item("SOLICITAR") = 0 And PosibleEditar) Then
                    If iColumna = 1 Then 'a�ade una fila nueva a la tabla
                        otblRow = New System.Web.UI.WebControls.TableRow
                        iFila = iFila + 1
                    Else  'Se situa en la fila actual
                        otblRow = Me.tblAcciones.Rows.Item(iFila - 2)
                    End If
                    'Accion
                    otblCell = New System.Web.UI.WebControls.TableCell
                    If PosibleEditar And Editable Then   'Si es el peticionario 
                        oEntryAccion = New DataEntry.GeneralEntry
                        oEntryAccion.CampoDef_CampoODesgHijo = 0 'en NOCONF_SOLICIT_ACC va solo campo_padre
                        oEntryAccion.CampoDef_DesgPadre = oRow.Item("ID_CCDef")
                        oEntryAccion.ID = "NoConfACCION_" & oRow.Item("COPIA_CAMPO")
                        oEntryAccion.Desglose = False
                        oEntryAccion.IsGridEditor = False
                        oEntryAccion.ReadOnly = False
                        oEntryAccion.BaseDesglose = False
                        oEntryAccion.Calculado = False
                        oEntryAccion.Desglose = False
                        oEntryAccion.Obligatorio = False
                        oEntryAccion.Intro = 0
                        oEntryAccion.Independiente = "Accion"
                        If oNoConformidad.Es_Noconformidad_Escalacion Then
                            oEntryAccion.Text = Textos(89)
                        Else
                            oEntryAccion.Text = Textos(10) & " " & oRow.Item("DEN_ACCION")
                        End If
                        oEntryAccion.InputStyle.CssClass = "captionBlueSmall"
                        oEntryAccion.Width = Unit.Percentage(100)
                        oEntryAccion.Tag = "NoConfACCION_" & oRow.Item("COPIA_CAMPO")
                        oEntryAccion.Tipo = TiposDeDatos.TipoGeneral.TipoCheckBox
                        oEntryAccion.TipoGS = TiposDeDatos.TipoCampoGS.SinTipo
                        If oRow.Item("SOLICITAR") = 1 Then
                            oEntryAccion.Valor = True
                        Else
                            oEntryAccion.Valor = False
                        End If
                        otblCell.Controls.Add(oEntryAccion)

                        Dim pag As FSNPage = Me.Page
                        oEntryAccion.Title = Me.Page.Title
                    Else
                        olabel = New System.Web.UI.WebControls.Label
                        If oNoConformidad.Es_Noconformidad_Escalacion Then
                            olabel.Text = Textos(89)
                        Else
                            olabel.Text = oRow.Item("DEN_ACCION") & " " & Textos(12)
                        End If

                        olabel.CssClass = "captionBlueSmall"
                        otblCell.Controls.Add(olabel)
                    End If
                    otblRow.Cells.Add(otblCell)

                    If iColumna = 1 Then  'a�ade las filas
                        tblAcciones.Rows.Add(otblRow)
                        otblRow = New System.Web.UI.WebControls.TableRow
                        iFila = iFila + 1
                    Else
                        otblRow = Me.tblAcciones.Rows.Item(iFila - 1)
                    End If

                    'Fecha l�mite:
                    otblCell = New System.Web.UI.WebControls.TableCell

                    Dim otblRow2 As System.Web.UI.WebControls.TableRow
                    Dim otblCell2 As System.Web.UI.WebControls.TableCell

                    oTable = New System.Web.UI.WebControls.Table
                    otblRow2 = New System.Web.UI.WebControls.TableRow
                    otblCell2 = New System.Web.UI.WebControls.TableCell

                    If PosibleEditar And Editable Then  'Si es el peticionario 
                        olabel = New System.Web.UI.WebControls.Label
                        olabel.Text = Textos(11)
                        olabel.CssClass = "captionBlueSmall"
                        otblCell2.Controls.Add(olabel)
                        otblRow2.Cells.Add(otblCell2)

                        otblCell2 = New System.Web.UI.WebControls.TableCell
                        oEntryAccion = New DataEntry.GeneralEntry
                        oEntryAccion.CampoDef_CampoODesgHijo = 0 'en NOCONF_SOLICIT_ACC va solo campo_padre
                        oEntryAccion.CampoDef_DesgPadre = oRow.Item("ID_CCDef")
                        oEntryAccion.ID = "NoConfFECLIM_" & oRow.Item("COPIA_CAMPO")
                        oEntryAccion.Desglose = False
                        oEntryAccion.IsGridEditor = False
                        oEntryAccion.ReadOnly = False
                        oEntryAccion.BaseDesglose = False
                        oEntryAccion.Calculado = False
                        oEntryAccion.Desglose = False
                        oEntryAccion.Obligatorio = False
                        oEntryAccion.Independiente = "Accion"
                        oEntryAccion.Tag = "NoConfFECLIM_" & oRow.Item("COPIA_CAMPO")
                        oEntryAccion.Tipo = TiposDeDatos.TipoGeneral.TipoFecha
                        oEntryAccion.TipoGS = TiposDeDatos.TipoCampoGS.SinTipo
                        oEntryAccion.Valor = oRow.Item("FECHA_LIMITE")
                        oEntryAccion.InputStyle.CssClass = "TipoFecha"
                        oEntryAccion.DateFormat = FSNUser.DateFormat
                        otblCell2.Controls.Add(oEntryAccion)
                        Dim pag As FSNPage = Page
                        oEntryAccion.Title = Page.Title
                    Else
                        olabel = New System.Web.UI.WebControls.Label
                        If Not IsDBNull(oRow.Item("FECHA_LIMITE")) Then
                            olabel.Text = Textos(11) & " " & FormatDate(oRow.Item("FECHA_LIMITE"), FSNUser.DateFormat)
                        Else
                            olabel.Text = Textos(11)
                        End If
                        olabel.CssClass = "captionBlueSmall"
                        olabel.Width = Unit.Percentage(100)
                        otblCell.Controls.Add(olabel)
                    End If

                    otblRow2.Cells.Add(otblCell2)

                    'a�ade la fila a la tabla de acciones
                    oTable.Rows.Add(otblRow2)
                    otblCell.Controls.Add(oTable)
                    otblRow.Cells.Add(otblCell)

                    If iColumna = 3 Then
                        iColumna = 1
                    Else
                        If iColumna = 1 Then
                            Me.tblAcciones.Rows.Add(otblRow)
                        End If
                        iColumna = iColumna + 1
                    End If
                End If
            Next
        End If
    End Sub
    ''' <summary>
    ''' Carga el combo de Version
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo m�ximo:0</remarks>
    Private Sub CargarComboVersiones()
        ''''''Combo de versi�n: 
        oNoConformidad.LoadVersiones()

        Dim dt As New DataTable
        dt.Columns.Add("CLAVE", GetType(System.String))
        dt.Columns.Add("TEXTO", GetType(System.String))

        indiceCombo.Value = -1

        If enCursoDeAprobacion OrElse oNoConformidad.Data.Tables(0).Rows.Count <= 1 Then
            Me.lblVersion.Attributes.Add("style", "visibility: hidden")
            Me.wddVersion.Attributes.Add("style", "visibility: hidden")
        End If

        Dim Index As Integer = 0
        Dim dFecha As Date
        Dim sQuien As String
        Dim DifUTC As Integer = oInstancia.DiferenciaUTCServidor
        Dim DifOtz As Integer = Request.QueryString("Otz")

        For Each row As DataRow In oNoConformidad.Data.Tables(0).Rows
            If row.Item("NUM_VERSION") = oNoConformidad.Version AndAlso row.Item("INSTANCIA") = oInstancia.ID Then
                indiceCombo.Value = Index
            Else
                Index = Index + 1
            End If

            dFecha = row.Item("FECHA_MODIF")
            dFecha = dFecha.AddMinutes(-1 * (DifUTC + DifOtz))

            sQuien = IIf(IsDBNull(row.Item("PERSONA")), row.Item("USUPORT_NOM"), row.Item("PERSONA"))

            Dim rowAdd As DataRow = dt.NewRow

            rowAdd.Item("CLAVE") = CStr(row.Item("INSTANCIA")) & "##"
            rowAdd.Item("CLAVE") = rowAdd.Item("CLAVE") & CStr(row.Item("NUM_VERSION"))

            rowAdd.Item("TEXTO") = FormatDate(dFecha, FSNUser.DateFormat, "g") & " " & sQuien

            dt.Rows.Add(rowAdd)
        Next

        wddVersion.DataSource = dt
        wddVersion.TextField = "TEXTO"
        wddVersion.ValueField = "CLAVE"
        wddVersion.DataBind()
        wddVersion.SelectedItemIndex = indiceCombo.Value
    End Sub
    ''' <summary>
    ''' Establece la visibilidad de los botones del header de la pagina en funci�n de a q tenga permiso el usuario o si es revisor
    ''' </summary>
    ''' <param name="PosibleEditar">Si es posible editar la NC segun permisos, revisor, usuario conectado, ... etc</param>
    ''' <param name="PosibleReabrir">Si es posible reabrir la NC segun permisos, revisor, usuario conectado, ... etc</param>
    ''' <remarks>Llamada desde: Page_Load;  tiempo maximo:0,1</remarks>
    Private Sub MostrarSegunPosibilidadEdicionYPagRevisor(ByVal PosibleEditar As Boolean, ByVal PosibleReabrir As Boolean)
        If Not mbPagNCRevisor Then
            If PosibleEditar Then
                Select Case oNoConformidad.Estado
                    Case TipoEstadoNoConformidad.Guardada, TipoEstadoNoConformidad.EnCursoDeAprobacion
                        If oNoConformidad.Workflow > 0 Then
                            FSNPageHeader.VisibleBotonEmitir = False
                            FSNPageHeader.VisibleBotonMail = False
                            oInstancia.DevolverEtapaActual(Idioma, FSNUser.CodPersona)
                            FSNPageHeader.VisibleBotonEliminar = (Not oNoConformidad.Estado = TipoEstadoNoConformidad.EnCursoDeAprobacion And Not (oInstancia.RolActual = 0 Or oInstancia.Etapa = 0))
                            FSNPageHeader.VisibleBotonGuardar = Not (oInstancia.RolActual = 0 Or oInstancia.Etapa = 0)
                        Else
                            If oNoConformidad.PeticionarioCod <> FSNUser.CodPersona Then FSNPageHeader.VisibleBotonEmitir = False
                            FSNPageHeader.VisibleBotonEliminar = True
                            FSNPageHeader.VisibleBotonGuardar = True
                        End If
                        FSNPageHeader.VisibleBotonReabrir = False
                        FSNPageHeader.VisibleBotonCierreNegativo = False
                        FSNPageHeader.VisibleBotonCierrePositivo = False
                        lblFechaResolver.Visible = False
                        txtFecApliMejoras.Visible = False
                    Case TipoEstadoNoConformidad.Abierta
                        FSNPageHeader.VisibleBotonReabrir = False
                        FSNPageHeader.VisibleBotonEmitir = False
                        FSNPageHeader.VisibleBotonCierreNegativo = True
                        FSNPageHeader.VisibleBotonCierrePositivo = True
                        FSNPageHeader.VisibleBotonAnular = True
                        FSNPageHeader.OnClientClickAnular = "AnularNoConformidad();return false;"
                        FSNPageHeader.VisibleBotonEliminar = True
                        FSNPageHeader.VisibleBotonGuardar = True
                        lblFechaResolver.Visible = False
                        txtFecApliMejoras.Visible = False
                    Case TipoEstadoNoConformidad.CierrePosPendiente
                        FSNPageHeader.VisibleBotonReabrir = False
                        FSNPageHeader.VisibleBotonEmitir = False
                        FSNPageHeader.VisibleBotonCierreNegativo = True
                        FSNPageHeader.VisibleBotonCierrePositivo = True
                        FSNPageHeader.VisibleBotonAnular = False
                        FSNPageHeader.VisibleBotonEliminar = False
                        FSNPageHeader.VisibleBotonGuardar = False
                        lblFechaResolver.Visible = False
                        txtFecApliMejoras.Visible = False
                    Case Else
                        FSNPageHeader.VisibleBotonEmitir = False
                        FSNPageHeader.VisibleBotonCierreNegativo = False
                        FSNPageHeader.VisibleBotonCierrePositivo = False
                        FSNPageHeader.VisibleBotonGuardar = False
                        FSNPageHeader.VisibleBotonEliminar = True
                        If Not PosibleReabrir Then FSNPageHeader.VisibleBotonReabrir = False
                        lblUnicoUnidadQA.Visible = True
                        wddUnidadQA.Visible = False
                        If LbltxtRevisorBD.Text = "" Then
                            LblRevisorCorto.Visible = False
                            LbltxtRevisorBD.Visible = False
                        Else
                            LblRevisorCorto.Visible = True
                            LbltxtRevisorBD.Visible = True
                        End If
                        wddRevisor.Visible = False
                        lblFechaResolver.Visible = True
                        txtFecApliMejoras.Visible = oNoConformidad.Es_Noconformidad_Escalacion
                        fsentryNoConfFecDesde.Visible = False
                        fsentryFecApliMejoras.Visible = False
                End Select
            Else
                'Solo podr� visualizar los datos y eliminarla si es el peticionario
                FSNPageHeader.VisibleBotonGuardar = False
                FSNPageHeader.VisibleBotonCalcular = False
                FSNPageHeader.VisibleBotonReabrir = False

                FSNPageHeader.VisibleBotonMail = (Not oNoConformidad.Es_Noconformidad_Escalacion_Hist)
                FSNPageHeader.VisibleBotonImpExp = (Not oNoConformidad.Es_Noconformidad_Escalacion_Hist)

                If oNoConformidad.PeticionarioCod = FSNUser.CodPersona Then
                    FSNPageHeader.VisibleBotonEliminar = (Not oNoConformidad.Es_Noconformidad_Escalacion_Hist)
                    If oNoConformidad.Estado = TipoEstadoNoConformidad.Anulada Then
                        FSNPageHeader.VisibleBotonReabrir = PosibleReabrir
                    End If
                Else
                    ' el q entra no es el peticionario
                    FSNPageHeader.VisibleBotonEliminar = False
                End If

                FSNPageHeader.VisibleBotonEmitir = False
                FSNPageHeader.VisibleBotonGuardar = False
                FSNPageHeader.VisibleBotonCierreNegativo = False
                FSNPageHeader.VisibleBotonCierrePositivo = False
                lblFechaResolver.Visible = True
                txtFecApliMejoras.Visible = oNoConformidad.Es_Noconformidad_Escalacion
                fsentryNoConfFecDesde.Visible = False
                fsentryFecApliMejoras.Visible = False
            End If
        Else
            'Activamos los botones que pueden aparecer en la pantalla
            If Session("VolverdetalleNoconformidad") = "DetPuntNc" Then
                FSNPageHeader.VisibleBotonVolver = True
                FSNPageHeader.VisibleBotonImpExp = True
                FSNPageHeader.VisibleBotonCierrePositivo = False
                FSNPageHeader.VisibleBotonCierreNegativo = False
            Else
                FSNPageHeader.VisibleBotonVolver = True
                FSNPageHeader.VisibleBotonImpExp = True
                FSNPageHeader.VisibleBotonCierrePositivo = True
                FSNPageHeader.VisibleBotonCierreNegativo = True
            End If

            'Desactivar el resto
            FSNPageHeader.VisibleBotonAnular = False
            FSNPageHeader.VisibleBotonGuardar = False
            FSNPageHeader.VisibleBotonCalcular = False
            FSNPageHeader.VisibleBotonReabrir = False
            FSNPageHeader.VisibleBotonMail = False
            FSNPageHeader.VisibleBotonEliminar = False
            FSNPageHeader.VisibleBotonEmitir = False

            lblFechaResolver.Visible = False
            txtFecApliMejoras.Visible = False
            fsentryNoConfFecDesde.Visible = False
            fsentryFecApliMejoras.Visible = False
        End If
    End Sub
    Private Sub CargarNotificadosProveedor()
        Dim dsNotificadosProveedor As DataSet = oNoConformidad.Load_Notificados_Proveedor()
        Dim Item As Infragistics.Web.UI.ListControls.DropDownItem

        For i As Integer = 0 To dsNotificadosProveedor.Tables(0).Rows.Count - 1
            Item = New Infragistics.Web.UI.ListControls.DropDownItem
            Item.Value = dsNotificadosProveedor.Tables(0).Rows(i)("Id")
            Item.Text = dsNotificadosProveedor.Tables(0).Rows(i)("Contacto")
            Item.Selected = IIf(dsNotificadosProveedor.Tables(0).Rows(i)("Seleccionado") = 0, False, True)

            With wddNotificadosProveedor
                If Item.Selected Then
                    If .CurrentValue = "" Then
                        .CurrentValue = Item.Text
                    Else
                        .CurrentValue = .CurrentValue & "," & Item.Text
                    End If
                End If

                wddNotificadosProveedor.Items.Add(Item)
            End With
        Next
        If wddNotificadosProveedor.CurrentValue Is Nothing Then
            wddNotificadosProveedor.CurrentValue = ""
        End If
    End Sub
    Private Sub CargarNotificadosInternos(ByVal ListaNotificadosInternos As String, ByVal Inicio As Boolean)
        If Inicio Then
            Dim Item As Infragistics.Web.UI.ListControls.DropDownItem
            Dim dsNotificadosInternos As DataSet = oNoConformidad.Load_Notificados_Internos()
            For i As Integer = 0 To dsNotificadosInternos.Tables(0).Rows.Count - 1
                Item = New Infragistics.Web.UI.ListControls.DropDownItem
                Item.Value = dsNotificadosInternos.Tables(0).Rows(i)("Cod")
                Item.Text = dsNotificadosInternos.Tables(0).Rows(i)("Contacto")
                If oNoConformidad.Estado = TipoEstadoNoConformidad.Guardada Then
                    Item.Selected = True
                Else
                    Item.Selected = IIf(dsNotificadosInternos.Tables(0).Rows(i)("Notificar") = 0, False, True)
                End If

                With wddNotificadosInternos
                    If Item.Selected Then
                        If .CurrentValue = "" Then
                            .CurrentValue = Item.Text
                        Else
                            .CurrentValue = .CurrentValue & "," & Item.Text
                        End If
                    End If
                    wddNotificadosInternos.Items.Add(Item)
                End With
            Next
            If wddNotificadosInternos.SelectedItems.Count = 0 Then
                wddNotificadosInternos.CurrentValue = ""
            End If
        Else
            Dim NotificadosInternos() As String = Split(ListaNotificadosInternos, "###")
            Dim Item As Infragistics.Web.UI.ListControls.DropDownItem
            Dim ExisteItem As Boolean

            For i As Integer = 0 To NotificadosInternos.Length - 2
                Item = New Infragistics.Web.UI.ListControls.DropDownItem
                Item.Value = Split(NotificadosInternos(i), "#")(0)
                Item.Text = Split(NotificadosInternos(i), "#")(1) & IIf(Split(NotificadosInternos(i), "#")(2) <> "null", " (" & Split(NotificadosInternos(i), "#")(2) & ")", "")
                Item.Selected = True

                With wddNotificadosInternos
                    If .CurrentValue = "" Then
                        .CurrentValue = Item.Text
                    Else
                        .CurrentValue = .CurrentValue & "," & Item.Text
                    End If

                    ExisteItem = False
                    For Each ItemAntiguo As Infragistics.Web.UI.ListControls.DropDownItem In wddNotificadosInternos.Items
                        If Item.Value = ItemAntiguo.Value Then
                            ItemAntiguo.Selected = True
                            ExisteItem = True
                            Exit For
                        End If
                    Next
                    If Not ExisteItem Then
                        wddNotificadosInternos.Items.Add(Item)
                    End If
                End With
            Next
            If wddNotificadosInternos.SelectedItems.Count = 0 Then
                wddNotificadosInternos.CurrentValue = ""
            End If
        End If
    End Sub
    ''' <summary>
    ''' Carga el GridView gvRegistroMails con el historico de las notificaciones realizadas para esta no conformidad
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load;  tiempo maximo:0,1</remarks>
    Private Sub CargarRegistroMails()
        Dim oEmails As FSNServer.NoConformidad
        oEmails = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))

        With gvRegistroMails
            .DataSource = oEmails.Load_Registro_Emails_NoConformidad(oNoConformidad.Instancia)
            .DataBind()
        End With
    End Sub
    ''' <summary>
    ''' Inicializa la linea del grid gvRegistroMails
    ''' </summary>
    ''' <param name="sender">GRID</param>
    ''' <param name="e">evento</param>
    ''' <remarks>Llamada desde: CargarRegistroMails;  tiempo maximo:0,1</remarks>
    Private Sub gvRegistroMails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRegistroMails.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            With e.Row
                If Not IsDBNull(.DataItem("FECHA")) Then
                    .Cells(3).Text = CType(.DataItem("FECHA"), Date).ToString("d", FSNUser.DateFormat)
                End If

                If .DataItem("KO") Then
                    CType(.FindControl("imgOK"), Image).Visible = False
                Else
                    CType(.FindControl("imgKO"), Image).Visible = False
                End If

                e.Row.Attributes.Add("onmouseover", "this.style.fontWeight='bold';this.style.cursor='hand';")
                e.Row.Attributes.Add("onclick", "OpenEmailSender('" & .DataItem("ID") & "');")
                e.Row.Attributes.Add("onmouseout", "this.style.fontWeight='normal';")
            End With
        End If
    End Sub
#Region "Traido de noconformidadRevisor.aspx y adaptado"
    ''' <summary>
    ''' El revisor Acepta el cierre de la no conformidad
    ''' Por lo que la no conformidad se cierra y envia el mail al peticionario
    ''' </summary>
    ''' <param name="Sender">Origen del evento</param>
    ''' <param name="e">Los argumentos del evento</param> 
    ''' <remarks>Llamada desde:FSNPageHeader.CierrePositivo; Tiempo m�ximo: 0,1</remarks>
    Private Sub FSNPageHeader_CierrePositivo_Click(ByVal Sender As Object, ByVal E As System.EventArgs) Handles FSNPageHeader.CierrePositivo_Click
        Dim oNoConformidad As FSNServer.NoConformidad

        'Cargamos la no conformidad
        oNoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
        oNoConformidad.ID = Request("NoConformidad")
        oNoConformidad.Load(Idioma)

        'Cargamos la variable de pantalla q indica si estamos con un revisor y q la nc esta cerrada din revisar
        mbPagNCRevisor = False
        If (oNoConformidad.Revisor = FSNUser.Cod) AndAlso
        ((oNoConformidad.Estado = TipoEstadoNoConformidad.CierrePosSinRevisar) OrElse (oNoConformidad.Estado = TipoEstadoNoConformidad.CierreNegSinRevisar)) Then
            mbPagNCRevisor = True
        End If

        If Not mbPagNCRevisor Then
            Exit Sub
        Else
            Dim bPositivo As Boolean

            If oNoConformidad.Estado = TipoEstadoNoConformidad.CierrePosSinRevisar Then
                bPositivo = True
            Else
                bPositivo = False
            End If

            If (oNoConformidad.AccionEstado = TipoAccionSolicitud.RechazoEficazRevisor) Or
            (oNoConformidad.AccionEstado = TipoAccionSolicitud.RechazoNoEficazRevisor) Then
                oNoConformidad.CargarComentarioCierre()
            End If

            oNoConformidad.CierreNoConformidad(bPositivo, FSNUser.Email, VacioToNothing(oNoConformidad.ComentCierre), False, FSNUser.CodPersona)

            'No ha habido errores, la solicitud pasa a compras:
            If bPositivo = True Then
                Response.Redirect("cierreOK.aspx?CierrePos=1&NoConformidad=" & oNoConformidad.ID & "&Instancia=" & oNoConformidad.Instancia)
            Else
                Response.Redirect("cierreOK.aspx?CierrePos=0&NoConformidad=" & oNoConformidad.ID & "&Instancia=" & oNoConformidad.Instancia)
            End If
        End If
    End Sub
#End Region
End Class