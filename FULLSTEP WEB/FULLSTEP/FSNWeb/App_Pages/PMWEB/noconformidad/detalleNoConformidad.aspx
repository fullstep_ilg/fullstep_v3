﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="detalleNoConformidad.aspx.vb" Inherits="Fullstep.FSNWeb.detalleNoConformidad" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Register TagPrefix="fsde" Namespace="Fullstep.DataEntry" Assembly="DataEntry" %>
<%@ Register Assembly="Infragistics.WebUI.UltraWebTab.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<%@ Register Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebNavigator" TagPrefix="ignav" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
</head>
<script type="text/javascript">
    CreateXmlHttp();
    function ErrorValidacion(contenido) {
        alert(arrTextosML[14].replace("$$$", contenido));
        HabilitarBotones(true);
    }
    /*''' <summary>
    ''' Crear el objeto para llamar con ajax a ComprobarVersionQa y EliminarVersionProveedor
    ''' </summary>
    ''' <remarks>Llamada desde: javascript ; Tiempo máximo: 0</remarks>*/
    function CreateXmlHttp() {
        // Probamos con IE
        try {
            // Funcionará para JavaScript 5.0
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (oc) {
                xmlHttp = null;
            }
        }
        // Si no se trataba de un IE, probamos con esto
        if (!xmlHttp && typeof XMLHttpRequest != "undefined") xmlHttp = new XMLHttpRequest();

        return xmlHttp;
    }
    wProgreso = null;

    /*''' <summary>
    ''' Redimensionar los desgloses readonly de la pantalla. Se da una altura en función del numero
    ''' de lineas, concretamente 30 por fila (mas q de sobra), aparte de 20 pixel para q tenga algo
    ''' de altura aunque no haya lineas. 
    ''' </summary>   
    ''' <remarks>Llamada desde: onResize; Tiempo máximo: 0</remarks>*/
    function resize() {
        for (i = 0; i < arrDesgloses.length; i++) {
            sDiv = arrDesgloses[i].replace("tblDesglose", "divDesglose")
            if (document.getElementById(sDiv)) document.getElementById(sDiv).style.width = parseFloat(document.body.offsetWidth) - 80 + 'px';
        }
    }
    /*''' <summary>
    ''' Iniciar la pagina. Se marca q la carga del combo de veriones es la inicial.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
    function init() {
        resize();
        window.focus();
        OcultarEspera();
    }

    function localEval(s) {
        eval(s)
    }

    /*''' <summary>
    ''' Elimina una versión de la no conformidad
    ''' </summary>
    ''' <remarks>Llamada desde: cmdEliminar; Tiempo máximo: 0,2</remarks>*/
    function EliminarVersion() {
        if (document.getElementById("lnkBotonEliminar"))
            if (document.getElementById("lnkBotonEliminar").disabled)
                return false

        if (confirm(arrTextosML[0]) == true) {
            CreateXmlHttp();
            if (xmlHttp) {
                var params = "Instancia=" + document.forms["frmDetalle"].elements["Instancia"].value + "&Version=" + document.forms["frmDetalle"].elements["Version"].value;

                xmlHttp.open("POST", "../_common/controlarVersionQA.aspx", false);
                xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                xmlHttp.send(params);

                resultadoProcesarAcciones(false, 2);
            }
            else {
                alert(arrTextosML[5]);
            }
        }
    }
    /*''' <summary>
    ''' Realiza el envio o guardado de una no conformidad
    ''' </summary>
    ''' <param name="bEnviar">envias(1) o guardas (0) o cierras positivo (2) o cierras negativo (3)</param>       
    ''' <param name="bMapper">Si debemos hacer validadciones de mapper o no</param>
    ''' <returns>False, si falta algún dato necesario.</returns>
    ''' <remarks>Llamada desde:cmdEmitir	cmdGuardar; Tiempo máximo: 0</remarks>*/
    function Guardar(bEnviar, bMapper, guardaConFlujo, idAccion, bloque, comp_obl, guarda, bloq, rechazo) {
        if (bloq == 2) {
            alert(arrTextosML[3])
            return false
        }
        if (!idAccion) {
            if (document.getElementById("lnkBotonEmitir"))
                if (document.getElementById("lnkBotonEmitir").disabled)
                    return false

            if (document.getElementById("lnkBotonGuardar"))
                if (document.getElementById("lnkBotonGuardar").disabled)
                    return false
        }

        bMensajePorMostrar = document.getElementById("bMensajePorMostrar")
        if (bMensajePorMostrar)
            if (bMensajePorMostrar.value == "1") {
                bMensajePorMostrar.value = "0";
                return false;
            }
        //Comprueba que se hayan rellenado todos los campos obligatorios        
        bEstado = document.getElementById("EstadoNoConf")
        if (bEstado)            
            if (((bEstado.value != 0) || (bEstado.value == 0 && bEnviar == 1) || idAccion) && comp_obl) {                               
                //Estado 0 es Guardado. Comprobar solo en caso de envio.
                //Estado diferente de 0 es Emitido/Cerrado... Comprobar siempre al guardar.				
                var respOblig = comprobarObligatorios()
                switch (respOblig) {
                    case "":  //no falta ningun campo obligatorio
                        break;
                    case "filas0": //no se han introducido filas en un desglose obligatorio
                        alert(arrTextosML[1])
                        return false
                        break;
                    default: //falta algun campo obligatorio
                        alert(arrTextosML[1] + '\n' + respOblig)
                        return false
                        break;
                }                
            }          

        if (bEnviar == 1 || idAccion) {  //si va a emitir comprueba que se haya introducido la fecha limite de resolución:		 
            oEntry = fsGeneralEntry_getById("fsentryNoConfFecDesde")
            if (oEntry.getValue() == null) {
                alert(arrTextosML[2])
                return false
            }
        }

        //Comprobar que las fechas de inicio y fin de las acciones no sobrepasan el limite correspondiente
        if (validar_fechas_acciones() == false) {
            alert(arrTextosML[3]);
            return false;
        }

        //Comprueba que se haya introducido al menos un contacto de proveedor
        var wDropDownNotificadosProveedor = $find("wddNotificadosProveedor")
        if (wDropDownNotificadosProveedor.get_selectedItems().length == 0) {
            alert(arrTextosML[13])
            return false
        }

        //Comprueba que se haya introducido un unidad qa
        if (document.getElementById("ComboQA").value == 1) {
            var wDropDownUnidadQA = $find("<%=wddUnidadQA.ClientID%>")
            if (wDropDownUnidadQA.get_selectedItems().length == 0) {
                alert(arrTextosML[4])
                return false
            } else {
                if (wDropDownUnidadQA.get_selectedItems()[0].get_value() == '') {
                    alert(arrTextosML[4])
                    return false
                }
            }
        }

        if (comprobarTextoLargo() == false)
            return false

        if ((bEnviar == 2) || (bEnviar == 3)) {
            if (document.getElementById("lnkBotonCierrePositivo"))
                if (document.getElementById("lnkBotonCierrePositivo").disabled)
                    return false

            if (document.getElementById("lnkBotonCierreNegativo"))
                if (document.getElementById("lnkBotonCierreNegativo").disabled)
                    return false
        }
        HabilitarBotones(false);

        if (wProgreso == null) {
            wProgreso = true;
            MostrarEspera();
        }

        if (bEstado) {
            if (bEstado.value != 0 && bEstado.value != 9) {//Emitida
                CreateXmlHttp();
                if (xmlHttp) {
                    var params = "Instancia=" + document.forms["frmDetalle"].elements["Instancia"].value + "&Version=" + document.forms["frmDetalle"].elements["Version"].value;

                    xmlHttp.open("POST", "../_common/controlarVersionQA.aspx", false);
                    xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                    xmlHttp.send(params);

                    resultadoProcesarAcciones(bEnviar, 1, bMapper);

                } else {
                    alert(arrTextosML[5]);

                    HabilitarBotones(true)
                    wProgreso = null;
                }
            } else HazSubmit(bEnviar, bMapper, guardaConFlujo, idAccion, bloque, comp_obl, guarda, bloq, rechazo);//Guardada 
        }
        return false;
    }
    /*''' <summary>
    ''' Tras q se ejecute sincronamente controlarVersionQA.aspx controlamos sus resultados y obramos en 
    ''' consecuencia.
    ''' </summary>
    ''' <param name="bParam">envias(1) o guardas (0) o cierras positivo (2) o cierras negativo (3)</param>   
    ''' <param name="Quien">1 guardando 2 eliminando 3 cerrando 4 anular 5 rechazando cierre</param>        
    ''' <remarks>Llamada desde: EliminarVersion     Guardar     Cerrar      Rechazar_onclick; Tiempo máximo: 0</remarks>*/
    function resultadoProcesarAcciones(bParam, Quien, bMapper) {
        var retorno;
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            retorno = xmlHttp.responseText;

            if (retorno == 1) { //en proceso
                alert(arrTextosML[7])
                HabilitarBotones(true)
                wProgreso = null;
                return
            } else
                if (retorno.search("Version") > -1) { //Si no es misma version, y la última versión es de tipo 3, quiere decir 
                    //que el proveedor tiene una versión guardada sin enviar.    
                    if ((bParam == 2) || (bParam == 3)) //antes quien 3
                        Mensaje = 12
                    else
                        if (Quien == 1)
                            Mensaje = 8
                        else
                            if (Quien == 2)
                                Mensaje = 11
                            else
                                if (Quien == 4)
                                    Mensaje = 15
                                else
                                    if (Quien == 5)
                                        Mensaje = 12
                    //Se usa el mismo texto para cerrar y rechazar cierre	        
                    if (confirm(arrTextosML[Mensaje]) == true) {//En este caso en vez de un alert mostraremos un confirm:		                
                        //Si el usuario confirma la acción, antes de llamar a guardarInstancia procederemos a eliminar la versión de tipo 3
                        retorno = retorno.substring(retorno.search("n") + 1)

                        var params = "NoConformidad=" + document.forms["frmDetalle"].elements["NoConformidad"].value + "&Instancia=" + document.forms["frmDetalle"].elements["Instancia"].value + "&Version=" + retorno;

                        xmlHttp.open("POST", "../_common/EliminarVersionProveedor.aspx", false);
                        xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                        xmlHttp.send(params);

                        document.forms["frmDetalle"].elements["Version"].value = retorno - 1

                        var params = "Instancia=" + document.forms["frmDetalle"].elements["Instancia"].value + "&Version=" + document.forms["frmDetalle"].elements["Version"].value;

                        xmlHttp.open("POST", "../_common/controlarVersionQA.aspx", false);
                        xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                        xmlHttp.send(params);

                        resultadoProcesarAcciones(bParam, Quien, bMapper)

                        return
                    } else {
                        HabilitarBotones(true)
                        wProgreso = null;
                    }
                    return;
                } else
                    if (retorno == 3) {//Si no es misma versión en función del tipo mostraremos un  
                        //alert y el usuario no podrá guardar los cambios. Tipo Usu Qa
                        alert(arrTextosML[9])
                        HabilitarBotones(true)
                        wProgreso = null;
                        return
                    } else
                        if (retorno == 4) {//Si no es misma versión en función del tipo mostraremos un  
                            //alert y el usuario no podrá guardar los cambios. Tipo Proveedor
                            alert(arrTextosML[10])
                            HabilitarBotones(true)
                            wProgreso = null;
                            return;
                        }
        }
        switch (Quien) {
            case 1:
                HazSubmit(bParam, bMapper);
                break;
            case 2:                
                window.open("eliminarNoConformidad.aspx?NoConformidad=" + document.forms["frmDetalle"].elements["NoConformidad"].value +
                                                        "&Instancia=" + document.forms["frmDetalle"].elements["Instancia"].value +
                                                        "&Version=" + document.forms["frmDetalle"].elements["Version"].value +
                                                        "&EsNoConfEscalacion=" + (esNoConf_Escalacion ? 1 : 0) +
                                                        "&Estado=" + document.forms["frmDetalle"].elements["EstadoNoConf"].value +
                                                        "&Workflow=" + document.forms["frmDetalle"].elements["Workflow"].value, "iframeWSServer");
                break;
            case 4:
                window.open("anularNoConformidad.aspx?NoConformidad=" + document.forms["frmDetalle"].elements["NoConformidad"].value +
                                                            "&Instancia=" + document.forms["frmDetalle"].elements["Instancia"].value +
                                                            "&EsNoConfEscalacion=" + (esNoConf_Escalacion ? 1 : 0) +
                                                            "&Volver=" + "<%=request("volver")%>", "_self");
                    break;
                case 5:
                    window.open("cierreRevisorOK.aspx?NoConformidad=" + document.forms["frmDetalle"].elements["NoConformidad"].value +
                                                        "&Instancia=" + document.getElementById("Instancia").value +
                                                        "&Volver=" + "<%=request("volver")%>", "_self")
                break;
            default:
                break;
        }
    }

    /*''' <summary>
	''' Si se puede continuar guardando o cerrando. Esta función llama a guardarinstancia y se graba/cierra
	''' </summary>
	''' <param name="bEnviar">envias(1) o guardas (0) o cierras positivo (2) o cierras negativo (3)</param>  
	''' <param name="bMapper">Si debemos hacer validaciones de mapper o no</param>  
	''' <remarks>Llamada desde: resultadoProcesarAcciones; Tiempo máximo: 0,5</remarks>*/
    function HazSubmit(bEnviar, bMapper, guardaConFlujo, idAccion, bloque, comp_obl, guarda, bloq, rechazo) {
        
        if (idAccion) {
            EjecutarAccion(idAccion, bloque, comp_obl, guarda, bloq, rechazo);
        } else {
            if (bMapper == true) MontarFormularioSubmit(false, false, true);
            else MontarFormularioSubmit();
        }

        if (bEnviar == 1)
            document.forms["frmSubmit"].elements["GEN_Enviar"].value = 1
        else //0,2,3 son guardados
            document.forms["frmSubmit"].elements["GEN_Enviar"].value = 0
        if (!idAccion) {
            if (guardaConFlujo == true) {
                document.forms["frmSubmit"].elements["GEN_Accion"].value = "guardarsolicitud";
                document.forms["frmSubmit"].elements["GEN_Guardar"].value = 1;
                document.forms["frmSubmit"].elements["GEN_Bloque"].value = bloque;
            }
            document.forms["frmSubmit"].elements["PantallaMaper"].value = bMapper;
        }

        document.forms["frmSubmit"].elements["GEN_NoConfProve"].value = document.getElementById("Proveedor").value;
        document.forms["frmSubmit"].elements["GEN_Notificar"].value = 0;
        if ($get("wddProveERP")) {
            if ($get("wddProveERP").style.display != "none") {
                var owddProveERP = $find("wddProveERP");
                if (owddProveERP.get_selectedItems().length > 0) {
                    document.forms["frmSubmit"].elements["GEN_NoConfProveERP"].value = owddProveERP.get_selectedItems()[0].get_value();
                    //al ppio de la denominación va el código
                    document.forms["frmSubmit"].elements["GEN_NoConfProveERPDen"].value = owddProveERP.get_selectedItems()[0].get_text().replace(owddProveERP.get_selectedItems()[0].get_value() + " - ", "");
                }
            }
        }
        //Contactos/Notificados del proveedor
        //Notificados del proveedor
        var wDropDownNotificadosProveedor = $find("wddNotificadosProveedor")
        var cadenaIdsContactoProveedor = ""
        for (j = 0; j < wDropDownNotificadosProveedor.get_selectedItems().length; j++) {
            cadenaIdsContactoProveedor = cadenaIdsContactoProveedor + wDropDownNotificadosProveedor.get_selectedItems()[j].get_value() + "#";
        }
        document.forms["frmSubmit"].elements["GEN_NoConfContactosProveedor"].value = cadenaIdsContactoProveedor

        //Notificados internos, del departamento o unidad de negocio
        var wDropDownNotificadosInternos = $find("wddNotificadosInternos")
        var cadenaIdsNotificadosInternos = ""
        for (j = 0; j < wDropDownNotificadosInternos.get_selectedItems().length; j++) {
            cadenaIdsNotificadosInternos = cadenaIdsNotificadosInternos + wDropDownNotificadosInternos.get_selectedItems()[j].get_value() + "#";
        }
        document.forms["frmSubmit"].elements["GEN_NoConfContactosInternos"].value = cadenaIdsNotificadosInternos

        if (idEscalacionProve) document.forms["frmSubmit"].elements["GEN_IdEscalacionProve"].value = idEscalacionProve;
        document.forms["frmSubmit"].elements["GEN_NivelEscalacion"].value = nivelEscalacion;
        document.forms["frmSubmit"].elements["GEN_EsNoconfEscalacion"].value = esNoconfEscalacion;
        if (bEnviar == 2) {
            document.forms["frmSubmit"].elements["GEN_Accion"].value = "cerrarpositivonoconformidad";
            document.forms["frmSubmit"].elements["GEN_Volver"].value = "<%=Request("Volver")%>";
        }
        else
            if (bEnviar == 3) {
                document.forms["frmSubmit"].elements["GEN_Accion"].value = "cerrarnegativonoconformidad";
                document.forms["frmSubmit"].elements["GEN_Volver"].value = "<%=Request("Volver")%>";
            }
            else {
                if (!idAccion && guardaConFlujo != true) {
                    document.forms["frmSubmit"].elements["GEN_Accion"].value = "guardarnoconformidad";
                }
            }

        //Figura del revisor
        oEntry = document.getElementById("AccesoRevisor")
        if (oEntry.value == 1) {
            oEntry = document.getElementById("codRevisor")

            if ((oEntry.value == '') || (oEntry.value == null)) {
                //Se ve el combo del revisor lo usamos como referencia
                var wDropDownRevisor = $find("<%=wddRevisor.ClientID%>")

                if (wDropDownRevisor == null) {
                    document.forms["frmSubmit"].elements["GEN_NoConfRevisor"].value = document.getElementById("CodigoRevisorAnterior").value
                    document.forms["frmSubmit"].elements["GEN_CambioRevisor"].value = 0
                }
                else {
                    document.forms["frmSubmit"].elements["GEN_NoConfRevisor"].value = wDropDownRevisor.get_selectedItems()[0].get_value()

                    if (document.getElementById("CodigoRevisorAnterior").value == wDropDownRevisor.get_selectedItems()[0].get_value()) {
                        document.forms["frmSubmit"].elements["GEN_CambioRevisor"].value = 0
                    } else {
                        document.forms["frmSubmit"].elements["GEN_CambioRevisor"].value = 1
                    }

                    document.getElementById("CodigoRevisorAnterior").value = wDropDownRevisor.get_selectedItems()[0].get_value();
                }
            }
            else {
                document.forms["frmSubmit"].elements["GEN_NoConfRevisor"].value = oEntry.value
            }
        }
        else {
            document.forms["frmSubmit"].elements["GEN_NoConfRevisor"].value = null
        }
        oEntry = fsGeneralEntry_getById("fsentryNoConfFecDesde")
        var d = oEntry.getValue()
        if (d != null) {
            document.forms["frmSubmit"].elements["GEN_DespubDia"].value = d.getDate()
            document.forms["frmSubmit"].elements["GEN_DespubMes"].value = d.getMonth() + 1
            document.forms["frmSubmit"].elements["GEN_DespubAnyo"].value = d.getFullYear()
        }
        oEntry = fsGeneralEntry_getById("fsentryFecApliMejoras");
        var FecApliPlan = oEntry.getValue();
        if (FecApliPlan != null) {
            document.forms["frmSubmit"].elements["GEN_FecApliPlanDia"].value = FecApliPlan.getDate();
            document.forms["frmSubmit"].elements["GEN_FecApliPlanMes"].value = FecApliPlan.getMonth() + 1;
            document.forms["frmSubmit"].elements["GEN_FecApliPlanAnyo"].value = FecApliPlan.getFullYear();
        }
        if (document.getElementById("ComboQA").value == 1) {
            var wDropDownUnidadQA = $find("<%=wddUnidadQA.ClientID%>")
            document.forms["frmSubmit"].elements["GEN_NoConfUnidadQa"].value = wDropDownUnidadQA.get_selectedItems()[0].get_value()
        } else {
            document.forms["frmSubmit"].elements["GEN_NoConfUnidadQa"].value = document.getElementById("UnicoUnidadQA").value
        }

        if (document.getElementById("txtComent") != null) {
            document.forms["frmSubmit"].elements["GEN_NoConfComentAlta"].value = document.getElementById("txtComent").value
        }
        oFrm = MontarFormularioCalculados()
        sInner = oFrm.innerHTML
        oFrm.innerHTML = ""
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner)
        document.forms["frmSubmit"].submit()

        return
    }
    function EjecutarAccion(idAccion, bloque, comp_obl, guarda, rechazo) {
    	if (rechazo == false) {
    		var respOblig = comprobarParticipantes();
    		if (respOblig !== '') {
    			alert(arrTextosML[1] + '\n' + respOblig);
                return false;
            }
        }
        var frmDetalleElements = document.forms["frmDetalle"].elements;
        var HayMapper = false
        if (frmDetalleElements["PantallaMaper"].value == "True")
            HayMapper = true;

        MontarFormularioSubmit(guarda, true, HayMapper, false);

        var frmSubmitElements = document.forms["frmSubmit"].elements;
        frmSubmitElements["GEN_AccionRol"].value = idAccion;
        frmSubmitElements["GEN_Bloque"].value = bloque;
        frmSubmitElements["GEN_Rol"].value = oRol_Id;
        frmSubmitElements["PantallaMaper"].value = HayMapper;
        frmSubmitElements["DeDonde"].value = "Guardar";

        frmSubmitElements = null;
        frmDetalleElements = null;
        HayMapper = null;
    }

    /*Descripcion:=Llama a la pagina para exportacion de datos
      Llamada desde:Option de menu "Impr./Exp."
      Tiempo ejecucion:0seg.*/
    function cmdImpExp_onclick() {
        if (document.getElementById("lnkBotonImpExp"))
            if (document.getElementById("lnkBotonImpExp").disabled)
                return false

        window.open('../seguimiento/impexp_sel.aspx?Instancia=' + document.getElementById("Instancia").value + '&NoConformidad=' + document.forms["frmDetalle"].elements["NoConformidad"].value + '&Version=' + document.forms["frmDetalle"].elements["Version"].value + '&TipoImpExp=3', '_new', 'fullscreen=no,height=115,width=315,location=no,menubar=no,resizable=no,scrollbars=no,status=yes,titlebar=yes,toolbar=no,left=200,top=200');
        return false
    }

    /*''' <summary>
	''' El revisor ha rechazado la noconformidad
	''' se redirige a un pagina donde se le envia al peticionario un mail
	'''	donde se le explica los motivos del rechazo
	''' </summary>
	''' <remarks>Llamada desde: FSNPageHeader.OnClientClickCierreNegativo; Tiempo máximo: 0</remarks>*/
    function Rechazar_onclick() {
        if (document.getElementById("lnkBotonCierreNegativo"))
            if (document.getElementById("lnkBotonCierreNegativo").disabled)
                return false

        if (comprobarTextoLargo() == false)
            return false

        var params = "Instancia=" + document.forms["frmDetalle"].elements["Instancia"].value + "&Version=" + document.forms["frmDetalle"].elements["Version"].value;

        xmlHttp.open("POST", "../_common/controlarVersionQA.aspx", false);
        xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        xmlHttp.send(params);

        resultadoProcesarAcciones(true, 5)
        return false
    }

    /*''' <summary>
    ''' Vuelve a la pagina anterior. 
    ''' En caso de abrir en modo consulta desde PMWeb2008\script\puntuacion\DetallePuntuacionNc no vuelve se cierra
    ''' </summary>
    ''' <remarks>Llamada desde: Cuando se pincha al enlace ("Volver"); Tiempo máximo:0,1</remarks>*/
    function HistoryHaciaAtras() {
        //Evitar los enter q hacen q el 1er botón de FSPMPageHeader se ejecute
        if (esIntro == true) {
            esIntro = false;
            return false;
        }

        if (document.getElementById("lnkBotonVolver"))
            if (document.getElementById("lnkBotonVolver").disabled)
                return false

        if ("<%=Session("VolverdetalleNoconformidad")%>" == "")
            window.history.go(-1)
        else
            if ("<%=Session("VolverdetalleNoconformidad")%>" == "DetPuntNc")
                window.close();
            else
                window.open("<%=Session("VolverdetalleNoconformidad")%>", "_top")
        }

        function CalcularCamposCalculados() {
            if (document.getElementById("lnkBotonCalcular"))
                if (document.getElementById("lnkBotonCalcular").disabled)
                    return false

            oFrm = MontarFormularioCalculados()
            oFrm.submit()
            return false
        }
        /*''' <summary>
		''' Para reabrir una no conformidad
		''' </summary>
		''' <remarks>Llamada desde: FSNPageHeader.OnClientClickReabrir; Tiempo máximo: 0,2</remarks>*/
        function Reabrir() {
            if (document.getElementById("lnkBotonReabrir"))
                if (document.getElementById("lnkBotonReabrir").disabled)
                    return false

            //Contactos/Notificados del proveedor
            //Notificados del proveedor
            var wDropDownNotificadosProveedor = $find("wddNotificadosProveedor")
            var cadenaIdsContactoProveedor = ""
            for (j = 0; j < wDropDownNotificadosProveedor.get_selectedItems().length; j++) {
                cadenaIdsContactoProveedor = cadenaIdsContactoProveedor + wDropDownNotificadosProveedor.get_selectedItems()[j].get_value() + ";";
            }
            //Notificados internos, del departamento o unidad de negocio
            var wDropDownNotificadosInternos = $find("wddNotificadosInternos")
            var cadenaIdsNotificadosInternos = ""
            for (j = 0; j < wDropDownNotificadosInternos.get_selectedItems().length; j++) {
                cadenaIdsNotificadosInternos = cadenaIdsNotificadosInternos + wDropDownNotificadosInternos.get_selectedItems()[j].get_value() + ";";
            }
            window.open("ReabrirNoconformidad.aspx?NoConformidad=" + document.forms["frmDetalle"].elements["NoConformidad"].value + "&NotifProv=" + cadenaIdsContactoProveedor + "&NotifInt=" + cadenaIdsNotificadosInternos, "_self")

        }
        /*''' Revisado por: Jbg. Fecha: 26/10/2011
		''' <summary>
		''' Abrir una pantalla para enviar emails a una serie de proveedores
		''' </summary>
		''' <remarks>Llamada desde: FSNPageHeader.OnClientClickMail ; Tiempo máximo: 0</remarks>*/
        function EnviarMail() {
            if (document.getElementById("lnkBotonMail"))
                if (document.getElementById("lnkBotonMail").disabled)
                    return false

            var newWindow = window.open("../_common/MailProveedores.aspx?Instancia=" + document.forms["frmDetalle"].elements["Instancia"].value + "&Origen=3&Proveedores=" + document.forms["frmDetalle"].elements["Email"].value, "_blank", "width=710,height=360,status=yes,resizable=no,top=180,left=200");
            newWindow.focus();
        }

        /*''' <summary>
          ''' Tras un guardado no se hace ni submit ni se va a otra pantalla. Eso implica q haya datos q no estan 
          '''	actualizados en pantalla pero si en bbdd. Esta función habilita los botones y oculta la ventana de espera.
          '''	Despues actualiza en pantalla los calculados y los botones de cierre los muestra si ya todo esta finalizado 
          '''	y revisado. Por ultimo actualiza el combo de versiones.
          ''' </summary>
          ''' <param name="sCadena">Persona de nueva versión para el combo de versiones</param>
          ''' <param name="ListaCalc">Lista de pares que contiene los Id de los calculados y su valor ya calculado</param> 		
          ''' <param name="FinalRevisada">Numero de acciones en estado distinto a finalizado y revisado</param>
          ''' <param name="version">nueva versión para el combo de versiones</param>		
          ''' <param name="TiempoNoConf">Año, mes, dia, hora y minuto de la nueva version separados por #. Y diferencia horaria con el servidor.</param>		
          ''' <remarks>Llamada desde: GuardarInstancia.aspx; Tiempo máximo:0,1</remarks>*/
        function activarVentana(sCadena, ListaCalc, FinalRevisada, version, TiempoNoConf) {
            HabilitarBotones(true);
            wProgreso = null;

            document.getElementById("Notificar").value = 0

            document.getElementById("Version").value = version

            var ParListaCalc = "";
            var ListaCalcId = "";
            var ListaCalcVal = "";

            while (ListaCalc != "") {
                ParListaCalc = ListaCalc.substring(0, ListaCalc.indexOf("#"))

                ListaCalcId = ParListaCalc.substring(0, ParListaCalc.indexOf("@"))
                ListaCalcVal = ParListaCalc.substring(ParListaCalc.indexOf("@") + 1)

                if (document.getElementById(ListaCalcId)) {
                    document.getElementById(ListaCalcId).value = ListaCalcVal;
                }

                if (ListaCalc.indexOf("#"))
                    ListaCalc = ListaCalc.substring(ListaCalc.indexOf("#") + 1)
            }

            bEstado = document.getElementById("EstadoNoConf")
            if (bEstado) {
                if (bEstado.value != 0) {//Emitida
                    addItem(sCadena, version, TiempoNoConf);

                    var combo = $find('<%=wddVersion.ClientID %>');
                    combo.set_visible(true);
                    document.getElementById("lblVersion").style.visibility = 'visible';
                }
            }
        }

        /*''' <summary>
		''' Tras un guardado no se hace ni submit ni se va a otra pantalla. Eso implica q haya datos q no estan 
		'''	actualizados en pantalla pero si en bbdd. Esta funciÃ¯Â¿Â½n habilita los botones y oculta la ventana de espera.
		'''	Por ultimo actualiza el combo de versiones.
		''' </summary>
		''' <param name="sCadena">Persona de nueva versiÃ³n para el combo de versiones</param>
		''' <param name="version">nueva versiÃ³n para el combo de versiones</param>	
		''' <param name="TiempoNoConf">AÃ±o, mes, dia, hora, minuto y segundos de la nueva version separados por #. Y diferencia horaria con el servidor.</param>				    	
		''' <remarks>Llamada desde: activarVentana; Tiempo mÃ¡ximo:0,1</remarks>*/
        function addItem(sCadena, version, TiempoNoConf) {
            var combo = $find('<%=wddVersion.ClientID %>');
            var item = combo.get_items().createItem();

            var sText;
            var Horario = TiempoNoConf.split("#");
            var FechaLocal;
            FechaLocal = new Date();
            otz = FechaLocal.getTimezoneOffset();
            diferencia = parseInt(Horario[6]) + otz;

            FechaLocal = new Date(Horario[0], Horario[1] - 1, Horario[2], Horario[3], Horario[4] - diferencia, Horario[5]);
            valorTempVersion = myDatetoStr(FechaLocal, vDatefmt, true);

            sText = valorTempVersion + ' ' + sCadena;

            item.set_text(sText);

            var sValue = document.forms["frmDetalle"].elements["Instancia"].value + "##" + version;

            item.set_value(sValue);

            bEstasAñadiendo = 1;

            combo.get_items().add(item)
            combo.set_activeItem(item, true)
        }

        function HabilitarBotones(opcion) {
            var deshabilitar = !opcion

            if (document.getElementById("lnkBotonCalcular")) document.getElementById("lnkBotonCalcular").disabled = deshabilitar;
            if (document.getElementById("lnkBotonMail")) document.getElementById("lnkBotonMail").disabled = deshabilitar;
            if (document.getElementById("lnkBotonImpExp")) document.getElementById("lnkBotonImpExp").disabled = deshabilitar;
            if (document.getElementById("lnkBotonEmitir")) document.getElementById("lnkBotonEmitir").disabled = deshabilitar;
            if (document.getElementById("lnkBotonGuardar")) document.getElementById("lnkBotonGuardar").disabled = deshabilitar;
            if (document.getElementById("lnkBotonEliminar")) document.getElementById("lnkBotonEliminar").disabled = deshabilitar;
            if (document.getElementById("lnkBotonCierrePositivo")) document.getElementById("lnkBotonCierrePositivo").disabled = deshabilitar;
            if (document.getElementById("lnkBotonCierreNegativo")) document.getElementById("lnkBotonCierreNegativo").disabled = deshabilitar;
            if (document.getElementById("lnkBotonReabrir")) document.getElementById("lnkBotonReabrir").disabled = deshabilitar;
            if (document.getElementById("lnkBotonVolver")) document.getElementById("lnkBotonVolver").disabled = deshabilitar;
            if (document.getElementById("lnkBotonAnular")) document.getElementById("lnkBotonAnular").disabled = deshabilitar;

            if (arrObligatorios.length > 0)
                if (document.getElementById("lblCamposObligatorios"))
                    document.getElementById("lblCamposObligatorios").style.display = ""

            OcultarEspera()
        }

        //Estaba habilitando los botones antes de ocultar el progreso. Parecia q te dejaba dar a varios botones mientras estaba en progreso.
        function DarTiempoAOcultarProgreso() {
            $("[id*='lnkBoton']").removeAttr('disabled');
        }

        /*
		Descripcion: Oculta el div de espera y muestra los tabs de grupos
		Llamada desde= onunload() // HabilitarBotones()     //Init()
		Tiempo ejecucion:=0,1seg. 
		*/
        function OcultarEspera() {
            wProgreso = null;

            document.getElementById("divProgreso").style.display = 'none';
            document.getElementById("divForm2").style.display = 'inline';

            setTimeout('DarTiempoAOcultarProgreso()', 250);

            i = 0;
            bSalir = false;
            while (bSalir == false) {
                if (document.getElementById("uwtGrupos_div" + i)) {
                    document.getElementById("uwtGrupos_div" + i).style.visibility = 'visible';
                    i = i + 1;
                }
                else {
                    bSalir = true;
                }
            }

        }
        /*
		Descripcion: Muestra el div de espera y oculta los tabs de grupos
		Llamada desde= Guardar
		Tiempo ejecucion:=0,1seg. 
		*/
        function MostrarEspera() {
            wProgreso = true;

            $("[id*='lnkBoton']").attr('disabled', 'disabled');

            document.getElementById("lblCamposObligatorios").style.display = "none"
            document.getElementById("divForm2").style.display = 'none';

            i = 0;
            bSalir = false;
            while (bSalir == false) {
                if (document.getElementById("uwtGrupos_div" + i)) {
                    document.getElementById("uwtGrupos_div" + i).style.visibility = 'hidden';
                    i = i + 1;
                }
                else {
                    bSalir = true;
                }
            }
            document.getElementById("divProgreso").style.display = 'inline';
        }
</script>
<script type="text/javascript">
    /*
	''' <summary>
	''' Esta funcion convierte un objeto javascript Date en una cadena con una fecha
	''' en el formato especificado en vdatefmt
	''' </summary>
	''' <param name="fec">Fecha a convertir</param>
	''' <param name="vdatefmt">formato del usuario</param>        
	''' <param name="bConHora">Si la fecha tiene tb las horas/param>        
	''' <returns>Devuelve cadena con la fecha en el formato del usuario</returns>
	''' <remarks>Llamada desde=CargarGrid; Tiempo máximo=0</remarks>
	*/
    function myDatetoStr(fec, vdatefmt, bConHora) {
        var vDia, vMes, str;

        if (fec == null) return ("");

        re = /dd/i;
        vDia = "00".substr(0, 2 - fec.getDate().toString().length) + fec.getDate().toString();
        str = vdatefmt.replace(re, vDia);

        re = /mm/i;

        vMes = "00".substr(0, 2 - (fec.getMonth() + 1).toString().length) + (fec.getMonth() + 1).toString();
        str = str.replace(re, vMes);

        re = /yyyy/i;

        str = str.replace(re, fec.getFullYear());
        if (bConHora) str += (" " + "00".substr(0, 2 - (fec.getHours()).toString().length) + fec.getHours() + ":" + "00".substr(0, 2 - (fec.getMinutes()).toString().length) + fec.getMinutes());
        return (str);
    }

    /*''' <summary>
	''' Evento que salta tras elegir una version en el combo. Carga los datos de la noConformidad en esa version
	''' </summary>
	''' <param name="sender">componente input</param>
	''' <param name="eventArgs">evento</param>    
	''' <remarks>Llamada desde=evento del dropdown; Tiempo maximo=0seg.</remarks>*/
    function wddVersion_selectedIndexChanged(sender, eventArgs) {
        var aValue = sender.get_selectedItems()[0].get_value().split("##");

        var iInstancia = aValue[0];
        var iVersion = aValue[1];

        var FechaLocal
        FechaLocal = new Date()
        otz = FechaLocal.getTimezoneOffset()

        window.open("detalleNoConformidad.aspx?Instancia=" + iInstancia.toString() + "&NoConformidad=" + document.forms["frmDetalle"].elements["NoConformidad"].value + "&Version=" + iVersion.toString() + "&Otz=" + otz.toString(), "_self")
    }

    /*''' <summary>
	''' Evento que salta tras aÃ±adir una version en el combo.
	''' </summary>
	''' <param name="sender">componente input</param>
	''' <param name="eventArgs">evento</param>    
	''' <remarks>Llamada desde=evento del dropdown; Tiempo maximo=0seg.</remarks>*/
    function wddVersion_ItemAdded(sender, eventArgs) {
        sender.selectItemByIndex(sender.get_items().getLength() - 1, true, true)
    }
    var bEstasAñadiendo;
    /*''' <summary>
	''' Evento que salta al abrirse el combo. Tras aÃ±adir una version en el combo este se abre automaticamente, esta funciÃ³n lo cierra para 
	''' evitar q se quede abierto tras el guardado obligando al usuario a cerrarlo Ã©l.
	''' </summary>
	''' <param name="sender">componente input</param>
	''' <param name="eventArgs">evento</param>    
	''' <remarks>Llamada desde=evento del dropdown; Tiempo maximo=0seg.</remarks>*/
    function wddVersion_DropDownOpening(sender, eventArgs) {
        if (bEstasAñadiendo == 1) {
            bEstasAñadiendo = 0;
            eventArgs.set_cancel(true);
        }
    }

    function OcultarFechasResolucion() {
        if (document.getElementById("pnlFechasResolucion").style.display == "block") {
            document.getElementById("pnlFechasResolucion").style.display = "none";
            document.getElementById("imgCollapseFechasResolucion").style.display = "none";
            document.getElementById("imgExpandFechasResolucion").style.display = "block";
        } else {
            document.getElementById("pnlFechasResolucion").style.display = "block";
            document.getElementById("imgCollapseFechasResolucion").style.display = "block";
            document.getElementById("imgExpandFechasResolucion").style.display = "none";
        }
    }

    function OcultarNotificaciones() {
        if (document.getElementById("pnlNotificaciones").style.display != "none") {
            document.getElementById("pnlNotificaciones").style.display = "none";
            document.getElementById("imgCollapseNotificaciones").style.display = "none";
            document.getElementById("imgExpandNotificaciones").style.display = "inline";
        } else {
            document.getElementById("pnlNotificaciones").style.display = "block";
            document.getElementById("imgCollapseNotificaciones").style.display = "inline";
            document.getElementById("imgExpandNotificaciones").style.display = "none";
        }
    }

    function BuscarNotificadoInterno() {
        var newWindow = window.open("<%=ConfigurationManager.AppSettings("RutaFS")%>_common/BuscadorUsuarios.aspx?IDControl=wddNotificadosInternos&desde=QA", "_blank", "width=770,height=530,status=yes,resizable=no,top=200,left=200");
        newWindow.focus();
    }
    function AgregarNotificadosInternos(NotificadosInternos) {
        window.__doPostBack('wddNotificadosInternos', NotificadosInternos)
    }
    function validarLength(e, l) {
        if (e.value.length > l)
            return false
        else
            return true
    }

    function textCounter(e, l) {
        if (e.value.length > l)
            e.value = e.value.substring(0, l);
    }
    function OpenEmailSender(IdMail) {
        var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>_Common/EnvioMails.aspx?IdMail=" + IdMail + "&Volver=DetalleNoConformidad", "_blank", "width=800,height=700,status=no,resizable=no,top=200,left=200,menubar=no");
	    newWindow.focus();
	}

	/*
	Descripcion:= Funcion que envia a la pagina de AnularNoConformidad.aspx, para que el usuario pueda anular la 
				noConformidad.
	Llamada:= evento onClick de la opcion Anular de la cabecera.
	Tiempo ejecucion:=0seg.
	*/
	function AnularNoConformidad() {
	    if (document.getElementById("lnkBotonAnular"))
	        if (document.getElementById("lnkBotonAnular").disabled)
	            return false

	    CreateXmlHttp();
	    if (xmlHttp) {
	        var params = "Instancia=" + document.forms["frmDetalle"].elements["Instancia"].value + "&Version=" + document.forms["frmDetalle"].elements["Version"].value;

	        xmlHttp.open("POST", "../_common/controlarVersionQA.aspx", false);
	        xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
	        xmlHttp.send(params);

	        resultadoProcesarAcciones(false, 4);
	    }
	    else {
	        alert(arrTextosML[5]);

	        HabilitarBotones(true)
	        wProgreso = null;
	    }


	}
	function initTab(webTab) {
	    var cp = document.getElementById(webTab.ID + '_cp');
	    cp.style.minHeight = '300px';
	}
	var esIntro;
	/*''' <summary>
	''' Evitar los enter q hacen q el 1er botón de FSPMPageHeader se ejecute
	''' </summary>
	''' <param name="event">Pulsación</param>
	''' <returns>True para Cualquier tecla q pulses excepto el enter</returns>
	''' <remarks>Llamada desde: sistema; Tiempo máximo:0</remarks>*/
	function Form_KeyPres(event) {
	    esIntro = false;
	    if ((event.srcElement.type != 'textarea') && (event.keyCode == 13))
	        esIntro = true;
	    return true;
	}
</script>
<body onresize="resize()" onload="init()" onunload="if (wProgreso != null) OcultarEspera();wProgreso=null;">
    <form id="frmDetalle" method="post" runat="server" onkeypress="return Form_KeyPres(event);">
        <uc1:menu ID="Menu1" runat="server" OpcionMenu="Calidad" OpcionSubMenu="NoConformidades"></uc1:menu>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <CompositeScript>
                <Scripts>
                    <asp:ScriptReference Name="ExtenderBase.BaseScripts.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Common.Common.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Compat.Timer.Timer.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Animation.Animations.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Animation.AnimationBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="PopupExtender.PopupBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="AutoComplete.AutoCompleteBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Path="../alta/js/AdjacentHTML.js" />
                    <asp:ScriptReference Path="../alta/js/jsAlta.js" />
                </Scripts>
            </CompositeScript>
        </asp:ScriptManager>
        <table id="Table1" style="width: 100%;" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td colspan="11">
                    <fsn:FSNPageHeader runat="server" ID="FSNPageHeader" UrlImagenCabecera="./images/noconformidades2.gif"></fsn:FSNPageHeader>
                </td>
            </tr>
            <tr style="padding-left: 5px;">
                <td class="fondoCabecera" colspan="11">
                    <table id="tblCabecera" cellspacing="3" cellpadding="0" border="0" style="height: 100%; width: 100%;">
                        <tr style="padding-bottom: 10px;">
                            <td colspan="3" style="padding-left: 10px;">
                                <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
                                    <tr id="LineaRevisor" runat="server">
                                        <td colspan="3">
                                            <asp:Label ID="lblCierreRevisor" runat="server" CssClass="labelBoldRed"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:Label ID="lblInfoNoConformidad" runat="server" CssClass="labelBold"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="HyperDetalle" runat="server" CssClass="CaptionLink" Text="DDetalle de tareas"></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0">
                                    <tr>
                                        <td style="padding-left: 10px; display: inline; white-space: nowrap;">
                                            <asp:Label ID="lblUnidadQA" runat="server" CssClass="captionBlue" Style="display: inline-block;" Text="DUnidad de Negocio"></asp:Label>
                                            <input id="Ocultar" runat="server" value="1" type="hidden" name="Ocultar" />
                                            <input id="ComboQA" runat="server" value="0" type="hidden" name="ComboQA" />
                                            <input id="UnicoUnidadQA" runat="server" value="0" type="hidden" name="UnicoUnidadQA" />
                                            <asp:Label ID="lblUnicoUnidadQA" runat="server" CssClass="label" Style="padding-left: 5px; display: inline-block;" Text="DUnidad de Negocio"></asp:Label>
                                            <ig:WebDropDown ID="wddUnidadQA" runat="server" Height="22px" Width="200px" Style="display: inline-block; vertical-align: middle;"
                                                EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" CurrentValue="">
                                            </ig:WebDropDown>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table border="0" style="">
                                    <tr>
                                        <td style="padding-left: 10px; display: inline; white-space: nowrap;">
                                            <asp:Label ID="lblProveERP" runat="server" Text="DProveedor ERP" CssClass="captionBlue" Style="display: inline-block;"></asp:Label>
                                            <asp:Label ID="lblProveERPNoEdit" runat="server" CssClass="label" Style="padding-left: 5px; display: inline-block;"></asp:Label>
                                            <ig:WebDropDown ID="wddProveERP" runat="server" Height="22px" Width="25em" Style="display: inline-block; vertical-align: middle; padding-left: 5px;"
                                                EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" EnableClientRendering="true" CurrentValue="" ValueField="Cod" TextField="Den">
                                                <ItemTemplate>
                                                    <li value=${Cod}>${Den}</li>
                                                </ItemTemplate>
                                            </ig:WebDropDown>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table id="tblrevisor" border="0">
                                    <tr>
                                        <td style="padding-left: 10px; display: inline; white-space: nowrap;">
                                            <asp:Label ID="LblRevisorCorto" runat="server" CssClass="captionBlue" Style="display: inline-block;" Text="Drevisor"></asp:Label>
                                            <asp:Label ID="LbltxtRevisorBD" runat="server" CssClass="label" Visible="False" Style="padding-left: 5px;">DrevisorBD</asp:Label>
                                            <ig:WebDropDown ID="wddRevisor" runat="server" Height="22px" Width="200px" Style="display: inline-block; vertical-align: middle;"
                                                EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" CurrentValue="">
                                            </ig:WebDropDown>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td style="padding-left: 10px; display: inline; white-space: nowrap;">
                                            <asp:Label ID="lblVersion" runat="server" CssClass="captionBlue">DVersion</asp:Label>
                                            <ig:WebDropDown ID="wddVersion" runat="server" EnableViewState="true" Height="22px" Width="250px"
                                                EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true"
                                                EnableClosingDropDownOnBlur="true" CurrentValue="" Style="padding-left: 5px; display: inline-block; vertical-align: middle;">
                                                <ClientEvents SelectionChanged="wddVersion_selectedIndexChanged" ItemAdded="wddVersion_ItemAdded" DropDownOpening="wddVersion_DropDownOpening" />
                                            </ig:WebDropDown>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
                                    <tr>
                                        <td style="padding-left: 10px; display: inline; white-space: nowrap;">
                                            <asp:Label ID="lblComentarios" runat="server" Width="100%" CssClass="captionBlue">DComentarios:</asp:Label>
                                        </td>
                                        <td style="width: 100%; padding-left: 10pX;">
                                            <asp:TextBox onkeypress="return validarLength(this,500)" ID="txtComent" runat="server" onkeydown="textCounter(this,500)"
                                                onkeyup="textCounter(this,500)" Style="width: 90%" onchange="return validarLength(this,500)" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <table id="tblComentarios" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td style="padding-left: 10px;" colspan="2">
                                            <asp:Label ID="lblCierreInforme" runat="server" CssClass="fntRed">D¡El revisor ha rechazo el cierre eficaz!</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 10px; width: 174px">
                                            <asp:Label ID="lblComentRechazo" runat="server" CssClass="captionBlue">DlblMotivorechazo:</asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lbltxtComentrechazo" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 10px; width: 174px">
                                            <asp:Label ID="lblComentCierre" runat="server" CssClass="captionBlue">DComentarios del Cierre:</asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lbltxtComentCierre" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="padding-bottom: 10px;">
                            <td colspan="3">
                                <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
                                    <tr>
                                        <td>
                                            <asp:Panel runat="server" ID="pnlTituloFechasResolucion">
                                                <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;" class="PanelCabecera">
                                                    <tr style="height: 20px;">
                                                        <td>
                                                            <asp:ImageButton runat="server" ID="imgCollapseFechasResolucion" Style="display: none; cursor: hand;" />
                                                            <asp:ImageButton runat="server" ID="imgExpandFechasResolucion" Style="cursor: hand;" />
                                                        </td>
                                                        <td style="width: 100%;">
                                                            <asp:Label ID="lblTituloFechasResolucion" runat="server" Width="100%">DFechasResolucion</asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr style="padding-top: 5px;">
                                        <td>
                                            <asp:Panel runat="server" ID="pnlFechasResolucion" Style="display: none;">
                                                <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
                                                    <tr>
                                                        <td style="padding-left: 10px;">
                                                            <asp:Label ID="lblFecResolver" runat="server" CssClass="captionBlue">DResolver antes de:</asp:Label>
                                                        </td>
                                                        <td rowspan="2" style="padding-left: 10px;">
                                                            <asp:Table ID="tblAcciones" runat="server" Width="100%"
                                                                CssClass="fondoCabecera" CellSpacing="0" CellPadding="0" Style="padding-left: 30px;">
                                                            </asp:Table>
                                                        </td>
                                                        <td style="padding-left: 10px;">
                                                            <asp:Label ID="lblFecApliMejoras" runat="server" CssClass="captionBlue">DResolver antes de:</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-left: 10px;">
                                                            <fsde:GeneralEntry ID="fsentryNoConfFecDesde" runat="server" Width="120px" Independiente="1"
                                                                DropDownGridID="fsentryNoConfFecDesde" Tipo="TipoFecha" Tag="NoConfFecDesde">
                                                                <InputStyle CssClass="TipoFecha"></InputStyle>
                                                            </fsde:GeneralEntry>
                                                            <asp:Label ID="lblFechaResolver" runat="server" CssClass="label">lblFechaResolver</asp:Label>
                                                        </td>
                                                        <td style="padding-left: 10px;">
                                                            <fsde:GeneralEntry ID="fsentryFecApliMejoras" runat="server" Width="120px" Independiente="1"
                                                                DropDownGridID="fsentryFecApliMejoras" Tipo="TipoFecha" Tag="FecApliMejoras">
                                                                <InputStyle CssClass="TipoFecha"></InputStyle>
                                                            </fsde:GeneralEntry>
                                                            <asp:Label ID="txtFecApliMejoras" runat="server" CssClass="label">txtFecApliMejoras</asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="padding-bottom: 10px;">
                            <td colspan="3">
                                <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
                                    <tr>
                                        <td>
                                            <asp:Panel runat="server" ID="pnlTituloNotificaciones">
                                                <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;" class="PanelCabecera">
                                                    <tr style="height: 20px;">
                                                        <td>
                                                            <asp:ImageButton runat="server" ID="imgCollapseNotificaciones" Style="display: none; cursor: hand;" />
                                                            <asp:ImageButton runat="server" ID="imgExpandNotificaciones" Style="cursor: hand;" />
                                                        </td>
                                                        <td style="width: 100%;">
                                                            <asp:Label ID="lblNotificados" runat="server" Width="100%">DNotificaciones</asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 0px;">
                                            <asp:Panel runat="server" ID="pnlNotificaciones" Style="display: none;">
                                                <table id="tbl" cellpadding="0" cellspacing="0" border="0" style="width: 100%; height: 0px;">
                                                    <tr>
                                                        <td colspan="4" style="padding-left: 10px; padding-bottom: 5px">
                                                            <asp:Label ID="lblSeleccioneNotificados" runat="server" CssClass="captionBlue">DSeleccione los notificados del cambio de estado de las acciones:</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-left: 10px; padding-bottom: 15px; clear: both;">
                                                            <asp:Label ID="lblContacto" runat="server" CssClass="captionBlue">DNotificados proveedor:</asp:Label>
                                                        </td>
                                                        <td style="padding-bottom: 15px;">
                                                            <ig:WebDropDown ID="wddNotificadosProveedor" runat="server"
                                                                EnableDropDownAsChild="false" EnableMultipleSelection="True"
                                                                EnableClosingDropDownOnSelect="False" DropDownAnimationType="Linear"
                                                                DropDownContainerMaxHeight="100px" DropDownContainerWidth="300px" Width="300px" CurrentValue="">
                                                            </ig:WebDropDown>
                                                        </td>
                                                        <td style="padding-left: 10px; padding-bottom: 15px;">
                                                            <asp:Label ID="lblContactoInterno" runat="server" CssClass="captionBlue">DNotificados internos:</asp:Label>
                                                        </td>
                                                        <td style="padding-bottom: 15px;">
                                                            <table cellpadding="0" cellspacing="0" border="0" style="width: 100%">
                                                                <tr>
                                                                    <td>
                                                                        <asp:UpdatePanel ID="updNotificadoresInternos" runat="server">
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="imgBuscarNotificadosInternos" EventName="Click" />
                                                                            </Triggers>
                                                                            <ContentTemplate>
                                                                                <ig:WebDropDown ID="wddNotificadosInternos" runat="server"
                                                                                    EnableDropDownAsChild="false" EnableMultipleSelection="True"
                                                                                    EnableClosingDropDownOnSelect="False" DropDownAnimationType="Linear"
                                                                                    DropDownContainerMaxHeight="100px" DropDownContainerWidth="300px" Width="300px" CurrentValue="">
                                                                                </ig:WebDropDown>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                    <td style="vertical-align: bottom;">
                                                                        <asp:ImageButton ID="imgBuscarNotificadosInternos" runat="server" ImageUrl="~/App_Pages/PMWEB/_common/images/Img_Lupa.png" Style="cursor: hand; margin-left: 2px; margin-right: 2px;" />
                                                                    </td>
                                                                    <td style="width: 100%;">&nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="padding-left: 10px;">
                                                            <table cellpadding="0" cellspacing="0" border="0" class="TablaRegistroMail">
                                                                <tr class="FilaTituloRegistroMail">
                                                                    <td colspan="6">
                                                                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Image runat="server" ID="imgMail" ImageUrl="~/App_Pages/PMWEB/_common/images/sobre.gif" Style="vertical-align: middle;" />
                                                                                </td>
                                                                                <td style="width: 100%;">
                                                                                    <asp:Label runat="server" ID="lblTituloHistoricoNotificaciones" CssClass="LabelTituloRegistroMail">DHistorico de notificaciones</asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="CabeceraRegistroMail" style="width: 320px; padding-left: 5px;">
                                                                        <asp:Label runat="server" ID="lblAsunto">DAsunto</asp:Label>
                                                                    </td>
                                                                    <td class="CabeceraRegistroMail" style="width: 220px;">
                                                                        <asp:Label runat="server" ID="lblDe">DDe</asp:Label>
                                                                    </td>
                                                                    <td class="CabeceraRegistroMail" style="width: 220px;">
                                                                        <asp:Label runat="server" ID="lblPara">DPara</asp:Label>
                                                                    </td>
                                                                    <td class="CabeceraRegistroMail" style="width: 100px;">
                                                                        <asp:Label runat="server" ID="lblFecha">DFecha</asp:Label>
                                                                    </td>
                                                                    <td class="CabeceraRegistroMail" style="width: 50px;">
                                                                        <asp:Label runat="server" ID="lblEnviado">DEnv.</asp:Label>
                                                                    </td>
                                                                    <td class="CabeceraRegistroMail" style="width: 15px;">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="6" style="vertical-align: top;">
                                                                        <div style="height: 80px; width: 100%; overflow: auto;">
                                                                            <table cellpadding="0" cellspacing="0" border="0" style="height: 81px;">
                                                                                <tr style="vertical-align: top;">
                                                                                    <td>
                                                                                        <asp:GridView runat="server" ID="gvRegistroMails"
                                                                                            ShowHeader="false" AutoGenerateColumns="false" GridLines="Horizontal">
                                                                                            <RowStyle CssClass="FilasGridMail" />
                                                                                            <Columns>
                                                                                                <asp:BoundField DataField="SUBJECT" ItemStyle-Width="320px" />
                                                                                                <asp:BoundField DataField="DIR_RESPUESTA" ItemStyle-Width="220px" />
                                                                                                <asp:BoundField DataField="PARA" ItemStyle-Width="220px" />
                                                                                                <asp:BoundField DataField="FECHA" ItemStyle-Width="100px" />
                                                                                                <asp:TemplateField>
                                                                                                    <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Image runat="server" ID="imgOK" ImageUrl="~/App_Pages/PMWEB/_common/images/aprobado.gif" />
                                                                                                        <asp:Image runat="server" ID="imgKO" ImageUrl="~/App_Pages/PMWEB/_common/images/Img_Ico_Alerta_Peq.gif" />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="height: 20px;">
                <td>&nbsp</td>
            </tr>
            <tr style="padding-left: 5px;">
                <td colspan="11">
                    <asp:Label ID="lblCamposObligatorios" Style="z-index: 102; display: none;" runat="server" CssClass="captionRed">Los campos marcados con (*) son de obligada cumplimentacion</asp:Label>
                </td>
            </tr>
        </table>
        <div id="divAcciones" runat="server">
            <ignav:UltraWebMenu ID="uwPopUpAcciones" runat="server" WebMenuTarget="PopupMenu" SubMenuImage="ig_menuTri.gif"
                ScrollImageTop="ig_menu_scrollup.gif" Cursor="Default" ScrollImageBottomDisabled="ig_menu_scrolldown_disabled.gif" ScrollImageTopDisabled="ig_menu_scrollup_disabled.gif"
                ScrollImageBottom="ig_menu_scrolldown.gif" AutoPostBack="false">
                <MenuClientSideEvents InitializeMenu="" ItemChecked="" ItemClick="" SubMenuDisplay="" ItemHover=""></MenuClientSideEvents>
                <DisabledStyle ForeColor="LightGray"></DisabledStyle>
                <HoverItemStyle Cursor="Hand"></HoverItemStyle>
                <IslandStyle Cursor="Default" BorderWidth="1px" Font-Size="8pt" Font-Names="MS Sans Serif" BorderStyle="Outset" ForeColor="Black" BackColor="LightGray"></IslandStyle>
                <ExpandEffects ShadowColor="LightGray"></ExpandEffects>
                <TopSelectedStyle Cursor="Default"></TopSelectedStyle>
                <SeparatorStyle BackgroundImage="ig_menuSep.gif" CustomRules="background-repeat:repeat-x; "></SeparatorStyle>
                <Levels>
                    <ignav:Level Index="0"></ignav:Level>
                </Levels>
            </ignav:UltraWebMenu>
        </div>
        <div id="divListados" runat="server">
            <ignav:UltraWebMenu ID="uwPopUpListados" Style="z-index: 112; left: 192px; position: absolute; top: 24px"
                runat="server" WebMenuTarget="PopupMenu" SubMenuImage="ig_menuTri.gif" ScrollImageTop="ig_menu_scrollup.gif" Cursor="Default" ScrollImageBottomDisabled="ig_menu_scrolldown_disabled.gif"
                ScrollImageTopDisabled="ig_menu_scrollup_disabled.gif" ScrollImageBottom="ig_menu_scrolldown.gif">
                <ItemStyle CssClass="ugMenuItem"></ItemStyle>
                <DisabledStyle ForeColor="LightGray"></DisabledStyle>
                <HoverItemStyle Cursor="Hand" CssClass="ugMenuItemHover"></HoverItemStyle>
                <IslandStyle Cursor="Default" BorderWidth="1px" Font-Size="8pt" Font-Names="MS Sans Serif" BorderStyle="Outset"
                    ForeColor="Black" BackColor="LightGray">
                </IslandStyle>
                <ExpandEffects ShadowColor="LightGray"></ExpandEffects>
                <TopSelectedStyle Cursor="Default"></TopSelectedStyle>
                <SeparatorStyle BackgroundImage="ig_menuSep.gif" CssClass="SeparatorClass" CustomRules="background-repeat:repeat-x; "></SeparatorStyle>
                <Levels>
                    <ignav:Level Index="0"></ignav:Level>
                </Levels>
            </ignav:UltraWebMenu>
        </div>
        <div id="divProgreso" style="display: inline">
            <table id="tblProgreso" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
                <tr style="height: 50px">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:TextBox ID="lblProgreso" Style="text-align: center" runat="server" CssClass="captionBlue"
                            Width="100%" BorderStyle="None" BorderWidth="0">Su solicitud está siendo tramitada. Espere unos instantes...</asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:Image ID="imgProgreso" runat="server" src="../_common/images/cursor-espera_grande.gif"></asp:Image>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divForm2" style="display: none;">
            <table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
                <tr>
                    <td valign="middle">
                        <igtab:UltraWebTab ID="uwtGrupos" runat="server" Width="100%" Height="450px" BorderStyle="Solid" BorderWidth="1px"
                            ThreeDEffect="False" DummyTargetUrl=" " FixedLayout="True" CustomRules="padding:10px;"
                            DisplayMode="Scrollable" EnableViewState="false">
                            <DefaultTabStyle Height="24px" CssClass="uwtDefaultTab">
                                <Padding Left="20px" Right="20px"></Padding>
                            </DefaultTabStyle>
                            <RoundedImage NormalImage="ig_tab_blueb2.gif" HoverImage="ig_tab_blueb1.gif" FillStyle="LeftMergedWithCenter"></RoundedImage>
                            <ClientSideEvents InitializeTabs="initTab" />
                        </igtab:UltraWebTab>
                    </td>
                </tr>
            </table>
            <input id="Instancia" type="hidden" size="16" name="Instancia" runat="server" />
            <input id="FuerzaDesgloseLectura" type="hidden" size="16" name="FuerzaDesgloseLectura" runat="server" />
            <input id="NoConformidad" type="hidden" size="16" name="NoConformidad" runat="server" />
            <input id="txtEnviar" type="hidden" name="Enviar" runat="server" />
            <input id="Version" type="hidden" name="Version" runat="server" />
            <input id="VersionInicialCargada" type="hidden" name="VersionInicialCargada" runat="server" />
            <input id="Proveedor" type="hidden" name="Proveedor" runat="server" />
            <input id="Email" type="hidden" size="10" runat="server" />
            <input id="Notificar" type="hidden" name="Notificar" />
            <input id="DenProveedor" type="hidden" name="DenProveedor" runat="server" />
            <input id="Tipo" type="hidden" name="Tipo" runat="server" />
            <input id="FecAlta" type="hidden" name="FecAlta" runat="server" />
            <input id="BotonCalcular" type="hidden" value="0" name="BotonCalcular" runat="server" />
            <input id="EstadoNoConf" type="hidden" value="0" name="EstadoNoConf" runat="server" />
            <input id="Workflow" type="hidden" value="0" name="Workflow" runat="server" />
            <input id="CodigoRevisorAnterior" name="CodigoRevisorAnterior" type="hidden" runat="server" />
            <input id="PantallaMaper" type="hidden" name="PantallaMaper" runat="server" />
        </div>
    </form>
    <form id="frmCalculados" name="frmCalculados" action="../alta/recalcularimportes.aspx"
        method="post" target="fraWSServer">
    </form>
    <form id="frmDesglose" name="frmDesglose" method="post" target="winDesglose">
    </form>
    <div id="divDropDowns" style="z-index: 101; visibility: hidden; position: absolute; top: 400px"></div>
    <div id="divAlta" style="visibility: hidden"></div>
    <div id="divCalculados" style="z-index: 109; visibility: hidden; position: absolute; top: 0px"></div>
    <iframe id="iframeWSServer" style="z-index: 104; left: 40px; visibility: hidden; width: 268px; position: absolute; top: 388px; height: 62px"
        name="iframeWSServer" src="../blank.htm"></iframe>
    <input id="AccesoRevisor" type="hidden" size="16" name="AccesoRevisor" runat="server" />
    <input id="codRevisor" type="hidden" size="16" name="CodRevisor" runat="server" />
    <input type="hidden" runat="server" id="diferenciaUTCServidor" name="diferenciaUTCServidor" />
    <input id="indiceCombo" type="hidden" name="indiceCombo" runat="server" />

    <script type="text/javascript">
        $(document).ready(function () {
            if (esNoconfEscalacion) {
                $('#lblFecResolver').parent().appendTo($('#tblAcciones').parent().parent())
                $('#fsentryNoConfFecDesde__tbl').parent().appendTo($('#fsentryFecApliMejoras__tbl').parent().parent())
                $('#lblFechaResolver').parent().appendTo($('#txtFecApliMejoras').parent().parent())
            }
            else {
                $('#lblFecApliMejoras').parent().hide();
                $('#fsentryFecApliMejoras__tbl').parent().hide();
            }
            if ($get('wddProveERP') != undefined) {
                if ($get('wddProveERP').style.display != "none" && emp != undefined) {
                    var owddProveERP = $find('wddProveERP');
                    if ($('#Proveedor').val() != undefined && $('#Proveedor').val() != "") {
                        cargarProveerdoresERP(emp, $('#Proveedor').val());

                        for (var i = 0; i < owddProveERP.get_items().getLength() ; i++) {
                            if (owddProveERP.get_items().get_item(i).get_value() == proveERP) {
                                owddProveERP.set_selectedItemIndex(owddProveERP.get_items().get_item(i).get_index());
                                var text = owddProveERP.get_items().get_item(i).get_text();
                                owddProveERP.set_currentValue(text, true);
                                break;
                            }
                        }
                    }
                }
            }
            else {                
                if ($get('lblProveERPNoEdit') != undefined) {
                    if ($get('lblProveERPNoEdit').style.display != "none" && emp != undefined) $get('lblProveERPNoEdit').innerText = proveERP + (proveERPDen == "" ? "" : " - " + proveERPDen);
                }
            }
        })

        function mostrarMenuListados(event) {
            var p = $get("lnkBotonListados").getBoundingClientRect();            
            igmenu_showMenu('uwPopUpListados', event, p.left, p.bottom);
            return false;            
        }

        function wddUnidadQA_SelectionChanged(sender, e) {
            //Visibilidad de Proveedor en ERP
            if (e.getNewSelection().length > 0) {
                if (e.getNewSelection()[0].get_value() != undefined) {
                    $.when($.ajax({
                        type: "POST",
                        url: rutaQA + 'noconformidad/altaNoconformidad.aspx/ProveERPVisible',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({ UNQA: e.getNewSelection()[0].get_value() }),
                        async: false
                    })).done(function (msg) {
                        if (msg.d[0] == 0) {
                            $('#lblProveERP').hide();
                            $('#wddProveERP').hide();
                        }
                        else {
                            $('#lblProveERP').show();
                            $('#wddProveERP').show();

                            emp = msg.d[1];
                        }

                        //Vaciar el valor de la combo                             
                        var owddProveERP = $find('wddProveERP');
                        var dataSource = [];
                        owddProveERP.set_dataSource(dataSource);
                        owddProveERP.dataBind();
                        owddProveERP.set_currentValue('', true);
                    })
                }
                else {
                    $('#lblProveERP').hide();
                    $('#wddProveERP').hide();
                }
            }
            else {
                $('#lblProveERP').hide();
                $('#wddProveERP').hide();
            }

        }

        function wddProveERP_DropDownOpening(sender, e) {
            var bUNQASel = false;
            if ($get('wddUnidadQA') != undefined) {
                var wDropDownUnidadQA = $find($('[id$=wddUnidadQA]').attr('id'));
                if (wDropDownUnidadQA.get_selectedItems().length > 0) {
                    bUNQASel = (wDropDownUnidadQA.get_selectedItems()[0].get_value() != '');
                }
            }
            else {
                bUNQASel = ($('#UnicoUnidadQA').val() != "")
            }

            if ($('#Proveedor').val() != undefined && $('#Proveedor').val() != "" && bUNQASel) cargarProveerdoresERP(emp, $('#Proveedor').val());
        }

        function cargarProveerdoresERP(emp, codprove) {
            $.when($.ajax({
                type: "POST",
                url: rutaQA + 'noconformidad/altaNoconformidad.aspx/ObtenerProveedoresERP',
                data: "{'iEmpresa':'" + emp + "','sProve':'" + codprove + "'}",
                contentType: "application/json; utf-8",
                dataType: "json",
                async: false
            })).done(function (msg) {
                var dataSource;
                if (msg.d != null) {
                    dataSource = jQuery.parseJSON('[' + msg.d + ']')
                }
                else {//No hay datos
                    dataSource = jQuery.parseJSON('[{""Cod"":"""",""Den"":""""}]')
                }
                var oWddProves = $find('wddProveERP');
                oWddProves.set_dataSource(dataSource);
                oWddProves.dataBind();
            });
        }
        function mostrarMenuAcciones(event, i) {
            var p = $get("lnkBotonAccion" + i.toString()).getBoundingClientRect();
            igmenu_showMenu('uwPopUpAcciones', event, p.left, p.bottom);
            return false;
        }
    </script>
</body>
</html>
