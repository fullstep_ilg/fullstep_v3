
    Public Class aprobRechazoAccion
    Inherits FSNPage

        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
    "<script language=""{0}"">{1}</script>"

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
        Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
        Protected WithEvents txtComent As System.Web.UI.HtmlControls.HtmlTextArea
        Protected WithEvents cmdAceptar As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents cmdCancelar As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents Aprobar As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents desglose As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents index As System.Web.UI.HtmlControls.HtmlInputHidden

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim oDict As FSNServer.Dictionary
            Dim oTextos As DataTable
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")

        Dim sIdi As String = Session("FSN_User").Idioma.ToString()
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If

            oDict = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
            oDict.LoadData(TiposDeDatos.ModulosIdiomas.AprobRechazoAccion, sIdi)
            oTextos = oDict.Data.Tables(0)

            Me.desglose.Value = Request("desglose")
            Me.index.Value = Request("index")

            'Textos de idiomas:
            Me.cmdAceptar.Value = oTextos.Rows(4).Item(1)
            Me.cmdCancelar.Value = oTextos.Rows(5).Item(1)

            If Request("Aprobar") = True Then 'Aprueba
                Me.Aprobar.Value = 1
                Me.lblTitulo.Text = oTextos.Rows(0).Item(1) & ": " & Request("DenAccion")
                Me.lblMensaje.Text = oTextos.Rows(2).Item(1)
            Else
                Me.Aprobar.Value = 0 'Rechaza 
                Me.lblTitulo.Text = oTextos.Rows(1).Item(1) & ": " & Request("DenAccion")
                Me.lblMensaje.Text = oTextos.Rows(3).Item(1)
            End If

            Dim sClientTexts As String
            sClientTexts = ""
            sClientTexts += "var msjComentario = '" + JSText(oTextos.Rows(6).Item(1)) + "';"  'Debe meter un comentario para aprobar la Acci�n
            sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
            Page.ClientScript.RegisterStartupScript(Me.GetType(),"TextosAPrAccionCliente", sClientTexts)

            oTextos = Nothing
            oDict = Nothing
        End Sub

    End Class


