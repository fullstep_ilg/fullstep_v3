
Public Class cierreRevisorOK
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    Protected WithEvents NoConformidad As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtComent As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents cmdAceptar As System.Web.UI.WebControls.Button
    Protected WithEvents cmdCancelar As System.Web.UI.WebControls.Button
    Protected WithEvents lblRechazo As System.Web.UI.WebControls.Label
    Protected WithEvents lblID As System.Web.UI.WebControls.Label
    Protected WithEvents lblCierreEfectivo As System.Web.UI.WebControls.Label
    Protected WithEvents lblresponsable As System.Web.UI.WebControls.Label
    Protected WithEvents lblIDBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblCierreEfectivoBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblresponsableBD As System.Web.UI.WebControls.Label
    Protected WithEvents txtPeticionario As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents FSNPageHeader As Fullstep.FSNWebControls.FSNPageHeader
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private oNoConformidad As FSNServer.NoConformidad
    Private oInstancia As FSNServer.Instancia
    Dim FSWSServer As FSNServer.Root

    ''' <summary>
    ''' Pantalla para informar de q ha ido bien el cierre
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>Llamada desde: La propia p�gina, cada vez que se carga. La usa cierrerevisor.aspx
    ''' Tiempo m�ximo:0,1</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        Dim sIdi As String = FSNUser.Idioma.ToString()

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.RechazoNoConfRevisor

        cmdCancelar.Attributes.Add("onclick", "IrADetalle();return false;")

        Me.NoConformidad.Value = Request("NoConformidad")

        'Carga los datos de la no conformidad:
        oNoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
        oNoConformidad.Instancia = Request("Instancia")
        oNoConformidad.CargarDatosDesdeInstancia()

        'textos
        'botones
        Me.cmdAceptar.Text = Textos(5)
        Me.cmdCancelar.Text = Textos(6)

        'etiquetas de cabecera
        FSNPageHeader.TituloCabecera = Textos(9)
        Me.lblID.Text = Textos(0)
        Me.lblIDBD.Text = oNoConformidad.Instancia
        Me.lblresponsable.Text = Textos(2)
        Me.lblresponsableBD.Text = oNoConformidad.Peticionario
        Me.lblCierreEfectivo.Text = Textos(3)
        Me.lblCierreEfectivoBD.Text = oNoConformidad.FecCierre.ToString("d", FSNUser.DateFormat)
        Me.lblMensaje.Text = Textos(4)
        If oNoConformidad.Estado = 5 Then
            Me.lblRechazo.Text = Textos(7)
        Else
            Me.lblRechazo.Text = Textos(8)
        End If
        'Rellenamos el codigo del peticionario en un imput oculto
        Me.txtPeticionario.Value = oNoConformidad.PeticionarioCod

    End Sub

    Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Unload
        oInstancia = Nothing
        oNoConformidad = Nothing
        FSWSServer = Nothing
    End Sub

    ''' <summary>
    ''' Indica que la noConformidad ha sido rechazada o no
    ''' En instacia_est se guardar en el campo accio se guarda el valor (Rechazadopor revisor)
    ''' en el campo coment se guarda el comentario para el peticionario
    ''' se actualiza el estado de la no conformidad a 1 (ABIERTA)
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo m�ximo:0,3</remarks>
    Private Sub cmdAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        Dim bPositivo As Boolean
        'Actualizamos el estado de la no conformidad
        'El primer parametro es el texto xa el peticionario
        'El 2� parametro es el email del revisor
        If oNoConformidad.Estado = TipoEstadoNoConformidad.CierrePosSinRevisar Then
            bPositivo = True
        Else
            bPositivo = False
        End If
        oNoConformidad.RechazarNoConformidadPorRevisor(bPositivo, VacioToNothing(txtComent.InnerText), FSNUser.Email, FSNUser.CodPersona)
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "init", "<script>window.open(""" & ConfigurationManager.AppSettings("rutaQA2008") & "NoConformidades/NoConformidades.aspx" & """,""_top"")</script>")
    End Sub
End Class