
    Public Class noconfEmitidaAWorkfl
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
        Protected WithEvents lblSolicitud As System.Web.UI.WebControls.Label
        Protected WithEvents lblPeticionario As System.Web.UI.WebControls.Label
        Protected WithEvents lblAprobador As System.Web.UI.WebControls.Label
        Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
        Protected WithEvents lblMensaje2 As System.Web.UI.WebControls.Label

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim oDict As FSNServer.Dictionary
            Dim oTextos As DataTable
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
            Dim oInstancia As FSNServer.Instancia
            Dim oUser As FSNServer.User
        oUser = Session("FSN_User")

        Dim sIdi As String = Session("FSN_User").Idioma.ToString()
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If

            oDict = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
            oDict.LoadData(TiposDeDatos.ModulosIdiomas.NoConfonfEmitidaWorkfl, sIdi)
            oTextos = oDict.Data.Tables(0)

            'Textos de idiomas:
            Me.lblTitulo.Text = oTextos.Rows(0).Item(1)
            Me.lblMensaje.Text = oTextos.Rows(5).Item(1)
            Me.lblMensaje2.Text = oTextos.Rows(4).Item(1)

            'Carga los datos de la instancia:
            oInstancia = FSWSServer.Get_Object(GetType(FSNServer.Instancia))
            oInstancia.ID = Request("Instancia")
            oInstancia.Load(sIdi)

            'Datos de la instancia:
            Me.lblSolicitud.Text = oTextos.Rows(1).Item(1) & oInstancia.ID
            Me.lblPeticionario.Text = oTextos.Rows(2).Item(1) & oInstancia.NombrePeticionario

            If oInstancia.AprobadorActual = Nothing Then
                Me.lblAprobador.Text = oTextos.Rows(3).Item(1) & oInstancia.UnidadYDep
            Else
                Me.lblAprobador.Text = oTextos.Rows(3).Item(1) & oInstancia.NombreAprobadorActual
            End If

            oInstancia = Nothing
            oTextos = Nothing
            oDict = Nothing

        End Sub

    End Class

