
Public Class noconfEmitidaOK
    Inherits FSNPage

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf &
"<script language=""{0}"">{1}</script>"

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblPeticionario As System.Web.UI.WebControls.Label
    Protected WithEvents lblPeticionarioValue As System.Web.UI.WebControls.Label
    Protected WithEvents lblProveedor As System.Web.UI.WebControls.Label
    Protected WithEvents lblProveedorValue As System.Web.UI.WebControls.Label
    Protected WithEvents lblContacto As System.Web.UI.WebControls.Label
    Protected WithEvents lblContactoValue As System.Web.UI.WebControls.Label
    Protected WithEvents lblIdentificador As System.Web.UI.WebControls.Label
    Protected WithEvents lblIdentificadorValue As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecEmision As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecEmisionValue As System.Web.UI.WebControls.Label
    Protected WithEvents lblInf As System.Web.UI.WebControls.Label
    Protected WithEvents diferenciaUTCServidor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblNotificadosInternosPersonas As System.Web.UI.WebControls.Label
    Protected WithEvents lblNotificadosInternos As System.Web.UI.WebControls.Label
    Protected WithEvents FSNPageHeader As Fullstep.FSNWebControls.FSNPageHeader
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' <summary>
    ''' Carga los elementos de la pagina cuando se emite una noConformidad
    ''' </summary>
    ''' <param name="sender">Propios del evento</param>
    ''' <param name="e">propios del evento</param>                
    ''' <remarks>Llamada desde=CargarGrid; Tiempo m�ximo=0</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oNoConformidad As FSNServer.NoConformidad
        Dim sIdi As String = FSNUser.Idioma.ToString()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.NoConformidadEmitidaOK

        FSNPageHeader.TituloCabecera = Textos(0)
        FSNPageHeader.VisibleBotonVolver = True
        FSNPageHeader.TextoBotonVolver = Textos(12)
        FSNPageHeader.OnClientClickVolver = "IrAlVisorNoConformidades();return false;"

        'Carga los datos de la no conformidad:
        oNoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
        oNoConformidad.Instancia = Request("Instancia")
        oNoConformidad.CargarDatosDesdeInstancia()
        diferenciaUTCServidor.Value = oNoConformidad.DiferenciaUTCServidor

        'Datos de la instancia:
        Me.lblIdentificador.Text = Textos(1) & " "
        Me.lblIdentificadorValue.Text = Request("Instancia")
        Me.lblPeticionario.Text = Textos(2) & " "
        Me.lblPeticionarioValue.Text = oNoConformidad.Peticionario
        Me.lblFecEmision.Text = Textos(3) & " "
        Me.lblFecEmisionValue.Text = ""
        Me.lblProveedor.Text = Textos(4) & " "
        Me.lblProveedorValue.text = oNoConformidad.Proveedor & " - " & oNoConformidad.ProveDen
        Me.lblContacto.Text = Textos(5) & " "
        Me.lblContactoValue.Text = oNoConformidad.NotificadosProveedor

        If oNoConformidad.NotificadosInternos Is Nothing Then
            lblNotificadosInternos.Visible = False
            Me.lblNotificadosInternosPersonas.Visible = False
        Else
            lblNotificadosInternos.Text = Textos(11)
            Me.lblNotificadosInternosPersonas.Text = oNoConformidad.NotificadosInternos
        End If

        Me.lblInf.Text = Textos(10)


        Dim sClientTextVars As String
        sClientTextVars = ""
        sClientTextVars += "vDatefmt='" + FSNUser.DateFormat.ShortDatePattern + "';"
        sClientTextVars += "vAnyo='" + Year(oNoConformidad.FechaAlta).ToString + "';"
        sClientTextVars += "vMes='" + Month(oNoConformidad.FechaAlta).ToString + "';"
        sClientTextVars += "vDia='" + Day(oNoConformidad.FechaAlta).ToString + "';"
        sClientTextVars += "vHoras='" + Hour(oNoConformidad.FechaAlta).ToString + "';"
        sClientTextVars += "vMinutos='" + Minute(oNoConformidad.FechaAlta).ToString + "';"
        sClientTextVars += "vSegundos='" + Second(oNoConformidad.FechaAlta).ToString + "';"

        sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsUser", sClientTextVars)

        oNoConformidad = Nothing

        If Session("VolverVisor") = "" Then
            Session("VolverVisor") = ConfigurationManager.AppSettings("rutaQA2008") & "NoConformidades/NoConformidades.aspx"
        End If
    End Sub
End Class
