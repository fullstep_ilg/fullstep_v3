<%@ Page Language="vb" AutoEventWireup="false" Codebehind="noconfEmitidaOK.aspx.vb" Inherits="Fullstep.FSNWeb.noconfEmitidaOK"%>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">	
	</head>	
	<script type="text/javascript"> <!--
		/*''' <summary>
		''' Vuelve a la pagina anterior
		''' </summary>     
		''' <remarks>Llamada desde:FSNPageHeader.OnClientClickVolver; Tiempo m�ximo:0</remarks>*/
		function IrAlVisorNoConformidades() {
		    sVolver = "<%=Session("VolverVisor")%>"
		    window.open(sVolver, "_top")
		}

		/*
		''' <summary>
		''' Esta funcion convierte un objeto javascript Date en una cadena con una fecha
		''' en el formato especificado en vdatefmt
		''' </summary>
		''' <param name="fec">Fecha a convertir</param>
		''' <param name="vdatefmt">formato del usuario</param>        
		''' <param name="bConHora">Si la fecha tiene tb las horas/param>        
		''' <returns>Devuelve cadena con la fecha en el formato del usuario</returns>
		''' <remarks>Llamada desde=CargarGrid; Tiempo m�ximo=0</remarks>
		*/
		function myDatetoStr(fec, vdatefmt, bConHora) {
			var vDia
			var vMes
			var str

			if (fec == null) {
				return ("")
			}
			re = /dd/i
			vDia = "00".substr(0, 2 - fec.getDate().toString().length) + fec.getDate().toString()
			str = vdatefmt.replace(re, vDia)

			re = /mm/i

			vMes = "00".substr(0, 2 - (fec.getMonth() + 1).toString().length) + (fec.getMonth() + 1).toString()
			str = str.replace(re, vMes)

			re = /yyyy/i

			str = str.replace(re, fec.getFullYear())
			if (bConHora)
				str += (" " + "00".substr(0, 2 - (fec.getHours()).toString().length) + fec.getHours() + ":" + "00".substr(0, 2 - (fec.getMinutes()).toString().length) + fec.getMinutes())
			return (str)
		}

		/*
		''' <summary>
		''' Al cargarse la pagina pone la hora en la que se creo la NoConformdidad con la hora del equipo del usuario y el formato del usuario
		''' </summary>      
		''' <remarks>Llamada desde=onLoad_Body; Tiempo m�ximo=0</remarks>
		*/
		function Init() {
			var FechaLocal
			FechaLocal = new Date()
			otz = FechaLocal.getTimezoneOffset()

			var diferencia = parseFloat(window.document.getElementById("diferenciaUTCServidor").value) + otz


			FechaLocal = new Date(vAnyo, vMes - 1, vDia, vHoras,vMinutos - diferencia,vSegundos)
			//vDatefmt se crea en el vb page_loal
			valorTempVersion = myDatetoStr(FechaLocal, vDatefmt, true)
			var lblFecEmisionValue = document.getElementById("<%=lblFecEmisionValue.ClientID%>");
			lblFecEmisionValue.innerHTML = valorTempVersion
		}
--></script>	
	<body onload="Init()">
		<form id="Form1" method="post" runat="server">
		<input type="hidden" runat="server" id="diferenciaUTCServidor" name="diferenciaUTCServidor" />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
		<uc1:menu id="Menu1" runat="server" OpcionMenu="Calidad" OpcionSubMenu="NoConformidades"></uc1:menu>        
		<table id="tblTitulo" cellspacing="0" cellpadding="0" border="0" style="width:100%; margin-top:3.5em;">
			<tr>
				<td colspan="2" style="width:100%;">
					<fsn:FSNPageHeader runat="server" ID="FSNPageHeader" UrlImagenCabecera="./images/noconformidades2.gif">
					</fsn:FSNPageHeader>
				</td>
			</tr>
		</table>
		<table id="Table1" border="0" style="width:100%;" class="fondoCabecera">
			<tr>
				<td style="width:50%; padding-left:10px;">
					<asp:Label ID="lblIdentificador" runat="server" CssClass="captionBlue">lblIdentificador</asp:Label>
					<asp:Label ID="lblIdentificadorValue" runat="server" CssClass="label">lblIdentificador</asp:Label>
				</td>
				<td width="50%">
					<asp:Label ID="lblProveedor" runat="server" CssClass="captionBlue"></asp:Label>
					<asp:Label ID="lblProveedorValue" runat="server" CssClass="label"></asp:Label>
				</td>
			</tr>
			<tr>
				<td style="padding-left:10px;">
					<asp:Label ID="lblFecEmision" runat="server" CssClass="captionBlue"></asp:Label>
					<asp:Label ID="lblFecEmisionValue" runat="server" CssClass="label"></asp:Label>
				</td>
				<td>
					<asp:Label ID="lblContacto" runat="server" CssClass="captionBlue">lblContacto</asp:Label>
					<asp:Label ID="lblContactoValue" runat="server" CssClass="label">lblContacto</asp:Label>                    
				</td>
			</tr>
			<tr>
				<td style="padding-left:10px;">
					<asp:Label ID="lblPeticionario" runat="server" CssClass="captionBlue">lblPeticionario</asp:Label>
					<asp:Label ID="lblPeticionarioValue" runat="server" CssClass="label">lblPeticionario</asp:Label>
				</td>
				<td>
					<asp:Label ID="lblNotificadosInternos" runat="server" CssClass="captionBlue">lblContacto</asp:Label>
					<asp:Label ID="lblNotificadosInternosPersonas" runat="server" CssClass="label">lblContacto</asp:Label>
				</td>
			</tr>
			<tr>
				<td height="15%" colspan="3">
				</td>
			</tr>
		</table>
		<table style="width:100%;">
			<tr>
				<td width="100%">
					<asp:Label ID="lblInf" runat="server" CssClass="label">dPara consultar el estado de la no conformidad acceda al visor de no conformidades</asp:Label>
				</td>
			</tr>
		</table>
		</form>
	</body>
</html>
