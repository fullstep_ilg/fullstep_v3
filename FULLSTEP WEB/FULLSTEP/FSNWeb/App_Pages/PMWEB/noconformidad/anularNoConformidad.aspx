﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="anularNoConformidad.aspx.vb" Inherits="Fullstep.FSNWeb.anularNoConformidad" %>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
	</head>	
	<script type="text/javascript">
		var vDatefmt='';
		var vAnyo=0;

		
		/*
		Descripcion:= Nos lleva a la pagina del detalle de la NoConformidad
		Llamada desde:= Cuando se pulsa en el boton de cancelar
		Tiempo ejecucion:= 0seg.
		*/
		function IrADetalle()
		{ 
			var FechaLocal;
			FechaLocal = new Date();
			var otz = FechaLocal.getTimezoneOffset();	
		
			window.open("detalleNoConformidad.aspx?NoConformidad=" + "<%=Request("NoConformidad")%>&Otz="+ otz.toString() ,"_self")
		}
	
		/*
		Descripcion:= Nos lleva a la pagina del visor
		Llamada desde:= Cuando se pulsa en el boton de volver
		Tiempo ejecucion:= 0seg.
		*/
		function IrAlVisorNoConformidades()
		{
		sVolver = "<%=Session("VolverVisor")%>"
			window.open(sVolver, "_top")

		}
	
		/*
		''' <summary>
		''' Esta funcion convierte un objeto javascript Date en una cadena con una fecha
		''' en el formato especificado en vdatefmt
		''' </summary>
		''' <param name="fec">Fecha a convertir</param>
		''' <param name="vdatefmt">formato del usuario</param>        
		''' <param name="bConHora">Si la fecha tiene tb las horas/param>        
		''' <returns>Devuelve cadena con la fecha en el formato del usuario</returns>
		''' <remarks>Llamada desde=CargarGrid; Tiempo máximo=0</remarks>
		*/
		function myDatetoStr(fec, vdatefmt, bConHora) {
			var vDia
			var vMes
			var str

			if (fec == null) {
				return ("")
			}
			re = /dd/i
			vDia = "00".substr(0, 2 - fec.getDate().toString().length) + fec.getDate().toString()
			str = vdatefmt.replace(re, vDia)

			re = /mm/i

			vMes = "00".substr(0, 2 - (fec.getMonth() + 1).toString().length) + (fec.getMonth() + 1).toString()
			str = str.replace(re, vMes)

			re = /yyyy/i

			str = str.replace(re, fec.getFullYear())
			if (bConHora)
				str += (" " + "00".substr(0, 2 - (fec.getHours()).toString().length) + fec.getHours() + ":" + "00".substr(0, 2 - (fec.getMinutes()).toString().length) + fec.getMinutes())
			return (str)
		}

		/*
		''' <summary>
		''' Al cargarse la pagina pone la hora en la que se creo la NoConformdidad con la hora del equipo del usuario y el formato del usuario
		''' </summary>      
		''' <remarks>Llamada desde=onLoad_Body; Tiempo máximo=0</remarks>
		*/
		function Init() {
			var lblFechaAnulacionValue = document.getElementById("<%=lblFechaAnulacionBD.ClientID%>");
			hidFecha = document.getElementById("<%=hidFechaAnulacion.ClientID %>");
			if (hidFecha)
				hidFecha.value = lblFechaAnulacionValue.innerHTML
		}
		/*
		''' <summary>
		'''         Funcion que comprueba que la longitud que se puede introducir en el textarea
		''' </summary>
		''' <param name="e">el textarea</param>
		''' <param name="l">longitud alcanzable</param>        
		''' <returns>True si es correcto, false si ha excedido de la longitud</returns>        
		*/
		function validarLength(e, l) {
			if (e.value.length > l)
				return false
			else
				return true
		}
	
			/*
		''' <summary>
		'''         Funcion que comprueba que la longitud que se puede introducir en el textarea
		''' </summary>
		''' <param name="e">el textarea</param>
		''' <param name="l">longitud alcanzable</param>        
		''' <returns>True si es correcto, false si ha excedido de la longitud</returns>        
		*/
		function textCounter(e, l) {
			if (e.value.length > l)
				e.value = e.value.substring(0, l);
		}	
	</script>
<body onload="Init()">
	<form id="form1" runat="server">
	<uc1:menu ID="Menu1" runat="server" OpcionMenu="Calidad" OpcionSubMenu="NoConformidades">
	</uc1:menu>
	<asp:ScriptManager ID="ScriptManager1" runat="server">
	</asp:ScriptManager>
	<input type="hidden" id="diferenciaUTCServidor" runat="server" value="0" />
	<input type="hidden" id="Accion" runat="server" value="0" />
	<input type="hidden" id="hidFechaAnulacion" runat="server" />
	<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
		<tr>
			<td>
				<fsn:FSNPageHeader runat="server" ID="FSNPageHeader" UrlImagenCabecera="./images/noconformidades2.gif">
				</fsn:FSNPageHeader>
			</td>
		</tr>
	</table>
	<asp:UpdatePanel ID="upInformacionNoConformidad" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<table id="tblMensaje" border="0" cellpadding="0" cellspacing="0" style="width: 100%;
				padding-top: 15px">
				<tr>
					<td>
						<asp:Panel ID="PanelMensaje" runat="server" Visible="false">
							<asp:Label ID="lblMensaje" runat="server" Text="" Visible="false" CssClass="TituloDarkBlue"></asp:Label>
						</asp:Panel>
					</td>
				</tr>
			</table>
			<table border="0" cellpadding="0" cellspacing="0" style="padding-left: 5px; padding-right: 5px;
				padding-bottom: 20px; padding-top: 15px; width: 100%">
				<tr>
					<td>
						<table id="tblInformacionNoConformidad" class="fondoCabecera" style="width: 100%;
							padding-top: 3px; padding-bottom: 10px; padding-left: 10px; padding-right: 2px"
							cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td width="50%">
									<asp:Label ID="lblId" runat="server" CssClass="captionBlue"></asp:Label>
									<asp:Label ID="lblIdBD" runat="server" CssClass="label"></asp:Label>
								</td>
								<td width="50%">
									<asp:Label ID="lblProveedor" runat="server" CssClass="captionBlue"></asp:Label>
									<asp:Label ID="lblProveedorBD" runat="server" CssClass="label"></asp:Label>
								</td>
							</tr>
							<tr>
								<td>
									<asp:Label ID="lblFechaAnulacion" runat="server" CssClass="captionBlue"></asp:Label>
									<asp:Label ID="lblFechaAnulacionBD" runat="server" CssClass="label"></asp:Label>
								</td>
								<td>
									<asp:Label ID="lblContacto" runat="server" CssClass="captionBlue">lblPeticionario</asp:Label>
									<asp:Label ID="lblContactoBD" runat="server" CssClass="label">lblPeticionario</asp:Label>
								</td>
							</tr>
							<tr>
								<td>
									<asp:Label ID="lblUsuario" runat="server" CssClass="captionBlue">lblContacto</asp:Label>
									<asp:Label ID="lblUsuarioBD" runat="server" CssClass="label">lblContacto</asp:Label>
								</td>
								<td>
									<asp:Label ID="lblNotifInt" runat="server" CssClass="captionBlue">lblPeticionario</asp:Label>
									<asp:Label ID="lblNotifIntBD" runat="server" CssClass="label">lblPeticionario</asp:Label>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<asp:Panel ID="PanelComentarioAnulacion" runat="server">
				<table cellpadding="0" cellspacing="0" border="0" style="width: 99%; padding-left: 2px;
					padding-right: 2px">
					<tr>
						<td style="width: 15%; padding-left: 7px">
							<asp:Label ID="lblComentarios" runat="server" Width="100%" CssClass="captionBlue"
								Text="dComentario de la anulacion:"></asp:Label>
						</td>
					</tr>
					<tr>
						<td style="width: 100%; padding-left: 7px">
							<asp:TextBox onkeypress="return validarLength(this,500)" ID="txtComent" runat="server"
								onkeydown="textCounter(this,500)" onkeyup="textCounter(this,500)" onchange="return validarLength(this,500)"
								TextMode="MultiLine" Style="width: 100%; height: 100px;"></asp:TextBox>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<br />
			<table id="tblBotones" cellspacing="1" cellpadding="1" width="100%" border="0">
				<tr>
					<td style="padding-left: 7px">
						<table>
							<tr>
								<td align="right" style="padding-right: 5px">
									<input class="botonPMWEB" id="cmdAceptar" type="button" value="cmdAceptar" name="cmdAceptar"
										runat="server" />
								</td>
								<td align="left">
									<input class="botonPMWEB" id="cmdCancelar" onclick="IrADetalle()" type="button" value="cmdCancelar"
										name="cmdCancelar" runat="server" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</ContentTemplate>
	</asp:UpdatePanel>
	</form>
</body>
</html>
