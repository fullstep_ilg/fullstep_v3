<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="cierreOK.aspx.vb" Inherits="Fullstep.FSNWeb.cierreOK"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
        <asp:ScriptManager ID="sm" runat="server">
        </asp:ScriptManager>
		<uc1:menu id="Menu1" runat="server" OpcionMenu="Calidad" OpcionSubMenu="NoConformidades"></uc1:menu>
		<table id="Table1" style="z-index: 101; width:100%" border="0">
			<tr>
				<td style="width:100%;">
					<fsn:FSNPageHeader runat="server" ID="FSNPageHeader" UrlImagenCabecera="./images/noconformidades2.gif">
					</fsn:FSNPageHeader>                    
				</td>
			</tr>
			<tr>
				<td class="fondoCabecera">
					<table cellpadding="0" cellspacing="0" border="0" style="width:100%">
						<tr>
							<td style="width:50%; padding-left:10px;">
								<asp:Label ID="lblIdentificador" runat="server" CssClass="captionBlue">lblIdentificador</asp:Label>
								<asp:Label ID="lblIdentificadorBD" runat="server" CssClass="label">lblIdentificador</asp:Label>
							</td>
							
							<td style="width:50%; padding-left:10px;">
								<asp:Label ID="lblProveedor" runat="server" CssClass="captionBlue"></asp:Label>
								<asp:Label ID="lblProveedorBD" runat="server" CssClass="label" style="padding-left:10px;"></asp:Label>                                                                
							</td>                            
						</tr>
						<tr>
							<td style="width:50%; padding-left:10px;">
								<asp:Label ID="lblFecEmision" runat="server" CssClass="captionBlue" ></asp:Label>
								<asp:Label ID="lblFecEmisionBD" runat="server" CssClass="label" style="padding-left:10px;"></asp:Label>
							</td>
							<td style="width:50%; vertical-align:bottom; padding-top:10px; padding-left:10px;">
								<asp:Label ID="lblContacto" runat="server" CssClass="captionBlue">lblContacto</asp:Label>
								<asp:Label ID="lblContactoBD" runat="server" CssClass="label">lblContacto</asp:Label>
							</td>
						</tr>
						<tr>
							<td style="width:50%; padding-left:10px;">
								<asp:Label ID="lblFecCierre" runat="server" CssClass="captionBlue"></asp:Label>
								<asp:Label ID="lblFecCierreBD" runat="server" CssClass="label"></asp:Label>
							</td>
							<td colspan="3" style="width:50%; vertical-align:bottom; padding-top:10px; padding-left:10px;">
								<asp:Label ID="lblNotificadosInternos" runat="server" CssClass="captionBlue">lblNotifInt</asp:Label>
								<asp:Label ID="lblNotificadosInternosBD" runat="server" CssClass="label">lblNotifInt</asp:Label>
							</td>
						</tr>                       
					</table>
				</td>                
			</tr>   
		</table>			
		</form>
	</body>
</html>
