
Public Class cierre
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblIdBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblId As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecCierreBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecCierre As System.Web.UI.WebControls.Label
    Protected WithEvents lblProveBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblProve As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecAltaBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecAlta As System.Web.UI.WebControls.Label
    Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    Protected WithEvents lblNotifProve As System.Web.UI.WebControls.Label
    Protected WithEvents lblNotifProveBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblNotifInt As System.Web.UI.WebControls.Label
    Protected WithEvents lblNotifIntBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblTituloNeg As System.Web.UI.WebControls.Label
    Protected WithEvents cmdAceptar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents NoConformidad As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents hVolver As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cmdCancelar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents txtComent As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents FSNPageHeader As Fullstep.FSNWebControls.FSNPageHeader

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private oNoConformidad As FSNServer.NoConformidad

    ''' <summary>
    ''' Pantalla para cerrar una no conformidad. Puede q no definitivamente pq, de haber revisor, es el revisor 
    ''' quien da el visto bueno a este cierre dado por el usuario conectado.
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>Llamada desde: La propia p�gina, cada vez que se carga
    ''' Tiempo m�ximo:0,1</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sIdi As String = FSNUser.Idioma.ToString()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.CierreNoConformidad

        'Textos de idiomas:
        lblId.Text = Textos(0)
        lblProve.Text = Textos(1)
        lblFecAlta.Text = Textos(2)
        lblFecCierre.Text = Textos(3)
        lblMensaje.Text = Textos(4)
        cmdAceptar.Value = Textos(5)
        cmdCancelar.Value = Textos(6)
        lblNotifProve.Text = Textos(11)

        NoConformidad.Value = Request("NoConformidad")
        hVolver.Value = Request("Volver")
        'Carga los datos de la no conformidad:
        oNoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
        oNoConformidad.Instancia = Request("Instancia")
        oNoConformidad.CargarDatosDesdeInstancia()

        FSNPageHeader.TituloCabecera = Textos(10)
        'Datos de la no conformidad:
        If Request("Positivo") = True Then
            lblTituloNeg.Visible = False
        Else
            lblTituloNeg.Text = Textos(7)
        End If

        lblIdBD.Text = oNoConformidad.Instancia
        lblProveBD.Text = oNoConformidad.Proveedor & " - " & oNoConformidad.ProveDen
        lblFecAltaBD.Text = FormatDate(oNoConformidad.FechaAlta, FSNUser.DateFormat)
        lblFecCierreBD.Text = FormatDate(Now, FSNUser.DateFormat)

        Dim IdsNotificadosProveedor As String = Left(Replace(Request("NotifProv"), ";", ","), Replace(Request("NotifProv"), ";", ",").Length - 1)
        lblNotifProveBD.Text = oNoConformidad.ObtenerNombreNotificadosProveedor(oNoConformidad.Proveedor, IdsNotificadosProveedor)

        If Request("NotifInt") = "" Then
            lblNotifInt.Visible = False
            lblNotifIntBD.Visible = False
        Else
            Dim CodigosNotificadosInternos As String = Left(Replace(Request("NotifInt"), ";", ","), Replace(Request("NotifInt"), ";", ",").Length - 1)

            lblNotifInt.Text = Textos(12)
            lblNotifIntBD.Text = oNoConformidad.ObtenerNombreNotificadosInternos(CodigosNotificadosInternos)
        End If
    End Sub

    ''' <summary>
    ''' Realiza el cerrado de una no conformidad
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>Llamada desde: cmdAceptar
    ''' Tiempo m�ximo:0,1</remarks>
    Private Sub cmdAceptar_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAceptar.ServerClick
        'Cierra la no conformidad:
        Dim bPositivo As Boolean
        Dim sIdi As String = FSNUser.Idioma.ToString()

        If Request("Positivo") = True Then
            bPositivo = True
        Else
            bPositivo = False
        End If

        Dim NotificadosProveedor As String = Replace(Request("NotifProv"), ";", "#")
        Dim NotificadosInternos As String = Replace(Request("NotifInt"), ";", "#")

        oNoConformidad.CierreNoConformidad(bPositivo, FSNUser.Email, VacioToNothing(txtComent.InnerText), , FSNUser.CodPersona, NotificadosProveedor, NotificadosInternos)
        'No ha habido errores, la solicitud pasa a compras:
        If bPositivo = True Then
            Response.Redirect("cierreOK.aspx?CierrePos=1&NoConformidad=" & oNoConformidad.ID & "&Instancia=" & oNoConformidad.Instancia & "&volver=" & hVolver.Value)
        Else
            Response.Redirect("cierreOK.aspx?CierrePos=0&NoConformidad=" & oNoConformidad.ID & "&Instancia=" & oNoConformidad.Instancia & "&volver=" & hVolver.Value)
        End If
    End Sub

    Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Unload
        oNoConformidad = Nothing
    End Sub
End Class

