<%@ Page Language="vb" AutoEventWireup="false" Codebehind="aprobRechazoAccion.aspx.vb" Inherits="Fullstep.FSNWeb.aprobRechazoAccion"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	
	<script>		
		function validarLength(e,l)
		{
			if (e.value.length>l)
				return false
			else
				return true
		}

		function textCounter(e, l) 
		{ 
			if (e.value.length > l) 
				e.value = e.value.substring(0, l); 
		}
		/*''' <summary>
		''' Grabar un Cambio en el estado de una accion
		''' </summary>
		''' <remarks>Llamada desde: cmdAceptar/onclick; Tiempo m�ximo: 0,2</remarks>*/		
		function Aceptar()
		{
		//llama a una funci�n de jsAlta
		var sComentario=document.getElementById("txtComent").value;
		sComentario = sComentario.replace(/^\s*|\s*$/g,"");
		if(sComentario.length > 0 || document.getElementById("Aprobar").value == 0)
		{
			p = window.opener
			
			p.gestionarCambioEstadoAccion(document.getElementById("Aprobar").value,document.getElementById("desglose").value,document.getElementById("index").value,sComentario)
			window.close()
		}
		else
		{
			alert(msjComentario);
		}
		}		
	</script>	
	<body>
		<form id="frmAprobRechazo" method="post" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
			<CompositeScript>
                <Scripts>
                    <asp:ScriptReference Path="../alta/js/jsAlta.js" />
                </Scripts>
            </CompositeScript>
		</asp:ScriptManager>

		<P style="overflow:auto; height:485px;">
		<INPUT id="index" style="Z-INDEX: 106; LEFT: 8px; WIDTH: 128px; POSITION: absolute; TOP: 8px; HEIGHT: 22px"
				type="hidden" size="16" name="index" runat="server">
				<INPUT id="desglose" style="Z-INDEX: 104; LEFT: 8px; WIDTH: 128px; POSITION: absolute; TOP: 8px; HEIGHT: 22px"
				type="hidden" size="16" name="desglose" runat="server">
				<INPUT id="Aprobar" style="Z-INDEX: 103; LEFT: 8px; WIDTH: 128px; POSITION: absolute; TOP: 8px; HEIGHT: 22px"
				type="hidden" size="16" name="Aprobar" runat="server">			
			<TABLE id="tblMensajes" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 0px;"
				height="100%" cellSpacing="1" cellPadding="1" width="97%" border="0">
				<tr>
					<td height="5%">		                
						<asp:label id="lblTitulo" style="TOP: 20px" Width="95%" CssClass="Titulo" runat="server"></asp:label>		        				        
					</td>
				</tr>				
				<TR>
					<TD height="5%"><asp:label id="lblMensaje" Width="100%" CssClass="fntLogin" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD height="75%">
					<TEXTAREA onkeypress="return validarLength(this,500)" id="txtComent" onkeydown="textCounter(this,500)"
						onkeyup="textCounter(this,500)" style="WIDTH: 95%; HEIGHT: 100%" name="txtComentGestor" 
						onchange="return validarLength(this,500)"
						runat="server"></TEXTAREA></TD>
				</TR>
				<TR>
					<TD align="center" height="15%">
						<TABLE id="tblBotones" cellSpacing="1" cellPadding="1" width="300" border="0">
							<TR>
								<TD><INPUT class="botonPMWEB" id="cmdAceptar" onclick="Aceptar()" type="button" value="cmdAceptar"
										name="cmdAceptar" runat="server"></TD>
								<TD><INPUT class="botonPMWEB" id="cmdCancelar" onclick="window.close()" type="button" value="cmdCancelar"
										name="cmdCancelar" runat="server"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>			
		</P>
		</form>
	</body>
</html>
