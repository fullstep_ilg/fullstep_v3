<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ReabrirNoconformidad.aspx.vb" Inherits="Fullstep.FSNWeb.ReabrirNoconformidad"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>		
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<script>
		/*''' <summary>
		''' Nos lleva a la pagina del detalle de la NoConformidad
		''' </summary>
		''' <param name="id">no conformidad a cargar</param>
		''' <remarks>Llamada desde: Page_Load; Tiempo m�ximo: 0</remarks>*/
		function Refrescar(id) {
			var FechaLocal;
			FechaLocal = new Date();
			var otz = FechaLocal.getTimezoneOffset();	
				
			window.open("detalleNoConformidad.aspx?NoConformidad=" + id + "&Otz=" + otz.toString()+ "&volver=<%=Session("VolverdetalleNoconformidad")%>", "_self");
		}
	</script>
	<body>
		<form id="Form1" method="post" runat="server">
		</form>
	</body>
</html>
