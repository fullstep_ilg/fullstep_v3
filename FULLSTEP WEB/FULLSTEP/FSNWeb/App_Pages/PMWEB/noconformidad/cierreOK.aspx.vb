
    Public Class cierreOK
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblIdentificador As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecEmision As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecCierre As System.Web.UI.WebControls.Label
    Protected WithEvents lblProveedor As System.Web.UI.WebControls.Label
    Protected WithEvents lblContacto As System.Web.UI.WebControls.Label
    Protected WithEvents lblNotificadosInternos As System.Web.UI.WebControls.Label
    Protected WithEvents lblIdentificadorBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecEmisionBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecCierreBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblProveedorBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblContactoBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblNotificadosInternosBD As System.Web.UI.WebControls.Label
    Protected WithEvents FSNPageHeader As Fullstep.FSNWebControls.FSNPageHeader
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' <summary>
    ''' Pantalla para informar de q ha ido bien el cierre
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>Llamada desde: La propia p�gina, cada vez que se carga. La usa cierre.aspx
    ''' Tiempo m�ximo:0,1</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oNoConformidad As FSNServer.NoConformidad
        Dim sIdi As String = FSNUser.Idioma.ToString()

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.NoConformidadEmitidaOK

        FSNPageHeader.VisibleBotonVolver = True
        FSNPageHeader.TextoBotonVolver = Textos(12)
        If String.IsNullOrEmpty(Request.QueryString("volver")) Then
            FSNPageHeader.OnClientClickVolver = "window.open('" & ConfigurationManager.AppSettings("rutaQA2008") & "NoConformidades/NoConformidades.aspx" & "','_top');return false;"
        Else
            FSNPageHeader.OnClientClickVolver = "window.open('" & Replace(Request.QueryString("volver"), "*", "&") & "','_top');return false;"
        End If
        'Carga los datos de la no conformidad:
        oNoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
        oNoConformidad.Instancia = Request("Instancia")
        oNoConformidad.CargarDatosDesdeInstancia()

        'Datos de la instancia:
        lblIdentificador.Text = Textos(1)
        lblIdentificadorBD.Text = oNoConformidad.Instancia
        lblFecEmision.Text = Textos(3)
        lblFecEmisionBD.Text = FormatDate(oNoConformidad.FechaAlta, FSNUser.DateFormat)
        lblProveedor.Text = Textos(4)
        lblProveedorBD.Text = oNoConformidad.Proveedor & " - " & oNoConformidad.ProveDen
        lblFecCierre.Text = Textos(6)
        lblFecCierreBD.Text = FormatDate(Now, FSNUser.DateFormat)

        If Request("CierrePos") = 1 Then
            Select Case oNoConformidad.Estado
                Case TipoEstadoNoConformidad.CierrePosEnPlazo
                    FSNPageHeader.TituloCabecera = Textos(8)
                Case TipoEstadoNoConformidad.CierrePosFueraPlazo
                    FSNPageHeader.TituloCabecera = Textos(9)
                Case TipoEstadoNoConformidad.CierrePosPendiente
                    FSNPageHeader.TituloCabecera = Textos(13)
            End Select
        Else
            FSNPageHeader.TituloCabecera = Textos(7)
        End If

        Me.lblContacto.Text = Textos(5)
        Me.lblContactoBD.Text = oNoConformidad.NotificadosProveedor

        If oNoConformidad.NotificadosInternos Is Nothing Then
            lblNotificadosInternos.Visible = False
            Me.lblNotificadosInternosBD.Visible = False
        Else
            lblNotificadosInternos.Text = Textos(11)
            Me.lblNotificadosInternosBD.Text = oNoConformidad.NotificadosInternos
        End If
        oNoConformidad = Nothing
    End Sub

    End Class

