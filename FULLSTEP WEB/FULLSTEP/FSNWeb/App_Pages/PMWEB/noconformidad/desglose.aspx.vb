
Public Class noconfdesglose
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblSubTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents INPUTDESGLOSE As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblTituloData As System.Web.UI.WebControls.Label
    Protected WithEvents cmdGuardar As System.Web.UI.WebControls.Button
    Protected WithEvents cmdCalcular As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents Instancia As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Solicitud As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' <summary>
    ''' Cargar un desglose dado. Parece q tiene un problemilla con el input hiden de esta pantalla 
    ''' llamado Solicitud y el parametro que se le pasa a esta pantalla tambien llamado Solicitud.
    ''' Cuando el parametro es nothing mete en el hidden el string vacio y pasarle a un integer el 
    ''' string vacio (Dim lSolicitud As Integer = Request("Solicitud")) da un casque.
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">evento de sistema</param>     
    ''' <returns>Nada</returns>
    ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim idCampo As Integer = Request("campo")
        Dim sInput As String = Request("Input")
        Dim lInstancia As Integer = Request("instancia")
        Dim lSolicitud As Integer
        If IsNumeric(Request("Solicitud")) Then lSolicitud = Request("Solicitud")

        INPUTDESGLOSE.Value = sInput
        Instancia.Value = lInstancia

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Desglose

        Dim oCampo As FSNServer.Campo
        Dim lVersion As Long = Request("Version")
        Dim oInstancia As FSNServer.Instancia
        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = lInstancia
        oInstancia.Version = lVersion

        oCampo = FSNServer.Get_Object(GetType(FSNServer.Campo))
        oCampo.Id = idCampo

        If Request("Solicitud") <> Nothing Then
            Me.Solicitud.Value = lSolicitud
            oCampo.Load(Idioma, lSolicitud)
        Else
            oCampo.LoadInst(lInstancia, Idioma)
        End If

        lblTitulo.Text = Textos(0)
        lblTituloData.Text = oCampo.DenSolicitud(Idioma)

        Dim bSoloLectura As Boolean = (Request("SoloLectura") = "1")
        Dim oucDesglose As desgloseControl = Me.FindControl("ucDesglose")

        'En jsAlta.js hay para las funciones de a�adir linea y copia linea
        '       $create(AjaxControlToolkit.AutoCompleteBehavior ...rutaPM...
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ruta", "<script>var ruta = '" & ConfigurationManager.AppSettings("ruta") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")
        'para el buscador de art�culos
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM2008", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "version") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "version", _
                "<script>var version = '" & System.Configuration.ConfigurationManager.AppSettings("version") & "';</script>")
        End If

        Me.FindControl("frmAlta").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())

        cmdGuardar.Text = Textos(2)
        cmdGuardar.Attributes.Add("onclick", "return actualizarCampoDesgloseYCierre()")
        cmdCalcular.Value = Textos(3)

        Dim oNoConformidad As FSNServer.NoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
        oNoConformidad.ID = Request("NoConformidad")
        oNoConformidad.Version = lVersion
        oNoConformidad.Load(Idioma, lVersion)

        If oNoConformidad.Estado = TipoEstadoNoConformidad.CierreNegativo Or oNoConformidad.Estado = TipoEstadoNoConformidad.CierrePosEnPlazo Or oNoConformidad.Estado = TipoEstadoNoConformidad.CierrePosFueraPlazo Then
            cmdGuardar.Visible = False
        End If

        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.DetalleNoConformidad
        Dim sTituloAcciones(5) As String

        sTituloAcciones(2) = Textos(24)  'com.
        sTituloAcciones(3) = Textos(25)  'Estado interno
        sTituloAcciones(4) = Textos(26)  'Aprobar
        sTituloAcciones(5) = Textos(27)  'Rechazar

        Dim oAcciones As DataTable
        Dim oDSEstados As DataSet
        If lInstancia > 0 AndAlso oCampo.Tipo = TipoCampoPredefinido.NoConformidad Then
            Dim NoMostrarBotonesAcciones As Boolean = True
            If (oNoConformidad.PeticionarioCod = FSNUser.CodPersona) AndAlso (oNoConformidad.Estado = TipoEstadoNoConformidad.Guardada Or oNoConformidad.Estado = TipoEstadoNoConformidad.Abierta) Then NoMostrarBotonesAcciones = False
            oAcciones = oNoConformidad.CargarBotonesAprobarRechazarAcciones(sTituloAcciones, NoMostrarBotonesAcciones)
            oDSEstados = oNoConformidad.CargarEstadosComentariosInternosAcciones(Idioma, FSNUser.Cod, sTituloAcciones)
        End If

        oucDesglose = Me.FindControl("ucDesglose")
        With oucDesglose
            .Acciones = oAcciones
            .DSEstados = oDSEstados
            .Version = lVersion
            .TipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad
            If oCampo.DenGrupo(Idioma) = oCampo.Den(Idioma) Then
                .Titulo = oCampo.DenGrupo(Idioma)
            Else
                .Titulo = oCampo.DenGrupo(Idioma) + ";" + oCampo.Den(Idioma)
            End If
        End With
    End Sub
    Private Function rellenarAccionesCertificado() As DataTable
        Dim dt As DataTable = modUtilidades.GenerarDataTableAcciones
        Return dt
    End Function
End Class