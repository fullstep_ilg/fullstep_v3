<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="cierre.aspx.vb" Inherits="Fullstep.FSNWeb.cierre"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<script>
		/*''' <summary>
		''' Volver al detalle por cancelaci�n de cierre
		''' </summary>
		''' <remarks>Llamada desde: cmdCancelar.onclick; Tiempo m�ximo: 0 </remarks>*/				
		function IrADetalle()
		{
			var FechaLocal;
			FechaLocal = new Date();
			var otz = FechaLocal.getTimezoneOffset();		
		
			IdNoConf=document.getElementById("NoConformidad").value  
			window.open("detalleNoConformidad.aspx?NoConformidad=" + IdNoConf  + "&Otz=" + otz.toString() + "&Volver=<%=Request("Volver")%>" ,"_self")
		}
		
		function validarLength(e,l)
		{
			if (e.value.length>l)
				return false
			else
				return true
		}

		function textCounter(e, l) 
		{ 
			if (e.value.length > l) 
				e.value = e.value.substring(0, l); 
		} 
		
	</script>	
	<body>
		<form id="Form1" method="post" runat="server">
        <asp:ScriptManager ID="sm" runat="server"></asp:ScriptManager>
		<uc1:menu ID="Menu1" runat="server" OpcionMenu="Calidad" OpcionSubMenu="NoConformidades"></uc1:menu>			
		<input id="NoConformidad" type="hidden" size="16" name="NoConformidad" runat="server"/>
        <input id="hVolver" type="hidden" size="16" name="hVolver" runat="server"/>
        <fsn:FSNPageHeader runat="server" ID="FSNPageHeader" UrlImagenCabecera="./images/noconformidades2.gif"></fsn:FSNPageHeader>
		<table id="tblMensajes" style="z-index: 103; width:100%;" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td class="fondoCabecera" style="padding-left:10px;">
					<table class="bordeadoAzul" id="tblCabecera" style="z-index: 102; width:100%;" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td colspan="4">
								<asp:Label ID="lblTituloNeg" runat="server" CssClass="TituloSinDatos" Width="30%">lblTituloNeg</asp:Label>                                
							</td>
						</tr>
						<tr>
							<td width="7%">
								<asp:Label ID="lblId" runat="server" CssClass="captionBlue" Width="100%"></asp:Label>
							</td>
							<td width="20%">
								<asp:Label ID="lblIdBD" runat="server" CssClass="label" Width="100%"></asp:Label>
							</td>
							<td width="7%">
								<asp:Label ID="lblProve" runat="server" CssClass="captionBlue" Width="100%"></asp:Label>
							</td>
							<td width="20%">
								<asp:Label ID="lblProveBD" runat="server" CssClass="label" Width="100%"></asp:Label>
							</td>                            
						</tr>
						<tr>
							<td width="7%">
								<asp:Label ID="lblFecAlta" runat="server" CssClass="captionBlue" Width="100%"></asp:Label>
							</td>
							<td width="20%">
								<asp:Label ID="lblFecAltaBD" runat="server" CssClass="label" Width="100%"></asp:Label>
							</td>
							<td width="7%">
								<asp:Label ID="lblNotifProve" runat="server" CssClass="captionBlue" Width="100%"></asp:Label>
							</td>
							<td width="20%">
								<asp:Label ID="lblNotifProveBD" runat="server" CssClass="label" Width="100%"></asp:Label>
							</td> 
						</tr>
						<tr>
							<td width="7%">
								<asp:Label ID="lblFecCierre" runat="server" CssClass="captionBlue" Width="100%"></asp:Label>
							</td>
							<td width="20%">
								<asp:Label ID="lblFecCierreBD" runat="server" CssClass="label" Width="100%"></asp:Label>
							</td>
							<td width="7%">
								<asp:Label ID="lblNotifInt" runat="server" CssClass="captionBlue" Width="100%"></asp:Label>
							</td>
							<td width="20%">
								<asp:Label ID="lblNotifIntBD" runat="server" CssClass="label" Width="100%"></asp:Label>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="padding-left:10px;">
					<asp:Label ID="lblMensaje" runat="server" CssClass="fntLogin" Width="100%"></asp:Label>
				</td>
			</tr>
			<tr>
				<td style="height:250px; padding-left:10px;">
					<textarea onkeypress="return validarLength(this,500)" id="txtComent" onkeydown="textCounter(this,500)"
						onkeyup="textCounter(this,500)" style="width: 95%; height: 100%" name="txtComentGestor"
						onchange="return validarLength(this,500)" runat="server"></textarea>
				</td>
			</tr>
			<tr>
				<td align="center" height="15%">
					<table id="tblBotones" cellspacing="1" cellpadding="1" width="300" border="0">
						<tr>
							<td>
								<input class="botonPMWEB" id="cmdAceptar" type="button" value="cmdAceptar" runat="server">
							</td>
							<td>
								<input class="botonPMWEB" id="cmdCancelar" onclick="IrADetalle()" type="button" value="cmdCancelar"
									runat="server">
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		</form>
	</body>
</html>
