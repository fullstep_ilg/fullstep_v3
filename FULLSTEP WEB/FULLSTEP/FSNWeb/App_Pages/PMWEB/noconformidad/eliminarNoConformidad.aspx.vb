
    Public Class eliminarNoConformidad
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
   "<script language=""{0}"">{1}</script>"

    ''' <summary>
    ''' Elimina la versi�n de una no conformidad
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>Llamada desde: La propia p�gina, cada vez que se carga
    ''' Tiempo m�ximo:0,3</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Elimina la versi�n de la no conformidad
        Dim oNoConformidad As FSNServer.NoConformidad
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oUser As FSNServer.User = Session("FSN_User")

        oNoConformidad = FSWSServer.Get_Object(GetType(FSNServer.NoConformidad))
        With oNoConformidad
            .ID = Request("NoConformidad")

            .Instancia = Request("Instancia")
            .Version = Request("Version")
            .Workflow = Request("Workflow")
            .Estado = Request("Estado")
            .Es_Noconformidad_Escalacion = (Request("EsNoConfEscalacion") = 1)
        End With

        oNoConformidad.EliminarVersionesNoConformidad(oUser.CodPersona)

        If oNoConformidad.Version = 1 Or (oNoConformidad.Workflow > 0 AndAlso (oNoConformidad.Estado = TipoEstadoNoConformidad.EnCursoDeAprobacion OrElse oNoConformidad.Estado = TipoEstadoNoConformidad.Guardada)) Then
            'Si es una no conformidad sin flujo o una no conformidad con flujo en curso de aprobaci�n se elimina la no conformidad
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>Refrescar(null)</script>")
        Else
            Page.ClientScript.RegisterStartupScript(Me.GetType(),"claveStartUpScript", "<script>Refrescar(" & oNoConformidad.ID & ")</script>")
        End If
        oNoConformidad = Nothing
    End Sub
    End Class

