﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="altaNoconformidad.aspx.vb" Inherits="Fullstep.FSNWeb.altaNoconformidad" %>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Register TagPrefix="fsde" Namespace="Fullstep.DataEntry" Assembly="DataEntry" %>
<%@ Register Assembly="Infragistics.WebUI.UltraWebTab.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<%@ Register Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebNavigator" TagPrefix="ignav" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>" lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title>altaNoconformidad</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
</head>
<script type="text/javascript">
    wProgreso = null;
    CreateXmlHttp();
    //Muestra el contenido peligroso que ha efectuado un error al guardar.
    function ErrorValidacion(contenido) {
        alert(arrTextosML[6].replace("$$$", contenido));
        HabilitarBotones(true);
    };
    /*''' <summary>
    ''' Crear el objeto para llamar con ajax a ComprobarVersionQa
    ''' </summary>
    ''' <remarks>Llamada desde: javascript ; Tiempo máximo: 0</remarks>*/
    function CreateXmlHttp() {
        // Probamos con IE
        try {
            // Funcionará para JavaScript 5.0
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (oc) {
                xmlHttp = null;
            }
        }
        // Si no se trataba de un IE, probamos con esto
        if (!xmlHttp && typeof XMLHttpRequest != "undefined") xmlHttp = new XMLHttpRequest();

        return xmlHttp;
    };
    /*''' <summary>
    ''' Redimensiona la anchura del desglose
    ''' </summary>      
    ''' <remarks>Llamada desde=init(); Tiempo máximo=0seg.</remarks>*/
    function resize() {
        for (i = 0; i < arrDesgloses.length; i++) {
            sDiv = arrDesgloses[i].replace("tblDesglose", "divDesglose")
            if (document.getElementById(sDiv)) document.getElementById(sDiv).style.width = parseFloat(document.body.offsetWidth) - 80 + 'px';
        }
    };
    function init() {
        resize();
        OcultarEspera();
    };
    /*''' <summary>
    ''' Realiza el envio o guardado de una no conformidad
    ''' </summary>
    ''' <param name="bEmitir">emites(1) o guardas (0)</param>       
    ''' <param name="bMapper">Si hay validacion de mapper o no</param>
    ''' <param=id, bloque, comp_obl, guarda, rechazo>Para cuando se pulsa una acción</param>
    ''' <returns>False, si falta algún dato necesario.</returns>
    ''' <remarks>Llamada desde:cmdEmitir	cmdGuardar  accion; Tiempo máximo: 0</remarks>*/
    function Enviar(bEmitir, bMapper, guardaConFlujo, idAccion, bloque, comp_obl, guarda, rechazo) {
    	var respOblig;
        if (!idAccion) {
            if (document.getElementById("lnkBotonEmitir"))
                if (document.getElementById("lnkBotonEmitir").disabled) return false;

            if (document.getElementById("lnkBotonGuardar"))
                if (document.getElementById("lnkBotonGuardar").disabled) return false;
        }

        bMensajePorMostrar = document.getElementById("bMensajePorMostrar");
        if (bMensajePorMostrar) {
            if (bMensajePorMostrar.value == "1") {
                bMensajePorMostrar.value = "0";
                return false;
            }
        };

        //Si es una NoConf de escalacion pero no viene del panel de escalaciones (no es una aprobacion). Es un alta normal desde el visor de no conformidades		        
        if (esNoconfEscalacion && idEscalacionProve == 0) //Comprobamos que no hay NCE en UNQAs descendientes 
            //(no ponemos en la misma linea del if para llamar al webmethod solo si se dan las dos primeras condiciones)
            if (ErrorValidacionesNoConformidadEscalacion()) return false;

        //Comprueba que se hayan rellenado todos los campos obligatorios. Solo cuando envias.
        if (bEmitir == true || idAccion) {
            respOblig = comprobarObligatorios();
            switch (respOblig) {
                case "":  //no falta ningun campo obligatorio
                    break;
                case "filas0": //no se han introducido filas en un desglose obligatorio
                    alert(arrTextosML[0]);
                    return false;
                    break;
                default: //falta algun campo obligatorio
                    alert(arrTextosML[0] + '\n' + respOblig);
                    return false;
                    break;
            };
        };
        
        //Comprueba que se haya introducido el proveedor
        if (document.getElementById("Proveedor").value == "") {
            alert(arrTextosML[1]);
            return false;
        };

        //Comprueba que se haya introducido al menos un contacto de proveedor
        var wDropDownNotificadosProveedor = $find("wddNotificadosProveedor");
        if (wDropDownNotificadosProveedor.get_selectedItems().length == 0) {
            alert(arrTextosML[4]);
            return false;
        };

        //Comprueba que se haya introducido un unidad qa
        if (document.getElementById("ComboQA").value == 1) {
            var wDropDownUnidadQA = $find($('[id$=wddUnidadQA]').attr('id'));
            if (wDropDownUnidadQA.get_selectedItems().length == 0) {
                alert(arrTextosML[5]);
                return false;
            } else {
                if (wDropDownUnidadQA.get_selectedItems()[0].get_value() == '') {
                    alert(arrTextosML[5]);
                    return false;
                }
            }
        };

        if (bEmitir == true || idAccion) { //si va a emitir comprueba que se haya introducido la fecha limite de resolución:			
            oEntry = fsGeneralEntry_getById("fsentryNoConfFecDesde");
            if (oEntry.getValue() == null) {
                alert(arrTextosML[2]);
                return false;
            }
        };

        //Comprobar que las fechas de inicio y fin de las acciones no sobrepasan el limite correspondiente
        if (validar_fechas_acciones() == false) {
            alert(arrTextosML[3]);
            return false;
        };

        if (!guardaConFlujo && !rechazo) {
        	respOblig=comprobarParticipantes();
        	if (respOblig !== '') {
        		alert(arrTextosML[0] + '\n' + respOblig);
        		return false;
        	};
        };

        if (comprobarTextoLargo() == false) return false;
              
        HabilitarBotones(false);

        if (wProgreso == null) {
            wProgreso = true;
            MostrarEspera();
        };

        if (idAccion) {
            EjecutarAccion(idAccion, bloque, comp_obl, guarda, rechazo);
        } else {
            if (bMapper == true) MontarFormularioSubmit(false, false, true);
            else MontarFormularioSubmit();
        }

        if (bEmitir == true) document.forms["frmSubmit"].elements["GEN_Enviar"].value = 1;
        else document.forms["frmSubmit"].elements["GEN_Enviar"].value = 0;
        document.forms["frmSubmit"].elements["GEN_Instancia"].value = document.getElementById("NEW_Instancia").value;
        document.forms["frmSubmit"].elements["GEN_NoConformidad"].value = document.getElementById("NoConformidad").value;
        
        if (!idAccion) {
            if (guardaConFlujo == true || guardaConFlujo == "true") {
                document.forms["frmSubmit"].elements["GEN_Accion"].value = "guardarsolicitud";
                document.forms["frmSubmit"].elements["GEN_Guardar"].value = 1;
                document.forms["frmSubmit"].elements["GEN_Bloque"].value = bloque;
                document.forms["frmSubmit"].elements["GEN_Rol"].value = oRol_Id;
            } else {
                document.forms["frmSubmit"].elements["GEN_Accion"].value = "emitirnoconformidad";
            }
            document.forms["frmSubmit"].elements["PantallaMaper"].value = bMapper;
        }
        document.forms["frmSubmit"].elements["GEN_NoConfProve"].value = document.getElementById("Proveedor").value;
        if ($get("divProveERP").style.display != "none") {
            var owddProveERP = $find("wddProveERP");
            if (owddProveERP.get_selectedItems().length > 0) {
                document.forms["frmSubmit"].elements["GEN_NoConfProveERP"].value = owddProveERP.get_selectedItems()[0].get_value();
                //al ppio de la denominación va el código
                document.forms["frmSubmit"].elements["GEN_NoConfProveERPDen"].value = owddProveERP.get_selectedItems()[0].get_text().replace(owddProveERP.get_selectedItems()[0].get_value() + " - ", "");
            }
        }
        if (idEscalacionProve)
            document.forms["frmSubmit"].elements["GEN_NoConfProveDEN"].value = document.getElementById("lblDenProveedor").innerText;
        else
            document.forms["frmSubmit"].elements["GEN_NoConfProveDEN"].value = document.getElementById("txtProveedor").value;
        if (idEscalacionProve) document.forms["frmSubmit"].elements["GEN_IdEscalacionProve"].value = idEscalacionProve;
        document.forms["frmSubmit"].elements["GEN_NivelEscalacion"].value = nivelEscalacion;
        document.forms["frmSubmit"].elements["GEN_EsNoconfEscalacion"].value = esNoconfEscalacion;
        //Contactos/Notificados del proveedor
        var cadenaIdsContactoProveedor = ""
        for (j = 0; j < wDropDownNotificadosProveedor.get_selectedItems().length; j++) {
            cadenaIdsContactoProveedor = cadenaIdsContactoProveedor + wDropDownNotificadosProveedor.get_selectedItems()[j].get_value() + "#";
        }
        document.forms["frmSubmit"].elements["GEN_NoConfContactosProveedor"].value = cadenaIdsContactoProveedor;

        //Notificados internos, del departamento o unidad de negocio
        var wDropDownNotificadosInternos = $find("wddNotificadosInternos");
        var cadenaIdsNotificadosInternos = "";
        for (j = 0; j < wDropDownNotificadosInternos.get_selectedItems().length; j++) {
            cadenaIdsNotificadosInternos = cadenaIdsNotificadosInternos + wDropDownNotificadosInternos.get_selectedItems()[j].get_value() + "#";
        }
        document.forms["frmSubmit"].elements["GEN_NoConfContactosInternos"].value = cadenaIdsNotificadosInternos;

        if (document.getElementById("ComboQA").value == 1) {
            var wDropDownUnidadQA = $find($('[id$=wddUnidadQA]').attr('id'));
            document.forms["frmSubmit"].elements["GEN_NoConfUnidadQa"].value = wDropDownUnidadQA.get_selectedItems()[0].get_value();
        } else document.forms["frmSubmit"].elements["GEN_NoConfUnidadQa"].value = document.getElementById("UnicoUnidadQA").value;

        oEntry = document.getElementById("AccesoRevisor");
        if (oEntry.value == 1) {
            var wDropDownRevisor = $find($('[id$=wddRevisor]').attr('id'));
            document.forms["frmSubmit"].elements["GEN_NoConfRevisor"].value = wDropDownRevisor.get_selectedItems()[0].get_value();

            if (document.getElementById("CodRevisorAnterior").value == document.forms["frmSubmit"].elements["GEN_NoConfRevisor"].value)
                document.forms["frmSubmit"].elements["GEN_CambioRevisor"].value = 0;
            else
                document.forms["frmSubmit"].elements["GEN_CambioRevisor"].value = 1;
        } else document.forms["frmSubmit"].elements["GEN_NoConfRevisor"].value = null;

        oEntry = fsGeneralEntry_getById("fsentryNoConfFecDesde");
        var d = oEntry.getValue();
        if (d != null) {
            document.forms["frmSubmit"].elements["GEN_DespubDia"].value = d.getDate();
            document.forms["frmSubmit"].elements["GEN_DespubMes"].value = d.getMonth() + 1;
            document.forms["frmSubmit"].elements["GEN_DespubAnyo"].value = d.getFullYear();
        }
        oEntry = fsGeneralEntry_getById("fsentryFecApliMejoras");
        var FecApliPlan = oEntry.getValue();
        if (FecApliPlan != null) {
            document.forms["frmSubmit"].elements["GEN_FecApliPlanDia"].value = FecApliPlan.getDate();
            document.forms["frmSubmit"].elements["GEN_FecApliPlanMes"].value = FecApliPlan.getMonth() + 1;
            document.forms["frmSubmit"].elements["GEN_FecApliPlanAnyo"].value = FecApliPlan.getFullYear();
        }
        document.forms["frmSubmit"].elements["GEN_NoConfComentAlta"].value = document.getElementById("txtComent").value;

        var oFrm = MontarFormularioCalculados();
        var sInner = oFrm.innerHTML;
        oFrm.innerHTML = "";
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner);
        oFrm = null;
        sInner = null;
        document.forms["frmSubmit"].submit();
        return false;
    };
    //Descripción: Cuando se pulsa un botón para realizar una acción, se llama a esta función
    //Paramátros: id: id de la acción
    // bloque: id del bloque
    // comp_olb: si hay que comprobar los campos de obligatorios o no
    // guarda: si hay que guardar o versión o no
    // rechazo: indica si la accion implica un rechazo, ya sea temporal o definitivo
    // LLamada desde: Enviar
    function EjecutarAccion(id, bloque, comp_obl, guarda, rechazo) {
    	if (rechazo == false || rechazo == "false") {
    		var respOblig = comprobarParticipantes();
            if (respOblig !== '') {
            	alert(arrTextosML[0] + '\n' + respOblig);
                return false;
            }
        }
        var frmAltaElements = document.forms["frmAlta"].elements;
        var HayMapper = false
        if (frmAltaElements["PantallaMaper"].value == "True")
            HayMapper = true

        MontarFormularioSubmit(guarda, true, HayMapper, false)

        var frmSubmitElements = document.forms["frmSubmit"].elements;
        frmSubmitElements["GEN_AccionRol"].value = id;
        frmSubmitElements["GEN_Bloque"].value = bloque;
        
        frmSubmitElements["GEN_Rol"].value = oRol_Id;
        frmSubmitElements["PantallaMaper"].value = HayMapper;
        frmSubmitElements["DeDonde"].value = "Guardar";

        frmSubmitElements = null;
        frmAltaElements = null;
        HayMapper = null;
    }
    function ErrorValidacionesNoConformidadEscalacion() {
        var iUNQA;
        if (document.getElementById("ComboQA").value == 1) {
            var wDropDownUnidadQA = $find($('[id$=wddUnidadQA]').attr('id'));
            iUNQA = wDropDownUnidadQA.get_selectedItems()[0].get_value();
        } else iUNQA = document.getElementById("UnicoUnidadQA").value;
        var errorValidacion = false;
        $.when($.ajax({
            type: "POST",
            url: rutaPM + 'noconformidad/altaNoconformidad.aspx/Validaciones_NoConformidad_Escalacion',
            data: JSON.stringify({ Solicitud: $('#Solicitud').val(), Proveedor: $('#Proveedor').val(), UNQA: iUNQA }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false
        })).done(function (msg) {
            var datosValidacion = $.parseJSON(msg.d);
            if (datosValidacion.resultadoValidacion == 0) errorValidacion = false;
            else {
                var textoSustituir;
                switch (datosValidacion.resultadoValidacion) {
                    case 1:
                        textoSustituir = datosValidacion.tipoNoConformidad + ' - ' + datosValidacion.idNoConformidad + ' - ' + datosValidacion.UNQA + ' - ' + datosValidacion.estadoNoConformidad;
                        break;
                    case 2:
                        textoSustituir = datosValidacion.nivelEscalacion + ' - ' + datosValidacion.UNQA + ' - ' + datosValidacion.estadoEscalacion;
                        break;
                    default:
                        textoSustituir = datosValidacion.nivel_UNQA;
                        break;
                }
                alert(TextoErrorValidacionNoconformidadEscalacion[datosValidacion.resultadoValidacion].replace('###', textoSustituir));
                errorValidacion = true;
            }
        });
        return errorValidacion;
    };
    function validarLength(e, l) {
        if (e.value.length > l) return false;
        else return true;
    };
    function textCounter(e, l) {
        if (e.value.length > l) e.value = e.value.substring(0, l);
    }
    /*''' <summary>
    ''' Vuelve a la pagina anterior
    ''' </summary>
    ''' <remarks>Llamada desde: Cuando se pincha al enlace ("Volver"); Tiempo máximo:0,1</remarks>*/
    function HistoryHaciaAtras() {
        //Evitar los enter q hacen q el 1er botón de FSPMPageHeader se ejecute
        if (esIntro == true) {
            esIntro = false;
            return false;
        }

        if (document.getElementById("lnkBotonVolver"))
            if (document.getElementById("lnkBotonVolver").disabled)
                return false;

        valor = document.getElementById("desdeInicio");
        if (valor && valor.value == 1) window.open(rutaFS + 'Inicio.aspx', '_top');
        else if (esNoconfEscalacion && idEscalacionProve !== 0) window.open(valor.value, '_top');
        else window.open(rutaFS + 'QA/NoConformidades/NoConformidades.aspx', '_top');
    };
    function CalcularCamposCalculados() {
        if (document.getElementById("lnkBotonCalcular"))
            if (document.getElementById("lnkBotonCalcular").disabled)
                return false;

        oFrm = MontarFormularioCalculados();
        oFrm.submit();
        return false;
    };
    /*''' <summary>
    ''' Tras un guardado no se hace ni submit ni se va a otra pantalla. Eso implica q haya datos q no estan 
    '''	actualizados en pantalla pero si en bbdd. Esta función los actualiza en pantalla y en los input hidden.
    '''	Despues habilita los botones y oculta la ventana de espera.
    ''' </summary>
    ''' <param name="lInstancia">Instancia de No Conformidad</param>
    ''' <param name="lNoConformidad">id de No Conformidad</param>        
    ''' <param name="sProveedor">Proveedor de No Conformidad</param>
    ''' <param name="sContacto">Contacto de No Conformidad</param> 
    ''' <param name="sContactoDen">Denominación del Contacto de No Conformidad</param>		
    ''' <param name="sFecAlta">Fecha de Alta de No Conformidad</param>
    ''' <param name="ListaCalc">Lista de pares que contiene los Id de los calculados y su valor ya calculado</param> 		
    ''' <remarks>Llamada desde: GuardarInstancia.aspx; Tiempo máximo:0,1</remarks>*/
    function PonerIdyEstado(lInstancia, lNoConformidad, sProveedor, sContacto, sContactoDen, sFecAlta, ListaCalc, CodigoProveedor) {
        document.getElementById("divTabla1").style.display = 'none';
        document.getElementById("divTabla2").style.display = 'inline';
        document.getElementById("lblInfoNoConformidad").innerHTML = CodigoProveedor + ' - ' + sProveedor + '          (' + sFecAlta + ')';
        document.getElementById("lblCabecera").innerHTML = lInstancia + ' - ' + document.getElementById("DenominacionSolicitud").value;
        document.getElementById("NEW_Instancia").value = lInstancia;
        document.getElementById("NoConformidad").value = lNoConformidad;
        oEntry = document.getElementById("AccesoRevisor");
        if (oEntry.value == 1) {
            var wDropDownRevisor = $find($('[id$=wddRevisor]').attr('id'));
            document.getElementById("CodRevisorAnterior").value = wDropDownRevisor.get_selectedItems()[0].get_value();
        };

        var ParListaCalc = "";
        var ListaCalcId = "";
        var ListaCalcVal = "";
        while (ListaCalc != "") {
            ParListaCalc = ListaCalc.substring(0, ListaCalc.indexOf("#"));

            ListaCalcId = ParListaCalc.substring(0, ParListaCalc.indexOf("@"));
            ListaCalcVal = ParListaCalc.substring(ParListaCalc.indexOf("@") + 1);

            if (document.getElementById(ListaCalcId)) document.getElementById(ListaCalcId).value = ListaCalcVal;

            if (ListaCalc.indexOf("#")) ListaCalc = ListaCalc.substring(ListaCalc.indexOf("#") + 1);
        }
        HabilitarBotones(true);
        wProgreso = null;
    };
    /* Descripcion: Visualiza la opcion Eliminar en la cabecera. Oculta la ventana de espera.
    Llamada desde:PonerIdyEstado;
    Tiempo ejecucion:=0seg.*/
    function HabilitarBotones(opcion) {
        var deshabilitar = !opcion;

        if (document.getElementById("lnkBotonEmitir")) document.getElementById("lnkBotonEmitir").disabled = deshabilitar;
        if (document.getElementById("lnkBotonGuardar")) document.getElementById("lnkBotonGuardar").disabled = deshabilitar;
        if (document.getElementById("lnkBotonEliminar")) document.getElementById("lnkBotonEliminar").disabled = deshabilitar;
        if (document.getElementById("lnkBotonCalcular")) document.getElementById("lnkBotonCalcular").disabled = deshabilitar;
        if (document.getElementById("lnkBotonVolver")) document.getElementById("lnkBotonVolver").disabled = deshabilitar;

        if (deshabilitar) {
            //Mostrar el boton de cabecera Eliminar
            if (document.getElementById("Eliminar"))
                document.getElementById("Eliminar").style.display = 'inline';
        }

        if (arrObligatorios.length > 0)
            if (document.getElementById("lblCamposObligatorios"))
                document.getElementById("lblCamposObligatorios").style.display = ""

        OcultarEspera();
    };
    //Estaba habilitando los botones antes de ocultar el progreso. Parecia q te dejaba dar a varios botones mientras estaba en progreso.
    function DarTiempoAOcultarProgreso() {
        $('[id*=lnkBoton]').removeAttr('disabled');
    };
    /*Descripcion: Oculta el div de espera y muestra los tabs de grupos
    Llamada desde= onunload() // HabilitarBotones()     //Init()
    Tiempo ejecucion:=0,1seg.*/
    function OcultarEspera() {
        wProgreso = null;

        document.getElementById("divProgreso").style.display = 'none';
        document.getElementById("divForm2").style.display = '';
        document.getElementById("divForm3").style.display = '';

        setTimeout('DarTiempoAOcultarProgreso()', 250);

        i = 0;
        bSalir = false;
        while (bSalir == false) {
            if (document.getElementById("uwtGrupos_div" + i)) {
                document.getElementById("uwtGrupos_div" + i).style.visibility = 'visible';
                i = i + 1;
            } else bSalir = true;
        }
    };
    /*Descripcion: Muestra el div de espera y oculta los tabs de grupos
    Llamada desde= Enviar
    Tiempo ejecucion:=0,1seg. */
    function MostrarEspera() {
        wProgreso = true;

        $("[id*='lnkBoton']").attr('disabled', 'disabled');

        document.getElementById("lblCamposObligatorios").style.display = "none"
        document.getElementById("divForm2").style.display = 'none';
        document.getElementById("divForm3").style.display = 'none';

        i = 0;
        bSalir = false;
        while (bSalir == false) {
            if (document.getElementById("uwtGrupos_div" + i)) {
                document.getElementById("uwtGrupos_div" + i).style.visibility = 'hidden';
                i = i + 1;
            } else bSalir = true;
        }
        document.getElementById("lblProgreso").value = frmAlta.cadenaespera.value;
        document.getElementById("divProgreso").style.display = '';
    }
    /*''' <summary>
    ''' Mostrar una pantalla de selección de proveedores.
    ''' </summary>
    ''' <remarks>Llamada desde: imgBuscarProveedor; Tiempo máximo: 0</remarks>*/
    function BuscarProveedor() {
        var newWindow = window.open(rutaFS + '_common/BuscadorProveedores.aspx?Valor=&IDControl=txtProveedor&IDDepen=&Campo=Proveedor&NoConf=1&ValorOrgCompras=&Visibilidad=&Mat=&PM=false&desde=altaNoConformidad', '_blank', 'width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes');
    };
    function BuscarNotificadoInterno() {
        var newWindow = window.open(rutaFS + '_common/BuscadorUsuarios.aspx?IDControl=wddNotificadosInternos&desde=QA', '_blank', 'width=770,height=530,status=yes,resizable=no,top=200,left=200,scrollbars=yes');
    };
    function AgregarNotificadosInternos(NotificadosInternos) {
        window.__doPostBack('wddNotificadosInternos', NotificadosInternos)
    };
    /* Revisado por: blp. Fecha: 05/10/2011
    ''' <summary>
    ''' Elimina una versión de la no conformidad
    ''' </summary>
    ''' <remarks>Llamada desde: cmdEliminar; Tiempo máximo: 0,2</remarks>*/
    function EliminarVersion() {
        if (document.getElementById("lnkBotonEliminar"))
            if (document.getElementById("lnkBotonEliminar").disabled)
                return false;

        if (confirm(arrTextosML[0]) == true) {
            CreateXmlHttp();
            if (xmlHttp) {
                var params = "Instancia=" + document.forms["frmAlta"].elements["NEW_Instancia"].value + "&Version=1";
                HabilitarBotones(false);

                xmlHttp.open("POST", "../_common/controlarVersionQA.aspx", false);
                xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                xmlHttp.send(params);

                resultadoProcesarAccion();
            } else alert("Error Ajax");
        }
        return false;
    };
    /*''' <summary>
    ''' Tras q se ejecute sincronamente controlarVersionQA.aspx controlamos sus resultados y obramos en 
    ''' consecuencia.
    ''' </summary>      
    ''' <remarks>Llamada desde: EliminarVersion; Tiempo máximo: 0</remarks>*/
    function resultadoProcesarAccion() {
        var retorno;
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            retorno = xmlHttp.responseText;

            if (retorno == 1) { //en proceso
                alert("en proceso");
                HabilitarBotones(true);
                wProgreso = null;
                return;
            } else {
                HabilitarBotones(true);
                //Directamente eliminamos, xq no tenemos ninguna version de proveedor ni nada.
                window.open("eliminarNoConformidad.aspx?NoConformidad=" + document.forms["frmAlta"].elements["NoConformidad"].value + "&Instancia=" + document.forms["frmAlta"].elements["NEW_Instancia"].value + "&Version=1", "iframeWSServer");
            };
        };
    };
    function initTab(webTab) {
        var cp = document.getElementById(webTab.ID + '_cp');
        cp.style.minHeight = '300px';
    }
    var esIntro;
    /*''' <summary>
    ''' Evitar los enter q hacen q el 1er botón de FSPMPageHeader se ejecute
    ''' </summary>
    ''' <param name="event">Pulsación</param>
    ''' <returns>True para Cualquier tecla q pulses excepto el enter</returns>
    ''' <remarks>Llamada desde: sistema; Tiempo máximo:0</remarks>*/
    function Form_KeyPres(event) {
        esIntro = false;
        if ((event.srcElement.type != 'textarea') && (event.keyCode == 13)) esIntro = true;
        return true;
    };
    function mostrarMenuAcciones(event, i) {
        var p = $get("lnkBotonAccion" + i.toString()).getBoundingClientRect();
        igmenu_showMenu('uwPopUpAcciones', event, p.left, p.bottom);
        return false;
    }
</script>
<body onload="init()" onunload="if (wProgreso != null) OcultarEspera();wProgreso=null;">
    <form id="frmAlta" method="post" runat="server" onkeypress="return Form_KeyPres(event);">
        <iframe id="iframeWSServer" style="z-index: 103; left: 8px; visibility: hidden; position: absolute; top: 300px" name="iframeWSServer" src="../blank.htm"></iframe>
        <input id="AccesoRevisor" type="hidden" name="AccesoRevisor" runat="server" />
        <input id="Solicitud" type="hidden" name="Solicitud" runat="server" />
        <input id="cadenaespera" type="hidden" name="cadenaespera" runat="SERVER" />
        <input id="BotonCalcular" type="hidden" value="0" name="BotonCalcular" runat="server" />
        <input id="NEW_Instancia" type="hidden" name="NEW_Instancia" runat="server" />
        <input id="NoConformidad" type="hidden" name="NoConformidad" runat="server" />
        <input id="IdContacto" type="hidden" name="IdContacto" runat="server" />
        <input id="ContactoDen" type="hidden" name="ContactoDen" runat="server" />
        <input id="LITContacto" type="hidden" name="LITContacto" runat="server" />
        <input id="desdeInicio" name="desdeInicio" type="hidden" runat="server" />
        <input id="CodRevisorAnterior" name="CodRevisorAnterior" type="hidden" runat="server" />
        <input id="DenominacionSolicitud" name="DenominacionSolicitud" type="hidden" runat="server" />
        <input id="PantallaMaper" type="hidden" name="PantallaMaper" runat="server" />
        <input id="UNQAEscalacion" type="hidden" name="UNQAEscalacion" runat="server" />
        <uc1:menu ID="Menu1" runat="server" OpcionMenu="Calidad" OpcionSubMenu="NoConformidades"></uc1:menu>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <CompositeScript>
                <Scripts>
                    <asp:ScriptReference Name="ExtenderBase.BaseScripts.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Common.Common.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Compat.Timer.Timer.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Animation.Animations.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Animation.AnimationBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="PopupExtender.PopupBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="AutoComplete.AutoCompleteBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Path="../alta/js/AdjacentHTML.js" />
                    <asp:ScriptReference Path="../alta/js/jsAlta.js" />
                </Scripts>
            </CompositeScript>
        </asp:ScriptManager>
        <fsn:FSNPageHeader runat="server" ID="FSNPageHeader" UrlImagenCabecera="./images/noconformidades2.gif"></fsn:FSNPageHeader>
        <div id="divAcciones" runat="server">
            <ignav:UltraWebMenu ID="uwPopUpAcciones" runat="server" WebMenuTarget="PopupMenu" SubMenuImage="ig_menuTri.gif"
                ScrollImageTop="ig_menu_scrollup.gif" Cursor="Default" ScrollImageBottomDisabled="ig_menu_scrolldown_disabled.gif" ScrollImageTopDisabled="ig_menu_scrollup_disabled.gif"
                ScrollImageBottom="ig_menu_scrolldown.gif" AutoPostBack="false">
                <MenuClientSideEvents InitializeMenu="" ItemChecked="" ItemClick="" SubMenuDisplay="" ItemHover=""></MenuClientSideEvents>
                <DisabledStyle ForeColor="LightGray"></DisabledStyle>
                <HoverItemStyle Cursor="Hand"></HoverItemStyle>
                <IslandStyle Cursor="Default" BorderWidth="1px" Font-Size="8pt" Font-Names="MS Sans Serif" BorderStyle="Outset" ForeColor="Black" BackColor="LightGray"></IslandStyle>
                <ExpandEffects ShadowColor="LightGray"></ExpandEffects>
                <TopSelectedStyle Cursor="Default"></TopSelectedStyle>
                <SeparatorStyle BackgroundImage="ig_menuSep.gif" CustomRules="background-repeat:repeat-x; "></SeparatorStyle>
                <Levels>
                    <ignav:Level Index="0"></ignav:Level>
                </Levels>
            </ignav:UltraWebMenu>
        </div>
        <div class="fondoCabecera">
            <div id="divTabla1" style="margin: 10px 0px 0px 10px;">
                <asp:Label ID="lblUnidadQA" runat="server" CssClass="captionBlue" Style="display: inline-block; width: 12em;"></asp:Label>
                <input id="ComboQA" runat="server" value="0" type="hidden" name="ComboQA" />
                <input id="UnicoUnidadQA" runat="server" value="0" type="hidden" name="UnicoUnidadQA" />
                <asp:Label ID="lblUnicoUnidadQA" runat="server" CssClass="captionBlue" Style="display: inline-block; width: 20em;"></asp:Label>
                <ig:WebDropDown ID="wddUnidadQA" runat="server" Height="22px" Width="20em" Style="display: inline-block; vertical-align: middle;"
                    EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" CurrentValue="">
                </ig:WebDropDown>

                <asp:Label ID="lblDirigidaA" runat="server" CssClass="captionBlue" Style="display: inline-block; width: 7em; margin: 0em 2em;"></asp:Label>

                <asp:HiddenField ID="Proveedor" runat="server" />
                <asp:TextBox ID="txtProveedor" runat="server" AutoPostBack="true" ReadOnly="true" Width="20em"></asp:TextBox>
                <asp:ImageButton ID="imgBuscarProveedor" runat="server" ImageUrl="~/App_Pages/PMWEB/_common/images/Img_Lupa.png" Style="cursor: pointer;" />
                <asp:Label runat="server" ID="lblCodProveedor" CssClass="captionBlue"></asp:Label>
                <asp:Label runat="server" ID="lblDenProveedor" CssClass="captionBlue"></asp:Label>

                <div id="divProveERP" runat="server" style="display: inline;">
                    <asp:UpdatePanel ID="updProveERP" runat="server" style="height: 25px; display: block; vertical-align: middle; margin-top: 10px;">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="txtProveedor" EventName="TextChanged" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:Label ID="lblProveERP" runat="server" Text="DProveedor ERP" CssClass="captionBlue" Style="display: inline-block; width: 10em;"></asp:Label>
                            <ig:WebDropDown ID="wddProveERP" runat="server" Height="22px" Width="20em" Style="display: inline-block; vertical-align: middle;"
                                EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" EnableClientRendering="true" CurrentValue="" ValueField="Cod" TextField="Den">
                                <ItemTemplate>
                                    <li value=${Cod}>${Cod} - ${Den}</li>
                                </ItemTemplate>
                            </ig:WebDropDown>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div id="divTabla2" style="margin: 10px 0px 0px 10px; display: none;">
                <asp:Label ID="lblInfoNoConformidad" runat="server" CssClass="labelBold" Style="display: inline-block; color: #000;"></asp:Label>
            </div>
            <div style="margin: 5px 0px 0px 10px;">
                <asp:Label ID="LblRevisor" runat="server" CssClass="captionBlue" Style="display: inline-block;"></asp:Label>
                <ig:WebDropDown ID="wddRevisor" runat="server" Height="22px" Width="25em" Style="display: inline-block; vertical-align: middle;"
                    EnableMultipleSelection="false"
                    EnableClosingDropDownOnSelect="true" CurrentValue="">
                </ig:WebDropDown>
            </div>
            <div style="margin: 5px 0px 0px 10px;">
                <asp:Label ID="lblComentarios" runat="server" CssClass="captionBlue" Style="display: inline-block; width: 15em;"></asp:Label>
                <textarea onkeypress="return validarLength(this,500)" id="txtComent" rows="3" cols="20" style="width: 80em; vertical-align: top;"
                    onkeydown="textCounter(this,500)"
                    onkeyup="textCounter(this,500)"
                    onchange="return validarLength(this,500)">
				</textarea>
            </div>
            <div class="PanelCabecera" style="line-height: 20px; margin-top: 10px;">
                <asp:Label ID="lblFechasResolucion" runat="server" Style="margin-left: 1em;"></asp:Label>
            </div>
            <div style="margin: 5px 0px 0px 10px;">
                <div style="display: inline-block; margin: 5px 0px 0px 10px;">
                    <asp:Label ID="lblFecResolver" runat="server" CssClass="captionBlueSmall" Style="display: inline-block; padding-bottom: 0.3em;"></asp:Label>
                    <fsde:GeneralEntry ID="fsentryNoConfFecDesde" runat="server" Width="120px" Independiente="1"
                        Tag="NoConfFecDesde" Tipo="TipoFecha" DropDownGridID="fsentryNoConfFecDesde">
                        <InputStyle CssClass="TipoFecha"></InputStyle>
                    </fsde:GeneralEntry>
                </div>
                <asp:Table ID="tblAcciones" runat="server" CssClass="fondoCabecera" Style="display: inline-block; vertical-align: top; padding: 0em; border-spacing: 0em;"></asp:Table>
                <div style="display: inline-block; margin: 5px 0px 0px 10px;">
                    <asp:Label ID="lblFecApliMejoras" runat="server" CssClass="captionBlueSmall" Style="display: inline-block; padding-bottom: 0.3em;"></asp:Label>
                    <fsde:GeneralEntry ID="fsentryFecApliMejoras" runat="server" Width="120px" Independiente="1"
                        Tag="fsentryFecApliMejoras" Tipo="TipoFecha" DropDownGridID="fsentryFecApliMejoras">
                        <InputStyle CssClass="TipoFecha"></InputStyle>
                    </fsde:GeneralEntry>
                </div>
            </div>
            <div class="PanelCabecera" style="line-height: 20px; margin-top: 10px;">
                <asp:Label ID="lblNotificados" runat="server" Style="margin-left: 1em;"></asp:Label>
            </div>
            <div style="margin: 5px 0px 10px 10px;">
                <asp:Label ID="lblSeleccionContactos" runat="server" CssClass="captionBlueSmall" Style="display: block; margin-bottom: 0.5em;"></asp:Label>
                <asp:Label ID="lblContacto" runat="server" CssClass="captionBlue" Style="white-space: nowrap;"></asp:Label>
                <asp:UpdatePanel ID="udpNotificadosProveedor" runat="server" style="height: 25px; display: inline-block; vertical-align: middle; margin-left: 0.5em;">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="txtProveedor" EventName="TextChanged" />
                    </Triggers>
                    <ContentTemplate>
                        <ig:WebDropDown ID="wddNotificadosProveedor" runat="server" Width="280px" Height="22px"
                            EnableMultipleSelection="True"
                            EnableClosingDropDownOnSelect="False" CurrentValue="">
                        </ig:WebDropDown>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:Label ID="lblContactoInterno" runat="server" CssClass="captionBlue" Style="white-space: nowrap; margin-left: 3em;"></asp:Label>
                <asp:UpdatePanel ID="updNotificadoresInternos" runat="server" style="height: 25px; display: inline-block; vertical-align: middle; margin-left: 0.5em;">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="imgBuscarNotificadosInternos" EventName="Click" />
                    </Triggers>
                    <ContentTemplate>
                        <ig:WebDropDown ID="wddNotificadosInternos" runat="server" Width="280px" Height="22px"
                            EnableMultipleSelection="True"
                            EnableClosingDropDownOnSelect="False" CurrentValue="">
                        </ig:WebDropDown>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:ImageButton ID="imgBuscarNotificadosInternos" runat="server" ImageUrl="~/App_Pages/PMWEB/_common/images/Img_Lupa.png" Style="cursor: pointer; vertical-align: text-bottom;" />
            </div>
            <div style="margin: 5px 0px 10px 10px;">
                <asp:Label ID="lblCamposObligatorios" Style="z-index: 102; display: none;" runat="server" CssClass="captionRed">Los campos marcados con (*) son de obligada cumplimentacion</asp:Label>
            </div>
        </div>
        <div id="divProgreso" style="display: none; text-align: center;">
            <asp:Label runat="server" ID="lblProgreso" Style="display: block; border: 0em; margin: 2em;" CssClass="captionBlue"></asp:Label>
            <asp:Image ID="imgProgreso" runat="server" src="../_common/images/cursor-espera_grande.gif"></asp:Image>
        </div>
        <div id="divForm2" style="display: none;">
            <igtab:UltraWebTab ID="uwtGrupos" runat="server" Width="100%"
                BorderWidth="1px" BorderStyle="Solid"
                Height="450px" ThreeDEffect="False"
                DummyTargetUrl=" " FixedLayout="True" CustomRules="padding:7px;"
                DisplayMode="Scrollable" EnableViewState="false">
                <DefaultTabStyle Height="24px" CssClass="uwtDefaultTab">
                    <Padding Left="20px" Right="20px"></Padding>
                </DefaultTabStyle>
                <RoundedImage NormalImage="ig_tab_blueb2.gif" HoverImage="ig_tab_blueb1.gif" FillStyle="LeftMergedWithCenter"></RoundedImage>
                <ClientSideEvents InitializeTabs="initTab" />
            </igtab:UltraWebTab>
        </div>
    </form>
    <div id="divForm3" style="display: none">
        <form id="frmCalculados" name="frmCalculados" action="../alta/recalcularimportes.aspx" method="post" target="fraWSServer"></form>
        <form id="frmDesglose" name="frmDesglose" method="post" target="winDesglose"></form>
    </div>
    <div id="divDropDowns" style="z-index: 101; visibility: hidden; position: absolute; top: 400px"></div>
    <div id="divAlta" style="visibility: hidden"></div>
    <div id="divCalculados" style="z-index: 1001; visibility: hidden; position: absolute; top: 0px"></div>
    <script type="text/javascript">
        if (window.onresize) window.onresize = resize;
        else document.onresize = resize;

        function wddUnidadQA_SelectionChanged(sender, e) {
            //Visibilidad de Proveedor en ERP 
            if (e.getNewSelection().length > 0) {
                if (e.getNewSelection()[0].get_value() != undefined) {
                    $.when($.ajax({
                        type: "POST",
                        url: rutaQA + 'noconformidad/altaNoconformidad.aspx/ProveERPVisible',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({ UNQA: e.getNewSelection()[0].get_value() }),
                        async: false
                    })).done(function (msg) {
                        if (msg.d[0] == 0) {
                            $('#divProveERP').hide();
                        }
                        else {
                            $('#divProveERP').show();

                            emp = msg.d[1];
                        }

                        //Vaciar el valor de la combo                      
                        var owddProveERP = $find('wddProveERP');
                        var dataSource = [];
                        owddProveERP.set_dataSource(dataSource);
                        owddProveERP.dataBind();
                        owddProveERP.set_currentValue('', true);
                    })
                }
                else {
                    $('#divProveERP').hide();
                }
            }
            else {
                $('#divProveERP').hide();
            }

        }

        function wddProveERP_DropDownOpening(sender, e) {
            var bUNQASel = false;
            if ($('[id$=wddUnidadQA]').length > 0) var wDropDownUnidadQA = $find($('[id$=wddUnidadQA]').attr('id'));
            if (wDropDownUnidadQA != undefined) {
                if (wDropDownUnidadQA.get_selectedItems().length > 0) {
                    bUNQASel = (wDropDownUnidadQA.get_selectedItems()[0].get_value() != '');
                }
            }
            else {
                bUNQASel = ($('#UnicoUnidadQA').val() != "")
            }

            if ($('#Proveedor').val() != undefined && $('#Proveedor').val() != "" && bUNQASel) cargarProveerdoresERP(emp, $('#Proveedor').val());
        }

        function cargarProveerdoresERP(emp, codprove) {
            $.when($.ajax({
                type: "POST",
                url: rutaQA + 'noconformidad/altaNoconformidad.aspx/ObtenerProveedoresERP',
                data: "{'iEmpresa':'" + emp + "','sProve':'" + codprove + "'}",
                contentType: "application/json; utf-8",
                dataType: "json",
                async: false
            })).done(function (msg) {
                var dataSource;
                if (msg.d != null) {
                    dataSource = jQuery.parseJSON('[' + msg.d + ']')
                }
                else {//No hay datos
                    dataSource = jQuery.parseJSON('[{""Cod"":"""",""Den"":""""}]')
                }
                var oWddProves = $find('wddProveERP');
                oWddProves.set_dataSource(dataSource);
                oWddProves.dataBind();
            });
        }
        $(document).ready(function () {
            if (esNoconfEscalacion) {
                $('#lblFecResolver').parent().appendTo($('#tblAcciones').parent())
            }
            else {
                $('#lblFecApliMejoras').parent().hide();
            }
        });
    </script>
</body>
</html>