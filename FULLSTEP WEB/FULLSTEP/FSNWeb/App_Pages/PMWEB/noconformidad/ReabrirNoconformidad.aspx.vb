


Public Class ReabrirNoconformidad
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim oNoConformidad As FSNServer.NoConformidad

        Dim sIdi As String = FSNUser.Idioma.ToString()

        Dim NotificadosProveedor As String = Replace(Request("NotifProv"), ";", "#")
        Dim NotificadosInternos As String = Replace(Request("NotifInt"), ";", "#")

        'Carga los datos de la no conformidad:
        oNoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
        oNoConformidad.ID = Request("NoConformidad")
        oNoConformidad.Load(sIdi)
        oNoConformidad.NoConformidad_Reabrir(FSNUser.Email, oNoConformidad.Proveedor, NotificadosProveedor, NotificadosInternos)

        Page.ClientScript.RegisterStartupScript(Me.GetType(),"claveStartUpScript", "<script>Refrescar('" & oNoConformidad.ID & "')</script>")

        oNoConformidad = Nothing

    End Sub

End Class

