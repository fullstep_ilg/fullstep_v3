Imports System.Web.Script.Serialization
Imports Infragistics.Web.UI.ListControls

Public Class altaNoconformidad
    Inherits FSNPage

    Private Const AltaScriptKey As String = "AltaIncludeScript"
    Private Const IncludeScriptFormat As String = ControlChars.CrLf & "<script type=""{0}"" src=""{1}""></script>"
    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
#Region " Web Form Designer Generated Code "
#Region "Controles"
    Protected WithEvents Solicitud As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents AccesoRevisor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cadenaespera As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblDirigidaA As System.Web.UI.WebControls.Label
    Protected WithEvents lblComentarios As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecResolver As System.Web.UI.WebControls.Label
    Protected WithEvents lblCamposObligatorios As System.Web.UI.WebControls.Label
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents uwtGrupos As Infragistics.WebUI.UltraWebTab.UltraWebTab
    Protected WithEvents lblContacto As System.Web.UI.WebControls.Label
    Protected WithEvents tblAcciones As System.Web.UI.WebControls.Table
    Protected WithEvents txtEnviar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents fsentryNoConfPROVE As DataEntry.GeneralEntry
    Protected WithEvents fsentryNoConfCON As DataEntry.GeneralEntry
    Protected WithEvents fsentryNoConfFecDesde As DataEntry.GeneralEntry
    Protected WithEvents LblRevisor As System.Web.UI.WebControls.Label
    Protected WithEvents lblVolver As System.Web.UI.WebControls.Label
    Protected WithEvents lblProgreso As System.Web.UI.WebControls.Label
    Protected WithEvents imgProgreso As System.Web.UI.WebControls.Image
    Protected WithEvents Generalentry1 As DataEntry.GeneralEntry
    Protected WithEvents Generalentry2 As DataEntry.GeneralEntry
    Protected WithEvents lblCodProveedor As System.Web.UI.WebControls.Label
    Protected WithEvents lblDenProveedor As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechaValor As System.Web.UI.WebControls.TextBox
    Protected WithEvents NEW_Instancia As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents NEW_NoConformidad As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents NoConformidad As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents IdContacto As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblIdentificador As System.Web.UI.WebControls.Label
    Protected WithEvents lblIdentificadorValor As System.Web.UI.WebControls.Label
    Protected WithEvents lblDirigidaA2 As System.Web.UI.WebControls.Label
    Protected WithEvents lblContacto2 As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechaAlta As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechaAltaValor As System.Web.UI.WebControls.Label
    Protected WithEvents ContactoDen As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents LITContacto As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents BotonCalcular As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ComboQA As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents UnicoUnidadQA As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblUnicoUnidadQA As System.Web.UI.WebControls.Label
    Protected WithEvents lblUnidadQA As System.Web.UI.WebControls.Label
    Protected WithEvents desdeInicio As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtProveedor As System.Web.UI.WebControls.TextBox
    Protected WithEvents wddNotificadosProveedor As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents imgBuscarProveedor As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imgBuscarNotificadosInternos As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Proveedor As System.Web.UI.WebControls.HiddenField
    Protected WithEvents lblNotificados As System.Web.UI.WebControls.Label
    Protected WithEvents lblContactoInterno As System.Web.UI.WebControls.Label
    Protected WithEvents lblSeleccionContactos As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechasResolucion As System.Web.UI.WebControls.Label
    Protected WithEvents wddNotificadosInternos As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents DenominacionSolicitud As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents FSNPageHeader As Fullstep.FSNWebControls.FSNPageHeader
    Protected WithEvents wddUnidadQA As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents wddRevisor As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents UNQAEscalacion As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblProveERP As System.Web.UI.WebControls.Label
    Protected WithEvents wddProveERP As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents divProveERP As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblFecApliMejoras As System.Web.UI.WebControls.Label
    Protected WithEvents fsentryFecApliMejoras As DataEntry.GeneralEntry
    Protected WithEvents uwPopUpAcciones As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
#End Region
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    End Sub
    Private designerPlaceholderDeclaration As System.Object
    Private _oSolicitud As FSNServer.Solicitud
    Private miFecPresentacionMejoras As Integer
    Private oBloque As FSNServer.Bloque
    Private _oRol As FSNServer.Rol
    Protected ReadOnly Property oSolicitud() As FSNServer.Solicitud
        Get
            If _oSolicitud Is Nothing Then
                If IsPostBack Then
                    _oSolicitud = CType(Cache("oSolicitud_" & FSNUser.Cod), FSNServer.Solicitud)
                Else
                    _oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
                    _oSolicitud.ID = Request("TipoSolicit")
                    _oSolicitud.Load(FSNUser.Idioma.ToString, True)
                    _oSolicitud.Formulario.Load(FSNUser.Idioma.ToString, _oSolicitud.ID, _oSolicitud.BloqueActual, FSNUser.CodPersona, , , FSNUser.DateFmt)
                    InsertarEnCache("oSolicitud_" & FSNUser.Cod, _oSolicitud)
                End If
            End If
            Return _oSolicitud
        End Get
    End Property
    Protected ReadOnly Property oRol() As FSNServer.Rol
        Get
            If _oRol Is Nothing Then
                If IsPostBack Then
                    _oRol = CType(Cache("oRol" & FSNUser.Cod), FSNServer.Rol)
                Else
                    For Each _oRol In oBloque.Roles.Roles
                        If _oRol.Participantes Is Nothing Then
                            If _oRol.ExistePersona(FSNUser.UON1, FSNUser.UON2, FSNUser.UON3, FSNUser.Dep, FSNUser.CodPersona) Then Exit For
                        Else
                            If _oRol.Persona = FSNUser.CodPersona Then Exit For
                        End If
                    Next

                    Me.InsertarEnCache("oRol" & FSNUser.Cod, _oRol)
                End If
            End If
            Return _oRol
        End Get
    End Property
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        InitializeComponent()
    End Sub
#End Region
    ''' <summary>
    ''' Carga de la pagina de alta de la noConformidad
    ''' </summary>
    ''' <param name="sender">pantalla</param>
    ''' <param name="e">parametro de sistema</param>            
    ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,4</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AltaNoConformidad

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaQA", "var rutaQA='" & System.Configuration.ConfigurationManager.AppSettings("rutaQA") & "';", True)
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "emp", "var emp;", True)

        If Not IsPostBack Then
            Dim idEscalacionProve As Integer = 0
            If Request.QueryString("TipoSolicit") IsNot Nothing Then Session("FiltroNoConformidadAltaNoConformidad") = Request("TipoSolicit")
            If Request.QueryString("idEscalacionProve") IsNot Nothing Then
                idEscalacionProve = CInt(Request.QueryString("idEscalacionProve"))
                'Para que al volver desde el ok vaya al panel de proveedores
                Session("VolverVisor") = Replace(Request("DesdeInicio"), "*", "&")
                UNQAEscalacion.Value = Request.QueryString("unqa")
                Proveedor.Value = Request.QueryString("prove")
                Dim oProve As FSNServer.Proveedores = FSNServer.Get_Object(GetType(FSNServer.Proveedores))
                oProve.Carga_Proveedores_QA(sCod:=Request.QueryString("prove"), sUser:=FSNUser.Cod,
                                            RestricProvMat:=FSNUser.QARestProvMaterial, RestricProvEqp:=FSNUser.QARestProvEquipo, RestricProvCon:=FSNUser.QARestProvContacto)
                If oProve.Data.Tables(0).Rows.Count = 1 Then
                    lblCodProveedor.Text = oProve.Data.Tables(0).Rows(0)("COD") & " - "
                    lblDenProveedor.Text = oProve.Data.Tables(0).Rows(0)("DEN")
                End If
                txtProveedor.Visible = False
                imgBuscarProveedor.Visible = False
                CargarNotificadosProveedor()
            End If
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "esNoconfEscalacion", "var esNoconfEscalacion=" & oSolicitud.Es_Noconformidad_Escalacion.ToString.ToLower & ";", True)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "nivelEscalacion", "var nivelEscalacion=" & oSolicitud.Nivel_Noconformidad_Escalacion & ";", True)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "idEscalacionProve", "var idEscalacionProve=" & idEscalacionProve & ";", True)
            RegistrarScripts()

            Solicitud.Value = Request("TipoSolicit")
            ConfigurarCabecera()
            ConfigurarControles()
            If oSolicitud.Workflow Then CargarParticipantes()

            'Cargamos los textos de la pantalla de espera
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.Espera
            cadenaespera.Value = Textos(1)
            lblProgreso.Text = Textos(1)
        Else
            CargarCamposGrupo()
            If oSolicitud.Workflow Then CargarParticipantes()

            Select Case Request("__EVENTTARGET")
                Case "txtProveedor"
                    'Si est� visible wddProveERP vaciarlo
                    VisibilidadProveERP()
                    If divProveERP.Style.Item(HtmlTextWriterStyle.Display) <> "none" Then VaciarProveERP()
                    CargarNotificadosProveedor()
                Case "wddNotificadosInternos"
                    If Request("__EVENTARGUMENT") <> "" Then CargarNotificadosInternos(Request("__EVENTARGUMENT"))
                Case Else

            End Select
        End If
    End Sub
    ''' <summary>Limpia la selecci�n del Proveedor en ERP</summary>
    ''' <remaks>Llamada desde: Page_Load</remaks>
    Private Sub VaciarProveERP()
        If Not wddProveERP.SelectedItem Is Nothing Then wddProveERP.ClearSelection()
        wddProveERP.CurrentValue = String.Empty
    End Sub

    ''' <summary>
    ''' Obtiene los contactos del proveedor una vez que este se selecciona
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarNotificadosProveedor()
        Dim oContactos As FSNServer.Contactos
        oContactos = FSNServer.Get_Object(GetType(FSNServer.Contactos))
        oContactos.Proveedor = Proveedor.Value
        oContactos.LoadData()
        With wddNotificadosProveedor
            .DataSource = oContactos.Data
            .ValueField = "id"
            .TextField = "contacto"
        End With
    End Sub
    ''' <summary>
    ''' Carga en el combo correspondiente la lista de contactos internos que devuelve el buscador de personas
    ''' </summary>
    ''' <param name="ListaNotificadosInternos">Lista de los contactos internos separados por ### con su value y nombre</param>
    ''' <remarks></remarks>
    Private Sub CargarNotificadosInternos(ByVal ListaNotificadosInternos As String)
        Dim NotificadosInternos() As String = Split(ListaNotificadosInternos, "###")
        Dim Item As Infragistics.Web.UI.ListControls.DropDownItem

        For i As Integer = 0 To NotificadosInternos.Length - 2
            Item = New Infragistics.Web.UI.ListControls.DropDownItem
            Item.Value = Split(NotificadosInternos(i), "#")(0)
            Item.Text = Split(NotificadosInternos(i), "#")(1) & IIf(Split(NotificadosInternos(i), "#")(2) <> "null", " (" & Split(NotificadosInternos(i), "#")(2) & ")", "")
            Item.Selected = True

            With wddNotificadosInternos
                If .CurrentValue = "" Then
                    .CurrentValue = Item.Text
                Else
                    .CurrentValue = .CurrentValue & "," & Item.Text
                End If

                If wddNotificadosInternos.Items.FindItemByValue(Item.Value) Is Nothing Then
                    wddNotificadosInternos.Items.Add(Item)
                End If
            End With
        Next
    End Sub
    Private Sub wddNotificadosProveedor_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles wddNotificadosProveedor.DataBound
        If wddNotificadosProveedor.Items.Count = 1 Then
            wddNotificadosProveedor.SelectedValue = wddNotificadosProveedor.Items(0).Value
        Else
            wddNotificadosProveedor.CurrentValue = ""
        End If
    End Sub
    ''' <summary>
    ''' Configura los controles de las p�ginas, carga de textos, datos de BD...
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ConfigurarControles()
        imgBuscarProveedor.Attributes.Add("onclick", "BuscarProveedor();return false;")
        imgBuscarNotificadosInternos.Attributes.Add("onclick", "BuscarNotificadoInterno();return false;")

        fsentryNoConfFecDesde.DateFormat = FSNUser.DateFormat
        fsentryFecApliMejoras.DateFormat = FSNUser.DateFormat
        If oSolicitud.Es_Noconformidad_Escalacion Then
            CargarDatosConfiguracionEscalacion()
        End If
        desdeInicio.Value = Replace(Request("DesdeInicio"), "*", "&")

        'Textos:
        lblDirigidaA.Text = Textos(2)
        lblComentarios.Text = Textos(3)
        If oSolicitud.Es_Noconformidad_Escalacion Then
            lblFecResolver.Text = Textos(36)
        Else
            lblFecResolver.Text = Textos(4)
        End If
        lblContacto.Text = Textos(6)
        LblRevisor.Text = Textos(14)
        lblFechasResolucion.Text = Textos(21)
        lblNotificados.Text = Textos(22)
        lblContactoInterno.Text = Textos(23)
        lblSeleccionContactos.Text = Textos(24)
        LITContacto.Value = Textos(6) 'Contacto:
        lblUnidadQA.Text = Textos(19)
        lblProveERP.Text = Textos(33) & ":"
        lblFecApliMejoras.Text = Textos(35)

        If Not FSNServer.TipoAcceso.gbFSQA_Revisor Then 'NO SE MUESTRA EL COMBO DEL REVISOR, PQ NO ESTA PERMITIDO EN PARGEN_INTERNO
            wddRevisor.Visible = False
            LblRevisor.Visible = False
            AccesoRevisor.Value = 0
        Else
            AccesoRevisor.Value = 1
            'Cargar Combo Revisor
            'En Ouser tengo el usuario cargado
            CargarRevisores()
        End If

        wddUnidadQA.ClientEvents.SelectionChanged = "wddUnidadQA_SelectionChanged"
        wddProveERP.ClientEvents.DropDownOpening = "wddProveERP_DropDownOpening"

        CargarUnidadesNegocio()
        CargarCamposGrupo()
    End Sub
    ''' <summary>
    ''' Carga de BD los revisores en un webCombo
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarRevisores()
        Dim oRevisores As FSNServer.Users 'A�ADIDO QA FIGURA REVISOR
        oRevisores = FSNServer.Get_Object(GetType(FSNServer.Users))
        'Cargo los revisores
        'si FSNUser.FSQARevisorNoConf es true se carga los revisores
        oRevisores.LoadData(FSNUser.FSQARestRevisorUO, FSNUser.FSQARestRevisorDep, FSNUser.UON1, FSNUser.UON2, FSNUser.UON3, FSNUser.Dep)

        wddRevisor.DataSource = oRevisores.Data
        wddRevisor.TextField = "DEN"
        wddRevisor.ValueField = "COD"
        wddRevisor.DataBind()

        If Not Page.IsPostBack Then wddRevisor.SelectedItemIndex = 0
    End Sub
    ''' <summary>
    ''' Carga de BD las unidades de negocio en un webCombo
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarUnidadesNegocio()
        Dim oUnidadesQA As FSNServer.UnidadesNeg
        oUnidadesQA = FSNServer.Get_Object(GetType(FSNServer.UnidadesNeg))
        If oSolicitud.Es_Noconformidad_Escalacion And Not String.IsNullOrEmpty(UNQAEscalacion.Value) Then
            Dim dt As DataTable = oUnidadesQA.LoadUnQa(FSNUser.Idioma.ToString, UNQAEscalacion.Value)

            ComboQA.Value = 0

            UnicoUnidadQA.Value = UNQAEscalacion.Value
            lblUnicoUnidadQA.Text = dt.Rows(0)("UNIDAD")

            wddUnidadQA.Visible = False
        Else
            Dim dt As DataTable
            If oSolicitud.Es_Noconformidad_Escalacion Then
                dt = oUnidadesQA.UsuLoadData_Escalacion(FSNUser.Cod, oSolicitud.ID, FSNUser.Idioma.ToString)
                If dt.Rows.Count = 0 Then Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "init", "alert('" & JSText(Textos(29)) & "');HistoryHaciaAtras();", True)
            Else
                oUnidadesQA.UsuLoadData(FSNUser.Cod, FSNUser.Idioma.ToString, Application("Nivel_UnQa"), TipoAccesoUNQAS.EmitirNoConformidades)
                dt = oUnidadesQA.MontaDataSetUnaBanda()
            End If

            Select Case dt.Rows.Count
                Case 0
                    wddUnidadQA.Visible = False
                        lblUnicoUnidadQA.Text = ""
                    Case 1
                    ComboQA.Value = 0

                    UnicoUnidadQA.Value = dt.Rows(0)("ID")
                    lblUnicoUnidadQA.Text = dt.Rows(0)("DEN")

                    wddUnidadQA.Visible = False
                Case Else
                    ComboQA.Value = 1

                    lblUnicoUnidadQA.Visible = False

                    wddUnidadQA.DataSource = dt.DefaultView.ToTable(True, "ID", "DEN")
                    wddUnidadQA.TextField = "DEN"
                    wddUnidadQA.ValueField = "ID"
                    wddUnidadQA.DataBind()

                    If Not Page.IsPostBack Then wddUnidadQA.SelectedItemIndex = 0
            End Select
        End If

        VisibilidadProveERP()
    End Sub
    ''' <summary>
    ''' Carga los valores de la escalacion en funcion del XML de configuraci�n, en los campos necesarios
    ''' </summary>
    Private Sub CargarDatosConfiguracionEscalacion()
        Dim oXML As New System.Xml.XmlDocument
        Dim dsXML As DataSet
        oXML.Load(ConfigurationManager.AppSettings("rutaXMLConfEscalacion"))
        dsXML = New DataSet
        dsXML.ReadXml(New System.Xml.XmlNodeReader(oXML))

        Dim iNoConfFecDesde As Integer

        Dim row As DataRow
        row = dsXML.Tables("NivelEscalacion" & IIf(oSolicitud.Nivel_Noconformidad_Escalacion = 1, "_1", "").ToString).Select("Nivel = " & oSolicitud.Nivel_Noconformidad_Escalacion).First
        iNoConfFecDesde = row("Plazo_NCE_PdteCerrar_M") + row("Plazo_NCE_PdteValidarCierre_M")
        miFecPresentacionMejoras = row("Plazo_NCE_PdteCerrar_M")
        If iNoConfFecDesde > 0 Then
            fsentryNoConfFecDesde.Valor = DateAdd("M", iNoConfFecDesde, Now.Date)
        End If
    End Sub
    '''
    ''' <summary>Controla la visibilidad del combo de proveedores ERP</summary>
    ''' <remarks>Llamada desde: CargarUnidadesNegocio</remarks>
    Private Sub VisibilidadProveERP()
        If Not wddUnidadQA.Visible And Not lblUnicoUnidadQA.Visible Then
            divProveERP.Style.Item(HtmlTextWriterStyle.Display) = "none"
        Else
            Dim iUNQA As Integer
            If wddUnidadQA.Visible Then
                iUNQA = wddUnidadQA.SelectedItem.Value
            Else
                iUNQA = UnicoUnidadQA.Value
            End If
            Dim oProveVis As Integer() = ProveERPVisible(iUNQA)
            Dim bProveERPVisible As Boolean = CType(oProveVis(0), Boolean)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "set_emp", "var emp=" & oProveVis(1) & ";", True)
            divProveERP.Style.Item(HtmlTextWriterStyle.Display) = IIf(bProveERPVisible, "inline", "none")
        End If
    End Sub
    ''' <summary>
    ''' Configura la cabacera, botones visibles, textos, y programa los eventos de cliente. 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ConfigurarCabecera()
        '======================================
        '#### FUNCIONALIDAD DE LA CABECERA ####
        '======================================
        FSNPageHeader.VisibleBotonVolver = True
        FSNPageHeader.VisibleBotonCalcular = True
        FSNPageHeader.VisibleBotonGuardar = True
        FSNPageHeader.VisibleBotonEliminar = True
        FSNPageHeader.OcultarBotonEliminar = True
        'Eventos de cliente asociados a los botones de la cabecera
        FSNPageHeader.OnClientClickVolver = "HistoryHaciaAtras();return false;"
        FSNPageHeader.OnClientClickCalcular = "return CalcularCamposCalculados();"

        Dim oValidacionesIntegracion As FSNServer.PM_ValidacionesIntegracion
        oValidacionesIntegracion = FSNServer.Get_Object(GetType(FSNServer.PM_ValidacionesIntegracion))

        If oValidacionesIntegracion.ControlaConMaper(TablasIntegracion.PPM) Then
            FSNPageHeader.OnClientClickEmitir = "return Enviar(true,true);"
        Else
            FSNPageHeader.OnClientClickEmitir = "return Enviar(true);"
        End If

        If oSolicitud.Workflow Then
            FSNPageHeader.VisibleBotonEmitir = False
            FSNPageHeader.OnClientClickGuardar = "return Enviar(false, false, true, null, " + oSolicitud.BloqueActual.ToString() + ");"
        Else
            FSNPageHeader.VisibleBotonEmitir = True
            FSNPageHeader.OnClientClickGuardar = "return Enviar();"
        End If
        FSNPageHeader.OnClientClickEliminar = "EliminarVersion();return false;"

        'TEXTOS
        FSNPageHeader.TextoBotonVolver = Textos(15)
        FSNPageHeader.TextoBotonCalcular = Textos(16)
        FSNPageHeader.TextoBotonEmitir = Textos(1)
        FSNPageHeader.TextoBotonGuardar = Textos(5)
        FSNPageHeader.TextoBotonEliminar = Textos(25) '"Eliminar"
        FSNPageHeader.VisibleBotonCalcular = False
        lblCamposObligatorios.Text = Textos(28)
        FSNPageHeader.TituloCabecera = oSolicitud.Den(FSNUser.Idioma.ToString)
        DenominacionSolicitud.Value = oSolicitud.Den(FSNUser.Idioma.ToString)

        'SI EXISTE ALGUN CAMPO DE TIPO=3 (CALCULADO) HACEMOS VISIBLE EL BOTON DE CALCULAR
        FSNPageHeader.VisibleBotonCalcular = oSolicitud.Formulario.Grupos.Grupos.OfType(Of FSNServer.Grupo).Where(Function(x) x.DSCampos.Tables(0).Rows.OfType(Of DataRow).Where(Function(y) y("TIPO") = TipoCampoPredefinido.Calculado).Any).Any()
        If FSNPageHeader.VisibleBotonCalcular Then BotonCalcular.Value = 1

        'Cargar las acciones
        oBloque = FSNServer.Get_Object(GetType(FSNServer.Bloque))
        oBloque.CargarRolesBloque(oSolicitud.BloqueActual, FSNUser.CodPersona)

        If Not oBloque.Roles Is Nothing Then
            Dim oDSAcciones As DataSet = oRol.CargarAcciones(Idioma)
            If Not ClientScript.IsStartupScriptRegistered("oRol_Id") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "oRol_Id", "var oRol_Id=" & oRol.Id & ";", True)

            Dim oPopMenu As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
            Dim oItem As Infragistics.WebUI.UltraWebNavigator.Item
            Dim oRow As DataRow

            oPopMenu = Me.uwPopUpAcciones
            oPopMenu.Visible = False
            Dim sEjecutarAccion As String
            If oDSAcciones.Tables.Count > 0 Then
                If oDSAcciones.Tables(0).Rows.Count > 3 Then
                    oPopMenu.Visible = True
                    FSNPageHeader.VisibleBotonAccion1 = True
                    FSNPageHeader.TextoBotonAccion1 = Textos(38) ' "Otras acciones"
                    FSNPageHeader.OnClientClickAccion1 = "return mostrarMenuAcciones(event,1);"

                    For Each oRow In oDSAcciones.Tables(0).Rows
                        If IsDBNull(oRow.Item("DEN")) Then
                            oItem = oPopMenu.Items.Add("&nbsp;")
                        Else
                            If oRow.Item("DEN") = "" Then
                                oItem = oPopMenu.Items.Add("&nbsp;")
                            Else
                                oItem = oPopMenu.Items.Add(DBNullToStr(oRow.Item("DEN")))
                            End If
                        End If
                        sEjecutarAccion = "javascript:Enviar(false, false, false, " + oRow.Item("ACCION").ToString() + "," + oBloque.Id.ToString + "," + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, True.ToString.ToLower, False.ToString.ToLower)) + "," + (IIf(oRow.Item("GUARDA") = 1, True.ToString.ToLower, False.ToString.ToLower)) + "," + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, True.ToString.ToLower, False.ToString.ToLower)) + ");"
                        oItem.TargetUrl = sEjecutarAccion
                    Next
                Else
                    Dim cont As Byte = 0
                    For Each oRow In oDSAcciones.Tables(0).Rows
                        sEjecutarAccion = "javascript:Enviar(false, false, false, " + oRow.Item("ACCION").ToString() + "," + oBloque.Id.ToString + "," + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, True.ToString.ToLower, False.ToString.ToLower)) + "," + (IIf(oRow.Item("GUARDA") = 1, True.ToString.ToLower, False.ToString.ToLower)) + "," + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, True.ToString.ToLower, False.ToString.ToLower)) + "); return false;"
                        If cont = 0 Then
                            FSNPageHeader.VisibleBotonAccion1 = True
                            FSNPageHeader.TextoBotonAccion1 = DBNullToStr(oRow.Item("DEN"))
                            FSNPageHeader.OnClientClickAccion1 = sEjecutarAccion
                            cont = 1
                        ElseIf cont = 1 Then
                            FSNPageHeader.VisibleBotonAccion2 = True
                            FSNPageHeader.TextoBotonAccion2 = DBNullToStr(oRow.Item("DEN"))
                            FSNPageHeader.OnClientClickAccion2 = sEjecutarAccion
                            cont = 2
                        Else
                            FSNPageHeader.VisibleBotonAccion3 = True
                            FSNPageHeader.TextoBotonAccion3 = DBNullToStr(oRow.Item("DEN"))
                            FSNPageHeader.OnClientClickAccion3 = sEjecutarAccion
                        End If
                    Next
                End If
            End If
        End If
    End Sub
    ''' <summary>Carga la pesta�a de participantes si es necesario</summary>
    Private Sub CargarParticipantes()
        oRol.CargarParticipantes(Idioma)
        If oRol.Participantes.Tables(0).Rows.Count > 0 Then
            Dim oTabItem As New Infragistics.WebUI.UltraWebTab.Tab
            oTabItem.Text = Textos(37) 'Participantes
            oTabItem.Key = "PARTICIPANTES"

            uwtGrupos.Tabs.Add(oTabItem)

            oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
            oTabItem.ContentPane.UserControlUrl = "..\alta\participantes.ascx"

            Dim oucParticipantes As participantes = oTabItem.ContentPane.UserControl
            oucParticipantes.Idi = Idioma
            oucParticipantes.TabContainer = uwtGrupos.ClientID
            oucParticipantes.dsParticipantes = oRol.Participantes

            Dim oInputHidden As New System.Web.UI.HtmlControls.HtmlInputHidden
            oInputHidden.ID = "txtPre_PARTICIPANTES"
            oInputHidden.Value = oucParticipantes.ClientID
            Me.FindControl("frmAlta").Controls.Add(oInputHidden)
        End If
    End Sub

    ''' <summary>
    ''' Registra los scritps necesarios en la p�gina
    ''' </summary>
    ''' <remarks></remarks>
    ''' revisado por ilg(15/11/2011)
    Private Sub RegistrarScripts()
        Dim sClientTextVars As String
        sClientTextVars = ""
        sClientTextVars += "vdecimalfmt='" + FSNUser.DecimalFmt + "';"
        sClientTextVars += "vthousanfmt='" + FSNUser.ThousanFmt + "';"
        sClientTextVars += "vprecisionfmt='" + FSNUser.PrecisionFmt + "';"
        sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsUser", sClientTextVars)

        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(Textos(9)) + "';"  'Faltan campos obligatorios
        sClientTexts += "arrTextosML[1] = '" + JSText(Textos(10)) + "';"  'Seleccione un proveedor
        sClientTexts += "arrTextosML[2] = '" + JSText(Textos(11)) + "';"  'Seleccione la fecha l�mite de resoluci�n de la no conformidad
        sClientTexts += "arrTextosML[3] = '" + JSText(Textos(12)) + "';"  'Algunas fechas de inicio y/o fechas de fin de acci�n son posteriores a sus fechas limite correspondientes. Reviselos para poder Continuar.
        sClientTexts += "arrTextosML[4] = '" + JSText(Textos(13)) + "';"  'Seleccione al menos un contacto del proveedor
        sClientTexts += "arrTextosML[5] = '" + JSText(Textos(20)) + "';"  'Seleccione un unidad qa
        sClientTexts += "arrTextosML[6] = '" + JSText(Textos(26)) + "';"
        sClientTexts += "arrTextosML[7] = '" + JSText(Textos(39)) + "';"

        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

        If Not Page.ClientScript.IsClientScriptBlockRegistered("varFila") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varFila", "<script>var sFila = '" & JSText(Textos(27)) & "' </script>")

        Dim ilGMN1 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN1
        Dim ilGMN2 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN2
        Dim ilGMN3 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN3
        Dim ilGMN4 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN4
        Dim sScript As String
        sScript = ""
        sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
        sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
        sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
        sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "LongitudesCodigosKey", sScript)

        If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayDesgloses") Then Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses=new Array();</script>")

        'En jsAlta.js hay para las funciones de a�adir linea y copia linea    $create(AjaxControlToolkit.AutoCompleteBehavior ...rutaPM...
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM='" & ConfigurationManager.AppSettings("rutaPM") & "';</script>")
        'para el buscador de art�culos
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS='" & ConfigurationManager.AppSettings("rutaFS") & "';</script>")

        Dim sVariableJavascriptTextosPantalla As String = "var TextoErrorValidacionNoconformidadEscalacion = new Array();"
        sVariableJavascriptTextosPantalla &= "TextoErrorValidacionNoconformidadEscalacion[1]='" & JSText(Textos(30)) & "';"
        sVariableJavascriptTextosPantalla &= "TextoErrorValidacionNoconformidadEscalacion[2]='" & JSText(Textos(31)) & "';"
        sVariableJavascriptTextosPantalla &= "TextoErrorValidacionNoconformidadEscalacion[3]='" & JSText(Textos(32)) & "';"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextoErrorValidacionNoconformidadEscalacion", sVariableJavascriptTextosPantalla, True)
    End Sub
    ''' Revisado por: blp. Fecha: 05/10/2011
    ''' <summary>
    ''' Genera los controles para poder chequear las acciones a solicitar y establecer las fechas l�mite de cumplimentaci�n de las acciones.
    ''' </summary>
    ''' <remarks>Llamada desde CargarCamposGrupo(). M�x. inferior 1 seg</remarks>
    Private Sub ConfigurarCamposSolicitudAcciones()  '******************* Montar la tabla de acciones ****************************
        Dim iColumna As Integer
        Dim iFila As Integer
        Dim oTable As System.Web.UI.WebControls.Table
        Dim otblRowTablaAcciones As System.Web.UI.WebControls.TableRow
        Dim otblCellTablaAcciones As System.Web.UI.WebControls.TableCell
        Dim otblRow As System.Web.UI.WebControls.TableRow
        Dim otblCell As System.Web.UI.WebControls.TableCell
        Dim olabel As System.Web.UI.WebControls.Label
        Dim oEntryAccion As DataEntry.GeneralEntry

        iColumna = 1
        iFila = 0
        For Each oGrupo In oSolicitud.Formulario.Grupos.Grupos
            For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Acciones Then
                    If iColumna = 1 Then 'a�ade una fila nueva a la tabla
                        otblRowTablaAcciones = New System.Web.UI.WebControls.TableRow
                        iFila = iFila + 1
                    Else  'Se situa en la fila actual
                        otblRowTablaAcciones = Me.tblAcciones.Rows.Item(iFila - 1)
                    End If  'Check para solicitar la acci�n:
                    otblCellTablaAcciones = New System.Web.UI.WebControls.TableCell

                    oTable = New System.Web.UI.WebControls.Table
                    otblRow = New System.Web.UI.WebControls.TableRow
                    otblCell = New System.Web.UI.WebControls.TableCell

                    otblCell.ColumnSpan = 2

                    olabel = New System.Web.UI.WebControls.Label
                    If oSolicitud.Es_Noconformidad_Escalacion Then
                        olabel.Text = Textos(34)
                    Else
                        olabel.Text = Textos(8) & " " & oRow.Item("DEN_" & FSNUser.Idioma.ToString) 'Textos(7)
                    End If
                    olabel.CssClass = "captionBlueSmall"
                    otblCell.Controls.Add(olabel)
                    otblCell.Style("padding-left") = "20px"
                    otblRow.Cells.Add(otblCell)

                    oTable.Rows.Add(otblRow)

                    'Fecha l�mite:
                    otblCell = New System.Web.UI.WebControls.TableCell
                    otblRow = New System.Web.UI.WebControls.TableRow

                    oEntryAccion = New DataEntry.GeneralEntry
                    'oEntryAccion.CampoDef = estamos en solicitud no hay copia_campo_def
                    oEntryAccion.ID = "NoConfACCION_" & oRow.Item("ID")
                    oEntryAccion.Desglose = False
                    oEntryAccion.IsGridEditor = False
                    oEntryAccion.ReadOnly = False
                    oEntryAccion.BaseDesglose = False
                    oEntryAccion.Calculado = False
                    oEntryAccion.Desglose = False
                    oEntryAccion.Obligatorio = False
                    oEntryAccion.Intro = 0
                    oEntryAccion.Independiente = "Accion"
                    oEntryAccion.Text = ""
                    oEntryAccion.InputStyle.CssClass = "captionBlueSmall"
                    oEntryAccion.Width = Unit.Percentage(100)
                    oEntryAccion.Tag = "NoConfACCION_" & oRow.Item("ID")
                    oEntryAccion.Tipo = TiposDeDatos.TipoGeneral.TipoCheckBox
                    oEntryAccion.TipoGS = TiposDeDatos.TipoCampoGS.SinTipo
                    oEntryAccion.Valor = True

                    Dim pag As FSNPage = Me.Page
                    oEntryAccion.Title = Me.Page.Title

                    otblCell.Controls.Add(oEntryAccion)
                    otblCell.Style("padding-left") = "20px"
                    otblRow.Cells.Add(otblCell)

                    otblCell = New System.Web.UI.WebControls.TableCell

                    oEntryAccion = New DataEntry.GeneralEntry
                    'oEntryAccion.CampoDef = estamos en solicitud no hay copia_campo_def
                    oEntryAccion.ID = "NoConfFECLIM_" & oRow.Item("ID")
                    oEntryAccion.Desglose = False
                    oEntryAccion.IsGridEditor = False
                    oEntryAccion.ReadOnly = False
                    oEntryAccion.BaseDesglose = False
                    oEntryAccion.Calculado = False
                    oEntryAccion.Desglose = False
                    oEntryAccion.Obligatorio = False
                    oEntryAccion.Independiente = "Accion"
                    oEntryAccion.Tag = "NoConfFECLIM_" & oRow.Item("ID")
                    oEntryAccion.Tipo = TiposDeDatos.TipoGeneral.TipoFecha
                    oEntryAccion.TipoGS = TiposDeDatos.TipoCampoGS.SinTipo
                    oEntryAccion.InputStyle.CssClass = "TipoFecha"
                    oEntryAccion.DateFormat = FSNUser.DateFormat
                    'Parametro recogido del archivo de configuraci�n de las escalaciones
                    If miFecPresentacionMejoras > 0 Then
                        oEntryAccion.Valor = DateAdd("M", miFecPresentacionMejoras, Now.Date)
                        'Reiniciamos el valor por si en alg�n caso se plantean m�s acciones en las escalaciones
                        miFecPresentacionMejoras = 0
                    ElseIf Not IsDBNull(oRow.Item("VALOR_NUM")) Then
                        oEntryAccion.Valor = Now.AddDays(oRow.Item("VALOR_NUM"))
                    End If
                    otblCell.Controls.Add(oEntryAccion)

                    otblRow.Cells.Add(otblCell)

                    oTable.Rows.Add(otblRow)

                    otblCellTablaAcciones.Controls.Add(oTable)
                    otblRowTablaAcciones.Cells.Add(otblCellTablaAcciones)

                    If iColumna = 3 Then
                        iColumna = 1
                    Else
                        If iColumna = 1 Then
                            tblAcciones.Rows.Add(otblRowTablaAcciones)
                        End If
                        iColumna = iColumna + 1
                    End If
                End If
            Next
        Next
    End Sub
    Private Sub CargarCamposGrupo()
        Dim oGrupo As FSNServer.Grupo
        Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
        Dim oRow As DataRow = Nothing
        Dim oucDesglose As desgloseControl
        Dim oucCampos As campos
        Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden

        'Genera el tab con los datos del formulario asociado a la solicitud:
        uwtGrupos.Tabs.Clear()
        AplicarEstilosTab(uwtGrupos)
        Dim lIndex As Long = 0

        For Each oGrupo In oSolicitud.Formulario.Grupos.Grupos
            oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
            Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
            Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
            oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(FSNUser.Idioma.ToString), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
            oTabItem.Key = lIndex

            uwtGrupos.Tabs.Add(oTabItem)
            oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden

            oInputHidden.ID = "txtPre_" + lIndex.ToString
            lIndex += 1

            If oGrupo.NumCampos <= 1 Then
                For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                    If oRow.Item("VISIBLE") = 1 Then
                        Exit For
                    End If
                Next

                If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Desglose Or DBNullToSomething(oRow.Item("SUBTIPO")) = TiposDeDatos.TipoGeneral.TipoDesglose Then
                    If oRow.Item("VISIBLE") = 0 Then
                        uwtGrupos.Tabs.Remove(oTabItem)
                    Else
                        oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                        oTabItem.ContentPane.UserControlUrl = "..\alta\desglose.ascx"
                        oucDesglose = oTabItem.ContentPane.UserControl
                        oucDesglose.ID = oGrupo.Id.ToString
                        oucDesglose.Campo = oRow.Item("ID")
                        oucDesglose.TieneIdCampo = False
                        oucDesglose.Ayuda = DBNullToSomething(oRow.Item("AYUDA_" & Idioma))
                        oucDesglose.TabContainer = uwtGrupos.ClientID
                        oucDesglose.SoloLectura = (oRow.Item("ESCRITURA") = 0)
                        oucDesglose.Solicitud = oSolicitud.ID
                        oucDesglose.TipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad
                        oInputHidden.Value = oucDesglose.ClientID
                    End If
                Else
                    oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                    oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                    oucCampos = oTabItem.ContentPane.UserControl
                    oucCampos.ID = oGrupo.Id.ToString
                    oucCampos.dsCampos = oGrupo.DSCampos
                    oucCampos.IdGrupo = oGrupo.Id
                    oucCampos.Idi = Idioma.ToString
                    oucCampos.TabContainer = uwtGrupos.ClientID
                    oucCampos.Solicitud = oSolicitud.ID
                    oucCampos.TipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad
                    oucCampos.Formulario = oSolicitud.Formulario
                    oInputHidden.Value = oucCampos.ClientID
                End If
            Else
                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                oucCampos = oTabItem.ContentPane.UserControl
                oucCampos.ID = oGrupo.Id.ToString
                oucCampos.dsCampos = oGrupo.DSCampos
                oucCampos.IdGrupo = oGrupo.Id
                oucCampos.Idi = FSNUser.Idioma.ToString
                oucCampos.TabContainer = uwtGrupos.ClientID
                oucCampos.Solicitud = oSolicitud.ID
                oucCampos.TipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad
                oucCampos.Formulario = oSolicitud.Formulario
                oInputHidden.Value = oucCampos.ClientID
            End If
            Me.FindControl("frmAlta").Controls.Add(oInputHidden)
        Next
        ConfigurarCamposSolicitudAcciones()
        Me.FindControl("frmAlta").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())
    End Sub
#Region "Web Methods"
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function Validaciones_NoConformidad_Escalacion(ByVal Solicitud As Long, ByVal Proveedor As String, ByVal UNQA As Integer)
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")

            Dim oValidacionEscalacionNoConformidad As FSNServer.Escalaciones = FSNServer.Get_Object(GetType(FSNServer.Escalaciones))
            Dim oResultado As Object = oValidacionEscalacionNoConformidad.NivelesEscalacion_Panel_Escalacion_Get_Validaciones_Alta_Noconfomidad_Escalacion(Solicitud, Proveedor, UNQA, FSNUser.Idioma.ToString)

            Dim serializer As New JavaScriptSerializer
            If (oResultado.resultadoValidacion = 1) Then
                Dim Textos As DataTable
                Dim oDict As FSNServer.Dictionary
                oDict = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
                oDict.LoadData(TiposDeDatos.ModulosIdiomas.PanelEscalacionProveedores, FSNUser.Idioma)
                Textos = oDict.Data.Tables(0)
                Select Case oResultado.estadoNoConformidad
                    Case TipoEstadoNoConformidad.Abierta
                        oResultado.estadoNoConformidad = Textos(26)(1)
                    Case TipoEstadoNoConformidad.CierrePosEnPlazo
                        oResultado.estadoNoConformidad = Textos(28)(1)
                    Case TipoEstadoNoConformidad.CierrePosFueraPlazo
                        oResultado.estadoNoConformidad = Textos(29)(1)
                    Case TipoEstadoNoConformidad.CierreNegativo
                        oResultado.estadoNoConformidad = Textos(27)(1)
                    Case TipoEstadoNoConformidad.CierreNegSinRevisar, TipoEstadoNoConformidad.CierrePosSinRevisar
                        oResultado.estadoNoConformidad = Textos(30)(1)
                    Case TipoEstadoNoConformidad.Anulada
                        oResultado.estadoNoConformidad = Textos(31)(1)
                    Case Else
                        oResultado.estadoNoConformidad = String.Empty
                End Select
            ElseIf oResultado.resultadoValidacion = 2 Then
                Dim Textos As DataTable
                Dim oDict As FSNServer.Dictionary
                oDict = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
                oDict.LoadData(TiposDeDatos.ModulosIdiomas.PanelEscalacionProveedores, FSNUser.Idioma)
                Textos = oDict.Data.Tables(0)
                Select Case oResultado.estadoEscalacion
                    Case 1
                        oResultado.estadoEscalacion = Textos(35)(1)
                    Case 2
                        oResultado.estadoEscalacion = Textos(16)(1)
                    Case Else
                        oResultado.estadoEscalacion = Textos(14)(1)
                End Select
            End If

            Return serializer.Serialize(oResultado)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>Devuelve si el campo de proveedor en ERP debe estar visible</summary>
    ''' <param name="UNQA">Id de la UNQA</param>
    ''' <returns>booleano indicando la visibilidad</returns>
    ''' <remarks>llamada desde: ConfigurarControles</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ProveERPVisible(ByVal UNQA As Integer) As Integer()
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oProve As FSNServer.Proveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
        Dim dtDatos As DataTable = oProve.VisibilidadProveERP_NC(UNQA).Tables(0)
        Return New Integer() {dtDatos.Rows(0)("VISIBLE"), dtDatos.Rows(0)("EMPRESA")}
    End Function
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ObtenerProveedoresERP(ByVal iEmpresa As Integer, ByVal sProve As String) As String
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

        Dim oPed As FSNServer.CEmisionPedidos = FSNServer.Get_Object(GetType(FSNServer.CEmisionPedidos))
        Dim oProves As FSNServer.CProveERPs = oPed.CargarProveedoresERP(Nothing, iEmpresa, sProve, String.Empty)

        'Se toman s�lo los de c�digos diferentes: Se agrupa la colecci�n por c�digo y se coge el primer elemento de cada grupo
        Dim oDistinctProves As List(Of FSNServer.CProveERP) = oProves.GroupBy(Function(o) o.Cod).Select(Function(o) o.First()).ToList

        Dim sResultado As String = String.Empty
        For Each oProveERP As FSNServer.CProveERP In oDistinctProves
            sResultado &= "{""Cod"":""" & HttpUtility.JavaScriptStringEncode(oProveERP.Cod) & """,""Den"":""" & HttpUtility.JavaScriptStringEncode(oProveERP.Cod & " - " & oProveERP.Den) & """},"
        Next

        If Len(sResultado) > 0 Then sResultado = Left(sResultado, Len(sResultado) - 1)
        Return sResultado
    End Function
#End Region
End Class