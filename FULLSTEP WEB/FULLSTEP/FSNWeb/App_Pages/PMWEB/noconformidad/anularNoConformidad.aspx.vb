﻿Public Partial Class anularNoConformidad
    Inherits FSNPage

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
"<script language=""{0}"">{1}</script>"

    ''' <summary>
    '''        Evento que salta al cargar la pagina.
    '''        Carga los datos de la noConformidad
    ''' </summary>
    ''' <param name="sender">Las del evento</param>
    ''' <param name="e">Las del evento</param>        
    '''<remarks>>Timepo ejecucion:=0,3seg.</remarks>   
    ''' 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim lNoConformidad As Long = 0
        Dim lInstancia As Long = 0
        Dim sVolver As String

        lNoConformidad = Request.QueryString("NoConformidad")
        lInstancia = Request.QueryString("Instancia")
        FSNPageHeader.VisibleBotonVolver = True

        If Not IsPostBack Then
            FSNPageHeader.OnClientClickVolver = "IrADetalle();return false;"
            If Request.QueryString("volver") <> "" Then
                sVolver = Request.QueryString("volver")
                sVolver = Replace(sVolver, "*", "&")
                Session("VolverVisor") = sVolver
            End If
            If Session("VolverVisor") = "" Then
                Session("VolverVisor") = ConfigurationManager.AppSettings("rutaQA2008") & "NoConformidades/NoConformidades.aspx"
            End If
            CargarIdiomaControles()
            CargarDatosNoConformidad()
        Else
            FSNPageHeader.OnClientClickVolver = "IrAlVisorNoConformidades();return false;"
        End If
    End Sub

    ''' <summary>
    '''        Carga en el idioma del usuario los componentes de la pagina
    ''' </summary>
    '''<remarks>Llamada desde: Page_load // Tiempo ejecucion:=0,1seg.</remarks>   

    Private Sub CargarIdiomaControles()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.NoConformidadAnulada

        FSNPageHeader.TituloCabecera = Textos(0) '"Anular no conformidad"
        FSNPageHeader.TextoBotonVolver = Textos(1) '"Volver"
        lblId.Text = Textos(2) & ":" '"Identificador" & ": "
        lblProveedor.Text = Textos(3) & ":" '"Proveedor" & ": "
        lblFechaAnulacion.Text = Textos(4) & ":" '" Fecha de anulación" & ": "
        lblContacto.Text = Textos(5) & ":" '"Contacto" & ": "
        lblUsuario.Text = Textos(6) & ":" '"Usuario" & ": "
        lblComentarios.Text = Textos(7) '"Comentario de la anulación"
        lblMensaje.Text = Textos(8) & "." '"La no conformidad ha sido anulada satisfactoriamente" & "."

        cmdAceptar.Value = Textos(9) '"Aceptar"
        cmdCancelar.Value = Textos(10) '"Cancelar"
    End Sub

    ''' <summary>
    '''         Nos carga los datos de la noConformidad
    ''' </summary>
    '''<remarks>Llamada desde:= Page_load // Timepo ejecucion:=0,1seg.</remarks>   
    Private Sub CargarDatosNoConformidad()
        Dim oNoConformidad As FSNServer.NoConformidad

        oNoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
        oNoConformidad.Instancia = Request("Instancia")
        oNoConformidad.CargarDatosDesdeInstancia()

        lblIdBD.Text = Request.QueryString("Instancia")
        lblProveedorBD.Text = oNoConformidad.Proveedor & " - " & oNoConformidad.ProveDen

        lblContactoBD.Text = oNoConformidad.NotificadosProveedor
        lblUsuarioBD.Text = oNoConformidad.Peticionario

        lblFechaAnulacionBD.Text = FormatDate(Now, FSNUser.DateFormat)

        If oNoConformidad.NotificadosInternos Is Nothing Then
            lblNotifInt.Visible = False
            Me.lblNotifIntBD.Visible = False
        Else
            lblNotifInt.Text = Textos(11) & ":"
            Me.lblNotifIntBD.Text = oNoConformidad.NotificadosInternos
        End If

        oNoConformidad = Nothing

    End Sub

    ''' <summary>
    '''         Anula la noConformidad y envia mail a los contactos del proveedor y contactos internos
    '''         La segunda vez que se pulsa en aceptar nos manda al detalle de la noConformidad
    ''' </summary>
    ''' <param name="sender">Las del evento</param>
    ''' <param name="e">Las del evento</param>        
    '''<remarks>>Timepo ejecucion:=0,3seg.</remarks>       
    Private Sub cmdAceptar_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAceptar.ServerClick
        Dim oNoConformidad As FSNServer.NoConformidad
        oNoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))

        If cmdCancelar.Visible = True Then
            'Anular la NoConformidad
            oNoConformidad.ID = Request.QueryString("NoConformidad")
            oNoConformidad.Es_Noconformidad_Escalacion = (Request.QueryString("EsNoConfEscalacion") = 1)
            oNoConformidad.ComentAnulacion = txtComent.Text
            oNoConformidad.AnularNoConformidad(Request.QueryString("Instancia"), FSNUser.CodPersona, FSNUser.Email)
            PanelComentarioAnulacion.Visible = False
            cmdCancelar.Visible = False
            lblMensaje.Visible = True
            PanelMensaje.Visible = True
            lblFechaAnulacionBD.Text = hidFechaAnulacion.Value
            upInformacionNoConformidad.Update()
        Else
            Response.Redirect(Session("VolverVisor"))
        End If
        oNoConformidad = Nothing
    End Sub
End Class