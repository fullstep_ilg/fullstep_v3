<%@ Page Language="vb" AutoEventWireup="false" Codebehind="noconfEmitidaAWorkfl.aspx.vb" Inherits="Fullstep.FSNWeb.noconfEmitidaAWorkfl"%>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 384px"
				height="384" width="80%" border="0">
				<TR>
					<TD colSpan="2">
						<asp:Label id="lblTitulo" runat="server" Width="100%" CssClass="TituloSinDatos">lblTitulo</asp:Label></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD>
						<asp:Label id="lblSolicitud" runat="server" Width="100%" CssClass="captionBlue">lblSolicitud</asp:Label></TD>
					<TD>
						<asp:Label id="lblPeticionario" runat="server" Width="100%" CssClass="captionBlue">lblPeticionario</asp:Label></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<asp:Label id="lblAprobador" runat="server" Width="100%" CssClass="captionBlue">lblAprobador</asp:Label></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<asp:Label id="lblMensaje" runat="server" Width="100%" CssClass="fntLogin">lblMensaje</asp:Label></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<asp:Label id="lblMensaje2" runat="server" Width="100%" CssClass="fntLogin">lblMensaje2</asp:Label></TD>
					<TD></TD>
				</TR>
			</TABLE>
			<uc1:menu id="Menu1" runat="server"></uc1:menu>
		</form>
	</body>
</html>
