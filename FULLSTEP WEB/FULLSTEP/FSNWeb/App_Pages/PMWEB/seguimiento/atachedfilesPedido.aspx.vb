
    Public Class atachedfilesPedido
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblDescargar As System.Web.UI.WebControls.Label
        Protected WithEvents imgDescarga As System.Web.UI.WebControls.ImageButton
        Protected WithEvents uwgFiles As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
        Protected WithEvents tblCabecera As System.Web.UI.HtmlControls.HtmlTable
        Protected WithEvents lblSolicitud As System.Web.UI.WebControls.Label
        Protected WithEvents lblDenSolicitud As System.Web.UI.WebControls.Label
        Protected WithEvents lblPedido As System.Web.UI.WebControls.Label
        Protected WithEvents lblDenPedido As System.Web.UI.WebControls.Label
        Protected WithEvents lblLinea As System.Web.UI.WebControls.Label
        Protected WithEvents lblDenLinea As System.Web.UI.WebControls.Label
        Protected WithEvents txtPedido As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents txtLinea As System.Web.UI.HtmlControls.HtmlInputHidden
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
   "<script DEFER=true language=""{0}"">{1}</script>"
       
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim mlPedido As Long
        Dim mlInstancia As Long
        Dim mlLinea As Long

        Dim oPedido As FSNServer.Pedido
        Dim oAdjuntos As FSNServer.Adjuntos
        Dim oDS As DataSet

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AttachFilePedidos

        mlPedido = Request("Pedido")
        mlInstancia = Request("Instancia")
        If Request("Linea") <> Nothing And Request("Linea") <> "null" Then
            mlLinea = Request("Linea")
        End If

        txtPedido.Value = mlPedido
        txtLinea.Value = mlLinea

        oPedido = FSNServer.Get_Object(GetType(FSNServer.Pedido))
        oPedido.ID = mlPedido
        oPedido.Instancia = mlInstancia
        oPedido.Load(Idioma)

        lblDenSolicitud.Text = oPedido.Instancia & " " & oPedido.DenInstacia
        lblDenPedido.Text = oPedido.Anyo & "/" & oPedido.NumPedido & "/" & oPedido.NumOrden

        If mlLinea > 0 Then
            'Denomminacion de Linea
            Dim oFilaRow As DataRow
            If oPedido.Lineas.Select("ID=" & mlLinea).Length > 0 Then
                oFilaRow = oPedido.Lineas.Select("ID=" & mlLinea)(0)
                lblDenLinea.Text = oFilaRow.Item("CODART") & " " & oFilaRow.Item("DENART")
            End If
        Else
            tblCabecera.Rows(3).Visible = False
        End If

        lblSolicitud.Text = Textos(8)
        lblPedido.Text = Textos(9)
        lblLinea.Text = Textos(10)
        lblDescargar.Text = Textos(0)

        'Carga los adjuntos:
        oAdjuntos = FSNServer.Get_Object(GetType(FSNServer.Adjuntos))
        oAdjuntos.LoadAdjuntosPedido(mlPedido, mlLinea)

        oDS = oAdjuntos.Data

        'Rellena la grid:
        Me.uwgFiles.DataSource = oDS
        Me.uwgFiles.DataBind()

        uwgFiles.Bands(0).Columns.FromKey("ID").Hidden = True
        uwgFiles.Bands(0).Columns.FromKey("BYTES").Hidden = True

        uwgFiles.Bands(0).Columns.FromKey("NOM").Header.Caption = Textos(2)
        uwgFiles.Bands(0).Columns.FromKey("NOM").Width = Unit.Percentage(40)
        uwgFiles.Bands(0).Columns.FromKey("NOM").CellMultiline = Infragistics.WebUI.UltraWebGrid.CellMultiline.Yes
        uwgFiles.Bands(0).Columns.FromKey("NOM").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.Yes
        uwgFiles.Bands(0).Columns.FromKey("DATASIZE").Header.Caption = Textos(3)
        uwgFiles.Bands(0).Columns.FromKey("DATASIZE").Width = Unit.Percentage(5)
        uwgFiles.Bands(0).Columns.FromKey("DATASIZE").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.Yes
        uwgFiles.Bands(0).Columns.FromKey("COMENT").Header.Caption = Textos(4)
        uwgFiles.Bands(0).Columns.FromKey("COMENT").Width = Unit.Percentage(40)
        uwgFiles.Bands(0).Columns.FromKey("COMENT").CellMultiline = Infragistics.WebUI.UltraWebGrid.CellMultiline.Yes
        uwgFiles.Bands(0).Columns.FromKey("DESCARGAR").Header.Caption = Textos(5)
        uwgFiles.Bands(0).Columns.FromKey("ELIMINAR").ServerOnly = True
        uwgFiles.Bands(0).Columns.FromKey("SUSTITUIR").ServerOnly = True
        uwgFiles.Bands(0).Columns.FromKey("DESCARGAR").Move(9)
        uwgFiles.DataBind()

        uwgFiles.DisplayLayout.HeaderStyleDefault.CssClass = "CabeceraCampoArchivo"

        Dim sScript As String
        Dim sClientId As String = Replace(Me.uwgFiles.ClientID, "_", "x")
        sScript = "document.getElementById(""" + sClientId + "_addBox"").parentNode.style.height=""0px"""
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        Page.ClientScript.RegisterStartupScript(Me.GetType(),"startItKey", sScript)
    End Sub

    Private Sub uwgFiles_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgFiles.InitializeRow
        e.Row.Cells.FromKey("ELIMINAR").Value = "<img border=""0"" align=""center"" src=""" & ConfigurationManager.AppSettings("rutaPM") + "_common/images/eliminar.gif"">"
        e.Row.Cells.FromKey("DESCARGAR").Value = "<img border=""0"" align=""center"" src=""" & ConfigurationManager.AppSettings("rutaPM") + "_common/images/descargar.gif"" onclick=""Descargar(" & e.Row.Cells.FromKey("ID").Value & ")"">"
        e.Row.Cells.FromKey("SUSTITUIR").Value = "<img border=""0"" align=""center"" src=""" & ConfigurationManager.AppSettings("rutaPM") + "_common/images/sustituir.gif"">"
    End Sub
End Class

