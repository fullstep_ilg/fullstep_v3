<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="accionsolicitud.aspx.vb" Inherits="Fullstep.FSNWeb.accionsolicitud" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
</head>
<script type="text/javascript">
    /*''' <summary>
    ''' Vuelve a la pagina anterior. 
    ''' </summary>
    ''' <remarks>Llamada desde: Cuando se pincha al enlace ("Volver"); Tiempo m�ximo:0,1</remarks>*/			
    function IrASeguimiento()
    {
        switch (tipoSolicitud) {

            case '13':
                window.open("<%=ConfigurationManager.AppSettings("rutaPM2008")%>Tareas/VisorSolicitudes.aspx?TipoVisor=5", "_top");
                break;            
            case '14':
                window.open("<%=ConfigurationManager.AppSettings("rutaPM2008")%>Tareas/VisorSolicitudes.aspx?TipoVisor=7", "_top");
                break;
            default:
                window.open("<%=ConfigurationManager.AppSettings("rutaPM2008")%>Tareas/VisorSolicitudes.aspx", "_top");
            
        }

    }	
</script>
<body>
    <form id="Form1" method="post" runat="server">
        <asp:ScriptManager ID="sm" runat="server">
        </asp:ScriptManager>
        
        <div style="width:100%; position:fixed; z-index:100; background-color:white;">
            <uc1:menu ID="Menu1" runat="server"></uc1:menu>
        </div>

        <div style="padding-left: 15px; padding-bottom: 15px; padding-right: 15px; padding-top:14em;">

             <div id="pnlCabecera" align="center" style="background-color: whitesmoke; font-family: Arial; width: 99%; position: relative; border-radius: 5px; border: 0px none; box-shadow: rgb(119, 136, 153) 3px 3px 3px;">
	
             <table style="height: 35%; width: 70%; padding-top: 15px; padding-bottom: 15px; padding-left: 5px" cellspacing="0" cellpadding="1" border="0">
                                <tr>
                                    <td colspan="4" align="center" style="height: 100px;font-size: 18px;">
                                        <asp:Label ID="lblAccion" runat="server" CssClass="titulo" ></asp:Label>
                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblId" runat="server" CssClass="captionBlue"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblIdBD" runat="server" CssClass="captionDarkBlue"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblLitCreadoPor" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px" Text="Creado por"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCreadoPor" runat="server" CssClass="label"></asp:Label>
                                    </td>
                                </tr> 

                                <tr>

                                    <td>
                                        <asp:Label ID="lblLitFecAlta" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px" Text="Fecha alta"></asp:Label>
                                    </td>
                                    <td>

                                        <asp:Label ID="lblFecAlta" runat="server" CssClass="label"></asp:Label>
                                    </td>
                                    <td>
                                       <asp:Label ID="lblLitTipo" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px" Text="Tipo"></asp:Label>
                                    </td>
                                    <td>
                                         <asp:Label ID="lblTipo" runat="server" CssClass="label"></asp:Label>
                                    </td>

                                </tr>
                                <tr>
                                    <td  style="height: 151px" colspan="4" align="center" >
                                     <a onclick="IrASeguimiento()" href="#" name="lblVolver" runat="server">
                                         <img alt="" src="../images/volver_bullet_cuad_b.GIF"  style="vertical-align: bottom;"/>
                                         <asp:Label ID="lblVolver" runat="server" CssClass="volver">lblVolver</asp:Label></a>
                                    </td>
                                </tr>
                               
                                
                </table>
            </div>
        </div>



<%--        <div  align="center" style="padding-left: 15px; padding-bottom: 15px; padding-top:2em;">
            <div id="pnlCabecera" style="background-color: whitesmoke; font-family: Arial; width:70%; height:60%; position: relative; border-radius: 5px; border: 0px none; box-shadow: rgb(119, 136, 153) 3px 3px 3px;">
                <div  style="width:100%;" align="center" >
                    <asp:Label ID="lblAccion" runat="server" CssClass="captionDarkBlue" ></asp:Label>
                </div>

                <div  style="width:100%;" align="center" >
                    
                            <a onclick="IrASeguimiento()" href="#" name="lblVolver" runat="server"><asp:Label ID="lblVolver" runat="server" CssClass="volver">lblVolver</asp:Label></a>

                </div>

                <table  cellpadding="1" width="90%" border="0" height="60%">
                    <tr>
                        <td colspan="2" style="height: 50px"><asp:Label ID="lblTitulo" runat="server" CssClass="Titulo"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="height: 51px" width="25%"><asp:Label ID="lblId" runat="server" CssClass="captionBlue"></asp:Label></td>
                        <td><asp:Label ID="lblIdBD" runat="server" CssClass="captionDarkBlue"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 73px"></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                        </td>
                    </tr>
                </table>


            </div> 
        </div> --%>
        
       
    </form>
</body>
</html>
