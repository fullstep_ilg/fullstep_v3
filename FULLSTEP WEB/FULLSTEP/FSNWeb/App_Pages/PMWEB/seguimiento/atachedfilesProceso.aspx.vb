
    Public Class atachedfilesProceso
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblDescargar As System.Web.UI.WebControls.Label
        Protected WithEvents imgDescarga As System.Web.UI.WebControls.ImageButton
        Protected WithEvents uwgFiles As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
        Protected WithEvents lblProceso As System.Web.UI.WebControls.Label
        Protected WithEvents lblDenProceso As System.Web.UI.WebControls.Label
        Protected WithEvents lblGrupo As System.Web.UI.WebControls.Label
        Protected WithEvents lblDenGrupo As System.Web.UI.WebControls.Label
        Protected WithEvents lblItem As System.Web.UI.WebControls.Label
        Protected WithEvents lblDenItem As System.Web.UI.WebControls.Label
        Protected WithEvents tblCabecera As System.Web.UI.HtmlControls.HtmlTable
        Protected WithEvents txtAnyo As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents txtGMN1 As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents txtProce As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents txtGrupo As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents txtItem As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents txtProve As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents txtOfe As System.Web.UI.HtmlControls.HtmlInputHidden
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region


    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
   "<script DEFER=true language=""{0}"">{1}</script>"
       
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim mlAnyo As Long
        Dim msGMN1 As String
        Dim mlProce As Long
        Dim msGrupo As String
        Dim msCodGrupo As String
        Dim mlItem As Long
        Dim msProveedor As String
        Dim mlOfe As Long
        Dim oProceso As FSNServer.Proceso
        Dim oAdjuntos As FSNServer.Adjuntos
        Dim oDS As DataSet
        Dim oRow As DataRow
        Dim oItemRow As DataRow

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AttachFileProcesos

        mlAnyo = Request("Anyo")
        msGMN1 = Request("GMN1")
        mlProce = Request("Proce")

        If Request("Grupo") <> Nothing And Request("Grupo") <> "null" Then
            msGrupo = Request("Grupo")
            msCodGrupo = Request("GrupoCod")
        Else
            msGrupo = Nothing
            msCodGrupo = Nothing
        End If
        If Request("Item") <> Nothing And Request("Item") <> "null" Then
            mlItem = Request("Item")
        Else
            mlItem = Nothing
        End If
        If Request("Prove") <> Nothing And Request("Prove") <> "null" Then
            msProveedor = Request("Prove")
        Else
            msProveedor = Nothing
        End If
        If Request("OFE") <> Nothing And Request("OFE") <> "null" Then
            mlOfe = Request("OFE")
        Else
            mlOfe = Nothing
        End If

        txtAnyo.Value = mlAnyo
        txtGMN1.Value = msGMN1
        txtProce.Value = mlProce
        txtGrupo.Value = msGrupo
        txtItem.Value = mlItem
        txtProve.Value = msProveedor
        txtOfe.Value = mlOfe

        'Textos:
        oProceso = FSNServer.Get_Object(GetType(FSNServer.Proceso))
        oProceso.Anyo = mlAnyo
        oProceso.GMN1 = msGMN1
        oProceso.NumProce = mlProce
        oProceso.Instancia = Request("Instancia")
        oProceso.Load(Idioma)

        lblDenProceso.Text = oProceso.Anyo & "/" & oProceso.GMN1 & "/" & oProceso.NumProce & " " & oProceso.Den

        If msGrupo <> Nothing Then
            For Each oRow In oProceso.Grupos.Rows
                If oRow.Item("GRUPO") = msGrupo Then
                    lblDenGrupo.Text = msCodGrupo & " - " & oRow.Item("DEN_GRUPO")

                    If mlItem <> Nothing And mlItem <> 0 Then
                        For Each oItemRow In oRow.GetChildRows("REL_GRUPO_ITEM")
                            If oItemRow.Item("ID") = mlItem Then
                                lblDenItem.Text = DBNullToSomething(oItemRow.Item("ART")) & " - " & DBNullToSomething(oItemRow.Item("DESCR"))
                            End If
                        Next
                    Else
                        tblCabecera.Rows(3).Visible = False
                    End If
                End If
            Next
        Else
            tblCabecera.Rows(2).Visible = False
            tblCabecera.Rows(3).Visible = False
        End If

        lblProceso.Text = Textos(10)
        lblGrupo.Text = Textos(11)
        lblItem.Text = Textos(12)
        lblDescargar.Text = Textos(0)

        'Carga los adjuntos:
        oAdjuntos = FSNServer.Get_Object(GetType(FSNServer.Adjuntos))
        oAdjuntos.LoadAdjuntosProceso(mlAnyo, msGMN1, mlProce, msGrupo, mlItem, msProveedor, mlOfe)

        oDS = oAdjuntos.Data

        'Rellena la grid:
        uwgFiles.DataSource = oDS
        uwgFiles.DataBind()

        uwgFiles.Bands(0).Columns.FromKey("ID").Hidden = True
        uwgFiles.Bands(0).Columns.FromKey("OFE").Hidden = True
        uwgFiles.Bands(0).Columns.FromKey("PROVE").Hidden = True
        uwgFiles.Bands(0).Columns.FromKey("BYTES").Hidden = True

        uwgFiles.Bands(0).Columns.FromKey("FECALTA").Header.Caption = Textos(1)
        uwgFiles.Bands(0).Columns.FromKey("FECALTA").Width = Unit.Percentage(10)
        uwgFiles.Bands(0).Columns.FromKey("FECALTA").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.Yes
        uwgFiles.Bands(0).Columns.FromKey("NOM").Header.Caption = Textos(2)
        uwgFiles.Bands(0).Columns.FromKey("NOM").Width = Unit.Percentage(40)
        uwgFiles.Bands(0).Columns.FromKey("NOM").CellMultiline = Infragistics.WebUI.UltraWebGrid.CellMultiline.Yes
        uwgFiles.Bands(0).Columns.FromKey("NOM").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.Yes
        uwgFiles.Bands(0).Columns.FromKey("DATASIZE").Header.Caption = Textos(3)
        uwgFiles.Bands(0).Columns.FromKey("DATASIZE").Width = Unit.Percentage(5)
        uwgFiles.Bands(0).Columns.FromKey("DATASIZE").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.Yes
        uwgFiles.Bands(0).Columns.FromKey("COMENT").Header.Caption = Textos(4)
        uwgFiles.Bands(0).Columns.FromKey("COMENT").Width = Unit.Percentage(40)
        uwgFiles.Bands(0).Columns.FromKey("COMENT").CellMultiline = Infragistics.WebUI.UltraWebGrid.CellMultiline.Yes
        uwgFiles.Bands(0).Columns.FromKey("DESCARGAR").Header.Caption = Textos(5)
        uwgFiles.Bands(0).Columns.FromKey("ELIMINAR").ServerOnly = True
        uwgFiles.Bands(0).Columns.FromKey("SUSTITUIR").ServerOnly = True
        uwgFiles.Bands(0).Columns.FromKey("DESCARGAR").Move(9)
        uwgFiles.DataBind()

        uwgFiles.DisplayLayout.HeaderStyleDefault.CssClass = "CabeceraCampoArchivo"

        Dim sScript As String
        Dim sClientId As String = Replace(uwgFiles.ClientID, "_", "x")
        sScript = "document.getElementById(""" + sClientId + "_addBox"").parentNode.style.height=""0px"""
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        Page.ClientScript.RegisterStartupScript(Me.GetType(),"startItKey", sScript)
    End Sub

    Private Sub uwgFiles_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgFiles.InitializeRow
        e.Row.Cells.FromKey("ELIMINAR").Value = "<img border=""0"" align=""center"" src=""" & ConfigurationManager.AppSettings("rutaPM") + "_common/images/eliminar.gif"">"
        e.Row.Cells.FromKey("DESCARGAR").Value = "<img border=""0"" align=""center"" src=""" & ConfigurationManager.AppSettings("rutaPM") + "_common/images/descargar.gif"" onclick=""Descargar(" & e.Row.Cells.FromKey("ID").Value & ")"">"
        e.Row.Cells.FromKey("SUSTITUIR").Value = "<img border=""0"" align=""center"" src=""" & ConfigurationManager.AppSettings("rutaPM") + "_common/images/sustituir.gif"">"
    End Sub

    Private Sub uwgFiles_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwgFiles.InitializeLayout
        uwgFiles.Bands(0).Columns.FromKey("FECALTA").Format = FSNUser.DateFormat.ShortDatePattern
    End Sub
End Class

