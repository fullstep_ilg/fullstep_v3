<%@ Page Language="vb" AutoEventWireup="false" Codebehind="detallepedidovis.aspx.vb" Inherits="Fullstep.FSNWeb.detallepedidovis" EnableEventValidation="false"%>
<%@ Register TagPrefix="igtbl" Namespace="Infragistics.WebUI.UltraWebGrid" Assembly="Infragistics.WebUI.UltraWebGrid.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<script type="text/javascript"><!--
		function uwgLineas_CellClickHandler(gridName, cellId, button){
            var row = igtbl_getRowById(cellId); 
			var columnClicked = igtbl_getColumnById(cellId)
			
			codDest= row.getCell(5).getValue()
			codUP= row.getCell(3).getValue()
			if (columnClicked.Key=="DESTINO" && codDest!=null){
                var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/denominaciondest.aspx?codDest=" + codDest,"_blank", "width=400,height=180,status=yes,resizable=no,top=200,left=250");
                newWindow.focus();
			}
			else {
    			if (columnClicked.Key=="UNIDAD" && codUP!=null)	{
				    newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/denominacionUP.aspx?codUP=" + codUP,"_blank", "width=400,height=100,status=yes,resizable=no,top=200,left=250");
                    newWindow.focus();
			    }
			}
		}
		
		function irADetalleUnidad(Cod)
		{
			var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/denominacionUP.aspx?codUP=" + Cod.toString(),"_blank", "width=400,height=100,status=yes,resizable=no,top=200,left=250");
            newWindow.focus();
		}
		
		function DetalleProveedor(Cod)
		{
			var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detalleproveedor.aspx?codProv=" + Cod.toString(),"_blank", "width=500,height=200,status=yes,resizable=no,top=200,left=250");
            newWindow.focus();
		}
		
		function DetallePersona(Cod)
		{
			var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detallepersona.aspx?CodPersona=" + Cod.toString(),"_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
            newWindow.focus();
		}
		
		function show_atached_files(Instancia,Pedido,Linea)
		{
		    var newWindow = window.open("atachedfilesPedido.aspx?Instancia=" + Instancia + "&Pedido=" + Pedido + "&Linea=" + Linea,"_blank", "width=750,height=350,status=yes,resizable=no,top=200,left=200");
            newWindow.focus();
		}
		
		function imprimir(Instancia,Pedido)
		{
		window.open("detallepedidovis.aspx?Pedido=" + Pedido + "&Instancia=" + Instancia +  "&PDF=1","fraPMDetallePedidoHidden")
		}
		
--></script>	
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:label id="lblDetalle" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 24px"
				runat="server" CssClass="Titulo" Width="480px"></asp:label>
			<TABLE class="bordeadoAzul" id="Table1" style="Z-INDEX: 103; LEFT: 16px; POSITION: absolute; TOP: 56px; HEIGHT: 75px"
				cellSpacing="1" cellPadding="1" width="97%" border="0">
				<TR>
					<TD style="WIDTH: 157px"><asp:label id="lblSolicitud" runat="server" CssClass="CaptionBlue" Width="160px"></asp:label></TD>
					<TD><asp:label id="lblNumSol" runat="server" CssClass="CaptionDarkBlue"></asp:label></TD>
					<TD style="HEIGHT: 38px" vAlign="top" align="right"><INPUT language="javascript" class="botonPMWEB" id="cmdImprimir" onclick="return imprimir();"
							type="button" value="PDF" name="cmdImprimir" runat="server">&nbsp;</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 157px"><asp:label id="lblPedido" runat="server" CssClass="CaptionBlue" Width="160px"></asp:label></TD>
					<TD><asp:label id="lblNumPed" runat="server" CssClass="CaptionDarkBlue" Width="272px"></asp:label></TD>
					<TD><asp:label id="lblNumPedERP" runat="server" CssClass="CaptionBlue"></asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 157px"><asp:label id="lblProve" runat="server" CssClass="CaptionBlue" Width="160px"></asp:label></TD>
					<TD><nobr><asp:label id="lblProveBD" runat="server" CssClass="CaptionDarkBlue"></asp:label>&nbsp;
							<a id="cmdDetalleProve" class="aPMWeb" style="CURSOR: hand" name="cmdDetalleProve" runat="server"><IMG src="images/help.gif" border="0"></a></nobr></TD>
					<TD><asp:label id="lblEstado" runat="server" CssClass="CaptionDarkBlue" Width="328px"></asp:label></TD>
				</TR>
			</TABLE>
			<TABLE id="Table2" style="Z-INDEX: 102; LEFT: 16px; POSITION: absolute; TOP: 136px; HEIGHT: 0.86%"
				width="97%" border="0">
				<TR>
					<TD class="bordeadoAzul">
						<TABLE class="bordeadoAzul" id="tblPedido" cellSpacing="1" cellPadding="1"
							width="100%" border="0" runat="server">
							<TR>
								<TD><asp:label id="lblDatosGen" runat="server" CssClass="CaptionBlue" Width="100%">DDatos generales del pedido</asp:label></TD>
								<td><asp:label id="lblImporteTotal" runat="server" CssClass="CaptionBlue">DImporte:</asp:label><asp:label id="lblImporteTotalValor" runat="server" CssClass="CaptionDarkBlue"></asp:label></td>
							</TR>
							<TR>
								<TD class="subtitulo" vAlign="bottom" bgColor="dimgray" colSpan="2" height="3"></TD>
							</TR>
							<TR>
								<TD colSpan="2">
									<TABLE id="tblDatosPedido" width="100%" height="100%" border="0" runat="server">
										<TR>
											<TD valign="top"><nobr><asp:label id="lblFecha" runat="server" CssClass="captionBlueSmall">DFechaEmision:</asp:label></nobr></TD>
											<TD width="90%"><asp:label id="lblFechaBD" runat="server" CssClass="captionBlueSmall" Width="100%"></asp:label></TD>
										</TR>
										<TR>
											<TD valign="top"><nobr><asp:label id="lblPeticionario" runat="server" CssClass="captionBlueSmall">DPeticionario:</asp:label></nobr></TD>
											<TD width="90%"><nobr><asp:label id="lblPeticionarioBD" runat="server" CssClass="captionBlueSmall"></asp:label>&nbsp;
													<a id="cmdDetallePet" class="aPMWeb" style="CURSOR: hand" name="cmdDetallePet" runat="server"><IMG src="images/help.gif" border="0"></a></nobr></TD>
										</TR>
										<TR>
											<TD valign="top"><nobr><asp:label id="lblReceptor" runat="server" CssClass="captionBlueSmall">DReceptor:</asp:label></nobr></TD>
											<TD width="90%"><nobr><asp:label id="lblReceptorBD" runat="server" CssClass="captionBlueSmall"></asp:label>&nbsp;
													<a id="cmdDetalleRecep" class="aPMWeb" style="CURSOR: hand" name="cmdDetalleRecep" runat="server"><IMG src="images/help.gif" border="0"></a></nobr></TD>
										</TR>
										<TR>
											<TD valign="top"><nobr><asp:label id="lblObservaciones" runat="server" CssClass="captionBlueSmall"> DObesrvaciones Generales:</asp:label></nobr></TD>
											<TD width="90%"><asp:label id="lblObservacionesBD" runat="server" CssClass="captionBlueSmall" Width="100%"></asp:label></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="bordeadoAzul">
						<TABLE class="bordeadoAzul" id="tblLineas" cellSpacing="1" cellPadding="1"
							width="100%" border="0" runat="server">
							<TR>
								<TD colSpan="2"><asp:label id="lblLineas" runat="server" CssClass="CaptionBlue" Width="100%">DLineas de Pedido</asp:label></TD>
								<td><asp:label id="lblCantidad" runat="server" CssClass="CaptionBlue">DCantidad</asp:label></td>
								<td><asp:label id="lblUnidad" runat="server" CssClass="CaptionBlue">DUnidad</asp:label></td>
								<td><asp:label id="lblPrecioUd" runat="server" CssClass="CaptionBlue">DPrecio UD</asp:label></td>
								<td><asp:label id="lblImporte" runat="server" CssClass="CaptionBlue">DImporte</asp:label></td>
								<td><asp:label id="lblFecDeseada" runat="server" CssClass="CaptionBlue">DFec Deseada</asp:label></td>
								<td><asp:label id="lblFecProve" runat="server" CssClass="CaptionBlue">DFec Ent. Prove</asp:label></td>
								<td><asp:label id="lblObl" runat="server" CssClass="CaptionBlue">DObl.</asp:label></td>
							</TR>
							<TR>
								<TD class="subtitulo" vAlign="bottom" bgColor="dimgray" colSpan="9" height="3"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD align="center"><a id="lblCerrar" class="aPMWeb" onclick="window.parent.close()" href="#" name="lblCerrar" runat="server">cerrar</a></TD>
				</TR>
				<tr height="100%">
					<td height="100%"></td>
				</tr>
			</TABLE>
		</form>
	</body>
</html>
