
Public Class NWDetalleSolicitud
	Inherits FSNPage

#Region " Web Form Designer Generated Code "
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub
	Protected WithEvents uwtGrupos As Infragistics.WebUI.UltraWebTab.UltraWebTab
	Protected WithEvents HyperDetalle As System.Web.UI.WebControls.HyperLink
	Protected WithEvents lblCamposObligatorios As System.Web.UI.WebControls.Label
	Protected WithEvents Instancia As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents Version As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents txtIdTipo As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents txtEnviar As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents uwPopUpAcciones As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
	Protected WithEvents divAcciones As System.Web.UI.HtmlControls.HtmlGenericControl
	Protected WithEvents Bloque As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents Rol As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents cadenaespera As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents BotonCalcular As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents PantallaMaper As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents VinculacionesIdGuardar As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents VinculacionesPrimerGuardar As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents PantallaVinculaciones As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents uwPopUpListados As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
	Protected WithEvents divListados As System.Web.UI.HtmlControls.HtmlGenericControl
	Protected WithEvents divForm As System.Web.UI.HtmlControls.HtmlGenericControl
	Protected WithEvents frmAlta As System.Web.UI.HtmlControls.HtmlForm
	Protected WithEvents frmDetalle As System.Web.UI.HtmlControls.HtmlForm
	Protected WithEvents hidVolver As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents divForm2 As System.Web.UI.HtmlControls.HtmlGenericControl
	Protected WithEvents divForm3 As System.Web.UI.HtmlControls.HtmlGenericControl
	Protected WithEvents divProgreso As System.Web.UI.HtmlControls.HtmlGenericControl
	Protected WithEvents lblProgreso As System.Web.UI.WebControls.TextBox
	Protected WithEvents tblProgreso As System.Web.UI.HtmlControls.HtmlTable
	Protected WithEvents imgProgreso As System.Web.UI.WebControls.Image
	Protected WithEvents denFavorita As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents SolicitudFav As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents Observadores As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents SoloLectura As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents FSNPageHeader As Global.Fullstep.FSNWebControls.FSNPageHeader
	Protected WithEvents lblLitImporte As System.Web.UI.WebControls.Label
	Protected WithEvents lblLitCreadoPor As System.Web.UI.WebControls.Label
	Protected WithEvents lblLitEstado As System.Web.UI.WebControls.Label
	Protected WithEvents lblLitFecAlta As System.Web.UI.WebControls.Label
	Protected WithEvents lblLitTipo As System.Web.UI.WebControls.Label
	Protected WithEvents lblIDInstanciayEstado As System.Web.UI.WebControls.Label
	Protected WithEvents lblImporte As System.Web.UI.WebControls.Label
	Protected WithEvents lblCreadoPor As System.Web.UI.WebControls.Label
	Protected WithEvents lblEstado As System.Web.UI.WebControls.Label
	Protected WithEvents lblFecAlta As System.Web.UI.WebControls.Label
	Protected WithEvents lblTipo As System.Web.UI.WebControls.Label
	Protected WithEvents imgInfCreadoPor As Global.System.Web.UI.WebControls.Image
	Protected WithEvents imgInfTipo As Global.System.Web.UI.WebControls.Image
	Protected WithEvents FSNPanelDatosPeticionario As Global.Fullstep.FSNWebControls.FSNPanelInfo
	Protected WithEvents FSNPanelDatosProveedor As Global.Fullstep.FSNWebControls.FSNPanelInfo
	Protected WithEvents Menu1 As MenuControl
	'NOTE: The following placeholder declaration is required by the Web Form Designer.
	'Do not delete or move it.
	Private designerPlaceholderDeclaration As System.Object
	Private _oInstancia As FSNServer.Instancia
	Protected ReadOnly Property oInstancia() As FSNServer.Instancia
		Get
			If _oInstancia Is Nothing Then
				If Me.IsPostBack Then
					_oInstancia = CType(Cache("oInstancia" & FSNUser.Cod), FSNServer.Instancia)
				Else
					_oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
					_oInstancia.ID = Request("Instancia")

					If Request("ComboParticipante") <> "" Then
						bComboObservador = (Request("ComboParticipante") = 3)
						Me.Observadores.Value = IIf(bComboObservador, "1", "0")
						Session("FiltroSeguimientoParticipante") = Request("ComboParticipante")
					End If

					'Carga los datos de la instancia:
					_oInstancia.Load(Idioma, True)

					If _oInstancia.Peticionario <> FSNUser.CodPersona AndAlso _oInstancia.Estado = TipoEstadoSolic.Rechazada Then
						_oInstancia.Peticionario = _oInstancia.GetPeticionario_Rol(_oInstancia.ID)
						If _oInstancia.Peticionario = FSNUser.CodPersona Then
							bComboObservador = False 'Por si ha entrado como observador por sustituci�n
						End If
					End If

					Dim bEsObsyPart As Boolean
					If Request("ObsYPart") <> Nothing Then
						bEsObsyPart = Request("ObsYPart")
					End If
					_oInstancia.CargarCamposInstancia(Idioma, FSNUser.CodPersona, , True, bComboObservador, FSNUser.DateFmt, , , , bEsObsyPart)
					_oInstanciaGrupos = _oInstancia.Grupos

					If Not _oInstancia Is Nothing Then Me.InsertarEnCache("oInstancia" & FSNUser.Cod, _oInstancia)
					If Not _oInstanciaGrupos Is Nothing Then Me.InsertarEnCache("oInstanciaGrupos" & FSNUser.Cod, _oInstanciaGrupos)
				End If
			End If
			Return _oInstancia
		End Get
	End Property
	Private _oInstanciaGrupos As FSNServer.Grupos
	Protected ReadOnly Property oInstanciaGrupos() As FSNServer.Grupos
		Get
			If _oInstanciaGrupos Is Nothing Then
				If Me.IsPostBack Then
					_oInstanciaGrupos = CType(Cache("oInstanciaGrupos" & FSNUser.Cod), FSNServer.Grupos)
				Else
					_oInstancia.CargarCamposInstancia(Idioma, FSNUser.CodPersona, , True)
					_oInstanciaGrupos = _oInstancia.Grupos
				End If
			End If
			Return _oInstanciaGrupos
		End Get
	End Property
	Private _bSoloLectura As Boolean = False
	Protected Property bSoloLectura() As Boolean
		Get
			_bSoloLectura = ViewState("bSoloLectura")
			Return _bSoloLectura
		End Get
		Set(ByVal value As Boolean)
			_bSoloLectura = value
			ViewState("bSoloLectura") = _bSoloLectura
		End Set
	End Property
	Private _bComboObservador As Boolean = False
	Protected Property bComboObservador() As Boolean
		Get
			_bComboObservador = ViewState("bComboObservador")
			Return _bComboObservador
		End Get
		Set(ByVal value As Boolean)
			_bComboObservador = value
			ViewState("bComboObservador") = _bComboObservador
		End Set
	End Property
	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: This method call is required by the Web Form Designer
		'Do not modify it using the code editor.
		InitializeComponent()
	End Sub
#End Region
	Private Const IncludeScriptFormat As String = ControlChars.CrLf & "<script type=""{0}"" src=""{1}""></script>"
	Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
	''' <summary>
	''' Carga el detalle de la solicitud.
	''' </summary>
	''' <remarks>Llamada desde; Tiempo m�ximo:2seg. Revisador por mmv(18/10/2011)</remarks>
	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Response.Expires = -1
		If Page.IsPostBack Then
			CargarCamposGrupo()
			Exit Sub
		Else
			VinculacionesPrimerGuardar.Value = 0
			VinculacionesIdGuardar.Value = 0
		End If

		Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
		Dim lIndex As Integer = 0
		Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden
		Dim tipoSolicitud As Integer = 0

		CargarTextoEspera()

		tipoSolicitud = oInstancia.Solicitud.TipoSolicit

		Select Case tipoSolicitud
			Case TiposDeDatos.TipoDeSolicitud.Factura
				Menu1.OpcionMenu = "Facturacion"
				Menu1.OpcionSubMenu = "Seguimiento"
			Case TiposDeDatos.TipoDeSolicitud.Encuesta
				Menu1.OpcionMenu = "Calidad"
				Menu1.OpcionSubMenu = "Encuestas"
			Case TiposDeDatos.TipoDeSolicitud.SolicitudQA
				Menu1.OpcionMenu = "Calidad"
				Menu1.OpcionSubMenu = "SolicitudesQA"
			Case Else
				Menu1.OpcionMenu = "Procesos"
				Menu1.OpcionSubMenu = "Seguimiento"
		End Select

		ModuloIdioma = TiposDeDatos.ModulosIdiomas.DetalleSeguimiento
		ConfigurarCabeceraMenu()
		ConfigurarEstadoSolicitud()

		If Not ClientScript.IsStartupScriptRegistered("oRol_Id") Then _
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "oRol_Id", "var oRol_Id=" & oInstancia.RolActual & ";", True)
		If Not ClientScript.IsStartupScriptRegistered("tipoSolicitud") Then
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "tipoSolicitud", "<script>var tipoSolicitud = '" & tipoSolicitud & "';</script>")
		End If

		Instancia.Value = oInstancia.ID
		Version.Value = oInstancia.Version

		If oInstanciaGrupos Is Nothing Then
			'El Usuario no ha participado en la solicitud.
			Select Case tipoSolicitud
				Case TiposDeDatos.TipoDeSolicitud.Factura, TiposDeDatos.TipoDeSolicitud.Encuesta, TiposDeDatos.TipoDeSolicitud.SolicitudQA
					Response.Redirect("../workflow/UsuarioNoParticipante.aspx?TipoSolicitud=" & tipoSolicitud)
				Case Else
					Response.Redirect("../workflow/UsuarioNoParticipante.aspx")
			End Select
		End If

		SolicitudFav.Value = oInstancia.Solicitud.ID
		uwtGrupos.Tabs.Clear()
		AplicarEstilosTab(uwtGrupos)

		'Textos de idiomas:
		lblLitTipo.Text = Textos(0)
		lblLitCreadoPor.Text = Textos(40) & ":"
		lblLitEstado.Text = Textos(33) & ":"
		lblLitFecAlta.Text = Textos(41) & ":"
		HyperDetalle.Text = Textos(5)
		lblCamposObligatorios.Text = Textos(39)

		If oInstancia.Solicitud.ValidacionesIntegracion Is Nothing Then _
			oInstancia.Solicitud.ValidacionesIntegracion = FSNServer.Get_Object(GetType(FSNServer.PM_ValidacionesIntegracion))

		PantallaMaper.Value = oInstancia.Solicitud.ValidacionesIntegracion.ControlaConMaper()
		PantallaVinculaciones.Value = oInstancia.ExisteVinculaciones
		'*********************************************************************************************************
		lblFecAlta.Text = FormatDate(oInstancia.FechaAlta, FSNUser.DateFormat)
		If oInstancia.PeticionarioProve = "" Then
			lblCreadoPor.Text = oInstancia.NombrePeticionario
		Else
			lblCreadoPor.Text = oInstancia.PeticionarioProve + " (" + oInstancia.NombrePeticionario + ")"
		End If
		lblTipo.Text = oInstancia.Solicitud.Codigo + " - " + oInstancia.Solicitud.Den(Idioma)
		lblEstado.Text = oInstancia.CargarEstapaActual(Idioma)
		txtIdTipo.Value = oInstancia.Solicitud.ID
		Instancia.Value = oInstancia.ID

		imgInfTipo.Attributes.Add("onclick", "DetalleTipoSolicitud()")

		If Not oInstancia.CampoImporte Is Nothing Then
			lblLitImporte.Text = oInstancia.CampoImporte & ":"
			lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & oInstancia.Moneda
		Else
			If oInstancia.Solicitud.Tipo = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras Then
				lblLitImporte.Text = Textos(17)
				lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & oInstancia.Moneda
			Else
				lblImporte.Visible = False
			End If
		End If

		If FSNUser.AccesoCN Then
			If Not Page.ClientScript.IsClientScriptBlockRegistered("EntidadColaboracion") Then _
				Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "EntidadColaboracion",
					"var TipoEntidadColaboracion='SC'; var CodigoEntidadColaboracion ={identificador:" & oInstancia.ID &
					",texto:'" & JSText(oInstancia.ID & " - " & oInstancia.Solicitud.Den(Idioma)) & "'};", True)
		End If

		'Carga las propiedades del rol:
		Dim dsPermisosRol As DataSet
		dsPermisosRol = Session("FSN_User").VerDetallesPersona(oInstancia.ID)
		If dsPermisosRol.Tables.Count > 0 Then
			If dsPermisosRol.Tables(0).Rows(0).Item("VER_DETALLE_PER") = 0 Then
				imgInfCreadoPor.Visible = False
				lblCreadoPor.Visible = False
				lblLitCreadoPor.Visible = False
			Else
				If oInstancia.PeticionarioProve = "" Then
					imgInfCreadoPor.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosPeticionario.AnimationClientID & "', event, '" & FSNPanelDatosPeticionario.DynamicPopulateClientID & "', '" & oInstancia.Peticionario & "'); return false;")
				Else
					imgInfCreadoPor.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosProveedor.AnimationClientID & "', event, '" & FSNPanelDatosProveedor.DynamicPopulateClientID & "', '" & oInstancia.PeticionarioProve & "'); return false;")
				End If
			End If
			If oInstancia.IdWorkflow = Nothing Then  'Si no hay workflow no muestra el hiperv�nculo para el detalle del workflow
				HyperDetalle.Visible = False
			Else
				If dsPermisosRol.Tables(0).Rows(0).Item("VER_FLUJO") = 1 Then    'Si el rol no tiene permiso para ver el detalle del flujo.
					HyperDetalle.NavigateUrl = "NWhistoricoestados.aspx?Instancia=" + CStr(oInstancia.ID)
					HyperDetalle.Visible = True
				Else
					HyperDetalle.Visible = False
				End If
			End If
		End If

		CargarCamposGrupo()

		If oInstancia.Peticionario = FSNUser.CodPersona And (oInstancia.Estado = TipoEstadoSolic.Guardada _
		   Or oInstancia.Estado = TipoEstadoSolic.Rechazada) Then
			Dim oRol As FSNServer.Rol
			oInstancia.DevolverEtapaActual(Idioma, FSNUser.CodPersona)

			Rol.Value = oInstancia.RolActual
			Bloque.Value = oInstancia.Etapa

			If oInstancia.RolActual = 0 Or oInstancia.Etapa = 0 Then
			Else
				oRol = FSNServer.Get_Object(GetType(FSNServer.Rol))
				oRol.Id = oInstancia.RolActual
				oRol.Bloque = oInstancia.Etapa
				Me.VinculacionesIdGuardar.Value = oRol.CargarIdGuardar()

				'Participantes
				oRol.CargarParticipantes(Idioma, oInstancia.ID)
				If oRol.Participantes.Tables(0).Rows.Count > 0 Then
					Dim oucParticipantes As participantes

					oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
					oTabItem.Text = Textos(28) 'Participantes
					oTabItem.Key = "PARTICIPANTES"

					uwtGrupos.Tabs.Add(oTabItem)

					oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
					oTabItem.ContentPane.UserControlUrl = "../alta/participantes.ascx"

					oucParticipantes = oTabItem.ContentPane.UserControl
					oucParticipantes.Idi = Idioma
					oucParticipantes.TabContainer = uwtGrupos.ClientID

					oucParticipantes.dsParticipantes = oRol.Participantes

					oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden
					oInputHidden.ID = "txtPre_PARTICIPANTES"
					oInputHidden.Value = oucParticipantes.ClientID
					Me.FindControl("frmDetalle").Controls.Add(oInputHidden)
				End If
			End If
		End If

		GenerarScripts()
	End Sub
	''' <summary>
	''' Carga los Campos de los grupos de la solicitud
	''' </summary>
	''' <remarks></remarks>
	Private Sub CargarCamposGrupo()
		Dim oGrupo As FSNServer.Grupo
		Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
		Dim oucCampos As campos
		Dim oucDesglose As desgloseControl
		Dim oRow As DataRow
		Dim lIndex As Integer = 0
		Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden

		If Not oInstanciaGrupos Is Nothing Then
			If Not oInstanciaGrupos.Grupos Is Nothing Then
				For Each oGrupo In oInstanciaGrupos.Grupos
					oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden

					oInputHidden.ID = "txtPre_" + lIndex.ToString
                    oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                    Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
                    Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
                    oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(Idioma), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
                    oTabItem.Key = lIndex.ToString
					oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Hidden
					uwtGrupos.Tabs.Add(oTabItem)
					lIndex += 1

					If oGrupo.NumCampos <= 1 Then
						For Each oRow In oGrupo.DSCampos.Tables(0).Rows
							If oRow.Item("VISIBLE") = 1 Then
								Exit For
							End If

						Next
						If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Desglose Or (DBNullToSomething(oRow.Item("SUBTIPO")) = TiposDeDatos.TipoGeneral.TipoDesglose And DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.DesgloseActividad) Then
							If oRow.Item("VISIBLE") = 0 Then
								uwtGrupos.Tabs.Remove(oTabItem)
							Else
								oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
								oTabItem.ContentPane.UserControlUrl = "..\alta\desglose.ascx"
								oucDesglose = oTabItem.ContentPane.UserControl
								oucDesglose.ID = oGrupo.Id.ToString
								oucDesglose.Campo = oRow.Item("ID_CAMPO")
								oucDesglose.CampoOrigen = DBNullToDbl(oRow.Item("CAMPO_ORIGEN"))
								oucDesglose.TabContainer = uwtGrupos.ClientID
								oucDesglose.Instancia = oInstancia.ID
								oucDesglose.Version = oInstancia.Version
								oucDesglose.SoloLectura = bSoloLectura Or (oRow.Item("ESCRITURA") = 0)
								oucDesglose.PM = True
								oucDesglose.Observador = bComboObservador
								oucDesglose.Titulo = DBNullToSomething(oRow.Item("DEN_" & Idioma))
								oucDesglose.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
								oInputHidden.Value = oucDesglose.ClientID
							End If
						Else
							oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
							oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
							oucCampos = oTabItem.ContentPane.UserControl
							oucCampos.Instancia = oInstancia.ID
							oucCampos.ID = oGrupo.Id.ToString
							oucCampos.dsCampos = oGrupo.DSCampos
							oucCampos.IdGrupo = oGrupo.Id
							oucCampos.Idi = Idioma
							oucCampos.TabContainer = uwtGrupos.ClientID
							oucCampos.Version = oInstancia.Version
							oucCampos.SoloLectura = bSoloLectura
							oucCampos.InstanciaEstado = oInstancia.Estado
							oucCampos.InstanciaMoneda = oInstancia.Moneda
							oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
							oucCampos.PM = True
							oucCampos.Formulario = oInstancia.Solicitud.Formulario
							oucCampos.ObjInstancia = oInstancia
							oucCampos.Observador = bComboObservador
							oInputHidden.Value = oucCampos.ClientID
						End If
					Else
						oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
						oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
						oucCampos = oTabItem.ContentPane.UserControl
						oucCampos.Instancia = oInstancia.ID
						oucCampos.ID = oGrupo.Id.ToString
						oucCampos.dsCampos = oGrupo.DSCampos
						oucCampos.IdGrupo = oGrupo.Id
						oucCampos.Idi = Idioma
						oucCampos.TabContainer = uwtGrupos.ClientID
						oucCampos.Version = oInstancia.Version
						oInputHidden.Value = oucCampos.ClientID
						oucCampos.SoloLectura = bSoloLectura
						oucCampos.InstanciaEstado = oInstancia.Estado
						oucCampos.InstanciaMoneda = oInstancia.Moneda
						oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
						oucCampos.PM = True
						oucCampos.Formulario = oInstancia.Solicitud.Formulario
						oucCampos.ObjInstancia = oInstancia
						oucCampos.Observador = bComboObservador
					End If

					Me.FindControl("frmDetalle").Controls.Add(oInputHidden)
				Next
			End If
		End If
	End Sub
	''' <summary>
	''' Registra los script necesarios para la pantalla
	''' </summary>
	''' <remarks></remarks>
	Private Sub GenerarScripts()

		Dim sClientTextVars As String
		sClientTextVars = ""
		sClientTextVars += "vdecimalfmt='" + FSNUser.DecimalFmt + "';"
		sClientTextVars += "vthousanfmt='" + FSNUser.ThousanFmt + "';"
		sClientTextVars += "vprecisionfmt='" + FSNUser.PrecisionFmt + "';"
		sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
		Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsUser", sClientTextVars)

		Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")
		Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")
		Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeCambioTipoPedido", "<script>var mensajeCambioTipoPedido = '" & JSText(Textos(35)) & "' </script>")
		If Not Page.ClientScript.IsClientScriptBlockRegistered("varFila") Then _
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varFila", "<script>var sFila = '" & JSText(Textos(38)) & "' </script>")

		Dim sClientTexts As String
		sClientTexts = ""
		sClientTexts += "var arrTextosML = new Array();"
		sClientTexts += "arrTextosML[0] = '" + JSText(Textos(20)) + "';"
		sClientTexts += "arrTextosML[1] = '" + JSText(Textos(21)) + "';"
		sClientTexts += "arrTextosML[2] = '" + JSText(Textos(27)) + "';"
		sClientTexts += "arrTextosML[3] = '" + JSText(Textos(37)) + "';"
		sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
		Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

		Dim sScript As String
		Dim ilGMN1 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN1
		Dim ilGMN2 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN2
		Dim ilGMN3 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN3
		Dim ilGMN4 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN4
		sScript = ""
		sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
		sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
		sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
		sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
		sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
		Page.ClientScript.RegisterStartupScript(Me.GetType(), "LongitudesCodigosKey", sScript)

		Dim includeScript As String

		If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayDesgloses") Then
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")
		End If

		If Not Page.ClientScript.IsClientScriptBlockRegistered("volver") Then
			hidVolver.Value = Replace(Request("Volver"), "*", "&")
			includeScript = "function Volver() {" & vbCrLf
			If Request("volver") <> "" Then
				If InStr(Request("volver").ToLower, "visorsolicitudes.aspx") > 0 Then
					includeScript = includeScript & "top.location.href='" & Replace(Request("Volver"), "*", "&") & "';" & vbCrLf
					hidVolver.Value = Replace(Request("Volver"), "*", "&")
				Else
					includeScript = includeScript & "window.open('" & Replace(Request("Volver"), "*", "&") & "','_self');" & vbCrLf
				End If
			Else
				includeScript = includeScript & "window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?volver=1','_self');" & vbCrLf
			End If
			includeScript = includeScript & "}"
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "volver", includeScript, True)
		End If

		Me.FindControl("frmDetalle").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())
	End Sub
	''' Revisado por: sra. Fecha: 24/01/2012
	''' <summary>
	''' Configura la cabecera, muestra en el menu las posibles acciones
	''' Activamos los botones que pueden aparecer en la pantalla
	''' </summary>
	''' <remarks>Llamada desde:=Page_load; Tiempo m�ximo:=0,1seg.</remarks>
	Private Sub ConfigurarCabeceraMenu()
		FSNPageHeader.TituloCabecera = If(oInstancia.Den(Idioma) <> "", oInstancia.Den(Idioma), oInstancia.Solicitud.Den(Idioma))
		FSNPageHeader.UrlImagenCabecera = "images/SolicitudesPMSmall.jpg"

		FSNPageHeader.VisibleBotonVolver = True
		FSNPageHeader.VisibleBotonImpExp = True

		FSNPageHeader.TextoBotonEliminar = Textos(14)
		FSNPageHeader.TextoBotonImpExp = Textos(18)
		FSNPageHeader.TextoBotonVolver = Textos(23)
		FSNPageHeader.TextoBotonGuardar = Textos(15)
		FSNPageHeader.TextoBotonCalcular = Textos(16)
		FSNPageHeader.TextoBotonEnviarAFavoritos = Textos(30)
		FSNPageHeader.TextoBotonAprobar = Textos(31)
		FSNPageHeader.TextoBotonRechazar = Textos(32)
		FSNPageHeader.TextoBotonListados = Textos(29)
		FSNPageHeader.TextoBotonAccion1 = Textos(24) 'Otras acciones

		FSNPageHeader.OnClientClickVolver = "Volver(); return false;"
		FSNPageHeader.OnClientClickEliminar = "EliminarInstancia(); return false;"
		FSNPageHeader.OnClientClickGuardar = "Guardar(); return false;"
		FSNPageHeader.OnClientClickCalcular = "return CalcularCamposCalculados(); return false;"
		FSNPageHeader.OnClientClickImpExp = "return cmdImpExp_onclick(); return false;"
		FSNPageHeader.OnClientClickEnviarFavoritos = "return EnviarFavoritos(); return false;"


		If oInstancia.Peticionario = FSNUser.CodPersona Then 'Si es el peticionario podr� hacer algo, sino no: 
			Select Case oInstancia.Estado
				Case TipoEstadoSolic.Guardada   'Guardada
					FSNPageHeader.VisibleBotonEliminar = True
					FSNPageHeader.VisibleBotonGuardar = True
					FSNPageHeader.VisibleBotonEnviarAFavoritos = True
					FSNPageHeader.VisibleBotonAccion1 = True
				Case TipoEstadoSolic.Rechazada  'Rechazada    
					FSNPageHeader.VisibleBotonAccion1 = True
					FSNPageHeader.VisibleBotonEliminar = True
			End Select
		End If

		'Comprobamos si hay que hacer visible el boton de calcular
		If Not bComboObservador AndAlso oInstancia.Peticionario = FSNUser.CodPersona AndAlso (oInstancia.Estado = TipoEstadoSolic.Guardada Or oInstancia.Estado = TipoEstadoSolic.Rechazada) Then
			'SI EXISTE ALGUN CAMPO DE TIPO=3 (CALCULADO) HACEMOS VISIBLE EL BOTON DE CALCULAR
			If oInstanciaGrupos IsNot Nothing AndAlso oInstanciaGrupos.Grupos IsNot Nothing Then
				FSNPageHeader.VisibleBotonCalcular = oInstanciaGrupos.Grupos.OfType(Of FSNServer.Grupo).Where(Function(x) x.DSCampos.Tables(0).Rows.OfType(Of DataRow).Where(Function(y) y("TIPO") = TipoCampoPredefinido.Calculado).Any).Any()
			End If
			If FSNPageHeader.VisibleBotonCalcular Then BotonCalcular.Value = 1
		End If

		''''Carga las acciones posibles en el bot�n de Otras acciones (si es el peticionario) :
		If oInstancia.Peticionario = FSNUser.CodPersona And (oInstancia.Estado = TipoEstadoSolic.Guardada Or oInstancia.Estado = TipoEstadoSolic.Rechazada) Then
			Dim oRol As FSNServer.Rol
			Dim oPopMenu As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
			Dim oPopMenuListados As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
			Dim oItem As Infragistics.WebUI.UltraWebNavigator.Item
			Dim oItemListados As Infragistics.WebUI.UltraWebNavigator.Item

			oInstancia.DevolverEtapaActual(Idioma, FSNUser.CodPersona)

			If oInstancia.RolActual = 0 Or oInstancia.Etapa = 0 Then
				FSNPageHeader.VisibleBotonAccion1 = False
				FSNPageHeader.VisibleBotonGuardar = False
				FSNPageHeader.VisibleBotonEnviarAFavoritos = False
			Else
				oRol = FSNServer.Get_Object(GetType(FSNServer.Rol))
				oRol.Id = oInstancia.RolActual
				oRol.Bloque = oInstancia.Etapa

				Dim oDSAcciones As DataSet
				If oInstancia.Estado = TipoEstadoSolic.Rechazada = True Then
					oDSAcciones = oRol.CargarAcciones(Idioma, True)
				Else
					oDSAcciones = oRol.CargarAcciones(Idioma)
				End If

				oPopMenu = Me.uwPopUpAcciones
				Dim blnAprobar As Boolean = False
				Dim blnRechazar As Boolean = False
				Dim iAccionesRestarOtrasAcciones As Integer
				Dim iOtrasAcciones As Integer
				If oDSAcciones.Tables.Count > 0 Then
					If oDSAcciones.Tables(0).Rows.Count > 1 Then
						'Comprobamos si entre las acciones hay de tipo aprobar y rechazar
						For Each oRow As DataRow In oDSAcciones.Tables(0).Rows
							If DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
								blnAprobar = True
								iAccionesRestarOtrasAcciones = iAccionesRestarOtrasAcciones + 1
								If blnRechazar Then Exit For
							ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
								blnRechazar = True
								iAccionesRestarOtrasAcciones = iAccionesRestarOtrasAcciones + 1
								If blnAprobar Then Exit For
							End If
						Next
						iOtrasAcciones = oDSAcciones.Tables(0).Rows.Count - iAccionesRestarOtrasAcciones
						Dim cont As Integer = 0
						For Each oRow In oDSAcciones.Tables(0).Rows
							If iOtrasAcciones > 3 AndAlso DBNullToSomething(oRow.Item("RA_APROBAR")) <> 1 AndAlso DBNullToSomething(oRow.Item("RA_RECHAZAR")) <> 1 Then
								If IsDBNull(oRow.Item("DEN")) Then
									oItem = oPopMenu.Items.Add("&nbsp;")
								Else
									If oRow.Item("DEN") = "" Then
										oItem = oPopMenu.Items.Add("&nbsp;")
									Else
										oItem = oPopMenu.Items.Add(DBNullToStr(oRow.Item("DEN")))
									End If
								End If
								oItem.TargetUrl = "javascript:EjecutarAccion(" + oRow.Item("ACCION").ToString() + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "')"
							ElseIf DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
								FSNPageHeader.OnClientClickAprobar = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
								FSNPageHeader.TextoBotonAprobar = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
							ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
								FSNPageHeader.OnClientClickRechazar = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
								FSNPageHeader.TextoBotonRechazar = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
							Else
								If cont = 0 Then
									FSNPageHeader.OnClientClickAccion1 = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
									FSNPageHeader.TextoBotonAccion1 = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
									FSNPageHeader.VisibleBotonAccion1 = True
									cont = 1
								ElseIf cont = 1 Then
									FSNPageHeader.OnClientClickAccion2 = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
									FSNPageHeader.TextoBotonAccion2 = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
									FSNPageHeader.VisibleBotonAccion2 = True
									cont = 2
								Else
									FSNPageHeader.OnClientClickAccion3 = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
									FSNPageHeader.TextoBotonAccion3 = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
									FSNPageHeader.VisibleBotonAccion3 = True
								End If
							End If
						Next
						If blnAprobar Then
							FSNPageHeader.VisibleBotonAprobar = True
						End If
						If blnRechazar Then
							FSNPageHeader.VisibleBotonRechazar = True
						End If
						If iOtrasAcciones > 3 Then
							FSNPageHeader.VisibleBotonAccion1 = True
							FSNPageHeader.OnClientClickAccion1 = "return mostrarMenuAcciones(event,1);"
						End If
					Else
						If oDSAcciones.Tables(0).Rows.Count = 0 Then
							FSNPageHeader.VisibleBotonAccion1 = False
						Else
							Dim oRow As DataRow
							oRow = oDSAcciones.Tables(0).Rows(0)
							If DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
								FSNPageHeader.VisibleBotonAccion1 = False
								FSNPageHeader.VisibleBotonAprobar = True
								FSNPageHeader.OnClientClickAprobar = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
							ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
								FSNPageHeader.VisibleBotonAccion1 = False
								FSNPageHeader.VisibleBotonRechazar = True
								FSNPageHeader.OnClientClickRechazar = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
							Else
								FSNPageHeader.VisibleBotonAccion1 = True
								FSNPageHeader.OnClientClickAccion1 = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
								FSNPageHeader.TextoBotonAccion1 = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
							End If
						End If
					End If
				Else
					FSNPageHeader.VisibleBotonAccion1 = False
				End If
			End If

			''''Carga los listados posibles en el bot�n de Listados:
			oRol = FSNServer.Get_Object(GetType(FSNServer.Rol))
			oRol.Id = oInstancia.RolActual
			oRol.Bloque = oInstancia.Etapa

			Dim oDSListados As DataSet
			oDSListados = oRol.CargarListados(Idioma)

			oPopMenuListados = Me.uwPopUpListados

			If oDSListados.Tables.Count > 0 Then
				If oDSListados.Tables(0).Rows.Count > 1 Then
					FSNPageHeader.VisibleBotonListados = True
					FSNPageHeader.OnClientClickListados = "return mostrarMenuListados(event);"
					For Each oRow In oDSListados.Tables(0).Rows
						oItemListados = oPopMenuListados.Items.Add(oRow.Item("DEN"))
						oItemListados.TargetUrl = "javascript:LanzarListado('" + oRow.Item("ARCHIVO_RPT") + "'," + CStr(oInstancia.ID) + ")"
					Next
				Else
					If Not (oDSListados.Tables(0).Rows.Count = 0) Then
						Dim oRow As DataRow = oDSListados.Tables(0).Rows(0)
						FSNPageHeader.VisibleBotonListados = True
						FSNPageHeader.OnClientClickListados = "LanzarListado('" + oRow.Item("ARCHIVO_RPT") + "'," + CStr(oInstancia.ID) + "); return false;"
						FSNPageHeader.TextoBotonListados = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
					End If
				End If
			End If
		Else
			FSNPageHeader.VisibleBotonAccion1 = False
			FSNPageHeader.VisibleBotonListados = False
		End If
	End Sub
	Private Sub ConfigurarEstadoSolicitud()
		Dim sEtapaActual As String
		Select Case oInstancia.Estado
			Case TipoEstadoSolic.Guardada   'Guardada
				sEtapaActual = Textos(6)

			Case TipoEstadoSolic.Pendiente  'En curso
				sEtapaActual = Textos(25)
				bSoloLectura = True

			Case TipoEstadoSolic.SCPendiente, TipoEstadoSolic.SCAprobada, TipoEstadoSolic.SCRechazada, TipoEstadoSolic.SCAnulada, TipoEstadoSolic.SCCerrada
				sEtapaActual = Textos(26)
				bSoloLectura = True

			Case TipoEstadoSolic.Rechazada  'Rechazada    
				sEtapaActual = Textos(9)
				If oInstancia.Peticionario = FSNUser.CodPersona Then 'Si es el peticionario podr� hacer algo, sino no: 
					bSoloLectura = False
				Else
					bSoloLectura = True
				End If

			Case TipoEstadoSolic.Anulada  'Anulada
				sEtapaActual = Textos(10)
				If oInstancia.Peticionario = FSNUser.CodPersona Then 'Si es el peticionario podr� hacer algo, sino no: 
					bSoloLectura = False
				Else
					bSoloLectura = True
				End If

			Case TipoEstadoSolic.SCCerrada  'Cerrada  
				sEtapaActual = Textos(11)
				bSoloLectura = True
			Case Else
				sEtapaActual = oInstancia.CargarEstapaActual(Idioma, True)
				If sEtapaActual = "##" Then
					sEtapaActual = Textos(36)
				Else
					sEtapaActual = Textos(25)
				End If
		End Select
		lblIDInstanciayEstado.Text = oInstancia.ID & " (" & sEtapaActual & ")"
		Me.SoloLectura.Value = bSoloLectura
	End Sub
	''' <summary>
	''' Obtiene los textos de la pantalla de espera
	''' </summary>
	''' <remarks></remarks>
	Private Sub CargarTextoEspera()
		ModuloIdioma = TiposDeDatos.ModulosIdiomas.Espera

		Me.cadenaespera.Value = Textos(1)
		Me.lblProgreso.Text = Textos(2)
	End Sub
End Class