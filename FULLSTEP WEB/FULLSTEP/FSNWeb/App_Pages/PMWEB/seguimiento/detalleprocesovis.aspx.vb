Public Class detalleprocesovis
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblProceso As System.Web.UI.WebControls.Label
    Protected WithEvents lblResp As System.Web.UI.WebControls.Label
    Protected WithEvents lblEstado As System.Web.UI.WebControls.Label
    Protected WithEvents lblResponsable As System.Web.UI.WebControls.Label
    Protected WithEvents lblProcBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblCerrar As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblDatosGen As System.Web.UI.WebControls.Label
    Protected WithEvents tblDatosProceso As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblLitMaterial As System.Web.UI.WebControls.Label
    Protected WithEvents lblMaterial As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitMoneda As System.Web.UI.WebControls.Label
    Protected WithEvents lblMoneda As System.Web.UI.WebControls.Label
    Protected WithEvents lblApertura As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechaAper As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitFecLimite As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecLimite As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitAdjudicacion As System.Web.UI.WebControls.Label
    Protected WithEvents lblAdjudicacion As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitNecesidad As System.Web.UI.WebControls.Label
    Protected WithEvents lblNecesidad As System.Web.UI.WebControls.Label

    Protected WithEvents tblProceso As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents cmdImprimir As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents txtBody As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private oProceso As FSNServer.Proceso

    ''' <summary>
    ''' Carga inicial de la pantalla
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">evento de sistema</param>      
    ''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oFila As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oCelda As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oRow As DataRow
        Dim sEspecific As String
        Dim oAtribRow As DataRow
        Dim oProveRow As DataRow
        Dim oEspecRow As DataRow
        Dim sObservaciones As String

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.DetalleProceso

        oProceso = FSNServer.Get_Object(GetType(FSNServer.Proceso))
        oProceso.Anyo = Request("Anyo")
        oProceso.GMN1 = Request("GMN1")
        oProceso.NumProce = Request("Proce")
        oProceso.Instancia = Request("Instancia")
        oProceso.Load(Idioma)


        'Literales de los textos:
        lblTitulo.Text = Textos(0) 'Detalle de proceso
        lblProceso.Text = Textos(1) 'proceso:
        lblResp.Text = Textos(2) 'responsable:
        lblApertura.Text = Textos(3) 'apertura
        lblCerrar.InnerText = Textos(4) 'cerrar

        lblDatosGen.Text = Textos(26) 'Datos generales del proceso   
        lblLitMaterial.Text = Textos(27)  'Familia de material
        lblLitMoneda.Text = Textos(30)  'Moneda
        lblLitFecLimite.Text = Textos(31) 'Fecha l�mite para recibir ofertas
        lblLitAdjudicacion.Text = Textos(32) 'Adjudicaci�n
        lblLitNecesidad.Text = Textos(33) 'Necesidad

        lblProcBD.Text = oProceso.Anyo & "/" & oProceso.GMN1 & "/" & oProceso.NumProce & " - " & oProceso.Den
        lblResponsable.Text = oProceso.Responsable

        Me.cmdImprimir.Attributes("onclick") = "return imprimir(" + oProceso.Instancia.ToString() + "," + oProceso.Anyo.ToString + ",'" + oProceso.GMN1 + "'," + oProceso.NumProce.ToString + ");"

        Select Case oProceso.Estado
            Case TipoEstadoProceso.sinitems, TipoEstadoProceso.ConItemsSinValidar
                lblEstado.Text = Textos(13)  'Pendiente de validar apertura
            Case TipoEstadoProceso.validado, TipoEstadoProceso.Conproveasignados
                lblEstado.Text = Textos(14)  'Pendiente de asignar proveedores
            Case TipoEstadoProceso.conasignacionvalida
                lblEstado.Text = Textos(15)  'Pendiente de enviar peticiones
            Case TipoEstadoProceso.conpeticiones, TipoEstadoProceso.conofertas
                lblEstado.Text = Textos(16)  'En recepci�n de ofertas
            Case TipoEstadoProceso.ConObjetivosSinNotificar, TipoEstadoProceso.ConObjetivosSinNotificarYPreadjudicado
                lblEstado.Text = Textos(17)  'Pendiente de comunicar objetivos
            Case TipoEstadoProceso.PreadjYConObjNotificados
                lblEstado.Text = Textos(18)  'Pendiente de adjudicar
            Case TipoEstadoProceso.ParcialmenteCerrado
                lblEstado.Text = Textos(19)  'Parcialmente cerrado
            Case TipoEstadoProceso.conadjudicaciones
                lblEstado.Text = Textos(20)  'Pendiente de notificar adjudicaciones
            Case TipoEstadoProceso.ConAdjudicacionesNotificadas
                lblEstado.Text = Textos(21)  'Adjudicado y notificado
            Case TipoEstadoProceso.Cerrado
                lblEstado.Text = Textos(22)  'Anulado
        End Select


        'A�ade los materiales del proceso:
        lblMaterial.Text = ""
        If oProceso.Materiales.Rows.Count > 0 Then   'Materiales del proceso
            For Each oRow In oProceso.Materiales.Rows
                If lblMaterial.Text = "" Then
                    lblMaterial.Text = oRow.Item("GMN1") & "-" & oRow.Item("GMN2") & "-" & oRow.Item("GMN3") & "-" & oRow.Item("GMN4") & "-" & oRow.Item("DEN_GMN4")
                Else
                    lblMaterial.Text = lblMaterial.Text & "<br>" & oRow.Item("GMN1") & "-" & oRow.Item("GMN2") & "-" & oRow.Item("GMN3") & "-" & oRow.Item("GMN4") & "-" & oRow.Item("DEN_GMN4")
                End If
            Next
        End If

        'A�ade los datos generales del proceso:
        lblMoneda.Text = oProceso.Moneda & " - " & oProceso.DenMoneda
        lblFechaAper.Text = FormatDate(oProceso.FecAper, FSNUser.DateFormat)
        lblFecLimite.Text = IIf(oProceso.FecLimOfe = Nothing, "", FormatDate(oProceso.FecLimOfe, FSNUser.DateFormat))
        lblAdjudicacion.Text = IIf(oProceso.FecAdjudicacion = Nothing, "", FormatDate(oProceso.FecAdjudicacion, FSNUser.DateFormat))
        lblNecesidad.Text = IIf(oProceso.FecNecesidad = Nothing, "", FormatDate(oProceso.FecNecesidad, FSNUser.DateFormat))

        'Ahora otros datos generales del proceso,definidos a nivel de proceso:
        If oProceso.DefFechasSuministro = TipoAmbitoProceso.AmbProceso Then   'Fechas de suministro
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(34)
            oFila.Cells.Add(oCelda)

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            If Not (oProceso.FecIniSuministro = Nothing) Then
                oCelda.InnerText = FormatDate(oProceso.FecIniSuministro, FSNUser.DateFormat)
            Else
                oCelda.InnerText = ""
            End If
            oFila.Cells.Add(oCelda)
            oFila.Attributes("class") = "captionBlueSmall"
            tblDatosProceso.Rows.Add(oFila)

            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(35)
            oFila.Cells.Add(oCelda)

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            If Not (oProceso.FecFinSuministro = Nothing) Then
                oCelda.InnerText = FormatDate(oProceso.FecFinSuministro, FSNUser.DateFormat)
            Else
                oCelda.InnerText = ""
            End If
            oFila.Cells.Add(oCelda)
            oFila.Attributes("class") = "captionBlueSmall"
            tblDatosProceso.Rows.Add(oFila)
        End If

        If oProceso.DefDestino = TipoAmbitoProceso.AmbProceso Then  'Destino
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(9) & ":"
            oFila.Cells.Add(oCelda)

            If oProceso.DestinoCod <> Nothing Then
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = oProceso.DestinoCod & "-" & oProceso.DestinoDen
                oFila.Cells.Add(oCelda)
            End If
            oFila.Attributes("class") = "captionBlueSmall"
            tblDatosProceso.Rows.Insert(1, oFila)
        End If

        If oProceso.DefFormaPago = TipoAmbitoProceso.AmbProceso Then  'Forma de pago
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(28) & ":"
            oFila.Cells.Add(oCelda)

            If oProceso.FPagoCod <> Nothing Then
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = oProceso.FPagoCod & "-" & oProceso.FPagoDen
                oFila.Cells.Add(oCelda)
            End If
            oFila.Attributes("class") = "captionBlueSmall"
            tblDatosProceso.Rows.Insert(2, oFila)
        End If

        If oProceso.DefProvActual = TipoAmbitoProceso.AmbProceso Then  'Proveedor actual
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(29) & ":"
            oFila.Cells.Add(oCelda)

            If oProceso.ProvActualCod <> Nothing Then
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = oProceso.ProvActualCod & "-" & oProceso.ProvActualDen
                oFila.Cells.Add(oCelda)
            End If
            oFila.Attributes("class") = "captionBlueSmall"
            tblDatosProceso.Rows.Insert(3, oFila)
        End If

        If oProceso.Especificaciones <> Nothing Then   'Especificaciones del proceso
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(36)
            oFila.Cells.Add(oCelda)

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = oProceso.Especificaciones
            oFila.Cells.Add(oCelda)
            oFila.Attributes("class") = "captionBlueSmall"
            tblDatosProceso.Rows.Add(oFila)
        End If

        If oProceso.EspecificProce.Rows.Count > 0 Then   'Archivos de especificaciones del proceso
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(37)
            oFila.Cells.Add(oCelda)

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            For Each oRow In oProceso.EspecificProce.Rows
                sEspecific = sEspecific & oRow.Item("NOM") & "(" & oRow.Item("DATASIZE") / 1024 & " kb.);"
            Next
            sEspecific = Mid(sEspecific, 1, Len(sEspecific) - 1)
            oCelda.InnerText = sEspecific
            oCelda.InnerHtml = "<a class='aPMWeb' onclick=show_atached_files(" + oProceso.Instancia.ToString() + "," + oProceso.Anyo.ToString + ",'" + oProceso.GMN1 + "'," + oProceso.NumProce.ToString + ",null,null,null,null,null) href='#'>" + sEspecific + "</a>"
            oFila.Cells.Add(oCelda)
            oFila.Attributes("class") = "captionBlueSmall"
            tblDatosProceso.Rows.Add(oFila)
        End If


        If oProceso.ProveedoresAdj.Rows.Count > 0 Then
            'SI TIENE ADJUDICACIONES MUESTRA LOS PROVEEDORES
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(40)
            oCelda.ColSpan = 9
            oCelda.VAlign = "bottom"
            oFila.Cells.Add(oCelda)
            oFila.Attributes("class") = "captionBlue"
            oFila.Height = "30px"
            tblProceso.Rows.Add(oFila)

            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.ColSpan = 9
            oCelda.VAlign = "bottom"
            oCelda.BgColor = "dimgray"
            oFila.Cells.Add(oCelda)
            oFila.Attributes("class") = "subtitulo"
            oFila.Height = "3px"
            tblProceso.Rows.Add(oFila)

            For Each oProveRow In oProceso.ProveedoresAdj.Rows
                oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerHtml = "<a class='aPMWeb' onclick=irADetalleProveedor('" + oProveRow.Item("PROVECOD") + "') href='#'>" + oProveRow.Item("PROVECOD") & " " & oProveRow.Item("PROVEDEN") + "</a>"
                oCelda.ColSpan = 9
                oCelda.VAlign = "bottom"
                oFila.Cells.Add(oCelda)
                oFila.Attributes("class") = "captionBlueUnderline"
                oFila.Height = "40px"
                tblProceso.Rows.Add(oFila)

                'Atributos a nivel de proceso para el proveedor adjudicado
                If oProveRow.GetChildRows("REL_PROCE_PROVE_ATRIB").Length > 0 Then
                    For Each oAtribRow In oProveRow.GetChildRows("REL_PROCE_PROVE_ATRIB")
                        oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                        oCelda.InnerText = DBNullToStr(oAtribRow.Item("DEN"))
                        oCelda.ColSpan = 2
                        oFila.Cells.Add(oCelda)

                        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                        oCelda.ColSpan = 6
                        Select Case oAtribRow.Item("TIPO")
                            Case TiposDeDatos.TipoGeneral.TipoString
                                oCelda.InnerText = DBNullToStr(oAtribRow.Item("VALOR_TEXT"))
                            Case TiposDeDatos.TipoGeneral.TipoNumerico
                                oCelda.InnerText = DBNullToStr(oAtribRow.Item("VALOR_NUM"))
                            Case TiposDeDatos.TipoGeneral.TipoFecha
                                If Not IsDBNull(oAtribRow.Item("VALOR_FEC")) Then
                                    oCelda.InnerText = FormatDate(oAtribRow.Item("VALOR_FEC"), FSNUser.DateFormat)
                                Else
                                    oCelda.InnerText = ""
                                End If
                            Case TiposDeDatos.TipoGeneral.TipoBoolean
                                If Not IsDBNull(oAtribRow.Item("VALOR_BOOL")) Then
                                    Select Case oAtribRow.Item("VALOR_BOOL")
                                        Case 0
                                            oCelda.InnerText = Textos(46)
                                        Case 1
                                            oCelda.InnerText = Textos(47)
                                    End Select
                                Else
                                    oCelda.InnerText = ""
                                End If
                        End Select
                        oFila.Cells.Add(oCelda)
                        oFila.Attributes("class") = "captionBlueSmall"
                        tblProceso.Rows.Add(oFila)
                    Next
                End If

                'Observaciones de la oferta
                If oProveRow.GetChildRows("REL_PROCE_PROVE_ESP").Length > 0 Then
                    sObservaciones = ""
                    For Each oEspecRow In oProveRow.GetChildRows("REL_PROCE_PROVE_ESP")
                        sObservaciones = DBNullToSomething(oEspecRow.Item("OBSADJUN"))
                    Next
                    If sObservaciones <> "" Then
                        oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                        oCelda.InnerText = Textos(44)
                        oCelda.ColSpan = 2
                        oFila.Cells.Add(oCelda)

                        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                        oCelda.InnerText = DBNullToStr(sObservaciones)
                        oCelda.ColSpan = 6
                        oFila.Cells.Add(oCelda)
                        oFila.Attributes("class") = "captionBlueSmall"
                        tblProceso.Rows.Add(oFila)
                    End If
                End If
                'Archivos adjuntos de la oferta
                If oProveRow.GetChildRows("REL_PROCE_PROVE_ADJUN").Length > 0 Then
                    oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                    oCelda.InnerText = Textos(45)
                    oCelda.ColSpan = 2
                    oFila.Cells.Add(oCelda)

                    sEspecific = ""
                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                    For Each oEspecRow In oProveRow.GetChildRows("REL_PROCE_PROVE_ADJUN")
                        sEspecific = sEspecific & oEspecRow.Item("NOM") & "(" & oEspecRow.Item("DATASIZE") / 1024 & " kb.);"
                    Next
                    sEspecific = Mid(sEspecific, 1, Len(sEspecific) - 1)
                    oCelda.InnerHtml = "<a class='aPMWeb' onclick=show_atached_files(" + oProceso.Instancia.ToString() + "," + oProceso.Anyo.ToString + ",'" + oProceso.GMN1 + "'," + oProceso.NumProce.ToString + ",null,null,'" + oProveRow.Item("PROVECOD") + "'," + oProveRow.Item("OFE").ToString() + ",null) href='#'>" + sEspecific + "</a>"
                    oCelda.ColSpan = 6
                    oFila.Cells.Add(oCelda)
                    oFila.Attributes("class") = "captionBlueSmall"
                    tblProceso.Rows.Add(oFila)
                End If


                'Muestra los grupos e �tems para ese proevedor adjudicado
                If oProveRow.GetChildRows("REL_PROVE_GRUPO").Length > 0 Then
                    For Each oRow In oProveRow.GetChildRows("REL_PROVE_GRUPO")
                        CargarGrupos(oRow)
                    Next
                End If
            Next

        Else  'NO TIENE ADJUDICACIONES
            If oProceso.Grupos.Rows.Count > 0 Then   'Muestra los grupos e �tems del proceso.
                For Each oRow In oProceso.Grupos.Rows
                    CargarGrupos(oRow)
                Next
            End If
        End If

        If Page.IsPostBack = True Then
            Me.cmdImprimir.Visible = False
            Me.lblCerrar.Visible = False
        End If

        If Request("pdf") = 1 Then
            generarPDF()
        End If
    End Sub

    ''' <summary>
    ''' Muestra los grupos e �tems para ese proveedor
    ''' </summary>
    ''' <param name="oRow">Grupo a mostrar</param>
    ''' <remarks>Llamada desde: Page_Load ; Tiempo m�ximo: 0,2</remarks>
    Private Sub CargarGrupos(ByVal oRow As DataRow)
        Dim oFila As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oCelda As System.Web.UI.HtmlControls.HtmlTableCell
        Dim sEspecific As String
        Dim oItemRow As DataRow
        Dim oEspecRow As DataRow
        Dim oAtribRow As DataRow
        Dim sObservaciones As String

        'Le pasa por par�metro el grupo que se va a imprimir
        oFila = New System.Web.UI.HtmlControls.HtmlTableRow
        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
        oCelda.InnerText = Textos(12) & " " & oRow.Item("GRUPOCOD") & " - " & oRow.Item("DEN_GRUPO")
        oCelda.ColSpan = 9
        oCelda.VAlign = "bottom"
        oFila.Cells.Add(oCelda)
        oFila.Attributes("class") = "CaptionBlue"
        oFila.Height = "30px"
        tblProceso.Rows.Add(oFila)

        'Ahora otros datos generales del proceso,definidos a nivel de proceso:
        If oProceso.DefFechasSuministro = TipoAmbitoProceso.AmbGrupo Then    'Fechas de suministro
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(34)
            oCelda.ColSpan = 2
            oFila.Cells.Add(oCelda)

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            If Not (IsDBNull(oRow.Item("FECINI"))) Then
                oCelda.InnerText = FormatDate(oRow.Item("FECINI"), FSNUser.DateFormat)
            Else
                oCelda.InnerText = ""
            End If
            oFila.Cells.Add(oCelda)
            oCelda.ColSpan = 6
            oFila.Attributes("class") = "captionBlueSmall"
            tblProceso.Rows.Add(oFila)

            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(35)
            oCelda.ColSpan = 2
            oFila.Cells.Add(oCelda)

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            If Not (IsDBNull(oRow.Item("FECFIN"))) Then
                oCelda.InnerText = FormatDate(oRow.Item("FECFIN"), FSNUser.DateFormat)
            Else
                oCelda.InnerText = ""
            End If
            oFila.Cells.Add(oCelda)
            oCelda.ColSpan = 6
            oFila.Attributes("class") = "captionBlueSmall"
            tblProceso.Rows.Add(oFila)
        End If

        If oProceso.DefDestino = TipoAmbitoProceso.AmbGrupo Then   'Destino
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(9) & ":"
            oCelda.ColSpan = 2
            oFila.Cells.Add(oCelda)

            If Not IsDBNull(oRow.Item("DEST")) Then
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = oRow.Item("DEST") & "-" & oRow.Item("DEST_DEN")
                oCelda.ColSpan = 6
                oFila.Cells.Add(oCelda)
            End If
            oFila.Attributes("class") = "captionBlueSmall"
            tblProceso.Rows.Add(oFila)
        End If

        If oProceso.DefFormaPago = TipoAmbitoProceso.AmbGrupo Then  'Forma de pago
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(28) & ":"
            oCelda.ColSpan = 2
            oFila.Cells.Add(oCelda)

            If Not IsDBNull(oRow.Item("PAG")) Then
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = oRow.Item("PAG") & "-" & oRow.Item("PAG_DEN")
                oCelda.ColSpan = 6
                oFila.Cells.Add(oCelda)
            End If
            oFila.Attributes("class") = "captionBlueSmall"
            tblProceso.Rows.Add(oFila)
        End If

        If oProceso.DefProvActual = TipoAmbitoProceso.AmbGrupo Then  'Proveedor actual
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(29) & ":"
            oCelda.ColSpan = 2
            oFila.Cells.Add(oCelda)

            If Not IsDBNull(oRow.Item("PROVEACT")) Then
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = oRow.Item("PROVEACT") & "-" & oRow.Item("PROVE_DEN")
                oCelda.ColSpan = 6
                oFila.Cells.Add(oCelda)
            End If
            oFila.Attributes("class") = "captionBlueSmall"
            tblProceso.Rows.Add(oFila)
        End If

        If Not IsDBNull(oRow.Item("ESP")) Then
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(38)
            oFila.Cells.Add(oCelda)
            oCelda.ColSpan = 2

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = oRow.Item("ESP")
            oCelda.ColSpan = 6
            oFila.Cells.Add(oCelda)
            oFila.Attributes("class") = "captionBlueSmall"
            tblProceso.Rows.Add(oFila)
        End If

        If oRow.GetChildRows("REL_GRUPO_ESP").Length > 0 Then
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(39)
            oCelda.ColSpan = 2
            oFila.Cells.Add(oCelda)

            sEspecific = ""
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            For Each oEspecRow In oRow.GetChildRows("REL_GRUPO_ESP")
                sEspecific = sEspecific & oEspecRow.Item("NOM") & "(" & oEspecRow.Item("DATASIZE") / 1024 & " kb.);"
            Next
            sEspecific = Mid(sEspecific, 1, Len(sEspecific) - 1)
            oCelda.ColSpan = 6
            oCelda.InnerHtml = "<a class='aPMWeb' onclick=show_atached_files(" + oProceso.Instancia.ToString() + "," + oProceso.Anyo.ToString + ",'" + oProceso.GMN1 + "'," + oProceso.NumProce.ToString + ",'" + oEspecRow.Item("GRUPO").ToString + "',null,null,null,'" & oRow.Item("GRUPOCOD") & "') href='#'>" + sEspecific + "</a>"
            oFila.Cells.Add(oCelda)
            oFila.Attributes("class") = "captionBlueSmall"
            tblProceso.Rows.Add(oFila)
        End If

        'Atributos a nivel de grupo para el proveedor adjudicado
        If oRow.GetChildRows("REL_PROVE_GRUPO_ATRIB").Length > 0 Then
            For Each oAtribRow In oRow.GetChildRows("REL_PROVE_GRUPO_ATRIB")
                oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = DBNullToStr(oAtribRow.Item("DEN"))
                oCelda.ColSpan = 2
                oFila.Cells.Add(oCelda)

                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.ColSpan = 6
                Select Case oAtribRow.Item("TIPO")
                    Case TiposDeDatos.TipoGeneral.TipoString
                        oCelda.InnerText = DBNullToStr(oAtribRow.Item("VALOR_TEXT"))
                    Case TiposDeDatos.TipoGeneral.TipoNumerico
                        oCelda.InnerText = DBNullToStr(oAtribRow.Item("VALOR_NUM"))
                    Case TiposDeDatos.TipoGeneral.TipoFecha
                        If Not IsDBNull(oAtribRow.Item("VALOR_FEC")) Then
                            oCelda.InnerText = FormatDate(oAtribRow.Item("VALOR_FEC"), FSNUser.DateFormat)
                        Else
                            oCelda.InnerText = ""
                        End If
                    Case TiposDeDatos.TipoGeneral.TipoBoolean
                        If Not IsDBNull(oAtribRow.Item("VALOR_BOOL")) Then
                            Select Case oAtribRow.Item("VALOR_BOOL")
                                Case 0
                                    oCelda.InnerText = Textos(46)
                                Case 1
                                    oCelda.InnerText = Textos(47)
                            End Select
                        Else
                            oCelda.InnerText = ""
                        End If
                End Select
                oFila.Cells.Add(oCelda)
                oFila.Attributes("class") = "captionBlueSmall"
                tblProceso.Rows.Add(oFila)
            Next
        End If

        'Observaciones de las ofertas a nivel de grupo
        If oRow.GetChildRows("REL_PROVE_GRUPO_ESP").Length > 0 Then
            sObservaciones = ""
            For Each oEspecRow In oRow.GetChildRows("REL_PROVE_GRUPO_ESP")
                sObservaciones = DBNullToSomething(oEspecRow.Item("OBSADJUN"))
            Next
            If sObservaciones <> "" Then
                oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = Textos(44)
                oCelda.ColSpan = 2
                oFila.Cells.Add(oCelda)

                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = sObservaciones
                oCelda.ColSpan = 6
                oFila.Cells.Add(oCelda)
                oFila.Attributes("class") = "captionBlueSmall"
                tblProceso.Rows.Add(oFila)
            End If
        End If

        'Archivos adjuntos de la oferta
        If oRow.GetChildRows("REL_PROVE_GRUPO_ADJUN").Length > 0 Then
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(45)
            oCelda.ColSpan = 2
            oFila.Cells.Add(oCelda)

            sEspecific = ""
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            For Each oEspecRow In oRow.GetChildRows("REL_PROVE_GRUPO_ADJUN")
                sEspecific = sEspecific & oEspecRow.Item("NOM") & "(" & oEspecRow.Item("DATASIZE") / 1024 & " kb.);"
            Next
            sEspecific = Mid(sEspecific, 1, Len(sEspecific) - 1)
            oCelda.InnerHtml = "<a class='aPMWeb' onclick=show_atached_files(" + oProceso.Instancia.ToString() + "," + oProceso.Anyo.ToString + ",'" + oProceso.GMN1 + "'," + oProceso.NumProce.ToString + ",'" + oEspecRow.Item("GRUPO").ToString + "',null,'" + oRow.Item("PROVE") + "'," + oRow.GetParentRow("REL_PROVE_GRUPO").Item("OFE").ToString() + ",'" & oRow.Item("GRUPOCOD") & "') href='#'>" + sEspecific + "</a>"
            oCelda.ColSpan = 6
            oFila.Cells.Add(oCelda)
            oFila.Attributes("class") = "captionBlueSmall"
            tblProceso.Rows.Add(oFila)
        End If

        'Comprueba si hay �tems para ese grupo
        If oRow.GetChildRows("REL_GRUPO_ITEM").Length > 0 Then
            'genera la cabecera de los �tems:
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(41)
            oCelda.Attributes("class") = "CaptionBlue"
            oFila.Cells.Add(oCelda)

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = ""
            oCelda.Attributes("class") = "CaptionBlueSmallBold"
            oFila.Cells.Add(oCelda)

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(7)
            oCelda.Attributes("class") = "CaptionBlueSmallBold"
            oFila.Cells.Add(oCelda)

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(8)
            oCelda.Attributes("class") = "CaptionBlueSmallBold"
            oFila.Cells.Add(oCelda)

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(9)
            oCelda.Attributes("class") = "CaptionBlueSmallBold"
            oFila.Cells.Add(oCelda)

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(10)
            oCelda.Attributes("class") = "CaptionBlueSmallBold"
            oFila.Cells.Add(oCelda)

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = Textos(11)
            oCelda.Attributes("class") = "CaptionBlueSmallBold"
            oFila.Height = "25px"
            oFila.Cells.Add(oCelda)
            oFila.VAlign = "bottom"

            If oProceso.DefFormaPago = TipoAmbitoProceso.AmbItem Then
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = Textos(28)
                oCelda.Attributes("class") = "CaptionBlueSmallBold"
                oFila.Cells.Add(oCelda)
            End If

            If oProceso.DefProvActual = TipoAmbitoProceso.AmbItem Then
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = Textos(29)
                oCelda.Attributes("class") = "CaptionBlueSmallBold"
                oFila.Cells.Add(oCelda)
            End If

            'Si tiene adjudicaciones:
            If oProceso.Estado >= TipoEstadoProceso.ConObjetivosSinNotificarYPreadjudicado Then
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = Textos(23)  '%Adjudicado
                oCelda.Attributes("class") = "CaptionBlueSmallBold"
                oFila.Cells.Add(oCelda)

                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = Textos(24) 'precio
                oCelda.Attributes("class") = "CaptionBlueSmallBold"
                oFila.Cells.Add(oCelda)
            End If
            tblProceso.Rows.Add(oFila)

            'Ahora a�ade el detalle de los items:
            For Each oItemRow In oRow.GetChildRows("REL_GRUPO_ITEM")
                'Ahora a�ade el detalle de los items:
                oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = DBNullToStr(oItemRow.Item("ART"))
                oFila.Cells.Add(oCelda)

                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = DBNullToStr(oItemRow.Item("DESCR"))
                oFila.Cells.Add(oCelda)

                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = DBNullToStr(oItemRow.Item("CANT"))
                oCelda.Align = HorizontalAlign.Right
                oFila.Cells.Add(oCelda)

                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                If IsDBNull(oItemRow.Item("UNI")) Then
                    oCelda.InnerText = ""
                Else
                    oCelda.InnerHtml = "<a class='aPMWeb' onclick=irADetalleUnidad('" + oItemRow.Item("UNI") + "') href='#'>" + oItemRow.Item("UNI") + "</a>"
                End If
                oFila.Cells.Add(oCelda)

                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                If IsDBNull(oItemRow.Item("DEST")) Then
                    oCelda.InnerText = ""
                Else
                    oCelda.InnerHtml = "<a class='aPMWeb' onclick=irADetalleDest('" + oItemRow.Item("DEST") + "') href='#'>" + oItemRow.Item("DEST") + "</a>"
                End If
                oFila.Cells.Add(oCelda)

                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                If Not (IsDBNull(oItemRow.Item("FECINI"))) Then
                    oCelda.InnerText = FormatDate(oItemRow.Item("FECINI"), FSNUser.DateFormat)
                Else
                    oCelda.InnerText = ""
                End If
                oFila.Cells.Add(oCelda)

                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                If Not (IsDBNull(oItemRow.Item("FECFIN"))) Then
                    oCelda.InnerText = FormatDate(oItemRow.Item("FECFIN"), FSNUser.DateFormat)
                Else
                    oCelda.InnerText = ""
                End If
                oFila.Cells.Add(oCelda)

                If oProceso.DefFormaPago = TipoAmbitoProceso.AmbItem Then
                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                    If IsDBNull(oItemRow.Item("PAG")) Then
                        oCelda.InnerText = ""
                    Else
                        oCelda.InnerHtml = "<a class='aPMWeb' onclick=irADetalleFpago('" + oItemRow.Item("PAG") + "') href='#'>" + oItemRow.Item("PAG") + "</a>"
                    End If
                    oFila.Cells.Add(oCelda)
                End If

                If oProceso.DefProvActual = TipoAmbitoProceso.AmbItem Then
                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                    If IsDBNull(oItemRow.Item("PROVEACT")) Then
                        oCelda.InnerText = ""
                    Else
                        oCelda.InnerHtml = "<a class='aPMWeb' onclick=irADetalleProveedor('" + oItemRow.Item("PROVEACT") + "') href='#'>" + oItemRow.Item("PROVEACT") + "</a>"
                    End If
                    oFila.Cells.Add(oCelda)
                End If

                'si tiene adjudicaciones
                If oProceso.Estado >= TipoEstadoProceso.ConObjetivosSinNotificarYPreadjudicado Then
                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                    oCelda.InnerText = DBNullToStr(oItemRow.Item("PORCEN")) & "%"
                    oFila.Cells.Add(oCelda)

                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                    If Not IsDBNull(oItemRow.Item("PRECIO")) Then
                        oCelda.InnerText = FSNLibrary.FormatNumber(oItemRow.Item("PRECIO"), FSNUser.NumberFormat)
                    End If
                    oFila.Cells.Add(oCelda)
                End If

                oFila.Attributes("class") = "VisorRow"
                tblProceso.Rows.Add(oFila)

                If Not IsDBNull(oItemRow.Item("ESP")) Then  'Especificaciones
                    oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                    oCelda.InnerText = Textos(42)
                    oCelda.ColSpan = 2
                    oFila.Cells.Add(oCelda)

                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                    oCelda.InnerText = oItemRow.Item("ESP")
                    oCelda.ColSpan = 6
                    oFila.Cells.Add(oCelda)
                    oFila.Attributes("class") = "captionBlueSmall"
                    tblProceso.Rows.Add(oFila)
                End If

                If oItemRow.GetChildRows("REL_ITEM_ESP").Length > 0 Then  'Archivos de especificaciones del �tem
                    oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                    oCelda.InnerText = Textos(43)
                    oCelda.ColSpan = 2
                    oFila.Cells.Add(oCelda)

                    sEspecific = ""
                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                    For Each oEspecRow In oItemRow.GetChildRows("REL_ITEM_ESP")
                        sEspecific = sEspecific & oEspecRow.Item("NOM") & "(" & oEspecRow.Item("DATASIZE") / 1024 & " kb.);"
                    Next
                    sEspecific = Mid(sEspecific, 1, Len(sEspecific) - 1)
                    oCelda.InnerHtml = "<a class='aPMWeb' onclick=show_atached_files(" + oProceso.Instancia.ToString() + "," + oProceso.Anyo.ToString + ",'" + oProceso.GMN1 + "'," + oProceso.NumProce.ToString + ",'" + oItemRow.Item("GRUPO").ToString() + "'," + oItemRow.Item("ID").ToString() + ",null,null,'" & oRow.Item("GRUPOCOD") & "') href='#'>" + sEspecific + "</a>"
                    oCelda.ColSpan = 6
                    oFila.Cells.Add(oCelda)
                    oFila.Attributes("class") = "captionBlueSmall"
                    tblProceso.Rows.Add(oFila)
                End If

                'Atributos a nivel de item para el proveedor adjudicado
                If oItemRow.GetChildRows("REL_PROVE_ITEM_ATRIB").Length > 0 Then
                    For Each oAtribRow In oItemRow.GetChildRows("REL_PROVE_ITEM_ATRIB")
                        oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                        oCelda.InnerText = DBNullToStr(oAtribRow.Item("DEN"))
                        oCelda.ColSpan = 2
                        oFila.Cells.Add(oCelda)

                        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                        oCelda.ColSpan = 6
                        Select Case oAtribRow.Item("TIPO")
                            Case TiposDeDatos.TipoGeneral.TipoString
                                oCelda.InnerText = DBNullToStr(oAtribRow.Item("VALOR_TEXT"))
                            Case TiposDeDatos.TipoGeneral.TipoNumerico
                                oCelda.InnerText = DBNullToStr(oAtribRow.Item("VALOR_NUM"))
                            Case TiposDeDatos.TipoGeneral.TipoFecha
                                If Not IsDBNull(oAtribRow.Item("VALOR_FEC")) Then
                                    oCelda.InnerText = FormatDate(oAtribRow.Item("VALOR_FEC"), FSNUser.DateFormat)
                                Else
                                    oCelda.InnerText = ""
                                End If
                            Case TiposDeDatos.TipoGeneral.TipoBoolean
                                If Not IsDBNull(oAtribRow.Item("VALOR_BOOL")) Then
                                    Select Case oAtribRow.Item("VALOR_BOOL")
                                        Case 0
                                            oCelda.InnerText = Textos(46)
                                        Case 1
                                            oCelda.InnerText = Textos(47)
                                    End Select
                                Else
                                    oCelda.InnerText = ""
                                End If
                        End Select
                        oFila.Cells.Add(oCelda)
                        oFila.Attributes("class") = "captionBlueSmall"
                        tblProceso.Rows.Add(oFila)
                    Next
                End If

                'Observaciones de las ofertas a nivel de �tem
                If oItemRow.GetChildRows("REL_PROVE_ITEM_ESP").Length > 0 Then
                    sObservaciones = ""
                    For Each oEspecRow In oItemRow.GetChildRows("REL_PROVE_ITEM_ESP")
                        sObservaciones = DBNullToSomething(oEspecRow.Item("OBSADJUN"))
                    Next
                    If sObservaciones <> "" Then
                        oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                        oCelda.InnerText = Textos(44)
                        oCelda.ColSpan = 2
                        oFila.Cells.Add(oCelda)

                        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                        oCelda.InnerText = sObservaciones
                        oCelda.ColSpan = 6
                        oFila.Cells.Add(oCelda)
                        oFila.Attributes("class") = "captionBlueSmall"
                        tblProceso.Rows.Add(oFila)
                    End If
                End If
                'Archivos adjuntos de la oferta para el �tem y el proveedor
                If oItemRow.GetChildRows("REL_PROVE_ITEM_ADJUN").Length > 0 Then
                    oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                    oCelda.InnerText = Textos(45)
                    oCelda.ColSpan = 2
                    oFila.Cells.Add(oCelda)

                    sEspecific = ""
                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                    For Each oEspecRow In oItemRow.GetChildRows("REL_PROVE_ITEM_ADJUN")
                        sEspecific = sEspecific & oEspecRow.Item("NOM") & "(" & oEspecRow.Item("DATASIZE") / 1024 & " kb.);"
                    Next
                    sEspecific = Mid(sEspecific, 1, Len(sEspecific) - 1)
                    oCelda.InnerHtml = "<a class='aPMWeb' onclick=show_atached_files(" + oProceso.Instancia.ToString() + "," + oProceso.Anyo.ToString + ",'" + oProceso.GMN1 + "'," + oProceso.NumProce.ToString + ",'" + oItemRow.Item("GRUPO").ToString() + "'," + oItemRow.Item("ID").ToString() + ",'" + oRow.Item("PROVE") + "'," + oRow.GetParentRow("REL_PROVE_GRUPO").Item("OFE").ToString() + ",'" & oRow.Item("GRUPOCOD") & "') href='#'>" + sEspecific + "</a>"
                    oCelda.ColSpan = 6
                    oFila.Cells.Add(oCelda)
                    oFila.Attributes("class") = "captionBlueSmall"
                    tblProceso.Rows.Add(oFila)
                End If
            Next
        End If
    End Sub


    Private Sub generarPDF()
        Dim oStringWriter As New System.IO.StringWriter
        Dim oHtmlTextWriter As New System.Web.UI.HtmlTextWriter(oStringWriter)
        Me.cmdImprimir.Visible = False
        Me.lblCerrar.Visible = False

        oHtmlTextWriter.WriteLine("<HTML>")
        oHtmlTextWriter.WriteLine("<HEAD>")
        oHtmlTextWriter.WriteLine("<meta http-equiv=""Content-Type"" content=""text/html; charset=windows-1252"">")
        oHtmlTextWriter.WriteLine("<STYLE>@import url( " & ConfigurationManager.AppSettings("ruta") + "App_Themes/" & Me.Theme & ".css );</STYLE>")

        oHtmlTextWriter.WriteLine("</HEAD>")
        oHtmlTextWriter.WriteLine("<BODY>")

        Me.RenderControl(oHtmlTextWriter)
        oHtmlTextWriter.WriteLine("</BODY>")
        oHtmlTextWriter.WriteLine("</HTML>")

        '''''' Guardamos el HTML en un archivo
        Dim sPathBase As String
        Dim sPathHTML As String
        Dim sPathPDF As String
        Dim oFolder As System.IO.Directory

        sPathBase = ConfigurationManager.AppSettings("temp") + "\" + GenerateRandomPath()
        If Not oFolder.Exists(sPathBase) Then
            oFolder.CreateDirectory(sPathBase)
        End If
        sPathHTML = sPathBase + "\" + CStr(oProceso.Anyo) + CStr(oProceso.GMN1) + CStr(oProceso.NumProce) + ".html"
        sPathPDF = sPathBase + "\" + CStr(oProceso.Anyo) + CStr(oProceso.GMN1) + CStr(oProceso.NumProce) + ".pdf"

        Dim oStreamWriter As New System.IO.StreamWriter(sPathHTML, False, System.Text.Encoding.UTF8)
        oStreamWriter.Write(oStringWriter.ToString)
        oStreamWriter.Close()

        ''''''' Pasamos el HTML a PDF
        ConvertHTMLToPDF(sPathHTML, sPathPDF)

        ''''''' Ponemos el PDF a descargar
        Response.Redirect("detalleprocesohid.aspx?path=" & Server.UrlEncode(sPathPDF))
    End Sub
End Class

