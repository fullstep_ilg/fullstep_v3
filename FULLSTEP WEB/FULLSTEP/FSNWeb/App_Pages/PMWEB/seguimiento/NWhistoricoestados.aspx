<%@ Register TagPrefix="igtbl" Namespace="Infragistics.WebUI.UltraWebGrid" Assembly="Infragistics.WebUI.UltraWebGrid.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="NWhistoricoestados.aspx.vb" Inherits="Fullstep.FSNWeb.NWhistoricoestados"%>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
	</head>
    <!--JIM: colocamos esta porci�n de c�digo antes de la etiqueta de men�. menu.js realiza override de window.open, y si la colocamos
        despu�s no se ejecuta este c�digo -->
    <%If Not String.IsNullOrEmpty(Request("ConfiguracionGS")) Then%>
        <script type="text/javascript">
            window.open_ = window.open;
            window.open = function (url, name, props) {
	            if (url.toLowerCase().search("sessionid") > 0)
		            return window.open_(url, name, props);
	            else
		            return window.open_(url + '&SessionId=<%=Session("sSession")%>', name, props);
            }  
        </script>
    <%End If%>

	<script type="text/javascript">
		function VerDetallePersona(codPer){
			var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detallepersona.aspx?CodPersona=" + codPer.toString(),"_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
            newWindow.focus();
		}
		function VerDetalleProveedor(codProv) {
			var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detalleProveedor.aspx?codProv=" + codProv.toString(),"_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
            newWindow.focus();
		}
		function VerDetallePedido(IdPedido) {
			var newWindow = window.open("detallepedidoPMWEB.aspx?Pedido=" + IdPedido + "&Instancia=" + document.forms["Form1"].elements["txtInstancia"].value, "_blank", "width=950,height=600,status=yes,resizable=yes,top=200,left=200,scrollbars=yes");
            newWindow.focus();
		}
		function VerDetalleProceso(Anyo,GMN1,Proce) {
			var newWindow = window.open("detalleproceso.aspx?Anyo=" + Anyo + "&GMN1=" + GMN1 + "&Proce=" + Proce + "&Instancia=" + document.forms["Form1"].elements["txtInstancia"].value, "_blank", "width=950,height=600,status=yes,resizable=yes,top=200,left=200,scrollbars=yes");
            newWindow.focus();
		}
		function mostrarDiagramaFlujo()
		{
			var IdInstancia=document.forms["Form1"].elements["txtInstancia"].value   
			var CodContrato=document.forms["Form1"].elements["hidCodContrato"].value
			var newWindow = window.open("../workflow/flowDiagramLaunch.aspx?Instancia=" + IdInstancia + "&Codigo=" + CodContrato,"_blank", "width=700,height=400,status=yes,resizable=yes,top=200,left=200");
            newWindow.focus();
		}
		function VerComentario(id) {
			var newWindow = window.open("comentestado.aspx?ID=" + id,"_blank", "width=700,height=400,status=yes,resizable=no,top=200,left=200");
            newWindow.focus();
		}
		function HistoryHaciaAtras() { 
			window.history.go(-1)					
		}
	</script>	
	<body id="mi_body" runat="server">
		<form id="Form1" method="post" runat="server">
            <asp:ScriptManager ID="sm" runat="server">
            </asp:ScriptManager>
			<TABLE id="Table1" style="Z-INDEX: 108; LEFT: 8px; POSITION: absolute; TOP: 112px" cellSpacing="0"
				cellPadding="1" width="100%" border="0" runat="server">
				<TR><td width="5%"><img src="images/tareasPM.jpg" height="45" /></td>
					<td><asp:label id="lblTitulo" runat="server" CssClass="Titulo" Text="DDetalle de tareas"></asp:label></td>
					<td align="right">
						<a href="javascript:HistoryHaciaAtras()">
							<asp:Label id="lblVolver" runat="server" CssClass="volver">lblVolver</asp:Label></a>
					</td>
				</TR>
			</TABLE>
			<TABLE id="tblCabecera" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 160px; HEIGHT: 75px"
				cellSpacing="1" cellPadding="1" width="100%" border="0" class="fondoCabecera">
				<TR>
					<TD style="WIDTH: 15%">
						<asp:label id="lblId" runat="server" CssClass="captionBlue" Width="100%"></asp:label></TD>
					<TD style="WIDTH: 352px">
						<asp:label id="lblIdBD" runat="server" CssClass="captionDarkBlue" Width="100%">Label</asp:label></TD>
					<TD style="WIDTH: 15%">
						<asp:label id="lblFecha" runat="server" CssClass="captionBlue" Width="100%">lblFecha</asp:label></TD>
					<TD style="WIDTH: 352px">
						<asp:label id="lblFechaBD" runat="server" CssClass="captionDarkBlue" Width="100%">Label</asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 15%">
						<asp:label id="lblNombre" runat="server" CssClass="captionBlue" Width="100%"></asp:label></TD>
					<TD style="WIDTH: 352px">
						<asp:label id="lblNombreBD" runat="server" CssClass="captionDarkBlue" Width="100%">Label</asp:label></TD>
					<TD style="WIDTH: 15%"></TD>
					<TD style="WIDTH: 352px"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 10%">
						<asp:label id="lblPeticionario" runat="server" CssClass="captionBlue" Width="100%">lblPeticionario</asp:label></TD>
					<TD style="WIDTH: 352px">
						<asp:label id="lblPeticionarioBD" runat="server" CssClass="captionDarkBlue" Width="100%">Label</asp:label></TD>
					<TD style="WIDTH: 10%"></TD>
					<TD style="WIDTH: 352px"></TD>
				</TR>
			</TABLE>
			<uc1:menu id="Menu1" runat="server"></uc1:menu>
			<TABLE id="tblPosic" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 248px" height="85%"
				width="100%" border="0" runat="server">
				<TR height="45%">
					<TD >
						<TABLE id="tblWorkflow" height="100%" cellSpacing="1" cellPadding="1" width="100%" border="0"
							runat="server">
							<TR>
								<TD align="right">
										<a class="aPMWeb" href="javascript:mostrarDiagramaFlujo()"><asp:label id="lblFlujo" runat="server" Width="50%" CssClass="flujo"></asp:label></a></TD>
							</TR>
							<TR height="60">
								<TD><asp:label id="lblWorkFlow" runat="server" Width="100%" CssClass="VisorHead">Label</asp:label></TD>
							</TR>
							<TR>
								<TD valign="top"><igtbl:ultrawebgrid id="uwgHistorico" runat="server" Width="100%" CaptionAlign="Left">
										<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
											Name="uwgHistorico" ActivationObject-AllowActivation="false" ReadOnly="LevelTwo">
											<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</HeaderStyleDefault>
											<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" CssClass="VisorFrame"></FrameStyle>
											<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</FooterStyleDefault>
											<FixedHeaderStyleDefault CssClass="VisorHead"></FixedHeaderStyleDefault>
											<RowStyleDefault BorderWidth="1px" BorderStyle="Solid">
												<Padding Left="3px"></Padding>
												<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
											</RowStyleDefault>
										</DisplayLayout>
										<Bands>
											<igtbl:UltraGridBand></igtbl:UltraGridBand>
										</Bands>
									</igtbl:ultrawebgrid></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR height="45%" valign="top">
					<TD>
						<TABLE id="tblActual" height="40%" width="100%" border="0" runat="server">
							<TR>
								<TD height="3%"><asp:label id="lblActual" runat="server" Width="100%" CssClass="VisorHead">DSituaci�n actual</asp:label></TD>
							</TR>
							<TR>
								<TD><igtbl:ultrawebgrid id="uwgProcesos" runat="server" Width="100%">
										<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
											Name="uwgProcesos" ReadOnly="LevelTwo">
											<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</HeaderStyleDefault>
											<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" CssClass="VisorFrame"></FrameStyle>
											<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</FooterStyleDefault>
											<FixedHeaderStyleDefault CssClass="VisorHead"></FixedHeaderStyleDefault>
											<RowStyleDefault BorderWidth="1px" BorderStyle="Solid" CssClass="VisorRow">
												<Padding Left="3px"></Padding>
												<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
											</RowStyleDefault>
										</DisplayLayout>
										<Bands>
											<igtbl:UltraGridBand></igtbl:UltraGridBand>
										</Bands>
									</igtbl:ultrawebgrid></TD>
							</TR>
							<TR>
								<TD><igtbl:ultrawebgrid id="uwgLineasCat" runat="server" Width="100%">
										<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
											Name="uwgLineasCat" ReadOnly="LevelTwo">
											<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</HeaderStyleDefault>
											<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" CssClass="VisorFrame"></FrameStyle>
											<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</FooterStyleDefault>
											<RowStyleDefault BorderWidth="1px" BorderStyle="Solid" CssClass="VisorRow">
												<Padding Left="3px"></Padding>
												<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
											</RowStyleDefault>
										</DisplayLayout>
										<Bands>
											<igtbl:UltraGridBand></igtbl:UltraGridBand>
										</Bands>
									</igtbl:ultrawebgrid></TD>
							</TR>
							<TR>
								<TD><igtbl:ultrawebgrid id="uwgPedNeg" runat="server" Width="100%">
										<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
											Name="uwgPedNeg" ReadOnly="LevelTwo">
											<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</HeaderStyleDefault>
											<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" CssClass="VisorFrame"></FrameStyle>
											<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</FooterStyleDefault>
											<RowStyleDefault BorderWidth="1px" BorderStyle="Solid" CssClass="VisorRow">
												<Padding Left="3px"></Padding>
												<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
											</RowStyleDefault>
										</DisplayLayout>
										<Bands>
											<igtbl:UltraGridBand></igtbl:UltraGridBand>
										</Bands>
									</igtbl:ultrawebgrid></TD>
							</TR>
							<TR>
								<TD><igtbl:ultrawebgrid id="uwgPedCatNeg" runat="server" Width="100%">
										<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
											Name="uwgPedCatNeg" ReadOnly="LevelTwo">
											<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</HeaderStyleDefault>
											<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" CssClass="VisorFrame"></FrameStyle>
											<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</FooterStyleDefault>
											<RowStyleDefault BorderWidth="1px" BorderStyle="Solid" CssClass="VisorRow">
												<Padding Left="3px"></Padding>
												<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
											</RowStyleDefault>
										</DisplayLayout>
										<Bands>
											<igtbl:UltraGridBand></igtbl:UltraGridBand>
										</Bands>
									</igtbl:ultrawebgrid></TD>
							</TR>
							<TR>
								<TD><igtbl:ultrawebgrid id="uwgPedLibres" runat="server" Width="100%">
										<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
											Name="uwgPedLibres" ReadOnly="LevelTwo">
											<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</HeaderStyleDefault>
											<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" CssClass="VisorFrame"></FrameStyle>
											<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</FooterStyleDefault>
											<RowStyleDefault BorderWidth="1px" BorderStyle="Solid" CssClass="VisorRow">
												<Padding Left="3px"></Padding>
												<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
											</RowStyleDefault>
										</DisplayLayout>
										<Bands>
											<igtbl:UltraGridBand></igtbl:UltraGridBand>
										</Bands>
									</igtbl:ultrawebgrid></TD>
							</TR>
							<TR>
								<TD><igtbl:ultrawebgrid id="uwgPedDir" runat="server" Width="100%">
										<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
											Name="uwgPedDir" ReadOnly="LevelTwo">
											<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</HeaderStyleDefault>
											<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" CssClass="VisorFrame"></FrameStyle>
											<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</FooterStyleDefault>
											<RowStyleDefault BorderWidth="1px" BorderStyle="Solid" CssClass="VisorRow">
												<Padding Left="3px"></Padding>
												<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
											</RowStyleDefault>
										</DisplayLayout>
										<Bands>
											<igtbl:UltraGridBand></igtbl:UltraGridBand>
										</Bands>
									</igtbl:ultrawebgrid></TD>
							</TR>
							<TR>
								<TD><igtbl:ultrawebgrid id="uwgPedAbono" runat="server" Width="100%">
										<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
											Name="uwgPedAbono" ReadOnly="LevelTwo">
											<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</HeaderStyleDefault>
											<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" CssClass="VisorFrame"></FrameStyle>
											<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</FooterStyleDefault>
											<RowStyleDefault BorderWidth="1px" BorderStyle="Solid" CssClass="VisorRow">
												<Padding Left="3px"></Padding>
												<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
											</RowStyleDefault>
										</DisplayLayout>
										<Bands>
											<igtbl:UltraGridBand></igtbl:UltraGridBand>
										</Bands>
									</igtbl:ultrawebgrid></TD>
							</TR>
							<TR>
								<TD><igtbl:ultrawebgrid id="uwgPedAbierto" runat="server" Width="100%">
										<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
											Name="uwgPedAbierto" ReadOnly="LevelTwo">
											<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</HeaderStyleDefault>
											<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" CssClass="VisorFrame"></FrameStyle>
											<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</FooterStyleDefault>
											<RowStyleDefault BorderWidth="1px" BorderStyle="Solid" CssClass="VisorRow">
												<Padding Left="3px"></Padding>
												<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
											</RowStyleDefault>
										</DisplayLayout>
										<Bands>
											<igtbl:UltraGridBand></igtbl:UltraGridBand>
										</Bands>
									</igtbl:ultrawebgrid></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
			<input id="txtInstancia" type="hidden" name="txtInstancia" runat="server" />
			<input id="hidCodContrato" type="hidden" name="hidCodContrato" runat="server" />			
		</form>

	</body>
</html>
