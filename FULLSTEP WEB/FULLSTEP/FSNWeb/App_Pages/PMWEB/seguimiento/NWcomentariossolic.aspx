<%@ Page Language="vb" AutoEventWireup="false" Codebehind="NWcomentariossolic.aspx.vb" Inherits="Fullstep.FSNWeb.NWcomentariossolic"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    </head>
	<script type="text/javascript">
		function VerDetallePersona(codPer){
            var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detallepersona.aspx?CodPersona=" + codPer.toString(),"_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
            newWindow.focus();
        }
        function VerDetalleProveedor(codProv) {
            var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detalleProveedor.aspx?codProv=" + codProv.toString(),"_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
            newWindow.focus();
        }
        function VerDetallePedido(IdPedido) {
            var newWindow = window.open("detallepedidoPMWEB.aspx?Pedido=" + IdPedido + "&Instancia=" + document.forms["Form1"].elements["txtInstancia"].value, "_blank", "width=950,height=600,status=yes,resizable=yes,top=200,left=200,scrollbars=yes");
            newWindow.focus();
        }
		function VerDetalleProceso(Anyo,GMN1,Proce) {
		    var newWindow = window.open("detalleproceso.aspx?Anyo=" + Anyo + "&GMN1=" + GMN1 + "&Proce=" + Proce + "&Instancia=" + document.forms["Form1"].elements["txtInstancia"].value, "_blank", "width=950,height=600,status=yes,resizable=yes,top=200,left=200,scrollbars=yes");
            newWindow.focus();
		}
		function VerComentario(id) {
		    var newWindow = window.open("comentestado.aspx?ID=" + id,"_blank", "width=700,height=400,status=yes,resizable=no,top=200,left=200");
            newWindow.focus();
		}
		function mostrarDiagramaFlujo()
		{
			var IdInstancia=document.forms["Form1"].elements["txtInstancia"].value  
            var NumFactura=document.forms["Form1"].elements["hidNumFactura"].value 
			var newWindow = window.open("../workflow/flowDiagramLaunch.aspx?Instancia=" + IdInstancia + "&NumFactura=" + NumFactura,"_blank", "width=700,height=400,status=yes,resizable=yes,top=200,left=200");
            newWindow.focus();
		}
		
    </script>	
	<body onload="window.focus();">
		<form id="Form1" method="post" runat="server">
		    
			<TABLE id="tblComent" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" height="100%"
				cellspacing="1" cellpadding="1" width="100%" border="0">
                <tr><td><asp:PlaceHolder ID="phTitulo" runat="server"><table><tr><td><img src="images/tareasPM.jpg" height="45" /></td><td><asp:label id="lblTitulo" runat="server" CssClass="Titulo"></asp:label></td></tr></table></asp:PlaceHolder></td></tr>
				<TR height="10%">
					<TD class="fondoCabecera">
						<TABLE id="tblCabecera" style="WIDTH: 100%; HEIGHT: 75px" cellSpacing="1"
							cellPadding="1" width="100%" border="0">
							<TR>
								<TD style="WIDTH: 15%" nowrap><asp:label id="lblId" runat="server" CssClass="captionBlue"></asp:label></TD>
								<TD style="WIDTH: 35%" nowrap><asp:label id="lblIdBD" runat="server" CssClass="captionDarkBlue">Label</asp:label></TD>
								<TD style="WIDTH: 15%" nowrap><asp:label id="lblFecha" runat="server" CssClass="captionBlue">lblFecha</asp:label></TD>
								<TD style="WIDTH: 35%" nowrap><asp:label id="lblFechaBD" runat="server" CssClass="captionDarkBlue">Label</asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 15%" nowrap><asp:label id="lblNombre" runat="server" CssClass="captionBlue"></asp:label></TD>
								<TD style="WIDTH: 35%" nowrap><asp:label id="lblNombreBD" runat="server" CssClass="captionDarkBlue">Label</asp:label></TD>
								<TD style="WIDTH: 15%"></TD>
								<TD style="WIDTH: 35%"></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 15%" nowrap><asp:label id="lblPeticionario" runat="server" CssClass="captionBlue">lblPeticionario</asp:label></TD>
								<TD style="WIDTH: 35%" nowrap><asp:label id="lblPeticionarioBD" runat="server" CssClass="captionDarkBlue">Label</asp:label></TD>
								<TD style="WIDTH: 15%"></TD>
								<TD style="WIDTH: 35%"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR height="5%">
					<TD align="right"><a class="aPMWeb" href="javascript:mostrarDiagramaFlujo()"><asp:label id="lblFlujo" runat="server" CssClass="flujo" Width="50%">lblFlujo</asp:label></a></TD>
				</TR>
				<TR height="90%">
					<TD><igtbl:ultrawebgrid id="uwgHistorico" runat="server" Width="100%" CaptionAlign="Left">
										<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
											Name="uwgHistorico" ActivationObject-AllowActivation="false" ReadOnly="LevelTwo">
											<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</HeaderStyleDefault>
											<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" CssClass="VisorFrame"></FrameStyle>
											<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</FooterStyleDefault>
											<FixedHeaderStyleDefault CssClass="VisorHead"></FixedHeaderStyleDefault>
											<RowStyleDefault BorderWidth="1px" BorderStyle="Solid">
												<Padding Left="3px"></Padding>
												<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
											</RowStyleDefault>
										</DisplayLayout>
										<Bands>
											<igtbl:UltraGridBand></igtbl:UltraGridBand>
										</Bands>
									</igtbl:ultrawebgrid></TD>
				</TR>
				<tr>
					<td>
						<TABLE id="tblActual" height="40%" width="100%" border="0" runat="server">
							<TR>
								<TD height="3%"><asp:label id="lblActual" runat="server" CssClass="VisorHead" Width="100%">DSituación actual</asp:label></TD>
							</TR>
							<TR>
								<TD><igtbl:ultrawebgrid id="uwgProcesos" runat="server" Width="100%">
										<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
											Name="uwgProcesos" ReadOnly="LevelTwo">
											<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</HeaderStyleDefault>
											<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" CssClass="VisorFrame"></FrameStyle>
											<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</FooterStyleDefault>
											<FixedHeaderStyleDefault CssClass="VisorHead"></FixedHeaderStyleDefault>
											<SelectedRowStyleDefault BorderStyle="None"></SelectedRowStyleDefault>
											<RowStyleDefault BorderWidth="1px" BorderStyle="Solid" CssClass="VisorRow">
												<Padding Left="3px"></Padding>
												<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
											</RowStyleDefault>
										</DisplayLayout>
										<Bands>
											<igtbl:UltraGridBand></igtbl:UltraGridBand>
										</Bands>
									</igtbl:ultrawebgrid></TD>
							</TR>
							<TR>
								<TD><igtbl:ultrawebgrid id="uwgLineasCat" runat="server" Width="100%">
										<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
											Name="uwgLineasCat" ReadOnly="LevelTwo">
											<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</HeaderStyleDefault>
											<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" CssClass="VisorFrame"></FrameStyle>
											<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</FooterStyleDefault>
											<SelectedRowStyleDefault BorderStyle="None"></SelectedRowStyleDefault>
											<RowStyleDefault BorderWidth="1px" BorderStyle="Solid" CssClass="VisorRow">
												<Padding Left="3px"></Padding>
												<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
											</RowStyleDefault>
										</DisplayLayout>
										<Bands>
											<igtbl:UltraGridBand></igtbl:UltraGridBand>
										</Bands>
									</igtbl:ultrawebgrid></TD>
							</TR>
							<TR>
								<TD><igtbl:ultrawebgrid id="uwgPedNeg" runat="server" Width="100%">
										<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
											Name="uwgPedNeg" ReadOnly="LevelTwo">
											<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</HeaderStyleDefault>
											<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" CssClass="VisorFrame"></FrameStyle>
											<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</FooterStyleDefault>
											<SelectedRowStyleDefault BorderStyle="None"></SelectedRowStyleDefault>
											<RowStyleDefault BorderWidth="1px" BorderStyle="Solid" CssClass="VisorRow">
												<Padding Left="3px"></Padding>
												<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
											</RowStyleDefault>
										</DisplayLayout>
										<Bands>
											<igtbl:UltraGridBand></igtbl:UltraGridBand>
										</Bands>
									</igtbl:ultrawebgrid></TD>
							</TR>
							<TR>
								<TD><igtbl:ultrawebgrid id="uwgPedCatNeg" runat="server" Width="100%">
										<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
											Name="uwgPedCatNeg" ReadOnly="LevelTwo">
											<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</HeaderStyleDefault>
											<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" CssClass="VisorFrame"></FrameStyle>
											<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</FooterStyleDefault>
											<SelectedRowStyleDefault BorderStyle="None"></SelectedRowStyleDefault>
											<RowStyleDefault BorderWidth="1px" BorderStyle="Solid" CssClass="VisorRow">
												<Padding Left="3px"></Padding>
												<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
											</RowStyleDefault>
										</DisplayLayout>
										<Bands>
											<igtbl:UltraGridBand></igtbl:UltraGridBand>
										</Bands>
									</igtbl:ultrawebgrid></TD>
							</TR>
							<TR>
								<TD><igtbl:ultrawebgrid id="uwgPedLibres" runat="server" Width="100%">
										<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
											Name="uwgPedLibres" ReadOnly="LevelTwo">
											<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</HeaderStyleDefault>
											<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" CssClass="VisorFrame"></FrameStyle>
											<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</FooterStyleDefault>
											<SelectedRowStyleDefault BorderStyle="None"></SelectedRowStyleDefault>
											<RowStyleDefault BorderWidth="1px" BorderStyle="Solid" CssClass="VisorRow">
												<Padding Left="3px"></Padding>
												<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
											</RowStyleDefault>
										</DisplayLayout>
										<Bands>
											<igtbl:UltraGridBand></igtbl:UltraGridBand>
										</Bands>
									</igtbl:ultrawebgrid></TD>
							</TR>
							<TR>
								<TD><igtbl:ultrawebgrid id="uwgPedDir" runat="server" Width="100%">
										<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
											Name="uwgPedDir" ReadOnly="LevelTwo">
											<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</HeaderStyleDefault>
											<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" CssClass="VisorFrame"></FrameStyle>
											<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</FooterStyleDefault>
											<SelectedRowStyleDefault BorderStyle="None"></SelectedRowStyleDefault>
											<RowStyleDefault BorderWidth="1px" BorderStyle="Solid" CssClass="VisorRow">
												<Padding Left="3px"></Padding>
												<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
											</RowStyleDefault>
										</DisplayLayout>
										<Bands>
											<igtbl:UltraGridBand></igtbl:UltraGridBand>
										</Bands>
									</igtbl:ultrawebgrid></TD>
							</TR>
							<TR>
								<TD><igtbl:ultrawebgrid id="uwgPedAbono" runat="server" Width="100%">
										<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
											Name="uwgPedAbono" ReadOnly="LevelTwo">
											<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</HeaderStyleDefault>
											<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" CssClass="VisorFrame"></FrameStyle>
											<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
												<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
											</FooterStyleDefault>
											<SelectedRowStyleDefault BorderStyle="None"></SelectedRowStyleDefault>
											<RowStyleDefault BorderWidth="1px" BorderStyle="Solid" CssClass="VisorRow">
												<Padding Left="3px"></Padding>
												<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
											</RowStyleDefault>
										</DisplayLayout>
										<Bands>
											<igtbl:UltraGridBand></igtbl:UltraGridBand>
										</Bands>
									</igtbl:ultrawebgrid></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<TR height="5%">
					<TD align="center">
						<P>
							<asp:Label id="lblEnProceso" runat="server" Visible="False" CssClass="SinDatos"></asp:Label></P>
						<P>&nbsp;</P>
						<P>&nbsp;</P>
						<P>&nbsp;</P>
					</TD>
				</TR>
				<TR height="3%">
					<TD align="center"><INPUT id="cmdCerrar" type="button" value="Cerrar" name="cmdCerrar" runat="server" class="botonPMWEB" onclick="window.close()" /></TD>
				</TR>
			</TABLE>
			<INPUT id="txtInstancia" style="Z-INDEX: 102; LEFT: 72px; POSITION: absolute; TOP: 8px"
				type="hidden" name="txtInstancia" runat="server">
                <input id="hidNumFactura" type="hidden" name="hidNumFactura" runat="server" />	
                </form>
            
	</body>
</html>
