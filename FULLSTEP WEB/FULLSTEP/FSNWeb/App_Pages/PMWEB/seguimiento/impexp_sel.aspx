<%@ Page Language="vb" AutoEventWireup="false" Codebehind="impexp_sel.aspx.vb" Inherits="Fullstep.FSNWeb.impexp_sel"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title><%=System.Configuration.ConfigurationManager.AppSettings.Item("TituloVentanas_" & Idioma) %></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>    
	<script language="javascript" id="clientEventHandlersJS">
		//LLama a impexp pasandole el formato seleccionado por el usuario
	    function cmd_onclick(formato) {
			var desgloses = "";
			if (formato == "XML") {
				desgloses = "TABLA"
			} else {
				if (document.getElementById("chkFormato").checked == false)
					desgloses = "TABLA"
				else		
					desgloses = "LISTADO"
			}
            var newWindow = window.open('impexp.aspx?Contrato=' + document.getElementById("idContrato").value + '&Instancia=' + document.getElementById("Instancia").value + '&Certificado=' + document.getElementById("Certificado").value + '&NoConformidad=' + document.getElementById("NoConformidad").value + '&Version=' + document.getElementById("Version").value + '&TipoImpExp=' + document.getElementById("TipoImpExp").value + '&Observadores=' + document.getElementById("Observadores").value + '&Formato=' + formato + '&Desgloses=' + desgloses, '_blank', 'fullscreen=no,height=200,width=500,location=no,menubar=no,resizable=yes,scrollbars=yes,status=yes,titlebar=yes,toolbar=no,left=250,top=250');
            newWindow.focus();
		}

	</script>
	<body>
		<form id="Form1" method="post" runat="server">
				<INPUT id="Instancia" type="hidden" name="Instancia" runat="server">
				<INPUT id="idContrato" type="hidden" name="idContrato" runat="server">
				<INPUT id="NoConformidad" type="hidden" name="NoConformidad" runat="server">
				<INPUT id="TipoImpExp" type="hidden" name="TipoImpExp" runat="server">
				<INPUT id="Version" type="hidden" name="Version" runat="server">
				<INPUT id="Certificado" type="hidden" name="Certificado" runat="server">
				<INPUT id="Observadores" type="hidden" name="Observadores" runat="server">
		
				<INPUT id="cmdHTML" style="Z-INDEX: 101; LEFT: 128px; WIDTH: 60px; POSITION: absolute; TOP: 24px"
				type="button" size="20" value="HTML" name="cmdHTML" language="javascript" onclick="return cmd_onclick('HTML')" class="botonPMWEB"><INPUT 
					id="cmdPDF" style="Z-INDEX: 102; LEFT: 32px; WIDTH: 60px; POSITION: absolute; TOP: 24px; right: 717px;"
				type="button" value="PDF" name="cmdPDF" language="javascript" 
					onclick="return cmd_onclick('PDF')" class="botonPMWEB"><INPUT language="javascript" id="cmdXML" style="Z-INDEX: 103; LEFT: 224px; WIDTH: 60px; POSITION: absolute; TOP: 24px"
				onclick="return cmd_onclick('XML')" type="button" value="XML" name="cmdXML" class="botonPMWEB">
			<asp:CheckBox id="chkFormato" style="Z-INDEX: 104; LEFT: 32px; POSITION: absolute; TOP: 64px"
				runat="server" Width="168px" Height="16px" Font-Names="Tahoma" Font-Size="XX-Small" Text="Mostrar desgloses en listados"></asp:CheckBox>
		</form>
	</body>
</html>
