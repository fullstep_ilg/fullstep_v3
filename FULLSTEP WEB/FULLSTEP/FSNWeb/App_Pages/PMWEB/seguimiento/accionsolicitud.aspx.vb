
    Public Class accionsolicitud
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
    Protected WithEvents lblAccion As System.Web.UI.WebControls.Label
    Protected WithEvents lblId As System.Web.UI.WebControls.Label
    Protected WithEvents lblIdBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblVolver As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecAlta As System.Web.UI.WebControls.Label
    Protected WithEvents lblTipo As System.Web.UI.WebControls.Label
    Protected WithEvents lblCreadoPor As System.Web.UI.WebControls.Label

    Protected WithEvents lblLitCreadoPor As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitFecAlta As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitTipo As System.Web.UI.WebControls.Label

    Protected WithEvents Menu1 As MenuControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oInstancia As FSNServer.Instancia
        Dim tipoSolicitud As Integer = 0

        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = Request("Instancia")
        oInstancia.Cargar(Idioma)

        tipoSolicitud = oInstancia.Solicitud.TipoSolicit
        If Not ClientScript.IsStartupScriptRegistered("tipoSolicitud") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "tipoSolicitud", "<script>var tipoSolicitud = '" & tipoSolicitud & "';</script>")
        End If
        Select Case tipoSolicitud
            Case TiposDeDatos.TipoDeSolicitud.Factura
                Menu1.OpcionMenu = "Facturacion"
                Menu1.OpcionSubMenu = "Seguimiento"
            Case TiposDeDatos.TipoDeSolicitud.Encuesta
                Menu1.OpcionMenu = "Calidad"
                Menu1.OpcionSubMenu = "Encuestas"
            Case TiposDeDatos.TipoDeSolicitud.SolicitudQA
                Menu1.OpcionMenu = "Calidad"
                Menu1.OpcionSubMenu = "SolicitudesQA"
            Case Else
                Menu1.OpcionMenu = "Procesos"
                Menu1.OpcionSubMenu = "Seguimiento"
        End Select

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AccionSolicitud

        Me.lblId.Text = Textos(9)
        Me.lblIdBD.Text = Request("Instancia")
        Me.lblVolver.Text = Textos(14)

        Me.lblLitCreadoPor.Text = Textos(11)
        Me.lblLitFecAlta.Text = Textos(12)
        Me.lblLitTipo.Text = Textos(13)

        Me.lblFecAlta.Text = oInstancia.FechaAlta
        Me.lblTipo.Text = oInstancia.Solicitud.Codigo + " - " + oInstancia.Solicitud.Den(Idioma)
        If oInstancia.PeticionarioProve = "" Then
            lblCreadoPor.Text = oInstancia.NombrePeticionario
        Else
            lblCreadoPor.Text = oInstancia.PeticionarioProve + " (" + oInstancia.NombrePeticionario + ")"
        End If

        'Me.lblTitulo.Text = Textos(5)  'eliminación de solicitud

        If Request("Error") = 2 Then
            Me.lblAccion.Text = Textos(8)  'Imposible eliminar la solicitud. Existen procesos asociados
            Me.lblAccion.CssClass = "SinDatos"
        Else
            Me.lblAccion.Text = Textos(3) 'La solicitud ha sido eliminada correctamente
            Me.lblAccion.CssClass = "titulo"
        End If

    End Sub

End Class

