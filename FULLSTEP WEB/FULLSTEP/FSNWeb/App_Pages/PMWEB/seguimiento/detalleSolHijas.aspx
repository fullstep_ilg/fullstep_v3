<%@ Register TagPrefix="igtbl" Namespace="Infragistics.WebUI.UltraWebGrid" Assembly="Infragistics.WebUI.UltraWebGrid.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="detalleSolHijas.aspx.vb" Inherits="Fullstep.FSNWeb.detalleSolHijas" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<script type="text/javascript">	
	/* Descripci�n: funci�n que se encarga de descargar en una ventana el adjunto seleccionado en la grid
		   Par�metros de entrada:
			Cod: Cod de la persona
		*/		
		function VerDetallePersona(Cod){
			var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detallepersona.aspx?CodPersona=" + Cod.toString(),"_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
            newWindow.focus();
		}
	</script>	
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE class="cabeceraSolicitud" id="tblCabecera" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 16px; HEIGHT: 75px"
				cellSpacing="1" cellPadding="1" width="100%" border="0">
				<TR>
					<TD style="WIDTH: 15%"><asp:label id="lblId" runat="server" CssClass="captionBlue" Width="100%"></asp:label></TD>
					<TD style="WIDTH: 352px"><asp:label id="lblIdBD" runat="server" CssClass="captionDarkBlue" Width="100%">Label</asp:label></TD>
					<TD style="WIDTH: 15%"><asp:label id="lblFecha" runat="server" CssClass="captionBlue" Width="100%">lblFecha</asp:label></TD>
					<TD style="WIDTH: 352px"><asp:label id="lblFechaBD" runat="server" CssClass="captionDarkBlue" Width="100%">Label</asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 15%"><asp:label id="lblNombre" runat="server" CssClass="captionBlue" Width="100%"></asp:label></TD>
					<TD style="WIDTH: 352px"><asp:label id="lblNombreBD" runat="server" CssClass="captionDarkBlue" Width="100%">Label</asp:label></TD>
					<TD style="WIDTH: 15%"></TD>
					<TD style="WIDTH: 352px"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 10%"><asp:label id="lblPeticionario" runat="server" CssClass="captionBlue" Width="100%">lblPeticionario</asp:label></TD>
					<TD style="WIDTH: 352px"><asp:label id="lblPeticionarioBD" runat="server" CssClass="captionDarkBlue" Width="100%">Label</asp:label></TD>
					<TD style="WIDTH: 10%"></TD>
					<TD style="WIDTH: 352px"></TD>
				</TR>
			</TABLE>
			<TABLE id="tblWorkflow" style="Z-INDEX: 101; LEFT: 8px; WIDTH: 100%; POSITION: absolute; TOP: 128px; HEIGHT: 213px"
				cellSpacing="1" cellPadding="1" border="0">
				<TR>
					<TD vAlign="top"><igtbl:ultrawebgrid id="uwgSolicitudes" runat="server" Width="100%" CaptionAlign="Left" Height="203px">
							<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
								Name="uwgSolicitudes" TableLayout="Fixed">
								<AddNewBox>
									<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">

<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
</BorderDetails>

									</Style>
								</AddNewBox>
								<Pager>
									<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">

<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
</BorderDetails>

									</Style>
								</Pager>
								<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
									<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
								</HeaderStyleDefault>
								<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None" Height="203px"
									CssClass="VisorFrame"></FrameStyle>
								<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
									<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
								</FooterStyleDefault>
								<ActivationObject BorderStyle="None" BorderWidth="" BorderColor=""></ActivationObject>
								<FixedHeaderStyleDefault CssClass="VisorHead"></FixedHeaderStyleDefault>
								<EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
								<SelectedRowStyleDefault BorderStyle="None"></SelectedRowStyleDefault>
								<RowStyleDefault BorderWidth="1px" BorderStyle="Solid">
									<Padding Left="3px"></Padding>
									<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
								</RowStyleDefault>
							</DisplayLayout>
							<Bands>
								<igtbl:UltraGridBand AllowSorting="OnClient"></igtbl:UltraGridBand>
							</Bands>
						</igtbl:ultrawebgrid></TD>
				</TR>
			</TABLE>
			<TABLE id="TableTexto" style="Z-INDEX: 103; LEFT: 8px; WIDTH: 488px; POSITION: absolute; TOP: 96px; HEIGHT: 30px"
				cellSpacing="1" cellPadding="1" width="488" border="0">
				<TR>
					<TD style="HEIGHT: 13px"><asp:label id="lbltxtCabecera" runat="server" CssClass="UnderlineRed" Width="472px" Height="10px">Label</asp:label></TD>
				</TR>
			</TABLE>
			<TABLE id="Tablecerrar" style="Z-INDEX: 104; LEFT: 8px; WIDTH: 100%; POSITION: absolute; TOP: 344px; HEIGHT: 20px"
				cellSpacing="1" cellPadding="1" width="863" border="0">
				<TR>
					<td width="45%"></td>
					<TD width="10%" style="HEIGHT: 13px" align="center"><a class="aPMWeb" href="#" id="lblCerrar" onclick="window.close()" name="lblCerrar" runat="server">cerrar</a></TD>
					<td width="45%"></td>
				</TR>
			</TABLE>
		</form>
	</body>
</html>
