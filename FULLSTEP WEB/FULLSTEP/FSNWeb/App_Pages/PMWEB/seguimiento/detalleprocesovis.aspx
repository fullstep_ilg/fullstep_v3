<%@ Register TagPrefix="igtbl" Namespace="Infragistics.WebUI.UltraWebGrid" Assembly="Infragistics.WebUI.UltraWebGrid.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="detalleprocesovis.aspx.vb" Inherits="Fullstep.FSNWeb.detalleprocesovis" enableViewState="False" EnableEventValidation="false"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<script type="text/javascript"><!--
		function irADetalleDest(Cod)
		{
		    var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/denominaciondest.aspx?codDest=" + Cod.toString(),"_blank", "width=400,height=180,status=yes,resizable=no,top=200,left=250");
            newWindow.focus();
		}
		
		function irADetalleUnidad(Cod)
		{
		    var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/denominacionUP.aspx?codUP=" + Cod.toString(),"_blank", "width=400,height=100,status=yes,resizable=no,top=200,left=250");
            newWindow.focus();
		}
		
		function irADetalleFpago(Cod)
		{
	        var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/denominacionFPago.aspx?CodFPago=" + Cod.toString(),"_blank", "width=400,height=100,status=yes,resizable=no,top=200,left=250");
            newWindow.focus();
		}
		
		function irADetalleProveedor(Cod)
		{
	        var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detalleproveedor.aspx?codProv=" + Cod.toString(),"_blank", "width=500,height=200,status=yes,resizable=no,top=200,left=250");
            newWindow.focus();
		}
	
		function show_atached_files(Instancia,Anyo,GMN1,Proce,Grupo,Item,Prove,Ofe,GrupoCod)
		{
		    var newWindow = window.open("atachedfilesProceso.aspx?Instancia=" + Instancia + "&Anyo=" + Anyo + "&GMN1=" + GMN1 + "&Proce=" + Proce  + "&Grupo=" + Grupo + "&Item=" + Item + "&Prove=" + Prove + "&Ofe=" + Ofe + "&GrupoCod=" + GrupoCod,"_blank", "width=750,height=315,status=yes,resizable=no,top=200,left=200");
            newWindow.focus();
		}
		function imprimir(Instancia,Anyo,GMN1,Proce)
		{
		window.open("detalleprocesovis.aspx?anyo=" + Anyo + "&gmn1=" +  GMN1 + "&Proce=" + Proce + "&Instancia=" + Instancia +  "&PDF=1","fraPMDetalleProcesoHidden")
		}
		
--></script>	
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table2" style="Z-INDEX: 100; LEFT: 16px; POSITION: absolute; TOP: 10px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD class="bordeadoAzul">
						<TABLE id="tblCabecera" width="100%" border="0">
							<TR>
								<TD colSpan="3"><asp:label id="lblTitulo" runat="server" Width="704px" CssClass="CaptionDarkBlue"></asp:label></TD>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 90px; HEIGHT: 38px"><asp:label id="lblProceso" runat="server" Width="128px" CssClass="CaptionBlue"></asp:label></TD>
								<TD style="WIDTH: 379px; HEIGHT: 38px"><asp:label id="lblProcBD" runat="server" Width="100%" CssClass="CaptionDarkBlue"></asp:label></TD>
								<TD style="HEIGHT: 38px" vAlign="top" align="right"><INPUT language="javascript" class="botonPMWEB" id="cmdImprimir" onclick="return imprimir();"
										type="button" value="PDF" name="cmdImprimir" runat="server"></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 90px; HEIGHT: 38px"><asp:label id="lblResp" runat="server" Width="136px" CssClass="CaptionBlue"></asp:label></TD>
								<TD style="WIDTH: 379px; HEIGHT: 38px"><asp:label id="lblResponsable" runat="server" Width="100%" CssClass="CaptionDarkBlue"></asp:label></TD>
								<TD align="left"><asp:label id="lblEstado" runat="server" Width="100%" CssClass="CaptionDarkBlue"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR class="bordeadoAzul">
					<TD class="bordeadoAzul">
						<TABLE class="bordeadoAzul" id="tblProceso" style="HEIGHT: 215px" cellSpacing="1" cellPadding="1"
							width="100%" border="0" runat="server">
							<TR>
								<TD colSpan="9"><asp:label id="lblDatosGen" runat="server" Width="100%" CssClass="CaptionBlue">DDatos generales del proceso</asp:label></TD>
							</TR>
							<TR>
								<TD class="subtitulo" vAlign="bottom" bgColor="dimgray" colSpan="9" height="3"></TD>
							</TR>
							<TR>
								<TD colSpan="9">
									<TABLE id="tblDatosProceso" width="100%" border="0" runat="server">
										<TR>
											<TD><asp:label id="lblLitMaterial" runat="server" Width="100%" CssClass="captionBlueSmall">DFamilia de material:</asp:label></TD>
											<TD><asp:label id="lblMaterial" runat="server" Width="100%" CssClass="captionBlueSmall">Label</asp:label></TD>
										</TR>
										<TR>
											<TD><asp:label id="lblLitMoneda" runat="server" Width="100%" CssClass="captionBlueSmall">DMoneda:</asp:label></TD>
											<TD><asp:label id="lblMoneda" runat="server" Width="100%" CssClass="captionBlueSmall">Label</asp:label></TD>
										</TR>
										<TR>
											<TD><asp:label id="lblApertura" runat="server" Width="100%" CssClass="captionBlueSmall">DApertura</asp:label></TD>
											<TD><asp:label id="lblFechaAper" runat="server" Width="100%" CssClass="captionBlueSmall">Label</asp:label></TD>
										</TR>
										<TR>
											<TD><asp:label id="lblLitFecLimite" runat="server" Width="100%" CssClass="captionBlueSmall">DL�mite para recibir ofertas</asp:label></TD>
											<TD><asp:label id="lblFecLimite" runat="server" Width="100%" CssClass="captionBlueSmall">Label</asp:label></TD>
										</TR>
										<TR>
											<TD><asp:label id="lblLitAdjudicacion" runat="server" Width="100%" CssClass="captionBlueSmall">DAdjudicaci�n:</asp:label></TD>
											<TD><asp:label id="lblAdjudicacion" runat="server" Width="100%" CssClass="captionBlueSmall">Label</asp:label></TD>
										</TR>
										<TR>
											<TD><asp:label id="lblLitNecesidad" runat="server" Width="100%" CssClass="captionBlueSmall">DNecesidad:</asp:label></TD>
											<TD><asp:label id="lblNecesidad" runat="server" Width="100%" CssClass="captionBlueSmall">Label</asp:label></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD align="center"><a id="lblCerrar" class="aPMWeb" onclick="window.parent.close()" href="#" name="lblCerrar" runat="server">cerrar</a></TD>
				</TR>
			</TABLE>
			<INPUT id="txtBody" style="Z-INDEX: 103; LEFT: 8px; WIDTH: 128px; POSITION: absolute; TOP: 8px; HEIGHT: 22px"
				type="hidden" size="16" name="txtBody" runat="server">&nbsp;
		</form>
	</body>
</html>
