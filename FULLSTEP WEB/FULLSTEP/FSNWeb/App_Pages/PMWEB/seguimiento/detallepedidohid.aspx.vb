Public Class detallepedidohid
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        Dim sPath As String = Request("path")

        If sPath Is Nothing Then Exit Sub

        Dim ofile As New System.IO.FileInfo(sPath)

        Dim sBytes As Long = ofile.Length

        Dim arrPath2() As String = sPath.Split("\")

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "imprimir", "<script>var newWindow = window.open(""" & ConfigurationManager.AppSettings("rutaFS") & "_Common/download.aspx?path=" & arrPath2(UBound(arrPath2) - 1) & "&nombre=" & Server.UrlEncode(arrPath2(UBound(arrPath2))) & "&datasize=" & CStr(sBytes) & """,""_blank"",""width=600,height=275,status=no,resizable=yes,top=200,left=200,menubar=yes"");newWindow.focus();</script>")
    End Sub

End Class
