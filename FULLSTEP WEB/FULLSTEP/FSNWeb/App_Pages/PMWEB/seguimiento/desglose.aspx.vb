
    Public Class segdesglose
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblSubTitulo As System.Web.UI.WebControls.Label
        Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
        Protected WithEvents INPUTDESGLOSE As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents lblTituloData As System.Web.UI.WebControls.Label
        Protected WithEvents cmdGuardar As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents cmdCalcular As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents Instancia As System.Web.UI.HtmlControls.HtmlInputHidden

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
    Private Const IncludeScriptFormat As String = ControlChars.CrLf & "<script type=""{0}"" src=""{1}""></script>"
    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"

    ''' <summary>
    ''' Cargar un desglose en un popup
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">evento de sistema</param>        
    ''' <remarks>Llamada desde: seguimiento/Nwdetallesolicitud.aspx; Tiempo m�ximo:0,1</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim idCampo As Integer = Request("campo")
        Dim sInput As String = Request("Input")
        Dim lInstancia As Integer = Request("instancia")
        Dim oCampo As FSNServer.Campo

        INPUTDESGLOSE.Value = sInput
        Instancia.Value = lInstancia

        oCampo = FSNServer.Get_Object(GetType(FSNServer.Campo))
        oCampo.Id = idCampo
        oCampo.LoadInst(lInstancia, Idioma)

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Desglose

        lblTitulo.Text = Textos(0)
        lblTituloData.Text = oCampo.DenSolicitud(Idioma)

        If Request("BotonCalcular") <> Nothing Then
            If Request("BotonCalcular") = 1 Then
                cmdCalcular.Visible = True
            Else
                cmdCalcular.Visible = False
            End If
        End If

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ruta", "<script>var ruta = '" & ConfigurationManager.AppSettings("ruta") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "version") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "version", _
                "<script>var version = '" & System.Configuration.ConfigurationManager.AppSettings("version") & "';</script>")
        End If

        Dim bSoloLectura As Boolean = (Request("SoloLectura") = "1")
        Dim oucDesglose As desgloseControl = Me.FindControl("ucDesglose")
        oucDesglose.SoloLectura = bSoloLectura
        oucDesglose.Version = Request("Version")
        oucDesglose.PM = True
        oucDesglose.Titulo = oCampo.DenGrupo(Idioma) + ";" + oCampo.Den(Idioma)
        oucDesglose.TipoSolicitud = oCampo.TipoSolicitud
        oucDesglose.MonedaSolicitud = oCampo.MonedaSolicitud

        Me.FindControl("frmAlta").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())

        cmdGuardar.Value = Textos(2)
        cmdGuardar.Attributes.Add("onclick", "return actualizarCampoDesgloseYCierre()")
        cmdCalcular.Value = Textos(3)
    End Sub
End Class