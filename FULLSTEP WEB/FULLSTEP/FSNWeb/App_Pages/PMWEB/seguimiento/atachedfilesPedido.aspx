<%@ Page Language="vb" AutoEventWireup="false" Codebehind="atachedfilesPedido.aspx.vb" Inherits="Fullstep.FSNWeb.atachedfilesPedido"%>
<%@ Register TagPrefix="igtbl" Namespace="Infragistics.WebUI.UltraWebGrid" Assembly="Infragistics.WebUI.UltraWebGrid.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<script type="text/javascript">
		/* Descripci�n: funci�n que se encarga de descargar en una ventana el adjunto seleccionado en la grid
		   Par�metros de entrada:
			IdAdjunto: id del adjunto
		*/
		function Descargar(IdAdjunto) {
		    var pedido = document.querySelector("#txtPedido").value
		    var linea = document.querySelector("#txtLinea").value
		    var tipo = 5
		    if (document.querySelector("#lblDenLinea") != null)
				tipo = 6
			
			var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/attach.aspx?id=" + IdAdjunto.toString() + "&pedido=" + pedido + "&linea=" + linea + "&tipo=" + tipo , "_blank","width=600,height=275,status=no,resizable=yes,top=200,left=200,menubar=yes");
            newWindow.focus();
		}
	</script>	
	<body>
		<form id="Form1" method="post" runat="server">
			<P><INPUT id="txtPedido" type="hidden"	name="txtPedido" runat="server">
				<INPUT id="txtLinea" type="hidden"	name="txtLinea" runat="server">
				<TABLE id="tblCabecera" width="100%" runat="server">
					<TR>
						<TD style="WIDTH: 258px">
							<DIV title="  " style="DISPLAY: inline; WIDTH: 70px; HEIGHT: 15px" ms_positioning="FlowLayout"></DIV>
						</TD>
						<TD style="WIDTH: 506px"></TD>
						<TD colSpan="2"></TD>
					</TR>
					<tr>
						<td style="WIDTH: 258px; HEIGHT: 21px">
							<asp:label id="lblSolicitud" runat="server" CssClass="captionBlue" Width="100%">DSolicitud:</asp:label></td>
						<td style="WIDTH: 506px; HEIGHT: 21px" colSpan="3" rowSpan="1">
							<asp:label id="lblDenSolicitud" runat="server" CssClass="captionDarkBlue" Width="100%">DDenSolicitud</asp:label></td>
						<td colSpan="2" style="HEIGHT: 21px"></td>
					</tr>
					<TR>
						<TD style="WIDTH: 258px; HEIGHT: 33px" width="258"><asp:label id="lblPedido" runat="server" CssClass="captionBlue" Width="100%"> DPedido:</asp:label></TD>
						<td style="WIDTH: 506px"><asp:label id="lblDenPedido" runat="server" CssClass="captionDarkBlue" Width="100%"> DDenPedido</asp:label></td>
						<TD style="HEIGHT: 33px" align="right" width="20%"><asp:label id="lblDescargar" runat="server" CssClass="parrafo" Visible="False">Descargar todos</asp:label></TD>
						<TD style="HEIGHT: 33px" align="center" width="10%"><asp:imagebutton id="imgDescarga" runat="server" ImageUrl="../_common/images/DescargarEspecif.gif"
								Visible="False"></asp:imagebutton></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 258px; HEIGHT: 33px" width="258">
							<asp:Label id="lblLinea" runat="server" Width="100%" CssClass="captionBlue">DLinea:</asp:Label></TD>
						<TD style="WIDTH: 506px">
							<asp:Label id="lblDenLinea" runat="server" Width="100%" CssClass="captionDarkBlue">DDenLinea</asp:Label></TD>
						<TD style="HEIGHT: 33px" align="right" width="20%" colSpan="1" rowSpan="1"></TD>
						<TD style="HEIGHT: 33px" align="center" width="10%"></TD>
					</TR>
				</TABLE>
			</P>
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><igtbl:ultrawebgrid id="uwgFiles" runat="server" Width="100%" Height="192px">
							<DisplayLayout AllowAddNewDefault="Yes" RowHeightDefault="20px" Version="4.00" HeaderClickActionDefault="SortSingle"
								BorderCollapseDefault="Separate" Name="xctl0xuwgFiles" AllowUpdateDefault="Yes">
								<AddNewBox Hidden="False">
									<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray" CustomRules="visibility:hidden;height:0px;">

<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
</BorderDetails>

									</Style>
								</AddNewBox>
								<Pager>
									<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">

<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
</BorderDetails>

									</Style>
								</Pager>
								<HeaderStyleDefault Font-Size="XX-Small" Font-Names="Verdana" BackColor="WhiteSmoke" CssClass="VisorHead"></HeaderStyleDefault>
								<FrameStyle Width="100%" Height="192px" CssClass="VisorFrame"></FrameStyle>
								<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
									<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
								</FooterStyleDefault>
								<ActivationObject AllowActivation="False"></ActivationObject>
								<FixedHeaderStyleDefault CssClass="VisorHead"></FixedHeaderStyleDefault>
								<EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
								<RowStyleDefault BorderWidth="1px" Font-Size="XX-Small" Font-Names="Verdana" BorderColor="Gray" BorderStyle="Solid"
									CssClass="VisorRow">
									<Padding Left="3px"></Padding>
									<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
								</RowStyleDefault>
							</DisplayLayout>
							<Bands>
								<igtbl:UltraGridBand AllowUpdate="Yes" AllowAdd="Yes" RowSelectors="No">
									<HeaderStyle CssClass="CabeceraCampoArchivo"></HeaderStyle>
									<RowAlternateStyle CssClass="ugfilaalttabla"></RowAlternateStyle>
									<Columns>
										<igtbl:UltraGridColumn HeaderText="Descargar" Key="DESCARGAR" BaseColumnName="">
											<CellStyle HorizontalAlign="Center"></CellStyle>
											<Footer Key="DESCARGAR"></Footer>
											<Header Key="DESCARGAR" Caption="Descargar"></Header>
										</igtbl:UltraGridColumn>
										<igtbl:UltraGridColumn HeaderText="Sustituir" Key="SUSTITUIR" BaseColumnName="">
											<CellStyle HorizontalAlign="Center"></CellStyle>
											<Footer Key="SUSTITUIR"></Footer>
											<Header Key="SUSTITUIR" Caption="Sustituir"></Header>
										</igtbl:UltraGridColumn>
										<igtbl:UltraGridColumn HeaderText="Eliminar" Key="ELIMINAR" BaseColumnName="">
											<CellStyle HorizontalAlign="Center"></CellStyle>
											<Footer Key="ELIMINAR"></Footer>
											<Header Key="ELIMINAR" Caption="Eliminar"></Header>
										</igtbl:UltraGridColumn>
									</Columns>
									<RowStyle CssClass="ugfilatabla"></RowStyle>
								</igtbl:UltraGridBand>
							</Bands>
						</igtbl:ultrawebgrid>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
