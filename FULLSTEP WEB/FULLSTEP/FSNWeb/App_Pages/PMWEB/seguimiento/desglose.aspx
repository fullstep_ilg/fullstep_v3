﻿<%@ Register TagPrefix="uc2" TagName="desglose" Src="../alta/desglose.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="desglose.aspx.vb" Inherits="Fullstep.FSNWeb.segdesglose"%>
<%@ OutputCache Duration="1" VaryByParam="None"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%--<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">--%>
<html>
	<head runat="server">
		<title>desglose</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	
	<script type="text/javascript">
		//Revisado por: blp. Fecha: 03/10/2011
		//Actualiza los campos del desglose en la página de alta y cierra el popup
		//Llamada desde desglose.aspx -> cmdGuardar -> onclick Max. <1 seg
		function actualizarCampoDesgloseYCierre()
		{
			var bMensajePorMostrar = document.getElementById("bMensajePorMostrar")

			if (bMensajePorMostrar.value != "1") {
				actualizarCampoDesglose()
				window.close() 		
			}else 
				bMensajePorMostrar.value = "0";
			bMensajePorMostrar = null;
		}


		/*
		''' Revisado por: blp. Fecha: 03/10/2011
		''' función que lanza la función actualizarCampoDesglose y CalcularCamposCalculados de la página de origen
		''' llamada desde evento onclick del botón cmdCalcular. Máximo < 1 seg
		*/
		function CalcularDesgloseCalculados()
		{
			actualizarCampoDesglose()
			var p = window.opener
			p.CalcularCamposCalculados()
			p = null;
		}

		/* Revisado por: blp. Fecha: 03/10/2011
		''' <summary>
		''' javascript para cuando das a guardar cambios y cerrar 
		'''		Coge toda la información de los entrys y la mete/quita de los html de la pantalla q contiene al desglose.
		''' </summary>       
		''' <remarks>Llamada desde:	actualizarCampoDesgloseYCierre CalcularDesgloseCalculados; Tiempo máximo: 0 </remarks>*/
		function actualizarCampoDesglose() {
			var p = window.opener
			var linea = new Array()
			var oDesglose = p.fsGeneralEntry_getById(document.getElementById("INPUTDESGLOSE").value)
			//uwtGrupos__ctl0_147_fsentry2293__numRows

			var oTbl = document.getElementById(arrDesgloses[0])
			var sPre = arrDesgloses[0].replace("tblDesglose", "")
			oTbl = null;
			var iMaxIndex = document.getElementById(sPre + "numRows").value
			var k;
			var oUni;
			var oMat;
			for (i = 1; i <= iMaxIndex; i++)
				{
				linea = new Array()
				if (document.getElementById(sPre + i.toString() + "_Deleted") || document.getElementById(sPre + "_" + i.toString() + "_Deleted")) {
					for (var oCampo in arrCampos) {
						var s = arrCampos[oCampo]
						var re = /fsentry/
						while (s.search(re) >= 0) {
							s = s.replace(re,"fsdsentry_" + i.toString() + "_")
							}
						oCampo = fsGeneralEntry_getById(s)

						if (oCampo) {
						    if (oCampo.tipoGS != 3000) {
						        if (oCampo.tipoGS == 118)
						            linea[linea.length] = s + "_*"
						        else
						            linea[linea.length] = s

						        if (oCampo.tipo == 8) {
						            k = linea.length
						            linea[k] = new Array()
						            linea[k][0] = document.getElementById(oCampo.id + "__hAct").value
						            linea[k][1] = document.getElementById(oCampo.id + "__hNew").value
						            linea[k][2] = oCampo.getValue();
						        }
						        else
						            if (oCampo.tipoGS == 118) {
						                k = linea.length
						                linea[k] = new Array()
						                linea[k][0] = oCampo.getDataValue()
						                linea[k][1] = oCampo.articuloGenerico;
						                linea[k][2] = oCampo.DenArticuloModificado;
						                linea[k][3] = oCampo.codigoArticulo;
						                if (oCampo.idEntryUNI) {
						                    oUni = fsGeneralEntry_getById(oCampo.idEntryUNI)
						                    if ((!oUni) && (oCampo.UnidadDependent)) { //La Unidad está oculta
						                        linea[linea.length] = oCampo.idEntryUNI
						                        linea[linea.length] = oCampo.UnidadDependent.value
						                    }
						                }
						                oMat = fsGeneralEntry_getById(oCampo.dependentfield)
						                if (!oMat) //el material estÃ¡ oculto
						                {
						                    linea[linea.length] = oCampo.dependentfield
						                    linea[linea.length] = oCampo.Dependent.value
						                }
						            }
						            else
						                if ((oCampo.tipoGS == 119) || (oCampo.tipoGS == 104)) {
						                    linea[linea.length] = oCampo.getDataValue()
						                    if (oCampo.tipoGS == 104 || oCampo.tipoGS == 119) {
						                        oMat = fsGeneralEntry_getById(oCampo.dependentfield)
						                        if (!oMat) //el material está oculto
						                        {
						                            linea[linea.length] = oCampo.dependentfield
						                            linea[linea.length] = oCampo.Dependent.value
						                        }
						                    }
						                    if (oCampo.idEntryUNI) {
						                        oUni = fsGeneralEntry_getById(oCampo.idEntryUNI)
						                        if ((!oUni) && (oCampo.UnidadDependent)) {
						                            linea[linea.length] = oCampo.idEntryUNI
						                            linea[linea.length] = oCampo.UnidadDependent.value
						                        }
						                    }
						                } else
						                    if (oCampo.tipoGS == 2000) {
						                        k = linea.length
						                        linea[k] = new Array()
						                        linea[k][0] = oCampo.getValue();
						                        linea[k][1] = oCampo.dataValue;
						                    }
						                    else
						                        if ((oCampo.tipoGS == 0) && (oCampo.intro == 1) && ((oCampo.tipo == 2) || (oCampo.tipo == 3)))
						                            linea[linea.length] = oCampo.getValue()
						                        else {
						                            if (oCampo.tipo == 2) {
						                                if (oCampo.getDataValue() != null) {
						                                    if (oCampo.tipoGS == 125) {
						                                        linea[linea.length] = oCampo.getDataValue()
						                                    }
						                                    else
						                                        linea[linea.length] = oCampo.getDataValue().toString().replace(",", ".")
						                                } else
						                                    linea[linea.length] = oCampo.getDataValue()
						                            }
						                            else
						                                if ((oCampo.tipoGS == 0) && (oCampo.intro == 1) && ((oCampo.tipo == 1) || (oCampo.tipo == 5) || (oCampo.tipo == 6))) {
						                                    k = linea.length
						                                    linea[k] = new Array()
						                                    linea[k][0] = oCampo.getDataValue()
						                                    linea[k][1] = oCampo.getValue()
						                                }
						                                else {
						                                    if (oCampo.intro == 2) {
						                                        linea[linea.length] = oCampo.getValue();
						                                    }
						                                    else {
						                                        linea[linea.length] = oCampo.getDataValue();
						                                    }
						                                }
						                        }
						    }
						}
					}
					oDesglose.addDesgloseLine(linea)
				}
				else
					{
					for (oCampo in arrCampos)
						{
						s=arrCampos[oCampo]
						re=/fsentry/
						while (s.search(re)>=0)
							{
							s = s.replace(re,"fsdsentry_" + i.toString() + "_")
							}
						linea[linea.length]=s
						}
						
					oDesglose.removeDesgloseLine(linea)
					}
					
				}
			oNumRows = p.document.getElementById(oDesglose.id + "__numRows")
			oNumRows.value = iMaxIndex
			oNumRows = p.document.getElementById(oDesglose.id + "__numTotRows")
			oNumRows.value = iMaxIndex

			if (p.document.forms["frmSubmit"]) {
				s = p.document.forms["frmSubmit"].innerHTML;
				s = replaceAll(s, "MOVERPOPUP_", "MOVER_");
				p.document.forms["frmSubmit"].innerHTML = s;
			}
			p = null;
			linea = null;
			oDesglose = null;
			sPre = null;
			iMaxIndex = null;
			oCampo = null;
			s = null;
			re = null;
			k = null;
			oUni = null;
			oMat = null;
		}
			
		function localEval(s)
		{
		eval(s)
		}
		/* Revisado por: blp. Fecha: 03/10/2011
		''' <summary>
		''' Inicializa la pantalla
		''' Los cambios hechos en un desglose popup solo se salvan si pulsas "aceptar y guardar cambios" así q los
		''' MOVERPOPUP q se hayan hecho y no confirmado deben ser deshechados al entrar por si entras, mueves lineas,
		''' sales sin aceptar y vuelves a mover lineas.
		''' </summary>
		''' <remarks>Llamada desde: onload(); Tiempo máximo:0</remarks>*/	
		function init() {
		    resize();

		    var p = window.opener
		    if (p.document.forms["frmSubmit"]) {
			    var s = p.document.forms["frmSubmit"].innerHTML;
			    s = replaceAll(s, "MOVERPOPUP_", "NOGRABO_");
			    p.document.forms["frmSubmit"].innerHTML = s;
			    s = null;
		    }
		    p = null;
		}

		//Revisado por: blp. Fecha: 03/10/2011
		//Redimensiona el tamaño del desglose que se carga en la página
		//Llamada desde javascript. Máx < 1seg
		function resize()
			{
				var sDiv;
				for (i = 0; i < arrDesgloses.length; i++) {
					sDiv = arrDesgloses[i].replace("tblDesglose", "divDesglose")
					document.getElementById(sDiv).style.width = parseFloat(document.body.offsetWidth) - 20 + 'px';
					document.getElementById(sDiv).style.height = getWindowSize("Height") - 100 + 'px';
				}
				sDiv = null;
			}

			//AJAX
			var xmlHttp;
			/*''' <summary>
			''' Crear el objeto para llamar con ajax a ComprobarEnProceso
			''' </summary>
			''' <remarks>Llamada desde: javascript ; Tiempo máximo: 0</remarks>*/
			function CreateXmlHttp() {

				// Probamos con IE
				try {
					// Funcionará para JavaScript 5.0
					xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch (e) {
					try {
						xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					catch (oc) {
						xmlHttp = null;
					}
				}

				// Si no se trataba de un IE, probamos con esto
				if (!xmlHttp && typeof XMLHttpRequest != "undefined") {
					xmlHttp = new XMLHttpRequest();
				}

				return xmlHttp;
			}
				
		//Creamos el objeto xmlHttpRequest
		CreateXmlHttp();

		/*''' <summary>
		''' Hacer una validación a medida de cantidades antes de añadir lineas vinculadas
		''' </summary>
		''' <param name="sRoot">En q desglose, html, se van añadir lineas</param>
		''' <param name="IdCampo">En q desglose, form_campo.id, se van añadir lineas</param>        
		''' <param name="Row">Fila a añadir</param>
		''' <remarks>Llamada desde: PMWeb2008/BuscadorSolicitudes.aspx.vb; Tiempo máximo:0,1</remarks>*/
		function VincularCopiarFilaVacia(sRoot, IdCampo, Row) {
		    CreateXmlHttp();
			if (xmlHttp) {
			    if (Row.getCellFromKey) {
			        var params = "Instancia=" + Row.getCellFromKey("INSTANCIAORIGEN").getValue() + "&Desglose=" + Row.getCellFromKey("DESGLOSEORIGEN").getValue() + "&Linea=" + Row.getCellFromKey("LINEAORIGEN").getValue();
			    } else {
			        var params = "Instancia=" + Row.get_cellByColumnKey("INSTANCIAORIGEN").get_value() + "&Desglose=" + Row.get_cellByColumnKey("DESGLOSEORIGEN").get_value() + "&Linea=" + Row.get_cellByColumnKey("LINEAORIGEN").get_value();
			    }

				xmlHttp.open("POST", "../_common/controlarLineaVinculada.aspx", false);
				xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
				xmlHttp.send(params);

				//Tras q se ejecute sincronamente controlarLineaVinculada.aspx controlamos sus resultados y obramos en 
				//consecuencia.                            
				var retorno;
				if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
					retorno = xmlHttp.responseText;
					if (retorno == 'OK') { //pasa la comprobación
						copiarFilaVacia(sRoot, IdCampo, 0, Row)
					}
					else {
					    var newWindow = window.open("../_common/NoPasoControlarLineaVinculada.aspx?" + params, "_blank", "width=350,height=200,status=yes,resizable=no,top=200,left=300");
					    newWindow.focus();
					}
				}
			}
		}
		/*''' <summary>
		''' Añadir instancia vinculadas
		''' </summary>
		''' <param name="sRoot">En q desglose, html, se van añadir lineas</param>
		''' <param name="IdCampo">En q desglose, form_campo.id, se van añadir lineas</param>        
		''' <param name="sId">Id de la instancia vinculada</param>
		''' <param name="sDen">Descrip de la instancia vinculada</param> 
		''' <remarks>Llamada desde: PMWeb2008/BuscadorSolicitudes.aspx.vb; Tiempo máximo:0,1</remarks>*/
		function SeleccionarSolicitud(sRoot, IdCampo, sId, sDen) {
			copiarFilaVacia(sRoot, IdCampo, 0, null, sId, sDen);
		}
		/*''' <summary>
		''' Mueve la linea indicada de la instancia actual a la instancia indicada
		''' </summary>
		''' <param name="sRoot">nombre entry del desglose</param>	        
		''' <param name="IdCampo">De q desglose se va a mover la linea</param>        
		''' <param name="sId">Id de la instancia a la q mueves</param>
		''' <param name="index">fila q mueves</param>
		''' <param name="ObjCelda">tabla html donde esta el bt mover/copiar/elim.</param>
		''' <param name="Celda">Celda, html, donde esta el bt mover/copiar/elim. A traves de él se saca la fila y tabla html</param>
		''' <param name="Frame">Frame donde esta el desglose a borrar</param>          
		''' <remarks>Llamada desde: PMWeb2008/BuscadorSolicitudes.aspx.vb; Tiempo máximo:0,1</remarks>*/
		function VincularMoverAInstancia(sRoot, IdCampo, sId, Index, ObjCelda, Celda, Frame) {
		    CreateXmlHttp();
			if (xmlHttp) {
				var p = window.parent

				if (window.opener.frmDetalle.VinculacionesPrimerGuardar.value == 1)
					Index = DameLineaMoverAInstancia(IdCampo, Index, 1)

				var params = "Mover=1&Instancia=" + document.getElementById("Instancia").value + "&Desglose=" + IdCampo + "&Linea=" + Index;

				xmlHttp.open("POST", "../_common/controlarLineaVinculada.aspx", false);
				xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
				xmlHttp.send(params);

				//Tras q se ejecute sincronamente controlarLineaVinculada.aspx controlamos sus resultados y obramos en 
				//consecuencia.                            
				var retorno;
				if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
					retorno = xmlHttp.responseText;
					if (retorno == 'OK') { //pasa la comprobación
						MoverAInstancia(IdCampo, sId, Index, 1);

						deleteRow(ObjCelda, sRoot, Index, IdCampo)
						var oFrame = document.getElementById(Frame)
						oFrame.parentNode.removeChild(oFrame);
					}
					else
						if (retorno == 'PANT') {//existen lineas vinculadas pedir confirmacion
						    var newWindow = window.open('../_common/controlarLineaVinculadaMover.aspx?IdCampo=' + IdCampo + '&sId=' + sId + '&Index=' + Index + '&Celda=' + Celda + '&sRoot=' + sRoot + '&Frame=' + Frame + '&PopUp=1', "_blank", "width=350,height=105,status=yes,resizable=no,top=200,left=300");
						    newWindow.focus();
						}
				}
			}
		}    
	</script>
	<body onload="init()">
		<form id="frmAlta" method="post" runat="server">
			<asp:ScriptManager ID="ScriptManager1" runat="server">
                <CompositeScript>
                    <Scripts>
                        <asp:ScriptReference Name="ExtenderBase.BaseScripts.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Name="Common.Common.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Name="Compat.Timer.Timer.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Name="Animation.Animations.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Name="Animation.AnimationBehavior.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Name="PopupExtender.PopupBehavior.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Name="AutoComplete.AutoCompleteBehavior.js" Assembly="AjaxControlToolkit" />
                        <asp:ScriptReference Path="../alta/js/AdjacentHTML.js" />
                        <asp:ScriptReference Path="../alta/js/jsAlta.js" />
                        <asp:ScriptReference Path="~/js/jquery/jquery.min.js" />
                        <asp:ScriptReference Path="~/js/jquery/jquery.tmpl.min.js" />
                        <asp:ScriptReference Path="~/js/jquery/jquery-migrate.min.js" />
                        <asp:ScriptReference Path="~/js/jsUtilities.js" />
                    </Scripts>
                </CompositeScript>
			</asp:ScriptManager>
			<input type="hidden" id="bMensajePorMostrar" name="bMensajePorMostrar" value="0">
			<IFRAME id="iframeWSServer" style="Z-INDEX: 103; LEFT: 8px; VISIBILITY: hidden; POSITION: absolute; TOP: 200px"
				name="iframeWSServer" src="../blank.htm"></IFRAME>
			<table width="100%" border="0">
				<tr>
					<td width="70%"><asp:label id="lblTitulo" style="Z-INDEX: 101" runat="server" CssClass="captionBlue">Alta de solicitud tipo:</asp:label><asp:label id="lblTituloData" runat="server" CssClass="captionDarkBlue" Width="488px">Alta de solicitud tipo:</asp:label></td>
					<td align="left"><INPUT class="botonPMWEB" id="cmdCalcular" onclick="CalcularDesgloseCalculados()" type="button"
							value="Calcular" name="cmdCalcular" runat="server">
					</td>
					<td align="left">
					<input class="botonPMWEB" id="cmdGuardar" type="button" value="Guardar" runat="server" NAME="cmdGuardar">
					</td>
				</tr>
				<tr>
					<td colSpan="3"><uc2:desglose id="ucDesglose" runat="server"></uc2:desglose></td>
				</tr>
			</table>
			<div id="divDropDowns" style="Z-INDEX: 101; VISIBILITY: hidden; POSITION: absolute; TOP: 0px"></div>
			<input id="INPUTDESGLOSE" type="hidden" name="INPUTDESGLOSE" runat="server"> <input id="Instancia" type="hidden" name="Instancia" runat="server">
			<script type="text/javascript">
				if (window.onresize) {
					window.onresize = resize;
				}
				else {
					document.onresize = resize;
				}
			</script>
		</form>
	</body>
</html>
