
    Public Class detalleSolHijas
    Inherits FSNPage



        Private oInstancia As FSNServer.Instancia
        Protected WithEvents lblVolver As System.Web.UI.WebControls.Label
        Protected WithEvents lblId As System.Web.UI.WebControls.Label
        Protected WithEvents lblIdBD As System.Web.UI.WebControls.Label
        Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
        Protected WithEvents lblFechaBD As System.Web.UI.WebControls.Label
        Protected WithEvents lblNombre As System.Web.UI.WebControls.Label
        Protected WithEvents lblNombreBD As System.Web.UI.WebControls.Label
        Protected WithEvents lblPeticionario As System.Web.UI.WebControls.Label
        Protected WithEvents lblPeticionarioBD As System.Web.UI.WebControls.Label
        Protected WithEvents lblActual As System.Web.UI.WebControls.Label
        Protected WithEvents Table1 As System.Web.UI.HtmlControls.HtmlTable
        Protected WithEvents tblActual As System.Web.UI.HtmlControls.HtmlTable
        Protected WithEvents uwgSolicitudes As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
        Protected WithEvents lbltxtCabecera As System.Web.UI.WebControls.Label
    Protected WithEvents lblCerrar As System.Web.UI.HtmlControls.HtmlAnchor

        Private oUser As FSNServer.User


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        Dim idSolicitudPadre As Integer = Request("idSolPadre")
        Dim bHayProcesosOPedidos As Boolean

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.DetalleSolicitudesHijas

        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = idSolicitudPadre
        oInstancia.Load(Idioma, True)

        'Cabecera:
        Me.lblId.Text = Textos(0)
        Me.lblIdBD.Text = oInstancia.ID
        Me.lblFecha.Text = Textos(3)
        Me.lblFechaBD.Text = FormatDate(oInstancia.FechaAlta, FSNUser.DateFormat)
        Me.lblNombre.Text = Textos(1)
        Me.lblNombreBD.Text = oInstancia.Solicitud.Den(Idioma)
        Me.lblPeticionario.Text = Textos(2)
        Me.lblPeticionarioBD.Text = oInstancia.NombrePeticionario

        Me.lbltxtCabecera.Text = Textos(10)
        Me.lblCerrar.InnerText = Textos(11)

        'Rellena la grid
        oInstancia.CargarDetalleSolicitudesHijas(Idioma)

        If oInstancia.DetalleSolicitudesHijas.Tables.Count > 0 Then
            uwgSolicitudes.DataSource = oInstancia.DetalleSolicitudesHijas
            uwgSolicitudes.DataMember = ""
            uwgSolicitudes.DataBind()
        End If

        'Idiomas en las caption de la grid: 
        uwgSolicitudes.Bands(0).Columns.FromKey("IDENTIFICADOR").Header.Caption = Textos(4)
        uwgSolicitudes.Bands(0).Columns.FromKey("TITULO").Header.Caption = Textos(5)
        uwgSolicitudes.Bands(0).Columns.FromKey("PETICIONARIO").Header.Caption = Textos(6)
        uwgSolicitudes.Bands(0).Columns.FromKey("FECHA").Header.Caption = Textos(7)
        uwgSolicitudes.Bands(0).Columns.FromKey("IMPORTE").Header.Caption = Textos(8)
        uwgSolicitudes.Bands(0).Columns.FromKey("ESTADO").Header.Caption = Textos(9)

    End Sub

        Private Sub uwgSolicitudes_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwgSolicitudes.InitializeLayout
            'Hace invisibles algunas columnas

            uwgSolicitudes.Bands(0).Columns.FromKey("PERSONA").Hidden = True
            uwgSolicitudes.Bands(0).Columns.FromKey("MONEDA").Hidden = True

            uwgSolicitudes.Bands(0).Columns.FromKey("PETICIONARIO").CellStyle.CssClass = "TablaLink"
            'formato de la fecha:
            uwgSolicitudes.Bands(0).Columns.FromKey("FECHA").Format = oUser.DateFormat.ShortDatePattern
            'Redimensiona las columnas:
            uwgSolicitudes.Bands(0).Columns.FromKey("IDENTIFICADOR").Width = Unit.Percentage(15)
            uwgSolicitudes.Bands(0).Columns.FromKey("TITULO").Width = Unit.Percentage(25)
            uwgSolicitudes.Bands(0).Columns.FromKey("PETICIONARIO").Width = Unit.Percentage(15)
            uwgSolicitudes.Bands(0).Columns.FromKey("FECHA").Width = Unit.Percentage(15)
            uwgSolicitudes.Bands(0).Columns.FromKey("IMPORTE").Width = Unit.Percentage(15)
            uwgSolicitudes.Bands(0).Columns.FromKey("ESTADO").Width = Unit.Percentage(25)


            uwgSolicitudes.DisplayLayout.RowSelectorsDefault = Infragistics.WebUI.UltraWebGrid.RowSelectors.No
            uwgSolicitudes.DisplayLayout.HeaderStyleDefault.HorizontalAlign = HorizontalAlign.Left


            uwgSolicitudes.Bands(0).Columns.FromKey("IDENTIFICADOR").AllowResize = Infragistics.WebUI.UltraWebGrid.AllowSizing.Free
            uwgSolicitudes.Bands(0).Columns.FromKey("TITULO").AllowResize = Infragistics.WebUI.UltraWebGrid.AllowSizing.Free
            uwgSolicitudes.Bands(0).Columns.FromKey("PETICIONARIO").AllowResize = Infragistics.WebUI.UltraWebGrid.AllowSizing.Free
            uwgSolicitudes.Bands(0).Columns.FromKey("FECHA").AllowResize = Infragistics.WebUI.UltraWebGrid.AllowSizing.Free
            uwgSolicitudes.Bands(0).Columns.FromKey("IMPORTE").AllowResize = Infragistics.WebUI.UltraWebGrid.AllowSizing.Free
            uwgSolicitudes.Bands(0).Columns.FromKey("ESTADO").AllowResize = Infragistics.WebUI.UltraWebGrid.AllowSizing.Free

        End Sub

        Private Sub uwgSolicitudes_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgSolicitudes.InitializeRow
            If Not e.Row.Cells.FromKey("IMPORTE").Value Is Nothing Then
                e.Row.Cells.FromKey("IMPORTE").Value = Replace(e.Row.Cells.FromKey("IMPORTE").Value, ".", ",")
                e.Row.Cells.FromKey("IMPORTE").Value = FSNLibrary.FormatNumber(e.Row.Cells.FromKey("IMPORTE").Value, oUser.NumberFormat) + " " + e.Row.Cells.FromKey("MONEDA").Value
            End If
        e.Row.Cells.FromKey("PETICIONARIO").Value = "<a class='aPMWeb' onclick=""VerDetallePersona('" & e.Row.Cells.FromKey("PERSONA").Value & "');return false;"">" & e.Row.Cells.FromKey("PETICIONARIO").Value & "</a>"
    End Sub


    End Class

