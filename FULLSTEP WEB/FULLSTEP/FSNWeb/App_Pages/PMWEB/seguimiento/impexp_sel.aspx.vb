Public Class impexp_sel
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Instancia As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents chkFormato As System.Web.UI.WebControls.CheckBox
    Protected WithEvents Certificado As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents TipoImpExp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Version As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents NoConformidad As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Observadores As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents idContrato As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ImpExp_Sel

        Instancia.Value = Request("Instancia")
        Certificado.Value = Request("Certificado")
        NoConformidad.Value = Request("NoConformidad")
        Version.Value = Request("Version")
        TipoImpExp.Value = Request("TipoImpExp")
        Observadores.Value = Request("Observadores")
        idContrato.Value = Request("Contrato")

        chkFormato.Text = Textos(2)
    End Sub
End Class

