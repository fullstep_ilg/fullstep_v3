
    Public Class eliminarSolicitud
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
   "<script language=""{0}"">{1}</script>"


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Elimina la instancia:
        Dim oInstancia As FSNServer.Instancia
        Dim oDS As DataSet

        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = Request("Instancia")
        oDS = oInstancia.ItemsProcesoSolicitud()

        If oDS.Tables(0).Rows.Count = 0 Then
            oInstancia.Eliminar(FSNUser.Cod)
            Page.ClientScript.RegisterStartupScript(Me.GetType(),"claveStartUpScript", "<script>EliminarOK(" & oInstancia.ID & ")</script>")
        Else
            'Existen procesos asociados
            Page.ClientScript.RegisterStartupScript(Me.GetType(),"claveStartUpScript", "<script>EliminarNoOKExistenProcesos(" & oInstancia.ID & ")</script>")
        End If
    End Sub

End Class

