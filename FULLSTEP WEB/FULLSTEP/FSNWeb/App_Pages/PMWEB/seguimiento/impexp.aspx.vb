Imports system.xml
Imports System.Xml.Xsl

Public Class impexp

    Inherits FSNPage

    Private oInstancia As FSNServer.Instancia
    Private oCertificado As FSNServer.Certificado
    Private oNoConformidad As FSNServer.NoConformidad
    Private oContrato As FSNServer.Contrato

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents xmlPres As System.Web.UI.WebControls.Xml

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    ''' Revisado por:blp. Fecha: 28/09/2012
    ''' <summary>
    ''' Crea un informe de la solicitud compra/certificado/no conformidad dada (lo dice Request("TipoImpExp")) 
    ''' (ejemplo: solicitud compra Request("Instancia")) en el formato indicado (lo dice Request("Formato")
    ''' ejemplo: XML). 
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">evento de sistema</param>        
    ''' <remarks>Llamada desde: impexp_sel.aspx; Tiempo m�ximo:0,5</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Objetos para montar el documento XML
        Dim oXmlDoc As XmlDocument
        Dim oXmlEmt As XmlElement
        Dim oXMLDatoElemento As XmlElement
        Dim oXMLDatoTexto As XmlText
        Dim oXMLGrupos As XmlElement
        Dim oXMLGrupo As XmlElement
        Dim oXMLCampos As XmlElement
        Dim oXMLCampo As XmlElement
        Dim oXMLValorCampo As XmlElement
        Dim oXMLAcciones As XmlElement
        Dim oXMLAccion As XmlElement
        Dim oXMLRevisor As XmlElement
        Dim oXMLDesglose As XmlElement
        Dim oXMLFilaDesglose As XmlElement
        Dim oXMLColumnaDesglose As XmlElement
        Dim oXMLValorColumnaDesglose As XmlElement
        Dim oXMLDatoElementoDesglose As XmlElement
        Dim oXMLDatoTextoDesglose As XmlText
        Dim sDato As String
        Dim sDenDato As String ' Para la denominaci�n en los casos de tipo GS.
        ' Objetos para obtener la informaci�n del documento
        Dim iVersion As Long
        Dim oGrupo As FSNServer.Grupo
        Dim oCampo As FSNServer.Campo
        Dim oFilaCampo As DataRow
        Dim oDesglose As DataSet
        Dim oFilaDesglose As DataRow
        Dim oDSLista As DataSet
        Dim oAdjunto As DataRow
        Dim sAdjun As String
        Dim iColumna As Long
        Dim iFila As Long
        Dim iNumColumnas As Long
        Dim iNumFilas As Long
        Dim iCampoColumna() As Long ''?
        Dim iTipoColumna() As Integer
        Dim iTipoGSColumna() As Integer
        Dim sDenColumna() As String
        Dim bVisibleColumna() As Boolean
        Dim bIntroColumna() As Boolean
        Dim lTablaExterna() As Long
        Dim lEstado As String = String.Empty
        Dim bGrupoVisible As Boolean
        Dim bCampoVisible As Boolean
        Dim oRow As DataRow

        ' Modulo Idioma
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ImpExp

        ' Observador
        Dim bObservador As Boolean
        If Request("Observadores") = "1" Then
            bObservador = True
        Else
            bObservador = False
        End If

        ' Acci�n
        If Request("TipoImpExp") = "1" Then
            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
            oInstancia.ID = Request("Instancia")
            oInstancia.Load(Idioma, True)
            oInstancia.CargarCamposInstancia(Idioma, FSNUser.CodPersona, , True, bObservador, FSNUser.DateFmt)

        ElseIf Request("TipoImpExp") = "2" Then
            oCertificado = FSNServer.Get_Object(GetType(FSNServer.Certificado))
            oCertificado.ID = Request("Certificado")
            oCertificado.Load(Idioma)

            If Request("Version") <> Nothing Then
                iVersion = Request("Version")
                If iVersion <> Nothing Then
                    oCertificado.Version = iVersion
                    oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                    oInstancia.ID = Request("Instancia")
                End If
            Else
                oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                oInstancia.ID = oCertificado.Instancia
            End If
            oInstancia.Version = oCertificado.Version
            oInstancia.CargarCamposInstancia(Idioma, FSNUser.CodPersona, oCertificado.Proveedor, , , FSNUser.DateFmt)
        ElseIf Request("TipoImpExp") = "3" Then  'no conformidad
            oNoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
            oNoConformidad.ID = Request("NoConformidad")
            oNoConformidad.Load(Idioma, , True)

            If Request("Version") <> Nothing Then
                iVersion = Request("Version")
                If iVersion <> Nothing Then
                    oNoConformidad.Version = iVersion
                    oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                    oInstancia.ID = Request("Instancia")
                End If
            Else
                oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                oInstancia.ID = oNoConformidad.Instancia
            End If
            oInstancia.Version = oNoConformidad.Version
            oInstancia.CargarCamposInstancia(Idioma, FSNUser.CodPersona, oNoConformidad.Proveedor, , , FSNUser.DateFmt)

            oDSLista = oInstancia.CargarTiposEstadoAccion()
        End If

        ' Documento XML con los datos de la instancia
        oXmlDoc = New XmlDocument
        oXmlEmt = oXmlDoc.CreateElement("DOCUMENTO")
        oXmlDoc.AppendChild(oXmlEmt)

        oXMLDatoElemento = oXmlDoc.CreateElement("TIPO")
        oXmlEmt.AppendChild(oXMLDatoElemento)
        If Request("TipoImpExp") = "1" Then
            oXMLDatoTexto = oXmlDoc.CreateTextNode(oInstancia.Solicitud.Den(Idioma))
        ElseIf Request("TipoImpExp") = "2" Then
            oXMLDatoTexto = oXmlDoc.CreateTextNode(oCertificado.TipoDen)
        ElseIf Request("TipoImpExp") = "3" Then
            oXMLDatoTexto = oXmlDoc.CreateTextNode(oNoConformidad.TipoDen)
        End If
        oXMLDatoElemento.AppendChild(oXMLDatoTexto)
        oXMLDatoElemento = Nothing
        oXMLDatoTexto = Nothing

        If Not Request("Contrato") = "" Then
            oContrato = FSNServer.Get_Object(GetType(FSNServer.Contrato))
            oContrato.ID = Request("Contrato")
            oContrato.Load(Idioma)

            oXMLDatoElemento = oXmlDoc.CreateElement("COD")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            oXMLDatoTexto = oXmlDoc.CreateTextNode(oContrato.Codigo)
        ElseIf Request("TipoImpExp") = "2" Then
            oXMLDatoElemento = oXmlDoc.CreateElement("PROVEDEN")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            oXMLDatoTexto = oXmlDoc.CreateTextNode(oCertificado.Proveedor & " - " & oCertificado.ProveDen)
        Else
            oXMLDatoElemento = oXmlDoc.CreateElement("ID")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            oXMLDatoTexto = oXmlDoc.CreateTextNode(oInstancia.ID)
        End If
        oXMLDatoElemento.AppendChild(oXMLDatoTexto)
        oXMLDatoElemento = Nothing
        oXMLDatoTexto = Nothing

        If Request("TipoImpExp") = "1" OrElse Request("TipoImpExp") = "3" Then
            oXMLDatoElemento = oXmlDoc.CreateElement("PETICIONARIO")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            If Request("TipoImpExp") = "1" Then
                oXMLDatoTexto = oXmlDoc.CreateTextNode(oInstancia.NombrePeticionario)
            ElseIf Request("TipoImpExp") = "3" Then
                oXMLDatoTexto = oXmlDoc.CreateTextNode(oNoConformidad.Peticionario)
            End If
            oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing

            oXMLDatoElemento = oXmlDoc.CreateElement("FECHA")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            If Request("TipoImpExp") = "1" Then
                oXMLDatoTexto = oXmlDoc.CreateTextNode(FormatDateTime(oInstancia.FechaAlta, DateFormat.ShortDate))
            ElseIf Request("TipoImpExp") = "3" Then
                oXMLDatoTexto = oXmlDoc.CreateTextNode(FormatDateTime(oNoConformidad.FechaAlta, DateFormat.ShortDate))
            End If
            oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing
        ElseIf Request("TipoImpExp") = "2" Then
            'Fecha Alta
            oXMLDatoElemento = oXmlDoc.CreateElement("FECHAALTA")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            oXMLDatoTexto = oXmlDoc.CreateTextNode(IIf(oCertificado.FechaPub = Nothing, "", FormatDateTime(oCertificado.FechaPub, DateFormat.ShortDate)))
            oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing
            'Fecha limite de cumplimentaci�n 
            oXMLDatoElemento = oXmlDoc.CreateElement("FECHALIMCUMPL")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            oXMLDatoTexto = oXmlDoc.CreateTextNode(IIf(oCertificado.FechaLimCumpl = Nothing, "", FormatDateTime(oCertificado.FechaLimCumpl, DateFormat.ShortDate)))
            oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing
            'Fecha de despublicaci�n 
            oXMLDatoElemento = oXmlDoc.CreateElement("FECHADESPUBL")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            oXMLDatoTexto = oXmlDoc.CreateTextNode(IIf(oCertificado.FechaDesPub = Nothing, "", FormatDateTime(oCertificado.FechaDesPub, DateFormat.ShortDate)))
            oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing
        End If

        If Not Request("Contrato") = "" Then
            oXMLDatoElemento = oXmlDoc.CreateElement("EMPRESA")
            oXmlEmt.AppendChild(oXMLDatoElemento)

            oXMLDatoTexto = oXmlDoc.CreateTextNode(oContrato.Empresa)
            oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing

            oXMLDatoElemento = oXmlDoc.CreateElement("FINICIO")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            oXMLDatoTexto = oXmlDoc.CreateTextNode(oContrato.FechaInicio)
            oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing

            oXMLDatoElemento = oXmlDoc.CreateElement("FFIN")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            oXMLDatoTexto = oXmlDoc.CreateTextNode(oContrato.FechaFin)
            oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing

            oXMLDatoElemento = oXmlDoc.CreateElement("CONTACTO")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            oXMLDatoTexto = oXmlDoc.CreateTextNode(oContrato.Contacto)
            oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing

            oXMLDatoElemento = oXmlDoc.CreateElement("PROVEEDOR")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            oXMLDatoTexto = oXmlDoc.CreateTextNode(oContrato.DenProve)
            oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing

            oXMLDatoElemento = oXmlDoc.CreateElement("MONEDA")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            oXMLDatoTexto = oXmlDoc.CreateTextNode(oContrato.DenMoneda)
            oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing
        End If

        ' Estado
        If Request("TipoImpExp") = "3" Then
            Select Case oNoConformidad.Estado
                Case 0  'Guardada
                    lEstado = Textos(0)
                Case 1  'Abierta
                    lEstado = Textos(7)
                Case 2  'Cerrada pos. dentro de plazo
                    lEstado = Textos(8)
                Case 3  'Cerrada pos. fuera de plazo
                    lEstado = Textos(9)
                Case 4  'Cerrada negativamente
                    lEstado = Textos(10)
            End Select
        Else
            Select Case oInstancia.Estado
                Case 0
                    lEstado = Textos(0)
                Case 2, 100
                    lEstado = Textos(1)
                Case 7, 101
                    lEstado = Textos(2)
                Case 6, 102
                    lEstado = Textos(3)
                Case 8, 103
                    lEstado = Textos(4)
                Case 104
                    lEstado = Textos(5)
                Case Else
                    lEstado = Textos(6)
            End Select
        End If

        If Not Request("TipoImpExp") = "2" Then
            oXMLDatoElemento = oXmlDoc.CreateElement("ESTADO")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            oXMLDatoTexto = oXmlDoc.CreateTextNode(lEstado)
            oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing
        End If

        ' Aprobador actual
        If Not oInstancia.AprobadorActual Is Nothing Then
            oXMLDatoElemento = oXmlDoc.CreateElement("APROBADOR_ACTUAL")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            oXMLDatoTexto = oXmlDoc.CreateTextNode(oInstancia.NombreAprobadorActual)
            oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing
        End If

        ' Importe
        If Request("TipoImpExp") = "1" Then
            If IsNumeric(oInstancia.Importe) Then
                oXMLDatoElemento = oXmlDoc.CreateElement("IMPORTE")
                oXmlEmt.AppendChild(oXMLDatoElemento)
                oXMLDatoTexto = oXmlDoc.CreateTextNode(FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat))
                oXMLDatoElemento.AppendChild(oXMLDatoTexto)
                oXMLDatoElemento = Nothing
                oXMLDatoTexto = Nothing
            End If
        End If

        'Si es una no conformidad muestra tb la fecha l�mite de resoluci�n,proveedor, contacto, unidad de negocio, coment apertura y coment cierre.
        If Request("TipoImpExp") = "3" Then
            oXMLDatoElemento = oXmlDoc.CreateElement("PROVEEDOR")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            oXMLDatoTexto = oXmlDoc.CreateTextNode(oNoConformidad.Proveedor & " - " & oNoConformidad.ProveDen)
            oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing

            oXMLDatoElemento = oXmlDoc.CreateElement("CONTACTO")
            oXmlEmt.AppendChild(oXMLDatoElemento)

            Dim dsNotificadosProveedor As DataSet = oNoConformidad.Load_Notificados_Proveedor()
            Dim ContactosInternos As String = ""
            For Each row In dsNotificadosProveedor.Tables(0).Rows
                If row("Seleccionado") <> 0 Then
                    ContactosInternos = IIf(ContactosInternos = "", row("NOMBRECONTACTO"), ContactosInternos & ", " & row("NOMBRECONTACTO"))
                End If
            Next
            oXMLDatoTexto = oXmlDoc.CreateTextNode(ContactosInternos)

            oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing

            'Si existe el revisor,mostramos el dato
            If FSNServer.TipoAcceso.gbFSQA_Revisor Then
                If (oNoConformidad.Revisor <> "") Or (Not (oNoConformidad.Revisor Is Nothing)) Then
                    Dim oRevisor As FSNServer.User
                    oRevisor = FSNServer.Get_Object(GetType(FSNServer.User))
                    oRevisor.LoadUserData(oNoConformidad.Revisor)
                    oXMLRevisor = oXmlDoc.CreateElement("REVISOR")
                    oXmlEmt.AppendChild(oXMLRevisor)
                    oXMLDatoElemento = oXmlDoc.CreateElement("NOM_REVISOR")
                    oXMLRevisor.AppendChild(oXMLDatoElemento)
                    oXMLDatoTexto = oXmlDoc.CreateTextNode(oRevisor.Nombre)
                    oXMLDatoElemento.AppendChild(oXMLDatoTexto)
                    oXMLDatoElemento = Nothing
                    oXMLDatoTexto = Nothing
                    oRevisor = Nothing
                    oXMLRevisor = Nothing
                End If
            End If

            oXMLDatoElemento = oXmlDoc.CreateElement("FEC_LIM")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            oXMLDatoTexto = oXmlDoc.CreateTextNode(FormatDateTime(oNoConformidad.FechaLimResol, DateFormat.ShortDate))
            oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing

            Dim oUnidadesQA As FSNServer.UnidadesNeg
            Dim dt As DataTable

            oUnidadesQA = FSNServer.Get_Object(GetType(FSNServer.UnidadesNeg))
            dt = oUnidadesQA.LoadUnQa(Idioma, oNoConformidad.UnidadQA)

            oXMLDatoElemento = oXmlDoc.CreateElement("UNQA")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            If dt.Rows.Count > 0 Then
                oXMLDatoTexto = oXmlDoc.CreateTextNode(DBNullToStr(dt.Rows(0)("UNIDAD")))
            Else
                oXMLDatoTexto = oXmlDoc.CreateTextNode("")
            End If
            oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing

            oXMLDatoElemento = oXmlDoc.CreateElement("COMMENT_ALTA")
            oXmlEmt.AppendChild(oXMLDatoElemento)
            If Request("Formato") = "XML" Then
                oXMLDatoTexto = oXmlDoc.CreateTextNode(oNoConformidad.ComentAlta)
                oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            Else
                oXMLDatoElemento.AppendChild(oXmlDoc.CreateCDataSection(oNoConformidad.ComentAltaBR))
            End If

            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing

            oXMLDatoElemento = oXmlDoc.CreateElement("FEC_CIERRE")
            oXmlEmt.AppendChild(oXMLDatoElemento)

            oXMLDatoTexto = oXmlDoc.CreateTextNode(IIf(oNoConformidad.FecCierre = Nothing, "", FormatDateTime(oNoConformidad.FecCierre, DateFormat.ShortDate)))
            oXMLDatoElemento.AppendChild(oXMLDatoTexto)

            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing

            oXMLDatoElemento = oXmlDoc.CreateElement("COMMENT_CIERRE")
            oXmlEmt.AppendChild(oXMLDatoElemento)

            If (UCase(FSNUser.Cod) = UCase(oNoConformidad.Revisor)) _
            OrElse (oNoConformidad.Estado = TipoEstadoNoConformidad.CierreNegativo) _
            OrElse (oNoConformidad.Estado = TipoEstadoNoConformidad.CierrePosEnPlazo) _
            OrElse (oNoConformidad.Estado = TipoEstadoNoConformidad.CierrePosFueraPlazo) _
            OrElse ((oNoConformidad.Estado = TipoEstadoNoConformidad.Abierta) And _
            ((oNoConformidad.AccionEstado = TipoAccionSolicitud.RechazoEficazRevisor) Or _
            (oNoConformidad.AccionEstado = TipoAccionSolicitud.RechazoNoEficazRevisor)) And _
            (UCase(FSNUser.Cod) = UCase(oNoConformidad.PeticionarioCod)) And _
            (UCase(oNoConformidad.Revisor) = UCase(oNoConformidad.AccionEstadoPersona))) Then
                oNoConformidad.CargarComentarioCierre()
            End If
            If Request("Formato") = "XML" Then
                oXMLDatoTexto = oXmlDoc.CreateTextNode(oNoConformidad.ComentCierre)
                oXMLDatoElemento.AppendChild(oXMLDatoTexto)
            Else
                oXMLDatoElemento.AppendChild(oXmlDoc.CreateCDataSection(oNoConformidad.ComentCierreBR))
            End If
            oXMLDatoElemento = Nothing
            oXMLDatoTexto = Nothing

            'Genera la tabla de acciones:
            If oNoConformidad.Acciones.Rows.Count > 0 Then
                oXMLAcciones = oXmlDoc.CreateElement("ACCIONES")
                oXmlEmt.AppendChild(oXMLAcciones)

                For Each oRow In oNoConformidad.Acciones.Rows
                    If oRow.Item("SOLICITAR") = 1 Then
                        oXMLAccion = oXmlDoc.CreateElement("ACCION")
                        oXMLAcciones.AppendChild(oXMLAccion)

                        oXMLDatoElemento = oXmlDoc.CreateElement("NOM_ACCION")
                        oXMLAccion.AppendChild(oXMLDatoElemento)
                        oXMLDatoTexto = oXmlDoc.CreateTextNode(oRow.Item("DEN_ACCION"))
                        oXMLDatoElemento.AppendChild(oXMLDatoTexto)
                        oXMLDatoElemento = Nothing
                        oXMLDatoTexto = Nothing

                        oXMLDatoElemento = oXmlDoc.CreateElement("FECLIM_ACCION")
                        oXMLAccion.AppendChild(oXMLDatoElemento)
                        If IsDBNull(oRow.Item("FECHA_LIMITE")) Then
                            oXMLDatoTexto = oXmlDoc.CreateTextNode(Textos(19))
                        Else
                            oXMLDatoTexto = oXmlDoc.CreateTextNode(Textos(19) & " " & FormatDateTime(oRow.Item("FECHA_LIMITE"), DateFormat.ShortDate))
                        End If
                        oXMLDatoElemento.AppendChild(oXMLDatoTexto)
                        oXMLDatoElemento = Nothing
                        oXMLDatoTexto = Nothing
                    End If
                Next
            End If
        End If

        ' Grupos
        If Not oInstancia.Grupos.Grupos Is Nothing Then
            oXMLGrupos = oXmlDoc.CreateElement("GRUPOS")
            oXmlEmt.AppendChild(oXMLGrupos)

            ' Por cada grupo vamos a recorrer sus campos
            For Each oGrupo In oInstancia.Grupos.Grupos
                ' Si todos los campos del grupo no van a ser visibles,
                ' no se mostrar� el grupo.
                bGrupoVisible = False
                For Each oFilaCampo In oGrupo.DSCampos.Tables(0).Rows
                    If oFilaCampo.Item("VISIBLE") = 1 Then
                        If Request("TipoImpExp") = "3" And DBNullToSomething(oFilaCampo.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Acciones Then
                            For Each oRow In oNoConformidad.Acciones.Rows
                                If oRow.Item("COPIA_CAMPO") = oFilaCampo.Item("ID_CAMPO") Then
                                    If oRow.Item("SOLICITAR") = 1 Then
                                        bGrupoVisible = True
                                    End If
                                    Exit For
                                End If
                            Next
                        Else
                            bGrupoVisible = True
                        End If

                        If bGrupoVisible = True Then Exit For
                    End If
                Next

                If bGrupoVisible = True Then
                    ' Primero la cabecera del grupo
                    oXMLGrupo = oXmlDoc.CreateElement("GRUPO")
                    oXMLGrupos.AppendChild(oXMLGrupo)

                    oXMLDatoElemento = oXmlDoc.CreateElement("NOMBRE")
                    oXMLGrupo.AppendChild(oXMLDatoElemento)
                    oXMLDatoTexto = oXmlDoc.CreateTextNode(DBNullToStr(oGrupo.Den(Idioma)))
                    oXMLDatoElemento.AppendChild(oXMLDatoTexto)
                    oXMLDatoElemento = Nothing
                    oXMLDatoTexto = Nothing

                    If oGrupo.NumCampos >= 1 Then
                        oXMLCampos = oXmlDoc.CreateElement("CAMPOS")
                        oXMLGrupo.AppendChild(oXMLCampos)

                        ' Recorremos sus campos
                        For Each oFilaCampo In oGrupo.DSCampos.Tables(0).Rows
                            ' Cabecera del campo
                            bCampoVisible = True
                            If oFilaCampo.Item("VISIBLE") = 1 Then
                                If Request("TipoImpExp") = "3" Then  'Si es una no conformidad y un campo de tipo acci�n comprueba que est� solicitado:
                                    If DBNullToSomething(oFilaCampo.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Acciones Then
                                        For Each oRow In oNoConformidad.Acciones.Rows
                                            If oRow.Item("COPIA_CAMPO") = oFilaCampo.Item("ID_CAMPO") Then
                                                If oRow.Item("SOLICITAR") = 0 Then
                                                    bCampoVisible = False
                                                End If
                                                Exit For
                                            End If
                                        Next
                                    End If
                                End If
                            Else
                                bCampoVisible = False
                            End If

                            If bCampoVisible = True Then
                                oXMLCampo = oXmlDoc.CreateElement("CAMPO")
                                oXMLCampos.AppendChild(oXMLCampo)

                                oXMLDatoElemento = oXmlDoc.CreateElement("NOMBRE")
                                oXMLCampo.AppendChild(oXMLDatoElemento)
                                oXMLDatoTexto = oXmlDoc.CreateTextNode(DBNullToStr(oFilaCampo.Item("DEN_" & Idioma)))
                                oXMLDatoElemento.AppendChild(oXMLDatoTexto)

                                oXMLDatoElemento = Nothing
                                oXMLDatoTexto = Nothing

                                ' Si no es desglose, saldr� directamente el valor
                                If oFilaCampo.Item("SUBTIPO") < 9 Then
                                    oXMLValorCampo = oXmlDoc.CreateElement("VALOR")
                                    oXMLCampo.AppendChild(oXMLValorCampo)
                                    oXMLDatoElemento = oXmlDoc.CreateElement("VALOR_DATO")
                                    oXMLValorCampo.AppendChild(oXMLDatoElemento)
                                End If

                                If oFilaCampo.Item("TIPO") = 6 Then
                                    Dim sCampoValor As String
                                    Select Case oFilaCampo.Item("SUBTIPO")
                                        Case 2
                                            sCampoValor = "VALOR_NUM"
                                        Case 3
                                            sCampoValor = "VALOR_FEC"
                                        Case 4
                                            sCampoValor = "VALOR_BOOL"
                                        Case Else
                                            sCampoValor = "VALOR_TEXT"
                                    End Select
                                    Dim oTablaExterna As FSNServer.TablaExterna = FSNServer.Get_Object(GetType(FSNServer.TablaExterna))
                                    oTablaExterna.LoadDefTabla(oFilaCampo.Item("TABLA_EXTERNA"), False)
                                    oTablaExterna.LoadData(False)
                                    If Not IsDBNull(oFilaCampo.Item(sCampoValor)) Then
                                        sDato = oTablaExterna.DescripcionReg(oFilaCampo.Item(sCampoValor), Idioma, FSNUser.DateFormat, FSNUser.NumberFormat)
                                    Else
                                        sDato = ""
                                    End If
                                    oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
                                Else
                                    Select Case oFilaCampo.Item("SUBTIPO")
                                        Case 0  ' Sin tipo
                                            If Not (TypeOf (oFilaCampo.Item("VALOR_TEXT")) Is DBNull) Then
                                                sDato = oFilaCampo.Item("VALOR_TEXT")
                                            Else
                                                sDato = ""
                                            End If
                                            If Not TypeOf (oFilaCampo.Item("TIPO_CAMPO_GS")) Is DBNull Then
                                                If oFilaCampo.Item("TIPO_CAMPO_GS") >= TiposDeDatos.TipoCampoGS.Proveedor AndAlso oFilaCampo.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.DenArticulo AndAlso oFilaCampo.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo AndAlso oFilaCampo.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.CodArticulo Then
                                                    If oFilaCampo.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Partida OrElse oFilaCampo.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.CentroCoste OrElse oFilaCampo.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Activo OrElse oFilaCampo.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.TipoPedido Then
                                                        If sDato <> "" Then
                                                            If InStrRev(sDato, "#") > 0 Then
                                                                sDato = Right(sDato, Len(sDato) - InStrRev(sDato, "#"))
                                                            End If
                                                            If InStrRev(sDato, "|") > 0 Then 'si no est� en el nivel 1, si no m�s abajo
                                                                sDato = Right(sDato, Len(sDato) - InStrRev(sDato, "|"))
                                                            End If
                                                            sDenDato = sDato & " - " & Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oFilaCampo.Item("VALOR_TEXT_DEN"), Idioma)
                                                        Else
                                                            sDenDato = ""
                                                        End If
                                                    Else
                                                        sDenDato = DenGS(sDato, oFilaCampo.Item("TIPO_CAMPO_GS"), Idioma)

                                                        oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
                                                        oXMLDatoElemento.AppendChild(oXMLDatoTexto)
                                                        oXMLDatoElemento = Nothing
                                                        oXMLDatoTexto = Nothing
                                                    End If

                                                    oXMLDatoElemento = oXmlDoc.CreateElement("VALOR_DEN")
                                                    oXMLValorCampo.AppendChild(oXMLDatoElemento)
                                                    oXMLDatoTexto = oXmlDoc.CreateTextNode(sDenDato)
                                                Else
                                                    oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
                                                End If
                                            Else
                                                oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
                                            End If
                                        Case 1, 5, 6, 7  ' Textos
                                            If Not (TypeOf (oFilaCampo.Item("VALOR_TEXT")) Is DBNull) Then
                                                sDato = oFilaCampo.Item("VALOR_TEXT")
                                            Else
                                                sDato = ""
                                            End If

                                            If Not TypeOf (oFilaCampo.Item("TIPO_CAMPO_GS")) Is DBNull Then

                                                If oFilaCampo.Item("TIPO_CAMPO_GS") >= TiposDeDatos.TipoCampoGS.Proveedor AndAlso oFilaCampo.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.DenArticulo AndAlso oFilaCampo.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo AndAlso oFilaCampo.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.CodArticulo Then
                                                    If oFilaCampo.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Partida OrElse oFilaCampo.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.CentroCoste OrElse oFilaCampo.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Activo OrElse oFilaCampo.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.TipoPedido Then
                                                        If sDato <> "" Then
                                                            If InStrRev(sDato, "#") > 0 Then
                                                                sDato = Right(sDato, Len(sDato) - InStrRev(sDato, "#"))
                                                            End If
                                                            If InStrRev(sDato, "|") > 0 Then 'si no est� en el nivel 1, si no m�s abajo
                                                                sDato = Right(sDato, Len(sDato) - InStrRev(sDato, "|"))
                                                            End If
                                                            sDenDato = sDato & " - " & Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oFilaCampo.Item("VALOR_TEXT_DEN"), Idioma)
                                                        Else
                                                            sDenDato = ""
                                                        End If
                                                    Else
                                                        sDenDato = DenGS(sDato, oFilaCampo.Item("TIPO_CAMPO_GS"), Idioma)

                                                        oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
                                                        oXMLDatoElemento.AppendChild(oXMLDatoTexto)
                                                        oXMLDatoElemento = Nothing
                                                        oXMLDatoTexto = Nothing
                                                    End If

                                                    oXMLDatoElemento = oXmlDoc.CreateElement("VALOR_DEN")
                                                    oXMLValorCampo.AppendChild(oXMLDatoElemento)
                                                    oXMLDatoTexto = oXmlDoc.CreateTextNode(sDenDato)
                                                Else
                                                    oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
                                                End If
                                            Else
                                                oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
                                            End If
                                        Case 8 ' Archivos
                                            sAdjun = ""

                                            For Each oAdjunto In oFilaCampo.GetChildRows("REL_CAMPO_ADJUN")
                                                sAdjun += oAdjunto.Item("NOM") + " (" + FSNLibrary.FormatNumber((oAdjunto.Item("DATASIZE") / 1024), FSNUser.NumberFormat) + " Kb.), "
                                            Next
                                            If sAdjun <> "" Then
                                                sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
                                            End If
                                            If Len(sAdjun) > 90 Then
                                                sDato = Mid(sAdjun, 1, 90) & "..."
                                            Else
                                                sDato = sAdjun
                                            End If
                                            oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)

                                        Case 2  ' Num�ricos
                                            If Not (TypeOf (oFilaCampo.Item("VALOR_NUM")) Is DBNull) Then
                                                sDato = oFilaCampo.Item("VALOR_NUM")
                                            Else
                                                If (CInt(DBNullToSomething(oFilaCampo.Item("TIPO_CAMPO_GS"))) >= 110 And CInt(DBNullToSomething(oFilaCampo.Item("TIPO_CAMPO_GS"))) <= 113) Then
                                                    sDato = DBNullToStr(oFilaCampo.Item("VALOR_TEXT"))
                                                Else
                                                    sDato = ""
                                                End If

                                            End If
                                            If Not TypeOf (oFilaCampo.Item("TIPO_CAMPO_GS")) Is DBNull Then

                                                If oFilaCampo.Item("TIPO_CAMPO_GS") >= TiposDeDatos.TipoCampoGS.Proveedor AndAlso oFilaCampo.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.DenArticulo AndAlso oFilaCampo.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo AndAlso oFilaCampo.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.CodArticulo _
                                                    AndAlso oFilaCampo.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.ImporteRepercutido Then
                                                    If oFilaCampo.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Partida OrElse oFilaCampo.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.CentroCoste OrElse oFilaCampo.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Activo OrElse oFilaCampo.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.TipoPedido Then
                                                        If sDato <> "" Then
                                                            If InStrRev(sDato, "#") > 0 Then
                                                                sDato = Right(sDato, Len(sDato) - InStrRev(sDato, "#"))
                                                            End If
                                                            If InStrRev(sDato, "|") > 0 Then 'si no est� en el nivel 1, si no m�s abajo
                                                                sDato = Right(sDato, Len(sDato) - InStrRev(sDato, "|"))
                                                            End If
                                                            sDenDato = sDato & " - " & Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oFilaCampo.Item("VALOR_TEXT_DEN"), Idioma)
                                                        Else
                                                            sDenDato = ""
                                                        End If
                                                    Else
                                                        sDenDato = DenGS(sDato, oFilaCampo.Item("TIPO_CAMPO_GS"), Idioma)

                                                        oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
                                                        oXMLDatoElemento.AppendChild(oXMLDatoTexto)
                                                        oXMLDatoElemento = Nothing
                                                        oXMLDatoTexto = Nothing
                                                    End If

                                                    oXMLDatoElemento = oXmlDoc.CreateElement("VALOR_DEN")
                                                    oXMLValorCampo.AppendChild(oXMLDatoElemento)
                                                    oXMLDatoTexto = oXmlDoc.CreateTextNode(sDenDato)
                                                ElseIf (CInt(oFilaCampo.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.ImporteRepercutido) Then
                                                    sDenDato = DBNullToStr(oFilaCampo.Item("VALOR_TEXT"))

                                                    If sDato = "" Then
                                                        sDenDato = ""
                                                        oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
                                                    Else
                                                        oXMLDatoTexto = oXmlDoc.CreateTextNode(FSNLibrary.FormatNumber(sDato, FSNUser.NumberFormat))
                                                    End If

                                                    oXMLDatoElemento.AppendChild(oXMLDatoTexto)
                                                    oXMLDatoElemento = Nothing
                                                    oXMLDatoTexto = Nothing
                                                    oXMLDatoElemento = oXmlDoc.CreateElement("VALOR_DEN")
                                                    oXMLValorCampo.AppendChild(oXMLDatoElemento)

                                                    oXMLDatoTexto = oXmlDoc.CreateTextNode(sDenDato)
                                                Else
                                                    If sDato <> "" Then
                                                        oXMLDatoTexto = oXmlDoc.CreateTextNode(FSNLibrary.FormatNumber(sDato, FSNUser.NumberFormat))
                                                    Else
                                                        oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
                                                    End If
                                                End If
                                            Else
                                                If sDato <> "" Then
                                                    oXMLDatoTexto = oXmlDoc.CreateTextNode(FSNLibrary.FormatNumber(sDato, FSNUser.NumberFormat))
                                                Else
                                                    oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
                                                End If
                                            End If
                                        Case 3  ' Fechas
                                            If Not (TypeOf (oFilaCampo.Item("VALOR_FEC")) Is DBNull) Then
                                                oXMLDatoTexto = oXmlDoc.CreateTextNode(FormatDate(oFilaCampo.Item("VALOR_FEC"), FSNUser.DateFormat))
                                            Else
                                                oXMLDatoTexto = oXmlDoc.CreateTextNode("")
                                            End If
                                        Case 4  ' Booleanos
                                            If Not (TypeOf (oFilaCampo.Item("VALOR_BOOL")) Is DBNull) Then
                                                If oFilaCampo.Item("VALOR_BOOL") = 1 Then
                                                    oXMLDatoTexto = oXmlDoc.CreateTextNode(Textos(20))
                                                Else
                                                    oXMLDatoTexto = oXmlDoc.CreateTextNode(Textos(21))
                                                End If
                                            Else
                                                oXMLDatoTexto = oXmlDoc.CreateTextNode("")
                                            End If
                                        Case 9  ' Desgloses
                                            oXMLDesglose = oXmlDoc.CreateElement("DESGLOSE")
                                            oXMLCampo.AppendChild(oXMLDesglose)

                                            oCampo = FSNServer.Get_Object(GetType(FSNServer.Campo))
                                            oCampo.Id = oFilaCampo.Item("ID")

                                            ' Cargamos DataSet con cabecera y l�neas de desglose
                                            Dim bNuevoWorkflow As Boolean
                                            If Request("TipoImpExp") = "1" Then
                                                bNuevoWorkflow = True
                                            Else
                                                bNuevoWorkflow = False
                                            End If

                                            Dim bSacarNumLinea As Boolean = ((Request("TipoImpExp") = "1") _
                                                AndAlso (oInstancia.Solicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras _
                                                OrElse oInstancia.Solicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.PedidoExpress _
                                                OrElse oInstancia.Solicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.PedidoNegociado))
                                            oDesglose = oCampo.LoadInstDesglose(Idioma, oInstancia.ID, FSNUser.CodPersona, oInstancia.Version, , bNuevoWorkflow, bObservador, , , FSNUser.DateFmt, , , bSacarNumLinea)

                                            ' En la tabla 0 est�n las cabeceras
                                            ' Pasamos los datos a un array para tenerlos en el procesado
                                            ' de las filas
                                            If Request("TipoImpExp") = "3" Then
                                                If Not oNoConformidad.Data Is Nothing Then
                                                    'Carga las cabeceras de estado y estado interno
                                                    Dim oNewRow As DataRow
                                                    Dim oDTEstados As DataTable = oDesglose.Tables(0)
                                                    oNewRow = oDTEstados.NewRow
                                                    oNewRow.Item("ID") = TiposDeDatos.IdsFicticios.EstadoInterno
                                                    oNewRow.Item("GRUPO") = oGrupo.Id
                                                    oNewRow.Item("DEN_" & Idioma) = Textos(12) 'Estado interno
                                                    oNewRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio
                                                    oNewRow.Item("INTRO") = 0
                                                    oNewRow.Item("VISIBLE") = 1
                                                    oNewRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.EstadoInternoNoConf
                                                    oDTEstados.Rows.Add(oNewRow)

                                                    oNewRow = oDTEstados.NewRow
                                                    oNewRow.Item("ID") = TiposDeDatos.IdsFicticios.Comentario
                                                    oNewRow.Item("GRUPO") = oGrupo.Id
                                                    oNewRow.Item("DEN_" & Idioma) = Textos(22) 'Comentario
                                                    oNewRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo
                                                    oNewRow.Item("INTRO") = 0
                                                    oNewRow.Item("VISIBLE") = 1
                                                    oDTEstados.Rows.Add(oNewRow)

                                                    'Valores de estado actual y estado interno:
                                                    Dim oDTLineasDesglose As DataTable = oDesglose.Tables(2)
                                                    For Each oRow In oNoConformidad.EstadosComentariosInternosAcciones.Rows
                                                        If oRow.Item("CAMPO_PADRE") = oCampo.IdCopiaCampo Then
                                                            If oDSLista.Tables(0).Rows.Count = 0 Then
                                                            Else
                                                                oNewRow = oDTLineasDesglose.NewRow
                                                                oNewRow.Item("CAMPO_PADRE") = oRow.Item("CAMPO_PADRE")
                                                                oNewRow.Item("LINEA") = oRow.Item("LINEA")
                                                                oNewRow.Item("CAMPO_HIJO") = TiposDeDatos.IdsFicticios.EstadoActual
                                                                oNewRow.Item("VALOR_TEXT") = oRow.Item("ESTADO")
                                                                For iFila = 0 To oDSLista.Tables(0).Rows.Count - 1
                                                                    If oDSLista.Tables(0).Rows(iFila).Item("COD") = DBNullToSomething(oRow.Item("ESTADO")) Then
                                                                        oNewRow.Item("VALOR_TEXT") = oNewRow.Item("VALOR_TEXT") & "-" & oDSLista.Tables(0).Rows(iFila).Item("DEN_" & Idioma)
                                                                        Exit For
                                                                    End If
                                                                Next
                                                                oDTLineasDesglose.Rows.Add(oNewRow)
                                                            End If

                                                            oNewRow = oDTLineasDesglose.NewRow
                                                            oNewRow.Item("CAMPO_PADRE") = oRow.Item("CAMPO_PADRE")
                                                            oNewRow.Item("LINEA") = oRow.Item("LINEA")
                                                            oNewRow.Item("CAMPO_HIJO") = TiposDeDatos.IdsFicticios.EstadoInterno
                                                            oNewRow.Item("VALOR_NUM") = oRow.Item("ESTADO_INT")

                                                            Select Case oRow.Item("ESTADO_INT")
                                                                Case 0, TipoEstadoAcciones.SinRevisar
                                                                    oNewRow.Item("VALOR_TEXT") = Textos(13)
                                                                Case TipoEstadoAcciones.Aprobada
                                                                    oNewRow.Item("VALOR_TEXT") = Textos(14)
                                                                Case TipoEstadoAcciones.Rechazada
                                                                    oNewRow.Item("VALOR_TEXT") = Textos(15)
                                                                Case TipoEstadoAcciones.FinalizadaSinRevisar
                                                                    oNewRow.Item("VALOR_TEXT") = Textos(16)
                                                                Case TipoEstadoAcciones.FinalizadaRevisada
                                                                    oNewRow.Item("VALOR_TEXT") = Textos(17)
                                                            End Select
                                                            oDTLineasDesglose.Rows.Add(oNewRow)

                                                            oNewRow = oDTLineasDesglose.NewRow
                                                            oNewRow.Item("CAMPO_PADRE") = oRow.Item("CAMPO_PADRE")
                                                            oNewRow.Item("LINEA") = oRow.Item("LINEA")
                                                            oNewRow.Item("CAMPO_HIJO") = TiposDeDatos.IdsFicticios.Comentario
                                                            oNewRow.Item("VALOR_TEXT") = oRow.Item("COMENT")

                                                            oDTLineasDesglose.Rows.Add(oNewRow)
                                                        End If
                                                    Next
                                                End If
                                            End If

                                            iNumColumnas = oDesglose.Tables(0).Rows.Count

                                            ReDim iCampoColumna(iNumColumnas)
                                            ReDim iTipoColumna(iNumColumnas)
                                            ReDim iTipoGSColumna(iNumColumnas)
                                            ReDim sDenColumna(iNumColumnas)
                                            ReDim bVisibleColumna(iNumColumnas)
                                            ReDim bIntroColumna(iNumColumnas)
                                            ReDim lTablaExterna(iNumColumnas)

                                            iColumna = 0

                                            For Each oFilaDesglose In oDesglose.Tables(0).Rows

                                                iCampoColumna(iColumna) = oFilaDesglose.Item("ID")

                                                iTipoColumna(iColumna) = oFilaDesglose.Item("SUBTIPO")

                                                If Not TypeOf (oFilaDesglose.Item("TIPO_CAMPO_GS")) Is DBNull Then
                                                    iTipoGSColumna(iColumna) = oFilaDesglose.Item("TIPO_CAMPO_GS")
                                                Else
                                                    iTipoGSColumna(iColumna) = 0
                                                End If

                                                sDenColumna(iColumna) = DBNullToStr(oFilaDesglose.Item("DEN_" & Idioma))

                                                If iTipoGSColumna(iColumna) = TiposDeDatos.IdsFicticios.NumLinea Then
                                                    sDenColumna(iColumna) = Textos(23)
                                                End If

                                                If oFilaDesglose.Item("VISIBLE") = 1 Then
                                                    bVisibleColumna(iColumna) = True
                                                Else
                                                    bVisibleColumna(iColumna) = False
                                                End If

                                                If oFilaDesglose.Item("INTRO") = 1 Then
                                                    bIntroColumna(iColumna) = True
                                                Else
                                                    bIntroColumna(iColumna) = False
                                                End If

                                                lTablaExterna(iColumna) = CType(DBNullToDec(oFilaDesglose.Item("TABLA_EXTERNA")), Long)

                                                iColumna = iColumna + 1

                                            Next
                                            Dim distinctCounts As IEnumerable(Of Int32)
                                            Dim lineasReal As Integer()
                                            Dim iMaxIndex As Integer = 0
                                            ' Preparando el procesado de las filas del desglose
                                            'Problema en gestamp cuando habia desglose con 0 lineas cascaba
                                            If oDesglose.Tables(0).Rows.Count = 0 Then
                                                'le pongo valor 1 para que me genere la cabecera del desglose a pesar de que no tenga lineas
                                                iNumFilas = 1
                                                iMaxIndex = 1
                                            Else
                                                If Not (TypeOf (oDesglose.Tables(0).Rows(0).Item("LINEA")) Is System.DBNull) Then
                                                    iMaxIndex = oDesglose.Tables("LINEAS").Select("", "LINEA DESC")(0).Item("LINEA")
                                                    distinctCounts = From row In oDesglose.Tables("LINEAS")
                                                                     Select row.Field(Of Int32)("LINEA")
                                                                     Distinct
                                                    iNumFilas = distinctCounts.Count
                                                    ReDim lineasReal(iNumFilas - 1)
                                                    For contador As Integer = 0 To iNumFilas - 1
                                                        lineasReal(contador) = distinctCounts(contador)
                                                    Next
                                                End If
                                            End If

                                            Dim KeyFields(2) As DataColumn

                                            KeyFields(0) = oDesglose.Tables(2).Columns("LINEA")
                                            KeyFields(1) = oDesglose.Tables(2).Columns("CAMPO_PADRE")
                                            KeyFields(2) = oDesglose.Tables(2).Columns("CAMPO_HIJO")

                                            oDesglose.Tables(2).PrimaryKey = KeyFields

                                            ' Por cada fila, buscamos el valor de aquellas columnas visibles
                                            ' Si hay valor lo ponemos, si no ponemos solo la cabecera y un valor vac�o
                                            For iFila = 1 To iMaxIndex
                                                If iNumFilas = 1 OrElse lineasReal.Contains(iFila) Then
                                                    oXMLFilaDesglose = oXmlDoc.CreateElement("FILA_DESGLOSE")
                                                    oXMLDesglose.AppendChild(oXMLFilaDesglose)

                                                    For iColumna = 0 To iNumColumnas - 1
                                                        If bVisibleColumna(iColumna) = True Then
                                                            ' Cabecera
                                                            oXMLColumnaDesglose = oXmlDoc.CreateElement("COLUMNA_DESGLOSE")
                                                            oXMLFilaDesglose.AppendChild(oXMLColumnaDesglose)

                                                            oXMLDatoElementoDesglose = oXmlDoc.CreateElement("NOMBRE")
                                                            oXMLColumnaDesglose.AppendChild(oXMLDatoElementoDesglose)
                                                            oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDenColumna(iColumna))
                                                            oXMLDatoElementoDesglose.AppendChild(oXMLDatoTextoDesglose)
                                                            oXMLDatoElementoDesglose = Nothing
                                                            oXMLDatoTextoDesglose = Nothing

                                                            oXMLValorColumnaDesglose = oXmlDoc.CreateElement("VALOR")
                                                            oXMLColumnaDesglose.AppendChild(oXMLValorColumnaDesglose)
                                                            oXMLDatoElementoDesglose = oXmlDoc.CreateElement("VALOR_DATO")
                                                            oXMLValorColumnaDesglose.AppendChild(oXMLDatoElementoDesglose)

                                                            ' Datos ...
                                                            Dim SearchFields(2)
                                                            SearchFields(0) = iFila
                                                            If oCampo.IdCopiaCampo = Nothing Then
                                                                SearchFields(1) = oCampo.Id
                                                            Else
                                                                SearchFields(1) = oCampo.IdCopiaCampo
                                                            End If

                                                            If iTipoGSColumna(iColumna) = TiposDeDatos.TipoCampoGS.EstadoNoConf Then
                                                                SearchFields(2) = TiposDeDatos.IdsFicticios.EstadoActual
                                                            Else
                                                                SearchFields(2) = iCampoColumna(iColumna)
                                                            End If

                                                            oFilaDesglose = oDesglose.Tables(2).Rows.Find(SearchFields)

                                                            If oFilaDesglose Is Nothing Then
                                                                If Not oNoConformidad Is Nothing Then
                                                                    If oNoConformidad.Data.Tables("NOCONF_ACC").Rows.Count = 0 _
                                                                        AndAlso iCampoColumna(iColumna) = TiposDeDatos.IdsFicticios.EstadoInterno Then
                                                                        oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(Textos(13))
                                                                    Else
                                                                        sDato = ""
                                                                        oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
                                                                    End If
                                                                ElseIf iTipoGSColumna(iColumna) = TiposDeDatos.IdsFicticios.NumLinea Then
                                                                    sDato = iFila
                                                                    oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
                                                                Else
                                                                    sDato = ""
                                                                    oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
                                                                End If

                                                                oXMLDatoElementoDesglose.AppendChild(oXMLDatoTextoDesglose)
                                                            Else
                                                                If lTablaExterna(iColumna) = 0 Then
                                                                    Select Case iTipoColumna(iColumna)
                                                                        Case 0  ' Sin tipo
                                                                            If Not (TypeOf (oFilaDesglose.Item("VALOR_TEXT")) Is DBNull) Then
                                                                                sDato = oFilaDesglose.Item("VALOR_TEXT")
                                                                            Else
                                                                                sDato = ""
                                                                            End If

                                                                            If iTipoGSColumna(iColumna) <> 0 Then
                                                                                If iTipoGSColumna(iColumna) >= TiposDeDatos.TipoCampoGS.Proveedor AndAlso iTipoGSColumna(iColumna) <> TiposDeDatos.TipoCampoGS.DenArticulo AndAlso iTipoGSColumna(iColumna) <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo AndAlso iTipoGSColumna(iColumna) <> TiposDeDatos.TipoCampoGS.CodArticulo Then
                                                                                    If iTipoGSColumna(iColumna) = TiposDeDatos.TipoCampoGS.Partida OrElse iTipoGSColumna(iColumna) = TiposDeDatos.TipoCampoGS.CentroCoste OrElse iTipoGSColumna(iColumna) = TiposDeDatos.TipoCampoGS.Activo OrElse iTipoGSColumna(iColumna) = TiposDeDatos.TipoCampoGS.TipoPedido Then
                                                                                        If sDato <> "" Then
                                                                                            If InStrRev(sDato, "#") > 0 Then
                                                                                                sDato = Right(sDato, Len(sDato) - InStrRev(sDato, "#"))
                                                                                            End If
                                                                                            If InStrRev(sDato, "|") > 0 Then 'si no est� en el nivel 1, si no m�s abajo
                                                                                                sDato = Right(sDato, Len(sDato) - InStrRev(sDato, "|"))
                                                                                            End If
                                                                                            sDenDato = sDato & " - " & Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oFilaDesglose.Item("VALOR_TEXT_DEN"), Idioma)
                                                                                        Else
                                                                                            sDenDato = ""
                                                                                        End If
                                                                                    Else
                                                                                        sDenDato = DenGS(sDato, iTipoGSColumna(iColumna), Idioma)

                                                                                        oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
                                                                                        oXMLDatoElementoDesglose.AppendChild(oXMLDatoTextoDesglose)
                                                                                        oXMLDatoElementoDesglose = Nothing
                                                                                        oXMLDatoTextoDesglose = Nothing
                                                                                    End If

                                                                                    oXMLDatoElementoDesglose = oXmlDoc.CreateElement("VALOR_DEN")
                                                                                    oXMLValorColumnaDesglose.AppendChild(oXMLDatoElementoDesglose)
                                                                                    oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDenDato)
                                                                                Else
                                                                                    oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
                                                                                End If
                                                                            Else
                                                                                oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
                                                                            End If
                                                                        Case 1, 5, 6, 7, 8  ' Textos (Incluye adjuntos porque el nombre de los archivos est� en el texto)
                                                                            If bIntroColumna(iColumna) = False Then
                                                                                If Not (TypeOf (oFilaDesglose.Item("VALOR_TEXT")) Is DBNull) Then
                                                                                    sDato = oFilaDesglose.Item("VALOR_TEXT")
                                                                                Else
                                                                                    sDato = ""
                                                                                End If
                                                                                If iTipoGSColumna(iColumna) <> 0 Then

                                                                                    If iTipoGSColumna(iColumna) >= TiposDeDatos.TipoCampoGS.Proveedor AndAlso iTipoGSColumna(iColumna) <> TiposDeDatos.TipoCampoGS.DenArticulo AndAlso iTipoGSColumna(iColumna) <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo AndAlso iTipoGSColumna(iColumna) <> TiposDeDatos.TipoCampoGS.CodArticulo Then
                                                                                        If iTipoGSColumna(iColumna) = TiposDeDatos.TipoCampoGS.Partida OrElse iTipoGSColumna(iColumna) = TiposDeDatos.TipoCampoGS.CentroCoste OrElse iTipoGSColumna(iColumna) = TiposDeDatos.TipoCampoGS.Activo OrElse iTipoGSColumna(iColumna) = TiposDeDatos.TipoCampoGS.TipoPedido Then
                                                                                            If sDato <> "" Then
                                                                                                If InStrRev(sDato, "#") > 0 Then
                                                                                                    sDato = Right(sDato, Len(sDato) - InStrRev(sDato, "#"))
                                                                                                End If
                                                                                                If InStrRev(sDato, "|") > 0 Then 'si no est� en el nivel 1, si no m�s abajo
                                                                                                    sDato = Right(sDato, Len(sDato) - InStrRev(sDato, "|"))
                                                                                                End If
                                                                                                sDenDato = sDato & " - " & Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oFilaDesglose.Item("VALOR_TEXT_DEN"), Idioma)
                                                                                            Else
                                                                                                sDenDato = ""
                                                                                            End If
                                                                                        Else
                                                                                            sDenDato = DenGS(sDato, iTipoGSColumna(iColumna), Idioma)

                                                                                            oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
                                                                                            oXMLDatoElementoDesglose.AppendChild(oXMLDatoTextoDesglose)
                                                                                            oXMLDatoElementoDesglose = Nothing
                                                                                            oXMLDatoTextoDesglose = Nothing
                                                                                        End If

                                                                                        oXMLDatoElementoDesglose = oXmlDoc.CreateElement("VALOR_DEN")
                                                                                        oXMLValorColumnaDesglose.AppendChild(oXMLDatoElementoDesglose)
                                                                                        oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDenDato)
                                                                                    Else
                                                                                        oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
                                                                                    End If
                                                                                Else
                                                                                    oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)

                                                                                End If
                                                                            Else
                                                                                Dim oDrValorLista As DataRow

                                                                                Dim SearchValorLista(1)

                                                                                If Not (TypeOf (oFilaDesglose.Item("VALOR_NUM")) Is DBNull) Then
                                                                                    SearchValorLista(0) = oFilaDesglose.Item("CAMPO_HIJO")
                                                                                    SearchValorLista(1) = CInt(oFilaDesglose.Item("VALOR_NUM"))

                                                                                    oDrValorLista = oDesglose.Tables(1).Rows.Find(SearchValorLista)

                                                                                    If oDrValorLista Is Nothing Then
                                                                                        sDato = ""
                                                                                    Else
                                                                                        sDato = oDrValorLista.Item("VALOR_TEXT_" & Idioma)
                                                                                    End If
                                                                                Else
                                                                                    sDato = ""
                                                                                End If

                                                                                If iTipoGSColumna(iColumna) <> 0 Then
                                                                                    If iTipoGSColumna(iColumna) >= TiposDeDatos.TipoCampoGS.Proveedor AndAlso iTipoGSColumna(iColumna) <> TiposDeDatos.TipoCampoGS.DenArticulo AndAlso iTipoGSColumna(iColumna) <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo AndAlso iTipoGSColumna(iColumna) <> TiposDeDatos.TipoCampoGS.CodArticulo Then
                                                                                        If iTipoGSColumna(iColumna) = TiposDeDatos.TipoCampoGS.Partida OrElse iTipoGSColumna(iColumna) = TiposDeDatos.TipoCampoGS.CentroCoste OrElse iTipoGSColumna(iColumna) = TiposDeDatos.TipoCampoGS.Activo OrElse iTipoGSColumna(iColumna) = TiposDeDatos.TipoCampoGS.TipoPedido Then
                                                                                            If sDato <> "" Then
                                                                                                If InStrRev(sDato, "#") > 0 Then
                                                                                                    sDato = Right(sDato, Len(sDato) - InStrRev(sDato, "#"))
                                                                                                End If
                                                                                                If InStrRev(sDato, "|") > 0 Then 'si no est� en el nivel 1, si no m�s abajo
                                                                                                    sDato = Right(sDato, Len(sDato) - InStrRev(sDato, "|"))
                                                                                                End If
                                                                                                sDenDato = sDato & " - " & Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oFilaDesglose.Item("VALOR_TEXT_DEN"), Idioma)
                                                                                            Else
                                                                                                sDenDato = ""
                                                                                            End If
                                                                                        Else
                                                                                            sDenDato = DenGS(sDato, iTipoGSColumna(iColumna), Idioma)

                                                                                            oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
                                                                                            oXMLDatoElementoDesglose.AppendChild(oXMLDatoTextoDesglose)
                                                                                            oXMLDatoElementoDesglose = Nothing
                                                                                            oXMLDatoTextoDesglose = Nothing
                                                                                        End If

                                                                                        oXMLDatoElementoDesglose = oXmlDoc.CreateElement("VALOR_DEN")
                                                                                        oXMLValorColumnaDesglose.AppendChild(oXMLDatoElementoDesglose)
                                                                                        oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDenDato)
                                                                                    Else
                                                                                        oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
                                                                                    End If
                                                                                Else
                                                                                    oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
                                                                                End If
                                                                            End If
                                                                        Case 2  ' Num�ricos
                                                                            If Not (TypeOf (oFilaDesglose.Item("VALOR_NUM")) Is DBNull) Then
                                                                                sDato = oFilaDesglose.Item("VALOR_NUM")
                                                                            Else
                                                                                If (iTipoGSColumna(iColumna) >= 110 And iTipoGSColumna(iColumna) <= 113) Then
                                                                                    sDato = DBNullToStr(oFilaDesglose.Item("VALOR_TEXT"))
                                                                                Else
                                                                                    sDato = ""
                                                                                End If
                                                                            End If

                                                                            If iTipoGSColumna(iColumna) <> 0 Then
                                                                                If iTipoGSColumna(iColumna) >= TiposDeDatos.TipoCampoGS.Proveedor AndAlso iTipoGSColumna(iColumna) <> TiposDeDatos.TipoCampoGS.DenArticulo AndAlso iTipoGSColumna(iColumna) <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo AndAlso iTipoGSColumna(iColumna) <> TiposDeDatos.TipoCampoGS.CodArticulo _
                                                                                     AndAlso iTipoGSColumna(iColumna) <> TiposDeDatos.TipoCampoGS.ImporteRepercutido Then
                                                                                    If iTipoGSColumna(iColumna) = TiposDeDatos.TipoCampoGS.Partida OrElse iTipoGSColumna(iColumna) = TiposDeDatos.TipoCampoGS.CentroCoste OrElse iTipoGSColumna(iColumna) = TiposDeDatos.TipoCampoGS.Activo OrElse iTipoGSColumna(iColumna) = TiposDeDatos.TipoCampoGS.TipoPedido Then
                                                                                        If sDato <> "" Then
                                                                                            If InStrRev(sDato, "#") > 0 Then
                                                                                                sDato = Right(sDato, Len(sDato) - InStrRev(sDato, "#"))
                                                                                            End If
                                                                                            If InStrRev(sDato, "|") > 0 Then 'si no est� en el nivel 1, si no m�s abajo
                                                                                                sDato = Right(sDato, Len(sDato) - InStrRev(sDato, "|"))
                                                                                            End If
                                                                                            sDenDato = sDato & " - " & Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oFilaDesglose.Item("VALOR_TEXT_DEN"), Idioma)
                                                                                        Else
                                                                                            sDenDato = ""
                                                                                        End If
                                                                                    Else
                                                                                        sDenDato = DenGS(sDato, iTipoGSColumna(iColumna), Idioma)

                                                                                        oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
                                                                                        oXMLDatoElementoDesglose.AppendChild(oXMLDatoTextoDesglose)
                                                                                        oXMLDatoElementoDesglose = Nothing
                                                                                        oXMLDatoTextoDesglose = Nothing
                                                                                    End If

                                                                                    oXMLDatoElementoDesglose = oXmlDoc.CreateElement("VALOR_DEN")
                                                                                    oXMLValorColumnaDesglose.AppendChild(oXMLDatoElementoDesglose)
                                                                                    oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDenDato)
                                                                                ElseIf iTipoGSColumna(iColumna) = TiposDeDatos.TipoCampoGS.ImporteRepercutido Then
                                                                                    sDenDato = DBNullToStr(oFilaDesglose.Item("VALOR_TEXT"))

                                                                                    If sDato = "" Then
                                                                                        sDenDato = ""
                                                                                        oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
                                                                                    Else
                                                                                        oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(FSNLibrary.FormatNumber(sDato, FSNUser.NumberFormat))
                                                                                    End If

                                                                                    oXMLDatoElementoDesglose.AppendChild(oXMLDatoTextoDesglose)
                                                                                    oXMLDatoElementoDesglose = Nothing
                                                                                    oXMLDatoTextoDesglose = Nothing
                                                                                    oXMLDatoElementoDesglose = oXmlDoc.CreateElement("VALOR_DEN")
                                                                                    oXMLValorColumnaDesglose.AppendChild(oXMLDatoElementoDesglose)

                                                                                    oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDenDato)
                                                                                Else
                                                                                    If sDato <> "" Then
                                                                                        oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(FSNLibrary.FormatNumber(sDato, FSNUser.NumberFormat))
                                                                                    Else
                                                                                        oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
                                                                                    End If
                                                                                End If
                                                                            Else
                                                                                If sDato <> "" Then
                                                                                    oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(FSNLibrary.FormatNumber(sDato, FSNUser.NumberFormat))
                                                                                Else
                                                                                    oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
                                                                                End If
                                                                            End If
                                                                        Case 3  ' Fechas
                                                                            If Not (TypeOf (oFilaDesglose.Item("VALOR_FEC")) Is DBNull) Then
                                                                                oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(FormatDate(oFilaDesglose.Item("VALOR_FEC"), FSNUser.DateFormat))
                                                                            Else
                                                                                oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode("")
                                                                            End If
                                                                        Case 4  ' Booleanos
                                                                            If Not (TypeOf (oFilaDesglose.Item("VALOR_BOOL")) Is DBNull) Then
                                                                                If oFilaDesglose.Item("VALOR_BOOL") = 1 Then
                                                                                    oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(Textos(20))
                                                                                Else
                                                                                    oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(Textos(21))
                                                                                End If
                                                                            Else
                                                                                oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode("")
                                                                            End If
                                                                    End Select
                                                                Else
                                                                    Dim sCampoValor As String
                                                                    Select Case iTipoColumna(iColumna)
                                                                        Case 2
                                                                            sCampoValor = "VALOR_NUM"
                                                                        Case 3
                                                                            sCampoValor = "VALOR_FEC"
                                                                        Case 4
                                                                            sCampoValor = "VALOR_BOOL"
                                                                        Case Else
                                                                            sCampoValor = "VALOR_TEXT"
                                                                    End Select
                                                                    Dim oTablaExterna As FSNServer.TablaExterna = FSNServer.Get_Object(GetType(FSNServer.TablaExterna))
                                                                    oTablaExterna.LoadDefTabla(lTablaExterna(iColumna), False)
                                                                    oTablaExterna.LoadData(False)
                                                                    If Not IsDBNull(oFilaCampo.Item(sCampoValor)) Then
                                                                        sDato = oTablaExterna.DescripcionReg(oFilaCampo.Item(sCampoValor), Idioma, FSNUser.DateFormat, FSNUser.NumberFormat)
                                                                    Else
                                                                        sDato = ""
                                                                    End If
                                                                    oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
                                                                End If
                                                                oXMLDatoElementoDesglose.AppendChild(oXMLDatoTextoDesglose)
                                                            End If
                                                        End If ' Siguiente columna
                                                    Next  ' Siguiente fila
                                                End If
                                            Next
                                    End Select
                                End If

                                If oFilaCampo.Item("SUBTIPO") < 9 Then
                                    oXMLDatoElemento.AppendChild(oXMLDatoTexto)
                                    oXMLDatoElemento = Nothing
                                    oXMLDatoTexto = Nothing
                                End If
                            End If ' Siguiente campo del grupo
                        Next
                    End If
                End If ' Siguiente grupo de la instancia
            Next
        End If
        Select Case Request("Formato")
            Case "XML"
                Dim sPath As String
                Dim sBytes As Long

                sPath = ConfigurationManager.AppSettings("temp") + "\" + GenerateRandomPath()

                Dim oFolder As System.IO.Directory

                If Not oFolder.Exists(sPath) Then
                    oFolder.CreateDirectory(sPath)
                End If

                sPath += "\" + CStr(oInstancia.ID) + ".xml"

                oXmlDoc.Save(sPath)

                Dim ofile As New System.IO.FileInfo(sPath)

                sBytes = ofile.Length

                Dim arrPath() As String = sPath.Split("\")
                Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "_Common/download.aspx?path=" + arrPath(UBound(arrPath) - 1) + "&nombre=" + Server.UrlEncode(arrPath(UBound(arrPath))) + "&datasize=" + CStr(sBytes))
            Case "HTML"
                Dim oXslDoc As New XslTransform
                If Request("Desgloses") = "TABLA" Then
                    If Request("TipoImpExp") = "1" Then
                        If Not Request("Contrato") = "" Then
                            oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & Idioma & "_impexpContrtab.xslt")
                        Else
                            oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & Idioma & "_impexptab.xslt")
                        End If

                    ElseIf Request("TipoImpExp") = "2" Then
                        oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & Idioma & "_impexpcerttab.xslt")
                    ElseIf Request("TipoImpExp") = "3" Then
                        oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & Idioma & "_impexpnoconftab.xslt")
                    End If
                Else
                    If Request("TipoImpExp") = "1" Then
                        If Not Request("Contrato") = "" Then
                            oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & Idioma & "_impexpContrlin.xslt")
                        Else
                            oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & Idioma & "_impexplin.xslt")
                        End If

                    ElseIf Request("TipoImpExp") = "2" Then
                        oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & Idioma & "_impexpcertlin.xslt")
                    ElseIf Request("TipoImpExp") = "3" Then
                        oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & Idioma & "_impexpnoconflin.xslt")
                    End If
                End If
                ' Pasamos el HTML a una String                    
                xmlPres.Document = oXmlDoc
                xmlPres.Transform = oXslDoc

                'RUE a�adido para que se pueda elegir entre abrirlo y guardarlo
                ' Pasamos el HTML a una String
                Dim oStringWriter As New System.IO.StringWriter
                Dim oHtmlTextWriter As New System.Web.UI.HtmlTextWriter(oStringWriter)

                oHtmlTextWriter.WriteLine("<HTML>")
                oHtmlTextWriter.WriteLine("<HEAD>")
                oHtmlTextWriter.WriteLine("<meta http-equiv=""Content-Type"" content=""text/html; charset=windows-1252"">")
                oHtmlTextWriter.WriteLine("</HEAD>")
                oHtmlTextWriter.WriteLine("<BODY>")

                xmlPres.RenderControl(oHtmlTextWriter)

                oHtmlTextWriter.WriteLine("</BODY>")
                oHtmlTextWriter.WriteLine("</HTML>")

                ' Guardamos el HTML en un archivo

                Dim sPath As String
                Dim sBytes As Long

                sPath = ConfigurationManager.AppSettings("temp") + "\" + GenerateRandomPath()

                Dim oFolder As System.IO.Directory

                If Not oFolder.Exists(sPath) Then
                    oFolder.CreateDirectory(sPath)
                End If

                sPath += "\" + CStr(oInstancia.ID) + ".html"

                Dim oStreamWriter As New System.IO.StreamWriter(sPath, False, System.Text.Encoding.UTF8)
                oStreamWriter.Write(oStringWriter.ToString)
                oStreamWriter.Close()

                Dim ofile As New System.IO.FileInfo(sPath)
                sBytes = ofile.Length

                Dim arrPath() As String = sPath.Split("\")
                Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "_Common/download.aspx?path=" + arrPath(UBound(arrPath) - 1) + "&nombre=" + Server.UrlEncode(arrPath(UBound(arrPath))) + "&datasize=" + CStr(sBytes))
            Case "PDF"
                Dim oXslDoc As New XslTransform
                If Request("Desgloses") = "TABLA" Then
                    If Request("TipoImpExp") = "1" Then
                        If Not Request("Contrato") = "" Then
                            oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & Idioma & "_impexpContrtab.xslt")
                        Else
                            oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & Idioma & "_impexptab.xslt")
                        End If
                    ElseIf Request("TipoImpExp") = "2" Then
                        oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & Idioma & "_impexpcerttab.xslt")
                    ElseIf Request("TipoImpExp") = "3" Then
                        oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & Idioma & "_impexpnoconftab.xslt")
                    End If
                Else
                    If Request("TipoImpExp") = "1" Then
                        If Not Request("Contrato") = "" Then
                            oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & Idioma & "_impexpContrlin.xslt")
                        Else
                            oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & Idioma & "_impexplin.xslt")
                        End If
                    ElseIf Request("TipoImpExp") = "2" Then
                        oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & Idioma & "_impexpcertlin.xslt")
                    ElseIf Request("TipoImpExp") = "3" Then
                        oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & Idioma & "_impexpnoconflin.xslt")
                    End If
                End If

                xmlPres.Document = oXmlDoc
                xmlPres.Transform = oXslDoc

                ' Pasamos el HTML a una String
                Dim oStringWriter As New System.IO.StringWriter
                Dim oHtmlTextWriter As New System.Web.UI.HtmlTextWriter(oStringWriter)

                oHtmlTextWriter.WriteLine("<HTML>")
                oHtmlTextWriter.WriteLine("<HEAD>")
                oHtmlTextWriter.WriteLine("<meta http-equiv=""Content-Type"" content=""text/html; charset=windows-1252"">")
                oHtmlTextWriter.WriteLine("<STYLE>@import url( " & ConfigurationManager.AppSettings("ruta") + "App_Themes/" & Me.Theme & ".css );</STYLE>")
                oHtmlTextWriter.WriteLine("</HEAD>")
                oHtmlTextWriter.WriteLine("<BODY>")

                xmlPres.RenderControl(oHtmlTextWriter)

                oHtmlTextWriter.WriteLine("</BODY>")
                oHtmlTextWriter.WriteLine("</HTML>")

                ' Guardamos el HTML en un archivo
                Dim sPathBase As String
                Dim sPathHTML As String
                Dim sPathPDF As String
                Dim sBytes As Long
                Dim oFolder As System.IO.Directory

                sPathBase = ConfigurationManager.AppSettings("temp") + "\" + GenerateRandomPath()
                If Not oFolder.Exists(sPathBase) Then
                    oFolder.CreateDirectory(sPathBase)
                End If
                sPathHTML = sPathBase + "\" + CStr(oInstancia.ID) + ".html"
                sPathPDF = sPathBase + "\" + CStr(oInstancia.ID) + ".pdf"

                Dim oStreamWriter As New System.IO.StreamWriter(sPathHTML, False, System.Text.Encoding.UTF8)
                oStreamWriter.Write(oStringWriter.ToString)
                oStreamWriter.Close()

                ''''''' Pasamos el HTML a PDF
                ConvertHTMLToPDF(sPathHTML, sPathPDF)

                ' Ponemos el PDF a descargar
                Dim ofile As New System.IO.FileInfo(sPathPDF)
                sBytes = ofile.Length
                Dim arrPath2() As String = sPathPDF.Split("\")
                Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "_Common/download.aspx?path=" + arrPath2(UBound(arrPath2) - 1) + "&nombre=" + Server.UrlEncode(arrPath2(UBound(arrPath2))) + "&datasize=" + CStr(sBytes))
        End Select
    End Sub
    Private Function DenGS(ByRef sDato As String, ByVal tipo As Integer, ByVal Idi As String) As String
        ' Funci�n para completar el c�digo de GS con su descripci�n
        Dim sDen As String = String.Empty
        ' Definimos estas variables est�ticas para cuando venga despu�s de tener el material
        ' con el c�digo del art�culo a por la denominaci�n...
        Static sCodGMN1 As String
        Static sCodGMN2 As String
        Static sCodGMN3 As String
        Static sCodGMN4 As String

        ' Definimos esta variable est�tica para cuando venga a por la provincia despu�s de tener el pa�s...
        Static sCodPais As String

        ' Acci�n
        Try
            If (tipo >= 100) And (sDato <> "") Then
                Select Case tipo
                    Case TiposDeDatos.TipoCampoGS.Proveedor
                        Dim oProve As FSNServer.Proveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                        oProve.Cod = sDato
                        oProve.Load(Idi, True)
                        sDen = oProve.Den
                    Case TiposDeDatos.TipoCampoGS.FormaPago
                        Dim oFormaPago As FSNServer.FormasPago = FSNServer.Get_Object(GetType(FSNServer.FormasPago))
                        oFormaPago.LoadData(Idi, sDato)
                        sDen = oFormaPago.Data.Tables(0).Rows(0).Item("DEN")
                    Case TiposDeDatos.TipoCampoGS.Moneda
                        Dim oMoneda As FSNServer.Monedas = FSNServer.Get_Object(GetType(FSNServer.Monedas))
                        oMoneda.LoadData(Idi, sDato)
                        sDen = oMoneda.Data.Tables(0).Rows(0).Item("DEN")
                    Case TiposDeDatos.TipoCampoGS.Material
                        ' Grupo de material... Hay que extraer los c�digos
                        ' Primero mirar las longitudes en DIC
                        ' Luego extraerlos de sDato
                        ' Y luego... obtener la denominaci�n de GMN4.
                        Dim ilGMN1 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                        Dim ilGMN2 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                        Dim ilGMN3 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                        Dim ilGMN4 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN4

                        sCodGMN1 = ""
                        sCodGMN2 = ""
                        sCodGMN3 = ""
                        sCodGMN4 = ""

                        sCodGMN1 = Trim(Left(sDato, ilGMN1))

                        If Len(sDato) > ilGMN1 Then
                            sCodGMN2 = Trim(Mid(sDato, ilGMN1 + 1, ilGMN2))
                            If Len(sDato) > ilGMN1 + ilGMN2 Then
                                sCodGMN3 = Trim(Mid(sDato, ilGMN1 + ilGMN2 + 1, ilGMN3))
                                If Len(sDato) > ilGMN1 + ilGMN2 + ilGMN3 Then
                                    sCodGMN4 = Trim(Mid(sDato & Space(Len(sDato) - (ilGMN1 + ilGMN2 + ilGMN3 + ilGMN4)), ilGMN1 + ilGMN2 + ilGMN3 + 1, ilGMN4))

                                    ' GMN4
                                    Dim oGMN3 As FSNServer.GrupoMatNivel3 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel3))

                                    oGMN3.GMN1Cod = sCodGMN1
                                    oGMN3.GMN2Cod = sCodGMN2
                                    oGMN3.Cod = sCodGMN3
                                    oGMN3.CargarTodosLosGruposMatDesde(1, Idi, sCodGMN4, , True)
                                    sDen = oGMN3.GruposMatNivel4.Item(sCodGMN4).Den
                                Else ' GMN3
                                    Dim oGMN2 As FSNServer.GrupoMatNivel2 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel2))
                                    oGMN2.GMN1Cod = sCodGMN1
                                    oGMN2.Cod = sCodGMN2
                                    oGMN2.CargarTodosLosGruposMatDesde(1, Idi, sCodGMN3, , True)
                                    sDen = oGMN2.GruposMatNivel3.Item(sCodGMN3).Den
                                End If
                            Else ' GMN2
                                Dim oGMN1 As FSNServer.GrupoMatNivel1 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel1))

                                oGMN1.Cod = sCodGMN1
                                oGMN1.CargarTodosLosGruposMatDesde(1, Idi, sCodGMN2, , True)
                                sDen = oGMN1.GruposMatNivel2.Item(sCodGMN2).Den
                            End If
                        Else ' GMN1
                            Dim ogmn1 As FSNServer.GruposMatNivel1 = FSNServer.Get_Object(GetType(FSNServer.GruposMatNivel1))
                            ogmn1.CargarTodosLosGruposMatDesde(1, Idi, sCodGMN1, , True)
                            sDen = ogmn1.Item(sCodGMN1).Den
                        End If
                    Case TiposDeDatos.TipoCampoGS.CodArticulo
                        Dim oArt As FSNServer.Articulos = FSNServer.Get_Object(GetType(FSNServer.Articulos))
                        oArt.GMN1 = sCodGMN1
                        oArt.GMN2 = sCodGMN2
                        oArt.GMN3 = sCodGMN3
                        oArt.GMN4 = sCodGMN4
                        oArt.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil,
                                      sDato, , True, AccesoFSEP:=FSNServer.TipoAcceso.gbAccesoFSEP)

                        sDen = oArt.Data.Tables(0).Rows(0).Item("DEN")
                    Case TiposDeDatos.TipoCampoGS.Unidad
                        Dim oUnidad As FSNServer.Unidades = FSNServer.Get_Object(GetType(FSNServer.Unidad))
                        oUnidad.LoadData(Idi, sDato)
                        sDen = oUnidad.Data.Tables(0).Rows(0).Item("DEN")
                    Case TiposDeDatos.TipoCampoGS.Pais
                        sCodPais = ""

                        Dim oPais As FSNServer.Paises = FSNServer.Get_Object(GetType(FSNServer.Paises))
                        oPais.LoadData(Idi, sDato)
                        sDen = oPais.Data.Tables(0).Rows(0).Item("DEN")
                        sCodPais = sDato
                    Case TiposDeDatos.TipoCampoGS.Provincia
                        Dim oProvincia As FSNServer.Provincias = FSNServer.Get_Object(GetType(FSNServer.Provincias))
                        oProvincia.Pais = sCodPais
                        oProvincia.LoadData(Idi, sDato)

                        sDen = oProvincia.Data.Tables(0).Rows(0).Item("DEN")
                    Case TiposDeDatos.TipoCampoGS.Dest
                        Dim oDestino As FSNServer.Destino = FSNServer.Get_Object(GetType(FSNServer.Destino))
                        oDestino.Cod = sDato
                        oDestino.Load(Idi)

                        sDen = oDestino.Den
                    Case TiposDeDatos.TipoCampoGS.PRES1, TiposDeDatos.TipoCampoGS.Pres2, TiposDeDatos.TipoCampoGS.Pres3, TiposDeDatos.TipoCampoGS.Pres4
                        Dim oPres As Object

                        Select Case tipo
                            Case TiposDeDatos.TipoCampoGS.PRES1
                                oPres = FSNServer.Get_Object(GetType(FSNServer.PresProyectosNivel1))
                            Case TiposDeDatos.TipoCampoGS.Pres2
                                oPres = FSNServer.Get_Object(GetType(FSNServer.PresContablesNivel1))
                            Case TiposDeDatos.TipoCampoGS.Pres3
                                oPres = FSNServer.Get_Object(GetType(FSNServer.PresConceptos3Nivel1))
                            Case Else 'TiposDeDatos.TipoCampoGS.Pres4
                                oPres = FSNServer.Get_Object(GetType(FSNServer.PresConceptos4Nivel1))
                        End Select

                        Dim arrPresupuestos() As String = sDato.Split("#")
                        Dim oPresup As String
                        Dim arrPresup(2) As String
                        Dim iNivel As Integer
                        Dim lIdPresup As Long
                        Dim dPorcent As Double
                        Dim oDs As DataSet

                        sDen = ""
                        For Each oPresup In arrPresupuestos
                            arrPresup = oPresup.Split("_")
                            iNivel = arrPresup(0)
                            lIdPresup = arrPresup(1)
                            dPorcent = Numero(arrPresup(2), ".", ",")

                            Select Case iNivel
                                Case 1
                                    oPres.LoadData(lIdPresup, , , )
                                Case 2
                                    oPres.LoadData(Nothing, lIdPresup, , )
                                Case 3
                                    oPres.LoadData(Nothing, Nothing, lIdPresup, )
                                Case 4
                                    oPres.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                            End Select
                            oDs = oPres.Data

                            Dim iAnyo As Integer
                            Dim sUon1 As String
                            Dim sUon2 As String
                            Dim sUon3 As String

                            If Not oDs Is Nothing Then
                                If oDs.Tables(0).Rows.Count > 0 Then
                                    With oDs.Tables(0).Rows(0)
                                        'comprobamos que el valor por defecto cumpla la restricci�n del usuario. SI NO CUMPLE, NO APARECER�
                                        If Not oDs.Tables(0).Columns("ANYO") Is Nothing Then
                                            iAnyo = .Item("ANYO")
                                        End If
                                        sUon1 = DBNullToSomething(.Item("UON1"))
                                        sUon2 = DBNullToSomething(.Item("UON2"))
                                        sUon3 = DBNullToSomething(.Item("UON3"))

                                        Dim bSeleccionado As Boolean = True
                                        Dim sPorcentaje As String
                                        Dim sDenPres As String
                                        For i As Integer = 4 To 1 Step -1
                                            'PRESi:
                                            If Not IsDBNull(.Item("PRES" & i)) Then
                                                If bSeleccionado Then
                                                    sPorcentaje = " (" & dPorcent.ToString("0.00%", FSNUser.NumberFormat) & "); "
                                                    sDenPres = " - " & .Item("DEN").ToString
                                                Else
                                                    sPorcentaje = " - "
                                                    sDenPres = ""
                                                End If
                                                sDen = .Item("PRES" & i).ToString & sDenPres & sPorcentaje & sDen
                                                bSeleccionado = False
                                            End If
                                        Next
                                        If Not oDs.Tables(0).Columns("ANYO") Is Nothing Then
                                            sDen = .Item("ANYO").ToString & " - " & sDen
                                        End If

                                        bSeleccionado = True
                                        Dim sUONs As String = ""
                                        Dim sDenUON As String = ""
                                        For i As Integer = 3 To 1 Step -1
                                            'UONi
                                            If Not IsDBNull(.Item("UON" & i)) Then
                                                If bSeleccionado Then
                                                    sDenUON = " - " & .Item("UONDEN").ToString
                                                Else
                                                    sDenUON = " - "
                                                End If
                                                sUONs = .Item("UON" & i).ToString & sDenUON & sUONs
                                                bSeleccionado = False
                                            End If
                                        Next
                                        sDen = "( " & sUONs & " ) " & sDen
                                    End With
                                End If
                            End If
                        Next
                        sDato = ""
                End Select

                DenGS = sDen
            End If
        Catch ex As Exception
            ' Si hay alg�n problema para obtener el dato de denominaci�n de GS,
            ' no se incluye.
            DenGS = ""
        End Try
    End Function
End Class