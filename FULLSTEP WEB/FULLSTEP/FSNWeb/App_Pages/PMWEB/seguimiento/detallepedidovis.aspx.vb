Public Class detallepedidovis
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblDetalle As System.Web.UI.WebControls.Label
    Protected WithEvents lblCerrar As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents lblPedido As System.Web.UI.WebControls.Label
    Protected WithEvents lblNumPed As System.Web.UI.WebControls.Label
    Protected WithEvents lblProve As System.Web.UI.WebControls.Label
    Protected WithEvents lblProveBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblEstado As System.Web.UI.WebControls.Label
    Protected WithEvents lblNumPedERP As System.Web.UI.WebControls.Label
    Protected WithEvents lblSolicitud As System.Web.UI.WebControls.Label
    Protected WithEvents lblNumSol As System.Web.UI.WebControls.Label
    Protected WithEvents lblDatosGen As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechaBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblLineas As System.Web.UI.WebControls.Label
    Protected WithEvents tblPedido As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblDatosPedido As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblCantidad As System.Web.UI.WebControls.Label
    Protected WithEvents lblUnidad As System.Web.UI.WebControls.Label
    Protected WithEvents lblPrecioUd As System.Web.UI.WebControls.Label
    Protected WithEvents lblImporte As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecDeseada As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecProve As System.Web.UI.WebControls.Label
    Protected WithEvents lblObl As System.Web.UI.WebControls.Label
    Protected WithEvents lblImporteTotal As System.Web.UI.WebControls.Label
    Protected WithEvents lblImporteTotalValor As System.Web.UI.WebControls.Label
    Protected WithEvents lblPeticionario As System.Web.UI.WebControls.Label
    Protected WithEvents lblPeticionarioBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblReceptor As System.Web.UI.WebControls.Label
    Protected WithEvents lblReceptorBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblObservaciones As System.Web.UI.WebControls.Label
    Protected WithEvents lblObservacionesBD As System.Web.UI.WebControls.Label
    Protected WithEvents tblLineas As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents cmdDetalleProve As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents cmdDetallePet As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents cmdDetalleRecep As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents cmdImprimir As System.Web.UI.HtmlControls.HtmlInputButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim oPedido As FSNServer.Pedido
    Dim oMonedas As FSNServer.Monedas
    Private oTextos As DataTable

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim iIdi As Integer
        Dim oFila As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oCelda As System.Web.UI.HtmlControls.HtmlTableCell

        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.DetallePedido, Idioma)
        oTextos = oDict.Data.Tables(0)


        oPedido = FSNServer.Get_Object(GetType(FSNServer.Pedido))
        oPedido.ID = Request("Pedido")
        oPedido.Instancia = Request("Instancia")
        oPedido.Load(Idioma)

        lblDetalle.Text = oTextos.Rows(0).Item("TEXT_" & Idioma) 'Detalle de pedido
        lblSolicitud.Text = oTextos.Rows(1).Item("TEXT_" & Idioma) 'Solicitud
        lblPedido.Text = oTextos.Rows(2).Item("TEXT_" & Idioma) 'pedido

        ''' N�mero de pedido del ERP si procede

        If oPedido.Tipo = 0 Then
            If FSNServer.TipoAcceso.gbOblCodPedDir Then
                lblNumPedERP.Text = FSNServer.TipoAcceso.nomPedDirERP(Idioma) & ": " & oPedido.NumPedErp
            End If
        Else
            If FSNServer.TipoAcceso.gbOblCodPedido Then
                lblNumPedERP.Text = FSNServer.TipoAcceso.nomPedERP(Idioma) & ": " & oPedido.NumPedErp
            End If
        End If

        Me.cmdImprimir.Attributes("onclick") = "return imprimir(" + oPedido.Instancia.ToString() + "," + oPedido.ID.ToString() + ");"

        lblProve.Text = oTextos.Rows(4).Item("TEXT_" & Idioma) 'proveedor
        If oPedido.BajaLog Then
            lblEstado.Text = oTextos.Rows(42).Item("TEXT_" & Idioma)
        Else
            Select Case oPedido.Estado
                Case TipoEstadoOrdenEntrega.PendienteDeAprobacion
                    lblEstado.Text = oTextos.Rows(5).Item("TEXT_" & Idioma)
                Case TipoEstadoOrdenEntrega.DenegadoParcialAprob
                    lblEstado.Text = oTextos.Rows(6).Item("TEXT_" & Idioma)
                Case TipoEstadoOrdenEntrega.EmitidoAlProveedor
                    lblEstado.Text = oTextos.Rows(7).Item("TEXT_" & Idioma)
                Case TipoEstadoOrdenEntrega.AceptadoPorProveedor
                    lblEstado.Text = oTextos.Rows(8).Item("TEXT_" & Idioma)
                Case TipoEstadoOrdenEntrega.EnCamino
                    lblEstado.Text = oTextos.Rows(9).Item("TEXT_" & Idioma)
                Case TipoEstadoOrdenEntrega.EnRecepcion
                    lblEstado.Text = oTextos.Rows(10).Item("TEXT_" & Idioma)
                Case TipoEstadoOrdenEntrega.RecibidoYCerrado
                    lblEstado.Text = oTextos.Rows(11).Item("TEXT_" & Idioma)
                Case TipoEstadoOrdenEntrega.Anulado
                    lblEstado.Text = oTextos.Rows(12).Item("TEXT_" & Idioma)
                Case TipoEstadoOrdenEntrega.RechazadoPorProveedor
                    lblEstado.Text = oTextos.Rows(13).Item("TEXT_" & Idioma)
                Case TipoEstadoOrdenEntrega.DenegadoTotalAprobador
                    lblEstado.Text = oTextos.Rows(14).Item("TEXT_" & Idioma)
            End Select
        End If

        lblDatosGen.Text = oTextos.Rows(15).Item("TEXT_" & Idioma) 'Datos Generales del Pedido
        lblImporteTotal.Text = oTextos.Rows(16).Item("TEXT_" & Idioma) 'Importe Total
        lblFecha.Text = oTextos.Rows(17).Item("TEXT_" & Idioma) 'fecha de emisi�n
        lblPeticionario.Text = oTextos.Rows(18).Item("TEXT_" & Idioma) 'Peticionario
        lblReceptor.Text = oTextos.Rows(19).Item("TEXT_" & Idioma) 'Receptor
        lblObservaciones.Text = oTextos.Rows(20).Item("TEXT_" & Idioma) 'Observaciones Generales

        lblLineas.Text = oTextos.Rows(22).Item("TEXT_" & Idioma)  'Lineas de Pedidos
        lblCantidad.Text = oTextos.Rows(23).Item("TEXT_" & Idioma)  'Cantidad
        lblUnidad.Text = oTextos.Rows(24).Item("TEXT_" & Idioma) 'Unidad
        lblPrecioUd.Text = oTextos.Rows(25).Item("TEXT_" & Idioma) 'Precio Unitario
        lblImporte.Text = oTextos.Rows(26).Item("TEXT_" & Idioma) 'Importe
        lblFecDeseada.Text = oTextos.Rows(27).Item("TEXT_" & Idioma) 'Fecha ent. deseada
        lblFecProve.Text = oTextos.Rows(28).Item("TEXT_" & Idioma) 'Fecha ent. Proveedor
        lblObl.Text = oTextos.Rows(29).Item("TEXT_" & Idioma) 'Obl.

        lblNumSol.Text = oPedido.Instancia & " " & oPedido.DenInstacia
        lblNumPed.Text = oPedido.Anyo & "/" & oPedido.NumPedido & "/" & oPedido.NumOrden
        lblProveBD.Text = oPedido.Prov & " - " & oPedido.DenProv
        If oPedido.Prov <> Nothing And oPedido.Prov <> "" Then
            cmdDetalleProve.Attributes.Add("onclick", "javascript:DetalleProveedor('" & oPedido.Prov & "')")
        Else
            cmdDetalleProve.Visible = False
        End If
        lblImporteTotalValor.Text = FSNLibrary.FormatNumber((oPedido.Importe * oPedido.Cambio), FSNUser.NumberFormat) & " " & oPedido.Moneda

        lblFechaBD.Text = FormatDate(oPedido.Fecha, FSNUser.DateFormat)
        lblPeticionarioBD.Text = oPedido.Peticionario
        If oPedido.IDPeticionario <> Nothing And oPedido.IDPeticionario <> "" Then
            cmdDetallePet.Attributes.Add("onclick", "javascript:DetallePersona('" & oPedido.IDPeticionario & "')")
        Else
            cmdDetallePet.Visible = False
        End If
        lblReceptorBD.Text = oPedido.Receptor
        If oPedido.IDReceptor <> Nothing And oPedido.IDReceptor <> "" Then
            cmdDetalleRecep.Attributes.Add("onclick", "javascript:DetallePersona('" & oPedido.IDReceptor & "')")
        Else
            cmdDetalleRecep.Visible = False
        End If
        lblObservacionesBD.Text = oPedido.Observaciones

        For Each oAtribRow As DataRow In oPedido.Atributos.Rows
            'Ponemos los atributos del Pedido
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.VAlign = "top"
            oCelda.InnerHtml = "<nobr>" & oAtribRow.Item("DEN") & ":</nobr>"
            oFila.Cells.Add(oCelda)

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            Dim valorAtrib As String = obtenerValorAtributo(oAtribRow, oTextos.Rows(35), oTextos.Rows(36))
            oCelda.InnerText = valorAtrib
            oFila.Cells.Add(oCelda)
            oFila.Attributes("class") = "captionBlueSmall"
            tblDatosPedido.Rows.Add(oFila)
        Next

        If (oPedido.Adjuntos.Rows.Count > 0) Then
            'Ponemos los archivos adjuntos del Pedido
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.VAlign = "top"
            oCelda.InnerHtml = "<nobr>" & oTextos.Rows(21).Item("TEXT_" & Idioma) & "</nobr>"
            oFila.Cells.Add(oCelda)

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            Dim sEspecific As String = obtenerStringAdjuntos(oPedido.Adjuntos.Select("ORDEN=" & oPedido.ID))
            oCelda.InnerHtml = "<a class='aPMWeb' onclick=show_atached_files(" + oPedido.Instancia.ToString() + "," + oPedido.ID.ToString() + ",null) href='#'>" + sEspecific + "</a>"
            oCelda.ColSpan = 6
            oFila.Cells.Add(oCelda)
            oFila.Attributes("class") = "captionBlueSmall"
            tblDatosPedido.Rows.Add(oFila)
        End If
        'Linea que empuja todo hacia arriba:
        oFila = New System.Web.UI.HtmlControls.HtmlTableRow
        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
        oCelda.Height = Unit.Percentage(100).ToString()
        oCelda.ColSpan = 2
        oFila.Cells.Add(oCelda)
        tblDatosPedido.Rows.Add(oFila)

        For Each oLineaRow As DataRow In oPedido.Lineas.Rows
            'Vamos poniendo los datos generales de cada linea:
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow

            'CODARTICULO
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = oLineaRow.Item("CODART").ToString
            oCelda.Style.Add("font-weight", "bold")
            oFila.Cells.Add(oCelda)

            'DENARTICULO
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = oLineaRow.Item("DENART").ToString
            oCelda.Style.Add("font-weight", "bold")
            oFila.Cells.Add(oCelda)

            'CANTIDAD
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = oLineaRow.Item("CANTIDAD").ToString
            oFila.Cells.Add(oCelda)

            'UNIDAD
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            If IsDBNull(oLineaRow.Item("UNIDAD")) Then
                oCelda.InnerText = ""
            Else
                oCelda.InnerHtml = "<a class='aPMWeb' onclick=irADetalleUnidad('" + oLineaRow.Item("UNIDAD").ToString + "') href='#'>" + oLineaRow.Item("UNIDAD").ToString + "</a>"
            End If
            oFila.Cells.Add(oCelda)

            'PRECIO UNITARIO
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = FSNLibrary.FormatNumber((CDbl(oLineaRow.Item("PREC_UNIDAD")) * oPedido.Cambio), FSNUser.NumberFormat) & " " & oPedido.Moneda
            oFila.Cells.Add(oCelda)

            'IMPORTE
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = FSNLibrary.FormatNumber((CDbl(oLineaRow.Item("CANTIDAD")) * CDbl(oLineaRow.Item("PREC_UNIDAD")) * oPedido.Cambio), FSNUser.NumberFormat) & " " & oPedido.Moneda
            oFila.Cells.Add(oCelda)

            'FECHA DESEADA
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            If Not IsDBNull(oLineaRow.Item("FECENTREGA")) Then
                oCelda.InnerText = FormatDate(oLineaRow.Item("FECENTREGA"), FSNUser.DateFormat)
            End If
            oFila.Cells.Add(oCelda)

            'FECHA PROVEEDOR
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            If Not IsDBNull(oLineaRow.Item("FECENTREGAPROVE")) Then
                oCelda.InnerText = FormatDate(oLineaRow.Item("FECENTREGAPROVE"), FSNUser.DateFormat)
            End If
            oFila.Cells.Add(oCelda)

            'OBLIGATORIO
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            Select Case oLineaRow.Item("ENTREGA_OBL")
                Case 0
                    oCelda.InnerText = oTextos.Rows(36).Item("TEXT_" & Idioma).ToString
                Case 1
                    oCelda.InnerText = oTextos.Rows(35).Item("TEXT_" & Idioma).ToString
            End Select
            oFila.Cells.Add(oCelda)

            oFila.Attributes.Add("class", "VisorRow")
            tblLineas.Rows.Add(oFila)

            'Linea Separadora
            'oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            'oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            'oCelda.Attributes.Add("class", "subtitulo")
            'oCelda.VAlign = "bottom"
            'oCelda.BgColor = "dimgray"
            'oCelda.ColSpan = 9
            'oCelda.Height = "3"
            'oFila.Cells.Add(oCelda)
            'tblLineas.Rows.Add(oFila)

            'Atributos
            For Each oAtributoRow As DataRow In oLineaRow.GetChildRows("REL_LINEA_ATRIB")
                oFila = New System.Web.UI.HtmlControls.HtmlTableRow

                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.VAlign = "top"
                oCelda.InnerHtml = "<nobr>" & oAtributoRow.Item("DEN") & ":</nobr>"
                oFila.Cells.Add(oCelda)

                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                Dim valorAtrib As String = obtenerValorAtributo(oAtributoRow, oTextos.Rows(35), oTextos.Rows(36))
                oCelda.InnerText = valorAtrib
                oCelda.ColSpan = 8
                oFila.Cells.Add(oCelda)
                oFila.Attributes("class") = "captionBlueSmall"
                tblLineas.Rows.Add(oFila)
            Next

            'Adjuntos
            If oLineaRow.GetChildRows("REL_LINEA_ADJUN").Length > 0 Then
                oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.VAlign = "top"
                oCelda.InnerHtml = "<nobr>" & oTextos.Rows(30).Item("TEXT_" & Idioma) & "</nobr>"
                oFila.Cells.Add(oCelda)

                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                Dim sEspecific As String = obtenerStringAdjuntos(oLineaRow.GetChildRows("REL_LINEA_ADJUN"))
                oCelda.InnerHtml = "<a class='aPMWeb' onclick=show_atached_files(" + oPedido.Instancia.ToString() + "," + oPedido.ID.ToString + "," + oLineaRow.Item("ID").ToString + ") href='#'>" + sEspecific + "</a>"
                oCelda.ColSpan = 8
                oFila.Cells.Add(oCelda)
                oFila.Attributes("class") = "captionBlueSmall"
                tblLineas.Rows.Add(oFila)
            End If
            'Observaciones
            If Not IsDBNull(oLineaRow.Item("OBS")) Then
                oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.VAlign = "top"
                oCelda.InnerText = oTextos.Rows(31).Item("TEXT_" & Idioma)
                oFila.Cells.Add(oCelda)

                oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                oCelda.InnerText = oLineaRow.Item("OBS").ToString
                oCelda.ColSpan = 8
                oFila.Cells.Add(oCelda)
                oFila.Attributes("class") = "captionBlueSmall"
                tblLineas.Rows.Add(oFila)
            End If

            'Cant. Recibida
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.VAlign = "top"
            oCelda.InnerText = oTextos.Rows(32).Item("TEXT_" & Idioma)
            oFila.Cells.Add(oCelda)

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = oLineaRow.Item("CANTIDAD_REC").ToString & " " & oLineaRow.Item("UNIDAD").ToString
            oCelda.ColSpan = 8
            oFila.Cells.Add(oCelda)
            oFila.Attributes("class") = "captionBlueSmall"
            tblLineas.Rows.Add(oFila)

            'Cant. Pendiente
            oFila = New System.Web.UI.HtmlControls.HtmlTableRow
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.VAlign = "top"
            oCelda.InnerText = oTextos.Rows(33).Item("TEXT_" & Idioma)
            oFila.Cells.Add(oCelda)

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerText = (CDbl(oLineaRow.Item("CANTIDAD")) - CDbl(oLineaRow.Item("CANTIDAD_REC"))) & " " & oLineaRow.Item("UNIDAD").ToString
            oCelda.ColSpan = 8
            oFila.Cells.Add(oCelda)
            oFila.Attributes("class") = "captionBlueSmall"
            tblLineas.Rows.Add(oFila)
        Next
        'Linea que empuja todo hacia arriba:
        oFila = New System.Web.UI.HtmlControls.HtmlTableRow
        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
        oCelda.Height = Unit.Percentage(100).ToString()
        oCelda.ColSpan = 9
        oFila.Cells.Add(oCelda)
        tblLineas.Rows.Add(oFila)

        lblCerrar.InnerText = oTextos.Rows(34).Item("TEXT_" & Idioma) 'cerrar
        oTextos = Nothing
        If Request("pdf") = 1 Then
            'Pongo invisibles las imagenes de detalles para que no salgan en el pdf:
            cmdDetalleProve.Visible = False
            cmdDetallePet.Visible = False
            cmdDetalleRecep.Visible = False

            'Genero el pdf
            generarPDF()
        End If

    End Sub

    Private Function obtenerValorAtributo(ByVal oAtribRow As DataRow, ByVal oTrueRow As DataRow, ByVal oFalseRow As DataRow) As String
        Dim retorno As String
        Select Case oAtribRow.Item("TIPO")
            Case TiposDeDatos.TipoGeneral.TipoString
                retorno = oAtribRow.Item("VALOR_TEXT").ToString
            Case TiposDeDatos.TipoGeneral.TipoNumerico
                retorno = oAtribRow.Item("VALOR_NUM").ToString
            Case TiposDeDatos.TipoGeneral.TipoFecha
                If Not IsDBNull(oAtribRow.Item("VALOR_FEC")) Then
                    retorno = FormatDate(oAtribRow.Item("VALOR_FEC"), FSNUser.DateFormat)
                End If
            Case TiposDeDatos.TipoGeneral.TipoBoolean
                If Not IsDBNull(oAtribRow.Item("VALOR_BOOL")) Then
                    Select Case oAtribRow.Item("VALOR_BOOL")
                        Case 0
                            retorno = oFalseRow.Item("TEXT_" & Idioma).ToString
                        Case 1
                            retorno = oTrueRow.Item("TEXT_" & Idioma).ToString
                    End Select
                End If
        End Select
        obtenerValorAtributo = retorno
    End Function

    Private Function aMonedaUsuario(ByVal valor As Double, ByVal cambio As Double) As Double
        Dim valorMonCentral As Double = valor * cambio
        Dim equivalencia As Double = CDbl(oMonedas.Data.Tables(0).Rows(0).Item("EQUIV"))
        aMonedaUsuario = valorMonCentral / equivalencia
    End Function

    Private Function obtenerStringAdjuntos(ByVal oAdjuntosRows() As DataRow)
        Dim sRetorno As String = ""
        For Each oEspecRow As DataRow In oAdjuntosRows
            sRetorno = sRetorno & oEspecRow.Item("NOM") & "(" & oEspecRow.Item("DATASIZE") / 1024 & " kb.);"
        Next
        sRetorno = Mid(sRetorno, 1, Len(sRetorno) - 1)
        obtenerStringAdjuntos = sRetorno
    End Function

    Private Sub generarPDF()
        Dim oStringWriter As New System.IO.StringWriter
        Dim oHtmlTextWriter As New System.Web.UI.HtmlTextWriter(oStringWriter)
        Me.cmdImprimir.Visible = False
        Me.lblCerrar.Visible = False

        oHtmlTextWriter.WriteLine("<HTML>")
        oHtmlTextWriter.WriteLine("<HEAD>")
        oHtmlTextWriter.WriteLine("<meta http-equiv=""Content-Type"" content=""text/html; charset=windows-1252"">")
        oHtmlTextWriter.WriteLine("<STYLE>@import url( " & ConfigurationManager.AppSettings("ruta") + "App_Themes/" & Me.Theme & ".css );</STYLE>")

        oHtmlTextWriter.WriteLine("</HEAD>")
        oHtmlTextWriter.WriteLine("<BODY>")

        Me.RenderControl(oHtmlTextWriter)
        oHtmlTextWriter.WriteLine("</BODY>")
        oHtmlTextWriter.WriteLine("</HTML>")

        '''''' Guardamos el HTML en un archivo
        Dim sPathBase As String
        Dim sPathHTML As String
        Dim sPathPDF As String
        Dim oFolder As System.IO.Directory

        sPathBase = ConfigurationManager.AppSettings("temp") + "\" + GenerateRandomPath()
        If Not oFolder.Exists(sPathBase) Then
            oFolder.CreateDirectory(sPathBase)
        End If
        sPathHTML = sPathBase + "\" + CStr(oPedido.Anyo) + CStr(oPedido.NumPedido) + CStr(oPedido.NumOrden) + ".html"
        sPathPDF = sPathBase + "\" + CStr(oPedido.Anyo) + CStr(oPedido.NumPedido) + CStr(oPedido.NumOrden) + ".pdf"

        Dim oStreamWriter As New System.IO.StreamWriter(sPathHTML, False, System.Text.Encoding.UTF8)
        oStreamWriter.Write(oStringWriter.ToString)
        oStreamWriter.Close()

        ''' Pasamos el HTML a PDF
        ConvertHTMLToPDF(sPathHTML, sPathPDF)

        ''''''' Ponemos el PDF a descargar
        Response.Redirect("detallepedidohid.aspx?path=" & Server.UrlEncode(sPathPDF))
    End Sub

End Class

