
Public Class NWcomentariossolic
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()

    End Sub

    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblId As System.Web.UI.WebControls.Label
    Protected WithEvents lblNombre As System.Web.UI.WebControls.Label
    Protected WithEvents uwgHistorico As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
    Protected WithEvents lblActual As System.Web.UI.WebControls.Label
    Protected WithEvents uwgProcesos As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
    Protected WithEvents uwgLineasCat As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
    Protected WithEvents uwgPedNeg As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
    Protected WithEvents uwgPedCatNeg As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
    Protected WithEvents uwgPedDir As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
    Protected WithEvents uwgPedAbono As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
    Protected WithEvents uwgPedLibres As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
    Protected WithEvents tblActual As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents cmdCerrar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechaBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblIdBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblNombreBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblPeticionario As System.Web.UI.WebControls.Label
    Protected WithEvents lblPeticionarioBD As System.Web.UI.WebControls.Label
    Protected WithEvents txtInstancia As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents hidNumFactura As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblFlujo As System.Web.UI.WebControls.Label
    Protected WithEvents lblEnProceso As System.Web.UI.WebControls.Label
    Protected WithEvents phTitulo As System.Web.UI.WebControls.PlaceHolder
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private m_sIdiAcciones(2) As String
    Private m_sEstadoPed(10) As String
    Private m_sEstadoProceso(10) As String
    Private m_sIdiParalelas As String
    Private oInstancia As FSNServer.Instancia
    Private m_bVerDetallePer As Boolean

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim bHayProcesosOPedidos As Boolean
        Dim sNumFactura As String = String.Empty

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ComentariosSolic

        If Request("NumFactura") <> Nothing Then sNumFactura = Request("NumFactura")
        hidNumFactura.Value = sNumFactura

        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = Request("Instancia")
        oInstancia.Load(Idioma, True)

        txtInstancia.Value = oInstancia.ID

        m_sIdiAcciones(1) = Textos(19)  'Traslado
        m_sIdiAcciones(2) = Textos(20)  'Devoluci�n

        m_sEstadoPed(1) = Textos(37)  'Pendiente de aprobaci�n
        m_sEstadoPed(2) = Textos(38)  'Denegado parcial por aprobador
        m_sEstadoPed(3) = Textos(39)  'Emitido al proveedor
        m_sEstadoPed(4) = Textos(40) 'Aceptado por el proveedor
        m_sEstadoPed(5) = Textos(41)  'En camino
        m_sEstadoPed(6) = Textos(42)  'En recepci�n
        m_sEstadoPed(7) = Textos(43)  'Recibido completamente y cerrado
        m_sEstadoPed(8) = Textos(44)  'Anulado
        m_sEstadoPed(9) = Textos(45)  'Rechazado por proveedor
        m_sEstadoPed(10) = Textos(46) 'Denegado total por aprobador

        lblTitulo.Text = Textos(63)
        cmdCerrar.Value = Textos(60)
        lblFlujo.Text = Textos(71)
        m_sIdiParalelas = Textos(72)

        'Cabecera:
        If sNumFactura <> String.Empty Then
            lblId.Text = Textos(82) & ":"
            lblIdBD.Text = sNumFactura
        Else
            lblId.Text = Textos(8)
            lblIdBD.Text = oInstancia.ID
        End If

        lblFecha.Text = Textos(68)
        lblFechaBD.Text = FormatDate(oInstancia.FechaAlta, FSNUser.DateFormat)
        lblNombre.Text = Textos(69)
        lblNombreBD.Text = oInstancia.Solicitud.Den(Idioma)
        lblPeticionario.Text = Textos(70)
        lblPeticionarioBD.Text = oInstancia.NombrePeticionario

        Dim dsPermisosRol As DataSet
        dsPermisosRol = FSNUser.VerDetallesPersona(oInstancia.ID)
        If dsPermisosRol.Tables.Count > 0 Then
            If dsPermisosRol.Tables(0).Rows(0).Item("VER_DETALLE_PER") = 0 Then
                lblPeticionario.Visible = False
                lblPeticionarioBD.Visible = False
                m_bVerDetallePer = False
            Else
                m_bVerDetallePer = True
            End If
        End If

        If Request("EnProceso") = "1" Then
            lblEnProceso.Text = Textos(74)
            lblEnProceso.Visible = True
            lblFlujo.Visible = False
            lblActual.Visible = False
            uwgHistorico.Visible = False
            uwgProcesos.Visible = False
            uwgLineasCat.Visible = False
            uwgPedNeg.Visible = False
            uwgPedCatNeg.Visible = False
            uwgPedDir.Visible = False
            uwgPedAbono.Visible = False
            uwgPedLibres.Visible = False
            phTitulo.Visible = False
        Else

            'Rellena la grid de hist�rico de estados:
            oInstancia.CargarHistoricoEstados(Idioma)
            If oInstancia.HistoricoEst.Tables.Count > 0 Then
                uwgHistorico.DataSource = oInstancia.HistoricoEst
                uwgHistorico.DataMember = ""
                uwgHistorico.DataBind()
            End If

            'Idiomas en las caption de la grid: 
            uwgHistorico.Bands(0).Columns.FromKey("DEN_BLOQUE1").Header.Caption = Textos(64)
            uwgHistorico.Bands(0).Columns.FromKey("ROL").Header.Caption = Textos(65)
            uwgHistorico.Bands(0).Columns.FromKey("USUARIO").Header.Caption = Textos(3)
            uwgHistorico.Bands(0).Columns.FromKey("ACCION").Header.Caption = Textos(66)
            uwgHistorico.Bands(0).Columns.FromKey("FECHA").Header.Caption = Textos(4)
            uwgHistorico.Bands(0).Columns.FromKey("DEN_DESTINO").Header.Caption = Textos(67)
            uwgHistorico.Bands(0).Columns.FromKey("COMENT").Header.Caption = Textos(7)

            lblActual.Text = Textos(61)


            'Carga la grid de procesos si es que hay alguno relacionado:
            oInstancia.DevolverProcesosRelacionados()
            If oInstancia.Procesos.Tables.Count > 0 Then
                If oInstancia.Procesos.Tables(0).Rows.Count > 0 Then
                    bHayProcesosOPedidos = True
                    uwgProcesos.DataSource = oInstancia.Procesos
                    uwgProcesos.DataMember = ""
                    uwgProcesos.DataBind()

                    uwgProcesos.Bands(0).Columns.FromKey("PROCE").Header.Caption = Textos(28)
                    uwgProcesos.Bands(0).Columns.FromKey("DEN").Header.Caption = Textos(29)
                    uwgProcesos.Bands(0).Columns.FromKey("DETALLE").Header.Caption = Textos(30)
                    uwgProcesos.Bands(0).Columns.FromKey("FECNEC").Header.Caption = Textos(31)
                    uwgProcesos.Bands(0).Columns.FromKey("DEN_ESTADO").Header.Caption = Textos(0)
                Else
                    Me.tblActual.Rows(1).Visible = False
                    uwgProcesos.Visible = False
                End If
            Else
                Me.tblActual.Rows(1).Visible = False
                uwgProcesos.Visible = False
            End If

            'Carga la grid de l�neas de cat�logo:
            oInstancia.DevolverLineasCatRelacionadas()
            If oInstancia.Pedidos.Tables.Count > 0 Then
                If oInstancia.Pedidos.Tables(0).Rows.Count > 0 Then
                    bHayProcesosOPedidos = True
                    uwgLineasCat.DataSource = oInstancia.Pedidos
                    uwgLineasCat.DataMember = ""
                    uwgLineasCat.DataBind()

                    uwgLineasCat.Bands(0).Columns.FromKey("CAT").Header.Caption = Textos(58)
                    uwgLineasCat.Bands(0).Columns.FromKey("ARTICULO").Header.Caption = Textos(57)
                    uwgLineasCat.Bands(0).Columns.FromKey("DETALLE").Header.Caption = ""
                    uwgLineasCat.Bands(0).Columns.FromKey("PROVE").Header.Caption = Textos(33)
                    uwgLineasCat.Bands(0).Columns.FromKey("PROVEDEN").Header.Caption = ""
                Else
                    Me.tblActual.Rows(2).Visible = False
                    uwgLineasCat.Visible = False
                End If
            Else
                Me.tblActual.Rows(2).Visible = False
                uwgLineasCat.Visible = False
            End If

            Dim oCParametros As FSNServer.CParametros = FSNServer.Get_Object(GetType(FSNServer.CParametros))
            Dim sNum_Ped_ERP_Label As String = oCParametros.CargarLiteralParametros(TiposDeDatos.LiteralesParametros.PedidoERP, Usuario.Idioma)

            'Carga la grid de pedidos negociados (GS):
            oInstancia.DevolverPedidosRelacionados(0, bOblCodPedDir:=Me.Acceso.gbOblCodPedDir)
            If oInstancia.Pedidos.Tables.Count > 0 Then
                If oInstancia.Pedidos.Tables(0).Rows.Count > 0 Then
                    bHayProcesosOPedidos = True
                    uwgPedNeg.DataSource = oInstancia.Pedidos
                    uwgPedNeg.DataMember = ""
                    uwgPedNeg.DataBind()

                    uwgPedNeg.Bands(0).Columns.FromKey("PEDIDO").Header.Caption = Textos(32) 'Pedidos negociados
                    uwgPedNeg.Bands(0).Columns.FromKey("PROVEEDOR").Header.Caption = Textos(33)
                    uwgPedNeg.Bands(0).Columns.FromKey("DETALLE").Header.Caption = Textos(30)
                    uwgPedNeg.Bands(0).Columns.FromKey("FECEMISION").Header.Caption = Textos(34)
                    uwgPedNeg.Bands(0).Columns.FromKey("DEN_ESTADO").Header.Caption = Textos(0)
                    If Me.Acceso.gbOblCodPedDir Then
                        uwgPedNeg.Bands(0).Columns.FromKey("NUM_PED_ERP").Header.Caption = sNum_Ped_ERP_Label
                    End If
                Else
                    Me.tblActual.Rows(3).Visible = False
                    uwgPedNeg.Visible = False
                End If
            Else
                Me.tblActual.Rows(3).Visible = False
                uwgPedNeg.Visible = False
            End If

            'Carga la grid de pedidos de aprovisionamiento (pedidos de cat�logo negociados) (NO LIBRES):
            oInstancia.DevolverPedidosRelacionados(1, bOblCodPedDir:=Me.Acceso.gbOblCodPedDir)
            If oInstancia.Pedidos.Tables.Count > 0 Then
                If oInstancia.Pedidos.Tables(0).Rows.Count > 0 Then
                    bHayProcesosOPedidos = True
                    uwgPedCatNeg.DataSource = oInstancia.Pedidos
                    uwgPedCatNeg.DataMember = ""
                    uwgPedCatNeg.DataBind()

                    uwgPedCatNeg.Bands(0).Columns.FromKey("PEDIDO").Header.Caption = Textos(35) 'Pedidos de cat�logo negociados
                    uwgPedCatNeg.Bands(0).Columns.FromKey("PROVEEDOR").Header.Caption = Textos(33)
                    uwgPedCatNeg.Bands(0).Columns.FromKey("DETALLE").Header.Caption = Textos(30)
                    uwgPedCatNeg.Bands(0).Columns.FromKey("FECEMISION").Header.Caption = Textos(34)
                    uwgPedCatNeg.Bands(0).Columns.FromKey("DEN_ESTADO").Header.Caption = Textos(0)
                    If Me.Acceso.gbOblCodPedDir Then
                        uwgPedCatNeg.Bands(0).Columns.FromKey("NUM_PED_ERP").Header.Caption = sNum_Ped_ERP_Label
                    End If
                Else
                    Me.tblActual.Rows(4).Visible = False
                    uwgPedCatNeg.Visible = False
                End If
            Else
                Me.tblActual.Rows(4).Visible = False
                uwgPedCatNeg.Visible = False
            End If

            'Carga la grid de pedidos libres (EP libres):
            oInstancia.DevolverPedidosRelacionados(, True, bOblCodPedDir:=Me.Acceso.gbOblCodPedDir)
            If oInstancia.Pedidos.Tables.Count > 0 Then
                If oInstancia.Pedidos.Tables(0).Rows.Count > 0 Then
                    bHayProcesosOPedidos = True
                    uwgPedLibres.DataSource = oInstancia.Pedidos
                    uwgPedLibres.DataMember = ""
                    uwgPedLibres.DataBind()

                    uwgPedLibres.Bands(0).Columns.FromKey("PEDIDO").Header.Caption = Textos(36) 'Pedidos libres
                    uwgPedLibres.Bands(0).Columns.FromKey("PROVEEDOR").Header.Caption = Textos(33)
                    uwgPedLibres.Bands(0).Columns.FromKey("DETALLE").Header.Caption = Textos(30)
                    uwgPedLibres.Bands(0).Columns.FromKey("FECEMISION").Header.Caption = Textos(34)
                    uwgPedLibres.Bands(0).Columns.FromKey("DEN_ESTADO").Header.Caption = Textos(0)
                    If Me.Acceso.gbOblCodPedDir Then
                        uwgPedLibres.Bands(0).Columns.FromKey("NUM_PED_ERP").Header.Caption = sNum_Ped_ERP_Label
                    End If
                Else
                    Me.tblActual.Rows(5).Visible = False
                    uwgPedLibres.Visible = False
                End If
            Else
                Me.tblActual.Rows(5).Visible = False
                uwgPedLibres.Visible = False
            End If

            'Carga la grid de pedidos directos (integracion):
            oInstancia.DevolverPedidosRelacionados(2, bOblCodPedDir:=Me.Acceso.gbOblCodPedDir)
            If oInstancia.Pedidos.Tables.Count > 0 Then
                If oInstancia.Pedidos.Tables(0).Rows.Count > 0 Then
                    bHayProcesosOPedidos = True
                    uwgPedDir.DataSource = oInstancia.Pedidos
                    uwgPedDir.DataMember = ""
                    uwgPedDir.DataBind()

                    uwgPedDir.Bands(0).Columns.FromKey("PEDIDO").Header.Caption = Textos(83) 'Pedidos directos
                    uwgPedDir.Bands(0).Columns.FromKey("PROVEEDOR").Header.Caption = Textos(33)
                    uwgPedDir.Bands(0).Columns.FromKey("DETALLE").Header.Caption = Textos(30)
                    uwgPedDir.Bands(0).Columns.FromKey("FECEMISION").Header.Caption = Textos(34)
                    uwgPedDir.Bands(0).Columns.FromKey("DEN_ESTADO").Header.Caption = Textos(0)
                    If Me.Acceso.gbOblCodPedDir Then
                        uwgPedDir.Bands(0).Columns.FromKey("NUM_PED_ERP").Header.Caption = sNum_Ped_ERP_Label
                    End If

                Else
                    Me.tblActual.Rows(6).Visible = False
                        uwgPedDir.Visible = False
                    End If
                Else
                Me.tblActual.Rows(6).Visible = False
                uwgPedDir.Visible = False
            End If

            'Carga la grid de abonos:
            oInstancia.DevolverPedidosRelacionados(, , True, bOblCodPedDir:=Me.Acceso.gbOblCodPedDir)
            If oInstancia.Pedidos.Tables.Count > 0 Then
                If oInstancia.Pedidos.Tables(0).Rows.Count > 0 Then
                    bHayProcesosOPedidos = True
                    uwgPedAbono.DataSource = oInstancia.Pedidos
                    uwgPedAbono.DataMember = ""
                    uwgPedAbono.DataBind()

                    uwgPedAbono.Bands(0).Columns.FromKey("PEDIDO").Header.Caption = Textos(84) 'Abonos
                    uwgPedAbono.Bands(0).Columns.FromKey("PROVEEDOR").Header.Caption = Textos(33)
                    uwgPedAbono.Bands(0).Columns.FromKey("DETALLE").Header.Caption = Textos(30)
                    uwgPedAbono.Bands(0).Columns.FromKey("FECEMISION").Header.Caption = Textos(34)
                    uwgPedAbono.Bands(0).Columns.FromKey("DEN_ESTADO").Header.Caption = Textos(0)
                    If Me.Acceso.gbOblCodPedDir Then
                        uwgPedAbono.Bands(0).Columns.FromKey("NUM_PED_ERP").Header.Caption = sNum_Ped_ERP_Label
                    End If
                Else
                    Me.tblActual.Rows(7).Visible = False
                    uwgPedAbono.Visible = False
                End If
            Else
                Me.tblActual.Rows(7).Visible = False
                uwgPedAbono.Visible = False
            End If

            'si no hab�a ni pedidos ni procesos vinculados a la solicitud no se muestra esa fila de la tabla:
            If tblActual.Rows(1).Visible = False And tblActual.Rows(2).Visible = False And tblActual.Rows(3).Visible = False And tblActual.Rows(4).Visible = False And tblActual.Rows(5).Visible = False And tblActual.Rows(6).Visible = False And tblActual.Rows(7).Visible = False Then
                Me.tblActual.Visible = False
            End If

            If Not bHayProcesosOPedidos Then
                'No muestra las tablas de procesos y pedidos:
                Me.tblActual.Visible = False
            End If

        End If
    End Sub

    Private Sub uwgHistorico_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwgHistorico.InitializeLayout
        'Hace invisibles algunas columnas
        Me.uwgHistorico.Bands(0).Columns.FromKey("BLOQUE1").Hidden = True
        Me.uwgHistorico.Bands(0).Columns.FromKey("BLOQUE2").Hidden = True
        Me.uwgHistorico.Bands(0).Columns.FromKey("PER").Hidden = True
        Me.uwgHistorico.Bands(0).Columns.FromKey("PROVE").Hidden = True
        Me.uwgHistorico.Bands(0).Columns.FromKey("ESTADO").Hidden = True
        Me.uwgHistorico.Bands(0).Columns.FromKey("DEN_BLOQUE2").Hidden = True
        Me.uwgHistorico.Bands(0).Columns.FromKey("ID_OTRA_ACCION").Hidden = True
        Me.uwgHistorico.Bands(0).Columns.FromKey("DESTINATARIO").Hidden = True
        Me.uwgHistorico.Bands(0).Columns.FromKey("ESTADO").Hidden = True
        Me.uwgHistorico.Bands(0).Columns.FromKey("ETAPAS_PARALELO").Hidden = True
        Me.uwgHistorico.Bands(0).Columns.FromKey("ID").Hidden = True
        Me.uwgHistorico.Bands(0).Columns.FromKey("CONSULTA").Hidden = True

        If m_bVerDetallePer = False Then
            Me.uwgHistorico.Bands(0).Columns.FromKey("USUARIO").Hidden = True
        End If

        uwgHistorico.Bands(0).Columns.Add("DEN_DESTINO")
        uwgHistorico.Bands(0).Columns.FromKey("DEN_DESTINO").Move(9)

        'formato de la fecha:
        uwgHistorico.Bands(0).Columns.FromKey("FECHA").Format = FSNUser.DateFormat.ShortDatePattern & " " & FSNUser.DateFormat.ShortTimePattern

        'Redimensiona las columnas:
        uwgHistorico.Bands(0).Columns.FromKey("DEN_BLOQUE1").Width = Unit.Percentage(15)
        uwgHistorico.Bands(0).Columns.FromKey("ROL").Width = Unit.Percentage(10)
        uwgHistorico.Bands(0).Columns.FromKey("USUARIO").Width = Unit.Percentage(15)
        uwgHistorico.Bands(0).Columns.FromKey("ACCION").Width = Unit.Percentage(10)
        uwgHistorico.Bands(0).Columns.FromKey("FECHA").Width = Unit.Percentage(10)
        uwgHistorico.Bands(0).Columns.FromKey("DEN_BLOQUE2").Width = Unit.Percentage(15)
        uwgHistorico.Bands(0).Columns.FromKey("COMENT").Width = Unit.Percentage(15)

        uwgHistorico.DisplayLayout.RowSelectorsDefault = Infragistics.WebUI.UltraWebGrid.RowSelectors.No
        uwgHistorico.DisplayLayout.HeaderStyleDefault.HorizontalAlign = HorizontalAlign.Left
    End Sub

    ''' <summary>
    ''' Se lanza por cada fila del ultrawebgrid. Aqui definimos los estilos de cada fila, el texto que va en una celda determinada, si le ponermos hiperv�nculo,..
    ''' </summary>
    ''' <param name="sender">uwghistorico</param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub uwgHistorico_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgHistorico.InitializeRow

        If e.Row.Cells.FromKey("ETAPAS_PARALELO").Value > 1 Then
            e.Row.Cells.FromKey("DEN_DESTINO").Value = m_sIdiParalelas
        Else
            e.Row.Cells.FromKey("DEN_DESTINO").Value = e.Row.Cells.FromKey("DEN_BLOQUE2").Value
        End If

        Select Case e.Row.Cells.FromKey("ID_OTRA_ACCION").Value
            Case TipoAccionSolicitud.Traslado
                e.Row.Cells.FromKey("ACCION").Value = m_sIdiAcciones(1)
                e.Row.Cells.FromKey("DEN_DESTINO").Value = e.Row.Cells.FromKey("DESTINATARIO").Value
            Case TipoAccionSolicitud.Devolucion
                e.Row.Cells.FromKey("ACCION").Value = m_sIdiAcciones(2)
                e.Row.Cells.FromKey("DEN_DESTINO").Value = e.Row.Cells.FromKey("DESTINATARIO").Value
            Case TipoAccionSolicitud.Reemision
                e.Row.Cells.FromKey("DEN_ACCION").Value = m_sIdiAcciones(8)
                e.Row.Cells.FromKey("DEN_ACCION").Style.CssClass = "fntRed"
        End Select

        'Estilos de las filas:
        If e.Row.Cells.FromKey("ESTADO").Value = 1 Then
            e.Row.Style.CssClass = "ugfilatablaHistActual"
        ElseIf e.Row.Cells.FromKey("ESTADO").Value > 1 Then
            'Si ya est� tramitada o rechazada la fila se ver� en azul, sino en blanco:
            Select Case e.Row.Cells.FromKey("ESTADO").Value
                Case 3
                    e.Row.Cells.FromKey("ACCION").Value = Textos(85)  'Flujo de modificacion
                Case 4
                    e.Row.Cells.FromKey("ACCION").Value = Textos(86)  'Flujo de baja
                Case 101
                    e.Row.Cells.FromKey("ACCION").Value = Textos(77) 'Aprobar
                Case 102
                    e.Row.Cells.FromKey("ACCION").Value = Textos(78)  'Rechazar
                Case 103
                    e.Row.Cells.FromKey("ACCION").Value = Textos(79)   'Anular
                Case 104
                    e.Row.Cells.FromKey("ACCION").Value = Textos(80)  'Cerrar
            End Select
        End If

        'La columna de el comentario ser� un hiperv�nculo:
        If Len(e.Row.Cells.FromKey("COMENT").Value) > 25 Then
            e.Row.Cells.FromKey("COMENT").Value = Left(e.Row.Cells.FromKey("COMENT").Value, 25)
        End If
        If DBNullToSomething(e.Row.Cells.FromKey("COMENT").Value) <> Nothing Then
            e.Row.Cells.FromKey("COMENT").Value = "<a class='aPMWeb' onclick=""VerComentario('" & e.Row.Cells.FromKey("ID").Value & "');return false;"">" & e.Row.Cells.FromKey("COMENT").Value & "</a>"
            e.Row.Cells.FromKey("COMENT").Style.CssClass = "TablaLink"
        End If

        If DBNullToSomething(e.Row.Cells.FromKey("PER").Value) <> Nothing Then
            e.Row.Cells.FromKey("USUARIO").Style.CssClass = "TablaLink"
            e.Row.Cells.FromKey("USUARIO").Value = "<a class='aPMWeb' onclick=""VerDetallePersona('" & e.Row.Cells.FromKey("PER").Value & "');return false;"">" & e.Row.Cells.FromKey("USUARIO").Value & "</a>"
        ElseIf DBNullToSomething(e.Row.Cells.FromKey("PROVE").Value) <> Nothing Then
            e.Row.Cells.FromKey("USUARIO").Style.CssClass = "TablaLink"
            e.Row.Cells.FromKey("USUARIO").Value = "<a class='aPMWeb' onclick=""VerDetalleProveedor('" & e.Row.Cells.FromKey("PROVE").Value & "');return false;"">" & e.Row.Cells.FromKey("USUARIO").Value & "</a>"
        ElseIf e.Row.Cells.FromKey("ESTADO").Value > 1 Then
            e.Row.Cells.FromKey("USUARIO").Value = Textos(81)  ''"Administrador"
        End If

    End Sub

    Private Sub uwgProcesos_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwgProcesos.InitializeLayout
        'Hace invisibles algunas columnas
        uwgProcesos.Bands(0).Columns.FromKey("EST").Hidden = True
        uwgProcesos.Bands(0).Columns.FromKey("ADJDIR").Hidden = True
        uwgProcesos.Bands(0).Columns.FromKey("FECPRES").Hidden = True
        uwgProcesos.Bands(0).Columns.FromKey("COM").Hidden = True
        uwgProcesos.Bands(0).Columns.FromKey("ANYO").Hidden = True
        uwgProcesos.Bands(0).Columns.FromKey("GMN1").Hidden = True
        uwgProcesos.Bands(0).Columns.FromKey("COD").Hidden = True

        uwgProcesos.Bands(0).Columns.Add("PROCE")
        uwgProcesos.Bands(0).Columns.Add("DETALLE")
        uwgProcesos.Bands(0).Columns.Add("DEN_ESTADO")
        uwgProcesos.Bands(0).Columns.FromKey("PROCE").Move(0)
        uwgProcesos.Bands(0).Columns.FromKey("DETALLE").Move(5)
        uwgProcesos.Bands(0).Columns.FromKey("DEN_ESTADO").Move(7)

        'formato de la fecha:
        uwgProcesos.Bands(0).Columns.FromKey("FECNEC").Format = FSNUser.DateFormat.ShortDatePattern

        'Redimensiona las columnas:
        uwgProcesos.Bands(0).Columns.FromKey("PROCE").Width = Unit.Percentage(15)
        uwgProcesos.Bands(0).Columns.FromKey("DEN").Width = Unit.Percentage(20)
        uwgProcesos.Bands(0).Columns.FromKey("DETALLE").Width = Unit.Percentage(5)
        uwgProcesos.Bands(0).Columns.FromKey("FECNEC").Width = Unit.Percentage(10)
        uwgProcesos.Bands(0).Columns.FromKey("DEN_ESTADO").Width = Unit.Percentage(20)

        uwgProcesos.DisplayLayout.Bands(0).Columns.FromKey("DETALLE").CellStyle.HorizontalAlign = HorizontalAlign.Center
        uwgProcesos.DisplayLayout.Bands(0).Columns.FromKey("DETALLE").CellStyle.Cursor = Infragistics.WebUI.[Shared].Cursors.Hand

        uwgProcesos.DisplayLayout.RowSelectorsDefault = Infragistics.WebUI.UltraWebGrid.RowSelectors.No
    End Sub

    Private Sub uwgProcesos_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgProcesos.InitializeRow

        e.Row.Cells.FromKey("PROCE").Value = e.Row.Cells.FromKey("ANYO").Value & "/" & e.Row.Cells.FromKey("GMN1").Value & "/" & e.Row.Cells.FromKey("COD").Value

        Select Case e.Row.Cells.FromKey("EST").Value
            Case TipoEstadoProceso.sinitems, TipoEstadoProceso.ConItemsSinValidar
                e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoProceso(1)
            Case TipoEstadoProceso.validado, TipoEstadoProceso.Conproveasignados
                e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoProceso(2)
            Case TipoEstadoProceso.conasignacionvalida
                e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoProceso(3)
            Case TipoEstadoProceso.conpeticiones, TipoEstadoProceso.conofertas
                e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoProceso(4)
            Case TipoEstadoProceso.ConObjetivosSinNotificar, TipoEstadoProceso.ConObjetivosSinNotificarYPreadjudicado
                e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoProceso(5)
            Case TipoEstadoProceso.PreadjYConObjNotificados
                e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoProceso(6)
            Case TipoEstadoProceso.ParcialmenteCerrado
                e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoProceso(7)
            Case TipoEstadoProceso.conadjudicaciones
                e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoProceso(8)
            Case TipoEstadoProceso.ConAdjudicacionesNotificadas
                e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoProceso(9)
            Case TipoEstadoProceso.Cerrado
                e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoProceso(10)
        End Select

        e.Row.Cells.FromKey("DETALLE").Value = "<img border=""0"" align=""center"" src=""images/buscar.gif"" onclick=""VerDetalleProceso('" & e.Row.Cells.FromKey("ANYO").Value & "','" & e.Row.Cells.FromKey("GMN1").Value & "','" & e.Row.Cells.FromKey("COD").Value & "');return false;"">"
    End Sub

    ''' Revisado por: blp. Fecha: 18/07/2013
    ''' <summary>
    ''' Inicializar el grid
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. Max. 0,2 seg.</remarks>    
    Private Sub uwgPedDir_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwgPedDir.InitializeLayout
        InitializeLayout(uwgPedDir)
    End Sub

    ''' Revisado por: blp. Fecha: 18/07/2013
    ''' <summary>
    ''' Inicializar cada fila del grid
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. Max. 0,1 seg.</remarks>
    Private Sub uwgPedDir_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgPedDir.InitializeRow
        InitializeRow(e)
    End Sub

    ''' Revisado por: blp. Fecha: 18/07/2013
    ''' <summary>
    ''' Inicializar cada fila del grid
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. Max. 0,1 seg.</remarks>
    Private Sub uwgPedNeg_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgPedNeg.InitializeRow
        InitializeRow(e)
    End Sub

    ''' Revisado por: blp. Fecha: 18/07/2013
    ''' <summary>
    ''' Inicializar el grid
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. Max. 0,2 seg.</remarks>    
    Private Sub uwgPedNeg_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwgPedNeg.InitializeLayout
        InitializeLayout(uwgPedNeg)
    End Sub

    ''' Revisado por: blp. Fecha: 18/07/2013
    ''' <summary>
    ''' Inicializar cada fila del grid
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. Max. 0,1 seg.</remarks>
    Private Sub uwgPedCatNeg_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgPedCatNeg.InitializeRow
        InitializeRow(e)
    End Sub

    ''' Revisado por: blp. Fecha: 18/07/2013
    ''' <summary>
    ''' Inicializar el grid
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. Max. 0,2 seg.</remarks>    
    Private Sub uwgPedCatNeg_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwgPedCatNeg.InitializeLayout
        InitializeLayout(uwgPedCatNeg)
    End Sub

    ''' Revisado por: blp. Fecha: 18/07/2013
    ''' <summary>
    ''' Inicializar cada fila del grid
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. Max. 0,1 seg.</remarks>
    Private Sub uwgPedLibres_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgPedLibres.InitializeRow
        InitializeRow(e)
    End Sub

    ''' Revisado por: blp. Fecha: 18/07/2013
    ''' <summary>
    ''' Inicializar el grid
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. Max. 0,2 seg.</remarks>    
    Private Sub uwgPedLibres_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwgPedLibres.InitializeLayout
        InitializeLayout(uwgPedLibres)
    End Sub

    ''' Revisado por: blp. Fecha: 18/07/2013
    ''' <summary>
    ''' Inicializar cada fila del grid
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. Max. 0,1 seg.</remarks>
    Private Sub uwgPedAbono_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgPedAbono.InitializeRow
        InitializeRow(e)
    End Sub

    ''' Revisado por: blp. Fecha: 18/07/2013
    ''' <summary>
    ''' Inicializar el grid
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. Max. 0,2 seg.</remarks>    
    Private Sub uwgPedAbono_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwgPedAbono.InitializeLayout
        InitializeLayout(uwgPedAbono)
    End Sub

    Private Sub uwgLineasCat_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwgLineasCat.InitializeLayout
        'Hace invisibles algunas columnas:
        uwgLineasCat.Bands(0).Columns.FromKey("CODART").Hidden = True
        uwgLineasCat.Bands(0).Columns.FromKey("DENART").Hidden = True

        'a�ade las columnas de art�culo y l�nea:
        uwgLineasCat.Bands(0).Columns.Add("ARTICULO")
        uwgLineasCat.Bands(0).Columns.Add("DETALLE")
        uwgLineasCat.Bands(0).Columns.FromKey("ARTICULO").Move(1)
        uwgLineasCat.Bands(0).Columns.FromKey("DETALLE").Move(2)

        'Redimensiona las columnas:
        uwgLineasCat.Bands(0).Columns.FromKey("CAT").Width = Unit.Percentage(15)
        uwgLineasCat.Bands(0).Columns.FromKey("ARTICULO").Width = Unit.Percentage(20)
        uwgLineasCat.Bands(0).Columns.FromKey("DETALLE").Width = Unit.Percentage(5)
        uwgLineasCat.Bands(0).Columns.FromKey("PROVE").Width = Unit.Percentage(10)
        uwgLineasCat.Bands(0).Columns.FromKey("PROVEDEN").Width = Unit.Percentage(20)

        uwgLineasCat.DisplayLayout.RowSelectorsDefault = Infragistics.WebUI.UltraWebGrid.RowSelectors.No
    End Sub

    Private Sub uwgLineasCat_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgLineasCat.InitializeRow
        e.Row.Cells.FromKey("ARTICULO").Value = e.Row.Cells.FromKey("CODART").Value & " - " & e.Row.Cells.FromKey("DENART").Value
    End Sub

    ''' Revisado por: blp. Fecha: 18/07/2013
    ''' <summary>
    ''' Inicializa la grid que se pasa como par�metro.
    ''' Se definen datos de visibilidad de columnas y otras opciones de la grid
    ''' </summary>
    ''' <param name="uwg">Grid a inicializar.</param>
    ''' <remarks>Llamada desde NWcomentariossolic.aspx.vb. Maximo 0,2 seg.</remarks>
    Private Sub InitializeLayout(ByRef uwg As Infragistics.WebUI.UltraWebGrid.UltraWebGrid)
        'Hace invisibles algunas columnas
        uwg.Bands(0).Columns.FromKey("ID").Hidden = True
        uwg.Bands(0).Columns.FromKey("ANYO").Hidden = True
        uwg.Bands(0).Columns.FromKey("PNUM").Hidden = True
        uwg.Bands(0).Columns.FromKey("TIPO").Hidden = True
        uwg.Bands(0).Columns.FromKey("ONUM").Hidden = True
        uwg.Bands(0).Columns.FromKey("FECHA").Hidden = True
        uwg.Bands(0).Columns.FromKey("EST").Hidden = True
        uwg.Bands(0).Columns.FromKey("NUMEXT").Hidden = True
        uwg.Bands(0).Columns.FromKey("FECHA1").Hidden = True
        uwg.Bands(0).Columns.FromKey("PROVE").Hidden = True
        uwg.Bands(0).Columns.FromKey("DEN").Hidden = True
        uwg.Bands(0).Columns.FromKey("BAJA_LOG").Hidden = True
        uwg.Bands(0).Columns.FromKey("FEC_BAJA_LOG").Hidden = True

        uwg.Bands(0).Columns.Add("PEDIDO")
        uwg.Bands(0).Columns.Add("PROVEEDOR")
        uwg.Bands(0).Columns.Add("DETALLE")
        uwg.Bands(0).Columns.Add("DEN_ESTADO")
        uwg.Bands(0).Columns.FromKey("PEDIDO").Move(0)
        uwg.Bands(0).Columns.FromKey("PROVEEDOR").Move(8)
        uwg.Bands(0).Columns.FromKey("DETALLE").Move(9)
        uwg.Bands(0).Columns.FromKey("DEN_ESTADO").Move(15)
        If Me.Acceso.gbOblCodPedDir Then
            uwg.Bands(0).Columns.FromKey("NUM_PED_ERP").Move(16)
        Else
            uwg.Bands(0).Columns.FromKey("NUM_PED_ERP").Hidden = True
        End If

        'formato de la fecha:
        uwg.Bands(0).Columns.FromKey("FECEMISION").Format = FSNUser.DateFormat.ShortDatePattern

        'Redimensiona las columnas:
        uwg.Bands(0).Columns.FromKey("PEDIDO").Width = Unit.Percentage(15)
        uwg.Bands(0).Columns.FromKey("PROVEEDOR").Width = Unit.Percentage(20)
        uwg.Bands(0).Columns.FromKey("DETALLE").Width = Unit.Percentage(5)
        uwg.Bands(0).Columns.FromKey("FECEMISION").Width = Unit.Percentage(10)
        uwg.Bands(0).Columns.FromKey("DEN_ESTADO").Width = Unit.Percentage(20)
        If Me.Acceso.gbOblCodPedDir Then
            uwg.Bands(0).Columns.FromKey("NUM_PED_ERP").Width = Unit.Percentage(15)
        End If

        uwg.DisplayLayout.Bands(0).Columns.FromKey("DETALLE").CellStyle.HorizontalAlign = HorizontalAlign.Center
        uwg.DisplayLayout.Bands(0).Columns.FromKey("DETALLE").CellStyle.Cursor = Infragistics.WebUI.[Shared].Cursors.Hand

        uwg.DisplayLayout.RowSelectorsDefault = Infragistics.WebUI.UltraWebGrid.RowSelectors.No
    End Sub

    ''' Revisado por: blp. Fecha: 18/07/2013
    ''' <summary>
    ''' Inicializa la fila del grid que se pasa como par�metro.
    ''' </summary>
    ''' <remarks>Llamada desde NWcomentariossolic.aspx.vb. Maximo 0,2 seg.</remarks>
    Private Sub InitializeRow(ByRef e As Infragistics.WebUI.UltraWebGrid.RowEventArgs)
        e.Row.Cells.FromKey("PEDIDO").Value = e.Row.Cells.FromKey("ANYO").Value & "/" & e.Row.Cells.FromKey("PNUM").Value & "/" & e.Row.Cells.FromKey("ONUM").Value
        e.Row.Cells.FromKey("PROVEEDOR").Value = e.Row.Cells.FromKey("PROVE").Value & " - " & e.Row.Cells.FromKey("DEN").Value
        If e.Row.Cells.FromKey("BAJA_LOG").Value = 0 Then
            Select Case e.Row.Cells.FromKey("EST").Value
                Case TipoEstadoOrdenEntrega.PendienteDeAprobacion
                    e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoPed(1)
                Case TipoEstadoOrdenEntrega.DenegadoParcialAprob
                    e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoPed(2)
                Case TipoEstadoOrdenEntrega.EmitidoAlProveedor
                    e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoPed(3)
                Case TipoEstadoOrdenEntrega.AceptadoPorProveedor
                    e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoPed(4)
                Case TipoEstadoOrdenEntrega.EnCamino
                    e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoPed(5)
                Case TipoEstadoOrdenEntrega.EnRecepcion
                    e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoPed(6)
                Case TipoEstadoOrdenEntrega.RecibidoYCerrado
                    e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoPed(7)
                Case TipoEstadoOrdenEntrega.Anulado
                    e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoPed(8)
                Case TipoEstadoOrdenEntrega.RechazadoPorProveedor
                    e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoPed(9)
                Case TipoEstadoOrdenEntrega.DenegadoTotalAprobador
                    e.Row.Cells.FromKey("DEN_ESTADO").Value = m_sEstadoPed(10)
            End Select
        Else
            e.Row.Cells.FromKey("DEN_ESTADO").Value = Textos(88)
            e.Row.Style.CssClass = "fntRed"
        End If


        e.Row.Cells.FromKey("DETALLE").Value = "<img border=""0"" align=""center"" src=""images/buscar.gif"" onclick=""VerDetallePedido('" & e.Row.Cells.FromKey("ID").Value & "');return false;"">"
    End Sub
End Class

