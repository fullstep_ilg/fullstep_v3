﻿Partial Public Class altaContrato
    Inherits FSNPage

    Private Const IncludeScriptFormat As String = ControlChars.CrLf & "<script type=""{0}"" src=""{1}""></script>"
    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"

    Private oBloque As FSNServer.Bloque
    Private _oSolicitud As FSNServer.Solicitud
    Private sTextos(10) As String
    Dim lIdAdjuntoContrato As Long
    Dim lTamanyoAdjuntoContrato As Long
    Dim arrAdjuntoContrato(1) As Long

    ''' <summary>
    ''' Obtenemos los datos de la solicitud
    ''' A continuacion lo metemos en la cache
    ''' </summary>
    ''' <remarks></remarks>
    Protected ReadOnly Property oSolicitud() As FSNServer.Solicitud
        Get
            If _oSolicitud Is Nothing Then
                If Me.IsPostBack Then
                    _oSolicitud = CType(Cache("oSolicitud" & FSNUser.Cod), FSNServer.Solicitud)
                Else
                    _oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
                    _oSolicitud.ID = Request("Solicitud")

                    'Dim lIdContratoImportar As Long
                    'If Not Request("IDContratoImportar") Is Nothing Then lIdContratoImportar = Request("IdContratoImportar")
                    IdInstanciaImportar.Value = ContratoImportar


                    'Carga los datos de la instancia:
                    _oSolicitud.Load(Idioma, True)
                    _oSolicitud.Formulario.Load(Idioma, _oSolicitud.ID, _oSolicitud.BloqueActual, FSNUser.CodPersona, , , FSNUser.DateFmt,, ContratoImportar)

                    Me.InsertarEnCache("oSolicitud" & FSNUser.Cod, _oSolicitud)

                End If
            End If
            Return _oSolicitud
        End Get
    End Property
    Private _oRol As FSNServer.Rol
    ''' <summary>
    ''' Obtenemos los datos del rol
    ''' A continuacion lo metemos en la cache
    ''' </summary>
    ''' <remarks></remarks>
    Protected ReadOnly Property oRol() As FSNServer.Rol
        Get
            If _oRol Is Nothing Then
                If Me.IsPostBack Then
                    _oRol = CType(Cache("oRol" & FSNUser.Cod), FSNServer.Rol)
                Else
                    For Each _oRol In oBloque.Roles.Roles
                        If _oRol.Participantes Is Nothing Then
                            If _oRol.ExistePersona(FSNUser.UON1, FSNUser.UON2, FSNUser.UON3, FSNUser.Dep, FSNUser.CodPersona) Then Exit For
                        Else
                            If _oRol.Persona = FSNUser.CodPersona Then Exit For
                        End If
                    Next

                    Me.InsertarEnCache("oRol" & FSNUser.Cod, _oRol)
                End If
            End If
            Return _oRol
        End Get
    End Property

    Public ReadOnly Property ContratoImportar() As Long
        Get
            If Not Request("IdInstanciaImportar") Is Nothing Then
                Return Request("IdInstanciaImportar")
            Else
                Return 0
            End If
        End Get
    End Property

    ''' Revisado por: blp. Fecha: 05/10/2011
    ''' <summary>
    ''' Carga la pagina de contratos
    ''' </summary>
    ''' <param name="sender">Página</param>
    ''' <param name="e">argumentos del evento load</param>
    ''' <remarks>Llamada desde el evento Load de la página. Máx inferior a 1 seg </remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Expires = -1
        imgCollapseAlertas.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Theme & "/images/contraer_color.gif"
        imgExpandAlertas.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Theme & "/images/expandir_color.gif"
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AltaContrato

        If IsPostBack Then
            ConfigurarFormulario()
            Exit Sub
        End If

        hid_Proveedor.Value = Request("Proveedor")
        hid_ProcesoContrato.Value = Request("ProcesoContrato")
        hid_PathContrato.Value = Request("PathContrato")
        hid_NombreContrato.Value = Request("NombreContrato")
        hid_GruposContrato.Value = Request("GruposContrato")
        hid_ItemsContrato.Value = Request("ItemsContrato")
        hidEmpresa.Value = Request("Empresa")
        hid_DesdeInicio.Value = Request("DesdeInicio")

        'Si viene desde el Wizard de GS recojo el id del Adjunto de contrato que se ha grabado en Gs en PM_COPIA_ADJUN
        If Request("DesdeWizardGS") = 1 Then
            lIdAdjuntoContrato = Request("idAdjuntoContrato")
            lTamanyoAdjuntoContrato = Request("SizeAdjuntoContrato")
        End If

        CargarIdiomas()

        If Not IsPostBack Then
            ConfigurarCabeceraMenu()
            ConfigurarCabeceraContrato()
            CargarNotificados(0, "")

            CargarContratoImportar

        End If

        ConfigurarFormulario()
        GenerarScript()

        imgBuscarNotificados.Attributes.Add("onclick", "BuscarNotificadoInterno();return false;")
        imgCollapseAlertas.Attributes.Add("onclick", "OcultarAlertas();return false;")
        imgExpandAlertas.Attributes.Add("onclick", "OcultarAlertas();return false;")
        lblConfiguracionAlertas.Attributes.Add("onclick", "OcultarAlertas();return false;")
        imgInfProv.ImageUrl = "./images/info.gif"
        imgInfProv.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosProveedor.AnimationClientID & "', event, '" & FSNPanelDatosProveedor.DynamicPopulateClientID & "', '" & hid_Proveedor.Value & "'); return false;")
        imgInfEmpresa.ImageUrl = "./images/info.gif"
        imgInfEmpresa.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosEmpresa.AnimationClientID & "', event, '" & FSNPanelDatosEmpresa.DynamicPopulateClientID & "', '" & hidEmpresa.Value & "'); return false;")
        imgEmpresaLupa.Attributes.Add("onClick", "var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorEmpresas.aspx?desde=altaContrato&Solicitud=" & Request.QueryString("Solicitud") & "&Rol=" & oRol.Id & "&ProcesoContrato=" & Request.QueryString("ProcesoContrato") & "','_blank','width=800,height=600, location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no');return false;")
        ImgProveedorLupa.OnClientClick = "var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorProveedores.aspx?PM=true&Contr=true', '_blank', 'width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes');return false;"

        PantallaVinculaciones.Value = oSolicitud.ExisteVinculaciones

        If Not ClientScript.IsStartupScriptRegistered("oRol_Id") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "oRol_Id", "var oRol_Id=" & oRol.Id & ";", True)

        Me.FindControl("frmAlta").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "parametrosEntradaServicio") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "parametrosEntradaServicio",
                "var fechaInicioEntrada='" & fsentryFecInicio.ClientID & "';" &
                "var fechaExpiracionEntrada='" & fsentryFecFin.ClientID & "';" &
                "var proveedorEntrada='" & hid_Proveedor.ClientID & "';" &
                "var empresaEntrada='" & hidEmpresa.ClientID & "';" &
                "var alertaEntrada='" & txtAlerta.ClientID & "';" &
                "var periodoEmail='" & txtPeriodoEmail.ClientID & "';", True)
        End If

    End Sub

    ''' Revisado por: blp. Fecha: 05/10/2011
    ''' <summary>
    ''' Configura la cabecera, muestra en el menu las posibles acciones
    ''' Activamos los botones que pueden aparecer en la pantalla
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load; Tiempo máximo:=0,1seg.</remarks>
    Private Sub ConfigurarCabeceraMenu()
        If Not String.IsNullOrEmpty(Request("ConfiguracionGS")) Then
            'Estilo de la pagina
            Menu1.Visible = False
            my_body.Attributes.Add("class", "fondoGS")
            FSNPageHeader.AspectoGS = True
            FSNPageHeader.TituloCabecera = Replace(hid_NombreContrato.Value, ".doc", "")
            FSNPageHeader.VisibleBotonVolver = False
        Else
            FSNPageHeader.TituloCabecera = Request("NomContrato")
            FSNPageHeader.AspectoGS = False
            FSNPageHeader.VisibleBotonVolver = True
        End If
        my_body.Attributes.Add("onload", "iniciar();")
        hidConfiguracionGS.Value = IIf(Request("ConfiguracionGS") = "1", "1", "0")

        FSNPageHeader.VisibleBotonGuardar = True
        FSNPageHeader.VisibleBotonImpExp = True
        FSNPageHeader.OcultarBotonImpExp = True
        FSNPageHeader.VisibleBotonEliminar = True
        FSNPageHeader.OcultarBotonEliminar = True

        FSNPageHeader.TextoBotonCalcular = sTextos(4)
        FSNPageHeader.TextoBotonGuardar = sTextos(6)
        FSNPageHeader.TextoBotonImpExp = sTextos(7)
        FSNPageHeader.TextoBotonVolver = sTextos(8)
        FSNPageHeader.TextoBotonEliminar = sTextos(9)

        FSNPageHeader.UrlImagenCabecera = "images/AltaContrato.png"
        FSNPageHeader.OnClientClickCalcular = "return CalcularCamposCalculados()"
        FSNPageHeader.OnClientClickGuardar = "return Guardar();"
        FSNPageHeader.OnClientClickImpExp = "return cmdImpExp_onclick();"
        FSNPageHeader.OnClientClickVolver = "javascript:HistoryHaciaAtras();return false;"
        FSNPageHeader.OnClientClickEliminar = "return EliminarInstancia();"


        FSNPageHeader.TextoBotonImportar = sTextos(10)
        FSNPageHeader.VisibleBotonImportar = True
        FSNPageHeader.OnClientClickImportar = "return ImportarContrato()"

        'SI EXISTE ALGUN CAMPO DE TIPO=3 (CALCULADO) HACEMOS VISIBLE EL BOTON DE CALCULAR
        FSNPageHeader.VisibleBotonCalcular = oSolicitud.Formulario.Grupos.Grupos.OfType(Of FSNServer.Grupo).Where(Function(x) x.DSCampos.Tables(0).Rows.OfType(Of DataRow).Where(Function(y) y("TIPO") = TipoCampoPredefinido.Calculado).Any).Any()
        If FSNPageHeader.VisibleBotonCalcular Then BotonCalcular.Value = 1

        'Cargar las acciones
        oBloque = FSNServer.Get_Object(GetType(FSNServer.Bloque))
        oBloque.CargarRolesBloque(oSolicitud.BloqueActual, FSNUser.CodPersona)

        Dim oDSAcciones As DataSet = oRol.CargarAcciones(Idioma)
        Dim oPopMenu As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
        Dim oItem As Infragistics.WebUI.UltraWebNavigator.Item

        oPopMenu = uwPopUpAcciones
        oPopMenu.Visible = False
        If oDSAcciones.Tables.Count > 0 Then
            If oDSAcciones.Tables(0).Rows.Count > 2 Then
                oPopMenu.Visible = True
                FSNPageHeader.VisibleBotonAccion1 = True
                FSNPageHeader.TextoBotonAccion1 = sTextos(5) ' "Otras acciones"
                FSNPageHeader.OnClientClickAccion1 = "return mostrarMenuAcciones(event,1);"

                For Each oRow In oDSAcciones.Tables(0).Rows
                    If IsDBNull(oRow.Item("DEN")) Then
                        oItem = oPopMenu.Items.Add("&nbsp;")
                    Else
                        If oRow.Item("DEN") = "" Then
                            oItem = oPopMenu.Items.Add("&nbsp;")
                        Else
                            oItem = oPopMenu.Items.Add(DBNullToStr(oRow.Item("DEN")))
                        End If
                    End If
                    oItem.TargetUrl = "javascript:EjecutarAccion(" + oRow.Item("ACCION").ToString() + "," + oBloque.Id.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "')"
                Next
            Else
                Dim cont As Byte = 0
                For Each oRow In oDSAcciones.Tables(0).Rows
                    If cont = 0 Then
                        FSNPageHeader.VisibleBotonAccion1 = True
                        FSNPageHeader.TextoBotonAccion1 = DBNullToStr(oRow.Item("DEN"))
                        FSNPageHeader.OnClientClickAccion1 = "javascript:EjecutarAccion(" + oRow.Item("ACCION").ToString() + "," + oBloque.Id.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"

                        cont = 1
                    Else
                        FSNPageHeader.VisibleBotonAccion2 = True
                        FSNPageHeader.TextoBotonAccion2 = DBNullToStr(oRow.Item("DEN"))
                        FSNPageHeader.OnClientClickAccion2 = "javascript:EjecutarAccion(" + oRow.Item("ACCION").ToString() + "," + oBloque.Id.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                    End If
                Next
            End If
        End If
    End Sub
    ''' <summary>
    ''' Carga los campos relacionados con el contrato 
    ''' Nombre proveedor, contactos del proveedor, empresa (nombre), monedas...
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0,6seg</remarks>
    Private Sub ConfigurarCabeceraContrato()
        If FSNServer.TipoAcceso.gbAccesoLCX AndAlso Not Trim(LCase(FSNUser.PerfilLCX)).Equals(Trim(LCase("auditor"))) AndAlso Not Trim(LCase(FSNUser.PerfilLCX)).Equals(Trim(LCase("compras"))) Then
            Dim oEmpresa As FSNServer.Empresa
            oEmpresa = FSNServer.Get_Object(GetType(FSNServer.Empresa))
            oEmpresa.ID = FSNUser.EmpresaLCX
            oEmpresa.Load(FSNUser.Idioma)
            lblEmpresa.Text = oEmpresa.Den
            txtEmpresa.Visible = False
            imgEmpresaLupa.Visible = False
            hidEmpresa.Value = oEmpresa.ID ' añadido para el detalle
        Else 'Empresas  Como AccesoLCX es false cargo empresa como antiguamente
            Dim oEmpresas As FSNServer.Empresas
            Dim dtEmpresas As DataTable
            Dim bEmpresaObtenida As Boolean

            oEmpresas = FSNServer.Get_Object(GetType(FSNServer.Empresas))
            'Si viene de GS, miramos si el proceso tiene más de una empresa
            If hidEmpresa.Value <> "" AndAlso hidEmpresa.Value <> "0" AndAlso hid_ProcesoContrato.Value <> Nothing Then 'viene de GS
                Dim ProcesoContrato() As String = Split(hid_ProcesoContrato.Value, "_")
                dtEmpresas = oEmpresas.ObtenerEmpresas(Idioma, , , , , , , , , , , , ProcesoContrato(0), ProcesoContrato(1), ProcesoContrato(2), FSNUser.Pyme)
                If Not dtEmpresas Is Nothing Then
                    If dtEmpresas.Rows.Count > 0 Then bEmpresaObtenida = True
                    If dtEmpresas.Rows.Count = 1 Then
                        hidEmpresa.Value = dtEmpresas.Rows(0).Item("ID")
                        lblEmpresa.Text = dtEmpresas.Rows(0).Item("DEN")
                        txtEmpresa.Visible = False
                        imgEmpresaLupa.Visible = False
                    ElseIf dtEmpresas.Rows.Count > 1 Then
                        For i = 0 To dtEmpresas.Rows.Count - 1
                            If dtEmpresas.Rows(i).Item("ID").ToString = hidEmpresa.Value Then
                                txtEmpresa.Text = dtEmpresas.Rows(i).Item("DEN")
                                Exit For
                            End If
                        Next
                        imgInfEmpresa.Visible = False
                        lblEmpresa.Visible = False
                        txtEmpresa_AutoCompleteExtender.ContextKey = hid_ProcesoContrato.Value
                    End If
                End If
            End If

            'Puede que aunque venga desde GS, el proceso no está distribuido y no hayamos encontrado empresas en la búsqueda anterior
            If Not bEmpresaObtenida Then
                'miramos si el rol sólo tiene una o varias empresas para cargarla por defecto
                dtEmpresas = oEmpresas.ObtenerEmpresas(Idioma, , , , , , , , Request.QueryString("Solicitud"), oRol.Id, , , , , , FSNUser.Pyme)

                If Not dtEmpresas Is Nothing Then
                    If dtEmpresas.Rows.Count > 0 Then bEmpresaObtenida = True
                    If dtEmpresas.Rows.Count = 1 Then
                        hidEmpresa.Value = dtEmpresas.Rows(0).Item("ID")
                        lblEmpresa.Text = dtEmpresas.Rows(0).Item("DEN")
                        txtEmpresa.Visible = False
                        imgEmpresaLupa.Visible = False
                    ElseIf dtEmpresas.Rows.Count > 1 Then 'más de 1 empresa
                        imgInfEmpresa.Visible = False
                        lblEmpresa.Visible = False
                        txtEmpresa_AutoCompleteExtender.ContextKey = Request.QueryString("Solicitud") & "**" & oRol.Id
                    End If
                End If
            End If

            'Puede que aunque venga desde GS, el proceso no esté distribuido y no hayamos encontrado empresas en la búsquedas anteriores
            If Not bEmpresaObtenida AndAlso hidEmpresa.Value <> "" AndAlso hidEmpresa.Value <> "0" Then 'desde GS puede venir con 0
                Dim oEmpresa As FSNServer.Empresa
                oEmpresa = FSNServer.Get_Object(GetType(FSNServer.Empresa))
                oEmpresa.ID = hidEmpresa.Value
                oEmpresa.Load(FSNUser.Idioma)
                lblEmpresa.Text = oEmpresa.Den
                txtEmpresa.Visible = False
                imgEmpresaLupa.Visible = False
                oEmpresas = Nothing
            End If
            oEmpresas = Nothing
        End If

        If Request("FecInicio") <> Nothing Then
            Dim dtInicio As DateTime
            If (DateTime.TryParse(Request("FecInicio"), dtInicio)) Then
                Me.fsentryFecInicio.Valor = dtInicio
            End If
        End If

        If Request("FecFin") <> Nothing Then
            Dim dtFin As DateTime
            If (DateTime.TryParse(Request("FecFin"), dtFin)) Then
                Me.fsentryFecFin.Valor = dtFin
            End If
        End If

        lblProveedor.Text = Request("DenProveedor")

        CargarContactos()

        If Request("IdContacto") <> "" Then wddContactos.SelectedValue = Request("IdContacto")

        'Cargar Combo Expirar
        Dim Item As New Infragistics.Web.UI.ListControls.DropDownItem

        Item.Value = 1
        Item.Text = sTextos(0) 'dia(s)
        wddPeriodo.Items.Add(Item)
        wddEmail.Items.Add(Item)
        wddPeriodo.SelectedItemIndex = 0
        wddEmail.SelectedItemIndex = 0

        Item = New Infragistics.Web.UI.ListControls.DropDownItem
        Item.Value = 2
        Item.Text = sTextos(1) 'semana(s)
        wddPeriodo.Items.Add(Item)
        wddEmail.Items.Add(Item)
        Item = New Infragistics.Web.UI.ListControls.DropDownItem
        Item.Value = 3
        Item.Text = sTextos(2) '"mes(es)"
        wddPeriodo.Items.Add(Item)
        wddEmail.Items.Add(Item)

        CargarMonedas()

        Dim Importe As Object = oSolicitud.Formulario.ConCampoImporte
        If Not Importe Is Nothing Then
            lblImporte.Visible = True
            lblImporteValor.Visible = True
            lblMonedaImporte.Visible = True
            lblImporte.Text = sTextos(3) & ": "
            lblImporteValor.Text = FSNLibrary.FormatNumber(DBNullToSomething(Importe), FSNUser.NumberFormat) & " "
            lblMonedaImporte.Text = wddMonedas.CurrentValue
            lblMonedaImporte.ToolTip = wddMonedas.CurrentValue
        Else
            lblImporte.Visible = False
            lblImporteValor.Visible = False
            lblMonedaImporte.Visible = False
        End If
    End Sub
    ''' <summary>
    ''' Carga los campos/desgloses del formulario
    ''' </summary>
    ''' <remarks>Llamada desde:altaContrato_Init; Tiempo máximo:=Segun los campos del formulario</remarks>
    Private Sub ConfigurarFormulario()
        Dim oGrupo As FSNServer.Grupo
        Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
        Dim oucCampos As campos
        Dim oucDesglose As desgloseControl
        Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oRow As DataRow
        Dim sNombreContrato As String

        If Request("NombreContrato") <> "" Then sNombreContrato = Request("NombreContrato")

        Solicitud.Value = oSolicitud.ID
        Bloque.Value = oSolicitud.BloqueActual

        uwtGrupos.Tabs.Clear()

        Dim lIndex As Long = 0
        AplicarEstilosTab(uwtGrupos)

        For Each oGrupo In oSolicitud.Formulario.Grupos.Grupos
            oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
            Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
            Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
            oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(Idioma), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
            oTabItem.Key = lIndex
            uwtGrupos.Tabs.Add(oTabItem)

            oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden
            oInputHidden.ID = "txtPre_" + lIndex.ToString
            lIndex += 1

            If oGrupo.NumCampos <= 1 Then
                For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                    If oRow.Item("VISIBLE") = 1 Then
                        Exit For
                    End If
                Next
                If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Desglose Or (DBNullToSomething(oRow.Item("SUBTIPO")) = TiposDeDatos.TipoGeneral.TipoDesglose And DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.DesgloseActividad) Then
                    If oRow.Item("VISIBLE") = 0 Then
                        uwtGrupos.Tabs.Remove(oTabItem)
                    Else
                        oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                        oTabItem.ContentPane.UserControlUrl = "..\alta\desglose.ascx"
                        oucDesglose = oTabItem.ContentPane.UserControl
                        oucDesglose.ID = oGrupo.Id.ToString
                        oucDesglose.Campo = oRow.Item("ID")
                        oucDesglose.TieneIdCampo = False
                        oucDesglose.Ayuda = DBNullToSomething(oRow.Item("AYUDA_" & Idioma))
                        oucDesglose.TabContainer = uwtGrupos.ClientID
                        oucDesglose.SoloLectura = (oRow.Item("ESCRITURA") = 0)
                        oucDesglose.Solicitud = oSolicitud.ID
                        oucDesglose.PM = True
                        oucDesglose.Titulo = DBNullToSomething(oRow.Item("DEN_" & Idioma))
                        oucDesglose.TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Contrato
                        oInputHidden.Value = oucDesglose.ClientID
                    End If
                Else
                    oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                    oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                    oucCampos = oTabItem.ContentPane.UserControl
                    oucCampos.ID = oGrupo.Id.ToString
                    oucCampos.dsCampos = oGrupo.DSCampos
                    oucCampos.IdGrupo = oGrupo.Id
                    oucCampos.Idi = Idioma
                    oucCampos.TabContainer = uwtGrupos.ClientID
                    oucCampos.Solicitud = oSolicitud.ID
                    oucCampos.TipoSolicitud = oSolicitud.TipoSolicit
                    oucCampos.PM = True
                    oucCampos.Formulario = oSolicitud.Formulario
                    oucCampos.TamanyoContrato = lTamanyoAdjuntoContrato.ToString
                    oucCampos.NombreContrato = sNombreContrato
                    oucCampos.IdArchivoContrato = lIdAdjuntoContrato
                    oInputHidden.Value = oucCampos.ClientID
                End If
            Else
                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"

                oucCampos = oTabItem.ContentPane.UserControl
                oucCampos.ID = oGrupo.Id.ToString
                oucCampos.dsCampos = oGrupo.DSCampos
                oucCampos.IdGrupo = oGrupo.Id
                oucCampos.Idi = Idioma
                oucCampos.TabContainer = uwtGrupos.ClientID
                oucCampos.Solicitud = oSolicitud.ID
                oucCampos.TipoSolicitud = oSolicitud.TipoSolicit
                oucCampos.PM = True
                oucCampos.Formulario = oSolicitud.Formulario
                oucCampos.NombreContrato = sNombreContrato
                oucCampos.TamanyoContrato = lTamanyoAdjuntoContrato.ToString
                oucCampos.IdArchivoContrato = lIdAdjuntoContrato
                oInputHidden.Value = oucCampos.ClientID
            End If

            Me.FindControl("frmAlta").Controls.Add(oInputHidden)
        Next

        'Participantes
        oRol.CargarParticipantes(Idioma)
        If oRol.Participantes.Tables(0).Rows.Count > 0 Then
            Dim oucParticipantes As participantes

            oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
            oTabItem.Text = Textos(31) 'Participantes
            oTabItem.Key = "PARTICIPANTES"

            uwtGrupos.Tabs.Add(oTabItem)

            oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
            oTabItem.ContentPane.UserControlUrl = "../alta/participantes.ascx"

            oucParticipantes = oTabItem.ContentPane.UserControl
            oucParticipantes.Idi = Idioma
            oucParticipantes.TabContainer = uwtGrupos.ClientID

            oucParticipantes.dsParticipantes = oRol.Participantes

            oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden
            oInputHidden.ID = "txtPre_PARTICIPANTES"
            oInputHidden.Value = oucParticipantes.ClientID
            Me.FindControl("frmAlta").Controls.Add(oInputHidden)
        End If
    End Sub
    ''' <summary>
    ''' Registra las variables de javascript que utilizaremos
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load; Tiempo máximo:=0,2seg.</remarks>
    Private Sub GenerarScript()
        Dim sClientTextVars As String
        sClientTextVars = ""
        sClientTextVars += "vdecimalfmt='" + FSNUser.DecimalFmt + "';"
        sClientTextVars += "vthousanfmt='" + FSNUser.ThousanFmt + "';"
        sClientTextVars += "vprecisionfmt='" + FSNUser.PrecisionFmt + "';"
        sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsUser", sClientTextVars)

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")

        Dim ilGMN1 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN1
        Dim ilGMN2 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN2
        Dim ilGMN3 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN3
        Dim ilGMN4 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN4
        Dim sScript As String
        sScript = ""
        sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
        sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
        sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
        sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "LongitudesCodigosKey", sScript)

        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(Textos(25)) + "';"
        sClientTexts += "arrTextosML[1] = '" + JSText(Textos(26)) + "';"
        sClientTexts += "arrTextosML[2] = '" + JSText(Textos(27)) + "';"
        sClientTexts += "arrTextosML[3] = '" + JSText(Textos(28)) + "';"
        sClientTexts += "arrTextosML[4] = '" + JSText(Textos(29)) + "';"
        sClientTexts += "arrTextosML[5] = '" + JSText(Textos(30)) + "';"
        sClientTexts += "arrTextosML[6] = '" + JSText(Textos(32)) + "';"
        sClientTexts += "arrTextosML[7] = '" + JSText(Textos(33)) + "';"
        sClientTexts += "arrTextosML[8] = '" + JSText(Textos(34)) + "';"

        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

        If Not Page.ClientScript.IsClientScriptBlockRegistered("varFila") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varFila", "var sFila = '" & JSText(Textos(36)) & "';", True)

        If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayDesgloses") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "var arrDesgloses = new Array();", True)
        End If

        txtEmpresa.Attributes.Add("onKeyDown", "return ControlEmpresa(this, event);")
        txtProveedor.Attributes.Add("onKeyDown", "return ControlProveedor(this, event);")
    End Sub
    ''' <summary>
    ''' Carga en el combo correspondiente la lista de contactos internos que devuelve el buscador de personas
    ''' </summary>
    ''' <param name="bIsPostback">Si es postback o no</param>
    ''' <remarks></remarks>
    Private Sub CargarNotificados(ByVal bIsPostback As Boolean, ByVal ListaNotificados As String)
        Dim Item As Infragistics.Web.UI.ListControls.DropDownItem

        If bIsPostback Then
            Dim Notificados() As String = Split(ListaNotificados, "###")
            For i As Integer = 0 To Notificados.Length - 2
                Item = New Infragistics.Web.UI.ListControls.DropDownItem
                Item.Value = Split(Notificados(i), "#")(0)
                Item.Text = Split(Notificados(i), "#")(1) & IIf(Split(Notificados(i), "#")(2) <> "null", " (" & Split(Notificados(i), "#")(2) & ")", "")
                Item.Selected = True

                If wddNotificados.Items.FindItemByValue(Item.Value) Is Nothing Then
                    If wddNotificados.CurrentValue = "" Then
                        wddNotificados.CurrentValue = Item.Text
                    Else
                        wddNotificados.CurrentValue = wddNotificados.CurrentValue & "," & Item.Text
                    End If
                    wddNotificados.Items.Add(Item)
                End If
            Next
        Else
            'Añadir el usuario comprador responsable del proceso
            Dim sUsuarioResponsable() As String
            Dim sCodPersona As String = String.Empty
            Dim sNombre As String

            If Request("PersonaNotificadora") <> "" Then
                sUsuarioResponsable = Split(Request("PersonaNotificadora"), "_")
                sCodPersona = sUsuarioResponsable(0)
                sNombre = sUsuarioResponsable(1)
                Item = New Infragistics.Web.UI.ListControls.DropDownItem
                Item.Value = sCodPersona
                Item.Text = sNombre
                Item.Selected = True
                wddNotificados.CurrentValue = Item.Text
                wddNotificados.Items.Add(Item)
            End If

            If sCodPersona <> FSNUser.CodPersona Then
                Item = New Infragistics.Web.UI.ListControls.DropDownItem
                Item.Value = FSNUser.CodPersona
                Item.Text = FSNUser.Nombre & " (" & FSNUser.Email & ")"
                Item.Selected = (sCodPersona = "")

                wddNotificados.Items.Add(Item)
            End If
        End If
    End Sub
    ''' <summary>
    ''' Carga la combo con las monedas, por defecto selecciona la moneda de la solicitud
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load; Tiempo máximo:=0,32seg.</remarks>
    Private Sub CargarMonedas()
        Dim oMonedas As FSNServer.Monedas
        oMonedas = FSNServer.Get_Object(GetType(FSNServer.Monedas))
        oMonedas.LoadData(Idioma)

        wddMonedas.DataSource = oMonedas.Data
        wddMonedas.TextField = "DEN"
        wddMonedas.ValueField = "COD"
        wddMonedas.DataBind()
        wddMonedas.SelectedValue = FSNServer.TipoAcceso.gsMonedaCentral

        oMonedas = Nothing
    End Sub
    ''' Revisado por: blp. Fecha: 05/10/2011
    ''' <summary>
    ''' Revisado por: Sandra. Fecha: 09/03/2011
    ''' Carga los contactos del proveedor seleccionado. Mientras se hace la carga, se deshabilita el dropdown.
    ''' </summary>
    ''' <param name="proveCod">
    ''' Código del proveedor del que queremos cargar los contactos. 
    ''' Si no se pasa a la función, ésta busca en el campo hidden de la página por si se ha guardado allí el código del proveedor. 
    ''' Si ambos son nothing, no se carga nada
    ''' </param>
    ''' <remarks>Llamada desde: ConfigurarCabeceraContrato y Page_Load(); Tiempo máximo: 0,4 sg.</remarks>
    Private Sub CargarContactos(Optional ByVal proveCod As String = Nothing)
        wddContactos.DisplayMode = Infragistics.Web.UI.ListControls.DropDownDisplayMode.ReadOnly
        wddContactos.Items.Clear()
        wddContactos.CurrentValue = Nothing

        Dim proveCodDef As String = Nothing
        If Not proveCod = Nothing Then
            proveCodDef = proveCod
        ElseIf hid_Proveedor.Value <> "" Then
            proveCodDef = hid_Proveedor.Value
        End If
        If proveCodDef IsNot Nothing Then
            Dim oProve As FSNServer.Proveedor
            oProve = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
            oProve.Cod = hid_Proveedor.Value
            'Contactos del proveedor
            oProve.CargarContactos()

            wddContactos.DataSource = oProve.Contactos.Tables(0)
            wddContactos.TextField = "CONTACTO"
            wddContactos.ValueField = "ID"
            wddContactos.DataBind()
        End If

        Dim oItem As New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 0
        oItem.Text = ""
        wddContactos.Items.Insert(0, oItem)
        wddContactos.DisplayMode = Infragistics.Web.UI.ListControls.DropDownDisplayMode.DropDown
    End Sub
    ''' <summary>
    ''' Carga los elementos de la pagina con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load; Tiempo máximo:=0,1seg.</remarks>
    Private Sub CargarIdiomas()
        lblLitFechaInicio.Text = "(*) " & Textos(0)  '"Fecha de inicio"
        lblLitFechaExpiracion.Text = Textos(1) '"Fecha de expiración"
        lblLitProveedor.Text = "(*) " & Textos(2) '"Proveedor"
        lblLitContacto.Text = Textos(3) '"Contacto"
        lblLitEmpresa.Text = "(*) " & Textos(4) '"Empresa"
        lblLitMoneda.Text = "(*) " & Textos(5) '"Moneda"
        lblCamposObligatorios.Text = Textos(37)

        lblConfiguracionAlertas.Text = Textos(6) '"Configuración de alertas"
        lblMostrarAlerta.Text = Textos(7) '"Mostrar alerta"
        lblAntesExpirar.Text = Textos(8) '"antes de expirar"
        lblEmail.Text = Textos(11) & ":" '"Enviar email" & ":"
        lblNotificados.Text = Textos(12) & ":" '"Configurar notificados" & ":"

        sTextos(0) = Textos(13) '"dia(s)"
        sTextos(1) = Textos(14) '"semana(s)"
        sTextos(2) = Textos(15) '"mes(es)"
        sTextos(3) = Textos(16) '"Importe"
        sTextos(4) = Textos(17) '"Calcular"
        sTextos(5) = Textos(18) '"Otras acciones"
        sTextos(6) = Textos(19) '"Guardar"
        sTextos(7) = Textos(20) '"Impr./Exp"
        sTextos(8) = Textos(21) '"Volver"
        sTextos(9) = Textos(22) '"Eliminar"
        sTextos(10) = Textos(38) '"Importar"

        cadenaespera.Value = Textos(23) '"Su solicitud está siendo tramitada. Espere unos instantes..."
        lblProgreso.Text = Textos(24) '"Se está cargando la solicitud. Espere unos instantes..."

        'Textos Panel info-s
        FSNPanelDatosProveedor.Titulo = Textos(2) '"Proveedor"
        FSNPanelDatosProveedor.SubTitulo = Textos(35) '"InformaciÃ³n detallada"

        FSNPanelDatosEmpresa.Titulo = Textos(4) '"Empresa"
        FSNPanelDatosEmpresa.SubTitulo = Textos(35)  '"InformaciÃ³n detallada"
    End Sub
    ''' Revisado por: blp. Fecha: 03/10/2011
    ''' <summary>
    ''' Carga un combo con una llamada desde javascript
    ''' </summary>
    ''' <param name="sender">Control origen del evento</param>
    ''' <param name="e">la información del evento</param>
    ''' <remarks>Llamada desde: javascript del a página; Tiempo máximo:menor de 1 seg</remarks>
    Public Sub DropDown_ItemsRequested(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownItemsRequestedEventArgs)
        Dim oDropDown As Infragistics.Web.UI.ListControls.WebDropDown = CType(sender, Infragistics.Web.UI.ListControls.WebDropDown)

        If oDropDown.ID = "wddContactos" Then
            CargarContactos(e.Value)
        ElseIf oDropDown.ID = "wddNotificados" Then
            CargarNotificados(1, e.Value)
        End If
    End Sub

    Private Sub CargarContratoImportar()

        If IdInstanciaImportar.Value > 0 Then
            Dim oContrato2 As FSNServer.Contrato
            oContrato2 = FSNServer.Get_Object(GetType(FSNServer.Contrato))
            oContrato2.ID = oContrato2.BuscarIdContrato(IdInstanciaImportar.Value)
            oContrato2.Load(Idioma)

            fsentryFecInicio.Valor = FormatDate(oContrato2.FechaInicio, FSNUser.DateFormat)
            fsentryFecFin.Valor = FormatDate(oContrato2.FechaFin, FSNUser.DateFormat)

            hidEmpresa.Value = oContrato2.IdEmpresa
            If hidEmpresa.Value <> "" AndAlso hidEmpresa.Value <> "0" Then
                Dim oEmpresa As FSNServer.Empresa
                oEmpresa = FSNServer.Get_Object(GetType(FSNServer.Empresa))
                oEmpresa.ID = hidEmpresa.Value
                oEmpresa.Load(FSNUser.Idioma)
                txtEmpresa.Text = oEmpresa.Den
                oEmpresa = Nothing
            End If

            txtProveedor.Text = oContrato2.DenProve
            hid_Proveedor.Value = oContrato2.CodProve

            CargarContactos(oContrato2.CodProve)
            wddContactos.SelectedValue = oContrato2.IdContacto

            wddMonedas.SelectedValue = oContrato2.CodMoneda


            wddPeriodo.SelectedValue = oContrato2.PeriodoAlerta
            wddEmail.SelectedValue = oContrato2.PeriodoRepetirEmail

            Dim iAlerta As Integer
            Select Case oContrato2.PeriodoAlerta
                Case PeriodosAlertaContratos.Dias
                    iAlerta = oContrato2.Alerta
                Case PeriodosAlertaContratos.Semanas
                    iAlerta = oContrato2.Alerta / 7
                Case PeriodosAlertaContratos.Meses
                    iAlerta = oContrato2.Alerta / 30
            End Select
            txtAlerta.Text = iAlerta

            Dim iRepetirEmail As Integer
            Select Case oContrato2.PeriodoRepetirEmail
                Case PeriodosAlertaContratos.Dias
                    iRepetirEmail = oContrato2.RepetirEmail
                Case PeriodosAlertaContratos.Semanas
                    iRepetirEmail = oContrato2.RepetirEmail / 7
                Case PeriodosAlertaContratos.Meses
                    iRepetirEmail = oContrato2.RepetirEmail / 30
            End Select
            txtPeriodoEmail.Text = iRepetirEmail

            CargarNotificados(oContrato2.NotificadosNombreyEmail)

            oContrato2 = Nothing
        End If


    End Sub

    ''' <summary>
    ''' Carga en el combo correspondiente la lista de contactos internos que devuelve el buscador de personas
    ''' </summary>
    ''' <param name="ListaNotificados">Lista de los contactos internos separados por ### con su value y nombre</param>
    ''' <remarks></remarks>
    Private Sub CargarNotificados(ByVal ListaNotificados As String)

        Dim Item As Infragistics.Web.UI.ListControls.DropDownItem
        Dim Notificados() As String = Split(ListaNotificados, "###")

        For i As Integer = 0 To Notificados.Length - 2
            Item = New Infragistics.Web.UI.ListControls.DropDownItem
            Item.Value = Split(Notificados(i), "#")(0)
            Item.Text = Split(Notificados(i), "#")(1) & IIf(Split(Notificados(i), "#")(2) <> "null", " (" & Split(Notificados(i), "#")(2) & ")", "")
            Item.Selected = True


            If wddNotificados.Items.FindItemByValue(Item.Value) Is Nothing Then
                If wddNotificados.CurrentValue = "" Then
                    wddNotificados.CurrentValue = Item.Text
                Else
                    wddNotificados.CurrentValue = wddNotificados.CurrentValue & "," & Item.Text
                End If
                wddNotificados.Items.Add(Item)
            End If

        Next
    End Sub

End Class