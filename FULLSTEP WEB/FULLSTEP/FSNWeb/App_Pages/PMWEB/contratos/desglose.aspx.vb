
Public Class contdesglose
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblSubTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents INPUTDESGLOSE As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblTituloData As System.Web.UI.WebControls.Label
    Protected WithEvents cmdGuardar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdCalcular As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents Solicitud As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Favorito As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Instancia As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Const IncludeScriptFormat As String = ControlChars.CrLf & _
"<script type=""{0}"" src=""{1}""></script>"
    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
"<script language=""{0}"">{1}</script>"


    ''' <summary>
    ''' Cargar un desglose en un popup
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">evento de sistema</param>        
    ''' <returns>Nada</returns>
    ''' <remarks>Llamada desde: altacontrato.aspx, gestionContrato.aspx, detalleContrato.aspx, gestionContratoTrasladada.aspx; Tiempo m�ximo:0,1</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim idCampo As Integer = Request("campo")
        Dim sInput As String = Request("Input")
        Dim oCampo As FSNServer.Campo
        Dim lSolicitud As Long
        Dim lInstancia As Integer = Request("instancia")

        INPUTDESGLOSE.Value = sInput
        If Request("Solicitud") <> Nothing Then lSolicitud = Request("Solicitud")

        Solicitud.Value = lSolicitud
        Favorito.Value = Request("Favorito")
        Instancia.Value = lInstancia

        oCampo = FSNServer.Get_Object(GetType(FSNServer.Campo))
        oCampo.Id = idCampo

        If lSolicitud <> Nothing Then
            oCampo.Load(Idioma, lSolicitud)
        Else
            oCampo.LoadInst(lInstancia, Idioma)
        End If

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Desglose

        lblTitulo.Text = Textos(0)
        lblTituloData.Text = oCampo.DenSolicitud(Idioma)

        If Request("BotonCalcular") <> Nothing Then
            If Request("BotonCalcular") = 1 Then
                Me.cmdCalcular.Visible = True
            Else
                Me.cmdCalcular.Visible = False
            End If
        End If

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ruta", "<script>var ruta = '" & ConfigurationManager.AppSettings("ruta") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "version") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "version", _
                "<script>var version = '" & System.Configuration.ConfigurationManager.AppSettings("version") & "';</script>")
        End If

        Me.FindControl("frmAlta").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())

        Dim bSoloLectura As Boolean = (Request("SoloLectura") = "1")

        Dim oucdesglose As desgloseControl
        oucdesglose = Me.FindControl("ucdesglose")
        oucdesglose.Version = Request("Version")
        oucdesglose.PM = True
        oucdesglose.Titulo = oCampo.DenGrupo(Idioma) + " / " + oCampo.Den(Idioma)
        oucdesglose.TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Contrato

        cmdGuardar.Value = Textos(2)
        cmdGuardar.Attributes.Add("onclick", "return actualizarCampoDesgloseYCierre()")
        cmdCalcular.Value = Textos(3)
    End Sub

End Class

