﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="altaContrato.aspx.vb" Inherits="Fullstep.FSNWeb.altaContrato" EnableEventValidation="true" %>

<%@ Register Assembly="Infragistics.WebUI.UltraWebTab.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<%@ Register Assembly="Infragistics.WebUI.WebDataInput.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.WebDataInput" TagPrefix="igtxt" %>
<%@ Register Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebNavigator" TagPrefix="ignav" %>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Register TagPrefix="fsde" Namespace="Fullstep.DataEntry" Assembly="DataEntry" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>altaContrato</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
</head>

<!--JIM: colocamos esta porción de código antes de la etiqueta de menú. menu.js realiza override de window.open, y si la colocamos
        después no se ejecuta este código -->
<%If Not String.IsNullOrEmpty(Request("ConfiguracionGS")) Then%>
<script type="text/javascript">
    window.open_ = window.open;
    window.open = function (url, name, props) {
        if (url.toLowerCase().search("sessionid") > 0)
            return window.open_(url, name, props);
        else
            return window.open_(url + '&SessionId=<%=Session("sSession")%>', name, props);
    }
</script>
<%End If%>

<script type="text/javascript">
    wProgreso = null;

    //Descripcion:Muestra el contenido peligroso que ha efectuado un error al guardar.
    //Parametros entrada: 
    //contenido: Contenido del error
    //Llamada desde: errores.aspx.vb
    //Tiempo ejecucion:=0seg.
    function ErrorValidacion(contenido) {
        alert(arrTextosML[3].replace("$$$", contenido));
        OcultarEspera()

    }
    /*Descripcion:Inicializa los estilos de los tabs
    llamada desde:OnLoad del body
    Tiempo ejecucion:=0,1seg.*/
    function iniciar() {
        resize();
        OcultarEspera();
    }

    /*Descripcion:Despues del recalculo de los campos calculados en la pagina recalcularImportes.aspx.
    se devuelve el valor
    Parametros entrada:=
    importeConFormato: Importe con el formato del usuario
    Importe: Importe (Numerico)
    Llamada desde:=Click Calcular.
    Tiempo ejecucion:=0seg.*/
    function ponerCalculados(importeConFormato, importe) {
        lblImporteV = document.getElementById('<%=lblImporteValor.clientID %>')
        if (lblImporteV) {
            lblImporteV.innerHTML = importeConFormato;
        }
    }
    /*Descripcion: Le pone un ancho al desglose
	Llamada desde:=init().
	Tiempo ejecucion:=0seg.*/
    function resize() {
        for (i = 0; i < arrDesgloses.length; i++) {
            sDiv = arrDesgloses[i].replace("tblDesglose", "divDesglose")
            if (document.getElementById(sDiv))
                document.getElementById(sDiv).style.width = parseFloat(document.body.offsetWidth) - 95 + 'px';
        }
    }
    /*Descripcion: Se le asigna las dimensiones al tab
	Llamada desde:=opcion de menu "Guardar"
	Tiempo ejecucion:=1,2seg.*/
    function CalcularCamposCalculados() {
        oFrm = MontarFormularioCalculados()
        oFrm.submit()
        return false;
    }
    /*Descripcion: Abre una ventana con las opciones de exportacion...
	Llamada desde:=opcion de menu "Impr./Exp."
	Tiempo ejecucion:=0seg.*/
    function cmdImpExp_onclick() {
        window.open('../seguimiento/impexp_sel.aspx?Instancia=' + document.forms["frmAlta"].elements["NEW_Instancia"].value + '&TipoImpExp=1', '_new', 'fullscreen=no,height=115,width=315,location=no,menubar=no,resizable=no,scrollbars=no,status=yes,titlebar=yes,toolbar=no,left=200,top=200');
        return false;
    }
    /*Descripcion:= Funcion que carga los valores del buscador de empresas en la caja de texto
	Parametros:=
	idEmpresa --> Id Empresa
	nombreEmpresa --> Nombre empresa
	Llamada desde:= BuscadorEmpresas.aspx --> aceptar()*/
    function SeleccionarEmpresa(idEmpresa, nombreEmpresa, nifEmpresa) {
        //el nif no se utiliza, pero tiene que estar
        oIdEmpresa = document.getElementById('<%= hidEmpresa.clientID %>');
        if (oIdEmpresa)
            oIdEmpresa.value = idEmpresa;

        otxtEmpresa = document.getElementById('<%=txtEmpresa.clientID %>');
        if (otxtEmpresa)
            otxtEmpresa.value = nombreEmpresa;
    }
    /*Descripcion: Realiza la llamada de seleccion de Notificados
	Llamada desde:=Icono de notificados
	Tiempo ejecucion:=0seg.*/
    function BuscarNotificadoInterno() {
        var newWindow = window.open(rutaFS + "_common/BuscadorUsuarios.aspx?IDControl=wddNotificados&desde=AltaContratos", "_blank", "width=770,height=530,status=yes,resizable=no,top=200,left=200,scrollbars=yes");

    }
    /*''' Revisado por: blp. Fecha: 05/10/2011
	''' Carga el listado del contactos en el control wddContactos en funcion del proveedor seleccionado
	''' Llamada desde: BuscadorProveedores.aspx --> wdgDatos_CellSelectionChanged Máx < 1 seg*/
    function prove_seleccionado2(sCIF, sProveCod, sProveDen) {
        document.getElementById("<%=hid_Proveedor.ClientID%>").value = sProveCod
        document.getElementById("<%=txtProveedor.ClientID%>").value = sProveDen

        var wddContactos = $find("wddContactos")
        if (wddContactos) {
            wddContactos.loadItems(sProveCod);
            wddContactos = null;
        }
    }
    /* Revisado por: blp. Fecha: 05/10/2011
	Descripcion:Muestra en la combo el /los notificados seleccionados en la pagina de usuarios.aspx.
				Realiza un postback y en el servidor se hace la operacion de añadir a la combo
	Parametros
		Notificados: Cadena con los las personas seleccionadas para ser notificadas
	Llamada desde:usuarios.aspx
	Tiempo ejecucion:=0seg.*/
    function AgregarNotificadosInternos(Notificados) {
        var wddNotificados = $find("wddNotificados")
        if (wddNotificados) {
            wddNotificados.loadItems(Notificados)
            wddNotificados = null;
        }
    }
    //Descripción: Cuando se pulsa un botón para realizar una acción, se llama a esta función
    //Paramétros: id: id de la acción
    // bloque: id del bloque
    // comp_olb: si hay que comprobar los campos de obligatorios o no
    // guarda: si hay que guardar o versión o no
    // rechazo: indica si la accion implica un rechazo, ya sea temporal o definitivo
    function EjecutarAccion(id, bloque, comp_obl, guarda, rechazo) {
        setTimeout("EjecutarAccion2(" + id + "," + bloque + "," + comp_obl + "," + guarda + "," + rechazo + ")", 100);
    }


    function ImportarContrato() {

        window.open('<%=ConfigurationManager.AppSettings("rutaFS")%>_common/BuscadorContratos.aspx?Solicitud=' + $("#Solicitud").val() + '&Importar=1', '_blank', 'width=920,height=450,status=yes,resizable=no,scrollbars=yes,top=100,left=100');
		    return false;
		};

    //Descripción: Comprueba los campos obligatorios, si hay participantes o no, y llama a montarformulario
    //Paramétros: id: id de la acción
    // bloque: id del bloque
    // comp_olb: si hay que comprobar los campos de obligatorios o no
    // guarda: si hay que guardar o versión o no
    // rechazo: indica si la accion implica un rechazo, ya sea temporal o definitivo
    // LLamada desde: EjecutarAccion
    function EjecutarAccion2(id, bloque, comp_obl, guarda, rechazo) {
        //maper=True
    	//Fechas Inicio - Fin Contrato
    	var respOblig;
        oEntry = fsGeneralEntry_getById('<%= fsentryFecInicio.clientID %>');
        if (oEntry) {
            fechaAuxInicio = oEntry.getValue();
            if (fechaAuxInicio == null) {
                alert(arrTextosML[4])
                return false;
            }
        }

        oEntry = fsGeneralEntry_getById('<%= fsentryFecFin.clientID %>');
        if (oEntry) {
            fechaAuxFin = oEntry.getValue();
            if (fechaAuxFin != null) {
                if (fechaAuxFin < fechaAuxInicio) {
                    alert(arrTextosML[6])
                    return false;
                }
            }
        }

        oIdProveedor = document.getElementById('<%= hid_Proveedor.clientID %>');
        if (oIdProveedor)
            if (oIdProveedor.value == "") {
                alert(arrTextosML[7])
                return false;
            }

        oIdEmpresa = document.getElementById('<%= hidEmpresa.clientID %>');
        if (oIdEmpresa)
            if ((oIdEmpresa.value == "") || (oIdEmpresa.value == "0")) { //Desde GS puede llegar con valor 0
                alert(arrTextosML[5])
                return false;
            }

        //Monedas
        var wdd = $find("wddMonedas")
        if (wdd) {
            if (wdd.get_selectedItems().length == 0) {
                alert(arrTextosML[8]);
                return false;
            } else {
                if (wdd.get_selectedItems()[0].get_text() != wdd.get_currentValue()) {
                    alert(arrTextosML[8]);
                    return false;
                }
            }
        }

        if (comp_obl == true || comp_obl == "true") {
            bMensajePorMostrar = document.getElementById("bMensajePorMostrar")
            if (bMensajePorMostrar) {
                if (bMensajePorMostrar.value == "1") {
                    bMensajePorMostrar.value = "0";
                    return false;
                }
            }

            respOblig = comprobarObligatorios();
            switch (respOblig) {
                case "":  //no falta ningun campo obligatorio
                    break;
                case "filas0": //no se han introducido filas en un desglose obligatorio
                	alert(arrTextosML[0]);
                	return false;
                    break;
                default: //falta algun campo obligatorio
                	alert(arrTextosML[0] + '\n' + respOblig);
                	return false;
                    break;
            }
        }

        if (rechazo == false) {
        	respOblig=comprobarParticipantes();
        	if (respOblig !== '') {
        		alert(arrTextosML[0] + '\n' + respOblig);
        		return false;
        	};
        };

        if (document.forms["frmAlta"].elements["PantallaVinculaciones"].value == "True") {
            var instancia = document.getElementById("NEW_Instancia").value;
            var solicitud = 1;
            if (instancia > 0)
                solicitud = 0;
            if (Validar_Cantidades_Vinculadas(solicitud, instancia) == false) {
                return false
            }
        }

        if (wProgreso == null) {
            wProgreso = true;
            MostrarEspera()
        }

        setTimeout("MontarSubmitAccion(" + id + "," + bloque + "," + guarda + ")", 100);
    }
    /*  Descripcion:Monta el formulario para ser enviado a guardarInstancia.aspx
    Parametros entrada:
    id:= ID de la accion
    bloque:= ID del bloque
    guarda:= Si va a ser almacenada la informacion o no
    Llamada desde:=EjecutarAccion2
    Tiempo ejecucion:=1,2seg.*/
    function MontarSubmitAccion(id, bloque, guarda) {
        MontarFormularioSubmit(guarda, true, false, false)

        document.forms["frmSubmit"].elements["GEN_AccionRol"].value = id;
        document.forms["frmSubmit"].elements["GEN_Bloque"].value = bloque;
        document.forms["frmSubmit"].elements["GEN_Accion"].value = "altacontrato";
        document.forms["frmSubmit"].elements["GEN_Rol"].value = oRol_Id;

        //Variables de la cabecera del contrato
        CompletarFormulario()

        oFrm = MontarFormularioCalculados()

        sInner = oFrm.innerHTML
        oFrm.innerHTML = ""
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner)
        document.forms["frmSubmit"].submit()
        return false
    }

    /*	Descripcion:=Monta el formulario para el posterior guardado. (guardarInstancia.aspx)
    Llamada desde:Guardar()
    Tiempo ejecucion:1,5seg.*/
    function Guardar() {
        if (document.forms["frmAlta"].elements["PantallaVinculaciones"].value == "True") {
            var instancia = document.getElementById("NEW_Instancia").value;
            var solicitud = 1;
            if (instancia > 0)
                solicitud = 0;
            if (Validar_Cantidades_Vinculadas(solicitud, instancia) == false) {
                return false
            }
        }

        if (wProgreso == null) {
            wProgreso = true;
            MostrarEspera();
        }
        MontarFormularioSubmit(true, true, false, false)

        var frmSubmitElements = document.forms["frmSubmit"].elements;
        frmSubmitElements["GEN_AccionRol"].value = 0;
        frmSubmitElements["GEN_Bloque"].value = document.forms["frmAlta"].elements["Bloque"].value;
        frmSubmitElements["GEN_Enviar"].value = 0;
        frmSubmitElements["GEN_Accion"].value = "guardarcontrato";
        frmSubmitElements["PantallaMaper"].value = false;
        frmSubmitElements["GEN_Rol"].value = oRol_Id;

        //Variables de la cabecera del contrato
        CompletarFormulario()

        oFrm = MontarFormularioCalculados()
        sInner = oFrm.innerHTML
        oFrm.innerHTML = ""
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner)
        document.forms["frmSubmit"].submit()
        return false
    }

    /*Descripcion:Almacena en las variables el valor de Contrato y instancia, Oculta el div de espera y muestra los tabs
    Parametros entrada:=
    idContrato:=ID del contrato.
    idInstancia:ID Instancia.
    codContrato:Cod Contrato.	
    Llamada desde:= resultadoProcesarAccion
    Tiempo ejecucion:=0,1seg.*/
    function PonerIdContrato(idContrato, idInstancia, codContrato) {
        if (document.forms["frmAlta"].elements["hid_CodContrato"].value == "") {
            if (document.getElementById("lblCabecera"))
                document.getElementById("lblCabecera").innerHTML = codContrato + ' - ' + document.getElementById("lblCabecera").innerHTML;
        }

        if (idContrato > 0) {
            document.forms["frmAlta"].elements["hid_IdContrato"].value = idContrato
            document.forms["frmAlta"].elements["hid_CodContrato"].value = codContrato
            document.forms["frmAlta"].elements["NEW_Instancia"].value = idInstancia
        }

        HabilitarBotones();
    }

    //Estaba habilitando los botones antes de ocultar el progreso. Parecia q te dejaba dar a varios botones mientras estaba en progreso.
    function DarTiempoAOcultarProgreso() {
        $("[id*='lnkBoton']").removeAttr('disabled');
    }

    /*
    Descripcion: Oculta el div de espera y muestra los tabs de grupos
    Llamada desde= init() // HabilitarBotones() 
    Tiempo ejecucion:=0,1seg. 
    */
    function OcultarEspera() {
        wProgreso = null;

        document.getElementById("divProgreso").style.display = 'none';
        document.getElementById("divForm2").style.display = 'inline';
        document.getElementById("divForm3").style.none = 'inline';

        setTimeout('DarTiempoAOcultarProgreso()', 250);

        if (arrObligatorios.length > 0)
            if (document.getElementById("lblCamposObligatorios"))
                document.getElementById("lblCamposObligatorios").style.display = ""

        i = 0;
        bSalir = false;
        while (bSalir == false) {
            if (document.getElementById("uwtGrupos_div" + i)) {
                document.getElementById("uwtGrupos_div" + i).style.visibility = 'visible';
                i = i + 1;
            }
            else {
                bSalir = true;
            }
        }

    }
    /*
    Descripcion: Muestra el div de espera y oculta los tabs de grupos
    Llamada desde= EjecutarAccion2()  //Guardar()
    Tiempo ejecucion:=0,1seg. 
    */
    function MostrarEspera() {
        wProgreso = true;

        $("[id*='lnkBoton']").attr('disabled', 'disabled');

        document.getElementById("lblCamposObligatorios").style.display = "none"
        document.getElementById("divForm2").style.display = 'none';
        document.getElementById("divForm3").style.display = 'none';

        i = 0;
        bSalir = false;
        while (bSalir == false) {
            if (document.getElementById("uwtGrupos_div" + i)) {
                document.getElementById("uwtGrupos_div" + i).style.visibility = 'hidden';
                i = i + 1;
            }
            else {
                bSalir = true;
            }
        }
        document.getElementById("lblProgreso").value = document.forms["frmAlta"].elements["cadenaespera"].value;
        document.getElementById("divProgreso").style.display = 'inline';
    }
    /*
    Descripcion: Nos muestra nuevas opciones de menu ("Eliminar", "Impr./Exp.") y oculta el div de espera
    Llamada desde= PonerIdContrato() y cmdCancelar de guardarinstancia.aspx
    Tiempo ejecucion:=0,seg. 
    */
    function HabilitarBotones() {
        if (document.forms["frmAlta"].elements["hid_IdContrato"].value != "") {
            if (document.getElementById("Eliminar")) {
                document.getElementById("Eliminar").style.display = 'inline';
            }
            if (document.getElementById("ImpExp"))
                document.getElementById("ImpExp").style.display = 'inline';
        }

        OcultarEspera()
    }
    /*Descripcion:Funcion que oculta o muestra el panel con la informacion de las alertas
    del contrato.
    Llamada desde:=Click imagenes.
    Tiempo ejecucion:=0seg.*/
    function OcultarAlertas() {
        if (document.getElementById("pnlAlertasTabla").style.display == "block") {
            document.getElementById("pnlAlertasTabla").style.display = "none";
            document.getElementById("imgCollapseAlertas").style.display = "none";
            document.getElementById("imgExpandAlertas").style.display = "block";
        } else {
            document.getElementById("pnlAlertasTabla").style.display = "block";
            document.getElementById("imgCollapseAlertas").style.display = "block";
            document.getElementById("imgExpandAlertas").style.display = "none";
        }
    }
    /*
    Descripcion: Elimina un contrato
    Llamada desde= Funcion que se ejecuta al pulsar en la accion de menu "Eliminar"
    Tiempo ejecucion:=0,1seg. 
    */
    function EliminarInstancia() {
        if (confirm(arrTextosML[1]) == true)
            window.open("eliminarContrato.aspx?Alta=1&Instancia=" + document.forms["frmAlta"].elements["NEW_Instancia"].value + "&Contrato=" + document.forms["frmAlta"].elements["hid_IdContrato"].value, "iframeWSServer")
        return false;
    }
    /*Descripcion: Funcion que se encarga de completar la cadena con los campos del formulario
                 Contendra los campos de la cabecera del contrato
    Llamada desde= Guardar() // MontarSubmitAccion
    Tiempo ejecucion:=0,2seg.*/
    function CompletarFormulario() {
        var frmAltaElements = document.forms["frmAlta"].elements;
        var frmSubmitElements = document.forms["frmSubmit"].elements;

        frmSubmitElements["GEN_Instancia"].value = frmAltaElements["NEW_Instancia"].value
        frmSubmitElements["DeDonde"].value = 'CONTRATOS'
        frmSubmitElements["ID_CONTRATO"].value = frmAltaElements["hid_IdContrato"].value
        frmSubmitElements["COD_CONTRATO"].value = frmAltaElements["hid_CodContrato"].value

        frmSubmitElements["PathContrato"].value = frmAltaElements["hid_PathContrato"].value
        frmSubmitElements["NombreContrato"].value = frmAltaElements["hid_NombreContrato"].value
        frmSubmitElements["ProcesoContrato"].value = frmAltaElements["hid_ProcesoContrato"].value
        frmSubmitElements["GruposContrato"].value = frmAltaElements["hid_GruposContrato"].value
        frmSubmitElements["ItemsContrato"].value = frmAltaElements["hid_ItemsContrato"].value
        //EmpresaID
        oIdEmpresa = document.getElementById('<%= hidEmpresa.clientID %>');
        if (oIdEmpresa)
            frmSubmitElements["GEN_EmpresaID"].value = oIdEmpresa.value

        //CodProve
        frmSubmitElements["GEN_PROVE"].value = frmAltaElements["hid_Proveedor"].value

        //ContactoID
        var wdd = $find("wddContactos")
        if (wdd) {
            for (j = 0; j < wdd.get_selectedItems().length; j++) {
                frmSubmitElements["GEN_ContactoID"].value = wdd.get_selectedItems()[j].get_value();
            }

        }
        //Monedas
        var wdd = $find("wddMonedas")
        if (wdd) {
            for (j = 0; j < wdd.get_selectedItems().length; j++) {
                if (wdd.get_selectedItems()[j].get_text() == wdd.get_currentValue())
                    frmSubmitElements["GEN_CodMoneda"].value = wdd.get_selectedItems()[j].get_value();
            }

        }
        //Fechas Inicio - Fin Contrato
        oEntry = fsGeneralEntry_getById('<%= fsentryFecInicio.clientID %>')
        if (oEntry) {
            fechaAux = oEntry.getValue();
            if (fechaAux != null)
                fechaAux = fechaAux.getDate() + "/" + (fechaAux.getMonth() + 1) + "/" + (fechaAux.getFullYear())
            frmSubmitElements["GEN_FechaIni"].value = fechaAux
        }

        oEntry = fsGeneralEntry_getById('<%= fsentryFecFin.clientID %>')
        if (oEntry) {
            fechaAux = oEntry.getValue();
            if (fechaAux != null)
                fechaAux = fechaAux.getDate() + "/" + (fechaAux.getMonth() + 1) + "/" + (fechaAux.getFullYear())
            else
                fechaAux = ""
            frmSubmitElements["GEN_FechaFin"].value = fechaAux;
        }

        iDias = frmAltaElements["txtAlerta"].value

        var wdd = $find("wddPeriodo")
        iPeriodo = 1
        if (wdd) {
            for (j = 0; j < wdd.get_selectedItems().length; j++)
                iPeriodo = wdd.get_selectedItems()[j].get_value();

            if (iPeriodo > 1) {
                if (iPeriodo == 2)
                    iDias = iDias * 7
                else
                    iDias = iDias * 30
            }
        }
        frmSubmitElements["GEN_PeriodoAlerta"].value = iPeriodo
        frmSubmitElements["GEN_Alerta"].value = iDias

        iDias = frmAltaElements["txtPeriodoEmail"].value
        var wdd = $find("wddEmail")
        iPeriodo = 1
        if (wdd) {
            for (j = 0; j < wdd.get_selectedItems().length; j++)
                iPeriodo = wdd.get_selectedItems()[j].get_value();

            if (iPeriodo > 1) {
                if (iPeriodo == 2)
                    iDias = iDias * 7
                else
                    iDias = iDias * 30
            }
        }
        frmSubmitElements["GEN_PeriodoRepetirEmail"].value = iPeriodo
        frmSubmitElements["GEN_RepetirEmail"].value = iDias

        var wDropDownNotificados = $find("wddNotificados")
        var cadenaIdsContacto = ""
        for (j = 0; j < wDropDownNotificados.get_selectedItems().length; j++) {
            cadenaIdsContacto = cadenaIdsContacto + wDropDownNotificados.get_selectedItems()[j].get_value() + "#";
        }
        frmSubmitElements["GEN_Notificados"].value = cadenaIdsContacto
        //Añadimos una variable al formulario que nos indica que va a tener la configuracion de GS
        sVariableConfiguracionGS = "<INPUT type=hidden name=ConfiguracionGS>\n"
        var hidGS = document.getElementById("<%=hidConfiguracionGS.ClientID %>")
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sVariableConfiguracionGS)
        frmSubmitElements["ConfiguracionGS"].value = hidGS.value;
    }
    /*Descripcion:Escribe en el campo de importe el codigo de la moneda seleccionada
    Llamada desde:=Evento que salta al cambiar el valor en el combo de moneda
    Tiempo ejecucion:=0seg.*/
    function CambioMoneda() {
        sCodMoneda = ''
        var wdd = $find("<%=wddMonedas.ClientID %>")
        if (wdd != null)
            sCodMoneda = wdd.get_currentValue();

        lblMonedaImporteV = document.getElementById('<%=lblMonedaImporte.clientID %>');
        if (lblMonedaImporteV) {
            lblMonedaImporteV.innerHTML = sCodMoneda
            lblMonedaImporteV.title = sCodMoneda
        }
    }
    /*  Descripcion: Elimina el contenido de empresa, al pulsar eliminar. En caso contrario solo lectura
        Parametros entrada:
            elem:la caja de texto
            event:Evento pulsado.
        Llamada desde: Evento que salta al pulsar la tecla de <-, suprimir
        Tiempo ejecucion:=0seg. */
    function ControlEmpresa(elem, event) {
        //tanto si escribe algo como si pulsa la tecla de <- o suprimir, limpiamos lo que hay en el hidden
        oIdEmpresa = document.getElementById('<%= hidEmpresa.clientID %>');
        if (oIdEmpresa)
            oIdEmpresa.value = "";
        if (event.keyCode == 8 || event.keyCode == 46) {  //quitamos el espacio || event.keyCode == 32) {
            elem.value = ""
            return false;
        } else
            return true;
    }
    /* Revisado por: blp. Fecha: 05/10/2011
    Descripcion: Elimina el contenido del proveedor y el contacto, al pulsar eliminar. En caso contrario solo lectura
    Parametros entrada:
    elem:la caja de texto
    event:Evento pulsado.
    Llamada desde: Evento que salta al pulsar la tecla de <-, suprimir
    Tiempo ejecucion:=0seg. */
    function ControlProveedor(elem, event) {
        if (event.keyCode == 8 || event.keyCode == 46 || event.keyCode == 32) {
            elem.value = ""
            oIdProveedor = document.getElementById('<%= hid_Proveedor.clientID %>');
            if (oIdProveedor)
                oIdProveedor.value = "";
            var wddContactos = $find("wddContactos")
            if (wddContactos) {
                wddContactos.loadItems(oIdProveedor.value);
                wddContactos = null;
            }
        }
        return false;
    }
    /*''' <summary>
        ''' Vuelve a la pagina anterior
        ''' </summary>
        ''' <remarks>Llamada desde: Cuando se pincha al enlace ("Volver"); Tiempo máximo:0,1</remarks>*/
    function HistoryHaciaAtras() {
        valor = document.getElementById("hid_DesdeInicio")
        if (valor) {
            if (valor.value == 1)
                window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>Inicio.aspx", "_self");
            else
                window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>PM/Contratos/AltaContratos.aspx", "_self");
        }
    }

    function localEval(s) {
        eval(s)
    }

    function adjustHeight() {
        var tab = igtab_getTabById('uwtGrupos');
        // <td> which is used as content pane
        var cp = document.getElementById(tab.ID + '_cp');
        // <table> of tab
        var table = tab.element;
        // <div> container of tab
        var container = table.parentNode;
        // height available for tab
        var height = container.clientHeight;
        if (!height) return;
        // difference between heights of tab and content pane
        var heightShift = tab._myHeightShift;
        // 4 - is adjustment for top/bottom borders of tab
        if (!heightShift)
            heightShift = tab._myHeightShift = (table.offsetHeight - cp.offsetHeight + 4);
        // calculate height for content pane (can be improved for different browsers)
        height -= heightShift;
        if (height < 0) return;
        // set height of content pane to make height of tab to fit with container
        cp.style.height = height + 'px';
    }

    var xmlHttp;
    /*''' <summary>
    ''' Crear el objeto para llamar con ajax a ComprobarEnProceso
    ''' </summary>
    ''' <remarks>Llamada desde: javascript ; Tiempo máximo: 0</remarks>*/
    function CreateXmlHttp() {
        // Probamos con IE
        try {
            // Funcionará para JavaScript 5.0
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (oc) {
                xmlHttp = null;
            }
        }

        // Si no se trataba de un IE, probamos con esto
        if (!xmlHttp && typeof XMLHttpRequest != "undefined") {
            xmlHttp = new XMLHttpRequest();
        }

        return xmlHttp;
    }
    //Creamos el objeto xmlHttpRequest
    CreateXmlHttp();

    /*''' <summary>
    ''' Hacer una validación a medida de cantidades antes de añadir lineas vinculadas
    ''' </summary>
    ''' <param name="sRoot">En q desglose, html, se van añadir lineas</param>
    ''' <param name="IdCampo">En q desglose, form_campo.id, se van añadir lineas</param>        
    ''' <param name="Row">Fila a añadir</param>
    ''' <remarks>Llamada desde: PMWeb2008/BuscadorSolicitudes.aspx.vb; Tiempo máximo:0,1</remarks>*/
    function VincularCopiarFilaVacia(sRoot, IdCampo, Row) {
        CreateXmlHttp();
        if (xmlHttp) {
            if (Row.getCellFromKey) {
                var params = "Instancia=" + Row.getCellFromKey("INSTANCIAORIGEN").getValue() + "&Desglose=" + Row.getCellFromKey("DESGLOSEORIGEN").getValue() + "&Linea=" + Row.getCellFromKey("LINEAORIGEN").getValue();
            } else {
                var params = "Instancia=" + Row.get_cellByColumnKey("INSTANCIAORIGEN").get_value() + "&Desglose=" + Row.get_cellByColumnKey("DESGLOSEORIGEN").get_value() + "&Linea=" + Row.get_cellByColumnKey("LINEAORIGEN").get_value();
            }
            xmlHttp.open("POST", "../_common/controlarLineaVinculada.aspx", false);
            xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            xmlHttp.send(params);

            //Tras q se ejecute sincronamente controlarLineaVinculada.aspx controlamos sus resultados y obramos en 
            //consecuencia.                            
            var retorno;
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                retorno = xmlHttp.responseText;
                if (retorno == 'OK') { //pasa la comprobación
                    copiarFilaVacia(sRoot, IdCampo, 0, Row)
                }
                else {
                    var newWindow = window.open("../_common/NoPasoControlarLineaVinculada.aspx?" + params, "_blank", "width=350,height=200,status=yes,resizable=no,top=200,left=300");
                }
            }
        }
    }
    /*''' <summary>
    ''' Añadir instancia vinculadas
    ''' </summary>
    ''' <param name="sRoot">En q desglose, html, se van añadir lineas</param>
    ''' <param name="IdCampo">En q desglose, form_campo.id, se van añadir lineas</param>        
    ''' <param name="sId">Id de la instancia vinculada</param>
    ''' <param name="sDen">Descrip de la instancia vinculada</param> 
    ''' <remarks>Llamada desde: PMWeb2008/BuscadorSolicitudes.aspx.vb; Tiempo máximo:0,1</remarks>*/
    function SeleccionarSolicitud(sRoot, IdCampo, sId, sDen) {
        copiarFilaVacia(sRoot, IdCampo, 0, null, sId, sDen);
    }
    //Recoge el ID de la empresa seleccionada con el autocompletar
    function selected_Empresa(sender, e) {
        oIdEmpresa = document.getElementById('<%= hidEmpresa.ClientID %>');
        if (oIdEmpresa)
            oIdEmpresa.value = e._value;
    }
    function mostrarMenuAcciones(event, i) {
        var p = $get("lnkBotonAccion" + i.toString()).getBoundingClientRect();
        igmenu_showMenu('uwPopUpAcciones', event, p.left, p.bottom);
        return false;
    }
</script>
<body id="my_body" runat="server" onresize="resize()">
    <form id="frmAlta" method="post" runat="server">
        <iframe id="iframeWSServer" style="z-index: 103; left: 8px; visibility: hidden; position: absolute; top: 300px"
            name="iframeWSServer" src="../blank.htm"></iframe>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <CompositeScript>
                <Scripts>
                    <asp:ScriptReference Path="../../../js/jquery/jquery.min.js" />
                    <asp:ScriptReference Name="ExtenderBase.BaseScripts.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Common.Common.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Compat.Timer.Timer.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Animation.Animations.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Animation.AnimationBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="PopupExtender.PopupBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="AutoComplete.AutoCompleteBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Path="../alta/js/AdjacentHTML.js" />
                    <asp:ScriptReference Path="../alta/js/jsAlta.js" />
                </Scripts>
            </CompositeScript>
        </asp:ScriptManager>
        <div>
            <table width="100%" cellpadding="0" border="0" onload="iniciar()">
                <tr>
                    <td>
                        <uc1:menu ID="Menu1" runat="server" OpcionMenu="Contratos" OpcionSubMenu="AltaContratos"></uc1:menu>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="pnlContrato" runat="server" BackColor="#f5f5f5" Font-Names="Arial">
                <table id="Table2" style="height: 15%; width: 100%; padding-bottom: 15px;" cellspacing="0" cellpadding="1" border="0">
                    <tr>
                        <td style="padding-top: 5px; padding-bottom: 5px;" class="fondoCabecera">
                            <table id="tblCabecera" style="width: 100%;" border="0">
                                <tr>
                                    <td style="width: 15%">
                                        <asp:Label ID="lblLitFechaInicio" runat="server" CssClass="captionBlue">(*) Fecha de inicio</asp:Label></td>
                                    <td style="width: 35%">
                                        <asp:Label ID="lblLitProveedor" runat="server" CssClass="captionBlue">(*) Proveedor</asp:Label></td>
                                    <td style="width: 30%">
                                        <asp:Label ID="lblLitContacto" runat="server" CssClass="captionBlue">Contacto</asp:Label></td>
                                    <td style="width: 20%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="width: 15%">
                                        <fsde:GeneralEntry ID="fsentryFecInicio" runat="server" Width="120px" Independiente="1"
                                            DropDownGridID="fsentryFecInicio" Tipo="TipoFecha" Tag="FecInicio">
                                            <InputStyle Width="130px" CssClass="TipoFecha" />
                                        </fsde:GeneralEntry>
                                    </td>
                                    <td style="width: 35%">
                                        <table style="background-color: #f5f5f5; width: 300px; height: 20px; border: solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <%  If Not String.IsNullOrEmpty(Request("ConfiguracionGS")) Then%>
                                                <td>
                                                    <asp:Label ID="lblProveedor" runat="server" CssClass="label" Style="width: 280px; overflow: hidden; white-space: nowrap;" Text="ESB201068357 SEIN BATU S.A."></asp:Label>
                                                </td>
                                                <td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px; width: 15px">
                                                    <asp:Image ID="imgInfProv" runat="server" />
                                                </td>
                                                <%Else%>
                                                <td>
                                                    <asp:TextBox ID="txtProveedor" runat="server" Width="280px" BorderWidth="0px"></asp:TextBox>
                                                </td>
                                                <td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px; width: 15px">
                                                    <asp:ImageButton ID="ImgProveedorLupa" runat="server" ImageUrl="./images/Img_Ico_Lupa.gif" CommandName="BuscadorProveedor" />
                                                </td>
                                                <%End If%>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 30%">
                                        <asp:UpdatePanel ID="upContactos" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                            <ContentTemplate>
                                                <ig:WebDropDown ID="wddContactos" runat="server" Width="250px" EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" DropDownContainerHeight="50px" DropDownContainerMaxHeight="100px" DropDownContainerWidth="250px" EnableDropDownAsChild="false"
                                                    OnItemsRequested="DropDown_ItemsRequested">
                                                </ig:WebDropDown>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td style="width: 20%" nowrap="nowrap">
                                        <asp:Label ID="lblImporte" runat="server" Text="Importe:" CssClass="captionBlue"></asp:Label>
                                        <asp:Label ID="lblImporteValor" runat="server" CssClass="label"></asp:Label>
                                        <asp:Label ID="lblMonedaImporte" runat="server" CssClass="label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%">
                                        <asp:Label ID="lblLitFechaExpiracion" runat="server" CssClass="captionBlue" Text="Fecha de expiracion"></asp:Label></td>
                                    <td style="width: 35%">
                                        <asp:Label ID="lblLitEmpresa" runat="server" CssClass="captionBlue" Text="(*) Empresa"></asp:Label></td>
                                    <td style="width: 30%">
                                        <asp:Label ID="lblLitMoneda" runat="server" CssClass="captionBlue" Text="(*) Moneda"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 15%">
                                        <fsde:GeneralEntry ID="fsentryFecFin" runat="server" Width="120px" Independiente="1"
                                            DropDownGridID="fsentryFecFin" Tipo="TipoFecha" Tag="FecFin">
                                            <InputStyle Width="130px" CssClass="TipoFecha" />
                                        </fsde:GeneralEntry>
                                    </td>
                                    <td>
                                        <table style="background-color: White; width: 300px; height: 20px; border: solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtEmpresa" runat="server" Width="280px" BorderWidth="0px">
                                                    </asp:TextBox>
                                                    <ajx:AutoCompleteExtender ID="txtEmpresa_AutoCompleteExtender" runat="server" CompletionSetCount="10" DelimiterCharacters="" Enabled="True"
                                                        MinimumPrefixLength="1" ServiceMethod="GetEmpresas" ServicePath="~/AutoCompletePMWEB.asmx" TargetControlID="txtEmpresa" EnableCaching="False" UseContextKey="true" OnClientItemSelected="selected_Empresa" CompletionListCssClass="autoCompleteList" CompletionListItemCssClass="autoCompleteListItem" CompletionListHighlightedItemCssClass="autoCompleteSelectedListItem">
                                                    </ajx:AutoCompleteExtender>
                                                    <asp:Label ID="lblEmpresa" runat="server" CssClass="label" Style="width: 280px; overflow: hidden; white-space: nowrap;"></asp:Label>
                                                </td>
                                                <td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px; width: 15px">
                                                    <asp:Image ID="imgInfEmpresa" runat="server" />
                                                    <asp:ImageButton ID="imgEmpresaLupa" runat="server" ImageUrl="./images/Img_Ico_Lupa.gif" CommandName="BuscadorEmpresa" />
                                                </td>
                                            </tr>
                                            <asp:HiddenField ID="hidEmpresa" runat="server" />
                                            <asp:HiddenField ID="hidConfiguracionGS" runat="server" />
                                        </table>
                                    </td>
                                    <td style="width: 30%">
                                        <ig:WebDropDown ID="wddMonedas" runat="server" Width="100px" EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" DropDownContainerHeight="100px" DropDownContainerWidth="100px" EnableDropDownAsChild="false" AutoFilterQueryType="StartsWith" EnableCustomValues="false" AutoSelectOnMatch="false">
                                            <ClientEvents ValueChanged="CambioMoneda" />
                                        </ig:WebDropDown>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:Label ID="lblCamposObligatorios" Style="z-index: 102; display: none;" runat="server" CssClass="captionRed">Los campos marcados con (*) son de obligada cumplimentacion</asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlAlertas" runat="server" Width="100%">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="middle" align="left" style="padding-top: 5px">
                                <asp:Panel runat="server" ID="pnlTituloAlertas" CssClass="Rectangulo">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
                                        <tr style="height: 20px;">
                                            <td>
                                                <asp:ImageButton runat="server" ID="imgCollapseAlertas" Style="display: none; cursor: hand;" hspace="5" />
                                                <asp:ImageButton runat="server" ID="imgExpandAlertas" Style="cursor: hand;" hspace="5" />
                                            </td>
                                            <td style="width: 100%;">
                                                <asp:Label ID="lblConfiguracionAlertas" runat="server" Width="100%" Text="Configuración alertas" Style="cursor: hand;" CssClass="captionBlue"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <!-- Tabla alertas -->
                <asp:Panel ID="pnlAlertasTabla" runat="server" CssClass="Rectangulo" Style="display: none; width: 90%">
                    <div style="padding-left: 40px; padding-top: 10px">
                        <fieldset>
                            <table width="60%" border="0" cellpadding="4" cellspacing="0" style="padding-left: 10px">
                                <tr>
                                    <td nowrap="nowrap">
                                        <asp:Label ID="lblMostrarAlerta" runat="server" Text="Mostrar Alerta" CssClass="caption"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAlerta" runat="server" Text="0" Width="40px"></asp:TextBox>
                                    </td>
                                    <td style="width: 10px">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:Image ID="imgArribaAlerta" runat="server" ImageUrl="~/App_Pages/PMWEB/contratos/images/flecha_arriba.GIF" /></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Image ID="imgAbajoAlerta" runat="server" ImageUrl="~/App_Pages/PMWEB/contratos/images/flecha_abajo.GIF" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <ig:WebDropDown ID="wddPeriodo" runat="server" Width="100px" EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" DropDownContainerHeight="100px" DropDownContainerWidth="100px" EnableDropDownAsChild="false" EnableCustomValues="false">
                                        </ig:WebDropDown>
                                    </td>
                                    <td nowrap="nowrap">
                                        <asp:Label ID="lblAntesExpirar" runat="server" Text="antes de expirar" CssClass="caption"></asp:Label>
                                    </td>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="lblEmail" runat="server" Text="Enviar email cada:" CssClass="caption"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPeriodoEmail" runat="server" Text="0" Width="40px"></asp:TextBox>
                                    </td>
                                    <td style="width: 10px">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:Image ID="imgArribaAlerta2" runat="server" ImageUrl="~/App_Pages/PMWEB/contratos/images/flecha_arriba.GIF" /></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Image ID="imgAbajoAlerta2" runat="server" ImageUrl="~/App_Pages/PMWEB/contratos/images/flecha_abajo.GIF" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <ig:WebDropDown ID="wddEmail" runat="server" Width="100px" EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" DropDownContainerHeight="100px" DropDownContainerWidth="100px" EnableDropDownAsChild="false" EnableCustomValues="false">
                                        </ig:WebDropDown>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap="nowrap">
                                        <asp:Label ID="lblNotificados" runat="server" Text="Configurar notificados:" CssClass="caption"></asp:Label>
                                    </td>
                                    <td nowrap="nowrap" colspan="9">
                                        <table cellpadding="0" cellspacing="0" border="0" style="width: 100%">
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel ID="updNotificadores" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="imgBuscarNotificados" EventName="Click" />
                                                        </Triggers>
                                                        <ContentTemplate>
                                                            <ig:WebDropDown ID="wddNotificados" runat="server" Width="450px" Height="20px"
                                                                EnableMultipleSelection="True" EnableClosingDropDownOnSelect="False" EnableDropDownAsChild="false"
                                                                OnItemsRequested="DropDown_ItemsRequested" MultiSelectValueDelimiter=";">
                                                            </ig:WebDropDown>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                                <td style="width: 100%; padding-left: 3px;">
                                                    <asp:ImageButton ID="imgBuscarNotificados" runat="server" ImageUrl="~/App_Pages/PMWEB/contratos/images/Img_Ico_Lupa.gif" Style="cursor: hand;" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                    <ajx:NumericUpDownExtender ID="NumericUpDownExtender1" runat="server"
                        TargetControlID="txtAlerta" Width="40" TargetButtonDownID="imgAbajoAlerta"
                        TargetButtonUpID="imgArribaAlerta" Minimum="0"
                        RefValues="" ServiceDownMethod="" ServiceUpMethod="" />

                    <ajx:NumericUpDownExtender ID="NumericUpDownExtender2" runat="server"
                        TargetControlID="txtPeriodoEmail" Width="40" TargetButtonDownID="imgAbajoAlerta2"
                        TargetButtonUpID="imgArribaAlerta2" Minimum="0"
                        RefValues="" ServiceDownMethod="" ServiceUpMethod="" />
                </asp:Panel>
            </asp:Panel>
            <br />
            <ajx:DropShadowExtender TrackPosition="true" ID="DropShadowExtender1"
                runat="server" Opacity="0.5" Width="3" TargetControlID="pnlContrato"
                Rounded="true">
            </ajx:DropShadowExtender>
        </div>
        <div id="divProgreso" style="display: inline;" class="fondoGS">
            <table id="tblProgreso" cellspacing="0" cellpadding="0" width="101%" border="0" runat="server" class="fondoGS">
                <tr style="height: 30px">
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:TextBox ID="lblProgreso" Style="text-align: center" runat="server" Width="100%"
                            CssClass="fondoGS" BorderWidth="0" BorderStyle="None">Su solicitud está siendo tramitada. Espere unos instantes...</asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:Image ID="imgProgreso" runat="server" src="../_common/images/cursor-espera_grande.gif"></asp:Image>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divForm2">
            <table cellspacing="3" cellpadding="3" width="100%" border="0">
                <tr>
                    <td width="100%" colspan="4" valign="top">
                        <igtab:UltraWebTab ID="uwtGrupos" Style="z-index: 101;" runat="server" BorderStyle="Solid" BorderWidth="1px"
                            Width="100%" EnableViewState="False" ThreeDEffect="False" DummyTargetUrl=" " FixedLayout="True"
                            CustomRules="padding:10px;" DisplayMode="Scrollable">
                            <DefaultTabStyle Height="24px" CssClass="uwtDefaultTab">
                                <Padding Left="20px" Right="20px"></Padding>
                            </DefaultTabStyle>
                            <RoundedImage NormalImage="ig_tab_blueb2.gif" HoverImage="ig_tab_blueb1.gif" FillStyle="LeftMergedWithCenter"></RoundedImage>
                        </igtab:UltraWebTab>
                    </td>
                </tr>
            </table>
            <input id="Solicitud" type="hidden" name="Solicitud" runat="server" />
            <input id="Bloque" type="hidden" name="Bloque" runat="server" />
            <input id="hid_IdContrato" type="hidden" name="hid_IdContrato" runat="server" />
            <input id="hid_CodContrato" type="hidden" name="hid_CodContrato" runat="server" />
            <input id="cadenaespera" type="hidden" name="cadenaespera" runat="server" />
            <input id="BotonCalcular" type="hidden" value="0" name="BotonCalcular" runat="server" />
            <input id="PantallaMaper" type="hidden" name="PantallaMaper" runat="server" />
            <input id="NEW_Instancia" type="hidden" name="NEW_Instancia" runat="server" />
            <input id="hid_Proveedor" type="hidden" runat="server" />
            <input id="hid_ProcesoContrato" type="hidden" runat="server" />
            <input id="hid_GruposContrato" type="hidden" runat="server" />
            <input id="hid_ItemsContrato" type="hidden" runat="server" />
            <input id="hid_NombreContrato" type="hidden" runat="server" />
            <input id="hid_PathContrato" type="hidden" runat="server" />
            <input id="hid_DesdeInicio" type="hidden" runat="server" />
            <input id="IdInstanciaImportar" type="hidden" name="IdInstanciaImportar" runat="server" />

            <input id="PantallaVinculaciones" type="hidden" name="PantallaVinculaciones" runat="SERVER" />
        </div>
        <div id="divAcciones" runat="server">
            <ignav:UltraWebMenu ID="uwPopUpAcciones" runat="server" WebMenuTarget="PopupMenu" SubMenuImage="ig_menuTri.gif"
                ScrollImageTop="ig_menu_scrollup.gif" Cursor="Default" ScrollImageBottomDisabled="ig_menu_scrolldown_disabled.gif" ScrollImageTopDisabled="ig_menu_scrollup_disabled.gif"
                ScrollImageBottom="ig_menu_scrolldown.gif" AutoPostBack="false">
                <MenuClientSideEvents InitializeMenu="" ItemChecked="" ItemClick="" SubMenuDisplay="" ItemHover=""></MenuClientSideEvents>
                <DisabledStyle ForeColor="LightGray"></DisabledStyle>
                <HoverItemStyle Cursor="Hand"></HoverItemStyle>
                <IslandStyle Cursor="Default" BorderWidth="1px" Font-Size="8pt" Font-Names="MS Sans Serif" BorderStyle="Outset"
                    ForeColor="Black" BackColor="LightGray">
                </IslandStyle>
                <ExpandEffects ShadowColor="LightGray"></ExpandEffects>
                <TopSelectedStyle Cursor="Default"></TopSelectedStyle>
                <SeparatorStyle BackgroundImage="ig_menuSep.gif"
                    CustomRules="background-repeat:repeat-x; "></SeparatorStyle>
                <Levels>
                    <ignav:Level Index="0"></ignav:Level>
                </Levels>
            </ignav:UltraWebMenu>
        </div>
        <fsn:FSNPanelInfo ID="FSNPanelDatosProveedor" runat="server" ServicePath="~/App_Pages/PMWEB/ConsultasPMWEB.asmx" ServiceMethod="Obtener_DatosProveedor" TipoDetalle="0"></fsn:FSNPanelInfo>
        <fsn:FSNPanelInfo ID="FSNPanelDatosEmpresa" runat="server" ServicePath="~/App_Pages/PMWEB/ConsultasPMWEB.asmx" ServiceMethod="Obtener_DatosEmpresa" TipoDetalle="0"></fsn:FSNPanelInfo>
    </form>
    <div id="divForm3" style="display: none">
        <form id="frmCalculados" name="frmCalculados" action="../alta/recalcularimportes.aspx?desde=contratos" method="post" target="fraWSServer"></form>
        <form id="frmDesglose" name="frmDesglose" method="post" target="winDesglose"></form>
    </div>
    <div id="divAlta" style="visibility: hidden"></div>
    <div id="divDropDowns" style="z-index: 101; visibility: hidden; position: absolute; top: 300px"></div>
    <div id="divCalculados" style="z-index: 107; visibility: hidden; position: absolute; top: 0px"></div>
</body>
</html>
