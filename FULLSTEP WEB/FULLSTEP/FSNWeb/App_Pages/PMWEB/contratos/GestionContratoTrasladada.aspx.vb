﻿Partial Public Class GestionContratoTrasladada
    Inherits FSNPage


#Region " Web Form Designer Generated Code "



    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private _oInstancia As FSNServer.Instancia
    Protected ReadOnly Property oInstancia() As FSNServer.Instancia
        Get
            If _oInstancia Is Nothing Then
                If Me.IsPostBack Then
                    _oInstancia = CType(Cache("oInstancia" & FSNUser.Cod), FSNServer.Instancia)
                Else
                    _oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                    _oInstancia.ID = Request("Instancia")

                    'Carga los datos de la instancia:
                    _oInstancia.Load(Idioma, True)
                    _oInstancia.Solicitud.Load(Idioma)
                    _oInstancia.DevolverEtapaActualTraslado(Idioma)
                    Me.InsertarEnCache("oInstancia" & FSNUser.Cod, _oInstancia)
                End If
            End If
            Return _oInstancia
        End Get
    End Property

    Private _oInstanciaGrupos As FSNServer.Grupos
    Protected ReadOnly Property oInstanciaGrupos() As FSNServer.Grupos
        Get
            If _oInstanciaGrupos Is Nothing Then
                If Me.IsPostBack Then
                    _oInstanciaGrupos = CType(Cache("oInstanciaGrupos_" & FSNUser.Cod), FSNServer.Grupos)
                Else
                    _oInstancia.CargarCamposInstancia(Idioma, Usuario.CodPersona, , True)
                    _oInstanciaGrupos = oInstancia.Grupos

                    Me.InsertarEnCache("oInstanciaGrupos_" & FSNUser.Cod, _oInstanciaGrupos)
                End If
            End If
            Return _oInstanciaGrupos
        End Get
    End Property

    Private _bSoloLectura As Boolean = False
    Protected Property bSoloLectura() As Boolean
        Get
            ViewState("bSoloLectura") = _bSoloLectura
            Return ViewState("bSoloLectura")
        End Get
        Set(ByVal value As Boolean)
            _bSoloLectura = value
        End Set
    End Property

    Private _oContrato As FSNServer.Contrato
    Protected ReadOnly Property oContrato() As FSNServer.Contrato
        Get
            If _oContrato Is Nothing Then
                If Me.IsPostBack Then
                    _oContrato = CType(Cache("oContrato" & FSNUser.Cod), FSNServer.Contrato)
                Else
                    _oContrato = FSNServer.Get_Object(GetType(FSNServer.Contrato))
                    _oContrato.ID = Request("Contrato")
                    hid_Contrato.Value = Request("Contrato")
                    _oContrato.Codigo = Request("Codigo")
                    'Carga los datos del contrato:
                    _oContrato.Load(Idioma)
                    hid_CodMoneda.Value = _oContrato.CodMoneda
                    Me.InsertarEnCache("oContrato" & FSNUser.Cod, _oContrato)
                End If
            End If
            Return _oContrato
        End Get
    End Property

#End Region

    Private Const IncludeScriptFormat As String = ControlChars.CrLf & _
"<script type=""{0}"" src=""{1}""></script>"
    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
"<script language=""{0}"">{1}</script>"

    Private arrOrdenEstados(7) As String
    Private arrTextosEstados(7) As String

    ''' <summary>
    '''Muestra la pagina de que la instancia ha sido trasladada
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">evento de sistema</param>           
    ''' <remarks>Tiempo máximo: 0,3</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.Expires = -1

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.GestionContratoTraslado

        RegistrarScripts()

        If Page.IsPostBack Then
            CrearCamposDinamicos()
            Exit Sub
        End If


        Me.Bloque.Value = oInstancia.Etapa
        Me.Version.Value = oInstancia.Version
        Me.Instancia.Value = oInstancia.ID
        Me.txtPeticionario.Value = oInstancia.Peticionario
        Me.txtIdTipo.Value = oInstancia.Solicitud.ID
        Me.txtComent.Text = oInstancia.ComentarioEstado

        CargarTextos()
        VisualizarLinkFlujo()
        MostrarImporteContrato()

        CargarDatosContrato()
        ConfigurarCabecera()
        CrearCamposDinamicos()

        Me.FindControl("frmDetalle").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())

    End Sub

    ''' <summary>
    ''' Configura la cabecera, muestra en el menu las posibles acciones
    ''' Activamos los botones que pueden aparecer en la pantalla
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load; Tiempo máximo:=0,1seg.</remarks>
    Private Sub ConfigurarCabecera()
        FSNPageHeader.TextoBotonCalcular = Textos(17)
        FSNPageHeader.TextoBotonGuardar = Textos(19)
        FSNPageHeader.TextoBotonImpExp = Textos(20)
        FSNPageHeader.TextoBotonVolver = Textos(21)
        FSNPageHeader.TextoBotonEliminar = Textos(22)
        FSNPageHeader.TextoBotonTrasladar = Textos(31)
        FSNPageHeader.TextoBotonDevolver = Textos(32)

        FSNPageHeader.VisibleBotonImpExp = True
        FSNPageHeader.VisibleBotonGuardar = True
        FSNPageHeader.VisibleBotonDevolver = True
        FSNPageHeader.VisibleBotonVolver = True
        If Request("OcultarBotonVolver") = "1" Then FSNPageHeader.VisibleBotonVolver = False
        If FSNUser.PMPermitirTraslados AndAlso oInstancia.PermitirTraslados Then FSNPageHeader.VisibleBotonTrasladar = True

        FSNPageHeader.OnClientClickCalcular = "return CalcularCamposCalculados();return false;"
        FSNPageHeader.OnClientClickImpExp = "return cmdImpExp_onclick();return false;"
        FSNPageHeader.OnClientClickGuardar = "return Guardar();return false;"
        FSNPageHeader.OnClientClickTrasladar = "return Trasladar();return false;"
        FSNPageHeader.OnClientClickDevolver = "return Devolver();return false;"
        If Request("desde") = "SeguimientoSolicitudesPM" Then
            FSNPageHeader.OnClientClickVolver = "return Volver(1);return false;"
        ElseIf Request("desde") = "Inicio" Then 'desde webpart
            FSNPageHeader.OnClientClickVolver = "return Volver(3);return false;"
        Else 'desde visor de contratos
            FSNPageHeader.OnClientClickVolver = "return Volver(2);return false;"
        End If

        If Request("ConfiguracionGS") = "1" Then
            'Estilo de la pagina
            FSNPageHeader.AspectoGS = True
            mi_body.Attributes.Add("class", "fondoGS")
            Menu1.Visible = False
        Else
            FSNPageHeader.AspectoGS = False
            mi_body.Attributes.Add("class", "")
            Menu1.Visible = True
        End If

        mi_body.Attributes.Add("onload", "inicializar();")
        mi_body.Attributes.Add("onunload", "finalizar();")

        If oContrato.Nombre <> Nothing Then
            FSNPageHeader.TituloCabecera = oContrato.Nombre
        Else
            FSNPageHeader.TituloCabecera = oInstancia.Solicitud.Den(Idioma)
        End If
        FSNPageHeader.UrlImagenCabecera = "images/Contrato.png"

        If FSNUser.AccesoCN Then
            If Not Page.ClientScript.IsClientScriptBlockRegistered("EntidadColaboracion") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "EntidadColaboracion", _
                    "var TipoEntidadColaboracion='CO'; var CodigoEntidadColaboracion ={identificador:" & oInstancia.ID & _
                    ",texto:'" & JSText(oInstancia.ID & " - " & IIf(oContrato.Nombre Is Nothing, oInstancia.Solicitud.Den(Idioma), oContrato.Nombre)) & "'};", True)
            End If
        End If

        VisualizarOpcionesTraslado()
    End Sub

    ''' <summary>
    ''' Muestra las opciones de traslado
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,2seg</remarks>
    Private Sub VisualizarOpcionesTraslado()

        If oInstancia.DestinatarioEst = Usuario.CodPersona Or oInstancia.EsSustitutodeDestinatario(Usuario.CodPersona, oInstancia.DestinatarioEst) Then
            'Le han trasladado la solicitud y la tiene que cumplimentar:
            oInstancia.DevolverDatosTraslado(Usuario.CodPersona)
            Me.HyperDetalle.Visible = False
            Me.txtTraslado.Value = oInstancia.PersonaEst
        Else
            'Es una solicitud trasladada a otra persona, no podrá hacer nada:
            oInstancia.DevolverDatosTraslado(Usuario.CodPersona, True)
            FSNPageHeader.VisibleBotonDevolver = False
            FSNPageHeader.VisibleBotonGuardar = False
            FSNPageHeader.VisibleBotonTrasladar = False
            FSNPageHeader.VisibleBotonCalcular = False
            Me.txtTraslado.Value = oInstancia.DestinatarioEst
            bSoloLectura = True
        End If

    End Sub
    ''' <summary>
    ''' Nos muestra o no el link para poder ver el flujo por el que ha ido la instancia
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0,seg.</remarks>
    Private Sub VisualizarLinkFlujo()
        'hipervínculo para el detalle del workflow
        If oInstancia.Solicitud.Workflow = Nothing Then  'Si no hay workflow no muestra el hipervínculo
            Me.HyperDetalle.Visible = False
        Else
            If oInstancia.VerDetalleFlujo = True Then
                HyperDetalle.Visible = True
                Me.HyperDetalle.NavigateUrl = "../seguimiento/NWhistoricoestados.aspx?Instancia=" + CStr(oInstancia.ID) & "&ConfiguracionGS=" & Request("ConfiguracionGS") & "&Codigo=" & oContrato.Codigo
            Else
                Me.HyperDetalle.Visible = False
            End If
        End If
    End Sub
    ''' <summary>
    ''' Registra las variables que vamos a utilizar en javascript
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub RegistrarScripts()
        Dim sClientTextVars As String
        sClientTextVars = ""
        sClientTextVars += "vdecimalfmt='" + Usuario.DecimalFmt + "';"
        sClientTextVars += "vthousanfmt='" + Usuario.ThousanFmt + "';"
        sClientTextVars += "vprecisionfmt='" + Usuario.PrecisionFmt + "';"
        sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsUser", sClientTextVars)

        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(Textos(28)) + "';" '"Se detectó un contenido potencialmente peligroso. Por favor modifique el siguiente contenido: $$$"
        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM2008", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")

        If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayDesgloses") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")
        End If

    End Sub
    ''' <summary>
    ''' Carga los campos de la instancia
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo máximo:(Depende de los campos de la isntancia)</remarks>
    Private Sub CrearCamposDinamicos()
        Dim oRow As DataRow
        Dim lIndex As Integer = 0
        Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oDS As DataSet
        Dim oucCampos As campos
        Dim oucDesglose As desgloseControl

        Dim oGrupo As FSNServer.Grupo
        Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab

        'Rellena el tab :
        uwtGrupos.Tabs.Clear()
        AplicarEstilosTab(uwtGrupos)

        If Not oInstanciaGrupos.Grupos Is Nothing Then
            'SI EXISTE ALGUN CAMPO DE TIPO=3 (CALCULADO) HACEMOS VISIBLE EL BOTON DE CALCULAR
            FSNPageHeader.VisibleBotonCalcular = oInstanciaGrupos.Grupos.OfType(Of FSNServer.Grupo).Where(Function(x) x.DSCampos.Tables(0).Rows.OfType(Of DataRow).Where(Function(y) y("TIPO") = TipoCampoPredefinido.Calculado).Any).Any()

            For Each oGrupo In oInstanciaGrupos.Grupos
                oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden

                oInputHidden.ID = "txtPre_" + lIndex.ToString
                oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
                Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
                oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(Idioma), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
                oTabItem.Key = lIndex.ToString
                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Hidden
                uwtGrupos.Tabs.Add(oTabItem)
                lIndex += 1

                If oGrupo.DSCampos.Tables.Count > 0 Then
                    If oGrupo.NumCampos <= 1 Then
                        For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                            If oRow.Item("VISIBLE") = 1 Then
                                Exit For
                            End If
                        Next
                        If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Desglose Or (DBNullToSomething(oRow.Item("SUBTIPO")) = TiposDeDatos.TipoGeneral.TipoDesglose And DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.DesgloseActividad) Then
                            If oRow.Item("VISIBLE") = 0 Then
                                uwtGrupos.Tabs.Remove(oTabItem)
                            Else
                                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                                oTabItem.ContentPane.UserControlUrl = "..\alta\desglose.ascx"
                                oucDesglose = oTabItem.ContentPane.UserControl
                                oucDesglose.ID = oGrupo.Id.ToString
                                oucDesglose.Campo = oRow.Item("ID_CAMPO")
                                oucDesglose.TabContainer = uwtGrupos.ClientID
                                oucDesglose.Instancia = oInstancia.ID
                                oucDesglose.TieneIdCampo = True
                                oucDesglose.Ayuda = DBNullToSomething(oRow.Item("AYUDA_" & Idioma))
                                oucDesglose.Version = oInstancia.Version
                                oucDesglose.SoloLectura = bSoloLectura Or (oRow.Item("ESCRITURA") = 0)
                                oucDesglose.PM = True
                                oucDesglose.Titulo = DBNullToSomething(oRow.Item("DEN_" & Idioma))
                                oucDesglose.TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Contrato
                                oInputHidden.Value = oucDesglose.ClientID
                            End If
                        Else
                            oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                            oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                            oucCampos = oTabItem.ContentPane.UserControl
                            oucCampos.Instancia = oInstancia.ID
                            oucCampos.ID = oGrupo.Id.ToString
                            oucCampos.dsCampos = oGrupo.DSCampos
                            oucCampos.IdGrupo = oGrupo.Id
                            oucCampos.Idi = Idioma
                            oucCampos.TabContainer = uwtGrupos.ClientID
                            oucCampos.Version = oInstancia.Version
                            oucCampos.SoloLectura = bSoloLectura
                            oucCampos.PM = True
                            oucCampos.Formulario = oInstancia.Solicitud.Formulario
                            oucCampos.ObjInstancia = oInstancia
                            oucCampos.InstanciaMoneda = oInstancia.Moneda
                            oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                            oInputHidden.Value = oucCampos.ClientID
                        End If

                    Else
                        oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                        oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                        oucCampos = oTabItem.ContentPane.UserControl
                        oucCampos.Instancia = oInstancia.ID
                        oucCampos.ID = oGrupo.Id.ToString
                        oucCampos.dsCampos = oGrupo.DSCampos
                        oucCampos.IdGrupo = oGrupo.Id
                        oucCampos.Idi = Idioma
                        oucCampos.TabContainer = uwtGrupos.ClientID
                        oucCampos.Version = oInstancia.Version
                        oucCampos.SoloLectura = bSoloLectura
                        oucCampos.PM = True
                        oucCampos.Formulario = oInstancia.Solicitud.Formulario
                        oucCampos.ObjInstancia = oInstancia
                        oucCampos.InstanciaMoneda = oInstancia.Moneda
                        oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                        oInputHidden.Value = oucCampos.ClientID
                    End If
                End If
                Me.FindControl("frmDetalle").Controls.Add(oInputHidden)

            Next
        End If
    End Sub

    ''' <summary>
    ''' Carga el idioma correspondiente el los controles
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0seg.</remarks>
    Private Sub CargarTextos()

        lblLitFechaInicio.Text = Textos(0) & ":" '"Fecha de inicio"
        lblLitFechaExpiracion.Text = Textos(1) & ":" '"Fecha de expiración"
        'lblLitProveedor.Text = "(*) " & Textos(2) '"Proveedor"
        lblLitContacto.Text = Textos(3) & ":" '"Contacto"
        lblLitEmpresa.Text = Textos(4) & ":" '"Empresa"
        lblLitMoneda.Text = Textos(5) & ":" '"Moneda"

        lblLitMostrarAlerta.Text = Textos(7) & ":" '"Mostrar alerta"
        lblLitRecordatorioAlerta.Text = Textos(11) & ":" '"Mostrar alerta"
        lblLitNotificados.Text = Textos(12) & ":" '"Configurar notificados" & ":"

        Me.cadenaespera.Value = Textos(23) '"Su solicitud está siendo tramitada. Espere unos instantes..."
        Me.lblProgreso.Text = Textos(24) '"Se está cargando la solicitud. Espere unos instantes..."
        HyperDetalle.Text = Textos(30)

        arrOrdenEstados(0) = TiposDeDatos.EstadosVisorContratos.Guardado
        arrOrdenEstados(1) = TiposDeDatos.EstadosVisorContratos.En_Curso_De_Aprobacion
        arrOrdenEstados(2) = TiposDeDatos.EstadosVisorContratos.Vigentes
        arrOrdenEstados(3) = TiposDeDatos.EstadosVisorContratos.Proximo_a_Expirar
        arrOrdenEstados(4) = TiposDeDatos.EstadosVisorContratos.Expirados
        arrOrdenEstados(5) = TiposDeDatos.EstadosVisorContratos.Rechazados
        arrOrdenEstados(6) = TiposDeDatos.EstadosVisorContratos.Anulados

        arrTextosEstados(0) = Textos(33) '"Guardados"
        arrTextosEstados(1) = Textos(34) '"En curso de aprobaciÃƒÂ³n"
        arrTextosEstados(2) = Textos(35) '"Vigentes"
        arrTextosEstados(3) = Textos(36) '"proximo a expirar"
        arrTextosEstados(4) = Textos(37) '"Expirados"
        arrTextosEstados(5) = Textos(38) '"Rechazados"
        arrTextosEstados(6) = Textos(39) '"Anulados"

        'Textos Panel info-s
        FSNPanelDatosProveedor.Titulo = Textos(2) '"Proveedor"
        FSNPanelDatosProveedor.SubTitulo = Textos(40) '"Informacion detallada"


        FSNPanelDatosPeticionario.Titulo = Textos(41) '"Peticionario"
        FSNPanelDatosPeticionario.SubTitulo = Textos(40) '"Informacion detallada"

        FSNPanelDatosEmpresa.Titulo = Textos(4) '"Empresa"
        FSNPanelDatosEmpresa.SubTitulo = Textos(40) '"Informacion detallada"



    End Sub

    ''' <summary>
    ''' Carga la informacion del contrato en los campos
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub CargarDatosContrato()
        Dim sEstado As String
        Dim i As Byte
        If oContrato.Estado >= 0 Then
            For i = 0 To UBound(Me.arrOrdenEstados)
                If oContrato.Estado = arrOrdenEstados(i) Then
                    sEstado = arrTextosEstados(i)
                    Exit For
                End If
            Next
            If sEstado <> "" Then
                sEstado = " (" & sEstado & ")"
            End If
        End If

        lblIDInstanciayEstado.Text = oContrato.Codigo & sEstado




        lblFechaInicio.Text = FormatDate(oContrato.FechaInicio, FSNUser.DateFormat)
        lblProveedor.Text = oContrato.DenProve
        lblContacto.Text = oContrato.Contacto
        If Not oContrato.FechaFin Is Nothing Then
            lblFechaFin.Text = FormatDate(oContrato.FechaFin, FSNUser.DateFormat)
        Else
            lblFechaFin.Visible = False
        End If
        lblEmpresa.Text = oContrato.Empresa
        lblMoneda.Text = oContrato.CodMoneda & " - " & oContrato.DenMoneda

        imgInfProv.ImageUrl = "./images/info.gif"
        imgInfPeticionario.ImageUrl = "./images/info.gif"
        imgInfEmpresa.ImageUrl = "./images/info.gif"

        lblPeticionario.Text = oInstancia.NombrePeticionario
        lblFechaCreacion.Text = "(" & FormatDate(oInstancia.FechaAlta, FSNUser.DateFormat) & ")"

        'Panel infos
        imgInfProv.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosProveedor.AnimationClientID & "', event, '" & FSNPanelDatosProveedor.DynamicPopulateClientID & "', '" & oContrato.CodProve & "'); return false;")
        imgInfPeticionario.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosPeticionario.AnimationClientID & "', event, '" & FSNPanelDatosPeticionario.DynamicPopulateClientID & "', '" & oInstancia.Peticionario & "'); return false;")
        imgInfEmpresa.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosEmpresa.AnimationClientID & "', event, '" & FSNPanelDatosEmpresa.DynamicPopulateClientID & "', '" & oContrato.IdEmpresa & "'); return false;")


        lblMostrarAlerta.Text = String.Empty
        lblRecordatorioAlerta.Text = String.Empty
        lblNotificados.Text = String.Empty

        'alertas
        If oContrato.Alerta > 0 Then '5 semana(s) antes de expira
            lblMostrarAlerta.Text = getPeriodoAlertas(oContrato.Alerta, oContrato.PeriodoAlerta) & " " _
                                    & getTextPeriodoAlertas(oContrato.PeriodoAlerta) & " " _
                                    & Textos(8)
        End If

        If oContrato.RepetirEmail > 0 Then '5 semana(s)
            lblRecordatorioAlerta.Text = getPeriodoAlertas(oContrato.RepetirEmail, oContrato.PeriodoRepetirEmail) & " " _
                                    & getTextPeriodoAlertas(oContrato.PeriodoRepetirEmail)
        End If

        If oContrato.NotificadosNombreyEmail.Length > 0 Then
            lblNotificados.Text = CargarNotificadosText(oContrato.NotificadosNombreyEmail)
        End If
    End Sub

    ''' <summary>
    ''' Muestra o oculta el importe del contrato si se puso un campo de importe en el formulario
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0seg.</remarks>
    Private Sub MostrarImporteContrato()
        If Not oInstancia.CampoImporte Is Nothing Then
            lblMoneda.Visible = False
            lblLitMoneda.Visible = False
            Me.lblLitImporte.Text = Textos(16) & ":"
            lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & oContrato.CodMoneda
            lblLitImporte.Visible = True
            lblImporte.Visible = True
        Else
            lblMoneda.Visible = True
            lblLitMoneda.Visible = True
            lblLitImporte.Visible = False
            Me.lblImporte.Visible = False
        End If
    End Sub

    ''' <summary>
    ''' devuelve el texto del periodo
    ''' </summary>
    Private Function getTextPeriodoAlertas(ByVal intPeriodoID As Integer) As String

        Select Case intPeriodoID
            Case 1
                Return Textos(13) 'dia(s)
            Case 2
                Return Textos(14) 'semana(s)
            Case 3
                Return Textos(15) 'mes(es)
            Case Else
                Return String.Empty
        End Select
    End Function

    ''' <summary>
    ''' devuelve la catidad convirtiendola en el periodo
    ''' </summary>
    Private Function getPeriodoAlertas(ByVal intNumDias As Integer, ByVal intPeriodoID As Integer) As Integer
        Select Case intPeriodoID
            Case 1
                Return intNumDias
            Case 2
                Return intNumDias / 7
            Case 3
                Return intNumDias / 30
            Case Else
                Return 0
        End Select
    End Function

    ''' <summary>
    ''' Carga en el label la lista de contactos internos que devuelve el buscador de personas
    ''' </summary>
    ''' <param name="ListaNotificados">Lista de los contactos internos separados por ### con su value y nombre</param>
    ''' <remarks></remarks>
    Private Function CargarNotificadosText(ByVal ListaNotificados As String) As String
        Dim Notificados() As String = Split(ListaNotificados, "###")
        Dim strReturn As String = String.Empty

        For i As Integer = 0 To Notificados.Length - 2
            If strReturn.Length > 0 Then strReturn &= "; "
            strReturn &= Split(Notificados(i), "#")(1) & IIf(Split(Notificados(i), "#")(2) <> "null", " (" & Split(Notificados(i), "#")(2) & ")", "")
        Next
        Return strReturn
    End Function

End Class