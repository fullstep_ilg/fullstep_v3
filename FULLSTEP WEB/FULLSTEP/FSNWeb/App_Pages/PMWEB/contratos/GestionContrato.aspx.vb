﻿Public Class GestionContrato
    Inherits FSNPage

#Region " Web Form Designer Generated Code "
    <DebuggerStepThrough()> Private Sub InitializeComponent()
    End Sub
    Private designerPlaceholderDeclaration As System.Object
    ''' <summary>
    ''' Propiedad que carga la informacion de la instancia
    ''' </summary>
    Private _oInstancia As FSNServer.Instancia
    Protected ReadOnly Property oInstancia() As FSNServer.Instancia
        Get
            If _oInstancia Is Nothing Then
                If Me.IsPostBack Then
                    _oInstancia = CType(Cache("oInstancia" & FSNUser.Cod), FSNServer.Instancia)
                Else
                    _oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                    _oInstancia.ID = Request("Instancia")

                    'Carga los datos de la instancia:
                    _oInstancia.Load(Idioma, True)
                    If Not _oInstancia.Solicitud Is Nothing Then
                        _oInstancia.Solicitud.Load(Idioma)
                    End If
                    _oInstancia.DevolverEtapaActual(Idioma, FSNUser.CodPersona)

                    Me.InsertarEnCache("oInstancia" & FSNUser.Cod, _oInstancia)
                End If
            End If
            Return _oInstancia
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que carga la informacion del contrato
    ''' </summary>
    Private _oContrato As FSNServer.Contrato
    Protected ReadOnly Property oContrato() As FSNServer.Contrato
        Get
            If _oContrato Is Nothing Then
                If Me.IsPostBack Then
                    _oContrato = CType(Cache("oContrato" & FSNUser.Cod), FSNServer.Contrato)
                Else
                    _oContrato = FSNServer.Get_Object(GetType(FSNServer.Contrato))
                    _oContrato.ID = Request("Contrato")
                    hid_IdContrato.Value = Request("Contrato")
                    _oContrato.Codigo = Request("Codigo")
                    hid_CodContrato.Value = Request("Codigo")
                    'Carga los datos del contrato:
                    _oContrato.Load(Idioma)
                    hid_CodMoneda.Value = _oContrato.CodMoneda
                    hid_Proveedor.Value = _oContrato.CodProve
                    Me.InsertarEnCache("oContrato" & FSNUser.Cod, _oContrato)
                End If
            End If
            Return _oContrato
        End Get
    End Property
    ''' <summary>
    ''' Propiedad que carga si la instancia esta en modo lectura
    ''' </summary>
    Private _bSoloLectura As Boolean = False
    Protected Property bSoloLectura() As Boolean
        Get
            _bSoloLectura = ViewState("bSoloLectura")
            Return _bSoloLectura
        End Get
        Set(ByVal value As Boolean)
            _bSoloLectura = value
            ViewState("bSoloLectura") = _bSoloLectura
        End Set
    End Property
    Private _oInstanciaGrupos As FSNServer.Grupos
    Protected ReadOnly Property oInstanciaGrupos() As FSNServer.Grupos
        Get
            If _oInstanciaGrupos Is Nothing Then
                If Me.IsPostBack Then
                    _oInstanciaGrupos = CType(Cache("oInstanciaGrupos_" & FSNUser.Cod), FSNServer.Grupos)
                Else
                    _oInstancia.CargarCamposInstancia(Idioma, FSNUser.CodPersona, , True, False)
                    _oInstanciaGrupos = oInstancia.Grupos

                    Me.InsertarEnCache("oInstanciaGrupos_" & FSNUser.Cod, _oInstanciaGrupos)
                End If
            End If
            Return _oInstanciaGrupos
        End Get
    End Property
    ''' <summary>
    ''' Carga el detalle de la solicitud.
    ''' </summary>
    ''' <param name="sender">Explicación parámetro 1</param>
    ''' <param name="e">Explicación parámetro 2</param>        
    ''' <remarks>Llamada desde; Tiempo máximo:2seg.</remarks>
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        InitializeComponent()
    End Sub
#End Region
    Private Const IncludeScriptFormat As String = ControlChars.CrLf & "<script type=""{0}"" src=""{1}""></script>"
    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
    Private arrOrdenEstados(7) As String
    Private arrTextosEstados(7) As String
    ''' <summary>
    ''' Carga el detalle de la solicitud.
    ''' </summary>
    ''' <param name="sender">Explicación parámetro 1</param>
    ''' <param name="e">Explicación parámetro 2</param>        
    ''' <remarks>Llamada desde; Tiempo máximo:2seg.</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.Expires = -1

        If Page.IsPostBack Then
            CargarCamposFormulario()
            Exit Sub
        Else
            VinculacionesPrimerGuardar.Value = 0
            VinculacionesIdGuardar.Value = 0
        End If

        Dim sVolver As String
        If Request("volver") <> "" Then
            sVolver = Request("volver")
            sVolver = Replace(sVolver, "*", "&")
            Session("VolverdetalleContrato") = sVolver
        End If

        CargarIdiomas()
        RegistrarScripts()

        PantallaVinculaciones.Value = oInstancia.ExisteVinculaciones
        Version.Value = oInstancia.Version
        Instancia.Value = oInstancia.ID
        hid_IdContrato.Value = oContrato.ID

        MostrarImporteContrato()
        'Comprueba en que estado y paso está la instancia y muestra en casa caso los botones correspondientes:
        bSoloLectura = False

        CargarCamposFormulario()
        VisualizarLinkFlujo()
        CargarParticipantes()
        CargarAccionesEtapa()
        CargarListados()
        ConfigurarCabecera()
        CargarDatosContrato()

        If Not ClientScript.IsStartupScriptRegistered("oRol_Id") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "oRol_Id", "var oRol_Id=" & oInstancia.RolActual & ";", True)
    End Sub
    ''' <summary>
    ''' Nos muestra o no el link para poder ver el flujo por el que ha ido la instancia
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0,seg.</remarks>
    Private Sub VisualizarLinkFlujo()
        'hipervínculo para el detalle del workflow
        If oInstancia.Solicitud.Workflow = Nothing Then  'Si no hay workflow no muestra el hipervínculo
            Me.HyperDetalle.Visible = False
        Else
            If oInstancia.VerDetalleFlujo = True Then
                HyperDetalle.Visible = True
                Me.HyperDetalle.NavigateUrl = "../seguimiento/NWhistoricoestados.aspx?Instancia=" + CStr(oInstancia.ID) & "&ConfiguracionGS=" & Request("ConfiguracionGS") & "&Codigo=" & oContrato.Codigo
            Else
                Me.HyperDetalle.Visible = False
            End If
        End If
    End Sub
    ''' <summary>
    ''' Configura la cabecera, muestra en el menu las posibles acciones
    ''' Activamos los botones que pueden aparecer en la pantalla
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load; Tiempo máximo:=0,1seg.</remarks>
    Private Sub ConfigurarCabecera()

        FSNPageHeader.TextoBotonCalcular = Textos(17)
        FSNPageHeader.TextoBotonGuardar = Textos(19)
        FSNPageHeader.TextoBotonImpExp = Textos(20)
        FSNPageHeader.TextoBotonVolver = Textos(21)
        FSNPageHeader.TextoBotonTrasladar = Textos(31)

        FSNPageHeader.OnClientClickCalcular = "return CalcularCamposCalculados();return false;"
        FSNPageHeader.OnClientClickGuardar = "return Guardar();return false;"
        FSNPageHeader.OnClientClickImpExp = "return cmdImpExp_onclick();return false;"
        If Request("desde") = "SeguimientoSolicitudesPM" Then
            FSNPageHeader.OnClientClickVolver = "return Volver(1);return false;"
        ElseIf Request("desde") = "Inicio" Then 'desde webpart
            FSNPageHeader.OnClientClickVolver = "return Volver(3);return false;"
        Else 'desde visor de contratos
            FSNPageHeader.OnClientClickVolver = "return Volver(2);return false;"
        End If

        If Request("ConfiguracionGS") = "1" Then
            'Estilo de la pagina
            FSNPageHeader.AspectoGS = True
            mi_body.Attributes.Add("class", "fondoGS")
            divProgreso.Attributes.Add("class", "fondoGS")
            Menu1.Visible = False
        Else
            FSNPageHeader.AspectoGS = False
            mi_body.Attributes.Add("class", "")
            divProgreso.Attributes.Add("class", "")
            Menu1.Visible = True
        End If

        mi_body.Attributes.Add("onload", "inicializar();")
        mi_body.Attributes.Add("onunload", "finalizar();")

        FSNPageHeader.VisibleBotonVolver = True
        FSNPageHeader.VisibleBotonImpExp = True
        If Request("OcultarBotonVolver") = "1" Then FSNPageHeader.VisibleBotonVolver = False

        If oContrato.Nombre <> Nothing Then
            FSNPageHeader.TituloCabecera = oContrato.Nombre
        Else
            FSNPageHeader.TituloCabecera = oInstancia.Solicitud.Den(Idioma)
        End If
        FSNPageHeader.UrlImagenCabecera = "images/Contrato.png"

        If FSNUser.AccesoCN Then
            If Not Page.ClientScript.IsClientScriptBlockRegistered("EntidadColaboracion") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "EntidadColaboracion", _
                    "var TipoEntidadColaboracion='CO'; var CodigoEntidadColaboracion ={identificador:" & oInstancia.ID & _
                    ",texto:'" & JSText(oInstancia.ID & " - " & IIf(oContrato.Nombre Is Nothing, oInstancia.Solicitud.Den(Idioma), oContrato.Nombre)) & "'};", True)
            End If
        End If
        If FSNUser.PMPermitirTraslados AndAlso oInstancia.PermitirTraslados AndAlso Not (oInstancia.Estado = TipoEstadoSolic.Anulada) Then
            FSNPageHeader.OnClientClickTrasladar = "return Trasladar();return false;"
            FSNPageHeader.VisibleBotonTrasladar = True
        End If

        'Si la instancia está anulada no se podrá hacer nada con ella:
        If oInstancia.Estado = TipoEstadoSolic.Anulada Then bSoloLectura = True
    End Sub
    ''' <summary>
    ''' Registra las variables que vamos a utilizar en javascript
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub RegistrarScripts()
        Dim sClientTextVars As String
        sClientTextVars = ""
        sClientTextVars += "vdecimalfmt='" + FSNUser.DecimalFmt + "';"
        sClientTextVars += "vthousanfmt='" + FSNUser.ThousanFmt + "';"
        sClientTextVars += "vprecisionfmt='" + FSNUser.PrecisionFmt + "';"
        sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsUser", sClientTextVars)

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM2008", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")

        If Not Page.ClientScript.IsClientScriptBlockRegistered("varFila") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varFila", "<script>var sFila = '" & JSText(Textos(47)) & "' </script>")

        Dim ilGMN1 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN1
        Dim ilGMN2 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN2
        Dim ilGMN3 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN3
        Dim ilGMN4 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN4
        Dim sScript As String
        sScript = ""
        sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
        sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
        sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
        sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "LongitudesCodigosKey", sScript)

        Me.FindControl("frmDetalle").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())

        If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayDesgloses") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")
        End If

        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(Textos(25)) + "';"
        sClientTexts += "arrTextosML[1] = '" + JSText(Textos(26)) + "';"
        sClientTexts += "arrTextosML[2] = '" + JSText(Textos(27)) + "';"
        sClientTexts += "arrTextosML[3] = '" + JSText(Textos(28)) + "';"

        If oInstancia.InstanciaBloqueBloq = TipoBloqueoEtapa.BloqueoSalida Then
            Dim sMensaje As String
            Dim oBloque As FSNServer.Bloque
            Dim oDS As DataSet

            oBloque = FSNServer.Get_Object(GetType(FSNServer.Bloque))
            oBloque.Id = oInstancia.Etapa
            oDS = oBloque.DevolverEtapasBloqueoSalida(Idioma)

            'sMensaje = "En curso" & vbCrLf & "¡Imposible ejecutar la acción!" & vbCrLf
            sMensaje = Textos(35) & vbCrLf & Textos(36) & vbCrLf
            For Each oRow In oDS.Tables(0).Rows
                sMensaje = sMensaje & oRow.Item("DEN") & vbCrLf
            Next
            sClientTexts += "arrTextosML[3] = '" + JSText(sMensaje) + "';"

            oBloque = Nothing
        End If
        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)
    End Sub
    ''' <summary>
    ''' Carga la pestaña de participantes
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,4seg.</remarks>
    Private Sub CargarParticipantes()
        Dim oRol As FSNServer.Rol
        Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
        Dim oucParticipantes As participantes
        Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden

        oRol = FSNServer.Get_Object(GetType(FSNServer.Rol))
        oRol.Id = oInstancia.RolActual
        oRol.Bloque = oInstancia.Etapa
        Rol.Value = oInstancia.RolActual
        Bloque.Value = oInstancia.Etapa

        If oInstancia.RolActual <> 0 Then
            oRol.Id = oInstancia.RolActual
            oRol.Bloque = oInstancia.Etapa

            oRol.CargarParticipantes(Idioma, oInstancia.ID)

            If oRol.Participantes.Tables(0).Rows.Count > 0 Then

                oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                oTabItem.Text = Textos(46) 'Participantes
                oTabItem.Key = "PARTICIPANTES"

                uwtGrupos.Tabs.Add(oTabItem)

                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Hidden
                oTabItem.ContentPane.UserControlUrl = "..\alta\participantes.ascx"

                oucParticipantes = oTabItem.ContentPane.UserControl
                oucParticipantes.Idi = Idioma
                oucParticipantes.TabContainer = uwtGrupos.ClientID

                oucParticipantes.dsParticipantes = oRol.Participantes

                oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden
                oInputHidden.ID = "txtPre_PARTICIPANTES"
                oInputHidden.Value = oucParticipantes.ClientID
                Me.FindControl("frmDetalle").Controls.Add(oInputHidden)
            End If
        End If
        oRol = Nothing
    End Sub
    ''' <summary>
    ''' Carga las acciones posibles en el botón de Otras acciones
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,34seg.</remarks>
    Private Sub CargarAccionesEtapa()
        Dim oPopMenu As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
        Dim oItem As Infragistics.WebUI.UltraWebNavigator.Item
        Dim oRow As DataRow

        Dim oRol As FSNServer.Rol
        oRol = FSNServer.Get_Object(GetType(FSNServer.Rol))
        oRol.Id = oInstancia.RolActual
        oRol.Bloque = oInstancia.Etapa

        Me.Bloque.Value = oInstancia.Etapa

        If oInstancia.RolActual > 0 And oInstancia.Etapa > 0 Then
            oRol.Bloque = oInstancia.Etapa

            Dim oDSAcciones As DataSet = oRol.CargarAcciones(Idioma)

            Me.VinculacionesIdGuardar.Value = oRol.CargarIdGuardar()

            oPopMenu = Me.uwPopUpAcciones
            Dim blnAprobar As Boolean = False
            Dim blnRechazar As Boolean = False
            Dim iAccionesRestarOtrasAcciones As Integer
            Dim iOtrasAcciones As Integer
            If oDSAcciones.Tables.Count > 0 Then
                If oDSAcciones.Tables(0).Rows.Count > 1 Then
                    'Comprobamos si entre las acciones hay de tipo aprobar y rechazar
                    For Each oRow In oDSAcciones.Tables(0).Rows
                        If DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
                            blnAprobar = True
                            iAccionesRestarOtrasAcciones = iAccionesRestarOtrasAcciones + 1
                            If blnRechazar Then Exit For
                        ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
                            blnRechazar = True
                            iAccionesRestarOtrasAcciones = iAccionesRestarOtrasAcciones + 1
                            If blnAprobar Then Exit For
                        End If
                    Next
                    iOtrasAcciones = oDSAcciones.Tables(0).Rows.Count - iAccionesRestarOtrasAcciones
                    For Each oRow In oDSAcciones.Tables(0).Rows
                        If iOtrasAcciones > 1 AndAlso DBNullToSomething(oRow.Item("RA_APROBAR")) <> 1 AndAlso DBNullToSomething(oRow.Item("RA_RECHAZAR")) <> 1 Then
                            oPopMenu.Visible = True
                            FSNPageHeader.VisibleBotonAccion1 = True
                            FSNPageHeader.TextoBotonAccion1 = Textos(18)
                            FSNPageHeader.OnClientClickAccion1 = "return mostrarMenuAcciones(event,1);"

                            If IsDBNull(oRow.Item("DEN")) Then
                                oItem = oPopMenu.Items.Add("&nbsp;")
                            Else
                                If oRow.Item("DEN") = "" Then
                                    oItem = oPopMenu.Items.Add("&nbsp;")
                                Else
                                    oItem = oPopMenu.Items.Add(DBNullToStr(oRow.Item("DEN")))
                                End If
                            End If
                            oItem.TargetUrl = "javascript:EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(DBNullToSomething(oRow.Item("CUMP_OBL_ROL")) = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("GUARDA")) = 1, "true", "false")) + "'," + CStr(oInstancia.InstanciaBloqueBloq) + ",'" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "')"
                        ElseIf DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
                            FSNPageHeader.OnClientClickAprobar = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(DBNullToSomething(oRow.Item("CUMP_OBL_ROL")) = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("GUARDA")) = 1, "true", "false")) + "'," + CStr(oInstancia.InstanciaBloqueBloq) + ",'" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                        ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
                            FSNPageHeader.OnClientClickRechazar = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(DBNullToSomething(oRow.Item("CUMP_OBL_ROL")) = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("GUARDA")) = 1, "true", "false")) + "'," + CStr(oInstancia.InstanciaBloqueBloq) + ",'" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                        Else
                            FSNPageHeader.VisibleBotonAccion1 = True
                            FSNPageHeader.OnClientClickAccion1 = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(DBNullToSomething(oRow.Item("CUMP_OBL_ROL")) = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("GUARDA")) = 1, "true", "false")) + "'," + CStr(oInstancia.InstanciaBloqueBloq) + ",'" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                            FSNPageHeader.TextoBotonAccion1 = DBNullToStr(oRow.Item("DEN"))
                        End If
                    Next
                    If blnAprobar Then
                        FSNPageHeader.TextoBotonAprobar = Textos(32) '"Aprobar"
                        FSNPageHeader.VisibleBotonAprobar = True
                    End If
                    If blnRechazar Then
                        FSNPageHeader.TextoBotonRechazar = Textos(33) '"Rechazar"
                        FSNPageHeader.VisibleBotonRechazar = True
                    End If
                Else
                    If oDSAcciones.Tables(0).Rows.Count > 0 Then
                        oRow = oDSAcciones.Tables(0).Rows(0)
                        If DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
                            FSNPageHeader.TextoBotonAprobar = Textos(32) '"Aprobar"
                            FSNPageHeader.VisibleBotonAprobar = True
                            FSNPageHeader.OnClientClickAprobar = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(DBNullToSomething(oRow.Item("CUMP_OBL_ROL")) = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("GUARDA")) = 1, "true", "false")) + "'," + CStr(oInstancia.InstanciaBloqueBloq) + ",'" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                        ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
                            FSNPageHeader.TextoBotonRechazar = Textos(33) '"Rechazar"
                            FSNPageHeader.VisibleBotonRechazar = True
                            FSNPageHeader.OnClientClickRechazar = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(DBNullToSomething(oRow.Item("CUMP_OBL_ROL")) = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("GUARDA")) = 1, "true", "false")) + "'," + CStr(oInstancia.InstanciaBloqueBloq) + ",'" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                        Else
                            FSNPageHeader.TextoBotonAccion1 = DBNullToStr(oRow.Item("DEN"))
                            FSNPageHeader.OnClientClickAccion1 = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(DBNullToSomething(oRow.Item("CUMP_OBL_ROL")) = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("GUARDA")) = 1, "true", "false")) + "'," + CStr(oInstancia.InstanciaBloqueBloq) + ",'" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                            FSNPageHeader.VisibleBotonAccion1 = True
                        End If
                    End If
                End If
            End If
        End If
        oRol = Nothing
    End Sub
    ''' <summary>
    ''' Carga los campos de la instancia
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo máximo:(Depende de los campos de la isntancia)</remarks>
    Private Sub CargarCamposFormulario()
        uwtGrupos.Tabs.Clear()
        AplicarEstilosTab(uwtGrupos)

        Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim lIndex As Integer = 0
        Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
        Dim oRow As DataRow
        Dim oucCampos As campos
        Dim oucDesglose As desgloseControl

        Dim oGrupo As FSNServer.Grupo

        If Not oInstanciaGrupos.Grupos Is Nothing Then
            'SI EXISTE ALGUN CAMPO DE TIPO=3 (CALCULADO) HACEMOS VISIBLE EL BOTON DE CALCULAR
            FSNPageHeader.VisibleBotonCalcular = oInstanciaGrupos.Grupos.OfType(Of FSNServer.Grupo).Where(Function(x) x.DSCampos.Tables(0).Rows.OfType(Of DataRow).Where(Function(y) y("TIPO") = TipoCampoPredefinido.Calculado).Any).Any()
            If FSNPageHeader.VisibleBotonCalcular Then BotonCalcular.Value = 1

            For Each oGrupo In oInstanciaGrupos.Grupos
                oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden

                oInputHidden.ID = "txtPre_" + lIndex.ToString
                oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
                Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
                oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(Idioma), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
                oTabItem.Key = lIndex.ToString
                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                uwtGrupos.Tabs.Add(oTabItem)
                lIndex += 1

                If oGrupo.DSCampos.Tables.Count > 0 Then
                    If oGrupo.NumCampos <= 1 Then
                        For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                            If oRow.Item("VISIBLE") = 1 Then
                                Exit For
                            End If

                        Next

                        If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Desglose Or oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoDesglose Then
                            If oRow.Item("VISIBLE") = 0 Then
                                uwtGrupos.Tabs.Remove(oTabItem)
                            Else
                                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                                oTabItem.ContentPane.UserControlUrl = "..\alta\desglose.ascx"
                                oucDesglose = oTabItem.ContentPane.UserControl
                                oucDesglose.ID = oGrupo.Id.ToString
                                oucDesglose.Campo = oRow.Item("ID_CAMPO")
                                oucDesglose.TabContainer = uwtGrupos.ClientID
                                oucDesglose.Instancia = oInstancia.ID
                                oucDesglose.TieneIdCampo = True
                                oucDesglose.Ayuda = DBNullToSomething(oRow.Item("AYUDA_" & Idioma))
                                oucDesglose.Version = oInstancia.Version
                                oucDesglose.SoloLectura = bSoloLectura Or (oRow.Item("ESCRITURA") = 0)
                                oucDesglose.PM = True
                                oucDesglose.Titulo = DBNullToSomething(oRow.Item("DEN_" & Idioma))
                                oucDesglose.TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Contrato
                                oInputHidden.Value = oucDesglose.ClientID
                            End If
                        Else
                            oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                            oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                            oucCampos = oTabItem.ContentPane.UserControl
                            oucCampos.Instancia = oInstancia.ID
                            oucCampos.ID = oGrupo.Id.ToString
                            oucCampos.dsCampos = oGrupo.DSCampos
                            oucCampos.IdGrupo = oGrupo.Id
                            oucCampos.Idi = Idioma
                            oucCampos.TabContainer = uwtGrupos.ClientID
                            oucCampos.Version = oInstancia.Version
                            oucCampos.SoloLectura = bSoloLectura
                            oucCampos.InstanciaMoneda = oInstancia.Moneda
                            oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                            oucCampos.PM = True
                            oucCampos.Formulario = oInstancia.Solicitud.Formulario
                            oucCampos.ObjInstancia = oInstancia
                            oucCampos.IdContrato = oContrato.ID
                            oucCampos.NombreContrato = oContrato.NombreAdjuntoContrato
                            oInputHidden.Value = oucCampos.ClientID
                        End If

                    Else
                        oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                        oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                        oucCampos = oTabItem.ContentPane.UserControl
                        oucCampos.Instancia = oInstancia.ID
                        oucCampos.ID = oGrupo.Id.ToString
                        oucCampos.dsCampos = oGrupo.DSCampos
                        oucCampos.IdGrupo = oGrupo.Id
                        oucCampos.Idi = Idioma
                        oucCampos.TabContainer = uwtGrupos.ClientID
                        oucCampos.Version = oInstancia.Version
                        oucCampos.SoloLectura = bSoloLectura
                        oucCampos.InstanciaMoneda = oInstancia.Moneda
                        oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                        oucCampos.PM = True
                        oucCampos.Formulario = oInstancia.Solicitud.Formulario
                        oucCampos.ObjInstancia = oInstancia
                        oucCampos.IdContrato = oContrato.ID
                        oucCampos.NombreContrato = oContrato.NombreAdjuntoContrato
                        oInputHidden.Value = oucCampos.ClientID
                    End If
                End If
                Me.FindControl("frmDetalle").Controls.Add(oInputHidden)
            Next
        End If
    End Sub
    ''' <summary>
    ''' Carga los listados posibles en el botón de Listados
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub CargarListados()

        Dim oPopMenuListados As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
        Dim oItemListados As Infragistics.WebUI.UltraWebNavigator.Item
        Dim oDSListados As DataSet
        Dim oRow As DataRow

        Dim oRol As FSNServer.Rol
        oRol = FSNServer.Get_Object(GetType(FSNServer.Rol))
        oRol.Id = oInstancia.RolActual
        oRol.Bloque = oInstancia.Etapa

        oDSListados = oRol.CargarListados(Idioma)

        oPopMenuListados = Me.uwPopUpListados

        If oDSListados.Tables.Count > 0 Then
            If oDSListados.Tables(0).Rows.Count > 0 Then
                FSNPageHeader.VisibleBotonListados = True
                FSNPageHeader.TextoBotonListados = Textos(34) '"Listados"
                FSNPageHeader.OnClientClickListados = "return mostrarMenuListados(event);"

                For Each oRow In oDSListados.Tables(0).Rows
                    oItemListados = oPopMenuListados.Items.Add(oRow.Item("DEN"))
                    oItemListados.TargetUrl = "javascript:LanzarListado('" + oRow.Item("ARCHIVO_RPT") + "'," + CStr(oInstancia.ID) + ");"
                Next
            End If

        End If
    End Sub
    ''' <summary>
    ''' Carga la informacion del contrato en los campos
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo mÃ¡ximo:0,1seg.</remarks>
    Private Sub CargarDatosContrato()
        Dim sEstado As String
        Dim i As Byte
        If oContrato.Estado >= 0 Then
            For i = 0 To UBound(Me.arrOrdenEstados)
                If oContrato.Estado = arrOrdenEstados(i) Then
                    sEstado = arrTextosEstados(i)
                    Exit For
                End If
            Next
            If sEstado <> "" Then
                sEstado = " (" & sEstado & ")"
            End If
        End If

        lblIDInstanciayEstado.Text = oContrato.Codigo & sEstado

        lblFechaInicio.Text = FormatDate(oContrato.FechaInicio, FSNUser.DateFormat)
        lblProveedor.Text = oContrato.CodProve & " " & oContrato.DenProve
        lblContacto.Text = oContrato.Contacto
        If Not oContrato.FechaFin Is Nothing Then
            lblFechaFin.Text = FormatDate(oContrato.FechaFin, FSNUser.DateFormat)
        Else
            lblFechaFin.Visible = False
        End If
        lblEmpresa.Text = oContrato.Empresa
        lblMoneda.Text = oContrato.CodMoneda & " - " & oContrato.DenMoneda

        imgInfProv.ImageUrl = "./images/info.gif"
        imgInfPeticionario.ImageUrl = "./images/info.gif"
        imgInfEmpresa.ImageUrl = "./images/info.gif"

        lblPeticionario.Text = oInstancia.NombrePeticionario
        lblFechaCreacion.Text = "(" & FormatDate(oInstancia.FechaAlta, FSNUser.DateFormat) & ")"


        'Panel infos
        imgInfProv.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosProveedor.AnimationClientID & "', event, '" & FSNPanelDatosProveedor.DynamicPopulateClientID & "', '" & oContrato.CodProve & "'); return false;")
        imgInfPeticionario.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosPeticionario.AnimationClientID & "', event, '" & FSNPanelDatosPeticionario.DynamicPopulateClientID & "', '" & oInstancia.Peticionario & "'); return false;")
        imgInfEmpresa.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosEmpresa.AnimationClientID & "', event, '" & FSNPanelDatosEmpresa.DynamicPopulateClientID & "', '" & oContrato.IdEmpresa & "'); return false;")
        lblMostrarAlerta.Text = String.Empty
        lblRecordatorioAlerta.Text = String.Empty
        lblNotificados.Text = String.Empty

        'alertas
        If oContrato.Alerta > 0 Then
            lblMostrarAlerta.Text = getPeriodoAlertas(oContrato.Alerta, oContrato.PeriodoAlerta) & " " _
                                    & getTextPeriodoAlertas(oContrato.PeriodoAlerta) & " " _
                                    & Textos(8)
            '5 semana(s) antes de expira
        End If

        If oContrato.RepetirEmail > 0 Then
            lblRecordatorioAlerta.Text = getPeriodoAlertas(oContrato.RepetirEmail, oContrato.PeriodoRepetirEmail) & " " _
                                    & getTextPeriodoAlertas(oContrato.PeriodoRepetirEmail)
            '5 semana(s)
        End If

        If oContrato.NotificadosNombreyEmail.Length > 0 Then
            lblNotificados.Text = CargarNotificadosText(oContrato.NotificadosNombreyEmail)
        End If
    End Sub

    ''' <summary>
    ''' devuelve el texto del periodo
    ''' </summary>
    Private Function getTextPeriodoAlertas(ByVal intPeriodoID As Integer) As String
        Select Case intPeriodoID
            Case 1
                Return Textos(13) 'dia(s)
            Case 2
                Return Textos(14) 'semana(s)
            Case 3
                Return Textos(15) 'mes(es)
            Case Else
                Return String.Empty
        End Select
    End Function

    ''' <summary>
    ''' devuelve la catidad convirtiendola en el periodo
    ''' </summary>
    Private Function getPeriodoAlertas(ByVal intNumDias As Integer, ByVal intPeriodoID As Integer) As Integer
        Select Case intPeriodoID
            Case 1
                Return intNumDias
            Case 2
                Return intNumDias / 7
            Case 3
                Return intNumDias / 30
            Case Else
                Return 0
        End Select
    End Function

    ''' <summary>
    ''' Carga en el label la lista de contactos internos que devuelve el buscador de personas
    ''' </summary>
    ''' <param name="ListaNotificados">Lista de los contactos internos separados por ### con su value y nombre</param>
    ''' <remarks></remarks>
    Private Function CargarNotificadosText(ByVal ListaNotificados As String) As String
        Dim Notificados() As String = Split(ListaNotificados, "###")
        Dim strReturn As String = String.Empty
        For i As Integer = 0 To Notificados.Length - 2
            If strReturn.Length > 0 Then strReturn &= "; "
            strReturn &= Split(Notificados(i), "#")(1) & IIf(Split(Notificados(i), "#")(2) <> "null", " (" & Split(Notificados(i), "#")(2) & ")", "")
        Next
        Return strReturn
    End Function

    ''' <summary>
    ''' Carga el idioma correspondiente el los controles
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0seg.</remarks>
    Private Sub CargarIdiomas()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.GestionContrato
        lblLitFechaInicio.Text = Textos(0) & ":"  '"Fecha de inicio"
        lblLitFechaExpiracion.Text = Textos(1) & ":" '"Fecha de expiración"
        'lblLitProveedor.Text = "(*) " & Textos(2) '"Proveedor"
        lblLitContacto.Text = Textos(3) & ":" '"Contacto"
        lblLitEmpresa.Text = Textos(4) & ":" '"Empresa"
        lblLitMoneda.Text = Textos(5) & ":" '"Moneda"
        lblCamposObligatorios.Text = Textos(48)
        Me.cadenaespera.Value = Textos(23) '"Su solicitud está siendo tramitada. Espere unos instantes..."
        Me.lblProgreso.Text = Textos(24) '"Se está cargando la solicitud. Espere unos instantes..."
        HyperDetalle.Text = Textos(30)

        ''alertas
        lblLitMostrarAlerta.Text = Textos(7) & ":" '"Mostrar alerta"
        lblLitRecordatorioAlerta.Text = Textos(11) & ":" '"Mostrar alerta"
        lblLitNotificados.Text = Textos(12) & ":" '"Configurar notificados" & ":"

        arrOrdenEstados(0) = TiposDeDatos.EstadosVisorContratos.Guardado
        arrOrdenEstados(1) = TiposDeDatos.EstadosVisorContratos.En_Curso_De_Aprobacion
        arrOrdenEstados(2) = TiposDeDatos.EstadosVisorContratos.Vigentes
        arrOrdenEstados(3) = TiposDeDatos.EstadosVisorContratos.Proximo_a_Expirar
        arrOrdenEstados(4) = TiposDeDatos.EstadosVisorContratos.Expirados
        arrOrdenEstados(5) = TiposDeDatos.EstadosVisorContratos.Rechazados
        arrOrdenEstados(6) = TiposDeDatos.EstadosVisorContratos.Anulados

        arrTextosEstados(0) = Textos(37) '"Guardados"
        arrTextosEstados(1) = Textos(38) '"En curso de aprobaciÃ³n"
        arrTextosEstados(2) = Textos(39) '"Vigentes"
        arrTextosEstados(3) = Textos(40) '"proximo a expirar"
        arrTextosEstados(4) = Textos(41) '"Expirados"
        arrTextosEstados(5) = Textos(42) '"Rechazados"
        arrTextosEstados(6) = Textos(43) '"Anulados"

        'Textos Panel info-s
        FSNPanelDatosProveedor.Titulo = Textos(2) '"Proveedor"
        FSNPanelDatosProveedor.SubTitulo = Textos(44) '"InformaciÃ³n detallada"

        FSNPanelDatosPeticionario.Titulo = Textos(45) '"Peticionario"
        FSNPanelDatosPeticionario.SubTitulo = Textos(44) '"Datos del peticionario"

        FSNPanelDatosEmpresa.Titulo = Textos(4) '"Empresa"
        FSNPanelDatosEmpresa.SubTitulo = Textos(44) '"Datos de empresa"

    End Sub
    ''' <summary>
    ''' Muestra o oculta el importe del contrato si se puso un campo de importe en el formulario
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0seg.</remarks>
    Private Sub MostrarImporteContrato()
        If Not oInstancia.CampoImporte Is Nothing Then
            lblMoneda.Visible = False
            lblLitMoneda.Visible = False
            Me.lblLitImporte.Text = Textos(16) & ":"
            lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & oContrato.CodMoneda
            lblLitImporte.Visible = True
            lblImporte.Visible = True
        Else
            lblMoneda.Visible = True
            lblLitMoneda.Visible = True
            lblLitImporte.Visible = False
            Me.lblImporte.Visible = False
        End If
    End Sub
End Class