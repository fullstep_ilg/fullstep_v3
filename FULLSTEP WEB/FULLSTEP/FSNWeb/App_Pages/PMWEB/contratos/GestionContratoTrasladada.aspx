﻿<%@ Register Assembly="Infragistics.WebUI.UltraWebTab.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GestionContratoTrasladada.aspx.vb" Inherits="Fullstep.FSNWeb.GestionContratoTrasladada" EnableViewState="False" %>

<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
</head>
<!--JIM: colocamos esta porción de código antes de la etiqueta de menú. menu.js realiza override de window.open, y si la colocamos
        después no se ejecuta este código -->
<%If Not String.IsNullOrEmpty(Request("ConfiguracionGS")) Then%>
<script type="text/javascript">
    window.open_ = window.open;
    window.open = function (url, name, props) {
        if (url.toLowerCase().search("sessionid") > 0)
            return window.open_(url, name, props);
        else
            return window.open_(url + '&SessionId=<%=Session("sSession")%>', name, props);
		    }
</script>
<%End If%>


<script type="text/javascript">
    wProgreso = null;

    //Descripcion:Muestra el contenido peligroso que ha efectuado un error al guardar.
    //Parametros entrada: 
    //contenido: Contenido del error
    //Llamada desde: errores.aspx.vb
    //Tiempo ejecucion:=0seg.
    function ErrorValidacion(contenido) {
        alert(arrTextosML[3].replace("$$$", contenido));
        OcultarEspera()
    }
    /*Descripcion: Le pone un ancho al desglose
      Llamada desde:carga de la pagina
      Tiempo ejecucion:0,1seg.*/
    function resize() {
        for (i = 0; i < arrDesgloses.length; i++) {
            sDiv = arrDesgloses[i].replace("tblDesglose", "divDesglose")
            if (document.getElementById(sDiv))
                document.getElementById(sDiv).style.width = parseFloat(document.body.offsetWidth) - 95 + 'px';
        }
    }
    /*Descripcion: Evalua y ejecuta la instruccion que se le pasa commo parametro
      parametro:
        s--> Instruccion a evaluar
      Tiempo ejecucion:0,1seg.*/
    function localEval(s) {
        eval(s)
    }
    /*Descripcion: Muestra los divs que indican que se esta realizando un proceso.
      Llamada desde:=Guardar() // Trasladar() // Devolver()
      Tiempo ejecucion:0,1seg.*/
    function MostrarEspera() {
        wProgreso = true;

        $("[id*='lnkBoton']").attr('disabled', 'disabled');

        document.getElementById("divForm2").style.display = 'none';
        document.getElementById("divForm3").style.display = 'none';
        document.getElementById("igtabuwtGrupos").style.display = 'none';

        i = 0;
        bSalir = false;
        while (bSalir == false) {
            if (document.getElementById("uwtGrupos_div" + i)) {
                document.getElementById("uwtGrupos_div" + i).style.visibility = 'hidden';
                i = i + 1;
            }
            else {
                bSalir = true;
            }
        }
        document.getElementById("lblProgreso").value = document.forms["frmDetalle"].elements["cadenaespera"].value;
        document.getElementById("divProgreso").style.display = 'inline';
    }

    //Estaba habilitando los botones antes de ocultar el progreso. Parecia q te dejaba dar a varios botones mientras estaba en progreso.
    function DarTiempoAOcultarProgreso() {
        $("[id*='lnkBoton']").removeAttr('disabled');
    }

    /*Descripcion: Oculta los divs que indican que se esta realizando un proceso y Muestra los grupos.
      Llamada desde:=HabilitarBotones // Finalizar()
      Tiempo ejecucion:0,1seg.*/
    function OcultarEspera() {
        wProgreso = null;

        document.getElementById("divProgreso").style.display = 'none';
        document.getElementById("divForm2").style.display = 'inline';
        document.getElementById("divForm3").style.display = 'inline';
        document.getElementById("igtabuwtGrupos").style.display = '';

        setTimeout('DarTiempoAOcultarProgreso()', 250);

        i = 0;
        bSalir = false;
        while (bSalir == false) {
            if (document.getElementById("uwtGrupos_div" + i)) {
                document.getElementById("uwtGrupos_div" + i).style.visibility = 'visible';
                i = i + 1;
            }
            else {
                bSalir = true;
            }
        }
        return
    }
    /*Descripcion:=Oculta los divs de espera
		Llamada desde:GuardarInstancia // Una vez que se carga la pagina
		Tiempo ejecucion:0,1seg.*/
    function HabilitarBotones() {
        OcultarEspera();
    }
    /*Descripcion:=Monta el formulario para el posterior guardado. (guardarInstancia.aspx)
		Llamada desde:opcion guardar del menu.
		Tiempo ejecucion:1,5seg.*/
    function Guardar() {
        MostrarEspera()
        setTimeout("MontarSubmitGuardar()", 100)
        return false;
    }
    /*Descripcion:=Monta el formulario para el posterior traslado. (guardarInstancia.aspx)
		Llamada desde:opcion "Trasladar" del menu.
		Tiempo ejecucion:1,5seg.*/
    function Trasladar() {
        MostrarEspera()

        setTimeout("MontarSubmitTrasladar()", 100)
        return false;
    }
    /*Descripcion:=Monta el formulario para el posterior devolucion. (guardarInstancia.aspx)
		Llamada desde:opcion "Devolver" del menu.
		Tiempo ejecucion:1,5seg.*/
    function Devolver() {
        MostrarEspera()

        setTimeout("MontarSubmitDevolver()", 100)
        return false;
    }
    /*Descripcion:=Monta el formulario para el posterior guardado. (guardarInstancia.aspx)
		Llamada desde:Guardar()
		Tiempo ejecucion:1,5seg.*/
    function MontarSubmitGuardar() {
        MontarFormularioSubmit(true, true, false, false)

        var frmSubmitElements = document.forms["frmSubmit"].elements;
        frmSubmitElements["GEN_AccionRol"].value = 0
        frmSubmitElements["GEN_Bloque"].value = document.forms["frmDetalle"].elements["Bloque"].value
        frmSubmitElements["GEN_Enviar"].value = 0
        frmSubmitElements["GEN_Accion"].value = "guardarcontrato"

        CompletarFormulario()

        oFrm = MontarFormularioCalculados()
        sInner = oFrm.innerHTML
        oFrm.innerHTML = ""
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner)
        document.forms["frmSubmit"].submit()
        return false;
    }
    /*Descripcion:=Monta el formulario para el posterior devolucion. (guardarInstancia.aspx)
    Llamada desde:Devolver()
    Tiempo ejecucion:1,5seg.*/
    function MontarSubmitDevolver() {
        MontarFormularioSubmit(true, true, false, false)

        var frmSubmitElements = document.forms["frmSubmit"].elements;
        frmSubmitElements["GEN_AccionRol"].value = 0
        frmSubmitElements["GEN_Bloque"].value = document.forms["frmDetalle"].elements["Bloque"].value
        frmSubmitElements["GEN_Enviar"].value = 0
        frmSubmitElements["GEN_Accion"].value = "trasladadadevolvercontrato"

        CompletarFormulario()

        oFrm = MontarFormularioCalculados()
        sInner = oFrm.innerHTML
        oFrm.innerHTML = ""
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner)
        document.forms["frmSubmit"].submit()
        return false;
    }
    /*Descripcion:=Monta el formulario para el posterior traslado. (guardarInstancia.aspx)
		Llamada desde:Trasladar()
		Tiempo ejecucion:1,5seg.*/
    function MontarSubmitTrasladar() {
        MontarFormularioSubmit(true, true, false, false)

        var frmSubmitElements = document.forms["frmSubmit"].elements;
        frmSubmitElements["GEN_Enviar"].value = 0
        frmSubmitElements["GEN_Accion"].value = "trasladadatrasladarcontrato"
        frmSubmitElements["GEN_AccionRol"].value = 0
        frmSubmitElements["GEN_Bloque"].value = document.forms["frmDetalle"].elements["Bloque"].value

        CompletarFormulario()

        oFrm = MontarFormularioCalculados()
        sInner = oFrm.innerHTML
        oFrm.innerHTML = ""
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner)
        document.forms["frmSubmit"].submit()
        return
    }
    /*Descripcion:=Monta el formulario para realizar el calculo de los campos.
		Llamada desde:Opcion de menu "Calcular"
		Tiempo ejecucion:1,5seg.*/
    function CalcularCamposCalculados() {
        oFrm = MontarFormularioCalculados()
        oFrm.submit()
        return false;
    }
    /*Descripcion:=Iniciliza los tabs de los grupos
      Llamada desde:evento init de los tabls
      Tiempo ejecucion:0seg.*/
    function initTab(webTab) {
        var cp = document.getElementById(webTab.ID + '_cp');
        cp.style.minHeight = '300px';
    }
    /*Descripcion:=Llama a la pagina para exportacion de datos
      Llamada desde:Option de menu "Impr./Exp."
      Tiempo ejecucion:0seg.*/
    function cmdImpExp_onclick() {
        window.open('../seguimiento/impexp_sel.aspx?Instancia=' + document.getElementById("Instancia").value + '&TipoImpExp=1&Contrato=' + document.forms["frmDetalle"].elements["hid_Contrato"].value, '_new', 'fullscreen=no,height=115,width=315,location=no,menubar=no,resizable=no,scrollbars=no,status=yes,titlebar=yes,toolbar=no,left=200,top=200');
        return false;
    }
    /*Descripcion:Despues del recalculo de los campos calculados en la pagina recalcularImportes.aspx.
                  se devuelve el valor
    Parametros entrada:=
        importeConFormato: Importe con el formato del usuario
        Importe: Importe (Numerico)
    Llamada desde:=Click Calcular.
    Tiempo ejecucion:=0seg.*/
    function ponerCalculados(importeConFormato, importe) {
        lblImporteV = document.getElementById('<%=lblImporte.clientID %>')
		if (lblImporteV) {
		    lblImporteV.innerHTML = "Importe: " + importeConFormato;
		}
	}
	/*Descripcion:=Añade el ID del contrato para el envio de la informacion de los campos a GuardarInstancia.aspx
      Llamada desde:MontarSubmitGuardar // MontarSubmitDevolver // MontarSubmitTrasladar
      Tiempo ejecucion:0seg.*/
	function CompletarFormulario() {
		document.forms["frmSubmit"].elements["ID_CONTRATO"].value = document.forms["frmDetalle"].elements["hid_Contrato"].value
	};
	//''' <summary>
	//''' funcion que inicializa los tabs
	//''' </summary>
	//''' <remarks>Llamada desde:=funcion que se ejecuta al cargar la pagina; Tiempo máximo:0,1</remarks>
	function inicializar() {
		resize();
	}
	//''' <summary>
	//''' funcion que inicializa los tabs
	//''' </summary>
	//''' <remarks>Llamada desde:=funcion que se ejecuta al descargar la pagina; Tiempo máximo:0,1</remarks>	
	function finalizar() {
		if (wProgreso != null)
		    OcultarEspera();
		wProgreso = null;
	}
	//''' <summary>
	//''' Vuelve a la pagina anterior. O cierra la pagina si se trata de una llamada GS
	//''' </summary>
	//    Parametros entrada: iDesde  (1) = VisorSolicitudes
	//''' <remarks>Llamada desde:Al pinchar en volver; Tiempo máximo:0</remarks>
	function Volver(iDesde) {
		if (iDesde == 1)
		    window.open("<%=ConfigurationManager.AppSettings("rutaPM2008") %>tareas/VisorSolicitudes.aspx", "_top")
		else {
			if (iDesde == 2)
			    window.open("<%=ConfigurationManager.AppSettings("rutaPM2008") %>contratos/VisorContratos.aspx", "_top")
			else
				window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>Inicio.aspx", "_self")
        }
        return false;
    }
</script>
<body runat="server" id="mi_body" onresize="resize()">
    <form id="frmDetalle" method="post" runat="server">
        <uc1:menu ID="Menu1" runat="server" OpcionMenu="Contratos" OpcionSubMenu="Seguimiento" />

        <fsn:FSNPanelInfo ID="FSNPanelDatosProveedor" runat="server" ServicePath="~/App_Pages/PMWEB/ConsultasPMWEB.asmx" ServiceMethod="Obtener_DatosProveedor" TipoDetalle="0"></fsn:FSNPanelInfo>
        <fsn:FSNPanelInfo ID="FSNPanelDatosPeticionario" runat="server" ServicePath="~/App_Pages/PMWEB/ConsultasPMWEB.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="0"></fsn:FSNPanelInfo>
        <fsn:FSNPanelInfo ID="FSNPanelDatosEmpresa" runat="server" ServicePath="~/App_Pages/PMWEB/ConsultasPMWEB.asmx" ServiceMethod="Obtener_DatosEmpresa" TipoDetalle="0"></fsn:FSNPanelInfo>

        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <CompositeScript>
                <Scripts>
                    <asp:ScriptReference Name="ExtenderBase.BaseScripts.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Common.Common.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Compat.Timer.Timer.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Animation.Animations.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Animation.AnimationBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="PopupExtender.PopupBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="AutoComplete.AutoCompleteBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Path="../alta/js/AdjacentHTML.js" />
                    <asp:ScriptReference Path="../alta/js/jsAlta.js" />
                </Scripts>
            </CompositeScript>
        </asp:ScriptManager>
        <iframe id="iframeWSServer" style="z-index: 109; left: 8px; visibility: hidden; position: absolute; top: 200px"
            name="iframeWSServer" src="../blank.htm"></iframe>
        <input id="Bloque" type="hidden" name="Bloque" runat="server">
        <input id="Instancia" type="hidden" name="Instancia" runat="server">
        <input id="txtEnviar" type="hidden" name="Enviar" runat="server">
        <input id="Version" type="hidden" name="Version" runat="server">
        <input id="hid_CodMoneda" type="hidden" runat="server" />
        <div>
            <table id="Table1" style="height: 15%; width: 100%; padding-bottom: 15px;" cellspacing="0" cellpadding="1" border="0">
                <tr>
                    <td colspan="7">
                        <fsn:FSNPageHeader runat="server" ID="FSNPageHeader" UrlImagenCabecera="~/App_Pages/PMWEB/contratos/images/calendar-color.png"></fsn:FSNPageHeader>
                    </td>
                </tr>
            </table>

            <div style="padding-left: 15px; padding-bottom: 15px">
                <asp:Panel ID="pnlContrato" runat="server" BackColor="#f5f5f5" Font-Names="Arial" Width="95%">
                    <table id="Table2" style="height: 15%; width: 100%; padding-bottom: 15px; padding-left: 5px" cellspacing="0" cellpadding="1" border="0">
                        <tr>
                            <td style="padding-top: 5px; padding-bottom: 5px;" class="fondoCabecera">
                                <table id="Table3" style="width: 100%; table-layout: fixed; padding-left: 10px" border="0">
                                    <tr>
                                        <td style="width: 50%">
                                            <asp:Label ID="lblIDInstanciayEstado" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td style="width: 50%" nowrap="nowrap">
                                            <asp:Label ID="lblProveedor" runat="server" CssClass="label" Text="Proveedor" Font-Bold="true"></asp:Label>
                                            <asp:Image ID="imgInfProv" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="3" nowrap="nowrap">
                                            <table style="width: 100%" border="0">
                                                <tr>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="lblLitFechaInicio" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px">Fecha de inicio</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblFechaInicio" runat="server" CssClass="label" Text="FechaInicio"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblLitEmpresa" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px" Text="Empresa"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblEmpresa" runat="server" CssClass="label" Text="Empresa"></asp:Label>
                                                        <asp:Image ID="imgInfEmpresa" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td nowrap="nowrap" style="width: 120px">
                                                        <asp:Label ID="lblLitFechaExpiracion" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px" Text="Fecha de expiracion"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblFechaFin" runat="server" CssClass="label" Text="FechaFin"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblLitMoneda" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px" Text="Moneda"></asp:Label>
                                                        <asp:Label ID="lblLitImporte" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px" Visible="false"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblMoneda" runat="server" CssClass="label" Text="Moneda"></asp:Label>
                                                        <asp:Label ID="lblImporte" runat="server" CssClass="label" Visible="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td nowrap="nowrap" style="width: 120px">
                                                         <asp:Label ID="lblLitMostrarAlerta" runat="server" Text="Mostrar Alerta" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label>
                                                    </td>
                                                    <td>
                                                         <asp:Label ID="lblMostrarAlerta" runat="server" Text="Mostrar Alerta" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblLitRecordatorioAlerta" runat="server" Text="Mostrar Alerta" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRecordatorioAlerta" runat="server" Text="Mostrar Alerta" CssClass="label"></asp:Label>
                                                    </td>

                                                </tr>

                                            </table>
                                        </td>
                                        <td nowrap="nowrap" colspan="2">
                                            <asp:Label ID="lblLitCreador" runat="server" Text="Creado por:" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label>
                                            <asp:Label ID="lblPeticionario" runat="server" CssClass="label"></asp:Label>
                                            <asp:Image ID="imgInfPeticionario" runat="server" />
                                            <asp:Label ID="lblFechaCreacion" runat="server" Text="(01/01/0000)" CssClass="label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblLitContacto" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px" Text="Contacto"></asp:Label>
                                            <asp:Label ID="lblContacto" runat="server" CssClass="label" Text="Contacto"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">

                                            <asp:Label ID="lblLitNotificados" runat="server" Text="Configurar notificados:" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label>
                                             <asp:Label ID="lblNotificados" runat="server" Text="" CssClass="label"></asp:Label>

                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                    </table>
                    
                    <div style="padding-top: 15px">
                    <asp:HyperLink ID="HyperDetalle" runat="server" Width="100%" CssClass="CaptionLink"></asp:HyperLink>
                    </div> 

                </asp:Panel>
            </div>

            <ajx:DropShadowExtender TrackPosition="true" ID="DropShadowExtender1" runat="server" Opacity="0.5" Width="3" TargetControlID="pnlContrato" Rounded="true"></ajx:DropShadowExtender>
        </div>

        <%--Texto traslado--%>
        <table id="coment" cellspacing="5" cellpadding="1" width="100%" border="0" height="15%">
            <tr height="25%">
                <td valign="middle" colspan="8" height="100%">
                    <asp:TextBox ID="txtComent" runat="server" Width="100%" Height="100%" ReadOnly="True" TextMode="MultiLine"
                        BackColor="#E0E0E0"></asp:TextBox></td>
            </tr>
        </table>

        <div id="divProgreso" style="display: inline">
            <table id="tblProgreso" cellspacing="2" cellpadding="1" width="100%" border="0" runat="server">
                <tr style="height: 50px">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:TextBox ID="lblProgreso" runat="server" BorderStyle="None" BorderWidth="0" CssClass="captionBlue"
                            Width="100%" Style="text-align: center">Su solicitud está siendo tramitada. Espere unos instantes...</asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:Image ID="imgProgreso" runat="server" src="../_common/images/cursor-espera_grande.gif"></asp:Image></td>
                </tr>
            </table>
        </div>
        <div id="divForm2" style="display: none">
            <table cellspacing="3" cellpadding="1" width="100%" border="0">
                <tr>
                    <td width="100%" colspan="4">
                        <igtab:UltraWebTab ID="uwtGrupos" runat="server" Width="100%" BorderStyle="Solid" BorderWidth="1px"
                            CustomRules="padding:10px;" FixedLayout="True" DummyTargetUrl=" " ThreeDEffect="False" DisplayMode="Scrollable" EnableViewState="False">
                            <DefaultTabStyle Height="24px" CssClass="uwtDefaultTab">
                                <Padding Left="20px" Right="20px"></Padding>
                            </DefaultTabStyle>
                            <ClientSideEvents InitializeTabs="initTab" />
                            <RoundedImage NormalImage="ig_tab_blueb2.gif" HoverImage="ig_tab_blueb1.gif" FillStyle="LeftMergedWithCenter"></RoundedImage>
                        </igtab:UltraWebTab>

                    </td>
                </tr>
            </table>
            <input id="txtTraslado" type="hidden" name="CodPer" runat="server">
            <input id="txtIdTipo" type="hidden" name="txtIdTipo" runat="server">
            <input id="txtPeticionario" type="hidden" name="txtIdTipo" runat="server">
            <div id="divDropDowns" style="z-index: 103; visibility: hidden; position: absolute; top: 300px"></div>
            <div id="divCalculados" style="z-index: 110; visibility: hidden; position: absolute; top: 0px" name="divCalculados"></div>
            <div id="divAlta" style="visibility: hidden"></div>
            <input id="cadenaespera" type="hidden" name="cadenaespera" runat="SERVER">
            <input id="BotonCalcular" type="hidden" value="0" name="BotonCalcular" runat="server">
            <input id="hid_Contrato" type="hidden" runat="server" />
        </div>
    </form>
    <div id="divForm3" style="display: none">
        <form id="frmCalculados" name="frmCalculados" action="../alta/recalcularimportes.aspx?desde=contratos"
            method="post" target="fraWSServer">
        </form>
        <form id="frmDesglose" name="frmDesglose" method="post" target="winDesglose">
        </form>
    </div>
    <script type="text/javascript">OcultarEspera();</script>
</body>
</html>