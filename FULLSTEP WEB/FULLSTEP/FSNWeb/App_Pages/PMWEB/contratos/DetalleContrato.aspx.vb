﻿Public Class DetalleContrato
    Inherits FSNPage

#Region " Web Form Designer Generated Code "
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    End Sub
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
    Private m_bCabeceraEditable As Boolean
    Private _oInstancia As FSNServer.Instancia
    ''' <summary>
    ''' Propiedad que carga la informacion de la instancia
    ''' </summary>
    Protected ReadOnly Property oInstancia() As FSNServer.Instancia
        Get
            If _oInstancia Is Nothing Then
                If Me.IsPostBack Then
                    _oInstancia = CType(Cache("oInstancia" & FSNUser.Cod), FSNServer.Instancia)
                Else
                    _oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                    _oInstancia.ID = Request("Instancia")

                    If Request("Observador") = "1" Then
                        bObservador = True
                    End If

                    'Carga los datos de la instancia:
                    _oInstancia.Load(Idioma, True)
                    If Not _oInstancia.Solicitud Is Nothing Then
                        Dim resultadoBaja As Object = _oInstancia.EsPeticionarioFlujo(_oInstancia.Solicitud.ID, TiposWorkFlow.Baja, FSNUser.CodPersona)
                        Dim resultadoModificacion As Object = _oInstancia.EsPeticionarioFlujo(_oInstancia.Solicitud.ID, TiposWorkFlow.Modificacion, FSNUser.CodPersona)
                        RolBaja = resultadoBaja.Rol
                        RolModificacion = resultadoModificacion.Rol

                        If FSNServer.TipoAcceso.gbAccesoLCX Then
                            'aqui compruebo si acceso_LCX esta a true
                            Dim codContrato As Integer
                            codContrato = Request("Contrato")

                            bEsPeticionarioFlujoBaja = _oInstancia.EsPeticionarioFlujo_LCX(codContrato, FSNUser.EmpresaLCX, FSNUser.PerfilLCX, FSNUser.Cod)
                            bEsPeticionarioFlujoBaja = bEsPeticionarioFlujoBaja And CType(resultadoBaja.EsPeticionarioFlujo, Boolean)
                            bEsPeticionarioFlujoModificacion = _oInstancia.EsPeticionarioFlujo_LCX(codContrato, FSNUser.EmpresaLCX, FSNUser.PerfilLCX, FSNUser.Cod)
                            bEsPeticionarioFlujoModificacion = bEsPeticionarioFlujoModificacion And CType(resultadoModificacion.EsPeticionarioFlujo, Boolean)
                        Else
                            bEsPeticionarioFlujoBaja = CType(resultadoBaja.EsPeticionarioFlujo, Boolean)
                            bEsPeticionarioFlujoModificacion = CType(resultadoModificacion.EsPeticionarioFlujo, Boolean)
                        End If

                        If _oInstancia.Estado = TipoEstadoSolic.EnCursoFlujoBaja AndAlso bEsPeticionarioFlujoBaja Then
                            _oInstancia.Solicitud.Load(Idioma, True, TiposWorkFlow.Baja)
                        ElseIf _oInstancia.Estado = TipoEstadoSolic.EnCursoFlujoModificacion AndAlso bEsPeticionarioFlujoModificacion Then
                            _oInstancia.Solicitud.Load(Idioma, True, TiposWorkFlow.Modificacion)
                        Else
                            _oInstancia.Solicitud.Load(Idioma)
                        End If
                    End If

                    Me.InsertarEnCache("oInstancia" & FSNUser.Cod, _oInstancia)
                End If
            End If
            Return _oInstancia
        End Get
    End Property
    Private _oContrato As FSNServer.Contrato
    ''' <summary>
    ''' Propiedad que carga la informacion del contrato
    ''' </summary>
    Protected ReadOnly Property oContrato() As FSNServer.Contrato
        Get
            If _oContrato Is Nothing Then
                If Me.IsPostBack Then
                    _oContrato = CType(Cache("oContrato" & FSNUser.Cod), FSNServer.Contrato)
                Else
                    _oContrato = FSNServer.Get_Object(GetType(FSNServer.Contrato))
                    If Request("desde") = "SeguimientoSolicitudesPM" AndAlso Request("Contrato") = Nothing Then
                        Dim oContrato2 As FSNServer.Contrato
                        oContrato2 = FSNServer.Get_Object(GetType(FSNServer.Contrato))
                        _oContrato.ID = oContrato2.BuscarIdContrato(Request("Instancia"))
                        oContrato2 = Nothing
                        Me.hid_IdContrato.Value = _oContrato.ID
                    Else
                        _oContrato.ID = Request("Contrato")
                        Me.hid_IdContrato.Value = Request("Contrato")
                        _oContrato.Codigo = Request("Codigo")
                    End If

                    'Carga los datos del contrato:
                    _oContrato.Load(Idioma)
                    Me.hid_CodMoneda.Value = _oContrato.CodMoneda
                    Me.InsertarEnCache("oContrato" & FSNUser.Cod, _oContrato)
                End If
            End If
            Return _oContrato
        End Get
    End Property
    Private _oInstanciaGrupos As FSNServer.Grupos
    Protected ReadOnly Property oInstanciaGrupos() As FSNServer.Grupos
        Get
            If _oInstanciaGrupos Is Nothing Then
                If Me.IsPostBack Then
                    _oInstanciaGrupos = CType(Cache("oInstanciaGrupos_" & FSNUser.Cod), FSNServer.Grupos)
                Else
                    If _oInstancia.Estado = TipoEstadoSolic.EnCursoFlujoModificacion Then
                        _oInstancia.CargarCamposInstancia(Idioma, FSNUser.CodPersona, , True, bObservador, , TiposWorkFlow.Modificacion)
                    ElseIf _oInstancia.Estado = TipoEstadoSolic.EnCursoFlujoBaja Then
                        _oInstancia.CargarCamposInstancia(Idioma, FSNUser.CodPersona, , True, bObservador, , TiposWorkFlow.Baja)
                    Else
                        _oInstancia.CargarCamposInstancia(Idioma, FSNUser.CodPersona, , True, bObservador, , TiposWorkFlow.Alta)
                    End If
                    _oInstanciaGrupos = oInstancia.Grupos
                    Me.InsertarEnCache("oInstanciaGrupos_" & FSNUser.Cod, _oInstanciaGrupos)
                End If
            End If
            Return _oInstanciaGrupos
        End Get
    End Property
    Private bSoloLectura As Boolean = False
    Private bObservador As Boolean = False
    Private bEsPeticionarioFlujoBaja As Boolean = False
    Private bEsPeticionarioFlujoModificacion As Boolean = False
    Private RolBaja As Long
    Private RolModificacion As Long
    ''' <summary>
    ''' Carga los campos de la instancia
    ''' </summary>
    ''' <param name="sender">elemento</param>
    ''' <param name="e">argumentos</param>        
    ''' <remarks>Llamada desde:Evento que salta al cargar la pagina; Tiempo máximo:(depende del nº de campos de la instancia)</remarks>
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        InitializeComponent()
    End Sub
#End Region
    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
    Private arrOrdenEstados(7) As String
    Private arrTextosEstados(7) As String
    ''' <summary>
    ''' Carga el detalle de la solicitud.
    ''' </summary>
    ''' <param name="sender">Explicación parámetro 1</param>
    ''' <param name="e">Explicación parámetro 2</param>        
    ''' <remarks>Llamada desde; Tiempo máximo:2seg.</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.Expires = -1

        If Page.IsPostBack Then
            Select Case Request("__EVENTTARGET")
                Case "wddNotificados"
                    If Request("__EVENTARGUMENT") <> "" Then
                        CargarNotificados(Request("__EVENTARGUMENT"))
                    End If
                Case "wddContactos"
                    CargarContactos()
                    upContactos.Update()
            End Select
            CargarGrupos()
            Exit Sub
        Else
            VinculacionesPrimerGuardar.Value = 0
            VinculacionesIdGuardar.Value = 0
        End If

        If Request("Observador") = "1" Then
            bObservador = True
            Me.Observadores.Value = IIf(bObservador, "1", "0")
        End If

        Dim sVolver As String
        If Request("volver") <> "" Then
            sVolver = Request("volver")
            sVolver = Replace(sVolver, "*", "&")
            Session("VolverdetalleContrato") = sVolver
        End If

        CargarIdiomas()

        Instancia.Value = oInstancia.ID
        Contrato.Value = oContrato.ID
        Version.Value = oInstancia.Version
        SoloLectura.Value = bSoloLectura
        Instancia.Value = oInstancia.ID

        ConfigurarCabecera()
        CargarDatosContrato()
        RegistrarScripts()
        MostrarGraficoFlujo()
        MostrarImporteContrato()
        CargarGrupos()

        If Not ClientScript.IsStartupScriptRegistered("oRol_Id") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "oRol_Id", "var oRol_Id=" & oInstancia.RolActual & ";", True)

        PantallaVinculaciones.Value = oInstancia.ExisteVinculaciones

        Me.FindControl("frmDetalle").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())
    End Sub
    ''' <summary>
    ''' Si el usuario tiene permiso de visualizar el detalle del flujo se muestra el link
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub MostrarGraficoFlujo()
        Dim dsPermisosRol As DataSet
        dsPermisosRol = Session("FSN_User").VerDetallesPersona(oInstancia.ID)
        If dsPermisosRol.Tables.Count > 0 Then
            If oInstancia.Solicitud.Workflow = Nothing Then  'Si no hay workflow no muestra el hipervínculo para el detalle del workflow
                Me.HyperDetalle.Visible = False
            Else
                If dsPermisosRol.Tables(0).Rows(0).Item("VER_FLUJO") = 1 Then    'Si el rol no tiene permiso para ver el detalle del flujo.
                    Me.HyperDetalle.Visible = True
                    Me.HyperDetalle.NavigateUrl = "../seguimiento/NWhistoricoestados.aspx?Instancia=" + CStr(oInstancia.ID) & "&ConfiguracionGS=" & Request("ConfiguracionGS") & "&Codigo=" & oContrato.Codigo
                Else
                    Me.HyperDetalle.Visible = False
                End If
            End If
        End If
    End Sub
    ''' <summary>
    ''' Carga los campos de la instancia
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo máximo:(Depende de los campos de la isntancia)</remarks>
    Private Sub CargarGrupos()
        Dim oGrupo As FSNServer.Grupo
        Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
        Dim oucCampos As campos
        Dim oucDesglose As desgloseControl
        Dim oRow As DataRow = Nothing
        Dim lIndex As Integer = 0
        Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden

        uwtGrupos.Tabs.Clear()
        AplicarEstilosTab(uwtGrupos)

        If Not oInstanciaGrupos.Grupos Is Nothing Then
            If Not bObservador Then
                'SI EXISTE ALGUN CAMPO DE TIPO=3 (CALCULADO) HACEMOS VISIBLE EL BOTON DE CALCULAR
                FSNPageHeader.VisibleBotonCalcular = oInstanciaGrupos.Grupos.OfType(Of FSNServer.Grupo).Where(Function(x) x.DSCampos.Tables(0).Rows.OfType(Of DataRow).Where(Function(y) y("TIPO") = TipoCampoPredefinido.Calculado).Any).Any()
                If FSNPageHeader.VisibleBotonCalcular Then BotonCalcular.Value = 1
            End If
            For Each oGrupo In oInstanciaGrupos.Grupos
                oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden

                oInputHidden.ID = "txtPre_" + lIndex.ToString
                oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
                Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
                oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(Idioma), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
                oTabItem.Key = lIndex.ToString
                uwtGrupos.Tabs.Add(oTabItem)
                lIndex += 1

                If oGrupo.NumCampos <= 1 Then
                    For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                        If oRow.Item("VISIBLE") = 1 Then
                            Exit For
                        End If
                    Next
                    If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Desglose Or oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoDesglose Then
                        If oRow.Item("VISIBLE") = 0 Then
                            uwtGrupos.Tabs.Remove(oTabItem)
                        Else
                            oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                            oTabItem.ContentPane.UserControlUrl = "..\alta\desglose.ascx"
                            oucDesglose = oTabItem.ContentPane.UserControl
                            oucDesglose.ID = oGrupo.Id.ToString
                            oucDesglose.Campo = oRow.Item("ID_CAMPO")
                            oucDesglose.CampoOrigen = DBNullToDbl(oRow.Item("CAMPO_ORIGEN"))
                            oucDesglose.TabContainer = uwtGrupos.ClientID
                            oucDesglose.Instancia = oInstancia.ID
                            oucDesglose.TieneIdCampo = True
                            oucDesglose.Ayuda = DBNullToSomething(oRow.Item("AYUDA_" & Idioma))
                            oucDesglose.Version = oInstancia.Version
                            oucDesglose.SoloLectura = bSoloLectura Or (oRow.Item("ESCRITURA") = 0)
                            oucDesglose.PM = True
                            oucDesglose.Observador = bObservador
                            oucDesglose.Titulo = DBNullToSomething(oRow.Item("DEN_" & Idioma))
                            oucDesglose.TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Contrato
                            oInputHidden.Value = oucDesglose.ClientID
                        End If
                    Else
                        oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                        oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                        oucCampos = oTabItem.ContentPane.UserControl
                        oucCampos.Instancia = oInstancia.ID
                        oucCampos.ID = oGrupo.Id.ToString
                        oucCampos.dsCampos = oGrupo.DSCampos
                        oucCampos.IdGrupo = oGrupo.Id
                        oucCampos.Idi = Idioma
                        oucCampos.TabContainer = uwtGrupos.ClientID
                        oucCampos.Version = oInstancia.Version
                        oucCampos.SoloLectura = bSoloLectura
                        oucCampos.InstanciaMoneda = oInstancia.Moneda
                        oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                        oucCampos.PM = True
                        oucCampos.Formulario = oInstancia.Solicitud.Formulario
                        oucCampos.ObjInstancia = oInstancia
                        oucCampos.IdContrato = oContrato.ID
                        oucCampos.NombreContrato = oContrato.NombreAdjuntoContrato
                        oucCampos.Observador = bObservador
                        oInputHidden.Value = oucCampos.ClientID
                    End If
                Else
                    oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                    oTabItem.ContentPane.UserControlUrl = "..\alta\campos.ascx"
                    oucCampos = oTabItem.ContentPane.UserControl
                    oucCampos.Instancia = oInstancia.ID
                    oucCampos.ID = oGrupo.Id.ToString
                    oucCampos.dsCampos = oGrupo.DSCampos
                    oucCampos.IdGrupo = oGrupo.Id
                    oucCampos.Idi = Idioma
                    oucCampos.TabContainer = uwtGrupos.ClientID
                    oucCampos.Version = oInstancia.Version
                    oucCampos.SoloLectura = bSoloLectura
                    oucCampos.InstanciaMoneda = oInstancia.Moneda
                    oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                    oucCampos.PM = True
                    oucCampos.Formulario = oInstancia.Solicitud.Formulario
                    oucCampos.ObjInstancia = oInstancia
                    oucCampos.Observador = bObservador
                    oucCampos.IdContrato = oContrato.ID
                    oucCampos.NombreContrato = oContrato.NombreAdjuntoContrato
                    oInputHidden.Value = oucCampos.ClientID
                End If
                Me.FindControl("frmDetalle").Controls.Add(oInputHidden)
            Next
        End If

        If oInstancia.RolActual > 0 And oInstancia.Etapa > 0 Then
            'Participantes
            Dim oRol As FSNServer.Rol
            oRol = FSNServer.Get_Object(GetType(FSNServer.Rol))
            oRol.Id = oInstancia.RolActual
            oRol.Bloque = oInstancia.Etapa
            oRol.CargarParticipantes(Idioma, oInstancia.ID)

            If oRol.Participantes.Tables(0).Rows.Count > 0 Then
                Dim oucParticipantes As participantes

                oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                oTabItem.Text = Textos(43) 'Participantes
                oTabItem.Key = "PARTICIPANTES"

                uwtGrupos.Tabs.Add(oTabItem)

                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                oTabItem.ContentPane.UserControlUrl = "../alta/participantes.ascx"

                oucParticipantes = oTabItem.ContentPane.UserControl
                oucParticipantes.Idi = Idioma
                oucParticipantes.TabContainer = uwtGrupos.ClientID

                oucParticipantes.dsParticipantes = oRol.Participantes

                oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden
                oInputHidden.ID = "txtPre_PARTICIPANTES"
                oInputHidden.Value = oucParticipantes.ClientID
                Me.FindControl("frmDetalle").Controls.Add(oInputHidden)
            End If
        End If
    End Sub
    ''' <summary>
    ''' Revisado por: Sandra. Fecha: 17/03/2011
    ''' Registra las variables que vamos a utilizar en javascript
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub RegistrarScripts()
        Dim sClientTextVars As String
        sClientTextVars = ""
        sClientTextVars += "vdecimalfmt='" + FSNUser.DecimalFmt + "';"
        sClientTextVars += "vthousanfmt='" + FSNUser.ThousanFmt + "';"
        sClientTextVars += "vprecisionfmt='" + FSNUser.PrecisionFmt + "';"
        sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsUser", sClientTextVars)

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM2008", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")

        If Not Page.ClientScript.IsClientScriptBlockRegistered("varFila") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varFila", "<script>var sFila = '" & JSText(Textos(49)) & "' </script>")

        Dim sScript As String
        Dim ilGMN1 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN1
        Dim ilGMN2 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN2
        Dim ilGMN3 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN3
        Dim ilGMN4 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN4
        sScript = ""
        sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
        sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
        sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
        sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        ClientScript.RegisterStartupScript(Me.GetType(), "LongitudesCodigosKey", sScript)

        Me.FindControl("frmDetalle").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())

        If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayDesgloses") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")
        End If

        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(Textos(25)) + "';"
        sClientTexts += "arrTextosML[1] = '" + JSText(Textos(26)) + "';"
        sClientTexts += "arrTextosML[2] = '" + JSText(Textos(27)) + "';"
        sClientTexts += "arrTextosML[3] = '" + JSText(Textos(28)) + "';"
        sClientTexts += "arrTextosML[4] = '" + JSText(Textos(41)) + "';"
        sClientTexts += "arrTextosML[5] = '" + JSText(Textos(42)) + "';"
        sClientTexts += "arrTextosML[6] = '" + JSText(Textos(44)) + "';"
        sClientTexts += "arrTextosML[7] = '" + JSText(Textos(45)) + "';"
        sClientTexts += "arrTextosML[8] = '" + JSText(Textos(46)) + "';"
        sClientTexts += "arrTextosML[9] = '" + JSText(Textos(47)) + "';"
        sClientTexts += "arrTextosML[10] = '" + JSText(Textos(48)) + "';"
        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

        txtEmpresa.Attributes.Add("onKeyDown", "return ControlEmpresa(this, event);")
        txtProveedor.Attributes.Add("onKeyDown", "return ControlProveedor(this, event);")
    End Sub
    ''' <summary>
    ''' Revisado por: Sandra. Fecha: 17/03/2011
    ''' Configura la barra de menus de la pagina. Si viene desde GS se le aplica tb el estilo de GS
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub ConfigurarCabecera()
        FSNPageHeader.TextoBotonCalcular = Textos(17)
        FSNPageHeader.TextoBotonGuardar = Textos(19)
        FSNPageHeader.TextoBotonImpExp = Textos(20)
        FSNPageHeader.TextoBotonVolver = Textos(21)
        FSNPageHeader.TextoBotonEliminar = Textos(22)
        FSNPageHeader.TextoBotonModificar = Textos(40)

        FSNPageHeader.OnClientClickCalcular = "return CalcularCamposCalculados();return false;"
        FSNPageHeader.OnClientClickGuardar = "return Guardar();return false;"
        FSNPageHeader.OnClientClickImpExp = "return cmdImpExp_onclick();"
        FSNPageHeader.OnClientClickEliminar = "return EliminarInstancia();"
        If Request("desde") = "SeguimientoSolicitudesPM" Then
            FSNPageHeader.OnClientClickVolver = "return Volver(1);return false;"
        ElseIf Request("desde") = "Inicio" Then 'desde webpart
            FSNPageHeader.OnClientClickVolver = "return Volver(3);return false;"
        Else 'desde visor contratos
            FSNPageHeader.OnClientClickVolver = "return Volver(2);return false;"
        End If

        FSNPageHeader.VisibleBotonVolver = True

        If Request("ConfiguracionGS") = "1" Then
            'Estilo de la pagina
            FSNPageHeader.AspectoGS = True
            mi_body.Attributes.Add("class", "fondoGS")
            divProgreso.Attributes.Add("class", "fondoGS")
            Menu1.Visible = False
        Else
            FSNPageHeader.AspectoGS = False
            mi_body.Attributes.Add("class", "")
            divProgreso.Attributes.Add("class", "")
            Menu1.Visible = True
        End If
        mi_body.Attributes.Add("onload", "inicializar();")
        mi_body.Attributes.Add("onunload", "finalizar();")
        hidConfiguracionGS.Value = IIf(Request("ConfiguracionGS") = "1", "1", "0")

        FSNPageHeader.VisibleBotonImpExp = True

        If Request("OcultarBotonVolver") = "1" Then FSNPageHeader.VisibleBotonVolver = False

        If oContrato.Nombre <> Nothing Then
            FSNPageHeader.TituloCabecera = oContrato.Nombre
        Else
            FSNPageHeader.TituloCabecera = oInstancia.Solicitud.Den(Idioma)
        End If

        FSNPageHeader.UrlImagenCabecera = "images/Contrato.png"

        If FSNUser.AccesoCN Then
            If Not Page.ClientScript.IsClientScriptBlockRegistered("EntidadColaboracion") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "EntidadColaboracion", _
                    "var TipoEntidadColaboracion='CO';" & _
                    "var CodigoEntidadColaboracion ={identificador:" & oInstancia.ID & "" & _
                                                    ",texto:'" & JSText(oContrato.Codigo & " - " & IIf(oContrato.Nombre Is Nothing, oInstancia.Solicitud.Den(Idioma), oContrato.Nombre)) & "'" & _
                                                    ",proveedor:'" & JSText(oContrato.DenProve) & "'};", True)
            End If
        End If
        '***************** Configuración según el estado de la instancia: ****************************
        Select Case oInstancia.Estado
            Case TipoEstadoSolic.Guardada   'Guardada
                If oInstancia.Peticionario = FSNUser.CodPersona Then 'Si es el peticionario podrá hacer algo, sino no: 
                    FSNPageHeader.VisibleBotonEliminar = True
                    FSNPageHeader.VisibleBotonGuardar = True
                    FSNPageHeader.VisibleBotonAccion1 = True
                End If

            Case TipoEstadoSolic.EnCurso  'En curso
                bSoloLectura = True

            Case TipoEstadoSolic.SCPendiente, TipoEstadoSolic.SCAprobada, TipoEstadoSolic.SCRechazada, TipoEstadoSolic.SCAnulada, TipoEstadoSolic.SCCerrada
                bSoloLectura = True
                If oContrato.Estado <> TiposDeDatos.EstadosVisorContratos.Anulados AndAlso oContrato.Estado <> TiposDeDatos.EstadosVisorContratos.Rechazados Then
                    If oInstancia.Solicitud.WorkflowBaja <> Nothing AndAlso bEsPeticionarioFlujoBaja Then
                        FSNPageHeader.OnClientClickEliminar = "return iniciarWorkflowBaja(" & oInstancia.Solicitud.WorkflowBaja & "," & RolBaja & ");"
                        FSNPageHeader.VisibleBotonEliminar = True
                    End If
                    If oInstancia.Solicitud.WorkflowModificacion <> Nothing AndAlso bEsPeticionarioFlujoModificacion Then
                        FSNPageHeader.OnClientClickModificar = "return iniciarWorkflowModificacion(" & oInstancia.Solicitud.WorkflowModificacion & "," & RolModificacion & ");"
                        FSNPageHeader.VisibleBotonModificar = True
                    End If
                End If
            Case TipoEstadoSolic.Rechazada  'Rechazada    
                FSNPageHeader.VisibleBotonAccion1 = True
                FSNPageHeader.VisibleBotonEliminar = True
                bSoloLectura = Not (oInstancia.Peticionario = FSNUser.CodPersona) 'Si es el peticionario podrá hacer algo, sino no: 

            Case TipoEstadoSolic.Anulada  'Anulada
                bSoloLectura = True
            Case TipoEstadoSolic.EnCursoFlujoBaja
                If bEsPeticionarioFlujoBaja Then
                    Me.FSNPageHeader.VisibleBotonAccion1 = True
                Else
                    bSoloLectura = True
                End If
            Case TipoEstadoSolic.EnCursoFlujoModificacion
                If bEsPeticionarioFlujoModificacion Then
                    Me.FSNPageHeader.VisibleBotonAccion1 = True
                Else
                    bSoloLectura = True
                End If
        End Select

        Dim oRow As DataRow
        ''''Carga las acciones posibles en el botón de Otras acciones (si es el peticionario) :
        If (oInstancia.Peticionario = FSNUser.CodPersona And (oInstancia.Estado = TipoEstadoSolic.Guardada Or oInstancia.Estado = TipoEstadoSolic.Rechazada)) _
            OrElse oInstancia.Estado = TipoEstadoSolic.EnCursoFlujoBaja _
            OrElse oInstancia.Estado = TipoEstadoSolic.EnCursoFlujoModificacion Then

            oInstancia.DevolverEtapaActual(Idioma, FSNUser.CodPersona)
            m_bCabeceraEditable = True

            Dim oPopMenu As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
            Dim oItem As Infragistics.WebUI.UltraWebNavigator.Item

            If (oInstancia.RolActual = 0 Or oInstancia.Etapa = 0) AndAlso Not (bEsPeticionarioFlujoModificacion Or bEsPeticionarioFlujoBaja) Then
                Me.FSNPageHeader.VisibleBotonAccion1 = False
                Me.FSNPageHeader.VisibleBotonGuardar = False
                m_bCabeceraEditable = False

            Else
                Dim oRol As FSNServer.Rol
                oRol = FSNServer.Get_Object(GetType(FSNServer.Rol))
                oRol.Id = oInstancia.RolActual
                oRol.Bloque = oInstancia.Etapa

                Me.VinculacionesIdGuardar.Value = oRol.CargarIdGuardar()
                Me.Rol.Value = oInstancia.RolActual
                Me.Bloque.Value = oInstancia.Etapa


                Dim oDSAcciones As DataSet
                If oInstancia.Estado = TipoEstadoSolic.Rechazada = True Then
                    oDSAcciones = oRol.CargarAcciones(Idioma, True)
                Else
                    oDSAcciones = oRol.CargarAcciones(Idioma)
                End If

                oPopMenu = Me.uwPopUpAcciones
                Dim blnAprobar As Boolean = False
                Dim blnRechazar As Boolean = False
                If oDSAcciones.Tables.Count > 0 Then
                    If oDSAcciones.Tables(0).Rows.Count = 0 Then
                        FSNPageHeader.VisibleBotonAccion1 = False
                    ElseIf oDSAcciones.Tables(0).Rows.Count > 2 Then
                        oPopMenu.Visible = True
                        FSNPageHeader.VisibleBotonAccion1 = True
                        FSNPageHeader.TextoBotonAccion1 = Textos(18)
                        FSNPageHeader.OnClientClickAccion1 = "return mostrarMenuAcciones(event,1);"

                        For Each oRow In oDSAcciones.Tables(0).Rows
                            If IsDBNull(oRow.Item("DEN")) Then
                                oItem = oPopMenu.Items.Add("&nbsp;")
                            Else
                                If oRow.Item("DEN") = "" Then
                                    oItem = oPopMenu.Items.Add("&nbsp;")
                                Else
                                    oItem = oPopMenu.Items.Add(DBNullToStr(oRow.Item("DEN")))
                                End If
                            End If
                            oItem.TargetUrl = "javascript:EjecutarAccion(" + oRow.Item("ACCION").ToString() + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "')"
                        Next
                    Else
                        Dim cont As Byte = 0
                        For Each oRow In oDSAcciones.Tables(0).Rows
                            If cont = 0 Then
                                FSNPageHeader.VisibleBotonAccion1 = True
                                FSNPageHeader.TextoBotonAccion1 = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                                FSNPageHeader.OnClientClickAccion1 = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"

                                cont = 1
                            Else
                                FSNPageHeader.VisibleBotonAccion2 = True
                                FSNPageHeader.TextoBotonAccion2 = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                                FSNPageHeader.OnClientClickAccion2 = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                            End If
                        Next
                    End If
                End If
            End If
        End If
    End Sub
    ''' <summary>
    ''' Carga la informacion del contrato en los campos
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub CargarDatosContrato()
        Dim sEstado As String = Nothing
        Dim i As Byte
        If oContrato.Estado >= 0 Then
            For i = 0 To UBound(Me.arrOrdenEstados)
                If oContrato.Estado = arrOrdenEstados(i) Then
                    sEstado = arrTextosEstados(i)
                    Exit For
                End If
            Next
            If Not String.IsNullOrEmpty(sEstado) Then
                sEstado = " (" & sEstado & ")"
            End If
        End If

        If Not (oInstancia.Estado = TipoEstadoSolic.Guardada OrElse (m_bCabeceraEditable = True AndAlso (oInstancia.Estado = TipoEstadoSolic.EnCursoFlujoModificacion OrElse oInstancia.Estado = TipoEstadoSolic.EnCursoFlujoBaja))) Then
            pnlContratoEditable.Visible = False
            pnlContrato.Visible = True

            lblIDInstanciayEstado.Text = oContrato.Codigo & sEstado
            lblFechaInicio.Text = FormatDate(oContrato.FechaInicio, FSNUser.DateFormat)
            lblProveedor.Text = oContrato.CodProve & " " & oContrato.DenProve
            lblContacto.Text = oContrato.Contacto
            If Not oContrato.FechaFin Is Nothing Then
                lblFechaFin.Text = FormatDate(oContrato.FechaFin, FSNUser.DateFormat)
            Else
                lblFechaFin.Visible = False
            End If
            lblEmpresa.Text = oContrato.Empresa
            lblMoneda.Text = oContrato.CodMoneda & " - " & oContrato.DenMoneda

            imgInfProv.ImageUrl = "./images/info.gif"
            imgInfPeticionario.ImageUrl = "./images/info.gif"
            imgInfEmpresa.ImageUrl = "./images/info.gif"

            lblPeticionario.Text = oInstancia.NombrePeticionario

            lblFechaCreacion.Text = "(" & FormatDate(oInstancia.FechaAlta, FSNUser.DateFormat) & ")"

            'alertas
            lblMostrarAlertaNoEdit.Text = String.Empty
            lblRecordatorioAlertaNoEdit.Text = String.Empty
            lblNotificadosNoEdit.Text = String.Empty

            'alertas
            If oContrato.Alerta > 0 Then '5 semana(s) antes de expira
                lblMostrarAlertaNoEdit.Text = getPeriodoAlertas(oContrato.Alerta, oContrato.PeriodoAlerta) & " " _
                                    & getTextPeriodoAlertas(oContrato.PeriodoAlerta) & " " _
                                    & Textos(8)
            End If

            If oContrato.RepetirEmail > 0 Then '5 semana(s)
                lblRecordatorioAlertaNoEdit.Text = getPeriodoAlertas(oContrato.RepetirEmail, oContrato.PeriodoRepetirEmail) & " " _
                                    & getTextPeriodoAlertas(oContrato.PeriodoRepetirEmail)
            End If

            If oContrato.NotificadosNombreyEmail.Length > 0 Then
                lblNotificadosNoEdit.Text = CargarNotificadosText(oContrato.NotificadosNombreyEmail)
            End If

            'Panel infos
            imgInfProv.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosProveedor.AnimationClientID & "', event, '" & FSNPanelDatosProveedor.DynamicPopulateClientID & "', '" & oContrato.CodProve & "'); return false;")
            imgInfPeticionario.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosPeticionario.AnimationClientID & "', event, '" & FSNPanelDatosPeticionario.DynamicPopulateClientID & "', '" & oInstancia.Peticionario & "'); return false;")
            imgInfEmpresa.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosEmpresa.AnimationClientID & "', event, '" & FSNPanelDatosEmpresa.DynamicPopulateClientID & "', '" & oContrato.IdEmpresa & "'); return false;")
        Else
            pnlContratoEditable.Visible = True
            pnlContrato.Visible = False
            ''''Parte editable
            lblIDInstanciayEstadoEditable.Text = oContrato.Codigo & sEstado
            imgBuscarNotificados.Attributes.Add("onclick", "BuscarNotificadoInterno();return false;")
            imgCollapseAlertas.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Theme & "/images/contraer_color.gif"
            imgExpandAlertas.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Me.Theme & "/images/expandir_color.gif"
            imgCollapseAlertas.Attributes.Add("onclick", "OcultarAlertas();return false;")
            imgExpandAlertas.Attributes.Add("onclick", "OcultarAlertas();return false;")
            lblConfiguracionAlertas.Attributes.Add("onclick", "OcultarAlertas();return false;")
            imgEmpresaLupa.Attributes.Add("onClick", "var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorEmpresas.aspx?desde=altaContrato&Solicitud=" & oInstancia.Solicitud.ID & "&Rol=" & oInstancia.RolActual & "&copia=1&Contrato=" & oContrato.ID & "' ,'_blank','width=800,height=600, location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no');return false;")
            txtEmpresa_AutoCompleteExtender.ContextKey = IIf(Request.QueryString("Solicitud") = Nothing, "0", Request.QueryString("Solicitud")) & "**" & oInstancia.RolActual & "**1**" & oContrato.ID
            ImgProveedorLupa.OnClientClick = "var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorProveedores.aspx?PM=true&Contr=true', '_blank', 'width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes');return false;"

            'Cargar Combo Expirar
            Dim Item As New Infragistics.Web.UI.ListControls.DropDownItem

            Item.Value = PeriodosAlertaContratos.Dias
            Item.Text = Textos(13) 'dia(s)
            wddPeriodo.Items.Add(Item)
            wddEmail.Items.Add(Item)
            Item = New Infragistics.Web.UI.ListControls.DropDownItem
            Item.Value = PeriodosAlertaContratos.Semanas
            Item.Text = Textos(14) 'semana(s)
            wddPeriodo.Items.Add(Item)
            wddEmail.Items.Add(Item)
            Item = New Infragistics.Web.UI.ListControls.DropDownItem
            Item.Value = PeriodosAlertaContratos.Meses
            Item.Text = Textos(15) '"mes(es)"
            wddPeriodo.Items.Add(Item)
            wddEmail.Items.Add(Item)

            wddPeriodo.SelectedValue = oContrato.PeriodoAlerta
            wddEmail.SelectedValue = oContrato.PeriodoRepetirEmail

            Dim iAlerta As Integer
            Select Case oContrato.PeriodoAlerta
                Case PeriodosAlertaContratos.Dias
                    iAlerta = oContrato.Alerta
                Case PeriodosAlertaContratos.Semanas
                    iAlerta = oContrato.Alerta / 7
                Case PeriodosAlertaContratos.Meses
                    iAlerta = oContrato.Alerta / 30
            End Select
            txtAlerta.Text = iAlerta

            Dim iRepetirEmail As Integer
            Select Case oContrato.PeriodoRepetirEmail
                Case PeriodosAlertaContratos.Dias
                    iRepetirEmail = oContrato.RepetirEmail
                Case PeriodosAlertaContratos.Semanas
                    iRepetirEmail = oContrato.RepetirEmail / 7
                Case PeriodosAlertaContratos.Meses
                    iRepetirEmail = oContrato.RepetirEmail / 30
            End Select
            txtPeriodoEmail.Text = iRepetirEmail

            hidEmpresa.Value = oContrato.IdEmpresa
            If hidEmpresa.Value <> "" AndAlso hidEmpresa.Value <> "0" Then
                Dim oEmpresa As FSNServer.Empresa
                oEmpresa = FSNServer.Get_Object(GetType(FSNServer.Empresa))
                oEmpresa.ID = hidEmpresa.Value
                oEmpresa.Load(FSNUser.Idioma)
                txtEmpresa.Text = oEmpresa.Den
                oEmpresa = Nothing
            End If

            hid_Proveedor.Value = oContrato.CodProve
            txtProveedor.Text = oContrato.DenProve
            fsentryFecInicio.Valor = oContrato.FechaInicio
            fsentryFecFin.Valor = oContrato.FechaFin

            CargarNotificados(oContrato.NotificadosNombreyEmail)
            CargarContactos()
            CargarMonedas()
        End If
    End Sub

    ''' <summary>
    ''' devuelve el texto del periodo
    ''' </summary>
    Private Function getTextPeriodoAlertas(ByVal intPeriodoID As Integer) As String
        Select Case intPeriodoID
            Case 1
                Return Textos(13) 'dia(s)
            Case 2
                Return Textos(14) 'semana(s)
            Case 3
                Return Textos(15) 'mes(es)
            Case Else
                Return String.Empty
        End Select
    End Function

    ''' <summary>
    ''' devuelve la catidad convirtiendola en el periodo
    ''' </summary>
    Private Function getPeriodoAlertas(ByVal intNumDias As Integer, ByVal intPeriodoID As Integer) As Integer
        Select Case intPeriodoID
            Case 1
                Return intNumDias
            Case 2
                Return intNumDias / 7
            Case 3
                Return intNumDias / 30
            Case Else
                Return 0
        End Select
    End Function



    ''' <summary>
    ''' Carga el idioma correspondiente el los controles
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0seg.</remarks>
    Private Sub CargarIdiomas()

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.DetalleContrato

        lblLitFechaInicio.Text = Textos(0) & ":" '"Fecha de inicio"
        lblLitFechaExpiracion.Text = Textos(1) & ":" '"Fecha de expiración"
        'lblLitProveedor.Text = "(*) " & Textos(2) '"Proveedor"
        lblLitContacto.Text = Textos(3) & ":" '"Contacto"
        lblLitEmpresa.Text = Textos(4) & ":" '"Empresa"
        lblLitMoneda.Text = Textos(5) & ":" '"Moneda"
        lblCamposObligatorios.Text = Textos(50)

        lblLitFechaInicioEditable.Text = "(*) " & Textos(0)  '"Fecha de inicio"
        lblLitFechaExpiracionEditable.Text = Textos(1) '"Fecha de expiración"
        lblLitProveedorEditable.Text = "(*) " & Textos(2) '"Proveedor"
        lblLitContactoEditable.Text = Textos(3) '"Contacto"
        lblLitEmpresaEditable.Text = "(*) " & Textos(4) '"Empresa"
        lblLitMonedaEditable.Text = "(*) " & Textos(5) '"Moneda"

        lblConfiguracionAlertas.Text = Textos(6) '"Configuración de alertas"
        lblMostrarAlerta.Text = Textos(7) '"Mostrar alerta"
        lblAntesExpirar.Text = Textos(8)
        lblEmail.Text = Textos(11)
        lblNotificados.Text = Textos(12) & ":" '"Notificados" & ":"

        lblLitNotificadosNoEdit.Text = Textos(12) & ":" '"Notificados" & ":"
        lblLitMostrarAlertaNoEdit.Text = Textos(7) '"Mostrar alerta"
        lblLitRecordatorioAlertaNoEdit.Text = Textos(11) & ":" ' Enviar recordatorio cada

        Me.cadenaespera.Value = Textos(23) '"Su solicitud está siendo tramitada. Espere unos instantes..."
        Me.lblProgreso.Text = Textos(24) '"Se está cargando la solicitud. Espere unos instantes..."
        'lblMensajeAlertas.Text = Textos(29) '"No hay ninguna alerta establecida."
        HyperDetalle.Text = Textos(30)

        arrOrdenEstados(0) = TiposDeDatos.EstadosVisorContratos.Guardado
        arrOrdenEstados(1) = TiposDeDatos.EstadosVisorContratos.En_Curso_De_Aprobacion
        arrOrdenEstados(2) = TiposDeDatos.EstadosVisorContratos.Vigentes
        arrOrdenEstados(3) = TiposDeDatos.EstadosVisorContratos.Proximo_a_Expirar
        arrOrdenEstados(4) = TiposDeDatos.EstadosVisorContratos.Expirados
        arrOrdenEstados(5) = TiposDeDatos.EstadosVisorContratos.Rechazados
        arrOrdenEstados(6) = TiposDeDatos.EstadosVisorContratos.Anulados

        arrTextosEstados(0) = Textos(31) '"Guardados"
        arrTextosEstados(1) = Textos(32) '"En curso de aprobación"
        arrTextosEstados(2) = Textos(33) '"Vigentes"
        arrTextosEstados(3) = Textos(34) '"proximo a expirar"
        arrTextosEstados(4) = Textos(35) '"Expirados"
        arrTextosEstados(5) = Textos(36) '"Rechazados"
        arrTextosEstados(6) = Textos(37) '"Anulados"

        'Textos Panel info-s
        FSNPanelDatosProveedor.Titulo = Textos(2) '"Proveedor"
        FSNPanelDatosProveedor.SubTitulo = Textos(38) '"Información detallada"


        FSNPanelDatosPeticionario.Titulo = Textos(39) '"Peticionario"
        FSNPanelDatosPeticionario.SubTitulo = Textos(38) '"Datos del peticionario"

        FSNPanelDatosEmpresa.Titulo = Textos(4) '"Empresa"
        FSNPanelDatosEmpresa.SubTitulo = Textos(38) '"Datos de empresa"

    End Sub
    ''' <summary>
    ''' Muestra o oculta el importe del contrato si se puso un campo de importe en el formulario
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0seg.</remarks>
    Private Sub MostrarImporteContrato()
        If Not oInstancia.CampoImporte Is Nothing Then
            lblMoneda.Visible = False
            lblLitMoneda.Visible = False
            Me.lblLitImporte.Text = Textos(16) & ":"
            lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & oContrato.CodMoneda
            lblLitImporte.Visible = True
            lblImporte.Visible = True

            lblImporteEditable.Visible = True
            lblImporteValorEditable.Visible = True
            lblMonedaImporteEditable.Visible = True
            lblImporteEditable.Text = Textos(16) & ": "
            lblImporteValorEditable.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " "
            lblMonedaImporteEditable.Text = wddMonedas.CurrentValue
            lblMonedaImporteEditable.ToolTip = wddMonedas.CurrentValue

        Else
            lblMoneda.Visible = True
            lblLitMoneda.Visible = True
            lblLitImporte.Visible = False
            Me.lblImporte.Visible = False
        End If
    End Sub
    ''' <summary>
    ''' Revisado por: Sandra. Fecha: 09/03/2011
    ''' Carga los contactos del proveedor seleccionado. Mientras se hace la carga, se deshabilita el dropdown.
    ''' </summary>
    ''' <remarks>Llamada desde: ConfigurarCabeceraContrato y Page_Load(); Tiempo máximo: 0,4 sg.</remarks>
    Private Sub CargarContactos()

        wddContactos.DisplayMode = Infragistics.Web.UI.ListControls.DropDownDisplayMode.ReadOnly

        wddContactos.Items.Clear()
        wddContactos.CurrentValue = Nothing

        If hid_Proveedor.Value <> "" Then
            Dim oProve As FSNServer.Proveedor
            oProve = FSNServer.Get_Object(GetType(FSNServer.Proveedor))

            oProve.Cod = hid_Proveedor.Value

            'Contactos del proveedor
            oProve.CargarContactos()

            wddContactos.DataSource = oProve.Contactos.Tables(0)
            wddContactos.TextField = "CONTACTO"
            wddContactos.ValueField = "ID"
            wddContactos.DataBind()
        End If

        If oContrato.IdContacto <> Nothing Then
            wddContactos.SelectedValue = oContrato.IdContacto
        End If


        Dim oItem As New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 0
        oItem.Text = ""
        wddContactos.Items.Insert(0, oItem)

        wddContactos.DisplayMode = Infragistics.Web.UI.ListControls.DropDownDisplayMode.DropDown
    End Sub
    ''' <summary>
    ''' Carga la combo con las monedas, por defecto selecciona la moneda de la solicitud
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load; Tiempo máximo:=0,32seg.</remarks>
    Private Sub CargarMonedas()

        Dim oMonedas As FSNServer.Monedas
        oMonedas = FSNServer.Get_Object(GetType(FSNServer.Monedas))
        oMonedas.LoadData(Idioma)

        wddMonedas.DataSource = oMonedas.Data
        wddMonedas.TextField = "DEN"
        wddMonedas.ValueField = "COD"
        wddMonedas.DataBind()

        If oContrato.CodMoneda = Nothing Then
            wddMonedas.CurrentValue = ""
            wddMonedas.SelectedValue = Nothing
        Else
            wddMonedas.SelectedValue = oContrato.CodMoneda
        End If

        oMonedas = Nothing
    End Sub
    ''' <summary>
    ''' Carga en el combo correspondiente la lista de contactos internos que devuelve el buscador de personas
    ''' </summary>
    ''' <param name="ListaNotificados">Lista de los contactos internos separados por ### con su value y nombre</param>
    ''' <remarks></remarks>
    Private Sub CargarNotificados(ByVal ListaNotificados As String)

        Dim Item As Infragistics.Web.UI.ListControls.DropDownItem
        Dim Notificados() As String = Split(ListaNotificados, "###")

        For i As Integer = 0 To Notificados.Length - 2
            Item = New Infragistics.Web.UI.ListControls.DropDownItem
            Item.Value = Split(Notificados(i), "#")(0)
            Item.Text = Split(Notificados(i), "#")(1) & IIf(Split(Notificados(i), "#")(2) <> "null", " (" & Split(Notificados(i), "#")(2) & ")", "")
            Item.Selected = True


            If wddNotificados.Items.FindItemByValue(Item.Value) Is Nothing Then
                If wddNotificados.CurrentValue = "" Then
                    wddNotificados.CurrentValue = Item.Text
                Else
                    wddNotificados.CurrentValue = wddNotificados.CurrentValue & "," & Item.Text
                End If
                wddNotificados.Items.Add(Item)
            End If

        Next
    End Sub

    ''' <summary>
    ''' Carga en el label la lista de contactos internos que devuelve el buscador de personas
    ''' </summary>
    ''' <param name="ListaNotificados">Lista de los contactos internos separados por ### con su value y nombre</param>
    ''' <remarks></remarks>
    Private Function CargarNotificadosText(ByVal ListaNotificados As String) As String

        Dim Item As Infragistics.Web.UI.ListControls.DropDownItem
        Dim Notificados() As String = Split(ListaNotificados, "###")
        Dim strReturn As String = String.Empty

        For i As Integer = 0 To Notificados.Length - 2
            If strReturn.Length > 0 Then strReturn &= "; "
            strReturn &= Split(Notificados(i), "#")(1) & IIf(Split(Notificados(i), "#")(2) <> "null", " (" & Split(Notificados(i), "#")(2) & ")", "")
        Next

        Return strReturn

    End Function

End Class