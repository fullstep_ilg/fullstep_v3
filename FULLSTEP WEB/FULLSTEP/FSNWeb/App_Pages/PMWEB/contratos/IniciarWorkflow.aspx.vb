﻿Public Partial Class IniciarWorkflow
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Elimina la instancia:
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oInstancia As FSNServer.Instancia

        oInstancia = FSWSServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = Request("Instancia")
        oInstancia.IniciarWorkflowContrato(Request("Contrato"), Request("Estado"), Request("Workflow"), FSNUser.CodPersona, Request("Rol"))

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "volver", "<script>window.parent.fraWSMain.RecargarContrato()</script>")
    End Sub

End Class