﻿Public Partial Class EliminarContrato
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Elimina la instancia:
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oInstancia As FSNServer.Instancia
        Dim sUrl As String

        oInstancia = FSWSServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = Request("Instancia")
        oInstancia.EliminarContrato(Request("Contrato"), Session("FSN_User").Cod)

        If Request("Alta") = "1" Then
            sUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx"
        Else
            sUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Contratos/VisorContratos.aspx"
        End If

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "volver", "<script>EliminarOK('" & sUrl & "')</script>")
    End Sub

End Class