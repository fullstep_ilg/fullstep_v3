﻿Partial Public Class ComprobarAprobContratos
    Inherits FSNPage

    ''' <summary>
    ''' Dirigue la solicitud ha la pagina que corresponda
    ''' </summary>
    ''' <param name="sender">Explicación parámetro 1</param>
    ''' <param name="e">Explicación parámetro 2</param>        
    ''' <remarks>Llamada desde; Tiempo máximo:0,3seg.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim oInstancia As FSNServer.Instancia
        Dim lContrato As Long
        Dim oContrato As FSNServer.Contrato
        Dim sDesde As String = Request("desde")
        Dim direccion As String = Request("volver")
        Dim sUrlRedirect As String

        oContrato = FSNServer.Get_Object(GetType(FSNServer.Contrato))


        lContrato = oContrato.BuscarIdContrato(Request("Instancia"))

        If Request("Contrato") <> Nothing Then
            oContrato.Codigo = Request("Contrato")
        Else
            oContrato.Codigo = oContrato.BuscarCodContrato(Request("Instancia"))
        End If

        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = Request("Instancia")
        oInstancia.DevolverEtapaActual(Idioma, Usuario.CodPersona)

        If oInstancia.InstanciaBloqueTrasladada = True Then
            sUrlRedirect = "GestionContratoTrasladada.aspx?Instancia=" & CStr(oInstancia.ID) & "&Codigo=" & oContrato.Codigo & "&Contrato=" & lContrato
        ElseIf oContrato.WorkflowSoloIniciado(oInstancia.ID) OrElse oInstancia.Etapa = 0 Then 'etapa finalizada
            sUrlRedirect = "DetalleContrato.aspx?Instancia=" & CStr(oInstancia.ID) & "&Contrato=" & lContrato & "&Codigo=" & oContrato.Codigo & "&Observador=" & IIf(oInstancia.ComprobarEsObservador(Usuario.CodPersona, True), 1, 0)
        ElseIf oInstancia.ComprobarEsObservador(Usuario.CodPersona, True) AndAlso oInstancia.AprobadorActual <> Nothing AndAlso oInstancia.AprobadorActual <> Usuario.CodPersona Then 'es observador pero no aprobador
            sUrlRedirect = "DetalleContrato.aspx?Instancia=" & CStr(oInstancia.ID) & "&Contrato=" & lContrato & "&Codigo=" & oContrato.Codigo & "&Observador=1"
        ElseIf oInstancia.AprobadorActual = Nothing AndAlso oInstancia.Etapa <> 0 Then
            sUrlRedirect = "aprobacionContrato.aspx?Instancia=" & CStr(oInstancia.ID) & "&Contrato=" & lContrato
        ElseIf oInstancia.Estado = TipoEstadoSolic.Guardada Then
            sUrlRedirect = "DetalleContrato.aspx?Instancia=" & CStr(oInstancia.ID) & "&Contrato=" & lContrato & "&Codigo=" & oContrato.Codigo & "&Observador=0"
        Else
            sUrlRedirect = "GestionContrato.aspx?Instancia=" & CStr(oInstancia.ID) & "&Codigo=" & oContrato.Codigo & "&Contrato=" & lContrato
        End If

        sUrlRedirect = sUrlRedirect & "&ConfiguracionGS=" & Request("ConfiguracionGS") & "&Volver=" & direccion & "&desde=" & sDesde
        If Not String.IsNullOrEmpty(Request("OcultarBotonVolver")) Then
            sUrlRedirect = sUrlRedirect & "&OcultarBotonVolver=" & Request("OcultarBotonVolver")
        End If
        Response.Redirect(sUrlRedirect)

        oInstancia = Nothing
        oContrato = Nothing

    End Sub

End Class