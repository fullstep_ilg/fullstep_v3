﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class altaContrato
    
    '''<summary>
    '''my_body control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents my_body As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''frmAlta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents frmAlta As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager
    
    '''<summary>
    '''Menu1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Menu1 As Global.Fullstep.FSNWeb.MenuControl
    
    '''<summary>
    '''FSNPageHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FSNPageHeader As Global.Fullstep.FSNWebControls.FSNPageHeader
    
    '''<summary>
    '''pnlContrato control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlContrato As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblLitFechaInicio control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLitFechaInicio As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblLitProveedor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLitProveedor As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblLitContacto control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLitContacto As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''fsentryFecInicio control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsentryFecInicio As Global.Fullstep.DataEntry.GeneralEntry
    
    '''<summary>
    '''lblProveedor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProveedor As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''imgInfProv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgInfProv As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''txtProveedor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtProveedor As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ImgProveedorLupa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ImgProveedorLupa As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''upContactos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upContactos As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''wddContactos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wddContactos As Global.Infragistics.Web.UI.ListControls.WebDropDown
    
    '''<summary>
    '''lblImporte control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblImporte As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblImporteValor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblImporteValor As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblMonedaImporte control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMonedaImporte As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblLitFechaExpiracion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLitFechaExpiracion As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblLitEmpresa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLitEmpresa As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblLitMoneda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLitMoneda As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''fsentryFecFin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsentryFecFin As Global.Fullstep.DataEntry.GeneralEntry
    
    '''<summary>
    '''txtEmpresa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEmpresa As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtEmpresa_AutoCompleteExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEmpresa_AutoCompleteExtender As Global.AjaxControlToolkit.AutoCompleteExtender
    
    '''<summary>
    '''lblEmpresa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEmpresa As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''imgInfEmpresa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgInfEmpresa As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''imgEmpresaLupa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgEmpresaLupa As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''hidEmpresa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hidEmpresa As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''hidConfiguracionGS control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hidConfiguracionGS As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''wddMonedas control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wddMonedas As Global.Infragistics.Web.UI.ListControls.WebDropDown
    
    '''<summary>
    '''lblCamposObligatorios control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCamposObligatorios As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlAlertas control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlAlertas As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''pnlTituloAlertas control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlTituloAlertas As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''imgCollapseAlertas control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgCollapseAlertas As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''imgExpandAlertas control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgExpandAlertas As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''lblConfiguracionAlertas control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblConfiguracionAlertas As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlAlertasTabla control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlAlertasTabla As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblMostrarAlerta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMostrarAlerta As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtAlerta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAlerta As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''imgArribaAlerta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgArribaAlerta As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''imgAbajoAlerta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgAbajoAlerta As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''wddPeriodo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wddPeriodo As Global.Infragistics.Web.UI.ListControls.WebDropDown
    
    '''<summary>
    '''lblAntesExpirar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAntesExpirar As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEmail As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtPeriodoEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPeriodoEmail As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''imgArribaAlerta2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgArribaAlerta2 As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''imgAbajoAlerta2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgAbajoAlerta2 As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''wddEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wddEmail As Global.Infragistics.Web.UI.ListControls.WebDropDown
    
    '''<summary>
    '''lblNotificados control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNotificados As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''updNotificadores control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updNotificadores As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''wddNotificados control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wddNotificados As Global.Infragistics.Web.UI.ListControls.WebDropDown
    
    '''<summary>
    '''imgBuscarNotificados control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgBuscarNotificados As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''NumericUpDownExtender1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents NumericUpDownExtender1 As Global.AjaxControlToolkit.NumericUpDownExtender
    
    '''<summary>
    '''NumericUpDownExtender2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents NumericUpDownExtender2 As Global.AjaxControlToolkit.NumericUpDownExtender
    
    '''<summary>
    '''DropShadowExtender1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DropShadowExtender1 As Global.AjaxControlToolkit.DropShadowExtender
    
    '''<summary>
    '''tblProgreso control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblProgreso As Global.System.Web.UI.HtmlControls.HtmlTable
    
    '''<summary>
    '''lblProgreso control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProgreso As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''imgProgreso control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgProgreso As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''uwtGrupos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents uwtGrupos As Global.Infragistics.WebUI.UltraWebTab.UltraWebTab
    
    '''<summary>
    '''Solicitud control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Solicitud As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''Bloque control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Bloque As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hid_IdContrato control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hid_IdContrato As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hid_CodContrato control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hid_CodContrato As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''cadenaespera control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cadenaespera As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''BotonCalcular control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BotonCalcular As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''PantallaMaper control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PantallaMaper As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''NEW_Instancia control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents NEW_Instancia As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hid_Proveedor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hid_Proveedor As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hid_ProcesoContrato control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hid_ProcesoContrato As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hid_GruposContrato control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hid_GruposContrato As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hid_ItemsContrato control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hid_ItemsContrato As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hid_NombreContrato control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hid_NombreContrato As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hid_PathContrato control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hid_PathContrato As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hid_DesdeInicio control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hid_DesdeInicio As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''IdInstanciaImportar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents IdInstanciaImportar As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''PantallaVinculaciones control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PantallaVinculaciones As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''divAcciones control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divAcciones As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''uwPopUpAcciones control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents uwPopUpAcciones As Global.Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
    
    '''<summary>
    '''FSNPanelDatosProveedor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FSNPanelDatosProveedor As Global.Fullstep.FSNWebControls.FSNPanelInfo
    
    '''<summary>
    '''FSNPanelDatosEmpresa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FSNPanelDatosEmpresa As Global.Fullstep.FSNWebControls.FSNPanelInfo
End Class
