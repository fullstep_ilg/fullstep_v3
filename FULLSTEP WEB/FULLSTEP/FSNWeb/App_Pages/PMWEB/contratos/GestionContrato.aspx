﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GestionContrato.aspx.vb" Inherits="Fullstep.FSNWeb.GestionContrato" EnableViewState="False" %>

<%@ Register TagPrefix="ignav" Namespace="Infragistics.WebUI.UltraWebNavigator" Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Register TagPrefix="igtab" Namespace="Infragistics.WebUI.UltraWebTab" Assembly="Infragistics.WebUI.UltraWebTab.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head runat="server">
    <title></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
</head>
<!--JIM: colocamos esta porción de código antes de la etiqueta de menú. menu.js realiza override de window.open, y si la colocamos
        después no se ejecuta este código -->
<%If Not String.IsNullOrEmpty(Request("ConfiguracionGS")) Then%>
<script type="text/javascript">
    window.open_ = window.open;
    window.open = function (url, name, props) {
        if (url.toLowerCase().search("sessionid") > 0)
            return window.open_(url, name, props);
        else
            return window.open_(url + '&SessionId=<%=Session("sSession")%>', name, props);
		    }
</script>
<%End If%>

<script type="text/javascript">
    wProgreso = null;

    //Descripcion:Muestra el contenido peligroso que ha efectuado un error al guardar.
    //Parametros entrada: 
    //contenido: Contenido del error
    //Llamada desde: errores.aspx.vb
    //Tiempo ejecucion:=0seg.
    function ErrorValidacion(contenido) {
        alert(arrTextosML[3].replace("$$$", contenido));
        OcultarEspera()
    }
    /*Descripcion:Funcion que oculta o muestra el panel con la informacion de las alertas
                  del contrato.
    Llamada desde:=Click imagenes.
    Tiempo ejecucion:=0seg.*/
    function OcultarAlertas() {
        if (document.getElementById("pnlAlertasTabla").style.display == "block") {
            document.getElementById("pnlAlertasTabla").style.display = "none";
            document.getElementById("imgCollapseAlertas").style.display = "none";
            document.getElementById("imgExpandAlertas").style.display = "block";
        } else {
            document.getElementById("pnlAlertasTabla").style.display = "block";
            document.getElementById("imgCollapseAlertas").style.display = "block";
            document.getElementById("imgExpandAlertas").style.display = "none";
        }
    }
    /*Descripcion: Evalua y ejecuta la instruccion que se le pasa commo parametro
      parametro:
        s--> Instruccion a evaluar
      Tiempo ejecucion:0,1seg.*/
    function localEval(s) {
        eval(s)
    }
    /*Descripcion: Le pone un ancho al desglose
      Llamada desde:carga de la pagina
      Tiempo ejecucion:0,1seg.*/
    function resize() {
        for (i = 0; i < arrDesgloses.length; i++) {
            sDiv = arrDesgloses[i].replace("tblDesglose", "divDesglose")
            if (document.getElementById(sDiv))
                document.getElementById(sDiv).style.width = parseFloat(document.body.offsetWidth) - 95 + 'px';
        }
    }
    /*Descripcion: Pagina para abrir el rpt
      Parametros entrada:
        NombreArchivo:
        Instancia:
      Llamada desde:carga de la pagina
      Tiempo ejecucion:0,1seg.*/
    function LanzarListado(NombreArchivo, Instancia) {
        var newWindow = window.open("../listados/ObtenerInforme.aspx?FileReport=" + NombreArchivo + "&Instancia=" + Instancia, "_blank", "width=800,height=600,status=no,resizable=yes,top=100,left=100, ScrollBars=yes");
        return false;
    }
    /*Descripcion: Muestra los divs que indican que se esta realizando un proceso.
      Llamada desde:=Guardar() // Trasladar() 
      Tiempo ejecucion:0,1seg.*/
    function MostrarEspera() {
        wProgreso = true;

        $("[id*='lnkBoton']").attr('disabled', 'disabled');

        document.getElementById("lblCamposObligatorios").style.display = "none"
        document.getElementById("divForm2").style.display = 'none';
        document.getElementById("divForm3").style.display = 'none';
        document.getElementById("igtabuwtGrupos").style.display = 'none';
        
        i = 0;
        bSalir = false;
        while (bSalir == false) {
            if (document.getElementById("uwtGrupos_div" + i)) {
                document.getElementById("uwtGrupos_div" + i).style.visibility = 'hidden';
                i = i + 1;
            }
            else {
                bSalir = true;
            }
        }
        document.getElementById("lblProgreso").value = document.forms["frmDetalle"].elements["cadenaespera"].value;
        document.getElementById("divProgreso").style.display = 'inline';
    }

    //Estaba habilitando los botones antes de ocultar el progreso. Parecia q te dejaba dar a varios botones mientras estaba en progreso.
    function DarTiempoAOcultarProgreso() {
        $("[id*='lnkBoton']").removeAttr('disabled');
    }

    /*Descripcion: Oculta los divs que indican que se esta realizando un proceso y Muestra los grupos.
    Llamada desde:=HabilitarBotones // Finalizar()
    Tiempo ejecucion:0,1seg.*/
    function OcultarEspera() {
        wProgreso = null;

        document.getElementById("divProgreso").style.display = 'none';
        document.getElementById("divForm2").style.display = 'inline';
        document.getElementById("divForm3").style.display = 'inline';
        document.getElementById("igtabuwtGrupos").style.display = '';

        setTimeout('DarTiempoAOcultarProgreso()', 250);

        if (arrObligatorios.length > 0)
            if (document.getElementById("lblCamposObligatorios"))
                document.getElementById("lblCamposObligatorios").style.display = ""

        i = 0;
        bSalir = false;
        while (bSalir == false) {
            if (document.getElementById("uwtGrupos_div" + i)) {
                document.getElementById("uwtGrupos_div" + i).style.visibility = 'visible';
                i = i + 1;
            }
            else {
                bSalir = true;
            }
        }
        return
    }
    /*  Descripcion:Monta el formulario para ser enviado a guardarInstancia.aspx
    Parametros entrada:
    id:= ID de la accion
    bloque:= ID del bloque
    guarda:= Si va a ser almacenada la informacion o no
    Llamada desde:=EjecutarAccion2
    Tiempo ejecucion:=1,2seg.*/
    function MontarSubmitAccion(id, bloque, comp_obl, guarda) {
        MontarFormularioSubmit(guarda, true, false, false)

        var frmSubmitElements = document.forms["frmSubmit"].elements;
        frmSubmitElements["GEN_AccionRol"].value = id;
        frmSubmitElements["GEN_Bloque"].value = bloque;
        frmSubmitElements["GEN_Rol"].value = oRol_Id;
        frmSubmitElements["PantallaMaper"].value = false;
        frmSubmitElements["DeDonde"].value = 'Detalle';
        frmSubmitElements["ID_CONTRATO"].value = document.forms["frmDetalle"].elements["hid_IdContrato"].value;
        frmSubmitElements["COD_CONTRATO"].value = document.forms["frmDetalle"].elements["hid_CodContrato"].value;
        //Añadimos una variable al formulario que nos indica que va a tener la configuracion de GS
        sVariableConfiguracionGS = "<INPUT type=hidden name=ConfiguracionGS>\n";
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sVariableConfiguracionGS);
        frmSubmitElements["ConfiguracionGS"].value = "<%=request("ConfiguracionGS") %>";

		oFrm = MontarFormularioCalculados();
		sInner = oFrm.innerHTML;
		oFrm.innerHTML = "";
		document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner);

		if (document.forms["frmDetalle"].elements["VinculacionesIdGuardar"].value == id) {
			if (document.forms["frmDetalle"].elements["VinculacionesPrimerGuardar"].value != "0") {
			    for (var indice in htControlFilasVinc) {
			        if (htControlFilas[indice] == "0") {
			            htControlFilasVinc[indice] = "0";
			        }
			    }
			}
		}
		document.forms["frmSubmit"].submit();

		if (document.forms["frmDetalle"].elements["VinculacionesIdGuardar"].value == id) {
			var i;
			for (var iddesglose in htControlArrVinc) {
			    i = 0;

			    if (document.forms["frmDetalle"].elements["VinculacionesPrimerGuardar"].value == "0") {
			        for (var indice in htControlFilasVinc) {

			            campo = indice.substring(0, indice.indexOf("_"))

			            if (campo == iddesglose) {
			                if (htControlFilas[indice] == "0") {
			                    htControlFilasVinc[indice] = "0";
			                    i++;
			                }
			                else {
			                    htControlFilasVinc[indice] = String(parseInt(htControlFilasVinc[indice]) - i);
			                }
			            }
			        }
			    }
			    else {
			        for (var indice in htControlFilasVinc) {
			            campo = indice.substring(0, indice.indexOf("_"))

			            if (campo == iddesglose) {
			                if (htControlFilasVinc[indice] != "0") {
			                    i++;
			                    htControlFilasVinc[indice] = String(i);
			                }
			            }
			        }
			    }
			}
			document.forms["frmDetalle"].elements["VinculacionesPrimerGuardar"].value = 1;
		}
		return false;
    }
    //Descripción: Cuando se pulsa un botón para realizar una acción, se llama a esta función
    //Paramétros: id: id de la acción
    // bloque: id del bloque
    // comp_olb: si hay que comprobar los campos de obligatorios o no
    // guarda: si hay que guardar o versión o no
    // rechazo: indica si la accion implica un rechazo, ya sea temporal o definitivo
    function EjecutarAccion(id, bloque, comp_obl, guarda, bloq, rechazo) {
        setTimeout("EjecutarAccion2(" + id + "," + bloque + "," + comp_obl + "," + guarda + "," + rechazo + ")", 100);
    }
    //Descripción: Comprueba los campos obligatorios, si hay participantes o no, y llama a montarformulario
    //Paramétros: id: id de la acción
    // bloque: id del bloque
    // comp_olb: si hay que comprobar los campos de obligatorios o no
    // guarda: si hay que guardar o versión o no
    // rechazo: indica si la accion implica un rechazo, ya sea temporal o definitivo
    // LLamada desde: EjecutarAccion
    function EjecutarAccion2(id, bloque, comp_obl, guarda, bloq, rechazo) {
    	var respOblig;
        if (bloq == 2) {
            alert(arrTextosML[3])
            return false
        }
        if (comp_obl == true || comp_obl == "true") {
            bMensajePorMostrar = document.getElementById("bMensajePorMostrar")
            if (bMensajePorMostrar)
                if (bMensajePorMostrar.value == "1") {
                    bMensajePorMostrar.value = "0";
                    return false;
                }

            respOblig = comprobarObligatorios()
            switch (respOblig) {
                case "":  //no falta ningun campo obligatorio
                    break;
                case "filas0": //no se han introducido filas en un desglose obligatorio
                	alert(arrTextosML[0]);
                	return false;
                    break;
                default: //falta algun campo obligatorio
                	alert(arrTextosML[0] + '\n' + respOblig);
                	return false;
                    break;
            }
        }

        if (rechazo == false) {
        	respOblig = comprobarParticipantes();
        	if (respOblig !== '') {
        		alert(arrTextosML[0] + '\n' + respOblig);
        		return false;
        	};
        };

        if (document.forms["frmDetalle"].elements["PantallaVinculaciones"].value == "True") {
            if (Validar_Cantidades_Vinculadas(0, document.getElementById("Instancia").value) == false) {
                return false
            }
        }

        if (wProgreso == null) {
            wProgreso = true;
            MostrarEspera();
        }

        MontarSubmitAccion(id, bloque, comp_obl, guarda)
        return false;

    }
    /*Descripcion:=Monta el formulario para el posterior guardado. (guardarInstancia.aspx)
        Llamada desde:Guardar()
        Tiempo ejecucion:1,5seg.*/
    function MontarSubmitGuardar() {
        MontarFormularioSubmit(true, true, false, false)

        var frmDetalleElements = document.forms["frmDetalle"].elements;
        var frmSubmitElements = document.forms["frmSubmit"].elements;
        frmSubmitElements["GEN_AccionRol"].value = frmDetalleElements["Rol"].value;
        frmSubmitElements["GEN_Bloque"].value = frmDetalleElements["Bloque"].value;
        frmSubmitElements["GEN_Rol"].value = oRol_Id;
        frmSubmitElements["GEN_Enviar"].value = 0;
        frmSubmitElements["GEN_Accion"].value = "guardarcontrato";
        frmSubmitElements["PantallaMaper"].value = false;
        frmSubmitElements["DeDonde"].value = 'Detalle';
        frmSubmitElements["ID_CONTRATO"].value = frmDetalleElements["hid_IdContrato"].value;
        frmSubmitElements["COD_CONTRATO"].value = frmDetalleElements["hid_CodContrato"].value;
        //Añadimos una variable al formulario que nos indica que va a tener la configuracion de GS
        sVariableConfiguracionGS = "<INPUT type=hidden name=ConfiguracionGS>\n";
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sVariableConfiguracionGS);
        frmSubmitElements["ConfiguracionGS"].value = "<%=request("ConfiguracionGS") %>";

		oFrm = MontarFormularioCalculados();
		sInner = oFrm.innerHTML;
		oFrm.innerHTML = "";
		document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner);
		document.forms["frmSubmit"].submit();
    }
    /*Descripcion:=Monta el formulario para el posterior guardado. (guardarInstancia.aspx)
	Llamada desde:Guardar()
	Tiempo ejecucion:1,5seg.*/
    function Guardar() {
        if (document.forms["frmDetalle"].elements["PantallaVinculaciones"].value == "True")
            if (Validar_Cantidades_Vinculadas(0, document.getElementById("Instancia").value) == false)
                return false;

        if (wProgreso == null) {
            wProgreso = true;
            MostrarEspera();
        }

        setTimeout("MontarSubmitGuardar()", 100);
    }
    /*Descripcion:=Monta el formulario para realizar el calculo de los campos.
		Llamada desde:Opcion de menu "Calcular"
		Tiempo ejecucion:1,5seg.*/
    function CalcularCamposCalculados() {
        oFrm = MontarFormularioCalculados();
        oFrm.submit();
        return false;
    }
    //''' <summary>
    //''' funcion que inicializa los tabs
    //''' </summary>
    //''' <remarks>Llamada desde:=funcion que se ejecuta al cargar la pagina; Tiempo máximo:0,1</remarks>
    function inicializar() {
        resize();
    }
    //''' <summary>
    //''' funcion que inicializa los tabs
    //''' </summary>
    //''' <remarks>Llamada desde:=funcion que se ejecuta al descargar la pagina; Tiempo máximo:0,1</remarks>	
    function finalizar() {
        if (wProgreso != null)
            OcultarEspera();
        wProgreso = null;
    }
    /*Descripcion:=Oculta los divs de espera
	Llamada desde:GuardarInstancia // Una vez que se carga la pagina
	Tiempo ejecucion:0,1seg.*/
    function HabilitarBotones() {
        OcultarEspera();
    }
    /*  Descripcion:Monta el formulario para que la instancia sea trasladada.
		Llamada desde:opcion del menu "Trasladar"
		Tiempo Ejecucion:(Depende de los campos de la instancia) */
    function Trasladar() {
        if (wProgreso == null) {
            wProgreso = true;
            MostrarEspera();
        }

        MontarSubmitTrasladar();
        return false;
    }
    /*  Descripcion:Monta el formulario para que la instancia sea trasladada.
		Llamada desde:Trasladar()
		Tiempo Ejecucion:(Depende de los campos de la instancia)*/
    function MontarSubmitTrasladar() {
        MontarFormularioSubmit(true, true, false, false)

        var frmSubmitElements = document.forms["frmSubmit"].elements;
        frmSubmitElements["GEN_AccionRol"].value = 0
        frmSubmitElements["GEN_Bloque"].value = document.forms["frmDetalle"].elements["Bloque"].value;
        frmSubmitElements["ID_CONTRATO"].value = document.forms["frmDetalle"].elements["hid_IdContrato"].value;
        frmSubmitElements["COD_CONTRATO"].value = document.forms["frmDetalle"].elements["hid_CodContrato"].value;
        frmSubmitElements["GEN_Rol"].value = oRol_Id;
        frmSubmitElements["GEN_Enviar"].value = 0;
        frmSubmitElements["GEN_Accion"].value = "trasladarcontrato";
        frmSubmitElements["PantallaMaper"].value = false;
        frmSubmitElements["DeDonde"].value = 'Gestion';
        //Añadimos una variable al formulario que nos indica que va a tener la configuracion de GS
        sVariableConfiguracionGS = "<INPUT type=hidden name=ConfiguracionGS>\n";
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sVariableConfiguracionGS);
        frmSubmitElements["ConfiguracionGS"].value = "<%=request("ConfiguracionGS") %>";

        oFrm = MontarFormularioCalculados();
        sInner = oFrm.innerHTML;
        oFrm.innerHTML = "";
        document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner);
        document.forms["frmSubmit"].submit();
    }
    /*Descripcion:Despues del recalculo de los campos calculados en la pagina recalcularImportes.aspx.
	se devuelve el valor
	Parametros entrada:=
	importeConFormato: Importe con el formato del usuario
	Importe: Importe (Numerico)
	Llamada desde:=Click Calcular.
	Tiempo ejecucion:=0seg.*/
    function ponerCalculados(importeConFormato, importe) {
        var sCodMoneda = ''
        lblImporteV = document.getElementById('<%=lblImporte.clientID %>')
		CodMoneda = document.getElementById('<%=hid_CodMoneda.clientID %>')
	    if (CodMoneda) {
	        sCodMoneda = CodMoneda.value;
	    }
	    if (lblImporteV) {
	        lblImporteV.innerHTML = importeConFormato + " " + sCodMoneda;
	    }
	}
	//''' <summary>
	//''' Vuelve a la pagina anterior. O cierra la pagina si se trata de una llamada GS
	//''' </summary>
	//    Parametros entrada: iDesde  (1) = VisorSolicitudes
	//''' <remarks>Llamada desde:Al pinchar en volver; Tiempo máximo:0</remarks>
	function Volver(iDesde) {
	    if (iDesde == 1)
	        window.open("<%=ConfigurationManager.AppSettings("rutaPM2008") %>tareas/VisorSolicitudes.aspx", "_top")
		else {
	        if (iDesde == 2)
                if ("<%=Session("VolverdetalleContrato")%>" == "")
		            window.open("<%=ConfigurationManager.AppSettings("rutaPM2008") %>contratos/VisorContratos.aspx", "_top")
                else
                    window.open("<%=Session("VolverdetalleContrato")%>", "_top")
			else
			    window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>Inicio.aspx", "_self")
        }
        return false;
    }
    /*Descripcion:=Llama a la pagina para exportacion de datos
	Llamada desde:Option de menu "Impr./Exp."
	Tiempo ejecucion:0seg.*/
    function cmdImpExp_onclick() {
        var newWindow = window.open('../seguimiento/impexp_sel.aspx?Instancia=' + document.getElementById("Instancia").value + '&Contrato=' + document.getElementById("hid_IdContrato").value + '&Observadores=' + document.getElementById("Observadores").value + '&TipoImpExp=1', '_new', 'fullscreen=no,height=115,width=315,location=no,menubar=no,resizable=no,scrollbars=no,status=yes,titlebar=yes,toolbar=no,left=200,top=200');
        return false;
    }
    //-->
</script>
<script language="javascript" type="text/javascript">
    var xmlHttp;
    /*''' <summary>
	''' Crear el objeto para llamar con ajax a ComprobarEnProceso
	''' </summary>
	''' <remarks>Llamada desde: javascript ; Tiempo máximo: 0</remarks>*/
    function CreateXmlHttp() {
        // Probamos con IE
        try {
            // Funcionará para JavaScript 5.0
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (oc) {
                xmlHttp = null;
            }
        }

        // Si no se trataba de un IE, probamos con esto
        if (!xmlHttp && typeof XMLHttpRequest != "undefined") {
            xmlHttp = new XMLHttpRequest();
        }

        return xmlHttp;
    }
    //Creamos el objeto xmlHttpRequest
    CreateXmlHttp();

    /*''' <summary>
	''' Hacer una validación a medida de cantidades antes de añadir lineas vinculadas
	''' </summary>
	''' <param name="sRoot">En q desglose, html, se van añadir lineas</param>
	''' <param name="IdCampo">En q desglose, form_campo.id, se van añadir lineas</param>        
	''' <param name="Row">Fila a añadir</param>
	''' <remarks>Llamada desde: PMWeb2008/BuscadorSolicitudes.aspx.vb; Tiempo máximo:0,1</remarks>*/
    function VincularCopiarFilaVacia(sRoot, IdCampo, Row) {
        CreateXmlHttp();
        if (xmlHttp) {
            if (Row.getCellFromKey) {
                var params = "Instancia=" + Row.getCellFromKey("INSTANCIAORIGEN").getValue() + "&Desglose=" + Row.getCellFromKey("DESGLOSEORIGEN").getValue() + "&Linea=" + Row.getCellFromKey("LINEAORIGEN").getValue();
            } else {
                var params = "Instancia=" + Row.get_cellByColumnKey("INSTANCIAORIGEN").get_value() + "&Desglose=" + Row.get_cellByColumnKey("DESGLOSEORIGEN").get_value() + "&Linea=" + Row.get_cellByColumnKey("LINEAORIGEN").get_value();
            }

            xmlHttp.open("POST", "../_common/controlarLineaVinculada.aspx", false);
            xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            xmlHttp.send(params);

            //Tras q se ejecute sincronamente controlarLineaVinculada.aspx controlamos sus resultados y obramos en 
            //consecuencia.                            
            var retorno;
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                retorno = xmlHttp.responseText;
                if (retorno == 'OK') { //pasa la comprobación
                    copiarFilaVacia(sRoot, IdCampo, 0, Row)
                }
                else {
                    var newWindow = window.open("../_common/NoPasoControlarLineaVinculada.aspx?" + params, "_blank", "width=350,height=200,status=yes,resizable=no,top=200,left=300");
                }
            }
        }
    }
    /*''' <summary>
	''' Añadir instancia vinculadas
	''' </summary>
	''' <param name="sRoot">En q desglose, html, se van añadir lineas</param>
	''' <param name="IdCampo">En q desglose, form_campo.id, se van añadir lineas</param>        
	''' <param name="sId">Id de la instancia vinculada</param>
	''' <param name="sDen">Descrip de la instancia vinculada</param> 
	''' <remarks>Llamada desde: PMWeb2008/BuscadorSolicitudes.aspx.vb; Tiempo máximo:0,1</remarks>*/
    function SeleccionarSolicitud(sRoot, IdCampo, sId, sDen) {
        copiarFilaVacia(sRoot, IdCampo, 0, null, sId, sDen);
    }
    /*''' <summary>
	''' Mueve la linea indicada de la instancia actual a la instancia indicada
	''' </summary>
	''' <param name="sRoot">nombre entry del desglose</param>	    
	''' <param name="IdCampo">De q desglose se va a mover la linea</param>        
	''' <param name="sId">Id de la instancia a la q mueves</param>
	''' <param name="index">fila q mueves</param>
	''' <param name="ObjCelda">tabla html donde esta el bt mover/copiar/elim.</param>        
	''' <param name="Celda">Celda, html, donde esta el bt mover/copiar/elim. A traves de él se saca la fila y tabla html</param>
	''' <param name="Frame">Frame donde esta el desglose a borrar</param>          
	''' <remarks>Llamada desde: PMWeb2008/BuscadorSolicitudes.aspx.vb; Tiempo máximo:0,1</remarks>*/
    function VincularMoverAInstancia(sRoot, IdCampo, sId, Index, ObjCelda, Celda, Frame) {
        CreateXmlHttp();
        if (xmlHttp) {
            IndexCtrl = Index
            if (document.forms["frmDetalle"].elements["VinculacionesPrimerGuardar"].value == 1)
                IndexCtrl = DameLineaMoverAInstancia(IdCampo, Index, 0)

            var params = "Mover=1&Instancia=" + document.getElementById("Instancia").value + "&Desglose=" + IdCampo + "&Linea=" + IndexCtrl;

            xmlHttp.open("POST", "../_common/controlarLineaVinculada.aspx", false);
            xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            xmlHttp.send(params);

            //Tras q se ejecute sincronamente controlarLineaVinculada.aspx controlamos sus resultados y obramos en 
            //consecuencia.                            
            var retorno;
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                retorno = xmlHttp.responseText;
                if (retorno == 'OK') { //pasa la comprobación	                    
                    MoverAInstancia(IdCampo, sId, Index, 0);

                    var p = window.parent
                    deleteRow(ObjCelda, sRoot, Index, IdCampo)
                    var oFrame = document.getElementById(Frame)
                    oFrame.parentNode.removeChild(oFrame);
                }
                else
                    if (retorno == 'PANT') {//existen lineas vinculadas pedir confirmacion
                        var newWindow = window.open('../_common/controlarLineaVinculadaMover.aspx?IdCampo=' + IdCampo + '&sId=' + sId + '&Index=' + Index + '&Celda=' + Celda + '&sRoot=' + sRoot + '&Frame=' + Frame + '&PopUp=0', "_blank", "width=350,height=105,status=yes,resizable=no,top=200,left=300");
                    }
            }
        }
    }
    function mostrarMenuListados(event) {
        var p = $get("lnkBotonListados").getBoundingClientRect();
        igmenu_showMenu('uwPopUpListados', event, p.left, p.bottom);
        return false;
    }
    function mostrarMenuAcciones(event, i) {
        var p = $get("lnkBotonAccion" + i.toString()).getBoundingClientRect();
        igmenu_showMenu('uwPopUpAcciones', event, p.left, p.bottom);
        return false;
    }
</script>
<body runat="server" id="mi_body" onresize="resize()">
    <form id="frmDetalle" method="post" runat="server">
        <uc1:menu ID="Menu1" runat="server" OpcionMenu="Contratos" OpcionSubMenu="Seguimiento"></uc1:menu>

        <fsn:FSNPanelInfo ID="FSNPanelDatosProveedor" runat="server" ServicePath="~/App_Pages/PMWEB/ConsultasPMWEB.asmx" ServiceMethod="Obtener_DatosProveedor" TipoDetalle="0"></fsn:FSNPanelInfo>
        <fsn:FSNPanelInfo ID="FSNPanelDatosPeticionario" runat="server" ServicePath="~/App_Pages/PMWEB/ConsultasPMWEB.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="0"></fsn:FSNPanelInfo>
        <fsn:FSNPanelInfo ID="FSNPanelDatosEmpresa" runat="server" ServicePath="~/App_Pages/PMWEB/ConsultasPMWEB.asmx" ServiceMethod="Obtener_DatosEmpresa" TipoDetalle="0"></fsn:FSNPanelInfo>

        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <CompositeScript>
                <Scripts>
                    <asp:ScriptReference Name="ExtenderBase.BaseScripts.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Common.Common.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Compat.Timer.Timer.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Animation.Animations.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="Animation.AnimationBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="PopupExtender.PopupBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="AutoComplete.AutoCompleteBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Path="../alta/js/AdjacentHTML.js" />
                    <asp:ScriptReference Path="../alta/js/jsAlta.js" />
                </Scripts>
            </CompositeScript>
        </asp:ScriptManager>

        <iframe id="iframeWSServer" style="z-index: 102; left: 8px; visibility: hidden; position: absolute; top: 208px"
            name="iframeWSServer" src="../blank.htm"></iframe>
        <input id="Rol" type="hidden" name="Rol" runat="server" />
        <input id="Bloque" type="hidden" name="Bloque" runat="server" />
        <input id="Instancia" type="hidden" size="16" name="Instancia" runat="server" />
        <input id="Version" type="hidden" name="Version" runat="server" />
        <input id="txtIdTipo" type="hidden" name="txtIdTipo" runat="server" />
        <input id="txtPeticionario" type="hidden" name="txtIdTipo" runat="server" />
        <input id="txtEnviar" type="hidden" name="Enviar" runat="server" />
        <input id="Observadores" type="hidden" name="Observadores" runat="server" />
        <input id="bMensajePorMostrar" type="hidden" value="0" name="bMensajePorMostrar" />
        <input id="hid_IdContrato" type="hidden" runat="server" />
        <input id="hid_CodContrato" type="hidden" runat="server" />
        <input id="hid_CodMoneda" type="hidden" runat="server" />
        <input id="hid_Proveedor" type="hidden" runat="server" />

        <div>
            <table id="Table1" style="height: 15%; width: 100%; padding-bottom: 15px;" cellspacing="0" cellpadding="1" border="0">
                <tr>
                    <td colspan="7">
                        <fsn:FSNPageHeader runat="server" ID="FSNPageHeader" UrlImagenCabecera="~/App_Pages/PMWEB/contratos/images/calendar-color.png"></fsn:FSNPageHeader>
                    </td>
                </tr>
            </table>
            <div style="padding-left: 15px; padding-bottom: 15px">
                <asp:Panel ID="pnlContrato" runat="server" BackColor="#f5f5f5" Font-Names="Arial" Width="95%">
                    <table id="Table2" style="height: 15%; width: 100%; padding-bottom: 15px; padding-left: 5px" cellspacing="0" cellpadding="1" border="0">
                        <tr>
                            <td style="padding-top: 5px; padding-bottom: 5px;" class="fondoCabecera">
                                <table id="Table3" style="width: 100%; table-layout: fixed; padding-left: 10px" border="0">
                                    <tr>
                                        <td style="width: 50%">
                                            <asp:Label ID="lblIDInstanciayEstado" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                        </td>

                                        <td style="width: 50%" nowrap="nowrap">
                                            <asp:Label ID="lblProveedor" runat="server" CssClass="label" Text="Proveedor" Font-Bold="true"></asp:Label>
                                            <asp:Image ID="imgInfProv" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="3" nowrap="nowrap">
                                            <table style="width: 100%" border="0">
                                                <tr>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="lblLitFechaInicio" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px">Fecha de inicio</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblFechaInicio" runat="server" CssClass="label" Text="FechaInicio"></asp:Label>
                                                    </td>

                                                    <td>
                                                        <asp:Label ID="lblLitEmpresa" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px" Text="Empresa"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblEmpresa" runat="server" CssClass="label" Text="Empresa"></asp:Label>
                                                        <asp:Image ID="imgInfEmpresa" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td nowrap="nowrap" style="width: 120px">
                                                        <asp:Label ID="lblLitFechaExpiracion" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px" Text="Fecha de expiracion"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblFechaFin" runat="server" CssClass="label" Text="FechaFin"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblLitMoneda" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px" Text="Moneda"></asp:Label>
                                                        <asp:Label ID="lblLitImporte" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px" Visible="false"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblMoneda" runat="server" CssClass="label" Text="Moneda"></asp:Label>
                                                        <asp:Label ID="lblImporte" runat="server" CssClass="label" Visible="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td nowrap="nowrap" style="width: 120px">
                                                         <asp:Label ID="lblLitMostrarAlerta" runat="server" Text="Mostrar Alerta" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label>
                                                    </td>
                                                    <td>
                                                         <asp:Label ID="lblMostrarAlerta" runat="server" Text="Mostrar Alerta" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblLitRecordatorioAlerta" runat="server" Text="Mostrar Alerta" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRecordatorioAlerta" runat="server" Text="Mostrar Alerta" CssClass="label"></asp:Label>
                                                    </td>

                                                </tr>
                                            </table>
                                        </td>
                                        <td nowrap="nowrap">
                                            <asp:Label ID="lblLitCreador" runat="server" Text="Creado por:" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label>
                                            <asp:Label ID="lblPeticionario" runat="server" CssClass="label"></asp:Label>
                                            <asp:Image ID="imgInfPeticionario" runat="server" />
                                            <asp:Label ID="lblFechaCreacion" runat="server" Text="(01/01/0000)" CssClass="label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblLitContacto" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px" Text="Contacto"></asp:Label>
                                            <asp:Label ID="lblContacto" runat="server" CssClass="label" Text="Contacto"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>

                                            <asp:Label ID="lblLitNotificados" runat="server" Text="Configurar notificados:" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label>
                                             <asp:Label ID="lblNotificados" runat="server" Text="" CssClass="label"></asp:Label>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblCamposObligatorios" Style="z-index: 102; display: none;" runat="server" CssClass="captionRed">Los campos marcados con (*) son de obligada cumplimentacion</asp:Label></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div style="padding-top: 15px">
                        <asp:HyperLink ID="HyperDetalle" runat="server" Width="100%" CssClass="CaptionLink"></asp:HyperLink>
                    </div>
                </asp:Panel>
            </div>

            <ajx:DropShadowExtender TrackPosition="true" ID="DropShadowExtender1" runat="server" Opacity="0.5" Width="3" TargetControlID="pnlContrato" Rounded="true">
            </ajx:DropShadowExtender>
        </div>
        <div id="divProgreso" runat="server" style="display: inline">
            <table id="tblProgreso" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
                <tr style="height: 50px">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:TextBox ID="lblProgreso" Style="text-align: center" runat="server" Width="100%" CssClass="captionBlue"
                            BorderWidth="0" BorderStyle="None">Su solicitud está siendo tramitada. Espere unos instantes...</asp:TextBox></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:Image ID="imgProgreso" runat="server" src="../_common/images/cursor-espera_grande.gif"></asp:Image></td>
                </tr>
            </table>
        </div>
        <div id="divForm2" style="display: none">
            <table cellspacing="3" cellpadding="3" width="100%" border="0">
                <tr>
                    <td width="100%" colspan="4">
                        <igtab:UltraWebTab ID="uwtGrupos" runat="server" Width="100%" BorderWidth="1px" BorderStyle="Solid"
                            ThreeDEffect="False" DummyTargetUrl=" " FixedLayout="True" CustomRules="padding:10px;" DisplayMode="Scrollable" EnableViewState="False">
                            <DefaultTabStyle Height="24px" CssClass="uwtDefaultTab">
                                <Padding Left="20px" Right="20px"></Padding>
                            </DefaultTabStyle>
                            <RoundedImage NormalImage="ig_tab_blueb2.gif" HoverImage="ig_tab_blueb1.gif" FillStyle="LeftMergedWithCenter"></RoundedImage>
                        </igtab:UltraWebTab>
                        <script>
                            i = 0;
                            bSalir = false;
                            while (bSalir == false) {
                                if (document.getElementById("uwtGrupos_div" + i)) {
                                    document.getElementById("uwtGrupos_div" + i).style.visibility = 'hidden';
                                    i = i + 1;
                                } else {
                                    bSalir = true;
                                }
                            }
                        </script>
                    </td>
                </tr>
            </table>
            <div id="divDropDowns" style="visibility: hidden; position: absolute; top: 300px"></div>
            <div id="divCalculados" style="z-index: 109; left: 128px; visibility: hidden; position: absolute; top: 8px" name="divCalculados"></div>
            <div id="divAcciones" runat="server">
                <ignav:UltraWebMenu ID="uwPopUpAcciones" runat="server" WebMenuTarget="PopupMenu" SubMenuImage="ig_menuTri.gif"
                    ScrollImageTop="ig_menu_scrollup.gif" Cursor="Default" ScrollImageBottomDisabled="ig_menu_scrolldown_disabled.gif" ScrollImageTopDisabled="ig_menu_scrollup_disabled.gif"
                    ScrollImageBottom="ig_menu_scrolldown.gif">
                    <ItemStyle CssClass="ugMenuItem"></ItemStyle>
                    <DisabledStyle ForeColor="LightGray"></DisabledStyle>
                    <HoverItemStyle Cursor="Hand" CssClass="ugMenuItemHover"></HoverItemStyle>
                    <IslandStyle Cursor="Default" BorderWidth="1px" Font-Size="8pt" Font-Names="MS Sans Serif" BorderStyle="Outset"
                        ForeColor="Black" BackColor="LightGray">
                    </IslandStyle>
                    <ExpandEffects ShadowColor="LightGray"></ExpandEffects>
                    <TopSelectedStyle Cursor="Default"></TopSelectedStyle>
                    <SeparatorStyle BackgroundImage="ig_menuSep.gif" CssClass="SeparatorClass" CustomRules="background-repeat:repeat-x; "></SeparatorStyle>
                    <Levels>
                        <ignav:Level Index="0"></ignav:Level>
                    </Levels>
                </ignav:UltraWebMenu>
            </div>
            <div id="divListados" runat="server">
                <ignav:UltraWebMenu ID="uwPopUpListados" Style="z-index: 112; left: 192px; position: absolute; top: 24px"
                    runat="server" WebMenuTarget="PopupMenu" SubMenuImage="ig_menuTri.gif" ScrollImageTop="ig_menu_scrollup.gif" Cursor="Default" ScrollImageBottomDisabled="ig_menu_scrolldown_disabled.gif"
                    ScrollImageTopDisabled="ig_menu_scrollup_disabled.gif" ScrollImageBottom="ig_menu_scrolldown.gif">
                    <ItemStyle CssClass="ugMenuItem"></ItemStyle>
                    <DisabledStyle ForeColor="LightGray"></DisabledStyle>
                    <HoverItemStyle Cursor="Hand" CssClass="ugMenuItemHover"></HoverItemStyle>
                    <IslandStyle Cursor="Default" BorderWidth="1px" Font-Size="8pt" Font-Names="MS Sans Serif" BorderStyle="Outset"
                        ForeColor="Black" BackColor="LightGray">
                    </IslandStyle>
                    <ExpandEffects ShadowColor="LightGray"></ExpandEffects>
                    <TopSelectedStyle Cursor="Default"></TopSelectedStyle>
                    <SeparatorStyle BackgroundImage="ig_menuSep.gif" CssClass="SeparatorClass" CustomRules="background-repeat:repeat-x; "></SeparatorStyle>
                    <Levels>
                        <ignav:Level Index="0"></ignav:Level>
                    </Levels>
                </ignav:UltraWebMenu>
            </div>
            <input id="cadenaespera" type="hidden" name="cadenaespera" runat="server" />
            <input id="BotonCalcular" type="hidden" value="0" name="BotonCalcular" runat="server" />
            <input id="PantallaMaper" type="hidden" name="PantallaMaper" runat="server" />
            <input id="SoloLectura" type="hidden" name="SoloLectura" runat="server" />
            <input id="Contrato" type="hidden" name="Contrato" runat="server" />
            <input id="PantallaVinculaciones" type="hidden" name="PantallaVinculaciones" runat="server" />
            <input id="VinculacionesIdGuardar" type="hidden" name="VinculacionesIdGuardar" runat="server" />
            <input id="VinculacionesPrimerGuardar" type="hidden" name="VinculacionesPrimerGuardar" runat="server" />
        </div>
    </form>

    <div id="divForm3" style="display: none">
        <form id="frmCalculados" name="frmCalculados" action="../alta/recalcularimportes.aspx?desde=contratos"
            method="post" target="fraWSServer">
        </form>
        <form id="frmDesglose" name="frmDesglose" method="post" target="winDesglose">
        </form>
    </div>
    <script type="text/javascript">OcultarEspera();</script>
</body>
</html>