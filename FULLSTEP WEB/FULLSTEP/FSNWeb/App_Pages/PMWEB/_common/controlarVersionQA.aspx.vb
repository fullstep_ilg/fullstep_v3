﻿Public Class controlarVersionQA
    Inherits FSNPage

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
"<script language=""{0}"">{1}</script>"

    ''' <summary>
    ''' Carga de pantalla. Realmente lo q hace es q por ajax sincrono se desea saber si se puede o no 
    ''' grabar lo q esta editando el usuario en DetalleNoConformidad.aspx ó DetalleCertificado.aspx.
    ''' </summary>
    ''' <param name="sender">pantalla</param>
    ''' <param name="e">parametro de sistema</param>     
    ''' <remarks>Llamada desde: DetalleNoConformidad.aspx DetalleCertificado.aspx; Tiempo máximo: 0,1</remarks>
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Expires = -1
        If Request("Certificado") Is Nothing Then
            ComprobacionesNoConform()
        Else
            ComprobacionesCertif()
        End If
    End Sub

    ''' <summary>
    ''' Se desea saber si se puede o no grabar lo q esta editando el usuario en DetalleNoConformidad.aspx
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo máximo:0,1</remarks>
    Private Sub ComprobacionesNoConform()
        Dim oInstancia As FSNServer.Instancia

        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = Request("Instancia")

        oInstancia.ComprobarGrabacionNC()

        If oInstancia.EnProceso = 1 Then
            Responder("1")
        ElseIf oInstancia.Version = Request("Version") Then
            Responder("0")
        Else 'Not (oInstancia.Version = Request("Version"))
            If oInstancia.TipoVersion = 3 Then
                Responder("Version" & oInstancia.Version)
            ElseIf oInstancia.TipoVersion = 2 Then
                Responder("3")
            Else
                Responder("4")
            End If
        End If
    End Sub

    ''' <summary>
    ''' Se desea saber si se puede o no grabar lo q esta editando el usuario en DetalleCertificado.aspx
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo máximo:0,1</remarks>
    Private Sub ComprobacionesCertif()
        Dim oInstancia As FSNServer.Instancia

        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
        oInstancia.ID = Request("Instancia")

        oInstancia.ComprobarGrabacionCertif(Request("Certificado"))

        If oInstancia.EnProceso = 1 Then
            Responder("1")
        ElseIf oInstancia.Version = Request("Version") Then
            If oInstancia.CertifActivo Then
                Responder("0")
            Else
                Responder("2")
            End If
        Else 'Not (oInstancia.Version = Request("Version"))
            If oInstancia.TipoVersion = 3 Then
                Responder("Version" & oInstancia.Version)
            ElseIf oInstancia.TipoVersion = 2 Then
                Responder("3")
            Else
                Responder("4")
            End If
        End If
    End Sub

    ''' <summary>
    ''' Se ha hecho una llamada Ajax sincrona a esta pantalla, esta función devuelve el resultado de la ejecución
    ''' </summary>
    ''' <param name="Respuesta">Respuesta de la ejecución</param>    
    ''' <remarks>Llamada desde: Page_Load; Tiempo máximo: 0</remarks>
    Private Sub Responder(ByVal Respuesta As String)
        Response.Clear()
        Response.ContentType = "text/plain"
        Response.Write(Respuesta)
        Response.End()
    End Sub

End Class