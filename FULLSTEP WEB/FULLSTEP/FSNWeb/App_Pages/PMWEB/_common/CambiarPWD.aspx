﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CambiarPWD.aspx.vb" Inherits="Fullstep.FSNWeb.CambiarPWD" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:ScriptManager ID="ScriptMAnager1" runat="server">
    </asp:ScriptManager>
        <asp:Panel ID="pnlSeguridad" runat="server" BackColor="White" ForeColor="Black">
        <table cellspacing="1" cellpadding="3" style="padding:[2][2][2][2]" border="0" width="100%">
        <tr>
        <td align ="left"><img alt="" runat="server" id="imgChngPwd" src="~/App_Pages/PMWEB/_common/images/ChangePassword.jpg" /></td>
        <td align ="center"><asp:label id="lblTituloCambioPWD" runat="server"  CssClass="captionBlue" Text="DCambio de contraseña"></asp:label></td>
        <td align ="right" valign="top"></td>
        </tr>
    </table>
    <center>
    <table cellspacing="1" cellpadding="3" style="padding:[2][2][2][2]" border="0"> 
    <tr><td><asp:Label ID="lblPWDActual" runat="server" CssClass="parrafo" Text="DContraseña actual"></asp:Label></td>
        <td><asp:TextBox ID="txtPWDActual" runat="server" TextMode="Password"></asp:TextBox></td>
    </tr>  
    <tr><td colspan="2"><hr /></td></tr>  
    <tr><td><asp:Label ID="lblPWDNueva" runat="server" CssClass="parrafo" Text="DNueva contraseña"></asp:Label></td>
        <td><asp:TextBox ID="txtPWDNueva" runat="server" TextMode="Password"></asp:TextBox></td>
    </tr>     
     <tr><td><asp:Label ID="lblPWDNuevaRep" runat="server" CssClass="parrafo" Text="DRepetir contraseña"></asp:Label></td>
        <td><asp:TextBox ID="txtPWDNuevaRep" runat="server" TextMode="Password"></asp:TextBox></td>
    </tr> 
     </table>
     
            <br />
     <table>
    <tr>
        <td colspan="2" align="center"><asp:button id="cmdAceptarPWD" runat="server" CssClass="botonPMWEB" Text="Aceptar"></asp:button></td>
        
    </tr>
      </table>
      </center>
    </asp:Panel> 
    <asp:Panel ID="PnelMsgCambioContr" runat="server" BackColor="White" ForeColor="Black" style="display:none"
     BorderColor="Black" BorderStyle="Solid" BorderWidth="1" Width="300">
     <asp:UpdatePanel ID="UpDatosPanelCambioContr" runat="server" UpdateMode="Conditional">
     <ContentTemplate>
     <asp:Table ID="tblInfoContrOK" runat="server">
     <asp:TableRow>
     <asp:TableCell>
     <asp:Image ID="ImgInfoContrMal" runat="server" ImageAlign="Left" ImageUrl="~/App_Pages/PMWEB/_common/images/alert-large.gif" />
     </asp:TableCell>
     <asp:TableCell>
     <asp:Label ID="LblMsgContr" runat="server" CssClass="parrafo" text="DLa contraseña se ha cambiado correctamente">
     </asp:Label>
     </asp:TableCell>
     </asp:TableRow>
     </asp:table>
     </ContentTemplate>
     </asp:UpdatePanel>
     <asp:Table ID="Table1" runat="server">
     <asp:TableRow> 
     <asp:TableCell Width="175" HorizontalAlign="Right">
     <asp:button id="cmdAceptarContrOK" runat="server" CssClass="botonPMWEB" Text="Aceptar"></asp:button>
     </asp:TableCell>
     <asp:TableCell Width="125"></asp:TableCell>
     </asp:TableRow>
     </asp:Table>
     </asp:Panel>
    <ajx:ModalPopupExtender ID="mpeMsgCambioContr" runat="server"
        DynamicServicePath="" Enabled="True" RepositionMode="RepositionOnWindowResizeAndScroll" PopupDragHandleControlID="PnlOcContr"
        PopupControlID="PnelMsgCambioContr" TargetControlID="BtnOcMsgCambioContr" BackgroundCssClass="modalBackground" DropShadow="True"
        CancelControlID="cmdAceptarContrOK">
    </ajx:ModalPopupExtender>
    <asp:Button ID="Button1" runat="server" style="display:none" />

     <asp:Button ID="BtnOcMsgCambioContr" runat="server" style="display:none" />
     <asp:Panel ID="PnlOcContr" runat="server" style="display:none"></asp:Panel>
    </div>
    </form>
</body>
</html>
