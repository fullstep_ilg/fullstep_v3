Public Class selectorCamposCertif
    Inherits FSNPage

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
"<script language=""{0}"">{1}</script>"

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents uwtCampos As Infragistics.WebUI.UltraWebNavigator.UltraWebTree

    Protected WithEvents TableLineas As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents cmdGuardar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdTemp As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents Titulos As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents TableTitulos As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents Lineas As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents Botones As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    ''' Revisado por: Jbg. Fecha: 02/12/2011
    ''' <summary>
    ''' Carga la pagina
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde: Sistema; Tiempo m�ximo:0,5seg.</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oUser As FSNServer.User
        Dim sIdi As String = Session("FSN_User").Idioma.ToString
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If
        Dim node As Infragistics.WebUI.UltraWebNavigator.Node
        Dim newNode As Infragistics.WebUI.UltraWebNavigator.Node
        Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
        Dim oConfVisibles As FSNServer.ConfCampos
        Dim oUnidadesNeg As FSNServer.UnidadesNeg

        If Page.IsPostBack Then Exit Sub

        Select Case Request("sFormulario")
            Case "mantenimientoMat", "detalleMatGSProv"
                oDict.LoadData(TiposDeDatos.ModulosIdiomas.SelectorCamposMatQA, sIdi)
            Case "certifBusqueda"
                oDict.LoadData(TiposDeDatos.ModulosIdiomas.SelectorCamposCertif, sIdi)
            Case "panelProve"
                oDict.LoadData(TiposDeDatos.ModulosIdiomas.SelectorCamposProveQA, sIdi)
        End Select

        Dim oTextos As DataTable = oDict.Data.Tables(0)

        oUser = Session("FSN_User")

        Me.cmdGuardar.Value = oTextos.Rows(2).Item(1)
        If Request("sFormulario") = "panelProve" Then Me.cmdTemp.Value = oTextos.Rows(21).Item(1)

        oConfVisibles = FSWSServer.Get_Object(GetType(FSNServer.ConfCampos))
        oConfVisibles.DevolverConfiguracionVisible(oUser.Cod, Request("sFormulario"), SelectorReal)

        If Request("sFormulario") = "panelProve" Then
            'TableLineas TableTitulos
            Me.Titulos.Visible = True
            Me.uwtCampos.Visible = False

            If oUser.QAPPAuto Then
                Me.cmdGuardar.Visible = False
            End If

            CargarVariables(oConfVisibles.Data, oTextos)
        Else
            'Carga el �rbol :
            Me.cmdTemp.Visible = False

            Me.Titulos.Visible = False
            Me.Lineas.Style.Item("top") = "1px"

            Dim sClientTexts As String

            If Request("sFormulario") = "certifBusqueda" Then
                sClientTexts = "PosicionarCertif()"
                sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "PosicionarC", sClientTexts)
            Else
                'Me.Lineas.Style.Item("HEIGHT") = "250px"
                sClientTexts = "PosicionarMat()"
                sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "PosicionarM", sClientTexts)
                If Request.Browser("Browser") = "IE" Then Me.uwtCampos.Height = System.Web.UI.WebControls.Unit.Pixel(255)
            End If

            node = uwtCampos.Nodes.Add(oTextos.Rows(1).Item(1), "raiz")  'Carga el nodo ra�z
            Me.uwtCampos.Levels(1).LevelCheckBoxes = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.True

            'Ahora va a�adiendo los campos a mostrar:
            Select Case Request("sFormulario")
                Case "mantenimientoMat", "detalleMatGSProv"
                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(3).Item(1)  'Material de QA
                    newNode.DataKey = "MATERIAL_QA"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("MATERIAL_QA_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(4).Item(1)  'GS1
                    newNode.DataKey = "GMN1"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("GMN1_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(5).Item(1)  'Den.nivel 1
                    newNode.DataKey = "DEN_GMN1"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("DEN_GMN1_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(6).Item(1)  'GS2
                    newNode.DataKey = "GMN2"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("GMN2_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(7).Item(1)  'Den.nivel 2
                    newNode.DataKey = "DEN_GMN2"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("DEN_GMN2_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(8).Item(1)  'GS3
                    newNode.DataKey = "GMN3"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("GMN3_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(9).Item(1)  'Den.nivel 3
                    newNode.DataKey = "DEN_GMN3"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("DEN_GMN3_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(10).Item(1)  'GS4
                    newNode.DataKey = "GMN4"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("GMN4_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(11).Item(1)  'Den.nivel 4
                    newNode.DataKey = "DEN_GMN4"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("DEN_GMN4_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    If Request("sFormulario") = "mantenimientoMat" Then
                        newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                        newNode.Text = oTextos.Rows(12).Item(1)  'Tipos de certificado
                        newNode.DataKey = "CERTIFICADOS"
                        If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                            If oConfVisibles.Data.Tables(0).Rows(0).Item("CERTIFICADOS_VISIBLE") = 1 Then newNode.Checked = True
                        End If
                        node.Nodes.Add(newNode)
                    End If

                Case "certifBusqueda"
                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(3).Item(1)  'Proveedor
                    newNode.DataKey = "PROVE_DEN"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("PROVE_DEN_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(4).Item(1)  'Real
                    newNode.DataKey = "ES_REAL"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("ES_REAL_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(5).Item(1)  'De baja
                    newNode.DataKey = "ES_BAJA"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("ES_BAJA_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(6).Item(1)  'Material de QA
                    newNode.DataKey = "DEN_MATERIAL"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("DEN_MATERIAL_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(8).Item(1)  '�hay certificado?
                    newNode.DataKey = "HAY_CERTIF"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("HAY_CERTIF_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(7).Item(1)  'Tipo de certificado
                    newNode.DataKey = "DEN_TIPO_CERTIFICADO"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("DEN_TIPO_CERTIFICADO_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(9).Item(1)  'Nombre
                    newNode.DataKey = "NOMBRE_CERTIF"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("NOMBRE_CERTIF_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(10).Item(1)  'Publicado
                    newNode.DataKey = "PUBLICADA"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("PUBLICADA_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(11).Item(1)  'Fecha de publicaci�n
                    newNode.DataKey = "FEC_PUBLICACION"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("FEC_PUBLICACION_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(12).Item(1)  'Fecha de despublicaci�n
                    newNode.DataKey = "FEC_DESPUB"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("FEC_DESPUB_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(13).Item(1)  'Fecha de solicitud
                    newNode.DataKey = "FEC_SOLICITUD"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("FEC_SOLICITUD_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(21).Item(1)  'Fecha l�mite para cumplimentar la solicitud
                    newNode.DataKey = "FEC_LIM_CUMPLIM"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("FEC_LIM_CUMPLIM_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(14).Item(1)  'Fecha de respuesta
                    newNode.DataKey = "FEC_RESPUESTA"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("FEC_RESPUESTA_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(15).Item(1)  'Fecha de obtenci�n
                    newNode.DataKey = "FEC_OBTENCION"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("FEC_OBTENCION_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(16).Item(1)  'Fecha de expiraci�n
                    newNode.DataKey = "FEC_EXPIRACION"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("FEC_EXPIRACION_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(17).Item(1)  'Alcance
                    newNode.DataKey = "ALCANCE"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("ALCANCE_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(18).Item(1)  'Entidad certificadora
                    newNode.DataKey = "ENTIDAD"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("ENTIDAD_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(19).Item(1)  'N�mero de certificado
                    newNode.DataKey = "NUM_CERTIF"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("NUM_CERTIF_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)

                    newNode = New Infragistics.WebUI.UltraWebNavigator.Node
                    newNode.Text = oTextos.Rows(20).Item(1)  'Material de GS
                    newNode.DataKey = "MAT_GS"
                    If oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                        If oConfVisibles.Data.Tables(0).Rows(0).Item("MAT_GS_VISIBLE") = 1 Then newNode.Checked = True
                    End If
                    node.Nodes.Add(newNode)
            End Select

            'si no est� guardada la configuraci�n de los campos chequea todos los nodos:
            If Not oConfVisibles.Data.Tables(0).Rows.Count > 0 Then
                For Each newNode In Me.uwtCampos.Nodes(0).Nodes
                    newNode.Checked = True
                Next
            End If

            Me.uwtCampos.DataKeyOnClient = True
            Me.uwtCampos.ExpandAll()
        End If

        oConfVisibles = Nothing
        oTextos = Nothing
        oDict = Nothing
        oUser = Nothing
    End Sub

    Private Function GenerarDataSetConfig(ByVal Request As System.Web.HttpRequest) As DataSet
        Dim oUser As FSNServer.User = Session("FSN_User")
        Dim DS As DataSet
        Dim dtTabla As DataTable
        Dim keysTabla(0) As DataColumn
        Dim FSPMServer As FSNServer.Root = Session("FSN_Server")
        Dim dtNewRow As DataRow
        Dim onode As Infragistics.WebUI.UltraWebNavigator.Node
        Dim i As Integer

        DS = New DataSet

        'Crea la tabla temporal para los campos modificados:
        dtTabla = DS.Tables.Add("TEMP_CONFIGURACION")
        dtTabla.Columns.Add("USU", System.Type.GetType("System.String"))

        For i = 0 To Me.uwtCampos.Nodes(0).Nodes.Count - 1
            dtTabla.Columns.Add(Me.uwtCampos.Nodes(0).Nodes(i).DataKey & "_VISIBLE", System.Type.GetType("System.Int32"))
            dtTabla.Columns(Me.uwtCampos.Nodes(0).Nodes(i).DataKey & "_VISIBLE").AllowDBNull = False
            dtTabla.Columns(Me.uwtCampos.Nodes(0).Nodes(i).DataKey & "_VISIBLE").DefaultValue = 0
        Next

        keysTabla(0) = dtTabla.Columns("USU")
        dtTabla.PrimaryKey = keysTabla

        'Genera un dataset con una fila:
        dtNewRow = dtTabla.NewRow
        dtNewRow.Item("USU") = oUser.Cod

        dtTabla.Rows.Add(dtNewRow)

        'Cambio el estado a modified:
        dtNewRow.AcceptChanges()

        For i = 0 To Me.uwtCampos.CheckedNodes.Count - 1
            onode = Me.uwtCampos.CheckedNodes.Item(i)
            dtNewRow.Item(onode.DataKey & "_VISIBLE") = 1
        Next

        Return DS

    End Function

    Private Function GenerarDataSetConfigPanelProve(ByVal Request As System.Web.HttpRequest, ByVal oDs As DataSet, ByVal Temporal As Boolean) As DataSet
        Dim oUser As FSNServer.User = Session("FSN_User")
        Dim DS As DataSet
        Dim dtTablaUPD As DataTable
        Dim dtTablaINS As DataTable
        Dim keysTabla(2) As DataColumn
        Dim FSPMServer As FSNServer.Root = Session("FSN_Server")
        Dim dtNewRow As DataRow
        Dim sRequestV As String
        Dim sRequestVC As String
        Dim bExiste, bTratar As Boolean
        Dim iVisibleV, iVisibleVC As Integer
        Dim sIdvarcal1, sIdvarcal2, sIdvarcal3, sIdvarcal4, sIdvarcal5 As String

        'oDs tiene todo conf_campos_panel_prove

        DS = New DataSet

        'Crea la tabla temporal para los campos modificados:
        dtTablaUPD = DS.Tables.Add("TEMP_CONFIGURACION_UPDATE")
        dtTablaUPD.Columns.Add("USU", System.Type.GetType("System.String"))
        dtTablaUPD.Columns("USU").AllowDBNull = False
        dtTablaUPD.Columns("USU").DefaultValue = oUser.Cod

        dtTablaUPD.Columns.Add("CAMPO", System.Type.GetType("System.Int32"))
        dtTablaUPD.Columns("CAMPO").AllowDBNull = False

        dtTablaUPD.Columns.Add("NIVEL", System.Type.GetType("System.Int32"))
        dtTablaUPD.Columns("NIVEL").AllowDBNull = False

        dtTablaUPD.Columns.Add("VISIBLE", System.Type.GetType("System.Int32"))
        dtTablaUPD.Columns("VISIBLE").AllowDBNull = False
        dtTablaUPD.Columns("VISIBLE").DefaultValue = 0

        dtTablaUPD.Columns.Add("VISIBLE_CAL", System.Type.GetType("System.Int32"))
        dtTablaUPD.Columns("VISIBLE_CAL").AllowDBNull = False
        dtTablaUPD.Columns("VISIBLE_CAL").DefaultValue = 0

        keysTabla(0) = dtTablaUPD.Columns("USU")
        keysTabla(1) = dtTablaUPD.Columns("CAMPO")
        keysTabla(2) = dtTablaUPD.Columns("NIVEL")

        dtTablaUPD.PrimaryKey = keysTabla

        'Crea la tabla temporal para los campos insertados:
        dtTablaINS = DS.Tables.Add("TEMP_CONFIGURACION_INSERT")
        dtTablaINS.Columns.Add("USU", System.Type.GetType("System.String"))
        dtTablaINS.Columns("USU").AllowDBNull = False
        dtTablaINS.Columns("USU").DefaultValue = oUser.Cod

        dtTablaINS.Columns.Add("CAMPO", System.Type.GetType("System.Int32"))
        dtTablaINS.Columns("CAMPO").AllowDBNull = False

        dtTablaINS.Columns.Add("NIVEL", System.Type.GetType("System.Int32"))
        dtTablaINS.Columns("NIVEL").AllowDBNull = False

        dtTablaINS.Columns.Add("VISIBLE", System.Type.GetType("System.Int32"))
        dtTablaINS.Columns("VISIBLE").AllowDBNull = False
        dtTablaINS.Columns("VISIBLE").DefaultValue = 0

        dtTablaINS.Columns.Add("VISIBLE_CAL", System.Type.GetType("System.Int32"))
        dtTablaINS.Columns("VISIBLE_CAL").AllowDBNull = False
        dtTablaINS.Columns("VISIBLE_CAL").DefaultValue = 0

        keysTabla(0) = dtTablaINS.Columns("USU")
        keysTabla(1) = dtTablaINS.Columns("CAMPO")
        keysTabla(2) = dtTablaINS.Columns("NIVEL")

        dtTablaINS.PrimaryKey = keysTabla

        For i As Integer = 1 To 8
            bExiste = False
            bTratar = False

            sRequestV = "10_" & CStr(i) & "_v"
            iVisibleV = IIf(Request(sRequestV) <> Nothing, 1, 0)

            For Each row As DataRow In oDs.Tables(0).Rows
                'Ver existencia
                If row("ORDEN") = 0 Then 'var predef
                    If row("ID_VAR_CAL1") = i Then
                        If Va_A_Bbdd(Temporal, True, row("VISIBLE_1"), iVisibleV) Then
                            dtNewRow = dtTablaUPD.NewRow
                            bTratar = True
                        Else
                            bTratar = False
                        End If
                        '
                        bExiste = True
                        '
                        Exit For
                    End If
                Else
                    dtNewRow = dtTablaINS.NewRow

                    bTratar = True

                    Va_A_Bbdd(False, False, 1, iVisibleV)

                    Exit For
                End If
            Next
            '
            If bTratar Then
                dtNewRow("CAMPO") = i
                dtNewRow("NIVEL") = 10
                dtNewRow("VISIBLE") = iVisibleV
                dtNewRow("VISIBLE_CAL") = 0
                '
                If bExiste Then
                    dtTablaUPD.Rows.Add(dtNewRow)
                Else
                    dtTablaINS.Rows.Add(dtNewRow)
                End If
            End If
        Next

        sIdvarcal1 = ""
        sIdvarcal2 = ""
        sIdvarcal3 = ""
        sIdvarcal4 = ""
        sIdvarcal5 = ""

        Dim iPos As Integer
        Dim sNomColumna As String

        For Each row As DataRow In oDs.Tables(0).Rows
            Select Case row("ORDEN")
                Case 0
                Case 1 'VC0
                    bExiste = IIf(row("EXISTE1") <> -1, True, False)
                    bTratar = False

                    sRequestV = "0_" & CStr(row("ID_VAR_CAL1")) & "_v"
                    iVisibleV = IIf(Request(sRequestV) <> Nothing, 1, 0)

                    sRequestVC = "0_" & CStr(row("ID_VAR_CAL1")) & "_vc"
                    iVisibleVC = IIf(Request(sRequestVC) <> Nothing, 1, 0)

                    If bExiste Then
                        'If row("VISIBLE_1") <> iVisibleV Then
                        If Va_A_Bbdd(Temporal, True, row("VISIBLE_1"), iVisibleV) Then
                            dtNewRow = dtTablaUPD.NewRow
                            bTratar = True
                        End If

                        'If row("VISIBLE_CAL_1") <> iVisibleVC Then
                        If Va_A_Bbdd(Temporal, True, row("VISIBLE_CAL_1"), iVisibleVC) Then
                            dtNewRow = dtTablaUPD.NewRow
                            bTratar = True
                        End If

                    Else
                        dtNewRow = dtTablaINS.NewRow

                        Va_A_Bbdd(Temporal, False, 0, iVisibleV)
                        Va_A_Bbdd(Temporal, False, 0, iVisibleVC)

                        bTratar = True
                    End If
                    '
                    If bTratar = True Then
                        dtNewRow("CAMPO") = row("ID_VAR_CAL1")
                        dtNewRow("NIVEL") = 0
                        dtNewRow("VISIBLE") = iVisibleV
                        dtNewRow("VISIBLE_CAL") = iVisibleVC
                        '
                        If bExiste Then
                            dtTablaUPD.Rows.Add(dtNewRow)
                        Else
                            dtTablaINS.Rows.Add(dtNewRow)
                        End If
                    End If
                Case 2 'VC12345
                    If (Not (row("ID_VAR_CAL1") Is System.DBNull.Value)) AndAlso (sIdvarcal1 <> CStr(row("ID_VAR_CAL1"))) Then
                        sIdvarcal1 = CStr(row("ID_VAR_CAL1"))
                        '
                        bExiste = IIf(row("EXISTE1") <> -1, True, False)
                        bTratar = False

                        sRequestV = "1_" & sIdvarcal1 & "_v"
                        iVisibleV = IIf(Request(sRequestV) <> Nothing, 1, 0)

                        sRequestVC = "1_" & sIdvarcal1 & "_vc"
                        iVisibleVC = IIf(Request(sRequestVC) <> Nothing, 1, 0)

                        If bExiste Then
                            If Va_A_Bbdd(Temporal, True, row("VISIBLE_1"), iVisibleV) Then
                                dtNewRow = dtTablaUPD.NewRow
                                bTratar = True
                            End If

                            'If row("VISIBLE_CAL_1") <> iVisibleVC Then
                            If Va_A_Bbdd(Temporal, True, row("VISIBLE_CAL_1"), iVisibleVC) Then
                                dtNewRow = dtTablaUPD.NewRow
                                bTratar = True
                            End If

                        Else
                            dtNewRow = dtTablaINS.NewRow

                            Va_A_Bbdd(Temporal, False, 0, iVisibleV)
                            Va_A_Bbdd(Temporal, False, 0, iVisibleVC)

                            bTratar = True
                        End If
                        '
                        If bTratar = True Then
                            dtNewRow("CAMPO") = sIdvarcal1
                            dtNewRow("NIVEL") = 1
                            dtNewRow("VISIBLE") = iVisibleV
                            dtNewRow("VISIBLE_CAL") = iVisibleVC
                            '
                            If bExiste Then
                                dtTablaUPD.Rows.Add(dtNewRow)
                            Else
                                dtTablaINS.Rows.Add(dtNewRow)
                            End If
                        End If
                    End If

                    If (Not (row("ID_VAR_CAL2") Is System.DBNull.Value)) AndAlso (sIdvarcal2 <> CStr(row("ID_VAR_CAL2"))) Then
                        sIdvarcal2 = CStr(row("ID_VAR_CAL2"))
                        '
                        bExiste = IIf(row("EXISTE2") <> -1, True, False)
                        bTratar = False

                        sRequestV = "2_" & sIdvarcal2 & "_v"
                        iVisibleV = IIf(Request(sRequestV) <> Nothing, 1, 0)

                        sRequestVC = "2_" & sIdvarcal2 & "_vc"
                        iVisibleVC = IIf(Request(sRequestVC) <> Nothing, 1, 0)

                        If oUser.QAPPAuto Then
                            If bExiste Then
                                If Va_A_Bbdd(True, True, row("VISIBLE_2"), iVisibleV) Then
                                    bTratar = True
                                End If

                                If Va_A_Bbdd(True, True, row("VISIBLE_CAL_2"), iVisibleVC) Then
                                    bTratar = True
                                End If
                            Else
                                Va_A_Bbdd(True, False, 0, iVisibleV)
                                Va_A_Bbdd(True, False, 0, iVisibleVC)
                            End If

                            ''''bTratar = True
                        Else

                            If bExiste Then

                                If Va_A_Bbdd(Temporal, True, row("VISIBLE_2"), iVisibleV) Then
                                    bTratar = True
                                End If

                                If Va_A_Bbdd(Temporal, True, row("VISIBLE_CAL_2"), iVisibleVC) Then
                                    bTratar = True
                                End If

                            Else

                                Va_A_Bbdd(Temporal, False, 0, iVisibleV)
                                Va_A_Bbdd(Temporal, False, 0, iVisibleVC)

                                bTratar = True
                            End If
                        End If

                        If bTratar = True Then
                            If bExiste Then
                                dtNewRow = dtTablaUPD.NewRow
                            Else
                                dtNewRow = dtTablaINS.NewRow
                            End If

                            dtNewRow("CAMPO") = sIdvarcal2
                            dtNewRow("NIVEL") = 2
                            dtNewRow("VISIBLE") = iVisibleV
                            dtNewRow("VISIBLE_CAL") = iVisibleVC
                            '
                            If bExiste Then
                                dtTablaUPD.Rows.Add(dtNewRow)
                            Else
                                dtTablaINS.Rows.Add(dtNewRow)
                            End If
                        End If
                    End If

                    If (Not (row("ID_VAR_CAL3") Is System.DBNull.Value)) AndAlso (sIdvarcal3 <> CStr(row("ID_VAR_CAL3"))) Then
                        sIdvarcal3 = CStr(row("ID_VAR_CAL3"))
                        '
                        bExiste = IIf(row("EXISTE3") <> -1, True, False)
                        bTratar = False

                        sRequestV = "3_" & sIdvarcal3 & "_v"
                        iVisibleV = IIf(Request(sRequestV) <> Nothing, 1, 0)

                        sRequestVC = "3_" & sIdvarcal3 & "_vc"
                        iVisibleVC = IIf(Request(sRequestVC) <> Nothing, 1, 0)

                        If oUser.QAPPAuto Then
                            If bExiste Then
                                If Va_A_Bbdd(True, True, row("VISIBLE_2"), iVisibleV) Then
                                    bTratar = True
                                End If

                                If Va_A_Bbdd(True, True, row("VISIBLE_CAL_2"), iVisibleVC) Then
                                    bTratar = True
                                End If
                            Else
                                Va_A_Bbdd(True, False, 0, iVisibleV)
                                Va_A_Bbdd(True, False, 0, iVisibleVC)
                            End If

                            ''''bTratar = True
                        Else

                            If bExiste Then
                                'If row("VISIBLE_3") <> iVisibleV Then
                                If Va_A_Bbdd(Temporal, True, row("VISIBLE_3"), iVisibleV) Then
                                    bTratar = True
                                End If

                                'If row("VISIBLE_CAL_3") <> iVisibleVC Then
                                If Va_A_Bbdd(Temporal, True, row("VISIBLE_CAL_3"), iVisibleVC) Then
                                    bTratar = True
                                End If

                            Else

                                Va_A_Bbdd(Temporal, False, 0, iVisibleV)
                                Va_A_Bbdd(Temporal, False, 0, iVisibleVC)

                                bTratar = True
                            End If
                        End If
                        '
                        If bTratar = True Then
                            If bExiste Then
                                dtNewRow = dtTablaUPD.NewRow
                            Else
                                dtNewRow = dtTablaINS.NewRow
                            End If

                            dtNewRow("CAMPO") = sIdvarcal3
                            dtNewRow("NIVEL") = 3
                            dtNewRow("VISIBLE") = iVisibleV
                            dtNewRow("VISIBLE_CAL") = iVisibleVC
                            '
                            If bExiste Then
                                dtTablaUPD.Rows.Add(dtNewRow)
                            Else
                                dtTablaINS.Rows.Add(dtNewRow)
                            End If
                        End If

                    End If

                    If (Not (row("ID_VAR_CAL4") Is System.DBNull.Value)) AndAlso (sIdvarcal4 <> CStr(row("ID_VAR_CAL4"))) Then
                        sIdvarcal4 = CStr(row("ID_VAR_CAL4"))
                        '
                        bExiste = IIf(row("EXISTE4") <> -1, True, False)
                        bTratar = False

                        sRequestV = "4_" & sIdvarcal4 & "_v"
                        iVisibleV = IIf(Request(sRequestV) <> Nothing, 1, 0)

                        sRequestVC = "4_" & sIdvarcal4 & "_vc"
                        iVisibleVC = IIf(Request(sRequestVC) <> Nothing, 1, 0)

                        If oUser.QAPPAuto Then
                            If bExiste Then
                                If Va_A_Bbdd(True, True, row("VISIBLE_2"), iVisibleV) Then
                                    bTratar = True
                                End If

                                If Va_A_Bbdd(True, True, row("VISIBLE_CAL_2"), iVisibleVC) Then
                                    bTratar = True
                                End If
                            Else
                                Va_A_Bbdd(True, False, 0, iVisibleV)
                                Va_A_Bbdd(True, False, 0, iVisibleVC)
                            End If

                            ''''bTratar = True
                        Else

                            If bExiste Then
                                'If row("VISIBLE_4") <> iVisibleV Then 
                                If Va_A_Bbdd(Temporal, True, row("VISIBLE_4"), iVisibleV) Then
                                    bTratar = True
                                End If

                                'If row("VISIBLE_CAL_4") <> iVisibleVC Then
                                If Va_A_Bbdd(Temporal, True, row("VISIBLE_CAL_4"), iVisibleVC) Then
                                    bTratar = True
                                End If

                            Else
                                Va_A_Bbdd(Temporal, False, 0, iVisibleV)
                                Va_A_Bbdd(Temporal, False, 0, iVisibleVC)

                                bTratar = True
                            End If
                        End If
                        '
                        If bTratar = True Then
                            If bExiste Then
                                dtNewRow = dtTablaUPD.NewRow
                            Else
                                dtNewRow = dtTablaINS.NewRow
                            End If

                            dtNewRow("CAMPO") = sIdvarcal4
                            dtNewRow("NIVEL") = 4
                            dtNewRow("VISIBLE") = iVisibleV
                            dtNewRow("VISIBLE_CAL") = iVisibleVC
                            '
                            If bExiste Then
                                dtTablaUPD.Rows.Add(dtNewRow)
                            Else
                                dtTablaINS.Rows.Add(dtNewRow)
                            End If

                        End If
                    End If

                    If (Not (row("ID_VAR_CAL5") Is System.DBNull.Value)) AndAlso (sIdvarcal5 <> CStr(row("ID_VAR_CAL5"))) Then
                        sIdvarcal5 = CStr(row("ID_VAR_CAL5"))
                        '
                        bExiste = IIf(row("EXISTE5") <> -1, True, False)
                        bTratar = False

                        sRequestV = "5_" & sIdvarcal5 & "_v"
                        iVisibleV = IIf(Request(sRequestV) <> Nothing, 1, 0)

                        sRequestVC = "5_" & sIdvarcal5 & "_vc"
                        iVisibleVC = IIf(Request(sRequestVC) <> Nothing, 1, 0)

                        If oUser.QAPPAuto Then
                            If bExiste Then
                                If Va_A_Bbdd(True, True, row("VISIBLE_2"), iVisibleV) Then
                                    bTratar = True
                                End If

                                If Va_A_Bbdd(True, True, row("VISIBLE_CAL_2"), iVisibleVC) Then
                                    bTratar = True
                                End If
                            Else
                                Va_A_Bbdd(True, False, 0, iVisibleV)
                                Va_A_Bbdd(True, False, 0, iVisibleVC)
                            End If

                            ''''bTratar = True
                        Else

                            If bExiste Then
                                'If row("VISIBLE_5") <> iVisibleV Then
                                If Va_A_Bbdd(Temporal, True, row("VISIBLE_5"), iVisibleV) Then
                                    bTratar = True
                                End If

                                'If row("VISIBLE_CAL_5") <> iVisibleVC Then
                                If Va_A_Bbdd(Temporal, True, row("VISIBLE_CAL_5"), iVisibleVC) Then
                                    bTratar = True
                                End If

                            Else

                                Va_A_Bbdd(Temporal, False, 0, iVisibleV)
                                Va_A_Bbdd(Temporal, False, 0, iVisibleVC)

                                bTratar = True
                            End If
                        End If
                        '
                        If bTratar = True Then
                            If bExiste Then
                                dtNewRow = dtTablaUPD.NewRow
                            Else
                                dtNewRow = dtTablaINS.NewRow
                            End If

                            dtNewRow("CAMPO") = sIdvarcal5
                            dtNewRow("NIVEL") = 5
                            dtNewRow("VISIBLE") = iVisibleV
                            dtNewRow("VISIBLE_CAL") = iVisibleVC
                            '
                            If bExiste Then
                                dtTablaUPD.Rows.Add(dtNewRow)
                            Else
                                dtTablaINS.Rows.Add(dtNewRow)
                            End If
                        End If

                    End If
                Case 3 'Negocio
                    bExiste = IIf(row("EXISTE1") <> -1, True, False)
                    bTratar = False

                    sRequestV = "20_" & CStr(row("ID_VAR_CAL1")) & "_v"
                    iVisibleV = IIf(Request(sRequestV) <> Nothing, 1, 0)

                    If bExiste Then
                        'If row("VISIBLE_1") <> iVisibleV Then
                        If Va_A_Bbdd(Temporal, True, row("VISIBLE_1"), iVisibleV) Then
                            dtNewRow = dtTablaUPD.NewRow
                            bTratar = True
                        End If
                    Else
                        dtNewRow = dtTablaINS.NewRow

                        Va_A_Bbdd(Temporal, False, 0, iVisibleV)

                        bTratar = True
                    End If
                    '
                    If bTratar = True Then
                        dtNewRow("CAMPO") = row("ID_VAR_CAL1")
                        dtNewRow("NIVEL") = 20
                        dtNewRow("VISIBLE") = iVisibleV
                        dtNewRow("VISIBLE_CAL") = 0
                        '
                        If bExiste Then
                            dtTablaUPD.Rows.Add(dtNewRow)
                        Else
                            dtTablaINS.Rows.Add(dtNewRow)
                        End If
                    End If
            End Select
        Next

        dtTablaUPD.AcceptChanges()

        Return DS
    End Function

    Private Sub CargarVariables(ByVal oDs As DataSet, ByVal oTextos As DataTable)
        Dim oFilaTit As System.Web.UI.HtmlControls.HtmlTableRow 'Titulo
        Dim oFilaNorm As System.Web.UI.HtmlControls.HtmlTableRow 'Normales
        Dim oFilaVC As System.Web.UI.HtmlControls.HtmlTableRow '012345
        Dim oFilaNego As System.Web.UI.HtmlControls.HtmlTableRow 'Negocio
        Dim oCelda As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oCheck As System.Web.UI.WebControls.CheckBox
        Dim sIdvarcal1, sIdvarcal2, sIdvarcal3, sIdvarcal4, sIdvarcal5 As String
        Dim oUser As FSNServer.User
        Dim bVisible As Boolean
        Dim bVisibleCal As Boolean

        oUser = Session("FSN_User")

        'Titulos
        oFilaTit = New System.Web.UI.HtmlControls.HtmlTableRow

        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
        oCelda.ColSpan = 3
        oCelda.InnerText = oTextos.Rows(1).Item(1) 'Mostrar los siguientes campos
        oCelda.Attributes("class") = "caption"
        oFilaTit.Cells.Add(oCelda)

        Me.TableTitulos.Rows.Add(oFilaTit)

        oFilaTit = New System.Web.UI.HtmlControls.HtmlTableRow

        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
        'oCelda.ColSpan = 1
        oCelda.Style.Item("width") = "15%"
        oCelda.InnerText = oTextos.Rows(16).Item(1) 'Visible
        oCelda.Style.Item("BORDER-BOTTOM") = "black 1px dashed"
        oCelda.Attributes("class") = "parrafo"
        'oCelda.Align = "Center"
        oFilaTit.Cells.Add(oCelda)

        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
        'oCelda.ColSpan = 1
        oCelda.Style.Item("width") = "75%"
        oCelda.InnerText = oTextos.Rows(17).Item(1) 'Campo
        oCelda.Style.Item("BORDER-BOTTOM") = "black 1px dashed"
        oCelda.Attributes("class") = "parrafo"
        oCelda.Align = "Left"
        oFilaTit.Cells.Add(oCelda)

        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
        'oCelda.ColSpan = 1
        oCelda.Style.Item("width") = "10%"
        oCelda.InnerText = oTextos.Rows(18).Item(1) 'Calificacion
        oCelda.Style.Item("BORDER-BOTTOM") = "black 1px dashed"
        oCelda.Attributes("class") = "parrafo"
        oCelda.Align = "Right"
        oFilaTit.Cells.Add(oCelda)

        Me.TableTitulos.Rows.Add(oFilaTit)

        'campos normales
        Dim bEncontrado As Boolean = False
        For i As Integer = 1 To 8
            oFilaNorm = New System.Web.UI.HtmlControls.HtmlTableRow

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCheck = New System.Web.UI.WebControls.CheckBox
            oCheck.ID = "10_" & CStr(i) & "_v"

            oCheck.Checked = False

            bEncontrado = False
            For Each row As DataRow In oDs.Tables(0).Rows
                If row("ORDEN") = 0 Then
                    If row("ID_VAR_CAL1") = i Then
                        oCheck.Checked = IIf(row("VISIBLE_1"), True, False)
                        bEncontrado = True
                        Exit For
                    End If
                Else
                    Exit For
                End If
            Next

            If Not bEncontrado Then oCheck.Checked = True

            oCelda.ColSpan = 1
            oCelda.Align = "Center"
            oCelda.Controls.Add(oCheck)
            oFilaNorm.Cells.Add(oCelda)

            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.ColSpan = 6
            Select Case i
                Case 1 'Proveedor
                    oCelda.InnerText = oTextos.Rows(3).Item(1)
                Case 2 'Denom. Prove
                    oCelda.InnerText = oTextos.Rows(20).Item(1)
                Case 3 'Cif
                    oCelda.InnerText = oTextos.Rows(4).Item(1)
                Case 4 'Material QA
                    oCelda.InnerText = oTextos.Rows(5).Item(1)
                Case 5 'Contacto calidad
                    oCelda.InnerText = oTextos.Rows(8).Item(1)
                Case 6 'Potencial
                    oCelda.InnerText = oTextos.Rows(9).Item(1)
                Case 7 'Baja en calidad
                    oCelda.InnerText = oTextos.Rows(10).Item(1)
                Case 8 'Material de GS
                    oCelda.InnerText = oTextos.Rows(11).Item(1)
            End Select
            oCelda.Attributes("class") = "parrafo"
            oFilaNorm.Cells.Add(oCelda)

            Me.TableLineas.Rows.Add(oFilaNorm)
        Next

        sIdvarcal1 = ""
        sIdvarcal2 = ""
        sIdvarcal3 = ""
        sIdvarcal4 = ""
        sIdvarcal5 = ""

        Dim iCol As Integer
        Dim sColumnas() As String
        If oUser.QAPPAuto Then
            sColumnas = Split(Session("Columnas"), "#")
        End If

        For Each row As DataRow In oDs.Tables(0).Rows
            Select Case row("ORDEN")
                Case 0
                Case 1 'VC0
                    oFilaVC = New System.Web.UI.HtmlControls.HtmlTableRow

                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell

                    oCheck = New System.Web.UI.WebControls.CheckBox
                    oCheck.ID = "0_" & CStr(row("ID_VAR_CAL1")) & "_v"
                    oCheck.Checked = IIf(row("VISIBLE_1"), True, False)

                    oCelda.ColSpan = 1
                    oCelda.Align = "Center"
                    oCelda.Controls.Add(oCheck)

                    oFilaVC.Cells.Add(oCelda)

                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                    oCelda.InnerText = oTextos.Rows(19).Item(1)
                    oCelda.Attributes("class") = "parrafo"
                    oCelda.ColSpan = 5

                    oFilaVC.Cells.Add(oCelda)

                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                    oCheck = New System.Web.UI.WebControls.CheckBox
                    oCheck.ID = "0_" & CStr(row("ID_VAR_CAL1")) & "_vc"
                    oCheck.Checked = IIf(row("VISIBLE_CAL_1"), True, False)

                    oCelda.ColSpan = 1
                    oCelda.Align = "Center"
                    oCelda.Controls.Add(oCheck)
                    oFilaVC.Cells.Add(oCelda)

                    Me.TableLineas.Rows.Add(oFilaVC)

                Case 2 'VC12345

                    If (Not (row("ID_VAR_CAL1") Is System.DBNull.Value)) AndAlso (sIdvarcal1 <> CStr(row("ID_VAR_CAL1"))) Then
                        sIdvarcal1 = CStr(row("ID_VAR_CAL1"))
                        LineaVarCal("1", sIdvarcal1, row("TEXTO_1"), row("VISIBLE_1"), row("VISIBLE_CAL_1"), 5)
                    End If

                    If (Not (row("ID_VAR_CAL2") Is System.DBNull.Value)) AndAlso (sIdvarcal2 <> CStr(row("ID_VAR_CAL2"))) Then
                        sIdvarcal2 = CStr(row("ID_VAR_CAL2"))
                        If oUser.QAPPAuto Then 'Si la carga es autom�tica, miramos que si hemos sacado la variable de calidad
                            bVisible = False
                            bVisibleCal = False
                            For iCol = 0 To UBound(sColumnas) - 1
                                If "PUNT_2_" & sIdvarcal2 = sColumnas(iCol) Then
                                    bVisible = True
                                    Exit For
                                End If
                            Next
                            For iCol = 0 To UBound(sColumnas) - 1
                                If "CAL_2_" & sIdvarcal2 = sColumnas(iCol) Then
                                    bVisibleCal = True
                                    Exit For
                                End If
                            Next
                            LineaVarCal("2", sIdvarcal2, row("TEXTO_2"), bVisible, bVisibleCal, 4)
                        Else
                            LineaVarCal("2", sIdvarcal2, row("TEXTO_2"), row("VISIBLE_2"), row("VISIBLE_CAL_2"), 4)
                        End If
                    End If

                    If (Not (row("ID_VAR_CAL3") Is System.DBNull.Value)) AndAlso (sIdvarcal3 <> CStr(row("ID_VAR_CAL3"))) Then
                        sIdvarcal3 = CStr(row("ID_VAR_CAL3"))
                        If oUser.QAPPAuto Then 'Si la carga es autom�tica, miramos que si hemos sacado la variable de calidad
                            bVisible = False
                            bVisibleCal = False
                            For iCol = 0 To UBound(sColumnas) - 1
                                If "PUNT_3_" & sIdvarcal3 = sColumnas(iCol) Then
                                    bVisible = True
                                    Exit For
                                End If
                            Next
                            For iCol = 0 To UBound(sColumnas) - 1
                                If "CAL_3_" & sIdvarcal3 = sColumnas(iCol) Then
                                    bVisibleCal = True
                                    Exit For
                                End If
                            Next
                            LineaVarCal("3", sIdvarcal3, row("TEXTO_3"), bVisible, bVisibleCal, 3)
                        Else
                            LineaVarCal("3", sIdvarcal3, row("TEXTO_3"), row("VISIBLE_3"), row("VISIBLE_CAL_3"), 3)
                        End If

                    End If

                    If (Not (row("ID_VAR_CAL4") Is System.DBNull.Value)) AndAlso (sIdvarcal4 <> CStr(row("ID_VAR_CAL4"))) Then
                        sIdvarcal4 = CStr(row("ID_VAR_CAL4"))
                        If oUser.QAPPAuto Then 'Si la carga es autom�tica, miramos que si hemos sacado la variable de calidad
                            bVisible = False
                            bVisibleCal = False
                            For iCol = 0 To UBound(sColumnas) - 1
                                If "PUNT_4_" & sIdvarcal4 = sColumnas(iCol) Then
                                    bVisible = True
                                    Exit For
                                End If
                            Next
                            For iCol = 0 To UBound(sColumnas) - 1
                                If "CAL_4_" & sIdvarcal4 = sColumnas(iCol) Then
                                    bVisibleCal = True
                                    Exit For
                                End If
                            Next
                            LineaVarCal("4", sIdvarcal4, row("TEXTO_4"), bVisible, bVisibleCal, 2)
                        Else
                            LineaVarCal("4", sIdvarcal4, row("TEXTO_4"), row("VISIBLE_4"), row("VISIBLE_CAL_4"), 2)
                        End If
                    End If

                    If (Not (row("ID_VAR_CAL5") Is System.DBNull.Value)) AndAlso (sIdvarcal5 <> CStr(row("ID_VAR_CAL5"))) Then
                        sIdvarcal5 = CStr(row("ID_VAR_CAL5"))
                        If oUser.QAPPAuto Then 'Si la carga es autom�tica, miramos que si hemos sacado la variable de calidad
                            bVisible = False
                            bVisibleCal = False
                            For iCol = 0 To UBound(sColumnas) - 1
                                If "PUNT_5_" & sIdvarcal5 = sColumnas(iCol) Then
                                    bVisible = True
                                    Exit For
                                End If
                            Next
                            For iCol = 0 To UBound(sColumnas) - 1
                                If "CAL_5_" & sIdvarcal5 = sColumnas(iCol) Then
                                    bVisibleCal = True
                                    Exit For
                                End If
                            Next
                            LineaVarCal("5", sIdvarcal5, row("TEXTO_5"), bVisible, bVisibleCal, 1)
                        Else
                            LineaVarCal("5", sIdvarcal5, row("TEXTO_5"), row("VISIBLE_5"), row("VISIBLE_CAL_5"), 1)
                        End If
                    End If

                Case 3 'Negocio
                    oFilaNego = New System.Web.UI.HtmlControls.HtmlTableRow

                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell

                    oCheck = New System.Web.UI.WebControls.CheckBox
                    oCheck.ID = "20_" & CStr(row("ID_VAR_CAL1")) & "_v"
                    oCheck.Checked = IIf(row("VISIBLE_1"), True, False)

                    oCelda.ColSpan = 1
                    oCelda.Align = "Center"
                    oCelda.Controls.Add(oCheck)

                    oFilaNego.Cells.Add(oCelda)

                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                    oCelda.InnerText = row("TEXTO_1")
                    oCelda.Attributes("class") = "parrafo"
                    oCelda.ColSpan = 6

                    oFilaNego.Cells.Add(oCelda)

                    Me.TableLineas.Rows.Add(oFilaNego)

            End Select
        Next
    End Sub

    Private Sub LineaVarCal(ByVal Nivel As String, ByVal Id As String, ByVal Texto As String, ByVal Visible As Integer, ByVal VisibleCal As Integer, ByVal Span As Integer)
        Dim oFilaVC As System.Web.UI.HtmlControls.HtmlTableRow '12345
        Dim oCelda As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oCheck As System.Web.UI.WebControls.CheckBox

        oFilaVC = New System.Web.UI.HtmlControls.HtmlTableRow

        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell

        oCheck = New System.Web.UI.WebControls.CheckBox
        oCheck.ID = Nivel & "_" & Id & "_v"
        oCheck.Checked = IIf(Visible, True, False)

        oCelda.ColSpan = 1
        oCelda.Align = "Center"
        oCelda.Controls.Add(oCheck)

        oFilaVC.Cells.Add(oCelda)

        If 5 - Span > 0 Then
            oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
            oCelda.InnerHtml = "&nbsp;"
            oCelda.ColSpan = 5 - Span
            oFilaVC.Cells.Add(oCelda)
        End If

        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
        oCelda.InnerText = Texto
        oCelda.ColSpan = Span
        oFilaVC.Cells.Add(oCelda)

        oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
        oCheck = New System.Web.UI.WebControls.CheckBox
        oCheck.ID = Nivel & "_" & Id & "_vc"
        oCheck.Checked = IIf(VisibleCal, True, False)

        oCelda.ColSpan = 1
        oCelda.Align = "Center"
        oCelda.Controls.Add(oCheck)
        oFilaVC.Cells.Add(oCelda)

        Select Case Nivel
            Case "1"
                oFilaVC.Attributes("class") = "captionBlueSmall"
            Case "2"
                oFilaVC.Attributes("class") = "captionDarkBlueSmall"
            Case "3"
                oFilaVC.Attributes("class") = "captionBlackSmall"
            Case "4"
                oFilaVC.Attributes("class") = "captionLightGraySmall"
            Case "5"
                oFilaVC.Attributes("class") = "captionDarkGraySmall"
        End Select

        Me.TableLineas.Rows.Add(oFilaVC)

    End Sub

    Private Sub cmdTemp_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTemp.ServerClick
        'NO Guardar en BD
        Dim ds As DataSet
        Dim oUser As FSNServer.User
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oConfCampos As FSNServer.ConfCampos
        Dim sNomGrid As String

        Session("SelectorReal") = "NOReal"
        Session("Cargar") = 0

        oUser = Session("FSN_User")

        oConfCampos = FSWSServer.Get_Object(GetType(FSNServer.ConfCampos))
        oConfCampos.DevolverConfiguracionVisible(oUser.Cod, Request("sFormulario"), 1)

        ds = GenerarDataSetConfigPanelProve(Request, oConfCampos.Data, True)

        oConfCampos.ModificarVisibilidadCampos(oUser.Cod, ds, Request("sFormulario"))

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "initFin", "<script>refrescarPanelProve()</script>")
    End Sub

    Private Sub cmdGuardar_ServerClick1(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdGuardar.ServerClick
        'Guardar en BD
        Dim ds As DataSet
        Dim oUser As FSNServer.User
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oConfCampos As FSNServer.ConfCampos
        Dim sNomGrid As String

        Session("SelectorReal") = "Real"
        Session("Cargar") = 0

        oUser = Session("FSN_User")

        oConfCampos = FSWSServer.Get_Object(GetType(FSNServer.ConfCampos))
        oConfCampos.DevolverConfiguracionVisible(oUser.Cod, Request("sFormulario"), 1)

        If Request("sFormulario") = "panelProve" Then
            ds = GenerarDataSetConfigPanelProve(Request, oConfCampos.Data, False)
        Else
            ds = GenerarDataSetConfig(Request)
        End If

        oConfCampos.ModificarVisibilidadCampos(oUser.Cod, ds, Request("sFormulario"))

        If Request("sFormulario") = "panelProve" Then
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "initFin", "<script>refrescarPanelProve()</script>")
        Else
            'Selecciona la grid que se modificar�:
            Select Case Request("sFormulario")
                Case "mantenimientoMat", "detalleMatGSProv"
                    sNomGrid = "whdgMateriales"
                Case "certifBusqueda"
                    sNomGrid = "uwgCertificados"
            End Select

            Page.ClientScript.RegisterStartupScript(Me.GetType(), "init", "<script>refrescarConfiguracion('" & sNomGrid & "')</script>")
        End If
    End Sub

    Private Function Va_A_Bbdd(ByVal Temporal As Boolean, ByVal Update As Boolean, ByVal ValorBbdd As Integer, ByRef ValorPantalla As Integer) As Boolean
        Va_A_Bbdd = False

        If Temporal Then 'No se graba la configuraci�n. Se hace una configuraci�n 'temporal'
            If Update Then
                If ValorBbdd = 101 AndAlso ValorPantalla = 1 Then
                    ValorPantalla = 1
                    Va_A_Bbdd = True
                ElseIf ValorBbdd = 101 AndAlso ValorPantalla = 0 Then
                    Va_A_Bbdd = False
                ElseIf ValorBbdd = 100 AndAlso ValorPantalla = 1 Then
                    Va_A_Bbdd = False
                ElseIf ValorBbdd = 100 AndAlso ValorPantalla = 0 Then
                    ValorPantalla = 0
                    Va_A_Bbdd = True
                ElseIf ValorBbdd <> ValorPantalla AndAlso ValorPantalla = 1 Then
                    ValorPantalla = 100
                    Va_A_Bbdd = True
                ElseIf ValorBbdd <> ValorPantalla AndAlso ValorPantalla = 0 Then
                    ValorPantalla = 101
                    Va_A_Bbdd = True
                End If
            Else
                Va_A_Bbdd = True

                If ValorPantalla = 1 Then 'pone a 1
                    ValorPantalla = 100
                Else 'pone a 0
                    ValorPantalla = 0
                End If
            End If
        Else
            If ValorBbdd = 101 OrElse ValorBbdd = 100 Then
                Va_A_Bbdd = True
            ElseIf ValorBbdd <> ValorPantalla Then
                Va_A_Bbdd = True
            End If
        End If
    End Function

    Private Function SelectorReal() As Short
        SelectorReal = 1

        If Session("SelectorReal") <> Nothing Then
            If Session("SelectorReal") = "NOReal" Then
                SelectorReal = 0
            End If
        End If
    End Function
End Class

