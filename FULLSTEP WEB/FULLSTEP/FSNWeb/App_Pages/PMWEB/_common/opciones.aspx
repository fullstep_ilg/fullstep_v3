<%@ Page Language="vb" AutoEventWireup="false" Codebehind="opciones.aspx.vb" Inherits="Fullstep.FSNWeb.opciones" enableViewState="True"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="False" name="vs_snapToGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<script>
		function ComprobarSeparadores(mensaje)
		{
			if ($find("ugtxtSeparadorDecimal").get_currentValue() == $find("ugtxtSeparadorMiles").get_currentValue()) {
				alert(mensaje);
				return false;
			}
			else {
				return true;
			}
		}
	</script>	
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:ScriptManager ID="scrMng_opciones" runat="server">
			</asp:ScriptManager>
			<TABLE id="tblOpciones" style="Z-INDEX: 101; LEFT: 10px; POSITION: absolute; TOP: 12px"
				cellSpacing="1" cellPadding="1" width="100%" border="0" runat="server">
				<TR>
					<TD vAlign="middle" colSpan="2"><table width="100%"><tr><td width="65"><img alt="" src="images/useroptions.jpg" /></td><td><asp:label id="lblTitulo" runat="server" CssClass="captionBlue">Opciones del usuario</asp:label></td>
					<td align="right" valign="top"><img alt="" src="images/Bt_Cerrar.png" onclick="self.close()" /></td></tr></table></TD>
				</TR>
				<TR>
					<TD vAlign="middle" style="WIDTH: 240px"><asp:label id="lblSeparadorDecimal" runat="server" CssClass="parrafo">Separador decimal</asp:label></TD>
					<TD vAlign="middle"><ig:WebDropDown ID="ugtxtSeparadorDecimal" runat="server" Width="125px"
						DropDownContainerHeight="100px" DropDownContainerWidth="125px" DropDownAnimationType="EaseOut" 
						EnablePaging="False" PageSize="12">
							<Items>
								<ig:DropDownItem Key="COD" Value="," Text=",">
								</ig:DropDownItem>
								<ig:DropDownItem Key="COD" Value="." Text=".">
								</ig:DropDownItem>
							</Items>
						</ig:WebDropDown></TD>
				</TR>
				<TR>
					<TD vAlign="middle" style="WIDTH: 240px"><asp:label id="lblNumeroDecimales" runat="server" CssClass="parrafo">N�mero de decimales que se muestran</asp:label></TD>
					<TD vAlign="middle"><ig:WebDropDown ID="ugtxtNumeroDecimales" runat="server" Width="125px"
						DropDownContainerHeight="100px" DropDownContainerWidth="125px" DropDownAnimationType="EaseOut" 
						EnablePaging="False" PageSize="12" Font-Size="10px">
							<Items>
								<ig:DropDownItem Key="COD" Value="0" Text="0">
								</ig:DropDownItem>
								<ig:DropDownItem Key="COD" Value="1" Text="1">
								</ig:DropDownItem>
								<ig:DropDownItem Key="COD" Value="2" Text="2">
								</ig:DropDownItem>
								<ig:DropDownItem Key="COD" Value="3" Text="3">
								</ig:DropDownItem>
								<ig:DropDownItem Key="COD" Value="4" Text="4">
								</ig:DropDownItem>
								<ig:DropDownItem Key="COD" Value="5" Text="5">
								</ig:DropDownItem>
							</Items>
						</ig:WebDropDown></TD>
				</TR>
				<TR>
					<TD vAlign="middle" style="WIDTH: 240px"><asp:label id="lblSeparadorMiles" runat="server" CssClass="parrafo">Separador de grupo de miles</asp:label></TD>
					<TD vAlign="middle"><ig:WebDropDown ID="ugtxtSeparadorMiles" runat="server" Width="125px"
						DropDownContainerHeight="100px" DropDownContainerWidth="125px" DropDownAnimationType="EaseOut" 
						EnablePaging="False" PageSize="12">
							<Items>
								<ig:DropDownItem Key="COD" Value="" Text="(ninguno)">
								</ig:DropDownItem>
								<ig:DropDownItem Key="COD" Value="," Text=",">
								</ig:DropDownItem>
								<ig:DropDownItem Key="COD" Value="." Text=".">
								</ig:DropDownItem>
								<ig:DropDownItem Key="COD" Value=" " Text="(espacio)">
								</ig:DropDownItem>
							</Items>
						</ig:WebDropDown></TD>
				</TR>
				<TR>
					<TD vAlign="middle" style="WIDTH: 240px"><asp:label id="lblFormatoFecha" runat="server" CssClass="parrafo">Formato de fecha</asp:label></TD>
					<TD vAlign="middle"><ig:WebDropDown ID="ugtxtFormatoFecha" runat="server" Width="125px"
						DropDownContainerHeight="100px" DropDownContainerWidth="125px" DropDownAnimationType="EaseOut" 
						EnablePaging="False" PageSize="12">
							<Items>
								<ig:DropDownItem Key="COD" Text="dd/mm/yyyy" Value="dd/mm/yyyy">
								</ig:DropDownItem>
								<ig:DropDownItem Key="COD" Text="mm/dd/yyyy" Value="mm/dd/yyyy">
								</ig:DropDownItem>
								<ig:DropDownItem Key="COD" Text="dd-mm-yyyy" Value="dd-mm-yyyy">
								</ig:DropDownItem>
								<ig:DropDownItem Key="COD" Text="mm-dd-yyyy" Value="mm-dd-yyyy">
								</ig:DropDownItem>
								<ig:DropDownItem Key="COD" Text="dd.mm.yyyy" Value="dd.mm.yyyy">
								</ig:DropDownItem>
								<ig:DropDownItem Key="COD" Text="mm.dd.yyyy" Value="mm.dd.yyyy">
								</ig:DropDownItem>
							</Items>
						</ig:WebDropDown></TD>
				</TR>
				<TR>
					<TD vAlign="middle" style="WIDTH: 240px"><asp:label id="lblFormatoEmail" runat="server" CssClass="parrafo">Formato de comunicaciones v�a EMail</asp:label></TD>
					<TD vAlign="middle"><ig:WebDropDown ID="ugtxtFormatoEmail" runat="server" Width="125px"
						DropDownContainerHeight="100px" DropDownContainerWidth="125px" DropDownAnimationType="EaseOut" 
						EnablePaging="False" PageSize="12">
							<Items>
								<ig:DropDownItem Key="COD" Text="HTML" Value="1">
								</ig:DropDownItem>
								<ig:DropDownItem Key="COD" Text="Texto" Value="0">
								</ig:DropDownItem>
							</Items>
						</ig:WebDropDown></TD>
				</TR>
				<TR>
					<TD vAlign="middle" style="WIDTH: 240px"><asp:label id="lblIdioma" runat="server" CssClass="parrafo">Idioma</asp:label></TD>
					<TD vAlign="middle"><ig:WebDropDown ID="ugtxtIdioma" runat="server" Width="125px"
						DropDownContainerHeight="100px" DropDownContainerWidth="125px" DropDownAnimationType="EaseOut" 
						EnablePaging="False" PageSize="12">
							<ClientEvents />
							<Templates></Templates>
						</ig:WebDropDown></TD>
				</TR>
				<TR>
					<TD vAlign="middle" style="WIDTH: 240px"></TD>
					<TD vAlign="top"></TD>
				</TR>
				<TR>
					<TD align="center" colSpan="2"><asp:button id="cmdAceptar" runat="server" CssClass="botonPMWEB" Text="Aceptar"></asp:button></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</html>
