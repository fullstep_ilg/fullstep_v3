
Public Class attachfile
    Inherits FSNPage


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents miFile As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents txtComent As System.Web.UI.WebControls.TextBox
    Protected WithEvents IDCAMPO As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents FSENTRY As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents hid_desgloseAct As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents l As System.Web.UI.WebControls.Label
    Protected WithEvents lblComent As System.Web.UI.WebControls.Label
    Protected WithEvents cmdUpload As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents lblSeleccione As System.Web.UI.WebControls.Label
    Protected WithEvents filaLblComentarios As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents filaTxtComentarios As System.Web.UI.HtmlControls.HtmlTableRow

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' <summary>
    ''' Carga de la pantalla
    ''' </summary>
    ''' <param name="sender">pantalla</param>
    ''' <param name="e">parametro de sistema</param>            
    ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AttachFile

		lblSeleccione.Text = Textos(0)
		cmdUpload.Value = Textos(3)

		If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextoBotonAdjuntar") Then
			Dim scriptTextos As String
			scriptTextos = "var textoBotonAdjuntar = new Array();"
			scriptTextos &= "textoBotonAdjuntar[0]='" & Textos(10) & "';"
			scriptTextos &= "textoBotonAdjuntar[1]='" & Textos(11) & "';"
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextoBotonAdjuntar", scriptTextos, True)
		End If

		If Request("desgloseActividad") Is Nothing Then
			lblComent.Text = Textos(1)
			IDCAMPO.Value = Request("Campo")
			FSENTRY.Value = Request("Input")
			If Request("coment") <> "null" Then
				txtComent.Text = Replace(Request("Coment"), "/n", vbCrLf)
			End If
			If Request("EsContrato") = 1 Then
				If Request("idAdjuntoContrato") <> "" AndAlso Request("idAdjuntoContrato") <> "0" Then
					lblComent.Text = Textos(6) + " " + Server.UrlDecode(Request("Nombre"))
				End If
			Else
				If Request("ID") <> "" AndAlso Request("ID") <> "0" Then
					lblComent.Text = Textos(6) + " " + Server.UrlDecode(Request("Nombre"))
				End If
			End If
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "MostrarComentario", "<script>var mostrarComentario; mostrarComentario=true;</script>")
		Else
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "MostrarComentario", "<script>var mostrarComentario; mostrarComentario=false;</script>")

			hid_desgloseAct.Value = "1"
        End If

		If Not IsPostBack() Then
			'Control del tama�o del adjunto: MaxRequestLength est� en Kbytes
			miFile.Attributes("onchange") = "checkFileSize(" & ConfigurationManager.GetSection("system.web/httpRuntime").MaxRequestLength * 1024 & ",'" & Textos(9) & "');"

			If Request("Error") = "1" Then Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AlertFileTooLarge", "<script>alert('" & Textos(9) & "');</script>")

			ModuloIdioma = TiposDeDatos.ModulosIdiomas.DesgloseControl
			txtComent.Attributes("onchange") = "return validarLengthyCortarStringLargo(this,500,'" & Textos(12) & "')"
			ModuloIdioma = TiposDeDatos.ModulosIdiomas.AttachFile
		End If
	End Sub

    ''' <summary>
    ''' Grabar el adjunto y el comentario
    ''' </summary>
    ''' <param name="sender">bot�n Upload</param>
    ''' <param name="e">evento del bot�n</param>        
    ''' <remarks>Llamada desde: sistema (bot�n Upload); Tiempo m�ximo:0</remarks>
    Private Sub Submit1_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpload.ServerClick
        Dim lOldTimeOut As Long = Server.ScriptTimeout
        Dim COMENTSalto As String

        Server.ScriptTimeout = 1800

        Dim oAdjun As FSNServer.Adjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
        If Not Me.miFile.PostedFile Is Nothing And Me.miFile.PostedFile.ContentLength > 0 Then
            Dim fn As String = System.IO.Path.GetFileName(miFile.PostedFile.FileName)
            Dim vFolderPath As String = ConfigurationManager.AppSettings("TEMP")
            Dim SaveLocation As String = vFolderPath + "\" + fn
            Dim SaveLocationScript As String = SaveLocation.Replace("\", "\\")
            Dim EntryId As String = String.Empty
            Dim Origen As String = String.Empty
            Dim IdContrato As String = String.Empty
            Dim Coment As String = String.Empty
            miFile.PostedFile.SaveAs(SaveLocation)
            If Not Request("EntryId") Is Nothing Then
                EntryId = Request("EntryId")
            End If
            If Not Request("Origen") Is Nothing Then
                Origen = Request("Origen")
            End If
            If Not Request("IdContrato") Is Nothing Then
                IdContrato = Request("IdContrato")
            End If
            
            If hid_desgloseAct.Value <> "1" Then
                oAdjun.Coment = Request("txtComent")
                oAdjun.Per = FSNUser.CodPersona
                If Not Request("EsContrato") Is Nothing Then
                    Dim arrAdjuntoContrato(1) As Long
                    Dim idArchivoContrato As Long
                    arrAdjuntoContrato = oAdjun.Save_Adjun_Contrato_Wizard(FSNUser.CodPersona, "", fn, vFolderPath, IIf(Request("idContrato"), Request("idContrato"), 0), oAdjun.Coment)
                    idArchivoContrato = arrAdjuntoContrato(0)
                    COMENTSalto = Replace(oAdjun.Coment, vbCrLf, "@VBCRLF@")
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>anyadirAdjunto('" + EntryId + "'," + idArchivoContrato.ToString + ",'" + Replace(fn, "'", "\'") + "','" + JSText(oAdjun.Coment) + "'," + JSNum(CLng(miFile.PostedFile.ContentLength)) + ",'" + JSText(COMENTSalto) + "','" + Origen + "','" & IdContrato & "',1,'" + JSText(vFolderPath) + "')</script>")
                Else
                    Dim objReturn As Long()
                    objReturn = oAdjun.Save_Adjun(FSNUser.CodPersona, "", fn, vFolderPath, 0, oAdjun.Coment)
                    oAdjun.Id = objReturn(0)
                    oAdjun.DataSize = objReturn(1)
                    COMENTSalto = Replace(oAdjun.Coment, vbCrLf, "@VBCRLF@")
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>anyadirAdjunto('" + EntryId + "'," + oAdjun.Id.ToString + ",'" + Replace(oAdjun.Nom, "'", "\'") + "','" + JSText(oAdjun.Coment) + "'," + JSNum(CLng(oAdjun.DataSize / 1024)) + ",'" + JSText(COMENTSalto) + "','" + Origen + "','" & IdContrato & "',0,'')</script>")
                End If
            Else
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>DevolverPathPlantilla(encodeURI('" + SaveLocationScript + "'))</script>")
            End If
        End If
        oAdjun = Nothing
        Server.ScriptTimeout = lOldTimeOut
    End Sub
End Class

