<%@ Page Language="vb" AutoEventWireup="false" Codebehind="attachfile.aspx.vb" Inherits="Fullstep.FSNWeb.attachfile"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">		
	</head>
	<script type="text/javascript" src="../../../js/jquery/jquery.min.js"></script>
	<script type="text/javascript">		
		$(document).ready(function () {
			if (!mostrarComentario) {
				$('#lblComent').hide();
				$('#txtComent').hide();				
			}
			$('#btnAdjuntar').text(textoBotonAdjuntar[0]);
			$('#lblFichero').text(textoBotonAdjuntar[1]);
		});
		function anyadirAdjunto(entryId, Id, nom, com, datasize, comsalto, origen, idContrato, EsContrato, PathAdjunto) {
			/*''' <summary>
			''' A�adir en la pantalla de adjuntos el adjunto recien insertado y cerrar.
			''' </summary>
			''' <param name="Id">Id en bbdd del adjunto</param>
			''' <param name="nom">Nombre del adjunto</param>        
			''' <param name="com">Comentario del adjunto</param>
			''' <param name="datasize">tama�o del adjunto</param>
			''' <param name="comsalto">Comentario del adjunto sin saltos de linea. Se han traducido por @VBCRLF@.</param>  
			''' <returns>Nada</returns>
			''' <remarks>Llamada desde:Submit1_ServerClick; Tiempo m�ximo:0</remarks>*/
			if (origen.indexOf('atachedfilesDesglose.aspx') >= 0) {
				p = window.opener;
				j = p.opener;
				j.envioInfo(entryId, Id, nom, com, datasize, comsalto, origen, 0);
				p.envioInfoAdj(entryId, Id, nom, com, datasize, comsalto, origen);
			} else {
				if (origen.indexOf('atachedfiles.ascx') >= 0) {
					p = window.opener;
					var campo = document.getElementById("IDCAMPO");
					var fsEntry = document.getElementById("FSENTRY");
					p.adjuntoAnyadido(Id, nom, com, datasize, 2, campo.value, fsEntry.value, comsalto);
				} else {
					p = window.opener
					if (EsContrato == 1) p.envioInfoAdjuntoContrato(entryId, Id, nom, com, datasize, comsalto, origen, idContrato, PathAdjunto);
					else p.envioInfo(entryId, Id, nom, com, datasize, comsalto, origen, idContrato);
				}
			}
			window.close();
		}
		function DevolverPathPlantilla(pathPlantilla) {
			p = window.opener;
			p.LeerPlantillaSubida(pathPlantilla);
			window.close();
		}
		function simular_click() {
			document.Form1.miFile.click();
			document.Form1.cmdUpload.click();
        }        
		function checkFileSize(MaxSize, Msg) {
			/*''' <summary>Chequea el tama�o del adjunto</summary>
			''' <param name="MaxSize">Tama�o m�ximo del adjunto</param>
			''' <param name="Msg">Mensaje a mostrar si se supera el tama�o m�ximo</param>                        
			''' <remarks>Llamada desde: miFile.onchange</remarks> */
			var file = document.getElementById('miFile');
			$('#lblFichero').text(textoBotonAdjuntar[1]);
            if (typeof file.files != 'undefined') {
                if (typeof file.files[0] != 'undefined') {
                    if (file.files[0] != null) {
                        //file.size est� en bytes y MaxSize en kBytes
                        if (file.files[0].size > (MaxSize)) {
                        	alert(Msg);
                        	file.value = '';
                        } else $('#lblFichero').text(file.files[0].name);
                    }
                }
            }
        }        
		function validarLengthyCortarStringLargo(oCampo, longitud, msg) {
			/*Valida la longitud de un dataentry de tipo texto y si no cumple la condici�n, corta el texto.
			Llamadad desde: las cajas de texto de los dataentry medio y largo.        */
            var retorno = validarLengthStringLargo(oCampo, longitud, msg);
            if (!retorno) {
                oCampo.value = oCampo.value.substr(0, longitud);
            }
            return retorno;
        };        
		function validarLengthStringLargo(oCampo, longitud, msg) {
			/* <summary> ''' Valida la longitud de un dataentry de tipo texto
			''' </summary>
			''' <returns>Nada</returns>
			''' <remarks>Llamada desde: Todos los dataentry de tipo texto medio,largo y string ; Tiempo m�ximo: instantaneo</remarks> */
            msg = msg.replace("\\n", "\n")
            msg = msg.replace("\\n", "\n")
            if (oCampo.value.length > longitud) {
            	alert(msg + longitud);
            	return false;
            } else return true;
        };
	</script>	
	<body>
		<form id="Form1" method="post" runat="server">
			<div style="padding:1em;">
				<asp:Label id="lblSeleccione" runat="server" CssClass="captionBlue" style="display:block; margin-bottom:0.5em;"></asp:Label>
				<div class="bordear" style="display:inline-flex; width:100%; height:1.6em;">
					<div style="position:relative; width:18em; line-height:1.5em;">
						<button id="btnAdjuntar" class="botonPMWEB" style="position:absolute; top:0; left:0; z-index:0; width:20em;">Adjuntar</button>					
						<input runat="server" id="miFile" type="file" name="miFile" class="botonPMWEB" style="position:absolute; top:0; left:0; z-index:1; width:20em; opacity:0;" />					
					</div>
					<span id="lblFichero" style="display:inline; line-height:1.6em;"></span>
				</div>
				<asp:Label id="lblComent" runat="server" CssClass="CaptionBlue" style="display:block; margin:1em 0em 0.5em 0em;"></asp:Label>
				<asp:TextBox id="txtComent" runat="server" Rows="4" TextMode="MultiLine" style="width:100%; margin-bottom:0.5em;"></asp:TextBox>
				<div style="text-align:center;"><input id="cmdUpload" type="submit" value="Upload" name="Submit1" runat="server" class="botonPMWEB" /></div>
				<input type="hidden" id="IDCAMPO" name="IDCAMPO" runat="server" /> 
				<input type="hidden" id="hid_desgloseAct" name="hid_desgloseAct" runat="server" value="0" /> 
				<input type="hidden" id="FSENTRY" name="FSENTRY" runat="server" />
			</div>	
		</form>
	</body>
</html>
