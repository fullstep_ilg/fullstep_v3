﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="popupDesglose_Delete_Mover.aspx.vb" Inherits="Fullstep.FSNWeb.popupDesglose_Delete_Mover" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	
	<script type="text/javascript">
		/*''' <summary>
		''' Eliminar la linea
		''' </summary>
		''' <remarks>Llamada desde:hypquitar.Attributes/OnClick; Tiempo máximo:0</remarks>*/
		function quitar(){			
			var p = window.parent
			var a =   "" + "<%=Request("celda")%>"
			var b =  "" + "<%=Request("ClientID")%>"
			var c = "" + "<%=Request("index")%>"		
			var d = "" + "<%=Request("idCampo")%>"
			p.deleteRow(p.document.getElementById(a),b,c, d)		
			var oFrame = p.document.getElementById("<%=Request("FrameId")%>")
            if(oFrame)
			    oFrame.parentNode.removeChild(oFrame);
		}
		/*''' <summary>
		''' Mueve la linea de una solicitud a otra
		''' </summary>
		''' <remarks>Llamada desde:hypMover.Attributes/OnClick; Tiempo máximo:0</remarks>*/
		function mover(){			
			var pant = window.parent
			var a =   "" + "<%=Request("celda")%>"
			var b =  "" + "<%=Request("ClientID")%>"
			var c = "" + "<%=Request("index")%>"		
			var d = "" + "<%=Request("idCampo")%>"
			var s = "" + "<%=Request("solicitud")%>"
			var i = "" + "<%=Request("instancia")%>"
			var f = "" + "<%=Request("FrameId")%>"
			var p = "" + "<%=Request("Popup")%>" 
			pant.moverFilaSeleccionada(b, c, d, s, i, a, f, p)
		}		
	</script> 
	<body>
	<form id="form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <CompositeScript>
            <Scripts>
                <asp:ScriptReference Path="../alta/js/jsAlta.js" />
            </Scripts>
        </CompositeScript>
    </asp:ScriptManager>
			<table cellpadding="0" cellspacing="0" class="cabeceraDesglose" id="Table2" style="Z-INDEX: 100; LEFT: 0px; WIDTH: 136px; BORDER-TOP-STYLE: double; BORDER-RIGHT-STYLE: double; BORDER-LEFT-STYLE: double; POSITION: absolute; TOP: 0px; HEIGHT: 8px; BORDER-BOTTOM-STYLE: double"
				border="0">
				<tr valign="bottom">
					<td>
						<a id="cmdimgquitar" class="aPMWeb" style="CURSOR: hand" onclick="javascript:quitar()" name="imgquitar">
							<img src="../alta/images/EliminarRegistro.gif" border="0" height="16" width="16"></a>
					</td>
					<td>
						<asp:LinkButton style="TEXT-DECORATION:none" CssClass="cabeceraDesglose" id="hypquitar" runat="server">Eliminar línea</asp:LinkButton>
					</td>
				</tr>
			</table>				
			<table cellpadding="0" cellspacing="0" class="cabeceraDesglose" id="Table1" style="Z-INDEX: 100; LEFT: 0px; WIDTH: 136px; BORDER-RIGHT-STYLE: double; BORDER-LEFT-STYLE: double; POSITION: absolute; TOP: 24px; HEIGHT: 8px; BORDER-BOTTOM-STYLE: double"
				border="0">				
				<tr valign="bottom">
					<td>
						<a id="cmdimgmover" class="aPMWeb" style="CURSOR: hand" onclick="javascript:hypMover()" name="imgmover">
							<img src="../alta/images/MoverRegistro.gif" border="0" height="16" width="16"></a>
					</td>
					<td>
						<asp:LinkButton style="TEXT-DECORATION:none" CssClass="cabeceraDesglose" id="hypMover" runat="server">Mover línea</asp:LinkButton>
					</td>
				</tr>				
			</table>
	</form>
	</body>
</html>
