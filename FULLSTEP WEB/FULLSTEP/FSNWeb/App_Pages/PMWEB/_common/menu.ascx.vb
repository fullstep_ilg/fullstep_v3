Imports Fullstep
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports System.IO

Public Class MenuControl
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents uwmWS As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
    '''<summary>
    '''head control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents head As Global.System.Web.UI.WebControls.ContentPlaceHolder
    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm
    '''<summary>
    '''panMenuCabecera control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panMenuCabecera As Global.System.Web.UI.WebControls.Panel
    '''<summary>
    '''lnkbtnInicio control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnInicio As Global.System.Web.UI.WebControls.LinkButton
    '''<summary>
    '''lnkbtnAyuda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnAyuda As Global.System.Web.UI.WebControls.LinkButton
    '''<summary>
    '''lnkbtnPreferencias control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPreferencias As Global.System.Web.UI.WebControls.LinkButton
    '''<summary>
    '''lnkbtnSeguridad control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnSeguridad As Global.System.Web.UI.WebControls.LinkButton
    '''<summary>
    '''lnkbtnCerrarSesion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnCerrarSesion As Global.System.Web.UI.WebControls.LinkButton

    Protected WithEvents lnkbtnPoliticaCookies As Global.System.Web.UI.WebControls.LinkButton
    '''<summary>
    '''imgLogo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgLogo As Global.System.Web.UI.WebControls.Image
    '''<summary>
    '''panMenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panMenu As Global.System.Web.UI.WebControls.Panel
    '''<summary>
    '''fsnTabInformes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabInformes As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabIntegracion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabIntegracion As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabFacturacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabFacturacion As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''panTabsCtrlPresup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panTabsCtrlPresup As Global.System.Web.UI.WebControls.Panel
    '''<summary>
    '''fsnTabCtrlPres control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabCtrlPres As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabCtrlPresup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabCtrlPresup As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabCatalogo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabCatalogo As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabColaboracion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabColaboracion As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabCalidad control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabCalidad As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabCalidadConfiguracion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabCalidadConfiguracion As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabCalEncuestas control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabCalEncuestas As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabCalSolicitudesQA control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabCalSolicitudesQA As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabContratos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabContratos As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabProcesos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabProcesos As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''panTabsProcesos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panTabsProcesos As Global.System.Web.UI.WebControls.Panel
    '''<summary>
    '''fsnTabProcParametros control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabProcParametros As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabProcSeguimiento control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabProcSeguimiento As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabContrSeguimiento control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabContrSeguimiento As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabProcAlta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabProcAlta As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabContrAlta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabContrAlta As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''panTabsCalidad control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panTabsCalidad As Global.System.Web.UI.WebControls.Panel
    '''<summary>
    '''panTabsCalidadConfiguracion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panTabsCalidadConfiguracion As Global.System.Web.UI.WebControls.Panel
    '''<summary>
    '''fsnTabCalNotificaciones control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabCalNotificaciones As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabCalNoConformidades control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabCalNoConformidades As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabCalCertificados control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabCalCertificados As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabCalPanelProveedores control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabCalPanelProveedores As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabCalPanelEscalacionProveedores control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabCalPanelEscalacionProveedores As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''panTabsCatalogo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panTabsCatalogo As Global.System.Web.UI.WebControls.Panel
    '''<summary>
    '''fsnTabCatParametros control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabCatParametros As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabCatAprobacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabCatAprobacion As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabCatRecepcion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabCatRecepcion As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabCatSeguimiento control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabCatSeguimiento As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabCatEmision control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabCatEmision As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''panMnuProcesos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panMnuProcesos As Global.System.Web.UI.WebControls.Panel
    '''<summary>
    '''mnuProcesos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mnuProcesos As Global.System.Web.UI.WebControls.Menu
    '''<summary>
    '''panMnuContratos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panMnuContratos As Global.System.Web.UI.WebControls.Panel
    '''<summary>
    '''mnuContratos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mnuContratos As Global.System.Web.UI.WebControls.Menu
    '''<summary>
    '''panMnuCalidad control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panMnuCalidad As Global.System.Web.UI.WebControls.Panel
    '''<summary>
    '''mnuCalidad control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mnuCalidad As Global.System.Web.UI.WebControls.Menu
    '''<summary>
    '''panMnuCalidadConfiguracion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panMnuCalidadConfiguracion As Global.System.Web.UI.WebControls.Panel
    '''<summary>
    '''panMnuEncuestas control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panMnuEncuestas As Global.System.Web.UI.WebControls.Panel
    '''<summary>
    '''panMnuSolicitudesQA control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panMnuSolicitudesQA As Global.System.Web.UI.WebControls.Panel
    '''<summary>
    '''mnuCalidadConfiguracion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>    
    Protected WithEvents mnuCalidadConfiguracion As Global.System.Web.UI.WebControls.Menu
    '''<summary>
    '''mnuEncuestas control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>   
    Protected WithEvents mnuEncuestas As Global.System.Web.UI.WebControls.Menu
    '''<summary>
    '''mnuSolicitudesQA control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>   
    Protected WithEvents mnuSolicitudesQA As Global.System.Web.UI.WebControls.Menu
    '''<summary>
    '''panMnuCatalogo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panMnuCatalogo As Global.System.Web.UI.WebControls.Panel
    '''<summary>
    '''mnuCatalogo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mnuCatalogo As Global.System.Web.UI.WebControls.Menu
    '''<summary>
    '''panMnuCtrlPres control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panMnuCtrlPres As Global.System.Web.UI.WebControls.Panel
    '''<summary>
    '''panMnuInformes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panMnuInformes As Global.System.Web.UI.WebControls.Panel
    '''<summary>
    '''mnuCtrlPres control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mnuCtrlPres As Global.System.Web.UI.WebControls.Menu
    '''<summary>
    '''mnuInformes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mnuInformes As Global.System.Web.UI.WebControls.Menu
    '''<summary>
    '''fsnTabCtrlPresActivos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabCtrlPresActivos As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabInforme control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabInforme As Global.Fullstep.FSNWebControls.FSNTab
    '''<summary>
    '''fsnTabBI control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fsnTabBI As Global.Fullstep.FSNWebControls.FSNTab
    Protected WithEvents pnlSubMenu As Global.System.Web.UI.WebControls.Panel
    Protected WithEvents fsnTabSeguridad As Global.Fullstep.FSNWebControls.FSNTab
    Protected WithEvents panMnuSeguridad As Global.System.Web.UI.WebControls.Panel
    Protected WithEvents mnuSeguridad As Global.System.Web.UI.WebControls.Menu

    Protected WithEvents panTabsFacturacion As Global.System.Web.UI.WebControls.Panel
    Protected WithEvents panMnuFacturacion As Global.System.Web.UI.WebControls.Panel
    Protected WithEvents fsnTabFactAlta As Global.Fullstep.FSNWebControls.FSNTab
    Protected WithEvents fsnTabFactSeguimiento As Global.Fullstep.FSNWebControls.FSNTab
    Protected WithEvents fsnTabFactAutofacturacion As Global.Fullstep.FSNWebControls.FSNTab
    Protected WithEvents mnuFacturacion As Global.System.Web.UI.WebControls.Menu

    Protected WithEvents LblProcesando As Global.System.Web.UI.WebControls.Label
    Protected WithEvents panTabsNotificaciones As Global.System.Web.UI.WebControls.Panel
#End Region

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script DEFER=true language=""{0}"">{1}</script>"
    Private Const IncludeScriptFormat As String = ControlChars.CrLf & "<script type=""{0}"" src=""{1}""></script>"
    Public Seccion As String
    Public OpcionMenu As String
    Public OpcionSubMenu As String
    Private dPropiedades As New Dictionary(Of String, String)
    Private mipage As FSNPage
    Dim pages As System.Web.Configuration.PagesSection = ConfigurationManager.GetSection("system.web/pages")
    Dim theme As String = pages.Theme


    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Carga el men� para poder navegar por todas las aplicaciones y opciones a las que el usuario tiene acceso
    ''' </summary>
    ''' <remarks>Llamada desde: Al cargarse el control; Tiempo m�ximo: 1 sg</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'IMP: Estas variables tienen que estar aqui arriba porque tienen que estar definidas tanto si el menu es VISIBLE o NO.
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "ruta") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "ruta", "ruta='" & ConfigurationManager.AppSettings("ruta") & "';", True)
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "rutaFS") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "rutaFS", "var rutaFS='" & ConfigurationManager.AppSettings("rutaFS") & "';", True)
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "rutaPM") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "rutaPM", "var rutaPM='" & ConfigurationManager.AppSettings("rutaPM") & "';", True)
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "timeMenu") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "timeMenu", "var timeMenu;", True)

        Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

        mipage = CType(Page, FSNPage)
        mipage.ModuloIdioma = ModulosIdiomas.Menu

        LblProcesando.text = mipage.Textos(28)
        If Seccion <> "" AndAlso Session("sSession") <> "" Then
            Session("Secci�n") = Seccion
        End If

        If FSWSServer.TipoDeAutenticacion = TiposDeAutenticacion.Windows OrElse
            FSWSServer.TipoDeAutenticacion = TiposDeAutenticacion.LDAP OrElse
            FSWSServer.TipoDeAccesoLCX Then
            lnkbtnPoliticaCookies.Text = mipage.Textos(69)
            lnkbtnPoliticaCookies.PostBackUrl = ConfigurationManager.AppSettings.Get("ruta") & "App_Pages/politicacookies.aspx"
        Else
            lnkbtnPoliticaCookies.Visible = False
        End If

        lnkbtnPreferencias.Text = mipage.Textos(5)
        lnkbtnCerrarSesion.Text = mipage.Textos(20)
        lnkbtnAyuda.Text = mipage.Textos(19)
        lnkbtnAyuda.Visible = False
        lnkbtnInicio.Text = mipage.Textos(0)
        lnkbtnSeguridad.Text = mipage.Textos(30)
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "BloqueoPopUpsActivado") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "BloqueoPopUpsActivado", "var sBloqueoPopupActivado='" & mipage.Textos(72) & "';", True)
        End If

        If FSWSServer.TipoDeAccesoLCX = True OrElse Session("HayAutenticacionWindows") = True Then 'A diferencia de FSWeb hay que a�adir parent para que se cierre la ventana 
            lnkbtnCerrarSesion.Attributes.Add("onclick", "window.open('','_parent','');window.parent.close();return false;")
        Else
            lnkbtnCerrarSesion.OnClientClick = "document.location.href='" & ConfigurationManager.AppSettings("rutaPM") & "_common/AbandonarSesion.aspx';return false;"
        End If

        lnkbtnInicio.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaFS") & "Inicio.aspx" & "','_top'); return false;"
        lnkbtnPreferencias.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaPM") & "_common/opciones.aspx', 'Ventana_Opciones', 'width=390,height=260, location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no'); return false;"
        lnkbtnSeguridad.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaPM") & "_common/CambiarPWD.aspx', 'Cambiar_Contrase�a', 'width=390,height=310, location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no,top=200,left=200'); return false;"

        imgLogo.ImageUrl = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & theme & "/images/logo.gif"

        Dim pag As FSNPage
        pag = Me.Page
        If pag.ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/jquery.min.js")) Is Nothing OrElse pag.ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/jquery.min.js")).Count = 0 Then pag.ScriptMgr.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.min.js"))
        If pag.ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/jquery.tmpl.min.js")) Is Nothing OrElse pag.ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/jquery.tmpl.min.js")).Count = 0 Then pag.ScriptMgr.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.tmpl.min.js"))
        If pag.ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/jquery.ui.min.js")) Is Nothing OrElse pag.ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/jquery.ui.min.js")).Count = 0 Then pag.ScriptMgr.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.ui.min.js"))
        If pag.ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/tmpl.min.js")) Is Nothing OrElse pag.ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/tmpl.min.js")).Count = 0 Then pag.ScriptMgr.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/tmpl.min.js"))
        If pag.ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/jquery-migrate.min.js")) Is Nothing OrElse pag.ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/jquery-migrate.min.js")).Count = 0 Then pag.ScriptMgr.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery-migrate.min.js"))
        If pag.ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/App_Pages/PMWEB/_common/js/menu.js")) Is Nothing OrElse pag.ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/App_Pages/PMWEB/_common/js/menu.js")).Count = 0 Then pag.ScriptMgr.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/PMWEB/_common/js/menu.js"))

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "autenticacionIntegradaFrame") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "autenticacionIntegradaFrame",
                "var autenticacionIntegradaFrame = " & (FSWSServer.TipoDeAutenticacion = TiposDeAutenticacion.ServidorExterno).ToString.ToLower & "; " &
                "var parent_url ='" & ConfigurationManager.AppSettings("rutaAutenticacionIntegrada") & "';", True)
        End If

        Dim bAccesoFSPM As Boolean
        Dim bAccesoContratos As Boolean
        Dim bAccesoFSQA As Boolean
        Dim bAccesoFSEP As Boolean
        Dim bAccesoFSSM As Boolean
        Dim bAccesoFSINT As Boolean
        Dim bAccesoFACT As Boolean
        Dim nAplicaciones As Integer = 0
        Dim oAcceso As FSNLibrary.TiposDeDatos.ParametrosGenerales
        Dim oUser As FSNServer.User = mipage.Usuario

        If Not IsPostBack() AndAlso Not FSWSServer.TipoDeAutenticacion = TiposDeAutenticacion.ServidorExterno Then
            CargarTextos()

            oAcceso = mipage.FSNServer.TipoAcceso

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "activadoCN") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "activadoCN",
                    "<script>var activadoCN = " & oAcceso.gbAccesoFSCN.ToString.ToLower & ";</script>")
            End If

            Session("HayAutenticacionWindows") = False
            If oAcceso.giWinSecurityWeb = TiposDeAutenticacion.Windows Then
                lnkbtnSeguridad.Visible = False
                Session("HayAutenticacionWindows") = True
            End If
            Session("HayAutenticacionLCX") = False
            If oAcceso.gbAccesoLCX = True Then
                lnkbtnSeguridad.Visible = False
                Session("HayAutenticacionLCX") = True
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Accesos y n� de aplicaciones a las que tiene acceso
            ''''''''''''''''''''''''''''''''''''''''''''''''''''
            If oAcceso.gsAccesoFSPM <> FSNLibrary.TipoAccesoFSPM.SinAcceso And mipage.FSNUser.AccesoPM Then
                bAccesoFSPM = True
                nAplicaciones = nAplicaciones + 1
            End If
            If oAcceso.gbAccesoContratos AndAlso mipage.FSNUser.AccesoContratos Then
                bAccesoContratos = True
                nAplicaciones = nAplicaciones + 1
            End If
            If oAcceso.gsAccesoFSQA <> FSNLibrary.TipoAccesoFSQA.SinAcceso AndAlso (mipage.FSNUser.AccesoQA Or mipage.FSNUser.EsPeticionarioSolicitudesQA Or mipage.FSNUser.EsParticipanteSolicitudesQA Or mipage.FSNUser.EsPeticionarioEncuestas Or mipage.FSNUser.EsParticipanteEncuestas) Then
                bAccesoFSQA = True
                nAplicaciones = nAplicaciones + 1
            End If
            If oAcceso.gbAccesoFSEP And mipage.FSNUser.AccesoEP Then
                bAccesoFSEP = True
                nAplicaciones = nAplicaciones + 1
            End If
            If oAcceso.gbAccesoFSSM And mipage.FSNUser.AccesoSM Then
                bAccesoFSSM = True
                nAplicaciones = nAplicaciones + 1
            End If
            If oAcceso.g_bAccesoIS AndAlso oUser.AccesoINT Then
                bAccesoFSINT = True
                nAplicaciones = nAplicaciones + 1
            End If
            If oAcceso.g_bAccesoFSFA AndAlso oUser.AccesoFACT Then
                bAccesoFACT = True
                nAplicaciones = nAplicaciones + 1
            End If

            fsnTabSeguridad.Visible = oUser.AccesoSG
            panMnuSeguridad.Visible = oUser.AccesoSG
            fsnTabColaboracion.Visible = oAcceso.gbAccesoFSCN

            '--------------------'
            'Pestania de informes'
            '--------------------'
            fsnTabInformes.Visible = False
            If MostrarInformes(bAccesoFSEP, bAccesoFSPM, bAccesoFSQA, oUser, oAcceso) Then
                fsnTabInformes.Visible = True
            Else
                mnuInformes.Items.Remove(mnuInformes.FindItem("Informes"))
            End If
            If oUser.AccesoFSAL And oAcceso.gbAccesoFSAL Then
                fsnTabInformes.Visible = True
                If (Not oUser.ALVisorActividad) Then
                    mnuInformes.Items.Remove(mnuInformes.FindItem("Visor Actividad"))
                End If
            Else
                mnuInformes.Items.Remove(mnuInformes.FindItem("Visor Actividad"))
            End If
            If oUser.AccesoFSAL And oAcceso.gbAccesoFSAL Then
                fsnTabInformes.Visible = True
                If (Not oUser.ALVisorActividad) Then
                    mnuInformes.Items.Remove(mnuInformes.FindItem("Visor Rendimiento"))
                End If
            Else
                mnuInformes.Items.Remove(mnuInformes.FindItem("Visor Rendimiento"))
            End If
            If oUser.AccesoBI And oAcceso.gbAccesoFSBI And (oAcceso.giAccesoFSBITipo <> TipoAccesoFSBI.SinAcceso) Then
                fsnTabInformes.Visible = True
                Select Case oAcceso.giAccesoFSBITipo
                    Case TipoAccesoFSBI.Completo
                        If (Not oUser.BIConfigurarCubos) Then
                            mnuInformes.Items.Remove(mnuInformes.FindItem("Cubos"))
                        End If
                        If (Not oUser.BIConfigurarQlikApps) Then
                            mnuInformes.Items.Remove(mnuInformes.FindItem("QlikApps"))
                        End If
                    Case TipoAccesoFSBI.SoloCubosBI
                        mnuInformes.Items.Remove(mnuInformes.FindItem("QlikViewer"))
                        mnuInformes.Items.Remove(mnuInformes.FindItem("QlikApps"))
                        If (Not oUser.BIConfigurarCubos) Then
                            mnuInformes.Items.Remove(mnuInformes.FindItem("Cubos"))
                        End If
                    Case TipoAccesoFSBI.SoloQlikApps
                        mnuInformes.Items.Remove(mnuInformes.FindItem("BusinessInteligence"))
                        mnuInformes.Items.Remove(mnuInformes.FindItem("Cubos"))
                        If (Not oUser.BIConfigurarQlikApps) Then
                            mnuInformes.Items.Remove(mnuInformes.FindItem("QlikApps"))
                        End If
                End Select
            Else
                'quitamos los submenus ya que el propio menu hace de link a informes
                mnuInformes.Items.Remove(mnuInformes.FindItem("BusinessInteligence"))
                mnuInformes.Items.Remove(mnuInformes.FindItem("QlikViewer"))
                mnuInformes.Items.Remove(mnuInformes.FindItem("Cubos"))
                mnuInformes.Items.Remove(mnuInformes.FindItem("QlikApps"))
            End If
            If MostrarInformes(bAccesoFSEP, bAccesoFSPM, bAccesoFSQA, oUser, oAcceso) Then
                If (Not (oUser.AccesoBI And oAcceso.gbAccesoFSBI) And Not (oUser.AccesoFSAL And oAcceso.gbAccesoFSAL)) Then
                    'Si solo tiene acceso a los informes quitamos el submenu, ya que el propio menu hace de enlace.
                    If (Not mnuInformes.FindItem("Informes") Is Nothing) Then
                        mnuInformes.Items.Remove(mnuInformes.FindItem("Informes"))
                    End If
                End If
            End If
            'fsnTabInformes.Visible = MostrarInformes(bAccesoFSEP, bAccesoFSPM, bAccesoFSQA, oUser, oAcceso)
            '------------------------'
            'Fin Pestania de informes'
            '------------------------'
            fsnTabIntegracion.Visible = bAccesoFSINT
            fsnTabFacturacion.Visible = bAccesoFACT

            If oUser.AccesoVisorNotificaciones Then
                Me.panTabsNotificaciones.Visible = True
            End If

            Select Case nAplicaciones
                Case 0
                    'Master.Desconectar()
                Case 1
                    fsnTabProcesos.Visible = False
                    panMnuProcesos.Visible = False
                    fsnTabContratos.Visible = False
                    panMnuContratos.Visible = False
                    fsnTabCalidad.Visible = False
                    panMnuCalidad.Visible = False
                    fsnTabCalidadConfiguracion.Visible = False
                    panMnuCalidadConfiguracion.Visible = False
                    fsnTabCatalogo.Visible = False
                    panMnuCatalogo.Visible = False
                    fsnTabCtrlPresup.Visible = False
                    panMnuCtrlPres.Visible = False
                    fsnTabFacturacion.Visible = False
                    panMnuFacturacion.Visible = False
                    fsnTabCalEncuestas.Visible = False
                    panMnuEncuestas.Visible = False
                    fsnTabCalSolicitudesQA.Visible = False
                    panMnuSolicitudesQA.Visible = False
                    'est� a nivel de usuarios porque puede tratarse de una pyme y en ese caso se hace la comprobaci�n por UON de ese usuario
                    If bAccesoContratos Then
                        fsnTabContratos.Visible = True
                        panMnuContratos.Visible = True
                    Else
                        fsnTabContratos.Visible = False
                        panMnuContratos.Visible = False
                    End If
                    If bAccesoFSPM Then
                        panTabsProcesos.Visible = True
                    ElseIf bAccesoFSQA Then
                        panTabsCalidad.Visible = True
                        fsnTabCalPanelProveedores.Visible = mipage.FSNUser.QAPuntuacionProveedores
                        fsnTabCalCertificados.Visible = oAcceso.gbAccesoQACertificados
                        fsnTabCalNoConformidades.Visible = oAcceso.gbAccesoQANoConformidad
                        fsnTabCalPanelEscalacionProveedores.Visible = oUser.QAPanelEscalacionProveedoresAccesible

                        fsnTabCalidadConfiguracion.Visible = True
                        panMnuCalidadConfiguracion.Visible = True
                        If Not (mipage.FSNUser.QAMantenimientoUNQA OrElse mipage.FSNUser.QAMantenimientoProv) Then mnuCalidadConfiguracion.Items.Remove(mnuCalidadConfiguracion.FindItem("UnidadesNegocio"))
                        If Not mipage.FSNUser.QAMantenimientoMat Then mnuCalidadConfiguracion.Items.Remove(mnuCalidadConfiguracion.FindItem("Materiales"))
                        If Not oUser.QAPermitirMantNivelesEscalacion Then mnuCalidadConfiguracion.Items.Remove(mnuCalidadConfiguracion.FindItem("Escalacion"))
                        fsnTabCalidadConfiguracion.Visible = {oUser.QAPuntuacionProveedores, oUser.QACertifModExpiracion, oUser.QAModificarPeriodoValidezCertificados,
                                oUser.QACertifModExpiracion, oUser.QAModificarPeriodoValidezCertificados, oUser.QAContacAutom,
                                oAcceso.gbVarCal_Materiales, oUser.QAMantenimientoUNQA, oUser.QAModifProvCalidad, oUser.QAModifNotifCalif,
                                oUser.QAModifNotifProximidadNc, oUser.QAModifNotifMateriales, oUser.QACertifModExpiracion, oUser.QAContacAutom,
                                oUser.QAModifProvCalidad, oUser.QAPermisoModificarNivelesDefectoUNQA, oUser.QAPermitirMantNivelesEscalacion}.Contains(True)

                        fsnTabCalEncuestas.Visible = (oUser.EsPeticionarioEncuestas Or oUser.EsParticipanteEncuestas)
                        panMnuEncuestas.Visible = (oUser.EsPeticionarioEncuestas Or oUser.EsParticipanteEncuestas)
                        If Not oUser.EsPeticionarioEncuestas Then mnuEncuestas.Items.Remove(mnuEncuestas.FindItem("EncuestasAlta"))
                        If Not oUser.EsParticipanteEncuestas Then mnuEncuestas.Items.Remove(mnuEncuestas.FindItem("EncuestasSeguimiento"))

                        fsnTabCalSolicitudesQA.Visible = (oUser.EsPeticionarioSolicitudesQA Or oUser.EsParticipanteSolicitudesQA)
                        panMnuSolicitudesQA.Visible = (oUser.EsPeticionarioSolicitudesQA Or oUser.EsParticipanteSolicitudesQA)
                        If Not oUser.EsPeticionarioSolicitudesQA Then mnuSolicitudesQA.Items.Remove(mnuSolicitudesQA.FindItem("SolicitudesQAAlta"))
                        If Not oUser.EsParticipanteSolicitudesQA Then mnuSolicitudesQA.Items.Remove(mnuSolicitudesQA.FindItem("SolicitudesQASeguimiento"))
                    ElseIf bAccesoFSEP Then
                        panTabsCatalogo.Visible = True
                        Dim bAprovisionador As Boolean = (oUser.EPTipo = TipoAccesoFSEP.SoloAprovisionador Or oUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador)
                        Dim bAprobador As Boolean = (oUser.EPTipo = TipoAccesoFSEP.SoloAprobador Or oUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador Or oUser.EsAprobadorSolicitudContraPedidoAbierto)
                        fsnTabCatEmision.Visible = (bAprovisionador OrElse oUser.EPPermitirEmitirPedidosDesdePedidoAbierto)
                        fsnTabCatSeguimiento.Visible = bAprovisionador OrElse oUser.EPPermitirEmitirPedidosDesdePedidoAbierto
                        fsnTabCatAprobacion.Visible = bAprobador
                    ElseIf bAccesoFSSM Then
                        panTabsCtrlPresup.Visible = True
                        fsnTabCtrlPres.Visible = mipage.FSNUser.SMControlPresupuestario
                        fsnTabCtrlPresActivos.Visible = mipage.FSNUser.SMConsultarActivos
                    ElseIf bAccesoFACT Then
                        panTabsFacturacion.Visible = True
                        fsnTabFactAlta.Visible = oUser.IMAltaFacturas
                        fsnTabFactSeguimiento.Visible = oUser.IMSeguimientoFacturas
                        fsnTabFactAutofacturacion.Visible = oUser.IMVisorAutofacturas
                    End If
                Case Else
                    'Este tab de Contratos se mostrara solo cuando solo se tiene acceso a Pm. Hay 2 tabs de contratos para que si solo tiene
                    'acceso a Pm no aparezca como la ultima pestaña
                    If Not bAccesoFSPM Then
                        fsnTabProcesos.Visible = False
                        panMnuProcesos.Visible = False
                    End If
                    'est� a nivel de usuarios porque puede tratarse de una pyme y en ese caso se hace la comprobaci�n por UON de ese usuario
                    If bAccesoContratos Then
                        fsnTabContratos.Visible = True
                        panMnuContratos.Visible = True
                    Else
                        fsnTabContratos.Visible = False
                        panMnuContratos.Visible = False
                    End If

                    fsnTabCalidadConfiguracion.Visible = False
                    panMnuCalidadConfiguracion.Visible = False
                    fsnTabCalEncuestas.Visible = False
                    panMnuEncuestas.Visible = False
                    fsnTabCalSolicitudesQA.Visible = False
                    panMnuSolicitudesQA.Visible = False
                    If bAccesoFSQA Then
                        If Not oUser.QAMantenimientoUNQA AndAlso Not oUser.QAMantenimientoProv AndAlso mnuCalidad.FindItem("Configuracion") IsNot Nothing Then mnuCalidad.FindItem("Configuracion").ChildItems.Remove(mnuCalidad.FindItem("Configuracion/UnidadesNegocio")) 'unis
                        If Not mipage.FSNUser.QAMantenimientoMat AndAlso mnuCalidad.FindItem("Configuracion") IsNot Nothing Then mnuCalidad.FindItem("Configuracion").ChildItems.Remove(mnuCalidad.FindItem("Configuracion/Materiales")) 'mat
                        If Not oUser.QAPermitirMantNivelesEscalacion AndAlso mnuCalidad.FindItem("Configuracion") IsNot Nothing Then mnuCalidad.FindItem("Configuracion").ChildItems.Remove(mnuCalidad.FindItem("Configuracion/Escalacion"))
                        If Not oAcceso.gbAccesoQANoConformidad Then mnuCalidad.Items.Remove(mnuCalidad.FindItem("NoConformidades"))
                        If Not oAcceso.gbAccesoQACertificados Then mnuCalidad.Items.Remove(mnuCalidad.FindItem("Certificados"))
                        If Not mipage.FSNUser.QAPuntuacionProveedores Then mnuCalidad.Items.Remove(mnuCalidad.FindItem("PanelProveedores"))
                        If Not oUser.QAPanelEscalacionProveedoresAccesible Then mnuCalidad.Items.Remove(mnuCalidad.FindItem("PanelEscalacionProveedores"))
                        If Not {oUser.QAPuntuacionProveedores, oUser.QACertifModExpiracion, oUser.QAModificarPeriodoValidezCertificados,
                            oUser.QACertifModExpiracion, oUser.QAModificarPeriodoValidezCertificados, oUser.QAContacAutom,
                            oAcceso.gbVarCal_Materiales, oUser.QAMantenimientoUNQA, oUser.QAModifProvCalidad, oUser.QAModifNotifCalif,
                            oUser.QAModifNotifProximidadNc, oUser.QAModifNotifMateriales, oUser.QACertifModExpiracion, oUser.QAContacAutom,
                            oUser.QAModifProvCalidad, oUser.QAPermisoModificarNivelesDefectoUNQA, oUser.QAPermitirMantNivelesEscalacion}.Contains(True) Then
                            mnuCalidad.Items.Remove(mnuCalidad.FindItem("Configuracion"))
                        End If
                        If Not (oUser.EsPeticionarioSolicitudesQA Or oUser.EsParticipanteSolicitudesQA) Then
                            mnuCalidad.Items.Remove(mnuCalidad.FindItem("SolicitudesQA"))
                        Else
                            If Not oUser.EsPeticionarioSolicitudesQA Then mnuCalidad.FindItem("SolicitudesQA").ChildItems.Remove(mnuCalidad.FindItem("SolicitudesQA/SolicitudesQAAlta"))
                            If Not oUser.EsParticipanteSolicitudesQA Then mnuCalidad.FindItem("SolicitudesQA").ChildItems.Remove(mnuCalidad.FindItem("SolicitudesQA/SolicitudesQASeguimiento"))
                        End If
                        If Not (oUser.EsPeticionarioEncuestas Or oUser.EsParticipanteEncuestas) Then
                            mnuCalidad.Items.Remove(mnuCalidad.FindItem("Encuestas"))
                        Else
                            If Not oUser.EsPeticionarioEncuestas Then mnuCalidad.FindItem("Encuestas").ChildItems.Remove(mnuCalidad.FindItem("Encuestas/EncuestasAlta"))
                            If Not oUser.EsParticipanteEncuestas Then mnuCalidad.FindItem("Encuestas").ChildItems.Remove(mnuCalidad.FindItem("Encuestas/EncuestasSeguimiento"))
                        End If
                    Else
                        fsnTabCalidad.Visible = False
                        panMnuCalidad.Visible = False
                    End If
                    If bAccesoFSEP Then
                        Dim bAprovisionador As Boolean = (mipage.FSNUser.EPTipo = TipoAccesoFSEP.SoloAprovisionador Or mipage.FSNUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador)
                        Dim bAprobador As Boolean = (mipage.FSNUser.EPTipo = TipoAccesoFSEP.SoloAprobador Or mipage.FSNUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador Or mipage.FSNUser.EsAprobadorSolicitudContraPedidoAbierto)
                        If Not bAprobador Then
                            mnuCatalogo.Items.Remove(mnuCatalogo.FindItem("Aprobacion"))
                        End If
                        If Not (bAprovisionador OrElse oUser.EPPermitirEmitirPedidosDesdePedidoAbierto) Then
                            mnuCatalogo.Items.Remove(mnuCatalogo.FindItem("Seguimiento"))
                            mnuCatalogo.Items.Remove(mnuCatalogo.FindItem("Emision"))
                        End If
                    Else
                        fsnTabCatalogo.Visible = False
                        panMnuCatalogo.Visible = False
                    End If
                    If bAccesoFSSM Then
                        If Not oUser.SMControlPresupuestario Then mnuCtrlPres.Items.Remove(mnuCtrlPres.FindItem("ControlPresupuestario"))
                        If Not oUser.SMConsultarActivos Then mnuCtrlPres.Items.Remove(mnuCtrlPres.FindItem("Activos"))
                    Else
                        fsnTabCtrlPresup.Visible = False
                        panMnuCtrlPres.Visible = False
                    End If
                    If bAccesoFACT Then
                        If Not oUser.IMAltaFacturas Then mnuFacturacion.Items.Remove(mnuFacturacion.FindItem("Alta"))
                        If Not oUser.IMSeguimientoFacturas Then mnuFacturacion.Items.Remove(mnuFacturacion.FindItem("Seguimiento"))
                        If Not oUser.IMVisorAutofacturas Then mnuFacturacion.Items.Remove(mnuFacturacion.FindItem("Autofacturacion"))
                    Else
                        fsnTabFacturacion.Visible = False
                        panMnuFacturacion.Visible = False
                    End If
            End Select
        End If

        LeerFicheroTXT()

        If Not FSWSServer.TipoDeAutenticacion = TiposDeAutenticacion.ServidorExterno Then
            InicializarControles(oUser)
            Seleccionar(OpcionMenu, OpcionSubMenu)

            If mipage.Usuario.AccesoCN Then
                If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "segundosNotificacionesCN") Then _
                    Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "segundosnNotificacionesCN",
                        "var segundosNotificacionesCN=" & CType(ConfigurationManager.AppSettings("segundosNotificacionesCN"), Integer) * 1000 & ";", True)
                Dim modulo As Integer = mipage.ModuloIdioma
                mipage.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Menu
                If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosTiempoRelativo") Then
                    Dim sVariableJavascriptTextosTiempoRelativo As String = "var TextosTiempoRelativo = new Array();"
                    sVariableJavascriptTextosTiempoRelativo &= "TextosTiempoRelativo[0]='" & JSText(mipage.Textos(46)) & "';"
                    sVariableJavascriptTextosTiempoRelativo &= "TextosTiempoRelativo[1]='" & JSText(mipage.Textos(47)) & "';"
                    sVariableJavascriptTextosTiempoRelativo &= "var unidadesTiempo = {" &
                    "1:'" & mipage.Textos(48) & "'" & ",2:'" & mipage.Textos(49) & "'" & ",3:'" & mipage.Textos(50) & "'" &
                    ",4:'" & mipage.Textos(51) & "'" & ",5:'" & mipage.Textos(52) & "'" & ",6:'" & mipage.Textos(53) & "'" &
                    ",11:'" & mipage.Textos(54) & "'" & ",12:'" & mipage.Textos(55) & "'" & ",13:'" & mipage.Textos(56) & "'" &
                    ",14:'" & mipage.Textos(57) & "'" & ",15:'" & mipage.Textos(58) & "'" & ",16:'" & mipage.Textos(59) & "'" & "};"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosTiempoRelativo", sVariableJavascriptTextosTiempoRelativo, True)
                End If

                mipage.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Colaboracion
                If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
                    Dim sVariableJavascriptTextos As String = "var Textos = new Array();"
                    Dim sVariableJavascriptTextosCKEditor As String = "var TextosCKEditor = new Array();"

                    For i As Integer = 0 To 73
                        sVariableJavascriptTextos &= "Textos[" & i & "]='" & JSText(mipage.Textos(i)) & "';"
                        If i = 20 Then sVariableJavascriptTextosCKEditor &= "TextosCKEditor[1]='" & JSText(mipage.Textos(i)) & "';"
                        If i = 21 Then sVariableJavascriptTextosCKEditor &= "TextosCKEditor[2]='" & JSText(mipage.Textos(i)) & "';"
                    Next
                    For i As Integer = 83 To 85
                        sVariableJavascriptTextos &= "Textos[" & i - 9 & "]='" & JSText(mipage.Textos(i)) & "';"
                    Next
                    For i As Integer = 74 To 89
                        sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 71 & "]='" & JSText(mipage.Textos(i)) & "';"
                    Next
                    For i As Integer = 90 To 121
                        sVariableJavascriptTextos &= "Textos[" & i - 13 & "]='" & JSText(mipage.Textos(i)) & "';"
                    Next
                    For i As Integer = 142 To 143
                        sVariableJavascriptTextos &= "Textos[" & i - 33 & "]='" & JSText(mipage.Textos(i)) & "';"
                    Next
                    For i As Integer = 144 To 147
                        sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 125 & "]='" & JSText(mipage.Textos(i)) & "';"
                    Next
                    For i As Integer = 148 To 149
                        sVariableJavascriptTextos &= "Textos[" & i - 37 & "]='" & JSText(mipage.Textos(i)) & "';"
                    Next
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextos, True)
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosCKEditor", sVariableJavascriptTextosCKEditor, True)
                End If
                If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "DateFormatUsu") Then
                    With mipage.Usuario
                        Dim sVariablesJavascript As String = "var UsuMask = '" & .DateFormat.ShortDatePattern & "';"
                        sVariablesJavascript = sVariablesJavascript & "var UsuMaskSeparator='" & .DateFormat.DateSeparator & "';"
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "DateFormatUsu", sVariablesJavascript, True)
                    End With
                End If
                mipage.ModuloIdioma = modulo
            End If
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered("ParametrosEP") Then
            Dim sScript As String = "function ParametrosEP(valor){" & vbCrLf &
                "if (valor=='Parametros'){" & vbCrLf &
                "window.open('" & ConfigurationManager.AppSettings("rutaEP") & "ParametrosEP.aspx?Idioma=" & mipage.Idioma & "&Modificar=-1" & "','winFormatos','width=500,height=300, location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =yes');" & vbCrLf &
                "return false;}" & vbCrLf &
                "else {" & vbCrLf &
                "document.forms[0].target='_top';" & vbCrLf &
                "return true;}}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ParametrosEP", sScript, True)
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered("ParametrosPM") Then
            Dim sScript As String = "function ParametrosPM(valor){" & vbCrLf &
                "if (valor=='Parametros'){" & vbCrLf &
                "window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Parametros/parametrosPM.aspx" & "','parametrosPM','width=500,height=250, location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no');" & vbCrLf &
                "return false;}" & vbCrLf &
                "else {" & vbCrLf &
                "document.forms[0].target='_top';" & vbCrLf &
                "return true;}}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ParametrosPM", sScript, True)
        End If

        If Not pag.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "version") Then
            pag.ClientScript.RegisterClientScriptBlock(Me.GetType(), "version",
                "<script>var version = '" & System.Configuration.ConfigurationManager.AppSettings("version") & "';</script>")
        End If

        If FSWSServer.TipoDeAutenticacion = TiposDeAutenticacion.ServidorExterno Then
            Visible = False
            Exit Sub
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "ActivarTabs") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "ActivarTabs", ScriptActivarTabs(), True)

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "MostrarOcultarMenu") Then
            Dim sScript As String = ScriptTabMouseOver()
            sScript = sScript & ScriptTabMouseOut()
            Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "MostrarOcultarMenu", sScript, True)
        End If

        If Not Page.ClientScript.IsStartupScriptRegistered(Me.Page.GetType(), "IniciarMenu") Then _
            Page.ClientScript.RegisterStartupScript(Me.Page.GetType(), "IniciarMenu", "TabsOut();", True)
    End Sub
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Inicializa todos los controles del men�
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load; Tiempo m�ximo: 0sg.</remarks>
    Private Sub InicializarControles(ByVal oUser As Fullstep.FSNServer.User)
        pnlSubMenu.Attributes.Add("onmouseover", "window.clearTimeout(timeMenu);")
        pnlSubMenu.Attributes.Add("onmouseout", "timeMenu=window.setTimeout(TabsOut,1500);")

        InicializarTab(fsnTabSeguridad, "seguridad")
        InicializarTab(fsnTabColaboracion, "colaboracion")
        InicializarTab(fsnTabProcesos, "procesos")
        InicializarTab(fsnTabContratos, "contratos")
        InicializarTab(fsnTabCalidad, "calidad")
        InicializarTab(fsnTabCalidadConfiguracion, "configuracion")
        InicializarTab(fsnTabCatalogo, "catalogo")
        InicializarTab(fsnTabCtrlPresup, "ctrlpresup")
        InicializarTab(fsnTabInformes, "informes")
        InicializarTab(fsnTabProcAlta, "procalta")
        InicializarTab(fsnTabProcSeguimiento, "procseguimiento")
        InicializarTab(fsnTabProcParametros, "procparametros")
        InicializarTab(fsnTabContrAlta, "contralta")
        InicializarTab(fsnTabContrSeguimiento, "contrseguimiento")
        InicializarTab(fsnTabCalPanelProveedores, "calpanelproveedores")
        InicializarTab(fsnTabCalCertificados, "calcertificados")
        InicializarTab(fsnTabCalNoConformidades, "calnoconformidades")
        InicializarTab(fsnTabCalNotificaciones, "calnotificaciones")
        InicializarTab(fsnTabCatEmision, "catemision")
        InicializarTab(fsnTabCatSeguimiento, "catseguimiento")
        InicializarTab(fsnTabCatRecepcion, "catrecepcion")
        InicializarTab(fsnTabCatAprobacion, "cataprobacion")
        InicializarTab(fsnTabCatParametros, "catparametros")
        InicializarTab(fsnTabCtrlPres, "ctrlpres")
        InicializarTab(fsnTabCtrlPresActivos, "ctrlpresactivos")
        InicializarTab(fsnTabIntegracion, "integracion")
        InicializarTab(fsnTabFacturacion, "facturacion")
        InicializarTab(fsnTabFactAlta, "factalta")
        InicializarTab(fsnTabFactSeguimiento, "factseguimiento")
        InicializarTab(fsnTabFactAutofacturacion, "factautofacturacion")
        InicializarTab(fsnTabCalSolicitudesQA, "calsolicitudesqa")
        InicializarTab(fsnTabCalEncuestas, "calencuestas")

        'PM (falta par�metros)
        mnuProcesos.Attributes.Add("align", "right")
        InicializarEstiloSubmenu(mnuProcesos, "lnkmnuProcesos")
        If Not mnuProcesos.FindItem("AltaSolicitudes") Is Nothing Then
            mnuProcesos.FindItem("AltaSolicitudes").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx"
        End If
        If Not mnuProcesos.FindItem("Seguimiento") Is Nothing Then
            mnuProcesos.FindItem("Seguimiento").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx"
        End If
        If Not fsnTabProcAlta Is Nothing Then
            fsnTabProcAlta.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx" & "','_top'); return false;"
        End If
        If Not fsnTabProcSeguimiento Is Nothing Then
            fsnTabProcSeguimiento.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx" & "','_top'); return false;"
        End If

        'Contratos
        mnuContratos.Attributes.Add("align", "right")
        InicializarEstiloSubmenu(mnuContratos, "lnkmnuContratos")
        If Not mnuContratos.FindItem("AltaContratos") Is Nothing Then
            mnuContratos.FindItem("AltaContratos").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "contratos/AltaContratos.aspx"
        End If
        If Not mnuContratos.FindItem("Seguimiento") Is Nothing Then
            mnuContratos.FindItem("Seguimiento").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "contratos/VisorContratos.aspx"
        End If
        If Not fsnTabContrAlta Is Nothing Then
            fsnTabContrAlta.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "contratos/AltaContratos.aspx" & "','_top'); return false;"
        End If
        If Not fsnTabContrSeguimiento Is Nothing Then
            fsnTabContrSeguimiento.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "contratos/VisorContratos.aspx" & "','_top'); return false;"
        End If

        'QA
        mnuCalidad.Attributes.Add("align", "right")
        mnuCalidadConfiguracion.Attributes.Add("align", "right")
        mnuSolicitudesQA.Attributes.Add("align", "right")
        mnuEncuestas.Attributes.Add("align", "right")
        InicializarEstiloSubmenu(mnuCalidad, "lnkmnuCalidad", True)
        InicializarEstiloSubmenu(mnuCalidadConfiguracion, "lnkmnuCalidadConfiguracion")
        InicializarEstiloSubmenu(mnuSolicitudesQA, "lnkmnuSolicitudesQA")
        InicializarEstiloSubmenu(mnuEncuestas, "lnkmnuEncuestas")
        If Not mnuCalidad.FindItem("PanelProveedores") Is Nothing Then
            mnuCalidad.FindItem("PanelProveedores").NavigateUrl = ConfigurationManager.AppSettings("rutaQA2008") & "puntuacion/panelCalidadFiltro.aspx?opcion=PanelProveedores"
        End If
        If mnuCalidad.FindItem("Configuracion") IsNot Nothing AndAlso mnuCalidad.FindItem("Configuracion").ChildItems IsNot Nothing Then
            If mnuCalidad.FindItem("Configuracion/Materiales") IsNot Nothing Then _
                mnuCalidad.FindItem("Configuracion/Materiales").NavigateUrl = ConfigurationManager.AppSettings("rutaQA") &
                "Frames.aspx?pagina=" & Server.UrlEncode("materiales/mantenimientomat.aspx")
            If mnuCalidad.FindItem("Configuracion/UnidadesNegocio") IsNot Nothing Then _
                mnuCalidad.FindItem("Configuracion/UnidadesNegocio").NavigateUrl = ConfigurationManager.AppSettings("rutaQA") &
                "Frames.aspx?pagina=" & Server.UrlEncode("materiales/UnidadesNegocio.aspx")
            If mnuCalidad.FindItem("Configuracion/Parametros") IsNot Nothing Then _
                mnuCalidad.FindItem("Configuracion/Parametros").NavigateUrl = ConfigurationManager.AppSettings("rutaQA") &
                "Frames.aspx?pagina=" & Server.UrlEncode("_common/parametrosgeneralesPMWEB.aspx")
            If mnuCalidad.FindItem("Configuracion/Escalacion") IsNot Nothing Then _
                mnuCalidad.FindItem("Configuracion/Escalacion").NavigateUrl = ConfigurationManager.AppSettings("rutaQA2008") &
                "Configuracion/ConfigNivelEscalacionProves.aspx"
        End If
        If Not mnuCalidad.FindItem("Certificados") Is Nothing Then
            mnuCalidad.FindItem("Certificados").NavigateUrl = ConfigurationManager.AppSettings("rutaQA2008") & "Certificados/Certificados.aspx"
        End If
        If Not mnuCalidad.FindItem("NoConformidades") Is Nothing Then
            mnuCalidad.FindItem("NoConformidades").NavigateUrl = ConfigurationManager.AppSettings("rutaQA2008") & "NoConformidades/NoConformidades.aspx"
        End If
        If Not mnuCalidad.FindItem("Notificaciones") Is Nothing Then
            mnuCalidad.FindItem("Notificaciones").NavigateUrl = ConfigurationManager.AppSettings("rutaFS") & "_common/Notificaciones/Notificaciones.aspx"
        End If
        If Not mnuCalidad.FindItem("PanelEscalacionProveedores") Is Nothing Then
            mnuCalidad.FindItem("PanelEscalacionProveedores").NavigateUrl = ConfigurationManager.AppSettings("rutaQA2008") & "EscalacionProveedores/PanelEscalacionProves.aspx"
        End If
        If mnuCalidad.FindItem("SolicitudesQA") IsNot Nothing AndAlso mnuCalidad.FindItem("SolicitudesQA").ChildItems IsNot Nothing Then
            If mnuCalidad.FindItem("SolicitudesQA/SolicitudesQAAlta") IsNot Nothing Then _
                mnuCalidad.FindItem("SolicitudesQA/SolicitudesQAAlta").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.SolicitudQA
            If mnuCalidad.FindItem("SolicitudesQA/SolicitudesQASeguimiento") IsNot Nothing Then _
                mnuCalidad.FindItem("SolicitudesQA/SolicitudesQASeguimiento").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=" & TipoVisor.SolicitudesQA
        End If
        If mnuCalidad.FindItem("Encuestas") IsNot Nothing AndAlso mnuCalidad.FindItem("Encuestas").ChildItems IsNot Nothing Then
            If mnuCalidad.FindItem("Encuestas/EncuestasAlta") IsNot Nothing Then _
                mnuCalidad.FindItem("Encuestas/EncuestasAlta").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.Encuesta
            If mnuCalidad.FindItem("Encuestas/EncuestasSeguimiento") IsNot Nothing Then _
                mnuCalidad.FindItem("Encuestas/EncuestasSeguimiento").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=" & TipoVisor.Encuestas
        End If

        If Not fsnTabCalPanelProveedores Is Nothing Then
            fsnTabCalPanelProveedores.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaQA2008") & "puntuacion/panelCalidadFiltro.aspx?opcion=PanelProveedores" & "','_top'); return false;"
        End If
        If Not fsnTabCalCertificados Is Nothing Then
            fsnTabCalCertificados.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaQA2008") & "Certificados/Certificados.aspx" & "','_top'); return false;"
        End If
        If Not fsnTabCalNoConformidades Is Nothing Then
            fsnTabCalNoConformidades.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaQA2008") & "NoConformidades/NoConformidades.aspx" & "','_top'); return false;"
        End If
        If Not fsnTabCalPanelEscalacionProveedores Is Nothing Then
            fsnTabCalPanelEscalacionProveedores.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaQA2008") & "EscalacionProveedores/PanelEscalacionProves.aspx" & "','_top'); return false;"
        End If
        If mnuCalidadConfiguracion.FindItem("Materiales") IsNot Nothing Then
            mnuCalidadConfiguracion.FindItem("Materiales").NavigateUrl = ConfigurationManager.AppSettings("rutaQA") & "Frames.aspx?pagina=" & Server.UrlEncode("materiales/mantenimientomat.aspx")
        End If
        If mnuCalidadConfiguracion.FindItem("UnidadesNegocio") IsNot Nothing Then
            mnuCalidadConfiguracion.FindItem("UnidadesNegocio").NavigateUrl = ConfigurationManager.AppSettings("rutaQA") & "Frames.aspx?pagina=" & Server.UrlEncode("materiales/UnidadesNegocio.aspx")
        End If
        If mnuCalidadConfiguracion.FindItem("Parametros") IsNot Nothing Then
            mnuCalidadConfiguracion.FindItem("Parametros").NavigateUrl = ConfigurationManager.AppSettings("rutaQA") & "Frames.aspx?pagina=" & Server.UrlEncode("_common/parametrosgeneralesPMWEB.aspx")
        End If
        If mnuCalidadConfiguracion.FindItem("Escalacion") IsNot Nothing Then
            mnuCalidadConfiguracion.FindItem("Escalacion").NavigateUrl = ConfigurationManager.AppSettings("rutaQA2008") & "Configuracion/ConfigNivelEscalacionProves.aspx"
        End If
        If Not fsnTabCalNotificaciones Is Nothing Then
            fsnTabCalNotificaciones.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_common/Notificaciones/Notificaciones.aspx" & "','_top'); return false;"
        End If
        If panTabsCalidad.Visible AndAlso mnuCalidadConfiguracion.Items.Count = 1 Then
            mnuCalidadConfiguracion.Visible = False
            fsnTabCalidadConfiguracion.Text = mnuCalidadConfiguracion.FindItem("Parametros").Text
            fsnTabCalidadConfiguracion.OnClientClick = "window.open('" &
                ConfigurationManager.AppSettings("rutaQA") &
                "Frames.aspx?pagina=" & Server.UrlEncode("_common/parametrosgeneralesPMWEB.aspx") & "','_top'); return false;"
        Else
            If mnuCalidad.FindItem("Configuracion") IsNot Nothing AndAlso mnuCalidad.FindItem("Configuracion").ChildItems.Count = 1 Then
                mnuCalidad.FindItem("Configuracion").Text = mnuCalidad.FindItem("Configuracion/Parametros").Text
                mnuCalidad.FindItem("Configuracion").ChildItems.Remove(mnuCalidad.FindItem("Configuracion/Parametros"))
                mnuCalidad.FindItem("Configuracion").Selectable = True
                mnuCalidad.FindItem("Configuracion").NavigateUrl = ConfigurationManager.AppSettings("rutaQA") &
                    "Frames.aspx?pagina=" & Server.UrlEncode("_common/parametrosgeneralesPMWEB.aspx")
            End If
        End If
        If mnuSolicitudesQA.FindItem("SolicitudesQAAlta") IsNot Nothing Then
            mnuSolicitudesQA.FindItem("SolicitudesQAAlta").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.SolicitudQA
        End If
        If mnuSolicitudesQA.FindItem("SolicitudesQASeguimiento") IsNot Nothing Then
            mnuSolicitudesQA.FindItem("SolicitudesQASeguimiento").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=" & TipoVisor.SolicitudesQA
        End If
        If mnuEncuestas.FindItem("EncuestasAlta") IsNot Nothing Then
            mnuEncuestas.FindItem("EncuestasAlta").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.Encuesta
        End If
        If mnuEncuestas.FindItem("EncuestasSeguimiento") IsNot Nothing Then
            mnuEncuestas.FindItem("EncuestasSeguimiento").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=" & TipoVisor.Encuestas
        End If
        If panTabsCalidad.Visible AndAlso mnuSolicitudesQA.Items.Count = 1 Then
            If mnuSolicitudesQA.Items.Count = 1 Then
                mnuSolicitudesQA.Visible = False
                If oUser.EsPeticionarioSolicitudesQA Then
                    fsnTabCalSolicitudesQA.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.SolicitudQA & "','_top'); return false;"
                Else
                    fsnTabCalSolicitudesQA.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=" & TipoVisor.SolicitudesQA & "','_top'); return false;"
                End If
            End If
        Else
            If mnuCalidad.FindItem("SolicitudesQA") IsNot Nothing AndAlso mnuCalidad.FindItem("SolicitudesQA").ChildItems.Count = 1 Then
                If oUser.EsPeticionarioSolicitudesQA Then
                    mnuCalidad.FindItem("SolicitudesQA").ChildItems.Remove(mnuCalidad.FindItem("SolicitudesQA/SolicitudesQAAlta"))
                    mnuCalidad.FindItem("SolicitudesQA").Selectable = True
                    mnuCalidad.FindItem("SolicitudesQA").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.SolicitudQA
                Else
                    mnuCalidad.FindItem("SolicitudesQA").ChildItems.Remove(mnuCalidad.FindItem("SolicitudesQA/SolicitudesQASeguimiento"))
                    mnuCalidad.FindItem("SolicitudesQA").Selectable = True
                    mnuCalidad.FindItem("SolicitudesQA").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=" & TipoVisor.SolicitudesQA
                End If
            End If
        End If
        If panTabsCalidad.Visible AndAlso mnuEncuestas.Items.Count = 1 Then
            If mnuEncuestas.Items.Count = 1 Then
                mnuEncuestas.Visible = False
                If oUser.EsPeticionarioEncuestas Then
                    fsnTabCalEncuestas.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.Encuesta & "','_top'); return false;"
                Else
                    fsnTabCalEncuestas.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=" & TipoVisor.Encuestas & "','_top'); return false;"
                End If
            End If
        Else
            If mnuCalidad.FindItem("Encuestas") IsNot Nothing AndAlso mnuCalidad.FindItem("Encuestas").ChildItems.Count = 1 Then
                If oUser.EsPeticionarioEncuestas Then
                    mnuCalidad.FindItem("Encuestas").ChildItems.Remove(mnuCalidad.FindItem("Encuestas/EncuestasAlta"))
                    mnuCalidad.FindItem("Encuestas").Selectable = True
                    mnuCalidad.FindItem("Encuestas").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.Encuesta
                Else
                    mnuCalidad.FindItem("Encuestas").ChildItems.Remove(mnuCalidad.FindItem("Encuestas/EncuestasSeguimiento"))
                    mnuCalidad.FindItem("Encuestas").Selectable = True
                    mnuCalidad.FindItem("Encuestas").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=" & TipoVisor.Encuestas
                End If
            End If
        End If

        'EP
        mnuCatalogo.Attributes.Add("align", "right")
        InicializarEstiloSubmenu(mnuCatalogo, "lnkmnuCatalogo")
        If Not (oUser.EPTipo = TipoAccesoFSEP.SoloAprovisionador OrElse oUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador) Then
            fsnTabCatEmision.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaEP") & "PedidoAbierto.aspx" & "','_top'); return false;"
        Else
            fsnTabCatEmision.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaEP") & "CatalogoWeb.aspx" & "','_top'); return false;"
        End If
        fsnTabCatSeguimiento.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaEP") & "Seguimiento.aspx" & "','_top'); return false;"
        fsnTabCatRecepcion.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaEP") & "Recepcion.aspx" & "','_top'); return false;"
        fsnTabCatAprobacion.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaEP") & "Aprobacion.aspx" & "','_top'); return false;"
        For Each x As MenuItem In mnuCatalogo.Items
            Select Case x.Value
                Case "Emision"
                    If Not (mipage.FSNUser.EPTipo = TipoAccesoFSEP.SoloAprovisionador Or mipage.FSNUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador) Then
                        x.NavigateUrl = ConfigurationManager.AppSettings("rutaEP") & "PedidoAbierto.aspx"
                    Else
                        x.NavigateUrl = ConfigurationManager.AppSettings("rutaEP") & "CatalogoWeb.aspx"
                    End If
                Case "Seguimiento"
                    x.NavigateUrl = ConfigurationManager.AppSettings("rutaEP") & "Seguimiento.aspx"
                Case "Recepcion"
                    x.NavigateUrl = ConfigurationManager.AppSettings("rutaEP") & "Recepcion.aspx"
                Case "Aprobacion"
                    x.NavigateUrl = ConfigurationManager.AppSettings("rutaEP") & "Aprobacion.aspx"
            End Select
        Next

        'SM
        mnuCtrlPres.Attributes.Add("align", "right")
        'debajo de la pestaña Control presupuestario
        If mnuCtrlPres.Visible Then
            If Not mnuCtrlPres.FindItem("ControlPresupuestario") Is Nothing Then
                mnuCtrlPres.FindItem("ControlPresupuestario").NavigateUrl = ConfigurationManager.AppSettings("rutaSM") & "ControlGasto/ControlPresupuestario.aspx"
            End If
            If Not mnuCtrlPres.FindItem("Activos") Is Nothing Then
                mnuCtrlPres.FindItem("Activos").NavigateUrl = ConfigurationManager.AppSettings("rutaSM") & "activos/Activos.aspx"
            End If
        End If
        '1era linea de menu
        If panTabsCtrlPresup.Visible Then
            If Not fsnTabCtrlPres Is Nothing Then
                fsnTabCtrlPres.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaSM") & "ControlGasto/ControlPresupuestario.aspx" & "','_top'); return false;"
            End If
            If Not fsnTabCtrlPresActivos Is Nothing Then
                fsnTabCtrlPresActivos.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaSM") & "activos/Activos.aspx" & "','_top'); return false;"
            End If
        End If

        'Informes
        If mnuInformes.Items.Count = 0 Then
            fsnTabInformes.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_Common/Informes/Informes.aspx" & "','_top'); return false;"
        Else
            fsnTabInformes.OnClientClick = "return false;"
            mnuInformes.Attributes.Add("align", "right")
            For Each x As MenuItem In mnuInformes.Items
                Select Case x.Value
                    Case "Informes"
                        x.NavigateUrl = ConfigurationManager.AppSettings("rutaFS") & "_Common/Informes/Informes.aspx"
                    Case "BusinessInteligence"
                        x.NavigateUrl = ConfigurationManager.AppSettings("rutaFS") & "BI/BI.aspx"
                    Case "QlikViewer"
                        x.NavigateUrl = ConfigurationManager.AppSettings("rutaFS") & "BI/VisorQlik.aspx"
                    Case "Visor Actividad"
                        x.NavigateUrl = ConfigurationManager.AppSettings("rutaFS") & "BI/VisorActividad.aspx"
                    Case "Visor Rendimiento"
                        x.NavigateUrl = ConfigurationManager.AppSettings("rutaFS") & "BI/VisorRendimiento.aspx"
                    Case "Cubos"
                        x.NavigateUrl = ConfigurationManager.AppSettings("rutaFS") & "BI/Cubos.aspx"
                    Case "QlikApps"
                        x.NavigateUrl = ConfigurationManager.AppSettings("rutaFS") & "BI/QlikApps.aspx"
                End Select
            Next
        End If

        'Colaboración
        If Not fsnTabColaboracion Is Nothing Then
            fsnTabColaboracion.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaFS") & "cn/cn_MuroUsuario.aspx" & "','_top'); return false;"
        End If

        'Integracion
        fsnTabIntegracion.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaFS") & "INT/VisorIntegracion.aspx','_top'); return false;"

        'Facturación
        mnuFacturacion.Attributes.Add("align", "right")
        fsnTabFactAlta.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=13" & "','_top'); return false;"
        fsnTabFactSeguimiento.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=5" & "','_top'); return false;"
        fsnTabFactAutofacturacion.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Facturas/VisorFacturas.aspx" & "','_top'); return false;"
        For Each x As MenuItem In mnuFacturacion.Items
            Select Case x.Value
                Case "Alta"
                    x.NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=13"
                Case "Seguimiento"
                    x.NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=5"
                Case "Autofacturacion"
                    x.NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Facturas/VisorFacturas.aspx"
            End Select
        Next

        'Seguridad
        InicializarEstiloSubmenu(mnuSeguridad, "lnkmnuPerfilesSeguridad")
        mnuSeguridad.Attributes.Add("align", "right")
        mnuSeguridad.FindItem("PerfilesSeguridad").NavigateUrl = ConfigurationManager.AppSettings("rutaFS") & "SG/VisorPerfiles.aspx"
    End Sub
    ''' <summary>
    ''' Genera las funciones javascript para activar y desactivar las pesta�as
    ''' </summary>
    ''' <returns>C�digo javascript de las funciones</returns>
    ''' <remarks></remarks>
    Private Function ScriptActivarTabs() As String
        Dim sBrowser As String = ""
        Dim Version As Integer
        Dim arBrowsers As New ArrayList
        sBrowser = Request.Browser.Browser
        Version = Request.Browser.MajorVersion
        arBrowsers = Request.Browser.Browsers
        Dim sScript As String = "function activarTab(nombre, colorfondo, colorletra) {" & vbCrLf & _
            "var mtab = document.getElementById(nombre);" & vbCrLf & _
            "mtab.style.backgroundColor = colorfondo;" & vbCrLf & _
            "mtab.style.color = colorletra; " & vbCrLf
        If sBrowser <> "IE" Or Version < 7 Then
            sScript = sScript & "if (mtab.childNodes.length>2) {" & vbCrLf & _
                "mtab.childNodes[1].style.backgroundPosition = '0px -12px';" & vbCrLf & _
                "mtab.childNodes[2].style.backgroundPosition = '-6px -12px';" & vbCrLf & _
                "}" & vbCrLf
        Else
            sScript = sScript & "var tabla = mtab.childNodes[0];" & vbCrLf & _
                "try{" & vbCrLf & _
                "tabla.style.backgroundColor = colorfondo;" & vbCrLf & _
                "tabla.style.color = colorletra;" & vbCrLf & _
                "tabla.rows[0].cells[0].style.backgroundPosition = '0px -12px';" & vbCrLf & _
                "tabla.rows[0].cells[2].style.backgroundPosition = '-6px -12px';" & vbCrLf & _
                "}" & vbCrLf & "catch(e) {}" & vbCrLf
        End If
        sScript = sScript & "}" & vbCrLf & _
            "function desactivarTab(nombre, colorfondo, colorletra) {" & vbCrLf & _
            "try{" & vbCrLf & _
            "var mtab = document.getElementById(nombre);" & vbCrLf & _
            "mtab.style.backgroundColor = colorfondo;" & vbCrLf & _
            "mtab.style.color = colorletra;" & vbCrLf
        If sBrowser <> "IE" Or Version < 7 Then
            sScript = sScript & "if (mtab.childNodes.length>2) {" & vbCrLf & _
                "mtab.childNodes[1].style.backgroundPosition = '0px 0px';" & vbCrLf & _
                "mtab.childNodes[2].style.backgroundPosition = '-6px 0px';" & vbCrLf & _
                "}" & vbCrLf
        Else
            sScript = sScript & "var tabla = mtab.childNodes[0];" & vbCrLf & _
                "try{" & vbCrLf & _
                "tabla.style.backgroundColor = colorfondo;" & _
                "tabla.style.color = colorletra;" & vbCrLf & _
                "tabla.rows[0].cells[0].style.backgroundPosition = '0px 0px';" & vbCrLf & _
                "tabla.rows[0].cells[2].style.backgroundPosition = '-6px 0px';" & vbCrLf & _
                "}" & vbCrLf & "catch(e) {}" & vbCrLf
        End If
        sScript = sScript & "}" & vbCrLf & "catch(e) {}" & vbCrLf
        sScript = sScript & "}" & vbCrLf
        Return sScript
    End Function
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Genera las funciones javascript para el mouse over de las pesta�as
    ''' </summary>
    ''' <returns>C�digo javascript de las funciones</returns>
    ''' <remarks>Llamada desde: menu_load ; Tiempo m�ximo: 0</remarks>
    Private Function ScriptTabMouseOver() As String
        Dim sScript As String = "function TabMouseOver(tab){" & vbCrLf
        If fsnTabSeguridad.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabSeguridad, "seguridad", panMnuSeguridad)
        If fsnTabColaboracion.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabColaboracion, "colaboracion", Nothing)
        If fsnTabProcesos.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabProcesos, "procesos", panMnuProcesos)
        If fsnTabContratos.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabContratos, "contratos", panMnuContratos)
        If fsnTabCalidad.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCalidad, "calidad", panMnuCalidad)
        If fsnTabCalidadConfiguracion.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCalidadConfiguracion, "configuracion", panMnuCalidadConfiguracion)
        If fsnTabCatalogo.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCatalogo, "catalogo", panMnuCatalogo)
        If fsnTabCtrlPresup.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCtrlPresup, "ctrlpresup", panMnuCtrlPres)
        If fsnTabFacturacion.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabFacturacion, "facturacion", panMnuFacturacion)
        If fsnTabInformes.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabInformes, "informes", panMnuInformes)
        If fsnTabProcAlta.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabProcAlta, "procalta", Nothing)
        If fsnTabProcSeguimiento.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabProcSeguimiento, "procseguimiento", Nothing)
        If fsnTabProcParametros.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabProcParametros, "procparametros", Nothing)
        If fsnTabContrAlta.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabContrAlta, "contralta", Nothing)
        If fsnTabContrSeguimiento.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabContrSeguimiento, "contrseguimiento", Nothing)
        If fsnTabCalPanelProveedores.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCalPanelProveedores, "calpanelproveedores", Nothing)
        If fsnTabCalCertificados.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCalCertificados, "calcertificados", Nothing)
        If fsnTabCalNoConformidades.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCalNoConformidades, "calnoconformidades", Nothing)
        If fsnTabCalSolicitudesQA.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCalSolicitudesQA, "calsolicitudesqa", panMnuSolicitudesQA)
        If fsnTabCalEncuestas.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCalEncuestas, "calencuestas", panMnuEncuestas)
        If fsnTabCalPanelProveedores.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCalPanelProveedores, "calnotificaciones", Nothing)
        If fsnTabCatEmision.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCatEmision, "catemision", Nothing)
        If fsnTabCatSeguimiento.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCatSeguimiento, "catseguimiento", Nothing)
        If fsnTabCatRecepcion.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCatRecepcion, "catrecepcion", Nothing)
        If fsnTabCatAprobacion.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCatAprobacion, "cataprobacion", Nothing)
        If fsnTabCatParametros.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCatParametros, "catparametros", Nothing)
        If fsnTabCtrlPres.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCtrlPres, "ctrlpres", Nothing)
        If fsnTabCtrlPresActivos.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCtrlPresActivos, "ctrlpresactivos", Nothing)
        If fsnTabIntegracion.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabIntegracion, "integracion", Nothing)
        If fsnTabFactAlta.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabFactAlta, "factalta", Nothing)
        If fsnTabFactSeguimiento.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabFactSeguimiento, "factseguimiento", Nothing)
        If fsnTabFactAutofacturacion.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabFactAutofacturacion, "factautofacturacion", Nothing)
        sScript = sScript & " }"
        Return sScript
    End Function
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Genera las funciones javascript para el mouse out de las pesta�as
    ''' </summary>
    ''' <returns>C�digo javascript de las funciones</returns>
    ''' <remarks>Llamada desde: menu_load ; Tiempo m�ximo: 0</remarks>
    Private Function ScriptTabMouseOut() As String
        Dim sScript As String = "function TabsOut(){" & vbCrLf & _
            "window.clearTimeout(timeMenu);" & vbCrLf
        If fsnTabSeguridad.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabSeguridad, panMnuSeguridad)
        If fsnTabColaboracion.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabColaboracion, Nothing)
        If fsnTabProcesos.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabProcesos, panMnuProcesos)
        If fsnTabContratos.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabContratos, panMnuContratos)
        If fsnTabCalidad.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCalidad, panMnuCalidad)
        If fsnTabCalidadConfiguracion.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCalidadConfiguracion, panMnuCalidadConfiguracion)
        If fsnTabCatalogo.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCatalogo, panMnuCatalogo)
        If fsnTabCtrlPresup.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCtrlPresup, panMnuCtrlPres)
        If fsnTabFacturacion.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabFacturacion, panMnuFacturacion)
        If fsnTabInformes.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabInformes, panMnuInformes)
        If fsnTabProcAlta.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabProcAlta, Nothing)
        If fsnTabProcSeguimiento.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabProcSeguimiento, Nothing)
        If fsnTabProcParametros.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabProcParametros, Nothing)
        If fsnTabContrAlta.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabContrAlta, Nothing)
        If fsnTabContrSeguimiento.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabContrSeguimiento, Nothing)
        If fsnTabCalPanelProveedores.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCalPanelProveedores, Nothing)
        If fsnTabCalCertificados.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCalCertificados, Nothing)
        If fsnTabCalNoConformidades.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCalNoConformidades, Nothing)
        If fsnTabCalSolicitudesQA.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCalSolicitudesQA, panMnuSolicitudesQA)
        If fsnTabCalEncuestas.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCalEncuestas, panMnuEncuestas)
        If fsnTabCatEmision.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCatEmision, Nothing)
        If fsnTabCatSeguimiento.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCatSeguimiento, Nothing)
        If fsnTabCatRecepcion.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCatRecepcion, Nothing)
        If fsnTabCatParametros.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCatParametros, Nothing)
        If fsnTabCtrlPres.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCtrlPres, Nothing)
        If fsnTabCtrlPresActivos.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCtrlPresActivos, Nothing)
        If fsnTabIntegracion.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabIntegracion, Nothing)
        If fsnTabFactAlta.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabFactAlta, Nothing)
        If fsnTabFactSeguimiento.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabFactSeguimiento, Nothing)
        If fsnTabFactAutofacturacion.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabFactAutofacturacion, Nothing)
        sScript = sScript & "}"
        Return sScript
    End Function
    ''' <summary>
    ''' Iniciliza las pesta�as del men�
    ''' </summary>
    ''' <param name="TabControl">Pesta�a del men�</param>
    ''' <param name="Cod">C�digo</param>
    ''' <remarks>Llamada desde: InicializarControles; Tiempo m�ximo: 0 sg;</remarks>
    Private Sub InicializarTab(ByVal TabControl As FSNWebControls.FSNTab, ByVal Cod As String)
        Dim sValor As String = ""
        TabControl.Attributes.Add("onmouseover", "window.clearTimeout(timeMenu); TabMouseOver('" & Cod & "'); return false")
        TabControl.Attributes.Add("onmouseout", "timeMenu=window.setTimeout(TabsOut,1000); return false")
        If dPropiedades.TryGetValue("FSNTAB_ColorFondo", sValor) Then TabControl.ColorFondo = System.Drawing.Color.FromName(sValor)
        If dPropiedades.TryGetValue("FSNTAB_ColorFondoHover", sValor) Then TabControl.ColorFondoHover = System.Drawing.Color.FromName(sValor)
        If dPropiedades.TryGetValue("FSNTAB_ColorLetra", sValor) Then TabControl.ColorLetra = System.Drawing.Color.FromName(sValor)
        If dPropiedades.TryGetValue("FSNTAB_ColorLetraHover", sValor) Then TabControl.ColorLetraHover = System.Drawing.Color.FromName(sValor)
        If dPropiedades.TryGetValue("FSNTAB_ImagesBordes", sValor) Then TabControl.ImagenesBordes = sValor
    End Sub
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Inicializa el estilo de los submenus
    ''' </summary>
    ''' <param name="menu">Nombre del objeto menu que contendr� varios items</param>
    ''' <param name="sNombrePlantilla">Nombre de la plantilla que utilizan los items del menu</param>
    ''' <remarks>Llamada desde: InicializarControles; Tiempo m�ximo: 0 sg;</remarks>
    Private Sub InicializarEstiloSubmenu(ByVal menu As System.Web.UI.WebControls.Menu, ByVal sNombrePlantilla As String, Optional ByVal Dinamico As Boolean = False)
        Dim sValor As String = ""
        Dim Plantilla As String = sNombrePlantilla
        Dim PlantillaDin As String = sNombrePlantilla & "Dinamico"

        For i As Integer = 0 To menu.Controls.Count - 1
            If Dinamico AndAlso CType(menu.Controls(i).FindControl(sNombrePlantilla), FSNWebControls.FSNHyperlink) Is Nothing Then
                sNombrePlantilla = PlantillaDin
                'Para q el hover funcione correctamente hace falta esto , q no da nigun problema
                'PERO tambien hace falta DynamicHoverStyle y este IMPLICA poner runat server en el head de todas las paginas q usen menu.ascx
                '   SE decide q no haya HOVER para PMWeb por lo q FSNHYPER_ColorFondoDinamicHover sera white (pq si es submenu no se pone background) 
                '   para todos los bts dinamicos
                If Request.Browser.Browser = "IE" Then If dPropiedades.TryGetValue("FSNHYPER_ColorFondoDinamicHover", sValor) Then CType(menu.Controls(i).FindControl(sNombrePlantilla), FSNWebControls.FSNHyperlink).ColorFondoHover = System.Drawing.Color.FromName(sValor)
            Else
                sNombrePlantilla = Plantilla

                If dPropiedades.TryGetValue("FSNHYPER_ColorFondo", sValor) Then CType(menu.Controls(i).FindControl(sNombrePlantilla), FSNWebControls.FSNHyperlink).ColorFondo = System.Drawing.Color.FromName(sValor)
                If dPropiedades.TryGetValue("FSNHYPER_ColorFondoHover", sValor) Then CType(menu.Controls(i).FindControl(sNombrePlantilla), FSNWebControls.FSNHyperlink).ColorFondoHover = System.Drawing.Color.FromName(sValor)
                If dPropiedades.TryGetValue("FSNHYPER_ImagesBordes", sValor) Then CType(menu.Controls(i).FindControl(sNombrePlantilla), FSNWebControls.FSNHyperlink).ImagenesBordes = sValor
            End If

            sNombrePlantilla = Plantilla
        Next
    End Sub
    Private Function TabScriptMouseOver(ByVal TabControl As FSNWebControls.FSNTab, ByVal Cod As String, ByVal PanelControl As Panel) As String
        Dim sScript As String = "if (tab=='" & Cod & "'){" & vbCrLf & _
            " activarTab('" & TabControl.ClientID & "', '" & Drawing.ColorTranslator.ToHtml(TabControl.ColorFondoHover) & "', '" & Drawing.ColorTranslator.ToHtml(TabControl.ColorLetraHover) & "');" & vbCrLf
        If Not PanelControl Is Nothing Then
            sScript = sScript & " mostrarsubmenu('" & PanelControl.ClientID & "');" & vbCrLf
        End If
        sScript = sScript & " } else {" & vbCrLf & _
            " desactivarTab('" & TabControl.ClientID & "', '" & Drawing.ColorTranslator.ToHtml(TabControl.ColorFondo) & "', '" & Drawing.ColorTranslator.ToHtml(TabControl.ColorLetra) & "');" & vbCrLf
        If Not PanelControl Is Nothing Then
            sScript = sScript & " ocultarsubmenu('" & PanelControl.ClientID & "');" & vbCrLf
        End If
        sScript = sScript & " }" & vbCrLf
        Return sScript
    End Function
    Private Function TabScriptMouseOut(ByVal TabControl As FSNWebControls.FSNTab, ByVal PanelControl As Panel) As String
        Dim sScript As String
        If TabControl.Selected Then
            sScript = "activarTab('" & TabControl.ClientID & "', '" & Drawing.ColorTranslator.ToHtml(TabControl.ColorFondoHover) & "', '" & Drawing.ColorTranslator.ToHtml(TabControl.ColorLetraHover) & "');" & vbCrLf
            If Not PanelControl Is Nothing Then _
                sScript = sScript & "mostrarsubmenu('" & PanelControl.ClientID & "');" & vbCrLf
        Else
            sScript = "desactivarTab('" & TabControl.ClientID & "', '" & Drawing.ColorTranslator.ToHtml(TabControl.ColorFondo) & "', '" & Drawing.ColorTranslator.ToHtml(TabControl.ColorLetra) & "');" & vbCrLf
            If Not PanelControl Is Nothing Then _
            sScript = sScript & "ocultarsubmenu('" & PanelControl.ClientID & "');" & vbCrLf
        End If
        Return sScript
    End Function
    ''' <summary>
    ''' Muestra la pesta�a de informes dependiendo de si el usuario ve o no alg�n informe
    ''' </summary>
    ''' <param name="bAccesoFSEP">Si tiene acceso al producto EP</param>
    ''' <param name="bAccesoFSPM">Si tiene acceso al producto PM</param>
    ''' <param name="bAccesoFSQA">Si tiene acceso al producto QA</param>
    ''' <param name="oUser">Objeto usuario</param>
    ''' <param name="ParamGen">Los par�metros generales</param>
    ''' <returns>Si hay que mostrar o no la pesta�a de Informes</returns>
    ''' <remarks>LLamada desde: Pagina_Load; Tiempo m�ximo: 0,1 sg;</remarks>
    Private Function MostrarInformes(ByVal bAccesoFSEP As Boolean, ByVal bAccesoFSPM As Boolean, ByVal bAccesoFSQA As Boolean, ByVal oUser As Object, ByVal ParamGen As FSNLibrary.TiposDeDatos.ParametrosGenerales) As Boolean
        Dim bMostrarInformes As Boolean = False
        If bAccesoFSEP Then 'en EP hay informes. Si tiene acceso a EP se tiene que mostrar la pesta�a Informes
            bMostrarInformes = True 'no hace falta comprobar que tiene acceso a otras aplicaciones.
        Else
            Dim oListados As FSNServer.Listados = mipage.FSNServer.Get_Object(GetType(FSNServer.Listados))
            If bAccesoFSPM And bAccesoFSQA Then
                oListados.LoadListadosPersonalizados(oUser.Cod, True, True)
                If oListados.Data.Tables(0).Rows.Count > 0 Or oListados.Data.Tables(1).Rows.Count > 0 Then
                    bMostrarInformes = True
                End If
            Else
                If mipage.Acceso.gsAccesoFSPM <> FSNLibrary.TipoAccesoFSPM.SinAcceso And mipage.FSNUser.AccesoPM = True Then
                    oListados.LoadListadosPersonalizados(mipage.FSNUser.Cod, True, False)
                ElseIf mipage.Acceso.gsAccesoFSQA <> FSNLibrary.TipoAccesoFSQA.SinAcceso And mipage.FSNUser.AccesoQA = True Then
                    oListados.LoadListadosPersonalizados(mipage.FSNUser.Cod, False, True)
                End If
                If Not oListados.Data Is Nothing Then
                    If oListados.Data.Tables(0).Rows.Count > 0 Then
                        bMostrarInformes = True
                    End If
                End If
            End If
        End If
        Return bMostrarInformes
    End Function
    ''' Revisado por: Jbg. Fecha: 08/11/2011
    ''' <summary>
    ''' Carga los textos del menu y submenu
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load; Tiempo m�ximo: 0 sg;</remarks>
    Private Sub CargarTextos()
        Dim modulo As TiposDeDatos.ModulosIdiomas = mipage.ModuloIdioma

        fsnTabProcesos.Text = mipage.Textos(8)
        fsnTabContratos.Text = mipage.Textos(33)
        fsnTabCalidad.Text = mipage.Textos(9)
        fsnTabCalidadConfiguracion.Text = mipage.Textos(42)
        fsnTabCatalogo.Text = mipage.Textos(21)
        fsnTabProcAlta.Text = mipage.Textos(1)
        mnuProcesos.Items(0).Text = mipage.Textos(1)
        fsnTabProcSeguimiento.Text = mipage.Textos(2)
        mnuProcesos.Items(1).Text = mipage.Textos(2)
        fsnTabProcParametros.Text = mipage.Textos(14)
        mnuProcesos.Items(2).Text = mipage.Textos(14)

        fsnTabInformes.Text = mipage.Textos(25)
        mnuInformes.FindItem("Informes").Text = mipage.Textos(67)
        mnuInformes.FindItem("BusinessInteligence").Text = mipage.Textos(73)
        mnuInformes.FindItem("Cubos").Text = mipage.Textos(74)
        mnuInformes.FindItem("Visor Actividad").Text = mipage.Textos(70)
        mnuInformes.FindItem("Visor Rendimiento").Text = mipage.Textos(71)
        mnuInformes.FindItem("QlikViewer").Text = mipage.Textos(75)
        mnuInformes.FindItem("QlikApps").Text = mipage.Textos(76)

        fsnTabContrAlta.Text = mipage.Textos(62)
        mnuContratos.Items(0).Text = mipage.Textos(62)
        fsnTabContrSeguimiento.Text = mipage.Textos(2)
        mnuContratos.Items(1).Text = mipage.Textos(2)

        fsnTabCalPanelEscalacionProveedores.Text = mipage.Textos(79)
        mnuCalidad.FindItem("PanelEscalacionProveedores").Text = mipage.Textos(79)
        fsnTabCalPanelProveedores.Text = mipage.Textos(12)
        mnuCalidad.FindItem("PanelProveedores").Text = mipage.Textos(12)
        mnuCalidad.FindItem("Configuracion").Text = mipage.Textos(42)
        mnuCalidadConfiguracion.FindItem("Materiales").Text = mipage.Textos(13)
        mnuCalidad.FindItem("Configuracion/Materiales").Text = mipage.Textos(13)
        fsnTabCalCertificados.Text = mipage.Textos(11)
        mnuCalidad.FindItem("Certificados").Text = mipage.Textos(11)
        fsnTabCalNoConformidades.Text = mipage.Textos(10)
        mnuCalidad.FindItem("NoConformidades").Text = mipage.Textos(10)
        If mipage.FSNUser.QAMantenimientoUNQA Then
            mnuCalidadConfiguracion.FindItem("UnidadesNegocio").Text = mipage.Textos(18)
            mnuCalidad.FindItem("Configuracion/UnidadesNegocio").Text = mipage.Textos(18)
        ElseIf mipage.FSNUser.QAMantenimientoProv Then
            mnuCalidadConfiguracion.FindItem("UnidadesNegocio").Text = mipage.Textos(32)
            mnuCalidad.FindItem("Configuracion/UnidadesNegocio").Text = mipage.Textos(32)
        End If
        If mipage.FSNUser.QAPermitirMantNivelesEscalacion Then
            mnuCalidadConfiguracion.FindItem("Escalacion").Text = mipage.Textos(78)
            mnuCalidad.FindItem("Configuracion/Escalacion").Text = mipage.Textos(78)
        End If
        mnuCalidadConfiguracion.FindItem("Parametros").Text = mipage.Textos(14)
        mnuCalidad.FindItem("Configuracion/Parametros").Text = mipage.Textos(14)
        mnuCalidad.FindItem("SolicitudesQA").Text = mipage.Textos(82)
        mnuCalidad.FindItem("SolicitudesQA/SolicitudesQAAlta").Text = mipage.Textos(84)
        mnuCalidad.FindItem("SolicitudesQA/SolicitudesQASeguimiento").Text = mipage.Textos(85)
        mnuCalidad.FindItem("Encuestas").Text = mipage.Textos(83)
        mnuCalidad.FindItem("Encuestas/EncuestasAlta").Text = mipage.Textos(84)
        mnuCalidad.FindItem("Encuestas/EncuestasSeguimiento").Text = mipage.Textos(85)

        fsnTabCalSolicitudesQA.Text = mipage.Textos(82)
        If mipage.FSNUser.EsPeticionarioSolicitudesQA Then mnuSolicitudesQA.FindItem("SolicitudesQAAlta").Text = mipage.Textos(84)
        If mipage.FSNUser.EsParticipanteSolicitudesQA Then mnuSolicitudesQA.FindItem("SolicitudesQASeguimiento").Text = mipage.Textos(85)
        fsnTabCalEncuestas.Text = mipage.Textos(83)
        If mipage.FSNUser.EsPeticionarioEncuestas Then mnuEncuestas.FindItem("EncuestasAlta").Text = mipage.Textos(84)
        If mipage.FSNUser.EsParticipanteEncuestas Then mnuEncuestas.FindItem("EncuestasSeguimiento").Text = mipage.Textos(85)
        fsnTabCalNotificaciones.Text = mipage.Textos(41)

        fsnTabCatEmision.Text = mipage.Textos(22)
        mnuCatalogo.Items(0).Text = mipage.Textos(22)
        fsnTabCatSeguimiento.Text = mipage.Textos(2)
        mnuCatalogo.Items(1).Text = mipage.Textos(2)
        fsnTabCatRecepcion.Text = mipage.Textos(23)
        mnuCatalogo.Items(2).Text = mipage.Textos(23)
        fsnTabCatAprobacion.Text = mipage.Textos(3)
        mnuCatalogo.Items(3).Text = mipage.Textos(3)
        fsnTabCatParametros.Text = mipage.Textos(14)
        mnuCatalogo.Items(4).Text = mipage.Textos(14)

        fsnTabCtrlPresup.Text = mipage.Textos(40)

        fsnTabColaboracion.Text = mipage.Textos(43)

        fsnTabCtrlPres.Text = mipage.Textos(24) '"Control Presupuestario"
        mnuCtrlPres.Items(0).Text = mipage.Textos(24) '"Control Presupuestario"
        mnuCtrlPres.Items(1).Text = mipage.Textos(31)

        fsnTabIntegracion.Text = mipage.Textos(60)
        fsnTabFacturacion.Text = mipage.Textos(61)
        mnuFacturacion.FindItem("Alta").Text = mipage.Textos(80)
        mnuFacturacion.FindItem("Seguimiento").Text = mipage.Textos(2)
        mnuFacturacion.FindItem("Autofacturacion").Text = mipage.Textos(81)

        fsnTabFactAlta.Text = mipage.Textos(80)
        fsnTabFactSeguimiento.Text = mipage.Textos(2)
        fsnTabFactAutofacturacion.Text = mipage.Textos(81)

        fsnTabSeguridad.Text = mipage.Textos(63)
        mnuSeguridad.FindItem("PerfilesSeguridad").Text = mipage.Textos(64)
        'El tab de integracion muestra un icono y un tooltip en caso de parada del servicio de integracion
        Dim UrlImagen As String
        Dim dtFechaParada As DateTime = Nothing
        Dim oInt As FSNServer.VisorInt = mipage.FSNServer.Get_Object(GetType(FSNServer.VisorInt))
        If oInt.HayParadaServicioInt(dtFechaParada) Then
            UrlImagen = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/alert-rojo-mini.png"
            fsnTabIntegracion.Text = "<img src='" & UrlImagen & "'  style='text-align:center;' border=0/> " & fsnTabIntegracion.Text
            mipage.ModuloIdioma = ModulosIdiomas.VisorIntegracion
            fsnTabIntegracion.ToolTip = mipage.Textos(73) & vbCrLf & mipage.Textos(74)
            If Not IsNothing(dtFechaParada) Then
                fsnTabIntegracion.ToolTip = fsnTabIntegracion.ToolTip & Format(dtFechaParada, mipage.FSNUser.DateFormat.ShortDatePattern) & " " & Format(dtFechaParada, mipage.FSNUser.DateFormat.ShortTimePattern)
            End If
        End If

        mipage.ModuloIdioma = modulo
    End Sub
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Seleccionar opciones men�
    ''' </summary>
    ''' <param name="Pestania">Pesta�a a seleccionar</param>
    ''' <param name="Opcion">Opci�n del submenu a seleccionar</param>
    ''' <remarks>LLamada desde: Page_Load; Tiempo m�ximo: 0 sg;</remarks>
    Public Sub Seleccionar(ByVal Pestania As String, ByVal Opcion As String)
        fsnTabProcesos.Selected = False
        fsnTabContratos.Selected = False
        fsnTabCalidad.Selected = False
        fsnTabCalidadConfiguracion.Selected = False
        fsnTabCalSolicitudesQA.Selected = False
        fsnTabCalEncuestas.Selected = False
        fsnTabCatalogo.Selected = False
        fsnTabCtrlPresup.Selected = False
        fsnTabInformes.Selected = False
        fsnTabIntegracion.Selected = False
        fsnTabFacturacion.Selected = False
        fsnTabSeguridad.Selected = False

        Select Case Pestania
            Case "Procesos"
                fsnTabProcesos.Selected = True
                mnuProcesos.Visible = True
                If Opcion <> Nothing Then
                    mnuProcesos.FindItem(Opcion).Selected = True
                    Select Case Opcion
                        Case "AltaSolicitudes"
                            fsnTabProcAlta.Selected = True
                        Case "Seguimiento"
                            fsnTabProcSeguimiento.Selected = True
                        Case "Parametros"
                            fsnTabProcParametros.Selected = True
                    End Select
                End If
            Case "Contratos"
                fsnTabContratos.Selected = True
                mnuContratos.Visible = True
                If Opcion <> Nothing Then
                    mnuContratos.FindItem(Opcion).Selected = True
                    Select Case Opcion
                        Case "AltaContratos"
                            fsnTabContrAlta.Selected = True
                        Case "Seguimiento"
                            fsnTabContrSeguimiento.Selected = True
                    End Select
                End If
            Case "Calidad"
                fsnTabCalidad.Selected = True
                mnuCalidad.Visible = True
                If Opcion <> Nothing Then
                    mnuCalidad.FindItem(Opcion).Selected = True
                    Select Case Opcion
                        Case "PanelProveedores"
                            fsnTabCalPanelProveedores.Selected = True
                        Case "Certificados"
                            fsnTabCalCertificados.Selected = True
                        Case "NoConformidades"
                            fsnTabCalNoConformidades.Selected = True
                        Case "SolicitudesQA"
                            fsnTabCalSolicitudesQA.Selected = True
                        Case "Encuestas"
                            fsnTabCalEncuestas.Selected = True
                        Case "PanelEscalacionProveedores"
                            fsnTabCalPanelEscalacionProveedores.Selected = True
                    End Select
                End If
            Case "Configuracion"
                fsnTabCalidadConfiguracion.Selected = True
                fsnTabCalidad.Selected = True
                mnuCalidad.Visible = True
                If Opcion <> Nothing Then
                    If mnuCalidad.FindItem("Configuracion") IsNot Nothing AndAlso mnuCalidad.FindItem("Configuracion").ChildItems.Count = 0 Then
                        mnuCalidad.FindItem("Configuracion").Selected = True
                    Else
                        mnuCalidadConfiguracion.FindItem(Opcion).Selected = True
                        Select Case Opcion
                            Case "Materiales"
                                mnuCalidad.FindItem("Configuracion/Materiales").Selected = True
                            Case ("UnidadesNegocio")
                                mnuCalidad.FindItem("Configuracion/UnidadesNegocio").Selected = True
                            Case "Parametros"
                                mnuCalidad.FindItem("Configuracion/Parametros").Selected = True
                        End Select
                    End If
                End If
            Case "Catalogo"
                fsnTabCatalogo.Selected = True
                mnuCatalogo.Visible = True
                If Opcion <> Nothing Then
                    mnuCatalogo.FindItem(Opcion).Selected = True
                    Select Case Opcion
                        Case "Emision"
                            fsnTabCatEmision.Selected = True
                        Case "Seguimiento"
                            fsnTabCatSeguimiento.Selected = True
                        Case "Recepcion"
                            fsnTabCatRecepcion.Selected = True
                        Case "Aprobacion"
                            fsnTabCatAprobacion.Selected = True
                        Case "Parametros"
                            fsnTabCatParametros.Selected = True
                    End Select
                End If
            Case "CtrlPresup"
                fsnTabCtrlPresup.Selected = True
                mnuCtrlPres.Visible = True
                Select Case Opcion
                    Case "Activos"
                        mnuCtrlPres.FindItem(Opcion).Selected = True
                        fsnTabCtrlPresActivos.Selected = True
                End Select
            Case "Informes"
                fsnTabInformes.Selected = True
            Case "Integracion"
                fsnTabIntegracion.Selected = True
            Case "Facturacion"
                fsnTabFacturacion.Selected = True
                mnuFacturacion.Visible = True
                mnuFacturacion.FindItem(Opcion).Selected = True
                Select Case Opcion
                    Case "Alta"
                        fsnTabFactAlta.Selected = True
                    Case "Seguimiento"
                        fsnTabFactSeguimiento.Selected = True
                    Case "Autofacturacion"
                        fsnTabFactAutofacturacion.Selected = True
                End Select
            Case "Seguridad"
                fsnTabSeguridad.Selected = True
                mnuSeguridad.Visible = True
                mnuSeguridad.FindItem(Opcion).Selected = True
        End Select
    End Sub
    ''' <summary>
    ''' Lee un fichero txt que contiene las propiedades definidas para ese cliente de los controls FSNtab y FSNHyperLink
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load; Tiempo m�ximo: 0,1 sg;</remarks>
    Private Sub LeerFicheroTXT()
        Dim sLinea As String
        Dim sNombreFichero As String = Server.MapPath("~") & "\App_Themes\" & theme & "\" & theme & ".txt"

        If File.Exists(sNombreFichero) Then
            Dim oRead As StreamReader = File.OpenText(sNombreFichero)
            Dim sKey As String
            Dim sValue As String

            Do While oRead.Peek() >= 0
                sLinea = CType(oRead.ReadLine(), String)
                If InStr(sLinea, "=") > 0 Then
                    sKey = Mid(sLinea, 1, InStr(sLinea, "=") - 1)
                    sValue = Right(sLinea, Len(sLinea) - InStr(sLinea, "="))
                    sValue = sValue.Replace("""", "")
                    dPropiedades.Add(sKey, sValue)
                End If
            Loop
            oRead.Close()
            oRead = Nothing
        End If
    End Sub
End Class
