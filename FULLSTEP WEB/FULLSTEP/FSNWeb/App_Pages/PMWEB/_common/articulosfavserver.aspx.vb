﻿Public Class articulosfavserver
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ProvFavServer

        If Not Me.IsPostBack Then
            chkMostrarMat.Text = Textos(0)
            If FSNUser.PMMostrarDefArticuloMat Then
                Me.chkMostrarMat.Checked = True
            Else
                Me.chkMostrarMat.Checked = False
            End If
        End If

        Dim oArticulosFavoritos As FSNServer.ArticulosFavoritos

        Dim sGMN1 As String = ""
        Dim sGMN2 As String = ""
        Dim sGMN3 As String = ""
        Dim sGMN4 As String = ""
        Dim sUON1 As String = ""
        Dim sUON2 As String = ""
        Dim sUON3 As String = ""

        Dim ilGMN1 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN1
        Dim ilGMN2 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN2
        Dim ilGMN3 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN3
        Dim ilGMN4 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN4

        Dim sMat As String = Request("Material")
        Dim sOrgCompras As String = Request("OrgCompras")
        Dim sCentro As String = Request("Centro")
        Dim sUONs As String = Request("UnidadOrganizativa")
        Dim sMatOrig As String = sMat
        Dim sProveSumiArt As String = Request("ProveSumiArt")

        Dim arrMat(4) As String
        Dim lLongCod As Integer
        Dim i As Integer
        ''Solo sacamos el material en caso de que el check esté marcado
        If chkMostrarMat.Checked Then
            For i = 1 To 4
                arrMat(i) = ""
            Next i
            i = 1
            While Trim(sMat) <> ""
                Select Case i
                    Case 1
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                    Case 2
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                    Case 3
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                    Case 4
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
                End Select
                arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
                sMat = Mid(sMat, lLongCod + 1)
                i = i + 1
            End While
            sGMN1 = arrMat(1)
            sGMN2 = arrMat(2)
            sGMN3 = arrMat(3)
            sGMN4 = arrMat(4)
        End If

        Dim sUon = New String() {"", "", ""}
        Dim j As Integer
        If sUONs <> "" Then
            Dim temp() As String = Split(sUONs, "-")
            For j = 0 To temp.Length - 1
                sUon(j) = Trim(temp(j))
            Next
        End If
        sUON1 = sUon(0)
        sUON2 = sUon(1)
        sUON3 = sUon(2)

        oArticulosFavoritos = FSNServer.Get_Object(GetType(FSNServer.ArticulosFavoritos))
        oArticulosFavoritos.LoadData(FSNUser.Cod, sGMN1, sGMN2, sGMN3, sGMN4, sOrgCompras, sCentro, sUON1, sUON2, sUON3, sProveSumiArt, chkMostrarMat.Checked)
        Dim oGrid As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
        oGrid = Me.uwgArticuloFav

        oGrid.DataSource = oArticulosFavoritos.Data
        oGrid.DataBind()
        If oGrid.Bands.Count = 0 Then
            oGrid.Bands.Add(New Infragistics.WebUI.UltraWebGrid.UltraGridBand)
        End If
        Me.uwgArticuloFav.Columns.FromKey("COD").Hidden = True
        Me.uwgArticuloFav.Columns.FromKey("DEN").Hidden = True

    End Sub
    Private Sub chkMostrarMat_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMostrarMat.CheckedChanged
        FSNUser.Actualizar_MostrarArticuloFavMat(chkMostrarMat.Checked)
        FSNUser.PMMostrarDefArticuloMat = chkMostrarMat.Checked
    End Sub
    Private Sub uwgArticuloFav_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgArticuloFav.InitializeRow
        e.Row.Cells.FromKey("CODDEN").Value = "<a class='aPMWeb' onclick=""SeleccionarArticulo('" & e.Row.Cells.FromKey("COD").Value & "','" & e.Row.Cells.FromKey("DEN").Value & "');return false;"">" & e.Row.Cells.FromKey("CODDEN").Value & "</a>"
    End Sub

End Class