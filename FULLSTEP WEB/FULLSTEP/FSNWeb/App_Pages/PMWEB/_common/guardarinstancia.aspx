<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="guardarinstancia.aspx.vb" Inherits="Fullstep.FSNWeb.guardarinstancia" EnableViewState="True" %>
<%@ Register TagPrefix="igtbl" Namespace="Infragistics.WebUI.UltraWebGrid" Assembly="Infragistics.WebUI.UltraWebGrid.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="uc1" TagName="menu" Src="menu.ascx" %>
<%@ Register TagPrefix="fsde" Namespace="Fullstep.DataEntry" Assembly="DataEntry" %>
<%@ Register TagPrefix="igtab" Namespace="Infragistics.WebUI.UltraWebTab" Assembly="Infragistics.WebUI.UltraWebTab.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
	<title></title>
	<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
	<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
	<meta content="JavaScript" name="vs_defaultClientScript"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
</head>
<!--JIM: colocamos esta porci�n de c�digo antes de la etiqueta de men�. menu.js realiza override de window.open, y si la colocamos
    despu�s no se ejecuta este c�digo -->
<% If Not String.IsNullOrEmpty(Request("ConfiguracionGS")) AndAlso Request("ConfiguracionGS") = "1" Then%>
<script type="text/javascript">
    window.open_ = window.open;
    window.open = function (url, name, props) {
        if (url.toLowerCase().search("sessionid") > 0)
            return window.open_(url, name, props);
        else
            return window.open_(url + '&SessionId=<%=Session("sSession")%>', name, props);
    }
</script>
<%End If%>
<script type="text/javascript" language="javascript">
    //Muestra el detalle del tipo de solicitud
    function DetalleTipoSolicitud(IdSolicitud) {
        var newWindow = window.open("../alta/solicitudPMWEB.aspx?Solicitud=" + IdSolicitud, "_blank", "width=700,height=450,status=no,resizable=no,top=100,left=100");
        return false;
    }

    //Monta el formulario para mostrar la ventana de avisos con las precondiciones
    function montarFormularioAvisos() {
        if (!document.forms["frmAvisos"])
            document.body.insertAdjacentHTML("beforeEnd", "<form name='frmAvisos' action='../workflow/avisosprecondiciones.aspx' target='ventanaAvisos' id='frmSubmit' method='post' style='visibility:hidden;position:absolute;top:0;left:0'></form>")
        else
            document.forms["frmAvisos"].innerHTML = ""

        var oLabel;
        var i = 1;
        var bSalir = false;

        while (bSalir == false) {
            if (document.getElementById("Aviso" + i)) {
                document.getElementById("Aviso" + i).style.visibility = 'visible';
                oLabel = document.getElementById("Aviso" + i);
                anyadirInput(oLabel.name, oLabel.value);
                i = i + 1;
            } else {
                bSalir = true;
            }
        }
    }

    //A�ade el inpuy tipo hidden necesario con el nombre y el valor pasados como par�metros
    //Par�metros de entrada:
    //  sInput: nombre del input que se utilizar� para recoger el valor
    //  valor: valor del input
    //Llamada desde: montarFormularioAvisos
    function anyadirInput(sInput, valor) {
        if ((valor == "null") || (valor == null)) valor = ""

        var f = document.forms["frmAvisos"]
        if (!f.elements[sInput]) {
            f.insertAdjacentHTML("beforeEnd", "<INPUT type=hidden name=" + sInput + ">\n")
            f.elements[sInput].value = valor
        }
    }

    //Abre la ventana con las precondiciones que no cumple			
    function enviarAvisos() {
        var newWindow = window.open('../workflow/avisosprecondiciones.aspx', 'ventanaAvisos', 'height=200,width=400,status=yes,toolbar=no,menubar=no,location=no');
        document.forms['frmAvisos'].submit();
    }

    function FinMapper(MapperStr) {
        alert(MapperStr);
    }

    function validarLength(e, l) {
        if (e.value.length > l)
            return false
        else
            return true
    }

    function textCounter(e, l) {
        if (e.value.length > l)
            e.value = e.value.substring(0, l);
    }

    //Muestra el detalle del peticionario
    function BuscarPeticionario() {
        var Cod = document.forms["Form1"].elements["txtPeticionario"].value
        if (Cod == "")
            Cod = document.forms["Form1"].elements["txtPeticionarioDevolucion"].value
        if (Cod == "")
            Cod = document.forms["Form1"].elements["txtPeticionarioTraslado"].value
        if (Cod == "")
            Cod = document.forms["Form1"].elements["txtPeticionarioTrasladoUsu"].value
        var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detallepersona.aspx?CodPersona=" + Cod.toString(), "_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
    }

    function usu_seleccionado(sPerCod, sNombre, rolId) {
        var row;
        var oGrid = igtbl_getGridById("ugtxtRoles")
        for (i = 0; i <= oGrid.Rows.length - 1; i++) {
            row = oGrid.Rows.getRow(i)
            if (row.getCell(0).getValue() == rolId)
                break;
        }

        row.getCell(2).setValue(sPerCod)
        row.getCell(4).setValue(sNombre)

        //Cambia el estilo porque ya hay usuario seleccionado:
        var cell = row.getCell(4)
        var elem = cell.Element
        if (elem.textContent)
            elem.textContent = sNombre
        elem.style.color = "black"
    }

    function prove_seleccionado3(rolId, sProveCod, sProveDen, lConID, sConEmail) {
        var row;
        var oGrid = igtbl_getGridById("ugtxtRoles")
        for (i = 0; i <= oGrid.Rows.length - 1; i++) {
            row = oGrid.Rows.getRow(i)
            if (row.getCell(0).getValue() == rolId)
                break;
        }

        //var row = igtbl_getRowById(cellId)

        row.getCell(3).setValue(sProveCod)
        row.getCell(11).setValue(lConID)

        if (sConEmail == "")
            row.getCell(4).setValue(sProveDen)
        else
            row.getCell(4).setValue(sProveDen + ' (' + sConEmail + ')')

        //Cambia el estilo porque ya hay proveedor seleccionado:
        var cell = row.getCell(4)
        var elem = cell.Element
        if (elem.textContent) {
            if (sConEmail == "")
                elem.textContent = sProveDen
            else
                elem.textContent = sProveDen + ' (' + sConEmail + ')'
        }
        elem.style.color = "black"
    }

    //Deshabilita los botones para que el usuario no pueda pulsarlos, mientras se realiza una acci�n
    function DeshabilitarBotones() {
        var frmForm1Elements = document.forms["Form1"].elements;
        if (frmForm1Elements["cmdAceptar"])
            frmForm1Elements["cmdAceptar"].disabled = true;
        if (frmForm1Elements["cmdCancelar"])
            frmForm1Elements["cmdCancelar"].disabled = true;

    }

    //Muestra el frame donde se ha cargado guardarinstancia.aspx
    function MostrarFSWSServer() {
        parent.document.getElementById('fraWS').rows = "0,0,*";
    }

    //Muestra el frame principal donde est� cargada la p�gina con la solicitud (NWAlta, NWDetalleSolicitud, GestionInstancia,...)
    function MostrarFSWSMain() {
        parent.document.getElementById('fraWS').rows = "0,*,0";
    }

    function AbrirEnfraWSMain(pagina) {
        window.open(pagina, "fraWSMain");
        MostrarFSWSMain();
    }

    //Ha ido bien la acci�n, la devoluci�n o el traslado, y volvemos al alta de solicitudes o al seguimiento
    function TodoOK(bAlta, bContrato, configuracionGS, rutaVolver) {
        if (bContrato) {
            if (configuracionGS == 1) {
                window.open("<%=System.Configuration.ConfigurationManager.AppSettings("rutaPM2008")%>contratos/VisorContratos_prev.aspx?desdeGS=1", "_top");
            } else {
                if (bAlta) {
                    if (rutaVolver != '') //Si viene de un webpart ira a Inicio.aspx
                        window.open(rutaVolver, "_top");
                    else
                        window.open("<%=System.Configuration.ConfigurationManager.AppSettings("rutaPM2008")%>contratos/AltaContratos.aspx", "_top");
                } else {
                    if (rutaVolver != '') //Si viene de un webpart ira a Inicio.aspx
                        window.open(rutaVolver, "_top");
                    else
                        window.open("<%=System.Configuration.ConfigurationManager.AppSettings("rutaPM2008")%>contratos/VisorContratos.aspx", "_top");
                }
            }
        } else {
            if (rutaVolver != '') { //Si viene de un webpart ira a Inicio.aspx
                window.open(rutaVolver, "_top");
            } else {
                if (document.getElementById("hTipoVisor").value == 2) {
                    window.open("<%=System.Configuration.ConfigurationManager.AppSettings("rutaQA2008")%>NoConformidades/NoConformidades.aspx", "_top");
                }else{
                    if (document.getElementById("hTipoVisor").value == 1)
                        window.open("<%=System.Configuration.ConfigurationManager.AppSettings("rutaPM2008")%>Tareas/VisorSolicitudes.aspx", "_top");
                    else
                        window.open("<%=System.Configuration.ConfigurationManager.AppSettings("rutaPM2008")%>Tareas/VisorSolicitudes.aspx?TipoVisor=" + document.getElementById("hTipoVisor").value, "_top");
                }
            }
        }
    }

    function OcultarDivs() {
        var divDevolverSiguientesEtapas = document.getElementById("divDevolverSiguientesEtapas")
        divDevolverSiguientesEtapas.style.display = 'none';
        var divDevolucion = document.getElementById("divDevolucion")
        divDevolucion.style.display = 'none';
        var divTraslado = document.getElementById("divTraslado")
        divTraslado.style.display = 'none'
        var divCabecera = document.getElementById("divCabecera")
        if (divCabecera)
            divCabecera.style.display = 'none'
    }

    //Muestra el detalle de la persona
    //Par�metro de entrada: Cod de la persona
    function VerDetallePersona(Per) {
        var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detallepersona.aspx?CodPersona=" + Per.toString(), "_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");

        }

        //Muestra el detalle del proveedor
        //Par�metro de entrada: Cod del proveedor
        function VerDetalleProveedor(Prove) {
            var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detalleproveedor.aspx?codProv=" + Prove.toString(), "_blank", "width=500,height=200,status=yes,resizable=no,top=200,left=250");

	}

	//Abre el buscador de usuarios para elegir el destinatario de la acci�n
	//Par�metros de entrada:    Rol: Id del rol
	//En el par�metro FilaGrid le pasamos ahora el id del rol para encontrar la fila en la que estamos asignando el usuario
	function BuscadorUsuarios(Rol) {
	    if (document.getElementById("Version").value == "0")
	        var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>_Common/BuscadorUsuarios.aspx?Rol=" + Rol + "&idFilaGrid=" + Rol, "_blank", "width=750,height=475,status=yes,resizable=no,top=200,left=200");
	    else
	        newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>_Common/BuscadorUsuarios.aspx?Rol=" + Rol + "&Instancia=" + document.getElementById("Instancia").value + "&idFilaGrid=" + Rol, "_blank", "width=750,height=475,status=yes,resizable=no,top=200,left=200");

    }

    //Abre el buscador de proveedores para elegir el destinatario de la acci�n
    //Par�metros de entrada:    Rol: Id del rol
    //En el par�metro FilaGrid le pasamos ahora el id del rol para encontrar la fila en la que estamos asignando el proveedor
    function BuscadorProveedores(Rol) {
        if (document.getElementById("Version").value == "0")
            var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>_Common/BuscadorProveedores.aspx?PM=true&Rol=" + Rol + "&IDControl=ugtxtRoles&idFilaGrid=" + Rol + "&ComboContactos=true", "_blank", "width=835,height=635,status=yes,resizable=yes,top=0,left=150,scrollbars=yes");
        else
            newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>_Common/BuscadorProveedores.aspx?PM=true&Rol=" + Rol + "&Instancia=" + document.getElementById("Instancia").value + "&IDControl=ugtxtRoles&idFilaGrid=" + Rol + "&ComboContactos=true", "_blank", "width=835,height=635,status=yes,resizable=yes,top=0,left=150,scrollbars=yes");

    }

    /*''' <summary>
    ''' Responder al evento click en una celda del grid
    ''' </summary>
    ''' <param name="gridName">Nombre del grid</param>
    ''' <param name="cellId">Id de la celda</param>        
    ''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>*/
    function ugtxtRoles_ClickCellButtonHandler(gridName, cellId) {
        var row = igtbl_getRowById(cellId);
        var cell

        cell = row.getCell(0)
        var Rol = cell.getValue()
        cell = row.getCell(2)
        var Per = cell.getValue()
        cell = row.getCell(3)
        var Prove = cell.getValue()
        cell = row.getCell(4)
        var Nombre = cell.getValue()
        cell = row.getCell(7)
        var Tipo = cell.getValue()
        cell = row.getCell(8)
        var Asignar = cell.getValue()

        if (Prove == null && Per == null && Asignar == 1)  //Si no hay ninguna persona/prove/listado de participantes seleccionados:
        {
            if (Tipo == "2" && Prove == null) {
                if (document.getElementById("Version").value == "0")
                    var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>_common/BuscadorProveedores.aspx?PM=true&Rol=" + Rol + "&IDControl=ugtxtRoles&idFilaGrid=" + cellId + "&ComboContactos=true", "_blank", "width=835,height=635,status=yes,resizable=yes,top=0,left=150,scrollbars=yes");
                else
                    newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>_common/BuscadorProveedores.aspx?PM=true&Rol=" + Rol + "&Instancia=" + document.getElementById("Instancia").value + "&IDControl=ugtxtRoles&idFilaGrid=" + cellId + "&ComboContactos=true", "_blank", "width=835,height=635,status=yes,resizable=yes,top=0,left=150,scrollbars=yes");

            } else
                if (Per == null) {
                    if (document.getElementById("Version").value == "0")
                        newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>_Common/BuscadorUsuarios.aspx?Rol=" + Rol + "&idFilaGrid=" + cellId, "_blank", "width=750,height=475,status=yes,resizable=no,top=200,left=200");
                    else
                        newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>_Common/BuscadorUsuarios.aspx?Rol=" + Rol + "&Instancia=" + document.getElementById("Instancia").value + "&idFilaGrid=" + cellId, "_blank", "width=750,height=475,status=yes,resizable=no,top=200,left=200");

                }
        }
        else {
            if (Prove == null)
                //detalle de persona
                newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detallepersona.aspx?CodPersona=" + Per.toString(), "_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
            else
                //detalle de proveedor
                newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/detalleproveedor.aspx?codProv=" + Prove.toString(), "_blank", "width=500,height=200,status=yes,resizable=no,top=200,left=250");
        }

    }

    //script traslados		
    //Se abre el buscador de proveedores para que el usuario pueda elegir el proveedor al que se le va a hacer el traslado
    function BuscarProveedor() {
        var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>_common/BuscadorProveedores.aspx?PM=true&ComboContactos=false&CIF=" + document.forms["Form1"].elements["txtCIF"].value + "&COD=" + document.forms["Form1"].elements["txtCod"].value + "&DEN=" + document.forms["Form1"].elements["txtDen"].value, "_blank", "width=835,height=635,status=yes,resizable=yes,top=0,left=150,scrollbars=yes");

    }

    //Una vez seleccionado el proveedor, el buscador llama a esta funci�n que le pone valores a las cajas de texto del CIF, C�digo y Nombre del proveedor
    //Par�metros de entrada:
    //  sCIF: CIF del proveedor seleccionado
    //  sProveCod: Cod del proveedor seleccionado
    //  sProveDen: Nombre del proveedor seleccionado
    function prove_seleccionado2(sCIF, sProveCod, sProveDen) {
        var frmForm1Elements = document.forms["Form1"].elements;
        frmForm1Elements["txtCIF"].value = sCIF
        frmForm1Elements["txtCod"].value = sProveCod
        frmForm1Elements["txtDen"].value = sProveDen

        LimpiarUsuario()
    }

    //Se abre el buscador de usuarios para que el usuario pueda elegir el usuario al que se le va a hacer el traslado
    function BuscarUsuario() {
        var oWDD_UO
        var oWDD_Usu
        if ($find("ugtxtUO_wdd").get_selectedItem() != null) {
            oWDD_UO = $find("ugtxtUO_wdd").get_selectedItemIndex();
        }

        if ($find("ugtxtDEP_").get_selectedItem() != null) {
            oWDD_Usu = $find("ugtxtDEP_").get_selectedItemIndex();
        }
        var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS")%>_Common/BuscadorUsuarios.aspx?desde=PM&UO=" + oWDD_UO + "&DEP=" + oWDD_Usu + "&NOMBRE=" + document.forms["Form1"].elements["txtNombre"].value + "&APE=" + document.forms["Form1"].elements["txtApe"].value, "_blank", "width=750,height=475,status=yes,resizable=no,top=200,left=200");

    }

    //Una vez seleccionado el usuario, el buscador llama a esta funci�n que le pone valores a las cajas de texto del C�digo, Nombre, Apellidos, Departamento y UON del usuario
    //Par�metros de entrada:
    //  sUsuCod: Cod del usuario seleccionado
    //  sNombre: Nombre del usuario seleccionado
    //  sApe: Apellidos del usuario seleccionado
    //  sDep: Departamento del usuario seleccionado
    //  sUON: UON del usuario seleccionado
    function usu_seleccionado_traslado(sUsuCod, sNombre, sApe, sDep, sUON) {
        var oCombo = igcmbo_getComboById("ugtxtUO")
        oCombo.setDisplayValue(sUON)

        var oCombo2 = igcmbo_getComboById("ugtxtDEP")
        oCombo2.setDisplayValue(sDep)

        var frmForm1Elements = document.forms["Form1"].elements;
        frmForm1Elements["txtNombre"].value = sNombre
        frmForm1Elements["txtApe"].value = sApe
        frmForm1Elements["CodPer"].value = sUsuCod

        LimpiarProveedor()
    }

    //Limpia los campos del proveedor
    function LimpiarProveedor() {
        var frmForm1Elements = document.forms["Form1"].elements;
        frmForm1Elements["txtCIF"].value = ""
        frmForm1Elements["txtCod"].value = ""
        frmForm1Elements["txtDen"].value = ""
    }

    //Limpia los campos del usuario
    function LimpiarUsuario() {
        var frmForm1Elements = document.forms["Form1"].elements;
        frmForm1Elements["CodPer"].value = ""
        frmForm1Elements["txtNombre"].value = ""
        frmForm1Elements["txtApe"].value = ""

        var oWdg_UO = $find("ugtxtUO_wdd")
        if (oWdg_UO != null) {
            oWdg_UO.set_currentValue("");
        }

        var oWdg_DEP = $find("ugtxtDEP_")
        if (oWdg_DEP != null) {
            oWdg_DEP.selectItemByIndex(0, true, true);
        }
    }

    //Deshabilita los botones del traslado
    function DeshabilitarBotonesTraslado() {
        var frmForm1Elements = document.forms["Form1"].elements;
        if (frmForm1Elements["cmdContinuarTraslado"])
            frmForm1Elements["cmdContinuarTraslado"].disabled = true
        if (frmForm1Elements["cmdCancelarTraslado"])
            frmForm1Elements["cmdCancelarTraslado"].disabled = true

    }

    function ControlImporte(titulo, importeAcumulado, importeSolicitud, indicebloqueoAviso) {
        sTexto = arrTextosML[indicebloqueoAviso] + '\n' + '"' + titulo + '"' + '\n' + arrTextosML[2] + '=' + importeAcumulado + '\n' + arrTextosML[3] + '=' + importeSolicitud;
        alert(sTexto);
    }

    //Deshabilita los botones de la confirmaci�n del traslado
    function DeshabilitarBotonesTrasladarUsu() {
        var frmForm1Elements = document.forms["Form1"].elements;
        if (frmForm1Elements["cmdAceptarTrasladarUsu"])
            frmForm1Elements["cmdAceptarTrasladarUsu"].disabled = true;
        if (frmForm1Elements["cmdCancelarTrasladarUsu"])
            frmForm1Elements["cmdCancelarTrasladarUsu"].disabled = true;
    }


    //Se viene de la ventana de traslado a Usuario interno y 
    //Hay que recargar los combos con info actualizada
    //Par�metros de entrada:
    //  selectedUON: UON del usuario seleccionado
    //  selectedDEP: Departamento del usuario seleccionado
    //  selectedNOM: Nombre del usuario seleccionado
    //  selectedAPE: Apellidos del usuario seleccionado
    //  selectedCOD: Cod del usuario seleccionado
    function SelectedPostBack(selectedUON, selectedDEP, selectedNOM, selectedAPE, selectedCOD) {
        var frmForm1Elements = document.forms["Form1"].elements;
        frmForm1Elements["SelectedUON"].value = selectedUON;
        frmForm1Elements["SelectedDEP"].value = selectedDEP;
        frmForm1Elements["SelectedNOM"].value = selectedNOM;
        frmForm1Elements["SelectedAPE"].value = selectedAPE;
        frmForm1Elements["SelectedCOD"].value = selectedCOD;
        frmForm1Elements["SelectedPOSTBACK"].value = "1";
        document.forms["Form1"].submit();
    }
    function ponerValorCampoCalculado(campo, valor, tipo) {
        p = window.parent.frames["fraWSMain"]
        if (tipo == 1) {
            oEntry = p.fsGeneralEntry_getById(campo)
            if (oEntry) {
                oEntry.setValue(valor)
            }
        }
        else {
            if (p.document.getElementById(campo)) {
                p.document.getElementById(campo).value = valor
                if (p.oWinDesglose != null) {
                    if (!p.oWinDesglose.closed) {
                        var sAux
                        sAux = campo
                        sAux = sAux.split("__")
                        sAux = "ucDesglose_fsdsentry_" + sAux[sAux.length - 2] + "_" + sAux[sAux.length - 1]
                        oEntry = p.oWinDesglose.fsGeneralEntry_getById(sAux)
                        if (oEntry) oEntry.setValue(valor);                        
                    }
                }
            }
        }
	}	
	function ConfirmarAvisoValidacionMapper() {
		window.parent.fraWSMain.document.forms["frmSubmit"].elements["GEN_MostrarAvisoMapper"].value = false;
		window.parent.fraWSMain.document.forms["frmSubmit"].submit();
	};
</script>
<body runat="server" id="mi_body">	
	<form id="frmSubmit" style="left: 0px; visibility: hidden; position: absolute; top: 0px"
		name="frmAvisos" action="../workflow/avisosprecondiciones.aspx" method="post" target="ventanaAvisos">
	</form>
	<form id="Form1" method="post" runat="server">
		<asp:ScriptManager ID="scrMng_guardarins" runat="server">
			<CompositeScript>
				<Scripts>
					<asp:ScriptReference Path="../alta/js/AdjacentHTML.js" />
				</Scripts>
			</CompositeScript>
		</asp:ScriptManager>
		<input id="Bloque" type="hidden" name="Bloque" runat="server" />
		<input id="BloqueDestinoWS" type="hidden" name="BloqueDestinoWS" runat="server" />
		<input id="RolActual" type="hidden" name="RolActual" runat="server" />
		<input id="Instancia" type="hidden" name="Instancia" runat="server" />
		<input id="PedidoDirecto" type="hidden" name="PedidoDirecto" runat="server" />
		<input id="ComentAltaNoConf" type="hidden" name="ComentAltaNoConf" runat="server" />
		<input id="Guardar" type="hidden" name="Guardar" runat="server" />
		<input id="Accion" type="hidden" name="Accion" runat="server" />
		<input id="idAccion" type="hidden" name="idAccion" runat="server" />
		<input id="Solicitud" type="hidden" name="Solicitud" runat="server" />
		<input id="Formulario" type="hidden" name="Formulario" runat="server" />
		<input id="Workflow" type="hidden" name="Workflow" runat="server" />
		<input id="Notificar" type="hidden" name="Notificar" runat="server" />
		<input id="total" type="hidden" name="total" runat="server" />
		<input id="EstadoInstancia" type="hidden" name="EstadoInstancia" runat="server" />
		<input id="txtPeticionario" type="hidden" name="txtPeticionario" runat="server" />
		<input id="txtPeticionarioTraslado" type="hidden" name="txtIdTipo" runat="server" />
		<input id="CodPer" type="hidden" name="CodPer" runat="server" />
		<input id="txtPeticionarioDevolucion" type="hidden" name="txtIdTipo" runat="server" />
		<input id="txtPeticionarioTrasladoUsu" type="hidden" name="txtIdTipo" runat="server" />
		<input id="txtcadenaserror1" type="hidden" name="txtcadenaserror1" runat="server" />
		<input id="txtcadenaserror2" type="hidden" name="txtcadenaserror2" runat="server" />
		<input id="txtcadenaserror3" type="hidden" name="txtcadenaserror3" runat="server" />
		<input id="txtcadenaserror4" type="hidden" name="txtcadenaserror4" runat="server" />
		<input id="Version" type="hidden" name="Version" runat="server" />
		<input id="txtIdiMsgbox" type="hidden" name="txtIdiMsgbox" runat="server" />
		<input id="IdRowContacto" type="hidden" name="IdRowContacto" runat="server" />
		<input id="txtCualquiera" type="hidden" name="txtCualquiera" runat="server" />
		<input id="SelectedUON" type="hidden" name="SelectedUON" runat="server" />
		<input id="SelectedDEP" type="hidden" name="SelectedDEP" runat="server" />
		<input id="SelectedNOM" type="hidden" name="SelectedNOM" runat="server" />
		<input id="SelectedAPE" type="hidden" name="SelectedAPE" runat="server" />
		<input id="SelectedCOD" type="hidden" name="SelectedCOD" runat="server" />
		<input id="SelectedPOSTBACK" type="hidden" name="SelectedPOSTBACK" runat="server" />
		<input id="SelectedItem" type="hidden" name="SelectedItem" runat="server" />
		<input id="SelectedItem2" type="hidden" name="SelectedItem2" runat="server" />
		<input id="AvisoBloqueo" type="hidden" name="AvisoBloqueo" runat="server" />
		<input id="idRefSol" type="hidden" name="idRefSol" runat="server" />
		<input id="NewInstancia" type="hidden" name="NewInstancia" runat="server" />
		<input id="NoConformidad" type="hidden" name="NoConformidad" runat="server" />
		<input id="Certificado" type="hidden" name="Certificado" runat="server" />
		<input id="Contrato" type="hidden" runat="server" />
		<input id="CodContrato" type="hidden" runat="server" />
		<input id="ControlPartidas" type="hidden" runat="server" />
		<input id="UsuarioConfirmaAccion" type="hidden" runat="server" />
		<input id="ConfiguracionGS" type="hidden" runat="server" />
		<input id="NombreXML" type="hidden" runat="server" />
		<input id="tipoSolicitud" type="hidden" runat="server" />
		<input id="hTipoVisor" type="hidden" runat="server" />
		<input id="hidVolver" type="hidden" runat="server" />
		<div id="ContenedorAvisos" runat="server"></div>
		<uc1:menu ID="Menu1" runat="server" />
		<fsn:FSNPanelInfo ID="FSNPanelDatosPeticionario" runat="server" ServicePath="~/App_Pages/PMWEB/ConsultasPMWEB.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="2"></fsn:FSNPanelInfo>
		<fsn:FSNPageHeader ID="FSNPageHeader" runat="server" UrlImagenCabecera="images/SolicitudesPMSmall.jpg"></fsn:FSNPageHeader>
		<!------------------------------------------------->
		<div style="padding-left: 15px; padding-bottom: 15px; padding-top: 45px;" id="divCabecera" runat="server">
			<asp:Panel ID="pnlCabecera" runat="server" BackColor="#f5f5f5" Font-Names="Arial" Width="95%">
				<table style="height: 15%; width: 100%; padding-bottom: 15px; padding-left: 5px" cellspacing="0" cellpadding="1" border="0">
					<tr>
						<td style="padding-top: 5px; padding-bottom: 5px;" class="fondoCabecera">
							<table style="width: 100%; padding-left: 10px" border="0">
								<tr>
									<td colspan="2">
										<asp:Label ID="lblIDInstanciayEstado" runat="server" CssClass="label" Font-Bold="true"></asp:Label></td>
									<td>
										<asp:Label ID="lblLitImporte" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
									<td>
										<asp:Label ID="lblImporte" runat="server" CssClass="label" Font-Bold="true"></asp:Label></td>
								</tr>
								<tr>
									<td>
										<asp:Label ID="lblLitCreadoPor" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
									<td>
										<asp:Label ID="lblCreadoPor" runat="server" CssClass="label">
										</asp:Label><asp:Image ID="imgInfCreadoPor" ImageUrl="images/info.gif" runat="server" />
									</td>
									<td>
										<asp:Label ID="lblLitEstado" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
									<td>
										<asp:Label ID="lblEstado" runat="server" CssClass="label"></asp:Label></td>
									<td>
										<asp:HyperLink ID="HyperDetalle" runat="server" CssClass="CaptionLink"></asp:HyperLink></td>
								</tr>
								<tr>
									<td>
										<asp:Label ID="lblLitFecAlta" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
									<td>
										<asp:Label ID="lblFecAlta" runat="server" CssClass="label"></asp:Label></td>
									<td>
										<asp:Label ID="lblLitTipo" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
									<td>
										<asp:Label ID="lblTipo" runat="server" CssClass="label"></asp:Label>
										<asp:Image ID="imgInfTipo" ImageUrl="images/info.gif" runat="server" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</asp:Panel>
		</div>

		<ajx:DropShadowExtender TrackPosition="true" ID="DropShadowExtender1" runat="server" Opacity="0.5" Width="3" TargetControlID="pnlCabecera" Rounded="true">
		</ajx:DropShadowExtender>
		<!---------------------------------------------------->

		<div id="divDevolverSiguientesEtapas" style="clear: both; width: 100%; float: left; margin-left: 15px; padding-bottom: 15px; display: none" runat="server">
			<table id="tblGeneral" height="70%" cellspacing="1" cellpadding="1" width="95%" border="0" runat="server">
				<tr>
					<td style="height: 15px" valign="bottom">
						<asp:Label ID="lblEtapas" runat="server" Width="100%" CssClass="fntLogin"></asp:Label></td>
				</tr>
				<tr>
					<td style="height: 50px">
						<textarea id="txtaEtapas" runat="server" rows="4" cols="1" style="width: 100%" readonly></textarea></td>
				</tr>
				<tr>
					<td style="height: 26px" valign="bottom">
						<asp:Label ID="lblRoles" runat="server" Width="100%" CssClass="fntLogin"></asp:Label></td>
				</tr>
				<tr>
					<td style="height: 50px">
						<igtbl:UltraWebGrid ID="ugtxtRoles" runat="server" Width="100%" Height="60px">
							<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" RowSelectorsDefault="No"
								Name="ugtxtRoles">
								<AddNewBox>
									<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
										<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White" > </BorderDetails >
									</Style>
								</AddNewBox>
								<Pager>
									<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
										<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White" > </BorderDetails >
									</Style>
								</Pager>
								<FrameStyle Width="100%" BorderWidth="1px" Font-Size="8pt" Font-Names="Verdana" BorderStyle="Solid" Height="60px">
								</FrameStyle>
								<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
									<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
								</FooterStyleDefault>
								<EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
							</DisplayLayout>
							<Bands>
								<igtbl:UltraGridBand FixedHeaderIndicator="None" CellClickAction="RowSelect" RowSelectors="No">
									<HeaderStyle CssClass="cabeceraPMWEB"></HeaderStyle>
									<RowAlternateStyle CssClass="ugfilaalttabla"></RowAlternateStyle>
									<RowStyle CssClass="ugfilatabla"></RowStyle>
								</igtbl:UltraGridBand>
							</Bands>
						</igtbl:UltraWebGrid></td>
				</tr>
				<tr>
					<td style="height: 26px" valign="bottom">
						<asp:Label ID="lblComentarios" runat="server" Width="100%" CssClass="fntLogin"></asp:Label></td>
				</tr>
				<tr>
					<td style="height: 100px">
						<textarea onkeypress="return validarLength(this,4000)" id="txtComent" onkeydown="textCounter(this,4000)"
							onkeyup="textCounter(this,4000)" style="width: 100%; height: 100px" name="txtComent" onchange="return validarLength(this,4000)"
							runat="server"></textarea>
					</td>
				</tr>
				<tr>
					<td style="height: 25px" align="center">
						<table id="tblBotones" cellspacing="1" cellpadding="1" width="100%" border="0">
							<tr>
								<td align="center">
									<input class="botonPMWEB" id="cmdAceptar" onclick="DeshabilitarBotones();" type="button" name="cmdAceptar" runat="server" />
								</td>
								<td align="left">
									<input class="botonPMWEB" id="cmdCancelar" onclick="DeshabilitarBotones(); OcultarDivs(); MostrarFSWSMain(); parent.fraWSMain.HabilitarBotones();"
										type="button" name="cmdCancelar" runat="server" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<div id="divTraslado" style="display: none" runat="server">
			<table id="Table1" style="z-index: 102; left: 16px;" height="50%" cellspacing="1" cellpadding="1" width="100%" border="0">
				<tr height="25%">
					<td>
						<table id="tblProv" cellspacing="7" cellpadding="1" width="100%" border="0">
							<tr>
								<td>
									<asp:Label ID="lblTrasladoProv" runat="server" Width="100%" CssClass="subTitulo"></asp:Label></td>
							</tr>
							<tr>
								<td>
									<asp:Label ID="lblMsg1" runat="server" Width="100%" CssClass="parrafo"></asp:Label></td>
							</tr>
							<tr>
								<td align="center">
									<table id="tblBuscaProv" cellspacing="1" cellpadding="1" width="50%" border="0">
										<tr>
											<td style="height: 27px" width="150px">
												<asp:Label ID="lblCIF" runat="server" Width="100%" CssClass="parrafo"></asp:Label></td>
											<td style="height: 27px">
												<asp:TextBox ID="txtCIF" runat="server"></asp:TextBox></td>
											<td style="height: 27px"></td>
										</tr>
										<tr>
											<td>
												<asp:Label ID="lblCod" runat="server" Width="100%" CssClass="parrafo"></asp:Label></td>
											<td>
												<asp:TextBox ID="txtCod" runat="server"></asp:TextBox></td>
											<td width="88px" align="right">
												<input id="cmdBuscar" type="button" name="cmdBuscar" runat="server" class="botonPMWEB" onclick="javascript: BuscarProveedor()" /></td>
										</tr>
										<tr>
											<td>
												<asp:Label ID="lblDen" runat="server" Width="100%" CssClass="parrafo"></asp:Label></td>
											<td>
												<asp:TextBox ID="txtDen" runat="server"></asp:TextBox></td>
											<td></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr height="25%">
					<td>
						<table id="tblUsu" cellspacing="7" cellpadding="1" width="100%" border="0">
							<tr>
								<td>
									<asp:Label ID="lblTrasladoUsu" runat="server" Width="100%" CssClass="subTitulo"></asp:Label></td>
							</tr>
							<tr>
								<td>
									<asp:Label ID="lblMsg2" runat="server" Width="100%" CssClass="parrafo"></asp:Label></td>
							</tr>
							<tr>
								<td align="center">
									<table id="tblBuscarUsu" cellspacing="1" cellpadding="1" width="50%" border="0">
										<tr>
											<td width="150px">
												<asp:Label ID="lblUO" runat="server" Width="100%" CssClass="parrafo"></asp:Label></td>
											<td>
												<ig:WebDropDown ID="ugtxtUO_wdd" runat="server" Width="100%" EnableClosingDropDownOnSelect="true"
													DropDownContainerWidth="" DropDownAnimationType="EaseOut" EnablePaging="False" PageSize="12"
													DropDownContainerHeight="100px">
													<AutoPostBackFlags SelectionChanged="On" />
												</ig:WebDropDown>
											</td>
											<td rowspan="4" valign="middle" width="88px" align="right">
												<input id="cmdBuscar2" type="button" name="cmdBuscar2" runat="server" class="botonPMWEB" onclick="javascript: BuscarUsuario()" />
											</td>
										</tr>
										<tr>
											<td>
												<asp:Label ID="lblDep" runat="server" Width="100%" CssClass="parrafo"></asp:Label></td>
											<td>
												<asp:UpdatePanel ID="Up_Uon" runat="server" UpdateMode="Conditional">
													<Triggers>
														<asp:AsyncPostBackTrigger ControlID="ugtxtUO_wdd" EventName="SelectionChanged" />
													</Triggers>
													<ContentTemplate>
														<ig:WebDropDown ID="ugtxtDEP_" runat="server" Width="100%" EnableClosingDropDownOnSelect="true"
															DropDownContainerHeight="100px" DropDownContainerWidth="" DropDownAnimationType="EaseOut"
															EnablePaging="False" PageSize="12" OnSelectionChanged="ugtxtDEP__SelectionChanged">
															<ClientEvents SelectionChanged="selectedIndexChanged" />
														</ig:WebDropDown>
													</ContentTemplate>
												</asp:UpdatePanel>
											</td>
										</tr>
										<tr>
											<td>
												<asp:Label ID="lblNombre" runat="server" Width="100%" CssClass="parrafo"></asp:Label></td>
											<td>
												<asp:TextBox ID="txtNombre" runat="server"></asp:TextBox></td>
										</tr>
										<tr>
											<td>
												<asp:Label ID="lblApe" runat="server" Width="100%" CssClass="parrafo"></asp:Label></td>
											<td>
												<asp:TextBox ID="txtApe" runat="server"></asp:TextBox></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table id="TblBotones" cellspacing="1" cellpadding="1" width="30%" border="0">
							<tr>
								<td align="center">
									<input class="botonPMWEB" id="cmdContinuarTraslado" onclick="DeshabilitarBotonesTraslado();" type="button" name="cmdContinuarTraslado" runat="server" />
								</td>
								<td align="center">
									<input class="botonPMWEB" id="cmdCancelarTraslado" onclick="javascript: OcultarDivs(); MostrarFSWSMain(); parent.fraWSMain.HabilitarBotones();" type="button" name="cmdCancelarTraslado" runat="server" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<div id="divDevolucion" style="display: none" runat="server">
			<table id="Table1Devolucion" style="z-index: 102; left: 16px;" height="90%" cellspacing="1" cellpadding="1" width="100%" border="0">
				<tr>
					<td>
						<table id="tblDatos" height="60%" cellspacing="1" cellpadding="1" width="100%" border="0" runat="server">
							<tr height="15%">
								<td width="20%">
									<asp:Label ID="lblOrigen" runat="server" Width="100%" CssClass="captionGris"></asp:Label>
								</td>
								<td width="80%">
									<asp:Label ID="lblUsuario" runat="server" Width="100%" CssClass="captionGris"></asp:Label>
								</td>
							</tr>
							<tr height="4%">
								<td colspan="2">
									<asp:Label ID="lblComent" runat="server" Width="100%" CssClass="fntLogin"></asp:Label>
								</td>
								<td></td>
							</tr>
							<tr height="20%">
								<td colspan="2">
									<textarea onkeypress="return validarLength(this,4000)" id="txtComentDevolucion" onkeydown="textCounter(this,4000)"
										onkeyup="textCounter(this,4000)" style="width: 95%; height: 100%" name="txtComent" onchange="return validarLength(this,4000)" runat="server"></textarea>
								</td>
								<td></td>
							</tr>
							<tr>
								<td valign="middle" align="center" colspan="2">
									<table id="tblBotonesDevolucion" style="width: 100px; height: 10px" cellspacing="1" cellpadding="1" border="0">
										<tr>
											<td>
												<input class="botonPMWEB" id="cmdAceptarDevolucion" type="button" name="cmdAceptarDevolucion" runat="server" />
											</td>
											<td>
												<input class="botonPMWEB" id="cmdCancelarDevolucion" onclick="OcultarDivs(); MostrarFSWSMain(); parent.fraWSMain.HabilitarBotones();"
													type="button" name="cmdCancelarDevolucion" runat="server" />
											</td>
										</tr>
									</table>
								</td>
								<td></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<div id="divtrasladoUsu" style="display: none" runat="server">
			<table id="Table1TrasladoUsu" style="z-index: 102; left: 16px;" height="60%" cellspacing="1" cellpadding="1" width="100%" border="0">
				<tr>
					<td valign="top">
						<table id="tblDatosTrasladoUsu" height="100%" cellspacing="1" cellpadding="1" width="100%" border="0" runat="server">
							<tr height="15%">
								<td width="30%">
									<asp:Label ID="lblTraslado" runat="server" Width="100%" CssClass="captionGris"></asp:Label>
								</td>
								<td width="70%">
									<asp:Label ID="lblUsuarioTrasladoUsu" runat="server" Width="100%" CssClass="captionGris"></asp:Label>
								</td>
							</tr>
							<tr>
								<td width="30%">
									<asp:Label ID="lblContacto" runat="server" Width="100%" CssClass="captionGris"></asp:Label>
								</td>
								<td>
									<ig:WebDropDown ID="ugtxtContacto_" runat="server" Width="288px" EnableClosingDropDownOnSelect="true"
										DropDownContainerHeight="150px" DropDownContainerWidth="" DropDownAnimationType="EaseOut"
										EnablePaging="False" PageSize="12">
										<ClientEvents SelectionChanged="selectedIndexChanged" />
									</ig:WebDropDown>
								</td>
							</tr>
							<tr height="15%">
								<td>
									<asp:Label ID="lblIntroFec" runat="server" Width="100%" CssClass="fntLogin"></asp:Label>
								</td>
								<td>
									<fsde:GeneralEntry ID="GeneralFecha" runat="server" Tipo="TipoFecha" Tag="FeCCumpl" DropDownGridID="GeneralFecha">
										<InputStyle Width="150px" CssClass="TipoFecha" />
									</fsde:GeneralEntry>
								</td>
							</tr>
							<tr height="4%">
								<td colspan="2">
									<asp:Label ID="lblComentTrasladoUsu" runat="server" Width="100%" CssClass="fntLogin"></asp:Label>
								</td>
								<td></td>
							</tr>
							<tr height="20%">
								<td colspan="2">
									<textarea onkeypress="return validarLength(this,4000)" id="txtComentTrasladoUsu" onkeydown="textCounter(this,4000)"
										onkeyup="textCounter(this,4000)" style="width: 95%; height: 100%; min-height: 100px;" name="txtComent" onchange="return validarLength(this,4000)"
										runat="server"></textarea>
								</td>
								<td></td>
							</tr>
							<tr>
								<td valign="middle" align="center" colspan="2">
									<table id="tblBotonesTrasladoUsu" style="width: 100px; height: 20px" cellspacing="1" cellpadding="1" width="100" border="0">
										<tr>
											<td>
												<input class="botonPMWEB" id="CmdAceptarTrasladoUsu" onclick="DeshabilitarBotonesTrasladarUsu();"
													type="button" name="cmdAceptarTrasladoUsu" runat="server" />
											</td>
											<td>
												<input class="botonPMWEB" id="CmdCancelarTrasladoUsu" onclick="DeshabilitarBotonesTrasladarUsu(); divtrasladoUsu.style.display = 'none'; divTraslado.style.display = 'inline'"
													type="button" name="cmdCancelarTrasladoUsu" runat="server" />
											</td>
										</tr>
									</table>
								</td>
								<td></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<div id="divControlPartidas" runat="server" style="display: none">
			<table>
				<tr>
					<td style="height: 30px; position: absolute; padding-left: 10px">
						<asp:Image ID="imgControlDisponible" runat="server" ImageUrl="images/billete.bmp" />
					</td>
				</tr>
			</table>

			<table width="100%">
				<tr>
					<td style="height: 25px; padding-left: 80px" class="cabeceraColor">
						<asp:Label runat="server" ID="lblControlDisponible" Width="100%" CssClass="cabeceraColor"></asp:Label>
					</td>
				</tr>
			</table>

			<table runat="server" id="tblControlImportes" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding-left: 10px">
				<tr>
					<td colspan="5">
						<table>
							<tr>
								<td style="padding-left: 10px; padding-right: 15px">
									<asp:Image runat="server" ID="imgPrecaucion" ImageUrl="./images/Icono_Error_Amarillo_40x40.gif" />
								</td>
								<td>
									<asp:Label runat="server" ID="lblMsjControlPartidas1" Text="" CssClass="TituloSinDatos" Width="80%"></asp:Label>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="5">
						<table runat="server" id="tblControlIN" width="100%">
						</table>
					</td>
				</tr>
				<tr style="padding-top: 15px">
					<td style="text-align: right">
						<input type="button" runat="server" id="cmdAceptarControlPartida" visible="false" />
					</td>
					<td>
						<input type="button" runat="server" id="cmdCancelarControlPartida" onclick="DeshabilitarBotones(); OcultarDivs(); MostrarFSWSMain(); parent.fraWSMain.HabilitarBotones();" />
					</td>
				</tr>
			</table>
		</div>
		<script type="text/javascript">
            function selectedIndexChanged(sender, e) {
                var newSelectedItem = e.getNewSelection()[0];
                var oldSelectedItem = e.getOldSelection()[0];
                var oItem
                if (newSelectedItem != null) {
                    newSelectedItem._element.className = 'igdd_FullstepListItem igdd_FullstepListItemB igdd_FullstepListItemSelected';
                    newSelectedItem = null;
                }
                if (oldSelectedItem != null) {
                    oldSelectedItem._element.className = 'igdd_FullstepListItem igdd_FullstepListItemB';
                    oldSelectedItem = null;
                }
            }
            function resetDropDownStyles(itemIndex) {
                var dropdown = $find("ugtxtUO_wdd");
                if (dropdown != null) {
                    var dropdownItems = dropdown.get_items();
                    if (dropdownItems != null) {
                        for (var i = 0; i < dropdownItems.getLength() ; i++) {
                            dropdownItems.getItem(i)._element.className = 'igdd_FullstepListItem igdd_FullstepListItemB';
                        }
                        if (itemIndex != '') {
                            dropdown.selectItemByIndex(itemIndex, true, true);
                        }
                        dropdownItems = null;
                    }
                    dropdown = null;
                }
			}			
		</script>
	</form>
</body>

</html>
