﻿Public Class controlarLineaVinculada
    Inherits FSNPage

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
    ''' <summary>
    ''' Carga de pantalla. Realmente lo q hace es q por ajax sincrono se desea saber si se puede o no 
    ''' añadir una linea vinculada en NWAlta.aspx y NWGestionInstancia.aspx y alta/desglose.aspx y workflow/desglose.aspx.
    ''' </summary>
    ''' <param name="sender">pantalla</param>
    ''' <param name="e">parametro de sistema</param>     
    ''' <remarks>Llamada desde: NWAlta.aspx NWGestionInstancia.aspx workflow/desglose.aspx alta/desglose.aspx; Tiempo máximo: 0,1</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Expires = -1
        If Not (Request("Mover") Is Nothing) Then
            Dim oInstancia As FSNServer.Instancia
            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
            oInstancia.ID = Request("Instancia")

            If Not oInstancia.ExisteLineaVinculacion(Request("Desglose"), Request("Linea")) Then
                Responder("OK")
            Else
                Responder("PANT")
            End If
        ElseIf Not (Request("Solicitud") Is Nothing) Then
            Dim oSolicitud As FSNServer.Solicitud
            oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
            oSolicitud.ID = Request("Solicitud")

            Responder(oSolicitud.ComprobarEmitirCantidadesVincular(Request("DatosOrigen"), Numero(Request("Cantidad"), ".")))
        ElseIf Not (Request("InstanciaVinc") Is Nothing) Then
            Dim oInstancia As FSNServer.Instancia
            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
            oInstancia.ID = Request("InstanciaVinc")

            Responder(oInstancia.ComprobarEmitirCantidadesVincular(Request("DatosOrigen"), Numero(Request("Cantidad"), ".")))
        Else
            Dim oInstancia As FSNServer.Instancia
            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
            oInstancia.ID = Request("Instancia")

            Responder(oInstancia.ComprobarCantidadesVincular(False, Request("Desglose"), Request("Linea"), Me.Idioma, FSNUser.DateFmt))
        End If
    End Sub
    ''' <summary>
    ''' Se ha hecho una llamada Ajax sincrona a esta pantalla, esta función devuelve el resultado de la ejecución
    ''' </summary>
    ''' <param name="Respuesta">Respuesta de la ejecución</param>    
    ''' <remarks>Llamada desde: Page_Load; Tiempo máximo: 0</remarks>
    Private Sub Responder(ByVal Respuesta As String)
        Response.Clear()
        Response.ContentType = "text/plain"
        Response.Write(Respuesta)
        Response.End()
    End Sub
End Class