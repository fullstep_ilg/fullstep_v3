<%@ Page Language="vb" AutoEventWireup="false" Codebehind="selectorCampos.aspx.vb" Inherits="Fullstep.FSNWeb.selectorCamposCertif"%>
<%@ Register TagPrefix="ignav" Namespace="Infragistics.WebUI.UltraWebNavigator" Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<script type="text/javascript"><!--		
		function refrescarPanelProve ()
		{
			var p=window.opener  
			
			p.FuerzaRecarga()
				
			window.close()							
		}
		
		function refrescarConfiguracion(sNameGrid)
		{
		    var p = window.opener

		    var oTree = igtree_getTreeById("uwtCampos")
		    var nodes = oTree.getNodes()
		    var onode = nodes[0]
		    var columnas = new Array();
		    var i = 0;

		    onode = onode.getFirstChild()
		    while (onode) {
		        if (onode.getChecked() == true) {
		            columnas[i] = onode.getDataKey();
		            i++;
		        }
		        onode = onode.getNextSibling()
		    }
		    p.mostrarColumnas(columnas)
		    window.close()
        }

		/*''' <summary>
		''' Posicionar en pantalla la botonera, estas seleccionado campos para certifBusqueda.aspx
		''' </summary>
		''' <remarks>Llamada desde: selectorCampos.aspx.vb/page_load; Tiempo m�ximo: 0</remarks>*/			
		function PosicionarCertif(){
			document.getElementById("Botones").style.TOP = '380px'
		}
		/*''' <summary>
		''' Posicionar en pantalla la botonera, estas seleccionado campos para mantenimientoMat
		''' </summary>
		''' <remarks>Llamada desde: selectorCampos.aspx.vb/page_load; Tiempo m�ximo: 0</remarks>*/		
		function PosicionarMat(){
			document.getElementById("Botones").style.TOP = '310px'	
		}		
--></script>	
	<body>
		<form id="Form1" method="post" runat="server">
			<DIV id="Titulos" style="HEIGHT: 40px" runat="server">
				<TABLE id="TableTitulos" cellSpacing="1" cellPadding="1" width="95%" border="0" runat="server">
				</TABLE>
			</DIV>
			<DIV id="Lineas" style="OVERFLOW: auto; TOP: 0px; margin: 8px" runat="server">
				<TABLE id="Table1" cellSpacing="1" cellPadding="1" style="border: 0; width:100%;">
					<TR>
						<TD>
							<TABLE id="TableLineas" cellSpacing="1" cellPadding="1" width="100%" border="0" runat="server">
							</TABLE>
							<ignav:ultrawebtree id="uwtCampos" runat="server" WebTreeTarget="HierarchicalTree" 
							CssClass="igTree" width="97%">
								<SelectedNodeStyle ForeColor="White" BackColor="Navy"></SelectedNodeStyle>
								<Levels>
									<ignav:Level Index="0"></ignav:Level>
									<ignav:Level Index="1"></ignav:Level>
								</Levels>
							</ignav:ultrawebtree></TD>
					</TR>
				</TABLE>
			</DIV>
			<DIV id="Botones" style="HEIGHT: 20px;" runat="server">
				<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="90%" border="0">
					<TR height="100%">
						<TD vAlign="bottom" align="center"><INPUT class="botonPMWEB" id="cmdTemp" type="button" value="DTemp" name="cmdTemp" runat="server">
							<INPUT class="botonPMWEB" id="cmdGuardar" type="button" value="DGuardar" name="cmdGuardar" runat="server">
						</TD>
					</TR>
				</TABLE>
			</DIV>
		</form>
	</body>
</html>
