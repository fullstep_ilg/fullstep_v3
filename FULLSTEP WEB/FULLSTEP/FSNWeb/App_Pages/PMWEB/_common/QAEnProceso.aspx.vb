Imports PMWeb.modUtilidades
Public Class QAEnProceso
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Protected WithEvents lblId As System.Web.UI.WebControls.Label
    Protected WithEvents lblIdBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechaBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblEnProceso As System.Web.UI.WebControls.Label
    Protected WithEvents lblProveedor As System.Web.UI.WebControls.Label
    Protected WithEvents lblProveedorBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblTipo As System.Web.UI.WebControls.Label
    Protected WithEvents lblTipoBD As System.Web.UI.WebControls.Label
    Protected WithEvents btnCerrar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected Titulo As String


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sIdi As String
        Dim oDict As FSNServer.Dictionary
        Dim oTextos As DataTable
        Dim FSWSServer As FSNServer.Root
        Dim oUser As FSNServer.User

        FSWSServer = Session("FSN_Server")
        oUser = Session("FSN_User")
        oUser.LoadUserData(oUser.Cod)

        sIdi = oUser.Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        oDict = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
        If Request("Certificado") <> Nothing Then
            oDict.LoadData(TiposDeDatos.ModulosIdiomas.DetalleCertificado, sIdi)
        Else
            oDict.LoadData(TiposDeDatos.ModulosIdiomas.NoConformidades, sIdi)
        End If
        oTextos = oDict.Data.Tables(0)

        Dim sInstancia As String = Request("Instancia")
        Dim sFecAlta As String = Request("FecAlta")
        Dim sProveedor As String = Request("Proveedor")
        Dim Tipo As String = Request("Tipo")

        If Request("Certificado") <> Nothing Then
            lblId.Text = oTextos.Rows(15).Item(1)
            lblFecha.Text = oTextos.Rows(4).Item(1)
            lblProveedor.Text = oTextos.Rows(3).Item(1)
            lblTipo.Text = oTextos.Rows(16).Item(1)
            btnCerrar.Value = oTextos.Rows(17).Item(1)
            lblEnProceso.Text = oTextos.Rows(18).Item(1)
            Titulo = oTextos.Rows(19).Item(1)
        Else
            lblId.Text = oTextos.Rows(6).Item(1) & IIf(InStr(1, oTextos.Rows(6).Item(1), ":") > 0, "", ":")
            lblFecha.Text = oTextos.Rows(8).Item(1) & IIf(InStr(1, oTextos.Rows(8).Item(1), ":") > 0, "", ":")
            lblProveedor.Text = oTextos.Rows(9).Item(1) & IIf(InStr(1, oTextos.Rows(9).Item(1), ":") > 0, "", ":")
            lblTipo.Text = oTextos.Rows(4).Item(1) & IIf(InStr(1, oTextos.Rows(4).Item(1), ":") > 0, "", ":")
            btnCerrar.Value = oTextos.Rows(33).Item(1)
            lblEnProceso.Text = oTextos.Rows(36).Item(1)
            Titulo = oTextos.Rows(37).Item(1)
        End If

        lblIdBD.Text = sInstancia

        If sFecAlta <> "null" Then
            Dim iDia, iMes, iAnyo As Integer
            iDia = Split(sFecAlta, "/")(0)
            iMes = Split(sFecAlta, "/")(1)
            iAnyo = Split(sFecAlta, "/")(2)

            Dim sFechaAlta As New Date(iAnyo, iMes, iDia)
            lblFechaBD.Text = FormatDate(sFechaAlta, oUser.DateFormat)

        End If
        lblProveedorBD.Text = sProveedor
        lblTipoBD.Text = Tipo
    End Sub

End Class
