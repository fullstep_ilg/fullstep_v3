Namespace Common
    Public Class atachedfiles
        Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
        Protected WithEvents lblSubTitulo As System.Web.UI.WebControls.Label
        Protected WithEvents lblDescargar As System.Web.UI.WebControls.Label
        Protected WithEvents imgDescarga As System.Web.UI.WebControls.ImageButton
        Protected WithEvents uwgFiles As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
        Protected WithEvents GeneralEntry1 As DataEntry.GeneralEntry
        Protected WithEvents IDCAMPO As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents cmdAnyadir As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents lblTipo As System.Web.UI.WebControls.Label
        Protected WithEvents lblNomCampo As System.Web.UI.WebControls.Label
        Protected WithEvents FSENTRY As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents txtInstancia As System.Web.UI.HtmlControls.HtmlInputHidden

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private mlInstancia As Long
        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf &
    "<script DEFER=true language=""{0}"">{1}</script>"
        Private Const IncludeScriptFormat As String = ControlChars.CrLf &
    "<script type=""{0}"" src=""{1}""></script>"

        Property Instancia() As Long
            Get
                Return mlInstancia
            End Get
            Set(ByVal Value As Long)
                mlInstancia = Value
            End Set
        End Property

        ''' <summary>
        ''' Carga de la pantalla
        ''' </summary>
        ''' <param name="sender">pantalla</param>
        ''' <param name="e">parametro de sistema</param>            
        ''' <returns>Nada, es un evento de sistema</returns>
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
            Dim bReadOnly As Boolean = (Request("readOnly") = 1)
            Dim oUser As FSNServer.User
            oUser = Session("FSN_User")

            Dim sIdi As String = Session("FSN_User").Idioma.ToString()
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If
            Dim oDictionary As FSNServer.Dictionary
            Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))

            oDict.LoadData(TiposDeDatos.ModulosIdiomas.DetalleCampoArchivo, sIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)

            Dim idCampo As Integer = CInt(Request("Campo"))

            Dim idAdjuntos As String = Request("Valor")

            idAdjuntos = Replace(idAdjuntos, "xx", ",")

            Dim idAdjuntosNew As String = Request("Valor2").Replace("xx", ",")


            Dim oAdjuntos As FSNServer.Adjuntos

            oAdjuntos = FSWSServer.Get_Object(GetType(FSNServer.Adjuntos))

            If Request("Instancia") <> Nothing Then
                mlInstancia = Request("Instancia")
            End If

            If mlInstancia = Nothing Then
                oAdjuntos.Load(Request("tipo"), idAdjuntos, idAdjuntosNew, IIf((Request("Favorito")) = "1", True, False))
            Else
                Me.txtInstancia.Value = mlInstancia
                oAdjuntos.LoadInst(Request("tipo"), idAdjuntos, idAdjuntosNew)
                'Si es una adjunto metido desde A�adir por valor por defecto. La informaci�n
                'esta en LINEA_DESGLOSE_ADJUN
                If oAdjuntos.Data.Tables(0).Rows.Count = 0 Then
                    oAdjuntos.Load(Request("tipo"), idAdjuntos, idAdjuntosNew)
                End If

            End If

            Dim oCampo As FSNServer.Campo


            Dim lSolicitud As Long

            If Request("Solicitud") <> Nothing Then
                lSolicitud = Request("Solicitud")
            End If

            oCampo = FSWSServer.Get_Object(GetType(FSNServer.Campo))
            oCampo.Id = idCampo
            Me.IDCAMPO.Value = idCampo
            Me.FSENTRY.Value = Request("Input")
            Dim oDS As DataSet


            Dim oSolicitud As FSNServer.Solicitud

            If mlInstancia > 0 Then
                oCampo.LoadInst(mlInstancia, sIdi)
            Else
                oCampo.Load(sIdi, lSolicitud)
            End If
            oDS = oAdjuntos.Data


            Me.lblTitulo.Text = oTextos.Rows(8).Item("TEXT_" & sIdi)
            Me.lblTipo.Text = oCampo.DenSolicitud(sIdi)
            Me.lblSubTitulo.Text = oCampo.DenGrupo(sIdi)
            Me.lblNomCampo.Text = oCampo.Den(sIdi)
            Me.cmdAnyadir.Value = oTextos.Rows(10).Item(1)

            Me.uwgFiles.DataSource = oDS

            Me.uwgFiles.DataBind()



            uwgFiles.Bands(0).Columns.FromKey("ID").Hidden = True
            uwgFiles.Bands(0).Columns.FromKey("TIPO").Hidden = True

            uwgFiles.Bands(0).Columns.FromKey("PER").Hidden = True

            uwgFiles.Bands(0).Columns.FromKey("FECALTA").Header.Caption = oTextos.Rows(1).Item("TEXT_" & sIdi)
            uwgFiles.Bands(0).Columns.FromKey("FECALTA").Width = Unit.Percentage(10)
            uwgFiles.Bands(0).Columns.FromKey("FECALTA").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
            uwgFiles.Bands(0).Columns.FromKey("NOMBRE").Header.Caption = oTextos.Rows(9).Item("TEXT_" & sIdi)
            uwgFiles.Bands(0).Columns.FromKey("NOMBRE").Width = Unit.Percentage(18)
            uwgFiles.Bands(0).Columns.FromKey("NOMBRE").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
            uwgFiles.Bands(0).Columns.FromKey("NOM").Header.Caption = oTextos.Rows(2).Item("TEXT_" & sIdi)
            uwgFiles.Bands(0).Columns.FromKey("NOM").Width = Unit.Percentage(18)
            uwgFiles.Bands(0).Columns.FromKey("NOM").CellMultiline = Infragistics.WebUI.UltraWebGrid.CellMultiline.Yes
            uwgFiles.Bands(0).Columns.FromKey("NOM").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
            uwgFiles.Bands(0).Columns.FromKey("DATASIZE").Header.Caption = oTextos.Rows(3).Item("TEXT_" & sIdi)
            uwgFiles.Bands(0).Columns.FromKey("DATASIZE").Width = Unit.Percentage(5)
            uwgFiles.Bands(0).Columns.FromKey("DATASIZE").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
            uwgFiles.Bands(0).Columns.FromKey("COMENT").Header.Caption = oTextos.Rows(4).Item("TEXT_" & sIdi)
            uwgFiles.Bands(0).Columns.FromKey("COMENT").Width = Unit.Percentage(40)
            uwgFiles.Bands(0).Columns.FromKey("COMENT").CellMultiline = Infragistics.WebUI.UltraWebGrid.CellMultiline.Yes
            uwgFiles.Bands(0).Columns.FromKey("COMENT").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
            uwgFiles.Bands(0).Columns.FromKey("ELIMINAR").Header.Caption = oTextos.Rows(7).Item("TEXT_" & sIdi)
            uwgFiles.Bands(0).Columns.FromKey("ELIMINAR").Width = Unit.Percentage(3)
            uwgFiles.Bands(0).Columns.FromKey("SUSTITUIR").Header.Caption = oTextos.Rows(6).Item("TEXT_" & sIdi)
            uwgFiles.Bands(0).Columns.FromKey("SUSTITUIR").Width = Unit.Percentage(3)
            uwgFiles.Bands(0).Columns.FromKey("DESCARGAR").Header.Caption = oTextos.Rows(5).Item("TEXT_" & sIdi)
            uwgFiles.Bands(0).Columns.FromKey("DESCARGAR").Width = Unit.Percentage(3)
            uwgFiles.Bands(0).Columns.FromKey("NOM").Width = Unit.Percentage(40)
            uwgFiles.Bands(0).Columns.FromKey("IDIOMA").Hidden = True
            uwgFiles.Bands(0).Columns.FromKey("ADJUN").Hidden = True
            uwgFiles.Bands(0).Columns.FromKey("CAMPO").Hidden = True
            uwgFiles.Bands(0).Columns.FromKey("RUTA").Hidden = True
            uwgFiles.Bands(0).Columns.FromKey("PORTAL").Hidden = True
            uwgFiles.Bands(0).Columns.FromKey("COMENTSalto").Hidden = True
            If bReadOnly Then
                uwgFiles.Bands(0).Columns.FromKey("ELIMINAR").ServerOnly = True
                uwgFiles.Bands(0).Columns.FromKey("SUSTITUIR").ServerOnly = True
                Me.cmdAnyadir.Visible = False
            Else
                uwgFiles.Bands(0).Columns.FromKey("ELIMINAR").Move(11)
                uwgFiles.Bands(0).Columns.FromKey("SUSTITUIR").Move(11)
            End If
            uwgFiles.Bands(0).Columns.FromKey("DESCARGAR").Move(11)
            Me.uwgFiles.DataBind()

            Dim sScript As String
            Dim includeScript As String
            Dim sClientId As String = Replace(Me.uwgFiles.ClientID, "_", "x")
            sScript = "document.all(""" + sClientId + "_addBox"").parentElement.style.height=""0px"""
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "startItKey", sScript)


        End Sub

        ''' <summary>
        ''' Inicializar cada linea del grid. Bien con un icono, bien en una columna oculta traduce los saltos de 
        ''' linea por su codigo javascript /n
        ''' </summary>
        ''' <param name="sender">parametro de sistema</param>
        ''' <param name="e">parametro de sistema</param>     
        ''' <returns>Nada</returns>
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub uwgFiles_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgFiles.InitializeRow
            e.Row.Cells.FromKey("COMENTSalto").Value = Replace(e.Row.Cells.FromKey("COMENTSalto").Value, vbCrLf, "/n")

            e.Row.Cells.FromKey("ELIMINAR").Value = "<img border=""0"" style=""cursor:pointer"" align=""center"" src='" & ConfigurationManager.AppSettings("rutaPM") + "_common/images/eliminar.gif'" & " onclick=""eliminarAdjunto('" & e.Row.Cells.FromKey("TIPO").Value & "'," & e.Row.Cells.FromKey("ID").Value & ");return false;"">"
            e.Row.Cells.FromKey("SUSTITUIR").Value = "<img border=""0"" style=""cursor:pointer"" align=""center"" src='" & ConfigurationManager.AppSettings("rutaPM") + "_common/images/sustituir.gif'" & "  onclick=""sustituirAdjunto('" & e.Row.Cells.FromKey("TIPO").Value & "'," & e.Row.Cells.FromKey("ID").Value & ",'" & e.Row.Cells.FromKey("NOM").Value & "','" & e.Row.Cells.FromKey("COMENTSalto").Value & "' );return false;"">"
            e.Row.Cells.FromKey("DESCARGAR").Value = "<img border=""0"" style=""cursor:pointer"" align=""center"" src='" & ConfigurationManager.AppSettings("rutaPM") + "_common/images/descargar.gif'" & "  onclick=""descargarAdjunto('" & e.Row.Cells.FromKey("TIPO").Value & "'," & e.Row.Cells.FromKey("ID").Value & ");return false;"">"
        End Sub
    End Class
End Namespace