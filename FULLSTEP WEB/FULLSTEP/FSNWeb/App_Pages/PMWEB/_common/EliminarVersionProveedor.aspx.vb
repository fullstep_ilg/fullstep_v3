﻿Public Class EliminarVersionProveedor
    Inherits FSNPage

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
"<script language=""{0}"">{1}</script>"

    ''' <summary>
    ''' Carga de pantalla. Realmente lo q hace es q por ajax sincrono elimina la version guardada sin enviar
    ''' del proveedor antes de poder grabar lo q esta editando el usuario en DetalleNoConformidad.aspx 
    ''' ó DetalleCertificado.aspx.
    ''' </summary>
    ''' <param name="sender">pantalla</param>
    ''' <param name="e">parametro de sistema</param>     
    ''' <remarks>Llamada desde: DetalleNoConformidad.aspx DetalleCertificado.aspx; Tiempo máximo: 0,1</remarks>
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Expires = -1
        If Not Request("NoConformidad") Is Nothing Then
            'Elimina la versión de la no conformidad
            Dim oNoConformidad As FSNServer.NoConformidad

            oNoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
            oNoConformidad.ID = Request("NoConformidad")
            oNoConformidad.Instancia = Request("Instancia")

            oNoConformidad.Version = Request("Version")

            oNoConformidad.EliminarVersionesNoConformidad(FSNUser.CodPersona, FSNUser.Email, True)

            oNoConformidad = Nothing
        Else
            'Elimina la versión del certificado
            Dim oCertificado As FSNServer.Certificado

            oCertificado = FSNServer.Get_Object(GetType(FSNServer.Certificado))
            oCertificado.ID = Request("Certificado")
            oCertificado.Instancia = Request("Instancia")
            oCertificado.Version = Request("Version")

            oCertificado.EliminarVersionesCertificado(FSNUser.CodPersona, FSNUser.Email, True)

            oCertificado = Nothing
        End If
    End Sub

    ''' <summary>
    ''' Se ha hecho una llamada Ajax sincrona a esta pantalla, esta función devuelve el resultado de la ejecución
    ''' </summary>
    ''' <param name="Respuesta">Respuesta de la ejecución</param>    
    ''' <remarks>Llamada desde: Page_Load; Tiempo máximo: 0</remarks>
    Private Sub Responder(ByVal Respuesta As Integer)
        Response.Clear()
        Response.ContentType = "text/plain"
        Response.Write(Respuesta.ToString)
        Response.End()
    End Sub
End Class