﻿Public Partial Class BuscadorCentroCosteServer
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim oCentrosCoste As FSNServer.CentrosCoste
        Dim sDen As String
        Dim sCod As String
        Dim sCC As String
        Dim fsEntryCC As String
        Dim bVerUON As Boolean

        sCC = Request("idCC")
        fsEntryCC = Request("fsEntryCC")
        If Request("VerUON") <> Nothing Then
            bVerUON = Request("VerUON")
        End If

        If sCC <> Nothing Then
            If InStr(sCC, " - ") > 0 Then sCC = Left(sCC, InStr(sCC, " - "))

            oCentrosCoste = FSNServer.Get_Object(GetType(FSNServer.CentrosCoste))
            oCentrosCoste.BuscarCentrosCoste(Usuario.Cod, Idioma, sCC, bVerUON)

            If oCentrosCoste.Data.Tables(0).Rows.Count > 0 Then
                sCod = oCentrosCoste.Data.Tables(0).Rows(0).Item("COD")
                sDen = oCentrosCoste.Data.Tables(0).Rows(0).Item("DEN")
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>ponerDen_Correcto('" & fsEntryCC & "','" & sDen & "','" & sCod & "')</script>")
            Else
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>borrarValorCentroCoste('" & fsEntryCC & "', true)</script>")
            End If
        Else
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>borrarValorCentroCoste('" & fsEntryCC & "', false)</script>")
        End If
    End Sub

End Class