<%@ Page Language="vb" AutoEventWireup="false" Codebehind="LinkMonedas.aspx.vb" Inherits="Fullstep.FSNWeb.LinkMonedas" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<script type="text/javascript">
		/*''' <summary>
		''' Asigna al dataentry el codigo de la moneda y el cambio repercutido
		''' </summary>      
		''' <returns>nada</returns>
		''' <remarks>Llamada desde: wdgMoneda; Tiempo m�ximo: 0</remarks>*/
		function wdgMoneda_RowSelectionChanged(sender, e){	 

            var selectedRows = e.getSelectedRows();
		    var row = selectedRows.getItem(0);

			var cellCOD = row.get_cellByColumnKey("COD");
			var cellDEN = row.get_cellByColumnKey("DEN");
			var cellEQUIV = row.get_cellByColumnKey("EQUIV");
			var p = window.parent
			var oEntry = p.fsGeneralEntry_getById("<%=Request("EntryMoneda")%>")
		
			oEntry.MonRepercutido=cellCOD.get_value()
			oEntry.CambioRepercutido=cellEQUIV.get_value()
		
			var oEntryBt = p.document.getElementById("<%=Request("EntryBoton")%>")
			oEntryBt.innerHTML=cellCOD.get_value()

			var oFrame = p.document.getElementById("<%=Request("FrameId")%>")
            //Pongo una peque�a espera para asegurar que no cierro el frame antes de que el evento termine
            setTimeout(function(){oFrame.parentNode.removeChild(oFrame)}, 100);
			//oFrame.parentNode.removeChild(oFrame);
		}
	</script>	
	<body class="EstadosFondo2">
		<form id="Form1" method="post" runat="server">
            <asp:ScriptManager ID="scrMonedas" runat="server"></asp:ScriptManager>
            <div style="Z-INDEX: 101; LEFT: 0px; POSITION: absolute; TOP: 0px; WIDTH: 199px; HEIGHT: 199px"
				class="cabeceraSolicitud igTreeEnTab">
			    <ig:WebDataGrid id="wdgMoneda" runat="server" Width="100%" Height="199px" ShowHeader="false"
                AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true"  EnableAjaxViewState="false" EnableDataViewState="true">
                    <Behaviors>
                        <ig:Selection CellClickAction="Row" RowSelectType="Single" Enabled="true">
                            <SelectionClientEvents RowSelectionChanged="wdgMoneda_RowSelectionChanged" />
                        </ig:Selection>  
                    </Behaviors>
				</ig:WebDataGrid>
            </div>
		</form>
	</body>
</html>

