﻿Public Partial Class popupDesglose_Mover
    Inherits FSNPage

    ''' <summary>
    ''' Cargar la opción de "eliminar/copiar/mover línea" en un desglose al pinchar la flecha de la izda o la dcha.
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">evento de sistema</param>        
    ''' <remarks>Llamada desde: alta/desglose.ascx; Tiempo máximo:0,1</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.popupDesglose

            Me.hypaniadir.Text = Textos(0)
            hypaniadir.Attributes.Add("OnClick", "javascript:aniadir(); return false;")

            Me.hypquitar.Text = Textos(1)
            hypquitar.Attributes.Add("OnClick", "javascript:quitar(); return false;")

            Me.hypMover.Text = Textos(2)
            hypMover.Attributes.Add("OnClick", "javascript:mover(); return false;")
        End If
    End Sub

End Class