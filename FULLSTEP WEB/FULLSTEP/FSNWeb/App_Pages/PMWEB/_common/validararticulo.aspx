<%@ Page Language="vb" AutoEventWireup="false" Codebehind="validararticulo.aspx.vb" Inherits="Fullstep.FSNWeb.validararticulo"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>	
	<script>		
		function articuloIncorrecto()
		{
		var p=window.parent		
		oArt = p.fsGeneralEntry_getById(sArtClientId)	
		if (oArt.getValue()!="")
			alert(arrTextosML[0])
			
		oArt.setValue(null)
		
		oArt.setDataValue(null)
		}
		
	    function articuloValidado()
		{
		var p=window.parent		
            oMat = p.fsGeneralEntry_getById(sMatClientId)

		if (oMat)
			p.mat_seleccionado(sMatClientId , sGMN1, sGMN2, sGMN3, sGMN4, 4 ,sDenGMN,'', TodosNiveles)
		
		oArt = p.fsGeneralEntry_getById(sArtClientId)	
		oArt.setValue(sCodArt + " - " + sDenArt)
		oArt.setDataValue(sCodArt)
		
		if (!oMat)
		{
			var sDenAux = '';	
			s=""
			for (i=0;i<ilGMN1-sGMN1.length;i++)
				s+=" "
			
			sMat = s + sGMN1
			sDenAux +=  ' (' + sGMN1
			if (sGMN2)
			{
				s=""
				for (i=0;i<ilGMN2-sGMN2.length;i++)
					s+=" "
				sMat = sMat + s + sGMN2
				sDenAux += ' - ' + sGMN2
				sDenShort = sGMN2 + ' - ' + sDenGMN
			}
			else
				sDenShort = sGMN1 + ' - ' + sDenGMN

			if (sGMN3)
			{
				s=""
				for (i=0;i<ilGMN3-sGMN3.length;i++)
					s+=" "
				sMat = sMat + s + sGMN3
				sDenAux += ' - ' + sGMN3
				sDenShort = sGMN3 + ' - ' + sDenGMN
			}
			if (sGMN4)
			{
				s=""
				for (i=0;i<ilGMN4-sGMN4.length;i++)
					s+=" "
				sMat = sMat + s + sGMN4
				sDenAux += ' - ' + sGMN4
				sDenShort = sGMN4 + ' - ' + sDenGMN
				
			}
			sDenAux += ')'
			oArt.Dependent.value = sMat;

		}
		
		if (oArt.idEntryPREC) 
			if (oArt.idEntryPREC != '') {
				oPrecio = p.fsGeneralEntry_getById(oArt.idEntryPREC);
				if (oPrecio) {
					oPrecio.setValue(dPrecioUltADJ);
					oPrecio.setDataValue(dPrecioUltADJ);
				}
			}

				
		if (oArt.idEntryPROV) 
			if (oArt.idEntryPROV != '') {
				oProveedor = p.fsGeneralEntry_getById(oArt.idEntryPROV);
				if (oProveedor) { 
						oProveedor.setDataValue(sCodPROV);
						if (sCodPROV != '')
							oProveedor.setValue(sCodPROV + " - " + sDenPROV);
						else
							oProveedor.setValue('');			
				}	
			}		
	
		
									
		if (oArt.idEntryUNI)
			if (oArt.idEntryUNI!= '') {
				oUnidad = p.fsGeneralEntry_getById(oArt.idEntryUNI);
				if (oUnidad) {
					oUnidad.setDataValue(sUNI);
					oUnidad.setValue(sDenUNI)
				}else
					if (oArt.UnidadDependent) {
						oArt.UnidadDependent.value = sUnidad;
					}
			}
		
		}
		
	</script>	
	<body>
		<form id="Form1" method="post" runat="server">
		</form>
	</body>
</html>
