

    Public Class detallesolicitudpadre
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblIdentificador As System.Web.UI.WebControls.Label
        Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
        Protected WithEvents lblImporteAprob As System.Web.UI.WebControls.Label
        Protected WithEvents lblImporteAcum As System.Web.UI.WebControls.Label
    Protected WithEvents cmdCerrar As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents txtIdentificador As System.Web.UI.WebControls.Label
        Protected WithEvents txtTitulo As System.Web.UI.WebControls.Label
        Protected WithEvents txtImporteAprob As System.Web.UI.WebControls.Label
        Protected WithEvents txtImporteAcum As System.Web.UI.WebControls.Label

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oUser As FSNServer.User = Session("FSN_User")
            Dim sIdi As String = oUser.Idioma.ToString()
            Dim idSolPadre As Integer
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If
            If Request("idSolPadre") = "" Then
                idSolPadre = 0
            Else
                idSolPadre = Request("idSolPadre")
            End If
            Dim sMon As String
            If Request("Moneda") <> "" Then
                sMon = Request("Moneda")
            Else
                sMon = "EUR"
            End If

            Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
            oDict.LoadData(TiposDeDatos.ModulosIdiomas.DetalleSeguimientoPadre, sIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)

         Me.lblIdentificador.Text = oTextos.Rows(1).Item(1)  'Identificador
            Me.lblTitulo.Text = oTextos.Rows(2).Item(1)         'T�tulo
            Me.lblImporteAprob.Text = oTextos.Rows(3).Item(1)    'Importe aprobado
            Me.lblImporteAcum.Text = oTextos.Rows(4).Item(1)   'Importe acumulado
            Me.cmdCerrar.Value = oTextos.Rows(5).Item(1)      'Cerrar

            If idSolPadre > 0 Then
                Dim oInstancias As FSNServer.Instancias
                oInstancias = FSWSServer.Get_Object(GetType(FSNServer.Instancias))
                oInstancias.DevolverSolicitudesPadre(sIdi, sMon, idSolPadre)
                If oInstancias.Data.Tables(0).Rows.Count > 0 Then
                    Me.txtIdentificador.Text = idSolPadre
                    Me.txtTitulo.Text = oInstancias.Data.Tables(0).Rows(0).Item(1)
                    Me.txtImporteAprob.Text = FSNLibrary.FormatNumber(oInstancias.Data.Tables(0).Rows(0).Item(3), oUser.NumberFormat) & " " & DBNullToStr(oInstancias.Data.Tables(0).Rows(0).Item(6))
                    Me.txtImporteAcum.Text = FSNLibrary.FormatNumber(oInstancias.Data.Tables(0).Rows(0).Item(5), oUser.NumberFormat) & " " & DBNullToStr(oInstancias.Data.Tables(0).Rows(0).Item(6))
                End If
                oInstancias = Nothing
            Else
                Me.txtIdentificador.Text = idSolPadre
                Me.txtTitulo.Text = ""
                Me.txtImporteAprob.Text = 0
                Me.txtImporteAcum.Text = 0

            End If

        End Sub

    End Class

