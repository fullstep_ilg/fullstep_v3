Public Class popUpTexto
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
	Protected WithEvents txtTexto As System.Web.UI.WebControls.TextBox
	Protected WithEvents cmdAceptar As System.Web.UI.WebControls.Button

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub


#End Region

    ''' <summary>
    ''' Carga en el textarea el texto
    ''' </summary>
    ''' <remarks>Llamada desde: Cualquier dataentry de texto largo o texto medio; Tiempo = 0seg </remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		ModuloIdioma = TiposDeDatos.ModulosIdiomas.ComentEstado
		txtTexto.Text = Request.QueryString("texto")
		txtTexto.Height = Request.QueryString("height")
		Me.cmdAceptar.Text = Textos(0)
	End Sub

End Class
