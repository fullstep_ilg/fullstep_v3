﻿Public Partial Class atachedfilesDesglose
    Inherits FSNPage
    Dim SoloLectura As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell

        Dim DivContenedor As New HtmlControls.HtmlGenericControl("DIV")
        Dim oTblInput As New System.Web.UI.WebControls.Table
        Dim oTblCellInput As System.Web.UI.WebControls.TableCell
        Dim oTblRowInput As System.Web.UI.WebControls.TableRow

        Dim idCampo As Integer = CInt(Request("idCampo"))
        Dim oCampo As FSNServer.Campo
        
        Dim idAdjuntos As String = String.Empty
        Dim idAdjuntosNew As String = String.Empty
        Dim NombreAdjunto As String = String.Empty
        Dim Instancia As Long
        Dim bFavorito As Boolean
        Dim iTipo As Integer
        Dim EntryID As String
        Dim sDataDefecto As String
        Dim Solicitud As Long

        Try
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.AttachFile
            'Recogemos el QueryString
            If Request("defecto") <> "" Then
                sDataDefecto = Request("defecto")
            End If
            If Request("tipo") <> "" Then
                iTipo = Request("tipo")
            End If
            If Request("instancia") <> "" Then
                Instancia = Request("instancia")
            End If
            If Request("adjuntos") <> "" Then
                idAdjuntos = Request("adjuntos")
            End If
            If Request("adjuntosNew") <> "" Then
                idAdjuntosNew = Request("adjuntosNew")
            End If
            If Request("solicitud") <> "" Then
                Solicitud = Request("solicitud")
            End If
            If Request("EntryID") <> "" Then
                EntryID = Request("EntryID")
            End If
            If Request("readOnly") <> "" Then
                SoloLectura = Request("readOnly")
            End If

            Dim objAdjuntos() As String = System.Text.RegularExpressions.Regex.Split(idAdjuntos, "xx")
            Dim objAdjuntosNew() As String = System.Text.RegularExpressions.Regex.Split(idAdjuntosNew, "xx")

            Dim TotalAdjuntos As Short = IIf(objAdjuntos.Length = 1 And objAdjuntos(0) = "", 0, objAdjuntos.Length) + IIf(objAdjuntosNew.Length = 1 And objAdjuntosNew(0) = "", 0, objAdjuntosNew.Length)

            If idAdjuntos <> "" Then
                idAdjuntos = Replace(idAdjuntos, "xx", ",")
            End If
            If idAdjuntosNew <> "" Then
                idAdjuntosNew = Replace(idAdjuntosNew, "xx", ",")
            End If

            oCampo = FSNServer.Get_Object(GetType(FSNServer.Campo))
            oCampo.Id = idCampo
            If Instancia = 0 Then
                oCampo.Load(Idioma, Solicitud)
            Else
                oCampo.LoadInst(Instancia, Idioma)
            End If

            oTbl = New System.Web.UI.WebControls.Table

            oTbl = Page.Form.FindControl("tblcontenido")

            Me.lblSubTitulo.Text = oCampo.DenSolicitud(Idioma) & " -> " & oCampo.DenGrupo(Idioma)
            Me.lblTitulo.Text = Request("nombreCampo")
           

            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

            Dim oAdjuntos As FSNServer.Adjuntos

            oAdjuntos = FSWSServer.Get_Object(GetType(FSNServer.Adjuntos))


            'Obtenemos lo adjuntos
            If Instancia = 0 Then
                oAdjuntos.Load(iTipo, idAdjuntos, idAdjuntosNew, bFavorito)
            Else
                oAdjuntos.LoadInst(iTipo, idAdjuntos, idAdjuntosNew)
                'Si es una adjunto metido desde Añadir por valor por defecto. La información
                'esta en LINEA_DESGLOSE_ADJUN
                If sDataDefecto <> "" And Not sDataDefecto Is Nothing Then
                    Dim sPatron As String = sDataDefecto
                    Dim sPatron1 As String = sDataDefecto & ",*"
                    Dim sPatron2 As String = "*," & sDataDefecto & ",*"
                    Dim sPatron3 As String = "*," & sDataDefecto
                    If idAdjuntos Like sPatron Or idAdjuntos Like sPatron1 Or idAdjuntos Like sPatron2 Or idAdjuntos Like sPatron3 Then
                        oAdjuntos.Load(iTipo, idAdjuntos, idAdjuntosNew)
                    End If
                End If

            End If

                Dim tablaContenido As New HtmlControls.HtmlTable
                Dim filaContenido As HtmlControls.HtmlTableRow
                Dim celdaContenido As HtmlControls.HtmlTableCell
                tablaContenido.Style("width") = "100%"
                tablaContenido.CellSpacing = 0
                tablaContenido.CellPadding = 3

                'Cargo los adjuntos
                For i = 0 To oAdjuntos.Data.Tables(0).Rows.Count - 1


                    Dim EnlaceFichero As New HtmlControls.HtmlAnchor
                    Dim lblPersona As New Label
                    Dim lblFechaAdjunto As New Label
                    Dim imgEliminar As New HtmlControls.HtmlImage
                    Dim imgSustituir As New HtmlControls.HtmlImage
                    Dim txtComentarios As New TextBox
                    Dim txtComentariosReadOnly As New HtmlControls.HtmlTextArea
                    Dim DivComentarios As New HtmlControls.HtmlGenericControl("DIV")
                    Dim imgPreviaAdjun As New Image
                    Dim hidComentario As New HtmlControls.HtmlInputHidden

                    Try
                        Dim sClaseFila As String = "ugfilatablaImPar"
                        If i Mod 2 = 0 Then
                            sClaseFila = "ugfilatablaPar"
                        End If

                        filaContenido = New HtmlControls.HtmlTableRow
                        filaContenido.Style("width") = "100%"
                        filaContenido.Style("padding-top") = "10px"
                        filaContenido.Attributes.Add("class", sClaseFila)

                        'Enlace fichero
                        EnlaceFichero.InnerText = oAdjuntos.Data.Tables(0).Rows(i)("NOM") & " (" & oAdjuntos.Data.Tables(0).Rows(i)("DATASIZE") & " kb)"
                        EnlaceFichero.ID = oAdjuntos.Data.Tables(0).Rows(i)("ID")
                        EnlaceFichero.Attributes("onclick") = "descargarAdjunto('" & oAdjuntos.Data.Tables(0).Rows(i)("id") & "', '" & oAdjuntos.Data.Tables(0).Rows(i)("tipo") & "', '" & Instancia.ToString & "','" & EntryID & "')"
                        EnlaceFichero.Style("text-decoration") = "underline"
                        'EnlaceFichero.Attributes.Add("class", "enlaceAdj")
                        EnlaceFichero.HRef = "#"
                        EnlaceFichero.Style("color") = "#0000FF"
                        celdaContenido = New HtmlControls.HtmlTableCell
                        celdaContenido.Width = Unit.Percentage(45).ToString

                        'Input Hidden con el ID y tipo del adjunto y la instancia
                        Dim inputID As New HtmlControls.HtmlInputHidden
                        inputID.ID = "Adj_" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                        inputID.Value = oAdjuntos.Data.Tables(0).Rows(i)("ID") & "###" & oAdjuntos.Data.Tables(0).Rows(i)("tipo") & "###" & Instancia.ToString
                        celdaContenido.Controls.Add(EnlaceFichero)
                        celdaContenido.Controls.Add(inputID)
                        filaContenido.Cells.Add(celdaContenido)

                        'Fecha en la que se adjunto el documento
                        celdaContenido = New HtmlControls.HtmlTableCell
                        celdaContenido.Width = Unit.Percentage(20).ToString
                        lblFechaAdjunto.Text = oAdjuntos.Data.Tables(0).Rows(i)("FECALTA").ToString
                        lblFechaAdjunto.CssClass = "fntLogin"
                        celdaContenido.Controls.Add(lblFechaAdjunto)
                        filaContenido.Cells.Add(celdaContenido)


                        'Persona que adjunto el documento
                        celdaContenido = New HtmlControls.HtmlTableCell
                        celdaContenido.Width = Unit.Percentage(25).ToString
                        lblPersona.Text = oAdjuntos.Data.Tables(0).Rows(i)("NOMBRE").ToString
                        lblPersona.CssClass = "fntLogin"
                        celdaContenido.Controls.Add(lblPersona)
                        filaContenido.Cells.Add(celdaContenido)


                        'Eliminar adjunto
                        If Not SoloLectura = "true" Then
                            celdaContenido = New HtmlControls.HtmlTableCell
                            celdaContenido.Width = Unit.Percentage(3).ToString
                            celdaContenido.Align = "right"
                            imgEliminar.Alt = "Eliminar"
                            imgEliminar.ID = "Eliminar_" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                        imgEliminar.Src = ConfigurationManager.AppSettings("ruta") & "images/eliminar.gif"
                            imgEliminar.Attributes("onclick") = "javascript:p=window.opener;p.borrarAdjunto('" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "', '" & oAdjuntos.Data.Tables(0).Rows(i)("tipo").ToString & "', '" & Instancia.ToString & "','" & EntryID & "','" & Page.AppRelativeVirtualPath & "'); borrarAdjuntoDetalle('" + EntryID + "', '" + SoloLectura + "','" & Page.AppRelativeVirtualPath & "')"
                            celdaContenido.Controls.Add(imgEliminar)
                            filaContenido.Cells.Add(celdaContenido)
                        End If

                        'Sustituir adjunto
                        If Not SoloLectura = "true" Then
                            celdaContenido = New HtmlControls.HtmlTableCell
                            celdaContenido.Width = Unit.Percentage(3).ToString
                            celdaContenido.Align = "right"
                            imgSustituir.ID = "Sustituir_" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                            imgSustituir.Alt = "Sustituir"
                        imgSustituir.Src = ConfigurationManager.AppSettings("rutaPM") & "_common/images/sustituir.gif"
                        imgSustituir.Attributes("onclick") = "javascript:sustituirAdjuntoDetalle('" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "', '" & oAdjuntos.Data.Tables(0).Rows(i)("tipo").ToString & "', '" & Instancia.ToString & "','" & EntryID & "','" & Page.AppRelativeVirtualPath & "','" & Replace(Server.UrlEncode(oAdjuntos.Data.Tables(0).Rows(i)("NOM").ToString()), "'", "\'") & "')"
                            celdaContenido.Controls.Add(imgSustituir)
                            filaContenido.Cells.Add(celdaContenido)
                        End If

                        tablaContenido.Rows.Add(filaContenido)

                        'Añado el textArea para los comentarios
                        filaContenido = New HtmlControls.HtmlTableRow
                        filaContenido.Attributes.Add("class", sClaseFila)
                        hidComentario.ID = EntryID & "__hidComent" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                        hidComentario.Value = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"))
                        If Not SoloLectura.ToLower = "true" Then
                            txtComentarios.ID = EntryID & "__text" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                            txtComentarios.TextMode = TextBoxMode.MultiLine
                            txtComentarios.Height = Unit.Pixel(33)
                            txtComentarios.MaxLength = 500
                            txtComentarios.Style("overflow") = "hidden"
                            txtComentarios.Text = oAdjuntos.Data.Tables(0).Rows(i)("COMENT")
                            txtComentarios.Attributes("onfocus") = "aumentarTamanoCajaTexto('" + EntryID + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "')"
                            txtComentarios.Attributes("onblur") = "reducirTamanoCajaTexto('" + EntryID + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("TIPO").ToString + "','" + Instancia.ToString + "',1)"
                            txtComentarios.Width = "600"
                            celdaContenido = New HtmlControls.HtmlTableCell
                            celdaContenido.Attributes("colspan") = "5"
                            celdaContenido.Width = "700px"
                            celdaContenido.Align = "center"
                            celdaContenido.Controls.Add(txtComentarios)
                            celdaContenido.Controls.Add(hidComentario)
                            filaContenido.Cells.Add(celdaContenido)
                            tablaContenido.Rows.Add(filaContenido)
                        Else
                            If oAdjuntos.Data.Tables(0).Rows(i)("COMENT").ToString.Trim <> "" Then
                                txtComentariosReadOnly.ID = EntryID & "__text" & oAdjuntos.Data.Tables(0).Rows(i)("ID")
                                txtComentariosReadOnly.Cols = 110
                                txtComentariosReadOnly.Rows = 2
                                Dim bAjustarTamanoCajaTexto As Boolean
                                Dim posicion As Integer

                                posicion = InStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"), Chr(13) & Chr(10))
                                If posicion > 0 Then
                                    posicion = InStr(posicion + 2, oAdjuntos.Data.Tables(0).Rows(i)("COMENT"), Chr(13) & Chr(10))
                                    If posicion > 0 Then
                                        If posicion >= 140 Then
                                            posicion = 140
                                        End If
                                        txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")).Substring(0, posicion - 1) & " ..."
                                        bAjustarTamanoCajaTexto = True
                                    Else
                                        If Len(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")) >= 140 Then
                                            bAjustarTamanoCajaTexto = True
                                            posicion = InStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"), Chr(13) & Chr(10))
                                            If Len(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")) > (posicion + 70) Then
                                                posicion = posicion + 70
                                            Else
                                                posicion = 140
                                            End If
                                            txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")).Substring(0, posicion - 1) & " ..."
                                        Else
                                            txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"))
                                        End If
                                    End If
                                Else
                                    If Len(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")) >= 140 Then
                                        bAjustarTamanoCajaTexto = True
                                        txtComentariosReadOnly.InnerText = DevolverTextoAjustado(DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT")), 70, 140, 2) & " ..."
                                        posicion = txtComentariosReadOnly.InnerText.Length - 4
                                    Else
                                        txtComentariosReadOnly.InnerText = DBNullToStr(oAdjuntos.Data.Tables(0).Rows(i)("COMENT"))
                                    End If

                                End If


                                txtComentariosReadOnly.Style("border") = "none"
                                txtComentariosReadOnly.Style("overflow") = "hidden"
                                txtComentariosReadOnly.Style("background-color") = "Transparent"
                                If bAjustarTamanoCajaTexto Then
                                    txtComentariosReadOnly.Attributes("onmouseover") = "aumentarTamanoCajaTexto('" + EntryID + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "',1)"
                                    txtComentariosReadOnly.Attributes("onblur") = "reducirTamanoCajaTexto('" + EntryID + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("ID").ToString + "', '" + oAdjuntos.Data.Tables(0).Rows(i)("TIPO").ToString + "','" + Instancia.ToString + "',0,'" + (posicion - 1).ToString + "')"
                                End If
                                txtComentariosReadOnly.Attributes("readonly") = "true"
                                txtComentariosReadOnly.Attributes("background-color") = "Transparent"
                                celdaContenido = New HtmlControls.HtmlTableCell
                                celdaContenido.Attributes("colspan") = "3"
                                celdaContenido.Width = "700px"
                                celdaContenido.Align = "center"
                                celdaContenido.Controls.Add(txtComentariosReadOnly)
                                celdaContenido.Controls.Add(hidComentario)
                                filaContenido.Cells.Add(celdaContenido)
                                tablaContenido.Rows.Add(filaContenido)
                            End If
                        End If

                    Catch ex As Exception

                    End Try
                Next

                If Not SoloLectura.ToLower = "true" Then
                    Dim butAñadir As New HtmlControls.HtmlInputButton

                butAñadir.ID = EntryID & "_btnAnadir"
                butAñadir.Value = Textos(8)
                butAñadir.Attributes.Add("class", "botonPMWEB")
                butAñadir.Style("margin-top") = "10px"
                    butAñadir.Attributes("onclick") = "show_atach_file('" + Instancia.ToString() + "', '" + idAdjuntos.ToString + "', '" + iTipo.ToString + "', '" + EntryID + "','" + Page.AppRelativeVirtualPath + "',0)"

                    filaContenido = New HtmlControls.HtmlTableRow
                    celdaContenido = New HtmlControls.HtmlTableCell
                    celdaContenido.Controls.Add(butAñadir)
                    filaContenido.Cells.Add(celdaContenido)
                    tablaContenido.Rows.Add(filaContenido)

                End If


                DivContenedor.ID = EntryID + "_divcontenedor"
                DivContenedor.Controls.Add(tablaContenido)
                oTblCell = New WebControls.TableCell
                oTblCell.Controls.Add(DivContenedor)
                oTblRow = New WebControls.TableRow
                oTblRow.Cells.Add(oTblCell)
                oTbl.Rows.Add(oTblRow)
        Catch ex As Exception

        End Try
    End Sub


   
End Class