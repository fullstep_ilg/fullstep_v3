Imports Office = Microsoft.Office.Core
Imports Microsoft.Office.Interop
Imports VBIDE = Microsoft.Vbe.Interop
Imports System.IO
Imports System.Security.Principal
Public Class attach
    Inherits FSNPage


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim arrPath() As String

        If Request("idContrato") <> "" Then
            TratarAdjuntoContrato()
        ElseIf Request("desgloseActividad") = 1 Then
            BajarPlantillaExcel(CInt(Request("NumFases")))
        Else
            If Request("adjuntosNew") <> "" OrElse Request("adjuntos") <> "" Then
                'Se descargan todos los adjuntos en un fichero comprimido
                Dim NuevosAdjuntos As String()
                Dim Adjuntos() As String
                Dim Tipos() As String

                Dim sPath As String
                Dim NombreFicheroZip As String


                Tipos = Split(Request("tipo"), "xx")

                sPath = ConfigurationManager.AppSettings("temp") + "\" + FSNLibrary.modUtilidades.GenerateRandomPath()
                NuevosAdjuntos = Split(Request("adjuntosNew"), ",")
                Adjuntos = Split(Request("adjuntos"), ",")

                For i As Short = 0 To NuevosAdjuntos.GetUpperBound(0)
                    Dim Tipo As String
                    If NuevosAdjuntos(i) = "" Then Exit For

                    Dim oAdjunto As FSNServer.Adjunto
                    oAdjunto = FSWSServer.Get_Object(GetType(FSNServer.Adjunto))
                    oAdjunto.Id = NuevosAdjuntos(i)

                    'Buscamos el tipo del adjunto que tratamos
                    For x = 0 To Tipos.GetUpperBound(0)
                        Dim pos As Short = Tipos(x).IndexOf(",")
                        If Tipos(x).Substring(0, pos).ToString = NuevosAdjuntos(i).ToString Then
                            Tipo = Tipos(x).Substring(pos + 1)
                        End If
                    Next

                    If Request("instancia") = "0" Or Request("instancia") = "" Then
                        oAdjunto.LoadFromRequest(Tipo)
                        oAdjunto.SaveAdjunToDiskZip(Tipo, sPath)
                    Else
                        oAdjunto.LoadInstFromRequest(Tipo)
                        'Si es una adjunto metido desde A�adir por valor por defecto. La informaci�n esta en LINEA_DESGLOSE_ADJUN
                        If oAdjunto.dataSize = -1000 Then
                            oAdjunto.LoadFromRequest(Tipo)
                            oAdjunto.SaveAdjunToDiskZip(Tipo, sPath)
                        Else
                            oAdjunto.SaveAdjunToDiskZip(Tipo, sPath, True)
                        End If
                    End If
                Next

                For i As Short = 0 To Adjuntos.GetUpperBound(0)
                    Dim Tipo As String
                    If Adjuntos(i) = "" Then Exit For

                    Dim oAdjunto As FSNServer.Adjunto
                    oAdjunto = FSWSServer.Get_Object(GetType(FSNServer.Adjunto))
                    oAdjunto.Id = Adjuntos(i)

                    'Buscamos el tipo del adjunto que tratamos
                    For x = 0 To Tipos.GetUpperBound(0)
                        Dim pos As Short = Tipos(x).IndexOf(",")
                        If Tipos(x).Substring(0, pos).ToString = Adjuntos(i).ToString Then
                            Tipo = Tipos(x).Substring(pos + 1)
                        End If
                    Next

                    If Request("instancia") = "0" Or Request("instancia") = "" Then
                        oAdjunto.LoadFromRequest(Tipo)
                        oAdjunto.SaveAdjunToDiskZip(Tipo, sPath)
                    Else
                        oAdjunto.LoadInstFromRequest(Tipo)
                        'Si es una adjunto metido desde A�adir por valor por defecto. La informaci�n esta en LINEA_DESGLOSE_ADJUN
                        If oAdjunto.dataSize = -1000 Then
                            oAdjunto.LoadFromRequest(Tipo)
                            oAdjunto.SaveAdjunToDiskZip(Tipo, sPath)
                        Else
                            oAdjunto.SaveAdjunToDiskZip(Tipo, sPath, True)
                        End If
                    End If
                Next

                NombreFicheroZip = FSNLibrary.modUtilidades.Comprimir(sPath, "*.*", sPath, True, False)

                Dim FicheroZip As String() = Split(NombreFicheroZip, "##")
                arrPath = sPath.Split("\")
                Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "_Common/download.aspx?path=" + arrPath(UBound(arrPath)) + "&nombre=" + FicheroZip(0) + "&datasize=" + FicheroZip(1))
            Else
                'Solo se descarga un adjunto
                Dim IdAdjunto As Integer = Request("id")

                Dim oAdjunto As FSNServer.Adjunto
                oAdjunto = FSWSServer.Get_Object(GetType(FSNServer.Adjunto))
                oAdjunto.Id = Request("Id")

                Dim sPath As String

                If Request("Proce") <> "" And Request("Proce") <> Nothing Then
                    oAdjunto.LoadFromProceso(Request("Anyo"), Request("GMN1"), Request("Proce"), IIf(Request("Grupo") = "", Nothing, Request("Grupo")), Request("Item"), IIf(Request("Prove") = "", Nothing, Request("Prove")), Request("Ofe"))
                    sPath = oAdjunto.SaveAdjunToDiskProce(Request("Anyo"), Request("GMN1"), Request("Proce"), IIf(Request("Grupo") = "", Nothing, Request("Grupo")), Request("Item"), IIf(Request("Prove") = "", Nothing, Request("Prove")), Request("Ofe"))
                ElseIf Request("pedido") <> "" And Request("pedido") <> Nothing Then
                    If Request("linea") <> "" And Request("linea") <> Nothing Then
                        oAdjunto.LoadFromPedido(Request("pedido"), Request("linea"))
                    Else
                        oAdjunto.LoadFromPedido(Request("pedido"))
                    End If
                    Select Case Request("tipo")
                        Case 5, 6
                            Dim iTipo As Integer
                            If Request("linea") = "" Or Request("linea") = "0" Or Request("linea") = Nothing Then
                                iTipo = TiposDeDatos.Adjunto.Tipo.Orden_Entrega
                            Else
                                iTipo = TiposDeDatos.Adjunto.Tipo.Linea_Pedido
                            End If
                            Dim sNombre As String = oAdjunto.Nom
                            sPath = ConfigurationManager.AppSettings("temp") + "\" + FSNLibrary.modUtilidades.GenerateRandomPath() + "\"
                            oAdjunto.EscribirADisco(sPath, sNombre, iTipo)
                            sPath = sPath & sNombre
                        Case Else
                            sPath = oAdjunto.SaveAdjunToDisk(Request("tipo"), False)
                    End Select


                ElseIf Request("instancia") = "" Or Request("instancia") = "0" Then
                    oAdjunto.LoadFromRequest(Request("tipo"))
                    sPath = oAdjunto.SaveAdjunToDisk(Request("tipo"))
                Else
                    oAdjunto.LoadInstFromRequest(Request("tipo"))

                    'Si es una adjunto metido desde A�adir por valor por defecto. La informaci�n
                    'esta en LINEA_DESGLOSE_ADJUN
                    If oAdjunto.dataSize = -1000 Then
                        oAdjunto.LoadFromRequest(Request("tipo"))
                        sPath = oAdjunto.SaveAdjunToDisk(Request("tipo"))
                    Else
                        sPath = oAdjunto.SaveAdjunToDisk(Request("tipo"), True)
                    End If
                End If

                arrPath = sPath.Split("\")
                Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "_Common/download.aspx?path=" + arrPath(UBound(arrPath) - 1) + "&nombre=" + Server.UrlEncode(arrPath(UBound(arrPath))) + "&datasize=" + oAdjunto.dataSize.ToString)
            End If
        End If
    End Sub

    Private Sub TratarAdjuntoContrato()

        Dim bDescargar As Boolean
        Dim id As Integer
        Dim oAdjunto As FSNServer.Adjunto
        Dim oFolder As System.IO.Directory

        oAdjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))

        Dim sFolder As String

        sFolder = FSNLibrary.modUtilidades.GenerateRandomPath()

        If Page.IsPostBack Then
            Page.Response.Redirect(Request.Url.AbsoluteUri)
        End If

        id = Request("Instancia")
        ' comprueba PARGEN_INTERNO.ACCESO_LCX=true 
        If FSNServer.TipoAcceso.gbAccesoLCX Then
            bDescargar = ComprobarDescargaFicContratoLCX(FSNUser.Cod, FSNUser.PerfilLCX, id, FSNUser.EmpresaLCX)
        Else
            bDescargar = True
        End If
        If bDescargar = True Then
            Dim impersonationContext As WindowsImpersonationContext

            Dim sPath As String = ConfigurationManager.AppSettings("temp") + "\" + sFolder

            If Not oFolder.Exists(sPath) Then oFolder.CreateDirectory(sPath)

            sPath = sPath & "\" & Server.UrlDecode(Request("NombreAdjunto"))
            FSNServer.Impersonate()

            impersonationContext = RealizarSuplantacion(FSNServer.ImpersonateLogin, FSNServer.ImpersonatePassword, FSNServer.ImpersonateDominio)

            Dim buffer As Byte()

            buffer = oAdjunto.LeerContratoAdjunto(Request("idContrato"), Request("idArchivoContrato"))

            System.IO.File.WriteAllBytes(sPath, buffer)

            If Not impersonationContext Is Nothing Then
                impersonationContext.Undo()
            End If

            Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "_Common/download.aspx?idArchivoContrato=" & Request("idArchivoContrato") & "&IdContrato=" & Request("idContrato") & "&path=" & sFolder & "&nombre=" & Request("NombreAdjunto") & "&datasize=" & Request("datasize") & "&Instancia=" & Request("Instancia") & "&Descargable=1") 'Envia 1 si es descargable
        Else
            Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "_Common/download.aspx?idArchivoContrato=" & Request("idArchivoContrato") & "&IdContrato=" & Request("idContrato") & "&path=" & sFolder & "&nombre=" & Request("NombreAdjunto") & "&datasize=" & Request("datasize") & "&Instancia=" & Request("Instancia") & "&Descargable=0") 'Envia 0 si no es descargable
        End If
    End Sub
    Protected Sub Render(ByVal writer As Object)
        Dim sw As IO.StringWriter = New IO.StringWriter
        Dim htmlWriter As HtmlTextWriter = New HtmlTextWriter(sw)
        MyBase.Render(htmlWriter)
        Dim html As String = sw.ToString
        Dim startPoint As Integer = html.IndexOf("<input type=""hidden"" name=""__VIEWSTATE""")
        If (startPoint >= 0) Then
            Dim endPoint As Integer = (html.IndexOf("/>", startPoint) + 2)
            Dim viewStateInput As String = html.Substring(startPoint, (endPoint - startPoint))
            html = html.Remove(startPoint, (endPoint - startPoint))
            Dim formEndStart As Integer = html.IndexOf("</form>")
            If (formEndStart >= 0) Then
                html = html.Insert(formEndStart, String.Format("{0}", viewStateInput))
            End If
        End If
        writer.Write(html)
    End Sub
    ''' <summary>
    ''' comprueba si el usuario puede descargar el archivo adjunto
    ''' </summary>
    ''' <param name="usuario">string del usuario actual</param>
    ''' <param name="perfil">string con el perfil del usuario pueden ser "Consulta" '"Gestor" "Administrador" "Auditor"</param>
    ''' <param name="instancia">int con el la instancia actual</param>
    ''' <param name="empresa">string con la empresa</param>
    ''' <returns>Una variable booleana que indica si podemos o no descargar el archivo adjunto</returns>
    ''' <remarks></remarks>
    Public Function ComprobarDescargaFicContratoLCX(ByVal usuario As String, ByVal perfil As String, ByVal instancia As Integer, Optional ByVal empresa As Integer = 0) As Boolean
        Dim bpuedoDescargar As Boolean
        Dim oEmpresa As FSNServer.Empresa
        oEmpresa = FSNServer.Get_Object(GetType(FSNServer.Empresa))

        bpuedoDescargar = False

        If (DBNullToStr(perfil)).Equals("auditor") Then
            bpuedoDescargar = True
        Else
            bpuedoDescargar = oEmpresa.ComprobarDescargaLCX(usuario, perfil, instancia, empresa)
        End If
        Return bpuedoDescargar
    End Function

    Private Sub BajarPlantillaExcel(ByVal NumFases As Short)
        Dim sRutaPlantilla As String = ConfigurationManager.AppSettings("Ruta_Plantilla_Proyecto")
        Dim sNombrePlantilla As String = ConfigurationManager.AppSettings("Nombre_Plantilla_Proyecto")
        Dim sRutaTemp As String = ConfigurationManager.AppSettings("temp")

        Dim oProyecto As FSNServer.Proyecto
        oProyecto = FSNServer.Get_Object(GetType(FSNServer.Proyecto))

        Dim sCarpeta As String
        sCarpeta = oProyecto.CrearPlantillaExcel(NumFases, Idioma, sRutaPlantilla & sNombrePlantilla, sRutaTemp, sNombrePlantilla, FSNServer.LongitudesDeCodigos)

        If sCarpeta <> "" Then
            Dim arrDatos() As String
            arrDatos = sCarpeta.Split("#")
            Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "_Common/download.aspx?path=" & arrDatos(0) & "&nombre=" & sNombrePlantilla + "&datasize=" + arrDatos(1))
        End If
    End Sub
End Class

