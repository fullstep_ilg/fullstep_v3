<%@ Page Language="vb" AutoEventWireup="false" Codebehind="presupfavserver.aspx.vb" Inherits="Fullstep.FSNWeb.presupfavserver"%>
<%@ Register TagPrefix="igtbl" Namespace="Infragistics.WebUI.UltraWebGrid" Assembly="Infragistics.WebUI.UltraWebGrid.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
        <style type="text/css">
		    html,body,form
		    {
		        width:100%;
		        height:100%;
		    }
		    #uwgPresupFav_div
		    {
		        height :200px !important;
		    }
		    table.cabeceraSolicitud
		    {
		        padding-top :0px;
		    }
		</style>
	</head>
	<script type="text/javascript">
		function SeleccionarPresupuesto(sTexto, sValor) {
			var p = window.parent;
			var oEntry = p.fsGeneralEntry_getById("<%=Request("Entry")%>");
			p.pres_seleccionado(oEntry.id, sValor, sTexto, '0', oEntry.idCampo);
			if (p.document.getElementById("<%=Request("FrameId")%>")){
				var oFrame = p.document.getElementById("<%=Request("FrameId")%>")
				oFrame.parentNode.removeChild(oFrame);
			}
		}

		function uwgPresupFav_CellClickHandler(gridName, cellId, button){
			//Add code to handle your event here.
			var row = igtbl_getRowById(cellId);
			var p = window.parent;
			var oEntry = p.fsGeneralEntry_getById("<%=Request("Entry")%>");
			var sTexto='';
			if (oEntry.tipoGS==110 || oEntry.tipoGS==111) sTexto+=row.getCell(12).getValue() + ' - ';
			var iNivel=row.getCell(1).getValue();
			sTexto+=row.getCell(3).getValue();
			if (iNivel>1) sTexto+=' - ' + row.getCell(4).getValue();
			if (iNivel>2) sTexto+=' - ' + row.getCell(5).getValue();
			if (iNivel>3) sTexto+=' - ' + row.getCell(6).getValue();
			sTexto+=' (100,00%); ';
			var sValor=iNivel.toString() + '_' + row.getCell(2).getValue() + '_1';
			p.pres_seleccionado(oEntry.id, sValor, sTexto, '0', oEntry.idCampo);
			
			if (p.document.all("<%=Request("FrameId")%>")){
				var oFrame = p.document.all("<%=Request("FrameId")%>")
				oFrame.parentNode.removeChild(oFrame);
			}
		}
	</script>	
	<body class="EstadosFondo2" topmargin="0" leftmargin="0" bottommargin="0" rightmargin="0">
		<form id="Form1" method="post" runat="server">
		<table cellpadding="0" cellspacing="0" width="100%" style="overflow:auto; height:100% " class="cabeceraSolicitud igTreeEnTab"><tr>
            <td width="100%" style="height:100%">
			<igtbl:ultrawebgrid id="uwgPresupFav" runat="server" Height="190px" Width="100%">
				<DisplayLayout RowHeightDefault="20px" Version="4.00" GridLinesDefault="None" ScrollBar="Auto"
					BorderCollapseDefault="Separate" Name="uwgPresupFav" ReadOnly="LevelTwo">
					<HeaderStyleDefault BorderStyle="Solid" BackColor="LightGray">
						<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
					</HeaderStyleDefault>
					<FrameStyle Width="100%" BorderWidth="1px" Font-Size="8pt" BorderStyle="None"
						Height="100%" CssClass="cabeceraSolicitud igTreeEnTab"></FrameStyle>
					<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
						<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
					</FooterStyleDefault>
					<EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
					<RowStyleDefault BorderWidth="1px" BorderColor="Gray" BorderStyle="Solid">
						<Padding Left="3px"></Padding>
						<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
					</RowStyleDefault>
				</DisplayLayout>
				<Bands>
					<igtbl:UltraGridBand ColHeadersVisible="No" CellClickAction="RowSelect" RowSelectors="No"></igtbl:UltraGridBand>
				</Bands>
			</igtbl:ultrawebgrid>
		</td></tr></table>
		</form>
	</body>
</html>
