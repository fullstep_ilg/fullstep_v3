Public Class solicitudesserver
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents IDCONTROL As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltitulo As System.Web.UI.WebControls.Label
    Protected WithEvents uwgSolicitudes As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
    Protected WithEvents cmdAceptar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents lblSolicitudes As System.Web.UI.WebControls.Label
    Protected WithEvents cmdBuscar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents txtUnidadOrg As DataEntry.GeneralEntry
    Protected WithEvents lblUnidadOrg As System.Web.UI.WebControls.Label
    Protected WithEvents txtPeticionario As DataEntry.GeneralEntry
    Protected WithEvents ugtxtTipoSolicitudes As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents lblIdentificador As System.Web.UI.WebControls.Label
    Protected WithEvents lblDescripcion As System.Web.UI.WebControls.Label
    Protected WithEvents txtIDSolicitud As DataEntry.GeneralEntry
    Protected WithEvents txtDescripcion As DataEntry.GeneralEntry
    Protected WithEvents lblPeticionario As System.Web.UI.WebControls.Label
    Protected WithEvents tblFiltros As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblIdTipoSolicitud As System.Web.UI.WebControls.Label
    Protected WithEvents lblTipoSolicitud As System.Web.UI.WebControls.Label

    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim idRefTipoSol As Integer

        If Not IsPostBack Then
            IDCONTROL.Value = Request("idDataEntry")
            idRefTipoSol = Request("idRefSol")

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.BusquedaSolicitudes

            Me.lbltitulo.Text = Textos(0)
            Me.lblTipoSolicitud.Text = Textos(1)  'Tipo de Solicitud
            Me.lblIdentificador.Text = Textos(2)  'Identificador
            Me.lblDescripcion.Text = Textos(3)    'Descripcion:
            Me.lblPeticionario.Text = Textos(4)   'Peticionario:
            Me.lblUnidadOrg.Text = Textos(5)           'Unidad org:
            Me.lblSolicitudes.Text = Textos(6)    'Solicitudes
            Me.cmdBuscar.Value = Textos(7)
            Me.cmdAceptar.Value = Textos(8)

            cmdAceptar.Visible = False

            If FSNUser.PMRestRefAbiertasUO = True Then
                lblUnidadOrg.Visible = False
                txtUnidadOrg.Visible = False
            End If

            If idRefTipoSol > 0 Then
                lblIdTipoSolicitud.Visible = True
                ugtxtTipoSolicitudes.Visible = False
                Dim oSolicitud As FSNServer.Solicitud

                oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
                oSolicitud.ID = idRefTipoSol
                oSolicitud.Load(Idioma)

                lblIdTipoSolicitud.Text = oSolicitud.Codigo & " - " & oSolicitud.Den(Idioma)

            Else
                ugtxtTipoSolicitudes.Visible = True
                lblIdTipoSolicitud.Visible = False
            End If
        End If

        Dim oSolicitudes As FSNServer.Solicitudes

        oSolicitudes = FSNServer.Get_Object(GetType(FSNServer.Solicitudes))
        oSolicitudes.LoadData(Usuario.Cod, Idioma, bEsCombo:=1, sCodPer:=Usuario.CodPersona)

        ugtxtTipoSolicitudes.TextField = "DEN"
        ugtxtTipoSolicitudes.ValueField = "ID"
        Dim ugtxtTipoSolicitudes_wdg As Infragistics.Web.UI.GridControls.WebDataGrid = ugtxtTipoSolicitudes.Controls(0).Controls(0).FindControl("ugtxtTipoSolicitudes_wdg")
        ugtxtTipoSolicitudes_wdg.DataSource = oSolicitudes.Data
        ugtxtTipoSolicitudes_wdg.DataBind()
        ugtxtTipoSolicitudes_wdg.Columns("ID").Hidden = True

        If Not Page.ClientScript.IsClientScriptBlockRegistered("rutaFS") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' </script>")

        oSolicitudes = Nothing
    End Sub

    Private Sub cmdBuscar_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscar.ServerClick


        Dim iIDTipoSol As Integer
        Dim iIDSolPadre As Integer
        Dim sUON1 As String
        Dim sUON2 As String
        Dim sUON3 As String


        ModuloIdioma = TiposDeDatos.ModulosIdiomas.BusquedaSolicitudes

        If FSNUser.PMRestRefAbiertasUO Then
            sUON1 = FSNUser.UON1
            sUON2 = FSNUser.UON2
            sUON3 = FSNUser.UON3
        Else
            Dim sUnidadOrg As Object = Split(Me.txtUnidadOrg.Valor, " - ")
            Dim iNivel As Integer = UBound(sUnidadOrg)

            Select Case iNivel
                Case 1
                    sUON1 = sUnidadOrg(0)
                Case 2
                    sUON1 = sUnidadOrg(0)
                    sUON2 = sUnidadOrg(1)
                Case 3
                    sUON1 = sUnidadOrg(0)
                    sUON2 = sUnidadOrg(1)
                    sUON3 = sUnidadOrg(2)
            End Select
        End If

        If txtIDSolicitud.Valor = "" Then
            iIDSolPadre = 0
        Else
            If IsNumeric(txtIDSolicitud.Valor) Then
                iIDSolPadre = CInt(txtIDSolicitud.Valor)
            Else
                iIDSolPadre = 0
            End If
        End If

        If Request("idRefSol") > 0 Then
            iIDTipoSol = Request("idRefSol")
        Else
            Dim ugtxtTipoSolicitudes_wdg As Infragistics.Web.UI.GridControls.WebDataGrid = CType(ugtxtTipoSolicitudes.Controls(0).Controls(0).FindControl("ugtxtTipoSolicitudes_wdg"), Infragistics.Web.UI.GridControls.WebDataGrid)
            If ugtxtTipoSolicitudes_wdg.Behaviors.Selection.SelectedRows.Count > 0 _
            AndAlso ugtxtTipoSolicitudes_wdg.Behaviors.Selection.SelectedRows(0).Items(0).Value IsNot System.DBNull.Value Then
                iIDTipoSol = ugtxtTipoSolicitudes_wdg.Behaviors.Selection.SelectedRows(0).Items(0).Value
            Else
                iIDTipoSol = Nothing
            End If
        End If
        Dim sMon As String
        If Request("Moneda") <> "" Then
            sMon = Request("Moneda")
        Else
            sMon = "EUR"
        End If

        Dim oInstancias As FSNServer.Instancias
        oInstancias = FSNServer.Get_Object(GetType(FSNServer.Instancias))
        oInstancias.DevolverSolicitudesPadre(Idioma, sMon, iIDSolPadre, iIDTipoSol, txtDescripcion.Valor, Me.txtPeticionario.Valor, sUON1, sUON2, sUON3)

        uwgSolicitudes.DataSource = oInstancias.Data
        uwgSolicitudes.DataBind()
        If oInstancias.Data.Tables.Count > 0 Then
            'Caption de la grid de instancias:
            uwgSolicitudes.Bands(0).AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
            uwgSolicitudes.Bands(0).Columns.FromKey("ID_INS").Header.Caption = Textos(2)  'Identificador
            uwgSolicitudes.Bands(0).Columns.FromKey("DESCR").Header.Caption = Textos(9)  'Titulo
            uwgSolicitudes.Bands(0).Columns.FromKey("FECHA").Header.Caption = Textos(10)   'Fecha
            uwgSolicitudes.Bands(0).Columns.FromKey("IMPORTE").Header.Caption = Textos(11) 'Importe
            uwgSolicitudes.Bands(0).Columns.FromKey("DISPONIBLE").Header.Caption = Textos(12) 'Disponible

            uwgSolicitudes.Bands(0).Columns.FromKey("ID_INS").Width = Unit.Percentage(15)
            uwgSolicitudes.Bands(0).Columns.FromKey("DESCR").Width = Unit.Percentage(52)
            uwgSolicitudes.Bands(0).Columns.FromKey("FECHA").Width = Unit.Percentage(11)
            uwgSolicitudes.Bands(0).Columns.FromKey("IMPORTE").Width = Unit.Percentage(11)
            uwgSolicitudes.Bands(0).Columns.FromKey("IMPORTE").AllowResize = Infragistics.WebUI.UltraWebGrid.AllowSizing.Free
            uwgSolicitudes.Bands(0).Columns.FromKey("DISPONIBLE").Width = Unit.Percentage(11)
            uwgSolicitudes.Bands(0).Columns.FromKey("DISPONIBLE").AllowResize = Infragistics.WebUI.UltraWebGrid.AllowSizing.Free
            uwgSolicitudes.Bands(0).Columns.FromKey("ACUMULADO").Hidden = True
            uwgSolicitudes.Bands(0).Columns.FromKey("MONEDA").Hidden = True
            uwgSolicitudes.Visible = True
            cmdAceptar.Visible = True
            lblSolicitudes.Visible = True

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(),"Mensaje1", "<script>var sMensajeImporte1 = '" & Textos(13) & "'</script>")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(),"Mensaje2", "<script>var sMensajeImporte2 = '" & Textos(14) & "'</script>")
        Else
            uwgSolicitudes.Visible = False
            cmdAceptar.Visible = False
            lblSolicitudes.Visible = False

        End If
    End Sub

    Private Sub uwgSolicitudes_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgSolicitudes.InitializeRow
        Dim oUser As FSNServer.User = Session("FSN_User")


        If Not e.Row.Cells.FromKey("IMPORTE").Value Is Nothing Then
            e.Row.Cells.FromKey("IMPORTE").Value = Replace(e.Row.Cells.FromKey("Importe").Value, ".", ",")
            e.Row.Cells.FromKey("IMPORTE").Value = FSNLibrary.FormatNumber(e.Row.Cells.FromKey("Importe").Value, oUser.NumberFormat)
        End If

        If Not e.Row.Cells.FromKey("DISPONIBLE").Value Is Nothing Then
            e.Row.Cells.FromKey("DISPONIBLE").Value = Replace(e.Row.Cells.FromKey("Disponible").Value, ".", ",")
            e.Row.Cells.FromKey("DISPONIBLE").Value = FSNLibrary.FormatNumber(e.Row.Cells.FromKey("Disponible").Value, oUser.NumberFormat)
        End If

    End Sub
End Class

