﻿Public Partial Class BuscadorActivoServer
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sCodActivo As String
        Dim sDen As String
        Dim sCod As String
        Dim sCentroCoste As String
        Dim fsEntryId As String

        sCodActivo = Request("codActivo")
        fsEntryId = Request("fsEntryId")
        sCentroCoste = Request("idCentroCoste")

        If Not String.IsNullOrEmpty(sCodActivo) Then
            Dim oSMServer As FSNServer.Root = TryCast(Session("FSN_Server"), FSNServer.Root)
            Dim oActivoServer As FSNServer.Activo
            oActivoServer = oSMServer.Get_Object(GetType(FSNServer.Activo))
            Dim oSMUser As FSNServer.User = TryCast(Session("FS_SM_User"), FSNServer.User)
            Dim result As DataSet = oActivoServer.BuscarActivo(Idioma, oSMUser.Cod, sCodActivo, sCentroCoste)
            If result.Tables(0).Rows.Count > 0 Then
                sCod = result.Tables(0).Rows(0).Item("COD")
                sDen = result.Tables(0).Rows(0).Item("DEN")
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>ponerDen_Correcto('" & fsEntryId & "','" & sCod & " - " & sDen & "','" & sCod & "')</script>")
            Else
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>borrarValorActivo('" & fsEntryId & "', true)</script>")
            End If
        Else
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>borrarValorActivo('" & fsEntryId & "', false)</script>")
        End If

    End Sub
End Class