
Public Class denominaciondest
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblLitCod As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitDen As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitDir As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitCP As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitPob As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitProv As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitPais As System.Web.UI.WebControls.Label
    Protected WithEvents lblCod As System.Web.UI.WebControls.Label
    Protected WithEvents lblDen As System.Web.UI.WebControls.Label
    Protected WithEvents lblDir As System.Web.UI.WebControls.Label
    Protected WithEvents lblCP As System.Web.UI.WebControls.Label
    Protected WithEvents lblPob As System.Web.UI.WebControls.Label
    Protected WithEvents lblProv As System.Web.UI.WebControls.Label
    Protected WithEvents lblPais As System.Web.UI.WebControls.Label
    Protected WithEvents cmdCerrar As System.Web.UI.HtmlControls.HtmlInputButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oDestino As FSNServer.Destino

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.DenominacionDest
        
        lblLitCod.Text = Textos(1)
        lblLitDen.Text = Textos(2)
        lblLitDir.Text = Textos(3)
        lblLitCP.Text = Textos(4)
        lblLitPob.Text = Textos(5)
        lblLitProv.Text = Textos(6)
        lblLitPais.Text = Textos(7)
        cmdCerrar.Value = Textos(8)

        'carga los datos del destino:
        oDestino = FSNServer.Get_Object(GetType(FSNServer.Destino))
        oDestino.Cod = Request("CodDest")
        oDestino.Load(Idioma)

        lblCod.Text = oDestino.Cod
        lblDen.Text = oDestino.Den
        lblDir.Text = oDestino.Dir
        lblCP.Text = oDestino.CP
        lblPob.Text = oDestino.Poblacion
        lblProv.Text = oDestino.Provincia
        lblPais.Text = oDestino.Pais
    End Sub
End Class

