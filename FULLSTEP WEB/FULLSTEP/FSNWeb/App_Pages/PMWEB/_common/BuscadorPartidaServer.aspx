﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuscadorPartidaServer.aspx.vb" Inherits="Fullstep.FSNWeb.BuscadorPartidaServer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
</head>
<script language="javascript">
    function ponerDen_Correcto(idFSEntry, sDen, sCod,idCentroCoste,sUon,sUonDen) {

        p = window.parent.window.document;

        idFSEntry = idFSEntry.replace("__t", "");
        e = p.getElementById(idFSEntry + "__tbl")
        if (e)
            oEntry = e.Object

        if (oEntry) {
            oEntry.setValue(sDen);
            oEntry.setDataValue(sCod);
            oEntry.Editor.elem.className = "TipoTextoMedioVerde"
        }
        //Para poner también el centro de coste
        idCentroCoste = idCentroCoste.replace("__t", "");
        e = p.getElementById(idCentroCoste + "__tbl")
        if (e)
            oEntryCC = e.Object
        if (oEntryCC) {
            oEntryCC.setValue(sUonDen);
            oEntryCC.setDataValue(sUon);
            oEntryCC.Editor.elem.className = "TipoTextoMedioVerde"
        }

        window.close();
    }

    function borrarValor(idFSEntry,bEsIncorrecto) {
        p = window.parent.window.document;

        idFSEntry = idFSEntry.replace("__t", "");
        e = p.getElementById(idFSEntry + "__tbl")
        if (e)
            oEntry = e.Object

        if (oEntry) {
            oEntry.setDataValue('');
            if (bEsIncorrecto)
                oEntry.Editor.elem.className = "TipoTextoMedioRojo"
            else
                oEntry.Editor.elem.className = "TipoTextoMedio"
        }

        window.close();
    }
</script>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
