Public Class tablaexternareg
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblReg As System.Web.UI.WebControls.Table
    Protected WithEvents cmdCerrar As System.Web.UI.HtmlControls.HtmlInputButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' <summary>
    ''' Cargar el detalle de una tabla externa
    ''' </summary>
    ''' <param name="sender">parametro de sistema</param>
    ''' <param name="e">parametro de sistema</param>   
    ''' <remarks>Llamada desde: jsAlta.js/show_externa_ReadOnly  jsAlta.js/AbrirTablaExterna; Tiempo m�ximo:0,1</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim iTabla As Integer = CInt(Request.QueryString("tabla"))
        Dim oTablaExterna As FSNServer.TablaExterna = FSWSServer.Get_Object(GetType(FSNServer.TablaExterna))
        oTablaExterna.LoadDefTabla(iTabla, False)

        Dim sTipoCampo As String
        For Each oRow As System.Data.DataRow In oTablaExterna.DefTabla.Tables(0).Rows
            If UCase(oRow.Item("CAMPO")) = UCase(oTablaExterna.PrimaryKey) Then
                sTipoCampo = oRow.Item("TIPOCAMPO")
                Exit For
            End If
        Next
        Dim oValor As Object
        Dim sValor As String = ""
        Select Case sTipoCampo
            Case "NUMERICO"
                oValor = Numero(Request.QueryString("valor"), ".")
            Case "FECHA"
                oValor = CType(Request.QueryString("valor"), Date)
            Case "TEXTO"
                oValor = CType(Request.QueryString("valor"), String)
        End Select
        If Not IsPostBack() Then
            Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & _
                "<script language=""{0}"">{1}</script>"
            Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.TablaExterna
            cmdCerrar.Value = Textos(2)
            Dim oRow As DataRow = oTablaExterna.BuscarRegistro(oValor.ToString(), Nothing, True)
            For Each oColumn As System.Data.DataColumn In oRow.Table.Columns
                If UCase(oColumn.ColumnName) <> UCase(oTablaExterna.ArtKey) Then
                    For Each oRow2 As System.Data.DataRow In oTablaExterna.DefTabla.Tables(0).Rows
                        If UCase(oRow2.Item("CAMPO")) = UCase(oColumn.ColumnName) Then
                            Dim oFila As New TableRow
                            Dim oCeldaCab As New TableCell
                            oCeldaCab.Text = oRow2.Item("NOMBRE")
                            oCeldaCab.CssClass = "ugfilatablaCabecera"
                            oFila.Cells.Add(oCeldaCab)
                            Dim oCeldaDat As New TableCell
                            Select Case oRow2.Item("TIPOCAMPO")
                                Case "FECHA"
                                    oCeldaDat.Text = CType(oRow.Item(oColumn.ColumnName), Date).ToString(FSNUser.DateFormat.ShortDatePattern)
                                Case "NUMERICO"
                                    If InStr(UCase(oColumn.DataType.Name), "INT") > 0 Then
                                        oCeldaDat.Text = VisualizacionNumero(oRow.Item(oColumn.ColumnName), FSNUser.NumberFormat.NumberDecimalSeparator, "", 0)
                                    Else
                                        oCeldaDat.Text = FSNLibrary.FormatNumber(Numero(oRow.Item(oColumn.ColumnName), "."), FSNUser.NumberFormat)
                                    End If
                                Case "NO_COMP"
                                    If UCase(oColumn.DataType.Name) = "BOOLEAN" Then
                                        Dim chkDat As New HtmlInputCheckBox
                                        chkDat.Checked = CType(oRow.Item(oColumn.ColumnName), Boolean)
                                        chkDat.Disabled = True
                                        oCeldaDat.Controls.Add(chkDat)
                                    Else
                                        oCeldaDat.Text = DBNullToStr(oRow.Item(oColumn.ColumnName))
                                    End If
                                Case Else
                                    oCeldaDat.Text = DBNullToStr(oRow.Item(oColumn.ColumnName))
                            End Select
                            oCeldaDat.CssClass = "ugfilatablaHist"
                            oFila.Cells.Add(oCeldaDat)
                            tblReg.Rows.Add(oFila)
                            Exit For
                        End If
                    Next
                End If
            Next

            Page.ClientScript.RegisterStartupScript(Me.GetType(),"redimwindow", String.Format(IncludeScriptKeyFormat, "javascript", "cambiartamanyo(" & tblReg.Rows.Count & ")"))
        End If
    End Sub

End Class

