﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="detallepartida.aspx.vb" Inherits="Fullstep.FSNWeb.detallepartida" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
</head>
<body onload="window.focus();">
    <form id="form1" runat="server">
    <table cellspacing="1" cellpadding="4" width="100%" border="0" height="100%">
	<TR><td width="5%"><img src="images/candado.jpg" /></td>
		<TD width="90%" align="center" nowrap><asp:label id="lblTitulo" runat="server" CssClass="titulo"></asp:label></TD>
		<TD width="5%"><img src="images/Bt_Cerrar.png" onclick="javascript:self.close();" /></TD>
		</TR>
	</table>
	<table cellspacing="1" cellpadding="4" width="100%" border="0" height="100%">
	<TR><TD nowrap><asp:Label id="lblCod" runat="server" CssClass="captionBlue"></asp:Label></TD>
		<TD><asp:Label id="lblCodBD" runat="server" CssClass="captionDarkBlue"></asp:Label></TD>
	</TR>
	<TR><TD nowrap><asp:Label id="lblNombre" runat="server" CssClass="captionBlue"></asp:Label></TD>
		<TD><asp:Label id="lblNombreBD" runat="server" CssClass="captionDarkBlue"></asp:Label></TD>
    </TR>
	<TR><TD nowrap><asp:Label id="lblCentro" runat="server" CssClass="captionBlue"></asp:Label></TD>
		<TD><asp:Label id="lblCentroBD" runat="server" CssClass="captionDarkBlue"></asp:Label></TD>
	</TR>
	<TR><TD nowrap><asp:Label id="lblFechaInicio" runat="server" CssClass="captionBlue"></asp:Label></TD>
		<TD><asp:Label id="lblFechaInicioBD" runat="server" CssClass="captionDarkBlue"></asp:Label></TD>
	</TR>
	<TR><TD nowrap><asp:Label id="lblFechaFin" runat="server" CssClass="captionBlue"></asp:Label></TD>
		<TD><asp:Label id="lblFechaFinBD" runat="server" CssClass="captionDarkBlue"></asp:Label></TD>
	</TR>
	</TABLE>
    </form>
</body>
</html>
