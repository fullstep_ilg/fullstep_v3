

Public Class comprobarEnProcesoServer
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private FSWSServer As FSNServer.Root

    ''' <summary>
    ''' Mostrar la pantalla. Usa JStext para evitar casques por caracateres problematicos javascript.
    ''' </summary>
    ''' <param name="sender">objeto lanza la carga</param>
    ''' <param name="e">evento de sistema</param>        
    ''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0 </remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FSWSServer = Session("FSN_Server")
            Dim lNoConformidad As Long = Request("NoConformidad")
            Dim lCertificado As Long = Request("Certificado")
            Dim Operacion As String = Request("Operacion") 'operaci�n a realizar dentro del detalle de noconformidad
            Dim sInstancia As String = Request("Instancia")
            Dim oInstancia As FSNServer.Instancia
            Dim sClave As String = Request("Clave")
            Dim sCodProv As String = Request("CodProv")
            Dim sCod As String = Request("Cod")
            Dim sScript As String
            Dim sDesdeCertificados As String = Request("DesdeCertificados")
            Dim sInstancias As String = Request("Instancias")
            Dim oCadenaInstancias As Object
            Dim n As String
            If sDesdeCertificados <> "" Then
                Dim sResultado As String
                Dim Certificados As String = Request("Certificados")
                If sInstancias <> "" Then
                    sInstancias = Left(sInstancias, Len(sInstancias) - 1)
                    oCadenaInstancias = Split(sInstancias, ";")
                    For Each n In oCadenaInstancias
                        oInstancia = FSWSServer.Get_Object(GetType(FSNServer.Instancia))
                        oInstancia.ID = CLng(n)
                        Dim iEnProceso As Integer = oInstancia.ComprobarEnProceso()
                        sResultado = sResultado & oInstancia.ID & "_" & iEnProceso & ";"
                    Next
                End If
                If Certificados <> "" Then
                    Certificados = Left(Certificados, Len(Certificados) - 1)
                End If
                Dim sAccion As String = Request("Accion")


                If sResultado <> "" Then
                    sResultado = Left(sResultado, Len(sResultado) - 1)
                End If




                sScript = "<script>window.parent.comprobadoEnProceso('" & sResultado & "','" & Certificados & "','" & sAccion & "');</script>"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(),"NWSeguimientoEnProceso", sScript)


            Else

                oInstancia = FSWSServer.Get_Object(GetType(FSNServer.Instancia))
                oInstancia.ID = CLng(sInstancia)
                Dim iEnProceso As Integer = oInstancia.ComprobarEnProceso()

                If lCertificado > 0 Or Operacion <> "" Then 'certifBusqueda, detalleCertificado, detalleNoConformidad
                    Dim FecAlta As String = Request("FecAlta")
                    Dim Proveedor As String = Request("Proveedor")
                    Dim Tipo As String = Request("Tipo")

                    sScript = "<script>window.parent.comprobadoEnProceso('" & sInstancia & "','" & sClave & "'," + iEnProceso.ToString() + ",'" + IIf(lCertificado > 0, lCertificado.ToString(), lNoConformidad.ToString()) + "','" + FecAlta + "','" + Proveedor + "','" + Tipo + "');</script>"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(),"NWSeguimientoEnProceso", sScript)
            ElseIf lNoConformidad > 0 Then

                Dim sRevisor As String = Request("Revisor")
                Dim sTitulo As String = Request("Titulo")
                Dim sTexto_cierre As String = Request("TextoCierre")
                Dim sTexto_apertura As String = Request("TextoApertura")
                Dim Estado As String = Request("estado")
                Dim sValorCombo As String = Request("ValorCombo")
                Dim FecAlta As String = Request("FecAlta")
                Dim Proveedor As String = Request("Proveedor")
                Dim Tipo As String = Request("TipoNoConf")
                Dim sCodPet As String = Request("CodPet")

                sScript = "<script>window.parent.mostrarVentana('" & sInstancia & "','" & lNoConformidad & "','" & sClave & "'," + iEnProceso.ToString() + ",'" & sRevisor & "','" + sCod + "','" + sCodProv + "','" & JSText(sTitulo) & "','" & JSText(sTexto_cierre) & "','" & JSText(sTexto_apertura) & "'," & Estado & ",'" & sValorCombo & "','" & (FecAlta) & "','" & Proveedor & "','" & JSText(Tipo) & "','" & sCodPet & "');</script>"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(),"NWSeguimientoEnProceso", sScript)
            Else

                Dim sCodPet As String = Request("CodPet")
                Dim sProcesos As String = Request("Procesos")
                Dim sPedidos As String = Request("Pedidos")
                Dim accionesAprobar As String = Request("accionesAprobar")

                Dim accionesRechazar As String = Request("accionesRechazar")
                If accionesAprobar <> "" Then
                    Dim arrAccionesAprobar As String() = accionesAprobar.Split(";")
                    Dim accionAprobar As String()
                    For i As Integer = 0 To UBound(arrAccionesAprobar)
                        accionAprobar = arrAccionesAprobar(i).Split(",")
                        Session("APROBAR_" & accionAprobar(0)) = CInt(accionAprobar(1))
                    Next
                End If
                If accionesRechazar <> "" Then
                    Dim arrAccionesRechazar = accionesRechazar.Split(";")
                    Dim accionRechazar As String()
                    For i As Integer = 0 To UBound(arrAccionesRechazar)
                        accionRechazar = arrAccionesRechazar(i).Split(",")
                        Session("RECHAZAR_" & accionRechazar(0)) = CInt(accionRechazar(1))
                    Next
                End If
                Dim sVerDetallePer As String = Request("VerDetallePer")
                If sVerDetallePer = "" Then
                    sVerDetallePer = "0"
                End If
                Dim Aprobar As String = Request("Aprobar")
                Dim Rechazar As String = Request("Rechazar")
                Dim Bloque As String = Request("Bloque")
                Dim Estado As String = Request("Estado")

                Dim sVerDetalleFlujo As String = Request("VerDetalleFlujo")
                If sVerDetalleFlujo = "" Then
                    sVerDetalleFlujo = "0"
                End If




                Dim sIdi As String = Session("FSN_User").Idioma.ToString()
                If sIdi = Nothing Then
                    sIdi = ConfigurationManager.AppSettings("idioma")
                End If


                sScript = "<script>window.parent.mostrarVentana('" & sInstancia & "','" & sClave & "'," + iEnProceso.ToString() + ",'" + sCod + "','" + sCodProv + "','" & sCodPet & "','" & sProcesos & "','" & sPedidos & "'," + sVerDetallePer
                If Aprobar <> "" Then
                    sScript = sScript & "," & Aprobar & "," & Rechazar & "," & Bloque & ",'" & Estado & "'"
                End If
                sScript = sScript + "," + sVerDetalleFlujo + ");</script>"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(),"NWSeguimientoEnProceso", sScript)
                End If
            End If
        End Sub

    End Class


