Public Class MailProveedores
    Inherits FSNPage

    Public Titulo As String
    Protected WithEvents cmdAdjuntar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents lblAsunto As System.Web.UI.WebControls.Label
    Protected WithEvents lblinfo As System.Web.UI.WebControls.Label
    Protected WithEvents cmdPara As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents ugtxtAsunto As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtRuta As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ugtxtRuta As System.Web.UI.WebControls.TextBox
    Protected WithEvents ParaExtendido As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents CCExtendido As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents AdjuntoExtendido As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents cmdCCO As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdCC As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents CCOExtendido As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents btnEnviar As System.Web.UI.WebControls.Button
    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
#Region " Web Form Designer Generated Code "
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    End Sub
    Protected WithEvents lbltitulo As System.Web.UI.WebControls.Label
    Protected WithEvents ugtxtTexto As System.Web.UI.WebControls.TextBox
    Private designerPlaceholderDeclaration As System.Object
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        InitializeComponent()
    End Sub
#End Region
    ''' <summary>
    ''' Cargar la pagina en envio de mail a proveedores.
    ''' Para="" implica error. Modo de actuar: si en Cco solo una direcci�n ent (Para=Cco)(limpia Cco) sino (Para=Email usuario conectado)
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>        
    ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.MailProveedores

        lbltitulo.Text = If(Request("NumeroFactura") <> "", Textos(16), Textos(0))
        Titulo = lbltitulo.Text
        btnEnviar.Text = Textos(1)
        cmdPara.Value = Textos(2)
        cmdCC.Value = Textos(3)
        cmdCCO.Value = Textos(11)
        lblAsunto.Text = Textos(4)
        cmdAdjuntar.Value = Textos(5)
        lblinfo.Text = Textos(12)

        btnEnviar.Attributes.Add("onclick", "return ComprobarDatosMail();")
        '
        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(Textos(6)) + " ';"   'Seleccione alguno
        sClientTexts += "arrTextosML[1] = '" + JSText(Textos(7)) + " ';"   'Problemas con el servidor
        sClientTexts += "arrTextosML[2] = '" + JSText(Textos(8)) + " ';"   'Problemas con los adjuntos
        sClientTexts += "arrTextosML[3] = '" + JSText(Textos(9)) + " ';"   'notificador. Consulte con FULLSTEP
        sClientTexts += "arrTextosML[4] = '" + JSText(Textos(10)) + " ';"   'Problemas desconocidos
        sClientTexts += "arrTextosML[5] = '" + JSText(Textos(13)) + " ';"   'El campo Para no puede estar vacio
        sClientTexts += "arrTextosML[6] = '" + JSText(Textos(14)) + " ';"   'La direcci�n ##### introducida no es correcta
        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

        If Not Page.IsPostBack Then
            If Request("NumeroFactura") <> "" Then ugtxtAsunto.Text = Replace(Textos(15), "#####", Request("NumeroFactura"))
            CCOExtendido.Value = Request("Proveedores")

            If ParaExtendido.Value = "" Then
                If InStr(CCOExtendido.Value, ";", CompareMethod.Text) > 0 Then
                    If Len(CCOExtendido.Value) = InStr(CCOExtendido.Value, ",", CompareMethod.Text) Then
                        ParaExtendido.Value = CCOExtendido.Value
                        CCOExtendido.Value = ""
                    Else
                        ParaExtendido.Value = Session("FSN_User").Email
                    End If
                Else
                    ParaExtendido.Value = CCOExtendido.Value
                    CCOExtendido.Value = ""
                End If
            End If

            AdjuntoExtendido.Value = ""

            txtRuta.Value = GenerateRandomPath()

            Dim sTemp As String = ConfigurationManager.AppSettings("temp") & "\" & txtRuta.Value
            If Not System.IO.Directory.Exists(sTemp) Then System.IO.Directory.CreateDirectory(sTemp)
        End If
    End Sub
    Private Function PreparaAdjuntos() As String
        Dim Adjuntos As String = Me.AdjuntoExtendido.Value
        Dim sTemp As String = ConfigurationManager.AppSettings("temp") & "\" & txtRuta.Value & "\"
        Dim sPath As String
        Dim sRes As String = ""

        While InStr(Adjuntos, ";", CompareMethod.Binary)
            sPath = Left(Adjuntos, InStr(Adjuntos, ";", CompareMethod.Binary) - 1)
            'No comprobamos si el fichero exista pq no funciona el los servidores el Exists por un 
            'problema de permisos.  Los ficheros se acaban de guardar as� que estar�n.(EPB 09/11/2006)
            'Fichero = New System.IO.FileInfo(sPath)
            sRes = sRes & sPath & ";"
            Adjuntos = Right(Adjuntos, Adjuntos.Length - InStr(Adjuntos, ";", CompareMethod.Binary))
        End While
        Return sRes
    End Function
    '''Revisado por: Jbg; Fecha: 24/10/2011
    ''' <summary>
    ''' Envia un correo
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>        
    ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
    Private Sub btnEnviar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnviar.Click
        Dim sClientTexts As String
        Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
        Dim oProves As FSNServer.Proveedores
        oProves = FSNServer.Get_Object(GetType(FSNServer.Proveedores))

        If AdjuntoExtendido.InnerText <> "" Then
            If Right(AdjuntoExtendido.InnerText, 1) = ";" Then
            Else
                AdjuntoExtendido.InnerText = AdjuntoExtendido.InnerText + ";"
            End If
        End If

        Dim lIdInstancia As Long = If(Request("Instancia") Is Nothing, 0, Request("Instancia"))
        Dim EntidadNotificacion As Integer = If(Request("Origen") Is Nothing, 0, Request("Origen"))

        Dim res As Integer = oProves.EnviarMailAProveedores(ParaExtendido.Value, CCExtendido.Value, CCOExtendido.Value, ugtxtAsunto.Text, ugtxtTexto.Text,
                                                            FSNUser.Email, AdjuntoExtendido.InnerText, txtRuta.Value, False, lIdInstancia, EntidadNotificacion, FSNUser.Idioma)
        Select Case res
            Case ErroresEMail.SinError
                sClientTexts = ""
                sClientTexts += "window.close()"
                sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextoOk", sClientTexts)
            Case ErroresEMail.Send
                sClientTexts = ""
                sClientTexts += "alert(arrTextosML[1])"
                sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextoNOk", sClientTexts)
            Case ErroresEMail.Adjuntos
                sClientTexts = ""
                sClientTexts += "alert(arrTextosML[2])"
                sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextoNOk", sClientTexts)
            Case ErroresEMail.CreacionObjeto
                sClientTexts = ""
                sClientTexts += "alert(arrTextosML[3])"
                sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextoNOk", sClientTexts)
            Case Else
                sClientTexts = ""
                sClientTexts += "alert(arrTextosML[4])"
                sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextoNOk", sClientTexts)
        End Select
    End Sub
End Class