﻿Public Partial Class AsignacionUsuarios
    Inherits FSNPage

    Private _oProve As FSNServer.Proveedor
    ''' <summary>
    ''' Obtenemos los datos de la solicitud
    ''' A continuacion lo metemos en la cache
    ''' </summary>
    ''' <remarks></remarks>
    Protected ReadOnly Property oProve() As FSNServer.Proveedor
        Get
            If _oProve Is Nothing Then
                If Me.IsPostBack And Request("__EVENTTARGET") <> "BuscadorProveedor" Then
                    _oProve = CType(Cache("oProve" & FSNUser.Cod), FSNServer.Proveedor)
                Else
                    _oProve = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                    Me.InsertarEnCache("oProve" & FSNUser.Cod, _oProve)
                End If
            End If
            Return _oProve
        End Get
    End Property

    Private _bSoloLectura As Boolean = False
    ''' <summary>
    ''' Propiedad que carga si la instancia esta en modo lectura
    ''' </summary>
    Protected Property SoloLectura() As Boolean
        Get
            _bSoloLectura = ViewState("bSoloLectura")
            Return _bSoloLectura
        End Get
        Set(ByVal value As Boolean)
            _bSoloLectura = value
            ViewState("bSoloLectura") = _bSoloLectura
        End Set
    End Property

    Private i As Integer = 0

    ''' <summary>
    '''Lleva a cabo la carga de la pagina.
    ''' Segun los parametros --> 
    '''      Inicializa la pagina
    '''      Carga los contactos del proveedor
    '''      Una vez que ha seleccionado un usuario carga su categoria y coste/hora
    '''      Una vez que ha seleccionado una categoria carga su coste/hora
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>        
    ''' <remarks>Tiempo máximo:1seg.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AsignacionUsuarios

        
        If Page.IsPostBack Then
            Dim dCosteHora As Double
            Dim iElementoSeleccionado As Integer = -1
            Dim idCategoria As Integer
            Dim iSeleccion As Integer = 0
            Dim lIdCategoriaUsuario As Long
            Dim iLineaEliminar As Integer = -1
            Select Case Request("__EVENTTARGET")
                Case "BuscadorProveedor"
                    If Request("__EVENTARGUMENT") <> "" Then
                        CargarContactosyCategoria(Request("__EVENTARGUMENT"))
                    End If
                Case "SeleccionUsuario"
                    Dim sResultado() As String
                    sResultado = Request("__EVENTARGUMENT").Split("_")
                    iElementoSeleccionado = sResultado(0)
                    iSeleccion = 1
                    lIdCategoriaUsuario = UsuarioSeleccionado(sResultado(1))

                Case "SeleccionCategoria"
                    Dim sResultado() As String
                    iSeleccion = 2
                    sResultado = Request("__EVENTARGUMENT").Split("_")
                    iElementoSeleccionado = sResultado(0)
                    dCosteHora = CategoriaSeleccionada(sResultado(1))
                Case "EliminarLinea"
                    iLineaEliminar = Request("__EVENTARGUMENT")
                    iLineaEliminar = iLineaEliminar - 1
            End Select
            If hdnRows.Value <> "" Then
                For i = 0 To hdnRows.Value
                    If iLineaEliminar <> i Then
                        AnyadirLinea(i)
                        If (i = iElementoSeleccionado - 1) Then
                            'Una vez de que se ha creado los dropdown-s en la pagina. Buscamos los elementos seleccionados
                            If iSeleccion = 1 Then
                                If lIdCategoriaUsuario > 0 Then
                                    idCategoria = ObtenerIndiceComboCategoria(iElementoSeleccionado, lIdCategoriaUsuario)
                                    If idCategoria > 0 Then
                                        dCosteHora = CategoriaSeleccionada(idCategoria)
                                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerCategoriayValorCoste", "<script>PonerCategoriayValorCoste('ddlCategoria_" & iElementoSeleccionado & "','" & idCategoria & "','" & "txtCosteHora_" & iElementoSeleccionado & "','" & FSNLibrary.FormatNumber(dCosteHora, FSNUser.NumberFormat) & "')</script>")
                                    End If
                                    Else
                                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerCategoriayValorCoste", "<script>PonerCategoriayValorCoste('ddlCategoria_" & iElementoSeleccionado & "','0','" & "txtCosteHora_" & iElementoSeleccionado & "','')</script>")
                                    End If
                                End If
                            If iSeleccion = 2 Then
                                Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerValorCoste", "<script>PonerValorCoste('" & "txtCosteHora_" & iElementoSeleccionado & "','" & FSNLibrary.FormatNumber(dCosteHora, FSNUser.NumberFormat) & "')</script>")
                            End If
                        End If
                    End If
                Next
                If iLineaEliminar > 0 Then
                    hdnRows.Value = hdnRows.Value - 1
                ElseIf iLineaEliminar = 0 Then
                    hdnRows.Value = ""
                End If
                updPnlUsuarios.Update()
            End If

        Else
            CargarIdiomas()
            Dim sSubTarea As String
            If Request("SubTarea") <> "" Then
                sSubTarea = Server.UrlDecode(Request("SubTarea"))
            End If

            lblCabecera1.Text = Replace(lblCabecera1.Text, "XXX", sSubTarea)


            SoloLectura = Request("SoloLectura")
            If SoloLectura Then
                'Modo solo lectura
                ModoLectura()
            Else
                'Modo modificacion
                ModoModificacion()

            End If
            cmdCancelar.Attributes.Add("onClick", "return CerrarVentana();")
            IDCONTROL.Value = CInt(Request("IdControl"))


            End If

    End Sub

    ''' <summary>
    ''' Carga los textos con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:page_load(); Tiempo máximo:0seg.</remarks>
    Private Sub CargarIdiomas()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AsignacionUsuarios

        lblCabecera1.Text = Textos(1) '"Usuarios asignados a XXX"
        lblRangoUsuarios.Text = Textos(2) '"Si desea asignar estos usuarios a otras sub-actividades, introdúzcalas separadas por compas. Por ejemplo:1,4,5-9"
        If Not SoloLectura Then
            lblSeleccioneProveedor.Text = Textos(3) & ":" '"Seleccione el proveedor" & ":"
        Else
            lblSeleccioneProveedor.Text = Textos(4) & ":" '"Proveedor" & ":"
        End If

        lblCab2Usuario.Text = Textos(5) '"Usuario"
        lblCab2Categoria.Text = Textos(6) '"Categoría"
        lblCab2CosteHora.Text = Textos(7) '"Coste/hora"

        cmdAceptar.Value = Textos(8) '"Aceptar"
        cmdCancelar.Value = Textos(9) '"Cancelar"

        'Textos Panel info-s
        FSNPanelDatosProveedor.Titulo = Textos(4) '"Proveedor"
        FSNPanelDatosProveedor.SubTitulo = Textos(10) '"Información detallada"

    End Sub

    ''' <summary>
    ''' Añade una fila en blanco en la tabla de usuarios
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>        
    ''' <remarks>Tiempo máximo:0,2seg.</remarks>
    Private Sub imgAnyadirLinea_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAnyadirLinea.Click
        If hdnRows.Value = "" Then
            hdnRows.Value = 0
        Else
            hdnRows.Value = hdnRows.Value + 1
        End If
        AnyadirLinea(hdnRows.Value)
        updPnlUsuarios.Update()
    End Sub


    ''' <summary>
    ''' Añade una fila en blanco en la tabla de usuarios
    ''' </summary>
    ''' <param name="count">nº linea</param>
    ''' <param name="iIndiceComboUsuario">ID Contacto</param>       
    ''' <param name="iIndiceComboCategoria">ID Categoria</param>
    ''' <param name="sCosteHoraConFormato">Coste hora</param>
    ''' <returns>Explicación retorno de la función</returns>
    ''' <remarks>Llamada desde:page_load // imgAnyadirLinea_Click(); Tiempo máximo</remarks>
    Protected Sub AnyadirLinea(ByVal count As Integer, Optional ByVal iIndiceComboUsuario As Integer = -1, Optional ByVal iIndiceComboCategoria As Integer = -1, Optional ByVal sCosteHoraConFormato As String = "")
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AsignacionUsuarios
        Dim tblRow As New HtmlTableRow
        Dim oCell As New HtmlTableCell
        Dim ddl As New DropDownList
        Dim txtCosteHora As New TextBox
        Dim _wneCosteHora As Infragistics.WebUI.WebDataInput.WebNumericEdit
        Dim lblMoneda As New Label
        Dim imgEliminar As New Image
        Dim rowCount As Integer

        If Me.hdnRows.Value <> "" Then
            rowCount = 1 + count
        End If

        'Usuario
        oCell = New HtmlTableCell
        ddl = New DropDownList
        ddl.ID = "ddlUsuario_" & rowCount.ToString
        ddl.DataSource = oProve.UsuariosyCategorias.Tables(1)
        ddl.DataTextField = "CONTACTO"
        ddl.DataValueField = "ID"
        ddl.DataBind()
        ddl.Items.Insert(0, "")
        ddl.Width = Unit.Pixel(225)
        ddl.Attributes.Add("onchange", "cambioUsuario(this);return false;")
        If iIndiceComboUsuario >= 0 Then
            ddl.SelectedValue = iIndiceComboUsuario
        End If
        oCell.Align = HorizontalAlign.Center
        oCell.Controls.Add(ddl)
        oCell.Width = 225
        oCell.Style.Add("border-bottom", "solid 1px #DDDDDD")
        oCell.Style.Add("padding-bottom", "3px")
        tblRow.Cells.Add(oCell)

        'Categoria
        oCell = New HtmlTableCell
        ddl = New DropDownList
        ddl.ID = "ddlCategoria_" & rowCount.ToString
        ddl.DataSource = oProve.UsuariosyCategorias.Tables(2)
        ddl.DataTextField = "CATEGORIA"
        ddl.DataValueField = "ID_CAT_LAB"
        ddl.DataBind()
        ddl.Items.Insert(0, "")
        ddl.Width = Unit.Pixel(210)
        ddl.Attributes.Add("onchange", "cambioCategoria(this);return false;")
        If iIndiceComboCategoria >= 0 Then
            ddl.SelectedValue = iIndiceComboCategoria
        End If
        oCell.Align = HorizontalAlign.Center
        oCell.Controls.Add(ddl)
        oCell.Width = 210
        oCell.Style.Add("border-bottom", "solid 1px #DDDDDD")
        oCell.Style.Add("padding-left", "5px")
        oCell.Style.Add("padding-bottom", "3px")
        tblRow.Cells.Add(oCell)


        'Coste/Hora y Moneda del proveedor
        oCell = New HtmlTableCell
        txtCosteHora.ID = "txtCosteHora_" & rowCount.ToString
        txtCosteHora.Width = 80
        If sCosteHoraConFormato <> "" Then
            txtCosteHora.Text = sCosteHoraConFormato
        End If
        oCell.Align = HorizontalAlign.Center

        txtCosteHora.Attributes.Add("onkeypress", "return limiteDecimalesTextBox(" & txtCosteHora.ClientID & _
                                                                            ",event" & _
                                                                            ",'" & FSNUser.DecimalFmt & _
                                                                            "','" & FSNUser.ThousanFmt & _
                                                                            "'," & FSNUser.PrecisionFmt & ", false);")


        txtCosteHora.Attributes.Add("onchange", "addThousandSeparator(" & txtCosteHora.ClientID & ",'" & _
                                                                FSNUser.DecimalFmt & "','" & _
                                                                FSNUser.ThousanFmt & "'," & _
                                                                FSNUser.PrecisionFmt & "," & _
                                                                "event); validarNumero(" & _
                                                                txtCosteHora.ClientID & ",'" & _
                                                                FSNUser.DecimalFmt & "','" & _
                                                                FSNUser.ThousanFmt & "'," & _
                                                                FSNUser.PrecisionFmt & ");")

        txtCosteHora.Attributes.Add("onpaste", "return validarpastenum(" & txtCosteHora.ClientID & _
                                                                    ",event" & _
                                                                    ",'" & FSNUser.DecimalFmt & _
                                                                    "','" & FSNUser.ThousanFmt & _
                                                                    "'," & FSNUser.PrecisionFmt & ");")



        oCell.Controls.Add(txtCosteHora)


        'Moneda del provedor
        lblMoneda.ID = "lblMoneda_" & rowCount.ToString
        lblMoneda.CssClass = "parrafo"
        lblMoneda.Text = oProve.MonCod
        oCell.Controls.Add(lblMoneda)

        oCell.Width = 145
        oCell.Style.Add("nowrap", "nowrap")
        oCell.Style.Add("border-bottom", "solid 1px #DDDDDD")
        oCell.Style.Add("padding-bottom", "3px")
        tblRow.Cells.Add(oCell)

        'Eliminar Fila
        oCell = New HtmlTableCell
        oCell.Width = 45
        imgEliminar.ID = "btnEliminar_" & rowCount.ToString
        imgEliminar.ImageUrl = "~/App_Themes/" & Me.Theme & "/images/Eliminar.gif"
        imgEliminar.Attributes.Add("onClick", "EliminarLinea(this,'" & Textos(11) & "');return false;")
        oCell.Align = "left"
        oCell.Controls.Add(imgEliminar)
        oCell.Style.Add("border-bottom", "solid 1px #DDDDDD")
        oCell.Style.Add("padding-bottom", "3px")
        tblRow.Cells.Add(oCell)



        tblUsuariosIN.Rows.Add(tblRow)
    End Sub

    ''' <summary>
    ''' Dibuja una tabla con los contactos, sus categorias y costes/hora
    ''' </summary>
    ''' <param name="sValor">Cadena con los valores
    '''      "codigoProveedor#DenProve#CodMoneda/NombreContacto1_idContacto_NombreCategoria1_idCategoria_Coste#.../NLineaARepetir"
    ''' </param>
    ''' <param name="sCodMoneda">Codigo moneda</param>        
    ''' <returns>Explicación retorno de la función</returns>
    ''' <remarks>Llamada desde:ModoLectura(); Tiempo máximo:0,2seg.</remarks>
    Protected Sub CargarLineasTablaUsuarios(ByVal sValor As String, ByVal sCodMoneda As String)

        Dim tblRow As New HtmlTableRow
        Dim oCell As New HtmlTableCell

        Dim lblUsuario As New Label
        Dim lblCategoria As New Label
        Dim lblCosteHora As New Label
        Dim lblMoneda As New Label
        Dim rowCount As Integer

        Dim sResultado() As String
        Dim sResultado2() As String

        sResultado = Split(sValor, ";;")
        If sValor <> "____" Then
            For i = 0 To UBound(sResultado)
                tblRow = New HtmlTableRow
                sResultado2 = Split(sResultado(i), "_")

                'Usuario
                oCell = New HtmlTableCell
                lblUsuario = New Label
                lblUsuario.ID = "lblUsuario_" & i.ToString
                lblUsuario.Text = sResultado2(0)
                lblUsuario.CssClass = "Normal"

                oCell.Align = HorizontalAlign.Left
                oCell.Controls.Add(lblUsuario)
                oCell.Width = 225
                oCell.Style.Add("border-bottom", "solid 1px #DDDDDD")
                oCell.Style.Add("padding-left", "10px")
                oCell.Style.Add("padding-bottom", "3px")
                tblRow.Cells.Add(oCell)

                'Categoria
                oCell = New HtmlTableCell
                lblCategoria = New Label
                lblCategoria.ID = "lblCategoria_" & i.ToString
                lblCategoria.CssClass = "Normal"
                lblCategoria.Text = sResultado2(2)

                oCell.Align = HorizontalAlign.Left
                oCell.Controls.Add(lblCategoria)
                oCell.Width = 225
                oCell.Style.Add("border-bottom", "solid 1px #DDDDDD")
                oCell.Style.Add("padding-left", "10px")
                oCell.Style.Add("padding-bottom", "3px")
                tblRow.Cells.Add(oCell)

                'Coste/Hora y Moneda del proveedor
                oCell = New HtmlTableCell
                oCell.Width = 145
                lblCosteHora = New Label
                lblCosteHora.ID = "lblCosteHora_" & i.ToString
                lblCosteHora.CssClass = "Normal"
                lblCosteHora.Text = FSNLibrary.FormatNumber(sResultado2(4), FSNUser.NumberFormat) & " " & sCodMoneda
                lblCosteHora.Width = 100

                oCell.Controls.Add(lblCosteHora)
                If SoloLectura Then
                    oCell.Align = "left"
                Else
                    oCell.Align = "right"
                End If
                oCell.Style.Add("border-bottom", "solid 1px #DDDDDD")
                oCell.Style.Add("padding-right", "15px")
                oCell.Style.Add("padding-bottom", "3px")
                tblRow.Cells.Add(oCell)
                tblUsuariosIN.Rows.Add(tblRow)
            Next
        End If
        

    End Sub


    ''' <summary>
    '''          Cargar Los contactos, y Categorias - Coste /hora
    '''          Y viene cargado el codigo de moneda del proveedor --> oProve.codMoneda
    ''' </summary>
    ''' <param name="sCodProve">Codigo proveedor</param>
    ''' <param name="ParametroEntrada2">Explicación parámetro 2</param>        
    ''' <remarks>Llamada desde:Page_load // ModoModificacion(); Tiempo máximo:0,35seg</remarks>
    Private Sub CargarContactosyCategoria(ByVal sCodProve As String)
        oProve.Cod = sCodProve
        oProve.Den = DenProve.Value

        oProve.CargarContactosyCategorias(FSNUser.Idioma)
        imgAnyadirLinea.Enabled = True
    End Sub

    ''' <summary>
    ''' Devuelve el ID del Contacto
    ''' </summary>
    ''' <param name="iIndiceCategoria">indice de la combo de Usuarios</param>   
    ''' <returns>ID de la Contacto</returns>
    ''' <remarks>Llamada desde:page_load(); Tiempo máximo:0,1seg.</remarks>
    Private Function UsuarioSeleccionado(ByVal iIndiceUsuario As Integer) As Long
        Dim lResultado As Long
        If Not oProve Is Nothing Then
            If iIndiceUsuario > 0 Then
                lResultado = DBNullToDbl(oProve.UsuariosyCategorias.Tables(1).Rows(iIndiceUsuario - 1).Item("ID_CAT_LAB"))
            End If
            UsuarioSeleccionado = lResultado
        End If
    End Function

    ''' <summary>
    ''' Devuelve el ID de la categoria
    ''' </summary>
    ''' <param name="iIndiceCategoria">indice de la combo de categoria</param>   
    ''' <returns>ID de la categoria</returns>
    ''' <remarks>Llamada desde:page_load(); Tiempo máximo:0,1seg.</remarks>
    Private Function CategoriaSeleccionada(ByVal iIndiceCategoria As Integer) As Double
        Dim lResultado As Long
        If Not oProve Is Nothing Then
            If iIndiceCategoria > 0 Then
                lResultado = DBNullToDbl(oProve.UsuariosyCategorias.Tables(2).Rows(iIndiceCategoria - 1).Item("COSTE_HORA"))
            End If
            CategoriaSeleccionada = lResultado
        End If

    End Function

    ''' <summary>
    ''' Obtiene el indice en la combo de Categoria de un usuario
    ''' </summary>
    ''' <param name="iIndiceComboSeleccionado">Indice de la fila en la tabla </param>
    ''' <param name="dIdCategoriaUsuario">ID de la categoria</param>        
    ''' <returns>Indice de la combo de categoria</returns>
    ''' <remarks>Llamada desde; Tiempo máximo</remarks>
    Private Function ObtenerIndiceComboCategoria(ByVal iIndiceComboSeleccionado As Integer, ByVal dIdCategoriaUsuario As Double) As Double
        Dim ddl As DropDownList
        Dim iResultado As Integer
        ddl = Me.FindControl("ddlCategoria_" & iIndiceComboSeleccionado)

        If Not ddl Is Nothing Then
            ddl.SelectedValue = dIdCategoriaUsuario
            iResultado = ddl.SelectedIndex
            ObtenerIndiceComboCategoria = iResultado
        End If

    End Function


    ''' <summary>
    ''' Dibuja una tabla con los contactos, sus categorias y costes/hora
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,25</remarks>
    Private Sub ModoLectura()
        tblCC.Visible = False
        txtProveedor.Visible = False
        imgProveedorLupa.Visible = False
        lblRangoUsuarios.Visible = False
        txtRangoUsuarios.Visible = False
        imgAnyadirLinea.Visible = False
        filaAceptarCancelar.Visible = False
        cmdCerrar.Attributes.Add("onClick", "return CerrarVentana();")

        lblProveedor.Visible = True
        imgInfProv.Visible = True
        imgInfProv.ImageUrl = "~/App_Themes/" & Me.Theme & "/images/info.gif"

        Dim sCodProve As String
        Dim sDenProve As String
        Dim sCodMoneda As String
        Dim sValor As String
        

        sCodProve = Server.UrlDecode(Request("CodProve"))
        sDenProve = Server.UrlDecode(Request("DenProve"))
        sCodMoneda = Server.UrlDecode(Request("CodMoneda"))
        sValor = Server.UrlDecode(Request("LineasUsuario"))

        If sValor <> "" Then
            CargarLineasTablaUsuarios(sValor, sCodMoneda)
        End If


        lblProveedor.Text = sCodProve & " - " & sDenProve

        'Panel infos
        imgInfProv.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosProveedor.AnimationClientID & "', event, '" & FSNPanelDatosProveedor.DynamicPopulateClientID & "', '" & sCodProve & "'); return false;")

    End Sub

    ''' <summary>
    ''' Dibuja una tabla con combos de Usuarios, categorias y Coste/horas del proveedor
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0,25seg.</remarks>
    Private Sub ModoModificacion()
        'Tratar las lineas si las tuviera

        Dim sCodProve As String
        Dim sDenProve As String
        Dim sCodMoneda As String
        Dim sValor As String
        Dim sLineasContactos As String


        sCodProve = Server.UrlDecode(Request("CodProve"))
        sDenProve = Server.UrlDecode(Request("DenProve"))
        sCodMoneda = Server.UrlDecode(Request("CodMoneda"))
        sValor = Server.UrlDecode(Request("LineasUsuario"))
        
        If sValor <> "" And sValor <> "____" Then
            Dim arrLineas() As String
            Dim arrDetalle() As String

            CargarContactosyCategoria(sCodProve)
            oProve.Den = sDenProve
            txtProveedor.Text = oProve.Cod & "-" & sDenProve

            'Tratar las lineas
            sLineasContactos = sValor
            arrLineas = Split(sLineasContactos, ";;")

            Dim CosteHora As String
            hdnRows.Value = "0"
            For i = 0 To UBound(arrLineas)
                arrDetalle = Split(arrLineas(i), "_")
                CosteHora = FSNLibrary.FormatNumber(arrDetalle(4), FSNUser.NumberFormat)
                AnyadirLinea(i, arrDetalle(1), arrDetalle(3), CosteHora)
                hdnRows.Value = i
            Next

        End If

        filaCerrar.Visible = False
        txtRangoUsuarios.Attributes.Add("onkeypress", "return ValidarRangoUsuarios(this, event);")
        txtRangoUsuarios.Attributes.Add("onpaste", "return ValidarRangoUsuariosPaste();")
        imgProveedorLupa.ImageUrl = "~/App_Themes/" & Me.Theme & "/images/lupa.gif"
        imgAnyadirLinea.ImageUrl = "~/App_Pages/PMWEB/alta/images/Anyadir.gif"
        imgProveedorLupa.OnClientClick = "var newWindow = window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorProveedores.aspx?PM=true', '_blank', 'width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes');newWindow.focus();return false;"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")

    End Sub


    ''' <summary>
    ''' Contruye una cadena con la informacion seleccionada en la pagina para pasarla al campo del desglose
    ''' </summary>
    ''' <param name="sender">El elemento</param>
    ''' <param name="e">evento</param>        
    ''' <remarks>Tiempo máximo:0,4seg.</remarks>
    Private Sub cmdAceptar_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAceptar.ServerClick
        Dim ddl As DropDownList
        Dim txtCosteHora As TextBox
        Dim i As Integer
        Dim sResultado As String
        Dim sResultadoIN As String

        If SoloLectura Then Exit Sub

        'sResultado = --> 
        '1 Proveedor
        '2 cadena de usuarios 
        '   NombreUsuario (a mostrar en la pagina de llamada)
        '   Id Contacto
        '   NombreCategoria
        '   idCategoria 
        '   coste(hora)
        '3 string con las lineas a copiar
        '
        If hdnRows.Value <> "" Then
            sResultado = oProve.Cod & "#" & oProve.Den & "#" & oProve.MonCod

            Dim sNombre As String
            Dim sNombreCategoria As String
            Dim idContacto As Long
            Dim idCategoria As Long
            Dim CosteHora As Double
            For i = 1 To hdnRows.Value + 1
                sNombre = ""
                sNombreCategoria = ""
                idContacto = 0
                idCategoria = 0
                CosteHora = 0
                ddl = Me.FindControl("ddlUsuario_" & i)
                If Not ddl Is Nothing Then
                    If ddl.SelectedIndex > 0 Then
                        sNombre = oProve.UsuariosyCategorias.Tables(1).Rows(ddl.SelectedIndex - 1).Item("NOMBRE")
                        idContacto = ddl.SelectedValue
                    End If
                End If

                ddl = Me.FindControl("ddlCategoria_" & i)
                If Not ddl Is Nothing Then
                    If ddl.SelectedIndex > 0 Then
                        sNombreCategoria = oProve.UsuariosyCategorias.Tables(2).Rows(ddl.SelectedIndex - 1).Item("CATEGORIA")
                        idCategoria = ddl.SelectedValue
                    End If
                End If

                txtCosteHora = Me.FindControl("txtCosteHora_" & i)
                If Not txtCosteHora Is Nothing Then
                    If txtCosteHora.Text <> "" Then
                        CosteHora = txtCosteHora.Text
                        CosteHora = Replace(CosteHora, FSNUser.DecimalFmt, "") 'Quitamos el signo decimal
                    End If
                End If

                If sResultadoIN <> "" Then sResultadoIN = sResultadoIN & "#"
                sResultadoIN = sResultadoIN & sNombre & "_" & idContacto & "_" & sNombreCategoria & "_" & idCategoria & "_" & CosteHora

            Next
            If sResultadoIN <> "" Then
                sResultado = sResultado & "/" & sResultadoIN
            End If
            sResultado = sResultado & "/" & txtRangoUsuarios.Text
        End If
        'Page.ClientScript.RegisterStartupScript(Me.GetType(), "DevolverSeleccionAsignacionUsuarios", "<script>DevolverUsuarios('" & sResultado & "');</script>")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "DevolverSeleccionAsignacionUsuarios", "<script>DevolverSeleccionAsignacionUsuarios('" & sResultado & "')</script>")

    End Sub
End Class