<%@ Page Language="vb" AutoEventWireup="false" Codebehind="materiales.aspx.vb" Inherits="Fullstep.FSNWeb.materiales"%>
<%@ Register TagPrefix="ignav" Namespace="Infragistics.WebUI.UltraWebNavigator" Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<script>
	    function getNivel(oNode){
	        if (oNode.getParent()) {
	            return getNivel(oNode.getParent()) + 1;
	        } else {
	            return 1;
	        }
	    }

		//Descripci�n:	A�ade los datos del item seleccionado	
		//Param. Salida: -
		//LLamada:	Desde el boton aceptar de esta pagina
		//Tiempo:	0
	    function seleccionarMaterial() {
		    var oTree = igtree_getTreeById("uwtMateriales");
		    var oNode = oTree.getSelectedNode();

		    if (oNode==null){
		        alert(arrTextosML[0]);
		        return false;
			};
			var sGMNDen = oNode.getText().split(" - ")[1];
			var sGMN = new Array();
			var sGMN1;
			var sGMN2;
			var sGMN3;
			var sGMN4;

			if (getNivel(oNode) < NivelSeleccion && NivelSeleccion != 0) {
			    if (!sMat) {
			        alert(sSeleccionarMatNivel.replace("###", NivelSeleccion));
			        return false;
			    } else {
			        alert(sSeleccionarMatNiveloSuperior.replace("###", NivelSeleccion));
			        return false;
			    }
			}
			var iNivel = 0;
		    while (oNode){
		        iNivel++;
		        sGMN[iNivel] = oNode.getDataKey();
		        oNode = oNode.getParent();	
			};
			j = 1;
		    for (i=iNivel;i>0;i--){
		        eval("sGMN" + i.toString() + "=sGMN[" + j.toString() + "]");
			    j++;
		    };


            if (window.document.Form1.desde.value == 'WebPart') {
                var sDen = '';
                materialDen = window.opener.document.getElementById(document.Form1.IDCONTROL.value);
                materialGS = window.opener.document.getElementById(document.Form1.IDControl_MaterialGS.value);
                if (materialDen) {
                    switch (iNivel) {
                        case 1:
                            sDen = sGMN1 + " - "
                            break
                        case 2:
                            sDen = sGMN2 + " - "
                            break
                        case 3:
                            sDen = sGMN3 + " - "
                            break
                        case 4:
                            sDen = sGMN4 + " - "
                            break
                    };
                    sDen = sDen + sGMNDen;
                    materialDen.value = sDen;
                };
                if (materialGS) {
                    switch (iNivel) {
                        case 1:
                            materialGS.value = sGMN1;
                            break
                        case 2:
                            materialGS.value = sGMN1 + "-" + sGMN2;
                            break
                        case 3:
                            materialGS.value = sGMN1 + "-" + sGMN2 + "-" + sGMN3;
                            break
                        case 4:
                            materialGS.value = sGMN1 + "-" + sGMN2 + "-" + sGMN3 + "-" + sGMN4;
                            break
                    };
                };


            } else {
			    if (window.document.Form1.desde.value == 'VisorSol') window.opener.mat_seleccionado(document.Form1.IDCONTROL.value, sGMN1, sGMN2, sGMN3, sGMN4, iNivel, sGMNDen, document.Form1.IDCONTROLVALUE.value,TodosNiveles);
                else window.opener.mat_seleccionado(document.Form1.IDCONTROL.value, sGMN1, sGMN2, sGMN3, sGMN4, iNivel, sGMNDen, 'Materiales', TodosNiveles);
            }
            window.close();	
		};
		function init() {
			//Con Internet Explorer 7, la ventana de materiales sal�a en un segundo plano cuando
			//se abr�a desde el buscador de art�culos, dandole el foco a la ventana se evita y sale
			//en un primer plano.
			window.focus();
		}
	</script>	
	<body onload="init()";>
		<form id="Form1" method="post" runat="server">
			<table Width="100%" style="height:80%"><tr>
				<td width="2%"><img src="images/filtro_materiales.gif" /></td>
				<td><asp:Label ID="lblTitulo" runat="server" Text="DSelecci�n de familia de material" CssClass="titulo"></asp:Label></td></tr>
			<tr><td colspan="2" valign="top">
			<ignav:ultrawebtree id="uwtMateriales" style="Z-INDEX: 101;"
				runat="server" Width="100%" CssClass="igTree" Height="400px">
				<SelectedNodeStyle ForeColor="White" BackColor="Navy"></SelectedNodeStyle>
				<Levels>
					<ignav:Level Index="0"></ignav:Level>
					<ignav:Level Index="1"></ignav:Level>
				</Levels>
			</ignav:ultrawebtree>
			</td></tr>
			</table>
			<TABLE id="Table1" style="Z-INDEX: 109;height:20%" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD align="right"><INPUT id="cmdAceptar" onclick="seleccionarMaterial()" type="button" value="Seleccionar"
							name="cmdAceptar" runat="server" class="botonPMWEB"></TD>
					<TD align="left"><INPUT id="cmdCancelar" onclick="window.close()" type="button" value="Cancelar" name="cmdCancelar"
							runat="server" class="botonPMWEB"></TD>
				</TR>
			</TABLE>
			<input runat="server" id="IDCAMPO" type="hidden" name="IDCAMPO" /> 
			<input runat="server" id="IDCONTROL" type="hidden" name="IDCAMPO" />
            <input runat="server" id="IDCONTROLVALUE" type="hidden" name="IDCONTROLVALUE" />
			<input runat="server" id="desde" type="hidden" name="desde" />
			<input type="hidden" id="IDControl_MaterialGS" name="IDControl_MaterialGS" runat="server" />
		</form>
	</body>
</html>
