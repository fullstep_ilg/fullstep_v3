﻿Public Partial Class BuscadorPartidaServer
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim oPartida As FSNServer.Partida
        Dim sDen As String
        Dim sCod As String
        Dim sPres5 As String
        Dim sCodPartida As String
        Dim fsEntry As String
        Dim sCentroCoste As String
        Dim idCentroCoste, sUon, sUonDen As String
        Dim separador As Integer

        sCodPartida = Request("CodPartida")
        sPres5 = Request("Pres5")
        fsEntry = Request("fsEntry")
        sCentroCoste = Request("CentroCoste")
        idCentroCoste = Request("IDCentroCoste")

        If sCodPartida <> Nothing Then
            If InStr(sCodPartida, " - ") > 0 Then sCodPartida = Left(sCodPartida, InStr(sCodPartida, " - "))

            oPartida = FSNServer.Get_Object(GetType(FSNServer.Partida))
            oPartida.BuscarPartida(Usuario.Cod, Idioma, sPres5, sCodPartida, sCentroCoste)

            If oPartida.Data.Tables(0).Rows.Count > 0 Then
                sCod = oPartida.Data.Tables(0).Rows(0).Item("UON1") & "#"
                If Not IsDBNull(oPartida.Data.Tables(0).Rows(0).Item("UON2")) Then
                    sCod = sCod & oPartida.Data.Tables(0).Rows(0).Item("UON2") & "#"
                End If
                If Not IsDBNull(oPartida.Data.Tables(0).Rows(0).Item("UON3")) Then
                    sCod = sCod & oPartida.Data.Tables(0).Rows(0).Item("UON3") & "#"
                End If
                If Not IsDBNull(oPartida.Data.Tables(0).Rows(0).Item("UON4")) Then
                    sCod = sCod & oPartida.Data.Tables(0).Rows(0).Item("UON4") & "#"
                End If
                sUon = sCod.Substring(0, sCod.Length - 1)
                separador = sUon.LastIndexOf("#")
                If separador > 0 Then
                    sUonDen = sUon.Substring(separador + 1, sUon.Length - separador - 1)
                Else
                    sUonDen = sUon
                End If
                sUonDen = sUonDen & " - " & oPartida.Data.Tables(0).Rows(0).Item("UONDEN")
                sCod = sCod & oPartida.Data.Tables(0).Rows(0).Item("COD")
                sDen = oPartida.Data.Tables(0).Rows(0).Item("DEN")
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>ponerDen_Correcto('" & fsEntry & "','" & Replace(sDen, "'", "\'") & "','" & DblQuote(sCod) & "','" & DblQuote(idCentroCoste) & "','" & DblQuote(sUon) & "','" & DblQuote(sUonDen) & "')</script>")
            Else
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>borrarValor('" & fsEntry & "', true)</script>")
            End If
        Else
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>borrarValor('" & fsEntry & "', false)</script>")
        End If
    End Sub

End Class