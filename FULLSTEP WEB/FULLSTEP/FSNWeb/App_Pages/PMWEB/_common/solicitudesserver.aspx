<%@ Register TagPrefix="fsde" Namespace="Fullstep.DataEntry" Assembly="DataEntry" %>
<%@ Register TagPrefix="igtbl" Namespace="Infragistics.WebUI.UltraWebGrid" Assembly="Infragistics.WebUI.UltraWebGrid.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="solicitudesserver.aspx.vb" Inherits="Fullstep.FSNWeb.solicitudesserver"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	
	<script>
		function aceptar(){		

		var oGrid = igtbl_getGridById("uwgSolicitudes");
		
		if (oGrid.getActiveRow()) {
			var cellId = oGrid.getActiveRow().Id;
			var sId = ""; var sDen = "";
			if (cellId) {
				var row = igtbl_getRowById(cellId); 
				if (row) {	
					var cell = row.getCell(0);  //Identificador
					var sId= cell.getValue();
					cell = row.getCell(1);   //T�tulo
					var sDen = cell.getValue();
					if (row.getCell(4).getValue() == 0) { //Disponible
						alert(sMensajeImporte1 + ' "' + sDen + '"' + '\n' + sMensajeImporte2);
					
					}
				}
			}
		}	
		window.opener.Solicitud_Padre_seleccionado(document.Form1.IDCONTROL.value, sId, sDen);	
		window.close()
	
		}		
	</script>	
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:ScriptManager ID="scrMng_ss" runat="server">
                <CompositeScript>
                    <Scripts>
                        <asp:ScriptReference Path="../alta/js/jsAlta.js" />
                    </Scripts>
                </CompositeScript>
            </asp:ScriptManager>
			<asp:label id="lbltitulo" style="Z-INDEX: 102; LEFT: 16px; POSITION: absolute; TOP: 16px" runat="server"
				CssClass="subtitulo">B�squeda de solicitudes</asp:label><asp:label id="lblTipoSolicitud" style="Z-INDEX: 110; LEFT: 24px; POSITION: absolute; TOP: 40px"
				runat="server" CssClass="CampoSoloLectura">DTipo de Solicitud</asp:label><INPUT class="botonPMWEB" id="cmdBuscar" style="Z-INDEX: 107; LEFT: 528px; POSITION: absolute; TOP: 184px"
				type="button" value="Buscar" name="cmdBuscar" runat="server">
			<asp:label id="lblSolicitudes" style="Z-INDEX: 105; LEFT: 8px; POSITION: absolute; TOP: 224px"
				runat="server" CssClass="CampoSoloLectura" Visible="False">DSolicitudes</asp:label><INPUT class="botonPMWEB" id="cmdAceptar" style="Z-INDEX: 104; LEFT: 288px; POSITION: absolute; TOP: 472px"
				onclick="aceptar()" type="button" value="Aceptar" name="cmdAceptar" runat="server">
			<igtbl:ultrawebgrid id="uwgSolicitudes" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 240px"
				runat="server" Visible="False" Width="650px" Height="224px">
				<DisplayLayout AllowSortingDefault="OnClient" RowHeightDefault="20px" Version="4.00" ScrollBarView="Vertical"
					HeaderClickActionDefault="SortSingle" BorderCollapseDefault="Separate" AllowColSizingDefault="Free"
					Name="uwgSolicitudes" TableLayout="Fixed" AllowUpdateDefault="Yes">
					<AddNewBox>
						<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
						</Style>
					</AddNewBox>
					<Pager>
						<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
						</Style>
					</Pager>
					<HeaderStyleDefault BorderStyle="Solid" BackColor="LightGray">
						<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
					</HeaderStyleDefault>
					<FrameStyle Width="650px" BorderWidth="1px" Font-Size="8pt" Font-Names="Verdana" BorderStyle="Solid"
						Height="224px"></FrameStyle>
					<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
						<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
					</FooterStyleDefault>
					<EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
					<RowStyleDefault Width="100px" BorderWidth="1px" BorderColor="Gray" BorderStyle="Solid">
						<Padding Left="3px"></Padding>
						<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
					</RowStyleDefault>
				</DisplayLayout>
				<Bands>
					<igtbl:UltraGridBand ColHeadersVisible="Yes" SelectTypeRow="Single" CellClickAction="RowSelect" AllowColSizing="Fixed"
						RowSelectors="No">
						<RowStyle Width="50px" Cursor="Hand" VerticalAlign="Middle"></RowStyle>
						<SelectedRowStyle ForeColor="White" BackColor="DarkBlue"></SelectedRowStyle>
					</igtbl:UltraGridBand>
				</Bands>
			</igtbl:ultrawebgrid>&nbsp; <INPUT id="IDCONTROL" style="Z-INDEX: 101; LEFT: 48px; POSITION: absolute; TOP: 16px" type="hidden"
				name="IDCONTROL" runat="server">
			<HR id="barra" style="Z-INDEX: 106; LEFT: 16px; WIDTH: 99%; POSITION: absolute; TOP: 216px"
				width="99%" color="gainsboro" noShade SIZE="2">
			<TABLE id="tblFiltros" style="Z-INDEX: 108; LEFT: 24px; WIDTH: 450px; POSITION: absolute; TOP: 88px; HEIGHT: 91px"
				cellSpacing="0" cellPadding="0" width="450" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 90px"><asp:label id="lblIdentificador" runat="server" CssClass="CampoSoloLectura">DIdentificador</asp:label></TD>
					<TD><asp:label id="lblDescripcion" runat="server" CssClass="CampoSoloLectura">DDescripci�n</asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 90px"><fsde:generalentry id="txtIDSolicitud" runat="server" Width="100%" MaxLength="100" Tag="ID" Tipo="TipoTextoCorto">
							<InputStyle CssClass="TipoTextoMedio"></InputStyle>
						</fsde:generalentry></TD>
					<TD><fsde:generalentry id="txtDescripcion" runat="server" Width="100%" Tag="DEN" Tipo="TipoString" maxlength="200">
							<InputStyle CssClass="TipoTextoMedio"></InputStyle>
						</fsde:generalentry></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 90px"><asp:label id="lblPeticionario" runat="server" CssClass="CampoSoloLectura">DPeticionario:</asp:label></TD>
					<TD><fsde:generalentry id="txtPeticionario" runat="server" Width="100%" Height="23" Tag="COD" Tipo="TipoString"
							DropDownGridID="txtPeticionario" tipoGS="Persona">
							<InputStyle CssClass="TipoTextoMedio"></InputStyle>
						</fsde:generalentry></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 90px"><asp:label id="lblUnidadOrg" runat="server" CssClass="CampoSoloLectura">DUnidad org:</asp:label></TD>
					<TD><fsde:generalentry id="txtUnidadOrg" runat="server" Width="100%" Tag="COD" Tipo="TipoString" tipoGS="UnidadOrganizativa">
							<INPUTSTYLE CssClass="TipoTextoMedio"></INPUTSTYLE>
						</fsde:generalentry></TD>
				</TR>
			</TABLE>
			<TABLE id="Table1" style="Z-INDEX: 109; LEFT: 24px; WIDTH: 528px; POSITION: absolute; TOP: 64px; HEIGHT: 16px"
				cellSpacing="0" cellPadding="0" width="300" border="0">
				<TR>
					<TD>
						<ig:WebDropDown ID="ugtxtTipoSolicitudes" runat="server" Width="450px"
						DropDownContainerHeight="250px" DropDownContainerWidth="" DropDownAnimationType="EaseOut" 
						EnablePaging="False" PageSize="12">
							<Items>
								<ig:DropDownItem>
								</ig:DropDownItem>
							</Items>
							<ItemTemplate>
								<ig:WebDataGrid ID="ugtxtTipoSolicitudes_wdg" runat="server"
									AutoGenerateColumns="false" Width="100%" ShowHeader="false">
									<Columns>
										<ig:BoundDataField DataFieldName="ID" Key="ID" Width="0%">
										</ig:BoundDataField>
										<ig:BoundDataField DataFieldName="COD" Key="COD" Width="40%">
										</ig:BoundDataField>
										<ig:BoundDataField DataFieldName="DEN" Key="DEN" Width="60%">
										</ig:BoundDataField>
									</Columns>
									<Behaviors>
										<ig:Selection RowSelectType="Single" Enabled="True" CellClickAction="Row">
											<SelectionClientEvents RowSelectionChanged="WebDataGrid_RowSelectionChanged" />
										</ig:Selection>
									</Behaviors>
								</ig:WebDataGrid>
							</ItemTemplate>
						</ig:WebDropDown>
					</TD>
					<TD><asp:label id="lblIdTipoSolicitud" runat="server" CssClass="parrafo" Visible="False">DDescripci�n</asp:label></TD>
				</TR>
			</TABLE>
			<script type="text/javascript">
				// The client event �RowSelectionChanged� takes two parameters sender and e
				// sender  is the object which is raising the event
				// e is the RowSelectionChangedEventArgs

				function WebDataGrid_RowSelectionChanged(sender, e) {

					//Gets the selected rows collection of the WebDataGrid
					var selectedRows = e.getSelectedRows();
					//Gets the row that is selected from the selected rows collection
					var row = selectedRows.getItem(0);
					//Gets the second cell object in the row
					//In this case it is ProductName cell 
					var cell = row.get_cell(2);
					//Gets the text in the cell
					var text = cell.get_text();

					//Gets reference to the WebDropDown
					var dropdown = null;
					dropdown = $find("<%= ugtxtTipoSolicitudes.clientID %>");
					if (dropdown != null) {
						//Sets the text of the value display to the product name of the selected row
						dropdown.set_currentValue(text, true);
						dropdown.closeDropDown()
					}
				}
			</script>		
		</form>
	</body>
</html>
