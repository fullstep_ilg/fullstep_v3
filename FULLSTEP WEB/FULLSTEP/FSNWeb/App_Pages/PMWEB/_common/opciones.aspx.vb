
Public Class opciones
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents cmdAceptar As System.Web.UI.WebControls.Button
    Protected WithEvents lblIdioma As System.Web.UI.WebControls.Label
    Protected WithEvents lblDestDefecto As System.Web.UI.WebControls.Label
    Protected WithEvents lblMostrarImgArticulos As System.Web.UI.WebControls.Label
    Protected WithEvents lblMostrarAtributos As System.Web.UI.WebControls.Label
    Protected WithEvents lblMostrarCantMin As System.Web.UI.WebControls.Label
    Protected WithEvents lblOcultarProve As System.Web.UI.WebControls.Label
    Protected WithEvents lblOcultarArt As System.Web.UI.WebControls.Label
    Protected WithEvents chkOcultarArt As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMostrarImgArticulos As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMostrarAtributos As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkMostrarCantMin As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkOcultarProve As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ugtxtIdioma As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents lblFormatoFecha As System.Web.UI.WebControls.Label
    Protected WithEvents ugtxtFormatoFecha As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents lblFormatoEmail As System.Web.UI.WebControls.Label
    Protected WithEvents ugtxtFormatoEmail As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents ugtxtSeparadorMiles As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents lblSeparadorMiles As System.Web.UI.WebControls.Label
    Protected WithEvents lblSeparadorDecimal As System.Web.UI.WebControls.Label
    Protected WithEvents ugtxtSeparadorDecimal As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents lblNumeroDecimales As System.Web.UI.WebControls.Label
    Protected WithEvents ugtxtNumeroDecimales As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents tblOpciones As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private FSWSServer As FSNServer.Root

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Page.IsPostBack Then
            Exit Sub
        End If

        Dim sIdi As String
        Dim oUser As FSNServer.User

        FSWSServer = Session("FSN_Server")
        oUser = Session("FSN_User")

        sIdi = oUser.Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Opciones

        Me.cmdAceptar.Text = Textos(0)
        Me.cmdAceptar.Attributes.Add("onclick", "return ComprobarSeparadores('" & Textos(27) & "');")
        Me.lblSeparadorDecimal.Text = Textos(10) & ":"
        Me.lblNumeroDecimales.Text = Textos(11) & ":"
        Me.lblSeparadorMiles.Text = Textos(12) & ":"
        Me.lblTitulo.Text = Textos(8)

        Me.lblFormatoFecha.Text = Textos(15) & ":"
        Me.lblFormatoEmail.Text = Textos(16) & ":"
        Me.ugtxtSeparadorMiles.Items(0).Text = Textos(13)
        Me.ugtxtSeparadorMiles.Items(3).Text = Textos(14)
        Me.lblIdioma.Text = Textos(20) & ":"

        Dim oIdiomas As FSNServer.Idiomas
        oIdiomas = FSWSServer.Get_Object(GetType(FSNServer.Idiomas))
        oIdiomas.Load()

        Me.ugtxtIdioma.TextField = "DEN"
        Me.ugtxtIdioma.ValueField = "COD"
        Me.ugtxtIdioma.DataSource = oIdiomas.data.Tables(0)
        Me.ugtxtIdioma.DataBind()

        For Each oItem As Infragistics.Web.UI.ListControls.DropDownItem In Me.ugtxtIdioma.Items
            If oItem.Value = oUser.Idioma.ToString() Then
                Me.ugtxtIdioma.SelectedItemIndex = oItem.Index
                Exit For
            End If
        Next

        Me.ugtxtFormatoEmail.Items(0).Text = Textos(17)
        Me.ugtxtFormatoEmail.Items(1).Text = Textos(18)
        Me.ugtxtSeparadorDecimal.TextField = "COD"
        Me.ugtxtSeparadorDecimal.ValueField = "COD"
        Me.ugtxtNumeroDecimales.TextField = "COD"
        Me.ugtxtNumeroDecimales.ValueField = "COD"
        Me.ugtxtSeparadorMiles.TextField = "COD"
        Me.ugtxtSeparadorMiles.ValueField = "COD"

        Me.ugtxtFormatoFecha.TextField = "COD"
        Me.ugtxtFormatoFecha.ValueField = "COD"
        Me.ugtxtFormatoEmail.TextField = "DEN"
        Me.ugtxtFormatoEmail.ValueField = "COD"

        Me.ugtxtSeparadorDecimal.CurrentValue = oUser.DecimalFmt
        Me.ugtxtSeparadorDecimal.SelectedValue = oUser.DecimalFmt
        Me.ugtxtNumeroDecimales.CurrentValue = oUser.PrecisionFmt
        Me.ugtxtNumeroDecimales.SelectedValue = oUser.PrecisionFmt

        Select Case oUser.ThousanFmt
            Case Nothing
                'SelectedValue -> Es el VALOR del elemento del combo
                Me.ugtxtSeparadorMiles.SelectedValue = oUser.ThousanFmt
                'currentValue -> Es el TEXTO del elemento del combo (que se muestra en el combo sin desplegar)
                Me.ugtxtSeparadorMiles.CurrentValue = Textos(13)
                'S�lo se puede asignar a ambas propiedades el mismo valor si text y value coinciden
                'Si no, hay que usar la propiedad SelectedItemIndex 
            Case ","
                Me.ugtxtSeparadorMiles.SelectedValue = oUser.ThousanFmt
                Me.ugtxtSeparadorMiles.CurrentValue = oUser.ThousanFmt
            Case "."
                Me.ugtxtSeparadorMiles.SelectedValue = oUser.ThousanFmt
                Me.ugtxtSeparadorMiles.CurrentValue = oUser.ThousanFmt
            Case " "
                Me.ugtxtSeparadorMiles.SelectedValue = oUser.ThousanFmt
                Me.ugtxtSeparadorMiles.CurrentValue = Textos(14)
        End Select
        Me.ugtxtFormatoFecha.SelectedValue = oUser.DateFmt
        Me.ugtxtFormatoFecha.CurrentValue = oUser.DateFmt
        Select Case oUser.TipoEmail
            Case 0
                'currentValue = Texto a mostrar
                Me.ugtxtFormatoEmail.CurrentValue = Textos(18)
                Me.ugtxtFormatoEmail.SelectedItemIndex = 1
            Case 1
                Me.ugtxtFormatoEmail.CurrentValue = Textos(17)
                Me.ugtxtFormatoEmail.SelectedItemIndex = 0
        End Select
    End Sub

    ''' <summary>
    ''' Guarda las opciones del usuario en Base de Datos
    ''' </summary>
    ''' <remarks>Llamada desde: Al pulsar el bot�n Aceptar. Tiempo m�ximo: 0 sg</remarks>
    Private Sub cmdAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        FSNUser.DecimalFmt = ugtxtSeparadorDecimal.SelectedItem.Value
        FSNUser.PrecisionFmt = ugtxtNumeroDecimales.SelectedItem.Value
        FSNUser.ThousanFmt = ugtxtSeparadorMiles.SelectedItem.Value

        If ugtxtFormatoFecha.SelectedItem IsNot Nothing Then
            FSNUser.DateFmt = ugtxtFormatoFecha.SelectedItem.Value
        Else
            FSNUser.DateFmt = String.Empty
        End If
        FSNUser.TipoEmail = ugtxtFormatoEmail.SelectedItem.Value
        FSNUser.Idioma = ugtxtIdioma.SelectedValue

        FSNUser.SaveUserData()

        Dim webService As New FSNSessionService.SessionService
        Session("CargarUserDataFSNWeb") = True

        ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "cerrar", "window.opener.location.href=window.opener.location.href; window.close();", True)
    End Sub
End Class

