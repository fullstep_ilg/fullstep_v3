﻿var ruta;

var CKEDITOR_BASEPATH = ruta + 'ckeditor/';
/*CONTROL DE ERRORES AJAX*/
$.ajaxSetup({
    error: function (error) {
        var errorGuardarError = "at Fullstep.FSNWeb.Error_Handler.Guardar_Error"
        var textoError = error.responseText
        if (textoError.indexOf(errorGuardarError) == -1) {
            $.when($.ajax({
                type: "POST",
                url: rutaFS + '_Common/App_Services/Error_Handler.asmx/Guardar_Error',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ App_Error: $.parseJSON(error.responseText) }),
                async: false
            })).done(function (msg) {
                var textos = msg.d;
                if ($('#popupErrorAlert').length == 0) {
                    $.when($.get(rutaFS + '_Common/Errores/html/errorAlert.htm', function (errorPopUp) {
                        errorPopUp = errorPopUp.replace(/src="/gi, 'src="' + ruta);
                        $('body').append(errorPopUp);
                    })).done(function () {
                        MostrarError(textos);
                    });
                } else {
                    MostrarError(textos);
                }
            });
        }
    }
});
function MostrarError(textos) {
    $('#popupFondo').css('height', $(document).height());
    $('#popupFondo').show();
    $.each(textos, function (key, value) {
        $('#' + key).html(value);
    });
    CentrarPopUp($('#popupErrorAlert'));
    $('#popupErrorAlert').show();
}
$('#btnAceptarErrorAlert').live('click', function () {
    $('#popupErrorAlert').hide();
    $('#popupFondo').hide();
    if (typeof (OcultarCargando) !== 'undefined' && $.isFunction(OcultarCargando)) OcultarCargando();
});
/*SOBREESCRIBIMOS LA FUNCION getScript para que no guarde/guarde en cache*/
(function ($) {
    $.getScript = function (url, callback, cache) {
        $.ajax({
            type: "GET",
            url: url,
            success: callback,
            dataType: "script",
            cache: cache || false
        });
    };
})(jQuery);

function trim(sValor) {
    var re = / /

    var i

    var sTmp = sValor

    while (sTmp.search(re) == 0)
        sTmp = sTmp.replace(re, "")

    var sRev = ""
    for (i = 0; i < sTmp.length; i++)
        sRev = sTmp.charAt(i) + sRev


    while (sRev.search(re) == 0)
        sRev = sRev.replace(re, "")

    sTmp = ""
    for (i = 0; i < sRev.length; i++)
        sTmp = sRev.charAt(i) + sTmp

    return (sTmp)

}

var XD = function () {
    var interval_id,
    last_hash,
    cache_bust = 1,
    attached_callback,
    window = this;

    return {
        postMessage: function (message, target_url, target) {            
            if (!target_url) return;            

            target = target || parent;  // default to parent

            if (window['postMessage']) {
                // the browser supports window.postMessage, so call it with a targetOrigin
                // set appropriately, based on the target_url parameter.
                target['postMessage'](message, target_url.replace(/([^:]+:\/\/[^\/]+).*/, '$1'));

            } else if (target_url) {
                // the browser does not support window.postMessage, so set the location
                // of the target to target_url#message. A bit ugly, but it works! A cache
                // bust parameter is added to ensure that repeat messages trigger the callback.
                target.location = target_url.replace(/#.*$/, '') + '#' + (+new Date) + (cache_bust++) + '&' + message;
            }
        }
    };
}();

var windowOpen = window.open;
//override de window open para que el popup se abra centrado
//y advierta si está activado el bloqueo de popup
window.open = function (url, name, features) {    
    if (typeof (autenticacionIntegradaFrame) != 'undefined' && autenticacionIntegradaFrame && name == '_top') {
        XD.postMessage(url, parent_url, parent.parent);
    }else{    
        var params = new Array();
        //extraer especificaciones
        if (features) {
            var specArr = features.split(",");
            for (i = 0; i < specArr.length; i++) {
                var spec = specArr[i];
                var key = trim(spec.split("=")[0]);
                var value = trim(spec.split("=")[1]);
                params[key] = value;
            }
            if ((params["width"] || params["height"])) {
                if (params["left"] == undefined) params["left"] = (screen.width - (params["width"] == undefined ? (screen.width / 2) : params["width"])) / 2;
                if (params["top"] == undefined) params["top"] = (screen.height - (params["height"] == undefined ? (screen.height / 2) : params["height"])) / 2;
            }
        }
        features = "";
        for (i in params) {
            features += i + "=" + params[i] + ",";
        }
        features = features.substring(0, features.length - 1);
        var popUp = windowOpen(url, name, features);
        //Tarea 3301 : Si el bloqueo de popups está activado mostramos una alerta ya que no se van a poder abrir
        if (popUp == null || typeof (popUp) == 'undefined') {
            if(navigator.userAgent.toLowerCase().indexOf('safari/') > -1)
                alert(sBloqueoPopupActivado);
            return false;
        } else {
            popUp.focus();
        }

        return popUp;
    }
};

$(document).ready(function () {
    $.getScript(ruta + 'js/jsUtilities.js', function () { }, true);
    $.getScript(ruta + 'js/jquery/jquery.json.min.js', function () { }, true);
    if (typeof (activadoCN) !== 'undefined' && activadoCN) {
        $.getScript(ruta + 'js/json2.js',function(){
            $.when($.ajax({
                type: "POST",
                url: rutaFS + '_Common/App_Services/User.asmx/Get_Logged_User',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false
            })).done(function (msg) {
                usuario = msg.d;                
                if (usuario != null && usuario.AccesoCN) Cargar_Opciones_CN();
            });
        },true);
    }
    $('#lnkNotificacionesCN').live('click', function () {
        window.location.href = rutaFS + 'CN/cn_MuroUsuario.aspx';
    });    
});

function Cargar_Opciones_CN() {
    $("#formFileupload").attr("action", rutaFS + 'CN/FileTransferHandler.ashx');    
	if (usuario.CNPermisoCrearMensajes) {
		$('#lnkNuevoMensajeMaster').show();
		$.getScript(rutaFS + 'CN/js/cn_ui_nuevosMensajes.js',function(){},true);		
		$.getScript(rutaFS + 'CN/js/cn_funciones.js', function () {
		    if ($('#divMuro').length == 0) {		            
		        $.getScript(ruta + 'ckeditor/ckeditor.js', function () { }, true);
		        $.getScript(ruta + 'js/jquery/plugins/autogrow.js', function () { }, true);
		        $.getScript(ruta + 'js/jquery/plugins/jquery.ui.datepicker.js', function () { }, true);
		        $.getScript(ruta + 'js/jquery/plugins/jquery.ui.maskedinput.js', function () { }, true);
		        $.getScript(ruta + 'js/jquery/plugins/jquery.ui.fsCombo.js', function () { }, true);
		        $.getScript(ruta + 'js/jquery/plugins/jquery.iframe-transport.js', function () { }, true);
		        $.getScript(ruta + 'js/jquery/plugins/video.js', function () { VideoJS.setupAllWhenReady(); }, true);		        
		        $.getScript(ruta + 'js/jquery/plugins/jquery.ui.treeview.js', function () { }, true);	                    
		        VerificarNotificacionesCN(false);
		        if (segundosNotificacionesCN !== 0) {
		            setInterval(function () { VerificarNotificacionesCN(true) }, segundosNotificacionesCN);
		        }		            
		    };
		    $.getScript(ruta + 'js/jquery/plugins/jquery.fileupload.js', function () {
		        $.getScript(ruta + 'js/jquery/plugins/jquery.fileupload-ui.js', function () {
		            $.when($.get(rutaFS + 'CN/html/_adjuntos.tmpl.htm', function (templates) {
		                $('body').append(templates);
		            })).done(function () {
		                $('#fileupload').fileupload();
		                $('#fileupload').show();
		                $('#fileupload .fileinput-button').css('right', '');
		                $('#fileupload .fileinput-button').css('left', -10000);
		                $('#fileupload .fileinput-button').css('top', -10000);
		                $('#fileupload .fileinput-button').live('mouseleave', function () {
		                    $('#fileupload .fileinput-button').css('left', -10000);
		                    $('#fileupload .fileinput-button').css('top', -10000);
		                    $('.Seleccionable').removeClass('Seleccionable');
		                });
		            });
		        }, true);
		    }, true);
		}, true);
	}

	$('#lnkNuevoMensajeMaster').live('click', function () {
		if ($('#divNuevosMensajesMaster').length == 0) {
			$.get(rutaFS + 'CN/html/_nuevo_mensaje.htm', function (menuNuevoMensaje) {
				$('body').prepend('<div id="popupNuevoMensaje" class="popupCN Texto12" style="display:none; position:absolute; z-index:1002; width:85%; max-height:80%; text-align:left; padding:5px; overflow:auto;"></div>');
				menuNuevoMensaje = menuNuevoMensaje.replace(/src="/gi, 'src="' + ruta);
				$('#popupNuevoMensaje').prepend(menuNuevoMensaje);
				$.each($('#popupNuevoMensaje [id]'), function () {
					$(this).attr('id', $(this).attr('id') + 'Master');
				});
				EstablecerTextosMenuMensajeNuevo();
				$('#txtNuevoMensajeTituloMaster').autoGrow();
				$('#txtEventoFechaMaster').datepicker({
					showOn: 'both',
					buttonImage: ruta + 'images/colorcalendar.png',
					buttonImageOnly: true,
					buttonText: '',
					dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
					showAnim: 'slideDown'
				}, $.datepicker.regional[usuario.Idioma_CodigoUniversal]);
				if (usuario.CNPermisoCrearMensajesUrgentes) $('#NuevoMensajeUrgenteMaster').show();
				MostrarFormularioNuevoMensaje(true, true, 'NuevoTemaMaster', 0, false);                            
				var heightContenido = $('#Contenido').height();
				CentrarPopUpAncho($('#popupNuevoMensaje'));
				$('#popupNuevoMensaje').css('top', 150);
				$('#popupFondo').css('height', $(document).height());
				$('#popupFondo').show();
				$('#Contenido').css('height', heightContenido);
				$('#popupNuevoMensaje').show();
			});
		} else {
			MostrarFormularioNuevoMensaje(true, true, 'NuevoTemaMaster', 0, false); ;
			var heightContenido = $('#Contenido').height();
			CentrarPopUpAncho($('#popupNuevoMensaje'));
			$('#popupNuevoMensaje').css('top', 150);
			$('#popupFondo').css('height', $(document).height());
			$('#popupFondo').show();
			$('#Contenido').css('height', heightContenido);
			$('#popupNuevoMensaje').show();
		}
	});
}

function VerificarNotificacionesCN(mostrarDivNotificaciones) {    
	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'CN/App_Services/CN.asmx/Comprobar_Notificaciones',
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: true
	})).done(function (msg) {
		var Notificaciones = 0;
		$.each(msg.d, function () {
			switch (this.value) {
				case "0":
					break;
				case "5":
					if (!$('#popupMensajeUrgente').is(':visible')) {
						var idMensaje = parseInt(this.text);
						$.get(rutaFS + 'CN/html/_mensajes.tmpl.htm', function (mensajes) {
							$('body').append(mensajes);
							$.getScript(rutaFS + 'CN/js/cn_mensajes_ui.js', function () {
								ObtenerMensajeUrgente(idMensaje);
							});
						});
					}
					break;
				default:
					Notificaciones += parseInt(this.text);
					break;
			}
		});
		if (Notificaciones > 0) {
			$('#NumeroAlertNotificacionCN').text(Notificaciones);
			$('#lnkNotificacionesCN').show();
			if ($('#divMuro').length > 0 && mostrarDivNotificaciones) {
				$('#divPanelNotificaciones span').text(Textos[82].replace('###', Notificaciones));
				$('#divNotificaciones').show();
			}
		} else {
			$('#lnkNotificacionesCN').hide();
		}
	});
}

function ObtenerMensajeUrgente(idMensaje) {
    var timeoffset = new Date().getTimezoneOffset();
	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'CN/App_Services/CN.asmx/Obtener_Mensaje',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify({ idMensaje: idMensaje, timezoneoffset: timeoffset }),
		dataType: "json",
		async: true
	})).done(function (msg) {
		if ($('#popupMensajeUrgente').length == 0) {
			$.get(rutaFS + 'CN/html/_PopUp_MensajeUrgente.htm', function (divmensaje) {
				$('body').append(divmensaje);
				CargarMensajeUrgente(msg);
				$('#btnAceptarPopUpMensajeUrgente').live('click', function () {
					$('#popupFondo').hide();
					$('#popupMensajeUrgente').hide();
					MensajeUrgenteLeido($(this).attr('IdMensaje'));
				});
			});
		} else {
			CargarMensajeUrgente(msg);
		}
	});
}

function CargarMensajeUrgente(msg) {
	var cn = { posts: [] };
	var mensaje = msg.d;

	$('#btnAceptarPopUpMensajeUrgente').attr('IdMensaje', mensaje.IdMensaje);

	if (mensaje.Tipo == 2) {
		mensaje.Fecha = eval("new " + mensaje.Fecha.slice(1, -1));
		mensaje.Cuando = mensaje.Fecha.getEventDate();
		mensaje.MesCorto = mensaje.Fecha.getEventMonthNameAbr();
		mensaje.Dia = mensaje.Fecha.getDate();
		mensaje.Hora = mensaje.Fecha.getHours() + ':' + mensaje.Fecha.getMinutes();
	}

	mensaje.FechaAlta = eval("new " + mensaje.FechaAlta.slice(1, -1));
	mensaje.FechaActualizacion = eval("new " + mensaje.FechaActualizacion.slice(1, -1));

	mensaje.FechaAltaRelativa = relativeTime(mensaje.FechaAlta);
	mensaje.FechaActualizacionRelativa = relativeTime(mensaje.FechaActualizacion);

	if (mensaje.Respuestas) {
		$.each(mensaje.Respuestas, function () {
			this.FechaAlta = eval("new " + this.FechaAlta.slice(1, -1));
			this.FechaActualizacion = eval("new " + this.FechaActualizacion.slice(1, -1));

			this.FechaAltaRelativa = relativeTime(this.FechaAlta);
			this.FechaActualizacionRelativa = relativeTime(this.FechaActualizacion);
		});
	}
	cn.posts.push(mensaje);

	$('#divMensajeUrgente').empty();
	$('#divMensajeUrgente').css('max-height', '');
	var ul = $('<ul>').appendTo($('#divMensajeUrgente'));
	$('#itemMensaje').tmpl(cn.posts).appendTo(ul);

	var heightContenido = $('#Contenido').height();
	CentrarPopUp($('#popupMensajeUrgente'));
	$('#popupFondo').css('height', $(document).height());
	$('#popupFondo').show();
	$('#Contenido').css('height', heightContenido);

	$('#imgMensajeUrgente').attr('src', ruta + 'images/ColaboracionHeader.png');
	$('#divMensajeUrgente [id^=divOpcionesMensaje_]').hide();
	$('#divNuevaRespuestaDefault_' + mensaje.IdMensaje).hide();

	$('#btnAceptarPopUpMensajeUrgente').text(Textos[20]);
	$('#divMensajeUrgente [id^=msgDescargar__]').text(Textos[79]);

	$('#popupMensajeUrgente').show();
	$('#divMensajeUrgente').css('max-height', $('#popupMensajeUrgente').height() - 85);
}

function MensajeUrgenteLeido(idMensaje) {
    var timeoffset = new Date().getTimezoneOffset();
	$.when($.ajax({
		type: "POST",
		url: rutaFS + 'CN/App_Services/CN.asmx/MensajeUrgente_Leido',
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify({ idMensaje: idMensaje, timezoneoffset: timeoffset }),
		dataType: "json",
		async: true
	})).done(function (msg) {
		$('#btnAceptarPopUpMensajeUrgente').removeAttr('IdMensaje');
		if (msg && msg.d) {
			CargarMensajeUrgente(msg);
		}
	});
}