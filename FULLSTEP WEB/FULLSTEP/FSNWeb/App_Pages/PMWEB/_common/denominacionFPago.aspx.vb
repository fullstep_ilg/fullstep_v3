
Public Class denominacionFPago
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblLitCod As System.Web.UI.WebControls.Label
    Protected WithEvents lblCod As System.Web.UI.WebControls.Label
    Protected WithEvents lblLitDen As System.Web.UI.WebControls.Label
    Protected WithEvents lblDen As System.Web.UI.WebControls.Label
    Protected WithEvents cmdCerrar As System.Web.UI.HtmlControls.HtmlInputButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oFPago As FSNServer.FormasPago

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.DenominacionDest

        lblLitCod.Text = Textos(1)
        lblLitDen.Text = Textos(2)
        cmdCerrar.Value = Textos(8)

        oFPago = FSNServer.Get_Object(GetType(FSNServer.FormasPago))
        oFPago.LoadData(Idioma, Request("CodFPago"))

        lblCod.Text = oFPago.Data.Tables(0).Rows(0).Item("COD")
        lblDen.Text = oFPago.Data.Tables(0).Rows(0).Item("DEN")

        oFPago = Nothing
    End Sub

End Class

