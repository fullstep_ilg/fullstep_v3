

    Public Class BuscadorArticulo
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
    ''' <summary>
    ''' Cargar la pagina. Obtiene los datos del art�culo a partir de los par�metros que se pasan a la p�gina.
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>        
    ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sArt As String
        Dim ArtfsEntry As String
        Dim DenfsEntry As String
        Dim MatfsEntry As String
        Dim CCfsEntry As String
        Dim oArts As FSNServer.Articulos
        Dim arrMat(4) As String
        Dim sDen As String = String.Empty
        Dim i As Byte
        Dim lLongCod As Long
        Dim sMat As String
        Dim desde As String = Nothing
        Dim bGenerico As Boolean
        Dim bExiste As Boolean
        Dim msIdi As String = Usuario.Idioma
        Dim sGMN1, sGMN2, sGMN3, sGMN4, sDenGMN, sUnidad, sDenUnidad, sNumeroDeDecimales, sDenOrgCompras, sDenCentro As String
        Dim sConcepto, sAlmacenamiento, sRecepcion, sTipoRecepcion As String
        Dim sConceptoTipoPedido, sAlmacenamientoTipoPedido, sRecepcionTipoPedido As String
        Dim sPREC_Ult_Adj, sCODPROV_Ult_Adj, sDENPROV_Ult_Adj As String
        Dim PRECfsEntry, PROVfsEntry, UNIfsEntry, OrgfsEntry, CentrofsEntry, CANTfsEntry As String
        Dim sInstanciaMoneda As String = ""
        Dim bUsar_OrgCompras As Boolean
        Dim sCodOrgCompras As String = Nothing
        Dim sCodCentro As String = Nothing
        Dim bCargarUltAdj As Boolean
        Dim bCargarCC As Boolean
        Dim iTipoSolicitud As TiposDeDatos.TipoDeSolicitud
        Dim sCodProve As String = String.Empty
        Dim sIDFSEntryProve As String = String.Empty
        Dim sIDFSEntryUniPedido As String = String.Empty
        Dim sCodMon_Ult_Adj As String = String.Empty
        Dim sUnidadPedido As String = String.Empty
        Dim sDenUnidadPedido As String = String.Empty
        Dim sUltAdjAnyo As String = String.Empty
        Dim sUltAdjGmn1 As String = String.Empty
        Dim sUltAdjProceCod As String = String.Empty
        Dim sUltAdjProceDen As String = String.Empty
        Dim sUltAdjProveCod As String = String.Empty
        Dim sUltAdjProveDen As String = String.Empty
        Dim sUon1 As String = String.Empty
        Dim sUon2 As String = String.Empty
        Dim sUon3 As String = String.Empty
        Dim sUon4 As String = String.Empty
        Dim bCargaFpago As Boolean = False
        Dim sIDFSEntryFPago As String = String.Empty
        Dim sIDFSEntryFPagoHidden As String = String.Empty
        Dim sFPagoPROVUltADJ As String = String.Empty
        Dim sDenFPagoPROVUltADJ As String = String.Empty
        Dim codProve As String
        Dim miUON As String
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.BusquedaNuevoCodArticulos

        codProve = Request("codProve")
        sMat = Request("idMaterial")
        sArt = Request("idArt")
        ArtfsEntry = Request("fsEntryArt")
        DenfsEntry = Request("fsEntryDen")
        CCfsEntry = Request("fsEntryCC")

        If Not Request("fsEntryMat") Is Nothing Then
            MatfsEntry = Request("fsEntryMat")
        End If
        If Not Request("desde") Is Nothing Then
            desde = Request("desde")
        End If

        If Not Request("fsEntryPrec") Is Nothing And Request("fsEntryPrec") <> "" Then
            PRECfsEntry = Request("fsEntryPrec")
        End If
        If Not Request("fsEntryProv") Is Nothing And Request("fsEntryProv") <> "" Then
            PROVfsEntry = Request("fsEntryProv")
        End If
        If Not Request("fsEntryUni") Is Nothing And Request("fsEntryUni") <> "" Then
            UNIfsEntry = Request("fsEntryUni")
        End If
        If Not Request("fsEntryCant") Is Nothing And Request("fsEntryCant") <> "" Then
            CANTfsEntry = Request("fsEntryCant")
        End If
        If Not Request("fsEntryOrg") Is Nothing And Request("fsEntryOrg") <> "" Then
            OrgfsEntry = Request("fsEntryOrg")
            bUsar_OrgCompras = True
        Else
            bUsar_OrgCompras = False
        End If
        If Not Request("fsEntryCentro") Is Nothing And Request("fsEntryCentro") <> "" Then
            CentrofsEntry = Request("fsEntryCentro")
        End If

        If bUsar_OrgCompras Then
            If Request("CodOrgCompras") <> "" Then  'Valor Inicial
                sCodOrgCompras = IIf(Request("CodOrgCompras") = "-1", "", Request("CodOrgCompras"))
            End If

            If Request("CodCentro") <> "" Then
                sCodCentro = IIf(Request("CodCentro") = "-1", "", Request("CodCentro"))
            End If
        End If

        If Request("InstanciaMoneda") <> "" Then sInstanciaMoneda = Request("InstanciaMoneda")
        If PRECfsEntry <> "" Or PROVfsEntry <> "" Then
            bCargarUltAdj = True
        End If

        If Not Request("iTipoSolicit") Is Nothing And Request("iTipoSolicit") <> "" Then
            iTipoSolicitud = strToInt(Request("iTipoSolicit"))
            If Not Request("sCodProve") Is Nothing And Request("sCodProve") <> "" Then
                sCodProve = Request("sCodProve")
            End If
            If Not Request("sIDCodProve") Is Nothing And Request("sIDCodProve") <> "" Then
                sIDFSEntryProve = Request("sIDCodProve")
            End If
            If Not Request("sIDCodUniPedido") Is Nothing And Request("sIDCodUniPedido") <> "" Then
                sIDFSEntryUniPedido = Request("sIDCodUniPedido")
            End If

            sIDFSEntryFPago = IIf(IsNothing(Request("IDFPago")), "", Request("IDFPago"))
            sIDFSEntryFPagoHidden = IIf(IsNothing(Request("IDFPagoHidden")), "", Request("IDFPagoHidden"))

            If Me.Acceso.gbAccesoFSSM Then
                sUon1 = Request("Uon1")
                sUon2 = Request("Uon2")
                sUon3 = Request("Uon3")
                sUon4 = Request("Uon4")
            End If

            bCargaFpago = (sIDFSEntryFPago <> String.Empty OrElse sIDFSEntryFPagoHidden <> String.Empty)
        End If

        Dim bArticulosNegociadosExpress As Boolean = Me.FSNServer.TipoAcceso.gbArticulosNegociadosPedExpress
        If bArticulosNegociadosExpress AndAlso iTipoSolicitud <> TiposDeDatos.TipoDeSolicitud.PedidoExpress Then
            'Articulos negociados
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PedidoExpressNegociable", "<script>isPedidoExpressNegociable = true;</script>")
        Else
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PedidoExpressNegociable", "<script>isPedidoExpressNegociable = false;</script>")
        End If
        sConceptoTipoPedido = Request("concepto")
        sAlmacenamientoTipoPedido = Request("almacenamiento")
        sRecepcionTipoPedido = Request("Recepcion")

        If CCfsEntry <> "" Then bCargarCC = True

        Dim sMat2, sMat3 As String
        oArts = FSNServer.Get_Object(GetType(FSNServer.Articulos))
        sMat3 = Replace(sMat, " ", "")
        If desde = "MATERIAL" Or sMat <> "" Then
            For i = 1 To 4
                arrMat(i) = ""
            Next i
            i = 1
            While Trim(sMat) <> ""
                Select Case i
                    Case 1
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                    Case 2
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                    Case 3
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                    Case 4
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
                End Select
                arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
                sMat = Mid(sMat, lLongCod + 1)
                i = i + 1
            End While
            oArts.GMN1 = arrMat(1)
            oArts.GMN2 = arrMat(2)
            oArts.GMN3 = arrMat(3)
            oArts.GMN4 = arrMat(4)
        End If
        miUON = IIf(IsNothing(Request("miUON")), "", Request("miUON"))
        Dim sUon = New String() {"", "", ""}
        Dim j As Integer
        If miUON <> "" Then
            Dim temp() As String = Split(miUON, "-")
            For j = 0 To temp.Length - 1
                sUon(j) = Trim(temp(j))
            Next
        End If

        Dim sProveSumiArt As String = Request("ProveSumiArt")

        If iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado OrElse iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad Then
            oArts.LoadData(FSNUser.Cod, False, False, sArt, , True, , , , msIdi, , sInstanciaMoneda, bCargarUltAdj, sCodOrgCompras, sCodCentro, bCargarCC, bUsar_OrgCompras, AccesoFSEP:=FSNServer.TipoAcceso.gbAccesoFSEP)
        ElseIf iTipoSolicitud <> TiposDeDatos.TipoDeSolicitud.PedidoExpress AndAlso iTipoSolicitud <> TiposDeDatos.TipoDeSolicitud.PedidoNegociado Then
            oArts.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil,
                           sArt, , True, , , , msIdi, , sInstanciaMoneda, bCargarUltAdj, sCodOrgCompras, sCodCentro, bCargarCC, bUsar_OrgCompras _
                              , , , , , , , , , , RestringirMaterialUsuario:=AccionesDeSeguridad.PMRestringirMaterialAsignadoUsuario, CodProveedor:=codProve _
                              , UON1:=IIf(sUon(0) Is String.Empty, Nothing, sUon(0)), UON2:=IIf(sUon(1) Is String.Empty, Nothing, sUon(1)), UON3:=IIf(sUon(2) Is String.Empty, Nothing, sUon(2)),
                           CodProveedorSumiArt:=sProveSumiArt, AccesoFSEP:=FSNServer.TipoAcceso.gbAccesoFSEP)
        Else
            Dim iFiltrarNegociados As Integer
            If iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoNegociado Then
                bCargarUltAdj = True
                iFiltrarNegociados = 1 'FILTRA POR ART�CULOS NEGOCIADOS
                oArts.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil,
                               sArt, , True, , , , msIdi, , sInstanciaMoneda, bCargarUltAdj, sCodOrgCompras, sCodCentro, bCargarCC, bUsar_OrgCompras _
                               , , , , , , , , , , , iFiltrarNegociados, sCodProve, bCargaFpago, Me.Acceso.gbUsar_OrgCompras, Me.Acceso.gbAccesoFSSM _
                               , sUon1, sUon2, sUon3, sUon4, RestringirMaterialUsuario:=AccionesDeSeguridad.PMRestringirMaterialAsignadoUsuario, CodProveedorSumiArt:=sProveSumiArt, AccesoFSEP:=FSNServer.TipoAcceso.gbAccesoFSEP)
            ElseIf iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoExpress Then
                bCargarUltAdj = False
                If Me.FSNServer.TipoAcceso.gbArticulosNegociadosPedExpress Then
                    bCargarUltAdj = True
                    iFiltrarNegociados = 3 'FILTRA POR ART�CULOS NEGOCIADOS O NO
                Else
                    iFiltrarNegociados = 2 'FILTRA ARTICULOS NO NEGOCIADOS
                End If
                oArts.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil,
                               sArt, , True, , , , msIdi, , sInstanciaMoneda, bCargarUltAdj, sCodOrgCompras, sCodCentro, bCargarCC, bUsar_OrgCompras _
                               , , , , , , , , , , , iFiltrarNegociados, sCodProve, RestringirMaterialUsuario:=AccionesDeSeguridad.PMRestringirMaterialAsignadoUsuario,
                               CodProveedorSumiArt:=sProveSumiArt, AccesoFSEP:=FSNServer.TipoAcceso.gbAccesoFSEP)
                'Si no hay datos, significa que no hay datos para ese art�culo o bien que hay datos pero que son de adjudicaciones vigentes.
                'Comprobamos si hay adjudicaciones vigentes, para poder informar posteriormente al usuario.
                If oArts.Data.Tables(0).Rows.Count = 0 Then
                    oArts.CargarProveProveUltAdj(sArt, sCodOrgCompras, sCodCentro)
                    If oArts.DataUltAdjVig IsNot Nothing AndAlso oArts.DataUltAdjVig.Tables.Count > 0 AndAlso oArts.DataUltAdjVig.Tables(0).Rows.Count > 0 Then
                        sUltAdjAnyo = oArts.DataUltAdjVig.Tables(0).Rows(0).Item("ANYO")
                        sUltAdjGmn1 = oArts.DataUltAdjVig.Tables(0).Rows(0).Item("GMN1")
                        sUltAdjProceCod = oArts.DataUltAdjVig.Tables(0).Rows(0).Item("COD")
                        sUltAdjProceDen = oArts.DataUltAdjVig.Tables(0).Rows(0).Item("DEN")
                        sUltAdjProveCod = oArts.DataUltAdjVig.Tables(0).Rows(0).Item("CODPROVE")
                        sUltAdjProveDen = oArts.DataUltAdjVig.Tables(0).Rows(0).Item("DENPROVE")
                    End If
                End If
            End If
        End If

        If oArts.Data.Tables(0).Rows.Count > 0 Then
            sDen = oArts.Data.Tables(0).Rows(0).Item("DEN")
            bGenerico = CBool(oArts.Data.Tables(0).Rows(0).Item("GENERICO"))
            sGMN1 = oArts.Data.Tables(0).Rows(0).Item("GMN1")
            sGMN2 = oArts.Data.Tables(0).Rows(0).Item("GMN2")
            sGMN3 = oArts.Data.Tables(0).Rows(0).Item("GMN3")
            sGMN4 = oArts.Data.Tables(0).Rows(0).Item("GMN4")
            sDenGMN = oArts.Data.Tables(0).Rows(0).Item("DENGMN")
            sUnidad = oArts.Data.Tables(0).Rows(0).Item("UNI")
            sDenUnidad = oArts.Data.Tables(0).Rows(0).Item("DENUNI")
            sUnidadPedido = DBNullToStr(oArts.Data.Tables(0).Rows(0).Item("UNI"))
            sDenUnidadPedido = DBNullToStr(oArts.Data.Tables(0).Rows(0).Item("DENUNI"))
            If (iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoNegociado) Then
                sUnidad = oArts.Data.Tables(0).Rows(0).Item("UNI_ULT_ADJ")
                sDenUnidad = oArts.Data.Tables(0).Rows(0).Item("DENUNI_ULT_ADJ")
            End If
            If IsDBNull(oArts.Data.Tables(0).Rows(0).Item("NUMDEC")) Then
                sNumeroDeDecimales = "null"
            Else
                sNumeroDeDecimales = oArts.Data.Tables(0).Rows(0).Item("NUMDEC")
            End If
            sConcepto = oArts.Data.Tables(0).Rows(0).Item("CONCEPTO")
            sAlmacenamiento = oArts.Data.Tables(0).Rows(0).Item("ALMACENAR")
            sRecepcion = oArts.Data.Tables(0).Rows(0).Item("RECEPCIONAR")
            sTipoRecepcion = oArts.Data.Tables(0).Rows(0).Item("TIPORECEPCION")

            If bUsar_OrgCompras Then
                sCodOrgCompras = oArts.Data.Tables(0).Rows(0).Item("CODORGCOMPRAS")
                sDenOrgCompras = oArts.Data.Tables(0).Rows(0).Item("DENORGCOMPRAS")
                If oArts.Data.Tables(0).Rows.Count > 1 Then
                    '1 art 1 org n centros
                    sCodCentro = ""
                    sDenCentro = ""
                Else
                    sCodCentro = oArts.Data.Tables(0).Rows(0).Item("CODCENTRO")
                    sDenCentro = oArts.Data.Tables(0).Rows(0).Item("DENCENTRO")
                End If
            End If

            If bCargarUltAdj AndAlso Not bGenerico Then
                If Not IsDBNull(oArts.Data.Tables(0).Rows(0).Item("PREC_ULT_ADJ")) Then
                    sPREC_Ult_Adj = FSNLibrary.FormatNumber(oArts.Data.Tables(0).Rows(0).Item("PREC_ULT_ADJ"), FSNUser.NumberFormat)
                End If
                sCODPROV_Ult_Adj = DBNullToStr(oArts.Data.Tables(0).Rows(0).Item("CODPROV_ULT_ADJ"))
                sDENPROV_Ult_Adj = DBNullToStr(oArts.Data.Tables(0).Rows(0).Item("DENPROV_ULT_ADJ"))
            End If

            bExiste = True
            sMat2 = sGMN1 & sGMN2 & sGMN3 & sGMN4
            'pedido negociado
            If iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoNegociado Then
                sCodMon_Ult_Adj = DBNullToStr(oArts.Data.Tables(0).Rows(0).Item("MON_ULT_ADJ"))
                If bCargaFpago Then
                    sFPagoPROVUltADJ = DBNullToStr(oArts.Data.Tables(0).Rows(0).Item("PAG_ULT_ADJ"))
                    sDenFPagoPROVUltADJ = DBNullToStr(oArts.Data.Tables(0).Rows(0).Item("DENPAG_ULT_ADJ"))
                End If
            End If
            'pedido express
            If (iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoExpress) And Me.FSNServer.TipoAcceso.gbArticulosNegociadosPedExpress Then
                sCODPROV_Ult_Adj = DBNullToStr(oArts.Data.Tables(0).Rows(0).Item("CODPROV_ULT_ADJ"))
            End If
        Else
            bExiste = False
            sNumeroDeDecimales = "null"
        End If

        If desde = "ARTICULO" Then
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>ponerCodigoArticulo('" & ArtfsEntry & "','" & DenfsEntry & "'," & IIf(bGenerico, "true", "false") & "," & IIf(bExiste, "true", "false") & ")</script>")
            If Not Page.ClientScript.IsClientScriptBlockRegistered("MensajeCOD1") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "MensajeCOD1", "<script>var sMensajeCOD = '" & Textos(0) & "'</script>")
            End If
        Else
            If desde = "MATERIAL" And sMat2 <> sMat3 And bExiste Then
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>ponerCodArt_Den_Blanco('" & ArtfsEntry & "','" & DenfsEntry & "')</script>")
            Else
                If desde = "CENTRO" Then 'Si hemos cambiado el centro y ahora el articulo no existe para el mismo, lo borramos
                    If bExiste = False Then
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "BorrarArticulo", "<script>BorrarArticulo('" & ArtfsEntry & "','" & DenfsEntry & "', '" & PRECfsEntry & "','" & UNIfsEntry & "','" & sIDFSEntryUniPedido & "');</script>")
                    End If
                Else
                    Dim sDenBuena As String
                    sDenBuena = Replace(sDen, "\", "\\")
                    sDenBuena = Replace(sDenBuena, "'", "\'")
                    Dim sProveBueno As String
                    sProveBueno = Replace(sDENPROV_Ult_Adj, "\", "\\")
                    sProveBueno = Replace(sProveBueno, "'", "\'")

                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeConceptoGasto", "<script>var mensajeConceptoGasto = '" & Textos(2) & "' </script>")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeConceptoInversion", "<script>var mensajeConceptoInversion = '" & Textos(3) & "' </script>")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeNoAlmacenable", "<script>var mensajeNoAlmacenable = '" & Textos(4) & "' </script>")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeAlmacenamientoObligatorio", "<script>var mensajeAlmacenamientoObligatorio = '" & Textos(5) & "' </script>")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeNoRecepcionable", "<script>var mensajeNoRecepcionable = '" & Textos(6) & "' </script>")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeRecepcionObligatoria", "<script>var mensajeRecepcionObligatoria = '" & Textos(7) & "' </script>")

                    Dim bArticuloValidoNegociadoOExpress As Boolean = True
                    Dim sScript As String = String.Empty
                    If ((iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoExpress) OrElse (iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoNegociado)) Then
                        'PEDIDO NEGOCIADO
                        If (iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoNegociado) Then
                            If sCodMon_Ult_Adj <> "" AndAlso sCodMon_Ult_Adj = sInstanciaMoneda AndAlso Not bGenerico AndAlso oArts.Data.Tables(0).Rows.Count > 0 Then
                                sScript = "<script>rellenarCampos('" & ArtfsEntry & "','" & DenfsEntry & "','" & sDenBuena & "'," & IIf(bExiste, "true", "false") & ",'" & PRECfsEntry & "','" & sPREC_Ult_Adj & "','" & PROVfsEntry & "','" & sCODPROV_Ult_Adj & "','" & sProveBueno & "','" & UNIfsEntry & "','" & sUnidad & "','" & sDenUnidad & "'," & IIf(bGenerico, "true", "false") & ",'" & OrgfsEntry & "','" & sCodOrgCompras & "','" & sDenOrgCompras & "','" & CentrofsEntry & "','" & sCodCentro & "','" & sDenCentro & "','" & sConceptoTipoPedido & "','" & sAlmacenamientoTipoPedido & "','" & sRecepcionTipoPedido & "','" & sConcepto & "','" & sAlmacenamiento & "','" & sRecepcion & "','" & CANTfsEntry & "'," & sNumeroDeDecimales & ",'" & iTipoSolicitud & "','" & sInstanciaMoneda & "','" & sIDFSEntryProve & "','" & sIDFSEntryUniPedido & "', '" & sUnidadPedido & "', '" & sDenUnidadPedido & "', '" & sIDFSEntryFPago & "', '" & sIDFSEntryFPagoHidden & "', '" & sFPagoPROVUltADJ & "', '" & sDenFPagoPROVUltADJ & "', '" & sTipoRecepcion & "')</script>"
                            Else
                                sScript = "<script>"
                                If sCodMon_Ult_Adj <> "" AndAlso sCodMon_Ult_Adj <> sInstanciaMoneda Then
                                    '�Imposible seleccionar el art�culo! �nicamente es posible seleccionar art�culos adjudicados en la misma moneda de la solicitud ###. Ha seleccionado un art�culo adjudicado en @@@.
                                    Dim sMensajeMonedaAdjudicacion = Replace(Replace(Textos(9), "###", sInstanciaMoneda), "@@@", sCodMon_Ult_Adj)
                                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeMonedaAdjudicacion", "<script>var mensajeMonedaAdjudicacion= '" & JSText(sMensajeMonedaAdjudicacion) & "' </script>")
                                    sScript += "alert(mensajeMonedaAdjudicacion);"
                                End If
                                If bGenerico Then
                                    'No es posible seleccionar art�culos gen�ricos en las solicitudes de tipo "Pedido Negociado".
                                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeArticuloGenerico", "<script>var mensajeArticuloGenerico= '" & JSText(Textos(10)) & "' </script>")
                                    sScript += "alert(mensajeArticuloGenerico);"
                                End If
                                If oArts.Data.Tables(0).Rows.Count = 0 Then
                                    'No es posible seleccionar art�culos sin adjudicaciones vigentes en las solicitudes de tipo "Pedido Negociado".
                                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeSinAdjudicacionesVigentes", "<script>var mensajeSinAdjudicacionesVigentes= '" & JSText(Textos(8)) & "' </script>")
                                    sScript += "alert(mensajeSinAdjudicacionesVigentes);"
                                End If
                                sScript += "BorrarArticulo('" & ArtfsEntry & "','" & DenfsEntry & "', '" & PRECfsEntry & "','" & UNIfsEntry & "','" & sIDFSEntryUniPedido & "');"
                                sScript += "</script>"
                                bArticuloValidoNegociadoOExpress = False
                            End If
                        End If
                        'PEDIDO EXPR�S
                        If (iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoExpress And Not bArticulosNegociadosExpress) Then
                            If oArts.Data.Tables(0).Rows.Count = 0 Then
                                sScript = "<script>"
                                If oArts.DataUltAdjVig IsNot Nothing AndAlso oArts.DataUltAdjVig.Tables.Count > 0 AndAlso oArts.DataUltAdjVig.Tables(0).Rows.Count > 0 _
                                        And Not bArticulosNegociadosExpress Then
                                    'Este art�culo tiene adjudicaciones vigentes:
                                    'Proceso: xxxx/xxx/xxx - xxxxxxx
                                    'Proveedor: xxxxxxxx - xxxxxxxx
                                    'Para realizar un pedido debe seleccionar la solicitud de "Pedido Negociado".

                                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeConAdjudicacionesVigentes",
                                                                                "<script>var mensajeConAdjudicacionesVigentes= '" & Textos(11) & ":\n" &
                                                                                                                                    Textos(13) & ": " & sUltAdjAnyo & "/" & sUltAdjGmn1 & "/" & sUltAdjProceCod.Replace("'", "''") & " - " & sUltAdjProceDen.Replace("'", "''") & "\n" &
                                                                                                                                    Textos(14) & ": " & sUltAdjProveCod.Replace("'", "''") & " - " & sUltAdjProveDen.Replace("'", "''") & "\n" &
                                                                                                                                    Textos(12) & "'</script>")
                                    sScript += "alert(mensajeConAdjudicacionesVigentes);"
                                End If
                                sScript += "BorrarArticulo('" & ArtfsEntry & "','" & DenfsEntry & "', '" & PRECfsEntry & "','" & UNIfsEntry & "','" & sIDFSEntryUniPedido & "');"
                                sScript += "</script>"
                                bArticuloValidoNegociadoOExpress = False
                            Else
                                sScript = "<script>rellenarCampos('" & ArtfsEntry & "','" & DenfsEntry & "','" & sDenBuena & "'," & IIf(bExiste, "true", "false") & ",'" & PRECfsEntry & "','" & sPREC_Ult_Adj & "','" & PROVfsEntry & "','" & sCODPROV_Ult_Adj & "','" & sProveBueno & "','" & UNIfsEntry & "','" & sUnidad & "','" & sDenUnidad & "'," & IIf(bGenerico, "true", "false") & ",'" & OrgfsEntry & "','" & sCodOrgCompras & "','" & sDenOrgCompras & "','" & CentrofsEntry & "','" & sCodCentro & "','" & sDenCentro & "','" & sConceptoTipoPedido & "','" & sAlmacenamientoTipoPedido & "','" & sRecepcionTipoPedido & "','" & sConcepto & "','" & sAlmacenamiento & "','" & sRecepcion & "','" & CANTfsEntry & "'," & sNumeroDeDecimales & ",'" & iTipoSolicitud & "','" & sInstanciaMoneda & "','" & sIDFSEntryProve & "','" & sIDFSEntryUniPedido & "', '" & sUnidadPedido & "', '" & sDenUnidadPedido & "','','','','', '" & sTipoRecepcion & "');</script>"
                            End If
                        End If
                        'PEDIDO EXPRESS ESPECIAL, permitir articulos negociados
                        If (bArticulosNegociadosExpress) Then
                            sScript = "<script>"
                            If sCodMon_Ult_Adj <> "" AndAlso sCodMon_Ult_Adj <> sInstanciaMoneda Then
                                '�Imposible seleccionar el art�culo! �nicamente es posible seleccionar art�culos adjudicados en la misma moneda de la solicitud ###. Ha seleccionado un art�culo adjudicado en @@@.
                                Dim sMensajeMonedaAdjudicacion = Replace(Replace(Textos(9), "###", sInstanciaMoneda), "@@@", sCodMon_Ult_Adj)
                                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeMonedaAdjudicacion", "<script>var mensajeMonedaAdjudicacion= '" & JSText(sMensajeMonedaAdjudicacion) & "' </script>")
                                sScript += "alert(mensajeMonedaAdjudicacion);"
                                bArticuloValidoNegociadoOExpress = False
                            Else
                                If oArts.Data.Tables(0).Rows.Count = 0 Then
                                    sScript += "rellenarCampos('" & ArtfsEntry & "','" & DenfsEntry & "','" & sDenBuena & "'," & IIf(bExiste, "true", "false") & ",'" & PRECfsEntry & "','" & sPREC_Ult_Adj & "','" & PROVfsEntry & "','" & sCODPROV_Ult_Adj & "','" & sProveBueno & "','" & UNIfsEntry & "','" & sUnidad & "','" & sDenUnidad & "'," & IIf(bGenerico, "true", "false") & ",'" & OrgfsEntry & "','" & sCodOrgCompras & "','" & sDenOrgCompras & "','" & CentrofsEntry & "','" & sCodCentro & "','" & sDenCentro & "','" & sConceptoTipoPedido & "','" & sAlmacenamientoTipoPedido & "','" & sRecepcionTipoPedido & "','" & sConcepto & "','" & sAlmacenamiento & "','" & sRecepcion & "','" & CANTfsEntry & "'," & sNumeroDeDecimales & ",'" & iTipoSolicitud & "','" & sInstanciaMoneda & "','" & sIDFSEntryProve & "','" & sIDFSEntryUniPedido & "', '" & sUnidadPedido & "', '" & sDenUnidadPedido & "','','','','', '" & sTipoRecepcion & "');"
                                Else
                                    sScript += "rellenarCampos('" & ArtfsEntry & "','" & DenfsEntry & "','" & sDenBuena & "'," & IIf(bExiste, "true", "false") & ",'" & PRECfsEntry & "','" & sPREC_Ult_Adj & "','" & PROVfsEntry & "','" & sCODPROV_Ult_Adj & "','" & sProveBueno & "','" & UNIfsEntry & "','" & sUnidad & "','" & sDenUnidad & "'," & IIf(bGenerico, "true", "false") & ",'" & OrgfsEntry & "','" & sCodOrgCompras & "','" & sDenOrgCompras & "','" & CentrofsEntry & "','" & sCodCentro & "','" & sDenCentro & "','" & sConceptoTipoPedido & "','" & sAlmacenamientoTipoPedido & "','" & sRecepcionTipoPedido & "','" & sConcepto & "','" & sAlmacenamiento & "','" & sRecepcion & "','" & CANTfsEntry & "'," & sNumeroDeDecimales & ",'" & iTipoSolicitud & "','" & sInstanciaMoneda & "','" & sIDFSEntryProve & "','" & sIDFSEntryUniPedido & "', '" & sUnidadPedido & "', '" & sDenUnidadPedido & "', '" & sIDFSEntryFPago & "', '" & sIDFSEntryFPagoHidden & "', '" & sFPagoPROVUltADJ & "', '" & sDenFPagoPROVUltADJ & "', '" & sTipoRecepcion & "');"
                                End If
                            End If
                            sScript += "</script>"
                        End If
                    Else
                        sScript = "<script>rellenarCampos('" & ArtfsEntry & "','" & DenfsEntry & "','" & sDenBuena & "'," & IIf(bExiste, "true", "false") & ",'" & PRECfsEntry & "','" & sPREC_Ult_Adj & "','" & PROVfsEntry & "','" & sCODPROV_Ult_Adj & "','" & sProveBueno & "','" & UNIfsEntry & "','" & sUnidad & "','" & sDenUnidad & "'," & IIf(bGenerico, "true", "false") & ",'" & OrgfsEntry & "','" & sCodOrgCompras & "','" & sDenOrgCompras & "','" & CentrofsEntry & "','" & sCodCentro & "','" & sDenCentro & "','" & sConceptoTipoPedido & "','" & sAlmacenamientoTipoPedido & "','" & sRecepcionTipoPedido & "','" & sConcepto & "','" & sAlmacenamiento & "','" & sRecepcion & "','" & CANTfsEntry & "'," & sNumeroDeDecimales & ",'" & iTipoSolicitud & "','" & sInstanciaMoneda & "','" & sIDFSEntryProve & "','" & sIDFSEntryUniPedido & "', '" & sUnidadPedido & "', '" & sDenUnidadPedido & "','','','','', '" & sTipoRecepcion & "');</script>"
                    End If

                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", sScript)

                    If Not ClientScript.IsClientScriptBlockRegistered("MensajeCOD2") Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "MensajeCOD2", "<script>var sMensajeCOD = '" & Textos(1) & "'</script>")
                    End If
                    'Cuando tengamos un art�culo no v�lido para una solicitud de pedido Negociado o Expres, no queremos ejecutar el resto:
                    'bArticuloValidoNegociadoOExpress ser� true cuando la solicitud no sea de pedido Negociado o Expres
                    'Y cuando sea un art�culo v�lido para una solicitud de tipo Negociado o Expres
                    If bArticuloValidoNegociadoOExpress Then
                        If bUsar_OrgCompras AndAlso Request("CodOrgCompras") <> "" AndAlso Request("CodOrgCompras") <> sCodOrgCompras Then
                            'Los presupuestos se borran
                            Page.ClientScript.RegisterStartupScript(Me.GetType(), "BorrarPresupuestos", "<script>borrarPresupuestos('" & ArtfsEntry & "')</script>")
                        End If

                        If bExiste Then
                            If Not ClientScript.IsClientScriptBlockRegistered("GMNs") Then
                                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "GMN1", "<script>var ilGMN1 = " & FSNServer.LongitudesDeCodigos.giLongCodGMN1 & "</script>")
                                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "GMN2", "<script>var ilGMN2 = " & FSNServer.LongitudesDeCodigos.giLongCodGMN2 & "</script>")
                                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "GMN3", "<script>var ilGMN3 = " & FSNServer.LongitudesDeCodigos.giLongCodGMN3 & "</script>")
                                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "GMN4", "<script>var ilGMN4 = " & FSNServer.LongitudesDeCodigos.giLongCodGMN4 & "</script>")
                            End If
                            Dim sDenGMNBuena As String
                            sDenGMNBuena = Replace(sDenGMN, "\", "\\")
                            sDenGMNBuena = Replace(sDenGMNBuena, "'", "\'")

                            Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript2", "<script>ponerMaterial('" & MatfsEntry & "','" & sGMN1 & "','" & sGMN2 & "','" & sGMN3 & "','" & sGMN4 _
                                        & "','" & sDenGMNBuena & "','" & ArtfsEntry & "','" & DenfsEntry & "','" & IIf(Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles, 1, 0) & "')</script>")
                        End If
                    End If

                End If
            End If
        End If
    End Sub


    ''' <summary>
    ''' Al cargar lineas desde excel. 
    ''' Si la den art es readonly, hay q cargar aqui la den. 
    ''' Si el gmn es readonly, hay q cargar aqui el material. 
    ''' Si el articulo no es generico y esta marcado para traer precio ult adj, hay q cargar aqui el precio.
    ''' </summary>
    ''' <param name="idArt">Codigo de art�culo</param>
    ''' <param name="fsEntryArt">entry art�culo</param>
    ''' <param name="fsEntryDen">entry denominaci�n</param>
    ''' <param name="fsEntryMat">entry material</param>
    ''' <param name="fsEntryPrec">entry precio unitario</param>
    ''' <param name="InstanciaMoneda">moneda en la mostrar el precio</param>
    ''' <returns>Array con 0-Den Art,1-Generico, 2-Precio Ult Adj, 3-Den Gmn, 4-Cod Gmn, 5-Tooltip Gmn</returns>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function CargarDesdeExcel(idArt, fsEntryArt, fsEntryDen, fsEntryMat, fsEntryPrec, InstanciaMoneda) As Object
        Try
            Dim miArray() As String = {"0", "0", "0", "0", "0", "0"}

            Dim sArt As String
            Dim ArtfsEntry As String
            Dim DenfsEntry As String
            Dim MatfsEntry As String
            Dim oArts As FSNServer.Articulos
            Dim sDen As String = String.Empty
            Dim bGenerico As Boolean
            Dim bExiste As Boolean
            Dim sPREC_Ult_Adj As String
            Dim sInstanciaMoneda As String = ""
            Dim bCargarUltAdj As Boolean
            Dim sGMN1, sGMN2, sGMN3, sGMN4, sDenGMN As String

            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")

            sArt = idArt
            ArtfsEntry = fsEntryArt
            DenfsEntry = fsEntryDen
            MatfsEntry = fsEntryMat

            If InstanciaMoneda <> "" Then sInstanciaMoneda = InstanciaMoneda
            If fsEntryPrec <> "" Then bCargarUltAdj = True

            oArts = FSNServer.Get_Object(GetType(FSNServer.Articulos))
            oArts.LoadData(FSNUser.Cod, FSNUser.PMRestriccionUONSolicitudAUONUsuario, FSNUser.PMRestriccionUONSolicitudAUONPerfil,
                           sArt, , True, , , , FSNUser.Idioma, , sInstanciaMoneda, bCargarUltAdj, RestringirMaterialUsuario:=AccionesDeSeguridad.PMRestringirMaterialAsignadoUsuario, AccesoFSEP:=FSNServer.TipoAcceso.gbAccesoFSEP)

            If oArts.Data.Tables(0).Rows.Count > 0 Then
                sDen = oArts.Data.Tables(0).Rows(0).Item("DEN")
                bGenerico = CBool(oArts.Data.Tables(0).Rows(0).Item("GENERICO"))
                sGMN1 = oArts.Data.Tables(0).Rows(0).Item("GMN1")
                sGMN2 = oArts.Data.Tables(0).Rows(0).Item("GMN2")
                sGMN3 = oArts.Data.Tables(0).Rows(0).Item("GMN3")
                sGMN4 = oArts.Data.Tables(0).Rows(0).Item("GMN4")
                sDenGMN = oArts.Data.Tables(0).Rows(0).Item("DENGMN")
                If bCargarUltAdj AndAlso Not bGenerico Then
                    If Not IsDBNull(oArts.Data.Tables(0).Rows(0).Item("PREC_ULT_ADJ")) Then
                        sPREC_Ult_Adj = FSNLibrary.FormatNumber(oArts.Data.Tables(0).Rows(0).Item("PREC_ULT_ADJ"), FSNUser.NumberFormat)
                    End If
                End If
                bExiste = True
            Else
                bExiste = False
            End If

            Dim sDenGMNBuena As String
            Dim ilGMN1 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN1
            Dim ilGMN2 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN2
            Dim ilGMN3 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN3
            Dim ilGMN4 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN4
            sDenGMNBuena = Replace(sDenGMN, "\", "\\")
            sDenGMNBuena = Replace(sDenGMNBuena, "'", "\'")
            sDenGMNBuena = sGMN4 & " - " & sDenGMNBuena

            Dim sDenGMNTipBuena As String = "(" & sGMN1
            sDenGMNTipBuena = sDenGMNTipBuena & " - " & sGMN2
            sDenGMNTipBuena = sDenGMNTipBuena & " - " & sGMN3
            sDenGMNTipBuena = sDenGMNTipBuena & " - " & sGMN4 & ")"

            Dim sDenBuena As String
            sDenBuena = Replace(sDen, "\", "\\")
            sDenBuena = Replace(sDenBuena, "'", "\'")
            sDenBuena = sDenBuena

            While sGMN1.Length < ilGMN1
                sGMN1 = " " & sGMN1
            End While
            While sGMN2.Length < ilGMN2
                sGMN2 = " " & sGMN2
            End While
            While sGMN3.Length < ilGMN3
                sGMN3 = " " & sGMN3
            End While
            While sGMN4.Length < ilGMN4
                sGMN4 = " " & sGMN4
            End While
            Dim sCodGMNBuena As String = sGMN1 & sGMN2 & sGMN3 & sGMN4

            miArray(0) = sDenBuena
            miArray(1) = IIf(bGenerico, "true", "false")
            miArray(2) = sPREC_Ult_Adj
            miArray(3) = sDenGMNBuena
            miArray(4) = sCodGMNBuena
            miArray(5) = sDenGMNTipBuena

            Return miArray
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class

