<%@ Page Language="vb" AutoEventWireup="false" Codebehind="popUpTexto.aspx.vb" Inherits="Fullstep.FSNWeb.popUpTexto" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name=vs_defaultClientScript content="JavaScript">
		<meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
   <body onload="doInit();">
	<form id="form1" runat="server">
	<table width="100%" border="0">
	<tr>
		<td align="center">
			<asp:TextBox id="txtTexto" runat="server" TextMode="Multiline" ReadOnly="true" Width="100%" CssClass="textarea"></asp:TextBox>
		</td>
	</tr>
	<tr>
		<td align="center" style="padding-top:5px">
		<asp:Button ID="cmdAceptar" runat="server" OnClientClick="window.close(); return false;" />
		</td>
	</tr>
	</table>
	</form>
  </body>
  
</html>
