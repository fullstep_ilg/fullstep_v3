
    Public Class presupuestosreadonly
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents tblPresupuestos As System.Web.UI.HtmlControls.HtmlTable
        Protected WithEvents cmdAceptar As System.Web.UI.HtmlControls.HtmlInputButton

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oUser As FSNServer.User = Session("FSN_User")
        Dim sIdi As String = Session("FSN_User").Idioma.ToString()
            Dim lInstancia As Long
            Dim lSolicitud As Long

            Dim oDSPresupuestos As DataSet = New DataSet

            oDSPresupuestos.Tables.Add(New DataTable("Presupuestos"))
            oDSPresupuestos.Tables(0).Columns.Add("UON1", Type.GetType("System.String"))
            oDSPresupuestos.Tables(0).Columns.Add("DEN_UON1", Type.GetType("System.String"))
            oDSPresupuestos.Tables(0).Columns.Add("UON2", Type.GetType("System.String"))
            oDSPresupuestos.Tables(0).Columns.Add("DEN_UON2", Type.GetType("System.String"))
            oDSPresupuestos.Tables(0).Columns.Add("UON3", Type.GetType("System.String"))
            oDSPresupuestos.Tables(0).Columns.Add("DEN_UON3", Type.GetType("System.String"))
            oDSPresupuestos.Tables(0).Columns.Add("PRES1", Type.GetType("System.String"))
            oDSPresupuestos.Tables(0).Columns.Add("DEN_PRES1", Type.GetType("System.String"))
            oDSPresupuestos.Tables(0).Columns.Add("PRES2", Type.GetType("System.String"))
            oDSPresupuestos.Tables(0).Columns.Add("DEN_PRES2", Type.GetType("System.String"))
            oDSPresupuestos.Tables(0).Columns.Add("PRES3", Type.GetType("System.String"))
            oDSPresupuestos.Tables(0).Columns.Add("DEN_PRES3", Type.GetType("System.String"))
            oDSPresupuestos.Tables(0).Columns.Add("PRES4", Type.GetType("System.String"))
            oDSPresupuestos.Tables(0).Columns.Add("DEN_PRES4", Type.GetType("System.String"))
            oDSPresupuestos.Tables(0).Columns.Add("ANYO", Type.GetType("System.Int32"))
            oDSPresupuestos.Tables(0).Columns.Add("PORCENTAJE", Type.GetType("System.Double"))

            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If

            Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
            oDict.LoadData(TiposDeDatos.ModulosIdiomas.Presupuestos, sIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)

            Me.cmdAceptar.Value = oTextos.Rows(2).Item(1)

            Dim iTipo As Integer

            If Request("Solicitud") <> Nothing Then
                lSolicitud = Request("Solicitud")
            End If

            If Request("Instancia") <> Nothing Then
                lInstancia = Request("Instancia")
            End If

            Dim oCampo As FSNServer.Campo
            Dim idCampo As Integer = CInt(Request("Campo"))
            oCampo = FSWSServer.Get_Object(GetType(FSNServer.Campo))
            oCampo.Id = idCampo

            If lInstancia > 0 Then
                oCampo.LoadInst(lInstancia, sIdi)
            Else
                oCampo.Load(sIdi, lSolicitud)
            End If

            Select Case oCampo.IdTipoCampoGS
                Case TiposDeDatos.TipoCampoGS.PRES1
                    iTipo = 1
                Case TiposDeDatos.TipoCampoGS.Pres2
                    iTipo = 2
                Case TiposDeDatos.TipoCampoGS.Pres3
                    iTipo = 3
                Case TiposDeDatos.TipoCampoGS.Pres4
                    iTipo = 4
            End Select

            Dim bConCantidad As Boolean
            Dim dCantidad As Decimal
            bConCantidad = (Request("CANTIDAD") <> Nothing)
            If bConCantidad Then
                dCantidad = Numero(Request("CANTIDAD"), ".", ",")
                If dCantidad = 0 Then
                    bConCantidad = False
                End If
            End If
            


            Dim Valor As String = Request("Valor")
            If Valor <> "" Then
                Dim arrPresupuestos() As String = Valor.Split("#")
                Dim arrPresup As String
                Dim arrPresupuesto() As String
                Dim iNivel As Integer
                Dim lIdPresup As Integer
                Dim dPorcent As Double

                Dim oPres1 As FSNServer.PresProyectosNivel1

                For Each arrPresup In arrPresupuestos
                    arrPresupuesto = arrPresup.Split("_")

                    iNivel = arrPresupuesto(0)
                    lIdPresup = arrPresupuesto(1)
                    dPorcent = Numero(arrPresupuesto(2), ".", ",")

                    Dim oNuevaLineaPres As DataRow = oDSPresupuestos.Tables(0).NewRow

                    'A partir del id del presupuesto y el nivel obtenemos los c�digos de unidad organizativa y de presupuesto en el dataset DS
                    Select Case oCampo.IdTipoCampoGS
                        Case TiposDeDatos.TipoCampoGS.PRES1
                            iTipo = 1

                        Case TiposDeDatos.TipoCampoGS.Pres2
                            iTipo = 2


                        Case TiposDeDatos.TipoCampoGS.Pres3
                            iTipo = 3


                        Case TiposDeDatos.TipoCampoGS.Pres4
                            iTipo = 4

                    End Select
                    oPres1 = FSWSServer.Get_Object(GetType(FSNServer.PresProyectosNivel1))
                    oPres1.DetallePresupuesto(iTipo, iNivel, lIdPresup)
                    oNuevaLineaPres("UON1") = oPres1.Data.Tables(0).Rows(0)("UON1")
                    oNuevaLineaPres("DEN_UON1") = oPres1.Data.Tables(0).Rows(0)("DEN_UON1")
                    oNuevaLineaPres("UON2") = oPres1.Data.Tables(0).Rows(0)("UON2")
                    oNuevaLineaPres("DEN_UON2") = oPres1.Data.Tables(0).Rows(0)("DEN_UON2")
                    oNuevaLineaPres("UON3") = oPres1.Data.Tables(0).Rows(0)("UON3")
                    oNuevaLineaPres("DEN_UON3") = oPres1.Data.Tables(0).Rows(0)("DEN_UON3")
                    oNuevaLineaPres("PRES1") = oPres1.Data.Tables(0).Rows(0)("PRES1")
                    oNuevaLineaPres("DEN_PRES1") = oPres1.Data.Tables(0).Rows(0)("DEN_PRES1")
                    oNuevaLineaPres("PRES2") = oPres1.Data.Tables(0).Rows(0)("PRES2")
                    oNuevaLineaPres("DEN_PRES2") = oPres1.Data.Tables(0).Rows(0)("DEN_PRES2")
                    oNuevaLineaPres("PRES3") = oPres1.Data.Tables(0).Rows(0)("PRES3")
                    oNuevaLineaPres("DEN_PRES3") = oPres1.Data.Tables(0).Rows(0)("DEN_PRES3")
                    oNuevaLineaPres("PRES4") = oPres1.Data.Tables(0).Rows(0)("PRES4")
                    oNuevaLineaPres("DEN_PRES4") = oPres1.Data.Tables(0).Rows(0)("DEN_PRES4")
                    If (iTipo < 3) Then
                        oNuevaLineaPres("ANYO") = oPres1.Data.Tables(0).Rows(0)("ANYO")
                    End If
                    oNuevaLineaPres("PORCENTAJE") = dPorcent
                    oDSPresupuestos.Tables(0).Rows.Add(oNuevaLineaPres)


                Next

                '''Llenar la Tabla HTML con los valores Obtenidos
                Dim fila As HtmlTableRow
                Dim celda As HtmlTableCell
                Dim sHtmlCelda As String

                Dim UON1 As String
                Dim UON2 As String
                Dim UON3 As String

                Dim sPlantillaSeparador As String = "&nbsp;&nbsp;&nbsp;"
                Dim sSeparador As String
                Dim i As Integer
                Dim j As Integer

                For Each dr As DataRow In oDSPresupuestos.Tables(0).Rows
                    If dr("UON1").ToString <> UON1 Or dr("UON2").ToString <> UON2 Or dr("UON3").ToString <> UON3 Then
                        UON1 = dr("UON1").ToString
                        UON2 = dr("UON2").ToString
                        UON3 = dr("UON3").ToString
                        'Metemos Cabecera de Unidad Organizativa:
                        fila = New HtmlTableRow
                        celda = New HtmlTableCell
                        sHtmlCelda = ""
                        For i = 1 To 3
                            sSeparador = ""
                            'UONi:
                            If (Not dr("UON" & i) Is DBNull.Value) Then
                                For j = 1 To i
                                    sSeparador = sSeparador & sPlantillaSeparador
                                Next
                                sHtmlCelda = sHtmlCelda & sSeparador & dr("UON" & i).ToString & " - " & dr("DEN_UON" & i).ToString & "<BR>"
                            End If
                        Next

                        celda.InnerHtml = "<span class='captionBlue'>" & sHtmlCelda & "</span>"
                        celda.Attributes.Add("class", "EstadosFondo1")
                        celda.ColSpan = 2
                        fila.Cells.Add(celda)
                        tblPresupuestos.Rows.Add(fila)
                    End If
                    'Metemos el Presupuesto:
                    fila = New HtmlTableRow
                    celda = New HtmlTableCell
                    'Ahora metemos el Anyo:
                    celda.InnerText = dr("ANYO").ToString
                    celda.VAlign = "top"
                    fila.Cells.Add(celda)

                    celda = New HtmlTableCell

                    Dim bSeleccionado As Boolean = True
                    Dim sIniNegrita As String
                    Dim sFinNegrita As String
                    Dim sPorcentaje As String

                    sHtmlCelda = ""
                    For i = 4 To 1 Step -1
                        sSeparador = ""
                        'PRESi:
                        If (Not dr("PRES" & i) Is DBNull.Value) Then
                            If bSeleccionado Then
                                sIniNegrita = "<B>"
                                sFinNegrita = "</B>"
                                dPorcent = dr("PORCENTAJE")
                                sPorcentaje = " ( " & dPorcent.ToString("0.00%", oUser.NumberFormat) & " )"
                            Else
                                sIniNegrita = ""
                                sFinNegrita = ""
                                sPorcentaje = ""
                            End If
                            For j = 1 To i
                                sSeparador = sSeparador & sPlantillaSeparador
                            Next
                            sHtmlCelda = sSeparador & sIniNegrita & dr("PRES" & i).ToString & " - " & dr("DEN_PRES" & i).ToString & sPorcentaje & sFinNegrita & "<BR>" & sHtmlCelda
                            bSeleccionado = False
                        End If
                    Next

                    celda.InnerHtml = sHtmlCelda
                    fila.Cells.Add(celda)
                    tblPresupuestos.Rows.Add(fila)
                Next


            End If

            
        End Sub

        

    End Class

