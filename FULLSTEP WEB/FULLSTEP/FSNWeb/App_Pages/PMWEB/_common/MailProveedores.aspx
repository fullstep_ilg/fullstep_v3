<%@ Register TagPrefix="igtxt" Namespace="Infragistics.WebUI.WebDataInput" Assembly="Infragistics.WebUI.WebDataInput.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="MailProveedores.aspx.vb" Inherits="Fullstep.FSNWeb.MailProveedores"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<SCRIPT type="text/javascript"><!--
			function NoEnter(event)
			{
				if (event.keyCode==13)
				{
					return false
				}
				return true
			}
			/*
			''' <summary>
			''' Muestra todos los mails a los q enviar el correo
			''' </summary>
			''' <param name="Ctrl">A q concepto va a ir el mail seleccionado</param>
			''' <param name="Mail">mail seleccionado</param>        
			''' <remarks>Llamada desde: BuscadorProveedores.aspx/Aceptar ; Tiempo m�ximo</remarks>		
			*/
			function MasMail (Ctrl,Mail)
			{	
				if (Ctrl=="Para")
					document.forms["Form1"].elements["ParaExtendido"].value = Mail;	
				else
					if (Ctrl=="CC")
						document.forms["Form1"].elements["CCExtendido"].value = Mail;	
					else			
						document.forms["Form1"].elements["CCOExtendido"].value = Mail;					
			}				
			/*''' <summary>
			''' Mostrar una pantalla de selecci�n de contactos.
			''' </summary>
			''' <param name="Ctrl">Donde se van a dejar los contactos</param>        
			''' <remarks>Llamada desde: cmdPara     cmdCC       cmdCCO; Tiempo m�ximo: 0</remarks>*/		
			function Para(Ctrl)
			{	
				if (Ctrl=="Para"){
					var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorProveedores.aspx?" %>desde=Mail&PM=false&Lista=" + document.forms["Form1"].elements["ParaExtendido"].value + "&Ctrl=Para", "_blank", "width=835,height=635,status=yes,resizable=yes,top=0,left=150,scrollbars=yes");
				}
				else
					if (Ctrl=="CC"){
						newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorProveedores.aspx?" %>desde=Mail&PM=false&Lista=" + document.forms["Form1"].elements["CCExtendido"].value + "&Ctrl=CC", "_blank", "width=835,height=635,status=yes,resizable=yes,top=0,left=150,scrollbars=yes");
					}
					else{
						newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorProveedores.aspx?" %>desde=Mail&PM=false&Lista=" + document.forms["Form1"].elements["CCOExtendido"].value + "&Ctrl=CCO", "_blank", "width=835,height=635,status=yes,resizable=yes,top=0,left=150,scrollbars=yes");
					}
                newWindow.focus();
			}	
			/*
			''' <summary>
			''' Abre la pantalla de seleccion de archivo
			''' </summary>
			''' <remarks>Llamada desde: cmdAdjuntar onclick; Tiempo m�ximo:0</remarks>		
			*/		
			function Adjuntar()
			{	
				var adjuntos = document.forms["Form1"].elements["AdjuntoExtendido"].value 
				var Ruta = document.forms["Form1"].elements["txtRuta"].value
				var newWindow = window.open("../_common/attachfileEMail.aspx?att=" + adjuntos + "&Ruta=" + Ruta ,"_blank", "width=390,height=100,status=yes,resizable=no,top=280,left=400");
                newWindow.focus();
			}
			/*
			''' <summary>
			''' Adjunta un fichero al mail (solo cambia la label, lo bbdd va aparte)
			''' </summary>
			''' <param name="fich">fichero</param>
			''' <remarks>Llamada desde: attachfileEMail.aspx ; Tiempo m�ximo:0</remarks>		
			*/				
			function AdjuntarAttach(fich)
			{	
				document.forms["Form1"].elements["AdjuntoExtendido"].value=document.forms["Form1"].elements["AdjuntoExtendido"].value + fich
			}

			function ComprobarDatosMail() {
				if (document.getElementById("ParaExtendido").value == "") {
					alert(arrTextosML[5]);
					return false;
				}else{
				var addresses = (document.getElementById("ParaExtendido").value + ";" + document.getElementById("CCExtendido").value + ";" + document.getElementById("CCOExtendido").value).split(";");
				for (i = 0; i < addresses.length; i++) {
					if (!ValidateMailAdress(addresses[i].replace(/^\s+/g, '').replace(/\s+$/g, ''))) {
							alert(arrTextosML[6].replace("#####",addresses[i]));
							return false
						}
					}
				}
				return true            
			}

			function ValidateMailAdress(address) {
				var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
				if (address != "") {         
					if (reg.test(address) == false) {                
						return false;
					}
				}
				return true;
			}
--></SCRIPT>	
	<body>
		<form id="Form1" method="post" runat="server">
		<div style="clear:both; float:left; margin-top:10px; margin-left:10px;">            
			<asp:Label runat="server" ID="lbltitulo" CssClass="subtitulo">DEnvio de emails a proveedores</asp:Label>
		</div>
		<div style="float:right; margin-top:10px;">
			<asp:Button runat="server" ID="btnEnviar" CssClass="botonPMWEB" />					   
		</div>
		<div style="clear:both; float:left; margin-top:3px; margin-left:10px;">
			<asp:Label runat="server" id="lblInfo" CssClass="parrafo" Text="...."></asp:Label>
		</div>
		<div style="clear:both; float:left; margin-top:3px; padding-left:10px;line-height:20px;">
			<input runat="server" id="cmdPara" name="cmdPara" type="button" value="DPara" onclick="Para('Para')"
				style="width: 70px; height: 20px" class="botonPMWEB"/>
			<textarea runat="server" id="ParaExtendido" name="ParaExtendido" onkeypress="return NoEnter(event)" 
				style="width: 585px; height:20px; vertical-align:bottom" rows="1" cols="71"></textarea>
		</div>
		<div style="clear:both; float:left; margin-top:3px; padding-left:10px; line-height:20px;">
			<input runat="server" id="cmdCC" name="cmdCC" type="button" value="DCc" onclick="Para('CC')"
				style="width: 70px; height: 20px" class="botonPMWEB" />
			<textarea runat="server" id="CCExtendido" name="CCExtendido" onkeypress="return NoEnter(event)" 
				style="width: 585px; height:20px; vertical-align:bottom" rows="1" cols="71"></textarea>            
		</div>
		<div style="clear:both; float:left; margin-top:3px; padding-left:10px; line-height:20px;">
			<input runat="server" id="cmdCCO" name="cmdCCO" type="button" value="DCCo" onclick="Para('CCO')"
				style="width: 70px; height: 20px" class="botonPMWEB" />
			<textarea runat="server" id="CCOExtendido" name="CCOExtendido" onkeypress="return NoEnter(event)"
				style="width: 585px; height:20px; vertical-align:bottom" rows="1" cols="71"></textarea>
		</div>
        <div style="clear:both; float:left; margin-top:3px; padding-left:10px; line-height:20px;">
            <div style="width: 70px; height: 20px; text-align: center; display: inline-block">
                <asp:Label runat="server" id="lblAsunto" CssClass="parrafo">DAsunto</asp:Label>
            </div>			
			<asp:TextBox runat="server" id="ugtxtAsunto" MaxLength="500" Wrap="False" style="width: 585px; height:20px; vertical-align:bottom"></asp:TextBox>
		</div>
		<div style="clear:both; float:left; margin-top:3px; padding-left:10px;">
			<input runat="server" id="cmdAdjuntar" name="cmdAdjuntar" type="button" value="Adjuntar..." onclick="Adjuntar()"
				style="width: 70px; height: 20px" class="botonPMWEB" />
			<textarea runat="server" id="AdjuntoExtendido" name="AdjuntoExtendido" onkeypress="return NoEnter(event)"
				style="width: 585px; height:20px; vertical-align:bottom" rows="1" cols="71"></textarea>
		</div>
		<div style="clear:both; float:left; margin-top:3px; padding-left:10px;">
			<asp:TextBox runat="server" id="ugtxtTexto" TextMode="MultiLine" MaxLength="2000" Width="660px" Height="144px" Rows="4"></asp:TextBox>
			<input id="txtRuta" style="z-index: 111; left: 320px; width: 160px; position: absolute;
				top: 8px; height: 24px" type="hidden" size="21" name="txtRuta" runat="server"/>
		</div>
		</form>
	</body>
</html>
