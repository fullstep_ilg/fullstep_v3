
    Public Class materiales
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents uwtMateriales As Infragistics.WebUI.UltraWebNavigator.UltraWebTree
        Protected WithEvents cmdAceptar As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents cmdCancelar As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents IDCAMPO As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents IDCONTROL As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents IDCONTROLVALUE As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents IDControl_MaterialGS As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents desde As System.Web.UI.HtmlControls.HtmlInputHidden

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Const IncludeScriptFormat As String = ControlChars.CrLf & _
  "<script type=""{0}"" src=""{1}""></script>"
        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
    "<script language=""{0}"">{1}</script>"
    ''' <summary>
    ''' Carga la pagina de materiales. Carga el grid con los materiales.
    ''' </summary>
    ''' <param name="sender">propiedades del evento</param>
    ''' <param name="e">propiedades del evento</param>        
    ''' <remarks>Tiempo m�ximo=1seg.</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Put user code to initialize the page here
        Me.IDCONTROL.Value = Request("IdControl")
        Me.IDCONTROLVALUE.Value = Request("IdControlValue")
        Me.desde.Value = Request("desde")
        Me.IDControl_MaterialGS.Value = Request("MaterialGS_BBDD")

        If Request("readonly") = 1 Then
            Me.cmdAceptar.Visible = False
            Me.cmdCancelar.Visible = False
        End If
        CargarArbolMateriales()
    End Sub
    ''' <summary>
    ''' Carga el arbol de materiales GMN
    ''' </summary>
    ''' <remarks>Llamada desde=Page_Load; Tiempo m�ximo=1.5seg.</remarks>
    Private Sub CargarArbolMateriales()
        Dim oGruposMaterial As FSNServer.GruposMaterial

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Materiales

        Me.lblTitulo.Text = Textos(4)
        Me.cmdAceptar.Value = Textos(1)
        Me.cmdCancelar.Value = Textos(2)

        oGruposMaterial = FSNServer.Get_Object(GetType(FSNServer.GruposMaterial))
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NivelSeleccion", "<script>var NivelSeleccion = " & Me.NivelSeleccion & ";</script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TodosNiveles", "<script>var TodosNiveles = " & IIf(Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles, 1, 0) & ";</script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "txtSeleccionarMaterial", "<script> var sSeleccionarMatNivel='" & Textos(5) & "'; var sSeleccionarMatNiveloSuperior='" & Textos(6) & "';</script>")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sMat", "<script>var sMat='" & Request("Mat") & "'</script>")
        Dim sMat As String = Request("Mat")
        Dim arrMat(4) As String
        Dim lLongCod As Integer
        Dim i As Integer
        For i = 1 To 4
            arrMat(i) = ""
        Next i

        i = 1
        If sMat <> "" Then
            While Trim(sMat) <> ""
                Select Case i
                    Case 1
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                    Case 2
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                    Case 3
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                    Case 4
                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
                End Select
                arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
                sMat = Mid(sMat, lLongCod + 1)
                i = i + 1
            End While
        Else
            If FSNUser.Pyme > 0 Then
                arrMat(1) = oGruposMaterial.DevolverGMN1_PYME(FSNUser.Pyme)
            End If
        End If

        Dim sGMN1 As String = arrMat(1)
        Dim sGMN2 As String = arrMat(2)
        Dim sGMN3 As String = arrMat(3)
        Dim sGMN4 As String = arrMat(4)

        Dim CodProve As String = Request("CodProve")

        oGruposMaterial.LoadData(sGMN1, sGMN2, sGMN3, sGMN4, sIdi:=Idioma, sPer:=FSNUser.CodPersona, PMRestricMatUsu:=FSNUser.FSPMRestrMatUsu, sCodProve:=CodProve, iNivelSeleccion:=NivelSeleccion)
        Me.uwtMateriales.ClearAll()

        Me.uwtMateriales.DataSource = oGruposMaterial.Data.Tables(0).DefaultView
        Me.uwtMateriales.Levels(0).LevelImage = "./images/carpetaCommodityTxiki.gif"
        Me.uwtMateriales.Levels(0).RelationName = "REL_NIV1_NIV2"
        Me.uwtMateriales.Levels(0).ColumnName = "DEN_" + Idioma
        Me.uwtMateriales.Levels(0).LevelKeyField = "COD"
        Me.uwtMateriales.Levels(1).LevelImage = "./images/carpetaFamilia.gif"
        Me.uwtMateriales.Levels(1).RelationName = "REL_NIV2_NIV3"
        Me.uwtMateriales.Levels(1).ColumnName = "DEN_" + Idioma
        Me.uwtMateriales.Levels(1).LevelKeyField = "COD"
        Me.uwtMateriales.Levels(2).LevelImage = "./images/carpetaGrupo.gif"
        Me.uwtMateriales.Levels(2).RelationName = "REL_NIV3_NIV4"
        Me.uwtMateriales.Levels(2).ColumnName = "DEN_" + Idioma
        Me.uwtMateriales.Levels(2).LevelKeyField = "COD"
        Me.uwtMateriales.Levels(3).LevelImage = "./images/carpetaCerrada.gif"
        Me.uwtMateriales.Levels(3).ColumnName = "DEN_" + Idioma
        Me.uwtMateriales.Levels(3).LevelKeyField = "COD"
        Me.uwtMateriales.DataBind()
        Me.uwtMateriales.DataKeyOnClient = True

        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(Textos(3)) + "';"
        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)
    End Sub

    Private Sub uwtMateriales_NodeBound(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebNavigator.WebTreeNodeEventArgs) Handles uwtMateriales.NodeBound
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oUser As FSNServer.User = Session("FSN_User")

        Dim sValor As String = Request("Valor")
        Dim arrMat(4) As String
        Dim lLongCod As Integer
        Dim i As Integer
        For i = 1 To 4
            arrMat(i) = ""
        Next i

        i = 1
        While Trim(sValor) <> ""
            Select Case i
                Case 1
                    lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN1
                Case 2
                    lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN2
                Case 3
                    lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN3
                Case 4
                    lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN4
            End Select
            arrMat(i) = Trim(Mid(sValor, 1, lLongCod))
            sValor = Mid(sValor, lLongCod + 1)
            i = i + 1
        End While


        Dim sGMN1 As String = arrMat(1)
        Dim sGMN2 As String = arrMat(2)
        Dim sGMN3 As String = arrMat(3)
        Dim sGMN4 As String = arrMat(4)
        Dim iNivel As Integer


        If sGMN4 <> Nothing Then
            iNivel = 4
        ElseIf sGMN3 <> Nothing Then
            inivel = 3
        ElseIf sGMN2 <> Nothing Then
            inivel = 2
        Else
            inivel = 1
        End If
        Dim sScript As String

        If e.Node.Level = iNivel - 1 Then
            If iNivel = 4 Then
                If e.Node.DataKey = sGMN4 And e.Node.Parent.DataKey = sGMN3 And e.Node.Parent.Parent.DataKey = sGMN2 And e.Node.Parent.Parent.Parent.DataKey = sGMN1 Then
                    e.Node.Parent.Expanded = True
                    e.Node.Parent.Parent.Expanded = True
                    e.Node.Parent.Parent.Parent.Expanded = True
                    sScript = "var oNode = igtree_getNodeById('uwtMateriales" + e.Node.GetIdString + "');if (oNode) oNode.setSelected(true);"
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "node" + e.Node.DataKey.ToString(), "<script>" + sScript + "</script>")
                End If
            ElseIf iNivel = 3 Then
                If e.Node.DataKey = sGMN3 And e.Node.Parent.DataKey = sGMN2 And e.Node.Parent.Parent.DataKey = sGMN1 Then
                    e.Node.Parent.Expanded = True
                    e.Node.Parent.Parent.Expanded = True
                    sScript = "var oNode = igtree_getNodeById('uwtMateriales" + e.Node.GetIdString + "');if (oNode) oNode.setSelected(true);"
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "node" + e.Node.DataKey.ToString(), "<script>" + sScript + "</script>")
                End If
            ElseIf iNivel = 2 Then
                If e.Node.DataKey = sGMN2 And e.Node.Parent.DataKey = sGMN1 Then
                    e.Node.Parent.Expanded = True
                    sScript = "var oNode = igtree_getNodeById('uwtMateriales" + e.Node.GetIdString + "');if (oNode) oNode.setSelected(true);"
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "node" + e.Node.DataKey.ToString(), "<script>" + sScript + "</script>")
                End If
            ElseIf iNivel = 1 Then
                If e.Node.DataKey = sGMN1 Then
                    sScript = "var oNode = igtree_getNodeById('uwtMateriales" + e.Node.GetIdString + "');if (oNode) oNode.setSelected(true);"
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "node" + e.Node.DataKey.ToString(), "<script>" + sScript + "</script>")
                End If
            End If
        End If
    End Sub

    Public ReadOnly Property NivelSeleccion() As Integer
        Get
            If (Request.QueryString("NivelSeleccion") Is Nothing) Then
                Return 0
            Else
                Return Request.QueryString("NivelSeleccion")
            End If
        End Get
    End Property

    End Class

