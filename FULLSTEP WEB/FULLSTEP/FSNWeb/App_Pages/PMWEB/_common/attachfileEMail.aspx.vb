
Public Class attachfileEMail
    Inherits FSNPage

    Public Titulo As String
    Protected WithEvents cmdUpload As System.Web.UI.HtmlControls.HtmlInputButton
    Private Ruta As String

    Private mbFaltaPComa As Boolean

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblSeleccione As System.Web.UI.WebControls.Label
    Protected WithEvents miFile As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents FSENTRY As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Ruta = Request("Ruta")

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AttachFile

        Titulo = Textos(0)
        Me.lblSeleccione.Text = Textos(0)
        Me.cmdUpload.Value = Textos(3)

        Dim att As String = Request("att")
        mbFaltaPComa = False
        If att <> "" AndAlso Right(att, 1) <> ";" Then
            mbFaltaPComa = True
            att = att & ";"
        End If
    End Sub

    Private Sub cmdUpload_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpload.ServerClick
        Dim lOldTimeOut As Long = Server.ScriptTimeout
        Dim sAdjun As String = ""

        If mbFaltaPComa Then
            sAdjun = ";"
        End If

        sAdjun = sAdjun & System.IO.Path.GetFileName(miFile.PostedFile.FileName)

        If miFile.PostedFile.FileName <> "" Then
            Dim sFich As String = System.IO.Path.GetFileName(miFile.PostedFile.FileName)
            Dim sTemp As String = ConfigurationManager.AppSettings("temp") & "\" & Ruta & "\"

            miFile.PostedFile.SaveAs(sTemp & sFich)

            sAdjun = sAdjun & ";"
        End If

        Server.ScriptTimeout = 1800

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>anyadirAdjunto('" + sAdjun + "')</script>")

        Server.ScriptTimeout = lOldTimeOut
    End Sub

End Class

