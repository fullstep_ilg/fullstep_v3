Imports Infragistics.Web.UI.ListControls
Imports Fullstep.FSNLibrary.Operadores

Public Class parametrosgeneralesPMWEB
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents cmdAceptar As System.Web.UI.WebControls.Button
    Protected WithEvents lblProxExpirar As System.Web.UI.WebControls.Label
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblPeriodoValidez As System.Web.UI.WebControls.Label
    Protected WithEvents tblProveedores As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblNotificaciones As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents chkAdjProve As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkPedDirGS As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkPedApEP As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkAsigAVal As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkAsigAPot As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkAsigProve As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkDesAVal As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkDesAPot As System.Web.UI.WebControls.CheckBox
    Protected WithEvents uwtMateriales As Infragistics.WebUI.UltraWebNavigator.UltraWebTree
    Protected WithEvents chkAltaMat As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblProvePotAVal As System.Web.UI.WebControls.Label
    Protected WithEvents lblNotif As System.Web.UI.WebControls.Label
    Protected WithEvents lblContactoAutom As System.Web.UI.WebControls.Label
    Protected WithEvents SeparaContacto As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents Contacto As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents Expiracion As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents LabelExpiracion As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents LabelPeriodoValidez As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents PeriodoValidez As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents chkManual As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkAutomatica As System.Web.UI.WebControls.CheckBox
    Protected WithEvents tblMatManualAutomatica As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblUsuario As System.Web.UI.WebControls.Label
    Protected WithEvents lblMatManualAutomatica As System.Web.UI.WebControls.Label
    Protected WithEvents tblAutomatica As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblAutomatica As System.Web.UI.WebControls.Label
    Protected WithEvents divAutomatica As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents uwtParametros As Infragistics.WebUI.UltraWebTab.UltraWebTab
    Protected WithEvents tblGeneral As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents txtCodContacto As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents SeparaManualAutomatica As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tblGrupoIdiomas As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblGrupoUNQA As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents idNodoRaiz As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblGrupoUNQA As System.Web.UI.WebControls.Label
    Protected WithEvents tblNotificacionesCalif As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents chkNotifCambioCalif As System.Web.UI.WebControls.CheckBox
    Protected WithEvents SeparaNotificaciones As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tblNotificacionesProximidadNc As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents chkNotifProximFinResolucionNc As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkNotifProximFinAccionesNc As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblComboPeriodoNc As System.Web.UI.WebControls.Label
    Protected WithEvents SeparaNotifProximidadNc As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents wddProximidad As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents wddPeriodoValidez As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents wddContacto As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents wddNiveles As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents wddPeriodoNotifNc As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents wddTipoFiltroMat As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents tblNivelesDefectoUNQAProveedor As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents wddNivelesDefectoUNQAProveedor As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents lblNivelesDefectoUNQAProve As System.Web.UI.WebControls.Label
    Protected WithEvents UNQADefecto As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divAdjProve As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divAsigProve As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divTipoNotifCambioCalif As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblMatConversion As System.Web.UI.WebControls.Label
    Protected WithEvents wddMateriales As Infragistics.Web.UI.ListControls.WebDropDown
    Protected WithEvents rdbMensual As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rdbEnCadaCambio As System.Web.UI.WebControls.RadioButton
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Const IncludeScriptFormat As String = ControlChars.CrLf &
      "<script type=""{0}"" src=""{1}""></script>"
    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf &
      "<script language=""{0}"">{1}</script>"

    Private oParametros As FSNServer.Parametros

    ''' <summary>
    ''' Carga la pantalla y aplica estilos.
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>           
    ''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.Expires = -1%
        If Page.IsPostBack Then
            Me.lblTitulo = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblTitulo")
            Me.lblProxExpirar = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblProxExpirar")
            Me.lblContactoAutom = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblContactoAutom")
            Me.lblProvePotAVal = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblProvePotAVal")
            Me.chkAdjProve = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkAdjProve")
            Me.chkAsigProve = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkAsigProve")
            Me.divAdjProve = Me.uwtParametros.GetTab(0).ContentPane.FindControl("divAdjProve")
            Me.divAsigProve = Me.uwtParametros.GetTab(0).ContentPane.FindControl("divAsigProve")
            Me.divTipoNotifCambioCalif = Me.uwtParametros.GetTab(1).ContentPane.FindControl("divTipoNotifCambioCalif")
            Me.chkPedDirGS = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkPedDirGS")
            Me.chkPedApEP = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkPedApEP")
            Me.lblMatConversion = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblMatConversion")
            Me.wddMateriales = Me.uwtParametros.GetTab(0).ContentPane.FindControl("wddMateriales")
            Me.lblNotif = Me.uwtParametros.GetTab(1).ContentPane.FindControl("lblNotif")
            Me.chkAsigAVal = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkAsigAVal")
            Me.chkAsigAPot = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkAsigAPot")
            Me.chkDesAVal = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkDesAVal")
            Me.chkDesAPot = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkDesAPot")
            Me.chkAltaMat = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkAltaMat")
            Me.lblUsuario = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblUsuario")
            Me.lblMatManualAutomatica = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblMatManualAutomatica")
            Me.chkManual = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkManual")
            Me.chkAutomatica = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkAutomatica")
            Me.lblAutomatica = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblAutomatica")
            Me.wddProximidad = Me.uwtParametros.GetTab(0).ContentPane.FindControl("wddProximidad")
            Me.LabelExpiracion = Me.uwtParametros.GetTab(0).ContentPane.FindControl("LabelExpiracion")
            Me.Expiracion = Me.uwtParametros.GetTab(0).ContentPane.FindControl("Expiracion")
            Me.wddPeriodoValidez = Me.uwtParametros.GetTab(0).ContentPane.FindControl("wddPeriodoValidez")
            Me.LabelPeriodoValidez = Me.uwtParametros.GetTab(0).ContentPane.FindControl("LabelPeriodoValidez")
            Me.PeriodoValidez = Me.uwtParametros.GetTab(0).ContentPane.FindControl("PeriodoValidez")
            Me.wddContacto = Me.uwtParametros.GetTab(0).ContentPane.FindControl("wddContacto")
            Me.SeparaContacto = Me.uwtParametros.GetTab(0).ContentPane.FindControl("SeparaContacto")
            Me.SeparaManualAutomatica = Me.uwtParametros.GetTab(0).ContentPane.FindControl("SeparaManualAutomatica")
            Me.Contacto = Me.uwtParametros.GetTab(0).ContentPane.FindControl("Contacto")
            Me.tblMatManualAutomatica = Me.uwtParametros.GetTab(0).ContentPane.FindControl("tblMatManualAutomatica")
            Me.chkManual = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkManual")
            Me.chkAutomatica = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkAutomatica")
            Me.wddNiveles = Me.uwtParametros.GetTab(0).ContentPane.FindControl("wddNiveles")
            Me.tblProveedores = Me.uwtParametros.GetTab(0).ContentPane.FindControl("tblProveedores")
            Me.chkAdjProve = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkAdjProve")
            Me.chkAsigProve = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkAsigProve")
            Me.divAdjProve = Me.uwtParametros.GetTab(0).ContentPane.FindControl("divAdjProve")
            Me.divAsigProve = Me.uwtParametros.GetTab(0).ContentPane.FindControl("divAsigProve")
            Me.chkPedDirGS = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkPedDirGS")
            Me.chkPedApEP = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkPedApEP")
            Me.tblNotificaciones = Me.uwtParametros.GetTab(1).ContentPane.FindControl("tblNotificaciones")
            Me.uwtMateriales = Me.uwtParametros.GetTab(1).ContentPane.FindControl("uwtMateriales")
            Me.tblNotificacionesCalif = Me.uwtParametros.GetTab(1).ContentPane.FindControl("tblNotificacionesCalif")
            Me.chkNotifCambioCalif = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkNotifCambioCalif")
            Me.SeparaNotificaciones = Me.uwtParametros.GetTab(1).ContentPane.FindControl("SeparaNotificaciones")
            Me.tblNotificacionesProximidadNc = Me.uwtParametros.GetTab(1).ContentPane.FindControl("tblNotificacionesProximidadNc")
            Me.chkNotifProximFinResolucionNc = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkNotifProximFinResolucionNc")
            Me.chkNotifProximFinAccionesNc = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkNotifProximFinAccionesNc")
            Me.lblComboPeriodoNc = Me.uwtParametros.GetTab(1).ContentPane.FindControl("lblComboPeriodoNc")
            Me.wddPeriodoNotifNc = Me.uwtParametros.GetTab(1).ContentPane.FindControl("wddPeriodoNotifNc")
            Me.wddTipoFiltroMat = Me.uwtParametros.GetTab(1).ContentPane.FindControl("wddTipoFiltroMat")
            Me.SeparaNotifProximidadNc = Me.uwtParametros.GetTab(1).ContentPane.FindControl("SeparaNotifProximidadNc")
            Me.chkAsigAVal = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkAsigAVal")
            Me.chkAsigAPot = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkAsigAPot")
            Me.chkDesAVal = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkDesAVal")
            Me.chkDesAPot = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkDesAPot")
            Me.chkAltaMat = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkAltaMat")
            Me.tblGrupoUNQA = Me.uwtParametros.GetTab(0).ContentPane.FindControl("tblGrupoUNQA")
            Me.tblGrupoIdiomas = Me.uwtParametros.GetTab(0).ContentPane.FindControl("tblGrupoIdiomas")
            Me.lblGrupoUNQA = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblGrupoUNQA")
            Me.tblNivelesDefectoUNQAProveedor = Me.uwtParametros.GetTab(0).ContentPane.FindControl("tblNivelesDefectoUNQAProveedor")
            Me.lblNivelesDefectoUNQAProve = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblNivelesDefectoUNQAProve")
            Me.wddNivelesDefectoUNQAProveedor = Me.uwtParametros.GetTab(0).ContentPane.FindControl("wddNivelesDefectoUNQAProveedor")
            Me.wddTipoFiltroMat = Me.uwtParametros.GetTab(0).ContentPane.FindControl("wddTipoFiltroMat")
            Me.rdbMensual = Me.uwtParametros.GetTab(1).ContentPane.FindControl("rdbMensual")
            Me.rdbEnCadaCambio = Me.uwtParametros.GetTab(1).ContentPane.FindControl("rdbEnCadaCambio")
        End If

        AplicarEstilosTab(uwtParametros)
    End Sub

    ''' <summary>
    ''' Realiza la traducci�n multiidioma, establece el click de cmdAceptar, oculta/muestra los diferentes
    ''' campos, combos, checks, listbox y tabs en funci�n de los parametros de la instalaci�n y del usuario
    ''' conectado. Carga los diferentes campos, combos, checks y listbox. Aplica estilos de combos.
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>          
    ''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        'Put user code to initialize the page here

        Dim iProx As Integer
        Dim Item As Infragistics.Web.UI.ListControls.DropDownItem
        Dim Personas As FSNServer.Personas
        Dim i As Integer

        Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.ParametrosGenerales

        Me.uwtParametros.GetTab(0).Text = Textos(26)
        Me.uwtParametros.GetTab(1).Text = Textos(27)

        Me.lblTitulo = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblTitulo")
        Me.lblProxExpirar = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblProxExpirar")
        Me.lblContactoAutom = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblContactoAutom")
        Me.lblProvePotAVal = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblProvePotAVal")
        Me.chkAdjProve = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkAdjProve")
        Me.chkAsigProve = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkAsigProve")
        Me.divAdjProve = Me.uwtParametros.GetTab(0).ContentPane.FindControl("divAdjProve")
        Me.divAsigProve = Me.uwtParametros.GetTab(0).ContentPane.FindControl("divAsigProve")
        Me.chkPedDirGS = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkPedDirGS")
        Me.chkPedApEP = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkPedApEP")
        Me.lblMatConversion = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblMatConversion")
        Me.wddMateriales = Me.uwtParametros.GetTab(0).ContentPane.FindControl("wddMateriales")
        Me.lblNotif = Me.uwtParametros.GetTab(1).ContentPane.FindControl("lblNotif")
        Me.chkAsigAVal = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkAsigAVal")
        Me.chkAsigAPot = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkAsigAPot")
        Me.chkDesAVal = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkDesAVal")
        Me.chkDesAPot = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkDesAPot")
        Me.chkAltaMat = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkAltaMat")
        Me.lblUsuario = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblUsuario")
        Me.lblMatManualAutomatica = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblMatManualAutomatica")
        Me.chkManual = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkManual")
        Me.chkAutomatica = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkAutomatica")
        Me.lblAutomatica = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblAutomatica")
        Me.wddProximidad = Me.uwtParametros.GetTab(0).ContentPane.FindControl("wddProximidad")
        Me.LabelExpiracion = Me.uwtParametros.GetTab(0).ContentPane.FindControl("LabelExpiracion")
        Me.Expiracion = Me.uwtParametros.GetTab(0).ContentPane.FindControl("Expiracion")
        Me.wddPeriodoValidez = Me.uwtParametros.GetTab(0).ContentPane.FindControl("wddPeriodoValidez")
        Me.LabelPeriodoValidez = Me.uwtParametros.GetTab(0).ContentPane.FindControl("LabelPeriodoValidez")
        Me.lblPeriodoValidez = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblPeriodoValidez")
        Me.PeriodoValidez = Me.uwtParametros.GetTab(0).ContentPane.FindControl("PeriodoValidez")
        Me.wddContacto = Me.uwtParametros.GetTab(0).ContentPane.FindControl("wddContacto")
        Me.SeparaContacto = Me.uwtParametros.GetTab(0).ContentPane.FindControl("SeparaContacto")
        Me.SeparaManualAutomatica = Me.uwtParametros.GetTab(0).ContentPane.FindControl("SeparaManualAutomatica")
        Me.Contacto = Me.uwtParametros.GetTab(0).ContentPane.FindControl("Contacto")
        Me.tblMatManualAutomatica = Me.uwtParametros.GetTab(0).ContentPane.FindControl("tblMatManualAutomatica")
        Me.chkManual = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkManual")
        Me.chkAutomatica = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkAutomatica")
        Me.wddNiveles = Me.uwtParametros.GetTab(0).ContentPane.FindControl("wddNiveles")
        Me.tblProveedores = Me.uwtParametros.GetTab(0).ContentPane.FindControl("tblProveedores")
        Me.chkAsigProve = Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkAsigProve")
        Me.tblNotificaciones = Me.uwtParametros.GetTab(1).ContentPane.FindControl("tblNotificaciones")
        Me.uwtMateriales = Me.uwtParametros.GetTab(1).ContentPane.FindControl("uwtMateriales")
        Me.tblNotificacionesCalif = Me.uwtParametros.GetTab(1).ContentPane.FindControl("tblNotificacionesCalif")
        Me.chkNotifCambioCalif = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkNotifCambioCalif")
        Me.divTipoNotifCambioCalif = Me.uwtParametros.GetTab(1).ContentPane.FindControl("divTipoNotifCambioCalif")
        Me.SeparaNotificaciones = Me.uwtParametros.GetTab(1).ContentPane.FindControl("SeparaNotificaciones")
        Me.tblNotificacionesProximidadNc = Me.uwtParametros.GetTab(1).ContentPane.FindControl("tblNotificacionesProximidadNc")
        Me.chkNotifProximFinResolucionNc = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkNotifProximFinResolucionNc")
        Me.chkNotifProximFinAccionesNc = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkNotifProximFinAccionesNc")
        Me.lblComboPeriodoNc = Me.uwtParametros.GetTab(1).ContentPane.FindControl("lblComboPeriodoNc")
        Me.wddPeriodoNotifNc = Me.uwtParametros.GetTab(1).ContentPane.FindControl("wddPeriodoNotifNc")
        Me.SeparaNotifProximidadNc = Me.uwtParametros.GetTab(1).ContentPane.FindControl("SeparaNotifProximidadNc")
        Me.chkAsigAVal = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkAsigAVal")
        Me.chkAsigAPot = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkAsigAPot")
        Me.chkDesAVal = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkDesAVal")
        Me.chkDesAPot = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkDesAPot")
        Me.chkAltaMat = Me.uwtParametros.GetTab(1).ContentPane.FindControl("chkAltaMat")
        Me.tblGrupoUNQA = Me.uwtParametros.GetTab(0).ContentPane.FindControl("tblGrupoUNQA")
        Me.tblGrupoIdiomas = Me.uwtParametros.GetTab(0).ContentPane.FindControl("tblGrupoIdiomas")
        Me.lblGrupoUNQA = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblGrupoUNQA")
        Me.rdbMensual = Me.uwtParametros.GetTab(1).ContentPane.FindControl("rdbMensual")
        Me.rdbEnCadaCambio = Me.uwtParametros.GetTab(1).ContentPane.FindControl("rdbEnCadaCambio")

        Me.tblNivelesDefectoUNQAProveedor = Me.uwtParametros.GetTab(0).ContentPane.FindControl("tblNivelesDefectoUNQAProveedor")
        Me.lblNivelesDefectoUNQAProve = Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblNivelesDefectoUNQAProve")
        Me.wddNivelesDefectoUNQAProveedor = Me.uwtParametros.GetTab(0).ContentPane.FindControl("wddNivelesDefectoUNQAProveedor")
        Me.wddTipoFiltroMat = Me.uwtParametros.GetTab(0).ContentPane.FindControl("wddTipoFiltroMat")
        DirectCast(Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblPanelProve"), Label).Text = Textos(40) & "." & Textos(41)

        Me.lblTitulo.Text = Textos(0)
        Me.lblProxExpirar.Text = Textos(7)
        Me.lblContactoAutom.Text = Textos(20)
        Me.lblProvePotAVal.Text = Textos(8)
        Me.chkAdjProve.Text = Textos(9)
        Me.chkPedDirGS.Text = Textos(10)
        Me.chkPedApEP.Text = Textos(11)
        Me.lblNotif.Text = Textos(13)
        Me.chkAsigAVal.Text = Textos(14)
        Me.chkNotifCambioCalif.Text = Textos(32)
        Me.chkAsigAPot.Text = Textos(15)
        Me.chkDesAVal.Text = Textos(16)
        Me.chkDesAPot.Text = Textos(17)
        Me.chkAltaMat.Text = Textos(18)
        Me.cmdAceptar.Text = Textos(19)
        Me.cmdAceptar.Attributes.Add("onclick", "return montarForm();")

        Me.lblUsuario.Text = Textos(28)
        Me.lblMatManualAutomatica.Text = Textos(22)
        Me.chkManual.Text = Textos(23)
        Me.chkAutomatica.Text = Textos(24)
        Me.lblAutomatica.Text = Textos(25)
        Me.lblGrupoUNQA.Text = Textos(29) 'Denominaciones del nodo raiz de las unidades de negocio
        Me.lblPeriodoValidez.Text = Textos(30)
        Me.chkNotifProximFinResolucionNc.Text = Textos(33)
        Me.chkNotifProximFinAccionesNc.Text = Textos(34)
        Me.lblComboPeriodoNc.Text = Textos(35)
        Me.chkAsigProve.Text = Textos(45)
        Me.lblMatConversion.Text = Textos(46) & ":"
        Me.rdbMensual.Text = Textos(47)
        Me.rdbEnCadaCambio.Text = Textos(48)

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "AlertaNivelUNQADefecto", String.Format(IncludeScriptKeyFormat, "javascript", "var alertaNivelUNQADefecto = '" & Textos(39) & "'"))

        oParametros = FSNServer.Get_Object(GetType(FSNServer.Parametros))
        oParametros.LoadData()

        If Acceso.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso And FSNUser.AccesoQA Then
            'Check Box de notificaciones de errores del calculo de puntuaciones
            If FSNUser.AccesoQA And FSNUser.QAPuntuacionProveedores Then
                With CType(Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkRecibirNotificacionesCalcPunt"), CheckBox)
                    .Visible = True
                    .Text = Textos(36)
                    .Checked = FSNUser.QANotificarErrorCalculoPuntuaciones
                End With
            Else
                CType(Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkRecibirNotificacionesCalcPunt"), CheckBox).Visible = False
            End If

            'COMBO DE PROXIMIDAD Y PERIODO VALIDEZ
            If FSNUser.QACertifModExpiracion = False Then
                Me.LabelExpiracion.Visible = False

                Me.Expiracion.Visible = False
            Else
                '********* Carga el combo de las fechas de proximidad de expiraci�n de los certificados*********
                wddProximidad.TextField = "DEN"
                wddProximidad.ValueField = "ID"
            End If

            If FSNUser.QAModificarPeriodoValidezCertificados = False Then
                Me.LabelPeriodoValidez.Visible = False

                Me.PeriodoValidez.Visible = False
            Else
                '********* Carga el combo de las fechas de proximidad de expiraci�n de los certificados*********
                wddPeriodoValidez.TextField = "DEN"
                wddPeriodoValidez.ValueField = "ID"
            End If

            If FSNUser.QACertifModExpiracion Or FSNUser.QAModificarPeriodoValidezCertificados Then
                'Para el combo del periodo de validez de los certificados a�adimos el valor: Fecha expiraci�n
                If FSNUser.QAModificarPeriodoValidezCertificados Then
                    Item = New Infragistics.Web.UI.ListControls.DropDownItem
                    Item.Value = 0
                    Item.Text = Textos(31)

                    If wddPeriodoValidez.Items.FindItemByValue(Item.Value) Is Nothing Then
                        wddPeriodoValidez.Items.Add(Item)
                    End If
                End If

                'Los d�as:
                For i = 1 To 6
                    Item = New Infragistics.Web.UI.ListControls.DropDownItem
                    Item.Value = i

                    If i = 1 Then
                        Item.Text = i & " " & Textos(1)
                    Else
                        Item.Text = i & " " & Textos(2)
                    End If

                    If FSNUser.QACertifModExpiracion Then
                        If wddProximidad.Items.FindItemByValue(Item.Value) Is Nothing Then
                            wddProximidad.Items.Add(Item)
                        End If
                    End If

                    Item = New Infragistics.Web.UI.ListControls.DropDownItem
                    Item.Value = i
                    If i = 1 Then
                        Item.Text = i & " " & Textos(1)
                    Else
                        Item.Text = i & " " & Textos(2)
                    End If

                    If FSNUser.QAModificarPeriodoValidezCertificados Then
                        If wddPeriodoValidez.Items.FindItemByValue(Item.Value) Is Nothing Then
                            wddPeriodoValidez.Items.Add(Item)
                        End If
                    End If
                Next i

                'Las semanas:
                iProx = 7
                For i = 1 To 3
                    Item = New Infragistics.Web.UI.ListControls.DropDownItem
                    Item.Value = iProx * i
                    If i = 1 Then
                        Item.Text = i & " " & Textos(5)
                    Else
                        Item.Text = i & " " & Textos(6)
                    End If

                    If FSNUser.QACertifModExpiracion Then
                        If wddProximidad.Items.FindItemByValue(Item.Value) Is Nothing Then
                            wddProximidad.Items.Add(Item)
                        End If
                    End If

                    Item = New Infragistics.Web.UI.ListControls.DropDownItem
                    Item.Value = iProx * i
                    If i = 1 Then
                        Item.Text = i & " " & Textos(5)
                    Else
                        Item.Text = i & " " & Textos(6)
                    End If

                    If FSNUser.QAModificarPeriodoValidezCertificados Then
                        If wddPeriodoValidez.Items.FindItemByValue(Item.Value) Is Nothing Then
                            wddPeriodoValidez.Items.Add(Item)
                        End If
                    End If
                Next i

                'Los meses:
                iProx = 30
                For i = 1 To 6
                    Item = New Infragistics.Web.UI.ListControls.DropDownItem
                    Item.Value = iProx * i
                    If i = 1 Then
                        Item.Text = i & " " & Textos(3)
                    Else
                        Item.Text = i & " " & Textos(4)
                    End If

                    If FSNUser.QACertifModExpiracion Then
                        If wddProximidad.Items.FindItemByValue(Item.Value) Is Nothing Then
                            wddProximidad.Items.Add(Item)
                        End If
                    End If

                    Item = New Infragistics.Web.UI.ListControls.DropDownItem

                    Item.Value = iProx * i
                    If i = 1 Then
                        Item.Text = i & " " & Textos(3)
                    Else
                        Item.Text = i & " " & Textos(4)
                    End If

                    If FSNUser.QAModificarPeriodoValidezCertificados Then
                        If wddPeriodoValidez.Items.FindItemByValue(Item.Value) Is Nothing Then
                            wddPeriodoValidez.Items.Add(Item)
                        End If
                    End If
                Next i

                If FSNUser.QACertifModExpiracion Then
                    Me.wddProximidad.TextField = "DEN"
                    Me.wddProximidad.ValueField = "ID"

                    'Selecciona el correspondiente:
                    For Each Item In Me.wddProximidad.Items
                        If Item.Value = oParametros.QAProxExpirar Then
                            Me.wddProximidad.SelectedItemIndex = Item.Index
                            Exit For
                        End If
                    Next
                End If

                If FSNUser.QAModificarPeriodoValidezCertificados Then
                    Me.wddPeriodoValidez.TextField = "DEN"
                    Me.wddPeriodoValidez.ValueField = "ID"

                    'Selecciona el correspondiente:
                    For Each Item In Me.wddPeriodoValidez.Items
                        If Item.Value = oParametros.QAPeriodoValidezCertificado Then
                            Me.wddPeriodoValidez.SelectedItemIndex = Item.Index
                            Exit For
                        End If
                    Next
                End If
            End If


            If FSNUser.QAContacAutom = True Then
                Personas = FSNServer.Get_Object(GetType(FSNServer.Personas))

                Personas.LoadData(, , , , , , , , True)

                wddContacto.DataSource = Personas.Data
                Me.wddContacto.TextField = "NOMBRE"
                Me.wddContacto.ValueField = "COD"
                wddContacto.DataBind()

                Me.wddContacto.CurrentValue = ""
                Me.txtCodContacto.Value = ""
                For Each Item In Me.wddContacto.Items
                    If oParametros.ContactoAutomQA = Item.Value Then
                        Me.wddContacto.SelectedItemIndex = Item.Index
                        Me.txtCodContacto.Value = Item.Value
                        Exit For
                    End If
                Next

                Personas = Nothing
            Else
                Me.SeparaContacto.Visible = False
                Me.Contacto.Visible = False
            End If

            If Acceso.gbVarCal_Materiales Then
                Me.tblMatManualAutomatica.Visible = True
            Else
                Me.tblMatManualAutomatica.Visible = False
            End If

            Me.chkManual.Checked = FSNUser.QAPPManual
            Me.chkAutomatica.Checked = FSNUser.QAPPAuto

            Me.wddNiveles.TextField = "NIVEL"
            Me.wddNiveles.ValueField = "NIVEL"

            'Los niveles:
            For i = 1 To 5
                Item = New Infragistics.Web.UI.ListControls.DropDownItem
                Item.Value = i
                Item.Text = i
                If wddNiveles.Items.FindItemByValue(Item.Value) Is Nothing Then
                    wddNiveles.Items.Add(Item)
                End If
            Next i

            If Me.chkAutomatica.Checked Then
                Me.wddNiveles.Enabled = True
                For Each Item In Me.wddNiveles.Items
                    If FSNUser.QAPPNivel = Item.Value Then
                        Me.wddNiveles.SelectedItemIndex = Item.Index
                        Exit For
                    End If
                Next
            Else
                Me.wddNiveles.CurrentValue = ""
                Me.wddNiveles.Enabled = False
            End If

            If FSNUser.QAMantenimientoUNQA Then
                '************* Carga las denominaciones *************
                Dim oCelda As System.Web.UI.HtmlControls.HtmlTableCell
                Dim oFila As System.Web.UI.HtmlControls.HtmlTableRow
                Dim oText As System.Web.UI.WebControls.TextBox
                Dim oLabel As System.Web.UI.WebControls.Label
                Dim oUNQA As FSNServer.UnidadNeg
                oUNQA = FSNServer.Get_Object(GetType(FSNServer.UnidadNeg))

                oUNQA.PYME = FSNUser.Pyme
                oUNQA.CargarIDNodoRaiz()
                idNodoRaiz.Value = oUNQA.Id

                oUNQA.UnidadesNeg_CargarIdiomas()


                For i = 0 To oUNQA.IdiomasNodo.Tables(0).Rows.Count - 1

                    oFila = New System.Web.UI.HtmlControls.HtmlTableRow
                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                    oLabel = New System.Web.UI.WebControls.Label
                    oLabel.Text = oUNQA.IdiomasNodo.Tables(0).Rows(i).Item("DEN").ToString() & ":"
                    oLabel.CssClass = "parrafo"
                    oCelda.Controls.Add(oLabel)

                    oFila.Cells.Add(oCelda)

                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                    oText = New System.Web.UI.WebControls.TextBox
                    oText.ID = "txtDen_" & oUNQA.IdiomasNodo.Tables(0).Rows(i).Item("COD").ToString()
                    oText.Text = DBNullToStr(oUNQA.IdiomasNodo.Tables(0).Rows(i).Item("DEN_NODO").ToString())
                    oText.Width = Unit.Pixel(400)
                    oText.MaxLength = 200
                    oCelda.Controls.Add(oText)
                    oFila.Cells.Add(oCelda)

                    Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
                    oHid = New System.Web.UI.HtmlControls.HtmlInputHidden
                    oHid.ID = "txtDenE_" & oUNQA.IdiomasNodo.Tables(0).Rows(i).Item("COD").ToString()
                    oHid.Value = oUNQA.IdiomasNodo.Tables(0).Rows(i).Item("EXISTE").ToString()
                    oCelda = New System.Web.UI.HtmlControls.HtmlTableCell
                    oCelda.Controls.Add(oHid)
                    oFila.Cells.Add(oCelda)

                    Me.tblGrupoIdiomas.Rows.Add(oFila)
                Next
                oUNQA = Nothing
            Else
                Me.tblGrupoUNQA.Visible = False
            End If
        End If

        CargarMateriales()

        Dim ProvePotAVal As Boolean = FSNUser.QAModifProvCalidad 'Variable que se inicializa con el Parametro de Seguridad de QA "Permitir modificar el modo de traspaso de proveedores al panel de calidad"

        Me.tblProveedores.Visible = ProvePotAVal

        If ProvePotAVal Then
            Me.chkAdjProve.Checked = oParametros.PotAValAdjudicacion
            divAsigProve.Style("visibility") = IIf(Me.chkAdjProve.Checked, "visible", "hidden")
            chkAsigProve.Checked = oParametros.PotAValProvisionalSelProve()
            Me.chkPedDirGS.Checked = oParametros.PotAValPedDirGS
            Me.chkPedApEP.Checked = oParametros.PotAValPedEP
        End If

        Dim NotifUsu As Boolean = FSNUser.QAModifNotifMateriales 'Variable que se inicializa con el Parametro de Seguridad de QA "Permitir modificar los parametros generales de notificaciones por cambios en materiales de GS"
        Dim NotifCalif As Boolean = FSNUser.QAModifNotifCalif 'Variable que se inicializa con el Parametro de Seguridad de QA "Permitir modificar el parametro general de notificacion al proveedor por cambio de calificacion total"
        Dim NotifProximidad As Boolean = FSNUser.QAModifNotifProximidadNc 'Variable que se inicializa con el Parametro de Seguridad de QA "Permitir modificar los par�metros generales de notificaci�n de proximidad de fechas de fin de una no conformidad"

        Me.uwtParametros.GetTab(1).Visible = NotifUsu OrElse NotifCalif OrElse NotifProximidad

        Me.tblNotificacionesCalif.Visible = NotifCalif
        If NotifCalif Then
            Me.chkNotifCambioCalif.Checked = oParametros.NotifProveCambioCalif
            divTipoNotifCambioCalif.Style("display") = IIf(Me.chkNotifCambioCalif.Checked, "inline-block", "none")
            If oParametros.TipoNotifProveCambioCalif = TipoNotifCambioCalif.Mensual Then
                Me.rdbMensual.Checked = True
            Else
                Me.rdbEnCadaCambio.Checked = True
            End If

            If Not NotifUsu AndAlso Not NotifProximidad Then Me.SeparaNotificaciones.Visible = False
        End If

        Me.tblNotificacionesProximidadNc.Visible = NotifProximidad
        If NotifProximidad Then
            Me.chkNotifProximFinAccionesNc.Checked = oParametros.NotifProximFinAccionesNc
            Me.chkNotifProximFinResolucionNc.Checked = oParametros.NotifProximFinResolucionNc

            If oParametros.NotifProximFinAccionesNc OrElse oParametros.NotifProximFinResolucionNc Then
                Me.wddPeriodoNotifNc.Enabled = True
            Else
                Me.wddPeriodoNotifNc.Enabled = False
            End If

            'Los d�as:
            For i = 1 To 6
                Item = New Infragistics.Web.UI.ListControls.DropDownItem

                Item.Value = i
                If i = 1 Then
                    Item.Text = i & " " & Textos(1)
                Else
                    Item.Text = i & " " & Textos(2)
                End If

                If wddPeriodoNotifNc.Items.FindItemByValue(Item.Value) Is Nothing Then
                    wddPeriodoNotifNc.Items.Add(Item)
                End If
            Next i

            'Las semanas:
            iProx = 7
            For i = 1 To 3
                Item = New Infragistics.Web.UI.ListControls.DropDownItem

                Item.Value = iProx * i
                If i = 1 Then
                    Item.Text = i & " " & Textos(5)
                Else
                    Item.Text = i & " " & Textos(6)
                End If

                If wddPeriodoNotifNc.Items.FindItemByValue(Item.Value) Is Nothing Then
                    wddPeriodoNotifNc.Items.Add(Item)
                End If
            Next i

            'Los meses:
            iProx = 30
            For i = 1 To 6
                Item = New Infragistics.Web.UI.ListControls.DropDownItem

                Item.Value = iProx * i
                If i = 1 Then
                    Item.Text = i & " " & Textos(3)
                Else
                    Item.Text = i & " " & Textos(4)
                End If

                If wddPeriodoNotifNc.Items.FindItemByValue(Item.Value) Is Nothing Then
                    wddPeriodoNotifNc.Items.Add(Item)
                End If
            Next i

            Me.wddPeriodoNotifNc.TextField = "DEN"
            Me.wddPeriodoNotifNc.ValueField = "ID"

            'Selecciona el correspondiente:
            For Each Item In Me.wddPeriodoNotifNc.Items
                If Item.Value = oParametros.QAPeriodoNotificacionNc Then
                    Me.wddPeriodoNotifNc.SelectedItemIndex = Item.Index
                    Exit For
                End If
            Next

            If Not NotifUsu Then
                Me.SeparaNotifProximidadNc.Visible = False
            End If
        End If

        Me.tblNotificaciones.Visible = NotifUsu
        Me.uwtMateriales.Visible = NotifUsu
        If NotifUsu Then

            Me.chkAsigAVal.Checked = oParametros.NotifUsuAsigMatVal
            Me.chkAsigAPot.Checked = oParametros.NotifUsuAsigMatPot
            Me.chkDesAVal.Checked = oParametros.NotifUsuDesAsigMatVal
            Me.chkDesAPot.Checked = oParametros.NotifUsuDesAsigMatPot
            Me.chkAltaMat.Checked = oParametros.NotifUsuAltaMatGS

            '************* Carga el �rbol de materiales *************
            Dim oGruposMaterial As FSNServer.GruposMaterial
            oGruposMaterial = FSNServer.Get_Object(GetType(FSNServer.GruposMaterial))
            If FSNUser.Pyme > 0 Then
                Dim sGMN1 As String = ""
                sGMN1 = oGruposMaterial.DevolverGMN1_PYME(FSNUser.Pyme)
                oGruposMaterial.LoadData(sGMN1, , , , Idioma, , )
            Else
                oGruposMaterial.LoadData(, , , , Idioma, , )
            End If

            Me.uwtMateriales.ClearAll()

            Me.uwtMateriales.DataSource = oGruposMaterial.Data.Tables(0).DefaultView
            Me.uwtMateriales.Levels(0).LevelImage = "../_common/images/carpetaCommodityTxiki.gif"
            Me.uwtMateriales.Levels(0).RelationName = "REL_NIV1_NIV2"
            Me.uwtMateriales.Levels(0).ColumnName = "DEN_" + Idioma
            Me.uwtMateriales.Levels(0).LevelKeyField = "COD"
            Me.uwtMateriales.Levels(0).LevelCheckBoxes = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.True
            Me.uwtMateriales.Levels(0).CheckboxColumnName = "QA_NOTIF_USU"

            Me.uwtMateriales.Levels(1).LevelImage = "../_common/images/carpetaFamilia.gif"
            Me.uwtMateriales.Levels(1).RelationName = "REL_NIV2_NIV3"
            Me.uwtMateriales.Levels(1).ColumnName = "DEN_" + Idioma
            Me.uwtMateriales.Levels(1).LevelKeyField = "COD"
            Me.uwtMateriales.Levels(1).LevelCheckBoxes = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.True
            Me.uwtMateriales.Levels(1).CheckboxColumnName = "QA_NOTIF_USU"

            Me.uwtMateriales.Levels(2).LevelImage = "../_common/images/carpetaGrupo.gif"
            Me.uwtMateriales.Levels(2).RelationName = "REL_NIV3_NIV4"
            Me.uwtMateriales.Levels(2).ColumnName = "DEN_" + Idioma
            Me.uwtMateriales.Levels(2).LevelKeyField = "COD"
            Me.uwtMateriales.Levels(2).LevelCheckBoxes = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.True
            Me.uwtMateriales.Levels(2).CheckboxColumnName = "QA_NOTIF_USU"

            Me.uwtMateriales.Levels(3).LevelImage = "../_common/images/carpetaCerrada.gif"
            Me.uwtMateriales.Levels(3).ColumnName = "DEN_" + Idioma
            Me.uwtMateriales.Levels(3).LevelKeyField = "COD"
            Me.uwtMateriales.Levels(3).LevelCheckBoxes = Infragistics.WebUI.UltraWebNavigator.CheckBoxes.False

            Me.uwtMateriales.DataBind()
            Me.uwtMateriales.DataKeyOnClient = True

            oGruposMaterial = Nothing
        End If

        If FSNUser.QACertifModExpiracion = False And FSNUser.QAContacAutom = False And FSNUser.QAModifProvCalidad = False Then
            Me.lblTitulo.Visible = False
            If Acceso.gbVarCal_Materiales Then
                Me.SeparaManualAutomatica.Visible = False
            End If
        End If

        tblNivelesDefectoUNQAProveedor.Visible = FSNUser.QAPermisoModificarNivelesDefectoUNQA
        If FSNUser.QAPermisoModificarNivelesDefectoUNQA Then
            lblNivelesDefectoUNQAProve.Text = Textos(38)
            Dim sNivelesCheckeados As String = "," & oParametros.QANivelesDefectoUNQAProveedor & ","

            UNQADefecto.Value = oParametros.QANivelesDefectoUNQAProveedor

            For k As Integer = 0 To oParametros.QAMaximoNivelUNQA
                Item = New Infragistics.Web.UI.ListControls.DropDownItem
                Item.Value = k

                Item.Text = Textos(37) & k & IIf(k = 0, " (" & FSNUser.NombreGruposIdiomaUsu & ")", "")

                Item.Selected = sNivelesCheckeados.Contains("," & k & ",")

                With wddNivelesDefectoUNQAProveedor
                    If .CurrentValue = "" Then
                        .CurrentValue = Item.Text
                    Else
                        .CurrentValue = .CurrentValue & "," & Item.Text
                    End If

                    .Items.Add(Item)
                End With
            Next
        End If

        If FSNUser.QAPermisoModificarTipoFiltroMaterial Then
            DirectCast(Me.uwtParametros.GetTab(0).ContentPane.FindControl("lblPanelProve"), Label).Visible = True
            Me.wddTipoFiltroMat.Visible = True
            Me.cargarComboTipoFiltroMat()
            Me.wddTipoFiltroMat.SelectedValue = oParametros.FiltroMaterialQA
            Me.wddTipoFiltroMat.SelectedItemIndex = oParametros.FiltroMaterialQA
        End If
    End Sub

    Private Sub uwtMateriales_NodeBound(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebNavigator.WebTreeNodeEventArgs) Handles uwtMateriales.NodeBound
        Dim oNode As Infragistics.WebUI.UltraWebNavigator.Node
        If e.Node.Checked = True Then
            e.Node.Tag = Request("Material")
            'e.Node.Expanded = True
            If Not e.Node.Parent Is Nothing AndAlso Not e.Node.Parent.Checked AndAlso Not e.Node.Parent.Expanded Then
                oNode = e.Node.Parent
                While Not oNode Is Nothing
                    oNode.Expanded = True
                    oNode = oNode.Parent
                End While
            End If

        End If
    End Sub

    ''' <summary>
    ''' Guarda los parametros de PARGEN_QA
    ''' </summary>
    ''' <param name="sender">Propios del evento</param>
    ''' <param name="e">propios del evento</param>       
    ''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1</remarks>
    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        oParametros = FSNServer.Get_Object(GetType(FSNServer.Parametros))
        oParametros.LoadData()

        If Acceso.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso And FSNUser.AccesoQA Then
            If FSNUser.QACertifModExpiracion Then
                If wddProximidad.SelectedItemIndex < 6 Then oParametros.QAProxExpirar = (wddProximidad.SelectedItemIndex + 1)
                If wddProximidad.SelectedItemIndex >= 6 And wddProximidad.SelectedItemIndex < 9 Then oParametros.QAProxExpirar = (wddProximidad.SelectedItemIndex - 5) * 7
                If wddProximidad.SelectedItemIndex >= 9 Then oParametros.QAProxExpirar = (wddProximidad.SelectedItemIndex - 8) * 30
            End If

            If FSNUser.QAModificarPeriodoValidezCertificados Then
                If wddPeriodoValidez.SelectedItemIndex <= 7 Then oParametros.QAPeriodoValidezCertificado = wddPeriodoValidez.SelectedItemIndex
                If wddPeriodoValidez.SelectedItemIndex > 7 And wddPeriodoValidez.SelectedItemIndex < 10 Then oParametros.QAPeriodoValidezCertificado = (wddPeriodoValidez.SelectedItemIndex - 6) * 7
                If wddPeriodoValidez.SelectedItemIndex >= 10 Then oParametros.QAPeriodoValidezCertificado = (wddPeriodoValidez.SelectedItemIndex - 9) * 30
            End If

            If FSNUser.QAContacAutom Then oParametros.ContactoAutomQA = Me.txtCodContacto.Value
        End If

        Dim ProvePotAVal As Boolean = FSNUser.QAModifProvCalidad 'Variable que se inicializa con el Parametro de Seguridad de QA "Permitir modificar el modo de traspaso de proveedores al panel de calidad"
        If ProvePotAVal Then
            oParametros.PotAValAdjudicacion = Me.chkAdjProve.Checked
            oParametros.PotAValProvisionalSelProve = chkAsigProve.Checked
            oParametros.PotAValPedDirGS = Me.chkPedDirGS.Checked
            oParametros.PotAValPedEP = Me.chkPedApEP.Checked
        End If

        'Restricci�n de la conversi�n autom�tica a determinados materiales de QA
        ActualizarMatQAProvesPotAVal()

        Dim NotifCalif As Boolean = FSNUser.QAModifNotifCalif 'Variable que se inicializa con el Parametro de Seguridad de QA "Permitir modificar el parametro general de notificacion al proveedor por cambio de calificacion total"
        If NotifCalif Then
            oParametros.NotifProveCambioCalif = Me.chkNotifCambioCalif.Checked
            oParametros.TipoNotifProveCambioCalif = If(Me.rdbMensual.Checked, TipoNotifCambioCalif.Mensual, TipoNotifCambioCalif.EnCadaCambio)
        End If

        Dim NotifUsu As Boolean = FSNUser.QAModifNotifMateriales 'Variable que se inicializa con el Parametro de Seguridad de QA "Permitir modificar los parametros generales de notificaciones por cambios en materiales de GS"
        If NotifUsu Then
            oParametros.NotifUsuAsigMatVal = Me.chkAsigAVal.Checked
            oParametros.NotifUsuAsigMatPot = Me.chkAsigAPot.Checked
            oParametros.NotifUsuDesAsigMatVal = Me.chkDesAVal.Checked
            oParametros.NotifUsuDesAsigMatPot = Me.chkDesAPot.Checked
            oParametros.NotifUsuAltaMatGS = Me.chkAltaMat.Checked

            If Me.chkAltaMat.Checked Then
                '************* Guardar el �rbol de materiales *************
                Dim ds As DataSet
                Dim GMNSs As FSNServer.GruposMaterial

                ds = GenerarDataSetModifGMN(Request)

                GMNSs = FSNServer.Get_Object(GetType(FSNServer.GruposMaterial))
                GMNSs.ModificarNotificacionUsuariosQA(ds)
            End If
        End If

        Dim NotifProximidad As Boolean = FSNUser.QAModifNotifProximidadNc 'Variable que se inicializa con el Parametro de Seguridad de QA "Permitir modificar los par�metros generales de notificaci�n de proximidad de fechas de fin de una no conformidad"
        If NotifProximidad Then
            oParametros.NotifProximFinAccionesNc = Me.chkNotifProximFinAccionesNc.Checked
            oParametros.NotifProximFinResolucionNc = Me.chkNotifProximFinResolucionNc.Checked

            If wddPeriodoNotifNc.SelectedItemIndex < 6 Then oParametros.QAPeriodoNotificacionNc = (wddPeriodoNotifNc.SelectedItemIndex + 1)
            If wddPeriodoNotifNc.SelectedItemIndex >= 6 And wddPeriodoNotifNc.SelectedItemIndex < 9 Then oParametros.QAPeriodoNotificacionNc = (wddPeriodoNotifNc.SelectedItemIndex - 5) * 7
            If wddPeriodoNotifNc.SelectedItemIndex >= 9 Then oParametros.QAPeriodoNotificacionNc = (wddPeriodoNotifNc.SelectedItemIndex - 8) * 30
        End If

        If FSNUser.QAMantenimientoUNQA Then
            Dim dsDen As DataSet
            Dim oUNQA As FSNServer.UnidadNeg
            oUNQA = FSNServer.Get_Object(GetType(FSNServer.UnidadNeg))
            oUNQA.Id = idNodoRaiz.Value
            dsDen = GenerarDataSetIdiomasUNQA(Request)
            oUNQA.ModificarNodo(dsDen, True)
        End If

        If FSNUser.QAPermisoModificarNivelesDefectoUNQA Then oParametros.QANivelesDefectoUNQAProveedor = UNQADefecto.Value

        If oParametros.FiltroMaterialQA <> Me.wddTipoFiltroMat.SelectedItemIndex AndAlso FSNUser.QAPermisoModificarTipoFiltroMaterial Then
            If Me.wddTipoFiltroMat.SelectedItemIndex <> -1 Then
                oParametros.FiltroMaterialQA = Me.wddTipoFiltroMat.SelectedItemIndex
            End If
            'adem�s de actualizar el valor eliminamos de la sesi�n los materiales seleccionados
            HttpContext.Current.Session("materialesseleccionados") = Nothing
        End If

        '************* Guardar parametros Generales *************
        oParametros.GuardarOpciones()

        FSNUser.Actualizar_PPManualAuto(Me.chkManual.Checked, Me.chkAutomatica.Checked, wddNiveles.SelectedItemIndex + 1)

        If Acceso.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso And FSNUser.AccesoQA Then
            'Check Box de notificaciones de errores del calculo de puntuaciones
            If FSNUser.AccesoQA And FSNUser.QAPuntuacionProveedores Then
                FSNUser.Actualizar_QANotifErrorCalculoPuntuaciones(CType(Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkRecibirNotificacionesCalcPunt"), CheckBox).Checked)
            End If
        End If

        FSNUser.QAPPManual = Me.chkManual.Checked
        FSNUser.QAPPAuto = Me.chkAutomatica.Checked
        FSNUser.QAPPNivel = wddNiveles.SelectedItemIndex + 1

        FSNUser.QANotificarErrorCalculoPuntuaciones = CType(Me.uwtParametros.GetTab(0).ContentPane.FindControl("chkRecibirNotificacionesCalcPunt"), CheckBox).Checked
        Session("FSN_User") = FSNUser

        oParametros = Nothing

        Response.Redirect(Request.RawUrl)
    End Sub
    ''' <summary>Comprueba si ha habido modificaciones en los materiales de QA para la conversi�n autom�tica del proveedor</summary>
    Private Sub ActualizarMatQAProvesPotAVal()
        Dim dsMateriales As DataSet = Materiales()

        Dim bCambio As Boolean = False
        If Not dsMateriales Is Nothing Then
            Dim dvMateriales As New DataView(dsMateriales.Tables(0), String.Empty, "ID", DataViewRowState.CurrentRows)
            For i As Integer = 0 To wddMateriales.Items.Count - 1

                If wddMateriales.Items(i).Selected <> IIf(dvMateriales.FindRows(wddMateriales.Items(i).Value)(0)("POT_A_VAL") = 1, True, False) Then
                    bCambio = True
                    dvMateriales.FindRows(wddMateriales.Items(i).Value)(0)("POT_A_VAL") = IIf(wddMateriales.Items(i).Selected, 1, 0)
                End If
            Next
        Else
            bCambio = True
        End If

        If bCambio Then
            Dim oMaterialesQA As FSNServer.MaterialesQA
            oMaterialesQA = Me.FSNServer.Get_Object(GetType(FSNServer.MaterialesQA))
            oMaterialesQA.ActualizarProvesPotencialesAValidos(dsMateriales)
            Me.InsertarEnCache("Materiales" & FSNUser.Cod, dsMateriales)
        End If
    End Sub

    ''' <summary>Carga la combo con los materiales QA</summary>
    ''' <remarks>Llamada desde:Page_PreRender</remarks>
    Private Sub CargarMateriales()
        With wddMateriales
            .TextField = "DEN_" & Me.Idioma
            .ValueField = "ID"
            .DataSource = Materiales()
            .DataBind()
        End With
    End Sub

    Private Function Materiales() As DataSet
        Dim dsMateriales As DataSet
        If Me.IsPostBack Then dsMateriales = CType(Cache("Materiales" & FSNUser.Cod), DataSet)
        If dsMateriales Is Nothing Then
            Dim oMaterialesQA As FSNServer.MaterialesQA
            oMaterialesQA = Me.FSNServer.Get_Object(GetType(FSNServer.MaterialesQA))
            dsMateriales = oMaterialesQA.GetData(Me.Idioma, , False, Me.FSNUser.Pyme, Me.FSNUser.QARestProvMaterial, Me.FSNUser.CodPersona)
            Me.InsertarEnCache("Materiales" & FSNUser.Cod, dsMateriales)
        End If

        Return dsMateriales
    End Function

    Private Function GenerarDataSetModifGMN(ByVal Request As System.Web.HttpRequest) As DataSet
        Dim oItem As System.Collections.Specialized.NameValueCollection
        Dim loop1 As Integer
        Dim arr1() As String
        Dim DS As DataSet
        Dim dtGMN1 As DataTable
        Dim dtGMN2 As DataTable
        Dim dtGMN3 As DataTable
        Dim keysGMN1(0) As DataColumn
        Dim findGMN1(0) As Object
        Dim keysGMN2(1) As DataColumn
        Dim findGMN2(1) As Object
        Dim keysGMN3(2) As DataColumn
        Dim findGMN3(2) As Object
        Dim FSPMServer As FSNServer.Root = Session("FSN_Server")
        Dim sRequest As String
        Dim oRequest As Object
        Dim dtNewRow As DataRow
        Dim bInsertar As Boolean

        DS = New DataSet

        'Crea la tabla temporal para los gmn1 del GS modificados:
        dtGMN1 = DS.Tables.Add("TEMP_GMN1")
        dtGMN1.Columns.Add("COD", System.Type.GetType("System.String"))
        dtGMN1.Columns.Add("QA_NOTIF_USU", System.Type.GetType("System.Int32"))
        keysGMN1(0) = dtGMN1.Columns("COD")
        dtGMN1.PrimaryKey = keysGMN1

        'Crea la tabla temporal para los gmn2 del GS modificados:
        dtGMN2 = DS.Tables.Add("TEMP_GMN2")
        dtGMN2.Columns.Add("GMN1", System.Type.GetType("System.String"))
        dtGMN2.Columns.Add("COD", System.Type.GetType("System.String"))
        dtGMN2.Columns.Add("QA_NOTIF_USU", System.Type.GetType("System.Int32"))
        keysGMN2(0) = dtGMN2.Columns("GMN1")
        keysGMN2(1) = dtGMN2.Columns("COD")
        dtGMN2.PrimaryKey = keysGMN2

        'Crea la tabla temporal para los gmn3 del GS modificados:
        dtGMN3 = DS.Tables.Add("TEMP_GMN3")
        dtGMN3.Columns.Add("GMN1", System.Type.GetType("System.String"))
        dtGMN3.Columns.Add("GMN2", System.Type.GetType("System.String"))
        dtGMN3.Columns.Add("COD", System.Type.GetType("System.String"))
        dtGMN3.Columns.Add("QA_NOTIF_USU", System.Type.GetType("System.Int32"))
        keysGMN3(0) = dtGMN3.Columns("GMN1")
        keysGMN3(1) = dtGMN3.Columns("GMN2")
        keysGMN3(2) = dtGMN3.Columns("COD")
        dtGMN3.PrimaryKey = keysGMN3

        oItem = Request.Form
        arr1 = oItem.AllKeys
        For loop1 = 0 To arr1.GetUpperBound(0)
            sRequest = arr1(loop1).ToString

            oRequest = sRequest.Split("_")

            bInsertar = False

            Select Case oRequest(0)

                Case "GMN1"
                    findGMN1(0) = Split(oRequest(1), "&&&")(0)

                    dtNewRow = dtGMN1.Rows.Find(findGMN1)
                    If dtNewRow Is Nothing Then
                        dtNewRow = dtGMN1.NewRow
                    End If

                    dtNewRow.Item("COD") = Split(oRequest(1), "&&&")(0)

                    If dtNewRow.RowState = DataRowState.Detached Then
                        dtGMN1.Rows.Add(dtNewRow)
                    End If
                    'Cambio el estado a modified:
                    dtNewRow.AcceptChanges()
                    dtNewRow.Item("QA_NOTIF_USU") = CInt(Split(oRequest(1), "&&&")(1))

                Case "GMN2"
                    findGMN2(0) = Split(oRequest(1), "&&&")(0)
                    findGMN2(1) = Split(oRequest(1), "&&&")(1)

                    dtNewRow = dtGMN2.Rows.Find(findGMN2)
                    If dtNewRow Is Nothing Then
                        dtNewRow = dtGMN2.NewRow
                    End If

                    dtNewRow.Item("GMN1") = Split(oRequest(1), "&&&")(0)
                    dtNewRow.Item("COD") = Split(oRequest(1), "&&&")(1)

                    If dtNewRow.RowState = DataRowState.Detached Then
                        dtGMN2.Rows.Add(dtNewRow)
                    End If
                    'Cambio el estado a modified:
                    dtNewRow.AcceptChanges()
                    dtNewRow.Item("QA_NOTIF_USU") = CInt(Split(oRequest(1), "&&&")(2))

                Case "GMN3"
                    findGMN3(0) = Split(oRequest(1), "&&&")(0)
                    findGMN3(1) = Split(oRequest(1), "&&&")(1)
                    findGMN3(2) = Split(oRequest(1), "&&&")(2)

                    dtNewRow = dtGMN3.Rows.Find(findGMN3)
                    If dtNewRow Is Nothing Then
                        dtNewRow = dtGMN3.NewRow
                    End If

                    dtNewRow.Item("GMN1") = Split(oRequest(1), "&&&")(0)
                    dtNewRow.Item("GMN2") = Split(oRequest(1), "&&&")(1)
                    dtNewRow.Item("COD") = Split(oRequest(1), "&&&")(2)

                    If dtNewRow.RowState = DataRowState.Detached Then
                        dtGMN3.Rows.Add(dtNewRow)
                    End If
                    'Cambio el estado a modified:
                    dtNewRow.AcceptChanges()
                    dtNewRow.Item("QA_NOTIF_USU") = CInt(Split(oRequest(1), "&&&")(3))

            End Select
        Next loop1

        oItem = Nothing

        Return DS
    End Function

    ''' <summary>
    ''' Cargar un Dataset con los textos en los diferentes idiomas para la unidad de negocio de nivel 0
    ''' </summary>
    ''' <param name="Request">HttpRequest con los campos</param>        
    ''' <returns>Dataset con los textos en los diferentes idiomas para la unidad de negocio de nivel 0</returns>
    ''' <remarks>Llamada desde: cmdAceptar_Click ; Tiempo m�ximo:0</remarks>
    Private Function GenerarDataSetIdiomasUNQA(ByVal Request As System.Web.HttpRequest) As DataSet
        Dim oItem As System.Collections.Specialized.NameValueCollection
        Dim loop1 As Integer
        Dim arr1() As String
        Dim DS As DataSet
        Dim dtUNQA As DataTable
        Dim sRequest As String
        Dim oRequest As Object
        Dim oRequest2 As Object
        Dim dtNewRow As DataRow
        Dim find(0) As Object
        Dim keys(0) As DataColumn

        Dim sCod, sDen As String
        DS = New DataSet

        'Crea la tabla temporal para los idiomas del nodo Raiz UNQA
        dtUNQA = DS.Tables.Add("IDIOMAS")
        dtUNQA.Columns.Add("COD", System.Type.GetType("System.String"))
        dtUNQA.Columns.Add("DEN", System.Type.GetType("System.String"))
        dtUNQA.Columns.Add("EXISTE", System.Type.GetType("System.String"))
        keys(0) = dtUNQA.Columns("COD")
        dtUNQA.PrimaryKey = keys

        oItem = Request.Form
        arr1 = oItem.AllKeys
        For loop1 = 0 To arr1.GetUpperBound(0)
            sRequest = arr1(loop1).ToString

            oRequest = sRequest.Split("$")

            Select Case oRequest(0)

                Case "uwtParametros"
                    oRequest2 = sRequest.Split("$")
                    If UBound(oRequest2) > 1 Then
                        oRequest2 = oRequest2(2).split("_")
                        Select Case oRequest2(0)
                            Case "txtDen"
                                sCod = oRequest2(1)
                                sDen = Request(sRequest)

                                find(0) = sCod
                                dtNewRow = dtUNQA.Rows.Find(find)
                                If dtNewRow Is Nothing Then
                                    dtNewRow = dtUNQA.NewRow
                                End If

                                dtNewRow.Item("COD") = sCod
                                dtNewRow.Item("DEN") = sDen

                                If dtNewRow.RowState = DataRowState.Detached Then
                                    dtUNQA.Rows.Add(dtNewRow)
                                End If
                            Case "txtDenE"
                                sCod = oRequest2(1)
                                find(0) = sCod
                                dtNewRow = dtUNQA.Rows.Find(find)
                                If dtNewRow Is Nothing Then
                                    dtNewRow = dtUNQA.NewRow

                                End If
                                dtNewRow.Item("EXISTE") = Request(sRequest)
                                If dtNewRow.RowState = DataRowState.Detached Then
                                    dtUNQA.Rows.Add(dtNewRow)
                                End If
                        End Select
                    End If

            End Select
        Next loop1

        oItem = Nothing

        Return DS
    End Function

    Private Sub cargarComboTipoFiltroMat()
        Dim oItem As DropDownItem
        oItem = New DropDownItem()
        oItem.Text = Me.Textos(42)
        oItem.Value = FiltroMaterial.FiltroMatQA
        wddTipoFiltroMat.Items.Add(oItem)
        oItem = New DropDownItem()
        oItem.Text = Me.Textos(43)
        oItem.Value = FiltroMaterial.FiltroMatGS
        wddTipoFiltroMat.Items.Add(oItem)
        oItem = New DropDownItem()
        oItem.Text = Me.Textos(44)
        oItem.Value = FiltroMaterial.FiltroMatQAGS
        wddTipoFiltroMat.Items.Add(oItem)
    End Sub

    Private Sub wddMateriales_ItemBound(sender As Object, e As DropDownItemBoundEventArgs) Handles wddMateriales.ItemBound
        e.Value.Selected = (e.Value.DataItem.Row("POT_A_VAL") = 1)
    End Sub

End Class

