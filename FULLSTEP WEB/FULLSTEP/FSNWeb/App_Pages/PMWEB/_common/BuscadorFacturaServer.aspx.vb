﻿Public Class BuscadorFacturaServer
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim oFactura As FSNServer.Factura
        Dim lId As Long
        Dim sNumFactura As String
        Dim sCodProveedor As String
        Dim fsEntry As String

        sNumFactura = Request("id")
        sCodProveedor = Request("CodProveedor")
        fsEntry = Request("fsEntry")
        
        If sNumFactura <> Nothing Then
            oFactura = FSNServer.Get_Object(GetType(FSNServer.Factura))
            lId = oFactura.BuscarFactura(sNumFactura, sCodProveedor)

            If lId > 0 Then
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>ponerCorrecto('" & fsEntry & "','" & sNumFactura & "'," & lId & ")</script>")
            Else
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>borrarValorFactura('" & fsEntry & "', true)</script>")
            End If
        Else
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>borrarValorFactura('" & fsEntry & "', false)</script>")
        End If
    End Sub

End Class