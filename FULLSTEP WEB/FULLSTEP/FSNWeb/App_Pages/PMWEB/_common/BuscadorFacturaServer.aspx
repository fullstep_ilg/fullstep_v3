﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuscadorFacturaServer.aspx.vb" Inherits="Fullstep.FSNWeb.BuscadorFacturaServer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    
</head>
<body>
    <script type="text/javascript">
        var sinColor = '<%=Request("sinColor") %>'
        function ponerCorrecto(idFSEntry, sNumFactura, lId) {
            var oEntry;
            p = window.parent.window.document;

            idFSEntry = idFSEntry.replace("__t", "");
            e = p.getElementById(idFSEntry + "__tbl")
            if (e)
                oEntry = e.Object

            if (oEntry) {
                oEntry.setDataValue(sNumFactura);
                if (sinColor == '1')
                    oEntry.Editor.elem.className = "TipoTextoMedio"
                else
                    oEntry.Editor.elem.className = "TipoTextoMedioVerde"
            }

            window.close();
        }

        function borrarValorFactura(idFSEntry, bEsIncorrecto) {
            var oEntry;
            p = window.parent.window.document;

            idFSEntry = idFSEntry.replace("__t", "");
            e = p.getElementById(idFSEntry + "__tbl")
            if (e)
                oEntry = e.Object

            if (oEntry) {
                oEntry.setDataValue('');
                if (bEsIncorrecto)
                    oEntry.Editor.elem.className = "TipoTextoMedioRojo"
                else
                    oEntry.Editor.elem.className = "TipoTextoMedio"
            }

            window.close();
        }
    </script>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
