﻿Public Partial Class AbandonarSesion
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Desconectar()
    End Sub


    ''' <summary>
    ''' Cierra la sesión y redirige a la página de login.
    ''' </summary>
    ''' <remarks>Llamadas desde: lnkbtnCerrarSesion_Click</remarks>
    Public Sub Desconectar()
        Dim bHayAutenticacionWindows, bHayAutenticacionLCX As Boolean

        bHayAutenticacionLCX = Session("HayAutenticacionLCX")
        bHayAutenticacionWindows = Session("HayAutenticacionWindows")
        Session.Abandon()
        Session.Clear()

        If bHayAutenticacionWindows OrElse bHayAutenticacionLCX Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "cerrar", "<script>cerrar();</script>")
        Else
            Response.Redirect(ConfigurationManager.AppSettings("ruta") & "Default.aspx")
        End If
    End Sub
End Class