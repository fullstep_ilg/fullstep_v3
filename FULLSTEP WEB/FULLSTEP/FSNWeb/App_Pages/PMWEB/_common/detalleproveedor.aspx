<%@ Page Language="vb" AutoEventWireup="false" Codebehind="detalleproveedor.aspx.vb" Inherits="Fullstep.FSNWeb.detalleproveedor"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblPer" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" cellSpacing="1"
				cellPadding="1" width="100%" border="0" height="100%">
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px">
						<asp:Label id="lblCod" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist">
						<asp:Label id="lblCodBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px">
						<asp:Label id="lblDen" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist">
						<asp:Label id="lblDenBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px">
						<asp:Label id="lblNIF" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist">
						<asp:Label id="lblNIFBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px">
						<asp:Label id="lblDir" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist">
						<asp:Label id="lblDirBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px">
						<asp:Label id="lblCP" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist">
						<asp:Label id="lblCPBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px">
						<asp:Label id="lblPob" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist">
						<asp:Label id="lblPobBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px">
						<asp:Label id="lblPais" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist">
						<asp:Label id="lblPaisBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px">
						<asp:Label id="lblProvincia" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist">
						<asp:Label id="lblProvinciaBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px">
						<asp:Label id="lblMon" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist">
						<asp:Label id="lblMonBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD align="center" colSpan="2"><INPUT class="botonPMWEB" id="cmdCerrar" onclick="window.close()" type="button" value="DCerrar"
							name="cmdCerrar" runat="server"></TD>
					<TD></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</html>
