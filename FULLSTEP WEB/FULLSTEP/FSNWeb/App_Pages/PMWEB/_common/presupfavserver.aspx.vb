
Public Class presupfavserver
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents uwgPresupFav As Infragistics.WebUI.UltraWebGrid.UltraWebGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oPresupsFavoritos As FSNServer.PresupuestosFavoritos
        oPresupsFavoritos = FSNServer.Get_Object(GetType(FSNServer.PresupuestosFavoritos))
        oPresupsFavoritos.LoadData(FSNUser.Cod, Request("tipo"), Request("orgcom").ToString())

        Dim oGrid As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
        oGrid = Me.uwgPresupFav

        oGrid.DataSource = oPresupsFavoritos.Data
        oGrid.DataBind()
        If oGrid.Bands.Count = 0 Then
            oGrid.Bands.Add(New Infragistics.WebUI.UltraWebGrid.UltraGridBand)
        End If
        Me.uwgPresupFav.Columns.FromKey("FAVID").Hidden = True
        Me.uwgPresupFav.Columns.FromKey("NIVEL").Hidden = True
        Me.uwgPresupFav.Columns.FromKey("PRESID").Hidden = True
        Me.uwgPresupFav.Columns.FromKey("PRES1").Hidden = True
        Me.uwgPresupFav.Columns.FromKey("PRES2").Hidden = True
        Me.uwgPresupFav.Columns.FromKey("PRES3").Hidden = True
        Me.uwgPresupFav.Columns.FromKey("PRES4").Hidden = True
        Me.uwgPresupFav.Columns.FromKey("UON1").Hidden = True
        Me.uwgPresupFav.Columns.FromKey("UON2").Hidden = True
        Me.uwgPresupFav.Columns.FromKey("UON3").Hidden = True
        Me.uwgPresupFav.Columns.FromKey("DEN").Hidden = True
        Me.uwgPresupFav.Columns.FromKey("UONDEN").Hidden = True
        If Me.uwgPresupFav.Columns.Exists("ANYO") Then Me.uwgPresupFav.Columns.FromKey("ANYO").Hidden = True
    End Sub


    Private Sub uwgPresupFav_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgPresupFav.InitializeRow
        Dim sTexto As String = Nothing
        If Request("tIPO") = 1 OrElse Request("Tipo") = 2 Then
            sTexto = e.Row.Cells.FromKey("ANYO").Value & " - "
        End If
        sTexto = sTexto & e.Row.Cells.FromKey("PRES1").Value
        If e.Row.Cells.FromKey("NIVEL").Value > 1 Then
            sTexto = sTexto & " - " & e.Row.Cells.FromKey("PRES2").Value
        End If
        If e.Row.Cells.FromKey("NIVEL").Value > 2 Then
            sTexto = sTexto & " - " & e.Row.Cells.FromKey("PRES3").Value
        End If
        If e.Row.Cells.FromKey("NIVEL").Value > 3 Then
            sTexto = sTexto & " - " & e.Row.Cells.FromKey("PRES4").Value
        End If
        sTexto = sTexto & " (100,00%); "

        Dim sValor As String = Nothing
        sValor = e.Row.Cells.FromKey("NIVEL").Value & "_" & e.Row.Cells.FromKey("PRESID").Value & "_1"

        e.Row.Cells.FromKey("PRESUPDEN").Value = "<a class='aPMWeb' onclick=""SeleccionarPresupuesto('" & sTexto & "','" & sValor & "');return false;"">" & e.Row.Cells.FromKey("PRESUPDEN").Value & "</a>"
    End Sub
End Class

