<%@ Control Language="vb" AutoEventWireup="false" Codebehind="menu.ascx.vb" Inherits="Fullstep.FSNWeb.MenuControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
    <script type="text/javascript">


        function mostrarsubmenu(nombre) {
            var msubmenu = document.getElementById(nombre);
            if (msubmenu) {
                msubmenu.style.display = 'block';
                msubmenu.style.visibility = 'visible';
            }
        }

        function ocultarsubmenu(nombre) {
            var msubmenu = document.getElementById(nombre);
            if (msubmenu) {
                msubmenu.style.display = 'none';
                msubmenu.style.visibility = 'hidden';
            }
        }
</script>
<div id="Cabecera" style="text-align: right; min-width: 945px; min-height: 30px; padding-bottom: 20px; width: 100%;" class="Cabecera">
    <asp:Panel ID="panMenuCabecera" runat="server" Style="float:right; padding-right: 20px; padding-top: 5px">
        <div style="float:left; line-height:15px; padding:0px 5px; border-right:solid 1px White;">
            <asp:LinkButton ID="lnkbtnPoliticaCookies" runat="server" CssClass="Cabecera Texto10" PostBackUrl="~/App_Pages/politicacookies.aspx">Politica de contraseņas</asp:LinkButton>
        </div>
        <div style="float:left; line-height:15px; padding:0px 5px; border-right:solid 1px White;">
            <asp:LinkButton ID="lnkbtnPreferencias" runat="server" CssClass="Cabecera Texto10"></asp:LinkButton>
        </div>
        <asp:Panel runat="server" ID="pnlCambioPassword"  style="float:left; line-height:15px; padding:0px 5px; border-right:solid 1px White;">
            <asp:LinkButton ID="lnkbtnSeguridad" runat="server" CssClass="Cabecera Texto10"></asp:LinkButton>
        </asp:Panel>                            
        <div id="lnkNuevoMensajeMaster" style="display:none; cursor:pointer; position:relative; float:left; padding:0px 5px; border-right:solid 1px White;">
			<asp:Image runat="server" ID="Image3" CssClass="Image16" ImageUrl="~/App_Pages/PMWEB/images/ColaboracionHeader.png" Style="vertical-align: middle;" />
		</div>
        <div id="lnkNotificacionesCN" style="display:none; cursor:pointer; position:relative; float:left; padding-left:5px; padding-right:10px; border-right:solid 1px White;">
			<asp:Image runat="server" ID="Image4" ImageUrl="~/App_Pages/PMWEB/images/alertNotif.png" Style="vertical-align: middle; margin-right: 5px;" />
			<div id="AlertNotificacionCN" class="fondoAlerta" style="position:absolute; text-align:center; vertical-align:middle; width:15px; height:15px; top:5px; right:5px;">
				<span id="NumeroAlertNotificacionCN" class="Texto12 TextoClaro Negrita" style="line-height:15px;">0</span>
			</div>
		</div>
        <div style="float:left; line-height:15px; padding:0px 5px;">
			<asp:LinkButton ID="lnkbtnInicio" runat="server" CssClass="Cabecera Texto12"></asp:LinkButton>
        </div>							
		<asp:Panel runat="server" ID="pnlCerrarSesion" style="float:left; line-height:15px; padding:0px 5px; border-left:solid 1px White;">
            <asp:LinkButton ID="lnkbtnCerrarSesion" runat="server" CssClass="Cabecera Texto12"></asp:LinkButton>
		</asp:Panel>
        <asp:LinkButton ID="lnkbtnAyuda" runat="server" CssClass="Cabecera Texto12"></asp:LinkButton>                        
    </asp:Panel>
</div>
<div id="Pestanias" style="position:relative; text-align:right;">
    <div class="Cabecera Texto12" style="display:inline-block; width:100%; min-height:35px;">
        <fsn:FSNTab ID="fsnTabSeguridad" runat="server" Height="28px" OnClientClick="return false;" class="tabMenu" />
        <fsn:FSNTab ID="fsnTabColaboracion" runat="server" Height="28px"  class="tabMenu"  />
        <asp:Panel ID="panMenu" runat="server">
            <fsn:FSNTab ID="fsnTabInformes" runat="server" Height="28px" class="tabMenu" />
            <fsn:FSNTab ID="fsnTabIntegracion" runat="server" Height="28px" class="tabMenu"  />
            <fsn:FSNTab ID="fsnTabFacturacion" runat="server" Height="28px"  OnClientClick="return false" class="tabMenu"  />
            <fsn:FSNTab ID="fsnTabCtrlPresup" runat="server" Height="28px" OnClientClick="return false" class="tabMenu"  />
            <fsn:FSNTab ID="fsnTabCalidad" runat="server" Height="28px" OnClientClick="return false" class="tabMenu"  />
            <fsn:FSNTab ID="fsnTabCatalogo" runat="server" Height="28px" OnClientClick="return false" class="tabMenu"  />
            <fsn:FSNTab ID="fsnTabContratos" runat="server" Height="28px" OnClientClick="return false" class="tabMenu"  />
            <fsn:FSNTab ID="fsnTabProcesos" runat="server" Height="28px" OnClientClick="return false" class="tabMenu"  />
        </asp:Panel>
        <asp:Panel ID="panTabsProcesos" runat="server" Visible="false">
            <fsn:FSNTab ID="fsnTabProcParametros" runat="server" Height="28px" OnClientClick="ParametrosPM('Parametros'); return false" />
            <fsn:FSNTab ID="fsnTabProcSeguimiento" runat="server" Height="28px" />
            <fsn:FSNTab ID="fsnTabProcAlta" runat="server" Height="28px" />
        </asp:Panel>
        <asp:Panel ID="panTabsContratos" runat="server" Visible="false">
            <fsn:FSNTab ID="fsnTabContrSeguimiento" runat="server" Height="28px" />
            <fsn:FSNTab ID="fsnTabContrAlta" runat="server" Height="28px" />
        </asp:Panel>
        <asp:Panel ID="panTabsCalidad" runat="server" Visible="false">
            <fsn:FSNTab ID="fsnTabCalidadConfiguracion" runat="server" Height="28px" OnClientClick="return false" />                                    
            <fsn:FSNTab ID="fsnTabCalNoConformidades" runat="server" Height="28px" />
            <fsn:FSNTab ID="fsnTabCalCertificados" runat="server" Height="28px" />
            <fsn:FSNTab ID="fsnTabCalEncuestas" runat="server" Height="28px" OnClientClick="return false" /> 
            <fsn:FSNTab ID="fsnTabCalSolicitudesQA" runat="server" Height="28px" OnClientClick="return false" /> 
            <fsn:FSNTab ID="fsnTabCalPanelProveedores" runat="server" Height="28px" />            
            <fsn:FSNTab ID="fsnTabCalPanelEscalacionProveedores" runat="server" Height="28px" OnClientClick="TempCargando()" />
        </asp:Panel>
        <asp:Panel ID="panTabsCatalogo" runat="server" Visible="false">
            <fsn:FSNTab ID="fsnTabCatParametros" runat="server" Height="28px" OnClientClick="ParametrosEP('Parametros'); return false" />
            <fsn:FSNTab ID="fsnTabCatAprobacion" runat="server" Height="28px" />
            <fsn:FSNTab ID="fsnTabCatRecepcion" runat="server" Height="28px" />
            <fsn:FSNTab ID="fsnTabCatSeguimiento" runat="server" Height="28px" />
            <fsn:FSNTab ID="fsnTabCatEmision" runat="server" Height="28px" />
        </asp:Panel>
        <asp:Panel ID="panTabsCtrlPresup" runat="server" Visible="false">
            <fsn:FSNTab ID="fsnTabCtrlPresActivos" runat="server" Height="28px" />
            <fsn:FSNTab ID="fsnTabCtrlPres" runat="server" Height="28px" />
        </asp:Panel>
        <asp:Panel ID="panTabsNotificaciones" runat="server" Visible="false" class="tabMenu">
            <fsn:FSNTab ID="fsnTabCalNotificaciones" CssClass="tabMenu" runat="server" Height="28px" />
        </asp:Panel>
        <asp:Panel ID="panTabsFacturacion" runat="server" Visible="false">
            <fsn:FSNTab ID="fsnTabFactAlta" runat="server" Height="28px" OnClientClick="TempCargando()" />
            <fsn:FSNTab ID="fsnTabFactSeguimiento" runat="server" Height="28px" OnClientClick="TempCargando()" />    
            <fsn:FSNTab ID="fsnTabFactAutofacturacion" runat="server" Height="28px" OnClientClick="TempCargando()" />    
        </asp:Panel>
    </div>
    <div class="MenuPpal" style="display:inline-block; width:100%; min-height:35px;">
        <!--ContentSubMenu-->
        <asp:Panel runat="server" ID="pnlSubMenu" Style="float: right; width: 100%; height: 35px;">
            <asp:Panel ID="panMnuProcesos" runat="server" HorizontalAlign="Right" Style="display: none; float:right; margin-top:8px;">
                <asp:Menu ID="mnuProcesos" runat="server" Orientation="Horizontal">
                    <StaticMenuItemStyle HorizontalPadding="10px" />
                    <Items>
                        <asp:MenuItem Value="AltaSolicitudes"></asp:MenuItem>
                        <asp:MenuItem Value="Seguimiento"></asp:MenuItem>
                        <asp:MenuItem Value="Parametros"></asp:MenuItem>
                    </Items>
                    <StaticItemTemplate>
                        <fsn:FSNHyperlink ID="lnkmnuProcesos" runat="server" Font-Size="12px" SkinID="SubmenuPMWEB"
                            Selected='<%# Eval("Selected") %>' Text='<%# Eval("Text") %>' NavigateUrl='<%# Eval("NavigateUrl") %>'
                            CommandArgument='<%# Eval("Value") %>' Target="_top" onclick='<%# "return ParametrosPM(""" & Eval("Value") & """)" %>' />
                    </StaticItemTemplate>
                </asp:Menu>
            </asp:Panel>
            <asp:Panel ID="panMnuContratos" runat="server" HorizontalAlign="Right" Style="display: none; float:right; margin-top:8px;">
                <asp:Menu ID="mnuContratos" runat="server" Orientation="Horizontal">
                    <StaticMenuItemStyle HorizontalPadding="10px" />
                    <Items>
                        <asp:MenuItem Value="AltaContratos"></asp:MenuItem>
                        <asp:MenuItem Value="Seguimiento"></asp:MenuItem>
                    </Items>
                    <StaticItemTemplate>
                        <fsn:FSNHyperlink ID="lnkmnuContratos" runat="server" Font-Size="12px" SkinID="SubmenuPMWEB"
                            Selected='<%# Eval("Selected") %>' Text='<%# Eval("Text") %>' NavigateUrl='<%# Eval("NavigateUrl") %>'
                            CommandArgument='<%# Eval("Value") %>' Target="_top" onclick='<%# "return ParametrosPM(""" & Eval("Value") & """)" %>' />
                    </StaticItemTemplate>
                </asp:Menu>
            </asp:Panel>
            <asp:Panel ID="panMnuCalidad" runat="server" HorizontalAlign="Right" Style="display: none; float:right; margin-top:8px;">
                <asp:Menu ID="mnuCalidad" runat="server" Orientation="Horizontal">
                    <StaticMenuItemStyle HorizontalPadding="10px" />
                    <Items>
                        <asp:MenuItem Value="PanelEscalacionProveedores"></asp:MenuItem>
                        <asp:MenuItem Value="PanelProveedores"></asp:MenuItem>
                        <asp:MenuItem Value="SolicitudesQA">
                        <asp:MenuItem Value="SolicitudesQAAlta"></asp:MenuItem>
                        <asp:MenuItem Value="SolicitudesQASeguimiento"></asp:MenuItem>
                            </asp:MenuItem>
                            <asp:MenuItem Value="Encuestas">
                                <asp:MenuItem Value="EncuestasAlta"></asp:MenuItem>
                                <asp:MenuItem Value="EncuestasSeguimiento"></asp:MenuItem>
                            </asp:MenuItem>
                        <asp:MenuItem Value="Certificados"></asp:MenuItem>
                        <asp:MenuItem Value="NoConformidades"></asp:MenuItem>
                        <asp:MenuItem Value="Configuracion" Selectable="false">
                            <asp:MenuItem Value="Materiales"></asp:MenuItem>
                            <asp:MenuItem Value="UnidadesNegocio"></asp:MenuItem>
                            <asp:MenuItem Value="Escalacion"></asp:MenuItem>
                            <asp:MenuItem Value="Parametros"></asp:MenuItem>                            
                        </asp:MenuItem>
                    </Items>
                    <StaticItemTemplate>
                        <fsn:FSNHyperlink ID="lnkmnuCalidad" runat="server" Font-Size="12px" SkinID="SubmenuPMWEB"
                            Selected='<%# Eval("Selected") %>' Text='<%# Eval("Text") %>' NavigateUrl='<%# Eval("NavigateUrl") %>'
                            CommandArgument='<%# Eval("Value") %>' Target="_top" onclick="document.forms[0].target='_top'" />
                    </StaticItemTemplate>
                    <DynamicItemTemplate>
                        <fsn:FSNHyperlink ID="lnkmnuCalidadDinamico" runat="server" SubMenu="true" Font-Size="12px"
                            SkinID="SubmenuDinamicPMWEB" Selected='<%# Eval("Selected") %>' Text='<%# Eval("Text") %>'
                            NavigateUrl='<%# Eval("NavigateUrl") %>' CommandArgument='<%# Eval("Value") %>'
                            Target="_top" onclick="document.forms[0].target='_top'" />
                    </DynamicItemTemplate>
                    <DynamicMenuItemStyle CssClass="SubMenuMIStyle" />
                    <DynamicSelectedStyle CssClass="SubMenuSelStyle" />
                </asp:Menu>
            </asp:Panel>
            <asp:Panel ID="panMnuSolicitudesQA" runat="server" HorizontalAlign="Right" Style="display: none">
                <asp:Menu ID="mnuSolicitudesQA" runat="server" Orientation="Horizontal">
                    <StaticMenuItemStyle HorizontalPadding="10px" />
                    <Items>
                        <asp:MenuItem Value="SolicitudesQAAlta"></asp:MenuItem>
                        <asp:MenuItem Value="SolicitudesQASeguimiento"></asp:MenuItem>                                   
                    </Items>
                    <StaticItemTemplate>
                        <fsn:FSNHyperlink ID="lnkmnuSolicitudesQA" runat="server" Font-Size="12px"
                            SkinID="Submenu" Selected='<%# Eval("Selected") %>' Text='<%# Eval("Text") %>'
                            NavigateUrl='<%# Eval("NavigateUrl") %>' CommandArgument='<%# Eval("Value") %>'/>
                    </StaticItemTemplate>
                </asp:Menu>
            </asp:Panel>
            <asp:Panel ID="panMnuEncuestas" runat="server" HorizontalAlign="Right" Style="display: none">
                <asp:Menu ID="mnuEncuestas" runat="server" Orientation="Horizontal">
                    <StaticMenuItemStyle HorizontalPadding="10px" />
                    <Items>
                        <asp:MenuItem Value="EncuestasAlta"></asp:MenuItem>
                        <asp:MenuItem Value="EncuestasSeguimiento"></asp:MenuItem>                                   
                    </Items>
                    <StaticItemTemplate>
                        <fsn:FSNHyperlink ID="lnkmnuEncuestas" runat="server" Font-Size="12px"
                            SkinID="Submenu" Selected='<%# Eval("Selected") %>' Text='<%# Eval("Text") %>'
                            NavigateUrl='<%# Eval("NavigateUrl") %>' CommandArgument='<%# Eval("Value") %>'/>
                    </StaticItemTemplate>
                </asp:Menu>
            </asp:Panel>
            <asp:Panel ID="panMnuCalidadConfiguracion" runat="server" HorizontalAlign="Right" Style="display: none; float:right; margin-top:8px;">
                <asp:Menu ID="mnuCalidadConfiguracion" runat="server" Orientation="Horizontal">
                    <StaticMenuItemStyle HorizontalPadding="10px" />
                    <Items>
                        <asp:MenuItem Value="Materiales"></asp:MenuItem>
                        <asp:MenuItem Value="UnidadesNegocio"></asp:MenuItem>
                        <asp:MenuItem Value="Escalacion"></asp:MenuItem>
                        <asp:MenuItem Value="Parametros"></asp:MenuItem>                        
                    </Items>
                    <StaticItemTemplate>
                        <fsn:FSNHyperlink ID="lnkmnuCalidadConfiguracion" runat="server" Font-Size="12px"
                             SkinID="SubmenuPMWEB" Selected='<%# Eval("Selected") %>' Text='<%# Eval("Text") %>'
                            NavigateUrl='<%# Eval("NavigateUrl") %>' CommandArgument='<%# Eval("Value") %>'
                            Target="_top" onclick="document.forms[0].target='_top'" />
                    </StaticItemTemplate>
                </asp:Menu>
            </asp:Panel>
            <asp:Panel ID="panMnuCatalogo" runat="server" HorizontalAlign="Right" Style="display: none; float:right; margin-top:8px;">
                <asp:Menu ID="mnuCatalogo" runat="server" Orientation="Horizontal">
                    <StaticMenuItemStyle HorizontalPadding="10px" />
                    <Items>
                        <asp:MenuItem Value="Emision"></asp:MenuItem>
                        <asp:MenuItem Value="Seguimiento"></asp:MenuItem>
                        <asp:MenuItem Value="Recepcion"></asp:MenuItem>
                        <asp:MenuItem Value="Aprobacion"></asp:MenuItem>
                        <asp:MenuItem Value="Parametros"></asp:MenuItem>
                    </Items>
                    <StaticItemTemplate>
                        <fsn:FSNHyperlink ID="lnkmnuCatalogo" runat="server" Font-Size="12px" SkinID="SubmenuPMWEB"
                            Selected='<%# Eval("Selected") %>' Text='<%# Eval("Text") %>' NavigateUrl='<%# Eval("NavigateUrl") %>'
                            CommandArgument='<%# Eval("Value") %>' Target="_top" onclick='<%# "return ParametrosEP(""" & Eval("Value") & """)" %>' />
                    </StaticItemTemplate>
                </asp:Menu>
            </asp:Panel>
            <asp:Panel ID="panMnuCtrlPres" runat="server" HorizontalAlign="Right" Style="display: none; float:right; margin-top:8px;">
                <asp:Menu ID="mnuCtrlPres" runat="server" Orientation="Horizontal">
                    <StaticMenuItemStyle HorizontalPadding="10px" />
                    <Items>
                        <asp:MenuItem Value="ControlPresupuestario"></asp:MenuItem>
                        <asp:MenuItem Value="Activos"></asp:MenuItem>
                    </Items>
                    <StaticItemTemplate>
                        <fsn:FSNHyperlink ID="lnkmnuCtrlPres" runat="server" Font-Size="12px" SkinID="SubmenuPMWEB"
                            Selected='<%# Eval("Selected") %>' Text='<%# Eval("Text") %>' NavigateUrl='<%# Eval("NavigateUrl") %>'
                            CommandArgument='<%# Eval("Value") %>' Target="_top" />
                    </StaticItemTemplate>
                </asp:Menu>
            </asp:Panel>
            <asp:Panel ID="panMnuFacturacion" runat="server" HorizontalAlign="Right" Style="display: none; float:right; margin-top:8px;">
                <asp:Menu ID="mnuFacturacion" runat="server" Orientation="Horizontal">
                    <StaticMenuItemStyle HorizontalPadding="10px" />
                    <Items>
                        <asp:MenuItem Value="Alta"></asp:MenuItem>
                        <asp:MenuItem Value="Seguimiento"></asp:MenuItem>
                        <asp:MenuItem Value="Autofacturacion"></asp:MenuItem>
                    </Items>
                    <StaticItemTemplate>
                        <fsn:FSNHyperlink ID="lnkmnuFacturacion" runat="server" Font-Size="12px" SkinID="SubmenuPMWEB"
                            Selected='<%# Eval("Selected") %>' Text='<%# Eval("Text") %>' NavigateUrl='<%# Eval("NavigateUrl") %>'
                            CommandArgument='<%# Eval("Value") %>' Target="_top" onclick="TempCargando()" />
                    </StaticItemTemplate>
                </asp:Menu>
            </asp:Panel>
            <asp:Panel ID="panMnuSeguridad" runat="server" HorizontalAlign="Right" Style="display: none; float: right; margin-top: 8px;">
                <asp:Menu ID="mnuSeguridad" runat="server" Orientation="Horizontal">
                    <StaticMenuItemStyle HorizontalPadding="10px" />
                    <Items>
                        <asp:MenuItem Value="PerfilesSeguridad"></asp:MenuItem>
                    </Items>
                    <StaticItemTemplate>
                        <fsn:FSNHyperlink ID="lnkmnuPerfilesSeguridad" runat="server" Font-Size="12px" SkinID="SubmenuPMWEB"
                            Selected='<%# Eval("Selected") %>' Text='<%# Eval("Text") %>' NavigateUrl='<%# Eval("NavigateUrl") %>'
                            CommandArgument='<%# Eval("Value") %>'/>
                    </StaticItemTemplate>
                </asp:Menu>
            </asp:Panel>
            <asp:Panel ID="panMnuInformes" runat="server" HorizontalAlign="Right" Style="display: none; float:right; margin-top:8px;">
                <asp:Menu ID="mnuInformes" runat="server" Orientation="Horizontal">
                    <StaticMenuItemStyle HorizontalPadding="10px" />
                    <Items>
                        <asp:MenuItem Value="Informes"></asp:MenuItem>
                        <asp:MenuItem Value="BusinessInteligence"></asp:MenuItem>
                        <asp:MenuItem Value="QlikViewer"></asp:MenuItem>
					    <asp:MenuItem Value="Visor Actividad"></asp:MenuItem>
                        <asp:MenuItem Value="Visor Rendimiento"></asp:MenuItem>
                        <asp:MenuItem Value="Cubos"></asp:MenuItem>
                        <asp:MenuItem Value="QlikApps"></asp:MenuItem>
                    </Items>
                    <StaticItemTemplate>
                        <fsn:FSNHyperlink ID="lnkmnuInformes" runat="server" Font-Size="12px" SkinID="SubmenuPMWEB"
                            Selected='<%# Eval("Selected") %>' Text='<%# Eval("Text") %>' NavigateUrl='<%# Eval("NavigateUrl") %>'
                            CommandArgument='<%# Eval("Value") %>' Target="_top" onclick='<%# "return ParametrosPM(""" & Eval("Value") & """)" %>' />
                    </StaticItemTemplate>
                </asp:Menu>
            </asp:Panel>
        </asp:Panel>
    </div>
    <table cellpadding="0" cellspacing="0" border="0" style="width:195px; height:70px; position:absolute; left:35px; bottom:0px;">                                
        <tr>
            <td style="width:195px; background-color:White;">
                <asp:Image ID="Image1" runat="server" style="float:left; vertical-align:bottom;" ImageUrl="images/menu/LogoTopLeft.jpg" />                                        
                <asp:Image ID="Image2" runat="server" style="float:right; vertical-align:bottom;" ImageUrl="images/menu/LogoTopRight.jpg" />
            </td>
        </tr>
        <tr>
            <td style="width:195px; height:65px; background-color:White; text-align:center; vertical-align:middle;">
                <asp:Image ID="imgLogo" runat="server" style="vertical-align:middle; max-width:195px;" ImageUrl="images/menu/logo.jpg" />
            </td>
        </tr>
    </table>
</div>
<div id="popupFondo" class="FondoModal" style="display:none; position:absolute; z-index:1001; margin:auto; width:100%; top:0; left:0;">
    <div id="pnlCargando" class="popupCN" style="display:none; padding:1em 2em; position: fixed; z-index:1002; top: 50%; left: 50%; transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); text-align:center;">
        <img src="../images/cargando.gif" style="vertical-align:middle;" />	
	    <asp:Label ID="LblProcesando" runat="server" style="color:Black; margin-left:0.5em;"></asp:Label>
    </div>
</div>