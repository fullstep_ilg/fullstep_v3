﻿Public Partial Class detallepartida
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim oPartida As FSNServer.Partida
        Dim ds As DataSet

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Partidas

        Me.lblTitulo.Text = Request("Titulo")
        Me.lblCod.Text = Textos(9)
        Me.lblNombre.Text = Textos(10)
        Me.lblCentro.Text = Textos(1)
        Me.lblFechaInicio.Text = Textos(11)
        Me.lblFechaFin.Text = Textos(12)

        'carga los datos de la partida:
        oPartida = FSNServer.Get_Object(GetType(FSNServer.Partida))
        ds = oPartida.BuscarDetallePartida(Request("PRES5"), Request("texto"), Idioma)

        If ds.Tables(0).Rows.Count > 0 Then
            lblCodBD.Text = ds.Tables(0).Rows(0).Item("COD")
            lblNombreBD.Text = DBNullToSomething(ds.Tables(0).Rows(0).Item("DEN"))
            lblFechaInicioBD.Text = FormatDate(ds.Tables(0).Rows(0).Item("FECINI"), Usuario.DateFormat)
            lblFechaFinBD.Text = FormatDate(ds.Tables(0).Rows(0).Item("FECFIN"), Usuario.DateFormat)
        End If
        If ds.Tables(1).Rows.Count > 0 Then
            lblCentroBD.Text = DBNullToSomething(ds.Tables(1).Rows(0).Item(0))
        End If

        ds = Nothing
        oPartida = Nothing
    End Sub

End Class