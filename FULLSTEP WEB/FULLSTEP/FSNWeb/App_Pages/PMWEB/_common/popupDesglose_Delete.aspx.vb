Public Class popupDesglose_Delete
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents hypquitar As System.Web.UI.WebControls.LinkButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' <summary>
    ''' Cargar la opci�n de "eliminar l�nea" en un desglose al pinchar la flecha de la izda o la dcha.
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">evento de sistema</param>        
    ''' <returns>Nada</returns>
    ''' <remarks>Llamada desde: alta/desglose.ascx; Tiempo m�ximo:0,1</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Me.IsPostBack Then
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.popupDesglose
            
            Me.hypquitar.Text = Textos(1)
            hypquitar.Attributes.Add("OnClick", "javascript:quitar(); return false;")
        End If
    End Sub

End Class