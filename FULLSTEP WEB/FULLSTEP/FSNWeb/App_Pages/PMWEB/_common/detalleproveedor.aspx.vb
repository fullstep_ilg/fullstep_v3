
Public Class detalleproveedor
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCod As System.Web.UI.WebControls.Label
    Protected WithEvents lblCodBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblDen As System.Web.UI.WebControls.Label
    Protected WithEvents lblDenBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblNIF As System.Web.UI.WebControls.Label
    Protected WithEvents lblNIFBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblDir As System.Web.UI.WebControls.Label
    Protected WithEvents lblDirBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblCP As System.Web.UI.WebControls.Label
    Protected WithEvents lblCPBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblPob As System.Web.UI.WebControls.Label
    Protected WithEvents lblPobBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblPais As System.Web.UI.WebControls.Label
    Protected WithEvents lblPaisBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblProvincia As System.Web.UI.WebControls.Label
    Protected WithEvents lblProvinciaBD As System.Web.UI.WebControls.Label
    Protected WithEvents lblMon As System.Web.UI.WebControls.Label
    Protected WithEvents lblMonBD As System.Web.UI.WebControls.Label
    Protected WithEvents cmdCerrar As System.Web.UI.HtmlControls.HtmlInputButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oProve As FSNServer.Proveedor

        Dim sIdi As String = Session("FSN_User").Idioma.ToString()
        If sIdi = Nothing Then sIdi = ConfigurationManager.AppSettings("idioma")

        Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.DetalleProveedor, sIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        lblCod.Text = oTextos.Rows(0).Item(1)
        lblDen.Text = oTextos.Rows(1).Item(1)
        lblNIF.Text = oTextos.Rows(2).Item(1)
        lblDir.Text = oTextos.Rows(3).Item(1)
        lblCP.Text = oTextos.Rows(4).Item(1)
        lblPob.Text = oTextos.Rows(5).Item(1)
        lblPais.Text = oTextos.Rows(6).Item(1)
        lblProvincia.Text = oTextos.Rows(7).Item(1)
        lblMon.Text = oTextos.Rows(8).Item(1)
        cmdCerrar.Value = oTextos.Rows(9).Item(1)

        'carga los datos de la persona:
        oProve = FSWSServer.Get_Object(GetType(FSNServer.Proveedor))
        oProve.Cod = Request("codProv")
        oProve.Load(sIdi)

        lblCodBD.Text = oProve.Cod
        lblDenBD.Text = DBNullToSomething(oProve.Den)
        lblNIFBD.Text = DBNullToSomething(oProve.NIF)
        lblDirBD.Text = DBNullToSomething(oProve.Dir)
        lblCPBD.Text = DBNullToSomething(oProve.CP)
        lblPobBD.Text = DBNullToSomething(oProve.Pob)
        lblPaisBD.Text = DBNullToSomething(oProve.PaiDen)
        lblProvinciaBD.Text = DBNullToSomething(oProve.ProviDen)
        lblMonBD.Text = DBNullToSomething(oProve.MonCod) & " - " & DBNullToSomething(oProve.MonDen)

        oProve = Nothing
        oTextos = Nothing
    End Sub
End Class