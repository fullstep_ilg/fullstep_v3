<%@ Register TagPrefix="fsde" Namespace="Fullstep.DataEntry" Assembly="DataEntry" %>
<%@ Register TagPrefix="igtxt" Namespace="Infragistics.WebUI.WebDataInput" Assembly="Infragistics.WebUI.WebDataInput.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Control Language="vb" AutoEventWireup="false" Codebehind="atachedfiles.ascx.vb" Inherits="Fullstep.FSNWeb.Common.atachedfiles" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="igtbl" Namespace="Infragistics.WebUI.UltraWebGrid" Assembly="Infragistics.WebUI.UltraWebGrid.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<script type="text/javascript">
var tipoOld
var idAdjuntoOld
var filaOldIndex

//funcion que elimina el adjunto de la grid y del input si fuera el caso
function eliminarAdjunto(tipo, IdAdjunto){
    var input = document.all("<%=me.ClientID%>_FSENTRY").value 
    p=window.opener
    try
    {
        oInput = p.fsGeneralEntry_getById(input)
        oInput.removeFile(tipo, IdAdjunto)
    }
    catch(err)
    {}
    var oGrid = igtbl_getGridById("<%=me.ClientID%>xuwgFiles")
    var oRow
    for (i=0;i<oGrid.Rows.length;i++){
        oRow = oGrid.Rows.getRow(i);
        if(oRow.getCellFromKey("ID").getValue()==IdAdjunto){
            oRow.remove()
            break
        }    
    }
    recorrerGrid()
}
//funcion que sustituye el adjunto de la grid por otro
function sustituirAdjunto(tipo,idAdjunto,nom, coment){
    tipoOld=tipo
    idAdjuntoOld=idAdjunto
    var oRow
    var oGrid = igtbl_getGridById("<%=me.ClientID%>xuwgFiles")
    for (i=0;i<oGrid.Rows.length;i++){
        oRow = oGrid.Rows.getRow(i);
        if(oRow.getCellFromKey("ID").getValue()==idAdjunto){
            filaOldIndex=i
            break
        }
    }
    var instancia = ""
	if (document.all("<%=me.clientID%>_txtInstancia"))
		instancia = document.all("<%=me.clientID%>_txtInstancia").value
    var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/attachfile.aspx?Origen=atachedfiles.ascx&instancia=" + instancia +"&id=" + idAdjunto.toString() + "&tipo=" + tipo.toString() + "&Campo=" + document.all("<%=me.ClientID%>_IDCAMPO").value +"&Input=" + document.all("<%=me.ClientID%>_FSENTRY").value + "&Nombre=" + nom + "&Coment=" + coment, "_blank","width=420,height=235,status=no,resizable=no,top=200,left=300");
    newWindow.focus();

    recorrerGrid()
}
//funcion que descarga el adjunto seleccionado
function descargarAdjunto(tipo,idAdjunto,instancia){
    
    var instancia = ""
	if (document.all("<%=me.clientID%>_txtInstancia"))
		instancia = document.all("<%=me.clientID%>_txtInstancia").value
    var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/attach.aspx?instancia=" + instancia +"&id=" + idAdjunto.toString() + "&tipo=" + tipo.toString(), "_blank","width=600,height=275,status=no,resizable=yes,top=200,left=200")
    newWindow.focus();
}

function recorrerGrid()
{
var oGrid = igtbl_getGridById("<%=me.ClientID%>xuwgFiles")
var sAdjun = ""
for (i=0;i<oGrid.Rows.length;i++)
	{
	oRow = oGrid.Rows.getRow(i);
	sAdjun += oRow.getCellFromKey("NOM").getValue() + " (" +  oRow.getCellFromKey("DATASIZE").getValue().toString() + " kb.), "
	}
sAdjun = sAdjun.substr(0,sAdjun.length-2)
var input = document.all("<%=me.ClientID%>_FSENTRY").value 
try
  {
    oInput = p.fsGeneralEntry_getById(input)
    oInput.setValue(sAdjun)
  }
catch(err)
  {}
}
	
function anyadirAdjunto()
{
	var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaPM")%>_common/attachfile.aspx?Origen=atachedfiles.ascx&Campo=" + document.all("<%=me.ClientID%>_IDCAMPO").value +"&Input=" + document.all("<%=me.ClientID%>_FSENTRY").value ,"_blank", "width=420,height=235,status=no,resizable=no,top=200,left=300")
    newWindow.focus();
}

/*
''' <summary>
''' A�adir en el grid el adjunto recien insertado.
''' </summary>
''' <param name="Id">Id en bbdd del adjunto</param>
''' <param name="nom">Nombre del adjunto</param>        
''' <param name="com">Comentario del adjunto</param>
''' <param name="datasize">tama�o del adjunto</param>
''' <param name="tipo">tipo de adjunto. 2 significa nuevo</param>
''' <param name="campo">Id en bbdd del campo fichero</param>
''' <param name="input">Id entry del campo fichero</param>
''' <param name="comsalto">Comentario del adjunto sin saltos de linea. Se han traducido por @VBCRLF@.</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:_common/attachfile.aspx; Tiempo m�ximo:0</remarks>
*/
function adjuntoAnyadido(Id,nom,com,datasize,tipo, campo, input,comsalto)
{
p=window.opener
try
  {
    oInput = p.fsGeneralEntry_getById(input)
  }
catch(err)
  {
    oInput=null
  }

if (filaOldIndex)
	{
    var oGrid = igtbl_getGridById("<%=me.ClientID%>xuwgFiles")
	for (i=0;i<oGrid.Rows.length;i++){
        if(i==filaOldIndex){
            oRow = oGrid.Rows.getRow(i);
            break
        }
    }
    try{
        oInput.removeFile(tipoOld, idAdjuntoOld)
    }catch(err){}
	
	filaOldIndex=null
	tipoOld = null
	idAdjuntoOld=null
	}	
else
	oRow = igtbl_addNew("<%=me.ClientID%>xuwgFiles",0); 
	
var imagen
var celdaHtml
var instancia

if (document.all("<%=me.clientID%>_txtInstancia"))
    instancia=document.all("<%=me.clientID%>_txtInstancia").value
	
oCell = oRow.getCellFromKey("TIPO")
oCell.setValue(tipo)
oCell = oRow.getCellFromKey("ID")
oCell.setValue(Id)
oCell = oRow.getCellFromKey("FECALTA")
oCell.setValue(new Date())
oCell = oRow.getCellFromKey("NOMBRE")
oCell.setValue("<%=Session("FSN_User").Nombre%>")
oCell = oRow.getCellFromKey("NOM")
oCell.setValue(nom)
oCell = oRow.getCellFromKey("DATASIZE")
oCell.setValue(datasize)
oCell = oRow.getCellFromKey("ELIMINAR")
celdaHtml = oCell.getElement()
imagen="<img style='cursor:pointer' src='<%=ConfigurationManager.AppSettings("rutaPM")%>_common/images/eliminar.gif' onclick=\"eliminarAdjunto('" + tipo + "'," + Id + ");return false;\">"
celdaHtml.innerHTML = imagen
oCell = oRow.getCellFromKey("DESCARGAR")
celdaHtml = oCell.getElement()
imagen="<img style='cursor:pointer' src='<%=ConfigurationManager.AppSettings("rutaPM")%>_common/images/descargar.gif' onclick=\"descargarAdjunto('" + tipo + "'," + Id + "," + instancia +");return false;\">"
celdaHtml.innerHTML = imagen
oCell = oRow.getCellFromKey("SUSTITUIR")
celdaHtml = oCell.getElement()
imagen="<img style='cursor:pointer' src='<%=ConfigurationManager.AppSettings("rutaPM")%>_common/images/sustituir.gif' onclick=\"sustituirAdjunto('" + tipo + "'," + Id + ",'" + nom +"','"+ com + "');return false;\">"
celdaHtml.innerHTML = imagen
oCell = oRow.getCellFromKey("COMENT")
while (com.toString().indexOf("&#39;") != -1){   
    com = com.toString().replace("&#39;","'");   
} 
while (com.toString().indexOf("&#34;") != -1){   
   com = com.toString().replace("&#34;",'"');   
}
oCell.setValue(com)

oCell = oRow.getCellFromKey("COMENTSalto")
while (comsalto.toString().indexOf("@VBCRLF@") != -1){   
    comsalto = comsalto.toString().replace("@VBCRLF@","/n");   
}  
oCell.setValue(comsalto)

if(oInput)
    oInput.addFile(Id)
recorrerGrid()

}
</SCRIPT>
<P>
	<TABLE id="Table1" width="100%">
		<TR>
			<TD style="WIDTH: 258px">
				<DIV title="  " style="DISPLAY: inline; WIDTH: 70px; HEIGHT: 15px" ms_positioning="FlowLayout"></DIV>
			</TD>
			<TD style="WIDTH: 506px"></TD>
			<TD colSpan="2"></TD>
		</TR>
		<tr>
			<td style="WIDTH: 258px"><asp:label id="lblTitulo" runat="server" CssClass="captionBlue">Alta de solicitud tipo:</asp:label></td>
			<td style="WIDTH: 506px"><asp:label id="lblTipo" runat="server" CssClass="captionDarkBlue">Tipo de solicitud</asp:label></td>
			<td colSpan="2"></td>
		</tr>
		<TR>
			<TD style="WIDTH: 258px; HEIGHT: 33px" width="258"><asp:label id="lblSubTitulo" runat="server" CssClass="captionBlue"> Grupo:</asp:label></TD>
			<td style="WIDTH: 506px"><asp:label id="lblNomCampo" runat="server" CssClass="captionDarkBlue">Nombre del campo</asp:label></td>
			<TD style="HEIGHT: 33px" align="right" width="20%"><asp:label id="lblDescargar" runat="server" CssClass="parrafo" Visible="False">Descargar todos</asp:label></TD>
			<TD style="HEIGHT: 33px" align="center" width="10%"><asp:imagebutton id="imgDescarga" runat="server" ImageUrl="images/DescargarEspecif.gif" Visible="False"></asp:imagebutton></TD>
		</TR>
	</TABLE>
</P>
<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
       
        <igtbl:ultrawebgrid id="uwgFiles" runat="server" Width="100%" Height="192px">
				<DisplayLayout AllowAddNewDefault="Yes" RowHeightDefault="20px" Version="4.00" HeaderClickActionDefault="SortSingle"
					BorderCollapseDefault="Separate" Name="xctl0xuwgFiles" AllowUpdateDefault="Yes">
					<AddNewBox Hidden="False">
						<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray" CustomRules="visibility:hidden;height:0px;">

<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
</BorderDetails>

						</Style>
					</AddNewBox>
					<Pager>
						<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">

<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
</BorderDetails>

						</Style>
					</Pager>
					<HeaderStyleDefault Font-Size="XX-Small" Font-Names="Verdana" BackColor="WhiteSmoke" CssClass="VisorHead"></HeaderStyleDefault>
					<FrameStyle Width="100%" Height="192px" CssClass="VisorFrame"></FrameStyle>
					<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
						<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
					</FooterStyleDefault>
					
					<ActivationObject AllowActivation="False"></ActivationObject>
					<FixedHeaderStyleDefault CssClass="VisorHead"></FixedHeaderStyleDefault>
					<EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
					<RowStyleDefault BorderWidth="1px" Font-Size="XX-Small" Font-Names="Verdana" BorderColor="Gray" BorderStyle="Solid"
						CssClass="VisorRow">
						<Padding Left="3px"></Padding>
						<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
					</RowStyleDefault>
				</DisplayLayout>
				<Bands>
					<igtbl:UltraGridBand AllowUpdate="Yes" AllowAdd="Yes" RowSelectors="No">
						<HeaderStyle CssClass="CabeceraCampoArchivo"></HeaderStyle>
						<RowAlternateStyle CssClass="ugfilaalttabla"></RowAlternateStyle>
						<Columns>
							<igtbl:UltraGridColumn HeaderText="Descargar" Key="DESCARGAR" BaseColumnName="" AllowUpdate="No">
								<Footer Key="DESCARGAR"></Footer>
								<Header Key="DESCARGAR" Caption="Descargar"></Header>
							</igtbl:UltraGridColumn>
							<igtbl:UltraGridColumn HeaderText="Sustituir" Key="SUSTITUIR" BaseColumnName="" AllowUpdate="No">
								<Footer Key="SUSTITUIR"></Footer>
								<Header Key="SUSTITUIR" Caption="Sustituir"></Header>
							</igtbl:UltraGridColumn>
							<igtbl:UltraGridColumn HeaderText="Eliminar" Key="ELIMINAR" BaseColumnName="" AllowUpdate="No">
								<Footer Key="ELIMINAR"></Footer>
								<Header Key="ELIMINAR" Caption="Eliminar"></Header>
							</igtbl:UltraGridColumn>
						</Columns>
						<RowStyle CssClass="ugfilatabla"></RowStyle>
					</igtbl:UltraGridBand>
				</Bands>
			</igtbl:ultrawebgrid>
		</td>
	</tr>
	<tr>
		<td>
			<INPUT id="cmdAnyadir" onclick="anyadirAdjunto()" type="button" value="A�adir archivo"
				name="cmdAnyadir" runat="server" class="botonPMWEB">
		</td>
	</tr>
</table>
<INPUT id="IDCAMPO" type="hidden" name="IDCAMPO" runat="server"> <INPUT id="txtInstancia" type="hidden" name="txtInstancia" runat="server">
<INPUT id="FSENTRY" type="hidden" name="FSENTRY" runat="server">
