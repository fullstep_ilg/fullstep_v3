<%@ Register TagPrefix="igtbl" Namespace="Infragistics.WebUI.UltraWebGrid" Assembly="Infragistics.WebUI.UltraWebGrid.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="provfavserver.aspx.vb" Inherits="Fullstep.FSNWeb.provfavserver"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
	</head>
	<style type="text/css"> 
		#uwgProvFav_div
		{
			height :160px !important; 
		}
	</style>
	<script type="text/javascript">
		function SeleccionarProveedor(sCod, sDen) {
			var p = window.parent;
			var oEntry = p.fsGeneralEntry_getById("<%=Request("Entry")%>");

		    if (oEntry.TipoSolicit == 8) {
		        var sProveedor;
		        sProveedor = oEntry.getDataValue();
		        p.borraArticulosAdjudicadosProve(sProveedor);
		        p.BorrarProveedoresERP(oEntry);
		    }

			oEntry.setValue(sCod + " - " + sDen)
			oEntry.setDataValue(sCod)
			
			if (p.document.getElementById("<%=Request("FrameId")%>")){
				var oFrame = p.document.getElementById("<%=Request("FrameId")%>")
				oFrame.parentNode.removeChild(oFrame);
			}
		}
	</script>	
	<body class="EstadosFondo2">
		<form id="Form1" method="post" runat="server">
			<table style="Z-INDEX: 101; LEFT: 0px; WIDTH: 200px; POSITION: absolute; TOP: 0px; HEIGHT: 200px"
				class="cabeceraSolicitud igTreeEnTab">
				<tr>
					<td><igtbl:ultrawebgrid id="uwgProvFav" style="Z-INDEX: 102; LEFT: 2px; POSITION: absolute; TOP: 2px" runat="server"
							Width="200px" Height="160px">
							<DisplayLayout RowHeightDefault="20px" Version="4.00" GridLinesDefault="None"
								BorderCollapseDefault="Separate" Name="uwgProvFav" ReadOnly="LevelTwo" ScrollBar="Auto">
								<HeaderStyleDefault BorderStyle="Solid" BackColor="LightGray">
									<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
								</HeaderStyleDefault>
								<FrameStyle Width="196px" Cursor="Hand" BorderWidth="1px" Font-Size="8pt" BorderStyle="None"
									Height="160px"></FrameStyle>
								<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
									<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
								</FooterStyleDefault>
								<RowStyleDefault BorderWidth="1px" BorderColor="Gray" BorderStyle="Solid">
									<Padding Left="3px"></Padding>
									<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
								</RowStyleDefault>
							</DisplayLayout>
							<Bands>
								<igtbl:UltraGridBand ColHeadersVisible="No" CellClickAction="RowSelect" RowSelectors="No"></igtbl:UltraGridBand>
							</Bands>
						</igtbl:ultrawebgrid><asp:checkbox id="chkMostrarMat" style="Z-INDEX: 102; LEFT: 2px; POSITION: absolute; TOP: 176px"
							runat="server" Text="Mostrar s�lo los del material" Font-Size="10px" AutoPostBack="True"></asp:checkbox>
						<hr style="Z-INDEX: 103; LEFT: 0px; POSITION: absolute; TOP: 170px; BACKGROUND-COLOR: black"
							width="196" size="1" />
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
