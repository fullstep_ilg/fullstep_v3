<%@ Page Language="vb" AutoEventWireup="false" Codebehind="QAEnProceso.aspx.vb" Inherits="Fullstep.FSNWeb.QAEnProceso" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblComent" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" height="100%"
				cellSpacing="1" cellPadding="1" width="100%" border="0">
				<TR height="10%">
					<TD>
						<TABLE class="cabeceraSolicitud" id="tblCabecera" style="WIDTH: 100%; HEIGHT: 75px" cellSpacing="1"
							cellPadding="1" width="100%" border="0">
							<TR>
								<TD style="WIDTH: 15%"><asp:label id="lblId" runat="server" CssClass="captionBlue" Width="100%"></asp:label></TD>
								<TD style="WIDTH: 352px"><asp:label id="lblIdBD" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label></TD>
								<TD style="WIDTH: 15%"><asp:label id="lblFecha" runat="server" CssClass="captionBlue" Width="100%">lblFecha</asp:label></TD>
								<TD style="WIDTH: 352px"><asp:label id="lblFechaBD" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 15%"></TD>
								<TD style="WIDTH: 352px"></TD>
								<TD style="WIDTH: 15%"></TD>
								<TD style="WIDTH: 352px"></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 10%"><asp:label id="lblTipo" runat="server" CssClass="captionBlue" Width="100%">lblTipo</asp:label></TD>
								<TD style="WIDTH: 352px"><asp:label id="lblTipoBD" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label></TD>
								<TD style="WIDTH: 10%"><asp:label id="lblProveedor" runat="server" CssClass="captionBlue" Width="100%">lblDirijida a:</asp:label></TD>
								<TD style="WIDTH: 352px"><asp:label id="lblProveedorBD" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD align="center">
						<p>
							<table width="90%" align="center" cellpadding="4">
								<tr>
									<td><img src="images/Icono_Error_Amarillo_40x40.gif"></td>
									<td><asp:Label id="lblEnProceso" runat="server" CssClass="SinDatos"></asp:Label></td>
								</tr>
							</table>
						</p>
					</TD>
				</TR>
				<TR>
					<TD align="center"><input type="button" id="btnCerrar" onclick="window.close()" class="botonPMWEB" runat="server"
							NAME="btnCerrar"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</html>
