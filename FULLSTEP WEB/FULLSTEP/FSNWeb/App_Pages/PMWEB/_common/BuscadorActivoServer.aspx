﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuscadorActivoServer.aspx.vb"
    Inherits="Fullstep.FSNWeb.BuscadorActivoServer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>

    <script type="text/javascript">
        function ponerDen_Correcto(idFSEntry, value, dataValue) {

            p = window.parent.window.document;

            idFSEntry = idFSEntry.replace("__t", "");
            e = p.getElementById(idFSEntry + "__tbl")
            if (e)
                oEntry = e.Object

            if (oEntry) {
                oEntry.setValue(value);
                oEntry.setDataValue(dataValue);
                oEntry.Editor.elem.className = "TipoTextoMedioVerde"
            }

            window.close();
        }

        function borrarValorActivo(idFSEntry,bEsIncorrecto) {
            p = window.parent.window.document;

            idFSEntry = idFSEntry.replace("__t", "");
            e = p.getElementById(idFSEntry + "__tbl")
            if (e)
                oEntry = e.Object

            if (oEntry) {
                oEntry.setDataValue('');
                if (bEsIncorrecto)
                    oEntry.Editor.elem.className = "TipoTextoMedioRojo"
                else
                    oEntry.Editor.elem.className = "TipoTextoMedio"
            }

            window.close();
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
    </div>
    </form>
</body>
</html>
