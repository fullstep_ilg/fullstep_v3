
Public Class detalledenominacion
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCaso1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblDescripcionCaso1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblCaso2 As System.Web.UI.WebControls.Label
    Protected WithEvents lblDescripcionCaso2 As System.Web.UI.WebControls.Label
    Protected WithEvents lblCaso3 As System.Web.UI.WebControls.Label
    Protected WithEvents lblDescripcionCaso3 As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
"<script language=""{0}"">{1}</script>"


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.DetalleDenominacionPresupuestos

        Dim sScriptTitulo
        sScriptTitulo += "var cadenatitulo = '" + Textos(0) + "';"
        sScriptTitulo = String.Format(IncludeScriptKeyFormat, "javascript", sScriptTitulo)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "scripttitulo", sScriptTitulo)

        Me.lblCaso1.Text = Textos(1)
        Me.lblDescripcionCaso1.Text = Textos(2)
        Me.lblCaso2.Text = Textos(3)
        Me.lblDescripcionCaso2.Text = Textos(4)
        Me.lblCaso3.Text = Textos(5)
        Me.lblDescripcionCaso3.Text = Textos(6)
    End Sub

End Class

