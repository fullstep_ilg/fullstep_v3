﻿Public Partial Class CambiarPWD
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Contraseñas
        If Not IsPostBack Then
            lblPWDActual.Text = Textos(1)
            lblPWDNueva.Text = Textos(2)
            lblPWDNuevaRep.Text = Textos(3)
            lblTituloCambioPWD.Text = Textos(17)
            cmdAceptarPWD.Text = Textos(4)
            cmdAceptarContrOK.Text = Textos(4)
            LblMsgContr.Text = Textos(9)
        End If
    End Sub

    ''' <summary>
    ''' Evento que se genera al pulsar el botón de aceptar la contraseña, comprobando los datos y llamando al webservice
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento(btnAceptarPWD)</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: Automática, cuando se pincha el botón
    ''' Tiempo máximo: 0,2 seg</remarks>
    Private Sub cmdAceptarPWD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAceptarPWD.Click
        Dim webService As New FSNWebServicePWD.CambioPWD
        Dim errorPWD As Integer = 0
        Dim dFechaNuevaPWD As Date
        Dim sPassword As String
        Dim sFile As String = "\log_"
        Dim sPer As String

        'ha podido cambiarse la imagen
        ImgInfoContrMal.ImageUrl = "~/App_Pages/PMWEB/_common/images/alert-large.gif"

        If Usuario.Password <> txtPWDActual.Text Then
            LblMsgContr.Text = Textos(16)
            txtPWDActual.Text = ""
            UpDatosPanelCambioContr.Update()
            mpeMsgCambioContr.Show()
            Exit Sub
        ElseIf txtPWDNueva.Text <> txtPWDNuevaRep.Text Then
            LblMsgContr.Text = Textos(15)
            txtPWDNuevaRep.Text = ""
            UpDatosPanelCambioContr.Update()
            mpeMsgCambioContr.Show()
            Exit Sub
        End If
        Try
            dFechaNuevaPWD = Date.Now
            errorPWD = webService.CambiarLogin(0, Usuario.Cod, txtPWDActual.Text, Usuario.Cod, txtPWDNueva.Text, _
                                               dFechaNuevaPWD.Year, dFechaNuevaPWD.Month, dFechaNuevaPWD.Day, dFechaNuevaPWD.Hour, dFechaNuevaPWD.Minute, dFechaNuevaPWD.Second, _
                                               0, "", _
                                               Usuario.FechaPassword.Year, Usuario.FechaPassword.Month, Usuario.FechaPassword.Day, Usuario.FechaPassword.Hour, Usuario.FechaPassword.Minute, Usuario.FechaPassword.Second)
        Catch ex As Exception
            Exit Sub
        End Try

        If errorPWD = 0 Then
            Usuario.Password = txtPWDNueva.Text
            Usuario.FechaPassword = dFechaNuevaPWD

            ImgInfoContrMal.ImageUrl = "~/App_Pages/PMWEB/_common/images/ChangePassword.jpg"
            LblMsgContr.Text = Textos(6)
            mpeMsgCambioContr.Show()
            cmdAceptarContrOK.Attributes.Add("onclick", "window.close()")
        Else
            txtPWDNueva.Text = ""
            txtPWDNuevaRep.Text = ""
            Select Case errorPWD
                Case 1
                    Dim iLongitudMinima As Integer
					If FSNServer.TipoAcceso.gbCOMPLEJIDAD_PWD Then
						iLongitudMinima = If(FSNServer.TipoAcceso.giMIN_SIZE_PWD > 8, FSNServer.TipoAcceso.giMIN_SIZE_PWD, 8)
					Else
						iLongitudMinima = 8
					End If
                    LblMsgContr.Text = Textos(7) & " " & iLongitudMinima & " " & Textos(8)
                Case 2
                    LblMsgContr.Text = Textos(9) & "<br>" & Textos(20) & "<p>" & Textos(7) & " " & FSNServer.TipoAcceso.giMIN_SIZE_PWD & " " & Textos(8) & "</p><p>" & Textos(21) & "</p><p>" & Textos(22) & "</p>"
                Case 3
                    LblMsgContr.Text = Textos(10)
                Case 4
                    LblMsgContr.Text = Textos(11) & FSNServer.TipoAcceso.giHISTORICO_PWD & Textos(12)
                Case 5
                    LblMsgContr.Text = Textos(13)
                Case Else
                    LblMsgContr.Text = Textos(14)
                    txtPWDActual.Text = ""
            End Select
            UpDatosPanelCambioContr.Update()
            mpeMsgCambioContr.Show()
        End If
    End Sub
End Class