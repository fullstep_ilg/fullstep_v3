﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuscadorCentroCosteServer.aspx.vb" Inherits="Fullstep.FSNWeb.BuscadorCentroCosteServer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
</head>
<script>
    var sinColor = '<%=Request("sinColor") %>'
    function ponerDen_Correcto(idFSEntryCC, sDen, sCod) {
        var oEntry;
        p = window.parent.window.document;

        idFSEntryCC = idFSEntryCC.replace("__t", "");
        e = p.getElementById(idFSEntryCC + "__tbl")
        if (e)
            oEntry = e.Object

        if (oEntry) {
            oEntry.setValue(sDen);
            oEntry.setDataValue(sCod);
            if (sinColor == '1')
                oEntry.Editor.elem.className = "TipoTextoMedio"
            else
                oEntry.Editor.elem.className = "TipoTextoMedioVerde"
        }

        window.close();
    }

    function borrarValorCentroCoste(idFSEntryCC,bEsIncorrecto) {
        var oEntry;
        p = window.parent.window.document;

        idFSEntryCC = idFSEntryCC.replace("__t", "");
        e = p.getElementById(idFSEntryCC + "__tbl")
        if (e)
            oEntry = e.Object

        if (oEntry) {
            oEntry.setDataValue('');
            if (bEsIncorrecto)
                oEntry.Editor.elem.className = "TipoTextoMedioRojo"
            else
                oEntry.Editor.elem.className = "TipoTextoMedio"
        }

        window.close();
    }
</script>

<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
