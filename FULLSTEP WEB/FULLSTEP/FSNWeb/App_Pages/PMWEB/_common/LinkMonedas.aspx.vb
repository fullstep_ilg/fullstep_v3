Public Class LinkMonedas
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents wdgMoneda As Infragistics.Web.UI.GridControls.WebDataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' <summary>
    ''' Carga una grid con las monedas
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>LLamada desde: Al cargarse la p�gina; Tiempo m�ximo: 0,1 sg.</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oMonedas As FSNServer.Monedas = FSNServer.Get_Object(GetType(FSNServer.Monedas))
        oMonedas.LoadData(Idioma, , , , True)

        Dim oGrid As Infragistics.Web.UI.GridControls.WebDataGrid
        oGrid = Me.wdgMoneda
        oGrid.Rows.Clear()
        oGrid.Columns.Clear()
        oGrid.DataSource = oMonedas.Data
        CrearColumnas()
        oGrid.DataBind()

        Me.wdgMoneda.Columns("COD").Hidden = True
        Me.wdgMoneda.Columns("EQUIV").Hidden = True
        Me.wdgMoneda.Columns("FECACT").Hidden = True
    End Sub

    Private Sub CrearColumnas()
        Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
        Dim nombre As String

        For i As Integer = 0 To wdgMoneda.DataSource.Tables(0).Columns.Count - 1
            campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
            nombre = wdgMoneda.DataSource.Tables(0).Columns(i).ToString
            With campoGrid
                .Key = nombre
                .DataFieldName = nombre
                .Header.Text = nombre
                .CssClass = "sinSalto alturaMonedas"
            End With
            wdgMoneda.Columns.Add(campoGrid)
        Next
    End Sub

    Private Sub wdgMoneda_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wdgMoneda.InitializeRow
        e.Row.Items.FindItemByKey("DEN").Tooltip = e.Row.Items.FindItemByKey("DEN").Text
    End Sub
End Class
