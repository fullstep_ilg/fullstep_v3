<%@ Page Language="vb" AutoEventWireup="false" Codebehind="BuscadorArticuloserver.aspx.vb" Inherits="Fullstep.FSNWeb.BuscadorArticulo" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
	<title></title>
	<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
	<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
	<meta name="vs_defaultClientScript" content="JavaScript">
	<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
</head>

<script type="text/javascript">
    function rellenarCampos(idFSEntryCod, idFSEntryDen, sValor, bExiste, idFSEntryPrec, lPrecio, idFSEntryProv, sCodProv, sDenProv, idFSEntryUni, sUnidad, sDenUnidad, bGenerico, idfsEntryOrg, sCodOrg, sDenOrg, idfsEntryCentro, sCodCentro, sDenCentro, sConceptoTipoPedido, sAlmacenamientoTipoPedido, sRecepcionTipoPedido, sConcepto, sAlmacenamiento, sRecepcion, idFSEntryCant, NumeroDeDecimales, iTipoSolicitud, sInstanciaMoneda, sIDFSEntryProve, sIDFSEntryUniPedido, sUnidadPedido, sDenUnidadPedido, sIDFSEntryFPago, sIDFSEntryFPagoHidden, sFPagoPROVUltADJ, sDenFPagoPROVUltADJ, sTipoRecepcion) {
        //rue 310309	
        //Descripci�n:	Rellena los campos del articulo y asociados introducido por el usuario
		//Param. Entrada: 
		//idFSEntryCod:     DataEntry del codigo
		//idFSEntryDen:     DataEntry de la denominacion
		//sValor:           Valor del atriculo
		//bExiste:          Existe en BD
		//idFSEntryPrec:    Dataentry del precio
		//lPrecio:          Precio
		//idFSEntryProv:    DataEntry del proveedor
		//sCodProv:         Codigo del proveedor
		//sDenProv:         Denominacion del proveedor
		//idFSEntryUni:     Dataentry de la unidad
		//sUnidad:          Unidad del pedido. Si estamos en un pedido negociado es la Unidad de la adjudicaci�n.
        //sDenUnidad:       Denominacion de la unidad. Si estamos en un pedido negociado es la denominaci�n de la Unidad de la adjudicaci�n.
		//bGenerico:        Es generico el articulo
		//idfsEntryOrg:     DatEntry organizacion de compra
		//sCodOrg:          Codigo de organizacion de compras
		//sDenOrg:          Denominacion de organizacion de compras
		//idfsEntryCentro:  DatEntry del centro
		//sCodCentro:       Codigo del centro
		//sDenCentro:       Denominacion del centro
		//idFSEntryCant:    Dataentry de la cantidad
        //NumeroDeDecimales:N� de decimales que admite el campo cantidad
        //iTipoSolicitud:   Tipo de solicitud de la instancia
        //sInstanciaMoneda: Moneda de la instancia
        //sIDFSEntryProve:      Id del DataEntry de Proveedor (s�lo si el tipo de solicitud es 8-Pedido Expres o 9-Pedido Negociado)
        //sIDFSEntryUniPedido:  Id den DataEntry de la unidad de pedido (s�lo si el tipo de solicitud es 8-Pedido Expres o 9-Pedido Negociado)
        //sUnidadPedido:    C�digo de la unidad del pedido.
        //sDenUnidadPedido: Denominacion de la unidad del pedido.
        //sIDFSEntryFPago: Id del dataentry de Forma de pago
        //sIDFSEntryFPagoHidden: Id del dataentry oculto de forma de pago
        //sFPagoPROVUltADJ: Forma pago de la �ltima adjudicaci�n vigente.
        //sDenFPagoPROVUltADJ:  Descripci�n de la Forma pago de la �ltima adjudicaci�n vigente.
        //sTipoRecepcion:   Tipo de recepci�n (0: por cantidad / 1: por importe)
		//Param. Salida: -
		//LLamada:	BuscadorArticuloserver.aspx
        //Tiempo:	0
        var e = null;
		var oArt = null;	
		var oDen = null;
		var oPrecio = null;
		var oProveedor = null;
		var oUnidad = null;
		var oOrg = null;
		var oCentro = null;
        var p = null;
        var pp = null;
		p =window.parent.window.document;
	
		idFSEntryCod = idFSEntryCod.replace("__t","");
		e = p.getElementById(idFSEntryCod + "__tbl")

		if (e)
			oArt = e.Object;	
	
		idFSEntryDen = idFSEntryDen.replace("__t","");
		e = p.getElementById(idFSEntryDen+"__tbl");
		if (e)
			oDen = e.Object ;

		if ((sValor != null) && (sValor != "")) {
        
			//Validaciones Concepto
			if (sConceptoTipoPedido != '') {
				if ((sConceptoTipoPedido == '1') && (sConcepto == '0')) {
					alert(mensajeConceptoGasto);
					ponerCodArt_Den_Blanco(idFSEntryCod, idFSEntryDen);
					return false;
				}

				if ((sConceptoTipoPedido == '0') && (sConcepto == '1')) {
					alert(mensajeConceptoInversion);
					ponerCodArt_Den_Blanco(idFSEntryCod, idFSEntryDen);
					return false;
				}
			}

			//Validaciones Almacenamiento
			if (sAlmacenamientoTipoPedido != '') {
				if ((sAlmacenamientoTipoPedido == '1') && (sAlmacenamiento == '0')) {
					alert(mensajeNoAlmacenable);
					ponerCodArt_Den_Blanco(idFSEntryCod, idFSEntryDen);
					return false;
				}

				if ((sAlmacenamientoTipoPedido == '0') && (sAlmacenamiento == '1')) {
					alert(mensajeAlmacenamientoObligatorio);
					ponerCodArt_Den_Blanco(idFSEntryCod, idFSEntryDen);
					return false;
				}
			}

			//Validaciones Recepci�n
			if (sRecepcionTipoPedido != '') {
				if ((sRecepcionTipoPedido == '1') && (sRecepcion == '0')) {
					alert(mensajeNoRecepcionable);
					ponerCodArt_Den_Blanco(idFSEntryCod, idFSEntryDen);
					return false;
				}

				if ((sRecepcionTipoPedido == '0') && (sRecepcion == '1')) {
					alert(mensajeRecepcionObligatoria);
					ponerCodArt_Den_Blanco(idFSEntryCod, idFSEntryDen);
					return false;
				}
			}
		
			//si pasa las validaciones
			oArt.articuloCodificado =  true;
			oArt.articuloGenerico = bGenerico;
			oArt.Concepto = sConcepto;
			oArt.Almacenamiento = sAlmacenamiento;
			oArt.Recepcion = sRecepcion;
		
			if ((oDen != null) && (oDen.tipoGS == 118)) {
				oDen.setValue(sValor);
				oDen.articuloGenerico = bGenerico;
				oDen.Concepto = sConcepto;
				oDen.Almacenamiento = sAlmacenamiento;
				oDen.Recepcion = sRecepcion;
			}
		}
		
		/*	Si no existe el articulo y no tiene el check activado de Permitir a�adir nuevos articulos, 
		   Poner el codigo de articulo en blanco, visualizar mensaje		*/
		if ((!oArt.anyadir_art) && (!bExiste))
		{
			if ((oDen) && (oDen.tipoGS == 118))
				sValorAux = oDen.getDataValue();
			oArt.setValue("");		
			if ((oDen) && (oDen.tipoGS == 118)) {
				oDen.setValue(sValorAux);
				oDen.codigoArticulo="";
			}
			alert(sMensajeCOD);
		
		}else 
		{
			if ((oDen) && (oDen.tipoGS == 118)){
				oDen.codigoArticulo = oArt.getDataValue();
			}
		
			//Cargar el valor del ULTIMO PRECIO ADJUDICADO
        if ((idFSEntryPrec) && (!bGenerico)) {
				idFSEntryPrec = idFSEntryPrec.replace("__t","");	
				e = p.getElementById(idFSEntryPrec+"__tbl")
				if (e)
					oPrecio = e.Object
				if (oPrecio) {
					oPrecio.setValue(lPrecio);
					oPrecio.setDataValue(lPrecio);
				}
			}
		
			//Cargar el valor del ULTIMO PROVEEDOR ADJUDICADO
        if ((idFSEntryProv) && (!bGenerico)) {
				idFSEntryProv = idFSEntryProv.replace("__t","");	
				e = p.getElementById(idFSEntryProv+"__tbl")
				if (e)
					oProveedor = e.Object
				if (oProveedor) {
					if (sCodProv!='')
						oProveedor.setValue(sCodProv + " - " + sDenProv);
					else
						oProveedor.setValue('');
					oProveedor.setDataValue(sCodProv);
				}
			}
		
			//Cargar la Unidad del articulo seleccionado.
			if (idFSEntryUni) {
				idFSEntryUni = idFSEntryUni.replace("__t","");	
				e = p.getElementById(idFSEntryUni+"__tbl")
				if (e)
					oUnidad = e.Object
				if (oUnidad) {
					oUnidad.setValue(sDenUnidad);
					oUnidad.setDataValue(sUnidad);		
				}else
					//Si la Unidad esta oculta almacenarla en el atributo del articulo UnidadOculta el valor de la UNIDAD actualizado
					if (oArt) {			
						if (oArt.UnidadDependent) {
							oArt.UnidadDependent.value = sUnidad;
						}
					}
					if (oDen)
						if (oDen.UnidadDependent) {
							oDen.UnidadDependent.value = sUnidad;
						}
			}

			//Cargar en la cantidad el numero de decimales de la unidad del articulo seleccionado.
			if (idFSEntryCant) {
				idFSEntryCant = idFSEntryCant.replace("__t", "");
				e = p.getElementById(idFSEntryCant + "__tbl")
				if (e) {
					oCantidad = e.Object
					if (oCantidad) {
						oCantidad.NumeroDeDecimales = NumeroDeDecimales;
						oCantidad.UnidadOculta = sDenUnidad
						oCantidad.TipoRecepcion = sTipoRecepcion
						limiteDecimalesTextChanged(oCantidad.Editor, oCantidad.Editor.text, null)
						if (sTipoRecepcion == "1") {
						    oCantidad.setValue("1");
						}
					}
				}
			}	
	        	
			//Cargar la organizacion del Art�culo seleccionado
			if (idfsEntryOrg) {
				idfsEntryOrg = idfsEntryOrg.replace("__t","");	
				e = p.getElementById(idfsEntryOrg+"__tbl")
				if (e)
					oOrg = e.Object
				if (oOrg) {
					if (sCodOrg!='')
						oOrg.setValue(sCodOrg + " - " + sDenOrg);
					else
						oOrg.setValue('');
					oOrg.setDataValue(sCodOrg);
				}		
			}
		
			//Cargar el centro de organizacion del Art�culo seleccionado
			if (idfsEntryCentro) {
				idfsEntryCentro = idfsEntryCentro.replace("__t","");	
				e = p.getElementById(idfsEntryCentro+"__tbl")
				if (e)
					oCentro = e.Object
				if (oCentro) {
					if (sCodCentro!='')
						oCentro.setValue(sCodCentro + " - " + sDenCentro);
					else
						oCentro.setValue('');
					oCentro.setDataValue(sCodCentro);				
				}
            }

            //tipo solicitud 8 (pedido expres) o 9 (pedido negociado)
            if ((iTipoSolicitud == 8) || (iTipoSolicitud == 9)) {
                //Cargar la Unidad de Pedido
                if (sIDFSEntryUniPedido) {
				    sIDFSEntryUniPedido = sIDFSEntryUniPedido.replace("__t","");	
				    e = p.getElementById(sIDFSEntryUniPedido+"__tbl")
				    if (e)
					    oUnidadPedido = e.Object
				    if (oUnidadPedido) {
				        oUnidadPedido.setValue(sDenUnidadPedido);
				        oUnidadPedido.setDataValue(sUnidadPedido);
					}
					else {
					    //Si la Unidad esta oculta almacenarla en el atributo del articulo UnidadOculta el valor de la UNIDAD actualizado
					    if (oArt) {
					        if (oArt.UnidadPedidoDependent) {
					            oArt.UnidadPedidoDependent.value = sUnidad;
					        }
					    }
					    if (oDen)
					        if (oDen.UnidadPedidoDependent) {
					            oDen.UnidadPedidoDependent.value = sUnidad;
					        }
					}
	            }
                //tipo solicitud 9 (pedido negociado)
				//Pedidos express con par�metro mostrar negociados activo
				if (iTipoSolicitud == 9 || iTipoSolicitud == 8) {
                    //Cargar el PROVEEDOR AUNQUE EST� FUERA DEL DESGLOSE
                    if (sIDFSEntryProve) {
                        sIDFSEntryProve = sIDFSEntryProve.replace("__t", "");
                        e = p.getElementById(sIDFSEntryProve + "__tbl")
                        if (!e) {
                            //El proveedor puede estar a la misma altura que el desglose cuando este no es popup, pero si es popup, hay que mirar en su opener:                            
                            pp = window.parent.opener.window.document;
                            if (pp) {
                                e = pp.getElementById(sIDFSEntryProve + "__tbl")
                            }
                        }
                        if (e) {
                            oProveedor = e.Object
                        }
                        if (oProveedor) {
                            if (sCodProv != ''){
                                oProveedor.setValue(sCodProv + " - " + sDenProv);
                                oProveedor.setDataValue(sCodProv);
                            }

                            //Rellenamos la forma de pago:
                            //Si est� oculta:
                            if (sIDFSEntryFPagoHidden != "") {
                                oProveedor.FPagoOculta = sFPagoPROVUltADJ
                            }
                            //Si est� visible:
                            if (sIDFSEntryFPago != "") {
                                sIDFSEntryFPago = sIDFSEntryFPago.replace("__t", "");
                                e = p.getElementById(sIDFSEntryFPago + "__tbl");
                                if (!e) {
                                    pp = window.parent.opener.window.document;
                                    e = pp.getElementById(sIDFSEntryFPago + "__tbl");
                                }
                                if (e) {
                                    oFPago = e.Object;
                                }
                                if (oFPago) {
                                    oFPago.setDataValue(sFPagoPROVUltADJ);
                                    oFPago.setValue(sFPagoPROVUltADJ + " - " + sDenFPagoPROVUltADJ);
                                }
                            }

                        }
                    }
                }

            }
		}

		if (p.getElementById("bMensajePorMostrar"))
			p.getElementById("bMensajePorMostrar").value="0"
		window.close();
	}
	//idFSEntryCod:     DataEntry del codigo de art�culo.
	//idFSEntryDen:     DataEntry de la denominacion del art�culo.
	//idFSEntryPrec:    Dataentry del precio
	//idFSEntryUni:     Dataentry de la unidad
	//sIDFSEntryUniPedido:  Id den DataEntry de la unidad de pedido (s�lo si el tipo de solicitud es 8-Pedido Expres o 9-Pedido Negociado)
	function BorrarArticulo(idFSEntryCod, idFSEntryDen, idFSEntryPrec, idFSEntryUni, idFSEntryUniPedido) {
    	var e = null;
		var oArt = null;	
		var oDen = null;
		var oPrecio = null;

		p =window.parent.window.document;
		
		idFSEntryCod = idFSEntryCod.replace("__t","");	
		e = p.getElementById(idFSEntryCod+"__tbl")
		if (e)
			oArt = e.Object;	
			
		idFSEntryDen = idFSEntryDen.replace("__t","");
		e = p.getElementById(idFSEntryDen+"__tbl");
		if (e)
			oDen = e.Object ;
			
		
		if ((oArt != null) && (oArt.tipoGS == 119)) {
			oArt.setValue("");
			oArt.setDataValue("");
			//Borrar Precio s�lo si se elimina el c�digo del art�culo
			if (idFSEntryPrec) {
			    idFSEntryPrec = idFSEntryPrec.replace("__t", "");
			    e = p.getElementById(idFSEntryPrec + "__tbl")
			    if (e)
			        oPrecio = e.Object
			    if (oPrecio) {
			        oPrecio.setValue("");
			        oPrecio.setDataValue("");
			    }
			}
			//Borrar la Unidad del articulo solo si se elimina el art�culo
			if (idFSEntryUni) {
			    idFSEntryUni = idFSEntryUni.replace("__t", "");
			    e = p.getElementById(idFSEntryUni + "__tbl")
			    if (e)
			        oUnidad = e.Object
			    if (oUnidad) {
			        oUnidad.setValue("");
			        oUnidad.setDataValue("");
			    }
			}
            //Borrar la Unidad de Pedido solo si se elimina el art�culo.
			if (idFSEntryUniPedido) {
			    idFSEntryUniPedido = idFSEntryUniPedido.replace("__t", "");
			    e = p.getElementById(idFSEntryUniPedido + "__tbl")
			    if (e)
			        oUnidad = e.Object
			    if (oUnidad) {
			        oUnidad.setValue("");
			        oUnidad.setDataValue("");
			    }
			}
		}
		
		if ((oDen != null) && (oDen.tipoGS == 118)) {
			oDen.setValue("");
			oDen.setDataValue("");
			oDen.codigoArticulo = ""; 
		}
	}
	function ponerCodigoArticulo (idFSEntryCod,idFSEntryDen,bGenerico,bExiste) {
		var e = null;
		var oArt = null;
		var oDen = null;	
		var oCC = null;
	
		p =window.parent.window.document;
	
		idFSEntryCod = idFSEntryCod.replace("__t","");
		e = p.getElementById(idFSEntryCod+"__tbl");
		if (e)
			oArt = e.Object ;
	
		if (oArt) {
		    if ((bExiste) && (!bGenerico || isPedidoExpressNegociable)) {
				idFSEntryDen = idFSEntryDen.replace("__tden","");
				idFSEntryDen = idFSEntryDen.replace("__t","");
				e = p.getElementById(idFSEntryDen+"__tbl");
				if (e)
					oDen = e.Object ;
				if (oDen) 
					sValorAnt = oDen.getValue();			
				alert(sMensajeCOD);
				oArt.setValue("");					
				if (oDen) {
					oDen.setValue(sValorAnt);
					oDen.codigoArticulo="";
				}
			}
		}
		if (p.getElementById("bMensajePorMostrar"))
			p.getElementById("bMensajePorMostrar").value="0"
		window.close();
	}
	function ponerMaterial (IDControl, sGMN1, sGMN2, sGMN3, sGMN4, sDen,IDControlArt,IDControlDen,TodosNiveles) {
		var sDenAux = '';	
		var o = null;
		var oArt = null;	
		var oDen = null;	

		p =window.parent.window.document;

		//si el art�culo est� vac�o es que no ha cumplido las validaciones y no puede seleccionar el art�culo
		idFSEntryArt = IDControlArt.replace("__t", "");
		e = p.getElementById(idFSEntryArt + "__tbl");
		if (e)
			oArt = e.Object;
		if (oArt) {
			if (oArt.getValue() == '')
				return false;
		}
		
		idFSEntryMat= IDControl.replace("__t","");
		e = p.getElementById(idFSEntryMat+"__tbl");
		if (e)
			o = e.Object ;	
	
		s=""
		for (i=0;i<ilGMN1-sGMN1.length;i++)
			s+=" "
	
		sMat = s + sGMN1
		sDenAux +=  ' (' + sGMN1
		if (sGMN2)
		{
			s=""
			for (i=0;i<ilGMN2-sGMN2.length;i++)
				s+=" "
			sMat = sMat + s + sGMN2
            sDenAux += ' - ' + sGMN2
            if (TodosNiveles == 1) {
                sDenShort = sGMN1 + ' - ' + sGMN2 + ' - ' + sDen
            } else {
                sDenShort = sGMN2 + ' - ' + sDen
            }
		}
		else
			sDenShort = sGMN1 + ' - ' + sDen

		if (sGMN3)
		{
			s=""
			for (i=0;i<ilGMN3-sGMN3.length;i++)
				s+=" "
			sMat = sMat + s + sGMN3
            sDenAux += ' - ' + sGMN3
            if (TodosNiveles == 1) {
                sDenShort = sGMN1 + ' - ' + sGMN2 + ' - ' + sGMN3 + ' - ' + sDen
            } else {
                sDenShort = sGMN3 + ' - ' +  sDen
            }
		}
		if (sGMN4)
		{
			s=""
			for (i=0;i<ilGMN4-sGMN4.length;i++)
				s+=" "
			sMat = sMat + s + sGMN4
            sDenAux += ' - ' + sGMN4
            if (TodosNiveles == 1) {
                sDenShort = sGMN1 + ' - ' + sGMN2 + ' - ' + sGMN3 + ' - ' + sGMN4 + ' - ' +  sDen
            } else {
                sDenShort = sGMN4 + ' - ' + sDen
            }
		
		}
		sDenAux += ')'
		if (o) {
			o.setDataValue(sMat)
			o.setValue(sDenShort)	
			o.setToolTip(sDenAux)
		}else {
				idFSEntryArt= IDControlArt.replace("__t","");
				e = p.getElementById(idFSEntryArt+"__tbl");
				if (e)
					oArt = e.Object ;
	
				if (oArt) 
					oArt.Dependent.value = sMat;
				
				if (IDControlDen) {
					idFSEntryDen = IDControlDen.replace("__t","");
					e = p.getElementById(idFSEntryDen+"__tbl");
					if (e)
						oDen = e.Object ;
		
					if (oDen) 
						oDen.Dependent.value = sMat;
				}
			}
	
	}
	function ponerCodArt_Den_Blanco(idFSEntryCod,idFSEntryDen) {
		p =window.parent.window.document;
		//Poner el codigo de Articulo en blanco
		idFSEntryCod = idFSEntryCod.replace("__t","");	
		e = p.getElementById(idFSEntryCod+"__tbl")
		if (e)
			oEntry = e.Object
		
		if (oEntry) {
			oEntry.setValue("");	
	
		}
		//Poner la denominaci�n en blanco
		idFSEntryDen = idFSEntryDen.replace("__t","");	
		e = p.getElementById(idFSEntryDen+"__tbl")
		if (e)
			oEntry = e.Object		
		if (oEntry) 
			oEntry.setValue("");	
	
	
		}
	//Borra los presupuestos
	//Par�metro de entrada: Id del dataentry del cod del art�culo
	function borrarPresupuestos(idFSEntryCod) {
		var e = null;
		var oArt = null;	
		
		p =window.parent.window.document;
		idFSEntryCod = idFSEntryCod.replace("__t","");	
		e = p.getElementById(idFSEntryCod+"__tbl")
		if (e)
			oArt = e.Object;	
		if (oArt) {
			if (oArt.idCampoPadre) {
				var desglose = oArt.id.substr(0,oArt.id.search("_fsdsentry"))
				var index = oArt.id.substr(oArt.id.search("_fsdsentry_") + 11, oArt.id.search(oArt.idCampo) - oArt.id.search("_fsdsentry_") - 12)
				var oEntry = null;
				for (i=0;i<p.all(desglose + "_tblDesgloseHidden").rows[0].cells.length;i++)
				{
					s = p.all(desglose + "_tblDesgloseHidden").rows[0].cells[i].innerHTML
					re=/fsentry/
			
					x = s.search(re)
					y = s.search("__tbl")
			
					id = s.substr(x+7,y-x-7)
					if (id!="")
						oEntry = window.parent.fsGeneralEntry_getById(desglose + "_fsentry" + id)
									
					if (oEntry)
					{
						if ((oEntry.tipoGS==110) || (oEntry.tipoGS==111) || (oEntry.tipoGS==112) || (oEntry.tipoGS==113)) {
							var pres = oEntry.id
							oDSEntry = 	window.parent.fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + window.parent.fsGeneralEntry_getById(pres).idCampo)
										
							oDSEntry.setValue("");
							oDSEntry.setDataValue("");
						}
					}
					}
						
			}
		}			
	}	
</script>
<body>
	<form id="frmBuscador" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <CompositeScript>
            <Scripts>
                <asp:ScriptReference Path="../alta/js/jsAlta.js" />
            </Scripts>
        </CompositeScript>
    </asp:ScriptManager>
	</form>
</body>
</html>
