
    Public Class tablaexternaserver
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim iTabla As Integer = CInt(Request.QueryString("tabla"))
        Dim sArt As String = String.Empty
        If Request.QueryString("art").ToString() <> "" Then _
                sArt = Request.QueryString("art").ToString()
        Dim oTablaExterna As FSNServer.TablaExterna = FSNServer.Get_Object(GetType(FSNServer.TablaExterna))
        oTablaExterna.LoadDefTabla(iTabla, sArt, True)
        Dim oRowReg As DataRow = oTablaExterna.BuscarRegistro(Request.QueryString("valor").ToString(), sArt, True)
        Dim sValor As String
        Dim sValorMostrar As String
        Dim sScript As String
        Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"

        If Not oRowReg Is Nothing Then
            Dim sTipoClave As String = String.Empty

            For Each oRow As DataRow In oTablaExterna.DefTabla.Tables(0).Rows
                If UCase(oRow.Item("CAMPO")) = UCase(oTablaExterna.PrimaryKey) Then
                    sTipoClave = oRow.Item("TIPOCAMPO")
                    Exit For
                End If
            Next
            If sTipoClave = "FECHA" Then
                sValor = FormatDate(CType(oRowReg.Item(oTablaExterna.PrimaryKey), Date), FSNUser.DateFormat)
            Else
                sValor = oRowReg.Item(oTablaExterna.PrimaryKey)
            End If
            sValorMostrar = oTablaExterna.DescripcionReg(oRowReg, Idioma, FSNUser.DateFormat, FSNUser.NumberFormat)
            sScript = "window.parent.valor_externa_seleccionado('" & JSText(Request.QueryString("fsEntryID")) & "', '" & JSText(sValor) & "', '" & JSText(sValorMostrar) & "', '" & JSText(IIf(sArt Is Nothing, "", sArt)) & "');"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "ValorTablaExternaAct", sScript)
        Else
            Dim sMensaje As String = String.Empty
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.TablaExterna
            sMensaje = oTablaExterna.Nombre & ControlChars.CrLf & Textos(1)
            sScript = "window.parent.errorval_externa('" & JSText(Request.QueryString("fsEntryID")) & "', '" & JSText(sMensaje) & "');"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "ValorTablaExternaAct", sScript)
        End If
    End Sub

End Class

