﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AsignacionUsuarios.aspx.vb" Inherits="Fullstep.FSNWeb.AsignacionUsuarios" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>" lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">

<head runat="server">
    <title></title>
    <script type="text/javascript">
        /*Descripcion:Deja en la caja de texto el valor del proveedor seleccionado en el buscador de proveedores
        parametros entrada:
            sCIF: CIF proveedor
            sProveCod:= Codigo proveedor
            sProveDen: Denominacion proveedor
        Llamada desde:proveedores.aspx
        Tiempo ejecucion:0px        */
      
        function prove_seleccionado2(sCIF, sProveCod, sProveDen) {
            if (sProveCod != "") {
                document.getElementById("<%=txtProveedor.ClientID%>").value = sProveCod + " - " + sProveDen
                window.document.form1.DenProve.value = sProveDen
                window.__doPostBack('BuscadorProveedor', sProveCod)
            }
        }

        /*
        **  Returns the caret (cursor) position of the specified text field.
        **  Return value range is 0-oField.length.
        */
        function doGetCaretPosition(oField) {

            // Initialize
            var iCaretPos = 0;

            // IE Support
            if (document.selection) {

                // Set focus on the element
                oField.focus();

                // To get cursor position, get empty selection range
                var oSel = document.selection.createRange();

                // Move selection start to 0 position
                oSel.moveStart('character', -oField.value.length);

                // The caret position is selection length
                iCaretPos = oSel.text.length;
            }

            // Firefox support
            else if (oField.selectionStart || oField.selectionStart == '0')
                iCaretPos = oField.selectionStart;

            // Return results
            return (iCaretPos);
        }
        
        function IsNumeric(expression)   {  
            return (String(expression).search(/^\d+$/) != -1);  
        }

        function ValidarRangoUsuariosPaste() {
            var texto = window.clipboardData.getData('Text');
            //quita los espacios en blanco
            texto =  texto.replace(/^\s+/i, '').replace(/\s+$/i, '');

            var str = '';
            for (var i = 0; i < texto.length; i++) {

                if ((texto.charCodeAt(i) >= 48 && texto.charCodeAt(i) <= 57) || (texto.charCodeAt(i) == 45) || (texto.charCodeAt(i) == 44)) {

                    if ((texto.charCodeAt(i) == 45) || (texto.charCodeAt(i) == 44)) {
                        if (i > 0) {
                            if (texto.charCodeAt(i - 1) >= 48 && texto.charCodeAt(i - 1) <= 57)
                                str += texto.charAt(i);
                        }
                    } else
                        str += texto.charAt(i);
                }

            }

            if (texto != str) event.returnValue = false;
        }

        function ValidarRangoUsuarios(elemento, evento) {
            var CharKeyCode;
            if (!evento.charCode) { //IE
                CharKeyCode = evento.keyCode;
            }
            else {//Firefox
                CharKeyCode = evento.charCode;
            }

            if ((CharKeyCode >= 48 && CharKeyCode <= 57) || (CharKeyCode == 45) || (CharKeyCode == 44)) {
                if ((CharKeyCode == 44) || (CharKeyCode == 45)) {
                    var CharPosition = doGetCaretPosition(elemento);
                    if (CharPosition == 0) {
                        return false;
                    } else {
                        caracterAnterior = elemento.value.charAt(CharPosition - 1);
                        if (IsNumeric(caracterAnterior))
                            return true
                        else
                            return false;
                    }

                }
                return true;
            } else
                return false;
        }

        function cambioCategoria(elemento) {
            var arrAux = '';
            indice = -1;
            if (elemento.id != '') {
                arrAux = elemento.id.split("_");
                indice = arrAux[1];
            }
            if (indice != -1) {
                indiceCategoria = elemento.selectedIndex;
                if (indiceCategoria > 0) {
                    sResultado = indice.toString() + '_' + indiceCategoria
                    window.__doPostBack('SeleccionCategoria', sResultado)
                } else {// poner en blanco coste/hora
                    oElem = window.document.getElementById("txtCosteHora_" + indice)
                    if (oElem)
                        oElem.value = ""
                }
            }

        }

        function cambioUsuario(elemento) {
            var arrAux = '';
            indice = -1;
            if (elemento.id != '') {
                arrAux = elemento.id.split("_");
                indice = arrAux[1];
            }
            if (indice != -1) {
                indiceCategoria = elemento.selectedIndex;
                if (indiceCategoria > 0) {
                    sResultado = indice.toString() + '_' + indiceCategoria
                    window.__doPostBack('SeleccionUsuario', sResultado)
                } else {
                    //poner el blanco la categoria
                    oElem = window.document.getElementById("ddlCategoria_" + indice)
                    if (oElem) {
                        oElem.selectedIndex = 0;
                    }                    
                    // poner en blanco coste/hora
                    oElem = window.document.getElementById("txtCosteHora_" + indice)
                    if (oElem)
                        oElem.value = ""
                }
            }

        }

        function PonerCategoriayValorCoste(elementoIdComboCategoria, indiceCategoria,elementoId, valor) {
            oElem = window.document.getElementById(elementoIdComboCategoria)
            //Categoria Usuario
            if (oElem)
                oElem.selectedIndex = indiceCategoria
            //Coste/hora
            oElem = window.document.getElementById(elementoId)
            if (oElem)
                oElem.value = valor
        }

        function PonerValorCoste(elementoId, valor) {
            oElem = window.document.getElementById(elementoId)
            if (oElem)
                oElem.value = valor
        }

        function EliminarLinea(elemento, sMsg) {
            var arrAux = '';
            indice = -1;
            if (elemento.id != '') {
                arrAux = elemento.id.split("_");
                indice = arrAux[1];
            }
            if (indice != -1) {
                if (confirm(sMsg) == true)
                    window.__doPostBack('EliminarLinea', indice);
                
            }

        }

        function DevolverSeleccionAsignacionUsuarios(sValor) {
            p = window.opener
            p.DevolverSeleccionAsignacionUsuarios(window.document.form1.IDCONTROL.value, sValor)
            window.close();

        }
        function CerrarVentana() {
            window.close();
        }


        var eventoPaste = false;

        //Función que 
        //1. Impide escribir caracteres no numéricos
        //2. Limita el número de decimales a los propios de cada unidad en los controles textbox
        //elemento->El control
        //evento->El evento
        //vdecimalfmt -> Separador decimales
        //vthousanfmt -> Separador miles
        //vprecisionfmt -> nº decimales
        function limiteDecimalesTextBox(elemento, evento, vdecimalfmt, vthousanfmt, vprecisionfmt, eventPaste) {

            var mascaraOK = 1;
            var longitudTexto=0;
            if (eventPaste == true) {
                var textoPegado = window.clipboardData.getData('Text');
                var CharPosition = doGetCaretPosition(elemento);
                var selectedTextLength = document.selection.createRange().text.length;
                var texto = elemento.value.substring(0, CharPosition) + textoPegado + elemento.value.slice(CharPosition + selectedTextLength);
                longitudTexto = textoPegado.length;
                //Sólo separador de decimales, un guión y números
                //El separador de miles no lo permitimos, para evitar confusiones
                //var regEx = new RegExp('[^\\d^\\' + vdecimalfmt + '^\\' + vthousanfmt + '^\\-]', 'g');
                var regEx = new RegExp('[^\\d^\\' + vdecimalfmt + '^\\' + '^\\-]', 'g');
                if (!allowInString(texto, regEx)) {
                    //"Número incorrecto.\nSeparador decimal: " + vdecimalfmt + "\nSeparador de miles: " + vthousanfmt
                    //            if (errMsg) {
                    //                alert(errMsg);
                    //            }
                    elemento.focus();
                    //return false;
                    mascaraOK = 0;
                }

                //Guion sólo una vez y el primer carácter
                var regEx = new RegExp('\\-', "g");
                var arrElementos = texto.match(regEx);
                if (arrElementos != null) {
                    if (arrElementos.length > 1) {
                        mascaraOK = 0;
                    }
                    else {
                        if (texto.indexOf('-') >= 0) {
                            mascaraOK = 0;
                        }
                    }
                }
                //Decimal sólo una vez
                var regEx = new RegExp('\\' + vdecimalfmt, "g");
                var arrElementos = texto.match(regEx);
                if (arrElementos != null) {
                    if (arrElementos.length > 1) {
                        //return false;
                        mascaraOK = 0;
                    }
                }

            } // Fin del if paste
            else {
            
                //Impide escribir caracteres no numéricos
                mascaraOK = mascaraNumero(elemento, vdecimalfmt, vthousanfmt, evento);
                longitudTexto = elemento.value.length;
                //Limita el número de decimales a los propios de cada unidad en los controles textbox
                var CharKeyCode;
                if (!evento.charCode) { //IE
                    CharKeyCode = String.fromCharCode(evento.keyCode);
                }
                else {//Firefox
                    CharKeyCode = String.fromCharCode(evento.charCode);
                }
                var CharPosition = doGetCaretPosition(elemento);
                var selectedTextLength = document.selection.createRange().text.length;
                //El texto se compone de:
                // Lo que contiene el textbox
                // Más lo que escribe el usuario en el keypress (ubicado en el punto donde corresponda)
                // Menos lo que se borrar al hacer el keypress (si había texto seleccionado)
                //Rdo:
                var texto = elemento.value.substring(0, CharPosition) + CharKeyCode + elemento.value.slice(CharPosition + selectedTextLength);
            }
            
            
            if (mascaraOK == 0) {
                return false;
            }
            else {
                if (selectedTextLength == longitudTexto)
                    return true
                else {
                    //LIMITE DE DECIMALES
                    //var avisoMaxDecimales = elemento.getAttribute("avisoDecimales");
                    if (vprecisionfmt != null && vprecisionfmt >= 0 && vdecimalfmt != null && texto != '') {
                        if (texto.indexOf(vdecimalfmt) >= 0) {
                            var textoEntero = texto.substr(0, (texto.indexOf(vdecimalfmt) != -1) ? texto.indexOf(vdecimalfmt) : texto.length);
                            var textoDecimales = texto.slice(texto.indexOf(vdecimalfmt) + 1);
                            if (textoDecimales.length > vprecisionfmt) {
                                return false;
                            }
                            else {
                                return true;
                            }
                        }
                    }
                    eventoPaste = false;
                    return true;
                }
            }
        }

        //evita el tecleo de caracteres incorrectos cuando se introduce un número
        //se debe incluir en el onkeypress del input que se quiera limitar
        function mascaraNumero(elemento, vdecimalfmt, vthousanfmt, evento) {
            var CharKeyCode;
            if (!evento.charCode) { //IE
                CharKeyCode = evento.keyCode;
            }
            else {//Firefox
               CharKeyCode = evento.charCode;
            }

            if (CharKeyCode == vdecimalfmt.charCodeAt(0) || (CharKeyCode >= 48 && CharKeyCode <= 57) || CharKeyCode == 45) {
                //Cuando el carácter introducido es "-" vamos a controlar que sólo se pueda introducir cuando es el primer carácter en el control
                if (CharKeyCode == 45) {
                    var CharPosition = doGetCaretPosition(elemento);
                    if (CharPosition == 0) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                }
                //El decimal sólo se puede introducir una vez
                if (CharKeyCode == vdecimalfmt.charCodeAt(0)) {
                    var CharPosition = doGetCaretPosition(elemento);
                    var CharKey = String.fromCharCode(CharKeyCode);
                    var selectedTextLength = document.selection.createRange().text.length;
                    var texto = elemento.value.substring(0, CharPosition) + CharKey + elemento.value.slice(CharPosition + selectedTextLength);
                    var regEx = new RegExp('\\' + vdecimalfmt, "g");
                    //var regEx = '/\\' + vdecimalfmt + '/g';
                    var arrElementos = texto.match(regEx);
                    if (arrElementos != null) {
                        if (arrElementos.length > 1) {
                            return 0;
                        }
                        else {
                            return 1;
                        }
                    }
                }
                return 1;
            }
            else {
                return 0;
            }
        }


        //Función que añade el separador de miles cuando el numero sea mayor o igual a 1000
        function addThousandSeparator(elemento, vdecimalfmt, vthousanfmt, vprecisionfmt, evento) {
            var thousandRegex = new RegExp('\\' + vthousanfmt, "g");
            var textInteger = elemento.value.substr(0, (elemento.value.indexOf(vdecimalfmt) != -1) ? elemento.value.indexOf(vdecimalfmt) : elemento.value.length);
            var textInteger = textInteger.replace(thousandRegex, "");
            var textDecimal = elemento.value.slice((elemento.value.indexOf(vdecimalfmt) != -1) ? elemento.value.indexOf(vdecimalfmt) : elemento.value.length);
            if (textInteger.length == 0) {
                textInteger = '0';
            }
            if (textDecimal.length == 0 || textDecimal == vdecimalfmt) {
                var zeros = '';
                for (var i = 1; i <= vprecisionfmt; i++) {
                    zeros += '0';
                }
                textDecimal = vdecimalfmt + zeros;
            }
            if (textInteger.length > 3) {
                for (i = textInteger.length - 3; i > 0; i -= 3) {
                    textInteger = textInteger.substr(0, i) + vthousanfmt + textInteger.slice(i);
                }
            }
            elemento.value = textInteger + textDecimal;
        }



        //Función que comprueba que el número tiene el formato adecuado
        //Si es así y si la función se está aplicando a un control precio o cantidad de una línea de pedido, actualiza los datos en pantalla de precio total de la línea y de precio total a nivel de orden
        //elemento = Control modificado y que ha lanzado el evento.
        //vdecimalfmt = Separador de decimales
        //vthousanfmt = Separador de miles
        //vprecisionfmt = Número de decimales del control que ha lanzado el evento
        function validarNumero(elemento, vdecimalfmt, vthousanfmt, vprecisionfmt) {
            var rdo = false;
            if (!elemento.value) {
                //return true;
                rdo = true;
            }
            valor = elemento.value;
            if (valor.length == 0) {
                //return true;
                rdo = true;
            }

            if (rdo != true) {
                //En el número sólo puede haber numeros, separador de decimales, separador de miles o el signo '-'
                var regEx = new RegExp('[^\\d^\\' + vdecimalfmt + '^\\' + vthousanfmt + '^\\-]', 'g');
                if (!allowInString(valor, regEx)) {

                    //"Número incorrecto.\nSeparador decimal: " + vdecimalfmt + "\nSeparador de miles: " + vthousanfmt
                    if (errMsg) {
                        alert(errMsg);
                    }
                    elemento.focus();
                    //return false;
                    rdo = false;
                }
            }
        }


        //Función que devuelve true si el texto cumple con la expresión regular pasada. Si no, devuelve false
        function allowInString(InString, RefRegEx) {
            if (InString.length == 0) return (false);
            if (InString.match(RefRegEx) != null) {
                return false;
            }
            return true;
        }


        //Validado del paste
        function validarpastenum(elemento, evento, vdecimalfmt, vthousanfmt, vprecisionfmt) {
            eventPaste = true;
            return limiteDecimalesTextBox(elemento, evento, vdecimalfmt, vthousanfmt, vprecisionfmt, true)
        }



    </script>
</head>
<body style="background-color:#F1F1F1">
    <form id="form1" runat="server">
    <input type="hidden" id="IDCONTROL" runat="server" />
    <input type="hidden" id="DenProve" runat="server" />
    
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        
    <fsn:FSNPanelInfo ID="FSNPanelDatosProveedor" runat="server" ServicePath="~/App_Pages/PMWEB/ConsultasPMWEB.asmx" ServiceMethod="Obtener_DatosProveedor" TipoDetalle="1"></fsn:FSNPanelInfo>
        
    <div style="padding-left:5px;padding-top:5px">
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblSeleccioneProveedor" runat="server" Text="Seleccione el proveedor:" CssClass="captionBlue" Font-Bold="true"></asp:Label>
                </td>
                <td>
                    <table id="tblCC" runat="server" style="background-color:White;width:300px;height:20px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:TextBox ID="txtProveedor" runat="server" Width="275px" BorderWidth="0px"></asp:TextBox>
                        </td>
                        <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
                            <asp:ImageButton id="imgProveedorLupa" runat="server" />
                        </td>
                    </tr>                
                    </table>
                    <asp:Label ID="lblProveedor" runat="server" Visible="false" CssClass="Normal"></asp:Label>
                    <asp:Image ID="imgInfProv" runat="server" Visible="false" />
                    
                </td>
            </tr>
        </table>
        
        <asp:UpdatePanel ID="updPnlUsuarios" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width:100%">
                    <tr>
                        <td>
                            <table ID="tblUsuarios" runat="server" style="width:650px" cellpadding="0" cellspacing="0" border="0">
                                <tr style="background-color:#DDDDDD;height:20px">
                                    <td colspan="3" style="padding-left:5px">
                                        <asp:Label ID="lblCabecera1" runat="server" CssClass="captionBlue" Text="dUsuarios asignados a Diseño Técnico"></asp:Label>
                                    </td>
                                </tr>
                                <tr style="background-color:White;height:20px">
                                    <td style="width:200px;text-align:left;border-bottom:solid 1px #DDDDDD;padding-left:5px">
                                        <asp:Label ID="lblCab2Usuario" CssClass="captionBlue" runat="server" Text="dUsuario"></asp:Label>
                                    </td>
                                    <td style="width:190px;text-align:left;border-bottom:solid 1px #DDDDDD;padding-left:10px">
                                        <asp:Label ID="lblCab2Categoria" CssClass="captionBlue" runat="server" Text="dCategoria"></asp:Label>
                                    </td>
                                    <td style="width:155px;text-align:left;border-bottom:solid 1px #DDDDDD">
                                        <asp:Label ID="lblCab2CosteHora" CssClass="captionBlue" runat="server" Text="dCoste/hora"></asp:Label>
                                    </td>
                                </tr>
                                <tr style="width:650px;height:150px;background-color:White">
                                    <td colspan="3">
                                        <div style="overflow:scroll;height:150px;width:100%;overflow-x: hidden;">
                                            <table ID="tblUsuariosIN" runat="server" width="100%"></table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top">
                            <asp:ImageButton ID="imgAnyadirLinea" runat="server" Enabled="false" />
                        </td>
                    </tr>
                    
                
                </table>

                
                <asp:HiddenField ID="hdnRows" runat="server"/>
        </ContentTemplate>
        </asp:UpdatePanel>
        
        <br />
        
        <table>
        <tr>
            <td>
                <asp:Label ID="lblRangoUsuarios" runat="server" CssClass="captionBlueSmall" Text="dSi desea asignar estos usuarios a otras sub-actividades, introdúzcalas separadas por compas. Por ejemplo:1,4,5-9"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtRangoUsuarios" runat="server" Width="400px"></asp:TextBox>
            </td>
        </tr>
        </table>
        
        <br />
        
        <table style="width:100%">
            <tr id="filaAceptarCancelar" runat="server">
                <td align="right">
                    <input id="cmdAceptar" type="button" runat="server" value="Aceptar" />
                    
                </td>
                <td align="left">
                     <input id="cmdCancelar" type="button" runat="server" value="Cancelar" />
                </td>
            </tr>
            <tr id="filaCerrar" runat="server">
                <td align="center" colspan="2">
                    <input id="cmdCerrar" type="button" runat="server" value="Cerrar" />
                </td>
            </tr>
        </table>
           
    </div>
    </form>
</body>
</html>
