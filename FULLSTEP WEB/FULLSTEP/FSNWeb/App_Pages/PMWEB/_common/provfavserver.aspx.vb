
    Public Class provfavserver
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
    Protected WithEvents UltraWebGrid1 As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
    Protected WithEvents chkMostrarMat As System.Web.UI.WebControls.CheckBox
    Protected WithEvents uwgProvFav As Infragistics.WebUI.UltraWebGrid.UltraWebGrid

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.ProvFavServer

        If Not Me.IsPostBack Then
            chkMostrarMat.Text = Textos(0)
            If FSNUser.PMMostrarDefProvMat Then
                Me.chkMostrarMat.Checked = True
            Else
                Me.chkMostrarMat.Checked = False
            End If
        End If

        Dim oProvesFavoritos As FSNServer.ProveedoresFavoritos

        Dim sScript As String

        Dim sGMN1 As String = ""
        Dim sGMN2 As String = ""
        Dim sGMN3 As String = ""
        Dim sGMN4 As String = ""

        Dim ilGMN1 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN1
        Dim ilGMN2 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN2
        Dim ilGMN3 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN3
        Dim ilGMN4 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN4


        Dim sMat As String = Request("Material")

        Dim sMatOrig As String = sMat


        Dim arrMat(4) As String
        Dim lLongCod As Integer
        Dim i As Integer
        For i = 1 To 4
            arrMat(i) = ""
        Next i

        i = 1
        While Trim(sMat) <> ""
            Select Case i
                Case 1
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                Case 2
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                Case 3
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                Case 4
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
            End Select
            arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
            sMat = Mid(sMat, lLongCod + 1)
            i = i + 1
        End While


        sGMN1 = arrMat(1)
        sGMN2 = arrMat(2)
        sGMN3 = arrMat(3)
        sGMN4 = arrMat(4)

        oProvesFavoritos = FSNServer.Get_Object(GetType(FSNServer.ProveedoresFavoritos))
        If Acceso.gbUsar_OrgCompras = True And Request("OrgCompras") <> "" Then
            If Me.chkMostrarMat.Checked = True Then
                oProvesFavoritos.LoadData(FSNUser.Cod, Request("OrgCompras"), sGMN1, sGMN2, sGMN3, sGMN4)
            Else
                oProvesFavoritos.LoadData(FSNUser.Cod, Request("OrgCompras"))
            End If
        Else
            If Me.chkMostrarMat.Checked = True Then
                oProvesFavoritos.LoadData(FSNUser.Cod, , sGMN1, sGMN2, sGMN3, sGMN4)
            Else
                oProvesFavoritos.LoadData(FSNUser.Cod)
            End If
        End If

        Dim oGrid As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
        oGrid = Me.uwgProvFav

        oGrid.DataSource = oProvesFavoritos.Data
        oGrid.DataBind()
        If oGrid.Bands.Count = 0 Then
            oGrid.Bands.Add(New Infragistics.WebUI.UltraWebGrid.UltraGridBand)
        End If
        Me.uwgProvFav.Columns.FromKey("COD").Hidden = True
        Me.uwgProvFav.Columns.FromKey("NIF").Hidden = True
    End Sub
    Private Sub chkMostrarMat_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMostrarMat.CheckedChanged

    End Sub
    Private Sub uwgProvFav_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgProvFav.InitializeRow
        e.Row.Cells.FromKey("DEN").Value = "<a class='aPMWeb' onclick=""SeleccionarProveedor('" & JSText(e.Row.Cells.FromKey("COD").Value) & "','" & JSText(e.Row.Cells.FromKey("DEN").Value) & "');return false;"">" & e.Row.Cells.FromKey("DEN").Value & "</a>"
    End Sub
End Class