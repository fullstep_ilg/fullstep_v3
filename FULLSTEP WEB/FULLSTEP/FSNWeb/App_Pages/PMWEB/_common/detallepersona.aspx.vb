
    Public Class detallepersona
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblCod As System.Web.UI.WebControls.Label
        Protected WithEvents lblCodBD As System.Web.UI.WebControls.Label
        Protected WithEvents lblNombre As System.Web.UI.WebControls.Label
        Protected WithEvents lblNombreBD As System.Web.UI.WebControls.Label
        Protected WithEvents lblApe As System.Web.UI.WebControls.Label
        Protected WithEvents lblApeBD As System.Web.UI.WebControls.Label
        Protected WithEvents lblCargo As System.Web.UI.WebControls.Label
        Protected WithEvents lblCargoBD As System.Web.UI.WebControls.Label
        Protected WithEvents lblTfno As System.Web.UI.WebControls.Label
        Protected WithEvents lblTfnoBD As System.Web.UI.WebControls.Label
        Protected WithEvents lblFax As System.Web.UI.WebControls.Label
        Protected WithEvents lblFaxBD As System.Web.UI.WebControls.Label
        Protected WithEvents lblMail As System.Web.UI.WebControls.Label
        Protected WithEvents lblMailBD As System.Web.UI.WebControls.Label
        Protected WithEvents lblDep As System.Web.UI.WebControls.Label
        Protected WithEvents lblDepBD As System.Web.UI.WebControls.Label
        Protected WithEvents cmdCerrar As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents lblOrg As System.Web.UI.WebControls.Label
        Protected WithEvents lblOrgBD As System.Web.UI.WebControls.Label

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.DetallePersona

        Me.lblCod.Text = Textos(0)
        Me.lblNombre.Text = Textos(1)
        Me.lblApe.Text = Textos(2)
        Me.lblCargo.Text = Textos(3)
        Me.lblTfno.Text = Textos(4)
        Me.lblFax.Text = Textos(5)
        Me.lblMail.Text = Textos(6)
        Me.lblDep.Text = Textos(7)
        Me.lblOrg.Text = Textos(8)
        cmdCerrar.Value = Textos(9)

        'carga los datos de la persona:        
        Dim oPersona As FSNServer.Persona = FSNServer.Get_Object(GetType(FSNServer.Persona))
        oPersona.LoadData(Request("CodPersona"), True)
        If Not oPersona.Codigo Is Nothing Then
            With oPersona
                lblCodBD.Text = .Codigo
                lblNombreBD.Text = .Nombre
                lblApeBD.Text = .Apellidos
                lblDepBD.Text = .DenDepartamento
                lblOrgBD.Text = .UONs
                lblCargoBD.Text = .Cargo
                lblTfnoBD.Text = .Telefono
                lblFaxBD.Text = .Fax
                lblMailBD.Text = .EMail
            End With
        End If
    End Sub

    End Class

