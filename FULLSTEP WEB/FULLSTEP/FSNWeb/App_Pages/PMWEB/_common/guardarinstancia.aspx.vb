Imports System.IO
Imports System.Data.SqlClient
Imports Fullstep.FSNServer
Imports System.Web.Script.Serialization

Public Class guardarinstancia
	Inherits FSNPage

#Region " Web Form Designer Generated Code "
	'This call is required by the Web Form Designer.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub
	Protected WithEvents ContenedorAvisos As System.Web.UI.HtmlControls.HtmlGenericControl
	Protected WithEvents lblEtapas As System.Web.UI.WebControls.Label
	Protected WithEvents lblRoles As System.Web.UI.WebControls.Label
	Protected WithEvents ugtxtRoles As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
	Protected WithEvents lblComentarios As System.Web.UI.WebControls.Label
	Protected WithEvents lblTrasladoProv As System.Web.UI.WebControls.Label
	Protected WithEvents lblMsg1 As System.Web.UI.WebControls.Label
	Protected WithEvents lblCIF As System.Web.UI.WebControls.Label
	Protected WithEvents txtCIF As System.Web.UI.WebControls.TextBox
	Protected WithEvents lblCod As System.Web.UI.WebControls.Label
	Protected WithEvents txtCod As System.Web.UI.WebControls.TextBox
	Protected WithEvents lblDen As System.Web.UI.WebControls.Label
	Protected WithEvents txtDen As System.Web.UI.WebControls.TextBox
	Protected WithEvents lblTrasladoUsu As System.Web.UI.WebControls.Label
	Protected WithEvents lblMsg2 As System.Web.UI.WebControls.Label
	Protected WithEvents lblUO As System.Web.UI.WebControls.Label
	Protected WithEvents ugtxtUO_wdd As Infragistics.Web.UI.ListControls.WebDropDown
	Protected WithEvents lblDep As System.Web.UI.WebControls.Label
	Protected WithEvents ugtxtDEP_ As Infragistics.Web.UI.ListControls.WebDropDown
	Protected WithEvents lblNombre As System.Web.UI.WebControls.Label
	Protected WithEvents txtNombre As System.Web.UI.WebControls.TextBox
	Protected WithEvents lblApe As System.Web.UI.WebControls.Label
	Protected WithEvents txtApe As System.Web.UI.WebControls.TextBox
	Protected WithEvents lblOrigen As System.Web.UI.WebControls.Label
	Protected WithEvents lblUsuario As System.Web.UI.WebControls.Label
	Protected WithEvents lblComent As System.Web.UI.WebControls.Label
	Protected WithEvents lblTraslado As System.Web.UI.WebControls.Label
	Protected WithEvents lblContacto As System.Web.UI.WebControls.Label
	Protected WithEvents ugtxtContacto_ As Infragistics.Web.UI.ListControls.WebDropDown
	Protected WithEvents lblIntroFec As System.Web.UI.WebControls.Label
	Protected WithEvents GeneralFecha As DataEntry.GeneralEntry
	Protected WithEvents lblComentTrasladoUsu As System.Web.UI.WebControls.Label
	Protected WithEvents Bloque As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents Instancia As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents PedidoDirecto As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents ComentAltaNoConf As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents Guardar As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents Accion As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents idAccion As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents Solicitud As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents Formulario As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents Workflow As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents Notificar As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents total As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents EstadoInstancia As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents CodPer As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents txtcadenaserror1 As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents txtcadenaserror2 As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents txtcadenaserror3 As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents txtcadenaserror4 As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents hidVolver As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents Version As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents divDevolverSiguientesEtapas As System.Web.UI.HtmlControls.HtmlGenericControl
	Protected WithEvents tblGeneral As System.Web.UI.HtmlControls.HtmlTable
	Protected WithEvents txtComent As System.Web.UI.HtmlControls.HtmlTextArea
	Protected WithEvents txtaEtapas As System.Web.UI.HtmlControls.HtmlTextArea
	Protected WithEvents cmdAceptar As System.Web.UI.HtmlControls.HtmlInputButton
	Protected WithEvents cmdCancelar As System.Web.UI.HtmlControls.HtmlInputButton
	Protected WithEvents divTraslado As System.Web.UI.HtmlControls.HtmlGenericControl
	Protected WithEvents cmdBuscar As System.Web.UI.HtmlControls.HtmlInputButton
	Protected WithEvents cmdBuscar2 As System.Web.UI.HtmlControls.HtmlInputButton
	Protected WithEvents cmdContinuarTraslado As System.Web.UI.HtmlControls.HtmlInputButton
	Protected WithEvents cmdCancelarTraslado As System.Web.UI.HtmlControls.HtmlInputButton
	Protected WithEvents divDevolucion As System.Web.UI.HtmlControls.HtmlGenericControl
	Protected WithEvents tblDatos As System.Web.UI.HtmlControls.HtmlTable
	Protected WithEvents txtComentDevolucion As System.Web.UI.HtmlControls.HtmlTextArea
	Protected WithEvents cmdAceptarDevolucion As System.Web.UI.HtmlControls.HtmlInputButton
	Protected WithEvents cmdCancelarDevolucion As System.Web.UI.HtmlControls.HtmlInputButton
	Protected WithEvents divtrasladoUsu As System.Web.UI.HtmlControls.HtmlGenericControl
	Protected WithEvents tblDatosTrasladoUsu As System.Web.UI.HtmlControls.HtmlTable
	Protected WithEvents txtComentTrasladoUsu As System.Web.UI.HtmlControls.HtmlTextArea
	Protected WithEvents CmdAceptarTrasladoUsu As System.Web.UI.HtmlControls.HtmlInputButton
	Protected WithEvents CmdCancelarTrasladoUsu As System.Web.UI.HtmlControls.HtmlInputButton
	Protected WithEvents Menu1 As Global.Fullstep.FSNWeb.MenuControl
	Protected WithEvents mi_body As Global.System.Web.UI.HtmlControls.HtmlGenericControl
	Protected WithEvents cmdAceptarControlPartida As System.Web.UI.HtmlControls.HtmlInputButton
	Protected WithEvents cmdCancelarControlPartida As System.Web.UI.HtmlControls.HtmlInputButton
	Protected WithEvents lblMsjControlPartidas1 As System.Web.UI.WebControls.Label
	Protected WithEvents tblControlImportes As Global.System.Web.UI.HtmlControls.HtmlTable
	Protected WithEvents tblControlIN As Global.System.Web.UI.HtmlControls.HtmlTable
	Protected WithEvents ControlPartidas As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents UsuarioConfirmaAccion As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents lblControlDisponible As System.Web.UI.WebControls.Label
	Protected WithEvents ConfiguracionGS As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents tipoSolicitud As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents hTipoVisor As System.Web.UI.HtmlControls.HtmlInputHidden
	Private designerPlaceholderDeclaration As System.Object
	Private m_sCualquiera As String
	Protected WithEvents txtIdiMsgbox As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents IdRowContacto As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents txtCualquiera As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents SelectedUON As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents SelectedDEP As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents SelectedNOM As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents SelectedAPE As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents SelectedPOSTBACK As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents SelectedItem As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents SelectedItem2 As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents SelectedCOD As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents AvisoBloqueo As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents idRefSol As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents NewInstancia As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents BloqueDestinoWS As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents RolActual As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents NoConformidad As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents Certificado As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents Contrato As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents CodContrato As System.Web.UI.HtmlControls.HtmlInputHidden
	Protected WithEvents NombreXML As System.Web.UI.HtmlControls.HtmlInputHidden
	Private m_sMsgboxAccion(3) As String
	Protected WithEvents divCabecera As System.Web.UI.HtmlControls.HtmlGenericControl
	Protected WithEvents FSNPageHeader As Global.Fullstep.FSNWebControls.FSNPageHeader
	Protected WithEvents lblLitImporte As System.Web.UI.WebControls.Label
	Protected WithEvents lblLitCreadoPor As System.Web.UI.WebControls.Label
	Protected WithEvents lblLitEstado As System.Web.UI.WebControls.Label
	Protected WithEvents lblLitFecAlta As System.Web.UI.WebControls.Label
	Protected WithEvents lblLitTipo As System.Web.UI.WebControls.Label
	Protected WithEvents lblIDInstanciayEstado As System.Web.UI.WebControls.Label
	Protected WithEvents lblImporte As System.Web.UI.WebControls.Label
	Protected WithEvents lblCreadoPor As System.Web.UI.WebControls.Label
	Protected WithEvents lblEstado As System.Web.UI.WebControls.Label
	Protected WithEvents lblFecAlta As System.Web.UI.WebControls.Label
	Protected WithEvents lblTipo As System.Web.UI.WebControls.Label
	Protected WithEvents imgInfCreadoPor As Global.System.Web.UI.WebControls.Image
	Protected WithEvents imgInfTipo As Global.System.Web.UI.WebControls.Image
	Protected WithEvents imgControlDisponible As Global.System.Web.UI.WebControls.Image
	Protected WithEvents FSNPanelDatosPeticionario As Global.Fullstep.FSNWebControls.FSNPanelInfo
	Protected WithEvents FSNPanelDatosTrasladadaPor As Global.Fullstep.FSNWebControls.FSNPanelInfo
	Protected WithEvents Up_Uon As System.Web.UI.UpdatePanel
	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: This method call is required by the Web Form Designer
		'Do not modify it using the code editor.
		InitializeComponent()
	End Sub
#End Region
#Region "Variables Pagina"
	Private _blnAlta As Boolean = False
	''' <summary>
	''' Propiedad que carga si la instancia esta en modo lectura
	''' </summary>
	Protected Property blnAlta() As Boolean
		Get
			_blnAlta = ViewState("blnAlta")
			Return _blnAlta
		End Get
		Set(ByVal value As Boolean)
			_blnAlta = value
			ViewState("blnAlta") = _blnAlta
		End Set
	End Property
	Private _blnEtapaConfirmada As Boolean = False
	''' <summary>
	''' Propiedad que carga si el usuario ya ha confirmado la acci�n
	''' </summary>
	Protected Property blnEtapaConfirmada() As Boolean
		Get
			_blnEtapaConfirmada = ViewState("blnEtapaConfirmada")
			Return _blnEtapaConfirmada
		End Get
		Set(ByVal value As Boolean)
			_blnEtapaConfirmada = value
			ViewState("blnEtapaConfirmada") = _blnEtapaConfirmada
		End Set
	End Property
	Private _blnControlPartidasMostrado As Boolean = False
	''' <summary>
	''' Propiedad que carga si el usuario ya ha confirmado la acci�n
	''' </summary>
	Protected Property blnControlPartidasMostrado() As Boolean
		Get
			_blnControlPartidasMostrado = ViewState("blnControlPartidasMostrado")
			Return _blnControlPartidasMostrado
		End Get
		Set(ByVal value As Boolean)
			_blnControlPartidasMostrado = value
			ViewState("blnControlPartidasMostrado") = _blnControlPartidasMostrado
		End Set
	End Property
	Private _blnAccionSinControlDisponible As Boolean = False
	''' <summary>
	''' Propiedad que carga si hay que llevar a cabo el control de disponible para esta acci�n
	''' </summary>
	Protected Property blnAccionSinControlDisponible() As Boolean
		Get
			_blnAccionSinControlDisponible = ViewState("blnAccionSinControlDisponible")
			Return _blnAccionSinControlDisponible
		End Get
		Set(ByVal value As Boolean)
			_blnAccionSinControlDisponible = value
			ViewState("blnAccionSinControlDisponible") = _blnAccionSinControlDisponible
		End Set
	End Property
	Private ReadOnly Property IdInstanciaImportar As Long
		Get
			If Not Request("IdInstanciaImportar") Is Nothing Then
				Return Request("IdInstanciaImportar")
			Else
				Return Nothing
			End If
		End Get
	End Property
	Private oInstancia As FSNServer.Instancia
	Private _sMonedaInstancia As String
	Protected Property MonedaInstancia() As String
		Get
			_sMonedaInstancia = ViewState("MonedaInstancia")
			Return _sMonedaInstancia
		End Get
		Set(ByVal value As String)
			_sMonedaInstancia = value
			ViewState("MonedaInstancia") = _sMonedaInstancia
		End Set
	End Property
	Private _dsDatosPartidas As DataSet
	Protected Property dsDatosPartidas() As DataSet
		Get
			_dsDatosPartidas = CType(Cache("oControlPartidas" & FSNUser.Cod), DataSet)
			Return _dsDatosPartidas
		End Get
		Set(ByVal value As DataSet)
			Cache.Remove("oControlPartidas" & FSNUser.Cod)
			Me.InsertarEnCache("oControlPartidas" & FSNUser.Cod, value)
		End Set
	End Property
	Private _dsDatosSolicitud As DataSet
	Protected Property dsDatosSolicitud() As DataSet
		Get
			_dsDatosSolicitud = CType(Cache("dsDatosAlmacenar" & FSNUser.Cod), DataSet)
			Return _dsDatosSolicitud
		End Get
		Set(ByVal value As DataSet)
			Me.InsertarEnCache("dsDatosAlmacenar" & FSNUser.Cod, value)
		End Set
	End Property
	Private _dsRoles As DataSet
	Protected Property dsRoles() As DataSet
		Get
			_dsRoles = CType(Cache("dsRoles" & FSNUser.Cod), DataSet)
			Return _dsRoles
		End Get
		Set(ByVal value As DataSet)
			Me.InsertarEnCache("dsRoles" & FSNUser.Cod, value)
		End Set
	End Property
	Private oEtapas As DataSet
	Private sRolPorWebService As String
	Private sTextoLista() As String
	Private m_sIdiListaPart As String
	Private m_sIdiSeleccioneRol As String
	Private ds As DataSet
	Private dsControlPartidas As DataSet
	Private dTotal As Double
	Dim oSolicitud As FSNServer.Solicitud
	Dim lSolicitud, lInstancia, lCertificado, lNoConformidad As Long
	Dim blnBorrarPersona As Boolean = True
	Private oNoConformidad As FSNServer.NoConformidad
	Private oCertificado As FSNServer.Certificado
	Private iNoConformidad_Certificado As Integer ' 1 = NoConformidad ; 2 = Certificado
	Private ArrayGruposPadre() As Long
	Private ArrayGrupos() As Long
	Private ListaProvesEnviados As String = ""
#End Region
	''' Revisado por: blp. Fecha: 14/11/2011
	''' <summary>
	''' M�todo que se lanza en el evento Load de la p�gina
	''' </summary>
	''' <param name="sender">Control que lanza el evento</param>
	''' <param name="e">Argumentos del evento</param>
	''' <remarks>Llamada desde evento Load. M�x: 1 seg</remarks>
	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Dim blnOk As Boolean

		If SelectedPOSTBACK.Value = "1" Then
			'Se viene de la ventana de traslado a Usuario interno y 
			'Hay que recargar los combos con info actualizada
			blnBorrarPersona = False
			SelectedPOSTBACK.Value = ""
			ActualizarCombos_()
		Else
			blnBorrarPersona = True
		End If

		If ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/jquery.min.js")) Is Nothing OrElse Me.ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/jquery.min.js")).Count = 0 Then Me.ScriptMgr.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.min.js"))
		If ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/jquery.tmpl.min.js")) Is Nothing OrElse Me.ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/jquery.tmpl.min.js")).Count = 0 Then Me.ScriptMgr.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.tmpl.min.js"))
		If ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/jquery.ui.min.js")) Is Nothing OrElse Me.ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/jquery.ui.min.js")).Count = 0 Then Me.ScriptMgr.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.ui.min.js"))
		If ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/tmpl.min.js")) Is Nothing OrElse Me.ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/tmpl.min.js")).Count = 0 Then Me.ScriptMgr.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/tmpl.min.js"))
		If ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/jquery-migrate.min.js")) Is Nothing OrElse Me.ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/js/jquery/jquery-migrate.min.js")).Count = 0 Then Me.ScriptMgr.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery-migrate.min.js"))
		If ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/App_Pages/PMWEB/_common/js/menu.js")) Is Nothing OrElse Me.ScriptMgr.CompositeScript.Scripts.Where(Function(o As ScriptReference) (o.Path = "~/App_Pages/PMWEB/_common/js/menu.js")).Count = 0 Then Me.ScriptMgr.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/PMWEB/_common/js/menu.js"))

		If Page.IsPostBack Then
			If Request("__EVENTTARGET") = "btnConfirmarAvisoMapper" Then
				Dim serializer As New JavaScriptSerializer
				Dim oInfo As Object = serializer.Deserialize(Of Object)(Request("__EVENTARGUMENT"))
				Dim oInstancia As FSNServer.Instancia
				oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
				oInstancia.ID = oInfo("Instancia")
				oInstancia.Load(Idioma, True)

				idAccion.Value = oInfo("idAccion")
				Bloque.Value = oInfo("Bloque")

				Select Case oInfo("Accion")
					Case "trasladadadevolverinstancia", "trasladadadevolvercontrato"
						oInstancia.Solicitud.Load(Idioma)

						DevolverInstancia(oInstancia)
					Case "trasladarinstancia", "trasladadatrasladarinstancia", "trasladarcontrato", "trasladadatrasladarcontrato"
						TrasladarInstancia(oInstancia, Accion.Value)
					Case Else 'ES GUARDADO O UNA ACCION CUALQUIERA
						oInstancia.Solicitud.Load(Idioma)

						If oInfo("Version") = 0 Then
							oEtapas = oInstancia.DevolverSiguientesEtapasForm(idAccion.Value, FSNUser.CodPersona, Idioma, ds, sRolPorWebService, Bloque.Value, Workflow.Value)
						Else
							oEtapas = oInstancia.DevolverSiguientesEtapas(idAccion.Value, FSNUser.CodPersona, Idioma, ds, , , sRolPorWebService)
						End If

						RealizarAccion_Guardar(oInstancia, Accion.Value)
				End Select
			Else
				Exit Sub
			End If
		End If
		oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
		Try
			If Request("GEN_Accion") = "enviarcertificados" OrElse Request("GEN_Accion") = "guardarcertificado" OrElse Request("GEN_Accion") = "guardarnoconformidad" _
			OrElse Request("GEN_Accion") = "emitirnoconformidad" OrElse Request("GEN_Accion") = "cerrarpositivonoconformidad" OrElse Request("GEN_Accion") = "cerrarnegativonoconformidad" Then
				GuardarQA()
			Else
				'Informamos las variables que vamos a utilizar
				lInstancia = strToLong(Request("GEN_Instancia"))
				lSolicitud = strToLong(Request("GEN_Solicitud"))

				Accion.Value = Request("GEN_Accion") 'Para saber la accion que estamos haciendo. Si viene vac�o es que es una accion cualquiera
				Solicitud.Value = Request("GEN_Solicitud")
				Notificar.Value = (Val(Request("GEN_Notificar") = 1))
				Guardar.Value = Request("GEN_Guardar")
				ComentAltaNoConf.Value = Request("GEN_NoConfComentAlta")
				PedidoDirecto.Value = Request("GEN_PedidoDirecto")
				idAccion.Value = Val(Request("GEN_AccionRol")) 'Nos llega el ID de la acci�n correspondiente
				Bloque.Value = Request("GEN_Bloque") 'Nos llega el ID del bloque correspondiente
				Version.Value = Val(Request("GEN_Version")) 'Si es 0 estamos dando de alta la solicitud
				Contrato.Value = Request("ID_CONTRATO")
				CodContrato.Value = Request("COD_CONTRATO")
				Instancia.Value = lInstancia
				ConfiguracionGS.Value = Request("ConfiguracionGS")
				hidVolver.Value = Request("sVolver")
				RolActual.Value = strToLong(Request("GEN_Rol").ToString)

				Dim bMostrarMensaje As Boolean = False
				Dim sInstanciaMoneda As String = "EUR"
				If Request("GEN_AVISOBLOQUEO") = "" Then
					AvisoBloqueo.Value = TipoAvisoBloqueo.SinAsignar
				Else
					AvisoBloqueo.Value = Request("GEN_AVISOBLOQUEO")
				End If

				idRefSol.Value = strToDbl(Request("GEN_IDREFSOL"))

				'LO PRIMERO ES SABER SI ESTAMOS DANDO DE ALTA LA SOLICITUD, ESTO LO SABEMOS RECOGIENDO EL N� DE VERSI�N
				If Version.Value = 0 Then blnAlta = True

				blnOk = GenerarDataSetyHacerComprobaciones()

				If Not blnOk Then
					Page.ClientScript.RegisterStartupScript(Me.GetType(), "volver", "<script>parent.fraWSMain.HabilitarBotones();</script>")
				Else
					If Not oSolicitud Is Nothing Then oInstancia.Solicitud = oSolicitud

					Dim lFormularioID As Long
					If blnAlta Then lFormularioID = oInstancia.Solicitud.Formulario.Id

					If Accion.Value = "" Or Accion.Value = "guardarsolicitud" Or Accion.Value = "guardarcontrato" Then
						''COMPROBACI�N DEL BLOQUEO POR IMPORTE DE LA SOLICITUD DEL PADRE
						bMostrarMensaje = ComprobarBloqueosSolicitud(lFormularioID, lInstancia)
					End If

					'Ponemos la pagina con el estilo de GS
					If Request("ConfiguracionGS") = "1" Then
						mi_body.Attributes.Add("class", "fondoGS")
						Menu1.Visible = False
					End If

					If (bMostrarMensaje And AvisoBloqueo.Value = TipoAvisoBloqueo.Bloqueo And (Accion.Value = "" Or Accion.Value = "guardarsolicitud" Or Accion.Value = "guardarcontrato")) Then
						Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "HabilitarBotones", "<script>if (window.parent.fraWSMain != null) window.parent.fraWSMain.HabilitarBotones();</script>")
					Else
						If blnAlta And lInstancia = 0 Then 'SI ES UNA ALTA Y NO HEMOS CREADO LA INSTANCIA, LA CREAMOS
							If Not Request("IdFavorito") = Nothing Then oInstancia.IdFavorita = Request("IdFavorito")

							If IdInstanciaImportar <> Nothing Then oInstancia.IdInstanciaOrigen = IdInstanciaImportar

							If Not String.IsNullOrEmpty(Contrato.Value) Or (Accion.Value = "guardarcontrato" Or Accion.Value = "altacontrato") Then
								Contrato_Prev()
								oInstancia.ID = lInstancia
							ElseIf Not String.IsNullOrEmpty(Request("GEN_NoConformidad")) Or Not String.IsNullOrEmpty(Request("GEN_NoConfProve")) Then
								DarAltaNoConformidad(False, strToBoolean(Request("GEN_EsNoconfEscalacion")), strToInt(Request("GEN_IdEscalacionProve")))
								lInstancia = Instancia.Value
								lNoConformidad = NoConformidad.Value
								oInstancia.ID = lInstancia
							Else
								oInstancia.Create_Prev()
								lInstancia = oInstancia.ID
							End If
							oInstancia.Actualizar_En_proceso(1, strToLong(Bloque.Value.ToString))

							oInstancia.FechaAlta = Now
							oInstancia.Peticionario = FSNUser.CodPersona
							oInstancia.NombrePeticionario = FSNUser.Nombre
						Else
							oInstancia.ID = lInstancia
							If tipoSolicitud.Value = TiposDeDatos.TipoDeSolicitud.NoConformidad AndAlso lInstancia > 0 Then
								ModificarNoConformidad(False, strToBoolean(Request("GEN_EsNoconfEscalacion")), strToInt(Request("GEN_IdEscalacionProve")))
								lNoConformidad = NoConformidad.Value
							End If
						End If

						If oInstancia Is Nothing Then oInstancia.Load(Idioma)
						If oInstancia.Solicitud Is Nothing OrElse oInstancia.Solicitud.Formulario Is Nothing Then oInstancia.Solicitud.Load(Idioma)

						Instancia.Value = lInstancia

						If Not blnAlta Then OcultarDetallePer()

						If Not (Accion.Value = "guardarsolicitud" OrElse Accion.Value = "guardarcontrato") Then 'ES UNA ACCION CUALQUIERA
							''------ Inicializa oEtapas ------------------------------------------------------------------------------------------------
							If blnAlta Then
								oEtapas = oInstancia.DevolverSiguientesEtapasForm(idAccion.Value, FSNUser.CodPersona, Idioma, ds, sRolPorWebService, Bloque.Value, Workflow.Value)
							Else
								oEtapas = oInstancia.DevolverSiguientesEtapas(idAccion.Value, FSNUser.CodPersona, Idioma, ds, , , sRolPorWebService)
							End If
							''------------------------------------------------------------------------------------------------------------------------------
							'Mapper
							If Not String.IsNullOrEmpty(Request("PantallaMaper")) AndAlso Request("PantallaMaper") <> "undefined" AndAlso CBool(Request("PantallaMaper")) AndAlso CBool(Request("GEN_MostrarAvisoMapper")) Then 'SOLO CUANDO PROCEDA
								Dim MapperStr As String
								Dim iNumError As Integer
								Dim dsMapper As DataSet
								Dim oErrorMapper As Object = Nothing
								Dim TextosGS As FSNServer.TextosGS
								Dim DeDonde As String = Request("DeDonde")
								Dim bSave As Boolean = (DeDonde <> "Guardar")
								Dim oDSPares As DataSet = Nothing
								dsMapper = GenerarDatasetMapper(Request, oDSPares, Request("GEN_Accion"), oInstancia, bSave, Idioma)

								If lSolicitud = 0 Then
									dsMapper = oInstancia.CompletarFormulario(dsMapper, Usuario.CodPersona, True, True, Idioma)
								Else 'Es un alta
									dsMapper = oSolicitud.CompletarFormulario(dsMapper, True, Usuario.CodPersona, True)
								End If

								Dim strBloqueDestino As String = ""
								If (oEtapas.Tables.Count > 0) AndAlso (oEtapas.Tables.Count > 1) Then
									For Each oRow As DataRow In oEtapas.Tables(0).Rows
										strBloqueDestino = strBloqueDestino & oRow.Item("BLOQUE") & ","
									Next
									strBloqueDestino = IIf(Right(strBloqueDestino, 1) = ",", Left(strBloqueDestino, Len(strBloqueDestino) - 1), strBloqueDestino)
								End If

								oErrorMapper = oInstancia.ControlMapper(dsMapper, ds, Idioma, oInstancia.Peticionario, Val(Request("GEN_AccionRol")), strBloqueDestino, blnAlta)
								If Not oErrorMapper Is Nothing Then
									TextosGS = FSNServer.Get_Object(GetType(FSNServer.TextosGS))
									iNumError = CInt(oErrorMapper.iNumError)
									If iNumError = -100 Then
										MapperStr = oErrorMapper.strError
									Else
										MapperStr = TextosGS.MensajeError(MapperModuloMensaje.PedidoDirecto, iNumError, Idioma, oErrorMapper.strError)
									End If

									oInstancia.Actualizar_En_proceso(0, strToLong(Request("GEN_Bloque").ToString))
									oInstancia.Actualizar_En_proceso(0)

									Dim sJavaScript As String
									If oErrorMapper.tipoValidacionIntegracion = TipoValidacionIntegracion.Mensaje_Bloqueo Then
										sJavaScript = "alert('" & JSText(MapperStr) & "');if (window.parent.fraWSMain != null) window.parent.fraWSMain.HabilitarBotones();"
										Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "MapperAlert", sJavaScript, True)
									Else 'Aviso
										Dim ModuloIdiomaAux As TiposDeDatos.ModulosIdiomas = ModuloIdioma
										ModuloIdioma = TiposDeDatos.ModulosIdiomas.GuardarInstancia
										MapperStr = oErrorMapper.strError & Chr(10) & Textos(14)
										ModuloIdioma = ModuloIdiomaAux
										sJavaScript = "if (confirm('" & JSText(MapperStr) & "')) ConfirmarAvisoValidacionMapper();"
										sJavaScript &= "if (window.parent.fraWSMain != null) window.parent.fraWSMain.HabilitarBotones();"
										Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "MapperConfirm", sJavaScript, True)
									End If
									Exit Sub
								End If
							End If
						End If

						Select Case Accion.Value
							Case "trasladadadevolverinstancia", "trasladadadevolvercontrato"
								DevolverInstancia(oInstancia)
							Case "trasladarinstancia", "trasladadatrasladarinstancia", "trasladarcontrato", "trasladadatrasladarcontrato"
								TrasladarInstancia(oInstancia, Accion.Value)
							Case Else
								'ES GUARDADO O UNA ACCION CUALQUIERA
								RealizarAccion_Guardar(oInstancia, Accion.Value)
						End Select
					End If
				End If
			End If
			hTipoVisor.Value = GetTipoVisorFromTipoSolicitud(tipoSolicitud.Value)
		Catch ex As Exception
			oInstancia.Actualizar_En_proceso(0, strToLong(Request("GEN_Bloque").ToString))
			oInstancia.Actualizar_En_proceso(0)
			Dim oFileW As System.IO.StreamWriter
			oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & "\log_" & FSNUser.Cod & ".txt", True)
			oFileW.WriteLine()
			oFileW.WriteLine("Catch del load de guardar instancia: " & ex.Message & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
			oFileW.Close()

			'guardamos el objeto exception en una variable de session para que la trate la p�gina de errores.
			Session("Exception") = ex
			Page.ClientScript.RegisterStartupScript(Me.GetType(), "MostrarPantallaError", "<script>window.parent.fraWSMain.location = '" & ConfigurationManager.AppSettings("rutaFS") & "/_Common/Errores/errores.aspx?pagina=" & Request.Url.ToString & "';</script>")
		End Try
		If Contrato.Value <> "" Then
			If Version.Value = 0 Then
				Menu1.OpcionMenu = "Contratos"
				Menu1.OpcionSubMenu = "AltaContratos"
			Else
				Menu1.OpcionMenu = "Contratos"
				Menu1.OpcionSubMenu = "Seguimiento"
			End If
		ElseIf lCertificado > 0 OrElse CType(tipoSolicitud.Value, TiposDeDatos.TipoDeSolicitud) = TiposDeDatos.TipoDeSolicitud.Certificado Then
			Menu1.OpcionMenu = "Calidad"
			Menu1.OpcionSubMenu = "Certificados"
		ElseIf lNoConformidad > 0 Then
			Menu1.OpcionMenu = "Calidad"
			Menu1.OpcionSubMenu = "NoConformidades"
		ElseIf tipoSolicitud.Value <> Nothing AndAlso CType(tipoSolicitud.Value, TiposDeDatos.TipoDeSolicitud) = TiposDeDatos.TipoDeSolicitud.Factura Then
			Menu1.OpcionMenu = "Facturacion"
			Menu1.OpcionSubMenu = "Seguimiento"
		ElseIf tipoSolicitud.Value <> Nothing AndAlso CType(tipoSolicitud.Value, TiposDeDatos.TipoDeSolicitud) = TiposDeDatos.TipoDeSolicitud.Encuesta Then
			Menu1.OpcionMenu = "Calidad"
			Menu1.OpcionSubMenu = "Encuestas"
		ElseIf tipoSolicitud.Value <> Nothing AndAlso CType(tipoSolicitud.Value, TiposDeDatos.TipoDeSolicitud) = TiposDeDatos.TipoDeSolicitud.SolicitudQA Then
			Menu1.OpcionMenu = "Calidad"
			Menu1.OpcionSubMenu = "SolicitudesQA"
		Else
			Menu1.OpcionMenu = "Procesos"
			Menu1.OpcionSubMenu = "Seguimiento"
		End If
	End Sub
	Private Function DevolverIdCampoActual(ByRef oDsPares As DataSet, ByVal IdCampoVersion As Long) As Long
		If oDsPares Is Nothing Then
			Return IdCampoVersion
		End If
		Dim find(0) As Object
		find(0) = IdCampoVersion
		Dim oRow As DataRow

		oRow = oDsPares.Tables(0).Rows.Find(find)
		Return oRow.Item("IDCAMPOACTUAL")
	End Function
	''' <summary>
	''' Grabar una no conformidad o certificado. 
	''' No Conformidades: Tras un guardado no se hace ni submit ni se va a otra pantalla. Eso implica q haya datos q no estan 
	'''	actualizados en pantalla pero si en bbdd. Esta funci�n crea una lista de los calculados con sus valores ya calculados
	''' e indica si hay alguna accion q no este finalizada y revisada.
	''' </summary>
	''' <remarks>Llamada desde: Page_Load ; Tiempo m�ximo: 0,1</remarks>
	Private Sub GuardarQA()
		Dim m_sMsgboxAccion(3) As String
		Dim bEnviar As Boolean
		Dim bNotificar As Boolean
		Dim bAlgunaNoFinalRevisada As Boolean = False
		Dim ListaCalc As String = Nothing
		Dim TiempoNoConf As String
		Dim TiempoCert As String

		Try
			Guardar.Value = Request("GEN_Enviar")
			bEnviar = Guardar.Value

			ModuloIdioma = TiposDeDatos.ModulosIdiomas.GuardarInstancia

			Dim sNombreVersion As String = Request("GEN_NombreVersion")

			If Not String.IsNullOrEmpty(Request("GEN_Solicitud")) Then lSolicitud = CType(Request("GEN_Solicitud"), Long)
			If Not String.IsNullOrEmpty(Request("GEN_Certificado")) Then lCertificado = CType(Request("GEN_Certificado"), Long)
			If Not String.IsNullOrEmpty(Request("GEN_NoConformidad")) Then lNoConformidad = CType(Request("GEN_NoConformidad"), Long)
			Solicitud.Value = lSolicitud
			If Not lSolicitud = 0 Then
				oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
				oSolicitud.ID = lSolicitud
			End If
			If Not String.IsNullOrEmpty(Request("GEN_Version")) Then
				Version.Value = CType(Request("GEN_Version"), Long)
			Else
				Version.Value = 0
			End If
			Accion.Value = Request("GEN_Accion")

			tipoSolicitud.Value = If(lCertificado > 0, TiposDeDatos.TipoDeSolicitud.Certificado, TiposDeDatos.TipoDeSolicitud.NoConformidad)

			ListaProvesEnviados = ""

			If Request("GEN_Notificar") <> Nothing Then bNotificar = (Request("GEN_Notificar") = 1)

			Notificar.Value = IIf(bNotificar, 1, 0)

			If Not String.IsNullOrEmpty(Request("GEN_Instancia")) Then
				'Si no es un alta bloqueamos la instancia poniendola en proceso.
				lInstancia = CType(Request("GEN_Instancia"), Long)

				If lInstancia > 0 Then
					Instancia.Value = lInstancia
					oInstancia.Solicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
					oInstancia.ID = lInstancia
					oInstancia.Actualizar_En_proceso(1)
					oInstancia.Solicitud.ID = lSolicitud
				End If
			End If

			If ValidacionesMapper(bEnviar) Then
				ds = New DataSet
				If Accion.Value = "cerrarpositivonoconformidad" OrElse Accion.Value = "cerrarnegativonoconformidad" Then
					Dim oRequestManager As New RequestManager(Session, FSNServer, Request, "guardarnoconformidad", lSolicitud, lInstancia, Usuario, tipoSolicitud.Value)
					ds = oRequestManager.ProcessRequest(ArrayGruposPadre, ArrayGrupos, iNoConformidad_Certificado, bAlgunaNoFinalRevisada)
				Else
					Dim oRequestManager As New RequestManager(Session, FSNServer, Request, Accion.Value, lSolicitud, lInstancia, Usuario, tipoSolicitud.Value)
					ds = oRequestManager.ProcessRequest(ArrayGruposPadre, ArrayGrupos, iNoConformidad_Certificado, bAlgunaNoFinalRevisada)
				End If

				CompletarDataSetCamposInvisiblesLectura(ds, lSolicitud)

				CompletarDataSetCamposCalculados(ds, Idioma, ListaCalc, False)

				Guardar.Value = Request("GEN_Enviar")
				bEnviar = Guardar.Value
				GeneralFecha.Valor = Now

				''Si es un alta se crea la estructura de la NoConformidad //Certificado
				If lInstancia = 0 Then
					'Es un alta
					Workflow.Value = 1

					If iNoConformidad_Certificado = 1 Then
						'Si es el alta de una NoConformidad
						DarAltaNoConformidad(bEnviar, strToBoolean(Request("GEN_EsNoconfEscalacion")), strToInt(Request("GEN_IdEscalacionProve")))
					ElseIf iNoConformidad_Certificado = 2 Then
						'Si es el alta de un Certificado
						DarAltaCertificado(bEnviar)
					End If
				Else
					Workflow.Value = 0
					''No es una alta //Sino un guardado
					If lNoConformidad > 0 Then
						ModificarNoConformidad(bEnviar, strToBoolean(Request("GEN_EsNoconfEscalacion")), strToInt(Request("GEN_IdEscalacionProve")))
					ElseIf lCertificado > 0 Then
						Certificado.Value = lCertificado
					End If
				End If

				'Ya que si se solicitan varios certificados el xml se genera y se procesa en el metodo DarAltaCertificado
				If Not (iNoConformidad_Certificado = 2 And lInstancia = 0) Then
					'Llamamos al guardado XML y al web service mediante Thread para que no de Request time out
					GuardarQaThread()
				ElseIf lInstancia > 0 Then
					oInstancia.Actualizar_En_proceso(1)
				End If

				Dim TsMinutosDiferencia As TimeSpan = Date.Now - Date.UtcNow
				Dim MinutosDiferencia As Integer = (TsMinutosDiferencia.Hours * 60) + TsMinutosDiferencia.Minutes
				TiempoNoConf = Now.Year.ToString & "#" & Now.Month.ToString & "#" & Now.Day.ToString & "#" & Now.Hour.ToString & "#" & Now.Minute.ToString & "#" & Now.Second.ToString & "#" & CStr(MinutosDiferencia)
				TiempoCert = Now.Year.ToString & "#" & Now.Month.ToString & "#" & Now.Day.ToString & "#" & Now.Hour.ToString & "#" & Now.Minute.ToString & "#" & Now.Second.ToString & "#" & CStr(MinutosDiferencia)

				Version.Value = strToDbl(Version.Value) + 1
				tipoSolicitud.Value = TiposDeDatos.TipoDeSolicitud.NoConformidad
				Select Case Me.Accion.Value
					Case "emitirnoconformidad"
						If bEnviar Then
							Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "init", "<script>window.open(""" & "../noconformidad/noconfEmitidaOK.aspx?Instancia=" & oNoConformidad.Instancia & """,""fraWSMain"")</script>")
						Else
							Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerIdyEstado", "<script>window.parent.fraWSMain.PonerIdyEstado('" & oNoConformidad.Instancia & "','" & oNoConformidad.ID & "','" & JSText(oNoConformidad.ProveDen) & "','','','" & FormatDate(oNoConformidad.FechaAlta, FSNUser.DateFormat) & "','" & ListaCalc & "','" & oNoConformidad.Proveedor & "');</script>")
						End If
					Case "guardarnoconformidad"
						If bEnviar Then
							Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "init", "<script>window.open(""" & "../noconformidad/noconfEmitidaOK.aspx?Instancia=" & oNoConformidad.Instancia & """,""fraWSMain"")</script>")
						Else
							Page.ClientScript.RegisterStartupScript(Me.GetType(), "ActivarVentana", "<script>window.parent.fraWSMain.activarVentana('" & FSNUser.Nombre & "','" & ListaCalc & "'," & IIf(bAlgunaNoFinalRevisada, IIf(Not FSNUser.QACierreNoConf, 1, 0), 0) & ",'" & Version.Value & "','" & TiempoNoConf & "');</script>")
						End If
					Case "cerrarpositivonoconformidad"
						If Not (Request("GEN_NoConfRevisor") = "null" OrElse Request("GEN_NoConfRevisor") = "") Then
							Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "init", "<script>window.open(""" & "../noconformidad/cierreRevisor.aspx?NoConformidad=" & oNoConformidad.ID & "&Positivo=1&codRevisor=" & Request("GEN_NoConfRevisor") & "&NotifProv=" & oNoConformidad.NotificadosProveedor & "&NotifInt=" & oNoConformidad.NotificadosInternos & "&Instancia=" & oNoConformidad.Instancia & "&volver=" & Request("GEN_Volver") & """,""fraWSMain"")</script>")
						Else
							Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "init", "<script>window.open('../noconformidad/cierre.aspx?NoConformidad=" & oNoConformidad.ID & "&Positivo=1&NotifProv=" & oNoConformidad.NotificadosProveedor & "&NotifInt=" & oNoConformidad.NotificadosInternos & "&Instancia=" & oNoConformidad.Instancia & "&volver=" & Request("GEN_Volver") & "','fraWSMain')</script>")
						End If
					Case "cerrarnegativonoconformidad"
						If Not (Request("GEN_NoConfRevisor") = "null" OrElse Request("GEN_NoConfRevisor") = "") Then
							Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "init", "<script>window.open(""" & "../noconformidad/cierreRevisor.aspx?NoConformidad=" & oNoConformidad.ID & "&Positivo=0&codRevisor=" & Request("GEN_NoConfRevisor") & "&NotifProv=" & oNoConformidad.NotificadosProveedor & "&NotifInt=" & oNoConformidad.NotificadosInternos & "&Instancia=" & oNoConformidad.Instancia & "&volver=" & Request("GEN_Volver") & """,""fraWSMain"")</script>")
						Else
							Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "init", "<script>window.open('../noconformidad/cierre.aspx?NoConformidad=" & oNoConformidad.ID & "&Positivo=0&NotifProv=" & oNoConformidad.NotificadosProveedor & "&NotifInt=" & oNoConformidad.NotificadosInternos & "&Instancia=" & oNoConformidad.Instancia & "&volver=" & Request("GEN_Volver") & "','fraWSMain')</script>")
						End If
					Case "enviarcertificados"
						tipoSolicitud.Value = TiposDeDatos.TipoDeSolicitud.Certificado
						Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "init", "<script>window.open(""" & "../certificados/publicadoOK.aspx?DesdeCertificado=1&Instancia=" & ListaProvesEnviados & "&Volver=" & Request("GEN_Volver") & """ ,""_self"")</script>") 'fraWSMain
					Case "guardarcertificado"
						tipoSolicitud.Value = TiposDeDatos.TipoDeSolicitud.Certificado
						Page.ClientScript.RegisterStartupScript(Me.GetType(), "HabilitarBotones", "<script>window.parent.fraWSMain.HabilitarBotones('" & FSNUser.Nombre & "','" & Version.Value & "','" & TiempoCert & "');</script>")
				End Select
			End If
		Catch ex As Exception
			Dim oFileW As System.IO.StreamWriter
			Dim sFile As String = "\log_"
			oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & sFile & FSNUser.Cod & ".txt", True)
			oFileW.WriteLine()
			oFileW.WriteLine("Catch del load de guardar instancia: " & ex.Message & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
			oFileW.Close()

			'guardamos el objeto exception en una variable de session para que la trate la p�gina de errores.
			Session("Exception") = ex
			Page.ClientScript.RegisterStartupScript(Me.GetType(), "MostrarPantallaError", "<script>window.parent.fraWSMain.location = '" & ConfigurationManager.AppSettings("rutaFS") & "/_Common/Errores/errores.aspx?pagina=" & Request.Url.ToString & "';</script>")
		End Try
	End Sub
	Public Function ComprobarPrecondiciones(ByVal oInstancia As FSNServer.Instancia, ByVal dsPrecondiciones As DataSet, ByVal dsCampos As DataSet) As DataTable
		Dim dtAvisos As DataTable = New DataTable
		Dim oMonedas As Monedas = Me.FSNServer.Get_Object(GetType(Monedas))
		Dim dCambioVigenteInstancia As Double = 1
		Dim dsMonedas As DataSet

		dtAvisos.Columns.Add("TIPO", Type.GetType("System.Int32"))
		dtAvisos.Columns.Add("COD", Type.GetType("System.String"))
		dtAvisos.Columns.Add("DEN", Type.GetType("System.String"))

		Dim bCumple As Boolean
		If dsPrecondiciones.Tables.Count > 0 Then
			If Not oInstancia.Moneda Is Nothing Then
				dsMonedas = oMonedas.Get_Monedas("SPA", oInstancia.Moneda, bAllDatos:=True)
				dCambioVigenteInstancia = dsMonedas.Tables(0).Rows(0)("EQUIV")
			End If

			For Each drPrecondicion As DataRow In dsPrecondiciones.Tables(0).Rows
				bCumple = ComprobarCondiciones(oInstancia, drPrecondicion.Item("FORMULA"), drPrecondicion.GetChildRows("PRECOND_CONDICIONES"), dsCampos, dCambioVigenteInstancia)
				If bCumple Then
					Dim newRow As DataRow = dtAvisos.NewRow
					newRow.Item("TIPO") = drPrecondicion.Item("TIPO")
					newRow.Item("COD") = drPrecondicion.Item("COD")
					newRow.Item("DEN") = drPrecondicion.Item("DEN")
					dtAvisos.Rows.Add(newRow)
				End If
			Next
		End If
		Return dtAvisos
	End Function
	''' <summary>
	''' Procedimiento que comprueba si se cumpln las precondiciones definidas en la acci�n
	''' </summary>
	''' <remarks>
	''' Llamada desde: PmWeb/guardarInstancia/ComprobarPreCondiciones
	''' Tiempo m�ximo: 1 seg</remarks>
	Public Function ComprobarCondiciones(ByVal oInstancia As FSNServer.Instancia, ByVal sFormula As String, ByVal drCondiciones As DataRow(), ByVal dsCampos As DataSet, ByVal dCambioVigenteInstancia As Double) As Boolean
		Dim cm As New SqlCommand
		Dim drCampo As DataRow
		Dim oValorCampo As Object = Nothing
		Dim oValor As Object = Nothing
		Dim iTipo As TiposDeDatos.TipoGeneral
		Dim oDSFormula As New DataSet
		Dim oDSCond As New DataSet
		Dim oCond As DataRow
		Dim i As Long
		Dim sVariables() As String = Nothing
		Dim dValues() As Double = Nothing
		Dim iEq As New USPExpress.USPExpression
		Dim sMoneda As String
		Dim dCambioEnlace As Double
		Dim dst As New DataSet

		Try
			i = 0
			For Each oCond In drCondiciones
				ReDim Preserve sVariables(i)
				ReDim Preserve dValues(i)

				sVariables(i) = DBNullToSomething(oCond.Item("COD"))
				dValues(i) = 0 'El valor ser� 1 o 0 dependiendo de si se cumple o no la condici�n
				sMoneda = DBNullToStr(oCond.Item("MON"))
				dCambioEnlace = DBNullToDbl(oCond.Item("CAMBIO"))
				'<EXPRESION IZQUIEDA> <OPERADOR> <EXPRESION DERECHA>
				'Primero evaluamos <EXPRESION IZQUIERDA> 
				Select Case oCond.Item("TIPO_CAMPO")
					Case 1 'Campo de formulario
						If dsCampos.Tables(0).Select("CAMPO = " & oCond.Item("CAMPO")).Length > 0 Then
							drCampo = dsCampos.Tables(0).Select("CAMPO = " & oCond.Item("CAMPO"))(0)
							Select Case drCampo.Item("SUBTIPO")
								Case 2
									oValorCampo = DBNullToSomething(drCampo.Item("VALOR_NUM"))
								Case 3
									oValorCampo = DBNullToSomething(drCampo.Item("VALOR_FEC"))
								Case 4
									oValorCampo = DBNullToSomething(drCampo.Item("VALOR_BOOL"))
								Case Else
									oValorCampo = DBNullToSomething(drCampo.Item("VALOR_TEXT"))
							End Select
							iTipo = drCampo.Item("SUBTIPO")
							'Si es del tipo 7 habr� que hacer una llamada al webservice para la validaci�n
							If oCond.Item("TIPO") = 7 Then
								Dim WS_Consultas As New ConsultasPMWEB
								Dim params As New ConsultasPMWEB.JSonServicio
								Dim paramsEnt As New Dictionary(Of String, FSNServer.ParametroServicio)
								Dim paramsSal As New Dictionary(Of String, FSNServer.ParametroServicio)
								Dim sUrl As String = String.Empty
								Dim nomResult As String = String.Empty
								Dim nomEntrada As String = String.Empty
								Dim campoId As String = String.Empty
								Dim pair As KeyValuePair(Of String, FSNServer.ParametroServicio)
								If DBNullToStr(oCond.Item("CAMPO_ORIGEN")) = "" Then
									campoId = oCond.Item("CAMPO")
								Else
									campoId = oCond.Item("CAMPO_ORIGEN")
								End If
								params = WS_Consultas.Obtener_Servicio(oCond.Item("SERVICIO"), campoId, 0, "", "", "", 0, "", "", "", 0, 0, 0, 0) 'Ni instancia ni usuario conectado se usan en este caso
								sUrl = params.Url
								paramsEnt = params.ParamsEntrada
								For Each pair In paramsEnt
									nomEntrada = paramsEnt(pair.Key).Den
								Next
								paramsSal = params.ParamsSalida
								For Each pair In paramsSal
									If paramsSal(pair.Key).IndError = 1 Then
										nomResult = paramsSal(pair.Key).Den
									End If
								Next
								oValorCampo = LlamarWSServicio(sUrl, nomEntrada, oValorCampo, nomResult)
							End If
							If (sMoneda <> "" Or oCond.Item("TIPO_VALOR") = 6 Or oCond.Item("TIPO_VALOR") = 7) Then
								If oInstancia.Cambio <> 0 Then
									oValorCampo = CDec(oValorCampo / oInstancia.Cambio)
								End If
							End If
						End If
					Case 2 'Peticionario
						oValorCampo = oInstancia.GetPeticionario_Sustitucion(oInstancia.ID)
						iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
					Case 3 'Departamento del peticionario
						Dim oPer As FSNServer.Persona = FSNServer.Get_Object(GetType(FSNServer.Persona))
						oPer.LoadData(oInstancia.Peticionario)
						oValorCampo = oPer.Departamento
						iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
					Case 4 'UON del peticionario
						Dim oPer As FSNServer.Persona = FSNServer.Get_Object(GetType(FSNServer.Persona))
						oPer.LoadData(oInstancia.Peticionario)
						oValorCampo = oPer.UONs
						iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
					Case 5 'N� de procesos de compra abiertos
						If Request("GEN_Version") = "" Then
							oValorCampo = 0
						Else
							oInstancia.DevolverProcesosRelacionados()
							oValorCampo = oInstancia.Procesos.Tables(0).Rows.Count
						End If
						iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
					Case 6 'Importe adj (LO DEVUELVE EN MONEDA CENTRAL...Y SE COMPARA CON MONEDA DEL FORMULARIO --->PENDIENTE DE CAMBIAR)
						If Request("GEN_Version") = "" Then
							oValorCampo = 0
						Else
							oValorCampo = oInstancia.ObtenerImporteAdjudicado
						End If
						iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
					Case 7 'Importe de la solicitud de compra
						oValorCampo = oInstancia.Importe

						If oInstancia.Cambio <> 0 Then
							oValorCampo = CDec(oValorCampo / oInstancia.Cambio)
						End If
						iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
				End Select

				'Luego <EXPRESION DERECHA>
				Select Case oCond.Item("TIPO_VALOR")
					Case 1 'Campo de formulario
						If dsCampos.Tables(0).Select("CAMPO = " & oCond.Item("CAMPO_VALOR")).Length > 0 Then
							drCampo = dsCampos.Tables(0).Select("CAMPO = " & oCond.Item("CAMPO_VALOR"))(0)

							Select Case drCampo.Item("SUBTIPO")
								Case 2
									oValor = DBNullToSomething(drCampo.Item("VALOR_NUM"))
								Case 3
									oValor = DBNullToSomething(drCampo.Item("VALOR_FEC"))
								Case 4
									oValor = DBNullToSomething(drCampo.Item("VALOR_BOOL"))
								Case Else
									oValor = DBNullToSomething(drCampo.Item("VALOR_TEXT"))
							End Select
							If (sMoneda <> "" Or oCond.Item("TIPO_CAMPO") = 6 Or oCond.Item("TIPO_CAMPO") = 7) Then
								If oInstancia.Cambio <> 0 Then
									oValor = CDec(oValor / oInstancia.Cambio)
								End If
							End If
						End If
					Case 10 'valor est�tico
						Select Case iTipo
							Case 2
								oValor = DBNullToSomething(oCond.Item("VALOR_NUM"))
							Case 3
								oValor = DBNullToSomething(oCond.Item("VALOR_FEC"))
							Case 4
								oValor = DBNullToSomething(oCond.Item("VALOR_BOOL"))
							Case Else
								If oCond.Item("TIPO_CAMPO") = 4 Then 'UON DEL PETICIONARIO
									Dim oUON() As String
									Dim iIndice As Integer
									oValor = Nothing
									oUON = Split(oCond.Item("VALOR_TEXT"), "-")
									For iIndice = 0 To UBound(oUON)
										If oUON(iIndice) <> "" Then
											oValor = oValor & Trim(oUON(iIndice)) & "-"
										End If
									Next iIndice
									oValor = Left(oValor, Len(oValor) - 1)
								Else
									oValor = DBNullToSomething(oCond.Item("VALOR_TEXT"))
								End If
						End Select
						If sMoneda <> "" And dCambioEnlace <> 0 Then
							oValor = CDec(oValor / dCambioEnlace)
						ElseIf (oCond.Item("TIPO_CAMPO") = 6 Or oCond.Item("TIPO_CAMPO") = 7) And oInstancia.Cambio <> 0 Then
							oValor = CDec(oValor / oInstancia.Cambio)
						End If
					Case 2 'Peticionario
						oValor = oInstancia.GetPeticionario_Sustitucion(oInstancia.ID)
						iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
					Case 3 'Departamento del peticionario
						Dim oPer As FSNServer.Persona = FSNServer.Get_Object(GetType(FSNServer.Persona))
						oPer.LoadData(oInstancia.Peticionario)
						oValor = oPer.Departamento
						iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
					Case 4 'UON del peticionario
						Dim oPer As FSNServer.Persona = FSNServer.Get_Object(GetType(FSNServer.Persona))
						oPer.LoadData(oInstancia.Peticionario)
						oValor = oPer.UONs
						iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
					Case 5 'N� de procesos de compra abiertos
						If Request("GEN_Version") = "" Then
							'If Request("CALC_NumVersion") = "" Then
							oValor = 0
						Else
							oInstancia.DevolverProcesosRelacionados()
							oValor = oInstancia.Procesos.Tables(0).Rows.Count
						End If
						iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
					Case 6 'Importe adj (LO DEVUELVE EN MONEDA CENTRAL...Y SE COMPARA CON MONEDA DEL FORMULARIO --->PENDIENTE DE CAMBIAR)
						If Request("GEN_Version") = "" Then
							oValor = 0
						Else
							oValor = oInstancia.ObtenerImporteAdjudicado
						End If
						iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
					Case 7 'Importe de la solicitud de compra
						oValor = oInstancia.Importe

						If oInstancia.Cambio <> 0 Then
							oValor = CDec(oValor / oInstancia.Cambio)
						End If
						iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
				End Select

				'y por �ltimo con el OPERADOR obtenemos el valor
				Select Case iTipo
					Case 2, 3
						If oValorCampo Is Nothing Then 'Condici�n vac�a
							If oValor Is Nothing Then
								dValues(i) = 1
							Else
								dValues(i) = 0
							End If
						Else
							Select Case oCond.Item("OPERADOR")
								Case ">"
									If oValorCampo > oValor Then
										dValues(i) = 1
									Else
										dValues(i) = 0
									End If
								Case "<"
									If oValorCampo < oValor Then
										dValues(i) = 1
									Else
										dValues(i) = 0
									End If
								Case ">="
									If oValorCampo >= oValor Then
										dValues(i) = 1
									Else
										dValues(i) = 0
									End If
								Case "<="
									If oValorCampo <= oValor Then
										dValues(i) = 1
									Else
										dValues(i) = 0
									End If
								Case "="
									If oValorCampo = oValor Then
										dValues(i) = 1
									Else
										dValues(i) = 0
									End If
								Case "<>"
									If oValorCampo <> oValor Then
										dValues(i) = 1
									Else
										dValues(i) = 0
									End If
							End Select
						End If
					Case 4
						If oValorCampo Is Nothing Then 'Condici�n vac�a
							If oValor Is Nothing Then
								dValues(i) = 1
							Else
								dValues(i) = 0
							End If
						Else
							If IIf(IsNothing(oValorCampo), -1, oValorCampo) = IIf(IsNothing(oValor), -1, oValor) Then
								dValues(i) = 1
							Else
								dValues(i) = 0
							End If
						End If
					Case Else
						If oValorCampo Is Nothing Then 'Condici�n vac�a
							If oValor Is Nothing Then
								dValues(i) = 1
							Else
								dValues(i) = 0
							End If
						Else
							Select Case UCase(oCond.Item("OPERADOR"))
								Case "="
									If UCase(oValorCampo) = UCase(oValor) Then
										dValues(i) = 1
									Else
										dValues(i) = 0
									End If
								Case "LIKE"
									If Left(oValor, 1) = "*" Then
										If oValorCampo = Nothing Then 'Condici�n vac�a
											If oValor = Nothing Then
												dValues(i) = 1
											Else
												dValues(i) = 0
											End If
										Else
											If Right(oValor, 1) = "*" Then
												oValor = oValor.ToString.Replace("*", "")
												If InStr(oValorCampo.ToString, oValor.ToString) >= 0 Then
													dValues(i) = 1
												Else
													dValues(i) = 0
												End If
											Else
												oValor = oValor.ToString.Replace("*", "")
												If oValorCampo.ToString.EndsWith(oValor.ToString) Then
													dValues(i) = 1
												Else
													dValues(i) = 0
												End If
											End If
										End If
									Else
										If oValorCampo = Nothing Then 'Condici�n vac�a
											If oValor = Nothing Then
												dValues(i) = 1
											Else
												dValues(i) = 0
											End If
										Else
											If Right(oValor, 1) = "*" Then
												oValor = oValor.ToString.Replace("*", "")
												If oValorCampo.ToString.StartsWith(oValor.ToString) Then
													dValues(i) = 1
												Else
													dValues(i) = 0
												End If
											Else
												If UCase(oValorCampo.ToString) = UCase(oValor.ToString) Then
													dValues(i) = 1
												Else
													dValues(i) = 0
												End If

											End If
										End If
									End If
								Case "<>"
									If Not (oValorCampo Is Nothing) Then
										If UCase(oValorCampo.ToString) <> UCase(oValor.ToString) Then
											dValues(i) = 1
										Else
											dValues(i) = 0
										End If
									Else
										dValues(i) = 0
									End If
							End Select
						End If
				End Select

				i += 1
			Next
			oValor = Nothing
			Try
				iEq.Parse(sFormula, sVariables)
				oValor = iEq.Evaluate(dValues)

			Catch ex As USPExpress.ParseException
			Catch e As Exception
			End Try
		Catch e As Exception
			Dim a As String = e.Message
		End Try

		Return (oValor > 0)
	End Function
	''' <summary>
	''' Devuelve el dataset que utiliza la mapper para las validaciones
	''' </summary>
	''' <param name="Request">HttpRequest con los campos</param>
	''' <param name="oDSPares">Dataset; en caso de que exista se usa para recuperar el ID del campo</param>        
	''' <param name="sAccion">Accion sobre la instancia; no se utiliza en esta funci�n</param>   
	''' <param name="Objeto">Instancia para la que se generar� el dataset</param>   
	''' <param name="bSave">Indica si la instancia est� creada (guardada) o no</param>   
	''' <param name="Idioma">Idioma del usuario</param>   
	''' <returns>Devuelve el objeto dataset generado</returns>
	''' <remarks>Llamada desde: GenerarDatasetYHacerComprobaciones, GuardarConWorkflow;</remarks>
	Private Function GenerarDatasetMapper(ByVal Request As System.Web.HttpRequest, ByVal oDSPares As DataSet,
			ByVal sAccion As String, ByVal Objeto As FSNServer.Instancia, ByVal bSave As Boolean,
			ByVal Idioma As String) As DataSet

		Dim oItem As System.Collections.Specialized.NameValueCollection
		Dim loop1 As Integer
		Dim arr1() As String
		Dim DS As DataSet
		Dim dt As DataTable
		Dim oDTDesglose As DataTable
		Dim keys(0) As DataColumn
		Dim find(0) As Object
		Dim keysDesglose(2) As DataColumn
		Dim findDesglose(2) As Object

		DS = New DataSet

		dt = DS.Tables.Add("TEMP")
		dt.Columns.Add("CAMPO", System.Type.GetType("System.Int32"))
		dt.Columns.Add("VALOR_TEXT", System.Type.GetType("System.String"))
		dt.Columns.Add("VALOR_NUM", System.Type.GetType("System.Double"))
		dt.Columns.Add("VALOR_FEC", System.Type.GetType("System.DateTime"))
		dt.Columns.Add("VALOR_BOOL", System.Type.GetType("System.Int32"))
		dt.Columns.Add("ES_SUBCAMPO", System.Type.GetType("System.Int32"))
		dt.Columns.Add("SUBTIPO", System.Type.GetType("System.Int32"))
		dt.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dt.Columns.Add("IDATRIB", System.Type.GetType("System.Int32"))
		dt.Columns.Add("VALERP", System.Type.GetType("System.Int32"))
		dt.Columns.Add("TIPOGS", System.Type.GetType("System.Int32"))
		dt.Columns.Add("ORDEN", System.Type.GetType("System.Int32"))

		keys(0) = dt.Columns("CAMPO")
		dt.PrimaryKey = keys

		oDTDesglose = DS.Tables.Add("TEMPDESGLOSE")
		oDTDesglose.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		oDTDesglose.Columns.Add("CAMPO_PADRE", System.Type.GetType("System.Int32"))
		oDTDesglose.Columns.Add("CAMPO_HIJO", System.Type.GetType("System.Int32"))
		oDTDesglose.Columns.Add("VALOR_TEXT", System.Type.GetType("System.String"))
		oDTDesglose.Columns.Add("VALOR_NUM", System.Type.GetType("System.Double"))
		oDTDesglose.Columns.Add("VALOR_FEC", System.Type.GetType("System.DateTime"))
		oDTDesglose.Columns.Add("VALOR_BOOL", System.Type.GetType("System.Int32"))
		oDTDesglose.Columns.Add("LINEA_OLD", System.Type.GetType("System.Int32"))
		oDTDesglose.Columns.Add("IDATRIB", System.Type.GetType("System.Int32"))
		oDTDesglose.Columns.Add("VALERP", System.Type.GetType("System.Int32"))
		oDTDesglose.Columns.Add("TIPOGS", System.Type.GetType("System.Int32"))
		oDTDesglose.Columns.Add("TABLA_EXTERNA", System.Type.GetType("System.Int32"))
		oDTDesglose.Columns.Add("CALCULADO", System.Type.GetType("System.Boolean"))
		oDTDesglose.Columns.Add("CALCULADO_TIT", System.Type.GetType("System.String"))
		oDTDesglose.Columns("LINEA_OLD").AllowDBNull = True

		keysDesglose(0) = oDTDesglose.Columns("LINEA")
		keysDesglose(1) = oDTDesglose.Columns("CAMPO_PADRE")
		keysDesglose(2) = oDTDesglose.Columns("CAMPO_HIJO")
		oDTDesglose.PrimaryKey = keysDesglose

		Dim sRequest As String
		Dim oRequest As Object
		Dim dtNewRow As DataRow = Nothing
		Dim iTipo As TiposDeDatos.TipoGeneral

		oItem = Request.Form
		arr1 = oItem.AllKeys
		For loop1 = 0 To arr1.GetUpperBound(0)
			sRequest = arr1(loop1).ToString
			oRequest = sRequest.Split("_")

			If oRequest(0) = "CAMPO" Or oRequest(0) = "CAMPOID" Or oRequest(0) = "DESG" Or oRequest(0) = "DESGID" Or oRequest(0) = "PART" Then
				iTipo = oRequest(UBound(oRequest))
			End If

			Select Case oRequest(0)
				Case "PROVE"
					'Certif
				Case "PART"
					'Participantes
				Case "GEN"
				Case "CAMPO"
					'CAMPO_17046_7
					find(0) = oRequest(1)
					dtNewRow = dt.Rows.Find(find)
					If dtNewRow Is Nothing Then
						dtNewRow = dt.NewRow
					End If

					dtNewRow.Item("CAMPO") = oRequest(1)

					If Not IsNothing(Request(sRequest)) Then
						Select Case iTipo
							Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoArchivo
								dtNewRow.Item("VALOR_TEXT") = strToDBNull(Request(sRequest))
							Case TiposDeDatos.TipoGeneral.TipoNumerico
								If Request(sRequest) = "" Then
									dtNewRow.Item("VALOR_NUM") = System.DBNull.Value
								Else
									dtNewRow.Item("VALOR_NUM") = Numero(Request(sRequest), ".", ",")
								End If
							Case TiposDeDatos.TipoGeneral.TipoBoolean
								If Request(sRequest) = "" Then
									dtNewRow.Item("VALOR_BOOL") = System.DBNull.Value
								Else
									dtNewRow.Item("VALOR_BOOL") = Numero(Request(sRequest))
								End If
							Case TiposDeDatos.TipoGeneral.TipoFecha
								If igDateToDate(Request(sRequest)) <> Nothing Then
									dtNewRow.Item("VALOR_FEC") = igDateToDate(Request(sRequest))
								End If
							Case TiposDeDatos.TipoGeneral.TipoEditor
								dtNewRow.Item("VALOR_TEXT") = strToDBNull(Request(sRequest))
						End Select
					End If
					dtNewRow.Item("SUBTIPO") = iTipo
					dtNewRow.Item("ES_SUBCAMPO") = 0
					If dtNewRow.RowState = DataRowState.Detached Then
						dt.Rows.Add(dtNewRow)
					End If
				Case "CAMPOID"
					'PARA LOS CAMPOS DE TIPO TEXTO Y INTRODUCCI�N LISTA HAY QUE METER EL INDICE DEL ELEMENTO SELECCIONADO EN VALOR_NUM
					If iTipo = TiposDeDatos.TipoGeneral.TipoString Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoCorto Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoLargo Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio Then
						find(0) = oRequest(1)
						dtNewRow = dt.Rows.Find(find)
						If dtNewRow Is Nothing Then
							dtNewRow = dt.NewRow
						End If
						dtNewRow.Item("CAMPO") = oRequest(1)

						Select Case iTipo
							Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoArchivo
								If Request(sRequest) = "" Then
									'Numero("", ".", ",") da 0.0 aunque la funci�n devuelva realmente Nothing
									dtNewRow.Item("VALOR_NUM") = System.DBNull.Value
								Else
									dtNewRow.Item("VALOR_NUM") = Numero(Request(sRequest), ".", ",")
								End If
						End Select
						dtNewRow.Item("ES_SUBCAMPO") = 0
					End If
					If dtNewRow.RowState = DataRowState.Detached Then dt.Rows.Add(dtNewRow)
				Case "CAMPOAT"
					find(0) = oRequest(1)
					dtNewRow = dt.Rows.Find(find)
					If dtNewRow Is Nothing Then
						dtNewRow = dt.NewRow
					End If
					dtNewRow.Item("CAMPO") = oRequest(1)
					dtNewRow.Item("IDATRIB") = Request(sRequest)

					If dtNewRow.RowState = DataRowState.Detached Then
						dt.Rows.Add(dtNewRow)
					End If
				Case "CAMPOERP"
					'Validaci�n ERP (llamada externa a la BAPI de SAP para comprobar si el valor existe):
					find(0) = oRequest(1)
					dtNewRow = dt.Rows.Find(find)
					If dtNewRow Is Nothing Then
						dtNewRow = dt.NewRow
					End If
					dtNewRow.Item("CAMPO") = oRequest(1)
					dtNewRow.Item("VALERP") = Request(sRequest)

					If dtNewRow.RowState = DataRowState.Detached Then
						dt.Rows.Add(dtNewRow)
					End If
				Case "CAMPOTIPOGS"
					find(0) = oRequest(1)
					dtNewRow = dt.Rows.Find(find)
					If dtNewRow Is Nothing Then
						dtNewRow = dt.NewRow
					End If
					dtNewRow.Item("CAMPO") = oRequest(1)
					dtNewRow.Item("TIPOGS") = Numero(Request(sRequest))

					If dtNewRow.RowState = DataRowState.Detached Then
						dt.Rows.Add(dtNewRow)
					End If
				Case "CAMPOGRUPO"
					find(0) = oRequest(1)
					dtNewRow = dt.Rows.Find(find)
					If dtNewRow Is Nothing Then
						dtNewRow = dt.NewRow
					End If
					dtNewRow.Item("CAMPO") = oRequest(1)
					dtNewRow.Item("GRUPO") = Request(sRequest)

					If dtNewRow.RowState = DataRowState.Detached Then
						dt.Rows.Add(dtNewRow)
					End If
				Case "CAMPOORDEN"
					find(0) = oRequest(1)
					dtNewRow = dt.Rows.Find(find)
					If dtNewRow Is Nothing Then
						dtNewRow = dt.NewRow
					End If
					dtNewRow.Item("CAMPO") = oRequest(1)
					dtNewRow.Item("ORDEN") = Request(sRequest)

					If dtNewRow.RowState = DataRowState.Detached Then
						dt.Rows.Add(dtNewRow)
					End If
				Case "DESG"
					'DESG_17048_17058_3_6
					find(0) = oRequest(1)
					dtNewRow = dt.Rows.Find(find)
					If dtNewRow Is Nothing Then
						dtNewRow = dt.NewRow
						dtNewRow.Item("CAMPO") = oRequest(1)
						dtNewRow.Item("ES_SUBCAMPO") = 0
						dt.Rows.Add(dtNewRow)
					End If

					find(0) = oRequest(2)
					dtNewRow = dt.Rows.Find(find)
					If dtNewRow Is Nothing Then
						dtNewRow = dt.NewRow
						dtNewRow.Item("CAMPO") = oRequest(2)
						dtNewRow.Item("ES_SUBCAMPO") = 1
						dt.Rows.Add(dtNewRow)
					End If

					findDesglose(0) = oRequest(3)
					findDesglose(1) = oRequest(1)
					findDesglose(2) = oRequest(2)
					dtNewRow = oDTDesglose.Rows.Find(findDesglose)
					If dtNewRow Is Nothing Then
						dtNewRow = oDTDesglose.NewRow
					End If

					dtNewRow.Item("LINEA") = oRequest(3)
					dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
					dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

					If Request(sRequest) <> Nothing Then
						Select Case iTipo
							Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoArchivo
								dtNewRow.Item("VALOR_TEXT") = Request(sRequest)
							Case TiposDeDatos.TipoGeneral.TipoNumerico
								If Request(sRequest) = "" Then
									dtNewRow.Item("VALOR_NUM") = System.DBNull.Value
								Else
									dtNewRow.Item("VALOR_NUM") = Numero(Request(sRequest), ".", ",")
								End If
							Case TiposDeDatos.TipoGeneral.TipoBoolean
								If Request(sRequest) = "" Then
									dtNewRow.Item("VALOR_BOOL") = System.DBNull.Value
								Else
									dtNewRow.Item("VALOR_BOOL") = Numero(Request(sRequest))
								End If
							Case TiposDeDatos.TipoGeneral.TipoFecha
								If igDateToDate(Request(sRequest)) <> Nothing Then
									dtNewRow.Item("VALOR_FEC") = igDateToDate(Request(sRequest))
								End If
							Case TiposDeDatos.TipoGeneral.TipoEditor
								dtNewRow.Item("VALOR_TEXT") = strToDBNull(Request(sRequest))
						End Select
					End If
					If dtNewRow.RowState = DataRowState.Detached Then
						oDTDesglose.Rows.Add(dtNewRow)
					End If
				Case "DESGID"
					'DESGID_17048_17053_2_6:
					If iTipo = TiposDeDatos.TipoGeneral.TipoString Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoCorto Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoLargo Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio Then
						findDesglose(0) = oRequest(3)
						findDesglose(1) = oRequest(1)
						findDesglose(2) = oRequest(2)
						dtNewRow = oDTDesglose.Rows.Find(findDesglose)
						If dtNewRow Is Nothing Then
							dtNewRow = oDTDesglose.NewRow
						End If

						dtNewRow.Item("LINEA") = oRequest(3)
						dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
						dtNewRow.Item("CAMPO_HIJO") = oRequest(2)
						Select Case iTipo
							Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoArchivo
								dtNewRow.Item("VALOR_NUM") = Numero(Request(sRequest), ".", ",")
						End Select

						If Request(sRequest) <> "" Then
							dtNewRow.Item("VALOR_TEXT") = DameDescripcion(0, dtNewRow.Item("VALOR_NUM"), dtNewRow.Item("CAMPO_HIJO"), Objeto, bSave, Idioma)
						Else
							If IsDBNull(dtNewRow.Item("VALOR_TEXT")) Then
								dtNewRow.Item("VALOR_TEXT") = ""
							End If
						End If

						If dtNewRow.RowState = DataRowState.Detached Then
							oDTDesglose.Rows.Add(dtNewRow)
						End If
					End If
				Case "DESGTIPOGS"
					findDesglose(0) = oRequest(3)
					findDesglose(1) = oRequest(1)
					findDesglose(2) = oRequest(2)
					dtNewRow = oDTDesglose.Rows.Find(findDesglose)
					If dtNewRow Is Nothing Then
						dtNewRow = oDTDesglose.NewRow
					End If
					dtNewRow.Item("LINEA") = oRequest(3)
					dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
					dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

					dtNewRow.Item("TIPOGS") = Numero(Request(sRequest))

					If dtNewRow.RowState = DataRowState.Detached Then
						oDTDesglose.Rows.Add(dtNewRow)
					End If
				Case "DESGAT"
					findDesglose(0) = oRequest(3)
					findDesglose(1) = oRequest(1)
					findDesglose(2) = oRequest(2)
					dtNewRow = oDTDesglose.Rows.Find(findDesglose)
					If dtNewRow Is Nothing Then
						dtNewRow = oDTDesglose.NewRow
					End If
					dtNewRow.Item("LINEA") = oRequest(3)
					dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
					dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

					dtNewRow.Item("IDATRIB") = Request(sRequest)

					If dtNewRow.RowState = DataRowState.Detached Then
						oDTDesglose.Rows.Add(dtNewRow)
					End If
				Case "DESGERP"
					findDesglose(0) = oRequest(3)
					findDesglose(1) = oRequest(1)
					findDesglose(2) = oRequest(2)
					dtNewRow = oDTDesglose.Rows.Find(findDesglose)
					If dtNewRow Is Nothing Then
						dtNewRow = oDTDesglose.NewRow
					End If
					dtNewRow.Item("LINEA") = oRequest(3)
					dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
					dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

					dtNewRow.Item("VALERP") = Request(sRequest)

					If dtNewRow.RowState = DataRowState.Detached Then
						oDTDesglose.Rows.Add(dtNewRow)
					End If
				Case "DESGTABLAEXT"
					If Not Request(sRequest) = "" Then
						findDesglose(0) = oRequest(3)
						findDesglose(1) = oRequest(1)
						findDesglose(2) = oRequest(2)
						dtNewRow = oDTDesglose.Rows.Find(findDesglose)
						If dtNewRow Is Nothing Then
							dtNewRow = oDTDesglose.NewRow
						End If
						dtNewRow.Item("LINEA") = oRequest(3)
						dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
						dtNewRow.Item("CAMPO_HIJO") = oRequest(2)
						dtNewRow.Item("TABLA_EXTERNA") = Request(sRequest)
					End If

					If dtNewRow.RowState = DataRowState.Detached Then
						oDTDesglose.Rows.Add(dtNewRow)
					End If
				Case "DESGCALC"
					If Not Request(sRequest) = "" Then
						findDesglose(0) = oRequest(3)
						findDesglose(1) = oRequest(1)
						findDesglose(2) = oRequest(2)
						dtNewRow = oDTDesglose.Rows.Find(findDesglose)
						If dtNewRow Is Nothing Then
							dtNewRow = oDTDesglose.NewRow
						End If
						dtNewRow.Item("LINEA") = oRequest(3)
						dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
						dtNewRow.Item("CAMPO_HIJO") = oRequest(2)
						dtNewRow.Item("CALCULADO") = True
					End If

					If dtNewRow.RowState = DataRowState.Detached Then
						oDTDesglose.Rows.Add(dtNewRow)
					End If
				Case "DESGCALCTIT"
					If Not Request(sRequest) = "" Then
						findDesglose(0) = oRequest(3)
						findDesglose(1) = oRequest(1)
						findDesglose(2) = oRequest(2)
						dtNewRow = oDTDesglose.Rows.Find(findDesglose)
						If dtNewRow Is Nothing Then
							dtNewRow = oDTDesglose.NewRow
						End If
						dtNewRow.Item("LINEA") = oRequest(3)
						dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
						dtNewRow.Item("CAMPO_HIJO") = oRequest(2)
						dtNewRow.Item("CALCULADO_TIT") = Request(sRequest)
					End If
					If dtNewRow.RowState = DataRowState.Detached Then
						oDTDesglose.Rows.Add(dtNewRow)
					End If
			End Select
		Next loop1
		'volvemos a recorrernos el request para actualizar las l�neas de desglose con la correspondiente l�nea en bd
		'Importante para que recupere todos los datos cuando hay un campo de desglose en escritura y el resto en lectura (PMServer-Instancia.vb-CompletarFormulario)
		For loop1 = 0 To arr1.GetUpperBound(0)
			sRequest = arr1(loop1).ToString
			oRequest = sRequest.Split("_")

			Select Case oRequest(0)
				Case "LINDESG"
					'LINDESG_17048_2: 2
					If Request(sRequest) <> Nothing Then
						Dim oLineas As DataRow()
						oLineas = oDTDesglose.Select("LINEA =" & oRequest(2).ToString() & " AND CAMPO_PADRE = " & oRequest(1).ToString())
						For Each dtNewRow In oLineas
							dtNewRow.Item("LINEA_OLD") = Numero(Request(sRequest))
						Next
					End If
			End Select
		Next

		'REVISAR SI LO SIGUIENTE ES NECESARIO
		Dim oTable As DataTable
		oTable = DS.Tables("TEMP")
		For Each dtNewRow In oTable.Rows
			dtNewRow.Item("CAMPO") = DevolverIdCampoActual(oDSPares, dtNewRow.Item("CAMPO"))
		Next

		oTable = DS.Tables("TEMPDESGLOSE")
		For Each dtNewRow In oTable.Rows
			dtNewRow.Item("CAMPO_PADRE") = DevolverIdCampoActual(oDSPares, dtNewRow.Item("CAMPO_PADRE"))
			dtNewRow.Item("CAMPO_HIJO") = DevolverIdCampoActual(oDSPares, dtNewRow.Item("CAMPO_HIJO"))
		Next

		Return DS
	End Function
	''' <summary>
	''' Devuelve el dataset que utiliza la mapper para las validaciones
	''' </summary>
	''' <param name="Request">HttpRequest con los campos</param>
	''' <param name="sAccion">Accion sobre la instancia; no se utiliza en esta funci�n</param>   
	''' <param name="Objeto">Instancia para la que se generar� el dataset</param>   
	''' <param name="bSave">Indica si la instancia est� creada (guardada) o no</param>   
	''' <param name="Idioma">Idioma del usuario</param>   
	''' <returns>Devuelve el objeto dataset generado</returns>
	''' <remarks>Llamada desde: GenerarDatasetYHacerComprobaciones, GuardarConWorkflow;</remarks>
	Private Function GenerarDatasetMapperQA(ByVal Request As System.Web.HttpRequest,
			ByVal sAccion As String, ByVal Objeto As FSNServer.Instancia, ByVal bSave As Boolean,
			ByVal Idioma As String) As DataSet
		Dim oItem As System.Collections.Specialized.NameValueCollection
		Dim loop1 As Integer
		Dim arr1() As String
		Dim DS As DataSet
		Dim dt As DataTable
		Dim oDTDesglose As DataTable
		Dim keys(0) As DataColumn
		Dim find(0) As Object
		Dim keysDesglose(2) As DataColumn
		Dim findDesglose(2) As Object

		DS = New DataSet

		dt = DS.Tables.Add("TEMP")
		dt.Columns.Add("CAMPO", System.Type.GetType("System.Int32"))
		dt.Columns.Add("VALOR_TEXT", System.Type.GetType("System.String"))
		dt.Columns.Add("VALOR_NUM", System.Type.GetType("System.Double"))
		dt.Columns.Add("VALOR_FEC", System.Type.GetType("System.DateTime"))
		dt.Columns.Add("VALOR_BOOL", System.Type.GetType("System.Int32"))
		dt.Columns.Add("ES_SUBCAMPO", System.Type.GetType("System.Int32"))
		dt.Columns.Add("SUBTIPO", System.Type.GetType("System.Int32"))
		dt.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dt.Columns.Add("IDATRIB", System.Type.GetType("System.Int32"))
		dt.Columns.Add("TIPOGS", System.Type.GetType("System.Int32"))
		dt.Columns.Add("TABLA_EXTERNA", System.Type.GetType("System.Int32"))
		dt.Columns.Add("ORDEN", System.Type.GetType("System.Int32"))

		keys(0) = dt.Columns("CAMPO")
		dt.PrimaryKey = keys

		oDTDesglose = DS.Tables.Add("TEMPDESGLOSE")
		oDTDesglose.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		oDTDesglose.Columns.Add("CAMPO_PADRE", System.Type.GetType("System.Int32"))
		oDTDesglose.Columns.Add("CAMPO_HIJO", System.Type.GetType("System.Int32"))
		oDTDesglose.Columns.Add("VALOR_TEXT", System.Type.GetType("System.String"))
		oDTDesglose.Columns.Add("VALOR_NUM", System.Type.GetType("System.Double"))
		oDTDesglose.Columns.Add("VALOR_FEC", System.Type.GetType("System.DateTime"))
		oDTDesglose.Columns.Add("VALOR_BOOL", System.Type.GetType("System.Int32"))
		oDTDesglose.Columns.Add("LINEA_OLD", System.Type.GetType("System.Int32"))
		oDTDesglose.Columns.Add("IDATRIB", System.Type.GetType("System.Int32"))
		oDTDesglose.Columns.Add("VALERP", System.Type.GetType("System.Int32"))
		oDTDesglose.Columns.Add("TIPOGS", System.Type.GetType("System.Int32"))
		oDTDesglose.Columns.Add("TABLA_EXTERNA", System.Type.GetType("System.Int32"))
		oDTDesglose.Columns.Add("CALCULADO", System.Type.GetType("System.Boolean"))
		oDTDesglose.Columns.Add("CALCULADO_TIT", System.Type.GetType("System.String"))
		oDTDesglose.Columns("LINEA_OLD").AllowDBNull = True

		keysDesglose(0) = oDTDesglose.Columns("LINEA")
		keysDesglose(1) = oDTDesglose.Columns("CAMPO_PADRE")
		keysDesglose(2) = oDTDesglose.Columns("CAMPO_HIJO")
		oDTDesglose.PrimaryKey = keysDesglose

		Dim sRequest As String
		Dim oRequest As Object
		Dim dtNewRow As DataRow
		Dim iTipo As TiposDeDatos.TipoGeneral
		oItem = Request.Form
		arr1 = oItem.AllKeys
		For loop1 = 0 To arr1.GetUpperBound(0)
			sRequest = arr1(loop1).ToString
			oRequest = sRequest.Split("_")

			If oRequest(0) = "CAMPO" Or oRequest(0) = "CAMPOID" Or oRequest(0) = "DESG" Or oRequest(0) = "DESGID" Or oRequest(0) = "PART" Then
				iTipo = oRequest(UBound(oRequest))
			End If

			Select Case oRequest(0)
				Case "CAMPO"
					'CAMPO_17046_7
					find(0) = oRequest(1)
					dtNewRow = dt.Rows.Find(find)
					If dtNewRow Is Nothing Then
						dtNewRow = dt.NewRow
					End If
					dtNewRow.Item("CAMPO") = oRequest(1)

					If Not IsNothing(Request(sRequest)) Then
						Select Case iTipo
							Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoArchivo
								dtNewRow.Item("VALOR_TEXT") = strToDBNull(Request(sRequest))
							Case TiposDeDatos.TipoGeneral.TipoNumerico
								If Request(sRequest) = "" Then
									dtNewRow.Item("VALOR_NUM") = System.DBNull.Value
								Else
									dtNewRow.Item("VALOR_NUM") = Numero(Request(sRequest), ".", ",")
								End If
							Case TiposDeDatos.TipoGeneral.TipoBoolean
								If Request(sRequest) = "" Then
									dtNewRow.Item("VALOR_BOOL") = System.DBNull.Value
								Else
									dtNewRow.Item("VALOR_BOOL") = Numero(Request(sRequest))
								End If
							Case TiposDeDatos.TipoGeneral.TipoFecha
								If igDateToDate(Request(sRequest)) <> Nothing Then
									dtNewRow.Item("VALOR_FEC") = igDateToDate(Request(sRequest))
								End If
							Case TiposDeDatos.TipoGeneral.TipoEditor
								dtNewRow.Item("VALOR_TEXT") = strToDBNull(Request(sRequest))
						End Select
					End If
					dtNewRow.Item("SUBTIPO") = iTipo
					dtNewRow.Item("ES_SUBCAMPO") = 0
					If dtNewRow.RowState = DataRowState.Detached Then
						dt.Rows.Add(dtNewRow)
					End If
				Case "CAMPOTIPOGS"
					find(0) = oRequest(1)
					dtNewRow = dt.Rows.Find(find)
					If dtNewRow Is Nothing Then
						dtNewRow = dt.NewRow
					End If
					dtNewRow.Item("CAMPO") = oRequest(1)
					dtNewRow.Item("TIPOGS") = Numero(Request(sRequest))

					If dtNewRow.RowState = DataRowState.Detached Then
						dt.Rows.Add(dtNewRow)
					End If
				Case "CAMPOGRUPO"
					find(0) = oRequest(1)
					dtNewRow = dt.Rows.Find(find)
					If dtNewRow Is Nothing Then
						dtNewRow = dt.NewRow
					End If
					dtNewRow.Item("CAMPO") = oRequest(1)
					dtNewRow.Item("GRUPO") = Request(sRequest)

					If dtNewRow.RowState = DataRowState.Detached Then
						dt.Rows.Add(dtNewRow)
					End If
				Case "CAMPOTABLAEXT"
					find(0) = oRequest(1)
					dtNewRow = dt.Rows.Find(find)
					If dtNewRow Is Nothing Then
						dtNewRow = dt.NewRow
					End If
					dtNewRow.Item("CAMPO") = oRequest(1)
					dtNewRow.Item("TABLA_EXTERNA") = Request(sRequest)

					If dtNewRow.RowState = DataRowState.Detached Then
						dt.Rows.Add(dtNewRow)
					End If
			End Select
		Next loop1

		Return DS
	End Function
	Private Function DameDescripcion(ByVal IdTabla As Short, ByVal IdLista As Integer, ByVal Campo As Integer,
			ByVal Objeto As FSNServer.Instancia, ByVal bSave As Boolean,
			ByVal Idioma As String) As String
		Dim bEsta As Boolean
		Dim dtCombosUso As DataSet
		Dim Indice As Integer = 0
		Dim iCampoLista() As Integer = Nothing
		Dim iIdLista() As Integer = Nothing
		DameDescripcion = String.Empty
		Try
			bEsta = False
			If Not iCampoLista Is Nothing Then
				For i As Integer = 0 To UBound(iCampoLista)
					If iCampoLista(i) = Campo Then
						bEsta = True
						Exit For
					End If
				Next
				Indice = UBound(iCampoLista) + 1
			Else
				Indice = 0
			End If

			If Not bEsta Then
				dtCombosUso = Objeto.CargaComboMaper(bSave, Campo, Idioma)

				For Each oRow As DataRow In dtCombosUso.Tables(IdTabla).Rows
					ReDim Preserve iCampoLista(Indice)
					iCampoLista(Indice) = Campo

					ReDim Preserve iIdLista(Indice)
					iIdLista(Indice) = oRow("ORDEN")

					ReDim Preserve sTextoLista(Indice)
					sTextoLista(Indice) = oRow("VALOR_TEXT")

					Indice = Indice + 1
				Next
			End If

			For i As Integer = 0 To UBound(iCampoLista)
				If iCampoLista(i) = Campo AndAlso iIdLista(i) = IdLista Then
					DameDescripcion = sTextoLista(i)
					Exit Function
				End If
			Next
		Catch ex As Exception
			Return String.Empty
		End Try
	End Function
	Private Sub ugtxtRoles_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles ugtxtRoles.InitializeLayout
		With e.Layout.Bands(0)
			'Oculta las columnas:
			.Columns.FromKey("ROL").Hidden = True
			.Columns.FromKey("PER").Hidden = True
			.Columns.FromKey("PROVE").Hidden = True
			.Columns.FromKey("COMO_ASIGNAR").Hidden = True
			.Columns.FromKey("CON").Hidden = True
			.Columns.FromKey("TIPO").Hidden = True
			.Columns.FromKey("ASIGNAR").Hidden = True
			.Columns.FromKey("CAMPO").Hidden = True
			.Columns.FromKey("DEN").Width = Unit.Percentage(35%)
			.Columns.FromKey("NOMBRE").Width = Unit.Percentage(65%)
			.Columns.Add("SEL")
			.Columns.FromKey("SEL").Width = Unit.Point(20)
			.Columns.Add("CONTACTOS")
			.Columns.FromKey("CONTACTOS").Hidden = True
		End With
	End Sub
	''' <summary>
	''' Pone el nombre de la persona a la q le va a llegar tras el cambio de etapa. En caso de lista, pone "(Se asignara ..." indicando q todavia no se sabe quien.
	''' Muestra/Oculta el bt para ver detalle persona/seleccionar persona. Se oculta si es lista de personas.
	''' </summary>
	''' <param name="sender">Control que lanza el evento</param>
	''' <param name="e">Argumentos del evento</param>
	''' <remarks>llamada desde el evento. M�ximo 0,2 seg</remarks>
	Private Sub ugtxtRoles_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles ugtxtRoles.InitializeRow
		If e.Row.Cells.FromKey("ASIGNAR").Value = 1 AndAlso e.Row.Cells.FromKey("PROVE").Value = Nothing AndAlso e.Row.Cells.FromKey("PER").Value = Nothing Then
			e.Row.Cells.FromKey("NOMBRE").Value = m_sIdiSeleccioneRol
			e.Row.Cells.FromKey("NOMBRE").Style.CssClass = "fntRequired"
			e.Row.Cells.FromKey("SEL").Value = "<input type=""button"" value = ""..."""
			If e.Row.Cells.FromKey("TIPO").Value = "2" AndAlso e.Row.Cells.FromKey("PROVE").Value = Nothing Then
				e.Row.Cells.FromKey("SEL").Value = e.Row.Cells.FromKey("SEL").Value & " onclick=""BuscadorProveedores(" & e.Row.Cells.FromKey("ROL").Value & ")"">"
			ElseIf e.Row.Cells.FromKey("PER").Value = Nothing Then
				e.Row.Cells.FromKey("SEL").Value = e.Row.Cells.FromKey("SEL").Value & " onclick=""BuscadorUsuarios(" & e.Row.Cells.FromKey("ROL").Value & ")"">"
			End If
		ElseIf (e.Row.Cells.FromKey("COMO_ASIGNAR").Value = 3 OrElse e.Row.Cells.FromKey("COMO_ASIGNAR").Value = 4) _
		AndAlso e.Row.Cells.FromKey("PER").Value = Nothing Then 'Es una lista de participantes
			e.Row.Cells.FromKey("NOMBRE").Value = m_sIdiListaPart
		Else
			e.Row.Cells.FromKey("SEL").Value = "<input type=""button"" value = ""   """
			If e.Row.Cells.FromKey("PROVE").Value = Nothing Then
				e.Row.Cells.FromKey("SEL").Value = e.Row.Cells.FromKey("SEL").Value & " onclick=""VerDetallePersona('" & e.Row.Cells.FromKey("PER").Value & "')"">"
			Else
				e.Row.Cells.FromKey("SEL").Value = e.Row.Cells.FromKey("SEL").Value & " onclick=""VerDetalleProveedor('" & e.Row.Cells.FromKey("PROVE").Value & "')"">"
			End If
		End If
	End Sub
	''' <summary>
	''' Revisado por: Sandra. Fecha: 23/03/2011.
	''' Genera el dataset con los campos del formulario y hace las comprobaciones necesarias para ver si puede seguir adelante
	''' </summary>
	''' <returns>True o False dependiendo de si ha cumplido todas las comprobaciones</returns>
	''' <remarks>Llamada desde: Page_Load(); Tiempo m�ximo: 2 sg;</remarks>
	Function GenerarDataSetyHacerComprobaciones() As Boolean
		Dim tmpRow() As DataRow

		If Not lSolicitud = 0 Then
			oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
			oSolicitud.ID = lSolicitud
			oSolicitud.Load(Idioma)
			If lInstancia = 0 Then
				Formulario.Value = oSolicitud.Formulario.Id
				Workflow.Value = oSolicitud.Workflow.ToString
				PedidoDirecto.Value = oSolicitud.PedidoDirecto
			End If
		End If

		oInstancia.ID = lInstancia
		If Not lInstancia = 0 Then
			oInstancia.Load(Idioma, True)

			Formulario.Value = oInstancia.IdFormulario
			Workflow.Value = oInstancia.IdWorkflow
			PedidoDirecto.Value = oInstancia.PedidoDirecto
		Else
			oInstancia.IdFormulario = oSolicitud.Formulario.Id
			oInstancia.Peticionario = FSNUser.CodPersona
			oInstancia.Solicitud = oSolicitud
		End If

		tipoSolicitud.Value = oInstancia.Solicitud.TipoSolicit

		Try
			If Guardar.Value = "1" Then
				Dim oRequestManager As New RequestManager(Session, FSNServer, Request, Accion.Value, lSolicitud, lInstancia, Usuario, tipoSolicitud.Value)
				ds = oRequestManager.ProcessRequest(ArrayGruposPadre, ArrayGrupos, iNoConformidad_Certificado)
				dsDatosSolicitud = ds.Copy
				If ds.Tables("TEMP").Rows.Count = 0 Then
					Guardar.Value = 0
				Else
					If Not lSolicitud = 0 Then
						ds = oSolicitud.CompletarFormulario(ds, True, Usuario.CodPersona)
					Else
						ds = oInstancia.CompletarFormulario(ds, Usuario.CodPersona, True, , , True)
					End If
				End If

				'RECALCULAMOS LOS CAMPOS CALCULADOS POR SI LAS FLY
				Dim oDS As DataSet
				Dim oDSDesglose As DataSet
				Dim bCalcular As Boolean
				Dim oDSRow As DataRow
				Dim oRow As DataRow
				Dim dt As DataTable = ds.Tables("TEMP")
				Dim oDSRowDesglose As DataRow
				Dim oDSRowDesgloseOculto As DataRow
				Dim sPre As String
				Dim sRequest As String
				Dim iTipoGS As TiposDeDatos.TipoCampoGS
				Dim iTipo As TiposDeDatos.TipoGeneral
				Dim iTipoGSDesglose As TiposDeDatos.TipoCampoGS
				Dim iTipoDesglose As TiposDeDatos.TipoGeneral
				Dim vValor As Object
				Dim oCampo As FSNServer.Campo
				Dim k As Integer
				Dim iLinea As Integer
				Dim oDSDesglosePadreVisible As DataSet
				Dim oDSRowDesglosePadreVisible As DataRow
				'2918 - Si estamos en un alta buscamos la moneda de la instancia en el campo de tipo moneda (si existiera) dentro del formulario
				'Si existe el campo asignamos a la instancia la moneda de dicho campo
				'En caso contrario (bien no alta, o no se encuentra moneda) se deja tal cual estaba
				If Not dt Is Nothing Then
					Dim dr() As DataRow = dt.Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.Moneda)
					If dr.Any Then oInstancia.Moneda = DBNullToStr(dr(0)("VALOR_TEXT"))
				End If

				If blnAlta Then
					If IsNothing(oSolicitud) Then
						oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
						oSolicitud.ID = Request("CALC_Solicitud")
						oSolicitud.Load(Idioma)
					End If
					oDS = oSolicitud.Formulario.LoadCamposCalculados(oSolicitud.ID, Usuario.CodPersona)

					'2918 - Si se trata de un contrato vamos a leer la moneda del combo del formulario de contratos 
					If Not String.IsNullOrEmpty(Contrato.Value) Then oInstancia.Moneda = ds.Tables("CONTRATO").Rows(0)("MON")
				Else
					If IsNothing(oInstancia) Then
						oInstancia.ID = Request("CALC_Instancia")
						oInstancia.Load(Idioma)
					End If
					oInstancia.Version = Request("CALC_NumVersion")
					oDS = oInstancia.LoadCamposCalculados(Usuario.CodPersona)
				End If

				MonedaInstancia = oInstancia.Moneda

				Dim findDesglose(2) As Object
				Dim find(0) As Object
				Dim oRowDesglose As DataRow
				Dim keysDesglose(2) As DataColumn
				Dim oDTDesglose As DataTable
				oDTDesglose = ds.Tables("TEMPDESGLOSE")

				Dim drComprador() As DataRow
				drComprador = ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.Comprador & " AND VALOR_TEXT IS NOT NULL")
				If drComprador.Length > 0 Then oInstancia.Comprador = DBNullToSomething(drComprador(0).Item("VALOR_TEXT"))

				Dim sVariables() As String
				Dim sVariablesDesglose() As String
				Dim dValues() As Double
				Dim dValuesDesglose() As Double
				Dim dValuesTotalDesglose() As Double
				Dim i As Integer = 0
				Dim iDesglose As Integer
				Dim Eliminados As Integer = 0
				Dim iEq As New USPExpress.USPExpression
				Dim oRowWorkflow As DataRow
				Dim bEstaInvisibleElDesglose As Boolean
				ReDim sVariables(oDS.Tables(0).Rows.Count - 1)
				ReDim dValues(oDS.Tables(0).Rows.Count - 1)

				For Each oDSRow In oDS.Tables(0).Rows
					bEstaInvisibleElDesglose = False

					sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString())

					sVariables(i) = DBNullToSomething(oDSRow.Item("ID_CALCULO"))

					iTipoGS = DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS"))
					iTipo = oDSRow.Item("SUBTIPO")

					If Version.Value = 0 Then
						sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID").ToString()
					Else
						sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO").ToString()
					End If
					If oDSRow.Item("ESCRITURA") = 1 Then 'sra antes VISIBLE
						vValor = Numero(VacioToNothing(Request(sRequest)), ".", ",")
					Else
						vValor = DBNullToSomething(oDSRow.Item("VALOR_NUM"))
					End If

					If oDSRow.Item("TIPO") = TipoCampoPredefinido.Calculado Then
						If Not IsDBNull(oDSRow.Item("ORIGEN_CALC_DESGLOSE")) Then
							oCampo = FSNServer.Get_Object(GetType(FSNServer.Campo))

							If Version.Value = 0 Then
								oCampo.Id = oDSRow.Item("ORIGEN_CALC_DESGLOSE")
								'Mirar si en la solicitud hay algun campo de Partidas Presupuestarias
								oDSDesglose = oCampo.LoadCalculados(oSolicitud.ID, Usuario.CodPersona)
								oDSDesglosePadreVisible = oSolicitud.Formulario.LoadDesglosePadreVisible(oSolicitud.ID, oCampo.Id, Usuario.CodPersona)
							Else
								oCampo.Id = DBNullToInteger(oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE"))
								oDSDesglose = oCampo.LoadInstCalculados(lInstancia, Usuario.CodPersona)
								oDSDesglosePadreVisible = oInstancia.LoadDesglosePadreVisible(oCampo.Id, Usuario.CodPersona)
							End If

							ReDim sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
							ReDim dValuesDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
							ReDim dValuesTotalDesglose(oDSDesglose.Tables(0).Rows.Count - 1)

							Dim dTotalDesglose As Double
							Dim bPopUp As Boolean
							Dim bDesgloseEmergente As Boolean
							Dim iNumRows As Integer
							Dim lineasReal As Integer()

							bCalcular = False
							dTotalDesglose = 0
							iNumRows = 0
							'Compruebo si el campo desglose es visible
							bEstaInvisibleElDesglose = True
							If oDSDesglosePadreVisible.Tables(0).Rows.Count > 0 Then
								For Each oDSRowDesglosePadreVisible In oDSDesglosePadreVisible.Tables(0).Rows
									If oDSRowDesglosePadreVisible.Item("VISIBLE") = 1 Then
										bEstaInvisibleElDesglose = False
										Exit For
									End If
								Next
							End If

							'Miro como si fuera visible
							'si es visible y tiene lineas me dara el numero de lineas
							If bEstaInvisibleElDesglose = False Then
								sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString())
								iNumRows = Request("CALC_" + sPre + "_tblDesglose") 'Filas de desglose que salen en pesta�a
								bPopUp = False
								If iNumRows = Nothing Then 'El desglose es de tipo popup
									If Version.Value = 0 Then
										iNumRows = Request("CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "_tblDesglose")
										bDesgloseEmergente = False
										If iNumRows = Nothing Then
											bDesgloseEmergente = True
											iNumRows = Request("CALC_" + sPre + "_fsentry" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "__numRows")
										End If
									Else
										'Desglose no Emergente "CALC_uwtGrupos__ctl0xx_5491_5491_133510_tblDesglose"
										iNumRows = Request("CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_tblDesglose")
										Dim conta As Integer = 1
										If iNumRows <> Nothing Then
											bDesgloseEmergente = False
											ReDim lineasReal(iNumRows)
											For numLinea As Integer = 1 To iNumRows
												For campoCalc As Integer = 0 To oDSDesglose.Tables(0).Rows.Count - 1
													If Not Request("CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + numLinea.ToString + "_" + oDSDesglose.Tables(0).Rows(campoCalc).Item("ID_CAMPO").ToString) Is Nothing Then
														lineasReal(conta) = numLinea
														Exit For
													ElseIf Not Request("CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_fsentry" + oDSDesglose.Tables(0).Rows(campoCalc).Item("ID_CAMPO").ToString) Is Nothing Then
														lineasReal(conta) = numLinea
														Exit For
													End If
												Next
												conta += 1
											Next
										Else
											bDesgloseEmergente = True
											iNumRows = Request("CALC_" + sPre + "_fsentry" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "__numTotRows")
											ReDim lineasReal(iNumRows)
											For numLinea As Integer = 1 To iNumRows
												For campoCalc As Integer = 0 To oDSDesglose.Tables(0).Rows.Count - 1
													If Not Request("CALC_" + sPre + "_fsentry" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "__" + numLinea.ToString + "__" + oDSDesglose.Tables(0).Rows(campoCalc).Item("ID_CAMPO").ToString) Is Nothing Then
														lineasReal(conta) = numLinea
														Exit For
													End If
												Next
												conta += 1
											Next
										End If
									End If
									bPopUp = True
								Else
									If Version.Value <> 0 Then
										ReDim lineasReal(iNumRows)
										Dim conta As Integer = 1
										For numLinea As Integer = 1 To iNumRows
											For campoCalc As Integer = 0 To oDSDesglose.Tables(0).Rows.Count - 1
												If Not Request("CALC_" + sPre + "_fsdsentry_" + numLinea.ToString + "_" + oDSDesglose.Tables(0).Rows(campoCalc).Item("ID_CAMPO").ToString) Is Nothing Then
													lineasReal(conta) = numLinea
													Exit For
												End If
											Next
											conta += 1
										Next
									End If
								End If
							Else
								'si el numero de lineas es 0 miro para ver si es pq el desglose esta oculto
								If oDSDesglose.Tables(1).Rows.Count > 0 Then
									For Each oDSRowDesgloseOculto In oDSDesglose.Tables(1).Rows
										If oDSRowDesgloseOculto.Item("Linea") > iNumRows Then
											iNumRows = oDSRowDesgloseOculto.Item("Linea")
											bEstaInvisibleElDesglose = True
										End If
									Next

									ReDim lineasReal(iNumRows) 'Acepta todas las lineas. Corporate / 2016 / 54. El grid dos columnas, no editables, solic vinculada y empresa
									For numLinea As Integer = 1 To iNumRows
										lineasReal(numLinea) = numLinea
									Next
								End If
							End If

							Dim EliminadoporNothing As Boolean = False
							For k = 1 To iNumRows
								If Version.Value = 0 OrElse lineasReal.Contains(k) Then
									EliminadoporNothing = False
									iDesglose = 0
									For Each oDSRowDesglose In oDSDesglose.Tables(0).Rows
										sVariablesDesglose(iDesglose) = oDSRowDesglose.Item("ID_CALCULO")
										bCalcular = True

										iTipoGSDesglose = DBNullToSomething(oDSRowDesglose.Item("TIPO_CAMPO_GS"))
										iTipoDesglose = oDSRow.Item("SUBTIPO")
										vValor = Nothing

										If (iTipoGSDesglose = TiposDeDatos.TipoCampoGS.PrecioUnitario AndAlso oDSRowDesglose.Item("VISIBLE") = 1) OrElse ((oDSRowDesglose.Item("ESCRITURA") = 1 And Version.Value > 0) Or (oDSRowDesglose.Item("VISIBLE") = 1 And Version.Value = 0)) And Not bEstaInvisibleElDesglose Then
											'''''Si es visible y el desglose esta visible
											'Si el desglose esta visible
											If Version.Value = 0 Then
												If bPopUp Then
													If bDesgloseEmergente Then
														sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ORIGEN_CALC_DESGLOSE") & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID")
													Else
														'CALC_uwtGrupos__ctl0xx_623_623_10549_fsdsentry_1_10551
														sRequest = "CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID").ToString
													End If
												Else
													'CALC_uwtGrupos__ctl0x_146_fsdsentry_1_2283
													sRequest = "CALC_" + sPre + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID").ToString()
												End If
											Else
												If bPopUp Then
													If bDesgloseEmergente Then
														sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE") & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID_CAMPO")
													Else 'CALC_uwtGrupos__ctl0xx_5491_5491_133510_fsdsentry_1_133486
														sRequest = "CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID_CAMPO").ToString
													End If
												Else 'CALC_uwtGrupos__ctl0x_146_fsdsentry_1_2283
													sRequest = "CALC_" + sPre + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID_CAMPO").ToString()
												End If
											End If
											vValor = Numero(VacioToNothing(Request(sRequest)), ".", ",")

											If Request(sRequest) Is Nothing Then
												EliminadoporNothing = True
											End If
										Else
											'si esta oculto bien pq no es visible o pq su campo padre el desglose esta oculto
											keysDesglose(0) = oDSDesglose.Tables(1).Columns("LINEA")
											keysDesglose(1) = oDSDesglose.Tables(1).Columns("CAMPO_PADRE")
											keysDesglose(2) = oDSDesglose.Tables(1).Columns("CAMPO_HIJO")

											oDSDesglose.Tables(1).PrimaryKey = keysDesglose
											iLinea = k
											findDesglose(0) = (k).ToString()
											If Version.Value = 0 Then
												findDesglose(1) = oDSRow.Item("ORIGEN_CALC_DESGLOSE")
												findDesglose(2) = oDSRowDesglose.Item("ID").ToString()
											Else
												findDesglose(1) = oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE")
												findDesglose(2) = oDSRowDesglose.Item("ID_CAMPO").ToString()
											End If
											oRowDesglose = oDSDesglose.Tables(1).Rows.Find(findDesglose)
											Dim iTmpLinea As Integer = iLinea
											While oRowDesglose Is Nothing And iTmpLinea > 0
												iTmpLinea -= 1
												findDesglose(0) = iTmpLinea
												oRowDesglose = oDSDesglose.Tables(1).Rows.Find(findDesglose)
											End While
											If Not oRowDesglose Is Nothing Then
												vValor = DBNullToSomething(oRowDesglose.Item("VALOR_NUM"))
											End If
										End If

										If oDSRowDesglose.Item("TIPO") = TipoCampoPredefinido.Calculado Then
											dValuesDesglose(iDesglose) = 0
										Else
											dValuesDesglose(iDesglose) = Numero(vValor)
										End If

										dValuesTotalDesglose(iDesglose) = CDec(dValuesTotalDesglose(iDesglose) + dValuesDesglose(iDesglose))
										iDesglose += 1
									Next
									iDesglose = 0
									For Each oDSRowDesglose In oDSDesglose.Tables(0).Rows
										Dim entryCalc As String = sPre + "_" & oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString() & "_"
										If oDSRowDesglose.Item("TIPO") = TipoCampoPredefinido.Calculado Then
											Try
												If Not EliminadoporNothing Then
													iEq.Parse(oDSRowDesglose.Item("FORMULA"), sVariablesDesglose)
													dValuesDesglose(iDesglose) = iEq.Evaluate(dValuesDesglose)
													iLinea = k
													findDesglose(0) = k.ToString()
													If Version.Value = 0 Then
														findDesglose(1) = oDSRow.Item("ORIGEN_CALC_DESGLOSE")
														findDesglose(2) = oDSRowDesglose.Item("ID").ToString()
													Else
														findDesglose(1) = oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE")
														findDesglose(2) = oDSRowDesglose.Item("ID_CAMPO").ToString()
													End If
													entryCalc += findDesglose(1).ToString() + "_fsdsentry_" + findDesglose(0).ToString() + "_" + findDesglose(2).ToString()
													oRowDesglose = Nothing
													oRowDesglose = oDTDesglose.Rows.Find(findDesglose)

													If Not oRowDesglose Is Nothing Then
														oRowDesglose.Item("VALOR_NUM") = dValuesDesglose(iDesglose)
														Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerCalculado_" + findDesglose(2).ToString(), "<script>ponerValorCampoCalculado('" + entryCalc + "'," + JSNum(dValuesDesglose(iDesglose)) + ",1);</script>")
														dValuesTotalDesglose(iDesglose) = CDec(dValuesTotalDesglose(iDesglose) + dValuesDesglose(iDesglose))
													Else
														'En la tabla temporal
														If bEstaInvisibleElDesglose OrElse (oDSRow.Item("ESCRITURA") = 0) Then
															dValuesTotalDesglose(iDesglose) = CDec(dValuesTotalDesglose(iDesglose) + dValuesDesglose(iDesglose))
														End If
													End If
												End If
											Catch ex As USPExpress.ParseException
											Catch e As Exception
											End Try
										End If
										iDesglose += 1
									Next
									ReDim Preserve sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count)
									ReDim Preserve dValuesDesglose(oDSDesglose.Tables(0).Rows.Count)

									Try
										sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count) = oDSRow.Item("FORMULA") + " * 1"
										iEq.Parse(oDSRow.Item("FORMULA"), sVariablesDesglose)

										dValuesDesglose(oDSDesglose.Tables(0).Rows.Count) = iEq.Evaluate(dValuesDesglose)
										dTotalDesglose = CDec(dTotalDesglose + dValuesDesglose(oDSDesglose.Tables(0).Rows.Count))
									Catch ex As USPExpress.ParseException
									Catch e As Exception
									End Try
								End If
							Next

							Dim entryImporte As String = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString()) + "_fsentry"
							If Not IsDBNull(oDSRow.Item("FORMULA")) Then
								Try
									If bCalcular = True Then
										If oDSRow.Item("ES_SUBCAMPO") = 0 Then
											dTotalDesglose = iEq.Evaluate(dValuesTotalDesglose)
										End If
										dValues(i) = dTotalDesglose
									End If

									If Version.Value = 0 Then
										find(0) = oDSRow.Item("ID")
									Else
										find(0) = oDSRow.Item("ID_CAMPO")
									End If
									entryImporte += find(0).ToString()
									oRow = dt.Rows.Find(find)
									If Not oRow Is Nothing Then
										oRow.Item("VALOR_NUM") = dValues(i)
									End If
								Catch ex As USPExpress.ParseException
								Catch e As Exception
								End Try
							End If

							If (oDSRow.Item("ES_IMPORTE_TOTAL") = 1 AndAlso oDSRow.Item("ES_SUBCAMPO") = 0) Then
								dTotal = dValues(i)
								Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerImporteTotal", "<script>ponerValorCampoCalculado('" + entryImporte + "'," + JSNum(dTotal) + ",1);</script>")
							End If
						Else
							dValues(i) = 0
						End If
					Else
						dValues(i) = Numero(vValor)
						If (oDSRow.Item("ES_IMPORTE_TOTAL") = 1 AndAlso oDSRow.Item("ES_SUBCAMPO") = 0) Then
							dTotal = dValues(i)
						End If
					End If
					i += 1
				Next
				i = 0

				For Each oDSRow In oDS.Tables(0).Rows
					sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString())
					If oDSRow.Item("TIPO") = TipoCampoPredefinido.Calculado Then
						If Version.Value = 0 Then
							find(0) = oDSRow.Item("ID")
						Else
							find(0) = oDSRow.Item("ID_CAMPO")
						End If
						oRow = dt.Rows.Find(find)

						Try
							iEq.Parse(oDSRow.Item("FORMULA"), sVariables)
							dValues(i) = iEq.Evaluate(dValues)
							If Not oRow Is Nothing Then
								oRow.Item("VALOR_NUM") = dValues(i)
							End If
						Catch ex As USPExpress.ParseException
						Catch e As Exception
						End Try
						If (DBNullToSomething(oDSRow.Item("ES_IMPORTE_TOTAL")) = 1 AndAlso DBNullToSomething(oDSRow.Item("ES_SUBCAMPO")) = 0) Then
							oRowWorkflow = oRow
						End If
					End If
					i += 1
				Next
				If Not oRowWorkflow Is Nothing Then dTotal = DBNullToSomething(oRowWorkflow.Item("VALOR_NUM"))
			End If  'end de Guardar=1

			ViewState("Importe") = dTotal

			If Not idAccion.Value = 0 Then 'SOLO PARA LOS CAMBIOS DE ETAPA
				If Guardar.Value = "1" Then
					Dim sAlmacenamiento As String = String.Empty
					Dim sConcepto As String = String.Empty
					'Recogemos el tipo de pedido seleccionado (IMP: sirve para las dos comprobaciones siguientes)
					If ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.TipoPedido).Length > 0 Then
						sAlmacenamiento = Request("ALMACENAMIENTO_" & ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.TipoPedido)(0).Item("campo"))
						sConcepto = Request("CONCEPTO_" & ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.TipoPedido)(0).Item("campo"))
					End If

					'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
					'Control de almac�n seleccionado, cuando el almacenamiento es obligatorio: 
					'   Si el tipo de pedido es de almacenamiento obligatorio ser� necesario seleccionar un almac�n para cada uno de los articulos seleccionados.
					'   - Si el campo de sistema art�culo se encuentra fuera del desglose, habr� que validar que se haya seleccionado un almac�n (campo fuera de desglose).
					'	- Si el campo de sistema art�culo se encuentra dentro de un desglose, habr� que validar que se haya seleccionado un almac�n para cada l�nea/art�culo del desglose.
					'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
					Dim iAlmacen As Integer = Nothing
					Dim bMostrarMensajeSeleccionarAlmacen As Boolean
					Dim bHayAlmacenEnElFormulario As Boolean
					Dim sCodArticulo As String = String.Empty
					Dim iTipoGS As Integer
					Dim drAlmacen() As DataRow

					drAlmacen = ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.Almacen)
					If drAlmacen.Length > 0 Then iAlmacen = DBNullToDbl(drAlmacen(0).Item("VALOR_NUM"))

					If ds.Tables("TEMP").Select("TIPOGS IN(" & TiposDeDatos.TipoCampoGS.NombreCertif & ") AND VALOR_TEXT IS NOT NULL").Count = 1 Then _
						sCodArticulo = DBNullToSomething(ds.Tables("TEMP").Select("TIPOGS IN(" & TiposDeDatos.TipoCampoGS.NombreCertif & ") AND VALOR_TEXT IS NOT NULL").First.Item("VALOR_TEXT"))

					'Si es solicitude de compra generamos un dataset para el posterior control de disponible
					If oInstancia.Solicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras Then _
						GenerarDataSetPartidasPresupuestarias()

					If sCodArticulo <> Nothing AndAlso sAlmacenamiento = "1" AndAlso iAlmacen = Nothing Then _
						bMostrarMensajeSeleccionarAlmacen = True

					If Not bMostrarMensajeSeleccionarAlmacen Then
						bHayAlmacenEnElFormulario = True 'partimos de que hay almacen en el desglose
						If sAlmacenamiento = "1" Then
							For Each oDSRow In ds.Tables("TEMPDESGLOSE").Rows
								iAlmacen = Nothing
								sCodArticulo = Nothing
								iTipoGS = DBNullToSomething(oDSRow.Item("TIPOGS"))

								If iTipoGS = TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
									sCodArticulo = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
									'obtenemos el almacen en esa linea/desglose
									tmpRow = ds.Tables("TEMPDESGLOSE").Select("LINEA=" & oDSRow.Item("LINEA") & " AND CAMPO_PADRE=" & oDSRow.Item("CAMPO_PADRE") & " AND TIPOGS=" & TiposDeDatos.TipoCampoGS.Almacen)
									If tmpRow.Length > 0 Then
										iAlmacen = DBNullToDbl(tmpRow(0).Item("VALOR_NUM"))
										If iAlmacen = Nothing Then
											bMostrarMensajeSeleccionarAlmacen = True
											Exit For
										End If
									Else
										bHayAlmacenEnElFormulario = False
									End If
								End If
							Next
						End If
					End If

					If bMostrarMensajeSeleccionarAlmacen OrElse Not bHayAlmacenEnElFormulario Then
						ModuloIdioma = TiposDeDatos.ModulosIdiomas.GuardarInstancia
						Page.ClientScript.RegisterStartupScript(Me.GetType(), "init", "<script>alert('" & JSText(Replace(Textos(10), "###", sCodArticulo)) & "');</script>")
						Return False
					End If

					'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
					'Control de almacenamiento: si el tipo de pedido es no almacenable, no se permitir� seleccionar un almac�n.
					'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
					If sAlmacenamiento = "0" Then 'No almacenable
						If ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.Almacen & " AND VALOR_NUM IS NOT NULL").Length > 0 _
							OrElse ds.Tables("TEMPDESGLOSE").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.Almacen & " AND VALOR_NUM IS NOT NULL").Length > 0 Then

							ModuloIdioma = TiposDeDatos.ModulosIdiomas.GuardarInstancia
							Page.ClientScript.RegisterStartupScript(Me.GetType(), "init", "<script>alert('" & JSText(Replace(Textos(11), "###", sCodArticulo)) & "');</script>")
							Return False
						End If
					End If
					'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
					'Control del concepto del tipo de pedido. Si el tipo de pedido es de solo "gasto", no debe seleccionar un activo
					'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
					If sConcepto = "0" Then
						'Miramos en los campos simples y en los campos del desglose
						If ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.Activo & " AND VALOR_TEXT IS NOT NULL").Length > 0 _
							OrElse ds.Tables("TEMPDESGLOSE").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.Activo & " AND VALOR_TEXT IS NOT NULL").Length > 0 Then
							ModuloIdioma = TiposDeDatos.ModulosIdiomas.GuardarInstancia
							Page.ClientScript.RegisterStartupScript(Me.GetType(), "init", "<script>alert('" & JSText(Textos(24)) & "');</script>")
							Return False
						End If
					End If

					'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
					'Control de A�o partida: Partida en campo
					'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
					If ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.AnyoPartida & " AND VALOR_NUM IS NULL").Length > 0 _
						AndAlso ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.Partida & " AND VALOR_TEXT IS NOT NULL").Length > 0 Then
						ModuloIdioma = TiposDeDatos.ModulosIdiomas.GuardarInstancia
						'Debe cumplimentar el a�o de imputaci�n. Descrip_campo_a�o
						'Debemos obtener el titulo del campo 
						Dim infoCampoAnyoPartida As DataRow
						If blnAlta Then
							infoCampoAnyoPartida = oInstancia.GetCampoDef(ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.AnyoPartida & " AND VALOR_NUM IS NULL")(0)("CAMPO"))
						Else
							infoCampoAnyoPartida = oInstancia.GetCampoDef(ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.AnyoPartida & " AND VALOR_NUM IS NULL")(0)("CAMPO_DEF"))
						End If
						If Not String.IsNullOrEmpty(infoCampoAnyoPartida("PARTIDAPRES").ToString) _
							AndAlso infoCampoAnyoPartida("PARTIDAPRES") = ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.Partida & " AND VALOR_TEXT IS NOT NULL")(0)("CAMPO") Then
							Page.ClientScript.RegisterStartupScript(Me.GetType(), "init", "<script>alert('" & JSText(Replace(Textos(26), "###",
																															 infoCampoAnyoPartida("DEN_" & FSNUser.Idioma.ToString))) & "');</script>")
							Return False
						End If
					End If
					If ds.Tables("TEMPDESGLOSE").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.AnyoPartida & " AND VALOR_NUM IS NULL").Length > 0 _
						AndAlso ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.Partida & " AND VALOR_TEXT IS NOT NULL").Length > 0 Then
						ModuloIdioma = TiposDeDatos.ModulosIdiomas.GuardarInstancia
						'Debemos obtener el titulo del campo 
						Dim infoCampoAnyoPartida As DataRow
						If blnAlta Then
							infoCampoAnyoPartida = oInstancia.GetCampoDef(ds.Tables("TEMPDESGLOSE").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.AnyoPartida & " AND VALOR_NUM IS NULL")(0)("CAMPO_HIJO"))
						Else
							infoCampoAnyoPartida = oInstancia.GetCampoDef(ds.Tables("TEMPDESGLOSE").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.AnyoPartida & " AND VALOR_NUM IS NULL")(0)("CAMPO_DEF"))
						End If
						If Not String.IsNullOrEmpty(infoCampoAnyoPartida("PARTIDAPRES").ToString) _
							AndAlso infoCampoAnyoPartida("PARTIDAPRES") = ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.Partida & " AND VALOR_TEXT IS NOT NULL")(0)("CAMPO") Then
							Page.ClientScript.RegisterStartupScript(Me.GetType(), "init", "<script>alert('" & JSText(Replace(Textos(26), "###",
																													infoCampoAnyoPartida("DEN_" & FSNUser.Idioma.ToString))) & "');</script>")
							Return False
						End If
					End If

					'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
					'Control de vigencia de fechas: Comprobamos que la fecha de vigencia del contrato  se encuentra dentro de las fechas de inicio y fin de suministro
					'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
					Dim sPRES5 As String = String.Empty
					Dim sDenominacion As String = String.Empty
					Dim bCumpleControlFechas As Boolean = True
					Dim dFechaIniSuministro As Date
					Dim dFechaFinSuministro As Date
					Dim sPartida As String = String.Empty
					Dim oPartida As FSNServer.Partida
					Dim configuracion As FSNServer.PargenSM

					tmpRow = ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.Partida & " AND VALOR_TEXT IS NOT NULL")
					If tmpRow.Length > 0 Then
						sPRES5 = sPRES5 & Request("PRES5_" & tmpRow(0).Item("CAMPO")) & ","
						sPartida = Right(tmpRow(0).Item("VALOR_TEXT"), Len(tmpRow(0).Item("VALOR_TEXT")) - InStrRev(tmpRow(0).Item("VALOR_TEXT"), "#")) & ","
					End If

					tmpRow = ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.IniSuministro)
					If tmpRow.Length > 0 Then
						dFechaIniSuministro = DBNullToSomething(tmpRow(0).Item("VALOR_FEC"))
					End If

					tmpRow = ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.FinSuministro)
					If tmpRow.Length > 0 Then
						dFechaFinSuministro = DBNullToSomething(tmpRow(0).Item("VALOR_FEC"))
					End If

					If Len(sPRES5) > 0 Then
						If (dFechaIniSuministro = Nothing AndAlso dFechaFinSuministro = Nothing) Then
							'Comprobar vigencia respecto del d�a de hoy
							dFechaIniSuministro = Today
							dFechaFinSuministro = Today
						End If
						Dim sPresupuestos As Object = Split(sPRES5, ",")
						Dim sPartidas As Object = Split(sPartida, ",")
						Dim index As Integer = 0
						Do While index <= UBound(sPresupuestos)
							configuracion = Session("FSSMPargen").Item(sPresupuestos(index))
							If Not configuracion Is Nothing Then
								If configuracion.Vigencia = 1 OrElse configuracion.Vigencia = 2 Then
									oPartida = FSNServer.Get_Object(GetType(FSNServer.Partida))
									If Not oPartida.CumpleControlFechas(sPresupuestos(index), sPartidas(index), dFechaIniSuministro, dFechaFinSuministro) Then
										sPRES5 = sPresupuestos(index)
										bCumpleControlFechas = False
										Exit Do
									End If
								End If
							End If
							index = index + 1
						Loop
					End If

					If bCumpleControlFechas Then
						Dim iNumFilas As Integer = 0
						Dim distinctCounts As IEnumerable(Of Int32)
						Dim lineasReal As Integer()
						Dim iMaxIndex As Integer = 0
						Dim findDesglose(0) As Object

						If ds.Tables("TEMPDESGLOSE").Rows.Count > 0 Then
							iMaxIndex = ds.Tables("TEMPDESGLOSE").Select("", "LINEA DESC")(0).Item("LINEA")
							distinctCounts = From row In ds.Tables("TEMPDESGLOSE")
											 Select row.Field(Of Int32)("LINEA")
											 Distinct
							iNumFilas = distinctCounts.Count
							ReDim lineasReal(iNumFilas - 1)
							For contador As Integer = 0 To iNumFilas - 1
								lineasReal(contador) = distinctCounts(contador)
							Next
						End If
						For i = 1 To iMaxIndex
							If lineasReal.Contains(i) Then
								sPRES5 = Nothing
								sPartida = Nothing
								dFechaIniSuministro = Nothing
								dFechaFinSuministro = Nothing

								tmpRow = ds.Tables("TEMPDESGLOSE").Select("LINEA=" & i & " AND TIPOGS=" & TiposDeDatos.TipoCampoGS.Partida)
								If tmpRow.Length > 0 Then
									sPRES5 = Request("DPRES5_" & tmpRow(0).Item("CAMPO_PADRE") & "_" & tmpRow(0).Item("CAMPO_HIJO") & "_" & tmpRow(0).Item("LINEA") & "_1")
									sPartida = DBNullToSomething(tmpRow(0).Item("VALOR_TEXT"))
									If Len(sPartida) > 0 Then
										sPartida = Right(sPartida, Len(sPartida) - InStrRev(sPartida, "#"))
									End If
								End If
								If Not String.IsNullOrEmpty(sPRES5) Then
									'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
									'Control de A�o partida: Partida en desglose
									'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
									tmpRow = ds.Tables("TEMPDESGLOSE").Select("LINEA=" & i & " AND TIPOGS=" & TiposDeDatos.TipoCampoGS.AnyoPartida & " AND VALOR_NUM IS NULL AND CAMPO_PADRE =" & tmpRow(0).Item("CAMPO_PADRE"))
									If tmpRow.Length > 0 AndAlso Not String.IsNullOrEmpty(sPartida) Then
										ModuloIdioma = TiposDeDatos.ModulosIdiomas.GuardarInstancia
										Page.ClientScript.RegisterStartupScript(Me.GetType(), "init", "<script>alert('" & JSText(Replace(Textos(26), "###",
													oInstancia.GetCampoDef(ds.Tables("TEMPDESGLOSE").Select("LINEA=" & i &
																									" AND TIPOGS=" & TiposDeDatos.TipoCampoGS.AnyoPartida & " AND VALOR_NUM IS NULL " &
																									"AND CAMPO_PADRE =" & tmpRow(0).Item("CAMPO_PADRE"))(0)("CAMPO_DEF"))("DEN_" & FSNUser.Idioma.ToString))) &
													"');</script>")
										Return False
									End If

									tmpRow = ds.Tables("TEMPDESGLOSE").Select("LINEA=" & i & " AND TIPOGS=" & TiposDeDatos.TipoCampoGS.IniSuministro)
									If tmpRow.Length > 0 Then
										dFechaIniSuministro = DBNullToSomething(tmpRow(0).Item("VALOR_FEC"))
									End If

									tmpRow = ds.Tables("TEMPDESGLOSE").Select("LINEA=" & i & " AND TIPOGS=" & TiposDeDatos.TipoCampoGS.FinSuministro)
									If tmpRow.Length > 0 Then
										dFechaFinSuministro = DBNullToSomething(tmpRow(0).Item("VALOR_FEC"))
									End If

									If Not sPartida Is Nothing AndAlso sPartida.Length > 0 Then
										If (dFechaIniSuministro = Nothing AndAlso dFechaFinSuministro = Nothing) Then
											'Comprobar vigencia respecto del d�a de hoy
											dFechaIniSuministro = Today
											dFechaFinSuministro = Today
										End If
										configuracion = Session("FSSMPargen").Item(sPRES5)
										If Not configuracion Is Nothing Then
											If configuracion.Vigencia = 1 OrElse configuracion.Vigencia = 2 Then
												oPartida = FSNServer.Get_Object(GetType(FSNServer.Partida))
												If Not oPartida.CumpleControlFechas(sPRES5, sPartida, dFechaIniSuministro, dFechaFinSuministro) Then
													bCumpleControlFechas = False
													Exit For
												End If
											End If
										End If
									End If
								End If
							End If
						Next
					End If

					If Not bCumpleControlFechas Then
						oPartida = FSNServer.Get_Object(GetType(FSNServer.Partida))
						sDenominacion = oPartida.ObtenerDenominacionPartida(sPRES5, Idioma)
						ModuloIdioma = TiposDeDatos.ModulosIdiomas.GuardarInstancia
						If configuracion.Vigencia = 1 Then 'AVISO
							Page.ClientScript.RegisterStartupScript(Me.GetType(), "init", "<script>alert('" & JSText(sDenominacion & ":" & Textos(8) & " " & Textos(9)) & "');</script>")
						ElseIf configuracion.Vigencia = 2 Then 'BLOQUEO
							Page.ClientScript.RegisterStartupScript(Me.GetType(), "init", "<script>alert('" & JSText(sDenominacion & ":" & Textos(7) & " " & Textos(8)) & "');</script>")
							Return False
						End If
					End If
				End If
			End If

			'Hacemos las comprobaciones pertinentes (Precondiciones y Mapper)
			'********* Precondiciones *******************************************************
			If Not idAccion.Value = 0 Then 'SOLO PARA LOS CAMBIOS DE ETAPA
				Dim dsPrecondiciones As DataSet

				oInstancia.Importe = dTotal
				If blnAlta Then
					dsPrecondiciones = oSolicitud.ObtenerPrecondicionesAccion(idAccion.Value, Idioma)
				Else
					dsPrecondiciones = oInstancia.ObtenerPrecondicionesAccion(idAccion.Value, Idioma, Version.Value)
				End If

				Dim dtAvisos As DataTable
				dtAvisos = ComprobarPrecondiciones(oInstancia, dsPrecondiciones, ds)
				If dtAvisos.Rows.Count > 0 Then
					'Mostrar los mensajes en un popup
					Dim i As Integer = 1
					For Each drAviso As DataRow In dtAvisos.Rows
						Dim oLabel As HtmlInputHidden = New HtmlInputHidden
						oLabel.Name = "Aviso"
						oLabel.ID = "Aviso" & i
						oLabel.Value = drAviso.Item("TIPO") & "|" & drAviso.Item("COD") & "|" & drAviso.Item("DEN")
						ContenedorAvisos.Controls.Add(oLabel)
						i = i + 1
					Next
					Page.ClientScript.RegisterStartupScript(Me.GetType(), "init", "<script>montarFormularioAvisos(); enviarAvisos();</script>")

					If dtAvisos.Rows.OfType(Of DataRow).Where(Function(x) x("TIPO") = 2).Any Then Return False
				End If
			End If
			'*****************************************************************************
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			''''''''''''''''''''''Control de Integraci�n de art�culos '''''''''''''''''''''''''''''''''''''''''''''
			'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' 
			If idAccion.Value <> 0 Then 'SOLO PARA LOS CAMBIOS DE ETAPA
				If Guardar.Value = "1" Then
					Dim bValidarIntegracion As Boolean
					'comprobar si la accion es de rechazo definitivo o anulaci�n
					bValidarIntegracion = Not oInstancia.AccionSinControlDisponible(idAccion.Value)

					If bValidarIntegracion Then
						Dim oIntegracion As FSNServer.Integracion
						oIntegracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))
						'Creamos dos conjuntos para almacenar los c�digos de art�culos diferentes presentes en la solicitud y as� no realizar comprobaciones
						' sobre integraci�n de un mismo art�culo.
						Dim aArticulosNoIntegrados As New HashSet(Of String)
						Dim aArticulosIntegrados As New HashSet(Of String)
						Dim texto As String = ""
						Dim sOrgCompras As String = ""
						Dim drOrgCompras() As DataRow
						'comprobar organizaci�n de compras

						If Me.Acceso.gbUsar_OrgCompras Then
							'comprobar si existe campo del tipo organizaci�n de compras
							' cabecera
							drOrgCompras = ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND VALOR_TEXT IS NOT NULL")
							If drOrgCompras.Length > 0 Then
								sOrgCompras = DBNullToSomething(drOrgCompras(0).Item("VALOR_TEXT"))
							End If

							' desglose
							If sOrgCompras = "" Then
								drOrgCompras = ds.Tables("TEMPDESGLOSE").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND VALOR_TEXT IS NOT NULL")
								If drOrgCompras.Length > 0 Then
									sOrgCompras = DBNullToSomething(drOrgCompras(0).Item("VALOR_TEXT"))
								End If
							End If

							If sOrgCompras <> "" Then
								'Extraer erp a partir de organizacion de compras
								Dim sErp As String = oIntegracion.getErpFromOrgCompras(sOrgCompras)
								If oIntegracion.hayIntegracionPM(sErp) Then
									If oIntegracion.hayIntegracionArticulos(sErp) Then
										For Each oDSRow In ds.Tables("TEMP").Rows
											If DBNullToSomething(oDSRow.Item("TIPOGS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
												'si el campo tipo articulo tiene contenido 
												If DBNullToStr(oDSRow.Item("VALOR_TEXT")) <> "" Then
													'verificamos que no haya sido comprobado ya
													If Not aArticulosNoIntegrados.Contains(oDSRow.Item("VALOR_TEXT")) AndAlso
														Not aArticulosIntegrados.Contains(oDSRow.Item("VALOR_TEXT")) Then
														If DBNullToStr(oDSRow.Item("VALOR_TEXT")) <> "" Then
															If Not oIntegracion.estaIntegradoArticulo(oDSRow.Item("VALOR_TEXT"), sErp) Then
																aArticulosNoIntegrados.Add(oDSRow.Item("VALOR_TEXT"))
															Else
																aArticulosIntegrados.Add(oDSRow.Item("VALOR_TEXT"))
															End If
														End If
													End If
												End If
											End If
										Next

										For Each oDSRow In ds.Tables("TEMPDESGLOSE").Rows
											If DBNullToSomething(oDSRow("TIPOGS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
												'si el campo tipo articulo tiene contenido y no se encuentra entre los ya comprobados
												If DBNullToStr(oDSRow.Item("VALOR_TEXT")) <> "" Then
													If Not aArticulosNoIntegrados.Contains(oDSRow.Item("VALOR_TEXT")) AndAlso
														Not aArticulosIntegrados.Contains(oDSRow.Item("VALOR_TEXT")) Then
														If (DBNullToStr(oDSRow.Item("VALOR_TEXT")) <> "") Then
															If Not oIntegracion.estaIntegradoArticulo(oDSRow.Item("VALOR_TEXT"), sErp) Then
																aArticulosNoIntegrados.Add(oDSRow.Item("VALOR_TEXT"))
															Else
																aArticulosIntegrados.Add(oDSRow.Item("VALOR_TEXT"))
															End If
														End If
													End If
												End If
											End If
										Next
										If aArticulosNoIntegrados.Count > 0 Then
											ModuloIdioma = TiposDeDatos.ModulosIdiomas.GuardarInstancia
											Dim arts As String = String.Join(",", aArticulosNoIntegrados.ToArray)
											Dim textoIntegracion = Replace(Me.Textos(25), "\n", Chr(10)) & arts  ' "�Imposible realizar acci�n! \n Los siguientes art�culos no se encuentran correctamente integrados : " 
											Page.ClientScript.RegisterStartupScript(Me.GetType(), "init", "<script>alert('" & JSText(textoIntegracion) & "');</script>")
											Return False
										End If
									End If
								End If
							End If
						End If
					End If
				End If
			End If
			'si ha pasado las comprobaciones inicilizamos los campos de validaci�n a NULL
			oInstancia.ActualizarEstadoValidacion(EstadoValidacion.SinEstado, Nothing)

			Return True 'Este uno nos dir� que ha pasado las comprobaciones
		Catch ex As Exception
			Dim oFileW As System.IO.StreamWriter
			oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & "\log_" & FSNUser.Cod & ".txt", True)
			oFileW.WriteLine()
			oFileW.WriteLine("Catch GenerarDataSetyHacerComprobaciones: " & ex.Message & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
			oFileW.Close()

			'guardamos el objeto exception en una variable de session para que la trate la p�gina de errores.
			Session("Exception") = ex
			Page.ClientScript.RegisterStartupScript(Me.GetType(), "MostrarPantallaError", "<script>window.parent.fraWSMain.location = '" & ConfigurationManager.AppSettings("rutaFS") & "/_Common/Errores/errores.aspx?pagina=" & Request.Url.ToString & "';</script>")
		End Try
	End Function
	''' Revisado por: blp. Fecha:14/11/2011
	''' <summary>
	''' Pone la instancia en proceso y completa el XML que se enviar� el servicio
	''' </summary>
	''' <remarks>Llamada desde: Page_Load, cmdAceptar_ServerClick, cmdAceptarDevolucion_ServerClick, cmdAceptarTrasladoUsu_ServerClick; Tiempo m�ximo: 0,5 sg</remarks>
	Private Sub GuardarConWorkflowThread(Optional ByVal NoEstaGuardando As Boolean = True)
		'Inicio de la Zona de C�digo al que los Threads deben entrar de uno en uno
		Dim xmlName As String = FSNUser.Cod & "#" & Instancia.Value & "#" & Bloque.Value
		Try
			Dim lIDTiempoProc As Long
			oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, FSNUser.Cod, Bloque.Value)

			'Leer el XML y meterlo en el dataset
			If NoEstaGuardando Then
				ds = New DataSet
				ds = CType(Session(NombreXML.Value), DataSet)
				Session.Remove(NombreXML.Value)
			End If

			If Guardar.Value = "1" Then
				If Not dsRoles Is Nothing Then
					If Not dsRoles.Tables("ROLES") Is Nothing Then
						'Puede ocurrir que sea rol establecido en la etapa X usando campo de pantalla Y para
						'la etapa X+1, no dandolo entre los datos de la solicitud pero si en la pantalla de
						'confirmar envio. Entonces, establece el copia_campo.valor_text y pm_copia_rol.per/prove
						Dim oRow As DataRow
						Dim dCampos() As DataRow
						For Each oRow In dsRoles.Tables("ROLES").Rows
							If Not IsDBNull(oRow.Item("CAMPO")) Then
								If oRow.Item("CAMPO") <> "" Then
									dCampos = ds.Tables("TEMP").Select("CAMPO = " & oRow.Item("CAMPO"))
									If dCampos.Length > 0 AndAlso DBNullToStr(dCampos(0)("VALOR_TEXT")) = "" Then
										If DBNullToStr(oRow.Item("PER")) <> "" Then
											dCampos(0)("VALOR_TEXT") = oRow.Item("PER")
										ElseIf DBNullToStr(oRow.Item("PROVE")) <> "" Then
											dCampos(0)("VALOR_TEXT") = oRow.Item("PROVE")
										End If
									End If
								End If
							End If
						Next
					End If
				End If
			End If

			With ds.Tables("SOLICITUD").Rows(0)
				Dim fechageneral As String
				Dim fechageneralcontrol As String
				fechageneral = "01-01-0001"
				'El control fecha general se inicializa con '12.00.00 AM' 
				'Para comprobar ese caso se hace esta modificaci�n
				fechageneralcontrol = CType(GeneralFecha.Valor, Date).ToString("dd-MM-yyyy")
				If fechageneralcontrol = fechageneral Then
					.Item("TRASLADO_FECHA") = ""
				Else
					.Item("TRASLADO_FECHA") = GeneralFecha.Valor
				End If
				If txtComentTrasladoUsu.InnerText.Length > 4000 Then
					.Item("TRASLADO_COMENTARIO") = Left(txtComentTrasladoUsu.InnerText, 4000)
				Else
					.Item("TRASLADO_COMENTARIO") = txtComentTrasladoUsu.InnerText
				End If
				If txtComentDevolucion.InnerText.Length > 4000 Then
					.Item("DEVOLUCION_COMENTARIO") = Left(txtComentDevolucion.InnerText, 4000)
				Else
					.Item("DEVOLUCION_COMENTARIO") = txtComentDevolucion.InnerText
				End If
				If txtComent.InnerText.Length > 4000 Then
					.Item("COMENTARIO") = Left(txtComent.InnerText, 4000)
				Else
					.Item("COMENTARIO") = txtComent.InnerText
				End If
				.Item("TRASLADO_USUARIO") = CodPer.Value
				.Item("TRASLADO_PROVEEDOR") = txtCod.Text
				.Item("TRASLADO_PROVEEDOR_CONTACTO") = IdRowContacto.Value
				.Item("IDTIEMPOPROC") = lIDTiempoProc
			End With

			If Not dsRoles Is Nothing Then
				If Not dsRoles.Tables("ROLES") Is Nothing Then
					ds.Tables.Add(dsRoles.Tables("ROLES").Copy)
					dsRoles.Clear()
				End If
			End If

			If Not String.IsNullOrEmpty(NewInstancia.Value) Then
				oInstancia.ID = NewInstancia.Value
				oInstancia.Actualizar_En_proceso(1, Bloque.Value)
			End If

			Try
				'Cargar XML completo
				oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=TiempoProcesamiento.InicioDisco)
				If ConfigurationManager.AppSettings("SERVIDOR_TRATAMIENTO_INSTANCIAS") = "0" Then
					'Se hace con un StringWriter porque el m�todo GetXml del dataset no incluye el esquema
					Dim oSW As New System.IO.StringWriter()
					ds.WriteXml(oSW, XmlWriteMode.WriteSchema)

					Dim oSrvTrataInst As New FSNWebServiceXML.TratamientoInstancias
					oSrvTrataInst.TransferirXMLEnEpera(oSW.ToString(), xmlName)
				Else
					ds.WriteXml(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#FSNWEB.xml", XmlWriteMode.WriteSchema)
					If File.Exists(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml") Then _
						File.Delete(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
					FileSystem.Rename(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#FSNWEB.xml",
									  ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
				End If
			Catch ex As Exception
				'Aqui borrar el registro de tiempo de procesamiento
				oInstancia.BorrarTiempoProcesamiento(lIDTiempoProc)
				Throw ex
			End Try
		Catch ex As Exception
			Dim oFileW As System.IO.StreamWriter
			oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & "\log_" & FSNUser.Cod & ".txt", True)
			oFileW.WriteLine()
			oFileW.WriteLine("Error Thread: " & ex.Message() & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
			oFileW.Close()
		End Try
	End Sub
	''' <summary>
	''' Genera un XML con los datos de la instancia y lo deja en RutaXML con el nombre: usu#instancia#bloque#n#letra#.xml
	''' </summary>
	''' <remarks>Llamada desde: Page_Load; Tiempo m�ximo: 0,2 sg.</remarks>
	Private Sub GuardarConWorkflowXML(Optional ByVal NoEstaGuardando As Boolean = True)
		Try
			NombreXML.Value = FSNUser.Cod & "#" & Instancia.Value & "#" & Bloque.Value & "#FSNWEB.xml"
			'Si para esta instancia hay algun otro bloque en proceso es que se esta procesando en paralelo
			'Generaremos el XML si el completar formulario, que se hara en el servicio windows antes de tratarsem (TAREA 3571)
			Dim EtapaEnParaleloEnProceso As Boolean = oInstancia.ComprobarEtapasEnParaleloEnProceso(Bloque.Value)
			If EtapaEnParaleloEnProceso Then ds = dsDatosSolicitud.Copy

			Dim dt As New DataTable
			With dt.Columns
				.Add("TIPO_PROCESAMIENTO_XML")
				.Add("TIPO_DE_SOLICITUD")
				.Add("COMPLETO")
				.Add("CODIGOUSUARIO")
				.Add("INSTANCIA")
				.Add("SOLICITUD")
				.Add("USUARIO")
				.Add("USUARIO_EMAIL")
				.Add("USUARIO_IDIOMA")
				.Add("PEDIDO_DIRECTO")
				.Add("FORMULARIO")
				.Add("WORKFLOW")
				.Add("IMPORTE", System.Type.GetType("System.Double"))
				.Add("COMENTALTANOCONF")
				.Add("ACCION")
				.Add("IDACCION")
				.Add("IDACCIONFORM")
				.Add("GUARDAR")
				.Add("NOTIFICAR")
				.Add("TRASLADO_USUARIO")
				.Add("TRASLADO_PROVEEDOR")
				.Add("TRASLADO_FECHA")
				.Add("TRASLADO_COMENTARIO")
				.Add("TRASLADO_PROVEEDOR_CONTACTO")
				.Add("DEVOLUCION_COMENTARIO")
				.Add("COMENTARIO")
				.Add("BLOQUE_ORIGEN")
				.Add("NUEVO_ID_INSTANCIA")
				.Add("BLOQUES_DESTINO")
				.Add("ROL_ACTUAL")
				.Add("NOCONFORMIDAD")       '27
				.Add("CERTIFICADO")         '28
				.Add("CONTRATO")            '29
				.Add("FACTURA")             '30
				.Add("PEDIDO")          '31
				.Add("IDTIEMPOPROC")
			End With
			Dim drSolicitud As DataRow
			drSolicitud = dt.NewRow
			With drSolicitud
				.Item("TIPO_PROCESAMIENTO_XML") = CInt(TipoProcesamientoXML.FSNWeb)
				.Item("TIPO_DE_SOLICITUD") = CInt(oInstancia.Solicitud.TipoSolicit)
				.Item("COMPLETO") = Convert.ToInt32(Not EtapaEnParaleloEnProceso)
				.Item("CODIGOUSUARIO") = FSNUser.Cod
				.Item("INSTANCIA") = Instancia.Value
				.Item("SOLICITUD") = Solicitud.Value
				.Item("USUARIO") = FSNUser.CodPersona
				.Item("USUARIO_EMAIL") = DBNullToStr(FSNUser.Email)
				.Item("USUARIO_IDIOMA") = FSNUser.Idioma.ToString()
				.Item("PEDIDO_DIRECTO") = PedidoDirecto.Value
				.Item("FORMULARIO") = Formulario.Value
				.Item("WORKFLOW") = Workflow.Value
				.Item("IMPORTE") = ViewState("Importe")
				.Item("COMENTALTANOCONF") = ComentAltaNoConf.Value
				.Item("ACCION") = Accion.Value
				.Item("IDACCION") = idAccion.Value
				.Item("IDACCIONFORM") = idAccion.Value
				.Item("GUARDAR") = Guardar.Value
				.Item("NOTIFICAR") = Notificar.Value
				.Item("TRASLADO_USUARIO") = CodPer.Value
				.Item("TRASLADO_PROVEEDOR") = txtCod.Text
				Dim fechageneral As String
				Dim fechageneralcontrol As String
				fechageneral = "01-01-0001"
				'El control fecha general se inicializa con '12.00.00 AM' 
				'Para comprobar ese caso se hace esta modificaci�n
				fechageneralcontrol = CType(GeneralFecha.Valor, Date).ToString("dd-MM-yyyy")
				If fechageneralcontrol = fechageneral Then
					.Item("TRASLADO_FECHA") = ""
				Else
					.Item("TRASLADO_FECHA") = GeneralFecha.Valor
				End If
				If txtComentTrasladoUsu.InnerText.Length > 4000 Then
					.Item("TRASLADO_COMENTARIO") = Left(txtComentTrasladoUsu.InnerText, 4000)
				Else
					.Item("TRASLADO_COMENTARIO") = txtComentTrasladoUsu.InnerText
				End If
				If Not ugtxtContacto_.SelectedItem Is Nothing Then
					.Item("TRASLADO_PROVEEDOR_CONTACTO") = ugtxtContacto_.SelectedItem.Value
				Else
					.Item("TRASLADO_PROVEEDOR_CONTACTO") = String.Empty
				End If

				If txtComentDevolucion.InnerText.Length > 4000 Then
					.Item("DEVOLUCION_COMENTARIO") = Left(txtComentDevolucion.InnerText, 4000)
				Else
					.Item("DEVOLUCION_COMENTARIO") = txtComentDevolucion.InnerText
				End If
				If txtComent.InnerText.Length > 4000 Then
					.Item("COMENTARIO") = Left(txtComent.InnerText, 4000)
				Else
					.Item("COMENTARIO") = txtComent.InnerText
				End If
				.Item("BLOQUE_ORIGEN") = Bloque.Value
				.Item("NUEVO_ID_INSTANCIA") = NewInstancia.Value
				.Item("BLOQUES_DESTINO") = BloqueDestinoWS.Value
				.Item("ROL_ACTUAL") = RolActual.Value
				.Item("CONTRATO") = IIf(String.IsNullOrEmpty(Contrato.Value), 0, Contrato.Value)
				.Item("NOCONFORMIDAD") = IIf(String.IsNullOrEmpty(NoConformidad.Value), 0, NoConformidad.Value)
				.Item("IDTIEMPOPROC") = 0
			End With
			dt.Rows.Add(drSolicitud)
			dt.TableName = "SOLICITUD"

			If ds Is Nothing OrElse (Not ds Is Nothing AndAlso Not (Guardar.Value = "1")) Then ds = New DataSet
			ds.Tables.Add(dt.Copy)

			If NoEstaGuardando Then Session(NombreXML.Value) = ds
		Catch ex As Exception
			Throw ex
		End Try
	End Sub
	''' <summary>
	''' Al pulsar este bot�n el usuario confirma la acci�n a realizar sobre la solicitud.
	''' </summary>
	''' <remarks>Llamada desde: al pulsar el bot�n de aceptar de la confirmaci�n; Tiempo m�ximo: 0,1 sg.</remarks>
	Private Sub cmdAceptar_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAceptar.ServerClick
		oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
		oInstancia.ID = CType(Instancia.Value, Long)
		oInstancia.Actualizar_En_proceso(1, strToLong(Bloque.Value.ToString))

		'Primero comprobamos que todos los datos para un cambio de etapa son correctos, si es asi y hay activado control disponible lo comprobamos
		Dim bMostrarMensaje As Boolean
		If Accion.Value = "" Or Accion.Value = "guardarsolicitud" Or Accion.Value = "altacontrato" Then
			bMostrarMensaje = ComprobarBloqueosSolicitud(Formulario.Value, Instancia.Value)
		End If

		If (bMostrarMensaje And AvisoBloqueo.Value = TipoAvisoBloqueo.Bloqueo And (Accion.Value = "" Or Accion.Value = "guardarsolicitud")) Then
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "HabilitarBotones", "<script>DeshabilitarBotones();MostrarFSWSMain();parent.fraWSMain.HabilitarBotones();</script>")
			Exit Sub
		End If

		Dim bEjecutarAccion As Boolean = True
		'1� Comprueba que se hayan introducido todos los roles que estaban vac�os:
		Dim i As Integer
		For i = 0 To ugtxtRoles.Rows.Count - 1
			If ugtxtRoles.Rows(i).Cells.FromKey("ASIGNAR").Value = 1 _
			AndAlso ugtxtRoles.Rows(i).Cells.FromKey("PROVE").Value = "" _
			AndAlso ugtxtRoles.Rows(i).Cells.FromKey("PER").Value = "" _
			AndAlso ugtxtRoles.Rows(i).Cells.FromKey("COMO_ASIGNAR").Value <> 3 Then
				bEjecutarAccion = False
				Exit For
			End If
		Next

		If Not bEjecutarAccion Then
			Dim sClientTexts As String
			Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"

			'Pone bien los estilos
			For i = 0 To ugtxtRoles.Rows.Count - 1
				If ugtxtRoles.Rows(i).Cells.FromKey("PER").Value = Nothing And ugtxtRoles.Rows(i).Cells.FromKey("PROVE").Value = Nothing _
					And ugtxtRoles.Rows(i).Cells.FromKey("ASIGNAR").Value = 1 Then
					ugtxtRoles.Rows(i).Cells.FromKey("NOMBRE").Style.CssClass = "fntRequired"
				Else
					ugtxtRoles.Rows(i).Cells.FromKey("NOMBRE").Style.CssClass = ""
				End If
			Next i

			sClientTexts = ""
			sClientTexts += "alert(""" & JSText(txtIdiMsgbox.Value) & """)"
			sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
			Page.ClientScript.RegisterStartupScript(Me.GetType(), "VisibleLayer", "<script>document.getElementById('divDevolverSiguientesEtapas').style.display='';</script>")
			Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)
			Exit Sub
		End If

		blnEtapaConfirmada = True

		'1� Genera un dataset con los roles a los que se ha introducido persona o prove:
		Dim oRow As DataRow
		Dim dsAux As New DataSet
		dsAux.Tables.Add("ROLES")
		dsAux.Tables(0).Columns.Add("ROL")
		dsAux.Tables(0).Columns.Add("PER")
		dsAux.Tables(0).Columns.Add("PROVE")
		dsAux.Tables(0).Columns.Add("CON")
		dsAux.Tables(0).Columns.Add("TIPO")
		dsAux.Tables(0).Columns.Add("CAMPO")

		For i = 0 To ugtxtRoles.Rows.Count - 1
			If ugtxtRoles.Rows(i).Cells.FromKey("ASIGNAR").Value = 1 AndAlso Not ugtxtRoles.Rows(i).Cells.FromKey("COMO_ASIGNAR").Value = 2 Then
				oRow = dsAux.Tables(0).NewRow
				oRow.Item("ROL") = ugtxtRoles.Rows(i).Cells.FromKey("ROL").Value
				oRow.Item("PER") = ugtxtRoles.Rows(i).Cells.FromKey("PER").Value
				oRow.Item("PROVE") = ugtxtRoles.Rows(i).Cells.FromKey("PROVE").Value
				oRow.Item("CON") = IIf(ugtxtRoles.Rows(i).Cells.FromKey("CONTACTOS").Value = Nothing, ugtxtRoles.Rows(i).Cells.FromKey("CON").Value, ugtxtRoles.Rows(i).Cells.FromKey("CONTACTOS").Value)
				oRow.Item("TIPO") = ugtxtRoles.Rows(i).Cells.FromKey("TIPO").Value
				oRow.Item("CAMPO") = DBNullToStr(ugtxtRoles.Rows(i).Cells.FromKey("CAMPO").Value)
				dsAux.Tables(0).Rows.Add(oRow)
			End If
		Next
		dsRoles = dsAux.Copy()
		dsAux = Nothing

		'Ahora controlamos las partidas si existen (dsPartidas no Nothing)
		'Si es solicitude de compra y tiene imputacion controlamos el dispobible
		If CType(tipoSolicitud.Value, TiposDeDatos.TipoDeSolicitud) = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras Then
			If (oInstancia.AccionSinControlDisponible(idAccion.Value)) Then
				'Es un rechazo o anulacion / Eliminaria de las partidas el solicitado anteriormente guardado
				Dim oPRES5 As FSNServer.Partida
				oPRES5 = FSNServer.Get_Object(GetType(FSNServer.Partida))
				oPRES5.ActualizarImportesPartidaRechazoAnulacion(oInstancia.ID, FSNUser.Idioma.ToString)
			ElseIf dsDatosPartidas IsNot Nothing Then
				'Es un cambio de etapa
				'Si todas las partidas cumplen con el disponible actualizamos las tablas INSTANCIA_PRES5 y la tabla INSTANCIA_PRES5_DESG
				Select Case ActualizarPartidasPresupuestarias()
					Case EnumTipoControlDisponible.Aviso
						MostrarControlPartidas(EnumTipoControlDisponible.Aviso)
						Exit Sub
					Case EnumTipoControlDisponible.Bloqueo
						'Cancelamos los cambios incluso antes de mostrar el bloqueo debido a que podrian quedar los cambios hasta que de al boton cancelar, y mejor liberar cuanto antes
						CancelarCambiosPartidasPresupuestarias()
						MostrarControlPartidas(EnumTipoControlDisponible.Bloqueo)
						Exit Sub
					Case Else 'Todo ha ido bien, actualizamos las tablas INSTANCIA_PRES5 y la tabla INSTANCIA_PRES5_DESG
						ActualizarDatosPartidasPresupuestarias()
				End Select
			End If
		End If

		Page.ClientScript.RegisterStartupScript(Me.GetType(), "TodoOK", "<script>TodoOK(" & IIf(Me.Version.Value = 0, "true", "false") & "," & IIf(Contrato.Value <> "", "true", "false") & ", " & IIf(mi_body.Attributes("class") = "fondoGS", "1", "0") & ",'" & IIf(Me.hidVolver.Value <> "", Me.hidVolver.Value, "") & "');</script>")

		''Efectuamos cambio de etapa
		GuardarConWorkflowThread()
	End Sub
	Private Sub cmdAceptarControlPartida_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAceptarControlPartida.ServerClick
		oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
		oInstancia.ID = CType(Instancia.Value, Long)
		oInstancia.Actualizar_En_proceso(1, strToLong(Bloque.Value.ToString))

		Dim oPRES5 As FSNServer.Partida
		oPRES5 = FSNServer.Get_Object(GetType(FSNServer.Partida))
		Dim viewPartidas As New DataView(dsDatosPartidas.Tables("CONTROL"))
		Dim dtPartidas As DataTable = viewPartidas.ToTable(True, "ANYO", "PRES0", "PRES1", "PRES2", "PRES3", "PRES4", "OK")
		Dim confSM As FSNServer.PargensSMs = Nothing

		If Session("FSSMPargen") IsNot Nothing Then confSM = Session("FSSMPargen")

		'Forma de actuar. Primero actualizamos la tabla PRES5_IMPORTES. NO actualizamos IMPORTES_PRES5 ni IMPORTES_PRES5_DESG
		'Si todo va bien, actualizamos estas dos ultimas tablas
		'Si alguna partida no cumple disponible:
		'a)Si es bloqueo, utilizando los datos de IMPORTES_PRES5 echamos atras los cambios en las partidas de la instancia realizadas antes
		'b)Si es aviso, mostramos el aviso=>despues del resultado del aviso sabremos si echar atras los cambios o realizar todos
		Dim drResultadoFiltro() As DataRow
		Dim oResultado As Object
		Dim ControlDisponible As EnumTipoControlDisponible = EnumTipoControlDisponible.NoImputa
		For Each drPartida As DataRow In dtPartidas.Rows
			drResultadoFiltro = dsDatosPartidas.Tables("CONTROL") _
															.Select("PRES0='" & drPartida("PRES0") & "'" &
																	IIf(IsDBNull(drPartida("PRES1")), " AND PRES1 IS NULL", " AND PRES1='" & drPartida("PRES1") & "'") &
																	IIf(IsDBNull(drPartida("PRES2")), " AND PRES2 IS NULL", " AND PRES2='" & drPartida("PRES2") & "'") &
																	IIf(IsDBNull(drPartida("PRES3")), " AND PRES3 IS NULL", " AND PRES3='" & drPartida("PRES3") & "'") &
																	IIf(IsDBNull(drPartida("PRES4")), " AND PRES4 IS NULL", " AND PRES4='" & drPartida("PRES4") & "' " &
																	IIf(drPartida("ANYO") = 0, "", " AND ANYO=" & drPartida("ANYO")) & "AND OK=0"))
			'Si hay lineas para la partida dada y ademas se imputa en solicitudes de compra llamamos al stored de actualizar importes
			If drResultadoFiltro.Any AndAlso Not CType(confSM.Item(drPartida("PRES0")), PargenSM).ImputacionSolicitudCompra = PargenSM.EnumTipoImputacion.NoSeImputa Then
				oResultado = oPRES5.ActualizarImportesPartida(drPartida("PRES0"), drPartida("PRES1"), drPartida("PRES2"), drPartida("PRES3"), drPartida("PRES4"),
															confSM.Item(drPartida("PRES0")).ControlDispSC, CType(Instancia.Value, Long), MonedaInstancia,
															drResultadoFiltro.CopyToDataTable.Compute("SUM(TOTAL)", ""), FSNUser.Idioma.ToString, True, iAnyo:=drPartida("ANYO"))
				For Each drLinea As DataRow In drResultadoFiltro
					drLinea("COD_PARTIDA") = oResultado.CodPartida
					drLinea("DEN_PARTIDA") = oResultado.DenPartida
					drLinea("PRESUPUESTADO") = oResultado.Presupuestado
					drLinea("DISPONIBLE") = oResultado.Disponible
					drLinea("FC") = oResultado.FC
					drLinea("MON") = oResultado.Moneda
					drLinea("OK") = oResultado.OK
				Next
			End If
		Next
		ControlPartidas.Value = ""
		dsDatosPartidas = dsDatosPartidas.Copy
		ActualizarDatosPartidasPresupuestarias()

		Page.ClientScript.RegisterStartupScript(Me.GetType(), "TodoOK", "<script>TodoOK(" & IIf(Me.Version.Value = 0, "true", "false") & "," & IIf(Contrato.Value <> "", "true", "false") & ", " & IIf(mi_body.Attributes("class") = "fondoGS", "1", "0") & ",'" & IIf(Me.hidVolver.Value <> "", Me.hidVolver.Value, "") & "');</script>")

		'Efectuamos cambio de etapa
		GuardarConWorkflowThread()
	End Sub
	Private Sub cmdCancelarControlPartida_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancelarControlPartida.ServerClick
		ControlPartidas.Value = ""
		blnEtapaConfirmada = False
		blnControlPartidasMostrado = False

		'Solo cancelamos cambios si es un aviso, sino ya los hemos cancelado antes.
		'La forma de saber si es un aviso es viendo si puede aceptar en la pantalla, sino, es un bloqueo
		If cmdAceptarControlPartida.Visible Then CancelarCambiosPartidasPresupuestarias()
	End Sub
	''' <summary>
	''' Crea un tabla con los datos de las partidas a imputar en la solicitud. Se guarda en cache para poder gestionarla en las aceptaciones de los avisos.
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	Private Function GenerarDataSetPartidasPresupuestarias() As Boolean
		'Control partidas
		Dim bControlPartidaCampo, bControlPartidaDesglose, bAnyoPartidaCampo As Boolean
		Dim findLinea(1) As Object
		Dim iAnyo As Integer
		'Miramos si hay campo de partida presupuestaria en el formulario y luego por desglose
		bControlPartidaDesglose = False
		bControlPartidaCampo = False
		If dsDatosSolicitud.Tables("TEMP") IsNot Nothing _
			AndAlso dsDatosSolicitud.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.Partida).Length > 0 Then bControlPartidaCampo = True
		'Miramos si el a�o de la partida esta en el formulario
		If dsDatosSolicitud.Tables("TEMP") IsNot Nothing _
			AndAlso dsDatosSolicitud.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.AnyoPartida).Length > 0 Then
			bAnyoPartidaCampo = True
		End If

		'La partida esta en el desglose?
		If dsDatosSolicitud.Tables("TEMPDESGLOSE") IsNot Nothing _
			AndAlso dsDatosSolicitud.Tables("TEMPDESGLOSE").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.Partida).Length > 0 Then bControlPartidaDesglose = True

		'si hay campo de partida presupuestaria generamos una tabla donde guardaremos informacion para hacer el posible control de disponible
		Dim dtCP As DataTable = Nothing
		Dim dtNewRowControl As DataRow
		''Si en la solicitud hay alguna partida, haremos control de partidas...
		If (bControlPartidaCampo Or bControlPartidaDesglose) Then
			Dim key(1) As DataColumn
			dsControlPartidas = New DataSet
			dtCP = dsControlPartidas.Tables.Add("CONTROL")
			dtCP.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
			dtCP.Columns.Add("CAMPO_PADRE", System.Type.GetType("System.Int32"))
			dtCP.Columns.Add("ANYO", System.Type.GetType("System.Int32"))
			dtCP.Columns("ANYO").DefaultValue = 0
			dtCP.Columns.Add("PRES0", System.Type.GetType("System.String"))
			dtCP.Columns.Add("PRES1", System.Type.GetType("System.String"))
			dtCP.Columns.Add("PRES2", System.Type.GetType("System.String"))
			dtCP.Columns.Add("PRES3", System.Type.GetType("System.String"))
			dtCP.Columns.Add("PRES4", System.Type.GetType("System.String"))
			dtCP.Columns.Add("COD_ARTICULO", System.Type.GetType("System.String"))
			dtCP.Columns.Add("DEN_ARTICULO", System.Type.GetType("System.String"))
			dtCP.Columns.Add("CANT", System.Type.GetType("System.Double"))
			dtCP.Columns.Add("PRECIO_UNITARIO", System.Type.GetType("System.Double"))
			dtCP.Columns.Add("COD_PARTIDA", System.Type.GetType("System.String"))
			dtCP.Columns.Add("DEN_PARTIDA", System.Type.GetType("System.String"))
			dtCP.Columns.Add("PRESUPUESTADO", System.Type.GetType("System.Double"))
			dtCP.Columns.Add("COMPROMETIDO", System.Type.GetType("System.Double"))
			dtCP.Columns.Add("SOLICITADO", System.Type.GetType("System.Double"))
			dtCP.Columns.Add("FC", System.Type.GetType("System.Double"))
			dtCP.Columns.Add("MON", System.Type.GetType("System.String"))
			dtCP.Columns.Add("DISPONIBLE", System.Type.GetType("System.Double"))
			dtCP.Columns.Add("OK", System.Type.GetType("System.Boolean"))
			dtCP.Columns.Add("TOTAL", System.Type.GetType("System.Double"))

			key(0) = dtCP.Columns("LINEA")
			key(1) = dtCP.Columns("CAMPO_PADRE")
			dtCP.PrimaryKey = key
		End If

		Dim sAux() As String
		Dim arrPartida() As String
		Dim iNivel As Integer
		Dim sPres0 As String = ""
		Dim sPres1 As String = ""
		Dim sPres2 As String = ""
		Dim sPres3 As String = ""
		Dim sPres4 As String = ""
		'El dato de la partida presupuestaria esta a nivel de formulario y no en el desglose
		'Cogemos los datos de la partida para rellenar luego la partida en cada linea de desglose
		If bControlPartidaCampo AndAlso Not bControlPartidaDesglose Then
			For Each oDSRow As DataRow In ds.Tables("TEMP").Select(
					"TIPOGS IN(" & TiposDeDatos.TipoCampoGS.Partida & ") AND VALOR_TEXT IS NOT NULL")
				'Podemos buscar el valor de '00#00#00#01#001 en la coleccion y obtener sus valores!!!!
				'"00#00#00#01#003|011" (Nivel 2 de partida)
				sAux = Split(DBNullToSomething(oDSRow.Item("VALOR_TEXT")), "#")
				arrPartida = Split(sAux(UBound(sAux)), "|")
				iNivel = UBound(arrPartida) + 1

				sPres0 = Request(Request.Form.AllKeys.Where(Function(x) x.Split("_")(0) = "PRES5").First.ToString)
				If iNivel = 4 Then sPres4 = arrPartida(3)
				If iNivel >= 3 Then sPres3 = arrPartida(2)
				If iNivel >= 2 Then sPres2 = arrPartida(1)
				If iNivel >= 1 Then sPres1 = arrPartida(0)
			Next
		End If
		If bAnyoPartidaCampo Then
			If CType(Session("FSSMPargen").Item(sPres0), PargenSM).Plurianual AndAlso ds.Tables("TEMP").Select("TIPOGS IN(" & TiposDeDatos.TipoCampoGS.AnyoPartida & ") AND VALOR_NUM IS NOT NULL").Any Then
				iAnyo = DBNullToSomething(ds.Tables("TEMP").Select("TIPOGS IN(" & TiposDeDatos.TipoCampoGS.AnyoPartida & ") AND VALOR_NUM IS NOT NULL")(0)("VALOR_NUM"))
			End If
		End If
		' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		''Control de partidas
		' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		If bControlPartidaCampo OrElse bControlPartidaDesglose Then
			Dim dv As New DataView(ds.Tables("TEMPDESGLOSE"))
			Dim listaIdsDesglose As String = String.Join(",", ds.Tables("TEMPDESGLOSE").AsEnumerable.Select(Function(x) x("CAMPO_PADRE")).Distinct)
			'Si tenemos campo cantidad y campo precio unitario hacemos la gestion de partidas
			If dv.ToTable(True, "TIPOGS").Rows.OfType(Of DataRow).Where(Function(x) {TiposDeDatos.TipoCampoGS.Cantidad, TiposDeDatos.TipoCampoGS.PrecioUnitario}.Contains(DBNullToInteger(x("TIPOGS")))).ToList.Count = 2 Then
				Dim dtDatosImporteTotal As DataTable = Nothing
				If lSolicitud = 0 Then
					Dim oInstanciaImporteTotal As FSNServer.Instancia
					oInstanciaImporteTotal = FSNServer.Get_Object(GetType(FSNServer.Instancia))
					dtDatosImporteTotal = oInstanciaImporteTotal.ObtenerCamposImporteTotal(listaIdsDesglose)
				Else
					'Vamos a ver si hay algun campo del desglose marcado como que es importe total de la linea(formula aplicada)
					Dim oSolicitudImporteTotal As FSNServer.Solicitud
					oSolicitudImporteTotal = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
					dtDatosImporteTotal = oSolicitudImporteTotal.ObtenerCamposImporteTotal(listaIdsDesglose)
				End If
				For Each campoImporteTotal As DataRow In dtDatosImporteTotal.Rows
					For Each lineaImporte As DataRow In ds.Tables("TEMPDESGLOSE").Rows.OfType(Of DataRow).Where(Function(z) z("CAMPO_PADRE") = campoImporteTotal("CAMPO_PADRE") AndAlso z("CAMPO_HIJO") = campoImporteTotal("ID"))
						lineaImporte("ES_IMPORTE_TOTAL") = 1
					Next
				Next
				Dim ExisteCampoImporteTotal As Boolean
				For Each linea As DataRow In dv.ToTable(True, "LINEA", "CAMPO_PADRE").Rows.OfType(Of DataRow).Where(Function(y) Not y("LINEA") = 0)
					dtNewRowControl = dtCP.NewRow
					dtNewRowControl.Item("LINEA") = linea("LINEA")
					dtNewRowControl.Item("CAMPO_PADRE") = linea("CAMPO_PADRE")
					dtCP.Rows.Add(dtNewRowControl)
					Dim listaCamposPartidas As List(Of DataRow) = ds.Tables("TEMPDESGLOSE").Rows.OfType(Of DataRow).Where(Function(x) x("LINEA") = linea("LINEA") AndAlso x("CAMPO_PADRE") = linea("CAMPO_PADRE") _
													AndAlso (({TiposDeDatos.TipoCampoGS.NuevoCodArticulo,
																TiposDeDatos.TipoCampoGS.DenArticulo,
																TiposDeDatos.TipoCampoGS.Partida}.Contains(DBNullToInteger(x("TIPOGS"))) _
																AndAlso Not IsDBNull(x("VALOR_TEXT"))) _
														OrElse {TiposDeDatos.TipoCampoGS.Cantidad,
																TiposDeDatos.TipoCampoGS.PrecioUnitario,
																TiposDeDatos.TipoCampoGS.AnyoPartida}.Contains(DBNullToInteger(x("TIPOGS"))) _
														OrElse DBNullToInteger(x("ES_IMPORTE_TOTAL")) = 1)).ToList
					listaCamposPartidas.Sort(Function(k, l) String.Compare(DBNullToInteger(k("TIPOGS")), DBNullToInteger(l("TIPOGS"))))
					ExisteCampoImporteTotal = False
					For Each oDSRow In listaCamposPartidas
						Select Case DBNullToSomething(oDSRow.Item("TIPOGS"))
							Case TiposDeDatos.TipoCampoGS.Cantidad
								dtNewRowControl.Item("CANT") = DBNullToDbl(oDSRow.Item("VALOR_NUM"))
							Case TiposDeDatos.TipoCampoGS.PrecioUnitario
								If ExisteCampoImporteTotal Then
									dtNewRowControl.Item("PRECIO_UNITARIO") = CDec(DBNullToDbl(dtNewRowControl.Item("TOTAL")) / DBNullToDbl(dtNewRowControl.Item("CANT")))
								Else
									dtNewRowControl.Item("PRECIO_UNITARIO") = DBNullToDbl(oDSRow.Item("VALOR_NUM"))
									dtNewRowControl.Item("TOTAL") = CDec(DBNullToDbl(dtNewRowControl.Item("CANT")) * DBNullToDbl(dtNewRowControl.Item("PRECIO_UNITARIO")))
								End If
							Case TiposDeDatos.TipoCampoGS.DenArticulo
								dtNewRowControl.Item("DEN_ARTICULO") = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
							Case TiposDeDatos.TipoCampoGS.NuevoCodArticulo
								dtNewRowControl.Item("COD_ARTICULO") = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
							Case TiposDeDatos.TipoCampoGS.AnyoPartida
								dtNewRowControl.Item("ANYO") = DBNullToInteger(oDSRow.Item("VALOR_NUM"))
							Case TiposDeDatos.TipoCampoGS.Partida
								'SOLO ENTRA POR AQUI SI TIENE CAMPO PARTIDA PRESUPUESTARIA DENTRO DE DESGLOSE
								'Podemos buscar el valor de '00#00#00#01#001 en la coleccion y obtener sus valores!!!!
								'"00#00#00#01#003|011" (Nivel 2 de partida)
								sAux = Split(DBNullToSomething(oDSRow.Item("VALOR_TEXT")), "#")
								arrPartida = Split(sAux(UBound(sAux)), "|")
								iNivel = UBound(arrPartida) + 1

								sPres0 = Request(Request.Form.AllKeys.Where(Function(x) x = "DPRES5_" & oDSRow("CAMPO_PADRE") & "_" & oDSRow("CAMPO_HIJO") & "_" & oDSRow.Item("LINEA") & "_1").First.ToString).ToString
								If iNivel = 4 Then sPres4 = arrPartida(3)
								If iNivel >= 3 Then sPres3 = arrPartida(2)
								If iNivel >= 2 Then sPres2 = arrPartida(1)
								If iNivel >= 1 Then sPres1 = arrPartida(0)

								dtNewRowControl.Item("PRES0") = sPres0
								dtNewRowControl.Item("PRES1") = sPres1
								dtNewRowControl.Item("PRES2") = sPres2
								dtNewRowControl.Item("PRES3") = sPres3
								dtNewRowControl.Item("PRES4") = sPres4
							Case 0
								ExisteCampoImporteTotal = True
								dtNewRowControl.Item("TOTAL") = DBNullToDbl(oDSRow("VALOR_NUM"))
						End Select
					Next
				Next
			End If

			If bControlPartidaCampo AndAlso Not bControlPartidaDesglose Then
				'El control de la partida es en formulario y no en desglose. 
				'Por tanto, las variables sPres1--sPres4 tendran los valores de la partida a nivel formulario
				If dtCP.Rows.Count > 0 Then
					'Pone los valores de la partida a cada linea de desglose para hacer el control
					For Each dRow As DataRow In dtCP.Select("NOT (LINEA=0 AND CAMPO_PADRE=0)")
						If bAnyoPartidaCampo Then dRow.Item("ANYO") = iAnyo
						dRow.Item("PRES0") = sPres0
						dRow.Item("PRES1") = sPres1
						dRow.Item("PRES2") = sPres2
						dRow.Item("PRES3") = sPres3
						dRow.Item("PRES4") = sPres4
					Next
				Else
					dtNewRowControl = dtCP.NewRow
					dtNewRowControl.Item("LINEA") = 0
					dtNewRowControl.Item("CAMPO_PADRE") = 0
					If bAnyoPartidaCampo Then dtNewRowControl.Item("ANYO") = iAnyo
					dtNewRowControl.Item("PRES0") = sPres0
					dtNewRowControl.Item("PRES1") = sPres1
					dtNewRowControl.Item("PRES2") = sPres2
					dtNewRowControl.Item("PRES3") = sPres3
					dtNewRowControl.Item("PRES4") = sPres4
					dtNewRowControl.Item("TOTAL") = dTotal
					dtCP.Rows.Add(dtNewRowControl)
				End If
			End If
			dsDatosPartidas = dsControlPartidas.Copy
		End If
	End Function
	''' <summary>
	''' Con los datos existentes en la tabla guardada en cache con los datos de las partidas a imputar por la solicitud, efectuamos el guardado en base de datos
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	Private Function ActualizarPartidasPresupuestarias()
		Dim oPRES5 As FSNServer.Partida
		oPRES5 = FSNServer.Get_Object(GetType(FSNServer.Partida))
		Dim viewPartidas As New DataView(dsDatosPartidas.Tables("CONTROL"))
		Dim dtPartidas As DataTable = viewPartidas.ToTable(True, "ANYO", "PRES0", "PRES1", "PRES2", "PRES3", "PRES4", "OK")
		Dim confSM As FSNServer.PargensSMs = Nothing

		If Session("FSSMPargen") IsNot Nothing Then confSM = Session("FSSMPargen")

		'Forma de actuar. Primero actualizamos la tabla PRES5_IMPORTES. NO actualizamos IMPORTES_PRES5 ni IMPORTES_PRES5_DESG
		'Si todo va bien, actualizamos estas dos ultimas tablas
		'Si alguna partida no cumple disponible:
		'a)Si es bloqueo, utilizando los datos de IMPORTES_PRES5 echamos atras los cambios en las partidas de la instancia realizadas antes
		'b)Si es aviso, mostramos el aviso=>despues del resultado del aviso sabremos si echar atras los cambios o realizar todos
		Dim drResultadoFiltro() As DataRow
		Dim oResultado As Object
		Dim ControlDisponible As EnumTipoControlDisponible = EnumTipoControlDisponible.NoImputa
		For Each drPartida As DataRow In dtPartidas.Select("PRES0 IS NOT NULL AND NOT PRES0=''")
			drResultadoFiltro = dsDatosPartidas.Tables("CONTROL") _
															.Select("PRES0='" & drPartida("PRES0") & "'" &
																	IIf(IsDBNull(drPartida("PRES1")), " AND PRES1 IS NULL", " AND PRES1='" & drPartida("PRES1") & "'") &
																	IIf(IsDBNull(drPartida("PRES2")), " AND PRES2 IS NULL", " AND PRES2='" & drPartida("PRES2") & "'") &
																	IIf(IsDBNull(drPartida("PRES3")), " AND PRES3 IS NULL", " AND PRES3='" & drPartida("PRES3") & "'") &
																	IIf(IsDBNull(drPartida("PRES4")), " AND PRES4 IS NULL", " AND PRES4='" & drPartida("PRES4") & "'") &
																	IIf(CType(confSM.Item(drPartida("PRES0")), PargenSM).Plurianual AndAlso Not drPartida("ANYO") = 0, " AND ANYO=" & drPartida("ANYO"), ""))
			'Si hay lineas para la partida dada y ademas se imputa en solicitudes de compra llamamos al stored de actualizar importes
			If drResultadoFiltro.Any AndAlso Not CType(confSM.Item(drPartida("PRES0")), PargenSM).ImputacionSolicitudCompra = PargenSM.EnumTipoImputacion.NoSeImputa Then
				oResultado = oPRES5.ActualizarImportesPartida(drPartida("PRES0"), drPartida("PRES1"), drPartida("PRES2"), drPartida("PRES3"), drPartida("PRES4"), confSM.Item(drPartida("PRES0")).ControlDispSC,
												CType(Instancia.Value, Long), MonedaInstancia, drResultadoFiltro.CopyToDataTable.Compute("SUM(TOTAL)", ""),
												FSNUser.Idioma.ToString, False, bControlDispConPres:=CType(confSM.Item(drPartida("PRES0")), PargenSM).ControlDispConPres, iAnyo:=drPartida("ANYO"))
				For Each drLinea As DataRow In drResultadoFiltro
					drLinea("COD_PARTIDA") = oResultado.CodPartida
					drLinea("DEN_PARTIDA") = oResultado.DenPartida
					drLinea("PRESUPUESTADO") = oResultado.Presupuestado
					drLinea("DISPONIBLE") = oResultado.Disponible
					drLinea("FC") = oResultado.FC
					drLinea("MON") = oResultado.Moneda
					drLinea("OK") = oResultado.OK
				Next
				'Si no cumple el control del disponible mantendremos una variable de lo que hacer (aviso/bloqueo).
				'Si hay algun bloqueo prevalece sobre los avisos y no variaremos la variable
				If Not oResultado.OK AndAlso Not ControlDisponible = EnumTipoControlDisponible.Bloqueo Then
					If Not confSM.Item(drPartida("PRES0")).ControlDispSC = EnumTipoControlDisponible.NoImputa Then ControlDisponible = confSM.Item(drPartida("PRES0")).ControlDispSC
				End If
			End If
		Next
		ControlPartidas.Value = ""
		Dim totalPartida As Double
		For Each partidaError As DataRow In viewPartidas.ToTable(True, "PRES0", "PRES1", "PRES2", "PRES3", "PRES4", "OK").Select("PRES0 IS NOT NULL AND OK IS NOT NULL").AsEnumerable.Where(Function(x) Not x("OK"))
			totalPartida = 0
			For Each lineaPartida As DataRow In dsDatosPartidas.Tables("CONTROL") _
								.Select("PRES0='" & partidaError("PRES0") & "'" &
										IIf(IsDBNull(partidaError("PRES1")), " AND PRES1 IS NULL", " AND PRES1='" & partidaError("PRES1") & "'") &
										IIf(IsDBNull(partidaError("PRES2")), " AND PRES2 IS NULL", " AND PRES2='" & partidaError("PRES2") & "'") &
										IIf(IsDBNull(partidaError("PRES3")), " AND PRES3 IS NULL", " AND PRES3='" & partidaError("PRES3") & "'") &
										IIf(IsDBNull(partidaError("PRES4")), " AND PRES4 IS NULL", " AND PRES4='" & partidaError("PRES4") & "'"))
				totalPartida = CDec(totalPartida + CDec(lineaPartida("TOTAL") * lineaPartida("FC")))
			Next

			ControlPartidas.Value &= partidaError("PRES0") & "#" & partidaError("PRES1") & "#" & partidaError("PRES2") & "#" & partidaError("PRES3") & "#" & partidaError("PRES4") & "#" & totalPartida & "|"
		Next
		dsDatosPartidas = dsDatosPartidas.Copy
		Return ControlDisponible
	End Function
	''' <summary>
	''' Si todo ha ido bien al imputar a las partidas y no hay problemas con el control del disponible actualizamos la tabla INSTANCIA_PRES5 y la tabla INSTANCIA_PRES5_DESGLOSE
	''' Creamos un XML donde iran los datos de las partidas a actualizar. Este XML el stored lo convierte en tabla mediante los tag del XML para facilitar la operacion
	''' </summary>
	''' <remarks></remarks>
	Private Sub ActualizarDatosPartidasPresupuestarias()
		Dim oPRES5 As FSNServer.Partida
		oPRES5 = FSNServer.Get_Object(GetType(FSNServer.Partida))

		Dim viewPartidas As New DataView(dsDatosPartidas.Tables("CONTROL"))
		viewPartidas.RowFilter = "OK=1"
		Dim dtPartidas As DataTable = viewPartidas.ToTable(True, "PRES0", "PRES1", "PRES2", "PRES3", "PRES4", "FC", "ANYO")

		If Not dtPartidas.Rows.Count = 0 Then
			Dim xmlInfoPartida, xmlInfoPartidaPlurianual As XElement
			xmlInfoPartida = New XElement("InfoDatosPartidas", {dtPartidas.Select("PRES0 IS NOT NULL AND ANYO=0").AsEnumerable() _
																.Select(Function(x) _
																			New XElement("Partidas", {
																							New XElement("PRES0", x("PRES0")),
																							New XElement("PRES1", x("PRES1")),
																							IIf(String.IsNullOrEmpty(x("PRES2")), Nothing, New XElement("PRES2", x("PRES2"))),
																							IIf(String.IsNullOrEmpty(x("PRES3")), Nothing, New XElement("PRES3", x("PRES3"))),
																							IIf(String.IsNullOrEmpty(x("PRES4")), Nothing, New XElement("PRES4", x("PRES4"))),
																							New XElement("FC", x("FC")),
																							New XElement("TOTAL", dsDatosPartidas.Tables("CONTROL") _
																								.Select("PRES0='" & x("PRES0") & "'" &
																										IIf(IsDBNull(x("PRES1")), " AND PRES1 IS NULL", " AND PRES1='" & x("PRES1") & "'") &
																										IIf(IsDBNull(x("PRES2")), " AND PRES2 IS NULL", " AND PRES2='" & x("PRES2") & "'") &
																										IIf(IsDBNull(x("PRES3")), " AND PRES3 IS NULL", " AND PRES3='" & x("PRES3") & "'") &
																										IIf(IsDBNull(x("PRES4")), " AND PRES4 IS NULL", " AND PRES4='" & x("PRES4") & "'") &
																										" AND ANYO=" & x("ANYO")).CopyToDataTable.Compute("SUM(TOTAL)", ""))})),
																dsDatosPartidas.Tables("CONTROL").Select("PRES0 IS NOT NULL AND OK IS NOT NULL AND (CAMPO_PADRE<>0 AND LINEA<>0) AND ANYO=0").AsEnumerable() _
																.Select(Function(x) _
																	New XElement("LineasPartidas", {
																			New XElement("CAMPO_PADRE", x("CAMPO_PADRE")),
																			New XElement("LINEA", x("LINEA")),
																			New XElement("PRES0", x("PRES0")),
																			New XElement("PRES1", x("PRES1")),
																			IIf(String.IsNullOrEmpty(x("PRES2")), Nothing, New XElement("PRES2", x("PRES2"))),
																			IIf(String.IsNullOrEmpty(x("PRES3")), Nothing, New XElement("PRES3", x("PRES3"))),
																			IIf(String.IsNullOrEmpty(x("PRES4")), Nothing, New XElement("PRES4", x("PRES4"))),
																			New XElement("PRECIO_UNITARIO", x("PRECIO_UNITARIO")),
																			New XElement("CANT", x("CANT")),
																			New XElement("FC", x("FC"))}))})
			If Not xmlInfoPartida.IsEmpty Then _
				oPRES5.ActualizarDatosInstanciaPartida(oInstancia.ID, xmlInfoPartida.ToString, blnAlta)

			xmlInfoPartidaPlurianual = New XElement("InfoDatosPartidas", {dtPartidas.Select("PRES0 IS NOT NULL AND NOT ANYO=0").AsEnumerable() _
																.Select(Function(x) _
																			New XElement("Partidas", {
																							New XElement("PRES0", x("PRES0")),
																							New XElement("PRES1", x("PRES1")),
																							IIf(String.IsNullOrEmpty(x("PRES2")), Nothing, New XElement("PRES2", x("PRES2"))),
																							IIf(String.IsNullOrEmpty(x("PRES3")), Nothing, New XElement("PRES3", x("PRES3"))),
																							IIf(String.IsNullOrEmpty(x("PRES4")), Nothing, New XElement("PRES4", x("PRES4"))),
																							New XElement("ANYO", x("ANYO")),
																							New XElement("FC", x("FC")),
																							New XElement("TOTAL", dsDatosPartidas.Tables("CONTROL") _
																								.Select("PRES0='" & x("PRES0") & "'" &
																										IIf(IsDBNull(x("PRES1")), " AND PRES1 IS NULL", " AND PRES1='" & x("PRES1") & "'") &
																										IIf(IsDBNull(x("PRES2")), " AND PRES2 IS NULL", " AND PRES2='" & x("PRES2") & "'") &
																										IIf(IsDBNull(x("PRES3")), " AND PRES3 IS NULL", " AND PRES3='" & x("PRES3") & "'") &
																										IIf(IsDBNull(x("PRES4")), " AND PRES4 IS NULL", " AND PRES4='" & x("PRES4") & "'") &
																										" AND ANYO=" & x("ANYO")).CopyToDataTable.Compute("SUM(TOTAL)", ""))})),
																dsDatosPartidas.Tables("CONTROL").Select("PRES0 IS NOT NULL AND OK IS NOT NULL AND (CAMPO_PADRE<>0 AND LINEA<>0) AND NOT ANYO=0").AsEnumerable() _
																.Select(Function(x) _
																	New XElement("LineasPartidas", {
																			New XElement("CAMPO_PADRE", x("CAMPO_PADRE")),
																			New XElement("LINEA", x("LINEA")),
																			New XElement("PRES0", x("PRES0")),
																			New XElement("PRES1", x("PRES1")),
																			IIf(String.IsNullOrEmpty(x("PRES2")), Nothing, New XElement("PRES2", x("PRES2"))),
																			IIf(String.IsNullOrEmpty(x("PRES3")), Nothing, New XElement("PRES3", x("PRES3"))),
																			IIf(String.IsNullOrEmpty(x("PRES4")), Nothing, New XElement("PRES4", x("PRES4"))),
																			New XElement("ANYO", x("ANYO")),
																			New XElement("PRECIO_UNITARIO", x("PRECIO_UNITARIO")),
																			New XElement("CANT", x("CANT")),
																			New XElement("FC", x("FC"))}))})
			If Not xmlInfoPartidaPlurianual.IsEmpty Then _
				oPRES5.ActualizarDatosInstanciaPartida(oInstancia.ID, xmlInfoPartidaPlurianual.ToString, blnAlta, True)
		End If
	End Sub
	''' <summary>
	''' Si hay que echar atras los cambios realizados al imputar la tarea creamos un XML con las partidas modificadas para deshacer cambios
	''' </summary>
	''' <remarks></remarks>
	Private Sub CancelarCambiosPartidasPresupuestarias()
		Dim oPRES5 As FSNServer.Partida
		oPRES5 = FSNServer.Get_Object(GetType(FSNServer.Partida))
		Dim viewPartidas As New DataView(dsDatosPartidas.Tables("CONTROL"))
		Dim dtPartidas As DataTable = viewPartidas.ToTable(True, "PRES0", "PRES1", "PRES2", "PRES3", "PRES4", "OK")
		Dim xmlInfoPartidasCancelar As XElement
		'Cancelamos los cambios realizados, y estos seran los que tengan el campo OK=1, en los de OK=0 no se han realizado cambios
		xmlInfoPartidasCancelar = New XElement("InfoDatosPartidas", dtPartidas.Select("PRES0 IS NOT NULL AND OK=1").Select(Function(x) New XElement("Partida", {New XElement("PRES0", x("PRES0")),
																						New XElement("PRES1", x("PRES1")),
																						IIf(IsDBNull(x("PRES2")), Nothing, New XElement("PRES2", x("PRES2"))),
																						IIf(IsDBNull(x("PRES3")), Nothing, New XElement("PRES3", x("PRES3"))),
																						IIf(IsDBNull(x("PRES4")), Nothing, New XElement("PRES4", x("PRES4")))})))

		oPRES5.CancelarCambiosPartidasPresupuestarias(Instancia.Value, xmlInfoPartidasCancelar.ToString)
	End Sub
	''' Revisado por: blp. Fecha: 14/11/2011
	''' <summary>
	''' Carga la combo de departamentos cuando se selecciona una UON
	''' </summary>
	''' <param name="UON1">Unidad organizativa de nivel 1</param>
	''' <param name="UON2">Unidad organizativa de nivel 2</param>
	''' <param name="UON3">Unidad organizativa de nivel 3</param>
	''' <remarks>Llamada desde: Page_Load, ugtxtUO_wdg_RowSelectionChanged, ActualizarCombos2; Tiempo m�ximo: 0,5 sg</remarks>
	Private Sub CargarDepartamentos(Optional ByVal UON1 As String = Nothing, Optional ByVal UON2 As String = Nothing, Optional ByVal UON3 As String = Nothing)
		Dim oDepartamentos As FSNServer.Departamentos
		Dim iNivel As Integer

		oDepartamentos = FSNServer.Get_Object(GetType(FSNServer.Departamentos))
		If Not UON3 = Nothing Then
			iNivel = 3
		ElseIf Not UON2 = Nothing Then
			iNivel = 2
		ElseIf Not UON1 = Nothing Then
			iNivel = 1
		Else
			iNivel = 0
		End If
		oDepartamentos.LoadData(iNivel, UON1, UON2, UON3, FSNUser.Cod, RestricDEPTraslado:=FSNUser.PMRestringirPersonasDepartamentoUsuario)

		ugtxtDEP_.TextField = "DEN"
		ugtxtDEP_.ValueField = "COD"
		If FSNUser.PMRestringirPersonasDepartamentoUsuario Then
			oDepartamentos.Data.Tables(0).Rows(0).Delete()
		End If
		ugtxtDEP_.DataSource = oDepartamentos.Data
		ugtxtDEP_.DataBind()
		If Not FSNUser.PMRestringirPersonasDepartamentoUsuario Then
			ugtxtDEP_.Items(0).Text = Me.txtCualquiera.Value
		End If
		For Each oItem As Infragistics.Web.UI.ListControls.DropDownItem In ugtxtDEP_.Items
			oItem.CssClass = "igdd_FullstepListItemB"
		Next
		Dim filas_ As Integer = ugtxtDEP_.Items.Count
		Dim i_ As Integer
		For i_ = 0 To filas_ - 1
			If ugtxtDEP_.Items(i_).Value = Me.SelectedDEP.Value Then
				ugtxtDEP_.ActiveItemIndex = i_
				ugtxtDEP_.SelectedItemIndex = i_
				Me.SelectedItem2.Value = i_
				Me.SelectedDEP.Value = ""
				Exit For
			End If
		Next
		If ugtxtDEP_.Items.Count > 0 AndAlso ugtxtDEP_.SelectedItems.Count = 0 Then
			ugtxtDEP_.ActiveItemIndex = 0
			ugtxtDEP_.SelectedItemIndex = 0
		End If

		oDepartamentos = Nothing
	End Sub
	''' <summary>
	''' Al pulsar este bot�n el usuario continua con el traslado de la solicitud
	''' </summary>
	''' <remarks>Llamada desde: al pulsar el bot�n de continuar; Tiempo m�ximo: 0,1 sg.</remarks>
	Private Sub cmdContinuarTraslado_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdContinuarTraslado.ServerClick
		Dim oProveedores As FSNServer.Proveedores
		Dim sClientTexts As String
		Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"

		If txtCod.Text <> "" And txtCod.Text <> Nothing Then  'Va a trasladar a un proveedor:
			'Primero comprueba que el proveedor exista:
			oProveedores = FSNServer.Get_Object(GetType(FSNServer.Proveedores))
			oProveedores.LoadData(Idioma, txtCod.Text, , , FSNUser.Cod, PMRestricProveMat:=FSNUser.PMRestricProveMat)

			If oProveedores.Data.Tables(0).Rows.Count = 0 Then
				sClientTexts = ""
				sClientTexts += "alert(""" & JSText(Me.txtcadenaserror2.Value) & """)"
				sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
				Page.ClientScript.RegisterStartupScript(Me.GetType(), "VisibleLayer", "<script>document.getElementById('divTraslado').style.display='inline'</script>")
				Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)
			Else
				TrasladoUsu()
			End If
		ElseIf Me.CodPer.Value <> "" And Me.CodPer.Value <> Nothing Then  'Va a trasladar a un usuario:
			'No podemos trasladar la solicitud al mismo que nos la traslado, eso se llama "Devoluci�n" y tiene su bot�n correspondiente...
			If Me.Accion.Value = "trasladadatrasladarinstancia" Then
				'Llega con el objeto instancia cargado sin el par�metro a true, por lo que personaEst llegaba a NULL
				Dim oInstanciaTrasladada As FSNServer.Instancia
				oInstanciaTrasladada = FSNServer.Get_Object(GetType(FSNServer.Instancia))
				oInstanciaTrasladada.ID = Me.Instancia.Value
				oInstanciaTrasladada.Load(Idioma, True)

				If Me.CodPer.Value = oInstanciaTrasladada.PersonaEst Then
					sClientTexts = ""
					sClientTexts += "alert(""" & JSText(Me.txtcadenaserror4.Value) & """)"
					sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
					Page.ClientScript.RegisterStartupScript(Me.GetType(), "VisibleLayer", "<script>document.getElementById('divTraslado').style.display='inline'</script>")
					Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)
					Exit Sub
				End If
				oInstanciaTrasladada = Nothing
			End If
			TrasladoUsu()
		Else
			'Se debe rellenar o proveedor o usuario
			sClientTexts = ""
			sClientTexts += "alert(""" & JSText(Me.txtcadenaserror3.Value) & """)"
			sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
			Page.ClientScript.RegisterStartupScript(Me.GetType(), "VisibleLayer", "<script>document.getElementById('divTraslado').style.display='inline'</script>")
			Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)
		End If
	End Sub
	''' <summary>
	''' Al pulsar este bot�n el usuario confirma la devoluci�n de la solicitud.
	''' </summary>
	''' <remarks>Llamada desde: al pulsar el bot�n de aceptar de la confirmaci�n; Tiempo m�ximo: 0,1 sg.</remarks>
	Private Sub cmdAceptarDevolucion_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAceptarDevolucion.ServerClick
		'Carga los datos de la instancia:
		oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
		oInstancia.ID = Instancia.Value
		oInstancia.Load(Idioma, True)
		'Si la instancia ha sido anulada por el peticionario no se puede trasladar:
		If oInstancia.Estado = TipoEstadoSolic.Anulada Then
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AccionAnulada", "<script>window.open(""" & "../workflow/imposibleaccion.aspx?Instancia=" & oInstancia.ID & "&Accion=5&TipoSolicitud=" & tipoSolicitud.Value & """,""fraWSMain"")</script>")
		Else
			oInstancia.Actualizar_En_proceso(1, strToLong(Bloque.Value.ToString))
			'Vuelve a la p�gina de alta de solicitudes o seguimiento
			Page.ClientScript.RegisterStartupScript(Me.GetType(), "TodoOK", "<script>TodoOK(" & IIf(Me.Version.Value = 0, "true", "false") & "," & IIf(Contrato.Value <> "", "true", "false") & ", " &
										   IIf(mi_body.Attributes("class") = "fondoGS", "1", "0") & ",'" & IIf(hidVolver.Value <> "", hidVolver.Value, "") & "');</script>")
			GuardarConWorkflowThread()
		End If
	End Sub
	''' Revisado por:blp. Fecha: 14/11/2011
	''' <summary>
	''' Al pulsar este bot�n el usuario confirma la realizaci�n del traslado de la solicitud.
	''' </summary>
	''' <param name="sender">Control que lanza el evento</param>
	''' <param name="e">Argumentos del evento</param>
	''' <remarks>Llamada desde: al pulsar el bot�n de aceptar de la confirmaci�n; Tiempo m�ximo: 0,1 sg.</remarks>
	Private Sub CmdAceptarTrasladoUsu_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdAceptarTrasladoUsu.ServerClick
		Dim sClientTexts As String
		Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"

		'Carga los datos de la instancia:
		oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
		oInstancia.ID = CType(Instancia.Value, Long)
		oInstancia.Load(Idioma, True)

		'Si la instancia ha sido anulada por el peticionario no se puede trasladar:
		If oInstancia.Estado = TipoEstadoSolic.Anulada Then
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AccionAnulada", "<script>window.open(""" & "../workflow/imposibleaccion.aspx?Instancia=" & oInstancia.ID & "&Accion=3&TipoSolicitud=" & tipoSolicitud.Value & """,""fraWSMain"")</script>")
		Else
			oInstancia.Actualizar_En_proceso(1, strToLong(Bloque.Value.ToString))
			If CodPer.Value <> "" And CodPer.Value <> Nothing Then 'Traslada a un usuario:
				Page.ClientScript.RegisterStartupScript(Me.GetType(), "TodoOK", "<script>TodoOK(" & IIf(Me.Version.Value = 0, "true", "false") & "," & IIf(Contrato.Value <> "", "true", "false") & ", " & IIf(mi_body.Attributes("class") = "fondoGS", "1", "0") & ",'" & IIf(Me.hidVolver.Value <> "", Me.hidVolver.Value, "") & "');</script>")
			Else  'Traslada a un proveedor:
				Dim sContacto As String = String.Empty
				If Not ugtxtContacto_.SelectedItem Is Nothing Then
					sContacto = ugtxtContacto_.SelectedItem.Text
					IdRowContacto.Value = ugtxtContacto_.SelectedItem.Value
				End If
				If sContacto = String.Empty Then
					sClientTexts = ""
					sClientTexts += "alert(""" & JSText(Me.txtcadenaserror3.Value) & """)"
					sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
					Page.ClientScript.RegisterStartupScript(Me.GetType(), "VisibleLayer", "<script>document.getElementById('divtrasladoUsu').style.display='inline';</script>")
					Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)
					Exit Sub
				End If
				'Vuelve a la p�gina de alta de solicitudes o seguimiento
				Page.ClientScript.RegisterStartupScript(Me.GetType(), "TodoOK", "<script>TodoOK(" & IIf(Me.Version.Value = 0, "true", "false") & "," & IIf(Contrato.Value <> "", "true", "false") & ", " & IIf(mi_body.Attributes("class") = "fondoGS", "1", "0") & ",'" & IIf(Me.hidVolver.Value <> "", Me.hidVolver.Value, "") & "');</script>")
			End If
			GuardarConWorkflowThread()
		End If
		oInstancia = Nothing
	End Sub
	''' Revisado por: blp. Fecha: 14/11/2011
	''' <summary>
	''' Carga la pagina (datos, labels...) con los datos de un traslado de la instancia
	''' </summary>
	''' <remarks>Llamada desde=cmdContinuarTraslado; M�x: 1 seg</remarks>
	Private Sub TrasladoUsu()
		Dim oInstancia As FSNServer.Instancia

		ModuloIdioma = TiposDeDatos.ModulosIdiomas.Trasladar
		'Textos de idiomas:
		lblLitCreadoPor.Text = Textos(19) & ":"
		lblLitFecAlta.Text = Textos(18) & ":"
		lblLitTipo.Text = Textos(17) & ":"
		lblIntroFec.Text = Textos(8)
		lblComentTrasladoUsu.Text = Textos(9)
		CmdAceptarTrasladoUsu.Value = Textos(10)
		CmdCancelarTrasladoUsu.Value = Textos(11)
		txtcadenaserror1.Value = Textos(12)
		lblContacto.Text = Textos(13)
		txtcadenaserror3.Value = Textos(15)

		'Carga los datos de la instancia:
		oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
		oInstancia.ID = Instancia.Value
		oInstancia.Load(Idioma, True)
		oInstancia.Solicitud.Load(Idioma)

		OcultarDetallePer()

		'Datos de la instancia:
		FSNPageHeader.TituloCabecera = Textos(0) & " " & oInstancia.Den(Idioma)
		lblTipo.Text = oInstancia.Solicitud.Codigo & " - " & oInstancia.Solicitud.Den(Idioma)
		imgInfTipo.Attributes.Add("onclick", "DetalleTipoSolicitud(" & oInstancia.Solicitud.ID & ")")
		If String.IsNullOrEmpty(Contrato.Value) Then
			lblIDInstanciayEstado.Text = oInstancia.ID
		Else
			lblIDInstanciayEstado.Text = CodContrato.Value
		End If
		If oInstancia.PeticionarioProve = "" Then
			lblCreadoPor.Text = oInstancia.NombrePeticionario
		Else
			lblCreadoPor.Text = oInstancia.PeticionarioProve + " (" + oInstancia.NombrePeticionario + ")"
		End If
		lblFecAlta.Text = FormatDate(oInstancia.FechaAlta, FSNUser.DateFormat)

		If Not oInstancia.CampoImporte Is Nothing Then
			lblLitImporte.Text = oInstancia.CampoImporte & ":"
			lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & oInstancia.Moneda
		Else
			If oInstancia.Solicitud.Tipo = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras Then
				lblLitImporte.Text = Textos(2)
				lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & oInstancia.Moneda
			Else
				lblLitImporte.Visible = False
				lblImporte.Visible = False
			End If
		End If

		If Me.CodPer.Value <> Nothing And Me.CodPer.Value <> "" Then
			lblTraslado.Text = Textos(6)
			txtcadenaserror2.Value = Textos(6)
			'Oculta el combo de los contactos:
			tblDatosTrasladoUsu.Rows(1).Visible = False

			'Carga los datos del usuario:
			Dim oPersona As FSNServer.Persona = FSNServer.Get_Object(GetType(FSNServer.Persona))
			oPersona.LoadData(Me.CodPer.Value)
			If Not oPersona.Codigo Is Nothing Then lblUsuario.Text = oPersona.Nombre & " " & oPersona.Apellidos & " (" & Textos(7) & oPersona.DenDepartamento & ")"

		Else  'Traslada a proveedor:
			lblTraslado.Text = Textos(14)
			txtcadenaserror2.Value = Textos(14)

			Dim oProveedor As FSNServer.Proveedor
			oProveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
			oProveedor.Cod = Me.txtCod.Text
			oProveedor.Load(Idioma, True)
			lblUsuario.Text = oProveedor.Cod & " - " & oProveedor.Den & " (" & oProveedor.NIF & ")"

			'Carga los contactos:
			oProveedor.CargarContactos(False)

			ugtxtContacto_.TextField = "CONTACTO"
			ugtxtContacto_.ValueField = "ID"
			ugtxtContacto_.DataSource = oProveedor.Contactos
			ugtxtContacto_.DataBind()
			For Each oItem As Infragistics.Web.UI.ListControls.DropDownItem In ugtxtContacto_.Items
				oItem.CssClass = "igdd_FullstepListItemB"
			Next
			ugtxtContacto_.CurrentValue = String.Empty

			oProveedor = Nothing
		End If

		FindControl("Form1").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())

		oInstancia = Nothing

		Page.ClientScript.RegisterStartupScript(Me.GetType(), "TrasladosUsu", "<script>document.getElementById('divTraslado').style.display='none';document.getElementById('divtrasladoUsu').style.display='inline';</script>")
	End Sub
	''' Revisado por: blp. Fecha: 14/11/2011
	''' <summary>
	''' M�todo que recarga los combos bajo ciertas condiciones
	''' </summary>
	''' <remarks>Llamada desde page_load. M�ximo 0,1 seg.</remarks>
	Private Sub ActualizarCombos_()
		Me.txtNombre.Text = Me.SelectedNOM.Value
		Me.txtApe.Text = Me.SelectedAPE.Value
		Me.CodPer.Value = Me.SelectedCOD.Value

		Dim cod_uon(3) As String
		cod_uon = Me.SelectedUON.Value.Split(";")
		Dim i As Integer
		For i = 0 To 2
			If cod_uon(i) = "null" Then cod_uon(i) = Nothing
		Next

		Dim filas_ As Integer = ugtxtUO_wdd.Items.Count
		For i = 0 To filas_ - 1
			Dim cod_uon_Wdd As String() = ugtxtUO_wdd.Items(i).Value.Split(";")
			Dim sUON1 As String = IIf(cod_uon_Wdd(0) Is System.DBNull.Value, Nothing, cod_uon_Wdd(0))
			Dim sUON2 As String = IIf(cod_uon_Wdd(1) Is System.DBNull.Value, Nothing, cod_uon_Wdd(1))
			Dim sUON3 As String = IIf(cod_uon_Wdd(2) Is System.DBNull.Value, Nothing, cod_uon_Wdd(2))
			If sUON1 = cod_uon(0) And sUON2 = cod_uon(1) And sUON3 = cod_uon(2) Then
				For Each oItem As Infragistics.Web.UI.ListControls.DropDownItem In ugtxtUO_wdd.Items
					oItem.Selected = False
					'oItem.CssClass = "igdd_FullstepListItemB"
				Next
				If ugtxtUO_wdd.SelectedItemIndex = i Then
					ActualizarCombos2_()
				Else
					ugtxtUO_wdd.ActiveItemIndex = i
					ugtxtUO_wdd.SelectedItemIndex = i
					ugtxtUO_wdd.CurrentValue = ugtxtUO_wdd.SelectedItem.Text
					Me.SelectedItem.Value = i
					If Page.IsPostBack Then
						CargarDepartamentos(cod_uon(0), cod_uon(1), cod_uon(2))
					End If
					Exit For
				End If
			End If
		Next

		Page.ClientScript.RegisterStartupScript(Me.GetType(), "VisibleLayerUO", "<script>document.getElementById('divTraslado').style.display='inline'</script>")

		Dim sScript As String = "<script language=""javascript"">" &
								"window.onload = function(){var t, t=setTimeout(""resetDropDownStyles('" & ugtxtUO_wdd.SelectedItemIndex & "')"", 200);}" &
								"</script>"

		Page.ClientScript.RegisterStartupScript(Me.GetType(), "resetDropDownStyles", sScript)
	End Sub
	''' Revisado por: blp. Fecha: 14/11/2011
	''' <summary>
	''' Metodo que se ejecuta en el evento OnSelectionChanged del control ugtxtDEP_
	''' </summary>
	''' <param name="sender">control que lanza el evento</param>
	''' <param name="e">par�metros del evento</param>
	''' <remarks>llamada desde el evento. M�ximo 0,2 seg</remarks>
	Public Sub ugtxtDEP__SelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles ugtxtDEP_.SelectionChanged
		If blnBorrarPersona = True Then
			Me.txtNombre.Text = ""
			Me.txtApe.Text = ""
		End If

		If ugtxtDEP_.Items.Count = 1 Then
			ugtxtDEP_.ActiveItemIndex = 0
			ugtxtDEP_.SelectedItemIndex = 0
		End If
		If Me.SelectedItem2.Value <> "" Then
			ugtxtDEP_.ActiveItemIndex = CInt(Me.SelectedItem2.Value)
			ugtxtDEP_.SelectedItemIndex = CInt(Me.SelectedItem2.Value)
		End If
		Page.ClientScript.RegisterStartupScript(Me.GetType(), "VisibleLayerUO", "<script>document.getElementById('divTraslado').style.display='inline'</script>")
	End Sub
	''' Revisado por: blp. Fecha: 14/11/2011
	''' <summary>
	''' M�todo que actualiza los combos bajo ciertas condiciones
	''' </summary>
	''' <remarks>Llamada desde ActualizarCombos_</remarks>
	Private Sub ActualizarCombos2_()
		If Me.SelectedItem.Value <> "" Then
			ugtxtUO_wdd.ActiveItemIndex = CInt(Me.SelectedItem.Value)
			ugtxtUO_wdd.SelectedItemIndex = CInt(Me.SelectedItem.Value)
			ugtxtUO_wdd.CurrentValue = ugtxtUO_wdd.SelectedItem.Text
			Me.SelectedItem.Value = ""
		End If
		If Page.IsPostBack Then
			If ugtxtUO_wdd.Items(0).Text <> "" Then
				If ugtxtUO_wdd.SelectedItems.Count > 0 Then
					Dim cod_uon_Wdd As String() = ugtxtUO_wdd.SelectedItem.Value.Split(";")
					Dim sUON1 As String = IIf(cod_uon_Wdd(0) Is System.DBNull.Value, Nothing, cod_uon_Wdd(0))
					Dim sUON2 As String = IIf(cod_uon_Wdd(1) Is System.DBNull.Value, Nothing, cod_uon_Wdd(1))
					Dim sUON3 As String = IIf(cod_uon_Wdd(2) Is System.DBNull.Value, Nothing, cod_uon_Wdd(2))
					CargarDepartamentos(sUON1, sUON2, sUON3)
				End If
				Page.ClientScript.RegisterStartupScript(Me.GetType(), "VisibleLayerUO", "<script>document.getElementById('divTraslado').style.display='inline'</script>")
			End If
		End If
	End Sub
	''' <summary>
	''' Al pulsar el bot�n cancelar de la confirmaci�n del cambio de etapa, si se ha generado un nuevo id de instancia, se elimina
	''' </summary>
	''' <remarks>Llamada desde: Al pulsar el bot�n cancelar. Tiempo m�ximo: 0,2 sg</remarks>
	Private Sub cmdCancelar_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancelar.ServerClick
		Dim oInstancia As FSNServer.Instancia
		oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))

		If Val(NewInstancia.Value) <> 0 And NewInstancia.Value <> "" Then
			Dim lContrato As Long = 0

			If Contrato.Value <> "" Then lContrato = Contrato.Value

			oInstancia.NuevoID = Val(NewInstancia.Value)
			oInstancia.EliminarNuevo(lContrato, FSNUser.Cod)
			NewInstancia.Value = 0
		Else
			oInstancia.ID = Instancia.Value
			oInstancia.Actualizar_En_proceso(0, strToLong(Bloque.Value))
			oInstancia.Actualizar_En_proceso(0)
		End If
		oInstancia = Nothing
		ControlPartidas.Value = ""
		blnEtapaConfirmada = False
		blnControlPartidasMostrado = False
		Session.Remove(NombreXML.Value)
	End Sub
	''' <summary>
	''' Si el usuario no tiene permiso para ver el detalle del peticionario, se ocultan las etiquetas y el enlace al detalle.
	''' </summary>
	''' <remarks>LLamada desde: Page_Load, TrasladoUsu; Tiempo m�ximo: 0,01</remarks>
	Private Sub OcultarDetallePer()
		If Not String.IsNullOrEmpty(Instancia.Value) Then
			Dim dsPermisosRol As DataSet
			dsPermisosRol = FSNUser.VerDetallesPersona(Instancia.Value)
			If dsPermisosRol.Tables.Count > 0 Then
				If dsPermisosRol.Tables(0).Rows(0).Item("VER_DETALLE_PER") = 0 Then
					lblLitCreadoPor.Visible = False
					lblCreadoPor.Visible = False
					imgInfCreadoPor.Visible = False
				Else
					imgInfCreadoPor.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosPeticionario.AnimationClientID & "'," &
												   "event, '" & FSNPanelDatosPeticionario.DynamicPopulateClientID & "', '" & FSNUser.CodPersona & "'); return false;")
				End If
			End If
		End If
	End Sub
	''' Revisado por: blp. Fecha:14/11/2011
	''' <summary>
	''' M�todo que guarda los Threads
	''' </summary>
	''' <remarks>Llamada desde GuardarQA y DarAltaCertificado.M�x: 1 segCmdAceptarTrasladoUsu_ServerClick</remarks>
	Private Sub GuardarQaThread()
		Try
			oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
			oInstancia.ID = CType(Instancia.Value, Long)

			Dim lIDTiempoProc As Long
			oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, FSNUser.Cod)

			''''Aqu� se crea el dataset completo que se enviar� al Servicio
			''''Creando una nueva tabla y a�adiendo los valores
			If ds.Tables.Contains("SOLICITUD") Then ds.Tables.Remove("SOLICITUD")
			ds.Tables.Add("SOLICITUD")
			With ds.Tables("SOLICITUD").Columns
				.Add("TIPO_PROCESAMIENTO_XML")
				.Add("TIPO_DE_SOLICITUD")
				.Add("COMPLETO")
				.Add("CODIGOUSUARIO")
				.Add("INSTANCIA")
				.Add("SOLICITUD")
				.Add("USUARIO")
				.Add("USUARIO_EMAIL")
				.Add("USUARIO_IDIOMA")
				.Add("PEDIDO_DIRECTO")
				.Add("FORMULARIO")
				.Add("WORKFLOW")                'En las NOCONFORMIDADES y CERTIFICADOS al no tener workflow se utiliza para saber si es un alta o modificacion
				.Add("IMPORTE", System.Type.GetType("System.Double"))
				.Add("COMENTALTANOCONF")
				.Add("ACCION")
				.Add("IDACCION")
				.Add("IDACCIONFORM")
				.Add("GUARDAR")
				.Add("NOTIFICAR")
				.Add("COMENTARIO")
				.Add("NOCONFORMIDAD")
				.Add("CERTIFICADO")
				.Add("IDTIEMPOPROC")
			End With
			Dim drSolicitud As DataRow
			drSolicitud = ds.Tables("SOLICITUD").NewRow
			With drSolicitud
				.Item("TIPO_PROCESAMIENTO_XML") = CInt(TipoProcesamientoXML.FSNWeb)
				.Item("TIPO_DE_SOLICITUD") = If(iNoConformidad_Certificado = 1, CInt(TiposDeDatos.TipoDeSolicitud.NoConformidad), CInt(TiposDeDatos.TipoDeSolicitud.Certificado))
				.Item("COMPLETO") = 1
				.Item("CODIGOUSUARIO") = FSNUser.Cod
				.Item("INSTANCIA") = Instancia.Value
				.Item("SOLICITUD") = Solicitud.Value
				.Item("USUARIO") = FSNUser.CodPersona
				.Item("USUARIO_EMAIL") = DBNullToStr(FSNUser.Email)
				.Item("USUARIO_IDIOMA") = FSNUser.Idioma.ToString()
				.Item("PEDIDO_DIRECTO") = 0
				.Item("FORMULARIO") = Formulario.Value
				.Item("WORKFLOW") = Workflow.Value
				.Item("IMPORTE") = 0
				.Item("COMENTALTANOCONF") = ComentAltaNoConf.Value
				.Item("ACCION") = Accion.Value
				.Item("IDACCION") = idAccion.Value
				.Item("IDACCIONFORM") = idAccion.Value
				.Item("GUARDAR") = Guardar.Value
				.Item("NOTIFICAR") = Notificar.Value
				.Item("COMENTARIO") = txtComent.InnerText
				.Item("NOCONFORMIDAD") = IIf(String.IsNullOrEmpty(NoConformidad.Value), 0, NoConformidad.Value)
				.Item("CERTIFICADO") = IIf(String.IsNullOrEmpty(Certificado.Value), 0, Certificado.Value)
				.Item("IDTIEMPOPROC") = lIDTiempoProc
			End With
			ds.Tables("SOLICITUD").Rows.Add(drSolicitud)

			'Cargar XML completo
			Dim xmlName As String = FSNUser.Cod & "#" & Instancia.Value & "#0"
			oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=TiempoProcesamiento.InicioDisco)
			If ConfigurationManager.AppSettings("SERVIDOR_TRATAMIENTO_INSTANCIAS") = "0" Then
				'Se hace con un StringWriter porque el m�todo GetXml del dataset no incluye el esquema
				Dim oSW As New System.IO.StringWriter()
				ds.WriteXml(oSW, XmlWriteMode.WriteSchema)

				Dim oSrvTrataInst As New FSNWebServiceXML.TratamientoInstancias
				oSrvTrataInst.TransferirXMLEnEpera(oSW.ToString(), xmlName)
			Else
				ds.WriteXml(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#FSNWEB.xml", XmlWriteMode.WriteSchema)
				If File.Exists(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml") Then _
					File.Delete(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
				FileSystem.Rename(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#FSNWEB.xml",
								  ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
			End If
		Catch ex As Exception
			Dim oFileW As System.IO.StreamWriter
			oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & "\log_" & FSNUser.Cod & ".txt", True)
			oFileW.WriteLine()
			oFileW.WriteLine("Error Thread: " & ex.Message() & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
			oFileW.Close()
		End Try
	End Sub
	''' <summary>
	''' Inserta los datos generales del contrato.
	''' </summary>
	''' <returns>Devuelve el objeto oError para comprobar si ha habido errores.</returns>
	''' <remarks>Llamada desde: Page_Load y cmdAceptarControlPartida_ServerClick; Tiempo m�x: 0,2 sg.</remarks>
	Private Sub Contrato_Prev()
		Dim oContrato As FSNServer.Contrato
		oContrato = FSNServer.Get_Object(GetType(FSNServer.Contrato))

		Dim dt As New DataTable
		Dim dr As DataRow
		dt = ds.Tables("CONTRATO")
		dr = dt.Rows(0)

		oContrato.Nombre = dr.Item("NOMBRE")
		If Not IsDBNull(dr.Item("FEC_INI")) Then oContrato.FechaInicio = dr.Item("FEC_INI")
		If Not IsDBNull(dr.Item("FEC_FIN")) Then oContrato.FechaFin = dr.Item("FEC_FIN")

		oContrato.CodProve = dr.Item("PROVE")
		oContrato.CodMoneda = dr.Item("MON")
		oContrato.IdContacto = DBNullToDbl(dr.Item("CONTACTO"))
		oContrato.IdEmpresa = dr.Item("EMP")

		If Contrato.Value <> "" Then oContrato.ID = Contrato.Value
		oContrato.Proceso = dr.Item("PROCESO")
		oContrato.Grupos = dr.Item("GRUPOS")
		oContrato.Items = dr.Item("ITEMS")
		oContrato.Create_Prev(oSolicitud.ID, oSolicitud.Formulario.Id, oSolicitud.Workflow,
									   FSNUser.CodPersona, lInstancia)

		Contrato.Value = oContrato.ID
		CodContrato.Value = oContrato.Codigo
		Instancia.Value = lInstancia

		oContrato = Nothing
	End Sub
	Private Function ValidacionesMapper(ByVal bEnviar As Boolean) As Boolean
		ValidacionesMapper = True

		'Mapper
		If Not String.IsNullOrEmpty(Request("PantallaMaper")) AndAlso Request("PantallaMaper") <> "undefined" AndAlso CBool(Request("PantallaMaper")) AndAlso bEnviar AndAlso CBool(Request("GEN_MostrarAvisoMapper")) Then
			Dim MapperStr As String
			Dim iNumError As Integer
			Dim dsMapper As New DataSet
			Dim TextosGS As FSNServer.TextosGS
			Dim DeDonde As String = Request("DeDonde")
			Dim bSave As Boolean = (DeDonde <> "Guardar")

			dsMapper = GenerarDatasetMapperQA(Request, Request("GEN_Accion"), oInstancia, bSave, Idioma)

			If Not lSolicitud = 0 Then
				dsMapper = oSolicitud.CompletarFormulario(dsMapper, True, Usuario.CodPersona, True)
			Else
				dsMapper = oInstancia.CompletarFormulario(dsMapper, Usuario.CodPersona, True, True, Idioma)
			End If

			Dim strError As String = String.Empty
			Dim ValidacionesIntegracion As FSNServer.PM_ValidacionesIntegracion = FSNServer.Get_Object(GetType(FSNServer.PM_ValidacionesIntegracion))
			Dim iTipoValidacionIntegracion As TipoValidacionIntegracion

			iTipoValidacionIntegracion = ValidacionesIntegracion.ControlMapperQA(dsMapper, Idioma, iNumError, strError)
			If Not iTipoValidacionIntegracion = TipoValidacionIntegracion.SinMensaje Then
				TextosGS = FSNServer.Get_Object(GetType(FSNServer.TextosGS))
				iNumError = CInt(iNumError)
				If iNumError = -100 Then
					MapperStr = strError
				Else
					MapperStr = TextosGS.MensajeError(MapperModuloMensaje.PedidoDirecto,
													iNumError, Idioma, strError)
				End If

				oInstancia.Actualizar_En_proceso(0)

				Dim sJavaScript As String
				If iTipoValidacionIntegracion = TipoValidacionIntegracion.Mensaje_Bloqueo Then
					sJavaScript = "alert('" & JSText(MapperStr) & "');if (window.parent.fraWSMain != null) window.parent.fraWSMain.HabilitarBotones();"
					Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "MapperAlert", sJavaScript, True)
				Else 'Aviso
					sJavaScript = "if (confirm('" & JSText(MapperStr) & "')) ConfirmarAvisoValidacionMapper();"
					sJavaScript &= "if (window.parent.fraWSMain != null) window.parent.fraWSMain.HabilitarBotones();"
					Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "MapperConfirm", sJavaScript, True)
				End If

				ValidacionesMapper = False
			End If
		End If
	End Function
	Private Sub CompletarDataSetCamposInvisiblesLectura(ByRef ds As DataSet, ByVal lSolicitud As Long)
		If Not lSolicitud = 0 Then
			ds = oSolicitud.CompletarFormulario(ds)
		Else
			ds = oInstancia.CompletarFormulario(ds, FSNUser.CodPersona, , , , True)
		End If
	End Sub
	Private Sub CompletarDataSetCamposCalculados(ByRef ds As DataSet, ByVal sIdi As String, ByRef ListaCalc As String, ByVal bWorkflow As Boolean)
		'RECALCULAMOS LOS CAMPOS CALCULADOS POR SI LAS FLY
		Dim oDS As DataSet
		Dim oDSDesglose As DataSet
		Dim bCalcular As Boolean
		Dim oDSRow As DataRow
		Dim oRow As DataRow
		Dim dt As DataTable
		dt = ds.Tables("TEMP")

		Dim oDSRowDesglose As DataRow
		Dim oDSRowDesgloseOculto As DataRow
		Dim sPre As String
		Dim sPreCalc As String
		Dim sRequest As String
		Dim iTipoGS As TiposDeDatos.TipoCampoGS
		Dim iTipo As TiposDeDatos.TipoGeneral
		Dim iTipoGSDesglose As TiposDeDatos.TipoCampoGS
		Dim iTipoDesglose As TiposDeDatos.TipoGeneral
		Dim vValor As Object
		Dim oCampo As FSNServer.Campo
		Dim k As Integer
		Dim iLinea As Integer

		If Version.Value = 0 Then
			oSolicitud.Load(Me.Idioma)
			oDS = oSolicitud.Formulario.LoadCamposCalculados(Request("CALC_Solicitud"))
		Else
			oInstancia.ID = Request("CALC_Instancia")
			oInstancia.Version = Request("CALC_NumVersion")
			oDS = oInstancia.LoadCamposCalculados(FSNUser.CodPersona, , bWorkflow)
		End If

		Dim findDesglose(2) As Object
		Dim find(0) As Object
		Dim oRowDesglose As DataRow
		Dim keysDesglose(2) As DataColumn
		Dim oDTDesglose As DataTable
		oDTDesglose = ds.Tables("TEMPDESGLOSE")

		Dim sVariables() As String
		Dim sVariablesDesglose() As String
		Dim dValues() As Double
		Dim dValuesDesglose() As Double
		Dim dValuesTotalDesglose() As Double
		Dim i As Integer
		Dim iDesglose As Integer

		ReDim sVariables(oDS.Tables(0).Rows.Count - 1)
		ReDim dValues(oDS.Tables(0).Rows.Count - 1)

		Dim Eliminados As Integer
		Eliminados = 0
		Dim iEq As New USPExpress.USPExpression

		i = 0
		Dim oRowWorkflow As DataRow = Nothing
		Dim bEstaInvisibleElDesglose As Boolean
		Dim oDSDesglosePadreVisible As DataSet

		For Each oDSRow In oDS.Tables(0).Rows
			bEstaInvisibleElDesglose = False

			sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString())
			sVariables(i) = DBNullToSomething(oDSRow.Item("ID_CALCULO"))

			iTipoGS = DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS"))
			iTipo = oDSRow.Item("SUBTIPO")
			If Version.Value = 0 Then
				sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID").ToString()
			Else
				sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO").ToString()
			End If
			If oDSRow.Item("VISIBLE") = 1 Then
				vValor = Numero(VacioToNothing(Request(sRequest)), ".", ",")
			Else
				vValor = DBNullToSomething(oDSRow.Item("VALOR_NUM"))
			End If

			If oDSRow.Item("TIPO") = TipoCampoPredefinido.Calculado Then
				If Not IsDBNull(oDSRow.Item("ORIGEN_CALC_DESGLOSE")) Then
					oCampo = FSNServer.Get_Object(GetType(FSNServer.Campo))
					If Version.Value = 0 Then
						oCampo.Id = oDSRow.Item("ORIGEN_CALC_DESGLOSE")
						oDSDesglose = oCampo.LoadCalculados(oSolicitud.ID, FSNUser.CodPersona)
						oDSDesglosePadreVisible = oSolicitud.Formulario.LoadDesglosePadreVisible(oSolicitud.ID, oCampo.Id)
					Else
						oCampo.Id = oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE")
						oDSDesglose = oCampo.LoadInstCalculados(lInstancia, FSNUser.CodPersona)
						oDSDesglosePadreVisible = oInstancia.LoadDesglosePadreVisible(oCampo.Id)
					End If

					ReDim sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
					ReDim dValuesDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
					ReDim dValuesTotalDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
					bCalcular = False

					Dim dTotalDesglose As Double
					dTotalDesglose = 0

					Dim iNumRows As Integer
					iNumRows = 0

					Dim bPopUp As Boolean
					Dim bDesgloseEmergente As Boolean

					'Compruebo si el campo desglose es visible                    
					bEstaInvisibleElDesglose = True
					If oDSDesglosePadreVisible.Tables(0).Rows.Count > 0 Then
						For Each oDSRowDesglosePadreVisible In oDSDesglosePadreVisible.Tables(0).Rows
							If oDSRowDesglosePadreVisible.Item("VISIBLE") = 1 Then
								bEstaInvisibleElDesglose = False
								Exit For
							End If
						Next
					End If

					'Miro como si fuera visible
					'si es visible y tiene lineas me dara el numero de lineas
					If bEstaInvisibleElDesglose = False Then
						sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString())
						iNumRows = Request("CALC_" + sPre + "_tblDesglose") 'Filas de desglose que salen en pesta�a
						bPopUp = False
						If iNumRows = Nothing Then
							'El desglose es de tipo popup
							If Version.Value = 0 Then
								iNumRows = Request("CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "_tblDesglose")
								bDesgloseEmergente = False
								If iNumRows = Nothing Then
									bDesgloseEmergente = True
									iNumRows = Request("CALC_" + sPre + "_fsentry" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "__numTotRows")
								End If
							Else
								'Desglose no Emergente "CALC_uwtGrupos__ctl0xx_5491_5491_133510_tblDesglose"
								iNumRows = Request("CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_tblDesglose")
								bDesgloseEmergente = False
								If iNumRows = Nothing Then
									bDesgloseEmergente = True
									iNumRows = Request("CALC_" + sPre + "_fsentry" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "__numTotRows")
								End If
							End If
							bPopUp = True
						End If
					Else
						'si el numero de lineas es 0 miro para ver si es pq el desglose esta oculto
						'If iNumRows = 0 Then
						If oDSDesglose.Tables(1).Rows.Count > 0 Then
							For Each oDSRowDesgloseOculto In oDSDesglose.Tables(1).Rows
								If oDSRowDesgloseOculto.Item("Linea") > iNumRows Then
									iNumRows = oDSRowDesgloseOculto.Item("Linea")
									bEstaInvisibleElDesglose = True
								End If
							Next
						End If
						'End If
					End If

					Dim EliminadoporNothing As Boolean
					EliminadoporNothing = False
					Dim MirarEliminadoporNothing As Boolean

					Dim distinctCounts As IEnumerable(Of Int32)
					Dim iMaxIndex As Integer
					If Version.Value = 0 Then
						iMaxIndex = iNumRows
					Else
						iMaxIndex = oDSDesglose.Tables(1).Select("", "LINEA DESC")(0).Item("LINEA")
						distinctCounts = From row In oDSDesglose.Tables(1)
										 Select row.Field(Of Int32)("LINEA")
										 Distinct
					End If
					For k = 1 To iMaxIndex
						If Version.Value = 0 OrElse distinctCounts.Contains(k) Then
							EliminadoporNothing = False
							MirarEliminadoporNothing = True

							iDesglose = 0
							For Each oDSRowDesglose In oDSDesglose.Tables(0).Rows
								sVariablesDesglose(iDesglose) = oDSRowDesglose.Item("ID_CALCULO")
								bCalcular = True

								iTipoGSDesglose = DBNullToSomething(oDSRowDesglose.Item("TIPO_CAMPO_GS"))
								iTipoDesglose = oDSRow.Item("SUBTIPO")

								If oDSRowDesglose.Item("VISIBLE") = 1 And Not bEstaInvisibleElDesglose Then 'And oDSRow.Item("VISIBLE") = 1 Then
									'Si es visible y el desglose esta visible
									If Version.Value = 0 Then
										If bPopUp Then
											If bDesgloseEmergente Then
												sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ORIGEN_CALC_DESGLOSE") & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID")
											Else
												'CALC_uwtGrupos__ctl0xx_623_623_10549_fsdsentry_1_10551
												sRequest = "CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID").ToString
											End If
										Else
											'CALC_uwtGrupos__ctl0x_146_fsdsentry_1_2283
											sRequest = "CALC_" + sPre + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID").ToString()
										End If
									Else
										If bPopUp Then
											If bDesgloseEmergente Then
												sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE") & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID_CAMPO")
											Else
												'CALC_uwtGrupos__ctl0xx_5491_5491_133510_fsdsentry_1_133486
												sRequest = "CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID_CAMPO").ToString
											End If
										Else
											'CALC_uwtGrupos__ctl0x_146_fsdsentry_1_2283
											sRequest = "CALC_" + sPre + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID_CAMPO").ToString()
										End If
									End If
									vValor = Numero(VacioToNothing(Request(sRequest)), ".", ",")

									If MirarEliminadoporNothing Then
										MirarEliminadoporNothing = False
										If Request(sRequest) Is Nothing Then
											EliminadoporNothing = True
										End If
									End If
								Else
									'si esta oculto bien pq no es visible o pq su campo padre el desglose esta oculto
									keysDesglose(0) = oDSDesglose.Tables(1).Columns("LINEA")
									keysDesglose(1) = oDSDesglose.Tables(1).Columns("CAMPO_PADRE")
									keysDesglose(2) = oDSDesglose.Tables(1).Columns("CAMPO_HIJO")

									oDSDesglose.Tables(1).PrimaryKey = keysDesglose
									iLinea = k
									findDesglose(0) = (k).ToString()
									If Version.Value = 0 Then
										findDesglose(1) = oDSRow.Item("ORIGEN_CALC_DESGLOSE")
										findDesglose(2) = oDSRowDesglose.Item("ID").ToString()
									Else
										findDesglose(1) = oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE")
										findDesglose(2) = oDSRowDesglose.Item("ID_CAMPO").ToString()
									End If
									oRowDesglose = oDSDesglose.Tables(1).Rows.Find(findDesglose)
									Dim iTmpLinea As Integer = iLinea
									While oRowDesglose Is Nothing And iTmpLinea > 0
										iTmpLinea -= 1
										findDesglose(0) = iTmpLinea

										oRowDesglose = oDSDesglose.Tables(1).Rows.Find(findDesglose)
									End While
									If Not oRowDesglose Is Nothing Then
										vValor = DBNullToSomething(oRowDesglose.Item("VALOR_NUM"))
									End If
								End If

								If oDSRowDesglose.Item("TIPO") = TipoCampoPredefinido.Calculado Then
									dValuesDesglose(iDesglose) = 0
								Else
									dValuesDesglose(iDesglose) = Numero(vValor)
								End If
								dValuesTotalDesglose(iDesglose) = CDec(dValuesTotalDesglose(iDesglose) + dValuesDesglose(iDesglose))
								iDesglose += 1
							Next
							iDesglose = 0
							For Each oDSRowDesglose In oDSDesglose.Tables(0).Rows
								If oDSRowDesglose.Item("TIPO") = TipoCampoPredefinido.Calculado Then
									Try
										If Not EliminadoporNothing Then
											iEq.Parse(oDSRowDesglose.Item("FORMULA"), sVariablesDesglose)
											dValuesDesglose(iDesglose) = iEq.Evaluate(dValuesDesglose)
											iLinea = k
											findDesglose(0) = k.ToString()
											If Version.Value = 0 Then
												findDesglose(1) = oDSRow.Item("ORIGEN_CALC_DESGLOSE")
												findDesglose(2) = oDSRowDesglose.Item("ID").ToString()
											Else
												findDesglose(1) = oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE")
												findDesglose(2) = oDSRowDesglose.Item("ID_CAMPO").ToString()
											End If
											oRowDesglose = Nothing
											oRowDesglose = oDTDesglose.Rows.Find(findDesglose)

											If Not oRowDesglose Is Nothing Then
												oRowDesglose.Item("VALOR_NUM") = dValuesDesglose(iDesglose)
												dValuesTotalDesglose(iDesglose) = CDec(dValuesTotalDesglose(iDesglose) + dValuesDesglose(iDesglose))
												''''
												If Version.Value = 0 Then
													If bPopUp Then
														If bDesgloseEmergente Then
															sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ORIGEN_CALC_DESGLOSE") & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID")
														Else
															'CALC_uwtGrupos__ctl0xx_623_623_10549_fsdsentry_1_10551
															sRequest = "CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID").ToString & "__t_t"
														End If
													Else
														'CALC_uwtGrupos__ctl0x_146_fsdsentry_1_2283
														sRequest = "CALC_" + sPre + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID").ToString() & "__t_t"
													End If
												Else
													If bPopUp Then
														If bDesgloseEmergente Then
															sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE") & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID_CAMPO")
														Else
															'CALC_uwtGrupos__ctl0xx_5491_5491_133510_fsdsentry_1_133486
															sRequest = "CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID_CAMPO").ToString & "__t_t"
														End If
													Else
														'CALC_uwtGrupos__ctl0x_146_fsdsentry_1_2283
														sRequest = "CALC_" + sPre + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID_CAMPO").ToString() & "__t_t"
													End If
												End If
												ListaCalc = ListaCalc & Replace(sRequest, "CALC_", "") & "@" & CStr(oRowDesglose.Item("VALOR_NUM")) & "#"
											Else
												'En la tabla temporal
												If oDSRow.Item("VISIBLE") = 0 Then
													dValuesTotalDesglose(iDesglose) = CDec(dValuesTotalDesglose(iDesglose) + dValuesDesglose(iDesglose))
												End If

											End If
										End If
									Catch ex As USPExpress.ParseException
									Catch e As Exception
									End Try
								End If
								iDesglose += 1
							Next

							ReDim Preserve sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count)
							ReDim Preserve dValuesDesglose(oDSDesglose.Tables(0).Rows.Count)
							Try
								sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count) = oDSRow.Item("FORMULA") + " * 1"
								iEq.Parse(oDSRow.Item("FORMULA"), sVariablesDesglose)

								dValuesDesglose(oDSDesglose.Tables(0).Rows.Count) = iEq.Evaluate(dValuesDesglose)
								dTotalDesglose = CDec(dTotalDesglose + dValuesDesglose(oDSDesglose.Tables(0).Rows.Count))
							Catch ex As USPExpress.ParseException
								'Se captura y no hace nada para que siga con el calculo
							Catch e As Exception
								'Se captura y no hace nada para que siga con el calculo
							End Try
						End If
					Next

					If Not IsDBNull(oDSRow.Item("FORMULA")) Then
						Try
							If bCalcular = True Then
								If oDSRow.Item("ES_SUBCAMPO") = 0 Then
									dTotalDesglose = iEq.Evaluate(dValuesTotalDesglose)
								End If
								dValues(i) = dTotalDesglose
							End If
							If Version.Value = 0 Then
								find(0) = oDSRow.Item("ID")
							Else
								find(0) = oDSRow.Item("ID_CAMPO")
							End If
							oRow = dt.Rows.Find(find)
							If Not oRow Is Nothing Then
								oRow.Item("VALOR_NUM") = dValues(i)

								''''
								sPreCalc = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString())
								If Version.Value = 0 Then
									sRequest = "CALC_" + sPreCalc & "_fsentry" & oDSRow.Item("ID").ToString() & "__t_t"
								Else
									sRequest = "CALC_" + sPreCalc & "_fsentry" & oDSRow.Item("ID_CAMPO").ToString() & "__t_t"
								End If
								ListaCalc = ListaCalc & Replace(sRequest, "CALC_", "") & "@" & CStr(oRow.Item("VALOR_NUM")) & "#"
							End If
						Catch ex As USPExpress.ParseException
							'Se captura y no hace nada para que siga con el calculo
						Catch e As Exception
							'Se captura y no hace nada para que siga con el calculo
						End Try
					End If
				Else
					dValues(i) = 0
				End If
			Else
				dValues(i) = Numero(vValor)
				If (oDSRow.Item("ES_IMPORTE_TOTAL") = 1 AndAlso oDSRow.Item("ES_SUBCAMPO") = 0) Then
					dTotal = dValues(i)
				End If
			End If
			i += 1
		Next
		i = 0

		For Each oDSRow In oDS.Tables(0).Rows
			sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString())
			If oDSRow.Item("TIPO") = TipoCampoPredefinido.Calculado Then
				If Version.Value = 0 Then
					find(0) = oDSRow.Item("ID")
				Else
					find(0) = oDSRow.Item("ID_CAMPO")
				End If
				oRow = dt.Rows.Find(find)

				Try
					iEq.Parse(oDSRow.Item("FORMULA"), sVariables)
					dValues(i) = iEq.Evaluate(dValues)
					If Not oRow Is Nothing Then
						oRow.Item("VALOR_NUM") = dValues(i)
						''''
						sPreCalc = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString())
						If Version.Value = 0 Then
							sRequest = "CALC_" + sPreCalc & "_fsentry" & oDSRow.Item("ID").ToString() & "__t_t"
						Else
							sRequest = "CALC_" + sPreCalc & "_fsentry" & oDSRow.Item("ID_CAMPO").ToString() & "__t_t"
						End If
						ListaCalc = ListaCalc & Replace(sRequest, "CALC_", "") & "@" & CStr(oRow.Item("VALOR_NUM")) & "#"
					End If
				Catch ex As USPExpress.ParseException
					'Se captura y no hace nada para que siga con el calculo
				Catch e As Exception
					'Se captura y no hace nada para que siga con el calculo
				End Try
				If (DBNullToSomething(oDSRow.Item("ES_IMPORTE_TOTAL")) = 1 AndAlso DBNullToSomething(oDSRow.Item("ES_SUBCAMPO")) = 0) Then
					oRowWorkflow = oRow
				End If
			End If
			i += 1
		Next
		If Not oRowWorkflow Is Nothing Then
			dTotal = DBNullToSomething(oRowWorkflow.Item("VALOR_NUM"))
		End If
	End Sub
	Private Sub DarAltaNoConformidad(ByVal bEmitir As Boolean, ByVal EsNoconfEscalacion As Boolean, ByVal idEscalacionProve As Integer)
		oNoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))

		''Variable que sirven para crear la instancia de la NoConformidad antes de pasar por el WebService
		oNoConformidad.IDSolicitud = lSolicitud

		oNoConformidad.Proveedor = Request("GEN_NoConfProve")
		oNoConformidad.ProveDen = Request("GEN_NoConfProveDEN")
		If Request("GEN_NoConfProveERP") <> String.Empty Then oNoConformidad.ProveedorERP = Request("GEN_NoConfProveERP")
		If Request("GEN_NoConfProveERPDen") <> String.Empty Then oNoConformidad.ProveedorERPDen = Request("GEN_NoConfProveERPDen")

		oNoConformidad.NotificadosProveedor = Request("GEN_NoConfContactosProveedor")
		oNoConformidad.NotificadosInternos = Request("GEN_NoConfContactosInternos")
		oNoConformidad.Peticionario = Usuario.CodPersona

		If Request("GEN_NoConfUnidadQa") <> Nothing Then oNoConformidad.UnidadQA = Request("GEN_NoConfUnidadQa")
		If Request("GEN_NoConfComentAlta") <> Nothing Then ComentAltaNoConf.Value = Request("GEN_NoConfComentAlta")
		If Request("GEN_NoConfRevisor") <> "null" Then oNoConformidad.Revisor = Request("GEN_NoConfRevisor")

		If Request("GEN_DespubAnyo") <> "" AndAlso Request("GEN_DespubMes") <> "" AndAlso Request("GEN_DespubDia") <> "" Then
			Dim D As Date
			D = New Date(Request("GEN_DespubAnyo"), Request("GEN_DespubMes"), Request("GEN_DespubDia"))
			oNoConformidad.FechaLimResol = D
		End If
		If Request("GEN_FecApliPlanAnyo") <> "" AndAlso Request("GEN_FecApliPlanMes") <> "" AndAlso Request("GEN_FecApliPlanDia") <> "" Then
			Dim D As Date
			D = New Date(Request("GEN_FecApliPlanAnyo"), Request("GEN_FecApliPlanMes"), Request("GEN_FecApliPlanDia"))
			oNoConformidad.FechaAplicPlan = D
		End If
		oNoConformidad.FechaAlta = Now

		If oSolicitud.Workflow > 0 Then
			oNoConformidad.Create_Prev(bEmitir, oSolicitud.Workflow)
		Else
			oNoConformidad.Create_Prev(bEmitir)
		End If

		Instancia.Value = oNoConformidad.Instancia
		NoConformidad.Value = oNoConformidad.ID

		If EsNoconfEscalacion Then
			'Movemos las NC que generan la escalacion a historico y vinculamos la escalacion pdte a esta NC poniendo la escalacion en curso
			oNoConformidad.Aprobacion_Escalacion(oNoConformidad.ID, idEscalacionProve, oNoConformidad.Proveedor, idEscalacionProve = 0, bEmitir)
			If bEmitir Then
				'Es un alta de no conformidad para escalacion
				'CERRAMOS de forma automatica como NO EFICAZ las posibles no conformidades de escalacion existentes en niveles inferiores.
				Dim ModuloIdiomaAux As TiposDeDatos.ModulosIdiomas = ModuloIdioma
				ModuloIdioma = TiposDeDatos.ModulosIdiomas.PanelEscalacionProveedores
				Dim sCommnetCierre As String = Textos(33)
				Dim oUnidadesQA As FSNServer.UnidadesNeg = FSNServer.Get_Object(GetType(FSNServer.UnidadesNeg))
				sCommnetCierre = Replace(sCommnetCierre, "###", Textos(4) & CInt(Request("GEN_NivelEscalacion")) & " (" & oUnidadesQA.LoadUnQa(FSNUser.Idioma.ToString, oNoConformidad.UnidadQA).Rows(0)("UNIDAD") & ")")
				Dim oNoConformidadCierreEscalacion As FSNServer.NoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
				For Each rowNCE As DataRow In oNoConformidadCierreEscalacion.Obtener_NoConformidades_Escalacion_NivelesInferiores(oNoConformidad.ID).Rows
					With oNoConformidadCierreEscalacion
						.ID = rowNCE("ID")
						.Instancia = rowNCE("INSTANCIA")
						.CargarDatosDesdeInstancia()
						.CierreNoConformidad(False, FSNUser.Email, sCommnetCierre, False, FSNUser.CodPersona, .NotificadosProveedor, .NotificadosInternos)
					End With
				Next
				oNoConformidadCierreEscalacion = Nothing
				ModuloIdioma = ModuloIdiomaAux

				'Notificamos el alta de la NC de escalacion (NCE) a los usuarios con permiso para recibir notificaciones en la UNQA de la NCE y para el nivel de escalacion
				Dim permisoAviso As Integer
				Select Case CInt(Request("GEN_NivelEscalacion"))
					Case 1
						permisoAviso = PermisoRecibirAviso_Escalacion_UNQA.Nivel_1
					Case 2
						permisoAviso = PermisoRecibirAviso_Escalacion_UNQA.Nivel_2
					Case 3
						permisoAviso = PermisoRecibirAviso_Escalacion_UNQA.Nivel_3
					Case Else
						permisoAviso = PermisoRecibirAviso_Escalacion_UNQA.Nivel_4
				End Select
				oNoConformidad.Notificacion_Escalacion_Proveedor(oNoConformidad.Instancia, idEscalacionProve, oNoConformidad.UnidadQA, permisoAviso)
			End If
		End If
	End Sub
	''' <summary>
	''' Dar de alta una lista de Certificados
	''' </summary>
	''' <param name="bEmitir">si se emite o no</param>
	''' <remarks>Llamada desde: GuardarQA ; Tiempo m�ximo: 0,2</remarks>
	Private Sub DarAltaCertificado(ByVal bEmitir As Boolean)
		If Not ds.Tables("TEMPCERTIFICADO") Is Nothing Then
			For i = 0 To ds.Tables("TEMPCERTIFICADO").Rows.Count - 1
				oCertificado = FSNServer.Get_Object(GetType(FSNServer.Certificado))

				oCertificado.IDSolicitud = lSolicitud
				oCertificado.PeticionarioCod = FSNUser.CodPersona
				'Por cada proveedor que hay en el dataset de certificado crear una instancia y un certificado
				oCertificado.Proveedor = ds.Tables("TEMPCERTIFICADO").Rows(i).Item("PROVE")
				oCertificado.Contacto = ds.Tables("TEMPCERTIFICADO").Rows(i).Item("CONTACTO")
				If ds.Tables("TEMPCERTIFICADO").Rows(i).Item("FEC_DESPUB") Is DBNull.Value Then
					oCertificado.FechaDesPub = Nothing
				Else
					oCertificado.FechaDesPub = ds.Tables("TEMPCERTIFICADO").Rows(i).Item("FEC_DESPUB")
				End If
				oCertificado.FechaLimCumpl = ds.Tables("TEMPCERTIFICADO").Rows(i).Item("FEC_LIM_CUMPLIM")

				oCertificado.Create_Prev(bEmitir)

				Instancia.Value = oCertificado.Instancia
				Certificado.Value = oCertificado.ID

				If Accion.Value = "enviarcertificados" Then
					If ListaProvesEnviados = "" Then
						ListaProvesEnviados = CStr(oCertificado.Instancia)
					Else
						ListaProvesEnviados = ListaProvesEnviados & "@" & CStr(oCertificado.Instancia)
					End If
				End If

				'Llamamos al guardado XML y al web service mediante Thread para que no de Request time out
				GuardarQaThread()
			Next i
		End If
	End Sub
	''' <summary>
	''' Recoge y modifica los datos de la NoConformidad
	''' </summary>
	''' <param name="bEmitir">1-emitir 0-guardar</param>
	''' <remarks>Llamadas desde: GuardarQA; Teimpo maximo: 0,1</remarks>
	Private Sub ModificarNoConformidad(ByVal bEmitir As Boolean, ByVal EsNoconfEscalacion As Boolean, ByVal idEscalacionProve As Integer)
		oNoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
		oNoConformidad.Instancia = Me.Instancia.Value

		oNoConformidad.ID = CType(Request("GEN_NoConformidad"), Long)
		Me.NoConformidad.Value = oNoConformidad.ID

		oNoConformidad.Proveedor = Request("GEN_NoConfProve")
		If Request("GEN_NoConfProveERP") <> String.Empty Then oNoConformidad.ProveedorERP = Request("GEN_NoConfProveERP")
		If Request("GEN_NoConfProveERPDen") <> String.Empty Then oNoConformidad.ProveedorERPDen = Request("GEN_NoConfProveERPDen")
		oNoConformidad.NotificadosProveedor = Request("GEN_NoConfContactosProveedor")
		oNoConformidad.NotificadosInternos = Request("GEN_NoConfContactosInternos")

		If Request("GEN_NoConfUnidadQa") <> Nothing Then
			oNoConformidad.UnidadQA = Request("GEN_NoConfUnidadQa")
		End If
		If Request("GEN_NoConfRevisor") <> "null" Then
			oNoConformidad.Revisor = Request("GEN_NoConfRevisor")
		End If
		If Request("GEN_NoConfComentAlta") <> Nothing Then
			ComentAltaNoConf.Value = Request("GEN_NoConfComentAlta")
		End If
		If Request("GEN_DespubAnyo") <> "" AndAlso Request("GEN_DespubMes") <> "" AndAlso Request("GEN_DespubDia") <> "" Then
			Dim D As Date
			D = New Date(Request("GEN_DespubAnyo"), Request("GEN_DespubMes"), Request("GEN_DespubDia"))
			oNoConformidad.FechaLimResol = D
		End If
		If Request("GEN_FecApliPlanAnyo") <> "" AndAlso Request("GEN_FecApliPlanMes") <> "" AndAlso Request("GEN_FecApliPlanDia") <> "" Then
			Dim D As Date
			D = New Date(Request("GEN_FecApliPlanAnyo"), Request("GEN_FecApliPlanMes"), Request("GEN_FecApliPlanDia"))
			oNoConformidad.FechaAplicPlan = D
		End If

		oNoConformidad.Save_Prev(bEmitir)
		oNoConformidad.NotificadosProveedor = Replace(oNoConformidad.NotificadosProveedor, "#", ";")
		oNoConformidad.NotificadosInternos = Replace(oNoConformidad.NotificadosInternos, "#", ";")
		If EsNoconfEscalacion AndAlso bEmitir Then
			'Movemos las NC que generan la escalacion a historico y vinculamos la escalacion pdte a esta NC poniendo la escalacion en curso
			oNoConformidad.Aprobacion_Escalacion(oNoConformidad.ID, idEscalacionProve, oNoConformidad.Proveedor, idEscalacionProve = 0, bEmitir)
			'Es un alta de no conformidad para escalacion
			'CERRAMOS de forma automatica como NO EFICAZ las posibles no conformidades de escalacion existentes en niveles inferiores.
			Dim ModuloIdiomaAux As TiposDeDatos.ModulosIdiomas = ModuloIdioma
			ModuloIdioma = TiposDeDatos.ModulosIdiomas.PanelEscalacionProveedores
			Dim sCommnetCierre As String = Textos(33)
			Dim oUnidadesQA As FSNServer.UnidadesNeg = FSNServer.Get_Object(GetType(FSNServer.UnidadesNeg))
			sCommnetCierre = Replace(sCommnetCierre, "###", Textos(4) & CInt(Request("GEN_NivelEscalacion")) & " (" & oUnidadesQA.LoadUnQa(FSNUser.Idioma.ToString, oNoConformidad.UnidadQA).Rows(0)("UNIDAD") & ")")
			Dim oNoConformidadCierreEscalacion As FSNServer.NoConformidad = FSNServer.Get_Object(GetType(FSNServer.NoConformidad))
			For Each rowNCE As DataRow In oNoConformidadCierreEscalacion.Obtener_NoConformidades_Escalacion_NivelesInferiores(oNoConformidad.ID).Rows
				With oNoConformidadCierreEscalacion
					.ID = rowNCE("ID")
					.Instancia = rowNCE("INSTANCIA")
					.CargarDatosDesdeInstancia()
					.CierreNoConformidad(False, FSNUser.Email, sCommnetCierre, False, FSNUser.CodPersona, .NotificadosProveedor, .NotificadosInternos)
				End With
			Next
			oNoConformidadCierreEscalacion = Nothing
			ModuloIdioma = ModuloIdiomaAux


			'Notificamos el alta de la NC de escalacion (NCE) a los usuarios con permiso para recibir notificaciones en la UNQA de la NCE y para el nivel de escalacion
			Dim permisoAviso As Integer
			Select Case CInt(Request("GEN_NivelEscalacion"))
				Case 1
					permisoAviso = PermisoRecibirAviso_Escalacion_UNQA.Nivel_1
				Case 2
					permisoAviso = PermisoRecibirAviso_Escalacion_UNQA.Nivel_2
				Case 3
					permisoAviso = PermisoRecibirAviso_Escalacion_UNQA.Nivel_3
				Case Else
					permisoAviso = PermisoRecibirAviso_Escalacion_UNQA.Nivel_4
			End Select
			oNoConformidad.Notificacion_Escalacion_Proveedor(oNoConformidad.Instancia, idEscalacionProve, oNoConformidad.UnidadQA, permisoAviso)
		End If
	End Sub
	Private Function ComprobarBloqueosSolicitud(ByVal lFormularioID As Long, ByVal lInstancia As Long) As Boolean
		Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.GuardarInstancia

		Dim bMostrarMensaje As Boolean = False
		If idRefSol.Value <> 0 Then
			Dim oInstancias As FSNServer.Instancias
			oInstancias = FSNServer.Get_Object(GetType(FSNServer.Instancias))
			oInstancias.DevolverSolicitudesPadre(Idioma, , idRefSol.Value, , , , , , , lFormularioID, lInstancia)

			If oInstancias.Data.Tables(0).Rows.Count > 0 Then
				'(Disponible - ImporteInstancia)
				If (oInstancias.Data.Tables(0).Rows(0).Item(4) - ViewState("Importe")) < 0 Then
					Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
					Dim sClientTexts As String
					Dim ImporteSolicitud As String
					Dim ImporteAcumulado As String

					sClientTexts = "var arrTextosML = new Array();"
					sClientTexts &= "arrTextosML[0] = '" & JSText(Textos(2)) & "';" '�Imposible realizar la acci�n! Se ha superado el importe acumulado para la solicitud
					sClientTexts &= "arrTextosML[1] = '" & JSText(Textos(3)) & "';" '�Atenci�n! Se ha superado el importe acumulado para la solicitud
					sClientTexts &= "arrTextosML[2] = '" & JSText(Textos(4)) & "';" 'Importe acumulado
					sClientTexts &= "arrTextosML[3] = '" & JSText(Textos(5)) & "';" 'Importe solicitud

					sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
					Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosMICliente", sClientTexts)

					bMostrarMensaje = True
					ImporteAcumulado = FSNLibrary.FormatNumber((oInstancias.Data.Tables(0).Rows(0).Item(3) - oInstancias.Data.Tables(0).Rows(0).Item(4) + ViewState("Importe")), FSNUser.NumberFormat) & " " & DBNullToStr(oInstancias.Data.Tables(0).Rows(0).Item(6))
					ImporteSolicitud = FSNLibrary.FormatNumber(oInstancias.Data.Tables(0).Rows(0).Item(3), FSNUser.NumberFormat) & " " & DBNullToStr(oInstancias.Data.Tables(0).Rows(0).Item(6))
					Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>ControlImporte('" & oInstancias.Data.Tables(0).Rows(0).Item(1) & "', '" & ImporteAcumulado & "','" & ImporteSolicitud & "'," & AvisoBloqueo.Value & ")</script>")
				End If
			End If
			oInstancias = Nothing
		End If
		ComprobarBloqueosSolicitud = bMostrarMensaje
	End Function
	''' <summary>
	''' Muestra la p�gina de confirmaci�n de devoluci�n de una solicitud trasladada o un contrato trasladado.
	''' </summary>
	''' <param name="oInstancia">Objeto instancia</param>
	''' <remarks>Llamada desde: Page_Load y cmdAceptarControlPartida_ServerClick; Tiempo m�x: 0,2 sg;</remarks>
	Private Sub DevolverInstancia(ByRef oInstancia As FSNServer.Instancia)
		If oInstancia.Estado = TipoEstadoSolic.Anulada Then
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "init", "<script>window.open(""" & "../workflow/imposibleaccion.aspx?Instancia=" & lInstancia & "&Accion=5&TipoSolicitud=" & tipoSolicitud.Value & """,""fraWSMain"")</script>")
			Exit Sub
		End If

		Page.ClientScript.RegisterStartupScript(Me.GetType(), "MostrarFSWSServer", "<script>MostrarFSWSServer();</script>")

		ModuloIdioma = TiposDeDatos.ModulosIdiomas.Devolucion

		'Textos de idiomas:
		lblLitCreadoPor.Text = Textos(12) & ":"
		lblLitTipo.Text = Textos(10) & ":"
		lblLitFecAlta.Text = Textos(11) & ":"
		lblOrigen.Text = Textos(5)
		lblComent.Text = Textos(6)
		cmdAceptarDevolucion.Value = Textos(7)
		cmdCancelarDevolucion.Value = Textos(8)
		txtcadenaserror2.Value = Textos(0)
		txtcadenaserror1.Value = Textos(9)

		'Datos de la instancia:
		FSNPageHeader.TituloCabecera = Textos(0) & ": " & oInstancia.Den(Idioma)
		lblTipo.Text = oInstancia.Solicitud.Codigo & " - " & oInstancia.Solicitud.Den(Idioma)
		imgInfTipo.Attributes.Add("onclick", "DetalleTipoSolicitud(" & oInstancia.Solicitud.ID & ")")
		If String.IsNullOrEmpty(Contrato.Value) Then
			lblIDInstanciayEstado.Text = oInstancia.ID
		Else
			lblIDInstanciayEstado.Text = CodContrato.Value
		End If
		If oInstancia.PeticionarioProve = "" Then
			lblCreadoPor.Text = oInstancia.NombrePeticionario
		Else
			lblCreadoPor.Text = oInstancia.PeticionarioProve + " (" + oInstancia.NombrePeticionario + ")"
		End If
		lblFecAlta.Text = FormatDate(oInstancia.FechaAlta, FSNUser.DateFormat)
		lblUsuario.Text = oInstancia.NombrePersonaEst

		If Not oInstancia.CampoImporte Is Nothing Then
			lblLitImporte.Text = oInstancia.CampoImporte & ":"
			lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & oInstancia.Moneda
		Else
			If oInstancia.Solicitud.Tipo = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras Then
				lblLitImporte.Text = Textos(2)
				lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & oInstancia.Moneda
			Else
				lblLitImporte.Visible = False
				lblImporte.Visible = False
			End If
		End If

		GuardarConWorkflowXML()

		Page.ClientScript.RegisterStartupScript(Me.GetType(), "TrasladosDevolucion", "<script>document.getElementById('divDevolverSiguientesEtapas').style.display='none';document.getElementById('divTraslado').style.display='none';document.getElementById('divDevolucion').style.display='inline';</script>")
	End Sub
	''' Revisado por: Jbg. Fecha: 20/10/2011
	''' <summary>
	''' Si se trata tanto del traslado de una solicitud como de un contrato, se muestra su confirmaci�n.
	''' </summary>
	''' <param name="oInstancia">Objeto Instancia</param>
	''' <param name="sAccion">Cadena con la acci�n que se est� realizando</param>
	''' <remarks>Llamada desde: cmdAceptarControlPartica_ServerClick, Page_Load(); Tiempo m�ximo: 0,2 sg</remarks>
	Private Sub TrasladarInstancia(ByRef oInstancia As FSNServer.Instancia, ByVal sAccion As String)
		If ((sAccion = "trasladarinstancia") Or (sAccion = "trasladarcontrato")) Then
			If oInstancia.Estado = TipoEstadoSolic.Anulada Then
				ModuloIdioma = TiposDeDatos.ModulosIdiomas.GuardarInstancia

				txtcadenaserror1.Value = Textos(0) 'Imposible aprobar:
				txtcadenaserror2.Value = Textos(1) 'Imposible trasladar:
				Dim sClientTexts As String
				Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
				sClientTexts = ""
				sClientTexts += "alert(""" & JSText(m_sMsgboxAccion(2)) & """)"
				sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
				Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)
				Dim sTipoVisor As String = If(GetTipoVisorFromTipoSolicitud(tipoSolicitud.Value) = TipoVisor.Solicitudes, "", "?TipoVisor=" & GetTipoVisorFromTipoSolicitud(tipoSolicitud.Value))
				Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "init", "<script>window.open(""" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx" & sTipoVisor & """,""_top"")</script>")

				oInstancia = Nothing
				Exit Sub
			End If
		ElseIf ((sAccion = "trasladadatrasladarinstancia") Or (sAccion = "trasladadatrasladarcontrato")) Then
			If oInstancia.Estado = TipoEstadoSolic.Anulada Then
				Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "init", "<script>window.open(""" & "../workflow/imposibleaccion.aspx?Instancia=" & lInstancia & "&Accion=3&TipoSolicitud=" & tipoSolicitud.Value & """,""fraWSMain"")</script>")
				Exit Sub
			End If
		End If

		Page.ClientScript.RegisterStartupScript(Me.GetType(), "MostrarFSWSServer", "<script>MostrarFSWSServer();</script>")

		Dim oUnidadesUO As FSNServer.UnidadesOrg

		ModuloIdioma = TiposDeDatos.ModulosIdiomas.Traslados
		lblLitCreadoPor.Text = Textos(25) & ":"
		lblLitTipo.Text = Textos(23) & ":"
		lblLitFecAlta.Text = Textos(24) & ":"
		lblTrasladoProv.Text = Textos(5)
		lblMsg1.Text = Textos(6)
		lblCIF.Text = Textos(7)
		lblCod.Text = Textos(8)
		lblDen.Text = Textos(9)
		lblTrasladoUsu.Text = Textos(10)
		lblMsg2.Text = Textos(11)
		lblUO.Text = Textos(12)
		lblDep.Text = Textos(13)
		lblNombre.Text = Textos(14)
		lblApe.Text = Textos(15)
		cmdBuscar.Value = Textos(16)
		cmdBuscar2.Value = Textos(16)
		cmdContinuarTraslado.Value = Textos(17)
		cmdCancelarTraslado.Value = Textos(21) 'Cancelar
		txtCualquiera.Value = Textos(18)
		m_sCualquiera = txtCualquiera.Value
		txtcadenaserror1.Value = Textos(0)
		txtcadenaserror2.Value = Textos(19)
		txtcadenaserror3.Value = Textos(20)
		txtcadenaserror4.Value = Textos(22)

		FSNPageHeader.TituloCabecera = Textos(0) & ": " & oInstancia.Den(Idioma)
		lblTipo.Text = oInstancia.Solicitud.Codigo + " - " + oInstancia.Solicitud.Den(Idioma)
		imgInfTipo.Attributes.Add("onclick", "DetalleTipoSolicitud(" & oInstancia.Solicitud.ID & ")")
		If String.IsNullOrEmpty(Contrato.Value) Then
			lblIDInstanciayEstado.Text = oInstancia.ID
		Else
			lblIDInstanciayEstado.Text = CodContrato.Value
		End If
		If oInstancia.PeticionarioProve = "" Then
			lblCreadoPor.Text = oInstancia.NombrePeticionario
		Else
			lblCreadoPor.Text = oInstancia.PeticionarioProve + " (" + oInstancia.NombrePeticionario + ")"
		End If
		Me.lblFecAlta.Text = FormatDate(oInstancia.FechaAlta, FSNUser.DateFormat)

		If Not oInstancia.CampoImporte Is Nothing Then
			lblLitImporte.Text = oInstancia.CampoImporte & ":"
			lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & oInstancia.Moneda
		Else
			If oInstancia.Solicitud.Tipo = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras Then
				lblLitImporte.Text = Textos(2)  'Importe:
				lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSNUser.NumberFormat) & " " & oInstancia.Moneda
			Else
				lblLitImporte.Visible = False
				lblImporte.Visible = False
			End If
		End If

		'Carga el combo de las unidades organizarivas:
		oUnidadesUO = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
		oUnidadesUO.CargarUnidadesOrganizativas(Idioma, FSNUser.Cod, FSNUser.PMRestringirPersonasUOUsuario)

		ugtxtUO_wdd.TextField = "DEN"
		ugtxtUO_wdd.ValueField = "COD"
		ugtxtUO_wdd.DataSource = From datos In oUnidadesUO.Data.Tables(0)
								 Select New With {.COD = datos("COD_UON1") & ";" & datos("COD_UON2") & ";" & datos("COD_UON3"), .DEN = datos("DEN")}
		ugtxtUO_wdd.DataBind()
		If FSNUser.PMRestringirPersonasUOUsuario = False Then
			ugtxtUO_wdd.Items(0).Text = Me.txtCualquiera.Value
		End If
		For Each oItem As Infragistics.Web.UI.ListControls.DropDownItem In ugtxtUO_wdd.Items
			oItem.CssClass = "igdd_FullstepListItemB"
		Next
		ugtxtUO_wdd.ActiveItemIndex = 0
		ugtxtUO_wdd.SelectedItemIndex = 0
		oUnidadesUO = Nothing

		'combo de departamentos:
		If ugtxtUO_wdd.SelectedItemIndex <> -1 Then
			Dim cod_uon As String() = ugtxtUO_wdd.SelectedItem.Value.Split(";")
			Dim sUON1 As String = IIf(cod_uon(0) Is System.DBNull.Value, Nothing, cod_uon(0))
			Dim sUON2 As String = IIf(cod_uon(1) Is System.DBNull.Value, Nothing, cod_uon(1))
			Dim sUON3 As String = IIf(cod_uon(2) Is System.DBNull.Value, Nothing, cod_uon(2))
			CargarDepartamentos(sUON1, sUON2, sUON3)
		End If

		GuardarConWorkflowXML()

		Page.ClientScript.RegisterStartupScript(Me.GetType(), "Traslados", "<script>document.getElementById('divDevolverSiguientesEtapas').style.display='none'; document.getElementById('divDevolucion').style.display='none'; document.getElementById('divTraslado').style.display='inline'</script>")
	End Sub
	''' <summary>
	''' Genera el xml para guardado o realizaci�n de una acci�n de una solicitud o contrato.
	''' </summary>
	''' <param name="oInstancia">Objeto instancia</param>
	''' <param name="sAccion">Acci�n que se est� realizando (guardarsolicitud, guardarcontrato, "",...)</param>
	''' <remarks>Llamada desde: Page_Load y cmdAceptarControlPartida_ServerClick; Tiempo m�x: 0,5 sg;</remarks>
	Private Sub RealizarAccion_Guardar(ByVal oInstancia As FSNServer.Instancia, ByVal sAccion As String)
		If Not (sAccion = "guardarsolicitud" OrElse sAccion = "guardarcontrato") Then 'ES UNA ACCION CUALQUIERA
			ModuloIdioma = TiposDeDatos.ModulosIdiomas.RealizarAccion

			'Textos de idiomas:
			If oInstancia.Solicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.Factura Then
				lblEtapas.Text = Textos(17)
			Else
				lblEtapas.Text = Textos(3)
			End If
			m_sIdiSeleccioneRol = Textos(5)
			lblComentarios.Text = Textos(8)
			cmdAceptar.Value = Textos(9)
			cmdCancelar.Value = Textos(10)
			txtIdiMsgbox.Value = Textos(13)
			m_sIdiListaPart = Textos(12)
			EstadoInstancia.Value = oInstancia.Estado

			'Datos de la instancia:
			Formulario.Value = oInstancia.IdFormulario

			'Carga la acci�n:
			Dim oAccion As FSNServer.Accion
			oAccion = FSNServer.Get_Object(GetType(FSNServer.Accion))
			oAccion.Id = idAccion.Value
			oAccion.CargarAccion(Idioma)

			If oAccion.IncrementarId And Not oAccion.TipoRechazo = TipoRechazoAccion.RechazoDefinitivo And Not oAccion.TipoRechazo = TipoRechazoAccion.Anulacion Then
				NewInstancia.Value = oInstancia.ReservarInstanciaID()
				oInstancia.NuevoID = Val(Me.NewInstancia.Value)
				FSNPageHeader.TituloCabecera = oInstancia.NuevoID & " - " & Textos(11) & " " & oAccion.Den
			Else
				If Not String.IsNullOrEmpty(Contrato.Value) Then
					FSNPageHeader.TituloCabecera = CodContrato.Value & " - " & Textos(11) & " " & oAccion.Den
				Else
					FSNPageHeader.TituloCabecera = oInstancia.ID & " - " & Textos(11) & " " & oAccion.Den
				End If
			End If

			If oAccion.TipoRechazo = TipoRechazoAccion.RechazoDefinitivo Or oAccion.TipoRechazo = TipoRechazoAccion.RechazoTemporal Then
				lblRoles.Text = Textos(14)
			Else
				lblRoles.Text = Textos(4)
			End If

			oInstancia.Importe = ViewState("Importe")

			'Comprueba las etapas por las que pasar� (oEtapas se carga en el Page_load) 
			If oEtapas.Tables.Count = 0 Then 'Tarea 3400: Me aseguro q sea as� lo de "(oEtapas se carga en el Page_load) cambiado por Integraci�n"
				If blnAlta Then
					oEtapas = oInstancia.DevolverSiguientesEtapasForm(idAccion.Value, FSNUser.CodPersona, Idioma, ds, sRolPorWebService, Bloque.Value, Workflow.Value)
				Else
					oEtapas = oInstancia.DevolverSiguientesEtapas(idAccion.Value, FSNUser.CodPersona, Idioma, ds, , , sRolPorWebService)
				End If
			End If
			Dim RolPorWebService As Boolean = False
			If Not sRolPorWebService = "-1" Then
				If Not (Accion.Value = "guardarsolicitud" OrElse Accion.Value = "guardarcontrato") Then
					Dim ContadorParalelo As Integer = 0 ' 0-> no etapas paralelo. Eoc-> etapas paralelo.
					Dim sBloqueRolPorWebService() As String
					sBloqueRolPorWebService = Split(sRolPorWebService, ",")
					Dim sArrRolPorWebService() As String
					Dim lRolPorWebService As String

					If (oEtapas.Tables.Count > 1) Then
						For Each oRow As DataRow In oEtapas.Tables(0).Rows
							BloqueDestinoWS.Value = oRow.Item("BLOQUE")
							lRolPorWebService = -1

							For i As Integer = 0 To UBound(sBloqueRolPorWebService)
								sArrRolPorWebService = Split(sBloqueRolPorWebService(i), "@")

								If sArrRolPorWebService(0) = CStr(oRow.Item("BLOQUE")) Then
									lRolPorWebService = CLng(sArrRolPorWebService(1))
									Exit For
								End If
							Next

							If lRolPorWebService > -1 Then
								RolPorWebService = True
								GuardarConWorkflowXML()

								BloqueDestinoWS.Value = ""

								Dim HaIdoMalWebService As Boolean = Instancia_LlamadaWebService(lRolPorWebService, oRow.Item("BLOQUE"), ContadorParalelo)
								ContadorParalelo = ContadorParalelo + 1

								If HaIdoMalWebService Then
									Session.Remove(NombreXML.Value)

									oInstancia.Actualizar_En_proceso(0, Bloque.Value)
									oInstancia.Actualizar_En_proceso(0)

									Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AbrirEnfraWSMain", "<script>AbrirEnfraWSMain(""" & "../workflow/accionInvalida.aspx?Instancia=" & lInstancia & "&Accion=" & idAccion.Value & "&Version=" & Version.Value & """)</script>")
									Exit Sub
								End If
							Else
									BloqueDestinoWS.Value = ""
							End If
						Next
					End If
				End If
			End If

			If oEtapas.Tables.Count > 0 Then
				If oEtapas.Tables.Count > 1 Then
					For Each oRow As DataRow In oEtapas.Tables(0).Rows
						BloqueDestinoWS.Value = Me.BloqueDestinoWS.Value & oRow.Item("BLOQUE") & " "
						txtaEtapas.InnerText = Me.txtaEtapas.InnerText & oRow.Item("DEN") & Chr(13) & Chr(10)
					Next
					If oEtapas.Tables(0).Rows(0).Item("TIPO") = TipoBloque.Final Then   'Si llega a la etapa de fin:
						lblRoles.Visible = False
						tblGeneral.Rows(3).Visible = False
						lblComentarios.Text = String.Format("{0}:", Textos(16))
					Else
						ugtxtRoles.DataSource = oEtapas.Tables(1)
						ugtxtRoles.DataBind()

						ugtxtRoles.Columns.FromKey("DEN").Header.Caption = Textos(6)
						ugtxtRoles.Columns.FromKey("NOMBRE").Header.Caption = If(oInstancia.Solicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.Factura, Replace(Textos(7), " PM ", " IM "), Textos(7))
					End If
				Else ' = 1
					oInstancia.Actualizar_En_proceso(0, Bloque.Value)
					oInstancia.Actualizar_En_proceso(0)

					Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AbrirEnfraWSMain", "<script>AbrirEnfraWSMain(""" & "../workflow/accionInvalida.aspx?Instancia=" & lInstancia & "&Accion=" & idAccion.Value & "&Version=" & Version.Value & """)</script>")
					Exit Sub
				End If
			Else
				If oAccion.Tipo = 0 AndAlso oAccion.TipoRechazo = 0 AndAlso oAccion.Guardar Then
					If Not String.IsNullOrEmpty(Contrato.Value) Then
						Accion.Value = "guardarcontrato"
					Else
						Accion.Value = "guardarsolicitud"
					End If
					If oInstancia.Solicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras _
							AndAlso (Not dsDatosPartidas Is Nothing AndAlso dsDatosPartidas.Tables("CONTROL").Select("PRES0 IS NOT NULL").Count > 0) _
							OrElse blnAccionSinControlDisponible Then
						oInstancia.ID = Instancia.Value
						ActualizarPartidasPresupuestarias()
						ActualizarDatosPartidasPresupuestarias()
					End If
				Else 'No provoca ning�n cambio de etapa, as� que quita del formulario las filas correspondientes a los cambios de etapas y formularios:
					Dim i As Integer
					For i = 6 To 2 Step -1
						tblGeneral.Rows(i).Visible = False
					Next i
				End If
			End If

			Select Case Accion.Value
				Case "guardarsolicitud"
					oInstancia.Actualizar_En_proceso(1, Bloque.Value)
					'Es una accion que va a guardar
					GuardarConWorkflowXML(NoEstaGuardando:=False)
					GuardarConWorkflowThread(NoEstaGuardando:=False)

					If blnAlta Then
						Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerIdyEstado", "<script>window.parent.fraWSMain.PonerIdSolicitud(" & lInstancia & ");</script>")
					Else
						'ViewState("Importe") es lo que se manda en el XML como importe de la solicitud.
						Page.ClientScript.RegisterStartupScript(Me.GetType(), "HabilitarBotones", "<script>window.parent.fraWSMain.HabilitarBotones(); if(window.parent.fraWSMain.lblImporte) window.parent.fraWSMain.lblImporte.innerHTML='" & FSNLibrary.FormatNumber(ViewState("Importe"), FSNUser.NumberFormat) & " " & oInstancia.Moneda & "';</script>")
					End If
				Case "guardarcontrato"
					oInstancia.Actualizar_En_proceso(1, Bloque.Value)
					GuardarConWorkflowXML(NoEstaGuardando:=False)
					GuardarConWorkflowThread(NoEstaGuardando:=False)

					If blnAlta Then
						Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerIdyEstado", "<script>window.parent.fraWSMain.PonerIdContrato('" & Contrato.Value & "','" & lInstancia & "','" & CodContrato.Value & "');</script>")
					Else
						Page.ClientScript.RegisterStartupScript(Me.GetType(), "HabilitarBotones", "<script>window.parent.fraWSMain.HabilitarBotones();</script>")
					End If
				Case Else
					If Not RolPorWebService Then GuardarConWorkflowXML()

					Dim jsScript As String = "document.getElementById('divTraslado').style.display='none';"
					jsScript += "document.getElementById('divDevolucion').style.display='none';"
					jsScript += "document.getElementById('divDevolverSiguientesEtapas').style.display='inline';"
					Page.ClientScript.RegisterStartupScript(Me.GetType(), "DevolverSiguientesEtapas", jsScript, True)
					'HA PASADO LAS COMPROBACIONES ASI QUE MOSTRAMOS EL FRAWSSERVER
					Page.ClientScript.RegisterStartupScript(Me.GetType(), "MostrarFSWSServer", "<script>MostrarFSWSServer();</script>")
			End Select
			divCabecera.Visible = False

			oEtapas = Nothing
			oInstancia = Nothing
		Else    'HEMOS PULSADO EN GUARDAR
			If Not String.IsNullOrEmpty(Contrato.Value) Then
				oInstancia.Actualizar_En_proceso(1, Bloque.Value)
				GuardarConWorkflowXML(NoEstaGuardando:=False)
				GuardarConWorkflowThread(NoEstaGuardando:=False)

				If blnAlta Then
					Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerIdyEstado", "<script>window.parent.fraWSMain.PonerIdContrato('" & Contrato.Value & "','" & lInstancia & "','" & CodContrato.Value & "');</script>")
				Else
					Page.ClientScript.RegisterStartupScript(Me.GetType(), "HabilitarBotones", "<script>window.parent.fraWSMain.HabilitarBotones();</script>")
				End If
			Else
				oInstancia.Actualizar_En_proceso(1, Bloque.Value)
				GuardarConWorkflowXML(NoEstaGuardando:=False)
				GuardarConWorkflowThread(NoEstaGuardando:=False)

				If blnAlta Then
					If tipoSolicitud.Value = TiposDeDatos.TipoDeSolicitud.NoConformidad Then
						Dim ListaCalc As String = ""
						CompletarDataSetCamposCalculados(ds, Idioma, ListaCalc, False)
						Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerIdyEstado", "<script>window.parent.fraWSMain.PonerIdyEstado('" & oNoConformidad.Instancia & "','" & oNoConformidad.ID & "','" & JSText(oNoConformidad.ProveDen) & "','','','" & FormatDate(oNoConformidad.FechaAlta, FSNUser.DateFormat) & "','" & ListaCalc & "','" & oNoConformidad.Proveedor & "');</script>")
					Else
						Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerIdyEstado", "<script>window.parent.fraWSMain.PonerIdSolicitud(" & Instancia.Value & ");</script>")
					End If
				Else
					'Al guardar una solicitud ya guardada anteriormente (NWDetalleSolicitud) cambiando la moneda, oInstancia.Importe vale distinto que ViewState("Importe"). Se hace oInstancia.Importe = ViewState("Importe") cuando la acci�n es distinta de "guardarsolicitud" y "guardarcontrato" (�?).
					'ViewState("Importe") es lo que se manda en el XML como importe de la solicitud.
					Page.ClientScript.RegisterStartupScript(Me.GetType(), "HabilitarBotones", "<script>window.parent.fraWSMain.HabilitarBotones(); if(window.parent.fraWSMain.lblImporte) window.parent.fraWSMain.lblImporte.innerHTML='" & FSNLibrary.FormatNumber(ViewState("Importe"), FSNUser.NumberFormat) & " " & oInstancia.Moneda & "';</script>")
				End If
			End If
		End If
	End Sub
	''' <summary>
	''' Revisado por: 16/11/2011
	''' Muestra el detalle de las partidas con un disponible inferior al importe solicitado. El mensaje puede ser de aviso o de bloqueo.
	''' </summary>
	''' <param name="iMensajeControlPartidas">Si es un aviso o bloqueo</param>
	''' <returns>Devuelve true/false</returns>
	''' <remarks>Llamada desde: page_Load, cmdAceptar_Click; Tiempo m�x: 0,2 sg</remarks>
	Private Function MostrarControlPartidas(ByVal iMensajeControlPartidas As Integer) As Boolean
		Dim bControlCampo As Boolean = False
		Dim sPres5 As String = String.Empty
		Dim lblDatos As Label
		Dim sCodPartida As String = String.Empty
		Dim iAnyo As Integer = 0
		Dim iAnyoANT As Integer = -1
		Dim sDenPartida As String = String.Empty
		Dim dPresupuestado As Double
		Dim sCodPartidaANT As String = String.Empty
		Dim dTotalSolicitud As Double
		Dim iConLineas As Integer = 0
		Dim dDisponible As Double
		Dim sCodMoneda As String = String.Empty
		Dim dGastoImputadoLinea As Double = 0
		Dim sNombre As String = String.Empty
		Dim dFactorConversion As Double = 0
		Dim bUnicaLinea As Boolean = False

		Me.ModuloIdioma = TiposDeDatos.ModulosIdiomas.GuardarInstancia

		blnControlPartidasMostrado = True

		lblControlDisponible.Text = Textos(12)
		If iMensajeControlPartidas = 1 Then
			lblMsjControlPartidas1.Text = Textos(23) & ". " & Textos(14)
			cmdAceptarControlPartida.Value = Textos(20)
			cmdAceptarControlPartida.Visible = True
			cmdCancelarControlPartida.Value = Textos(21)
		Else
			lblMsjControlPartidas1.Text = Textos(13) & " " & Textos(23) & ":"
			cmdCancelarControlPartida.Value = Textos(22)
		End If

		dsDatosPartidas.Tables(0).DefaultView.Sort = "PRES0 ASC"
		If Not dsDatosPartidas Is Nothing AndAlso dsDatosPartidas.Tables(0).Select("LINEA=0").Length > 0 Then
			Dim dRowLinea0 As DataRow
			dRowLinea0 = dsDatosPartidas.Tables(0).Select("LINEA=0")(0)
			bControlCampo = True
			sPres5 = dRowLinea0.Item("PRES0")
			sCodPartida = DBNullToStr(dRowLinea0.Item("COD_PARTIDA"))
			iAnyo = DBNullToInteger(dRowLinea0.Item("ANYO"))
			sDenPartida = DBNullToSomething(dRowLinea0.Item("DEN_PARTIDA"))
			dPresupuestado = DBNullToDbl(dRowLinea0.Item("PRESUPUESTADO"))
			dDisponible = DBNullToDbl(dRowLinea0.Item("DISPONIBLE"))
			dFactorConversion = DBNullToDbl(dRowLinea0.Item("FC"))
			sCodMoneda = DBNullToStr(dRowLinea0.Item("MON"))
			dTotalSolicitud = CDec(DBNullToDbl(dRowLinea0.Item("TOTAL")) * dFactorConversion)
			If Not (dsDatosPartidas.Tables(0).Select("LINEA > 0").Length > 0) Then
				bUnicaLinea = True
			End If
		End If
		For Each iPartida In dsDatosPartidas.Tables(0).Rows.OfType(Of DataRow).Select(Function(ip) ip("PRES0")).Distinct.ToList
			Dim configuracion As FSNServer.PargenSM = Nothing
			If Not dsDatosPartidas.Tables(0) Is Nothing Then
				If Not Session("FSSMPargen") Is Nothing Then
					If Not bControlCampo Then
						configuracion = Session("FSSMPargen").Item(dsDatosPartidas.Tables(0).Rows(0).Item("PRES0"))
					Else
						configuracion = Session("FSSMPargen").Item(sPres5)
					End If
					If Not configuracion Is Nothing Then
						sNombre = configuracion.DenNivel
					End If
				End If
			End If
			'Cabecera
			Dim tblRow As New HtmlTableRow
			Dim oCell As New HtmlTableCell
			tblRow = New HtmlTableRow
			tblRow.Attributes("class") = "cabeceraColor"
			oCell = New HtmlTableCell
			If configuracion.Plurianual Then
				oCell.Width = "25%"
			Else
				oCell.Width = "30%"
			End If
			oCell.Style.Add("padding-left", "5px")
			Dim lblHeader As New Label
			lblHeader = New Label
			lblHeader.ID = "lblHeader_1"
			lblHeader.Text = sNombre
			oCell.Controls.Add(lblHeader)
			tblRow.Cells.Add(oCell)

			If configuracion.Plurianual Then
				oCell = New HtmlTableCell
				oCell.Width = "5%"
				oCell.Style.Add("padding-left", "5px")
				lblHeader = New Label
				lblHeader.ID = "lblHeader_6"
				lblHeader.Text = Textos(27)
				oCell.Controls.Add(lblHeader)
				tblRow.Cells.Add(oCell)
			End If
			'
			oCell = New HtmlTableCell
			oCell.Width = "12%"
			oCell.Style.Add("padding-left", "5px")
			lblHeader = New Label
			lblHeader.ID = "lblHeader_2"
			lblHeader.Text = Textos(15)
			oCell.Controls.Add(lblHeader)
			tblRow.Cells.Add(oCell)

			oCell = New HtmlTableCell
			oCell.Width = "10%"
			oCell.Style.Add("padding-left", "5px")
			lblHeader = New Label
			lblHeader.ID = "lblHeader_3"
			lblHeader.Text = Textos(16)
			oCell.Controls.Add(lblHeader)
			tblRow.Cells.Add(oCell)

			oCell = New HtmlTableCell
			oCell.Width = "12%"
			oCell.Style.Add("padding-left", "5px")
			lblHeader = New Label
			lblHeader.ID = "lblHeader_4"
			lblHeader.Text = Textos(17)
			oCell.Controls.Add(lblHeader)
			tblRow.Cells.Add(oCell)

			oCell = New HtmlTableCell
			oCell.Width = "36%"
			oCell.Style.Add("padding-left", "5px")
			lblHeader = New Label
			lblHeader.ID = "lblHeader_5"
			lblHeader.Text = Textos(18)
			oCell.Controls.Add(lblHeader)
			tblRow.Cells.Add(oCell)
			tblControlIN.Controls.Add(tblRow)

			For Each oRow In dsDatosPartidas.Tables("CONTROL").Select("OK IS NOT NULL").AsEnumerable.Where(Function(x) Not x("OK")).OrderBy(Of String)(Function(x) x("COD_PARTIDA")).ToList()
				If oRow.Item("LINEA") > 0 Or bUnicaLinea Then
					tblRow = New HtmlTableRow
					If Not bControlCampo Then
						sCodPartida = DBNullToStr(oRow.Item("COD_PARTIDA"))
						iAnyo = DBNullToInteger(oRow.Item("ANYO"))
					End If
					If (Not (sCodPartida = sCodPartidaANT) Or (sCodPartidaANT = "")) Then
						iAnyoANT = -1
						'Si ya hay lineas anteriores introducidas...
						If iConLineas > 1 Then
							'Mostrar el total de las lineas y ajustar las columnas con el rowspan
							If Not tblControlIN.FindControl("celda_1_" & sCodPartidaANT) Is Nothing Then _
								CType(tblControlIN.FindControl("celda_1_" & sCodPartidaANT), HtmlTableCell).RowSpan = iConLineas

							If Not tblControlIN.FindControl("celda_2_" & sCodPartidaANT & "_" & iAnyo) Is Nothing Then _
								CType(tblControlIN.FindControl("celda_2_" & sCodPartidaANT & "_" & iAnyo), HtmlTableCell).RowSpan = iConLineas

							If Not tblControlIN.FindControl("celda_3_" & sCodPartidaANT & "_" & iAnyo) Is Nothing Then _
								CType(tblControlIN.FindControl("celda_3_" & sCodPartidaANT & "_" & iAnyo), HtmlTableCell).RowSpan = iConLineas

							configuracion = Session("FSSMPargen").Item(dsDatosPartidas.Tables(0).Rows(0).Item("PRES0"))

							'A�adir el total
							oCell = New HtmlTableCell
							oCell.ColSpan = IIf(configuracion.Plurianual, 4, 3)
							oCell.Align = "right"
							lblDatos = New Label
							lblDatos.CssClass = "parrafo"
							lblDatos.ID = "lblLitTotal_" & sCodPartidaANT
							lblDatos.Text = Textos(19)
							oCell.Controls.Add(lblDatos)
							tblRow.Cells.Add(oCell)
							'Total Solicitud
							oCell = New HtmlTableCell
							oCell.Height = "25px"
							oCell.BgColor = "#FF0000"
							oCell.Align = "right"
							lblDatos = New Label
							lblDatos.CssClass = "parrafo"
							lblDatos.ID = "lblTotal_" & sCodPartidaANT
							lblDatos.Text = FSNLibrary.FormatNumber(dTotalSolicitud, FSNUser.NumberFormat) & " " & sCodMoneda
							oCell.Controls.Add(lblDatos)
							tblRow.Cells.Add(oCell)
							tblControlIN.Controls.Add(tblRow)

							'la linea
							tblRow = New HtmlTableRow
							oCell = New HtmlTableCell
							oCell.ColSpan = IIf(configuracion.Plurianual, 6, 5)
							oCell.Height = "25px"
							oCell.Style.Add("border-bottom", "solid 1px black")
							Dim espacio As New Literal()
							espacio.Text = "&nbsp;"
							oCell.Controls.Add(espacio)
							tblRow.Cells.Add(oCell)
							tblControlIN.Controls.Add(tblRow)
							tblRow = New HtmlTableRow
						Else
							If Not String.IsNullOrEmpty(sCodPartidaANT) Then
								If Not tblControlIN.FindControl("celda_4_0_" & sCodPartidaANT & "_" & iAnyo) Is Nothing Then
									CType(tblControlIN.FindControl("celda_4_0_" & sCodPartidaANT & "_" & iAnyo), HtmlTableCell).BgColor = "#FF0000"
									CType(tblControlIN.FindControl("celda_4_0_" & sCodPartidaANT & "_" & iAnyo), HtmlTableCell).Height = "25px"

									'la linea
									tblRow = New HtmlTableRow
									oCell = New HtmlTableCell
									oCell.ColSpan = IIf(configuracion.Plurianual, 6, 5)
									oCell.Height = "25px"
									oCell.Style.Add("border-bottom", "solid 1px black")
									Dim espacio As New Literal()
									espacio.Text = "&nbsp;"
									oCell.Controls.Add(espacio)
									tblRow.Cells.Add(oCell)
									tblControlIN.Controls.Add(tblRow)
									tblRow = New HtmlTableRow
								End If
							End If
						End If
						iConLineas = 0
						dTotalSolicitud = 0

						If Not bControlCampo Then sDenPartida = DBNullToStr(oRow.Item("DEN_PARTIDA"))

						'Partida
						oCell = New HtmlTableCell
						oCell.ID = "celda_1_" & sCodPartida
						lblDatos = New Label
						lblDatos.CssClass = "parrafo"
						lblDatos.ID = "lblDatos_1_" & iConLineas & "_" & sCodPartida
						lblDatos.Text = sCodPartida & " - " & sDenPartida
						oCell.Controls.Add(lblDatos)
						tblRow.Cells.Add(oCell)
					End If
					If Not (iAnyo = iAnyoANT) Then
						If Not bControlCampo Then
							dPresupuestado = DBNullToDbl(oRow.Item("PRESUPUESTADO"))
							dDisponible = DBNullToDbl(oRow.Item("DISPONIBLE"))
							dFactorConversion = DBNullToDbl(oRow.Item("FC"))
							sCodMoneda = DBNullToStr(oRow.Item("MON"))
						End If
						If configuracion.Plurianual Then
							oCell = New HtmlTableCell
							oCell.ID = "celda_6_" & sCodPartida & "_" & iAnyo
							oCell.Align = "center"
							lblDatos = New Label
							lblDatos.CssClass = "parrafo"
							lblDatos.ID = "lblDatos_6_" & iConLineas & "_" & sCodPartida & "_" & iAnyo
							lblDatos.Text = iAnyo
							oCell.Controls.Add(lblDatos)
							tblRow.Cells.Add(oCell)
						End If
						'Presupuestado
						oCell = New HtmlTableCell
						oCell.ID = "celda_2_" & sCodPartida & "_" & iAnyo
						oCell.Align = "right"
						lblDatos = New Label
						lblDatos.CssClass = "parrafo"
						lblDatos.ID = "lblDatos_2_" & iConLineas & "_" & sCodPartida & "_" & iAnyo
						lblDatos.Text = FSNLibrary.FormatNumber(dPresupuestado, FSNUser.NumberFormat) & " " & sCodMoneda
						oCell.Controls.Add(lblDatos)
						tblRow.Cells.Add(oCell)

						'Disponible actual
						oCell = New HtmlTableCell
						oCell.ID = "celda_3_" & sCodPartida & "_" & iAnyo
						oCell.Align = "right"
						lblDatos = New Label
						lblDatos.CssClass = "parrafo"
						lblDatos.ID = "lblDatos_3_" & iConLineas & "_" & sCodPartida & "_" & iAnyo
						lblDatos.Text = FSNLibrary.FormatNumber(dDisponible, FSNUser.NumberFormat) & " " & sCodMoneda
						oCell.Controls.Add(lblDatos)
						tblRow.Cells.Add(oCell)
					End If
					'Gasto imputado a la solicitud
					oCell = New HtmlTableCell
					oCell.ID = "celda_4_" & iConLineas & "_" & sCodPartida & "_" & iAnyo
					oCell.Align = "right"
					lblDatos = New Label
					lblDatos.CssClass = "parrafo"
					lblDatos.ID = "lblDatos_4_" & iConLineas & "_" & sCodPartida & "_" & iAnyo
					dGastoImputadoLinea = CDec(DBNullToDbl(oRow.Item("TOTAL")) * dFactorConversion)
					lblDatos.Text = FSNLibrary.FormatNumber(dGastoImputadoLinea, FSNUser.NumberFormat) & " " & sCodMoneda
					dTotalSolicitud = CDec(dTotalSolicitud + dGastoImputadoLinea)
					oCell.Controls.Add(lblDatos)
					tblRow.Cells.Add(oCell)
					'Articulo
					oCell = New HtmlTableCell
					lblDatos = New Label
					lblDatos.CssClass = "parrafo"
					lblDatos.ID = "lblDatos_5_" & iConLineas & "_" & sCodPartida & "_" & iAnyo
					If Not IsDBNull(oRow.Item("COD_ARTICULO")) Then
						lblDatos.Text = DBNullToStr(oRow.Item("COD_ARTICULO")) & " - " & DBNullToStr(oRow.Item("DEN_ARTICULO"))
					End If
					oCell.Controls.Add(lblDatos)
					tblRow.Cells.Add(oCell)
					tblControlIN.Controls.Add(tblRow)
					sCodPartidaANT = sCodPartida
					iAnyoANT = iAnyo
					iConLineas = iConLineas + 1
				End If
			Next

			If iConLineas > 1 Then
				'Mostrar el total de las lineas y ajustar las columnas con el rowspan
				If Not tblControlIN.FindControl("celda_1_" & sCodPartidaANT) Is Nothing Then _
					CType(tblControlIN.FindControl("celda_1_" & sCodPartidaANT), HtmlTableCell).RowSpan = iConLineas

				If Not tblControlIN.FindControl("celda_2_" & sCodPartidaANT) Is Nothing Then _
					CType(tblControlIN.FindControl("celda_2_" & sCodPartidaANT), HtmlTableCell).RowSpan = iConLineas

				If Not tblControlIN.FindControl("celda_3_" & sCodPartidaANT) Is Nothing Then _
					CType(tblControlIN.FindControl("celda_3_" & sCodPartidaANT), HtmlTableCell).RowSpan = iConLineas

				'A�adir el total
				tblRow = New HtmlTableRow
				oCell = New HtmlTableCell
				oCell.ColSpan = IIf(configuracion.Plurianual, 4, 3)
				oCell.Align = "right"
				lblDatos = New Label
				lblDatos.CssClass = "parrafo"
				lblDatos.ID = "lblLitTotal_" & sCodPartida
				lblDatos.Text = Textos(19)
				oCell.Controls.Add(lblDatos)
				tblRow.Cells.Add(oCell)

				oCell = New HtmlTableCell
				oCell.Align = "right"
				oCell.BgColor = "#FF0000"
				lblDatos = New Label
				lblDatos.CssClass = "parrafo"
				lblDatos.ID = "lblTotal_" & sCodPartida
				lblDatos.Text = FSNLibrary.FormatNumber(dTotalSolicitud, FSNUser.NumberFormat) & " " & sCodMoneda
				oCell.Controls.Add(lblDatos)
				tblRow.Cells.Add(oCell)
				tblControlIN.Controls.Add(tblRow)
			Else
				If Not tblControlIN.FindControl("celda_4_0_" & sCodPartidaANT) Is Nothing Then
					CType(tblControlIN.FindControl("celda_4_0_" & sCodPartidaANT), HtmlTableCell).BgColor = "#FF0000"
					CType(tblControlIN.FindControl("celda_4_0_" & sCodPartidaANT), HtmlTableCell).Height = "25px"
				End If
			End If
		Next
		divCabecera.Visible = False
		Page.ClientScript.RegisterStartupScript(Me.GetType(), "ControlPartidas", "<script>document.getElementById('divDevolverSiguientesEtapas').style.display='none';document.getElementById('divTraslado').style.display='none';document.getElementById('divDevolucion').style.display='none';document.getElementById('divControlPartidas').style.display='inline'</script>")
		Page.ClientScript.RegisterStartupScript(Me.GetType(), "MostrarFSWSServer", "<script>MostrarFSWSServer();</script>")
	End Function
	''' <summary>
	''' Proceso que lleva a cabo la eliminacion de las partidas que el importe sea inferior al solicitado
	''' 1.- Comprueba si la partida esta fuera del desglose
	''' 2.- Si hay una unica linea sabemos que esta fuera y no hay desglose
	''' 3.- Trata aquellas lineas del desglose y elimina aquellas lineas que cumplen con el disponible
	''' </summary>
	''' <returns>Devuelve una cadena con las partidas y el importe para su posterior almacenamiento en BBDD</returns>
	''' <remarks>Llamada desde; Tiempo m�ximo</remarks>
	Private Function TratarDSControlPartidas() As String
		Dim bControlCampo As Boolean = False
		Dim sCodPartida As String = String.Empty
		Dim sCodPartidaANT As String = String.Empty
		Dim dTotalSolicitud As Double
		Dim iConLineas As Integer = 0
		Dim dDisponible As Double
		Dim dImporte As Double
		Dim sLineasEliminar As String = String.Empty
		Dim sLineasTratar As String = String.Empty
		Dim iIndex As Integer
		Dim sPres0 As String = String.Empty
		Dim sPres1 As String = String.Empty
		Dim sPres2 As String = String.Empty
		Dim sPres3 As String = String.Empty
		Dim sPres4 As String = String.Empty
		Dim dFactorConversion As Double = 1
		Dim sResultado As String = String.Empty ' Cadena de contendra las partidas con el gasto imputado a la solicitud, para que lo procesemos en la BBDD
		Dim bSaltarDesglose As Boolean = False

		blnControlPartidasMostrado = False

		dsControlPartidas.Tables("CONTROL").DefaultView.Sort = "PRES0 ASC"
		If dsControlPartidas IsNot Nothing AndAlso dsControlPartidas.Tables(0).Select("LINEA=0").Length > 0 Then
			Dim dRowLinea0 As DataRow
			dRowLinea0 = dsControlPartidas.Tables(0).Select("LINEA=0")(0)
			bControlCampo = True
			sPres0 = DBNullToStr(dRowLinea0.Item("PRES0"))
			sPres1 = DBNullToStr(dRowLinea0.Item("PRES1"))
			sPres2 = DBNullToStr(dRowLinea0.Item("PRES2"))
			sPres3 = DBNullToStr(dRowLinea0.Item("PRES3"))
			sPres4 = DBNullToStr(dRowLinea0.Item("PRES4"))
			dDisponible = CDec(DBNullToDbl(dRowLinea0.Item("PRESUPUESTADO")) - DBNullToDbl(dRowLinea0.Item("COMPROMETIDO")) - DBNullToDbl(dRowLinea0.Item("SOLICITADO")))
			dFactorConversion = DBNullToDbl(dRowLinea0.Item("FC"))
			dTotalSolicitud = CDec((DBNullToDbl(dRowLinea0.Item("CANT")) * DBNullToDbl(dRowLinea0.Item("PRECIO_UNITARIO"))) * dFactorConversion)
			bSaltarDesglose = True
		End If

		If bSaltarDesglose Then
			If dDisponible >= dTotalSolicitud Then
				dsControlPartidas.Tables(0).Rows.Clear()
			End If
			sResultado = sPres0 & "#" & sPres1 & "#" & sPres2 & "#" & sPres3 & "#" & sPres4 & "#" & DblToSQLFloat(dTotalSolicitud) & "|"
			TratarDSControlPartidas = sResultado
			Exit Function
		End If

		For Each oRow In dsControlPartidas.Tables(0).DefaultView.Table.Rows
			If oRow.item("LINEA") > 0 Then
				If Not bControlCampo Then
					sPres0 = DBNullToStr(oRow.item("PRES0"))
					sPres1 = DBNullToStr(oRow.item("PRES1"))
					sPres2 = DBNullToStr(oRow.item("PRES2"))
					sPres3 = DBNullToStr(oRow.item("PRES3"))
					sPres4 = DBNullToStr(oRow.item("PRES4"))
				End If
				sCodPartida = sPres0 & "#" & sPres1 & "#" & sPres2 & "#" & sPres3 & "#" & sPres4

				If sCodPartida <> sCodPartidaANT AndAlso sCodPartidaANT <> "" Then
					If dDisponible >= dTotalSolicitud Then
						'Eliminar las partidas correctas
						sLineasEliminar = sLineasEliminar & sLineasTratar
					End If
					sResultado = sResultado & sCodPartidaANT & "#" & DblToSQLFloat(dTotalSolicitud) & "|"

					dTotalSolicitud = 0
					sLineasTratar = ""
				End If
				If Not bControlCampo Then
					dFactorConversion = DBNullToDbl(oRow.item("FC"))
					dDisponible = CDec(DBNullToDbl(oRow.item("PRESUPUESTADO")) - DBNullToDbl(oRow.item("COMPROMETIDO")) - DBNullToDbl(oRow.item("SOLICITADO")))
				End If
				dImporte = CDec((DBNullToDbl(oRow.item("CANT")) * DBNullToDbl(oRow.item("PRECIO_UNITARIO"))) * dFactorConversion)
				dTotalSolicitud = CDec(dTotalSolicitud + dImporte)
				If bControlCampo Then
					sLineasTratar = sLineasTratar & "," & iIndex + 1
				Else
					sLineasTratar = sLineasTratar & "," & iIndex
				End If
				sCodPartidaANT = sCodPartida
				iIndex = iIndex + 1
			End If
		Next

		If dDisponible >= dTotalSolicitud Then
			'Eliminar las partidas correctas
			sLineasEliminar = sLineasEliminar & sLineasTratar
		End If
		sResultado = sResultado & sCodPartidaANT & "#" & DblToSQLFloat(dTotalSolicitud) & "|"

		If sLineasEliminar <> "" Then
			Dim arrAux() As String
			arrAux = Split(sLineasEliminar, ",")
			For i = UBound(arrAux) To 0 Step -1
				If arrAux(i) <> "" Then
					dsControlPartidas.Tables(0).DefaultView.Table.Rows.RemoveAt(arrAux(i))
				End If
			Next
			If dsControlPartidas.Tables(0).Rows.Count = 1 AndAlso dsControlPartidas.Tables(0).Select("LINEA=0").Length = 1 Then
				dsControlPartidas.Tables(0).Rows.RemoveAt(0)
			End If
		End If
		TratarDSControlPartidas = sResultado
	End Function
	''' Revisado por: blp. Fecha: 14/11/2011
	'''     ''' <summary>
	''' M�todo que se ejecuta en el evento SelectionChanged del control ugtxtUO_wdd
	''' </summary>
	''' <param name="sender">Control ugtxtUO_wdd</param>
	''' <param name="e">Argumentos del evento</param>
	''' <remarks>Llamada desde el evento. Tiempo m�ximo menos 0,1 seg</remarks>
	Public Sub ugtxtUO_wdd_SelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles ugtxtUO_wdd.SelectionChanged
		If blnBorrarPersona = True Then
			Me.txtNombre.Text = ""
			Me.txtApe.Text = ""
		End If
		Me.SelectedItem.Value = ""
		If Page.IsPostBack Then
			If ugtxtUO_wdd.SelectedItems.Count > 0 Then
				With ugtxtUO_wdd.SelectedItem()
					Dim aUON As String() = .Value.Split(";")
					Dim sUON1 As String = IIf(aUON(0) Is System.DBNull.Value, Nothing, aUON(0))
					Dim sUON2 As String = IIf(aUON(1) Is System.DBNull.Value, Nothing, aUON(1))
					Dim sUON3 As String = IIf(aUON(2) Is System.DBNull.Value, Nothing, aUON(2))
					CargarDepartamentos(sUON1, sUON2, sUON3)
				End With
			End If
			Page.ClientScript.RegisterStartupScript(Me.GetType(), "VisibleLayerUO", "<script>document.getElementById('divTraslado').style.display='inline'</script>")
		End If
		If FSNUser.PMRestringirPersonasUOUsuario = False Then
			ugtxtUO_wdd.Items(0).Text = Me.txtCualquiera.Value
		End If
		ugtxtUO_wdd.ClearSelection()
		Up_Uon.Update()
	End Sub
	''' <summary>
	''' Funcion que con la accion retorna el Xml que contiene la llamada externa
	''' </summary>
	''' <param name="idAccion"></param>
	''' <returns>Devuelve el nombre del Xml que contiene la llamada externa</returns>
	''' <remarks></remarks>
	Private Function DevolverXmlLlamadaExterna(ByVal idAccion As Int32) As FSNServer.Accion
		Dim oAccion As FSNServer.Accion = FSNServer.Get_Object(GetType(FSNServer.Accion))
		oAccion.Id = idAccion
		oAccion.CargarAccion(ConfigurationManager.AppSettings("idioma"))

		Return oAccion
	End Function
	''' <summary>
	''' Ejecuta la llamada a un webservice q no incluimos en el proyecto.
	''' </summary>
	''' <param name="sUrl">url del servicio con la funci�n a realizar. Ejemplo:http://localhost/FSNWebService_31900_9/GES_Tallent_WS.asmx/DevolverAprobador </param>
	''' <param name="nomEntradaXml">Nombre de la variable q usa el webservice a realizar". En caso de webmethod para gestamp Tarea 3400, el xml</param>
	''' <param name="valorXml">Valor de la variable q usa el webservice a realizar. En caso de webmethod para gestamp Tarea 3400, el Xml (ruta+nombre)</param>
	''' <param name="nomResult">Nombre de la variable del webservice donde se deja el resultado</param>
	''' <param name="nomEntradaInstancia">Nombre de la variable Instancia q usa el webservice a realizar.</param>
	''' <param name="valorInstancia">Instancia q usa el webservice a realizar.</param> 
	''' <param name="nomEntradaBloque">Nombre de la variable "Bloque destino q usa el webservice a realizar"</param>
	''' <param name="valorBloque">Bloque q usa el webservice a realizar</param>  
	''' <returns>Lo q responde el webservice</returns>
	''' <remarks>Llamada desde: ComprobarCondiciones    Instancia_LlamadaWebService ; Tiempo m�ximo: 0,3</remarks>
	Private Function LlamarWSServicio(ByVal sUrl As String, ByVal nomEntradaXml As String, ByVal valorXml As String, ByVal nomResult As String,
				Optional ByVal nomEntradaInstancia As String = "", Optional ByVal valorInstancia As String = "",
				Optional ByVal nomEntradaBloque As String = "", Optional ByVal valorBloque As String = "") As String
		Dim valorResult As String = String.Empty
		Dim url, metodo, sSoapAction, sXml As String
		Dim num As Integer

		num = sUrl.IndexOf(".asmx")
		url = sUrl.Substring(0, num + 5)
		metodo = sUrl.Substring(num + 6)
		sSoapAction = "http://tempuri.org/" + metodo

		sXml = "<?xml version=""1.0"" encoding=""utf-8""?>"
		sXml = sXml + "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"
		sXml = sXml + "<soap:Body>"
		sXml = sXml + "<" + metodo + " xmlns=""http://tempuri.org/"">"
		sXml = sXml + "<" + nomEntradaXml + ">" + valorXml + "</" + nomEntradaXml + ">"
		If nomEntradaInstancia <> "" Then sXml = sXml + "<" + nomEntradaInstancia + ">" + valorInstancia + "</" + nomEntradaInstancia + ">"
		If nomEntradaBloque <> "" Then sXml = sXml + "<" + nomEntradaBloque + ">" + valorBloque + "</" + nomEntradaBloque + ">"
		sXml = sXml + "</" + metodo + ">"
		sXml = sXml + "</soap:Body>"
		sXml = sXml + "</soap:Envelope>"

		Dim targetURI As New Uri(url)
		Dim req As System.Net.HttpWebRequest = DirectCast(System.Net.WebRequest.Create(targetURI), System.Net.HttpWebRequest)
		req.Headers.Add("SOAPAction", sSoapAction)
		req.ContentType = "text/xml; charset=""utf-8"""
		req.Accept = "text/xml"
		req.Method = "POST"
		Dim stm As Stream = req.GetRequestStream
		Dim stmw As New StreamWriter(stm)
		stmw.Write(sXml)
		stmw.Close()
		stm.Close()

		'Se realiza la llamada al WS
		Dim response As System.Net.WebResponse
		response = req.GetResponse()
		Dim responseStream As Stream = response.GetResponseStream

		Dim sr As New StreamReader(responseStream)
		Dim soapResult As String
		'Recogemos la respuesta del WS
		soapResult = sr.ReadToEnd
		Dim xmlResponse As New System.Xml.XmlDocument
		xmlResponse.LoadXml(soapResult)
		Dim xmlResult As System.Xml.XmlNode
		xmlResult = xmlResponse.GetElementsByTagName(nomResult).Item(0)
		If Not xmlResult Is Nothing Then
			'Recojo el valor del indicador de error
			valorResult = xmlResult.InnerText
		End If
		Return valorResult
	End Function
	''' <summary>
	''' Gestamp tiene 100 etapas a las q ir desde peticionario en varios flujos. Se quitan para q solo sea una etapa y un web 
	''' service desde integraci�n nos dice cuales son las personas de autoasignaci�n.
	''' </summary>
	''' <param name="lRolPorWebService">Rol para 100 etapas.</param>
	''' <param name="BloqueDestino">esa unica etapa q antes eran 100</param>
	''' <param name="ContadorParalelo">Por si tiene q ir a varias etapas, los participantes cambian de uno a otro.</param>  
	''' <returns>Si ha podido o no devolver una lista de personas separadas por #</returns>
	''' <remarks>Llamada desde: RealizarAccion_Guardar ; Tiempo maximo: 0,3</remarks>
	Private Function Instancia_LlamadaWebService(ByVal lRolPorWebService As Long, ByVal BloqueDestino As Long, ByVal ContadorParalelo As Integer) As Boolean
		Dim dsXML As DataSet = CType(Session(NombreXML.Value), DataSet)
		dsXML.WriteXml(ConfigurationManager.AppSettings("rutaXMLPendiente") & "\" & NombreXML.Value, XmlWriteMode.WriteSchema)

		Dim oValorCampo As Object
		oValorCampo = LlamarWSServicio(ConfigurationManager.AppSettings("UrlParticipantesExterno"), "oXMLDocCadena",
									   ConfigurationManager.AppSettings("rutaXMLPendiente") & "\" & NombreXML.Value,
									   "DevolverAprobadorResponse", "oXMLDocInstancia", "", "oXMLDocBLoque", BloqueDestino)

		'oValorCampo sera una lista de personas separadas por #. Ejemplo: 37HA#C227K#
		Dim oDTPMParticipantes As DataTable
		If ContadorParalelo = 0 Then
			oDTPMParticipantes = ds.Tables.Add("TEMP_PARTICIPANTES_EXTERNOS")
			oDTPMParticipantes.Columns.Add("ID", System.Type.GetType("System.Int32"))
			oDTPMParticipantes.Columns.Add("ROL", System.Type.GetType("System.Int32"))
			oDTPMParticipantes.Columns.Add("PER", System.Type.GetType("System.String"))
			'esto es un caso muy concreto de gestamp, nunca habra proveedor implicado
		Else
			oDTPMParticipantes = ds.Tables("TEMP_PARTICIPANTES_EXTERNOS")
		End If

		Dim keys(1) As DataColumn
		keys(0) = oDTPMParticipantes.Columns("ROL")
		keys(1) = oDTPMParticipantes.Columns("PER")
		oDTPMParticipantes.PrimaryKey = keys

		Dim dtNewRow As DataRow
		Dim sAux() As String = Split(oValorCampo, "#")

		For i As Integer = 0 To UBound(sAux) - 1 Step 1
			dtNewRow = oDTPMParticipantes.NewRow

			dtNewRow.Item("ID") = i + 1
			dtNewRow.Item("ROL") = lRolPorWebService
			dtNewRow.Item("PER") = sAux(i)

			oDTPMParticipantes.Rows.Add(dtNewRow)
		Next
		Session(NombreXML.Value) = ds
		Return (oValorCampo = "")
	End Function

#Region "clase RequestManager"

	Private Class RequestManager
		Private _DS As DataSet
		Private _Request As System.Web.HttpRequest
		Private _dt As DataTable
		Private _dtAdjun As DataTable
		Private _dtDesglose As DataTable
		Private _dtDesgloseAdjun As DataTable
		Private _dtCertif As DataTable
		Private _dtNoConf As DataTable
		Private _dtNoConfAccion As DataTable
		Private _dtNoConfEstadosAccion As DataTable
		Private _dtPMParticipantes As DataTable
		Private _dtContrato As DataTable
		Private _dtVincular As DataTable
		Private _dtMoverVincular As DataTable
		Private _dtDesgloseActividad As DataTable
		Private _sAccion As String
		Private _Usuario As FSNServer.User
		Private _bEsSolicitud As Boolean
		Private _iTipoSolicitud As Integer
		Private _Session As HttpSessionState
		Private _FSNServer As FSNServer.Root
		Private _lSolicitud As Long
		Private _lInstancia As Long

		Public Sub New(ByRef Session As HttpSessionState, ByRef FSNServer As FSNServer.Root, ByVal Request As System.Web.HttpRequest, ByVal sAccion As String, ByVal lSolicitud As Long, ByVal lInstancia As Long,
					   ByVal Usuario As FSNServer.User, ByVal iTipoSolicitud As Integer)
			_lInstancia = lInstancia
			_bEsSolicitud = (lInstancia = 0)
			_Request = Request
			_Usuario = Usuario
			_sAccion = sAccion
			_DS = New DataSet
			_iTipoSolicitud = iTipoSolicitud
			_Session = Session
			_FSNServer = FSNServer
			_lSolicitud = lSolicitud

			NuevoDT()
			NuevoDTAdjun()
			NuevoDTDesglose()
			NuevoDTDesgloseAdjun()
			NuevoDTCertif()
			NuevoDTNoConf()
			NuevoDTNoConfAccion()
			NuevoDTNoConfEstadosAccion()
			NuevoDTPMParticipantes()
			NuevoDTContrato()
			NuevoDTVincular()
			NuevoDTMoverVincular()
			NuevoDTDesgloseActividad()
		End Sub

		''' <summary>Crear un dataset con N tablas temporales (N pq QA difiere de PM) q contenga todos los campos a grabar</summary>
		''' <param name="ArrayGruposPadre"></param>
		''' <param name="ArrayGrupos"></param>
		''' <param name="iNoConformidad_Certificado"></param>
		''' <param name="bAlgunaNoFinalRevisada"></param>  
		''' <returns>Dataset</returns>
		''' <remarks>Llamada desde:esta pantalla GuardarQA GenerarDataSetyHacerComprobaciones</remarks>
		Public Function ProcessRequest(ByRef ArrayGruposPadre() As Long, ByRef ArrayGrupos() As Long, ByRef iNoConformidad_Certificado As Integer, Optional ByRef bAlgunaNoFinalRevisada As Boolean = False) As DataSet
			Dim oItem As System.Collections.Specialized.NameValueCollection
			Dim loop1 As Integer
			Dim arr1() As String
			Dim sRequest As String
			Dim oRequest As Object
			Dim iTipo As TiposDeDatos.TipoGeneral
			Dim findDesgloseAct(0) As Object
			ReDim ArrayGruposPadre(0)
			ReDim ArrayGrupos(0)
			Dim find(0) As Object
			Dim findDesglose(2) As Object
			Dim findAcc(1) As Object
			Dim findDesgloseAdjun(1) As Object
			Dim findVinc(1) As Object

			oItem = _Request.Form
			arr1 = oItem.AllKeys
			Dim ContDesgActividad As Short = 0
			For loop1 = 0 To arr1.GetUpperBound(0)
				sRequest = arr1(loop1).ToString

				oRequest = sRequest.Split("_")

				If oRequest(0) = "CAMPO" OrElse oRequest(0) = "CAMPOID" OrElse oRequest(0) = "DESG" OrElse oRequest(0) = "DESGID" _
				OrElse oRequest(0) = "PART" OrElse oRequest(0) = "CAMPOCCD" OrElse oRequest(0) = "ADJUNCCD" _
				OrElse oRequest(0) = "DESGCCDHP" OrElse oRequest(0) = "DESGCCDPP" OrElse oRequest(0) = "DESGCCDPNP" _
				OrElse oRequest(0) = "DESGCCDHNP" OrElse oRequest(0) = "ADJUNDESGCCDHP" OrElse oRequest(0) = "ADJUNDESGCCDPP" _
				OrElse oRequest(0) = "ADJUNDESGCCDHNP" OrElse oRequest(0) = "ADJUNDESGCCDPNP" OrElse oRequest(0) = "ADJUNCOMENT" Then
					iTipo = oRequest(UBound(oRequest))
				End If

				Select Case oRequest(0)
					Case "PROVE"
						ProcesarCampoPROVE(oRequest, sRequest)
					Case "PART"
						ProcesarCampoPART(oRequest, sRequest)
					Case "GEN"
					Case "DESGACT" 'Desglose de Actividad
						ProcesarCampoDESGACT(oRequest, sRequest, ContDesgActividad)
					Case "DESGACTOBS"
						ProcesarCampoDESGACTOBS(oRequest, sRequest, ContDesgActividad, findDesgloseAct)
					Case "DESGACTPROVE"
						ProcesarCampoDESGACTPROVE(oRequest, sRequest, ContDesgActividad, findDesgloseAct)
					Case "DESGACTCODUSU"
						ProcesarCampoDESGACTCODUSU(oRequest, sRequest, ContDesgActividad, findDesgloseAct)
					Case "DESGACTTAREADEN"
						ProcesarCampoDESGACTTAREADEN(oRequest, sRequest, ContDesgActividad, findDesgloseAct)
					Case "DESGACTHORASGRADOAV"
						ProcesarCampoDESGACTHORASGRADOAV(oRequest, sRequest, ContDesgActividad, findDesgloseAct)
					Case "DESGACTNIV1"
						ProcesarCampoDESGACTNIV1(oRequest, sRequest, ContDesgActividad, findDesgloseAct)
					Case "DESGACTNIV2"
						ProcesarCampoDESGACTNIV2(oRequest, sRequest, ContDesgActividad, findDesgloseAct)
					Case "DESGACTNIV3"
						ProcesarCampoDESGACTNIV3(oRequest, sRequest, ContDesgActividad, findDesgloseAct)
					Case "DESGACTNIV4"
						ProcesarCampoDESGACTNIV4(oRequest, sRequest, ContDesgActividad, findDesgloseAct)
					Case "DESGACTFECHAS"
						ProcesarCampoDESGACTFECHAS(oRequest, sRequest, ContDesgActividad, findDesgloseAct)
					Case "DESGACTCODCAT"
						ProcesarCampoDESGACTCODCAT(oRequest, sRequest, ContDesgActividad, findDesgloseAct)
					Case "DESGACTCOSTE"
						ProcesarCampoDESGACTCOSTE(oRequest, sRequest, ContDesgActividad, findDesgloseAct)
					Case "DESGACTCODCON"
						ProcesarCampoDESGACTCODCON(oRequest, sRequest, ContDesgActividad, findDesgloseAct)
					Case "DMONREPER"
						ProcesarCampoDMONREPER(oRequest, sRequest, find, findDesglose)
					Case "DEQREPER"
						ProcesarCampoDEQREPER(oRequest, sRequest, find, findDesglose)
					Case "MONREPER"
						ProcesarCampoMONREPER(oRequest, sRequest, find)
					Case "EQREPER"
						ProcesarCampoEQREPER(oRequest, sRequest, find)
					Case "CAMPOSAVEVALUESDESG"
						ProcesarCampoCAMPOSAVEVALUESDESG(oRequest, sRequest, findDesglose)
					Case "CAMPO"
						ProcesarCampoCAMPO(oRequest, sRequest, find, iTipo)
					Case "CAMPOSAVEVALUES"
						ProcesarCampoCAMPOSAVEVALUES(oRequest, sRequest, find)
					Case "CAMPOCONTACTOS"
						ProcesarCampoCAMPOCONTACTOS(oRequest, sRequest, find)
					Case "CAMPOID"
						ProcesarCampoCAMPOID(oRequest, sRequest, find, iTipo)
					Case "CAMPOTIPOGS"
						ProcesarCampoCAMPOTIPOGS(oRequest, sRequest, find)
					Case "CAMPOCCD"
						ProcesarCampoCAMPOCCD(oRequest, sRequest, find)
					Case "DESG"
						ProcesarCampoDESG(oRequest, sRequest, find, findAcc, findDesglose, iTipo, bAlgunaNoFinalRevisada, ArrayGruposPadre, ArrayGrupos)
					Case "DESGID"
						ProcesarCampoDESGID(oRequest, sRequest, findDesglose, iTipo)
					Case "DESGTIPOGS"
						ProcesarCampoDESGTIPOGS(oRequest, sRequest, findDesglose, iTipo, ArrayGruposPadre, ArrayGrupos)
					Case "DESGCCDPP", "DESGCCDPNP"
						ProcesarCampoDESGCCDPP(oRequest, sRequest, findDesglose, ArrayGruposPadre, ArrayGrupos)
					Case "DESGCCDHP", "DESGCCDHNP"
						ProcesarCampoDESGCCDHP(oRequest, sRequest, findDesglose, ArrayGruposPadre, ArrayGrupos)
					Case "ADJUN"
						ProcesarCampoADJUN(oRequest, sRequest, find)
					Case "ADJUNCCD"
						ProcesarCampoADJUNCCD(oRequest, sRequest, find)
					Case "ADJUNCOMENT"
						ProcesarCampoADJUNCOMENT(oRequest, sRequest, find)
					Case "ADJUNDESG"
						ProcesarCampoADJUNDESG(oRequest, sRequest, findDesgloseAdjun)
					Case "ADJUNDESGCCDPP", "ADJUNDESGCCDPNP"
						ProcesarCampoADJUNDESGCCDPP(oRequest, sRequest, findDesgloseAdjun)
					Case "ADJUNDESGCCDHP", "ADJUNDESGCCDHNP"
						ProcesarCampoADJUNDESGCCDHP(oRequest, sRequest, findDesgloseAdjun)
					Case "ACCION"
						ProcesarCampoACCION(oRequest, sRequest, find)
					Case "DSOLVINC"
						ProcesarCampoDSOLVINC(oRequest, sRequest, find, findVinc)
					Case "MOVERLINEA"
						ProcesarCampoMOVERLINEA(oRequest, sRequest)
				End Select
			Next loop1

			'volvemos a recorrernos el request para actualizar las l�neas de desglose con la correspondiente l�nea en bd
			For loop1 = 0 To arr1.GetUpperBound(0)
				sRequest = arr1(loop1).ToString
				oRequest = sRequest.Split("_")

				Select Case oRequest(0)
					Case "CON"
						ProcesarCampoCON(oRequest, sRequest, iNoConformidad_Certificado)
					Case "LINDESG"
						ProcesarCampoLINDESG(oRequest, sRequest)
					Case "LINILV"
						ProcesarCampoLINILV(oRequest, sRequest)
					Case "LINOLDILV"
						ProcesarCampoLINOLDILV(oRequest, sRequest)
					Case "DESGCCDHP", "DESGCCDHNP"
						ProcesarCampoLINDESGCCDHP(oRequest, sRequest, find, findDesglose, ArrayGruposPadre, ArrayGrupos)
				End Select
			Next

			If _Request("DeDonde") = "CONTRATOS" Then ProcesarContrato()

			'Si estamos creando una No conformidad.....:
			If _Request("GEN_NoConfProve") <> Nothing Or _Request("GEN_NoConformidad") <> Nothing Then ProcesarNoConformidad(iNoConformidad_Certificado)

			'0 acciones quedamos en q todas estan sin finalizar y revisar
			If _dtNoConfEstadosAccion.Rows.Count = 0 Then bAlgunaNoFinalRevisada = True

			Return _DS
		End Function

		''' <summary>
		''' Devuelve desde bbdd o desde el array q contiene relacion campopadre-grupodesglose, el grupo en q esta un desglose 
		''' </summary>
		''' <param name="idCampoPadre">campo padre del desglose</param>
		''' <param name="bDesdeBd">desde bbdd o desde el array</param>
		''' <returns>el grupo en q esta un desglose </returns>
		''' <remarks>Llamada desde: GenerarDatset; Tiempo maximo: 0,1</remarks>
		Private Function DevolverGrupo(ByRef ArrayGruposPadre() As Long, ByRef ArrayGrupos() As Long, ByVal idCampoPadre As Integer, Optional ByVal bDesdeBd As Boolean = True) As String
			If bDesdeBd Then
				Dim FSPMServer As FSNServer.Root = _Session("FSN_Server")
				Dim lInstancia As Long = _Request("GEN_Instancia")

				Dim oCampo As FSNServer.Campo
				oCampo = _FSNServer.Get_Object(GetType(FSNServer.Campo))
				oCampo.Id = idCampoPadre

				oCampo.LoadInst(lInstancia, "SPA")

				Dim bEsta As Boolean = False
				For i As Integer = 0 To UBound(ArrayGruposPadre)
					If ArrayGruposPadre(i) = idCampoPadre Then
						bEsta = True
						Exit For
					End If
				Next
				If Not bEsta Then
					ReDim Preserve ArrayGruposPadre(UBound(ArrayGruposPadre) + 1)
					ArrayGruposPadre(UBound(ArrayGruposPadre)) = idCampoPadre
					ReDim Preserve ArrayGrupos(UBound(ArrayGrupos) + 1)
					ArrayGrupos(UBound(ArrayGrupos)) = oCampo.IdGrupo.ToString
				End If

				Return oCampo.IdGrupo.ToString
				oCampo = Nothing
			Else
				For i As Integer = 0 To UBound(ArrayGruposPadre)
					If ArrayGruposPadre(i) = idCampoPadre Then
						Return ArrayGrupos(i)
					End If
				Next
			End If
			Return String.Empty
		End Function

		Private Function CreaListaAdjuntosDefecto(ByVal CampoPadre As Long) As String
			Dim Indice As Integer
			Dim ListaDesgloseDefecto As String = String.Empty
			Dim ListaAdjuntosDefecto() As String
			ReDim ListaAdjuntosDefecto(0)

			Try
				CreaListaAdjuntosDefecto = "#D" & CStr(CampoPadre) & "##"
				If Strings.InStr(ListaDesgloseDefecto, CreaListaAdjuntosDefecto, CompareMethod.Text) <> 0 Then
					For i As Integer = 0 To UBound(ListaAdjuntosDefecto)
						If Strings.InStr(ListaAdjuntosDefecto(i), CreaListaAdjuntosDefecto, CompareMethod.Text) <> 0 Then
							Return ListaAdjuntosDefecto(i)
						End If
					Next
				Else
					ListaDesgloseDefecto = ListaDesgloseDefecto & CreaListaAdjuntosDefecto
					Indice = UBound(ListaAdjuntosDefecto) + 1
					ReDim Preserve ListaAdjuntosDefecto(Indice)
				End If

				Dim ds As DataSet = _FSNServer.Load_AdjuntosDefecto(_lSolicitud, CampoPadre)
				For Each row As DataRow In ds.Tables(0).Rows
					'Crea string #DIdDesgl##IdAdj##IdAdj##
					CreaListaAdjuntosDefecto = CreaListaAdjuntosDefecto & row(0) & "##"
				Next

				ListaAdjuntosDefecto(Indice) = CreaListaAdjuntosDefecto

				Return CreaListaAdjuntosDefecto
			Catch ex As Exception
				CreaListaAdjuntosDefecto = "#D" & CStr(CampoPadre) & "##"
			End Try
		End Function

		Private Sub NuevoDT()
			Dim keys(0) As DataColumn

			_dt = _DS.Tables.Add("TEMP")
			With _dt
				.Columns.Add("CC_ID", System.Type.GetType("System.Int32"))
				.Columns.Add("CCD_ID", System.Type.GetType("System.Int32"))
				.Columns.Add("CAMPO", System.Type.GetType("System.Int32"))
				.Columns.Add("VALOR_TEXT", System.Type.GetType("System.String"))
				.Columns.Add("VALOR_NUM", System.Type.GetType("System.Double"))
				.Columns.Add("VALOR_FEC", System.Type.GetType("System.DateTime"))
				.Columns.Add("VALOR_BOOL", System.Type.GetType("System.Int32"))
				.Columns.Add("ES_SUBCAMPO", System.Type.GetType("System.Int32"))
				.Columns.Add("SUBTIPO", System.Type.GetType("System.Int32"))
				.Columns.Add("TIPOGS", System.Type.GetType("System.Int32"))
				.Columns.Add("CAMBIO", System.Type.GetType("System.Double"))
				.Columns.Add("SAVE_VALUES", System.Type.GetType("System.Int32"))
				If Not _bEsSolicitud Then .Columns.Add("CAMPO_DEF", System.Type.GetType("System.Int32"))
				.Columns.Add("CONTACTOS", System.Type.GetType("System.String"))
				keys(0) = .Columns("CAMPO")
				.PrimaryKey = keys
			End With
		End Sub

		Private Sub NuevoDTAdjun()
			Dim keysAdjun(0) As DataColumn

			_dtAdjun = _DS.Tables.Add("TEMPADJUN")
			With _dtAdjun
				.Columns.Add("CC_ID", System.Type.GetType("System.Int32"))
				.Columns.Add("CAMPO", System.Type.GetType("System.Int32"))
				.Columns.Add("ID", System.Type.GetType("System.Int32"))
				.Columns.Add("TIPO", System.Type.GetType("System.Int32"))
				.Columns.Add("COMENT", System.Type.GetType("System.String"))
				If Not _bEsSolicitud Then .Columns.Add("CAMPO_DEF", System.Type.GetType("System.Int32"))
				keysAdjun(0) = _dtAdjun.Columns("ID")
				.PrimaryKey = keysAdjun
			End With
		End Sub

		Private Sub NuevoDTDesglose()
			Dim keysDesglose(2) As DataColumn

			_dtDesglose = _DS.Tables.Add("TEMPDESGLOSE")
			With _dtDesglose
				.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
				.Columns.Add("CC_ID_PADRE", System.Type.GetType("System.Int32"))
				.Columns.Add("CC_ID_HIJO", System.Type.GetType("System.Int32"))
				.Columns.Add("CAMPO_PADRE", System.Type.GetType("System.Int32"))
				.Columns.Add("CAMPO_HIJO", System.Type.GetType("System.Int32"))
				.Columns.Add("VALOR_TEXT", System.Type.GetType("System.String"))
				.Columns.Add("VALOR_NUM", System.Type.GetType("System.Double"))
				.Columns.Add("VALOR_FEC", System.Type.GetType("System.DateTime"))
				.Columns.Add("VALOR_BOOL", System.Type.GetType("System.Int32"))
				.Columns.Add("CAMBIO", System.Type.GetType("System.Double"))
				.Columns.Add("LINEA_OLD", System.Type.GetType("System.Int32"))
				.Columns.Add("LINEAOLD_ILV", System.Type.GetType("System.Int32"))
				.Columns.Add("LINEA_ILV", System.Type.GetType("System.Int32"))
				.Columns.Add("TIPOGS", System.Type.GetType("System.Int32"))
				.Columns.Add("SAVE_VALUES", System.Type.GetType("System.Int32"))
				If Not _bEsSolicitud Then
					.Columns.Add("CAMPO_PADRE_DEF", System.Type.GetType("System.Int32"))
					.Columns.Add("CAMPO_HIJO_DEF", System.Type.GetType("System.Int32"))
				End If
				.Columns.Add("ES_IMPORTE_TOTAL", System.Type.GetType("System.Int32"))
				.Columns("LINEA_OLD").AllowDBNull = True
				.Columns("LINEA_ILV").AllowDBNull = True
				.Columns("LINEAOLD_ILV").AllowDBNull = True
				keysDesglose(0) = .Columns("LINEA")
				keysDesglose(1) = .Columns("CAMPO_PADRE")
				keysDesglose(2) = .Columns("CAMPO_HIJO")
				.PrimaryKey = keysDesglose
			End With
		End Sub

		Private Sub NuevoDTDesgloseAdjun()
			Dim keysDesgloseAdjun(1) As DataColumn

			_dtDesgloseAdjun = _DS.Tables.Add("TEMPDESGLOSEADJUN")
			With _dtDesgloseAdjun
				.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
				.Columns.Add("CC_ID_PADRE", System.Type.GetType("System.Int32"))
				.Columns.Add("CC_ID_HIJO", System.Type.GetType("System.Int32"))
				.Columns.Add("CAMPO_PADRE", System.Type.GetType("System.Int32"))
				.Columns.Add("CAMPO_HIJO", System.Type.GetType("System.Int32"))
				.Columns.Add("ID", System.Type.GetType("System.Int32"))
				.Columns.Add("TIPO", System.Type.GetType("System.Int32"))
				If Not _bEsSolicitud Then
					.Columns.Add("CAMPO_PADRE_DEF", System.Type.GetType("System.Int32"))
					.Columns.Add("CAMPO_HIJO_DEF", System.Type.GetType("System.Int32"))
				End If
				keysDesgloseAdjun(0) = .Columns("ID")
				keysDesgloseAdjun(1) = .Columns("LINEA")
				.PrimaryKey = keysDesgloseAdjun
			End With
		End Sub

		Private Sub NuevoDTCertif()
			_dtCertif = _DS.Tables.Add("TEMPCERTIFICADO")
			With _dtCertif
				.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
				.Columns.Add("PROVE", System.Type.GetType("System.String"))
				.Columns.Add("CONTACTO", System.Type.GetType("System.Int32"))
				.Columns.Add("FEC_DESPUB", System.Type.GetType("System.DateTime"))
				.Columns.Add("FEC_LIM_CUMPLIM", System.Type.GetType("System.DateTime"))
				.Columns.Add("PETICIONARIO", System.Type.GetType("System.String"))
				.Columns("CONTACTO").AllowDBNull = True
			End With
		End Sub

		Private Sub NuevoDTNoConf()
			_dtNoConf = _DS.Tables.Add("TEMPNOCONFORMIDAD")
			With _dtNoConf
				.Columns.Add("ID", System.Type.GetType("System.Int32"))
				.Columns.Add("PROVE", System.Type.GetType("System.String"))
				.Columns.Add("PROVE_ERP", System.Type.GetType("System.String"))
				.Columns.Add("PROVE_ERP_DEN", System.Type.GetType("System.String"))
				.Columns.Add("CONTACTO", System.Type.GetType("System.Int32"))
				.Columns.Add("FEC_LIM_RESOL", System.Type.GetType("System.DateTime"))
				.Columns.Add("FEC_APLICACION_PLAN", System.Type.GetType("System.DateTime"))
				.Columns.Add("PETICIONARIO", System.Type.GetType("System.String"))
				.Columns.Add("REVISOR", System.Type.GetType("System.String"))
				.Columns.Add("UNIDADQA", System.Type.GetType("System.Int32"))
				.Columns("CONTACTO").AllowDBNull = True
				.Columns.Add("CAMBIOREVISOR", System.Type.GetType("System.Int32"))
			End With
		End Sub

		Private Sub NuevoDTNoConfAccion()
			Dim keys(0) As DataColumn

			_dtNoConfAccion = _DS.Tables.Add("TEMPNOCONF_SOLICIT_ACC")
			With _dtNoConfAccion
				.Columns.Add("CC_ID", System.Type.GetType("System.Int32"))
				.Columns.Add("COPIA_CAMPO", System.Type.GetType("System.Int32"))
				.Columns.Add("SOLICITAR", System.Type.GetType("System.Int32"))
				.Columns.Add("FECHA_LIMITE", System.Type.GetType("System.DateTime"))
				If Not _bEsSolicitud Then .Columns.Add("CAMPO_DEF", System.Type.GetType("System.Int32"))
				keys(0) = .Columns("COPIA_CAMPO")
				_dtNoConfAccion.PrimaryKey = keys
			End With
		End Sub

		Private Sub NuevoDTNoConfEstadosAccion()
			Dim keysAcc(1) As DataColumn

			_dtNoConfEstadosAccion = _DS.Tables.Add("TEMPNOCONF_ACC")
			With _dtNoConfEstadosAccion
				.Columns.Add("CC_ID", System.Type.GetType("System.Int32"))
				.Columns.Add("CAMPO_PADRE", System.Type.GetType("System.Int32"))
				.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
				.Columns.Add("ESTADO", System.Type.GetType("System.String"))
				.Columns.Add("ESTADO_INT", System.Type.GetType("System.Int32"))
				.Columns.Add("COMENT", System.Type.GetType("System.String"))
				.Columns.Add("LINEA_OLD", System.Type.GetType("System.Int32"))
				If Not _bEsSolicitud Then .Columns.Add("CAMPO_DEF", System.Type.GetType("System.Int32"))
				.Columns("LINEA_OLD").AllowDBNull = True
				keysAcc(0) = .Columns("CAMPO_PADRE")
				keysAcc(1) = .Columns("LINEA")
				.PrimaryKey = keysAcc
			End With
		End Sub

		Private Sub NuevoDTPMParticipantes()
			Dim keys(0) As DataColumn

			_dtPMParticipantes = _DS.Tables.Add("TEMP_PARTICIPANTES")
			With _dtPMParticipantes
				.Columns.Add("ROL", System.Type.GetType("System.Int32"))
				.Columns.Add("TIPO", System.Type.GetType("System.Int16"))
				.Columns.Add("PER", System.Type.GetType("System.String"))
				.Columns.Add("PROVE", System.Type.GetType("System.String"))
				.Columns.Add("CON", System.Type.GetType("System.String"))
				keys(0) = .Columns("ROL")
				.PrimaryKey = keys
			End With
		End Sub

		Private Sub NuevoDTContrato()
			_dtContrato = _DS.Tables.Add("CONTRATO")
			With _dtContrato
				.Columns.Add("INSTANCIA", System.Type.GetType("System.Int32"))
				.Columns.Add("NOMBRE", System.Type.GetType("System.String"))
				.Columns.Add("EMP", System.Type.GetType("System.Int32"))
				.Columns.Add("PROVE", System.Type.GetType("System.String"))
				.Columns.Add("CONTACTO", System.Type.GetType("System.Int16"))
				.Columns.Add("MON", System.Type.GetType("System.String"))
				.Columns.Add("FEC_INI", System.Type.GetType("System.DateTime"))
				.Columns.Add("FEC_FIN", System.Type.GetType("System.DateTime"))
				.Columns.Add("ALERTA", System.Type.GetType("System.Int16"))
				.Columns.Add("REPETIR_EMAIL", System.Type.GetType("System.Int16"))
				.Columns.Add("PERIODO_ALERTA", System.Type.GetType("System.Int16"))
				.Columns.Add("PERIODO_REPETIR_EMAIL", System.Type.GetType("System.Int16"))
				.Columns.Add("ALERTA_IMPORTE_DESDE", System.Type.GetType("System.Double"))
				.Columns.Add("ALERTA_IMPORTE_HASTA", System.Type.GetType("System.Double"))
				.Columns.Add("NOTIFICADOS", System.Type.GetType("System.String"))
				.Columns.Add("PROCESO", System.Type.GetType("System.String"))
				.Columns.Add("GRUPOS", System.Type.GetType("System.String"))
				.Columns.Add("ITEMS", System.Type.GetType("System.String"))
				.Columns.Add("PATH", System.Type.GetType("System.String"))
			End With
		End Sub

		Private Sub NuevoDTVincular()
			Dim keysVinc(1) As DataColumn

			_dtVincular = _DS.Tables.Add("VINCULAR")
			With _dtVincular
				.Columns.Add("CC_ID", System.Type.GetType("System.Int32"))
				.Columns.Add("DESGLOSEVINCULADA", System.Type.GetType("System.Int32"))
				.Columns.Add("LINEAVINCULADA", System.Type.GetType("System.Int16"))
				.Columns.Add("INSTANCIAORIGEN", System.Type.GetType("System.Int32"))
				.Columns.Add("DESGLOSEORIGEN", System.Type.GetType("System.Int32"))
				.Columns.Add("LINEAORIGEN", System.Type.GetType("System.Int16"))
				keysVinc(0) = .Columns("DESGLOSEVINCULADA")
				keysVinc(1) = .Columns("LINEAVINCULADA")
				.PrimaryKey = keysVinc
			End With
		End Sub

		Private Sub NuevoDTMoverVincular()
			_dtMoverVincular = _DS.Tables.Add("MOVERVINCULAR")
			With _dtMoverVincular
				.Columns.Add("DESGLOSE", System.Type.GetType("System.Int32"))
				.Columns.Add("LINEA_ILV", System.Type.GetType("System.Int16"))
				.Columns.Add("INSTANCIADESTINO", System.Type.GetType("System.Int32"))
			End With
		End Sub

		Private Sub NuevoDTDesgloseActividad()
			Dim keysDesgloseActividad(0) As DataColumn

			_dtDesgloseActividad = _DS.Tables.Add("DESG_ACTIVIDAD")
			With _dtDesgloseActividad
				.Columns.Add("ID", System.Type.GetType("System.Int32"))
				.Columns.Add("IDPROY", System.Type.GetType("System.Int32"))
				.Columns.Add("STATUS", System.Type.GetType("System.String"))
				.Columns.Add("PROVE", System.Type.GetType("System.String"))
				.Columns.Add("NUMLINEA", System.Type.GetType("System.Int16"))
				.Columns.Add("NUMLINEA_OLD", System.Type.GetType("System.Int16"))
				.Columns.Add("NUMFASES", System.Type.GetType("System.Int16"))
				.Columns.Add("CODTAREA", System.Type.GetType("System.String"))
				.Columns.Add("IDTAREA", System.Type.GetType("System.Int32"))
				.Columns.Add("DENTAREA", System.Type.GetType("System.String"))
				.Columns.Add("HORASEST", System.Type.GetType("System.Int16"))
				.Columns.Add("GRADOAVANCE", System.Type.GetType("System.Int16"))
				.Columns.Add("HITO", System.Type.GetType("System.Int16"))
				.Columns.Add("CODCONTACTOS", System.Type.GetType("System.String"))
				.Columns.Add("CODUSUARIOS", System.Type.GetType("System.String"))
				.Columns.Add("CODCATEGORIAS", System.Type.GetType("System.String"))
				.Columns.Add("COSTETAREA", System.Type.GetType("System.String"))
				.Columns.Add("IDNIV1", System.Type.GetType("System.Int16"))
				.Columns.Add("CODNIV1", System.Type.GetType("System.String"))
				.Columns.Add("DENNIV1", System.Type.GetType("System.String"))
				.Columns.Add("IDNIV2", System.Type.GetType("System.Int16"))
				.Columns.Add("CODNIV2", System.Type.GetType("System.String"))
				.Columns.Add("DENNIV2", System.Type.GetType("System.String"))
				.Columns.Add("IDNIV3", System.Type.GetType("System.Int16"))
				.Columns.Add("CODNIV3", System.Type.GetType("System.String"))
				.Columns.Add("DENNIV3", System.Type.GetType("System.String"))
				.Columns.Add("IDNIV4", System.Type.GetType("System.Int16"))
				.Columns.Add("CODNIV4", System.Type.GetType("System.String"))
				.Columns.Add("DENNIV4", System.Type.GetType("System.String"))
				.Columns.Add("FECEST_INICIO", System.Type.GetType("System.DateTime"))
				.Columns.Add("FECEST_FIN", System.Type.GetType("System.DateTime"))
				.Columns.Add("ESTADO", System.Type.GetType("System.Int16"))
				.Columns.Add("OBS", System.Type.GetType("System.String"))
				.Columns.Add("EMP", System.Type.GetType("System.Int16"))
				keysDesgloseActividad(0) = .Columns("ID")
				.PrimaryKey = keysDesgloseActividad
			End With
		End Sub

		Private Sub ProcesarCampoPROVE(ByRef oRequest As Object, ByVal sRequest As String)
			Dim dtNewRow As DataRow = _dtCertif.NewRow
			dtNewRow.Item("LINEA") = oRequest(1)
			dtNewRow.Item("PROVE") = _Request(sRequest)
			If _Request("GEN_DespubAnyo") = "null" Then
				dtNewRow.Item("FEC_DESPUB") = System.DBNull.Value
			Else
				Dim D As Date
				D = New Date(_Request("GEN_DespubAnyo"), _Request("GEN_DespubMes"), _Request("GEN_DespubDia"))
				dtNewRow.Item("FEC_DESPUB") = D
			End If

			If _Request("GEN_LimiteCumplimentacionAnyo") = "null" Then
				dtNewRow.Item("FEC_LIM_CUMPLIM") = System.DBNull.Value
			Else
				Dim D As Date
				D = New Date(_Request("GEN_LimiteCumplimentacionAnyo"), _Request("GEN_LimiteCumplimentacionMes"), _Request("GEN_LimiteCumplimentacionDia"))
				dtNewRow.Item("FEC_LIM_CUMPLIM") = D
			End If

			dtNewRow.Item("PETICIONARIO") = _Usuario.CodPersona
			_dtCertif.Rows.Add(dtNewRow)
		End Sub

		Private Sub ProcesarCampoPART(ByRef oRequest As Object, ByVal sRequest As String)
			Dim dtNewRow As DataRow = _dtPMParticipantes.NewRow
			dtNewRow.Item("ROL") = CInt(oRequest(2))
			Dim iTipoGS As TiposDeDatos.TipoCampoGS = CInt(oRequest(1))
			dtNewRow.Item("TIPO") = iTipoGS
			If iTipoGS = TiposDeDatos.TipoCampoGS.Persona Then
				dtNewRow.Item("PER") = strToDBNull(_Request(sRequest))
				dtNewRow.Item("PROVE") = System.DBNull.Value
				dtNewRow.Item("CON") = System.DBNull.Value
			Else
				Dim sAux As String = _Request(sRequest)
				Dim sProve As String
				Dim sCon As String
				If InStr(sAux, "###") > 0 Then
					sProve = Left(sAux, InStr(sAux, "###") - 1)
					sCon = Right(sAux, Len(sAux) - Len(sProve) - 3)
				Else
					sProve = ""
					sCon = ""
				End If
				dtNewRow.Item("PER") = System.DBNull.Value
				dtNewRow.Item("PROVE") = strToDBNull(sProve)
				dtNewRow.Item("CON") = strToDBNull(sCon)
			End If
			_dtPMParticipantes.Rows.Add(dtNewRow)
		End Sub

		Private Sub ProcesarCampoDESGACT(ByRef oRequest As Object, ByVal sRequest As String, ByRef ContDesgActividad As Short)
			Dim dtNewRow As DataRow = _dtDesgloseActividad.NewRow
			ContDesgActividad += 1
			dtNewRow.Item("ID") = ContDesgActividad
			dtNewRow.Item("NUMLINEA") = oRequest(1)
			dtNewRow.Item("CODTAREA") = strToDBNull(_Request(sRequest))
			dtNewRow.Item("STATUS") = oRequest(2)
			dtNewRow.Item("HITO") = CInt(oRequest(3))
			dtNewRow.Item("IDPROY") = CInt(oRequest(4))
			dtNewRow.Item("ESTADO") = IIf(CInt(oRequest(5)) = -1, System.DBNull.Value, CInt(oRequest(5)))
			dtNewRow.Item("NUMFASES") = CInt(oRequest(6))
			If oRequest(7) = "" Then
				dtNewRow.Item("IDTAREA") = 0
			Else
				dtNewRow.Item("IDTAREA") = CInt(oRequest(7))
			End If
			dtNewRow.Item("EMP") = CInt(oRequest(8))
			dtNewRow.Item("NUMLINEA_OLD") = CInt(oRequest(9))
			_dtDesgloseActividad.Rows.Add(dtNewRow)
		End Sub

		Private Sub ProcesarCampoDESGACTOBS(ByRef oRequest As Object, ByVal sRequest As String, ByRef ContDesgActividad As Short, ByRef findDesgloseAct As Object())
			findDesgloseAct(0) = ContDesgActividad
			Dim dtNewRow As DataRow = _dtDesgloseActividad.Rows.Find(findDesgloseAct)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
				dtNewRow.Item("NUMLINEA") = oRequest(1)
				dtNewRow.Item("OBS") = strToDBNull(_Request(sRequest))
				_dt.Rows.Add(dtNewRow)
			Else
				dtNewRow.Item("OBS") = strToDBNull(_Request(sRequest))
			End If
		End Sub

		Private Sub ProcesarCampoDESGACTPROVE(ByRef oRequest As Object, ByVal sRequest As String, ByRef ContDesgActividad As Short, ByRef findDesgloseAct As Object())
			findDesgloseAct(0) = ContDesgActividad 'oRequest(1)
			Dim dtNewRow As DataRow = _dtDesgloseActividad.Rows.Find(findDesgloseAct)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
				dtNewRow.Item("NUMLINEA") = oRequest(1)
				dtNewRow.Item("PROVE") = strToDBNull(_Request(sRequest))
				_dt.Rows.Add(dtNewRow)
			Else
				dtNewRow.Item("PROVE") = strToDBNull(_Request(sRequest))
			End If
		End Sub

		Private Sub ProcesarCampoDESGACTCODUSU(ByRef oRequest As Object, ByVal sRequest As String, ByRef ContDesgActividad As Short, ByRef findDesgloseAct As Object())
			findDesgloseAct(0) = ContDesgActividad 'oRequest(1)
			Dim dtNewRow As DataRow = _dtDesgloseActividad.Rows.Find(findDesgloseAct)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
				dtNewRow.Item("NUMLINEA") = oRequest(1)
				dtNewRow.Item("CODUSUARIOS") = strToDBNull(_Request(sRequest))
				_dt.Rows.Add(dtNewRow)
			Else
				dtNewRow.Item("CODUSUARIOS") = strToDBNull(_Request(sRequest))
			End If
		End Sub

		Private Sub ProcesarCampoDESGACTTAREADEN(ByRef oRequest As Object, ByVal sRequest As String, ByRef ContDesgActividad As Short, ByRef findDesgloseAct As Object())
			findDesgloseAct(0) = ContDesgActividad 'oRequest(1)
			Dim dtNewRow As DataRow = _dtDesgloseActividad.Rows.Find(findDesgloseAct)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
				dtNewRow.Item("NUMLINEA") = oRequest(1)
				dtNewRow.Item("DENTAREA") = DBNullToStr(_Request(sRequest))
				_dt.Rows.Add(dtNewRow)
			Else
				dtNewRow.Item("DENTAREA") = DBNullToStr(_Request(sRequest))
			End If
		End Sub

		Private Sub ProcesarCampoDESGACTHORASGRADOAV(ByRef oRequest As Object, ByVal sRequest As String, ByRef ContDesgActividad As Short, ByRef findDesgloseAct As Object())
			findDesgloseAct(0) = ContDesgActividad 'oRequest(1)
			Dim dtNewRow As DataRow = _dtDesgloseActividad.Rows.Find(findDesgloseAct)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
				dtNewRow.Item("NUMLINEA") = oRequest(1)
				dtNewRow.Item("HORASEST") = DBNullToInteger(oRequest(2))
				dtNewRow.Item("GRADOAVANCE") = DBNullToInteger(oRequest(3))
				_dt.Rows.Add(dtNewRow)
			Else
				dtNewRow.Item("HORASEST") = DBNullToInteger(oRequest(2))
				dtNewRow.Item("GRADOAVANCE") = DBNullToInteger(oRequest(3))
			End If
		End Sub

		Private Sub ProcesarCampoDESGACTNIV1(ByRef oRequest As Object, ByVal sRequest As String, ByRef ContDesgActividad As Short, ByRef findDesgloseAct As Object())
			findDesgloseAct(0) = ContDesgActividad 'oRequest(1)
			Dim dtNewRow As DataRow = _dtDesgloseActividad.Rows.Find(findDesgloseAct)
			Dim oCodNiv(1) As String
			oCodNiv = System.Text.RegularExpressions.Regex.Split(CStr(oRequest(3)), "[\s$][\s$][\s$]")
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
				dtNewRow.Item("NUMLINEA") = oRequest(1)
				dtNewRow.Item("IDNIV1") = DBNullToInteger(oCodNiv(0))
				dtNewRow.Item("CODNIV1") = DBNullToStr(oCodNiv(1).Replace("#@", " "))
				dtNewRow.Item("DENNIV1") = DBNullToStr(_Request(sRequest))
				_dt.Rows.Add(dtNewRow)
			Else
				dtNewRow.Item("IDNIV1") = DBNullToInteger(oCodNiv(0))
				dtNewRow.Item("CODNIV1") = DBNullToStr(oCodNiv(1).Replace("#@", " "))
				dtNewRow.Item("DENNIV1") = DBNullToStr(_Request(sRequest))
			End If
		End Sub

		Private Sub ProcesarCampoDESGACTNIV2(ByRef oRequest As Object, ByVal sRequest As String, ByRef ContDesgActividad As Short, ByRef findDesgloseAct As Object())
			findDesgloseAct(0) = ContDesgActividad 'oRequest(1)
			Dim dtNewRow As DataRow = _dtDesgloseActividad.Rows.Find(findDesgloseAct)
			Dim oCodNiv(1) As String
			oCodNiv = System.Text.RegularExpressions.Regex.Split(CStr(oRequest(3)), "[\s$][\s$][\s$]")
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
				dtNewRow.Item("NUMLINEA") = oRequest(1)
				dtNewRow.Item("IDNIV2") = DBNullToInteger(oCodNiv(0))
				dtNewRow.Item("CODNIV2") = DBNullToStr(oCodNiv(1).Replace("#@", " "))
				dtNewRow.Item("DENNIV2") = DBNullToStr(_Request(sRequest))
				_dt.Rows.Add(dtNewRow)
			Else
				dtNewRow.Item("IDNIV2") = DBNullToInteger(oCodNiv(0))
				dtNewRow.Item("CODNIV2") = DBNullToStr(oCodNiv(1).Replace("#@", " "))
				dtNewRow.Item("DENNIV2") = DBNullToStr(_Request(sRequest))
			End If
		End Sub

		Private Sub ProcesarCampoDESGACTNIV3(ByRef oRequest As Object, ByVal sRequest As String, ByRef ContDesgActividad As Short, ByRef findDesgloseAct As Object())
			findDesgloseAct(0) = ContDesgActividad 'oRequest(1)
			Dim oCodNiv(1) As String
			oCodNiv = System.Text.RegularExpressions.Regex.Split(CStr(oRequest(3)), "[\s$][\s$][\s$]")
			Dim dtNewRow As DataRow = _dtDesgloseActividad.Rows.Find(findDesgloseAct)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
				dtNewRow.Item("NUMLINEA") = oRequest(1)
				dtNewRow.Item("IDNIV3") = DBNullToInteger(oCodNiv(0))
				dtNewRow.Item("CODNIV3") = DBNullToStr(oCodNiv(1).Replace("#@", " "))
				dtNewRow.Item("DENNIV3") = DBNullToStr(_Request(sRequest))
				_dt.Rows.Add(dtNewRow)
			Else
				dtNewRow.Item("IDNIV3") = DBNullToInteger(oCodNiv(0))
				dtNewRow.Item("CODNIV3") = DBNullToStr(oCodNiv(1).Replace("#@", " "))
				dtNewRow.Item("DENNIV3") = DBNullToStr(_Request(sRequest))
			End If
		End Sub

		Private Sub ProcesarCampoDESGACTNIV4(ByRef oRequest As Object, ByVal sRequest As String, ByRef ContDesgActividad As Short, ByRef findDesgloseAct As Object())
			findDesgloseAct(0) = ContDesgActividad 'oRequest(1)
			Dim oCodNiv(1) As String
			oCodNiv = System.Text.RegularExpressions.Regex.Split(CStr(oRequest(3)), "[\s$][\s$][\s$]")
			Dim dtNewRow As DataRow = _dtDesgloseActividad.Rows.Find(findDesgloseAct)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
				dtNewRow.Item("NUMLINEA") = oRequest(1)
				dtNewRow.Item("IDNIV4") = DBNullToInteger(oCodNiv(0))
				dtNewRow.Item("CODNIV4") = DBNullToStr(oCodNiv(1).Replace("#@", " "))
				dtNewRow.Item("DENNIV4") = DBNullToStr(_Request(sRequest))
				_dt.Rows.Add(dtNewRow)
			Else
				dtNewRow.Item("IDNIV4") = DBNullToInteger(oCodNiv(0))
				dtNewRow.Item("CODNIV4") = DBNullToStr(oCodNiv(1).Replace("#@", " "))
				dtNewRow.Item("DENNIV4") = DBNullToStr(_Request(sRequest))
			End If
		End Sub

		Private Sub ProcesarCampoDESGACTFECHAS(ByRef oRequest As Object, ByVal sRequest As String, ByRef ContDesgActividad As Short, ByRef findDesgloseAct As Object())
			Dim supportedFormats() As String = New String() {"MM/dd/yyyy", "MM/dd/yy", "dd/MM/yyyy", "dd/MM/yy", "dd.MM.yy", "MM.dd.yy"}

			findDesgloseAct(0) = ContDesgActividad 'oRequest(1)
			Dim dtNewRow As DataRow = _dtDesgloseActividad.Rows.Find(findDesgloseAct)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
				dtNewRow.Item("NUMLINEA") = oRequest(1)
				If Not strToDBNull(oRequest(2)) Is System.DBNull.Value Then
					Dim fec As Date
					fec = DateTime.ParseExact(oRequest(2), supportedFormats, System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None)
					dtNewRow.Item("FECEST_INICIO") = fec
				Else
					dtNewRow.Item("FECEST_INICIO") = strToDBNull(oRequest(2))
				End If
				If Not strToDBNull(oRequest(3)) Is System.DBNull.Value Then
					Dim fec As Date
					fec = DateTime.ParseExact(oRequest(3), supportedFormats, System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None)
					dtNewRow.Item("FECEST_FIN") = fec
				Else
					dtNewRow.Item("FECEST_FIN") = strToDBNull(oRequest(3))
				End If
				_dt.Rows.Add(dtNewRow)
			Else
				If Not strToDBNull(oRequest(2)) Is System.DBNull.Value Then
					Dim fec As Date
					fec = DateTime.ParseExact(oRequest(2), supportedFormats, System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None)
					dtNewRow.Item("FECEST_INICIO") = fec
				Else
					dtNewRow.Item("FECEST_INICIO") = strToDBNull(oRequest(2))
				End If
				If Not strToDBNull(oRequest(3)) Is System.DBNull.Value Then
					Dim fec As Date
					fec = DateTime.ParseExact(oRequest(3), supportedFormats, System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None)
					dtNewRow.Item("FECEST_FIN") = fec
				Else
					dtNewRow.Item("FECEST_FIN") = strToDBNull(oRequest(3))
				End If
			End If
		End Sub

		Private Sub ProcesarCampoDESGACTCODCAT(ByRef oRequest As Object, ByVal sRequest As String, ByRef ContDesgActividad As Short, ByRef findDesgloseAct As Object())
			findDesgloseAct(0) = ContDesgActividad 'oRequest(1)
			Dim dtNewRow As DataRow = _dtDesgloseActividad.Rows.Find(findDesgloseAct)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
				dtNewRow.Item("NUMLINEA") = oRequest(1)
				dtNewRow.Item("CODCATEGORIAS") = DBNullToStr(_Request(sRequest))
				_dt.Rows.Add(dtNewRow)
			Else
				dtNewRow.Item("CODCATEGORIAS") = DBNullToStr(_Request(sRequest))
			End If
		End Sub

		Private Sub ProcesarCampoDESGACTCOSTE(ByRef oRequest As Object, ByVal sRequest As String, ByRef ContDesgActividad As Short, ByRef findDesgloseAct As Object())
			findDesgloseAct(0) = ContDesgActividad 'oRequest(1)
			Dim dtNewRow As DataRow = _dtDesgloseActividad.Rows.Find(findDesgloseAct)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
				dtNewRow.Item("NUMLINEA") = oRequest(1)
				dtNewRow.Item("COSTETAREA") = DBNullToStr(_Request(sRequest))
				_dt.Rows.Add(dtNewRow)
			Else
				dtNewRow.Item("COSTETAREA") = DBNullToStr(_Request(sRequest))
			End If
		End Sub

		Private Sub ProcesarCampoDESGACTCODCON(ByRef oRequest As Object, ByVal sRequest As String, ByRef ContDesgActividad As Short, ByRef findDesgloseAct As Object())
			findDesgloseAct(0) = ContDesgActividad 'oRequest(1)
			Dim dtNewRow As DataRow = _dtDesgloseActividad.Rows.Find(findDesgloseAct)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
				dtNewRow.Item("NUMLINEA") = oRequest(1)
				dtNewRow.Item("CODCONTACTOS") = strToDBNull(_Request(sRequest))
				_dt.Rows.Add(dtNewRow)
			Else
				dtNewRow.Item("CODCONTACTOS") = strToDBNull(_Request(sRequest))
			End If
		End Sub

		Private Sub ProcesarCampoDMONREPER(ByRef oRequest As Object, ByVal sRequest As String, ByRef find As Object(), ByRef findDesglose As Object())
			find(0) = oRequest(1)
			Dim dtNewRow As DataRow = _dt.Rows.Find(find)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
				dtNewRow.Item("CAMPO") = oRequest(1)
				dtNewRow.Item("ES_SUBCAMPO") = 0
				_dt.Rows.Add(dtNewRow)
			End If

			find(0) = oRequest(2)
			dtNewRow = _dt.Rows.Find(find)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
				dtNewRow.Item("CAMPO") = oRequest(2)
				dtNewRow.Item("ES_SUBCAMPO") = 1
				_dt.Rows.Add(dtNewRow)
			End If

			findDesglose(0) = oRequest(3)
			findDesglose(1) = oRequest(1)
			findDesglose(2) = oRequest(2)
			dtNewRow = _dtDesglose.Rows.Find(findDesglose)
			If dtNewRow Is Nothing Then
				dtNewRow = _dtDesglose.NewRow
			End If

			dtNewRow.Item("LINEA") = oRequest(3)
			dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
			dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

			dtNewRow.Item("VALOR_TEXT") = strToDBNull(_Request(sRequest))
		End Sub

		Private Sub ProcesarCampoDEQREPER(ByRef oRequest As Object, ByVal sRequest As String, ByRef find As Object(), ByRef findDesglose As Object())
			find(0) = oRequest(1)
			Dim dtNewRow As DataRow = _dt.Rows.Find(find)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
				dtNewRow.Item("CAMPO") = oRequest(1)
				dtNewRow.Item("ES_SUBCAMPO") = 0
				_dt.Rows.Add(dtNewRow)
			End If

			find(0) = oRequest(2)
			dtNewRow = _dt.Rows.Find(find)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
				dtNewRow.Item("CAMPO") = oRequest(2)
				dtNewRow.Item("ES_SUBCAMPO") = 1
				_dt.Rows.Add(dtNewRow)
			End If

			findDesglose(0) = oRequest(3)
			findDesglose(1) = oRequest(1)
			findDesglose(2) = oRequest(2)
			dtNewRow = _dtDesglose.Rows.Find(findDesglose)
			If dtNewRow Is Nothing Then
				dtNewRow = _dtDesglose.NewRow
			End If

			dtNewRow.Item("LINEA") = oRequest(3)
			dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
			dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

			dtNewRow.Item("CAMBIO") = Numero(_Request(sRequest), ".", ",")
		End Sub

		Private Sub ProcesarCampoMONREPER(ByRef oRequest As Object, ByVal sRequest As String, ByRef find As Object())
			find(0) = oRequest(1)
			Dim dtNewRow As DataRow = _dt.Rows.Find(find)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
			End If
			dtNewRow.Item("CAMPO") = oRequest(1)
			dtNewRow.Item("VALOR_TEXT") = strToDBNull(_Request(sRequest))
		End Sub

		Private Sub ProcesarCampoEQREPER(ByRef oRequest As Object, ByVal sRequest As String, ByRef find As Object())
			find(0) = oRequest(1)
			Dim dtNewRow As DataRow = _dt.Rows.Find(find)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
			End If
			dtNewRow.Item("CAMPO") = oRequest(1)
			dtNewRow.Item("CAMBIO") = Numero(_Request(sRequest), ".", ",")
		End Sub

		Private Sub ProcesarCampoCAMPOSAVEVALUESDESG(ByRef oRequest As Object, ByVal sRequest As String, ByRef findDesglose As Object())
			findDesglose(0) = oRequest(3)
			findDesglose(1) = oRequest(1)
			findDesglose(2) = oRequest(2)
			Dim dtNewRow As DataRow = _dtDesglose.Rows.Find(findDesglose)
			If dtNewRow Is Nothing Then
				dtNewRow = _dtDesglose.NewRow
			End If
			dtNewRow.Item("LINEA") = oRequest(3)
			dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
			dtNewRow.Item("CAMPO_HIJO") = oRequest(2)
			dtNewRow.Item("SAVE_VALUES") = IIf(UCase(_Request(sRequest)) = "FALSE", 0, 1)
			If dtNewRow.RowState = DataRowState.Detached Then
				_dtDesglose.Rows.Add(dtNewRow)
			End If
		End Sub

		Private Sub ProcesarCampoCAMPO(ByRef oRequest As Object, ByVal sRequest As String, ByRef find As Object(), ByVal iTipo As TiposDeDatos.TipoGeneral)
			'CAMPO_17046_7
			find(0) = oRequest(1)
			Dim dtNewRow As DataRow = _dt.Rows.Find(find)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
			End If

			dtNewRow.Item("CAMPO") = oRequest(1)
			'If Request(sRequest) <> Nothing Then
			If Not IsNothing(_Request(sRequest)) Then
				Select Case iTipo
					Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoArchivo, TiposDeDatos.TipoGeneral.TipoPresBajaLog, TiposDeDatos.TipoGeneral.TipoDepBajaLog
						dtNewRow.Item("VALOR_TEXT") = strToDBNull(_Request(sRequest))
					Case TiposDeDatos.TipoGeneral.TipoNumerico, TiposDeDatos.TipoGeneral.TipoDesglose 'Trata el tipo numerico y el desglose de actividad
						If _Request(sRequest) = "" Then
							dtNewRow.Item("VALOR_NUM") = System.DBNull.Value
						Else
							dtNewRow.Item("VALOR_NUM") = Numero(_Request(sRequest), ".", ",")
						End If
					Case TiposDeDatos.TipoGeneral.TipoBoolean
						If _Request(sRequest) = "" Then
							dtNewRow.Item("VALOR_BOOL") = System.DBNull.Value
						Else
							dtNewRow.Item("VALOR_BOOL") = Numero(_Request(sRequest))
						End If
					Case TiposDeDatos.TipoGeneral.TipoFecha
						Try
							If _Request(sRequest) <> Nothing Then
								dtNewRow.Item("VALOR_FEC") = DateToBDDate(_Request(sRequest))
							End If
						Catch ex As Exception
							If igDateToDate(_Request(sRequest)) <> Nothing Then
								dtNewRow.Item("VALOR_FEC") = igDateToDate(_Request(sRequest))
							End If
						End Try
					Case TiposDeDatos.TipoGeneral.TipoEditor
						dtNewRow.Item("VALOR_TEXT") = strToDBNull(_Request(sRequest))
				End Select
			End If
			dtNewRow.Item("SUBTIPO") = iTipo
			dtNewRow.Item("ES_SUBCAMPO") = 0
			If dtNewRow.RowState = DataRowState.Detached Then
				_dt.Rows.Add(dtNewRow)
			End If
		End Sub

		Private Sub ProcesarCampoCAMPOSAVEVALUES(ByRef oRequest As Object, ByVal sRequest As String, ByRef find As Object())
			find(0) = oRequest(1)
			Dim dtNewRow As DataRow = _dt.Rows.Find(find)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
			End If
			dtNewRow.Item("CAMPO") = oRequest(1)
			dtNewRow.Item("SAVE_VALUES") = IIf(UCase(_Request(sRequest)) = "FALSE", 0, 1)
			If dtNewRow.RowState = DataRowState.Detached Then
				_dt.Rows.Add(dtNewRow)
			End If
		End Sub

		Private Sub ProcesarCampoCAMPOCONTACTOS(ByRef oRequest As Object, ByVal sRequest As String, ByRef find As Object())
			find(0) = oRequest(1)
			Dim dtNewRow As DataRow = _dt.Rows.Find(find)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
			End If
			dtNewRow.Item("CAMPO") = oRequest(1)
			dtNewRow.Item("CONTACTOS") = _Request(sRequest)
			If dtNewRow.RowState = DataRowState.Detached Then
				_dt.Rows.Add(dtNewRow)
			End If
		End Sub

		Private Sub ProcesarCampoCAMPOID(ByRef oRequest As Object, ByVal sRequest As String, ByRef find As Object(), ByVal iTipo As TiposDeDatos.TipoGeneral)
			Dim dtNewRow As DataRow = Nothing

			'PARA LOS CAMPOS DE TIPO TEXTO Y INTRODUCCI�N LISTA HAY QUE METER EL INDICE DEL ELEMENTO SELECCIONADO EN VALOR_NUM
			If iTipo = TiposDeDatos.TipoGeneral.TipoString Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoCorto Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoLargo Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio Then
				find(0) = oRequest(1)
				dtNewRow = _dt.Rows.Find(find)
				If dtNewRow Is Nothing Then
					dtNewRow = _dt.NewRow
				End If

				dtNewRow.Item("CAMPO") = oRequest(1)

				Select Case iTipo
					Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoArchivo
						If _Request(sRequest) = "" Then
							'Numero("", ".", ",") da 0.0 aunque la funci�n devuelva realmente Nothing
							dtNewRow.Item("VALOR_NUM") = System.DBNull.Value
						Else
							dtNewRow.Item("VALOR_NUM") = Numero(_Request(sRequest), ".", ",")
						End If
				End Select
				dtNewRow.Item("ES_SUBCAMPO") = 0
			End If
			If dtNewRow.RowState = DataRowState.Detached Then
				_dt.Rows.Add(dtNewRow)
			End If
		End Sub

		Private Sub ProcesarCampoCAMPOTIPOGS(ByRef oRequest As Object, ByVal sRequest As String, ByRef find As Object())
			find(0) = oRequest(1)
			Dim dtNewRow As DataRow = _dt.Rows.Find(find)
			If dtNewRow Is Nothing Then
				dtNewRow = _dt.NewRow
			End If

			dtNewRow.Item("CAMPO") = oRequest(1)
			dtNewRow.Item("TIPOGS") = Numero(_Request(sRequest))

			If dtNewRow.RowState = DataRowState.Detached Then
				_dt.Rows.Add(dtNewRow)
			End If
		End Sub

		Private Sub ProcesarCampoCAMPOCCD(ByRef oRequest As Object, ByVal sRequest As String, ByRef find As Object())
			If Not _bEsSolicitud Then
				find(0) = oRequest(1)
				Dim dtNewRow As DataRow = _dt.Rows.Find(find)
				If dtNewRow Is Nothing Then
					dtNewRow = _dt.NewRow
				End If

				dtNewRow.Item("CAMPO") = oRequest(1)
				dtNewRow.Item("CAMPO_DEF") = Numero(_Request(sRequest))

				If dtNewRow.RowState = DataRowState.Detached Then
					_dt.Rows.Add(dtNewRow)
				End If
			End If
		End Sub

		Private Sub ProcesarCampoDESG(ByRef oRequest As Object, ByVal sRequest As String, ByRef find As Object(), ByRef findAcc As Object(), ByRef findDesglose As Object(), ByVal iTipo As TiposDeDatos.TipoGeneral,
									  ByRef bAlgunaNoFinalRevisada As Boolean, ByRef ArrayGruposPadre() As Long, ByRef ArrayGrupos() As Long)
			Dim dtNewRow As DataRow = Nothing

			'DESG_17048_17058_3_6
			Dim sGrupo As String = String.Empty
			If {"guardarnoconformidad", "guardarsolicitud", ""}.Contains(_sAccion) _
				AndAlso _iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad _
				AndAlso _lInstancia > 0 AndAlso _lSolicitud = 0 Then _
				sGrupo = DevolverGrupo(ArrayGruposPadre, ArrayGrupos, oRequest(1))

			Dim sAux As String = Replace(oRequest(2), sGrupo, "")
			If {"guardarnoconformidad", "guardarsolicitud", ""}.Contains(_sAccion) _
				AndAlso _iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad _
				AndAlso {CInt(TiposDeDatos.IdsFicticios.EstadoActual).ToString,
							CInt(TiposDeDatos.IdsFicticios.EstadoInterno).ToString,
							CInt(TiposDeDatos.IdsFicticios.Comentario).ToString}.Contains(sAux) Then
				oRequest(2) = sAux
				findAcc(0) = oRequest(1)
				findAcc(1) = oRequest(3)
				dtNewRow = _dtNoConfEstadosAccion.Rows.Find(findAcc)
				If dtNewRow Is Nothing Then
					dtNewRow = _dtNoConfEstadosAccion.NewRow
					dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
					dtNewRow.Item("LINEA") = oRequest(3)
				End If
				Select Case oRequest(2)
					Case TiposDeDatos.IdsFicticios.EstadoActual
						dtNewRow.Item("CAMPO_PADRE") = oRequest(4)
						dtNewRow.Item("LINEA") = oRequest(3)
						dtNewRow.Item("ESTADO") = _Request(sRequest) 'Directamente estamos recuperando el c�digo por que de primeras se mete el c�digo, es solo cuando seleccionaas de la lista cuando se le mete el �ndice.                                
					Case TiposDeDatos.IdsFicticios.EstadoInterno
						If Numero(_Request(sRequest), ".", ",") <> 0 Then
							dtNewRow.Item("ESTADO_INT") = Numero(_Request(sRequest), ".", ",")
							If dtNewRow.Item("ESTADO_INT") <> TipoEstadoAcciones.FinalizadaRevisada Then
								bAlgunaNoFinalRevisada = True
							End If
						Else
							dtNewRow.Item("ESTADO_INT") = 1
							bAlgunaNoFinalRevisada = True
						End If
					Case TiposDeDatos.IdsFicticios.Comentario
						dtNewRow.Item("COMENT") = _Request(sRequest)
				End Select
				If dtNewRow.RowState = DataRowState.Detached Then
					_dtNoConfEstadosAccion.Rows.Add(dtNewRow)
				End If
			Else
				'emitirnoconformidad   
				If sAux = CInt(TiposDeDatos.IdsFicticios.EstadoActual).ToString Then
					oRequest(2) = sAux
					findAcc(0) = oRequest(1)
					findAcc(1) = oRequest(3)
					dtNewRow = _dtNoConfEstadosAccion.Rows.Find(findAcc)
					If dtNewRow Is Nothing Then
						dtNewRow = _dtNoConfEstadosAccion.NewRow
						dtNewRow.Item("CAMPO_PADRE") = oRequest(4)
						dtNewRow.Item("LINEA") = oRequest(3)
					End If

					dtNewRow.Item("ESTADO") = _Request(sRequest) 'Directamente estamos recuperando el c�digo por que de primeras se mete el c�digo, es solo cuando seleccionaas de la lista cuando se le mete el �ndice.
					dtNewRow.Item("ESTADO_INT") = 1 'SIN REVISAR                            
					bAlgunaNoFinalRevisada = True

					If dtNewRow.RowState = DataRowState.Detached Then
						_dtNoConfEstadosAccion.Rows.Add(dtNewRow)
					End If
				Else
					find(0) = oRequest(1)
					dtNewRow = _dt.Rows.Find(find)
					If dtNewRow Is Nothing Then
						dtNewRow = _dt.NewRow
						dtNewRow.Item("CAMPO") = oRequest(1)
						dtNewRow.Item("ES_SUBCAMPO") = 0
						dtNewRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoDesglose
						_dt.Rows.Add(dtNewRow)
					End If

					find(0) = oRequest(2)
					dtNewRow = _dt.Rows.Find(find)
					If dtNewRow Is Nothing Then
						dtNewRow = _dt.NewRow
						dtNewRow.Item("CAMPO") = oRequest(2)
						dtNewRow.Item("ES_SUBCAMPO") = 1
						_dt.Rows.Add(dtNewRow)
					End If

					findDesglose(0) = oRequest(3)
					findDesglose(1) = oRequest(1)
					findDesglose(2) = oRequest(2)
					dtNewRow = _dtDesglose.Rows.Find(findDesglose)
					If dtNewRow Is Nothing Then
						dtNewRow = _dtDesglose.NewRow
					End If

					dtNewRow.Item("LINEA") = oRequest(3)
					dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
					dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

					If _Request(sRequest) <> Nothing Then
						Select Case iTipo
							Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoArchivo
								If _Request(sRequest) = "nulo" Then
									dtNewRow.Item("VALOR_TEXT") = System.DBNull.Value
								Else
									dtNewRow.Item("VALOR_TEXT") = _Request(sRequest)
								End If
							Case TiposDeDatos.TipoGeneral.TipoNumerico
								If _Request(sRequest) = "" Then
									dtNewRow.Item("VALOR_NUM") = System.DBNull.Value
								Else
									dtNewRow.Item("VALOR_NUM") = Numero(_Request(sRequest), ".", ",")
								End If
							Case TiposDeDatos.TipoGeneral.TipoBoolean
								If _Request(sRequest) = "" Then
									dtNewRow.Item("VALOR_BOOL") = System.DBNull.Value
								Else
									dtNewRow.Item("VALOR_BOOL") = Numero(_Request(sRequest))
								End If
							Case TiposDeDatos.TipoGeneral.TipoFecha
								Try
									If _Request(sRequest) <> Nothing Then
										dtNewRow.Item("VALOR_FEC") = DateToBDDate(_Request(sRequest))
									End If
								Catch ex As Exception
									If igDateToDate(_Request(sRequest)) <> Nothing Then
										dtNewRow.Item("VALOR_FEC") = igDateToDate(_Request(sRequest))
									End If
								End Try
							Case TiposDeDatos.TipoGeneral.TipoEditor
								dtNewRow.Item("VALOR_TEXT") = strToDBNull(_Request(sRequest))
						End Select
					End If
					If dtNewRow.RowState = DataRowState.Detached Then
						_dtDesglose.Rows.Add(dtNewRow)
					End If
				End If
			End If
		End Sub

		Private Sub ProcesarCampoDESGID(ByRef oRequest As Object, ByVal sRequest As String, ByRef findDesglose As Object(), ByVal iTipo As TiposDeDatos.TipoGeneral)
			'DESGID_17048_17053_2_6:
			If iTipo = TiposDeDatos.TipoGeneral.TipoString Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoCorto Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoLargo Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio Then
				findDesglose(0) = oRequest(3)
				findDesglose(1) = oRequest(1)
				findDesglose(2) = oRequest(2)
				Dim dtNewRow As DataRow = _dtDesglose.Rows.Find(findDesglose)
				If dtNewRow Is Nothing Then
					dtNewRow = _dtDesglose.NewRow
				End If

				dtNewRow.Item("LINEA") = oRequest(3)
				dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
				dtNewRow.Item("CAMPO_HIJO") = oRequest(2)
				Select Case iTipo
					Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoArchivo
						dtNewRow.Item("VALOR_NUM") = IIf(_Request(sRequest) = "", System.DBNull.Value, Numero(_Request(sRequest), ".", ","))
				End Select
				If dtNewRow.RowState = DataRowState.Detached Then
					_dtDesglose.Rows.Add(dtNewRow)
				End If
			End If
		End Sub

		Private Sub ProcesarCampoDESGTIPOGS(ByRef oRequest As Object, ByVal sRequest As String, ByRef findDesglose As Object(), ByVal iTipo As TiposDeDatos.TipoGeneral, ByRef ArrayGruposPadre() As Long, ByRef ArrayGrupos() As Long)
			Dim sAuxRequest2 As String = oRequest(2).ToString
			sAuxRequest2 = Replace(oRequest(2), DevolverGrupo(ArrayGruposPadre, ArrayGrupos, oRequest(1), False), "")

			If {"guardarnoconformidad", "guardarsolicitud", ""}.Contains(_sAccion) _
				AndAlso _iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad _
				AndAlso {CInt(TiposDeDatos.IdsFicticios.EstadoActual).ToString,
							CInt(TiposDeDatos.IdsFicticios.EstadoInterno).ToString,
							CInt(TiposDeDatos.IdsFicticios.Comentario).ToString}.Contains(sAuxRequest2) Then
				oRequest(2) = sAuxRequest2
			Else
				'emitirnoconformidad   
				If sAuxRequest2 = CInt(TiposDeDatos.IdsFicticios.EstadoActual).ToString Then
					oRequest(2) = sAuxRequest2
				End If
			End If

			findDesglose(0) = oRequest(3)
			findDesglose(1) = oRequest(1)
			findDesglose(2) = oRequest(2)
			Dim dtNewRow As DataRow = _dtDesglose.Rows.Find(findDesglose)
			If dtNewRow Is Nothing Then
				dtNewRow = _dtDesglose.NewRow
			End If
			dtNewRow.Item("LINEA") = oRequest(3)
			dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
			dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

			dtNewRow.Item("TIPOGS") = Numero(_Request(sRequest))

			If dtNewRow.RowState = DataRowState.Detached Then
				_dtDesglose.Rows.Add(dtNewRow)
			End If
		End Sub

		Private Sub ProcesarCampoDESGCCDPP(ByRef oRequest As Object, ByVal sRequest As String, ByRef findDesglose As Object(), ByRef ArrayGruposPadre() As Long, ByRef ArrayGrupos() As Long)
			If Not _bEsSolicitud Then
				Dim sAuxRequest2 As String = oRequest(2).ToString
				sAuxRequest2 = Replace(oRequest(2), DevolverGrupo(ArrayGruposPadre, ArrayGrupos, oRequest(1), False), "")

				If {"guardarnoconformidad", "guardarsolicitud", ""}.Contains(_sAccion) _
					AndAlso _iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad _
					AndAlso {CInt(TiposDeDatos.IdsFicticios.EstadoActual).ToString,
							CInt(TiposDeDatos.IdsFicticios.EstadoInterno).ToString,
							CInt(TiposDeDatos.IdsFicticios.Comentario).ToString}.Contains(sAuxRequest2) Then
					oRequest(2) = sAuxRequest2
				Else
					'emitirnoconformidad   
					If sAuxRequest2 = CInt(TiposDeDatos.IdsFicticios.EstadoActual).ToString Then
						oRequest(2) = sAuxRequest2
					End If
				End If

				findDesglose(0) = oRequest(3)
				findDesglose(1) = oRequest(1)
				findDesglose(2) = oRequest(2)
				Dim dtNewRow As DataRow = _dtDesglose.Rows.Find(findDesglose)
				If dtNewRow Is Nothing Then
					dtNewRow = _dtDesglose.NewRow
				End If
				dtNewRow.Item("LINEA") = oRequest(3)
				dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
				dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

				dtNewRow.Item("CAMPO_PADRE_DEF") = Numero(_Request(sRequest))

				If dtNewRow.RowState = DataRowState.Detached Then
					_dtDesglose.Rows.Add(dtNewRow)
				End If
			End If
		End Sub

		Private Sub ProcesarCampoDESGCCDHP(ByRef oRequest As Object, ByVal sRequest As String, ByRef findDesglose As Object(), ByRef ArrayGruposPadre() As Long, ByRef ArrayGrupos() As Long)
			If Not _bEsSolicitud Then
				Dim sAuxRequest2 As String = oRequest(2).ToString
				sAuxRequest2 = Replace(oRequest(2), DevolverGrupo(ArrayGruposPadre, ArrayGrupos, oRequest(1), False), "")

				If {"guardarnoconformidad", "guardarsolicitud", ""}.Contains(_sAccion) _
					AndAlso _iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad _
					AndAlso {CInt(TiposDeDatos.IdsFicticios.EstadoActual).ToString,
							CInt(TiposDeDatos.IdsFicticios.EstadoInterno).ToString,
							CInt(TiposDeDatos.IdsFicticios.Comentario).ToString}.Contains(sAuxRequest2) Then
					oRequest(2) = sAuxRequest2
				Else
					'emitirnoconformidad   
					If sAuxRequest2 = CInt(TiposDeDatos.IdsFicticios.EstadoActual).ToString Then
						oRequest(2) = sAuxRequest2
					End If
				End If

				findDesglose(0) = oRequest(3)
				findDesglose(1) = oRequest(1)
				findDesglose(2) = oRequest(2)
				Dim dtNewRow As DataRow = _dtDesglose.Rows.Find(findDesglose)
				If dtNewRow Is Nothing Then
					dtNewRow = _dtDesglose.NewRow
				End If
				dtNewRow.Item("LINEA") = oRequest(3)
				dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
				dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

				dtNewRow.Item("CAMPO_HIJO_DEF") = Numero(_Request(sRequest))

				If dtNewRow.RowState = DataRowState.Detached Then
					_dtDesglose.Rows.Add(dtNewRow)
				End If
			End If
		End Sub

		Private Sub ProcesarCampoADJUN(ByRef oRequest As Object, ByVal sRequest As String, ByRef find As Object())
			'ADJUN_17063_2
			If _Request(sRequest) <> Nothing Then
				find(0) = Numero(_Request(sRequest))
				Dim dtNewRow As DataRow = _dtAdjun.Rows.Find(find)
				If dtNewRow Is Nothing Then
					dtNewRow = _dtAdjun.NewRow
				End If

				dtNewRow.Item("CAMPO") = oRequest(1)
				dtNewRow.Item("TIPO") = oRequest(2)
				dtNewRow.Item("ID") = Numero(_Request(sRequest))

				If dtNewRow.RowState = DataRowState.Detached Then
					_dtAdjun.Rows.Add(dtNewRow)
				End If
			End If
		End Sub

		Private Sub ProcesarCampoADJUNCCD(ByRef oRequest As Object, ByVal sRequest As String, ByRef find As Object())
			If Not _bEsSolicitud Then
				find(0) = oRequest(3)
				Dim dtNewRow As DataRow = _dtAdjun.Rows.Find(find)
				If dtNewRow Is Nothing Then
					dtNewRow = _dtAdjun.NewRow
				End If

				dtNewRow.Item("CAMPO") = oRequest(1)
				dtNewRow.Item("TIPO") = oRequest(2)
				dtNewRow.Item("ID") = oRequest(3)
				dtNewRow.Item("CAMPO_DEF") = Numero(_Request(sRequest))

				If dtNewRow.RowState = DataRowState.Detached Then
					_dtAdjun.Rows.Add(dtNewRow)
				End If
			End If
		End Sub

		Private Sub ProcesarCampoADJUNCOMENT(ByRef oRequest As Object, ByVal sRequest As String, ByRef find As Object())
			If _Request(sRequest) <> Nothing Then
				find(0) = oRequest(3)
				Dim dtNewRow As DataRow = _dtAdjun.Rows.Find(find)
				If dtNewRow Is Nothing Then
					dtNewRow = _dtAdjun.NewRow
					dtNewRow.Item("ID") = oRequest(3)
				End If
				dtNewRow.Item("COMENT") = _Request(sRequest)

				If dtNewRow.RowState = DataRowState.Detached Then
					_dtAdjun.Rows.Add(dtNewRow)
				End If
			End If
		End Sub

		Private Sub ProcesarCampoADJUNDESG(ByRef oRequest As Object, ByVal sRequest As String, ByRef findDesgloseAdjun As Object())
			'ADJUNDESG_17064_17079_1_2
			If _Request(sRequest) <> Nothing Then
				findDesgloseAdjun(0) = Numero(_Request(sRequest))
				findDesgloseAdjun(1) = oRequest(3)
				Dim dtNewRow As DataRow = _dtDesgloseAdjun.Rows.Find(findDesgloseAdjun)
				If dtNewRow Is Nothing Then
					dtNewRow = _dtDesgloseAdjun.NewRow
				End If
				dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
				dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

				dtNewRow.Item("LINEA") = oRequest(3)
				Dim Lista As String = CreaListaAdjuntosDefecto(oRequest(1))

				If Strings.InStr(Lista, "##" & CStr(_Request(sRequest)) & "##", CompareMethod.Text) <> 0 Then
					'Los nuevos son tipo 1. �ya se ha creado en bbdd el registro tabla ADJUN?
					'Y si me creo tipo 3 desde aqui y 
					'que el stored cree en ADJUN y update 163## por 12345##
					dtNewRow.Item("TIPO") = 3
				Else
					dtNewRow.Item("TIPO") = oRequest(4)
				End If
				dtNewRow.Item("ID") = Numero(_Request(sRequest))

				If dtNewRow.RowState = DataRowState.Detached Then
					_dtDesgloseAdjun.Rows.Add(dtNewRow)
				End If
			End If
		End Sub

		Private Sub ProcesarCampoADJUNDESGCCDPP(ByRef oRequest As Object, ByVal sRequest As String, ByRef findDesgloseAdjun As Object())
			If Not _bEsSolicitud Then
				findDesgloseAdjun(0) = oRequest(5)
				findDesgloseAdjun(1) = oRequest(3)
				Dim dtNewRow As DataRow = _dtDesgloseAdjun.Rows.Find(findDesgloseAdjun)
				If dtNewRow Is Nothing Then
					dtNewRow = _dtDesgloseAdjun.NewRow
				End If
				dtNewRow.Item("LINEA") = oRequest(3)
				dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
				dtNewRow.Item("CAMPO_HIJO") = oRequest(2)
				'TIPO lo hace Case "ADJUNDESG"
				dtNewRow.Item("ID") = oRequest(5)

				dtNewRow.Item("CAMPO_PADRE_DEF") = Numero(_Request(sRequest))

				If dtNewRow.RowState = DataRowState.Detached Then
					_dtDesgloseAdjun.Rows.Add(dtNewRow)
				End If
			End If
		End Sub

		Private Sub ProcesarCampoADJUNDESGCCDHP(ByRef oRequest As Object, ByVal sRequest As String, ByRef findDesgloseAdjun As Object())
			If Not _bEsSolicitud Then
				findDesgloseAdjun(0) = oRequest(5)
				findDesgloseAdjun(1) = oRequest(3)
				Dim dtNewRow As DataRow = _dtDesgloseAdjun.Rows.Find(findDesgloseAdjun)
				If dtNewRow Is Nothing Then
					dtNewRow = _dtDesgloseAdjun.NewRow
				End If
				dtNewRow.Item("LINEA") = oRequest(3)
				dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
				dtNewRow.Item("CAMPO_HIJO") = oRequest(2)
				'TIPO lo hace Case "ADJUNDESG"
				dtNewRow.Item("ID") = oRequest(5)

				dtNewRow.Item("CAMPO_HIJO_DEF") = Numero(_Request(sRequest))

				If dtNewRow.RowState = DataRowState.Detached Then
					_dtDesgloseAdjun.Rows.Add(dtNewRow)
				End If
			End If
		End Sub

		Private Sub ProcesarCampoACCION(ByRef oRequest As Object, ByVal sRequest As String, ByRef find As Object())
			find(0) = oRequest(2)
			Dim dtNewRow As DataRow = _dtNoConfAccion.Rows.Find(find)
			If dtNewRow Is Nothing Then
				dtNewRow = _dtNoConfAccion.NewRow
			End If

			dtNewRow.Item("COPIA_CAMPO") = oRequest(2)
			'If Request(sRequest) <> Nothing Then
			If oRequest(1) = "NoConfFECLIM" Then
				If igDateToDate(_Request(sRequest)) <> Nothing Then
					dtNewRow.Item("FECHA_LIMITE") = igDateToDate(_Request(sRequest))
				End If
			Else
				If _Request(sRequest) = True Then
					dtNewRow.Item("SOLICITAR") = 1
				Else
					dtNewRow.Item("SOLICITAR") = 0
				End If
			End If
			If Not _bEsSolicitud Then dtNewRow.Item("CAMPO_DEF") = oRequest(4)
			If dtNewRow.RowState = DataRowState.Detached Then
				_dtNoConfAccion.Rows.Add(dtNewRow)
			End If
		End Sub

		Private Sub ProcesarCampoDSOLVINC(ByRef oRequest As Object, ByVal sRequest As String, ByRef find As Object(), ByRef findVinc As Object())
			If _Request(sRequest) <> Nothing Then
				find(0) = oRequest(1)
				Dim dtNewRow As DataRow = _dt.Rows.Find(find)
				If dtNewRow Is Nothing Then
					dtNewRow = _dt.NewRow
					dtNewRow.Item("CAMPO") = oRequest(1)
					dtNewRow.Item("ES_SUBCAMPO") = 0
					dtNewRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoDesglose
					If _dt.Columns.IndexOf("CAMPO_DEF") <> -1 Then
						dtNewRow.Item("CAMPO_DEF") = oRequest(5)
					End If
					_dt.Rows.Add(dtNewRow)
				End If
				'Si es el campo padre del desglose vinculado no grabaremos la linea que en caso de que exista se grabara a continuacion
				If Not oRequest(3) = "0" Then
					oRequest(2) = CInt(TiposDeDatos.IdsFicticios.DesgloseVinculado).ToString
					findVinc(0) = oRequest(1)
					findVinc(1) = oRequest(3)
					dtNewRow = _dtVincular.Rows.Find(findVinc)
					If dtNewRow Is Nothing Then
						dtNewRow = _dtVincular.NewRow
						dtNewRow.Item("DESGLOSEVINCULADA") = oRequest(1)
						dtNewRow.Item("LINEAVINCULADA") = oRequest(3)
					End If
					Dim sOrigen As String() = Split(_Request(sRequest), "@")
					dtNewRow.Item("INSTANCIAORIGEN") = sOrigen(0)
					If UBound(sOrigen) > 0 Then
						dtNewRow.Item("DESGLOSEORIGEN") = sOrigen(1)
						dtNewRow.Item("LINEAORIGEN") = sOrigen(2)
					End If

					If dtNewRow.RowState = DataRowState.Detached Then
						_dtVincular.Rows.Add(dtNewRow)
					End If
				End If
			End If
		End Sub

		Private Sub ProcesarCampoMOVERLINEA(ByRef oRequest As Object, ByVal sRequest As String)
			Dim dtNewRow As DataRow = _dtMoverVincular.NewRow
			dtNewRow.Item("DESGLOSE") = oRequest(1)
			dtNewRow.Item("LINEA_ILV") = oRequest(2)
			dtNewRow.Item("INSTANCIADESTINO") = _Request(sRequest)

			If dtNewRow.RowState = DataRowState.Detached Then
				_dtMoverVincular.Rows.Add(dtNewRow)
			End If
		End Sub

		Private Sub ProcesarCampoCON(ByRef oRequest As Object, ByVal sRequest As String, ByRef iNoConformidad_Certificado As Integer)
			Dim dRowCertif() As DataRow
			iNoConformidad_Certificado = 2
			dRowCertif = _dtCertif.Select("LINEA= " + oRequest(1).ToString)
			Dim dtNewRow As DataRow = dRowCertif(0)
			If _Request(sRequest) = "" Then
				dtNewRow.Item("CONTACTO") = System.DBNull.Value
			Else
				dtNewRow.Item("CONTACTO") = Numero(_Request(sRequest))
			End If
		End Sub

		Private Sub ProcesarCampoLINDESG(ByRef oRequest As Object, ByVal sRequest As String)
			'LINDESG_17048_2: 2
			If _Request(sRequest) <> Nothing Then
				Dim oLineas As DataRow()

				oLineas = _dtDesglose.Select("LINEA =" & oRequest(2).ToString() & " AND CAMPO_PADRE = " & oRequest(1).ToString())
				For Each dtNewRow In oLineas
					dtNewRow.Item("LINEA_OLD") = Numero(_Request(sRequest))
				Next

				oLineas = _dtNoConfEstadosAccion.Select("LINEA =" & oRequest(2).ToString() & " AND CAMPO_PADRE = " & oRequest(1).ToString())
				For Each dtNewRow In oLineas
					dtNewRow.Item("LINEA_OLD") = Numero(_Request(sRequest))
				Next
			End If
		End Sub

		Private Sub ProcesarCampoLINILV(ByRef oRequest As Object, ByVal sRequest As String)
			If _Request(sRequest) <> Nothing Then
				Dim oLineas As DataRow()

				oLineas = _dtDesglose.Select("LINEA =" & oRequest(2).ToString() & " AND CAMPO_PADRE = " & oRequest(1).ToString())
				For Each dtNewRow In oLineas
					dtNewRow.Item("LINEA_ILV") = Numero(_Request(sRequest))
				Next
			End If
		End Sub

		Private Sub ProcesarCampoLINOLDILV(ByRef oRequest As Object, ByVal sRequest As String)
			If _Request(sRequest) <> Nothing Then
				Dim oLineas As DataRow()

				oLineas = _dtDesglose.Select("LINEA =" & oRequest(2).ToString() & " AND CAMPO_PADRE = " & oRequest(1).ToString())
				For Each dtNewRow In oLineas
					dtNewRow.Item("LINEAOLD_ILV") = Numero(_Request(sRequest))
				Next
			End If
		End Sub

		Private Sub ProcesarCampoLINDESGCCDHP(ByRef oRequest As Object, ByVal sRequest As String, ByRef find As Object(), ByRef findDesglose As Object(), ByRef ArrayGruposPadre() As Long, ByRef ArrayGrupos() As Long)
			Dim dtNewRow As DataRow = Nothing

			If Not _bEsSolicitud Then
				Dim sAuxRequest2 As String = oRequest(2).ToString
				sAuxRequest2 = Replace(oRequest(2), DevolverGrupo(ArrayGruposPadre, ArrayGrupos, oRequest(1), False), "")

				If {"guardarnoconformidad", "guardarsolicitud", ""}.Contains(_sAccion) _
					AndAlso _iTipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad _
					AndAlso {CInt(TiposDeDatos.IdsFicticios.EstadoActual).ToString,
							CInt(TiposDeDatos.IdsFicticios.EstadoInterno).ToString,
							CInt(TiposDeDatos.IdsFicticios.Comentario).ToString}.Contains(sAuxRequest2) Then
					findDesglose(0) = oRequest(3)
					findDesglose(1) = oRequest(1)
					findDesglose(2) = sAuxRequest2
					dtNewRow = _dtDesglose.Rows.Find(findDesglose)

					'Solo en, los siguientes casos
					'   TiposDeDatos.IdsFicticios.EstadoActual
					'   TiposDeDatos.IdsFicticios.EstadoInterno
					'   TiposDeDatos.IdsFicticios.Comentario
					'que no importa q no hagan nada en dt pq no son datos de copia_linea_desglose sino 
					'de noconf_acc o noconf_solict_acc
					Dim lPadre As Long = dtNewRow("CAMPO_PADRE_DEF")
					Dim oLineas As DataRow()
					oLineas = _dtNoConfEstadosAccion.Select("LINEA =" & oRequest(3).ToString() & " AND CAMPO_PADRE = " & oRequest(1).ToString())
					For Each dtNewRowAcc As DataRow In oLineas
						dtNewRowAcc.Item("CAMPO_DEF") = lPadre
					Next

					If sAuxRequest2 = CStr(TiposDeDatos.IdsFicticios.EstadoActual) Then
						dtNewRow.Item("CAMPO_HIJO_DEF") = CStr(TiposDeDatos.IdsFicticios.EstadoActual)
					End If

					find(0) = oRequest(1)
					dtNewRow = _dt.Rows.Find(find)
					dtNewRow.Item("CAMPO_DEF") = lPadre
				ElseIf sAuxRequest2 = CStr(TiposDeDatos.IdsFicticios.EstadoActual) Then
					findDesglose(0) = oRequest(3)
					findDesglose(1) = oRequest(1)
					findDesglose(2) = sAuxRequest2
					dtNewRow = _dtDesglose.Rows.Find(findDesglose)

					Dim lHijo As Long = dtNewRow("CAMPO_HIJO_DEF")
					Dim lPadre As Long = dtNewRow("CAMPO_PADRE_DEF")

					dtNewRow.Item("CAMPO_HIJO_DEF") = CStr(TiposDeDatos.IdsFicticios.EstadoActual)

					find(0) = oRequest(1)
					dtNewRow = _dt.Rows.Find(find)
					dtNewRow.Item("CAMPO_DEF") = lPadre
				Else
					findDesglose(0) = oRequest(3)
					findDesglose(1) = oRequest(1)
					findDesglose(2) = oRequest(2)
					dtNewRow = _dtDesglose.Rows.Find(findDesglose)

					Dim lHijo As Long = dtNewRow("CAMPO_HIJO_DEF")
					Dim lPadre As Long = dtNewRow("CAMPO_PADRE_DEF")

					find(0) = oRequest(2)
					dtNewRow = _dt.Rows.Find(find)
					dtNewRow.Item("CAMPO_DEF") = lHijo

					find(0) = oRequest(1)
					dtNewRow = _dt.Rows.Find(find)
					dtNewRow.Item("CAMPO_DEF") = lPadre
				End If
			End If
		End Sub

		Private Sub ProcesarContrato()
			'Desde el alta o el detalle de contrato
			Dim dtNewRow As DataRow = _dtContrato.NewRow
			dtNewRow.Item("NOMBRE") = _Request("NombreContrato")
			If String.IsNullOrEmpty(_Request("GEN_EmpresaID")) Then
				dtNewRow.Item("EMP") = 0
			Else
				dtNewRow.Item("EMP") = CDbl(_Request("GEN_EmpresaID"))
			End If

			dtNewRow.Item("PROVE") = _Request("GEN_PROVE")
			If _Request("GEN_ContactoID") <> "" Then
				dtNewRow.Item("CONTACTO") = _Request("GEN_ContactoID")
			End If
			dtNewRow.Item("MON") = _Request("GEN_CodMoneda")
			Dim sAux() As String
			sAux = Split(_Request("GEN_FechaIni"), "/")
			If UBound(sAux) > 0 Then
				Dim D As Date
				D = New Date(sAux(2), sAux(1), sAux(0))
				dtNewRow.Item("FEC_INI") = D
			End If
			sAux = Split(_Request("GEN_FechaFin"), "/")
			If UBound(sAux) > 0 Then
				Dim D As Date
				D = New Date(sAux(2), sAux(1), sAux(0))
				dtNewRow.Item("FEC_FIN") = D
			End If
			If String.IsNullOrEmpty(_Request("GEN_Alerta")) Then
				dtNewRow.Item("ALERTA") = 0
			Else
				dtNewRow.Item("ALERTA") = _Request("GEN_Alerta")
			End If
			If String.IsNullOrEmpty(_Request("GEN_RepetirEmail")) Then
				dtNewRow.Item("REPETIR_EMAIL") = 0
			Else
				dtNewRow.Item("REPETIR_EMAIL") = _Request("GEN_RepetirEmail")
			End If
			If String.IsNullOrEmpty(_Request("GEN_PeriodoAlerta")) Then
				dtNewRow.Item("PERIODO_ALERTA") = 0
			Else
				dtNewRow.Item("PERIODO_ALERTA") = _Request("GEN_PeriodoAlerta")
			End If
			If String.IsNullOrEmpty(_Request("GEN_PeriodoRepetirEmail")) Then
				dtNewRow.Item("PERIODO_REPETIR_EMAIL") = 0
			Else
				dtNewRow.Item("PERIODO_REPETIR_EMAIL") = _Request("GEN_PeriodoRepetirEmail")
			End If
			If _Request("GEN_AlertaImporteDesde") <> "" And _Request("GEN_AlertaImporteDesde") <> "null" Then
				dtNewRow.Item("ALERTA_IMPORTE_DESDE") = _Request("GEN_AlertaImporteDesde")
			End If
			If _Request("GEN_AlertaImporteHasta") <> "" And _Request("GEN_AlertaImporteHasta") <> "null" Then
				dtNewRow.Item("ALERTA_IMPORTE_HASTA") = _Request("GEN_AlertaImporteHasta")
			End If
			If _Request("GEN_Notificados") <> "" Then
				dtNewRow.Item("NOTIFICADOS") = _Request("GEN_Notificados")
			End If

			dtNewRow.Item("PROCESO") = _Request("ProcesoContrato")
			dtNewRow.Item("GRUPOS") = _Request("GruposContrato")
			dtNewRow.Item("ITEMS") = _Request("ItemsContrato")
			dtNewRow.Item("PATH") = _Request("PathContrato")

			_dtContrato.Rows.Add(dtNewRow)
		End Sub

		Private Sub ProcesarNoConformidad(ByRef iNoConformidad_Certificado As Integer)
			iNoConformidad_Certificado = 1
			Dim dtNewRow As DataRow = _dtNoConf.NewRow
			If _Request("GEN_NoConformidad") <> Nothing Then  'Guardar� la no conformidad
				dtNewRow.Item("PROVE") = _Request("GEN_NoConfProve")
				dtNewRow.Item("ID") = _Request("GEN_NoConformidad")
			Else  'Est� pidiendo una no conformidad
				dtNewRow.Item("PROVE") = _Request("GEN_NoConfProve")
				If _Request("GEN_NoConfCon") = "" Then
					dtNewRow.Item("CONTACTO") = System.DBNull.Value
				Else
					dtNewRow.Item("CONTACTO") = Numero(_Request("GEN_NoConfCon"))
				End If
				dtNewRow.Item("PETICIONARIO") = _Usuario.CodPersona
			End If
			If _Request("GEN_DespubAnyo") <> "" AndAlso _Request("GEN_DespubMes") <> "" AndAlso _Request("GEN_DespubDia") <> "" Then
				Dim D As Date
				D = New Date(_Request("GEN_DespubAnyo"), _Request("GEN_DespubMes"), _Request("GEN_DespubDia"))
				dtNewRow.Item("FEC_LIM_RESOL") = D
			Else
				dtNewRow.Item("FEC_LIM_RESOL") = System.DBNull.Value
			End If
			If _Request("GEN_FecApliPlanAnyo") <> "" AndAlso _Request("GEN_FecApliPlanMes") <> "" AndAlso _Request("GEN_FecApliPlanDia") <> "" Then
				Dim D As Date
				D = New Date(_Request("GEN_FecApliPlanAnyo"), _Request("GEN_FecApliPlanMes"), _Request("GEN_FecApliPlanDia"))
				dtNewRow.Item("FEC_APLICACION_PLAN") = D
			Else
				dtNewRow.Item("FEC_APLICACION_PLAN") = System.DBNull.Value
			End If

			If _Request("GEN_NoConfRevisor") = "null" Then
				dtNewRow.Item("REVISOR") = System.DBNull.Value
			Else
				dtNewRow.Item("REVISOR") = _Request("GEN_NoConfRevisor")
			End If
			If _Request("GEN_NoConfUnidadQa") = "null" Then
				dtNewRow.Item("UNIDADQA") = System.DBNull.Value
			Else
				dtNewRow.Item("UNIDADQA") = _Request("GEN_NoConfUnidadQa")
			End If
			If _Request("GEN_CambioRevisor") <> "" Then
				dtNewRow.Item("CAMBIOREVISOR") = _Request("GEN_CambioRevisor")
			Else
				dtNewRow.Item("CAMBIOREVISOR") = 0
			End If
			If _Request("GEN_NoConfProveERP") = "" Then
				dtNewRow.Item("PROVE_ERP") = System.DBNull.Value
			Else
				dtNewRow.Item("PROVE_ERP") = _Request("GEN_NoConfProveERP")
			End If
			If _Request("GEN_NoConfProveERPDen") = "" Then
				dtNewRow.Item("PROVE_ERP_DEN") = System.DBNull.Value
			Else
				dtNewRow.Item("PROVE_ERP_DEN") = _Request("GEN_NoConfProveERPDen")
			End If
			_dtNoConf.Rows.Add(dtNewRow)
		End Sub
	End Class
#End Region
End Class