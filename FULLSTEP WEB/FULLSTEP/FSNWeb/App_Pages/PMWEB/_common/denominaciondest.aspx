<%@ Page Language="vb" AutoEventWireup="false" Codebehind="denominaciondest.aspx.vb" Inherits="Fullstep.FSNWeb.denominaciondest"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblDest" cellSpacing="1" cellPadding="1" width="100%" border="0" height="80%"
				style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 8px">
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px; HEIGHT: 1px">
						<asp:Label id="lblLitCod" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist" style="HEIGHT: 1px">
						<asp:Label id="lblCod" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px; HEIGHT: 8px">
						<asp:Label id="lblLitDen" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist" style="HEIGHT: 8px">
						<asp:Label id="lblDen" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px; HEIGHT: 13px">
						<asp:Label id="lblLitDir" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist" style="HEIGHT: 13px">
						<asp:Label id="lblDir" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px; HEIGHT: 6px">
						<asp:Label id="lblLitCP" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist" style="HEIGHT: 6px">
						<asp:Label id="lblCP" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px; HEIGHT: 20px">
						<asp:Label id="lblLitPob" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist" style="HEIGHT: 20px">
						<asp:Label id="lblPob" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px; HEIGHT: 8px">
						<asp:Label id="lblLitProv" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist" style="HEIGHT: 8px">
						<asp:Label id="lblProv" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px; HEIGHT: 10px">
						<asp:Label id="lblLitPais" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist" style="HEIGHT: 10px">
						<asp:Label id="lblPais" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD colSpan="2" align="center" height="50">
					<INPUT id="cmdCerrar" name="cmdCerrar" class="botonPMWEB" type="button" value="DCerrar" runat="server" onclick="window.close()"></TD>
					<TD></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</html>
