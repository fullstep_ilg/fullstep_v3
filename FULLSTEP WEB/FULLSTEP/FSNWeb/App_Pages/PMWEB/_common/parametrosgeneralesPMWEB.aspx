<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="parametrosgeneralesPMWEB.aspx.vb" Inherits="Fullstep.FSNWeb.parametrosgeneralesPMWEB" %>    
<%@ Register TagPrefix="igtab" Namespace="Infragistics.WebUI.UltraWebTab" Assembly="Infragistics.WebUI.UltraWebTab.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Register TagPrefix="ignav" Namespace="Infragistics.WebUI.UltraWebNavigator" Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
	<title></title>
	<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
	<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
	<meta content="JavaScript" name="vs_defaultClientScript" />
	<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
</head>
	
	<script type="text/javascript">
		var arrGMN1 = [];
		var arrGMN2 = [];
		var arrGMN3 = [];		

		function uwtMateriales_NodeChecked(treeId, nodeId, bChecked) {

			var oTree = igtree_getTreeById(treeId)
			var node = igtree_getNodeById(nodeId)

			//inhabilita el evento nodeclick para que no se lance cuando chequeemos los padres o hijos
			s = oTree.Events.NodeChecked[0]
			oTree.Events.NodeChecked[0] = ""

			//Mantener Array de Modificados
			gestionArray(node, bChecked)

			if (!bChecked) {
				//se deschequean los padres
				var onodeParent = node.getParent()
				while (onodeParent) {
					//Si hay que cambiar el Checked del Nodo
					if (onodeParent.getChecked() != bChecked) {
						//Se lo cambio y
						onodeParent.setChecked(bChecked)
						//Mantener Array de Modificados
						gestionArray(onodeParent, bChecked)
					}
					onodeParent = onodeParent.getParent()
				}
			}

			//se des|chequean los hijos
			if (node.hasChildren() == true) {
				var onodeChild1 = node.getFirstChild()
				if (onodeChild1.getLevel() < 3) //Los GMN4 no los consideramos
				{
					while (onodeChild1) {
						//Si hay que cambiar el Checked del Nodo
						if (onodeChild1.getChecked() != bChecked) {
							//Se lo cambio y
							onodeChild1.setChecked(bChecked)
							//Mantener Array de Modificados
							gestionArray(onodeChild1, bChecked)
						}

						if (onodeChild1.hasChildren() == true) {
							var onodeChild2 = onodeChild1.getFirstChild()
							if (onodeChild2.getLevel() < 3) {
								while (onodeChild2) {
									//Si hay que cambiar el Checked del Nodo
									if (onodeChild2.getChecked() != bChecked) {
										//Se lo cambio y
										onodeChild2.setChecked(bChecked)
										//Mantener Array de Modificados
										gestionArray(onodeChild2, bChecked)
									}

									/*if (onodeChild2.hasChildren()==true)
									{
									var onodeChild3=onodeChild2.getFirstChild()
									while (onodeChild3)
									{
									onodeChild3.setChecked(bChecked)
									onodeChild3 = onodeChild3.getNextSibling()
									}
									}*/
									onodeChild2 = onodeChild2.getNextSibling()
								}
							}
						}

						onodeChild1 = onodeChild1.getNextSibling()
					}
				}
			}

			oTree.Events.NodeChecked[0] = s
		}

		function gestionArray(node, bChecked) {
			//Guarda o saca el nodo del Array de Modificados
			var valorNuevo = (bChecked ? "1" : "0");
			//var valorViejo = (bChecked ? "0" : "1");
			//var encontrado = false;

			var iLevel = node.getLevel()
			switch (iLevel) {
				case 0: //GMN1
					if (node.getTag() == "+") //Si estaba en el array, lo sacamos
					{
						arrGMN1[node.getDataKey()] = null;
						node.setTag("-");
					}
					else //Si no estaba en el array, lo metemos
					{
						node.setTag("+");
						arrGMN1[node.getDataKey()] = node.getDataKey() + "&&&" + valorNuevo;
					}
					break;

				case 1: //GMN2
					if (node.getTag() == "+") //Si estaba en el array, lo sacamos
					{
						arrGMN2[node.getParent().getDataKey() + "&&&" + node.getDataKey()] = null;
						node.setTag("-");

					}
					else //Si no estaba en el array, lo metemos
					{
						node.setTag("+");
						arrGMN2[node.getParent().getDataKey() + "&&&" + node.getDataKey()] = node.getParent().getDataKey() + "&&&" + node.getDataKey() + "&&&" + valorNuevo;
					}
					break;

				case 2: //GMN3
					if (node.getTag() == "+") //Si estaba en el array, lo sacamos
					{
						arrGMN3[node.getParent().getParent().getDataKey() + "&&&" + node.getParent().getDataKey() + "&&&" + node.getDataKey()] = null
						node.setTag("-");

					}
					else //Si no estaba en el array, lo metemos
					{
						node.setTag("+");
						arrGMN3[node.getParent().getParent().getDataKey() + "&&&" + node.getParent().getDataKey() + "&&&" + node.getDataKey()] = node.getParent().getParent().getDataKey() + "&&&" + node.getParent().getDataKey() + "&&&" + node.getDataKey() + "&&&" + valorNuevo;
					}
					break;
			}

		}
		/*
		<summary>
		Se ejecuta onclick de cmdAceptar antes del submit del formulario.A�ade al formulario input type hidden con los materiales chequeados en el treeview de materiales de GS
		Adem�s vacia el contenido del input hidden que genera el ultrawebgrid uwtMateriales, ya que al pasar PMWeb a 2008, el contenido de este input da un error por contener $
		Como el contenido del input no se utiliza para recoger los valores del arbol, lo vaciamos y evitamos el error. 
		</summary>
		<remarks>Llamada desde:  cmdAceptar.Attributes.Add("onclick", "return montarForm();")</remarks>
		*/
		function montarForm() {
			for (var n in arrGMN1) {
				if (arrGMN1[n] != null) {
					anyadirInput("GMN1_" + arrGMN1[n], arrGMN1[n])
				}
			}
			for (var n in arrGMN2) {
				if (arrGMN2[n] != null) {
					anyadirInput("GMN2_" + arrGMN2[n], arrGMN2[n])
				}
			}
			for (var n in arrGMN3) {
				if (arrGMN3[n] != null) {
					anyadirInput("GMN3_" + arrGMN3[n], arrGMN3[n])
				}
			}
			if (document.forms["Form1"].elements["<%=uwtMateriales.UniqueID%>"]) {
				document.forms["Form1"].elements["<%=uwtMateriales.UniqueID%>"].value = "";
			}

			if (document.getElementById("<%=wddNivelesDefectoUNQAProveedor.ClientID%>")) {
				if (document.getElementById("UNQADefecto").value=='') {
					alert(alertaNivelUNQADefecto)
					return false;
				}
			}
			return true;
		}
		/*''' <summary>
		''' A�ade un hidden a la pantalla con la informaci�n proporcionada
		''' </summary>
		''' <param name="sInput">Id del hidden</param>
		''' <param name="valor">valor del hidden</param>        
		''' <remarks>Llamada desde: montarForm; Tiempo m�ximo: 0</remarks>>*/
		function anyadirInput(sInput, valor) {
			var s
			if (valor == "null") {
				valor = ""
			}
			if (valor == null) {
				valor = ""
			}

			var f = document.forms["Form1"]
			if (!f.elements[sInput]) {
				s = "<INPUT type=hidden name=" + sInput + ">\n"
				f.insertAdjacentHTML("beforeEnd", s)
				f.elements[sInput].value = valor
			}
		}

		function enableTree(bEnabled) {
			var oTree = igtree_getTreeById("uwtMateriales")
			var nodesN1 = oTree.getNodes()
			var s = "";
			for (i = 0; i < nodesN1.length; i++) {
				nodesN1[i].setEnabled(bEnabled)
			}
		}

		/*''' <summary>
		''' En la secci�n "Mostrar variables de calidad en el panel de proveedores de forma" se establece q la forma es Manual
		''' </summary>
		''' <remarks>Llamada desde: chkManual.onclick; Tiempo m�ximo: 0</remarks>*/
		function Manual() {
			var wDropDownNiveles = document.getElementById("<%=wddNiveles.ClientID%>")
			if (wDropDownNiveles) {
				wDropDownNiveles.CurrentValue = "";
			}
			document.getElementById("uwtParametros__ctl0_chkManual").checked = true;
			document.getElementById("uwtParametros__ctl0_chkAutomatica").checked = false;
			MostrarOcultarAutomatica();
		}

		function Auto() {
			document.getElementById("uwtParametros__ctl0_chkAutomatica").checked = true;
			document.getElementById("uwtParametros__ctl0_chkManual").checked = false;
			MostrarOcultarAutomatica();
		}

		//''' <summary>Muestra u oculta el check chkAsigProve</summary>
		//''' <remarks>Llamada desde: chkAdjProve.onclick</remarks>
		function chkAdjProve_Click(sender) {		                             
            var oDivAsigProve = document.getElementById("<%=divAsigProve.ClientID%>");
            if (sender.checked) {
                oDivAsigProve.style.visibility = "visible";
            }
            else {
                oDivAsigProve.style.visibility = "hidden";                
                document.getElementById("<%=chkAsigProve.ClientID%>").checked = false;
            }
		}

        //''' <summary>Muestra u oculta el check chkAsigProve</summary>
		//''' <remarks>Llamada desde: chkAdjProve.onclick</remarks>
	    function chkNotifCambioCalif_Click(sender) {
	        var oDivTipoNotifCambioCalif = document.getElementById("<%=divTipoNotifCambioCalif.ClientID%>");
            if (sender.checked) {
                oDivTipoNotifCambioCalif.style.display = "inline-block";
            }
            else {
                oDivTipoNotifCambioCalif.style.display = "none";                
            }
		}	    

		/*''' <summary>
		''' Si los dos combos de Notificar Proximidad Fin Resoluci�n/Fin Acci�n No Conformidad estan 
		''' deschequeados el combo de d�as de antelaci�n no esta accesible, eoc el combo esta accesible
		''' </summary>
		''' <remarks>Llamada desde: chkNotifProximFinResolucionNc.onclick
		'''         chkNotifProximFinAccionesNc.onclick; Tiempo m�ximo:0</remarks>*/
		function NotifProximFin() {
			if (document.getElementById("uwtParametros__ctl1_chkNotifProximFinResolucionNc")) {
				if (document.getElementById("uwtParametros__ctl1_chkNotifProximFinAccionesNc")) {
					var wDropDownPeriodoNotifNc = $find("<%=wddPeriodoNotifNc.ClientID%>")
					if (document.getElementById("uwtParametros__ctl1_chkNotifProximFinResolucionNc").checked == false) {
						if (document.getElementById("uwtParametros__ctl1_chkNotifProximFinAccionesNc").checked == false) {
							wDropDownPeriodoNotifNc.set_enabled(false);
						}
						else
							wDropDownPeriodoNotifNc.set_enabled(true);
					}
					else
						wDropDownPeriodoNotifNc.set_enabled(true);
				}
			}
		}

		/*''' <summary>
		''' Habilitar/deshabilitar el dropdown de niveles
		''' </summary>
		''' <remarks>Llamada desde: javascript Manual() y Auto(); Tiempo m�ximo:0</remarks>*/
		function MostrarOcultarAutomatica() {
			if (document.getElementById("uwtParametros__ctl0_chkManual")) {
				var wDropDownNiveles = $find("<%=wddNiveles.ClientID%>")
				if (document.getElementById("uwtParametros__ctl0_chkManual").checked == true) {
					wDropDownNiveles.set_enabled(false);
				}
				else
					wDropDownNiveles.set_enabled(true)
			}
		}
		/*''' <summary>
		''' Mantener en una hiden el codigo del contacto
		''' </summary>
		''' <param name="sender">componente input</param>
		''' <param name="eventArgs">evento</param>
		''' <remarks>Llamada desde: wddContacto/clientEvents; Tiempo m�ximo:0</remarks>*/
		function wddContacto_selectedIndexChanged(sender, eventArgs) {
			if (sender.get_selectedItems().length > 0)
				document.getElementById("txtCodContacto").value = sender.get_selectedItems()[0].get_value()
			else
				document.getElementById("txtCodContacto").value = ""
		}

		function wddNivelesDefectoUNQAProveedor_SelectionChanged(sender, item) {
			var hiddenUNQADefecto = document.getElementById("UNQADefecto")
			hiddenUNQADefecto.value = ''
				
			if (sender.get_selectedItems().length != 0) {                
				var i;
				for (i=0;i<sender.get_selectedItems().length;i++){
					if (hiddenUNQADefecto.value == '') {
						hiddenUNQADefecto.value = sender.get_selectedItems()[i].get_value()
					}else{
					hiddenUNQADefecto.value = hiddenUNQADefecto.value + ',' + sender.get_selectedItems()[i].get_value()
					}
				}
			}
		}		
		</script>
<body>
	<form id="Form1" method="post" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server">		
        <CompositeScript>
            <Scripts>
                <%--<asp:ScriptReference Name="ExtenderBase.BaseScripts.js" Assembly="AjaxControlToolkit" />
			    <asp:ScriptReference Name="Compat.Timer.Timer.js" Assembly="AjaxControlToolkit" />
			    <asp:ScriptReference Name="Animation.Animations.js" Assembly="AjaxControlToolkit" />
			    <asp:ScriptReference Name="Animation.AnimationBehavior.js" Assembly="AjaxControlToolkit" />
			    <asp:ScriptReference Name="PopupExtender.PopupBehavior.js" Assembly="AjaxControlToolkit" />
			    <asp:ScriptReference Name="AutoComplete.AutoCompleteBehavior.js"
				    Assembly="AjaxControlToolkit" />
                <asp:ScriptReference Path="../alta/js/AdjacentHTML.js" />--%>
            </Scripts>
        </CompositeScript>
	</asp:ScriptManager>    

	<input id="txtCodContacto" type="hidden" name="txtCodContacto" runat="server" />
	<input id="idNodoRaiz" name="idNodoRaiz" runat="server" type="hidden" />
	<input id="UNQADefecto" name="UNQADefecto" runat="server" type="hidden" />
	<table id="tblGeneral" style="left: 8px; position: absolute; top: 140px"
		cellspacing="1" cellpadding="1" width="100%" border="0" runat="server">
		<tr>
			<td>
				<igtab:ultrawebtab id="uwtParametros" runat="server" width="100%"
					height="90%" customrules="padding:10px;" fixedlayout="True"
					dummytargeturl=" " threedeffect="False" borderwidth="1px" borderstyle="Solid"
					selectedtab="0">
					<defaulttabstyle height="24px" cssclass="uwtDefaultTab">
						<Padding Left="20px" Right="20px"></Padding>
					</defaulttabstyle>
					<roundedimage normalimage="ig_tab_blueb2.gif" hoverimage="ig_tab_blueb1.gif" fillstyle="LeftMergedWithCenter"></roundedimage>
					<tabs>
						<igtab:Tab Key="Parametros" Text="DPar&#225;metros">
							<ContentTemplate>
								<table id="tblParametros">
									<tr>
										<td vAlign="middle">
											<asp:label id="lblTitulo" runat="server" EnableViewState="False" CssClass="captionBlue">DGenerales</asp:label>
										</td>
									</tr>
									<tr id="LabelExpiracion" runat="server">
										<td vAlign="middle">
											<asp:label id="lblProxExpirar" runat="server" EnableViewState="False" CssClass="parrafo">DProximidad expiraci�n certificados:</asp:label>
										</td>
									</tr>
									<tr id="Expiracion" runat="server">
										<td vAlign="top">
											<nobr>
												<ig:WebDropDown ID ="wddProximidad" runat="server" Height="22px" Width="200px" 
													EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true"
													AutoFilterQueryType="StartsWith" AutoSelectOnMatch="true" EnableAutoCompleteFirstMatch="true" 
													EnableAutoFiltering="Off" EnableCustomValues="false" EnableCustomValueSelection="true" EnableClosingDropDownOnBlur="true"
													EnableAnimations="false" EnableDropDownAsChild="false">
												</ig:WebDropDown>
											</nobr>
										</td>
									</tr>
									<tr id="LabelPeriodoValidez" runat="server">
										<td vAlign="middle">
											<asp:label id="lblPeriodoValidez" runat="server" EnableViewState="False" CssClass="parrafo">DProximidad expiraci�n certificados:</asp:label>
										</td>
									</tr>
									<tr id="PeriodoValidez" runat="server">
										<td vAlign="top">
											<nobr>
												<ig:WebDropDown ID ="wddPeriodoValidez" runat="server" Height="22px" Width="200px" 
													EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true"
													AutoFilterQueryType="StartsWith" AutoSelectOnMatch="true" EnableAutoCompleteFirstMatch="true" 
													EnableAutoFiltering="Off" EnableCustomValues="false" EnableCustomValueSelection="true" EnableClosingDropDownOnBlur="true"
													EnableAnimations="false" EnableDropDownAsChild="false">
												</ig:WebDropDown>
											</nobr>													
										</td>
									</tr>
									<tr id="SeparaContacto" runat="server">
										<td>
											<hr class="lineasepgruesa"/>
										</td>
									</tr>
									<tr id="Contacto" runat="server">
										<td style="HEIGHT: 16px">
											<asp:label id="lblContactoAutom" runat="server" EnableViewState="False" CssClass="parrafo"
												Height="20px">DUsar el siguiente contacto para los certificados de solicitud aut�matica:</asp:label>
											<ig:WebDropDown ID="wddContacto" runat="server" EnableViewState="True" Height="22px" Width="200px" 
													EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" AutoFilterQueryType="StartsWith"
													AutoSelectOnMatch="true" EnableAutoCompleteFirstMatch="true" EnableAutoFiltering="Off" 
													EnableCustomValues="false" EnableCustomValueSelection="true" EnableClosingDropDownOnBlur="true"
													EnableAnimations="false" EnableDropDownAsChild="false">
                                                    <ClientEvents SelectionChanged="wddContacto_selectedIndexChanged" />
											</ig:WebDropDown>
										</td>
									</tr>
									<tr>
										<td>
											<table id="tblProveedores" cellSpacing="1" cellPadding="1" width="100%" border="0" runat="server">
												<tr>
													<td>
														<hr class="lineasepgruesa"/>
													</td>
												</tr>
												<tr>
													<td vAlign="middle">
														<asp:label id="lblProvePotAVal" runat="server" EnableViewState="False" CssClass="parrafo">DProveedores Potenciales a Proveedores V�lidos:</asp:label>
													</td>
												</tr>
												<tr>
                                                    <td>
													    <div id="divAdjProve" style="margin-right:5px; display: inline-block;" runat="server">
														    <asp:checkbox id="chkAdjProve" runat="server" EnableViewState="False" CssClass="parrafo" Text="DAdjudicar proveedor" onclick="chkAdjProve_Click(this)"></asp:checkbox>
													    </div>
                                                        <div id="divAsigProve" style="display: inline-block; visibility:visible" runat="server">
														    <asp:checkbox id="chkAsigProve" runat="server" EnableViewState="False" CssClass="parrafo" Text="DAsignar proveedor"></asp:checkbox>
                                                        </div>
													</td>                                                    
												</tr>
												<tr>
													<td>
														<asp:checkbox id="chkPedDirGS" runat="server" EnableViewState="False" CssClass="parrafo" Text="DPedido directo desde GS"></asp:checkbox>
													</td>
												</tr>
												<tr>
													<td>
														<asp:checkbox id="chkPedApEP" runat="server" EnableViewState="False" CssClass="parrafo" Text="DPedido de Aprovisionamiento desde EP"></asp:checkbox>
													</td>
												</tr>
                                                <tr> 
                                                    <td style="padding-top:5px;">
                                                        <div style="margin-right:5px; display: inline-block; float:left;">
                                                            <asp:label id="lblMatConversion" runat="server" EnableViewState="False" CssClass="parrafo">DActivar la conversi�n autom�tica s�lo en los siguientes Materiales:</asp:label>
                                                        </div> 
                                                        <div style="margin-right:5px; display: inline-block; float:left; width:320px;">                                                       
                                                            <ig:WebDropDown ID="wddMateriales" runat="server" ViewStateMode="Enabled" Width="98%" EnableMultipleSelection="true" EnableClosingDropDownOnSelect="false" DropDownContainerWidth="298px" 
							                                    EnableDropDownAsChild="false" CurrentValue=""  EnableClosingDropDownOnBlur="true"> 
						                                    </ig:WebDropDown>
                                                        </div>                                                    
                                                    </td>                                                   
                                                </tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<table id="tblGrupoUNQA" cellSpacing="1" cellPadding="1" width="100%" border="0" runat="server">
												<tr>
													<td>
														<hr class="lineasepgruesa"/>
													</td>
												</tr>
												<tr>
													<td vAlign="middle">
														<asp:label id="lblGrupoUNQA" runat="server" EnableViewState="False" CssClass="parrafo">DDenominaciones del nodo raiz de las unidades de negocio</asp:label>
													</td>
												</tr>
												<tr>
													<td>
														<table id="tblGrupoIdiomas" runat="server">
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<table runat="server" id="tblNivelesDefectoUNQAProveedor" cellSpacing="1" cellPadding="1" width="100%" border="0">
												<tr>
													<td>
														<hr class="lineasepgruesa"/>
													</td>
												</tr>
												<tr>
													<td style="vertical-align:middle">
														<asp:label id="lblNivelesDefectoUNQAProve" runat="server" EnableViewState="False" CssClass="parrafo" Text="DNivel o niveles de unidades de negocio a mostrar por defecto al proveedor en sus calificaciones"></asp:label>
													</td>
												</tr>
												<tr>
													<td style="vertical-align:top">
														<nobr>
															<ig:WebDropDown ID ="wddNivelesDefectoUNQAProveedor" runat="server" Height="22px" Width="300px" 
																EnableMultipleSelection="true" EnableClosingDropDownOnSelect="false"
																AutoSelectOnMatch="true" EnableAutoCompleteFirstMatch="true" 
																EnableAutoFiltering="Off" EnableCustomValues="true" EnableCustomValueSelection="true" 
																EnableClosingDropDownOnBlur="true" EnableAnimatins="false" EnableDropDownAsChild="true">	
																<ClientEvents SelectionChanged="wddNivelesDefectoUNQAProveedor_SelectionChanged" />                                                                
															</ig:WebDropDown>											            
														</nobr>
													</td>
												</tr>
											</table>
										</td>									    
									</tr>
                                    <tr>
                                        <td>
                                            <asp:label ID ="lblPanelProve" runat="server" CssClass="parrafo" Text="dPanel de proveedores. Selecci�n de proveedores por material" Visible ="false"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ig:WebDropDown ID="wddTipoFiltroMat" runat="server" EnableViewState="True" Height="22px" Width="300px" 
													EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" AutoFilterQueryType="StartsWith"
													AutoSelectOnMatch="true" EnableAutoCompleteFirstMatch="true" EnableAutoFiltering="Off" 
													EnableCustomValues="false" EnableCustomValueSelection="true" EnableClosingDropDownOnBlur="true"
													EnableAnimations="false" EnableDropDownAsChild="false" Visible ="false">
											</ig:WebDropDown>
                                        </td>
                                    </tr>
									<tr>
										<td>
											<table id="tblMatManualAutomatica" cellSpacing="1" cellPadding="1" width="100%" border="0"
												runat="server">
												<tr id="SeparaManualAutomatica" runat="server">
													<td>
														<hr class="lineasepgruesa"/>
													</td>
												</tr>
												<tr>
													<td vAlign="middle">
														<asp:label id="lblUsuario" runat="server" EnableViewState="False" CssClass="captionBlue">DUsuario</asp:label>
													</td>
												</tr>
												<tr>
													<td vAlign="middle">
														<asp:label id="lblMatManualAutomatica" runat="server" EnableViewState="False" CssClass="parrafo">DMostrar variables de calidad en el panel de proveedores de forma:</asp:label>
													</td>
												</tr>
												<tr>
													<td>
														<asp:checkbox id="chkManual" onclick="Manual()" runat="server" EnableViewState="False" CssClass="parrafo" Text="DManual"></asp:checkbox>
													</td>
												</tr>
												<tr>
													<td>
														<asp:checkbox id="chkAutomatica" onclick="Auto()" runat="server" EnableViewState="False" CssClass="parrafo" Text="DAutom�tica"></asp:checkbox>
														<div id="divAutomatica" runat="server">
															<table id="tblAutomatica" cellSpacing="1" cellPadding="1" width="100%" border="0" runat="server">
																<tr>
																	<td vAlign="middle">
																		<asp:label id="lblAutomatica" runat="server" EnableViewState="False" CssClass="parrafo">DN� de niveles de variables a mostrar:</asp:label>
																		<ig:WebDropDown ID="wddNiveles" runat="server" EnableViewState="True" Height="22px" Width="200px" 
																			EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" DropDownContainerHeight="114px" 
																			AutoFilterQueryType="StartsWith" AutoSelectOnMatch="true" EnableAutoCompleteFirstMatch="true" 
																			EnableAutoFiltering="Off" EnableCustomValues="false" EnableCustomValueSelection="true" EnableClosingDropDownOnBlur="true"
																			EnableAnimations="false" EnableDropDownAsChild="false">
																		</ig:WebDropDown>
																	</td>
																</tr>
															</table>
														</div>
													</td>
												</tr>
												<tr>
													<td>
														<asp:checkbox id="chkRecibirNotificacionesCalcPunt" runat="server" EnableViewState="False" CssClass="parrafo" Text="DRecibir notificaci�n de errores en el c�lculo de puntuaciones."></asp:checkbox>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</ContentTemplate>
						</igtab:Tab>
						<igtab:Tab Key="Notificaciones" Text="DNotificaciones">
							<ContentTemplate>
								<table id="tblNotificacionesCalif" cellSpacing="1" cellPadding="1" width="100%" border="0" runat="server">
									<tr>
										<td>
											<asp:checkbox id="chkNotifCambioCalif" runat="server" EnableViewState="False" CssClass="parrafo" Text="DNotificar a los proveedores los cambios de calificaci�n en la puntuaci�n total" onclick="chkNotifCambioCalif_Click(this)"></asp:checkbox>&nbsp;                                            
										</td>
									</tr>    
                                    <tr>
                                        <td>
                                            <div id="divTipoNotifCambioCalif" style="display: inline-block; visibility:visible; padding-left:1em;" runat="server">
											    <asp:RadioButton id="rdbMensual" runat="server" EnableViewState="False" CssClass="parrafo" Text="DComunicar cambio de puntuci�n mensualmente" GroupName="TipoNotif" style="display:block;"></asp:RadioButton>
                                                <asp:RadioButton id="rdbEnCadaCambio" runat="server" EnableViewState="False" CssClass="parrafo" Text="DComunicar en cada cambio de puntuacion" GroupName="TipoNotif" style="display:block;"></asp:RadioButton>
                                            </div>
                                        </td>                                        
                                    </tr>                                
									<tr id="SeparaNotificaciones" runat="server">
										<td>
											<hr width="90%" align="left" class="lineasepgruesa"/>
										</td>
									</tr>									    
								</table>	
								<table id="tblNotificacionesProximidadNc" cellspacing="1" cellpadding="1" width="100%" border="0" runat="server">
									<tr>
										<td>
											<asp:checkbox id="chkNotifProximFinResolucionNc"  onclick="NotifProximFin()" runat="server" EnableViewState="False" CssClass="parrafo" Text="DNotificar a los proveedores de la pr�ximidad de la fecha fin de resoluci�n de una no conformidad abierta."></asp:checkbox>&nbsp;
										</td>						                    
									</tr>
									<tr>
										<td>
											<asp:checkbox id="chkNotifProximFinAccionesNc"  onclick="NotifProximFin()" runat="server" EnableViewState="False" CssClass="parrafo" Text="DNotificar a los proveedores de la pr�ximidad de las fechas l�mite de las acciones solicitidas en una no conformidad abierta."></asp:checkbox>&nbsp;
										</td>						                    						                    
									</tr>
									<tr>
										<td>
											<asp:label id="lblComboPeriodoNc" runat="server" EnableViewState="False" CssClass="parrafo">DEnviar los avisos de recordatorio por proximidad de fechas de fin de las no conformidades con la siguiente antelaci�n:</asp:label>
										</td>
									</tr>
									<tr>
										<td>
											<ig:WebDropDown ID="wddPeriodoNotifNc" runat="server" Height="22px" Width="200px" 
												EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" EnableClosingDropDownOnBlur="true"
												AutoFilterQueryType="StartsWith" AutoSelectOnMatch="true" EnableAutoCompleteFirstMatch="true" 
												EnableAutoFiltering="Off" EnableCustomValues="false" EnableCustomValueSelection="true"
												EnableAnimations="false" EnableDropDownAsChild="false"></ig:WebDropDown>
										</td>
									</tr>				                    
									<tr id ="SeparaNotifProximidadNc" runat="server">
										<td>
											<hr width="90%" align="left" class="lineasepgruesa"/>
										</td>
									</tr>
								</table>
								<table id="tblNotificaciones" cellSpacing="1" cellPadding="1" width="100%" border="0" runat="server">
									<tr>
										<td vAlign="middle" height="5">
											<asp:label id="lblNotif" runat="server" CssClass="parrafo">DNotificar Usuarios</asp:label>
										</td>
									</tr>
									<tr>
										<td style="HEIGHT: 25px">
											<asp:checkbox id="chkAsigAVal" runat="server" EnableViewState="False" CssClass="parrafo" Text="DAsignaci�n Material a Proveedores Validos"></asp:checkbox>&nbsp;
										</td>
									</tr>
									<tr>
										<td>
											<asp:checkbox id="chkDesAVal" runat="server" EnableViewState="False" CssClass="parrafo" Text="DDesasignaci�n Material a Proveedores Validos"></asp:checkbox>
										</td>
									</tr>
									<tr>
										<td>
											<asp:checkbox id="chkAsigAPot" runat="server" EnableViewState="False" CssClass="parrafo" Text="DAsignaci�n Material a Proveedores Potenciales"></asp:checkbox>
										</td>
									</tr>
									<tr>
										<td>
											<asp:checkbox id="chkDesAPot" runat="server" EnableViewState="False" CssClass="parrafo" Text="DDesasignaci�n Material a Proveedores Potenciales"></asp:checkbox>
										</td>
									</tr>
									<tr>
										<td>
											<asp:checkbox id="chkAltaMat" runat="server" EnableViewState="False" CssClass="parrafo" Text="DAlta Materiales GS"></asp:checkbox>
										</td>
									</tr>
									<tr>
										<td>
											<ignav:ultrawebtree id="uwtMateriales" runat="server" EnableViewState="False" CssClass="igTree" Height="300px"
												WebTreeTarget="HierarchicalTree" width="90%">
												<SelectedNodeStyle ForeColor="White" BackColor="Navy"></SelectedNodeStyle>
												<Levels>
													<ignav:Level Index="0"></ignav:Level>
													<ignav:Level Index="1"></ignav:Level>
												</Levels>
												<ClientSideEvents NodeChecked="uwtMateriales_NodeChecked"></ClientSideEvents>
											</ignav:ultrawebtree></td>
									</tr>
								</table>
							</ContentTemplate>
						</igtab:Tab>
					</tabs>
				</igtab:ultrawebtab>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				<asp:Button ID="cmdAceptar" runat="server" EnableViewState="False" Text="Aceptar"
					CssClass="botonPMWEB"></asp:Button>
			</td>
		</tr>
	</table>
	<uc1:menu ID="Menu1" runat="server" OpcionMenu="Configuracion" OpcionSubMenu="Parametros">
	</uc1:menu>
	</form>
</body>
</html>
