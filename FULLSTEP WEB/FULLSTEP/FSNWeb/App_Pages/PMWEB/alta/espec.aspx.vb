
Public Class espec
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oAdjunto As FSNServer.Adjunto
        oAdjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
        oAdjunto.Id = Request("Id")
        oAdjunto.Load()

        Dim sPath As String = oAdjunto.SaveAdjunToDisk(0)
        Dim arrPath() As String = sPath.Split("\")
        Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "_Common/download.aspx?path=" + arrPath(UBound(arrPath) - 1) + "&nombre=" + Server.UrlEncode(arrPath(UBound(arrPath))) + "&datasize=" + oAdjunto.dataSize.ToString)
    End Sub

End Class

