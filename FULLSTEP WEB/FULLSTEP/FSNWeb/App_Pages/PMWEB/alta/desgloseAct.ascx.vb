﻿Public Partial Class desgloseAct
    Inherits System.Web.UI.UserControl

    Private mlIdProyecto As Long
    Private mbObligatorio As Boolean
    Private mbReadOnly As Boolean
    Private msID As String
    Private msTitulo As String
    Private mlCampo As Long

    Private FSNServer As FSNServer.Root
    Private sIdioma As String = ConfigurationManager.AppSettings("idioma")
    Dim FSNUser As FSNServer.User
    Dim DenomFases As New Dictionary(Of String, String)
    Dim oProyecto As FSNServer.Proyecto
    Dim iMaxLinea As Short
    Dim oTextos As DataTable
    ''' <summary>
    ''' Id del proyecto a cargar en el grid
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property IdProyecto() As Long
        Get
            Return mlIdProyecto
        End Get
        Set(ByVal value As Long)
            mlIdProyecto = value
        End Set
    End Property

    Public Property Obligatorio() As Boolean
        Get
            Return mbObligatorio
        End Get
        Set(ByVal value As Boolean)
            mbObligatorio = value
        End Set
    End Property

    Public Property SoloLectura() As Boolean
        Get
            Return mbReadOnly
        End Get
        Set(ByVal value As Boolean)
            mbReadOnly = value
        End Set
    End Property
    Public Property ID() As String
        Get
            Return msID
        End Get
        Set(ByVal value As String)
            msID = value
        End Set
    End Property
    Public Property Titulo() As String
        Get
            Return msTitulo
        End Get
        Set(ByVal value As String)
            msTitulo = value
        End Set
    End Property

    Public Property Campo() As Long
        Get
            Return mlCampo
        End Get
        Set(ByVal value As Long)
            mlCampo = value
        End Set
    End Property



    ''' <summary>
    ''' Carga de la pagina
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim dsTareas As DataSet

        FSNServer = Session("FSN_Server")
        FSNUser = Session("FSN_User")
        oProyecto = FSNServer.Get_Object(GetType(FSNServer.Proyecto))

        Dim dr As DataRow
        CargarDenominacionFases()

        dsTareas = DevolverTareasProyecto()
        ConfigurarControles(dsTareas.Tables(0))
        uwgDesgloseActividad.DataSource = dsTareas.Tables(0)
        uwgDesgloseActividad.DataBind()


        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptPagina") Then
            Dim sScript = vbCrLf & "var sSession='" & HttpContext.Current.Session("sSession").ToString & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "idSession", sScript, True)
        End If


    End Sub
    ''' <summary>
    ''' Funcion que devuelve las tareas  de un determinado proyecto
    ''' </summary>
    ''' <returns>dataset con las tareas</returns>
    ''' <remarks></remarks>
    Private Function DevolverTareasProyecto() As DataSet

        Try
            
            Return oProyecto.DevolverTareasProyecto(mlIdProyecto, sIdioma)

        Catch ex As Exception

        End Try
    End Function

    ''' <summary>
    ''' Procedimiento que configura los controles de la pagina...su idioma
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ConfigurarControles(ByVal dtTareas As DataTable)
        If mbReadOnly Then
            Me.ibAñadir.Visible = False
            Me.ibEliminarFilas.Visible = False
            Me.ibSubirExcel.Visible = False
            Me.ibBajarExcel.Visible = False
        End If
        If mbReadOnly Then
            hid_ReadOnly.Value = "1"
        Else
            hid_ReadOnly.Value = "0"
        End If
        hid_NumFases.Value = oProyecto.NumFasesProyecto
        hid_EstadoDefecto.Value = oProyecto.EstadoDefecto
        hid_Oblig.Value = IIf(mbObligatorio, "1", "0")

        Dim oDict As FSNServer.Dictionary = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.DesgloseActividad, sIdioma)
        oTextos = oDict.Data.Tables(0)

        lblTituloDesglose.Text = msTitulo
        btnOk.Text = oTextos.Rows(9)(1)
        btnOkReadOnly.Text = oTextos.Rows(9)(1)
        btnCancelar.Text = oTextos.Rows(10)(1)
        btnAceptarEliminarTarea.Text = oTextos.Rows(9)(1)
        btnCancelarEliminarTarea.Text = oTextos.Rows(10)(1)
        ibAñadir.ToolTip = oTextos.Rows(23)(1)
        ibEliminarFilas.ToolTip = oTextos.Rows(24)(1)
        ibSubirExcel.ToolTip = oTextos.Rows(28)(1)
        ibBajarExcel.ToolTip = oTextos.Rows(29)(1)

        Dim sLineasDesglose As String = String.Empty
        Select Case oProyecto.NumFasesProyecto
            Case 1
                For Each dr As DataRow In dtTareas.Rows
                    sLineasDesglose = sLineasDesglose & dr("COD_NIV4") & "#" & dr("CODIGO") & ";"
                Next
            Case 2
                For Each dr As DataRow In dtTareas.Rows
                    sLineasDesglose = sLineasDesglose & dr("COD_NIV3") & "#" & dr("COD_NIV4") & "#" & dr("CODIGO") & ";"
                Next
            Case 3
                For Each dr As DataRow In dtTareas.Rows
                    sLineasDesglose = sLineasDesglose & dr("COD_NIV2") & "#" & dr("COD_NIV3") & "#" & dr("COD_NIV4") & "#" & dr("CODIGO") & ";"
                Next
            Case 4
                For Each dr As DataRow In dtTareas.Rows
                    sLineasDesglose = sLineasDesglose & dr("COD_NIV1") & "#" & dr("COD_NIV2") & "#" & dr("COD_NIV3") & "#" & dr("COD_NIV4") & "#" & dr("CODIGO") & ";"
                Next
        End Select
        If sLineasDesglose <> String.Empty Then sLineasDesglose = sLineasDesglose.Substring(0, sLineasDesglose.Length - 1)

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptLineasDesglose") Then
            Dim sScript = vbCrLf & "var lineasDesglose='" & sLineasDesglose & " ';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sLineasDesglose", sScript, True)
        End If


        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptVarComentario") Then
            Dim sScript = vbCrLf & "var sComentario='" & oTextos.Rows(8)(1) & " ';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "idComentario", sScript, True)
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptEliminar") Then
            Dim sScript = vbCrLf & "var sEliminar='" & oTextos.Rows(16)(1) & " ';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sEliminar", sScript, True)
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptEliminarConHoras") Then
            Dim sTexto As String = oTextos.Rows(17)(1).ToString.Replace("XXXX", DenomFases("TAREA"))
            Dim sScript = vbCrLf & "var sEliminarConHoras='" & sTexto & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sEliminarConHoras", sScript, True)
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptEliminarTodas") Then
            Dim sScript = vbCrLf & "var sEliminarTodas='" & oTextos.Rows(18)(1) & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sElimTodas", sScript, True)
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptEliminarTodasConHoras") Then
            Dim sTexto As String = oTextos.Rows(19)(1).ToString.Replace("XXXX", DenomFases("TAREA"))
            Dim sScript = vbCrLf & "var sEliminarTodasConHoras='" & sTexto & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sElimTodasConHoras", sScript, True)
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptDenomTarea") Then
            Dim sScript = vbCrLf & "var sDenomTarea='" & DenomFases("TAREA") & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sTarea", sScript, True)
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptContinuar") Then
            Dim sScript = vbCrLf & "var sContinuar='" & oTextos.Rows(15)(1) & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sCont", sScript, True)
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptIdProyecto") Then
            Dim sScript = vbCrLf & "var sIdProyecto='" & mlIdProyecto & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sIdProy", sScript, True)
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptNumFases") Then
            Dim sScript = vbCrLf & "var sNumFases=" & oProyecto.NumFasesProyecto & ";" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sNumFas", sScript, True)
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptObligatorio") Then
            Dim sScript = vbCrLf & "var sObligatorio=" & IIf(mbObligatorio, "1", "0") & ";" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sOblig", sScript, True)
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptTextos") Then
            Dim sScript = vbCrLf & "var sTextos='" & oTextos.Rows(30)(1) & "#" & oTextos.Rows(22)(1) & "#" & oTextos.Rows(14)(1) & "#" & oTextos.Rows(31)(1) & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sText", sScript, True)
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptValidacion") Then
            Dim sScript = vbCrLf & "var sValidacionPlantilla='" & oTextos.Rows(32)(1) & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sValid", sScript, True)
        End If
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptRutaPM") Then
            Dim sScript = vbCrLf & "var sRutaPM='" & ConfigurationManager.AppSettings("rutaPM") & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sRutPM", sScript, True)
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "sError") Then
            Dim sScript = vbCrLf & "var sErrorPlantilla='" & oTextos.Rows(34)(1) & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sErrorPlan", sScript, True)
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptFechas") Then
            Dim sScript = vbCrLf & "var sFechas='" & oTextos.Rows(33)(1) & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sFec", sScript, True)
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptErrorFases") Then
            Dim sScript = vbCrLf & "var sErrorFases='" & oTextos.Rows(35)(1) & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sErrFases", sScript, True)
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptTareaExiste") Then
            Dim sScript = vbCrLf & "var sTareaExiste='" & oTextos.Rows(36)(1).ToString.Replace("XXXX", DenomFases("TAREA")) & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "TareaExiste", sScript, True)
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptNombreGrid") Then
            Dim sScript = vbCrLf & "var sNombreGridDesgloseAct='" & uwgDesgloseActividad.ClientID & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sNomGrid", sScript, True)
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "sNumeroLineas") Then
            Dim sScript = vbCrLf & "var sNumLineas=" & dtTareas.Rows.Count & ";" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sNLineas", sScript, True)
        End If

    End Sub
    ''' <summary>
    ''' Procedimiento que carga las denominaciones de las distintas fases del proyecto
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarDenominacionFases()
        Try
            Dim dtFases As DataTable


            oProyecto = FSNServer.Get_Object(GetType(FSNServer.Proyecto))

            dtFases = oProyecto.DevolverDenominacionFases(sIdioma)
            DenomFases.Clear()
            For Each dr As DataRow In dtFases.Rows
                Select Case dr("ID")
                    Case "39"
                        DenomFases.Add("NIVEL1", dr("DEN"))
                    Case "40"
                        DenomFases.Add("NIVEL2", dr("DEN"))
                    Case "41"
                        DenomFases.Add("NIVEL3", dr("DEN"))
                    Case "42"
                        DenomFases.Add("NIVEL4", dr("DEN"))
                    Case "43"
                        DenomFases.Add("TAREA", dr("DEN"))
                End Select
            Next

        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Procedimiento que inicializa el grid de datos
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub uwgDesgloseActividad_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwgDesgloseActividad.InitializeLayout

        uwgDesgloseActividad.Columns.FromKey("TAREADEN").Header.Caption = IIf(mbObligatorio = True, "(*)" & DenomFases("TAREA").ToString, DenomFases("TAREA").ToString)
        uwgDesgloseActividad.Columns.FromKey("NUM").Header.Caption = IIf(mbObligatorio = True, "(*)" & oTextos.Rows(4)(1), oTextos.Rows(4)(1))
        uwgDesgloseActividad.Columns.FromKey("FECEST_INICIO").Header.Caption = IIf(mbObligatorio = True, "(*)" & oTextos.Rows(1)(1), oTextos.Rows(1)(1))
        uwgDesgloseActividad.Columns.FromKey("FECEST_FIN").Header.Caption = IIf(mbObligatorio = True, "(*)" & oTextos.Rows(2)(1), oTextos.Rows(2)(1))
        uwgDesgloseActividad.Columns.FromKey("HORAS_ASIG").Header.Caption = IIf(mbObligatorio = True, "(*)" & oTextos.Rows(13)(1), oTextos.Rows(13)(1))
        uwgDesgloseActividad.Columns.FromKey("GRADO_AVANCE").Header.Caption = oTextos.Rows(5)(1)
        uwgDesgloseActividad.Columns.FromKey("COMENTARIOS").Header.Caption = oTextos.Rows(12)(1)
        uwgDesgloseActividad.Columns.FromKey("USUARIOS").Header.Caption = oTextos.Rows(11)(1)
        uwgDesgloseActividad.Columns.FromKey("ESTADO").Header.Caption = oTextos.Rows(6)(1)

        If mbReadOnly = True Then
            uwgDesgloseActividad.Columns.FromKey("ELIMINAR").Hidden = True
            uwgDesgloseActividad.Columns.FromKey("EDITAR").Hidden = True
        End If

        'Oculto columnas
        uwgDesgloseActividad.Columns.FromKey("CODESTADO").Hidden = True
        uwgDesgloseActividad.Columns.FromKey("ID").Hidden = True
        uwgDesgloseActividad.Columns.FromKey("CODIGO").Hidden = True
        uwgDesgloseActividad.Columns.FromKey("HITO").Hidden = True
        uwgDesgloseActividad.Columns.FromKey("TAREA").Hidden = True
        uwgDesgloseActividad.Columns.FromKey("CODUSUARIOS").Hidden = True
        uwgDesgloseActividad.Columns.FromKey("CODCONTACTOS").Hidden = True
        uwgDesgloseActividad.Columns.FromKey("DENUSUARIOS").Hidden = True
        uwgDesgloseActividad.Columns.FromKey("DENPROVE").Hidden = True
        uwgDesgloseActividad.Columns.FromKey("MON").Hidden = True
        uwgDesgloseActividad.Columns.FromKey("OBS").Hidden = True
        uwgDesgloseActividad.Columns.FromKey("STATUS").Hidden = True
        uwgDesgloseActividad.Columns.FromKey("EMP").Hidden = True
        uwgDesgloseActividad.Columns.FromKey("CODPROVE").Hidden = True
        uwgDesgloseActividad.Columns.FromKey("COSTE_TARE").Hidden = True
        uwgDesgloseActividad.Columns.FromKey("CODCATEGORIA").Hidden = True
        uwgDesgloseActividad.Columns.FromKey("CATEGORIA").Hidden = True
        uwgDesgloseActividad.Columns.FromKey("NUM_OLD").Hidden = True


        uwgDesgloseActividad.Columns.FromKey("GRADO_AVANCE").CellStyle.HorizontalAlign = HorizontalAlign.Right
        uwgDesgloseActividad.Columns.FromKey("HORAS_ASIG").CellStyle.HorizontalAlign = HorizontalAlign.Right
        uwgDesgloseActividad.Columns.FromKey("NUM").CellStyle.HorizontalAlign = HorizontalAlign.Right

        
        uwgDesgloseActividad.Columns.FromKey("FECEST_INICIO").CellStyle.HorizontalAlign = HorizontalAlign.Right
        uwgDesgloseActividad.Columns.FromKey("FECEST_FIN").CellStyle.HorizontalAlign = HorizontalAlign.Right

        WebDateTimeEdit1.DisplayModeFormat = FSNUser.DateFormat.ShortDatePattern
        WebDateTimeEdit1.EditModeFormat = FSNUser.DateFormat.ShortDatePattern
        WebDateTimeEdit1.DataMode = Infragistics.WebUI.WebDataInput.DateDataMode.Date

        uwgDesgloseActividad.Columns.FromKey("FECEST_FIN").Format = FSNUser.DateFormat.ShortDatePattern
        uwgDesgloseActividad.Columns.FromKey("FECEST_FIN").EditorControlID = WebDateTimeEdit1.UniqueID
        uwgDesgloseActividad.Columns.FromKey("FECEST_FIN").Type = Infragistics.WebUI.UltraWebGrid.ColumnType.Custom

        uwgDesgloseActividad.Columns.FromKey("FECEST_INICIO").Format = FSNUser.DateFormat.ShortDatePattern
        uwgDesgloseActividad.Columns.FromKey("FECEST_INICIO").EditorControlID = WebDateTimeEdit1.UniqueID
        uwgDesgloseActividad.Columns.FromKey("FECEST_INICIO").Type = Infragistics.WebUI.UltraWebGrid.ColumnType.Custom
        

        Select Case oProyecto.NumFasesProyecto
            Case 1
                uwgDesgloseActividad.Columns.FromKey("COD_NIV4").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("ID_NIV4").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("DEN_NIV4").Move(3)
                uwgDesgloseActividad.Columns.FromKey("TAREADEN").Move(4)
                uwgDesgloseActividad.Columns.FromKey("COMENTARIOS").Move(uwgDesgloseActividad.Columns.Count - 1)
                uwgDesgloseActividad.Columns.FromKey("USUARIOS").Move(uwgDesgloseActividad.Columns.Count)
                If mbObligatorio Then
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV4").Header.Caption = "(*)" & DenomFases("NIVEL1").ToString
                Else
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV4").Header.Caption = DenomFases("NIVEL1").ToString
                End If
            Case 2
                uwgDesgloseActividad.Columns.FromKey("COD_NIV4").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("ID_NIV4").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("COD_NIV3").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("ID_NIV3").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("DEN_NIV3").Move(3)
                uwgDesgloseActividad.Columns.FromKey("DEN_NIV4").Move(4)
                uwgDesgloseActividad.Columns.FromKey("TAREADEN").Move(5)
                uwgDesgloseActividad.Columns.FromKey("COMENTARIOS").Move(uwgDesgloseActividad.Columns.Count - 1)
                uwgDesgloseActividad.Columns.FromKey("USUARIOS").Move(uwgDesgloseActividad.Columns.Count)
                If mbObligatorio Then
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV4").Header.Caption = "(*)" & DenomFases("NIVEL2").ToString
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV3").Header.Caption = "(*)" & DenomFases("NIVEL1").ToString
                Else
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV4").Header.Caption = DenomFases("NIVEL2").ToString
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV3").Header.Caption = DenomFases("NIVEL1").ToString
                End If
            Case 3
                uwgDesgloseActividad.Columns.FromKey("COD_NIV4").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("ID_NIV4").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("COD_NIV3").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("ID_NIV3").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("COD_NIV2").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("ID_NIV2").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("DEN_NIV2").Move(3)
                uwgDesgloseActividad.Columns.FromKey("DEN_NIV3").Move(4)
                uwgDesgloseActividad.Columns.FromKey("DEN_NIV4").Move(5)
                uwgDesgloseActividad.Columns.FromKey("TAREADEN").Move(6)
                uwgDesgloseActividad.Columns.FromKey("COMENTARIOS").Move(uwgDesgloseActividad.Columns.Count - 1)
                uwgDesgloseActividad.Columns.FromKey("USUARIOS").Move(uwgDesgloseActividad.Columns.Count)
                If mbObligatorio Then
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV4").Header.Caption = "(*)" & DenomFases("NIVEL3").ToString
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV3").Header.Caption = "(*)" & DenomFases("NIVEL2").ToString
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV2").Header.Caption = "(*)" & DenomFases("NIVEL1").ToString
                Else
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV4").Header.Caption = DenomFases("NIVEL3").ToString
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV3").Header.Caption = DenomFases("NIVEL2").ToString
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV2").Header.Caption = DenomFases("NIVEL1").ToString
                End If
            Case 4
                uwgDesgloseActividad.Columns.FromKey("COD_NIV4").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("ID_NIV4").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("COD_NIV3").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("ID_NIV3").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("COD_NIV2").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("ID_NIV2").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("COD_NIV1").Hidden = True
                uwgDesgloseActividad.Columns.FromKey("ID_NIV1").Hidden = True

                uwgDesgloseActividad.Columns.FromKey("DEN_NIV1").Move(3)
                uwgDesgloseActividad.Columns.FromKey("DEN_NIV2").Move(4)
                uwgDesgloseActividad.Columns.FromKey("DEN_NIV3").Move(5)
                uwgDesgloseActividad.Columns.FromKey("DEN_NIV4").Move(6)
                uwgDesgloseActividad.Columns.FromKey("TAREADEN").Move(7)
                uwgDesgloseActividad.Columns.FromKey("COMENTARIOS").Move(uwgDesgloseActividad.Columns.Count - 1)
                uwgDesgloseActividad.Columns.FromKey("USUARIOS").Move(uwgDesgloseActividad.Columns.Count)
                If mbObligatorio Then
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV4").Header.Caption = "(*)" & DenomFases("NIVEL4").ToString
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV3").Header.Caption = "(*)" & DenomFases("NIVEL3").ToString
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV2").Header.Caption = "(*)" & DenomFases("NIVEL2").ToString
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV1").Header.Caption = "(*)" & DenomFases("NIVEL1").ToString
                Else
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV4").Header.Caption = DenomFases("NIVEL4").ToString
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV3").Header.Caption = DenomFases("NIVEL3").ToString
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV2").Header.Caption = DenomFases("NIVEL2").ToString
                    uwgDesgloseActividad.Columns.FromKey("DEN_NIV1").Header.Caption = DenomFases("NIVEL1").ToString
                End If
        End Select

    End Sub

    ''' <summary>
    ''' Procedimiento que configura cada fila segun se va cargando en el grid
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub uwgDesgloseActividad_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgDesgloseActividad.InitializeRow

        If DBNullToInteger(DataBinder.Eval(e.Data, "NUM")) > iMaxLinea Then
            iMaxLinea = DBNullToInteger(DataBinder.Eval(e.Data, "NUM"))
        End If

        'HITO


        Dim tc As Infragistics.WebUI.UltraWebGrid.TemplatedColumn = CType(uwgDesgloseActividad.Bands(0).Columns.FromKey("COL_HITO"), Infragistics.WebUI.UltraWebGrid.TemplatedColumn)
        Dim imgHito As HtmlControls.HtmlImage = CType(CType(tc.CellItems(e.Row.Index), Infragistics.WebUI.UltraWebGrid.CellItem).FindControl("imgHito"), HtmlControls.HtmlImage)

        If DataBinder.Eval(e.Data, "HITO").ToString = "0" Then
            'Si no es un Hito le quito la imagen de la bandera
            imgHito.Height = "0"
        Else
            uwgDesgloseActividad.Columns.FromKey("COL_HITO").Hidden = False
            imgHito.Alt = oTextos(14)(1)
        End If

        'USUARIOS

        Dim tcUsu As Infragistics.WebUI.UltraWebGrid.TemplatedColumn = CType(uwgDesgloseActividad.Bands(0).Columns.FromKey("USUARIOS"), Infragistics.WebUI.UltraWebGrid.TemplatedColumn)
        Dim lblUsuarios As Label = CType(CType(tcUsu.CellItems(e.Row.Index), Infragistics.WebUI.UltraWebGrid.CellItem).FindControl("lblUsuarios"), Label)
        Dim btnUsu As HtmlControls.HtmlImage = CType(CType(tcUsu.CellItems(e.Row.Index), Infragistics.WebUI.UltraWebGrid.CellItem).FindControl("btnUsuarios"), HtmlControls.HtmlImage)
        Dim cellUsu As HtmlControls.HtmlTableCell = CType(CType(tcUsu.CellItems(e.Row.Index), Infragistics.WebUI.UltraWebGrid.CellItem).FindControl("cellUsuarios"), HtmlControls.HtmlTableCell)

        If DataBinder.Eval(e.Data, "DENUSUARIOS").ToString.Length > 30 Then
            lblUsuarios.Text = DataBinder.Eval(e.Data, "DENUSUARIOS").ToString.Substring(0, 30) & "..."
            lblUsuarios.Style.Add("filter", "alpha(opacity = 100)")
            lblUsuarios.Style.Add("opacity", "100")
        Else
            If DataBinder.Eval(e.Data, "DENUSUARIOS").ToString = "" Then
                lblUsuarios.Text = "Texto"
                lblUsuarios.Style.Add("filter", "alpha(opacity = 0)")
                lblUsuarios.Style.Add("opacity", "0")
                If mbReadOnly = True Then
                    cellUsu.Style.Add("visibility", "hidden") 'Si no hay usuarios y estamos en modo solo lectura no se muestra el boton
                End If
            Else
                lblUsuarios.Text = DataBinder.Eval(e.Data, "DENUSUARIOS").ToString
                lblUsuarios.Style.Add("filter", "alpha(opacity = 100)")
                lblUsuarios.Style.Add("opacity", "100")
            End If

        End If
        lblUsuarios.Style.Add("color", "#494949")
        cellUsu.Attributes.Add("onclick", "AbrirUsuarios(" & e.Row.Index & ")")
        btnUsu.Src = ConfigurationManager.AppSettings("rutaPM") & "_common/images/3puntos.gif"
        btnUsu.Attributes.Add("onclick", "AbrirUsuarios(" & e.Row.Index & ")")

        'EDITAR

        Dim tcEditar As Infragistics.WebUI.UltraWebGrid.TemplatedColumn = CType(uwgDesgloseActividad.Bands(0).Columns.FromKey("EDITAR"), Infragistics.WebUI.UltraWebGrid.TemplatedColumn)
        Dim imgEditar As HtmlControls.HtmlImage = CType(CType(tcEditar.CellItems(e.Row.Index), Infragistics.WebUI.UltraWebGrid.CellItem).FindControl("btnEditar"), HtmlControls.HtmlImage)

        imgEditar.Attributes.Add("onclick", "Editar(" & e.Row.Index & ")")
        imgEditar.Alt = oTextos(22)(1)

        'ELIMINAR

        Dim tcEliminar As Infragistics.WebUI.UltraWebGrid.TemplatedColumn = CType(uwgDesgloseActividad.Bands(0).Columns.FromKey("ELIMINAR"), Infragistics.WebUI.UltraWebGrid.TemplatedColumn)
        Dim imgEliminar As HtmlControls.HtmlImage = CType(CType(tcEliminar.CellItems(e.Row.Index), Infragistics.WebUI.UltraWebGrid.CellItem).FindControl("imgEliminar"), HtmlControls.HtmlImage)

        imgEliminar.Attributes.Add("onclick", "Eliminar(" & e.Row.Index & ")")
        imgEliminar.Alt = oTextos(30)(1)

        'COMENTARIOS

        Dim tcComentarios As Infragistics.WebUI.UltraWebGrid.TemplatedColumn = CType(uwgDesgloseActividad.Bands(0).Columns.FromKey("COMENTARIOS"), Infragistics.WebUI.UltraWebGrid.TemplatedColumn)
        Dim imgComentario As HtmlControls.HtmlImage = CType(CType(tcComentarios.CellItems(e.Row.Index), Infragistics.WebUI.UltraWebGrid.CellItem).FindControl("imgComentario"), HtmlControls.HtmlImage)

        If DBNullToStr(DataBinder.Eval(e.Data, "OBS")).ToString = "" Then
            'Si no tiene comentarios
            imgComentario.Height = "0"
        Else
            imgComentario.Attributes.Add("onclick", "AbrirComentarios(" & e.Row.Index & ")")
            imgComentario.Alt = oTextos(31)(1)
            uwgDesgloseActividad.Columns.FromKey("COMENTARIOS").Hidden = False
        End If


    End Sub


    Private Sub uwgDesgloseActividad_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles uwgDesgloseActividad.Load
        Dim sScript = vbCrLf & "var MaxFila=" & iMaxLinea.ToString & vbCrLf
        Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sMaxFila", sScript, True)

        If uwgDesgloseActividad.Rows.Count = 0 Then
            ibEliminarFilas.Style.Add("visibility", "hidden")
        End If
    End Sub
End Class