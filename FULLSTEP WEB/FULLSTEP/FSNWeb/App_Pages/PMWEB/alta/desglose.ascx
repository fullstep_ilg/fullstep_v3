<%@ Control Language="vb" AutoEventWireup="false" Codebehind="desglose.ascx.vb" Inherits="Fullstep.FSNWeb.desgloseControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False"%>
<script type="text/javascript">
    var cargandoLineas, MostrandoError;
    cargandoLineas = false;
    MostrandoError = false;
    $(document).ready(function () {
        $.each($('[id$=lblInfoExcel]'), function () {
            if ($(this).attr('OcultarBotonesExcel') == 'false') $(this).next().attr('src', rutaPM + 'alta/images/help.gif');
            else $(this).closest('table').hide();
        });
    });
    function GenerarPlantillaExcel(idCampoDesglose) {        
        //Al pulsar el boton de generar excel, llamaremos al servicio que genere la excel 
        //y cancelaremos la accion del boton para no hacer postback
        MostrarCargando();
        var codOrgCompras, codCentro, codUnidadOrganizativa, codCentroCoste, codDestino;
        var idsOrgComprasOutDesglose = $(arrInputs).filter(function (index, element) { return (fsGeneralEntry_getById(element) !== null && fsGeneralEntry_getById(element).idCampoPadre == null && fsGeneralEntry_getById(element).tipoGS == 123) });
        if (idsOrgComprasOutDesglose.length == 0) codOrgCompras = '';
        else codOrgCompras = fsGeneralEntry_getById(idsOrgComprasOutDesglose[0]).getDataValue();
        
        var idsCentroOutDesglose = $(arrInputs).filter(function (index, element) { return (fsGeneralEntry_getById(element) !== null && fsGeneralEntry_getById(element).idCampoPadre == null && fsGeneralEntry_getById(element).tipoGS == 124) });
        if (idsCentroOutDesglose.length == 0) codCentro = '';
        else codCentro = fsGeneralEntry_getById(idsCentroOutDesglose[0]).getDataValue();

        var idsDestinoOutDesglose = $(arrInputs).filter(function (index, element) { return (fsGeneralEntry_getById(element) !== null && fsGeneralEntry_getById(element).idCampoPadre == null && fsGeneralEntry_getById(element).tipoGS == 109) });
        if (idsDestinoOutDesglose.length == 0) codDestino = '';
        else codDestino = fsGeneralEntry_getById(idsDestinoOutDesglose[0]).getDataValue();

        var idsUnidadOrganizativa = $(arrInputs).filter(function (index, element) { return (fsGeneralEntry_getById(element) !== null && fsGeneralEntry_getById(element).idCampoPadre == null && fsGeneralEntry_getById(element).tipoGS == 121) });
        if (idsUnidadOrganizativa.length == 0) codUnidadOrganizativa = '';
        else codUnidadOrganizativa = fsGeneralEntry_getById(idsUnidadOrganizativa[0]).getDataValue();

        var idsCentroCoste = $(arrInputs).filter(function (index, element) { return (fsGeneralEntry_getById(element) !== null && fsGeneralEntry_getById(element).idCampoPadre == null && fsGeneralEntry_getById(element).tipoGS == 129) });
        if (idsCentroCoste.length == 0) codCentroCoste = '';
        else codCentroCoste = fsGeneralEntry_getById(idsCentroCoste[0]).getDataValue();

        var idSolicitud = 0;
        var idInstancia = 0;
        if (document.getElementById("Solicitud")) idSolicitud = parseInt(document.getElementById("Solicitud").value);
        if (document.getElementById("Instancia")) idInstancia = parseInt(document.getElementById("Instancia").value);
        if (idInstancia == 0 && document.getElementById("NEW_Instancia") && document.getElementById("NEW_Instancia").value !== '') idInstancia = parseInt(document.getElementById("NEW_Instancia").value);

        var idsPartida = $(arrInputs).filter(function (index, element) { return (fsGeneralEntry_getById(element) !== null && fsGeneralEntry_getById(element).idCampoPadre == null && fsGeneralEntry_getById(element).tipoGS == 130) });
        if (idsPartida.length == 0) {
            codPartida0 = '';
            codPartida = '';
        } else {
            codPartida0 = fsGeneralEntry_getById(idsPartida[0]).PRES5;
            codPartida = fsGeneralEntry_getById(idsPartida[0]).getDataValue();
        }

        var params = JSON.stringify({ idCampoDesglose: idCampoDesglose, orgCompras: codOrgCompras, centro: codCentro, destino: codDestino, favorito: favorito, idSolicitud: idSolicitud, idInstancia: idInstancia, UnidadOrganizativa: codUnidadOrganizativa, CentroCoste: codCentroCoste, Partida0: codPartida0, Partida: codPartida });
        
        $.when($.ajax({
            type: "POST",
            url: rutaPM + 'UtilesDesglose.asmx/GenerarExcelDesglose',
            contentType: "application/json; charset=utf-8",
            data: params,
            dataType: "json",
            async: true
        })).done(function (filename) {            
            OcultarCargando();
            window.open(rutaPM + 'Files.ashx?file=' + filename.d, '_blank');
        });
        return false;
    };
    function CargarExcelDesglose(idCampoDesglose) {
        var sMoneda="";
        var oEntry = buscarCampoEnFormulario(102); //busca el campo de tipo moneda
        if (oEntry) {
            if (oEntry.getDataValue()) {
                sMoneda = oEntry.getDataValue();
            } else {
                alert(sMensajeMonedaObligatoria);
            }
        } else {
            sMoneda="OK"
        }
   
        if (sMoneda != "") {
            //Al pulsar el boton de cargar excel, lanzaremos el evento click del input type file
            //para buscar el fichero excel a cargar. Limitamos la busqueda a ficheros excel           
            $('[idCampoDesglose=' + idCampoDesglose + ']').click();
        }
        return false;
    };
    function CargarLineasDesglose(resultado, nombreDesgloseCargar, idCampoDesglose) {
        period = 50;
        var k = 0;
        window.requestAnimationFrame(function loop() {
            anyadirFilaDesgloseConDatos(nombreDesgloseCargar, idCampoDesglose, false, resultado.valoresLinea[k], false);
            if (++k <= resultado.valoresLinea.length - 1) window.requestAnimationFrame(loop);
            else { agregandoDesdeExcel = false; cargandoLineas = false; GestionarCargando(); };
        });
    }
    $('[idCampoDesglose]').live('change', function () {
        var idCampoDesglose = $(this).attr('idCampoDesglose');
        var nombreDesgloseCargar = $(this).attr('nombreDesglose');
        //En cuanto seleccione un excel salta este evento (no hay otra forma de controlar la seleccion de un archivo)
        //Lo subimos a servidor mediante el controlador de eventos Files.ashx y despues llamamos al servicio que lo
        //leera y cargara
        var fichero = $('[idCampoDesglose=' + idCampoDesglose + ']')[0].files[0];
        if (fichero == null) return false;
        //Vaciamos el valor del input type file por si se vuelve a llamarlo cogiendo el mismo fichero
        $('[idCampoDesglose=' + idCampoDesglose + ']')[0].value = '';
        MostrarCargando();
        var formData = new FormData();
        formData.append("files", fichero);
        //Guardo ficher seleccionado
        $.when($.ajax({
            url: rutaPM + "Files.ashx", //web service
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })).done(function (filename) {
            var idUnidadArticulo;
            var idsUnidadArticulo = $(arrInputs).filter(function (index, element) { return (fsGeneralEntry_getById(element) !== null && fsGeneralEntry_getById(element).idCampoPadre !== null && fsGeneralEntry_getById(element).tipoGS == 105) });
            if (idsUnidadArticulo.length == 0) idUnidadArticulo = 0;
            else idUnidadArticulo = fsGeneralEntry_getById(idsUnidadArticulo[0]).idCampo;            

            //
            //Partida
            //
            var sCentroCostePartida;            
            var idsCentroCostePartida = $(arrInputs).filter(function (index, element) { return (fsGeneralEntry_getById(element) !== null && fsGeneralEntry_getById(element).idCampoPadre == null && fsGeneralEntry_getById(element).tipoGS == 129) });
            if (idsCentroCostePartida.length == 0) sCentroCostePartida = '';
            else {
                //idsCentroCostePartida tiene los N centros de los N grupos. Hay q identificar cual es realmente el de tu desglose.
                var sBuscoCentrodePartida;
                var idsPartida = $(arrInputs).filter(function (index, element) { return (fsGeneralEntry_getById(element) !== null && fsGeneralEntry_getById(element).idCampoPadre == idCampoDesglose && fsGeneralEntry_getById(element).tipoGS == 130) })
                if (idsPartida.length == 0) sCentroCostePartida = '';
                else {
                    sBuscoCentrodePartida = fsGeneralEntry_getById(idsPartida[0]).idEntryCC
                    var indice;
                    for (indice = 0; indice < idsCentroCostePartida.length; indice++) {
                        if (fsGeneralEntry_getById(idsCentroCostePartida[indice]).idCampo == sBuscoCentrodePartida) { //Solicitud
                            if (fsGeneralEntry_getById(idsCentroCostePartida[indice]).getValue() == "")
                                sCentroCostePartida = '';
                            else
                                sCentroCostePartida = fsGeneralEntry_getById(idsCentroCostePartida[indice]).getDataValue(); 
                        } else { 
                            if (fsGeneralEntry_getById(idsCentroCostePartida[indice]).campo_origen == sBuscoCentrodePartida) { //Instancia
                                if (fsGeneralEntry_getById(idsCentroCostePartida[indice]).getValue() == "")
                                    sCentroCostePartida = '';
                                else
                                    sCentroCostePartida = fsGeneralEntry_getById(idsCentroCostePartida[indice]).getDataValue(); 
                            }
                        }
                    }
                }
            }

            //
            //AnyoImputacion
            //
            var sPartida0AnyoImputacion;
            var sPartidaAnyoImputacion;       
            var sCentroCosteAnyoImputacion; 
            var idsPartidaAnyoImputacion = $(arrInputs).filter(function (index, element) { return (fsGeneralEntry_getById(element) !== null && fsGeneralEntry_getById(element).idCampoPadre == null && fsGeneralEntry_getById(element).tipoGS == 130) });
            if (idsPartidaAnyoImputacion.length == 0) {
                //No hay Partida en campo -> lo cogera del desglose (el centro lo tendras en sCentroCostePartida o '')
                sPartida0AnyoImputacion = '';
                sPartidaAnyoImputacion = '';
                sCentroCosteAnyoImputacion = sCentroCostePartida;
            } else {
                //idsPartidaAnyoImputacion tiene las N partidas de los N grupos. Hay q identificar cual es realmente el de tu desglose.
                var sBuscoPartidadeAnyo;
                var idsAnyo = $(arrInputs).filter(function (index, element) { return (fsGeneralEntry_getById(element) !== null && fsGeneralEntry_getById(element).idCampoPadre == idCampoDesglose && fsGeneralEntry_getById(element).tipoGS == 151) })
                if (idsAnyo.length == 0) {
                    //No hay AnyoImputacion en desglose. Nada q hacer.
                    sPartida0AnyoImputacion = '';
                    sPartidaAnyoImputacion = '';
                    sCentroCosteAnyoImputacion = '';
                } else {
                    if (fsGeneralEntry_getById(idsAnyo[0]).EntryPartidaPlurianualCampo == 0) {
                        //La partida del AnyoImputacion No esta en campo -> lo cogera del desglose (el centro lo tendras en sCentroCostePartida o '')
                        sPartida0AnyoImputacion = '';
                        sPartidaAnyoImputacion = '';
                        sCentroCosteAnyoImputacion = sCentroCostePartida;
                    } else {
                        sBuscoPartidadeAnyo = fsGeneralEntry_getById(idsAnyo[0]).IdEntryPartidaPlurianual
                        var indice;
                        for (indice = 0; indice < idsPartidaAnyoImputacion.length; indice++) {
                            if (fsGeneralEntry_getById(idsPartidaAnyoImputacion[indice]).id == sBuscoPartidadeAnyo) { //Solicitud //Instancia
                                if (fsGeneralEntry_getById(idsPartidaAnyoImputacion[indice]).getValue() == "") {
                                    sPartida0AnyoImputacion = '';
                                    sPartidaAnyoImputacion = '';
                                } else {
                                    sPartida0AnyoImputacion = fsGeneralEntry_getById(idsPartidaAnyoImputacion[indice]).PRES5; 
                                    sPartidaAnyoImputacion = fsGeneralEntry_getById(idsPartidaAnyoImputacion[indice]).getDataValue(); 
                                }

                                //Obtener sCentroCosteAnyoImputacion
                                //      Tienes en idsCentroCostePartida los centro coste a nivel campo
                                if (idsCentroCostePartida.length == 0) sCentroCosteAnyoImputacion = '';
                                else {
                                    //idsCentroCostePartida tiene los N centros de los N grupos. Hay q identificar cual es realmente el de tu desglose.
                                    var sBuscoCentrodeAnyoImputacion;
                                    sBuscoCentrodeAnyoImputacion = fsGeneralEntry_getById(idsPartidaAnyoImputacion[indice]).idEntryCC;
                                    var indice2do;
                                    for (indice2do = 0; indice2do < idsCentroCostePartida.length; indice2do++) {
                                        if (fsGeneralEntry_getById(idsCentroCostePartida[indice2do]).id == sBuscoCentrodeAnyoImputacion) { //Solicitud  //Instancia
                                            if (fsGeneralEntry_getById(idsCentroCostePartida[indice2do]).getValue() == "")
                                                sCentroCosteAnyoImputacion = '';
                                            else
                                                sCentroCosteAnyoImputacion = fsGeneralEntry_getById(idsCentroCostePartida[indice2do]).getDataValue(); 
                                        } 
                                    }
                                }
                            } 
                        }
                    }
                }
            }

            $.when($.ajax({
                type: "POST",
                url: rutaPM + 'UtilesDesglose.asmx/CargarExcelDesglose',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ idCampoDesglose: idCampoDesglose, fileName: filename, idCampoUnidadArticulo: idUnidadArticulo, idSolicitudActual: idSolicitudActual, sCentroCostePartida: sCentroCostePartida, sPartida0AnyoImputacion: sPartida0AnyoImputacion, sPartidaAnyoImputacion: sPartidaAnyoImputacion, sCentroCosteAnyoImputacion: sCentroCosteAnyoImputacion }),
                async: false
            })).done(function (data) {
                var resultado = $.parseJSON(data.d);

                if (parseInt(resultado.error) == 0) {
                    $('.tablaDatos tbody').empty(); //borramos todas las filas menos la primera que es la cabecera
                    $('.tablaDatos th:nth-child(1)').html(TextosPantalla[0]);
                    $('.tablaDatos th:nth-child(2)').html(TextosPantalla[1]);
                    $('.tablaDatos th:nth-child(3)').html(TextosPantalla[2]);
                    $('#lblImprimir').text(TextosPantalla[4]);
                    $('#lblTituloPopUpError').text(TextosPantalla[5]);
                    $('#lblTituloPopUpError').text(TextosPantalla[6]);

                    cargandoLineas = true;
                    agregandoDesdeExcel = true;
                    CargarLineasDesglose(resultado, nombreDesgloseCargar, idCampoDesglose);
                    if (resultado.errores.length > 0) {
                        MostrandoError = true;
                        $('#lineaErrorDesglose').tmpl(resultado.errores).appendTo($('.tablaDatos tbody'));
                        $('#pnlErroresImportacionExcel').show();
                    }
                } else {
                    alert(TextosErrorExportacionExcelDesglose[parseInt(resultado.error)]);
                    GestionarCargando();
                }
            });
        });
    });
    $('#btnCerrarPanelErrores').live('click', function () {
        $('#pnlErroresImportacionExcel').hide();
        MostrandoError = false;
        GestionarCargando();
    });
    $('#btnImprimir').live('click', function () {
        var divToPrint = document.getElementById('pnlErroresImportacionExcel');
        var newWin = window.open('', 'Print-Window', 'width=100,height=100');
        newWin.document.open();
        newWin.document.write('<link href='+ruta +'App_Themes/' + theme + '/' + theme + '.css" rel="stylesheet" type="text/css"></link><link href='+ruta+'App_Themes/' + theme + '/cn_Styles.css" rel="stylesheet" type="text/css"></link><body onload="window.print()">' + divToPrint.innerHTML + '</body>');
        $('#btnCerrarPanelErrores', newWin.document).css('display', 'none');
        $('#btnImprimir', newWin.document).css('display', 'none');
        newWin.document.close();
        setTimeout(function () { newWin.close(); }, 10);
    });
    $('[id$=tblGeneral]').on('click','img.Image12', function (e) {
        if (e.handled !== true) {
            window.open(rutaPM + 'alta/AyudaDesglose.aspx', '_blank', 'width=600,height=400,status=no,top=200,left=300,resizable=yes,scrollbars=yes');
            e.handled = true;
        }
    });
    function GestionarCargando() {
        if (!cargandoLineas && !MostrandoError) OcultarCargando();
    };
</script>
<table id="tblGeneral" runat="server" width="100%">
    <tr>
        <td valign="top" colspan="2">
            <div id="divDesglose" style="width: 730px; overflow: auto; clip: rect(0px, auto, auto, 0px); padding-bottom: 20px; min-height: 0%;" runat="server">
                <table id="tblTituloDesglose" runat="server" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table id="tblDesglose" runat="server" class="bordeDesglose" border="1" cellpadding="4"></table>                                                            
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td>            
            <input runat="server" id="cmdAnyadir" class="botonPMWEB" style="width: 120px; height: 24px; cursor:pointer;" type="button" name="cmdAnyadir" />
        </td> 
        <td>
            <table>
                <tr>
                    <td class="SeparadorLeft" style="cursor:default;">
                        <asp:Label runat="server" ID="lblInfoExcel" CssClass="TextoResaltado" style="line-height:24px; margin:0em 1em;"></asp:Label>
                        <img alt="" class="Image12" style="vertical-align:middle; cursor:pointer;" />
                        <input runat="server" id="cmdGenerarExcelDesglose" class="botonPMWEB" style="height:24px; cursor:pointer;" type="button" />
                        <input runat="server" id="cmdCargarExcelDesglose" class="botonPMWEB" style="height:24px; cursor:pointer;" type="button" /> 
                    </td>
                </tr>
            </table>                       
        </td>
    </tr>
</table>
<input type="file" runat="server" id="fuDesglose" style="display:none;" accept=".xls" />
<input runat="server" id="cmdSubmit" style="display:none;" type="submit" name="cmdSubmit"  />
<asp:Button runat="server" ID="btnCargarExcelDesglose" style="display:none;" />
<div id="popupFondoDesglose" class="FondoModalTrasparente" style="display: none;"></div>
<div id="pnlCargandoDesglose" class="updateProgressPM">
    <div style="position: relative; top: 30%; text-align: center;">
        <img src="../images/cargando.gif" style="vertical-align:middle;" />
        <asp:Label runat="server" ID="lblCargandoDesglose" style="color:Black; margin-left:0.5em;"></asp:Label>
    </div>            
</div>
<div style="overflow: hidden; width: 0px; position: absolute; top: 0px; height: 0px">
    <table id="tblDesgloseHidden" style="visibility: hidden" runat="server"></table>
</div>
<div id="pnlErroresImportacionExcel" class="popupCN" style="display:none; position: fixed; z-index:1003; top:50%; left:50%; transform:translate(-50%, -50%); -webkit-transform:translate(-50%, -50%); width:70%;">
    <img alt="" src="../images/alert-rojo.png" style="position:absolute; top:0.5em; left:0.5em;" />
    <img alt="" id="btnCerrarPanelErrores" src="../images/Bt_Cerrar.png" style="position:absolute; top:0.5em; right:0.5em; cursor:pointer;" />
    <div id="btnImprimir" style="position:absolute; right:4em; top:0.7em; cursor:pointer;">
        <img alt="" src="../images/imprimirph.gif" style="vertical-align:middle;" />
        <span id="lblImprimir" class="TextoClaro"></span>
    </div>
    <div class="CapaTituloPanelInfo" style="height:3em; line-height:3em;">
        <span id="lblTituloPopUpError" class="TituloPopUpPanelInfo" style="margin-left:60px;"></span>
    </div>
    <div class="CapaSubTituloPanelInfo" style="height:2.5em; line-height:2.5em;">
        <span id="lblDescripcionPopUpError" class="SubTituloPopUpPanelInfo" style="margin-left:60px;"></span>
    </div>  
    <div style="padding:0.5em; max-height:60vh; overflow:auto;">
        <table class="tablaDatos" style="width:100%;">
            <thead>
                <tr>
                    <th style="width:7em;"></th>
                    <th style="width:25%;"></th>
                    <th></th>
                </tr>  
            </thead>
            <tbody></tbody>         
        </table>
    </div>      
</div>
<script id="lineaErrorDesglose" type="text/x-jquery-tmpl">
    <tr>
        <td style='text-align:right; padding-right:0.2em;'>${Key}</td>
        <td>${Value.Key}</td>
        <td class='TextoResaltado'>${TextosPantalla[3].replace('###',Value.Key) + ': ' + Value.Value}</td>
    </tr>
</script>
