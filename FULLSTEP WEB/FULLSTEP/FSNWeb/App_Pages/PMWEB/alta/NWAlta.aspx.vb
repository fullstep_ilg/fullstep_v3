Public Class NWAlta
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Style1 = New Infragistics.WebUI.UltraWebNavigator.Style

    End Sub
    Protected WithEvents uwtGrupos As Infragistics.WebUI.UltraWebTab.UltraWebTab
    Protected WithEvents tblCabecera As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents Solicitud As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblCamposObligatorios As System.Web.UI.WebControls.Label
    Protected WithEvents uwPopUpAcciones As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
    Protected WithEvents Bloque As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Style1 As Infragistics.WebUI.UltraWebNavigator.Style
    Protected WithEvents cadenaespera As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents BotonCalcular As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents PantallaMaper As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents PantallaVinculaciones As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Favorito As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents IdFavorito As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents IdInstanciaImportar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents frmAlta As System.Web.UI.HtmlControls.HtmlForm
    Protected WithEvents divForm1 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divForm2 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divForm3 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divProgreso As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblProgreso As System.Web.UI.WebControls.TextBox
    Protected WithEvents imgProgreso As System.Web.UI.WebControls.Image
    Protected WithEvents tblProgreso As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents NEW_Instancia As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents denFavorita As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents hid_DesdeInicio As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents controlMenu As MenuControl
    Protected WithEvents FSNPageHeader As Global.Fullstep.FSNWebControls.FSNPageHeader
#End Region
#Region "Properties"
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
    Private oBloque As FSNServer.Bloque
    Private _oRol As FSNServer.Rol
    ''' <summary>
    ''' Obtenemos los datos del rol
    ''' A continuacion lo metemos en la cache
    ''' </summary>
    ''' <remarks></remarks>
    Protected ReadOnly Property oRol() As FSNServer.Rol
        Get
            If _oRol Is Nothing Then
                If IsPostBack Then
                    _oRol = CType(Cache("oRol" & FSNUser.Cod), FSNServer.Rol)
                Else
                    For Each _oRol In oBloque.Roles.Roles
                        If _oRol.Participantes Is Nothing Then
                            If _oRol.ExistePersona(FSNUser.UON1, FSNUser.UON2, FSNUser.UON3, FSNUser.Dep, FSNUser.CodPersona) Then Exit For
                        Else
                            If _oRol.Persona = FSNUser.CodPersona Then Exit For
                        End If
                    Next

                    Me.InsertarEnCache("oRol" & FSNUser.Cod, _oRol)
                End If
            End If
            Return _oRol
        End Get
    End Property
    Private _oSolicitud As FSNServer.Solicitud
    Protected ReadOnly Property oSolicitud() As FSNServer.Solicitud
        Get
            If _oSolicitud Is Nothing Then
                If IsPostBack Then
                    _oSolicitud = CType(Cache("oSolicitud" & FSNUser.Cod), FSNServer.Solicitud)
                Else
                    _oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
                    _oSolicitud.ID = Request("Solicitud")

                    'Carga los datos de la instancia:
                    _oSolicitud.Load(Idioma, True)

                    Dim bFavorito As Boolean = IIf((Request("Favorito")) = "1", True, False)
                    Dim lFavorito As Long = 0
                    Dim lIdInstanciaImportar As Long
                    If Not String.IsNullOrEmpty(Request("IdFavorito")) Then lFavorito = CDbl(Request("IdFavorito"))
                    If Not Request("IdInstanciaImportar") Is Nothing Then lIdInstanciaImportar = Request("IdInstanciaImportar")
                    IdFavorito.Value = lFavorito
                    IdInstanciaImportar.Value = InstanciaImportar

                    _oSolicitud.Formulario.Load(Idioma, _oSolicitud.ID, _oSolicitud.BloqueActual, FSNUser.CodPersona, bFavorito, lFavorito, FSNUser.DateFmt, , InstanciaImportar)

                    InsertarEnCache("oSolicitud" & FSNUser.Cod, _oSolicitud)
                End If
            End If
            Return _oSolicitud
        End Get
    End Property
    Public ReadOnly Property InstanciaImportar() As Long
        Get
            If Not Request("IdInstanciaImportar") Is Nothing Then
                Return Request("IdInstanciaImportar")
            Else
                Return 0
            End If
        End Get
    End Property
#End Region
#Region "Page Events"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
    ''' Revisado por: blp. Fecha: 05/10/2011
    ''' <summary>
    ''' Carga la pagina de alta de solicitud
    ''' </summary>
    ''' <param name="sender">La p�gina</param>
    ''' <param name="e">las del evento</param>        
    ''' <remarks>Llamada desde el evento de carga de la p�gina; Tiempo m�ximo=0,3seg.</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.Expires = -1

        If Page.IsPostBack Then
            CargarCamposGrupo()
            Exit Sub
        End If

        CargarTextoEspera()

        ModuloIdioma = TiposDeDatos.ModulosIdiomas.AltaSolicitudes
        ConfigurarCabeceraMenu()

        If Not ClientScript.IsStartupScriptRegistered("oRol_Id") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "oRol_Id", "var oRol_Id=" & oRol.Id & ";", True)
        If Not ClientScript.IsStartupScriptRegistered("tipoSolicitud") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "tipoSolicitud", "<script>var tipoSolicitud = '" & CInt(oSolicitud.TipoSolicit) & "';</script>")
        End If

        Dim oucParticipantes As participantes
        Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
        Dim sScript As String
        Dim sClientTextVars As String
        sClientTextVars = ""
        sClientTextVars += "vdecimalfmt='" + FSNUser.DecimalFmt + "';"
        sClientTextVars += "vthousanfmt='" + FSNUser.ThousanFmt + "';"
        sClientTextVars += "vprecisionfmt='" + FSNUser.PrecisionFmt + "';"
        sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
        If Not ClientScript.IsStartupScriptRegistered("VarsUser") Then _
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsUser", sClientTextVars)

        lblCamposObligatorios.Text = Textos(23)
        hid_DesdeInicio.Value = Request("DesdeInicio")
        Select Case oSolicitud.TipoSolicit
            Case TiposDeDatos.TipoDeSolicitud.Factura
                controlMenu.OpcionMenu = "Facturacion"
                controlMenu.OpcionSubMenu = "Alta"
            Case TiposDeDatos.TipoDeSolicitud.Encuesta
                controlMenu.OpcionMenu = "Calidad"
                controlMenu.OpcionSubMenu = "Encuestas"
            Case TiposDeDatos.TipoDeSolicitud.SolicitudQA
                controlMenu.OpcionMenu = "Calidad"
                controlMenu.OpcionSubMenu = "SolicitudesQA"
            Case Else
                controlMenu.OpcionMenu = "Procesos"
                controlMenu.OpcionSubMenu = "AltaSolicitudes"
        End Select

        Solicitud.Value = oSolicitud.ID
        Bloque.Value = oSolicitud.BloqueActual

        oRol.CargarParticipantes(Idioma, lFavorita:=IdFavorito.Value)

        CargarCamposGrupo()

        If oRol.Participantes.Tables(0).Rows.Count > 0 Then
            oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
            oTabItem.Text = Textos(8) 'Participantes
            oTabItem.Key = "PARTICIPANTES"

            uwtGrupos.Tabs.Add(oTabItem)

            oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
            oTabItem.ContentPane.UserControlUrl = "participantes.ascx"

            oucParticipantes = oTabItem.ContentPane.UserControl
            oucParticipantes.Idi = Idioma
            oucParticipantes.TabContainer = uwtGrupos.ClientID
            oucParticipantes.dsParticipantes = oRol.Participantes

            oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden
            oInputHidden.ID = "txtPre_PARTICIPANTES"
            oInputHidden.Value = oucParticipantes.ClientID 'oucParticipantes.ClientID
            Me.FindControl("frmAlta").Controls.Add(oInputHidden)
        End If

        PantallaMaper.Value = oSolicitud.ValidacionesIntegracion.ControlaConMaper()
        Favorito.Value = Request("Favorito")
        FSNPageHeader.VisibleBotonImportar = True
        PantallaVinculaciones.Value = oSolicitud.ExisteVinculaciones

        Dim sClientTexts As String
        sClientTexts = ""
        sClientTexts += "var arrTextosML = new Array();"
        sClientTexts += "arrTextosML[0] = '" + JSText(Textos(5)) + "';"
        sClientTexts += "arrTextosML[1] = '" + JSText(Textos(10)) + "';"
        sClientTexts += "arrTextosML[2] = '" + JSText(Textos(21)) + "';"
        sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)

        If Not Page.ClientScript.IsClientScriptBlockRegistered("varFila") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varFila", "<script>var sFila = '" & JSText(Textos(22)) & "' ;</script>")
        If Not Page.ClientScript.IsStartupScriptRegistered("TextosMICliente") Then _
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)
        If Not Page.ClientScript.IsClientScriptBlockRegistered("rutaFS") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' ;</script>")
        If Not Page.ClientScript.IsClientScriptBlockRegistered("rutaPM") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' ;</script>")
        If Not Page.ClientScript.IsClientScriptBlockRegistered("rutaFS") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", "<script>var rutaFS = '" & ConfigurationManager.AppSettings("rutaFS") & "' ;</script>")
        If Not Page.ClientScript.IsClientScriptBlockRegistered("mensajeCambioTipoPedido") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "mensajeCambioTipoPedido", "<script>var mensajeCambioTipoPedido = '" & JSText(Textos(20)) & "' ;</script>")

        sScript = "var ilGMN1 = " + FSNServer.LongitudesDeCodigos.giLongCodGMN1.ToString() + ";"
        sScript += "var ilGMN2 = " + FSNServer.LongitudesDeCodigos.giLongCodGMN2.ToString() + ";"
        sScript += "var ilGMN3 = " + FSNServer.LongitudesDeCodigos.giLongCodGMN3.ToString() + ";"
        sScript += "var ilGMN4 = " + FSNServer.LongitudesDeCodigos.giLongCodGMN4.ToString() + ";"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        If Not Page.ClientScript.IsStartupScriptRegistered("LongitudesCodigosKey") Then _
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "LongitudesCodigosKey", sScript)

        If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayDesgloses") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")

        Me.FindControl("frmAlta").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())
    End Sub
#End Region
    Public oFile As System.IO.StreamWriter
    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
    Private Sub CargarCamposGrupo()
        Dim oGrupo As FSNServer.Grupo
        Dim oucCampos As campos
        Dim oucDesglose As desgloseControl
        Dim oRow As DataRow = Nothing
        Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab

        uwtGrupos.Tabs.Clear()
        Dim lIndex As Long = 0
        AplicarEstilosTab(uwtGrupos)

        For Each oGrupo In oSolicitud.Formulario.Grupos.Grupos
            oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
            Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
            Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
            oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(Idioma), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
            oTabItem.Key = lIndex
            uwtGrupos.Tabs.Add(oTabItem)

            oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden

            oInputHidden.ID = "txtPre_" + lIndex.ToString
            lIndex += 1

            If oGrupo.NumCampos <= 1 Then
                For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                    If oRow.Item("VISIBLE") = 1 Then
                        Exit For
                    End If
                Next
                If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Desglose Or (DBNullToSomething(oRow.Item("SUBTIPO")) = TiposDeDatos.TipoGeneral.TipoDesglose And DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.DesgloseActividad) Then
                    If oRow.Item("VISIBLE") = 0 Then
                        uwtGrupos.Tabs.Remove(oTabItem)
                    Else
                        oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                        oTabItem.ContentPane.UserControlUrl = "desglose.ascx"
                        oucDesglose = oTabItem.ContentPane.UserControl
                        oucDesglose.ID = oGrupo.Id.ToString
                        oucDesglose.Campo = oRow.Item("ID")
                        oucDesglose.TabContainer = uwtGrupos.ClientID
                        oucDesglose.SoloLectura = (oRow.Item("ESCRITURA") = 0)
                        oucDesglose.Solicitud = oSolicitud.ID
                        oucDesglose.TieneIdCampo = False
                        oucDesglose.Ayuda = DBNullToSomething(oRow.Item("AYUDA_" & Idioma))
                        oucDesglose.PM = True
                        oucDesglose.Titulo = DBNullToSomething(oRow.Item("DEN_" & Idioma))
                        oucDesglose.TipoSolicitud = oSolicitud.TipoSolicit
                        oInputHidden.Value = oucDesglose.ClientID
                    End If
                Else
                    oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                    oTabItem.ContentPane.UserControlUrl = "campos.ascx"
                    oucCampos = oTabItem.ContentPane.UserControl
                    oucCampos.ID = oGrupo.Id.ToString
                    oucCampos.dsCampos = oGrupo.DSCampos
                    oucCampos.IdGrupo = oGrupo.Id
                    oucCampos.Idi = Idioma
                    oucCampos.TabContainer = uwtGrupos.ClientID
                    oucCampos.Solicitud = oSolicitud.ID
                    oucCampos.TipoSolicitud = oSolicitud.TipoSolicit
                    oucCampos.PM = True
                    oucCampos.Formulario = oSolicitud.Formulario
                    oInputHidden.Value = oucCampos.ClientID
                End If
            Else
                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                oTabItem.ContentPane.UserControlUrl = "campos.ascx"
                oucCampos = oTabItem.ContentPane.UserControl
                oucCampos.ID = oGrupo.Id.ToString
                oucCampos.dsCampos = oGrupo.DSCampos
                oucCampos.IdGrupo = oGrupo.Id
                oucCampos.Idi = Idioma
                oucCampos.TabContainer = uwtGrupos.ClientID
                oucCampos.Solicitud = oSolicitud.ID
                oucCampos.TipoSolicitud = oSolicitud.TipoSolicit
                oucCampos.PM = True
                oucCampos.Formulario = oSolicitud.Formulario
                oInputHidden.Value = oucCampos.ClientID
            End If

            Me.FindControl("frmAlta").Controls.Add(oInputHidden)
        Next
    End Sub
    ''' Revisado por: sra. Fecha: 24/01/2012
    ''' <summary>
    ''' Configura la cabecera, muestra en el menu las posibles acciones
    ''' Activamos los botones que pueden aparecer en la pantalla
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load; Tiempo m�ximo:=0,1seg.</remarks>
    Private Sub ConfigurarCabeceraMenu()
        Dim tituloCab As String = String.Empty
        Select Case oSolicitud.TipoSolicit
            Case TiposDeDatos.TipoDeSolicitud.Factura
                tituloCab = Textos(26)
            Case TiposDeDatos.TipoDeSolicitud.Encuesta
                tituloCab = Textos(27)
            Case TiposDeDatos.TipoDeSolicitud.SolicitudQA
                tituloCab = Textos(28)
            Case Else
                tituloCab = Textos(19)
        End Select
        FSNPageHeader.TituloCabecera = tituloCab & "/" & oSolicitud.Den(Idioma)
        FSNPageHeader.UrlImagenCabecera = "images/SolicitudesPMSmall.jpg"

        FSNPageHeader.TextoBotonVolver = Textos(24)
        FSNPageHeader.VisibleBotonVolver = True
        FSNPageHeader.OnClientClickVolver = "javascript:HistoryHaciaAtras();return false;"

        FSNPageHeader.TextoBotonGuardar = Textos(3)
        FSNPageHeader.VisibleBotonGuardar = True
        FSNPageHeader.OnClientClickGuardar = "Guardar(); return false;"

        FSNPageHeader.TextoBotonCalcular = Textos(11)
        FSNPageHeader.VisibleBotonCalcular = False
        FSNPageHeader.OnClientClickCalcular = "return CalcularCamposCalculados(); return false;"

        FSNPageHeader.TextoBotonEnviarAFavoritos = Textos(16)
        FSNPageHeader.VisibleBotonEnviarAFavoritos = True
        FSNPageHeader.OnClientClickEnviarFavoritos = "return EnviarFavoritos(); return false;"

        FSNPageHeader.TextoBotonImportar = Textos(25)
        FSNPageHeader.VisibleBotonImportar = True
        FSNPageHeader.OnClientClickImportar = "return ImportarInstancia()"

        'SI EXISTE ALGUN CAMPO DE TIPO=3 (CALCULADO) HACEMOS VISIBLE EL BOTON DE CALCULAR
        FSNPageHeader.VisibleBotonCalcular = oSolicitud.Formulario.Grupos.Grupos.OfType(Of FSNServer.Grupo).Where(Function(x) x.DSCampos.Tables(0).Rows.OfType(Of DataRow).Where(Function(y) y("TIPO") = TipoCampoPredefinido.Calculado).Any).Any()
        If FSNPageHeader.VisibleBotonCalcular Then BotonCalcular.Value = 1

        'Cargar las acciones
        oBloque = FSNServer.Get_Object(GetType(FSNServer.Bloque))
        oBloque.CargarRolesBloque(oSolicitud.BloqueActual, FSNUser.CodPersona)

        Dim oDSAcciones As DataSet = oRol.CargarAcciones(Idioma)
        Dim oPopMenu As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
        Dim oItem As Infragistics.WebUI.UltraWebNavigator.Item
        Dim oRow As DataRow

        oPopMenu = Me.uwPopUpAcciones
        oPopMenu.Visible = False
        If oDSAcciones.Tables.Count > 0 Then
            If oDSAcciones.Tables(0).Rows.Count > 3 Then
                oPopMenu.Visible = True
                FSNPageHeader.VisibleBotonAccion1 = True
                FSNPageHeader.TextoBotonAccion1 = Textos(9) ' "Otras acciones"
                FSNPageHeader.OnClientClickAccion1 = "return mostrarMenuAcciones(event,1);"

                For Each oRow In oDSAcciones.Tables(0).Rows
                    If IsDBNull(oRow.Item("DEN")) Then
                        oItem = oPopMenu.Items.Add("&nbsp;")
                    Else
                        If oRow.Item("DEN") = "" Then
                            oItem = oPopMenu.Items.Add("&nbsp;")
                        Else
                            oItem = oPopMenu.Items.Add(DBNullToStr(oRow.Item("DEN")))
                        End If
                    End If
                    oItem.TargetUrl = "javascript:EjecutarAccion(" + oRow.Item("ACCION").ToString() + "," + oBloque.Id.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "');"
                Next
            Else
                Dim cont As Byte = 0
                For Each oRow In oDSAcciones.Tables(0).Rows
                    If cont = 0 Then
                        FSNPageHeader.VisibleBotonAccion1 = True
                        FSNPageHeader.TextoBotonAccion1 = DBNullToStr(oRow.Item("DEN"))
                        FSNPageHeader.OnClientClickAccion1 = "javascript:EjecutarAccion(" + oRow.Item("ACCION").ToString() + "," + oBloque.Id.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                        cont = 1
                    ElseIf cont = 1 Then
                        FSNPageHeader.VisibleBotonAccion2 = True
                        FSNPageHeader.TextoBotonAccion2 = DBNullToStr(oRow.Item("DEN"))
                        FSNPageHeader.OnClientClickAccion2 = "javascript:EjecutarAccion(" + oRow.Item("ACCION").ToString() + "," + oBloque.Id.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                        cont = 2
                    Else
                        FSNPageHeader.VisibleBotonAccion3 = True
                        FSNPageHeader.TextoBotonAccion3 = DBNullToStr(oRow.Item("DEN"))
                        FSNPageHeader.OnClientClickAccion3 = "javascript:EjecutarAccion(" + oRow.Item("ACCION").ToString() + "," + oBloque.Id.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                    End If
                Next
            End If
        End If
    End Sub
    ''' <summary>
    ''' Obtiene los textos de la pantalla de espera
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarTextoEspera()
        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Espera
        cadenaespera.Value = Textos(1)
        lblProgreso.Text = Textos(2)
    End Sub
    ''' <summary>
    ''' Funci�n que comprueba si un art�culo tiene adjudicaciones vigentes para un determinado proveedor
    ''' </summary>
    ''' <param name="codArt">cod.Articulo</param>
    ''' <param name="codPro">cod.Proveedor</param>
    ''' <returns>1 si tiene adjudicaciones vigentes, 0 si no las tiene</returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function comprobarAdjudicadoArticulo(ByVal codArt As String, ByVal codPro As String) As String
        Try
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            Dim oArticulos As Fullstep.FSNServer.Articulos = FSNServer.Get_Object(GetType(FSNServer.Articulos))
            'cargamos las �ltimas adjudicaciones para el proveedor en cuesti�n
            Dim ds As DataSet = oArticulos.CargarUltimasAdjudicaciones(sCod:=codArt, Proveedor:=codPro)
            Dim hoy As Date = Today
            For Each dr As DataRow In ds.Tables(0).Rows
                If (dr("FECINI")) < hoy And dr("FECFIN") > hoy Then
                    Return 1
                End If
            Next
            Return 0
        Catch e As Exception
            Throw e
        End Try
    End Function
    ''' <summary>Obtiene las UONs correspondientes a un Centro de Coste</summary>
    ''' <param name="sCodCentroCoste">El c�digo del centro de coste</param>
    ''' <remarks>Llamada desde: buscadorListaExterna.ObtenerUONCentroSM</remarks>
    <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
    Public Shared Function ObtenerUONCentroCoste(ByVal sCodCentroCoste As String) As String
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

        If InStrRev(sCodCentroCoste, "#") >= 0 Then sCodCentroCoste = Right(sCodCentroCoste, Len(sCodCentroCoste) - InStrRev(sCodCentroCoste, "#"))

        'Buscar UONs
        Dim sUONs As String = String.Empty
        Dim oCentrosCoste As FSNServer.CentrosCoste
        oCentrosCoste = FSNServer.Get_Object(GetType(FSNServer.CentrosCoste))
        oCentrosCoste.BuscarCentrosCoste(FSNUser.Cod, FSNUser.Idioma, sCodCentroCoste, True)
        If oCentrosCoste.Data.Tables(0).Rows.Count > 0 Then
            sUONs = oCentrosCoste.Data.Tables(0).Rows(0).Item("DEN").ToString

            If sUONs.IndexOf(" - ") > -1 Then sUONs = sUONs.Substring(0, sUONs.IndexOf(" - "))
        End If

        Return sUONs
    End Function
End Class