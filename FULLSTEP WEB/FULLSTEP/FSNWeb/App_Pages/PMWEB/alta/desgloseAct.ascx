﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="desgloseAct.ascx.vb" Inherits="Fullstep.FSNWeb.desgloseAct" %>
<%@ Register TagPrefix="igtbl" Namespace="Infragistics.WebUI.UltraWebGrid" Assembly="Infragistics.WebUI.UltraWebGrid.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="igtxt" Namespace="Infragistics.WebUI.WebDataInput" Assembly="Infragistics.WebUI.WebDataInput.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>


<script id="scriptPagina" language="javascript" type="text/javascript" >
    var rowGrid;
    var celdaGrid;
    var celdaId;
    var vEliminarTodas;
    var NomGridDesgloseAct = "<%=uwgDesgloseActividad.ClientID%>"
    
    function setInnerText(sourceElement,str)
    {
	    if(document.all)
		    sourceElement.innerText=str;
	    else
		    sourceElement.textContent=str;
    }
    function DevolverSeleccionAsignacionUsuarios(linea, sValor) { 
        var grid = igtbl_getGridById("<%=uwgDesgloseActividad.ClientID%>");
        var Newrow = grid.Rows.getRow(parseInt(linea));
        var codProve;
        var denProve;
        var moneda;
        var denUsuarios="";
        var codUsuarios = "";
        var codCategorias = "";
        var idCategorias = "";
        var costesHora = "";
        var arrDatos;
        var arrProve;
        var arrUsuarios;
        var arrDatosUsuarios;
        var arrLineasComa;
        var arrLineasGuion;

        arrDatos = sValor.split("/"); //Separo Proveedor#Usuarios#string con las lineas a copiar
        if (arrDatos.length > 1) {
            arrProve = arrDatos[0].split("#") //Separo los distintos datos del proveedor
            arrUsuarios = arrDatos[1].split("#") //Separo los distintos usuarios si es que son mas de uno
            arrLineasComa = arrDatos[2].split(",")
            codProve = arrProve[0]
            denProve = arrProve[1]
            moneda = arrProve[2]

            //Cambio la configuracion de edicion del grid
            grid.AllowUpdate = 1;

            var NewCelda = Newrow.getCellFromKey("STATUS");

            if (NewCelda.getValue() == null || NewCelda.getValue() == 'null' || NewCelda.getValue() == 'Null' || NewCelda.getValue() == '')
                NewCelda.setValue('M')

            var CeldaProve = Newrow.getCellFromKey("CODPROVE");
            CeldaProve.setValue(codProve)

            var CeldaDenProve = Newrow.getCellFromKey("DENPROVE");
            CeldaDenProve.setValue(denProve)

            var CeldaMon = Newrow.getCellFromKey("MON");
            CeldaMon.setValue(moneda)

            //recojo los usuarios
            var x = 0;
            var i = 0;
            denUsuarios = '';
            codUsuarios = '';
            codCategorias = '';
            idCategorias = '';
            costesHora = '';
            for (x = 0; x <= arrUsuarios.length - 1; x++) {
                arrDatosUsuarios = arrUsuarios[x].split("_")

                denUsuarios = denUsuarios + arrDatosUsuarios[0] + ";"
                codUsuarios = codUsuarios + arrDatosUsuarios[1] + "#"
                codCategorias = codCategorias + arrDatosUsuarios[2] + "#"
                idCategorias = idCategorias + arrDatosUsuarios[3] + "#"
                costesHora = costesHora + arrDatosUsuarios[4] + "#"
            }
            //le quito el ultimo caracter que sobra
            denUsuarios = denUsuarios.substring(0, denUsuarios.length - 1)
            codUsuarios = codUsuarios.substring(0, codUsuarios.length - 1)
            codCategorias = codCategorias.substring(0, codCategorias.length - 1)
            idCategorias = idCategorias.substring(0, idCategorias.length - 1)
            costesHora = costesHora.substring(0, costesHora.length - 1)


            var CeldaCon = Newrow.getCellFromKey("CODCONTACTOS"); //Codigo de usuarios, columna no viisble
            CeldaCon.setValue(codUsuarios)

            var CeldaUsu = Newrow.getCellFromKey("USUARIOS"); //usuarios, templated column visible
            var celdaHtml = CeldaUsu.getElement()
            //Asigno las denominaciones de usuarios al label del templated Column
            if (celdaHtml.getElementsByTagName("SPAN").item(0).firstChild) {
                if (denUsuarios.length > 30)
                    celdaHtml.getElementsByTagName("SPAN").item(0).firstChild.nodeValue = denUsuarios.substring(0, 30) + "..."
                else
                    celdaHtml.getElementsByTagName("SPAN").item(0).firstChild.nodeValue = denUsuarios
            }
            else {
                if (denUsuarios.length > 30)
                    setInnerText(celdaHtml.getElementsByTagName("SPAN").item(0), denUsuarios.substring(0, 30) + "..." )
                else
                    setInnerText(celdaHtml.getElementsByTagName("SPAN").item(0), denUsuarios)
            }
            celdaHtml.getElementsByTagName("SPAN").item(0).style.filter = "alpha(opacity = 100)"
            if(celdaHtml.getElementsByTagName("SPAN").item(0).style.opacity)
                celdaHtml.getElementsByTagName("SPAN").item(0).style.opacity = 100

            var NewCelda = Newrow.getCellFromKey("DENUSUARIOS"); //Denominacion de usuarios
            NewCelda.setValue(denUsuarios)

            var NewCelda = Newrow.getCellFromKey("CODCATEGORIA");  //Codigo de Categoria
            NewCelda.setValue(codCategorias)

            var NewCelda = Newrow.getCellFromKey("CATEGORIA"); //Id de Categoria
            NewCelda.setValue(idCategorias)

            var NewCelda = Newrow.getCellFromKey("COSTE_TARE"); //Id de Categoria
            NewCelda.setValue(costesHora)

            var z = 0
            var y = 0
            for (z = 0; z <= arrLineasComa.length - 1; z++) {
                if (arrLineasComa[z].indexOf("-") != -1) {
                    arrLineasGuion = arrLineasComa[z].split("-");
                    for (y = parseInt(arrLineasGuion[0]); y <= parseInt(arrLineasGuion[1]); y++) {
                        Newrow = grid.Rows.getRow(y - 1);
                        if (Newrow) {
                            var celdaHito = Newrow.getCellFromKey("HITO");
                            if (parseInt(celdaHito.getValue()) == 0) {

                                NewCelda = Newrow.getCellFromKey("STATUS");
                                if (NewCelda.getValue() == null || NewCelda.getValue() == 'null' || NewCelda.getValue() == 'Null' || NewCelda.getValue() == '')
                                    NewCelda.setValue('M')

                                CeldaProve = Newrow.getCellFromKey("CODPROVE");
                                CeldaProve.setValue(codProve)

                                CeldaDenProve = Newrow.getCellFromKey("DENPROVE");
                                CeldaDenProve.setValue(denProve)

                                CeldaMon = Newrow.getCellFromKey("MON");
                                CeldaMon.setValue(moneda)

                                //recojo los usuarios
                                var x = 0;
                                var i = 0;
                                denUsuarios = '';
                                codUsuarios = '';
                                codCategorias = '';
                                idCategorias = '';
                                costesHora = '';
                                for (x = 0; x <= arrUsuarios.length - 1; x++) {
                                    arrDatosUsuarios = arrUsuarios[x].split("_")

                                    denUsuarios = denUsuarios + arrDatosUsuarios[0] + ";"
                                    codUsuarios = codUsuarios + arrDatosUsuarios[1] + "#"
                                    codCategorias = codCategorias + arrDatosUsuarios[2] + "#"
                                    idCategorias = idCategorias + arrDatosUsuarios[3] + "#"
                                    costesHora = costesHora + arrDatosUsuarios[4] + "#"
                                }
                                //le quito el ultimo caracter que sobra
                                denUsuarios = denUsuarios.substring(0, denUsuarios.length - 1)
                                codUsuarios = codUsuarios.substring(0, codUsuarios.length - 1)
                                codCategorias = codCategorias.substring(0, codCategorias.length - 1)
                                idCategorias = idCategorias.substring(0, idCategorias.length - 1)
                                costesHora = costesHora.substring(0, costesHora.length - 1)


                                var CeldaCon = Newrow.getCellFromKey("CODCONTACTOS"); //Codigo de usuarios, columna no viisble
                                CeldaCon.setValue(codUsuarios)

                                var CeldaUsu = Newrow.getCellFromKey("USUARIOS"); //usuarios, templated column visible
                                var celdaHtml = CeldaUsu.getElement()
                                //Asigno las denominaciones de usuarios al label del templated Column
                                if (celdaHtml.getElementsByTagName("SPAN").item(0).firstChild) {
                                    if (denUsuarios.length > 30)
                                        celdaHtml.getElementsByTagName("SPAN").item(0).firstChild.nodeValue = denUsuarios.substring(0, 30) + "..."
                                    else
                                        celdaHtml.getElementsByTagName("SPAN").item(0).firstChild.nodeValue = denUsuarios
                                }
                                else {
                                    if (denUsuarios.length > 30)
                                        setInnerText(celdaHtml.getElementsByTagName("SPAN").item(0), denUsuarios.substring(0, 30) + "...")
                                    else
                                        setInnerText(celdaHtml.getElementsByTagName("SPAN").item(0), denUsuarios)
                                }
                                celdaHtml.getElementsByTagName("SPAN").item(0).style.filter = "alpha(opacity = 100)"
                                if(celdaHtml.getElementsByTagName("SPAN").item(0).style.opacity)
                                    celdaHtml.getElementsByTagName("SPAN").item(0).style.opacity = 100
                                
                                var NewCelda = Newrow.getCellFromKey("DENUSUARIOS"); //Denominacion de usuarios
                                NewCelda.setValue(denUsuarios)

                                var NewCelda = Newrow.getCellFromKey("CODCATEGORIA");  //Codigo de Categoria
                                NewCelda.setValue(codCategorias)

                                var NewCelda = Newrow.getCellFromKey("CATEGORIA"); //Id de Categoria
                                NewCelda.setValue(idCategorias)

                                var NewCelda = Newrow.getCellFromKey("COSTE_TARE"); //Id de Categoria
                                NewCelda.setValue(costesHora)
                            }
                        }

                    }
                } else {
                    Newrow = grid.Rows.getRow(parseInt(arrLineasComa[z]) - 1);
                    if (Newrow) {
                        var celdaHito = Newrow.getCellFromKey("HITO");
                        if (parseInt(celdaHito.getValue()) == 0) {
                            NewCelda = Newrow.getCellFromKey("STATUS");
                            if (NewCelda.getValue() == null || NewCelda.getValue() == 'null' || NewCelda.getValue() == 'Null' || NewCelda.getValue() == '')
                                NewCelda.setValue('M')

                            CeldaProve = Newrow.getCellFromKey("CODPROVE");
                            CeldaProve.setValue(codProve)

                            CeldaDenProve = Newrow.getCellFromKey("DENPROVE");
                            CeldaDenProve.setValue(denProve)

                            CeldaMon = Newrow.getCellFromKey("MON");
                            CeldaMon.setValue(moneda)

                            //recojo los usuarios
                            var x = 0;
                            var i = 0;
                            denUsuarios = '';
                            codUsuarios = '';
                            codCategorias = '';
                            idCategorias = '';
                            costesHora = '';
                            for (x = 0; x <= arrUsuarios.length - 1; x++) {
                                arrDatosUsuarios = arrUsuarios[x].split("_")

                                denUsuarios = denUsuarios + arrDatosUsuarios[0] + ";"
                                codUsuarios = codUsuarios + arrDatosUsuarios[1] + "#"
                                codCategorias = codCategorias + arrDatosUsuarios[2] + "#"
                                idCategorias = idCategorias + arrDatosUsuarios[3] + "#"
                                costesHora = costesHora + arrDatosUsuarios[4] + "#"
                            }
                            //le quito el ultimo caracter que sobra
                            denUsuarios = denUsuarios.substring(0, denUsuarios.length - 1)
                            codUsuarios = codUsuarios.substring(0, codUsuarios.length - 1)
                            codCategorias = codCategorias.substring(0, codCategorias.length - 1)
                            idCategorias = idCategorias.substring(0, idCategorias.length - 1)
                            costesHora = costesHora.substring(0, costesHora.length - 1)


                            var CeldaCon = Newrow.getCellFromKey("CODCONTACTOS"); //Codigo de usuarios, columna no viisble
                            CeldaCon.setValue(codUsuarios)

                            var CeldaUsu = Newrow.getCellFromKey("USUARIOS"); //usuarios, templated column visible
                            var celdaHtml = CeldaUsu.getElement()
                            //Asigno las denominaciones de usuarios al label del templated Column
                            if (celdaHtml.getElementsByTagName("SPAN").item(0).firstChild) {
                                if (denUsuarios.length > 30)
                                    celdaHtml.getElementsByTagName("SPAN").item(0).firstChild.nodeValue = denUsuarios.substring(0, 30) + "..."
                                else
                                    celdaHtml.getElementsByTagName("SPAN").item(0).firstChild.nodeValue = denUsuarios
                            }
                            else {
                                if (denUsuarios.length > 30)
                                    setInnerText(celdaHtml.getElementsByTagName("SPAN").item(0),denUsuarios.substring(0, 30) + "...")
                                else
                                    setInnerText(celdaHtml.getElementsByTagName("SPAN").item(0),denUsuarios)
                            }
                            celdaHtml.getElementsByTagName("SPAN").item(0).style.filter = "alpha(opacity = 100)"
                            if(celdaHtml.getElementsByTagName("SPAN").item(0).style.opacity)
                                celdaHtml.getElementsByTagName("SPAN").item(0).style.opacity = 100
                            
                            var NewCelda = Newrow.getCellFromKey("DENUSUARIOS"); //Denominacion de usuarios
                            NewCelda.setValue(denUsuarios)

                            var NewCelda = Newrow.getCellFromKey("CODCATEGORIA");  //Codigo de Categoria
                            NewCelda.setValue(codCategorias)

                            var NewCelda = Newrow.getCellFromKey("CATEGORIA"); //Id de Categoria
                            NewCelda.setValue(idCategorias)

                            var NewCelda = Newrow.getCellFromKey("COSTE_TARE"); //Id de Categoria
                            NewCelda.setValue(costesHora)
                        }
                    }

                }
            }

            //Cambio la configuracion de edicion del grid
            grid.AllowUpdate = 2;

        } else {
            //Cambio la configuracion de edicion del grid
            grid.AllowUpdate = 1;

            var NewCelda = Newrow.getCellFromKey("STATUS");

            if (NewCelda.getValue() == null || NewCelda.getValue() == 'null' || NewCelda.getValue() == 'Null' || NewCelda.getValue() == '')
                NewCelda.setValue('M')

            var CeldaProve = Newrow.getCellFromKey("CODPROVE");
            CeldaProve.setValue('')

            var CeldaDenProve = Newrow.getCellFromKey("DENPROVE");
            CeldaDenProve.setValue('')

            var CeldaMon = Newrow.getCellFromKey("MON");
            CeldaMon.setValue('')

            var CeldaCon = Newrow.getCellFromKey("CODCONTACTOS"); //Codigo de usuarios, columna no viisble
            CeldaCon.setValue('')

            var CeldaUsu = Newrow.getCellFromKey("USUARIOS"); //usuarios, templated column visible
            var celdaHtml = CeldaUsu.getElement()
            //Asigno las denominaciones de usuarios al label del templated Column
            if (celdaHtml.getElementsByTagName("SPAN").item(0).firstChild) {
                celdaHtml.getElementsByTagName("SPAN").item(0).firstChild.nodeValue = ''
            }
            else {
                setInnerText(celdaHtml.getElementsByTagName("SPAN").item(0), '')
            }
            celdaHtml.getElementsByTagName("SPAN").item(0).style.filter = "alpha(opacity = 0)"
            if(celdaHtml.getElementsByTagName("SPAN").item(0).style.opacity)
                                celdaHtml.getElementsByTagName("SPAN").item(0).style.opacity = 0

            var NewCelda = Newrow.getCellFromKey("DENUSUARIOS"); //Denominacion de usuarios
            NewCelda.setValue('')

            var NewCelda = Newrow.getCellFromKey("CODCATEGORIA");  //Codigo de Categoria
            NewCelda.setValue('')

            var NewCelda = Newrow.getCellFromKey("CATEGORIA"); //Id de Categoria
            NewCelda.setValue('')

            var NewCelda = Newrow.getCellFromKey("COSTE_TARE"); //Id de Categoria
            NewCelda.setValue('')

            //Cambio la configuracion de edicion del grid
            grid.AllowUpdate = 2;
        }
        
    }
    
    /*''' <summary>
    ''' Responder al evento click en una celda del grid, se utiliza para recoger la celda pulsada
    ''' </summary>
    ''' <param name="gridName">Nombre del grid</param>
    ''' <param name="cellId">Id de la celda</param>        
    ''' <param name="button">Botón pulsado</param>  
    ''' <remarks>Llamada desde: sistema; Tiempo máximo:0</remarks>*/
    function uwgDesgloseActividad_CellClickHandler(gridName, cellId, button) {

        var row = igtbl_getRowById(cellId);
        var celda = igtbl_getColumnById(cellId)
        var oGrid = igtbl_getGridById("<%=uwgDesgloseActividad.ClientID%>")
        rowGrid = row;
        celdaGrid = celda;
        celdaId = cellId;
    }

    function prove_seleccionado2(sCIF, sProveCod, sProveDen) {
        var oLineaProve = document.getElementById("<%=hid_LineaProve.ClientID%>")
        var oGrid = igtbl_getGridById("<%=uwgDesgloseActividad.ClientID%>")
        var linea = parseInt(oLineaProve.value)
        var row = oGrid.Rows.getRow(linea);
        var celda = row.getCellFromKey("CODCONTACTOS")
        celda.setValue(sProveCod)

        var CeldaUsu = row.getCellFromKey("USUARIOS"); //usuarios, templated column visible
        var celdaHtml = CeldaUsu.getElement()
        var sProve= sProveCod + '-' +sProveDen 
        //Asigno las denominaciones de usuarios al label del templated Column
        if (celdaHtml.getElementsByTagName("SPAN").item(0).firstChild) {
            if (sProve.length > 30)
                celdaHtml.getElementsByTagName("SPAN").item(0).firstChild.nodeValue = sProve.substring(0, 30) + "..."
            else
                celdaHtml.getElementsByTagName("SPAN").item(0).firstChild.nodeValue = sProve
        }
        else {
            if (sProve.length > 30)
                setInnerText(celdaHtml.getElementsByTagName("SPAN").item(0), sProve.substring(0, 30) + "...")
            else
                setInnerText(celdaHtml.getElementsByTagName("SPAN").item(0), sProve)
        }
        celdaHtml.getElementsByTagName("SPAN").item(0).style.filter = "alpha(opacity = 100)"
        if(celdaHtml.getElementsByTagName("SPAN").item(0).style.opacity)
                                celdaHtml.getElementsByTagName("SPAN").item(0).style.opacity = 100
        
    }
    //Funcion que llamara a la ventana de Asignacion de usuarios o al buscador de proveedores
    //linea: linea sobre la que se ha pulsado en el grid
    //llamada desde: desgloseAct.ascx.vb/inicializeRow ;Tiempo maximo:0,1
    function AbrirUsuarios(linea) {
        //var oGrid = igtbl_getGridById("<%=uwgDesgloseActividad.ClientID%>")
        var idContactos;
        var denContactos;
        var codProve;
        var denProve;
        var hito;
        var DenTarea;
        var linea_usuario = "";
        var NomGrid = "<%=uwgDesgloseActividad.ClientID%>"
        
        var oGrid = igtbl_getGridById(NomGrid)      
        rowGrid=oGrid.Rows.getRow(linea)
        
        idContactos = rowGrid.getCellFromKey("CODCONTACTOS").getValue();
        if (idContactos == null)
            idContactos = ""
        denContactos = rowGrid.getCellFromKey("DENUSUARIOS").getValue();
        if (denContactos == null)
            denContactos = ""
        codProve = rowGrid.getCellFromKey("CODPROVE").getValue();
        if (codProve == null)
            codProve = ""
        denProve = rowGrid.getCellFromKey("DENPROVE").getValue();
        if (denProve == null)
            denProve = ""   
        hito = rowGrid.getCellFromKey("HITO").getValue();
        DenTarea = rowGrid.getCellFromKey("TAREADEN").getValue();
        moneda = rowGrid.getCellFromKey("MON").getValue();
        categorias = rowGrid.getCellFromKey("CATEGORIA").getValue();
        if (categorias == null)
            categorias = ""
        codCategorias = rowGrid.getCellFromKey("CODCATEGORIA").getValue();
        if (codCategorias == null)
            codCategorias = ""     
        coste_tare = rowGrid.getCellFromKey("COSTE_TARE").getValue();
        if (coste_tare == null)
            coste_tare = ""     
        

        var oReadOnly = document.getElementById("<%=hid_ReadOnly.ClientID%>")

        if (hito == "0") {
            //No es un HITO y abro la pantalla de asignacion de usuarios
            arrIdContactos = idContactos.split("#");
            arrContactos = denContactos.split(";");
            arrCategorias = categorias.split("#");
            arrCodCategorias = codCategorias.split("#");
            arrCoste = coste_tare.split("#");


            for (i = 0; i <= arrIdContactos.length - 1; i++) {
                if (linea_usuario != "")
                    linea_usuario = linea_usuario + ";;"
                linea_usuario = linea_usuario + encodeURI(arrContactos[i]) + "_" + arrIdContactos[i] + "_" + arrCodCategorias[i] + "_" + arrCategorias[i] + "_" + arrCoste[i]
            }

            window.open("../_common/AsignacionUsuarios.aspx?IDControl=" + linea + " &CodProve=" + encodeURI(codProve) + "&DenProve=" + encodeURI(denProve) + "&CodMoneda=" + encodeURI(moneda) + "&SoloLectura=" + oReadOnly.value + "&SubTarea=" + encodeURI(DenTarea) + "&LineasUsuario=" + encodeURI(linea_usuario), "asignacionUsuarios", "width=725px,height=365px, location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =yes");

        } else {
            //Es un HITO y abro la pantalla de buscador de proveedores
            var oLineaProve = document.getElementById("<%=hid_LineaProve.ClientID%>")
            oLineaProve.value = linea
            var newWindow = window.open("<%=ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorProveedores.aspx?" %>Valor=" + idContactos + "&IDControl=&PM=true", "_blank", "width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes");
            
        }
    }

    //Funcion que llamara a la ventana de alta de tareas
    function Añadir() {
        var oHidNumFases = document.getElementById("<%=hid_NumFases.ClientID%>")
        var NomGrid = "<%=uwgDesgloseActividad.ClientID%>"
        var NomEliminarFilas = "<%=ibEliminarFilas.ClientID%>"
        
        switch (oHidNumFases.value) {
            case "1": { var newWindow = window.open("../alta/altaTarea.aspx?ElimFilas=" + NomEliminarFilas + "&Obligatorio=" + sObligatorio + "&NomGrid=" + NomGrid + "&NumFases=" + oHidNumFases.value + "&MaxFila=" + MaxFila + "&Editar=0", "_blank", "width=720,height=380,status=yes,resizable=no,top=200,left=200"); break }
            case "2": { newWindow = window.open("../alta/altaTarea.aspx?ElimFilas=" + NomEliminarFilas + "&Obligatorio=" + sObligatorio + "&NomGrid=" + NomGrid + "&NumFases=" + oHidNumFases.value + "&MaxFila=" + MaxFila + "&Editar=0", "_blank", "width=720,height=420,status=yes,resizable=no,top=200,left=200"); break }
            case "3": { newWindow = window.open("../alta/altaTarea.aspx?ElimFilas=" + NomEliminarFilas + "&Obligatorio=" + sObligatorio + "&NomGrid=" + NomGrid + "&NumFases=" + oHidNumFases.value + "&MaxFila=" + MaxFila + "&Editar=0", "_blank", "width=720,height=460,status=yes,resizable=no,top=200,left=200"); break }
            case "4": { newWindow = window.open("../alta/altaTarea.aspx?ElimFilas=" + NomEliminarFilas + "&Obligatorio=" + sObligatorio + "&NomGrid=" + NomGrid + "&NumFases=" + oHidNumFases.value + "&MaxFila=" + MaxFila + "&Editar=0", "_blank", "width=720,height=500,status=yes,resizable=no,top=200,left=200"); break }
            default: { newWindow = window.open("../alta/altaTarea.aspx?ElimFilas=" + NomEliminarFilas + "&Obligatorio=" + sObligatorio + "&NomGrid=" + NomGrid + "&NumFases=" + oHidNumFases.value + "&MaxFila=" + MaxFila + "&Editar=0", "_blank", "width=720,height=520,status=yes,resizable=no,top=200,left=200"); break }
        }
        
    }
    
    
    //funcion que muestra el popup de los comentarios de una tarea
    function AbrirComentarios(NumLinea) {
        var oGrid = igtbl_getGridById("<%=uwgDesgloseActividad.ClientID%>")      
        rowGrid=oGrid.Rows.getRow(NumLinea)
        var popup;
        var oHidReadOnly = document.getElementById("<%=hid_ReadOnly.ClientID%>")
        popup = $find("<%=mpeComentarios.ClientID%>")
        var comentarios = "";
        comentarios = rowGrid.getCellFromKey("OBS").getValue();
        var lblCabecera = document.getElementById("<%=lblCabecera.ClientID%>")
        setInnerText(lblCabecera, sComentario + sDenomTarea + " '" + rowGrid.getCellFromKey("TAREADEN").getValue() + "'");
        var pnlComentarios = document.getElementById("<%=pnlComentarios.ClientID%>")
        if (oHidReadOnly.value == 1) {
            //modo solo lectura
            var DivEdicion = document.getElementById("<%=divEdicion.ClientID%>")
            var DivConsulta = document.getElementById("<%=divConsulta.ClientID%>")
            var lblComentarios = document.getElementById("<%=lblComentarios.ClientID%>")
            DivEdicion.style.display = 'none'
            DivConsulta.style.display = 'block'
            setInnerText(lblComentarios, comentarios);
            popup.show();
            
        } else {
            //modo edicion
            var DivEdicion = document.getElementById("<%=divEdicion.ClientID%>")
            var DivConsulta = document.getElementById("<%=divConsulta.ClientID%>")
            DivEdicion.style.display = 'block'
            DivConsulta.style.display = 'none'
            var textArea = document.getElementById("<%=txtComentarios.ClientID%>")
            textArea.value = comentarios
            popup.show();
            textArea.focus()
        }
        pnlComentarios.style.position = 'absolute'
        
    }
    
    //Funcion que muestra el popup de borrado de una tarea
    function Eliminar(NumLinea) {
        
        var oGrid = igtbl_getGridById("<%=uwgDesgloseActividad.ClientID%>")      
        rowGrid=oGrid.Rows.getRow(NumLinea)
        
        var tarea= rowGrid.getCellFromKey("TAREA").getValue()
        var hito = rowGrid.getCellFromKey("HITO").getValue()
        var status = rowGrid.getCellFromKey("STATUS").getValue()
        var pnlEliminarTarea = document.getElementById("<%=pnlEliminarTarea.ClientID%>")
        if (hito == '0' && (status == '' || status == 'M' || status == null)) {
            // 1.- Creamos el objeto xmlHttpRequest
            CreateXmlHttp();

            // 2.- Definimos la llamada para hacer un simple GET.
            var ajaxRequest = rutaPM + 'GestionaAjax.aspx?accion=4&Tarea=' + tarea
            // 3.- Marcar qué función manejará la respuesta
            xmlHttp.onreadystatechange = recogeInfoEliminar;

            // 4.- Enviar
            xmlHttp.open("GET", ajaxRequest, true);
            xmlHttp.send("");
        } else {
            var popup = $find("<%=mpeEliminarTarea.ClientID%>")
            var lblEliminar = document.getElementById("<%=lblEliminarTarea.ClientID%>")
            var lblContinuar = document.getElementById("<%=lblDeseaContinuar.ClientID%>")
            setInnerText(lblEliminar, sEliminar + sDenomTarea + '.')
            setInnerText(lblContinuar, sContinuar)
            vEliminarTodas = 0;
            popup.show();
            pnlEliminarTarea.style.position = 'absolute'
            //rowGrid.setHidden(true);
        }
    }

    //Funcion que muestra el popup de borrado de todas las tareas
    function EliminarTodas() {
            
        // 1.- Creamos el objeto xmlHttpRequest
        CreateXmlHttp();

        // 2.- Definimos la llamada para hacer un simple GET.
        var ajaxRequest = rutaPM + 'GestionaAjax.aspx?accion=5&Proyecto=' + sIdProyecto
        // 3.- Marcar qué función manejará la respuesta
        xmlHttp.onreadystatechange = recogeInfoEliminarTodas;

        // 4.- Enviar
        xmlHttp.open("GET", ajaxRequest, true);
        xmlHttp.send("");
        

    }


    //Función que recoge la respuesta de la peticion Ajax
    function recogeInfoEliminar() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            var popup = $find("<%=mpeEliminarTarea.ClientID%>")
            var lblEliminar = document.getElementById("<%=lblEliminarTarea.ClientID%>")
            var lblContinuar = document.getElementById("<%=lblDeseaContinuar.ClientID%>")
           if (xmlHttp.responseText == '1') {
               setInnerText(lblEliminar, sEliminarConHoras + '.')
               setInnerText(lblContinuar, sContinuar)
           } else {
               setInnerText(lblEliminar, sEliminar + sDenomTarea + '.')
               setInnerText(lblContinuar, sContinuar)
           }
           vEliminarTodas = 0;
           var pnlEliminarTarea = document.getElementById("<%=pnlEliminarTarea.ClientID%>")
           popup.show();
           pnlEliminarTarea.style.position='absolute'
        }
    }

    //Función que recoge la respuesta de la peticion Ajax
    function recogeInfoEliminarTodas() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            var popup = $find("<%=mpeEliminarTarea.ClientID%>")
            var lblEliminar = document.getElementById("<%=lblEliminarTarea.ClientID%>")
            var lblContinuar = document.getElementById("<%=lblDeseaContinuar.ClientID%>")
            if (xmlHttp.responseText == '1') {
                setInnerText(lblEliminar,sEliminarTodasConHoras + '.')
                setInnerText(lblContinuar, sContinuar)
            } else {
                setInnerText(lblEliminar, sEliminarTodas + '.')
                setInnerText(lblContinuar, sContinuar)
            }
            vEliminarTodas = 1;
            var pnlEliminarTarea = document.getElementById("<%=pnlEliminarTarea.ClientID%>")
            popup.show();
            pnlEliminarTarea.style.position = 'absolute'
        }
    }
    
    //funcion que se ejecuta al aceptar el borrado de una tarea
    function EliminarOK() {
        var popup = $find("<%=mpeEliminarTarea.ClientID%>")
        var EliminarTodasFilas = document.getElementById("<%=ibEliminarFilas.ClientID%>")
        var oGrid = igtbl_getGridById("<%=uwgDesgloseActividad.ClientID%>")
        if (vEliminarTodas == 0) {
            var celdaStatus = rowGrid.getCellFromKey("STATUS")
            var celdaNum = rowGrid.getCellFromKey("NUM")
            
            celdaStatus.setValue('D');  //se marca como eliminada
            rowGrid.setHidden(true); //solo se elimina una(se oculta)
            if (celdaNum.getValue() == MaxFila)
                MaxFila=MaxFila-1
            
            if(oGrid.Rows.length==1) //es la unica que queda y se elimina)
                EliminarTodasFilas.style.visibility = "hidden"
            ActualizarLineasDesglose()
            sNumLineas = sNumLineas-1
        } else {
            //se eliminan todas(se ocultan)
            var x = 0;
            for (x = 0; x <= oGrid.Rows.length - 1; x++) {
                var row = oGrid.Rows.getRow(x);
                var celdaStatus = row.getCellFromKey("STATUS")
                celdaStatus.setValue('D');  //se marca como eliminada
                row.setHidden(true);
            }
            MaxFila = 0
            sNumLineas = 0
            lineasDesglose = ''
            EliminarTodasFilas.style.visibility = "hidden"
        }
        popup.hide();
    }

    //funcion que actualiza la variable LineasDesglose cuando se modifica una tarea
    function ActualizarLineasDesglose() {
        lineasDesglose = ''

        var oNumFases = document.getElementById("<%=hid_NumFases.ClientID%>")
        var NomGrid = "<%=uwgDesgloseActividad.ClientID%>"
        var grid = igtbl_getGridById(NomGrid);



        var x = 0;
        for (x = 0; x <= grid.Rows.length - 1; x++) {
            var row = grid.Rows.getRow(x);
            var celda = row.getCellFromKey("STATUS");
            if ((celda.getValue() == 'A' || celda.getValue() == 'M') ) {
                switch (oNumFases.value) {
                    case '1':
                        {
                            if (lineasDesglose == '')
                                lineasDesglose = row.getCellFromKey("COD_NIV4").getValue() + '#' + row.getCellFromKey("CODIGO").getValue()
                            else
                                lineasDesglose = lineasDesglose + ";" + row.getCellFromKey("COD_NIV4").getValue() + '#' + row.getCellFromKey("CODIGO").getValue()

                            break
                        }
                    case '2':
                        {
                            if (lineasDesglose == '')
                                lineasDesglose = row.getCellFromKey("COD_NIV3").getValue() + '#' + row.getCellFromKey("COD_NIV4").getValue() + '#' + row.getCellFromKey("CODIGO").getValue()
                            else
                                lineasDesglose = lineasDesglose + ";" + row.getCellFromKey("COD_NIV3").getValue() + '#' + row.getCellFromKey("COD_NIV4").getValue() + '#' + row.getCellFromKey("CODIGO").getValue()

                            break
                        }
                    case '3':
                        {
                            if (lineasDesglose == '')
                                lineasDesglose = row.getCellFromKey("COD_NIV2").getValue() + '#' + row.getCellFromKey("COD_NIV3").getValue() + '#' + row.getCellFromKey("COD_NIV4").getValue() + '#' + row.getCellFromKey("CODIGO").getValue()
                            else
                                lineasDesglose = lineasDesglose + ";" + row.getCellFromKey("COD_NIV2").getValue() + '#' + row.getCellFromKey("COD_NIV3").getValue() + '#' + row.getCellFromKey("COD_NIV4").getValue() + '#' + row.getCellFromKey("CODIGO").getValue()
                            break
                        }

                    case '4':
                        {
                            if (lineasDesglose == '')
                                lineasDesglose = row.getCellFromKey("COD_NIV1").getValue() + '#' + row.getCellFromKey("COD_NIV2").getValue() + '#' + row.getCellFromKey("COD_NIV3").getValue() + '#' + row.getCellFromKey("COD_NIV4").getValue() + '#' + row.getCellFromKey("CODIGO").getValue()
                            else
                                lineasDesglose = lineasDesglose + ";" + row.getCellFromKey("COD_NIV1").getValue() + '#' + row.getCellFromKey("COD_NIV2").getValue() + '#' + row.getCellFromKey("COD_NIV3").getValue() + '#' + row.getCellFromKey("COD_NIV4").getValue() + '#' + row.getCellFromKey("CODIGO").getValue()
                            break
                        }
                }
            }
        }
    }
    
    
    //Funcion que llamara a la ventana de alta de tareas en modo edicion
    function Editar(NumLinea) {
        var fecInicio;
        var fecFin;
        var horasAsig;
        var linea;
        var gradoAvance;
        var hito;
        var DenTarea;
        var codTarea;
        var codEstado;
        var obs;
        var oNumFases = document.getElementById("<%=hid_NumFases.ClientID%>")
        var NomGrid = "<%=uwgDesgloseActividad.ClientID%>"
        var NomEliminarFilas = "<%=ibEliminarFilas.ClientID%>"
        var oGrid = igtbl_getGridById("<%=uwgDesgloseActividad.ClientID%>")        
        rowGrid=oGrid.Rows.getRow(NumLinea)
        
        
        fecInicio = rowGrid.getCellFromKey("FECEST_INICIO").getValue();
        fecFin = rowGrid.getCellFromKey("FECEST_FIN").getValue();
        if(fecInicio)
            fecInicio = fecInicio.format("MM/dd/yy")
        if(fecFin)
            fecFin = fecFin.format("MM/dd/yy")
        
        horasAsig = rowGrid.getCellFromKey("HORAS_ASIG").getValue();
        gradoAvance = rowGrid.getCellFromKey("GRADO_AVANCE").getValue();
        if (gradoAvance == null) gradoAvance = "0" 
        hito = rowGrid.getCellFromKey("HITO").getValue();
        codTarea = rowGrid.getCellFromKey("CODIGO").getValue();
        if (codTarea == null) codTarea = "" 
        DenTarea = rowGrid.getCellFromKey("TAREADEN").getValue();
        if (DenTarea == null) DenTarea = "" 
        linea = rowGrid.getCellFromKey("NUM").getValue();
        codEstado = rowGrid.getCellFromKey("CODESTADO").getValue();   
        obs = rowGrid.getCellFromKey("OBS").getValue();
        if (obs == null) obs = ""     
        

        if (oNumFases.value == 1) {
            var fase1 = rowGrid.getCellFromKey("DEN_NIV4").getValue();
            var codfase1 = rowGrid.getCellFromKey("COD_NIV4").getValue();

            var newWindow = window.open("../alta/altaTarea.aspx?ElimFilas=" + NomEliminarFilas +  "&Obligatorio=" + sObligatorio +"&Editar=1&linea=" + rowGrid.getIndex() + "&NomGrid=" + NomGrid + "&NumFases=" + oNumFases.value + "&num=" + linea + "&fecInicio=" + fecInicio + "&fecFin=" + fecFin + "&codEstado=" + codEstado + "&gradoAvance=" + gradoAvance + "&obs=" + obs + "&horasAsig=" + horasAsig + "&Hito=" + hito + "&fase1=" + fase1 + "&codfase1=" + codfase1 + "&tarea=" + DenTarea + "&codtarea=" + codTarea, "_blank", "width=720,height=380,status=yes,resizable=no,top=200,left=200");
        } else if (oNumFases.value == 2) {
            var fase1 = rowGrid.getCellFromKey("DEN_NIV3").getValue();
            var fase2 = rowGrid.getCellFromKey("DEN_NIV4").getValue();
            var codfase1 = rowGrid.getCellFromKey("COD_NIV3").getValue();
            var codfase2 = rowGrid.getCellFromKey("COD_NIV4").getValue();
            newWindow = window.open("../alta/altaTarea.aspx?ElimFilas=" + NomEliminarFilas + "&Obligatorio=" + sObligatorio + "&Editar=1&linea=" + rowGrid.getIndex() + "&NomGrid=" + NomGrid + "&NumFases=" + oNumFases.value + "&num=" + linea + "&fecInicio=" + fecInicio + "&fecFin=" + fecFin + "&codEstado=" + codEstado + "&gradoAvance=" + gradoAvance + "&obs=" + obs + "&horasAsig=" + horasAsig + "&Hito=" + hito + "&fase1=" + fase1 + "&codfase1=" + codfase1 + "&fase2=" + fase2 + "&codfase2=" + codfase2 + "&tarea=" + DenTarea + "&codtarea=" + codTarea, "_blank", "width=720,height=420,status=yes,resizable=no,top=200,left=200");
        } else if (oNumFases.value == 3) {
            var fase1 = rowGrid.getCellFromKey("DEN_NIV2").getValue();
            var fase2 = rowGrid.getCellFromKey("DEN_NIV3").getValue();
            var fase3 = rowGrid.getCellFromKey("DEN_NIV4").getValue();
            var codfase1 = rowGrid.getCellFromKey("COD_NIV2").getValue();
            var codfase2 = rowGrid.getCellFromKey("COD_NIV3").getValue();
            var codfase3 = rowGrid.getCellFromKey("COD_NIV4").getValue();

            newWindow = window.open("../alta/altaTarea.aspx?ElimFilas=" + NomEliminarFilas + "&Obligatorio=" + sObligatorio + "&Editar=1&linea=" + rowGrid.getIndex() + "&NomGrid=" + NomGrid + "&NumFases=" + oNumFases.value + "&num=" + linea + "&fecInicio=" + fecInicio + "&fecFin=" + fecFin + "&codEstado=" + codEstado + "&gradoAvance=" + gradoAvance + "&obs=" + obs + "&horasAsig=" + horasAsig + "&Hito=" + hito + "&fase1=" + fase1 + "&codfase1=" + codfase1 + "&fase2=" + fase2 + "&codfase2=" + codfase2 + "&fase3=" + fase3 + "&codfase3=" + codfase3 + "&tarea=" + DenTarea + "&codtarea=" + codTarea, "_blank", "width=720,height=460,status=yes,resizable=no,top=200,left=200");
        } else {
            var fase1 = rowGrid.getCellFromKey("DEN_NIV1").getValue();
            var fase2 = rowGrid.getCellFromKey("DEN_NIV2").getValue();
            var fase3 = rowGrid.getCellFromKey("DEN_NIV3").getValue();
            var fase4 = rowGrid.getCellFromKey("DEN_NIV4").getValue();
            var codfase1 = rowGrid.getCellFromKey("COD_NIV1").getValue();
            var codfase2 = rowGrid.getCellFromKey("COD_NIV2").getValue();
            var codfase3 = rowGrid.getCellFromKey("COD_NIV3").getValue();
            var codfase4 = rowGrid.getCellFromKey("COD_NIV4").getValue();

            newWindow = window.open("../alta/altaTarea.aspx?ElimFilas=" + NomEliminarFilas + "&Obligatorio=" + sObligatorio + "&Editar=1&linea=" + rowGrid.getIndex() + "&NomGrid=" + NomGrid + "&NumFases=" + oNumFases.value + "&num=" + linea + "&fecInicio=" + fecInicio + "&fecFin=" + fecFin + "&codEstado=" + codEstado + "&gradoAvance=" + gradoAvance + "&obs=" + obs + "&horasAsig=" + horasAsig + "&Hito=" + hito + "&fase1=" + fase1 + "&codfase1=" + codfase1 + "&fase2=" + fase2 + "&codfase2=" + codfase2 + "&fase3=" + fase3 + "&codfase3=" + codfase3 + "&fase4=" + fase4 + "&codfase4=" + codfase4 + "&tarea=" + DenTarea + "&codtarea=" + codTarea, "_blank", "width=720,height=500,status=yes,resizable=no,top=200,left=200");
        }
        
    }
    //funcion que pone como modificada la fila y actualiza las observaciones desde el modal popup
    //Edicion: indica si esta en modo de edicion o solo lectura
    function ComentariosOK(edicion) {
        
        
        if (edicion == 1) {
            var txtComentarios = document.getElementById("<%=txtComentarios.ClientID%>")
            rowGrid.getCellFromKey("OBS").setValue(txtComentarios.value);
            var NewCelda = rowGrid.getCellFromKey("STATUS");
            if (NewCelda.getValue() == null || NewCelda.getValue() == 'null' || NewCelda.getValue() == 'Null' || NewCelda.getValue() == '')
                NewCelda.setValue('M')
        }
        $find("<%=mpeComentarios.ClientID%>").hide();
    }
    //funcion que muestra el boton de eliminar todas las filas
    function MostrarEliminarFilas() {
        var oEliminarFilas = document.getElementById("<%=ibEliminarFilas.ClientID%>");
        oEliminarFilas.style.visibility='visible'

    }

    //funcion que abre la pantalla para seleccionar la plantilla a subir
    function SubirExcel() {
        var newWindow = window.open(rutaPM + "_common/attachfile.aspx?desgloseActividad=1", "_blank", "width=600,height=200,status=no,resizable=yes,top=200,left=200");
        
    }


    //funcion que hara una peticion Ajax para leer la plantilla Excel
    //pathPlantilla: path de la plantilla
    function LeerPlantillaSubida(pathPlantilla) {
        // 1.- Creamos el objeto xmlHttpRequest
        CreateXmlHttp();
        var pnlUpProgress = document.getElementById("<%=panelUpdateProgress.ClientID%>")
        $find("<%=ModalProgress.ClientID%>").show();
        pnlUpProgress.style.position='absolute'
        
        // 2.- Definimos la llamada para hacer un simple GET.
        var ajaxRequest = rutaPM + "GestionaAjax.aspx?accion=6&path="+ pathPlantilla 
        // 3.- Marcar qué función manejará la respuesta
        xmlHttp.onreadystatechange = recogeInfoLeerPlantilla;

        // 4.- Enviar
        xmlHttp.open("GET", ajaxRequest, true);
        xmlHttp.send("");
    }
    //Funcion que descarga la plantilla excel para que den de alta tareas o hitos
    function BajarExcel() {
        var oNumFases = document.getElementById("<%=hid_NumFases.ClientID%>")
        var newWindow = window.open(rutaPM + "_common/attach.aspx?desgloseActividad=1&NumFases=" + oNumFases.value, "_blank", "width=600,height=275,status=no,resizable=yes,top=200,left=200");
        
    }

    //funcion que valida si es un numero
    function IsNumeric(expression)
	{
	    return (String(expression).search(/^\d+$/) != -1);
	}
	//funcion que valida si es una fecha
	function isDate(sDate) {
        var scratch = new Date(sDate);
        if (scratch.toString() == "NaN" || scratch.toString() == "Invalid Date") {
             return false;
        } else 
        {
            return true;
        }
    }

    //funcion que valida que la plantilla se haya introducido correctamente
    //Valores: String con los valores de la plantillas
    //NumFases: Numero de fases que tiene el proyecto
    //Oblig: 1 si es obligatorio el desglose de actividad
    function ValidarPlantilla(Valores, NumFases,Oblig) {
        var arrLineas;
        var lineasDesgloseOld = lineasDesglose
        arrLineas = Valores.split("&&");
        if (arrLineas.length >= 1) {
            for (i = 0; i <= arrLineas.length - 1; i++) {
                arrCols = arrLineas[i].split("$$")
                var num = arrCols[0]
                if(num=='' || (!IsNumeric(num)) )  return false
                var codNiv1 = arrCols[1]
                if (codNiv1 =='') return false
                var denNiv1 = arrCols[2]
                if (denNiv1 == '') return false

              
                
                switch (parseInt(NumFases)) {
                    case 1:
                        {
                            var codTarea = arrCols[3]
                            if (codTarea == '') return false
                            var denTarea = arrCols[4]
                            if (denTarea == '') return false
                            var hito = arrCols[5]
                            if (hito == '' || (!IsNumeric(hito))) {
                                return false
                            } else {
                                if (parseInt(hito) > 1 || parseInt(hito) < 0) return false
                            }
                            if (hito == '0') {
                                var fecIni = arrCols[6]
                                var fecFin = arrCols[7]
                                if (fecIni != '' || isDate(fecIni) == true) {
                                    if (fecFin != '' || isDate(fecFin) == true) {
                                        var FechaIni = new Date(parseInt(fecIni.substring(6, 10)), parseInt(fecIni.substring(3, 5)) - 1, parseInt(fecIni.substring(0, 2)))
                                        var FechaFin = new Date(parseInt(fecFin.substring(6, 10)), parseInt(fecFin.substring(3, 5)) - 1, parseInt(fecFin.substring(0, 2)))
                                        if (FechaFin < FechaIni) return 1
                                    }
                                }
                            }

                            if (Oblig == "1" || Oblig == 1) {
                                var fecIni = arrCols[6]
                                if (fecIni == '' || isDate(fecIni) == false) return false
                                var fecFin = arrCols[7]
                                if (fecFin == '' || isDate(fecFin) == false) return false
                                var horas = arrCols[8]
                                if (horas == '' || (!IsNumeric(horas))) return false
                            }
                            break
                        }
                    case 2:
                        {
                            var codNiv2 = arrCols[3]
                            if (codNiv2 == '') return false
                            var denNiv2 = arrCols[4]
                            if (denNiv2 == '') return false
                            var codTarea = arrCols[5]
                            if (codTarea == '') return false
                            var denTarea = arrCols[6]
                            if (denTarea == '') return false
                            var hito = arrCols[7]
                            if (hito == '' || (!IsNumeric(hito))) {
                                return false
                            } else {
                                if (parseInt(hito) > 1 || parseInt(hito) < 0) return false
                            }
                            if (hito == '0') {
                                var fecIni = arrCols[8]
                                var fecFin = arrCols[9]
                                if (fecIni != '' || isDate(fecIni) == true) {
                                    if (fecFin != '' || isDate(fecFin) == true) {
                                        var FechaIni = new Date(parseInt(fecIni.substring(6, 10)), parseInt(fecIni.substring(3, 5)) - 1, parseInt(fecIni.substring(0, 2)))
                                        var FechaFin = new Date(parseInt(fecFin.substring(6, 10)), parseInt(fecFin.substring(3, 5)) - 1, parseInt(fecFin.substring(0, 2)))
                                        if (FechaFin < FechaIni) return 1
                                    }
                                }
                            }
                            if (Oblig == "1" || Oblig == 1) {
                                var fecIni = arrCols[8]
                                if (fecIni == '' || isDate(fecIni) == false) return false
                                var fecFin = arrCols[9]
                                if (fecFin == '' || isDate(fecFin) == false) return false
                                var horas = arrCols[10]
                                if (horas == '' || (!IsNumeric(horas))) return false
                            }
                        
                        break
                    }
                    case 3:
                        {
                            var codNiv2 = arrCols[3]
                            if (codNiv2 == '') return false
                            var denNiv2 = arrCols[4]
                            if (denNiv2 == '') return false
                            var codNiv3 = arrCols[5]
                            if (codNiv3 == '') return false
                            var denNiv3 = arrCols[6]
                            if (denNiv3 == '') return false
                            var codTarea = arrCols[7]
                            if (codTarea == '') return false
                            var denTarea = arrCols[8]
                            if (denTarea == '') return false
                            var hito = arrCols[9]
                            if (hito == '' || (!IsNumeric(hito))) {
                                return false
                            } else {
                                if (parseInt(hito) > 1 || parseInt(hito) < 0) return false
                            }

                            if (hito == '0') {
                                var fecIni = arrCols[10]
                                var fecFin = arrCols[11]
                                if (fecIni != '' || isDate(fecIni) == true) {
                                    if (fecFin != '' || isDate(fecFin) == true) {
                                        var FechaIni = new Date(parseInt(fecIni.substring(6, 10)), parseInt(fecIni.substring(3, 5)) - 1, parseInt(fecIni.substring(0, 2)))
                                        var FechaFin = new Date(parseInt(fecFin.substring(6, 10)), parseInt(fecFin.substring(3, 5)) - 1, parseInt(fecFin.substring(0, 2)))
                                        if (FechaFin < FechaIni) return 1
                                    }
                                }
                            }
                            if (Oblig == "1" || Oblig == 1) {
                                var fecIni = arrCols[10]
                                if (fecIni == '' || isDate(fecIni) == false) return false
                                var fecFin = arrCols[11]
                                if (fecFin == '' || isDate(fecFin) == false) return false
                                var horas = arrCols[12]
                                if (horas == '' || (!IsNumeric(horas))) return false
                            }
                            break
                        }
                    case 4:
                        {
                            var codNiv2 = arrCols[3]
                            if (codNiv2 == '') return false
                            var denNiv2 = arrCols[4]
                            if (denNiv2 == '') return false
                            var codNiv3 = arrCols[5]
                            if (codNiv3 == '') return false
                            var denNiv3 = arrCols[6]
                            if (denNiv3 == '') return false
                            var codNiv4 = arrCols[7]
                            if (codNiv4 == '') return false
                            var denNiv4 = arrCols[8]
                            if (denNiv4 == '') return false
                            var codTarea = arrCols[9]
                            if (codTarea == '') return false
                            var denTarea = arrCols[10]
                            if (denTarea == '') return false
                            var hito = arrCols[11]
                            if (hito == '' || (!IsNumeric(hito))) {
                                return false
                            } else {
                                if (parseInt(hito) > 1 || parseInt(hito) < 0) return false
                            }

                            if (hito == '0') {
                                var fecIni = arrCols[12]
                                var fecFin = arrCols[13]
                                if (fecIni != '' || isDate(fecIni) == true) {
                                    if (fecFin != '' || isDate(fecFin) == true) {
                                        var FechaIni = new Date(parseInt(fecIni.substring(6, 10)), parseInt(fecIni.substring(3, 5)) - 1, parseInt(fecIni.substring(0, 2)))
                                        var FechaFin = new Date(parseInt(fecFin.substring(6, 10)), parseInt(fecFin.substring(3, 5)) - 1, parseInt(fecFin.substring(0, 2)))
                                        if (FechaFin < FechaIni) return 1
                                    }
                                }
                            }
                            if (Oblig == "1" || Oblig == 1) {
                                var fecIni = arrCols[12]
                                if (fecIni == '' || isDate(fecIni) == false) return false
                                var fecFin = arrCols[13]
                                if (fecFin == '' || isDate(fecFin) == false) return false
                                var horas = arrCols[14]
                                if (horas == '' || (!IsNumeric(horas))) return false
                            }
                            break
                        }

                } //fin switch

                var NewTarea = '';
                var ArrLineas = lineasDesglose.split(";");
                switch (parseInt(NumFases)) {
                    case 1:
                        {
                            //Valido la Tarea
                            for (z = 0; z <= ArrLineas.length - 1; z++) {
                                if (ArrLineas[z].indexOf(codTarea) != -1 && ArrLineas[z].split("#")[1].length == codTarea.length) {
                                    var arrFases = ArrLineas[z].split("#");
                                    var Cod1 = arrFases[0]
                                    if (codNiv1 != Cod1) {
                                        lineasDesglose = lineasDesgloseOld
                                        return 'error'
                                    }
                                }
                            }

                            //Valido que la tarea no exista
                            NewTarea = codNiv1 + "#" + codTarea
                            if (lineasDesglose.indexOf(NewTarea) != -1) {
                                lineasDesglose = lineasDesgloseOld
                                return 'existe'
                            }
                            
                            if (lineasDesglose == '')
                                lineasDesglose = codNiv1 + "#" + codTarea
                            else
                                lineasDesglose = lineasDesglose + ";" + codNiv1 + '#' + codTarea
                            break
                        }
                    case 2:
                        {

                            //Valido el Nivel2
                            for (z = 0; z <= ArrLineas.length - 1; z++) {
                                if (ArrLineas[z].indexOf(codNiv2) != -1 && ArrLineas[z].split("#")[1].length == codNiv2.length) {
                                    var arrFases = ArrLineas[z].split("#");
                                    var Cod1 = arrFases[0]
                                    if (codNiv1 != Cod1) {
                                        lineasDesglose = lineasDesgloseOld
                                        return 'error'
                                    }
                                }
                            }
                            //Valido la Tarea
                            for (z = 0; z <= ArrLineas.length - 1; z++) {
                                if (ArrLineas[z].indexOf(codTarea) != -1 && ArrLineas[z].split("#")[2].length == codTarea.length) {
                                    var arrFases = ArrLineas[z].split("#");
                                    var Cod2 = arrFases[1]
                                    if (codNiv2 != Cod2) {
                                        lineasDesglose = lineasDesgloseOld
                                        return 'error'
                                    }
                                }
                            }

                            //Valido que la tarea no exista
                            NewTarea = codNiv1 + '#' + codNiv2 + '#' + codTarea
                            if (lineasDesglose.indexOf(NewTarea) != -1) {
                                lineasDesglose = lineasDesgloseOld
                                return 'existe'
                            }

                            if (lineasDesglose == '')
                                lineasDesglose = codNiv1 + '#' + codNiv2 + '#' + codTarea
                            else
                                lineasDesglose = lineasDesglose + ";" + codNiv1 + '#' + codNiv2 + '#' + codTarea
                            break
                        }
                    case 3:
                        {
                            //Valido el Nivel2
                            for (z = 0; z <= ArrLineas.length - 1; z++) {
                                if (ArrLineas[z].indexOf(codNiv2) != -1 && ArrLineas[z].split("#")[1].length == codNiv2.length) {
                                    var arrFases = ArrLineas[z].split("#");
                                    var Cod1 = arrFases[0]
                                    if (codNiv1 != Cod1) {
                                        lineasDesglose = lineasDesgloseOld
                                        return 'error'
                                    }
                                }
                            }
                            //Valido el Nivel3
                            for (z = 0; z <= ArrLineas.length - 1; z++) {
                                if (ArrLineas[z].indexOf(codNiv3) != -1 && ArrLineas[z].split("#")[2].length == codNiv3.length) {
                                    var arrFases = ArrLineas[z].split("#");
                                    var Cod2 = arrFases[1]
                                    if (codNiv2 != Cod2) {
                                        lineasDesglose = lineasDesgloseOld
                                        return 'error'
                                    }
                                }
                            }
                            //Valido la Tarea
                            for (z = 0; z <= ArrLineas.length - 1; z++) {
                                if (ArrLineas[z].indexOf(codTarea) != -1 && ArrLineas[z].split("#")[3].length == codTarea.length) {
                                    var arrFases = ArrLineas[z].split("#");
                                    var Cod3 = arrFases[2]
                                    if (codNiv3 != Cod3) {
                                        lineasDesglose = lineasDesgloseOld
                                        return 'error'
                                    }
                                }
                            }

                            //Valido que la tarea no exista
                            NewTarea = codNiv1 + '#' + codNiv2 + '#' + codNiv3 + '#' + codTarea
                            if (lineasDesglose.indexOf(NewTarea) != -1) {
                                lineasDesglose = lineasDesgloseOld
                                return 'existe'
                            }
                            
                            if (lineasDesglose == '')
                                lineasDesglose = codNiv1 + '#' + codNiv2 + '#' + codNiv3 + '#' + codTarea
                            else
                                lineasDesglose = lineasDesglose + ";" + codNiv1 + '#' + codNiv2 + '#' + codNiv3 + '#' + codTarea
                            break
                        }
                    case 4:
                        {

                            //Valido el Nivel2
                            for (z = 0; z <= ArrLineas.length - 1; z++) {
                                if (ArrLineas[z].indexOf(codNiv2) != -1 && ArrLineas[z].split("#")[1].length == codNiv2.length) {
                                    var arrFases = ArrLineas[z].split("#");
                                    var Cod1 = arrFases[0]
                                    if (codNiv1 != Cod1) {
                                        lineasDesglose = lineasDesgloseOld
                                        return 'error'
                                    }
                                }
                            }
                            //Valido el Nivel3
                            for (z = 0; z <= ArrLineas.length - 1; z++) {
                                if (ArrLineas[z].indexOf(codNiv3) != -1 && ArrLineas[z].split("#")[2].length == codNiv3.length) {
                                    var arrFases = ArrLineas[z].split("#");
                                    var Cod2 = arrFases[1]
                                    if (codNiv2 != Cod2) {
                                        lineasDesglose = lineasDesgloseOld
                                        return 'error'
                                    }
                                }
                            }
                            //Valido el Nivel4
                            for (z = 0; z <= ArrLineas.length - 1; z++) {
                                if (ArrLineas[z].indexOf(codNiv4) != -1 && ArrLineas[z].split("#")[3].length == codNiv4.length) {
                                    var arrFases = ArrLineas[z].split("#");
                                    var Cod3 = arrFases[2]
                                    if (codNiv3 != Cod3) {
                                        lineasDesglose = lineasDesgloseOld
                                        return 'error'
                                    }
                                }
                            }
                            //Valido la Tarea
                            for (z = 0; z <= ArrLineas.length - 1; z++) {
                                if (ArrLineas[z].indexOf(codTarea) != -1 && ArrLineas[z].split("#")[4].length == codTarea.length) {
                                    var arrFases = ArrLineas[z].split("#");
                                    var Cod4 = arrFases[3]
                                    if (codNiv4 != Cod4) {
                                        lineasDesglose = lineasDesgloseOld
                                        return 'error'
                                    }
                                }
                            }

                            //Valido que la tarea no exista
                            NewTarea = codNiv1 + '#' + codNiv2 + '#' + codNiv3 + '#' + codNiv4 + '#' + codTarea
                            if (lineasDesglose.indexOf(NewTarea) != -1) {
                                lineasDesglose = lineasDesgloseOld
                                return 'existe'
                            }
                            
                            if (lineasDesglose == '')
                                lineasDesglose = codNiv1 + '#' + codNiv2 + '#' + codNiv3 + '#' + codNiv4 + '#' + codTarea
                            else
                                lineasDesglose = lineasDesglose + ";" + codNiv1 + '#' + codNiv2 + '#' + codNiv3 + '#' + codNiv4 + '#' + codTarea
                            break
                        }
                }
                
            }//fin For
        }
    }
    
    //funcion que lee el string de la plantilla excel y rellena el grid
    function recogeInfoLeerPlantilla() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            var arrLineas;
            var arrCols;
            var respuesta = xmlHttp.responseText
            var oNumFases = document.getElementById("<%=hid_NumFases.ClientID%>")
            var oEstadoDefecto = document.getElementById("<%=hid_EstadoDefecto.ClientID%>")
            var oOblig = document.getElementById("<%=hid_Oblig.ClientID%>")

            if (respuesta == "ERROR") {
                //Error de formato de plantilla
                $find("<%=ModalProgress.ClientID%>").hide();
                alert(sErrorPlantilla) 
                return false
            }
            var resp = ValidarPlantilla(respuesta, oNumFases.value, oOblig.value)
            if (resp == false) { //Error de validacion de plantilla
                $find("<%=ModalProgress.ClientID%>").hide();
                alert(sValidacionPlantilla)
                return false
            }
            if (resp == 1) { //error en fechas de plantilla
                $find("<%=ModalProgress.ClientID%>").hide();
                alert(sFechas)
                return false
            }
            if (resp == 'error') { //Error en fases de Plantilla
                $find("<%=ModalProgress.ClientID%>").hide();
                alert(sErrorFases)
                return false
            }
            if (resp == 'existe') { //Error en fases de Plantilla
                $find("<%=ModalProgress.ClientID%>").hide();
                alert(sTareaExiste)
                return false
            }


            var i = 0;
            var grid = igtbl_getGridById("<%=uwgDesgloseActividad.ClientID%>");
            var lineasGrid=parseInt(grid.Rows.length)
            arrLineas = respuesta.split("&&");
            if (arrLineas.length >= 1) {
                
                for (i = 0; i <= arrLineas.length - 1; i++) {
                    arrCols = arrLineas[i].split("$$")
                    var num = arrCols[0]
                    var codNiv1 = arrCols[1]
                    var denNiv1 = arrCols[2]
                    
                    switch (oNumFases.value) {
                        case "1":
                            {
                                var codTarea = arrCols[3]
                                var denTarea = arrCols[4]
                                var hito = arrCols[5]
                                var fecIni = arrCols[6]
                                var fecFin = arrCols[7]
                                var horas = arrCols[8]
                                var gradoA = arrCols[9]
                                var estado = arrCols[10]
                                var obs = arrCols[11]
                                break 
                            }
                        case "2":
                            {
                                var codNiv2 = arrCols[3]
                                var denNiv2 = arrCols[4]
                                var codTarea = arrCols[5]
                                var denTarea = arrCols[6]
                                var hito = arrCols[7]
                                var fecIni = arrCols[8]
                                var fecFin = arrCols[9]
                                var horas = arrCols[10]
                                var gradoA = arrCols[11]
                                var estado = arrCols[12]
                                var obs = arrCols[13]
                                break 
                            }
                         case "3":
                            {
                             var codNiv2 = arrCols[3]
                             var denNiv2 = arrCols[4]
                             var codNiv3 = arrCols[5]
                             var denNiv3 = arrCols[6]
                             var codTarea = arrCols[7]
                             var denTarea = arrCols[8]
                             var hito = arrCols[9]
                             var fecIni = arrCols[10]
                             var fecFin = arrCols[11]
                             var horas = arrCols[12]
                             var gradoA = arrCols[13]
                             var estado = arrCols[14]
                             var obs = arrCols[15]
                             break 
                          }
                      case "4":
                          {
                              var codNiv2 = arrCols[3]
                              var denNiv2 = arrCols[4]
                              var codNiv3 = arrCols[5]
                              var denNiv3 = arrCols[6]
                              var codNiv4 = arrCols[7]
                              var denNiv4 = arrCols[8]
                              var codTarea = arrCols[9]
                              var denTarea = arrCols[10]
                              var hito = arrCols[11]
                              var fecIni = arrCols[12]
                              var fecFin = arrCols[13]
                              var horas = arrCols[14]
                              var gradoA = arrCols[15]
                              var estado = arrCols[16]
                              var obs = arrCols[17]
                              break 
                         }

                 }
                 
                 igtbl_addNew("<%=uwgDesgloseActividad.ClientID%>", 0);
                 var Newrow = grid.Rows.getRow(grid.Rows.length - 1)

                 var Textos = sTextos
                 var arrTextos = Textos.split('#')

                 var HitoHTML = "<div><IMG ID='imgHito' border=0 alt=" + arrTextos[2] + " title=" + arrTextos[2] + " align=middle  src='" + sRutaPM + "alta/images/Hito.gif' /> </div>";

                 var EditarHTML = "<TABLE style='WIDTH: 100%; DISPLAY: inline' cellSpacing=0 cellPadding=0><TR><TD><IMG id=idEditar onclick=Editar(" + (lineasGrid + parseInt(i)) + ") border=0 alt=" + arrTextos[1] + " title=" + arrTextos[1] + " align=middle src='" + sRutaPM + "alta/images/Lapiz.png'></TD></TR></TBODY></TABLE>";

                 var UsuariosHTML = "<TABLE style='WIDTH: 100%;' cellSpacing=0 cellPadding=0><TR><TD><SPAN style='WIDTH: 220px; heigth: 15px DISPLAY: inline-block; filter: alpha(opacity = 0) ; opacity: 0; ;font-size:8pt; font-family:Verdana; color: #494949' id=lblUsuarios>texto</SPAN> </TD><TD style='FONT-SIZE: 1pt' onclick=AbrirUsuarios(" + (lineasGrid + parseInt(i)) + ") class=fsstyentrydd vAlign=center width=12 align=middle><IMG id=btnUsuarios  border=0 alt=dAbrirUsuarios align=middle src='" + sRutaPM + "_common/images/trespuntos.gif'> </TD></TR></TABLE>";

                 if (obs != '')
                     var ComentarioHTML = "<div><IMG ID='imgComentario' border=0 alt=" + arrTextos[3] + " title=" + arrTextos[3] + " align=middle onclick=AbrirComentarios(" + (lineasGrid + parseInt(i)) + ")  src='" + sRutaPM + "alta/images/SolicitudesPmSmall.gif' /> </div>";
                 else
                     var ComentarioHTML = "<div><IMG ID='imgComentario' border=0 alt=" + arrTextos[3] + " title=" + arrTextos[3] + " align=middle width=0  src='" + sRutaPM + "alta/images/SolicitudesPmSmall.gif' /> </div>";

                 var EliminarHTML = "<div><IMG ID='imgEliminar' border=0 onclick=Eliminar(" + (lineasGrid + parseInt(i)) + ") alt=" + arrTextos[0] + " title=" + arrTextos[0] + " align=middle  src='" + sRutaPM + "alta/images/Eliminar.gif' /> </div>";



                 //Cambio la configuracion de edicion del grid
                 var currentSettings = grid.AllowAddNew;
                 grid.AllowAddNew = 1;
                 grid.AllowUpdate = 1;


                 var NewCelda = Newrow.getCellFromKey("EDITAR");
                 var celdaHtml = NewCelda.getElement()
                 celdaHtml.innerHTML = EditarHTML

                 var NewCelda = Newrow.getCellFromKey("USUARIOS");
                 var celdaHtml = NewCelda.getElement()
                 celdaHtml.innerHTML = UsuariosHTML

                 var NewCelda = Newrow.getCellFromKey("ELIMINAR");
                 var celdaHtml = NewCelda.getElement()
                 celdaHtml.innerHTML = EliminarHTML

                 var NewCelda = Newrow.getCellFromKey("STATUS");
                 NewCelda.setValue('A') //Indicamos que es un alta de tarea

                 var NewCelda = Newrow.getCellFromKey("ID");
                 NewCelda.setValue(sIdProyecto)

                 var NewCelda = Newrow.getCellFromKey("EMP");
                 NewCelda.setValue(0)

                 if (parseInt(hito) == 1) {
                     var NewCelda = Newrow.getCellFromKey("COL_HITO");
                     var celdaHtml = NewCelda.getElement()
                     celdaHtml.innerHTML = HitoHTML
                     var CeldaHito = Newrow.getCellFromKey("HITO")
                     CeldaHito.setValue("1")
                 } else {
                     var CeldaHito = Newrow.getCellFromKey("HITO")
                     CeldaHito.setValue("0")
                 }

                 if (obs != '') {
                     var NewCelda = Newrow.getCellFromKey("COMENTARIOS");
                     var celdaHtml = NewCelda.getElement()
                     celdaHtml.innerHTML = ComentarioHTML
                 }

                 var CeldaCodTarea = Newrow.getCellFromKey("CODIGO")
                 CeldaCodTarea.setValue(codTarea )

                 var CeldaDen = Newrow.getCellFromKey("TAREADEN")
                 CeldaDen.setValue(denTarea)


                 switch (oNumFases.value) {
                     case '1':
                         {
                             var CeldaCodNiv4 = Newrow.getCellFromKey("COD_NIV4")
                             if (codNiv1)
                                 CeldaCodNiv4.setValue(codNiv1)

                             var CeldaDenNiv4 = Newrow.getCellFromKey("DEN_NIV4")
                             if (denNiv1)
                                 CeldaDenNiv4.setValue(denNiv1)
                             break
                         }
                     case '2':
                         {
                             var CeldaCodNiv4 = Newrow.getCellFromKey("COD_NIV4")
                             if (codNiv2)
                                 CeldaCodNiv4.setValue(codNiv2)

                             var CeldaCodNiv3 = Newrow.getCellFromKey("COD_NIV3")
                             if (codNiv1)
                                 CeldaCodNiv3.setValue(codNiv1)


                             var CeldaDenNiv4 = Newrow.getCellFromKey("DEN_NIV4")
                             if (denNiv2)
                                 CeldaDenNiv4.setValue(denNiv2)

                             var CeldaDenNiv3 = Newrow.getCellFromKey("DEN_NIV3")
                             if (denNiv1)
                                 CeldaDenNiv3.setValue(denNiv1)
                             break
                         }
                     case '3':
                         {
                             var CeldaCodNiv4 = Newrow.getCellFromKey("COD_NIV4")
                             if (codNiv3)
                                 CeldaCodNiv4.setValue(codNiv3)

                             var CeldaCodNiv3 = Newrow.getCellFromKey("COD_NIV3")
                             if (codNiv2)
                                 CeldaCodNiv3.setValue(codNiv2)

                             var CeldaCodNiv2 = Newrow.getCellFromKey("COD_NIV2")
                             if (codNiv1)
                                 CeldaCodNiv2.setValue(codNiv1)

                             var CeldaDenNiv4 = Newrow.getCellFromKey("DEN_NIV4")
                             if (denNiv3)
                                 CeldaDenNiv4.setValue(denNiv3)

                             var CeldaDenNiv3 = Newrow.getCellFromKey("DEN_NIV3")
                             if (denNiv2)
                                 CeldaDenNiv3.setValue(denNiv2)

                             var CeldaDenNiv2 = Newrow.getCellFromKey("DEN_NIV2")
                             if (denNiv1)
                                 CeldaDenNiv2.setValue(denNiv1)

                             break
                         }

                     case '4':
                         {
                             var CeldaCodNiv4 = Newrow.getCellFromKey("COD_NIV4")
                             if (codNiv4)
                                 CeldaCodNiv4.setValue(codNiv4)

                             var CeldaCodNiv3 = Newrow.getCellFromKey("COD_NIV3")
                             if (codNiv3)
                                 CeldaCodNiv3.setValue(codNiv3)

                             var CeldaCodNiv2 = Newrow.getCellFromKey("COD_NIV2")
                             if (codNiv2)
                                 CeldaCodNiv2.setValue(codNiv2)

                             var CeldaCodNiv1 = Newrow.getCellFromKey("COD_NIV1")
                             if (codNiv1)
                                 CeldaCodNiv1.setValue(codNiv1)


                             var CeldaDenNiv4 = Newrow.getCellFromKey("DEN_NIV4")
                             if (denNiv4 )
                                 CeldaDenNiv4.setValue(denNiv4)

                             var CeldaDenNiv3 = Newrow.getCellFromKey("DEN_NIV3")
                             if (denNiv3)
                                 CeldaDenNiv3.setValue(denNiv3)

                             var CeldaDenNiv2 = Newrow.getCellFromKey("DEN_NIV2")
                             if (denNiv2)
                                 CeldaDenNiv2.setValue(denNiv2)

                             var CeldaDenNiv1 = Newrow.getCellFromKey("DEN_NIV1")
                             if (denNiv1)
                                 CeldaDenNiv1.setValue(denNiv1)
                             break
                         }
                 }
                 var CeldaFecInicio = Newrow.getCellFromKey("FECEST_INICIO")
                 if (fecIni != '') {
                     var fIni = new Date(fecIni.substring(6, 10), parseInt(fecIni.substring(3, 5))-1, fecIni.substring(0, 2))
                     CeldaFecInicio.setValue(fIni.toString())
                 } else
                     CeldaFecInicio.setValue('')

                 if (parseInt(hito)==0) { //Si no es Hito
                     var CeldaFecFin = Newrow.getCellFromKey("FECEST_FIN")
                     if (fecFin != '') {
                         var fFin = new Date(fecFin.substring(6, 10), parseInt(fecFin.substring(3, 5))-1, fecFin.substring(0, 2))
                         CeldaFecFin.setValue(fFin.toString())
                     } else
                         CeldaFecFin.setValue('')

                     var CeldaHorasAsig = Newrow.getCellFromKey("HORAS_ASIG")
                     if ( horas  == '')
                         CeldaHorasAsig.setValue(0)
                     else
                         CeldaHorasAsig.setValue(horas)


                     var CeldaGradoAvance = Newrow.getCellFromKey("GRADO_AVANCE")
                     if (gradoA == '' )
                         CeldaGradoAvance.setValue(0)
                     else
                         CeldaGradoAvance.setValue(gradoA)
                 }
                 else {
                     var CeldaFecFin = Newrow.getCellFromKey("FECEST_FIN")
                     var CeldaHorasAsig = Newrow.getCellFromKey("HORAS_ASIG")
                     var CeldaGradoAvance = Newrow.getCellFromKey("GRADO_AVANCE")

                     CeldaFecFin.setValue('')
                     CeldaHorasAsig.setValue(0)
                     CeldaGradoAvance.setValue(0)

                 }


                 var CeldaEstado = Newrow.getCellFromKey("ESTADO")
                 if (estado != '') {
                     var arrEstado = estado.split("-")
                     CeldaEstado.setValue(arrEstado[1])
                     var CeldaCodEstado = Newrow.getCellFromKey("CODESTADO")
                     CeldaCodEstado.setValue(arrEstado[0])
                 } else {
                     //se le asigna el estado por defecto del proyecto
                     var arrEstado = oEstadoDefecto.value.toString().split("-")
                     var CeldaEstado = Newrow.getCellFromKey("ESTADO")
                     CeldaEstado.setValue(arrEstado[1])
                     var CeldaCodEstado = Newrow.getCellFromKey("CODESTADO")
                     CeldaCodEstado.setValue(arrEstado[0])
                 }
                 
                 var CeldaObs = Newrow.getCellFromKey("OBS")
                 CeldaObs.setValue(obs)

                 var CeldaNum = Newrow.getCellFromKey("NUM")

                 var CeldaNumOld = Newrow.getCellFromKey("NUM_OLD")

                 var grid = igtbl_getGridById("<%=uwgDesgloseActividad.ClientID%>");

                 var x = 0;
                 var bFind=0
                 for (x = 0; x <= grid.Rows.length - 1; x++) {
                     var row = grid.Rows.getRow(x);
                     var celda = row.getCellFromKey("NUM");
                     if (celda.getValue() == num) {
                         CeldaNum.setValue(MaxFila + 1)
                         CeldaNumOld.setValue(MaxFila + 1)
                         MaxFila = MaxFila + 1
                         bFind = 1
                         break;
                     }
                 }
                 if (bFind == 0) {
                     CeldaNum.setValue(num)
                     CeldaNumOld.setValue(num)
                 }
                              
                 if (parseInt(num) > MaxFila)
                     MaxFila = parseInt(num)

                 grid.AllowUpdate = 2;
                 grid.AllowAddNew = currentSettings;

                 var oElimFilas = document.getElementById("<%=ibEliminarFilas.ClientID%>")
                 oElimFilas.style.visibility = "visible"
                 $find("<%=ModalProgress.ClientID%>").hide();
             }
             sNumLineas = parseInt(sNumLineas) + parseInt(arrLineas.length)
            }
        }
    }
    
</script>


<div>
    <div class="cabeceraTituloDesglose"><asp:Label ID="lblTituloDesglose" runat="server" >dDesgloseActividad</asp:Label></div>
    <div>
         <div style="border: 1px solid #CCCCCC;">  
    <table>
    <tr>
        <td>
            
            <asp:Panel ID="panSinPaginacion" runat="server" Visible="True" width="100%">
                
                    <table border="0" width="100%">
                    <tr>
                        
                        <td align="right" width="15%">
                            <table border="0">
                                <tr>
                                    <td><asp:ImageButton ID="ibEliminarFilas" runat="server" SkinID="Excel"  OnClientClick="EliminarTodas();return false;" AlternateText='dEliminarFilas' ImageUrl="~/App_Pages/PMWEB/alta/images/eliminar.gif" /></td>
                                    <td><asp:ImageButton ID="ibAñadir" runat="server" SkinID="Excel"  OnClientClick="Añadir();return false;" AlternateText='dAñadir' ImageUrl="~/App_Pages/PMWEB/alta/images/Anyadir.gif" /></td>
                                    <td><asp:ImageButton ID="ibSubirExcel" runat="server" SkinID="Excel" ImageUrl="~/App_Pages/PMWEB/alta/images/Excelu.GIF"  OnClientClick="SubirExcel();return false;" AlternateText='dSubirExcel' /></td>
                                    <td><asp:ImageButton ID="ibBajarExcel" runat="server" SkinID="Excel" ImageUrl="~/App_Pages/PMWEB/alta/images/Exceld.GIF"  OnClientClick="BajarExcel();return false;" AlternateText='dBajarExcel'/></td>                
                                 </tr>
                            </table>
                        </td>
                    </tr>
                    </table>
                
            </asp:Panel>
        </td>
    </tr>
    <tr>
    <td>
    <div id="DivGrid" style="width:100%">
        <igtbl:UltraWebGrid id="uwgDesgloseActividad" 
            runat="server" Width="325px"  
             Height="200px" >
           
            <Bands>
            
            
            <igtbl:UltraGridBand DataKeyField="" AddButtonCaption="" AddButtonToolTipText="" AllowColSizing="Free" >
                
                
                <Columns>
                <igtbl:TemplatedColumn Key="ELIMINAR" Width="20px">
                    <CellTemplate>
                            <table style="width: 100%; display: inline" cellspacing="0" cellpadding="0">
                            <tr>
                                <td><img runat="server" alt="dEliminar" id="imgEliminar" border="0" align="middle" src="~/App_Pages/PMWEB/alta/images/eliminar.gif" unselectable="on"/></td>
                            </tr>
                            </table> 
                    </CellTemplate>
                </igtbl:TemplatedColumn>
                <igtbl:TemplatedColumn Key="EDITAR" Width="20px">
                    <CellTemplate>
                            <table style="width: 100%; display: inline" cellspacing="0" cellpadding="0">
                            <tr>
                                <td><img runat="server" alt="dEditar" id="btnEditar" border="0" align="middle" src="~/App_Pages/PMWEB/alta/images/Lapiz.png" unselectable="on"/></td>
                            </tr>
                            </table> 
                    </CellTemplate>
                </igtbl:TemplatedColumn>
                
                <igtbl:TemplatedColumn Key="COL_HITO"  Width="25px">            
                    <CellTemplate>
                            <table style="width: 100%; display: inline" cellspacing="0" cellpadding="0">
                            <tr>
                                <td><img runat="server" alt="dHito" id="imgHito" border="0" align="middle" src="~/App_Pages/PMWEB/alta/images/Hito.gif" unselectable="on"/></td>
                            </tr>
                            </table> 
                    </CellTemplate>
                </igtbl:TemplatedColumn>
                <igtbl:TemplatedColumn Key="COMENTARIOS"  Width="25px">            
                    <CellTemplate>
                            <table style="width: 100%; display: inline" cellspacing="0" cellpadding="0">
                            <tr>
                                <td><img runat="server" alt="dComentarios" id="imgComentario" border="0" align="middle" src="~/App_Pages/PMWEB/alta/images/SolicitudesPMSmall.gif" unselectable="on"/></td>
                            </tr>
                            </table> 
                    </CellTemplate>
                </igtbl:TemplatedColumn>
                <igtbl:TemplatedColumn Key="USUARIOS"   Width="245px">            
                    <CellTemplate>
                            <table style="width: 100%; display: inline" cellspacing="0" cellpadding="0">
                            <tr>
                            <td><asp:Label Font-Names="Verdana" Font-Size="8pt" Width="220px" Height="15px" ID="lblUsuarios" runat="server" ></asp:Label> </td>
                            <td id="cellUsuarios" runat="server" style="FONT-SIZE: 1pt" class="fsstyentrydd" valign="middle" width="12px" align="center"  unselectable="on" ><img runat="server" alt="dAbrirUsuarios" id="btnUsuarios" border="0" align="middle" unselectable="on" /> 
                            </td>
                            </tr>
                            </table>
                    </CellTemplate>
                </igtbl:TemplatedColumn>
                
                </Columns>
            </igtbl:UltraGridBand>
    </Bands>

    <DisplayLayout Version="4.00" Name="ctl00xuwgDesgloseActividad" RowHeightDefault="20px" AllowColSizingDefault="NotSet"
                rowselectorsdefault="No" AllowUpdateDefault="No" AllowAddNewDefault="Yes"    >
    <FrameStyle Font-Names="Verdana"  Font-Size="8pt" Height="210px" Width="100%" BorderStyle="None"></FrameStyle>

    <ClientSideEvents  CellClickHandler="uwgDesgloseActividad_CellClickHandler"></ClientSideEvents>

    <HeaderStyleDefault BorderColor="Gray" BorderWidth="1px" BorderStyle="Solid">
    </HeaderStyleDefault>
    <RowStyleDefault>
    <Padding Left="3px"></Padding>

    <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
    </RowStyleDefault>
    
    <ActivationObject BorderStyle="None" BorderWidth="0px" AllowActivation="False" BorderColor="Transparent"></ActivationObject>
    </DisplayLayout>
 
    </igtbl:UltraWebGrid>
    
    <igtxt:WebDateTimeEdit ID="WebDateTimeEdit1" runat="server"></igtxt:WebDateTimeEdit>
    </div>
    </td>
    </tr>
    </table>   
         </div> 
         <asp:HiddenField ID="hid_ReadOnly" runat="server" />
         <asp:HiddenField ID="hid_Oblig" runat="server" />
         <asp:HiddenField ID="hid_NumFases" runat="server" />
         <asp:HiddenField ID="hid_EstadoDefecto" runat="server" />
         <asp:HiddenField ID="hid_LineaProve" runat="server" />
</div> 
    <asp:Button ID="btnPopUpHidden" runat="server" Style="display:none"/>
    <asp:Panel ID="pnlComentarios" runat="server" cssClass="modalPopup" style="display:none; max-width:400px;max-height:300px; width:400px;" >
     <div>
	       <div id="divCabecera" style="display: table-cell; vertical-align: middle; height:45px;padding-top:12px; text-align:center">
	            <asp:Label ID="lblCabecera" runat="server" Text="dActividad XXXXXXXX" CssClass="titulo"  ></asp:Label>
	       </div>
	       <div id="divEdicion" runat="server">
	           <div style="text-align:center">
	                <asp:TextBox ID="txtComentarios" runat="server" TextMode="MultiLine" Width="360px" Height="100px"></asp:TextBox>
	           </div>
	           <br />
	           <div style="width:48%; text-align:right ; float:left; padding-right:2% ">
	                <asp:Button  ID="btnOk" runat="server" Text="dAceptar"  OnClientClick="ComentariosOK(1); return false;" />
	           </div>
	           <div style="width:48%; float:left; padding-left:2%">
	                <asp:Button  ID="btnCancelar" runat="server" Text="dCancelar"  />
	           </div>
	       </div>
	       <div id="divConsulta" runat="server" >
	           <div style="text-align:center; width:100%">
	                <asp:Label ID="lblComentarios" CssClass="Normal" runat="server" Width="360px" Height="100px" Style="text-align:left" ></asp:Label>
	           </div>
	           <br />
	           <div style="width:100%; text-align:center; padding-bottom:10px">
	                <asp:Button  ID="btnOkReadOnly" runat="server" Text="dAceptar" OnClientClick="ComentariosOK(0); return false;" />
	           </div>
	       </div>
	 </div>

    </asp:Panel>   
    <ajx:ModalPopupExtender ID="mpeComentarios" CancelControlID="btnCancelar" BackgroundCssClass="modalBackground"
        Enabled="true" PopupControlID="pnlComentarios" TargetControlID ="btnPopUpHidden" 
        RepositionMode="RepositionOnWindowResizeAndScroll" runat="server">
    </ajx:ModalPopupExtender>
     <asp:Panel ID="pnlEliminarTarea" runat="server" cssClass="modalPopup" style="display:none; max-width:300px;max-height:120px; width:300px; height:120px;" >
     <div>
	       <div id="divCuerpoEliminarTarea" runat="server">
	           <div style="text-align:center; padding-top:15px" >
	               <asp:Label ID="lblEliminarTarea" runat="server" CssClass="caption" Width="260px" Style="text-align:center" ></asp:Label>
	           </div>
	           <div style="text-align:center">
	               <asp:Label ID="lblDeseaContinuar" runat="server" CssClass="caption"  Width="260px" Style="text-align:center" ></asp:Label>
	           </div>
	           <br />
	           <div style="width:48%; text-align:right; float:left; padding-right:2% ">
	                <asp:Button  ID="btnAceptarEliminarTarea" runat="server" Text="dAceptar"  OnClientClick="EliminarOK(); return false;" />
	           </div>
	           <div style="width:48%; float:left; padding-left:2%">
	                <asp:Button  ID="btnCancelarEliminarTarea" runat="server" Text="dCancelar"  />
	           </div>
	       </div>
	 </div>
    </asp:Panel>   

    <ajx:ModalPopupExtender ID="mpeEliminarTarea" CancelControlID="btnCancelarEliminarTarea" BackgroundCssClass="modalBackground"
        Enabled="true" PopupControlID="pnlEliminarTarea" TargetControlID ="btnPopUpHidden" 
        RepositionMode="RepositionOnWindowResizeAndScroll" runat="server">
    </ajx:ModalPopupExtender>
    
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="upProgress" style="display:none; position:absolute; width:180px; height:65px;">
			<div style="position: relative; top: 30%; text-align: center;">
				<asp:Image ID="ImgProgress" runat="server" ImageUrl="~/App_Pages/PMWEB/alta/images/cargando.gif" style="vertical-align:middle"  />
			</div>
    </asp:Panel>
	<ajx:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress" BackgroundCssClass="modalBackground"
			PopupControlID="panelUpdateProgress" DropShadow="false" RepositionMode="RepositionOnWindowResizeAndScroll"  />
</div> 