﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AyudaDesglose.aspx.vb" Inherits="Fullstep.FSNWeb.AyudaDesglose" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="CapaTituloPanelInfo" style="padding-left:1em; line-height:3em;">
            <asp:Label runat="server" ID="lblTituloAyudaDesglose" CssClass="TituloPopUpPanelInfo"></asp:Label>
        </div>
        <div class="CapaSubTituloPanelInfo" style="padding-left:1em; line-height:2em;">
            <asp:Label runat="server" ID="lblSubTituloAyudaDesglose" CssClass="SubTituloPopUpPanelInfo"></asp:Label>
        </div>
        <div>
            <asp:Label runat="server" ID="lblInstruccionesAyudaDesglose" CssClass="Texto12" style="line-height:1.5em;"></asp:Label>
        </div>
    </form>
</body>
</html>
