Public Class desgloseControl
    Inherits System.Web.UI.UserControl

#Region "Web Form Designer Generated Code"
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    End Sub
    Protected WithEvents cmdAnyadir As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdGenerarExcelDesglose As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdCargarExcelDesglose As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents fuDesglose As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents cmdSubmit As System.Web.UI.HtmlControls.HtmlInputSubmit
    Protected WithEvents tblTituloDesglose As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblDesglose As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblDesgloseHidden As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblGeneral As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents divDesglose As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblInfoExcel As System.Web.UI.WebControls.Label
    Protected WithEvents lblCargandoDesglose As System.Web.UI.WebControls.Label
    Private designerPlaceholderDeclaration As System.Object
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
#End Region
#Region "Constantes"
    Private mlId As Long
    Private mlCampoOrigen As Long
    Private msTabContainer As String
    Private mlInstancia As Long
    Private mlVersion As Long
    Private mlVersionCert As Long
    Private mbSoloLectura As Boolean
    Private mbFuerzaLectura As Boolean
    Private moTextos As DataTable
    Private mlSolicitud As Long
    Private msProveedor As String
    Private moAcciones As DataTable
    Private moDSEstados As DataSet
    Private msTitulo As String
    Private msAyuda As String
    Private bCentroRelacionado As Boolean
    Private sCodOrgCompras As String
    Private sIDCampoOrgCompras As String
    Private bDepartamentoRelacionado As Boolean
    Private sUnidadOrganizativa As String
    Private sidDataEntryFORMOrgCompras As String
    Private sCodCentro As String
    Private sidDataEntryFORMCentro As String
    Private sIDCampoCentro As String
    Private bPM As Boolean
    Private sEliminar As String
    Private sCopiar_Eliminar As String
    Private mbObservador As Boolean
    Private mbTieneIdCampo As Boolean
    Private sAdjun As String
    Private idAdjun As String
    Private sidDataEntryFORMMaterial As String
    Private msMonedaSolicitud As String
    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
#End Region
    Dim pages As System.Web.Configuration.PagesSection = ConfigurationManager.GetSection("system.web/pages")
    Dim theme As String = pages.Theme
#Region "PROPIEDADES"
    Property Titulo() As String
        Get
            Return msTitulo
        End Get
        Set(ByVal Value As String)
            msTitulo = Value
        End Set
    End Property
    Property Solicitud() As Long
        Get
            Return mlSolicitud
        End Get
        Set(ByVal Value As Long)
            mlSolicitud = Value
        End Set
    End Property
    Property Proveedor() As String
        Get
            Return mlSolicitud
        End Get
        Set(ByVal Value As String)
            msProveedor = Value
        End Set
    End Property
    Property Instancia() As Long
        Get
            Return mlInstancia
        End Get
        Set(ByVal Value As Long)
            mlInstancia = Value
        End Set
    End Property
    Property Version() As Long
        Get
            Return mlVersion
        End Get
        Set(ByVal Value As Long)
            mlVersion = Value
        End Set
    End Property
    ''' <summary>
    ''' Los certificados, una vez q hay instancia, para determinar si los campos del desglose son visible/editables usan la version
    ''' de la tabla certificado. Tras un guardado en mlVersion tienes la version q hab�a al entrar en el detallecertificado pero en
    ''' bbdd tienes otra versi�n (puedes grabar n veces sin hacer postback). Esta propiedad es para contener la versi�n de bbdd y 
    ''' pasarla al oCampo.LoadInstDesglose.
    ''' </summary>
    ''' <remarks>Llamada desde: Page_load; Tiempo:0</remarks>
    Property VersionCert() As Long
        Get
            Return mlVersionCert
        End Get
        Set(ByVal Value As Long)
            mlVersionCert = Value
        End Set
    End Property
    Property Campo() As Long
        Get
            Return mlId
        End Get
        Set(ByVal Value As Long)
            mlId = Value
        End Set
    End Property
    Property CampoOrigen() As Long
        Get
            Return mlCampoOrigen
        End Get
        Set(ByVal Value As Long)
            mlCampoOrigen = Value
        End Set
    End Property
    Property TabContainer() As String
        Get
            Return msTabContainer
        End Get
        Set(ByVal Value As String)
            msTabContainer = Value
        End Set
    End Property
    Property SoloLectura() As Boolean
        Get
            Return mbSoloLectura
        End Get
        Set(ByVal Value As Boolean)
            mbSoloLectura = Value

        End Set
    End Property
    Property FuerzaLectura() As Boolean
        Get
            Return mbFuerzaLectura
        End Get
        Set(ByVal Value As Boolean)
            mbFuerzaLectura = Value
        End Set
    End Property
    Property Acciones() As DataTable
        Get
            Return moAcciones
        End Get
        Set(ByVal Value As DataTable)
            moAcciones = Value
        End Set
    End Property
    Property DSEstados() As DataSet
        Get
            Return moDSEstados
        End Get
        Set(ByVal Value As DataSet)
            moDSEstados = Value
        End Set
    End Property
    Property CentroRelacionado() As Boolean
        Get
            Return bCentroRelacionado
        End Get
        Set(ByVal Value As Boolean)
            bCentroRelacionado = Value
        End Set
    End Property
    Property CodOrgCompras() As String
        Get
            Return sCodOrgCompras
        End Get
        Set(ByVal Value As String)
            sCodOrgCompras = Value
        End Set
    End Property
    Property CodCentro() As String
        Get
            Return sCodCentro
        End Get
        Set(ByVal Value As String)
            sCodCentro = Value
        End Set
    End Property
    Private _sUnidadOrganizativa As String
    Public Property UnidadOrganizativa() As String
        Get
            Return _sUnidadOrganizativa
        End Get
        Set(ByVal value As String)
            _sUnidadOrganizativa = value
        End Set
    End Property
    Private _sCentroCoste As String
    Public Property CentroCoste() As String
        Get
            Return _sCentroCoste
        End Get
        Set(ByVal value As String)
            _sCentroCoste = value
        End Set
    End Property
    Property idCampoOrgCompras() As String
        Get
            Return sIDCampoOrgCompras
        End Get
        Set(ByVal Value As String)
            sIDCampoOrgCompras = Value
        End Set
    End Property
    Property idCampoCentro() As String
        Get
            Return sIDCampoCentro
        End Get
        Set(ByVal Value As String)
            sIDCampoCentro = Value
        End Set
    End Property
    Property idDataEntryFORMMaterial() As String
        Get
            Return sidDataEntryFORMMaterial
        End Get
        Set(ByVal Value As String)
            sidDataEntryFORMMaterial = Value
        End Set
    End Property
    Property idDataEntryFORMOrgCompras() As String
        Get
            Return sidDataEntryFORMOrgCompras
        End Get
        Set(ByVal Value As String)
            sidDataEntryFORMOrgCompras = Value
        End Set
    End Property
    Property idDataEntryFORMCentro() As String
        Get
            Return sidDataEntryFORMCentro
        End Get
        Set(ByVal Value As String)
            sidDataEntryFORMCentro = Value
        End Set
    End Property
    Private sidDataEntryUnidadOrganizativa As String
    Public Property idDataEntryFORMUnidadOrganizativa() As String
        Get
            Return sidDataEntryUnidadOrganizativa
        End Get
        Set(ByVal value As String)
            sidDataEntryUnidadOrganizativa = value
        End Set
    End Property
    Private _sidDataEntryFORMCentroCoste As String
    Public Property idDataEntryFORMCentroCoste() As String
        Get
            Return _sidDataEntryFORMCentroCoste
        End Get
        Set(ByVal value As String)
            _sidDataEntryFORMCentroCoste = value
        End Set
    End Property
    Private _sidCampoPartida As String
    Property idCampoPartida() As String
        Get
            Return _sidCampoPartida
        End Get
        Set(ByVal Value As String)
            _sidCampoPartida = Value
        End Set
    End Property
    Property PM() As Boolean
        Get
            Return bPM
        End Get
        Set(ByVal Value As Boolean)
            bPM = Value
        End Set
    End Property
    Property Observador() As Boolean
        Get
            Return mbObservador
        End Get
        Set(ByVal Value As Boolean)
            mbObservador = Value

        End Set
    End Property
    Property Ayuda() As String
        Get
            Return msAyuda
        End Get
        Set(ByVal Value As String)
            msAyuda = Value
        End Set
    End Property
    Property TieneIdCampo() As Boolean
        Get
            Return mbTieneIdCampo
        End Get
        Set(ByVal Value As Boolean)
            mbTieneIdCampo = Value
        End Set
    End Property
    Property MonedaSolicitud() As String
        Get
            Return msMonedaSolicitud
        End Get
        Set(ByVal Value As String)
            msMonedaSolicitud = Value
        End Set
    End Property
    Private mTipoSolicitud As TiposDeDatos.TipoDeSolicitud
    Public Property TipoSolicitud() As TiposDeDatos.TipoDeSolicitud
        Get
            Return mTipoSolicitud
        End Get
        Set(ByVal value As TiposDeDatos.TipoDeSolicitud)
            mTipoSolicitud = value
        End Set
    End Property
    Private mEnCursoDeAprobacion As Boolean
    Public Property EnCursoDeAprobacion() As Boolean
        Get
            Return mEnCursoDeAprobacion
        End Get
        Set(ByVal value As Boolean)
            mEnCursoDeAprobacion = value
        End Set
    End Property
#End Region
    Dim oUser As FSNServer.User
    Dim FSNServer As FSNServer.Root
    Dim sIdi As String
    Dim mipage As FSNPage
    Dim mioFSDSEntryAnyoPartida As DataEntry.GeneralEntry = Nothing
    ''' Revisado por: blp. Fecha: 04/10/2011
    ''' <summary>
    ''' Cargar un desglose dado.
    ''' NOTA: Ahora en 2008 los name q no creamos explicitamente usan $ para separar los trozos de los 
    ''' diferentes controles del q es hijo (ejemplo uwtGrupos$_ctl0x$128903$fsentry646101$_4__646180),
    ''' antes en 2003 se usaba _ y por ello resultaba q el id y el name eran iguales. Esto importa pq 
    ''' los request se hacen por nombre no por id. En 2003 se le pasaba el id y funcionaba, pero ya no.
    ''' NOTA: Una vez dada de alta la solicitud. Si una columna es readonly (salvo fechas ,material, art,
    ''' den art, presup, unidad, org compras y centro) se debe mostrar como si fuera un entry pero sin crear 
    ''' el entry realmente. Se crea un html con un bot�n tres puntos si hay la celda tiene valor y el tipo de 
    ''' campo de la columna usa bot�n tres puntos.
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">evento de sistema</param>        
    ''' <remarks>Llamada desde: alta/alta.aspx   alta/campos.aspx   alta/NWalta.aspx    certificados/altacertificado.aspx
    ''' certificados/desglose.aspx  certificados/detallecertificado.aspx  noconformidad/altanoconformidad.aspx
    ''' noconformidad/desglose.aspx noconformidad/detallenoconformidad.aspx     noconformidad/noconformidadrevisor.aspx
    ''' seguimiento/desglose.aspx   seguimiento/detallesolicitud.aspx   seguimiento/Nwdetallesolicitud.aspx
    ''' workflow/aprobacion.aspx    workflow/desglose.aspx  workflow/gestioninstancia.aspx
    ''' workflow/gestiontrasladada.aspx     workflow/instanciaasignada.aspx     workflow/Nwgestioninstancia.aspx
    '''; Tiempo m�ximo:0,1</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Page.IsPostBack Then Exit Sub

        Dim bHayCamposModificables, bHayCamposModificablesNumerico As Boolean
        Dim sScript, sScriptFechasSuministro, sClase, sCodOrgCompras2 As String
        Dim bPopUp As Boolean
        Dim sCodCentro2 As String = Nothing
        Dim oRowLista() As DataRow

        sCodOrgCompras2 = String.Empty : sScriptFechasSuministro = String.Empty
        sClase = String.Empty
        mipage = CType(Page, FSNPage)
        FSNServer = mipage.FSNServer
        sIdi = mipage.Idioma
        oUser = mipage.FSNUser
        mipage.ModuloIdioma = TiposDeDatos.ModulosIdiomas.DesgloseControl

        cmdAnyadir.Value = mipage.Textos(0)
        lblInfoExcel.Text = mipage.Textos(51)
        cmdGenerarExcelDesglose.Value = mipage.Textos(36)
        cmdCargarExcelDesglose.Value = mipage.Textos(37)
        sEliminar = mipage.Textos(13)
        sCopiar_Eliminar = ""

        lblInfoExcel.Attributes.Add("OcultarBotonesExcel", SoloLectura.ToString.ToLower)

        Dim idCampo, idCampoOrigen, CopiaCampoDef_Padre As Long
        If mlId = Nothing Then
            idCampo = Request("campo")
            idCampoOrigen = Request("campo_origen")
            bPopUp = True
        Else
            idCampo = mlId
            idCampoOrigen = mlCampoOrigen
            bPopUp = False
        End If

        If mlInstancia = Nothing Then mlInstancia = Request("Instancia")
        If mlSolicitud = Nothing Then mlSolicitud = Request("Solicitud")
        If msProveedor = Nothing Then msProveedor = Request("Proveedor")

        Dim lFavorito As Long = strToInt(Request("IdFavorito"))
        Dim bFavorito As Boolean = (Request("Favorito") = "1")
        Dim oHid As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidNumRowsGrid As New System.Web.UI.HtmlControls.HtmlInputHidden
        oHid.ID = "numRows"
        oHidNumRowsGrid.ID = "numRowsGrid"

        If Request("Observador") <> "" AndAlso Request("Observador") = "1" Then mbObservador = True
        If Request("SoloLectura") = "1" Or mbObservador Then mbSoloLectura = True
        If Request("DesgloseLectura") = "1" Then mbFuerzaLectura = True

        cmdAnyadir.Visible = Not mbSoloLectura

        Dim oCampo As FSNServer.Campo
        oCampo = FSNServer.Get_Object(GetType(FSNServer.Campo))
        oCampo.Id = idCampo

        If mlInstancia > 0 Then
            oCampo.LoadInst(mlInstancia, sIdi, oUser.CodPersona)
        Else
            oCampo.Load(sIdi, mlSolicitud)
        End If

        If Not oCampo.PermiteExcel Then
            lblInfoExcel.Attributes.Add("OcultarBotonesExcel", True.ToString.ToLower)
        Else
            lblInfoExcel.Attributes.Add("OcultarBotonesExcel", SoloLectura.ToString.ToLower)
        End If

        If oCampo.Vinculado Then
            cmdAnyadir.Attributes("onclick") = "copiarFilaVinculada('" & Me.ClientID & "','" & idCampo.ToString & "','" & mlInstancia & "')"
            lblInfoExcel.Visible = False
            cmdCargarExcelDesglose.Visible = False
            cmdGenerarExcelDesglose.Visible = False
        Else
            cmdAnyadir.Attributes("onclick") = "copiarFilaVacia('" & Me.ClientID & "','" & idCampo.ToString & "'," & IIf(bFavorito, 1, 0) & ",null,null,null)"
        End If

        If oCampo.LineasPreconf > 0 Then tblGeneral.Rows.RemoveAt(1)

        If moAcciones Is Nothing OrElse Not oCampo.Tipo = TipoCampoPredefinido.NoConformidad Then
            moAcciones = Nothing
            moDSEstados = Nothing
        End If

        Dim oDS, oDsDefecto, oDSPres As DataSet
        Dim oRow, oRowDesglose, oRowAcc, oNewRow As DataRow
        Dim oFSEntry, oFSDSEntry, oFSDSEntryDefecto As DataEntry.GeneralEntry
        Dim iTipoGS As TiposDeDatos.TipoCampoGS
        Dim iTipo As TiposDeDatos.TipoGeneral
        Dim oArts As FSNServer.Articulos
        Dim oPres1 As FSNServer.PresProyectosNivel1
        Dim oPres2 As FSNServer.PresContablesNivel1
        Dim oPres3 As FSNServer.PresConceptos3Nivel1
        Dim oPres4 As FSNServer.PresConceptos4Nivel1
        Dim oProve As FSNServer.Proveedor
        Dim sKeyMaterial, sKeyArticulo, sRestrictMat, sKeyPais, sKeyDestino, sTexto, sValor, sValorMat, sToolTip, sDenominacion, oPresup As String
        Dim sCodOrgComprasFORM, sKeyOrgCompras, sKeyUnidadOrganizativa, sKeyOrgComprasDataCheck, sKeyCentro, sCodArticulo, sInstanciaMoneda, sCodPais, sCodCentroCoste, sOrden, sCodDest As String
        Dim arrCamposMaterial(0) As Long
        Dim arrMat(4) As String
        Dim sKeyTipoRecepcion As String = "0"
        Dim oGMN1s As FSNServer.GruposMatNivel1 = Nothing
        Dim oGMN1 As FSNServer.GrupoMatNivel1 = Nothing
        Dim oGMN2 As FSNServer.GrupoMatNivel2 = Nothing
        Dim oGMN3 As FSNServer.GrupoMatNivel3 = Nothing
        Dim iNivel, lIdPresup, iContadorPres, iAnyo As Integer
        Dim dPorcent As Decimal
        Dim arrPresupuestos() As String
        Dim arrPresup(2) As String
        Dim oRowHeader As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oCellHeader As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oDivHeader As System.Web.UI.HtmlControls.HtmlControl
        Dim oSpanHeader As System.Web.UI.HtmlControls.HtmlContainerControl
        Dim ilGMN1 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN1
        Dim ilGMN2 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN2
        Dim ilGMN3 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN3
        Dim ilGMN4 As Integer = FSNServer.LongitudesDeCodigos.giLongCodGMN4
        Dim bCentroRelacionadoFORM, bQA As Boolean

        oDSPres = Nothing
        oFSEntry = Nothing
        sToolTip = String.Empty : sInstanciaMoneda = String.Empty : sKeyMaterial = String.Empty
        sKeyPais = String.Empty : sKeyOrgCompras = String.Empty : sKeyUnidadOrganizativa = String.Empty : sKeyOrgComprasDataCheck = String.Empty
        sCodOrgComprasFORM = String.Empty : sKeyCentro = String.Empty : sKeyDestino = String.Empty : sKeyArticulo = String.Empty : sCodArticulo = String.Empty : sCodPais = String.Empty
        sDenominacion = String.Empty : sCodDest = String.Empty : sCodCentroCoste = String.Empty

        If Not IsNothing(Request("CentroRelacionadoFORM")) Then
            bCentroRelacionadoFORM = Request("CentroRelacionadoFORM")
        ElseIf bCentroRelacionado = True Then
            bCentroRelacionadoFORM = True
            sCodOrgComprasFORM = sCodOrgCompras
        End If

        If Not IsNothing(Request("CodOrgComprasFORM")) Then sCodOrgComprasFORM = Request("CodOrgComprasFORM")

        sRestrictMat = String.Empty
        If oCampo.GMN1 IsNot Nothing Then sRestrictMat = oCampo.GMN1 + (Space(ilGMN1 - oCampo.GMN1.Length))
        If oCampo.GMN2 IsNot Nothing Then sRestrictMat += oCampo.GMN2 + (Space(ilGMN2 - oCampo.GMN2.Length))
        If oCampo.GMN3 IsNot Nothing Then sRestrictMat += oCampo.GMN3 + (Space(ilGMN3 - oCampo.GMN3.Length))
        If oCampo.GMN4 IsNot Nothing Then sRestrictMat += oCampo.GMN4 + (Space(ilGMN4 - oCampo.GMN4.Length))

        sOrden = "ORDEN"
        If mlInstancia > 0 Then
            If mTipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado Then
                oDS = oCampo.LoadInstDesglose(sIdi, mlInstancia, oUser.CodPersona, mlVersion, msProveedor, , , True, mlVersionCert, oUser.DateFmt)
                sOrden = "ORDEN1"
                bQA = True
                If oCampo.RecienPublicado Then Me.cmdAnyadir.Visible = False
            ElseIf mTipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad Then
                oDS = oCampo.LoadInstDesglose(sIdi, mlInstancia, oUser.CodPersona, mlVersion, msProveedor, mEnCursoDeAprobacion, , True, , oUser.DateFmt)
                sOrden = "ORDEN1"
                bQA = True
            Else
                Dim bSacarNumLinea As Boolean = {TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras, TiposDeDatos.TipoDeSolicitud.PedidoExpress, TiposDeDatos.TipoDeSolicitud.PedidoNegociado}.Contains(mTipoSolicitud)
                oDS = oCampo.LoadInstDesglose(sIdi, mlInstancia, oUser.CodPersona, mlVersion, msProveedor, True, mbObservador, True, , oUser.DateFmt, IIf(Request("Bloque") <> Nothing, CLng(Request("Bloque")), 0), IIf(Request("Rol") <> Nothing, CLng(Request("Rol")), 0), bSacarNumLinea)
            End If
            CopiaCampoDef_Padre = oCampo.Id

            sInstanciaMoneda = msMonedaSolicitud
        Else
            If {TiposDeDatos.TipoDeSolicitud.Certificado, TiposDeDatos.TipoDeSolicitud.NoConformidad}.Contains(mTipoSolicitud) Then
                oDS = oCampo.LoadDesglose(sIdi, oCampo.IdSolicitud, , , , oUser.DateFmt, , )
                sOrden = "ORDEN1"
                bQA = True
            Else
                Dim bSacarNumLinea As Boolean = {TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras, TiposDeDatos.TipoDeSolicitud.PedidoExpress, TiposDeDatos.TipoDeSolicitud.PedidoNegociado}.Contains(mTipoSolicitud)
                If bPopUp Then
                    oDS = oCampo.LoadDesglose(sIdi, oCampo.IdSolicitud, oUser.CodPersona, , , , bSacarNumLinea, InstanciaImportar)
                Else
                    oDS = oCampo.LoadDesglose(sIdi, oCampo.IdSolicitud, oUser.CodPersona, bFavorito, lFavorito, , bSacarNumLinea, InstanciaImportar)
                End If
            End If
        End If
        oDsDefecto = oDS.Copy
        Dim cont As Integer
        Dim todasLectura As Boolean = True
        For cont = 0 To oDS.Tables(0).Rows.Count - 1
            If oDS.Tables(0).Rows(cont).Item("ESCRITURA") = 1 Or DBNullToSomething(oDS.Tables(0).Rows(cont).Item("TIPO_CAMPO_GS")) = TiposDeDatos.IdsFicticios.DesgloseVinculado Then
                todasLectura = False
                Exit For
            End If
        Next
        If todasLectura Then
            lblInfoExcel.Attributes.Add("OcultarBotonesExcel", True)
            cmdAnyadir.Visible = False
        End If

        If Not Page.ClientScript.IsStartupScriptRegistered("idSolicitudActual") Then _
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "idSolicitudActual", "var idSolicitudActual='" & oCampo.IdSolicitud & "';", True)

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "favorito") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "favorito", "var favorito = " & bFavorito.ToString.ToLower & ";", True)

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosErrorExportacionExcelDesglose") Then
            Dim sVariableJavascriptTextosPantalla As String = "var TextosErrorExportacionExcelDesglose = new Array();"
            sVariableJavascriptTextosPantalla &= "TextosErrorExportacionExcelDesglose[1]='" & JSText(mipage.Textos(52)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosErrorExportacionExcelDesglose[2]='" & JSText(Replace(mipage.Textos(56), "###", ConfigurationManager.AppSettings("MaximoNumeroLineasImportarExcel"))) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosErrorExportacionExcelDesglose", sVariableJavascriptTextosPantalla, True)
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeMonedaObligatoria") Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeMonedaObligatoria", "<script>var sMensajeMonedaObligatoria = '" & JSText(mipage.Textos(26)) & "'</script>")
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeCargarProveSumi_CambioArt") Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeCargarProveSumi_CambioArt", "<script>var sMensajeCargarProveSumi_CambioArt = '" & JSText(mipage.Textos(49)) & "'</script>")
        End If

        If oDS.Tables.Count > 0 Then
            Dim columnaPRE_ID_CAMPO_NC As New DataColumn("PRE_ID_CAMPO_NC", System.Type.GetType("System.Int32"))
            columnaPRE_ID_CAMPO_NC.DefaultValue = 0
            oDS.Tables(0).Columns.Add(columnaPRE_ID_CAMPO_NC)

            If oDS.Tables.Contains("LINEAS") Then
                Dim columnaPRE_ID_CAMPO_HIJO_NC As New DataColumn("PRE_ID_CAMPO_HIJO_NC", System.Type.GetType("System.Int32"))
                columnaPRE_ID_CAMPO_HIJO_NC.DefaultValue = 0
                oDS.Tables("LINEAS").Columns.Add(columnaPRE_ID_CAMPO_HIJO_NC)
            End If
        End If

        If Not moDSEstados Is Nothing Then
            If Not moDSEstados.Tables(0).Columns.Contains("PRE_ID_CAMPO_NC") Then
                moDSEstados.Tables(0).Columns.Add(New DataColumn("PRE_ID_CAMPO_NC", System.Type.GetType("System.Int32")))
            End If
            For Each oRow In moDSEstados.Tables(0).Rows
                oDS.Tables(0).ImportRow(oRow)
                If bPopUp Then
                    oDS.Tables(0).Rows(oDS.Tables(0).Rows.Count - 1).Item("ID") = oRow.Item("ID").ToString
                    oDS.Tables(0).Rows(oDS.Tables(0).Rows.Count - 1).Item("PRE_ID_CAMPO_NC") = 0
                Else
                    oDS.Tables(0).Rows(oDS.Tables(0).Rows.Count - 1).Item("PRE_ID_CAMPO_NC") = oCampo.IdGrupo.ToString
                    oDS.Tables(0).Rows(oDS.Tables(0).Rows.Count - 1).Item("ID") = oRow.Item("ID").ToString
                End If
                oDS.Tables(0).Rows(oDS.Tables(0).Rows.Count - 1).Item("SAVE_VALUES") = 0
            Next
            '---
            For Each oRow In moDSEstados.Tables(1).Rows
                oDS.Tables(1).ImportRow(oRow)
            Next
            '---
            If Not moDSEstados.Tables(2).Columns.Contains("PRE_ID_CAMPO_HIJO_NC") Then
                moDSEstados.Tables(2).Columns.Add(New DataColumn("PRE_ID_CAMPO_HIJO_NC", System.Type.GetType("System.Int32")))
            End If

            For Each oRow In moDSEstados.Tables(2).Select("CAMPO_PADRE= " & idCampo.ToString)
                oNewRow = oDS.Tables("LINEAS").NewRow
                oNewRow.Item("LINEA") = oRow.Item("LINEA")
                oNewRow.Item("CAMPO_PADRE") = oRow.Item("CAMPO_PADRE")
                If bPopUp Then
                    oNewRow.Item("PRE_ID_CAMPO_HIJO_NC") = 0
                    oNewRow.Item("CAMPO_HIJO") = oRow.Item("CAMPO_HIJO").ToString
                Else
                    oNewRow.Item("PRE_ID_CAMPO_HIJO_NC") = oCampo.IdGrupo.ToString
                    oNewRow.Item("CAMPO_HIJO") = oRow.Item("CAMPO_HIJO").ToString
                End If
                oNewRow.Item("VALOR_NUM") = oRow.Item("VALOR_NUM")
                oNewRow.Item("VALOR_TEXT") = oRow.Item("VALOR_TEXT")
                oDS.Tables("LINEAS").Rows.Add(oNewRow)
            Next
        End If

        cmdCargarExcelDesglose.Attributes.Add("onclick", "CargarExcelDesglose(" & idCampo & ")")
        cmdGenerarExcelDesglose.Attributes.Add("onclick", "GenerarPlantillaExcel(" & idCampo & ")")
        fuDesglose.Attributes.Add("idCampoDesglose", idCampo)
        fuDesglose.Attributes.Add("nombreDesglose", Me.ClientID)

        'A�adimos la tabla de datos a una cache por si da a generar excel no tener que volver a consultar a base de datos (ponemos con expiracion tipo 2 que es mas tiempo)
        'Pasamos tb el tipo de solicitud para las validaciones de los datos del Excel. Si el tipo de solicitud es pedido express y tiene parametro interno de que puede actuar como negociado, pasamos negociado
        HttpContext.Current.Cache.Insert("infoDesglose_" & oUser.Cod & "_" & idCampo, {mTipoSolicitud, oDS}, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin_2"), 0))

        Dim otblRow As System.Web.UI.HtmlControls.HtmlTableRow
        Dim otblCell As System.Web.UI.HtmlControls.HtmlTableCell
        Dim otblCellData As System.Web.UI.HtmlControls.HtmlTableCell = Nothing
        Dim otblRowData As System.Web.UI.HtmlControls.HtmlTableRow
        Dim desgoblig As Boolean = False
        Dim oHidden As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oImg As System.Web.UI.WebControls.HyperLink
        Dim oImgCabecera As System.Web.UI.WebControls.Image
        Dim oImgAyuda As System.Web.UI.WebControls.HyperLink = Nothing
        Dim lContador As Integer = 0
        Dim bAlgunaColumnaVisible As Boolean = False

        oRowHeader = New System.Web.UI.HtmlControls.HtmlTableRow
        otblRow = New System.Web.UI.HtmlControls.HtmlTableRow

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "claveArrayEntrys" + idCampo.ToString) Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "claveArrayEntrys" + idCampo.ToString, "<script>var arrEntrys_" + idCampo.ToString + " = new Array()</script>")
        End If
        Dim iLineas As Integer = 0
        Dim iMaxIndex As Integer = 0
        Dim NAdjuntos As Integer
        Dim AuxNAdjuntos As String
        Dim AuxNAdjunto As String
        Dim bEstaAdjun As Boolean
        Dim i As Integer
        Dim distinctCounts As IEnumerable(Of Int32)
        Dim lineasReal As Integer() = Nothing
        Dim sIdCampoProveedor As String = String.Empty
        Dim nomDesglose As String = Request("nomDesglose")
        If bPopUp Then
            iLineas = Request(nomDesglose + "$_numRows")
            iMaxIndex = Request(nomDesglose + "$_numTotRows")
            ReDim lineasReal(iMaxIndex - 1)
            Dim contador As Integer = 0
            For campoCalc As Integer = 1 To iMaxIndex
                If Not Request(nomDesglose + "$_" + campoCalc.ToString + "__Deleted") Is Nothing Then
                    lineasReal(contador) = campoCalc
                    contador += 1
                End If
            Next
            iLineas = contador
        Else
            If oDS.Tables(0).Rows.Count > 0 AndAlso oDS.Tables("LINEAS").Rows.Count > 0 Then
                distinctCounts = From row In oDS.Tables("LINEAS")
                                 Select row.Field(Of Int32)("LINEA")
                                 Order By "LINEA" Ascending
                                 Distinct
                iLineas = distinctCounts.Count
                ReDim lineasReal(iLineas - 1)
                iMaxIndex = oDS.Tables("LINEAS").Select("", "LINEA DESC")(0).Item("LINEA")
                For contador As Integer = 0 To iLineas - 1
                    lineasReal(contador) = distinctCounts(contador)
                Next
            End If
        End If

        'Para optimizar la velocidad de ejecuci�n de MontarFormularioSubmit en jsAlta.js
        Dim sJavaScript As String
        If Not bPopUp Then
            If Not Page.ClientScript.IsStartupScriptRegistered("claveArrayDesgloses" & CStr(idCampo)) Then _
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "claveArrayDesgloses" & CStr(idCampo), "<script>htControlArrVinc[""" & idCampo & """]=""" & CStr(idCampo) & """;</script>")
        End If

        Dim conta As Integer = 0
        For i = 1 To iMaxIndex
            If lineasReal.Contains(i) Then
                conta += 1
                sJavaScript = "<script>htControlFilas[""" & idCampo & "_" & CStr(i) & """]=""" & CStr(conta) & """;</script>"
                If Not Page.ClientScript.IsStartupScriptRegistered("ControlFilas" & idCampo & "_" & CStr(i)) Then _
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "ControlFilas" & idCampo & "_" & CStr(i), sJavaScript)
                If Not bPopUp Then
                    sJavaScript = "<script>htControlFilasVinc[""" & idCampo & "_" & CStr(i) & """]=""" & CStr(conta) & """;</script>"
                    If Not Page.ClientScript.IsStartupScriptRegistered("ControlFilasVinc" & idCampo & "_" & CStr(i)) Then _
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "ControlFilasVinc" & idCampo & "_" & CStr(i), sJavaScript)

                    If oCampo.MoverLinea Then
                        sJavaScript = "<script>htControlArrVincMovible[""" & idCampo & "_" & CStr(i) & """]=""##Si##"";</script>"
                    Else
                        sJavaScript = "<script>htControlArrVincMovible[""" & idCampo & "_" & CStr(i) & """]=""##No##"";</script>"
                    End If
                    If Not Page.ClientScript.IsStartupScriptRegistered("ControlArrVincMovible" & idCampo & "_" & CStr(i)) Then _
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "ControlArrVincMovible" & idCampo & "_" & CStr(i), sJavaScript)
                End If
            End If
        Next

        Dim vLineasColumnas(iLineas) As Integer
        Dim oDSRowAdjun As DataRow
        Dim keys(3) As DataColumn
        Dim oDeletedColumn As New DataColumn("DELETED")
        Dim oIndexColumn As New DataColumn("INDEX")

        keys(0) = oDS.Tables(2).Columns("LINEA")
        keys(1) = oDS.Tables(2).Columns("CAMPO_PADRE")
        keys(2) = oDS.Tables(2).Columns("CAMPO_HIJO")
        keys(3) = oDS.Tables(2).Columns("PRE_ID_CAMPO_HIJO_NC")

        oDeletedColumn.DataType = System.Type.GetType("System.Boolean")
        oDeletedColumn.DefaultValue = True
        oDeletedColumn.AllowDBNull = False

        oIndexColumn.DataType = System.Type.GetType("System.Int32")
        oDS.Tables(2).PrimaryKey = keys

        oDS.Tables(2).Columns.Add(oDeletedColumn)
        oDS.Tables(2).Columns.Add(oIndexColumn)

        Dim findTheseVals(3) As Object
        Dim sIdCampoDesglose As String = ""
        Dim sIdCampoHijoDesglose As String = ""

        tblDesglose.Rows.Add(oRowHeader)
        For Each oRow In oDS.Tables(2).Rows
            If oRow.Item("PRE_ID_CAMPO_HIJO_NC") = 0 Then
                sIdCampoHijoDesglose = oRow.Item("CAMPO_HIJO").ToString
            Else
                sIdCampoHijoDesglose = oRow.Item("PRE_ID_CAMPO_HIJO_NC").ToString + oRow.Item("CAMPO_HIJO").ToString
            End If
            'Guardamos los campos GS ocultos
            Dim sOculto As String
            Dim oRow2 As DataRow
            For Each oRow2 In oDS.Tables(0).Rows
                If oRow2.Item("PRE_ID_CAMPO_NC") = 0 Then
                    sIdCampoDesglose = oRow2.Item("ID").ToString
                Else
                    sIdCampoDesglose = oRow2.Item("PRE_ID_CAMPO_NC").ToString + oRow2.Item("ID").ToString
                End If
                If (DBNullToSomething(oRow.Item("TIPO_CAMPO_GS"))) Then

                    If (oRow2.Item("VISIBLE") = 0) And sIdCampoHijoDesglose = sIdCampoDesglose Then
                        Dim sLinea As String = oRow.Item("LINEA")
                        If DBNullToSomething(oRow.Item("VALOR_NUM")) Is Nothing Then
                            sOculto = oRow.Item("TIPO_CAMPO_GS") & "_" & oRow.Item("CAMPO_PADRE") & "__" & oRow.Item("VALOR_TEXT") & "___" & oRow.Item("LINEA")
                        Else
                            sOculto = oRow.Item("TIPO_CAMPO_GS") & "_" & oRow.Item("CAMPO_PADRE") & "__" & oRow.Item("VALOR_NUM") & "___" & oRow.Item("LINEA")
                        End If

                        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "OcultosDes" & oRow.Item("TIPO_CAMPO_GS") & "_" & oRow.Item("CAMPO_PADRE") & "_" & sIdCampoHijoDesglose & "_" & oRow.Item("LINEA")) Then
                            Page.ClientScript.RegisterStartupScript(Page.GetType(), "OcultosDes" & oRow.Item("TIPO_CAMPO_GS") & "_" & oRow.Item("CAMPO_PADRE") & "_" & sIdCampoHijoDesglose & "_" & oRow.Item("LINEA"), "<script>arrGSOcultos[arrGSOcultos.length] = '" & sOculto & "'</script>")
                        End If
                    End If
                End If
            Next
        Next

        'ESTO ES LO QUE ESTABA
        If Not mbSoloLectura Then
            oCellHeader = New System.Web.UI.HtmlControls.HtmlTableCell
            oCellHeader.ID = "COL_Copiar_Eliminar"
            oCellHeader.InnerHtml = sCopiar_Eliminar
            oCellHeader.Attributes("class") = "cabeceraDesglose"
            oRowHeader.Cells.Add(oCellHeader)

            oImg = New System.Web.UI.WebControls.HyperLink
            oImg.ImageUrl = "../alta/images/Flecha_Dcha.GIF"
            oImg.NavigateUrl = "javascript:void(null)"
            oImg.Attributes("onclick") = "popupDesgloseClickEvent(this, this.parentNode,'" + Me.ClientID + "','##newIndex##'," + idCampo.ToString() + ",'izda'," + oCampo.LineasPreconf.ToString() + ",'" + IIf(oCampo.MoverLinea, "##Si##", "##No##") + "'," + oCampo.IdSolicitud.ToString() + "," + mlInstancia.ToString() + "," + CStr(IIf(bPopUp, 1, 0)) + ",event)"
            oImg.ID = "cmdCopiar_Eliminar"

            otblCell = New System.Web.UI.HtmlControls.HtmlTableCell
            otblCell.ID = "celdaCopiar_Eliminar"
            otblCell.Controls.Add(oImg)
            otblCell.Align = "center"
            otblRow.Cells.Add(otblCell)
        End If

        Dim sCodPaisDefecto As String = ""
        Dim sCodOrgCompraDefecto As String = ""
        Dim sCodCentroDefecto As String = ""
        Dim sCodUnidadOrgDefecto As String = ""

        CtrlHayDatosPorDefecto(oDsDefecto, oCampo, bQA, sCodPaisDefecto, sCodOrgCompraDefecto, sCodCentroDefecto, sCodUnidadOrgDefecto)

        Dim bAyuda As Boolean
        For Each oRow In oDS.Tables(0).Rows
            bAyuda = False 'Inicialmente no se muestra el icono de ayuda, se comprobara posteriormente
            If oRow.Item("PRE_ID_CAMPO_NC") <> 0 Then
                sIdCampoDesglose = oRow.Item("PRE_ID_CAMPO_NC").ToString + oRow.Item("ID").ToString
            Else
                sIdCampoDesglose = oRow.Item("ID").ToString
            End If
            If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Material Then
                sKeyMaterial = "fsentry" + sIdCampoDesglose
                ReDim Preserve arrCamposMaterial(UBound(arrCamposMaterial) + 1)
                arrCamposMaterial(UBound(arrCamposMaterial)) = oRow.Item("ID")
            ElseIf DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
                sKeyArticulo = "fsentry" + sIdCampoDesglose
            ElseIf DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.OrganizacionCompras Then
                sKeyOrgCompras = "fsentry" + sIdCampoDesglose
            End If

            If oRow.Item("VISIBLE") = 1 Then
                oCellHeader = New System.Web.UI.HtmlControls.HtmlTableCell
                oCellHeader.ID = "COL_" + sIdCampoDesglose

                Dim oLbl As System.Web.UI.HtmlControls.HtmlGenericControl
                oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
                If DBNullToSomething(oRow.Item("OBLIGATORIO")) = 1 Then
                    If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.IdsFicticios.DesgloseVinculado Then
                        oLbl.InnerHtml = "(*)" + mipage.Textos(19)
                        oRow.Item("DEN_" & sIdi) = mipage.Textos(19)
                    Else
                        oLbl.InnerHtml = "(*)" + DBNullToSomething(oRow.Item("DEN_" & sIdi))
                    End If
                    oLbl.Style("font-weight") = "bold"
                ElseIf DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.IdsFicticios.NumLinea Then
                    oLbl.InnerHtml = mipage.Textos(24)
                    oRow.Item("DEN_" & sIdi) = mipage.Textos(24)
                    oLbl.Style("font-weight") = "bold"
                Else
                    oLbl.InnerHtml = DBNullToSomething(oRow.Item("DEN_" & sIdi))
                    If (oLbl.InnerHtml.Length) > 30 Then
                        oLbl.InnerHtml = Left(oLbl.InnerHtml, 30)
                        oLbl.InnerHtml = oLbl.InnerHtml & "..."
                        oCellHeader.Width = "200"
                        oCellHeader.Attributes.Add("title", DBNullToSomething(oRow.Item("DEN_" & sIdi)))
                    End If
                End If

                If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.PrecioUnitario Then
                    oLbl.Attributes.Add("nombreMoneda", "")
                    oLbl.InnerHtml = oLbl.InnerHtml & " (" & If(mlInstancia > 0, sInstanciaMoneda, If(msMonedaSolicitud <> "", msMonedaSolicitud, "")) & ")"
                End If

                oRow.Item("DEN_" & sIdi) = oRow.Item("DEN_" & sIdi).ToString.Replace("'", "\'")
                oRow.Item("DEN_" & sIdi) = oRow.Item("DEN_" & sIdi).ToString.Replace("""", "\""")
                '----------------------------------------------------------------------------------------------------------------------------------------------------------------
                'Comprobaremos si se le tiene que poner la imagen de ayuda en la cabecera
                'La comprobacion con "PRE_ID_CAMPO_NC" es debida a campos de No conformidades(campos de desglose de acciones Comentario/Estado Interno)
                'que no tienen id en COPIA_CAMPO y por lo tanto no tendran ayuda, ya que no se le puede configurar la ayuda en el dise�o de formularios
                If (Not DBNullToStr(oRow.Item("AYUDA_" & sIdi)) = "") OrElse Not IsDBNull(oRow.Item("FORMULA")) AndAlso oRow.Item("PRE_ID_CAMPO_NC") = 0 Then
                    oImgAyuda = New System.Web.UI.WebControls.HyperLink
                    oImgAyuda.ImageUrl = "images\help.gif"
                    oImgAyuda.NavigateUrl = "javascript:void(null)"
                    If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.IdsFicticios.DesgloseVinculado Then
                        If Not oRow.Table.Columns("id_campo") Is Nothing Then
                            oImgAyuda.Attributes("onclick") = "show_help(" + (oRow.Item("ID_CAMPO") - 2000).ToString + "," + mlInstancia.ToString + ")"
                        Else
                            oImgAyuda.Attributes("onclick") = "show_help(" + (oRow.Item("ID") - 2000).ToString + "," & IIf(mlInstancia > 0, mlInstancia.ToString & ",", " null,") + mlSolicitud.ToString + ")"
                        End If
                    Else
                        If Not oRow.Table.Columns("id_campo") Is Nothing Then
                            oImgAyuda.Attributes("onclick") = "show_help(" + oRow.Item("ID_CAMPO").ToString + "," + mlInstancia.ToString + ")"
                        Else
                            oImgAyuda.Attributes("onclick") = "show_help(" + oRow.Item("ID").ToString + "," & IIf(mlInstancia > 0, mlInstancia.ToString & ",", " null,") + mlSolicitud.ToString + ")"
                        End If
                    End If
                    bAyuda = True
                End If

                If oRow.Item("TIPO") = 3 Then
                    oImg = New System.Web.UI.WebControls.HyperLink
                    oImg.ImageUrl = "../_common/images/sumatorio.gif"

                    oImg.NavigateUrl = "javascript:void(null)"

                    If mlInstancia > 0 Then
                        oImg.Attributes("onclick") = "show_help(" + sIdCampoDesglose + "," + mlInstancia.ToString + ")"
                    Else
                        oImg.Attributes("onclick") = "show_help(" + sIdCampoDesglose + ",null," + mlSolicitud.ToString + ")"
                    End If

                    Dim oTblSum As System.Web.UI.HtmlControls.HtmlTable
                    Dim oTblRowSum As System.Web.UI.HtmlControls.HtmlTableRow
                    Dim oTblCellSum As System.Web.UI.HtmlControls.HtmlTableCell

                    oTblSum = New System.Web.UI.HtmlControls.HtmlTable
                    oTblSum.Border = 0
                    oTblSum.CellPadding = 0
                    oTblSum.CellSpacing = 0
                    oTblSum.Width = "100%"
                    oTblRowSum = New System.Web.UI.HtmlControls.HtmlTableRow
                    oTblCellSum = New System.Web.UI.HtmlControls.HtmlTableCell
                    oTblCellSum.Width = "10%"
                    oTblCellSum.Controls.Add(oImg)
                    oTblRowSum.Controls.Add(oTblCellSum)
                    oTblSum.Controls.Add(oTblRowSum)
                    oTblCellSum = New System.Web.UI.HtmlControls.HtmlTableCell
                    oTblCellSum.Controls.Add(oLbl)
                    oTblCellSum.Attributes("class") = "cabeceraDesglose"
                    oTblRowSum.Controls.Add(oTblCellSum)
                    If bAyuda Then
                        'Se mete el icono de ayuda en la cabecera del campo
                        oTblCellSum = New System.Web.UI.HtmlControls.HtmlTableCell
                        oTblCellSum.Width = "5%"
                        oTblCellSum.Style("align") = "right"
                        oTblCellSum.Controls.Add(oImgAyuda)
                        oTblRowSum.Controls.Add(oTblCellSum)
                    End If
                    oCellHeader.Controls.Add(oTblSum)
                Else
                    If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.PRES1 Or DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Pres2 _
                            Or DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Pres3 Or DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Pres4 Then
                        Dim temp As String
                        temp = "javascript:show_pres_desde_cabecera('" + Request("Input") + "','" + sIdCampoDesglose + "','" + oRow.Item("TIPO_CAMPO_GS").ToString() + "','" + mlInstancia.ToString() + "','" + mlSolicitud.ToString() + "');"

                        Dim oTblSum As System.Web.UI.HtmlControls.HtmlTable
                        Dim oTblRowSum As System.Web.UI.HtmlControls.HtmlTableRow
                        Dim oTblCellSum As System.Web.UI.HtmlControls.HtmlTableCell

                        oTblSum = New System.Web.UI.HtmlControls.HtmlTable
                        oTblSum.Border = 0
                        oTblSum.CellPadding = 0
                        oTblSum.CellSpacing = 0
                        oTblSum.Width = "100%"
                        oTblRowSum = New System.Web.UI.HtmlControls.HtmlTableRow
                        oTblCellSum = New System.Web.UI.HtmlControls.HtmlTableCell
                        oTblCellSum.Controls.Add(oLbl)
                        oTblCellSum.Attributes("class") = "cabeceraDesglose"
                        If bAyuda Then
                            oTblCellSum.Style.Add("width ", "165px")
                        Else
                            oTblCellSum.Style.Add("width ", "190px")
                        End If
                        oTblRowSum.Controls.Add(oTblCellSum)

                        If Not ((oRow.Item("ESCRITURA") = 0) Or mbSoloLectura) Then
                            oImgCabecera = New System.Web.UI.WebControls.Image
                            oImgCabecera.ImageUrl = "../_common/images/3puntos.gif"
                            oTblCellSum = New System.Web.UI.HtmlControls.HtmlTableCell
                            'oTblCellSum.Attributes.Add("class", "fsstyentrydd")
                            oTblCellSum.Attributes.Add("onclick", temp)
                            oTblCellSum.Controls.Add(oImgCabecera)
                            oTblRowSum.Controls.Add(oTblCellSum)
                        End If
                        If bAyuda Then
                            'Se mete el icono de ayuda en la cabecera del campo
                            oTblCellSum = New System.Web.UI.HtmlControls.HtmlTableCell
                            oTblCellSum.Width = "5%"
                            oTblCellSum.Style("align") = "right"
                            oTblCellSum.Style("padding-left") = "10px"
                            oTblCellSum.Controls.Add(oImgAyuda)
                            oTblRowSum.Controls.Add(oTblCellSum)
                        End If
                        oTblSum.Controls.Add(oTblRowSum)
                        oCellHeader.Controls.Add(oTblSum)
                    Else
                        If bAyuda Then
                            Dim oTblSum As System.Web.UI.HtmlControls.HtmlTable
                            Dim oTblRowSum As System.Web.UI.HtmlControls.HtmlTableRow
                            Dim oTblCellSum As System.Web.UI.HtmlControls.HtmlTableCell

                            oTblSum = New System.Web.UI.HtmlControls.HtmlTable
                            oTblSum.Border = 0
                            oTblSum.CellPadding = 0
                            oTblSum.CellSpacing = 0
                            oTblSum.Width = "100%"
                            oTblRowSum = New System.Web.UI.HtmlControls.HtmlTableRow
                            oTblSum.Controls.Add(oTblRowSum)
                            oTblCellSum = New System.Web.UI.HtmlControls.HtmlTableCell
                            oTblCellSum.Controls.Add(oLbl)
                            oTblCellSum.Attributes("class") = "cabeceraDesglose"
                            oTblRowSum.Controls.Add(oTblCellSum)

                            'Se mete el icono de ayuda en la cabecera del campo
                            oTblCellSum = New System.Web.UI.HtmlControls.HtmlTableCell
                            oTblCellSum.Width = "5%"
                            oTblCellSum.Style("align") = "right"
                            oTblCellSum.Controls.Add(oImgAyuda)
                            oTblRowSum.Controls.Add(oTblCellSum)

                            oCellHeader.Controls.Add(oTblSum)
                        Else
                            oCellHeader.Controls.Add(oLbl)
                        End If
                    End If
                End If
                iTipo = oRow.Item("SUBTIPO")
                iTipoGS = DBNullToSomething(oRow.Item("TIPO_CAMPO_GS"))
                oCellHeader.Attributes("class") = "cabeceraDesglose"
                oRowHeader.Cells.Add(oCellHeader)

                otblCell = New System.Web.UI.HtmlControls.HtmlTableCell

                oFSEntry = CrearObjetoDataEntry(oRow, oDS, oCampo, CopiaCampoDef_Padre, sIdCampoDesglose, idCampo, idCampoOrigen, iTipoGS, iTipo, bPopUp, sRestrictMat, arrMat, sKeyMaterial, sInstanciaMoneda, sIdCampoProveedor,
                                                sKeyTipoRecepcion, sKeyDestino, sKeyPais, iNivel, sCodPaisDefecto, sKeyOrgCompras, sOrden, sKeyUnidadOrganizativa, sCodUnidadOrgDefecto, sKeyOrgComprasDataCheck,
                                                sCodOrgCompraDefecto, bCentroRelacionadoFORM, sCodOrgComprasFORM, sKeyCentro, sCodCentroDefecto, sKeyArticulo, bHayCamposModificables, bHayCamposModificablesNumerico)

                otblCell.Controls.Add(oFSEntry)
                otblRow.Cells.Add(otblCell)
                If Not Page.ClientScript.IsStartupScriptRegistered(oFSEntry.ClientID) Then _
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), oFSEntry.ClientID, "<script>arrEntrys_" + idCampo.ToString + "[arrEntrys_" + idCampo.ToString + ".length] = '" + oFSEntry.InitScript + "';</script>")

                '####################################################################################################################################
                '####################################################################################################################################
                '####################################################################################################################################
                'inicio desglose
                Dim contaLinea As Integer = 0
                For i = 1 To iMaxIndex
                    If lineasReal.Contains(i) Then
                        contaLinea += 1
                        If contaLinea Mod 2 = 0 Then
                            sClase = "ugfilaalttablaDesglose"
                        Else
                            sClase = "ugfilatablaDesglose"
                        End If

                        Try
                            otblRowData = Me.tblDesglose.Rows(contaLinea)

                        Catch ex As Exception
                            otblRowData = New System.Web.UI.HtmlControls.HtmlTableRow
                            Me.tblDesglose.Rows.Add(otblRowData)
                            'jpa Tarea 187 --------------------------------------------------------------------------------
                            If Not mbSoloLectura Then
                                'la flecha de la izda
                                oImg = New System.Web.UI.WebControls.HyperLink
                                oImg.ImageUrl = "../alta/images/Flecha_Dcha.GIF"
                                oImg.NavigateUrl = "javascript:void(null)"
                                oImg.Attributes("onclick") = "popupDesgloseClickEvent(this, this.parentNode,'" + Me.ClientID + "'," + i.ToString() + "," + idCampo.ToString() + ",'izda'," + oCampo.LineasPreconf.ToString() + ",'" + IIf(oCampo.MoverLinea, "##Si##", "##No##") + "'," + oCampo.IdSolicitud.ToString() + "," + mlInstancia.ToString() + "," + CStr(IIf(bPopUp, 1, 0)) + ",event)"
                                oImg.ID = "cmdCopiar_Eliminar_Izda" + i.ToString + "_" + sIdCampoDesglose
                                otblCellData = New System.Web.UI.HtmlControls.HtmlTableCell
                                otblCellData.ID = "celdaCopiar_Eliminar_Izda" + i.ToString + "_" + sIdCampoDesglose
                                otblCellData.Controls.Add(oImg)
                                otblCellData.Attributes("class") = sClase
                                otblCellData.Align = "center"
                                otblRowData.Cells.Add(otblCellData)
                                vLineasColumnas(contaLinea - 1) += 1
                            End If
                            'fin jpa Tarea 187 --------------------------------------------------------------------------------
                        End Try

                        findTheseVals(0) = i
                        findTheseVals(1) = oCampo.IdCopiaCampo
                        If oCampo.IdCopiaCampo = Nothing Then
                            findTheseVals(1) = oCampo.Id
                        Else
                            findTheseVals(1) = oCampo.IdCopiaCampo
                        End If
                        findTheseVals(2) = oRow.Item("ID")
                        findTheseVals(3) = 0
                        oRowDesglose = oDS.Tables(2).Rows.Find(findTheseVals)

                        Dim Entry3Puntos As Boolean = False
                        If Not oRowDesglose Is Nothing Then
                            Entry3Puntos = (Len(DBNullToStr(oRowDesglose.Item("VALOR_TEXT"))) > 80)
                        End If

                        If mlInstancia > 0 AndAlso Not oRowDesglose Is Nothing AndAlso (oRow.Item("ESCRITURA") = "0" AndAlso oRow.Item("TIPO") <> 3 AndAlso
                            oRow.Item("SUBTIPO") <> TiposDeDatos.TipoGeneral.TipoFecha AndAlso
                            oRow.Item("SUBTIPO") <> TiposDeDatos.TipoGeneral.TipoArchivo AndAlso
                            oRow.Item("SUBTIPO") <> TiposDeDatos.TipoGeneral.TipoTextoLargo AndAlso
                            oRow.Item("SUBTIPO") <> TiposDeDatos.TipoGeneral.TipoEditor AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Material AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.DenArticulo AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.PrecioUnitario AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Unidad AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.UnidadPedido AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.OrganizacionCompras AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Centro AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.PRES1 AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Pres2 AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Pres3 AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Pres4 AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Partida AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.AnyoPartida AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.EstadoInternoNoConf AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Accion AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.IdsFicticios.Comentario AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.IdsFicticios.DesgloseVinculado AndAlso
                            DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.CentroCoste AndAlso
                            IIf(oCampo.Vinculado, (DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Cantidad), True) AndAlso
                            (Not ((oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio OrElse
                                oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoCorto OrElse
                                oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoString) AndAlso
                                DBNullToSomething(oRow.Item("INTRO")) = 0 AndAlso
                                (DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.SinTipo OrElse Entry3Puntos)
                            ))) Then

                            tblDesglose.Style.Add("width ", "100%") 'misma apariencia que con dataentrys. MOVER para que solo se ejecute una vez

                            Dim oDiv As System.Web.UI.HtmlControls.HtmlGenericControl
                            If Not oRowDesglose Is Nothing Then
                                oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")

                                oDiv = New System.Web.UI.HtmlControls.HtmlGenericControl("DIV")
                                If Not IsDBNull(oRowDesglose.Item("TIPO_CAMPO_GS")) AndAlso (DBNullToSomething(oRowDesglose.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Material _
                                    OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.NuevoCodArticulo _
                                    OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Unidad _
                                    OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.UnidadPedido _
                                    OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.FormaPago _
                                    OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.PRES1 _
                                    OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres2 _
                                    OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres3 _
                                    OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres4 _
                                    OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.UnidadOrganizativa _
                                    OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Almacen _
                                    OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Empresa) Then
                                    oDiv.Style.Item("width") = "200px"
                                ElseIf DBNullToSomething(oRowDesglose.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.DenArticulo _
                                    OrElse DBNullToSomething(oRowDesglose.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Proveedor _
                                    OrElse DBNullToSomething(oRowDesglose.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.ProveedorAdj Then
                                    oDiv.Style.Item("width") = "400px"
                                ElseIf oRow.Item("TIPO") = "6" OrElse DBNullToSomething(oRowDesglose.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Dest OrElse oRow("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo Then
                                    oDiv.Style.Item("width") = "250px"
                                ElseIf oRow("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo Then
                                    oDiv.Style.Item("width") = "250px"
                                ElseIf oRow("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio Then
                                    oDiv.Style.Item("width") = "200px"
                                ElseIf oRow("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoBoolean Then
                                    oDiv.Style.Item("width") = "50px"
                                Else
                                    oDiv.Style.Item("width") = "200px"
                                End If
                                If Not mbSoloLectura Then
                                    If DBNullToSomething(oRowDesglose.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Proveedor Then
                                        oDiv.Style.Item("width") = "150px"
                                    ElseIf oRow("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo Then
                                        oDiv.Style.Item("width") = "100px"
                                    End If
                                End If
                                oDiv.Style.Item("OVERFLOW") = "hidden"

                                If oRow("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo Then
                                    oLbl.Attributes("class") = "TipoArchivo"
                                End If
                                'En los presupuestos, las unidades organizativas y los departamentos, valor_text_cod indica si est� en baja l�gica o no.
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                    If DBNullToSomething(oRowDesglose.Item("VALOR_TEXT_COD")) = "1" AndAlso Not IsDBNull(oRowDesglose.Item("TIPO_CAMPO_GS")) AndAlso (oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.PRES1 OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres2 OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres3 OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres4 OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.UnidadOrganizativa OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Departamento) Then
                                        oLbl.Attributes("class") = "StringTachadotrasparent"
                                    End If
                                End If
                                oLbl.InnerHtml = EscribirValorCampo(oRow, oRowDesglose, oDS)
                                otblCellData = New System.Web.UI.HtmlControls.HtmlTableCell
                                otblCellData.Attributes("class") = sClase

                                If oRow("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoNumerico AndAlso Not {TiposDeDatos.TipoCampoGS.Almacen, TiposDeDatos.TipoCampoGS.Empresa}.Contains(DBNullToSomething(oRow.Item("TIPO_CAMPO_GS"))) Then
                                    otblCellData.Align = "right"
                                End If
                                otblCellData.NoWrap = True
                                otblCellData.Attributes("class") = sClase
                                otblCellData.ID = "fsdsentry_" + i.ToString + "_" + sIdCampoDesglose

                                If oLbl.InnerHtml <> "&nbsp;" Then
                                    Dim oImgPopUp As System.Web.UI.WebControls.HyperLink
                                    If oRow.Item("TIPO") = "6" AndAlso oRowDesglose.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                        If Not IsDBNull(oRowDesglose.Item("VALOR_TEXT_DEN")) Then
                                            Dim sTxt As String = oRowDesglose.Item("VALOR_TEXT_DEN").ToString()
                                            Dim sTxt2 As String
                                            If sTxt.IndexOf("]#[") > 0 Then
                                                sTxt2 = sTxt.Substring(2, sTxt.IndexOf("]#[") - 2)
                                            Else
                                                sTxt2 = sTxt.Substring(2, sTxt.Length - 3)
                                            End If

                                            oImgPopUp = New System.Web.UI.WebControls.HyperLink
                                            oImgPopUp.ImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(Fullstep.DataEntry.GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")
                                            oImgPopUp.NavigateUrl = "javascript:AbrirTablaExterna('" & oRow.Item("TABLA_EXTERNA") & "','" & sTxt2 & "')"
                                            oDiv.Controls.Add(oLbl)
                                            otblCellData.Controls.Add(CargarTextoYBotonEnTabla(oDiv, oImgPopUp))
                                        Else
                                            oDiv.Controls.Add(oLbl)
                                            otblCellData.Controls.Add(oDiv)
                                        End If
                                    ElseIf ((oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio OrElse oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo) AndAlso
                                        (IsDBNull(oRowDesglose.Item("TIPO_CAMPO_GS")) OrElse DBNullToSomething(oRowDesglose.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.DescrBreve OrElse DBNullToSomething(oRowDesglose.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.DescrDetallada OrElse DBNullToSomething(oRowDesglose.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Partida OrElse DBNullToSomething(oRowDesglose.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Activo)) _
                                        AndAlso (DBNullToSomething(oRow.Item("INTRO")) = "0") Then

                                        oImgPopUp = New System.Web.UI.WebControls.HyperLink
                                        oImgPopUp.ImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(Fullstep.DataEntry.GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")
                                        If oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo Then
                                            oImgPopUp.NavigateUrl = "javascript:AbrirFicherosAdjuntos('" & idAdjun & "','" & sIdCampoDesglose & "','" & mlInstancia & "',3)"
                                        ElseIf DBNullToSomething(oRowDesglose.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Partida Then
                                            oImgPopUp.NavigateUrl = "javascript:AbrirDetallePartida('" & oRow.Item("DEN_" & sIdi) & "','" & oRow.Item("PRES5") & "','" & oRowDesglose.Item("VALOR_TEXT") & "')"
                                        ElseIf DBNullToSomething(oRowDesglose.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Activo Then
                                            oImgPopUp.NavigateUrl = "javascript:AbrirDetalleActivo('" & oRowDesglose.Item("VALOR_TEXT") & "')"
                                        Else
                                            Dim textSourceId As String = String.Format("{0}_{1}_{2}", oFSEntry.IdContenedor, i.ToString(), sIdCampoDesglose)
                                            oImgPopUp.NavigateUrl = "javascript:AbrirPopUpTexto('" & Replace(oRow.Item("DEN_" & sIdi), "'", "\'") & "','" & textSourceId & "'," & IIf(oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo, "true", "false") & ")"
                                        End If
                                        oDiv.Controls.Add(oLbl)
                                        otblCellData.Controls.Add(CargarTextoYBotonEnTabla(oDiv, oImgPopUp))

                                    ElseIf Not IsDBNull(oRowDesglose.Item("TIPO_CAMPO_GS")) AndAlso (oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.PRES1 OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres2 OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres3 OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres4 OrElse oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Material) Then
                                        oImgPopUp = New System.Web.UI.WebControls.HyperLink
                                        oImgPopUp.ImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(Fullstep.DataEntry.GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")

                                        If oRowDesglose.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Material Then
                                            oImgPopUp.NavigateUrl = "javascript:AbrirMateriales('" & oRowDesglose.Item("VALOR_TEXT") & "')"
                                        Else
                                            oImgPopUp.NavigateUrl = "javascript:AbrirPresupuestos('" & oRowDesglose.Item("VALOR_TEXT") & "','" & sIdCampoDesglose & "','" & mlInstancia & "')"
                                        End If

                                        oDiv.Controls.Add(oLbl)
                                        otblCellData.Controls.Add(CargarTextoYBotonEnTabla(oDiv, oImgPopUp))

                                    ElseIf ((oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio OrElse oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo) AndAlso
                                        (IsDBNull(oRowDesglose.Item("TIPO_CAMPO_GS"))) AndAlso (DBNullToSomething(oRow.Item("INTRO")) = "1")) Then

                                        oImgPopUp = New System.Web.UI.WebControls.HyperLink
                                        oImgPopUp.ImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(Fullstep.DataEntry.GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")
                                        If (oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio OrElse oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo) Then
                                            Dim textSourceId As String = String.Format("{0}_{1}_{2}", oFSEntry.IdContenedor, i.ToString(), sIdCampoDesglose)
                                            oImgPopUp.NavigateUrl = "javascript:AbrirPopUpTexto('" & Replace(oRow.Item("DEN_" & sIdi), "'", "\'") & "','" & textSourceId & "'," & IIf(oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo, "true", "false") & ",'" & DBNullToSomething(oRowDesglose.Item("VALOR_TEXT")) & "')"
                                        End If
                                        oDiv.Controls.Add(oLbl)
                                        otblCellData.Controls.Add(CargarTextoYBotonEnTabla(oDiv, oImgPopUp))
                                    ElseIf DBNullToSomething(oRowDesglose.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Dest Then
                                        oImgPopUp = New System.Web.UI.WebControls.HyperLink
                                        oImgPopUp.ImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(Fullstep.DataEntry.GeneralEntry), "Fullstep.DataEntry.info.gif")
                                        oImgPopUp.NavigateUrl = "javascript:showDetalleDestino(null,'" & oRowDesglose.Item("VALOR_TEXT") & "')"
                                        oDiv.Controls.Add(oLbl)
                                        otblCellData.Controls.Add(CargarTextoYBotonEnTabla(oDiv, oImgPopUp))
                                    ElseIf DBNullToSomething(oRowDesglose.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.TotalLineaAdj Then
                                        oImgPopUp = New System.Web.UI.WebControls.HyperLink
                                        oImgPopUp.ImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(Fullstep.DataEntry.GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")
                                        oImgPopUp.NavigateUrl = "javascript:showDetalleTotalLineaAdj(" & mlInstancia & "," & oRowDesglose.Item("CAMPO_PADRE") & " , " & i & ",false)"

                                        oDiv.Controls.Add(oLbl)
                                        otblCellData.Controls.Add(CargarTextoYBotonEnTabla(oDiv, oImgPopUp))
                                    ElseIf DBNullToSomething(oRowDesglose.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.TotalLineaPreadj Then
                                        oImgPopUp = New System.Web.UI.WebControls.HyperLink
                                        oImgPopUp.ImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(Fullstep.DataEntry.GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")
                                        oImgPopUp.NavigateUrl = "javascript:showDetalleTotalLineaAdj(" & mlInstancia & "," & oRowDesglose.Item("CAMPO_PADRE") & " , " & i & ",true)"

                                        oDiv.Controls.Add(oLbl)
                                        otblCellData.Controls.Add(CargarTextoYBotonEnTabla(oDiv, oImgPopUp))
                                    Else
                                        oDiv.Controls.Add(oLbl)
                                        otblCellData.Controls.Add(oDiv)
                                    End If
                                Else
                                    oDiv.Controls.Add(oLbl)
                                    otblCellData.Controls.Add(oDiv)
                                End If

                                'hidden con el valor
                                Dim oHidCampo As New System.Web.UI.HtmlControls.HtmlInputHidden
                                oHidCampo.ID = i.ToString + "_" + sIdCampoDesglose
                                If oRow("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo Then
                                    oHidCampo.Value = idAdjun
                                ElseIf oRow("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoBoolean Then
                                    oHidCampo.Value = DBNullToSomething(oRowDesglose.Item("VALOR_BOOL"))
                                ElseIf oRow("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then
                                    oHidCampo.Value = DBNullToSomething(oRowDesglose.Item("VALOR_FEC"))
                                ElseIf oRow("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoNumerico OrElse (DBNullToSomething(oRow.Item("INTRO")) = 1 AndAlso DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = 0) Then
                                    oHidCampo.Value = DBNullToSomething(oRowDesglose.Item("VALOR_NUM"))
                                Else
                                    oHidCampo.Value = DBNullToSomething(oRowDesglose.Item("VALOR_TEXT"))
                                End If
                                otblCellData.Controls.Add(oHidCampo)

                                otblRowData.Cells.Add(otblCellData)
                            Else
                                otblCellData = New System.Web.UI.HtmlControls.HtmlTableCell
                                oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
                                oLbl.InnerHtml = "&nbsp;"
                                otblCellData.Controls.Add(oLbl)
                                otblCellData.Attributes("class") = sClase
                                otblRowData.Cells.Add(otblCellData)
                            End If

                            vLineasColumnas(contaLinea - 1) = vLineasColumnas(contaLinea - 1) + 1
                        Else
                            oFSDSEntry = CrearObjetoDataEntryDesglose(oFSEntry, sIdCampoDesglose, iTipoGS, iTipo, oRow, i, vLineasColumnas, contaLinea, oDS, sInstanciaMoneda, sCodCentroCoste)

                            otblCellData = New System.Web.UI.HtmlControls.HtmlTableCell
                            otblCellData.Controls.Add(oFSDSEntry)
                            otblCellData.Attributes("class") = sClase
                            otblRowData.Cells.Add(otblCellData)
                        End If
                    End If
                Next
                'fin desglose
                '####################################################################################################################################
                '####################################################################################################################################
            End If

            Dim bDato As Boolean
            Dim iIndex As Integer

            If oCampo.RecienPublicado Then
                sIdCampoDesglose = oDS.Tables(0).Select("ID=" + sIdCampoDesglose)(0).Item("CAMPOBD_PANT")
            End If

            For i = 1 To iMaxIndex
                If lineasReal.Contains(i) Then
                    If bPopUp And (Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose) <> Nothing Or Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_hnew") <> Nothing) Then
                        bDato = True
                        iIndex = i
                    Else
                        If bPopUp Then
                            bDato = False
                        Else
                            bDato = True
                            iIndex = i
                        End If
                    End If

                    If bDato Then
                        findTheseVals(0) = i
                        findTheseVals(1) = oCampo.IdCopiaCampo
                        If oCampo.IdCopiaCampo = Nothing Then
                            findTheseVals(1) = oCampo.Id
                        Else
                            findTheseVals(1) = oCampo.IdCopiaCampo
                        End If
                        findTheseVals(2) = oRow.Item("ID")
                        findTheseVals(3) = oRow.Item("PRE_ID_CAMPO_NC")
                        oRowDesglose = oDS.Tables(2).Rows.Find(findTheseVals)

                        If oRowDesglose Is Nothing Then
                            oRowDesglose = oDS.Tables(2).NewRow
                            oRowDesglose.Item("LINEA") = i
                            If oCampo.IdCopiaCampo = Nothing Then
                                oRowDesglose.Item("CAMPO_PADRE") = oCampo.Id
                            Else
                                oRowDesglose.Item("CAMPO_PADRE") = oCampo.IdCopiaCampo
                            End If
                            oRowDesglose.Item("TIPO_CAMPO_GS") = oRow.Item("TIPO_CAMPO_GS")
                            oRowDesglose.Item("CAMPO_HIJO") = oRow.Item("ID")
                            oRowDesglose.Item("PRE_ID_CAMPO_HIJO_NC") = oRow.Item("PRE_ID_CAMPO_NC")
                            oDS.Tables(2).Rows.Add(oRowDesglose)
                        End If
                        'uwtGrupos__ctl0_147_fsentry2293__1__2309
                        oRowDesglose.Item("INDEX") = iIndex
                        oRowDesglose.Item("DELETED") = False
                        If bPopUp Then
                            Select Case oRow.Item("SUBTIPO")
                                Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoEditor
                                    sValor = Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose)
                                    If sValor <> "" AndAlso sValor <> "null" Then
                                        oRowDesglose.Item("VALOR_TEXT") = sValor
                                    End If
                                    If Not IsDBNull(oRow.Item("TIPO_CAMPO_GS")) Then
                                        If oRow.Item("INTRO") = 1 And oRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.SinTipo Then
                                            oRowDesglose.Item("VALOR_NUM") = Numero(sValor, oUser.DecimalFmt, oUser.ThousanFmt)

                                            sValor = Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_DescrComboPopUp")
                                            oRowDesglose.Item("VALOR_TEXT") = sValor
                                        ElseIf oRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.EstadoInternoNoConf Then
                                            oRowDesglose.Item("VALOR_NUM") = Numero(sValor, oUser.DecimalFmt, oUser.ThousanFmt)
                                        End If
                                    End If
                                    If IsDBNull(oRow.Item("TIPO_CAMPO_GS")) And oRow.Item("INTRO") = 1 Then
                                        oRowDesglose.Item("VALOR_NUM") = Numero(sValor, oUser.DecimalFmt, oUser.ThousanFmt)

                                        sValor = Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_DescrComboPopUp")
                                        oRowDesglose.Item("VALOR_TEXT") = sValor
                                    End If
                                Case TiposDeDatos.TipoGeneral.TipoNumerico
                                    'Preguntar pq me viene como numerico los de los presupuestos
                                    If Not IsDBNull(oRow.Item("TIPO_CAMPO_GS")) Then
                                        If oRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.PRES1 Or oRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres2 Or oRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres3 Or oRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres4 Then
                                            sValor = Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose)
                                            If sValor <> "" Then
                                                oRowDesglose.Item("VALOR_TEXT") = sValor
                                            End If
                                        End If
                                    End If
                                    'Pregunta pq puede resultar q lo q no es nothing en _MonRepercutido
                                    If Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose) <> "null" Then
                                        oRowDesglose.Item("VALOR_NUM") = Numero(Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose), ".", ",")
                                    End If
                                Case TiposDeDatos.TipoGeneral.TipoFecha
                                    If oRow.Item("intro") = 1 Then
                                        Try
                                            oRowDesglose.Item("VALOR_FEC") = Replace(Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose) + "####" + Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_hNew"), "#", "")
                                        Catch ex As Exception
                                            oRowDesglose.Item("VALOR_FEC") = igDateToDate(Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose))
                                        End Try
                                    Else
                                        oRowDesglose.Item("VALOR_FEC") = igDateToDate(Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose))
                                    End If
                                Case TiposDeDatos.TipoGeneral.TipoBoolean
                                    oRowDesglose.Item("VALOR_BOOL") = Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose)
                                Case TiposDeDatos.TipoGeneral.TipoArchivo
                                    oRowDesglose.Item("VALOR_TEXT") = Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose) + "####" + Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_hNew")
                            End Select
                        End If

                        If (bPopUp _
                        AndAlso DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.ImporteRepercutido _
                        AndAlso Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_MonRepercutido") <> Nothing _
                        AndAlso Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_MonRepercutido") <> "") Then
                            oRowDesglose.Item("VALOR_TEXT") = Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_MonRepercutido")
                            oRowDesglose.Item("CAMBIO") = Numero(Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_CambioRepercutido"), ".", ",")
                        End If

                        If (bPopUp _
                        AndAlso DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.IdsFicticios.DesgloseVinculado _
                        AndAlso Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_IdSolicVinculada") <> Nothing _
                        AndAlso Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_IdSolicVinculada") <> "") _
                        AndAlso oRowDesglose.Table.Columns.Contains("ORIGEN") Then
                            oRowDesglose.Item("ORIGEN") = Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_IdSolicVinculada")
                        End If
                    End If
                End If
            Next

            'Tarea 2227
            'Este orden es necesario para construir luego en el orden correcto el desglose cuando se trata de un desglose que se abre en ventana emergente.
            'Si no, en vez de cargarse l�nea a l�nea se cargar�a campo a campo y eso distorsionar�a los valores entre campos dependientes
            oDS.Tables(2).DefaultView.Sort = "LINEA ASC"

            lContador += 1
            If oRow.Item("VISIBLE") = 1 Then
                bAlgunaColumnaVisible = True
            End If
            If oDS.Tables(0).Rows.Count = lContador Then
                If bAlgunaColumnaVisible Then
                    Dim contaLinea As Integer = 0
                    For i = 1 To iMaxIndex
                        If lineasReal.Contains(i) Then
                            contaLinea += 1
                            If contaLinea Mod 2 = 0 Then
                                sClase = "ugfilaalttablaDesglose"
                            Else
                                sClase = "ugfilatablaDesglose"
                            End If
                            otblRowData = Me.tblDesglose.Rows(contaLinea)

                            If Not moAcciones Is Nothing Then
                                For Each oRowAcc In moAcciones.Rows
                                    Dim oControl As Control
                                    If Not IsDBNull(oRowAcc.Item("IMAGEN")) Then
                                        oImg = New System.Web.UI.WebControls.HyperLink
                                        oImg.ImageUrl = oRowAcc.Item("IMAGEN")
                                        oImg.NavigateUrl = "javascript:void(null)"
                                        oImg.Attributes("onclick") = oRowAcc.Item("JAVASCRIPTEVENT") + "(this.parentElement,'" + Me.ClientID + "'," + i.ToString() + ")"
                                        oControl = oImg
                                    Else
                                        Dim oBoton As HtmlInputButton = New HtmlInputButton
                                        oBoton.Value = DBNullToSomething(oRowAcc.Item("TEXTO"))
                                        oBoton.Attributes("onclick") = oRowAcc.Item("JAVASCRIPTEVENT") + "(this.parentElement,'" + Me.ClientID + "'," + i.ToString() + ")"
                                        oBoton.Attributes.Add("class", "botonPMWEB")
                                        oControl = oBoton
                                    End If
                                    oControl.ID = oRowAcc.Item("ID") + "_" + i.ToString()
                                    otblCellData = New System.Web.UI.HtmlControls.HtmlTableCell
                                    otblCellData.Controls.Add(oControl)
                                    otblCellData.Attributes("class") = sClase
                                    otblCellData.Align = "center"
                                    otblRowData.Cells.Add(otblCellData)
                                Next
                            End If

                            If Not mbSoloLectura Then
                                otblCellData = New System.Web.UI.HtmlControls.HtmlTableCell
                                otblCellData.ID = "celdaCopiar_Eliminar" + i.ToString

                                oImg = New System.Web.UI.WebControls.HyperLink
                                oImg.ImageUrl = "../alta/images/Flecha_Izda.GIF"
                                oImg.NavigateUrl = "javascript:void(null)"
                                oImg.Attributes("onclick") = "popupDesgloseClickEvent(this, this.parentNode,'" + Me.ClientID + "'," + i.ToString() + "," + idCampo.ToString() + ",'dcha'," + oCampo.LineasPreconf.ToString() + ",'" + IIf(oCampo.MoverLinea, "##Si##", "##No##") + "'," + oCampo.IdSolicitud.ToString() + "," + mlInstancia.ToString() + "," + CStr(IIf(bPopUp, 1, 0)) + ",event)"
                                oImg.ID = "cmdCopiar_Eliminar" + i.ToString

                                otblCellData.Controls.Add(oImg)
                                otblCellData.Attributes("class") = sClase
                                otblCellData.Align = "center"
                                otblRowData.Cells.Add(otblCellData)
                                vLineasColumnas(contaLinea - 1) += 1
                            End If

                            oHidden = New System.Web.UI.HtmlControls.HtmlInputHidden
                            oHidden.ID = "_" + i.ToString + "_Deleted"
                            oHidden.Name = "_" + i.ToString + "_Deleted"
                            oHidden.Value = "no"
                            otblCellData.Controls.Add(oHidden)

                            oHidden = New System.Web.UI.HtmlControls.HtmlInputHidden
                            oHidden.ID = "_" + i.ToString + "_Linea"
                            oHidden.Name = "_" + i.ToString + "_Linea"
                            oHidden.Value = i
                            otblCellData.Controls.Add(oHidden)
                        End If
                    Next
                End If
            End If
        Next
        '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        If Not moAcciones Is Nothing Then
            For Each oRowAcc In moAcciones.Rows
                'CABECERA PARA LAS ACCIONEs
                oDivHeader = New System.Web.UI.HtmlControls.HtmlGenericControl("DIV")
                oCellHeader = New System.Web.UI.HtmlControls.HtmlTableCell
                oCellHeader.ID = "COL_" + oRowAcc.Item("ID")
                oSpanHeader = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
                oSpanHeader.InnerHtml = DBNullToSomething(oRowAcc.Item("TITULO"))

                If DBNullToSomething(oRowAcc.Item("WIDTH")) > 0 Then
                    oDivHeader.Style.Item("width") = oRowAcc.Item("WIDTH").ToString + "px"
                    'oCellHeader.Width = oRowAcc.Item("WIDTH")
                End If
                oCellHeader.Attributes("class") = "cabeceraDesglose"
                oDivHeader.Controls.Add(oSpanHeader)
                oCellHeader.Controls.Add(oDivHeader)
                oRowHeader.Cells.Add(oCellHeader)

                'ACCIONES EN LA FILA OCULTA
                Dim oControl As Control
                If Not IsDBNull(oRowAcc.Item("IMAGEN")) Then
                    oImg = New System.Web.UI.WebControls.HyperLink
                    oImg.ImageUrl = oRowAcc.Item("IMAGEN")
                    oImg.NavigateUrl = "javascript:void(null)"
                    oImg.Attributes("onclick") = oRowAcc.Item("JAVASCRIPTEVENT") + "(this.parentElement,'" + Me.ClientID + "','##newIndex##')"
                    oControl = oImg
                Else
                    Dim oBoton As HtmlInputButton = New HtmlInputButton
                    oBoton.Value = DBNullToSomething(oRowAcc.Item("TEXTO"))
                    oBoton.Attributes("onclick") = oRowAcc.Item("JAVASCRIPTEVENT") + "(this.parentElement,'" + Me.ClientID + "','##newIndex##')"
                    oBoton.Attributes.Add("class", "botonPMWEB")
                    oControl = oBoton
                End If
                oControl.ID = oRowAcc.Item("ID")
                otblCell = New System.Web.UI.HtmlControls.HtmlTableCell
                otblCell.Controls.Add(oControl)
                otblCell.Attributes("class") = sClase
                otblCell.Align = "center"
                otblRow.Cells.Add(otblCell)
            Next
        End If

        If msTitulo <> Nothing Then
            Dim oRowTitle As New System.Web.UI.HtmlControls.HtmlTableRow
            Dim oCellTitle As New System.Web.UI.HtmlControls.HtmlTableCell
            Dim oLblTitulo As New Label
            oCellTitle = New System.Web.UI.HtmlControls.HtmlTableCell

            oLblTitulo.Text = msTitulo
            oLblTitulo.Style.Add("margin-right", "15px")
            oLblTitulo.Style.Add("vertical-align", "top")
            oCellTitle.Controls.Add(oLblTitulo)
            oCellTitle.Attributes("class") = "cabeceraTituloDesglose"
            If msAyuda <> "" Then 'Si el campo padre del desglose tiene ayuda se mete el icono
                Dim ImgAyuda As System.Web.UI.WebControls.HyperLink

                ImgAyuda = New System.Web.UI.WebControls.HyperLink
                ImgAyuda.ImageUrl = "images\help.gif"
                ImgAyuda.NavigateUrl = "javascript:void(null)"
                ImgAyuda.Attributes.Add("margin-left", "15px")
                If TieneIdCampo Then
                    ImgAyuda.Attributes("onclick") = "show_help(" + Campo.ToString + "," + mlInstancia.ToString + ")"
                Else
                    ImgAyuda.Attributes("onclick") = "show_help(" + Campo.ToString + "," & IIf(mlInstancia > 0, mlInstancia.ToString & ",", " null,") + mlSolicitud.ToString + ")"
                End If
                oCellTitle.Controls.Add(ImgAyuda)
            End If
            oRowTitle.Cells.Add(oCellTitle)
            Me.tblTituloDesglose.Rows.Insert(0, oRowTitle)
        End If

        'jpa Tarea 187 --------------------------------------------------------------------------------
        'la primera cabecera de tblDesgloseHidden
        If Not mbSoloLectura Then
            oCellHeader = New System.Web.UI.HtmlControls.HtmlTableCell
            oCellHeader.ID = "COL_Copiar_Eliminar_Izda"
            oCellHeader.InnerHtml = sCopiar_Eliminar
            oCellHeader.Attributes("class") = "cabeceraDesglose"
            oRowHeader.Cells.Add(oCellHeader)

            otblCell = New System.Web.UI.HtmlControls.HtmlTableCell
            otblCell.ID = "celdaCopiar_Eliminar_Izda_hdd"

            oImg = New System.Web.UI.WebControls.HyperLink
            oImg.ImageUrl = "../alta/images/Flecha_Izda.GIF"
            oImg.NavigateUrl = "javascript:void(null)"
            oImg.Attributes("onclick") = "popupDesgloseClickEvent(this, this.parentNode,'" + Me.ClientID + "','##newIndex##'," + idCampo.ToString() + ",'dcha'," + oCampo.LineasPreconf.ToString() + ",'" + IIf(oCampo.MoverLinea, "##Si##", "##No##") + "'," + oCampo.IdSolicitud.ToString() + "," + mlInstancia.ToString() + "," + CStr(IIf(bPopUp, 1, 0)) + ",event)"
            oImg.ID = "cmdCopiar_Eliminar_Izda"

            otblCell.Controls.Add(oImg)
            otblCell.Align = "center"

            otblRow.Cells.Add(otblCell)
        End If
        'fin jpa Tarea 187 --------------------------------------------------------------------------------

        Me.tblDesgloseHidden.Rows.Add(otblRow)

        oHid.Value = iMaxIndex
        oHidNumRowsGrid.Value = iLineas
        Me.Controls.Add(oHid)
        Me.Controls.Add(oHidNumRowsGrid)

        'Si hay acciones, debemos ordenarlas por fecha de inicio:
        If Not moAcciones Is Nothing Then
            'Obtengo el ID del Campo Fecha de Inicio
            Dim lIDFechaInicio As Long
            Dim aAUX As DataRow()
            aAUX = oDS.Tables(0).Select("TIPO_CAMPO_GS=" & TiposDeDatos.TipoCampoGS.Fec_inicio)
            If (aAUX.Length > 0) Then
                lIDFechaInicio = CLng(aAUX(0).Item("ID"))
                'Obtengo un array de DataRows ordenadas por la Fecha de Inicio
                Dim aOrdenadosPorFecha As DataRow()
                aOrdenadosPorFecha = oDS.Tables(2).Select("CAMPO_HIJO=" & lIDFechaInicio & " AND DELETED=false AND INDEX IS NOT NULL", "VALOR_FEC ASC")
                'Creo un Array de Arrays de DataRow para tener todas las l�neas de Accion enteras ordenadas por Fecha:
                Dim aLineas As ArrayList = New ArrayList(aOrdenadosPorFecha.Length)
                For Each dr As DataRow In aOrdenadosPorFecha
                    aLineas.Add(oDS.Tables(2).Select("LINEA=" & dr.Item("LINEA").ToString))
                Next
                'Modifico el INDEX de cada linea:
                For j As Integer = 0 To aLineas.Count - 1
                    For Each dr As DataRow In aLineas(j)
                        dr.Item("INDEX") = lineasReal(j)
                    Next
                Next
            End If
        End If

        Dim bArticuloGenerico As Boolean = True
        Dim bYaControlado As Boolean = False

        'Tarea 2227
        'Pasamos el defaultview (que hemos ordenado previamente por l�nea) a table
        'De este modo sabemos que el desglose se cargar� l�nea a l�nea, no por columnas
        Dim oDSTableLINEAS As DataTable = oDS.Tables(2).DefaultView.ToTable()

        For Each oRow In oDSTableLINEAS.Rows
            If oRow.Item("PRE_ID_CAMPO_HIJO_NC") = 0 Then
                sIdCampoHijoDesglose = oRow.Item("CAMPO_HIJO").ToString
            Else
                sIdCampoHijoDesglose = oRow.Item("PRE_ID_CAMPO_HIJO_NC").ToString + oRow.Item("CAMPO_HIJO").ToString
            End If
            sValor = ""
            sValorMat = ""

            bYaControlado = False

            Try
                oFSDSEntry = Me.tblDesglose.FindControl("fsdsentry_" + oRow.Item("INDEX").ToString + "_" + sIdCampoHijoDesglose)
            Catch
                oFSDSEntry = Nothing
            End Try

            If Not oFSDSEntry Is Nothing Then
                iTipo = oFSDSEntry.Tipo
                iTipoGS = oFSDSEntry.TipoGS

                If (Not IsDBNull(oRow.Item("VALOR_TEXT")) Or Not IsDBNull(oRow.Item("VALOR_NUM")) Or Not IsDBNull(oRow.Item("VALOR_FEC")) Or Not IsDBNull(oRow.Item("VALOR_BOOL")) _
                    Or iTipo = TiposDeDatos.TipoGeneral.TipoArchivo Or iTipo = TiposDeDatos.TipoGeneral.TipoEditor Or iTipoGS = TiposDeDatos.TipoCampoGS.NuevoCodArticulo _
                    Or iTipoGS = TiposDeDatos.TipoCampoGS.DenArticulo Or iTipoGS = TiposDeDatos.TipoCampoGS.CodArticulo Or iTipoGS = TiposDeDatos.TipoCampoGS.CentroCoste _
                    Or iTipoGS = TiposDeDatos.TipoCampoGS.Partida) And oRow.Item("DELETED") = False Then

                    If bDepartamentoRelacionado AndAlso iTipoGS = TiposDeDatos.TipoCampoGS.Departamento Then
                        bDepartamentoRelacionado = False
                    End If

                    Select Case iTipo
                        Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio
                            oFSDSEntry.Valor = DBNullToSomething(oRow.Item("VALOR_TEXT"))
                            oFSDSEntry.Text = oFSDSEntry.Valor
                            If Not oFSDSEntry.Valor Is Nothing Then
                                If iTipoGS = TiposDeDatos.TipoCampoGS.SinTipo And oFSDSEntry.Intro = 1 Then
                                    If oFSDSEntry.Lista.Tables.Count > 0 OrElse oFSDSEntry.ReadOnly Then
                                        If DBNullToSomething(oRow.Item("VALOR_NUM").ToString) <> Nothing Then
                                            oRowLista = oFSDSEntry.Lista.Tables(0).Select("ID = " + oRow.Item("CAMPO_HIJO").ToString + " and ORDEN=" + oRow.Item("VALOR_NUM").ToString)
                                        Else
                                            oRowLista = oFSDSEntry.Lista.Tables(0).Select("ID = " + oRow.Item("CAMPO_HIJO").ToString + " and VALOR_TEXT_" & sIdi & " =" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))
                                        End If
                                        If oRowLista.Length > 0 Then
                                            oFSDSEntry.Text = oRowLista(0).Item("VALOR_TEXT_" & sIdi)
                                            oFSDSEntry.Valor = oRow.Item("VALOR_NUM")
                                        ElseIf DBNullToSomething(oRow.Item("VALOR_NUM")) <> Nothing Then
                                            oFSDSEntry.Valor = oRow.Item("VALOR_NUM")
                                        Else
                                            oFSDSEntry.Valor = Nothing
                                        End If
                                    Else
                                        oFSDSEntry.Valor = Nothing
                                    End If
                                ElseIf iTipoGS = TiposDeDatos.IdsFicticios.DesgloseVinculado Then
                                    If oRow.Table.Columns.Contains("ORIGEN") Then
                                        oFSDSEntry.Text = oFSDSEntry.Valor
                                        oFSDSEntry.Valor = CStr(oRow.Item("ORIGEN"))
                                    Else
                                        oFSDSEntry.Text = ""
                                        oFSDSEntry.Valor = Nothing
                                    End If
                                End If
                            End If
                        Case TiposDeDatos.TipoGeneral.TipoNumerico
                            oFSDSEntry.Valor = DBNullToSomething(oRow.Item("VALOR_NUM"))
                        Case TiposDeDatos.TipoGeneral.TipoFecha
                            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechasDefecto") Then
                                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechasDefecto", "<script>var sMensajeFechaDefecto = '" & JSText(mipage.Textos(17)) & "'</script>")
                            End If
                            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "FechaNoAnteriorAlSistema") Then
                                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "FechaNoAnteriorAlSistema", "<script>var sFechaNoAnteriorAlSistema = '" & JSText(mipage.Textos(48)) & "'</script>")
                            End If
                            If iTipoGS = TiposDeDatos.TipoCampoGS.SinTipo And oFSDSEntry.Intro = 1 Then
                                oFSDSEntry.Valor = FormatDate(DBNullToSomething(oRow.Item("VALOR_FEC")), oUser.DateFormat)
                            Else
                                Dim iTipoFecha As TiposDeDatos.TipoFecha
                                iTipoFecha = DBNullToSomething(DBNullToSomething(oRow.Item("VALOR_NUM")))
                                oFSDSEntry.Valor = DevolverFechaRelativaA(iTipoFecha, DBNullToSomething(oRow.Item("VALOR_FEC")), bFavorito)
                            End If
                        Case TiposDeDatos.TipoGeneral.TipoArchivo
                            If bPopUp Then
                                Dim sRequestAdjun As String
                                Dim idAdjuntos As String
                                Dim idAdjuntosNew As String
                                sAdjun = ""
                                idAdjun = ""
                                If IsDBNull(oRow.Item("VALOR_TEXT")) OrElse (oRow.Item("VALOR_TEXT") = "undefined") _
                                    OrElse (Split(oRow.Item("VALOR_TEXT"), "####")(0) = String.Empty AndAlso Split(oRow.Item("VALOR_TEXT"), "####")(1) = String.Empty) Then
                                    sRequestAdjun = ""
                                Else
                                    sRequestAdjun = oRow.Item("VALOR_TEXT")

                                    idAdjuntos = Split(sRequestAdjun, "####")(0)
                                    idAdjuntosNew = Split(sRequestAdjun, "####")(1)

                                    idAdjuntos = Replace(idAdjuntos, "xx", ",")
                                    idAdjuntosNew = Replace(idAdjuntosNew, "xx", ",")

                                    Dim oAdjuntos As FSNServer.Adjuntos
                                    oAdjuntos = FSNServer.Get_Object(GetType(FSNServer.Adjuntos))
                                    Dim oAdjuntosFormCampo As FSNServer.Adjuntos
                                    oAdjuntosFormCampo = FSNServer.Get_Object(GetType(FSNServer.Adjuntos))

                                    If mlInstancia = Nothing Then
                                        oAdjuntos.Load(3, idAdjuntos, idAdjuntosNew, bFavorito Or InstanciaImportar)
                                    Else
                                        oAdjuntos.LoadInst(3, idAdjuntos, idAdjuntosNew)

                                        ' Uno popup donde has a�adido una linea con adjunto por defecto. La 2da vez q lo 
                                        ' abres (sin salirte de la pantalla por eso es 2da vez) en page_load tienes 
                                        ' en idAdjuntos el id de form_campo no de copia_linea_desglose_adjun y no sale el texto.
                                        If idAdjuntos <> "" Then
                                            NAdjuntos = DameNumAdjuntos(idAdjuntos)
                                            NAdjuntos = NAdjuntos + IIf(idAdjuntosNew <> "", DameNumAdjuntos(idAdjuntosNew), 0)
                                        End If

                                        If NAdjuntos > oAdjuntos.Data.Tables(0).Rows.Count Then
                                            AuxNAdjuntos = idAdjuntos & ","

                                            While AuxNAdjuntos <> ""
                                                bEstaAdjun = False

                                                AuxNAdjunto = Left(AuxNAdjuntos, InStr(AuxNAdjuntos, ",") - 1)

                                                For Each oDSRowAdjun In oAdjuntos.Data.Tables(0).Rows
                                                    If oDSRowAdjun.Item("ID").ToString() = AuxNAdjunto Then
                                                        bEstaAdjun = True
                                                        Exit For
                                                    End If
                                                Next

                                                If bEstaAdjun = False Then
                                                    oAdjuntosFormCampo.Load(3, AuxNAdjunto, "", False)
                                                    For Each oDSRowAdjun In oAdjuntosFormCampo.Data.Tables(0).Rows
                                                        idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                                                        sAdjun += oDSRowAdjun.Item("NOM") + " (" + FSNLibrary.FormatNumber(DBNullToSomething(oDSRowAdjun.Item("DATASIZE")), oUser.NumberFormat) + " Kb.), "
                                                    Next
                                                End If

                                                AuxNAdjuntos = Mid(AuxNAdjuntos, InStr(AuxNAdjuntos, ",") + 1)
                                            End While
                                            'Los q estan en idAdjuntosNew no son un problema pq nunca son de form_campo
                                        End If
                                    End If

                                    For Each oDSRowAdjun In oAdjuntos.Data.Tables(0).Rows
                                        idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                                        'el tama�o del fichero ya viene en Kbs.
                                        sAdjun += oDSRowAdjun.Item("NOM") + " (" + FSNLibrary.FormatNumber(DBNullToSomething(oDSRowAdjun.Item("DATASIZE")), oUser.NumberFormat) + " Kb.), "
                                    Next

                                    If sAdjun <> "" Then
                                        sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
                                        idAdjun = idAdjun.Substring(0, idAdjun.Length - 2)
                                    End If
                                End If

                                oFSDSEntry.Valor = sRequestAdjun
                                oFSDSEntry.Text = AjustarAnchoTextoPixels(sAdjun, 150, "Verdana", 12, False)
                            Else
                                Dim idAdjunForm As String
                                sAdjun = ""
                                idAdjun = ""
                                idAdjunForm = ""

                                'oDSTableLINEAS (oDs.Tables(2) ordenado por l�nea) ha perdido las relaciones con los otros datatables
                                Dim sfindTheseVals(3) As Object
                                sfindTheseVals(0) = oRow("LINEA")
                                sfindTheseVals(1) = oRow("CAMPO_PADRE")
                                sfindTheseVals(2) = oRow("CAMPO_HIJO")
                                sfindTheseVals(3) = 0
                                Dim oRowNoDefaultView As DataRow = oDS.Tables(2).Rows.Find(sfindTheseVals)

                                If bFavorito Or InstanciaImportar > 0 Then
                                    For Each oDSRowAdjun In oRowNoDefaultView.GetChildRows("REL_LINEA_ADJUNTO")
                                        If oDSRowAdjun.Item("TIPO").ToString() = "1" Then
                                            'Los que vienen desde el dise�o
                                            idAdjunForm += oDSRowAdjun.Item("ID").ToString() + "xx"
                                        Else
                                            'Los que hemos a�adido anteriormente
                                            idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                                        End If
                                        sAdjun += oDSRowAdjun.Item("NOM") + " (" + FSNLibrary.FormatNumber((oDSRowAdjun.Item("DATASIZE") / 1024), oUser.NumberFormat) + " Kb.), "
                                    Next
                                    If sAdjun <> "" Then
                                        sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
                                        If idAdjun <> "" Then
                                            idAdjun = idAdjun.Substring(0, idAdjun.Length - 2)
                                        End If
                                        If idAdjunForm <> "" Then
                                            idAdjunForm = idAdjunForm.Substring(0, idAdjunForm.Length - 2)
                                        End If
                                    End If

                                    oFSDSEntry.Valor = idAdjunForm & "####" & idAdjun
                                Else
                                    For Each oDSRowAdjun In oRowNoDefaultView.GetChildRows("REL_LINEA_ADJUNTO")
                                        idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                                        sAdjun += oDSRowAdjun.Item("NOM") + " (" + FSNLibrary.FormatNumber((DBNullToSomething(oDSRowAdjun.Item("DATASIZE")) / 1024), oUser.NumberFormat) + " Kb.), "
                                    Next
                                    If sAdjun <> "" Then
                                        sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
                                        idAdjun = idAdjun.Substring(0, idAdjun.Length - 2)
                                    End If
                                    oFSDSEntry.Valor = idAdjun
                                End If
                                oFSDSEntry.Text = AjustarAnchoTextoPixels(sAdjun, 150, "Verdana", 12, False)
                            End If

                        Case TiposDeDatos.TipoGeneral.TipoBoolean
                            oFSDSEntry.Valor = DBNullToSomething(oRow.Item("VALOR_BOOL"))
                            If Not IsDBNull(oRow.Item("VALOR_BOOL")) Then
                                oFSDSEntry.Text = If(oRow.Item("VALOR_BOOL") = 1, mipage.Textos(15), mipage.Textos(16))
                            End If
                        Case TiposDeDatos.TipoGeneral.TipoEditor
                            oFSDSEntry.Valor = DBNullToSomething(oRow.Item("VALOR_TEXT"))
                            oFSDSEntry.Text = mipage.Textos(21)
                    End Select

                    Select Case iTipoGS
                        Case TiposDeDatos.TipoCampoGS.CodArticulo
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oArts = FSNServer.Get_Object(GetType(FSNServer.Articulos))

                                Dim oArticulo As FSNServer.PropiedadesArticulo = oArts.Articulo_Get_Properties(oRow.Item("VALOR_TEXT"))
                                If oArticulo.ExisteArticulo Then
                                    oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                                    oFSDSEntry.Text = oArticulo.Denominacion
                                    oFSDSEntry.Concepto = oArticulo.Concepto
                                    oFSDSEntry.Almacenamiento = oArticulo.Almacenamiento
                                    oFSDSEntry.Recepcion = oArticulo.Recepcion
                                    oFSDSEntry.TipoRecepcion = oArticulo.TipoRecepcion
                                    sKeyTipoRecepcion = oFSDSEntry.TipoRecepcion
                                Else
                                    oFSDSEntry.Text = oRow.Item("VALOR_TEXT")
                                    oFSDSEntry.Valor = Nothing
                                End If

                                oFSDSEntry.Denominacion = oFSDSEntry.Text
                                oFSDSEntry.Width = Unit.Pixel(400)

                                Dim idCampoMat As Long

                                idCampoMat = 0
                                For Each i In arrCamposMaterial
                                    If i < oRow.Item("CAMPO_HIJO") Then
                                        If idCampoMat < i Then
                                            idCampoMat = i
                                        End If
                                    End If
                                Next
                                If idCampoMat > 0 Then
                                    Try
                                        sValorMat = oDS.Tables(2).Select("CAMPO_HIJO = " & idCampoMat & " AND LINEA = " & oRow.Item("LINEA"))(0).Item("VALOR_TEXT")
                                        oFSDSEntry.DependentValue = sValorMat
                                    Catch ex As Exception

                                    End Try
                                End If
                            End If
                            If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND CAMPO_PADRE = " & oRow.Item("CAMPO_PADRE") & " AND LINEA = " & oRow.Item("LINEA")).Length > 0 Then
                                oFSDSEntry.UnidadOculta = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND CAMPO_PADRE = " & oRow.Item("CAMPO_PADRE") & " AND LINEA = " & oRow.Item("LINEA"))(0).Item("VALOR_TEXT"))
                            End If
                        Case TiposDeDatos.TipoCampoGS.NuevoCodArticulo
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Text = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.codigoArticulo = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.ToolTip = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.Denominacion = oFSDSEntry.Text
                                oFSDSEntry.Width = Unit.Pixel(200)
                                oArts = FSNServer.Get_Object(GetType(FSNServer.Articulos))

                                Dim oArticulo As FSNServer.PropiedadesArticulo = oArts.Articulo_Get_Properties(oRow.Item("VALOR_TEXT"))
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                    If oArticulo.ExisteArticulo Then
                                        oFSDSEntry.Concepto = oArticulo.Concepto
                                        oFSDSEntry.Almacenamiento = oArticulo.Almacenamiento
                                        oFSDSEntry.Recepcion = oArticulo.Recepcion
                                        oFSDSEntry.TipoRecepcion = oArticulo.TipoRecepcion
                                        oFSDSEntry.articuloGenerico = oArticulo.Generico
                                        oFSDSEntry.articuloCodificado = True
                                    End If
                                Else
                                    If oArticulo.ExisteArticulo Then
                                        oFSDSEntry.Concepto = oArticulo.Concepto
                                        oFSDSEntry.Almacenamiento = oArticulo.Almacenamiento
                                        oFSDSEntry.Recepcion = oArticulo.Recepcion
                                        oFSDSEntry.TipoRecepcion = oArticulo.TipoRecepcion
                                        oFSDSEntry.articuloGenerico = oArticulo.Generico
                                    Else
                                        oFSDSEntry.articuloGenerico = True
                                    End If
                                End If
                                bArticuloGenerico = oFSDSEntry.articuloGenerico
                                sKeyTipoRecepcion = oFSDSEntry.TipoRecepcion
                                sCodArticulo = oFSDSEntry.codigoArticulo

                                Dim idCampoMat As Long
                                idCampoMat = 0
                                For Each i In arrCamposMaterial
                                    If i < oRow.Item("CAMPO_HIJO") Then
                                        If idCampoMat < i Then
                                            idCampoMat = i
                                        End If
                                    End If
                                Next
                                If idCampoMat > 0 Then
                                    Try
                                        sValorMat = oDS.Tables(2).Select("CAMPO_HIJO = " & idCampoMat & " AND LINEA = " & oRow.Item("LINEA"))(0).Item("VALOR_TEXT")
                                        oFSDSEntry.DependentValue = sValorMat
                                    Catch ex As Exception

                                    End Try
                                End If

                                If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND CAMPO_PADRE = " & oRow.Item("CAMPO_PADRE") & " AND LINEA = " & oRow.Item("LINEA")).Length > 0 Then
                                    oFSDSEntry.UnidadOculta = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND CAMPO_PADRE = " & oRow.Item("CAMPO_PADRE") & " AND LINEA = " & oRow.Item("LINEA"))(0).Item("VALOR_TEXT"))
                                End If
                                If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND CAMPO_PADRE = " & oRow.Item("CAMPO_PADRE") & " AND LINEA = " & oRow.Item("LINEA")).Length > 0 Then
                                    oFSDSEntry.UnidadPedidoOculta = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND CAMPO_PADRE = " & oRow.Item("CAMPO_PADRE") & " AND LINEA = " & oRow.Item("LINEA"))(0).Item("VALOR_TEXT"))
                                End If
                            Else
                                bArticuloGenerico = True
                                sKeyTipoRecepcion = "0"
                            End If
                            If sIdCampoProveedor <> String.Empty Then
                                oFSEntry.idEntryProveSumiArt = oFSEntry.IdContenedor & "_fsentry" & sIdCampoProveedor
                            End If
                        Case TiposDeDatos.TipoCampoGS.DenArticulo
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Text = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.ToolTip = oRow.Item("VALOR_TEXT")

                                oFSDSEntry.articuloGenerico = bArticuloGenerico
                                oFSDSEntry.TipoRecepcion = sKeyTipoRecepcion
                                If sCodArticulo <> "" Then
                                    oFSDSEntry.codigoArticulo = sCodArticulo
                                    sCodArticulo = ""
                                End If

                                Dim idCampoMat As Long = 0
                                For Each i In arrCamposMaterial
                                    If i < oRow.Item("CAMPO_HIJO") Then
                                        If idCampoMat < i Then
                                            idCampoMat = i
                                        End If
                                    End If
                                Next
                                If idCampoMat > 0 Then
                                    Try
                                        sValorMat = oDS.Tables(2).Select("CAMPO_HIJO = " & idCampoMat & " AND LINEA = " & oRow.Item("LINEA"))(0).Item("VALOR_TEXT")
                                        oFSDSEntry.DependentValue = sValorMat
                                    Catch ex As Exception

                                    End Try
                                End If
                            End If
                            If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND CAMPO_PADRE = " & oRow.Item("CAMPO_PADRE") & " AND LINEA = " & oRow.Item("LINEA")).Length > 0 Then
                                oFSDSEntry.UnidadOculta = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND CAMPO_PADRE = " & oRow.Item("CAMPO_PADRE") & " AND LINEA = " & oRow.Item("LINEA"))(0).Item("VALOR_TEXT"))
                            End If
                            If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND CAMPO_PADRE = " & oRow.Item("CAMPO_PADRE") & " AND LINEA = " & oRow.Item("LINEA")).Length > 0 Then
                                oFSDSEntry.UnidadPedidoOculta = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND CAMPO_PADRE = " & oRow.Item("CAMPO_PADRE") & " AND LINEA = " & oRow.Item("LINEA"))(0).Item("VALOR_TEXT"))
                            End If
                            If sIdCampoProveedor <> String.Empty Then
                                oFSEntry.idEntryProveSumiArt = oFSEntry.IdContenedor & "_fsentry" & sIdCampoProveedor
                            End If
                        Case TiposDeDatos.TipoCampoGS.Dest
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Text = TextoDelDropDown(oFSDSEntry.Lista, "COD", oRow.Item("VALOR_TEXT"), Not oFSDSEntry.ReadOnly, , , True)
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                            End If
                            Dim iPix As Integer = 250
                            If oFSDSEntry.Text IsNot Nothing Then
                                iPix = (oFSDSEntry.Text.Length * 7) + 5
                            End If
                            oFSDSEntry.Width = IIf(oFSDSEntry.ReadOnly, Unit.Pixel(iPix), Unit.Pixel(250))
                            If Not IsDBNull(oFSDSEntry.Valor) And Not CType(Me.Page, FSNPage).Acceso.gbUsar_OrgCompras Then
                                sCodDest = oFSDSEntry.Valor
                            End If
                            sKeyDestino = oFSDSEntry.ID
                        Case TiposDeDatos.TipoCampoGS.Cantidad
                            If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND CAMPO_PADRE = " & oRow.Item("CAMPO_PADRE") & " AND LINEA = " & oRow.Item("LINEA")).Length > 0 Then
                                Dim sCodUnidad As String
                                sCodUnidad = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND CAMPO_PADRE = " & oRow.Item("CAMPO_PADRE") & " AND LINEA = " & oRow.Item("LINEA"))(0).Item("VALOR_TEXT"))
                                If sCodUnidad <> Nothing Then
                                    Dim oUnis As FSNServer.Unidades
                                    oUnis = FSNServer.Get_Object(GetType(FSNServer.Unidades))
                                    oUnis.LoadData(sIdi, sCodUnidad, , False)
                                    If IsDBNull(oUnis.Data.Tables(0).Rows(0).Item("NUMDEC")) Then
                                        oFSDSEntry.NumeroDeDecimales = 15
                                    Else
                                        oFSDSEntry.NumeroDeDecimales = CInt(oUnis.Data.Tables(0).Rows(0).Item("NUMDEC"))
                                    End If
                                    oFSDSEntry.UnidadOculta = oUnis.Data.Tables(0).Rows(0).Item("DEN")
                                End If
                            End If
                            If sKeyTipoRecepcion = "1" Then
                                oFSDSEntry.TipoRecepcion = 1
                            End If
                        Case TiposDeDatos.TipoCampoGS.Unidad
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Text = TextoDelDropDown(oFSDSEntry.Lista, "COD", oRow.Item("VALOR_TEXT"), Not oFSDSEntry.ReadOnly)
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                            End If

                        Case TiposDeDatos.TipoCampoGS.UnidadPedido
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Text = TextoDelDropDown(oFSDSEntry.Lista, "COD", oRow.Item("VALOR_TEXT"), Not oFSDSEntry.ReadOnly)
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                            End If

                        Case TiposDeDatos.TipoCampoGS.FormaPago
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Text = TextoDelDropDown(oFSDSEntry.Lista, "COD", oRow.Item("VALOR_TEXT"), Not oFSDSEntry.ReadOnly)
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                            End If
                            oFSDSEntry.Width = Unit.Pixel(200)
                        Case TiposDeDatos.TipoCampoGS.Moneda
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Text = TextoDelDropDown(oFSDSEntry.Lista, "COD", oRow.Item("VALOR_TEXT"), Not oFSDSEntry.ReadOnly)
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                            End If

                        Case TiposDeDatos.TipoCampoGS.Pais
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Text = TextoDelDropDown(oFSDSEntry.Lista, "COD", oRow.Item("VALOR_TEXT"), Not oFSDSEntry.ReadOnly)
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                                sCodPais = oRow.Item("VALOR_TEXT")
                            End If

                            sKeyPais = oFSDSEntry.ID
                        Case TiposDeDatos.TipoCampoGS.Provincia
                            'Provincia es un campo relacionado por lo que la carga de datos se hace desde javascript(ajax)
                            'Esta carga se hace �nicamente cuando el campo tiene un valor por defecto y hay que buscar la descripci�n del valor por defecto
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                If Not String.IsNullOrEmpty(sCodPais) Then
                                    Dim oProvis As FSNServer.Provincias
                                    oProvis = FSNServer.Get_Object(GetType(FSNServer.Provincias))
                                    oProvis.Pais = DBNullToStr(sCodPais)
                                    oProvis.LoadData(sIdi)
                                    If sKeyPais <> String.Empty Then _
                                        oFSDSEntry.MasterField = sKeyPais
                                    'Como texto ponemos el c�digo y la descripci�n porque si no puede haber problemas al ser controles que cargan los datos desde javascript y tener Infragistics 11.1 limitaciones en ese sentido
                                    oFSDSEntry.Text = TextoDelDropDown(oProvis.Data, "COD", oRow.Item("VALOR_TEXT"), True, "PAICOD", sCodPais) 'sDenominacion
                                    oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                                End If
                            End If

                        Case TiposDeDatos.TipoCampoGS.PRES1, TiposDeDatos.TipoCampoGS.Pres2, TiposDeDatos.TipoCampoGS.Pres3, TiposDeDatos.TipoCampoGS.Pres4
                            oFSDSEntry.Width = Unit.Pixel(200)
                            oFSDSEntry.Valor = DBNullToSomething(oRow.Item("VALOR_TEXT"))
                            If DBNullToSomething(oRow.Item("VALOR_TEXT")) <> Nothing Then
                                Dim arrDenominaciones() As String = Nothing
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) And Not DBNullToSomething(oRow.Item("VALOR_TEXT")) = Nothing Then
                                    arrDenominaciones = oRow.Item("VALOR_TEXT_DEN").ToString().Split("#")
                                    If oRow.Item("VALOR_TEXT_COD") Then
                                        oFSDSEntry.InputStyle.CssClass = IIf(oFSDSEntry.ReadOnly, "StringTachadotrasparent", "StringTachado")
                                    Else
                                        oFSDSEntry.InputStyle.CssClass = IIf(oFSDSEntry.ReadOnly, "trasparent", "TipoTextoMedio")
                                    End If
                                Else
                                    oFSDSEntry.InputStyle.CssClass = IIf(oFSDSEntry.ReadOnly, "trasparent", "TipoTextoMedio")
                                End If
                                sValor = ""
                                sTexto = ""
                                sDenominacion = ""
                                iContadorPres = 0
                                arrPresupuestos = oRow.Item("VALOR_TEXT").split("#")
                                For Each oPresup In arrPresupuestos
                                    iContadorPres = iContadorPres + 1
                                    arrPresup = oPresup.Split("_")
                                    iNivel = arrPresup(0)
                                    lIdPresup = arrPresup(1)
                                    'Los presupuestos siempre llevan el porcentaje como 0.xx salvo cuando es 100% que llevan 1
                                    dPorcent = Numero(arrPresup(2), ".", "")
                                    If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) And Not DBNullToSomething(oRow.Item("VALOR_TEXT")) = Nothing Then
                                        sTexto = arrDenominaciones(iContadorPres - 1) & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); " & sTexto
                                        sValor += oPresup + "#"
                                    Else
                                        Select Case iTipoGS
                                            Case TiposDeDatos.TipoCampoGS.PRES1
                                                oPres1 = FSNServer.Get_Object(GetType(FSNServer.PresProyectosNivel1))
                                                Select Case iNivel
                                                    Case 1
                                                        oPres1.LoadData(lIdPresup)
                                                    Case 2
                                                        oPres1.LoadData(Nothing, lIdPresup)
                                                    Case 3
                                                        oPres1.LoadData(Nothing, Nothing, lIdPresup)
                                                    Case 4
                                                        oPres1.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                                                End Select
                                                oDSPres = oPres1.Data
                                                oPres1 = Nothing
                                            Case TiposDeDatos.TipoCampoGS.Pres2
                                                oPres2 = FSNServer.Get_Object(GetType(FSNServer.PresContablesNivel1))
                                                Select Case iNivel
                                                    Case 1
                                                        oPres2.LoadData(lIdPresup)
                                                    Case 2
                                                        oPres2.LoadData(Nothing, lIdPresup)
                                                    Case 3
                                                        oPres2.LoadData(Nothing, Nothing, lIdPresup)
                                                    Case 4
                                                        oPres2.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                                                End Select
                                                oDSPres = oPres2.Data
                                                oPres2 = Nothing
                                            Case TiposDeDatos.TipoCampoGS.Pres3
                                                oPres3 = FSNServer.Get_Object(GetType(FSNServer.PresConceptos3Nivel1))
                                                Select Case iNivel
                                                    Case 1
                                                        oPres3.LoadData(lIdPresup)
                                                    Case 2
                                                        oPres3.LoadData(Nothing, lIdPresup)
                                                    Case 3
                                                        oPres3.LoadData(Nothing, Nothing, lIdPresup)
                                                    Case 4
                                                        oPres3.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                                                End Select
                                                oDSPres = oPres3.Data
                                                oPres3 = Nothing
                                            Case TiposDeDatos.TipoCampoGS.Pres4
                                                oPres4 = FSNServer.Get_Object(GetType(FSNServer.PresConceptos4Nivel1))
                                                Select Case iNivel
                                                    Case 1
                                                        oPres4.LoadData(lIdPresup)
                                                    Case 2
                                                        oPres4.LoadData(Nothing, lIdPresup)
                                                    Case 3
                                                        oPres4.LoadData(Nothing, Nothing, lIdPresup)
                                                    Case 4
                                                        oPres4.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                                                End Select
                                                oDSPres = oPres4.Data
                                                oPres4 = Nothing
                                        End Select

                                        If Not oDSPres Is Nothing Then
                                            If oDSPres.Tables(0).Rows.Count > 0 Then
                                                With oDSPres.Tables(0).Rows(0)
                                                    'comprobamos que el valor por defecto cumpla la restricci�n del usuario. SI NO CUMPLE, NO APARECER�
                                                    If Not oDSPres.Tables(0).Columns("ANYO") Is Nothing Then
                                                        iAnyo = .Item("ANYO")

                                                    End If
                                                    Dim bSeleccionado As Boolean = True
                                                    Dim sPorcentaje As String
                                                    Dim sDenPresup As String = String.Empty
                                                    For i = 4 To 1 Step -1
                                                        'PRESi:
                                                        If Not IsDBNull(.Item("PRES" & i)) Then
                                                            If bSeleccionado Then
                                                                sPorcentaje = " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
                                                            Else
                                                                sPorcentaje = " - "
                                                            End If
                                                            sTexto = .Item("PRES" & i).ToString & sPorcentaje & sTexto
                                                            sDenPresup = " - " & .Item("PRES" & i).ToString
                                                            bSeleccionado = False
                                                        End If
                                                    Next
                                                    If Not oDSPres.Tables(0).Columns("ANYO") Is Nothing Then
                                                        sTexto = .Item("ANYO").ToString & " - " & sTexto
                                                        sDenPresup = .Item("ANYO").ToString & sDenPresup
                                                    End If
                                                    If sDenominacion <> "" Then sDenominacion = "#" & sDenominacion
                                                    sDenominacion = sDenPresup & sDenominacion

                                                    If (.Item("BAJALOG")) Then _
                                                        oFSDSEntry.InputStyle.CssClass = IIf(oFSDSEntry.ReadOnly, "StringTachadotrasparent", "StringTachado")

                                                    sValor += oPresup + "#"
                                                End With
                                            End If
                                        End If
                                    End If
                                Next
                                If sValor <> Nothing Then
                                    sValor = Left(sValor, sValor.Length - 1)
                                End If
                                oFSDSEntry.Valor = sValor
                                oFSDSEntry.Denominacion = sDenominacion
                                If sTexto = "" Then
                                    If Not oFSDSEntry.ReadOnly Then
                                        oFSDSEntry.Text = mipage.Textos(1)
                                        oFSDSEntry.Texto_Defecto = oFSDSEntry.Text
                                        bYaControlado = True
                                    End If
                                Else
                                    oFSDSEntry.Text = sTexto
                                End If
                            Else
                                If Not oFSDSEntry.ReadOnly Then
                                    oFSDSEntry.Text = mipage.Textos(1)
                                    oFSDSEntry.Texto_Defecto = oFSDSEntry.Text
                                    bYaControlado = True
                                End If
                            End If
                        Case TiposDeDatos.TipoCampoGS.Proveedor
                            If DBNullToSomething(oRow.Item("VALOR_TEXT")) <> Nothing Then
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = oRow.Item("VALOR_TEXT_DEN")
                                Else
                                    oProve = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                                    oProve.Cod = oRow.Item("VALOR_TEXT")
                                    oProve.Load(sIdi, True)
                                    sDenominacion = oProve.Den
                                End If
                                oFSDSEntry.Text = oRow.Item("VALOR_TEXT") + " - " + sDenominacion.Replace("'", "")
                                oFSDSEntry.Denominacion = sDenominacion
                            ElseIf Not oFSDSEntry.ReadOnly Then
                                oFSDSEntry.Text = mipage.Textos(2)
                                oFSDSEntry.Texto_Defecto = oFSDSEntry.Text
                                bYaControlado = True
                            End If
                        Case TiposDeDatos.TipoCampoGS.Material
                            Dim sMat As String = DBNullToSomething(oRow.Item("VALOR_TEXT"))
                            Dim lLongCod As Integer
                            sValorMat = sMat

                            For i = 1 To 4
                                arrMat(i) = ""
                            Next i

                            i = 1
                            While Trim(sMat) <> ""
                                Select Case i
                                    Case 1
                                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                                    Case 2
                                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                                    Case 3
                                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                                    Case 4
                                        lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
                                End Select
                                arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
                                sMat = Mid(sMat, lLongCod + 1)
                                i = i + 1
                            End While

                            If arrMat(4) <> "" Then
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
                                Else
                                    oGMN3 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel3))
                                    oGMN3.GMN1Cod = arrMat(1)
                                    oGMN3.GMN2Cod = arrMat(2)
                                    oGMN3.Cod = arrMat(3)
                                    oGMN3.CargarTodosLosGruposMatDesde(1, sIdi, arrMat(4), , True)
                                    sDenominacion = oGMN3.GruposMatNivel4.Item(arrMat(4)).Den
                                    oGMN3 = Nothing
                                End If
                                sToolTip = sDenominacion & " (" & arrMat(1) & " - " & arrMat(2) & " - " & arrMat(3) & " - " & arrMat(4) & ")"
                                If Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles Then
                                    sValor = arrMat(1) + " - " + arrMat(2) + " - " + arrMat(3) + " - " + arrMat(4) + " - " + sDenominacion
                                Else
                                    sValor = arrMat(4) + " - " + sDenominacion
                                End If

                            ElseIf arrMat(3) <> "" Then
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
                                Else
                                    oGMN2 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel2))
                                    oGMN2.GMN1Cod = arrMat(1)
                                    oGMN2.Cod = arrMat(2)
                                    oGMN2.CargarTodosLosGruposMatDesde(1, sIdi, arrMat(3), , True)
                                    sDenominacion = oGMN2.GruposMatNivel3.Item(arrMat(3)).Den
                                    oGMN2 = Nothing
                                End If
                                sToolTip = sDenominacion & "(" & arrMat(1) & " - " & arrMat(2) & " - " & arrMat(3) & ")"
                                If Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles Then
                                    sValor = arrMat(1) + " - " + arrMat(2) + " - " + arrMat(3) + " - " + sDenominacion
                                Else
                                    sValor = arrMat(3) + " - " + sDenominacion
                                End If
                            ElseIf arrMat(2) <> "" Then
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
                                Else
                                    oGMN1 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel1))
                                    oGMN1.Cod = arrMat(1)
                                    oGMN1.CargarTodosLosGruposMatDesde(1, sIdi, arrMat(2), , True)
                                    sDenominacion = oGMN1.GruposMatNivel2.Item(arrMat(2)).Den
                                    oGMN1 = Nothing
                                End If
                                sToolTip = sDenominacion & "(" & arrMat(1) & " - " & arrMat(2) & ")"
                                If Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles Then
                                    sValor = arrMat(1) + " - " + arrMat(2) + " - " + sDenominacion
                                Else
                                    sValor = arrMat(2) + " - " + sDenominacion
                                End If
                            ElseIf arrMat(1) <> "" Then
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
                                Else
                                    oGMN1s = FSNServer.Get_Object(GetType(FSNServer.GruposMatNivel1))
                                    oGMN1s.CargarTodosLosGruposMatDesde(1, sIdi, arrMat(1), , , True)
                                    sDenominacion = oGMN1s.Item(arrMat(1)).Den
                                    oGMN1s = Nothing
                                End If
                                sToolTip = sDenominacion & "(" & arrMat(1) & ")"
                                sValor = arrMat(1) + " - " + sDenominacion
                            End If
                            oFSDSEntry.Text = sValor
                            If sToolTip Is Nothing Then
                                oFSDSEntry.ToolTip = ""
                            Else
                                oFSDSEntry.ToolTip = sToolTip
                            End If
                            oFSDSEntry.Denominacion = sDenominacion
                            oFSDSEntry.Width = Unit.Pixel(200)

                        Case TiposDeDatos.TipoCampoGS.EstadoNoConf
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                If Not IsDBNull(oRow.Item("INDEX")) Then
                                    oFSDSEntry.Valor = oFSDSEntry.Lista.Tables(0).Rows(oRow.Item("INDEX")).Item("COD")
                                    oFSDSEntry.Text = Mid(oFSDSEntry.Lista.Tables(0).Rows(oRow.Item("INDEX")).Item("DEN"), 6)
                                End If
                            End If

                        Case TiposDeDatos.TipoCampoGS.EstadoInternoNoConf
                            If DBNullToSomething(oRow.Item("VALOR_NUM")) > 0 Then
                                Select Case DBNullToSomething(oRow.Item("VALOR_NUM"))
                                    Case TipoEstadoAcciones.Aprobada, TipoEstadoAcciones.FinalizadaRevisada
                                        Dim oBoton As HtmlInputButton = Me.FindControl("cmdAprobarAccion" + "_" + oRow.Item("INDEX").ToString)
                                        If Not IsNothing(oBoton) Then
                                            oBoton.Style.Add("visibility", "hidden")
                                        End If
                                    Case TipoEstadoAcciones.Rechazada
                                        Dim oBoton As HtmlInputButton = Me.FindControl("cmdRechazarAccion" + "_" + oRow.Item("INDEX").ToString)
                                        If Not IsNothing(oBoton) Then
                                            oBoton.Style.Add("visibility", "hidden")
                                        End If
                                End Select

                                oFSDSEntry.Valor = oRow.Item("VALOR_NUM")
                                oFSDSEntry.Text = mipage.Textos(oRow.Item("VALOR_NUM") + 4)
                            End If
                            oFSDSEntry.Width = Unit.Pixel(200)

                        Case TiposDeDatos.TipoCampoGS.Persona, TiposDeDatos.TipoCampoGS.Comprador
                            If DBNullToSomething(oRow.Item("VALOR_TEXT")) <> Nothing Then
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = DBNullToStr(oRow.Item("VALOR_TEXT_DEN"))
                                Else
                                    Dim oPersona As FSNServer.Persona = FSNServer.Get_Object(GetType(FSNServer.Persona))
                                    oPersona.LoadData(oRow.Item("VALOR_TEXT").ToString)
                                    sDenominacion = oPersona.Denominacion(oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.Persona)
                                End If
                                oFSDSEntry.Text = sDenominacion
                                oFSDSEntry.Denominacion = sDenominacion
                            End If
                            oFSDSEntry.Intro = 0
                            If oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.Comprador Then
                                If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeComprador") Then
                                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeComprador", "<script>var smensajeComprador = '" & JSText(mipage.Textos(23)) & "'</script>")
                                End If
                            End If
                        Case TiposDeDatos.TipoCampoGS.UnidadOrganizativa
                            sUnidadOrganizativa = ""
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                bDepartamentoRelacionado = True
                                sUnidadOrganizativa = oFSDSEntry.Valor
                                Dim arrUON() As String = oFSDSEntry.Valor.split("-")
                                Dim oUONData As FSNServer.UnidadesOrg
                                Dim ilong = arrUON.Length
                                Dim icount = 0
                                Dim sUon0, sUon1, sUon2, sUon3 As String
                                sUon0 = "" : sUon1 = "" : sUon2 = "" : sUon3 = ""

                                While icount < ilong - 1
                                    Select Case icount
                                        Case 0
                                            sUon1 = arrUON(0)
                                        Case 1
                                            sUon2 = arrUON(1)
                                        Case 2
                                            sUon3 = arrUON(2)
                                    End Select
                                    icount = icount + 1
                                End While
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.Text = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.Denominacion = arrUON(ilong - 1)
                                Dim iestado As Integer
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    iestado = oRow.Item("VALOR_TEXT_COD")
                                    oFSDSEntry.InputStyle.CssClass = IIf(iestado = 1, "StringTachado", "TipoTextoMedio")
                                Else
                                    oUONData = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))
                                    Select Case ilong
                                        Case 2
                                            oUONData.CargarEstado(LTrim(sUon1), , , )
                                        Case 3
                                            oUONData.CargarEstado(LTrim(sUon1), LTrim(sUon2), , )
                                        Case 4
                                            oUONData.CargarEstado(LTrim(sUon1), LTrim(sUon2), LTrim(sUon3), )
                                    End Select
                                    If oUONData.Data.Tables(0).Rows.Count > 0 Then
                                        iestado = oUONData.Data.Tables(0).Rows(0).Item("BAJALOG")
                                        oFSDSEntry.InputStyle.CssClass = IIf(iestado = 1, "StringTachado", "TipoTextoMedio")
                                    End If
                                End If
                            End If
                            sKeyUnidadOrganizativa = oFSDSEntry.ID
                        Case TiposDeDatos.TipoCampoGS.Departamento
                            If sKeyUnidadOrganizativa <> String.Empty Then _
                                oFSDSEntry.MasterField = sKeyUnidadOrganizativa

                            'Departamento es un campo relacionado por lo que la carga de datos se hace desde javascript(ajax)
                            'Esta carga se hace �nicamente cuando el campo tiene un valor por defecto y hay que buscar la descripci�n del valor por defecto
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                Dim sUnidadOrganizativa2 As String = ""
                                If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa).Length > 0 Then
                                    sUnidadOrganizativa2 = DBNullToStr(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa)(0).Item("VALOR_TEXT"))
                                End If

                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")

                                Dim oDepartamento As FSNServer.Departamento
                                oDepartamento = FSNServer.Get_Object(GetType(FSNServer.Departamento))
                                oFSDSEntry.InputStyle.CssClass = IIf(oDepartamento.EstaDeBaja(oRow.Item("VALOR_TEXT")) Or bDepartamentoRelacionado, "StringTachado", "TipoTextoMedio")

                                If sUnidadOrganizativa2 <> "" Then
                                    Dim oDepartamentos As FSNServer.Departamentos
                                    oDepartamentos = FSNServer.Get_Object(GetType(FSNServer.Departamentos))

                                    Dim vUnidadesOrganizativas(3) As String
                                    vUnidadesOrganizativas = Split(sUnidadOrganizativa2, " - ")

                                    Select Case UBound(vUnidadesOrganizativas)
                                        Case 0
                                            oDepartamentos.LoadData(0)
                                            oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                                        Case 1
                                            oDepartamentos.LoadData(1, vUnidadesOrganizativas(0))
                                            oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                                        Case 2
                                            oDepartamentos.LoadData(2, vUnidadesOrganizativas(0), vUnidadesOrganizativas(1))
                                            oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                                        Case 3
                                            oDepartamentos.LoadData(3, vUnidadesOrganizativas(0), vUnidadesOrganizativas(1), vUnidadesOrganizativas(2))
                                            oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                                    End Select

                                    'Como texto ponemos el c�digo y la descripci�n porque si no puede haber problemas al ser controles que cargan los datos desde javascript y tener Infragistics 11.1 limitaciones en ese sentido
                                    oFSDSEntry.Text = TextoDelDropDown(oDepartamentos.Data, "COD", oRow.Item("VALOR_TEXT"), True)
                                End If
                            End If
                            oFSDSEntry.Denominacion = sDenominacion
                        Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
                            sCodOrgCompras2 = ""
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                sCodOrgCompras2 = oRow.Item("VALOR_TEXT")

                                oFSDSEntry.Text = TextoDelDropDown(oFSDSEntry.Lista, "COD", oRow.Item("VALOR_TEXT"), Not oFSDSEntry.ReadOnly)
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                            End If
                            sKeyOrgComprasDataCheck = oFSDSEntry.ID
                        Case TiposDeDatos.TipoCampoGS.ProveedorERP
                            If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                'Si viene de una instancia tendra VALOR_TEXT_DEN
                                oFSDSEntry.Text = oRow.Item("VALOR_TEXT") & " - " & oRow.Item("VALOR_TEXT_DEN")
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                            Else
                                'Si viene por defecto el valor del formulario y no tiene VALOR_TEXT_DEN(que vendria solo cuando ya es una instancia)
                                If DBNullToStr(oRow.Item("VALOR_TEXT")) <> String.Empty Then
                                    Dim oProvesERP As FSNServer.CProveERPs
                                    oProvesERP = FSNServer.Get_Object(GetType(FSNServer.CProveERPs))
                                    Dim ds As DataSet = oProvesERP.CargarProveedoresERPtoDS(, , oRow.Item("VALOR_TEXT"))
                                    For Each dr As DataRow In ds.Tables(0).Rows
                                        oFSDSEntry.Text = dr("COD") & " - " & dr("DEN")
                                        oFSDSEntry.Valor = dr("COD")
                                        Exit For
                                    Next
                                    ds = Nothing
                                    oProvesERP = Nothing
                                End If
                            End If
                        Case TiposDeDatos.TipoCampoGS.Centro
                            If sKeyOrgComprasDataCheck <> String.Empty Then _
                                oFSDSEntry.MasterField = sKeyOrgComprasDataCheck

                            sCodCentro2 = ""

                            'Centro es un campo relacionado por lo que la carga de datos se hace desde javascript(ajax)
                            'Esta carga se hace �nicamente cuando el campo tiene un valor por defecto y hay que buscar la descripci�n del valor por defecto
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                sCodCentro2 = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")

                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    'Si viene de una instancia tendra VALOR_TEXT_DEN
                                    oFSDSEntry.Text = oRow.Item("VALOR_TEXT_DEN")
                                Else
                                    If sCodOrgCompras2 <> "" Then
                                        Dim oCentros As FSNServer.Centros
                                        oCentros = FSNServer.Get_Object(GetType(FSNServer.Centros))
                                        oCentros.LoadData(oUser.Cod, oUser.PMRestriccionUONSolicitudAUONUsuario, oUser.PMRestriccionUONSolicitudAUONPerfil, sCodOrgCompras2)
                                        oFSDSEntry.Text = TextoDelDropDown(oCentros.Data, "COD", oRow.Item("VALOR_TEXT"), True)
                                    End If
                                End If
                            End If

                            sKeyCentro = oFSDSEntry.ID

                        Case TiposDeDatos.TipoCampoGS.Almacen
                            If CType(Me.Page, FSNPage).Acceso.gbUsar_OrgCompras AndAlso sKeyCentro <> String.Empty Then
                                oFSDSEntry.MasterField = sKeyCentro
                            ElseIf Not CType(Me.Page, FSNPage).Acceso.gbUsar_OrgCompras AndAlso sKeyDestino <> String.Empty Then
                                oFSDSEntry.MasterField = sKeyDestino
                            End If

                            If Not IsDBNull(oRow.Item("VALOR_NUM")) OrElse sCodDest <> "" Then
                                If Not IsDBNull(oRow.Item("VALOR_NUM")) Then
                                    If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                        'Si viene de una instancia tendra VALOR_TEXT_DEN
                                        oFSDSEntry.Text = oRow.Item("VALOR_TEXT_DEN")
                                    Else
                                        Dim oAlmacenes As FSNServer.Almacenes
                                        oAlmacenes = FSNServer.Get_Object(GetType(FSNServer.Almacenes))
                                        oAlmacenes.LoadData(sCodCentro2)
                                        oFSDSEntry.Valor = oRow.Item("VALOR_NUM")
                                        oFSDSEntry.Text = TextoDelDropDown(oAlmacenes.Data, "ID", oRow.Item("VALOR_NUM"), True)
                                        oAlmacenes = Nothing
                                    End If
                                ElseIf sCodDest <> "" Then
                                    Dim oAlmacenes As FSNServer.Almacenes
                                    oAlmacenes = FSNServer.Get_Object(GetType(FSNServer.Almacenes))
                                    oAlmacenes.LoadData(, sCodDest)
                                    If oAlmacenes.Data.Tables(0).Rows.Count = 1 Then
                                        oFSDSEntry.Valor = oAlmacenes.Data.Tables(0).Rows(0).Item("ID").ToString
                                        oFSDSEntry.Text = TextoDelDropDown(oAlmacenes.Data, "ID", oAlmacenes.Data.Tables(0).Rows(0).Item("ID").ToString, True)
                                    End If
                                    oAlmacenes = Nothing
                                End If
                            End If
                        Case TiposDeDatos.TipoCampoGS.Empresa
                            If Not IsDBNull(oRow.Item("VALOR_NUM")) Then
                                Dim oEmpresa As FSNServer.Empresa
                                oEmpresa = FSNServer.Get_Object(GetType(FSNServer.Empresa))
                                oEmpresa.ID = oRow.Item("VALOR_NUM")
                                oEmpresa.Load(sIdi)
                                If Not String.IsNullOrEmpty(oEmpresa.NIF) Then oFSDSEntry.Text = oEmpresa.NIF & " - " & oEmpresa.Den
                                oEmpresa = Nothing

                                oFSDSEntry.Valor = oRow.Item("VALOR_NUM")
                            End If
                        Case TiposDeDatos.TipoCampoGS.IniSuministro
                            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechas") Then
                                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechas", "<script>var sMensajeFecha = '" + JSText(mipage.Textos(10)) + "';</script>")
                            End If
                            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechasDefecto") Then
                                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechasDefecto", "<script>var sMensajeFechaDefecto = '" + JSText(mipage.Textos(17)) + "';</script>")
                            End If
                        Case TiposDeDatos.TipoCampoGS.FinSuministro
                            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechas") Then
                                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechas", "<script>var sMensajeFecha = '" + JSText(mipage.Textos(10)) + "';</script>")
                            End If
                            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechasDefecto") Then
                                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechasDefecto", "<script>var sMensajeFechaDefecto = '" + JSText(mipage.Textos(17)) + "';</script>")
                            End If
                            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "FechaNoAnteriorAlSistema") Then
                                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "FechaNoAnteriorAlSistema", "<script>var sFechaNoAnteriorAlSistema = '" + JSText(mipage.Textos(48)) + "';</script>")
                            End If
                        Case TiposDeDatos.TipoCampoGS.ProveedorAdj
                            If DBNullToSomething(oRow.Item("VALOR_TEXT")) <> Nothing Then
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = oRow.Item("VALOR_TEXT_DEN")
                                Else
                                    oProve = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                                    oProve.Cod = oRow.Item("VALOR_TEXT")
                                    oProve.Load(sIdi, True)
                                    sDenominacion = oProve.Den.ToString.Replace("'", "")
                                End If
                                oFSDSEntry.Text = oRow.Item("VALOR_TEXT") + " - " + sDenominacion
                                oFSDSEntry.Denominacion = sDenominacion
                            End If
                        Case TiposDeDatos.TipoCampoGS.ImporteRepercutido
                            If Not IsDBNull(oRow.Item("VALOR_NUM")) Then
                                oFSDSEntry.Valor = oRow.Item("VALOR_NUM")
                                oFSDSEntry.Text = oRow.Item("VALOR_NUM")
                            End If
                            If mlInstancia = 0 Then
                                oFSDSEntry.MonRepercutido = IIf(IsDBNull(oRow.Item("VALOR_TEXT")), oRow.Item("MONCEN"), oRow.Item("VALOR_TEXT"))
                                oFSDSEntry.CambioRepercutido = oRow.Item("CAMBIO")
                            Else
                                oFSDSEntry.MonRepercutido = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.CambioRepercutido = oRow.Item("CAMBIO")
                            End If
                        Case TiposDeDatos.TipoCampoGS.CentroCoste
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                                If InStrRev(oFSDSEntry.Valor, "#") > 0 Then
                                    oFSDSEntry.Text = Right(oFSDSEntry.Valor, Len(oFSDSEntry.Valor) - InStrRev(oFSDSEntry.Valor, "#"))
                                Else
                                    oFSDSEntry.Text = oFSDSEntry.Valor
                                End If
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
                                    sDenominacion = oFSDSEntry.Text & " - " & sDenominacion
                                Else
                                    'ya nos devuelve el codigo - denominaci�n
                                    Dim oCentrosCoste As FSNServer.CentrosCoste
                                    oCentrosCoste = FSNServer.Get_Object(GetType(FSNServer.CentrosCoste))
                                    oCentrosCoste.BuscarCentrosCoste(oUser.Cod, sIdi, oFSDSEntry.Text, oFSDSEntry.VerUON)
                                    If oCentrosCoste.Data.Tables(0).Rows.Count > 0 Then
                                        sDenominacion = oCentrosCoste.Data.Tables(0).Rows(0).Item("DEN").ToString
                                    End If
                                    oCentrosCoste = Nothing
                                End If

                                oFSDSEntry.Text = sDenominacion
                                oFSDSEntry.Denominacion = sDenominacion
                            Else
                                'Miro si s�lo tiene un centro de coste al que imputar
                                Dim oCentrosCoste As FSNServer.CentrosCoste
                                Dim dCentros As DataSet
                                oCentrosCoste = FSNServer.Get_Object(GetType(FSNServer.CentrosCoste))
                                dCentros = oCentrosCoste.LoadData(oUser.Cod, sIdi, oFSEntry.VerUON)
                                'Del stored viene indicado con valor centro_sm si el centro de coste est� a ese nivel 
                                Dim iLevelCentro As Integer = 1
                                For iLevel As Integer = 0 To dCentros.Tables.Count - 1
                                    If dCentros.Tables(iLevel).Rows.Count > 0 Then
                                        If Not IsDBNull(dCentros.Tables(iLevel).Rows(0).Item("CENTRO_SM")) Then
                                            iLevelCentro = iLevel
                                            Exit For
                                        End If
                                    End If
                                Next
                                If dCentros.Tables(iLevelCentro).Rows.Count = 1 Then
                                    sDenominacion = dCentros.Tables(iLevelCentro).Rows(0).Item("DEN").ToString
                                    oFSDSEntry.Text = sDenominacion
                                    oFSDSEntry.Denominacion = sDenominacion
                                    Dim sValorCentro As String = ""
                                    Select Case iLevelCentro
                                        Case 4
                                            sValorCentro = dCentros.Tables(iLevelCentro).Rows(0).Item("UON1") & "#" & dCentros.Tables(iLevelCentro).Rows(0).Item("UON2") & "#" & dCentros.Tables(iLevelCentro).Rows(0).Item("UON3") & "#" & dCentros.Tables(iLevelCentro).Rows(0).Item("COD")
                                        Case 3
                                            sValorCentro = dCentros.Tables(iLevelCentro).Rows(0).Item("UON1") & "#" & dCentros.Tables(iLevelCentro).Rows(0).Item("UON2") & "#" & dCentros.Tables(iLevelCentro).Rows(0).Item("COD")
                                        Case 2
                                            sValorCentro = dCentros.Tables(iLevelCentro).Rows(0).Item("UON1") & "#" & dCentros.Tables(iLevelCentro).Rows(0).Item("COD")
                                        Case 1
                                            sValorCentro = dCentros.Tables(iLevelCentro).Rows(0).Item("COD")
                                    End Select
                                    oFSDSEntry.Valor = sValorCentro
                                End If
                                oCentrosCoste = Nothing
                            End If
                            If Not IsDBNull(oFSDSEntry.Valor) Then
                                sCodCentroCoste = oFSDSEntry.Valor
                            End If

                        Case TiposDeDatos.TipoCampoGS.Partida
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                                If InStrRev(oFSDSEntry.Valor, "#") > 0 Then
                                    If InStrRev(oFSDSEntry.Valor, "|") > 0 Then 'si no est� en el nivel 1, si no m�s abajo
                                        oFSDSEntry.Text = Right(oFSDSEntry.Valor, Len(oFSDSEntry.Valor) - InStrRev(oFSDSEntry.Valor, "|"))
                                    Else
                                        oFSDSEntry.Text = Right(oFSDSEntry.Valor, Len(oFSDSEntry.Valor) - InStrRev(oFSDSEntry.Valor, "#"))
                                    End If
                                Else
                                    oFSDSEntry.Text = oFSDSEntry.Valor
                                End If
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
                                    sDenominacion = oFSDSEntry.Text & " - " & sDenominacion
                                Else
                                    'ya nos devuelve el codigo - denominaci�n
                                    Dim oPartida As FSNServer.Partida
                                    oPartida = FSNServer.Get_Object(GetType(FSNServer.Partida))
                                    oPartida.BuscarPartida(oUser.Cod, sIdi, oFSDSEntry.PRES5, oFSDSEntry.Text)
                                    If oPartida.Data.Tables(0).Rows.Count > 0 Then
                                        sDenominacion = oPartida.Data.Tables(0).Rows(0).Item("DEN").ToString
                                    End If
                                    oPartida = Nothing
                                End If

                                oFSDSEntry.Text = sDenominacion
                                oFSDSEntry.Denominacion = sDenominacion
                            Else
                                'Miro si s�lo tiene una partida a la que imputar
                                Dim oPartida As FSNServer.Partida
                                Dim dPartidas As DataSet
                                oPartida = FSNServer.Get_Object(GetType(FSNServer.Partida))

                                Dim cPartidas As Fullstep.FSNServer.PRES5
                                cPartidas = Session("FSN_Server").Get_Object(GetType(FSNServer.PRES5))

                                Dim bPlurianual As Boolean = False
                                If Not Session("FSSMPargen") Is Nothing Then
                                    Dim configuracion As FSNServer.PargenSM
                                    configuracion = Session("FSSMPargen").Item(oFSDSEntry.PRES5)
                                    If Not configuracion Is Nothing Then
                                        bPlurianual = configuracion.Plurianual
                                    End If
                                End If

                                If Not String.IsNullOrEmpty(sCodCentroCoste) Then
                                    dPartidas = cPartidas.Partidas_LoadData(oFSDSEntry.PRES5, oUser.Cod, sIdi, bPlurianual, sCodCentroCoste)
                                Else
                                    dPartidas = cPartidas.Partidas_LoadData(oFSDSEntry.PRES5, oUser.Cod, sIdi, bPlurianual)
                                End If

                                'Del stored viene indicado como IMPUTABLE=1 la partidas a nivel minimo seleccionable. Este nivel es un valor entre 1 y 4. Del stored las partidas llegan a partir de la tabla 5.
                                Dim iNivelMinimo As Integer = 5
                                For iLevel As Integer = 5 To 8
                                    If dPartidas.Tables(iLevel).Rows.Count > 0 Then
                                        If dPartidas.Tables(iLevel).Rows(0).Item("IMPUTABLE") = 1 Then
                                            iNivelMinimo = iLevel
                                            Exit For
                                        End If
                                    End If
                                Next
                                If dPartidas.Tables(iNivelMinimo).Rows.Count = 1 Then
                                    If sCodCentroCoste = "" Then
                                        sCodCentroCoste = dPartidas.Tables(iNivelMinimo).Rows(0).Item("UON1") & "#"
                                        If DBNullToStr(dPartidas.Tables(iNivelMinimo).Rows(0).Item("UON2")) <> "" Then sCodCentroCoste = sCodCentroCoste & dPartidas.Tables(iNivelMinimo).Rows(0).Item("UON2") & "#"
                                        If DBNullToStr(dPartidas.Tables(iNivelMinimo).Rows(0).Item("UON3")) <> "" Then sCodCentroCoste = sCodCentroCoste & dPartidas.Tables(iNivelMinimo).Rows(0).Item("UON3") & "#"
                                        If DBNullToStr(dPartidas.Tables(iNivelMinimo).Rows(0).Item("UON4")) <> "" Then sCodCentroCoste = sCodCentroCoste & dPartidas.Tables(iNivelMinimo).Rows(0).Item("UON4") & "#"
                                    End If
                                    If Right(sCodCentroCoste, 1) <> "#" Then
                                        sCodCentroCoste = sCodCentroCoste & "#"
                                    End If
                                    oFSDSEntry.Valor = sCodCentroCoste & dPartidas.Tables(iNivelMinimo).Rows(0).Item("COD")
                                    sDenominacion = dPartidas.Tables(iNivelMinimo).Rows(0).Item("DEN")
                                    oFSDSEntry.Text = sDenominacion
                                    oFSDSEntry.Denominacion = sDenominacion
                                End If
                                oPartida = Nothing
                            End If

                        Case TiposDeDatos.TipoCampoGS.AnyoPartida
                            If Not IsDBNull(oRow.Item("VALOR_NUM")) Then
                                oFSDSEntry.Valor = oRow.Item("VALOR_NUM")
                                oFSDSEntry.Text = oFSDSEntry.Valor
                            Else
                                'Un unico a�o Y vacio ->  lo carga
                                Dim oAnyoPartida As FSNServer.PartidaPRES5
                                oAnyoPartida = FSNServer.Get_Object(GetType(FSNServer.PartidaPRES5))

                                If mioFSDSEntryAnyoPartida Is Nothing Then
                                    If Not oDS.Tables(0).Columns("COPIA_CAMPO_DEF") Is Nothing Then
                                        mioFSDSEntryAnyoPartida = Me.tblDesglose.FindControl("fsdsentry_" + oRow.Item("LINEA").ToString + "_" + oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Partida & " AND LINEA = " & oRow.Item("LINEA").ToString & " AND COPIA_CAMPO_DEF = " & oDS.Tables(0).Select("ID=" & oFSDSEntry.Tag.ToString)(0)("PARTIDAPRES").ToString)(0)("ID").ToString)
                                    Else
                                        mioFSDSEntryAnyoPartida = Me.tblDesglose.FindControl("fsdsentry_" + oRow.Item("LINEA").ToString + "_" + oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Partida & " AND LINEA = " & oRow.Item("LINEA").ToString & " AND ID = " & oDS.Tables(0).Select("ID=" & oFSDSEntry.Tag.ToString)(0)("PARTIDAPRES").ToString)(0)("ID").ToString)
                                    End If
                                End If
                                If Not mioFSDSEntryAnyoPartida Is Nothing Then
                                    oAnyoPartida.LoadDataAnyoPartida(DBNullToStr(mioFSDSEntryAnyoPartida.PRES5), DBNullToStr(NothingToDBNull(mioFSDSEntryAnyoPartida.Valor)))
                                    If oAnyoPartida.DataAnyoPartida.Tables(0).Rows.Count = 1 Then
                                        oFSDSEntry.Valor = oAnyoPartida.DataAnyoPartida.Tables(0).Rows(0)("ANYO")
                                        oFSDSEntry.Text = oFSDSEntry.Valor
                                    End If
                                End If
                            End If

                        Case TiposDeDatos.TipoCampoGS.Activo
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")

                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
                                    sDenominacion = oFSDSEntry.Valor & " - " & sDenominacion
                                Else
                                    Dim SMServer As FSNServer.Root = Session("FSN_Server")
                                    If Not SMServer Is Nothing Then
                                        Dim oActivo As FSNServer.Activo
                                        oActivo = FSNServer.Get_Object(GetType(FSNServer.Activo))
                                        oActivo.BuscarActivo(sIdi, oUser.Cod, oFSDSEntry.Valor)
                                        If oActivo.Data.Tables(0).Rows.Count > 0 Then
                                            sDenominacion = oActivo.Data.Tables(0).Rows(0).Item("DEN").ToString
                                            sDenominacion = oFSDSEntry.Valor & " - " & sDenominacion
                                        End If
                                    End If
                                End If
                                oFSDSEntry.Text = sDenominacion
                                oFSDSEntry.Denominacion = sDenominacion
                            End If
                    End Select
                    If oFSDSEntry.Tabla_Externa > 0 Then
                        oFSDSEntry.Width = Unit.Pixel(250)
                        If Not oFSDSEntry.Valor Is Nothing Then
                            If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                If Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    Dim sTxt As String = oRow.Item("VALOR_TEXT_DEN").ToString()
                                    Dim sTxt1 As String = sTxt.Substring(0, 1)
                                    Dim sTxtn As String = sTxt.Substring(sTxt.Length - 1, 1)
                                    Dim sTxt2 As String = ""
                                    If sTxt.IndexOf("]#[") > 0 Then
                                        sTxt2 = sTxt.Substring(2, sTxt.IndexOf("]#[") - 2)
                                    Else
                                        sTxt2 = sTxt.Substring(2, sTxt.Length - 3)
                                    End If
                                    Select Case sTxt1
                                        Case "s"
                                            oFSDSEntry.Text = sTxt2
                                        Case "n"
                                            oFSDSEntry.Text = Fullstep.FSNLibrary.FormatNumber(Numero(sTxt2, "."), oUser.NumberFormat)
                                        Case "d"
                                            oFSDSEntry.Text = CType(sTxt2, Date).ToString("d", oUser.DateFormat)
                                    End Select
                                    If sTxt.IndexOf("]#[") > 0 Then
                                        oFSDSEntry.Text = oFSDSEntry.Text & " "
                                        sTxt = sTxt.Substring(sTxt.IndexOf("]#[") + 3, sTxt.Length - sTxt.IndexOf("]#[") - 5)
                                        Select Case sTxtn
                                            Case "s"
                                                oFSDSEntry.Text = oFSDSEntry.Text & sTxt
                                            Case "n"
                                                oFSDSEntry.Text = oFSDSEntry.Text & Fullstep.FSNLibrary.FormatNumber(Numero(sTxt, "."), oUser.NumberFormat)
                                            Case "d"
                                                oFSDSEntry.Text = oFSDSEntry.Text & CType(sTxt, Date).ToString("d", oUser.DateFormat)
                                        End Select
                                    End If
                                End If
                                If Not IsDBNull(oRow.Item("VALOR_TEXT_COD")) Then oFSDSEntry.codigoArticulo = oRow.Item("VALOR_TEXT_COD")
                            Else
                                Dim oTablaExterna As FSNServer.TablaExterna = FSNServer.Get_Object(GetType(FSNServer.TablaExterna))
                                oTablaExterna.LoadDefTabla(oFSDSEntry.Tabla_Externa, False)
                                Dim oRowReg As DataRow = oTablaExterna.BuscarRegistro(oFSDSEntry.Valor.ToString(), , True)
                                If Not oRowReg Is Nothing Then
                                    oFSDSEntry.Text = oTablaExterna.DescripcionReg(oRowReg, sIdi, oUser.DateFormat, oUser.NumberFormat)
                                    oFSDSEntry.codigoArticulo = oTablaExterna.CodigoArticuloReg(oRowReg)
                                End If
                            End If
                            oFSDSEntry.Denominacion = oFSDSEntry.Text
                        End If
                    End If
                Else
                    bYaControlado = True
                End If

                If DBNullToSomething(oRow.Item("INDEX")) = 1 Then
                    oFSEntry = Me.tblDesglose.FindControl("fsentry" + sIdCampoHijoDesglose)

                    If oFSEntry.TipoGS <> TiposDeDatos.TipoCampoGS.EstadoInternoNoConf And oFSEntry.ID <> "fsentry" + oCampo.IdGrupo.ToString + CInt(TiposDeDatos.IdsFicticios.Comentario).ToString Then
                        oFSEntry.Valor = oFSDSEntry.Valor
                        oFSEntry.Text = oFSDSEntry.Text
                    End If
                End If
                ''--------DEFECTO---------
                If DBNullToSomething(oRow.Item("INDEX")) = 1 Then
                    oFSDSEntryDefecto = oFSDSEntry
                Else
                    Try
                        oFSDSEntryDefecto = Me.tblDesglose.FindControl("fsdsentry_1_" + sIdCampoHijoDesglose)
                    Catch
                        oFSDSEntryDefecto = Nothing
                    End Try
                End If
            Else ''--------DEFECTO---------
                If Not IsDBNull(oRow.Item("TIPO_CAMPO_GS")) Then
                    If oRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then ' Cuando el Codigo de Articulo este oculto
                        'Articulo oculto y con articulo en la linea
                        If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                            sCodArticulo = oRow.Item("VALOR_TEXT")
                            oArts = FSNServer.Get_Object(GetType(FSNServer.Articulos))
                            Dim oArticulo As FSNServer.PropiedadesArticulo = oArts.Articulo_Get_Properties(sCodArticulo)
                            If oArticulo.ExisteArticulo Then _
                                    bArticuloGenerico = oArticulo.Generico
                        Else
                            bArticuloGenerico = True
                        End If
                    End If
                End If
            End If
        Next

        Try
            'si no es popup se usa lo de bbdd siempre
            'si es popup la primera vez q lo abres se usa bbdd
            'si es popup la segunda vez q abres sin hacer postback se lee de request machacando lo de bbdd. El defecto es sobre lo de bbdd.
            CargarValoresDefecto(oDsDefecto, sIdi, iLineas, oCampo, bQA)
        Catch ex As Exception
        End Try
        oDS = Nothing

        Dim moduloidioma As Integer = mipage.ModuloIdioma
        mipage.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Menu
        lblCargandoDesglose.Text = mipage.Textos(28)
        mipage.ModuloIdioma = moduloidioma

        sScriptFechasSuministro = String.Format(IncludeScriptKeyFormat, "javascript", sScriptFechasSuministro)
        If Not Page.ClientScript.IsStartupScriptRegistered("arrFechasDesgloseAUX") Then _
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "arrFechasDesgloseAUX", sScriptFechasSuministro)

        sScript = "var sMensajeGenerico = '" + JSText(mipage.Textos(14)) + "';"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        If Not Page.ClientScript.IsStartupScriptRegistered("MensajeGenericos") Then _
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MensajeGenericos", sScript)

        sScript = ""
        sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
        sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
        sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
        sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)

        If Not Page.ClientScript.IsStartupScriptRegistered("LongitudesCodigosKey") Then _
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "LongitudesCodigosKey", sScript)

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "TextosBooleanos") Then _
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "TextosBooleanos", "<script>var sTextoSi = '" & JSText(mipage.Textos(15)) & "';var sTextoNo = '" & JSText(mipage.Textos(16)) & "';</script>")

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "InfraStylePath") Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "InfraStylePath", "<script>var InfraStylePath = '" & ConfigurationManager.AppSettings("InfraStylePath") & "';</script>")
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "rutaPM") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "claveArrayDesgloses") Then _
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")

        If Not Page.ClientScript.IsStartupScriptRegistered(Me.tblDesglose.ClientID) Then _
            Page.ClientScript.RegisterStartupScript(Page.GetType(), Me.tblDesglose.ClientID, "<script>arrDesgloses[arrDesgloses.length] = '" + Me.tblDesglose.ClientID + "';</script>")

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "theme") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "theme", "var theme = '" & Page.Theme & "';", True)

        sScript = ""
        sScript += "arrEstadosInternos[1] = new Array();arrEstadosInternos[1][0]=1;arrEstadosInternos[1][1]='" + JSText(mipage.Textos(5)) + "';"
        sScript += "arrEstadosInternos[2] = new Array();arrEstadosInternos[2][0]=2;arrEstadosInternos[2][1]='" + JSText(mipage.Textos(6)) + "';"
        sScript += "arrEstadosInternos[3] = new Array();arrEstadosInternos[3][0]=3;arrEstadosInternos[3][1]='" + JSText(mipage.Textos(7)) + "';"
        sScript += "arrEstadosInternos[4] = new Array();arrEstadosInternos[4][0]=4;arrEstadosInternos[4][1]='" + JSText(mipage.Textos(8)) + "';"
        sScript += "arrEstadosInternos[5] = new Array();arrEstadosInternos[5][0]=5;arrEstadosInternos[5][1]='" + JSText(mipage.Textos(9)) + "';"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        If Not Page.ClientScript.IsStartupScriptRegistered("arrEstadosInternos") Then _
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "arrEstadosInternos", sScript)

        sScript = ""
        sScript += "var lIdEstadoActual = '" + CInt(TiposDeDatos.IdsFicticios.EstadoActual).ToString + "';"
        sScript += "var lIdComentario = '" + CInt(TiposDeDatos.IdsFicticios.Comentario).ToString + "';"
        sScript += "var lIdEstadoInterno = '" + CInt(TiposDeDatos.IdsFicticios.EstadoInterno).ToString + "';"
        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        If Not Page.ClientScript.IsStartupScriptRegistered("idsFicticios") Then _
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "idsFicticios", sScript)

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
            Dim sVariableJavascriptTextosPantalla As String = "var TextosPantalla = new Array();"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[0]='" & JSText(mipage.Textos(24)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[1]='" & JSText(mipage.Textos(40)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[2]='" & JSText(mipage.Textos(41)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[3]='" & JSText(mipage.Textos(42)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[4]='" & JSText(mipage.Textos(43)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[5]='" & JSText(mipage.Textos(44)) & "';"
            sVariableJavascriptTextosPantalla &= "TextosPantalla[6]='" & JSText(mipage.Textos(45)) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextosPantalla, True)
        End If

        If Not Page.ClientScript.IsStartupScriptRegistered("MostrarCargando") Then
            Dim scriptMostrarCargando As String = "function MostrarCargando(){ $('#popupFondoDesglose').show(); $('#pnlCargandoDesglose').show(); };"
            scriptMostrarCargando &= "function OcultarCargando(){ $('#popupFondoDesglose').hide(); $('#pnlCargandoDesglose').hide(); }"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MostrarCargando", scriptMostrarCargando, True)
        End If

        If Not bHayCamposModificables And bPopUp Then
            If Not Page.ClientScript.IsStartupScriptRegistered("QuitarBotonGuardar") Then _
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "QuitarBotonGuardar", "<script> If (document.getElementById('cmdGuardar')) document.getElementById('cmdGuardar').style.visibility='hidden'; if (document.getElementById('cmdCalcular')) document.getElementById('cmdCalcular').style.visibility='hidden';</script>")
        Else
            If Not bHayCamposModificablesNumerico And bPopUp Then
                If Not Page.ClientScript.IsStartupScriptRegistered("QuitarBotonCalcular") Then _
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "QuitarBotonCalcular", "<script> if (document.getElementById('cmdCalcular')) document.getElementById('cmdCalcular').style.visibility='hidden';</script>")
            End If
        End If
    End Sub

#Region "Funciones de generaci�n de GeneralEntry desglose"
    Private Function CrearObjetoDataEntryDesglose(ByRef oFSEntry As DataEntry.GeneralEntry, ByVal sIdCampoDesglose As String, ByVal iTipoGS As TiposDeDatos.TipoCampoGS, ByVal iTipo As TiposDeDatos.TipoGeneral,
                                                  ByRef oRow As DataRow, ByVal i As Integer, ByRef vLineasColumnas As Integer(), ByVal contaLinea As Integer, ByRef oDS As DataSet,
                                                  ByVal sInstanciaMoneda As String, ByVal sCodCentroCoste As String) As DataEntry.GeneralEntry
        Dim oFSDSEntry As New DataEntry.GeneralEntry
        oFSDSEntry.Desglose = True
        oFSDSEntry.DesgloseCambiaWidth = True
        oFSDSEntry.PM = Me.PM
        oFSDSEntry.TipoSolicit = mTipoSolicitud
        oFSDSEntry.MonSolicit = msMonedaSolicitud
        oFSDSEntry.CampoDef_CampoODesgHijo = oFSEntry.CampoDef_CampoODesgHijo
        oFSDSEntry.CampoDef_DesgPadre = oFSEntry.CampoDef_DesgPadre
        oFSDSEntry.ID = "fsdsentry_" + i.ToString + "_" + sIdCampoDesglose
        oFSDSEntry.DropDownGridID = oFSEntry.ID
        oFSDSEntry.IdCampoPadre = oFSEntry.IdCampoPadre
        oFSDSEntry.TipoGS = iTipoGS
        oFSDSEntry.Width = oFSEntry.Width
        oFSDSEntry.Tipo = iTipo
        oFSDSEntry.Intro = oFSEntry.Intro
        oFSDSEntry.Tag = oFSEntry.Tag
        oFSDSEntry.Title = oFSEntry.Title
        oFSDSEntry.WindowTitle = Me.Page.Title
        oFSDSEntry.DenDesgloseMat = DBNullToStr(Me.Titulo)
        oFSDSEntry.DenGrupo = oFSEntry.DenGrupo
        oFSDSEntry.IsGridEditor = False
        oFSDSEntry.IdContenedor = oFSEntry.IdContenedor
        oFSDSEntry.TabContainer = oFSEntry.TabContainer
        oFSDSEntry.ReadOnly = oFSEntry.ReadOnly
        oFSDSEntry.RecordarValores = oFSEntry.RecordarValores
        oFSDSEntry.UsarOrgCompras = oFSEntry.UsarOrgCompras
        oFSDSEntry.ActivoSM = CType(Me.Page, FSNPage).Acceso.gbAccesoFSSM
        ''Comprobar Si tiene bloqueos de escritura 
        If Not oFSDSEntry.ReadOnly And PM Then
            If oRow.GetChildRows("REL_CAMPO_BLOQUEO").Length > 0 Then
                Dim bCumple As Boolean
                bCumple = ComprobarCondiciones(mlInstancia, oRow.Item("ESCRITURA_FORMULA"), oRow.GetChildRows("REL_CAMPO_BLOQUEO"), i)

                oFSDSEntry.ReadOnly = bCumple
                If bCumple Then
                    oFSDSEntry.LabelStyle.CssClass = "CampoSoloLectura"
                    oFSDSEntry.InputStyle.CssClass = "trasparent"
                    If (oFSDSEntry.TipoGS = TiposDeDatos.TipoGeneral.TipoArchivo) Or (oFSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoArchivo) Then
                        oFSDSEntry.InputStyle.Font.Size = System.Web.UI.WebControls.FontUnit.Parse("10px")
                    End If
                Else
                    oFSDSEntry.InputStyle.CopyFrom(oFSEntry.InputStyle)
                End If
            Else
                oFSDSEntry.InputStyle.CopyFrom(oFSEntry.InputStyle)
            End If
        Else
            oFSDSEntry.InputStyle.CopyFrom(oFSEntry.InputStyle)
        End If
        oFSDSEntry.Obligatorio = oFSEntry.Obligatorio
        oFSDSEntry.BaseDesglose = False

        oFSDSEntry.Maximo = oFSEntry.Maximo
        oFSDSEntry.Minimo = oFSEntry.Minimo
        oFSDSEntry.Lista = oFSEntry.Lista
        oFSDSEntry.DependentField = Replace(oFSEntry.DependentField, "fsentry", "fsdsentry_" + i.ToString + "_")
        oFSDSEntry.ValidationControl = oFSEntry.ValidationControl
        oFSDSEntry.MaxLength = oFSEntry.MaxLength
        oFSDSEntry.PopUpStyle = oFSEntry.PopUpStyle
        oFSDSEntry.PUCaptionBoton = oFSEntry.PUCaptionBoton
        oFSDSEntry.ButtonStyle.CopyFrom(oFSEntry.ButtonStyle)
        oFSDSEntry.Restric = oFSEntry.Restric
        oFSDSEntry.Calculado = oFSEntry.Calculado
        oFSDSEntry.NumberFormat = oUser.NumberFormat
        oFSDSEntry.DateFormat = oUser.DateFormat
        oFSDSEntry.Idi = oFSEntry.Idi
        oFSDSEntry.Orden = oFSEntry.Orden
        oFSDSEntry.AnyadirArt = oFSEntry.AnyadirArt
        oFSDSEntry.IdAtrib = oFSEntry.IdAtrib
        oFSDSEntry.ValErp = oFSEntry.ValErp
        oFSDSEntry.CargarUltADJ = oFSEntry.CargarUltADJ
        oFSDSEntry.CargarProveSumiArt = oFSEntry.CargarProveSumiArt
        oFSDSEntry.idEntryART = oFSEntry.idEntryART
        oFSDSEntry.ErrMsg = oFSEntry.ErrMsg
        oFSDSEntry.ErrMsgFormato = oFSEntry.ErrMsgFormato
        oFSDSEntry.idEntryProveSumiArt = oFSEntry.idEntryProveSumiArt
        oFSDSEntry.nLinea = i
        oFSDSEntry.nColumna = vLineasColumnas(contaLinea - 1)
        vLineasColumnas(contaLinea - 1) = vLineasColumnas(contaLinea - 1) + 1

        oFSDSEntry.IdCampoPadreOrigen = oFSEntry.IdCampoPadreOrigen
        oFSDSEntry.Campo_Origen = oFSEntry.Campo_Origen
        oFSDSEntry.Tabla_Externa = oFSEntry.Tabla_Externa
        oFSDSEntry.VerUON = oFSEntry.VerUON
        oFSDSEntry.PRES5 = oFSEntry.PRES5
        oFSDSEntry.idEntryCC = oFSEntry.idEntryCC
        oFSDSEntry.Servicio = oFSEntry.Servicio
        oFSDSEntry.Formato = oFSEntry.Formato
        oFSDSEntry.FechaNoAnteriorAlSistema = oFSEntry.FechaNoAnteriorAlSistema
        oFSDSEntry.idEntryPartidaPlurianual = oFSEntry.idEntryPartidaPlurianual
        oFSDSEntry.EntryPartidaPlurianualCampo = oFSEntry.EntryPartidaPlurianualCampo
        If Not (oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.Material) Then
            If oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.EstadoNoConf Then
                If mlInstancia > 0 Then
                    If oDS.Tables(4).Rows.Count > 0 AndAlso oDS.Tables(4).Rows.Count > (i - 1) Then
                        If Not IsDBNull(oDS.Tables(4).Rows.Item(i - 1).Item("ESTADO")) Then
                            oFSDSEntry.Text = oDS.Tables(4).Rows.Item(i - 1).Item("ESTADO")
                            oFSDSEntry.Texto_Defecto = oFSEntry.Text
                        End If
                    End If
                End If
            ElseIf oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.ImporteRepercutido Then
                oFSDSEntry.Texto_Defecto = oFSEntry.Texto_Defecto

                oFSDSEntry.MonCentral = oFSEntry.MonCentral
                oFSDSEntry.CambioCentral = oFSEntry.CambioCentral

                oFSDSEntry.MonRepercutido = oFSEntry.MonCentral
                oFSDSEntry.CambioRepercutido = oFSEntry.CambioCentral
            ElseIf oFSEntry.TipoGS = TiposDeDatos.IdsFicticios.NumLinea Then
                oFSDSEntry.Valor = oFSDSEntry.nLinea
                oFSDSEntry.Text = oFSDSEntry.nLinea
                oFSDSEntry.Texto_Defecto = Nothing
            Else
                If oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.NuevoCodArticulo Or oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.DenArticulo Then
                    oFSDSEntry.InstanciaMoneda = sInstanciaMoneda
                    oFSDSEntry.ToolTip = oFSEntry.ToolTip
                End If
                oFSDSEntry.Valor = oFSEntry.Valor
                oFSDSEntry.Text = oFSEntry.Text
                oFSDSEntry.Texto_Defecto = oFSEntry.Text
            End If
        End If
        'MasterField
        oFSDSEntry.MasterField = oFSEntry.MasterField.Replace("fsentry", "fsdsentry_" + i.ToString + "_")
        If oFSDSEntry.Intro = 2 Then
            If idDataEntryFORMOrgCompras <> "" Then
                oFSDSEntry.idDataEntryDependent = oFSEntry.idDataEntryDependent
            Else
                oFSDSEntry.idDataEntryDependent = Replace(oFSEntry.idDataEntryDependent, "fsentry_", "fsdsentry_" + i.ToString + "_")
            End If
        End If

        Select Case oFSDSEntry.TipoGS
            Case TiposDeDatos.TipoCampoGS.Proveedor
                TratarTipoCampoGSProveedorDesglose(oFSDSEntry, oFSEntry, i)
            Case TiposDeDatos.TipoCampoGS.ProveedorERP
                TratarTipoCampoGSProveedorERPDesglose(oFSDSEntry, oFSEntry, i)
            Case TiposDeDatos.TipoCampoGS.Material
                TratarTipoCampoGSMaterialDesglose(oFSDSEntry, oFSEntry, i)
            Case TiposDeDatos.TipoCampoGS.IniSuministro, TiposDeDatos.TipoCampoGS.FinSuministro
                TratarTipoCampoGSIniFinSuministroDesglose(oFSDSEntry, oFSEntry, i)
            Case TiposDeDatos.TipoCampoGS.OrganizacionCompras, TiposDeDatos.TipoCampoGS.Centro
                TratarTipoCampoGSOrgComprasCentroDesglose(oFSDSEntry, oFSEntry, i, oDS)
            Case TiposDeDatos.TipoCampoGS.UnidadOrganizativa
                TratarTipoCampoGSUnidadOrganizativaDesglose(oFSDSEntry, oFSEntry, i)
            Case TiposDeDatos.TipoCampoGS.CentroCoste
                TratarTipoCampoGSCentroCosteDesglose(oFSDSEntry, oFSEntry, i, sCodCentroCoste)
            Case TiposDeDatos.TipoCampoGS.NuevoCodArticulo, TiposDeDatos.TipoCampoGS.DenArticulo, TiposDeDatos.TipoCampoGS.CodArticulo
                TratarTipoCampoGSArticuloDesglose(oFSDSEntry, oFSEntry, i, sCodCentroCoste, oDS)
            Case TiposDeDatos.TipoCampoGS.Unidad
                TratarTipoCampoGSUnidadDesglose(oFSDSEntry, oFSEntry, i)
            Case TiposDeDatos.TipoCampoGS.Partida, TiposDeDatos.TipoCampoGS.Activo
                TratarTipoCampoGSPartidaActivoDesglose(oFSDSEntry, oFSEntry, i)
            Case TiposDeDatos.TipoCampoGS.AnyoPartida
                TratarTipoCampoGSAnyoPartidaDesglose(oFSDSEntry, oFSEntry, i, oDS, oRow)
            Case TiposDeDatos.TipoCampoGS.PRES1, TiposDeDatos.TipoCampoGS.Pres2, TiposDeDatos.TipoCampoGS.Pres3, TiposDeDatos.TipoCampoGS.Pres4
                TratarTipoCampoGSPresDesglose(oFSDSEntry, oFSEntry)
            Case TiposDeDatos.TipoCampoGS.Dest
                TratarTipoCampoGSDestDesglose(oFSDSEntry, oFSEntry, i)
        End Select

        Return oFSDSEntry
    End Function

#End Region

#Region "Funciones de tratamiento de campos desglose"

    Private Sub TratarTipoCampoGSProveedorDesglose(ByRef oFSDSEntry As DataEntry.GeneralEntry, ByRef oFSEntry As DataEntry.GeneralEntry, ByVal i As Integer)
        If idDataEntryFORMOrgCompras <> "" Then
            oFSDSEntry.idDataEntryDependent = oFSEntry.idDataEntryDependent
        Else
            oFSDSEntry.idDataEntryDependent = Replace(oFSEntry.idDataEntryDependent, "fsentry_", "fsdsentry_" + i.ToString + "_")
        End If
        oFSDSEntry.DependentField = Replace(oFSEntry.DependentField, "fsentry_", "fsdsentry_" + i.ToString + "_")
        oFSDSEntry.idDataEntryDependent2 = Replace(oFSEntry.idDataEntryDependent2, "fsentry_", "fsdsentry_" + i.ToString + "_")
        oFSDSEntry.idEntryART = Replace(oFSEntry.idEntryART, "fsentry_", "fsdsentry_" + i.ToString + "_")
    End Sub

    Private Sub TratarTipoCampoGSProveedorERPDesglose(ByRef oFSDSEntry As DataEntry.GeneralEntry, ByRef oFSEntry As DataEntry.GeneralEntry, ByVal i As Integer)
        If idDataEntryFORMOrgCompras <> "" Then
            oFSDSEntry.idDataEntryDependent = oFSEntry.idDataEntryDependent
        Else
            oFSDSEntry.idDataEntryDependent = Replace(oFSEntry.idDataEntryDependent, "fsentry_", "fsdsentry_" + i.ToString + "_")
        End If
        oFSDSEntry.idEntryPROV = Replace(oFSEntry.idEntryPROV, "fsentry_", "fsdsentry_" + i.ToString + "_")
    End Sub

    Private Sub TratarTipoCampoGSMaterialDesglose(ByRef oFSDSEntry As DataEntry.GeneralEntry, ByRef oFSEntry As DataEntry.GeneralEntry, ByVal i As Integer)
        oFSDSEntry.idDataEntryDependent2 = Replace(oFSEntry.idDataEntryDependent2, "fsentry_", "fsdsentry_" + i.ToString + "_")
        oFSDSEntry.idDataEntryFORMMaterial = Me.idDataEntryFORMMaterial
    End Sub

    Private Sub TratarTipoCampoGSIniFinSuministroDesglose(ByRef oFSDSEntry As DataEntry.GeneralEntry, ByRef oFSEntry As DataEntry.GeneralEntry, ByVal i As Integer)
        oFSDSEntry.idDataEntryDependent = Replace(oFSEntry.idDataEntryDependent, "fsentry", "fsdsentry_" + i.ToString)
    End Sub

    Private Sub TratarTipoCampoGSOrgComprasCentroDesglose(ByRef oFSDSEntry As DataEntry.GeneralEntry, ByRef oFSEntry As DataEntry.GeneralEntry, ByVal i As Integer, ByRef oDS As DataSet)
        oFSDSEntry.idDataEntryDependent = Replace(oFSEntry.idDataEntryDependent, "fsentry", "fsdsentry_" + i.ToString)
        If oFSDSEntry.TipoGS = TiposDeDatos.TipoCampoGS.OrganizacionCompras Then
            oFSDSEntry.DependentField = Replace(oFSEntry.DependentField, "fsentry_", "fsdsentry_" + i.ToString + "_")
            oFSDSEntry.idDataEntryDependent2 = Replace(oFSEntry.idDataEntryDependent2, "fsentry_", "fsdsentry_" + i.ToString + "_")
        End If
        If oFSDSEntry.TipoGS = TiposDeDatos.TipoCampoGS.Centro Then
            'Relacionar la organizacion de compras con el primer Centro del Desglose
            If sIDCampoOrgCompras <> "" Then
                Dim mioFSDSEntry As DataEntry.GeneralEntry
                mioFSDSEntry = Me.Parent.FindControl("fsentry" & sIDCampoOrgCompras)
                If Not mioFSDSEntry Is Nothing Then
                    mioFSDSEntry.idDataEntryDependent2 = oFSDSEntry.ClientID
                End If
            End If

            If CodOrgCompras <> "" Then
                oFSDSEntry.DependentValue = CodOrgCompras
            Else
                If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString).Length > 0 Then
                    oFSDSEntry.DependentValue = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString)(0).Item("VALOR_TEXT"))
                End If
            End If
            'Relacionar el Centro con la Organizacion de Compras fuera del desglose (Si lo hubiera)
            oFSDSEntry.idDataEntryDependent2 = Replace(oFSEntry.idDataEntryDependent2, "fsentry", "fsdsentry_" + i.ToString + "_")
            'Relacionar el Centro con el almacen (Si lo hubiera)
            oFSDSEntry.idDataEntryDependent3 = Replace(oFSEntry.idDataEntryDependent3, "fsentry", "fsdsentry_" + i.ToString + "_")
        End If
    End Sub

    Private Sub TratarTipoCampoGSUnidadOrganizativaDesglose(ByRef oFSDSEntry As DataEntry.GeneralEntry, ByRef oFSEntry As DataEntry.GeneralEntry, ByVal i As Integer)
        If idDataEntryFORMUnidadOrganizativa <> "" Then
            oFSDSEntry.idDataEntryDependent = oFSEntry.idDataEntryDependent
            Dim mioFSDSEntry As DataEntry.GeneralEntry
            mioFSDSEntry = Me.Parent.FindControl("fsentry" & sUnidadOrganizativa)
            If Not mioFSDSEntry Is Nothing Then
                mioFSDSEntry.idDataEntryDependent = oFSDSEntry.ClientID
            End If
        Else
            oFSDSEntry.idDataEntryDependent = Replace(oFSEntry.idDataEntryDependent, "fsentry_", "fsdsentry_" + i.ToString + "_")
        End If
    End Sub

    Private Sub TratarTipoCampoGSCentroCosteDesglose(ByRef oFSDSEntry As DataEntry.GeneralEntry, ByRef oFSEntry As DataEntry.GeneralEntry, ByVal i As Integer, ByVal sCodCentroCoste As String)
        If idDataEntryFORMCentroCoste <> "" Then
            oFSDSEntry.idDataEntryDependent = oFSEntry.idDataEntryDependent
            Dim mioFSDSEntry As DataEntry.GeneralEntry
            mioFSDSEntry = Me.Parent.FindControl("fsentry" & sCodCentroCoste)
            If Not mioFSDSEntry Is Nothing Then
                mioFSDSEntry.idDataEntryDependent = oFSDSEntry.ClientID
            End If
        Else
            oFSDSEntry.idDataEntryDependent = Replace(oFSEntry.idDataEntryDependent, "fsentry_", "fsdsentry_" + i.ToString + "_")
        End If
    End Sub

    Private Sub TratarTipoCampoGSArticuloDesglose(ByRef oFSDSEntry As DataEntry.GeneralEntry, ByRef oFSEntry As DataEntry.GeneralEntry, ByVal i As Integer, ByVal sCodCentroCoste As String, ByRef oDS As DataSet)
        If idDataEntryFORMOrgCompras <> "" Then
            oFSDSEntry.idDataEntryDependent = oFSEntry.idDataEntryDependent
            Dim mioFSDSEntry As DataEntry.GeneralEntry
            mioFSDSEntry = Me.Parent.FindControl("fsentry" & sIDCampoOrgCompras)
            If Not mioFSDSEntry Is Nothing Then
                mioFSDSEntry.idDataEntryDependent = oFSDSEntry.ClientID
            End If
        Else
            oFSDSEntry.idDataEntryDependent = Replace(oFSEntry.idDataEntryDependent, "fsentry_", "fsdsentry_" + i.ToString + "_")
        End If
        If idDataEntryFORMCentro <> "" Then
            oFSDSEntry.idDataEntryDependent2 = oFSEntry.idDataEntryDependent2
            Dim mioFSDSEntry As DataEntry.GeneralEntry
            mioFSDSEntry = Me.Parent.FindControl("fsentry" & sIDCampoCentro)
            If Not mioFSDSEntry Is Nothing Then
                mioFSDSEntry.idDataEntryDependent = oFSDSEntry.ClientID
            End If
        Else
            oFSDSEntry.idDataEntryDependent2 = Replace(oFSEntry.idDataEntryDependent2, "fsentry_", "fsdsentry_" + i.ToString + "_")
        End If
        If idDataEntryFORMUnidadOrganizativa <> "" Then
            oFSDSEntry.idDataEntryDependent3 = oFSEntry.idDataEntryDependent3
            Dim mioFSDSEntry As DataEntry.GeneralEntry
            mioFSDSEntry = Me.Parent.FindControl("fsentry" & sUnidadOrganizativa)
            If Not mioFSDSEntry Is Nothing Then
                mioFSDSEntry.idDataEntryDependent3 = oFSDSEntry.ClientID
            End If
        Else
            oFSDSEntry.idDataEntryDependent3 = Replace(oFSEntry.idDataEntryDependent3, "fsentry_", "fsdsentry_" + i.ToString + "_")
        End If
        If idDataEntryFORMCentroCoste <> "" Then
            oFSDSEntry.idDataEntryDependentCentroCoste = oFSEntry.idDataEntryDependentCentroCoste
            Dim mioFSDSEntry As DataEntry.GeneralEntry
            mioFSDSEntry = Me.Parent.FindControl("fsentry" & sCodCentroCoste)
            If Not mioFSDSEntry Is Nothing Then mioFSDSEntry.idDataEntryDependentCentroCoste = oFSDSEntry.ClientID
        Else
            oFSDSEntry.idDataEntryDependentCentroCoste = Replace(oFSEntry.idDataEntryDependentCentroCoste, "fsentry_", "fsdsentry_" + i.ToString + "_")
        End If

        If Not (oFSEntry.IdMaterialOculta Is System.DBNull.Value) Then
            oFSDSEntry.IdMaterialOculta = Replace(oFSEntry.IdMaterialOculta, "fsentry_", "fsdsentry_" + i.ToString + "_")
        End If
        If Not (oFSEntry.IdCodArticuloOculta Is System.DBNull.Value) Then
            oFSDSEntry.IdCodArticuloOculta = Replace(oFSEntry.IdCodArticuloOculta, "fsentry_", "fsdsentry_" + i.ToString + "_")
        End If
        If Not (oFSEntry.IdDenArticuloOculta Is System.DBNull.Value) Then
            oFSDSEntry.IdDenArticuloOculta = Replace(oFSEntry.IdDenArticuloOculta, "fsentry_", "fsdsentry_" + i.ToString + "_")
        End If

        'Buscar la organizacion de compras y el centro
        If CodOrgCompras <> "" Then
            oFSDSEntry.DependentValueDataEntry = CodOrgCompras
        Else
            If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString).Length > 0 Then
                oFSDSEntry.DependentValueDataEntry = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString)(0).Item("VALOR_TEXT"))
            End If
        End If

        If CodCentro <> "" Then
            oFSDSEntry.DependentValueDataEntry2 = CodCentro 'Centro Fuera del desglose (Oculto)
        Else
            If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Centro & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString).Length > 0 Then
                oFSDSEntry.DependentValueDataEntry2 = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Centro & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString)(0).Item("VALOR_TEXT"))
            End If
        End If

        If UnidadOrganizativa <> "" Then
            oFSDSEntry.DependentValueDataEntry3 = UnidadOrganizativa
        Else
            If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString).Length > 0 Then
                oFSDSEntry.DependentValueDataEntry3 = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString)(0).Item("VALOR_TEXT"))
            End If
        End If

        If Not CentroCoste = String.Empty Then
            oFSDSEntry.DependentValueCentroCoste = CentroCoste
        Else
            If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString).Length > 0 Then
                oFSDSEntry.DependentValueCentroCoste = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString)(0).Item("VALOR_TEXT"))
            End If
        End If

        If Not (oFSEntry.idEntryCANT Is System.DBNull.Value) Then
            oFSDSEntry.idEntryCANT = Replace(oFSEntry.idEntryCANT, "fsentry_", "fsdsentry_" + i.ToString + "_")
        End If
        If Not (oFSEntry.idEntryUNI Is System.DBNull.Value) Then
            oFSDSEntry.idEntryUNI = Replace(oFSEntry.idEntryUNI, "fsentry_", "fsdsentry_" + i.ToString + "_")
        End If
        If Not (oFSEntry.idEntryUNIPedido Is System.DBNull.Value) Then
            oFSDSEntry.idEntryUNIPedido = Replace(oFSEntry.idEntryUNIPedido, "fsentry_", "fsdsentry_" + i.ToString + "_")
        End If
        If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString).Length > 0 Then
            oFSDSEntry.UnidadOculta = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString)(0).Item("VALOR_TEXT"))
        End If
        If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString).Length > 0 Then
            oFSDSEntry.UnidadPedidoOculta = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString)(0).Item("VALOR_TEXT"))
        End If

        oFSDSEntry.idEntryPROV = Replace(oFSEntry.idEntryPROV, "fsentry_", "fsdsentry_" + i.ToString + "_")
        oFSDSEntry.idEntryPREC = Replace(oFSEntry.idEntryPREC, "fsentry_", "fsdsentry_" + i.ToString + "_")
        oFSDSEntry.idEntryCC = Replace(oFSEntry.idEntryCC, "fsentry_", "fsdsentry_" + i.ToString + "_")
        oFSDSEntry.idEntryProveSumiArt = Replace(oFSEntry.idEntryProveSumiArt, "fsentry_", "fsdsentry_" + i.ToString + "_")

        If idDataEntryFORMMaterial <> "" Then
            oFSDSEntry.idDataEntryFORMMaterial = oFSEntry.idDataEntryFORMMaterial
        End If
    End Sub

    Private Sub TratarTipoCampoGSUnidadDesglose(ByRef oFSDSEntry As DataEntry.GeneralEntry, ByRef oFSEntry As DataEntry.GeneralEntry, ByVal i As Integer)
        If Not (oFSEntry.idEntryCANT Is System.DBNull.Value) Then
            oFSDSEntry.idEntryCANT = Replace(oFSEntry.idEntryCANT, "fsentry_", "fsdsentry_" + i.ToString + "_")
        End If
    End Sub

    Private Sub TratarTipoCampoGSPartidaActivoDesglose(ByRef oFSDSEntry As DataEntry.GeneralEntry, ByRef oFSEntry As DataEntry.GeneralEntry, ByVal i As Integer)
        oFSDSEntry.idEntryCC = Replace(oFSEntry.idEntryCC, "fsentry_", "fsdsentry_" + i.ToString + "_")
    End Sub

	Private Sub TratarTipoCampoGSAnyoPartidaDesglose(ByRef oFSDSEntry As DataEntry.GeneralEntry, ByRef oFSEntry As DataEntry.GeneralEntry, ByVal i As Integer, ByRef oDs As DataSet, ByRef oRow As DataRow)
		Dim Quien As String = If(oDs.Tables(0).Columns("COPIA_CAMPO_DEF") Is Nothing, "ID", "COPIA_CAMPO_DEF")

		If Not String.IsNullOrEmpty(oRow.Item("PARTIDAPRES").ToString) Then
			If oDs.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Partida & " AND " & Quien & " = " & oRow.Item("PARTIDAPRES")).Length = 0 Then
				oFSDSEntry.idEntryPartidaPlurianual = oFSEntry.idEntryPartidaPlurianual
			Else
				oFSDSEntry.idEntryPartidaPlurianual = Replace(oFSEntry.idEntryPartidaPlurianual, "fsentry", "fsdsentry_" + i.ToString + "_")
			End If
			oFSDSEntry.EntryPartidaPlurianualCampo = oFSEntry.EntryPartidaPlurianualCampo
		End If
	End Sub

	Private Sub TratarTipoCampoGSPresDesglose(ByRef oFSDSEntry As DataEntry.GeneralEntry, ByRef oFSEntry As DataEntry.GeneralEntry)
        oFSDSEntry.Posee_Defecto = oFSEntry.Posee_Defecto
        oFSDSEntry.Texto_Defecto = oFSEntry.Texto_Defecto
        oFSDSEntry.Valor_Defecto = oFSEntry.Valor_Defecto
    End Sub

    Private Sub TratarTipoCampoGSDestDesglose(ByRef oFSDSEntry As DataEntry.GeneralEntry, ByRef oFSEntry As DataEntry.GeneralEntry, ByVal i As Integer)
        If Not oFSDSEntry.UsarOrgCompras Then
            'Relacionar el Centro con el almacen (Si lo hubiera)
            oFSDSEntry.idDataEntryDependent3 = Replace(oFSEntry.idDataEntryDependent3, "fsentry", "fsdsentry_" + i.ToString + "_")
        End If
    End Sub

#End Region

#Region "Funciones de generaci�n de GeneralEntry"
    Private Function CrearObjetoDataEntry(ByRef oRow As DataRow, ByRef oDS As DataSet, ByRef oCampo As FSNServer.Campo, ByVal CopiaCampoDef_Padre As Long, ByVal sIdCampoDesglose As String, ByVal idCampo As Long,
                                          ByVal idCampoOrigen As Long, ByVal iTipoGS As TiposDeDatos.TipoCampoGS, ByVal iTipo As TiposDeDatos.TipoGeneral, ByVal bPopUp As Boolean, ByVal sRestrictMat As String,
                                          ByRef arrMat As String(), ByRef sKeyMaterial As String, ByVal sInstanciaMoneda As String, ByRef sIdCampoProveedor As String, ByVal sKeyTipoRecepcion As String,
                                          ByRef sKeyDestino As String, ByRef sKeyPais As String, ByRef iNivel As Integer, ByVal sCodPaisDefecto As String, ByRef sKeyOrgCompras As String, ByVal sOrden As String,
                                          ByRef sKeyUnidadOrganizativa As String, ByVal sCodUnidadOrgDefecto As String, ByRef sKeyOrgComprasDataCheck As String, ByVal sCodOrgCompraDefecto As String,
                                          ByVal bCentroRelacionadoFORM As Boolean, ByVal sCodOrgComprasFORM As String, ByRef sKeyCentro As String, ByVal sCodCentroDefecto As String, ByRef sKeyArticulo As String,
                                          ByRef bHayCamposModificables As Boolean, ByRef bHayCamposModificablesNumerico As Boolean) As DataEntry.GeneralEntry
        Dim sIDCentro As String = String.Empty
        Dim sKeyAlmacen As String = String.Empty
        Dim sKeyProvincia As String = String.Empty

        Dim oFSEntry As New DataEntry.GeneralEntry
        oFSEntry.PM = Me.PM
        oFSEntry.TipoSolicit = mTipoSolicitud
        oFSEntry.MonSolicit = msMonedaSolicitud

        If mlInstancia > 0 Then
            oFSEntry.CampoDef_CampoODesgHijo = oRow.Item("COPIA_CAMPO_DEF")
            oFSEntry.CampoDef_DesgPadre = CopiaCampoDef_Padre
        ElseIf DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.IdsFicticios.DesgloseVinculado Then
            oFSEntry.CampoDef_CampoODesgHijo = -1
        End If
        oFSEntry.ID = "fsentry" + sIdCampoDesglose

        oFSEntry.IdCampoPadre = idCampo

        If idCampoOrigen > 0 Then oFSEntry.IdCampoPadreOrigen = idCampoOrigen

        oFSEntry.TipoGS = iTipoGS
        oFSEntry.Tipo = iTipo
        oFSEntry.Intro = oRow.Item("INTRO")
        Dim pag As FSNPage = Me.Page
        oFSEntry.Title = DBNullToSomething(oRow.Item("DEN_" & sIdi))
        oFSEntry.WindowTitle = Me.Page.Title

        oFSEntry.Tag = sIdCampoDesglose
        oFSEntry.DenGrupo = DBNullToStr(oRow.Item("DEN_GRUPO"))
        oFSEntry.IsGridEditor = False
        oFSEntry.IdContenedor = Me.ClientID
        oFSEntry.TabContainer = msTabContainer
        If oCampo.Vinculado AndAlso oCampo.EsteEsCampoVinculado(sIdCampoDesglose) Then
            oFSEntry.ReadOnly = True
            oFSEntry.CampoOrigenVinculado = oCampo.CampoVinculadoConQuien(sIdCampoDesglose)
        ElseIf mlInstancia > 0 AndAlso oCampo.Vinculado AndAlso oCampo.EsteEsCampoVinculado(oDS.Tables(0).Select("ID=" & sIdCampoDesglose)(0).Item("COPIA_CAMPO_DEF")) Then
            oFSEntry.ReadOnly = True
            oFSEntry.CampoOrigenVinculado = oCampo.CampoVinculadoConQuien(oDS.Tables(0).Select("ID=" & sIdCampoDesglose)(0).Item("COPIA_CAMPO_DEF"))
        Else
            oFSEntry.ReadOnly = (oRow.Item("ESCRITURA") = 0) Or mbFuerzaLectura
            oFSEntry.CampoOrigenVinculado = Nothing
        End If
        oFSEntry.RecordarValores = (oRow.Item("SAVE_VALUES") = 1)
        oFSEntry.Formato = DBNullToSomething(oRow.Item("FORMATO"))
        oFSEntry.ErrMsgFormato = mipage.Textos(27)

        If oRow.Item("ESCRITURA") And bPopUp Then
            bHayCamposModificables = True
        End If

        oFSEntry.Obligatorio = (DBNullToSomething(oRow.Item("OBLIGATORIO")) = 1)
        oFSEntry.BaseDesglose = True
        oFSEntry.Calculado = (oRow.Item("TIPO") = 3 Or oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoNumerico) And Not IsDBNull(oRow.Item("ID_CALCULO"))

        If (oRow.Item("TIPO") = 3 Or oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoNumerico) And Not IsDBNull(oRow.Item("ID_CALCULO")) And (oRow.Item("ESCRITURA")) And bPopUp Then
            bHayCamposModificablesNumerico = True
        End If

        oFSEntry.Desglose = True
        oFSEntry.NumberFormat = oUser.NumberFormat
        oFSEntry.DateFormat = oUser.DateFormat
        oFSEntry.Idi = sIdi
        oFSEntry.IdAtrib = DBNullToSomething(oRow.Item("ID_ATRIB_GS"))
        oFSEntry.ValErp = DBNullToSomething(oRow.Item("VALIDACION_ERP"))
        oFSEntry.Campo_Origen = DBNullToDbl(oRow.Item("CAMPO_ORIGEN"))
        oFSEntry.UsarOrgCompras = CType(Me.Page, FSNPage).Acceso.gbUsar_OrgCompras
        oFSEntry.ActivoSM = CType(Me.Page, FSNPage).Acceso.gbAccesoFSSM

        Select Case oFSEntry.Tipo
            Case TiposDeDatos.TipoGeneral.TipoNumerico
                oFSEntry.Maximo = DBNullToSomething(oRow.Item("MAXNUM"))
                oFSEntry.Minimo = DBNullToSomething(oRow.Item("MINNUM"))
            Case TiposDeDatos.TipoGeneral.TipoFecha
                oFSEntry.Maximo = DBNullToSomething(oRow.Item("MAXFEC"))
                oFSEntry.Minimo = DBNullToSomething(oRow.Item("MINFEC"))
            Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoTextoLargo,
                TiposDeDatos.TipoGeneral.TipoBoolean, TiposDeDatos.TipoGeneral.TipoArchivo
        End Select
        If oFSEntry.Intro = 1 Then
            If oDS.Tables.Count = 1 Then
                oFSEntry.Intro = 0
            Else
                oFSEntry.Lista = New DataSet
                oFSEntry.Lista.Merge(oRow.GetChildRows("REL_CAMPO_LISTA"))
            End If
        End If
        Select Case oFSEntry.Tipo
            Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoMedio
                TratarTipoGeneralTipoTexto(oFSEntry, oRow)
            Case TiposDeDatos.TipoGeneral.TipoNumerico
                TratarTipoGeneralTipoNumerico(oFSEntry)
            Case TiposDeDatos.TipoGeneral.TipoFecha
                TratarTipoGeneralTipoFecha(oFSEntry, oRow)
            Case TiposDeDatos.TipoGeneral.TipoTextoCorto
                TratarTipoGeneralTipoTextoCorto(oFSEntry, oRow)
            Case TiposDeDatos.TipoGeneral.TipoTextoLargo
                TratarTipoGeneralTipoTextoLargo(oFSEntry, oRow)
            Case TiposDeDatos.TipoGeneral.TipoArchivo
                TratarTipoGeneralTipoTextoArchivo(oFSEntry)
            Case TiposDeDatos.TipoGeneral.TipoBoolean
                TratarTipoGeneralTipoBoolean(oFSEntry)
            Case TiposDeDatos.TipoGeneral.TipoEditor
                TratarTipoGeneralTipoEditor(oFSEntry)
        End Select

        Select Case oFSEntry.TipoGS
            Case TiposDeDatos.IdsFicticios.NumLinea
                oFSEntry.Width = Unit.Pixel(60)
            Case TiposDeDatos.TipoCampoGS.Material
                TratarTipoCampoGSMaterial(oFSEntry, oRow, oDS, sRestrictMat, arrMat, sKeyMaterial)
            Case TiposDeDatos.TipoCampoGS.ProveedorERP
                TratarTipoCampoGSProveedorERP(oFSEntry, oRow, oDS)
            Case TiposDeDatos.TipoCampoGS.NuevoCodArticulo, TiposDeDatos.TipoCampoGS.CodArticulo, TiposDeDatos.TipoCampoGS.DenArticulo
                TratarTipoCampoGSArticulo(oFSEntry, oRow, oDS, sRestrictMat, sKeyMaterial, sInstanciaMoneda, sIDCentro, sIdCampoProveedor)
            Case TiposDeDatos.TipoCampoGS.Cantidad
                TratarTipoCampoGSCantidad(oFSEntry, sKeyTipoRecepcion)
            Case TiposDeDatos.TipoCampoGS.Dest
                TratarTipoCampoGSDestino(oFSEntry, oRow, oDS, sKeyAlmacen, sKeyDestino)
            Case TiposDeDatos.TipoCampoGS.Unidad
                TratarTipoCampoGSUnidad(oFSEntry, oRow, oDS)
            Case TiposDeDatos.TipoCampoGS.UnidadPedido
                TratarTipoCampoGSUnidadPedido(oFSEntry)
            Case TiposDeDatos.TipoCampoGS.FormaPago
                TratarTipoCampoGSFormaPago(oFSEntry)
            Case TiposDeDatos.TipoCampoGS.Moneda
                TratarTipoCampoGSMoneda(oFSEntry)
            Case TiposDeDatos.TipoCampoGS.Pais
                TratarTipoCampoGSPais(oFSEntry, oRow, oDS, sKeyPais, sKeyProvincia)
            Case TiposDeDatos.TipoCampoGS.Provincia
                TratarTipoCampoGSProvincia(oFSEntry, sKeyPais, sCodPaisDefecto)
            Case TiposDeDatos.TipoCampoGS.PRES1, TiposDeDatos.TipoCampoGS.Pres2, TiposDeDatos.TipoCampoGS.Pres3, TiposDeDatos.TipoCampoGS.Pres4
                TratarTipoCampoGSPres(oFSEntry, oRow, oDS, iNivel, sKeyOrgCompras)
            Case TiposDeDatos.TipoCampoGS.Subtipo
            Case TiposDeDatos.TipoCampoGS.ImporteRepercutido
                TratarTipoCampoGSImporteRepercutido(oFSEntry, oRow)
            Case TiposDeDatos.TipoCampoGS.Proveedor
                TratarTipoCampoGSProveedor(oFSEntry, oRow, oDS)
            Case TiposDeDatos.TipoCampoGS.EstadoNoConf
                TratarTipoCampoGSEstadoNoConf(oFSEntry)
            Case TiposDeDatos.TipoCampoGS.Fec_inicio, TiposDeDatos.TipoCampoGS.Fec_cierre
                oFSEntry.ValidationControl = "NoConfFECLIM_" & oFSEntry.IdCampoPadre
            Case TiposDeDatos.TipoCampoGS.EstadoInternoNoConf
                TratarTipoCampoGSEstadoInternoNoConf(oFSEntry)
            Case TiposDeDatos.TipoCampoGS.UnidadOrganizativa
                TratarTipoCampoGSUnidadOrganizativa(oFSEntry, oRow, oDS, sOrden, sKeyUnidadOrganizativa)
            Case TiposDeDatos.TipoCampoGS.Departamento
                TratarTipoCampoGSDepartamento(oFSEntry, oRow, oDS, sOrden, sCodUnidadOrgDefecto, sKeyUnidadOrganizativa)
            Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
                TratarTipoCampoGSOrganizacionCompras(oFSEntry, oRow, oDS, sOrden, sKeyOrgComprasDataCheck)
            Case TiposDeDatos.TipoCampoGS.Centro
                TratarTipoCampoGSCentro(oFSEntry, oRow, oDS, sOrden, sCodOrgCompraDefecto, sKeyOrgComprasDataCheck, sIDCentro, sIDCampoOrgCompras, bCentroRelacionadoFORM, sCodOrgComprasFORM, sidDataEntryFORMOrgCompras, sKeyAlmacen, sKeyCentro)
            Case TiposDeDatos.TipoCampoGS.Almacen
                TratarTipoCampoGSAlmacen(oFSEntry, oRow, sCodCentroDefecto, sKeyCentro, sKeyDestino, sOrden)
            Case TiposDeDatos.TipoCampoGS.Empresa
                TratarTipoCampoGSEmpresa(oFSEntry)
            Case TiposDeDatos.TipoCampoGS.IniSuministro
                TratarTipoCampoGSIniSuministro(oFSEntry, oRow, oDS)
            Case TiposDeDatos.TipoCampoGS.FinSuministro
                TratarTipoCampoGSFinSuministro(oFSEntry, oRow, oDS)
            Case TiposDeDatos.TipoCampoGS.PrecioUnitario
                TratarTipoCampoGSPrecioUnitario(oFSEntry, oRow)
            Case TiposDeDatos.TipoCampoGS.CentroCoste
                TratarTipoCampoGSCentroCoste(oFSEntry, oRow)
            Case TiposDeDatos.TipoCampoGS.Partida
                TratarTipoCampoGSPartida(oFSEntry, oRow)
            Case TiposDeDatos.TipoCampoGS.Activo
                TratarTipoCampoGSActivo(oFSEntry, oRow)
            Case TiposDeDatos.TipoCampoGS.AnyoPartida
                TratarTipoCampoGSAnyoPartida(oFSEntry, oDS, oRow)
        End Select

        '----------------------------------------------------------------------------------------------------------------------------------------------------------------
        If oRow.Item("TIPO") = 6 Then
            Dim bSinKeyArticulo As Boolean = True
            oFSEntry.Tabla_Externa = oRow.Item("TABLA_EXTERNA")
            oFSEntry.Width = Unit.Pixel(250)
            If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                oFSEntry.Text = oRow.Item("VALOR_TEXT_DEN")
                If Not IsDBNull(oRow.Item("VALOR_TEXT_COD")) Then
                    oFSEntry.codigoArticulo = oRow.Item("VALOR_TEXT_COD")
                    bSinKeyArticulo = False
                End If
            Else
                Dim oTablaExterna As FSNServer.TablaExterna = FSNServer.Get_Object(GetType(FSNServer.TablaExterna))
                oTablaExterna.LoadDefTabla(oFSEntry.Tabla_Externa)
                If Not oFSEntry.Valor Is Nothing Then
                    Dim oRowReg As DataRow = oTablaExterna.BuscarRegistro(oFSEntry.Valor.ToString(), , True)
                    oFSEntry.Text = oTablaExterna.DescripcionReg(oRowReg, sIdi, oUser.DateFormat, oUser.NumberFormat)
                    oFSEntry.codigoArticulo = oTablaExterna.CodigoArticuloReg(oRowReg)
                End If
                bSinKeyArticulo = oTablaExterna.ArtKey Is Nothing
            End If
            oFSEntry.Denominacion = oFSEntry.Text
            If sKeyArticulo Is Nothing Then
                For Each oRowaux As DataRow In oDS.Tables(0).Rows
                    If DBNullToSomething(oRowaux.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
                        If Not oDS.Tables(0).Columns("ID_CAMPO") Is Nothing Then
                            sKeyArticulo = "fsentry" + oRowaux.Item("ID_CAMPO").ToString
                        Else
                            sKeyArticulo = "fsentry" + oRowaux.Item("ID").ToString
                        End If
                        Exit For
                    End If
                Next
            End If
            If Not (sKeyArticulo Is Nothing Or bSinKeyArticulo) Then
                oFSEntry.DependentField = sKeyArticulo
            End If
        ElseIf oRow.Item("TIPO") = 7 Then
            oFSEntry.Servicio = oRow.Item("SERVICIO")
        End If
        If oFSEntry.Intro = 1 AndAlso Not (oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.Dest) AndAlso Not (oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.CodArticulo) _
                              AndAlso Not (oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.DenArticulo) AndAlso Not (oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.NuevoCodArticulo) Then
            'destino 250px
            'CodArticulo    DenArticulo     400px
            'resto combos
            Select Case oFSEntry.Tipo
                Case TiposDeDatos.TipoGeneral.TipoNumerico, TiposDeDatos.TipoGeneral.TipoFecha
                    oFSEntry.Width = Unit.Pixel(120)
                Case TiposDeDatos.TipoGeneral.TipoBoolean
                    oFSEntry.Width = Unit.Pixel(50)
                Case Else
                    Select Case oFSEntry.TipoGS
                        Case TiposDeDatos.TipoCampoGS.FormaPago
                            oFSEntry.Width = Unit.Pixel(200)
                        Case Else
                            oFSEntry.Width = Unit.Pixel(300)
                    End Select
            End Select
        End If
        If oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.SinTipo And oFSEntry.Intro = 2 Then
            'Atributos de lista externa
            If idDataEntryFORMOrgCompras <> "" Then ' Tiene un campo Organizacion de compras antes que el desglose!!!
                oFSEntry.idDataEntryDependent = idDataEntryFORMOrgCompras
            Else
                Dim sIDOrgCompras As String = String.Empty
                'Org compras
                If CType(Me.Page, FSNPage).Acceso.gbUsar_OrgCompras Then
                    If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                        If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                sIDOrgCompras = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                            Else
                                sIDOrgCompras = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                            End If
                        End If
                    End If
                End If

                If sIDOrgCompras <> "" Then
                    oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry_" & sIDOrgCompras
                Else
                    'Centro SM                                
                    Dim sIdCentroCoste As String = ""
                    If CType(Me.Page, FSNPage).Acceso.gbAccesoFSSM Then
                        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                                If Not oDS.Tables(0).Columns("ID_CAMPO") Is Nothing Then
                                    sIdCentroCoste = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                Else
                                    sIdCentroCoste = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                                End If
                            End If
                        End If
                        If sIdCentroCoste <> "" Then oFSEntry.idDataEntryDependent2 = oFSEntry.IdContenedor & "_fsentry_" & sIdCentroCoste
                    End If

                    'UON
                    If sIdCentroCoste = "" Then
                        Dim sIdUON As String = String.Empty
                        If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO")).Length = 0 Then
                            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                                If Not oDS.Tables(0).Columns("ID_CAMPO") Is Nothing Then
                                    sIdUON = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                Else
                                    sIdUON = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                                End If
                            End If
                        End If
                        If sIdUON <> "" Then oFSEntry.idDataEntryDependent3 = oFSEntry.IdContenedor & "_" & "fsentry_" & sIdUON
                    End If
                End If
            End If
        End If
        If oFSEntry.ReadOnly Then
            oFSEntry.LabelStyle.CssClass = "CampoSoloLectura"
            oFSEntry.InputStyle.CssClass = "trasparent"
            If (oFSEntry.TipoGS = TiposDeDatos.TipoGeneral.TipoArchivo) Or (oFSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoArchivo) Then
                oFSEntry.InputStyle.Font.Size = System.Web.UI.WebControls.FontUnit.Parse("10px")
            End If
        End If

        oFSEntry.UsarOrgCompras = CType(Me.Page, FSNPage).Acceso.gbUsar_OrgCompras

        Return oFSEntry
    End Function
#End Region

#Region "Funciones de tratamiento de campos"
    ''' <summary>Tratamiento de un tipo de campo general de tipo texto</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow">Fila con los datos del campo</param>
    Private Sub TratarTipoGeneralTipoTexto(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow)
        oFSEntry.InputStyle.CssClass = "TipoTextoMedio"
        If oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.DenArticulo Then
            oFSEntry.MaxLength = FSNServer.LongitudesDeCodigos.giLongCodDENART
        Else
            If oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.CodArticulo Or oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
                oFSEntry.MaxLength = FSNServer.LongitudesDeCodigos.giLongCodART
            Else

                If TiposDeDatos.TipoGeneral.TipoTextoMedio = TiposDeDatos.TipoGeneral.TipoTextoMedio And Not IsDBNull(oRow.Item("MAX_LENGTH")) Then
                    oFSEntry.MaxLength = oRow.Item("MAX_LENGTH")
                Else
                    oFSEntry.MaxLength = 800
                End If
            End If
        End If
        oFSEntry.ErrMsg = mipage.Textos(12)
    End Sub
    ''' <summary>Tratamiento de un tipo de campo general de tipo numerico</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    Private Sub TratarTipoGeneralTipoNumerico(ByRef oFSEntry As DataEntry.GeneralEntry)
        oFSEntry.InputStyle.CssClass = "TipoNumero"
    End Sub
    ''' <summary>Tratamiento de un tipo de campo general de tipo fecha</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    Private Sub TratarTipoGeneralTipoFecha(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow)
        oFSEntry.InputStyle.CssClass = "TipoFecha"
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "FechaNoAnteriorAlSistema") Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "FechaNoAnteriorAlSistema", "<script>var sFechaNoAnteriorAlSistema = '" + JSText(mipage.Textos(48)) + "';</script>")
        End If
        oFSEntry.FechaNoAnteriorAlSistema = DBNullToBoolean(oRow.Item("FECHA_VALOR_NO_ANT_SIS"))
    End Sub
    ''' <summary>Tratamiento de un tipo de campo general de tipo texto corto</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow">Fila con los datos del campo</param>
    Private Sub TratarTipoGeneralTipoTextoCorto(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow)
        If Not IsDBNull(oRow.Item("MAX_LENGTH")) Then
            oFSEntry.MaxLength = oRow.Item("MAX_LENGTH")
        Else
            oFSEntry.MaxLength = 100
        End If
        oFSEntry.Width = Unit.Pixel(200)
        oFSEntry.InputStyle.CssClass = "TipoTextoCorto"
    End Sub
    ''' <summary>Tratamiento de un tipo de campo general de tipo texto largo</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow">Fila con los datos del campo</param>
    Private Sub TratarTipoGeneralTipoTextoLargo(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow)
        oFSEntry.PopUpStyle = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & theme & ".css"
        oFSEntry.PUCaptionBoton = mipage.Textos(4)
        If Not IsDBNull(oRow.Item("MAX_LENGTH")) Then
            oFSEntry.MaxLength = oRow.Item("MAX_LENGTH")
        End If
        oFSEntry.InputStyle.CssClass = "TipoTextoLargo"
        If oFSEntry.ReadOnly Then
            oFSEntry.Width = Unit.Pixel(375)
        Else
            oFSEntry.Width = Unit.Pixel(250)
        End If
        oFSEntry.ErrMsg = mipage.Textos(12)
    End Sub
    ''' <summary>Tratamiento de un tipo de campo general de tipo archivo</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    Private Sub TratarTipoGeneralTipoTextoArchivo(ByRef oFSEntry As DataEntry.GeneralEntry)
        oFSEntry.InputStyle.CssClass = "TipoArchivo"
    End Sub
    ''' <summary>Tratamiento de un tipo de campo general de tipo boolean</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    Private Sub TratarTipoGeneralTipoBoolean(ByRef oFSEntry As DataEntry.GeneralEntry)
        oFSEntry.Lista = CommonAlta.CrearDataset(sIdi, mipage.Textos(15), mipage.Textos(16))
        oFSEntry.Intro = 1
    End Sub
    ''' <summary>Tratamiento de un tipo de campo general de tipo editor</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    Private Sub TratarTipoGeneralTipoEditor(ByRef oFSEntry As DataEntry.GeneralEntry)
        oFSEntry.Text = mipage.Textos(21)
    End Sub
    ''' <summary>Tratamiento de un tipo de campo GS Material</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow">Fila con los datos del campo</param>
    ''' <param name="oDS">Dataset con todos los campos</param>
    ''' <param name="sRestrictMat"></param>
    ''' <param name="arrMat"></param> 
    ''' <param name="sKeyMaterial"></param>
    ''' <remarks>Llamada desde: Page_Load</remarks>
    Private Sub TratarTipoCampoGSMaterial(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow, ByRef oDS As DataSet, ByVal sRestrictMat As String, ByRef arrMat As String(), ByRef sKeyMaterial As String)
        Dim sValor As String = String.Empty
        Dim sDenominacion As String = String.Empty
        Dim sToolTip As String
        Dim i As Integer
        Dim sMat As String
        oFSEntry.Valor = sRestrictMat
        sMat = sRestrictMat
        Dim lLongCod As Integer

        For i = 1 To 4
            arrMat(i) = ""
        Next i

        i = 1
        While Trim(sMat) <> ""
            Select Case i
                Case 1
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                Case 2
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                Case 3
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                Case 4
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
            End Select
            arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
            sMat = Mid(sMat, lLongCod + 1)
            i = i + 1
        End While

        sToolTip = ""

        If arrMat(4) <> "" Then
            If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
            Else
                Dim oGMN3 As FSNServer.GrupoMatNivel3 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel3))
                oGMN3.GMN1Cod = arrMat(1)
                oGMN3.GMN2Cod = arrMat(2)
                oGMN3.Cod = arrMat(3)
                oGMN3.CargarTodosLosGruposMatDesde(1, sIdi, arrMat(4), , True)
                sDenominacion = oGMN3.GruposMatNivel4.Item(arrMat(4)).Den
                oGMN3 = Nothing
            End If
            sToolTip = sDenominacion & " (" & arrMat(1) & " - " & arrMat(2) & " - " & arrMat(3) & " - " & arrMat(4) & ")"
            If Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles Then
                sValor = arrMat(1) & " - " & arrMat(2) & " - " & arrMat(3) & " - " & arrMat(4) + " - " + sDenominacion
            Else
                sValor = arrMat(4) + " - " + sDenominacion
            End If

        ElseIf arrMat(3) <> "" Then
            If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
            Else
                Dim oGMN2 As FSNServer.GrupoMatNivel2 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel2))
                oGMN2.GMN1Cod = arrMat(1)
                oGMN2.Cod = arrMat(2)
                oGMN2.CargarTodosLosGruposMatDesde(1, sIdi, arrMat(3), , True)
                sDenominacion = oGMN2.GruposMatNivel3.Item(arrMat(3)).Den
                oGMN2 = Nothing
            End If
            sToolTip = sDenominacion & "(" & arrMat(1) & " - " & arrMat(2) & " - " & arrMat(3) & ")"
            If Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles Then
                sValor = arrMat(1) & " - " & arrMat(2) & " - " & arrMat(3) + " - " + sDenominacion
            Else
                sValor = arrMat(3) + " - " + sDenominacion
            End If

        ElseIf arrMat(2) <> "" Then
            If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
            Else
                Dim oGMN1 As FSNServer.GrupoMatNivel1 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel1))
                oGMN1.Cod = arrMat(1)
                oGMN1.CargarTodosLosGruposMatDesde(1, sIdi, arrMat(2), , True)
                sDenominacion = oGMN1.GruposMatNivel2.Item(arrMat(2)).Den
                oGMN1 = Nothing
            End If
            sToolTip = sDenominacion & "(" & arrMat(1) & " - " & arrMat(2) & ")"
            If Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles Then
                sValor = arrMat(1) & " - " & arrMat(2) + " - " + sDenominacion
            Else
                sValor = arrMat(2) + " - " + sDenominacion
            End If

        ElseIf arrMat(1) <> "" Then
            If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
            Else
                Dim oGMN1s As FSNServer.GruposMatNivel1 = FSNServer.Get_Object(GetType(FSNServer.GruposMatNivel1))
                oGMN1s.CargarTodosLosGruposMatDesde(1, sIdi, arrMat(1), , , True)
                sDenominacion = oGMN1s.Item(arrMat(1)).Den
                oGMN1s = Nothing
            End If
            sToolTip = sDenominacion & "(" & arrMat(1) & ")"
            sValor = arrMat(1) + " - " + sDenominacion
        End If
        oFSEntry.ToolTip = sToolTip
        oFSEntry.Text = sValor
        oFSEntry.Denominacion = sDenominacion
        oFSEntry.InputStyle.CssClass = "TipoTexto"
        oFSEntry.Width = Unit.Pixel(200)
        sKeyMaterial = oFSEntry.ClientID
        oFSEntry.Restric = sRestrictMat
        Dim sIDProveedor As String = String.Empty
        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1").Length > 0 Then
                sIDProveedor = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1")(0).Item("ID_CAMPO")
            End If
        Else
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1").Length > 0 Then
                sIDProveedor = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1")(0).Item("ID")
            End If
        End If
        oFSEntry.idDataEntryDependent2 = oFSEntry.IdContenedor & "_" & "fsentry_" & sIDProveedor
        'Relacion con familia de materiales
        If Me.idDataEntryFORMMaterial <> "" Then
            oFSEntry.idDataEntryFORMMaterial = Me.idDataEntryFORMMaterial
        End If
    End Sub
    ''' <summary>Tratamiento de un tipo de campo GS ProveedorERP</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow">Fila con los datos del campo</param>
    ''' <param name="oDS">Dataset con todos los campos</param>
    ''' <remarks>Llamada desde: Page_Load</remarks>
    Private Sub TratarTipoCampoGSProveedorERP(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow, ByRef oDS As DataSet)
        'El campo Gs proveedor ERP dependera de 2 campos, el campo proveedor y el campo organizacion de compras para mostrar sus valores
        Dim oProvesERP As FSNServer.CProveERPs
        oProvesERP = FSNServer.Get_Object(GetType(FSNServer.CProveERPs))
        oFSEntry.Lista = oProvesERP.CargarProveedoresERPtoDS()

        Dim sIDOrgCompras As String = String.Empty
        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                sIDOrgCompras = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
            End If
        Else
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                sIDOrgCompras = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
            End If
        End If
        If sIDOrgCompras = "" AndAlso idDataEntryFORMOrgCompras <> "" Then ' Tiene un campo Organizacion de compras antes que el desglose a nivel de formulario, viene de campos.ascx Then
            oFSEntry.idDataEntryDependent = idDataEntryFORMOrgCompras
        ElseIf sIDOrgCompras <> "" Then 'El campo organizacion de compras estara en el propio desglose
            oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry_" & sIDOrgCompras
        End If

        'Si el campo proveedor ERP esta a nivel de desglose el campo Proveedor tiene que estar tambien a nivel de desglose
        Dim sIDProveedorDeProveERP As String = String.Empty
        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                sIDProveedorDeProveERP = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
            End If
        Else
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                sIDProveedorDeProveERP = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
            End If
        End If
        If sIDProveedorDeProveERP <> "" Then
            oFSEntry.idEntryPROV = oFSEntry.IdContenedor & "_" & "fsentry_" & sIDProveedorDeProveERP
        End If
        oFSEntry.Intro = 1
    End Sub
    ''' <summary>Tratamiento de campos de tipo GS importe repercutido</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow">Fila con todos los datos del campo</param>
    Private Sub TratarTipoCampoGSImporteRepercutido(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow)
        oFSEntry.Texto_Defecto = DBNullToSomething(oRow.Item("VALOR_NUM"))
        oFSEntry.MonCentral = oRow.Item("MONCEN")
        oFSEntry.CambioCentral = oRow.Item("CAMBIOCEN")
        oFSEntry.MonRepercutido = oFSEntry.MonCentral
        oFSEntry.CambioRepercutido = oFSEntry.CambioCentral
    End Sub
    ''' <summary>Tratamiento de campos de tipo GS Pres1, Pres2, Pres3 y Pres4</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow">Fila con todos los datos del campo</param>
    ''' <param name="oDS">Dataset con todos los campos</param>
    ''' <param name="iNivel"></param>
    ''' <param name="sKeyOrgCompras"></param>
    ''' <remarks>Llamada desde: Page_Load</remarks>
    Private Sub TratarTipoCampoGSPres(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow, ByRef oDS As DataSet, ByRef iNivel As Integer, ByRef sKeyOrgCompras As String)
        Dim sValor As String
        Dim sTexto As String
        Dim sDenominacion As String = String.Empty
        Dim iContadorPres As Integer
        Dim lIdPresup As Integer
        Dim arrPresupuestos() As String
        Dim arrPresup(2) As String
        Dim dPorcent As Decimal
        Dim oPres1 As FSNServer.PresProyectosNivel1
        Dim oPres2 As FSNServer.PresContablesNivel1
        Dim oPres3 As FSNServer.PresConceptos3Nivel1
        Dim oPres4 As FSNServer.PresConceptos4Nivel1
        Dim oDSPres As DataSet = Nothing
        Dim iAnyo As Integer

        oFSEntry.Width = Unit.Pixel(200)

        If oFSEntry.Valor = Nothing Then
            Select Case oFSEntry.TipoGS
                Case TiposDeDatos.TipoCampoGS.PRES1
                    If oUser.Pres1 <> Nothing Then
                        iNivel = Left(CStr(oUser.Pres1), 1)
                        lIdPresup = Mid(CStr(oUser.Pres1), 2)
                        oFSEntry.Valor = iNivel.ToString + "_" + lIdPresup.ToString + "_1"
                    End If
                Case TiposDeDatos.TipoCampoGS.Pres2
                    If oUser.Pres2 <> Nothing Then
                        iNivel = Left(CStr(oUser.Pres2), 1)
                        lIdPresup = Mid(CStr(oUser.Pres2), 2)
                        oFSEntry.Valor = iNivel.ToString + "_" + lIdPresup.ToString + "_1"
                    End If
                Case TiposDeDatos.TipoCampoGS.Pres3
                    If oUser.Pres3 <> Nothing Then
                        iNivel = Left(CStr(oUser.Pres3), 1)
                        lIdPresup = Mid(CStr(oUser.Pres3), 2)
                        oFSEntry.Valor = iNivel.ToString + "_" + lIdPresup.ToString + "_1"
                    End If
                Case TiposDeDatos.TipoCampoGS.Pres4
                    If oUser.Pres4 <> Nothing Then
                        iNivel = Left(CStr(oUser.Pres4), 1)
                        lIdPresup = Mid(CStr(oUser.Pres4), 2)
                        oFSEntry.Valor = iNivel.ToString + "_" + lIdPresup.ToString + "_1"
                    End If
            End Select

            If oFSEntry.Valor <> Nothing Then
                oFSEntry.Posee_Defecto = True
                oFSEntry.Valor_Defecto = oFSEntry.Valor
            End If
        End If

        If oFSEntry.Valor <> Nothing Then
            arrPresupuestos = oFSEntry.Valor.split("#")
            Dim arrDenominaciones() As String = Nothing
            If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") And Not DBNullToSomething(oRow.Item("VALOR_TEXT")) = Nothing Then
                sDenominacion = oRow.Item("VALOR_TEXT_DEN")
            End If
            sValor = ""
            sTexto = ""
            iContadorPres = 0
            For Each oPresup In arrPresupuestos
                arrPresup = oPresup.Split("_")
                iNivel = arrPresup(0)
                lIdPresup = arrPresup(1)
                dPorcent = Numero(arrPresup(2), oUser.DecimalFmt, oUser.ThousanFmt)
                iContadorPres = iContadorPres + 1

                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") And Not DBNullToSomething(oRow.Item("VALOR_TEXT")) = Nothing Then
                    sTexto = arrDenominaciones(iContadorPres - 1) & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); " & sTexto
                    sValor += oPresup + "#"
                Else
                    Select Case oFSEntry.TipoGS
                        Case TiposDeDatos.TipoCampoGS.PRES1
                            oPres1 = FSNServer.Get_Object(GetType(FSNServer.PresProyectosNivel1))
                            Select Case iNivel
                                Case 1
                                    oPres1.LoadData(lIdPresup)
                                Case 2
                                    oPres1.LoadData(Nothing, lIdPresup)
                                Case 3
                                    oPres1.LoadData(Nothing, Nothing, lIdPresup)
                                Case 4
                                    oPres1.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                            End Select
                            oDSPres = oPres1.Data
                            oPres1 = Nothing
                        Case TiposDeDatos.TipoCampoGS.Pres2
                            oPres2 = FSNServer.Get_Object(GetType(FSNServer.PresContablesNivel1))
                            Select Case iNivel
                                Case 1
                                    oPres2.LoadData(lIdPresup)
                                Case 2
                                    oPres2.LoadData(Nothing, lIdPresup)
                                Case 3
                                    oPres2.LoadData(Nothing, Nothing, lIdPresup)
                                Case 4
                                    oPres2.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                            End Select
                            oDSPres = oPres2.Data
                            oPres2 = Nothing
                        Case TiposDeDatos.TipoCampoGS.Pres3
                            oPres3 = FSNServer.Get_Object(GetType(FSNServer.PresConceptos3Nivel1))
                            Select Case iNivel
                                Case 1
                                    oPres3.LoadData(lIdPresup)
                                Case 2
                                    oPres3.LoadData(Nothing, lIdPresup)
                                Case 3
                                    oPres3.LoadData(Nothing, Nothing, lIdPresup)
                                Case 4
                                    oPres3.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                            End Select
                            oDSPres = oPres3.Data
                            oPres3 = Nothing
                        Case TiposDeDatos.TipoCampoGS.Pres4
                            oPres4 = FSNServer.Get_Object(GetType(FSNServer.PresConceptos4Nivel1))
                            Select Case iNivel
                                Case 1
                                    oPres4.LoadData(lIdPresup)
                                Case 2
                                    oPres4.LoadData(Nothing, lIdPresup)
                                Case 3
                                    oPres4.LoadData(Nothing, Nothing, lIdPresup)
                                Case 4
                                    oPres4.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                            End Select
                            oDSPres = oPres4.Data
                            oPres4 = Nothing
                    End Select

                    If Not oDSPres Is Nothing Then
                        If oDSPres.Tables(0).Rows.Count > 0 Then
                            With oDSPres.Tables(0).Rows(0)
                                'comprobamos que el valor por defecto cumpla la restricci�n del usuario. SI NO CUMPLE, NO APARECER�
                                If Not oDSPres.Tables(0).Columns("ANYO") Is Nothing Then
                                    iAnyo = .Item("ANYO")
                                End If

                                If Not oDSPres.Tables(0).Columns("ANYO") Is Nothing Then
                                    sTexto += .Item("ANYO").ToString + " - "
                                End If

                                If Not IsDBNull(.Item("PRES4")) Then
                                    sTexto += .Item("PRES4").ToString & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
                                ElseIf Not IsDBNull(.Item("PRES3")) Then
                                    sTexto += .Item("PRES3").ToString & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
                                ElseIf Not IsDBNull(.Item("PRES2")) Then
                                    sTexto += .Item("PRES2").ToString & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
                                ElseIf Not IsDBNull(.Item("PRES1")) Then
                                    sTexto += .Item("PRES1").ToString & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
                                End If

                                sValor += oPresup + "#"

                            End With
                        End If
                    End If
                End If
            Next
            If sValor <> Nothing Then
                sValor = Left(sValor, sValor.Length - 1)
            End If
            oFSEntry.Valor = sValor
            oFSEntry.Denominacion = sDenominacion
            If sTexto = "" Then
                If Not oFSEntry.ReadOnly Then
                    oFSEntry.Text = mipage.Textos(1)
                    oFSEntry.Texto_Defecto = oFSEntry.Text
                End If
            Else
                oFSEntry.Text = sTexto
                If oFSEntry.Posee_Defecto Then
                    oFSEntry.Texto_Defecto = sTexto
                End If
            End If
        Else
            If Not oFSEntry.ReadOnly Then
                oFSEntry.Text = mipage.Textos(1)
                oFSEntry.Texto_Defecto = oFSEntry.Text
            End If
        End If

        If sKeyOrgCompras Is Nothing Then
            For Each oRowaux As DataRow In oDS.Tables(0).Rows
                If DBNullToSomething(oRowaux.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.OrganizacionCompras Then
                    sKeyOrgCompras = "fsentry" + oRowaux.Item("ID").ToString
                    Exit For
                End If
            Next
        End If
        If Not (sKeyOrgCompras Is Nothing) Then
            oFSEntry.DependentField = sKeyOrgCompras
        End If
    End Sub
    ''' <summary>Tratamiento de un tipo de campo GS NuevoCodArticulo, CodArticulo o DenArticulo</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow">Fila con los datos del campo</param>
    ''' <param name="oDS">Dataset con todos los campos</param>
    ''' <param name="sRestrictMat"></param>
    ''' <param name="sKeyMaterial"></param>
    ''' <param name="sInstanciaMoneda"></param>
    ''' <param name="sIDCentro"></param>
    ''' <param name="sIdCampoProveedor"></param>
    ''' <remarks>Llamada desde: Page_Load</remarks>
    Private Sub TratarTipoCampoGSArticulo(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow, ByRef oDS As DataSet, ByVal sRestrictMat As String, ByVal sKeyMaterial As String, ByVal sInstanciaMoneda As String,
                                          ByRef sIDCentro As String, ByRef sIdCampoProveedor As String)
        Dim sKeyProveedor As String = String.Empty

        If oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
            oFSEntry.AnyadirArt = oRow.Item("ANYADIR_ART")
            oFSEntry.Width = Unit.Pixel(200)
        Else
            oFSEntry.Width = Unit.Pixel(400)
        End If
        oFSEntry.Restric = sRestrictMat
        oFSEntry.DependentField = sKeyMaterial
        oFSEntry.Intro = 1
        oFSEntry.InstanciaMoneda = sInstanciaMoneda

        If idDataEntryFORMOrgCompras <> "" Then ' Tiene un campo Organizacion de compras antes que el desglose!!!
            oFSEntry.idDataEntryDependent = idDataEntryFORMOrgCompras
        Else
            Dim sIDOrgCompras As String = String.Empty
            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                    sIDOrgCompras = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                End If
            Else
                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                    sIDOrgCompras = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                End If
            End If
            If sIDOrgCompras <> "" Then
                oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry_" & sIDOrgCompras
            End If
        End If

        If idDataEntryFORMCentro <> "" Then ' Tiene un campo Centro antes que el desglose!!!
            oFSEntry.idDataEntryDependent2 = idDataEntryFORMCentro
        Else
            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Centro & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                    sIDCentro = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Centro & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                End If
            Else
                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Centro & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                    sIDCentro = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Centro & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                End If
            End If
            If sIDCentro <> "" Then oFSEntry.idDataEntryDependent2 = oFSEntry.IdContenedor & "_" & "fsentry_" & sIDCentro
        End If

        Dim sIdUnidadOrganizativa As String = String.Empty
        If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO")).Length = 0 Then
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                If Not oDS.Tables(0).Columns("ID_CAMPO") Is Nothing Then
                    sIdUnidadOrganizativa = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                Else
                    sIdUnidadOrganizativa = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                End If
            End If
        End If
        If sIdUnidadOrganizativa = "" AndAlso Not idDataEntryFORMUnidadOrganizativa = "" Then ' Tiene un campo Organizacion de compras antes que el desglose a nivel de formulario, viene de campos.ascx Then
            oFSEntry.idDataEntryDependent3 = idDataEntryFORMUnidadOrganizativa
        ElseIf Not sIdUnidadOrganizativa = "" Then 'El campo organizacion de compras estara en el propio desglose
            oFSEntry.idDataEntryDependent3 = oFSEntry.IdContenedor & "_" & "fsentry_" & sIdUnidadOrganizativa
        End If

        Dim sCodCentroCoste As String = String.Empty
        If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO")).Length = 0 And
            oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa & " AND GRUPO = " & oRow.Item("GRUPO")).Length = 0 Then
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                If Not oDS.Tables(0).Columns("ID_CAMPO") Is Nothing Then
                    sCodCentroCoste = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                Else
                    sCodCentroCoste = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                End If
            End If
        End If
        If sCodCentroCoste = String.Empty AndAlso Not idDataEntryFORMCentroCoste = String.Empty Then ' Tiene un campo Organizacion de compras antes que el desglose a nivel de formulario, viene de campos.ascx Then
            oFSEntry.idDataEntryDependentCentroCoste = idDataEntryFORMCentroCoste
        ElseIf Not sCodCentroCoste = String.Empty Then 'El campo organizacion de compras estara en el propio desglose
            oFSEntry.idDataEntryDependentCentroCoste = oFSEntry.IdContenedor & "_" & "fsentry_" & sCodCentroCoste
        End If

        Dim sKeyCantidad As String = String.Empty
        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                sKeyCantidad = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
            End If
        Else
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                sKeyCantidad = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
            End If
        End If
        If sKeyCantidad <> "" Then oFSEntry.idEntryCANT = oFSEntry.IdContenedor & "_" & "fsentry_" & sKeyCantidad

        Dim sKeyUnidad As String = String.Empty
        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                sKeyUnidad = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
            End If
        Else
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                sKeyUnidad = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
            End If
        End If
        If sKeyUnidad <> "" Then oFSEntry.idEntryUNI = oFSEntry.IdContenedor & "_" & "fsentry_" & sKeyUnidad

        Dim sKeyUnidadPedido As String = String.Empty
        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                sKeyUnidadPedido = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
            End If
        Else
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                sKeyUnidadPedido = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
            End If
        End If
        If sKeyUnidadPedido <> "" Then oFSEntry.idEntryUNIPedido = oFSEntry.IdContenedor & "_" & "fsentry_" & sKeyUnidadPedido

        If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oRow.Item("GRUPO")).Length Then
            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                oFSEntry.IdMaterialOculta = oFSEntry.IdContenedor & "_" & "fsentry_" & DBNullToSomething(oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
            Else
                oFSEntry.IdMaterialOculta = oFSEntry.IdContenedor & "_" & "fsentry_" & DBNullToSomething(oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID"))
            End If
        End If
        If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oRow.Item("GRUPO")).Length Then
            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                oFSEntry.IdCodArticuloOculta = oFSEntry.IdContenedor & "_" & "fsentry_" & DBNullToSomething(oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
            Else
                oFSEntry.IdCodArticuloOculta = oFSEntry.IdContenedor & "_" & "fsentry_" & DBNullToSomething(oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID"))
            End If
        ElseIf oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oRow.Item("GRUPO")).Length Then
            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                oFSEntry.IdCodArticuloOculta = oFSEntry.IdContenedor & "_" & "fsentry_" & DBNullToSomething(oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
            Else
                oFSEntry.IdCodArticuloOculta = oFSEntry.IdContenedor & "_" & "fsentry_" & DBNullToSomething(oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID"))
            End If
        End If
        If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oRow.Item("GRUPO")).Length Then
            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                oFSEntry.IdDenArticuloOculta = oFSEntry.IdContenedor & "_" & "fsentry_" & DBNullToSomething(oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
            Else
                oFSEntry.IdDenArticuloOculta = oFSEntry.IdContenedor & "_" & "fsentry_" & DBNullToSomething(oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID"))
            End If
        End If

        'Relaci�n Con el proveedor
        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_ULT_ADJ=1").Length > 0 Then
                sKeyProveedor = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_ULT_ADJ=1")(0).Item("ID_CAMPO")
            End If
        Else
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_ULT_ADJ=1").Length > 0 Then
                sKeyProveedor = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_ULT_ADJ=1")(0).Item("ID")
            End If
        End If
        If sKeyProveedor <> "" Then
            oFSEntry.idEntryPROV = oFSEntry.IdContenedor & "_" & "fsentry_" & sKeyProveedor
        End If

        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_REL_PROVE_ART4=1").Length > 0 Then
                sIdCampoProveedor = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO") & " AND CARGAR_REL_PROVE_ART4=1")(0).Item("ID_CAMPO")
            End If
        Else
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_REL_PROVE_ART4=1").Length > 0 Then
                sIdCampoProveedor = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO") & " AND CARGAR_REL_PROVE_ART4=1")(0).Item("ID")
            End If
        End If
        If sIdCampoProveedor <> "" Then
            oFSEntry.idEntryProveSumiArt = oFSEntry.IdContenedor & "_" & "fsentry_" & sIdCampoProveedor
        End If

        'Relaci�n Con el Precio
        Dim sKeyPrecio As String = String.Empty
        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.PrecioUnitario & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_ULT_ADJ=1").Length > 0 Then
                sKeyPrecio = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.PrecioUnitario & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_ULT_ADJ=1")(0).Item("ID_CAMPO")
            End If
        Else
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.PrecioUnitario & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_ULT_ADJ=1").Length > 0 Then
                sKeyPrecio = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.PrecioUnitario & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_ULT_ADJ=1")(0).Item("ID")
            End If
        End If
        If sKeyPrecio <> "" Then oFSEntry.idEntryPREC = oFSEntry.IdContenedor & "_" & "fsentry_" & sKeyPrecio

        'Relacion con la Cuenta Contable
        Dim sKeyCC As String = String.Empty
        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select("ID_ATRIB_GS = " & TiposDeDatos.TipoAtributo.CuentaContable & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1").Length > 0 Then
                sKeyCC = oDS.Tables(0).Select("ID_ATRIB_GS = " & TiposDeDatos.TipoAtributo.CuentaContable & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1")(0).Item("ID_CAMPO")
            End If
        Else
            If oDS.Tables(0).Select("ID_ATRIB_GS = " & TiposDeDatos.TipoAtributo.CuentaContable & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1").Length > 0 Then
                sKeyCC = oDS.Tables(0).Select("ID_ATRIB_GS = " & TiposDeDatos.TipoAtributo.CuentaContable & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1")(0).Item("ID")
            End If
        End If
        If sKeyCC <> "" Then oFSEntry.idEntryCC = oFSEntry.IdContenedor & "_" & "fsentry_" & sKeyCC

        'Relacion con familia de materiales
        If Me.idDataEntryFORMMaterial <> "" Then
            oFSEntry.idDataEntryFORMMaterial = Me.idDataEntryFORMMaterial
        End If
    End Sub
    ''' <summary>Tratamiento de un campo de tipo cantidad</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="sKeyTipoRecepcion"></param>
    Private Sub TratarTipoCampoGSCantidad(ByRef oFSEntry As DataEntry.GeneralEntry, ByVal sKeyTipoRecepcion As String)
        oFSEntry.ErrMsg = JSText(mipage.Textos(18))
        If sKeyTipoRecepcion = "1" Then oFSEntry.Text = 1
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeRecep") Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeRecep", "<script>var smensajeRecep = '" & JSText(mipage.Textos(22)) & "'</script>")
        End If
    End Sub
    ''' <summary>Tratamiento de un campo de tipo destino</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow"></param>
    ''' <param name="oDS"></param>
    ''' <param name="sKeyAlmacen"></param>
    ''' <param name="sKeyDestino"></param>
    Private Sub TratarTipoCampoGSDestino(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow, ByRef oDS As DataSet, ByRef sKeyAlmacen As String, ByRef sKeyDestino As String)
        oFSEntry.Width = Unit.Pixel(250)

        Dim oDests As FSNServer.Destinos
        oDests = FSNServer.Get_Object(GetType(FSNServer.Destinos))
        oDests.LoadData(sIdi, oUser.CodPersona)

        oFSEntry.Lista = oDests.Data
        oFSEntry.Intro = 1
        'Relaci�n con Almacen
        If Not CType(Me.Page, FSNPage).Acceso.gbUsar_OrgCompras Then
            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
                    sKeyAlmacen = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                End If
            Else
                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
                    sKeyAlmacen = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                End If
            End If
            If Not sKeyAlmacen Is Nothing Then
                oFSEntry.idDataEntryDependent3 = oFSEntry.IdContenedor & "_" & "fsentry" & sKeyAlmacen
            End If
        End If
        sKeyDestino = oFSEntry.ID
    End Sub
    ''' <summary>Tratamiento de un campo de tipo unidad</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow"></param>
    ''' <param name="oDS"></param>
    Private Sub TratarTipoCampoGSUnidad(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow, ByRef oDS As DataSet)
        'Relacion con la cantidad
        Dim sKeyCantidad As String = String.Empty
        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                sKeyCantidad = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
            End If
        Else
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                sKeyCantidad = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
            End If
        End If
        If sKeyCantidad <> "" Then oFSEntry.idEntryCANT = oFSEntry.IdContenedor & "_" & "fsentry_" & sKeyCantidad

        Dim oUnis As FSNServer.Unidades
        oUnis = FSNServer.Get_Object(GetType(FSNServer.Unidades))
        oUnis.LoadData(sIdi, , , False)
        oFSEntry.Lista = oUnis.Data
        oFSEntry.Intro = 1
    End Sub
    ''' <summary>Tratamiento de un campo de tipo unidad de pedido</summary>   
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    Private Sub TratarTipoCampoGSUnidadPedido(ByRef oFSEntry As DataEntry.GeneralEntry)
        Dim oUnis As FSNServer.Unidades
        oUnis = FSNServer.Get_Object(GetType(FSNServer.Unidades))
        oUnis.LoadData(sIdi, , , False)
        oFSEntry.Lista = oUnis.Data
        oFSEntry.Intro = 1
    End Sub
    ''' <summary>Tratamiento de un campo de tipo forma de pago</summary>   
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    Private Sub TratarTipoCampoGSFormaPago(ByRef oFSEntry As DataEntry.GeneralEntry)
        Dim oPags As FSNServer.FormasPago
        oPags = FSNServer.Get_Object(GetType(FSNServer.FormasPago))
        oPags.LoadData(sIdi)
        oFSEntry.Lista = oPags.Data
        oFSEntry.Intro = 1
    End Sub
    ''' <summary>Tratamiento de un campo de tipo moneda</summary>   
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    Private Sub TratarTipoCampoGSMoneda(ByRef oFSEntry As DataEntry.GeneralEntry)
        Dim oMons As FSNServer.Monedas
        oMons = FSNServer.Get_Object(GetType(FSNServer.Monedas))
        oMons.LoadData(sIdi)
        oFSEntry.Lista = oMons.Data
        oFSEntry.Intro = 1
    End Sub
    ''' <summary>Tratamiento de un campo de tipo pa�s</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow"></param>
    ''' <param name="oDS"></param>
    ''' <param name="sKeyPais"></param>
    ''' <param name="sKeyProvincia"></param>
    Private Sub TratarTipoCampoGSPais(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow, ByRef oDS As DataSet, ByRef sKeyPais As String, ByRef sKeyProvincia As String)
        sKeyPais = oFSEntry.ID

        Dim oPaises As FSNServer.Paises
        oPaises = FSNServer.Get_Object(GetType(FSNServer.Paises))
        oPaises.LoadData(sIdi)

        oFSEntry.Lista = oPaises.Data
        oFSEntry.Intro = 1

        'Relaci�n con Provincia
        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Provincia & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
                sKeyProvincia = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Provincia & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
            End If
        Else
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Provincia & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
                sKeyProvincia = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Provincia & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
            End If
        End If
        If Not sKeyProvincia Is Nothing Then oFSEntry.DependentField = "fsentry" & sKeyProvincia
    End Sub
    ''' <summary>Tratamiento de un campo de tipo provincia</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="sKeyPais"></param>
    ''' <param name="sCodPaisDefecto"></param>
    Private Sub TratarTipoCampoGSProvincia(ByRef oFSEntry As DataEntry.GeneralEntry, ByVal sKeyPais As String, ByVal sCodPaisDefecto As String)
        'Rellenamos lista de datos del control s�lo para que pueda coger el DEN del valor por defecto
        'en la funci�n CargarValoresDefecto. All� vaciamos la lista porque un webdropdown 11.1 con carga desde javascript habilitada no puede cargarse en servidor, 
        'Los datos salen mal en pantalla
        If Not oFSEntry.ReadOnly Then
            If sCodPaisDefecto <> Nothing Then
                Dim oProvis As FSNServer.Provincias
                oProvis = FSNServer.Get_Object(GetType(FSNServer.Provincias))
                oProvis.Pais = DBNullToStr(sCodPaisDefecto)
                oProvis.LoadData(sIdi)

                oFSEntry.Lista = oProvis.Data
            End If
        End If

        If sKeyPais <> String.Empty Then oFSEntry.MasterField = sKeyPais

        oFSEntry.Intro = 1
    End Sub
    ''' <summary>Tratamiento de un campo de tipo unidad organizativa</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow"></param>
    ''' <param name="oDS"></param>
    ''' <param name="sOrden"></param>
    ''' <param name="sKeyUnidadOrganizativa"></param>
    Private Sub TratarTipoCampoGSUnidadOrganizativa(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow, ByRef oDS As DataSet, ByVal sOrden As String, ByRef sKeyUnidadOrganizativa As String)
        oFSEntry.Width = Unit.Pixel(200)
        oFSEntry.Intro = 1
        oFSEntry.Orden = oRow.Item(sOrden)
        sKeyUnidadOrganizativa = oFSEntry.ID

        If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Departamento & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
            Dim sKeyDepartamento As String = String.Empty
            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                sKeyDepartamento = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Departamento & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
            Else
                sKeyDepartamento = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Departamento & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
            End If
            oFSEntry.DependentField = "fsentry" & sKeyDepartamento
        End If

        Dim sKeyArticuloUON As String = String.Empty
        If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO")).Length = 0 Then
            If oDS.Tables(0).Select("(TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS =" & TiposDeDatos.TipoCampoGS.DenArticulo & ") AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                If Not oDS.Tables(0).Columns("ID_CAMPO") Is Nothing Then
                    sKeyArticuloUON = oDS.Tables(0).Select("(TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS =" & TiposDeDatos.TipoCampoGS.DenArticulo & ") AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                Else
                    sKeyArticuloUON = oDS.Tables(0).Select("(TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS =" & TiposDeDatos.TipoCampoGS.DenArticulo & ") AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                End If
            End If
        End If
        If sKeyArticuloUON = "" AndAlso Not idDataEntryFORMUnidadOrganizativa = "" Then 'Tiene un campo UON antes que el desglose a nivel de formulario, viene de campos.ascx Then
            oFSEntry.idDataEntryDependent = idDataEntryFORMUnidadOrganizativa
        ElseIf Not sKeyArticuloUON = "" Then 'El campo UON estara en el propio desglose
            oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry_" & sKeyArticuloUON
        End If
    End Sub
    ''' <summary>Tratamiento de un campo de tipo departamento</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow"></param>
    ''' <param name="oDS"></param>
    ''' <param name="sOrden"></param>
    ''' <param name="sCodUnidadOrgDefecto"></param>
    ''' <param name="sKeyUnidadOrganizativa"></param>
    Private Sub TratarTipoCampoGSDepartamento(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow, ByRef oDS As DataSet, ByVal sOrden As String, ByVal sCodUnidadOrgDefecto As String, ByVal sKeyUnidadOrganizativa As String)
        oFSEntry.Intro = 1
        'El departamento puede depender de la unidad organizativa o no.
        '   En la funci�n CargarValoresDefecto, vaciaremos la lista porque un webdropdown 11.1 con carga desde javascript habilitada no puede cargarse en servidor, 
        '   Los datos salen mal en pantalla
        If Not oFSEntry.ReadOnly Then
            Dim oDepartamentos As FSNServer.Departamentos
            oDepartamentos = FSNServer.Get_Object(GetType(FSNServer.Departamentos))

            If sCodUnidadOrgDefecto <> "" Then
                Dim vUnidadesOrganizativas(3) As String
                vUnidadesOrganizativas = Split(sCodUnidadOrgDefecto, " - ")

                Select Case UBound(vUnidadesOrganizativas)
                    Case 0
                        oDepartamentos.LoadData(0)
                        oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                    Case 1
                        oDepartamentos.LoadData(1, vUnidadesOrganizativas(0))
                        oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                    Case 2
                        oDepartamentos.LoadData(2, vUnidadesOrganizativas(0), vUnidadesOrganizativas(1))
                        oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                    Case 3
                        oDepartamentos.LoadData(3, vUnidadesOrganizativas(0), vUnidadesOrganizativas(1), vUnidadesOrganizativas(2))
                        oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                End Select

            Else
                oDepartamentos.LoadData()
                oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
            End If
            oFSEntry.Lista = oDepartamentos.Data
        End If

        oFSEntry.Orden = oRow.Item(sOrden)

        If sKeyUnidadOrganizativa <> String.Empty Then oFSEntry.MasterField = sKeyUnidadOrganizativa
    End Sub
    ''' <summary>Tratamiento de un campo de tipo organizaci�n de compras</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow"></param>
    ''' <param name="oDS"></param>
    ''' <param name="sOrden"></param>
    ''' <param name="sKeyOrgComprasDataCheck"></param>
    Private Sub TratarTipoCampoGSOrganizacionCompras(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow, ByRef oDS As DataSet, ByVal sOrden As String, ByRef sKeyOrgComprasDataCheck As String)
        Dim oOrganizacionesCompras As FSNServer.OrganizacionesCompras
        oOrganizacionesCompras = FSNServer.Get_Object(GetType(FSNServer.OrganizacionesCompras))
        oOrganizacionesCompras.LoadData(oUser.Cod, oUser.PMRestriccionUONSolicitudAUONUsuario, oUser.PMRestriccionUONSolicitudAUONPerfil)
        oFSEntry.Lista = oOrganizacionesCompras.Data

        oFSEntry.Intro = 1
        oFSEntry.Orden = oRow.Item(sOrden)
        Dim sCampoDepend As String = ""
        Dim sql As String
        sql = "(TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS =" & TiposDeDatos.TipoCampoGS.DenArticulo & ") AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 "

        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select(sql).Length > 0 Then sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID_CAMPO")
        Else
            If oDS.Tables(0).Select(sql).Length > 0 Then sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID")
        End If
        oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry_" & sCampoDepend
        sql = "TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 "

        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select(sql).Length > 0 Then sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID_CAMPO")
        Else
            If oDS.Tables(0).Select(sql).Length > 0 Then sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID")
        End If
        If sCampoDepend <> "" Then oFSEntry.DependentField = oFSEntry.IdContenedor & "_" & "fsentry_" & sCampoDepend

        sql = "(TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Centro & ") AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 "
        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then ' Relacionar la Organizacion de Compras con el Centro
            If oDS.Tables(0).Select(sql).Length > 0 Then sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID_CAMPO")
        Else
            If oDS.Tables(0).Select(sql).Length > 0 Then sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID")
        End If
        If sCampoDepend <> "" Then oFSEntry.idDataEntryDependent2 = oFSEntry.IdContenedor & "_" & "fsentry_" & sCampoDepend
        sKeyOrgComprasDataCheck = oFSEntry.ID
    End Sub
    '' <summary>Tratamiento de un campo de tipo GS Almacen</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="sCodCentroDefecto"></param>
    ''' <param name="sKeyCentro"></param>
    ''' <param name="sKeyDestino"></param>
    ''' <param name="sOrden"></param>
    Private Sub TratarTipoCampoGSAlmacen(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow, ByVal sCodCentroDefecto As String, ByRef sKeyCentro As String, ByRef sKeyDestino As String, ByVal sOrden As String)
        'El almac�n puede depender del centro o no.
        'En los casos en que NO DEPENDA, hay que cargar los datos. Por eso se controla que sKeyCentro est� vac�o o no.
        'En los casos en que SI DEPENDA, se cargar�n los datos desde el cliente y s�lo se obtendr�n los datos en este punto, cuando haya valor por defecto (bFnValorDefecto)
        '   En la funci�n CargarValoresDefecto, vaciaremos la lista porque un webdropdown 11.1 con carga desde javascript habilitada no puede cargarse en servidor, 
        '   Los datos salen mal en pantalla
        If Not oFSEntry.ReadOnly Then
            Dim oAlmacenes As FSNServer.Almacenes
            oAlmacenes = FSNServer.Get_Object(GetType(FSNServer.Almacenes))
            If sCodCentroDefecto = "" Then
                oAlmacenes.LoadData()
            Else
                oAlmacenes.LoadData(DBNullToStr(sCodCentroDefecto))
            End If
            oFSEntry.Lista = oAlmacenes.Data
            oAlmacenes = Nothing
        End If

        If CType(Me.Page, FSNPage).Acceso.gbUsar_OrgCompras Then
            If sKeyCentro IsNot Nothing AndAlso sKeyCentro <> String.Empty Then oFSEntry.MasterField = sKeyCentro
        Else
            If sKeyDestino IsNot Nothing AndAlso sKeyDestino <> String.Empty Then oFSEntry.MasterField = sKeyDestino
        End If

        oFSEntry.Intro = 1
        oFSEntry.Orden = oRow.Item(sOrden)
    End Sub
    '' <summary>Tratamiento de un campo de tipo GS Empresa</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    Private Sub TratarTipoCampoGSEmpresa(ByRef oFSEntry As DataEntry.GeneralEntry)
        Dim oEmpresas As FSNServer.Empresas
        oEmpresas = FSNServer.Get_Object(GetType(FSNServer.Empresas))
        oFSEntry.Lista = oEmpresas.CargarEmpresas(sIdi)
        oFSEntry.Intro = 1
    End Sub
    ''' <summary>Tratamiento de un campo de tipo GS inicio suministro</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow"></param>
    ''' <param name="oDS"></param>
    Private Sub TratarTipoCampoGSIniSuministro(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow, ByRef oDS As DataSet)
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechas") Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechas", "<script>var sMensajeFecha = '" + JSText(mipage.Textos(10)) + "';</script>")
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechasDefecto") Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechasDefecto", "<script>var sMensajeFechaDefecto = '" + JSText(mipage.Textos(17)) + "';</script>")
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "FechaNoAnteriorAlSistema") Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "FechaNoAnteriorAlSistema", "<script>var sFechaNoAnteriorAlSistema = '" + JSText(mipage.Textos(48)) + "';</script>")
        End If
        Dim sCampoFinSuministro As String = String.Empty
        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.FinSuministro & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                sCampoFinSuministro = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.FinSuministro & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
            End If
        Else
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.FinSuministro & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                sCampoFinSuministro = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.FinSuministro & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
            End If
        End If
        If sCampoFinSuministro <> String.Empty Then
            oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry_" & sCampoFinSuministro
        End If
        oFSEntry.FechaNoAnteriorAlSistema = DBNullToBoolean(oRow.Item("FECHA_VALOR_NO_ANT_SIS"))
    End Sub
    ''' <summary>Tratamiento de un campo de tipo GS fin suministro</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow"></param>
    ''' <param name="oDS"></param>
    Private Sub TratarTipoCampoGSFinSuministro(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow, ByRef oDS As DataSet)
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechas") Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechas", "<script>var sMensajeFecha = '" + JSText(mipage.Textos(10)) + "';</script>")
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechasDefecto") Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechasDefecto", "<script>var sMensajeFechaDefecto = '" + JSText(mipage.Textos(17)) + "';</script>")
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "FechaNoAnteriorAlSistema") Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "FechaNoAnteriorAlSistema", "<script>var sFechaNoAnteriorAlSistema = '" + JSText(mipage.Textos(48)) + "';</script>")
        End If
        Dim sCampoIniSuministro As String = String.Empty
        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.IniSuministro & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                sCampoIniSuministro = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.IniSuministro & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
            End If
        Else
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.IniSuministro & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                sCampoIniSuministro = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.IniSuministro & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
            End If
        End If
        If sCampoIniSuministro <> String.Empty Then
            oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry_" & sCampoIniSuministro
        End If
        oFSEntry.FechaNoAnteriorAlSistema = DBNullToBoolean(oRow.Item("FECHA_VALOR_NO_ANT_SIS"))
    End Sub
    ''' <summary>Tratamiento de un campo de tipo GS precio unitario</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow"></param>
    Private Sub TratarTipoCampoGSPrecioUnitario(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow)
        If Not IsDBNull(oRow.Item("CARGAR_ULT_ADJ")) Then
            oFSEntry.CargarUltADJ = oRow.Item("CARGAR_ULT_ADJ")
        Else
            oFSEntry.CargarUltADJ = False
        End If
    End Sub
    ''' <summary>Tratamiento de un campo de tipo GS centro coste</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow"></param>
    Private Sub TratarTipoCampoGSCentroCoste(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow)
        If Not IsDBNull(oRow.Item("VER_UON")) Then
            oFSEntry.VerUON = oRow.Item("VER_UON")
        Else
            oFSEntry.VerUON = False
        End If
    End Sub
    ''' <summary>Tratamiento de un campo de tipo GS partida</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow"></param>
    Private Sub TratarTipoCampoGSPartida(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow)
        oFSEntry.PRES5 = oRow.Item("PRES5")
        oFSEntry.Width = Unit.Pixel(400)
        If Not IsDBNull(oRow.Item("CENTRO_COSTE")) Then oFSEntry.idEntryCC = oRow.Item("CENTRO_COSTE")
    End Sub
    ''' <summary>Tratamiento de un campo de tipo GS activo</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow"></param>
    Private Sub TratarTipoCampoGSActivo(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow)
        If Not IsDBNull(oRow.Item("CENTRO_COSTE")) Then oFSEntry.idEntryCC = oRow.Item("CENTRO_COSTE")
    End Sub
    ''' <summary>Tratamiento de un campo de tipo GS activo</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow"></param>
    Private Sub TratarTipoCampoGSAnyoPartida(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oDs As DataSet, ByRef oRow As DataRow)
        oFSEntry.Intro = 1
        If Not IsDBNull(oRow.Item("PARTIDAPRES")) Then
            Dim Quien As String = "ID"
            If Not oDs.Tables(0).Columns("COPIA_CAMPO_DEF") Is Nothing Then
                Quien = "COPIA_CAMPO_DEF"
            End If

            If oDs.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Partida & " AND " & Quien & " = " & oRow.Item("PARTIDAPRES")).Length = 0 Then
                'Es un campo
                If Not oDs.Tables(0).Columns("COPIA_CAMPO_DEF") Is Nothing Then
                    oFSEntry.idEntryPartidaPlurianual = oFSEntry.TabContainer & "_" & "fsentry" & idCampoPartida
                Else
                    oFSEntry.idEntryPartidaPlurianual = oFSEntry.TabContainer & "_" & "fsentry" & oRow.Item("PARTIDAPRES")
                End If
                oFSEntry.EntryPartidaPlurianualCampo = 1
            Else 'esta en desglose
                If Not oDs.Tables(0).Columns("COPIA_CAMPO_DEF") Is Nothing Then
                    oFSEntry.idEntryPartidaPlurianual = oFSEntry.IdContenedor & "_" & "fsentry" & oDs.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Partida & " AND COPIA_CAMPO_DEF = " & oRow.Item("PARTIDAPRES"))(0).Item("ID")
                Else
                    oFSEntry.idEntryPartidaPlurianual = oFSEntry.IdContenedor & "_" & "fsentry" & oRow.Item("PARTIDAPRES")
                End If
                oFSEntry.EntryPartidaPlurianualCampo = 0
            End If
        End If
    End Sub
    ''' <summary>Tratamiento de un campo de tipo centro</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow"></param>
    ''' <param name="oDS"></param>
    ''' <param name="sOrden"></param>
    ''' <param name="sCodOrgCompraDefecto"></param>
    ''' <param name="sKeyOrgComprasDataCheck"></param>
    ''' <param name="sIDCentro"></param>
    ''' <param name="sIDCampoOrgCompras"></param>
    ''' <param name="bCentroRelacionadoFORM"></param>
    ''' <param name="sCodOrgComprasFORM"></param>
    ''' <param name="sidDataEntryFORMOrgCompras"></param>
    ''' <param name="sKeyAlmacen"></param>
    ''' <param name="sKeyCentro"></param>
    Private Sub TratarTipoCampoGSCentro(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow, ByRef oDS As DataSet, ByVal sOrden As String, ByVal sCodOrgCompraDefecto As String, ByVal sKeyOrgComprasDataCheck As String,
                                        ByRef sIDCentro As String, ByVal sIDCampoOrgCompras As String, ByVal bCentroRelacionadoFORM As Boolean, ByVal sCodOrgComprasFORM As String, ByVal sidDataEntryFORMOrgCompras As String,
                                        ByRef sKeyAlmacen As String, ByRef sKeyCentro As String)
        'El centro siempres depende de la organizaci�n de compras.
        'Rellenamos lista de datos del control s�lo para que pueda coger el DEN del valor por defecto
        'en la funci�n CargarValoresDefecto. All� vaciamos la lista porque un webdropdown 11.1 con carga desde javascript habilitada no puede cargarse en servidor, 
        'Los datos salen mal en pantalla
        If Not oFSEntry.ReadOnly Then
            Dim oCentros As FSNServer.Centros
            oCentros = FSNServer.Get_Object(GetType(FSNServer.Centros))
            If sCodOrgCompraDefecto <> "" Then
                oCentros.LoadData(oUser.Cod, oUser.PMRestriccionUONSolicitudAUONUsuario, oUser.PMRestriccionUONSolicitudAUONPerfil, DBNullToStr(sCodOrgCompraDefecto))
            Else
                oCentros.LoadData(oUser.Cod, oUser.PMRestriccionUONSolicitudAUONUsuario, oUser.PMRestriccionUONSolicitudAUONPerfil)
            End If

            oFSEntry.Lista = oCentros.Data
        End If

        If sKeyOrgComprasDataCheck <> String.Empty Then oFSEntry.MasterField = sKeyOrgComprasDataCheck

        oFSEntry.Intro = 1
        oFSEntry.Orden = oRow.Item(sOrden)
        sIDCentro = oFSEntry.Tag

        Dim sScript As String = "var bCentroRelacionadoFORM" & sIDCampoOrgCompras & " = " + IIf(bCentroRelacionadoFORM, "true", "false") + ";"
        If Not IsNothing(sCodOrgComprasFORM) Then
            sScript += "var sCodOrgComprasFORM" & sIDCampoOrgCompras & " = '" + sCodOrgComprasFORM.ToString() + "' ;"
        Else
            sScript += "var sCodOrgComprasFORM" & sIDCampoOrgCompras & " ;"
        End If
        oFSEntry.DependentField = sIDCampoOrgCompras

        sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
        If Not Page.ClientScript.IsStartupScriptRegistered("VariablesORGANIZACIONCOMPRAS" & oFSEntry.Tag) Then
            Page.ClientScript.RegisterStartupScript(Page.GetType, "VariablesORGANIZACIONCOMPRAS" & oFSEntry.Tag, sScript)
        End If

        If Not sidDataEntryFORMOrgCompras Is Nothing Then oFSEntry.idDataEntryDependent2 = sidDataEntryFORMOrgCompras

        'Relacionar el Centro con el articulo, (Para cuando se elimine el centro que se elimine el articulo)
        Dim sCampoDepend As String = String.Empty
        Dim sql As String
        sql = "(TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS =" & TiposDeDatos.TipoCampoGS.DenArticulo & ") AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 "

        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select(sql).Length > 0 Then
                sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID_CAMPO")
            End If
        Else
            If oDS.Tables(0).Select(sql).Length > 0 Then
                sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID")
            End If
        End If
        oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry_" & sCampoDepend
        'Relaci�n con Almacen
        If CType(Me.Page, FSNPage).Acceso.gbUsar_OrgCompras Then
            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
                    sKeyAlmacen = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                End If
            Else
                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
                    sKeyAlmacen = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                End If
            End If
            If Not sKeyAlmacen Is Nothing Then oFSEntry.idDataEntryDependent3 = oFSEntry.IdContenedor & "_" & "fsentry" & sKeyAlmacen
        End If
        sKeyCentro = oFSEntry.ID
    End Sub
    ''' <summary>Tratamiento de un campo de tipo estado no conf.</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    Private Sub TratarTipoCampoGSEstadoNoConf(ByRef oFSEntry As DataEntry.GeneralEntry)
        Dim oEsts As FSNServer.EstadosNoConf
        oEsts = FSNServer.Get_Object(GetType(FSNServer.EstadosNoConf))
        oEsts.LoadData(mlInstancia, "", sIdi)
        oFSEntry.Lista = oEsts.Data
        oFSEntry.Intro = 1
    End Sub
    ''' <summary>Tratamiento de un campo de tipo estado interno no conf.</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    Private Sub TratarTipoCampoGSEstadoInternoNoConf(ByRef oFSEntry As DataEntry.GeneralEntry)
        oFSEntry.Valor = 1
        oFSEntry.Text = mipage.Textos(5)
        oFSEntry.Texto_Defecto = oFSEntry.Text
    End Sub
    ''' <summary>Tratamiento de un campo de tipo proveedor</summary>
    ''' <param name="oFSEntry">Objeto GeneralEntry</param>
    ''' <param name="oRow">Fila con los datos del campo</param>
    ''' <param name="oDS">Dataset con todos los campos</param>
    ''' <remarks>Llamada desde: Page_Load</remarks>
    Private Sub TratarTipoCampoGSProveedor(ByRef oFSEntry As DataEntry.GeneralEntry, ByRef oRow As DataRow, ByRef oDS As DataSet)
        If Not oFSEntry.ReadOnly Then
            oFSEntry.Text = mipage.Textos(2)
            oFSEntry.Texto_Defecto = oFSEntry.Text
        End If
        If Not IsDBNull(oRow.Item("CARGAR_ULT_ADJ")) Then
            oFSEntry.CargarUltADJ = oRow.Item("CARGAR_ULT_ADJ")
        Else
            oFSEntry.CargarUltADJ = False
        End If
        If Not IsDBNull(oRow.Item("CARGAR_REL_PROVE_ART4")) AndAlso Not oRow.Item("CARGAR_REL_PROVE_ART4") = 0 Then
            oFSEntry.CargarProveSumiArt = oRow.Item("CARGAR_REL_PROVE_ART4")
            Dim sCampoCodArt As String = String.Empty
            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                    sCampoCodArt = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                End If
            Else
                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                    sCampoCodArt = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                End If
            End If
            oFSEntry.idEntryART = oFSEntry.IdContenedor & "_fsentry_" & sCampoCodArt
        Else
            oFSEntry.CargarProveSumiArt = False
        End If
        If idDataEntryFORMOrgCompras <> "" Then ' Tiene un campo Organizacion de compras antes que el desglose!!!
            oFSEntry.idDataEntryDependent = idDataEntryFORMOrgCompras
        End If
        Dim sCampoDepend As String = ""
        Dim sql As String
        sql = "TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 "

        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select(sql).Length > 0 Then
                sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID_CAMPO")
            End If
        Else
            If oDS.Tables(0).Select(sql).Length > 0 Then
                sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID")
            End If
        End If
        If sCampoDepend <> "" Then
            oFSEntry.DependentField = oFSEntry.IdContenedor & "_" & "fsentry_" & sCampoDepend
        End If
        Dim sIDMaterial As String = String.Empty
        If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1").Length > 0 Then
                sIDMaterial = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1")(0).Item("ID_CAMPO")
            End If
        Else
            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1").Length > 0 Then
                sIDMaterial = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1")(0).Item("ID")
            End If
        End If
        oFSEntry.idDataEntryDependent2 = oFSEntry.IdContenedor & "_" & "fsentry_" & sIDMaterial
    End Sub
#End Region
    Public Function ComprobarCondiciones(ByVal lInstancia As Long, ByVal sFormula As String, ByVal drCondiciones As DataRow(), ByVal nLinea As Long) As Boolean
        Dim drCampo As DataRow
        Dim dsCampos As DataSet
        Dim oValorCampo As Object = Nothing
        Dim oValor As Object = Nothing
        Dim iTipo As TiposDeDatos.TipoGeneral
        Dim oCond As DataRow
        Dim i As Long
        Dim sVariables() As String = Nothing
        Dim dValues() As Double = Nothing
        Dim iEq As New USPExpress.USPExpression
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim oUser As FSNServer.User = Session("FSN_User")
        Dim sIdi As String = Session("FSN_User").Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If
        Try
            i = 0
            For Each oCond In drCondiciones
                ReDim Preserve sVariables(i)
                ReDim Preserve dValues(i)

                sVariables(i) = oCond.Item("COD")
                dValues(i) = 0 'El valor ser� 1 o 0 dependiendo de si se cumple o no la condici�n

                '<EXPRESION IZQUIEDA> <OPERADOR> <EXPRESION DERECHA>
                'Primero evaluamos <EXPRESION IZQUIERDA> 
                Select Case oCond.Item("TIPO_CAMPO")
                    Case 1 'Campo de formulario
                        If lInstancia > 0 Then
                            Dim oInstancia As FSNServer.Instancia
                            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                            oInstancia.Version = mlVersion
                            dsCampos = oInstancia.cargarCampo(oCond.Item("CAMPO_DATO"))
                            oInstancia = Nothing
                        Else
                            Dim oSolicitud As FSNServer.Solicitud
                            oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
                            dsCampos = oSolicitud.cargarCampo(oCond.Item("CAMPO_DATO"))
                            oSolicitud = Nothing
                        End If

                        If dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_DATO")).Length > 0 Then
                            Dim bDesglose As Boolean = False
                            drCampo = dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_DATO"))(0)
                            If drCampo.Item("ES_SUBCAMPO") = 1 Then
                                If lInstancia > 0 Then
                                    Dim oInstancia As FSNServer.Instancia
                                    oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                                    oInstancia.Version = mlVersion
                                    oValorCampo = oInstancia.cargarCampoDesglose(oCond.Item("CAMPO_DATO"), nLinea)
                                    oInstancia = Nothing
                                Else
                                    Dim oSolicitud As FSNServer.Solicitud
                                    oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
                                    oValorCampo = oSolicitud.cargarCampoDesglose(oCond.Item("CAMPO_DATO"), nLinea)
                                    oSolicitud = Nothing
                                End If
                                bDesglose = True
                            End If
                            If Not bDesglose Then
                                Select Case drCampo.Item("SUBTIPO")
                                    Case TiposDeDatos.TipoGeneral.TipoNumerico
                                        oValorCampo = DBNullToSomething(drCampo.Item("VALOR_NUM"))
                                    Case TiposDeDatos.TipoGeneral.TipoFecha
                                        oValorCampo = DBNullToSomething(drCampo.Item("VALOR_FEC"))
                                    Case TiposDeDatos.TipoGeneral.TipoBoolean
                                        oValorCampo = DBNullToSomething(drCampo.Item("VALOR_BOOL"))
                                    Case Else
                                        oValorCampo = DBNullToStr(drCampo.Item("VALOR_TEXT"))
                                End Select
                            End If
                            iTipo = drCampo.Item("SUBTIPO")
                        End If
                    Case 2 'Peticionario
                        If mlInstancia > 0 Then
                            'Comprueba si la solicitud es de no conformidad o certificado.Esto habr� que quitarlo cuando
                            'se haga el workflow para certificados y no conformidades
                            Dim oInstancia As FSNServer.Instancia
                            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                            oInstancia.ID = lInstancia
                            oValorCampo = oInstancia.Get_Peticionario_Instancia
                            oInstancia = Nothing
                        Else
                            oValorCampo = oUser.CodPersona
                        End If
                        iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
                    Case 3 'Departamento del peticionario
                        If mlInstancia > 0 Then
                            Dim oInstancia As FSNServer.Instancia
                            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                            oInstancia.ID = lInstancia
                            Dim oPer As FSNServer.Persona = FSNServer.Get_Object(GetType(FSNServer.Persona))
                            oPer.LoadData(oInstancia.Get_Peticionario_Instancia)
                            oValorCampo = oPer.Departamento
                        Else
                            oValorCampo = oUser.Dep
                        End If
                        iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
                    Case 4 'UON del peticionario
                        If mlInstancia > 0 Then
                            Dim oInstancia As FSNServer.Instancia
                            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                            oInstancia.ID = lInstancia
                            Dim oPer As FSNServer.Persona = FSNServer.Get_Object(GetType(FSNServer.Persona))
                            oPer.LoadData(oInstancia.Get_Peticionario_Instancia)
                            oValorCampo = oPer.UONs
                        Else
                            If oUser.UON3 = "" Then
                                If oUser.UON2 = "" Then
                                    oValorCampo = oUser.UON1
                                Else
                                    oValorCampo = oUser.UON1 & "-" & oUser.UON2
                                End If
                            Else
                                oValorCampo = oUser.UON1 & "-" & oUser.UON2 & "-" & oUser.UON3
                            End If
                        End If
                        iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
                    Case 5 'N� de procesos de compra abiertos
                        If mlInstancia > 0 Then
                            Dim oInstancia As FSNServer.Instancia
                            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                            oInstancia.ID = lInstancia
                            oInstancia.DevolverProcesosRelacionados()
                            oValorCampo = oInstancia.Procesos.Tables(0).Rows.Count
                            oInstancia = Nothing
                        Else
                            oValorCampo = 0
                        End If
                        iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
                    Case 6 'Importe adj 
                        If mlInstancia > 0 Then
                            Dim oInstancia As FSNServer.Instancia
                            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                            oInstancia.ID = lInstancia
                            oValorCampo = oInstancia.ObtenerImporteAdjudicado
                            oInstancia = Nothing
                        Else
                            oValorCampo = 0
                        End If
                        iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
                    Case 7 'Importe de la solicitud de compra
                        If lInstancia > 0 Then
                            Dim oInstancia As FSNServer.Instancia
                            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                            oInstancia.ID = lInstancia
                            oInstancia.Load(sIdi)
                            oValorCampo = oInstancia.Importe
                            oInstancia = Nothing
                        Else
                            oValorCampo = 0
                        End If
                        iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
                End Select
                'Luego <EXPRESION DERECHA>
                Select Case oCond.Item("TIPO_VALOR")
                    Case 1 'Campo de formulario
                        If lInstancia > 0 Then
                            Dim oInstancia As FSNServer.Instancia
                            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                            oInstancia.Version = mlVersion
                            dsCampos = oInstancia.cargarCampo(oCond.Item("CAMPO_VALOR"))
                            oInstancia = Nothing
                        Else
                            Dim oSolicitud As FSNServer.Solicitud
                            oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
                            dsCampos = oSolicitud.cargarCampo(oCond.Item("CAMPO_VALOR"))
                            oSolicitud = Nothing
                        End If

                        If dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_VALOR")).Length > 0 Then
                            Dim bDesglose As Boolean = False
                            drCampo = dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_VALOR"))(0)

                            If drCampo.Item("ES_SUBCAMPO") = 1 Then
                                If lInstancia > 0 Then
                                    Dim oInstancia As FSNServer.Instancia
                                    oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                                    oInstancia.Version = mlVersion
                                    oValor = oInstancia.cargarCampoDesglose(oCond.Item("CAMPO_VALOR"), nLinea)
                                    oInstancia = Nothing
                                Else
                                    Dim oSolicitud As FSNServer.Solicitud
                                    oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
                                    oValor = oSolicitud.cargarCampoDesglose(oCond.Item("CAMPO_VALOR"), nLinea)
                                    oSolicitud = Nothing
                                End If
                                bDesglose = True
                            End If
                            If Not bDesglose Then
                                Select Case drCampo.Item("SUBTIPO")
                                    Case TiposDeDatos.TipoGeneral.TipoNumerico
                                        oValor = DBNullToSomething(drCampo.Item("VALOR_NUM"))
                                    Case TiposDeDatos.TipoGeneral.TipoFecha
                                        oValor = DBNullToSomething(drCampo.Item("VALOR_FEC"))
                                    Case TiposDeDatos.TipoGeneral.TipoBoolean
                                        oValor = DBNullToSomething(drCampo.Item("VALOR_BOOL"))
                                    Case Else
                                        oValor = DBNullToSomething(drCampo.Item("VALOR_TEXT"))
                                End Select
                            End If
                        End If
                    Case 2, 10 'valor est�tico
                        Select Case iTipo
                            Case TiposDeDatos.TipoGeneral.TipoNumerico
                                oValor = DBNullToSomething(oCond.Item("VALOR_NUM"))
                            Case TiposDeDatos.TipoGeneral.TipoFecha
                                oValor = DBNullToSomething(oCond.Item("VALOR_FEC"))
                            Case TiposDeDatos.TipoGeneral.TipoBoolean
                                oValor = DBNullToSomething(oCond.Item("VALOR_BOOL"))
                            Case Else
                                If oCond.Item("TIPO_CAMPO") = 4 Then 'UON DEL PETICIONARIO
                                    Dim oUON() As String
                                    Dim iIndice As Integer
                                    oValor = Nothing
                                    oUON = Split(oCond.Item("VALOR_TEXT"), "-")
                                    For iIndice = 0 To UBound(oUON)
                                        If oUON(iIndice) <> "" Then
                                            oValor = oValor & Trim(oUON(iIndice)) & "-"
                                        End If
                                    Next iIndice
                                    oValor = Left(oValor, Len(oValor) - 1)
                                Else
                                    oValor = DBNullToSomething(oCond.Item("VALOR_TEXT"))
                                End If
                        End Select
                End Select
                'y por �ltimo con el OPERADOR obtenemos el valor
                Select Case iTipo
                    Case 2, 3
                        Select Case oCond.Item("OPERADOR")
                            Case ">"
                                If oValorCampo > oValor Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If

                            Case "<"
                                If oValorCampo < oValor Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Case ">="
                                If oValorCampo >= oValor Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Case "<="
                                If oValorCampo <= oValor Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Case "="
                                If oValorCampo = oValor Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Case "<>"
                                If oValorCampo <> oValor Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                        End Select
                    Case 4
                        If IIf(IsNothing(oValorCampo), -1, oValorCampo) = oValor Then
                            dValues(i) = 1
                        Else
                            dValues(i) = 0
                        End If
                    Case Else
                        Select Case UCase(oCond.Item("OPERADOR"))
                            Case "="
                                If UCase(oValorCampo) = UCase(oValor) Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If

                            Case "LIKE"
                                If Left(oValor, 1) = "%" Then
                                    If Right(oValor, 1) = "%" Then
                                        oValor = oValor.ToString.Replace("%", "")
                                        If InStr(oValorCampo.ToString, oValor.ToString) > 0 Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Else
                                        oValor = oValor.ToString.Replace("%", "")
                                        If oValorCampo.ToString.EndsWith(oValor.ToString) Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    End If
                                Else
                                    If Right(oValor, 1) = "%" Then
                                        oValor = oValor.ToString.Replace("%", "")
                                        If oValorCampo.ToString.StartsWith(oValor.ToString) Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Else
                                        If UCase(oValorCampo.ToString) = UCase(oValor.ToString) Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If

                                    End If
                                End If
                            Case "<>"
                                If UCase(oValorCampo.ToString) <> UCase(oValor.ToString) Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                        End Select
                End Select
                i += 1
            Next
            oValor = Nothing
            Try
                iEq.Parse(sFormula, sVariables)
                oValor = iEq.Evaluate(dValues)
            Catch ex As USPExpress.ParseException

            End Try
        Catch e As Exception
            Dim a As String = e.Message
        End Try


        Return (oValor > 0)
    End Function
    ''' Revisado por: blp. Fecha: 04/10/2011
    ''' <summary>
    ''' Carga los valores por defecto desde la bbdd. Solo para campos no readonly pq los redonly son 
    ''' etiquetas (no es as�, algunos son entry-> unos con defecto otros no -> a�adir fila incorrecto)
    ''' Que pasa tras guardar un no conformidad, q la crea y llama a esta funci�n sin que haya una tabla 
    ''' 5, con un catch se recupera
    ''' </summary>
    ''' <param name="oDs">Dataset con los datos de la linea desglose</param>
    ''' <param name="sIdi">idioma</param>        
    ''' <param name="NumLineas">numero de lineas del desglose</param> 
    ''' <param name="oCampo">objeto desglose</param> 
    ''' <param name="bQA">si es qa o no</param> 
    ''' <remarks>Llamada desde: Page_load ; Tiempo m�ximo: instantaneo</remarks>
    Private Sub CargarValoresDefecto(ByVal oDs As DataSet, ByVal sIdi As String, ByVal NumLineas As Integer, ByVal oCampo As FSNServer.Campo, ByVal bQA As Boolean)
        Dim oFSDSEntry As DataEntry.GeneralEntry
        Dim oRow As DataRow
        Dim Lineas As Integer
        Dim i As Integer
        Dim sTexto As String
        Dim sValor As String
        Dim sValorMat As String
        Dim iContadorPres As Integer
        Dim sCodPais As String = String.Empty
        Dim iTipoGS As TiposDeDatos.TipoCampoGS
        Dim iTipo As TiposDeDatos.TipoGeneral
        Dim FSNServer As FSNServer.Root = mipage.FSNServer
        Dim oUser As FSNServer.User = mipage.FSNUser
        Dim bFav As Boolean = IIf((Request("Favorito")) = "1", True, False)
        Dim arrMat(4) As String
        Dim iNivel As Integer
        Dim lIdPresup As Integer
        Dim dPorcent As Decimal
        Dim arrPresupuestos() As String
        Dim oPresup As String
        Dim arrPresup(2) As String
        Dim oPres1 As FSNServer.PresProyectosNivel1
        Dim oPres2 As FSNServer.PresContablesNivel1
        Dim oPres3 As FSNServer.PresConceptos3Nivel1
        Dim oPres4 As FSNServer.PresConceptos4Nivel1
        Dim oDSPres As DataSet = Nothing
        Dim iAnyo As Integer
        Dim oGMN1s As FSNServer.GruposMatNivel1
        Dim oGMN1 As FSNServer.GrupoMatNivel1
        Dim oGMN2 As FSNServer.GrupoMatNivel2
        Dim oGMN3 As FSNServer.GrupoMatNivel3
        Dim oRowLista() As DataRow
        Dim DeDonde As Integer = 5

        Try
            Lineas = oDs.Tables(DeDonde).Rows.Count
        Catch ex As Exception
            DeDonde = 2

            If bQA Then oDs = oCampo.LoadDesglose(sIdi, oCampo.IdSolicitud, , , , oUser.DateFmt, , )
        End Try

        Dim sIdCampoHijoDesglose As String
        For Each oRow In oDs.Tables(DeDonde).Rows
            sIdCampoHijoDesglose = oRow.Item("CAMPO_HIJO").ToString
            sValor = ""

            Lineas = 0 'Para que siempre entre al menos una vez.
            Do While Lineas <= NumLineas
                Try
                    If Lineas = 0 Then
                        oFSDSEntry = Me.tblDesglose.FindControl("fsentry" & sIdCampoHijoDesglose)
                    Else
                        oFSDSEntry = Me.tblDesglose.FindControl("fsdsentry_" & Lineas.ToString & "_" & sIdCampoHijoDesglose)
                    End If

                Catch
                    oFSDSEntry = Nothing
                End Try

                If Not oFSDSEntry Is Nothing Then
                    oFSDSEntry.Posee_Defecto = False
                    oFSDSEntry.Valor_Defecto = System.DBNull.Value
                    oFSDSEntry.Texto_Defecto = IIf(oFSDSEntry.Text Is Nothing, "", oFSDSEntry.Text)
                    iTipo = oFSDSEntry.Tipo
                    iTipoGS = oFSDSEntry.TipoGS

                    If (Not IsDBNull(oRow.Item("VALOR_TEXT")) Or Not IsDBNull(oRow.Item("VALOR_NUM")) Or Not IsDBNull(oRow.Item("VALOR_FEC")) Or Not IsDBNull(oRow.Item("VALOR_BOOL")) Or iTipo = TiposDeDatos.TipoGeneral.TipoArchivo Or iTipoGS = TiposDeDatos.TipoCampoGS.NuevoCodArticulo Or iTipoGS = TiposDeDatos.TipoCampoGS.DenArticulo Or iTipoGS = TiposDeDatos.TipoCampoGS.CodArticulo) Then
                        Select Case iTipo
                            Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoEditor
                                oFSDSEntry.Valor_Defecto = DBNullToSomething(oRow.Item("VALOR_TEXT"))

                                If oFSDSEntry.Valor_Defecto <> Nothing Then
                                    oFSDSEntry.Posee_Defecto = True

                                    If iTipoGS = TiposDeDatos.TipoCampoGS.SinTipo And oFSDSEntry.Intro = 1 Then
                                        If oFSDSEntry.Lista.Tables.Count > 0 Then
                                            If DBNullToSomething(oRow.Item("VALOR_NUM").ToString) <> Nothing Then
                                                oRowLista = oFSDSEntry.Lista.Tables(0).Select("ID = " + oRow.Item("CAMPO_HIJO").ToString + " and ORDEN=" + oRow.Item("VALOR_NUM").ToString)
                                            Else
                                                oRowLista = oFSDSEntry.Lista.Tables(0).Select("ID = " + oRow.Item("CAMPO_HIJO").ToString + " and VALOR_TEXT_" & sIdi & " =" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))
                                            End If
                                            If oRowLista.Length > 0 Then
                                                oFSDSEntry.Texto_Defecto = oRowLista(0).Item("VALOR_TEXT_" & sIdi)
                                                oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_NUM")
                                            Else
                                                oFSDSEntry.Texto_Defecto = oFSDSEntry.Valor_Defecto
                                                oFSDSEntry.Valor_Defecto = Nothing
                                            End If
                                        Else
                                            oFSDSEntry.Texto_Defecto = oFSDSEntry.Valor_Defecto
                                            oFSDSEntry.Valor_Defecto = Nothing
                                        End If
                                    End If
                                End If

                            Case TiposDeDatos.TipoGeneral.TipoNumerico
                                oFSDSEntry.Valor_Defecto = DBNullToSomething(oRow.Item("VALOR_NUM"))
                                If oFSDSEntry.Valor_Defecto <> Nothing OrElse oFSDSEntry.Valor_Defecto = 0 Then oFSDSEntry.Posee_Defecto = True
                            Case TiposDeDatos.TipoGeneral.TipoFecha
                                Dim iTipoFecha As TiposDeDatos.TipoFecha
                                iTipoFecha = DBNullToSomething(DBNullToSomething(oRow.Item("VALOR_NUM")))
                                oFSDSEntry.Valor_Defecto = DevolverFechaRelativaA(iTipoFecha, DBNullToSomething(oRow.Item("VALOR_FEC")), bFav)
                                If oFSDSEntry.Valor_Defecto <> Nothing Then oFSDSEntry.Posee_Defecto = True
                            Case TiposDeDatos.TipoGeneral.TipoArchivo
                                sAdjun = ""
                                idAdjun = ""
                                Dim sNombreRelacion As String
                                If mlInstancia > 0 Then
                                    sNombreRelacion = "REL_LINEA_ADJUNTO_DEFECTO"
                                Else
                                    sNombreRelacion = "REL_LINEA_ADJUNTO"
                                End If
                                For Each oDSRowAdjun As DataRow In oRow.GetChildRows(sNombreRelacion)
                                    idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                                    sAdjun += oDSRowAdjun.Item("NOM") + " (" + FSNLibrary.FormatNumber((DBNullToSomething(oDSRowAdjun.Item("DATASIZE")) / 1024), oUser.NumberFormat) + " Kb.), "
                                Next
                                If sAdjun <> "" Then
                                    sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
                                    idAdjun = idAdjun.Substring(0, idAdjun.Length - 2)
                                End If
                                oFSDSEntry.Valor_Defecto = idAdjun
                                oFSDSEntry.Texto_Defecto = AjustarAnchoTextoPixels(sAdjun, 150, "Verdana", 12)

                                If oFSDSEntry.Valor_Defecto <> "" Then oFSDSEntry.Posee_Defecto = True
                            Case TiposDeDatos.TipoGeneral.TipoBoolean
                                oFSDSEntry.Valor_Defecto = DBNullToSomething(oRow.Item("VALOR_BOOL"))
                                If Not IsDBNull(oRow.Item("VALOR_BOOL")) Then
                                    oFSDSEntry.Texto_Defecto = If(oRow.Item("VALOR_BOOL") = 1, mipage.Textos(15), mipage.Textos(16))
                                End If
                                If Not oFSDSEntry.Valor_Defecto Is Nothing Then oFSDSEntry.Posee_Defecto = True
                        End Select

                        Select Case iTipoGS
                            Case TiposDeDatos.TipoCampoGS.CodArticulo
                                If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                    Dim oArts As FSNServer.Articulos

                                    oArts = FSNServer.Get_Object(GetType(FSNServer.Articulos))
                                    oArts.GMN1 = arrMat(1)
                                    oArts.GMN2 = arrMat(2)
                                    oArts.GMN3 = arrMat(3)
                                    oArts.GMN4 = arrMat(4)
                                    oArts.LoadData(oUser.Cod, oUser.PMRestriccionUONSolicitudAUONUsuario, oUser.PMRestriccionUONSolicitudAUONPerfil,
                                                   oRow.Item("VALOR_TEXT"), , True, AccesoFSEP:=FSNServer.TipoAcceso.gbAccesoFSEP)
                                    If oArts.Data.Tables(0).Rows.Count > 0 Then
                                        oFSDSEntry.Valor_Defecto = oArts.Data.Tables(0).Rows(0).Item("COD")
                                        oFSDSEntry.Texto_Defecto = oArts.Data.Tables(0).Rows(0).Item("DEN")
                                    Else
                                        oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT")
                                        oFSDSEntry.Valor_Defecto = Nothing
                                    End If
                                End If
                            Case TiposDeDatos.TipoCampoGS.NuevoCodArticulo
                                If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                    oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT")
                                    oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                End If
                            Case TiposDeDatos.TipoCampoGS.DenArticulo
                                If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                    oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT")
                                    oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                End If
                            Case TiposDeDatos.TipoCampoGS.Dest
                                If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                    oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))

                                    oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                    oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN") & If(IsDBNull(oRowLista(0).Item("POB")), "", " (" & oRowLista(0).Item("POB") & ")")
                                End If
                                oFSDSEntry.Lista = Nothing
                            Case TiposDeDatos.TipoCampoGS.Unidad
                                If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                    oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))

                                    oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                    oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN")
                                End If
                                oFSDSEntry.Lista = Nothing
                            Case TiposDeDatos.TipoCampoGS.UnidadPedido
                                If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                    oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))

                                    oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                    oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN")
                                End If
                                oFSDSEntry.Lista = Nothing
                            Case TiposDeDatos.TipoCampoGS.FormaPago
                                If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                    oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))

                                    oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                    oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN")
                                End If
                                oFSDSEntry.Lista = Nothing
                            Case TiposDeDatos.TipoCampoGS.Moneda
                                If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                    oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))

                                    oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                    oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN")
                                End If
                                oFSDSEntry.Lista = Nothing
                            Case TiposDeDatos.TipoCampoGS.Pais
                                If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                    oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))

                                    oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                    oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN")

                                    sCodPais = oRow.Item("VALOR_TEXT")
                                End If
                                oFSDSEntry.Lista = Nothing
                            Case TiposDeDatos.TipoCampoGS.ProveedorERP
                                If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                    oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                    If (oFSDSEntry.Lista IsNot Nothing) Then
                                        oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oFSDSEntry.Valor_Defecto))
                                        If oRowLista.Length > 0 Then _
                                            oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN")
                                    End If
                                End If
                                'Vaciamos el contenido porque s�lo lo necesit�bamos aqu� y har�a fallar la carga de datos desde ajax para los webdropdown 11.1
                                oFSDSEntry.Lista = Nothing
                            Case TiposDeDatos.TipoCampoGS.Provincia
                                If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                    oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                    If (oFSDSEntry.Lista IsNot Nothing) Then
                                        oRowLista = oFSDSEntry.Lista.Tables(0).Select("PAICOD=" + StrToSQLNULL(sCodPais) + " AND COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))
                                        If oRowLista.Length > 0 Then _
                                            oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN")
                                    End If
                                End If
                                'Vaciamos el contenido porque s�lo lo necesit�bamos aqu� y har�a fallar la carga de datos desde ajax para los webdropdown 11.1
                                oFSDSEntry.Lista = Nothing
                            Case TiposDeDatos.TipoCampoGS.PRES1, TiposDeDatos.TipoCampoGS.Pres2, TiposDeDatos.TipoCampoGS.Pres3, TiposDeDatos.TipoCampoGS.Pres4
                                oFSDSEntry.Valor_Defecto = DBNullToSomething(oRow.Item("VALOR_TEXT"))
                                If DBNullToSomething(oRow.Item("VALOR_TEXT")) <> Nothing Then
                                    sValor = ""
                                    sTexto = ""
                                    iContadorPres = 0
                                    arrPresupuestos = oRow.Item("VALOR_TEXT").split("#")
                                    For Each oPresup In arrPresupuestos
                                        iContadorPres = iContadorPres + 1
                                        arrPresup = oPresup.Split("_")
                                        iNivel = arrPresup(0)
                                        lIdPresup = arrPresup(1)
                                        'Los presupuestos siempre llevan el porcentaje como 0.xx salvo cuando es 100% que llevan 1
                                        dPorcent = Numero(arrPresup(2), ".", "")

                                        Select Case iTipoGS
                                            Case TiposDeDatos.TipoCampoGS.PRES1
                                                oPres1 = FSNServer.Get_Object(GetType(FSNServer.PresProyectosNivel1))
                                                Select Case iNivel
                                                    Case 1
                                                        oPres1.LoadData(lIdPresup)
                                                    Case 2
                                                        oPres1.LoadData(Nothing, lIdPresup)
                                                    Case 3
                                                        oPres1.LoadData(Nothing, Nothing, lIdPresup)
                                                    Case 4
                                                        oPres1.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                                                End Select
                                                oDSPres = oPres1.Data
                                                oPres1 = Nothing
                                            Case TiposDeDatos.TipoCampoGS.Pres2
                                                oPres2 = FSNServer.Get_Object(GetType(FSNServer.PresContablesNivel1))
                                                Select Case iNivel
                                                    Case 1
                                                        oPres2.LoadData(lIdPresup)
                                                    Case 2
                                                        oPres2.LoadData(Nothing, lIdPresup)
                                                    Case 3
                                                        oPres2.LoadData(Nothing, Nothing, lIdPresup)
                                                    Case 4
                                                        oPres2.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                                                End Select
                                                oDSPres = oPres2.Data
                                                oPres2 = Nothing
                                            Case TiposDeDatos.TipoCampoGS.Pres3
                                                oPres3 = FSNServer.Get_Object(GetType(FSNServer.PresConceptos3Nivel1))
                                                Select Case iNivel
                                                    Case 1
                                                        oPres3.LoadData(lIdPresup)
                                                    Case 2
                                                        oPres3.LoadData(Nothing, lIdPresup)
                                                    Case 3
                                                        oPres3.LoadData(Nothing, Nothing, lIdPresup)
                                                    Case 4
                                                        oPres3.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                                                End Select
                                                oDSPres = oPres3.Data
                                                oPres3 = Nothing
                                            Case TiposDeDatos.TipoCampoGS.Pres4
                                                oPres4 = FSNServer.Get_Object(GetType(FSNServer.PresConceptos4Nivel1))
                                                Select Case iNivel
                                                    Case 1
                                                        oPres4.LoadData(lIdPresup)
                                                    Case 2
                                                        oPres4.LoadData(Nothing, lIdPresup)
                                                    Case 3
                                                        oPres4.LoadData(Nothing, Nothing, lIdPresup)
                                                    Case 4
                                                        oPres4.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                                                End Select
                                                oDSPres = oPres4.Data
                                                oPres4 = Nothing
                                        End Select

                                        If Not oDSPres Is Nothing Then
                                            If oDSPres.Tables(0).Rows.Count > 0 Then
                                                With oDSPres.Tables(0).Rows(0)
                                                    'comprobamos que el valor por defecto cumpla la restricci�n del usuario. SI NO CUMPLE, NO APARECER�
                                                    If Not oDSPres.Tables(0).Columns("ANYO") Is Nothing Then
                                                        iAnyo = .Item("ANYO")
                                                    End If
                                                    Dim bSeleccionado As Boolean = True
                                                    Dim sPorcentaje As String
                                                    For i = 4 To 1 Step -1
                                                        'PRESi:
                                                        If Not IsDBNull(.Item("PRES" & i)) Then
                                                            If bSeleccionado Then
                                                                sPorcentaje = " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
                                                            Else
                                                                sPorcentaje = " - "
                                                            End If
                                                            sTexto = .Item("PRES" & i).ToString & sPorcentaje & sTexto
                                                            bSeleccionado = False
                                                        End If
                                                    Next
                                                    If Not oDSPres.Tables(0).Columns("ANYO") Is Nothing Then
                                                        sTexto = .Item("ANYO").ToString & " - " & sTexto
                                                    End If
                                                    sValor += oPresup + "#"
                                                End With
                                            End If
                                        End If

                                    Next
                                    If sValor <> Nothing Then
                                        sValor = Left(sValor, sValor.Length - 1)
                                    End If
                                    oFSDSEntry.Valor_Defecto = sValor
                                    If sTexto = "" Then
                                    Else
                                        oFSDSEntry.Texto_Defecto = sTexto
                                    End If
                                End If

                            Case TiposDeDatos.TipoCampoGS.Proveedor
                                If DBNullToSomething(oRow.Item("VALOR_TEXT")) <> Nothing Then
                                    Dim oProve As FSNServer.Proveedor
                                    oProve = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                                    oProve.Cod = oRow.Item("VALOR_TEXT")
                                    oProve.Load(sIdi, True)
                                    oFSDSEntry.Texto_Defecto = oProve.Cod + " - " + oProve.Den.ToString.Replace("'", "")
                                End If
                            Case TiposDeDatos.TipoCampoGS.Material
                                Dim sMat As String = DBNullToSomething(oRow.Item("VALOR_TEXT"))
                                Dim lLongCod As Integer
                                sValorMat = sMat

                                For i = 1 To 4
                                    arrMat(i) = ""
                                Next i

                                i = 1
                                While Trim(sMat) <> ""
                                    Select Case i
                                        Case 1
                                            lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                                        Case 2
                                            lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                                        Case 3
                                            lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                                        Case 4
                                            lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
                                    End Select
                                    arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
                                    sMat = Mid(sMat, lLongCod + 1)
                                    i = i + 1
                                End While

                                If arrMat(4) <> "" Then
                                    oGMN3 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel3))
                                    oGMN3.GMN1Cod = arrMat(1)
                                    oGMN3.GMN2Cod = arrMat(2)
                                    oGMN3.Cod = arrMat(3)

                                    oGMN3.CargarTodosLosGruposMatDesde(1, sIdi, arrMat(4), , True)
                                    If Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles Then
                                        sValor = arrMat(1) + " - " + arrMat(2) + " - " + arrMat(3) + " - " + arrMat(4) + " - " + oGMN3.GruposMatNivel4.Item(arrMat(4)).Den
                                    Else
                                        sValor = arrMat(4) + " - " + oGMN3.GruposMatNivel4.Item(arrMat(4)).Den
                                    End If
                                    oGMN3 = Nothing

                                ElseIf arrMat(3) <> "" Then
                                    oGMN2 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel2))
                                    oGMN2.GMN1Cod = arrMat(1)
                                    oGMN2.Cod = arrMat(2)
                                    oGMN2.CargarTodosLosGruposMatDesde(1, sIdi, arrMat(3), , True)
                                    If Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles Then
                                        sValor = arrMat(1) + " - " + arrMat(2) + " - " + arrMat(3) + " - " + oGMN2.GruposMatNivel3.Item(arrMat(3)).Den
                                    Else
                                        sValor = arrMat(3) + " - " + oGMN2.GruposMatNivel3.Item(arrMat(3)).Den
                                    End If
                                    oGMN2 = Nothing

                                ElseIf arrMat(2) <> "" Then
                                    oGMN1 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel1))
                                    oGMN1.Cod = arrMat(1)
                                    oGMN1.CargarTodosLosGruposMatDesde(1, sIdi, arrMat(2), , True)
                                    If Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles Then
                                        sValor = arrMat(1) + " - " + arrMat(2) + " - " + oGMN1.GruposMatNivel2.Item(arrMat(2)).Den
                                    Else
                                        sValor = arrMat(2) + " - " + oGMN1.GruposMatNivel2.Item(arrMat(2)).Den
                                    End If
                                    oGMN1 = Nothing

                                ElseIf arrMat(1) <> "" Then
                                    oGMN1s = FSNServer.Get_Object(GetType(FSNServer.GruposMatNivel1))
                                    oGMN1s.CargarTodosLosGruposMatDesde(1, sIdi, arrMat(1), , , True)
                                    sValor = arrMat(1) + " - " + oGMN1s.Item(arrMat(1)).Den
                                    oGMN1s = Nothing
                                End If
                                oFSDSEntry.Texto_Defecto = sValor
                            Case TiposDeDatos.TipoCampoGS.EstadoNoConf
                                If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                    Dim oInstancia As FSNServer.Instancia
                                    oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                                    oInstancia.ID = mlInstancia
                                    Dim oDSListaEstados As DataSet = oInstancia.CargarTiposEstadoAccion()
                                    Dim lIndexEstado As Integer
                                    If Not IsDBNull(oRow.Item("INDEX")) Then
                                        If lIndexEstado <> Nothing Then
                                            oFSDSEntry.Valor_Defecto = oDSListaEstados.Tables(0).Rows(lIndexEstado).Item("COD")
                                            oFSDSEntry.Texto_Defecto = Mid(oDSListaEstados.Tables(0).Rows(lIndexEstado).Item("DEN_" & sIdi), 6)
                                        Else
                                            Dim oEsts As FSNServer.EstadosNoConf
                                            oEsts = FSNServer.Get_Object(GetType(FSNServer.EstadosNoConf))
                                            oEsts.LoadData(mlInstancia, oRow.Item("VALOR_TEXT"), sIdi)
                                            If oEsts.Data.Tables(0).Rows.Count > 0 Then
                                                oFSDSEntry.Valor_Defecto = oEsts.Data.Tables(0).Rows(0).Item("COD")
                                                oFSDSEntry.Texto_Defecto = oEsts.Data.Tables(0).Rows(0).Item("DEN_" & sIdi)
                                            End If
                                        End If
                                    End If
                                End If
                            Case TiposDeDatos.TipoCampoGS.EstadoInternoNoConf
                                If DBNullToSomething(oRow.Item("VALOR_NUM")) > 0 Then
                                    oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_NUM")
                                    oFSDSEntry.Texto_Defecto = mipage.Textos(oRow.Item("VALOR_NUM") + 4)
                                End If
                            Case TiposDeDatos.TipoCampoGS.Persona, TiposDeDatos.TipoCampoGS.Comprador
                                If DBNullToSomething(oRow.Item("VALOR_TEXT")) <> Nothing Then
                                    Dim oPersona As FSNServer.Persona = FSNServer.Get_Object(GetType(FSNServer.Persona))
                                    oPersona.LoadData(oRow.Item("VALOR_TEXT").ToString)
                                    oFSDSEntry.Texto_Defecto = oPersona.Denominacion(oFSDSEntry.TipoGS = TiposDeDatos.TipoCampoGS.Persona)
                                End If
                            Case TiposDeDatos.TipoCampoGS.UnidadOrganizativa
                                sUnidadOrganizativa = ""
                                If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                    bDepartamentoRelacionado = True
                                    sUnidadOrganizativa = oFSDSEntry.Valor_Defecto
                                    Dim arrUON() As String = oFSDSEntry.Valor_Defecto.split("-")
                                    Dim ilong = arrUON.Length
                                    Dim icount = 0
                                    Dim sUon0 As String
                                    Dim sUon1 As String
                                    Dim sUon2 As String
                                    Dim sUon3 As String
                                    sUon0 = ""
                                    sUon1 = ""
                                    sUon2 = ""
                                    sUon3 = ""

                                    While icount < ilong - 1
                                        Select Case icount
                                            Case 0
                                                sUon1 = arrUON(0)
                                            Case 1
                                                sUon2 = arrUON(1)
                                            Case 2
                                                sUon3 = arrUON(2)
                                        End Select
                                        icount = icount + 1
                                    End While
                                    oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                    oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT")
                                End If
                            Case TiposDeDatos.TipoCampoGS.Departamento
                                If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                    oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                    If (oFSDSEntry.Lista IsNot Nothing) Then
                                        oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))
                                        If oRowLista.Length > 0 Then _
                                            oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN")
                                    End If
                                End If
                                'Vaciamos el contenido porque s�lo lo necesit�bamos aqu� y har�a fallar la carga de datos desde ajax para los webdropdown 11.1
                                oFSDSEntry.Lista = Nothing
                            Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
                                If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                    'Mostrar la informacion referente a la Organizacion de Compras
                                    oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                    oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))

                                    oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                    oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN")
                                End If
                            Case TiposDeDatos.TipoCampoGS.Centro
                                If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                    'Mostrar la informacion referente al CENTRO
                                    oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                    If (oFSDSEntry.Lista IsNot Nothing) Then
                                        oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))
                                        If oRowLista.Length > 0 Then _
                                            oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN")
                                    End If
                                End If
                                'Vaciamos el contenido porque s�lo lo necesit�bamos aqu� y har�a fallar la carga de datos desde ajax para los webdropdown 11.1
                                oFSDSEntry.Lista = Nothing
                            Case TiposDeDatos.TipoCampoGS.Almacen, TiposDeDatos.TipoCampoGS.Empresa
                                If Not IsDBNull(oRow.Item("VALOR_NUM")) Then
                                    'Mostrar la informacion referente al ALMACEN
                                    oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_NUM")
                                    If (oFSDSEntry.Lista IsNot Nothing) Then
                                        oRowLista = oFSDSEntry.Lista.Tables(0).Select("ID=" + StrToSQLNULL(oRow.Item("VALOR_NUM").ToString))
                                        If oRowLista.Length > 0 Then _
                                            oFSDSEntry.Texto_Defecto = "(" & oRow.Item("VALOR_NUM") & ") - " & oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN")
                                    End If
                                End If
                                'Vaciamos el contenido porque s�lo lo necesit�bamos aqu� y har�a fallar la carga de datos desde ajax para los webdropdown 11.1
                                oFSDSEntry.Lista = Nothing
                            Case TiposDeDatos.TipoCampoGS.ProveedorAdj
                                If DBNullToSomething(oRow.Item("VALOR_TEXT")) <> Nothing Then
                                    Dim oProve As FSNServer.Proveedor = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                                    oProve.Cod = oRow.Item("VALOR_TEXT")
                                    oProve.Load(sIdi, True)
                                    oFSDSEntry.Texto_Defecto = oProve.Cod + " - " + oProve.Den.ToString.Replace("'", "")
                                End If
                        End Select

                        If oFSDSEntry.Tabla_Externa > 0 Then
                            If Not oFSDSEntry.Valor_Defecto Is Nothing Then
                                Dim oTablaExterna As FSNServer.TablaExterna = FSNServer.Get_Object(GetType(FSNServer.TablaExterna))
                                oTablaExterna.LoadDefTabla(oFSDSEntry.Tabla_Externa, False)
                                oTablaExterna.LoadData(False)
                                oFSDSEntry.Texto_Defecto = oTablaExterna.DescripcionReg(oFSDSEntry.Valor_Defecto, sIdi, oUser.DateFormat, oUser.NumberFormat)
                            End If
                        End If
                    End If

                    If iTipoGS = TiposDeDatos.TipoCampoGS.ImporteRepercutido Then
                        If Not IsDBNull(oRow.Item("VALOR_NUM")) Then
                            oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_NUM")
                            oFSDSEntry.Posee_Defecto = True
                        End If

                        oFSDSEntry.MonCentral = oRow.Item("MONCEN")
                        oFSDSEntry.CambioCentral = oRow.Item("CAMBIOCEN")
                    End If
                End If
                Lineas = Lineas + 1
            Loop
        Next
        'En el load hemos recuperado datos para las listas de provincia, departamento, centro y almac�n
        'Son campos dependientes de otros campos, cuyos datos se cargan en los webdropdown desde ajax pero necesit�bamos esa carga para
        'poder coger el valor por defecto en esta funci�n (si es que hay alguna l�nea por defecto). 
        'Es necesario vaciar posteriormente las listas porque la versi�n 11.1 del webdropdown carga mal los datos desde ajax si primero se cargan desde servidor
        'Dado que no podemos estar seguros de que se hayan vaciado, lo comprobamos de nuevo aqu�
        If oDs.Tables(0) IsNot Nothing _
        AndAlso oDs.Tables(0).Columns("ID") IsNot Nothing _
        AndAlso oDs.Tables(0).Columns("TIPO_CAMPO_GS") IsNot Nothing Then
            For Each oFila As DataRow In oDs.Tables(0).Rows
                Lineas = 0 'Para que siempre entre al menos una vez.
                Do While Lineas <= NumLineas
                    Try
                        If Lineas = 0 Then
                            oFSDSEntry = Me.tblDesglose.FindControl("fsentry" & oFila("ID"))
                        Else
                            oFSDSEntry = Me.tblDesglose.FindControl("fsdsentry_" & Lineas.ToString & "_" & oFila("ID"))
                        End If

                    Catch
                        oFSDSEntry = Nothing
                    End Try

                    If Not oFSDSEntry Is Nothing Then
                        Select Case oFila("TIPO_CAMPO_GS")
                            Case TiposDeDatos.TipoCampoGS.Provincia,
                                 TiposDeDatos.TipoCampoGS.Departamento,
                                 TiposDeDatos.TipoCampoGS.Centro,
                                 TiposDeDatos.TipoCampoGS.Almacen,
                                 TiposDeDatos.TipoCampoGS.Empresa
                                If oFSDSEntry.Lista IsNot Nothing AndAlso oFSDSEntry.MasterField <> String.Empty Then
                                    oFSDSEntry.Lista = Nothing
                                End If
                        End Select
                    End If
                    Lineas += 1
                Loop
            Next
        End If
    End Sub
    ''' <summary>
    ''' Escribe un campo para poner dicho valor en una etiqueta, en vez de un dataentry.
    ''' </summary>
    ''' <param name="oDSRow">Datarow con los valores del campo</param>
    ''' <returns>Una cadena con el valor y si es un enlace con el c�digo correspondiente</returns>
    ''' <remarks>Llamada desde: Page_Load(); Tiempo m�ximo: 0 sg.</remarks>
    Private Function EscribirValorCampo(ByVal oRow As DataRow, ByVal oDSRow As DataRow, ByVal oDS As DataSet) As String
        Dim sValor As String = String.Empty
        Dim oRowLista() As DataRow
        If DBNullToSomething(oRow.Item("INTRO")) = "1" AndAlso Not String.IsNullOrEmpty(oDSRow.Item("VALOR_TEXT").ToString) Then
            If Not String.IsNullOrEmpty(oDSRow.Item("VALOR_NUM").ToString) Then
                oRowLista = oDS.Tables(1).Select("ID = " + oDSRow.Item("CAMPO_HIJO").ToString + " and ORDEN=" + oDSRow.Item("VALOR_NUM").ToString)
            Else
                oRowLista = oDS.Tables(1).Select("ID = " + oDSRow.Item("CAMPO_HIJO").ToString + " and VALOR_TEXT_" & sIdi & " =" + StrToSQLNULL(oDSRow.Item("VALOR_TEXT").ToString))
            End If

            If oRowLista.Length > 0 Then
                sValor = oRowLista(0).Item("VALOR_TEXT_" & sIdi)
            End If
        End If

        '- Si es una TABLA EXTERNA ya sea de tipo string, num�rico o fecha se obtiene la denominaci�n de VALOR_TEXT_DEN
        '- Si es un CAMPO DEL SISTEMA tb se obtiene la denominaci�n de VALOR_TEXT_DEN
        If String.IsNullOrEmpty(sValor) AndAlso oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
            If oRow.Item("TIPO") = "6" Then
                If Not IsDBNull(oDSRow.Item("VALOR_TEXT_DEN")) Then
                    sValor = DevolverTablaExterna(oDSRow.Item("VALOR_TEXT_DEN"))
                End If
            ElseIf (Not IsDBNull(oDSRow.Item("TIPO_CAMPO_GS")) AndAlso (oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.ProveedorAdj OrElse oDSRow.Item("TIPO_CAMPO_GS") >= TiposDeDatos.TipoCampoGS.Proveedor) AndAlso oDSRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.DenArticulo AndAlso oDSRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo AndAlso oDSRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.CodArticulo) Then
                If Not IsDBNull(oDSRow.Item("VALOR_TEXT_DEN")) Then
                    If oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.PRES1 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres2 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres3 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres4 Then
                        sValor = DevolverPresupuestosConPorcentajes(oDSRow.Item("VALOR_TEXT"), oDSRow.Item("VALOR_TEXT_DEN"))
                    Else
                        sValor = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), sIdi)
                    End If
                    If oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.ProveedorAdj OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Proveedor OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.OrganizacionCompras OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Centro Then
                        sValor = oDSRow.Item("VALOR_TEXT") & " - " & sValor
                    ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Material Then
                        sValor = DevolverMaterialConCodigoUltimoNivel(oDSRow.Item("VALOR_TEXT"), sValor)
                    ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.CentroCoste Then
                        If InStrRev(oDSRow.Item("VALOR_TEXT"), "#") > 0 Then
                            sValor = Right(oDSRow.Item("VALOR_TEXT"), Len(oDSRow.Item("VALOR_TEXT")) - InStrRev(oDSRow.Item("VALOR_TEXT"), "#")) & " - " & sValor
                        Else
                            sValor = oDSRow.Item("VALOR_TEXT") & " - " & sValor
                        End If
                    ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Partida Then
                        If InStrRev(oDSRow.Item("VALOR_TEXT"), "#") > 0 Then
                            If InStrRev(oDSRow.Item("VALOR_TEXT"), "|") > 0 Then 'si no est� en el nivel 1, si no m�s abajo
                                sValor = Right(oDSRow.Item("VALOR_TEXT"), Len(oDSRow.Item("VALOR_TEXT")) - InStrRev(oDSRow.Item("VALOR_TEXT"), "|")) & " - " & sValor
                            Else
                                sValor = Right(oDSRow.Item("VALOR_TEXT"), Len(oDSRow.Item("VALOR_TEXT")) - InStrRev(oDSRow.Item("VALOR_TEXT"), "#")) & " - " & sValor
                            End If
                        Else
                            sValor = oDSRow.Item("VALOR_TEXT") & " - " & sValor
                        End If
                    ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Activo Then
                        sValor = oDSRow.Item("VALOR_TEXT") & " - " & sValor
                    End If
                End If
            End If
        End If
        If String.IsNullOrEmpty(sValor) Then 'por si el campo DEN est� vac�o. Todav�a no se guardan los datos en este campo.
            Select Case oRow.Item("SUBTIPO")
                Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoTextoLargo
                    sValor = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
                    If (oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio OrElse oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo) AndAlso Len(sValor) > 80 Then
                        sValor = Mid(sValor, 1, 80) & "..."
                    End If
                Case TiposDeDatos.TipoGeneral.TipoNumerico
                    If Not IsDBNull(oDSRow.Item("VALOR_NUM")) Then
                        sValor = QuitarDecimalSiTodoCeros(FSNLibrary.FormatNumber(oDSRow.Item("VALOR_NUM"), oUser.NumberFormat))
                    End If
                Case TiposDeDatos.TipoGeneral.TipoFecha
                    If IsDBNull(oDSRow.Item("VALOR_FEC")) Then
                        sValor = "&nbsp;"
                    Else
                        sValor = FormatDate(DevolverFechaRelativaA(DBNullToSomething(oDSRow.Item("VALOR_NUM")), DBNullToSomething(oDSRow.Item("VALOR_FEC"))), oUser.DateFormat)
                    End If
                Case TiposDeDatos.TipoGeneral.TipoBoolean
                    If Not IsDBNull(oDSRow.Item("VALOR_BOOL")) Then
                        sValor = IIf(oDSRow.Item("VALOR_BOOL") = 1, mipage.Textos(15), mipage.Textos(16))
                    End If
                Case TiposDeDatos.TipoGeneral.TipoArchivo
                    sAdjun = ""
                    idAdjun = ""
                    Dim oDSRowAdjun As System.Data.DataRow
                    For Each oDSRowAdjun In oDSRow.GetChildRows("REL_LINEA_ADJUNTO")
                        idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                        sAdjun += oDSRowAdjun.Item("NOM") + " (" + FSNLibrary.FormatNumber((oDSRowAdjun.Item("DATASIZE") / 1024), oUser.NumberFormat) + " Kb.), "
                    Next
                    If sAdjun <> "" Then
                        sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
                        idAdjun = idAdjun.Substring(0, idAdjun.Length - 2)
                    End If

                    If Len(sAdjun) > 90 Then
                        sValor = Mid(sAdjun, 1, 90) & "..."
                    Else
                        sValor = sAdjun
                    End If

                    If sAdjun <> "" Then
                        Dim enlace As String
                        If oDSRow.GetChildRows("REL_LINEA_ADJUNTO").Length > 1 Then
                            enlace = "<a href=""javascript:AbrirFicherosAdjuntos('" & idAdjun & "','" & oRow.Item("ID") & "','" & mlInstancia & "',3)"">" & sValor & "</a>"
                        Else
                            enlace = "<a href=""javascript:IrAAdjunto('" & ObtenerPathAdjunto(idAdjun, 3) & "')"">" & sValor & "</a>"
                        End If
                        sValor = enlace
                    End If
                Case TiposDeDatos.TipoGeneral.TipoDesglose
                    sValor = mipage.Textos(3)
            End Select
        End If

        If Trim(sValor) = "" Then sValor = "&nbsp;"

        EscribirValorCampo = sValor
    End Function

    Private Function CargarTextoYBotonEnTabla(ByVal oLbl As HtmlGenericControl, ByVal oImgPopUp As HyperLink) As HtmlTable
        'Para que los botones de pop-up en materiales, presupuestos, archivos,.. salgan alineados a la derecha
        Dim Tbl As System.Web.UI.HtmlControls.HtmlTable
        Dim Row As System.Web.UI.HtmlControls.HtmlTableRow
        Dim Cell As System.Web.UI.HtmlControls.HtmlTableCell

        Row = New System.Web.UI.HtmlControls.HtmlTableRow
        Cell = New System.Web.UI.HtmlControls.HtmlTableCell
        Cell.Controls.Add(oLbl)
        Cell.Align = "left"
        Cell.NoWrap = True
        Cell.Attributes("class") = "CampoSoloLectura"
        Row.Cells.Add(Cell)

        Cell = New System.Web.UI.HtmlControls.HtmlTableCell
        Cell.Controls.Add(oImgPopUp)
        Cell.Align = "right"
        Cell.Attributes("class") = "fsstyentrydd"
        Row.Cells.Add(Cell)

        Tbl = New System.Web.UI.HtmlControls.HtmlTable
        Tbl.Width = "100%"
        Tbl.CellPadding = 0
        Tbl.CellSpacing = 0
        Tbl.Rows.Add(Row)

        CargarTextoYBotonEnTabla = Tbl
    End Function
    Function QuitarDecimalSiTodoCeros(ByVal sValor As String) As String
        Dim decimales As String = ""
        If oUser.PrecisionFmt > 0 AndAlso Right(sValor, oUser.PrecisionFmt + 1) = oUser.DecimalFmt & decimales.PadRight(oUser.PrecisionFmt, "0") Then
            sValor = Left(sValor, Len(sValor) - oUser.PrecisionFmt - 1)
        End If
        QuitarDecimalSiTodoCeros = sValor
    End Function
    Private Function DevolverMaterialConCodigoUltimoNivel(ByVal sValorText As String, ByVal sValorTextDen As String) As String
        Dim sMat As String = DBNullToSomething(sValorText)
        Dim arrMat(4) As String
        Dim lLongCod As Integer
        Dim i As Integer

        For i = 1 To 4
            arrMat(i) = ""
        Next i

        i = 1
        While Trim(sMat) <> ""
            Select Case i
                Case 1
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                Case 2
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                Case 3
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                Case 4
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
            End Select
            arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
            sMat = Mid(sMat, lLongCod + 1)
            i = i + 1
        End While

        If arrMat(4) <> "" Then
            If Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles Then
                Return arrMat(1) + " - " + arrMat(2) + " - " + arrMat(3) + " - " + arrMat(4) + " - " + sValorTextDen
            Else
                Return arrMat(4) + " - " + sValorTextDen
            End If
        ElseIf arrMat(3) <> "" Then
            If Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles Then
                Return arrMat(1) + " - " + arrMat(2) + " - " + arrMat(3) + " - " + sValorTextDen
            Else
                Return arrMat(3) + " - " + sValorTextDen
            End If
        ElseIf arrMat(2) <> "" Then
            If Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles Then
                Return arrMat(1) + " - " + arrMat(2) + " - " + sValorTextDen
            Else
                Return arrMat(2) + " - " + sValorTextDen
            End If
        ElseIf arrMat(1) <> "" Then
            Return arrMat(1) + " - " + sValorTextDen
        Else
            Return String.Empty
        End If
    End Function
    Private Function DevolverPresupuestosConPorcentajes(ByVal sValorText As String, ByVal sValorTextDen As String) As String
        Dim arrPresupuestos() As String = sValorText.Split("#")
        Dim arrDenominaciones() As String = sValorTextDen.ToString().Split("#")
        Dim oPresup As String
        Dim iContadorPres As Integer
        Dim arrPresup(2) As String
        Dim dPorcent As Double
        Dim sValor As String = String.Empty

        iContadorPres = 0
        For Each oPresup In arrPresupuestos
            iContadorPres = iContadorPres + 1
            arrPresup = oPresup.Split("_")
            'Los presupuestos siempre llevan el porcentaje como 0.xx salvo cuando es 100% que llevan 1
            dPorcent = Numero(arrPresup(2), ".", "")
            sValor = arrDenominaciones(iContadorPres - 1) & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); " & sValor
        Next
        DevolverPresupuestosConPorcentajes = sValor
    End Function
    ''' <summary>
    ''' Devuelve el valor del campo tipo tabla externa extra�do del campo VALOR_TEXT_DEN. En funci�n del tipo (s: string, n: num�rico, d: fecha)
    ''' </summary>
    ''' <param name="sValorTextDen">Contenido del campo VALOR_TEXT_DEN</param>
    ''' <returns>El contenido en el formato correcto.</returns>
    ''' <remarks>LLamada desde: Page_Load; Tiempo m�ximo: 0,01 sg</remarks>    
    Private Function DevolverTablaExterna(ByVal sValorTextDen As String) As String
        Dim sValor As String = String.Empty
        Dim sTxt As String = sValorTextDen.ToString()
        Dim sTxt2 As String
        If sTxt.IndexOf("]#[") > 0 Then
            sTxt2 = sTxt.Substring(2, sTxt.IndexOf("]#[") - 2)
        Else
            sTxt2 = sTxt.Substring(2, sTxt.Length - 3)
        End If
        Select Case sTxt.Substring(0, 1)
            Case "s"
                sValor = sTxt2
            Case "n"
                sValor = Fullstep.FSNLibrary.FormatNumber(Numero(sTxt2, "."), oUser.NumberFormat)
            Case "d"
                sValor = CType(sTxt2, Date).ToString("d", oUser.DateFormat)
        End Select
        If sTxt.IndexOf("]#[") > 0 Then
            sValor = sValor & " "
            sTxt2 = sTxt.Substring(sTxt.IndexOf("]#[") + 3, sTxt.Length - sTxt.IndexOf("]#[") - 5)
            Select Case sTxt.Substring(sTxt.Length - 1, 1)
                Case "s"
                    sValor = sValor & sTxt2
                Case "n"
                    sValor = sValor & Fullstep.FSNLibrary.FormatNumber(Numero(sTxt2, "."), oUser.NumberFormat)
                Case "d"
                    sValor = sValor & CType(sTxt2, Date).ToString("d", oUser.DateFormat)
            End Select
        End If
        DevolverTablaExterna = sValor
    End Function
    ''' <summary>
    ''' Obtiene el path para descargar el fichero adjunto.
    ''' </summary>
    ''' <param name="Adjuntoid">Id del adjunto</param>
    ''' <param name="Tipo">Si es un campo normal o pertenece a un desglose</param>
    ''' <returns>Devuelve la ruta de la ventana que se va a abrir para descargar el fichero.</returns>
    ''' <remarks>Llamada desde: EscribirValorCampo; Tiempo m�ximo: 1 sg;</remarks>
    Private Function ObtenerPathAdjunto(ByVal Adjuntoid As Long, Optional ByVal Tipo As Integer = 1) As String
        Dim sPath As String
        Dim cAdjunto As FSNServer.Adjunto

        cAdjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
        cAdjunto.Id = Adjuntoid
        cAdjunto.LoadInstFromRequest(Tipo)
        If cAdjunto.DataSize = -1000 Then
            cAdjunto.LoadFromRequest(Tipo)
            sPath = cAdjunto.SaveAdjunToDisk(Tipo)
        Else
            sPath = cAdjunto.SaveAdjunToDisk(Tipo, True)
        End If

        Dim arrPath() As String = sPath.Split("\")
        ObtenerPathAdjunto = ConfigurationManager.AppSettings("rutaFS") & "_Common/download.aspx?path=" + arrPath(UBound(arrPath) - 1) + "&nombre=" + arrPath(UBound(arrPath)) + "&datasize=" + cAdjunto.dataSize.ToString
    End Function
    ''' <summary>
    ''' Uno popup donde has a�adido una linea con adjunto por defecto. Si no eliminas/reeemplazas el 
    ''' adjunto por defecto. La 2da vez q lo abres (sin salirte de la pantalla por eso es 2da vez) en 
    ''' page_load tienes en idAdjuntos el id de form_campo no de copia_linea_desglose_adjun y no sale 
    ''' el texto.
    ''' Los q estan en idAdjuntosNew no son un problema pq nunca son de form_campo 
    ''' </summary>
    ''' <param name="IdAdjuntos">Lista de Ids de copia_linea_desglose_adjun o form_campo</param>     
    ''' <returns>Numero de adjuntos a cargar en page_load vengan de form_campo o no</returns>
    ''' <remarks>Llamada desde: Page_load; Tiempo m�ximo: 0,1</remarks>
    Private Function DameNumAdjuntos(ByVal IdAdjuntos As String) As Integer
        Dim NAdjuntos As Integer
        NAdjuntos = 1

        While InStr(IdAdjuntos, ",")
            NAdjuntos = NAdjuntos + 1

            IdAdjuntos = Mid(IdAdjuntos, InStr(IdAdjuntos, ",") + 1)
        End While

        Return NAdjuntos
    End Function
    ''' Revisado por: blp. Fecha: 04/10/2011
    ''' <summary>
    ''' Carga la denominaci�n del combo
    ''' </summary>
    ''' <param name="Lista">Origen de datos</param>
    ''' <param name="ColCod">Columna por la q buscar</param>
    ''' <param name="Valor">Valor de codigo a buscar</param>
    ''' <param name="bTextoGuion">Si la denominaci�n es den � cod - den</param>
    ''' <param name="ColPais">Columna pais por la q buscar</param>
    ''' <param name="ValorPais">Valor de pais a buscar</param>
    ''' <returns>la denominaci�n del combo</returns>
    ''' <remarks>Llamada desde: Page_load; Tiempo m�ximo:0</remarks>
    Private Function TextoDelDropDown(ByVal Lista As DataSet, ByVal ColCod As String, ByVal Valor As String, ByVal bTextoGuion As Boolean, _
            Optional ByVal ColPais As String = "", Optional ByVal ValorPais As String = Nothing, Optional ByVal bPoblacion As Boolean = False) As String
        Dim oRowLista() As DataRow
        Dim Sql As String = ColCod & "=" & StrToSQLNULL(Valor)

        If ColPais <> "" Then
            Sql = ColPais & "=" & StrToSQLNULL(ValorPais) & " AND " & Sql
        End If

        If Lista IsNot Nothing AndAlso Lista.Tables(0).Rows.Count > 0 Then
            oRowLista = Lista.Tables(0).Select(Sql)
            If oRowLista.Length > 0 Then
                If bTextoGuion Then
                    If bPoblacion Then
                        Return oRowLista(0).Item("COD") & " - " & oRowLista(0).Item("DEN") & If(IsDBNull(oRowLista(0).Item("POB")), "", " (" & oRowLista(0).Item("POB") & ")")
                    Else
                        Return oRowLista(0).Item("COD") & " - " & oRowLista(0).Item("DEN")
                    End If
                Else
                    Return oRowLista(0).Item("DEN")
                End If
            Else
                Return String.Empty
            End If
        Else 'Ejemplo: Hay departamento pero no unidad organizativa???
            Return ""
        End If
    End Function
    ''' Revisado por: blp. Fecha: 04/10/2011
    ''' <summary>
    ''' Carga los datos por defecto de los combos relacionados para usarlos en las lineas a�adidas.
    ''' Por javascript se hace el create dropdown para hacerlo se necesita los posibles valores y los valores se obtienen una unica vez
    ''' y los mete en oFsEntry.Lista. 
    ''' As� q si, por ejemplo pais-provincia, en el oFsEntry.Lista de la provincia se cargan todas, las lineas a�adidas tendran todas
    ''' pero si en el oFsEntry.Lista se cargan las de el pais X, las lineas a�adidas seran las del pais X y si por defecto tienes marcado
    ''' pais X esos son los datos q debe tener el webdropdown.
    ''' </summary>
    ''' <param name="Datos">Datos del desglose leido de bbdd. Antes q los popup se actualizen.</param>
    ''' <param name="oCampo">objeto desglose</param> 
    ''' <param name="bQA">si es qa o no</param> 
    ''' <param name="sCodPaisDefecto">Pais por defecto en lineas a�adidas</param>
    ''' <param name="sCodOrgCompraDefecto">Organizaci�n de Compras por defecto en lineas a�adidas</param>
    ''' <param name="sCodCentroDefecto">Centr por defecto en lineas a�adidas</param>
    ''' <param name="sCodUnidadOrgDefecto">Unidad Organizativa por defecto en lineas a�adidas</param>
    ''' <remarks>Llamada desde: Page_Load; Tiempo maximo: 0</remarks>
    Private Sub CtrlHayDatosPorDefecto(ByVal Datos As DataSet, ByVal oCampo As FSNServer.Campo, ByVal bQA As Boolean _
                                         , ByRef sCodPaisDefecto As String, ByRef sCodOrgCompraDefecto As String, ByRef sCodCentroDefecto As String, ByRef sCodUnidadOrgDefecto As String)

        Dim DeDonde As Integer = 5
        Dim Lineas As Integer

        Try
            Lineas = Datos.Tables(DeDonde).Rows.Count
        Catch ex As Exception
            DeDonde = 2

            If bQA Then Datos = oCampo.LoadDesglose(sIdi, oCampo.IdSolicitud, , , , oUser.DateFmt, , )
        End Try

        sCodPaisDefecto = ""
        sCodOrgCompraDefecto = ""
        sCodCentroDefecto = ""
        sCodUnidadOrgDefecto = ""

        For Each oRow In Datos.Tables(DeDonde).Rows
            If DBNullToSomething(oRow.Item("LINEA")) = 1 Then
                If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                    Select Case DBNullToInteger(oRow.Item("TIPO_CAMPO_GS"))
                        Case TiposDeDatos.TipoCampoGS.Pais
                            sCodPaisDefecto = oRow.Item("VALOR_TEXT")
                        Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
                            sCodOrgCompraDefecto = oRow.Item("VALOR_TEXT")
                        Case TiposDeDatos.TipoCampoGS.Centro
                            sCodCentroDefecto = oRow.Item("VALOR_TEXT")
                        Case TiposDeDatos.TipoCampoGS.UnidadOrganizativa
                            sCodUnidadOrgDefecto = oRow.Item("VALOR_TEXT")
                    End Select
                End If
            Else
                Exit For
            End If
        Next
    End Sub
    Public ReadOnly Property InstanciaImportar() As Long
        Get
            If Not Request("IdInstanciaImportar") Is Nothing  AndAlso Request("IdInstanciaImportar") <> "undefined" Then
                Return Request("IdInstanciaImportar")
            Else
                Return 0
            End If
        End Get
    End Property
End Class