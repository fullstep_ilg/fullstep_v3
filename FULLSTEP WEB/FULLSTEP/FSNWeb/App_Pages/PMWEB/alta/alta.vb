
Namespace CommonAlta
    Module alta

        ''' <summary>
        ''' Carga el componente webCalendar.
        ''' </summary>
        ''' <param name="sCulture">Ref cultural</param>       
        ''' <returns>Devuelve un componente de tipo WebCalendar configurado con el idioma del usuario</returns>
        ''' <remarks>Llamada desde=  GuardarInstancia.vb --> TrasladoUsu //NWAlta.aspx.vb -->Page_load
        '''                          alta.aspx.vb --> Page_load          //altaCertificado.aspx.vb--> Page_load
        '''                          DetalleCertificado.aspx.vb          //filtroCertificados.aspx.vb--> CargarCertificados
        '''                          AltaNoConformidad.aspx.vb           //NoConformidad.aspx.vb--> Page_load
        '''                          NoConformidadRevisor.aspx.vb        //NwDetalleSolicitud.aspx.vb --> Page_load
        '''                          NWSeguimiento.aspx.vb -->Page_load  // NWWorkflow.aspx.vb -->Page_load
        '''                          
        '''                          
        '''                          
        ''' ; Tiempo m�ximo=0seg.</remarks>
        Public Function InsertarCalendario() As Infragistics.WebUI.WebSchedule.WebCalendar
            Dim SharedCalendar As New Infragistics.WebUI.WebSchedule.WebCalendar

            With SharedCalendar
                .Style("display") = "none"
                .Style("z-index") = "-1"
                .ClientSideEvents.InitializeCalendar = "initCalendarEvent"
                .ClientSideEvents.DateClicked = "calendarDateClickedEvent"
                With .Layout
                    .NextMonthImageUrl = "ig_cal_grayN.gif"
                    .ShowTitle = "False"
                    .PrevMonthImageUrl = "ig_cal_grayP.gif"
                    '.FooterFormat = "Today: {0:d}"
                    .FooterFormat = ""
                    .FooterStyle.CssClass = "igCalendarFooterStyle"
                    .TodayDayStyle.CssClass = "igCalendarSelectedDayStyle"
                    .OtherMonthDayStyle.CssClass = "igCalendarOtherMonthDayStyle"
                    .CalendarStyle.CssClass = "igCalendarStyle"
                    .TodayDayStyle.CssClass = "igCalendarTodayDayStyle"
                    .DayHeaderStyle.CssClass = "igCalendarDayHeaderStyle"
                    .TitleStyle.CssClass = "igCalendarTitleStyle"
                End With
            End With
            Return SharedCalendar
        End Function


        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Crea el dataset para un combo de tipo boolean
        ''' </summary>
        ''' <param name="sIdi">Idioma en que tienen que ir los textos</param>
        ''' <returns>Dataset con una tabla que contiene los valores del combo booleano en el idioma solicitado</returns>
        ''' <remarks>Llamada desde campos.ascx.vb y desglose.ascx.vb. Tiempo m�x inferior a 0,1 seg</remarks>
        Public Function CrearDataset(ByVal sIdi As String, ByVal sTextoSi As String, ByVal sTextoNo As String) As DataSet
            Dim ds As New DataSet
            Dim oRow(1) As Object

            oRow(0) = "1"
            oRow(1) = sTextoSi
            ds.Tables.Add("BOOL")

            ds.Tables("BOOL").Columns.Add("value", System.Type.GetType("System.String"))
            ds.Tables("BOOL").Columns.Add("TEXT_" & sIdi, System.Type.GetType("System.String"))
            ds.Tables("BOOL").Rows.Add(oRow)

            oRow(0) = "0"
            oRow(1) = sTextoNo
            ds.Tables("BOOL").Rows.Add(oRow)
            Return ds

        End Function

    End Module

End Namespace
