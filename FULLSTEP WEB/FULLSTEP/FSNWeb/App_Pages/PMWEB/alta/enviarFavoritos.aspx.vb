

Public Class enviarFavoritos
    Inherits FSNPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents cmdCancelar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdContinuar As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents txtDenFavorita As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblTexto As System.Web.UI.WebControls.Label
    Protected WithEvents sMensaje1 As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim FSWSServer As FSNServer.Root = Session("FSN_Server")
        Dim oUser As FSNServer.User = Session("FSN_User")
        Dim tipoSolicitud As Integer = 0

        Dim sIdi As String = oUser.Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If
        Dim oDict As FSNServer.Dictionary = FSWSServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.EnviarFavoritos, sIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        If Request("TipoSolicitud") <> Nothing Then tipoSolicitud = CInt(Request("TipoSolicitud"))
        lblTitulo.Text = oTextos.Rows(1).Item(1)

        cmdContinuar.Value = oTextos.Rows(3).Item(1)
        cmdCancelar.Value = oTextos.Rows(4).Item(1)
        If tipoSolicitud = TiposDeDatos.TipoDeSolicitud.Factura Then
            lblTexto.Text = oTextos.Rows(6).Item(1)
            sMensaje1.Value = oTextos.Rows(7).Item(1)
        Else
            lblTexto.Text = oTextos.Rows(2).Item(1)
            sMensaje1.Value = oTextos.Rows(5).Item(1)
        End If

    End Sub

End Class


