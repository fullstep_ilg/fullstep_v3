﻿Public Partial Class altaTarea
    Inherits FSNPage

    Dim Edicion As Byte
    Dim msObligatorio As String
    Dim oProyecto As FSNServer.Proyecto

    Dim DenomFases As New Dictionary(Of String, String)
    Dim Hito As String
    Dim iNumFases As Byte
    Dim iMaxLinea As Short
    Private sIdioma As String = ConfigurationManager.AppSettings("idioma")

    ''' <summary>
    ''' Carga la pagina
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Edicion = CInt(Request("Editar"))
        Hito = Request("Hito")

        oProyecto = FSNServer.Get_Object(GetType(FSNServer.Proyecto))

        CargarDenominacionFases()
        ConfigurarControles()
        CargarEstadosTarea()
        CargarDatos()

        'Cargo el calendario para Fec.Inicio y Fec.Fin
        Me.FindControl("form1").Controls.Add(FSNWeb.CommonAlta.InsertarCalendario())
    End Sub
    ''' <summary>
    ''' Carga los estados en el combo de estados
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarEstadosTarea()
        Dim dtEstados As DataTable
        dtEstados = oProyecto.DevolverEstadosTareaDeProyecto(sIdioma)
        wddEstado.DataSource = dtEstados
        wddEstado.ValueField = "ID"
        wddEstado.TextField = "DEN"

        wddEstado.DataBind()

        For i = 0 To wddEstado.Items.Count - 1
            If dtEstados(i)(2) = True Then
                wddEstado.SelectedItemIndex = i
                Exit For
            End If
        Next

    End Sub

    ''' <summary>
    ''' Procedimiento que carga las denominaciones de las distintas fases del proyecto
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarDenominacionFases()
        Try
            Dim dtFases As DataTable


            oProyecto = FSNServer.Get_Object(GetType(FSNServer.Proyecto))

            dtFases = oProyecto.DevolverDenominacionFases(sIdioma)
            DenomFases.Clear()
            For Each dr As DataRow In dtFases.Rows
                Select Case dr("ID")
                    Case "39"
                        DenomFases.Add("NIVEL1", dr("DEN"))
                    Case "40"
                        DenomFases.Add("NIVEL2", dr("DEN"))
                    Case "41"
                        DenomFases.Add("NIVEL3", dr("DEN"))
                    Case "42"
                        DenomFases.Add("NIVEL4", dr("DEN"))
                    Case "43"
                        DenomFases.Add("TAREA", dr("DEN"))
                End Select
            Next

        Catch ex As Exception

        End Try
    End Sub
    ''' <summary>
    ''' Configuramos los controles segun sea necesario, visibilidad...maxlength...idioma
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ConfigurarControles()



        iNumFases = DBNullToInteger(Request("NumFases"))
        hid_NomGrid.Value = Request("NomGrid")
        hid_linea_grid.Value = Request("linea")
        hid_Obligatorio.Value = Request("Obligatorio")
        hid_NumFases.Value = DBNullToInteger(Request("NumFases"))
        hid_NomElimFilas.Value = Request("ElimFilas")

        Dim FSWServer As FSNServer.Root
        FSWServer = Session("FSN_Server")

        Select Case iNumFases
            Case 1
                filaNiv1.Visible = True
                lblNivel1.Text = DenomFases("NIVEL1") & "(*):"
                filaNiv2.Visible = False
                filaNiv3.Visible = False
                filaNiv4.Visible = False
            Case 2
                filaNiv1.Visible = True
                filaNiv2.Visible = True
                lblNivel1.Text = DenomFases("NIVEL1") & "(*):"
                lblNivel2.Text = DenomFases("NIVEL2") & "(*):"
                filaNiv3.Visible = False
                filaNiv4.Visible = False
            Case 3
                filaNiv1.Visible = True
                filaNiv2.Visible = True
                filaNiv3.Visible = True
                lblNivel1.Text = DenomFases("NIVEL1") & "(*):"
                lblNivel2.Text = DenomFases("NIVEL2") & "(*):"
                lblNivel3.Text = DenomFases("NIVEL3") & "(*):"
                filaNiv4.Visible = False
            Case 4
                filaNiv1.Visible = True
                filaNiv2.Visible = True
                filaNiv3.Visible = True
                filaNiv4.Visible = True
                lblNivel1.Text = DenomFases("NIVEL1") & "(*):"
                lblNivel2.Text = DenomFases("NIVEL2") & "(*):"
                lblNivel3.Text = DenomFases("NIVEL3") & "(*):"
                lblNivel4.Text = DenomFases("NIVEL4") & "(*):"
        End Select


        'longitud permitida de cajas de texto
        txtCodNivel1.MaxLength = FSWServer.LongitudesDeCodigos.giLongCodFSGA_CAT_TARE_COD
        txtDescNivel1.MaxLength = FSWServer.LongitudesDeCodigos.giLongCodFSGA_CAT_TARE_DEN
        txtCodNivel2.MaxLength = FSWServer.LongitudesDeCodigos.giLongCodFSGA_CAT_TARE_COD
        txtDescNivel2.MaxLength = FSWServer.LongitudesDeCodigos.giLongCodFSGA_CAT_TARE_DEN
        txtCodNivel3.MaxLength = FSWServer.LongitudesDeCodigos.giLongCodFSGA_CAT_TARE_COD
        txtDescNivel3.MaxLength = FSWServer.LongitudesDeCodigos.giLongCodFSGA_CAT_TARE_DEN
        txtCodNivel4.MaxLength = FSWServer.LongitudesDeCodigos.giLongCodFSGA_CAT_TARE_COD
        txtDescNivel4.MaxLength = FSWServer.LongitudesDeCodigos.giLongCodFSGA_CAT_TARE_DEN
        txtCodTarea.MaxLength = FSWServer.LongitudesDeCodigos.giLongCodFSGA_TARE_COD
        txtDescTarea.MaxLength = FSWServer.LongitudesDeCodigos.giLongCodFSGA_TARE_DEN
        txtObs.MaxLength = FSWServer.LongitudesDeCodigos.giLongCodFSGA_TARE_OBS

        'Idioma
        Dim oDict As FSNServer.Dictionary = FSWServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.DesgloseActividad, Idioma)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        Me.lblTarea.Text = DenomFases("TAREA") & "(*)" & ":"
        Me.lblObs.Text = oTextos.Rows(0)(1) & ":"
        Me.lblFecInicio.Text = IIf(hid_Obligatorio.Value = "1", oTextos.Rows(1)(1) & "(*)", oTextos.Rows(1)(1)) & ":"
        Me.lblFecFin.Text = IIf(hid_Obligatorio.Value = "1", oTextos.Rows(2)(1) & "(*)", oTextos.Rows(2)(1)) & ":"
        Me.lblHorasEst.Text = IIf(hid_Obligatorio.Value = "1", oTextos.Rows(3)(1) & "(*)", oTextos.Rows(3)(1)) & ":"
        Me.lblNumLinea.Text = oTextos.Rows(4)(1) & "(*)" & ":"
        Me.lblGradoAvance.Text = oTextos.Rows(5)(1) & ":"
        Me.lblEstado.Text = oTextos.Rows(6)(1) & ":"
        Me.chkHito.Text = oTextos.Rows(14)(1)
        Me.btnAceptar.Text = oTextos.Rows(9)(1)
        Me.btnCancelar.Text = oTextos.Rows(10)(1)


        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptCamposOblig") Then
            Dim sScript = vbCrLf & "var sCamposOblig='" & oTextos.Rows(20)(1) & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sOblig", sScript, True)
        End If
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptFechas") Then
            Dim sScript = vbCrLf & "var sFechas='" & oTextos.Rows(33)(1) & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sFec", sScript, True)
        End If
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptTextos") Then
            Dim sScript = vbCrLf & "var sTextos='" & oTextos.Rows(30)(1) & "#" & oTextos.Rows(22)(1) & "#" & oTextos.Rows(14)(1) & "#" & oTextos.Rows(31)(1) & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sText", sScript, True)
        End If
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "scriptRutaPM") Then
            Dim sScript = vbCrLf & "var sRutaPM='" & ConfigurationManager.AppSettings("rutaPM") & "';" & vbCrLf
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "sRutPM", sScript, True)
        End If


    End Sub
    ''' <summary>
    ''' Carga los datos en pantalla
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarDatos()
        If Edicion = 0 Then
            'Si es alta
            iMaxLinea = DBNullToInteger(Request("MaxFila"))
            Me.txtNumLinea.Text = iMaxLinea + 1
            Me.txtGradoAvance.Text = 0
            hid_Edicion.Value = 0
            hid_linea.Value = iMaxLinea + 1
        Else
            'Se entra en modo edicion
            Me.chkHito.Enabled = False
            hid_linea.Value = Request("num")
            hid_Edicion.Value = 1

            If Hito = "1" Then
                chkHito.Checked = True
                CeldaFechaFin.Visible = False
                lblFecFin.Visible = False
                filaHorayAvance.Visible = False
            End If

            Select Case iNumFases
                Case 1
                    txtCodNivel1.Text = DBNullToStr(Request("codfase1"))
                    txtDescNivel1.Text = DBNullToStr(Request("fase1"))
                    lblNivel1.Text = DenomFases("NIVEL1") & "(*)" & ":"
                Case 2
                    txtCodNivel1.Text = DBNullToStr(Request("codfase1"))
                    txtDescNivel1.Text = DBNullToStr(Request("fase1"))
                    lblNivel1.Text = DenomFases("NIVEL1") & "(*)" & ":"
                    txtCodNivel2.Text = DBNullToStr(Request("codfase2"))
                    txtDescNivel2.Text = DBNullToStr(Request("fase2"))
                    lblNivel2.Text = DenomFases("NIVEL2") & "(*)" & ":"
                Case 3
                    txtCodNivel1.Text = DBNullToStr(Request("codfase1"))
                    txtDescNivel1.Text = DBNullToStr(Request("fase1"))
                    lblNivel1.Text = DenomFases("NIVEL1") & "(*)" & ":"
                    txtCodNivel2.Text = DBNullToStr(Request("codfase2"))
                    txtDescNivel2.Text = DBNullToStr(Request("fase2"))
                    lblNivel2.Text = DenomFases("NIVEL2") & "(*)" & ":"
                    txtCodNivel3.Text = DBNullToStr(Request("codfase3"))
                    txtDescNivel3.Text = DBNullToStr(Request("fase3"))
                    lblNivel3.Text = DenomFases("NIVEL3") & "(*)" & ":"
                Case 4
                    txtCodNivel1.Text = DBNullToStr(Request("codfase1"))
                    txtDescNivel1.Text = DBNullToStr(Request("fase1"))
                    lblNivel1.Text = DenomFases("NIVEL1") & "(*)" & ":"
                    txtCodNivel2.Text = DBNullToStr(Request("codfase2"))
                    txtDescNivel2.Text = DBNullToStr(Request("fase2"))
                    lblNivel2.Text = DenomFases("NIVEL2") & "(*)" & ":"
                    txtCodNivel3.Text = DBNullToStr(Request("codfase3"))
                    txtDescNivel3.Text = DBNullToStr(Request("fase3"))
                    lblNivel3.Text = DenomFases("NIVEL3") & "(*)" & ":"
                    txtCodNivel4.Text = DBNullToStr(Request("codfase4"))
                    txtDescNivel4.Text = DBNullToStr(Request("fase4"))
                    lblNivel4.Text = DenomFases("NIVEL4") & "(*)" & ":"
            End Select

            Dim supportedFormats() As String = New String() {"MM/dd/yyyy", "MM/dd/yy", "dd/MM/yyyy", "dd/MM/yy"}

            Me.txtCodTarea.Text = DBNullToStr(Request("codtarea"))
            Me.txtDescTarea.Text = DBNullToStr(Request("tarea"))
            Dim fec As Date
            Try
                fec = DateTime.ParseExact(Request("fecFin"), supportedFormats, System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None)
                Me.dteFechaFin.Valor = fec

            Catch ex As Exception
                Me.dteFechaFin.Valor = ""

            End Try

            Try
                fec = DateTime.ParseExact(Request("fecInicio"), supportedFormats, System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None)
                Me.dteFechaInicio.Valor = fec

            Catch ex As Exception
                Me.dteFechaInicio.Valor = ""

            End Try
           
            
            If Not Request("codEstado") Is Nothing And Not Request("codEstado") = "null" Then
                For i = 0 To wddEstado.Items.Count - 1
                    If wddEstado.Items(i).Value = CInt(Request("codEstado")) Then
                        wddEstado.SelectedItemIndex = i
                        Exit For
                    End If
                Next
            Else

            End If
            
            If IsNumeric(Request("gradoAvance")) Then
                Me.txtGradoAvance.Text = DBNullToInteger(Request("gradoAvance"))
            End If
            If IsNumeric(Request("horasAsig")) Then
                Me.txtHorasEst.Text = DBNullToInteger(Request("horasAsig"))
            End If
            Me.txtNumLinea.Text = DBNullToInteger(Request("num"))
            Me.txtObs.Text = DBNullToStr(Request("obs"))

            End If
    End Sub
    ''' <summary>
    ''' evento que se dispara al cambiar de estado el check de hito
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub chkHito_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkHito.CheckedChanged
        If chkHito.Checked Then
            Me.dteFechaFin.Visible = False

            Me.lblFecFin.Visible = False
            Me.filaHorayAvance.Visible = False
        Else
            Me.dteFechaFin.Visible = True

            Me.lblFecFin.Visible = True
            Me.filaHorayAvance.Visible = True
        End If
    End Sub
End Class