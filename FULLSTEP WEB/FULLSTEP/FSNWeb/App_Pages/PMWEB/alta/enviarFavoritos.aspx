<%@ Page Language="vb" AutoEventWireup="false" Codebehind="enviarFavoritos.aspx.vb" Inherits="Fullstep.FSNWeb.enviarFavoritos"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head runat="server">
		<title></title>
		<meta name="vs_showGrid" content="True">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<script type="text/javascript">
		/*
		Descripcion: Guarda la solicitud favorita con el nombre introducido
		*/
		function enviarFavoritos () {
			denFavorita = document.getElementById("txtDenFavorita").value;
			
			if (denFavorita != '')  {
					
				p = window.opener;
				if (p) {
					
					p.document.forms["frmSubmitFav"].elements["denFavorita"].value = denFavorita;
					p.guardarFavoritos()
				}
				
				window.close();
			}else
				alert(window.document.forms["Form1"].elements["sMensaje1"].value);
	
		}

		/*
		Descripcion: Pone el foco en la caja de texto para escribir el nombre de la solicitud favorita
		*/
		function init() {
			if (window.document.forms["Form1"].elements["txtDenFavorita"])
				window.document.forms["Form1"].elements["txtDenFavorita"].focus();
		}
	</script>	
	<body onload="init()">
		<form id="Form1" method="post" runat="server">
			<input type="hidden" name="sMensaje1" id="sMensaje1" runat="server"><img src="images/favoritos_add.gif" style="Z-INDEX: 102; LEFT: 16px; POSITION: absolute; TOP: 4px" />
			<asp:Label id="lblTitulo" style="Z-INDEX: 102; LEFT: 56px; POSITION: absolute; TOP: 16px" runat="server"
				CssClass="tituloDarkBlue">Env�o de datos a secci�n de favoritos</asp:Label>
			<HR id="barra" style="Z-INDEX: 106; LEFT: 16px; POSITION: absolute; TOP: 40px" width="95%"
				color="gainsboro" noShade SIZE="3">
			<TABLE id="tblInf" style="Z-INDEX: 101; LEFT: 40px; POSITION: absolute; TOP: 56px; HEIGHT: 144px"
				cellSpacing="1" cellPadding="1" width="300" border="0">
				<TR>
					<TD colspan="2" style="HEIGHT: 37px"><asp:Label id="lblTexto" runat="server" Width="320px" Font-Size="12px" Font-Names="verdana">Introduzca un nombre para identificar la solicitud:</asp:Label></TD>
				</TR>
				<TR>
					<TD colspan="2" style="HEIGHT: 39px"><asp:TextBox id="txtDenFavorita" runat="server" Width="100%" CssClass="INPUT" MaxLength="100"></asp:TextBox></TD>
				</TR>
				<TR align="center">
					<TD width="50%"><INPUT class="botonPMWEB" id="cmdContinuar" type="button" value="cmdContinuar" name="cmdContinuar"
							onclick="enviarFavoritos()" runat="server"></TD>
					<TD width="50%"><INPUT class="botonPMWEB" id="cmdCancelar" onclick="window.close();" type="button" value="cmdCancelar"
							name="cmdCancelar" runat="server"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</html>
