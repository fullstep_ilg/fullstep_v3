Imports Fullstep.FSNServer
Public Class campos
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tblDesglose As System.Web.UI.WebControls.Table
    Protected WithEvents tblGeneral As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents cmdOk As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents cmdCancel As System.Web.UI.HtmlControls.HtmlInputButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private mdsCampos As DataSet
    Private msIdi As String
    Private moTextos As DataTable
    Private mlId As Long
    Private msTabContainer As String
    Private mlInstancia As Long
    Private mlSolicitud As Long
    Private mTipoSolicitud As TiposDeDatos.TipoDeSolicitud
    Private mlVersion As Long
    Private mbSoloLectura As Boolean
    Private mbFuerzaDesgloseLectura As Boolean
    Private msProveedor As String
    Private moAcciones As DataTable
    Private moDSEstados As DataSet
    Private mlInstanciaEstado As Long
    Private msInstanciaMoneda As String
    Private mbObservador As Boolean
    Private msNombreContrato As String = ""
    Private msTamanyoContrato As String = ""
    Private mlIdContrato As Long
    Private mlIdArchivoContrato As Long
    Private oUser As FSNServer.User
    Private FSNServer As FSNServer.Root
    Private oDict As FSNServer.Dictionary
    Private oTextos As DataTable
    Private sAdjun As String
    Private idAdjun As String
    Private bPM As Boolean
    Private _Instancia As Instancia
    Private _Formulario As Formulario
    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
    Private Const IncludeScriptFormatCal As String = ControlChars.CrLf & "<script type=""{0}"" src=""{1}{2}""></script>"

    Dim pages As System.Web.Configuration.PagesSection = ConfigurationManager.GetSection("system.web/pages")
    Dim theme As String = pages.Theme
    Dim sIdProveedor As String
    Property Instancia() As Long
        Get
            Return mlInstancia
        End Get
        Set(ByVal Value As Long)
            mlInstancia = Value
        End Set
    End Property
    Property Proveedor() As String
        Get
            Return msProveedor
        End Get
        Set(ByVal Value As String)
            msProveedor = Value
        End Set
    End Property
    Property Solicitud() As Long
        Get
            Return mlSolicitud
        End Get
        Set(ByVal Value As Long)
            mlSolicitud = Value
        End Set
    End Property
    Property TipoSolicitud As TiposDeDatos.TipoDeSolicitud
        Get
            Return mTipoSolicitud
        End Get
        Set(ByVal Value As TiposDeDatos.TipoDeSolicitud)
            mTipoSolicitud = Value
        End Set
    End Property
    Private mEnCursoDeAprobacion As Boolean
    Public Property EnCursoDeAprobacion() As Boolean
        Get
            Return mEnCursoDeAprobacion
        End Get
        Set(ByVal value As Boolean)
            mEnCursoDeAprobacion = value
        End Set
    End Property
    Property Version() As Long
        Get
            Return mlVersion
        End Get
        Set(ByVal Value As Long)
            mlVersion = Value
        End Set
    End Property
    Property dsCampos() As DataSet
        Get
            dsCampos = mdsCampos
        End Get
        Set(ByVal Value As DataSet)
            mdsCampos = Value
        End Set
    End Property
    Property Idi() As String
        Get
            Idi = msIdi
        End Get
        Set(ByVal Value As String)
            msIdi = Value
        End Set
    End Property
    Property TabContainer() As String
        Get
            TabContainer = msTabContainer
        End Get
        Set(ByVal Value As String)
            msTabContainer = Value
        End Set
    End Property
    Property IdGrupo() As Long
        Get
            IdGrupo = mlId
        End Get
        Set(ByVal Value As Long)
            mlId = Value
        End Set
    End Property
    Property Textos() As DataTable
        Get
            Textos = moTextos
        End Get
        Set(ByVal Value As DataTable)
            moTextos = Value
        End Set
    End Property
    Property SoloLectura() As Boolean
        Get
            Return mbSoloLectura
        End Get
        Set(ByVal Value As Boolean)
            mbSoloLectura = Value
        End Set
    End Property
    Property FuerzaDesgloseLectura() As Boolean
        Get
            Return mbFuerzaDesgloseLectura
        End Get
        Set(ByVal Value As Boolean)
            mbFuerzaDesgloseLectura = Value
        End Set
    End Property
    Property Acciones() As DataTable
        Get
            Return moAcciones
        End Get
        Set(ByVal Value As DataTable)
            moAcciones = Value
        End Set
    End Property
    Property DSEstados() As DataSet
        Get
            Return moDSEstados
        End Get
        Set(ByVal Value As DataSet)
            moDSEstados = Value
        End Set
    End Property
    Property InstanciaEstado() As Long
        Get
            Return mlInstanciaEstado
        End Get
        Set(ByVal Value As Long)
            mlInstanciaEstado = Value
        End Set
    End Property
    Property InstanciaMoneda() As String
        Get
            Return msInstanciaMoneda
        End Get
        Set(ByVal Value As String)
            msInstanciaMoneda = Value
        End Set
    End Property
    Property PM() As Boolean
        Get
            Return bPM
        End Get
        Set(ByVal Value As Boolean)
            bPM = Value
        End Set
    End Property
    Property Observador() As Boolean
        Get
            Return mbObservador
        End Get
        Set(ByVal Value As Boolean)
            mbObservador = Value
        End Set
    End Property
    Property NombreContrato() As String
        Get
            NombreContrato = msNombreContrato
        End Get
        Set(ByVal Value As String)
            msNombreContrato = Value
        End Set
    End Property
    Property TamanyoContrato() As String
        Get
            TamanyoContrato = msTamanyoContrato
        End Get
        Set(ByVal Value As String)
            msTamanyoContrato = Value
        End Set
    End Property
    Property IdContrato() As Long
        Get
            IdContrato = mlIdContrato
        End Get
        Set(ByVal Value As Long)
            mlIdContrato = Value
        End Set
    End Property
    Property IdArchivoContrato() As Long
        Get
            IdArchivoContrato = mlIdArchivoContrato
        End Get
        Set(ByVal Value As Long)
            mlIdArchivoContrato = Value
        End Set
    End Property
    Property Formulario As Formulario
        Get
            Return _Formulario
        End Get
        Set(value As Formulario)
            _Formulario = value
        End Set
    End Property
    Property ObjInstancia As Instancia
        Get
            Return _Instancia
        End Get
        Set(value As Instancia)
            _Instancia = value
        End Set
    End Property
    Public Sub New(Optional ByVal lId As Long = Nothing, Optional ByRef oDS As DataSet = Nothing, Optional ByVal sIdi As String = Nothing)
        If lId <> Nothing Then
            mdsCampos = oDS
            msIdi = sIdi
            mlId = lId
        End If
    End Sub
    ''' Revisado por: blp. Fecha: 04/10/2011
    ''' <summary>
    '''     Evento de sistema, carga de pantalla.
    '''     NOTA: Ahora aparte del dataentry este tambien inserta el javascript para el datecalendar, pq?
    '''     Pq si resulta q no hay ni un solo entry no se cargaba ese javascript y todos los Fullstep.
    '''     PMWeb.CommonAlta.InsertarCalendario() no funcionaban y dejaban visible el calendario. 
    '''     NOTA: Ahora en 2008 los name q no creamos explicitamente usan $ para separar los trozos de los 
    '''     diferentes controles del q es hijo (ejemplo uwtGrupos$_ctl0x$128903$fsentry646101$_4__646180),
    '''     antes en 2003 se usaba _ y por ello resultaba q el id y el name eran iguales. Esto importa pq 
    '''     los request se hacen por nombre no por id. En 2003 se le pasaba el id y funcionaba, pero ya no.
    ''' </summary>
    ''' <param name="sender">parametro de sistema</param>
    ''' <param name="e">parametro de sistema</param>               
    ''' <remarks>Llamada desde: altacertificado.aspx    detallecertificado.aspx  altanoconformidad.aspx    detallenoconformidad.aspx
    '''     noconformidadrevisor.aspx   detallesolicitud.aspx   nwdetallesolicitud.aspx     aprobacion.aspx     gestioninstancia.aspx
    '''     gestiontrasladada.aspx      instanciaasignada.aspx  NWgestionInstancia.aspx;Tiempo m�ximo: 0</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oLbl As System.Web.UI.HtmlControls.HtmlGenericControl
        Dim otblRow As System.Web.UI.WebControls.TableRow
        Dim otblCell As System.Web.UI.WebControls.TableCell
        Dim oDSRow, oDSRowAdjun As System.Data.DataRow
        Dim olabel As System.Web.UI.WebControls.Label
        Dim oimg As System.Web.UI.WebControls.HyperLink
        Dim oFSEntry As DataEntry.GeneralEntry
        Dim sClase, sAdjun, sCodPais, sKeyMaterial, sKeyArticulo, sKeyPais, sKeyProvincia, sKeyDestino, sKeyAlmacen, sKeyFPago, sKeyCodArt As String
        Dim sValor, sTexto, sDenominacion, sToolTip, sUnidadOrganizativa, sCodOrgCompras, sKeyOrgCompras, sCodCentro, sIDCampoOrgCompras, sidDataEntryOrgCompras, sIDCampoPartida As String
        Dim sIDCentro, sIdUnidadOrganizativa, sidDataEntryUnidadOrganizativa, sIdDataEntryCentroCoste, sIdDataEntryCentro, sIdCampoCentro, sKeyUnidadOrganizativa, sKeyOrgComprasDataCheck, sKeyCentro, sIdDataentryMaterial As String
        Dim idEntryPROV, idEntryPREC, idEntryCC, idEntryPartidaPlurianual As String
        Dim oArts As FSNServer.Articulos
        Dim oPres1 As FSNServer.PresProyectosNivel1
        Dim oPres2 As FSNServer.PresContablesNivel1
        Dim oPres3 As FSNServer.PresConceptos3Nivel1
        Dim oPres4 As FSNServer.PresConceptos4Nivel1
        Dim oProve As FSNServer.Proveedor
        Dim iTipoFecha As TiposDeDatos.TipoFecha
        Dim arrCamposMaterial(0) As Long
        Dim sKeyTipoRecepcion As String = "0"
        Dim oGMN1s As FSNServer.GruposMatNivel1
        Dim oGMN1 As FSNServer.GrupoMatNivel1
        Dim oGMN2 As FSNServer.GrupoMatNivel2
        Dim oGMN3 As FSNServer.GrupoMatNivel3
        Dim i, iNivel, lIdPresup As Integer
        Dim dPorcent As Decimal
        Dim oDS As DataSet
        Dim idCampo As Long
        Dim bDepartamentoRelacionado, bCentroRelacionado, bAlmacenRelacionado As Boolean
        Dim lOrdenOrgCompras, lOrdenCentro As Long
        Dim sCodCentroCoste As String = ""
        Dim sCodDest As String = ""
        Dim bFavorito As Boolean = IIf((Request("Favorito")) = "1", True, False)
        Dim lIdFavorito As Long = 0
        Dim bArticuloGenerico As Boolean
        Dim nColumna As Integer = 1
        Dim bDepBaja As Boolean = False
        Dim oDSRowLista As System.Data.DataRow
        Dim blnEncontradoenLista As Boolean = False

        FSNServer = Session("FSN_Server")
        oUser = Session("FSN_User")
        msIdi = oUser.Idioma.ToString()

        oDict = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.DesgloseControl, msIdi)
        oTextos = oDict.Data.Tables(0)

        If Not String.IsNullOrEmpty(Request("IdFavorito")) Then lIdFavorito = CDbl(Request("IdFavorito"))

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeMonedaObligatoria") Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeMonedaObligatoria", "<script>var sMensajeMonedaObligatoria = '" & JSText(oTextos.Rows.Item(26).Item(1)) & "'</script>")
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeEliminarArticulosCambioMaterial", "<script>var sMensajeEliminArtMat = '" & JSText(oTextos.Rows.Item(28).Item(1)) & "'</script>")
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeMaterialObligatorio", "<script>var sMensajeMaterialObligatorio = '" & JSText(oTextos.Rows.Item(29).Item(1)) & "'</script>")
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeImposibleCambiarMoneda", "<script>var sMensajeImposibleCambiarMoneda = '" & JSText(oTextos.Rows.Item(30).Item(1)) & "'</script>")
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MonedaActual", "<script>var sMonedaActual = '" & msInstanciaMoneda & "'</script>")
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "DenMonedaActual", "<script>var sDenMonedaActual = ''</script>")
        End If

        If Not Page.IsPostBack Then
            mdsCampos.Relations.Add("REL_CAMPO_CAMPO_HIJO", mdsCampos.Tables(0).Columns("CAMPO_HIJO"), mdsCampos.Tables(0).Columns("ID"), False)
            mdsCampos.Relations.Add("REL_CAMPO_CAMPO_PADRE", mdsCampos.Tables(0).Columns("CAMPO_PADRE"), mdsCampos.Tables(0).Columns("ID"), False)
        End If

        sClase = "ugfilatabla"

        Dim bExisteCampoOrigen As Boolean = False
        Dim dcColumnaCampoOrigen As DataColumn
        dcColumnaCampoOrigen = mdsCampos.Tables(0).Columns("CAMPO_ORIGEN")

        If Not dcColumnaCampoOrigen Is Nothing Then bExisteCampoOrigen = True

        For Each oDSRow In mdsCampos.Tables(0).Rows
            'Guardamos los campos GS ocultos
            Dim sOculto As String
            Dim sOcultoClave As String
            If (oDSRow.Item("VISIBLE") = 0) Then
                If (DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS"))) Then
                    If DBNullToSomething(oDSRow.Item("VALOR_NUM")) Is Nothing Then
                        sOculto = oDSRow.Item("TIPO_CAMPO_GS") & "_" & oDSRow.Item("GRUPO") & "__" & oDSRow.Item("VALOR_TEXT")
                    Else
                        sOculto = oDSRow.Item("TIPO_CAMPO_GS") & "_" & oDSRow.Item("GRUPO") & "__" & oDSRow.Item("VALOR_NUM")
                    End If

                    If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                        sOcultoClave = "Ocultos" & oDSRow.Item("ID_CAMPO") & "_" & oDSRow.Item("TIPO_CAMPO_GS") & "_" & oDSRow.Item("GRUPO")
                    Else
                        sOcultoClave = "Ocultos" & oDSRow.Item("ID") & "_" & oDSRow.Item("TIPO_CAMPO_GS") & "_" & oDSRow.Item("GRUPO")
                    End If

                    If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), sOcultoClave) Then
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), sOcultoClave, "<script>arrGSOcultos[arrGSOcultos.length] = '" & sOculto & "'</script>")
                    End If
                End If
            End If

            If DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Material Then
                ReDim Preserve arrCamposMaterial(UBound(arrCamposMaterial) + 1)
                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                    sKeyMaterial = "fsentry" + oDSRow.Item("ID_CAMPO").ToString
                    arrCamposMaterial(UBound(arrCamposMaterial)) = oDSRow.Item("ID_CAMPO")
                Else
                    sKeyMaterial = "fsentry" + oDSRow.Item("ID").ToString
                    arrCamposMaterial(UBound(arrCamposMaterial)) = oDSRow.Item("ID")
                End If
            ElseIf (DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.OrganizacionCompras) And (oDSRow.Item("VISIBLE") = 0) Then
                sCodOrgCompras = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
                lOrdenOrgCompras = DBNullToSomething(oDSRow.Item("ORDEN"))
                If Not mdsCampos.Tables(0).Columns("ID_CAMPO") Is Nothing Then
                    sKeyOrgCompras = "fsentry" + oDSRow.Item("ID_CAMPO").ToString
                Else
                    sKeyOrgCompras = "fsentry" + oDSRow.Item("ID").ToString
                End If
            ElseIf (DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Centro) And (oDSRow.Item("VISIBLE") = 0) Then
                sCodCentro = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
                lOrdenCentro = DBNullToSomething(oDSRow.Item("ORDEN"))
            ElseIf DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
                If Not mdsCampos.Tables(0).Columns("ID_CAMPO") Is Nothing Then
                    sKeyArticulo = "fsentry" + oDSRow.Item("ID_CAMPO").ToString
                Else
                    sKeyArticulo = "fsentry" + oDSRow.Item("ID").ToString
                End If
            End If

            If (oDSRow.Item("VISIBLE") = 1 And oDSRow.Item("SUBTIPO") <> TiposDeDatos.TipoGeneral.TipoDesglose) Or (oDSRow.Item("VISIBLE") = 1 And oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoDesglose And DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.DesgloseActividad And oDSRow.Item("POPUP") = 1) Then
                'No es un desglose o es un desglose de popup
                'Nombre del campo
                otblRow = New System.Web.UI.WebControls.TableRow
                otblCell = New System.Web.UI.WebControls.TableCell
                otblCell.Width = Unit.Percentage(40)
                otblCell.Attributes("valign") = "top"
                olabel = New System.Web.UI.WebControls.Label
                If DBNullToSomething(oDSRow.Item("OBLIGATORIO")) = 1 Then
                    olabel.Text = "(*)" + DBNullToSomething(oDSRow.Item("DEN_" & msIdi))
                    olabel.Style("font-weight") = "bold"
                Else
                    olabel.Text = DBNullToSomething(oDSRow.Item("DEN_" & msIdi))
                End If
                otblCell.Controls.Add(olabel)
                otblCell.CssClass = sClase
                otblRow.Cells.Add(otblCell)

                oDSRow.Item("DEN_" & msIdi) = oDSRow.Item("DEN_" & msIdi).ToString.Replace("'", "\'")
                oDSRow.Item("DEN_" & msIdi) = oDSRow.Item("DEN_" & msIdi).ToString.Replace("""", "\""")

                'Icono de ayuda
                otblCell = New System.Web.UI.WebControls.TableCell
                otblCell.Width = Unit.Percentage(1)
                otblCell.Attributes("valign") = "top"

                If Not IsDBNull(oDSRow.Item("AYUDA_" & msIdi)) OrElse Not IsDBNull(oDSRow.Item("FORMULA")) Then
                    oimg = New System.Web.UI.WebControls.HyperLink
                    oimg.ImageUrl = "images\help.gif"
                    oimg.NavigateUrl = "javascript:void(null)"
                    If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                        oimg.Attributes("onclick") = "show_help(" + oDSRow.Item("ID_CAMPO").ToString + "," + mlInstancia.ToString + ")"
                    Else
                        oimg.Attributes("onclick") = "show_help(" + oDSRow.Item("ID").ToString + ",null," + mlSolicitud.ToString + ")"
                    End If
                    otblCell.Controls.Add(oimg)
                Else
                    otblCell.Text = "&nbsp;"
                End If

                otblCell.CssClass = sClase
                otblRow.Cells.Add(otblCell)

                'Campo calculado
                otblCell = New System.Web.UI.WebControls.TableCell
                otblCell.Width = Unit.Percentage(1)
                otblCell.Attributes("valign") = "top"

                If oDSRow.Item("TIPO") = 3 Then
                    oimg = New System.Web.UI.WebControls.HyperLink
                    oimg.ImageUrl = "../_common/images/sumatorio.gif"
                    oimg.NavigateUrl = "javascript:void(null)"
                    If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                        oimg.Attributes("onclick") = "show_help(" + oDSRow.Item("ID_CAMPO").ToString + "," + mlInstancia.ToString + ")"
                    Else
                        oimg.Attributes("onclick") = "show_help(" + oDSRow.Item("ID").ToString + ",null," + mlSolicitud.ToString + ")"
                    End If
                    otblCell.Controls.Add(oimg)
                Else
                    otblCell.Text = "&nbsp;"
                End If
                otblCell.CssClass = sClase
                otblRow.Cells.Add(otblCell)

                'Valor del campo
                otblCell = New System.Web.UI.WebControls.TableCell
                otblCell.Width = Unit.Percentage(58)

                'SRA: Tarea 978
                If mlInstancia > 0 AndAlso oDSRow.Item("ESCRITURA") = "0" AndAlso oDSRow("TIPO") <> "3" AndAlso
                Not (oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoDesglose AndAlso oDSRow.Item("POPUP") = 1) AndAlso
                ((oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo _
                OrElse oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio) _
                AndAlso DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.SinTipo _
                AndAlso DBNullToSomething(oDSRow.Item("INTRO")) = 0) AndAlso
                DBNullToSomething(oDSRow.Item("SUBTIPO")) <> TiposDeDatos.TipoGeneral.TipoArchivo AndAlso
                DBNullToSomething(oDSRow.Item("SUBTIPO")) <> TiposDeDatos.TipoGeneral.TipoEditor AndAlso
                Not {TiposDeDatos.TipoCampoGS.Material, TiposDeDatos.TipoCampoGS.NuevoCodArticulo, TiposDeDatos.TipoCampoGS.DenArticulo,
                    TiposDeDatos.TipoCampoGS.Moneda, TiposDeDatos.TipoCampoGS.Unidad, TiposDeDatos.TipoCampoGS.UnidadPedido,
                    TiposDeDatos.TipoCampoGS.OrganizacionCompras, TiposDeDatos.TipoCampoGS.Centro, TiposDeDatos.TipoCampoGS.DescrBreve,
                    TiposDeDatos.TipoCampoGS.DescrDetallada, TiposDeDatos.TipoCampoGS.DescrBreveContrato, TiposDeDatos.TipoCampoGS.DescrDetalladaContrato,
                    TiposDeDatos.TipoCampoGS.NombreCertif, TiposDeDatos.TipoCampoGS.Certificado, TiposDeDatos.TipoCampoGS.EntidadCertificadora,
                    TiposDeDatos.TipoCampoGS.Alcance, TiposDeDatos.TipoCampoGS.NumCertificado, TiposDeDatos.TipoCampoGS.ListadosPersonalizados,
                    TiposDeDatos.TipoCampoGS.Titulo, TiposDeDatos.TipoCampoGS.Motivo, TiposDeDatos.TipoCampoGS.CausasYConclus,
                    TiposDeDatos.TipoCampoGS.Accion, TiposDeDatos.TipoCampoGS.Responsable, TiposDeDatos.TipoCampoGS.Observaciones,
                    TiposDeDatos.TipoCampoGS.TipoPedido, TiposDeDatos.TipoCampoGS.FormaPago, TiposDeDatos.TipoCampoGS.CentroCoste}.Contains(oDSRow.Item("TIPO_CAMPO_GS")) AndAlso
                Not (DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Partida AndAlso
                DBNullToSomething(oDSRow.Item("CENTRO_COSTE")) <> Nothing AndAlso
                Not IsDBNull(oDSRow.Item("VALOR_TEXT"))) Then
                    oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
                    If oDSRow("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo Then
                        oLbl.Attributes("class") = "TipoArchivo"
                    End If
                    'En los presupuestos, las unidades organizativas y los departamentos, valor_text_cod indica si est� en baja l�gica o no.
                    If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                        If DBNullToSomething(oDSRow.Item("VALOR_TEXT_COD")) = "1" AndAlso Not IsDBNull(oDSRow.Item("TIPO_CAMPO_GS")) AndAlso (oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.PRES1 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres2 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres3 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres4 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.UnidadOrganizativa OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Departamento) Then
                            oLbl.Attributes("class") = "StringTachadotrasparent"
                        End If
                    End If
                    oLbl.InnerHtml = EscribirValorCampo(oDSRow)

                    If oLbl.InnerHtml <> "&nbsp;" Then
                        Dim oImgPopUp As System.Web.UI.WebControls.HyperLink
                        'POPUPS con la descripci�n completa de textos medio y largos o archivos adjuntos
                        If oDSRow.Item("TIPO") = "6" Then
                            If Not IsDBNull(oDSRow.Item("VALOR_TEXT_DEN")) Then
                                Dim sTxt As String = oDSRow.Item("VALOR_TEXT_DEN").ToString()
                                Dim sTxt2 As String
                                If sTxt.IndexOf("]#[") > 0 Then
                                    sTxt2 = sTxt.Substring(2, sTxt.IndexOf("]#[") - 2)
                                Else
                                    sTxt2 = sTxt.Substring(2, sTxt.Length - 3)
                                End If

                                oImgPopUp = New System.Web.UI.WebControls.HyperLink
                                oImgPopUp.ImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(Fullstep.DataEntry.GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")
                                oImgPopUp.NavigateUrl = "javascript:AbrirTablaExterna('" & oDSRow.Item("TABLA_EXTERNA") & "','" & sTxt2 & "')"
                                otblCell.Controls.Add(CargarTextoYBotonEnTabla(oLbl, oImgPopUp))
                            Else
                                otblCell.Controls.Add(oLbl)
                            End If
                            otblCell.CssClass = sClase
                            otblRow.Cells.Add(otblCell)

                        ElseIf ((oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio OrElse oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo) _
                     AndAlso (IsDBNull(oDSRow.Item("TIPO_CAMPO_GS")) OrElse DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.DescrBreve OrElse DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.DescrDetallada OrElse DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Partida OrElse DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Activo)) _
                     AndAlso (DBNullToSomething(oDSRow.Item("INTRO")) = "0") Then

                            oImgPopUp = New System.Web.UI.WebControls.HyperLink
                            oImgPopUp.ImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(Fullstep.DataEntry.GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")

                            If oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo Then
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    oImgPopUp.NavigateUrl = "javascript:AbrirFicherosAdjuntos('" & idAdjun & "','" & oDSRow.Item("ID_CAMPO") & "','" & mlInstancia & "',1)"
                                Else
                                    oImgPopUp.NavigateUrl = "javascript:AbrirFicherosAdjuntos('" & idAdjun & "','" & oDSRow.Item("ID") & "','" & mlInstancia & "',1)"
                                End If
                            ElseIf DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Partida Then
                                oImgPopUp.NavigateUrl = "javascript:AbrirDetallePartida('" & oDSRow.Item("DEN_" & msIdi) & "','" & oDSRow.Item("PRES5") & "','" & oDSRow.Item("VALOR_TEXT") & "')"
                            ElseIf DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Activo Then
                                oImgPopUp.NavigateUrl = "javascript:AbrirDetalleActivo('" & oDSRow.Item("VALOR_TEXT") & "')"
                            Else
                                Dim textSourceId As String = Me.ClientID + "__fsentry" + oDSRow.Item("ID").ToString + "__t"
                                oImgPopUp.NavigateUrl = "javascript:AbrirPopUpTexto('" & Replace(oDSRow.Item("DEN_" & msIdi), "'", "\'") & "','" & textSourceId & "'," & IIf(oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo, "true", "false") & ")"
                            End If

                            otblCell.CssClass = sClase
                            otblCell.Controls.Add(CargarTextoYBotonEnTabla(oLbl, oImgPopUp))

                            'hidden con el valor
                            Dim oHidCampo As New System.Web.UI.HtmlControls.HtmlInputHidden
                            oHidCampo.ID = "_fsentry" + oDSRow.Item("ID").ToString + "__t"
                            If oDSRow("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo Then
                                oHidCampo.Value = idAdjun
                            ElseIf oDSRow("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoBoolean Then
                                oHidCampo.Value = DBNullToSomething(oDSRow.Item("VALOR_BOOL"))
                            ElseIf oDSRow("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then
                                oHidCampo.Value = DBNullToSomething(oDSRow.Item("VALOR_FEC"))
                            ElseIf oDSRow("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoNumerico Then
                                oHidCampo.Value = DBNullToSomething(oDSRow.Item("VALOR_NUM"))
                            Else
                                oHidCampo.Value = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
                            End If
                            otblCell.Controls.Add(oHidCampo)
                            otblRow.Cells.Add(otblCell)

                        ElseIf Not IsDBNull(oDSRow.Item("TIPO_CAMPO_GS")) AndAlso (oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.PRES1 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres2 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres3 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres4 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Material) Then
                            oImgPopUp = New System.Web.UI.WebControls.HyperLink
                            oImgPopUp.ImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(Fullstep.DataEntry.GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")

                            If oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Material Then
                                oImgPopUp.NavigateUrl = "javascript:AbrirMateriales('" & oDSRow.Item("VALOR_TEXT") & "')"
                            Else
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    oImgPopUp.NavigateUrl = "javascript:AbrirPresupuestos('" & oDSRow.Item("VALOR_TEXT") & "','" & oDSRow.Item("ID_CAMPO") & "','" & mlInstancia & "')"
                                Else
                                    oImgPopUp.NavigateUrl = "javascript:AbrirPresupuestos('" & oDSRow.Item("VALOR_TEXT") & "','" & oDSRow.Item("ID") & "','" & mlInstancia & "')"
                                End If
                            End If

                            otblCell.CssClass = sClase
                            otblCell.Controls.Add(CargarTextoYBotonEnTabla(oLbl, oImgPopUp))
                            otblRow.Cells.Add(otblCell)
                        ElseIf DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Dest Then
                            oImgPopUp = New System.Web.UI.WebControls.HyperLink
                            oImgPopUp.ImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(Fullstep.DataEntry.GeneralEntry), "Fullstep.DataEntry.info.gif")
                            oImgPopUp.NavigateUrl = "javascript:showDetalleDestino(null,'" & oDSRow.Item("VALOR_TEXT") & "')"
                            otblCell.Controls.Add(CargarTextoYBotonEnTabla(oLbl, oImgPopUp))
                            otblCell.CssClass = sClase
                            otblRow.Cells.Add(otblCell)
                        Else
                            If oDSRow.Item("TIPO") <> "6" AndAlso oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoNumerico And (IsDBNull(oDSRow.Item("TIPO_CAMPO_GS")) _
                            OrElse (Not {TiposDeDatos.TipoCampoGS.Almacen, TiposDeDatos.TipoCampoGS.RefSolicitud, TiposDeDatos.TipoCampoGS.Empresa}.Contains(oDSRow.Item("TIPO_CAMPO_GS")))) Then
                                otblCell.Controls.Add(CargarImporteEnTabla(oLbl))
                            Else
                                otblCell.Controls.Add(oLbl)
                            End If

                            otblCell.CssClass = sClase
                            otblRow.Cells.Add(otblCell)
                        End If

                    Else
                        otblCell.Controls.Add(oLbl)
                        otblCell.CssClass = sClase
                        otblRow.Cells.Add(otblCell)
                    End If

                    nColumna = nColumna + 1
                Else
                    oFSEntry = New DataEntry.GeneralEntry
                        oFSEntry.WindowTitle = Me.Page.Title
                        oFSEntry.PM = Me.PM
                        oFSEntry.ActivoSM = CType(Me.Page, FSNPage).Acceso.gbAccesoFSSM

                        oFSEntry.TipoSolicit = mTipoSolicitud
                        oFSEntry.MonSolicit = msInstanciaMoneda

                        oFSEntry.Width = Unit.Percentage(100)

                        If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                            oFSEntry.Tag = oDSRow.Item("ID_CAMPO")
                            oFSEntry.ID = "fsentry" + oDSRow.Item("ID_CAMPO").ToString
                            oFSEntry.CampoDef_CampoODesgHijo = oDSRow.Item("ID")
                        Else
                            oFSEntry.Tag = oDSRow.Item("ID")
                            oFSEntry.ID = "fsentry" + oDSRow.Item("ID").ToString
                        End If

                        oFSEntry.Name = Me.UniqueID
                        oFSEntry.IdContenedor = Me.ClientID
                        oFSEntry.Title = oDSRow.Item("DEN_" & msIdi)
                        oFSEntry.WindowTitle = Me.Page.Title
                        oFSEntry.Intro = oDSRow.Item("INTRO")
                        oFSEntry.Idi = msIdi
                        oFSEntry.TabContainer = msTabContainer
                        oFSEntry.TipoGS = DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS"))
                        oFSEntry.ReadOnly = (oDSRow.Item("ESCRITURA") = 0) Or mbSoloLectura
                        oFSEntry.RecordarValores = (oDSRow.Item("SAVE_VALUES") = 1)

                        ''Comprobar Si tiene bloqueos de escritura
                        If Not oFSEntry.ReadOnly AndAlso
                        mTipoSolicitud <> TiposDeDatos.TipoDeSolicitud.Certificado AndAlso
                        (mTipoSolicitud <> TiposDeDatos.TipoDeSolicitud.NoConformidad OrElse (mTipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad And EnCursoDeAprobacion)) Then
                            If (oDSRow.GetChildRows("REL_CAMPO_BLOQUEO").Length > 0) And (Not DBNullToSomething(oDSRow.Item("ESCRITURA_FORMULA")) Is Nothing) Then
                                Dim dsMonedas As DataSet
                                Dim oMonedas As Monedas = Me.FSNServer.Get_Object(GetType(Monedas))
                                Dim dCambioVigenteInstancia As Double = 1
                                If Not msInstanciaMoneda Is Nothing Then
                                    dsMonedas = oMonedas.Get_Monedas("SPA", msInstanciaMoneda, bAllDatos:=True)
                                    dCambioVigenteInstancia = dsMonedas.Tables(0).Rows(0)("EQUIV")
                                End If

                                oFSEntry.ReadOnly = ComprobarCondiciones(mlInstancia, oDSRow.Item("ESCRITURA_FORMULA"), oDSRow.GetChildRows("REL_CAMPO_BLOQUEO"), mdsCampos, dCambioVigenteInstancia)
                            End If
                        End If

                        oFSEntry.Obligatorio = (DBNullToSomething(oDSRow.Item("OBLIGATORIO")) = 1)
                        oFSEntry.Calculado = (oDSRow.Item("TIPO") = 3 Or oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoNumerico) And Not IsDBNull(oDSRow.Item("ID_CALCULO"))
                        oFSEntry.Orden = oDSRow.Item("ORDEN")
                        oFSEntry.Grupo = DBNullToStr(oDSRow.Item("GRUPO"))
                        oFSEntry.DenGrupo = DBNullToStr(oDSRow.Item("DEN_GRUPO"))
                        oFSEntry.IdAtrib = DBNullToSomething(oDSRow.Item("ID_ATRIB_GS"))
                        oFSEntry.ValErp = DBNullToSomething(oDSRow.Item("VALIDACION_ERP"))
                        oFSEntry.nColumna = nColumna
                        nColumna = nColumna + 1

                        If bExisteCampoOrigen Then
                            oFSEntry.Campo_Origen = DBNullToDbl(oDSRow.Item("CAMPO_ORIGEN"))
                        End If

                        If oFSEntry.Intro = 1 Then
                            If mdsCampos.Tables.Count = 1 And (DBNullToDbl(oDSRow.Item("CAMPO_HIJO")) = 0 And DBNullToDbl(oDSRow.Item("CAMPO_PADRE")) = 0) Then
                                oFSEntry.Intro = 0
                            Else
                                oFSEntry.Lista = New DataSet
                                oFSEntry.Lista.Merge(oDSRow.GetChildRows("REL_CAMPO_LISTA"))
                            End If

                            oFSEntry.idCampoListaEnlazada = DBNullToDbl(oDSRow.Item("ID"))

                            'Si se trata de una lista comprobamos si tiene alguna lista enlazada
                            If oDSRow.GetChildRows("REL_CAMPO_CAMPO_HIJO").Count Then
                                If mlInstancia > 0 Then
                                    oFSEntry.idCampoHijaListaEnlazada = DBNullToDbl(oDSRow.GetChildRows("REL_CAMPO_CAMPO_HIJO")(0)("ID_CAMPO"))

                                Else
                                    oFSEntry.idCampoHijaListaEnlazada = DBNullToDbl(oDSRow.GetChildRows("REL_CAMPO_CAMPO_HIJO")(0)("ID"))
                                End If
                            End If

                            'Si el campo padre de la lista es un campo de tipo material puede estar en cualquier otro grupo del formulario
                            Dim bCampoPadreEncontrado As Boolean
                            If oDSRow.GetChildRows("REL_CAMPO_CAMPO_PADRE").Count Then
                                bCampoPadreEncontrado = True
                                If mlInstancia > 0 Then
                                    oFSEntry.idCampoPadreListaEnlazada = DBNullToDbl(oDSRow.GetChildRows("REL_CAMPO_CAMPO_PADRE")(0)("ID_CAMPO"))
                                Else
                                    oFSEntry.idCampoPadreListaEnlazada = DBNullToDbl(oDSRow.GetChildRows("REL_CAMPO_CAMPO_PADRE")(0)("ID"))
                                End If
                            End If
                            If Not bCampoPadreEncontrado Then
                                If DBNullToDbl(oDSRow.Item("CAMPO_PADRE")) <> 0 Then
                                    If mlInstancia > 0 Then
                                        For Each oGrupo As FSNServer.Grupo In ObjInstancia.Grupos.Grupos
                                            If oGrupo.Id <> mlId Then
                                                Dim dvCampos As New DataView(oGrupo.DSCampos.Tables(0), "ID=" & oDSRow.Item("CAMPO_PADRE") & " AND TIPO=1 AND TIPO_CAMPO_GS=103", "", DataViewRowState.CurrentRows)
                                                If dvCampos.Count Then
                                                    oFSEntry.idCampoPadreListaEnlazada = DBNullToDbl(dvCampos(0)("ID_CAMPO"))
                                                    Exit For
                                                End If
                                            End If
                                        Next
                                    Else
                                        If _Formulario.Grupos Is Nothing Then _Formulario.Load(msIdi, mlSolicitud, , oUser.CodPersona, , , oUser.DateFmt)

                                        For Each oGrupo As FSNServer.Grupo In _Formulario.Grupos.Grupos
                                            If oGrupo.Id <> mlId Then
                                                Dim dvCampos As New DataView(oGrupo.DSCampos.Tables(0), "ID=" & oDSRow.Item("CAMPO_PADRE") & " AND TIPO=1 AND TIPO_CAMPO_GS=103", "", DataViewRowState.CurrentRows)
                                                If dvCampos.Count Then
                                                    oFSEntry.idCampoPadreListaEnlazada = DBNullToDbl(dvCampos(0)("ID"))
                                                    Exit For
                                                End If
                                            End If
                                        Next
                                    End If
                                End If
                            End If

                            If mlInstancia > 0 Then oFSEntry.Instancia = mlInstancia
                        End If

                        blnEncontradoenLista = False
                        oFSEntry.NumberFormat = oUser.NumberFormat
                        oFSEntry.DateFormat = oUser.DateFormat
                        oFSEntry.Tipo = oDSRow.Item("SUBTIPO")
                        oFSEntry.UsarOrgCompras = CType(Me.Page, FSNPage).Acceso.gbUsar_OrgCompras
                        Select Case oFSEntry.Tipo
                            Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoTextoLargo
                                If oFSEntry.Intro = 1 Then
                                    oFSEntry.Width = Unit.Percentage(100)
                                    If mlInstancia > 0 Then
                                        If DBNullToSomething(oDSRow.Item("VALOR_TEXT")) <> Nothing Then
                                            oFSEntry.Valor = DBNullToSomething(oDSRow.Item("VALOR_NUM"))
                                            oFSEntry.Text = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
                                            If oFSEntry.Lista.Tables.Count > 0 Then
                                                For Each oDSRowLista In oFSEntry.Lista.Tables(0).Rows
                                                    If DBNullToSomething(oDSRow.Item("VALOR_NUM")) > 0 AndAlso oDSRowLista.Item("ORDEN") = oDSRow.Item("VALOR_NUM") Then
                                                        oFSEntry.Text = DBNullToSomething(oDSRowLista.Item("VALOR_TEXT_" & msIdi))
                                                        Exit For
                                                    End If
                                                Next
                                            End If
                                        Else
                                            'Si es un atributo
                                            If oFSEntry.IdAtrib <> 0 Then
                                                'Si el campo es obligatorio y s�lo hay un valor de lista posible, se lo ponemos.
                                                If oFSEntry.Obligatorio AndAlso oFSEntry.Lista.Tables.Count > 0 AndAlso oFSEntry.Lista.Tables(0).Rows.Count = 1 Then
                                                    oFSEntry.Valor = DBNullToSomething(oFSEntry.Lista.Tables(0).Rows(0).Item("ORDEN"))
                                                    oFSEntry.Text = DBNullToSomething(oFSEntry.Lista.Tables(0).Rows(0).Item("VALOR_TEXT_" & msIdi))
                                                End If
                                            End If
                                        End If
                                    Else
                                        'Si es un atributo
                                        If oFSEntry.IdAtrib <> 0 Then
                                            If oFSEntry.Lista.Tables.Count > 0 Then
                                                'Recorremos los valores de lista que puede ver el Rol, si el valor por defecto est� entre ellos, lo ponemos
                                                For Each oDSRowLista In oFSEntry.Lista.Tables(0).Rows
                                                    If DBNullToSomething(oDSRow.Item("VALOR_TEXT")) = DBNullToSomething(oDSRowLista.Item("VALOR_TEXT_" & msIdi)) Then
                                                        blnEncontradoenLista = True
                                                        Exit For
                                                    End If
                                                Next
                                                If blnEncontradoenLista Then
                                                    If DBNullToSomething(oDSRow.Item("VALOR_NUM")) > 0 Then oFSEntry.Valor = DBNullToSomething(oDSRow.Item("VALOR_NUM"))
                                                    oFSEntry.Text = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
                                                Else
                                                    'Si el campo es obligatorio y s�lo hay un valor de lista posible, se lo ponemos.
                                                    If oFSEntry.Obligatorio AndAlso oFSEntry.Lista.Tables(0).Rows.Count = 1 Then
                                                        oFSEntry.Valor = DBNullToSomething(oFSEntry.Lista.Tables(0).Rows(0).Item("ORDEN"))
                                                        oFSEntry.Text = DBNullToSomething(oFSEntry.Lista.Tables(0).Rows(0).Item("VALOR_TEXT_" & msIdi))
                                                    End If
                                                End If
                                            End If
                                        Else
                                            If Not Page.IsPostBack Then
                                                If DBNullToSomething(oDSRow.Item("VALOR_NUM")) > 0 Then oFSEntry.Valor = DBNullToSomething(oDSRow.Item("VALOR_NUM"))
                                                oFSEntry.Text = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
                                                If oFSEntry.Lista.Tables.Count > 0 Then
                                                    For Each oDSRowLista In oFSEntry.Lista.Tables(0).Rows
                                                        If DBNullToSomething(oDSRow.Item("VALOR_NUM")) > 0 AndAlso oDSRowLista.Item("ORDEN") = oDSRow.Item("VALOR_NUM") Then
                                                            oFSEntry.Text = DBNullToSomething(oDSRowLista.Item("VALOR_TEXT_" & msIdi))
                                                            Exit For
                                                        End If
                                                    Next
                                                End If
                                            End If
                                        End If
                                    End If
                                Else
                                    oFSEntry.Valor = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
                                    oFSEntry.Text = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))

                                    oFSEntry.Formato = DBNullToSomething(oDSRow.Item("FORMATO"))
                                    If Not IsDBNull(oDSRow.Item("FORMATO")) Then
                                        oFSEntry.ErrMsgFormato = oTextos.Rows(27).Item(1)
                                    End If
                                End If
                                If oFSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoTextoMedio _
                            AndAlso Strings.Len(oFSEntry.Text) > 80 Then
                                    oFSEntry.Width = Unit.Percentage(100)
                                End If
                            Case TiposDeDatos.TipoGeneral.TipoNumerico
                                oFSEntry.Width = Unit.Pixel(120)
                                oFSEntry.Maximo = DBNullToSomething(oDSRow.Item("MAXNUM"))
                                oFSEntry.Minimo = DBNullToSomething(oDSRow.Item("MINNUM"))

                                If oFSEntry.Intro = 1 Then
                                    If mlInstancia > 0 Then
                                        If DBNullToSomething(oDSRow.Item("VALOR_NUM")) <> Nothing Then
                                            oFSEntry.Valor = DBNullToSomething(oDSRow.Item("VALOR_NUM"))
                                        Else
                                            'Si el campo es obligatorio y s�lo hay un valor de lista posible, se lo ponemos.
                                            If oFSEntry.Obligatorio AndAlso oFSEntry.Lista.Tables.Count > 0 AndAlso oFSEntry.Lista.Tables(0).Rows.Count = 1 Then
                                                oFSEntry.Valor = DBNullToSomething(oFSEntry.Lista.Tables(0).Rows(0).Item("VALOR_NUM"))
                                            End If
                                        End If
                                    Else
                                        'Si es un atributo
                                        If oFSEntry.IdAtrib <> 0 Then
                                            If oFSEntry.Lista.Tables.Count > 0 Then
                                                'Recorremos los valores de lista que puede ver el Rol, si el valor por defecto est� entre ellos, lo ponemos
                                                For Each oDSRowLista In oFSEntry.Lista.Tables(0).Rows
                                                    If DBNullToSomething(oDSRow.Item("VALOR_NUM")) = DBNullToSomething(oDSRowLista.Item("VALOR_NUM")) Then
                                                        blnEncontradoenLista = True
                                                        Exit For
                                                    End If
                                                Next
                                                If blnEncontradoenLista Then
                                                    oFSEntry.Valor = DBNullToSomething(oDSRow.Item("VALOR_NUM"))
                                                Else
                                                    'Si el campo es obligatorio y s�lo hay un valor de lista posible, se lo ponemos.
                                                    If oFSEntry.Obligatorio AndAlso oFSEntry.Lista.Tables(0).Rows.Count = 1 Then
                                                        oFSEntry.Valor = DBNullToSomething(oFSEntry.Lista.Tables(0).Rows(0).Item("VALOR_NUM"))
                                                    End If
                                                End If
                                            End If
                                        Else
                                            oFSEntry.Valor = DBNullToSomething(oDSRow.Item("VALOR_NUM"))
                                        End If
                                    End If
                                Else
                                    oFSEntry.Valor = DBNullToSomething(oDSRow.Item("VALOR_NUM"))
                                End If
                            Case TiposDeDatos.TipoGeneral.TipoFecha
                                If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechasDefecto") Then
                                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechasDefecto", "<script>var sMensajeFechaDefecto = '" & oTextos.Rows.Item(17).Item(1) & "'</script>")
                                End If
                                If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "FechaNoAnteriorAlSistema") Then
                                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "FechaNoAnteriorAlSistema", "<script>var sFechaNoAnteriorAlSistema = '" & oTextos.Rows.Item(48).Item(1) & "'</script>")
                                End If
                                iTipoFecha = DBNullToSomething(oDSRow.Item("VALOR_NUM"))
                                oFSEntry.Maximo = DBNullToSomething(oDSRow.Item("MAXFEC"))
                                oFSEntry.Minimo = DBNullToSomething(oDSRow.Item("MINFEC"))
                                oFSEntry.FechaNoAnteriorAlSistema = DBNullToBoolean(oDSRow.Item("FECHA_VALOR_NO_ANT_SIS"))
                                If oFSEntry.Intro = 1 Then
                                    oFSEntry.Width = Unit.Pixel(120)
                                    If mlInstancia > 0 Then
                                        If DBNullToSomething(oDSRow.Item("VALOR_FEC")) <> Nothing Then
                                            oFSEntry.Valor = FormatDate(oDSRow.Item("VALOR_FEC"), oUser.DateFormat)
                                        Else
                                            'Si el campo es obligatorio y s�lo hay un valor de lista posible, se lo ponemos.
                                            If oFSEntry.Obligatorio AndAlso oFSEntry.Lista.Tables.Count > 0 AndAlso oFSEntry.Lista.Tables(0).Rows.Count = 1 Then
                                                oFSEntry.Valor = FormatDate(DBNullToSomething(oFSEntry.Lista.Tables(0).Rows(0).Item("VALOR_FEC")), oUser.DateFormat)
                                            End If
                                        End If
                                    Else
                                        If oFSEntry.IdAtrib <> 0 Then
                                            If oFSEntry.Lista.Tables.Count > 0 Then
                                                'Recorremos los valores de lista que puede ver el Rol, si el valor por defecto est� entre ellos, lo ponemos
                                                For Each oDSRowLista In oFSEntry.Lista.Tables(0).Rows
                                                    If DBNullToSomething(oDSRow.Item("VALOR_FEC")) = DBNullToSomething(oDSRowLista.Item("VALOR_FEC")) Then
                                                        blnEncontradoenLista = True
                                                        Exit For
                                                    End If
                                                Next
                                                If blnEncontradoenLista Then
                                                    oFSEntry.Valor = FormatDate(DBNullToSomething(oDSRow.Item("VALOR_FEC")), oUser.DateFormat)
                                                Else
                                                    'Si el campo es obligatorio y s�lo hay un valor de lista posible, se lo ponemos.
                                                    If oFSEntry.Obligatorio AndAlso oFSEntry.Lista.Tables(0).Rows.Count = 1 Then _
                                                 oFSEntry.Valor = FormatDate(DBNullToSomething(oFSEntry.Lista.Tables(0).Rows(0).Item("VALOR_FEC")), oUser.DateFormat)
                                                End If
                                            End If
                                        ElseIf DBNullToSomething(oDSRow.Item("VALOR_FEC")) <> Nothing Then
                                            oFSEntry.Valor = FormatDate(DBNullToSomething(oDSRow.Item("VALOR_FEC")), oUser.DateFormat)
                                        End If
                                    End If
                                Else
                                    oFSEntry.Valor = DevolverFechaRelativaA(iTipoFecha, DBNullToSomething(oDSRow.Item("VALOR_FEC")), bFavorito)
                                End If
                            Case TiposDeDatos.TipoGeneral.TipoBoolean
                                oFSEntry.Width = Unit.Pixel(50)
                                oFSEntry.Valor = DBNullToSomething(oDSRow.Item("VALOR_BOOL"))
                                If Not IsDBNull(oDSRow.Item("VALOR_BOOL")) Then oFSEntry.Text = IIf(oDSRow.Item("VALOR_BOOL") = 1, oTextos.Rows(15).Item(1), oTextos.Rows(16).Item(1))
                            Case TiposDeDatos.TipoGeneral.TipoEditor
                                oFSEntry.Valor = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
                            oFSEntry.Text = oTextos.Rows(21).Item(1)
                        Case TiposDeDatos.TipoGeneral.TipoEnlace
                            If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                oFSEntry.Valor = DBNullToSomething(oDSRow.Item("VALOR_TEXT_DEN"))
                            End If
                            oFSEntry.Text = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
                        Case TiposDeDatos.TipoGeneral.TipoArchivo
                                Dim idAdjun As String
                                Dim idAdjunForm As String

                                sAdjun = Nothing
                                idAdjun = Nothing
                                idAdjunForm = Nothing

                                If bFavorito Or InstanciaImportar > 0 Then
                                    For Each oDSRowAdjun In oDSRow.GetChildRows("REL_CAMPO_ADJUN")
                                        If oDSRowAdjun.Item("TIPO").ToString() = "1" Then
                                            'Los que vienen desde el dise�o
                                            idAdjunForm += oDSRowAdjun.Item("ID").ToString() + "xx"
                                        Else
                                            'Los que hemos a�adido anteriormente
                                            idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                                        End If
                                        'idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                                        sAdjun += oDSRowAdjun.Item("NOM") + " (" + FSNLibrary.FormatNumber((oDSRowAdjun.Item("DATASIZE") / 1024), oUser.NumberFormat) + " Kb.), "
                                    Next
                                If Not sAdjun Is Nothing Then
                                    sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
                                    If Not idAdjun Is Nothing Then
                                        idAdjun = idAdjun.Substring(0, idAdjun.Length - 2)
                                    End If
                                    If Not idAdjunForm Is Nothing Then
                                        idAdjunForm = idAdjunForm.Substring(0, idAdjunForm.Length - 2)
                                    End If

                                End If
                                If oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.ArchivoContrato Then
                                    oFSEntry.IdArchivoContrato = idAdjun
                                End If

                                oFSEntry.Valor = idAdjunForm & "####" & idAdjun
                            Else
                                    If oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.ArchivoContrato Then
                                        oFSEntry.IdContrato = mlIdContrato

                                        If msNombreContrato <> "" Then
                                            sAdjun = msNombreContrato

                                            oFSEntry.TamanyoContrato = msTamanyoContrato
                                            For Each oDSRowAdjun In oDSRow.GetChildRows("REL_CAMPO_ADJUN")
                                                'En caso de que tuviera un fichero por defecto daria error
                                                If Not oDSRowAdjun.Table.Columns("ADJUN") Is Nothing Then
                                                    idAdjun = oDSRowAdjun.Item("ADJUN").ToString()
                                                End If
                                            Next
                                            If idAdjun = "" Or idAdjun Is Nothing Then
                                                idAdjun = mlIdArchivoContrato.ToString
                                            End If
                                            oFSEntry.IdArchivoContrato = idAdjun
                                        End If
                                    Else

                                        For Each oDSRowAdjun In oDSRow.GetChildRows("REL_CAMPO_ADJUN")
                                            idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                                            sAdjun += oDSRowAdjun.Item("NOM") + " (" + FSNLibrary.FormatNumber((oDSRowAdjun.Item("DATASIZE") / 1024), oUser.NumberFormat) + " Kb.), "
                                        Next
                                        If Not sAdjun Is Nothing Then
                                            sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
                                            idAdjun = idAdjun.Substring(0, idAdjun.Length - 2)
                                        End If
                                    End If
                                    oFSEntry.Valor = idAdjun

                                End If

                                If Len(sAdjun) > 90 Then
                                    oFSEntry.Text = Mid(sAdjun, 1, 90) & "..."
                                Else
                                    oFSEntry.Text = sAdjun
                                End If

                            Case TiposDeDatos.TipoGeneral.TipoDesglose
                                oFSEntry.InputStyle.CssClass = "Tipodesglose"
                                oFSEntry.Width = Unit.Percentage(100)
                                oFSEntry.ReadOnly = mbSoloLectura Or (oDSRow.Item("ESCRITURA") = 0)
                                If sidDataEntryOrgCompras <> "" Then
                                    oFSEntry.idDataEntryDependent = sidDataEntryOrgCompras
                                End If
                                'SIN TRADUCIR
                                oFSEntry.Valor = oTextos.Rows(3).Item(1)

                                Dim oCampo As FSNServer.Campo
                                oCampo = FSNServer.Get_Object(GetType(FSNServer.Campo))

                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    oCampo.Id = oDSRow.Item("ID_CAMPO")
                                Else
                                    oCampo.Id = oDSRow.Item("ID")
                                End If

                                If mlInstancia > 0 Then
                                    oCampo.LoadInst(mlInstancia, msIdi)
                                Else
                                    oCampo.Load(msIdi, mlSolicitud)
                                End If

                                If oCampo.Tipo = TipoCampoPredefinido.NoConformidad Then
                                Else
                                    moDSEstados = Nothing
                                    moAcciones = Nothing
                                End If

                                oFSEntry.Movible = oCampo.MoverLinea

                                If mlInstancia > 0 Then
                                    'Comprueba si la solicitud es de no conformidad o certificado.Esto habr� que quitarlo cuando
                                    'se haga el workflow para certificados y no conformidades
                                    If mTipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado OrElse mTipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad Then
                                        oDS = oCampo.LoadInstDesglose(msIdi, mlInstancia, oUser.CodPersona, mlVersion)
                                    Else
                                        oDS = oCampo.LoadInstDesglose(msIdi, mlInstancia, oUser.CodPersona, mlVersion, , True, mbObservador, , , oUser.DateFmt, IIf(Request("Bloque") <> Nothing, CLng(Request("Bloque")), 0), IIf(Request("Rol") <> Nothing, CLng(Request("Rol")), 0))
                                    End If
                                Else
                                    If mTipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado OrElse mTipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad Then
                                        oDS = oCampo.LoadDesglose(msIdi, oCampo.IdSolicitud, , , , oUser.DateFmt, InstanciaImportar)
                                    Else
                                        oDS = oCampo.LoadDesglose(msIdi, oCampo.IdSolicitud, oUser.CodPersona, bFavorito, lIdFavorito, , , InstanciaImportar)
                                    End If
                                End If

                                Dim oRow As DataRow
                                Dim oNewRow As DataRow

                                If Not moDSEstados Is Nothing Then
                                    For Each oRow In moDSEstados.Tables(0).Rows
                                        oDS.Tables(0).ImportRow(oRow)
                                        oDS.Tables(0).Rows(oDS.Tables(0).Rows.Count - 1).Item("ID") = oRow.Item("ID").ToString
                                    Next
                                    For Each oRow In moDSEstados.Tables(1).Rows
                                        oDS.Tables(1).ImportRow(oRow)
                                    Next
                                    idCampo = oCampo.IdCopiaCampo
                                    For Each oRow In moDSEstados.Tables(2).Select("CAMPO_PADRE= " & idCampo.ToString)
                                        oNewRow = oDS.Tables("LINEAS").NewRow
                                        oNewRow.Item("LINEA") = oRow.Item("LINEA")
                                        oNewRow.Item("CAMPO_PADRE") = oRow.Item("CAMPO_PADRE")
                                        oNewRow.Item("CAMPO_HIJO") = oRow.Item("CAMPO_HIJO").ToString
                                        oNewRow.Item("VALOR_NUM") = oRow.Item("VALOR_NUM")
                                        oNewRow.Item("VALOR_TEXT") = oRow.Item("VALOR_TEXT")
                                        oDS.Tables("LINEAS").Rows.Add(oNewRow)
                                    Next
                                End If

                                oFSEntry.Lista = oDS
                        End Select

                        sDenominacion = ""

                        Select Case oFSEntry.TipoGS
                            Case TiposDeDatos.TipoCampoGS.Material
                                sValor = ""
                                sKeyMaterial = oFSEntry.ClientID
                                If oDSRow.Item("VISIBLE") = 0 Then
                                    oFSEntry.Visible = False
                                End If
                                Dim sMat As String = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
                                Dim arrMat(4) As String
                                Dim lLongCod As Integer

                                For i = 1 To 4
                                    arrMat(i) = ""
                                Next i

                                i = 1
                                While Trim(sMat) <> ""
                                    Select Case i
                                        Case 1
                                            lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                                        Case 2
                                            lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                                        Case 3
                                            lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                                        Case 4
                                            lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
                                    End Select
                                    arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
                                    sMat = Mid(sMat, lLongCod + 1)
                                    i = i + 1
                                End While

                            sToolTip = ""
                            sDenominacion = ""

                            If arrMat(4) <> "" Then
                                If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                    sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
                                Else
                                    oGMN3 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel3))
                                    oGMN3.GMN1Cod = arrMat(1)
                                    oGMN3.GMN2Cod = arrMat(2)
                                    oGMN3.Cod = arrMat(3)
                                    oGMN3.CargarTodosLosGruposMatDesde(1, msIdi, arrMat(4), , True)
                                    sDenominacion = oGMN3.GruposMatNivel4.Item(arrMat(4)).Den
                                    oGMN3 = Nothing
                                End If
                                sToolTip = sDenominacion & " (" & arrMat(1) & " - " & arrMat(2) & " - " & arrMat(3) & " - " & arrMat(4) & ")"
                                If Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles Then
                                    sValor = arrMat(1) + " - " + arrMat(2) + " - " + arrMat(3) + " - " + arrMat(4) + " - " + sDenominacion
                                Else
                                    sValor = arrMat(4) + " - " + sDenominacion
                                End If

                            ElseIf arrMat(3) <> "" Then
                                If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                    sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
                                Else
                                    oGMN2 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel2))
                                    oGMN2.GMN1Cod = arrMat(1)
                                    oGMN2.Cod = arrMat(2)
                                    oGMN2.CargarTodosLosGruposMatDesde(1, msIdi, arrMat(3), , True)
                                    sDenominacion = oGMN2.GruposMatNivel3.Item(arrMat(3)).Den
                                    oGMN2 = Nothing
                                End If
                                sToolTip = sDenominacion & "(" & arrMat(1) & " - " & arrMat(2) & " - " & arrMat(3) & ")"
                                If Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles Then
                                    sValor = arrMat(1) + " - " + arrMat(2) + " - " + arrMat(3) + " - " + sDenominacion
                                Else
                                    sValor = arrMat(3) + " - " + sDenominacion
                                End If

                            ElseIf arrMat(2) <> "" Then
                                If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                    sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
                                Else
                                    oGMN1 = FSNServer.Get_Object(GetType(FSNServer.GrupoMatNivel1))
                                    oGMN1.Cod = arrMat(1)
                                    oGMN1.CargarTodosLosGruposMatDesde(1, msIdi, arrMat(2), , True)
                                    sDenominacion = oGMN1.GruposMatNivel2.Item(arrMat(2)).Den
                                    oGMN1 = Nothing
                                End If
                                sToolTip = sDenominacion & "(" & arrMat(1) & " - " & arrMat(2) & ")"
                                If Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles Then
                                    sValor = arrMat(1) + " - " + arrMat(2) + " - " + sDenominacion
                                Else
                                    sValor = arrMat(2) + " - " + sDenominacion
                                End If

                            ElseIf arrMat(1) <> "" Then
                                If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                    sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
                                Else
                                    oGMN1s = FSNServer.Get_Object(GetType(FSNServer.GruposMatNivel1))
                                    oGMN1s.CargarTodosLosGruposMatDesde(1, msIdi, arrMat(1), , , True)
                                    sDenominacion = oGMN1s.Item(arrMat(1)).Den
                                        oGMN1s = Nothing
                                    End If
                                    sToolTip = sDenominacion & "(" & arrMat(1) & ")"
                                    sValor = arrMat(1) + " - " + sDenominacion
                                End If
                                oFSEntry.Width = Unit.Percentage(100)
                                oFSEntry.Text = sValor
                                oFSEntry.ToolTip = sToolTip
                                oFSEntry.Denominacion = sDenominacion
                                oFSEntry.AnyadirArt = oDSRow.Item("ANYADIR_ART")
                                oFSEntry.NivelSeleccion = DBNullToInteger(oDSRow.Item("NIVEL_SELECCION"))
                                If oFSEntry.NivelSeleccion <> 0 Then
                                    sIdDataentryMaterial = oFSEntry.IdContenedor & "_" & "fsentry" & oFSEntry.Tag
                                    Dim sscript As String
                                    sscript = "var sDataEntryMaterialFORM='" & sIdDataentryMaterial & "'"
                                    sscript = String.Format(IncludeScriptKeyFormat, "javascript", sscript)
                                    If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "VariablesMATERIALEntry" & oFSEntry.Tag) Then _
                                     Page.ClientScript.RegisterStartupScript(Page.GetType(), "VariablesMATERIALEntry" & oFSEntry.Tag, sscript)
                                End If

                                'Comprobar si tiene alg�n campo de tipo lista como hija. Puede estar en cualquier grupo, 1� se busca en el grupo actual y despu�s en los otros
                                Dim bListaHijaEncontrada As Boolean
                                If oDSRow.GetChildRows("REL_CAMPO_CAMPO_HIJO").Count Then
                                    bListaHijaEncontrada = True
                                    If mlInstancia > 0 Then
                                        oFSEntry.idCampoHijaListaEnlazada = DBNullToDbl(oDSRow.GetChildRows("REL_CAMPO_CAMPO_HIJO")(0)("ID_CAMPO"))

                                    Else
                                        oFSEntry.idCampoHijaListaEnlazada = DBNullToDbl(oDSRow.GetChildRows("REL_CAMPO_CAMPO_HIJO")(0)("ID"))
                                    End If
                                End If
                                If Not bListaHijaEncontrada Then
                                    If mlInstancia > 0 Then
                                        For Each oGrupo As FSNServer.Grupo In ObjInstancia.Grupos.Grupos
                                            If oGrupo.Id <> mlId Then
                                                Dim dvCampos As New DataView(oGrupo.DSCampos.Tables(0), "CAMPO_PADRE=" & oFSEntry.Campo_Origen & " AND TIPO=0 AND INTRO=1 AND TIPO_CAMPO_GS IS NULL", "", DataViewRowState.CurrentRows)
                                                If dvCampos.Count Then
                                                    oFSEntry.idCampoHijaListaEnlazada = DBNullToDbl(dvCampos(0)("ID_CAMPO"))
                                                    Exit For
                                                End If
                                            End If
                                        Next
                                    Else
                                        If _Formulario.Grupos Is Nothing Then _Formulario.Load(msIdi, mlSolicitud, , oUser.CodPersona, , , oUser.DateFmt)

                                        For Each oGrupo As FSNServer.Grupo In _Formulario.Grupos.Grupos
                                            If oGrupo.Id <> mlId Then
                                                Dim dvCampos As New DataView(oGrupo.DSCampos.Tables(0), "CAMPO_PADRE=" & oFSEntry.Tag & " AND TIPO=0 AND INTRO=1 AND TIPO_CAMPO_GS IS NULL", "", DataViewRowState.CurrentRows)
                                                If dvCampos.Count Then
                                                    oFSEntry.idCampoHijaListaEnlazada = DBNullToDbl(dvCampos(0)("ID"))
                                                    Exit For
                                                End If
                                            End If
                                        Next
                                    End If
                                End If

                            Case TiposDeDatos.TipoCampoGS.CodArticulo 'CodArticulo Solicitudes anteriores
                                oFSEntry.Width = Unit.Percentage(100)

                                oFSEntry.DependentField = sKeyMaterial
                                oFSEntry.Intro = 1
                                oFSEntry.InstanciaMoneda = InstanciaMoneda
                                If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                    If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                        If oDSRow.Item("VALOR_TEXT_COD") = 0 Then
                                            oFSEntry.Valor = Nothing
                                            oFSEntry.Text = oDSRow.Item("VALOR_TEXT")
                                        Else
                                            oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
                                            oFSEntry.Text = oDSRow.Item("VALOR_TEXT_DEN")
                                        End If
                                    Else
                                        oArts = FSNServer.Get_Object(GetType(FSNServer.Articulos))

                                        Dim oArticulo As PropiedadesArticulo = oArts.Articulo_Get_Properties(oDSRow.Item("VALOR_TEXT"))
                                        If oArticulo.ExisteArticulo Then
                                            oFSEntry.Valor = oArticulo.Codigo
                                            oFSEntry.Text = oArticulo.Denominacion
                                            oFSEntry.Concepto = oArticulo.Concepto
                                            oFSEntry.Almacenamiento = oArticulo.Almacenamiento
                                            oFSEntry.Recepcion = oArticulo.Recepcion
                                            oFSEntry.TipoRecepcion = oArticulo.TipoRecepcion
                                            sKeyTipoRecepcion = oFSEntry.TipoRecepcion
                                        Else
                                            oFSEntry.Valor = Nothing
                                            oFSEntry.Text = oDSRow.Item("VALOR_TEXT")
                                        End If
                                    End If
                                End If
                                oFSEntry.Denominacion = oFSEntry.Text
                                Dim idCampoMat As Long
                                Dim sValorMat As String

                                idCampoMat = 0

                                'OBTENEMOS EL VALOR DEL MAT OCULTO
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    For Each i In arrCamposMaterial

                                        If i < oDSRow.Item("id_campo") Then
                                            If idCampoMat < i Then
                                                idCampoMat = i
                                            End If
                                        End If
                                    Next
                                    If idCampoMat > 0 Then
                                        Try
                                            sValorMat = mdsCampos.Tables(0).Select("ID_CAMPO = " & idCampoMat)(0).Item("VALOR_TEXT")

                                            oFSEntry.DependentValue = sValorMat
                                        Catch ex As Exception

                                        End Try
                                    End If
                                Else
                                    For Each i In arrCamposMaterial

                                        If i < oDSRow.Item("ID") Then
                                            If idCampoMat < i Then
                                                idCampoMat = i
                                            End If
                                        End If
                                    Next
                                    If idCampoMat > 0 Then
                                        Try
                                            sValorMat = mdsCampos.Tables(0).Select("ID = " & idCampoMat)(0).Item("VALOR_TEXT")

                                            oFSEntry.DependentValue = sValorMat
                                        Catch ex As Exception

                                        End Try
                                    End If
                                End If
                                If sidDataEntryOrgCompras <> "" Then oFSEntry.idDataEntryDependent = sidDataEntryOrgCompras
                                If sIDCentro <> "" Then oFSEntry.idDataEntryDependent2 = oFSEntry.IdContenedor & "_" & "fsentry" & sIDCentro
                                If Not sIdUnidadOrganizativa = String.Empty Then oFSEntry.idDataEntryDependent3 = oFSEntry.IdContenedor & "_" & "fsentry" & sIdUnidadOrganizativa
                                If Not sCodCentroCoste = String.Empty Then oFSEntry.idDataEntryDependentCentroCoste = oFSEntry.IdContenedor & "_" & "fsentry" & sCodCentroCoste

                                Dim sKeyCantidad As String
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyCantidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyCantidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                    End If
                                End If
                                If sKeyCantidad <> "" Then
                                    oFSEntry.idEntryCANT = oFSEntry.IdContenedor & "_fsentry" & sKeyCantidad
                                End If

                                Dim sKeyUnidad As String
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyUnidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyUnidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                    End If
                                End If
                                If sKeyUnidad <> "" Then
                                    oFSEntry.idEntryUNI = oFSEntry.IdContenedor & "_fsentry" & sKeyUnidad
                                End If
                                Dim sKeyUnidadPedido As String
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyUnidadPedido = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyUnidadPedido = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                    End If
                                End If
                                If sKeyUnidadPedido <> "" Then
                                    oFSEntry.idEntryUNIPedido = oFSEntry.IdContenedor & "_fsentry" & sKeyUnidadPedido
                                End If
                                'Buscar la organizacion de compras y el centro
                                Try
                                    oFSEntry.DependentValueDataEntry = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                    oFSEntry.DependentValueDataEntry2 = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Centro & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                    oFSEntry.DependentValueDataEntry3 = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                    oFSEntry.DependentValueCentroCoste = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                    oFSEntry.UnidadOculta = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                Catch ex As Exception

                                End Try
                                Try
                                    If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                        oFSEntry.IdMaterialOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
                                        oFSEntry.IdDenArticuloOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
                                    Else
                                        oFSEntry.IdMaterialOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID"))
                                        oFSEntry.IdDenArticuloOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID"))
                                    End If
                                Catch ex As Exception
                                End Try
                                'idEntryPROV --> fsentry12345
                                If Not idEntryPROV Is Nothing Then
                                    oFSEntry.idEntryPROV = oFSEntry.IdContenedor & "_" & idEntryPROV
                                End If
                                If Not idEntryPREC Is Nothing Then
                                    oFSEntry.idEntryPREC = oFSEntry.IdContenedor & "_" & idEntryPREC
                                End If
                                If Not idEntryCC Is Nothing Then
                                    oFSEntry.idEntryCC = oFSEntry.IdContenedor & "_" & idEntryCC
                                End If
                            Case TiposDeDatos.TipoCampoGS.ProveedorERP
                                'El campo Gs proveedor ERP dependera de 2 campos, el campo proveedor y el campo organizacion de compras para mostrar sus valores
                                'El campo proveedor debera estar al mismo nivel que el campo proveedor ERP asi que en este caso tiene que estar en el mismo grupo
                                Dim sKeyProveedorDeProveERP As String
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyProveedorDeProveERP = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyProveedorDeProveERP = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                    End If
                                End If

                                'Si el campo proveedor esta a nivel de formulario el de organizacion de compras tambien debe estarlo, aunque no tiene que estar en el mismo grupo
                                Dim sKeyOrgComprasDeProveERP As String
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras).Length > 0 Then
                                        sKeyOrgComprasDeProveERP = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras)(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras).Length > 0 Then
                                        sKeyOrgComprasDeProveERP = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras)(0).Item("ID")
                                    End If
                                End If
                                oFSEntry.idEntryPROV = oFSEntry.IdContenedor & "_fsentry" & sKeyProveedorDeProveERP
                                oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_fsentry" & sKeyOrgComprasDeProveERP

                                If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                    Dim oProvesERP As FSNServer.CProveERPs
                                    oProvesERP = FSNServer.Get_Object(GetType(FSNServer.CProveERPs))
                                    Dim ds As DataSet = oProvesERP.CargarProveedoresERPtoDS(, , oDSRow.Item("VALOR_TEXT"))
                                    For Each dr As DataRow In ds.Tables(0).Rows
                                        oFSEntry.Text = dr("COD") & " - " & dr("DEN")
                                        oFSEntry.Valor = dr("COD")
                                        Exit For
                                    Next
                                    ds = Nothing
                                    oProvesERP = Nothing
                                End If

                                oFSEntry.Intro = 1
                            Case TiposDeDatos.TipoCampoGS.NuevoCodArticulo 'CodArticulo Solicitudes nuevas
                                Dim idCampoMat As Long
                                Dim sValorMat As String

                                idCampoMat = 0
                                If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeCargarProveSumi_CambioArt") Then
                                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeCargarProveSumi_CambioArt", "<script>var sMensajeCargarProveSumi_CambioArt = '" + JSText(oTextos.Rows.Item(49).Item(1)) + "'</script>")
                                End If
                                'OBTENEMOS EL VALOR DEL MAT OCULTO
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    For Each i In arrCamposMaterial
                                        If i < oDSRow.Item("id_campo") Then
                                            If idCampoMat < i Then
                                                idCampoMat = i
                                            End If
                                        End If
                                    Next
                                    If idCampoMat > 0 Then
                                        Try
                                            sValorMat = mdsCampos.Tables(0).Select("ID_CAMPO = " & idCampoMat)(0).Item("VALOR_TEXT")

                                            oFSEntry.DependentValue = sValorMat
                                        Catch ex As Exception

                                        End Try
                                    End If
                                Else
                                    For Each i In arrCamposMaterial
                                        If i < oDSRow.Item("ID") Then
                                            If idCampoMat < i Then
                                                idCampoMat = i
                                            End If
                                        End If
                                    Next
                                    If idCampoMat > 0 Then
                                        Try
                                            sValorMat = mdsCampos.Tables(0).Select("ID = " & idCampoMat)(0).Item("VALOR_TEXT")

                                            oFSEntry.DependentValue = sValorMat
                                        Catch ex As Exception

                                        End Try
                                    End If
                                End If

                                Dim arrMat As Array
                                arrMat = Split(Replace(sValorMat, "  ", " "), " ")

                                oFSEntry.Width = Unit.Percentage(100)
                                oFSEntry.InputStyle.CssClass = "TipoTextoMedio"
                                oFSEntry.DependentField = sKeyMaterial
                                oFSEntry.Intro = 1
                                oFSEntry.InstanciaMoneda = InstanciaMoneda
                                If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                    oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
                                    oFSEntry.codigoArticulo = oDSRow.Item("VALOR_TEXT")
                                    oFSEntry.Text = oDSRow.Item("VALOR_TEXT")
                                    oFSEntry.ToolTip = oDSRow.Item("VALOR_TEXT")
                                    oFSEntry.Denominacion = oFSEntry.Text
                                    oArts = FSNServer.Get_Object(GetType(FSNServer.Articulos))

                                    Dim oArticulo As FSNServer.PropiedadesArticulo = oArts.Articulo_Get_Properties(oDSRow.Item("VALOR_TEXT"))
                                    If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                        If oArticulo.ExisteArticulo Then
                                            oFSEntry.Concepto = oArticulo.Concepto
                                            oFSEntry.Almacenamiento = oArticulo.Almacenamiento
                                            oFSEntry.Recepcion = oArticulo.Recepcion
                                            oFSEntry.TipoRecepcion = oArticulo.TipoRecepcion
                                            oFSEntry.articuloGenerico = oArticulo.Generico
                                            oFSEntry.articuloCodificado = True
                                        End If
                                    Else
                                        If oArticulo.ExisteArticulo Then
                                            oFSEntry.Concepto = oArticulo.Concepto
                                            oFSEntry.Almacenamiento = oArticulo.Almacenamiento
                                            oFSEntry.Recepcion = oArticulo.Recepcion
                                            oFSEntry.TipoRecepcion = oArticulo.TipoRecepcion
                                            oFSEntry.articuloGenerico = oArticulo.Generico
                                        Else
                                            oFSEntry.articuloGenerico = True
                                        End If
                                    End If
                                Else
                                    oFSEntry.articuloCodificado = False
                                End If

                                oFSEntry.AnyadirArt = oDSRow.Item("ANYADIR_ART")
                                bArticuloGenerico = oFSEntry.articuloGenerico
                                sKeyTipoRecepcion = oFSEntry.TipoRecepcion
                                sKeyCodArt = oFSEntry.codigoArticulo
                                If sidDataEntryOrgCompras <> "" Then oFSEntry.idDataEntryDependent = sidDataEntryOrgCompras
                                If sIDCentro <> "" Then oFSEntry.idDataEntryDependent2 = oFSEntry.IdContenedor & "_" & "fsentry" & sIDCentro
                                If Not sIdUnidadOrganizativa = String.Empty Then oFSEntry.idDataEntryDependent3 = oFSEntry.IdContenedor & "_" & "fsentry" & sIdUnidadOrganizativa
                                If Not sCodCentroCoste = String.Empty Then oFSEntry.idDataEntryDependentCentroCoste = oFSEntry.IdContenedor & "_" & "fsentry" & sCodCentroCoste

                                Dim sKeyCantidad As String
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyCantidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyCantidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                    End If
                                End If
                                If sKeyCantidad <> "" Then
                                    oFSEntry.idEntryCANT = oFSEntry.IdContenedor & "_fsentry" & sKeyCantidad
                                End If

                                Dim sKeyUnidad As String
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyUnidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyUnidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                    End If
                                End If
                                If sKeyUnidad <> "" Then
                                    oFSEntry.idEntryUNI = oFSEntry.IdContenedor & "_fsentry" & sKeyUnidad
                                End If
                                Dim sKeyUnidadPedido As String
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyUnidadPedido = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyUnidadPedido = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                    End If
                                End If
                                If sKeyUnidadPedido <> "" Then
                                    oFSEntry.idEntryUNIPedido = oFSEntry.IdContenedor & "_fsentry" & sKeyUnidadPedido
                                End If
                                'Buscar la organizacion de compras y el centro

                                If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                    oFSEntry.DependentValueDataEntry = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                End If
                                If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Centro & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                    oFSEntry.DependentValueDataEntry2 = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Centro & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                End If
                                If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                    oFSEntry.DependentValueDataEntry3 = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                End If
                                If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                    oFSEntry.DependentValueCentroCoste = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                End If
                                If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                    oFSEntry.UnidadOculta = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                End If
                                If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                    oFSEntry.UnidadPedidoOculta = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                End If
                                If sIdProveedor Is Nothing Then
                                    If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                        If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_REL_PROVE_ART4=1").Length > 0 Then
                                            sIdProveedor = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_REL_PROVE_ART4=1")(0).Item("ID_CAMPO"))
                                        End If
                                    Else
                                        If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_REL_PROVE_ART4=1").Length > 0 Then
                                            sIdProveedor = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_REL_PROVE_ART4=1")(0).Item("ID"))
                                        End If
                                    End If
                                End If
                                If Not sIdProveedor Is Nothing Then
                                    oFSEntry.idEntryProveSumiArt = oFSEntry.IdContenedor & "_fsentry" & sIdProveedor
                                End If

                                Try
                                    If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                        oFSEntry.IdMaterialOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
                                        oFSEntry.IdDenArticuloOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
                                    Else
                                        oFSEntry.IdMaterialOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID"))
                                        oFSEntry.IdDenArticuloOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID"))
                                    End If
                                Catch ex As Exception
                                End Try

                                'idEntryPROV --> fsentry12345
                                If Not idEntryPROV Is Nothing Then
                                    oFSEntry.idEntryPROV = oFSEntry.IdContenedor & "_" & idEntryPROV
                                End If
                                If Not idEntryPREC Is Nothing Then
                                    oFSEntry.idEntryPREC = oFSEntry.IdContenedor & "_" & idEntryPREC
                                End If
                                If Not idEntryCC Is Nothing Then
                                    oFSEntry.idEntryCC = oFSEntry.IdContenedor & "_" & idEntryCC
                                End If

                            Case TiposDeDatos.TipoCampoGS.DenArticulo
                                oFSEntry.Width = Unit.Percentage(100)
                                oFSEntry.InputStyle.CssClass = "TipoTextoMedio"
                                oFSEntry.DependentField = sKeyMaterial
                                oFSEntry.Intro = 1
                                oFSEntry.Valor = DBNullToStr(oDSRow.Item("VALOR_TEXT"))
                                oFSEntry.Text = DBNullToStr(oDSRow.Item("VALOR_TEXT"))
                                oFSEntry.ToolTip = DBNullToStr(oDSRow.Item("VALOR_TEXT"))
                                oFSEntry.InstanciaMoneda = InstanciaMoneda
                                oFSEntry.MaxLength = FSNServer.LongitudesDeCodigos.giLongCodDENART
                                oFSEntry.TipoRecepcion = sKeyTipoRecepcion
                                oFSEntry.codigoArticulo = sKeyCodArt
                                Dim idCampoMat As Long
                                Dim sValorMat As String

                                If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeArtGenerico") Then
                                    Dim ArtGenericoScript As String = "<script>var sMensajeArtGenerico = '" + JSText(oTextos.Rows.Item(14).Item(1)) + "';</script>"
                                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeArtGenerico", ArtGenericoScript)
                                End If
                                If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeCargarProveSumi_CambioArt") Then
                                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeCargarProveSumi_CambioArt", "<script>var sMensajeCargarProveSumi_CambioArt = '" + JSText(oTextos.Rows.Item(49).Item(1)) + "';</script>")
                                End If
                                idCampoMat = 0
                                'OBTENEMOS EL VALOR DEL MAT OCULTO
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    For Each i In arrCamposMaterial

                                        If i < oDSRow.Item("id_campo") Then
                                            If idCampoMat < i Then
                                                idCampoMat = i

                                            End If
                                        End If
                                    Next
                                    If idCampoMat > 0 Then
                                        Try
                                            sValorMat = mdsCampos.Tables(0).Select("ID_CAMPO = " & idCampoMat)(0).Item("VALOR_TEXT")
                                            oFSEntry.DependentValue = sValorMat
                                        Catch ex As Exception

                                        End Try
                                    End If
                                Else
                                    For Each i In arrCamposMaterial
                                        If i < oDSRow.Item("ID") Then
                                            If idCampoMat < i Then
                                                idCampoMat = i
                                            End If
                                        End If
                                    Next
                                    If idCampoMat > 0 Then
                                        Try
                                            sValorMat = mdsCampos.Tables(0).Select("ID = " & idCampoMat)(0).Item("VALOR_TEXT")
                                            oFSEntry.DependentValue = sValorMat
                                        Catch ex As Exception

                                        End Try
                                    End If
                                End If
                                oFSEntry.articuloGenerico = bArticuloGenerico
                                If sidDataEntryOrgCompras <> "" Then oFSEntry.idDataEntryDependent = sidDataEntryOrgCompras
                                If sIDCentro <> "" Then oFSEntry.idDataEntryDependent2 = oFSEntry.IdContenedor & "_" & "fsentry" & sIDCentro
                                If Not sIdUnidadOrganizativa = String.Empty Then oFSEntry.idDataEntryDependent3 = oFSEntry.IdContenedor & "_" & "fsentry" & sIdUnidadOrganizativa
                                If Not sCodCentroCoste = String.Empty Then oFSEntry.idDataEntryDependentCentroCoste = oFSEntry.IdContenedor & "_" & "fsentry" & sCodCentroCoste

                                Dim sKeyCantidad As String
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyCantidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyCantidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                    End If
                                End If
                                If sKeyCantidad <> "" Then
                                    oFSEntry.idEntryCANT = oFSEntry.IdContenedor & "_fsentry" & sKeyCantidad
                                End If

                                Dim sKeyUnidad As String
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyUnidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyUnidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                    End If
                                End If
                                If sKeyUnidad <> "" Then
                                    oFSEntry.idEntryUNI = oFSEntry.IdContenedor & "_fsentry" & sKeyUnidad
                                End If

                                Dim sKeyUnidadPedido As String
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyUnidadPedido = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyUnidadPedido = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                    End If
                                End If
                                If sKeyUnidadPedido <> "" Then
                                    oFSEntry.idEntryUNIPedido = oFSEntry.IdContenedor & "_fsentry" & sKeyUnidadPedido
                                End If
                                'Buscar la organizacion de compras y el centro

                                If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                    oFSEntry.DependentValueDataEntry = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                End If
                                If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Centro & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                    oFSEntry.DependentValueDataEntry2 = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Centro & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                End If
                                If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                    oFSEntry.DependentValueDataEntry3 = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                End If
                                If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                    oFSEntry.DependentValueCentroCoste = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                End If
                                If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                    oFSEntry.UnidadOculta = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                End If
                                If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                    oFSEntry.UnidadPedidoOculta = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadPedido & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                End If

                                Try
                                    If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                        oFSEntry.IdMaterialOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
                                        oFSEntry.IdCodArticuloOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
                                    Else
                                        oFSEntry.IdMaterialOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID"))
                                        oFSEntry.IdCodArticuloOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID"))
                                    End If
                                Catch ex As Exception
                                End Try

                                'idEntryPROV --> fsentry12345
                                If Not idEntryPROV Is Nothing Then
                                    oFSEntry.idEntryPROV = oFSEntry.IdContenedor & "_" & idEntryPROV
                                End If
                                If Not idEntryPREC Is Nothing Then
                                    oFSEntry.idEntryPREC = oFSEntry.IdContenedor & "_" & idEntryPREC
                                End If
                                If Not idEntryCC Is Nothing Then
                                    oFSEntry.idEntryCC = oFSEntry.IdContenedor & "_" & idEntryCC
                                End If
                                If sIdProveedor Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_REL_PROVE_ART4=1").Length > 0 Then
                                        sIdProveedor = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_REL_PROVE_ART4=1")(0).Item("VALOR_TEXT"))
                                    End If
                                End If
                                If Not sIdProveedor Is Nothing Then
                                    oFSEntry.idEntryProveSumiArt = oFSEntry.IdContenedor & "_fsentry" & sIdProveedor
                                End If

                            Case TiposDeDatos.TipoCampoGS.Dest
                                If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                    Dim oDests As FSNServer.Destinos
                                    oDests = FSNServer.Get_Object(GetType(FSNServer.Destinos))
                                    oDests.LoadData(msIdi, oUser.CodPersona)

                                    oFSEntry.Text = TextoDelDropDown(oDests.Data, "COD", oDSRow.Item("VALOR_TEXT"), Not oFSEntry.ReadOnly, , , True)
                                    oFSEntry.ToolTip = oFSEntry.Text
                                    oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
                                End If

                                oFSEntry.Width = Unit.Percentage(100)
                                oFSEntry.Intro = 1
                                If Not IsDBNull(oFSEntry.Valor) And Not CType(Me.Page, FSNPage).Acceso.gbUsar_OrgCompras Then
                                    sCodDest = oFSEntry.Valor
                                End If
                                'Relaci�n con Almacen
                                If Not CType(Me.Page, FSNPage).Acceso.gbUsar_OrgCompras Then
                                    If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                        If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
                                            sKeyAlmacen = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                        End If
                                    Else
                                        If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
                                            sKeyAlmacen = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                        End If
                                    End If
                                    If Not sKeyAlmacen Is Nothing Then
                                        oFSEntry.idDataEntryDependent3 = oFSEntry.IdContenedor & "_" & "fsentry" + sKeyAlmacen
                                    End If
                                End If
                                sKeyDestino = oFSEntry.ID
                            Case TiposDeDatos.TipoCampoGS.Cantidad
                                oFSEntry.ErrMsg = oTextos.Rows(18).Item(1)

                                Dim sCodUnidad As String
                                Dim sql As String
                                sql = "TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO")

                                If mdsCampos.Tables(0).Select(sql).Length > 0 Then
                                    sCodUnidad = DBNullToSomething(mdsCampos.Tables(0).Select(sql)(0).Item("VALOR_TEXT"))

                                    If sCodUnidad <> Nothing Then
                                        Dim oUnis As FSNServer.Unidades
                                        oUnis = FSNServer.Get_Object(GetType(FSNServer.Unidades))
                                        oUnis.LoadData(msIdi, sCodUnidad, , False)
                                        If IsDBNull(oUnis.Data.Tables(0).Rows(0).Item("NUMDEC")) Then
                                            oFSEntry.NumeroDeDecimales = 15
                                        Else
                                            oFSEntry.NumeroDeDecimales = CInt(oUnis.Data.Tables(0).Rows(0).Item("NUMDEC"))
                                        End If
                                        oFSEntry.UnidadOculta = oUnis.Data.Tables(0).Rows(0).Item("DEN")
                                    End If
                                End If
                                If sKeyTipoRecepcion = "1" Then
                                    oFSEntry.TipoRecepcion = 1
                                End If
                                If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeRecep") Then
                                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeRecep", "<script>var smensajeRecep = '" & oTextos.Rows.Item(22).Item(1) & "'</script>")
                                End If

                            Case TiposDeDatos.TipoCampoGS.Unidad
                                Dim sKeyCantidad As String
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyCantidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sKeyCantidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Cantidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                    End If
                                End If
                                If sKeyCantidad <> "" Then
                                    oFSEntry.idEntryCANT = oFSEntry.IdContenedor & "_fsentry" & sKeyCantidad
                                End If

                                If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                    Dim oUnis As FSNServer.Unidades
                                    oUnis = FSNServer.Get_Object(GetType(FSNServer.Unidades))
                                    oUnis.LoadData(msIdi, , , False)

                                    oFSEntry.Text = TextoDelDropDown(oUnis.Data, "COD", oDSRow.Item("VALOR_TEXT"), Not oFSEntry.ReadOnly)
                                    oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
                                End If

                                oFSEntry.Width = Unit.Percentage(100)
                                oFSEntry.Intro = 1

                            Case TiposDeDatos.TipoCampoGS.UnidadPedido
                                If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                    Dim oUnis As FSNServer.Unidades
                                    oUnis = FSNServer.Get_Object(GetType(FSNServer.Unidades))
                                    oUnis.LoadData(msIdi, , , False)

                                    oFSEntry.Text = TextoDelDropDown(oUnis.Data, "COD", oDSRow.Item("VALOR_TEXT"), Not oFSEntry.ReadOnly)
                                    oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
                                End If

                                oFSEntry.Width = Unit.Percentage(100)
                                oFSEntry.Intro = 1

                            Case TiposDeDatos.TipoCampoGS.FormaPago
                                If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                    Dim oPags As FSNServer.FormasPago
                                    oPags = FSNServer.Get_Object(GetType(FSNServer.FormasPago))
                                    oPags.LoadData(msIdi)

                                    oFSEntry.Text = TextoDelDropDown(oPags.Data, "COD", oDSRow.Item("VALOR_TEXT"), Not oFSEntry.ReadOnly)
                                    oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
                                End If

                                oFSEntry.Width = Unit.Percentage(100)
                                oFSEntry.Intro = 1
                            Case TiposDeDatos.TipoCampoGS.Moneda
                                If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                    Dim oMons As FSNServer.Monedas
                                    oMons = FSNServer.Get_Object(GetType(FSNServer.Monedas))
                                    oMons.LoadData(msIdi)

                                    oFSEntry.Text = TextoDelDropDown(oMons.Data, "COD", oDSRow.Item("VALOR_TEXT"), Not oFSEntry.ReadOnly)
                                    oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
                                End If

                                oFSEntry.Width = Unit.Percentage(100)
                                oFSEntry.Intro = 1

                            Case TiposDeDatos.TipoCampoGS.Pais
                                If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                    Dim oPaises As FSNServer.Paises
                                    oPaises = FSNServer.Get_Object(GetType(FSNServer.Paises))
                                    oPaises.LoadData(msIdi)

                                    oFSEntry.Text = TextoDelDropDown(oPaises.Data, "COD", oDSRow.Item("VALOR_TEXT"), Not oFSEntry.ReadOnly)
                                    sCodPais = oDSRow.Item("VALOR_TEXT")
                                    oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
                                End If
                                sKeyPais = oFSEntry.ClientID

                                oFSEntry.Intro = 1

                                'Relaci�n con Provincia
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Provincia & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
                                        sKeyProvincia = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Provincia & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Provincia & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
                                        sKeyProvincia = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Provincia & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                    End If
                                End If
                                If Not sKeyProvincia Is Nothing Then
                                    oFSEntry.DependentField = "fsentry" + sKeyProvincia
                                End If
                            Case TiposDeDatos.TipoCampoGS.Provincia
                                'Provincia es un campo relacionado por lo que la carga de datos se hace desde javascript(ajax)
                                'Esta carga se hace �nicamente cuando el campo tiene un valor por defecto y hay que buscar la descripci�n del valor por defecto
                                If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                    If Not String.IsNullOrEmpty(sCodPais) Then
                                        Dim oProvis As FSNServer.Provincias
                                        oProvis = FSNServer.Get_Object(GetType(FSNServer.Provincias))
                                        oProvis.Pais = DBNullToStr(sCodPais)
                                        oProvis.LoadData(msIdi)
                                        'Como texto ponemos el c�digo y la descripci�n porque si no puede haber problemas al ser controles que cargan los datos desde javascript y tener Infragistics 11.1 limitaciones en ese sentido
                                        oFSEntry.Text = TextoDelDropDown(oProvis.Data, "COD", oDSRow.Item("VALOR_TEXT"), True, "PAICOD", sCodPais) 'sDenominacion
                                        oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
                                    End If
                                End If

                                If sKeyPais <> String.Empty Then _
                             oFSEntry.MasterField = sKeyPais

                                oFSEntry.Intro = 1
                            Case TiposDeDatos.TipoCampoGS.PRES1, TiposDeDatos.TipoCampoGS.Pres2, TiposDeDatos.TipoCampoGS.Pres3, TiposDeDatos.TipoCampoGS.Pres4
                                oFSEntry.Width = Unit.Percentage(100)
                                oFSEntry.Valor = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
                                If oFSEntry.Valor = Nothing Then
                                    Select Case oFSEntry.TipoGS
                                        Case TiposDeDatos.TipoCampoGS.PRES1
                                            If oUser.Pres1 <> Nothing Then
                                                iNivel = Left(CStr(oUser.Pres1), 1)
                                                lIdPresup = Mid(CStr(oUser.Pres1), 2)
                                                oFSEntry.Valor = iNivel.ToString + "_" + lIdPresup.ToString + "_1"
                                            End If
                                        Case TiposDeDatos.TipoCampoGS.Pres2
                                            If oUser.Pres2 <> Nothing Then
                                                iNivel = Left(CStr(oUser.Pres2), 1)
                                                lIdPresup = Mid(CStr(oUser.Pres2), 2)
                                                oFSEntry.Valor = iNivel.ToString + "_" + lIdPresup.ToString + "_1"
                                            End If
                                        Case TiposDeDatos.TipoCampoGS.Pres3
                                            If oUser.Pres3 <> Nothing Then
                                                iNivel = Left(CStr(oUser.Pres3), 1)
                                                lIdPresup = Mid(CStr(oUser.Pres3), 2)
                                                oFSEntry.Valor = iNivel.ToString + "_" + lIdPresup.ToString + "_1"
                                            End If
                                        Case TiposDeDatos.TipoCampoGS.Pres4
                                            If oUser.Pres4 <> Nothing Then
                                                iNivel = Left(CStr(oUser.Pres4), 1)
                                                lIdPresup = Mid(CStr(oUser.Pres4), 2)
                                                oFSEntry.Valor = iNivel.ToString + "_" + lIdPresup.ToString + "_1"
                                            End If
                                    End Select

                                End If
                                If oFSEntry.Valor <> Nothing Then
                                    Dim arrPresupuestos() As String = oFSEntry.Valor.split("#")
                                    Dim oPresup As String
                                    Dim arrPresup(2) As String
                                    Dim iContadorPres As Integer
                                    Dim arrDenominaciones() As String
                                    If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") And Not DBNullToSomething(oDSRow.Item("VALOR_TEXT")) = Nothing Then
                                        arrDenominaciones = oDSRow.Item("VALOR_TEXT_DEN").ToString().Split("#")
                                        If oDSRow.Item("VALOR_TEXT_COD") Then
                                            oFSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoPresBajaLog
                                        Else
                                            oFSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
                                        End If
                                        sDenominacion = oDSRow.Item("VALOR_TEXT_DEN")
                                    Else
                                        oFSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
                                    End If
                                    iContadorPres = 0
                                    sTexto = ""
                                    sValor = ""
                                    sDenominacion = ""
                                    For Each oPresup In arrPresupuestos
                                        iContadorPres = iContadorPres + 1
                                        arrPresup = oPresup.Split("_")
                                        iNivel = arrPresup(0)
                                        lIdPresup = arrPresup(1)
                                        'Los presupuestos siempre llevan el porcentaje como 0.xx salvo cuando es 100% que llevan 1
                                        dPorcent = Numero(arrPresup(2), ".", "")

                                        If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") And Not DBNullToSomething(oDSRow.Item("VALOR_TEXT")) = Nothing Then
                                            sTexto = arrDenominaciones(iContadorPres - 1) & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); " & sTexto
                                            sValor += oPresup + "#"
                                        Else
                                            Select Case oFSEntry.TipoGS
                                                Case TiposDeDatos.TipoCampoGS.PRES1
                                                    oPres1 = FSNServer.Get_Object(GetType(FSNServer.PresProyectosNivel1))
                                                    Select Case iNivel
                                                        Case 1
                                                            oPres1.LoadData(lIdPresup)
                                                        Case 2
                                                            oPres1.LoadData(Nothing, lIdPresup)
                                                        Case 3
                                                            oPres1.LoadData(Nothing, Nothing, lIdPresup)
                                                        Case 4
                                                            oPres1.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                                                    End Select
                                                    oDS = oPres1.Data
                                                    oPres1 = Nothing
                                                Case TiposDeDatos.TipoCampoGS.Pres2
                                                    oPres2 = FSNServer.Get_Object(GetType(FSNServer.PresContablesNivel1))
                                                    Select Case iNivel
                                                        Case 1
                                                            oPres2.LoadData(lIdPresup)
                                                        Case 2
                                                            oPres2.LoadData(Nothing, lIdPresup)
                                                        Case 3
                                                            oPres2.LoadData(Nothing, Nothing, lIdPresup)
                                                        Case 4
                                                            oPres2.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                                                    End Select
                                                    oDS = oPres2.Data
                                                    oPres2 = Nothing
                                                Case TiposDeDatos.TipoCampoGS.Pres3
                                                    oPres3 = FSNServer.Get_Object(GetType(FSNServer.PresConceptos3Nivel1))
                                                    Select Case iNivel
                                                        Case 1
                                                            oPres3.LoadData(lIdPresup)
                                                        Case 2
                                                            oPres3.LoadData(Nothing, lIdPresup)
                                                        Case 3
                                                            oPres3.LoadData(Nothing, Nothing, lIdPresup)
                                                        Case 4
                                                            oPres3.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                                                    End Select
                                                    oDS = oPres3.Data
                                                    oPres3 = Nothing
                                                Case TiposDeDatos.TipoCampoGS.Pres4
                                                    oPres4 = FSNServer.Get_Object(GetType(FSNServer.PresConceptos4Nivel1))
                                                    Select Case iNivel
                                                        Case 1
                                                            oPres4.LoadData(lIdPresup)
                                                        Case 2
                                                            oPres4.LoadData(Nothing, lIdPresup)
                                                        Case 3
                                                            oPres4.LoadData(Nothing, Nothing, lIdPresup)
                                                        Case 4
                                                            oPres4.LoadData(Nothing, Nothing, Nothing, lIdPresup)
                                                    End Select
                                                    oDS = oPres4.Data
                                                    oPres4 = Nothing
                                            End Select
                                            Dim iAnyo As Integer

                                            If Not oDS Is Nothing Then

                                                If oDS.Tables(0).Rows.Count > 0 Then

                                                    With oDS.Tables(0).Rows(0)

                                                        'comprobamos que el valor por defecto cumpla la restricci�n del usuario. SI NO CUMPLE, NO APARECER�
                                                        If Not oDS.Tables(0).Columns("ANYO") Is Nothing Then
                                                            iAnyo = .Item("ANYO")

                                                        End If

                                                        Dim bSeleccionado As Boolean = True
                                                        Dim sPorcentaje As String
                                                        Dim sDenPresup As String
                                                        For i = 4 To 1 Step -1
                                                            'PRESi:
                                                            If Not IsDBNull(.Item("PRES" & i)) Then
                                                                If bSeleccionado Then
                                                                    sPorcentaje = " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
                                                                Else
                                                                    sPorcentaje = " - "
                                                                End If
                                                                sTexto = .Item("PRES" & i).ToString & sPorcentaje & sTexto
                                                                sDenPresup = " - " & .Item("PRES" & i).ToString
                                                                bSeleccionado = False
                                                            End If
                                                        Next
                                                        If sDenominacion <> "" Then sDenominacion = "#" & sDenominacion
                                                        If Not oDS.Tables(0).Columns("ANYO") Is Nothing Then
                                                            sTexto = .Item("ANYO").ToString & " - " & sTexto
                                                            sDenPresup = .Item("ANYO").ToString & sDenPresup
                                                        End If
                                                        sDenominacion = sDenPresup & sDenominacion

                                                        If (.Item("BAJALOG")) Then
                                                            oFSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoPresBajaLog
                                                        End If

                                                        sValor += oPresup + "#"

                                                    End With
                                                End If
                                            End If
                                        End If
                                    Next
                                    If sValor <> Nothing Then
                                        sValor = Left(sValor, sValor.Length - 1)
                                    End If
                                    oFSEntry.Valor = sValor
                                    oFSEntry.Denominacion = sDenominacion
                                    If sTexto = "" Then
                                        If Not oFSEntry.ReadOnly Then
                                            oFSEntry.Text = oTextos.Rows(1).Item(1)
                                        End If
                                    Else
                                        oFSEntry.Text = sTexto
                                    End If

                                Else
                                    If Not oFSEntry.ReadOnly Then
                                        oFSEntry.Text = oTextos.Rows(1).Item(1)
                                    End If
                                End If

                                If sKeyOrgCompras Is Nothing Then
                                    For Each oRow As DataRow In mdsCampos.Tables(0).Rows
                                        If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.OrganizacionCompras And oRow.Item("VISIBLE") = 1 Then
                                            If Not mdsCampos.Tables(0).Columns("ID_CAMPO") Is Nothing Then
                                                sKeyOrgCompras = "fsentry" + oRow.Item("ID_CAMPO").ToString
                                            Else
                                                sKeyOrgCompras = "fsentry" + oRow.Item("ID").ToString
                                            End If
                                            Exit For
                                        End If
                                    Next
                                End If
                                If Not (sKeyOrgCompras Is Nothing) Then _
                             oFSEntry.DependentField = sKeyOrgCompras

                            Case TiposDeDatos.TipoCampoGS.Proveedor
                                Dim oCampo As FSNServer.Campo
                                oCampo = FSNServer.Get_Object(GetType(FSNServer.Campo))

                                oCampo.Id = oDSRow.Item("ID")
                                oCampo.IdInstancia = mlInstancia
                                oCampo.IdSolicitud = mlSolicitud
                                If oCampo.ProveedorParticipanteAsociado Then
                                    oFSEntry.EsProveedorParticipante = True
                                Else
                                    oFSEntry.EsProveedorParticipante = False
                                End If
                                If Not IsDBNull(oDSRow.Item("CARGAR_REL_PROVE_ART4")) Then
                                    oFSEntry.CargarProveSumiArt = oDSRow.Item("CARGAR_REL_PROVE_ART4")
                                Else
                                    oFSEntry.CargarProveSumiArt = False
                                End If
                                If Not IsDBNull(oDSRow.Item("CARGAR_ULT_ADJ")) Then
                                    oFSEntry.CargarUltADJ = oDSRow.Item("CARGAR_ULT_ADJ")
                                Else
                                    oFSEntry.CargarUltADJ = False
                                End If
                                If Not IsDBNull(oDSRow.Item("CARGAR_ULT_ADJ")) AndAlso oDSRow.Item("CARGAR_ULT_ADJ") <> 0 OrElse
                               Not IsDBNull(oDSRow.Item("CARGAR_REL_PROVE_ART4")) AndAlso oDSRow.Item("CARGAR_REL_PROVE_ART4") <> 0 Then
                                    Dim mioFSDSEntry As DataEntry.GeneralEntry
                                    Dim sCampoCodArt, sCampoCodArtAnt As String
                                    Dim sCampoDenArt As String
                                    If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                        If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
                                            sCampoCodArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID_CAMPO")
                                        End If
                                        If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
                                            sCampoDenArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID_CAMPO")
                                        End If
                                        If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
                                            sCampoCodArtAnt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID_CAMPO")
                                        End If
                                    Else
                                        If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
                                            sCampoCodArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID")
                                        End If
                                        If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
                                            sCampoDenArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID")
                                        End If
                                        If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
                                            sCampoCodArtAnt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID")
                                        End If
                                    End If

                                    If IsDBNull(oDSRow.Item("CARGAR_REL_PROVE_ART4")) Or oDSRow.Item("CARGAR_REL_PROVE_ART4") = 0 Then
                                        mioFSDSEntry = Me.FindControl("fsentry" & sCampoCodArt)
                                        If Not mioFSDSEntry Is Nothing Then
                                            mioFSDSEntry.WindowTitle = Me.Page.Title
                                            mioFSDSEntry.idEntryPROV = oFSEntry.IdContenedor & "_" & oFSEntry.ClientID
                                        Else
                                            idEntryPROV = oFSEntry.ClientID
                                        End If
                                        mioFSDSEntry = Me.FindControl("fsentry" & sCampoDenArt)

                                        If Not mioFSDSEntry Is Nothing Then
                                            mioFSDSEntry.WindowTitle = Me.Page.Title
                                            mioFSDSEntry.idEntryPROV = oFSEntry.IdContenedor & "_" & oFSEntry.ClientID
                                        Else
                                            idEntryPROV = oFSEntry.ClientID
                                        End If
                                        mioFSDSEntry = Me.FindControl("fsentry" & sCampoCodArtAnt)

                                        If Not mioFSDSEntry Is Nothing Then
                                            mioFSDSEntry.WindowTitle = Me.Page.Title
                                            mioFSDSEntry.idEntryPROV = oFSEntry.IdContenedor & "_" & oFSEntry.ClientID
                                        Else
                                            idEntryPROV = oFSEntry.ClientID
                                        End If
                                    Else
                                        oFSEntry.idEntryART = oFSEntry.IdContenedor & "_fsentry" & sCampoCodArt
                                    End If
                                End If

                                If oFSEntry.Valor <> Nothing Then
                                    If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                        sDenominacion = oDSRow.Item("VALOR_TEXT_DEN")
                                    Else
                                        oProve = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                                        oProve.Cod = oFSEntry.Valor
                                        oProve.Load(msIdi, True)
                                        sDenominacion = oProve.Den
                                    End If
                                    oFSEntry.Text = oFSEntry.Valor + " - " + sDenominacion.Replace("'", "")
                                    oFSEntry.Denominacion = sDenominacion
                                ElseIf Not oFSEntry.ReadOnly Then
                                    oFSEntry.Text = oTextos.Rows(2).Item(1)
                                    'Guardamos en la variable el texto para saber cuando est� vacio
                                    If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "TextoProveVacio") Then
                                        Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "TextoProveVacio",
                                            "<script>var sTextoProveVacio = '" + oTextos.Rows(2).Item(1) + "';</script>")
                                    End If
                                End If
                                Dim sql As String
                                Dim sCampoDependent As String
                                sql = "(TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & ") AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 "

                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select(sql).Length > 0 Then
                                        sCampoDependent = mdsCampos.Tables(0).Select(sql)(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select(sql).Length > 0 Then
                                        sCampoDependent = mdsCampos.Tables(0).Select(sql)(0).Item("ID")
                                    End If
                                End If
                                If sCampoDependent <> "" Then ' Relacionar la Organizacion de Compras con el Centro
                                    oFSEntry.DependentField = oFSEntry.IdContenedor & "_" & "fsentry" & sCampoDependent
                                End If

                                'Relaci�n con Forma pago
                                If oFSEntry.TipoSolicit = TiposDeDatos.TipoDeSolicitud.PedidoNegociado OrElse oFSEntry.TipoSolicit = TiposDeDatos.TipoDeSolicitud.PedidoExpress Then
                                    Dim sValorFPago As String
                                    If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                        If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.FormaPago & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 0 ").Length > 0 Then
                                            sKeyFPago = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.FormaPago & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                            sValorFPago = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.FormaPago & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                        End If
                                    Else
                                        If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.FormaPago & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 0 ").Length > 0 Then
                                            sKeyFPago = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.FormaPago & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                            sValorFPago = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.FormaPago & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                        End If
                                    End If
                                    If Not sKeyFPago Is Nothing Then
                                        oFSEntry.idDataEntryDependent3 = oFSEntry.IdContenedor & "_" & "fsentry" + sKeyFPago
                                        oFSEntry.FPagoOculta = sValorFPago
                                    End If
                                End If
                            Case TiposDeDatos.TipoCampoGS.Persona, TiposDeDatos.TipoCampoGS.Comprador
                                If oFSEntry.Valor <> Nothing Then
                                    If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                        sDenominacion = DBNullToStr(oDSRow.Item("VALOR_TEXT_DEN"))
                                    Else
                                        Dim oPersona As FSNServer.Persona = FSNServer.Get_Object(GetType(FSNServer.Persona))
                                        oPersona.LoadData(oFSEntry.Valor.ToString)
                                        sDenominacion = oPersona.Denominacion(oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.Persona)
                                    End If
                                    oFSEntry.Text = sDenominacion
                                    oFSEntry.Denominacion = sDenominacion
                                End If
                                oFSEntry.Intro = 0
                                If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeComprador") Then
                                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeComprador", "<script>var smensajeComprador = '" & oTextos.Rows.Item(23).Item(1) & "'</script>")
                                End If
                            Case TiposDeDatos.TipoCampoGS.Departamento

                                If sKeyUnidadOrganizativa <> String.Empty Then _
                             oFSEntry.MasterField = sKeyUnidadOrganizativa

                                bDepBaja = False

                                'El departamento puede depender de la unidad organizativa o no.
                                '   En la funci�n CargarValoresDefecto, vaciaremos la lista porque un webdropdown 11.1 con carga desde javascript habilitada no puede cargarse en servidor, 
                                '   Los datos salen mal en pantalla
                                If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                    Dim oDepartamentos As FSNServer.Departamentos
                                    oDepartamentos = FSNServer.Get_Object(GetType(FSNServer.Departamentos))
                                    If bDepartamentoRelacionado Then
                                        Dim vUnidadesOrganizativas(3) As String
                                        sUnidadOrganizativa = DBNullToStr(NothingToDBNull(sUnidadOrganizativa))
                                        vUnidadesOrganizativas = Split(sUnidadOrganizativa, " - ")
                                        Select Case UBound(vUnidadesOrganizativas)
                                            Case 0
                                                If sUnidadOrganizativa <> "" Then
                                                    oDepartamentos.LoadData(0)
                                                    oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                                                End If
                                            Case 1
                                                oDepartamentos.LoadData(1, vUnidadesOrganizativas(0))
                                                oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                                            Case 2
                                                oDepartamentos.LoadData(2, vUnidadesOrganizativas(0), vUnidadesOrganizativas(1))
                                                oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                                            Case 3
                                                oDepartamentos.LoadData(3, vUnidadesOrganizativas(0), vUnidadesOrganizativas(1), vUnidadesOrganizativas(2))
                                                oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                                        End Select

                                    Else
                                        oDepartamentos.LoadData()
                                        oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                                    End If

                                    'Como texto ponemos el c�digo y la descripci�n porque si no puede haber problemas al ser controles que cargan los datos desde javascript y tener Infragistics 11.1 limitaciones en ese sentido
                                    oFSEntry.Text = TextoDelDropDown(oDepartamentos.Data, "COD", oDSRow.Item("VALOR_TEXT"), True)

                                    If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                        If oDSRow.Item("VALOR_TEXT_COD") = 1 Then bDepBaja = True
                                    Else
                                        Dim oDepartamento As FSNServer.Departamento
                                        oDepartamento = FSNServer.Get_Object(GetType(FSNServer.Departamento))
                                        oDepartamento.Cod = oDSRow.Item("VALOR_TEXT")

                                        If oDepartamento.EstaDeBaja(oDepartamento.Cod) Then bDepBaja = True

                                        oDepartamento = Nothing
                                    End If
                                    oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
                                End If

                                oFSEntry.Intro = 1

                            Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
                                If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                    Dim oOrganizacionesCompras As FSNServer.OrganizacionesCompras
                                    oOrganizacionesCompras = FSNServer.Get_Object(GetType(FSNServer.OrganizacionesCompras))
                                    oOrganizacionesCompras.LoadData(oUser.Cod, oUser.PMRestriccionUONSolicitudAUONUsuario, oUser.PMRestriccionUONSolicitudAUONPerfil)

                                    oFSEntry.Text = TextoDelDropDown(oOrganizacionesCompras.Data, "COD", oDSRow.Item("VALOR_TEXT"), Not oFSEntry.ReadOnly)
                                    oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
                                End If
                                oFSEntry.Intro = 1
                                lOrdenOrgCompras = oFSEntry.Orden
                                Dim sCampoDependent As String
                                Dim sql As String
                                sql = "(TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS =" & TiposDeDatos.TipoCampoGS.DenArticulo & " OR TIPO_CAMPO_GS =" & TiposDeDatos.TipoCampoGS.CodArticulo & ") AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 "

                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select(sql).Length > 0 Then
                                        sCampoDependent = mdsCampos.Tables(0).Select(sql)(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select(sql).Length > 0 Then
                                        sCampoDependent = mdsCampos.Tables(0).Select(sql)(0).Item("ID")
                                    End If
                                End If
                                If sCampoDependent <> "" Then
                                    oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry" & sCampoDependent
                                End If

                                sql = "(TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Centro & ") AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1"

                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select(sql).Length > 0 Then
                                        sCampoDependent = mdsCampos.Tables(0).Select(sql)(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select(sql).Length > 0 Then
                                        sCampoDependent = mdsCampos.Tables(0).Select(sql)(0).Item("ID")
                                    End If
                                End If
                                If sCampoDependent <> "" Then ' Relacionar la Organizacion de Compras con el Centro
                                    oFSEntry.idDataEntryDependent2 = oFSEntry.IdContenedor & "_" & "fsentry" & sCampoDependent
                                    'Buscar el valor del centro
                                    Try
                                        oFSEntry.DependentValueDataEntry2 = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Centro & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
                                    Catch ex As Exception

                                    End Try
                                End If

                                sql = "(TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & ") AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 "

                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select(sql).Length > 0 Then
                                        sCampoDependent = mdsCampos.Tables(0).Select(sql)(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select(sql).Length > 0 Then
                                        sCampoDependent = mdsCampos.Tables(0).Select(sql)(0).Item("ID")
                                    End If
                                End If
                                If sCampoDependent <> "" Then ' Relacionar la Organizacion de Compras con el Proveedor
                                    oFSEntry.DependentField = oFSEntry.IdContenedor & "_" & "fsentry" & sCampoDependent
                                End If

                                sidDataEntryOrgCompras = oFSEntry.IdContenedor & "_" & "fsentry" & oFSEntry.Tag

                                sKeyOrgComprasDataCheck = oFSEntry.ClientID
                            Case TiposDeDatos.TipoCampoGS.Centro
                                If sKeyOrgComprasDataCheck <> String.Empty Then _
                             oFSEntry.MasterField = sKeyOrgComprasDataCheck

                                'Centro es un campo relacionado por lo que la carga de datos se hace desde javascript(ajax)
                                'Esta carga se hace �nicamente cuando el campo tiene un valor por defecto y hay que buscar la descripci�n del valor por defecto
                                If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                    Dim oCentros As FSNServer.Centros
                                    oCentros = FSNServer.Get_Object(GetType(FSNServer.Centros))
                                    sCodOrgCompras = DBNullToStr(NothingToDBNull(sCodOrgCompras))
                                    If sCodOrgCompras <> "" Then 'bCentroRelacionado
                                        oCentros.LoadData(oUser.Cod, oUser.PMRestriccionUONSolicitudAUONUsuario, oUser.PMRestriccionUONSolicitudAUONPerfil, sCodOrgCompras)
                                    Else
                                        oCentros.LoadData(oUser.Cod, oUser.PMRestriccionUONSolicitudAUONUsuario, oUser.PMRestriccionUONSolicitudAUONPerfil)
                                    End If

                                    'Como texto ponemos el c�digo y la descripci�n porque si no puede haber problemas al ser controles que cargan los datos desde javascript y tener Infragistics 11.1 limitaciones en ese sentido
                                    oFSEntry.Text = TextoDelDropDown(oCentros.Data, "COD", oDSRow.Item("VALOR_TEXT"), True)

                                    oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
                                End If
                                Dim sScript As String
                                ''Variables de la ORGANIZACION DE COMPRAS (Carga las Varibles JS de la pag jsAlta.js)                          ''***************************************
                                sScript = "var sCodOrgComprasFORM" & oFSEntry.Tag & "=null;var bCentroRelacionadoFORM" & oFSEntry.Tag & "=false;"

                                sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                                If Not Page.ClientScript.IsStartupScriptRegistered("VariablesORGANIZACIONCOMPRAS" & oFSEntry.Tag) Then
                                    Page.ClientScript.RegisterStartupScript(Page.GetType, "VariablesORGANIZACIONCOMPRAS" & oFSEntry.Tag, sScript)
                                End If

                                oFSEntry.Intro = 1
                                sIDCentro = oFSEntry.Tag
                                lOrdenCentro = oFSEntry.Orden
                                'Relacionar el Centro con el articulo, (Para cuando se elimine el centro que se elimine el articulo)
                                Dim sCampoDepend As String
                                Dim sql As String
                                sql = "(TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS =" & TiposDeDatos.TipoCampoGS.DenArticulo & ") AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 "

                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select(sql).Length > 0 Then
                                        sCampoDepend = mdsCampos.Tables(0).Select(sql)(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select(sql).Length > 0 Then
                                        sCampoDepend = mdsCampos.Tables(0).Select(sql)(0).Item("ID")
                                    End If
                                End If
                                oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry" & sCampoDepend
                                sIdDataEntryCentro = oFSEntry.IdContenedor & "_" & "fsentry" & oFSEntry.Tag
                                oFSEntry.DependentValue = sCodOrgCompras

                                'Relaci�n con Almacen
                                If CType(Me.Page, FSNPage).Acceso.gbUsar_OrgCompras Then
                                    If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                        If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
                                            sKeyAlmacen = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                        End If
                                    Else
                                        If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
                                            sKeyAlmacen = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                        End If
                                    End If
                                    If Not sKeyAlmacen Is Nothing Then
                                        oFSEntry.idDataEntryDependent3 = oFSEntry.IdContenedor & "_" & "fsentry" + sKeyAlmacen
                                    End If
                                End If
                                sKeyCentro = oFSEntry.ClientID
                            Case TiposDeDatos.TipoCampoGS.Almacen
                                'El Almac�n puede depender del centro o no.
                                '   En la funci�n CargarValoresDefecto, vaciaremos la lista porque un webdropdown 11.1 con carga desde javascript habilitada no puede cargarse en servidor, 
                                '   Los datos salen mal en pantalla
                                If Not IsDBNull(oDSRow.Item("VALOR_NUM")) Or sCodDest <> "" Then
                                    Dim oAlmacenes As FSNServer.Almacenes
                                    oAlmacenes = FSNServer.Get_Object(GetType(FSNServer.Almacenes))
                                    If Not IsDBNull(oDSRow.Item("VALOR_NUM")) Then
                                        If sIDCentro = "" Then
                                            oAlmacenes.LoadData()
                                        Else
                                            oAlmacenes.LoadData(DBNullToStr(sCodCentro))
                                        End If

                                        'Como texto ponemos el c�digo y la descripci�n porque si no puede haber problemas al ser controles que cargan los datos desde javascript y tener Infragistics 11.1 limitaciones en ese sentido
                                        oFSEntry.Text = TextoDelDropDown(oAlmacenes.Data, "ID", oDSRow.Item("VALOR_NUM"), True)
                                        oFSEntry.Valor = oDSRow.Item("VALOR_NUM")
                                    ElseIf sCodDest <> "" Then
                                        oAlmacenes.LoadData(, sCodDest)
                                        If oAlmacenes.Data.Tables(0).Rows.Count = 1 Then
                                            oFSEntry.Valor = oAlmacenes.Data.Tables(0).Rows(0).Item("ID").ToString
                                            oFSEntry.Text = TextoDelDropDown(oAlmacenes.Data, "ID", oAlmacenes.Data.Tables(0).Rows(0).Item("ID").ToString, True)
                                        End If
                                    End If
                                End If

                                oFSEntry.Intro = 1

                                If CType(Me.Page, FSNPage).Acceso.gbUsar_OrgCompras Then
                                    If sKeyCentro IsNot Nothing AndAlso sKeyCentro <> String.Empty Then
                                        oFSEntry.MasterField = sKeyCentro
                                    End If
                                Else
                                    If sKeyDestino IsNot Nothing AndAlso sKeyDestino <> String.Empty Then
                                        oFSEntry.MasterField = sKeyDestino
                                    End If
                                End If
                            Case TiposDeDatos.TipoCampoGS.Empresa
                                If Not IsDBNull(oDSRow.Item("VALOR_NUM")) Then
                                    Dim oEmpresa As FSNServer.Empresa
                                    oEmpresa = FSNServer.Get_Object(GetType(FSNServer.Empresa))
                                    oEmpresa.ID = oDSRow.Item("VALOR_NUM")
                                    oEmpresa.Load(msIdi)
                                    If Not String.IsNullOrEmpty(oEmpresa.NIF) Then oFSEntry.Text = oEmpresa.NIF & " - " & oEmpresa.Den
                                    oEmpresa = Nothing

                                    oFSEntry.Valor = oDSRow.Item("VALOR_NUM")
                                End If
                                oFSEntry.Width = Unit.Percentage(100)
                            Case TiposDeDatos.TipoCampoGS.IniSuministro
                                If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechas") Then
                                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechas", "<script>var sMensajeFecha = '" + JSText(oTextos.Rows.Item(10).Item(1)) + "';</script>")
                                End If
                                If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechasDefecto") Then
                                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechasDefecto", "<script>var sMensajeFechaDefecto = '" + JSText(oTextos.Rows.Item(17).Item(1)) + "';</script>")
                                End If
                                If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "FechaNoAnteriorAlSistema") Then
                                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "FechaNoAnteriorAlSistema", "<script>var sFechaNoAnteriorAlSistema = '" + JSText(oTextos.Rows.Item(48).Item(1)) + "';</script>")
                                End If
                                Dim sIdCampo As String
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.FinSuministro & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sIdCampo = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.FinSuministro & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.FinSuministro & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sIdCampo = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.FinSuministro & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                    End If
                                End If
                                oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_fsentry" & sIdCampo
                                oFSEntry.FechaNoAnteriorAlSistema = DBNullToBoolean(oDSRow.Item("FECHA_VALOR_NO_ANT_SIS"))
                            Case TiposDeDatos.TipoCampoGS.FinSuministro
                                If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechas") Then
                                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechas", "<script>var sMensajeFecha = '" + JSText(oTextos.Rows.Item(10).Item(1)) + "';</script>")
                                End If
                                If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechasDefecto") Then
                                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechasDefecto", "<script>var sMensajeFechaDefecto = '" + JSText(oTextos.Rows.Item(17).Item(1)) + "';</script>")
                                End If
                                If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "FechaNoAnteriorAlSistema") Then
                                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "FechaNoAnteriorAlSistema", "<script>var sFechaNoAnteriorAlSistema = '" + JSText(oTextos.Rows.Item(48).Item(1)) + "';</script>")
                                End If
                                Dim sIdCampo As String
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.IniSuministro & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sIdCampo = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.IniSuministro & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.IniSuministro & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                        sIdCampo = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.IniSuministro & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                    End If
                                End If
                                oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_fsentry" & sIdCampo
                                oFSEntry.FechaNoAnteriorAlSistema = DBNullToBoolean(oDSRow.Item("FECHA_VALOR_NO_ANT_SIS"))
                            Case TiposDeDatos.TipoCampoGS.ImporteSolicitudesVinculadas
                                oFSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoTextoCorto
                                If ((mlInstanciaEstado = TipoEstadoSolic.SCPendiente) Or (mlInstanciaEstado = TipoEstadoSolic.SCAprobada) Or (mlInstanciaEstado = TipoEstadoSolic.SCCerrada)) Then
                                    Dim oInstancia As FSNServer.Instancia
                                    Dim aux As String
                                    oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                                    oInstancia.ID = mlInstancia

                                    aux = oInstancia.DevolverImporteSolicitudesVinculadas()
                                    oFSEntry.Text = FSNLibrary.FormatNumber(Replace(aux, ".", ","), oUser.NumberFormat)
                                    oFSEntry.Valor = aux
                                    oFSEntry.Intro = 1
                                    oInstancia = Nothing
                                Else
                                    oFSEntry.ReadOnly = True
                                End If

                            Case TiposDeDatos.TipoCampoGS.ImporteRepercutido
                                If Not IsDBNull(oDSRow.Item("VALOR_NUM")) Then
                                    oFSEntry.Valor = oDSRow.Item("VALOR_NUM")
                                End If
                                If mlInstancia > 0 Then
                                    oFSEntry.MonRepercutido = oDSRow.Item("VALOR_TEXT")
                                    oFSEntry.CambioRepercutido = oDSRow.Item("CAMBIO")
                                Else
                                    oFSEntry.MonRepercutido = oDSRow.Item("MONCEN")
                                    oFSEntry.CambioRepercutido = oDSRow.Item("CAMBIOCEN")
                                End If

                            Case TiposDeDatos.TipoCampoGS.RefSolicitud
                                If Not oFSEntry.ReadOnly Then
                                    If IsDBNull(oDSRow.Item("VALOR_NUM")) Then
                                        oFSEntry.Text = oTextos.Rows(11).Item(1) '(Pulse el bot�n para buscar una solicitud)
                                    Else
                                        Dim oInstancia As FSNServer.Instancia
                                        oFSEntry.Valor = oDSRow.Item("VALOR_NUM")
                                        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                                        oInstancia.ID = oDSRow.Item("VALOR_NUM")
                                        Dim sTitulo As String = oInstancia.DevolverTitulo(oUser.Idioma.ToString())
                                        If sTitulo <> "" Then
                                            oFSEntry.Text = oDSRow.Item("VALOR_NUM") & " - " & sTitulo
                                        Else
                                            oFSEntry.Text = oDSRow.Item("VALOR_NUM")
                                        End If
                                        oInstancia = Nothing

                                    End If
                                    oFSEntry.idRefSolicitud = IIf(IsDBNull(oDSRow.Item("IDREFSOLICITUD")), 0, oDSRow.Item("IDREFSOLICITUD"))
                                Else
                                    If Not IsDBNull(oDSRow.Item("VALOR_NUM")) Then
                                        Dim oInstancia As FSNServer.Instancia
                                        Dim sTitulo As String
                                        oFSEntry.Valor = oDSRow.Item("VALOR_NUM")
                                        oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))

                                        oInstancia.ID = oDSRow.Item("VALOR_NUM")
                                        sTitulo = oInstancia.DevolverTitulo(oUser.Idioma.ToString())
                                        If sTitulo <> "" Then
                                            oFSEntry.Text = oDSRow.Item("VALOR_NUM") & " - " & sTitulo
                                        Else
                                            oFSEntry.Text = oDSRow.Item("VALOR_NUM")
                                        End If
                                        oInstancia = Nothing

                                    End If
                                End If
                                oFSEntry.Width = Unit.Percentage(100)
                                oFSEntry.InstanciaMoneda = InstanciaMoneda
                                If IsDBNull(oDSRow.Item("AVISOBLOQUEOIMPACUM")) Then
                                    oFSEntry.AvisoBloqueoRefSOL = -1
                                Else
                                    oFSEntry.AvisoBloqueoRefSOL = oDSRow.Item("AVISOBLOQUEOIMPACUM")
                                End If

                            Case TiposDeDatos.TipoCampoGS.PrecioUnitario
                                If Not IsDBNull(oDSRow.Item("CARGAR_ULT_ADJ")) Then
                                    oFSEntry.CargarUltADJ = oDSRow.Item("CARGAR_ULT_ADJ")
                                    If oFSEntry.CargarUltADJ = True Then
                                        Dim mioFSDSEntry As DataEntry.GeneralEntry
                                        Dim sCampoCodArt, sCampoCodArtAnt As String
                                        Dim sCampoDenArt As String
                                        If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                            If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
                                                sCampoCodArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID_CAMPO")
                                            End If
                                            If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
                                                sCampoDenArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID_CAMPO")
                                            End If
                                            If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
                                                sCampoCodArtAnt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID_CAMPO")
                                            End If
                                        Else
                                            If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
                                                sCampoCodArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID")
                                            End If
                                            If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
                                                sCampoDenArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID")
                                            End If
                                            If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
                                                sCampoCodArtAnt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID")
                                            End If
                                        End If

                                        mioFSDSEntry = Me.FindControl("fsentry" & sCampoCodArt)

                                        If Not mioFSDSEntry Is Nothing Then
                                            mioFSDSEntry.WindowTitle = Me.Page.Title
                                            mioFSDSEntry.idEntryPREC = oFSEntry.IdContenedor & "_" & oFSEntry.ClientID
                                        Else
                                            idEntryPREC = oFSEntry.ClientID
                                        End If
                                        mioFSDSEntry = Me.FindControl("fsentry" & sCampoDenArt)

                                        If Not mioFSDSEntry Is Nothing Then
                                            mioFSDSEntry.WindowTitle = Me.Page.Title
                                            mioFSDSEntry.idEntryPREC = oFSEntry.IdContenedor & "_" & oFSEntry.ClientID
                                        Else
                                            idEntryPREC = oFSEntry.ClientID
                                        End If
                                        mioFSDSEntry = Me.FindControl("fsentry" & sCampoCodArtAnt)

                                        If Not mioFSDSEntry Is Nothing Then
                                            mioFSDSEntry.WindowTitle = Me.Page.Title
                                            mioFSDSEntry.idEntryPREC = oFSEntry.IdContenedor & "_" & oFSEntry.ClientID
                                        Else
                                            idEntryPREC = oFSEntry.ClientID
                                        End If


                                    End If
                                Else
                                    oFSEntry.CargarUltADJ = False
                                End If

                            Case TiposDeDatos.TipoCampoGS.UnidadOrganizativa
                                sUnidadOrganizativa = ""
                                If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                    bDepartamentoRelacionado = True
                                    sUnidadOrganizativa = oFSEntry.Valor
                                    Dim arrUON() As String = oFSEntry.Valor.split("-")
                                    Dim oUON As String
                                    Dim oUONData As FSNServer.UnidadesOrg
                                    Dim ilong = arrUON.Length
                                    Dim icount = 0
                                    Dim sUon0 As String
                                    Dim sUon1 As String
                                    Dim sUon2 As String
                                    Dim sUon3 As String
                                    sUon0 = ""
                                    sUon1 = ""
                                    sUon2 = ""
                                    sUon3 = ""

                                    While icount < ilong - 1
                                        Select Case icount
                                            Case 0
                                                sUon1 = arrUON(0)
                                            Case 1
                                                sUon2 = arrUON(1)
                                            Case 2
                                                sUon3 = arrUON(2)
                                        End Select
                                        icount = icount + 1
                                    End While
                                    oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
                                    oFSEntry.Text = oDSRow.Item("VALOR_TEXT")
                                    oFSEntry.Denominacion = arrUON(ilong - 1)
                                    Dim iestado As Integer
                                    If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oDSRow.Item("VALOR_TEXT_DEN")) Then
                                        iestado = oDSRow.Item("VALOR_TEXT_COD")
                                        oFSEntry.Tipo = IIf(iestado = 1, TiposDeDatos.TipoGeneral.TipoPresBajaLog, TiposDeDatos.TipoGeneral.TipoTextoMedio)
                                    Else
                                        oUONData = FSNServer.Get_Object(GetType(FSNServer.UnidadesOrg))

                                        Select Case ilong
                                            Case 2
                                                oUONData.CargarEstado(LTrim(sUon1), , , )
                                            Case 3
                                                oUONData.CargarEstado(LTrim(sUon1), LTrim(sUon2), , )
                                            Case 4
                                                oUONData.CargarEstado(LTrim(sUon1), LTrim(sUon2), LTrim(sUon3), )
                                        End Select

                                        If oUONData.Data.Tables(0).Rows.Count > 0 Then
                                            iestado = oUONData.Data.Tables(0).Rows(0).Item("BAJALOG")
                                            If iestado = 1 Then 'Esta de baja
                                                oFSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoPresBajaLog
                                            Else
                                                oFSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoTextoMedio

                                            End If
                                        End If
                                    End If
                                End If
                                oFSEntry.Intro = 1
                                sKeyUnidadOrganizativa = oFSEntry.ClientID
                                If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Departamento & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                    Dim sKeyDepartamento As String = String.Empty
                                    If Not mdsCampos.Tables(0).Columns("ID_CAMPO") Is Nothing Then
                                        sKeyDepartamento = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Departamento & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                    Else
                                        sKeyDepartamento = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Departamento & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                    End If
                                    oFSEntry.DependentField = "fsentry" & sKeyDepartamento
                                End If
                                sIdUnidadOrganizativa = oFSEntry.Tag
                                sidDataEntryUnidadOrganizativa = oFSEntry.IdContenedor & "_" & "fsentry" & oFSEntry.Tag
                                Dim sCampoDepend As String
                                If Not mdsCampos.Tables(0).Columns("ID_CAMPO") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length = 0 Then
                                        If mdsCampos.Tables(0).Select("(TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS =" & TiposDeDatos.TipoCampoGS.DenArticulo & ") AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
                                            sCampoDepend = mdsCampos.Tables(0).Select("(TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS =" & TiposDeDatos.TipoCampoGS.DenArticulo & ") AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 ")(0).Item("ID_CAMPO")
                                        End If
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length = 0 Then
                                        If mdsCampos.Tables(0).Select("(TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS =" & TiposDeDatos.TipoCampoGS.DenArticulo & ") AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
                                            sCampoDepend = mdsCampos.Tables(0).Select("(TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS =" & TiposDeDatos.TipoCampoGS.DenArticulo & ") AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 ")(0).Item("ID")
                                        End If
                                    End If
                                End If
                                oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry" & sCampoDepend
                            Case TiposDeDatos.TipoCampoGS.ProveedorAdj
                                If oFSEntry.Valor <> Nothing Then
                                    If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                        sDenominacion = oDSRow.Item("VALOR_TEXT_DEN")
                                    Else
                                        oProve = FSNServer.Get_Object(GetType(FSNServer.Proveedor))
                                        oProve.Cod = oFSEntry.Valor
                                        oProve.Load(msIdi, True)
                                        sDenominacion = oProve.Den
                                    End If
                                    oFSEntry.Text = oFSEntry.Valor + " - " + sDenominacion.Replace("'", "")
                                    oFSEntry.Denominacion = sDenominacion
                                Else
                                    If Not oFSEntry.ReadOnly Then
                                        oFSEntry.Text = oTextos.Rows(2).Item(1)
                                    End If
                                End If
                            Case TiposDeDatos.TipoCampoGS.CentroCoste
                                If Not IsDBNull(oDSRow.Item("VER_UON")) Then
                                    oFSEntry.VerUON = oDSRow.Item("VER_UON")
                                Else
                                    oFSEntry.VerUON = False
                                End If
                                If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                    oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
                                    If InStrRev(oFSEntry.Valor, "#") >= 0 Then
                                        oFSEntry.Text = Right(oFSEntry.Valor, Len(oFSEntry.Valor) - InStrRev(oFSEntry.Valor, "#"))
                                    Else
                                        oFSEntry.Text = oFSEntry.Valor
                                    End If
                                    If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oDSRow.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
                                        sDenominacion = oFSEntry.Text & " - " & sDenominacion
                                    Else
                                        'ya nos devuelve el codigo - denominaci�n
                                        Dim oCentrosCoste As FSNServer.CentrosCoste
                                        oCentrosCoste = FSNServer.Get_Object(GetType(FSNServer.CentrosCoste))
                                        oCentrosCoste.BuscarCentrosCoste(oUser.Cod, msIdi, oFSEntry.Text, oFSEntry.VerUON)
                                        If oCentrosCoste.Data.Tables(0).Rows.Count > 0 Then
                                            sDenominacion = oCentrosCoste.Data.Tables(0).Rows(0).Item("DEN").ToString
                                        End If
                                        oCentrosCoste = Nothing
                                    End If

                                    oFSEntry.Text = sDenominacion
                                    oFSEntry.Denominacion = sDenominacion
                                Else
                                    'Miro si s�lo tiene un centro de coste al que imputar
                                    Dim oCentrosCoste As FSNServer.CentrosCoste
                                    Dim dCentros As DataSet
                                    oCentrosCoste = FSNServer.Get_Object(GetType(FSNServer.CentrosCoste))
                                    dCentros = oCentrosCoste.LoadData(oUser.Cod, msIdi, oFSEntry.VerUON)

                                    'Del stored viene indicado con valor centro_sm si el centro de coste est� a ese nivel 
                                    Dim iLevelCentro As Integer = 1
                                    For iLevel As Integer = 0 To dCentros.Tables.Count - 1
                                        If dCentros.Tables(iLevel).Rows.Count > 0 Then
                                            If Not IsDBNull(dCentros.Tables(iLevel).Rows(0).Item("CENTRO_SM")) Then
                                                iLevelCentro = iLevel
                                                Exit For
                                            End If
                                        End If
                                    Next
                                    If dCentros.Tables(iLevelCentro).Rows.Count = 1 Then
                                        sDenominacion = dCentros.Tables(iLevelCentro).Rows(0).Item("DEN").ToString
                                        oFSEntry.Text = sDenominacion
                                        oFSEntry.Denominacion = sDenominacion
                                        Dim sValorCentro As String = ""
                                        Select Case iLevelCentro
                                            Case 4
                                                sValorCentro = dCentros.Tables(iLevelCentro).Rows(0).Item("UON1") & "#" & dCentros.Tables(iLevelCentro).Rows(0).Item("UON2") & "#" & dCentros.Tables(iLevelCentro).Rows(0).Item("UON3") & "#" & dCentros.Tables(iLevelCentro).Rows(0).Item("COD")
                                            Case 3
                                                sValorCentro = dCentros.Tables(iLevelCentro).Rows(0).Item("UON1") & "#" & dCentros.Tables(iLevelCentro).Rows(0).Item("UON2") & "#" & dCentros.Tables(iLevelCentro).Rows(0).Item("COD")
                                            Case 2
                                                sValorCentro = dCentros.Tables(iLevelCentro).Rows(0).Item("UON1") & "#" & dCentros.Tables(iLevelCentro).Rows(0).Item("COD")
                                            Case 1
                                                sValorCentro = dCentros.Tables(iLevelCentro).Rows(0).Item("COD")
                                        End Select
                                        oFSEntry.Valor = sValorCentro
                                    End If
                                    oCentrosCoste = Nothing
                                End If
                                sIdDataEntryCentroCoste = oFSEntry.IdContenedor & "_" & "fsentry" & oFSEntry.Tag
                            If Not IsDBNull(oFSEntry.Valor) Then sCodCentroCoste = oFSEntry.Valor

                        Case TiposDeDatos.TipoCampoGS.Partida
                            oFSEntry.PRES5 = oDSRow.Item("PRES5")
                            If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
                                If InStrRev(oFSEntry.Valor, "#") > 0 Then
                                    If InStrRev(oFSEntry.Valor, "|") > 0 Then 'si no est� en el nivel 1, si no m�s abajo
                                        oFSEntry.Text = Right(oFSEntry.Valor, Len(oFSEntry.Valor) - InStrRev(oFSEntry.Valor, "|"))
                                    Else
                                        oFSEntry.Text = Right(oFSEntry.Valor, Len(oFSEntry.Valor) - InStrRev(oFSEntry.Valor, "#"))
                                    End If
                                Else
                                    oFSEntry.Text = oFSEntry.Valor
                                End If
                                If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oDSRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
                                    sDenominacion = oFSEntry.Text & " - " & sDenominacion
                                Else
                                    'ya nos devuelve el codigo - denominaci�n
                                    Dim oPartida As FSNServer.Partida
                                    oPartida = FSNServer.Get_Object(GetType(FSNServer.Partida))
                                    oPartida.BuscarPartida(oUser.Cod, msIdi, oFSEntry.PRES5, oFSEntry.Text)
                                    If oPartida.Data.Tables(0).Rows.Count > 0 Then
                                        sDenominacion = oPartida.Data.Tables(0).Rows(0).Item("DEN").ToString
                                    End If
                                    oPartida = Nothing
                                End If

                                oFSEntry.Text = sDenominacion
                                oFSEntry.Denominacion = sDenominacion
                            Else
                                'Miro si s�lo tiene una partida a la que imputar
                                Dim oPartida As FSNServer.Partida
                                Dim dPartidas As DataSet
                                oPartida = FSNServer.Get_Object(GetType(FSNServer.Partida))

                                Dim cPartidas As Fullstep.FSNServer.PRES5
                                cPartidas = Session("FSN_Server").Get_Object(GetType(FSNServer.PRES5))

                                Dim bPlurianual As Boolean = False
                                If Not Session("FSSMPargen") Is Nothing Then
                                    Dim configuracion As FSNServer.PargenSM
                                    configuracion = Session("FSSMPargen").Item(oFSEntry.PRES5)
                                    If Not configuracion Is Nothing Then
                                        bPlurianual = configuracion.Plurianual
                                    End If
                                End If

                                If sCodCentroCoste <> "" Then
                                    dPartidas = cPartidas.Partidas_LoadData(oFSEntry.PRES5, oUser.Cod, msIdi, bPlurianual, sCodCentroCoste)
                                Else
                                    dPartidas = cPartidas.Partidas_LoadData(oFSEntry.PRES5, oUser.Cod, msIdi, bPlurianual)
                                End If
                                'Del stored viene indicado como IMPUTABLE=1 la partidas a nivel minimo seleccionable. Este nivel es un valor entre 1 y 4. Del stored las partidas llegan a partir de la tabla 5.
                                Dim iNivelMinimo As Integer = 5
                                For iLevel As Integer = 5 To 8
                                    If dPartidas.Tables(iLevel).Rows.Count > 0 Then
                                        If dPartidas.Tables(iLevel).Rows(0).Item("IMPUTABLE") = 1 Then
                                            iNivelMinimo = iLevel
                                            Exit For
                                        End If
                                    End If
                                Next
                                If dPartidas.Tables(iNivelMinimo).Rows.Count = 1 Then
                                    If Not sCodCentroCoste.Substring(sCodCentroCoste.Length - 1).Equals("#") Then sCodCentroCoste &= "#"
                                    oFSEntry.Valor = sCodCentroCoste & dPartidas.Tables(iNivelMinimo).Rows(0).Item("COD")
                                    sDenominacion = dPartidas.Tables(iNivelMinimo).Rows(0).Item("DEN")
                                    oFSEntry.Text = sDenominacion
                                    oFSEntry.Denominacion = sDenominacion
                                End If
                                oPartida = Nothing
                            End If
                            If Not IsDBNull(oDSRow.Item("CENTRO_COSTE")) Then
                                Dim sIdCampo As String
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND CAMPO_ORIGEN = " & oDSRow.Item("CENTRO_COSTE")).Length > 0 Then
                                        sIdCampo = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND CAMPO_ORIGEN = " & oDSRow.Item("CENTRO_COSTE"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    sIdCampo = oDSRow.Item("CENTRO_COSTE")
                                End If
                                oFSEntry.idEntryCC = oFSEntry.IdContenedor & "_fsentry" & sIdCampo
                            End If

                            sIDCampoPartida = oFSEntry.Tag

                        Case TiposDeDatos.TipoCampoGS.AnyoPartida
                            oFSEntry.Intro = 1

                            Dim sIdCampo As String = ""
                            If Not IsDBNull(oDSRow.Item("PARTIDAPRES")) Then
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Partida & " AND CAMPO_ORIGEN = " & oDSRow.Item("PARTIDAPRES")).Length > 0 Then
                                        sIdCampo = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Partida & " AND CAMPO_ORIGEN = " & oDSRow.Item("PARTIDAPRES"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    sIdCampo = oDSRow.Item("PARTIDAPRES")
                                End If
                                oFSEntry.idEntryPartidaPlurianual = oFSEntry.IdContenedor & "_fsentry" & sIdCampo
                                oFSEntry.EntryPartidaPlurianualCampo = 1
                            End If

                            If Not IsDBNull(oDSRow.Item("VALOR_NUM")) Then
                                oFSEntry.Valor = oDSRow.Item("VALOR_NUM")
                                oFSEntry.Text = oFSEntry.Valor
                            ElseIf sIdCampo <> "" Then
                                Dim mioFSDSEntry As DataEntry.GeneralEntry
                                mioFSDSEntry = Me.FindControl("fsentry" & sIdCampo)
                                If Not mioFSDSEntry Is Nothing Then
                                    Dim oAnyoPartida As FSNServer.PartidaPRES5
                                    oAnyoPartida = FSNServer.Get_Object(GetType(FSNServer.PartidaPRES5))
                                    oAnyoPartida.LoadDataAnyoPartida(DBNullToStr(mioFSDSEntry.PRES5), DBNullToStr(NothingToDBNull(mioFSDSEntry.Valor)))
                                    If oAnyoPartida.DataAnyoPartida.Tables(0).Rows.Count = 1 Then
                                        oFSEntry.Valor = oAnyoPartida.DataAnyoPartida.Tables(0).Rows(0)("ANYO")
                                        oFSEntry.Text = oFSEntry.Valor
                                    End If
                                End If
                            End If

                        Case TiposDeDatos.TipoCampoGS.Activo
                            If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                    oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
                                    If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                        sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
                                    End If

                                    If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oDSRow.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
                                        sDenominacion = oFSEntry.Valor & " - " & sDenominacion
                                    Else
                                        Dim SMServer As FSNServer.Root = Session("FSN_Server")
                                        If Not SMServer Is Nothing Then
                                            Dim oActivo As FSNServer.Activo
                                            oActivo = FSNServer.Get_Object(GetType(FSNServer.Activo))
                                            oActivo.BuscarActivo(msIdi, oUser.Cod, oFSEntry.Valor)
                                            If oActivo.Data.Tables(0).Rows.Count > 0 Then
                                                sDenominacion = oActivo.Data.Tables(0).Rows(0).Item("DEN").ToString
                                                sDenominacion = oFSEntry.Valor & " - " & sDenominacion
                                            End If
                                        End If
                                    End If

                                    oFSEntry.Text = sDenominacion
                                    oFSEntry.Denominacion = sDenominacion
                                End If

                                If Not IsDBNull(oDSRow.Item("CENTRO_COSTE")) Then
                                    Dim sIdCampo As String
                                    If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                        If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND CAMPO_ORIGEN = " & oDSRow.Item("CENTRO_COSTE")).Length > 0 Then
                                            sIdCampo = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND CAMPO_ORIGEN = " & oDSRow.Item("CENTRO_COSTE"))(0).Item("ID_CAMPO")
                                        End If
                                    Else
                                        sIdCampo = oDSRow.Item("CENTRO_COSTE")
                                    End If
                                    oFSEntry.idEntryCC = oFSEntry.IdContenedor & "_fsentry" & sIdCampo
                                End If
                            Case TiposDeDatos.TipoCampoGS.TipoPedido
                                Dim oTiposPedido As FSNServer.TiposPedido
                                Dim dsTiposPedidos As DataSet
                                oTiposPedido = FSNServer.Get_Object(GetType(FSNServer.TiposPedido))
                                dsTiposPedidos = oTiposPedido.LoadData(msIdi)
                                'oFSEntry.Width = Unit.Pixel(300)
                                oFSEntry.Width = Unit.Percentage(100)
                                oFSEntry.ListaTiposPedido = dsTiposPedidos
                                oFSEntry.Intro = 1
                                If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                    oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
                                    If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                        sDenominacion = oFSEntry.Valor & " - " & Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
                                    Else
                                        If dsTiposPedidos.Tables(0).Rows.Count > 0 Then
                                            If dsTiposPedidos.Tables(0).Select("COD = '" & oDSRow.Item("VALOR_TEXT") & "'").Length > 0 Then
                                                sDenominacion = dsTiposPedidos.Tables(0).Select("COD = '" & oDSRow.Item("VALOR_TEXT") & "'")(0).Item("COD") & " - " & dsTiposPedidos.Tables(0).Select("COD = '" & oDSRow.Item("VALOR_TEXT") & "'")(0).Item("DEN").ToString
                                            End If
                                        Else
                                            sDenominacion = ""
                                        End If
                                    End If
                                    oFSEntry.Text = sDenominacion
                                    oFSEntry.Denominacion = sDenominacion

                                    If dsTiposPedidos.Tables(0).Rows.Count > 0 AndAlso dsTiposPedidos.Tables(0).Select("COD = '" & oDSRow.Item("VALOR_TEXT") & "'").Length > 0 Then
                                        oFSEntry.Concepto = dsTiposPedidos.Tables(0).Select("COD = '" & oDSRow.Item("VALOR_TEXT") & "'")(0).Item("CONCEPTO")
                                        oFSEntry.Almacenamiento = dsTiposPedidos.Tables(0).Select("COD = '" & oDSRow.Item("VALOR_TEXT") & "'")(0).Item("ALMACENAR")
                                        oFSEntry.Recepcion = dsTiposPedidos.Tables(0).Select("COD = '" & oDSRow.Item("VALOR_TEXT") & "'")(0).Item("RECEPCIONAR")
                                    End If
                                End If
                                oTiposPedido = Nothing

                                Dim miPage As FSNPage
                                Dim moduloIdiomaAnterior As FSNLibrary.TiposDeDatos.ModulosIdiomas
                                Dim arrConcepto As String()
                                Dim arrAlmacenamiento As String()
                                Dim arrRecepcion As String()

                                miPage = CType(Page, FSNPage)
                                moduloIdiomaAnterior = miPage.ModuloIdioma
                                miPage.ModuloIdioma = TiposDeDatos.ModulosIdiomas.BusquedaArticulos
                                dsTiposPedidos.Tables(0).Columns(1).Caption = miPage.Textos(2)
                                dsTiposPedidos.Tables(0).Columns(2).Caption = miPage.Textos(3)
                                dsTiposPedidos.Tables(0).Columns(7).Caption = miPage.Textos(29)
                                dsTiposPedidos.Tables(0).Columns(8).Caption = miPage.Textos(30)
                                dsTiposPedidos.Tables(0).Columns(9).Caption = miPage.Textos(31)

                                ReDim arrConcepto(2)
                                arrConcepto(0) = miPage.Textos(32)
                                arrConcepto(1) = miPage.Textos(33)
                                arrConcepto(2) = miPage.Textos(20)

                                ReDim arrAlmacenamiento(2)
                                arrAlmacenamiento(0) = miPage.Textos(34)
                                arrAlmacenamiento(1) = miPage.Textos(35)
                                arrAlmacenamiento(2) = miPage.Textos(36)

                                ReDim arrRecepcion(2)
                                arrRecepcion(0) = miPage.Textos(37)
                                arrRecepcion(1) = miPage.Textos(35)
                                arrRecepcion(2) = miPage.Textos(36)

                                For Each oDSRowTipoPedido As DataRow In dsTiposPedidos.Tables(0).Rows
                                    oDSRowTipoPedido.Item(7) = arrConcepto(oDSRowTipoPedido.Item(3))
                                    oDSRowTipoPedido.Item(8) = arrAlmacenamiento(oDSRowTipoPedido.Item(4))
                                    oDSRowTipoPedido.Item(9) = arrRecepcion(oDSRowTipoPedido.Item(5))
                                Next

                                miPage.ModuloIdioma = moduloIdiomaAnterior
                            Case TiposDeDatos.TipoCampoGS.Factura
                                If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                                    Dim oFactura As FSNServer.Factura
                                    oFactura = FSNServer.Get_Object(GetType(FSNServer.Factura))
                                    oFSEntry.DependentValue = oFactura.BuscarFactura(oDSRow.Item("VALOR_TEXT"), Nothing)
                                End If

                                Dim sql As String
                                Dim sCampoDependent As String
                                sql = "(TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & ") AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 "

                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select(sql).Length > 0 Then
                                        sCampoDependent = mdsCampos.Tables(0).Select(sql)(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select(sql).Length > 0 Then
                                        sCampoDependent = mdsCampos.Tables(0).Select(sql)(0).Item("ID")
                                    End If
                                End If
                                If sCampoDependent <> "" Then ' Relacionar el proveedor con la factura
                                    oFSEntry.DependentField = "fsentry" & sCampoDependent
                                End If
                        End Select

                        If oDSRow.Item("TIPO") = 6 Then
                            Dim bSinKeyArticulo As Boolean = True
                            oFSEntry.Tabla_Externa = oDSRow.Item("TABLA_EXTERNA")
                            If oFSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoFecha AndAlso oFSEntry.Valor = New Date Then oFSEntry.Valor = Nothing
                            If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                If Not oFSEntry.Valor Is Nothing AndAlso Not IsDBNull(oDSRow.Item("VALOR_TEXT_DEN")) Then
                                    Dim sTxt As String = oDSRow.Item("VALOR_TEXT_DEN").ToString()
                                    Dim sTxt2 As String
                                    If sTxt.IndexOf("]#[") > 0 Then
                                        sTxt2 = sTxt.Substring(2, sTxt.IndexOf("]#[") - 2)
                                    Else
                                        sTxt2 = sTxt.Substring(2, sTxt.Length - 3)
                                    End If
                                    Select Case sTxt.Substring(0, 1)
                                        Case "s"
                                            oFSEntry.Text = sTxt2
                                        Case "n"
                                            oFSEntry.Text = Fullstep.FSNLibrary.FormatNumber(Numero(sTxt2, "."), oUser.NumberFormat)
                                        Case "d"
                                            oFSEntry.Text = CType(sTxt2, Date).ToString("d", oUser.DateFormat)
                                    End Select
                                    If sTxt.IndexOf("]#[") > 0 Then
                                        oFSEntry.Text = oFSEntry.Text & " "
                                        sTxt2 = sTxt.Substring(sTxt.IndexOf("]#[") + 3, sTxt.Length - sTxt.IndexOf("]#[") - 5)
                                        Select Case sTxt.Substring(sTxt.Length - 1, 1)
                                            Case "s"
                                                oFSEntry.Text = oFSEntry.Text & sTxt2
                                            Case "n"
                                                oFSEntry.Text = oFSEntry.Text & Fullstep.FSNLibrary.FormatNumber(Numero(sTxt2, "."), oUser.NumberFormat)
                                            Case "d"
                                                oFSEntry.Text = oFSEntry.Text & CType(sTxt2, Date).ToString("d", oUser.DateFormat)
                                        End Select
                                    End If
                                End If
                                If Not IsDBNull(oDSRow.Item("VALOR_TEXT_COD")) Then
                                    oFSEntry.codigoArticulo = oDSRow.Item("VALOR_TEXT_COD")
                                End If
                                Dim oTablaExterna As FSNServer.TablaExterna = FSNServer.Get_Object(GetType(FSNServer.TablaExterna))
                                oTablaExterna.LoadDefTabla(oFSEntry.Tabla_Externa, False)
                                bSinKeyArticulo = oTablaExterna.ArtKey Is Nothing
                            Else
                                Dim oTablaExterna As FSNServer.TablaExterna = FSNServer.Get_Object(GetType(FSNServer.TablaExterna))
                                oTablaExterna.LoadDefTabla(oFSEntry.Tabla_Externa, False)
                            If Not oFSEntry.Valor Is Nothing Then
                                Dim oRow As DataRow = oTablaExterna.BuscarRegistro(oFSEntry.Valor.ToString(), , True)
                                oFSEntry.Text = oTablaExterna.DescripcionReg(oRow, msIdi, oUser.DateFormat, oUser.NumberFormat)
                                oFSEntry.codigoArticulo = oTablaExterna.CodigoArticuloReg(oRow)
                            End If
                            bSinKeyArticulo = oTablaExterna.ArtKey Is Nothing
                            End If
                            oFSEntry.Denominacion = oFSEntry.Text
                            oFSEntry.Width = Unit.Percentage(100)
                        If sKeyArticulo Is Nothing Then
                            For Each oRow As DataRow In mdsCampos.Tables(0).Rows
                                If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
                                    If Not mdsCampos.Tables(0).Columns("ID_CAMPO") Is Nothing Then
                                        sKeyArticulo = "fsentry" + oRow.Item("ID_CAMPO").ToString
                                    Else
                                        sKeyArticulo = "fsentry" + oRow.Item("ID").ToString
                                    End If
                                    Exit For
                                End If
                            Next

                        End If
                        If Not (sKeyArticulo Is Nothing Or bSinKeyArticulo) Then _
                             oFSEntry.DependentField = sKeyArticulo
                    ElseIf oDSRow.Item("TIPO") = 7 Then
                        oFSEntry.Servicio = oDSRow.Item("SERVICIO")
                    End If

                    Select Case oFSEntry.IdAtrib
                            Case TiposDeDatos.TipoAtributo.CuentaContable
                                Dim mioFSDSEntry As DataEntry.GeneralEntry
                                Dim sCampoCodArt, sCampoCodArtAnt As String
                                Dim sCampoDenArt As String

                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
                                        sCampoCodArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID_CAMPO")
                                    End If
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
                                        sCampoDenArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID_CAMPO")
                                    End If
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
                                        sCampoCodArtAnt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
                                        sCampoCodArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID")
                                    End If
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
                                        sCampoDenArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID")
                                    End If
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
                                        sCampoCodArtAnt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID")
                                    End If
                                End If

                                mioFSDSEntry = Me.FindControl("fsentry" & sCampoCodArt)

                                If Not mioFSDSEntry Is Nothing Then
                                    mioFSDSEntry.WindowTitle = Me.Page.Title
                                    mioFSDSEntry.idEntryCC = oFSEntry.IdContenedor & "_" & oFSEntry.ClientID
                                Else
                                    idEntryCC = oFSEntry.ClientID
                                End If
                                mioFSDSEntry = Me.FindControl("fsentry" & sCampoDenArt)
                                If Not mioFSDSEntry Is Nothing Then
                                    mioFSDSEntry.idEntryCC = oFSEntry.IdContenedor & "_" & oFSEntry.ClientID
                                Else
                                    idEntryCC = oFSEntry.ClientID
                                End If
                                mioFSDSEntry = Me.FindControl("fsentry" & sCampoCodArtAnt)
                                If Not mioFSDSEntry Is Nothing Then
                                    mioFSDSEntry.idEntryCC = oFSEntry.IdContenedor & "_" & oFSEntry.ClientID
                                Else
                                    idEntryCC = oFSEntry.ClientID
                                End If
                        End Select

                        If oFSEntry.Intro = 0 Then
                            Select Case oFSEntry.Tipo
                                Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoMedio
                                    oFSEntry.InputStyle.CssClass = "TipoTextoMedio"

                                    If oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.DenArticulo Then
                                        oFSEntry.MaxLength = FSNServer.LongitudesDeCodigos.giLongCodDENART
                                    Else
                                        If oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.CodArticulo Or oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
                                            oFSEntry.MaxLength = FSNServer.LongitudesDeCodigos.giLongCodART
                                        Else

                                            If TiposDeDatos.TipoGeneral.TipoTextoMedio = TiposDeDatos.TipoGeneral.TipoTextoMedio And Not IsDBNull(oDSRow.Item("MAX_LENGTH")) Then
                                                oFSEntry.MaxLength = oDSRow.Item("MAX_LENGTH")
                                            Else
                                                oFSEntry.MaxLength = 800
                                            End If
                                        End If
                                    End If
                                    If oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.Departamento Then
                                        If bDepBaja Then
                                            oFSEntry.InputStyle.CssClass = "StringTachado"
                                        End If
                                    End If
                                    oFSEntry.ErrMsg = oTextos.Rows(12).Item(1)

                                Case TiposDeDatos.TipoGeneral.TipoDepBajaLog
                                    oFSEntry.InputStyle.CssClass = "StringTachado"
                                    oFSEntry.MaxLength = 800

                                Case TiposDeDatos.TipoGeneral.TipoNumerico
                                    oFSEntry.InputStyle.CssClass = "TipoNumero"
                                Case TiposDeDatos.TipoGeneral.TipoFecha
                                    oFSEntry.InputStyle.CssClass = "TipoFecha"
                                Case TiposDeDatos.TipoGeneral.TipoTextoCorto
                                    If Not IsDBNull(oDSRow.Item("MAX_LENGTH")) Then
                                        oFSEntry.MaxLength = oDSRow.Item("MAX_LENGTH")
                                    Else
                                        oFSEntry.MaxLength = 100
                                    End If
                                    oFSEntry.InputStyle.CssClass = "TipoTextoCorto"
                                Case TiposDeDatos.TipoGeneral.TipoTextoLargo
                                    oFSEntry.PopUpStyle = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & theme & ".css"
                                    oFSEntry.PUCaptionBoton = oTextos.Rows(4).Item(1)
                                    oFSEntry.InputStyle.CssClass = "TipoTextoLargo"
                                    If Not IsDBNull(oDSRow.Item("MAX_LENGTH")) Then
                                        oFSEntry.MaxLength = oDSRow.Item("MAX_LENGTH")
                                    End If

                                    oFSEntry.Width = Unit.Percentage(100)
                                    oFSEntry.ErrMsg = oTextos.Rows(12).Item(1)
                                Case TiposDeDatos.TipoGeneral.TipoArchivo
                                    oFSEntry.InputStyle.CssClass = "TipoArchivo"
                                    oFSEntry.ErrMsg = oTextos.Rows(12).Item(1)
                                Case TiposDeDatos.TipoGeneral.TipoBoolean
                                    oFSEntry.Lista = FSNWeb.CommonAlta.CrearDataset(msIdi, oTextos.Rows(15).Item(1), oTextos.Rows(16).Item(1))
                                    oFSEntry.Intro = 1
                                Case TiposDeDatos.TipoGeneral.TipoPresBajaLog
                                    oFSEntry.InputStyle.CssClass = "StringTachado"
                                    oFSEntry.MaxLength = 800

                            End Select
                        ElseIf oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.SinTipo And oFSEntry.Intro = 2 Then
                            'Atributos de lista externa

                            'Primero la org. compras, si no hay Centro SM y si no hay la UON
                            If sidDataEntryOrgCompras <> "" Then ' Tiene un campo Organizacion de compras antes que el desglose!!!
                                oFSEntry.idDataEntryDependent = sidDataEntryOrgCompras
                            Else
                                Dim sIDOrgCompras As String = String.Empty

                                'Org compras
                                If CType(Me.Page, FSNPage).Acceso.gbUsar_OrgCompras Then
                                    If Not mdsCampos.Tables(0).Columns("ID_CAMPO") Is Nothing Then
                                        If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                            If Not mdsCampos.Tables(0).Columns("ID_CAMPO") Is Nothing Then
                                                sIDOrgCompras = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                            Else
                                                sIDOrgCompras = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                            End If
                                        End If
                                    End If
                                End If

                                If sIDOrgCompras <> "" Then
                                    oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry" & sIDOrgCompras
                                Else
                                    'Centro SM                                
                                    Dim sIdCentroCoste As String = ""
                                    If CType(Me.Page, FSNPage).Acceso.gbAccesoFSSM Then
                                        If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                            If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                                If Not mdsCampos.Tables(0).Columns("ID_CAMPO") Is Nothing Then
                                                    sIdCentroCoste = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                                Else
                                                    sIdCentroCoste = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CentroCoste & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                                End If
                                            End If
                                        End If
                                        If sIdCentroCoste <> "" Then oFSEntry.idDataEntryDependent2 = oFSEntry.IdContenedor & "_fsentry" & sIdCentroCoste
                                    End If

                                    'UON   
                                    If sIdCentroCoste = "" Then
                                        If sidDataEntryUnidadOrganizativa <> "" Then
                                            oFSEntry.idDataEntryDependent3 = sidDataEntryUnidadOrganizativa
                                        Else
                                            Dim sIdUON As String = String.Empty
                                            If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                                If Not mdsCampos.Tables(0).Columns("ID_CAMPO") Is Nothing Then
                                                    sIdUON = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                                Else
                                                    sIdUON = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                                                End If
                                            End If
                                            If sIdUON <> "" Then oFSEntry.idDataEntryDependent3 = oFSEntry.IdContenedor & "_" & "fsentry" & sIdUON
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        If oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.Almacen Then
                            oFSEntry.Width = Unit.Percentage(100)
                            oFSEntry.MaxLength = 800
                        End If

                        If oFSEntry.ReadOnly Then
                            oFSEntry.LabelStyle.CssClass = "CampoSoloLectura"
                            oFSEntry.InputStyle.CssClass = "trasparent"

                            Select Case oFSEntry.Tipo
                                Case TiposDeDatos.TipoGeneral.TipoPresBajaLog
                                    oFSEntry.InputStyle.CssClass = "StringTachadotrasparent"
                            End Select
                        End If
                        If Page.IsPostBack Then
                            If (Request.Form.AllKeys.Contains(oFSEntry.SubmitKey)) Then
                                oFSEntry.Valor = Request(oFSEntry.SubmitKey)
                            End If
                        End If
                        otblCell.Controls.Add(oFSEntry)

                        otblCell.CssClass = sClase
                        otblRow.Cells.Add(otblCell)
                    End If

                    otblRow.Height = Unit.Pixel(20)

                    Me.tblDesglose.Rows.Add(otblRow)
                    If sClase = "ugfilatabla" Then
                        sClase = "ugfilaalttabla"
                    Else
                        sClase = "ugfilatabla"
                    End If
                    'SRA: Tarea 978
                    If (oDSRow.Item("ESCRITURA") = 1 AndAlso Not mbSoloLectura) OrElse (oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoDesglose And oDSRow.Item("POPUP") = 1) Then
                        Select Case oFSEntry.TipoGS
                            Case TiposDeDatos.TipoCampoGS.UnidadOrganizativa
                                bDepartamentoRelacionado = True
                                sUnidadOrganizativa = oFSEntry.Valor
                                sIdUnidadOrganizativa = oFSEntry.Tag
                            Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
                                bCentroRelacionado = True
                                sCodOrgCompras = oFSEntry.Valor
                                sIDCampoOrgCompras = oFSEntry.Tag
                            Case TiposDeDatos.TipoCampoGS.Centro
                                bAlmacenRelacionado = True
                                sCodCentro = oFSEntry.Valor
                                sIdCampoCentro = oFSEntry.Tag
                                'Por si la organizacion de Compras est� oculto actualizar si est� relacionado o no con el centro
                                If lOrdenOrgCompras = lOrdenCentro - 1 Then
                                    bCentroRelacionado = True
                                End If
                            Case Else
                                bDepartamentoRelacionado = False
                                bCentroRelacionado = False
                                bAlmacenRelacionado = False
                        End Select
                    End If
                Else
                    'Para Cuando el CodArticulo No este visible
                    Select Case DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS"))
                    Case TiposDeDatos.TipoCampoGS.NuevoCodArticulo
                        Dim idCampoMat As Long
                        Dim sValorMat As String

                        idCampoMat = 0

                        'OBTENEMOS EL VALOR DEL MAT OCULTO
                        If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                            For Each i In arrCamposMaterial
                                If i < oDSRow.Item("id_campo") Then
                                    If idCampoMat < i Then
                                        idCampoMat = i
                                    End If
                                End If
                            Next
                            If idCampoMat > 0 Then
                                Try
                                    sValorMat = mdsCampos.Tables(0).Select("ID_CAMPO = " & idCampoMat)(0).Item("VALOR_TEXT")

                                Catch ex As Exception

                                End Try
                            End If
                        Else
                            For Each i In arrCamposMaterial
                                If i < oDSRow.Item("ID") Then
                                    If idCampoMat < i Then
                                        idCampoMat = i
                                    End If
                                End If
                            Next
                            If idCampoMat > 0 Then
                                Try
                                    sValorMat = mdsCampos.Tables(0).Select("ID = " & idCampoMat)(0).Item("VALOR_TEXT")
                                Catch ex As Exception

                                End Try
                            End If
                        End If

                        Dim arrMat(4) As String
                        For i = 1 To 4
                            arrMat(i) = ""
                        Next i

                        Dim lLongCod As Long
                        i = 1
                        While Trim(sValorMat) <> ""
                            Select Case i
                                Case 1
                                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                                Case 2
                                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                                Case 3
                                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                                Case 4
                                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
                            End Select
                            arrMat(i) = Trim(Mid(sValorMat, 1, lLongCod))
                            sValorMat = Mid(sValorMat, lLongCod + 1)
                            i = i + 1
                        End While

                        If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
                            sKeyCodArt = oDSRow.Item("VALOR_TEXT")

                            oArts = FSNServer.Get_Object(GetType(FSNServer.Articulos))

                            oArts.GMN1 = arrMat(1)
                            oArts.GMN2 = arrMat(2)
                            oArts.GMN3 = arrMat(3)
                            oArts.GMN4 = arrMat(4)
                            oArts.LoadData(oUser.Cod, oUser.PMRestriccionUONSolicitudAUONUsuario, oUser.PMRestriccionUONSolicitudAUONPerfil,
                                           oDSRow.Item("VALOR_TEXT"), , True, AccesoFSEP:=FSNServer.TipoAcceso.gbAccesoFSEP)
                            If oArts.Data.Tables(0).Rows.Count > 0 Then
                                bArticuloGenerico = oArts.Data.Tables(0).Rows(0).Item("GENERICO")
                            End If
                            Dim bAnyadir_art As Boolean
                            bAnyadir_art = oDSRow.Item("ANYADIR_ART")
                            Dim idDataEntryMat As String

                            If Not Me.ClientID Is Nothing Then
                                idDataEntryMat = Me.ClientID & "_"
                            End If

                            If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                idDataEntryMat = idDataEntryMat & "fsentry" & (oDSRow.Item("ID_CAMPO") - 1)
                            Else
                                idDataEntryMat = idDataEntryMat & "fsentry" & (oDSRow.Item("ID") - 1)
                            End If
                        End If
                End Select
            End If

            If oDSRow.Item("VISIBLE") = 1 And oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoDesglose And DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.DesgloseActividad And oDSRow.Item("POPUP") = 0 Then
                ''''Desglose no popup Obligatorio. Sin dataentry no hay registro en arrObligatorio, sin este registro jsAlta no funciona.

                If (DBNullToSomething(oDSRow.Item("OBLIGATORIO")) = 1) Then
                    oFSEntry = New DataEntry.GeneralEntry
                    oFSEntry.Title = Me.Page.Title
                    oFSEntry.Width = Unit.Percentage(100)
                    If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                        oFSEntry.Tag = oDSRow.Item("ID_CAMPO").ToString
                        oFSEntry.ID = "OBLIG_" + Me.ID + "_" + oDSRow.Item("ID_CAMPO").ToString

                        oFSEntry.CampoDef_CampoODesgHijo = 0 'Es un campo desglose, es decir, el padre
                        oFSEntry.CampoDef_DesgPadre = oDSRow.Item("ID")
                    Else
                        oFSEntry.Tag = oDSRow.Item("ID").ToString
                        oFSEntry.ID = "OBLIG_" + Me.ID + "_" + oDSRow.Item("ID").ToString
                    End If
                    oFSEntry.IdContenedor = Me.ClientID
                    'Cambio de las doble comillas para que en JavaScript no de error
                    oDSRow.Item("DEN_" & msIdi) = oDSRow.Item("DEN_" & msIdi).ToString.Replace("'", "\'")
                    oDSRow.Item("DEN_" & msIdi) = oDSRow.Item("DEN_" & msIdi).ToString.Replace("""", "\""")
                    oFSEntry.Title = oDSRow.Item("DEN_" & msIdi)
                    oFSEntry.WindowTitle = Me.Page.Title
                    oFSEntry.Intro = oDSRow.Item("INTRO")
                    oFSEntry.Idi = msIdi
                    oFSEntry.TabContainer = msTabContainer
                    oFSEntry.TipoGS = DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS"))
                    oFSEntry.ReadOnly = (oDSRow.Item("ESCRITURA") = 0) Or mbSoloLectura
                    oFSEntry.Obligatorio = True
                    oFSEntry.Orden = oDSRow.Item("ORDEN")
                    oFSEntry.Grupo = oDSRow.Item("GRUPO")
                    oFSEntry.Tipo = TiposDeDatos.TipoGeneral.TipoDesgloseOblig
                    oFSEntry.BaseDesglose = False
                    oFSEntry.ActivoSM = CType(Me.Page, FSNPage).Acceso.gbAccesoFSSM

                    otblRow = New System.Web.UI.WebControls.TableRow
                    otblCell = New System.Web.UI.WebControls.TableCell
                    otblCell.Controls.Add(oFSEntry)
                    otblRow.Cells.Add(otblCell)
                    Me.tblDesglose.Rows.Add(otblRow)
                End If
                ''''
                '''Es un desglose sin popup
                nColumna = nColumna + 1
                Dim oucDesglose As desgloseControl = LoadControl("desglose.ascx")

                otblRow = New System.Web.UI.WebControls.TableRow
                otblCell = New System.Web.UI.WebControls.TableCell
                otblCell.Width = Unit.Percentage(100)
                otblCell.ColumnSpan = 4

                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                    oucDesglose.ID = Me.ID + "_" + oDSRow.Item("ID_CAMPO").ToString
                    oucDesglose.Campo = oDSRow.Item("ID_CAMPO")
                    If bExisteCampoOrigen Then
                        oucDesglose.CampoOrigen = DBNullToDbl(oDSRow.Item("CAMPO_ORIGEN"))
                    End If
                    oucDesglose.TieneIdCampo = True
                Else
                    oucDesglose.ID = Me.ID + "_" + oDSRow.Item("ID").ToString
                    oucDesglose.Campo = oDSRow.Item("ID")
                    oucDesglose.TieneIdCampo = False
                End If
                oucDesglose.TabContainer = Me.ClientID
                oucDesglose.Instancia = mlInstancia
                oucDesglose.Solicitud = mlSolicitud
                oucDesglose.Version = mlVersion
                oucDesglose.SoloLectura = mbSoloLectura Or (oDSRow.Item("ESCRITURA") = 0)
                oucDesglose.FuerzaLectura = mbFuerzaDesgloseLectura
                oucDesglose.Acciones = moAcciones
                oucDesglose.Ayuda = DBNullToSomething(oDSRow.Item("AYUDA_" & msIdi))
                oucDesglose.DSEstados = moDSEstados
                oucDesglose.Titulo = DBNullToSomething(oDSRow.Item("DEN_" & msIdi))
                oucDesglose.CentroRelacionado = bCentroRelacionado
                oucDesglose.CodOrgCompras = sCodOrgCompras
                oucDesglose.CodCentro = sCodCentro
                oucDesglose.idCampoOrgCompras = sIDCampoOrgCompras
                oucDesglose.TipoSolicitud = mTipoSolicitud
                oucDesglose.EnCursoDeAprobacion = mEnCursoDeAprobacion
                oucDesglose.idCampoPartida = sIDCampoPartida
                If sIDCampoOrgCompras <> "" Then
                    oucDesglose.idDataEntryFORMOrgCompras = sidDataEntryOrgCompras
                End If
                oucDesglose.UnidadOrganizativa = sUnidadOrganizativa
                If Not sIdUnidadOrganizativa = "" Then oucDesglose.idDataEntryFORMUnidadOrganizativa = sidDataEntryUnidadOrganizativa
                oucDesglose.CentroCoste = sCodCentroCoste
                If Not sCodCentroCoste = String.Empty Then oucDesglose.idDataEntryFORMCentroCoste = sIdDataEntryCentroCoste

                If bCentroRelacionado Then
                    oucDesglose.idDataEntryFORMCentro = sIdDataEntryCentro
                    oucDesglose.idCampoCentro = sIdCampoCentro
                End If
                oucDesglose.PM = Me.PM
                If Not Me.PM Then oucDesglose.Proveedor = Proveedor
                oucDesglose.Observador = mbObservador

                otblCell.Controls.Add(oucDesglose)
                otblRow.Cells.Add(otblCell)
                otblCell.CssClass = sClase

                Me.tblDesglose.Rows.Add(otblRow)

                If sIdDataentryMaterial <> "" Then
                    oucDesglose.idDataEntryFORMMaterial = sIdDataentryMaterial
                End If
            ElseIf oDSRow.Item("VISIBLE") = 1 And oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoDesglose And oDSRow.Item("POPUP") = 1 And DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.DesgloseActividad Then
                Dim sScript As String
                If sidDataEntryOrgCompras <> "" Then
                    sScript = "var sDataEntryOrgComprasFORM='" & sidDataEntryOrgCompras & "'"
                    sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                    If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "VariablesORGANIZACIONCOMPRASEntry" & oFSEntry.Tag) Then _
                     Page.ClientScript.RegisterStartupScript(Page.GetType(), "VariablesORGANIZACIONCOMPRASEntry" & oFSEntry.Tag, sScript)
                ElseIf sCodOrgCompras <> "" Then
                    sScript = "var sCodOrgComprasFORM='" & sCodOrgCompras & "'"
                    sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                    If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "VariablesORGANIZACIONCOMPRASEntry" & oFSEntry.Tag) Then _
                     Page.ClientScript.RegisterStartupScript(Page.GetType(), "VariablesORGANIZACIONCOMPRASEntry" & oFSEntry.Tag, sScript)
                End If

                If sIdDataEntryCentro <> "" Then
                    sScript = "var sDataEntryCentroFORM='" & sIdDataEntryCentro & "'"
                    sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                    If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "VariablesCENTROEntry" & oFSEntry.Tag) Then _
                     Page.ClientScript.RegisterStartupScript(Page.GetType(), "VariablesCENTROEntry" & oFSEntry.Tag, sScript)
                ElseIf sCodCentro <> "" Then
                    sScript = "var sCodCentroFORM='" & sCodCentro & "'"
                    sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                    If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "VariablesCENTROEntry" & oFSEntry.Tag) Then _
                     Page.ClientScript.RegisterStartupScript(Page.GetType(), "VariablesCENTROEntry" & oFSEntry.Tag, sScript)
                End If

                If Not sidDataEntryUnidadOrganizativa = "" Then
                    sScript = "var sDataEntryUnidadOrganizativaFORM='" & sidDataEntryUnidadOrganizativa & "'"
                    sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                    If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "VariablesUNIDADORGANIZATIVAEntry" & oFSEntry.Tag) Then _
                     Page.ClientScript.RegisterStartupScript(Page.GetType(), "VariablesUNIDADORGANIZATIVAEntry" & oFSEntry.Tag, sScript)
                End If

                If Not sIdDataEntryCentroCoste = "" Then
                    sScript = "var sDataEntryCentroCosteFORM='" & sIdDataEntryCentroCoste & "'"
                    sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                    If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "VariablesCENTROCOSTEEntry" & oFSEntry.Tag) Then _
                     Page.ClientScript.RegisterStartupScript(Page.GetType(), "VariablesCENTROCOSTEEntry" & oFSEntry.Tag, sScript)
                End If

                If sIdDataentryMaterial <> "" Then
                    sScript = "sDataEntryMaterialFORM='" & sIdDataentryMaterial & "'"
                    sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                    If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "VariablesMATERIALEntry" & oFSEntry.Tag) Then _
                         Page.ClientScript.RegisterStartupScript(Page.GetType(), "VariablesMATERIALEntry" & oFSEntry.Tag, sScript)
                End If


            ElseIf oDSRow.Item("VISIBLE") = 1 And DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.DesgloseActividad Then
                nColumna = nColumna + 1
                Dim oucDesgloseActividad As desgloseAct = LoadControl("desgloseAct.ascx")
                oucDesgloseActividad.IdProyecto = DBNullToInteger(oDSRow.Item("VALOR_NUM"))
                oucDesgloseActividad.Obligatorio = DBNullToInteger(oDSRow.Item("OBLIGATORIO"))
                oucDesgloseActividad.Titulo = DBNullToSomething(oDSRow.Item("DEN_" & msIdi))
                oucDesgloseActividad.SoloLectura = oDSRow.Item("ESCRITURA") = 0
                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                    oucDesgloseActividad.ID = Me.ID + "_" + oDSRow.Item("ID_CAMPO").ToString
                    oucDesgloseActividad.Campo = oDSRow.Item("ID_CAMPO")
                Else
                    oucDesgloseActividad.ID = Me.ID + "_" + oDSRow.Item("ID").ToString

                    oucDesgloseActividad.Campo = oDSRow.Item("ID")
                End If
                oFSEntry = New DataEntry.GeneralEntry
                oFSEntry.PM = Me.PM
                oFSEntry.WindowTitle = Me.Page.Title
                oFSEntry.ActivoSM = CType(Me.Page, FSNPage).Acceso.gbAccesoFSSM

                oFSEntry.TipoSolicit = mTipoSolicitud
                oFSEntry.MonSolicit = msInstanciaMoneda

                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                    oFSEntry.Tag = oDSRow.Item("ID_CAMPO").ToString
                    oFSEntry.ID = "fsentry" + oDSRow.Item("ID_CAMPO").ToString

                    oFSEntry.CampoDef_CampoODesgHijo = oDSRow.Item("ID")
                Else
                    oFSEntry.ID = "fsentry" + oDSRow.Item("ID").ToString
                    oFSEntry.Tag = oDSRow.Item("ID").ToString
                End If
                oFSEntry.IdCampoPadre = 0
                oFSEntry.Name = Me.UniqueID
                oFSEntry.IdContenedor = Me.ClientID
                oFSEntry.Idi = msIdi
                oFSEntry.TabContainer = msTabContainer
                oFSEntry.TipoGS = DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS"))
                oFSEntry.Tipo = oDSRow.Item("SUBTIPO")
                oFSEntry.Intro = oDSRow.Item("INTRO")
                oFSEntry.Title = DBNullToSomething(oDSRow.Item("DEN_" & msIdi))
                oFSEntry.WindowTitle = Me.Page.Title
                oFSEntry.Valor = DBNullToInteger(oDSRow.Item("VALOR_NUM"))
                oFSEntry.Obligatorio = DBNullToInteger(oDSRow.Item("OBLIGATORIO"))

                otblRow = New System.Web.UI.WebControls.TableRow
                otblCell = New System.Web.UI.WebControls.TableCell
                otblCell.Width = Unit.Percentage(100)
                otblCell.ColumnSpan = 4
                otblCell.Controls.Add(oFSEntry)
                otblCell.Controls.Add(oucDesgloseActividad)
                otblRow.Cells.Add(otblCell)
                otblCell.CssClass = sClase
                Me.tblDesglose.Rows.Add(otblRow)

            End If
        Next

        Me.Page.ClientScript.RegisterClientScriptResource(GetType(Fullstep.DataEntry.GeneralEntry), "Fullstep.DataEntry.dateCalendar.js")
    End Sub
    Public Function ComprobarCondiciones(ByVal lInstancia As Long, ByVal sFormula As String, ByVal drCondiciones As DataRow(), ByVal dsCampos As DataSet, ByVal dCambioVigenteInstancia As Double) As Boolean
        Dim drCampo As DataRow
        Dim oValorCampo As Object
        Dim oValor As Object
        Dim iTipo As TiposDeDatos.TipoGeneral
        Dim oCond As DataRow
        Dim i As Long
        Dim sVariables() As String
        Dim dValues() As Double
        Dim sMoneda As String
        Dim dCambioEnlace As Double

        Dim iEq As New USPExpress.USPExpression
        Dim FSNServer As FSNServer.Root = Session("FSN_Server")
        Dim oUser As FSNServer.User = Session("FSN_User")
        Try
            i = 0
            For Each oCond In drCondiciones
                ReDim Preserve sVariables(i)
                ReDim Preserve dValues(i)

                sVariables(i) = oCond.Item("COD")
                dValues(i) = 0 'El valor ser� 1 o 0 dependiendo de si se cumple o no la condici�n
                sMoneda = DBNullToStr(oCond.Item("MON"))
                dCambioEnlace = DBNullToDbl(oCond.Item("CAMBIO"))

                '<EXPRESION IZQUIEDA> <OPERADOR> <EXPRESION DERECHA>
                'Primero evaluamos <EXPRESION IZQUIERDA> 
                Select Case oCond.Item("TIPO_CAMPO")
                    Case 1 'Campo de formulario
                        If dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_DATO")).Length = 0 Then
                            If lInstancia > 0 Then
                                Dim oInstancia As FSNServer.Instancia
                                oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                                oInstancia.Version = mlVersion
                                dsCampos = oInstancia.cargarCampo(oCond.Item("CAMPO_DATO"))
                                oInstancia = Nothing
                            Else
                                Dim oSolicitud As FSNServer.Solicitud
                                oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
                                dsCampos = oSolicitud.cargarCampo(oCond.Item("CAMPO_DATO"))
                                oSolicitud = Nothing
                            End If
                        End If

                        If dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_DATO")).Length > 0 Then
                            Dim bDesglose As Boolean = False
                            Dim vValor As Object
                            drCampo = dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_DATO"))(0)

                            Select Case drCampo.Item("SUBTIPO")
                                Case 2
                                    oValorCampo = DBNullToSomething(drCampo.Item("VALOR_NUM"))
                                Case 3
                                    oValorCampo = DBNullToSomething(drCampo.Item("VALOR_FEC"))
                                Case 4
                                    oValorCampo = DBNullToSomething(drCampo.Item("VALOR_BOOL"))
                                Case Else
                                    oValorCampo = DBNullToStr(drCampo.Item("VALOR_TEXT"))
                            End Select

                            iTipo = drCampo.Item("SUBTIPO")
                        End If
                        If sMoneda <> "" Then
                            oValorCampo = oValorCampo / dCambioVigenteInstancia
                        End If
                    Case 2 'Peticionario
                        If mlInstancia > 0 Then
                            'Comprueba si la solicitud es de no conformidad o certificado.Esto habr� que quitarlo cuando
                            'se haga el workflow para certificados y no conformidades
                            Dim oInstancia As FSNServer.Instancia
                            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                            oInstancia.ID = lInstancia
                            oValorCampo = oInstancia.Get_Peticionario_Instancia()
                            oInstancia = Nothing
                        Else
                            oValorCampo = oUser.CodPersona
                        End If
                        iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
                    Case 3 'Departamento del peticionario
                        If mlInstancia > 0 Then
                            Dim oInstancia As FSNServer.Instancia
                            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                            oInstancia.ID = lInstancia
                            Dim oPer As FSNServer.Persona = FSNServer.Get_Object(GetType(FSNServer.Persona))
                            oPer.LoadData(oInstancia.Get_Peticionario_Instancia())
                            oValorCampo = oPer.Departamento
                        Else
                            oValorCampo = oUser.Dep
                        End If

                        iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
                    Case 4 'UON del peticionario
                        If mlInstancia > 0 Then
                            Dim oInstancia As FSNServer.Instancia
                            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                            oInstancia.ID = lInstancia
                            Dim oPer As FSNServer.Persona = FSNServer.Get_Object(GetType(FSNServer.Persona))
                            oPer.LoadData(oInstancia.Get_Peticionario_Instancia())
                            oValorCampo = oPer.UONs
                        Else
                            If oUser.UON3 = "" Then
                                If oUser.UON2 = "" Then
                                    oValorCampo = oUser.UON1
                                Else
                                    oValorCampo = oUser.UON1 & "-" & oUser.UON2
                                End If
                            Else
                                oValorCampo = oUser.UON1 & "-" & oUser.UON2 & "-" & oUser.UON3
                            End If
                        End If

                        iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
                    Case 5 'N� de procesos de compra abiertos
                        If mlInstancia > 0 Then
                            Dim oInstancia As FSNServer.Instancia
                            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                            oInstancia.ID = lInstancia
                            oInstancia.DevolverProcesosRelacionados()
                            oValorCampo = oInstancia.Procesos.Tables(0).Rows.Count
                            oInstancia = Nothing
                        Else
                            oValorCampo = 0
                        End If
                        iTipo = TiposDeDatos.TipoGeneral.TipoNumerico

                    Case 6 'Importe adj 
                        If mlInstancia > 0 Then
                            Dim oInstancia As FSNServer.Instancia
                            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                            oInstancia.ID = lInstancia
                            oValorCampo = oInstancia.ObtenerImporteAdjudicado
                            oInstancia = Nothing
                        Else
                            oValorCampo = 0
                        End If
                        iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
                    Case 7 'Importe de la solicitud de compra
                        If lInstancia > 0 Then
                            Dim oInstancia As FSNServer.Instancia
                            oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                            oInstancia.ID = lInstancia
                            oInstancia.Load(msIdi)
                            oValorCampo = oInstancia.Importe
                            oInstancia = Nothing
                        Else
                            oValorCampo = 0
                        End If
                        If sMoneda <> "" Then
                            oValorCampo = oValorCampo / dCambioVigenteInstancia
                        End If
                        iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
                End Select

                'Luego <EXPRESION DERECHA>
                Select Case oCond.Item("TIPO_VALOR")
                    Case 1 'Campo de formulario
                        If dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_VALOR")).Length = 0 Then
                            If lInstancia > 0 Then
                                Dim oInstancia As FSNServer.Instancia
                                oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                                oInstancia.Version = mlVersion
                                dsCampos = oInstancia.cargarCampo(oCond.Item("CAMPO_VALOR"))
                                oInstancia = Nothing
                            Else
                                Dim oSolicitud As FSNServer.Solicitud
                                oSolicitud = FSNServer.Get_Object(GetType(FSNServer.Solicitud))
                                dsCampos = oSolicitud.cargarCampo(oCond.Item("CAMPO_VALOR"))
                                oSolicitud = Nothing
                            End If
                        End If

                        If dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_VALOR")).Length > 0 Then
                            Dim vValor As Object
                            drCampo = dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_VALOR"))(0)
                            Select Case drCampo.Item("SUBTIPO")
                                Case 2
                                    oValor = DBNullToSomething(drCampo.Item("VALOR_NUM"))
                                Case 3
                                    oValor = DBNullToSomething(drCampo.Item("VALOR_FEC"))
                                Case 4
                                    oValor = DBNullToSomething(drCampo.Item("VALOR_BOOL"))
                                Case Else
                                    oValor = DBNullToSomething(drCampo.Item("VALOR_TEXT"))
                            End Select
                            If sMoneda <> "" Then
                                oValor = oValor / dCambioVigenteInstancia
                            End If
                        End If
                    Case 2, 10 'valor est�tico
                        Select Case iTipo
                            Case 2
                                oValor = DBNullToSomething(oCond.Item("VALOR_NUM"))
                            Case 3
                                oValor = DBNullToSomething(oCond.Item("VALOR_FEC"))
                            Case 4
                                oValor = DBNullToSomething(oCond.Item("VALOR_BOOL"))
                            Case Else
                                If oCond.Item("TIPO_CAMPO") = 4 Then 'UON DEL PETICIONARIO
                                    Dim oUON() As String
                                    Dim iIndice As Integer
                                    oValor = Nothing
                                    oUON = Split(oCond.Item("VALOR_TEXT"), "-")
                                    For iIndice = 0 To UBound(oUON)
                                        If oUON(iIndice) <> "" Then
                                            oValor = oValor & Trim(oUON(iIndice)) & "-"
                                        End If
                                    Next iIndice
                                    oValor = Left(oValor, Len(oValor) - 1)
                                Else
                                    oValor = DBNullToSomething(oCond.Item("VALOR_TEXT"))
                                End If
                        End Select
                        If sMoneda <> "" Then
                            oValor = oValor / dCambioEnlace
                        End If
                End Select
                'y por �ltimo con el OPERADOR obtenemos el valor
                Select Case iTipo
                    Case 2, 3
                        Select Case oCond.Item("OPERADOR")
                            Case ">"
                                If oValorCampo > oValor Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If

                            Case "<"
                                If oValorCampo < oValor Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Case ">="
                                If oValorCampo >= oValor Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Case "<="
                                If oValorCampo <= oValor Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Case "="
                                If oValorCampo = oValor Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Case "<>"
                                If oValorCampo <> oValor Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                        End Select
                    Case 4
                        If IIf(IsNothing(oValorCampo), -1, oValorCampo) = oValor Then
                            dValues(i) = 1
                        Else
                            dValues(i) = 0
                        End If
                    Case Else
                        Select Case UCase(oCond.Item("OPERADOR"))
                            Case "="
                                If UCase(oValorCampo) = UCase(oValor) Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If

                            Case "LIKE"
                                If Left(oValor, 1) = "%" Then
                                    If Right(oValor, 1) = "%" Then
                                        oValor = oValor.ToString.Replace("%", "")
                                        If InStr(oValorCampo.ToString, oValor.ToString) > 0 Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Else
                                        oValor = oValor.ToString.Replace("%", "")
                                        If oValorCampo.ToString.EndsWith(oValor.ToString) Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    End If
                                Else
                                    If Right(oValor, 1) = "%" Then
                                        oValor = oValor.ToString.Replace("%", "")
                                        If oValorCampo.ToString.StartsWith(oValor.ToString) Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Else
                                        If UCase(oValorCampo.ToString) = UCase(oValor.ToString) Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If

                                    End If
                                End If
                            Case "<>"
                                If UCase(oValorCampo.ToString) <> UCase(oValor.ToString) Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                        End Select
                End Select
                i += 1
            Next
            oValor = Nothing
            Try
                iEq.Parse(sFormula, sVariables)
                oValor = iEq.Evaluate(dValues)
            Catch ex As USPExpress.ParseException
            End Try
        Catch e As Exception
            Dim a As String = e.Message
        End Try

        Return (oValor > 0)
    End Function
    ''' <summary>
    ''' Escribe un campo para poner dicho valor en una etiqueta, en vez de un dataentry.
    ''' </summary>
    ''' <param name="oDSRow">Datarow con los valores del campo</param>
    ''' <returns>Una cadena con el valor y si es un enlace con el c�digo correspondiente</returns>
    ''' <remarks>Llamada desde: Page_Load(); Tiempo m�ximo: 0 sg.</remarks>
    Private Function EscribirValorCampo(ByVal oDSRow As DataRow) As String
        Dim sValor As String

        '- Si es una TABLA EXTERNA ya sea de tipo string, num�rico o fecha se obtiene la denominaci�n de VALOR_TEXT_DEN
        '- Si es un CAMPO DEL SISTEMA tb se obtiene la denominaci�n de VALOR_TEXT_DEN
        If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
            If oDSRow.Item("TIPO") = "6" Then
                If Not IsDBNull(oDSRow.Item("VALOR_TEXT_DEN")) Then
                    sValor = DevolverTablaExterna(oDSRow.Item("VALOR_TEXT_DEN"))
                End If
            ElseIf (Not IsDBNull(oDSRow.Item("TIPO_CAMPO_GS")) AndAlso (oDSRow.Item("TIPO_CAMPO_GS") >= TiposDeDatos.TipoCampoGS.ProveedorAdj OrElse oDSRow.Item("TIPO_CAMPO_GS") >= TiposDeDatos.TipoCampoGS.Proveedor) AndAlso oDSRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.DenArticulo AndAlso oDSRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo AndAlso oDSRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.CodArticulo) Then
                If Not IsDBNull(oDSRow.Item("VALOR_TEXT_DEN")) Then
                    If Not IsDBNull(oDSRow.Item("TIPO_CAMPO_GS")) AndAlso (oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.PRES1 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres2 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres3 OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Pres4) Then
                        sValor = DevolverPresupuestosConPorcentajes(oDSRow.Item("VALOR_TEXT"), oDSRow.Item("VALOR_TEXT_DEN"))
                    Else
                        sValor = Fullstep.FSNServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
                    End If
                    If oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.ProveedorAdj OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Proveedor OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.OrganizacionCompras OrElse oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Centro Then
                        sValor = oDSRow.Item("VALOR_TEXT") & " - " & sValor
                    ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Material Then
                        sValor = DevolverMaterialConCodigoUltimoNivel(oDSRow.Item("VALOR_TEXT"), sValor)
                    ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.CentroCoste Then
                        If InStrRev(oDSRow.Item("VALOR_TEXT"), "#") > 0 Then
                            sValor = Right(oDSRow.Item("VALOR_TEXT"), Len(oDSRow.Item("VALOR_TEXT")) - InStrRev(oDSRow.Item("VALOR_TEXT"), "#")) & " - " & sValor
                        Else
                            sValor = oDSRow.Item("VALOR_TEXT") & " - " & sValor
                        End If
                    ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Partida Then
                        If InStrRev(oDSRow.Item("VALOR_TEXT"), "#") > 0 Then
                            If InStrRev(oDSRow.Item("VALOR_TEXT"), "|") > 0 Then 'si no est� en el nivel 1, si no m�s abajo
                                sValor = Right(oDSRow.Item("VALOR_TEXT"), Len(oDSRow.Item("VALOR_TEXT")) - InStrRev(oDSRow.Item("VALOR_TEXT"), "|")) & " - " & sValor
                            Else
                                sValor = Right(oDSRow.Item("VALOR_TEXT"), Len(oDSRow.Item("VALOR_TEXT")) - InStrRev(oDSRow.Item("VALOR_TEXT"), "#")) & " - " & sValor
                            End If
                        Else
                            sValor = oDSRow.Item("VALOR_TEXT") & " - " & sValor
                        End If
                    ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.Activo Then
                        sValor = oDSRow.Item("VALOR_TEXT") & " - " & sValor
                    End If
                ElseIf oDSRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.RefSolicitud AndAlso Not IsDBNull(oDSRow.Item("VALOR_NUM")) Then
                    Dim oInstancia As FSNServer.Instancia
                    oInstancia = FSNServer.Get_Object(GetType(FSNServer.Instancia))
                    oInstancia.ID = oDSRow.Item("VALOR_NUM")
                    Dim sTitulo As String = oInstancia.DevolverTitulo(oUser.Idioma.ToString())
                    If sTitulo <> "" Then
                        sValor = oDSRow.Item("VALOR_NUM") & " - " & sTitulo
                    Else
                        sValor = oDSRow.Item("VALOR_NUM")
                    End If
                    oInstancia = Nothing
                End If

            End If
        End If
        If sValor = "" Then 'por si el campo DEN est� vac�o. Todav�a no se guardan los datos en este campo.
            Select Case oDSRow.Item("SUBTIPO")
                Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoTextoLargo
                    sValor = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
                    If (oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoMedio OrElse oDSRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo) AndAlso Len(sValor) > 80 Then
                        sValor = Mid(sValor, 1, 80) & "..."
                    End If
                Case TiposDeDatos.TipoGeneral.TipoNumerico
                    If Not IsDBNull(oDSRow.Item("VALOR_NUM")) Then
                        sValor = QuitarDecimalSiTodoCeros(FSNLibrary.FormatNumber(oDSRow.Item("VALOR_NUM"), oUser.NumberFormat))
                    End If
                Case TiposDeDatos.TipoGeneral.TipoFecha
                    If IsDBNull(oDSRow.Item("VALOR_FEC")) Then
                        sValor = "&nbsp;"
                    Else
                        sValor = FormatDate(DevolverFechaRelativaA(DBNullToSomething(oDSRow.Item("VALOR_NUM")), DBNullToSomething(oDSRow.Item("VALOR_FEC"))), oUser.DateFormat)
                    End If
                Case TiposDeDatos.TipoGeneral.TipoBoolean
                    If Not IsDBNull(oDSRow.Item("VALOR_BOOL")) Then
                        sValor = IIf(oDSRow.Item("VALOR_BOOL") = 1, oTextos.Rows(15).Item(1), oTextos.Rows(16).Item(1))
                    End If
                Case TiposDeDatos.TipoGeneral.TipoArchivo
                    sAdjun = ""
                    idAdjun = ""
                    Dim oDSRowAdjun As System.Data.DataRow
                    For Each oDSRowAdjun In oDSRow.GetChildRows("REL_CAMPO_ADJUN")
                        idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                        sAdjun += oDSRowAdjun.Item("NOM") + " (" + FSNLibrary.FormatNumber((oDSRowAdjun.Item("DATASIZE") / 1024), oUser.NumberFormat) + " Kb.), "
                    Next
                    If sAdjun <> "" Then
                        sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
                        idAdjun = idAdjun.Substring(0, idAdjun.Length - 2)
                    End If

                    If Len(sAdjun) > 90 Then
                        sValor = Mid(sAdjun, 1, 90) & "..."
                    Else
                        sValor = sAdjun
                    End If

                    If sAdjun <> "" Then
                        Dim enlace As String
                        If oDSRow.GetChildRows("REL_CAMPO_ADJUN").Length > 1 Then
                            enlace = "<a class='aPMWeb' href=""javascript:AbrirFicherosAdjuntos('" & idAdjun & "','" & oDSRow.Item("ID_CAMPO") & "','" & mlInstancia & "',1)"">" & sValor & "</a>"
                        Else
                            enlace = "<a class='aPMWeb' href=""javascript:IrAAdjunto('" & ObtenerPathAdjunto(idAdjun) & "')"">" & sValor & "</a>"
                        End If

                        sValor = enlace
                    End If
                Case TiposDeDatos.TipoGeneral.TipoDesglose
                    oDict.LoadData(TiposDeDatos.ModulosIdiomas.DesgloseControl, msIdi)
                    oTextos = oDict.Data.Tables(0)
                    sValor = oTextos.Rows(3).Item(1)
            End Select
        End If

        If Trim(sValor) = "" Then sValor = "&nbsp;"

        EscribirValorCampo = sValor
    End Function
    Function QuitarDecimalSiTodoCeros(ByVal sValor As String) As String
        Dim decimales As String = ""
        If oUser.PrecisionFmt > 0 AndAlso Right(sValor, oUser.PrecisionFmt + 1) = oUser.DecimalFmt & decimales.PadRight(oUser.PrecisionFmt, "0") Then
            sValor = Left(sValor, Len(sValor) - oUser.PrecisionFmt - 1)
        End If
        QuitarDecimalSiTodoCeros = sValor
    End Function
    Private Function CargarImporteEnTabla(ByVal oLbl As HtmlGenericControl) As HtmlTable
        'Para que los importes salgan alineados a la derecha
        Dim Tbl As System.Web.UI.HtmlControls.HtmlTable
        Dim Row As System.Web.UI.HtmlControls.HtmlTableRow
        Dim Cell As System.Web.UI.HtmlControls.HtmlTableCell

        Row = New System.Web.UI.HtmlControls.HtmlTableRow
        Cell = New System.Web.UI.HtmlControls.HtmlTableCell
        Cell.Controls.Add(oLbl)
        Cell.Align = "right"
        Cell.Attributes("class") = "CampoSoloLectura"
        Row.Cells.Add(Cell)

        Tbl = New System.Web.UI.HtmlControls.HtmlTable
        Tbl.Align = "left"
        Tbl.Width = "100px"
        Tbl.CellPadding = 0
        Tbl.CellSpacing = 0
        Tbl.Rows.Add(Row)

        CargarImporteEnTabla = Tbl
    End Function
    Private Function CargarTextoYBotonEnTabla(ByVal oLbl As HtmlGenericControl, ByVal oImgPopUp As HyperLink) As HtmlTable
        'Para que los botones de pop-up en materiales, presupuestos, archivos,.. salgan alineados a la derecha
        Dim Tbl As System.Web.UI.HtmlControls.HtmlTable
        Dim Row As System.Web.UI.HtmlControls.HtmlTableRow
        Dim Cell As System.Web.UI.HtmlControls.HtmlTableCell

        Row = New System.Web.UI.HtmlControls.HtmlTableRow
        Cell = New System.Web.UI.HtmlControls.HtmlTableCell
        Cell.Controls.Add(oLbl)
        Cell.Align = "left"
        Cell.NoWrap = True
        Cell.Attributes("class") = "CampoSoloLectura"
        Row.Cells.Add(Cell)

        Cell = New System.Web.UI.HtmlControls.HtmlTableCell
        Cell.Controls.Add(oImgPopUp)
        Cell.Align = "right"
        Cell.Attributes("class") = "fsstyentrydd"
        Row.Cells.Add(Cell)

        Tbl = New System.Web.UI.HtmlControls.HtmlTable
        Tbl.CellPadding = 0
        Tbl.CellSpacing = 0
        Tbl.Rows.Add(Row)

        CargarTextoYBotonEnTabla = Tbl
    End Function
    Private Function DevolverMaterialConCodigoUltimoNivel(ByVal sValorText As String, ByVal sValorTextDen As String) As String
        Dim sMat As String = DBNullToSomething(sValorText)
        Dim arrMat(4) As String
        Dim lLongCod As Integer
        Dim i As Integer

        For i = 1 To 4
            arrMat(i) = ""
        Next i

        i = 1
        While Trim(sMat) <> ""
            Select Case i
                Case 1
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN1
                Case 2
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN2
                Case 3
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN3
                Case 4
                    lLongCod = FSNServer.LongitudesDeCodigos.giLongCodGMN4
            End Select
            arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
            sMat = Mid(sMat, lLongCod + 1)
            i = i + 1
        End While

        If Me.FSNServer.TipoAcceso.gbMaterialVerTodosNiveles Then
            If arrMat(4) <> "" Then
                Return arrMat(1) + " - " + arrMat(2) + " - " + arrMat(3) + " - " + arrMat(4) + " - " + sValorTextDen
            ElseIf arrMat(3) <> "" Then
                Return arrMat(1) + " - " + arrMat(2) + " - " + arrMat(3) + " - " + sValorTextDen
            ElseIf arrMat(2) <> "" Then
                Return arrMat(1) + " - " + arrMat(2) + " - " + sValorTextDen
            ElseIf arrMat(1) <> "" Then
                Return arrMat(1) + " - " + sValorTextDen
            End If
        Else
            If arrMat(4) <> "" Then
                Return arrMat(4) + " - " + sValorTextDen
            ElseIf arrMat(3) <> "" Then
                Return arrMat(3) + " - " + sValorTextDen
            ElseIf arrMat(2) <> "" Then
                Return arrMat(2) + " - " + sValorTextDen
            ElseIf arrMat(1) <> "" Then
                Return arrMat(1) + " - " + sValorTextDen
            End If
        End If
    End Function
    Private Function DevolverPresupuestosConPorcentajes(ByVal sValorText As String, ByVal sValorTextDen As String) As String
        Dim arrPresupuestos() As String = sValorText.Split("#")
        Dim arrDenominaciones() As String = sValorTextDen.ToString().Split("#")
        Dim oPresup As String
        Dim iContadorPres As Integer
        Dim arrPresup(2) As String
        Dim dPorcent As Double
        Dim sValor As String

        iContadorPres = 0
        For Each oPresup In arrPresupuestos
            iContadorPres = iContadorPres + 1
            arrPresup = oPresup.Split("_")
            'Los presupuestos siempre llevan el porcentaje como 0.xx salvo cuando es 100% que llevan 1
            dPorcent = Numero(arrPresup(2), ".", "")
            sValor = arrDenominaciones(iContadorPres - 1) & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); " & sValor
        Next
        DevolverPresupuestosConPorcentajes = sValor
    End Function
    ''' <summary>
    ''' Devuelve el valor del campo tipo tabla externa extra�do del campo VALOR_TEXT_DEN. En funci�n del tipo (s: string, n: num�rico, d: fecha)
    ''' </summary>
    ''' <param name="sValorTextDen">Contenido del campo VALOR_TEXT_DEN</param>
    ''' <returns>El contenido en el formato correcto.</returns>
    ''' <remarks>LLamada desde: Page_Load; Tiempo m�ximo: 0,01 sg</remarks>
    Private Function DevolverTablaExterna(ByVal sValorTextDen As String) As String
        Dim sValor As String
        Dim sTxt As String = sValorTextDen.ToString()
        Dim sTxt2 As String
        If sTxt.IndexOf("]#[") > 0 Then
            sTxt2 = sTxt.Substring(2, sTxt.IndexOf("]#[") - 2)
        Else
            sTxt2 = sTxt.Substring(2, sTxt.Length - 3)
        End If
        Select Case sTxt.Substring(0, 1)
            Case "s"
                sValor = sTxt2
            Case "n"
                sValor = Fullstep.FSNLibrary.FormatNumber(Numero(sTxt2, "."), oUser.NumberFormat)
            Case "d"
                sValor = CType(sTxt2, Date).ToString("d", oUser.DateFormat)
        End Select
        If sTxt.IndexOf("]#[") > 0 Then
            sValor = sValor & " "
            sTxt2 = sTxt.Substring(sTxt.IndexOf("]#[") + 3, sTxt.Length - sTxt.IndexOf("]#[") - 5)
            Select Case sTxt.Substring(sTxt.Length - 1, 1)
                Case "s"
                    sValor = sValor & sTxt2
                Case "n"
                    sValor = sValor & Fullstep.FSNLibrary.FormatNumber(Numero(sTxt2, "."), oUser.NumberFormat)
                Case "d"
                    sValor = sValor & CType(sTxt2, Date).ToString("d", oUser.DateFormat)
            End Select
        End If
        DevolverTablaExterna = sValor
    End Function
    ''' <summary>
    ''' Guarda el archivo a disco y llama a download.aspx pasandole el path para mostrarlo o descargarlo
    ''' </summary>
    ''' <param name="Adjuntoid">id del archivo</param>
    ''' <param name="Tipo">Tipo (1: si es un campo normal, 2: el archivo es nuevo, 3: es un campo de un desglose)</param>
    ''' <returns>El enlace para que se abra el archivo con su nombre, path y tama�o.</returns>
    ''' <remarks></remarks>
    Private Function ObtenerPathAdjunto(ByVal Adjuntoid As Long, Optional ByVal Tipo As Integer = 1) As String
        Dim sPath As String
        Dim cAdjunto As FSNServer.Adjunto

        cAdjunto = FSNServer.Get_Object(GetType(FSNServer.Adjunto))
        cAdjunto.Id = Adjuntoid
        cAdjunto.LoadInstFromRequest(Tipo)
        If cAdjunto.dataSize = -1000 Then
            cAdjunto.LoadFromRequest(Tipo)
            sPath = cAdjunto.SaveAdjunToDisk(Tipo)
        Else
            sPath = cAdjunto.SaveAdjunToDisk(Tipo, True)
        End If

        Dim arrPath() As String = sPath.Split("\")
        ObtenerPathAdjunto = ConfigurationManager.AppSettings("rutaFS") & "_common/download.aspx?path=" + arrPath(UBound(arrPath) - 1) + "&nombre=" + arrPath(UBound(arrPath)) + "&datasize=" + cAdjunto.dataSize.ToString
    End Function
    ''' Revisado por: blp. Fecha: 04/10/2011
    ''' <summary>
    ''' Carga la denominaci�n del combo
    ''' </summary>
    ''' <param name="Lista">Origen de datos</param>
    ''' <param name="ColCod">Columna por la q buscar</param>
    ''' <param name="Valor">Valor de codigo a buscar</param>
    ''' <param name="bTextoGuion">Si la denominaci�n es den � cod - den</param>
    ''' <param name="ColPais">Columna pais por la q buscar</param>
    ''' <param name="ValorPais">Valor de pais a buscar</param>
    ''' <returns>la denominaci�n del combo</returns>
    ''' <remarks>Llamada desde: Page_load; Tiempo m�ximo:0</remarks>
    Private Function TextoDelDropDown(ByVal Lista As DataSet, ByVal ColCod As String, ByVal Valor As String, ByVal bTextoGuion As Boolean _
     , Optional ByVal ColPais As String = "", Optional ByVal ValorPais As String = Nothing, Optional ByVal bPoblacion As Boolean = False) As String
        Dim oRowLista() As DataRow
        Dim Sql As String = ColCod & "=" & StrToSQLNULL(Valor)

        If ColPais <> "" Then
            Sql = ColPais & "=" & StrToSQLNULL(ValorPais) & " AND " & Sql
        End If

        If Lista IsNot Nothing AndAlso Lista.Tables(0).Rows.Count > 0 Then
            oRowLista = Lista.Tables(0).Select(Sql)
            If oRowLista.Length > 0 Then
                If bTextoGuion Then
                    If bPoblacion Then
                        Return oRowLista(0).Item("COD") & " - " & oRowLista(0).Item("DEN") & If(IsDBNull(oRowLista(0).Item("POB")), "", " (" & oRowLista(0).Item("POB") & ")")
                    Else
                        Return oRowLista(0).Item("COD") & " - " & oRowLista(0).Item("DEN")
                    End If
                Else
                    Return oRowLista(0).Item("DEN")
                End If
            Else
                Return String.Empty
            End If
        Else
            'Ejemplo: Hay departamento pero no unidad organizativa???
            Return ""
        End If
    End Function
    Public ReadOnly Property InstanciaImportar() As Long
        Get
            If Not Request.QueryString("IdInstanciaImportar") Is Nothing Then
                Return Request.QueryString("IdInstanciaImportar")
            Else
                Return 0
            End If
        End Get
    End Property
End Class

